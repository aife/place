<?php

namespace Application\Service\Atexo;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Pages\Commun\DownloadFile;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonDestinatairePub;
use Application\Propel\Mpe\CommonEnveloppe;
use Application\Propel\Mpe\CommonHabilitationAgent;
use Application\Propel\Mpe\CommonHabilitationAgentPeer;
use Application\Propel\Mpe\CommonOrganisme;
use Application\Propel\Mpe\CommonOrganismeQuery;
use Application\Propel\Mpe\CommonService;
use Application\Propel\Mpe\CommonTContratTitulaire;
use Application\Propel\Mpe\CommonTEtablissement;
use Application\Propel\Mpe\CommonTEtablissementQuery;
use Application\Propel\Mpe\CommonTypeAvisPub;
use Application\Propel\Mpe\CommonTypeProcedureOrganisme;
use Application\Propel\Mpe\Om\BaseCommonHabilitationAgentPeer;
use Application\Service\Atexo\Agent\Atexo_Agent_CriteriaVo;
use Application\Service\Atexo\Chorus\Atexo_Chorus_Echange;
use Application\Service\Atexo\Chorus\Atexo_Chorus_Util;
use Application\Service\Atexo\Commission\Atexo_Commission_Seance;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Category;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Dce;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Decision;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureType;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Register;
use Application\Service\Atexo\Consultation\Atexo_Consultation_TypeAvis;
use Application\Service\Atexo\Contrat\Atexo_Contrat_FormePrix;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Inscrit;
use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN1;
use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2;
use Application\Service\Atexo\Message\Atexo_Message_RelationEchange;
use Application\Service\Atexo\Publicite\Atexo_Publicite_AvisPub;
use Application\Service\Atexo\Referentiel\Atexo_Referentiel_Referentiel;
use Application\Service\Atexo\Socle\Atexo_Socle_AgentServicesMetiers;
use Application\Service\Atexo\Statistiques\Atexo_Statistiques_CriteriaVo;
use Application\Service\Atexo\Statistiques\Atexo_Statistiques_RequetesStatistiques;
use DateTime;
use Exception;
use PDO;
use Prado\Prado;
use Spreadsheet_Excel_Writer;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_GenerationExcel
{
    public static $newLineSeparator = "\n";

    public function generateExcelRegistre($consultationId, $typeRegistre, $toFile = false, $organisme = null, $useSearchFunction = true)
    {
        if (!$organisme) {
            $organisme = Atexo_CurrentUser::getCurrentOrganism();
        }
        if ($useSearchFunction) {
            $criteria = new Atexo_Consultation_CriteriaVo();
            $criteria->setcalledFromPortail(false);
            $criteria->setIdService(Atexo_CurrentUser::getCurrentServiceId());
            $criteria->setAcronymeOrganisme($organisme);
            $criteria->setIdReference($consultationId);
            $criteria->setConnectedAgentId(Atexo_CurrentUser::getId());
            $consultation = (new Atexo_Consultation())->search($criteria);
        } else {
            $consultation = [];
            $consultation[] = (new Atexo_Consultation())->retrieveConsultation($consultationId, $organisme);
        }
        if (count($consultation) > 0 && $consultation[0] instanceof CommonConsultation) {
            $consultation = array_shift($consultation);

            (new Atexo_Config())->includePhpLibPath();
            require_once 'Spreadsheet/Excel/Writer.php';

            //Initialisation du fichier excel
            $tmpFileName = Atexo_Config::getParameter('COMMON_TMP').Prado::localize('TEXT_REGISTRER_XLS');
            if ($toFile) {
                $workbook = new Spreadsheet_Excel_Writer($tmpFileName);
            } else {
                $workbook = new Spreadsheet_Excel_Writer();
            }

            $workbook->setCustomColor(11, 0, 204, 255);
            $workbook->setCustomColor(12, 221, 221, 221);
            //Pour passer outre la limite des 255 caracteres
            $workbook->setVersion(8);

            //Style des entetes
            $formatStantardGras = &$workbook->addFormat();
            $formatStantardGras->setColor('black');
            $formatStantardGras->setPattern(1);
            $formatStantardGras->setFgColor('white');
            $formatStantardGras->setSize(8);
            $formatStantardGras->setAlign('left');
            //$formatStantardGras->setOutLine();
            $formatStantardGras->setBold(1);
            $formatStantardGras->setBorder(1);
            $formatStantardGras->setBorderColor(12);

            $formatStantard = &$workbook->addFormat();
            $formatStantard->setColor('black');
            $formatStantard->setPattern(1);
            $formatStantard->setFgColor('white');
            $formatStantard->setSize(8);
            $formatStantard->setAlign('left');
            $formatStantard->setAlign('top');
            $formatStantard->setBold(0);
            $formatStantard->setBorder(1);
            $formatStantard->setBorderColor(12);

            $formatStantardWrap = &$workbook->addFormat();
            $formatStantardWrap->setColor('black');
            $formatStantardWrap->setFgColor('white');
            $formatStantardWrap->setSize(8);
            $formatStantardWrap->setAlign('left');
            $formatStantardWrap->setAlign('top');
            $formatStantardWrap->setBold(0);
            $formatStantardWrap->setTextWrap();
            $formatStantardWrap->setBorder(1);
            $formatStantardWrap->setBorderColor(12);

            $formatEnteteTableau = &$workbook->addFormat();
            $formatEnteteTableau->setColor('black');
            $formatEnteteTableau->setPattern(1);
            $formatEnteteTableau->setFgColor(11);
            $formatEnteteTableau->setSize(8);
            $formatEnteteTableau->setAlign('left');
            //$formatEnteteTableau->setOutLine();
            $formatEnteteTableau->setBold(1);
            $formatEnteteTableau->setAlign('center');
            $formatEnteteTableau->setAlign('top');
            $formatEnteteTableau->setTextWrap();

            $corpsTableau = &$workbook->addFormat();
            $corpsTableau->setColor('black');
            $corpsTableau->setPattern(1);
            $corpsTableau->setFgColor('white');
            $corpsTableau->setSize(8);
            $corpsTableau->setAlign('top');
            //$corpsTableau->setOutLine();
            $corpsTableau->setBold(0);
            $corpsTableau->setTextWrap();
            $corpsTableau->setBorder(1);
            $corpsTableau->setBorderColor(12);

            // la feuille
            $nomOnglet = '';
            if ($typeRegistre == Atexo_Config::getParameter('REGISTRE_DEPOT_ELECTRONIQUE') ||
               $typeRegistre == Atexo_Config::getParameter('REGISTRE_DEPOT_PAPIER')) {
                $nomOnglet = Prado::localize('DEPOT_REPONSES');
            } elseif ($typeRegistre == Atexo_Config::getParameter('REGISTRE_RETRAIT_ELECTRONIQUE') ||
                     $typeRegistre == Atexo_Config::getParameter('REGISTRE_RETRAIT_PAPIER')) {
                $nomOnglet = Prado::localize('RETRAIT_DOSSIERS');
            } elseif ($typeRegistre == Atexo_Config::getParameter('REGISTRE_QUESTION_ELECTRONIQUE') ||
                     $typeRegistre == Atexo_Config::getParameter('REGISTRE_QUESTION_PAPIER')) {
                $nomOnglet = Prado::localize('QUESTIONS_RECUES');
            }
            $worksheet = &$workbook->addWorkSheet($nomOnglet);
            $worksheet->setInputEncoding(Atexo_Config::getParameter('HTTP_ENCODING'));

            $worksheet->setColumn(0, 0, '3,29');
            $worksheet->setColumn(1, 1, '10,14');
            $worksheet->setColumn(2, 2, '9,43');
            $worksheet->setColumn(3, 3, '8,29');
            $worksheet->setColumn(4, 4, '14,57');
            $worksheet->setColumn(5, 5, '9,43');
            $worksheet->setColumn(6, 6, '11');
            $worksheet->setColumn(7, 7, '14,86');
            $worksheet->setColumn(8, 8, '9,43');
            $worksheet->setColumn(9, 9, '10.49');
            $worksheet->setColumn(10, 10, '11');
            $worksheet->setColumn(11, 11, '13');

            $worksheet->setLandscape();

            if ($typeRegistre == Atexo_Config::getParameter('REGISTRE_DEPOT_ELECTRONIQUE') ||
               $typeRegistre == Atexo_Config::getParameter('REGISTRE_DEPOT_PAPIER')) {
                $nomRegistre = Prado::localize('REGISTRE_DES_DEPOTS');
                $nomfichier = Prado::localize('REGISTRE_DEPOT_REF_ORDRE').$consultation->getReferenceUtilisateur(true).'_'.date('Y').'_'.date('m').'_'.date('d').'.xls';
                $entete = '&"Arial"&C&8'.$nomfichier.' - &A';
                $worksheet->setColumn(10, 10, '30,29');
            } elseif ($typeRegistre == Atexo_Config::getParameter('REGISTRE_RETRAIT_ELECTRONIQUE') ||
                     $typeRegistre == Atexo_Config::getParameter('REGISTRE_RETRAIT_PAPIER')) {
                $nomRegistre = Prado::localize('REGISTRE_DES_RETRAITS');
                $nomfichier = Prado::localize('REGISTRE_RETRAIT_REF_ORDRE').$consultation->getReferenceUtilisateur(true).'_'.date('Y').'_'.date('m').'_'.date('d').'.xls';
                $entete = '&"Arial"&C&8'.$nomfichier.' - &A';
                $worksheet->setColumn(10, 10, '13');
                $worksheet->setColumn(11, 11, '16');
            } elseif ($typeRegistre == Atexo_Config::getParameter('REGISTRE_QUESTION_ELECTRONIQUE') ||
                     $typeRegistre == Atexo_Config::getParameter('REGISTRE_QUESTION_PAPIER')) {
                $nomRegistre = Prado::localize('REGISTRE_DES_QUESTIONS');
                $nomfichier = Prado::localize('REGISTRE_QUESTION_REF_ORDRE').$consultation->getReferenceUtilisateur(true).'_'.date('Y').'_'.date('m').'_'.date('d').'.xls';
                $entete = '&"Arial"&C&8'.$nomfichier.' - &A';
                $worksheet->setColumn(10, 10, '13');
                $worksheet->setColumn(11, 11, '16');
            }

            $worksheet->setHeader($entete);
            $worksheet->setFooter(self::getFooterImprimeLe());
            $worksheet->setMarginLeft(0.15);
            $worksheet->setMarginRight(0.7);

            for ($i = 0; $i <= 17; ++$i) {
                if (8 == $i) {
                    $nombreLigne = ceil(strlen($consultation->getIntitule()) / 120); //120 etant le nombre de caractere aproximatif par ligne (de la colonne D a la colonne L)
                    $nombreLigne = ($nombreLigne < 1) ? 1 : $nombreLigne;
                    $worksheet->setRow($i, '11.25' * $nombreLigne);
                } elseif (9 == $i) {
                    $nombreLigne = ceil(strlen($consultation->getObjet()) / 120); //120 etant le nombre de caractere aproximatif par ligne (de la colonne D a la colonne L)
                    $nombreLigne = ($nombreLigne < 1) ? 1 : $nombreLigne;
                    $worksheet->setRow($i, '11.25' * $nombreLigne);
                } else {
                    $worksheet->setRow($i, '11,25');
                }
            }
            $worksheet->setRow(18, '22,5');
            if ($typeRegistre == Atexo_Config::getParameter('REGISTRE_RETRAIT_ELECTRONIQUE') ||
               $typeRegistre == Atexo_Config::getParameter('REGISTRE_RETRAIT_PAPIER')) {
                if (!$toFile) {
                    $worksheet->setRow(18, '200');
                }
            }

            $worksheet->write(0, 0, $nomRegistre, $formatStantardGras);
            $worksheet->write(0, 1, '', $formatStantard);
            $worksheet->write(0, 2, '', $formatStantard);
            $worksheet->write(0, 3, '', $formatStantard);

            $worksheet->write(2, 0, Prado::localize('IDENTIF_CONS'), $formatStantardGras);
            $worksheet->write(2, 1, '', $formatStantard);
            $worksheet->write(2, 2, '', $formatStantard);
            $worksheet->write(2, 3, '', $formatStantard);

            $worksheet->write(3, 0, Prado::localize('DEFINE_REFERENCE').' :', $formatStantard);
            $worksheet->writeString(3, 4, $consultation->getReferenceUtilisateur(), $formatStantardWrap);
            $worksheet->write(3, 5, '', $formatStantard);
            $worksheet->write(3, 6, '', $formatStantard);
            $worksheet->write(3, 7, '', $formatStantard);
            $worksheet->write(3, 8, '', $formatStantard);
            $worksheet->write(3, 9, '', $formatStantard);
            $worksheet->write(3, 10, '', $formatStantard);

            $entitePublique = Atexo_Organismes::retrieveOrganismeByAcronyme($organisme);
            $worksheet->write(4, 0, Prado::localize('DEFINE_TEXT_ENTITE_PUBLIC').' :', $formatStantard);
            if ($entitePublique instanceof CommonOrganisme) {
                $worksheet->write(4, 4, $entitePublique->getSigle().' - '.$entitePublique->getDenominationOrg(), $formatStantardWrap);
            }
            $worksheet->write(4, 5, '', $formatStantard);
            $worksheet->write(4, 6, '', $formatStantard);
            $worksheet->write(4, 7, '', $formatStantard);
            $worksheet->write(4, 8, '', $formatStantard);
            $worksheet->write(4, 9, '', $formatStantard);
            $worksheet->write(4, 10, '', $formatStantard);

            $service = Atexo_EntityPurchase::getPathEntityById($consultation->getServiceId(), $organisme);
            $worksheet->write(5, 0, Prado::localize('TEXT_ENTITE_ACHAT').' :', $formatStantard);
            $worksheet->write(5, 4, $service, $formatStantardWrap);
            $worksheet->write(5, 5, '', $formatStantard);
            $worksheet->write(5, 6, '', $formatStantard);
            $worksheet->write(5, 7, '', $formatStantard);
            $worksheet->write(5, 8, '', $formatStantard);
            $worksheet->write(5, 9, '', $formatStantard);
            $worksheet->write(5, 10, '', $formatStantard);

            $typeAvis = (new Atexo_Consultation_TypeAvis())->retrieveTypeAnnonce($consultation->getIdTypeAvis());
            if ($typeAvis) {
                $worksheet->write(6, 0, Prado::localize('TYPE_ANNONCE').' :', $formatStantard);
                $worksheet->write(6, 4, $typeAvis->getIntituleAvis(), $formatStantardWrap);
            }
            $worksheet->write(6, 5, '', $formatStantard);
            $worksheet->write(6, 6, '', $formatStantard);
            $worksheet->write(6, 7, '', $formatStantard);
            $worksheet->write(6, 8, '', $formatStantard);
            $worksheet->write(6, 9, '', $formatStantard);
            $worksheet->write(6, 10, '', $formatStantard);

            $typeProcedure = (new Atexo_Consultation_ProcedureType())->retrieveTypeProcedure($consultation->getIdTypeProcedureOrg(), false, $organisme);
            $worksheet->write(7, 0, Prado::localize('TEXT_TYPE_PROCEDURE').' :', $formatStantard);
            $worksheet->write(7, 4, $typeProcedure, $formatStantardWrap);
            $worksheet->write(7, 5, '', $formatStantard);
            $worksheet->write(7, 6, '', $formatStantard);
            $worksheet->write(7, 7, '', $formatStantard);
            $worksheet->write(7, 8, '', $formatStantard);
            $worksheet->write(7, 9, '', $formatStantard);
            $worksheet->write(7, 10, '', $formatStantard);

            $worksheet->write(8, 0, Prado::localize('DEFINE_INTITULE_CONSULTATION').' :', $formatStantard);
            $worksheet->write(8, 4, $consultation->getIntitule(), $formatStantardWrap);
            $worksheet->write(8, 5, '', $formatStantard);
            $worksheet->write(8, 6, '', $formatStantard);
            $worksheet->write(8, 7, '', $formatStantard);
            $worksheet->write(8, 8, '', $formatStantard);
            $worksheet->write(8, 9, '', $formatStantard);
            $worksheet->write(8, 10, '', $formatStantard);

            $worksheet->write(9, 0, Prado::localize('DEFINE_OBJET_CONSULTATION').' :', $formatStantard);
            $worksheet->write(9, 4, $consultation->getObjet(), $formatStantardWrap);
            $worksheet->write(9, 5, '', $formatStantard);
            $worksheet->write(9, 6, '', $formatStantard);
            $worksheet->write(9, 7, '', $formatStantard);
            $worksheet->write(9, 8, '', $formatStantard);
            $worksheet->write(9, 9, '', $formatStantard);
            $worksheet->write(9, 10, '', $formatStantard);

            $worksheet->write(10, 0, Prado::localize('DEFINE_CATEGORIE_PRINCIPAL').' :', $formatStantard);
            $worksheet->write(10, 4, Atexo_Consultation_Category::retrieveLibelleCategorie($consultation->getCategorie(), false, $organisme), $formatStantardWrap);
            $worksheet->write(10, 5, '', $formatStantard);
            $worksheet->write(10, 6, '', $formatStantard);
            $worksheet->write(10, 7, '', $formatStantard);
            $worksheet->write(10, 8, '', $formatStantard);
            $worksheet->write(10, 9, '', $formatStantard);
            $worksheet->write(10, 10, '', $formatStantard);

            $worksheet->write(11, 0, Prado::localize('DEFINE_DATE_LIMITE_REMISE_PLIS').' :', $formatStantard);
            $worksheet->write(11, 4, Atexo_Util::iso2frnDateTime($consultation->getDatefin()), $formatStantardWrap);
            $worksheet->write(11, 5, '', $formatStantard);
            $worksheet->write(11, 6, '', $formatStantard);
            $worksheet->write(11, 7, '', $formatStantard);
            $worksheet->write(11, 8, '', $formatStantard);
            $worksheet->write(11, 9, '', $formatStantard);
            $worksheet->write(11, 10, '', $formatStantard);

            $worksheet->mergeCells(0, 0, 0, 3);
            $worksheet->mergeCells(2, 0, 2, 3);
            $worksheet->mergeCells(3, 0, 3, 3);
            $worksheet->mergeCells(4, 0, 4, 3);
            $worksheet->mergeCells(5, 0, 5, 3);
            $worksheet->mergeCells(6, 0, 6, 3);
            $worksheet->mergeCells(7, 0, 7, 3);
            $worksheet->mergeCells(8, 0, 8, 3);
            $worksheet->mergeCells(9, 0, 9, 3);
            $worksheet->mergeCells(10, 0, 10, 3);
            $worksheet->mergeCells(11, 0, 11, 3);

            $worksheet->mergeCells(3, 4, 3, 10);
            $worksheet->mergeCells(4, 4, 4, 10);
            $worksheet->mergeCells(5, 4, 5, 10);
            $worksheet->mergeCells(6, 4, 6, 10);
            $worksheet->mergeCells(7, 4, 7, 10);
            $worksheet->mergeCells(8, 4, 8, 10);
            $worksheet->mergeCells(9, 4, 9, 10);
            $worksheet->mergeCells(10, 4, 10, 10);
            $worksheet->mergeCells(11, 4, 11, 10);

            if ($typeRegistre == Atexo_Config::getParameter('REGISTRE_RETRAIT_ELECTRONIQUE') ||
             $typeRegistre == Atexo_Config::getParameter('REGISTRE_RETRAIT_PAPIER')) {
                $regsitreElectronique = (new Atexo_Consultation_Register())->retrieveRetraitsElectroniques($consultationId, [], $organisme);
                $registrePapier = (new Atexo_Consultation_Register())->retrieveRetraitsPapier($consultationId, [], $organisme);
            } elseif ($typeRegistre == Atexo_Config::getParameter('REGISTRE_QUESTION_ELECTRONIQUE') ||
                    $typeRegistre == Atexo_Config::getParameter('REGISTRE_QUESTION_PAPIER')) {
                $regsitreElectronique = (new Atexo_Consultation_Register())->retrieveQuestions($consultationId, [], Atexo_Config::getParameter('TYPE_DEPOT_QUESTION_ELECTRONIQUE'), $organisme);
                $registrePapier = (new Atexo_Consultation_Register())->retrieveQuestions($consultationId, [], Atexo_Config::getParameter('TYPE_DEPOT_QUESTION_PAPIER'), $organisme);
            } elseif ($typeRegistre == Atexo_Config::getParameter('REGISTRE_DEPOT_ELECTRONIQUE') ||
                    $typeRegistre == Atexo_Config::getParameter('REGISTRE_DEPOT_PAPIER')) {
                $regsitreElectronique = (new Atexo_Consultation_Register())->retrieveOffreElectronique($consultationId, [], $organisme);
                $registrePapier = (new Atexo_Consultation_Register())->retrieveOffrePapier($consultationId, [], $organisme);
            }

            $regsitreElectronique = Atexo_Util::sortArrayOfObjects($regsitreElectronique, 'NumRegistre', 'ASC', null);
            $registrePapier = Atexo_Util::sortArrayOfObjects($registrePapier, 'NumRegistre', 'ASC', null);

            if (!is_array($registrePapier)) {
                $registrePapier = [];
            }

            if (!is_array($regsitreElectronique)) {
                $regsitreElectronique = [];
            }

            $registres = array_merge($regsitreElectronique, $registrePapier);

            if (is_array($registres)) {
                $colonne = 0;
                $row = 18;

                //Creation entete

                $worksheet->write($row, $colonne++, Prado::localize('NUMERO'), $formatEnteteTableau);
                $worksheet->write($row, $colonne++, Prado::localize('TEXT_RAISON_SOCIALE'), $formatEnteteTableau);
                $worksheet->write($row, $colonne++, Prado::localize('NOM'), $formatEnteteTableau);
                $worksheet->write($row, $colonne++, Prado::localize('DEFINE_PRENOM'), $formatEnteteTableau);
                $worksheet->write($row, $colonne++, Prado::localize('TEXT_ADRESSE_ELECTRONIQUE'), $formatEnteteTableau);
                $worksheet->write($row, $colonne++, Prado::localize('HORODATAGE'), $formatEnteteTableau);
                $worksheet->write($row, $colonne++, Prado::localize('IDENTIFIANT_ENTREPRISE'), $formatEnteteTableau);
                $worksheet->write($row, $colonne++, Prado::localize('DEFINE_ADRESSE_POSTAL'), $formatEnteteTableau);
                $worksheet->write($row, $colonne++, Prado::localize('TEXT_TELEPHONE'), $formatEnteteTableau);
                $worksheet->write($row, $colonne++, Prado::localize('TEXT_FAX'), $formatEnteteTableau);

                if ($typeRegistre == Atexo_Config::getParameter('REGISTRE_RETRAIT_ELECTRONIQUE') ||
                  $typeRegistre == Atexo_Config::getParameter('REGISTRE_RETRAIT_PAPIER')) {
                    if ($toFile) {
                        $worksheet->write($row, $colonne++, Prado::localize('FICHIER_TELECHARGE'), $formatEnteteTableau);
                    } else {
                        $worksheet->write($row, $colonne++, Prado::localize('FICHIER_TELECHARGE_EXCEL_RETRAIT'), $formatEnteteTableau);
                    }
                } elseif ($typeRegistre == Atexo_Config::getParameter('REGISTRE_QUESTION_ELECTRONIQUE') ||
                        $typeRegistre == Atexo_Config::getParameter('REGISTRE_QUESTION_PAPIER')) {
                    $worksheet->write($row, $colonne++, Prado::localize('QUESTION'), $formatEnteteTableau);
                }
                // Envoies postaux complementaires
                if ($typeRegistre == Atexo_Config::getParameter('REGISTRE_RETRAIT_ELECTRONIQUE') ||
                  $typeRegistre == Atexo_Config::getParameter('REGISTRE_RETRAIT_PAPIER')) {
                    $worksheet->write($row, $colonne++, Prado::localize('TEXT_ENVOI_POSTAUX_COMPLEMENTAIRES'), $formatEnteteTableau);
                }
                $worksheet->write($row, $colonne++, Prado::localize('OBSERVATIONS'), $formatEnteteTableau);
                //fin entete

                foreach ($registres as $registreVo) {
                    ++$row;
                    $colonne = 0;

                    if ($registreVo->getTypeRegistre() == Atexo_Config::getParameter('REGISTRE_DEPOT_ELECTRONIQUE') ||
                      $registreVo->getTypeRegistre() == Atexo_Config::getParameter('REGISTRE_RETRAIT_ELECTRONIQUE') ||
                      $registreVo->getTypeRegistre() == Atexo_Config::getParameter('REGISTRE_QUESTION_ELECTRONIQUE')) {
                        $registreType = 'El';
                    } else {
                        $registreType = 'Pa';
                    }
                    $worksheet->write($row, $colonne++, $registreType.'. '.$registreVo->getNumRegistre(), $corpsTableau);
                    $worksheet->write($row, $colonne++, Atexo_Util::atexoHtmlEntitiesDecode($registreVo->getNomEntreprise()), $corpsTableau);
                    $worksheet->write($row, $colonne++, $registreVo->getNomContact(), $corpsTableau);
                    $worksheet->write($row, $colonne++, $registreVo->getPrenomContact(), $corpsTableau);
                    $worksheet->write($row, $colonne++, $registreVo->getMailContact(), $corpsTableau);

                    $horodatage = $registreVo->getHorodatage();
                    if ($registreVo->getRegistreAnnule()) {
                        $horodatage .= '

'.Prado::localize('ANNULE_LE').' : '.$registreVo->getDateAnnulation();
                    }

                    $worksheet->write($row, $colonne++, $horodatage, $corpsTableau);
                    $worksheet->write($row, $colonne++, $registreVo->getAccronymePaysEntreprise().' - '.$registreVo->getSirenEntreprise().' '.$registreVo->getNicEntreprise(), $corpsTableau);
                    $worksheet->write($row, $colonne++, $registreVo->getAdresseContact().((!$registreVo->getAdresseContactSuite()) ? '' : '
'.$registreVo->getAdresseContactSuite()).'
'.$registreVo->getCodePostalContact().' '.$registreVo->getVilleContact().'
'.$registreVo->getPaysEntreprise().(($registreVo->getAdresseContactSuite()) ? '' : '
'), $corpsTableau);
                    $worksheet->writeString($row, $colonne++, $registreVo->getTelephoneContact(), $corpsTableau);
                    $worksheet->writeString($row, $colonne++, $registreVo->getFaxContact(), $corpsTableau);

                    if ($registreVo->getTypeRegistre() == Atexo_Config::getParameter('REGISTRE_RETRAIT_ELECTRONIQUE') ||
                      $registreVo->getTypeRegistre() == Atexo_Config::getParameter('REGISTRE_RETRAIT_PAPIER')) {
                        $fichiers = '';
                        $listeFichiers = $registreVo->getFichiers();
                        foreach ($listeFichiers as $unFichier) {
                            if ('-' != $unFichier) {
                                $fichiers .= $unFichier.'
';
                            }
                        }
                        if ($toFile) {
                            $worksheet->write($row, $colonne++, $fichiers, $corpsTableau);
                        } else {
                            $worksheet->write($row, $colonne++, Atexo_Util::truncateChaine($fichiers, 255), $corpsTableau);
                        }
                    } elseif ($typeRegistre == Atexo_Config::getParameter('REGISTRE_QUESTION_ELECTRONIQUE') ||
                            $typeRegistre == Atexo_Config::getParameter('REGISTRE_QUESTION_PAPIER')) {
                        $worksheet->write($row, $colonne++, html_entity_decode($registreVo->getQuestion(), ENT_QUOTES), $corpsTableau);
                    }

                    if ($typeRegistre == Atexo_Config::getParameter('REGISTRE_RETRAIT_ELECTRONIQUE') ||
                       $typeRegistre == Atexo_Config::getParameter('REGISTRE_RETRAIT_PAPIER')) {
                        if (0 == $consultation->getTiragePlan() || (0 != $consultation->getTiragePlan() && !$registreVo->getTiragePlan())) {
                            $envoisPostauxComplentaires = '';
                        } elseif (0 != $consultation->getTiragePlan() && $registreVo->getTiragePlan() && 1 == $registreVo->getSupport()) {
                            $envoisPostauxComplentaires = Prado::localize('TEXT_FORMAT_PAPIER');
                        } elseif (0 != $consultation->getTiragePlan() && $registreVo->getTiragePlan() && 2 == $registreVo->getSupport()) {
                            $envoisPostauxComplentaires = Prado::localize('TEXT_FORMAT_CDR');
                        } elseif (0 != $consultation->getTiragePlan() && $registreVo->getTiragePlan() && 3 == $registreVo->getSupport()) {
                            $envoisPostauxComplentaires = Prado::localize('TEXT_FORMAT_CDR').' '.Prado::localize('TEXT_FORMAT_PAPIER');
                        } else {
                            $envoisPostauxComplentaires = '';
                        }
                        $worksheet->write($row, $colonne++, $envoisPostauxComplentaires, $corpsTableau);
                    }

                    if ($typeRegistre == Atexo_Config::getParameter('REGISTRE_QUESTION_ELECTRONIQUE') ||
                            $typeRegistre == Atexo_Config::getParameter('REGISTRE_QUESTION_PAPIER')) {
                        $worksheet->write($row, $colonne++, (($registreVo->getResponder()) ? 'Agent : '.$registreVo->getResponder().' ' : '').
                                                                 ($registreVo->getDateReponse() ? '('.$registreVo->getDateReponse().')' : '').'
'.$registreVo->getObservation(), $corpsTableau);
                    } else {
                        $worksheet->write($row, $colonne++, $registreVo->getObservation(), $corpsTableau);
                    }
                }
            }

            $workbook->close();

            if (!$toFile) {
                $workbook->send($nomfichier);
            } else {
                $pathFichier = Atexo_Config::getParameter('COMMON_TMP').$nomfichier;
                //echo "mv " . $tmpFileName . $pathFichier . " 2>&1 > ".Atexo_Config::getParameter('COMMON_TMP')."zip_error", $sys_answer; exit();
                system('mv '.$tmpFileName." '".$pathFichier."' > ".Atexo_Config::getParameter('COMMON_TMP').'zip_error 2>&1');

                return $pathFichier;
            }

            exit;
        }
    }

    //Generation excel : Statistiques Avis publies
    public function genererExcelAvisPublies($acronymeOrganisme, $idEntite, $nomService, $valeursCumules, $dateDebut, $dateFin, $allOrganisme, $data)
    {
        (new Atexo_Config())->includePhpLibPath();
        require_once 'Spreadsheet/Excel/Writer.php';
        $workbook = new Spreadsheet_Excel_Writer();

        $workbook->setCustomColor(11, 51, 204, 255);
        $workbook->setCustomColor(12, 221, 221, 221);
        //Pour passer outre la limite des 255 caracteres
        $workbook->setVersion(8);

        //Style des entetes
        $formatStantardGras = &$workbook->addFormat();
        $formatStantardGras->setSize(10);
        $formatStantardGras->setAlign('left');
        $formatStantardGras->setBold(1);

        $formatStantardGrasAlignRight = &$workbook->addFormat();
        $formatStantardGrasAlignRight->setSize(10);
        $formatStantardGrasAlignRight->setAlign('right');
        $formatStantardGrasAlignRight->setBold(1);
        $formatStantardGrasAlignRight->setBorder(1);

        $formatStantardGrasBorder = &$workbook->addFormat();
        $formatStantardGrasBorder->setSize(10);
        $formatStantardGrasBorder->setAlign('left');
        $formatStantardGrasBorder->setBold(1);
        $formatStantardGrasBorder->setBorder(1);

        $formatStantard = &$workbook->addFormat();
        $formatStantard->setSize(10);
        $formatStantard->setAlign('left');
        $formatStantard->setBold(0);

        $formatEnteteTableau = &$workbook->addFormat();
        $formatEnteteTableau->setFgColor(11);
        $formatEnteteTableau->setSize(10);
        $formatEnteteTableau->setAlign('left');
        //$formatEnteteTableau->setOutLine();
        $formatEnteteTableau->setBold(1);
        $formatEnteteTableau->setAlign('center');
        $formatEnteteTableau->setAlign('top');
        $formatEnteteTableau->setBorder(1);
        $formatEnteteTableau->setTextWrap();

        $corpsTableau = &$workbook->addFormat();
        $corpsTableau->setSize(10);
        $corpsTableau->setAlign('top');
        //$corpsTableau->setOutLine();
        $corpsTableau->setBold(0);
        $corpsTableau->setTextWrap();
        $corpsTableau->setBorder(1);
        $corpsTableau->setLeft(1);
        $corpsTableau->setRight(1);

        $marginLeft = &$workbook->addFormat();
        $marginLeft->setLeft(1);

        $marginRight = &$workbook->addFormat();
        $marginRight->setRight(1);

        $worksheet = &$workbook->addWorkSheet(Atexo_Util::utf8ToIso(Prado::localize('TEXT_AVIS_PUBLIES')));
        /*
         * Call to undefined method Spreadsheet_Excel_Writer_Worksheet::setInputEncoding() in
         *  /home/boukataya/public_html/PMI/mpe/protected/library/Atexo/GenerationExcel.php  on line 689
         * $worksheet->setInputEncoding(Atexo_Config::getParameter('HTTP_ENCODING'));
         */
        if ('iso-8859-1' != Atexo_Config::getParameter('HTTP_ENCODING')) {
            $worksheet->setInputEncoding(Atexo_Config::getParameter('HTTP_ENCODING'));
        }

        $worksheet->setLandscape();
        $worksheet->setPrintScale(77);
        $worksheet->hideGridlines();

        $nomFichier = Prado::localize('TEXT_FAX').date('Ymd');
        $worksheet->setHeader('&"Arial"&8'.$nomFichier.' - '.Prado::localize('TEXT_AVIS_PUBLIES'), '0.51');
        $worksheet->setFooter(self::getFooterImprimeLe(), '0.51');

        $worksheet->setMarginLeft('0.78');
        $worksheet->setMarginRight('0.78');
        $worksheet->setMarginTop('1.01');
        $worksheet->setMarginBottom('1.01');

        $worksheet->setColumn(0, 0, '66.57');
        $worksheet->setColumn(1, 1, '7.14');
        $worksheet->setColumn(2, 2, '9.57');
        $worksheet->setColumn(3, 3, '7.14');
        $worksheet->setColumn(4, 4, '9.57');
        $worksheet->setColumn(5, 5, '7.14');
        $worksheet->setColumn(6, 6, '9.57');
        $worksheet->setColumn(7, 7, '7.14');
        $worksheet->setColumn(8, 8, '9.57');
        $worksheet->setColumn(9, 9, '7.14');
        $worksheet->setColumn(10, 10, '9.57');
        $worksheet->setColumn(11, 11, '7.14');
        $worksheet->setColumn(12, 12, '9.57');

        $worksheet->setRow(0, '12.75');
        $worksheet->setRow(1, '12.75');
        $worksheet->setRow(2, '12.75');
        $worksheet->setRow(3, '12.75');
        $worksheet->setRow(4, '12.75');
        $worksheet->setRow(5, '12.75');
        $worksheet->setRow(6, '12.75');
        $worksheet->setRow(7, '12.75');
        $worksheet->setRow(8, '12.75');

        $worksheet->write(0, 0, Prado::localize('SUIVI_AVIS_PUBLIES'), $formatStantardGras);

        $intervalle = '';
        if ($dateDebut && $dateFin) {
            $intervalle = 'entre le '.$dateDebut.' et le '.$dateFin;
        } elseif ($dateDebut && !$dateFin) {
            $intervalle = 'e partir du '.$dateDebut;
        } elseif (!$dateDebut && $dateFin) {
            $intervalle = "jusqu'au ".$dateFin;
        }

        $worksheet->write(2, 0, Prado::localize('STATISTIQUES_CONCERNANT_PROCEDURE_MISE_EN_LIGNE').$intervalle, $formatStantard);

        if (!$allOrganisme) {
            $worksheet->write(3, 0, Prado::localize('TEXT_ENTITE_ACHAT').' : '.html_entity_decode($nomService, ENT_QUOTES).(($valeursCumules) ? ' / '.Prado::localize('TEXT_VALEUR_CUMULE') : ''), $formatStantard);
        } else {
            $worksheet->write(3, 0, Prado::localize('TEXT_ENTITE_ACHAT').' : '.Prado::localize('DEFINE_TOUS_SERVICES_ORGANISMES_CONFONDUS'), $formatStantard);
        }

        $worksheet->write(5, 1, Prado::localize('TEXT_ANNONCE_INFORMATION'), $formatEnteteTableau);
        $worksheet->write(5, 2, '', $formatEnteteTableau);
        $worksheet->write(5, 3, '', $formatEnteteTableau);
        $worksheet->write(5, 4, '', $formatEnteteTableau);

        $worksheet->mergeCells(5, 1, 5, 4);

        $worksheet->write(5, 5, Prado::localize('TEXT_ANNONCE_CONSULTATION'), $formatEnteteTableau);
        $worksheet->write(5, 6, '', $formatEnteteTableau);
        $worksheet->write(5, 7, '', $formatEnteteTableau);
        $worksheet->write(5, 8, '', $formatEnteteTableau);
        $worksheet->mergeCells(5, 5, 5, 8);

        $worksheet->write(5, 9, Prado::localize('TEXT_ANNONCE_ATTRIBUTION'), $formatEnteteTableau);
        $worksheet->write(5, 10, '', $formatEnteteTableau);
        $worksheet->write(5, 11, '', $formatEnteteTableau);
        $worksheet->write(5, 12, '', $formatEnteteTableau);
        $worksheet->mergeCells(5, 9, 5, 12);

        $worksheet->write(6, 1, Prado::localize('BOAMP'), $formatEnteteTableau);
        $worksheet->write(6, 2, Prado::localize('JOUE'), $formatEnteteTableau);
        $worksheet->write(6, 3, Prado::localize('JAL'), $formatEnteteTableau);
        $worksheet->write(6, 4, Prado::localize('TEXT_SOUS_TOTAL'), $formatEnteteTableau);

        $worksheet->write(6, 5, Prado::localize('BOAMP'), $formatEnteteTableau);
        $worksheet->write(6, 6, Prado::localize('JOUE'), $formatEnteteTableau);
        $worksheet->write(6, 7, Prado::localize('JAL'), $formatEnteteTableau);
        $worksheet->write(6, 8, Prado::localize('TEXT_SOUS_TOTAL'), $formatEnteteTableau);

        $worksheet->write(6, 9, Prado::localize('BOAMP'), $formatEnteteTableau);
        $worksheet->write(6, 10, Prado::localize('JOUE'), $formatEnteteTableau);
        $worksheet->write(6, 11, Prado::localize('JAL'), $formatEnteteTableau);
        $worksheet->write(6, 12, Prado::localize('TEXT_SOUS_TOTAL'), $formatEnteteTableau);

        $row = 7;
        if ($allOrganisme) {
            $arrayTypeProcedure = Atexo_Consultation_ProcedureType::retrieveProcedureType(true, false);
            $typesProceduresInit = (new Atexo_Consultation_ProcedureType())->initStatByTypeProcedure(true);
        } else {
            $arrayTypeProcedure = Atexo_Consultation_ProcedureType::retrieveProcedureType(false, false, $acronymeOrganisme);
            $typesProceduresInit = (new Atexo_Consultation_ProcedureType())->initStatByTypeProcedure(false, $acronymeOrganisme);
        }
        $totalBoampInformationFormalise = 0;
        $totalBoampAttributionFormalise = 0;
        $totalBoampConsultationFormalise = 0;

        $totalJoueInformationFormalise = 0;
        $totalJoueAttributionFormalise = 0;
        $totalJoueConsultationFormalise = 0;

        $totalJalInformationFormalise = 0;
        $totalJalAttributionFormalise = 0;
        $totalJalConsultationFormalise = 0;

        foreach ($typesProceduresInit as $typeProcedure => $valeur) {
            if ((('MAPA_INF' != $typeProcedure && 'MAPA_SUPP' != $typeProcedure) && $allOrganisme) || (!(new Atexo_Consultation_ProcedureType())->isMapa($typeProcedure) && !$allOrganisme)) {
                $colone = 0;
                $libelleProcedure = str_replace('&#8804;', Prado::localize('MAPA_INF_OU_EGALE'), $arrayTypeProcedure[$typeProcedure]);
                $libelleProcedure = str_replace('&#8805;', Prado::localize('MAPA_SUPP_OU_EGALE'), $libelleProcedure);
                $libelleProcedure = html_entity_decode(html_entity_decode($libelleProcedure));
                $worksheet->write($row, $colone++, $libelleProcedure, $corpsTableau);

                //Information
                //BOAMP,JAL,JOUE
                $value = isset($data['Information']['BOAMP'][$typeProcedure]) ? $data['Information']['BOAMP'][$typeProcedure] : '0';
                $worksheet->write($row, $colone++, number_format($value, '0', ',', ''), $corpsTableau);
                $totalBoampInformationFormalise += $value;
                $sousTotalInformationFormalise = $value;

                $value = isset($data['Information']['JOUE'][$typeProcedure]) ? $data['Information']['JOUE'][$typeProcedure] : '0';
                $worksheet->write($row, $colone++, number_format($value, '0', ',', ''), $corpsTableau);
                $totalJoueInformationFormalise += $value;
                $sousTotalInformationFormalise += $value;

                $value = isset($data['Information']['JAL'][$typeProcedure]) ? $data['Information']['JAL'][$typeProcedure] : '0';
                $worksheet->write($row, $colone++, number_format($value, '0', ',', ''), $corpsTableau);
                $totalJalInformationFormalise += $value;
                $sousTotalInformationFormalise += $value;

                //sous Total
                $worksheet->write($row, $colone++, number_format($sousTotalInformationFormalise, '0', ',', ''), $formatStantardGrasAlignRight);

                //AAPC
                //BOAMP,JAL,JOUE
                $value = isset($data['AAPC']['BOAMP'][$typeProcedure]) ? $data['AAPC']['BOAMP'][$typeProcedure] : '0';
                $worksheet->write($row, $colone++, number_format($value, '0', ',', ''), $corpsTableau);
                $totalBoampConsultationFormalise += $value;
                $sousTotalConsultationFormalise = $value;

                $value = isset($data['AAPC']['JOUE'][$typeProcedure]) ? $data['AAPC']['JOUE'][$typeProcedure] : '0';
                $worksheet->write($row, $colone++, number_format($value, '0', ',', ''), $corpsTableau);
                $totalJoueConsultationFormalise += $value;
                $sousTotalConsultationFormalise += $value;

                $value = isset($data['AAPC']['JAL'][$typeProcedure]) ? $data['AAPC']['JAL'][$typeProcedure] : '0';
                $worksheet->write($row, $colone++, number_format($value, '0', ',', ''), $corpsTableau);
                $totalJalConsultationFormalise += $value;
                $sousTotalConsultationFormalise += $value;

                //sous Total
                $worksheet->write($row, $colone++, number_format($sousTotalConsultationFormalise, '0', ',', ''), $formatStantardGrasAlignRight);

                //Attribution
                //BOAMP,JAL,JOUE
                $value = isset($data['Attribution']['BOAMP'][$typeProcedure]) ? $data['Attribution']['BOAMP'][$typeProcedure] : '0';
                $worksheet->write($row, $colone++, number_format($value, '0', ',', ''), $corpsTableau);
                $totalBoampAttributionFormalise += $value;
                $sousTotalAttributionFormalise = $value;

                $value = isset($data['Attribution']['JOUE'][$typeProcedure]) ? $data['Attribution']['JOUE'][$typeProcedure] : '0';
                $worksheet->write($row, $colone++, number_format($value, '0', ',', ''), $corpsTableau);
                $totalJoueAttributionFormalise += $value;
                $sousTotalAttributionFormalise += $value;

                $value = isset($data['Attribution']['JAL'][$typeProcedure]) ? $data['Attribution']['JAL'][$typeProcedure] : '0';
                $worksheet->write($row, $colone++, number_format($value, '0', ',', ''), $corpsTableau);
                $totalJalAttributionFormalise += $value;
                $sousTotalAttributionFormalise += $value;

                //sous Total
                $worksheet->write($row, $colone++, number_format($sousTotalAttributionFormalise, '0', ',', ''), $formatStantardGrasAlignRight);

                ++$row;
            }
        }

        $colonne = 0;
        $worksheet->write($row, $colonne++, Prado::localize('TEXT_TOTAL_PROCEDURES_FORMALISEES'), $formatStantardGrasBorder);

        $worksheet->write($row, $colonne++, number_format($totalBoampInformationFormalise, '0', ',', ''), $formatStantardGrasAlignRight);
        $worksheet->write($row, $colonne++, number_format($totalJoueInformationFormalise, '0', ',', ''), $formatStantardGrasAlignRight);
        $worksheet->write($row, $colonne++, number_format($totalJalInformationFormalise, '0', ',', ''), $formatStantardGrasAlignRight);
        $totalPubInfos = $totalBoampInformationFormalise + $totalJoueInformationFormalise + $totalJalInformationFormalise;
        $worksheet->write($row, $colonne++, number_format($totalPubInfos, '0', ',', ''), $formatStantardGrasAlignRight);

        $worksheet->write($row, $colonne++, number_format($totalBoampConsultationFormalise, '0', ',', ''), $formatStantardGrasAlignRight);
        $worksheet->write($row, $colonne++, number_format($totalJoueConsultationFormalise, '0', ',', ''), $formatStantardGrasAlignRight);
        $worksheet->write($row, $colonne++, number_format($totalJalConsultationFormalise, '0', ',', ''), $formatStantardGrasAlignRight);
        $totalPubCons = $totalBoampConsultationFormalise + $totalJoueConsultationFormalise + $totalJalConsultationFormalise;
        $worksheet->write($row, $colonne++, number_format($totalPubCons, '0', ',', ''), $formatStantardGrasAlignRight);

        $worksheet->write($row, $colonne++, number_format($totalBoampAttributionFormalise, '0', ',', ''), $formatStantardGrasAlignRight);
        $worksheet->write($row, $colonne++, number_format($totalJoueAttributionFormalise, '0', ',', ''), $formatStantardGrasAlignRight);
        $worksheet->write($row, $colonne++, number_format($totalJalAttributionFormalise, '0', ',', ''), $formatStantardGrasAlignRight);
        $totalAttributionPub = $totalBoampAttributionFormalise + $totalJoueAttributionFormalise + $totalJalAttributionFormalise;
        $worksheet->write($row, $colonne++, number_format($totalAttributionPub, '0', ',', ''), $formatStantardGrasAlignRight);

        ++$row;

        // $worksheet->write($row, 0, " ", $marginLeft);
        //$worksheet->write($row, 12, " ", $marginRight);

        ++$row;

        $totalBoampInformationAdapte = 0;
        $totalBoampAttributionAdapte = 0;
        $totalBoampConsultationAdapte = 0;

        $totalJoueInformationAdapte = 0;
        $totalJoueAttributionAdapte = 0;
        $totalJoueConsultationAdapte = 0;

        $totalJalInformationAdapte = 0;
        $totalJalAttributionAdapte = 0;
        $totalJalConsultationAdapte = 0;

        foreach ($typesProceduresInit as $typeProcedure => $valeur) {
            if ((('MAPA_INF' == $typeProcedure || 'MAPA_SUPP' == $typeProcedure) && $allOrganisme) || ((new Atexo_Consultation_ProcedureType())->isMapa($typeProcedure) && !$allOrganisme)) {
                $colone = 0;
                if ('MAPA_INF' == $typeProcedure) {
                    $libelleProcedure = Prado::localize('TEXT_PROCEDURES_ADAPTEES_90_EUR_SHT');
                } elseif ('MAPA_SUPP' == $typeProcedure) {
                    $libelleProcedure = Prado::localize('TEXT_PROCEDURES_ADAPTEES_SUPP_EGALES_90_EUR_HT');
                } else {
                    $libelleProcedure = $arrayTypeProcedure[$typeProcedure];
                }
                $libelleProcedure = str_replace('&#8804;', Prado::localize('MAPA_INF_OU_EGALE'), $libelleProcedure);
                $libelleProcedure = str_replace('&#8805;', Prado::localize('MAPA_SUPP_OU_EGALE'), $libelleProcedure);
                $libelleProcedure = html_entity_decode(html_entity_decode($libelleProcedure));
                $worksheet->write($row, $colone++, $libelleProcedure, $corpsTableau);

                //Information
                //BOAMP,JAL,JOUE
                $value = isset($data['Information']['BOAMP'][$typeProcedure]) ? $data['Information']['BOAMP'][$typeProcedure] : '0';
                $worksheet->write($row, $colone++, number_format($value, '0', ',', ''), $corpsTableau);
                $totalBoampInformationAdapte += $value;
                $sousTotalInformationAdapte = $value;

                $value = isset($data['Information']['JOUE'][$typeProcedure]) ? $data['Information']['JOUE'][$typeProcedure] : '0';
                $worksheet->write($row, $colone++, number_format($value, '0', ',', ''), $corpsTableau);
                $totalJoueInformationAdapte += $value;
                $sousTotalInformationAdapte += $value;

                $value = isset($data['Information']['JAL'][$typeProcedure]) ? $data['Information']['JAL'][$typeProcedure] : '0';
                $worksheet->write($row, $colone++, number_format($value, '0', ',', ''), $corpsTableau);
                $totalJalInformationAdapte += $value;
                $sousTotalInformationAdapte += $value;

                //sous Total
                $worksheet->write($row, $colone++, number_format($sousTotalInformationAdapte, '0', ',', ''), $formatStantardGrasAlignRight);

                //AAPC
                //BOAMP,JAL,JOUE
                $value = isset($data['AAPC']['BOAMP'][$typeProcedure]) ? $data['AAPC']['BOAMP'][$typeProcedure] : '0';
                $worksheet->write($row, $colone++, number_format($value, '0', ',', ''), $corpsTableau);
                $totalBoampConsultationAdapte += $value;
                $sousTotalConsultationAdapte = $value;

                $value = isset($data['AAPC']['JOUE'][$typeProcedure]) ? $data['AAPC']['JOUE'][$typeProcedure] : '0';
                $worksheet->write($row, $colone++, number_format($value, '0', ',', ''), $corpsTableau);
                $totalJoueConsultationAdapte += $value;
                $sousTotalConsultationAdapte += $value;

                $value = isset($data['AAPC']['JAL'][$typeProcedure]) ? $data['AAPC']['JAL'][$typeProcedure] : '0';
                $worksheet->write($row, $colone++, number_format($value, '0', ',', ''), $corpsTableau);
                $totalJalConsultationAdapte += $value;
                $sousTotalConsultationAdapte += $value;

                //sous Total
                $worksheet->write($row, $colone++, number_format($sousTotalConsultationAdapte, '0', ',', ''), $formatStantardGrasAlignRight);

                //Attribution
                //BOAMP,JAL,JOUE
                $value = isset($data['Attribution']['BOAMP'][$typeProcedure]) ? $data['Attribution']['BOAMP'][$typeProcedure] : '0';
                $worksheet->write($row, $colone++, number_format($value, '0', ',', ''), $corpsTableau);
                $totalBoampAttributionAdapte += $value;
                $sousTotalAttributionAdapte = $value;

                $value = isset($data['Attribution']['JOUE'][$typeProcedure]) ? $data['Attribution']['JOUE'][$typeProcedure] : '0';
                $worksheet->write($row, $colone++, number_format($value, '0', ',', ''), $corpsTableau);
                $totalJoueAttributionAdapte += $value;
                $sousTotalAttributionAdapte += $value;

                $value = isset($data['Attribution']['JAL'][$typeProcedure]) ? $data['Attribution']['JAL'][$typeProcedure] : '0';
                $worksheet->write($row, $colone++, number_format($value, '0', ',', ''), $corpsTableau);
                $totalJalAttributionAdapte += $value;
                $sousTotalAttributionAdapte += $value;

                //sous Total
                $worksheet->write($row, $colone++, number_format($sousTotalAttributionAdapte, '0', ',', ''), $formatStantardGrasAlignRight);

                ++$row;
            }
        }
        $colonne = 0;
        $worksheet->write($row, $colonne++, Prado::localize('TEXT_TOTAL_PROCEDURES_ADAPTEES'), $formatStantardGrasBorder);

        $worksheet->write($row, $colonne++, number_format($totalBoampInformationAdapte, '0', ',', ''), $formatStantardGrasAlignRight);
        $worksheet->write($row, $colonne++, number_format($totalJoueInformationAdapte, '0', ',', ''), $formatStantardGrasAlignRight);
        $worksheet->write($row, $colonne++, number_format($totalJalInformationAdapte, '0', ',', ''), $formatStantardGrasAlignRight);
        $worksheet->write($row, $colonne++, number_format(($totalBoampInformationAdapte + $totalJoueInformationAdapte + $totalJalInformationAdapte), '0', ',', ''), $formatStantardGrasAlignRight);

        $worksheet->write($row, $colonne++, number_format($totalBoampConsultationAdapte, '0', ',', ''), $formatStantardGrasAlignRight);
        $worksheet->write($row, $colonne++, number_format($totalJoueConsultationAdapte, '0', ',', ''), $formatStantardGrasAlignRight);
        $worksheet->write($row, $colonne++, number_format($totalJalConsultationAdapte, '0', ',', ''), $formatStantardGrasAlignRight);
        $worksheet->write($row, $colonne++, number_format(($totalBoampConsultationAdapte + $totalJoueConsultationAdapte + $totalJalConsultationAdapte), '0', ',', ''), $formatStantardGrasAlignRight);

        $worksheet->write($row, $colonne++, number_format($totalBoampAttributionAdapte, '0', ',', ''), $formatStantardGrasAlignRight);
        $worksheet->write($row, $colonne++, number_format($totalJoueAttributionAdapte, '0', ',', ''), $formatStantardGrasAlignRight);
        $worksheet->write($row, $colonne++, number_format($totalJalAttributionAdapte, '0', ',', ''), $formatStantardGrasAlignRight);
        $worksheet->write($row, $colonne++, number_format(($totalBoampAttributionAdapte + $totalJoueAttributionAdapte + $totalJalAttributionAdapte), '0', ',', ''), $formatStantardGrasAlignRight);

        ++$row;
        ++$row;
        $colonne = 0;
        $worksheet->write($row, $colonne++, Prado::localize('TEXT_GRAND_TOTAL'), $formatStantardGrasBorder);

        $worksheet->write($row, $colonne++, number_format(($totalBoampInformationFormalise + $totalBoampInformationAdapte), '0', ',', ''), $formatStantardGrasAlignRight);
        $worksheet->write($row, $colonne++, number_format(($totalJoueInformationFormalise + $totalJoueInformationAdapte), '0', ',', ''), $formatStantardGrasAlignRight);
        $worksheet->write($row, $colonne++, number_format(($totalJalInformationFormalise + $totalJalInformationAdapte), '0', ',', ''), $formatStantardGrasAlignRight);
        $worksheet->write($row, $colonne++, number_format(($totalBoampInformationFormalise + $totalBoampInformationAdapte)
                                                              + ($totalJoueInformationFormalise + $totalJoueInformationAdapte)
                                                              + ($totalJalInformationFormalise + $totalJalInformationAdapte), '0', ',', ''), $formatStantardGrasAlignRight);

        $worksheet->write($row, $colonne++, number_format(($totalBoampConsultationFormalise + $totalBoampConsultationAdapte), '0', ',', ''), $formatStantardGrasAlignRight);
        $worksheet->write($row, $colonne++, number_format(($totalJoueConsultationFormalise + $totalJoueConsultationAdapte), '0', ',', ''), $formatStantardGrasAlignRight);
        $worksheet->write($row, $colonne++, number_format(($totalJalConsultationFormalise + $totalJalConsultationAdapte), '0', ',', ''), $formatStantardGrasAlignRight);
        $worksheet->write($row, $colonne++, number_format((($totalBoampConsultationFormalise + $totalBoampConsultationAdapte)
                                                               + ($totalJoueConsultationFormalise + $totalJoueConsultationAdapte)
                                                               + ($totalJalConsultationFormalise + $totalJalConsultationAdapte)), '0', ',', ''), $formatStantardGrasAlignRight);

        $worksheet->write($row, $colonne++, number_format(($totalBoampAttributionFormalise + $totalBoampAttributionAdapte), '0', ',', ''), $formatStantardGrasAlignRight);
        $worksheet->write($row, $colonne++, number_format(($totalJoueAttributionFormalise + $totalJoueAttributionAdapte), '0', ',', ''), $formatStantardGrasAlignRight);
        $worksheet->write($row, $colonne++, number_format(($totalJalAttributionFormalise + $totalJalAttributionAdapte), '0', ',', ''), $formatStantardGrasAlignRight);
        $worksheet->write($row, $colonne++, number_format((($totalBoampAttributionFormalise + $totalBoampAttributionAdapte)
                                                               + ($totalJoueAttributionFormalise + $totalJoueAttributionAdapte)
                                                               + ($totalJalAttributionFormalise + $totalJalAttributionAdapte)), '0', ',', ''), $formatStantardGrasAlignRight);

        $workbook->close();
        $workbook->send($nomFichier.'.xls');
        exit;
    }

    public function genererExcelStatistiquesRechercheAvance($acronymeOrganisme, $data, $objetRecherche, $intervalRecherche, $valeurCumule, $nomEntite, $allOrgs = false, $pourcentage = false, $denominateur = false, $afficherCoordonnees = true)
    {
        (new Atexo_Config())->includePhpLibPath();
        require_once 'Spreadsheet/Excel/Writer.php';
        $workbook = new Spreadsheet_Excel_Writer();

        $workbook->setCustomColor(11, 51, 204, 255);
        $workbook->setCustomColor(12, 221, 221, 221);
        //Pour passer outre la limite des 255 caracteres
        $workbook->setVersion(8);

        //Style des entetes
        $formatStantardGras = &$workbook->addFormat();
        $formatStantardGras->setSize(10);
        $formatStantardGras->setAlign('left');
        $formatStantardGras->setBold(1);

        $formatStantardGrasAlignRight = &$workbook->addFormat();
        $formatStantardGrasAlignRight->setSize(10);
        $formatStantardGrasAlignRight->setAlign('right');
        $formatStantardGrasAlignRight->setBold(1);
        $formatStantardGrasAlignRight->setBorder(1);
        $formatStantardGrasAlignRight->setNumFormat('#,##0');
        if ($pourcentage) {
            $formatStantardGrasAlignRight->setNumFormat('0.00');
        }

        $formatStantardGrasBorder = &$workbook->addFormat();
        $formatStantardGrasBorder->setFgColor(11);
        $formatStantardGrasBorder->setSize(10);
        $formatStantardGrasBorder->setAlign('top');
        $formatStantardGrasBorder->setBold(1);
        $formatStantardGrasBorder->setTextWrap();
        $formatStantardGrasBorder->setBorder(1);

        $formatStantard = &$workbook->addFormat();
        $formatStantard->setSize(10);
        $formatStantard->setAlign('left');
        $formatStantard->setBold(0);

        $formatEnteteTableau = &$workbook->addFormat();
        $formatEnteteTableau->setFgColor(11);
        $formatEnteteTableau->setSize(10);
        $formatEnteteTableau->setAlign('left');
        $formatEnteteTableau->setBold(0);
        $formatEnteteTableau->setAlign('center');
        $formatEnteteTableau->setAlign('top');
        $formatEnteteTableau->setBorder(1);
        $formatEnteteTableau->setTextWrap();

        $corpsTableau = &$workbook->addFormat();
        $corpsTableau->setSize(10);
        $corpsTableau->setAlign('top');
        //$corpsTableau->setOutLine();
        $corpsTableau->setBold(0);
        $corpsTableau->setTextWrap();
        $corpsTableau->setBorder(1);
        $corpsTableau->setLeft(1);
        $corpsTableau->setRight(1);
        $corpsTableau->setNumFormat('#,##0');
        if ($pourcentage) {
            $corpsTableau->setNumFormat('0.00');
            $corpsTableau->setAlign('right');
        }

        $formatEnteteLigne = &$workbook->addFormat();
        $formatEnteteLigne->setSize(10);
        $formatEnteteLigne->setAlign('top');
        //$formatEnteteLigne->setOutLine();
        $formatEnteteLigne->setBold(1);
        $formatEnteteLigne->setTextWrap();
        $formatEnteteLigne->setBorder(1);
        $formatEnteteLigne->setLeft(1);
        $formatEnteteLigne->setRight(1);

        $marginLeft = &$workbook->addFormat();
        $marginLeft->setLeft(1);

        $marginRight = &$workbook->addFormat();
        $marginRight->setRight(1);

        $worksheet = &$workbook->addWorkSheet(Atexo_Util::utf8ToIso(Prado::localize('TEXT_RECHERCHE_AVANCEE')));
        /*
         * Call to undefined method Spreadsheet_Excel_Writer_Worksheet::setInputEncoding() in
         *  /home/boukataya/public_html/PMI/mpe/protected/library/Atexo/GenerationExcel.php  on line 1066
         * $worksheet->setInputEncoding(Atexo_Config::getParameter('HTTP_ENCODING'));
         */
        if ('iso-8859-1' != Atexo_Config::getParameter('HTTP_ENCODING')) {
            $worksheet->setInputEncoding(Atexo_Config::getParameter('HTTP_ENCODING'));
        }

        $worksheet->setLandscape();
        $worksheet->hideGridlines();

        $nomFichier = Prado::localize('TEXT_EXPORTS_STATISTIQUES_').date('Ymd');
        $worksheet->setHeader('&"Arial"&8'.$nomFichier.' - '.Prado::localize('TEXT_RECHERCHE_AVANCEE'), '0.51');
        $worksheet->setFooter(self::getFooterImprimeLe(), '0.51');

        $worksheet->setMarginLeft('0.78');
        $worksheet->setMarginRight('0.78');
        $worksheet->setMarginTop('1.01');
        $worksheet->setMarginBottom('1.01');
        //Taille de la premere colonne
        $worksheet->setColumn(0, 0, '50.43');

        $worksheet->write(0, 0, Prado::localize('TEXT_RECHERCHE_AVANCEE'), $formatStantardGras);

        $row = 2;

        $worksheet->write($row++, 0, Prado::localize('TEXT_OBJET_RECHERCHE').' : '.$objetRecherche, $formatStantard);
        if ($intervalRecherche) {
            $worksheet->write($row++, 0, Prado::localize('DEFINE_INTERVALLE_RECHERCHE').' : '.$intervalRecherche, $formatStantard);
        }

        $worksheet->write($row++, 0, html_entity_decode($nomEntite, ENT_QUOTES), $formatStantard);

        $worksheet->setRow(6, '25');
        $rowEntete = ++$row;
        ++$row;

        $totalParTypeProcedure = [];
        $totalMapaParTypeProcedure = [];

        $writeLibelleProcedure = true;
        $rowStart = $row;
        $arrayTypeProcedurePortail = Atexo_Consultation_ProcedureType::retrieveProcedureType(true, false);
        $arrayLibelleTypeProcedureOrg = Atexo_Consultation_ProcedureType::retrieveProcedureType(false, false, $acronymeOrganisme);

        $row = $rowStart;
        $rowEntete = $row - 1;
        $colonneStart = 1;
        //Debut boucle pour les entites publiques

        foreach ($data as $entite => $arrayData) {
            $arrayProcedure = [];
            $arrayMapa = [];

            //Taille pour chacune des lignes pour les entites publiques
            $worksheet->setRow($row, '12.75');
            //Ecriture des entetes des lignes
            //             if(self::getAcronyme($entite)) {
            //                 $worksheet->write($row, '0', self::getLibelleEntite($entite) .' ('.self::getAcronyme($entite).self::getDepartement($entite).')' , $formatEnteteLigne);
            //             } else {
            $worksheet->write($row, '0', self::getLibelleEntite(html_entity_decode($entite, ENT_QUOTES)), $formatEnteteLigne);
            //             }

            //Ecriture des libelles des procedures formalises.
            $colonne = $colonneStart;
            if ($allOrgs && $afficherCoordonnees) {
                $worksheet->setColumn($colonne, $colonne, '20');
                $worksheet->write($rowEntete, $colonne, Prado::localize('ACRONYME'), $formatEnteteTableau);
                $worksheet->write($row, $colonne, self::getAcronyme(html_entity_decode($entite, ENT_QUOTES)), $formatEnteteLigne);
                ++$colonne;
                $worksheet->setColumn($colonne, $colonne, '20');
                $worksheet->write($rowEntete, $colonne, Prado::localize('CODE_POSTAL'), $formatEnteteTableau);
                $worksheet->write($row, $colonne, self::getDepartement(html_entity_decode($entite, ENT_QUOTES)), $formatEnteteLigne);
                ++$colonne;
            }
            foreach ($arrayData as $typeProcedure => $valeur) {//echo $typeProcedure."=>".$valeur."<br>";
                if ('' != $typeProcedure && 'total' != $typeProcedure && 'grandTotal' != $typeProcedure && 'totalMapa' != $typeProcedure) {
                    if ((('MAPA_INF' != $typeProcedure && 'MAPA_SUPP' != $typeProcedure) && $allOrgs) || (!(new Atexo_Consultation_ProcedureType())->isMapa($typeProcedure) && !$allOrgs)) {
                        if ($writeLibelleProcedure) {
                            if ($allOrgs) {
                                $libelleProcedure = $arrayTypeProcedurePortail[$typeProcedure];
                            } else {
                                $libelleProcedure = $arrayLibelleTypeProcedureOrg[$typeProcedure];
                            }
                            $worksheet->write($rowEntete, $colonne, $libelleProcedure, $formatEnteteTableau);
                            $worksheet->setColumn($colonne, $colonne, '20');
                            ++$colonne;
                        }
                        $arrayProcedure[$typeProcedure] = $valeur;
                    }
                }
            }
            //Texte du total des procedures formalises.
            if ($writeLibelleProcedure) {
                $worksheet->write($rowEntete, $colonne, Prado::localize('TEXT_PROCEDURES_FORMALISEES_ACCORD_CADRE_SAD'), $formatStantardGrasBorder);
                $worksheet->setColumn($colonne, $colonne, '20');
                ++$colonne;
                ++$colonne;
            }

            //Ecriture des libelles des mapa.
            foreach ($arrayData as $typeProcedure => $valeur) {
                if ((('MAPA_INF' == $typeProcedure || 'MAPA_SUPP' == $typeProcedure) && $allOrgs) || ((new Atexo_Consultation_ProcedureType())->isMapa($typeProcedure) && !$allOrgs)) {
                    if ($writeLibelleProcedure) {
                        if ('MAPA_INF' == $typeProcedure) {
                            $libelleProcedure = Prado::localize('TEXT_PROCEDURES_ADAPTEES_90_EUR_SHT');
                        } elseif ('MAPA_SUPP' == $typeProcedure) {
                            $libelleProcedure = Prado::localize('TEXT_PROCEDURES_ADAPTEES_SUPP_EGALES_90_EUR_HT');
                        } else {
                            $libelleProcedure = $arrayLibelleTypeProcedureOrg[$typeProcedure];
                            /*if($allOrgs){
                               $libelleProcedure=$arrayTypeProcedurePortail[$typeProcedure];
                               }
                               else{
                                   $libelleProcedure=$arrayLibelleTypeProcedureOrg[$typeProcedure];
                               }*/
                        }
                        $libelleProcedure = str_replace('&#8804;', Prado::localize('MAPA_INF_OU_EGALE'), $libelleProcedure);
                        $libelleProcedure = str_replace('&#8805;', Prado::localize('MAPA_SUPP_OU_EGALE'), $libelleProcedure);
                        $worksheet->write($rowEntete, $colonne, $libelleProcedure, $formatEnteteTableau);
                        $worksheet->setColumn($colonne, $colonne, '20');
                        ++$colonne;
                    }
                    $arrayMapa[$typeProcedure] = $valeur;
                }
            }
            //Texte du total des mapa.
            if ($writeLibelleProcedure) {
                $worksheet->write($rowEntete, $colonne, Prado::localize('TEXT_TOTAL_PROCEDURES_ADAPTEES'), $formatStantardGrasBorder);
                $worksheet->setColumn($colonne, $colonne, '20');
                //Texte grand total
                ++$colonne;
                $worksheet->setColumn($colonne, $colonne, '20');
                $worksheet->write($rowEntete, $colonne, Prado::localize('TEXT_GRAND_TOTAL'), $formatStantardGrasBorder);
            }
            $total = 0;
            $totalMapa = 0;
            $grandTotal = 0;
            $colonne = $colonneStart;
            if ($allOrgs && $afficherCoordonnees) {
                ++$colonne;
                ++$colonne;
            }
            //Ecriture des valeurs des procedures formalisees et total de procedures formalisees pour chaque entite
            foreach ($arrayProcedure as $typeProcedure => $valeur) {
                if ('' != $typeProcedure && 'total' != $typeProcedure && 'grandTotal' != $typeProcedure && 'totalMapa' != $typeProcedure) {
                    if ($pourcentage) {
                        $value = number_format($valeur, '2', ',', '').'%';
                    } else {
                        $value = $valeur ? number_format($valeur, '0', ',', '') : '0';
                    }
                    $worksheet->write($row, $colonne, $value, $corpsTableau);

                    $totalParTypeProcedure[$typeProcedure] += $valeur;
                    $total += $valeur;
                }
                ++$colonne;
            }
            $grandTotal = $total;
            $totalParTypeProcedure['total'] += $total;

            //Total des procedures formalisees.

            if ($pourcentage) {
                if ($arrayData['total']) {
                    $value = number_format($arrayData['total'], '2', ',', '').'%';
                    $totalPourcentageFormalise += $arrayData['total'];
                } else {
                    $value = number_format(0, '2', ',', '').'%';
                }
                $worksheet->write($row, $colonne, $value, $formatStantardGrasAlignRight);
            } else {
                $worksheet->write($row, $colonne, $total ? $total : '0', $formatStantardGrasAlignRight);
            }

            ++$colonne;
            ++$colonne;
            //Ecriture des valeurs des mapa et total des mapa pour chaque entite
            foreach ($arrayMapa as $typeProcedure => $valeur) {
                if ('' != $typeProcedure && 'total' != $typeProcedure && 'grandTotal' != $typeProcedure && 'totalMapa' != $typeProcedure) {
                    if ($pourcentage) {
                        $value = number_format($valeur, '2', ',', '').'%';
                    } else {
                        $value = $valeur ? number_format($valeur, '0', ',', '') : '0';
                    }
                    $worksheet->write($row, $colonne, $value, $corpsTableau);
                    $total += $valeur;

                    $totalMapaParTypeProcedure[$typeProcedure] += $valeur;
                    $totalMapa += $valeur;
                }
                ++$colonne;
            }
            $totalMapaParTypeProcedure['total'] += $totalMapa;

            //Total des mapas

            if ($pourcentage) {
                if ($arrayData['totalMapa']) {
                    $value = number_format($arrayData['totalMapa'], '2', ',', '').'%';
                    $totalPourcentageMapa += $arrayData['totalMapa']; //echo $totalPourcentageMapa."<br>";
                } else {
                    $value = number_format(0, '2', ',', '').'%';
                }
                $worksheet->write($row, $colonne, $value, $formatStantardGrasAlignRight);
            } else {
                $worksheet->write($row, $colonne, $totalMapa ? number_format($totalMapa, '0', ',', '') : '0', $formatStantardGrasAlignRight);
            }

            //Grand total
            ++$colonne;
            $grandTotal += $totalMapa;

            if ($pourcentage) {
                if ($arrayData['grandTotal']) {
                    $value = number_format($arrayData['grandTotal'], '2', ',', '').'%';
                    $grandTotalPourcentage += $arrayData['grandTotal'];
                } else {
                    $value = number_format(0, '2', ',', '').'%';
                }
                $worksheet->write($row, $colonne, $value, $formatStantardGrasAlignRight);
            } else {
                $worksheet->write($row, $colonne, $grandTotal ? number_format($grandTotal, '0', ',', '') : '0', $formatStantardGrasAlignRight);
            }

            //Incrementation des lignes pour la liste des entites publiques
            ++$row;
            $writeLibelleProcedure = false;
        }//Fin boucle pour les entites publiques

        // Debut ligne "total"
        if (count($data) > 1) {
            $colonne = $colonneStart;
            $worksheet->write($row, '0', Prado::localize('TEXT_TOTAL'), $formatEnteteLigne);
            if ($allOrgs && $afficherCoordonnees) {
                $worksheet->write($row, $colonne, '', $corpsTableau);
                ++$colonne;
                $worksheet->write($row, $colonne, '', $corpsTableau);
                ++$colonne;
            }
            $decimal = 0;
            if (is_array($denominateur) && $pourcentage) {
                if (is_array($totalParTypeProcedure)) {
                    foreach ($totalParTypeProcedure as $key => $value) {
                        if ($denominateur[$key] > 0) {
                            $res[$key] = $totalParTypeProcedure[$key] / $denominateur[$key];
                        } else {
                            $res[$key] = 0;
                        }
                    }
                    $results = $res;
                }
                $totalParTypeProcedure = $results;
                $res = [];
                if (is_array($totalMapaParTypeProcedure)) {
                    foreach ($totalMapaParTypeProcedure as $key => $value) {
                        if ($denominateur[$key] > 0) {
                            $res[$key] = $totalMapaParTypeProcedure[$key] / $denominateur[$key];
                        } else {
                            $res[$key] = 0;
                        }
                    }
                    $results = $res;
                }
                $totalMapaParTypeProcedure = $results;
                $decimal = 2;
            }
            foreach ($totalParTypeProcedure as $key => $total) {
                if ('total' == $key) {
                    $gdTotal += $total;
                    if (is_array($denominateur) && $pourcentage) {
                        if ($denominateur['total'] > 0) {
                            $totalPourcentageFormalise = number_format(($totalPourcentageFormalise / $denominateur['total']), '2', ',', '').'%';
                        }
                        $worksheet->write($row, $colonne, $totalPourcentageFormalise, $formatStantardGrasAlignRight);
                    } else {
                        $worksheet->write($row, $colonne, $total ? number_format($total, $decimal, ',', '') : '0', $formatStantardGrasAlignRight);
                    }
                } else {
                    if ($pourcentage && is_array($denominateur)) {
                        $value = number_format($total, '2', ',', '').'%';
                        $worksheet->write($row, $colonne, $value, $formatStantardGrasAlignRight);
                    } else {
                        $worksheet->write($row, $colonne, $total ? number_format($total, $decimal, ',', '') : '0', $formatStantardGrasAlignRight);
                    }
                }
                ++$colonne;
            }
            ++$colonne;
            foreach ($totalMapaParTypeProcedure as $key => $total) {
                if ('total' == $key) {
                    $gdTotal += $total;
                    if (is_array($denominateur) && $pourcentage) {
                        if ($denominateur['totalMapa'] > 0) {
                            $totalPourcentageMapa = number_format(($totalPourcentageMapa / $denominateur['totalMapa']), '2', ',', '').'%';
                        }
                        $worksheet->write($row, $colonne, $totalPourcentageMapa, $formatStantardGrasAlignRight);
                    } else {
                        $worksheet->write($row, $colonne, $total ? number_format($total, $decimal, ',', '') : '0', $formatStantardGrasAlignRight);
                    }
                } else {
                    if ($pourcentage && is_array($denominateur)) {
                        $value = number_format($total, '2', ',', '').'%';
                        $worksheet->write($row, $colonne, $value, $formatStantardGrasAlignRight);
                    } else {
                        $worksheet->write($row, $colonne, $total ? number_format($total, $decimal, ',', '') : '0', $formatStantardGrasAlignRight);
                    }
                }
                ++$colonne;
            }

            if (is_array($denominateur) && $pourcentage) {
                if ($denominateur['grandTotal'] > 0) {
                    $grandTotalPourcentage = number_format(($grandTotalPourcentage / $denominateur['grandTotal']), '2', ',', '').'%';
                }
                $worksheet->write($row, $colonne, $grandTotalPourcentage, $formatStantardGrasAlignRight);
            } else {
                $worksheet->write($row, $colonne, $gdTotal ? number_format($gdTotal, '0', ',', '') : '0', $formatStantardGrasAlignRight);
            }
            ++$colonne;
            ++$colonne;
        }
        // Fin ligne "total"
        $workbook->close();
        $workbook->send($nomFichier.'.xls');
        exit;
    }

    public function genererExcelStatistiquesMetiers($acronymeOrganisme, $data, $objetRecherche, $valeurCumule, $nomEntite, $allOrgs = false)
    {
        (new Atexo_Config())->includePhpLibPath();
        require_once 'Spreadsheet/Excel/Writer.php';
        $workbook = new Spreadsheet_Excel_Writer();

        $workbook->setCustomColor(11, 51, 204, 255);
        $workbook->setCustomColor(12, 221, 221, 221);
        //Pour passer outre la limite des 255 caracteres
        $workbook->setVersion(8);

        //Style des entetes
        $formatStantardGras = &$workbook->addFormat();
        $formatStantardGras->setSize(10);
        $formatStantardGras->setAlign('left');
        $formatStantardGras->setBold(1);
        $formatStantardGrasAlignRight = &$workbook->addFormat();
        $formatStantardGrasAlignRight->setSize(10);
        $formatStantardGrasAlignRight->setAlign('right');
        $formatStantardGrasAlignRight->setBold(1);
        $formatStantardGrasAlignRight->setBorder(1);

        $formatStantardGrasBorder = &$workbook->addFormat();
        $formatStantardGrasBorder->setSize(10);
        $formatStantardGrasBorder->setAlign('left');
        $formatStantardGrasBorder->setBold(1);
        $formatStantardGrasBorder->setBorder(1);

        $formatStantard = &$workbook->addFormat();
        $formatStantard->setSize(10);
        $formatStantard->setAlign('left');
        $formatStantard->setBold(0);

        $formatEnteteTableau = &$workbook->addFormat();
        $formatEnteteTableau->setFgColor(11);
        $formatEnteteTableau->setSize(10);
        $formatEnteteTableau->setAlign('left');
        $formatEnteteTableau->setAlign('top');
        $formatEnteteTableau->setBorder(1);
        $formatEnteteTableau->setTextWrap();

        $corpsTableau = &$workbook->addFormat();
        $corpsTableau->setSize(10);
        $corpsTableau->setAlign('top');
        $corpsTableau->setBold(0);
        $corpsTableau->setTextWrap();
        $corpsTableau->setBorder(1);

        $marginLeft = &$workbook->addFormat();
        $marginLeft->setLeft(1);

        $marginRight = &$workbook->addFormat();
        $marginRight->setRight(1);

        $worksheet = &$workbook->addWorkSheet(Atexo_Util::utf8ToIso(Prado::localize('TEXT_DONNEES_METIER')));
        /*
         * Call to undefined method Spreadsheet_Excel_Writer_Worksheet::setInputEncoding()
         * in /home/boukataya/public_html/PMI/mpe/protected/library/Atexo/GenerationExcel.php  on line 1289
         *$worksheet->setInputEncoding(Atexo_Config::getParameter('HTTP_ENCODING'));
         **/
        if ('iso-8859-1' != Atexo_Config::getParameter('HTTP_ENCODING')) {
            $worksheet->setInputEncoding(Atexo_Config::getParameter('HTTP_ENCODING'));
        }

        $worksheet->setLandscape();
        $worksheet->setPrintScale(90);
        $worksheet->hideGridlines();

        $nomFichier = Prado::localize('TEXT_EXPORTS_STATISTIQUES_').date('Ymd');
        $worksheet->setHeader('&"Arial"&8'.$nomFichier.' - '.Prado::localize('TEXT_DONNEES_METIER'), '0.51');
        $worksheet->setFooter(self::getFooterImprimeLe(), '0.51');

        $worksheet->setMarginLeft('0.78');
        $worksheet->setMarginRight('0.78');
        $worksheet->setMarginTop('1.01');
        $worksheet->setMarginBottom('1.01');

        $worksheet->setColumn(0, 0, '67.86');
        $worksheet->setColumn(1, 1, '7.14');
        $worksheet->setColumn(2, 2, '9.57');
        $worksheet->setColumn(3, 3, '7.14');
        $worksheet->setColumn(4, 4, '9.57');
        $worksheet->setColumn(5, 5, '7.14');
        $worksheet->setColumn(6, 6, '9.57');
        $worksheet->setColumn(7, 7, '7.14');
        $worksheet->setColumn(8, 8, '9.57');
        $worksheet->setColumn(9, 9, '7.14');
        $worksheet->setColumn(10, 10, '9.57');
        $worksheet->setColumn(11, 11, '7.14');
        $worksheet->setColumn(12, 12, '9.57');

        $worksheet->write(0, 0, Prado::localize('TEXT_SUIVI_DONNEES_METIER'), $formatStantardGras);

        $worksheet->write(2, 0, $objetRecherche, $formatStantard);
        $worksheet->write(3, 0, Prado::localize('TEXT_ENTITE_ACHAT').' : '.html_entity_decode($nomEntite, ENT_QUOTES).$valeurCumule, $formatStantard);

        $worksheet->write(5, 1, Prado::localize('TEXT_PROCEDURES_MISES_LIGNE'), $formatEnteteTableau);
        $worksheet->write(5, 2, '', $formatEnteteTableau);
        $worksheet->write(5, 3, '', $formatEnteteTableau);
        $worksheet->write(5, 4, '', $formatEnteteTableau);

        $worksheet->write(5, 5, Prado::localize('RETRAITS'), $formatEnteteTableau);
        $worksheet->write(5, 6, '', $formatEnteteTableau);
        $worksheet->write(5, 7, '', $formatEnteteTableau);
        $worksheet->write(5, 8, '', $formatEnteteTableau);

        $worksheet->write(5, 9, Prado::localize('OFFRES'), $formatEnteteTableau);
        $worksheet->write(5, 10, '', $formatEnteteTableau);
        $worksheet->write(5, 11, '', $formatEnteteTableau);
        $worksheet->write(5, 12, '', $formatEnteteTableau);

        $worksheet->mergeCells(5, 1, 5, 4);
        $worksheet->mergeCells(5, 5, 5, 8);
        $worksheet->mergeCells(5, 9, 5, 12);

        $worksheet->write(6, 1, Prado::localize('DEFINE_TRAVAUX'), $formatEnteteTableau);
        $worksheet->write(6, 2, Prado::localize('DEFINE_FOURNITURES'), $formatEnteteTableau);
        $worksheet->write(6, 3, Prado::localize('DEFINE_SERVICES'), $formatEnteteTableau);
        $worksheet->write(6, 4, Prado::localize('TEXT_SOUS_TOTAL'), $formatEnteteTableau);

        $worksheet->write(6, 5, Prado::localize('DEFINE_TRAVAUX'), $formatEnteteTableau);
        $worksheet->write(6, 6, Prado::localize('DEFINE_FOURNITURES'), $formatEnteteTableau);
        $worksheet->write(6, 7, Prado::localize('DEFINE_SERVICES'), $formatEnteteTableau);
        $worksheet->write(6, 8, Prado::localize('TEXT_SOUS_TOTAL'), $formatEnteteTableau);

        $worksheet->write(6, 9, Prado::localize('DEFINE_TRAVAUX'), $formatEnteteTableau);
        $worksheet->write(6, 10, Prado::localize('DEFINE_FOURNITURES'), $formatEnteteTableau);
        $worksheet->write(6, 11, Prado::localize('DEFINE_SERVICES'), $formatEnteteTableau);
        $worksheet->write(6, 12, Prado::localize('TEXT_SOUS_TOTAL'), $formatEnteteTableau);
        if ($allOrgs) {
            $arrayTypeProcedure = Atexo_Consultation_ProcedureType::retrieveProcedureType(true, false);
            $typesProceduresInit = (new Atexo_Consultation_ProcedureType())->initStatByTypeProcedure(true);
        } else {
            $arrayTypeProcedure = Atexo_Consultation_ProcedureType::retrieveProcedureType(false, false, $acronymeOrganisme);
            $typesProceduresInit = (new Atexo_Consultation_ProcedureType())->initStatByTypeProcedure(false, $acronymeOrganisme);
        }
        /*
            $c=new Criteria();
            $c->add(TypeprocedurePeer::MAPA, '0');
            $connexion=Propel::getConnection($acronymeOrganisme);
             $typesProcedures=TypeprocedurePeer::doSelect($c, $connexion);
        */

        $row = 7;

        $totalProcMisEnLigneTravauxFormalise = 0;
        $totalProcMisEnLigneFournituresFormalise = 0;
        $totalProcMisEnLigneServicesFormalise = 0;
        $totalRetraitTravauxFormalise = 0;
        $totalRetraitFournitureFormalise = 0;
        $totalRetraitServiceFormalise = 0;
        $totalOffreTravauxFormalise = 0;
        $totalOffreFournitureFormalise = 0;
        $totalOffreServiceFormalise = 0;

        foreach ($typesProceduresInit as $typeProcedure => $valeur) {
            if ((('MAPA_INF' != $typeProcedure && 'MAPA_SUPP' != $typeProcedure) && $allOrgs) || (!(new Atexo_Consultation_ProcedureType())->isMapa($typeProcedure) && !$allOrgs)) {
                $colone = 0;
                $libelleProcedure = str_replace('&#8804;', Prado::localize('MAPA_INF_OU_EGALE'), $arrayTypeProcedure[$typeProcedure]);
                $libelleProcedure = str_replace('&#8805;', Prado::localize('MAPA_SUPP_OU_EGALE'), $libelleProcedure);
                $worksheet->write($row, $colone++, $libelleProcedure, $corpsTableau);

                $value = isset($data['procedureMiseEnLigne']['travaux'][$typeProcedure]) ? $data['procedureMiseEnLigne']['travaux'][$typeProcedure] : '0';
                $worksheet->write($row, $colone++, number_format($value, '0', ',', ''), $corpsTableau);
                $totalProcMisEnLigneTravauxFormalise += $value;
                $soustotalConsFormalise = $value;

                $value = isset($data['procedureMiseEnLigne']['fournitures'][$typeProcedure]) ? $data['procedureMiseEnLigne']['fournitures'][$typeProcedure] : '0';
                $worksheet->write($row, $colone++, number_format($value, '0', ',', ''), $corpsTableau);
                $totalProcMisEnLigneFournituresFormalise += $value;
                $soustotalConsFormalise += $value;

                $value = isset($data['procedureMiseEnLigne']['services'][$typeProcedure]) ? $data['procedureMiseEnLigne']['services'][$typeProcedure] : '0';
                $worksheet->write($row, $colone++, number_format($value, '0', ',', ''), $corpsTableau);
                $totalProcMisEnLigneServicesFormalise += $value;
                $soustotalConsFormalise += $value;

                $worksheet->write($row, $colone++, number_format($soustotalConsFormalise, '0', ',', ''), $formatStantardGrasAlignRight);

                $value = isset($data['retraits']['travaux'][$typeProcedure]) ? $data['retraits']['travaux'][$typeProcedure] : '0';
                $worksheet->write($row, $colone++, number_format($value, '0', ',', ''), $corpsTableau);
                $totalRetraitTravauxFormalise += $value;
                $soustotalRetraitsFormalise = $value;

                $value = isset($data['retraits']['fournitures'][$typeProcedure]) ? $data['retraits']['fournitures'][$typeProcedure] : '0';
                $worksheet->write($row, $colone++, number_format($value, '0', ',', ''), $corpsTableau);
                $totalRetraitFournitureFormalise += $value;
                $soustotalRetraitsFormalise += $value;

                $value = isset($data['retraits']['services'][$typeProcedure]) ? $data['retraits']['services'][$typeProcedure] : '0';
                $worksheet->write($row, $colone++, number_format($value, '0', ',', ''), $corpsTableau);
                $totalRetraitServiceFormalise += $value;
                $soustotalRetraitsFormalise += $value;

                $worksheet->write($row, $colone++, number_format($soustotalRetraitsFormalise, '0', ',', ''), $formatStantardGrasAlignRight);

                $value = isset($data['offres']['travaux'][$typeProcedure]) ? $data['offres']['travaux'][$typeProcedure] : '0';
                $worksheet->write($row, $colone++, number_format($value, '0', ',', ''), $corpsTableau);
                $totalOffreTravauxFormalise += $value;
                $soustotalOffresFormalise = $value;

                $value = isset($data['offres']['fournitures'][$typeProcedure]) ? $data['offres']['fournitures'][$typeProcedure] : '0';
                $worksheet->write($row, $colone++, number_format($value, '0', ',', ''), $corpsTableau);
                $totalOffreFournitureFormalise += $value;
                $soustotalOffresFormalise += $value;

                $value = isset($data['offres']['services'][$typeProcedure]) ? $data['offres']['services'][$typeProcedure] : '0';
                $worksheet->write($row, $colone++, number_format($value, '0', ',', ''), $corpsTableau);
                $totalOffreServiceFormalise += $value;
                $soustotalOffresFormalise += $value;

                $worksheet->write($row, $colone++, number_format($soustotalOffresFormalise, '0', ',', ''), $formatStantardGrasAlignRight);

                ++$colone;

                ++$row;
            }
        }

        $worksheet->write($row, 0, Prado::localize('TEXT_TOTAL_PROCEDURES_FORMALISEES'), $formatStantardGrasBorder);

        $worksheet->write($row, 1, number_format($totalProcMisEnLigneTravauxFormalise, '0', ',', ''), $formatStantardGrasAlignRight);
        $worksheet->write($row, 2, number_format($totalProcMisEnLigneFournituresFormalise, '0', ',', ''), $formatStantardGrasAlignRight);
        $worksheet->write($row, 3, number_format($totalProcMisEnLigneServicesFormalise, '0', ',', ''), $formatStantardGrasAlignRight);
        $worksheet->write($row, 4, number_format($totalProcMisEnLigneServicesFormalise
                                                + $totalProcMisEnLigneFournituresFormalise
                                                + $totalProcMisEnLigneTravauxFormalise, '0', ',', ''), $formatStantardGrasAlignRight);
        $worksheet->write($row, 5, number_format($totalRetraitTravauxFormalise, '0', ',', ''), $formatStantardGrasAlignRight);
        $worksheet->write($row, 6, number_format($totalRetraitFournitureFormalise, '0', ',', ''), $formatStantardGrasAlignRight);
        $worksheet->write($row, 7, number_format($totalRetraitServiceFormalise, '0', ',', ''), $formatStantardGrasAlignRight);
        $worksheet->write($row, 8, number_format($totalRetraitServiceFormalise
                                                + $totalRetraitTravauxFormalise
                                                + $totalRetraitFournitureFormalise, '0', ',', ''), $formatStantardGrasAlignRight);
        $worksheet->write($row, 9, number_format($totalOffreTravauxFormalise, '0', ',', ''), $formatStantardGrasAlignRight);
        $worksheet->write($row, 10, number_format($totalOffreFournitureFormalise, '0', ',', ''), $formatStantardGrasAlignRight);
        $worksheet->write($row, 11, number_format($totalOffreServiceFormalise, '0', ',', ''), $formatStantardGrasAlignRight);
        $worksheet->write($row, 12, number_format($totalOffreServiceFormalise
                                                 + $totalOffreTravauxFormalise
                                                 + $totalOffreFournitureFormalise, '0', ',', ''), $formatStantardGrasAlignRight);

        ++$row;
        ++$row;

        /*
            $c=new Criteria();
            $c->add(TypeprocedurePeer::MAPA, '1');
            $connexion=Propel::getConnection($acronymeOrganisme);
             $typesProcedures=TypeprocedurePeer::doSelect($c, $connexion);
        */
        $totalProcMisEnLigneTravauxAdapte = 0;
        $totalProcMisEnLigneFournituresAdapte = 0;
        $totalProcMisEnLigneServicesAdapte = 0;
        $totalRetraitTravauxAdapte = 0;
        $totalRetraitFournitureAdapte = 0;
        $totalRetraitServiceAdapte = 0;
        $totalOffreTravauxAdapte = 0;
        $totalOffreFournitureAdapte = 0;
        $totalOffreServiceAdapte = 0;

        foreach ($typesProceduresInit as $typeProcedure => $valeur) {
            if ((('MAPA_INF' == $typeProcedure || 'MAPA_SUPP' == $typeProcedure) && $allOrgs) || ((new Atexo_Consultation_ProcedureType())->isMapa($typeProcedure) && !$allOrgs)) {
                $colone = 0;
                if ('MAPA_INF' == $typeProcedure) {
                    $libelleProcedure = Prado::localize('TEXT_PROCEDURES_ADAPTEES_90_EUR_SHT');
                } elseif ('MAPA_SUPP' == $typeProcedure) {
                    $libelleProcedure = Prado::localize('TEXT_PROCEDURES_ADAPTEES_SUPP_EGALES_90_EUR_HT');
                } else {
                    $libelleProcedure = $arrayTypeProcedure[$typeProcedure];
                }
                $libelleProcedure = str_replace('&#8804;', Prado::localize('MAPA_INF_OU_EGALE'), $libelleProcedure);
                $libelleProcedure = str_replace('&#8805;', Prado::localize('MAPA_SUPP_OU_EGALE'), $libelleProcedure);
                $worksheet->write($row, $colone++, $libelleProcedure, $corpsTableau);

                $value = isset($data['procedureMiseEnLigne']['travaux'][$typeProcedure]) ? $data['procedureMiseEnLigne']['travaux'][$typeProcedure] : '0';
                $worksheet->write($row, $colone++, number_format($value, '0', ',', ''), $corpsTableau);
                $totalProcMisEnLigneTravauxAdapte += $value;
                $soustotalConsAdapte = $value;

                $value = isset($data['procedureMiseEnLigne']['fournitures'][$typeProcedure]) ? $data['procedureMiseEnLigne']['fournitures'][$typeProcedure] : '0';
                $worksheet->write($row, $colone++, number_format($value, '0', ',', ''), $corpsTableau);
                $totalProcMisEnLigneFournituresAdapte += $value;
                $soustotalConsAdapte += $value;

                $value = isset($data['procedureMiseEnLigne']['services'][$typeProcedure]) ? $data['procedureMiseEnLigne']['services'][$typeProcedure] : '0';
                $worksheet->write($row, $colone++, number_format($value, '0', ',', ''), $corpsTableau);
                $totalProcMisEnLigneServicesAdapte += $value;
                $soustotalConsAdapte += $value;

                $worksheet->write($row, $colone++, number_format($soustotalConsAdapte, '0', ',', ''), $formatStantardGrasAlignRight);

                $value = isset($data['retraits']['travaux'][$typeProcedure]) ? $data['retraits']['travaux'][$typeProcedure] : '0';
                $worksheet->write($row, $colone++, number_format($value, '0', ',', ''), $corpsTableau);
                $totalRetraitTravauxAdapte += $value;
                $soustotalRetraitsAdapte = $value;

                $value = isset($data['retraits']['fournitures'][$typeProcedure]) ? $data['retraits']['fournitures'][$typeProcedure] : '0';
                $worksheet->write($row, $colone++, number_format($value, '0', ',', ''), $corpsTableau);
                $totalRetraitFournitureAdapte += $value;
                $soustotalRetraitsAdapte += $value;

                $value = isset($data['retraits']['services'][$typeProcedure]) ? $data['retraits']['services'][$typeProcedure] : '0';
                $worksheet->write($row, $colone++, number_format($value, '0', ',', ''), $corpsTableau);
                $totalRetraitServiceAdapte += $value;
                $soustotalRetraitsAdapte += $value;

                $worksheet->write($row, $colone++, number_format($soustotalRetraitsAdapte, '0', ',', ''), $formatStantardGrasAlignRight);

                $value = isset($data['offres']['travaux'][$typeProcedure]) ? $data['offres']['travaux'][$typeProcedure] : '0';
                $worksheet->write($row, $colone++, number_format($value, '0', ',', ''), $corpsTableau);
                $totalOffreTravauxAdapte += $value;
                $soustotalOffresAdapte = $value;

                $value = isset($data['offres']['fournitures'][$typeProcedure]) ? $data['offres']['fournitures'][$typeProcedure] : '0';
                $worksheet->write($row, $colone++, number_format($value, '0', ',', ''), $corpsTableau);
                $totalOffreFournitureAdapte += $value;
                $soustotalOffresAdapte += $value;

                $value = isset($data['offres']['services'][$typeProcedure]) ? $data['offres']['services'][$typeProcedure] : '0';
                $worksheet->write($row, $colone++, number_format($value, '0', ',', ''), $corpsTableau);
                $totalOffreServiceAdapte += $value;
                $soustotalOffresAdapte += $value;

                $worksheet->write($row, $colone++, number_format($soustotalOffresAdapte, '0', ',', ''), $formatStantardGrasAlignRight);

                ++$row;
            }
        }

        $worksheet->write($row, 0, Prado::localize('TEXT_TOTAL_PROCEDURES_ADAPTEES'), $formatStantardGrasBorder);

        $worksheet->write($row, 1, number_format($totalProcMisEnLigneTravauxAdapte, '0', ',', ''), $formatStantardGrasAlignRight);
        $worksheet->write($row, 2, number_format($totalProcMisEnLigneFournituresAdapte, '0', ',', ''), $formatStantardGrasAlignRight);
        $worksheet->write($row, 3, number_format($totalProcMisEnLigneServicesAdapte, '0', ',', ''), $formatStantardGrasAlignRight);
        $worksheet->write($row, 4, number_format((($totalProcMisEnLigneTravauxAdapte + $totalProcMisEnLigneServicesAdapte + $totalProcMisEnLigneFournituresAdapte)), '0', ',', ''), $formatStantardGrasAlignRight);

        $worksheet->write($row, 5, number_format($totalRetraitTravauxAdapte, '0', ',', ''), $formatStantardGrasAlignRight);
        $worksheet->write($row, 6, number_format($totalRetraitFournitureAdapte, '0', ',', ''), $formatStantardGrasAlignRight);
        $worksheet->write($row, 7, number_format($totalRetraitServiceAdapte, '0', ',', ''), $formatStantardGrasAlignRight);
        $worksheet->write($row, 8, number_format(($totalRetraitTravauxAdapte + $totalRetraitFournitureAdapte + $totalRetraitServiceAdapte), '0', ',', ''), $formatStantardGrasAlignRight);

        $worksheet->write($row, 9, number_format($totalOffreTravauxAdapte, '0', ',', ''), $formatStantardGrasAlignRight);
        $worksheet->write($row, 10, number_format($totalOffreFournitureAdapte, '0', ',', ''), $formatStantardGrasAlignRight);
        $worksheet->write($row, 11, number_format($totalOffreServiceAdapte, '0', ',', ''), $formatStantardGrasAlignRight);
        $worksheet->write($row, 12, number_format($totalOffreTravauxAdapte + $totalOffreFournitureAdapte + $totalOffreServiceAdapte, '0', ',', ''), $formatStantardGrasAlignRight);

        ++$row;
        ++$row;
        $worksheet->write($row, 0, Prado::localize('TEXT_GRAND_TOTAL'), $formatStantardGrasBorder);

        $worksheet->write($row, 1, number_format(($totalProcMisEnLigneTravauxFormalise + $totalProcMisEnLigneTravauxAdapte), '0', ',', ''), $formatStantardGrasAlignRight);
        $worksheet->write($row, 2, number_format(($totalProcMisEnLigneFournituresFormalise + $totalProcMisEnLigneFournituresAdapte), '0', ',', ''), $formatStantardGrasAlignRight);
        $worksheet->write($row, 3, number_format(($totalProcMisEnLigneServicesFormalise + $totalProcMisEnLigneServicesAdapte), '0', ',', ''), $formatStantardGrasAlignRight);
        $worksheet->write($row, 4, number_format(($totalProcMisEnLigneTravauxFormalise + $totalProcMisEnLigneTravauxAdapte)
                                                + ($totalProcMisEnLigneFournituresFormalise + $totalProcMisEnLigneFournituresAdapte)
                                                + ($totalProcMisEnLigneServicesFormalise + $totalProcMisEnLigneServicesAdapte), '0', ',', ''), $formatStantardGrasAlignRight);

        $worksheet->write($row, 5, number_format(($totalRetraitTravauxFormalise + $totalRetraitTravauxAdapte), '0', ',', ''), $formatStantardGrasAlignRight);
        $worksheet->write($row, 6, number_format(($totalRetraitFournitureFormalise + $totalRetraitFournitureAdapte), '0', ',', ''), $formatStantardGrasAlignRight);
        $worksheet->write($row, 7, number_format(($totalRetraitServiceFormalise + $totalRetraitServiceAdapte), '0', ',', ''), $formatStantardGrasAlignRight);
        $worksheet->write($row, 8, number_format((($totalRetraitTravauxFormalise + $totalRetraitTravauxAdapte)
                                                + ($totalRetraitFournitureFormalise + $totalRetraitFournitureAdapte)
                                                + ($totalRetraitServiceFormalise + $totalRetraitServiceAdapte)), '0', ',', ''), $formatStantardGrasAlignRight);

        $worksheet->write($row, 9, number_format(($totalOffreTravauxFormalise + $totalOffreTravauxAdapte), '0', ',', ''), $formatStantardGrasAlignRight);
        $worksheet->write($row, 10, number_format(($totalOffreFournitureFormalise + $totalOffreFournitureAdapte), '0', ',', ''), $formatStantardGrasAlignRight);
        $worksheet->write($row, 11, number_format(($totalOffreServiceFormalise + $totalOffreServiceAdapte), '0', ',', ''), $formatStantardGrasAlignRight);
        $worksheet->write($row, 12, number_format((($totalOffreTravauxFormalise + $totalOffreTravauxAdapte)
                                                 + ($totalOffreFournitureFormalise + $totalOffreFournitureAdapte)
                                                 + ($totalOffreServiceFormalise + $totalOffreServiceAdapte)), '0', ',', ''), $formatStantardGrasAlignRight);

        $workbook->close();
        $workbook->send($nomFichier.'.xls');
        exit;
    }

    public function genererExcelProceduresAdaptes($acronymeOrganisme, $dataStatistique, $objetRecherche, $valeurCumule, $nomEntite)
    {
        (new Atexo_Config())->includePhpLibPath();
        require_once 'Spreadsheet/Excel/Writer.php';
        $workbook = new Spreadsheet_Excel_Writer();

        $workbook->setCustomColor(11, 51, 204, 255);
        $workbook->setCustomColor(12, 221, 221, 221);
        //Pour passer outre la limite des 255 caracteres
        $workbook->setVersion(8);

        //Style des entetes
        $formatStantardGras = &$workbook->addFormat();
        $formatStantardGras->setSize(10);
        $formatStantardGras->setAlign('left');
        $formatStantardGras->setBold(1);

        $formatStantard = &$workbook->addFormat();
        $formatStantard->setSize(10);
        $formatStantard->setAlign('left');
        $formatStantard->setBold(0);

        $formatEnteteTableau = &$workbook->addFormat();
        $formatEnteteTableau->setFgColor(11);
        $formatEnteteTableau->setSize(10);
        $formatEnteteTableau->setAlign('left');
        $formatEnteteTableau->setAlign('top');
        $formatEnteteTableau->setBorder(1);
        $formatEnteteTableau->setBold(1);
        $formatEnteteTableau->setTextWrap();

        $formatEnteteTableauCenter = &$workbook->addFormat();
        $formatEnteteTableauCenter->setFgColor(11);
        $formatEnteteTableauCenter->setSize(10);
        $formatEnteteTableauCenter->setAlign('center');
        $formatEnteteTableauCenter->setAlign('top');
        $formatEnteteTableauCenter->setBorder(1);
        $formatEnteteTableauCenter->setBold(1);
        $formatEnteteTableauCenter->setTextWrap();

        $corpsTableau = &$workbook->addFormat();
        $corpsTableau->setSize(10);
        $corpsTableau->setAlign('top');
        $corpsTableau->setBold(0);
        $corpsTableau->setTextWrap();
        $corpsTableau->setBorder(1);

        $corpsTableauBorderBasEtGauche = &$workbook->addFormat();
        $corpsTableauBorderBasEtGauche->setSize(10);
        $corpsTableauBorderBasEtGauche->setAlign('top');
        $corpsTableauBorderBasEtGauche->setBold(0);
        $corpsTableauBorderBasEtGauche->setTextWrap();
        $corpsTableauBorderBasEtGauche->setLeft(1);
        $corpsTableauBorderBasEtGauche->setBottom(1);

        $corpsTableauBorderGaucheDroite = &$workbook->addFormat();
        $corpsTableauBorderGaucheDroite->setSize(10);
        $corpsTableauBorderGaucheDroite->setAlign('top');
        $corpsTableauBorderGaucheDroite->setBold(0);
        $corpsTableauBorderGaucheDroite->setTextWrap();
        $corpsTableauBorderGaucheDroite->setLeft(1);
        $corpsTableauBorderGaucheDroite->setRight(1);

        $corpsTableauBorderGauche = &$workbook->addFormat();
        $corpsTableauBorderGauche->setSize(10);
        $corpsTableauBorderGauche->setAlign('top');
        $corpsTableauBorderGauche->setBold(0);
        $corpsTableauBorderGauche->setTextWrap();
        $corpsTableauBorderGauche->setLeft(1);

        $corpsTableauBorderGaucheDroiteHaut = &$workbook->addFormat();
        $corpsTableauBorderGaucheDroiteHaut->setSize(10);
        $corpsTableauBorderGaucheDroiteHaut->setAlign('top');
        $corpsTableauBorderGaucheDroiteHaut->setBold(0);
        $corpsTableauBorderGaucheDroiteHaut->setTextWrap();
        $corpsTableauBorderGaucheDroiteHaut->setLeft(1);
        $corpsTableauBorderGaucheDroiteHaut->setRight(1);
        $corpsTableauBorderGaucheDroiteHaut->setTop(1);

        $corpsTableauBorderBas = &$workbook->addFormat();
        $corpsTableauBorderBas->setSize(10);
        $corpsTableauBorderBas->setAlign('top');
        $corpsTableauBorderBas->setBold(0);
        $corpsTableauBorderBas->setTextWrap();
        $corpsTableauBorderBas->setBottom(1);

        $marginLeft = &$workbook->addFormat();
        $marginLeft->setLeft(1);

        $marginRight = &$workbook->addFormat();
        $marginRight->setRight(1);

        $worksheet = &$workbook->addWorkSheet(Atexo_Util::utf8ToIso(Prado::localize('PROCEDURE_ADAPTEES')));
        $worksheet->setInputEncoding(Atexo_Config::getParameter('HTTP_ENCODING'));

        $worksheet->setLandscape();
        $worksheet->hideGridlines();

        $nomFichier = Prado::localize('TEXT_EXPORTS_STATISTIQUES_').date('Ymd');
        $worksheet->setHeader('&"Arial"&8'.$nomFichier.' - '.Prado::localize('PROCEDURE_ADAPTEE'), '0.51');
        $worksheet->setFooter(self::getFooterImprimeLe(), '0.51');

        $worksheet->setMarginLeft('0.78');
        $worksheet->setMarginRight('0.78');
        $worksheet->setMarginTop('1.01');
        $worksheet->setMarginBottom('1.01');

        $worksheet->setColumn(0, 0, '24.71');
        $worksheet->setColumn(1, 1, '10.71');
        $worksheet->setColumn(2, 2, '22.57');
        $worksheet->setColumn(3, 3, '33.29');
        $worksheet->setColumn(4, 4, '26');
        $worksheet->setColumn(5, 5, '4.86');

        $worksheet->write(0, 0, Prado::localize('TEXT_SUIVI_MARCHES_PROCEDURE_ADAPTEE'), $formatStantardGras);

        $worksheet->write(2, 0, $objetRecherche, $formatStantard);
        if ($nomEntite) {
            $worksheet->write(3, 0, Prado::localize('TEXT_ENTITE_ACHAT').' : '.html_entity_decode($nomEntite, ENT_QUOTES).$valeurCumule, $formatStantard);
        }
        $worksheet->write(5, 2, Prado::localize('DEFINE_MARCHE_PROCEDURE_ADAPTE'), $formatEnteteTableauCenter);
        $worksheet->write(5, 3, '', $formatEnteteTableau);
        $worksheet->write(5, 4, '', $formatEnteteTableau);
        $worksheet->write(5, 5, '', $formatEnteteTableau);

        $worksheet->mergeCells(5, 2, 5, 5);

        $worksheet->write(6, 2, Prado::localize('DEFINE_TEXT_SANS_REPONSE_ELECTRONIQUE_AUTORISE'), $formatEnteteTableau);

        $worksheet->write(6, 3, Prado::localize('TEXT_AVEC_REPONSE_ELECTRONIQUE_SANS_SIGNATURE'), $formatEnteteTableau);

        $worksheet->write(6, 4, Prado::localize('TEXT_AVEC_REPONSE_ELECTRONIQUE_AVEC_SIGNATURE'), $formatEnteteTableau);

        $worksheet->write(6, 5, Prado::localize('TEXT_TOTAL'), $formatEnteteTableau);

        $row = 7;

        $worksheet->write($row, 0, Prado::localize('TEXT_PROCEDURES_MISES_LIGNE'), $corpsTableauBorderGaucheDroiteHaut);
        $worksheet->write($row, 1, Prado::localize('DEFINE_TRAVAUX'), $corpsTableau);
        $worksheet->write($row, 2, number_format($dataStatistique['procedureMiseEnLigne']['travaux']['sans_reponse'], '0', ',', ''), $corpsTableau);
        $worksheet->write($row, 3, number_format($dataStatistique['procedureMiseEnLigne']['travaux']['avec_reponses_sans_signature'], '0', ',', ''), $corpsTableau);
        $worksheet->write($row, 4, number_format($dataStatistique['procedureMiseEnLigne']['travaux']['avec_reponses_avec_signature'], '0', ',', ''), $corpsTableau);

        $total = $dataStatistique['procedureMiseEnLigne']['travaux']['sans_reponse']
               + $dataStatistique['procedureMiseEnLigne']['travaux']['avec_reponses_sans_signature']
               + $dataStatistique['procedureMiseEnLigne']['travaux']['avec_reponses_avec_signature'];
        $worksheet->write($row, 5, number_format($total, '0', ',', ''), $corpsTableau);
        ++$row;
        $worksheet->write($row, 0, '', $corpsTableauBorderGauche);
        $worksheet->write($row, 1, Prado::localize('DEFINE_FOURNITURES'), $corpsTableau);
        $worksheet->write($row, 2, number_format($dataStatistique['procedureMiseEnLigne']['fournitures']['sans_reponse'], '0', ',', ''), $corpsTableau);
        $worksheet->write($row, 3, number_format($dataStatistique['procedureMiseEnLigne']['fournitures']['avec_reponses_sans_signature'], '0', ',', ''), $corpsTableau);
        $worksheet->write($row, 4, number_format($dataStatistique['procedureMiseEnLigne']['fournitures']['avec_reponses_avec_signature'], '0', ',', ''), $corpsTableau);

        $total = $dataStatistique['procedureMiseEnLigne']['fournitures']['sans_reponse']
               + $dataStatistique['procedureMiseEnLigne']['fournitures']['avec_reponses_sans_signature']
               + $dataStatistique['procedureMiseEnLigne']['fournitures']['avec_reponses_avec_signature'];
        $worksheet->write($row, 5, number_format($total, '0', ',', ''), $corpsTableau);
        ++$row;
        $worksheet->write($row, 0, '', $corpsTableauBorderGauche);
        $worksheet->write($row, 1, Prado::localize('DEFINE_SERVICES'), $corpsTableau);
        $worksheet->write($row, 2, number_format($dataStatistique['procedureMiseEnLigne']['services']['sans_reponse'], '0', ',', ''), $corpsTableau);
        $worksheet->write($row, 3, number_format($dataStatistique['procedureMiseEnLigne']['services']['avec_reponses_sans_signature'], '0', ',', ''), $corpsTableau);
        $worksheet->write($row, 4, number_format($dataStatistique['procedureMiseEnLigne']['services']['avec_reponses_avec_signature'], '0', ',', ''), $corpsTableau);

        $total = $dataStatistique['procedureMiseEnLigne']['services']['sans_reponse']
               + $dataStatistique['procedureMiseEnLigne']['services']['avec_reponses_sans_signature']
               + $dataStatistique['procedureMiseEnLigne']['services']['avec_reponses_avec_signature'];
        $worksheet->write($row, 5, number_format($total, '0', ',', ''), $corpsTableau);
        ++$row;
        $worksheet->write($row, 0, '', $corpsTableauBorderBasEtGauche);
        $worksheet->write($row, 1, Prado::localize('TEXT_SOUS_TOTAL'), $corpsTableau);
        $worksheet->write($row, 2, number_format($dataStatistique['sousTotal']['procedureMiseEnLigne']['sans_reponse'], '0', ',', ''), $corpsTableau);
        $worksheet->write($row, 3, number_format($dataStatistique['sousTotal']['procedureMiseEnLigne']['avec_reponses_sans_signature'], '0', ',', ''), $corpsTableau);
        $worksheet->write($row, 4, number_format($dataStatistique['sousTotal']['procedureMiseEnLigne']['avec_reponses_avec_signature'], '0', ',', ''), $corpsTableau);

        $total = $dataStatistique['sousTotal']['procedureMiseEnLigne']['sans_reponse']
               + $dataStatistique['sousTotal']['procedureMiseEnLigne']['avec_reponses_sans_signature']
               + $dataStatistique['sousTotal']['procedureMiseEnLigne']['avec_reponses_avec_signature'];
        $worksheet->write($row, 5, number_format($total, '0', ',', ''), $corpsTableau);

        ++$row;
        $worksheet->write($row, 0, Prado::localize('RETRAITS'), $corpsTableauBorderGaucheDroite);
        $worksheet->write($row, 1, Prado::localize('DEFINE_TRAVAUX'), $corpsTableau);
        $worksheet->write($row, 2, number_format($dataStatistique['retraits']['travaux']['sans_reponse'], '0', ',', ''), $corpsTableau);
        $worksheet->write($row, 3, number_format($dataStatistique['retraits']['travaux']['avec_reponses_sans_signature'], '0', ',', ''), $corpsTableau);
        $worksheet->write($row, 4, number_format($dataStatistique['retraits']['travaux']['avec_reponses_avec_signature'], '0', ',', ''), $corpsTableau);

        $total = $dataStatistique['retraits']['travaux']['sans_reponse']
               + $dataStatistique['retraits']['travaux']['avec_reponses_sans_signature']
               + $dataStatistique['retraits']['travaux']['avec_reponses_avec_signature'];
        $worksheet->write($row, 5, number_format($total, '0', ',', ''), $corpsTableau);
        ++$row;

        $worksheet->write($row, 0, '', $corpsTableauBorderGauche);
        $worksheet->write($row, 1, Prado::localize('DEFINE_FOURNITURES'), $corpsTableau);
        $worksheet->write($row, 2, number_format($dataStatistique['retraits']['fournitures']['sans_reponse'], '0', ',', ''), $corpsTableau);
        $worksheet->write($row, 3, number_format($dataStatistique['retraits']['fournitures']['avec_reponses_sans_signature'], '0', ',', ''), $corpsTableau);
        $worksheet->write($row, 4, number_format($dataStatistique['retraits']['fournitures']['avec_reponses_avec_signature'], '0', ',', ''), $corpsTableau);

        $total = $dataStatistique['retraits']['fournitures']['sans_reponse']
               + $dataStatistique['retraits']['fournitures']['avec_reponses_sans_signature']
               + $dataStatistique['retraits']['fournitures']['avec_reponses_avec_signature'];
        $worksheet->write($row, 5, number_format($total, '0', ',', ''), $corpsTableau);
        ++$row;

        $worksheet->write($row, 0, '', $corpsTableauBorderGauche);
        $worksheet->write($row, 1, Prado::localize('DEFINE_SERVICES'), $corpsTableau);
        $worksheet->write($row, 2, number_format($dataStatistique['retraits']['services']['sans_reponse'], '0', ',', ''), $corpsTableau);
        $worksheet->write($row, 3, number_format($dataStatistique['retraits']['services']['avec_reponses_sans_signature'], '0', ',', ''), $corpsTableau);
        $worksheet->write($row, 4, number_format($dataStatistique['retraits']['services']['avec_reponses_avec_signature'], '0', ',', ''), $corpsTableau);

        $total = $dataStatistique['retraits']['services']['sans_reponse']
               + $dataStatistique['retraits']['services']['avec_reponses_sans_signature']
               + $dataStatistique['retraits']['services']['avec_reponses_avec_signature'];
        $worksheet->write($row, 5, number_format($total, '0', ',', ''), $corpsTableau);

        ++$row;
        $worksheet->write($row, 0, '', $corpsTableauBorderBasEtGauche);
        $worksheet->write($row, 1, Prado::localize('TEXT_SOUS_TOTAL'), $corpsTableau);
        $worksheet->write($row, 2, number_format($dataStatistique['sousTotal']['retraits']['sans_reponse'], '0', ',', ''), $corpsTableau);
        $worksheet->write($row, 3, number_format($dataStatistique['sousTotal']['retraits']['avec_reponses_sans_signature'], '0', ',', ''), $corpsTableau);
        $worksheet->write($row, 4, number_format($dataStatistique['sousTotal']['retraits']['avec_reponses_avec_signature'], '0', ',', ''), $corpsTableau);

        $total = $dataStatistique['sousTotal']['retraits']['sans_reponse']
               + $dataStatistique['sousTotal']['retraits']['avec_reponses_sans_signature']
               + $dataStatistique['sousTotal']['retraits']['avec_reponses_avec_signature'];
        $worksheet->write($row, 5, number_format($total, '0', ',', ''), $corpsTableau);

        ++$row;
        $worksheet->write($row, 0, Prado::localize('OFFRES'), $corpsTableauBorderGaucheDroite);
        $worksheet->write($row, 1, Prado::localize('DEFINE_TRAVAUX'), $corpsTableau);
        $worksheet->write($row, 2, number_format($dataStatistique['offres']['travaux']['sans_reponse'], '0', ',', ''), $corpsTableau);
        $worksheet->write($row, 3, number_format($dataStatistique['offres']['travaux']['avec_reponses_sans_signature'], '0', ',', ''), $corpsTableau);
        $worksheet->write($row, 4, number_format($dataStatistique['offres']['travaux']['avec_reponses_avec_signature'], '0', ',', ''), $corpsTableau);

        $total = $dataStatistique['offres']['travaux']['sans_reponse']
               + $dataStatistique['offres']['travaux']['avec_reponses_sans_signature']
               + $dataStatistique['offres']['travaux']['avec_reponses_avec_signature'];
        $worksheet->write($row, 5, number_format($total, '0', ',', ''), $corpsTableau);

        ++$row;
        $worksheet->write($row, 0, '', $corpsTableauBorderGauche);
        $worksheet->write($row, 1, Prado::localize('DEFINE_FOURNITURES'), $corpsTableau);
        $worksheet->write($row, 2, number_format($dataStatistique['offres']['fournitures']['sans_reponse'], '0', ',', ''), $corpsTableau);
        $worksheet->write($row, 3, number_format($dataStatistique['offres']['fournitures']['avec_reponses_sans_signature'], '0', ',', ''), $corpsTableau);
        $worksheet->write($row, 4, number_format($dataStatistique['offres']['fournitures']['avec_reponses_avec_signature'], '0', ',', ''), $corpsTableau);

        $total = $dataStatistique['offres']['fournitures']['sans_reponse']
               + $dataStatistique['offres']['fournitures']['avec_reponses_sans_signature']
               + $dataStatistique['offres']['fournitures']['avec_reponses_avec_signature'];
        $worksheet->write($row, 5, number_format($total, '0', ',', ''), $corpsTableau);
        ++$row;

        $worksheet->write($row, 0, '', $corpsTableauBorderGauche);
        $worksheet->write($row, 1, Prado::localize('DEFINE_SERVICES'), $corpsTableau);
        $worksheet->write($row, 2, number_format($dataStatistique['offres']['services']['sans_reponse'], '0', ',', ''), $corpsTableau);
        $worksheet->write($row, 3, number_format($dataStatistique['offres']['services']['avec_reponses_sans_signature'], '0', ',', ''), $corpsTableau);
        $worksheet->write($row, 4, number_format($dataStatistique['offres']['services']['avec_reponses_avec_signature'], '0', ',', ''), $corpsTableau);

        $total = $dataStatistique['offres']['services']['sans_reponse']
               + $dataStatistique['offres']['services']['avec_reponses_sans_signature']
               + $dataStatistique['offres']['services']['avec_reponses_avec_signature'];
        $worksheet->write($row, 5, number_format($total, '0', ',', ''), $corpsTableau);
        ++$row;
        $worksheet->write($row, 0, '', $corpsTableauBorderBasEtGauche);
        $worksheet->write($row, 1, Prado::localize('TEXT_SOUS_TOTAL'), $corpsTableau);
        $worksheet->write($row, 2, number_format($dataStatistique['sousTotal']['offres']['sans_reponse'], '0', ',', ''), $corpsTableau);
        $worksheet->write($row, 3, number_format($dataStatistique['sousTotal']['offres']['avec_reponses_sans_signature'], '0', ',', ''), $corpsTableau);
        $worksheet->write($row, 4, number_format($dataStatistique['sousTotal']['offres']['avec_reponses_avec_signature'], '0', ',', ''), $corpsTableau);

        $total = $dataStatistique['sousTotal']['offres']['sans_reponse']
               + $dataStatistique['sousTotal']['offres']['avec_reponses_sans_signature']
               + $dataStatistique['sousTotal']['offres']['avec_reponses_avec_signature'];
        $worksheet->write($row, 5, number_format($total, '0', ',', ''), $corpsTableau);

        $workbook->close();
        $workbook->send($nomFichier.'.xls');
        exit;
    }

    public function generateExcelHabilitation($acronymeOrganisme, $serviceId, $toFile = false, $inclureDesc = false)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));

        $agents = Atexo_Agent::retriveAgentsByServiceAndOrg($serviceId, $acronymeOrganisme, $inclureDesc);

        $organisme = Atexo_Organismes::retrieveOrganismeByAcronyme($acronymeOrganisme);
        $nomOrganisme = '';
        if ($organisme instanceof CommonOrganisme) {
            $nomOrganisme = $organisme->getDenominationOrg();
        }

        $nomService = '';
        if ('0' === $serviceId || 0 === $serviceId) {
            $nomService = $nomOrganisme;
        } else {
            $service = Atexo_EntityPurchase::retrieveEntityById($serviceId, $acronymeOrganisme);
            if ($service instanceof CommonService) {
                $nomService = $service->getLibelle();
            }
        }
        (new Atexo_Config())->includePhpLibPath();
        require_once 'Spreadsheet/Excel/Writer.php';
        if ($toFile) {
            $tmpFileName = Atexo_Config::getParameter('COMMON_TMP').Prado::localize('TEXT_REGISTRE_HABILITATION').session_id().'_'.time().'.xls';
            $workbook = new Spreadsheet_Excel_Writer($tmpFileName);
        } else {
            $workbook = new Spreadsheet_Excel_Writer();
        }

        $workbook->setCustomColor(11, 255, 255, 150);
        $workbook->setCustomColor(12, 192, 192, 192);
        $workbook->setCustomColor(13, 0, 0, 255);
        //Pour passer outre la limite des 255 caracteres
        $workbook->setVersion(8);

        //Style des entetes
        $stylePolice10GrasBleu = &$workbook->addFormat();
        $stylePolice10GrasBleu->setColor(13);
        $stylePolice10GrasBleu->setSize(10);
        $stylePolice10GrasBleu->setAlign('left');
        $stylePolice10GrasBleu->setBold(1);

        $stylePolice10BleuSimple = &$workbook->addFormat();
        $stylePolice10BleuSimple->setColor(13);
        $stylePolice10BleuSimple->setSize(10);
        $stylePolice10BleuSimple->setAlign('left');

        $styleSimplePolice10 = &$workbook->addFormat();
        $styleSimplePolice10->setColor('black');
        $styleSimplePolice10->setSize(10);
        $styleSimplePolice10->setAlign('left');

        $stylePolice8GrasBleu = &$workbook->addFormat();
        $stylePolice8GrasBleu->setColor(13);
        $stylePolice8GrasBleu->setSize(8);
        $stylePolice8GrasBleu->setAlign('left');
        $stylePolice8GrasBleu->setBold(1);

        $styleSimplePolice8 = &$workbook->addFormat();
        $styleSimplePolice8->setColor('black');
        $styleSimplePolice8->setSize(8);
        $styleSimplePolice8->setAlign('left');

        $styleSimplePolice8FondJaune = &$workbook->addFormat();
        $styleSimplePolice8FondJaune->setColor('black');
        $styleSimplePolice8FondJaune->setSize(8);
        $styleSimplePolice8FondJaune->setAlign('left');
        $styleSimplePolice8FondJaune->setFgColor(11);

        $styleSimplePolice8FondGris = &$workbook->addFormat();
        $styleSimplePolice8FondGris->setColor('black');
        $styleSimplePolice8FondGris->setSize(8);
        $styleSimplePolice8FondGris->setAlign('left');
        $styleSimplePolice8FondGris->setFgColor(12);

        $worksheet = &$workbook->addWorkSheet(Atexo_Util::utf8ToIso(Prado::localize('DEFINE_HABILITATIONS')));
        if ('iso-8859-1' != Atexo_Config::getParameter('HTTP_ENCODING')) {
            $worksheet->setInputEncoding(Atexo_Config::getParameter('HTTP_ENCODING'));
        }

        $worksheet->hideGridlines();
        $nomFichier = Prado::localize('TEXT_REGISTRE_HABILITATIONS_XLS').date('Y_m_d').'.xls';
        $worksheet->setHeader('&"Arial"&8'.$nomFichier.' - '.Prado::localize('DEFINE_HABILITATIONS'), '0.19');
        $worksheet->setFooter(self::getFooterImprimeLe(), '0.19');

        $worksheet->setMarginLeft('0.19');
        $worksheet->setMarginRight('0.19');
        $worksheet->setMarginTop('0.39');
        $worksheet->setMarginBottom('0.39');

        $worksheet->setColumn(0, 0, '8.14');
        $worksheet->setColumn(1, 1, '5.43');
        $worksheet->setColumn(2, 2, '36.86');

        for ($i = 4; $i < 72; ++$i) {
            $worksheet->setRow($i, '11');
        }

        $worksheet->write(0, 0, Prado::localize('TEXT_REGISTRE_AGENTS_HABILITATIONS'), $stylePolice10GrasBleu);

        $worksheet->write(1, 0, Prado::localize('TEXT_MINISTERE').' :', $stylePolice10BleuSimple);
        $worksheet->write(1, 2, $nomOrganisme, $styleSimplePolice10);

        $worksheet->write(2, 0, Prado::localize('TEXT_ENTITE_ACHAT').' :', $stylePolice10BleuSimple);
        $worksheet->write(2, 2, $nomService, $styleSimplePolice10);

        if ($agents && is_array($agents)) {
            $ligne = 4;
            //Initialisation du fichier excel
            $worksheet->write($ligne++, 2, Prado::localize('NOM'), $stylePolice8GrasBleu);
            $worksheet->write($ligne++, 2, Prado::localize('PRENOM'), $stylePolice8GrasBleu);
            $worksheet->write($ligne++, 2, Prado::localize('TEXT_IDENTIFIANT_SANS_POINTS'), $stylePolice8GrasBleu);
            $worksheet->write($ligne++, 2, Prado::localize('DEFINE_EMAIL_EA_PMI'), $stylePolice8GrasBleu);

            $worksheet->write($ligne++, 0, Prado::localize('TEXT_LISTE_HABILITATION_LEGENDE'), $stylePolice8GrasBleu);

            $worksheet->write($ligne, 0, Prado::localize('TEXT_GESTION_CONSULTATIONS'), $styleSimplePolice8FondJaune);
            $worksheet->write($ligne, 1, '', $styleSimplePolice8FondJaune);
            $worksheet->write($ligne++, 2, '', $styleSimplePolice8FondJaune);

            $worksheet->write($ligne, 1, Prado::localize('DEFINE_CREER_CONSULTATION'), $styleSimplePolice8FondGris);
            $worksheet->write($ligne++, 2, '', $styleSimplePolice8FondGris);
            if (Atexo_Module::isEnabled('ProcedureAdaptee')) {
                $worksheet->write($ligne++, 2, Prado::localize('TEXT_MAPA_INFERIEUR_90000'), $styleSimplePolice8);
                $worksheet->write($ligne++, 2, Prado::localize('TEXT_MAPA_SUPERIEUR_90000'), $styleSimplePolice8);
            }
            $worksheet->write($ligne++, 2, Prado::localize('TEXT_PROCEDURES_FORMALISEES'), $styleSimplePolice8);
            $worksheet->write($ligne++, 2, Prado::localize('TEXT_CREER_CONSULTATION_TRANSVERSE'), $styleSimplePolice8);

            $worksheet->write($ligne++, 1, Prado::localize('DEFINE_CREER_SUITE'), $styleSimplePolice8);
            $worksheet->write($ligne++, 1, Prado::localize('TEXT_GERER_PUBLICITE'), $styleSimplePolice8);
            $worksheet->write($ligne++, 1, Prado::localize('DEFINE_ENVOYER_PUBLICITE'), $styleSimplePolice8);
            $worksheet->write($ligne++, 1, Prado::localize('TEXT_MODIFIER_CONSULTATION_AVANT_VALIDATION'), $styleSimplePolice8);

            $worksheet->write($ligne++, 1, Prado::localize('TEXT_VALIDATION_SIMPLE'), $styleSimplePolice8);
            $worksheet->write($ligne++, 1, Prado::localize('TEXT_VALIDATION_INTERMEDIAIRE'), $styleSimplePolice8);
            $worksheet->write($ligne++, 1, Prado::localize('TEXT_VALIDATION_FINALE'), $styleSimplePolice8);

            $worksheet->write($ligne++, 1, '', $styleSimplePolice8);
            //$worksheet->write($ligne++, 1, Prado::localize('TEXT_MODIFIER_CONSULTATION_APRES_VALIDATION'), $styleSimplePolice8);
            if (Atexo_Module::isEnabled('ProcedureAdaptee')) {
                $worksheet->write($ligne++, 1, Prado::localize('TEXT_MODIFIER_CONSULTATION_MAPA_INFERIEUR_MONTANT_APRES_VALIDATION'), $styleSimplePolice8);
                $worksheet->write($ligne++, 1, Prado::localize('TEXT_MODIFIER_CONSULTATION_MAPA_SUPERIEUR_MONTANT_APRES_VALIDATION'), $styleSimplePolice8);
            }
            $worksheet->write($ligne++, 1, Prado::localize('TEXT_MODIFIER_CONSULTATION_PROCEDURES_FORMALISEES_APRES_VALIDATION'), $styleSimplePolice8);
            $worksheet->write($ligne++, 1, Prado::localize('TEXT_GERER_REGISTRE_RETRAITS'), $styleSimplePolice8);
            $worksheet->write($ligne++, 1, Prado::localize('TEXT_GERER_REGISTRE_QUESTIONS'), $styleSimplePolice8);
            $worksheet->write($ligne++, 1, Prado::localize('TEXT_GERER_REGISTRE_DEPOTS'), $styleSimplePolice8);
            $worksheet->write($ligne++, 1, Prado::localize('TEXT_SUIVI_SEUL_REGISTRE_RETRAITS'), $styleSimplePolice8);
            $worksheet->write($ligne++, 1, Prado::localize('TEXT_SUIVI_SEUL_REGISTRE_QUESTIONS'), $styleSimplePolice8);
            $worksheet->write($ligne++, 1, Prado::localize('TEXT_SUIVI_SEUL_REGISTRE_DEPOTS'), $styleSimplePolice8);

            $worksheet->write($ligne++, 1, '', $styleSimplePolice8);

            $worksheet->write($ligne++, 1, Prado::localize('TEXT_ACCES_REPONSES'), $styleSimplePolice8);
            $worksheet->write($ligne++, 1, Prado::localize('TEXT_TELECHARGEMENT_GROUPE_ANTICIPE_PLIS_CHIFFRES'), $styleSimplePolice8);
            $worksheet->write($ligne++, 1, Prado::localize('TEXT_TELECHARGEMENT_UNITAIRES_PLIS_CHIFFRES'), $styleSimplePolice8);
            $worksheet->write($ligne++, 1, Prado::localize('TEXT_OUVERTURE_CONDIDATURE_LIGNES'), $styleSimplePolice8);
            $worksheet->write($ligne++, 1, Prado::localize('TEXT_OUVERTURE_CONDIDATURE_DISTANCE'), $styleSimplePolice8);
            $worksheet->write($ligne++, 1, Prado::localize('TEXT_OUVERTURE_CANDIDATURE_HORS_LIGNE'), $styleSimplePolice8);
            $worksheet->write($ligne++, 1, Prado::localize('TEXT_ACCES_REPONSES'), $styleSimplePolice8);
            $worksheet->write($ligne++, 1, Prado::localize('TEXT_OUVERTURE_OFFRES_LIGNE'), $styleSimplePolice8);
            $worksheet->write($ligne++, 1, Prado::localize('TEXT_OUVERTURE_OFFRES_DISTANCE'), $styleSimplePolice8);
            $worksheet->write($ligne++, 1, Prado::localize('TEXT_OUVERTURE_OFFRES_HORS_LIGNE'), $styleSimplePolice8);
            if (Atexo_Module::isEnabled('EnveloppeOffreTechnique')) {
                $worksheet->write($ligne++, 1, Prado::localize('TEXT_OUVERTURE_OFFRES_TECHNIQUE_EN_LIGNE'), $styleSimplePolice8);
                $worksheet->write($ligne++, 1, Prado::localize('TEXT_OUVERTURE_OFFRES_TECHNIQUE_A_DISTANCE'), $styleSimplePolice8);
                $worksheet->write($ligne++, 1, Prado::localize('TEXT_OUVERTURE_OFFRES_TECHNIQUE_HORS_LIGNE'), $styleSimplePolice8);
            }
            if (Atexo_Module::isEnabled('EnveloppeAnonymat')) {
                $worksheet->write($ligne++, 1, Prado::localize('TEXT_OUVRIR_ENVELOPPES_ANONYMES_LIGNE'), $styleSimplePolice8);
                $worksheet->write($ligne++, 1, Prado::localize('TEXT_OUVRIR_ENVELOPPES_ANONYMES_DSITANCE'), $styleSimplePolice8);
                $worksheet->write($ligne++, 1, Prado::localize('TEXT_OUVRIR_ENVELOPPES_ANONYMES_HORS_LIGNE'), $styleSimplePolice8);
            }
            $worksheet->write($ligne++, 1, Prado::localize('REFUSER_ENVELOPPE'), $styleSimplePolice8);
            $worksheet->write($ligne++, 1, Prado::localize('RESTAURER_ENVELOPPE'), $styleSimplePolice8);
            $worksheet->write($ligne++, 1, Prado::localize('GERER_ADMISSIBILITE'), $styleSimplePolice8);
            $worksheet->write($ligne++, 1, Prado::localize('IMPORTER_ENVELOPPE'), $styleSimplePolice8);
            $worksheet->write($ligne++, 1, Prado::localize('TEXT_ENVOYER_MESSAGE'), $styleSimplePolice8);
            $worksheet->write($ligne++, 1, Prado::localize('TEXT_SUIVI_MESSAGES'), $styleSimplePolice8);
            $worksheet->write($ligne++, 1, Prado::localize('DEFINE_ACCES_HISTORIQUE_ENTREPRISES'), $styleSimplePolice8);
            $worksheet->write($ligne++, 1, '', $styleSimplePolice8);
            if (Atexo_Module::isEnabled('EncheresEntreprise')) {
                $worksheet->write($ligne++, 1, Prado::localize('TEXT_DEFINIR_ENCHERE'), $styleSimplePolice8);
                $worksheet->write($ligne++, 1, Prado::localize('TEXT_SUIVRE_ENCHERE'), $styleSimplePolice8);
            }

            $worksheet->write($ligne++, 1, '', $styleSimplePolice8);

            if (Atexo_Module::isEnabled('InterfaceModuleSub')) {
                $worksheet->write($ligne++, 1, Prado::localize('GERER_REOUVERTURES_MODIFICATION'), $styleSimplePolice8);
            }
            $worksheet->write($ligne++, 1, Prado::localize('TEXT_RENSEIGNER_DECISION'), $styleSimplePolice8);
            $worksheet->write($ligne++, 1, Prado::localize('DEFINE_SUIVI_SEUL_LA_DECISION'), $styleSimplePolice8);
            $worksheet->write($ligne++, 1, Prado::localize('DEFINE_INVITE_PERMANENT'), $styleSimplePolice8);
            if (Atexo_Module::isEnabled('ResultatAnalyseAvantDecision')) {
                $worksheet->write($ligne++, 1, Prado::localize('RESULTAT_ANALYSE'), $styleSimplePolice8);
            }
            if (Atexo_Module::isEnabled('AnnulerConsultation')) {
                $worksheet->write($ligne++, 1, Prado::localize('ANNULER_CONSULTATION'), $styleSimplePolice8);
            }
            $worksheet->write($ligne++, 1, '', $styleSimplePolice8);

            if (Atexo_Module::isEnabled('InterfaceModuleRsem')) {
                $worksheet->write($ligne, 0, Prado::localize('TEXTE_REDACTION_PIECES_MARCHE'), $styleSimplePolice8FondJaune);
                $worksheet->write($ligne, 1, '', $styleSimplePolice8FondJaune);
                $worksheet->write($ligne, 2, '', $styleSimplePolice8FondJaune);
                $worksheet->write($ligne, 3, '', $styleSimplePolice8FondJaune);
                $worksheet->write($ligne, 4, '', $styleSimplePolice8FondJaune);
                $worksheet->write($ligne++, 5, '', $styleSimplePolice8FondJaune);
                $worksheet->write($ligne++, 1, Prado::localize('TEXT_REPRENDRE_INTEGRALEMENT_ARTICLE'), $styleSimplePolice8);
                $worksheet->write($ligne++, 1, Prado::localize('TEXT_REDACTION_PIECES'), $styleSimplePolice8);
                $worksheet->write($ligne++, 1, Prado::localize('TEXT_VALIDER_PIECES'), $styleSimplePolice8);
                $worksheet->write($ligne++, 1, Prado::localize('TEXT_ADMINISTER_CLAUSES_ENTITE_ACHAT'), $styleSimplePolice8);
                $worksheet->write($ligne++, 1, Prado::localize('TEXT_GENERER_PIECES_FORMAT_ODT'), $styleSimplePolice8);
                $worksheet->write($ligne++, 1, Prado::localize('ADMINISTRER_LES_GABARITS_PLATEFORME'), $styleSimplePolice8);
                $worksheet->write($ligne++, 1, Prado::localize('ADMINISTRER_LES_GABARITS_ENTITE_PUBLIQUE'), $styleSimplePolice8);
                $worksheet->write($ligne++, 1, Prado::localize('ADMINISTRER_LES_GABARITS_ENTITE_ACHAT'), $styleSimplePolice8);
                $worksheet->write($ligne++, 1, Prado::localize('ADMINISTRER_LES_GABARITS_AGENT'), $styleSimplePolice8);

                $worksheet->write($ligne++, 1, '', $styleSimplePolice8);
            }
            $worksheet->write($ligne, 0, Prado::localize('GESTION_BASE_DONNEES_FOURNISSEURS'), $styleSimplePolice8FondJaune);
            $worksheet->write($ligne, 1, '', $styleSimplePolice8FondJaune);
            $worksheet->write($ligne, 2, '', $styleSimplePolice8FondJaune);
            $worksheet->write($ligne++, 3, '', $styleSimplePolice8FondJaune);

            $worksheet->write($ligne++, 1, Prado::localize('TEXT_VISUALISER_ENTREPRISES'), $styleSimplePolice8);
            if (Atexo_Module::isEnabled('GestionEntrepriseParAgent')) {
                $worksheet->write($ligne++, 1, Prado::localize('GERER_LES_FOURNISSEURS'), $styleSimplePolice8);
            }
            if (Atexo_Module::isEnabled('AnnuaireAcheteursPublics')) {
                $worksheet->write($ligne++, 1, Prado::localize('ANNUAIRE_ACHETEUR_PUBLIC'), $styleSimplePolice8);
            }
            $worksheet->write($ligne++, 1, '', $styleSimplePolice8);

            $worksheet->write($ligne, 0, Prado::localize('TEXT_GESTION_AUTRES_ANNONCES'), $styleSimplePolice8FondJaune);
            $worksheet->write($ligne, 1, '', $styleSimplePolice8FondJaune);
            $worksheet->write($ligne++, 2, '', $styleSimplePolice8FondJaune);

            $worksheet->write($ligne++, 1, Prado::localize('TEXT_CREER_ANNONCE_INFORMATION'), $styleSimplePolice8);
            $worksheet->write($ligne++, 1, Prado::localize('TEXT_CREER_ANNONCE_ATTRIBUTION'), $styleSimplePolice8);
            if (Atexo_Module::isEnabled('AutreAnnonceExtraitPv')) {
                $worksheet->write($ligne++, 1, Prado::localize('TEXT_CREER_ANNONCE_EXTRAIT_PV'), $styleSimplePolice8);
            }
            if (Atexo_Module::isEnabled('AutreAnnonceRapportAchevement')) {
                $worksheet->write($ligne++, 1, Prado::localize('TEXT_CREER_ANNONCE_RAPPORT_ACHEVEMENT'), $styleSimplePolice8);
            }
            if (Atexo_Module::isEnabled('AutreAnnonceProgrammePrevisionnel')) {
                $worksheet->write($ligne++, 1, Prado::localize('TEXT_CREER_ANNONCE_PROGRAMME_PREVISIONNEL'), $styleSimplePolice8);
            }

            $worksheet->write($ligne++, 1, '', $styleSimplePolice8);

            $worksheet->write($ligne, 0, Prado::localize('TEXT_ADMINISTRATION_METIER_EA'), $styleSimplePolice8FondJaune);
            $worksheet->write($ligne, 1, '', $styleSimplePolice8FondJaune);
            $worksheet->write($ligne++, 2, '', $styleSimplePolice8FondJaune);

            $worksheet->write($ligne++, 1, Prado::localize('TEXT_VISUALISER_ENTREPRISES'), $styleSimplePolice8);
            if (Atexo_Module::isEnabled('GestionEntrepriseParAgent')) {
                $worksheet->write($ligne++, 1, Prado::localize('GERER_LES_FOURNISSEURS'), $styleSimplePolice8);
            }
            $worksheet->write($ligne++, 1, Prado::localize('TEXT_SUPPRIMER_PLIS_REFUSES'), $styleSimplePolice8);
            if (Atexo_Module::isEnabled('Article133GenerationPf')) {
                $worksheet->write($ligne, 1, Prado::localize('TEXT_LISTE_MARCHES_133'), $styleSimplePolice8FondGris);
                $worksheet->write($ligne++, 2, '', $styleSimplePolice8FondGris);
                $worksheet->write($ligne++, 2, Prado::localize('TEXT_PUBLICATION'), $styleSimplePolice8);
            }

            if (Atexo_Module::isEnabled('Article133GenerationPf') && Atexo_Module::isEnabled('ExportMarchesNotifies', Atexo_CurrentUser::getCurrentOrganism())) {
                $worksheet->write($ligne++, 2, Prado::localize('LISTE_MARCHES_NOTIFIES'), $styleSimplePolice8);
            }
            $worksheet->write($ligne++, 1, Prado::localize('TEXT_ACCEDER_STAT_METIER'), $styleSimplePolice8);
            if (Atexo_Module::isEnabled('InterfaceChorusPmi')) {
                $worksheet->write($ligne++, 1, Prado::localize('DEFINE_SUIVI_FLUX_CHORUS_TRANSVERSAUX'), $styleSimplePolice8);
            }

            $worksheet->write($ligne++, 1, '', $styleSimplePolice8);

            $worksheet->write($ligne, 0, Prado::localize('TEXT_ARCHIVES'), $styleSimplePolice8FondJaune);
            $worksheet->write($ligne, 1, '', $styleSimplePolice8FondJaune);
            $worksheet->write($ligne++, 2, '', $styleSimplePolice8FondJaune);

            $worksheet->write($ligne++, 1, Prado::localize('TEXT_GERER_ARCHIVES'), $styleSimplePolice8);
            if (Atexo_Module::isEnabled('ArchivageConsultationSurPf')) {
                $worksheet->write($ligne++, 1, Prado::localize('TELECHARGEMENT_DES_ARCHIVES'), $styleSimplePolice8);
            }
            $worksheet->write($ligne++, 1, Prado::localize('TEXT_ACCEDER_TOUS_TELECHARGEMENTS'), $styleSimplePolice8);
            $worksheet->write($ligne++, 1, '', $styleSimplePolice8);

            $worksheet->write($ligne, 0, Prado::localize('TEXT_ADMINISTRATION_PMI_ENTITE_ACHAT'), $styleSimplePolice8FondJaune);
            $worksheet->write($ligne, 1, '', $styleSimplePolice8FondJaune);
            $worksheet->write($ligne++, 2, '', $styleSimplePolice8FondJaune);

            if (Atexo_Module::isEnabled('GererMonService')) {
                $worksheet->write($ligne++, 1, Prado::localize('GERER_MON_SERVICE'), $styleSimplePolice8);
            }
            $worksheet->write($ligne++, 1, Prado::localize('TEXT_GERER_ENTITE_ACHAT'), $styleSimplePolice8);
            $worksheet->write($ligne++, 1, Prado::localize('TEXT_GERER_AGENTS'), $styleSimplePolice8);
            $worksheet->write($ligne++, 1, Prado::localize('TEXT_GERER_HABILITATIONS'), $styleSimplePolice8);
            if (Atexo_Module::isEnabled('PubliciteFormatXml')) {
                $worksheet->write($ligne++, 1, Prado::localize('TEXT_GERER_CPT_BOAMP'), $styleSimplePolice8);
                $worksheet->write($ligne++, 1, Prado::localize('TEXT_GERER_COMPTES_MONITEUR'), $styleSimplePolice8);
            }
            if (Atexo_Module::isEnabled('GererAdressesService')) {
                $worksheet->write($ligne++, 1, Prado::localize('PARAMETRAGES_ADRESSES'), $styleSimplePolice8);
            }
            if (Atexo_Module::isEnabled('GererCertificatsAgent')) {
                $worksheet->write($ligne++, 1, Prado::localize('DEFINE_GERER_CERTIFICAT_AGENT'), $styleSimplePolice8);
            }
            if (Atexo_Module::isEnabled('GestionMandataire')) {
                $worksheet->write($ligne++, 1, Prado::localize('DEFINE_GERER_MANDATAIRE'), $styleSimplePolice8);
            }
            $worksheet->write($ligne++, 1, Prado::localize('GERER_COMPTES_JAL'), $styleSimplePolice8);
            $worksheet->write($ligne++, 1, Prado::localize('TEXT_DEFINE_GERER_CLES_CHIFFREMENT'), $styleSimplePolice8);
            $worksheet->write($ligne++, 1, Prado::localize('TEXT_FOURNISSEUR_DOC'), $styleSimplePolice8);
            if (Atexo_Module::isEnabled('CentralePublication')) {
                $worksheet->write($ligne++, 1, Prado::localize('TEXT_GERER_COMPTE_CENTRALES_PUBLICATION'), $styleSimplePolice8);
            }
            //$worksheet->write(71, 1, Prado::localize('DEFINE_STATISTIQUES_SITE'), $styleSimplePolice8);
            //$worksheet->write(72, 1, Prado::localize('DEFINE_STATISTIQUES_QoS'), $styleSimplePolice8);

            if (Atexo_Module::isEnabled('GestionNewsletter')) {
                $worksheet->write($ligne++, 1, '', $styleSimplePolice8);

                $worksheet->write($ligne, 0, Prado::localize('TEXT_GESTION_NEWSLETTER'), $styleSimplePolice8FondJaune);
                $worksheet->write($ligne, 1, '', $styleSimplePolice8FondJaune);
                $worksheet->write($ligne++, 2, '', $styleSimplePolice8FondJaune);
                $worksheet->write($ligne++, 1, Prado::localize('TEXT_GERER_NEWSLETTER'), $styleSimplePolice8);
                if (Atexo_Module::isEnabled('InterfaceModuleRsem')) {
                    $worksheet->write($ligne++, 1, Prado::localize('TEXT_GERER_NEWSLETTER_REDAC'), $styleSimplePolice8);
                }
            }

            if (Atexo_Module::isEnabled('MenuAgentSocietesExclues')) {
                $worksheet->write($ligne++, 1, '', $styleSimplePolice8);

                $worksheet->write($ligne, 0, Prado::localize('TEXT_SOCIETES_EXCLUES'), $styleSimplePolice8FondJaune);
                $worksheet->write($ligne, 1, '', $styleSimplePolice8FondJaune);
                $worksheet->write($ligne++, 2, '', $styleSimplePolice8FondJaune);

                $worksheet->write($ligne++, 1, Prado::localize('TEXT_CREER_SOCIETES_EXCLUES'), $styleSimplePolice8);
                $worksheet->write($ligne++, 1, Prado::localize('TEXT_MODIFIER_SOCIETES_EXCLUES'), $styleSimplePolice8);
                $worksheet->write($ligne++, 1, Prado::localize('TEXT_SUPPRIMER_SOCIETES_EXCLUES'), $styleSimplePolice8);
                $worksheet->write($ligne++, 1, Prado::localize('TEXT_PORTEE_SOCIETES_EXCLUES_TOUS_ORGANISMES'), $styleSimplePolice8);
            }
            if (Atexo_Module::isEnabled('GestionAdressesFacturationJAL')) {
                $worksheet->write($ligne++, 1, '', $styleSimplePolice8);
                $worksheet->write($ligne, 0, Prado::localize('TEXT_GESTION_ADRESSE_FACTURATION_JAL'), $styleSimplePolice8FondJaune);
                $worksheet->write($ligne, 1, '', $styleSimplePolice8FondJaune);
                $worksheet->write($ligne++, 2, '', $styleSimplePolice8FondJaune);
            }
            // la feuille (konoha)
            $colonne = 3;

            foreach ($agents as $agent) {
                $ligne = 4;
                $worksheet->setColumn($colonne, $colonne, '15.86');

                $criteria = new Criteria();
                $criteria->add(CommonHabilitationAgentPeer::ID_AGENT, $agent->getId());
                $habilitationsAgent = BaseCommonHabilitationAgentPeer::doSelectOne($criteria, $connexion);

                if ($habilitationsAgent instanceof CommonHabilitationAgent) {
                    $worksheet->write($ligne++, $colonne, $agent->getNom(), $styleSimplePolice8);
                    $worksheet->write($ligne++, $colonne, $agent->getPrenom(), $styleSimplePolice8);
                    $worksheet->write($ligne++, $colonne, $agent->getLogin(), $styleSimplePolice8);
                    $worksheet->write($ligne++, $colonne, $agent->getEmail(), $styleSimplePolice8);
                    ++$ligne;
                    $worksheet->write($ligne++, $colonne, '', $styleSimplePolice8FondJaune);
                    $worksheet->write($ligne++, $colonne, '', $styleSimplePolice8FondGris);
                    if (Atexo_Module::isEnabled('ProcedureAdaptee')) {
                        $worksheet->write($ligne++, $colonne, $habilitationsAgent->getGererMapaInferieurMontant(), $styleSimplePolice8);
                        $worksheet->write($ligne++, $colonne, $habilitationsAgent->getGererMapaSuperieurMontant(), $styleSimplePolice8);
                    }
                    $worksheet->write($ligne++, $colonne, $habilitationsAgent->getAdministrerProceduresFormalisees(), $styleSimplePolice8);
                    $worksheet->write($ligne++, $colonne, $habilitationsAgent->getCreerConsultationTransverse(), $styleSimplePolice8);

                    $worksheet->write($ligne++, $colonne, $habilitationsAgent->getCreerSuiteConsultation(), $styleSimplePolice8);
                    $worksheet->write($ligne++, $colonne, $habilitationsAgent->getPublierConsultation(), $styleSimplePolice8);
                    $worksheet->write($ligne++, $colonne, $habilitationsAgent->getEnvoyerPublicite(), $styleSimplePolice8);
                    $worksheet->write($ligne++, $colonne, $habilitationsAgent->getModifierConsultationAvantValidation(), $styleSimplePolice8);

                    $worksheet->write($ligne++, $colonne, $habilitationsAgent->getValidationSimple(), $styleSimplePolice8);
                    $worksheet->write($ligne++, $colonne, $habilitationsAgent->getValidationIntermediaire(), $styleSimplePolice8);
                    $worksheet->write($ligne++, $colonne, $habilitationsAgent->getValidationFinale(), $styleSimplePolice8);

                    $worksheet->write($ligne++, $colonne, '', $styleSimplePolice8);
                    //$worksheet->write($ligne++, $colonne, $habilitationsAgent->getModifierConsultationApresValidation(), $styleSimplePolice8);
                    if (Atexo_Module::isEnabled('ProcedureAdaptee')) {
                        $worksheet->write($ligne++, $colonne, $habilitationsAgent->getModifierConsultationMapaInferieurMontantApresValidation(), $styleSimplePolice8);
                        $worksheet->write($ligne++, $colonne, $habilitationsAgent->getModifierConsultationMapaSuperieurMontantApresValidation(), $styleSimplePolice8);
                    }
                    $worksheet->write($ligne++, $colonne, $habilitationsAgent->getModifierConsultationProceduresFormaliseesApresValidation(), $styleSimplePolice8);
                    $worksheet->write($ligne++, $colonne, $habilitationsAgent->getAccesRegistreRetraitsPapier(), $styleSimplePolice8);
                    $worksheet->write($ligne++, $colonne, $habilitationsAgent->getAccesRegistreQuestionsPapier(), $styleSimplePolice8);
                    $worksheet->write($ligne++, $colonne, $habilitationsAgent->getAccesRegistreDepotsPapier(), $styleSimplePolice8);
                    $worksheet->write($ligne++, $colonne, $habilitationsAgent->getSuiviSeulRegistreRetraitsPapier(), $styleSimplePolice8);
                    $worksheet->write($ligne++, $colonne, $habilitationsAgent->getSuiviSeulRegistreQuestionsPapier(), $styleSimplePolice8);
                    $worksheet->write($ligne++, $colonne, $habilitationsAgent->getSuiviSeulRegistreDepotsPapier(), $styleSimplePolice8);

                    $worksheet->write($ligne++, $colonne, '', $styleSimplePolice8);

                    $worksheet->write($ligne++, $colonne, $habilitationsAgent->getAccesReponses(), $styleSimplePolice8);
                    $worksheet->write($ligne++, $colonne, $habilitationsAgent->getTelechargementGroupeAnticipePlisChiffres(), $styleSimplePolice8);
                    $worksheet->write($ligne++, $colonne, $habilitationsAgent->getTelechargementUnitairePlisChiffres(), $styleSimplePolice8);
                    $worksheet->write($ligne++, $colonne, $habilitationsAgent->getOuvrirCandidatureEnLigne(), $styleSimplePolice8);
                    $worksheet->write($ligne++, $colonne, $habilitationsAgent->getOuvrirCandidatureADistance(), $styleSimplePolice8);
                    $worksheet->write($ligne++, $colonne, $habilitationsAgent->getOuvrirCandidatureHorsLigne(), $styleSimplePolice8);
                    $worksheet->write($ligne++, $colonne, $habilitationsAgent->getAccesReponses(), $styleSimplePolice8);
                    $worksheet->write($ligne++, $colonne, $habilitationsAgent->getOuvrirOffreEnLigne(), $styleSimplePolice8);
                    $worksheet->write($ligne++, $colonne, $habilitationsAgent->getOuvrirOffreADistance(), $styleSimplePolice8);
                    $worksheet->write($ligne++, $colonne, $habilitationsAgent->getOuvrirOffreHorsLigne(), $styleSimplePolice8);
                    if (Atexo_Module::isEnabled('EnveloppeOffreTechnique')) {
                        $worksheet->write($ligne++, $colonne, $habilitationsAgent->getOuvrirOffreTechniqueEnLigne(), $styleSimplePolice8);
                        $worksheet->write($ligne++, $colonne, $habilitationsAgent->getOuvrirOffreTechniqueADistance(), $styleSimplePolice8);
                        $worksheet->write($ligne++, $colonne, $habilitationsAgent->getOuvrirOffreTechniqueHorsLigne(), $styleSimplePolice8);
                    }
                    if (Atexo_Module::isEnabled('EnveloppeAnonymat')) {
                        $worksheet->write($ligne++, $colonne, $habilitationsAgent->getOuvrirAnonymatEnLigne(), $styleSimplePolice8);
                        $worksheet->write($ligne++, $colonne, $habilitationsAgent->getOuvrirAnonymatADistance(), $styleSimplePolice8);
                        $worksheet->write($ligne++, $colonne, $habilitationsAgent->getOuvrirAnonymatHorsLigne(), $styleSimplePolice8);
                    }
                    $worksheet->write($ligne++, $colonne, $habilitationsAgent->getRefuserEnveloppe(), $styleSimplePolice8);
                    $worksheet->write($ligne++, $colonne, $habilitationsAgent->getRestaurerEnveloppe(), $styleSimplePolice8);
                    $worksheet->write($ligne++, $colonne, $habilitationsAgent->getGererAdmissibilite(), $styleSimplePolice8);
                    $worksheet->write($ligne++, $colonne, $habilitationsAgent->getImporterEnveloppe(), $styleSimplePolice8);
                    $worksheet->write($ligne++, $colonne, $habilitationsAgent->getEnvoyerMessage(), $styleSimplePolice8);
                    $worksheet->write($ligne++, $colonne, $habilitationsAgent->getSuivreMessage(), $styleSimplePolice8);
                    $worksheet->write($ligne++, $colonne, $habilitationsAgent->getHistoriqueNavigationInscrits(), $styleSimplePolice8);

                    $worksheet->write($ligne++, $colonne, '', $styleSimplePolice8);

                    if (Atexo_Module::isEnabled('EncheresEntreprise')) {
                        $worksheet->write($ligne++, $colonne, $habilitationsAgent->getGererEncheres(), $styleSimplePolice8);
                        $worksheet->write($ligne++, $colonne, $habilitationsAgent->getSuivreEncheres(), $styleSimplePolice8);
                    }

                    $worksheet->write($ligne++, $colonne, '', $styleSimplePolice8);
                    if (Atexo_Module::isEnabled('InterfaceModuleSub')) {
                        $worksheet->write($ligne++, $colonne, $habilitationsAgent->getGererReouverturesModification(), $styleSimplePolice8);
                    }
                    $worksheet->write($ligne++, $colonne, $habilitationsAgent->getAttributionMarche(), $styleSimplePolice8);
                    $worksheet->write($ligne++, $colonne, $habilitationsAgent->getDecisionSuiviSeul(), $styleSimplePolice8);
                    if (Atexo_Module::isEnabled('ResultatAnalyseAvantDecision')) {
                        $worksheet->write($ligne++, $colonne, $habilitationsAgent->getResultatAnalyse(), $styleSimplePolice8);
                    }
                    if (Atexo_Module::isEnabled('AnnulerConsultation')) {
                        $worksheet->write($ligne++, $colonne, $habilitationsAgent->getAnnulerConsultation(), $styleSimplePolice8);
                    }
                    ++$ligne;
                    $worksheet->write($ligne++, $colonne, '', $styleSimplePolice8FondJaune);
                    if (Atexo_Module::isEnabled('InterfaceModuleRsem')) {
                        $worksheet->write($ligne++, $colonne, $habilitationsAgent->getReprendreIntegralementArticle(), $styleSimplePolice8);
                        $worksheet->write($ligne++, $colonne, $habilitationsAgent->getRedactionDocumentsRedac(), $styleSimplePolice8);
                        $worksheet->write($ligne++, $colonne, $habilitationsAgent->getValidationDocumentsRedac(), $styleSimplePolice8);
                        $worksheet->write($ligne++, $colonne, $habilitationsAgent->getAdministrerClausesEntiteAchats(), $styleSimplePolice8);
                        $worksheet->write($ligne++, $colonne, $habilitationsAgent->getGenererPiecesFormatOdt(), $styleSimplePolice8);
                        $worksheet->write($ligne++, $colonne, $habilitationsAgent->getGererGabaritEditeur(), $styleSimplePolice8);
                        $worksheet->write($ligne++, $colonne, $habilitationsAgent->getGererGabarit(), $styleSimplePolice8);
                        $worksheet->write($ligne++, $colonne, $habilitationsAgent->getGererGabaritEntiteAchats(), $styleSimplePolice8);
                        $worksheet->write($ligne++, $colonne, $habilitationsAgent->getGererGabaritAgent(), $styleSimplePolice8);
                        ++$ligne;
                        $worksheet->write($ligne++, $colonne, '', $styleSimplePolice8FondJaune);
                    }
                    $worksheet->write($ligne++, $colonne, $habilitationsAgent->getSuiviEntreprise(), $styleSimplePolice8);
                    if (Atexo_Module::isEnabled('GestionEntrepriseParAgent')) {
                        $worksheet->write($ligne++, $colonne, $habilitationsAgent->getGererLesEntreprises(), $styleSimplePolice8);
                    }
                    if (Atexo_Module::isEnabled('AnnuaireAcheteursPublics')) {
                        $worksheet->write($ligne++, $colonne, $habilitationsAgent->getAnnuaireAcheteur(), $styleSimplePolice8);
                    }

                    ++$ligne;
                    $worksheet->write($ligne++, $colonne, '', $styleSimplePolice8FondJaune);

                    $worksheet->write($ligne++, $colonne, $habilitationsAgent->getCreerAnnonceInformation(), $styleSimplePolice8);
                    $worksheet->write($ligne++, $colonne, $habilitationsAgent->getCreerAnnonceAttribution(), $styleSimplePolice8);
                    if (Atexo_Module::isEnabled('AutreAnnonceExtraitPv')) {
                        $worksheet->write($ligne++, $colonne, $habilitationsAgent->getCreerAnnonceExtraitPv(), $styleSimplePolice8);
                    }
                    if (Atexo_Module::isEnabled('AutreAnnonceRapportAchevement')) {
                        $worksheet->write($ligne++, $colonne, $habilitationsAgent->getCreerAnnonceRapportAchevement(), $styleSimplePolice8);
                    }
                    if (Atexo_Module::isEnabled('AutreAnnonceProgrammePrevisionnel')) {
                        $worksheet->write($ligne++, $colonne, $habilitationsAgent->getCreerAvisProgrammePrevisionnel(), $styleSimplePolice8);
                    }
                    ++$ligne;
                    $worksheet->write($ligne++, $colonne, '', $styleSimplePolice8FondJaune);

                    $worksheet->write($ligne++, $colonne, $habilitationsAgent->getSuiviEntreprise(), $styleSimplePolice8);
                    if (Atexo_Module::isEnabled('GestionEntrepriseParAgent')) {
                        $worksheet->write($ligne++, $colonne, $habilitationsAgent->getGererLesEntreprises(), $styleSimplePolice8);
                    }
                    $worksheet->write($ligne++, $colonne, $habilitationsAgent->getSupprimerEnveloppe(), $styleSimplePolice8);

                    if (Atexo_Module::isEnabled('Article133GenerationPf')) {
                        $worksheet->write($ligne++, $colonne, '', $styleSimplePolice8FondGris);

                        $worksheet->write($ligne++, $colonne, $habilitationsAgent->getPublicationMarches(), $styleSimplePolice8);
                    }
                    if (Atexo_Module::isEnabled('Article133GenerationPf') && Atexo_Module::isEnabled('ExportMarchesNotifies', Atexo_CurrentUser::getCurrentOrganism())) {
                        $worksheet->write($ligne++, $colonne, $habilitationsAgent->getListeMarchesNotifies(), $styleSimplePolice8);
                    }
                    $worksheet->write($ligne++, $colonne, $habilitationsAgent->getGererStatistiquesMetier(), $styleSimplePolice8);
                    if (Atexo_Module::isEnabled('InterfaceChorusPmi')) {
                        $worksheet->write($ligne++, $colonne, $habilitationsAgent->getSuiviFluxChorusTransversal(), $styleSimplePolice8);
                    }
                    ++$ligne;
                    $worksheet->write($ligne++, $colonne, '', $styleSimplePolice8FondJaune);

                    $worksheet->write($ligne++, $colonne, $habilitationsAgent->getGererArchives(), $styleSimplePolice8);
                    if (Atexo_Module::isEnabled('ArchivageConsultationSurPf')) {
                        $worksheet->write($ligne++, $colonne, $habilitationsAgent->getDownloadArchives(), $styleSimplePolice8);
                    }
                    $worksheet->write($ligne++, $colonne, $habilitationsAgent->getAccederTousTelechargements(), $styleSimplePolice8);
                    ++$ligne;
                    $worksheet->write($ligne++, $colonne, '', $styleSimplePolice8FondJaune);

                    if (Atexo_Module::isEnabled('GererMonService')) {
                        $worksheet->write($ligne++, $colonne, $habilitationsAgent->getGererMonService(), $styleSimplePolice8);
                    }
                    $worksheet->write($ligne++, $colonne, $habilitationsAgent->getGestionAgentPole(), $styleSimplePolice8);
                    $worksheet->write($ligne++, $colonne, $habilitationsAgent->getGestionAgents(), $styleSimplePolice8);
                    $worksheet->write($ligne++, $colonne, $habilitationsAgent->getGestionHabilitations(), $styleSimplePolice8);
                    if (Atexo_Module::isEnabled('PubliciteFormatXml')) {
                        $worksheet->write($ligne++, $colonne, $habilitationsAgent->getGestionCompteBoamp(), $styleSimplePolice8);
                        $worksheet->write($ligne++, $colonne, $habilitationsAgent->getGestionCompteGroupeMoniteur(), $styleSimplePolice8);
                    }
                    if (Atexo_Module::isEnabled('GererAdressesService')) {
                        $worksheet->write($ligne++, $colonne, $habilitationsAgent->getGererAdressesService(), $styleSimplePolice8);
                    }
                    if (Atexo_Module::isEnabled('GererCertificatsAgent')) {
                        $worksheet->write($ligne++, $colonne, $habilitationsAgent->getGestionCertificatsAgent(), $styleSimplePolice8);
                    }
                    if (Atexo_Module::isEnabled('GestionMandataire')) {
                        $worksheet->write($ligne++, $colonne, $habilitationsAgent->getGestionMandataire(), $styleSimplePolice8);
                    }
                    $worksheet->write($ligne++, $colonne, $habilitationsAgent->getGestionCompteJal(), $styleSimplePolice8);
                    $worksheet->write($ligne++, $colonne, $habilitationsAgent->getGestionBiCles(), $styleSimplePolice8);
                    $worksheet->write($ligne++, $colonne, $habilitationsAgent->getGestionFournisseursEnvoisPostaux(), $styleSimplePolice8);
                    if (Atexo_Module::isEnabled('CentralePublication')) {
                        $worksheet->write($ligne++, $colonne, $habilitationsAgent->getGestionCentralePub(), $styleSimplePolice8);
                    }
                    //$worksheet->write(71, $colonne, $habilitationsAgent->getStatistiquesSite(), $styleSimplePolice8);
                    //$worksheet->write(72, $colonne, $habilitationsAgent->getStatistiquesQos(), $styleSimplePolice8);
                    if (Atexo_Module::isEnabled('GestionNewsletter')) {
                        ++$ligne;
                        $worksheet->write($ligne++, $colonne, '', $styleSimplePolice8FondJaune);
                        $worksheet->write($ligne++, $colonne, $habilitationsAgent->getGererNewsletter(), $styleSimplePolice8);
                        if (Atexo_Module::isEnabled('InterfaceModuleRsem')) {
                            $worksheet->write($ligne++, $colonne, $habilitationsAgent->getGererNewsletterRedac(), $styleSimplePolice8);
                        }
                    }
                    if (Atexo_Module::isEnabled('MenuAgentSocietesExclues')) {
                        ++$ligne;
                        $worksheet->write($ligne++, $colonne, '', $styleSimplePolice8FondJaune);

                        $worksheet->write($ligne++, $colonne, $habilitationsAgent->getPorteeSocietesExclues(), $styleSimplePolice8);
                        $worksheet->write($ligne++, $colonne, $habilitationsAgent->getModifierSocietesExclues(), $styleSimplePolice8);
                        $worksheet->write($ligne++, $colonne, $habilitationsAgent->getSupprimerSocietesExclues(), $styleSimplePolice8);
                        $worksheet->write($ligne++, $colonne, $habilitationsAgent->getPorteeSocietesExcluesTousOrganismes(), $styleSimplePolice8);
                    }
                    if (Atexo_Module::isEnabled('GestionAdressesFacturationJAL')) {
                        ++$ligne;
                        $worksheet->write($ligne++, $colonne, '', $styleSimplePolice8FondJaune);

                        $worksheet->write($ligne++, $colonne, $habilitationsAgent->getGestionAdressesFacturationJal(), $styleSimplePolice8);
                    }

                    ++$ligne;
                    ++$ligne;
                    $worksheet->write($ligne++, $colonne, $habilitationsAgent->getValiderProjetAchat() ?: "0", $styleSimplePolice8);

                    ++$colonne;
                }
            }
        }

        $workbook->close();
        if (!$toFile) {
            $workbook->send($nomFichier);
        }

        exit;
    }

    public function genererListeEntrepriseExcel($listEntreprises, $toPdf = false)
    {
        try {
            $file = Atexo_Config::getParameter('COMMON_TMP').session_id().Atexo_Config::getParameter('NOM_FICHIER_EXPORT_RECHERCHE_ENTREPRISES');
            $nomFile = basename(Atexo_Config::getParameter('NOM_FICHIER_EXPORT_RECHERCHE_ENTREPRISES'));

            //Initialisation du fichier excel
            $workbook = new Spreadsheet_Excel_Writer($file);
            $workbook->setCustomColor(11, 51, 204, 255);
            //Pour passer outre la limite des 255 caracteres
            $workbook->setVersion(8);

            $format_bold = &$workbook->addFormat();
            $format_bold->setColor('black');
            $format_bold->setSize(10);
            $format_bold->setAlign('left');
            $format_bold->setBold(1);

            // format normal
            $format = &$workbook->addFormat();
            $format->setColor('black');
            $format->setSize(8);
            $format->setAlign('left');
            $format->setAlign('vjustify');

            $stylePolice10BlackSimple = &$workbook->addFormat();
            $stylePolice10BlackSimple->setColor('black');
            $stylePolice10BlackSimple->setSize(8);
            $stylePolice10BlackSimple->setAlign('left');

            $format_center = &$workbook->addFormat();
            $format_center = &$workbook->addFormat();
            $format_center->setColor('black');
            $format_center->setFgColor('white');
            $format_center->setFontFamily('Arial');
            $format_center->setSize(8);
            $format_center->setAlign('vcenter');
            $format_center->setalign('vjustify');
            $format_center->setAlign('center');

            //Style des cellules d'entete
            $format_head = &$workbook->addFormat();
            $format_head->setBold();
            $format_head->setColor('black');
            $format_head->setFgColor(11);
            $format_head->setAlign('center');
            $format_head->setFontFamily('Arial');
            $format_head->setSize(8);
            $format_head->setalign('vjustify');

            $format_montant = &$workbook->addFormat();
            $format_montant->setColor('black');
            $format_montant->setSize(8);
            $format_montant->setAlign('right');
            $format_montant->setalign('vjustify');

            $format_date = &$workbook->addFormat();
            $format_date->setColor('black');
            $format_date->setSize(8);
            $format_date->setAlign('left');
            $format_date->setalign('vjustify');

            // la feuille
            $worksheet = &$workbook->addWorkSheet(Prado::localize('TEXT_RESULTAT'));

            if ('iso-8859-1' != Atexo_Config::getParameter('HTTP_ENCODING')) {
                $worksheet->setInputEncoding(Atexo_Config::getParameter('HTTP_ENCODING'));
            }

            $worksheet->setLandscape();
            $worksheet->setPrintScale(100);
            //$worksheet->hideGridlines();
            $worksheet->setHeader('&"Arial"&8'.$nomFile.' - '.Prado::localize('TEXT_RESULTAT'), '0.20');
            $worksheet->setFooter(self::getFooterImprimeLe(), '0.20');

            $worksheet->setMarginLeft('0.20');
            $worksheet->setMarginRight('0.20');
            $worksheet->setMarginTop('0.40');
            $worksheet->setMarginBottom('0.40');

            //Creation de l'entete du fichier excel

            if (Atexo_Module::isEnabled('CompteEntrepriseRcville')) {
                $worksheet->setRow('2', '32');
                $worksheet->setColumn(0, 0, 14);
                $worksheet->setColumn(0, 1, 17);
                $worksheet->setColumn(0, 2, 24.71);
                $worksheet->setColumn(0, 3, 44.71);
                $worksheet->setRow(0, '18');
                $worksheet->setRow(1, '18');
            } else {
                $worksheet->setRow('2', '26.25');
                $worksheet->setColumn(0, 0, 20.71);
                $worksheet->setColumn(0, 1, 32);
                $worksheet->setColumn(0, 2, 9.71);
                $worksheet->setColumn(0, 3, 50.71);
                $worksheet->setRow(0, '14.25');
                $worksheet->setRow(1, '14.25');
            }

            $worksheet->setColumn(0, 4, 30);
            $worksheet->setColumn(0, 5, 30);
            $worksheet->setColumn(0, 6, 30);
            /*$worksheet->setMerge(0,0,0,2);
            $worksheet->setMerge(1,0,1,1);
                $worksheet->setMerge(1,2,1,4);
            $worksheet->setMerge(2,0,2,1);
                $worksheet->setMerge(2,2,2,6);*/

            $worksheet->write(0, 0, Prado::localize('TEXT_RECHERCHE_ENTREPRISE'), $format_bold);
            $col = 0;
            $row = 2;
            $worksheet->write($row, $col++, Prado::localize('TEXT_RAISON_SOCIALE'), $format_head);
            $worksheet->writeString($row, $col++, Prado::localize('TEXT_IDENTIFIANT_SANS_POINTS'), $format_head);

            $headerCol2 = Prado::localize('DEFINE_ADRESSE_POSTAL').self::$newLineSeparator.'/ ';
            // Ajout dela region si module active
            if (Atexo_Module::isEnabled('CompteEntrepriseRegion')) {
                $headerCol2 .= Prado::localize('TEXT_REGION').self::$newLineSeparator.'/ ';
            }
            // Ajout de la province si module active
            if (Atexo_Module::isEnabled('CompteEntrepriseProvince')) {
                $headerCol2 .= Prado::localize('TEXT_PROVINCE').self::$newLineSeparator.'/ ';
            }
            $worksheet->write($row, $col++, $headerCol2, $format_head);

            // ajout de domaines d'activites de l'entreprise si le module est active
            if (Atexo_Module::isEnabled('CompteEntrepriseDomaineActivite')) {
                $worksheet->write($row, $col++, Prado::localize('TEXT_DOMAINES_ACTIVITES'), $format_head);
            } else {
                $worksheet->write($row, $col++, Prado::localize('TEXT_CODE_APE'), $format_head);
            }

            $headerCol4 = Prado::localize('DEFINE_DESCRIPTION_ACTIVITE')."\n/ ";
            // Si module qualification est active on rajoute les qualifications de l'entreprise
            if (Atexo_Module::isEnabled('CompteEntrepriseQualification')) {
                $headerCol4 .= Prado::localize('TEXT_QUALIFICATIONS')."\n/ ";
            }

            // pareille pour l'agrement
            if (Atexo_Module::isEnabled('CompteEntrepriseAgrement')) {
                $headerCol4 .= Prado::localize('TEXT_AGREMENT');
            }

            $worksheet->write($row, $col++, $headerCol4, $format_head);

            $refsEntreprise = (new Atexo_Referentiel_Referentiel())->getAllLtReferentielForEntreprise();

            foreach ($refsEntreprise as $refEntreprise) {
                $libel = Prado::localize($refEntreprise->getCodeLibelle());
                $worksheet->write($row, $col, $libel, $format_head);
                ++$col;
            }
            $worksheet->write($row, $col++, Prado::localize('MAILS_CONTACTS'), $format_head);
            $worksheet->write($row, $col++, Prado::localize('TEXT_DATE_DE_CREATION'), $format_head);

            ++$row;
            //$marche = new Marche();
            foreach ($listEntreprises as $entreprise) {
                $col = 0;
                $worksheet->setRow($row, '45');
                $worksheet->write($row, $col++, Atexo_Util::atexoHtmlEntitiesDecode($entreprise->getNom()), $format);
                $worksheet->writeString($row, $col++, Atexo_Util::atexoHtmlEntitiesDecode($entreprise->getSiren().$entreprise->getnicSiege().$entreprise->getSirenetranger()), $format);
                $ligne2 = $entreprise->getAdresse().((!$entreprise->getAdresse2()) ? '' : self::$newLineSeparator.
                       $entreprise->getAdresse2()).self::$newLineSeparator.$entreprise->getCodepostal().self::$newLineSeparator;
                if (Atexo_Module::isEnabled('CompteEntrepriseProvince')) {
                    $ligne2 .= Atexo_Geolocalisation_GeolocalisationN1::retrieveDenomination1ByIdGeoN1($entreprise->getRegion()).self::$newLineSeparator;
                }
                if (Atexo_Module::isEnabled('CompteEntrepriseRegion')) {
                    $ligne2 .= (new Atexo_Geolocalisation_GeolocalisationN2())->retrieveDenomination1ByIdGeoN3($entreprise->getProvince()).self::$newLineSeparator;
                }
                $ligne2 .= $entreprise->getVilleadresse().self::$newLineSeparator.$entreprise->getPaysadresse();
                $worksheet->write($row, $col++, $ligne2, $format);

                if (Atexo_Module::isEnabled('CompteEntrepriseDomaineActivite')) {
                    $worksheet->write($row, $col++, Atexo_Consultation_Category::displayListeLibelleDomaineByArrayId($entreprise->getDomainesActivites(), self::$newLineSeparator), $format);
                } else {
                    $worksheet->write($row, $col++, $entreprise->getCodeape(), $format);
                }
                $ligne4 = $entreprise->getDescriptionActivite().self::$newLineSeparator;
                if (Atexo_Module::isEnabled('CompteEntrepriseAgrement')) {
                    $ligne4 .= (new Atexo_Agrement())->retrieveLibelleAgrementsByIds($entreprise->getAgrement()).self::$newLineSeparator;
                }
                if (Atexo_Module::isEnabled('CompteEntrepriseQualification')) {
                    $ligne4 .= (new Atexo_Qualifications())->displayListeLibelleDomaineByArrayId($entreprise->getQualification());
                }
                $worksheet->write($row, $col++, $ligne4, $format);

                foreach ($refsEntreprise as $ref) {
                    $entrepriseRef = (new Atexo_Referentiel_Referentiel())->getLtReferentielByEntreprise($entreprise->getId(), $ref->getId());
                    if ($entrepriseRef) {
                        $value = $entrepriseRef->getValeurPrincipaleLtReferentiel().$entrepriseRef->getValeurSecondaireLtReferentiel();
                    } else {
                        $value = '';
                    }
                    $value = implode(';', explode('#', ltrim($value, '#')));
                    $worksheet->write($row, $col++, $value, $format);
                }

                $listMails = (new Atexo_Entreprise_Inscrit())->getListMailInscritByEntreprise($entreprise->getId());
                $listMails = implode(', ', $listMails);
                $worksheet->write($row, $col++, $listMails, $format);
                $worksheet->write($row, $col++, Atexo_Util::iso2frnDateTime($entreprise->getDateCreation(), false), $format);

                ++$row;
            }

            $workbook->close();
            if ($toPdf) {
                $pathFichier = Atexo_Config::getParameter('COMMON_TMP').$nomFile;
                //echo "mv " . $tmpFileName . $pathFichier . " 2>&1 > ".Atexo_Config::getParameter('COMMON_TMP')."zip_error", $sys_answer; exit();
                system('mv '.$file." '".$pathFichier."' > ".Atexo_Config::getParameter('COMMON_TMP').'zip_error 2>&1');

                return $pathFichier;
            }
            $workbook->send($nomFile);

            $fh = fopen($file, 'rb');
            while (!feof($fh)) {
                $buf = fread($fh, 4096);
                echo $buf;
            }
            fclose($fh);
            header('Connection: close');
            @unlink($pathFichier);
            @unlink($file);
        } catch (\Exception $e) {
            throw new Exception(Prado::localize('TEXT_ERREUR_EXPORT_EXCEL').'  '.$e->getMessage());
        }
    }

    /*
     * Fonction qui generer les fichiers Execel quand le module est active (le checkbox "Restreindre la recherche" est coche)
     */

    public function genererListeEntrepriseExcelDonneeComplementaire($listEntreprises, $toPdf = false)
    {
        try {
            (new Atexo_Config())->includePhpLibPath();
            require_once 'Spreadsheet/Excel/Writer.php';

            //$file = $this->getApplication()->Parameters['FICHIER_EXCEL_GENERE'];
            $file = Atexo_Config::getParameter('COMMON_TMP').Prado::localize('TEXT_EXPORT_RECHERCHE_ENTREPRISES_XLS');
            $nomFile = basename($file);

            //Initialisation du fichier excel
            if ($toPdf) {
                $workbook = new Spreadsheet_Excel_Writer($file);
            } else {
                $workbook = new Spreadsheet_Excel_Writer();
            }
            $workbook->setCustomColor(11, 51, 204, 255);
            //Pour passer outre la limite des 255 caracteres
            $workbook->setVersion(8);

            // format normal
            $format = &$workbook->addFormat();
            $format->setColor('black');
            $format->setSize(10);
            $format->setAlign('center');
            $format->setAlign('top');
            $format->setTextWrap();

            //Style des cellules d'entete
            $format_head = &$workbook->addFormat();
            $format_head->setBold();
            $format_head->setColor('black');
            $format_head->setFgColor(11);
            $format_head->setAlign('center');
            $format_head->setFontFamily('Arial');
            $format_head->setSize(10);

            // la feuille
            $worksheet = &$workbook->addWorkSheet(Prado::localize('TEXT_RESULTAT'));
            $worksheet->setInputEncoding(Atexo_Config::getParameter('HTTP_ENCODING'));
            /*$worksheet->setLandscape();
            $worksheet->setPrintScale(100);
            //$worksheet->hideGridlines();
            $worksheet->setHeader("&\"Arial\"&8".$nomFile." - Resultat", '0.20');
            $worksheet->setFooter(self::getFooterImprimeLe(),  '0.20');

            $worksheet->setMarginLeft('0.20');
            $worksheet->setMarginRight('0.20');
            $worksheet->setMarginTop('0.40');
            $worksheet->setMarginBottom('0.40');
*/

            //Creation de l'entete du fichier excel
            $worksheet->setColumn(0, 0, '38.57');
            $worksheet->setColumn(1, 1, '59.43');
            $worksheet->setColumn(2, 2, '14.57');
            $worksheet->setColumn(3, 3, '11');
            $worksheet->setColumn(4, 4, '4.57');
            $worksheet->setColumn(5, 5, '6');
            $worksheet->setColumn(6, 6, '10');
            $worksheet->setColumn(7, 7, '3.71');
            $worksheet->setColumn(8, 8, '22.71');
            $worksheet->setColumn(9, 9, '14.86');
            $worksheet->setColumn(10, 10, '9.29');
            $worksheet->setColumn(11, 11, '19.57');
            $worksheet->setColumn(12, 12, '27.29');
            $worksheet->setColumn(13, 13, '25.86');
            $worksheet->setColumn(14, 14, '18.57');
            $worksheet->setColumn(15, 15, '18.57');
            $worksheet->setColumn(16, 16, '24.71');
            $worksheet->setColumn(17, 17, '23,86');
            $worksheet->setColumn(18, 18, '33.14');
            $worksheet->setColumn(19, 19, '14.43');
            $worksheet->setColumn(20, 20, '32');
            $worksheet->setColumn(21, 21, '16.71');
            $worksheet->setColumn(22, 22, '26');
            $worksheet->setColumn(23, 23, '33.14');
            $worksheet->setColumn(0, 24, 30);
            $worksheet->setColumn(0, 25, 30);
            $worksheet->setColumn(0, 26, 30);

            $worksheet->write(0, 0, Prado::localize('TEXT_RAISON_SOCIALE'), $format_head);
            $worksheet->write(0, 1, Prado::localize('TEXT_ADRESSE'), $format_head);
            $worksheet->write(0, 2, Prado::localize('TEXT_ADRESSE_SUITE').' :', $format_head);
            $worksheet->write(0, 3, Prado::localize('TEXT_CP'), $format_head);
            $worksheet->write(0, 4, Prado::localize('TEXT_VILLE'), $format_head);
            $worksheet->write(0, 5, Prado::localize('TEXT_PAYS'), $format_head);
            $worksheet->write(0, 6, Prado::localize('DEFINE_TEXT_TEL_EA_PMI'), $format_head);
            $worksheet->write(0, 7, Prado::localize('TEXT_FAX'), $format_head);
            $worksheet->write(0, 8, Prado::localize('TEXT_SIREN')."\n/".Prado::localize('TEXT_SIRET_SIEGE').' :', $format_head);
            $worksheet->write(0, 9, Prado::localize('TEXT_FJ'), $format_head);
            $worksheet->write(0, 10, Prado::localize('TEXT_CODE_APE'), $format_head);
            $worksheet->write(0, 11, Prado::localize('ADRESSE_ELECTRONIQUE'), $format_head);
            $worksheet->write(0, 12, Prado::localize('CG76_TYPE_FORMATION').' :', $format_head);
            $worksheet->write(0, 13, Prado::localize('CG76_COUT_MOYEN_JOURNEE').' :', $format_head);
            $worksheet->write(0, 14, Prado::localize('CG76_COLLABORATION_FPT'), $format_head);
            $worksheet->write(0, 15, Prado::localize('CG76_COLLABORATION_FPE'), $format_head);
            $worksheet->write(0, 16, Prado::localize('CG76_CENTRE_DOCUMENTATION').' :', $format_head);
            $worksheet->write(0, 17, Prado::localize('CG76_SERVICE_REPROGRAPHIE').' :', $format_head);
            $worksheet->write(0, 18, Prado::localize('CG76_SALLE_INFO').' :', $format_head);
            $worksheet->write(0, 19, Prado::localize('CG76_SALLE_COURS').' :', $format_head);
            $worksheet->write(0, 20, Prado::localize('CG76_AIRE_GEO_INTER').' :', $format_head);
            $worksheet->write(0, 21, Prado::localize('DEFINE_COMMENTAIRE').' : na', $format_head);
            $worksheet->write(0, 22, Prado::localize('TEXT_DOMAINE').' :', $format_head);
            $worksheet->write(0, 23, Prado::localize('TEXT_SDOMAINE').' :', $format_head);
            $col = 24;
            $refsEntreprise = (new Atexo_Referentiel_Referentiel())->getAllLtReferentielForEntreprise();
            foreach ($refsEntreprise as $refEntreprise) {
                $libel = Prado::localize($refEntreprise->getCodeLibelle());
                $worksheet->write(0, $col++, $libel, $format_head);
            }
            $worksheet->write(0, $col, Prado::localize('MAILS_CONTACTS'), $format_head);

            $row = 1;
            foreach ($listEntreprises as $entreprise) {
                $col = 0;
                if (0 == $entreprise['Fpt']) {
                    $entreprise['Fpt'] = Prado::localize('DEFINE_NON');
                } else {
                    $entreprise['Fpt'] = Prado::localize('DEFINE_OUI');
                }
                if (0 == $entreprise['Fpe']) {
                    $entreprise['Fpe'] = Prado::localize('DEFINE_NON');
                } else {
                    $entreprise['Fpt'] = Prado::localize('DEFINE_OUI');
                }
                if (0 == $entreprise['CentreDoc']) {
                    $entreprise['CentreDoc'] = Prado::localize('DEFINE_NON');
                } else {
                    $entreprise['CentreDoc'] = Prado::localize('DEFINE_OUI');
                }
                if (0 == $entreprise['ServiceRepro']) {
                    $entreprise['ServiceRepro'] = Prado::localize('DEFINE_NON');
                } else {
                    $entreprise['ServiceRepro'] = Prado::localize('DEFINE_OUI');
                }
                if (0 == $entreprise['SalleInfo']) {
                    $entreprise['SalleInfo'] = Prado::localize('DEFINE_NON');
                } else {
                    $entreprise['SalleInfo'] = Prado::localize('DEFINE_OUI');
                }
                if (0 == $entreprise['SalleCours']) {
                    $entreprise['SalleCours'] = Prado::localize('DEFINE_NON');
                } else {
                    $entreprise['SalleCours'] = Prado::localize('DEFINE_OUI');
                }
                if ('' != $entreprise['Commentaire']) {
                    $comentaire = $entreprise['Commentaire'];
                } else {
                    $comentaire = 'n/a';
                }
                //Contenu du fichier Exel
                $worksheet->write($row, $col++, Atexo_Util::atexoHtmlEntitiesDecode($entreprise['RaisonSocial']), $format);
                $worksheet->write($row, $col++, $entreprise['Adresse'], $format);
                $worksheet->write($row, $col++, $entreprise['Adresse2'], $format);
                $worksheet->write($row, $col++, $entreprise['CodePostal'], $format);
                $worksheet->write($row, $col++, $entreprise['Ville'], $format);
                $worksheet->write($row, $col++, $entreprise['Pays'], $format);
                $worksheet->writeString($row, $col++, $entreprise['Tel'], $format);
                $worksheet->writeString($row, $col++, $entreprise['Fax'], $format);
                $worksheet->writeString($row, $col++, $entreprise['Siret'], $format);
                $worksheet->write($row, $col++, $entreprise['FormationJuridique'], $format);
                $worksheet->write($row, $col++, $entreprise['CodeApe'], $format);
                $worksheet->write($row, $col++, $entreprise['Email'], $format);
                $worksheet->write($row, $col++, $entreprise['TypeFormation'], $format);
                $worksheet->write($row, $col++, $entreprise['CoutMoyen'], $format);
                $worksheet->write($row, $col++, $entreprise['Fpt'], $format);
                $worksheet->write($row, $col++, $entreprise['Fpe'], $format);
                $worksheet->write($row, $col++, $entreprise['CentreDoc'], $format);
                $worksheet->write($row, $col++, $entreprise['ServiceRepro'], $format);
                $worksheet->write($row, $col++, $entreprise['SalleInfo'], $format);
                $worksheet->write($row, $col++, $entreprise['SalleCours'], $format);
                $worksheet->write($row, $col++, $entreprise['AireGeoInter'], $format);
                $worksheet->write($row, $col++, $comentaire, $format);
                $worksheet->write($row, $col++, $entreprise['Domaine'], $format);
                $worksheet->write($row, $col++, $entreprise['SousDomaine'], $format);
                foreach ($refsEntreprise as $ref) {
                    $entrepriseRef = (new Atexo_Referentiel_Referentiel())->getLtReferentielByEntreprise($entreprise['id'], $ref->getId());

                    if ($entrepriseRef) {
                        $value = $entrepriseRef->getValeurPrincipaleLtReferentiel().$entrepriseRef->getValeurSecondaireLtReferentiel();
                    } else {
                        $value = '';
                    }
                    $value = implode(';', explode('#', ltrim($value, '#')));
                    $worksheet->write($row, $col++, $value, $format);
                }
                $worksheet->write($row, $col++, $entreprise['ListMailsContacts'], $format);

                ++$row;
            }

            if ($toPdf) {
                $pathFichier = Atexo_Config::getParameter('COMMON_TMP').$nomFile;
                //echo "mv " . $tmpFileName . $pathFichier . " 2>&1 > ".Atexo_Config::getParameter('COMMON_TMP')."zip_error", $sys_answer; exit();
                system('mv '.$file." '".$pathFichier."' > ".Atexo_Config::getParameter('COMMON_TMP').'zip_error 2>&1');

                return $pathFichier;
            } else {
                $workbook->close();
                $workbook->send($nomFile);
                exit;
            }
        } catch (\Exception $e) {
            throw new Exception(Prado::localize('TEXT_ERREUR_EXPORT_EXCEL').'  '.$e->getMessage());
        }
    }

    /**
     * Genere un excel contenant la liste des agents
     *     => Genere la liste des agents pour un service donne en passant l'id du service en parametre
     *     => Genere la liste des agents pour l'organisme.
     *
     * @param array  $agents            : liste des agents avec leurs habilitations
     * @param string $acronymeOrganisme : acronyme de l'organisme
     * @param string $serviceId         : identifiant du service
     * @param bool   $toFile            : precise si la fonction retourne le chemin du fichier ou fait la generation a la volee
     * @param bool   $allAgents         : precise si la generation concerne tous les agents
     *
     * @author     Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version    1.0
     *
     * @since      4.11.0
     *
     * @copyright  Atexo 2015
     */
    public function generateExcelAgents($agents, $acronymeOrganisme, $serviceId, $toFile = false, $allAgents = true)
    {
        $langue = Atexo_CurrentUser::readFromSession('lang');
        $organisme = Atexo_Organismes::retrieveOrganismeByAcronyme($acronymeOrganisme);
        $nomOrganisme = '';
        if ($organisme instanceof CommonOrganisme) {
            $nomOrganisme = $organisme->getDenominationOrg();
        }

        $nomService = '';
        if ('0' === $serviceId || 0 === $serviceId) {
            $nomService = $nomOrganisme;
            if ($allAgents) {
                $listeService = (new Atexo_EntityPurchase())->getAllServices($acronymeOrganisme);
            }
        } else {
            $service = Atexo_EntityPurchase::retrieveEntityById($serviceId, $acronymeOrganisme);
            if ($service instanceof CommonService) {
                $nomService = $service->getLibelle();
                $listeService[$service->getId()] = $service;
            }
        }
        if ($allAgents) {
            $listeService = (new Atexo_EntityPurchase())->getAllServices($acronymeOrganisme);
        }
        (new Atexo_Config())->includePhpLibPath();
        require_once 'Spreadsheet/Excel/Writer.php';
        if ($toFile) {
            $tmpFileName = Atexo_Config::getParameter('COMMON_TMP').Prado::localize('TEXT_REGISTRE_HABILITATION').session_id().'_'.time().'.xls';
            $workbook = new Spreadsheet_Excel_Writer($tmpFileName);
        } else {
            $workbook = new Spreadsheet_Excel_Writer();
        }

        $workbook->setCustomColor(11, 255, 255, 150);
        $workbook->setCustomColor(12, 192, 192, 192);
        $workbook->setCustomColor(13, 0, 0, 255);
        $workbook->setCustomColor(14, 153, 204, 255);
        //Pour passer outre la limite des 255 caracteres
        $workbook->setVersion(8);

        //Style des entetes
        $stylePolice10GrasBleu = &$workbook->addFormat();
        $stylePolice10GrasBleu->setColor(13);
        $stylePolice10GrasBleu->setSize(10);
        $stylePolice10GrasBleu->setAlign('left');
        $stylePolice10GrasBleu->setBold(1);

        $stylePolice10BleuSimple = &$workbook->addFormat();
        $stylePolice10BleuSimple->setColor(13);
        $stylePolice10BleuSimple->setSize(10);
        $stylePolice10BleuSimple->setAlign('left');

        $styleSimplePolice10 = &$workbook->addFormat();
        $styleSimplePolice10->setColor('black');
        $styleSimplePolice10->setSize(10);
        $styleSimplePolice10->setAlign('left');

        $stylePolice8GrasBleu = &$workbook->addFormat();
        $stylePolice8GrasBleu->setColor(13);
        $stylePolice8GrasBleu->setSize(8);
        $stylePolice8GrasBleu->setAlign('left');
        $stylePolice8GrasBleu->setBold(1);

        $styleSimplePolice8 = &$workbook->addFormat();
        $styleSimplePolice8->setColor('black');
        $styleSimplePolice8->setSize(8);
        $styleSimplePolice8->setAlign('left');

        $styleSimplePolice8FondJaune = &$workbook->addFormat();
        $styleSimplePolice8FondJaune->setColor('black');
        $styleSimplePolice8FondJaune->setSize(8);
        $styleSimplePolice8FondJaune->setAlign('left');
        $styleSimplePolice8FondJaune->setFgColor(11);

        $styleSimplePolice8FondGris = &$workbook->addFormat();
        $styleSimplePolice8FondGris->setColor('black');
        $styleSimplePolice8FondGris->setSize(8);
        $styleSimplePolice8FondGris->setAlign('left');
        $styleSimplePolice8FondGris->setFgColor(12);

        $styleSimplePolice8FondBleu = &$workbook->addFormat();
        $styleSimplePolice8FondBleu->setColor('black');
        $styleSimplePolice8FondBleu->setSize(8);
        $styleSimplePolice8FondGris->setAlign('left');
        $styleSimplePolice8FondBleu->setFgColor(14);

        $worksheet = &$workbook->addWorkSheet(Atexo_Util::utf8ToIso(Prado::localize('TEXT_AGENTS')));
        if ('iso-8859-1' != Atexo_Config::getParameter('HTTP_ENCODING')) {
            $worksheet->setInputEncoding(Atexo_Config::getParameter('HTTP_ENCODING'));
        }

        $worksheet->hideGridlines();
        $nomFichier = Prado::localize('TEXT_REGISTRE_AGENTS_XLS');
        $worksheet->setHeader('&"Arial"&8'.$nomFichier.' - '.Prado::localize('DEFINE_HABILITATION'), '0.19');
        $worksheet->setFooter(self::getFooterImprimeLe(), '0.19');

        $worksheet->setMarginLeft('0.19');
        $worksheet->setMarginRight('0.19');
        $worksheet->setMarginTop('0.39');
        $worksheet->setMarginBottom('0.39');

        $worksheet->setColumn(0, 0, '3.14');
        $worksheet->setColumn(1, 1, '42.86');

        for ($i = 2; $i < 62; ++$i) {
            if (7 == $i) { // la colonne "Entite de rattachement (SIGLE / SIGLE / SIGLE - Libelle)"
                $worksheet->setColumn($i, $i, '69.86');
            } else {
                $worksheet->setColumn($i, $i, '42.86');
            }
        }

        $worksheet->write(0, 0, Prado::localize('TEXT_REGISTRE_AGENTS'), $stylePolice10GrasBleu);

        $worksheet->write(1, 0, Prado::localize('DEFINE_TEXT_ENTITE_PUBLIC').' :', $stylePolice10BleuSimple);
        $worksheet->write(1, 2, $nomOrganisme, $styleSimplePolice10);

        if ('0' !== $serviceId && 0 !== $serviceId) {
            $worksheet->write(2, 0, Prado::localize('TEXT_ENTITE_ACHAT').' :', $stylePolice10BleuSimple);
            $worksheet->write(2, 2, $nomService, $styleSimplePolice10);
        }

        if ($agents && is_array($agents)) {
            $colonne = 1;
            $row = 5;
            //Initialisation du fichier excel
            $worksheet->write(5, $colonne++, Prado::localize('DEFINE_NOM_MAJ'), $stylePolice8GrasBleu);
            $worksheet->write(5, $colonne++, Prado::localize('DEFINE_PRENOM'), $stylePolice8GrasBleu);
            $worksheet->write(5, $colonne++, Prado::localize('TEXT_TEL'), $stylePolice8GrasBleu);
            $worksheet->write(5, $colonne++, Prado::localize('TEXT_FAX'), $stylePolice8GrasBleu);
            $worksheet->write(5, $colonne++, Prado::localize('ADRESSE_ELECTRONIQUE'), $stylePolice8GrasBleu);
            $worksheet->write(5, $colonne++, Prado::localize('TEXT_IDENTIFIANT_SANS_POINTS'), $stylePolice8GrasBleu);
            $worksheet->write(5, $colonne++, Prado::localize('TEXT_ENTITE_RATTACHEMENT_SIGLE_SIGLE_LIBELLE'), $stylePolice8GrasBleu);
            $worksheet->write(5, $colonne++, Prado::localize('DATE_DERNIERE_CONNEXION'), $stylePolice8GrasBleu);
            $worksheet->write(5, $colonne++, Prado::localize('TEXT_ETAT_COMPTE'), $stylePolice8GrasBleu);
            $worksheet->write(3, $colonne++, Prado::localize('TEXT_LISTE_HABILITATION_LEGENDE'), $stylePolice8GrasBleu);
            $worksheet->write(3, $colonne++, Prado::localize('TEXT_GESTION_CONSULTATIONS'), $styleSimplePolice8FondJaune);
            ++$row;
            $worksheet->write(4, $colonne, Prado::localize('DEFINE_CREER_CONSULTATION'), $styleSimplePolice8FondGris);
            if (Atexo_Module::isEnabled('ProcedureAdaptee')) {
                $worksheet->write(5, $colonne++, Prado::localize('TEXT_MAPA_INFERIEUR_90000'), $styleSimplePolice8FondBleu);
                $worksheet->write(5, $colonne++, Prado::localize('TEXT_MAPA_SUPERIEUR_90000'), $styleSimplePolice8FondBleu);
            }
            $worksheet->write(5, $colonne++, Prado::localize('TEXT_PROCEDURES_FORMALISEES'), $styleSimplePolice8FondBleu);
            $worksheet->write(5, $colonne++, Prado::localize('TEXT_CREER_CONSULTATION_TRANSVERSE'), $styleSimplePolice8FondBleu);
            $worksheet->write(4, $colonne++, Prado::localize('GESTION_ENVOL'), $styleSimplePolice8FondGris);
            $worksheet->write(4, $colonne++, Prado::localize('DEFINE_CREER_SUITE'), $styleSimplePolice8FondGris);
            $worksheet->write(4, $colonne++, Prado::localize('TEXT_GERER_PUBLICITE'), $styleSimplePolice8FondGris);
            $worksheet->write(4, $colonne++, Prado::localize('DEFINE_ENVOYER_PUBLICITE'), $styleSimplePolice8FondGris);
            $worksheet->write(4, $colonne++, Prado::localize('TEXT_MODIFIER_CONSULTATION_AVANT_VALIDATION'), $styleSimplePolice8FondGris);
            $worksheet->write(4, $colonne++, Prado::localize('TEXT_VALIDATION_SIMPLE'), $styleSimplePolice8FondGris);
            $worksheet->write(4, $colonne++, Prado::localize('TEXT_VALIDATION_INTERMEDIAIRE'), $styleSimplePolice8FondGris);
            $worksheet->write(4, $colonne++, Prado::localize('TEXT_VALIDATION_FINALE'), $styleSimplePolice8FondGris);
            //$worksheet->write(4, $colonne++, Prado::localize('TEXT_MODIFIER_CONSULTATION_APRES_VALIDATION'), $styleSimplePolice8FondGris);
            if (Atexo_Module::isEnabled('ProcedureAdaptee')) {
                $worksheet->write(4, $colonne++, Prado::localize('TEXT_MODIFIER_CONSULTATION_MAPA_INFERIEUR_MONTANT_APRES_VALIDATION'), $styleSimplePolice8FondGris);
                $worksheet->write(4, $colonne++, Prado::localize('TEXT_MODIFIER_CONSULTATION_MAPA_SUPERIEUR_MONTANT_APRES_VALIDATION'), $styleSimplePolice8FondGris);
            }
            $worksheet->write(4, $colonne++, Prado::localize('TEXT_MODIFIER_CONSULTATION_PROCEDURES_FORMALISEES_APRES_VALIDATION'), $styleSimplePolice8FondGris);
            $worksheet->write(4, $colonne++, Prado::localize('TEXT_GERER_REGISTRE_RETRAITS'), $styleSimplePolice8FondGris);
            $worksheet->write(4, $colonne++, Prado::localize('TEXT_GERER_REGISTRE_QUESTIONS'), $styleSimplePolice8FondGris);
            $worksheet->write(4, $colonne++, Prado::localize('TEXT_GERER_REGISTRE_DEPOTS'), $styleSimplePolice8FondGris);
            $worksheet->write(4, $colonne++, Prado::localize('TEXT_SUIVI_SEUL_REGISTRE_RETRAITS'), $styleSimplePolice8FondGris);
            $worksheet->write(4, $colonne++, Prado::localize('TEXT_SUIVI_SEUL_REGISTRE_QUESTIONS'), $styleSimplePolice8FondGris);
            $worksheet->write(4, $colonne++, Prado::localize('TEXT_SUIVI_SEUL_REGISTRE_DEPOTS'), $styleSimplePolice8FondGris);

            $worksheet->write(4, $colonne++, Prado::localize('TEXT_ACCES_REPONSES'), $styleSimplePolice8FondGris);
            $worksheet->write(4, $colonne++, Prado::localize('TEXT_TELECHARGEMENT_GROUPE_ANTICIPE_PLIS_CHIFFRES'), $styleSimplePolice8FondGris);
            $worksheet->write(4, $colonne++, Prado::localize('TEXT_TELECHARGEMENT_UNITAIRES_PLIS_CHIFFRES'), $styleSimplePolice8FondGris);
            $worksheet->write(4, $colonne++, Prado::localize('TEXT_OUVERTURE_CONDIDATURE_LIGNES'), $styleSimplePolice8FondGris);
            if (Atexo_Crypto::isModeAppletAgentActive()) {
                $worksheet->write(4, $colonne++, Prado::localize('TEXT_OUVERTURE_CONDIDATURE_DISTANCE'), $styleSimplePolice8FondGris);
            }
            $worksheet->write(4, $colonne++, Prado::localize('TEXT_OUVERTURE_CANDIDATURE_HORS_LIGNE'), $styleSimplePolice8FondGris);
            $worksheet->write(4, $colonne++, Prado::localize('TEXT_ACCES_REPONSES'), $styleSimplePolice8FondGris);
            $worksheet->write(4, $colonne++, Prado::localize('TEXT_OUVERTURE_OFFRES_LIGNE'), $styleSimplePolice8FondGris);
            if (Atexo_Crypto::isModeAppletAgentActive()) {
                $worksheet->write(4, $colonne++, Prado::localize('TEXT_OUVERTURE_OFFRES_DISTANCE'), $styleSimplePolice8FondGris);
            }
            $worksheet->write(4, $colonne++, Prado::localize('TEXT_OUVERTURE_OFFRES_HORS_LIGNE'), $styleSimplePolice8FondGris);
            if (Atexo_Module::isEnabled('EnveloppeOffreTechnique')) {
                $worksheet->write(4, $colonne++, Prado::localize('TEXT_OUVERTURE_OFFRES_TECHNIQUE_EN_LIGNE'), $styleSimplePolice8FondGris);
                if (Atexo_Crypto::isModeAppletAgentActive()) {
                    $worksheet->write(4, $colonne++, Prado::localize('TEXT_OUVERTURE_OFFRES_TECHNIQUE_A_DISTANCE'), $styleSimplePolice8FondGris);
                }
                $worksheet->write(4, $colonne++, Prado::localize('TEXT_OUVERTURE_OFFRES_TECHNIQUE_HORS_LIGNE'), $styleSimplePolice8FondGris);
            }
            if (Atexo_Module::isEnabled('EnveloppeAnonymat')) {
                $worksheet->write(4, $colonne++, Prado::localize('TEXT_OUVRIR_ENVELOPPES_ANONYMES_LIGNE'), $styleSimplePolice8FondGris);
                if (Atexo_Crypto::isModeAppletAgentActive()) {
                    $worksheet->write(4, $colonne++, Prado::localize('TEXT_OUVRIR_ENVELOPPES_ANONYMES_DSITANCE'), $styleSimplePolice8FondGris);
                }
                $worksheet->write(4, $colonne++, Prado::localize('TEXT_OUVRIR_ENVELOPPES_ANONYMES_HORS_LIGNE'), $styleSimplePolice8FondGris);
            }
            $worksheet->write(4, $colonne++, Prado::localize('REFUSER_ENVELOPPE'), $styleSimplePolice8FondGris);
            $worksheet->write(4, $colonne++, Prado::localize('RESTAURER_ENVELOPPE'), $styleSimplePolice8FondGris);
            $worksheet->write(4, $colonne++, Prado::localize('GERER_ADMISSIBILITE'), $styleSimplePolice8FondGris);
            $worksheet->write(4, $colonne++, Prado::localize('IMPORTER_ENVELOPPE'), $styleSimplePolice8FondGris);
            $worksheet->write(4, $colonne++, Prado::localize('TEXT_ENVOYER_MESSAGE'), $styleSimplePolice8FondGris);
            $worksheet->write(4, $colonne++, Prado::localize('TEXT_SUIVI_MESSAGES'), $styleSimplePolice8FondGris);
            $worksheet->write(4, $colonne++, Prado::localize('DEFINE_ACCES_HISTORIQUE_ENTREPRISES'), $styleSimplePolice8FondGris);
            if (Atexo_Module::isEnabled('EncheresEntreprise')) {
                $worksheet->write(4, $colonne++, Prado::localize('TEXT_DEFINIR_ENCHERE'), $styleSimplePolice8FondGris);
                $worksheet->write(4, $colonne++, Prado::localize('TEXT_SUIVRE_ENCHERE'), $styleSimplePolice8FondGris);
            }
            $worksheet->write(4, $colonne++, Prado::localize('TEXT_RENSEIGNER_DECISION'), $styleSimplePolice8FondGris);
            $worksheet->write(4, $colonne++, Prado::localize('DEFINE_SUIVI_SEUL_LA_DECISION'), $styleSimplePolice8FondGris);

            $worksheet->write(4, $colonne++, Prado::localize('TEXT_INVITE_PERMANENT_MON_ENTITE'), $styleSimplePolice8FondGris);
            $worksheet->write(4, $colonne++, Prado::localize('TEXT_INVITE_PERMANENT_ENTITE_DEPENDANTE'), $styleSimplePolice8FondGris);
            $worksheet->write(4, $colonne++, Prado::localize('TEXT_INVITE_PERMANENT_TRANSVERSE'), $styleSimplePolice8FondGris);

            if (Atexo_Module::isEnabled('ResultatAnalyseAvantDecision')) {
                $worksheet->write(4, $colonne++, Prado::localize('RESULTAT_ANALYSE'), $styleSimplePolice8FondGris);
            }
            if (Atexo_Module::isEnabled('AnnulerConsultation')) {
                $worksheet->write(4, $colonne++, Prado::localize('ANNULER_CONSULTATION'), $styleSimplePolice8FondGris);
            }

            if (Atexo_Module::isEnabled('InterfaceModuleRsem')) {
                $worksheet->write(3, $colonne++, Prado::localize('TEXTE_REDACTION_PIECES_MARCHE'), $styleSimplePolice8FondJaune);
                $worksheet->write(4, $colonne++, Prado::localize('TEXT_REPRENDRE_INTEGRALEMENT_ARTICLE'), $styleSimplePolice8FondGris);
                $worksheet->write(4, $colonne++, Prado::localize('TEXT_REDACTION_PIECES'), $styleSimplePolice8FondGris);
                $worksheet->write(4, $colonne++, Prado::localize('TEXT_VALIDER_PIECES'), $styleSimplePolice8FondGris);
                $worksheet->write(4, $colonne++, Prado::localize('TEXT_ADMINISTER_CLAUSES_ENTITE_ACHAT'), $styleSimplePolice8FondGris);
                $worksheet->write(4, $colonne++, Prado::localize('TEXT_GENERER_PIECES_FORMAT_ODT'), $styleSimplePolice8FondGris);
                $worksheet->write(4, $colonne++, Prado::localize('ADMINISTRER_LES_GABARITS_PLATEFORME'), $styleSimplePolice8FondGris);
                $worksheet->write(4, $colonne++, Prado::localize('ADMINISTRER_LES_GABARITS_ENTITE_PUBLIQUE'), $styleSimplePolice8FondGris);
                $worksheet->write(4, $colonne++, Prado::localize('ADMINISTRER_LES_GABARITS_ENTITE_ACHAT'), $styleSimplePolice8FondGris);
                $worksheet->write(4, $colonne++, Prado::localize('ADMINISTRER_LES_GABARITS_AGENT'), $styleSimplePolice8FondGris);
            }

            $worksheet->write(3, $colonne++, Prado::localize('GESTION_BASE_DONNEES_FOURNISSEURS'), $styleSimplePolice8FondJaune);
            $worksheet->write(4, $colonne++, Prado::localize('TEXT_VISUALISER_ENTREPRISES'), $styleSimplePolice8FondGris);
            if (Atexo_Module::isEnabled('GestionEntrepriseParAgent')) {
                $worksheet->write(4, $colonne++, Prado::localize('GERER_LES_FOURNISSEURS'), $styleSimplePolice8FondGris);
            }
            if (Atexo_Module::isEnabled('AnnuaireAcheteursPublics')) {
                $worksheet->write(4, $colonne++, Prado::localize('ANNUAIRE_ACHETEUR_PUBLIC'), $styleSimplePolice8FondGris);
            }

            $worksheet->write(3, $colonne++, Prado::localize('TEXT_GESTION_AUTRES_ANNONCES'), $styleSimplePolice8FondJaune);
            $worksheet->write(4, $colonne++, Prado::localize('TEXT_CREER_ANNONCE_INFORMATION'), $styleSimplePolice8FondGris);
            $worksheet->write(4, $colonne++, Prado::localize('TEXT_CREER_ANNONCE_ATTRIBUTION'), $styleSimplePolice8FondGris);
            if (Atexo_Module::isEnabled('AutreAnnonceExtraitPv')) {
                $worksheet->write(4, $colonne++, Prado::localize('TEXT_CREER_ANNONCE_EXTRAIT_PV'), $styleSimplePolice8FondGris);
            }
            if (Atexo_Module::isEnabled('AutreAnnonceRapportAchevement')) {
                $worksheet->write(4, $colonne++, Prado::localize('TEXT_CREER_ANNONCE_RAPPORT_ACHEVEMENT'), $styleSimplePolice8FondGris);
            }
            if (Atexo_Module::isEnabled('AutreAnnonceProgrammePrevisionnel')) {
                $worksheet->write(4, $colonne++, Prado::localize('TEXT_CREER_ANNONCE_PROGRAMME_PREVISIONNEL'), $styleSimplePolice8FondGris);
            }
            $worksheet->write(3, $colonne++, Prado::localize('TEXT_ADMINISTRATION_METIER_ENTITE_ACHAT'), $styleSimplePolice8FondJaune);
            $worksheet->write(4, $colonne++, Prado::localize('TEXT_VISUALISER_ENTREPRISES'), $styleSimplePolice8FondGris);
            if (Atexo_Module::isEnabled('GestionEntrepriseParAgent')) {
                $worksheet->write(4, $colonne++, Prado::localize('GERER_LES_FOURNISSEURS'), $styleSimplePolice8FondGris);
            }
            $worksheet->write(4, $colonne++, Prado::localize('TEXT_SUPPRIMER_PLIS_REFUSES'), $styleSimplePolice8FondGris);
            if (Atexo_Module::isEnabled('Article133GenerationPf')) {
                $worksheet->write(4, $colonne, Prado::localize('TEXT_LISTE_MARCHES_133'), $styleSimplePolice8FondGris);
                $worksheet->write(5, $colonne++, Prado::localize('TEXT_PUBLICATION'), $styleSimplePolice8FondBleu);
            }
            if (Atexo_Module::isEnabled('Article133GenerationPf') && Atexo_Module::isEnabled('ExportMarchesNotifies', Atexo_CurrentUser::getCurrentOrganism())) {
                $worksheet->write(5, $colonne++, Prado::localize('LISTE_MARCHES_NOTIFIES'), $styleSimplePolice8FondBleu);
            }
            $worksheet->write(4, $colonne++, Prado::localize('TEXT_ACCEDER_STAT_METIER'), $styleSimplePolice8FondGris);
            if (Atexo_Module::isEnabled('InterfaceChorusPmi')) {
                $worksheet->write(4, $colonne++, Prado::localize('DEFINE_SUIVI_FLUX_CHORUS_TRANSVERSAUX'), $styleSimplePolice8FondGris);
            }
            $worksheet->write(3, $colonne++, Prado::localize('TEXT_ARCHIVES'), $styleSimplePolice8FondJaune);
            $worksheet->write(4, $colonne++, Prado::localize('TEXT_GERER_ARCHIVES'), $styleSimplePolice8FondGris);
            if (Atexo_Module::isEnabled('ArchivageConsultationSurPf')) {
                $worksheet->write(4, $colonne++, Prado::localize('TELECHARGEMENT_DES_ARCHIVES'), $styleSimplePolice8FondGris);
            }

            $worksheet->write(4, $colonne++, Prado::localize('TEXT_ACCEDER_TOUS_TELECHARGEMENTS'), $styleSimplePolice8FondGris);
            $worksheet->write(3, $colonne++, Prado::localize('TEXT_ADMINISTRATION_PMI_EA'), $styleSimplePolice8FondJaune);

            if (Atexo_Module::isEnabled('GererMonService')) {
                $worksheet->write(4, $colonne++, Prado::localize('GERER_MON_SERVICE'), $styleSimplePolice8FondGris);
            }
            $worksheet->write(4, $colonne++, Prado::localize('TEXT_GERER_EA'), $styleSimplePolice8FondGris);
            $worksheet->write(4, $colonne++, Prado::localize('TEXT_GERER_AGENTS'), $styleSimplePolice8FondGris);
            $worksheet->write(4, $colonne++, Prado::localize('TEXT_GERER_HABILITATIONS'), $styleSimplePolice8FondGris);
            if (Atexo_Module::isEnabled('PubliciteFormatXml')) {
                $worksheet->write(4, $colonne++, Prado::localize('TEXT_GERER_CPT_BOAMP'), $styleSimplePolice8FondGris);
                $worksheet->write(4, $colonne++, Prado::localize('TEXT_GERER_COMPTES_MONITEUR'), $styleSimplePolice8FondGris);
            }
            if (Atexo_Module::isEnabled('GererAdressesService')) {
                $worksheet->write(4, $colonne++, Prado::localize('PARAMETRAGES_ADRESSES'), $styleSimplePolice8FondGris);
            }
            if (Atexo_Module::isEnabled('GererCertificatsAgent')) {
                $worksheet->write(4, $colonne++, Prado::localize('DEFINE_GERER_CERTIFICAT_AGENT'), $styleSimplePolice8FondGris);
            }
            if (Atexo_Module::isEnabled('GestionMandataire')) {
                $worksheet->write(4, $colonne++, Prado::localize('DEFINE_GERER_MANDATAIRE'), $styleSimplePolice8FondGris);
            }
            $worksheet->write(4, $colonne++, Prado::localize('GERER_COMPTES_JAL'), $styleSimplePolice8FondGris);
            $worksheet->write(4, $colonne++, Prado::localize('TEXT_DEFINE_GERER_CLES_CHIFFREMENT'), $styleSimplePolice8FondGris);
            $worksheet->write(4, $colonne++, Prado::localize('TEXT_FOURNISSEUR_DOC'), $styleSimplePolice8FondGris);
            if (Atexo_Module::isEnabled('CentralePublication')) {
                $worksheet->write(4, $colonne++, Prado::localize('TEXT_GERER_COMPTE_CENTRALES_PUBLICATION'), $styleSimplePolice8FondGris);
            }
            if (Atexo_Module::isEnabled('GestionNewsletter')) {
                $worksheet->write(3, $colonne, Prado::localize('TEXT_GESTION_NEWSLETTER'), $styleSimplePolice8FondJaune);
                $worksheet->write(4, $colonne++, '', $styleSimplePolice8);
                $worksheet->write(4, $colonne++, Prado::localize('TEXT_GERER_NEWSLETTER'), $styleSimplePolice8FondGris);
                if (Atexo_Module::isEnabled('InterfaceModuleRsem')) {
                    $worksheet->write(4, $colonne++, Prado::localize('TEXT_GERER_NEWSLETTER_REDAC'), $styleSimplePolice8FondGris);
                }
            }
            if (Atexo_Module::isEnabled('MenuAgentSocietesExclues')) {
                $worksheet->write(3, $colonne, Prado::localize('TEXT_SOCIETES_EXCLUES'), $styleSimplePolice8FondJaune);
                $worksheet->write(4, $colonne++, '', $styleSimplePolice8);

                $worksheet->write(4, $colonne++, Prado::localize('TEXT_CREER_SOCIETES_EXCLUES'), $styleSimplePolice8FondGris);
                $worksheet->write(4, $colonne++, Prado::localize('TEXT_MODIFIER_SOCIETES_EXCLUES'), $styleSimplePolice8FondGris);
                $worksheet->write(4, $colonne++, Prado::localize('TEXT_SUPPRIMER_SOCIETES_EXCLUES'), $styleSimplePolice8FondGris);
                $worksheet->write(4, $colonne++, Prado::localize('TEXT_PORTEE_SOCIETES_EXCLUES_TOUS_ORGANISMES'), $styleSimplePolice8FondGris);
                $worksheet->write(4, $colonne++, Prado::localize('TEXT_GESTION_ADRESSE_FACTURATION_JAL'), $styleSimplePolice8FondGris);
            }

            $worksheet->write(3, $colonne, Prado::localize('GESTION_RECEMENT_PROGRAMMATION_STRATEGIE'), $styleSimplePolice8FondJaune);
            $worksheet->write(4, $colonne++, '', $styleSimplePolice8);

            $worksheet->write(4, $colonne++, Prado::localize('TEXT_VALIDER_PROJET_ACHAT'), $styleSimplePolice8FondGris);

            // la feuille (konoha)
            $row = 6;
            foreach ($agents as $agent) {
                $colonne = 1;
                $entiteRattachement = '';

                //$entiteRattachement=Atexo_EntityPurchase::getPathEntityById($agent->getServiceId(), $acronymeOrganisme);
                if (0 != $agent->getServiceId()) {
                    $entity = $listeService[$agent->getServiceId()];
                    if ($entity) {
                        $entiteRattachement = $entity->getPathServiceTraduit();
                    }
                } elseif (!empty($organisme)) {
                    $getDenominationOrg = 'getDenominationOrg'.Atexo_Languages::getLanguageAbbreviation($langue);
                    if (0 == strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE'))
                           || !$organisme->$getDenominationOrg()) {
                        $entiteRattachement = $organisme->getSigle().' - '.$organisme->getDenominationOrg();
                    } else {
                        $entiteRattachement = $organisme->getSigle().' - '.$organisme->$getDenominationOrg();
                    }
                }

                $worksheet->setRow($row, '');

                $worksheet->write($row, $colonne++, $agent->getNom(), $styleSimplePolice8);
                $worksheet->write($row, $colonne++, $agent->getPrenom(), $styleSimplePolice8);
                $worksheet->writeString($row, $colonne++, $agent->getNumTel(), $styleSimplePolice8);
                $worksheet->writeString($row, $colonne++, $agent->getNumFax(), $styleSimplePolice8);
                $worksheet->write($row, $colonne++, $agent->getEmail(), $styleSimplePolice8);
                $worksheet->write($row, $colonne++, $agent->getLogin(), $styleSimplePolice8);
                $worksheet->write($row, $colonne++, $entiteRattachement, $styleSimplePolice8);
                $worksheet->write($row, $colonne++, $agent->getDateConnexion('d/m/Y H:i:s'), $styleSimplePolice8);
                $etatCompte = $agent->getActif() ? Prado::localize('TEXT_ACTIF') : Prado::localize('DEFINE_DESACTIVE');
                $worksheet->write($row, $colonne++, $etatCompte, $styleSimplePolice8);
                if ($agent->getCommonHabilitationAgent() instanceof CommonHabilitationAgent) {
                    $habilitationsAgent = $agent->getCommonHabilitationAgent();
                    //$worksheet->write($row, $colonne++, "", $styleSimplePolice8);
                    //$worksheet->write($row, 8, 'LISTE_HABILITATION_FIXM', $styleSimplePolice8);
                    $colonne += 2;
                    if (Atexo_Module::isEnabled('ProcedureAdaptee')) {
                        $worksheet->write($row, $colonne++, $habilitationsAgent->getGererMapaInferieurMontant(), $styleSimplePolice8);
                        $worksheet->write($row, $colonne++, $habilitationsAgent->getGererMapaSuperieurMontant(), $styleSimplePolice8);
                    }
                    $worksheet->write($row, $colonne++, $habilitationsAgent->getAdministrerProceduresFormalisees(), $styleSimplePolice8);
                    $worksheet->write($row, $colonne++, $habilitationsAgent->getCreerConsultationTransverse(), $styleSimplePolice8);
                    $worksheet->write($row, $colonne++, (($habilitationsAgent->getGestionEnvol()) ? 1 : 0), $styleSimplePolice8);
                    $worksheet->write($row, $colonne++, $habilitationsAgent->getCreerSuiteConsultation(), $styleSimplePolice8);
                    $worksheet->write($row, $colonne++, $habilitationsAgent->getPublierConsultation(), $styleSimplePolice8);
                    $worksheet->write($row, $colonne++, $habilitationsAgent->getEnvoyerPublicite(), $styleSimplePolice8);
                    $worksheet->write($row, $colonne++, $habilitationsAgent->getModifierConsultationAvantValidation(), $styleSimplePolice8);
                    $worksheet->write($row, $colonne++, $habilitationsAgent->getValidationSimple(), $styleSimplePolice8);
                    $worksheet->write($row, $colonne++, $habilitationsAgent->getValidationIntermediaire(), $styleSimplePolice8);
                    $worksheet->write($row, $colonne++, $habilitationsAgent->getValidationFinale(), $styleSimplePolice8);
                    //$worksheet->write($row, $colonne++, $habilitationsAgent->getModifierConsultationApresValidation(), $styleSimplePolice8);
                    if (Atexo_Module::isEnabled('ProcedureAdaptee')) {
                        $worksheet->write($row, $colonne++, $habilitationsAgent->getModifierConsultationMapaInferieurMontantApresValidation(), $styleSimplePolice8);
                        $worksheet->write($row, $colonne++, $habilitationsAgent->getModifierConsultationMapaSuperieurMontantApresValidation(), $styleSimplePolice8);
                    }
                    $worksheet->write($row, $colonne++, $habilitationsAgent->getModifierConsultationProceduresFormaliseesApresValidation(), $styleSimplePolice8);
                    $worksheet->write($row, $colonne++, $habilitationsAgent->getAccesRegistreRetraitsPapier(), $styleSimplePolice8);
                    $worksheet->write($row, $colonne++, $habilitationsAgent->getAccesRegistreQuestionsPapier(), $styleSimplePolice8);
                    $worksheet->write($row, $colonne++, $habilitationsAgent->getAccesRegistreDepotsPapier(), $styleSimplePolice8);
                    $worksheet->write($row, $colonne++, $habilitationsAgent->getSuiviSeulRegistreRetraitsPapier(), $styleSimplePolice8);
                    $worksheet->write($row, $colonne++, $habilitationsAgent->getSuiviSeulRegistreQuestionsPapier(), $styleSimplePolice8);
                    $worksheet->write($row, $colonne++, $habilitationsAgent->getSuiviSeulRegistreDepotsPapier(), $styleSimplePolice8);
                    $worksheet->write($row, $colonne++, $habilitationsAgent->getAccesReponses(), $styleSimplePolice8);
                    $worksheet->write($row, $colonne++, $habilitationsAgent->getTelechargementGroupeAnticipePlisChiffres(), $styleSimplePolice8);
                    $worksheet->write($row, $colonne++, $habilitationsAgent->getTelechargementUnitairePlisChiffres(), $styleSimplePolice8);
                    $worksheet->write($row, $colonne++, $habilitationsAgent->getOuvrirCandidatureEnLigne(), $styleSimplePolice8);
                    if (Atexo_Crypto::isModeAppletAgentActive()) {
                        $worksheet->write($row, $colonne++, $habilitationsAgent->getOuvrirCandidatureADistance(), $styleSimplePolice8);
                    }
                    $worksheet->write($row, $colonne++, $habilitationsAgent->getOuvrirCandidatureHorsLigne(), $styleSimplePolice8);
                    $worksheet->write($row, $colonne++, $habilitationsAgent->getAccesReponses(), $styleSimplePolice8);
                    $worksheet->write($row, $colonne++, $habilitationsAgent->getOuvrirOffreEnLigne(), $styleSimplePolice8);
                    if (Atexo_Crypto::isModeAppletAgentActive()) {
                        $worksheet->write($row, $colonne++, $habilitationsAgent->getOuvrirOffreADistance(), $styleSimplePolice8);
                    }
                    $worksheet->write($row, $colonne++, $habilitationsAgent->getOuvrirOffreHorsLigne(), $styleSimplePolice8);
                    if (Atexo_Module::isEnabled('EnveloppeOffreTechnique')) {
                        $worksheet->write($row, $colonne++, $habilitationsAgent->getOuvrirOffreTechniqueEnLigne(), $styleSimplePolice8);
                        if (Atexo_Crypto::isModeAppletAgentActive()) {
                            $worksheet->write($row, $colonne++, $habilitationsAgent->getOuvrirOffreTechniqueADistance(), $styleSimplePolice8);
                        }
                        $worksheet->write($row, $colonne++, $habilitationsAgent->getOuvrirOffreTechniqueHorsLigne(), $styleSimplePolice8);
                    }
                    if (Atexo_Module::isEnabled('EnveloppeAnonymat')) {
                        $worksheet->write($row, $colonne++, $habilitationsAgent->getOuvrirAnonymatEnLigne(), $styleSimplePolice8);
                        if (Atexo_Crypto::isModeAppletAgentActive()) {
                            $worksheet->write($row, $colonne++, $habilitationsAgent->getOuvrirAnonymatADistance(), $styleSimplePolice8);
                        }
                        $worksheet->write($row, $colonne++, $habilitationsAgent->getOuvrirAnonymatHorsLigne(), $styleSimplePolice8);
                    }
                    $worksheet->write($row, $colonne++, $habilitationsAgent->getRefuserEnveloppe(), $styleSimplePolice8);
                    $worksheet->write($row, $colonne++, $habilitationsAgent->getRestaurerEnveloppe(), $styleSimplePolice8);
                    $worksheet->write($row, $colonne++, $habilitationsAgent->getGererAdmissibilite(), $styleSimplePolice8);
                    $worksheet->write($row, $colonne++, $habilitationsAgent->getImporterEnveloppe(), $styleSimplePolice8);
                    $worksheet->write($row, $colonne++, $habilitationsAgent->getEnvoyerMessage(), $styleSimplePolice8);
                    $worksheet->write($row, $colonne++, $habilitationsAgent->getSuivreMessage(), $styleSimplePolice8);
                    $worksheet->write($row, $colonne++, $habilitationsAgent->getHistoriqueNavigationInscrits(), $styleSimplePolice8);

                    if (Atexo_Module::isEnabled('EncheresEntreprise')) {
                        $worksheet->write($row, $colonne++, $habilitationsAgent->getGererEncheres(), $styleSimplePolice8);
                        $worksheet->write($row, $colonne++, $habilitationsAgent->getSuivreEncheres(), $styleSimplePolice8);
                    }
                    $worksheet->write($row, $colonne++, $habilitationsAgent->getAttributionMarche(), $styleSimplePolice8);
                    $worksheet->write($row, $colonne++, $habilitationsAgent->getDecisionSuiviSeul(), $styleSimplePolice8);

                    $worksheet->write($row, $colonne++, $habilitationsAgent->getInvitePermanentMonEntite(), $styleSimplePolice8);
                    $worksheet->write($row, $colonne++, $habilitationsAgent->getInvitePermanentEntiteDependante(), $styleSimplePolice8);
                    $worksheet->write($row, $colonne++, $habilitationsAgent->getInvitePermanentTransverse(), $styleSimplePolice8);

                    if (Atexo_Module::isEnabled('ResultatAnalyseAvantDecision')) {
                        $worksheet->write($row, $colonne++, $habilitationsAgent->getResultatAnalyse(), $styleSimplePolice8);
                    }
                    if (Atexo_Module::isEnabled('AnnulerConsultation')) {
                        $worksheet->write($row, $colonne++, $habilitationsAgent->getAnnulerConsultation(), $styleSimplePolice8);
                    }

                    if (Atexo_Module::isEnabled('InterfaceModuleRsem')) {
                        $worksheet->write($row, $colonne++, '', $styleSimplePolice8);
                        $worksheet->write($row, $colonne++, $habilitationsAgent->getReprendreIntegralementArticle(), $styleSimplePolice8);
                        $worksheet->write($row, $colonne++, $habilitationsAgent->getRedactionDocumentsRedac(), $styleSimplePolice8);
                        $worksheet->write($row, $colonne++, $habilitationsAgent->getValidationDocumentsRedac(), $styleSimplePolice8);
                        $worksheet->write($row, $colonne++, $habilitationsAgent->getAdministrerClausesEntiteAchats(), $styleSimplePolice8);
                        $worksheet->write($row, $colonne++, $habilitationsAgent->getGenererPiecesFormatOdt(), $styleSimplePolice8);
                        $worksheet->write($row, $colonne++, $habilitationsAgent->getGererGabaritEditeur(), $styleSimplePolice8);
                        $worksheet->write($row, $colonne++, $habilitationsAgent->getGererGabarit(), $styleSimplePolice8);
                        $worksheet->write($row, $colonne++, $habilitationsAgent->getGererGabaritEntiteAchats(), $styleSimplePolice8);
                        $worksheet->write($row, $colonne++, $habilitationsAgent->getGererGabaritAgent(), $styleSimplePolice8);
                    }
                    $worksheet->write($row, $colonne++, '', $styleSimplePolice8);
                    $worksheet->write($row, $colonne++, $habilitationsAgent->getSuiviEntreprise(), $styleSimplePolice8);
                    if (Atexo_Module::isEnabled('GestionEntrepriseParAgent')) {
                        $worksheet->write($row, $colonne++, $habilitationsAgent->getGererLesEntreprises(), $styleSimplePolice8);
                    }
                    if (Atexo_Module::isEnabled('AnnuaireAcheteursPublics')) {
                        $worksheet->write($row, $colonne++, $habilitationsAgent->getAnnuaireAcheteur(), $styleSimplePolice8);
                    }
                    $worksheet->write($row, $colonne++, '', $styleSimplePolice8);
                    $worksheet->write($row, $colonne++, $habilitationsAgent->getCreerAnnonceInformation(), $styleSimplePolice8);
                    $worksheet->write($row, $colonne++, $habilitationsAgent->getCreerAnnonceAttribution(), $styleSimplePolice8);
                    if (Atexo_Module::isEnabled('AutreAnnonceExtraitPv')) {
                        $worksheet->write($row, $colonne++, $habilitationsAgent->getCreerAnnonceExtraitPv(), $styleSimplePolice8);
                    }
                    if (Atexo_Module::isEnabled('AutreAnnonceRapportAchevement')) {
                        $worksheet->write($row, $colonne++, $habilitationsAgent->getCreerAnnonceRapportAchevement(), $styleSimplePolice8);
                    }
                    if (Atexo_Module::isEnabled('AutreAnnonceProgrammePrevisionnel')) {
                        $worksheet->write($row, $colonne++, $habilitationsAgent->getCreerAvisProgrammePrevisionnel(), $styleSimplePolice8);
                    }
                    $worksheet->write($row, $colonne++, '', $styleSimplePolice8);
                    $worksheet->write($row, $colonne++, $habilitationsAgent->getSuiviEntreprise(), $styleSimplePolice8);
                    if (Atexo_Module::isEnabled('GestionEntrepriseParAgent')) {
                        $worksheet->write($row, $colonne++, $habilitationsAgent->getGererLesEntreprises(), $styleSimplePolice8);
                    }
                    $worksheet->write($row, $colonne++, $habilitationsAgent->getSupprimerEnveloppe(), $styleSimplePolice8);
                    if (Atexo_Module::isEnabled('Article133GenerationPf')) {
                        $worksheet->write($row, $colonne++, $habilitationsAgent->getPublicationMarches(), $styleSimplePolice8);
                    }
                    if (Atexo_Module::isEnabled('Article133GenerationPf') && Atexo_Module::isEnabled('ExportMarchesNotifies', Atexo_CurrentUser::getCurrentOrganism())) {
                        $worksheet->write($row, $colonne++, $habilitationsAgent->getListeMarchesNotifies(), $styleSimplePolice8);
                    }
                    $worksheet->write($row, $colonne++, $habilitationsAgent->getGererStatistiquesMetier(), $styleSimplePolice8);
                    if (Atexo_Module::isEnabled('InterfaceChorusPmi')) {
                        $worksheet->write($row, $colonne++, $habilitationsAgent->getSuiviFluxChorusTransversal(), $styleSimplePolice8);
                    }
                    $worksheet->write($row, $colonne++, '', $styleSimplePolice8);
                    $worksheet->write($row, $colonne++, $habilitationsAgent->getGererArchives(), $styleSimplePolice8);
                    if (Atexo_Module::isEnabled('ArchivageConsultationSurPf')) {
                        $worksheet->write($row, $colonne++, $habilitationsAgent->getDownloadArchives(), $styleSimplePolice8);
                    }
                    $worksheet->write($row, $colonne++, $habilitationsAgent->getAccederTousTelechargements(), $styleSimplePolice8);
                    $worksheet->write($row, $colonne++, '', $styleSimplePolice8);

                    if (Atexo_Module::isEnabled('GererMonService')) {
                        $worksheet->write($row, $colonne++, $habilitationsAgent->getGererMonService(), $styleSimplePolice8);
                    }
                    $worksheet->write($row, $colonne++, $habilitationsAgent->getGestionAgentPole(), $styleSimplePolice8);
                    $worksheet->write($row, $colonne++, $habilitationsAgent->getGestionAgents(), $styleSimplePolice8);
                    $worksheet->write($row, $colonne++, $habilitationsAgent->getGestionHabilitations(), $styleSimplePolice8);
                    if (Atexo_Module::isEnabled('PubliciteFormatXml')) {
                        $worksheet->write($row, $colonne++, $habilitationsAgent->getGestionCompteBoamp(), $styleSimplePolice8);
                        $worksheet->write($row, $colonne++, $habilitationsAgent->getGestionCompteGroupeMoniteur(), $styleSimplePolice8);
                    }
                    if (Atexo_Module::isEnabled('GererAdressesService')) {
                        $worksheet->write($row, $colonne++, $habilitationsAgent->getGererAdressesService(), $styleSimplePolice8);
                    }
                    if (Atexo_Module::isEnabled('GererCertificatsAgent')) {
                        $worksheet->write($row, $colonne++, $habilitationsAgent->getGestionCertificatsAgent(), $styleSimplePolice8);
                    }
                    if (Atexo_Module::isEnabled('GestionMandataire')) {
                        $worksheet->write($row, $colonne++, $habilitationsAgent->getGestionMandataire(), $styleSimplePolice8);
                    }
                    $worksheet->write($row, $colonne++, $habilitationsAgent->getGestionCompteJal(), $styleSimplePolice8);
                    $worksheet->write($row, $colonne++, $habilitationsAgent->getGestionBiCles(), $styleSimplePolice8);
                    $worksheet->write($row, $colonne++, $habilitationsAgent->getGestionFournisseursEnvoisPostaux(), $styleSimplePolice8);
                    if (Atexo_Module::isEnabled('CentralePublication')) {
                        $worksheet->write($row, $colonne++, $habilitationsAgent->getGestionCentralePub(), $styleSimplePolice8);
                    }

                    if (Atexo_Module::isEnabled('GestionNewsletter')) {
                        ++$colonne;
                        $worksheet->write($row, $colonne++, $habilitationsAgent->getGererNewsletter(), $styleSimplePolice8);
                        if (Atexo_Module::isEnabled('InterfaceModuleRsem')) {
                            $worksheet->write($row, $colonne++, $habilitationsAgent->getGererNewsletterRedac(), $styleSimplePolice8);
                        }
                    }
                    if (Atexo_Module::isEnabled('MenuAgentSocietesExclues')) {
                        ++$colonne;
                        $worksheet->write($row, $colonne++, $habilitationsAgent->getPorteeSocietesExclues(), $styleSimplePolice8);
                        $worksheet->write($row, $colonne++, $habilitationsAgent->getModifierSocietesExclues(), $styleSimplePolice8);
                        $worksheet->write($row, $colonne++, $habilitationsAgent->getSupprimerSocietesExclues(), $styleSimplePolice8);
                        $worksheet->write($row, $colonne++, $habilitationsAgent->getPorteeSocietesExcluesTousOrganismes(), $styleSimplePolice8);
                    }
                    if (Atexo_Module::isEnabled('GestionAdressesFacturationJAL')) {
                        $worksheet->write($row, $colonne++, $habilitationsAgent->getGestionAdressesFacturationJal(), $styleSimplePolice8);
                    }
                    ++$colonne;
                    ++$colonne;
                    $worksheet->write($row, $colonne++, $habilitationsAgent->getValiderProjetAchat() ?: "0", $styleSimplePolice8);
                }
                ++$row;
            }
        }

        $workbook->close();
        if (!$toFile) {
            $workbook->send($nomFichier);
        }

        exit;
    }

    public function generateExcelRegistreEchange($consultationId, $echangeResult, $toFile = false, $organisme = null, $useSearchFunction = true, $archive = false)
    {
        if (!$organisme) {
            $organisme = Atexo_CurrentUser::getCurrentOrganism();
        }
        if ($useSearchFunction) {
            $criteria = new Atexo_Consultation_CriteriaVo();
            $criteria->setcalledFromPortail(false);
            $criteria->setIdService(Atexo_CurrentUser::getCurrentServiceId());
            $criteria->setAcronymeOrganisme($organisme);
            $criteria->setIdReference($consultationId);
            $criteria->setConnectedAgentId(Atexo_CurrentUser::getId());
            $consultation = (new Atexo_Consultation())->search($criteria);
        } else {
            $consultation = [];
            $consultation[] = (new Atexo_Consultation())->retrieveConsultation($consultationId, $organisme);
        }

        if (count($consultation) > 0 && $consultation[0] instanceof CommonConsultation) {
            $consultation = array_shift($consultation);

            (new Atexo_Config())->includePhpLibPath();
            require_once 'Spreadsheet/Excel/Writer.php';

            //Initialisation du fichier excel
            $tmpFileName = Atexo_Config::getParameter('COMMON_TMP').Prado::localize('TEXT_REGISTRER_XLS');
            if ($toFile) {
                $workbook = new Spreadsheet_Excel_Writer($tmpFileName);
            } else {
                $workbook = new Spreadsheet_Excel_Writer();
            }

            $workbook->setCustomColor(11, 0, 204, 255);
            $workbook->setCustomColor(12, 221, 221, 221);
            //Pour passer outre la limite des 255 caracteres
            $workbook->setVersion(8);

            //Style des entetes
            $formatStantardGras = &$workbook->addFormat();
            $formatStantardGras->setColor('black');
            $formatStantardGras->setPattern(1);
            $formatStantardGras->setFgColor('white');
            $formatStantardGras->setSize(8);
            $formatStantardGras->setAlign('left');
            //$formatStantardGras->setOutLine();
            $formatStantardGras->setBold(1);
            $formatStantardGras->setBorder(1);
            $formatStantardGras->setBorderColor(12);

            $formatStantard = &$workbook->addFormat();
            $formatStantard->setColor('black');
            $formatStantard->setPattern(1);
            $formatStantard->setFgColor('white');
            $formatStantard->setSize(8);
            $formatStantard->setAlign('left');
            $formatStantard->setAlign('top');
            $formatStantard->setBold(0);
            $formatStantard->setBorder(1);
            $formatStantard->setBorderColor(12);

            $formatStantardWrap = &$workbook->addFormat();
            $formatStantardWrap->setColor('black');
            $formatStantardWrap->setFgColor('white');
            $formatStantardWrap->setSize(8);
            $formatStantardWrap->setAlign('left');
            $formatStantardWrap->setAlign('top');
            $formatStantardWrap->setBold(0);
            $formatStantardWrap->setTextWrap();
            $formatStantardWrap->setBorder(1);
            $formatStantardWrap->setBorderColor(12);

            $formatEnteteTableau = &$workbook->addFormat();
            $formatEnteteTableau->setColor('black');
            $formatEnteteTableau->setPattern(1);
            $formatEnteteTableau->setFgColor(11);
            $formatEnteteTableau->setSize(8);
            $formatEnteteTableau->setAlign('left');
            //$formatEnteteTableau->setOutLine();
            $formatEnteteTableau->setBold(1);
            $formatEnteteTableau->setAlign('center');
            $formatEnteteTableau->setAlign('top');
            $formatEnteteTableau->setTextWrap();

            $corpsTableau = &$workbook->addFormat();
            $corpsTableau->setColor('black');
            $corpsTableau->setPattern(1);
            $corpsTableau->setFgColor('white');
            $corpsTableau->setSize(8);
            $corpsTableau->setAlign('top');
            //$corpsTableau->setOutLine();
            $corpsTableau->setBold(0);
            $corpsTableau->setTextWrap();
            $corpsTableau->setBorder(1);
            $corpsTableau->setBorderColor(12);

            // la feuille
            $nomOnglet = Prado::localize('DEFINE_SUIVI_MESSAGES');

            $worksheet = &$workbook->addWorkSheet(Atexo_Util::utf8ToIso($nomOnglet));
            $worksheet->setInputEncoding(Atexo_Config::getParameter('HTTP_ENCODING'));

            $worksheet->setColumn(0, 0, '25');
            $worksheet->setColumn(1, 1, '25');
            $worksheet->setColumn(2, 2, '30');
            $worksheet->setColumn(3, 3, '15');
            $worksheet->setColumn(4, 4, '10');
            $worksheet->setColumn(5, 5, '15');
            $worksheet->setColumn(6, 6, '15');
            $worksheet->setColumn(6, 6, '10');
            $worksheet->setLandscape();

            $nomRegistre = Prado::localize('DEFINE_REGISTRES').' '.Prado::localize('DEFINE_SUIVI_MESSAGES_ECHANGES_ET_DOC_JOINTS');
            $nomfichier = Prado::localize('DEFINE_REGISTRES').'_'.
                       str_replace(' ', '_', Prado::localize('DEFINE_SUIVI_MESSAGES')).'_'.
                       $consultation->getReferenceUtilisateur(true).'_'.
                       date('Y').'_'.date('m').'_'.date('d').'.xls';
            $entete = '&"Arial"&C&8'.$nomfichier.' - &A';

            $worksheet->setHeader($entete);
            $worksheet->setFooter(self::getFooterImprimeLe());
            $worksheet->setMarginLeft(0.15);
            $worksheet->setMarginRight(0.7);

            for ($i = 0; $i <= 17; ++$i) {
                if (8 == $i) {
                    $nombreLigne = ceil(strlen($consultation->getIntitule()) / 45); //120 etant le nombre de caractere aproximatif par ligne (de la colonne D a la colonne L)
                    $nombreLigne = ($nombreLigne < 1) ? 1 : $nombreLigne;
                    $worksheet->setRow($i, '11.25' * $nombreLigne);
                } elseif (9 == $i) {
                    $nombreLigne = ceil(strlen($consultation->getObjet()) / 45); //120 etant le nombre de caractere aproximatif par ligne (de la colonne D a la colonne L)
                    $nombreLigne = ($nombreLigne < 1) ? 1 : $nombreLigne;
                    $worksheet->setRow($i, '11.25' * $nombreLigne);
                } else {
                    $worksheet->setRow($i, '11,25');
                }
            }

            $worksheet->setRow(18, '22,5');

            $worksheet->write(0, 0, $nomRegistre, $formatStantardGras);

            $worksheet->write(2, 0, Prado::localize('IDENTIF_CONS'), $formatStantardGras);

            $worksheet->write(3, 0, Prado::localize('DEFINE_REFERENCE').' :', $formatStantard);
            $worksheet->writeString(3, 2, $consultation->getReferenceUtilisateur(), $formatStantard);

            $entitePublique = Atexo_Organismes::retrieveOrganismeByAcronyme($organisme);
            $worksheet->write(4, 0, Prado::localize('DEFINE_TEXT_ENTITE_PUBLIC').' :', $formatStantard);
            if ($entitePublique instanceof CommonOrganisme) {
                $worksheet->write(4, 2, $entitePublique->getSigle().' - '.$entitePublique->getDenominationOrg(), $formatStantard);
            }

            $service = Atexo_EntityPurchase::getPathEntityById($consultation->getServiceId(), $organisme);
            $worksheet->write(5, 0, Prado::localize('TEXT_ENTITE_ACHAT').' :', $formatStantard);
            $worksheet->write(5, 2, $service, $formatStantard);

            $typeAvis = (new Atexo_Consultation_TypeAvis())->retrieveTypeAnnonce($consultation->getIdTypeAvis());
            if ($typeAvis) {
                $worksheet->write(6, 0, Prado::localize('TYPE_ANNONCE').' :', $formatStantard);
                $worksheet->write(6, 2, $typeAvis->getIntituleAvis(), $formatStantard);
            }

            $typeProcedure = (new Atexo_Consultation_ProcedureType())->retrieveTypeProcedure($consultation->getIdTypeProcedureOrg(), false, $organisme);
            $worksheet->write(7, 0, Prado::localize('TEXT_TYPE_PROCEDURE').' :', $formatStantard);
            $worksheet->write(7, 2, $typeProcedure, $formatStantard);

            $worksheet->write(8, 0, Prado::localize('DEFINE_INTITULE_CONSULTATION').' :', $formatStantard);
            $worksheet->write(8, 2, $consultation->getIntitule(), $formatStantardWrap);
            $worksheet->write(8, 4, '', $formatStantard);
            $worksheet->write(8, 5, '', $formatStantard);
            $worksheet->write(8, 6, '', $formatStantard);

            $worksheet->write(9, 0, Prado::localize('DEFINE_OBJET_CONSULTATION').' :', $formatStantard);
            $worksheet->write(9, 2, $consultation->getObjet(), $formatStantardWrap);
            $worksheet->write(9, 4, '', $formatStantard);
            $worksheet->write(9, 5, '', $formatStantard);
            $worksheet->write(9, 6, '', $formatStantard);

            $worksheet->mergeCells(8, 3, 8, 6);
            $worksheet->mergeCells(9, 3, 9, 6);

            $categorie = Atexo_Consultation_Category::retrieveLibelleCategorie($consultation->getCategorie(), false, $organisme);
            $worksheet->write(10, 0, Prado::localize('DEFINE_CATEGORIE_PRINCIPAL').' :', $formatStantard);
            $worksheet->write(10, 2, $categorie, $formatStantard);

            $worksheet->write(11, 0, Prado::localize('DEFINE_DATE_LIMITE_REMISE_PLIS').' :', $formatStantard);
            $worksheet->write(11, 2, Atexo_Util::iso2frnDateTime($consultation->getDatefin()), $formatStantard);

            $colonne = 0;
            $row = 18;
            //Creation entete
            $worksheet->write($row, $colonne++, Prado::localize('TEXT_DE'), $formatEnteteTableau);
            $worksheet->write($row, $colonne++, Prado::localize('TEXT_A'), $formatEnteteTableau);
            $worksheet->write($row, $colonne++, Prado::localize('DEFINE_TEXT_OBJET'), $formatEnteteTableau);
            $worksheet->write($row, $colonne++, Prado::localize('DEFINE_TEXT_PJ'), $formatEnteteTableau);
            $worksheet->write($row, $colonne++, Prado::localize('TEXT_DATE_HEURE'), $formatEnteteTableau);
            $worksheet->write($row, $colonne++, Prado::localize('DEFINE_TEXT_AR'), $formatEnteteTableau);
            $worksheet->write($row, $colonne++, Prado::localize('DEFINE_TEXT_FORMAT'), $formatEnteteTableau);
            if ($archive) {
                $worksheet->write($row, $colonne++, Prado::localize('DEFINE_TEXT_EMPLACEMENT_MESSAGE_ECHANGE'), $formatEnteteTableau);
            }

            //fin entete
            if (is_array($echangeResult)) {
                foreach ($echangeResult as $resultLigne) {
                    ++$row;
                    $colonne = 0;
                    $worksheet->write($row, $colonne++, $resultLigne['EXPEDITEUR'], $corpsTableau);
                    $worksheet->write($row, $colonne++, $resultLigne['MAIL_DESTINATAIRE'], $corpsTableau);
                    $worksheet->write($row, $colonne++, $resultLigne['OBJET'], $corpsTableau);
                    $idEchange = $resultLigne['ID_ECHANGE'];
                    if (!$result[$idEchange]['nomPjs']) {
                        $Pjs = Atexo_Message::getPjByIdEchange($resultLigne['ID_ECHANGE'], $organisme);
                        if (is_array($Pjs)) {
                            $nomPjs = [];
                            foreach ($Pjs as $pj) {
                                $nomPjs[] = $pj->getNomFichier();
                            }
                        }
                        if (is_array($nomPjs) && 0 != count($nomPjs)) {
                            $result[$idEchange]['nomPjs'] = implode("\n", $nomPjs);
                        }
                    }

                    $worksheet->write($row, $colonne++, $result[$idEchange]['nomPjs'], $corpsTableau);
                    $worksheet->write($row, $colonne++, Atexo_Util::iso2frnDateTime($resultLigne['DATE_MESSAGE']), $corpsTableau);

                    if ($resultLigne['TYPE_AR'] == Atexo_Config::getParameter('ID_TYPE_AR_DATE_HEURE')) {
                        $resultLigne['TypeAccuse'] = Atexo_Util::iso2frnDateTime($resultLigne['DATE_AR']);
                    } elseif ($resultLigne['TYPE_AR'] == Atexo_Config::getParameter('ID_TYPE_AR_NON_RETIRE')) {
                        $tabAllTypeAR = Atexo_Message::getAllTypesAR($organisme);
                        $resultLigne['TypeAccuse'] = $tabAllTypeAR[Atexo_Config::getParameter('ID_TYPE_AR_NON_RETIRE')];
                    } else {
                        $resultLigne['TypeAccuse'] = '-';
                    }

                    $worksheet->write($row, $colonne++, $resultLigne['TypeAccuse'], $corpsTableau);

                    $tabAllFormatEchange = Atexo_Message::getAllFormatsEchange($organisme, true);
                    $formatText = $tabAllFormatEchange[$resultLigne['FORMAT']];

                    $worksheet->write($row, $colonne++, $formatText, $corpsTableau);

                    if ($archive) {
                        $dateEchange = substr($resultLigne['DATE_MESSAGE'], '0', '10');
                        $ssDossier = 'Echange_'.$idEchange.'_'.$dateEchange;
                        $emplacement = Atexo_Config::getParameter('DIR_DOC_EXTERNE_MESSAGE_ECHANGE').'/'.$ssDossier;
                        $worksheet->write($row, $colonne++, $emplacement, $corpsTableau);
                    }
                }
            }
            $workbook->close();
            if (!$toFile) {
                $workbook->send($nomfichier);
            } else {
                $pathFichier = Atexo_Config::getParameter('COMMON_TMP').$nomfichier;
                //echo "mv " . $tmpFileName . $pathFichier . " 2>&1 > ".Atexo_Config::getParameter('COMMON_TMP')."zip_error", $sys_answer; exit();
                system('mv '.$tmpFileName." '".$pathFichier."' > ".Atexo_Config::getParameter('COMMON_TMP').'zip_error 2>&1');

                return $pathFichier;
            }
        }
        exit;
    }

    //Generation de l'excel des marches notifies
    public function genererExcelMarchesNotifies($arrayMarches, $criteriaVo)
    {
        (new Atexo_Config())->includePhpLibPath();
        require_once 'Spreadsheet/Excel/Writer.php';

        $workbook = new Spreadsheet_Excel_Writer();

        $format_head = &$workbook->addFormat();
        $format_head->setColor('black');
        $format_head->setPattern(1);
        $format_head->setFgColor(11);
        $format_head->setSize(8);
        $format_head->setAlign('left');
        //$format_head->setOutLine();
        $format_head->setBold(1);
        $format_head->setAlign('center');
        $format_head->setAlign('top');
        $format_head->setTextWrap();
        //Format gras
        $format_bold = &$workbook->addFormat();
        $format_bold->setColor('black');
        $format_bold->setSize(8);
        $format_bold->setAlign('left');
        $format_bold->setBold(1);
        $format_bold->setalign('vjustify');
        // format normal
        $format = &$workbook->addFormat();
        $format->setColor('black');
        $format->setSize(8);
        $format->setAlign('left');
        $format->setalign('top');
        $format->setTextWrap(1);

        // format petit caractere
        $format_retrecie = &$workbook->addFormat();
        $format_retrecie->setColor('black');
        $format_retrecie->setSize(7);
        $format_retrecie->setAlign('left');

        $worksheet = &$workbook->addWorkSheet(Atexo_Util::utf8ToIso(Prado::localize('DEFINE_MARCHES_NOTIFIES')));
        $worksheet->setInputEncoding(Atexo_Config::getParameter('HTTP_ENCODING'));

        //$worksheet->hideGridlines();
        $nomFichier = Prado::localize('DEFINE_NOM_EXCEL_LISTE_MARCHES_NOTIFIES');
        $worksheet->setHeader($nomFichier, '0.19');
        $worksheet->setFooter(self::getFooterImprimeLe(), '0.19');
        $workbook->setCustomColor(11, 0, 204, 255);
        $workbook->setCustomColor(12, 221, 221, 221);
        $workbook->setVersion(8);

        //Longueur des colonnes
        $worksheet->setColumn(0, 0, '31,29');
        $worksheet->setColumn(1, 1, '35');
        $worksheet->setColumn(2, 2, '12');
        $worksheet->setColumn(3, 3, '12,29');
        $worksheet->setColumn(4, 4, '17,29');
        $worksheet->setColumn(5, 5, '10');
        $worksheet->setColumn(6, 6, '12,43');
        $worksheet->setColumn(7, 7, '12,43');
        $worksheet->setColumn(8, 8, '9');
        $worksheet->setColumn(9, 9, '17,29');
        //$worksheet->setColumn(8, 8, '17,29');

        $worksheet->write(0, 0, Prado::localize('LISTE_MARCHES_NOTIFIES'), $format_bold);
        $worksheet->write(1, 0, Prado::localize('TEXT_RAPPEL_CRITERES_RECHERCHES'), $format_bold);
        $worksheet->write(2, 0, Prado::localize('DEFINE_TEXT_ENTITE_PUBLIC').' :');
        $worksheet->write(3, 0, Prado::localize('TEXT_ENTITE_ACHAT').' :');
        $worksheet->write(4, 0, Prado::localize('DEFINE_ATTRIBUTAIRE').' :');
        $worksheet->write(5, 0, Prado::localize('NATURE_DES_PRESTATIONS').' :');
        $worksheet->write(6, 0, Prado::localize('DATE_NOTIFICATION').' :');
        $worksheet->write(7, 0, Prado::localize('DEFINE_MOTS_CLES').' :');

        $worksheet->setLandscape();

        $organisme = Atexo_Organismes::retrieveOrganismeByAcronyme($criteriaVo->getAcronymeOrganisme());
        $langue = Atexo_CurrentUser::readFromSession('lang');
        $getDenominationOrg = 'getDenominationOrg'.Atexo_Languages::getLanguageAbbreviation($langue);
        if (0 == strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE'))
               || !$organisme->$getDenominationOrg()) {
            $denominationOrganisme = $organisme->getSigle().' - '.$organisme->getDenominationOrg();
        } else {
            $denominationOrganisme = $organisme->getSigle().' - '.$organisme->$getDenominationOrg();
        }

        $naturePrestation = (0 != $criteriaVo->getNatureMarche()) ?
                               Atexo_Consultation_Category::retrieveCatgorieConsultationTraduit($criteriaVo->getNatureMarche(), $langue)
                               : Prado::localize('DEFINE_TOUS');

        if ($criteriaVo->getAcronymeOrganisme()) {
            $worksheet->write(2, 1, $denominationOrganisme);
        }

        $worksheet->write(3, 1, Atexo_EntityPurchase::getEntityPathById($criteriaVo->getIdService(), $criteriaVo->getAcronymeOrganisme(), true, true));

        if ($criteriaVo->getNomAttributaire()) {
            $worksheet->write(4, 1, $criteriaVo->getNomAttributaire());
        }

        $worksheet->write(5, 1, $naturePrestation);

        if ($criteriaVo->getDate1()) {
            $worksheet->write(6, 1, $criteriaVo->getDate1());
        }
        if ($criteriaVo->getDate2()) {
            $worksheet->write(6, 2, $criteriaVo->getDate2());
        }
        if ($criteriaVo->getMotclef()) {
            $worksheet->write(7, 1, $criteriaVo->getMotclef());
        }

        if (is_array($arrayMarches) && count($arrayMarches)) {
            //Initialisation du fichier excel
            $worksheet->write(10, 0, Prado::localize('DEFINE_ENTITE_ACHAT'), $format_head);
            $worksheet->write(10, 1, Prado::localize('DEFINE_OBJET_MARCHE'), $format_head);
            $worksheet->write(10, 2, Prado::localize('DATE_NOTIFICATION'), $format_head);
            $worksheet->write(10, 3, Prado::localize('DEFINE_ATTRIBUTAIRE'), $format_head);
            $worksheet->write(10, 4, Prado::localize('TEXT_SIRET'), $format_head);
            $worksheet->write(10, 5, Prado::localize('DEFINE_CODE_POSTAL'), $format_head);
            $worksheet->write(10, 6, Prado::localize('TEXT_VILLE'), $format_head);
            $worksheet->write(10, 7, Prado::localize('MONTANT_EUR_HT'), $format_head);
            $worksheet->write(10, 8, Prado::localize('DEFINE_NATURE'), $format_head);
            $worksheet->write(10, 9, Prado::localize('TEXT_TYPE_PROCEDURE'), $format_head);
            $worksheet->write(10, 10, Prado::localize('TEXT_CODES_CPV'), $format_head);

            $row = 11;

            $langue = Atexo_Languages::readLanguageFromSession();
            //Remplissage des lignes avec les donnees
            foreach ($arrayMarches as $marches) {
                $pathService = Atexo_EntityPurchase::getEntityPathById($marches['service'], $criteriaVo->getAcronymeOrganisme(), true, true);
                if (1 == $marches['nature_prestation']) {
                    $nature = Prado::localize('DEFINE_TRAVAUX');
                } elseif (2 == $marches['nature_prestation']) {
                    $nature = Prado::localize('DEFINE_FOURNITURES');
                } elseif (3 == $marches['nature_prestation']) {
                    $nature = Prado::localize('DEFINE_SERVICES');
                }
                $libelleTypeProcedure = (new Atexo_Consultation_ProcedureType())->retrieveTypeProcedure($marches['id_type_procedure'], false, Atexo_CurrentUser::getCurrentOrganism(), $langue);
                $worksheet->write($row, 0, $pathService, $format);
                $worksheet->write($row, 1, html_entity_decode(html_entity_decode($marches['objet_marche'])), $format);
                $worksheet->write($row, 2, Atexo_Util::iso2frnDate($marches['date_notification']), $format);
                $worksheet->write($row, 3, $marches['attributaire'], $format);
                $worksheet->writeString($row, 4, $marches['siren_Attributaire'].$marches['nic_Attributaire'], $format);
                $worksheet->write($row, 5, $marches['code_postal'], $format);
                $worksheet->write($row, 6, $marches['ville'], $format);
                $worksheet->write($row, 7, $marches['montant_ht'], $format);
                $worksheet->write($row, 8, $nature, $format);
                $worksheet->write($row, 9, Atexo_Util::replaceCharactersMsWordWithRegularCharacters(htmlspecialchars_decode(html_entity_decode($libelleTypeProcedure))), $format);
                $worksheet->write($row, 10, str_replace('<br/>', ' ', $marches['code_cpv']), $format);
                ++$row;
            }
        }

        $workbook->close();
        $workbook->send($nomFichier);
        exit;
    }

    /*
    * Retourne le libelle de l'organisme
    * @Param: $entite: l'entite sous forme de chaine qui concatene le service ou l'organisme avec le libelle
    * @return: libelle de l'organisme
    */
    public function getLibelleEntite($entite)
    {
        if (false === strpos($entite, '#')) {
            return $entite;
        }
        $arrayEntite = explode('#', $entite);
        if ($arrayEntite[1]) {
            return $arrayEntite[1];
        }

        return '';
    }

    /*
     * Retourne le sigle ou le departement selon la valeur du parametre $sigle
     * @param: $entite: l'entite sous forme de chaine qui concatene le service ou l'organisme avec le libelle
     * @param: $sigle: a true retourne le sigle sinon le departement
     * @return: sigle ou departement
     */
    public function getDepartement($entite)
    {
        $arrayEntite = explode('#', $entite);
        if ($arrayEntite) {
            if (is_numeric($arrayEntite[0]) && 0 != $arrayEntite[0]) {
                $service = (new Atexo_EntityPurchase())->getEntityById($arrayEntite[0], Atexo_CurrentUser::getCurrentOrganism());
                if ($service) {
                    return $service->getCp();
                }
            } else {
                $acronyme = $arrayEntite[0];
                if ('0' == $acronyme) {
                    $acronyme = Atexo_CurrentUser::getCurrentOrganism();
                }
                $organisme = Atexo_Organismes::retrieveOrganismeByAcronyme($acronyme);
                if ($organisme) {
                    return $organisme->getCp();
                }
            }

            return '';
        }
    }

    /*
     * Retourne le sigle ou le departement selon la valeur du parametre $sigle
     * @param: $entite: l'entite sous forme de chaine qui concatene le service ou l'organisme avec le libelle
     * @param: $sigle: a true retourne le sigle sinon le departement
     * @return: sigle ou departement
     */
    public function getAcronyme($entite)
    {
        if (false === strpos($entite, '#')) {
            return '';
        }
        $arrayEntite = explode('#', $entite);
        if ($arrayEntite) {
            if (is_numeric($arrayEntite[0]) && 0 != $arrayEntite[0]) {
                return Atexo_CurrentUser::getCurrentOrganism();
            } else {
                $acronyme = $arrayEntite[0];
                if ('0' == $acronyme) {
                    $acronyme = Atexo_CurrentUser::getCurrentOrganism();
                }

                return $acronyme;
            }

            return '';
        }
    }

    public function generateExcelRegistreNotification($consultationId, $echangeResult, $toFile = false, $organisme = null, $useSearchFunction = true, $archive = false, $nomfichier = false)
    {
        if (!$organisme) {
            $organisme = Atexo_CurrentUser::getCurrentOrganism();
        }
        if ($useSearchFunction) {
            $criteria = new Atexo_Consultation_CriteriaVo();
            $criteria->setcalledFromPortail(false);
            $criteria->setIdService(Atexo_CurrentUser::getCurrentServiceId());
            $criteria->setAcronymeOrganisme($organisme);
            $criteria->setIdReference($consultationId);
            $criteria->setConnectedAgentId(Atexo_CurrentUser::getId());
            $consultation = (new Atexo_Consultation())->search($criteria);
        } else {
            $consultation = [];
            $consultation[] = (new Atexo_Consultation())->retrieveConsultation($consultationId, $organisme);
        }

        if (count($consultation) > 0 && $consultation[0] instanceof CommonConsultation) {
            $consultation = array_shift($consultation);

            (new Atexo_Config())->includePhpLibPath();
            require_once 'Spreadsheet/Excel/Writer.php';

            //Initialisation du fichier excel
            $tmpFileName = Atexo_Config::getParameter('COMMON_TMP').Prado::localize('TEXT_REGISTRER_XLS');
            if ($toFile) {
                $workbook = new Spreadsheet_Excel_Writer($tmpFileName);
            } else {
                $workbook = new Spreadsheet_Excel_Writer();
            }

            $workbook->setCustomColor(11, 0, 204, 255);
            $workbook->setCustomColor(12, 221, 221, 221);
            //Pour passer outre la limite des 255 caracteres
            $workbook->setVersion(8);

            //Style des entetes
            $formatStantardGras = &$workbook->addFormat();
            $formatStantardGras->setColor('black');
            $formatStantardGras->setPattern(1);
            $formatStantardGras->setFgColor('white');
            $formatStantardGras->setSize(8);
            $formatStantardGras->setAlign('left');
            //$formatStantardGras->setOutLine();
            $formatStantardGras->setBold(1);
            $formatStantardGras->setBorder(1);
            $formatStantardGras->setBorderColor(12);

            $formatStantard = &$workbook->addFormat();
            $formatStantard->setColor('black');
            $formatStantard->setPattern(1);
            $formatStantard->setFgColor('white');
            $formatStantard->setSize(8);
            $formatStantard->setAlign('left');
            $formatStantard->setAlign('top');
            $formatStantard->setBold(0);
            $formatStantard->setBorder(1);
            $formatStantard->setBorderColor(12);

            $formatStantardWrap = &$workbook->addFormat();
            $formatStantardWrap->setColor('black');
            $formatStantardWrap->setFgColor('white');
            $formatStantardWrap->setSize(8);
            $formatStantardWrap->setAlign('left');
            $formatStantardWrap->setAlign('top');
            $formatStantardWrap->setBold(0);
            $formatStantardWrap->setTextWrap();
            $formatStantardWrap->setBorder(1);
            $formatStantardWrap->setBorderColor(12);

            $formatEnteteTableau = &$workbook->addFormat();
            $formatEnteteTableau->setColor('black');
            $formatEnteteTableau->setPattern(1);
            $formatEnteteTableau->setFgColor(11);
            $formatEnteteTableau->setSize(8);
            $formatEnteteTableau->setAlign('left');
            //$formatEnteteTableau->setOutLine();
            $formatEnteteTableau->setBold(1);
            $formatEnteteTableau->setAlign('center');
            $formatEnteteTableau->setAlign('top');
            $formatEnteteTableau->setTextWrap();

            $corpsTableau = &$workbook->addFormat();
            $corpsTableau->setColor('black');
            $corpsTableau->setPattern(1);
            $corpsTableau->setFgColor('white');
            $corpsTableau->setSize(8);
            $corpsTableau->setAlign('top');
            //$corpsTableau->setOutLine();
            $corpsTableau->setBold(0);
            $corpsTableau->setTextWrap();
            $corpsTableau->setBorder(1);
            $corpsTableau->setBorderColor(12);

            // la feuille
            $nomOnglet = Prado::localize('DEFINE_REGISTRE_NOTIFICATION');

            $worksheet = &$workbook->addWorkSheet(Atexo_Util::utf8ToIso($nomOnglet));
            $worksheet->setInputEncoding(Atexo_Config::getParameter('HTTP_ENCODING'));

            $worksheet->setColumn(0, 0, '25');
            $worksheet->setColumn(1, 1, '25');
            $worksheet->setColumn(2, 2, '15');
            $worksheet->setColumn(3, 3, '15');
            $worksheet->setColumn(4, 4, '10');
            $worksheet->setColumn(5, 5, '15');
            $worksheet->setColumn(6, 6, '15');
            $worksheet->setColumn(6, 6, '10');
            $worksheet->setLandscape();

            $nomRegistre = Prado::localize('DEFINE_REGISTRE_NOTIFICATION');
            if (!$nomfichier) {
                $nomfichier = str_replace(' ', '_', Prado::localize('DEFINE_REGISTRE_NOTIFICATION')).'_'.
                             $consultation->getReferenceUtilisateur(true).'_'.
                             date('Y').'_'.date('m').'_'.date('d').'.xls';
            }
            $entete = '&"Arial"&C&8'.$nomfichier.' - &A';

            $worksheet->setHeader($entete);
            $worksheet->setFooter(self::getFooterImprimeLe());
            $worksheet->setMarginLeft(0.15);
            $worksheet->setMarginRight(0.7);

            for ($i = 0; $i <= 17; ++$i) {
                if (8 == $i) {
                    $nombreLigne = ceil(strlen($consultation->getTitre()) / 120); //120 etant le nombre de caractere aproximatif par ligne (de la colonne D a la colonne L)
                    $nombreLigne = ($nombreLigne < 1) ? 1 : $nombreLigne;
                    $worksheet->setRow($i, '11.25' * $nombreLigne);
                } elseif (9 == $i) {
                    $nombreLigne = ceil(strlen($consultation->getResume()) / 120); //120 etant le nombre de caractere aproximatif par ligne (de la colonne D a la colonne L)
                    $nombreLigne = ($nombreLigne < 1) ? 1 : $nombreLigne;
                    $worksheet->setRow($i, '11.25' * $nombreLigne);
                } else {
                    $worksheet->setRow($i, '11,25');
                }
            }

            $worksheet->setRow(18, '22,5');

            $worksheet->write(0, 0, $nomRegistre, $formatStantardGras);

            $worksheet->write(2, 0, Prado::localize('IDENTIF_CONS'), $formatStantardGras);

            $worksheet->write(3, 0, Prado::localize('DEFINE_REFERENCE').' :', $formatStantard);
            $worksheet->write(3, 2, $consultation->getReferenceUtilisateur(), $formatStantard);

            $entitePublique = Atexo_Organismes::retrieveOrganismeByAcronyme($organisme);
            $worksheet->write(4, 0, Prado::localize('DEFINE_TEXT_ENTITE_PUBLIC').' :', $formatStantard);
            if ($entitePublique instanceof CommonOrganisme) {
                $worksheet->write(4, 2, $entitePublique->getSigle().' - '.$entitePublique->getDenominationOrg(), $formatStantard);
            }

            $service = Atexo_EntityPurchase::getPathEntityById($consultation->getServiceId(), $organisme);
            $worksheet->write(5, 0, Prado::localize('TEXT_ENTITE_ACHAT').' :', $formatStantard);
            $worksheet->write(5, 2, $service, $formatStantard);

            $typeAvis = (new Atexo_Consultation_TypeAvis())->retrieveTypeAnnonce($consultation->getIdTypeAvis());
            if ($typeAvis) {
                $worksheet->write(6, 0, Prado::localize('TYPE_ANNONCE').' :', $formatStantard);
                $worksheet->write(6, 2, $typeAvis->getIntituleAvis(), $formatStantard);
            }
            $typeProcedure = (new Atexo_Consultation_ProcedureType())->retrieveTypeProcedure($consultation->getIdTypeProcedureOrg(), false, $organisme);
            $worksheet->write(7, 0, Prado::localize('TEXT_TYPE_PROCEDURE').' :', $formatStantard);
            $worksheet->write(7, 2, $typeProcedure, $formatStantard);

            $worksheet->write(8, 0, Prado::localize('DEFINE_INTITULE_CONSULTATION').' :', $formatStantard);
            $worksheet->write(8, 2, $consultation->getTitre(), $formatStantardWrap);
            $worksheet->write(8, 4, '', $formatStantard);
            $worksheet->write(8, 5, '', $formatStantard);
            $worksheet->write(8, 6, '', $formatStantard);

            $worksheet->write(9, 0, Prado::localize('DEFINE_OBJET_CONSULTATION').' :', $formatStantard);
            $worksheet->write(9, 2, $consultation->getResume(), $formatStantardWrap);
            $worksheet->write(9, 4, '', $formatStantard);
            $worksheet->write(9, 5, '', $formatStantard);
            $worksheet->write(9, 6, '', $formatStantard);

            $worksheet->mergeCells(8, 3, 8, 6);
            $worksheet->mergeCells(9, 3, 9, 6);

            $categorie = Atexo_Consultation_Category::retrieveLibelleCategorie($consultation->getCategorie(), false, $organisme);
            $worksheet->write(10, 0, Prado::localize('DEFINE_CATEGORIE_PRINCIPAL').' :', $formatStantard);
            $worksheet->write(10, 2, $categorie, $formatStantard);

            $worksheet->write(11, 0, Prado::localize('DEFINE_DATE_LIMITE_REMISE_PLIS').' :', $formatStantard);
            $worksheet->write(11, 2, Atexo_Util::iso2frnDateTime($consultation->getDatefin()), $formatStantard);

            $colonne = 0;
            $row = 18;
            //Creation entete
            $worksheet->write($row, $colonne++, Prado::localize('TEXT_DE'), $formatEnteteTableau);
            $worksheet->write($row, $colonne++, Prado::localize('TEXT_A'), $formatEnteteTableau);
            $worksheet->write($row, $colonne++, Prado::localize('DEFINE_TEXT_OBJET'), $formatEnteteTableau);
            $worksheet->write($row, $colonne++, Prado::localize('DEFINE_TEXT_PJ'), $formatEnteteTableau);
            $worksheet->write($row, $colonne++, Prado::localize('TEXT_DATE_HEURE'), $formatEnteteTableau);
            $worksheet->write($row, $colonne++, Prado::localize('DEFINE_TEXT_AR'), $formatEnteteTableau);
            $worksheet->write($row, $colonne++, Prado::localize('DEFINE_TEXT_FORMAT'), $formatEnteteTableau);
            $worksheet->write($row, $colonne++, Prado::localize('TEXT_NUM_MARCHE'), $formatEnteteTableau);
            if ($archive) {
                $worksheet->write($row, $colonne++, Prado::localize('DEFINE_TEXT_EMPLACEMENT_MESSAGE_ECHANGE'), $formatEnteteTableau);
            }

            //fin entate
            if (is_array($echangeResult)) {
                foreach ($echangeResult as $resultLigne) {
                    $destinataires = Atexo_Message::getDestinataireNotificationByIdEchange($resultLigne->getId(), $organisme);

                    $mailDestinataires = '';
                    foreach ($destinataires as $destinataire) {
                        $mailDestinataires .= $destinataire->getMailDestinataire()."\n";
                        $typeAR = $destinataire->getTypeAr();
                        $dateAr = $destinataire->getDateAr();
                    }
                    ++$row;
                    $colonne = 0;
                    $worksheet->write($row, $colonne++, $resultLigne->getExpediteur(), $corpsTableau);
                    $worksheet->write($row, $colonne++, $mailDestinataires, $corpsTableau);
                    $worksheet->write($row, $colonne++, $resultLigne->getObjet(), $corpsTableau);

                    $Pjs = Atexo_Message::getPjByIdEchange($resultLigne->getId(), $organisme);
                    $nomPjs = [];
                    if (is_array($Pjs)) {
                        foreach ($Pjs as $pj) {
                            $nomPjs[] = $pj->getNomFichier();
                        }
                    }

                    $nomPj = '';
                    if (is_array($nomPjs) && 0 != count($nomPjs)) {
                        $nomPj = implode("\n", $nomPjs);
                    }

                    $worksheet->write($row, $colonne++, $nomPj, $corpsTableau);
                    $worksheet->write($row, $colonne++, Atexo_Util::iso2frnDateTime($resultLigne->getDateMessage()), $corpsTableau);

                    if ($typeAR == Atexo_Config::getParameter('ID_TYPE_AR_DATE_HEURE')) {
                        $TypeAccuse = Atexo_Util::iso2frnDateTime($dateAr);
                    } elseif ($typeAR == Atexo_Config::getParameter('ID_TYPE_AR_NON_RETIRE')) {
                        $tabAllTypeAR = Atexo_Message::getAllTypesAR($organisme);
                        $TypeAccuse = $tabAllTypeAR[Atexo_Config::getParameter('ID_TYPE_AR_NON_RETIRE')];
                    } else {
                        $TypeAccuse = '-';
                    }

                    $worksheet->write($row, $colonne++, $TypeAccuse, $corpsTableau);

                    $tabAllFormatEchange = Atexo_Message::getAllFormatsEchange($organisme, true);
                    $FormatText = $tabAllFormatEchange[$resultLigne->getFormat()];
                    $worksheet->write($row, $colonne++, $FormatText, $corpsTableau);

                    $relation = (new Atexo_Message_RelationEchange())->getRelationByIdEchangeAndTypeRelation($resultLigne->getId(), Atexo_Config::getParameter('TYPE_RELATION_NOTIFICATION'), $organisme);
                    if ($relation) {
                        $decision = (new Atexo_Consultation_Decision())->retrieveDecisionById(
                            $relation->getIdExterne(),
                            $organisme
                        );
                    }
                    if ($decision) {
                        $worksheet->write($row, $colonne++, $decision->getNumeroMarche(), $corpsTableau);
                    } else {
                        $worksheet->write($row, $colonne++, '', $corpsTableau);
                    }

                    if ($archive) {
                        $idEchange = $resultLigne->getId();
                        $dateEchange = substr($resultLigne->getDateMessage(), '0', '10');
                        $ssDossier = 'Notification_'.$idEchange.'_'.$dateEchange;
                        $emplacement = Atexo_Config::getParameter('DIR_DOC_EXTERNE_NOTIFICATION').'/'.$ssDossier;
                        $worksheet->write($row, $colonne++, $emplacement, $corpsTableau);
                    }
                }
            }
            $workbook->close();
            if (!$toFile) {
                $workbook->send($nomfichier);
            } else {
                $pathFichier = Atexo_Config::getParameter('COMMON_TMP').$nomfichier;
                //echo "mv " . $tmpFileName . $pathFichier . " 2>&1 > ".Atexo_Config::getParameter('COMMON_TMP')."zip_error", $sys_answer; exit();
                system('mv '.$tmpFileName." '".$pathFichier."' > ".Atexo_Config::getParameter('COMMON_TMP').'zip_error 2>&1');

                return $pathFichier;
            }
        }
        exit;
    }

    public function generateExcelEchangeChorus($consultationId, $echangeResult, $decision, $toFile = false, $organisme = null, $useSearchFunction = true, $archive = false)
    {
        //print_r($echangeResult);exit;
        if (!$organisme) {
            $organisme = Atexo_CurrentUser::getCurrentOrganism();
        }
        if ($useSearchFunction) {
            $criteria = new Atexo_Consultation_CriteriaVo();
            $criteria->setcalledFromPortail(false);
            $criteria->setIdService(Atexo_CurrentUser::getCurrentServiceId());
            $criteria->setAcronymeOrganisme($organisme);
            $criteria->setIdReference($consultationId);
            $criteria->setConnectedAgentId(Atexo_CurrentUser::getId());
            $consultation = (new Atexo_Consultation())->search($criteria);
        } else {
            $consultation = [];
            $consultation[] = (new Atexo_Consultation())->retrieveConsultation($consultationId, $organisme);
        }

        if (count($consultation) > 0 && $consultation[0] instanceof CommonConsultation) {
            $consultation = array_shift($consultation);

            (new Atexo_Config())->includePhpLibPath();
            require_once 'Spreadsheet/Excel/Writer.php';

            //Initialisation du fichier excel
            $tmpFileName = Atexo_Config::getParameter('COMMON_TMP').Prado::localize('TEXT_REGISTRER_XLS');
            if ($toFile) {
                $workbook = new Spreadsheet_Excel_Writer($tmpFileName);
            } else {
                $workbook = new Spreadsheet_Excel_Writer();
            }

            $workbook->setCustomColor(11, 0, 204, 255);
            $workbook->setCustomColor(12, 221, 221, 221);
            //Pour passer outre la limite des 255 caractares
            $workbook->setVersion(8);

            //Style des entetes
            $formatStantardGras = &$workbook->addFormat();
            $formatStantardGras->setColor('black');
            $formatStantardGras->setPattern(1);
            $formatStantardGras->setFgColor('white');
            $formatStantardGras->setSize(8);
            $formatStantardGras->setAlign('left');
            //$formatStantardGras->setOutLine();
            $formatStantardGras->setBold(1);
            $formatStantardGras->setBorder(1);
            $formatStantardGras->setBorderColor(12);

            $formatStantard = &$workbook->addFormat();
            $formatStantard->setColor('black');
            $formatStantard->setPattern(1);
            $formatStantard->setFgColor('white');
            $formatStantard->setSize(8);
            $formatStantard->setAlign('left');
            $formatStantard->setAlign('top');
            $formatStantard->setBold(0);
            $formatStantard->setBorder(1);
            $formatStantard->setBorderColor(12);

            $formatStantardWrap = &$workbook->addFormat();
            $formatStantardWrap->setColor('black');
            $formatStantardWrap->setFgColor('white');
            $formatStantardWrap->setSize(8);
            $formatStantardWrap->setAlign('left');
            $formatStantardWrap->setAlign('top');
            $formatStantardWrap->setBold(0);
            $formatStantardWrap->setTextWrap();
            $formatStantardWrap->setBorder(1);
            $formatStantardWrap->setBorderColor(12);

            $formatEnteteTableau = &$workbook->addFormat();
            $formatEnteteTableau->setColor('black');
            $formatEnteteTableau->setPattern(1);
            $formatEnteteTableau->setFgColor(11);
            $formatEnteteTableau->setSize(8);
            $formatEnteteTableau->setAlign('left');
            //$formatEnteteTableau->setOutLine();
            $formatEnteteTableau->setBold(1);
            $formatEnteteTableau->setAlign('center');
            $formatEnteteTableau->setAlign('top');
            $formatEnteteTableau->setTextWrap();

            $corpsTableau = &$workbook->addFormat();
            $corpsTableau->setColor('black');
            $corpsTableau->setPattern(1);
            $corpsTableau->setFgColor('white');
            $corpsTableau->setSize(8);
            $corpsTableau->setAlign('top');
            //$corpsTableau->setOutLine();
            $corpsTableau->setBold(0);
            $corpsTableau->setTextWrap();
            $corpsTableau->setBorder(1);
            $corpsTableau->setBorderColor(12);

            // la feuille
            $nomOnglet = Prado::localize('DEFINE_REGISTRE_NOTIFICATION');

            $worksheet = &$workbook->addWorkSheet(Atexo_Util::utf8ToIso($nomOnglet));
            $worksheet->setInputEncoding(Atexo_Config::getParameter('HTTP_ENCODING'));

            $worksheet->setColumn(0, 0, '10');
            $worksheet->setColumn(1, 1, '20');
            $worksheet->setColumn(2, 2, '15');
            $worksheet->setColumn(3, 3, '15');
            $worksheet->setColumn(4, 4, '40');
            $worksheet->setColumn(5, 5, '15');
            $worksheet->setLandscape();

            $nomRegistre = Prado::localize('DEFINE_REGISTRES').' '.Prado::localize('TEXT_ECHANGE_CHORUS');
            $nomfichier = Prado::localize('DEFINE_REGISTRES').'_'.
                       str_replace(' ', '_', Prado::localize('TEXT_ECHANGE_CHORUS')).'_'.
                       $consultation->getReferenceUtilisateur(true).'_'.$decision->getLot().'_'.
                       date('Y_m_d').'.xls';
            $entete = '&"Arial"&C&8'.$nomfichier.' - &A';
        }
        $worksheet->setHeader($entete);
        $worksheet->setFooter(self::getFooterImprimeLe());
        $worksheet->setMarginLeft(0.15);
        $worksheet->setMarginRight(0.7);

        for ($i = 0; $i <= 17; ++$i) {
            if (8 == $i) {
                $nombreLigne = ceil(strlen($consultation->getTitre()) / 120); //120 atant le nombre de caracare aproximatif par ligne (de la colonne D e la colonne L)
                $nombreLigne = ($nombreLigne < 1) ? 1 : $nombreLigne;
                $worksheet->setRow($i, '11.25' * $nombreLigne);
            } elseif (9 == $i) {
                $nombreLigne = ceil(strlen($consultation->getResume()) / 120); //120 atant le nombre de caracare aproximatif par ligne (de la colonne D e la colonne L)
                $nombreLigne = ($nombreLigne < 1) ? 1 : $nombreLigne;
                $worksheet->setRow($i, '11.25' * $nombreLigne);
            } else {
                $worksheet->setRow($i, '11,25');
            }
        }

        $worksheet->setRow(18, '22,5');

        $worksheet->write(0, 0, $nomRegistre, $formatStantardGras);

        $worksheet->write(2, 0, Prado::localize('IDENTIF_CONS'), $formatStantardGras);

        $worksheet->write(3, 0, Prado::localize('DEFINE_REFERENCE').' :', $formatStantard);
        $worksheet->writeString(3, 2, $consultation->getReferenceUtilisateur(), $formatStantard);

        $entitePublique = Atexo_Organismes::retrieveOrganismeByAcronyme($organisme);
        $worksheet->write(4, 0, Prado::localize('DEFINE_TEXT_ENTITE_PUBLIC').' :', $formatStantard);
        if ($entitePublique instanceof CommonOrganisme) {
            $worksheet->write(4, 2, $entitePublique->getSigle().' - '.$entitePublique->getDenominationOrg(), $formatStantard);
        }

        $service = Atexo_EntityPurchase::getPathEntityById($consultation->getServiceId(), $organisme);
        $worksheet->write(5, 0, Prado::localize('TEXT_ENTITE_ACHAT').' :', $formatStantard);
        $worksheet->write(5, 2, $service, $formatStantard);

        $typeAvis = (new Atexo_Consultation_TypeAvis())->retrieveTypeAnnonce($consultation->getIdTypeAvis());
        if ($typeAvis) {
            $worksheet->write(6, 0, Prado::localize('TYPE_ANNONCE').' :', $formatStantard);
            $worksheet->write(6, 2, $typeAvis->getIntituleAvis(), $formatStantard);
        }
        $typeProcedure = (new Atexo_Consultation_ProcedureType())->retrieveTypeProcedure($consultation->getIdTypeProcedureOrg(), false, $organisme);
        $worksheet->write(7, 0, Prado::localize('TEXT_TYPE_PROCEDURE').' :', $formatStantard);
        $worksheet->write(7, 2, $typeProcedure, $formatStantard);

        $worksheet->write(8, 0, Prado::localize('DEFINE_INTITULE_CONSULTATION').' :', $formatStantard);
        $worksheet->write(8, 2, $consultation->getTitre(), $formatStantardWrap);
        $worksheet->write(8, 4, '', $formatStantard);
        $worksheet->write(8, 5, '', $formatStantard);
        $worksheet->write(8, 6, '', $formatStantard);

        $worksheet->write(9, 0, Prado::localize('DEFINE_OBJET_CONSULTATION').' :', $formatStantard);
        $worksheet->write(9, 2, $consultation->getResume(), $formatStantardWrap);
        $worksheet->write(9, 4, '', $formatStantard);
        $worksheet->write(9, 5, '', $formatStantard);
        $worksheet->write(9, 6, '', $formatStantard);

        $worksheet->mergeCells(8, 3, 8, 6);
        $worksheet->mergeCells(9, 3, 9, 6);

        $categorie = Atexo_Consultation_Category::retrieveLibelleCategorie($consultation->getCategorie(), false, $organisme);
        $worksheet->write(10, 0, Prado::localize('DEFINE_CATEGORIE_PRINCIPAL').' :', $formatStantard);
        $worksheet->write(10, 2, $categorie, $formatStantard);

        $worksheet->write(11, 0, Prado::localize('DEFINE_DATE_LIMITE_REMISE_PLIS').' :', $formatStantard);
        $worksheet->write(11, 2, Atexo_Util::iso2frnDateTime($consultation->getDatefin()), $formatStantard);

        $colonne = 0;
        $row = 18;
        //Creation entete
        $worksheet->write($row, $colonne++, Prado::localize('TEXT_NUMERO_ORDRE'), $formatEnteteTableau);
        $worksheet->write($row, $colonne++, Prado::localize('TEXT_AGENT'), $formatEnteteTableau);
        $worksheet->write($row, $colonne++, Prado::localize('TEXT_STATUT_ECHAGE'), $formatEnteteTableau);
        $worksheet->write($row, $colonne++, Prado::localize('TEXT_RETOUR_ECHAGE'), $formatEnteteTableau);
        $worksheet->write($row, $colonne++, Prado::localize('TEXT_CONTENU'), $formatEnteteTableau);
        if ($archive) {
            $worksheet->write($row, $colonne++, Prado::localize('DEFINE_TEXT_EMPLACEMENT_MESSAGE_ECHANGE'), $formatEnteteTableau);
        }

        //fin entete
        if (is_array($echangeResult)) {
            foreach ($echangeResult as $resultLigne) {
                ++$row;
                $colonne = 0;
                $worksheet->write($row, $colonne++, $resultLigne->getNumOrdre(), $corpsTableau); //TEXT_NUMERO_ORDRE
                   $worksheet->write($row, $colonne++, $resultLigne->getPrenomCreateur().' '.$resultLigne->getNomCreateur(), $corpsTableau); //TEXT_AGENT
                if ($resultLigne->getStatutechange()) {
                    $statut = Prado::localize('DEFINE_TEXT_DATE_ENVOI').' '.Atexo_Util::iso2frnDateTime($resultLigne->getDateEnvoi());
                } else {
                    $statut = Prado::localize('TEXT_BROUILLON').' '.Prado::localize('TEXT_CREE_LE').' '.Atexo_Util::iso2frnDateTime($resultLigne->getDateCreation());
                }
                $worksheet->write($row, $colonne++, $statut, $corpsTableau); //TEXT_STATUT_ECHAGE

                $retourChorus = $resultLigne->getRetourChorus();
                if ($retourChorus == Atexo_Config::getParameter('CHORUS_RETOUR_SANS_OBJET')) {
                    $retourChorus = Prado::localize('TEXT_SANS_OBJET');
                } elseif ($retourChorus == Atexo_Config::getParameter('CHORUS_RETOUR_INTEGRE')) {
                    $retourChorus = Prado::localize('TEXT_INTEGRE');
                } elseif ($retourChorus == Atexo_Config::getParameter('CHORUS_RETOUR_REJETE')) {
                    $retourChorus = Prado::localize('TEXT_REJETE');
                } elseif ($retourChorus == Atexo_Config::getParameter('CHORUS_RETOUR_IRRECEVABLE')) {
                    $retourChorus = Prado::localize('TEXT_IRRECEVABLE');
                } else {
                    $retourChorus = '';
                }
                $worksheet->write($row, $colonne++, $retourChorus, $corpsTableau); //TEXT_RETOUR_ECHAGE

                $idEchange = $resultLigne->getId();

                // DCE
                $pj = Prado::localize('TEXT_PIECES_DCE')."\n";
                $dce = Atexo_Consultation_Dce::getDce($consultationId, $organisme);
                $nomPjs = Atexo_Chorus_Util::getNomFichierDceEnvoyeAChorus($dce, $resultLigne->getDceItems(), $organisme);
                $pj .= implode(' # ', $nomPjs);

                // pieces Reponses
                $pj .= "\n\n".Prado::localize('PIECES_REPONSE_TITULAIRE')."\n";
                $nomPjs = Atexo_Chorus_Util::getNomPiecesReponceEnvoyeAChorus($consultationId, $organisme, $decision, $resultLigne->getIdsEnvItems());
                $pj .= implode(' # ', $nomPjs);

                // pieces attaches a la consultation
                $pj .= "\n\n".Prado::localize('PIECES_ATTACHEES_CONSULTATION')."\n";

                $documentsExternes = Atexo_Chorus_Echange::retreiveSelectedDocumentExterne($organisme, $idEchange);
                foreach ($documentsExternes as $documentExterne) {
                    $pj .= $documentExterne->getNom().' # ';
                }

                // pieces ouvertures analyse
                $pj .= "\n\n".Prado::localize('TEXT_PIECES_OUVERTURE_ET_ANALYSE')."\n";
                $piecesRapportSignature = Atexo_Chorus_Echange::retriveListeRapportSignatureEchange($resultLigne, $organisme, true);
                foreach ($piecesRapportSignature as $piece) {
                    $pj .= $piece['nomFichier'].' # ';
                }

                // pieces externes
                $pj .= "\n\n".Prado::localize('PIECES_ATTACHEES_CONSULTATION')."\n";
                $piecesexternes = Atexo_Chorus_Echange::retreivePiecesExternes($idEchange, $organisme);
                foreach ($piecesexternes as $pieceexterne) {
                    $pj .= $pieceexterne->getNomFichier().' # ';
                }

                $worksheet->write($row, $colonne++, $pj, $corpsTableau); //TEXT_CONTENU
                if ($archive) {
                    $idEchange = $resultLigne->getId();
                    $dateEchange = substr($resultLigne->getDateCreation(), '0', '10');
                    $dossier = Atexo_Config::getParameter('DIR_DOC_EXTERNE_CHORUS_ECHANGE');
                    $sDossier = 'Echange_Chorus_lot'.$decision->getLot();
                    $ssDossier = 'Chorus_'.$idEchange.'_'.$dateEchange;
                    $emplacement = $dossier.'/'.$sDossier.'/'.$ssDossier;
                    $worksheet->write($row, $colonne++, $emplacement, $corpsTableau);
                }
            }
        }
        $workbook->close();
        if (!$toFile) {
            $workbook->send($nomfichier);
        } else {
            $pathFichier = Atexo_Config::getParameter('COMMON_TMP').$nomfichier;
            system('mv '.$tmpFileName." '".$pathFichier."' > ".Atexo_Config::getParameter('COMMON_TMP').'zip_error 2>&1');

            return $pathFichier;
        }
        exit;
    }

    public function genererCsvTableauBordEchangesChorus($echanges, $forFso = false)
    {
        $separateur = ';';

        if (!$forFso) {
            $entete = Prado::localize('IDENTIFIANT_TECHNIQUE_ECHANGE').$separateur;
        } else {
            $entete = Prado::localize('IDENTIFIANT_TECHNIQUE_DECISION').$separateur;
        }
        $entete .= Prado::localize('DEFINE_NUMERO_FLUX').$separateur;
        $entete .= Prado::localize('DEFINE_TEXT_DATE_HEURE').$separateur;
        if (!$forFso) {
            $entete .= Prado::localize('TEXT_RETOUR_ECHAGE').$separateur;
        }
        $entete .= Prado::localize('CONTRAT_NUMERO_COURT').$separateur;
        if ($forFso) {
            $entete .= Prado::localize('DEFINE_STATUS').$separateur;
        }
        $entete .= Prado::localize('REFERENCE_INTERNE_CONTRAT').$separateur;
        if (!$forFso) {
            $entete .= Prado::localize('DEFINE_AGENT_EMETTEUR').$separateur;
        }
        $entete .= Prado::localize('DEFINE_TEXT_ENTITE_ACHAT');

        $lignes = '';
        if (is_array($echanges) && count($echanges)) {
            foreach ($echanges as $echange) {
                $lignes .= $echange['id'].$separateur;
                $lignes .= $echange['nom_fichier'].$separateur;
                $lignes .= (($echange['date_heure']) ? $echange['date_heure'] : '-').$separateur;
                if (!$forFso) {
                    $lignes .= (($echange['retour_chorus']) ? $echange['retour_chorus'] : '-').$separateur;
                }
                $lignes .= (($echange['numero_marche']) ? $echange['numero_marche'] : '-').$separateur;
                if ($forFso) {
                    $lignes .= (($echange['statut_ej']) ? $echange['statut_ej'] : '-').$separateur;
                }
                $lignes .= $echange['reference_libre'].$separateur;
                if (!$forFso) {
                    $lignes .= (($echange['nom_prenom_agent']) ? str_replace('<br/>', ' ', $echange['nom_prenom_agent']) : '-').$separateur;
                }
                $lignes .= $echange['entite_achat'];
                $lignes .= "\n";
            }
        }
        $contenuCsv = $entete."\n".$lignes;
        $contenuCsv = str_replace('</center>', '', str_replace('<center>', '', $contenuCsv));
        DownloadFile::downloadFileContent(Prado::localize('DEFINE_LISTE_FLUX_CHORUS').'_'.date('Y_m_d_h_i_s').'.csv', $contenuCsv);
    }

    public function generationPvOuvertureDesOffres($consultation, $enveloppes, $categorieLot, $organisme = null, $withVariante = false)
    {
        if (!$organisme) {
            $organisme = Atexo_CurrentUser::getCurrentOrganism();
        }

        (new Atexo_Config())->includePhpLibPath();
        require_once 'Spreadsheet/Excel/Writer.php';

        //Initialisation du fichier excel
        $workbook = new Spreadsheet_Excel_Writer();

        $workbook->setCustomColor(11, 0, 204, 255);
        $workbook->setCustomColor(12, 221, 221, 221);
        //Pour passer outre la limite des 255 caracteres
        $workbook->setVersion(8);

        //Style des entetes
        $formatStantardGras = &$workbook->addFormat();
        $formatStantardGras->setColor('black');
        $formatStantardGras->setPattern(1);
        $formatStantardGras->setFgColor('white');
        $formatStantardGras->setSize(8);
        $formatStantardGras->setAlign('left');
        //$formatStantardGras->setOutLine();
        $formatStantardGras->setBold(1);
        $formatStantardGras->setBorder(1);
        $formatStantardGras->setBorderColor(12);

        $formatStantard = &$workbook->addFormat();
        $formatStantard->setColor('black');
        $formatStantard->setPattern(1);
        $formatStantard->setFgColor('white');
        $formatStantard->setSize(8);
        $formatStantard->setAlign('left');
        $formatStantard->setAlign('top');
        $formatStantard->setBold(0);
        $formatStantard->setBorder(1);
        $formatStantard->setBorderColor(12);

        $formatStantardWrap = &$workbook->addFormat();
        $formatStantardWrap->setColor('black');
        $formatStantardWrap->setFgColor('white');
        $formatStantardWrap->setSize(8);
        $formatStantardWrap->setAlign('left');
        $formatStantardWrap->setAlign('top');
        $formatStantardWrap->setBold(0);
        $formatStantardWrap->setTextWrap();
        $formatStantardWrap->setBorder(1);
        $formatStantardWrap->setBorderColor(12);

        $formatEnteteTableau = &$workbook->addFormat();
        $formatEnteteTableau->setColor('black');
        $formatEnteteTableau->setPattern(1);
        $formatEnteteTableau->setFgColor(11);
        $formatEnteteTableau->setSize(8);
        $formatEnteteTableau->setAlign('left');
        //$formatEnteteTableau->setOutLine();
        $formatEnteteTableau->setBold(1);
        $formatEnteteTableau->setAlign('center');
        $formatEnteteTableau->setAlign('top');
        $formatEnteteTableau->setTextWrap();

        $merged_cells = &$workbook->addformat();
        $merged_cells->setColor('black');
        $merged_cells->setPattern(1);
        $merged_cells->setFgColor(11);
        $merged_cells->setSize(8);
        $merged_cells->setAlign('left');
        $merged_cells->setBold(1);
        $merged_cells->setAlign('vleft');
        // indique qu'une cellule de ce format servira a la fusion
        $merged_cells->setMerge();

        $corpsTableau = &$workbook->addFormat();
        $corpsTableau->setColor('black');
        $corpsTableau->setPattern(1);
        $corpsTableau->setFgColor('white');
        $corpsTableau->setSize(8);
        $corpsTableau->setAlign('top');
        //$corpsTableau->setOutLine();
        $corpsTableau->setBold(0);
        $corpsTableau->setTextWrap();
        $corpsTableau->setBorder(1);
        $corpsTableau->setBorderColor(12);

        $nomOnglet = Prado::localize('PV_OUVERTURE_OFFRES');

        $worksheet = &$workbook->addWorkSheet(Atexo_Util::utf8ToIso($nomOnglet));
        $worksheet->setInputEncoding(Atexo_Config::getParameter('HTTP_ENCODING'));

        $nomFeuille = Prado::localize('TEXT_PV_OUVERTURE_OFFRES');
        $nomfichier = Prado::localize('PV_FILE_OUVERTURE_OFFRES').'_'.Prado::localize('TEXT_REVUE').'_'.date('Y').date('m').date('d').'.xls';
        $entete = '&"Arial"&C&8'.$nomfichier.' - &A';
        $worksheet->setColumn(0, 0, '12');
        $worksheet->setColumn(1, 1, '12');
        $worksheet->setColumn(2, 2, '12');
        $worksheet->setColumn(3, 3, '15');
        $worksheet->setColumn(4, 4, '15');
        $worksheet->setColumn(5, 5, '20');
        $worksheet->setColumn(6, 6, '18');
        $worksheet->setColumn(7, 7, '12');
        $worksheet->setColumn(8, 8, '34');
        $worksheet->setColumn(9, 9, '34');

        $worksheet->setHeader($entete);
        $worksheet->setFooter(self::getFooterImprimeLe());
        $worksheet->setMarginLeft(0.15);
        $worksheet->setMarginRight(0.7);

        for ($i = 0; $i <= 17; ++$i) {
            if (8 == $i) {
                $nombreLigne = ceil(strlen($consultation->getIntitule()) / 120); //120 etant le nombre de caractere aproximatif par ligne (de la colonne D a la colonne L)
                $nombreLigne = ($nombreLigne < 1) ? 1 : $nombreLigne;
                $worksheet->setRow($i, '11.25' * $nombreLigne);
            } elseif (9 == $i) {
                $nombreLigne = ceil(strlen($consultation->getObjet()) / 120); //120 etant le nombre de caractere aproximatif par ligne (de la colonne D a la colonne L)
                $nombreLigne = ($nombreLigne < 1) ? 1 : $nombreLigne;
                $worksheet->setRow($i, '11.25' * $nombreLigne);
            } else {
                $worksheet->setRow($i, '11,25');
            }
        }

        $worksheet->setRow(18, '22,5');

        $worksheet->write(0, 0, $nomFeuille, $formatStantardGras);

        $worksheet->write(2, 0, Prado::localize('IDENTIF_CONS'), $formatStantardGras);

        $worksheet->write(3, 0, Prado::localize('DEFINE_REFERENCE').' :', $formatStantard);
        $worksheet->writeString(3, 3, $consultation->getReferenceUtilisateur(), $formatStantard);

        $entitePublique = Atexo_Organismes::retrieveOrganismeByAcronyme($organisme);
        $worksheet->write(4, 0, Prado::localize('DEFINE_TEXT_ENTITE_PUBLIC').' :', $formatStantard);
        if ($entitePublique instanceof CommonOrganisme) {
            $worksheet->write(4, 3, $entitePublique->getSigle().' - '.$entitePublique->getDenominationOrg(), $formatStantard);
        }

        $service = Atexo_EntityPurchase::getPathEntityById($consultation->getServiceId(), $organisme);
        $worksheet->write(5, 0, Prado::localize('TEXT_ENTITE_ACHAT').' :', $formatStantard);
        $worksheet->write(5, 3, $service, $formatStantard);

        $typeAvis = (new Atexo_Consultation_TypeAvis())->retrieveTypeAnnonce($consultation->getIdTypeAvis());
        if ($typeAvis) {
            $worksheet->write(6, 0, Prado::localize('TYPE_ANNONCE').' :', $formatStantard);
            $worksheet->write(6, 3, $typeAvis->getIntituleAvis(), $formatStantard);
        }
        $typeProcedure = (new Atexo_Consultation_ProcedureType())->retrieveTypeProcedure($consultation->getIdTypeProcedureOrg(), false, $organisme);
        $worksheet->write(7, 0, Prado::localize('TEXT_TYPE_PROCEDURE').' :', $formatStantard);
        $worksheet->write(7, 3, $typeProcedure, $formatStantard);

        $worksheet->setRow(18, '22,5');

        $worksheet->write(0, 0, $nomFeuille, $formatStantardGras);

        $worksheet->write(2, 0, Prado::localize('IDENTIF_CONS'), $formatStantardGras);

        $worksheet->write(3, 0, Prado::localize('DEFINE_REFERENCE').' :', $formatStantard);
        $worksheet->writeString(3, 3, $consultation->getReferenceUtilisateur(), $formatStantard);

        $entitePublique = Atexo_Organismes::retrieveOrganismeByAcronyme($organisme);
        $worksheet->write(4, 0, Prado::localize('DEFINE_TEXT_ENTITE_PUBLIC').' :', $formatStantard);
        if ($entitePublique instanceof CommonOrganisme) {
            $worksheet->write(4, 3, $entitePublique->getSigle().' - '.$entitePublique->getDenominationOrg(), $formatStantard);
        }

        $service = Atexo_EntityPurchase::getPathEntityById($consultation->getServiceId(), $organisme);
        $worksheet->write(5, 0, Prado::localize('TEXT_ENTITE_ACHAT').' :', $formatStantard);
        $worksheet->write(5, 3, $service, $formatStantard);

        $typeAvis = (new Atexo_Consultation_TypeAvis())->retrieveTypeAnnonce($consultation->getIdTypeAvis());
        if ($typeAvis) {
            $worksheet->write(6, 0, Prado::localize('TYPE_ANNONCE').' :', $formatStantard);
            $worksheet->write(6, 3, $typeAvis->getIntituleAvis(), $formatStantard);
        }
        $typeProcedure = (new Atexo_Consultation_ProcedureType())->retrieveTypeProcedure($consultation->getIdTypeProcedureOrg(), false, $organisme);
        $worksheet->write(7, 0, Prado::localize('TEXT_TYPE_PROCEDURE').' :', $formatStantard);
        $worksheet->write(7, 3, $typeProcedure, $formatStantard);

        $worksheet->write(8, 0, Prado::localize('DEFINE_INTITULE_CONSULTATION').' :', $formatStantard);
        $worksheet->write(8, 3, $consultation->getIntitule(), $formatStantardWrap);
        $worksheet->write(8, 4, '', $formatStantard);
        $worksheet->write(8, 5, '', $formatStantard);
        $worksheet->write(8, 6, '', $formatStantard);
        $worksheet->write(8, 7, '', $formatStantard);
        $worksheet->write(8, 8, '', $formatStantard);
        $worksheet->write(8, 9, '', $formatStantard);
        $worksheet->write(8, 10, '', $formatStantard);

        $worksheet->write(9, 0, Prado::localize('DEFINE_OBJET_CONSULTATION').' :', $formatStantard);
        $worksheet->write(9, 3, $consultation->getObjet(), $formatStantardWrap);
        $worksheet->write(9, 4, '', $formatStantard);
        $worksheet->write(9, 5, '', $formatStantard);
        $worksheet->write(9, 6, '', $formatStantard);
        $worksheet->write(9, 7, '', $formatStantard);
        $worksheet->write(9, 8, '', $formatStantard);
        $worksheet->write(9, 9, '', $formatStantard);
        $worksheet->write(9, 10, '', $formatStantard);

        $worksheet->mergeCells(8, 3, 8, 10);
        $worksheet->mergeCells(9, 3, 9, 10);

        if ($categorieLot) {
            $worksheet->write(10, 0, Prado::localize('DEFINE_LOT_NUMERO').' :', $formatStantard);
            $worksheet->write(10, 3, $categorieLot->getLot(), $formatStantard);

            $worksheet->write(11, 0, Prado::localize('DEFINE_INTITULE_LOT').' :', $formatStantard);
            $worksheet->write(11, 3, $categorieLot->getDescription(), $formatStantard);
        } else {
            $worksheet->write(10, 0, Prado::localize('DEFINE_LOT_NUMERO').' :', $formatStantard);
            $worksheet->write(10, 3, ' -- ', $formatStantard);

            $worksheet->write(11, 0, Prado::localize('DEFINE_INTITULE_LOT').' :', $formatStantard);
            $worksheet->write(11, 3, ' -- ', $formatStantard);
        }
        $categorie = Atexo_Consultation_Category::retrieveLibelleCategorie($consultation->getCategorie(), false, $organisme);
        $worksheet->write(12, 0, Prado::localize('DEFINE_CATEGORIE_PRINCIPAL').' :', $formatStantard);
        $worksheet->write(12, 3, $categorie, $formatStantard);

        $worksheet->write(13, 0, Prado::localize('DEFINE_DATE_LIMITE_REMISE_PLIS').' :', $formatStantard);
        $worksheet->write(13, 3, Atexo_Util::iso2frnDateTime($consultation->getDatefin()), $formatStantard);
        if ($categorieLot) {
            $idLot = $categorieLot->getLot();
        } else {
            $idLot = '0';
        }
        if (is_array($enveloppes)) {
            //Recuperer montant offre from ResultatAnalyse
            $query = ' SELECT * FROM resultat_analyse WHERE consultation_id='.$consultation->getId().' AND lot='.$idLot." AND organisme='".$organisme."'";
            $statement = Atexo_Db::getLinkCommon(true)->query($query, PDO::FETCH_ASSOC);
            $montantTab = [];
            $typeEnvTab = [];
            foreach ($statement as $row) {
                $montantTab[$row['id_offre']][$row['type_enveloppe']][$row['id']] = $row['montant_offre'];
                $typeEnvTab[$row['id_offre']] = $row['type_enveloppe'];
            }
            $colonne = 0;
            $row = 20;

            //Begin header
            $worksheet->write($row, $colonne++, Prado::localize('TEXT_TYPE_OFFRE'), $formatEnteteTableau);
            $worksheet->write($row, $colonne++, Prado::localize('TEXT_NUMERO_OFFRE'), $formatEnteteTableau);
            $worksheet->write($row, $colonne++, Prado::localize('TEXT_DATE_HEURE_DEPOT'), $formatEnteteTableau);
            $worksheet->write($row, $colonne++, Prado::localize('TEXT_ENTREPRISE'), $formatEnteteTableau);
            $worksheet->write($row, $colonne++, Prado::localize('IDENTIFIANT_ENTREPRISE'), $formatEnteteTableau);
            $worksheet->write($row, $colonne++, Prado::localize('DEFINE_ADRESSE_POSTAL'), $formatEnteteTableau);
            $worksheet->write($row, $colonne++, Prado::localize('STATUT_ENVELOPPE'), $formatEnteteTableau);
            $worksheet->write($row, $colonne++, Prado::localize('MONTANT_OFFRE'), $formatEnteteTableau);
            $worksheet->write($row, $colonne++, Prado::localize('OBSERVATIONS'), $formatEnteteTableau);
            $worksheet->write($row, $colonne++, Prado::localize('TEXT_SIGNATURES_REPRESENTANTS_SIGNATAIRES_PRESENTS'), $formatEnteteTableau);
            //End header
            ++$row;
            foreach ($enveloppes as $one) {
                //echo $row."/ *******</br>";
                $colonne = 0;
                if ($one instanceof CommonEnveloppe) {
                    $idEnveloppe = $one->getIdEnveloppeElectro();
                    $offreObject = $one->getCommonOffres();
                    $typeEnveloppe = Atexo_Config::getParameter('TYPE_ENVELOPPE_ELECTRONIQUE');
                    $dateDepot = Atexo_Util::iso2frnDateTime($offreObject->getUntrusteddate(), false, true);
                    $nomEntreprise = $offreObject->getNomEntrepriseInscrit();
                    $siret = (($offreObject->getSiretEntreprise()) ? $offreObject->getSiretEntreprise() : $offreObject->getIdentifiantNational());

                    $codePostalInscrit = $offreObject->getCodePostalInscrit();
                    $villeInscrit = $offreObject->getVilleInscrit();
                    $adresseInscrit = $offreObject->getAdresseInscrit();

                    $etablissement = (new CommonTEtablissementQuery())->findByIdEtablissement($offreObject->getIdEtablissement());
                    if ($etablissement[0] instanceof CommonTEtablissement) {
                        if ('' === $codePostalInscrit)  {
                            $codePostalInscrit = $etablissement[0]->getCodePostal();
                        }
                        if ('' === $villeInscrit) {
                            $villeInscrit = $etablissement[0]->getVille();
                        }
                        if ('' === $adresseInscrit) {
                            $adresseInscrit = $etablissement[0]->getAdresse();
                        }
                    }

                    $cpVilleAdresse = ($adresseInscrit ? $adresseInscrit : '-')
                        . ' '
                        . ($codePostalInscrit ? $codePostalInscrit : '-')
                        . ' '
                        . ($villeInscrit ? $villeInscrit : '-')
                    ;

                    $textNumOffre = Prado::localize('TEXT_ELECTRONIQUE');
                    $numOffre = 'EL '.$one->getNumPli();
                    $observations = $offreObject->getObservation();
                } else {
                    $idEnveloppe = $one->getIdEnveloppePapier();
                    $offreObject = $one->getCommonOffrePapier();
                    $typeEnveloppe = Atexo_Config::getParameter('TYPE_ENVELOPPE_PAPIER');
                    $dateDepot = Atexo_Util::iso2frnDateTime($offreObject->getDateDepot());
                    $nomEntreprise = $offreObject->getNomEntreprise();
                    $siret = (($offreObject->getSiret()) ? $offreObject->getSiret() : $offreObject->getIdentifiantNational());

                    $codePostalInscrit = $offreObject->getCodePostal();
                    $villeInscrit = $offreObject->getVille();
                    $adresseInscrit = $offreObject->getAdresse();

                    $etablissement = (new CommonTEtablissementQuery())->findByIdEtablissement($offreObject->getIdEtablissement());
                    if ($etablissement[0] instanceof CommonTEtablissement) {
                        if ('' === $codePostalInscrit)  {
                            $codePostalInscrit = $etablissement[0]->getCodePostal();
                        }
                        if ('' === $villeInscrit) {
                            $villeInscrit = $etablissement[0]->getVille();
                        }
                        if ('' === $adresseInscrit) {
                            $adresseInscrit = $etablissement[0]->getAdresse();
                        }
                    }

                    $cpVilleAdresse = ($adresseInscrit ? $adresseInscrit : '-')
                        . ' '
                        . ($codePostalInscrit ? $codePostalInscrit : '-')
                        . ' '
                        . ($villeInscrit ? $villeInscrit : '-')
                    ;
                    $textNumOffre = Prado::localize('TEXT_PAPIER');
                    $numOffre = 'PA '.$one->getNumPli();
                    $observations = $offreObject->getObservation();
                }

                if ($typeEnveloppe == Atexo_Config::getParameter('TYPE_ENVELOPPE_ELECTRONIQUE')) {
                    if (is_countable($montantTab) && !empty($montantTab) && count($montantTab[$offreObject->getId()][$typeEnveloppe]) < 1) {
                        //les montants par offres
                        $montants = (new Atexo_ActeDEngagement())->saveMontantOffre(
                            $idEnveloppe,
                            $consultation->getId(),
                            $idLot,
                            Atexo_Config::getParameter('TYPE_ENVELOPPE_ELECTRONIQUE'),
                            $withVariante
                        );
                    } else {
                        $montants = $montantTab[$offreObject->getId()][$typeEnveloppe];
                    }
                } else {
                    $montants = $montantTab[$offreObject->getId()][$typeEnveloppe];
                }
                if (is_array($montants) && $typeEnvTab[$offreObject->getId()] == $typeEnveloppe) {
                    foreach ($montants as $montant) {
                        $colonne = 0;
                        $worksheet->writeString($row, $colonne++, $textNumOffre, $corpsTableau);
                        $worksheet->writeString($row, $colonne++, $numOffre, $corpsTableau);
                        $worksheet->writeString($row, $colonne++, $dateDepot, $corpsTableau);
                        $worksheet->writeString($row, $colonne++, $nomEntreprise, $corpsTableau);
                        $worksheet->writeString($row, $colonne++, $siret, $corpsTableau);
                        $worksheet->write($row, $colonne++, $cpVilleAdresse, $corpsTableau);
                        $worksheet->writeString($row, $colonne++, $one->getLibelleStatutEnveloppe(), $corpsTableau);
                        $worksheet->writeString($row, $colonne++, $montant, $corpsTableau);
                        $worksheet->writeString($row, $colonne++, $observations, $corpsTableau);
                        $worksheet->writeString($row, $colonne++, '', $corpsTableau);
                        ++$row;
                    }
                } else {
                    $worksheet->writeString($row, $colonne++, $textNumOffre, $corpsTableau);
                    $worksheet->writeString($row, $colonne++, $numOffre, $corpsTableau);
                    $worksheet->writeString($row, $colonne++, $dateDepot, $corpsTableau);
                    $worksheet->writeString($row, $colonne++, $nomEntreprise, $corpsTableau);
                    $worksheet->writeString($row, $colonne++, $siret, $corpsTableau);
                    $worksheet->write($row, $colonne++, $cpVilleAdresse, $corpsTableau);
                    $worksheet->writeString($row, $colonne++, $one->getLibelleStatutEnveloppe(), $corpsTableau);
                    $worksheet->writeString($row, $colonne++, '', $corpsTableau);
                    $worksheet->writeString($row, $colonne++, $observations, $corpsTableau);
                    $worksheet->writeString($row, $colonne++, '', $corpsTableau);
                    ++$row;
                }
            }
        }
        $row = $row + 6;
        $worksheet->write($row, 0, Prado::localize('TEXT_OBSERVATIONS_GENERALES'), $merged_cells);
        $worksheet->write($row, 1, '', $merged_cells);
        $worksheet->write($row, 2, '', $merged_cells);
        $worksheet->write($row, 3, '', $merged_cells);
        $worksheet->write($row, 4, '', $merged_cells);

        $row = $row + 6;
        $worksheet->write($row, 0, Prado::localize('TEXT_FAIT_A'), $formatStantard);
        $worksheet->write($row, 1, '', $formatStantard);
        $worksheet->write($row, 2, Atexo_Util::iso2frnDateTime($consultation->getDatefin()), $formatStantard);
        $row = $row + 3;
        $worksheet->write($row, 0, Prado::localize('TEXT_SIGNATURES_AGENTS_AYANT_PROCEDE_A_LOUVERTURE'), $formatStantard);
        $workbook->close();
        $workbook->send($nomfichier);
        exit;
    }

    /**
     * Permet de generer l'excel tableau de bord ECO/SIP.
     *
     * @param $listeAvisPub : tableau des avis a afficher
     * @param $criteriaVo : objet criteres de recherche
     * @param $toFile : permet de specifier si l'avis doit etre stocke ou genere a la volee
     */
    public function genererTableauBordAvis($listeAvisPub, $criteriaVo, $toFile = false)
    {
        (new Atexo_Config())->includePhpLibPath();
        require_once 'Spreadsheet/Excel/Writer.php';

        //Initialisation du fichier excel
        $tmpFileName = Atexo_Config::getParameter('COMMON_TMP').Prado::localize('TEXT_TABLEAU_BORD_AVIS_XLS');
        if ($toFile) {
            $workbook = new Spreadsheet_Excel_Writer($tmpFileName);
        } else {
            $workbook = new Spreadsheet_Excel_Writer();
        }
        $workbook->setCustomColor(11, 0, 204, 255);
        $workbook->setCustomColor(12, 221, 221, 221);
        //Pour passer outre la limite des 255 caracteres
        $workbook->setVersion(8);

        //Style des entetes
        $formatStantardGras = &$workbook->addFormat();
        $formatStantardGras->setColor('black');
        $formatStantardGras->setPattern(1);
        $formatStantardGras->setFgColor('white');
        $formatStantardGras->setSize(8);
        $formatStantardGras->setAlign('left');
        //$formatStantardGras->setOutLine();
        $formatStantardGras->setBold(1);
        $formatStantardGras->setBorder(1);
        $formatStantardGras->setBorderColor(12);

        $formatStantard = &$workbook->addFormat();
        $formatStantard->setColor('black');
        $formatStantard->setPattern(1);
        $formatStantard->setFgColor('white');
        $formatStantard->setSize(8);
        $formatStantard->setAlign('left');
        $formatStantard->setAlign('top');
        $formatStantard->setBold(0);
        $formatStantard->setBorder(1);
        $formatStantard->setBorderColor(12);

        $formatStantardWrap = &$workbook->addFormat();
        $formatStantardWrap->setColor('black');
        $formatStantardWrap->setFgColor('white');
        $formatStantardWrap->setSize(8);
        $formatStantardWrap->setAlign('left');
        $formatStantardWrap->setAlign('top');
        $formatStantardWrap->setBold(0);
        $formatStantardWrap->setTextWrap();
        $formatStantardWrap->setBorder(1);
        $formatStantardWrap->setBorderColor(12);

        $formatEnteteTableau = &$workbook->addFormat();
        $formatEnteteTableau->setColor('black');
        $formatEnteteTableau->setPattern(1);
        $formatEnteteTableau->setFgColor(11);
        $formatEnteteTableau->setSize(8);
        $formatEnteteTableau->setAlign('left');
        //$formatEnteteTableau->setOutLine();
        $formatEnteteTableau->setBold(1);
        $formatEnteteTableau->setAlign('center');
        $formatEnteteTableau->setAlign('top');
        $formatEnteteTableau->setTextWrap();

        $corpsTableau = &$workbook->addFormat();
        $corpsTableau->setColor('black');
        $corpsTableau->setPattern(1);
        $corpsTableau->setFgColor('white');
        $corpsTableau->setSize(8);
        $corpsTableau->setAlign('top');
        //$corpsTableau->setOutLine();
        $corpsTableau->setBold(0);
        $corpsTableau->setTextWrap();
        $corpsTableau->setBorder(1);
        $corpsTableau->setBorderColor(12);
        // la feuille
        $nomOnglet = Prado::localize('TEXT_LISTES_FORMULAIRES_PUBLICITE_AVIS');
        $worksheet = &$workbook->addWorkSheet(Atexo_Util::utf8ToIso($nomOnglet));
        $worksheet->setInputEncoding(Atexo_Config::getParameter('HTTP_ENCODING'));
        //Definition de la largeur de la colonne
        for ($i = 0; $i < 16; ++$i) {
            $worksheet->setColumn($i, $i, '25');
        }

        $worksheet->setLandscape();
        $nomfichier = Prado::localize('TEXT_TABLEAU_BORD_AVIS_XLS').'_'.date('Y').'_'.date('m').'_'.date('d').'.xls';
        $entete = '&"Arial"&C&8'.$nomfichier.' - &A';
        $worksheet->setHeader($entete);
        $worksheet->setFooter(self::getFooterImprimeLe());
        $worksheet->setMarginLeft(0.15);
        $worksheet->setMarginRight(0.7);

        $worksheet->write(0, 0, Prado::localize('TEXT_RECHERCHE_AVANCEE'), $formatStantardGras);
        $objetRecherche = Prado::localize('TEXT_OBJET_RECHERCHE').' : '.Prado::localize('TEXT_LISTES_FORMULAIRES_PUBLICITE_AVIS');
        $objetRecherche .= ((new Atexo_Publicite_AvisPub())->isAccesGestionPubSip()) ? ' ('.Prado::localize('DEFINE_SIP').')' : ' ('.Prado::localize('TEXT_ENTITE_COORDINATRICE').')';
        $worksheet->write(2, 0, $objetRecherche, $formatStantard);
        // Debut bloc criteres de recherche
        $indiceLigne = 3;
        $indiceCol = 1;
        if ('' != $criteriaVo->getReferenceAvis()) {
            $worksheet->write($indiceLigne, 0, Prado::localize('REFERENCE_DE_LA_CONSULTATION').' :', $formatStantard);
            $worksheet->write($indiceLigne++, $indiceCol, $criteriaVo->getReferenceAvis(), $formatStantard);
        }
        if ('' != $criteriaVo->getCritereNomPrenomAgentValidateur()) {
            $worksheet->write($indiceLigne, 0, Prado::localize('NOM_PRENOM_AGENT_VALIDATEUR_PUB_ECO').' :', $formatStantard);
            $worksheet->write($indiceLigne++, $indiceCol, $criteriaVo->getCritereNomPrenomAgentValidateur(), $formatStantard);
        }
        $organisme = Atexo_Organismes::retrieveOrganismeByAcronyme($criteriaVo->getOrganisme());
        $libelleOrg = '';
        if ($organisme instanceof CommonOrganisme) {
            $libelleOrg = $organisme->getDenominationOrgTraduit();
        }
        if ('' != $criteriaVo->getOrganisme()) {
            $worksheet->write($indiceLigne, 0, Prado::localize('ORGANISME').' :', $formatStantard);
            $worksheet->write($indiceLigne++, $indiceCol, $libelleOrg, $formatStantard);
        }
        if ('' != $criteriaVo->getIdService()) {
            $libelleService = '';
            if (0 == $criteriaVo->getIdService()) {
                $libelleService = $organisme->getDenominationOrgTraduit();
            } else {
                $libelleService = Atexo_EntityPurchase::getPathEntityById($criteriaVo->getIdService(), $criteriaVo->getOrganisme(), Atexo_CurrentUser::readFromSession('lang'));
            }
            $worksheet->write($indiceLigne, 0, Prado::localize('TEXT_ENTITE_ACHAT').' :', $formatStantard);
            $worksheet->write($indiceLigne++, $indiceCol, $libelleService, $formatStantard);
        }
        if ('' != $criteriaVo->getIdTypeAvis()) {
            $worksheet->write($indiceLigne, 0, Prado::localize('TEXT_TYPE_AVIS').' :', $formatStantard);
            $worksheet->write($indiceLigne++, $indiceCol, (new CommonTypeAvisPub())->getLibelleAvisById($criteriaVo->getIdTypeAvis()), $formatStantard);
        }
        if ('' != $criteriaVo->getIdSupport() && '0' != $criteriaVo->getIdSupport()) {
            $support = '';
            if ($criteriaVo->getIdSupport() == Atexo_Config::getParameter('TYPE_SUPPORT_SIMAP2')) {
                $support = Prado::localize('TEXT_PORTAIL');
            }
            if ($criteriaVo->getIdSupport() == Atexo_Config::getParameter('TYPE_SUPPORT_OPOCE')) {
                $support = Prado::localize('TEXT_JOUE');
            }
            if ($criteriaVo->getIdSupport() == Atexo_Config::getParameter('TYPE_SUPPORT_PRESSE')) {
                $support = Prado::localize('TEXT_PRESSE');
            }
            $worksheet->write($indiceLigne, 0, Prado::localize('TEXT_DESTINATAIRE').' :', $formatStantard);
            $worksheet->write($indiceLigne++, $indiceCol, $support, $formatStantard);
        }
        //Type procedure
        if ('' != $criteriaVo->getIdTypeProcedure() && '0' != $criteriaVo->getIdTypeProcedure()) {
            $libelleTypeProcedure = '';
            $typesProcedures = Atexo_Consultation_ProcedureType::retrieveProcedureType(true, false);
            $typesProcedures[0] = '--- '.Prado::localize('TEXT_TOUTES_PROCEDURES').' ---';
            $libelleTypeProcedure = $typesProcedures[$criteriaVo->getIdTypeProcedure()];
            $worksheet->write($indiceLigne, 0, Prado::localize('TEXT_TYPE_PROCEDURE').' :', $formatStantard);
            $worksheet->write($indiceLigne++, $indiceCol, $libelleTypeProcedure, $formatStantard);
        }
        if ('' != $criteriaVo->getStatut() && '0' != $criteriaVo->getStatut()) {
            $statutAvis = '';
            if ($criteriaVo->getStatut() == Atexo_Config::getParameter('STATUT_AVIS_ENVOI_PLANIFIE')) {
                $statutAvis = Prado::localize('TEXT_ENVOI_PLANIFIE');
            }
            if ($criteriaVo->getStatut() == Atexo_Config::getParameter('STATUT_AVIS_ENVOYE')) {
                $statutAvis = Prado::localize('TEXT_ENVOYE');
            }
            if ((new Atexo_Publicite_AvisPub())->isAccesGestionPubSip() ) {
                if ($criteriaVo->getStatut() == Atexo_Config::getParameter('STATUT_AVIS_EN_ATTENTE_VALIDATION_SIP')) {
                    $statutAvis = Prado::localize('TEXT_A_TRAITER');
                }
                if ($criteriaVo->getStatut() == Atexo_Config::getParameter('STATUT_AVIS_REJETE_SIP')) {
                    $statutAvis = Prado::localize('TEXT_REJETES');
                }
            } else {
                if ($criteriaVo->getStatut() == Atexo_Config::getParameter('STATUT_AVIS_EN_ATTENTE_VALIDATION_ECO')) {
                    $statutAvis = Prado::localize('TEXT_A_TRAITER');
                }
                if ($criteriaVo->getStatut() == Atexo_Config::getParameter('STATUT_AVIS_EN_ATTENTE_VALIDATION_SIP')) {
                    $statutAvis = Prado::localize('TEXT_ATTENTE_VALIDATION_SIP');
                }
                if ($criteriaVo->getStatut() == Atexo_Config::getParameter('STATUT_AVIS_REJETE_ECO')) {
                    $statutAvis = Prado::localize('TEXT_REJETES');
                }
            }
            $worksheet->write($indiceLigne, 0, Prado::localize('DEFINE_STATUS').' :', $formatStantard);
            $worksheet->write($indiceLigne++, $indiceCol, $statutAvis, $formatStantard);
        }
        if ('' != $criteriaVo->getDateReceptionStart() || '' != $criteriaVo->getDateReceptionEnd()) {
            $worksheet->write($indiceLigne, 0, trim(Prado::localize('TEXT_DATE_RECEPTION').' :'), $formatStantard);
            $dateReception = '';
            if ('' != $criteriaVo->getDateReceptionStart()) {
                $dateReception .= Prado::localize('TEXT_A_PARTIR_DU').' '.$criteriaVo->getDateReceptionStart().' ';
            }
            if ('' != $criteriaVo->getDateReceptionEnd()) {
                $dateReception .= Prado::localize('TEXT_JUSQU_AU').' '.$criteriaVo->getDateReceptionEnd();
            }
            $worksheet->write($indiceLigne++, $indiceCol, $dateReception, $formatStantard);
        }
        if ('' != $criteriaVo->getDateRemisePlisStart() || '' != $criteriaVo->getDateRemisePlisEnd()) {
            $worksheet->write($indiceLigne, 0, trim(Prado::localize('DEFINE_DATE_LIMITE_AR').' :'), $formatStantard);
            $dateRemisePlis = '';
            if ('' != $criteriaVo->getDateRemisePlisStart()) {
                $dateRemisePlis .= Prado::localize('TEXT_A_PARTIR_DU').' '.$criteriaVo->getDateRemisePlisStart().' ';
            }
            if ('' != $criteriaVo->getDateRemisePlisEnd()) {
                $dateRemisePlis .= Prado::localize('TEXT_JUSQU_AU').' '.$criteriaVo->getDateRemisePlisEnd();
            }
            $worksheet->write($indiceLigne++, $indiceCol, $dateRemisePlis, $formatStantard);
        }
        // Fin bloc criteres de recherche

        $colonne = 0;
        $row = $indiceLigne + 1;
        //Definition de la hauteur de la ligne des libelles des colonnes
        $worksheet->setRow($row, '20');

        //Creation entete
        $worksheet->write($row, $colonne++, Prado::localize('DEFINE_PROCEDURE'), $formatEnteteTableau);
        $worksheet->write($row, $colonne++, Prado::localize('DEFINE_AUTEUR'), $formatEnteteTableau);
        $worksheet->write($row, $colonne++, Prado::localize('AGENT_CREATEUR'), $formatEnteteTableau);
        $worksheet->write($row, $colonne++, Prado::localize('AGENT_VALIDATEUR'), $formatEnteteTableau);
        $worksheet->write($row, $colonne++, Prado::localize('VALIDATION_AGENT'), $formatEnteteTableau);
        $worksheet->write($row, $colonne++, Prado::localize('DEFINE_TEXT_REFERENCE'), $formatEnteteTableau);
        $worksheet->write($row, $colonne++, Prado::localize('DEFINE_INTITULE'), $formatEnteteTableau);
        $worksheet->write($row, $colonne++, Prado::localize('OBJET'), $formatEnteteTableau);
        $worksheet->write($row, $colonne++, Prado::localize('ORGANISME'), $formatEnteteTableau);
        $worksheet->write($row, $colonne++, Prado::localize('TEXT_ENTITE_ACHAT'), $formatEnteteTableau);
        $worksheet->write($row, $colonne++, Prado::localize('TEXT_TYPE_AVIS'), $formatEnteteTableau);
        $worksheet->write($row, $colonne++, Prado::localize('TEXT_STATUT_AVIS'), $formatEnteteTableau);
        $worksheet->write($row, $colonne++, Prado::localize('DATE_COTURE'), $formatEnteteTableau);
        $worksheet->write($row, $colonne++, Prado::localize('DATE_DE_MISE_EN_LIGNE_SOUHAITEE'), $formatEnteteTableau);
        $worksheet->write($row, $colonne++, Prado::localize('TEXT_DELAI_SOUMISSION'), $formatEnteteTableau);
        $worksheet->write($row, $colonne++, Prado::localize('TEXT_SUPPORT'), $formatEnteteTableau);
        //fin entete
        if (is_array($listeAvisPub) && count($listeAvisPub)) {
            foreach ($listeAvisPub as $avisPub) {
                ++$row;
                $colonne = 0;
                $worksheet->write($row, $colonne++, $avisPub['libelle_type_procedure'], $corpsTableau);
                $worksheet->write($row, $colonne++, ('' != $avisPub['nom_createur']) ? $avisPub['nom_createur'] : '-', $corpsTableau);
                $worksheet->write($row, $colonne++, ('' != $avisPub['agent_createur_cons']) ? $avisPub['agent_createur_cons'] : '-', $corpsTableau);
                $worksheet->write($row, $colonne++, ('' != $avisPub['agent_validateur_pub']) ? $avisPub['agent_validateur_pub'] : '-', $corpsTableau);
                $worksheet->write($row, $colonne++, ($avisPub['etat_validation']) ? Prado::localize('DEFINE_OUI') : Prado::localize('DEFINE_NON'), $corpsTableau);
                $worksheet->write($row, $colonne++, $avisPub['reference'], $corpsTableau);
                $worksheet->write($row, $colonne++, ($avisPub['intitule']) ? $avisPub['intitule'] : '-', $corpsTableau);
                $worksheet->write($row, $colonne++, ($avisPub['Objet']) ? $avisPub['Objet'] : '-', $corpsTableau);
                $worksheet->write($row, $colonne++, $avisPub['sigleOrg'], $corpsTableau);
                $worksheet->write($row, $colonne++, $avisPub['libelleService'], $corpsTableau);
                $worksheet->write($row, $colonne++, ($avisPub['libelle_type_avis']) ? $avisPub['libelle_type_avis'] : '-', $corpsTableau);
                $worksheet->write($row, $colonne++, (new Atexo_Publicite_AvisPub())->getLibelleStatutAvisPub($avisPub['id_avis'], $avisPub['organisme']), $corpsTableau);
                $worksheet->write($row, $colonne++, (Atexo_Util::iso2frnDateTime($avisPub['datefin'], true)) ? Atexo_Util::iso2frnDateTime($avisPub['datefin'], true) : '-', $corpsTableau);
                $worksheet->write($row, $colonne++, ($avisPub['date_mise_en_ligne_souhaitee']) ? Atexo_Util::iso2frnDateTime($avisPub['date_mise_en_ligne_souhaitee'], true) : ' - ', $corpsTableau);
                $worksheet->write($row, $colonne++, (int) Atexo_Util::diffJours(
                    Atexo_Util::iso2frnDateTime(substr($avisPub['datefin'], 0, 10)),
                    Atexo_Util::iso2frnDate(substr($avisPub['date_mise_en_ligne_souhaitee'], 0, 10))
                ).' '.
                                                                               Prado::localize('TEXT_JOURS'), $corpsTableau);
                $listeDestinataires = (new Atexo_Publicite_AvisPub())->retrieveListeDestinatairesAvis($avisPub['id_avis'], $avisPub['organisme']);
                $listeSupport = [];
                if (is_array($listeDestinataires) && count($listeDestinataires)) {
                    foreach ($listeDestinataires as $dest) {
                        if ($dest instanceof CommonDestinatairePub) {
                            if ($dest->getIdSupport() == Atexo_Config::getParameter('TYPE_SUPPORT_OPOCE')) {
                                $listeSupport[] = Prado::localize('TEXT_JOUE');
                            } elseif ($dest->getIdSupport() == Atexo_Config::getParameter('TYPE_SUPPORT_SIMAP2')) {
                                $listeSupport[] = Prado::localize('TEXT_PORTAIL');
                            } elseif ($dest->getIdSupport() == Atexo_Config::getParameter('TYPE_SUPPORT_PRESSE')) {
                                $listeSupport[] = Prado::localize('TEXT_PRESSE');
                            }
                        }
                    }
                }
                $worksheet->write($row, $colonne++, implode('/', $listeSupport), $corpsTableau);
            }
        }
        $workbook->close();
        if (!$toFile) {
            $workbook->send($nomfichier);
        } else {
            $pathFichier = Atexo_Config::getParameter('COMMON_TMP').$nomfichier;
            //echo "mv " . $tmpFileName . $pathFichier . " 2>&1 > ".Atexo_Config::getParameter('COMMON_TMP')."zip_error", $sys_answer; exit();
            system('mv '.$tmpFileName." '".$pathFichier."' > ".Atexo_Config::getParameter('COMMON_TMP').'zip_error 2>&1');

            return $pathFichier;
        }
        exit;
    }

    public function genererExcelStatistiquesEntreprise($arrayValues)
    {
        (new Atexo_Config())->includePhpLibPath();
        require_once 'Spreadsheet/Excel/Writer.php';
        $workbook = new Spreadsheet_Excel_Writer();

        $workbook->setCustomColor(11, 51, 204, 255);
        $workbook->setCustomColor(12, 221, 221, 221);
        //Pour passer outre la limite des 255 caracteres
        $workbook->setVersion(8);

        //Style des entetes
        $formatStantardGras = &$workbook->addFormat();
        $formatStantardGras->setSize(10);
        $formatStantardGras->setAlign('left');
        $formatStantardGras->setBold(1);

        $formatStantardGrasAlignRight = &$workbook->addFormat();
        $formatStantardGrasAlignRight->setSize(10);
        $formatStantardGrasAlignRight->setAlign('right');
        $formatStantardGrasAlignRight->setBold(1);
        $formatStantardGrasAlignRight->setBorder(1);
        $formatStantardGrasAlignRight->setNumFormat('#,##0');

        $formatStantardGrasBorder = &$workbook->addFormat();
        $formatStantardGrasBorder->setFgColor(11);
        $formatStantardGrasBorder->setSize(10);
        $formatStantardGrasBorder->setAlign('top');
        $formatStantardGrasBorder->setBold(1);
        $formatStantardGrasBorder->setTextWrap();
        $formatStantardGrasBorder->setBorder(1);

        $formatStantard = &$workbook->addFormat();
        $formatStantard->setSize(10);
        $formatStantard->setAlign('left');
        $formatStantard->setBold(0);

        $formatEnteteTableau = &$workbook->addFormat();
        $formatEnteteTableau->setFgColor(11);
        $formatEnteteTableau->setSize(10);
        $formatEnteteTableau->setAlign('left');
        $formatEnteteTableau->setBold(0);
        $formatEnteteTableau->setAlign('center');
        $formatEnteteTableau->setAlign('top');
        $formatEnteteTableau->setBorder(1);
        $formatEnteteTableau->setTextWrap();

        $corpsTableau = &$workbook->addFormat();
        $corpsTableau->setSize(10);
        $corpsTableau->setAlign('top');
        //$corpsTableau->setOutLine();
        $corpsTableau->setBold(0);
        $corpsTableau->setTextWrap();
        $corpsTableau->setBorder(1);
        $corpsTableau->setLeft(1);
        $corpsTableau->setRight(1);
        $corpsTableau->setNumFormat('#,##0');

        $formatEnteteLigne = &$workbook->addFormat();
        $formatEnteteLigne->setSize(10);
        $formatEnteteLigne->setAlign('top');
        //$formatEnteteLigne->setOutLine();
        $formatEnteteLigne->setBold(1);
        $formatEnteteLigne->setTextWrap();
        $formatEnteteLigne->setBorder(1);
        $formatEnteteLigne->setLeft(1);
        $formatEnteteLigne->setRight(1);

        $marginLeft = &$workbook->addFormat();
        $marginLeft->setLeft(1);

        $marginRight = &$workbook->addFormat();
        $marginRight->setRight(1);

        $worksheet = &$workbook->addWorkSheet(Atexo_Util::utf8ToIso(Prado::localize('DEFINE_TEXT_STATISTIQUES_ENTREPRISE')));
        if ('iso-8859-1' != Atexo_Config::getParameter('HTTP_ENCODING')) {
            $worksheet->setInputEncoding(Atexo_Config::getParameter('HTTP_ENCODING'));
        }

        $worksheet->setLandscape();
        $worksheet->hideGridlines();

        $nomFichier = Prado::localize('TEXT_EXPORTS_STATISTIQUES_').date('Ymd');
        $worksheet->setHeader('&"Arial"&8'.$nomFichier.' - '.Prado::localize('DEFINE_TEXT_STATISTIQUES_ENTREPRISE'), '0.51');
        $worksheet->setFooter(self::getFooterImprimeLe(), '0.51');

        $worksheet->setMarginLeft('0.78');
        $worksheet->setMarginRight('0.78');
        $worksheet->setMarginTop('1.01');
        $worksheet->setMarginBottom('1.01');
        //Taille de la premere colonne

        $worksheet->setColumn(0, 0, '90');
        $worksheet->setColumn(1, 1, '5');

        $worksheet->write(0, 0, Prado::localize('DEFINE_TEXTE_SUIVI_DONNEES_ENTREPRISE'), $formatStantardGras);

        $nombreEntrepriseFrance = (new Atexo_Statistiques_RequetesStatistiques())->getNbreEntrepriseEnregistre(true);
        $nombreEntrepriseEtranger = (new Atexo_Statistiques_RequetesStatistiques())->getNbreEntrepriseEnregistre(false);
        $nombreUtilisateurResponsable = (new Atexo_Statistiques_RequetesStatistiques())->getNbrResponsaleEntreprise(true);
        $nbreCompteUserEse = number_format((new Atexo_Statistiques_RequetesStatistiques())->getNbrResponsaleEntreprise() + $nombreUtilisateurResponsable, '0', ',', ' ');
        $nombreUtilisateurResponsable = (new Atexo_Statistiques_RequetesStatistiques())->getNbrResponsaleEntreprise(true);

        $unikDepotAuPlus = (new Atexo_Statistiques_RequetesStatistiques())->getNombreEntreprisesAyantFait1SeulEtUniqueDepotOffreConcernantAuPlus1eConsultation();
        $plsrsDepotsAuPlus = (new Atexo_Statistiques_RequetesStatistiques())->getNombreEntreprisesAyantFait1SeulEtUniqueDepotOffreConcernantAuPlus1eConsultation(true);

        $EseDocCoffreFort = (new Atexo_Statistiques_RequetesStatistiques())->getCountjustificatifs();
        $compteUserServices = (new Atexo_Statistiques_RequetesStatistiques())->getEntrepriseByCategorie(Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES'));
        $compteUserTravaux = (new Atexo_Statistiques_RequetesStatistiques())->getEntrepriseByCategorie(Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX'));
        $compteUserFournitures = (new Atexo_Statistiques_RequetesStatistiques())->getEntrepriseByCategorie(Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES'));
        if (Atexo_Module::isEnabled('StatutCompteEntreprise')) {
            $nbrEntrpEA = (new Atexo_Statistiques_RequetesStatistiques())->getNbreEntrepriseEA();
            $nbrEntrpSIAE = (new Atexo_Statistiques_RequetesStatistiques())->getNbreEntrepriseSIAE();
        }

        $row = 3;
        $worksheet->write($row, 0, Prado::localize('DEFINE_TEXT_STATISTIQUES_PORTAIL'), $formatEnteteTableau);
        $worksheet->write($row++, 1, '', $corpsTableau);

        $worksheet->write($row, 0, Prado::localize('DEFINE_TEXT_ENTREPRISE_ENREGISTRE'), $corpsTableau);
        $worksheet->write($row++, 1, number_format(($nombreEntrepriseFrance + $nombreEntrepriseEtranger), '0', ',', ' '), $corpsTableau);

        $worksheet->write($row, 0, ' -'.Prado::localize('DEFINE_TEXT_SIEGE_FRANCE'), $corpsTableau);
        $worksheet->write($row++, 1, $nombreEntrepriseFrance, $corpsTableau);

        $worksheet->write($row, 0, ' -'.Prado::localize('DEFINE_TEXT_SIEGE_ETRANGER'), $corpsTableau);
        $worksheet->write($row++, 1, $nombreEntrepriseEtranger, $corpsTableau);

        $worksheet->write($row, 0, Prado::localize('DEFINE_TEXT_NOMBRE_UTILISATEUR_ENTREPRISE_AYANT_COMPTE_PLATE_FORME'), $corpsTableau);
        $worksheet->write($row++, 1, $nbreCompteUserEse, $corpsTableau);

        $worksheet->write($row, 0, Prado::localize('DEFINE_TEXT_RESPONSABLE_ENTREPRISE'), $corpsTableau);
        $worksheet->write($row++, 1, number_format($nombreUtilisateurResponsable, '0', ',', ' '), $corpsTableau);

        $worksheet->write($row, 0, Prado::localize('DEFINE_TEXT_NOMBRE_INSCRIT_ALERTE_ELECTRONIQUE'), $corpsTableau);
        $worksheet->write($row++, 1, number_format((new Atexo_Statistiques_RequetesStatistiques())->getNbrInscritAlerteElectronique(), '0', ',', ' '), $corpsTableau);

        $worksheet->write($row, 0, trim(Prado::localize('DEFINE_TEXT_REPARTITION_ENTREPRISE_CATEGORIE')), $corpsTableau);
        $worksheet->write($row++, 1, '', $corpsTableau);

        $worksheet->write($row, 0, ' -'.Prado::localize('DEFINE_TRAVAUX'), $corpsTableau);
        $worksheet->write($row++, 1, number_format($compteUserTravaux, '0', ',', ' '), $corpsTableau);

        $worksheet->write($row, 0, ' -'.Prado::localize('DEFINE_FOURNITURES'), $corpsTableau);
        $worksheet->write($row++, 1, number_format($compteUserFournitures, '0', ',', ' '), $corpsTableau);

        $worksheet->write($row, 0, ' -'.Prado::localize('DEFINE_SERVICES'), $corpsTableau);
        $worksheet->write($row++, 1, number_format($compteUserServices, '0', ',', ' '), $corpsTableau);

        $worksheet->write($row, 0, Prado::localize('DEFINE_TEXT_ENTREPRISE_AYANT_DEPOSE_DOCUMENT_COFFRE_FORT'), $corpsTableau);
        $worksheet->write($row++, 1, number_format($EseDocCoffreFort, '0', ',', ' '), $corpsTableau);

        $worksheet->write($row, 0, Prado::localize('TEXT_NOMBRE_ENTREPRISE_PRIMO_DEPOSANTE'), $corpsTableau);
        $worksheet->write($row++, 1, number_format($unikDepotAuPlus, '0', ',', ' '), $corpsTableau);

        $worksheet->write($row, 0, Prado::localize('TEXT_NOMBRE_ENTREPRISE_MULTI_DEPOSANTE'), $corpsTableau);
        $worksheet->write($row++, 1, number_format($plsrsDepotsAuPlus, '0', ',', ' '), $corpsTableau);

        if (Atexo_Module::isEnabled('StatutCompteEntreprise')) {
            $worksheet->write($row, 0, Prado::localize('DEFINE_NOMBRE_ENTREPRISE_ESAT_EA'), $corpsTableau);
            $worksheet->write($row++, 1, number_format($nbrEntrpEA, '0', ',', ' '), $corpsTableau);
            $worksheet->write($row, 0, Prado::localize('DEFINE_NOMBRE_ENTREPRISE_SIAE'), $corpsTableau);
            $worksheet->write($row++, 1, number_format($nbrEntrpSIAE, '0', ',', ' '), $corpsTableau);
        }

        $row += 2;
        $worksheet->write($row, 0, Prado::localize('DEFINE_TEXT_STATISTIQUES_PORTAIL_EN_COURS'), $formatEnteteTableau);
        $worksheet->write($row++, 1, '', $corpsTableau);

        $worksheet->write($row, 0, Prado::localize('DEFINE_TEXT_NOMBRE_RETRAITS_DCE'), $corpsTableau);
        $worksheet->write($row++, 1, number_format($arrayValues['nbrDce'], '0', ',', ' '), $corpsTableau);

        $worksheet->write($row, 0, ' -'.Prado::localize('DEFINE_TEXT_RETRAITS_DCE_DES_UTILISATEUR_PLATE_FORME'), $corpsTableau);
        $worksheet->write($row++, 1, number_format($arrayValues['nbrDceParInscrit'], '0', ',', ' '), $corpsTableau);

        $worksheet->write($row, 0, Prado::localize('DEFINE_TEXT_QUESTIONS_POSEES'), $corpsTableau);
        $worksheet->write($row++, 1, number_format($arrayValues['nbrQestions'], '0', ',', ' '), $corpsTableau);

        $worksheet->write($row, 0, Prado::localize('DEFINE_TEXT_REPONSE_ELECTRONIQUE'), $corpsTableau);
        $worksheet->write($row++, 1, number_format($arrayValues['nbreReponses'], '0', ',', ' '), $corpsTableau);

        $worksheet->write($row, 0, ' -'.Prado::localize('DEFINE_TEXT_AVEC_SIGNATURE_ELECTRONIQUE'), $corpsTableau);
        $worksheet->write($row++, 1, number_format($arrayValues['nbreReponsesAvecSignature'], '0', ',', ' '), $corpsTableau);

        $worksheet->write($row, 0, ' -'.Prado::localize('DEFINE_TEXT_SANS_SIGNATURE_ELECTRONIQUE'), $corpsTableau);
        $worksheet->write($row++, 1, number_format($arrayValues['nbreReponsesSansSignature'], '0', ',', ' '), $corpsTableau);

        $workbook->close();
        $workbook->send($nomFichier.'.xls');
        exit;
    }

    /**
     * Permet de generer l'excel des ordres du jour d'une seance de commission.
     *
     * @param $seance : seance de commission concernee
     * @param $listeOrdreDuJour : ordre du jour sous forme de liste des ordres de passage
     * @param $organisme : acronyme de l'organisme
     * @param $toFile : specifie si l'ordre du jour doit etre stocke ou genere a la volee
     */
    public function telechargerOrdreDuJour($seance, $listeOrdreDuJour, $organisme, $toFile = false)
    {
        (new Atexo_Config())->includePhpLibPath();
        require_once 'Spreadsheet/Excel/Writer.php';

        //Verification de l'existence du repertoire temporaire CAO, creation le cas echeant
        $pathDir = Atexo_Config::getParameter('CAO_TMP');
        if (!is_dir($pathDir)) {
            mkdir($pathDir, 0755, true);
        }

        //Initialisation du fichier excel
        $commission = $seance->getCommonTCAOCommission();
        $acroCommission = $commission->getSigle();
        $dateSeance = new DateTime($seance->getDate());
        $dateSeance = $dateSeance->format('Y-m-d_H-i');
        $nomfichier = Atexo_Config::getParameter('CAO_FICHIER_ORDRE_DU_JOUR').'_'.$dateSeance.'_'.$acroCommission.Atexo_Config::getParameter('CAO_EXTENSION_XLS');
        $arrayNomFichier = explode('.', $nomfichier);
        $tmpFileName = Atexo_Config::getParameter('CAO_TMP').$arrayNomFichier[0].'_'.session_id().'.'.$arrayNomFichier[1];

        if ($toFile) {
            $workbook = new Spreadsheet_Excel_Writer($tmpFileName);
        } else {
            $workbook = new Spreadsheet_Excel_Writer();
        }
        $workbook->setCustomColor(11, 0, 204, 255);
        $workbook->setCustomColor(12, 221, 221, 221);
        //Pour passer outre la limite des 255 caractères
        $workbook->setVersion(8);

        //Style des entetes
        $formatStantardGras = &$workbook->addFormat();
        $formatStantardGras->setColor('black');
        $formatStantardGras->setPattern(1);
        $formatStantardGras->setFgColor('white');
        $formatStantardGras->setSize(8);
        $formatStantardGras->setAlign('left');
        //$formatStantardGras->setOutLine();
        $formatStantardGras->setBold(1);
        $formatStantardGras->setBorder(1);
        $formatStantardGras->setBorderColor(12);

        $formatStantard = &$workbook->addFormat();
        $formatStantard->setColor('black');
        $formatStantard->setPattern(1);
        $formatStantard->setFgColor('white');
        $formatStantard->setSize(8);
        $formatStantard->setAlign('left');
        $formatStantard->setAlign('top');
        $formatStantard->setBold(0);
        $formatStantard->setBorder(1);
        $formatStantard->setBorderColor(12);

        $formatStantardWrap = &$workbook->addFormat();
        $formatStantardWrap->setColor('black');
        $formatStantardWrap->setFgColor('white');
        $formatStantardWrap->setSize(8);
        $formatStantardWrap->setAlign('left');
        $formatStantardWrap->setAlign('top');
        $formatStantardWrap->setBold(0);
        $formatStantardWrap->setTextWrap();
        $formatStantardWrap->setBorder(1);
        $formatStantardWrap->setBorderColor(12);

        $formatEnteteTableau = &$workbook->addFormat();
        $formatEnteteTableau->setColor('black');
        $formatEnteteTableau->setPattern(1);
        $formatEnteteTableau->setFgColor(11);
        $formatEnteteTableau->setSize(8);
        $formatEnteteTableau->setAlign('left');
        $formatEnteteTableau->setBold(1);
        $formatEnteteTableau->setAlign('center');
        $formatEnteteTableau->setAlign('top');
        $formatEnteteTableau->setTextWrap();

        $corpsTableau = &$workbook->addFormat();
        $corpsTableau->setColor('black');
        $corpsTableau->setPattern(1);
        $corpsTableau->setFgColor('white');
        $corpsTableau->setSize(8);
        $corpsTableau->setAlign('top');
        $corpsTableau->setAlign('center');
        $corpsTableau->setBold(0);
        $corpsTableau->setTextWrap();
        $corpsTableau->setBorder(1);
        $corpsTableau->setBorderColor(12);
        // la feuille
        $nomOnglet = Prado::localize('DEFINE_ORDRE_DU_JOUR');
        $worksheet = &$workbook->addWorkSheet(Atexo_Util::utf8ToIso($nomOnglet));
        $worksheet->setInputEncoding(Atexo_Config::getParameter('HTTP_ENCODING'));

        //Mise en page pour l'impression
        $worksheet->setLandscape();
        $worksheet->setPrintScale(65);
        $worksheet->hideGridlines();

        //Definition de la largeur de la colonne
        for ($i = 0; $i < 16; ++$i) {
            $worksheet->setColumn($i, $i, '25');
        }

        $entete = '&"Arial"&C&8'.$nomfichier.' - &A';
        $worksheet->setHeader($entete);
        $worksheet->setFooter(self::getFooterImprimeLe());
        $worksheet->setMarginLeft(0.3);
        $worksheet->setMarginRight(0.3);
        $worksheet->setMarginTop(0.3);
        $worksheet->setMarginBottom(0.3);

        $colonne = 0;
        $row = 0;

        //Titre en haut de la feuille
        $worksheet->write($row++, $colonne, Prado::localize('DEFINE_IDENTIFICATION_SEANCE'), $formatStantardGras);
        ++$row;

        //Bloc de rappel des informations sur la seance
        //Recuperation de l'organisme pour avoir la denomination
        $org_item = CommonOrganismeQuery::create()
           ->filterByAcronyme($organisme)
           ->findOne();
        $libelle_organisme = $org_item->getDenominationOrg();

        //Organisme

        $worksheet->write($row, $colonne++, Prado::localize('ORGANISME'), $formatStantardGras);
        $worksheet->write($row++, $colonne, $libelle_organisme, $formatStantard);
        //Type de commission
        $colonne = 0;
        $worksheet->write($row, $colonne++, Prado::localize('DEFINE_TYPE_COMMISSION'), $formatStantardGras);
        $worksheet->write($row++, $colonne, $seance->getCommonTCAOCommission()->getIntitule(), $formatStantard);
        //Date de la seance
        $colonne = 0;
        $worksheet->write($row, $colonne++, Prado::localize('DEFINE_TEXT_DATE_HEURE'), $formatStantardGras);
        $worksheet->write($row++, $colonne, Atexo_Util::iso2frnDateTime($seance->getDate()), $formatStantard);
        //Lieu de la seance
        $colonne = 0;
        $worksheet->write($row, $colonne++, Prado::localize('DEFINE_LIEU'), $formatStantardGras);
        $worksheet->write($row++, $colonne, $seance->getLieu(), $formatStantard);
        //Salle de la seance
        $colonne = 0;
        $worksheet->write($row, $colonne++, Prado::localize('DEFINE_SALLE'), $formatStantardGras);
        $worksheet->write($row++, $colonne, $seance->getSalle(), $formatStantard);
        //Statut de la seance
        $colonne = 0;
        $worksheet->write($row, $colonne++, Prado::localize('DEFINE_STATUT_SEANCE'), $formatStantardGras);
        $worksheet->write($row++, $colonne, (new Atexo_Commission_Seance())->getLibelleStatut($seance->getIdRefValStatut()), $formatStantard);

        ++$row;

        //Definition de la hauteur de la ligne des libelles des colonnes
        $worksheet->setRow($row, '20');

        //Creation entete du tableau des consultations a l'ordre du jour
        $colonne = 0;
        $worksheet->write($row, $colonne++, Prado::localize('DEFINE_HEURE_PASSAGE'), $formatEnteteTableau);
        $worksheet->write($row, $colonne++, Prado::localize('ETAPE'), $formatEnteteTableau);
        $worksheet->write($row, $colonne++, Prado::localize('TEXT_TYPE_PROCEDURE'), $formatEnteteTableau);
        $worksheet->write($row, $colonne++, Prado::localize('DEFINE_TEXT_REFERENCE'), $formatEnteteTableau);
        $worksheet->write($row, $colonne++, Prado::localize('OBJET'), $formatEnteteTableau);
        $worksheet->write($row, $colonne++, Prado::localize('TEXT_ENTITE_ACHAT'), $formatEnteteTableau);
        $worksheet->write($row, $colonne++, Prado::localize('DATE_COTURE'), $formatEnteteTableau);
        $worksheet->write($row, $colonne++, Prado::localize('INVITES_PONCTUELS'), $formatEnteteTableau);

        //Parcours de la liste des consultations a l'ordre du jour
        foreach ($listeOrdreDuJour as $ordreDuJour) {
            ++$row;
            $colonne = 0;

            //Ajustement de la hauteur de la ligne (ajustement automatique pas possible avec spreedsheet)
            $worksheet->setRow($row, '60');

            //Recuperation de la date de passage
            $datePassage = $ordreDuJour->getDatePassage();
            $_t_date = explode(' ', $datePassage);
            $heurePassage = $_t_date[1];
            $worksheet->write($row, $colonne++, $heurePassage, $corpsTableau);

            //Recuperation de l'etape
            $idEtape = $ordreDuJour->getIdRefOrgValEtape();
            if ($idEtape) {
                $etape = (new Atexo_Commission_Seance())->getEtapeListe($idEtape);
            } else {
                $etape = '-';
            }
            $worksheet->write($row, $colonne++, $etape, $corpsTableau);

            //Recuperation du type de procedure
            $libelleProc = (new Atexo_Consultation_ProcedureType())->retrieveTypeProcedure($ordreDuJour->getCommonTCAOCommissionConsultation()->getIdTypeProcedure(), false, $organisme, Atexo_CurrentUser::readFromSession('lang'));
            $worksheet->write($row, $colonne++, $libelleProc, $corpsTableau);

            //Recuperation de la reference
            if (!$ordreDuJour->getCommonTCAOCommissionConsultation()->getConsultationId()) {
                $worksheet->write($row, $colonne++, $ordreDuJour->getCommonTCAOCommissionConsultation()->getRefLibre(), $corpsTableau);
            } else {
                $worksheet->write($row, $colonne++, $ordreDuJour->getCommonTCAOCommissionConsultation()->getConsultationId(), $corpsTableau);
            }

            //Recuperation de l'intitule
            $worksheet->write($row, $colonne++, ($ordreDuJour->getCommonTCAOCommissionConsultation()->getIntitule()) ? $ordreDuJour->getCommonTCAOCommissionConsultation()->getIntitule() : '-', $corpsTableau);

            //Recuperation du service gestionnaire
            if (null != $ordreDuJour->getCommonTCAOCommissionConsultation()->getServiceGestionnaire()) {
                $worksheet->write($row, $colonne++, $ordreDuJour->getCommonTCAOCommissionConsultation()->getServiceGestionnaire(), $corpsTableau);
            } else {
                $libelleService = '-';
                if (0 == (int) $ordreDuJour->getCommonTCAOCommissionConsultation()->getIdServiceGestionnaire()) {
                    $objetOrganisme = Atexo_Organismes::retrieveOrganismeByAcronyme(Atexo_CurrentUser::getCurrentOrganism());
                    if ($objetOrganisme instanceof CommonOrganisme) {
                        $libelleService = $objetOrganisme->getDenominationOrgTraduit();
                    }
                } else {
                    $libelleService = Atexo_EntityPurchase::getPathEntityById($ordreDuJour->getCommonTCAOCommissionConsultation()->getIdServiceGestionnaire(), $ordreDuJour->getOrganisme(), Atexo_Languages::readLanguageFromSession());
                }
                $worksheet->write($row, $colonne++, $libelleService, $corpsTableau);
            }

            //Recuperation de la date de cloture
            $worksheet->write($row, $colonne++, (Atexo_Util::iso2frnDateTime($ordreDuJour->getCommonTCAOCommissionConsultation()->getDateCloture())) ? Atexo_Util::iso2frnDateTime($ordreDuJour->getCommonTCAOCommissionConsultation()->getDateCloture()) : '-', $corpsTableau);

            //Recuperation de la liste des invites ponctuels
            $listeInvitesPonctuelODJ = (new Atexo_Commission_Seance())->getAllInvitesOrdreDePassage($ordreDuJour->getIdOrdreDePassage(), $organisme);

            $prenomNomInvites = '';
            if (is_array($listeInvitesPonctuelODJ) && count($listeInvitesPonctuelODJ)) {
                foreach ($listeInvitesPonctuelODJ as $invite) {
                    $prenomNomInvites .= $invite->getPrenom().' '.$invite->getNom()."\n";
                }
            }
            $worksheet->write($row, $colonne++, ('' != $prenomNomInvites) ? $prenomNomInvites : '-', $corpsTableau);
        }
        $workbook->close();
        if (!$toFile) {
            $workbook->send($nomfichier);
        } else {
            return $tmpFileName;
        }
        exit;
    }

    /*
     * Permet de generer l'excel de l'annuaire des agents
     */
    public function generateExcelAnnuaireAgents(Atexo_Agent_CriteriaVo $criteriaVo, $toFile = false)
    {
        if ('' != Atexo_Config::getParameter('CODE_LIBELLE_REF_AGENT_FOR_SEARCH')) {
            $agents = Atexo_Agent::search($criteriaVo, false, Atexo_Config::getParameter('CODE_LIBELLE_REF_AGENT_FOR_SEARCH'));
        } else {
            $agents = Atexo_Agent::search($criteriaVo);
        }
        $acronymeOrganisme = $criteriaVo->getOrganisme();
        $organisme = Atexo_Organismes::retrieveOrganismeByAcronyme($criteriaVo->getOrganisme());
        $nomOrganisme = '';
        if ($organisme instanceof CommonOrganisme) {
            $nomOrganisme = $organisme->getDenominationOrg();
        } else {
            $nomOrganisme = Prado::localize('TEXT__TOUS_MINISTERES');
        }

        $nomService = '';

        (new Atexo_Config())->includePhpLibPath();
        require_once 'Spreadsheet/Excel/Writer.php';

        if ($toFile) {
            $tmpFileName = Atexo_Config::getParameter('COMMON_TMP').Prado::localize('TEXT_REGISTRE_HABILITATION').session_id().'_'.time().'.xls';
            $workbook = new Spreadsheet_Excel_Writer($tmpFileName);
        } else {
            $workbook = new Spreadsheet_Excel_Writer();
        }

        $workbook->setCustomColor(11, 255, 255, 150);
        $workbook->setCustomColor(12, 192, 192, 192);
        $workbook->setCustomColor(13, 0, 0, 255);
        $workbook->setCustomColor(14, 153, 204, 255);
        //Pour passer outre la limite des 255 caracteres
        $workbook->setVersion(8);

        //Style des entetes
        $stylePolice10GrasBleu = &$workbook->addFormat();
        $stylePolice10GrasBleu->setColor(13);
        $stylePolice10GrasBleu->setSize(10);
        $stylePolice10GrasBleu->setAlign('left');
        $stylePolice10GrasBleu->setBold(1);

        $stylePolice10BleuSimple = &$workbook->addFormat();
        $stylePolice10BleuSimple->setColor(13);
        $stylePolice10BleuSimple->setSize(10);
        $stylePolice10BleuSimple->setAlign('left');

        $styleSimplePolice10 = &$workbook->addFormat();
        $styleSimplePolice10->setColor('black');
        $styleSimplePolice10->setSize(10);
        $styleSimplePolice10->setAlign('left');

        $stylePolice8GrasBleu = &$workbook->addFormat();
        $stylePolice8GrasBleu->setColor(13);
        $stylePolice8GrasBleu->setSize(8);
        $stylePolice8GrasBleu->setAlign('left');
        $stylePolice8GrasBleu->setBold(1);

        $styleSimplePolice8 = &$workbook->addFormat();
        $styleSimplePolice8->setColor('black');
        $styleSimplePolice8->setSize(8);
        $styleSimplePolice8->setAlign('left');

        $styleSimplePolice8FondJaune = &$workbook->addFormat();
        $styleSimplePolice8FondJaune->setColor('black');
        $styleSimplePolice8FondJaune->setSize(8);
        $styleSimplePolice8FondJaune->setAlign('left');
        $styleSimplePolice8FondJaune->setFgColor(11);

        $styleSimplePolice8FondGris = &$workbook->addFormat();
        $styleSimplePolice8FondGris->setColor('black');
        $styleSimplePolice8FondGris->setSize(8);
        $styleSimplePolice8FondGris->setAlign('left');
        $styleSimplePolice8FondGris->setFgColor(12);

        $styleSimplePolice8FondBleu = &$workbook->addFormat();
        $styleSimplePolice8FondBleu->setColor('black');
        $styleSimplePolice8FondBleu->setSize(8);
        $styleSimplePolice8FondGris->setAlign('left');
        $styleSimplePolice8FondBleu->setFgColor(14);

        $worksheet = &$workbook->addWorkSheet(Atexo_Util::utf8ToIso(Prado::localize('TEXT_AGENTS')));
        if ('iso-8859-1' != Atexo_Config::getParameter('HTTP_ENCODING')) {
            $worksheet->setInputEncoding(Atexo_Config::getParameter('HTTP_ENCODING'));
        }

        $worksheet->hideGridlines();
        $nomFichier = Prado::localize('TEXT_REGISTRE_AGENTS_XLS');
        $worksheet->setHeader('&"Arial"&8'.$nomFichier.' - '.Prado::localize('DEFINE_HABILITATION'), '0.19');
        $worksheet->setFooter(self::getFooterImprimeLe(), '0.19');

        $worksheet->setMarginLeft('0.19');
        $worksheet->setMarginRight('0.19');
        $worksheet->setMarginTop('0.39');
        $worksheet->setMarginBottom('0.39');

        $worksheet->setColumn(0, 0, '3.14');
        $worksheet->setColumn(1, 1, '42.86');

        for ($i = 2; $i < 8; ++$i) {
            if (7 == $i) { // la colonne "Entite de rattachement (SIGLE / SIGLE / SIGLE - Libelle)"
                $worksheet->setColumn($i, $i, '69.86');
            } else {
                $worksheet->setColumn($i, $i, '42.86');
            }
        }

        $worksheet->write(0, 0, Prado::localize('TEXT_REGISTRE_AGENTS'), $stylePolice10GrasBleu);

        $worksheet->write(1, 0, Prado::localize('DEFINE_TEXT_ENTITE_PUBLIC').' :', $stylePolice10BleuSimple);
        $worksheet->write(1, 2, $nomOrganisme, $styleSimplePolice10);

        if ($agents && is_array($agents)) {
            $colonne = 1;
            $row = 5;
            //Initialisation du fichier excel
            $worksheet->write(5, $colonne++, Prado::localize('DEFINE_NOM_MAJ'), $stylePolice8GrasBleu);
            $worksheet->write(5, $colonne++, Prado::localize('DEFINE_PRENOM'), $stylePolice8GrasBleu);
            $worksheet->write(5, $colonne++, Prado::localize('TEXT_TEL'), $stylePolice8GrasBleu);
            $worksheet->write(5, $colonne++, Prado::localize('TEXT_FAX'), $stylePolice8GrasBleu);
            $worksheet->write(5, $colonne++, Prado::localize('ADRESSE_ELECTRONIQUE'), $stylePolice8GrasBleu);
            $worksheet->write(5, $colonne++, Prado::localize('TEXT_IDENTIFIANT_SANS_POINTS'), $stylePolice8GrasBleu);
            $worksheet->write(5, $colonne++, Prado::localize('TEXT_ENTITE_RATTACHEMENT_SIGLE_SIGLE_LIBELLE'), $stylePolice8GrasBleu);
            //liste des referentiels agent
            $refsAgent = (new Atexo_Referentiel_Referentiel())->getAllLtReferentielForAgent();
            if (is_array($refsAgent) && count($refsAgent) > 0) {
                foreach ($refsAgent as $oneRef) {
                    $worksheet->setColumn($colonne, $colonne, '42.86');
                    $worksheet->write(5, $colonne++, Prado::localize($oneRef->getCodeLibelle()), $stylePolice8GrasBleu);
                }
            }
            ++$row;

            // la feuille (konoha)
            $row = 6;
            foreach ($agents as $agent) {
                $colonne = 1;
                $entiteRattachement = '';

                //$entiteRattachement=Atexo_EntityPurchase::getPathEntityById($agent->getServiceId(), $acronymeOrganisme);
                if ('0' == $acronymeOrganisme) {
                    $organisme = Atexo_Organismes::retrieveOrganismeByAcronyme($agent->getOrganisme());
                }

                if (0 != $agent->getServiceId()) {
                    $service = Atexo_EntityPurchase::retrieveEntityById($agent->getServiceId(), $agent->getOrganisme());
                    if ($service instanceof CommonService) {
                        $nomService = $service->getLibelle();
                        $entiteRattachement = $service->getPathServiceTraduit();
                    }
                } else {
                    $getDenominationOrg = 'getDenominationOrg'.Atexo_Languages::getLanguageAbbreviation($langue);
                    if (0 == strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE'))
                           || !$organisme->$getDenominationOrg()) {
                        $entiteRattachement = $organisme->getSigle().' - '.$organisme->getDenominationOrg();
                    } else {
                        $entiteRattachement = $organisme->getSigle().' - '.$organisme->$getDenominationOrg();
                    }
                }

                $worksheet->setRow($row, '');
                $worksheet->write($row, $colonne++, $agent->getNom(), $styleSimplePolice8);
                $worksheet->write($row, $colonne++, $agent->getPrenom(), $styleSimplePolice8);
                $worksheet->writeString($row, $colonne++, $agent->getNumTel(), $styleSimplePolice8);
                $worksheet->writeString($row, $colonne++, $agent->getNumFax(), $styleSimplePolice8);
                $worksheet->write($row, $colonne++, $agent->getEmail(), $styleSimplePolice8);
                $worksheet->write($row, $colonne++, $agent->getLogin(), $styleSimplePolice8);
                $worksheet->write($row, $colonne++, $entiteRattachement, $styleSimplePolice8);

                if (is_array($refsAgent) && count($refsAgent) > 0) {
                    foreach ($agent->getReferentiel() as $oneRef) {
                        $worksheet->write($row, $colonne++, $oneRef, $styleSimplePolice8);
                    }
                }

                ++$row;
            }
        }

        $workbook->close();
        if (!$toFile) {
            $workbook->send($nomFichier);
        }

        exit;
    }

    /*
     * Permet de generer l'excel des organismes
     */
    public function generateExcelOrganismes($listeOrganismes, $toFile = false)
    {
        (new Atexo_Config())->includePhpLibPath();
        require_once 'Spreadsheet/Excel/Writer.php';
        if ($toFile) {
            $tmpFileName = Atexo_Config::getParameter('COMMON_TMP').Prado::localize('TEXT_REGISTRE_HABILITATION').session_id().'_'.time().'.xls';
            $workbook = new Spreadsheet_Excel_Writer($tmpFileName);
        } else {
            $workbook = new Spreadsheet_Excel_Writer();
        }

        $workbook->setCustomColor(11, 255, 255, 150);
        $workbook->setCustomColor(12, 192, 192, 192);
        $workbook->setCustomColor(13, 0, 0, 255);
        $workbook->setCustomColor(14, 153, 204, 255);
        //Pour passer outre la limite des 255 caracteres
        $workbook->setVersion(8);

        //Style des entetes
        $stylePolice10GrasBleu = &$workbook->addFormat();
        $stylePolice10GrasBleu->setColor(13);
        $stylePolice10GrasBleu->setSize(10);
        $stylePolice10GrasBleu->setAlign('left');
        $stylePolice10GrasBleu->setBold(1);

        $styleSimplePolice8 = &$workbook->addFormat();
        $styleSimplePolice8->setColor('black');
        $styleSimplePolice8->setSize(8);
        $styleSimplePolice8->setAlign('left');

        $styleSimplePolice8FondBleu = &$workbook->addFormat();
        $styleSimplePolice8FondBleu->setColor('black');
        $styleSimplePolice8FondBleu->setSize(10);
        $styleSimplePolice8FondBleu->setAlign('left');
        $styleSimplePolice8FondBleu->setBold(1);
        $styleSimplePolice8FondBleu->setFgColor(14);

        $worksheet = &$workbook->addWorkSheet(Atexo_Util::utf8ToIso(Prado::localize('ORGANISME')));
        if ('iso-8859-1' != Atexo_Config::getParameter('HTTP_ENCODING')) {
            $worksheet->setInputEncoding(Atexo_Config::getParameter('HTTP_ENCODING'));
        }

        $worksheet->hideGridlines();
        $nomFichier = Prado::localize('TEXT_ORGANISMES_XLS');
        $worksheet->setHeader('&"Arial"&8'.$nomFichier, '0.19');
        $worksheet->setFooter(self::getFooterImprimeLe(), '0.19');

        $worksheet->setMarginLeft('0.19');
        $worksheet->setMarginRight('0.19');
        $worksheet->setMarginTop('0.39');
        $worksheet->setMarginBottom('0.39');

        $worksheet->setColumn(0, 0, '25');
        $worksheet->setColumn(1, 1, '50');
        $worksheet->setColumn(2, 2, '30');
        $worksheet->setColumn(3, 3, '30');
        $worksheet->setColumn(4, 4, '50');

        $worksheet->write(0, 0, Prado::localize('DEFINE_LISTE_ORGANISMES'), $stylePolice10GrasBleu);

        if ($listeOrganismes && is_array($listeOrganismes)) {
            $colonne = 1;
            $row = 5;
            //Initialisation du fichier excel
            $worksheet->write(5, $colonne++, Prado::localize('DENOMINATION'), $styleSimplePolice8FondBleu);
            $worksheet->write(5, $colonne++, Prado::localize('ACRONYME'), $styleSimplePolice8FondBleu);
            $worksheet->write(5, $colonne++, Prado::localize('DEFINE_TEXT_SIGLE_EA_PMI'), $styleSimplePolice8FondBleu);
            $worksheet->write(5, $colonne++, Prado::localize('TEXT_SERVICES_ACCESSIBLES'), $styleSimplePolice8FondBleu);

            $row = 6;
            foreach ($listeOrganismes as $organisme) {
                if ($organisme instanceof CommonOrganisme) {
                    $colonne = 1;
                    $worksheet->setRow($row, '');
                    $worksheet->write($row, $colonne++, $organisme->getDenominationOrgTraduit(), $styleSimplePolice8);
                    $worksheet->write($row, $colonne++, $organisme->getAcronyme(), $styleSimplePolice8);
                    $worksheet->writeString($row, $colonne++, $organisme->getSigle(), $styleSimplePolice8);
                    $worksheet->writeString($row, $colonne++, $organisme->getServiceMetierActive(), $styleSimplePolice8);
                    ++$row;
                }
            }
        }

        $workbook->close();
        if (!$toFile) {
            $workbook->send($nomFichier);
        }

        exit;
    }

    public function generateExcelHistoriquesInscrits($idtrace, $consultationId, $organisme, $toFile = false, $oderBy = '', $sensTri = '')
    {
        $historques = (new Atexo_InscritOperationsTracker())->retrieveDetailsTraceByRefConsultation($idtrace, false, $oderBy, $sensTri);
        $traceObjet = (new Atexo_InscritOperationsTracker())->retrieveTraceById($idtrace);
        $nomEntrepise = Atexo_Entreprise::getNomEntreprise($traceObjet->getIdEntreprise());
        $consultation = (new Atexo_Consultation())->retrieveConsultation($consultationId, $organisme);
        if ($consultation instanceof CommonConsultation) {
            (new Atexo_Config())->includePhpLibPath();
            require_once 'Spreadsheet/Excel/Writer.php';
            //Initialisation du fichier excel
            $tmpFileName = Atexo_Config::getParameter('COMMON_TMP').Prado::localize('HISTORIQUES_INSCRITS_EXCEL');
            if ($toFile) {
                $workbook = new Spreadsheet_Excel_Writer($tmpFileName);
            } else {
                $workbook = new Spreadsheet_Excel_Writer();
            }

            $workbook->setCustomColor(11, 0, 204, 255);
            $workbook->setCustomColor(12, 221, 221, 221);
            //Pour passer outre la limite des 255 caracteres
            $workbook->setVersion(8);
            //Style des entetes
            $formatStantardGras = &$workbook->addFormat();
            $formatStantardGras->setColor('black');
            $formatStantardGras->setPattern(1);
            $formatStantardGras->setFgColor('white');
            $formatStantardGras->setSize(8);
            $formatStantardGras->setAlign('left');
            //$formatStantardGras->setOutLine();
            $formatStantardGras->setBold(1);

            $formatStantard = &$workbook->addFormat();
            $formatStantard->setColor('black');
            $formatStantard->setPattern(1);
            $formatStantard->setFgColor('white');
            $formatStantard->setSize(8);
            $formatStantard->setAlign('left');
            $formatStantard->setAlign('top');
            $formatStantard->setBold(0);

            $formatStantardTableau = &$workbook->addFormat();
            $formatStantardTableau->setColor('black');
            $formatStantardTableau->setPattern(1);
            $formatStantardTableau->setFgColor('white');
            $formatStantardTableau->setSize(8);
            $formatStantardTableau->setAlign('left');
            $formatStantardTableau->setAlign('top');
            $formatStantardTableau->setBold(0);
            $formatStantardTableau->setBorder(1);
            $formatStantardTableau->setBorderColor(12);

            $formatStantardWrap = &$workbook->addFormat();
            $formatStantardWrap->setColor('black');
            $formatStantardWrap->setFgColor('white');
            $formatStantardWrap->setSize(8);
            $formatStantardWrap->setAlign('left');
            $formatStantardWrap->setAlign('top');
            $formatStantardWrap->setBold(0);
            $formatStantardWrap->setTextWrap();

            $formatEnteteTableau = &$workbook->addFormat();
            $formatEnteteTableau->setColor('black');
            $formatEnteteTableau->setPattern(1);
            $formatEnteteTableau->setFgColor(11);
            $formatEnteteTableau->setSize(8);
            $formatEnteteTableau->setAlign('left');
            //$formatEnteteTableau->setOutLine();
            $formatEnteteTableau->setBold(1);
            $formatEnteteTableau->setAlign('center');
            $formatEnteteTableau->setAlign('top');
            $formatEnteteTableau->setTextWrap();

            $corpsTableau = &$workbook->addFormat();
            $corpsTableau->setColor('black');
            $corpsTableau->setPattern(1);
            $corpsTableau->setFgColor('white');
            $corpsTableau->setSize(8);
            $corpsTableau->setAlign('top');
            //$corpsTableau->setOutLine();
            $corpsTableau->setBold(0);
            $corpsTableau->setTextWrap();
            $corpsTableau->setBorder(1);
            $corpsTableau->setBorderColor(12);
            // la feuille
            $nomOnglet = Prado::localize('DEFINE_HISTORIQUES_NAVIGATION');
            $worksheet = &$workbook->addWorkSheet(Atexo_Util::utf8ToIso($nomOnglet));
            $worksheet->setInputEncoding(Atexo_Config::getParameter('HTTP_ENCODING'));

            $worksheet->setColumn(0, 0, '16,14');
            $worksheet->setColumn(1, 1, '10,14');
            $worksheet->setColumn(2, 2, '8,29');
            $worksheet->setColumn(3, 3, '14,57');
            $worksheet->setColumn(4, 4, '9,43');
            $worksheet->setColumn(5, 5, '11');
            $worksheet->setColumn(6, 6, '14,86');
            $worksheet->setColumn(7, 7, '9,43');
            $worksheet->setColumn(8, 8, '10.49');
            $worksheet->setColumn(9, 9, '11');
            $worksheet->setColumn(10, 10, '13');
            //Definit l'orientation de la page en tant que paysage.
            $worksheet->setLandscape();

            // $nomRegistre=Prado::localize('REGISTRE_DES_QUESTIONS');
            $nomfichier = Prado::localize('TITRE_HISTORIQUES_NAVIGATION_INSCRIT').'_'.$nomEntrepise.'_'.date('Y').'_'.date('m').'_'.date('d').'.xls';
            $entete = '&"Arial"&C&8'.$nomfichier.' - &A';
            $worksheet->setColumn(9, 9, '13');
            $worksheet->setColumn(10, 10, '16');

            $worksheet->setHeader($entete);
            $worksheet->setFooter(self::getFooterImprimeLe());
            $worksheet->setMarginLeft(0.15);
            $worksheet->setMarginRight(0.7);
            $row = 0;
            $lastColumn = 10;
            // le nombre du deuxieme colonne pour l'affichage,
            $secondColumn = 2;
            $thirdColumn = $secondColumn + 1;

            $worksheet->write($row, 0, Prado::localize('HISTORIQUE_DE_NAVIGATION'), $formatStantardGras);
            self::ajouterCelluleVide($worksheet, $row++, $secondColumn, $lastColumn, $formatStantard);
            self::ajouterCelluleVide($worksheet, $row++, $secondColumn, $lastColumn, $formatStantard);
            $worksheet->write($row, 0, Prado::localize('IDENTIF_CONS'), $formatStantardGras);
            self::ajouterCelluleVide($worksheet, $row++, $secondColumn, $lastColumn, $formatStantard);

            $worksheet->write($row, 0, Prado::localize('DEFINE_REFERENCE').' :', $formatStantardWrap);
            $worksheet->writeString($row, $secondColumn, $consultation->getReferenceUtilisateur(), $formatStantardWrap);
            self::ajouterCelluleVide($worksheet, $row++, $thirdColumn, $lastColumn, $formatStantard);

            $entitePublique = Atexo_Organismes::retrieveOrganismeByAcronyme($organisme);
            $worksheet->write($row, 0, Prado::localize('DEFINE_TEXT_ENTITE_PUBLIC').' :', $formatStantardWrap);
            if ($entitePublique instanceof CommonOrganisme) {
                $worksheet->write($row, $secondColumn, $entitePublique->getSigle().' - '.$entitePublique->getDenominationOrg(), $formatStantardWrap);
            } else {
                $worksheet->write($row, $secondColumn, '', $formatStantard);
            }
            self::ajouterCelluleVide($worksheet, $row++, $thirdColumn, $lastColumn, $formatStantard);

            $service = Atexo_EntityPurchase::getPathEntityById($consultation->getServiceId(), $organisme);
            $worksheet->write($row, 0, Prado::localize('TEXT_ENTITE_ACHAT').' :', $formatStantardWrap);
            $worksheet->write($row, $secondColumn, $service, $formatStantardWrap);
            self::ajouterCelluleVide($worksheet, $row++, $thirdColumn, $lastColumn, $formatStantard);

            $typeAvis = (new Atexo_Consultation_TypeAvis())->retrieveTypeAnnonce($consultation->getIdTypeAvis());
            if ($typeAvis) {
                $worksheet->write($row, 0, Prado::localize('TYPE_ANNONCE').' :', $formatStantardWrap);
                $worksheet->write($row, $secondColumn, $typeAvis->getIntituleAvis(), $formatStantardWrap);
            }
            self::ajouterCelluleVide($worksheet, $row++, $thirdColumn, $lastColumn, $formatStantard);

            $typeProcedure = (new Atexo_Consultation_ProcedureType())->retrieveTypeProcedure($consultation->getIdTypeProcedureOrg(), false, $organisme);
            $worksheet->write($row, 0, Prado::localize('TEXT_TYPE_PROCEDURE').' :', $formatStantardWrap);
            $worksheet->write($row, $secondColumn, $typeProcedure, $formatStantardWrap);
            self::ajouterCelluleVide($worksheet, $row++, $thirdColumn, $lastColumn, $formatStantard);

            $worksheet->write($row, 0, Prado::localize('DEFINE_INTITULE_CONSULTATION').' :', $formatStantardWrap);
            $worksheet->write($row, $secondColumn, $consultation->getIntitule(), $formatStantardWrap);
            self::ajouterCelluleVide($worksheet, $row++, $thirdColumn, $lastColumn, $formatStantard);

            $worksheet->write($row, 0, Prado::localize('DEFINE_OBJET_CONSULTATION').' :', $formatStantardWrap);
            $worksheet->write($row, $secondColumn, $consultation->getObjet(), $formatStantardWrap);
            self::ajouterCelluleVide($worksheet, $row++, $thirdColumn, $lastColumn, $formatStantard);

            $categorie = Atexo_Consultation_Category::retrieveLibelleCategorie($consultation->getCategorie(), false, $organisme);
            $worksheet->write($row, 0, Prado::localize('DEFINE_CATEGORIE_PRINCIPAL').' :', $formatStantardWrap);
            $worksheet->write($row, $secondColumn, $categorie, $formatStantardWrap);
            self::ajouterCelluleVide($worksheet, $row++, $thirdColumn, $lastColumn, $formatStantard);
            $worksheet->write($row, 0, Prado::localize('DEFINE_DATE_LIMITE_REMISE_PLIS').' :', $formatStantardWrap);
            $worksheet->write($row, $secondColumn, Atexo_Util::iso2frnDateTime($consultation->getDatefin()), $formatStantard);
            self::ajouterCelluleVide($worksheet, $row++, $thirdColumn, $lastColumn, $formatStantard);
            self::ajouterCelluleVide($worksheet, $row++, $secondColumn, $lastColumn, $formatStantard);
            $worksheet->setColumn(0, 13, '10,14');
            $worksheet->setColumn(1, 13, '100,14');
            $worksheet->write($row, 0, Prado::localize('TITRE_HISTORIQUES_CONCERNANT'), $formatStantardGras);
            self::ajouterCelluleVide($worksheet, $row++, $secondColumn, $lastColumn, $formatStantard);
            $worksheet->write($row, 0, Prado::localize('TEXT_RAISON_SOCIAL').' :', $formatStantardWrap);
            $worksheet->write($row, $secondColumn, $traceObjet->getEntreprise()->getNom(), $formatStantardWrap);
            self::ajouterCelluleVide($worksheet, $row++, $thirdColumn, $lastColumn, $formatStantard);
            $worksheet->write($row, 0, Prado::localize('DEFINE_NOM').' :', $formatStantardWrap);
            $worksheet->write($row, $secondColumn, $traceObjet->getNomInscrit(), $formatStantardWrap);
            self::ajouterCelluleVide($worksheet, $row++, $thirdColumn, $lastColumn, $formatStantard);
            $worksheet->write($row, 0, Prado::localize('DEFINE_PRENOM').' :', $formatStantardWrap);
            $worksheet->write($row, $secondColumn, $traceObjet->getPrenomInscrit(), $formatStantardWrap);
            self::ajouterCelluleVide($worksheet, $row++, $thirdColumn, $lastColumn, $formatStantard);
            $worksheet->write($row, 0, Prado::localize('DEFINE_ADRESSE_IP').' :', $formatStantardWrap);
            $worksheet->write($row, $secondColumn, $traceObjet->getAddrIp(), $formatStantard);
            self::ajouterCelluleVide($worksheet, $row++, $thirdColumn, $lastColumn, $formatStantard);

            $worksheet->write($row, 0, Prado::localize('DATE').' :', $formatStantardWrap);
            $worksheet->write($row, $secondColumn, Atexo_Util::iso2frnDate($traceObjet->getDate()), $formatStantard);
            self::ajouterCelluleVide($worksheet, $row++, $thirdColumn, $lastColumn, $formatStantard);
            self::ajouterCelluleVide($worksheet, $row++, $secondColumn, $lastColumn, $formatStantard);

            $colonne = 0;
            //         $row=20;
            $worksheet->setRow($row, '20');
            $worksheet->write($row, $colonne++, Prado::localize('DEFINE_DATE_ACTION'), $formatEnteteTableau);
            $worksheet->write($row, $colonne++, Prado::localize('DEFINE_DESCRIPTION_ACTION'), $formatEnteteTableau);
            $worksheet->write($row, 9, Prado::localize('DATE_DU_POSTE'), $formatEnteteTableau);
            $worksheet->mergeCells($row, 1, $row, 8);
            $worksheet->mergeCells($row, 9, $row, 10);

            if (count($historques) > 0) {
                foreach ($historques as $historque) {
                    ++$row;
                    $colonne = 0;
                    $worksheet->write($row, $colonne++, $historque['dateAction'], $corpsTableau);
                    $worksheet->write($row, $colonne++, Atexo_Util::replaceSpecialCharacters($historque['description']), $corpsTableau);
                    $worksheet->write($row, 9, empty($historque['dateActionPosteClient']) ? '-' : $historque['dateActionPosteClient'], $corpsTableau);
                    $worksheet->write($row, 2, '', $formatStantardTableau);
                    $worksheet->write($row, 3, '', $formatStantardTableau);
                    $worksheet->write($row, 4, '', $formatStantardTableau);
                    $worksheet->write($row, 5, '', $formatStantardTableau);
                    $worksheet->write($row, 6, '', $formatStantardTableau);
                    $worksheet->write($row, 7, '', $formatStantardTableau);
                    $worksheet->write($row, 8, '', $formatStantardTableau);
                    $worksheet->write($row, 9, '', $formatStantardTableau);
                    $worksheet->write($row, 10, '', $formatStantardTableau);

                    $worksheet->mergeCells($row, 1, $row, 8);
                    $worksheet->mergeCells($row, 9, $row, 10);
                }
            }
            $workbook->close();
            if (!$toFile) {
                $workbook->send($nomfichier);
            } else {
                $pathFichier = Atexo_Config::getParameter('COMMON_TMP').$nomfichier;
                system('mv '.$tmpFileName." '".$pathFichier."' > ".Atexo_Config::getParameter('COMMON_TMP').'zip_error 2>&1');

                return $pathFichier;
            }
        }
        exit;
    }

    /**
     * genere un excel de la liste des agents d'une entite publique avec leur services metier associes.
     */
    public function generateExcelAgentsSocle($acronymeOrganisme, $serviceId, $toFile = false, $allAgents = true)
    {
        if (!$allAgents) { //la liste des agents d'un service donne
            $agents = Atexo_Agent::retriveServiceAgents($acronymeOrganisme, $serviceId);
        } else { //la list de tout les agents de l'organisme en cours
            $agents = Atexo_Agent::retriveAgentsByOrganisme($acronymeOrganisme, true);
        }

        $organisme = Atexo_Organismes::retrieveOrganismeByAcronyme($acronymeOrganisme);
        $nomOrganisme = '';
        if ($organisme instanceof CommonOrganisme) {
            $nomOrganisme = $organisme->getDenominationOrg();
        }

        $nomService = '';
        if ('0' === $serviceId || 0 === $serviceId) {
            $nomService = $nomOrganisme;
            if ($allAgents) {
                $listeService = (new Atexo_EntityPurchase())->getAllServices($acronymeOrganisme);
            }
        } else {
            $service = Atexo_EntityPurchase::retrieveEntityById($serviceId, $acronymeOrganisme);
            if ($service instanceof CommonService) {
                $nomService = $service->getLibelle();
                $listeService[$service->getId()] = $service;
            }
        }
        (new Atexo_Config())->includePhpLibPath();
        require_once 'Spreadsheet/Excel/Writer.php';
        if ($toFile) {
            $tmpFileName = Atexo_Config::getParameter('COMMON_TMP').Prado::localize('TEXT_REGISTRE_HABILITATION').session_id().'_'.time().'.xls';
            $workbook = new Spreadsheet_Excel_Writer($tmpFileName);
        } else {
            $workbook = new Spreadsheet_Excel_Writer();
        }

        $workbook->setCustomColor(11, 255, 255, 150);
        $workbook->setCustomColor(12, 192, 192, 192);
        $workbook->setCustomColor(13, 0, 0, 255);
        $workbook->setCustomColor(14, 153, 204, 255);
        //Pour passer outre la limite des 255 caracteres
        $workbook->setVersion(8);

        //Style des entetes
        $stylePolice10GrasBleu = &$workbook->addFormat();
        $stylePolice10GrasBleu->setColor(13);
        $stylePolice10GrasBleu->setSize(10);
        $stylePolice10GrasBleu->setAlign('left');
        $stylePolice10GrasBleu->setBold(1);

        $stylePolice10BleuSimple = &$workbook->addFormat();
        $stylePolice10BleuSimple->setColor(13);
        $stylePolice10BleuSimple->setSize(10);
        $stylePolice10BleuSimple->setAlign('left');

        $styleSimplePolice10 = &$workbook->addFormat();
        $styleSimplePolice10->setColor('black');
        $styleSimplePolice10->setSize(10);
        $styleSimplePolice10->setAlign('left');

        $stylePolice8GrasBleu = &$workbook->addFormat();
        $stylePolice8GrasBleu->setColor(13);
        $stylePolice8GrasBleu->setSize(8);
        $stylePolice8GrasBleu->setAlign('left');
        $stylePolice8GrasBleu->setBold(1);

        $styleSimplePolice8 = &$workbook->addFormat();
        $styleSimplePolice8->setColor('black');
        $styleSimplePolice8->setSize(8);
        $styleSimplePolice8->setAlign('left');

        $styleSimplePolice8FondJaune = &$workbook->addFormat();
        $styleSimplePolice8FondJaune->setColor('black');
        $styleSimplePolice8FondJaune->setSize(8);
        $styleSimplePolice8FondJaune->setAlign('left');
        $styleSimplePolice8FondJaune->setFgColor(11);

        $styleSimplePolice8FondGris = &$workbook->addFormat();
        $styleSimplePolice8FondGris->setColor('black');
        $styleSimplePolice8FondGris->setSize(8);
        $styleSimplePolice8FondGris->setAlign('left');
        $styleSimplePolice8FondGris->setFgColor(12);

        $styleSimplePolice8FondBleu = &$workbook->addFormat();
        $styleSimplePolice8FondBleu->setColor('black');
        $styleSimplePolice8FondBleu->setSize(8);
        $styleSimplePolice8FondGris->setAlign('left');
        $styleSimplePolice8FondBleu->setFgColor(14);

        $worksheet = &$workbook->addWorkSheet(Atexo_Util::utf8ToIso(Prado::localize('TEXT_AGENTS')));
        if ('iso-8859-1' != Atexo_Config::getParameter('HTTP_ENCODING')) {
            $worksheet->setInputEncoding(Atexo_Config::getParameter('HTTP_ENCODING'));
        }

        $worksheet->hideGridlines();
        $nomFichier = Prado::localize('TEXT_REGISTRE_AGENTS_XLS');
        $worksheet->setHeader('&"Arial"&8'.$nomFichier.' - '.Prado::localize('DEFINE_HABILITATION'), '0.19');
        $worksheet->setFooter(self::getFooterImprimeLe(), '0.19');

        $worksheet->setMarginLeft('0.19');
        $worksheet->setMarginRight('0.19');
        $worksheet->setMarginTop('0.39');
        $worksheet->setMarginBottom('0.39');

        $worksheet->setColumn(0, 0, '3.14');
        $worksheet->setColumn(1, 1, '42.86');

        for ($i = 2; $i < 62; ++$i) {
            if (7 == $i) { // la colonne "Entite de rattachement (SIGLE / SIGLE / SIGLE - Libelle)"
                $worksheet->setColumn($i, $i, '69.86');
            } else {
                $worksheet->setColumn($i, $i, '42.86');
            }
        }

        $worksheet->write(0, 0, Prado::localize('TEXT_REGISTRE_AGENTS'), $stylePolice10GrasBleu);

        $worksheet->write(1, 0, Prado::localize('DEFINE_TEXT_ENTITE_PUBLIC').' :', $stylePolice10BleuSimple);
        $worksheet->write(1, 2, $nomOrganisme, $styleSimplePolice10);

        if ($agents && is_array($agents)) {
            $colonne = 1;
            $row = 5;
            //Initialisation du fichier excel
            $worksheet->write($row, $colonne++, Prado::localize('DEFINE_NOM_MAJ'), $stylePolice8GrasBleu);
            $worksheet->write($row, $colonne++, Prado::localize('DEFINE_PRENOM'), $stylePolice8GrasBleu);
            $worksheet->write($row, $colonne++, Prado::localize('TEXT_TEL'), $stylePolice8GrasBleu);
            $worksheet->write($row, $colonne++, Prado::localize('TEXT_FAX'), $stylePolice8GrasBleu);
            $worksheet->write($row, $colonne++, Prado::localize('ADRESSE_ELECTRONIQUE'), $stylePolice8GrasBleu);
            $worksheet->write($row, $colonne++, Prado::localize('TEXT_IDENTIFIANT_SANS_POINTS'), $stylePolice8GrasBleu);
            $worksheet->write($row, $colonne++, Prado::localize('TEXT_ENTITE_RATTACHEMENT_SIGLE_SIGLE_LIBELLE'), $stylePolice8GrasBleu);
            $worksheet->write($row, $colonne++, Prado::localize('LISTE_SERVICE_DISPONIBLE'), $stylePolice8GrasBleu);
            $worksheet->write($row, $colonne++, Prado::localize('DATE_DERNIERE_CONNEXION'), $stylePolice8GrasBleu);
            $worksheet->write($row, $colonne++, Prado::localize('TEXT_ETAT_COMPTE'), $stylePolice8GrasBleu);
            ++$row;
            foreach ($agents as $agent) {
                $colonne = 1;
                $entiteRattachement = '';
                //$entiteRattachement=Atexo_EntityPurchase::getPathEntityById($agent->getServiceId(), $acronymeOrganisme);
                if (0 != $agent->getServiceId()) {
                    $entity = $listeService[$agent->getServiceId()];
                    if ($entity) {
                        $entiteRattachement = $entity->getPathServiceTraduit();
                    }
                } else {
                    $getDenominationOrg = 'getDenominationOrg'.Atexo_Languages::getLanguageAbbreviation($langue);
                    if (0 == strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE'))
                           || !$organisme->$getDenominationOrg()) {
                        $entiteRattachement = $organisme->getSigle().' - '.$organisme->getDenominationOrg();
                    } else {
                        $entiteRattachement = $organisme->getSigle().' - '.$organisme->$getDenominationOrg();
                    }
                }

                $worksheet->setRow($row, '');

                $worksheet->write($row, $colonne++, $agent->getNom(), $styleSimplePolice8);
                $worksheet->write($row, $colonne++, $agent->getPrenom(), $styleSimplePolice8);
                $worksheet->writeString($row, $colonne++, $agent->getNumTel(), $styleSimplePolice8);
                $worksheet->writeString($row, $colonne++, $agent->getNumFax(), $styleSimplePolice8);
                $worksheet->write($row, $colonne++, $agent->getEmail(), $styleSimplePolice8);
                $worksheet->write($row, $colonne++, $agent->getLogin(), $styleSimplePolice8);
                $worksheet->write($row, $colonne++, $entiteRattachement, $styleSimplePolice8);

                $services = Atexo_Socle_AgentServicesMetiers::retreiveListeServiceAgent($agent->getId());
                //print_r($services);echo"<br/>";
                $listeService = '';
                if (is_array($services) && count($services)) {
                    foreach ($services as $oneService) {
                        $listeService .= $oneService->getsigle().'; ';
                    }
                }
                $worksheet->write($row, $colonne++, $listeService, $styleSimplePolice8);
                $worksheet->write($row, $colonne++, $agent->getDateConnexion('d/m/Y H:i:s'), $styleSimplePolice8);
                $etatCompte = $agent->getActif() ? Prado::localize('TEXT_ACTIF') : Prado::localize('DEFINE_DESACTIVE');
                $worksheet->write($row, $colonne++, $etatCompte, $styleSimplePolice8);
                $listeService = '';
                ++$row;
            }
        }

        $workbook->close();
        if (!$toFile) {
            $workbook->send($nomFichier);
        }

        exit;
    }

    /*
    * Permet d'ajouter des cellules vide       *
    */
    public function ajouterCelluleVide(&$worksheet, $numCellule, $debut, $fin, $format)
    {
        $worksheet->write($numCellule, 1, '', $format);
        $worksheet->mergeCells($numCellule, 0, $numCellule, 1);

        for ($i = $debut; $i <= $fin; ++$i) {
            $worksheet->write($numCellule, $i, '', $format);
        }
        $worksheet->mergeCells($numCellule, $debut - 1, $numCellule, $fin);
    }

    /**
     * Permet de générer l'excel des statistiques metiers pour les Marches Publics Simplifies (MPS).
     *
     * @param array $dataStat
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function genererExcelStatistiquesMps($dataStat)
    {
        try {
            /* Debut recuperation donnees excel*/
            // Bloc recapitulatif des criteres de recherche
            $objetRecherche = !empty($dataStat['recapRecherche']['objetRecherche']) ? Atexo_Util::toUtf8($dataStat['recapRecherche']['objetRecherche']) : '';
            $nomEntite = !empty($dataStat['recapRecherche']['entiteName']) ? Atexo_Util::toUtf8($dataStat['recapRecherche']['entiteName']) : '';
            // Nombre de consultations passées en mode MPS
            $nbrConsMpsTravaux = !empty($dataStat['nbrConsultationsMps']['travaux']) ? $dataStat['nbrConsultationsMps']['travaux'] : '0';
            $nbrConsMpsFournitures = !empty($dataStat['nbrConsultationsMps']['fournitures']) ? $dataStat['nbrConsultationsMps']['fournitures'] : '0';
            $nbrConsMpsServices = !empty($dataStat['nbrConsultationsMps']['services']) ? $dataStat['nbrConsultationsMps']['services'] : '0';
            // Nomdre de consultations MPS ayant reçu au moins une réponse en mode MPS
            $nbrConsMpsAvecRepMpsTravaux = !empty($dataStat['nbrConsMpsAyantReponseMps']['travaux']) ? $dataStat['nbrConsMpsAyantReponseMps']['travaux'] : '0';
            $nbrConsMpsAvecRepMpsFournitures = !empty($dataStat['nbrConsMpsAyantReponseMps']['fournitures']) ? $dataStat['nbrConsMpsAyantReponseMps']['fournitures'] : '0';
            $nbrConsMpsAvecRepMpsServices = !empty($dataStat['nbrConsMpsAyantReponseMps']['services']) ? $dataStat['nbrConsMpsAyantReponseMps']['services'] : '0';
            // Nombre de réponses MPS reçues pour les consultations MPS
            $nbrReponsesMpsTravaux = !empty($dataStat['nbrReponsesMps']['travaux']) ? $dataStat['nbrReponsesMps']['travaux'] : '0';
            $nbrReponsesMpsFournitures = !empty($dataStat['nbrReponsesMps']['fournitures']) ? $dataStat['nbrReponsesMps']['fournitures'] : '0';
            $nbrReponsesMpsServices = !empty($dataStat['nbrReponsesMps']['services']) ? $dataStat['nbrReponsesMps']['services'] : '0';
            /* Fin recuperation donnees excel*/

            //Instanciation de Atexo_Excel
            $atexoExcel = new Atexo_Excel();
            $atexoExcel->setDirectoryFile(Atexo_Config::getParameter('EXCEL_DIRECTORY'));
            $atexoExcel->setNameFile('Exports_Statistiques_MPS.xls');

            //Chargement a partir d'un modele
            $atexoExcel->chargerDepuisModele(Atexo_Config::getParameter('CHEMIN_MODELE_EXPORT_STATISTIQUES_MPS'));

            // Set properties
            $title = Prado::localize('SUIVI_MARCHES_PUBLICS_SIMPLIFIES').date('d/m/Y H:i:s');
            $atexoExcel->setCreator('MPE');
            $atexoExcel->setLastModifiedBy('MPE');
            $atexoExcel->setTitle($title);
            $atexoExcel->setSubject($title);
            $atexoExcel->setDescription($title);
            $atexoExcel->setKeywords($title);

            //Valeurs du bloc recapitulatif des criteres de recherche
            $atexoExcel->setCellValue(0, 'A3', $objetRecherche);
            $atexoExcel->setCellValue(0, 'A4', html_entity_decode($nomEntite, ENT_QUOTES));
            /* Nombre de consultations passées en mode MPS */
            $atexoExcel->setCellValue(0, 'C8', $nbrConsMpsTravaux);
            $atexoExcel->setCellValue(0, 'C9', $nbrConsMpsFournitures);
            $atexoExcel->setCellValue(0, 'C10', $nbrConsMpsServices);
            /* Nomdre de consultations MPS ayant reçu au moins une réponse en mode MPS */
            $atexoExcel->setCellValue(0, 'C12', $nbrConsMpsAvecRepMpsTravaux);
            $atexoExcel->setCellValue(0, 'C13', $nbrConsMpsAvecRepMpsFournitures);
            $atexoExcel->setCellValue(0, 'C14', $nbrConsMpsAvecRepMpsServices);
            /* Nombre de réponses MPS reçues pour les consultations MPS */
            $atexoExcel->setCellValue(0, 'C16', $nbrReponsesMpsTravaux);
            $atexoExcel->setCellValue(0, 'C17', $nbrReponsesMpsFournitures);
            $atexoExcel->setCellValue(0, 'C18', $nbrReponsesMpsServices);

            //Sauvegarder en fichier
            $atexoExcel->writeExcelDocument();

            //Envoyer les donnees vers le navigateur pour telechargement
            $atexoExcel->telechargerExcelDocument();
        } catch (\Exception $ex) {
            $logger = Atexo_LoggerManager::getLogger('app');
            $logger->error('error ATX_XLS: Impossible de generer le fichier excel '.$ex->getMessage());
        }
    }

    /**
     * Permet de generer l'excel des statistiques des contrats pour la recherche avancee.
     *
     * @param string $avecDetailsEntites
     * @param bool   $addColsSupplementaires
     * @param bool   $showPfShortName
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since 2017-place
     *
     * @copyright Atexo 2017
     */
    public static function genererExcelStatistiquesContratsRechercheAvance(array $dataStats, Atexo_Statistiques_CriteriaVo $objetCritereRecherche, $avecDetailsEntites, $addColsSupplementaires = false, $showPfShortName = false)
    {
        try {
            //Instanciation de Atexo_Excel
            $atexoExcel = new Atexo_Excel();
            $atexoExcel->setDirectoryFile(Atexo_Config::getParameter('EXCEL_DIRECTORY'));
            $atexoExcel->setNameFile(str_replace('{date}', date('Ymd'), Atexo_Config::getParameter('NOM_FICHIER_EXPORT_STATISTIQUES_CONTRATS')));

            //Chargement a partir d'un modele
            $template = Atexo_Config::getParameter('CHEMIN_MODELE_EXPORT_STATISTIQUES_CONTRATS');
            $atexoExcel->chargerDepuisModele($template);

            //Definition des styles
            $couleurEnteteCellule = '77B5FE';
            $styleCelluleGras = ['bold' => true];

            // Set properties
            $title = Prado::localize('SUIVI_MARCHES_PUBLICS_SIMPLIFIES').date('d/m/Y H:i:s');
            $atexoExcel->setCreator('MPE');
            $atexoExcel->setLastModifiedBy('MPE');
            $atexoExcel->setTitle($title);
            $atexoExcel->setSubject($title);
            $atexoExcel->setDescription($title);
            $atexoExcel->setKeywords($title);

            //Remplissage des donnees de l'excel
            $atexoExcel->setCellValue(0, 'A3', Prado::localize('TEXT_OBJET_RECHERCHE').' : '.$objetCritereRecherche->getObjetRecherche());
            $atexoExcel->setCellValue(0, 'A4', Prado::localize('DEFINE_INTERVALLE_RECHERCHE').' : '.$objetCritereRecherche->getIntervalleRechercheBlocRecap());
            $atexoExcel->setCellValue(0, 'A5', (true === $avecDetailsEntites) ? Prado::localize('TEXT_DETAIL_ENTITES') : ((true === $showPfShortName) ? Atexo_Config::getParameter('PF_SHORT_NAME') : ''));

            if (!empty($dataStats)) {
                $ligneDynamiqueStart = 7;
                $colonneDynamiqueStart = (true === $addColsSupplementaires) ? 2 : 0;
                $colonneEntite = 0;
                $colonneAcronyme = 1;
                $colonneCp = 2;

                $ligne = $ligneDynamiqueStart;
                $grandTotalParLigne = 0;
                $listeDonneesTotalParColonne = [];
                foreach ($dataStats as $nomEntite => $donneesParEntite) {
                    $colonne = $colonneEntite;
                    $totalParLigne = 0;

                    //Debut re-organisation du tableau : ajoute un tableau vide au debut correspondant au cas "$colonne = 0" et aussi les index du tableau pour qu'ils soient des index successifs
                    $array = ['nombreContrats' => 0, 'libelleTypeContrat' => ''];
                    if (true == $addColsSupplementaires) {
                        $keys = array_keys($donneesParEntite);
                        $firstKey = $keys[0];
                        $array['codePostal'] = $donneesParEntite[$firstKey]['codePostal'];
                        $array['organisme'] = $donneesParEntite[$firstKey]['organisme'];
                    }
                    for ($i = 0; $i <= $colonneDynamiqueStart; ++$i) {
                        $donneesParEntite = array_values(array_merge([$array], $donneesParEntite));
                    }
                    //Fin re-organisation du tableau : ajoute un tableau vide au debut correspondant au cas "$colonne = 0" et aussi les index du tableau pour qu'ils soient des index successifs

                    foreach ($donneesParEntite as $donneesTypeContrat) {
                        $nombreContratsParCellule = 0;
                        if ($ligne == $ligneDynamiqueStart) {
                            if (true === $addColsSupplementaires) {
                                if ($colonne == $colonneAcronyme) {
                                    $atexoExcel->setCellValueByColumnAndRow($colonne, $ligne, Prado::localize('ACRONYME'));
                                }
                                if ($colonne == $colonneCp) {
                                    $atexoExcel->setCellValueByColumnAndRow($colonne, $ligne, Prado::localize('CODE_POSTAL'));
                                }
                            }
                            if ($colonne > $colonneDynamiqueStart) {
                                $atexoExcel->setCellValueByColumnAndRow($colonne, $ligne, Prado::localize($donneesTypeContrat['libelleTypeContrat']));
                            }
                            if ($colonneEntite != $colonne) {
                                $atexoExcel->setBackGroundColor([$colonne, $ligne], $couleurEnteteCellule);
                                $atexoExcel->setFormatCellule([$colonne, $ligne], $styleCelluleGras);
                            }
                        } else {
                            $atexoExcel->setCellValueByColumnAndRow($colonneEntite, $ligne, $nomEntite);
                            $atexoExcel->setFormatCellule([$colonneEntite, $ligne], $styleCelluleGras);
                            if (true === $addColsSupplementaires) {
                                if ($colonne == $colonneAcronyme) {
                                    $atexoExcel->setCellValueByColumnAndRow($colonne, $ligne, $donneesTypeContrat['organisme']);
                                }
                                if ($colonne == $colonneCp) {
                                    $atexoExcel->setCellValueByColumnAndRow($colonne, $ligne, $donneesTypeContrat['codePostal']);
                                }
                            }
                            if ($colonne > $colonneDynamiqueStart) {
                                $nombreContratsParCellule = $donneesTypeContrat['nombreContrats'];
                                $atexoExcel->setCellValueByColumnAndRow($colonne, $ligne, $nombreContratsParCellule);
                            }
                        }

                        //Valeurs Max
                        $totalParLigne += $nombreContratsParCellule;
                        if (empty($listeDonneesTotalParColonne[$colonne][$ligne])) {
                            $listeDonneesTotalParColonne[$colonne][$ligne] = 0;
                        }
                        $listeDonneesTotalParColonne[$colonne][$ligne] += $nombreContratsParCellule;

                        ++$colonne;
                    }

                    if (empty($colonneMaxIndex)) {
                        $colonneMaxIndex = $atexoExcel->getColumnIndexFromString($atexoExcel->getHighestColumn());
                    }

                    //Debut total par ligne
                    $atexoExcel->setCellValueByColumnAndRow($colonneMaxIndex, $ligneDynamiqueStart, Prado::localize('DEFINE_TOTAL'));
                    $atexoExcel->setCellValueByColumnAndRow($colonneMaxIndex, $ligne, $totalParLigne);
                    $grandTotalParLigne += $totalParLigne;
                    $atexoExcel->setBackGroundColor([$colonneMaxIndex, $ligneDynamiqueStart], $couleurEnteteCellule);
                    $atexoExcel->setFormatCellule([$colonneMaxIndex, $ligneDynamiqueStart], $styleCelluleGras);
                    $atexoExcel->setFormatCellule([$colonneMaxIndex, $ligne], $styleCelluleGras);
                    //Fin total par ligne

                    ++$ligne;
                }
                $ligneMax = $atexoExcel->getHighestRow();

                //Debut total par colonne
                if (true === $avecDetailsEntites) {
                    foreach ($listeDonneesTotalParColonne as $colonne => $listeValeursParLigne) {
                        if ($colonne == $colonneEntite) {
                            $atexoExcel->setCellValueByColumnAndRow($colonneEntite, $ligneMax + 1, Prado::localize('DEFINE_TOTAL'));
                        } elseif ($colonne > $colonneDynamiqueStart) {
                            $atexoExcel->setCellValueByColumnAndRow($colonne, $ligneMax + 1, array_sum($listeValeursParLigne));
                        }
                        $atexoExcel->setFormatCellule([$colonne, $ligneMax + 1], $styleCelluleGras);
                    }

                    //Grand total
                    $atexoExcel->setCellValueByColumnAndRow($colonneMaxIndex, $ligneMax + 1, $grandTotalParLigne);
                    $atexoExcel->setFormatCellule([$colonneMaxIndex, $ligneMax + 1], $styleCelluleGras);
                }
                //Fin total par colonne
            }

            //Sauvegarder en fichier
            $atexoExcel->writeExcelDocument();

            //Envoyer les donnees vers le navigateur pour telechargement
            $atexoExcel->telechargerExcelDocument();
        } catch (Exception $exception) {
            throw $exception;
        }
    }

    /**
     * Permet de générer et télécharger l'Excel de la liste des contrats.
     *
     * @param array $dataStat
     *
     * @return void
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1.0
     *
     * @since 2017-place
     *
     * @copyright Atexo 2017
     */
    public static function genererExcelContrats($contrats)
    {
        try {
            //Instanciation de Atexo_Excel
            $atexoExcel = new Atexo_Excel();
            $atexoExcel->setDirectoryFile(Atexo_Config::getParameter('EXCEL_DIRECTORY'));
            $atexoExcel->setNameFile('Export_contrats_'.date('Y_m_d_H_i_s').'.xls');

            //Chargement a partir d'un modele
            $atexoExcel->chargerDepuisModele(Atexo_Config::getParameter('CHEMIN_MODELE_EXPORT_CONTRATS'));

            // Set properties
            $title = Prado::localize('EXPORT_CONTRATS');
            $atexoExcel->setCreator('MPE');
            $atexoExcel->setLastModifiedBy('MPE');
            $atexoExcel->setTitle($title);
            $atexoExcel->setSubject($title);
            $atexoExcel->setDescription($title);
            $atexoExcel->setKeywords($title);
            $atexoExcel->setBackGroundColor('A1:AI1', 'EFF0F1');
            //entete TODO
            $atexoExcel->setCellValue(0, 'A1', Prado::localize('NUMERO_CONTRAT'));
            $atexoExcel->setCellValue(0, 'B1', Prado::localize('CONTRAT_NUMERO_COURT'));
            $atexoExcel->setCellValue(0, 'C1', Prado::localize('CONTRAT_NUMERO_LONG'));
            $atexoExcel->setCellValue(0, 'D1', Prado::localize('TEXT_INTITULE_CONTRAT'));
            $atexoExcel->setCellValue(0, 'E1', Prado::localize('OBJET_CONTRAT'));
            $atexoExcel->setCellValue(0, 'F1', Prado::localize('DEFINE_TYPE_CONTRAT'));
            $atexoExcel->setCellValue(0, 'G1', Prado::localize('STATUT_CONTRAT_EXPORT'));
            $atexoExcel->setCellValue(0, 'H1', Prado::localize('ACCORD_CADRE_OU_SAD_LIE'));
            $atexoExcel->setCellValue(0, 'I1', Prado::localize('TEXT_NUMERO_CONTRAT_CHAPEAU'));
            $atexoExcel->setCellValue(0, 'J1', Prado::localize('TEXT_PROCEDURE_PASSATION'));
            $atexoExcel->setCellValue(0, 'K1', Prado::localize('REFERENCE_DE_LA_CONSULTATION'));
            $atexoExcel->setCellValue(0, 'L1', Prado::localize('NOM_ENTREPRISE_TITULAIRE'));
            $atexoExcel->setCellValue(0, 'M1', Prado::localize('SIRET_ENTREPRISE_TITULAIRE'));
            $atexoExcel->setCellValue(0, 'N1', Prado::localize('DEFINE_PME'));
            $atexoExcel->setCellValue(0, 'O1', Prado::localize('MONTANT_CONTRAT'));
            $atexoExcel->setCellValue(0, 'P1', Prado::localize('MONTANT_MAXIMUM_ESTIME_CONTRAT'));
            $atexoExcel->setCellValue(0, 'Q1', Prado::localize('TEXT_FORME_PRIX'));
            $atexoExcel->setCellValue(0, 'R1', Prado::localize('CATEGORIE_CONTRAT'));
            $atexoExcel->setCellValue(0, 'S1', Prado::localize('CODE_CPV_PRINCIPAL'));
            $atexoExcel->setCellValue(0, 'T1', Prado::localize('CLAUSES_SOCIALES'));
            $atexoExcel->setCellValue(0, 'U1', Prado::localize('CLAUSES_ENVIRONNEMENTALES'));
            $atexoExcel->setCellValue(0, 'V1', Prado::localize('TEXT_LIEU_PRINCIPAL_EXECUTION'));
            $atexoExcel->setCellValue(0, 'W1', Prado::localize('DATE_ATTRIBUTION_CONTRAT'));
            $atexoExcel->setCellValue(0, 'X1', Prado::localize('DATE_PREVISIONNELE_NOTIFICATION_CONTRAT'));
            $atexoExcel->setCellValue(0, 'Y1', Prado::localize('DATE_PREVISIONNELE_FIN_CONTRAT'));
            $atexoExcel->setCellValue(0, 'Z1', Prado::localize('DATE_PREVISIONNELE_MAX_FIN_CONTRAT'));
            $atexoExcel->setCellValue(0, 'AA1', Prado::localize('DATE_NOTIFICATION_CONTRAT'));
            $atexoExcel->setCellValue(0, 'AB1', Prado::localize('DATE_FIN_CONTRAT'));
            $atexoExcel->setCellValue(0, 'AC1', Prado::localize('DATE_MAX_FIN_CONTRAT'));
            $atexoExcel->setCellValue(0, 'AD1', Prado::localize('DEFINE_DUREE_MARCHE'));
            $atexoExcel->setCellValue(0, 'AE1', Prado::localize('DEFINE_TEXT_ENTITE_PUBLIC'));
            $atexoExcel->setCellValue(0, 'AF1', Prado::localize('TEXT_ENTITE_ACHAT'));
            $atexoExcel->setCellValue(0, 'AG1', Prado::localize('NOM_AGENT_GESTIONNAIRE_CONTRAT'));
            $atexoExcel->setCellValue(0, 'AH1', Prado::localize('PRENOM_AGENT_GESTIONNAIRE_CONTRAT'));
            $atexoExcel->setCellValue(0, 'AI1', Prado::localize('DONNEES_ESSENTIELLES_PUBLICATION_CONTRAT'));
            $ligne = 2;
            foreach ($contrats as $contrat) {
                if ($contrat instanceof CommonTContratTitulaire) {
                    $atexoExcel->setCellValue(0, 'A'.$ligne, $contrat->getReferenceLibre());
                    $atexoExcel->setCellValue(0, 'B'.$ligne, $contrat->getNumeroContrat());
                    $atexoExcel->setCellValue(0, 'C'.$ligne, $contrat->getNumLongOeap());
                    $atexoExcel->setCellValue(0, 'D'.$ligne, $contrat->getIntitule());
                    $atexoExcel->setCellValue(0, 'E'.$ligne, $contrat->getObjetContrat());
                    $atexoExcel->setCellValue(0, 'F'.$ligne, $contrat->getLibelleTypeContrat());
                    $atexoExcel->setCellValue(0, 'G'.$ligne, $contrat->getLibelleStatut());
                    $atexoExcel->setCellValue(0, 'H'.$ligne, $contrat->getNumeroLongContratAcSad());
                    $atexoExcel->setCellValue(0, 'I'.$ligne, $contrat->getNumeroContratChapeau());

                    $procedureLibelle = '';
                    if ($contrat->getProcedurePassationPivot()) {
                        $procedure = (new Atexo_Consultation_ProcedureType())->retrieveTypeProcedurePivotObject($contrat->getProcedurePassationPivot());
                        if (empty($procedure)) {
                            $procedure = (new Atexo_Consultation_ProcedureType())->retrieveTypeProcedureObject($contrat->getProcedurePassationPivot());
                        }

                        if ($procedure instanceof CommonTypeProcedureOrganisme) {
                            $procedureLibelle = Prado::localize($procedure->getLibelleTypeProcedure());
                        } else {
                            $procedureLibelle = Prado::localize($procedure->getLibelle());
                        }

                    }

                    $atexoExcel->setCellValue(0, 'J'.$ligne, $procedureLibelle);
                    $atexoExcel->setCellValue(0, 'K'.$ligne, $contrat->getReferenceConsultation());
                    $atexoExcel->setCellValue(0, 'L'.$ligne, $contrat->getNomTitulaireContrat());
                    $siret = ('-' != $contrat->getSirenTitulaireContrat()) ? $contrat->getSirenTitulaireContrat().('-' != $contrat->getNicTitulaireContrat() ? $contrat->getNicTitulaireContrat() : '') : '';
                    $atexoExcel->setCellValue(0, 'M'.$ligne, $siret);
                    $atexoExcel->setCellValue(0, 'N'.$ligne, ($contrat->getPmePmiEntreprise() ? Prado::localize('DEFINE_OUI') : Prado::localize('DEFINE_NON')));
                    $montant = Atexo_Util::getMontantArronditEspace($contrat->getMontantContrat());
                    $atexoExcel->setCellValue(0, 'O'.$ligne, $montant);

                    $montantMaxEstime =
                        $contrat->getCommonTContratMulti()?->getMontantMaxEstime() ?: $contrat->getMontantMaxEstime()
                    ;
                    $atexoExcel->setCellValue(
                        0,
                        'P' . $ligne,
                        $montantMaxEstime > 0 ? Atexo_Util::getMontantArronditEspace($montantMaxEstime) : null
                    );

                    $arrayFormePrix = Atexo_Contrat_FormePrix::getFormes();
                    $formePrix = $arrayFormePrix[$contrat->getFormePrix()];

                    $atexoExcel->setCellValue(0, 'Q'.$ligne, ('' != $contrat->getFormePrix() ? $formePrix : ''));
                    $atexoExcel->setCellValue(0, 'R'.$ligne, $contrat->getLibelleCategorie());
                    $atexoExcel->setCellValue(0, 'S'.$ligne, $contrat->getCodeCpv1());
                    $atexoExcel->setCellValue(0, 'T'.$ligne, (Atexo_Consultation::hasClauseSociale($contrat) ? Prado::localize('DEFINE_OUI') : Prado::localize('DEFINE_NON')));
                    $atexoExcel->setCellValue(0, 'U'.$ligne, (Atexo_Consultation::hasClauseEnvironnementales($contrat) ? Prado::localize('DEFINE_OUI') : Prado::localize('DEFINE_NON')));
                    $atexoExcel->setCellValue(0, 'V'.$ligne, $contrat->getNomLieuPrincipalExecution());
                    $atexoExcel->setCellValue(0, 'W'.$ligne, $contrat->getDateAttribution());
                    $atexoExcel->setCellValue(0, 'X'.$ligne, $contrat->getDatePrevueNotification());
                    $atexoExcel->setCellValue(0, 'Y'.$ligne, $contrat->getDatePrevueFinContrat());
                    $atexoExcel->setCellValue(0, 'Z'.$ligne, $contrat->getDatePrevueMaxFinContrat());
                    $atexoExcel->setCellValue(0, 'AA'.$ligne, $contrat->getDateNotification());
                    $atexoExcel->setCellValue(0, 'AB'.$ligne, $contrat->getDateFinContrat());
                    $atexoExcel->setCellValue(0, 'AC'.$ligne, $contrat->getDateMaxFinContrat());
                    $atexoExcel->setCellValue(0, 'AD'.$ligne, $contrat->getDureeInitialeContrat());
                    $atexoExcel->setCellValue(0, 'AE'.$ligne, $contrat->getDenominationOrganisme());
                    $atexoExcel->setCellValue(0, 'AF'.$ligne, $contrat->getLibelleService());
                    $atexoExcel->setCellValue(0, 'AG'.$ligne, $contrat->getNomAgent());
                    $atexoExcel->setCellValue(0, 'AH'.$ligne, $contrat->getPrenomAgent());
                    $atexoExcel->setCellValue(0, 'AI'.$ligne, ((0 === $contrat->getPublicationContrat()) ? Prado::localize('DEFINE_OUI') : Prado::localize('DEFINE_NON')));

                    ++$ligne;
                }
            }
            $atexoExcel->setAllBorders('A1:AI'.($ligne - 1));
            //Sauvegarder en fichier
            $atexoExcel->writeExcelDocument();

            //Envoyer les donnees vers le navigateur pour telechargement
            $atexoExcel->telechargerExcelDocument();
        } catch (\Exception $ex) {
            $logger = Atexo_LoggerManager::getLogger('app');
            $logger->error('error ATX_XLS: Impossible de generer le fichier excel '.$ex->getMessage());
        }
    }

    public function getFooterImprimeLe()
    {
        return utf8_decode('&"Arial"&L&8'.Prado::localize('IMPRIME_LE').' &D&8 '.Prado::localize('A_POUR_IMPRIMER_LE_A_DATE').' &T&R&8'.Prado::localize('PAGE').' &P/&N');
    }
}
