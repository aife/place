<?php

namespace Application\Service\Atexo;

use Application\Service\Atexo\Chorus\Atexo_Chorus_Util;
use ZipArchive;

/**
 * Classe de manipulation des fichiers zip.
 *
 * @author ZAKI Anas <anas.zaki@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Zip
{
    /**
     * retourne un tableau ordonné des pièces du zip.
     *
     * @param string $dce_tmp_file le nom du fichier zip
     *
     * @return array tableau ordonné des pièces du zip
     */
    public static function getOrderedFilesForZip($dce_tmp_file, $deleteOriginalZip = false)
    {
        $dce_items = [];
        $dce_items_size = [];
        $zip = zip_open($dce_tmp_file);
        if (is_resource($zip)) {
            $index = 0;
            $i = 0;
            while ($zip_entry = zip_read($zip)) {
                ++$index;
                $dce_items[$index] = (new Atexo_Config())->toPfEncoding((new self())->formaterZipEntryName(zip_entry_name($zip_entry)));
                $dce_items_size[$index] = zip_entry_compressedsize($zip_entry);
                ++$i;
            }
            zip_close($zip);
            if ($deleteOriginalZip) {
                (new Atexo_Files())->delete_file($dce_tmp_file);
            }
        }
        $ordered_res = [];
        $root = '';
        asort($dce_items, SORT_NATURAL);
        foreach ($dce_items as $index_file => $one_file) {
            $tab = explode('/', $one_file);
            $profondeur = count($tab) - 1;
            $base_path = substr($one_file, 0, strrpos($one_file, '/'));
            // Hack très laid (LCH) pour le "bug" de l'affichage en arborescence
            if (2 == $profondeur && 1 == $index_file) {
                $ordered_res['__R__'][] = ['index' => 0,
                                                'profondeur' => 0,
                                                'base' => substr($base_path, 0, strpos($base_path, '/')),
                                                'name' => substr($base_path, 0, strpos($base_path, '/')),
                                                'type' => 'folder', ];
            }
            if (1 == $profondeur && 1 == $index_file) {
                $ordered_res['__R__'][0] = ['index' => 0,
                                                'profondeur' => 0,
                                                'base' => $base_path,
                                                'name' => $base_path,
                                                'type' => 'folder', ];
                $root = "$base_path/";
            }
            if ($root == $one_file && '' != $root) {
                $ordered_res['__R__'][0]['index'] = $index_file;
                continue;
            }
            // remplissage du tableau ordered_res qui contient tous les fichiers et dossiers + des infos
            // d'affichage (profondeur etc.). La clé du tableau est le chemin du fichier ou dossier
            if (!$ordered_res[$base_path]) {
                $ordered_res[$base_path] = [];
            }
            if ('/' == substr($one_file, strlen($one_file) - 1, 1)) {// terminé par un "/" => c'est un répertoire
                if (strrpos($base_path, '/')) {// si contient un "/" => sous-répertoire, on extrait la dernière partie après les /
                    $name = substr($base_path, strrpos($base_path, '/') + 1);
                } else {
                    $name = $base_path; // pas de "/", c'est le dossier racine
                }
                // on ajoute l'élément à la fin du tableau ordered_res
                $ordered_res[$base_path][] = ['index' => $index_file,
                                                        'profondeur' => $profondeur - 1,
                                                        'base' => substr($one_file, 0, strrpos($one_file, '/')),
                                                        'name' => $name,
                                                        'type' => 'folder', ];
            } else {// non terminé par un "/" => c'est un fichier
                $taille = $dce_items_size[$index_file];
                $tailleOctet = $taille;
                $taille = ceil($taille / 1000);
                if (strrpos($one_file, '/')) {// fichier dans un ré&pertoire
                    $name = substr($one_file, strrpos($one_file, '/') + 1, strlen($one_file));
                    $base = substr($one_file, 0, strrpos($one_file, '/') + 1);
                    if ($ordered_res['__R__'][0]['name'] != $base_path && empty($ordered_res[$base_path]) && $index_file > 1) {
                        $ordered_res[$base_path][] = ['index' => "x_$index_file", 'base' => $base_path, 'profondeur' => ($profondeur - 1), 'name' => "$base_path", 'type' => 'folder'];
                    }
                } else {// fichier à la racine
                    $name = $one_file;
                    $base = '__R__';
                }
                // on ajoute l'élément à la fin du tableau ordered_res
                $ordered_res[$base_path][] = ['index' => $index_file,
                                                        'profondeur' => $profondeur,
                                                        'base' => $base,
                                                        'name' => $name,
                                                        'taille' => Atexo_Util::arrondirSizeFile($taille),
                                                        'tailleOctet' => $tailleOctet,
                                                        'type' => 'file', ];
            }
        }

        return $ordered_res;
    }

    /**
     * ajoute un fichier à un zip.
     *
     * @param $fileName chemin du fichier à ajouter au zip
     * @param $zipFileName chemin du fichier zip
     *
     * @return $zipFileName|""
     */
    public function addFileToZip($fileName, $zipFileName, $setLcType = false)
    {
        if ($setLcType) {
            $lc_type = setlocale(LC_CTYPE, 0);
            setlocale(LC_CTYPE, 'en_US.UTF-8');
        }
        system(
            'nice zip -j '.escapeshellarg($zipFileName).' '.escapeshellarg($fileName).'  > '.Atexo_Config::getParameter('COMMON_TMP').'zip_error_adil 2>&1',
            $sys_answer
        );
        if ($setLcType) {
            setlocale(LC_CTYPE, $lc_type);
        }
        if (0 == $sys_answer) {
            return $zipFileName;
        } else {
            return '';
        }
    }

    /**
     * ajoute un fichier à un zip.
     *
     * @param $fileName chemin du fichier à ajouter au zip
     * @param $zipFileName chemin du fichier zip
     *
     * @return $zipFileName|""
     */
    public function addFileToZipWithPath($fileName, $path, $zipFileName)
    {
        system(
            'nice zip '.escapeshellarg($zipFileName).' '.escapeshellarg($fileName).' > '.Atexo_Config::getParameter('COMMON_TMP').'zip_error 2>&1',
            $sys_answer
        );
        if (0 == $sys_answer) {
            return $zipFileName;
        } else {
            return '';
        }
    }

    /**
     * Retourne l'index maximum du fichiers zip.
     *
     * @param $dceTmpFile chemin du fichier à ajouter au zip
     *
     * @return int l'index maximum du fichier zip
     */
    public function getMaxIndexZip($dceTmpFile, $deleteOriginalZip = false)
    {
        $index = null;
        $zip = zip_open($dceTmpFile);

        if (is_resource($zip)) {
            $index = 0;
            while (zip_read($zip)) {
                ++$index;
            }
            zip_close($zip);
            if ($deleteOriginalZip) {
                (new Atexo_Files())->delete_file($dceTmpFile);
            }
        }

        return $index;
    }

    /**
     * Vérifier l'extension du fichier zip.
     *
     * @param $dceTmpFile chemin du fichier à ajouter au zip
     *
     * @return bool true | false
     */
    public function verifyZipExtension($dceTmpFile)
    {
        if ('zip' == substr($dceTmpFile, -3)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Permet de verifier si le format du fichier DCE est valide.
     */
    public function verifyZipIntegrity($zipPath)
    {
        $infile = Atexo_Config::getParameter('COMMON_TMP').'dce_'.session_id().time().'.zip';
        system('cp '.escapeshellarg($zipPath).' '.escapeshellarg($infile), $retVal);
        if (0 == $retVal) {
            // tester l'intégrité du fichier zip
            // -T revient au zip precedent en cas d'echec
            $cmd = 'nice zip -T '.escapeshellarg($infile);
            $sys_answer = shell_exec($cmd);
            if (0 == strlen($sys_answer)) {
                return false;
            } else {
                return true;
            }
        }
    }

    public function addDirToZip($dirName, $zipFileName, $path)
    {
        system(
            'cd '.escapeshellarg($path).'; nice zip -r '.escapeshellarg($zipFileName).' '.escapeshellarg($dirName).'  > '.Atexo_Config::getParameter('COMMON_TMP').'zip_error_adil 2>&1',
            $sys_answer
        );
        if (0 == $sys_answer) {
            return $zipFileName;
        } else {
            return '';
        }
    }

    /**
     * extrait un fichier d'un zip.
     *
     * @param $zipFileName string chemin du fichier zip
     * @param $extractionPath string répertoire où le fichier sera extrait
     * @param $fileIndex int index du fichier dans le zip (index du fichier via zip_read)
     * @param  $removeAccents boolean supprime les accents du nom de fichier extrait
     * @param $deleteOriginalZip boolean supprime le fichier zip et son répertoire parent
     *
     * @return string chemin du fichier extrait | false
     */
    public static function extractZippedFile($zipFileName, $extractionPath, $fileIndex, $removeAccents = false, $deleteOriginalZip = false)
    {
        $fileName = null;
        $zip = zip_open($zipFileName);
        if (is_resource($zip)) {
            $index = 0;
            while ($zip_entry = zip_read($zip)) {
                ++$index;
                if ($fileIndex == $index) {
                    $fileName = zip_entry_name($zip_entry);
                    $fileName = (new Atexo_Chorus_Util())->convertToAscii($fileName);

                    if ($removeAccents) {
                        $fileName = Atexo_Util::OterAccents($fileName);
                        $fileName = str_replace('?', '_', $fileName);
                    }

                    $rep = explode('/', $fileName);
                    $fileName = $rep[count($rep) - 1];
                    $fileName = str_replace(' ', '_', $fileName);
                    $i = 0;
                    while ($fileContent = zip_entry_read($zip_entry, 1024)) {
                        Atexo_Util::write_file($extractionPath.'/'.$fileName, $fileContent, (0 == $i) ? 'w' : 'a');
                        ++$i;
                    }
                }
            }
            zip_close($zip);
            if ($deleteOriginalZip) {
                (new Atexo_Files())->delete_file($zipFileName);
            }

            return $extractionPath.'/'.$fileName;
        }

        return false;
    }

    /**
     * Verifie que les extensions des fichiers de l'arborescence sont incluses dans la liste des extensions autorisées par le parametre EXTENTION_PIECES_JOINTES_CHORUS.
     *
     * @param $idBlob: id blob
     * @param $fileZip chemin du zip, s'il est renseigné idblob doit etre à null
     *
     * @return 1 s'il trouve une extension non autorisée ou 0 sinon
     */
    public function zipInvalideSendChorus($idBlob, $fileZip = null)
    {
        if (!$fileZip) {
            $Org = Atexo_CurrentUser::getCurrentOrganism();
            $blobResource = Atexo_Blob::acquire_lock_on_blob($idBlob, $Org);
            $blob_file = $blobResource['blob']['pointer'];
        } else {
            $blob_file = $fileZip;
        }
        $result = 0;
        if ($blob_file) {
            $ordered_res = (new self())->getOrderedFilesForZip($blob_file, false);
            if (is_array($ordered_res)) {
                foreach ($ordered_res as $directory_files) {
                    foreach ($directory_files as $directory_one_file) {
                        $extensionFile = strtoupper(Atexo_Util::getExtension($directory_one_file['name']));
                        $data = explode('#', str_replace(' ', '', Atexo_Config::getParameter('EXTENTION_PIECES_JOINTES_CHORUS')));
                        if (('folder' != $directory_one_file['type']) &&
                            (!in_array($extensionFile, $data) || ($directory_one_file['tailleOctet'] > Atexo_Config::getParameter('CHORUS_MAX_UPLOAD_FILE_SIZE')))) {
                            $result = 1;
                        }
                    }
                }
            }
        }

        return $result;
    }

    /**
     * Permet d'ajouter un fichier dans un zip.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @param string $zipFile:       chemin complet du fichier zip
     * @param string $pathFileToAdd: chemin complet du fichier à ajouter
     * @param string $nameFileOnZip: nom souhaité du fichier à ajouter dans le zip
     * @param bool   $mode:          false si l'on souhaite ouvrir le zip uniquement, 'CREATE' pour tenter de créér le zip
     *                               (Pour le mode, voir les options sur http://www.php.net/manual/fr/ziparchive.open.php)
     * @return: nom du fichier zip si ajout OK, false sinon
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     * @copyright Atexo 2015
     */
    public function addFile2Zip($zipFile, $pathFileToAdd, $nameFileOnZip = null, $mode = false)
    {
        try {
            if (!$nameFileOnZip) {
                $nameFileOnZip = basename($pathFileToAdd);
            }
            $zip = new ZipArchive();
            if (!$mode) {
                $zipOpen = $zip->open($zipFile);
            } else {
                $zipOpen = (new self())->openZip($zip, $zipFile, $mode);
            }
            if (true === $zipOpen) {
                $zip->addFile($pathFileToAdd, Atexo_Util::encodeToOsVisiteur($nameFileOnZip));
                $zip->close();

                return $zipFile;
            }
            $logger = Atexo_LoggerManager::getLogger('app');
            $logger->error("Impossible d'ouvrir le zip, CODE ERREUR = $zipOpen");

            return false;
        } catch (\Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('app');
            $logger->error("Erreur Ajout fichier dans l'archive ZIP : Chemin du zip = $zipFile , chemin du fichier = $pathFileToAdd \n\nErreur = ".$e->getMessage()." \n\nTrace : ".$e->getTraceAsString()." \n\nMethode = Atexo_Zip::addFile2Zip");
        }
    }

    /**
     * Permet d'ajouter un repertoire dans un archive ZIP.
     *
     * @param string     $zipFile:      chemin complet du fichier zip
     * @param string     $directoryName : nom du repertoire a ajouter dans l'archive
     * @param bool $mode          : false si l'on souhaite ouvrir le zip uniquement, 'CREATE' pour tenter de créér le zip
     *                                  (Pour le mode, voir les options sur http://www.php.net/manual/fr/ziparchive.open.php)
     *
     * @return bool
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function addFolder2Zip($zipFile, $directoryName, bool $mode = false)
    {
        try {
            $zip = new ZipArchive();
            if (!$mode) {
                $zipOpen = $zip->open($zipFile);
            } else {
                $zipOpen = (new self())->openZip($zip, $zipFile, $mode);
            }
            if (true === $zipOpen) {
                $zip->addEmptyDir(Atexo_Util::encodeToOsVisiteur($directoryName));
                $zip->close();

                return $zipFile;
            }
            $logger = Atexo_LoggerManager::getLogger('app');
            $logger->error("Impossible d'ouvrir le zip, CODE ERREUR = $zipOpen");

            return false;
        } catch (\Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('app');
            $logger->error("Erreur Ajout repertoire dans l'archive ZIP : Chemin du zip = $zipFile , Nom repertoire = $directoryName \n\nErreur = ".$e->getMessage()." \n\nTrace : ".$e->getTraceAsString()." \n\nMethode = Atexo_Zip::addFolder2Zip");
        }
    }

    /**
     * Tente d'ouvrir le zip en fonction du mode spécifié.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @param object $zipArchive: instance de ZipArchive
     * @param string $zipFile:    chemin complet du fichier zip
     * @param string $mode:       'CREATE' pour tenter de créér le zip, 'OVERWRITE' pour tenter d'écraser le zip ...
     *                            (Pour le mode, voir les options sur http://www.php.net/manual/fr/ziparchive.open.php)
     *
     * @return Retourne TRUE en cas de succès ou sinon, le code erreur (voir les codes d'erreurs sur http://www.php.net/manual/fr/ziparchive.open.php)
     */
    public function openZip(&$zipArchive, $zipFile, $mode)
    {
        $zipOpen = null;
        if (0 == strcmp($mode, 'CREATE')) {
            $zipOpen = $zipArchive->open($zipFile, ZipArchive::CREATE);
        } elseif (0 == strcmp($mode, 'OVERWRITE')) {
            $zipOpen = $zipArchive->open($zipFile, ZipArchive::OVERWRITE);
        } elseif (0 == strcmp($mode, 'EXCL')) {
            $zipOpen = $zipArchive->open($zipFile, ZipArchive::EXCL);
        } elseif (0 == strcmp($mode, 'CHECKCONS')) {
            $zipOpen = $zipArchive->open($zipFile, ZipArchive::CHECKCONS);
        }

        return $zipOpen;
    }

    /**
     * formatter zip entry name.
     *
     * @param string $name
     *
     * @return string $name formatte
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 2018-esr
     *
     * @copyright Atexo 2018
     */
    public function formaterZipEntryName($name)
    {
        $nameZip = $name;
        if (function_exists('iconv')) {
            $encodageCP1252 = 'CP1252';
            $encodageCP850 = 'CP850';
            if ($inputEncoding = mb_detect_encoding($name)) {
                if (!($nameZip = iconv($inputEncoding, $encodageCP1252, $name))) {
                    $nameZip = iconv($encodageCP850, $encodageCP1252, $name);
                } else {
                    $nameZip = iconv($inputEncoding, 'UTF-8', $name);
                }
            } else {
                $nameZip = iconv($encodageCP850, $encodageCP1252, $name);
            }
        }

        return $nameZip;
    }

    /**
     * retourne un tableau ordonné des pièces du zip.
     *
     * @param string $dce_tmp_file le nom du fichier zip
     *
     * @return array tableau ordonné des pièces du zip
     */

    /**
     * retourne un tableau ordonné des pièces du zip.
     *
     * @param string $dce_tmp_file le nom du fichier zip
     *
     * @return array tableau ordonné des pièces du zip
     */
    public static function getArrayOrderedFilesForZip($dce_tmp_file, &$maxIndex, $deleteOriginalZip = false)
    {
        $dceInfo = (new self())->getInfoFilesZip($dce_tmp_file, $maxIndex, $deleteOriginalZip);
        $dce_items = $dceInfo['chemins'];
        $dce_items_size = $dceInfo['tailles'];
        $ordered_res = [];
        if (is_array($dce_items)) {
            asort($dce_items, SORT_NATURAL);
        }
        foreach ($dce_items as $index_file => $one_file) {
            $base_path = substr($one_file, 0, strrpos($one_file, '/'));
            $arbo = explode('/', rtrim($base_path, '/'));
            if (strrpos($one_file, '/')) {// fichier dans un ré&pertoire
                $name = substr($one_file, strrpos($one_file, '/') + 1, strlen($one_file));
                $base = substr($one_file, 0, strrpos($one_file, '/') + 1);
            } else {// fichier à la racine
                $name = $one_file;
                $base = $one_file;
            }
            $taille = $dce_items_size[$index_file];
            $tailleOctet = $taille;
            $taille = ceil($taille / 1000);
            // on ajoute l'élément à la fin du tableau ordered_res
            $count = count($arbo);
            $element = &$ordered_res;
            if ('/' === substr($one_file, strlen($one_file) - 1, 1)) {
                for ($i = 0; $i < $count - 1; ++$i) {
                    if ($arbo[$i] && !isset($element[$arbo[$i]]['name'])) {
                        $element[$arbo[$i]] = [
                            'index' => ++$maxIndex,
                            'base' => rtrim($arbo[$i], '/'),
                            'name' => $arbo[$i],
                            'type' => 'folder',
                            'elements' => [], ];
                    }
                    $element = &$element[$arbo[$i]]['elements'];
                }
                $element[$arbo[$i]] = [
                    'index' => $index_file,
                    'base' => rtrim($base, '/'),
                    'name' => $arbo[$i],
                    'type' => 'folder',
                    'elements' => [], ];
            } else {
                if ($count > 0 && !empty($arbo[0])) {
                    for ($i = 0; $i < $count; ++$i) {
                        if ($arbo[$i] && !isset($element[$arbo[$i]]['name'])) {
                            $element[$arbo[$i]] = [
                                'index' => ++$maxIndex,
                                'base' => rtrim($base, '/'),
                                'name' => $arbo[$i],
                                'type' => 'folder',
                                'elements' => [], ];
                        }
                        $element = &$element[$arbo[$i]]['elements'];
                    }
                }
                $element[$name] = [
                    'index' => $index_file,
                    'base' => $base,
                    'name' => $name,
                    'taille' => Atexo_Util::arrondirSizeFile($taille),
                    'tailleOctet' => $tailleOctet,
                    'type' => 'file', ];
            }
        }

        return $ordered_res;
    }

    public function getInfoFilesZip($zip_tmp_file, &$index, $deleteOriginalZip = false)
    {
        $zip_items = [];
        $zip = zip_open($zip_tmp_file);
        if (is_resource($zip)) {
            $index = 0;
            while ($zip_entry = zip_read($zip)) {
                ++$index;
                $zip_items['chemins'][$index] = (new Atexo_Config())->toPfEncoding((new self())->formaterZipEntryName(zip_entry_name($zip_entry)));
                $zip_items['tailles'][$index] = zip_entry_compressedsize($zip_entry);
            }
            zip_close($zip);
            if ($deleteOriginalZip) {
                (new Atexo_Files())->delete_file($zip_tmp_file);
            }
        }

        return $zip_items;
    }

    /*
    * Génère l'arborescence d'un zip
    * @param $blobResource: bloc ressource
    * @param $idBlob: id blob
    * @param $arrayFilesItem: tableau contenant les index des fichiers
    * @param $fileZip: chemin du fichier zip, si ce dernier est utilisé $blobResource et $idBlob doivent etre à null
    * @param $fileItemHtml: id html des inputs dans l'arborescence
    * @param $showSelectedFile: si ce parametre est à true, on n'affiche que les fichiers selectionnés sans les cases à cocher
    * @return: le code source de l'arborescence
    */
    public function getArborescence($blobResource, $idBlob, $arrayFilesItem, $fileZip, $fileItemHtml, $showSelectedFile = false, $callFromDownloadDce = false, $tailFileChorusMax = false)
    {
        if (!$fileZip) {
            $file = $blobResource['blob']['pointer'];
        } else {
            $file = $fileZip;
        }
        $result_affiche = ' ';
        if ($file) {
            $maxIndex = 0;
            $ordered_res = (new self())->getArrayOrderedFilesForZip($file, $maxIndex, false);
            if ($showSelectedFile) {
                $result_affiche = (new self())->getArboHtmlForSelectedFile($ordered_res, $arrayFilesItem, $fileItemHtml);
            } else {
                $zipInvalideSendChorus = false;
                if (!$callFromDownloadDce) {
                    $zipInvalideSendChorus = (new self())->zipInvalideSendChorus($idBlob, $fileZip);
                }
                $result_affiche = (new self())->getArboHtml($ordered_res, $arrayFilesItem, $fileItemHtml, $maxIndex, $callFromDownloadDce, $tailFileChorusMax, $zipInvalideSendChorus);
            }
        }

        return $result_affiche;
    }

    /**
     * @param $arbo
     * @param $arrayFilesItem
     * @param $fileItemHtml
     * @param $maxIndex
     * @param bool $callFromDownloadDce
     * @param bool $tailFileChorusMax
     * @param bool $zipInvalideSendChorus
     *
     * @return string
     */
    public function getArboHtml($arbo, $arrayFilesItem, $fileItemHtml, $maxIndex, $callFromDownloadDce = false, $tailFileChorusMax = false, $zipInvalideSendChorus = false)
    {
        $html = '';
        if (is_array($arbo) && count($arbo)) {
            $html = '<ul>';
            foreach ($arbo as $element) {
                $checked = '';
                $html .= '<li>';
                if (in_array($element['index'], $arrayFilesItem)) {
                    $checked = 'checked="checked"';
                }
                $extensionFile = strtoupper(Atexo_Util::getExtension($element['name']));
                $data = explode('#', str_replace(' ', '', Atexo_Config::getParameter('EXTENTION_PIECES_JOINTES_CHORUS')));
                $disabled = '';
                $disabledCss = '';
                if (!$callFromDownloadDce && ('folder' != $element['type']) && !in_array($extensionFile, $data)) {
                    $disabled = 'disabled="false"';
                    $disabledCss = ' disabled-txt';
                }
                if ($tailFileChorusMax && $element['tailleOctet'] && $element['tailleOctet'] > Atexo_Config::getParameter('CHORUS_MAX_UPLOAD_FILE_SIZE')) {
                    $disabled = 'disabled="false"';
                    $disabledCss = ' disabled-txt';
                }
                $html .= '<span class="check-bloc">';
                $html .= '<input '.$disabled." type=\"checkbox\" name=\"$fileItemHtml".'_'.$element['index']."\"  id=\"$fileItemHtml".'_'.$element['index']
                    .'" '.$checked.' value="'.$element['base'];
                if ($callFromDownloadDce) {
                    $html .= "\" onClick=\"javascript:CheckUnCheckDCEItems(this.id, '".$maxIndex."')\"   />\n";
                } else {
                    $html .= "\" onclick=\"javascript:if(zipInvalideExtensionToChorus('".$element['type']."',".$zipInvalideSendChorus.", '".
                        implode(', ', $data)."')==false) {return false;};CheckUnCheckFileItems(this.id, '".$maxIndex."', '$fileItemHtml');\"   />\n";
                }
                $html .= '</span>';
                $html .= "<label class='content-bloc bloc-580 dce_entreprise".$disabledCss."' for='".$fileItemHtml.'_'.$element['index']."'>";
                $html .= (('folder' == $element['type']) ? '<strong>' : '').'<span class="float-left">';
                $html .= $element['name'].'&nbsp;'.(($element['taille']) ? '( '.$element['taille'].' )' : '');
                $html .= '</span>';
                $html .= (('folder' == $element['type']) ? '</strong>' : '');
                if ('' != $disabled) {
                    $html .= "<span class=\"float-left\"><img src=\"themes/images/picto-info.gif\"
												onmouseover=\"afficheBulle('msgInfoBulle', this)\"
												onmouseout=\"cacheBulle('msgInfoBulle')\"
												class=\"picto-info-intitule\"
												alt=\"Info-bulle\"
												style=\"margin-top:0;\"
												title=\"Info-bulle\" /> </span>";
                }
                $html .= '</label>';
                if ('folder' == $element['type']) {
                    $html .= (new self())->getArboHtml($element['elements'], $arrayFilesItem, $fileItemHtml, $maxIndex, $callFromDownloadDce, $tailFileChorusMax, $zipInvalideSendChorus);
                }
                $html .= '</li>';
            }
            $html .= '</ul>';
        }

        return $html;
    }

    /**
     * @param $arbo
     * @param $arrayFilesItem
     * @param $fileItemHtml
     *
     * @return string
     */
    public function getArboHtmlForSelectedFile($arbo, $arrayFilesItem, $fileItemHtml)
    {
        $html = '';
        if ((is_array($arrayFilesItem) && count($arrayFilesItem)) && (is_array($arbo) && count($arbo))) {
            $html = '<ul>';
            foreach ($arbo as $element) {
                $html .= '<li>';
                if (in_array($element['index'], $arrayFilesItem)) {
                    $html .= (('folder' == $element['type']) ? '<strong>' : '').'<span class="check-bloc">';
                    $html .= '</span><label class="content-bloc bloc-580" for="'.$fileItemHtml.'_'.$element['index'].'">';
                    $html .= '-&nbsp; ';
                    $html .= $element['name'].'&nbsp;'.(($element['taille']) ? '( '.$element['taille'].' )' : '');
                    $html .= '</label>'.(('folder' == $element['type']) ? '</strong>' : '');
                }
                if ('folder' == $element['type']) {
                    $html .= (new self())->getArboHtmlForSelectedFile($element['elements'], $arrayFilesItem, $fileItemHtml);
                }
                $html .= '</li>';
            }
            $html .= '</ul>';
        }

        return $html;
    }
}
