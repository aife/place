<?php

namespace Application\Service\Atexo;
use App\Entity\Agent;
use App\Service\PradoPasswordEncoderInterface;
use Application\Library\Propel\Connection\PropelPDO;
use Application\Propel\Mpe\CommonAgentTechniqueAssociationPeer;
use Application\Propel\Mpe\CommonAgentTechniqueTokenPeer;
use Zend\Http\Client;
use Zend\Http\Request;

use App\Security\PasswordEncoder\CommonPasswordEncoder;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonAgent;
use Application\Propel\Mpe\CommonAgentPeer;
use Application\Propel\Mpe\CommonAgentQuery;
use Application\Propel\Mpe\CommonAgentTechniqueToken;
use Application\Propel\Mpe\CommonAgentTechniqueTokenQuery;
use Application\Service\Atexo\Config\Atexo_Config_Exception;
use Exception;


class Atexo_TiersAuthentification
{
    /*
     * demande un ticket d'authetification,retourne un xml contenant le ticket
     */
    public function getTicketAuthentification($login, $pwd)
    {
        try {
            $client = new Client();
            $client->setUri(Atexo_Config::getParameter('URL_TIERS_REST_AUTENTIFICATION')
                . '/' . $login . '/' . $pwd);
            $client->setMethod(Request::METHOD_GET);
            $res = $client->send();
            $domDoc = (array)simplexml_load_string(trim($res));

            return $domDoc[0];
        } catch (\Exception $e) {
            echo "\n" . $e->getMessage();
            exit;
        }
    }

    /*
     * retourne le ticket d'authetification vcalide si existe, sinn cree un nouveau
     */
    public function createTicketAuthentificationTiers($login, $password)
    {
        $ticket = self::getTicket($login, $password);

        $xml = '<?xml version="1.0" encoding="UTF-8"?>' .
            '<ticket>' . $ticket . '</ticket>';

        return $xml;
    }

    /**
     * @param $login
     * @param $password
     *
     *
     * @throws Atexo_Config_Exception
     * @throws PropelException
     */
    public static function getTicket($login, $password): bool|string
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') .
            Atexo_Config::getParameter('CONST_READ_WRITE'));
        $agentTechniqueToken = self::getAgentTechniqueTokenByLoginPwd($login, $password, $connexionCom);
        if ($agentTechniqueToken instanceof CommonAgentTechniqueToken) {
            $agentTechniqueToken->setDateExpiration(Atexo_Util::dateDansFutur(
                date('Y-m-d H:i:s'),
                (int)Atexo_Config::getParameter('NBR_HEURE_MAINTENANCE_CNX')
            ));
            $agentTechniqueToken->save($connexionCom);
            return $agentTechniqueToken->getToken();
        }
        $agentTechnique = self::retrieveAgentTechniqueByLoginPassword($login, $password);

        return (new Atexo_TiersAuthentification())->createNewAgentTechniqueToken($agentTechnique, $connexionCom);
    }

    /**
     * @param $agentTechnique
     * @param $connexionCom
     * @throws PropelException
     * @throws \PropelException
     */
    public function createNewAgentTechniqueToken($agentTechnique, $connexionCom): string|bool
    {
        if ($agentTechnique instanceof CommonAgent) {
            $token = uniqid() . date('Ymdhis');
            $agentTechniqueToken = new CommonAgentTechniqueToken();
            $agentTechniqueToken->setToken($token);
            $agentTechniqueToken->setAgentId($agentTechnique->getId());
            $agentTechniqueToken->setCreateAt(date('Y-m-d H:i:s'));
            $agentTechniqueToken->setUpdateAt(date('Y-m-d H:i:s'));
            $agentTechniqueToken->setDateExpiration(Atexo_Util::dateDansFutur(
                date('Y-m-d H:i:s'),
                Atexo_Config::getParameter('NBR_HEURE_MAINTENANCE_CNX')
            ));
            $agentTechniqueToken->save($connexionCom);
            return $token;
        } else {
            return false;
        }
    }

    /**
     * @param $login
     * @param $password
     * @param null $connexionCom
     *
     *
     * @throws Atexo_Config_Exception
     * @throws PropelException
     * retourne le tiers en utilisant ($login,$password)
     */
    public static function getAgentTechniqueTokenByLoginPwd($login, $password, $connexionCom = null)
    {
        if (null === $connexionCom) {
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') .
                Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
        $agentTechnique = self::retrieveAgentTechniqueByLoginPassword($login, $password, $connexionCom);
        if ($agentTechnique) {
            $dateExpiration = date('Y-m-d H:i:s');
            $agentTechniqueToken = (new CommonAgentTechniqueTokenQuery())
                ->filterByDateExpiration($dateExpiration, Criteria::GREATER_THAN)
                ->findOneByAgentId($agentTechnique->getId());
            if ($agentTechniqueToken instanceof CommonAgentTechniqueToken) {
                return $agentTechniqueToken;
            }
        }
        return false;
    }

    /*
     * verifie la validité du ticket envoyé par une application tierce pour authentification
     */
    public static function retrieveAgentTechniqueByLoginPassword(
        string $login,
        string $password,
        PropelPDO $connexionCom = null
    ): ?CommonAgent {
        if (null === $connexionCom) {
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') .
                Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
        $agentTechnique = (new CommonAgentQuery())->findOneByArray(['login' => $login], $connexionCom);

        // Vérification du password via le passwordEncoder
        /** @var CommonPasswordEncoder $passwordEncoder */
        $passwordEncoder = Atexo_Util::getSfService(CommonPasswordEncoder::class);

        $isValidPassword = $agentTechnique && $passwordEncoder->isPasswordValid(
            Agent::class,
            $agentTechnique->getPassword(),
            $password
        );

        return $isValidPassword ? $agentTechnique : null;
    }

    /**
     * @param $token
     * @param null $connexionCom
     *
     * @return CommonAgentTechniqueToken
     *
     * @throws Atexo_Config_Exception
     * @throws PropelException
     *                                cherche un ticket d'authentifation valide si existe
     */
    public function retrieveAgentTechniqueTokenByToken($token, $connexionCom = null)
    {
        if (null === $connexionCom) {
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') .
                Atexo_Config::getParameter('CONST_READ_ONLY'));
        }

        return (new CommonAgentTechniqueTokenQuery())->findOneByToken($token);
    }

    /**
     * Verifie l'expiration du token.
     *
     * @param $token
     *
     * @return bool
     *
     * @throws Atexo_Config_Exception
     * @throws PropelException
     */
    public function VerifyToken($token)
    {
        $valid = false;
        $cnx = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') .
            Atexo_Config::getParameter('CONST_READ_ONLY'));

        $c = new Criteria();
        $c->add(CommonAgentTechniqueTokenPeer::TOKEN, $token);
        $c->add(
            CommonAgentTechniqueTokenPeer::DATE_EXPIRATION,
            date('Y-m-d H:i:s'),
            Criteria::GREATER_THAN
        );
        $agentTechniqueToken = CommonAgentTechniqueTokenPeer::doSelectOne($c, $cnx);
        if ($agentTechniqueToken instanceof CommonAgentTechniqueToken) {
            $valid = true;
        }

        return $valid;
    }

    /**
     * Retourne le login de la table Tiers.
     *
     * @param $ticket
     *
     * @return string
     *
     * @throws PropelException
     */
    public function getLoginByIdSsoTiers($ticket)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));

        $login = '';
        $c = new Criteria();
        $c->addJoin(CommonAgentPeer::ID, CommonAgentTechniqueTokenPeer::AGENT_ID, Criteria::JOIN);
        $c->add(CommonAgentTechniqueTokenPeer::TOKEN, $ticket);

        $agentTechnique = CommonAgentPeer::doSelectOne($c, $connexion);
        if ($agentTechnique instanceof CommonAgent) {
            $login = $agentTechnique->getLogin();
        }

        return $login;
    }

    /**
     * @param $token
     * @param $acronymeOrg
     * @param $services
     *
     * @return bool
     *
     * @throws Atexo_Config_Exception
     * @throws PropelException
     */
    public function verifieAccesToInfo($token, $acronymeOrg, $services)
    {
        $valid = false;
        $cnx = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') .
            Atexo_Config::getParameter('CONST_READ_ONLY'));

        $c = new Criteria();
        $c->add(CommonAgentTechniqueTokenPeer::TOKEN, $token);
        $agentTechniqueToken = CommonAgentTechniqueTokenPeer::doSelectOne($c, $cnx);
        if ($agentTechniqueToken instanceof CommonAgentTechniqueToken) {
            $c1 = new Criteria();
            $c1->add(CommonAgentTechniqueAssociationPeer::ID_AGENT, $agentTechniqueToken->getAgentId());
            if (CommonAgentTechniqueAssociationPeer::doCount($c1, $cnx)) {
                $c1->add(CommonAgentTechniqueAssociationPeer::ORGANISME, $acronymeOrg);
                if (is_array($services) && !empty($services) && !in_array(0, $services)) {
                    $c1->add(CommonAgentTechniqueAssociationPeer::SERVICE_ID, $services, Criteria::IN);
                }
                if (CommonAgentTechniqueAssociationPeer::doCount($c1, $cnx)) {
                    $valid = true;
                }
            } else {
                $valid = true;
            }
        }

        return $valid;
    }

    /**
     * @param $token
     *
     * @return array
     *
     * @throws Atexo_Config_Exception
     * @throws PropelException
     * @throws Exception
     */
    public function getAllOrganismesServicesAllowed($token)
    {
        $orgs = [];
        $cnx = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') .
            Atexo_Config::getParameter('CONST_READ_ONLY'));

        $c = new Criteria();
        $c->add(CommonAgentTechniqueTokenPeer::TOKEN, $token);
        $agentTechniqueToken = CommonAgentTechniqueTokenPeer::doSelectOne($c, $cnx);
        if ($agentTechniqueToken instanceof CommonAgentTechniqueToken) {
            $c1 = new Criteria();
            $c1->add(CommonAgentTechniqueAssociationPeer::ID_AGENT, $agentTechniqueToken->getAgentId());
            $agentAssociations = CommonAgentTechniqueAssociationPeer::doSelect($c1, $cnx);
            foreach ($agentAssociations as $agentAssociation) {
                if ($agentAssociation instanceof CommonAgentTechniqueAssociation) {
                    if ($agentAssociation->getIdService()) {
                        $serviceChildren = Atexo_EntityPurchase::retrieveAllChildrenServices(
                            $agentAssociation->getIdService(),
                            $agentAssociation->getOrganisme(),
                            true
                        );
                    } else {
                        $serviceChildren = [$agentAssociation->getIdService()];
                    }
                    $orgs[$agentAssociation->getOrganisme()] = $serviceChildren;
                }
            }
        } else {
            throw new Exception('token invalid');
        }

        return $orgs;
    }
}
