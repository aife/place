<?php

namespace Application\Service\Atexo\Newsletter;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonNewsletter;
use Application\Propel\Mpe\CommonNewsletterPeer;
use Application\Propel\Mpe\CommonNewsletterPieceJointePeer;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Files;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * Classe de gestion des newsletter.
 *
 * @author Maatalla Houriya <houriya.maatalla@atexo.com>
 * @copyright Atexo 2011
 *
 * @version 1.0
 *
 * @since MPE-4.0
 */
class Atexo_Newsletter_Newsletter
{
    /**
     * Fonction qui retourne la liste des newsletters liées au service_rédacteur passé en paramètre.
     *
     * @param int  $idService l'id du service
     * @param bool $org
     * @param bool $isRedac
     *
     * @return array CommonNewsletter un tableau des objets CommonNewsletter
     *
     * @author Maatalla Houriya <houriya.maatalla@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.0
     *
     * @copyright Atexo 2015
     */
    public function retrieveNewsletter($idService, $org = false, $isRedac = false)
    {
        if (!$org) {
            $org = Atexo_CurrentUser::getOrganismAcronym();
        }
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        if ($idService) {
            $c->add(CommonNewsletterPeer::ID_SERVICE_REDACTEUR, $idService);
        } else {
            $c->add(CommonNewsletterPeer::ID_SERVICE_REDACTEUR, null,  Criteria::ISNULL);
        }
        $c->add(CommonNewsletterPeer::ORGANISME, $org);
        $c->add(CommonNewsletterPeer::STATUT, null, Criteria::ISNOTNULL);
        if ($isRedac) {
            $c->add(CommonNewsletterPeer::IS_REDAC, '1');
        } else {
            $c->add(CommonNewsletterPeer::IS_REDAC, '0');
        }
        $newsletter = CommonNewsletterPeer::doSelect($c, $connexionCom);
        if ($newsletter) {
            return $newsletter;
        }

        return [];
    }

    /**
     * Fonction qui retourne la liste des newsletter liées à l'id passé en paramètre.
     *
     * @param : $id l'id du message
     *
     * @return : un tableau d'objet Newsletter ou un tableau vide si aucun élement trouvé
     */
    public function retrieveNewsletterById($id, $org = false)
    {
        if (!$org) {
            $org = Atexo_CurrentUser::getCurrentOrganism();
        }
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonNewsletterPeer::ID, $id);
        $c->add(CommonNewsletterPeer::ORGANISME, $org);
        $newsletter = CommonNewsletterPeer::doSelectOne($c, $connexionCom);
        if ($newsletter) {
            return $newsletter;
        } else {
            return [];
        }
    }

    /**
     * Fonction qui retourne la liste des newsletters liées à l'organisme en paramètre.
     *
     * @param : $organsime : l'oganisme de la newsletter
     *
     * @return : un tableau d'objet Newsletter ou un tableau vide si aucun élement trouvé
     */
    public static function retrieveNewsletterByStatut($statut, $con = null, $typeNewsLetter = false)
    {
        if (null === $con) {
            $con = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        }
        $criteria = new Criteria();
        $criteria->add(CommonNewsletterPeer::STATUT, $statut);
        if ('Redac' == $typeNewsLetter) {
            $criteria->add(CommonNewsletterPeer::IS_REDAC, '1');
        } elseif ('Mpe' == $typeNewsLetter) {
            $criteria->add(CommonNewsletterPeer::IS_REDAC, '0');
        }
        $v = CommonNewsletterPeer::doSelect($criteria, $con);
        if ($v) {
            return $v;
        }

        return [];
    }

    /**
     * Fonction qui retourne la liste des pièces jointes de la newsletter liés passée en paramétre.
     *
     * @param : $idNewsletter : l'id de la newsletter
     * @param : $organisme    : l'organisme
     *
     * @return : un tableau d'objet NewsletterPieceJointe ou un tableau vide si aucun élement trouvé
     */
    public static function retrieveNewsletterPiceceJointe($idNewsletter, $organisme, $con = null)
    {
        if (null === $con) {
            $con = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        }
        $criteria = new Criteria();
        $criteria->add(CommonNewsletterPieceJointePeer::ID_NEWSLETTER, $idNewsletter);
        $criteria->add(CommonNewsletterPieceJointePeer::ORGANISME, $organisme);
        $v = CommonNewsletterPieceJointePeer::doSelect($criteria, $con);
        if ($v) {
            return $v;
        } else {
            return [];
        }
    }

    /**
     * Fonction qui supprime l'objet pj à partir de l'id passé en paramètre.
     *
     * @param  $id id de la pj à supprimer
     * @param $org Organismes
     *
     * @return bool
     */
    public static function DeletePj($idpj, $org)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $PJ = CommonNewsletterPieceJointePeer::retrieveByPk($idpj, $org, $connexionCom);
        if (is_object($PJ)) {
            $PJ->delete($connexionCom);
        }
    }

    /**
     * Fonction qui supprime une newsletter de la base de donnée.
     *
     * @param : $id : id de la newsletter à supprimer
     */
    public function deleteNewsletter($id, $org = null)
    {
        if (!$org) {
            $org = Atexo_CurrentUser::getCurrentOrganism();
        }
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $Newsletter = CommonNewsletterPeer::retrieveByPK($id, $org, $connexion);

        return $Newsletter->delete($connexion);
    }

    /**
     * Fonction qui envoi une newsletter.
     *
     * @param : $newsletter : la newletter à envoyer
     */
    public function envoyerNewsletter(CommonNewsletter $Newsletter)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $c = new Criteria();
        $cc = '';
        $organisme = $Newsletter->getOrganisme();
        $c->add(CommonNewsletterPieceJointePeer::ORGANISME, $organisme);
        $taille = 0;
        $validePJ = false;
        $envoiMail = false;
        $from = $Newsletter->getEnvoyeParEmail();
        $Destinataires = $Newsletter->getDestinataires();
        $idService = $Newsletter->getIdServiceDestinataire();
        $idTypeDestinataire = $Newsletter->getTypeDestinataire();
        //si egale a 1 on envoie que pr les radeacteurs
        $typeNewsLetter = $Newsletter->getIsRedac();

        $to = [];
        // choix destinataires libres
        if ($Destinataires && ($idTypeDestinataire == Atexo_Config::getParameter('CHOIX_LIBRE'))) {
            $to = explode(',', $Destinataires);
        }
        // choix d'une entité bien précise (celle de l'agent ou autre)
        elseif (($idTypeDestinataire == Atexo_Config::getParameter('AUTRE_ENTITE')) || ($idTypeDestinataire == Atexo_Config::getParameter('MON_ENTITE'))) {
            if ('1' == $Newsletter->getInclureServicesDescendants()) {
                $services = [];
                $services = Atexo_EntityPurchase::getAllChilds((int) $idService, $services, $organisme);
                if (is_array($services)) {
                    foreach ($services as $oneService) {
                        $agents = Atexo_Agent::retriveServiceAgents($organisme, $oneService->getId());
                        if ($typeNewsLetter) {
                            $agents = (new Atexo_Agent())->getRedacteurs($agents);
                        }
                        foreach ($agents as $oneAgent) {
                            if ('1' == $oneAgent->getActif()) {
                                $to[] = $oneAgent->getEmail();
                            }
                        }
                    }
                    // si choix de mon entité
                    if ('0' == $Newsletter->getIdServiceDestinataire()) {
                        $agents = Atexo_Agent::retriveServiceAgents($organisme, '0');
                        if ($typeNewsLetter) {
                            $agents = (new Atexo_Agent())->getRedacteurs($agents);
                        }
                        foreach ($agents as $oneAgent) {
                            if ('1' == $oneAgent->getActif()) {
                                $to[] = $oneAgent->getEmail();
                            }
                        }
                    }
                }
            } else {
                $agents = Atexo_Agent::retriveServiceAgents($organisme, $idService);
                if ($typeNewsLetter) {
                    $agents = (new Atexo_Agent())->getRedacteurs($agents);
                }
                foreach ($agents as $oneAgent) {
                    if ('1' == $oneAgent->getActif()) {
                        $to[] = $oneAgent->getEmail();
                    }
                }
            }
        }

        $subject = $Newsletter->getObjet();
        $corps = nl2br($Newsletter->getCorps());
        $PJs = $Newsletter->getCommonNewsletterpiecejointes($c, $connexionCom);
        if (is_array($PJs) && 0 != count($PJs)) {
            $validePJ = true;
        }
        $dossier = '';
        $zipPj = (new Atexo_Message())->getZipPj($PJs, $taille, $organisme, $dossier);
        $taille = str_replace('.', ',', round($taille / (1_048_985.64)));
        // TODO test sur lecture du fichier
        if ($zipPj) {
            $code = (new Atexo_Files())->read_file($zipPj);
        }
        $fileName = Atexo_Config::getParameter('DEFINE_TEXT_PJ_ZIP');

        $message = file_get_contents(Atexo_Config::getParameter('PATH_FILE_MAIL_NEWSLETTER'));
        $message = str_replace('IMAGE_BANDEAU', Atexo_Message::getImageBandeau(), $message);
        $message = str_replace('SALUTATION', 'Bonjour', $message);
        $message = str_replace('TITRE', '', $message);
        $message = str_replace('CORPS_MAIL', $corps, $message);

        if ($to) {
            //envoi à chaque Destinataire apart pour ne pas divulguer les adresses des agents
            foreach ($to as $oneDest) {
                //si la piece jointe existe alors mailWithAttachement
                if ($validePJ) {
                    if ((new Atexo_Message())->mailWithAttachement($from, $oneDest, $subject, $message, $cc, $code, $fileName, '', true)) {
                        $envoiMail = true;
                    }
                }
                //sinon alors simpleMail
                else {
                    if (Atexo_Message::simpleMail($from, $oneDest, $subject, $message, $cc, '', false, true)) {
                        $envoiMail = true;
                    }
                }
            }
        }
        if (is_file($zipPj)) {
            if (!unlink($zipPj)) {
                Prado::log('Impossible de supprimer le fichier : '.$zipPj.' fonction envoyerNewsletter', TLogger::ERROR, 'Atexo');
            }
            Atexo_Util::removeDirectoryRecursively($dossier);
        }
        if ($envoiMail) {
            $Newsletter->setDateEnvoi(date('Y-m-d H:i:s'));
            $Newsletter->setStatut(Atexo_Config::getParameter('STATUS_ENVOYE'));
            $Newsletter->save($connexionCom);

            return $Newsletter->getId();
        } else {
            return false;
        }
    }

    /**
     * récupere l'objet pj à partir de l'id de la piéce passé en paramètre.
     *
     * @param  $id id de la pj
     * @param $org Organismes
     *
     * @return bool
     */
    public static function getPjByIdPiece($idpj, $org)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonNewsletterPieceJointePeer::PIECE, $idpj, Criteria::EQUAL);
        $c->add(CommonNewsletterPieceJointePeer::ORGANISME, $org, Criteria::EQUAL);
        $PJ = CommonNewsletterPieceJointePeer::doSelect($c, $connexionCom);

        return $PJ[0];
    }
}
