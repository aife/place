<?php

namespace Application\Service\Atexo;

use Application\Service\Atexo\Config\Atexo_Config_Exception;

/**
 * Class Atexo_ConfigClientBuilder.
 *
 * @author Sébastien Lepers <sebastien.lepers@atexo.com>
 */
class Atexo_ConfigClientBuilder
{
    private static array $ignoreSyncParameters = [
        'BASE_ROOT_DIR',
        'DB_PREFIX',
        'HOSTSPEC',
        'DATABASE_HOST',
        'DATABASE_PORT',
        'ENV_VARS_PREFIX',
        'DATABASE_USERNAME',
        'DATABASE_PASSWORD',
        'DATABASE_NAME',
        'DB_ENCODING',
        'DB_NAME_IN_UPPERCASE',
        'SYMFONY_ENV',
        'TIMEZONE',
        'COMMON_DB',
    ];

    // Modes de fonctionnement de Prado
    const APP_MODE_DEBUG = 'Debug';
    const APP_MODE_NORMAL = 'Normal';
    const APP_MODE_PERFORMANCE = 'Performance';

    private static array $parameters = [];

    /**
     * Ajoute un paramètre au fichier de configuration si celui-ci n'existe pas déjà.
     * Note : utilisable uniquement pour la génération du fichier de configuration !
     *
     * @param string $key
     * @param string $value
     */
    public static function setParameterIfNotExists($key, $value)
    {
        if (in_array($key, self::$ignoreSyncParameters)) {
            return;
        }
        if (!isset(self::$parameters[$key])) {
            self::setParameter($key, $value);
        }
    }

    /**
     * Ajoute ou modifie un paramètre au fichier de configuration
     * Note : utilisable uniquement pour la génération du fichier de configuration !
     *
     * @param string $key
     * @param string $value
     */
    public static function setParameter($key, $value)
    {
        if (in_array($key, self::$ignoreSyncParameters)) {
            return;
        }

        if (!is_string($key)) {
            throw new Atexo_Config_Exception('Key type not allowed.');
        }
        if (!is_string($value) && !is_numeric($value)) {
            throw new Atexo_Config_Exception('Value type not allowed.');
        }
        self::$parameters[$key] = $value;
    }

    /**
     * Retourne un paramètre du fichier de configuration.
     * Utilisable tout le temps.
     *
     * @param string $key
     *
     * @return string
     */
    public static function getParameter($key, $selectedOrg = null)
    {
        if (in_array($key, self::$ignoreSyncParameters)) {
            return getenv($key);
        }

        return self::$parameters[$key];
    }

    /**
     * Retourne la liste des paramètres du fichier de configuration.
     * Utilisable tout le temps.
     *
     * @return array
     */
    public static function getParameters()
    {
        return self::$parameters;
    }

    public static function setAppMode($mode)
    {
    }
}
