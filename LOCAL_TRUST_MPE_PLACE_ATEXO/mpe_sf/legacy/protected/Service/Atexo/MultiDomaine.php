<?php

namespace Application\Service\Atexo;

use Application\Propel\Mpe\CommonOrganisme;
use Application\Propel\Mpe\CommonOrganismeQuery;
use Exception;

class Atexo_MultiDomaine
{
    public static function getDomaine($domaineConfig, $option = [])
    {
        if (array_key_exists('multi_domaine', $option) && $option['multi_domaine'] == false) {
            return $domaineConfig;
        } else {
            $domaine = rtrim($domaineConfig, '/');
            if (array_key_exists('HTTP_HOST', $_SERVER) && $_SERVER['HTTP_HOST']) {
                $domaine = $_SERVER['REQUEST_SCHEME'] . '://' . $_SERVER['HTTP_HOST'];
            }

            if (array_key_exists('slash', $option) && $option['slash'] === true) {
                $domaine .= '/';
            }
            return $domaine;
        }
    }

    public static function replaceDomain($url, $multidomaine = true)
    {
        if ($multidomaine === true) {
            $option = [
                'slash' => true
            ];
             $url = str_replace(
                Atexo_Config::getParameter('PF_URL_REFERENCE'),
                Atexo_MultiDomaine::getDomaine(Atexo_Config::getParameter('PF_URL_REFERENCE'), $option),
                $url
            );
        }
        return $url;
    }

    public static function getPfUrl($org, $slash = false, $multidomaine = true)
    {
        $pfUrl =  Atexo_Config::getParameter('PF_URL_AGENT') != "/" ?
            Atexo_Config::getParameter('PF_URL_AGENT') :
            Atexo_Config::getParameter('PF_URL_REFERENCE');

        if ($multidomaine === true) {
            $pfUrl = Atexo_Config::getParameter('PF_URL_REFERENCE');
            try {
                $organismeQuery = new CommonOrganismeQuery(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
                $organisme = $organismeQuery->findOneByAcronyme($org);
                if ($organisme instanceof CommonOrganisme && null !== $organisme->getPfUrl()) {
                    $pfUrl = $organisme->getPfUrl();
                }
                $pfUrl = rtrim($pfUrl, '/');
                if (true === $slash) {
                    $pfUrl .= '/';
                }
            } catch (Exception $exception) {
                $logger = Atexo_LoggerManager::getLogger();
                $logger->error(
                    'Error lors de la récupération du pf_url. '.PHP_EOL.
                    ' Exception: '.$exception->getMessage().PHP_EOL.'Trace: '.$exception->getTraceAsString()
                );
            }
        }

        return $pfUrl;
    }
}
