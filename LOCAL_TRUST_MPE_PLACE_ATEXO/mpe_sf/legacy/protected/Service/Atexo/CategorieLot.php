<?php

namespace Application\Service\Atexo;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonCategorieLotPeer;

class Atexo_CategorieLot
{
    public function retrieveByOrganismeConsultationIdNumero($organisme, $consultationId, $numero)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonCategorieLotPeer::ORGANISME, $organisme, Criteria::EQUAL);
        $c->add(CommonCategorieLotPeer::CONSULTATION_ID, $consultationId, Criteria::EQUAL);
        $c->add(CommonCategorieLotPeer::LOT, $numero, Criteria::EQUAL);
        $categorieLotObject = CommonCategorieLotPeer::doSelectOne($c, $connexionCom);
        if ($categorieLotObject) {
            return $categorieLotObject;
        }

        return false;
    }
}
