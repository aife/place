<?php

namespace Application\Service\Atexo;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonTEtablissementPeer;
use Application\Propel\Mpe\CommonTEtablissementQuery;
use Application\Propel\Mpe\EntreprisePeer;

class Atexo_Etablissement
{
    public function getEntripseByIdEtablissement($idEtablement)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $commonTEtablissementQuery = new CommonTEtablissementQuery(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $etablissement = $commonTEtablissementQuery->findOneByIdEtablissement($idEtablement);

        return $etablissement;
    }

    public function retrieveEtablissementBySiret($siret)
    {
        $connexionCom = Propel::getConnection(
            Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY')
        );

        $c = new Criteria();
        $c->add(CommonTEtablissementPeer::CODE_ETABLISSEMENT, $siret);
        $company = EntreprisePeer::doSelectOne($c, $connexionCom);

        if ($company) {
            return $company;
        }

        return false;
    }
}
