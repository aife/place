<?php

namespace Application\Service\Atexo;

use Application\Service\Atexo\Controller\Atexo_Controller_Front;

/**
 * Classe de.
 *
 * @author Khalid BENAMAR <khalid.benamar@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_CodeCpv
{
    public function afficherBlocCpv($typeCPV, $principal, $secondaire, $min = 0, $max = 4, $langueCPV = 'fr', $css = '', $encoding = '', $inputCPV = '', $display = '')
    {
        $resCPV = '';
        $resCPV .= "<div id='bloc$inputCPV' name='bloc$inputCPV' style=\"display:$display\" >";
        $resCPV .= "<div class='champ-cpv' id='$typeCPV".$inputCPV."'></div>";

        //echo "<input type='hidden' id='demandeprincipal' name='demandeprincipal' value = '$demandeprincipal'/>";
        $resCPV .= "<input type='hidden' id='min' name='min' value = '$min'/>";
        $resCPV .= "<input type='hidden' id='max' name='max' value = '$max'/>";
        $resCPV .= "<input type='hidden' id='langueCPV' name='langueCPV' value = '$langueCPV'/>";
        $resCPV .= "<input type='hidden' id='typeCPV' name='inputCPV' value = '$typeCPV'/>";

        /*for ($i = 1; $i <= $max; $i++)
        {
            $resCPV .= "<input type='hidden' id='".$inputCPV."".$i."' name='".$inputCPV."".$i."' />";
            $resCPV .= "<input type='hidden' id='libelle_".$inputCPV."".$i."' name='libelle_".$inputCPV."".$i."' />";

        }*/
        $resCPV .= '</div>';
        $resCPV .= "<script type='text/javascript'> var principal = new Array(); var secondaires = new Array();";
        if (is_countable($principal) ? count($principal) : 0) {
            $resCPV .= 'principal[0] = "'.$principal[0].'";';
            $resCPV .= 'principal[1] = "'.$principal[1].'";';
        }
        if (is_countable($secondaire) ? count($secondaire) : 0) {
            for ($indexsecondaire = 0; $indexsecondaire < (is_countable($secondaire) ? count($secondaire) : 0); ++$indexsecondaire) {
                $resCPV .= "secondaires[ $indexsecondaire] = new Array();";
                $resCPV .= "secondaires[ $indexsecondaire][0] = '".$secondaire[$indexsecondaire][0]."';";
                $resCPV .= 'secondaires[ '.$indexsecondaire.'][1] = "'.$secondaire[$indexsecondaire][1].'";';
            }
        }
        $resCPV .= " remplirCPV('$inputCPV',principal,secondaires,'".Atexo_Controller_Front::t()."');";
        $resCPV .= ' </script>';

        return $resCPV;
    }
}
