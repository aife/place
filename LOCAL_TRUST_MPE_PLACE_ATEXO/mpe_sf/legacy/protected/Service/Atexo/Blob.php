<?php

namespace Application\Service\Atexo;
use Application\Service\Atexo\Atexo_Config;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonBlobFile;
use Application\Propel\Mpe\CommonBlobFilePeer;
use Application\Propel\Mpe\CommonBlobOrganismeFile;
use Application\Propel\Mpe\CommonBlobOrganismeFilePeer;
use Application\Service\Atexo\Blob\Atexo_Blob_DbManagement;
use Exception;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * Classe de gestion des fichier blob.
 *
 * @author Khalid BENAMAR <khalid.benamar@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Blob
{
    /*
     * $tab['hostspec'] => $hostspec, Atexo_Config::getParameter('HOSTSPEC')
     * $tab['username'] => $username, Atexo_Config::getParameter('DATABASE_USERNAME')
     * $tab['password'] => $password, Atexo_Config::getParameter('DATABASE_PASSWORD')
     * $tab['database'] => $FolderDestination
     */
    /*
     * @param $FolderDestination: determine la destination du fichier common si dossier common et acronyme org si un dossier org
     */
    public function insert_blob($file_name, $file_path, $FolderDestination, $subfolder = null, $extension = '')
    {
        //      return db_insert_blob($connObject->hostspec, $connObject->username, $connObject->password, $connObject->database, $file_name, $file_path);
        $dbManagement = new Atexo_Blob_DbManagement();

        $file = $dbManagement->db_insert_blob(
            Atexo_Config::getParameter('HOSTSPEC'),
            Atexo_Config::getParameter('DATABASE_USERNAME'),
            Atexo_Config::getParameter('DATABASE_PASSWORD'),
            $FolderDestination,
            $file_name,
            $file_path,
            $subfolder,
            $extension
        );

        return $file;
    }

    public function send_blob_to_client($blob_id, $FolderDestination)
    {
        self::sendBlobToClientWithoutLock($blob_id, $FolderDestination);
    }

    public function sendBlobToClientWithoutLock($blob_id, $FolderDestination)
    {
        $query_result_parsed = [];
        $chemin = null;
        $dbManagement = new Atexo_Blob_DbManagement();
        $db_host = Atexo_Config::getParameter('HOSTSPEC');
        $database = $FolderDestination;

        $connObject = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        if ($database == Atexo_Config::getParameter('COMMON_BLOB')) {
            $blob = CommonBlobFilePeer::retrieveByPK($blob_id, $connObject);
        } else {
            $blob = CommonBlobOrganismeFilePeer::retrieveByPK($blob_id, $connObject, $FolderDestination);
        }

//      $connObject->close(); methode close n'existe pas dans la classe PropelPDO.php
        unset($connObject);
        if ($blob instanceof CommonBlobOrganismeFile || $blob instanceof CommonBlobFile) {
            $query_result_parsed['id'] = $blob->getId();
            $query_result_parsed['revision'] = '0';
            $query_result_parsed['name'] = $blob->getName();
            $query_result_parsed['chemin'] = $blob->getChemin();
            $blob_id = $dbManagement->getBlobId($blob->getId(), $blob->getOldId());

            if (count($query_result_parsed)) {
                if (null !== $query_result_parsed['chemin']) {
                    $chemin = $query_result_parsed['chemin'];
                }
                $blob_resource = ['blob' => $query_result_parsed];
                $blob_resource['blob']['pointer'] = str_replace(
                    '//',
                    '/',
                    $dbManagement->db_get_blob_storage_file_path(
                        $db_host,
                        $database,
                        $blob_id,
                        '0',
                        $chemin,
                        false,
                        null,
                        $blob->getExtension()
                    )
                );
                $blob_resource['blob']['db_host'] = $db_host;
                $blob_resource['blob']['database'] = $database;
            }
        } else {
            throw new Exception('Mauvais paramètre');
        }
        if (false !== $blob_resource) {
            // header('Content-Length: ' . @filesize($blob_resource['blob']['pointer']));
            $fp = @fopen($blob_resource['blob']['pointer'], 'r');
            $block_count = floor(@filesize($blob_resource['blob']['pointer']) / Atexo_Config::getParameter('BLOB_BLOCK_SIZE'));
            $bytes_remaining = @filesize($blob_resource['blob']['pointer']) - ($block_count * Atexo_Config::getParameter('BLOB_BLOCK_SIZE'));
            for ($i = 0; $i < $block_count; ++$i) {
                echo @fread($fp, Atexo_Config::getParameter('BLOB_BLOCK_SIZE'));
                flush();
                ob_flush(); // pour les cas des gros fichiers (experimentation)
            }
            echo @fread($fp, $bytes_remaining);
            @fclose($fp);
        }
    }

    public function return_blob_to_client($blob_id, $FolderDestination, $withName = false, $subfolder = null)
    {
        $blob = [];
        $blob_resource = static::acquire_lock_on_blob($blob_id, $FolderDestination, $subfolder);
        $blob_content = '';
        if (false !== $blob_resource && false !== $blob_resource['blob']['pointer']) {
            $fp = @fopen($blob_resource['blob']['pointer'], 'r');
            $block_count = floor(@filesize($blob_resource['blob']['pointer']) / Atexo_Config::getParameter('BLOB_BLOCK_SIZE'));
            $bytes_remaining = @filesize($blob_resource['blob']['pointer']) - ($block_count * Atexo_Config::getParameter('BLOB_BLOCK_SIZE'));
            for ($i = 0; $i < $block_count; ++$i) {
                $blob_content .= @fread($fp, Atexo_Config::getParameter('BLOB_BLOCK_SIZE'));
            }
            if ($bytes_remaining > 0) {
                $blob_content .= @fread($fp, $bytes_remaining);
            }
            @fclose($fp);
            //$this->release_lock_on_blob($blob_resource);
            if ($withName) {
                $blob['name'] = $blob_resource['blob']['name'];
                $blob['content'] = $blob_content;
                $blob['revision'] = $blob_resource['blob']['revision'];

                return $blob;
            }

            return $blob_content;
        }
    }

    public static function acquire_lock_on_blob($blob_id, $FolderDestination, $subfolder = null)
    {
        $result = false;
        if ($blob_id) {
            $dbManagement = new Atexo_Blob_DbManagement();
            $result = $dbManagement->db_acquire_lock_on_blob(
                Atexo_Config::getParameter('HOSTSPEC'),
                Atexo_Config::getParameter('DATABASE_USERNAME'),
                Atexo_Config::getParameter('DATABASE_PASSWORD'),
                $FolderDestination,
                $blob_id,
                false,
                $subfolder
            );
        }

        return $result;
    }

    public function release_lock_on_blob($blob_resource)
    {
        $dbManagement = new Atexo_Blob_DbManagement();

        return $dbManagement->db_release_lock_on_blob($blob_resource);
    }

    /**
     * Retourne la taille d'un fichier en Ko sous format ($taille Ko).
     *
     * @param int    $blob_id    l'id du fichier
     * @param object $connObject la connexion
     *
     * @return string taille du fichier
     */
    public function getTailFile($blob_id, $FolderDestination)
    {
        try {
            $dbManagement = new Atexo_Blob_DbManagement();
            $retVal = false;
            $chemin = null;
            $extension = '';
            $connObject = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').
                Atexo_Config::getParameter('CONST_READ_ONLY'));
            if ($FolderDestination == Atexo_Config::getParameter('COMMON_BLOB')) {
                $blob = CommonBlobFilePeer::retrieveByPK($blob_id, $connObject);
            } else {
                $blob = CommonBlobOrganismeFilePeer::retrieveByPK($blob_id,  $connObject, $FolderDestination);
            }

            if ($blob instanceof CommonBlobFile || $blob instanceof CommonBlobOrganismeFile) {
                if (null !== $blob->getChemin()) {
                    $chemin = $blob->getChemin();
                }
                $extension = $blob->getExtension();

                $dbManagement = new Atexo_Blob_DbManagement();
                $blob_id = $dbManagement->getBlobId($blob->getId(), $blob->getOldId());
                $blobPath = $dbManagement->db_get_blob_storage_file_path(Atexo_Config::getParameter('HOSTSPEC'),
                    $FolderDestination,
                    $blob_id,
                    '0',
                    $chemin,
                    false,
                    null,
                    $extension
                );
                if (is_file($blobPath)) {
                    $retVal = @filesize($blobPath);
                }
            }

        } catch (Exception $exception) {
            Prado::log("BLOB ID n'existe pas " . $exception->getMessage() . " " . $exception->getTraceAsString(), TLogger::ERROR);
        }

        return $retVal;
    }

    /**
     * copie le contenu d'un blob.
     *
     * @param $blobId
     * @param $dest
     * @param $connObject
     *
     * @return 0 si OK
     */
    public static function copyBlob($blobId, $dest, $FolderDestination)
    {
        $retVal = null;
        $blob_resource = self::acquire_lock_on_blob($blobId, $FolderDestination);
        if (false !== $blob_resource && $blob_resource['blob']['pointer'] !== false) {
            if (!is_file($blob_resource['blob']['pointer'])
                && Atexo_Config::getParameter('USE_FAKE_FILESYSTEM')
                && in_array(Atexo_Config::getParameter('PF_ENV'), ['dev', 'rec'])) {
                file_put_contents($blob_resource['blob']['pointer'], 'Lorum Ipsum');
            }
            system('cp '.$blob_resource['blob']['pointer'].' '.escapeshellarg($dest), $retVal);
            //self::release_lock_on_blob($blob_resource);
        }

        return $retVal;
    }

    public function deleteBlobFile($blob_id, $FolderDestination)
    {
        $dbManagement = new Atexo_Blob_DbManagement();

        return $dbManagement->db_delete_blob(
            Atexo_Config::getParameter('HOSTSPEC'),
            Atexo_Config::getParameter('DATABASE_USERNAME'),
            Atexo_Config::getParameter('DATABASE_PASSWORD'),
            $FolderDestination,
            $blob_id
        );
    }

    public function sendBlobToClient($blob_id, $fileName, $FolderDestination)
    {
        $blob_resource = static::acquire_lock_on_blob($blob_id, $FolderDestination);
        if (false !== $blob_resource) {
            header('Content-Length: '.@filesize($blob_resource['blob']['pointer']));
            header('Content-Disposition: attachment; filename="'.str_replace(';', ';', (stripslashes($fileName))).'"');
            $fp = fopen($blob_resource['blob']['pointer'], 'rb');
            while (!feof($fp) && 0 == connection_status()) {
                set_time_limit(0);
                echo fread($fp, 8192);
                flush();
                ob_flush();
            }
            fclose($fp);
            $this->release_lock_on_blob($blob_resource);
        }
    }

    /**
     * duplique le contenu d'un blob et retourne l'id du duplicata.
     *
     * @param $blobId
     * @param $connObject
     *
     * @return idBlob du duplicata
     */
    public function duplicateBlob($blobId, $FolderDestination, $extension = '')
    {
        $blob_resource = static::acquire_lock_on_blob($blobId, $FolderDestination);

        if (false !== $blob_resource) {
            // réalisation d'une copie temporaire car insert_blob fait un mv du fichier passé en entrée
            $tmpFileName = Atexo_Config::getParameter('COMMON_TMP').'blobToDuplicate'.session_id().time();
            $cpResult = static::copyBlob($blobId, $tmpFileName, $FolderDestination);
            if (0 == $cpResult) {
                return $this->insert_blob($blob_resource['blob']['name'], $tmpFileName, $FolderDestination, $extension);
            }

            return false;
        }
    }

    public function deleteBlobFromBaseDirectory($blob_id, $FolderDestination)
    {
        $query_result_parsed = [];
        $chemin = null;
        $dbManagement = new Atexo_Blob_DbManagement();
        $db_host = Atexo_Config::getParameter('HOSTSPEC');
        $database = $FolderDestination;
        $database = $dbManagement->removePrefix($database);

        $connObject = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $blob = CommonBlobOrganismeFilePeer::retrieveByPK($blob_id,  $connObject, $FolderDestination);
        //$connObject->close();

        if ($blob instanceof CommonBlobOrganismeFile || $blob instanceof CommonBlobFile) {
            $query_result_parsed['id'] = $blob->getId();
            $query_result_parsed['revision'] = '0';
            $query_result_parsed['name'] = $blob->getName();
            $query_result_parsed['chemin'] = $blob->getChemin();
            $query_result_parsed['extension'] = $blob->getExtension();
            $blob_id = $dbManagement->getBlobId($blob->getId(), $blob->getOldId());
            if (count($query_result_parsed)) {
                if (null !== $query_result_parsed['chemin']) {
                    $chemin = $query_result_parsed['chemin'];
                }
                $blob_resource = ['blob' => $query_result_parsed];
                $blob_resource['blob']['pointer'] = $dbManagement->db_get_blob_storage_file_path(
                    $db_host,
                    $database,
                    $blob_id,
                    '0',
                    $chemin,
                    false,
                    null,
                    $blob->getExtension()
                );
                $blob_resource['blob']['db_host'] = $db_host;
                $blob_resource['blob']['database'] = $database;
            }
        } else {
            throw new Exception('Mauvais paramètre');
        }
        if (is_file($blob_resource['blob']['pointer'])) {
            if (unlink($blob_resource['blob']['pointer'])) {
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * Permet de retourner le blob sous format des petits bloc lors d'utilisation du WS.
     *
     * @param $idBlob l'id blob
     * @param $folderDestination le dossier de destination
     * @param $mime_boundary
     * @param $file
     * @param $nameFile le nom du fichier
     * @param $contentType le type de contenu
     * @param $encoding l'encoding
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function getBlocsBlobForWs($idBlob, $folderDestination, $mime_boundary, $file, $nameFile, $contentType, $encoding)
    {
        $blob_resource = $this->acquire_lock_on_blob($idBlob, $folderDestination);
        if ($blob_resource !== false && $blob_resource['blob']['pointer']!== false) {
            $fp = @fopen ($blob_resource['blob']['pointer'], 'r');
            if(!$file){
                $blobId = $blob_resource['blob']['old_id'] ?? $blob_resource['blob']['id']?? '';
                $blogRev = $blob_resource['blob']['revision'] ?? '';
                $file = ($blobId."-".$blogRev);
            }
            echo "\r\n--".$mime_boundary."\r\nContent-Disposition: form-data; name=\"".$file.'"; filename="'.$file."\"\r\nContent-Type:  ".$contentType."\r\nContent-Transfer-Encoding: ".$encoding."\r\n\r\n";
            while (!feof($fp)) {
                $blob_content = @fread($fp, Atexo_Config::getParameter('BLOB_BLOCK_SIZE'));
                echo $blob_content;
                flush();
                ob_flush();
            }
            @fclose($fp);
        }
    }

    /**
     * retourne les infos d'un blob par son id.
     *
     * @param int    $idBlob id du blob
     * @param string $org    organisme, dossier du blob
     *
     * @return string $blobContent contenu du blob
     *
     * @author ASO <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public static function getBlobById($idBlob, $org, $fileName = null, $withContent = true)
    {
        $blob = [];
        $atexoBlob = new Atexo_Blob();
        if ($withContent) {
            $blob = $atexoBlob->return_blob_to_client($idBlob, $org, true);
        }
        $blob['idBlob'] = $idBlob;
        $blob['organisme'] = $org;
        $blob['sizeBlob'] = $atexoBlob->getTailFile($idBlob, $org);
        if ($fileName) {
            $blob['file'] = $fileName;
        }

        return $blob;
    }

    /**
     * Retourne la path d'un fichier.
     *
     * @param int    $blob_id    l'id du fichier
     * @param object $connObject la connexion
     *
     * @return string path du fichier
     */
    public static function getPathFile($blob_id, $FolderDestination)
    {
        $dbManagement = new Atexo_Blob_DbManagement();
        $chemin = null;
        $extension = '';
        $connObject = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $blob = CommonBlobOrganismeFilePeer::retrieveByPK($blob_id, $connObject, $FolderDestination);

        if ($blob instanceof CommonBlobOrganismeFile) {
            if (null !== $blob->getChemin()) {
                $chemin = $blob->getChemin();
            }
            $extension = $blob->getExtension();
            $blob_id = $dbManagement->getBlobId($blob->getId(), $blob->getOldId());
        }

        $blobPath = $dbManagement->db_get_blob_storage_file_path(
            Atexo_Config::getParameter('HOSTSPEC'),
            $FolderDestination,
            $blob_id,
            '0',
            $chemin,
            false,
            null,
            $extension
        );

        if (is_file($blobPath)) {
            $retPath = $blobPath;
        } else {
            $retPath = false;
        }

        return $retPath;
    }

    public function changeExtensionBlob($idBlob, $folderDestination, $extension, $subFolder = null)
    {
        try {
            $dbManagement = new Atexo_Blob_DbManagement();

            $dbManagement->db_mv_extension_blob(
                Atexo_Config::getParameter('HOSTSPEC'),
                Atexo_Config::getParameter('DATABASE_USERNAME'),
                Atexo_Config::getParameter('DATABASE_PASSWORD'),
                $folderDestination,
                $idBlob,
                $subFolder,
                $extension
            );
        } catch (Exception $e) {
            Prado::log('Atexo_Blob fct changeExtensionBlob Error '.$e->getMessage().' '.$e->getTraceAsString(), TLogger::ERROR);
        }
    }
}
