<?php

namespace Application\Service\Atexo;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonAcheteurPublicPeer;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_PublicPurchaser
{
    /**
     * Fonction qui retourne la liste de touts les acheteurs publiques liés au service passé en paramètre et ses fils.
     *
     * @param : $idService l'id du service
     *
     * @return : un tableau d'objet Acheteurpublic ou un tableau vide si aucun élement trouvé
     */
    public function retrievePublicPurchasers($idService, $org = false, $allServices = true)
    {
        if (!$org) {
            $org = Atexo_CurrentUser::getCurrentOrganism();
        }
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        if ($allServices) {
            $arrayChilds = Atexo_EntityPurchase::getSubServices($idService, $org, true);
            if ($arrayChilds) {
                $c->add(CommonAcheteurPublicPeer::SERVICE_ID, $arrayChilds, Criteria::IN);
            }
        } else {
            $c->add(CommonAcheteurPublicPeer::SERVICE_ID, $idService);
        }
        $c->add(CommonAcheteurPublicPeer::ORGANISME, $org);
        $c->addAscendingOrderByColumn(CommonAcheteurPublicPeer::BOAMP_LOGIN);
        $publicPurchasers = CommonAcheteurPublicPeer::doSelect($c, $connexionCom);
        if ($publicPurchasers) {
            return $publicPurchasers;
        } else {
            return [];
        }
    }

    /**
     * Fonction qui retourne l'objet AcheteurPublic dont l'id est passé en paramètre.
     *
     * @param : $idPublicPurchasers l'id de l'acheteur public
     *
     * @return : un objet Acheteurpublic ou un objet Acheteurpublic vide si aucun élement trouvé
     */
    public function retrievePublicPurchasersById($idPublicPurchasers)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonAcheteurPublicPeer::ID, $idPublicPurchasers, Criteria::EQUAL);
        $c->add(CommonAcheteurPublicPeer::ORGANISME, Atexo_CurrentUser::getCurrentOrganism(), Criteria::EQUAL);
        $publicPurchasers = CommonAcheteurPublicPeer::doSelectOne($c, $connexionCom);
        if ($publicPurchasers) {
            return $publicPurchasers;
        } else {
            return false;
        }
    }

    /**
     * Fonction qui supprime un enregistrement acheteur public de la base de donnée.
     *
     * @param : $idPublicPurchaser : id de l'acheteur publique à supprimer
     */
    public function delete($idPublicPurchaser)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonAcheteurPublicPeer::ID, $idPublicPurchaser);
        $c->add(CommonAcheteurPublicPeer::ORGANISME, Atexo_CurrentUser::getCurrentOrganism());

        return CommonAcheteurPublicPeer::doDelete($c, $connexionCom);
    }

    /**
     * $publicPurchasers Acheteurpublic.
     */
    public function retrieveAllPublicPurchasers()
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonAcheteurPublicPeer::ORGANISME, Atexo_CurrentUser::getCurrentOrganism());
        $publicPurchasers = CommonAcheteurPublicPeer::doSelect($c, $connexionCom);
        if ($publicPurchasers) {
            return $publicPurchasers;
        } else {
            return [];
        }
    }

    /**
     * retourne les compte boamp du service d'attachement et le service associé à la consultation
     * $publicPurchasers Acheteurpublic.
     */
    public function retrievePublicPurchasersCons($idServiceRattach, $idServiceAssocie)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonAcheteurPublicPeer::SERVICE_ID, $idServiceRattach);
        $c->add(CommonAcheteurPublicPeer::ORGANISME, Atexo_CurrentUser::getCurrentOrganism());
        $c->addOr(CommonAcheteurPublicPeer::SERVICE_ID, $idServiceAssocie);
        $publicPurchasers = CommonAcheteurPublicPeer::doSelect($c, $connexionCom);
        if ($publicPurchasers) {
            return $publicPurchasers;
        } else {
            return [];
        }
    }
}
