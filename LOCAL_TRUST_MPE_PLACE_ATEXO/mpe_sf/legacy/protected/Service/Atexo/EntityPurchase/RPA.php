<?php

namespace Application\Service\Atexo\EntityPurchase;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonRPAPeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Organismes;

/**
 * Classe de manipulation des représentants du pouvoir adjudicateur.
 *
 * @author SBAI Yassine <yassine.sbai@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_EntityPurchase_RPA
{
    /**
     * retourne la liste des RPAs mises en cache.
     *
     * @param string $organisme
     *
     * @return array
     */
    public static function getRpaByIdService($IdService, $organisme)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonRPAPeer::SERVICE_ID, $IdService, CRITERIA::EQUAL);
        $c->add(CommonRPAPeer::ACRONYMEORG, $organisme);
        $RPAs = CommonRPAPeer::doSelect($c, $connexionCom);

        return $RPAs;
    }

    /**
     * retourne l'objet rpa à partir de l'id passé en paramètre.
     *
     * @param  $id id du rpa recherché
     * @param $org Organismes
     *
     * @return $rpa
     */
    public static function retrieveRpaById($idrpa, $org)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $RPA = CommonRPAPeer::retrieveByPk($idrpa, $org, $connexionCom);

        if ($RPA) {
            return $RPA;
        } else {
            return false;
        }
    }

    /**
     * supprime l'objet rpa à partir de l'id passé en paramètre.
     *
     * @param  $id id du rpa à supprimer
     * @param $org Organismes
     *
     * @return $rpa
     */
    public static function DeleteRpa($idrpa, $org)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));

        $RPA = CommonRPAPeer::retrieveByPk($idrpa, $org, $connexionCom);
        $IdService = $RPA->getServiceId();
        if (is_object($RPA)) {
            $RPA->delete($connexionCom);
        }

        return $IdService;
    }

    /**
     * retourne la liste des rpas mises en cache.
     *
     * @param string $organisme
     *
     * @return array
     */
    public static function getCachedRpas($IdService, $organisme)
    {
        if (!Atexo_CurrentUser::readFromSession('cachedRpas')) {
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            $c = new Criteria();
            $c->add(CommonRPAPeer::SERVICE_ID, $IdService, CRITERIA::EQUAL);
            $c->add(CommonRPAPeer::ACRONYMEORG, $organisme, CRITERIA::EQUAL);
            $Rpas = CommonRPAPeer::doSelect($c, $connexionCom);
            $allRpas = [];
            foreach ($Rpas as $oneRpa) {
                $allRpas[] = $oneRpa;
            }
            Atexo_CurrentUser::writeToSession('cachedRpas', $allRpas);
        }

        return Atexo_CurrentUser::readFromSession('cachedRpas');
    }

    /**
     * la remises en cache de la nouvelle liste des rpas de la BD.
     *
     * @param string $organisme
     *
     * @return array
     */
    public static function setCachedRpas($IdService, $organisme)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonRPAPeer::SERVICE_ID, $IdService, CRITERIA::EQUAL);
        $c->add(CommonRPAPeer::ACRONYMEORG, $organisme, CRITERIA::EQUAL);
        $Rpas = CommonRPAPeer::doSelect($c, $connexionCom);
        $allRpas = [];
        foreach ($Rpas as $oneRpa) {
            if ($oneRpa->getAdresse2()) {
                $oneRpa->setCoordonne($oneRpa->getAdresse1().'<br>'.$oneRpa->getAdresse2().'<br>'.$oneRpa->getCodepostal().'-'.$oneRpa->getVille().'-'.$oneRpa->getPays());
            } else {
                $oneRpa->setCoordonne($oneRpa->getAdresse1().'<br>'.$oneRpa->getCodepostal().'-'.$oneRpa->getVille().'-'.$oneRpa->getPays());
            }
            $allRpas[] = $oneRpa;
        }
        Atexo_CurrentUser::writeToSession('cachedRpas', $allRpas);
    }

    public static function retriveRpasByOrganismeAndDateModif($org, $dateModif = null)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        if ($dateModif) {
            $c->add(CommonRPAPeer::DATE_MODIFICATION, $dateModif, Criteria::GREATER_EQUAL);
        }
        $c->add(CommonRPAPeer::ACRONYMEORG, $org, CRITERIA::EQUAL);
        $Rpas = CommonRPAPeer::doSelect($c, $connexionCom);
        if ($Rpas) {
            return $Rpas;
        }

        return null;
    }

    public function getAllRpaslateForme($fromDate = null)
    {
        $arrayGlobal = [];
        $allOrganisme = Atexo_Organismes::retrieveOrganismes();
        foreach ($allOrganisme as $OneOrganisme) {
            $rpas = null;
            $rpas = self::retriveRpasByOrganismeAndDateModif($OneOrganisme->getAcronyme(), $fromDate);
            if (is_array($rpas)) {
                $arrayGlobal = array_merge($arrayGlobal, $rpas);
            }
        }
        //print_r($arrayGlobal);exit;
        return $arrayGlobal;
    }

    public function getResponsabeArchiveByIdService($IdService, $organisme)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonRPAPeer::SERVICE_ID, $IdService, CRITERIA::EQUAL);
        $c->add(CommonRPAPeer::RESPONSABLE_ARCHIVE, '1', CRITERIA::EQUAL);
        $c->add(CommonRPAPeer::ACRONYMEORG, $organisme, CRITERIA::EQUAL);
        $RPAs = CommonRPAPeer::doSelectOne($c, $connexionCom);

        return $RPAs;
    }
}
