<?php

namespace Application\Service\Atexo\EntityPurchase;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonMandataireServicePeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;

/**
 * Classe de gestion des mandataires.
 *
 * @author Maatalla Houriya <houriya.maatalla@atexo.com>
 * @copyright Atexo 2011
 *
 * @version 1.0
 *
 * @since MPE-4.0
 */
class Atexo_EntityPurchase_Mandataire
{
    /**
     * Fonction qui retourne la liste de touts les mandataires liés au service passé en paramètre.
     *
     * @param : $idService l'id du service
     *
     * @return : un tableau d'objet Mandataires ou un tableau vide si aucun élement trouvé
     */
    public function retrieveMandataire($idService, $org = false)
    {
        if (!$org) {
            $org = Atexo_CurrentUser::getCurrentOrganism();
        }
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonMandataireServicePeer::SERVICE_ID, $idService);
        $c->add(CommonMandataireServicePeer::ORGANISME, $org);
        $mandataire = CommonMandataireServicePeer::doSelect($c, $connexionCom);
        if ($mandataire) {
            return $mandataire;
        } else {
            return [];
        }
    }

    /**
     * Fonction qui supprime un mandataire de la base de donnée.
     *
     * @param : $id : id du mandataire à supprimer
     */
    public function deleteMandataire($id)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonMandataireServicePeer::ID, $id);
        $c->add(CommonMandataireServicePeer::ORGANISME, Atexo_CurrentUser::getCurrentOrganism());

        return CommonMandataireServicePeer::doDelete($c, $connexionCom);
    }

    /**
     * Fonction qui retourne le chemin des oganismes/services associés au mandataire connecté.
     *
     * @param : $idService l'id du service
     *
     * @return : un tableau d'objet Mandataires ou un tableau vide si aucun élement trouvé
     */
    public function getInfosMandataire($mandataire)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonMandataireServicePeer::MANDATAIRE, $mandataire, Criteria::EQUAL);
        $mandataires = CommonMandataireServicePeer::doSelect($c, $connexionCom);
        $arrayServiceMandataire = [];
        if ($mandataires && is_array($mandataires)) {
            foreach ($mandataires as $oneMandataire) {
                $arrayServiceMandataire[$oneMandataire->getServiceId()] = Atexo_EntityPurchase::getPathEntityById($oneMandataire->getServiceId(), $oneMandataire->getOrganisme());
            }
        }

        return $arrayServiceMandataire;
    }
}
