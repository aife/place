<?php

namespace Application\Service\Atexo\Geolocalisation;

use Application\Propel\Mpe\CommonGeolocalisationN1Query;

/**
 * Classe de manipulation des geolocalisation de niveau N1.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Geolocalisation_GeolocalisationN1
{
    /**
     * retourne les geolocalisations N1.
     */
    public function retrieveGeoN1($geoN0)
    {
        $geocalisationN1Query = new CommonGeolocalisationN1Query();

        return $geocalisationN1Query->retrieveGeoN1ByIdGeoN0($geoN0->getId());
    }

    /**
     * retourne les geolocalisations N1 identifié par id GEO N0.
     */
    public function retrieveGeoN1ByIdGeoN0($idGeoN0)
    {
        $geocalisationN1Query = new CommonGeolocalisationN1Query();

        return $geocalisationN1Query->retrieveGeoN1ByIdGeoN0($idGeoN0);
    }

    public static function retrieveDenomination1ByIdGeoN1($idGeoN1)
    {
        $geocalisationN1Query = new CommonGeolocalisationN1Query();
        $geoN1 = $geocalisationN1Query->retrieveGeolocalisationN1ById($idGeoN1);
        if ($geoN1) {
            return $geoN1[0]->getDenomination1();
        }

        return '';
    }
}
