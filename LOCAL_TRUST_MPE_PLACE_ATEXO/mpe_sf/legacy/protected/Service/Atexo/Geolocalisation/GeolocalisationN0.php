<?php

namespace Application\Service\Atexo\Geolocalisation;

use Application\Propel\Mpe\CommonGeolocalisationN0Query;

/**
 * Classe de manipulation des geolocalisation de niveau N0.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Geolocalisation_GeolocalisationN0
{
    /**
     * retourne tous les objets GeolocalisationN0.
     */
    public function retrieveAllGeoN0()
    {
        $geocalisationN0Query = new CommonGeolocalisationN0Query();

        return $geocalisationN0Query->retrieveAllGeoN0();
    }

    /**
     * retourne les objets GeolocalisationN0 actifs.
     *
     * @return array $geoN0 liste des objets CommonGeolocalisation
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function retrieveActifGeoN0()
    {
        $geocalisationN0Query = new CommonGeolocalisationN0Query();

        return $geocalisationN0Query->retrieveActifGeoN0();
    }
}
