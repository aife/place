<?php

namespace Application\Service\Atexo\Geolocalisation;

use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonGeolocalisationN2;
use Application\Propel\Mpe\CommonGeolocalisationN2Peer;
use Application\Propel\Mpe\CommonGeolocalisationN2Query;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;

/**
 * Classe de manipulation des geolocalisation de niveau N2.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Geolocalisation_GeolocalisationN2
{
    /**
     * retourne la liste des Geolocalisation N2 de type 0.
     */
    public function retrieveListGeolocalisation($geoN1)
    {
        if (!$geoN1 || null == $geoN1->getId()) {
            return '';
        } else {
            $geocalisationN2Query = new CommonGeolocalisationN2Query();

            return $geocalisationN2Query->retrieveListGeolocalisationN2byGeoN1Id($geoN1->getId());
        }
    }

    public static function retrieveGeolocalisationN2byIds($arrayIds)
    {
        $geocalisationN2Query = new CommonGeolocalisationN2Query();

        return $geocalisationN2Query->retrieveGeolocalisationN2byIds($arrayIds);
    }

    /**
     * retourne une liste d'objets geolocalisation N2
     * $param $id identifiant de l'objet geo N2.
     */
    public function retrieveGeolocalisationN2ById($id)
    {
        $arrayIds = [];
        if (!$id) {
            return false;
        }

        $arrayIds[] = $id;
        $geocalisationN2Query = new CommonGeolocalisationN2Query();

        return $geocalisationN2Query->retrieveGeolocalisationN2byIds($arrayIds);
    }

    /**
     * retourne la liste des geolocalisation de niveau N2 de type 1.
     */
    public function retrieveListGeolocalisationType1($geoN0, $exclureDesPays = true)
    {
        $geocalisationN2Query = new CommonGeolocalisationN2Query();

        return $geocalisationN2Query->retrieveListGeolocalisationType1($geoN0, $exclureDesPays);
    }

    /**
     * retourne la liste des Pays.
     */

    public static function retrieveListCountries($withaoutFrance = true, $whiteDomTom = false)
    {
        $geocalisationN2Query = new CommonGeolocalisationN2Query();
        return $geocalisationN2Query->retrieveListCountries($withaoutFrance, $whiteDomTom);
    }

    /**
     * retourne l'id du pays.
     */
    public function retrieveIdCountry($country)
    {
        $c = new Criteria();
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonGeolocalisationN2Peer::ID_GEOLOCALISATIONN1, 234);
        $c->add(CommonGeolocalisationN2Peer::DENOMINATION1, (string) $country, Criteria::EQUAL);
        $oneCountry = CommonGeolocalisationN2Peer::doSelectOne($c, $connexionCom);
        if ($oneCountry) {
            return $oneCountry->getId();
        }
    }

    /**
     * retourne l'id du pays.
     */
    public function retrieveCountryById($IdCountry)
    {
        $c = new Criteria();
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonGeolocalisationN2Peer::ID_GEOLOCALISATIONN1, 234);
        $c->add(CommonGeolocalisationN2Peer::ID, $IdCountry, Criteria::EQUAL);
        $oneCountry = CommonGeolocalisationN2Peer::doSelectOne($c, $connexionCom);
        if ($oneCountry) {
            return $oneCountry->getDenomination1();
        }
    }

    public function retrieveArrayDepartements()
    {
        $c = new Criteria();
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonGeolocalisationN2Peer::ID_GEOLOCALISATIONN1, 234, Criteria::NOT_EQUAL);
        $c->addAscendingOrderByColumn(CommonGeolocalisationN2Peer::DENOMINATION1);
        $departements = CommonGeolocalisationN2Peer::doSelect($c, $connexionCom);
        if ($departements) {
            $listeArray = [];
            foreach ($departements as $unDepartement) {
                $listeArray[$unDepartement->getId()] = $unDepartement->getDenomination1();
            }

            return $listeArray;
        } else {
            return [];
        }
    }

    public function retrieveArrayCountries()
    {
        $countries = self::retrieveListCountries(false);
        if (is_array($countries)) {
            $listeDesPays = [];
            foreach ($countries as $country) {
                if ($country instanceof CommonGeolocalisationN2) {
                    $listeDesPays[$country->getId()] = $country->getDenomination1();
                }
            }

            return $listeDesPays;
        }
    }

    public function retrieveDenomination2ByIdGeoN2($idGeoN2)
    {
        $geoN2 = self::retrieveGeolocalisationN2ById($idGeoN2);
        if ($geoN2[0] instanceof CommonGeolocalisationN2) {
            return $geoN2[0]->getDenomination2();
        } else {
            return '';
        }
    }

    /**
     * Fonction qui retourne l'id de la denomination passé en paramètre
     * Si cete fonction trouve deux Ids ou plus pour la même dénomination elle retournera false.
     */
    public function retrieveIdsByDenomination1($denomination)
    {
        $c = new Criteria();
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonGeolocalisationN2Peer::DENOMINATION1, $denomination);
        $pays = CommonGeolocalisationN2Peer::doSelect($c, $connexionCom);
        if (is_array($pays) && 1 == count($pays)) {
            $pays = array_shift($pays);
            if ($pays instanceof CommonGeolocalisationN2) {
                return $pays->getId();
            }
        }

        return false;
    }

    /**
     * retourne l'accronyme du pays en fonction de son id.
     *
     * @param ineteger $IdCountry
     *
     * @return string l'abbréviation du pays
     */
    public function retrieveAccronymeCountryById($IdCountry)
    {
        $c = new Criteria();
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonGeolocalisationN2Peer::ID_GEOLOCALISATIONN1, 234);
        $c->add(CommonGeolocalisationN2Peer::ID, $IdCountry, Criteria::EQUAL);
        $oneCountry = CommonGeolocalisationN2Peer::doSelectOne($c, $connexionCom);
        if ($oneCountry) {
            return $oneCountry->getDenomination2();
        }
    }

    /**
     * retourne l'accronyme du pays en fonction de son id.
     *
     * @param ineteger $IdCountry
     *
     * @return string l'abbréviation du pays
     */
    public function retrieveAccronymeCountryByDenomination($NameCountry)
    {
        $c = new Criteria();
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonGeolocalisationN2Peer::ID_GEOLOCALISATIONN1, 234);
        $c->add(CommonGeolocalisationN2Peer::DENOMINATION1, $NameCountry, Criteria::EQUAL);
        $oneCountry = CommonGeolocalisationN2Peer::doSelectOne($c, $connexionCom);
        if ($oneCountry) {
            return $oneCountry->getDenomination2();
        }
    }

    public function retrieveDenomination1ByIdGeoN3($idGeoN2)
    {
        $geoN2 = self::retrieveGeolocalisationN2ById($idGeoN2);
        if ($geoN2) {
            return $geoN2[0]->getDenomination1();
        }

        return '';
    }

    // permet de recuper id du departement à partir du code postal
    public function retrieveAccronymeCountryLikeCp($cp)
    {
        $c = new Criteria();
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
//       $c->add(CommonGeolocalisationN2Peer::DENOMINATION1, $NameCountry ,Criteria::EQUAL);
        $c->add(CommonGeolocalisationN2Peer::DENOMINATION1, '%'.$cp.'%', Criteria::LIKE);
        $oneCountry = CommonGeolocalisationN2Peer::doSelectOne($c, $connexionCom);

        if ($oneCountry) {
            return $oneCountry->getId();
        } else {
            return '0';
        }
    }

    // permet de recuper geoN2 à partir de la denomination 2
    public function retrieveGeoN2ByDenomination2($denomintation)
    {
        if (!$denomintation) {
            return '';
        }
        $c = new Criteria();
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonGeolocalisationN2Peer::DENOMINATION2, $denomintation, Criteria::LIKE);
        $geoN2 = CommonGeolocalisationN2Peer::doSelectOne($c, $connexionCom);
        if ($geoN2) {
            return $geoN2;
        } else {
            return [];
        }
    }

    /**
     * Permet de retourner la geolocalisation By code interface.
     *
     * @param $code
     *
     * @return CommonGeolocalisationN2
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function retrieveGeoN2ByCodeInterface($code)
    {
        $geocalisationN2Query = new CommonGeolocalisationN2Query();

        return $geocalisationN2Query->getGeolocalisationByCodeInterface($code);
    }

    /**
     * permet de recuper geoN2 à partir de la denomination 2.
     */
    public function retrieveGeoN2ByArrayDenomination2($denomintation)
    {
        if ('' == $denomintation[0]) {
            return [];
        }
        $c = new Criteria();
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonGeolocalisationN2Peer::DENOMINATION2, $denomintation, Criteria::IN);
        $geoN2 = CommonGeolocalisationN2Peer::doSelect($c, $connexionCom);
        if ($geoN2) {
            return $geoN2;
        } else {
            return [];
        }
    }

    /**
     * retourne la liste des Geolocalisation N2 de type 0
     * identifié par id Geo N 1.
     */
    public function retrieveListGeolocalisationN2($idGeoN1)
    {
        if (!$idGeoN1) {
            return '';
        }
        $c = new Criteria();
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonGeolocalisationN2Peer::ID_GEOLOCALISATIONN1, $idGeoN1);
        $listRes = CommonGeolocalisationN2Peer::doSelect($c, $connexionCom);
        if ($listRes) {
            return $listRes;
        }
    }

    /**
     * retourne des lieux d'execution.
     */
    public static function getLibelleLieuExecution($arrayIds, $langue = null)
    {
        $listGeoN2 = Atexo_Geolocalisation_GeolocalisationN2::retrieveGeolocalisationN2byIds($arrayIds);
        if ($listGeoN2) {
            $geoN2InString = '';
            $getDenomination1Traduit = 'getDenomination1'.Atexo_Languages::getLanguageAbbreviation($langue);
            foreach ($listGeoN2 as $oneGeoN2) {
                if (!$langue) {
                    $geoN2InString = $geoN2InString.$oneGeoN2->getDenomination1().', ';
                } else {
                    if (0 == strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE'))
                     || !$oneGeoN2->$getDenomination1Traduit()) {
                        $geoN2InString = $geoN2InString.$oneGeoN2->getDenomination1().', ';
                    } else {
                        $geoN2InString = $geoN2InString.$oneGeoN2->$getDenomination1Traduit().', ';
                    }
                }
            }

            return substr(($geoN2InString), 0, -1);
        } else {
            return '-';
        }
    }

    /**
     * Permet de recuperer le code du pays a partir de son libelle.
     *
     * @param string $libelleCountry : le libelle du pays
     *
     * @return string : le code du pays
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function retrieveCodeCountryByLibelle($libelleCountry)
    {
        $c = new Criteria();
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonGeolocalisationN2Peer::ID_GEOLOCALISATIONN1, 234);
        $c->add(CommonGeolocalisationN2Peer::DENOMINATION1, (string) $libelleCountry, Criteria::EQUAL);
        $country = CommonGeolocalisationN2Peer::doSelectOne($c, $connexionCom);
        $codeCountry = '';
        if ($country instanceof CommonGeolocalisationN2) {
            $codeCountry = $country->getDenomination2();
        }

        return $codeCountry;
    }

    /**
     * Permet de recuperer denomination2 d'une liste de geolocalisation n2.
     *
     * @param array $idsGeoN2 ids GeoN2
     *
     * @return string|array : denomination2
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1.0
     *
     * @since 2017-place
     *
     * @copyright Atexo 2017
     */
    public static function retrieveDenomination2ByIdsGeoN2($idsGeoN2, $separateur = null): string|array
    {
        $c = new Criteria();
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonGeolocalisationN2Peer::ID, $idsGeoN2, Criteria::IN);
        $geoN2s = CommonGeolocalisationN2Peer::doSelect($c, $connexionCom);
        $return = [];
        foreach ($geoN2s as $geoN2) {
            if ($geoN2 instanceof CommonGeolocalisationN2) {
                $return[] = $geoN2->getDenomination2();
            }
        }

        return ($separateur) ? implode($separateur, $return) : $return;
    }

    /**
     * @param $keyword
     *
     *
     * @throws PropelException
     */
    public function retrieveDepartements($keyword): array|bool
    {
        $word = trim($keyword);
        $arrayMotInterdit = ' and or et ou le la les un une du de des a à au aux son sa ses ne ni non sauf ce ces cet je tu il elle on nous vous ils elles etc mon ma ton ta vos se y en sur ';

        if (!strstr($arrayMotInterdit, $word)) {
            $word = '%'.Atexo_Util::replaceArrayCharactersByPourcentagage($word).'%';
            $actif_recherche_avancee = true;
            $results = self::retrieveListDepartementsByDenomNormalise($word, $actif_recherche_avancee);
            $data = [];

            if (count($results) > 0) {
                foreach ($results as $res) {
                    $id = $res->getId();
                    $data[$res->getDenomination1()]['id'] = $id;
                    $data[$res->getDenomination1()]['denomination'] = $res->getDenomination1();
                }
            }

            return $data;
        }

        return false;
    }

    /**
     * @param $denomNormalise
     * @param bool $actif_recherche_avancee
     *
     * @return array
     *
     * @throws PropelException
     */
    public function retrieveListDepartementsByDenomNormalise($denomNormalise, $actif_recherche_avancee = false)
    {
        $listRefOrgDenom = [];

        if ('' != $denomNormalise) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));

            $c = new Criteria();
            $c->add(CommonGeolocalisationN2Peer::DENOMINATION1, $denomNormalise, Criteria::LIKE);

            $c->addAscendingOrderByColumn(CommonGeolocalisationN2Peer::DENOMINATION1);
            $listRefOrgDenom = CommonGeolocalisationN2Peer::doSelect($c, $connexion);
        }

        return $listRefOrgDenom;
    }

    /**
     * @param int $id
     *
     * @return Null | CommonGeolocalisationN2
     *
     * @throws PropelException
     */
    public function retrieveDepartementById($id)
    {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $c = new Criteria();
            $c->add(CommonGeolocalisationN2Peer::ID, $id, Criteria::LIKE);
            $c->add(CommonGeolocalisationN2Peer::TYPE_LIEU, 1, Criteria::LIKE);
            return CommonGeolocalisationN2Peer::doSelectOne($c, $connexion);
    }
}
