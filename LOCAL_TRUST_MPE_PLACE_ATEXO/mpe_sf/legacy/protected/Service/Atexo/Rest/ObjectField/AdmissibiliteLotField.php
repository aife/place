<?php

namespace Application\Service\Atexo\Rest\ObjectField;

/**
 * Contient des information d'admissibilite lot.
 *
 * @author LEZ <loubna.ezziani@atexo.com>
 *
 * @since 2015-megalis
 *
 * @copyright Atexo 2016
 */
class Atexo_Rest_ObjectField_AdmissibiliteLotField
{
    protected $numeroLot;
    protected $admissibilite;

    /**
     * @return mixed
     */
    public function getLot()
    {
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAdmissibilite()
    {
        return $this->admissibilite;
    }

    /**
     * @param mixed $admissibilite
     */
    public function setAdmissibilite($admissibilite)
    {
        $this->admissibilite = $admissibilite;
    }

    /**
     * @return mixed
     */
    public function getNumeroLot()
    {
        return $this->numeroLot;
    }

    /**
     * @param mixed $numeroLot
     */
    public function setNumeroLot($numeroLot)
    {
        $this->numeroLot = $numeroLot;
    }
}
