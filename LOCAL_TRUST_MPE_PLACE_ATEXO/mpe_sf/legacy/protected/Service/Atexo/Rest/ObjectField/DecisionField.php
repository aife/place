<?php

namespace Application\Service\Atexo\Rest\ObjectField;

/**
 * Contient des information de decision.
 *
 * @author LEZ <loubna.ezziani@atexo.com>
 *
 * @since 2015-megalis
 *
 * @copyright Atexo 2016
 */
class Atexo_Rest_ObjectField_DecisionField
{
    protected $id;
    protected $organisme;
    protected $type;
    protected $reference;
    protected $collCommonLot;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    /**
     * @param mixed $organisme
     */
    public function setOrganisme($organisme)
    {
        $this->organisme = $organisme;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * @param mixed $reference
     */
    public function setReference($reference)
    {
        $this->reference = $reference;
    }

    /**
     * @return mixed
     */
    public function getCollCommonLot()
    {
        return $this->collCommonLot;
    }

    /**
     * @param mixed $collCommonLot
     */
    public function setCollCommonLot($collCommonLot)
    {
        $this->collCommonLot = $collCommonLot;
    }

    public function addDecisionLot($lot)
    {
        $this->collCommonLot[] = $lot;
    }
}
