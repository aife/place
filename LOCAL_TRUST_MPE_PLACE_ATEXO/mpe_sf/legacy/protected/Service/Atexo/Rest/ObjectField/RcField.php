<?php

namespace Application\Service\Atexo\Rest\ObjectField;

/**
 * Contient des information d'un fichier.
 *
 * @author LEZ <loubna.ezziani@atexo.com>
 *
 * @since 2015-place
 *
 * @copyright Atexo 2016
 */
class Atexo_Rest_ObjectField_RcField extends Atexo_Rest_ObjectField_FichierField
{
}
