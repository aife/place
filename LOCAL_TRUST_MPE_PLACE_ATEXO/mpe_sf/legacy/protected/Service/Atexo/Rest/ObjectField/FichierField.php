<?php

namespace Application\Service\Atexo\Rest\ObjectField;

/**
 * Contient des information d'un fichier.
 *
 * @author LEZ <loubna.ezziani@atexo.com>
 *
 * @since 2015-place
 *
 * @copyright Atexo 2016
 */
class Atexo_Rest_ObjectField_FichierField
{
    protected $id;
    protected $nom;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }
}
