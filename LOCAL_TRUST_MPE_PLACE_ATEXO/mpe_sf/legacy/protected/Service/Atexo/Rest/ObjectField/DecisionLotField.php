<?php

namespace Application\Service\Atexo\Rest\ObjectField;

use Application\Service\Atexo\Atexo_Util;

/**
 * Contient des information d'admissibilite lot.
 *
 * @author LEZ <loubna.ezziani@atexo.com>
 *
 * @since 2015-megalis
 *
 * @copyright Atexo 2016
 */
class Atexo_Rest_ObjectField_DecisionLotField
{
    protected $numeroLot;
    protected $decision;
    protected $dateDecision;
    protected $dateNotification;
    protected $trancheBudgetaire;
    protected $montantMarche;
    protected $objetMarche;
    protected $naturePrestation;
    protected $numeroMarche;
    protected $dateFinMarchePrevisionnelle;
    protected $dateFinMarcheReelle;
    protected $codePostalAttributaire;
    protected $sirenAttributaire;
    protected $nicAttributaire;

    /**
     * @return mixed
     */
    public function getLot()
    {
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDecision()
    {
        return $this->decision;
    }

    /**
     * @param mixed $decision
     */
    public function setDecision($decision)
    {
        $this->decision = $decision;
    }

    /**
     * @return mixed
     */
    public function getNumeroLot()
    {
        return $this->numeroLot;
    }

    /**
     * @param mixed $numeroLot
     */
    public function setNumeroLot($numeroLot)
    {
        $this->numeroLot = $numeroLot;
    }

    /**
     * @return mixed
     */
    public function getDateDecision()
    {
        return Atexo_Util::getDateXmlFromDateTimeVal($this->dateDecision);
    }

    /**
     * @param mixed $dateDecision
     */
    public function setDateDecision($dateDecision)
    {
        $this->dateDecision = $dateDecision;
    }

    /**
     * @return mixed
     */
    public function getDateNotification()
    {
        return Atexo_Util::getDateXmlFromDateTimeVal($this->dateNotification);
    }

    /**
     * @param mixed $dateNotification
     */
    public function setDateNotification($dateNotification)
    {
        $this->dateNotification = $dateNotification;
    }

    /**
     * @return mixed
     */
    public function getTrancheBudgetaire()
    {
        return $this->trancheBudgetaire;
    }

    /**
     * @param mixed $trancheBudgetaire
     */
    public function setTrancheBudgetaire($trancheBudgetaire)
    {
        $this->trancheBudgetaire = $trancheBudgetaire;
    }

    /**
     * @return mixed
     */
    public function getMontantMarche()
    {
        return $this->montantMarche;
    }

    /**
     * @param mixed $montantMarche
     */
    public function setMontantMarche($montantMarche)
    {
        $this->montantMarche = $montantMarche;
    }

    /**
     * @return mixed
     */
    public function getObjetMarche()
    {
        return $this->objetMarche;
    }

    /**
     * @param mixed $objetMarche
     */
    public function setObjetMarche($objetMarche)
    {
        $this->objetMarche = $objetMarche;
    }

    /**
     * @return mixed
     */
    public function getNaturePrestation()
    {
        return $this->naturePrestation;
    }

    /**
     * @param mixed $naturePrestation
     */
    public function setNaturePrestation($naturePrestation)
    {
        $this->naturePrestation = $naturePrestation;
    }

    /**
     * @return mixed
     */
    public function getNumeroMarche()
    {
        return $this->numeroMarche;
    }

    /**
     * @param mixed $numeroMarche
     */
    public function setNumeroMarche($numeroMarche)
    {
        $this->numeroMarche = $numeroMarche;
    }

    /**
     * @return mixed
     */
    public function getDateFinMarchePrevisionnelle()
    {
        return Atexo_Util::getDateXmlFromDateTimeVal($this->dateFinMarchePrevisionnelle);
    }

    /**
     * @param mixed $dateFinMarchePrevisionnelle
     */
    public function setDateFinMarchePrevisionnelle($dateFinMarchePrevisionnelle)
    {
        $this->dateFinMarchePrevisionnelle = $dateFinMarchePrevisionnelle;
    }

    /**
     * @return mixed
     */
    public function getDateFinMarcheReelle()
    {
        return Atexo_Util::getDateXmlFromDateTimeVal($this->dateFinMarcheReelle);
    }

    /**
     * @param mixed $dateFinMarcheReelle
     */
    public function setDateFinMarcheReelle($dateFinMarcheReelle)
    {
        $this->dateFinMarcheReelle = $dateFinMarcheReelle;
    }

    /**
     * @return mixed
     */
    public function getCodePostalAttributaire()
    {
        return $this->codePostalAttributaire;
    }

    /**
     * @param mixed $codePostalAttributaire
     */
    public function setCodePostalAttributaire($codePostalAttributaire)
    {
        $this->codePostalAttributaire = $codePostalAttributaire;
    }

    /**
     * @return mixed
     */
    public function getSirenAttributaire()
    {
        return $this->sirenAttributaire;
    }

    /**
     * @param mixed $sirenAttributaire
     */
    public function setSirenAttributaire($sirenAttributaire)
    {
        $this->sirenAttributaire = $sirenAttributaire;
    }

    /**
     * @return mixed
     */
    public function getNicAttributaire()
    {
        return $this->nicAttributaire;
    }

    /**
     * @param mixed $nicAttributaire
     */
    public function setNicAttributaire($nicAttributaire)
    {
        $this->nicAttributaire = $nicAttributaire;
    }
}
