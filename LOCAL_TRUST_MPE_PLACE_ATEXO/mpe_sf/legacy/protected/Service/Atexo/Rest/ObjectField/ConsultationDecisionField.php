<?php

namespace Application\Service\Atexo\Rest\ObjectField;

class Atexo_Rest_ObjectField_ConsultationDecisionField
{
    protected $typeDecision;
    protected $dateDecision;

    /**
     * @return mixed
     */
    public function getTypeDecision()
    {
        return $this->typeDecision;
    }

    /**
     * @param mixed $typeDecision
     *
     * @return Atexo_Rest_ObjectField_ConsultationDecisionField
     */
    public function setTypeDecision($typeDecision)
    {
        $this->typeDecision = $typeDecision;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getDateDecision()
    {
        return $this->dateDecision;
    }

    /**
     * @param mixed $dateDecision
     *
     * @return Atexo_Rest_ObjectField_ConsultationDecisionField
     */
    public function setDateDecision($dateDecision)
    {
        $this->dateDecision = $dateDecision;

        return $this;
    }
}
