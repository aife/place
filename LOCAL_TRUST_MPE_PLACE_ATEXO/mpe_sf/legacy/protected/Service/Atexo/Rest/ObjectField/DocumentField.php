<?php

namespace Application\Service\Atexo\Rest\ObjectField;

/**
 * Contient des information d'un document.
 *
 * @author LEZ <loubna.ezziani@atexo.com>
 *
 * @since esr-2015
 *
 * @copyright Atexo 2015
 */
class Atexo_Rest_ObjectField_DocumentField
{
    protected $idFichier;
    protected $nomFichier;
    protected $tailleFichier;
    protected $checksum;
    protected $typeFichier;
    protected $signature;
    protected $chiffrement;
    protected $empreinte;
    protected $verificationCertificat;
    protected string $typeChecksum = 'MD5';
    protected $jetonHorodatage;

    /**
     * @return mixed
     */
    public function getIdFichier()
    {
        return $this->idFichier;
    }

    /**
     * @param mixed $idFichier
     */
    public function setIdFichier($idFichier)
    {
        $this->idFichier = $idFichier;
    }

    /**
     * @return mixed
     */
    public function getNomFichier()
    {
        return $this->nomFichier;
    }

    /**
     * @param mixed $nomFichier
     */
    public function setNomFichier($nomFichier)
    {
        $this->nomFichier = $nomFichier;
    }

    /**
     * @return mixed
     */
    public function getTailleFichier()
    {
        return $this->tailleFichier;
    }

    /**
     * @param mixed $tailleFichier
     */
    public function setTailleFichier($tailleFichier)
    {
        $this->tailleFichier = $tailleFichier;
    }

    /**
     * @return mixed
     */
    public function getChecksum()
    {
        return $this->checksum;
    }

    /**
     * @param mixed $checksum
     */
    public function setChecksum($checksum)
    {
        $this->checksum = $checksum;
    }

    /**
     * @return mixed
     */
    public function getTypeFichier()
    {
        return $this->typeFichier;
    }

    /**
     * @param mixed $typeFichier
     */
    public function setTypeFichier($typeFichier)
    {
        $this->typeFichier = $typeFichier;
    }

    /**
     * @return mixed
     */
    public function getSignature()
    {
        return $this->signature;
    }

    /**
     * @param mixed $signature
     */
    public function setSignature($signature)
    {
        $this->signature = $signature;
    }

    /**
     * @return mixed
     */
    public function getChiffrement()
    {
        return $this->chiffrement;
    }

    /**
     * @param mixed $chiffrement
     */
    public function setChiffrement($chiffrement)
    {
        $this->chiffrement = $chiffrement;
    }

    /**
     * @return mixed
     */
    public function getEmpreinte()
    {
        return $this->empreinte;
    }

    /**
     * @param mixed $empreinte
     */
    public function setEmpreinte($empreinte)
    {
        $this->empreinte = $empreinte;
    }

    /**
     * @return mixed
     */
    public function getVerificationCertificat()
    {
        return $this->verificationCertificat;
    }

    /**
     * @param mixed $verificationCertificat
     */
    public function setVerificationCertificat($verificationCertificat)
    {
        $this->verificationCertificat = $verificationCertificat;
    }

    /**
     * @return mixed
     */
    public function getTypeChecksum()
    {
        return $this->typeChecksum;
    }

    /**
     * @param mixed $typeChecksum
     */
    public function setTypeChecksum($typeChecksum)
    {
        $this->typeChecksum = $typeChecksum;
    }

    /**
     * @return mixed
     */
    public function getJetonHorodatage()
    {
        return $this->jetonHorodatage;
    }

    /**
     * @param mixed $jetonHorodatage
     */
    public function setJetonHorodatage($jetonHorodatage)
    {
        $this->jetonHorodatage = $jetonHorodatage;
    }
}
