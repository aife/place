<?php

namespace Application\Service\Atexo\Rest\ObjectField;

/**
 * Contient des information d'un fichier.
 *
 * @author LEZ <loubna.ezziani@atexo.com>
 *
 * @since 2015-place
 *
 * @copyright Atexo 2016
 */
class Atexo_Rest_ObjectField_QuestionFichierField extends Atexo_Rest_ObjectField_FichierField
{
    protected $nomFichierQuestion;

    /**
     * @return mixed
     */
    public function getNomFichierQuestion()
    {
        return $this->nomFichierQuestion;
    }

    /**
     * @param mixed $nomFichierQuestion
     */
    public function setNomFichierQuestion($nomFichierQuestion)
    {
        $this->nomFichierQuestion = $nomFichierQuestion;
    }
}
