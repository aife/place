<?php

namespace Application\Service\Atexo\Rest\Action;

use Application\Propel\Mpe\CommonFichierEnveloppe;

/**
 * Contient des attributs d'enveloppe.
 *
 * @author LEZ <loubna.ezziani@atexo.com>
 * @copyright Atexo 2014
 *
 * @version 1.0
 *
 * @since 4.6.0
 */
class Atexo_Rest_Action_EnveloppeField
{
    protected ?string $typeEnveloppe = null;
    protected ?int $numeroLot = null;
    protected $plis;
    protected $id;

    /*
     * Get the value of [typeEnveloppe] column.
     *
     * @return string type d'enveloppe
     * @author LEZ <loubna.ezziani@atexo.com>
     * @version 1.0
     * @since 4.6.0
     * @copyright Atexo 2014
     */
    public function getTypeEnveloppe()
    {
        return $this->typeEnveloppe;
    }

    /**
     * Set the [typeEnveloppe] column value.
     *
     * @param string $value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setTypeEnveloppe($value)
    {
        $this->typeEnveloppe = $value;
    }

    /*
     * Get the value of [numeroLot] column.
     *
     * @return Integer num lot
     * @author LEZ <loubna.ezziani@atexo.com>
     * @version : 1.0
     * @since : 4.6.0
     * @copyright Atexo 2014
     */
    public function getNumeroLot()
    {
        return $this->numeroLot;
    }

    /**
     * Set the [numeroLot] column value.
     *
     * @param int $value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version : 1.0
     *
     * @since : 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setNumeroLot($value)
    {
        $this->numeroLot = $value;
    }

    /**
     * Permet d'avoir l'objet courant.
     *
     * @return Atexo_Rest_Action_EnveloppeField The current object (for fluent API support)
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version : 1.0
     *
     * @since : 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getEnveloppe()
    {
        return $this;
    }

    /*
     * Set the value of [plis] column.
     *
     * @param  CommonFichierEnveloppe $plis instanceof CommonFichierEnveloppe
     * @return void
     * @author LEZ <loubna.ezziani@atexo.com>
     * @version : 1.0
     * @since : 4.6.0
     * @copyright Atexo 2014
     */
    public function setPlis($plis)
    {
        $this->plis = $plis;
    }

    /**
     * Get the [plis] column value.
     *
     * @return CommonFichierEnveloppe
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version : 1.0
     *
     * @since : 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getPlis()
    {
        return $this->plis;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }
}
