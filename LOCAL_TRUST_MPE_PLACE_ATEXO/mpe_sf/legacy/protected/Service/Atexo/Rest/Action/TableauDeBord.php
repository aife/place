<?php

namespace Application\Service\Atexo\Rest\Action;

use App\Service\LoginFromPrado;
use Application\Library\Propel\Propel;
use Application\Pages\Commun\Auth;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_UserVo;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Registres\Atexo_Registres_Questions;

class Atexo_Rest_Action_TableauDeBord extends Atexo_Rest_Action_Actions
{
    public function post()
    {
        $logger = Atexo_LoggerManager::getLogger('consultation');
        $retour = $this->validateTicket($logger);

        if (true !== $retour) {
            return $retour;
        }
        $login = $_POST['login'];
        $loginService = Atexo_Util::getSfService(LoginFromPrado::class);
        $userConnected = $loginService->loginByUsername($login);
        $type = $_POST['type'];
        if (!$this->verifieAccesToInfo($_GET['ticket'], $userConnected->getOrganisme()->getAcronyme(), $userConnected->getServiceId())) {
            return static::generateXmlError($this->getMessageErrorForAccessDenied($userConnected->getOrganisme()->getAcronyme(), $userConnected->getServiceId()));
        }
        $criteriaVo = new Atexo_Consultation_CriteriaVo();
        $criteriaVo->setConnectedAgentId($userConnected->getId());
        $criteriaVo->setIdService($userConnected->getServiceId());
        $criteriaVo->setAcronymeOrganisme($userConnected->getOrganisme()->getAcronyme());
        $criteriaVo->setTypeSearch('tout');
        $criteriaVo->setSecondConditionOnEtatConsultation(Atexo_Config::getParameter('STATUS_AVANT_ARCHIVE'));
        $criteriaVo->setIdTypeAvis(Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION'));

        $consultation = new Atexo_Consultation();
        $arrayConsultation = $consultation->search($criteriaVo);
        $result = [];
        if ('questions' == $type) {
            $result = $this->getQuestionsResponse($arrayConsultation);
        } elseif ('consultationsencours' == $type) {
            $result = $this->getConsultationsencoursResponse($arrayConsultation);
        }

        return json_encode($result, JSON_THROW_ON_ERROR);
    }

    private function getQuestionsResponse($arrayConsultation)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));

        $arrayResponse = ['questionsAttenteReponses' => 0];

        foreach ($arrayConsultation as $consultation) {
            $arrayResponse['questionsAttenteReponses'] += (new Atexo_Registres_Questions())->getNbReponseByConsultation($consultation->getId(), $consultation->getOrganisme(), $connexion);
        }

        return $arrayResponse;
    }

    private function getConsultationsencoursResponse($arrayConsultation)
    {
        $arrayNbParStatut = [
            'attenteValidation' => 0,
            'elaboration' => 0,
            'consultation' => 0,
            'ouvertureAnalyse' => 0,
            'decision' => 0,
        ];
        foreach ($arrayConsultation as $consultation) {
            switch ($consultation->getStatusConsultation()) {
                case Atexo_Config::getParameter('STATUS_PREPARATION'):
                    $arrayNbParStatut['attenteValidation']++;
                    break;
                case Atexo_Config::getParameter('STATUS_ELABORATION'):
                    $arrayNbParStatut['elaboration']++;
                    break;
                case Atexo_Config::getParameter('STATUS_CONSULTATION'):
                    $arrayNbParStatut['consultation']++;
                    break;
                case Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE'):
                    $arrayNbParStatut['ouvertureAnalyse']++;
                    break;
                case Atexo_Config::getParameter('STATUS_DECISION'):
                    $arrayNbParStatut['decision']++;
                    break;
            }
        }

        return $arrayNbParStatut;
    }
}
