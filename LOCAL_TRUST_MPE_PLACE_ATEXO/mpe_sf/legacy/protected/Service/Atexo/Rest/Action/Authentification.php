<?php

namespace Application\Service\Atexo\Rest\Action;

use Application\Service\Atexo\Atexo_TiersAuthentification;
use Exception;

/*
 * permet de gerer l'authetification d'une application tierce via REST
 * @package  Atexo_Rest_Action
 * @subpackage Authentification
 *
 */

class Atexo_Rest_Action_Authentification extends Atexo_Rest_Action_Actions
{
    /*
     * permet d'avoir le ticket de l'authetification d'une application tierce via REST
     *
     * @param string $login,le login d'accés au web service,$password(string) : le mot de passe d'accés au web service
     * @return  string xml contenant le ticket
     */
    public function get($login, $password)
    {
        try {
            $ticketXml = (new Atexo_TiersAuthentification())->createTicketAuthentificationTiers($login, $password);
            $this->getHeaders('200');

            return $ticketXml;
        } catch (Exception) {
            $this->getHeaders('500');

            return static::generateXmlError('erreur technique');
        }
    }
}
