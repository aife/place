<?php

namespace Application\Service\Atexo\Rest\Action;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonOffrePapier;
use Application\Propel\Mpe\CommonOffres;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Decision;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Responses;
use Exception;

/**
 * permet de gérer les decision via ws.
 *
 * @author LEZ <loubna.ezziani@atexo.com>
 *
 * @version 1.0
 *
 * @since esr-2016
 *
 * @copyright Atexo 2016
 */
class Atexo_Rest_Action_Decision extends Atexo_Rest_Action_Actions
{
    /**
     * permet de retourner les info de decision.
     *
     * @param $organisme
     * @param $typeDepot ELECTRONIQUE|PAPIER
     * @param $id
     * @param null $organisme
     *
     * @return string xml contenant les information de la decision
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-megalis
     *
     * @copyright Atexo 2016
     */
    public function get($organisme, $typeDepot, $id)
    {
        $messageErreur = null;
        $typeDepotOffre = null;
        try {
            $logger = Atexo_LoggerManager::getLogger('contrat');
            $logger->info("Debut de la récupération d'info decision....");
            $retour = $this->validateTicket($logger);
            if (true === $retour) {
                $msgInfoDecision = "l'organisme => ".$organisme.' ,typeDepot => '.$typeDepot."  , l'id => ".$id;
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
                $logger->info('La récupération de la decision ('.$msgInfoDecision.')');
                $xmlResponse = '';
                $consultation = null;
                $typeDepot = strtoupper($typeDepot);
                switch ($typeDepot) {
                    case 'ELECTRONIQUE':
                        $typeDepotOffre = Atexo_Config::getParameter('DEPOT_ELECTRONIQUE');
                        $logger->info('La récupération du depot electronique');
                        $offre = (new Atexo_Consultation_Responses())->retrieveOffreById($id, $organisme);
                        if ($offre instanceof CommonOffres) {
                            $logger->info("La récupération de la consultation de l'offre electronique");
                            $consultation = $offre->getCommonConsultation($connexion);
                        } else {
                            $messageErreur = "Ce dépôt n'existe pas";
                        }
                        break;
                    case 'PAPIER':
                        $typeDepotOffre = Atexo_Config::getParameter('DEPOT_PAPIER');
                        $logger->info('La récupération du depot papier');
                        $offrePapier = (new Atexo_Consultation_Responses())->retrieveOffrePapierById($id, $organisme);
                        if ($offrePapier instanceof CommonOffrePapier) {
                            $logger->info("La récupération de la consultation de l'offre papier");
                            $consultation = $offrePapier->getCommonConsultation($connexion);
                        } else {
                            $messageErreur = "Ce dépôt n'existe pas";
                        }
                        break;
                    default:
                        $messageErreur = "Ce type de depot n'existe pas";
                }
                if (!($consultation instanceof CommonConsultation)
                    ||
                    !($this->verifieAccesToInfo($_GET['ticket'], $organisme, $consultation->getServiceId()))) {
                    $consultationExist = ($consultation instanceof CommonConsultation);
                    $log = $consultationExist ?
                        ('Vérification de droit acces KO organisme '.$organisme.' service '.$consultation->getServiceId()) :
                        ("consultation n'existe pas pr depot ".$typeDepot.' '.$id);
                    $logger->info($log);
                    $serviceId = $consultationExist ? $consultation->getServiceId() : null;
                    $messageErreur = $this->getMessageErrorForAccessDenied($organisme, $serviceId);
                }
                if (!$messageErreur) {
                    $messageErreur = (new Atexo_Consultation_Decision())->setInfoDecisionForWs($organisme, $id, $typeDepotOffre, $xmlResponse, $logger, $consultation);
                    if ($xmlResponse) {
                        $xmlResponse = $this->encapsulerNodeResponseWsMpe($xmlResponse, 'decisionEnveloppe');
                        $logger->info("La validation de l'xml à retourner");
                        $xmlValide = Atexo_Util::validateXmlBySchema($xmlResponse, Atexo_Config::getParameter('PATH_FILE_XSD_WS'), $logger);
                        if ($xmlValide) {
                            $logger->info("L'xml à envoyé est valide");
                            $this->getHeaders('200');

                            return $xmlResponse;
                        } else {
                            throw new Exception("la structure de l'xml genéré est invalide ");
                        }
                    } else {
                        $messageErreur ??= 'Les parametres ('.$msgInfoDecision.') sont invalides';
                    }
                }
                if ($messageErreur) {
                    $logger->error($messageErreur);
                    $this->getHeaders('400');
                    $logger->info('Fin de la récupération du document....');

                    return static::generateXmlError($messageErreur);
                }
            } else {
                return $retour;
            }
            $logger->info("Fin de la récupération d'info decision....");
        } catch (Exception $e) {
            $logger->error('Erreur technique = '.$e->getMessage().' '.$e->getTraceAsString());
            $this->getHeaders('500');

            return static::generateXmlError('erreur technique');
        }
    }
}
