<?php

namespace Application\Service\Atexo\Rest\Action;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonAVIS;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonDCE;
use Application\Propel\Mpe\CommonQuestionsDce;
use Application\Propel\Mpe\CommonRG;
use Application\Propel\Mpe\CommonTCandidatureMps;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_Document;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_MarchePublicSimplifie;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Config\Atexo_Config_Exception;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Dce;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Rg;
use Application\Service\Atexo\Oversight\Atexo_Oversight_Oversight;
use Application\Service\Atexo\Publicite\Atexo_Publicite_Avis;
use Application\Service\Atexo\Registres\Atexo_Registres_Questions;
use AtexoCrypto\Dto\Consultation;
use DateTime;
use Exception;

/**
 * permet de gérer les document via ws.
 *
 * @author LEZ <loubna.ezziani@atexo.com>
 *
 * @version 1.0
 *
 * @since esr-2015
 *
 * @copyright Atexo 2015
 */
class Atexo_Rest_Action_Document extends Atexo_Rest_Action_Actions
{
    /**
     * permet de retourner un fichier.
     *
     * @param $phase consultation|depot|archive|organisme
     * @param $typeDoc REGLEMENT_CONSULTATION|AVIS_PUBLICITE|DCE|QUESTION|PLIS|CANDIDATURE_MPS|AUTRE_PIECE|ORG_LOGO_GRAND|ORG_LOGO_PETIT
     * @param $id
     * @param null $organisme
     *
     * @return string xml contenant le statut de reponse et le fichier
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 2.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function get($phase, $typeDoc, $id, $organisme = null, $lot = null)
    {
        $params = [];

        $params['debutExecution'] = new DateTime('now');
        $params['nomBatch'] = 'WS-document-récupération';
        $params['service'] = 'Document';
        $entree = serialize(['phase' => $phase, 'typeDoc' => $typeDoc, 'id' => $id, 'organisme' => $organisme, 'lot' => $lot]);
        $params['variablesEntree'] = $entree;
        $params['typeFlux'] = 'WebService';
        $params['ticket'] = Atexo_Util::atexoHtmlEntities($_GET['ticket']);
        self::oversight($params);
        $params['code'] = Atexo_Oversight_Oversight::$codeSuccess;

        try {
            $logger = Atexo_LoggerManager::getLogger('crypto');
            $logger->info('Debut de la récupération du document....');
            $typeChecksum = isset($_GET['typeChecksum']) ? strtolower($_GET['typeChecksum']) : 'md5';
            if ('sha256' != $typeChecksum && 'md5' != $typeChecksum) {
                $logger->info('Erreur  : typeChecksum non valide');
                throw new Exception('Erreur  : typeChecksum non valide');
            }
            $retour = $this->validateTicket($logger);
            if (true === $retour) {
                $msgInfoDocument = ' phase => '.$phase.' ,le type => '.$typeDoc." , l'id => ".$id;
                if ($_GET['ticket'] != $organisme) {
                    $msgInfoDocument .= " et l'organisme => ".$organisme;
                } else {
                    $organisme = null;
                }
                $chiffrer = true;
                if (isset($_GET['chiffrer']) && !Atexo_Util::castToBoolean($_GET['chiffrer'])) {
                    $chiffrer = false;
                }
                $withFile = true;
                if (isset($_GET['withFile']) && !Atexo_Util::castToBoolean($_GET['withFile'])) {
                    $withFile = false;
                }
                if ($lot === $_GET['ticket']) {
                    $lot = 0;
                }
                if ($lot !== $_GET['ticket'] && null !== $lot) {
                    $msgInfoDocument .= ' et lot => '.$lot;
                }
                $logger->info('La récupération du document ('.$msgInfoDocument.')');
                $xmlResponse = '';
                $arrayBlob = [];
                $encoding = 'binary';
                $fileDefault = false;
                $contentType = 'application/octet-stream';
                $logger->info('Vérification de droit acces ...');
                if ($this->verifieAccesToInfoDocument($phase, $typeDoc, $id, $organisme)) {
                    $logger->info('Vérification de droit acces OK organisme '.$organisme);
                    $messageErreur = (new Atexo_Document())->doTratementGetDocument($phase, $typeDoc, $id, $organisme, $xmlResponse, $contentType, $arrayBlob, $logger, $typeChecksum, $chiffrer, $withFile, $lot);
                    if ($chiffrer && 'DEPOT' === strtoupper($phase) && 'PLIS' === strtoupper($typeDoc)) {
                        $encoding = 'base64';
                        $fileDefault = true;
                    }
                    if ($xmlResponse) {
                        $xmlValide = Atexo_Util::validateXmlBySchema(
                            $xmlResponse,
                            Atexo_Config::getParameter('PATH_FILE_XSD_WS'),
                            $logger
                        );
                        if ($xmlValide) {
                            $logger->info("L'xml à envoyé est valide");
                        } else {
                            $log = "La structure de l'xml genéré est invalide IP ".$_SERVER['REMOTE_ADDR'].
                               ' XML : '.print_r($xmlResponse, true)." \n ";
                            $messageErreur = "La structure de l'xml genéré est invalide pour les parametres ("
                               .$msgInfoDocument.')';
                            $logger->error($log);
                        }
                    }
                    if (!$messageErreur) {
                        if ($xmlResponse || (is_array($arrayBlob) && count($arrayBlob))) {
                            if ($withFile) {
                                $logger->info("L'envoi du document (".$msgInfoDocument.')');
                                self::sendDataWithFileBlocs(
                                    $arrayBlob,
                                    $xmlResponse,
                                    $encoding,
                                    $contentType,
                                    $fileDefault
                                );
                                try {
                                    self::oversight($params);
                                } catch (Exception $exceptionOversight) {
                                    $logger = Atexo_LoggerManager::getLogger('supervision');
                                    $logger->error('Error sur la supervision : '.
                                       $exceptionOversight->getMessage());
                                }
                                $logger->info('Document envoyé (phase => '.$phase.', le type => '.
                                   $typeDoc.' et id => '.$id.')');
                            } else {
                                $logger->info("L'envoi du xml d'info document (".$msgInfoDocument.')');
                                $this->getHeaders('200');

                                return $xmlResponse;
                            }
                        } else {
                            $messageErreur = 'Les parametres ('.$msgInfoDocument.') sont invalides';
                        }
                    }
                } else {
                    $logger->info("Pas de droits acces ou l'objet n'existe pas");
                    $messageErreur = $this->getMessageErrorForAccessDenied();
                }
                if ($messageErreur) {
                    $logger->error($messageErreur);
                    $this->getHeaders('400');
                    $logger->info('Fin de la récupération du document....');
                    $params['code'] = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;

                    return static::generateXmlError($messageErreur);
                }
            } else {
                return $retour;
            }
            $logger->info('Fin de la récupération du document....');
        } catch (Exception $e) {
            $logger->error('Erreur technique = '.$e->getMessage().' '.$e->getTraceAsString());
            $this->getHeaders('500');

            try {
                $params['code'] = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
                self::oversight($params);
            } catch (Exception $exceptionOversight) {
                $logger = Atexo_LoggerManager::getLogger('supervision');
                $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
            }

            return static::generateXmlError('erreur technique');
        }
        try {
            $params['oversight'] = self::oversight($params);
        } catch (Exception $exceptionOversight) {
            $logger = Atexo_LoggerManager::getLogger('supervision');
            $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
        }
    }

    /**
     * permet de modifier un fichier.
     *
     * @param $phase consultation|depot|archive|organisme
     * @param $typeDoc REGLEMENT_CONSULTATION|AVIS_PUBLICITE|DCE|QUESTION|PLIS|CANDIDATURE_MPS|AUTRE_PIECE|ORG_LOGO_GRAND|ORG_LOGO_PETIT
     * @param $id
     * @param null $organisme
     *
     * @return string xml contenant le statut de reponse et le fichier
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function post($phase, $typeDoc, $id, $organisme = null)
    {
        $messageErreur = null;
        $logger = Atexo_LoggerManager::getLogger('consultation');
        $params = [];
        try {
            $params['debutExecution'] = new DateTime('now');
            $params['nomBatch'] = 'WS-document-modification';
            $params['service'] = 'Document';
            $entree = serialize(['phase' => $phase, 'typeDoc' => $typeDoc, 'id' => $id, 'organisme' => $organisme]);
            $params['variablesEntree'] = $entree;
            $params['typeFlux'] = 'WebService';
            $params['ticket'] = Atexo_Util::atexoHtmlEntities($_GET['ticket']);
            self::oversight($params);
            $params['code'] = Atexo_Oversight_Oversight::$codeSuccess;
        } catch (Exception $exceptionOversight) {
            $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
        }

        try {
            $logger->info('Debut de la modification du document....');

            $logger->info('vérification du ticket ...');

            $retour = $this->validateTicket($logger);
            if (true === $retour) {
                $id = $this->getRightIdConsultation($id);
                $msgInfoDocument = ' phase => '.$phase.' ,le type => '.$typeDoc." , l'id => ".$id;
                if ($_GET['ticket'] != $organisme) {
                    $msgInfoDocument .= " et l'organisme => ".$organisme;
                } else {
                    $organisme = null;
                }
                $logger->info('Vérification de droit acces ...');
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                $consultation = (new Atexo_Consultation())->getConsultationByReference($id, $organisme, $connexion);
                if ($consultation instanceof CommonConsultation) {
                    if ($this->verifieAccesToInfo($_GET['ticket'], $organisme, $consultation->getServiceId())) {
                        $logger->info('Vérification de droit acces OK organisme '.$organisme);
                        $logger->info('La modification du document ('.$msgInfoDocument.')');
                        $phase = strtoupper($phase);
                        $typeDoc = strtoupper($typeDoc);
                        if (isset($_FILES) && is_array($_FILES) && count($_FILES)) {
                            $xmlResponse = '';
                            switch ($phase) {
                                case 'CONSULTATION':
                                    switch ($typeDoc) {
                                        case 'DCE':
                                            $messageErreur = (new Atexo_Consultation_Dce())->updateDCEFromWs($id, $organisme, $_FILES['DCE']['tmp_name'], $_FILES['DCE']['name'], $xmlResponse, $logger);
                                            break;
                                        case 'REGLEMENT_CONSULTATION':
                                            $messageErreur = (new Atexo_Consultation_Rg())->updateRGFromWs($id, $organisme, $_FILES['RC']['tmp_name'], $_FILES['RC']['name'], $xmlResponse, $logger);
                                            break;
                                    }
                                    break;
                            }

                            if (!$messageErreur) {
                                if ($xmlResponse) {
                                    $xmlValide = Atexo_Util::validateXmlBySchema($xmlResponse, Atexo_Config::getParameter('PATH_FILE_XSD_WS'), $logger);
                                    if ($xmlValide) {
                                        $logger->info("L'xml à envoyé est valide");
                                        $logger->info('Fin de la modification du document....');
                                        $this->getHeaders('200');
                                        try {
                                            $params['oversight'] = self::oversight($params);
                                        } catch (Exception $exceptionOversight) {
                                            $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
                                        }

                                        return $xmlResponse;
                                    } else {
                                        $log = "La structure de l'xml genéré est invalide IP ".$_SERVER['REMOTE_ADDR'].' XML : '.print_r($xmlResponse, true)." \n ";
                                        $messageErreur = "La structure de l'xml genéré est invalide pour les parametres (".$msgInfoDocument.')';
                                        $logger->error($log);
                                    }
                                } else {
                                    $messageErreur = 'Les parametres ('.$msgInfoDocument.') sont invalides';
                                }
                            }
                        } else {
                            $messageErreur = 'Merci de joindre un fichier';
                        }
                    } else {
                        $logger->info("Pas de droits acces ou l'objet n'existe pas");
                        $messageErreur = $this->getMessageErrorForAccessDenied($organisme, $consultation->getServiceId());
                    }
                } else {
                    $logger->info("Consultation n'existe pas");
                    $messageErreur = "Consultation n'existe pas";
                }

                if ($messageErreur) {
                    $logger->error($messageErreur);
                    $this->getHeaders('400');
                    $logger->info('Fin de la modification du document....');

                    try {
                        $params['code'] = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
                        $params['oversight'] = self::oversight($params);
                    } catch (Exception $exceptionOversight) {
                        $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
                    }

                    return static::generateXmlError($messageErreur);
                }
            } else {
                try {
                    $params['code'] = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
                    $params['oversight'] = self::oversight($params);
                } catch (Exception $exceptionOversight) {
                    $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
                }

                return $retour;
            }
            $logger->info('Fin de la modification du document....');
        } catch (Exception $e) {
            $logger->error('Erreur technique = '.$e->getMessage().' '.$e->getTraceAsString());
            $this->getHeaders('500');

            return static::generateXmlError('erreur technique');
        }
    }

    /**
     * @param $phase
     * @param $typeDoc
     * @param $id
     * @param $organisme
     *
     * @return bool
     *
     * @throws Atexo_Config_Exception
     */
    protected function verifieAccesToInfoDocument($phase, $typeDoc, $id, &$organisme)
    {
        $service = '';
        $reference = '';
        $phase = strtoupper($phase);
        $typeDoc = strtoupper($typeDoc);
        switch ($phase) {
            case 'DEPOT':
                switch ($typeDoc) {
                    case 'MPS':
                        $candidatureMps = (new Atexo_MarchePublicSimplifie())->retrieveCandidatureMpsByIdOffre($id, $organisme);
                        if ($candidatureMps instanceof CommonTCandidatureMps) {
                            $reference = $candidatureMps->getConsultationId();
                            $organisme = $candidatureMps->getOrganisme();
                        } else {
                            $organisme = null;
                        }
                        break;
                    case 'PLIS':
                        $rs = (new Atexo_Consultation())->getReferenceConsultationByIdFichierEnveloppe($id, $organisme);
                        $reference = $rs['id'];
                        $organisme = ($rs['organisme']) ?? null;
                        break;
                }
                break;
            case 'CONSULTATION':
                switch ($typeDoc) {
                    case 'DCE':
                        $dce = (new Atexo_Consultation_Dce())->retrieveDceById($id, $organisme);
                        if ($dce instanceof CommonDCE) {
                            $reference = $dce->getConsultationId();
                            $organisme = $dce->getOrganisme();
                        } else {
                            $organisme = null;
                        }
                        break;
                    case 'REGLEMENT_CONSULTATION':
                        $rg = (new Atexo_Consultation_Rg())->retrieveRgById($id, $organisme);
                        if ($rg instanceof CommonRG) {
                            $reference = $rg->getConsultationId();
                            $organisme = $rg->getOrganisme();
                        } else {
                            $organisme = null;
                        }
                        break;
                    case 'AVIS_PUBLICITE':
                        $avis = (new Atexo_Publicite_Avis())->retrieveAvisPublieById($id, Atexo_Config::getParameter('TYPE_FORMAT_LIBRE_FICHIER_JOINT'), Atexo_Config::getParameter('DESTINATAIRE_PUBLIE'), $organisme);
                        if ($avis instanceof CommonAVIS) {
                            $reference = $avis->getConsultationId();
                            $organisme = $avis->getOrganisme();
                        } else {
                            $organisme = null;
                        }
                        break;
                    case 'QUESTION':
                        $question = (new Atexo_Registres_Questions())->getQuestionByIdFichier($id, $organisme);
                        if ($question instanceof CommonQuestionsDce) {
                            $reference = $question->getConsultationId();
                            $organisme = $question->getOrganisme();
                        } else {
                            $organisme = null;
                        }
                        break;
                }
                break;
            case 'ORGANISME':
                if (in_array($typeDoc, ['ORG_LOGO_GRAND', 'ORG_LOGO_PETIT'])) {
                    $organisme = $id;
                }
                break;
            case 'ARCHIVE':
                if (in_array($typeDoc, ['AUTRE_PIECE', 'BORDEREAU_ARCHIVE'])) {
                    $reference = $id;
                }
                break;
        }

        if ($reference && $organisme) {
            $consultation = (new Atexo_Consultation())->getConsultationByReference($reference, $organisme);
            if ($consultation instanceof CommonConsultation) {
                $organisme = $consultation->getOrganisme();
                $service = $consultation->getServiceId();
            }
        }
        if ($organisme) {
            return $this->verifieAccesToInfo($_GET['ticket'], $organisme, $service);
        }

        return false;
    }
}
