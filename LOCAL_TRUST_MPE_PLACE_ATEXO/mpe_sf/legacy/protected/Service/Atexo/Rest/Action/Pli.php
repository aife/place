<?php

namespace Application\Service\Atexo\Rest\Action;

use Application\Library\Propel\Exception\PropelException;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonFichierEnveloppe;
use Application\Propel\Mpe\CommonOffres;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_FichierEnveloppe;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Config\Atexo_Config_Exception;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Responses;
use Application\Service\Atexo\Oversight\Atexo_Oversight_Oversight;
use Application\Service\Atexo\Serializer\Atexo_Serializer_Serializer;
use DateTime;
use Exception;

/**
 * permet de gérer une pli.
 *
 * @author ASO <ayoub.souidahmed@atexo.com>
 *
 * @version 1.0
 *
 * @since 4.6.0
 *
 * @copyright Atexo 2014
 */
class Atexo_Rest_Action_Pli extends Atexo_Rest_Action_Actions
{
    /**
     * permet de retourner le pli.
     *
     * @param int $idPli l'id du pli concerne
     *
     * @return string xml contenant les infos du pli
     *
     * @author ASO <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function get($idPli)
    {
        $params = [];
        try {
            $params['debutExecution'] = new DateTime('now');
            $params['nomBatch'] = 'WS-pli-get';
            $params['service'] = 'Plis';
            $entree = serialize(['plis' => $idPli]);
            $params['variablesEntree'] = $entree;
            $params['typeFlux'] = 'WebService';
            $params['poids'] = 0;
            $params['nbFlux'] = 1;
            $params['ticket'] = Atexo_Util::atexoHtmlEntities($_GET['ticket']);
            self::oversight($params);
            $params['code'] = Atexo_Oversight_Oversight::$codeSuccess;
        } catch (Exception $exceptionOversight) {
            $logger = Atexo_LoggerManager::getLogger('supervision');
            $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
        }

        try {
            $logger = Atexo_LoggerManager::getLogger('crypto');
            $retour = $this->validateTicket($logger);

            if (true === $retour) {
                $organisme = null;
                $serviceId = null;
                $logger->info('Vérification de droit acces ...');
                if (!$this->verifieAccesToInfoPli($idPli, $organisme, $serviceId)) {
                    $logger->info('Vérification de droit acces KO organisme '.$organisme.' service '.$serviceId);

                    return self::generateXmlError($this->getMessageErrorForAccessDenied($organisme, $serviceId));
                }
                $logger->info('Vérification de droit acces OK');
                $pli = (new Atexo_FichierEnveloppe())->getFichierEnveloppeById($idPli, $organisme);
                $blocsFichierEnveloppe = (new Atexo_Consultation_Responses())->getBlocsFichiersEnveloppesByIdEnveloppe($idPli, $organisme);
                $arrayBlob = [];
                if ($pli && is_array($blocsFichierEnveloppe) && count($blocsFichierEnveloppe)) {
                    $chiffrementOffre = Atexo_Util::getBooleanStringFromValInt((new Atexo_Consultation_Responses())->getChiffrementConsultationByIdEnveloppe($pli->getIdEnveloppe(), $pli->getOrganisme()));
                    $pli->setChiffrement($chiffrementOffre);
                    $logger->info('Recuperation des blocs du Pli');
                    foreach ($blocsFichierEnveloppe as $blocFichierEnveloppe) {
                        $logger->info("Recuperation du bloc dont l'id => ".$blocFichierEnveloppe->getIdBlobChiffre());
                        $arrayBlob[] = Atexo_Blob::getBlobById($blocFichierEnveloppe->getIdBlobChiffre(), $blocFichierEnveloppe->getOrganisme(), null, false);
                    }
                    $xmlResponse = self::getXmlInfosPli($pli);
                    if (is_array($arrayBlob) && count($arrayBlob)) {
                        $logger->info("Debut d'envoi des blocs du pli");
                        self::sendDataWithFile($arrayBlob, $xmlResponse, 'base64', 'application/octet-stream', true);
                        try {
                            self::oversight($params);
                        } catch (Exception $exceptionOversight) {
                            $logger = Atexo_LoggerManager::getLogger('supervision');
                            $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
                        }
                        $logger->info("Fin d'envoi des blocs du pli");
                    } else {
                        $logger->error("Le Fichier n'existe pas");
                        $this->getHeaders('404');

                        try {
                            self::oversight($params);
                        } catch (Exception $exceptionOversight) {
                            $logger = Atexo_LoggerManager::getLogger('supervision');
                            $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
                        }

                        return static::generateXmlError("Le Fichier n'existe pas");
                    }
                } else {
                    $logger->error("Le pli dont l'id = ".$idPli." n'existe pas");
                    $this->getHeaders('400');

                    try {
                        $params['oversight'] = self::oversight($params);
                    } catch (Exception $exceptionOversight) {
                        $logger = Atexo_LoggerManager::getLogger('supervision');
                        $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
                    }

                    return static::generateXmlError("Le pli dont l'id = ".$idPli." n'existe pas");
                }
            } else {
                try {
                    $params['code'] = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
                    self::oversight($params);
                } catch (Exception $exceptionOversight) {
                    $logger = Atexo_LoggerManager::getLogger('crypto');
                    $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
                }

                return $retour;
            }
        } catch (\Exception $e) {
            try {
                $params['code'] = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
                self::oversight($params);
            } catch (Exception $exceptionOversight) {
                $logger = Atexo_LoggerManager::getLogger('crypto');
                $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
            }

            $logger->error('Erreur technique = '.$e->getMessage().' '.$e->getTraceAsString());
            $this->getHeaders('500');

            return static::generateXmlError('erreur technique');
        }
    }

    /**
     * genere un xml contenant les infos d'un pli.
     *
     * @param CommonFichierEnveloppe $pli objet du CommonFichierEnveloppe
     *
     * @return string xml contenant les infos d'un pli
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function getXmlInfosPli($pli)
    {
        $serializer = new Atexo_Serializer_Serializer();
        $mappingFile = Atexo_Config::getParameter('PATH_FILE_MAPPING_PLI');
        $hiddenFile = Atexo_Config::getParameter('PATH_FILE_HIDDEN_FIELD_PLI');
        $xml = $serializer->SerializeClass($pli, 'CommonFichierEnveloppe', $mappingFile, $hiddenFile);

        return self::encapsulerNodeResponseWsMpe($xml, 'pli');
    }

    /**
     * permet d'insérer les blocs chiffres.
     *
     * @param string $fct       chiffre ou dechiffrer
     * @param int    $idFichier l'id du pli concerne
     *
     * @return string xml contenant les infos du pli et des blocs
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function post($fct, $idFichier)
    {
        $params = [];
        try {
            $params['debutExecution'] = new DateTime('now');
            $params['informationMetier'] = serialize(['organisme' => $_POST['organisme'], 'numeroOrdreBloc' => $_POST['numeroOrdreBloc'],
                'hashBloc' => $_POST['hashBloc'], ]);
            $params['variablesEntree'] = serialize(['fct' => $fct, 'idFichier' => $idFichier]);
            $params['nomBatch'] = 'WS-pli-post';
            $params['service'] = 'Plis';
            $params['typeFlux'] = 'WebService';
            $params['poids'] = 0;
            $params['nbFlux'] = 1;
            $params['ticket'] = Atexo_Util::atexoHtmlEntities($_GET['ticket']);
            $params['code'] = Atexo_Oversight_Oversight::$codeSuccess;
        } catch (Exception $exceptionOversight) {
            $logger = Atexo_LoggerManager::getLogger('crypto');
            $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
        }

        try {
            $logger = Atexo_LoggerManager::getLogger('crypto');
            $retour = $this->validateTicket($logger);
            if (true === $retour) {
                $organisme = null;
                $serviceId = null;
                $logger->info('Vérification de droit acces ...');
                if (!$this->verifieAccesToInfoPli($idFichier, $organisme, $serviceId)) {
                    $logger->info('Vérification de droit acces KO organisme '.$organisme.' service '.$serviceId);

                    return self::generateXmlError($this->getMessageErrorForAccessDenied($organisme, $serviceId));
                }
                $logger->info('Vérification de droit acces OK');

                if ('chiffre' == $fct) {
                    $xmlResult = $this->doTraitementChiffrement($idFichier, $logger);
                } elseif ('dechiffre' == $fct) {
                    $xmlResult = $this->doTraitementDechiffrement($idFichier, $logger);
                } else {
                    $logger->error('Pas de fontionalité avec ce nom '.$fct);
                    $this->getHeaders('404');
                    $xmlResult = static::generateXmlError('Pas de fontionalité avec ce nom '.$fct);
                }
            } else {
                $xmlResult = $retour;
            }
        } catch (\Exception $e) {
            $params['code'] = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
            $logger->error('Erreur technique = '.$e->getMessage().' '.$e->getTraceAsString());
            $this->getHeaders('500');
            $xmlResult = static::generateXmlError('erreur technique');
        }

        try {
            $params['oversight'] = self::oversight($params);
        } catch (Exception $exceptionOversight) {
            $logger = Atexo_LoggerManager::getLogger('crypto');
            $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
        }

        return $xmlResult;
    }

    /**
     * construit xml retour.
     *
     * @param int    $idFichier l'id du pli concerne
     * @param logger $logger
     *
     * @return string xml contenant les infos du pli et des blocs
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function doTraitementChiffrement($idFichier, $logger)
    {
        $erreurInsertBlob = null;
        $messageErreurInsertBlob = null;
        $logger->info('Debut chifrement idFichier '.$idFichier);
        if (!empty($_POST['organisme']) && !empty($_POST['numeroOrdreBloc']) && !empty($_FILES['bloc'])) {
            $logger->info("La recuperation du fichier dont l'id = ".$idFichier);
            $pli = (new Atexo_FichierEnveloppe())->getFichierEnveloppeById($idFichier, $_POST['organisme']);
            if ($pli instanceof CommonFichierEnveloppe) {
                $logger->info('le fichier existe');
                $offre = $pli->getCommonOffres();
                if ($offre instanceof CommonOffres) {
                    if ($offre->getStatutOffres() == Atexo_Config::getParameter('STATUT_ENV_FERMETURE_EN_COURS')
                        ||
                        $offre->getStatutOffres() == Atexo_Config::getParameter('STATUT_ENV_CHIFFREMENT_EN_COURS')
                    ) {
                        $infile = Atexo_Config::getParameter('COMMON_TMP').'/bloc_'.$idFichier.$_POST['organisme'].$_POST['numeroOrdreBloc'].time().uniqid();
                        $idBlob = 0;
                        $logger->info('Traitement fichier  idFichier '.$idFichier.' tmp_name bloc : '.$_FILES['bloc']['tmp_name'].' size bloc : '.$_FILES['bloc']['size']);
                        $logger->info('Copier le bloc vers infile');
                        if (move_uploaded_file($_FILES['bloc']['tmp_name'], $infile)) {
                            $doInsertBlob = true;
                            $erreurInsertBlob = false;
                            if (0 == filesize($infile)) {
                                $doInsertBlob = false;
                                $logger->error(' Erreur size bloc 0 idFichier '.$idFichier.' hashBloc '.$_POST['hashBloc'].' infile '.$infile);
                            }

                            if ($doInsertBlob) {
                                $logger->info("L'insertion du blob pour le fichier ".$idFichier.' organisme '.$_POST['organisme']);
                                $idBlob = Atexo_FichierEnveloppe::insertBlocChiffre($idFichier, $_POST['organisme'], $_POST['numeroOrdreBloc'], $infile);
                                $logger->info('chifrement insertion idFichier '.$idFichier.' path : '.$infile.' dans idBlob : '.$idBlob);
                                if ($idBlob) {
                                    $pathFileBlob = Atexo_Blob::getPathFile($idBlob, $_POST['organisme']);
                                    if (0 == filesize($pathFileBlob)) {
                                        $erreurInsertBlob = true;
                                        $messageErreurInsertBlob = "une erreur s'est produite lors de l'insertion du bloc : Erreur size bloc 0 idFichier ".$idFichier.' pathFileBlob '.$pathFileBlob;
                                        $logger->error(' Erreur size bloc 0 idFichier '.$idFichier.' hashBloc '.$_POST['hashBloc'].' pathFileBlob '.$pathFileBlob);
                                    } else {
                                        $hash256 = hash_file('sha256', $pathFileBlob);
                                        if (isset($_POST['hashBloc']) && 0 != strcasecmp($_POST['hashBloc'], $hash256)) {
                                            $erreurInsertBlob = true;
                                            $messageErreurInsertBlob = "une erreur s'est produite lors de l'insertion du bloc : le hash envoyé n est pas conforme ou hash calculé par MPE";
                                            $logger->error('Erreur hash fichier idFichier '.$idFichier.'  pathFileBlob => '.$pathFileBlob.' hashBloc '.$_POST['hashBloc'].' sha256 '.$hash256);
                                        }
                                    }
                                }
                            }
                        }
                        if ($erreurInsertBlob) {
                            $logger->error($messageErreurInsertBlob);
                            $this->getHeaders('500');
                            $xmlResult = static::generateXmlError($messageErreurInsertBlob);
                        } elseif ($idBlob) {
                            $xml = $this->getXmlReponseChiffrement($idFichier, $idBlob);
                            $xmlResult = (new Atexo_Rest_Action_Actions())->encapsulerNodeResponseWsMpe($xml, 'blocPli');
                            $logger->info('Fin chifrement xml retour '.$xmlResult);
                            $this->getHeaders('200');
                        } else {
                            $logger->error("une erreur s'est produite lors de l'insertion du bloc Post ".print_r($_POST, true).' Files '.print_r($_FILES, true));
                            $this->getHeaders('500');
                            $xmlResult = static::generateXmlError("une erreur s'est produite lors de l'insertion du bloc");
                        }
                    } else {
                        $erreur = "Statut de l'offre est invalide : ".$offre->getStatutOffres()." pour l'id fichier ".$idFichier.'. les statuts autorises sont : '.
                            Atexo_Config::getParameter('STATUT_ENV_FERMETURE_EN_COURS').' et '.Atexo_Config::getParameter('STATUT_ENV_CHIFFREMENT_EN_COURS');
                        $logger->error($erreur);
                        $this->getHeaders('400');
                        $xmlResult = static::generateXmlError($erreur);
                    }
                } else {
                    $logger->error("l'offre du fichier n'existe pas :  contenu du Post".print_r($_POST, true).' Files '.print_r($_FILES, true));
                    $this->getHeaders('400');
                    $xmlResult = static::generateXmlError('le parametre idFichier ou organisme est incorrect');
                }
            } else {
                $logger->error('le parametre idFichier ou organisme est incorrect :  contenu du Post'.print_r($_POST, true).' Files '.print_r($_FILES, true));
                $this->getHeaders('400');
                $xmlResult = static::generateXmlError('le parametre idFichier ou organisme est incorrect');
            }
        } else {
            $logger->error("l'un ou plusiers des parametres est manquant:  organisme, numeroOrdreBloc ou bloc ".print_r($_POST, true).' Files '.print_r($_FILES, true));
            $this->getHeaders('400');
            $xmlResult = static::generateXmlError("l'un ou plusiers des parametres est manquant:  organisme, numeroOrdreBloc ou bloc");
        }

        return $xmlResult;
    }

    /**
     * construit xml retour.
     *
     * @param int $idFichier l'id du pli concerne
     * @param int $idBloc    l'id du bloc inseré
     *
     * @return string xml contenant les infos du pli et des blocs
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function getXmlReponseChiffrement($idFichier, $idBloc)
    {
        $return = <<<XML
<blocPli>
	<idBloc>$idBloc</idBloc>
	<nomBloc>$idBloc-0</nomBloc>
	<idFichier>$idFichier</idFichier>
</blocPli>
XML;

        return $return;
    }

    /**
     * construit xml retour.
     *
     * @param int    $idFichier l'id du pli concerne
     * @param logger $logger
     *
     * @return string xml contenant les infos du pli et des blocs
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function doTraitementDechiffrement($idFichier, $logger)
    {
        if (isset($_FILES['file']) && !empty($_FILES['file'])) {
            $logger->info("Debut dechifrement du fichier dont l'id ".$idFichier);
            $fichierEnveloppe = (new Atexo_FichierEnveloppe())->getFichierEnveloppeById($idFichier);
            if ($fichierEnveloppe instanceof CommonFichierEnveloppe) {
                $logger->info('Fichier existe');
                do {
                    $infile = Atexo_Config::getParameter('COMMON_TMP').'/file_dechiffre_'.$idFichier.time().uniqid().random_int(0, mt_getrandmax());
                } while (is_file($infile));
                $insere = false;
                $logger->info('Copier le bloc vers infile');
                if (move_uploaded_file($_FILES['file']['tmp_name'], $infile)) {
                    $insere = Atexo_FichierEnveloppe::insertFichierDeciffre($fichierEnveloppe, $infile);
                }
                if ($insere) {
                    $xmlResult = Atexo_Rest_Action_Actions::generateXmlResponse('OK');
                    $logger->info('Fin dechifrement xml retour '.$xmlResult);
                    $this->getHeaders('200');
                } else {
                    $logger->error("une erreur s'est produite lors de l'insertion du fichier ".print_r($_FILES, true).' idFichier : '.$idFichier.' organisme : '.$fichierEnveloppe->getOrganisme());
                    $this->getHeaders('500');
                    $xmlResult = static::generateXmlError("une erreur s'est produite lors de l'insertion du fichier");
                }
            } else {
                $logger->error('le parametre idFichier est incorrect '.$idFichier);
                $this->getHeaders('400');
                $xmlResult = static::generateXmlError('le parametre idFichier est incorrect');
            }
        } else {
            $logger->error("aucun fichier n'est joint à la requête ".print_r($_FILES, true).' idFichier : '.$idFichier);
            $this->getHeaders('400');
            $xmlResult = static::generateXmlError("aucun fichier n'est joint à la requête");
        }

        return $xmlResult;
    }

    /**
     * @param $id
     * @param $organisme
     * @param $serviceId
     *
     * @return bool
     *
     * @throws Atexo_Config_Exception
     * @throws PropelException
     */
    protected function verifieAccesToInfoPli($id, &$organisme, &$serviceId)
    {
        $rs = (new Atexo_Consultation())->getReferenceConsultationByIdFichierEnveloppe($id, $organisme);
        $consultationId = $rs['id'];
        $organisme = ($rs['organisme']) ?? null;
        if ($consultationId && $organisme) {
            $consultation = (new Atexo_Consultation())->getConsultationByReference($consultationId, $organisme);
            if ($consultation instanceof CommonConsultation) {
                $organisme = $consultation->getOrganisme();
                $serviceId = $consultation->getServiceId();

                return $this->verifieAccesToInfo($_GET['ticket'], $organisme, $serviceId);
            }
        }

        return false;
    }
}
