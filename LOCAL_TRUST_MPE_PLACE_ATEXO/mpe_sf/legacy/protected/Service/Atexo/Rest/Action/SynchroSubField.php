<?php

namespace Application\Service\Atexo\Rest\Action;

/**
 * Contient des attributs de synchronisation avec sub.
 *
 * @author LEZ <loubna.ezziani@atexo.com>
 * @copyright Atexo 2015
 *
 * @version 1.0
 *
 * @since 4.9.0
 */
class Atexo_Rest_Action_SynchroSubField
{
    protected $_idCleExterneDispositifCandidature;
    protected $_idCleExterneDispositifOffre;

    /**
     * Set the value of [_secours] column.
     *
     * @param string $v new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function setIdCleExterneDispositifCandidature($v)
    {
        $this->_idCleExterneDispositifCandidature = $v;
    }

    // setIdCleExterneDispositifCandidature()

    /**
     * Get the value of [_idCleExterneDispositifCandidature] column.
     *
     * @return int idCleExterneDispositifCandidature
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getIdCleExterneDispositifCandidature()
    {
        return $this->_idCleExterneDispositifCandidature;
    }

    // getIdCleExterneDispositifCandidature()

    /**
     * Set the value of [_idCleExterneDispositifOffre] column.
     *
     * @param string $v new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function setIdCleExterneDispositifOffre($v)
    {
        $this->_idCleExterneDispositifOffre = $v;
    }

    // setIdCleExterneDispositifCandidature()

    /**
     * Get the value of [_idCleExterneDispositifOffre] column.
     *
     * @return int idCleExterneDispositifOffre
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getIdCleExterneDispositifOffre()
    {
        return $this->_idCleExterneDispositifOffre;
    }

    // getIdCleExterneDispositifOffre()
}
