<?php

namespace Application\Service\Atexo\Rest\Action;

use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonAgentQuery;
use Application\Propel\Mpe\CommonAgentTechniqueTokenQuery;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationArchive;
use Application\Propel\Mpe\CommonConsultationArchiveQuery;
use Application\Propel\Mpe\CommonConsultationQuery;
use Application\Propel\Mpe\CommonHabilitationAgentWsQuery;
use Application\Propel\Mpe\CommonOrganisme;
use Application\Propel\Mpe\CommonRPA;
use Application\Propel\Mpe\CommonService;
use Application\Propel\Mpe\CommonServicePeer;
use Application\Propel\Mpe\CommonServiceQuery;
use Application\Propel\Mpe\CommonWebServiceQuery;
use Application\Service\Atexo\Atexo_ArchivageExterne;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Files;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_TiersAuthentification;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Config\Atexo_Config_Exception;
use Application\Service\Atexo\Controller\Atexo_Controller_Front;
use Application\Service\Atexo\EntityPurchase\Atexo_EntityPurchase_RPA;
use Application\Service\Atexo\Oversight\Atexo_Oversight_Oversight;
use Application\Service\Atexo\Rest\ObjectField\Atexo_Rest_ObjectField_DocumentField;
use Application\Service\Atexo\Serializer\Atexo_Serializer_Serializer;
use DOMDocument;
use Exception;

/*
 * contient les fonctions utiles pour les ws MPE
 *
 * @author ASO <ayoub.souidahmed@atexo.com>
 * @version 1.0
 * @since 4.6.0
 * @copyright Atexo 2014
 * @package Atexo_Rest_Action
 */

class Atexo_Rest_Action_Actions
{
    public ?string $currentAgentTechniqueLogin = null;
    public ?string $currentAgentTechniqueOrganisme = null;
    public ?int $currentAgentTechniqueService = null;
    public $currentWs;

    /*
     * verifie la validité du ticket passé en parametre
     *
     * @param string $ticket,le ticket(sso) à verifier
     * @param string $fonctionnalite,l'id du fonctionnalité demandée
     * @param string $acronymeOrg
     * @return boolean
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @version 1.0
     * @since 4.6.0
     * @copyright Atexo 2014
     */
    public static function generateXmlSuccess($message = null)
    {
        return self::generateXmlResponse('OK', $message, false);
    }

    /**
     * Permet de générer un message d'erreur personnalisé lorsque d'un accès interdit pour un organisme/service.
     *
     * @return string
     */
    public function getMessageErrorForAccessDenied($organisme = null, $service = null)
    {
        $message = "L’application $this->currentAgentTechniqueLogin n’est pas habilité à consommer le WS $this->currentWs";

        if (!empty($organisme)) {
            $message = "L'application $this->currentAgentTechniqueLogin ne peut pas utiliser le WS $this->currentWs sur l’organisme $organisme";
            if (!is_null($service)) {
                $message .= " / service $service.";
            }
        }

        return $message;
    }

    /*
     * generer un xml d'invalidité du ticket
     *
     * @param string $ticket,le ticket(sso)
     * @return DOMDocument $domDocument xml d'invalidité du ticket
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @version 1.0
     * @since 4.6.0
     * @copyright Atexo 2014
     */

    public static function generateXmlResponse($statut, $message = null, $erreur = null)
    {
        $xml = '<?xml version="1.0" encoding="UTF-8" ?>';
        $xml .= '<mpe xmlns="http://www.atexo.com/epm/xml" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" '.
            'xsi:schemaLocation="http://www.atexo.com/epm/xml ATEXO%20-%20LOCAL%20TRUST%20MPE%20-%20WebServices.xsd">'.
            '<reponse statutReponse="'.$statut.'">';
        if ($message && null !== $erreur) {
            if (true === $erreur) {
                $xml .= '<messageErreur>'.$message.'</messageErreur>';
            } elseif (false === $erreur) {
                $xml .= '<message>'.$message.'</message>';
            }
        }
        $xml .= '</reponse> </mpe>';

        return Atexo_Util::toUtf8($xml);
    }

    /*
     * generer un xml d'erreur
     *
     * @param string $id,l'id de la ressource concerné par l'erreur
     * @param array $erreurs,tableau des erreurs
     * @return DOMDocument $domDocument xml d'erreur
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @version 1.0
     * @since 4.6.0
     * @copyright Atexo 2014
     */

    public function erreurTicket($ticket)
    {
        //TODO message erreur et type erreur

        $messageErreur = 'Ticket invalide';

        return self::erreur($ticket, ['500', 'Internal Server Error', $messageErreur, '006']);
    }

    /*
     * generer un xml d'erreur de fonction invoquée non suportée
     *
     * @return DOMDocument $domDocument xml d'erreur de fonction invoquée non suportée
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @version  1.0
     * @since 4.6.0
     * @copyright Atexo 2014
     */

    public function erreur($id = null, $erreurs = [])
    {
        $domDocument = new DOMDocument('1.0', 'UTF-8');
        $domDocument->xmlStandalone = true;

        $error = $domDocument->createElement('error', '');
        $domDocument->appendChild($error);

        $attribute = $domDocument->createAttribute('xmlns:xsi');
        $error->appendChild($attribute);
        $attributeValue = $domDocument->createTextNode('http://www.w3.org/2001/XMLSchema-instance');
        $attribute->appendChild($attributeValue);

        $attribute = $domDocument->createAttribute('xsi:noNamespaceSchemaLocation');
        $error->appendChild($attribute);
        $attributeValue = $domDocument->createTextNode('error.xsd');
        $attribute->appendChild($attributeValue);

        if (isset($erreurs[4])) {
            $classType = $domDocument->createElement('classType', utf8_encode($erreurs[4]));
            $error->appendChild($classType);
        }
        $errorCode = $domDocument->createElement('errorCode', utf8_encode($erreurs[3]));
        $error->appendChild($errorCode);

        $errorLabel = $domDocument->createElement('errorLabel', utf8_encode($erreurs[1]));
        $error->appendChild($errorLabel);

        $errorMessage = $domDocument->createElement('errorMessage', utf8_encode($erreurs[2]));
        $error->appendChild($errorMessage);

        $errorMessage = $domDocument->createElement('methodType', utf8_encode(strtoupper($_SERVER['REQUEST_METHOD'])));
        $error->appendChild($errorMessage);

        $ressourceId = $domDocument->createElement('resourceId', $id);
        $error->appendChild($ressourceId);

        header('HTTP/1.1 '.$erreurs[0].' '.$erreurs[1]);

        if ('405' != $erreurs[0]) {
            $domDocument->save(Atexo_Config::getParameter('LOG_DIR').'erreur_'.date('Ymd_H-i-s').'.xml');
        }

        return $domDocument;
    }

    public function unsupportedMethod()
    {
        return $this->erreur(null, ['405', 'Method Not Allowed', 'Method Not Allowed, use POST|PUT|DELETE']);
    }

    /*
     * verifie la validité d'un xml
     *
     * @param string $xml,la chaine xml a charger
     * @param string $schema,le schemaxsd
     * @return mixed boolean true en cas d'xml valid,string xml d'erreur si l'xml n'est pas valid
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @version 1.0
     * @since 4.6.0
     * @copyright Atexo 2014
     */

    public function isValid($xml, $schema = null)
    {
        if (empty($xml)) {
            $message = 'Bad Request : input was empty, no xml found';

            return self::erreur(null, ['400', 'Bad Request', $message, '001']);
        } else {
            $domDocument = new DOMDocument();
            $domDocument->loadXML($xml);

            $domDocument->save(Atexo_Config::getParameter('COMMON_TMP').'fichier_xml_socle_'.date('Ymd_H-i-s').'.xml');
            if (!$schema) {
                $schema = 'schema.xsd';
            }
            if (!$domDocument->schemaValidate(Atexo_Controller_Front::getRootPath().'/ressources/'.$schema)) {
                $message = "Bad Request : xml didn't match xsd";

                return self::erreur(null, ['400', 'Bad Request', $message, '001']);
            }
        }

        return true;
    }

    /*
     * ajoute l'entete xml pr ws MPE
     *
     * @param string $xmlResponse chaine xml
     * @param string $nodele nom du noeud a encapsuler
     * @return string $xmlResult chaine xml contennant le noeud passé en param avec l'entete xml Ws valide
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @version 1.0
     * @since 4.6.0
     * @copyright Atexo 2014
     */

    public function getNodeFromXml($xmlstring, $nameBalise)
    {
        $domDoc = new DomDocument();
        $domDoc->loadXML(Atexo_Util::toUtf8($xmlstring));
        $consElement = $domDoc->getElementsByTagName($nameBalise)->item(0);
        $consString = $domDoc->saveXML($consElement);

        return $consString;
    }

    /*
     * retourne un noeud de l'xml comme String
     *
     * @param string $xmlstring chaine xml
     * @param string $nameBalise le nom du noeud recuperer
     * @return string $xmlResult chaine xml du noeud passé en param
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @version 1.0
     * @since 4.6.0
     * @copyright Atexo 2014
     */

    public static function sendDataWithFile($arrayBlob, $xml, $encoding = 'binary', $contentType = 'application/octet-stream', $fileDefault = true)
    {
        $mime_boundary = md5(time());
        header('HTTP/1.1 200 OK');
        header('Content-Type: multipart/form-data; boundary='.$mime_boundary);
        self::getXmlforwsFiles($xml, $mime_boundary);

        do {
            $blob = array_shift($arrayBlob);
            if ($blob['content']) {
                $contentBlob = $blob['content'];
                $revision = $blob['revision'];
            } else {
                $atexoBlob = new Atexo_Blob();
                $blobInfo = $atexoBlob->return_blob_to_client($blob['idBlob'], $blob['organisme'], true);
                $contentBlob = $blobInfo['content'];
                $revision = $blobInfo['revision'];
            }
            $file = (!$fileDefault) ? $blob['file'] : $blob['idBlob'].'-'.$revision;
            $fileName = $blob['name'] ?: $file;

            $stream = "\r\n--".$mime_boundary."\r\nContent-Disposition: form-data; name=\"".$file.'"; filename="'.$fileName."\"\r\nContent-Type:  ".$contentType."\r\nContent-Transfer-Encoding: ".$encoding."\r\n\r\n".$contentBlob;
            echo $stream;
            flush();
            ob_flush();
            unset($blob);
        } while ((is_countable($arrayBlob) ? count($arrayBlob) : 0) > 0);

        $data = "\r\n--$mime_boundary--";
        echo $data;
        flush();
        ob_flush();

        return '';
    }

    /*
     * Permet de génerer l'xml de reponse du web service MPE
     *
     * @param string $statut statut de la reponse (OK|KO)
     * @param string $message message d'erreur en cas de $statut=KO
     * @return string $xml xml reponse
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @version 1.0
     * @since 4.6.0
     * @copyright Atexo 2014
     */

    /**
     * permet d'afficher un xml pour le ws.
     *
     * @param $xml
     * @param $mime_boundary
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public static function getXmlforwsFiles($xml, $mime_boundary)
    {
        $xmlContent = "\r\n--".$mime_boundary."\r\nContent-Disposition: form-data; name=\"xmlResponse\"\r\nContent-Type:  application/xml\r\n\r\n".$xml;
        echo $xmlContent;
        flush();
        ob_flush();
    }

    /*
     * Permet de génerer l'xml de succes pour le web service MPE
     *
     * @return string $xml xml reponse succes
     * @author  ASO <ayoub.souidahmed@atexo.com>
     * @version 1.0
     * @since 4.6.0
     * @copyright Atexo 2014
     */
    /**
     * Permet d'envoyer le contenu d'un fichier sous format du bloc via web service MPE(multipart),xml + fichier.
     *
     * @param $arrayBlob tableau de $blob
     * @param $xml xml reponse
     * @param string    $encoding
     * @param string    $contentType
     *
     * @return string
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     * @copyright Atexo 2016
     */
    public function sendDataWithFileBlocs($arrayBlob, $xml, $encoding = 'binary', $contentType = 'application/octet-stream', bool $fileDefault = true)
    {
        $mime_boundary = md5(time());
        header('HTTP/1.1 200 OK');
        header('Content-Type: multipart/form-data; boundary='.$mime_boundary);
        self::getXmlforwsFiles($xml, $mime_boundary);

        do {
            $blob = array_shift($arrayBlob);
            $file = (!$fileDefault) ? $blob['file'] : false;
            if ($blob['content']) {
                $contentBlob = $blob['content'];
                $stream = "\r\n--".$mime_boundary."\r\nContent-Disposition: form-data; name=\"".$file.'"; filename="'.$blob['name']."\"\r\nContent-Type:  ".$contentType."\r\nContent-Transfer-Encoding: ".$encoding."\r\n\r\n".$contentBlob;
                echo $stream;
            } else {
                $atexoBlob = new Atexo_Blob();
                $atexoBlob->getBlocsBlobForWs($blob['idBlob'], $blob['organisme'], $mime_boundary, $file, $blob['name'], $contentType, $encoding);
            }

            flush();
            ob_flush();
            unset($blob);
        } while ((is_countable($arrayBlob) ? count($arrayBlob) : 0) > 0);

        $data = "\r\n--$mime_boundary--";
        echo $data;
        flush();
        ob_flush();

        return '';
    }

    /*
     * Permet de génerer l'xml d'erreur pour le web service MPE
     *
     * @param string $message message d'erreur
     * @return string $xml xml reponse Error
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @version 1.0
     * @since 4.6.0
     * @copyright Atexo 2014
     */

    public function validateTicket($logger)
    {
        $access = true;
        $token = $_GET['ticket'];
        if (self::isSession()) {
            return $access;
        }

        if (!self::isTokenValide(Atexo_Util::atexoHtmlEntities($token))) {
            $this->getHeaders('401');
            $msgToLog = 'Le ticket est invalide : '.Atexo_Util::atexoHtmlEntities($_GET['ticket']);
            $logger->error($msgToLog);

            return self::generateXmlError($msgToLog);
        }

        $access = self::isGranted($token, $logger);

        return $access;
    }

    /*
     * recupere le header
     *
     * @param Integer $code le code http
     * @param string $contentTypele contentType
     * @return http header
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @version 1.0
     * @since 4.6.0
     * @copyright Atexo 2014
     */

    /**
     * @return bool
     */
    public function isSession()
    {
        $auth = false;
        if (Atexo_CurrentUser::getId()) {
            $auth = true;
        }

        return $auth;
    }

    public function isTokenValide($ticket)
    {
        return (new Atexo_TiersAuthentification())->VerifyToken($ticket);
    }

    /*
    * Permet d'envoyer le contenu de la reponse du web service MPE(multipart),xml + fichier
    *
    * @param array $arrayblob tableau de $blob array(name=> nom du fichier, content=>contenu du fichier)
    * @param string $xml xml reponse
    * @return void
    * @author ASO <ayoub.souidahmed@atexo.com>
    * @version 1.0
    * @since 4.6.0
    * @copyright Atexo 2014
    */

    public function getHeaders($code, $contentType = '')
    {
        header('HTTP/1.1 '.$code, true, $code);
        if (!$contentType) {
            $contentType = 'application/xml';
        }
        header('Content-type: '.$contentType);
    }

    public static function generateXmlError($message = null)
    {
        return self::generateXmlResponse('KO', $message, true);
    }

    /*
     * Permet de verifier la validation d'un ticket(sso_tiers)
     *
     * @param string $logger objet du Atexo_LoggerManager
     * @param string $acronymeOrg
     * @return boolean true or false
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @version 1.0
     * @since 4.6.0
     * @copyright Atexo 2014
     */
    /**
     * @param $token
     * @param $logger
     * @param null $connexion
     *
     *
     * @throws Atexo_Config_Exception
     * @throws PropelException
     */
    public function isGranted($token, $logger, $connexion = null): bool|string
    {
        if (null === $connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').
                Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
        $agentTechniqueToken = (new CommonAgentTechniqueTokenQuery())->findOneByToken($token);
        $agentId = $agentTechniqueToken->getAgentId();

        $agent = (new CommonAgentQuery())
            ->filterById($agentId)
            ->findOne($connexion);

        $path = self::getRoute();

        $webservice = (new CommonWebServiceQuery())->findOneByArray([
            'methodWs' => $_SERVER['REQUEST_METHOD'],
            'routeWs' => $path,
        ]);

        if (empty($webservice)) {
            return false;
        }

        $habilitationAgentWs = (new CommonHabilitationAgentWsQuery())->findOneByArray([
            'agentId' => $agent->getId(),
            'webServiceId' => $webservice->getId(),
        ]);

        $this->currentAgentTechniqueLogin = $agent->getLogin();
        $this->currentWs = $webservice->getNomWs();
        $this->currentAgentTechniqueOrganisme = $agent->getOrganisme();
        $this->currentAgentTechniqueService = $agent->getServiceId();

        if ((true === $agent->getTechnique() && $habilitationAgentWs)
            || false === $webservice->getUniquementTechnique()) {
            $granted = true;
        } else {
            $this->getHeaders('403');
            $msgToLog = $this->getMessageErrorForAccessDenied();
            $logger->error($msgToLog);
            $granted = self::generateXmlError($msgToLog);
        }

        return $granted;
    }

    private function getRoute()
    {
        $request_method = $_SERVER['REDIRECT_REQUEST_METHOD'] ?? $_SERVER['REQUEST_METHOD'];
        $request_method = strtolower($request_method);
        $path_info = $_SERVER['REQUEST_URI'];

        $discovered_handler = null;
        $regex_matches = [];
        $finalPattern = null;
        $routes = [
            '/api.php/ws/authentification/connexion/:alpha/:alpha' => 'Atexo_Rest_Action_Authentification',
            '/api.php/ws/annonceservice/create?ticket=:alpha' => 'Atexo_Rest_Action_AnnonceService',
            '/api.php/ws/annonceservice/update?ticket=:alpha' => 'Atexo_Rest_Action_AnnonceService',
            '/api.php/ws/annonceservice/get/:alpha/:number?ticket=:alpha' => 'Atexo_Rest_Action_AnnonceService',
            '/api.php/ws/consultation/create?ticket=:alpha' => 'Atexo_Rest_Action_Consultation',
            '/api.php/ws/consultation/update?ticket=:alpha' => 'Atexo_Rest_Action_Consultation',
            '/api.php/ws/consultation/get/:alpha/:number?ticket=:alpha' => 'Atexo_Rest_Action_Consultation',
            '/api.php/ws/tableaudebord?ticket=:alpha' => 'Atexo_Rest_Action_TableauDeBord',
            '/api.php/ws/registre/questions/:number?ticket=:alpha' => 'Atexo_Rest_Action_RegistreQuestions',
            '/api.php/ws/registre/question/:number?ticket=:alpha' => 'Atexo_Rest_Action_RegistreQuestion',
            '/api.php/ws/registre/retraitdces/:number?ticket=:alpha' => 'Atexo_Rest_Action_RegistreRetraitDces',
            '/api.php/ws/registre/retraitdce/:number?ticket=:alpha' => 'Atexo_Rest_Action_RegistreRetraitDce',
            '/api.php/ws/registre/depots/:number?ticket=:alpha' => 'Atexo_Rest_Action_RegistreDepots',
            '/api.php/ws/registre/depot/:number?ticket=:alpha' => 'Atexo_Rest_Action_RegistreDepot',
            '/api.php/ws/registre/depot/update?ticket=:alpha' => 'Atexo_Rest_Action_RegistreDepot',
            '/api.php/ws/registre/pli/:number?ticket=:alpha' => 'Atexo_Rest_Action_Pli',
            '/api.php/ws/registre/pli/:string/:number?ticket=:alpha' => 'Atexo_Rest_Action_Pli',
            '/api.php/ws/contactsBySiret/get/:number?ticket=:alpha' => 'Atexo_Rest_Action_ContactsSiret',
            '/api.php/ws/contactsByEntrepriseId/get/:number?ticket=:alpha' => 'Atexo_Rest_Action_ContactsIdEse',
            '/api.php/ws/entrepriseByRaisonSociale/get/:base64?ticket=:alpha' => 'Atexo_Rest_Action_EntrepriseByRaisonSociale',
            '/api.php/ws/entrepriseBySiren/get/:number?ticket=:alpha' => 'Atexo_Rest_Action_EntrepriseBySiren',
            '/api.php/ws/archive/listeid/:number?ticket=:alpha' => 'Atexo_Rest_Action_Archives',
            '/api.php/ws/archive/fichier/:alpha/:number?ticket=:alpha' => 'Atexo_Rest_Action_Archive',
            '/api.php/ws/organismes?ticket=:alpha' => 'Atexo_Rest_Action_Organismes',
            '/api.php/ws/organismes?ticket=:alpha&withLogo=:alpha' => 'Atexo_Rest_Action_Organismes',
            '/api.php/ws/organisme/:alpha?ticket=:alpha' => 'Atexo_Rest_Action_Organisme',
            '/api.php/ws/organisme/:alpha?ticket=:alpha&withLogo=:alpha' => 'Atexo_Rest_Action_Organisme',
            '/api.php/ws/document/:alpha/:alpha/:alpha?ticket=:alpha' => 'Atexo_Rest_Action_Document',
            '/api.php/ws/document/:alpha/:alpha/:alpha/:alpha?ticket=:alpha' => 'Atexo_Rest_Action_Document',
            '/api.php/ws/document/:alpha/:alpha/:alpha/:alpha/:alpha?ticket=:alpha' => 'Atexo_Rest_Action_Document',
            '/api.php/ws/depot/admissibilite/:alpha/:alpha/:number?ticket=:alpha' => 'Atexo_Rest_Action_Admissibilite',
            '/api.php/ws/depot/decision/:alpha/:alpha/:number?ticket=:alpha' => 'Atexo_Rest_Action_Decision',
            '/api.php/ws/offre/admissibilite/:alpha/:alpha/:number?ticket=:alpha' => 'Atexo_Rest_Action_Admissibilite',
            '/api.php/ws/offre/decision/:alpha/:alpha/:number?ticket=:alpha' => 'Atexo_Rest_Action_Decision',
            '/api.php/ws/registre/echanges/:alpha/:number?ticket=:alpha' => 'Atexo_Rest_Action_Echanges',
            '/api.php/ws/registre/echange/:alpha/:number?ticket=:alpha' => 'Atexo_Rest_Action_Echange',
            '/api.php/ws/fichier/:alpha/:number/:alpha?ticket=:alpha' => 'Atexo_Rest_Action_Fichier',
            '/api.php/ws/fichier/:alpha/:number/:alpha/:number?ticket=:alpha' => 'Atexo_Rest_Action_Fichier',
            '/api.php/ws/document/:alpha/:alpha/:alpha/:alpha?ticket=:alpha&typeChecksum=:alpha' => 'Atexo_Rest_Action_Document',
            '/api.php/ws/document/:alpha/:alpha/:alpha?ticket=:alpha&typeChecksum=:alpha' => 'Atexo_Rest_Action_Document',
            '/api.php/ws/document/:alpha/:alpha/:alpha?ticket=:alpha&chiffrer=:alpha' => 'Atexo_Rest_Action_Document',
            '/api.php/ws/document/:alpha/:alpha/:alpha?ticket=:alpha&chiffrer=:alpha&withFile=:alpha' => 'Atexo_Rest_Action_Document',
            '/api.php/ws/document/:alpha/:alpha/:alpha/:alpha?ticket=:alpha&withFile=:alpha' => 'Atexo_Rest_Action_Document',
            '/api.php/ws/document/:alpha/:alpha/:alpha?ticket=:alpha&withFile=:alpha' => 'Atexo_Rest_Action_Document',
            '/api.php/ws/document/:alpha/:alpha/:alpha?ticket=:alpha&typeChecksum=:alpha&chiffrer=:alpha' => 'Atexo_Rest_Action_Document',
            '/api.php/ws/document/:alpha/:alpha/:alpha?ticket=:alpha&typeChecksum=:alpha&chiffrer=:alpha&withFile=:alpha' => 'Atexo_Rest_Action_Document',
            '/api.php/ws/document/:alpha/:alpha/:alpha/:alpha?ticket=:alpha&typeChecksum=:alpha&withFile=:alpha' => 'Atexo_Rest_Action_Document',
            '/api.php/ws/document/:alpha/:alpha/:alpha?ticket=:alpha&typeChecksum=:alpha&withFile=:alpha' => 'Atexo_Rest_Action_Document',
            '/api.php/ws/document/synchro/:alpha/:all?type=:alpha' => 'Atexo_Rest_Action_DocumentsSynchro',
            '/api.php/ws/document/synchro/:alpha/:all/:alpha?type=:alpha' => 'Atexo_Rest_Action_DocumentsSynchro',
            '/api.php/ws/:alpha/:alpha?ticket=:alpha' => 'Atexo_Rest_Action_WebService',
        ];

        if (isset($routes[$path_info])) {
            $discovered_handler = $routes[$path_info];
        } elseif ($routes) {
            $tokens = [
                ':string' => '([a-zA-Z]+)',
                ':number' => '([0-9]+)',
                ':alpha' => '([a-zA-Z0-9-_]+)',
                ':base64' => '([a-zA-Z0-9-_=]+)',
                ':all' => '(.+)',
            ];

            foreach ($routes as $pattern => $handler_name) {
                $initPattern = $pattern;
                $pattern = strtr($pattern, $tokens);
                if (preg_match('#^/?'.str_replace('?', '', $pattern).'/?$#', str_replace('?', '', $path_info), $matches)) {
                    $discovered_handler = $handler_name;
                    $regex_matches = $matches;
                    $finalPattern = $initPattern;
                    break;
                }
            }
        }

        $explode = explode('?', $finalPattern);
        $explode = explode(':', $explode[0]);

        return $explode[0];
    }

    /**
     * Permet de retourner l'xml d'info d'un organisme.
     *
     * @param CommonOrganisme $organisme  l'organisme
     * @param array           $arrayLogos tableau contenant les info des logs d'organisme
     * @param $logger le logger
     *
     * @return string l'xml
     *
     * @throws Atexo_Config_Exception
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function getXmlResultOrganisme($organisme, &$arrayLogos, $logger, $withLogo = true)
    {
        if ($organisme instanceof CommonOrganisme) {
            $xmlProperties = Atexo_Config::getParameter('PATH_FILE_MAPPING_ORGANISME');
            $xmlHiddenField = Atexo_Config::getParameter('PATH_FILE_HIDDEN_FIELD_ORGANISME');
            $listOptioanlField = Atexo_Config::getParameter('PATH_FILE_OPTIONAL_FIELD_ORGANISME');
            if ($withLogo) {
                $logo = (new Atexo_Organismes())->getLogoOrganismeInfo($organisme->getAcronyme(), $logger);
                $organisme->setLogo($logo);
                if ($logo instanceof Atexo_Rest_Action_LogoField) {
                    $content = file_get_contents($logo->getPathFile());
                    $file = 'file_'.$organisme->getAcronyme().'';
                    $arrayLogos[] = ['name' => $logo->getFilename(), 'file' => $file, 'content' => $content];
                }
            }
            $logger->info("La récupération de l'xml à partir de l'objet organisme ");
            $serilizer = new Atexo_Serializer_Serializer();
            $xmlResult = $serilizer->SerializeClass($organisme, 'CommonOrganisme', $xmlProperties, $xmlHiddenField, $listOptioanlField);
            $xmlResult = (new Atexo_Rest_Action_Actions())->encapsulerNodeResponseWsMpe($xmlResult, 'organisme');

            return $xmlResult;
        } else {
            throw new Exception(" L'organisme passer en paramettre est invalide");
        }
    }

    public function encapsulerNodeResponseWsMpe($xmlResponse, $node)
    {
        $entete = self::generateXmlResponse('OK');
        $doc = new DOMDocument();
        $doc->loadXML(Atexo_Util::toUtf8($xmlResponse));
        $node = $doc->getElementsByTagName("$node")->item(0);
        $newdoc = new DOMDocument('1.0', 'UTF-8');
        $newdoc->formatOutput = true;
        $newdoc->loadXML($entete);
        $node = $newdoc->importNode($node, true);
        $response = $newdoc->getElementsByTagName('reponse')->item(0);
        $response->appendChild($node);

        return Atexo_Util::toUtf8($newdoc->saveXML());
    }

    /*
     * genere un xml contenant les infos d'un DocumentFiled
     *
     * @param Atexo_Rest_ObjectField_DocumentField $documentField objet du DocumentField
     * @return string xmlcontenant les infos d'un document
     * @author LEZ <loubna.ezziani@atexo.com>
     * @version 1.0
     * @since esr-2015
     * @copyright Atexo 2015
     */

    public function getXmlInfosObjectField($documentField)
    {
        $serializer = new Atexo_Serializer_Serializer();
        $mappingFile = Atexo_Config::getParameter('PATH_FILE_MAPPING_DOCUMENT');
        $listOptioanlField = Atexo_Config::getParameter('PATH_FILE_OPTIONAL_FIELD_DOCUMENT');
        $xmlResponse = $serializer->SerializeClass($documentField, $documentField::class, $mappingFile, false, $listOptioanlField);

        return (new Atexo_Rest_Action_Actions())->encapsulerNodeResponseWsMpe($xmlResponse, 'document');
    }

    /**
     * genere un xml contenant les infos d'une archive.
     *
     * @param CommonConsultation  $consultation objet du CommonConsultation
     * @param Atexo_LoggerManager $logger       tracer les etapes
     *
     * @return array contenant les infos du fichier d'archive
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function getArchive($consultation, $logger)
    {
        $nomFichier = null;
        $cheminDirAnnee = null;
        $archive = [];
        $content = null;
        $acronymeOrganisme = $consultation->getOrganisme();
        $org = Atexo_Organismes::retrieveOrganismeByAcronyme($acronymeOrganisme);
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));

        if (Atexo_Config::getParameter('ORGANISE_ARCHIVE_PAR_ANNEE') && $consultation->getDateFin()) {
            $cheminDirAnnee = '/'.Atexo_Util::getAnneeFromDate($consultation->getDateFin());
        }
        if (Atexo_Module::isEnabled('InterfaceArchiveArcadePmi')) {
            $currentServiceId = Atexo_CurrentUser::getCurrentServiceId();
            if (!empty($currentServiceId)) {
                $rpa = (new Atexo_EntityPurchase_RPA())->getResponsabeArchiveByIdService($currentServiceId, $consultation->getOrganisme());
                if (($rpa instanceof CommonRPA) && $rpa->getOrganisme()) {
                    $nomFichier = $rpa->getOrganisme().'_';
                }
            }
        }

        //initialisation des chemins de stockage racine
        $cheminDirRac = Atexo_Config::getParameter('FICHIERS_ARCHIVE_DIR').$org->getAcronyme();
        if (Atexo_Config::getParameter('ORGANISE_ARCHIVE_PAR_ANNEE') && $consultation->getDateFin()) {
            $cheminDirAnnee = Atexo_Util::getAnneeFromDate($consultation->getDateFin());
        }

        //l'identifiant du service de rattachement de la consultation s'il n'existe pas alors vaut 0
        $consultationServiceId = $consultation->getServiceId() ?: 0;
        //initialisation du nom du fichier de l'archive
        $downloadedName = (new Atexo_Files())->getNomFileXmlAndZip($consultation, $consultationServiceId);

        //on vérifie si l'archive est stockée dans la table consultation_archive, que la fragmentation a bien eu lieu et que son état de transmission est finalisée
        $consultationArchive = (new CommonConsultationArchiveQuery())
            ->filterByOrganisme($acronymeOrganisme)
            ->filterByConsultationId($consultation->getId())
            ->findOne($connexion);
        //2 cas possibles et ils sont exclusifs : si elle figure dans la table consultation_archive
        // cas 1 : l'archive est à récupérer en local et il ne s'agit pas d'un téléchargement par lot, $cheminFichierZip est lu directement en BD (cas par défaut)
        // cas 2 : l'archive est à récupérer à distance et il ne s'agit pas d'un téléchargement par lot, l'archive est téléchargée à distance
        if ($consultationArchive instanceof CommonConsultationArchive) {
            //cas 1 : par défaut on indique que le chemin de l'archive est en local, la valeur est écrasée si on arrive à la récupérer depuis ATLAS
            $cheminFichierZip = Atexo_Config::getParameter('FICHIERS_ARCHIVE_DIR').$consultationArchive->getCheminFichier();

            //cas 2 : téléchargement de l'archive à distance et si non OK on se replie sur la cas 1
            if (Atexo_Module::isEnabled('ArchivageConsultationSaeExterneTelechargementArchive')) {
                $logger->info('Récupération du DAC depuis ATLAS, module ArchivageConsultationSaeExterneTelechargementArchive actif');

                //cas 1 : stockage des archives à distance de type ATLAS, on récupère l'archive à distance si la fragmentation a bien eu lieu et la transmission de tous les blocs finalisées
                if ($consultationArchive->getStatusFragmentation()
                    && ($consultationArchive->getStatusGlobalTransmission() == Atexo_Config::getParameter('ARCHIVE_TRANSMISSION_FINALISEE'))
                    && !Atexo_ArchivageExterne::getDACFromAtlas($consultationArchive, $cheminFichierZip)
                ) {
                    $logger->error('Erreur récupération DAC depuis ATLAS : fichier non complet');
                    //la tentative de récupération des archives n'a pas fonctionné, dans ce cas on récupére l'archive en local
                    $cheminFichierZip = Atexo_Config::getParameter('FICHIERS_ARCHIVE_DIR').$consultationArchive->getCheminFichier();
                    $logger->info('La récupération du DAC va se faire en local à travers le chemin : '.$cheminFichierZip);
                }
            }

            //si jamais l'archive existe (une fois récupérée depuis ATLAS ou bien en local), on l'envoi via le navigateur
            if (is_file($cheminFichierZip)) {
                $content = file_get_contents($cheminFichierZip);
                $pathFile = $cheminFichierZip;
            } else {
                $logger->error('Erreur telechargement DAC : Le dossier '.$cheminFichierZip." n'existe pas ");
            }
        } //soit l'archive ne figure pas dans la table consultation_archive, dans ce cas il faut construire dynamiquement le chemin avec l'ancienne méthode
        else {
            $logger->info("L'archive n'est pas stockée dans la table consultation_archive pour la consultation ref ".$consultation->getId());

            $sigleService = '';
            $cheminDirService = '';
            $pathZip = null;
            if ($consultation->getServiceId()) {
                $c = new Criteria();
                $c->add(CommonServicePeer::ID, $consultation->getServiceId());
                $c->add(CommonServicePeer::ORGANISME, $org->getAcronyme());
                $service = CommonServicePeer::doSelectOne($c, $connexion);
                if ($service) {
                    $sigleService = trim(Atexo_Util::OterAccents($service->getSigle()));
                    $listeParent = Atexo_EntityPurchase::getAllParents($service->getId(), $org->getAcronyme());
                    for ($index = count($listeParent) - 1; $index >= 0; --$index) {
                        if ($listeParent[$index]['id']) {
                            $cheminDirService .= '/'.(str_replace('/', '_', trim(Atexo_Util::OterAccents($listeParent[$index]['libelle']))));
                        }
                    }
                    $cheminDirService .= '/'.(str_replace('/', '_', trim(Atexo_Util::OterAccents($service->getSigle()))));
                }
            } else {
                $sigleService = trim(Atexo_Util::OterAccents($org->getSigle()));
            }

            $nomFichierArchive = (new Atexo_Files())->getOldNomFileXmlAndZip($consultation, $sigleService, $nomFichier);
            $nomFichierZip = $nomFichierArchive.'.zip';
            $cheminDir = $cheminDirRac.'/'.$cheminDirAnnee.'/'.$cheminDirService;
            if (!is_file($cheminDir.'/'.$nomFichierZip)) {
                $cheminDir = $cheminDirRac.'/'.$cheminDirService;
            }

            $downloadedName = $nomFichierZip;
            $cheminFile = $cheminDir.'/'.$nomFichierZip;
            if (is_file($cheminFile)) {
                $content = file_get_contents($cheminFile);
                $pathFile = $cheminFile;
            } else {
                $logger->error('Erreur telechargement DAC : Le dossier '.$cheminFile." n'existe pas ");
            }
        }

        if ($content) {
            $archive['file'] = $downloadedName;
            $archive['name'] = $downloadedName;
            $archive['content'] = $content;
            $archive['pathFile'] = $pathFile;
        }

        return $archive;
    }

    /**
     * genere un xml contenant les infos d'un service.
     *
     * @param CommonService       $service objet du CommonService
     * @param Atexo_LoggerManager $logger  tracer les etapes
     *
     * @return string xml contenant les infos du service
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-megalis
     *
     * @copyright Atexo 2016
     */
    public function getXmlResultService($service, $logger)
    {
        if ($service instanceof CommonService) {
            $xmlProperties = Atexo_Config::getParameter('PATH_FILE_MAPPING_SERVICE');
            $xmlHiddenField = Atexo_Config::getParameter('PATH_FILE_HIDDEN_FIELD_SERVICE');
            //$listOptioanlField = Atexo_Config::getParameter('PATH_FILE_OPTIONAL_FIELD_SERVICE');
            $logger->info("La récupération de l'xml à partir de l'objet service ");
            $serilizer = new Atexo_Serializer_Serializer();
            $xmlResult = $serilizer->SerializeClass($service, CommonService::class, $xmlProperties, $xmlHiddenField);
            $xmlResult = (new Atexo_Rest_Action_Actions())->encapsulerNodeResponseWsMpe($xmlResult, 'service');

            return $xmlResult;
        } else {
            throw new Exception(' Le service passer en paramettre est invalide');
        }
    }

    /**
     * @param array $params
     *
     * @return CommonEchangesInterfaces|string|null
     *
     * @throws Exception
     */
    public static function oversight($params = [])
    {
        return Atexo_Oversight_Oversight::oversight($params);
    }

    protected function processXml($xml)
    {
    }

    public function getRightIdConsultation($id, $org = null)
    {
        $idCons = null;
        if (!empty($id)) {
            $consQ = new CommonConsultationQuery();
            $consQ->filterByReference($id);
            if ($org) {
                $consQ->filterByOrganisme($org);
            }
            $cons = $consQ->findOne();
            if ($cons instanceof CommonConsultation) {
                $idCons = $cons->getId();
            }
        }

        return $idCons ?? $id;
    }

    /**
     * @param $token
     * @param $organisme
     * @param $service
     *
     * @return bool
     *
     * @throws Atexo_Config_Exception
     * @throws PropelException
     */
    protected function verifieAccesToInfo($token, $organisme, $service)
    {
        $serviceAndParent = [];
        if ('' !== $service) {
            $serviceAndParent = Atexo_EntityPurchase::getArParentsId($service, $organisme, true);
        }

        return (new Atexo_TiersAuthentification())->verifieAccesToInfo($token, $organisme, $serviceAndParent);
    }

    /**
     * @param $token
     *
     * @return array
     *
     * @throws Atexo_Config_Exception
     * @throws PropelException
     */
    protected function getAllOrganismesAllowed($token)
    {
        return (new Atexo_TiersAuthentification())->getAllOrganismesServicesAllowed($token);
    }

    public function getIdService($serviceId, $organisme)
    {
        $connexion = Propel::getConnection(
            Atexo_Config::getParameter('COMMON_DB') .
            Atexo_Config::getParameter('CONST_READ_ONLY')
        );

        $oldServiceId = null;
        $service = (new CommonServiceQuery())
            ->filterByOldId($serviceId)
            ->filterByOrganisme($organisme)
            ->findOne($connexion);

        if ($service instanceof CommonService) {
            $oldServiceId = $serviceId;
            $serviceId = $service->getId();
        }

        return [$serviceId, $oldServiceId];
    }

    public function getOldIdServiceByIdService($serviceId): ?int
    {
        $connexion = Propel::getConnection(
            Atexo_Config::getParameter('COMMON_DB') .
            Atexo_Config::getParameter('CONST_READ_ONLY')
        );
        $oldServiceId = null;
        $service = (new CommonServiceQuery())
            ->filterById($serviceId)
            ->findOne($connexion);

        if ($service instanceof CommonService) {
            $oldServiceId =  $service->getOldId() ?? $serviceId;
        }

        return $oldServiceId;
    }
}
