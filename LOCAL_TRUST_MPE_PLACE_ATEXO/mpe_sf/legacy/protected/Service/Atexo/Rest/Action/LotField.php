<?php

namespace Application\Service\Atexo\Rest\Action;

use App\Entity\Consultation\ClausesN1;
use App\Service\Consultation\ConsultationService;
use App\Service\LotService;
use Application\Propel\Mpe\CommonCategorieLot;
use Application\Propel\Mpe\CommonConsultation;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CPV;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Category;
use stdClass;

/**
 * Contient des attributs et des redeclaration des methodes pour CommonCategorieLot.
 *
 * @author LEZ <loubna.ezziani@atexo.com>
 *
 * @version 1.0
 *
 * @since 4.6.0
 *
 * @copyright Atexo 2014
 */
class Atexo_Rest_Action_LotField extends CommonCategorieLot
{
    /**
     * Permet d'initialiser les valeurs de l'objet Atexo_Rest_Action_LotField.
     *
     * @param CommonCategorieLot $lot optionnel le lot d'ou on va copier les valeurs
     *
     * @return Atexo_Rest_Action_LotField une instance de ce objet
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function __construct($lot = null)
    {
        if ($lot instanceof CommonCategorieLot) {
            $lot->copyInto($this);
            $this->id = $lot->getId();
            $this->setObejtCpv(Atexo_Config::getParameter('MAX_CPV_ANNEXE'));
        }
    }

    /**
     * Get the value of [_libelleCategorieLot] column.
     *
     * @return string LibelleCategorieLot
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getLibelleCategorieLot()
    {
        $v = parent::getLibelleCategorieLot();

        return strtoupper($v);
    }

    /**
     * Set the value of [_codeLieu] column.
     *
     * @param string $v new value
     *
     * @return CommonCategorieLot The current object (for fluent API support)
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setlibelleCategorieLot($v)
    {
        $libelleCategorieLot = ucfirst(strtolower($v));
        $categorieLot = (new Atexo_Consultation_Category())->retrieveIdCategoryByLibelle($libelleCategorieLot);

        return parent::setCategorie($categorieLot);
    }

    /**
     * Set the value of [code_cp1,code_cpv2,cpv] column.
     *
     * @param Atexo_CPV $cpv new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setCpv($cpv)
    {
        if ($cpv instanceof Atexo_CPV) {
            $this->setCodeCpv1($cpv->getCodePrincipal());
            $codeCpv2 = '';
            if ($cpv->getCodeSecondaire1()) {
                $codeCpv2 .= $cpv->getCodeSecondaire1().'#';
            }
            if ($cpv->getCodeSecondaire2()) {
                $codeCpv2 .= $cpv->getCodeSecondaire2().'#';
            }
            if ($cpv->getCodeSecondaire3()) {
                $codeCpv2 .= $cpv->getCodeSecondaire3().'#';
            }
            $this->setCodeCpv2($codeCpv2);
            parent::setCpv($cpv);
        }
    }

    /**
     * Set the value of [clause_sociale_condition_execution] column.
     *
     * @param string $v new value
     *
     * @return CommonCategorieLot The current object (for fluent API support)
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setClauseSocialeConditionExecution($v)
    {
        $v = Atexo_Util::getIntFromValboolean(strtolower($v));

        return parent::setClauseSocialeConditionExecution($v);
    }

    // setClauseSocialeConditionExecution()

    /**
     * Set the value of [clause_sociale_insertion] column.
     *
     * @param string $v new value
     *
     * @return CommonCategorieLot The current object (for fluent API support)
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setClauseSocialeInsertion($v)
    {
        $v = Atexo_Util::getIntFromValboolean(strtolower($v));

        return parent::setClauseSocialeInsertion($v);
    }

    // setClauseSocialeInsertion()

    /**
     * Set the value of [clause_sociale_ateliers_proteges] column.
     *
     * @param string $v new value
     *
     * @return CommonCategorieLot The current object (for fluent API support)
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setClauseSocialeAteliersProteges($v)
    {
        $v = Atexo_Util::getIntFromValboolean(strtolower($v));

        return parent::setClauseSocialeAteliersProteges($v);
    }

    // setClauseSocialeAteliersProteges()

    /**
     * Set the value of [clause_sociale_ateliers_proteges] column.
     *
     * @param string $v new value
     *
     * @return CommonConsultation The current object (for fluent API support)
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setClauseSocialeSiae($v)
    {
        $v = Atexo_Util::getIntFromValboolean(strtolower($v));

        return parent::setClauseSocialeSiae($v);
    }

    // setClauseSocialeSiae()

    /**
     * Set the value of [clause_sociale_ateliers_proteges] column.
     *
     * @param string $v new value
     *
     * @return CommonConsultation The current object (for fluent API support)
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setClauseSocialeEess($v)
    {
        $v = Atexo_Util::getIntFromValboolean(strtolower($v));

        return parent::setClauseSocialeEss($v);
    }

    // setClauseSocialeEss()

    /**
     * Set the value of [clause_env_specs_techniques] column.
     *
     * @param string $v new value
     *
     * @return CommonCategorieLot The current object (for fluent API support)
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setClauseEnvSpecsTechniques($v)
    {
        $v = Atexo_Util::getIntFromValboolean(strtolower($v));

        return parent::setClauseEnvSpecsTechniques($v);
    }

    // setClauseEnvSpecsTechniques()

    /**
     * Set the value of [clause_env_cond_execution] column.
     *
     * @param string $v new value
     *
     * @return CommonCategorieLot The current object (for fluent API support)
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setClauseEnvCondExecution($v)
    {
        $v = Atexo_Util::getIntFromValboolean(strtolower($v));

        return parent::setClauseEnvCondExecution($v);
    }

    // setClauseEnvCondExecution()

    /**
     * Set the value of [clause_env_criteres_select] column.
     *
     * @param string $v new value
     *
     * @return CommonCategorieLot The current object (for fluent API support)
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setClauseEnvCriteresSelect($v)
    {
        $v = Atexo_Util::getIntFromValboolean(strtolower($v));

        return parent::setClauseEnvCriteresSelect($v);
    }

    // setClauseEnvCriteresSelect()

    /**
     * Retourne les clauses sociales au format stdClass
     *
     * @return stdClass|null
     */
    public function getClauseSociale(): ?stdClass
    {
        if (!Atexo_Module::isEnabled('ConsultationClause')) {
            return null;
        }

        $std = (new Atexo_Rest_Action_ConsultationField())->getInitializedClauseSocialeStdClass();

        /** @var LotService $lotService */
        $lotService = Atexo_Util::getSfService(LotService::class);
        $lot = $lotService->getLot($this->getId());

        return Atexo_Rest_Action_ConsultationField::populateClauseSocialeData($std, $lot->getClausesN1());
    }

    /**
     * Retourne les clauses environnementales au format stdClass
     *
     * @return stdClass|null
     */
    public function getClauseEnvironnementale(): ?stdClass
    {
        if (!Atexo_Module::isEnabled('ConsultationClause')) {
            return null;
        }

        $std = (new Atexo_Rest_Action_ConsultationField())->getInitializedClauseEnvironnementaleStdClass();

        /** @var LotService $lotService */
        $lotService = Atexo_Util::getSfService(LotService::class);
        $lot = $lotService->getLot($this->getId());

        return Atexo_Rest_Action_ConsultationField::populateClauseEnvironnementaleData($std, $lot->getClausesN1());
    }

    /**
     * Set the value of [clause_sociale] column.
     *
     * @param string $v new value
     *
     * @author OKO <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setClauseSociale($v)
    {
        if ('0' !== Atexo_Util::getIntFromValboolean($this->getClauseSocialeConditionExecution())
                || '0' !== Atexo_Util::getIntFromValboolean($this->getClauseSocialeInsertion())
                || '0' !== Atexo_Util::getIntFromValboolean($this->getClauseSocialeAteliersProteges())
                || '0' !== Atexo_Util::getIntFromValboolean($this->getClauseSocialeSiae())
                || '0' !== Atexo_Util::getIntFromValboolean($this->getClauseSocialeEss())
        ) {
            $v = '1';
        } else {
            $v = '2';
        }

        return parent::setClauseSociale($v);
    }

    // setClauseSociale

    /**
     * Set the value of [clause_environnementale] column.
     *
     * @param string $v new value
     *
     * @author OKO <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setClauseEnvironnementale($v)
    {
        if ('0' !== Atexo_Util::getIntFromValboolean($this->getClauseEnvSpecsTechniques())
                || '0' !== Atexo_Util::getIntFromValboolean($this->getClauseEnvCondExecution())
                || '0' !== Atexo_Util::getIntFromValboolean($this->getClauseEnvCriteresSelect())) {
            $v = '1';
        } else {
            $v = '2';
        }

        return parent::setClauseEnvironnementale($v);
    }

    // setClauseEnvironnementale

    /**
     * Affecte la valeur de la colonne [description].
     *
     * @param string $description
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setDescription($description)
    {
        parent::setDescription($description);
    }

    /**
     * Affecte la valeur de la colonne [description_detail].
     *
     * @param string $value
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setDescriptionDetail($value)
    {
        parent::setDescriptionDetail($value);
    }
}
