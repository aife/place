<?php

namespace Application\Service\Atexo\Rest\Action;

use Application\Propel\Mpe\Entreprise;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Entreprise;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Oversight\Atexo_Oversight_Oversight;
use Application\Service\Atexo\Serializer\Atexo_Serializer_Serializer;
use DateTime;
use Exception;

/*
 * permet d'avoir  une entreprise à partir de son siren
 *
 * @author LEZ <loubna.ezziani@atexo.com>
 * @version 1.0
 * @since 4.7.0
 * @copyright Atexo 2014
 * @package Atexo_Rest_Action
 *
 */
class Atexo_Rest_Action_EntrepriseBySiren extends Atexo_Rest_Action_Actions
{
    /*
     * permet de recuperer une entreprise à partir de son siren
     *
     * @param Integer $siren siren de l'entreprise
     * @return string xml contenant les infos de l'entreprise
     * @author LEZ <loubna.ezziani@atexo.com>
     * @version 1.0
     * @since 4.7.0
     * @copyright Atexo 2014
     */
    public function get($siren)
    {
        $xmlResult = null;
        $params = [];
        try {
            $params['debutExecution'] = new DateTime('now');
            $params['nomBatch'] = 'WS-entreprise-by-siren';
            $params['service'] = 'Entreprise';
            $entree = serialize(['siren' => $siren]);
            $params['variablesEntree'] = $entree;
            $params['informationMetier'] = $entree;
            $params['typeFlux'] = 'WebService';
            $params['ticket'] = Atexo_Util::atexoHtmlEntities($_GET['ticket']);
            self::oversight($params);
        } catch (Exception $exceptionOversight) {
            $logger = Atexo_LoggerManager::getLogger('supervision');
            $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
        }

        try {
            $logger = Atexo_LoggerManager::getLogger();
            $logger->info('Debut GET Entreprise by SIREN....');
            $retour = $this->validateTicket($logger);

            if (true === $retour) {
                $logger->info("La recuperation de l'entreprise dont l'siren= ".$siren);
                $entreprise = (new Atexo_Entreprise())->retrieveCompanyBdeBySiren($siren);

                if ($entreprise instanceof Entreprise) {
                    $xmlProperties = Atexo_Config::getParameter('PATH_FILE_MAPPING_ENTREPRISE');
                    $xmlHiddenField = Atexo_Config::getParameter('PATH_FILE_HIDDEN_FIELD_ENTREPRISE');
                    $listOptioanlField = Atexo_Config::getParameter('PATH_FILE_OPTIONAL_FIELD_ENTREPRISE');
                    $logger->info("La récupération de l'xm à partir de l'objet entreprise ");
                    $serilizer = new Atexo_Serializer_Serializer();
                    $xmlResult = $serilizer->SerializeClass($entreprise, 'Entreprise', $xmlProperties, $xmlHiddenField, $listOptioanlField);
                    $xmlResult = Atexo_Rest_Action_Actions::encapsulerNodeResponseWsMpe($xmlResult, 'entreprise');
                    $this->getHeaders('200');
                } else {
                    $this->getHeaders('400');
                    $logger->error("L'entreprise n'existe pas");
                    $xmlResult = static::generateXmlError("L'entreprise n'existe pas");
                }
                $params['code'] = Atexo_Oversight_Oversight::$codeSuccess;
                $res = $xmlResult;
            } else {
                $params['code'] = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
                $res = $retour;
            }
            try {
                self::oversight($params);
            } catch (Exception $exceptionOversight) {
                $logger = Atexo_LoggerManager::getLogger('supervision');
                $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
            }
        } catch (Exception $e) {
            $logger->error('Erreur technique = '.$e->getMessage());
            $this->getHeaders('500');
            $xmlResult = static::generateXmlError('Erreur technique'.$e->getMessage());
        }

        $logger->info('Fin  GET  Entreprise by SIREN....');

        return $xmlResult;
    }
}
