<?php

namespace Application\Service\Atexo\Rest\Action;

use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Attestation;
use Exception;

/**
 * Class Atexo_Rest_Action_DocumentsSynchro service de synchronisation des documents entreprise/etablissement.
 *
 * @author    Oumar KONATE <oumar.konate@atexo.com>
 *
 * @since     ESR2016
 *
 * @copyright Atexo 2016
 */
class Atexo_Rest_Action_DocumentsSynchro extends Atexo_Rest_Action_Actions
{
    /**
     * Permet de lancer la synchronisation des documents.
     *
     * @param $id
     * @param $refUtilisateur
     *
     *@author    Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since     ESR2016
     *
     * @copyright Atexo 2016
     */
    public function get($id, $refUtilisateur, $idOffre = '')
    {
        $typeSynchro = null;
        $logger = Atexo_LoggerManager::getLogger();
        $typeDocument = $_GET['type'];
        if (empty($typeDocument) || !in_array($typeDocument, [Atexo_Config::getParameter('TYPE_DOCUMENT_SGMAP_ENTREPRISE'), Atexo_Config::getParameter('TYPE_DOCUMENT_SGMAP_ETABLISSEMENT')])) {
            $logger->error("Le type de document est obligatoire: [idEntreprise/idEtablissement=$id], [refUtilisateurCons=$refUtilisateur], [typeDoc=$typeDocument]");
            throw new \Exception("Le type de document '$typeDocument' n'est pas valide. Valeurs possibles: (".Atexo_Config::getParameter('TYPE_DOCUMENT_SGMAP_ENTREPRISE').', '.Atexo_Config::getParameter('TYPE_DOCUMENT_SGMAP_ETABLISSEMENT').')');
        }
        $logger->info("Debut appel service synchro documents: [idEntreprise/idEtablissement=$id], [refUtilisateurCons=$refUtilisateur], [typeDoc=$typeDocument]");
        try {
            if ($typeDocument == Atexo_Config::getParameter('TYPE_DOCUMENT_SGMAP_ENTREPRISE')) {
                $logger->info("Appel de la synchronisation documents entreprise: [idEntreprise=$id], [refUtilisateurCons=$refUtilisateur]");
                $typeSynchro = true;
            } elseif ($typeDocument == Atexo_Config::getParameter('TYPE_DOCUMENT_SGMAP_ETABLISSEMENT')) {
                $logger->info("Appel de la synchronisation documents etablissement: [idEtablissement=$id], [refUtilisateurCons=$refUtilisateur]");
                $typeSynchro = false;
            }
            (new Atexo_Entreprise_Attestation())->synchroniserTypesDocumentsEntreprise($id, $typeSynchro, $refUtilisateur);

            if (!empty($idOffre)) {
                $logger->info("Association des attestations à l'offre : [id=$id], [type= $typeDocument], [refUtilisateurCons=$refUtilisateur], [idOffre=$idOffre]");
                (new Atexo_Entreprise_Attestation())->attacheAttestationToOffre($idOffre, $id, $typeDocument);
            }
        } catch (Exception $e) {
            $erreur = "Erreur lors du lancement du service de synchronisation des documents: [idEntreprise/idEtablissement=$id], [refUtilisateurCons=$refUtilisateur], [typeDoc=$typeDocument]";
            $erreur .= PHP_EOL.'Erreur: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString();
            $logger->error($erreur);
        }
        $logger->info("Fin appel service synchro documents: [idEntreprise/idEtablissement=$id], [refUtilisateurCons=$refUtilisateur], [typeDoc=$typeDocument]");
    }
}
