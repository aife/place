<?php

namespace Application\Service\Atexo\Rest\Action;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonEnveloppe;
use Application\Propel\Mpe\CommonOffres;
use Application\Propel\Mpe\CommonOffresPeer;
use Application\Propel\Mpe\CommonOffresQuery;
use Application\Service\Atexo\Agent\Atexo_Agent_Notifications;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_FichierEnveloppe;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Responses;
use Application\Service\Atexo\Oversight\Atexo_Oversight_Oversight;
use Application\Service\Atexo\Registres\Atexo_Registres_RegistreDepot;
use Application\Service\Atexo\Serializer\Atexo_Serializer_Serializer;
use AtexoCrypto\Dto\Offre;
use DateTime;
use Exception;

/**
 * permet de gérer le registre de depot.
 *
 * @author : LEZ <loubna.ezziani@atexo.com>
 *
 * @version : 1.0
 *
 * @since : 4.6.0
 *
 * @copyright Atexo 2014
 */
class Atexo_Rest_Action_RegistreDepot extends Atexo_Rest_Action_Actions
{
    /**
     * permet d'avoir le detail d'un registre de depot.
     *
     * @param int $idDepot l'id de depot concerne
     *
     * @return string un xml contenant les infos du registre de depot
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function get($idDepot)
    {
        $params = [];
        try {
            $params['debutExecution'] = new DateTime('now');
            $params['nomBatch'] = 'WS-dépôt-récupération-get';
            $params['service'] = 'Registre';
            $params['poids'] = 0;
            $params['ticket'] = Atexo_Util::atexoHtmlEntities($_GET['ticket']);
            self::oversight($params);
            $params['code'] = Atexo_Oversight_Oversight::$codeSuccess;
        } catch (Exception $exceptionOversight) {
            $logger = Atexo_LoggerManager::getLogger('supervision');
            $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
        }

        try {
            $logger = Atexo_LoggerManager::getLogger('crypto');
            $logger->info('Debut GET Registre Depot....');

            $logger->info('La vérification du ticket');
            $retour = $this->validateTicket($logger);
            if (true === $retour) {
                $offreQuery = new CommonOffresQuery();
                $offre = $offreQuery->getOffreNonBrouillonById($idDepot);
                if ($offre instanceof CommonOffres) {
                    $consultation = (new Atexo_Consultation())->retrieveConsultation($offre->getConsultationId(), $offre->getOrganisme());
                    if (!($consultation instanceof CommonConsultation)
                        ||
                        !($this->verifieAccesToInfo($_GET['ticket'], $consultation->getOrganisme(), $consultation->getServiceId()))
                    ) {
                        $consultationExist = ($consultation instanceof CommonConsultation);
                        $log = $consultationExist ?
                            ('Vérification de droit acces KO organisme '.$consultation->getOrganisme().' service '.$consultation->getServiceId()) :
                            ("consultation n'existe pas pr offre ".$idDepot);
                        $logger->info($log);
                        $serviceId = $consultationExist ? $consultation->getServiceId() : null;
                        $organisme = $consultationExist ? $consultation->getOrganisme() : null;

                        return self::generateXmlError($this->getMessageErrorForAccessDenied($organisme, $serviceId));
                    }
                } else {
                    return self::generateXmlError($this->getMessageErrorForAccessDenied());
                }
                $registreDepot = Atexo_Consultation_Responses::getRegistreDepot($idDepot, $logger);
                if ($registreDepot instanceof Atexo_Registres_RegistreDepot) {
                    $logger->info("Le depot dont l'id = ".$idDepot.'  existe');
                    $xmlResponse = $this->getXmlInfosRegistre($registreDepot);
                    $logger->info("La récupération de l'xml à partir de l'objet RegistreDepot ");
                    $xml = $this->encapsulerNodeResponseWsMpe($xmlResponse, 'registreDepot');
                    $logger->info('Le Registre de depot existe');
                    $this->getHeaders('200');
                } else {
                    $logger->error("Le depot dont l'id = ".$idDepot." n'existe pas");
                    $xml = static::generateXmlError("Le depot dont l'id = ".$idDepot." n'existe pas");
                    $this->getHeaders('400');
                }
                $res = $xml;
            } else {
                $params['code'] = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
                $res = $retour;
            }
            try {
                $params['oversight'] = self::oversight($params);
                unset($params['oversight']);
            } catch (Exception $exceptionOversight) {
                $logger = Atexo_LoggerManager::getLogger('supervision');
                $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
            }

            return $res;
        } catch (\Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('crypto');
            try {
                $params['code'] = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
                self::oversight($params);
            } catch (Exception $exceptionOversight) {
                $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
            }

            $logger->error('Erreur technique = '.$e->getMessage().' '.$e->getTraceAsString());
            $this->getHeaders('500');

            return static::generateXmlError('erreur technique');
        }
    }

    /**
     * genere un xml contenant les infos d'un depot.
     *
     * @param Atexo_Registres_RegistreDepot $registreDepot objet du RegistreDepot
     *
     * @return string xmlcontenant les infos d'un depot
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getXmlInfosRegistre($registreDepot)
    {
        $serializer = new Atexo_Serializer_Serializer();
        $mappingFile = Atexo_Config::getParameter('PATH_FILE_MAPPING_DEPOT');
        $hiddenFile = Atexo_Config::getParameter('PATH_FILE_HIDDEN_FIELD_DEPOT');
        $listOptioanlField = Atexo_Config::getParameter('PATH_FILE_OPTIONAL_FIELD_REGISTRE_DEPOT');
        $xml = $serializer->SerializeClass($registreDepot, 'Atexo_Registres_RegistreDepot', $mappingFile, $hiddenFile, $listOptioanlField);

        return $xml;
    }

    /**
     * permet de mettre à jour le statut de l'offre.
     *
     * @param int $idDepot l'id de depot concerne
     *
     * @return string un xml contenant les infos du registre de depot
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function post()
    {
        $params = [];
        try {
            $params['nomBatch'] = 'WS-dépôt-récupération-post';
            $params['service'] = 'Registre';
            $params['typeFlux'] = 'WebService';
            $params['ticket'] = Atexo_Util::atexoHtmlEntities($_GET['ticket']);
            $params['code'] = Atexo_Oversight_Oversight::$codeSuccess;
        } catch (Exception $exceptionOversight) {
            $logger = Atexo_LoggerManager::getLogger('supervision');
            $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
        }

        try {
            $logger = Atexo_LoggerManager::getLogger('crypto');
            $retour = $this->validateTicket($logger);
            if (true === $retour) {
                $xml = $_POST['xml'];

                $erreur = '';
                if ($xml) {
                    $xmlValide = Atexo_Util::validateXmlBySchema($xml, Atexo_Config::getParameter('PATH_FILE_XSD_WS'), $logger, $erreur);
                    if ($xmlValide) {
                        $xmlElements = simplexml_load_string($xml);
                        if (isset($xmlElements->envoi->offre)) {
                            $logger->info('Debut Post chiffrementxml recu '.$xml);
                            $xmlResult = $this->doTraitementChiffrement($xmlElements, $logger);
                            $logger->info('Fin Post chiffrement xml retour : '.$xmlResult);
                        } elseif (isset($xmlElements->envoi->enveloppe)) {
                            $logger->info('Debut Post dechiffrement xml recu '.$xml);
                            $xmlResult = $this->doTraitementDechiffrement($xmlElements, $logger);
                            $logger->info('Fin Post dechiffrement xml retour : '.$xmlResult);
                        } else {
                            $logger->error('La structure du fichier xml ne respecte pas le schema approprie : '.$xml);
                            $this->getHeaders('400');
                            $xmlResult = static::generateXmlError('La structure du fichier xml ne respecte pas le schema approprie');
                        }
                    } else {
                        $logger->error('La structure du fichier xml ne respecte pas le schema approprie : '.$xml);
                        $this->getHeaders('400');
                        $xmlResult = static::generateXmlError('La structure du fichier xml ne respecte pas le schema approprie : '.$erreur);
                    }
                } else {
                    $logger->error("Il n'ya pas du fichier xml à traiter");
                    $this->getHeaders('400');
                    $xmlResult = static::generateXmlError("Il n'ya pas du fichier xml à traiter");
                }
            } else {
                $xmlResult = $retour;
            }
        } catch (\Exception $e) {
            $params['code'] = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
            $logger->error('Erreur technique = '.$e->getMessage());
            $this->getHeaders('500');
            $xmlResult = static::generateXmlError('Erreur technique');
        }

        try {
            self::oversight($params);
        } catch (Exception $exceptionOversight) {
            $logger = Atexo_LoggerManager::getLogger('supervision');
            $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
        }

        return $xmlResult;
    }

    /**
     * construit xml retour.
     *
     * @param SimpleXmlElement $xmlElements
     * @param logger           $logger
     *
     * @return string       xml contenant les infos du pli et des blocs
     * @return false|string
     *
     * @throws Exception
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     */
    public function doTraitementChiffrement($xmlElements, $logger)
    {
        $connexion = null;
        $link = null;
        try {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $idOffre = (int) $xmlElements->envoi->offre->idOffre;
            $organisme = (string) $xmlElements->envoi->offre->organisme;
            $offre = Atexo_Consultation_Responses::retrieveOffreById($idOffre, $organisme);
            $logger->info('doTraitementChiffrement idOffre => '.$idOffre);
            if ($offre instanceof CommonOffres) {
                $consultation = $offre->getCommonConsultation($connexion);
                if (!($consultation instanceof CommonConsultation)
                    ||
                    !($this->verifieAccesToInfo($_GET['ticket'], $consultation->getOrganisme(), $consultation->getServiceId()))
                ) {
                    $consultationExist = ($consultation instanceof CommonConsultation);
                    $log = $consultationExist ?
                        ('Vérification de droit acces KO organisme '.$consultation->getOrganisme().' service '.$consultation->getServiceId()) :
                        ("consultation n'existe pas pr offre ".$idOffre);
                    $logger->info($log);
                    $serviceId = $consultationExist ? $consultation->getServiceId() : null;

                    return self::generateXmlError($this->getMessageErrorForAccessDenied($organisme, $serviceId));
                }
                if ($offre->getStatutOffres() == Atexo_Config::getParameter('STATUT_ENV_FERMETURE_EN_COURS')
                    ||
                    $offre->getStatutOffres() == Atexo_Config::getParameter('STATUT_ENV_CHIFFREMENT_EN_COURS')
                ) {
                    $connexion->beginTransaction();

                    $reponseAnnonceXML = base64_decode((string) $xmlElements->envoi->offre->reponseAnnonceXML);
                    $dateFinChiffrement = Atexo_Util::getDateTimeByFormat((string) $xmlElements->envoi->offre->heureFinChiffrement, 'Y-m-d H:i:s');
                    $statut = (int) $xmlElements->envoi->offre->statut;

                    $offre->setXmlString(utf8_encode($reponseAnnonceXML));
                    $offre->setStatutOffres($statut);
                    $offre->setDateFinChiffrement($dateFinChiffrement);
                    $offre->save($connexion);
                    $connexion->commit();
                    $logger->info('Offre mise à jour '.print_r($offre, true));
                    if ($xmlElements->envoi->offre->statut == Atexo_Config::getParameter('STATUT_ENV_FEREME')) {
                        $consultation = $offre->getCommonConsultation();
                        $link = Atexo_Db::getLinkCommon();
                        $link->beginTransaction();
                        if ($consultation instanceof CommonConsultation && 1 == $consultation->getChiffrementOffre()) {
                            $logger->info('la suppression des blobs fichiers ');
                            //suppression des fichiers en dur
                            Atexo_FichierEnveloppe::deleteBlobsFichiersNonChiffres($idOffre, $organisme);
                            //Changement de l'extension des fichiers jeton pour les consultation chiffré
                            Atexo_FichierEnveloppe::changeExtensionJeton($idOffre, $organisme);
                        }
                        $logger->info('la fermeture des enveloppes ');
                        // fermer les enveloppes
                        Atexo_Consultation_Responses::updatEnveloppeOffre(
                            $idOffre,
                            $organisme,
                            $xmlElements->envoi->offre->statut,
                            1
                        );
                        $logger->info("l'ouverture des enveloppes fictives");
                        // ouvrir les enveloppes fictives
                        Atexo_Consultation_Responses::updatEnveloppeOffre(
                            $idOffre,
                            $organisme,
                            Atexo_Config::getParameter('STATUT_ENV_OUVERTE_EN_LIGNE'),
                            0,
                            true
                        );
                        $link->commit();
                    }
                    $logger->info('Suppression des fichiers avec succès');

                    $this->getHeaders('200');
                    $xmlResult = self::generateXmlResponse('OK');
                } else {
                    $logger->error("Le statut de l'offre est invalide.L'etat de chiffrement de l'offre est ".$offre->getEtatChiffrement()."le statut de l'offre est ".$offre->getStatutOffres().' idOffre '.$idOffre.', organisme = '.$organisme);
                    $this->getHeaders('200');
                    $xmlResult = static::generateXmlError("Le statut de l'offre est invalide.L'etat de chiffrement de l'offre est ".$offre->getEtatChiffrement()."le statut de l'offre est ".$offre->getStatutOffres().' idOffre '.$idOffre.', organisme = '.$organisme);
                }
            } else {
                $logger->error("Aucune offre avec l'identifiant envoyé : idOffre ".$idOffre.', organisme = '.$organisme);
                $this->getHeaders('400');
                $xmlResult = static::generateXmlError("Aucune offre avec l'identifiant envoyé : idOffre ".$idOffre.', organisme = '.$organisme);
            }

            return $xmlResult;
        } catch (\Exception $e) {
            if (!empty($connexion)) {
                $connexion->rollBack();
            }
            if (!empty($link)) {
                $link->rollBack();
            }
            throw $e;
        }
    }

    /**
     * construit xml retour.
     *
     * @param SimpleXmlElement $xmlElements
     * @param logger           $logger
     *
     * @return string xml contenant les infos du pli et des blocs
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function doTraitementDechiffrement($xmlElements, $logger)
    {
        $connexion = null;
        try {
            $enveloppeXml = $xmlElements->envoi->enveloppe;
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $idOffre = (int) $enveloppeXml->idOffre;
            $organisme = (string) $enveloppeXml->organisme;
            $idEnveloppe = (int) $enveloppeXml->idEnveloppe;
            $logger->info('doTraitementDechiffrement idOffre => '.$idOffre.' idEnveloppe => '.$idEnveloppe);
            $connexion->beginTransaction();
            $enveloppe = Atexo_Consultation_Responses::retrieveEnveloppe($idEnveloppe, $organisme, $idOffre, $connexion);
            $offre = CommonOffresPeer::retrieveByPK($idOffre, $organisme, $connexion);
            $modeReponse = 0;
            if ($offre instanceof CommonOffres) {
                $logger->info('offre existe ');
                $consultation = $offre->getCommonConsultation();
                if (!($consultation instanceof CommonConsultation)
                    ||
                    !($this->verifieAccesToInfo($_GET['ticket'], $consultation->getOrganisme(), $consultation->getServiceId()))
                ) {
                    $consultationExist = ($consultation instanceof CommonConsultation);
                    $log = $consultationExist ?
                        ('Vérification de droit acces KO organisme '.$consultation->getOrganisme().' service '.$consultation->getServiceId()) :
                        ("consultation n'existe pas pr offre ".$idOffre);
                    $logger->info($log);
                    $serviceId = $consultationExist ? $consultation->getServiceId() : null;

                    return self::generateXmlError($this->getMessageErrorForAccessDenied($organisme, $serviceId));
                }
                if ($consultation instanceof CommonConsultation) {
                    $modeReponse = $consultation->getModeOuvertureReponse();
                    $referenceCons = $consultation->getId();
                    $referenceUtilisateurCons = $consultation->getReferenceUtilisateur();
                }
            }
            if ($enveloppe instanceof CommonEnveloppe) {
                $logger->info("l'enveloppe existe");
                $idAgent = (int) $enveloppeXml->idAgent;
                $statut = (int) $enveloppeXml->statut;
                $enveloppe->setStatutEnveloppe($statut);
                $logger->info("statut de l'enveloppe=> ".$statut);
                if ($statut == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_EN_LIGNE')) {
                    if (!$modeReponse) {
                        $description = '@@DEFINE_CONSULTATION@@';
                        $url = '?page=Agent.ouvertureEtAnalyse&id='.$referenceCons;
                        $notificationLibelle = '@@LABEL_NOTIFICATION_PLIS_DECHIFFRES@@';
                        $logger->info("ajout de la notification de dechiffrement de l'enveloppe");
                        (new Atexo_Agent_Notifications())->ajouterNotification($idAgent, $organisme, Atexo_Config::getParameter('ID_TYPE_NOTIFICATION_CONSULTATION'), $notificationLibelle, $description, $referenceUtilisateurCons, $connexion, $url);
                    }
                    if (isset($enveloppeXml->integriteFichier)) {
                        $integriteFichier = ('true' == (string) $enveloppeXml->integriteFichier) ? 1 : 0;
                        $enveloppe->setIntegriteFichier($integriteFichier);
                    }
                    $enveloppe->setCryptage('0');
                    if (isset($enveloppeXml->heureFinDechiffrement)) {
                        $dateFinDechiffrement = Atexo_Util::getDateTimeByFormat((string) $enveloppeXml->heureFinDechiffrement);
                        if (Atexo_Module::isEnabled('fourEyes')) {
                            $enveloppe->setDateheureOuvertureAgent2($dateFinDechiffrement);
                            $enveloppe->setAgentIdOuverture2($idAgent);
                        } else {
                            $enveloppe->setDateheureOuverture($dateFinDechiffrement);
                            $enveloppe->setAgentIdOuverture($idAgent);
                        }
                    }
                    $logger->info(" verification du hash de l'enveloppe ".$enveloppe->getIdEnveloppeElectro());
                    $resultatVerifiHash = $enveloppe->isHashFichiersOk();
                    $logger->info(" resultat de la verification du hash de l'enveloppe ".$enveloppe->getIdEnveloppeElectro().' => '.$resultatVerifiHash);
                    $enveloppe->setResultatVerificationHashFiles($resultatVerifiHash);
                }
                if ($statut === (int) Atexo_Config::getParameter('STATUT_ENV_DECHIFFREMENT_EN_COURS')) {
                    $enveloppe->setDateDebutDechiffrement(date('Y-m-d H:i:s'));
                    $logger->info('Enregistrement date début chiffrement');
                }
                $enveloppe->save($connexion);
                $logger->info('Enveloppe mise à jour '.print_r($enveloppe, true));

                if ($offre instanceof CommonOffres) {
                    if ($modeReponse && $statut == Atexo_Config::getParameter('STATUT_ENV_DECHIFFREMENT_EN_COURS')) {
                        $offre->setStatutOffres($statut);
                    }
                    $dateHorodatage = '';
                    $verifHorodatage = '';
                    if (isset($enveloppeXml->dateHorodatage)) {
                        $dateHorodatage = Atexo_Util::getDateTimeByFormat((string) $enveloppeXml->dateHorodatage);
                        $offre->setDateHorodatage($dateHorodatage);
                    }
                    $logger->info("idoffre $idOffre dateHorodatage ".$dateHorodatage);
                    if (isset($enveloppeXml->verificationHorodatage)) {
                        $verifHorodatage = ('true' == (string) $enveloppeXml->verificationHorodatage) ? 1 : 2;
                        $offre->setVerificationHotodatage($verifHorodatage);
                    }
                    $logger->info("idoffre $idOffre verifHorodatage ".$verifHorodatage);
                    $offre->save($connexion);
                    $connexion->commit();
                    $nbreEnveloppeFerme = Atexo_Consultation_Responses::nombreEnveloppeFerme($idOffre, $organisme,$connexion);
                    $logger->info("nombre d'enveloppes fermé ".$nbreEnveloppeFerme);
                    if (!$nbreEnveloppeFerme) {
                        Atexo_Consultation_Responses::ouvrirOffres($offre, Atexo_Config::getParameter('STATUT_ENV_OUVERTE_EN_LIGNE'), $idAgent, $connexion, $consultation, $logger);
                        $logger->info("Ouverture de l'offre ".$idOffre);
                    }
                }
                $this->getHeaders('200');
                $xmlResult = self::generateXmlResponse('OK');
            } else {
                $logger->error("Aucune enveloppe avec l'identifiant envoyé : idOffre = ".$idOffre.' idEnveloppe = '.$idEnveloppe.', organisme = '.$organisme);
                $this->getHeaders('400');
                $xmlResult = static::generateXmlError("Aucune enveloppe avec l'identifiant envoyé :idOffre = ".$idOffre.' idEnveloppe = '.$idEnveloppe.', organisme = '.$organisme);
            }

            return $xmlResult;
        } catch (\Exception $e) {
            if (!empty($connexion)) {
                $connexion->rollBack();
            }
            throw $e;
        }
    }
}
