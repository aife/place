<?php

namespace Application\Service\Atexo\Rest\Action;

use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Oversight\Atexo_Oversight_Oversight;
use DateTime;
use DOMDocument;
use Exception;

/**
 * permet de retourner touts les organismes.
 *
 * @author LEZ <loubna.ezziani@atexo.com>
 *
 * @version 1.0
 *
 * @since esr-2015
 *
 * @copyright Atexo 2015
 */
class Atexo_Rest_Action_Organismes extends Atexo_Rest_Action_Actions
{
    /**
     * permet recuperer des organismes d'une PF.
     *
     * @return string xml contenant les infos des organismes et les logs
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function get()
    {
        $logger = null;
        $params = [];
        try {
            $params['debutExecution'] = new DateTime('now');
            $params['nomBatch'] = 'WS-organismes-list';
            $params['service'] = 'Organisme';
            $params['typeFlux'] = 'WebService';
            $params['ticket'] = Atexo_Util::atexoHtmlEntities($_GET['ticket']);
            self::oversight($params);
            $params['code'] = Atexo_Oversight_Oversight::$codeSuccess;
        } catch (Exception $exceptionOversight) {
            $logger = Atexo_LoggerManager::getLogger('supervision');
            $logger->error('Error sur la supervision : ' . $exceptionOversight->getMessage());
        }

        try {
            $logger = Atexo_LoggerManager::getLogger();
            $logger->info('Debut GET Organismes....');
            $retour = $this->validateTicket($logger);
            if (true === $retour) {
                $logger->info('La recuperation des organismes ....');
                $organismes = Atexo_Organismes::retrieveOrganismes(true, null, false);
                if (is_array($organismes) && count($organismes)) {
                    $logger->info('Le nombre des organismes trouvé est = '.count($organismes));
                    $organismesNodes = '';
                    $arrayLogos = [];
                    $withLogo = isset($_GET['withLogo']) ? Atexo_Util::castToBoolean($_GET['withLogo']) : false;
                    foreach ($organismes as $organisme) {
                        $xmlResponse = self::getXmlResultOrganisme($organisme, $arrayLogos, $logger, $withLogo);
                        $organismesNodes .= self::getNodeFromXml($xmlResponse, 'organisme');
                    }
                    $logger->info("La géneration de l'xml de la liste des organismes");
                    $organismesNodes = '<organismes>'.$organismesNodes.'</organismes>';
                    $doc = new DOMDocument('1.0', 'UTF-8');
                    $doc->loadXML(Atexo_Util::toUtf8($organismesNodes));
                    $xmlListeOrganismes = $doc->saveXML();
                    $xmlResult = Atexo_Rest_Action_Actions::encapsulerNodeResponseWsMpe($xmlListeOrganismes, 'organismes');
                    $xmlValide = Atexo_Util::validateXmlBySchema($xmlResult, Atexo_Config::getParameter('PATH_FILE_XSD_WS'), $logger);
                    if ($xmlValide) {
                        $logger->info("L'xml est valide");
                        $log = date('Y-m-d H:i:s').' IP '.$_SERVER['REMOTE_ADDR'].' output : '.print_r($xmlResult, true)." \n ";
                        Atexo_Util::writeFile(Atexo_Config::getParameter('BASE_ROOT_DIR').'/logs/wsRestOrganismes.log', $log, 'a');
                        $this->getHeaders('200');
                        if (!$withLogo || empty($arrayLogos)) {
                            return $xmlResult;
                        } else {
                            self::sendDataWithFile($arrayLogos, $xmlResult, 'binary', 'image/jpeg', false);
                        }
                        try {
                            self::oversight($params);
                        } catch (Exception $exceptionOversight) {
                            $logger = Atexo_LoggerManager::getLogger('supervision');
                            $logger->error('Error sur la supervision : ' . $exceptionOversight->getMessage());
                        }
                    } else {
                        throw new Exception(" la structure de l'xml genere est invalide");
                    }
                } else {
                    $logger->error('Il y a Aucun organisme dans cette PF');
                    $logger->info('Fin  GET Organismes....');
                    $this->getHeaders('404');

                    try {
                        $params['oversight'] = self::oversight($params);
                        unset($params['oversight']);
                    } catch (Exception $exceptionOversight) {
                        $logger = Atexo_LoggerManager::getLogger('supervision');
                        $logger->error('Error sur la supervision : ' . $exceptionOversight->getMessage());
                    }

                    return static::generateXmlError('Il y a Aucun organisme dans cette PF');
                }
            } else {
                try {
                    $params['code'] = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
                    $params['oversight'] = self::oversight($params);
                    unset($params['oversight']);
                } catch (Exception $exceptionOversight) {
                    $logger = Atexo_LoggerManager::getLogger('supervision');
                    $logger->error('Error sur la supervision : ' . $exceptionOversight->getMessage());
                }

                return $retour;
            }
        } catch (Exception $e) {
            $logger->error('Erreur technique = ' . $e->getMessage() . ' ' . $e->getTraceAsString());
            $this->getHeaders('500');
            $logger->info('Fin  GET Organismes....');

            try {
                $params['code'] = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
                $params['oversight'] = self::oversight($params);
                unset($params['oversight']);
            } catch (Exception $exceptionOversight) {
                $logger = Atexo_LoggerManager::getLogger('supervision');
                $logger->error('Error sur la supervision : ' . $exceptionOversight->getMessage());
            }

            return static::generateXmlError('Erreur technique');
        }
        $logger->info('Fin  GET Organismes....');
    }
}

