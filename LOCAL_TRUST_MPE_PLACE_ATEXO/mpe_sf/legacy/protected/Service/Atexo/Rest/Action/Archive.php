<?php

namespace Application\Service\Atexo\Rest\Action;

use Application\Propel\Mpe\CommonConsultation;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Oversight\Atexo_Oversight_Oversight;
use DateTime;
use Exception;

/**
 * permet de gérer une pli.
 *
 * @author AME <amal.elbekkaoui@atexo.com>
 *
 * @version 1.0
 *
 * @since 4.6.0
 *
 * @copyright Atexo 2014
 */
class Atexo_Rest_Action_Archive extends Atexo_Rest_Action_Actions
{
    /**
     * permet recuperer une archive d'une consultation
     * à partir de sa reference et l'organisme.
     *
     * @param string $organisme l'organisme,Integer $refCons la reference de la consultation
     *
     * @return string xml contenant les infos de la consultation
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function get($organisme, $refCons)
    {
        $params = [];
        try {
            $params['debutExecution'] = new DateTime('now');
            $params['nomBatch'] = 'WS-archive-list';
            $params['service'] = 'Archives';
            $params['poids'] = 0;
            $params['nbFlux'] = 1;
            $params['ticket'] = Atexo_Util::atexoHtmlEntities($_GET['ticket']);
            self::oversight($params);
            $params['code'] = Atexo_Oversight_Oversight::$codeSuccess;
        } catch (Exception $exceptionOversight) {
            $logger = Atexo_LoggerManager::getLogger('supervision');
            $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
        }

        try {
            $logger = Atexo_LoggerManager::getLogger('archivage');
            $logger->info('Debut GET archive....');
            $logger->info('vérification du ticket');
//            $retour = $this->validateTicket($logger);
            $retour = true;
            if (true === $retour) {
                $logger->info('La recuperation de la consultation dont la referecne = '.$refCons." et l'organisme = ".$organisme.'....');
                $consultation = (new Atexo_Consultation())->retrieveConsultation($refCons, $organisme);
                if (($consultation instanceof CommonConsultation) && $consultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION')) {
                    $logger->info('Vérification de droit acces ...');
                    if ($this->verifieAccesToInfo($_GET['ticket'], $organisme, $consultation->getServiceId())) {
                        $logger->info('Vérification de droit acces OK organisme '.$organisme);
                        $logger->info("Recuperation de l'archive");
                        $archive = (new Atexo_Rest_Action_Actions())->getArchive($consultation, $logger);
                        $logger->info('Recuperation de xml retour');
                        $xmlResult = self::getXmlInfosArchive($consultation);
                        if (is_array($archive) && count($archive)) {
                            $encoding = 'binary';
                            $contentType = 'application/zip';
                            self::sendDataWithFile([$archive], $xmlResult, $encoding, $contentType, false);
                            $logger->info('Fin sendDataWithFile');
                        } else {
                            $logger->error("L'archive n'existe pas");
                            $this->getHeaders('404');

                            try {
                                self::oversight($params);
                            } catch (Exception $exceptionOversight) {
                                $logger = Atexo_LoggerManager::getLogger('supervision');
                                $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
                            }

                            return static::generateXmlError("L'archive n'existe pas");
                        }
                        $this->getHeaders('200');
                        $logger->info('Fin GET archive....');
                    } else {
                        $this->getHeaders('403');
                        $logger->error('vérification acces KO pr ticket '.
                            $_GET['ticket'].'organisme '.$organisme.' service '.$consultation->getServiceId());

                        return static::generateXmlError($this->getMessageErrorForAccessDenied($organisme, $consultation->getServiceId()));
                    }
                } else {
                    $this->getHeaders('400');
                    $logger->error("La consultation n'existe pas");

                    try {
                        self::oversight($params);
                    } catch (Exception $exceptionOversight) {
                        $logger = Atexo_LoggerManager::getLogger('supervision');
                        $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
                    }

                    return static::generateXmlError("La consultation n'existe pas");
                }
            } else {
                try {
                    $params['code'] = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
                    self::oversight($params);
                } catch (Exception $exceptionOversight) {
                    $logger = Atexo_LoggerManager::getLogger('supervision');
                    $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
                }

                return $retour;
            }
        } catch (Exception $e) {
            try {
                $params['code'] = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
                self::oversight($params);
            } catch (Exception $exceptionOversight) {
                $logger = Atexo_LoggerManager::getLogger('supervision');
                $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
            }

            $logger->error('Erreur technique = '.$e->getMessage());
            $this->getHeaders('500');

            return static::generateXmlError('Erreur technique');
        }
        $logger->info('Fin  GET archive....');
    }

    /**
     * genere un xml contenant les infos d'une archive.
     *
     * @param CommonConsultation $consultation objet du CommonConsultation
     *
     * @return string xml contenant les infos d'une archive
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public static function getXmlInfosArchive($consultation)
    {
        $xml = '<archive>';
        $xml .= '<id>'.$consultation->getId().'</id>';
        $xml .= '<organisme>'.$consultation->getOrganisme().'</organisme>';
        $xml .= '<dateArchivage>'.Atexo_Util::getDateXmlFromDateTimeVal($consultation->getDateArchivage()).'</dateArchivage>';
        $xml .= '</archive>';

        return (new Atexo_Rest_Action_Actions())->encapsulerNodeResponseWsMpe($xml, 'archive');
    }
}
