<?php

namespace Application\Service\Atexo\Rest\Action;

use Application\Propel\Mpe\CommonInscrit;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Inscrit;
use Application\Service\Atexo\Oversight\Atexo_Oversight_Oversight;
use Application\Service\Atexo\Serializer\Atexo_Serializer_Serializer;
use DateTime;
use DOMDocument;
use Exception;

/*
 * permet de recuperer une liste des contacts ayant la Siret fournie en parametre
 *
 * @author ASO <ayoub.souidahmed@atexo.com>
 * @version 1.0
 * @since 4.7.0
 * @copyright Atexo 2014
 * @package Atexo_Rest_Action
 */
class Atexo_Rest_Action_ContactsSiret extends Atexo_Rest_Action_Actions
{
    /*
     * genere un xml (valide ws MPE) contenant les informations des contacts ayant la Siret fournie en parametre
     *
     * @param Integer $siret,la Siret
     * @return string xml contenant les infos des contacts
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @version 1.0
     * @since 4.7.0
     * @copyright Atexo 2014
     */
    public function get($siret)
    {
        $logger = null;
        $params = [];
        try {
            $params['debutExecution'] = new DateTime('now');
            $params['nomBatch'] = 'WS-contact-by-siret';
            $params['service'] = 'Contact';
            $params['typeFlux'] = 'WebService';
            $params['ticket'] = Atexo_Util::atexoHtmlEntities($_GET['ticket']);
            self::oversight($params);
            $params['code'] = Atexo_Oversight_Oversight::$codeSuccess;
        } catch (Exception $exceptionOversight) {
            $logger = Atexo_LoggerManager::getLogger('supervision');
            $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
        }

        try {
            $logger = Atexo_LoggerManager::getLogger();
            $retour = $this->validateTicket($logger);
            if (true === $retour) {
                $logger->info('La récupération des contacts avec siret = '.$siret);
                $contacts = (new Atexo_Entreprise_Inscrit())->getInscritBySiret($siret);
                if (is_countable($contacts) ? count($contacts) : 0) {
                    $logger->info('Le nombre des contacts trouvé est = '.(is_countable($contacts) ? count($contacts) : 0));
                    $contactsNodes = '';
                    foreach ($contacts as $contact) {
                        $xmlResponse = self::getXmlInfosContact($contact);
                        $contactsNodes .= self::getNodeFromXml($xmlResponse, 'contact');
                    }
                    $logger->info("La géneration de l'xml de la liste des contacts");
                    $contactNodes = '<contacts>'.$contactsNodes.'</contacts>';
                    $doc = new DOMDocument('1.0', 'UTF-8');
                    $doc->loadXML(Atexo_Util::toUtf8($contactNodes));
                    $xmlListeContacts = $doc->saveXML();
                    $xmlResult = (new Atexo_Rest_Action_Actions())->encapsulerNodeResponseWsMpe($xmlListeContacts, 'contacts');

                    try {
                        self::oversight($params);
                    } catch (Exception $exceptionOversight) {
                        $logger = Atexo_LoggerManager::getLogger('supervision');
                        $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
                    }

                    return $xmlResult;
                } else {
                    $msgError = 'Aucun contact ne correspond à cette Siret';
                    $logger->error($msgError);
                    $this->getHeaders('404');

                    try {
                        self::oversight($params);
                    } catch (Exception $exceptionOversight) {
                        $logger = Atexo_LoggerManager::getLogger('supervision');
                        $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
                    }

                    return self::generateXmlError($msgError);
                }
            } else {
                try {
                    $params['code'] = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
                    self::oversight($params);
                } catch (Exception $exceptionOversight) {
                    $logger = Atexo_LoggerManager::getLogger('supervision');
                    $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
                }

                return $retour;
            }
        } catch (Exception $e) {
            $logger->error('Erreur technique = '.$e->getMessage().' '.$e->getTraceAsString());
            $this->getHeaders('500');

            try {
                $params['code'] = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
                self::oversight($params);
            } catch (Exception $exceptionOversight) {
                $logger = Atexo_LoggerManager::getLogger('supervision');
                $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
            }

            return self::generateXmlError('erreur techniques');
        }
    }

    /*
     * genere un xml contenant les infos d'un contact
     *
     * @param CommonInscrit $contact objet du CommonInscrit
     * @return string xml contenant les infos d'un contact
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @version 1.0
     * @since 4.6.0
     * @copyright Atexo 2014
     */
    public static function getXmlInfosContact($contact)
    {
        $serializer = new Atexo_Serializer_Serializer();
        $mappingFile = Atexo_Config::getParameter('PATH_FILE_MAPPING_INSCRIT');
        $hiddenField = Atexo_Config::getParameter('PATH_FILE_HIDDEN_FIELD_INSCRIT');
        $listOptioanlField = Atexo_Config::getParameter('PATH_FILE_OPTIONAL_FIELD_CONTACT');
        $xml = $serializer->SerializeClass($contact, 'CommonInscrit', $mappingFile, $hiddenField, $listOptioanlField);

        return $xml;
    }
}
