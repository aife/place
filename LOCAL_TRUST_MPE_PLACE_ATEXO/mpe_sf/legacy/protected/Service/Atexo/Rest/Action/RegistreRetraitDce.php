<?php

namespace Application\Service\Atexo\Rest\Action;

use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonTelechargement;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Oversight\Atexo_Oversight_Oversight;
use Application\Service\Atexo\RetraitDce\Atexo_RetraitDce_RetraitDce;
use Application\Service\Atexo\RetraitDce\Atexo_RetraitDce_TelechargementsDce;
use Application\Service\Atexo\Serializer\Atexo_Serializer_Serializer;
use DateTime;
use Exception;

/*
 * permet de recuperer les infos concernant l'objet RetraitDce dont l'id est fourni
 *
 * @author ASO <ayoub.souidahmed@atexo.com>
 * @version 1.0
 * @since 4.6.0
 * @copyright Atexo 2014
 * @package Atexo_Rest_Action
 */
class Atexo_Rest_Action_RegistreRetraitDce extends Atexo_Rest_Action_Actions
{
    /*
     * genere un xml (valide ws MPE) contenant les informations du RetraitDce dont l'id est fourni
     *
     * @param integer $idTelechargement,l'id de l'objet du RetraitDce
     * @return string xml contenant les infos de la RetraitDce
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @version 1.0
     * @since 4.6.0
     * @copyright Atexo 2014
     */
    public function get($idTelechargement)
    {
        $params = [];
        try {
            $params['debutExecution'] = new DateTime('now');
            $params['nomBatch'] = 'WS-RetraitDCE-récupération';
            $params['service'] = 'Registre';
            $entree = serialize(['idTelechargement' => $idTelechargement]);
            $params['variablesEntree'] = $entree;
            $params['informationMetier'] = $entree;
            $params['typeFlux'] = 'WebService';
            $params['poids'] = 0;
            $params['nbFlux'] = 1;
            $params['ticket'] = Atexo_Util::atexoHtmlEntities($_GET['ticket']);
            self::oversight($params);
            $params['code'] = Atexo_Oversight_Oversight::$codeSuccess;
        } catch (Exception $exceptionOversight) {
            $logger = Atexo_LoggerManager::getLogger('supervision');
            $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
        }

        try {
            $logger = Atexo_LoggerManager::getLogger();
            $logger->info('Debut GET Registre Retrait DCE....');
            $logger->info('La vérification du ticket');
            $retour = $this->validateTicket($logger);
            if (true === $retour) {
                $telechargementObject = (new Atexo_RetraitDce_TelechargementsDce())->getTelechargementById($idTelechargement);
                if ($telechargementObject instanceof CommonTelechargement) {
                    $consultation = (new Atexo_Consultation())->retrieveConsultation($telechargementObject->getConsultationId(), $telechargementObject->getOrganisme());
                    if (!($consultation instanceof CommonConsultation)
                        ||
                        !($this->verifieAccesToInfo($_GET['ticket'], $consultation->getOrganisme(), $consultation->getServiceId()))
                    ) {
                        $consultationExist = ($consultation instanceof CommonConsultation);
                        $log = $consultationExist ?
                            ('Vérification de droit acces KO organisme '.$consultation->getOrganisme().' service '
                                .$consultation->getServiceId())
                            :
                            ("consultation n'existe pas pr telechargement ".$idTelechargement);
                        $logger->info($log);
                        $serviceId = $consultationExist ? $consultation->getServiceId() : null;
                        $organisme = $consultationExist ? $consultation->getOrganisme() : null;

                        return self::generateXmlError($this->getMessageErrorForAccessDenied($organisme, $serviceId));
                    }
                } else {
                    return self::generateXmlError('Aucun Téléchagement avec cet id '.$idTelechargement);
                }
                $retraitDce = (new Atexo_RetraitDce_TelechargementsDce())->getObjectRetraitDce($idTelechargement);
                if ($retraitDce instanceof Atexo_RetraitDce_RetraitDce) {
                    $logger->info('Retrait DCE existe');
                    $xmlResponse = self::getXmlInfosTelechargementDce($retraitDce);
                    $xml = self::encapsulerNodeResponseWsMpe($xmlResponse, 'registreRetrait');
                    $logger->info("Creer l'xml à partir de l'objet RetraitDce (serialise)");
                    $this->getHeaders('200');

                    try {
                        $params['finExecution'] = 'endScript';
                        $params['oversight'] = self::oversight($params);
                        unset($params['finExecution']);
                        unset($params['oversight']);
                    } catch (Exception $exceptionOversight) {
                        $logger = Atexo_LoggerManager::getLogger('supervision');
                        $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
                    }

                    return $xml;
                } else {
                    $this->getHeaders('400');
                    $msgError = "Aucun Retrait ne correspond à l'id fourni";
                    $logger->error($msgError);

                    try {
                        $params['finExecution'] = 'endScript';
                        $params['oversight'] = self::oversight($params);
                        unset($params['finExecution']);
                        unset($params['oversight']);
                    } catch (Exception $exceptionOversight) {
                        $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
                    }

                    return self::generateXmlError($msgError);
                }
            } else {
                try {
                    $params['code'] = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
                    $params['finExecution'] = 'endScript';
                    $params['oversight'] = self::oversight($params);
                    unset($params['finExecution']);
                    unset($params['oversight']);
                } catch (Exception $exceptionOversight) {
                    $logger = Atexo_LoggerManager::getLogger('supervision');
                    $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
                }

                return $retour;
            }
        } catch (Exception $e) {
            $logger = Atexo_LoggerManager::getLogger();
            try {
                $params['code'] = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
                $params['finExecution'] = 'endScript';
                $params['oversight'] = self::oversight($params);
                unset($params['finExecution']);
                unset($params['oversight']);
            } catch (Exception $exceptionOversight) {
                $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
            }

            $logger->error('Erreur technique = '.$e->getMessage().' '.$e->getTraceAsString());
            $this->getHeaders('500');

            return self::generateXmlError('erreur technique');
        }
    }

    /*
     * retourne un xml conrentan les infos d'un retraitDce
     *
     * @param Atexo_RetraitDce_RetraitDce $retraitDce,objet de Atexo_RetraitDce_RetraitDce
     * @return string xml contenant les infos d'un retraitDce
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @version 1.0
     * @since 4.6.0
     * @copyright Atexo 2014
     */
    public function getXmlInfosTelechargementDce($retraitDce)
    {
        $serializer = new Atexo_Serializer_Serializer();
        $mappingFile = Atexo_Config::getParameter('PATH_FILE_MAPPING_RETRAIT_DCE');
        $listOptioanlField = Atexo_Config::getParameter('PATH_FILE_OPTIONAL_FIELD_REGISTRE_RETRAIT_DCE');
        $xml = $serializer->SerializeClass($retraitDce, Atexo_RetraitDce_RetraitDce::class, $mappingFile, false, $listOptioanlField);

        return $xml;
    }
}
