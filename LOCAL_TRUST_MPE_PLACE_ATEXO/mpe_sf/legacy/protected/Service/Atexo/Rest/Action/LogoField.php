<?php

namespace Application\Service\Atexo\Rest\Action;

/**
 * Contient des information du logo.
 *
 * @author LEZ <loubna.ezziani@atexo.com>
 *
 * @since esr-2015
 *
 * @copyright Atexo 2015
 */
class Atexo_Rest_Action_LogoField
{
    protected $filename;
    protected $size;
    protected $md5_checksum;
    protected $pathFile;

    /**
     * @return mixed
     */
    public function getFilename()
    {
        return $this->filename;
    }

    /**
     * @param mixed $filename
     */
    public function setFilename($filename)
    {
        $this->filename = $filename;
    }

    /**
     * @return mixed
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param mixed $size
     */
    public function setSize($size)
    {
        $this->size = $size;
    }

    /**
     * @return mixed
     */
    public function getMd5Checksum()
    {
        return $this->md5_checksum;
    }

    /**
     * @param mixed $md5_checksum
     */
    public function setMd5Checksum($md5_checksum)
    {
        $this->md5_checksum = $md5_checksum;
    }

    /**
     * @return mixed
     */
    public function getPathFile()
    {
        return $this->pathFile;
    }

    /**
     * @param mixed $pathFile
     */
    public function setPathFile($pathFile)
    {
        $this->pathFile = $pathFile;
    }
}
