<?php

namespace Application\Service\Atexo\Rest\Action;

use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonEchange;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Util;
use Exception;

/**
 * permet de gérer l'echange via ws.
 *
 * @author LEZ <loubna.ezziani@atexo.com>
 *
 * @version 1.0
 *
 * @since esr-2016
 *
 * @copyright Atexo 2016
 */
class Atexo_Rest_Action_Echange extends Atexo_Rest_Action_Actions
{
    /**
     * permet de retourner les info d'un echange.
     *
     * @param $organisme
     * @param $id l'id de l'echange ELECTRONIQUE|PAPIER
     *
     * @return string xml contenant les information de l'echange
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-megalis
     *
     * @copyright Atexo 2016
     */
    public function get($organisme, $id)
    {
        $xmlResponse = null;
        $msgInfoEchange = null;
        try {
            $logger = Atexo_LoggerManager::getLogger('messec');
            $logger->info("Debut de la récupération d'info d'echange....");
            $retour = $this->validateTicket($logger);
            if (true === $retour) {
                $logger->info("La récupération dde l'echange dont id = ".$id.' et organisme = '.$organisme);
                $echange = Atexo_Message::retrieveEchangeById($organisme, $id);
                if ($echange instanceof CommonEchange) {
                    $consultation = (new Atexo_Consultation())->retrieveConsultation($echange->getConsultationId(), $echange->getOrganisme());
                    if (!($consultation instanceof CommonConsultation)
                        ||
                        !($this->verifieAccesToInfo($_GET['ticket'], $consultation->getOrganisme(), $consultation->getServiceId()))
                    ) {
                        $consultationExist = ($consultation instanceof CommonConsultation);
                        $log = $consultationExist ?
                            ('Vérification de droit acces KO organisme '.$consultation->getOrganisme().' service '.$consultation->getServiceId()) :
                            ("consultation n'existe pas ".$echange->getConsultationId().'-'.$organisme);
                        $logger->info($log);
                        $serviceId = $consultationExist ? $consultation->getServiceId() : null;
                        $messageErreur = $this->getMessageErrorForAccessDenied($organisme, $serviceId);
                    } else {
                        $msgInfoEchange = "l'organisme => ".$organisme." , l'id => ".$id;
                        $logger->info("La récupération de l'echange (".$msgInfoEchange.')');
                        $xmlResponse = '';
                        // On renvoi le oldIdService pour clients spécifiques
                        if (true === (bool) getenv('SPECIFIQUE_PLACE')) {
                            $oldIdService = $this->getOldIdServiceByIdService($echange->getServiceId());
                            $echange->setServiceId($oldIdService);
                        }

                        $messageErreur = (new Atexo_Message())->setInfoEchangeForWs($echange, $xmlResponse, $logger);
                    }
                } else {
                    $messageErreur = "Cet echange n'existe pas";
                }
                if (!$messageErreur) {
                    if ($xmlResponse) {
                        $xmlResponse = $this->encapsulerNodeResponseWsMpe($xmlResponse, 'registreEchange');
                        $logger->info("La validation de l'xml à retourner");
                        $xmlValide = Atexo_Util::validateXmlBySchema($xmlResponse, Atexo_Config::getParameter('PATH_FILE_XSD_WS'), $logger);
                        if ($xmlValide) {
                            $logger->info("L'xml à envoyé est valide");
                            $this->getHeaders('200');
                            $logger->info("Fin de la récupération de l'echange....");

                            return $xmlResponse;
                        } else {
                            throw new Exception("la structure de l'xml genéré est invalide ");
                        }
                    } else {
                        $messageErreur = 'Les parametres ('.$msgInfoEchange.') sont invalides';
                    }
                }
                if ($messageErreur) {
                    $logger->error($messageErreur);
                    $this->getHeaders('400');
                    $logger->info("Fin de la récupération de l'echange....");

                    return static::generateXmlError($messageErreur);
                }
            } else {
                return $retour;
            }
        } catch (Exception $e) {
            $logger->error('Erreur technique = '.$e->getMessage().' '.$e->getTraceAsString());
            $this->getHeaders('500');

            return static::generateXmlError('erreur technique');
        }
    }
}
