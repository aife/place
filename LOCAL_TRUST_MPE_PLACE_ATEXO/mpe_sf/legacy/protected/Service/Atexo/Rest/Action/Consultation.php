<?php

namespace Application\Service\Atexo\Rest\Action;

use App\Service\Clauses\ClauseUseApiV0;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonService;
use Application\Propel\Mpe\CommonServiceQuery;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_Contrat;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_TypeContrat;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Config\Atexo_Config_Exception;
use Application\Service\Atexo\Oversight\Atexo_Oversight_Oversight;
use Application\Service\Atexo\Serializer\Atexo_Serializer_Serializer;
use DateTime;
use Exception;

/**
 * permet de créer ou modifier une consultation
 * à partir d'un xml contenant les données de la consultation.
 *
 * @author    LEZ <loubna.ezziani@atexo.com>
 *
 * @version   1.0
 *
 * @since     4.6.0
 *
 * @copyright Atexo 2014
 */
class Atexo_Rest_Action_Consultation extends Atexo_Rest_Action_Actions
{
    public const TYPE_CONTRAT_SYSTEME_ACQUISITION_DYNAMIQUE = 3;
    public const TYPE_CONTRAT_AC_MULTI_ATTRIBUTAIRE = 5;
    private ClauseUseApiV0 $clauseUseApiV0;

    public function __construct()
    {
        $this->clauseUseApiV0 = Atexo_Util::getSfService(ClauseUseApiV0::class);
    }

    /**
     * permet de créer ou modifier une consultation
     * à partir d'un xml poster contenant les données de la consultation.
     *
     * @param     string xml contenant les infos de la consultation passer en POST
     *
     * @return    string xml contenant les infos de la consultation
     *
     * @author    LEZ <loubna.ezziani@atexo.com>
     *
     * @version   1.0
     *
     * @since     4.6.0
     *
     * @copyright Atexo 2014
     *
     * @return    bool|string
     *
     * @throws    Atexo_Config_Exception
     */
    public function post()
    {
        $logger = null;
        $xmlResult = null;
        $params = [];
        try {
            $params['debutExecution'] = new DateTime('now');
            $params['nomBatch'] = 'WS-Consultation-création';
            $params['service'] = 'Consultation';
            $params['poids'] = 0;
            $params['ticket'] = Atexo_Util::atexoHtmlEntities($_GET['ticket']);
            self::oversight($params);
            $params['code'] = Atexo_Oversight_Oversight::$codeSuccess;
        } catch (Exception $exceptionOversight) {
            $logger = Atexo_LoggerManager::getLogger('supervision');
            $logger->error('Error sur la supervision : ' . $exceptionOversight->getMessage());
        }

        try {
            if (!isset($logger)) {
                $logger = Atexo_LoggerManager::getLogger('consultation');
            }
            $logger->info('Debut....');
            $organisme = null;
            if (Atexo_Module::isEnabled('webServiceParSilo')) {
                $logger->info('Début de La vérification SILO : ');
                $xml = $_POST['xml'];
                if ($xml) {
                    $xmlElements = simplexml_load_string($xml);
                    if ($xmlElements instanceof \SimpleXMLElement) {
                        $organisme = $xmlElements->envoi->consultation->organisme;
                        $logger->info("La vérification SILO du ticket avec l'organisme : " . $organisme);
                    }
                }
            }

            $retour = $this->validateTicket($logger);

            if (true === $retour) {
                $xml = $_POST['xml'];

                $log = date('Y-m-d H:i:s') . ' IP ' . $_SERVER['REMOTE_ADDR'] . ' input : ' . print_r($xml, true) . " \n POST " . print_r($_POST, true) . " \n FILES " . print_r($_FILES, true) . " \n ";
                Atexo_Util::writeFile(Atexo_Config::getParameter('BASE_ROOT_DIR') . '/logs/wsRestConsultation.log', $log, 'a');

                if ($xml) {
                    $xmlValide = Atexo_Util::validateXmlBySchema($xml, Atexo_Config::getParameter('PATH_FILE_XSD_WS'), $logger);

                    if ($xmlValide) {
                        $logger->info('xml valide , récupération du tag consultation');
                        $xmlConsultation = $this->getNodeFromXml($xml, 'consultation');
                        $xmlElements = simplexml_load_string($xml);
                        $consultationId = (int) $xmlElements->envoi->consultation->id;
                        $organisme = trim($xmlElements->envoi->consultation->organisme);
                        $serviceId = (int) $xmlElements->envoi->consultation->idDirectionService;
                        if ($serviceId != null) {
                            list($serviceId, $oldServiceId) = $this->getIdService($serviceId, $organisme);
                        }

                        $urlExterneConsultataion = $xmlElements->envoi->consultation->urlExterne;
                        $logger->info('Vérification de droit acces ...');
                        if ($this->verifieAccesToInfo($_GET['ticket'], $organisme, $serviceId)) {
                            $consultationOld = false;
                            $consultationId = $this->getRightIdConsultation($consultationId, $organisme);
                            $logger->info('Vérification de droit acces OK organisme ' . $organisme);
                            if ($consultationId && $organisme) {
                                $logger->info('La récupération de la consultation dont la reference = ' . $consultationId . " et l'organiseme= " . $organisme);
                                $consultationOld = (new Atexo_Consultation())->retrieveConsultation($consultationId, $organisme);
                            }
                            if ($consultationOld instanceof CommonConsultation) {
                                $logger->info('La consultation dont la reference = ' . $consultationId . " et l'organiseme= " . $organisme . ' existe');
                                $consultationFiled = new Atexo_Rest_Action_ConsultationField();
                                $logger->info("Copier les information de la consultation dans l'objet consultationField");
                                $consultationOld->copyInto($consultationFiled);
                                $serilizer = new Atexo_Serializer_Serializer($consultationFiled, Atexo_Rest_Action_ConsultationField::class);
                                $params['nomBatch'] = 'WS-Consultation-modification';
                            } else {
                                $serilizer = new Atexo_Serializer_Serializer();
                            }
                            $xmlProperties = Atexo_Config::getParameter('PATH_FILE_PROPERTIES_CONSULTATION');
                            $logger->info("La récupération de l'objet consultation à partir du xml");
                            $consultationFiled = $serilizer->DeserializeClass($xmlConsultation, $xmlProperties);
                            $consultationFiled->setId($consultationId);
                            $consultationFiled->setServiceId($serviceId);
                            if ($urlExterneConsultataion && ($consultationFiled instanceof Atexo_Rest_Action_ConsultationField)) {
                                $logger->info("L'enregistrement de la valeur de consultation externe = 1");
                                $consultationFiled->setConsultationExterne('1');
                            }

                            //check that the node exists in the XML.
                            if (isset($xmlElements->envoi->consultation->idContratRattachement) && !empty($xmlElements->envoi->consultation->idContratRattachement)) {
                                $idContratRattachement = $xmlElements->envoi->consultation->idContratRattachement;
                                $contrat = (new Atexo_Contrat())->retrieveContratOneByIdContratTitulaire($idContratRattachement);

                                //check if contract does not exists in t_contrat_titulaire
                                if (empty($contrat)) {
                                    return static::generateXmlError('idContratRattachement renseigné : l’identifiant ' . $idContratRattachement . ' ne correspond à aucun contrat existant.');
                                } else {
                                    //contract does exists in t_contrat_titulaire
                                    //check if statut != 4
                                    if (
                                        $contrat->getIdTypeContrat() != Atexo_Config::getParameter('TYPE_CONTRAT_SAD') &&
                                        $contrat->getIdTypeContrat() != self::TYPE_CONTRAT_SYSTEME_ACQUISITION_DYNAMIQUE   &&
                                        $contrat->getIdTypeContrat() != self::TYPE_CONTRAT_AC_MULTI_ATTRIBUTAIRE   &&
                                        $contrat->getStatutContrat() != Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT_EFFECTUEE')
                                    ) {
                                        return static::generateXmlError('idContratRattachement renseigné : le contrat ' . $idContratRattachement . ' n’est pas notifié.');
                                    }

                                    //test sur type du contrat: si marche n'est pas spécifique ou n'est pas subséquent => throw Error
                                    $typeContrat = (new Atexo_TypeContrat())->retrieveContratOneByIdTypeContrat($contrat->getIdTypeContrat());

                                    if (isset($xmlElements->envoi->consultation->contrat) && !empty($xmlElements->envoi->consultation->contrat)) {
                                        $typeContratConsultation = $xmlElements->envoi->consultation->contrat;
                                        //Règle de gestion 3 : le type de contrat renseigné pour la consultation à créer n’est ni « Marché subséquent », ni « Marché spécifique »
                                        if ('MARCHE_SPECIFIQUE' != $typeContratConsultation && 'MARCHE_SUBSEQUENT' != $typeContratConsultation) {
                                            return static::generateXmlError('idContratRattachement renseigné : le type de contrat de la consultation à créer doit être un Marché subséquent ou un Marché spécifique.');
                                        }

                                        //Règle de gestion 4: le type de contrat renseigné pour la consultation à créer est « Marché subséquent »
                                        // le type du contrat de rattachement renseigné dans la balise <idContratRattachement> n’est pas « Accord-cadre »
                                        if ('MARCHE_SUBSEQUENT' == $typeContratConsultation && $typeContrat->getAccordCadreSad() !== Atexo_Config::getParameter('TYPE_CONTRAT_ACCORD_CADRE')) {
                                            return static::generateXmlError('idContratRattachement renseigné : le type de contrat de la consultation à créer étant Marché subséquent, le contrat de rattachement doit être un Accord-cadre.');
                                        }

                                        //Règle de gestion 5 : le type de contrat renseigné pour la consultation à créer est « Marché spécifique »
                                        //le type du contrat de rattachement renseigné dans la balise <idContratRattachement> n’est pas « SAD »
                                        if ('MARCHE_SPECIFIQUE' == $typeContratConsultation && $typeContrat->getAccordCadreSad() !== Atexo_Config::getParameter('TYPE_CONTRAT_SAD')) {
                                            return static::generateXmlError('idContratRattachement renseigné : le type de contrat de la consultation à créer étant un Marché spécifique, le contrat de rattachement doit être un SAD.');
                                        }

                                        $consultationFiled->setIdContrat($idContratRattachement);
                                        $consultationFiled->setNumeroAc($contrat->getNumeroContrat());
                                    }
                                }
                            } else {
                                $consultationFiled->setIdContrat(0);
                                $consultationFiled->setNumeroAc(null);
                            }

                            $logger->info('Debut traitement des fichiers DCE, RC ');
                            if (isset($_FILES) && is_array($_FILES) && count($_FILES)) {
                                $logger->info('La récupération des fichiers de la consultation');
                                if ($_FILES['DCE']['tmp_name']) {
                                    if (filesize($_FILES['DCE']['tmp_name']) <= Atexo_Config::getParameter('MAX_SIZE_FILE_WS_REST')) {
                                        $logger->info('La récupération du fichier DCE  = ' . $_FILES['DCE']['name']);
                                        $infileDce = Atexo_Config::getParameter('COMMON_TMP') . 'dce' . session_id() . time();
                                        if (move_uploaded_file($_FILES['DCE']['tmp_name'], $infileDce)) {
                                            $consultationFiled->setDceContent($infileDce);
                                            $consultationFiled->setNomFichierDce($_FILES['DCE']['name']);
                                            $logger->info('DCE existe');
                                        } else {
                                            $logger->info('move_uploaded_file probleme reception  DCE _FILE : ' . $_FILES['DCE']['tmp_name'] . ' Destination : ' . $infileDce);
                                        }
                                    } else {
                                        $this->getHeaders('413');
                                        $logger->error('La taille du fichier DCE depasse la taille maximum');

                                        try {
                                            $params['oversight'] = self::oversight($params);
                                            unset($params['oversight']);
                                        } catch (Exception $exceptionOversight) {
                                            $logger = Atexo_LoggerManager::getLogger('supervision');
                                            $logger->error('Error sur la supervision : ' . $exceptionOversight->getMessage());
                                        }

                                        return static::generateXmlError('La taille du fichier DCE depasse la taille maximum');
                                    }
                                }
                                if ($_FILES['RC']['tmp_name']) {
                                    if (filesize($_FILES['RC']['tmp_name']) <= Atexo_Config::getParameter('MAX_SIZE_FILE_WS_REST')) {
                                        $infileRc = Atexo_Config::getParameter('COMMON_TMP') . 'rc' . session_id() . time();
                                        $logger->info('La récupération du fichier RC  = ' . $_FILES['RC']['name']);
                                        if (move_uploaded_file($_FILES['RC']['tmp_name'], $infileRc)) {
                                            $consultationFiled->setRcContent($infileRc);
                                            $consultationFiled->setNomFichierRc($_FILES['RC']['name']);
                                            $logger->info('RC existe');
                                        } else {
                                            $logger->info('move_uploaded_file probleme reception  RC _FILE : ' . $_FILES['RC']['tmp_name'] . ' Destination : ' . $infileRc);
                                        }
                                    } else {
                                        $this->getHeaders('413');
                                        $logger->error('La taille du fichier RC depasse la taille maximum');

                                        try {
                                            $params['oversight'] = self::oversight($params);
                                        } catch (Exception $exceptionOversight) {
                                            $logger = Atexo_LoggerManager::getLogger('supervision');
                                            $logger->error('Error sur la supervision : ' . $exceptionOversight->getMessage());
                                        }

                                        return static::generateXmlError('La taille du fichier RC depasse la taille maximum');
                                    }
                                }

                                if ($_FILES['PUBLICITE']['tmp_name']) {
                                    if (filesize($_FILES['PUBLICITE']['tmp_name']) <= Atexo_Config::getParameter('MAX_SIZE_FILE_WS_REST')) {
                                        $infilePublicite = Atexo_Config::getParameter('COMMON_TMP') . 'publicite' . session_id() . time();
                                        $logger->info('La récupération du fichier publicite  = ' . $_FILES['PUBLICITE']['name']);
                                        if (move_uploaded_file($_FILES['PUBLICITE']['tmp_name'], $infilePublicite)) {
                                            $consultationFiled->setFichierPubliciteContent($infilePublicite);
                                            $consultationFiled->setNomFichierPublicite($_FILES['PUBLICITE']['name']);
                                            $logger->info('fichier publicite existe');
                                        } else {
                                            $logger->info('move_uploaded_file probleme reception  fichier publicite _FILE : ' . $_FILES['PUBLICITE']['tmp_name'] . ' Destination : ' . $infilePublicite);
                                        }
                                    } else {
                                        $this->getHeaders('413');
                                        $logger->error('La taille du fichier publicite depasse la taille maximum');

                                        try {
                                            $params['oversight'] = self::oversight($params);
                                        } catch (Exception $exceptionOversight) {
                                            $logger = Atexo_LoggerManager::getLogger('supervision');
                                            $logger->error('Error sur la supervision : ' . $exceptionOversight->getMessage());
                                        }

                                        return static::generateXmlError('La taille du fichier publicite depasse la taille maximum');
                                    }
                                }
                            }

                            try {
                                $resultat = (new Atexo_Consultation())->createConsultation($consultationFiled, $oldServiceId);
                            } catch (\Throwable $throwable) {
                                $trace = 'Error technique : '
                                    . $throwable->getMessage()
                                    . ' stack trace : '
                                    . $throwable->getTraceAsString();
                                $logger->error($trace);
                                $this->getHeaders('500');
                                $this->setXMLForLog($xml);
                                return static::generateXmlError($trace);
                            }

                            if ($resultat instanceof Atexo_Rest_Action_ConsultationField) {
                                $xmlProperties = Atexo_Config::getParameter('PATH_FILE_MAPPING_CONSULTATION');
                                $xmlHiddenField = Atexo_Config::getParameter('PATH_FILE_HIDDEN_FIELD_CONSULTATION');
                                $listOptioanlField = Atexo_Config::getParameter('PATH_FILE_OPTIONAL_FIELD_CONSULTATION');
                                $logger->info("La récupération de l'xml à partir de l'objet consultation ");
                                $xmlResult = $serilizer->SerializeClass($resultat, Atexo_Rest_Action_ConsultationField::class, $xmlProperties, $xmlHiddenField, $listOptioanlField);
                                $xmlResult = (new Atexo_Rest_Action_Actions())->encapsulerNodeResponseWsMpe($xmlResult, 'consultation');
                                if (
                                    0 !== $consultationFiled->getId()
                                    && null !== $this->saveClauses(
                                        $xmlElements,
                                        $consultationFiled->getId(),
                                        Atexo_Util::atexoHtmlEntities($_GET['ticket'])
                                    )
                                ) {
                                    $xmlResult = $this->addClausesInXMLElement(simplexml_load_string($xmlResult), $consultationFiled->getId());
                                }
                                $this->getHeaders('200');
                            } else {
                                $this->getHeaders('400');
                                $logger->error($resultat);
                                $xmlResult = static::generateXmlError($resultat);
                                $this->setXMLForLog($xml);
                            }
                        } else {
                            $this->getHeaders('403');
                            $logger->error(
                                'vérification acces KO pr ticket ' .
                                $_GET['ticket'] . 'organisme ' . $organisme . ' service ' . $serviceId
                            );
                            $xmlResult = static::generateXmlError($this->getMessageErrorForAccessDenied($organisme, $serviceId));
                            $this->setXMLForLog($xml);
                        }
                    } else {
                        $this->getHeaders('400');

                        try {
                            $params['oversight'] = self::oversight($params);
                        } catch (Exception $exceptionOversight) {
                            $logger = Atexo_LoggerManager::getLogger('supervision');
                            $logger->error('Error sur la supervision : ' . $exceptionOversight->getMessage());
                        }

                        $this->setXMLForLog($xml);

                        return static::generateXmlError('La structure du fichier xml ne respecte pas le schema approprie');
                    }
                } else {
                    $logger->error("Il n'y a pas du fichier xml à traiter");
                    $this->getHeaders('400');
                }
            } else {
                try {
                    //@todo : Rajouter dans la nomenclature une erreur d'identification !
                    $params['code'] = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
                    $params['oversight'] = self::oversight($params);
                } catch (Exception $exceptionOversight) {
                    $logger = Atexo_LoggerManager::getLogger('supervision');
                    $logger->error('Error sur la supervision : ' . $exceptionOversight->getMessage());
                }

                return $retour;
            }
        } catch (\Exception $e) {
            $logger->error('Erreur technique = ' . $e->getMessage());
            $this->getHeaders('500');
            $xmlResult = static::generateXmlError('Erreur technique');
            $params['code'] = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
        }
        $logger->info('Fin....');
        $log = date('Y-m-d H:i:s') . ' IP ' . $_SERVER['REMOTE_ADDR'] . ' output : ' . print_r($xmlResult, true) . " \n ";
        Atexo_Util::writeFile(Atexo_Config::getParameter('BASE_ROOT_DIR') . '/logs/wsRestConsultation.log', $log, 'a');

        try {
            self::oversight($params);
        } catch (Exception $exceptionOversight) {
            $logger = Atexo_LoggerManager::getLogger('supervision');
            $logger->error('Error sur la supervision : ' . $exceptionOversight->getMessage());
        }
        return $xmlResult;
    }

    /**
     * permet recuperer une consultation
     * à partir de sa reference et l'organisme.
     *
     * @param     string $organisme l'organisme,Integer $refCons la reference de la consultation
     *
     * @return    string xml contenant les infos de la consultation
     *
     * @author    LEZ <loubna.ezziani@atexo.com>
     *
     * @version   1.0
     *
     * @since     4.6.0
     *
     * @copyright Atexo 2014
     */
    public function get($organisme, $consultationId): string
    {
        $logger = null;
        $params = [];
        try {
            $params['debutExecution'] = new DateTime('now');
            $params['nomBatch'] = 'WS-Consultation-récupération';
            $params['service'] = 'Consultation';
            $entree = serialize(['organisme' => $organisme, 'refConsutation' => $consultationId]);
            $params['variablesEntree'] = $entree;

            $params['typeFlux'] = 'WebService';
            $params['poids'] = 0;
            $params['ticket'] = Atexo_Util::atexoHtmlEntities($_GET['ticket']);
            self::oversight($params);
            $params['code'] = Atexo_Oversight_Oversight::$codeSuccess;
        } catch (Exception $exceptionOversight) {
            $logger = Atexo_LoggerManager::getLogger('supervision');
            $logger->error('Error sur la supervision : ' . $exceptionOversight->getMessage());
        }

        try {
            if (!isset($logger)) {
                $logger = Atexo_LoggerManager::getLogger('consultation');
            }
            $logger->info('Debut GET Consultation....');
            $consultationId = $this->getRightIdConsultation($consultationId, $organisme);
            $acronymeOrg = null;
            $refConsultation = null;
            $logger->info('vérification du ticket');
            $retour = $this->validateTicket($logger);
            if (true === $retour) {
                $logger->info('La recuperation de la consultation dont la referecne = ' . $consultationId . " et l'organisme = " . $organisme . '....');
                $consultation = (new Atexo_Consultation())->retrieveConsultation($consultationId, $organisme);
                if (($consultation instanceof CommonConsultation) && $consultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION')) {
                    $logger->info('Vérification de droit acces ...');
                    if ($this->verifieAccesToInfo($_GET['ticket'], $organisme, $consultation->getServiceId())) {
                        $logger->info('Vérification de droit acces OK organisme ' . $organisme);
                        if (0 === $consultation->getIdContrat()) {
                            $consultation->setIdContrat(null);
                        }
                        // On renvoi le oldIdService pour clients spécifiques
                        if (true === (bool) getenv('SPECIFIQUE_PLACE')) {
                            $oldIdService = $this->getOldIdServiceByIdService($consultation->getServiceId());
                            $consultation->setServiceId($oldIdService);
                        }
                        $xmlResult = self::getXmlConsultation($consultation, $logger);
                        $xmlResult = $this->addClausesInXMLElement(simplexml_load_string($xmlResult), $consultationId);
                        $this->getHeaders('200');
                    } else {
                        $this->getHeaders('403');
                        $logger->error(
                            'vérification acces KO pr ticket ' .
                            $_GET['ticket'] . 'organisme ' . $organisme . ' service ' . $consultation->getServiceId()
                        );
                        $xmlResult = static::generateXmlError($this->getMessageErrorForAccessDenied($organisme, $consultation->getServiceId()));
                    }
                } else {
                    $this->getHeaders('400');
                    $logger->error("La consultation n'existe pas");
                    $xmlResult = static::generateXmlError("La consultation n'existe pas");
                }
            } else {
                try {
                    $params['code'] = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
                    self::oversight($params);
                } catch (Exception $exceptionOversight) {
                    $logger = Atexo_LoggerManager::getLogger('supervision');
                    $logger->error('Error sur la supervision : ' . $exceptionOversight->getMessage());
                }

                return $retour;
            }
        } catch (\Exception $e) {
            $params['code'] = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
            $logger->error('Erreur technique = ' . $e->getMessage() . $e->getTraceAsString());
            $this->getHeaders('500');
            $xmlResult = static::generateXmlError('Erreur technique');
        }
        $logger->info('Fin  GET Consultation....');

        try {
            self::oversight($params);
        } catch (Exception $exceptionOversight) {
            $logger = Atexo_LoggerManager::getLogger('supervision');
            $logger->error('Error sur la supervision : ' . $exceptionOversight->getMessage());
        }

        return $xmlResult;
    }

    public function getXmlConsultation($consultation, $logger)
    {
        $consultationField = new Atexo_Rest_Action_ConsultationField($consultation);

        $xmlPropertiesFilePath = Atexo_Config::getParameter('PATH_FILE_MAPPING_CONSULTATION');
        $xmlHiddenFieldsFilePath = Atexo_Config::getParameter('PATH_FILE_HIDDEN_FIELD_CONSULTATION');
        $optionalFieldsFilePath = Atexo_Config::getParameter('PATH_FILE_OPTIONAL_FIELD_CONSULTATION');
        $logger->info("La récupération de l'xml à partir de l'objet consultation");
        $serializer = new Atexo_Serializer_Serializer();

        $xmlResult = $serializer->SerializeClass(
            $consultationField,
            Atexo_Rest_Action_ConsultationField::class,
            $xmlPropertiesFilePath,
            $xmlHiddenFieldsFilePath,
            $optionalFieldsFilePath
        );

        return (new Atexo_Rest_Action_Actions())->encapsulerNodeResponseWsMpe($xmlResult, 'consultation');
    }

    private function saveClauses(\SimpleXMLElement $xmlElements, int $idConsultation, string $ticket): bool
    {
        return $this->clauseUseApiV0->saveClausesConsultation($xmlElements, $idConsultation, $ticket);
    }

    private function addClausesInXMLElement(\SimpleXMLElement $xmlElements, int $idConsultation): string
    {
        return $this->clauseUseApiV0->addClausesConsultationInXMLElement($xmlElements, $idConsultation);
    }

    private function setXMLForLog($xml) {
        $loggerXML = Atexo_LoggerManager::getLogger('consultation');
        $loggerXML->error($xml);
    }
}
