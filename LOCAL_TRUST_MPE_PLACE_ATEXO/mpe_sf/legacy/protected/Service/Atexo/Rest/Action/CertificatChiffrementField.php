<?php

namespace Application\Service\Atexo\Rest\Action;

use Application\Propel\Mpe\CommonCertificatChiffrement;
use Application\Service\Atexo\Atexo_Util;

/**
 * Contient des attributs et des redeclarations des methodes pour CommonCertificatChiffrement.
 *
 * @author LEZ <loubna.ezziani@atexo.com>
 * @copyright Atexo 2014
 *
 * @version 1.0
 *
 * @since 4.6.0
 */
class Atexo_Rest_Action_CertificatChiffrementField extends CommonCertificatChiffrement
{
    protected ?string $_type = null;
    //private $_name       = "";
    //private $_secours    = null;

    private static array $mpeTypeEnveloppe = [
            'CANDIDATURE' => 1,
            'OFFRE' => 2,
            'ANONYMAT' => 3,
            'OFFRE_TECHNIQUE' => 4,
            'REPONSE' => 5,
    ];

    /**
     * Permet d'initialiser les valeurs de l'objet Atexo_Rest_Action_CertificatChiffrementField.
     *
     * @param CommonCertificatChiffrement $certificat      optionnel le certificat d'ou on va copier les valeurs
     * @param string                      $typeCertificat: type de certificat, envoyé dans le cas de mode d'ouverture par "REPONSE"
     *
     * @return Atexo_Rest_Action_CertificatChiffrementField une instance de ce objet
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function __construct($certificat = null, $typeCertificat = null)
    {
        if ($certificat instanceof CommonCertificatChiffrement) {
            $this->setCertificat(base64_encode($certificat->getCertificat()));
            if (null == $typeCertificat) {
                $typeCertificat = $certificat->getTypeEnv();
            }
            $this->setTypeEnv($typeCertificat);
        }
    }

    /**
     * Set the value of [_secours] column.
     *
     * @param string $v new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    //  public function setSecours($v)
//     {
    //      $v = Atexo_Util::getIntFromValboolean(strtolower($v));
//          $this->_secours = $v;
//     } // setSecours()

    /**
     * Get the value of [_secours] column.
     *
     * @return void
     *
     * @author GCH <gregory.chevret@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
//     private function getSecours()
//     {
//         return Atexo_Util::getBooleanStringFromValInt($this->_secours);
//     } // getSecours()

    /**
     * Set the value of [_name] column.
     *
     * @param string $v new value
     *
     * @return void
     *
     * @author GCH <gregory.chevret@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
//     public function setName($v)
//     {
//         $v = Atexo_Util::getIntFromValboolean(strtolower($v));
//         $this->_name = $v;
//     } // setName()

    /**
     * Get the value of [_name] column.
     *
     * @param string $v new value
     *
     * @return void
     *
     * @author GCH <gregory.chevret@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
//     private function getName()
//     {
//         return $this->_name;
//     } // getName()

    /**
     * Set the value of [_type] column.
     *
     * @param string $v new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     * @author GCH <gregory.chevret@atexo.com>
     *
     * @version 1.1
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setType($v)
    {
        $this->_type = $v;
        $this->setTypeEnv(self::$mpeTypeEnveloppe[$v]);
    }

    // setType()

    /**
     * get the value of [_type] column.
     *
     * @return void
     *
     * @author GCH <gregory.chevret@atexo.com>
     *
     * @version 1.1
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getType()
    {
        $typeEnv = array_keys(self::$mpeTypeEnveloppe, $this->getTypeEnv());

        return $typeEnv[0];
    }

    // getType()

    /**
     * Set the value of [certificat] column.
     *
     * @param string $v new value
     *
     * @return CommonCertificatChiffrement The current object (for fluent API support)
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setCertificat($v)
    {
        return parent::setCertificat(base64_decode($v));
    }

    // setCertificat()

    /**
     * Get the value of [certificat] column.
     *
     * @return objet stdClass
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getCertificatElement()
    {
        $std = new \stdClass();
        //      $certificat = parent::getCertificat();
//      $std->name = $this->getName();
        $std->type = $this->getType();
//      $std->secours = $this->getSecours();
        $std->certificat = base64_encode($this->getCertificat());

        return $std;
    }
}
