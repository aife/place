<?php

namespace Application\Service\Atexo\Rest\Action;

use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Inscrit;
use Application\Service\Atexo\Oversight\Atexo_Oversight_Oversight;
use DateTime;
use DOMDocument;
use Exception;

/*
 * permet de recuperer une liste des contacts ayant l'id de l'entreprise fournie en parametre
 *
 * @author ASO <ayoub.souidahmed@atexo.com>
 * @version 1.0
 * @since 4.7.0
 * @copyright Atexo 2014
 * @package Atexo_Rest_Action
 */
class Atexo_Rest_Action_ContactsIdEse extends Atexo_Rest_Action_Actions
{
    /*
     * genere un xml (valide ws MPE) contenant les informations des contacts ayant l'id de l'entreprise fournie en parametre
     *
     * @param Integer $idEse l'id de l'entreprise
     * @return string xml contenant les infos des contacts
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @version 1.0
     * @since 4.7.0
     * @copyright Atexo 2014
     */
    public function get($idEse)
    {
        $logger = null;
        $params = [];
        try {
            $params['debutExecution'] = new DateTime('now');
            $params['nomBatch'] = 'WS-contact-by-id';
            $params['service'] = 'Contact';
            $entree = serialize(['idEse' => $idEse]);
            $params['variablesEntree'] = $entree;
            $params['informationMetier'] = $entree;
            $params['typeFlux'] = 'WebService';
            $params['poids'] = 0;
            $params['nbFlux'] = 1;
            $params['ticket'] = Atexo_Util::atexoHtmlEntities($_GET['ticket']);
            self::oversight($params);
            $params['code'] = Atexo_Oversight_Oversight::$codeSuccess;
        } catch (Exception $exceptionOversight) {
            $logger = Atexo_LoggerManager::getLogger('supervision');
            $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
        }

        try {
            $logger = Atexo_LoggerManager::getLogger();
            $retour = $this->validateTicket($logger);
            if (true === $retour) {
                $logger->info("La récupération des contacts a partir de l'idEntreprise  = ".$idEse);
                $contacts = (new Atexo_Entreprise_Inscrit())->retrieveInscritByIdCompnay($idEse);
                if ($contacts) {
                    $logger->info('Le nombre des contacts trouvé est = '.(is_countable($contacts) ? count($contacts) : 0));
                    $contactsNodes = '';
                    foreach ($contacts as $contact) {
                        $xmlResponse = Atexo_Rest_Action_ContactsSiret::getXmlInfosContact($contact);
                        $contactsNodes .= self::getNodeFromXml($xmlResponse, 'contact');
                    }
                    $logger->info("La géneration de l'xml de la liste des contacts");
                    $contactNodes = '<contacts>'.$contactsNodes.'</contacts>';
                    $doc = new DOMDocument();
                    $doc->loadXML(Atexo_Util::toUtf8($contactNodes));
                    $xmlListeContacts = $doc->saveXML();
                    $xmlResult = (new Atexo_Rest_Action_Actions())->encapsulerNodeResponseWsMpe($xmlListeContacts, 'contacts');

                    try {
                        $params['code'] = Atexo_Oversight_Oversight::$codeSuccess;
                        $params['oversight'] = self::oversight($params);
                    } catch (Exception $exceptionOversight) {
                        $logger = Atexo_LoggerManager::getLogger();
                        $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
                    }

                    return $xmlResult;
                } else {
                    $msgError = 'Aucun contact ne correspond à cet IdEntreprise';
                    $logger->error($msgError);
                    $this->getHeaders('404');

                    try {
                        $params['code'] = Atexo_Oversight_Oversight::$codeSuccess;
                        $params['oversight'] = self::oversight($params);
                    } catch (Exception $exceptionOversight) {
                        $logger = Atexo_LoggerManager::getLogger();
                        $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
                    }

                    return self::generateXmlError($msgError);
                }
            } else {
                try {
                    $params['code'] = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
                    $params['oversight'] = self::oversight($params);
                } catch (Exception $exceptionOversight) {
                    $logger = Atexo_LoggerManager::getLogger();
                    $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
                }

                return $retour;
            }
        } catch (Exception $e) {
            $logger->error('Erreur technique = '.$e->getMessage().' '.$e->getTraceAsString());
            $this->getHeaders('500');

            try {
                $params['code'] = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
                $params['oversight'] = self::oversight($params);
            } catch (Exception $exceptionOversight) {
                $logger = Atexo_LoggerManager::getLogger();
                $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
            }

            return self::generateXmlError('erreur techniques');
        }
    }
}
