<?php

namespace Application\Service\Atexo\Rest\Action;

use Application\Propel\Mpe\CommonConsultation;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Message;
use Exception;

/**
 * permet de gérer les echanges via ws.
 *
 * @author LEZ <loubna.ezziani@atexo.com>
 *
 * @version 1.0
 *
 * @since esr-2016
 *
 * @copyright Atexo 2016
 */
class Atexo_Rest_Action_Echanges extends Atexo_Rest_Action_Actions
{
    /**
     * permet de retourner les echanges.
     *
     * @param $organisme l'organisme
     * @param $consultationId la reference d ela consultation
     *
     * @return string xml contenant les information de la decision
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-megalis
     *
     * @copyright Atexo 2016
     */
    public function get($organisme, $consultationId)
    {
        try {
            $logger = Atexo_LoggerManager::getLogger('messec');
            $logger->info('Debut de la récupération de la liste des echanges....');
            $retour = $this->validateTicket($logger);
            if (true === $retour) {
                $consultation = (new Atexo_Consultation())->retrieveConsultation($consultationId, $organisme);
                if (!($consultation instanceof CommonConsultation)
                    ||
                    !($this->verifieAccesToInfo($_GET['ticket'], $consultation->getOrganisme(), $consultation->getServiceId()))
                ) {
                    $consultationExist = ($consultation instanceof CommonConsultation);
                    $log = $consultationExist ?
                        ('Vérification de droit acces KO organisme '.$consultation->getOrganisme().' service '.$consultation->getServiceId()) :
                        ("consultation n'existe pas ".$consultationId.'-'.$organisme);
                    $logger->info($log);
                    $serviceId = $consultationExist ? $consultation->getServiceId() : null;
                    $xml = self::generateXmlError($this->getMessageErrorForAccessDenied($organisme, $serviceId));
                } else {
                    $msgInfoEchange = " l'organisme => ".$organisme.' , reference => '.$consultationId;
                    $logger->info('La récupération des échanges ('.$msgInfoEchange.')');
                    $xmlResponse = (new Atexo_Rest_Action_Echanges())->generateXmlIdsEchanges($consultationId, $organisme);
                    $xml = (new Atexo_Rest_Action_Actions())->encapsulerNodeResponseWsMpe($xmlResponse, 'listeIdRegistre');
                    $this->getHeaders('200');
                    $logger->info('Fin de la récupération de la liste des echanges....');
                }

                return $xml;
            } else {
                return $retour;
            }
        } catch (Exception $e) {
            $logger->error('Erreur technique = '.$e->getMessage().' '.$e->getTraceAsString());
            $this->getHeaders('500');

            return static::generateXmlError('erreur technique');
        }
    }

    /**
     * genere un xml contenant les ids des echanges associes à une consultation.
     *
     * @param $organisme l'organisme
     * @param $consultationId la reference de la consultation
     *
     * @return string xml contenant les Ids des echanges d'une consultation
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-megalis
     *
     * @copyright Atexo 2016
     */
    public function generateXmlIdsEchanges($consultationId, $organisme)
    {
        $id = $this->getRightIdConsultation($consultationId);
        $arrayIds = Atexo_Message::getEchangesIdsByRefConsultation($id, $organisme);
        $xml = '<listeIdRegistre referenceConsultation="'.$consultationId.'">';
        if (is_array($arrayIds) && count($arrayIds)) {
            foreach ($arrayIds as $onId) {
                $xml .= '<id>'.$onId.'</id>';
            }
        }
        $xml .= '</listeIdRegistre>';

        return $xml;
    }
}
