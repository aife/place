<?php

namespace Application\Service\Atexo\Rest\Action;

use Application\Propel\Mpe\CommonOrganisme;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Oversight\Atexo_Oversight_Oversight;
use DateTime;
use Exception;

/**
 * permet de recuperer un organisme.
 *
 * @author LEZ <loubna.ezziani@atexo.com>
 *
 * @version 1.0
 *
 * @since esr-2015
 *
 * @copyright Atexo 2015
 */
class Atexo_Rest_Action_Organisme extends Atexo_Rest_Action_Actions
{
    /**
     * permet recuperer un organisme
     * à partir de son acronyme.
     *
     * @param string $organisme l'acronyme de l'organisme
     *
     * @return string xml contenant les infos de l'organisme
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function get($acronymeOrg)
    {
        $params = [];
        $params['debutExecution'] = new DateTime('now');
        $params['nomBatch'] = 'WS-organismes-get';
        $params['service'] = 'Organisme';
        $params['typeFlux'] = 'WebService';
        $params['variablesEntree'] = serialize(['organisme' => $acronymeOrg]);
        $params['poids'] = 0;
        $params['nbFlux'] = 1;
        $params['ticket'] = Atexo_Util::atexoHtmlEntities($_GET['ticket']);
        self::oversight($params);
        $params['code'] = Atexo_Oversight_Oversight::$codeSuccess;

        try {
            $logger = Atexo_LoggerManager::getLogger();
            $logger->info('Debut GET Organisme....');
            $retour = $this->validateTicket($logger);
            if (true === $retour) {
                $logger->info("La recuperation de l'organisme = ".$acronymeOrg.'....');
                $organisme = Atexo_Organismes::retrieveOrganismeByAcronyme($acronymeOrg, true);
                if ($organisme instanceof CommonOrganisme) {
                    $arrayLogos = [];
                    $withLogo = isset($_GET['withLogo']) ? Atexo_Util::castToBoolean($_GET['withLogo']) : false;
                    $xmlResult = self::getXmlResultOrganisme($organisme, $arrayLogos, $logger, $withLogo);
                    $xmlValide = Atexo_Util::validateXmlBySchema($xmlResult, Atexo_Config::getParameter('PATH_FILE_XSD_WS'), $logger);
                    if ($xmlValide) {
                        $logger->info("L'xml est valide");
                        $log = date('Y-m-d H:i:s').' IP '.$_SERVER['REMOTE_ADDR'].' output : '.print_r($xmlResult, true)." \n ";
                        Atexo_Util::writeFile(Atexo_Config::getParameter('BASE_ROOT_DIR').'/logs/wsRestOrganisme.log', $log, 'a');
                        $this->getHeaders('200');
                        if (!$withLogo || empty($arrayLogos)) {
                            try {
                                $params['finExecution'] = 'endScript';
                                $params['oversight'] = self::oversight($params);
                                unset($params['finExecution']);
                                unset($params['oversight']);
                            } catch (Exception $exceptionOversight) {
                                $logger = Atexo_LoggerManager::getLogger('supervision');
                                $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
                            }

                            return $xmlResult;
                        } else {
                            self::sendDataWithFile($arrayLogos, $xmlResult, 'binary', 'image/jpeg', false);
                        }
                    } else {
                        throw new Exception("la structure de l'xml genéré est invalide ");
                    }
                } else {
                    $this->getHeaders('400');
                    $logger->error("L'organisme dont l'acronyme est ".$acronymeOrg." n'existe pas");
                    $logger->info('Fin  GET Organisme....');

                    try {
                        $params['finExecution'] = 'endScript';
                        $params['oversight'] = self::oversight($params);
                        unset($params['finExecution']);
                        unset($params['oversight']);
                    } catch (Exception $exceptionOversight) {
                        $logger = Atexo_LoggerManager::getLogger('supervision');
                        $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
                    }

                    return static::generateXmlError("L'organisme dont l'acronyme est ".$acronymeOrg." n'existe pas");
                }
            } else {
                try {
                    $params['code'] = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
                    $params['finExecution'] = 'endScript';
                    $params['oversight'] = self::oversight($params);
                    unset($params['finExecution']);
                    unset($params['oversight']);
                } catch (Exception $exceptionOversight) {
                    $logger = Atexo_LoggerManager::getLogger('supervision');
                    $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
                }

                return $retour;
            }
        } catch (Exception $e) {
            $logger->error('Erreur technique = '.$e->getMessage().' '.$e->getTraceAsString());
            $this->getHeaders('500');
            $logger->info('Fin  GET Organisme....');

            try {
                $params['code'] = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
                self::oversight($params);
            } catch (Exception $exceptionOversight) {
                $logger = Atexo_LoggerManager::getLogger('supervision');
                $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
            }

            return static::generateXmlError('Erreur technique');
        }
        $logger->info('Fin  GET Organisme....');
    }
}
