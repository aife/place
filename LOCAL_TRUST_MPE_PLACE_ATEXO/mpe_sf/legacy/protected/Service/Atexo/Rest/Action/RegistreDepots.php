<?php

namespace Application\Service\Atexo\Rest\Action;

use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationQuery;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Responses;
use Application\Service\Atexo\Oversight\Atexo_Oversight_Oversight;
use DateTime;
use Exception;

/*
 * permet de gérer le registre des depots
 *
 * @author : LEZ <loubna.ezziani@atexo.com>
 * @version : 1.0
 * @since : 4.6.0
 * @copyright Atexo 2014
 * @package  Atexo_Rest_Action
 */
class Atexo_Rest_Action_RegistreDepots extends Atexo_Rest_Action_Actions
{
    /*
     * Permet d'avoir les ids des depot d'une consultation by reference
     *
     * @param Integer $consultationId,la reference de la consultation concernee
     * @return  string un xml de reponse du registre de depots contenant la liste des ids depots
     * @author LEZ <loubna.ezziani@atexo.com>
     * @version 1.0
     * @since 4.6.0
     * @copyright Atexo 2014
     */
    public function get($consultationId)
    {
        $params = [];
        try {
            $params['debutExecution'] = new DateTime('now');
            $params['nomBatch'] = 'WS-dépôt-disponible-get';
            $params['service'] = 'Registre';
            $params['typeFlux'] = 'WebService';
            $params['poids'] = 0;
            $params['nbFlux'] = 1;
            $params['ticket'] = Atexo_Util::atexoHtmlEntities($_GET['ticket']);
            self::oversight($params);
            $params['code'] = Atexo_Oversight_Oversight::$codeSuccess;
        } catch (Exception $exceptionOversight) {
            $logger = Atexo_LoggerManager::getLogger('supervision');
            $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
        }

        try {
            $logger = Atexo_LoggerManager::getLogger('crypto');
            $logger->info('La vérification du ticket');
			$retour = $this->validateTicket($logger);
            $rightIdConsultation = $this->getRightIdConsultation($consultationId);
			if(true === $retour) {
                $consultation = CommonConsultationQuery::create()->findOneById($rightIdConsultation);
                if (!($consultation instanceof CommonConsultation)
                    ||
                    !($this->verifieAccesToInfo($_GET['ticket'], $consultation->getOrganisme(), $consultation->getServiceId()))) {
                    $consultationExist = ($consultation instanceof CommonConsultation);
                    $log = $consultationExist ?
                        ('Vérification de droit acces KO organisme '.$consultation->getOrganisme().' service '.$consultation->getServiceId()) :
                        ("consultation n'existe pas  ".$rightIdConsultation);
                    $logger->info($log);
                    $serviceId = $consultationExist ? $consultation->getServiceId() : null;
                    $organisme = $consultationExist ? $consultation->getOrganisme() : null;

                    return self::generateXmlError($this->getMessageErrorForAccessDenied($organisme, $serviceId));
                }
                $xmlResponse = $this->generateXmlIdsDepots($consultationId,$rightIdConsultation);
                $logger->info("La géneration de l'xml des ids depots");
                $xml = $this->encapsulerNodeResponseWsMpe($xmlResponse, 'listeIdRegistre');
                $this->getHeaders('200');

                try {
                    self::oversight($params);
                } catch (Exception $exceptionOversight) {
                    $logger = Atexo_LoggerManager::getLogger('supervision');
                    $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
                }

                return $xml;
            } else {
                try {
                    $params['code'] = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
                    $params['oversight'] = self::oversight($params);
                } catch (Exception $exceptionOversight) {
                    $logger = Atexo_LoggerManager::getLogger('supervision');
                    $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
                }

                return $retour;
            }
        } catch (Exception $e) {
            try {
                $params['code'] = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
                $params['oversight'] = self::oversight($params);
            } catch (Exception $exceptionOversight) {
                $logger = Atexo_LoggerManager::getLogger('supervision');
                $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
            }

            $logger->error('Erreur technique = '.$e->getMessage().' '.$e->getTraceAsString());
            $this->getHeaders('500');

            return static::generateXmlError('Erreur technique');
        }
    }

    /*
     * genere un xml contenant les ids des depots associes a une consultation
     *
     * @param Integer $refCons reference de la consultation
     * @return string xml contenant les Ids des depots d'une consultation
     * @author LEZ <loubna.ezziani@atexo.com>
     * @version 1.0
     * @since 4.6.0
     * @copyright Atexo 2014
     */
    public function generateXmlIdsDepots($consultationId,$rightIdConsultation){
        $arrayIds = (new Atexo_Consultation_Responses())->getIdsOffresByIdConsultation($rightIdConsultation);
        $xml = '<listeIdRegistre referenceConsultation="'.$consultationId.'">';
        if (is_array($arrayIds) && count($arrayIds)) {
            foreach ($arrayIds as $onId) {
                $xml .= '<id>'.$onId.'</id>';
            }
        }
        $xml .= '</listeIdRegistre>';

        return $xml;
    }
}
