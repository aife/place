<?php

namespace Application\Service\Atexo\Rest\Action;

use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationQuery;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Oversight\Atexo_Oversight_Oversight;
use Application\Service\Atexo\Registres\Atexo_Registres_Questions;
use AtexoCrypto\Dto\Consultation;
use DateTime;
use Exception;

/*
 * permet de recuperer la liste des id des question d'une consultation
 *
 * @author ASO <ayoub.souidahmed@atexo.com>
 * @version 1.0
 * @since 4.6.0
 * @copyright Atexo 2014
 * @package Atexo_Rest_Action
 */
class Atexo_Rest_Action_RegistreQuestions extends Atexo_Rest_Action_Actions
{
    /*
     * genere un xml (valide ws MPE) contenant les ids des questions associes à une consultation
     *
     * @param integer $refCons,la reference de la consultation
     * @return string xml,contenant les infos de la question
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @version  1.0
     * @since 4.6.0
     * @copyright Atexo 2014
     */
    public function get($consultationId)
    {
        $params = [];
        try {
            $params['debutExecution'] = new DateTime('now');
            $params['nomBatch'] = 'WS-questionConsultation-disponible';
            $params['service'] = 'Registre';
            $entree = serialize(['consultation' => $consultationId]);
            $params['variablesEntree'] = $entree;
            $params['informationMetier'] = $entree;
            $params['typeFlux'] = 'WebService';
            $params['poids'] = 0;
            $params['nbFlux'] = 1;
            $params['ticket'] = Atexo_Util::atexoHtmlEntities($_GET['ticket']);
            self::oversight($params);
            $params['code'] = Atexo_Oversight_Oversight::$codeSuccess;
        } catch (Exception $exceptionOversight) {
            $logger = Atexo_LoggerManager::getLogger('supervision');
            $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
        }

        try {
            $logger = Atexo_LoggerManager::getLogger();
            $refConsultation = null;
            $logger->info('vérification du ticket');
			$retour = $this->validateTicket($logger);
            $rightIdConsultation = $this->getRightIdConsultation($consultationId);
			if(true === $retour) {
                $consultation = CommonConsultationQuery::create()->findOneById($rightIdConsultation);
                if ($consultation instanceof CommonConsultation) {
                    $logger->info('verification des droits acces.... ');
                    if ($this->verifieAccesToInfo($_GET['ticket'], $consultation->getOrganisme(), $consultation->getServiceId())) {
                        $logger->info('verification des droits acces OK'.$consultation->getId());
                        $xmlResponse = self::generateXmlIdsQuestions($consultationId,$rightIdConsultation);
                        $xml = self::encapsulerNodeResponseWsMpe($xmlResponse, 'listeIdRegistre');
                        $this->getHeaders('200');
                        $logger->info("creer l'xml de la liste des ids des questions");
                        try {
                            self::oversight($params);
                        } catch (Exception $exceptionOversight) {
                            $logger = Atexo_LoggerManager::getLogger('supervision');
                            $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
                        }
                    } else {
                        $logger->error('verification des droits acces KO org '.$consultation->getOrganisme().' service '.$consultation->getServiceId());
                        $xml = self::generateXmlError($this->getMessageErrorForAccessDenied($consultation->getOrganisme(), $consultation->getServiceId()));
                    }
                } else {
                    $logger->error("Consultation $consultationId n'existe pas");
                    $xml = self::generateXmlError($this->getMessageErrorForAccessDenied());
                }

                return $xml;
            } else {
                try {
                    $params['code'] = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
                    self::oversight($params);
                } catch (Exception $exceptionOversight) {
                    $logger = Atexo_LoggerManager::getLogger('supervision');
                    $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
                }

                return $retour;
            }
        } catch (Exception $e) {
            try {
                $params['code'] = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
                self::oversight($params);
            } catch (Exception $exceptionOversight) {
                $logger = Atexo_LoggerManager::getLogger('supervision');
                $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
            }

            $this->getHeaders('500');
            $logger->error('Erreur technique = '.$e->getMessage().' '.$e->getTraceAsString());

            return self::generateXmlError('erreur techniques');
        }
    }

    /*
     * genere un xml contenant les ids des questions associes à une consultation
     *
     * @param integet $refCons,reference de la consultation
     * @return string xml contenant les Ids des questions d'une consultation
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @version 1.0
     * @since 4.6.0
     * @copyright Atexo 2014
     */
    public function generateXmlIdsQuestions($consultationId,$rightIdConsultation){
        $arrayIds = (new Atexo_Registres_Questions())->getAllQuestionsByConsultation($rightIdConsultation, false, false, null, true);
        $xml = '<listeIdRegistre referenceConsultation="'.$consultationId.'">';
        if (is_array($arrayIds) && count($arrayIds)) {
            foreach ($arrayIds as $onId) {
                $xml .= '<id>'.$onId.'</id>';
            }
        }
        $xml .= '</listeIdRegistre>';

        return $xml;
    }
}
