<?php

namespace Application\Service\Atexo\Rest\Action;

use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationQuery;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Oversight\Atexo_Oversight_Oversight;
use Application\Service\Atexo\RetraitDce\Atexo_RetraitDce_TelechargementsDce;
use DateTime;
use Exception;

/*
 * permet de recuperer la liste des id des RetraitDce liés à une consultation
 *
 * @author ASO <ayoub.souidahmed@atexo.com>
 * @version 1.0
 * @since 4.6.0
 * @copyright Atexo 2014
 * @package  Atexo_Rest_Action
 */
class Atexo_Rest_Action_RegistreRetraitDces extends Atexo_Rest_Action_Actions
{
    /*
     * genere un xml (valide ws MPE) contenant les ids des RetraitDce associes à une consultation
     *
     * @param integer $refCons,la reference de la consultation
     * @return string xml contenant les infos de la RetraitDce
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @version 1.0
     * @since 4.6.0
     * @copyright Atexo 2014
     */
    public function get($consultationId)
    {
        $params = [];
        try {
            $params['debutExecution'] = new DateTime('now');
            $params['nomBatch'] = 'WS-RetraitDCEConsultation-disponible';
            $params['service'] = 'Registre';
            $entree = serialize(['consultation' => $consultationId]);
            $params['variablesEntree'] = $entree;
            $params['informationMetier'] = $entree;
            $params['typeFlux'] = 'WebService';
            $params['poids'] = 0;
            $params['nbFlux'] = 1;
            $params['ticket'] = Atexo_Util::atexoHtmlEntities($_GET['ticket']);
            self::oversight($params);
            $params['code'] = Atexo_Oversight_Oversight::$codeSuccess;
        } catch (Exception $exceptionOversight) {
            $logger = Atexo_LoggerManager::getLogger();
            $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
        }

        try {
            $logger = Atexo_LoggerManager::getLogger();
            $consultationId = $this->getRightIdConsultation($consultationId);
            $retour = $this->validateTicket($logger);
            if (true === $retour) {
                $consultation = CommonConsultationQuery::create()->findOneById($consultationId);
                if (!($consultation instanceof CommonConsultation)
                    ||
                !($this->verifieAccesToInfo($_GET['ticket'], $consultation->getOrganisme(), $consultation->getServiceId()))) {
                    $consultationExist = ($consultation instanceof CommonConsultation);
                    $log = $consultationExist ?
                        ('Vérification de droit acces KO organisme '.$consultation->getOrganisme().' service '.$consultation->getServiceId()) :
                        ("consultation n'existe pas  ".$consultationId);
                    $logger->info($log);
                    $serviceId = $consultationExist ? $consultation->getServiceId() : null;
                    $organisme = $consultationExist ? $consultation->getOrganisme() : null;

                    return self::generateXmlError($this->getMessageErrorForAccessDenied($organisme, $serviceId));
                }
                $xmlResponse = self::generateXmlIdsDce($consultationId);
                $xml = self::encapsulerNodeResponseWsMpe($xmlResponse, 'listeIdRegistre');
                $logger->info("Creer l'xml de la liste des ids des RetraitDce (serialise)");
                $this->getHeaders('200');

                try {
                    self::oversight($params);
                } catch (Exception $exceptionOversight) {
                    $logger = Atexo_LoggerManager::getLogger();
                    $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
                }

                return $xml;
            } else {
                try {
                    $params['code'] = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
                    self::oversight($params);
                } catch (Exception $exceptionOversight) {
                    $logger = Atexo_LoggerManager::getLogger();
                    $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
                }

                return $retour;
            }
        } catch (Exception $e) {
            try {
                $params['code'] = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
                self::oversight($params);
            } catch (Exception $exceptionOversight) {
                $logger = Atexo_LoggerManager::getLogger();
                $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
            }

            $this->getHeaders('500');
            $logger->error('Erreur technique = '.$e->getMessage().' '.$e->getTraceAsString());

            return self::generateXmlError('erreur technique');
        }
    }

    /*
     * genere un xml contenant les ids des retraits associes a une consultation
     *
     * @param integer $refCons reference de la consultation
     * @return string xml contenant les Ids des retraits d'une consultation
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @version 1.0
     * @since 4.6.0
     * @copyright Atexo 2014
     */
    public function generateXmlIdsDce($consultationId)
    {
        $id = $this->getRightIdConsultation($consultationId);
        $arrayIds = (new Atexo_RetraitDce_TelechargementsDce())->getAllRetraitsByConsultation($id, false, true);
        $xml = '<listeIdRegistre referenceConsultation="'.$consultationId.'">';
        if (is_array($arrayIds) && count($arrayIds)) {
            foreach ($arrayIds as $onId) {
                $xml .= '<id>'.$onId.'</id>';
            }
        }
        $xml .= '</listeIdRegistre>';

        return $xml;
    }
}
