<?php

namespace Application\Service\Atexo\Rest\Action;

use App\Entity\Consultation;
use App\Entity\Consultation\ClausesN1;
use App\Repository\ConsultationRepository;
use Application\Propel\Mpe\CommonAgent;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonDCE;
use Application\Propel\Mpe\CommonGeolocalisationN2;
use Application\Propel\Mpe\CommonRG;
use Application\Propel\Mpe\CommonTDonneeComplementaire;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Certificat;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CPV;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Category;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Contrat;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Dce;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Decision;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureType;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Rg;
use Application\Service\Atexo\FormulaireSub\Atexo_FormulaireSub_Parametrage;
use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2;
use Application\Service\Atexo\Publicite\Atexo_Publicite_Avis;
use Application\Service\Atexo\Rest\ObjectField\Atexo_Rest_ObjectField_ConsultationDecisionField;
use Application\Service\Atexo\Rest\ObjectField\Atexo_Rest_ObjectField_DceField;
use Application\Service\Atexo\Rest\ObjectField\Atexo_Rest_ObjectField_PubliciteField;
use Application\Service\Atexo\Rest\ObjectField\Atexo_Rest_ObjectField_RcField;
use Doctrine\Common\Collections\Collection;
use stdClass;

/**
 * Contient des attributs et des redeclaration des methodes pour CommonConsultation.
 *
 * @author LEZ <loubna.ezziani@atexo.com>
 *
 * @version 1.0
 *
 * @since 4.6.0
 *
 * @copyright Atexo 2014
 */
class Atexo_Rest_Action_ConsultationField extends CommonConsultation
{
    protected $_collCertificats = [];
    protected $_nomFichierDce = null;
    protected $_dceContent = null;
    protected $_rcContent;
    protected $_nomFichierRc = null;
    protected $_nomFichierPublicite;
    protected $_fichierPubliciteContent;
    protected $_nomFichiers = [];
    protected $_collLieuExecutions = [];
    protected $_collAnnonceUrlExternes = [];

    protected $_consStatut;
    protected $_annonceStatut;
    protected $_codeRestreint;
    protected $_type;
    protected $dateFinConsultation;
    protected $dateMiseEnLigneCalculeConsultation;
    protected $_acronymeOrg;
    protected $_synchroSub;
    protected $typeContrat = null;
    protected $heritageModalitesReponseProcedure;
    protected $fichierDCE = null;
    protected $decision = null;
    protected $fichierRC = null;
    protected $_collFichierPublicites = [];

    const PREFIX_TYPE_CONTRAT = 'TYPE_CONTRAT_';

    protected static array $mpeLieuExecution = [
        '01' => '01',
        '02' => '02',
        '03' => '03',
        '04' => '04',
        '05' => '05',
        '06' => '06',
        '07' => '07',
        '08' => '08',
        '09' => '09',
        '10' => '10',
        '11' => '11',
        '12' => '12',
        '13' => '13',
        '14' => '14',
        '15' => '15',
        '16' => '16',
        '17' => '17',
        '18' => '18',
        '19' => '19',
        '21' => '21',
        '22' => '22',
        '23' => '23',
        '24' => '24',
        '25' => '25',
        '26' => '26',
        '27' => '27',
        '28' => '28',
        '29' => '29',
        '2A' => '2A',
        '2B' => '2B',
        '30' => '30',
        '31' => '31',
        '32' => '32',
        '33' => '33',
        '34' => '34',
        '35' => '35',
        '36' => '36',
        '37' => '37',
        '38' => '38',
        '39' => '39',
        '40' => '40',
        '41' => '41',
        '42' => '42',
        '43' => '43',
        '44' => '44',
        '45' => '45',
        '46' => '46',
        '47' => '47',
        '48' => '48',
        '49' => '49',
        '50' => '50',
        '51' => '51',
        '52' => '52',
        '53' => '53',
        '54' => '54',
        '55' => '55',
        '56' => '56',
        '57' => '57',
        '58' => '58',
        '59' => '59',
        '60' => '60',
        '61' => '61',
        '62' => '62',
        '63' => '63',
        '64' => '64',
        '65' => '65',
        '66' => '66',
        '67' => '67',
        '68' => '68',
        '69' => '69',
        '70' => '70',
        '71' => '71',
        '72' => '72',
        '73' => '73',
        '74' => '74',
        '75' => '75',
        '76' => '76',
        '77' => '77',
        '78' => '78',
        '79' => '79',
        '80' => '80',
        '81' => '81',
        '82' => '82',
        '83' => '83',
        '84' => '84',
        '85' => '85',
        '86' => '86',
        '87' => '87',
        '88' => '88',
        '89' => '89',
        '90' => '90',
        '91' => '91',
        '92' => '92',
        '93' => '93',
        '94' => '94',
        '95' => '95',
        '971' => 'GP',
        '972' => 'MQ',
        '973' => 'GF',
        '974' => 'RE',
        '975' => 'PM',
        '976' => 'YT',
        '977' => 'BL',
        '978' => 'MF',
        '986' => 'WF',
        '987' => 'PF',
        '988' => 'NC',
    ];

    /**
     * Permet d'initialiser les valeurs de l'objet Atexo_Rest_Action_ConsultationField.
     *
     * @param CommonConsultation $consultation optionnel la consultation d'ou on va copier les valeurs
     *
     * @return Atexo_Rest_Action_ConsultationField une instance de ce objet
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function __construct($consultation = null)
    {
        parent::__construct();
        if ($consultation instanceof CommonConsultation) {
            $consultation->copyInto($this);
            $this->id = $consultation->getId();
            $this->reference =
                $consultation->getReference() ?: $consultation->getId();
            $organisme = $this->getOrganisme();

            $certificats = (new Atexo_Certificat())->retrieveCertificatChiffrement($consultation->getId(), null, $organisme);
            $typeCertificat = null;
            if ('true' == $this->getModeOuvertureReponse()) {
                $certificats = [$certificats[0]];
                $typeCertificat = Atexo_Config::getParameter('TYPE_CERTIFICAT_REPONSE');
            }
            $certificats = (new Atexo_Certificat())->removeDoublonsParTypeCertif($certificats);
            foreach ($certificats as $certificat) {
                $certificatField = new Atexo_Rest_Action_CertificatChiffrementField($certificat, $typeCertificat);
                $this->addCertificats($certificatField);
            }

            $dce = (new Atexo_Consultation_Dce())->getDce($consultation->getId(), $organisme);
            if ($dce instanceof CommonDCE) {
                $fichierDCE = new Atexo_Rest_ObjectField_DceField();
                $fichierDCE->setId($dce->getId());
                $fichierDCE->setNom($dce->getNomDce());
                $this->setFichierDCE($fichierDCE);
            }
            $rg = (new Atexo_Consultation_Rg())->getReglement($consultation->getId(), $organisme);
            if ($rg instanceof CommonRG) {
                $fichierRC = new Atexo_Rest_ObjectField_RcField();
                $fichierRC->setId($rg->getId());
                $fichierRC->setNom($rg->getNomFichier());
                $this->setFichierRC($fichierRC);
            }

            if ($consultation->getLieuExecution()) {
                $lieuxExecutions = explode(',', $consultation->getLieuExecution());
                $this->setNewLieuExecutions($lieuxExecutions);
            }

            $this->setDatefin($consultation->getDatefin());
            $abbreviation = Atexo_Consultation_ProcedureType::retrieveProcedureById($this->getIdTypeProcedureOrg(), $organisme);
            if ($abbreviation) {
                $this->setAbbreviationTypeProcedure($abbreviation->getAbreviationInterface());
            }
            $this->setObejtCpv(Atexo_Config::getParameter('MAX_CPV_ANNEXE'));

            $decision = $this->getConsultationDecision($consultation->getId(), $consultation->getOrganisme(), 0);
            if (null !== $decision->getTypeDecision())  {
                $this->setDecision($this->getConsultationDecision($consultation->getId(), $consultation->getOrganisme(), 0));
            }

            $lots = $this->getAllLots(null,  $consultation->getOrganisme(), $consultation->getId());
            foreach ($lots as $lot) {
                $lot->setDecision($this->getConsultationDecision($consultation->getId(), $consultation->getOrganisme(), $lot->getLot()));

                if (!empty($lot->getClauseSocialeConditionExecution())) {
                    $lot->setClauseSocialeConditionExecution(1);
                }

                if (!empty($lot->getClauseSocialeInsertion())) {
                    $lot->setClauseSocialeInsertion(1);
                }
                $lotField = new Atexo_Rest_Action_LotField($lot);
                $this->addCommonCategorieLot($lotField);
            }
            $listeAvisUrl = (new Atexo_Publicite_Avis())->retreiveAvisFormatLibreUrlByUrl($consultation->getId(), $this->getOrganisme());
            if (is_array($listeAvisUrl) && count($listeAvisUrl)) {
                foreach ($listeAvisUrl as $avis) {
                    $this->addAnnonceUrlExternes($avis->getUrl());
                }
            }

            $envCand = Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE');
            $paramFormulaireCandidature = (new Atexo_FormulaireSub_Parametrage())->retrieveParametrageEnveloppe($consultation->getId(), $this->getOrganisme(), $envCand);
            $envOffre = Atexo_Config::getParameter('TYPE_ENV_OFFRE');
            $paramFormulaireOffre = (new Atexo_FormulaireSub_Parametrage())->retrieveParametrageEnveloppe($consultation->getId(), $this->getOrganisme(), $envOffre);
            if ($paramFormulaireCandidature || $paramFormulaireOffre) {
                $synchroSub = new Atexo_Rest_Action_SynchroSubField();
                $synchroSub->setIdCleExterneDispositifCandidature($paramFormulaireCandidature->getCleExterneDispositif());
                $synchroSub->setIdCleExterneDispositifOffre($paramFormulaireOffre->getCleExterneDispositif());
                $this->setSynchroSub($synchroSub);
            }

            if (!$consultation->getAlloti()) {
                $donneeComplementaire = $consultation->getDonneComplementaire();
                if ($donneeComplementaire instanceof CommonTDonneeComplementaire) {
                    $this->justificationNonAllotissement = $donneeComplementaire->getJustificationNonAlloti();
                }
            } else {
                $this->justificationNonAllotissement = '';
            }
            $listeAvis = (new Atexo_Publicite_Avis())->retreiveAvisPublieByConsultation($consultation->getId(), $this->getOrganisme(), Atexo_Config::getParameter('TYPE_FORMAT_LIBRE_FICHIER_JOINT'));
            if (is_array($listeAvis)) {
                foreach ($listeAvis as $avis) {
                    $fichierPub = new Atexo_Rest_ObjectField_PubliciteField();
                    $fichierPub->setId($avis->getId());
                    $fichierPub->setNom($avis->getNomFichier());
                    $this->addFichiersPublicite($fichierPub);
                }
            }
        }

        if (!empty($this->getClauseSocialeConditionExecution())) {
            $this->setClauseSocialeConditionExecution(1);
        }

        if (!empty($this->getClauseSocialeInsertion())) {
            $this->setClauseSocialeInsertion(1);
        }
    }

    private function getConsultationDecision($refConsultation, $organisme, $lot)
    {
        $decision = (new Atexo_Consultation_Decision())->retrieveDecisionLot($refConsultation, $organisme, $lot);
        $consultationDecision = new Atexo_Rest_ObjectField_ConsultationDecisionField();
        if ($decision) {
            $consultationDecision->setTypeDecision(Atexo_Util::getLibelleDecision($decision->getIdTypeDecision()));
            $consultationDecision->setDateDecision(Atexo_Util::frnDate2iso($decision->getDateDecision()));
        }

        return $consultationDecision;
    }

    public function setReference($v)
    {
        return parent::setId($v);
    }

    /**
     * Set the value of [datemiseenligne] column.
     *
     * @param string $v new value
     *
     * @return CommonConsultation The current object (for fluent API support)
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setDatemiseenligne($v)
    {
        $v = Atexo_Util::getDateTimeValideFromDateXml($v);
        $this->setDateMiseEnLigneCalcule($v);

        return parent::setDatemiseenligne($v);
    }

    // setDatemiseenligne()

    /**
     * Get the value of [datemiseenligne] column.
     *
     * @return CommonConsultation The current object (for fluent API support)
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getDateMiseEnLigneCalculeConsultation()
    {
        $dateMiseEnLigneCalcule = parent::getDateMiseEnLigneCalcule();

        return $dateMiseEnLigneCalcule == null ? null : Atexo_Util::getDateXmlFromDateTimeVal($dateMiseEnLigneCalcule);
    }

    // getDatemiseenligne()

    /**
     * Set the value of [datefin] column.
     *
     * @param string $v new value
     *
     * @return CommonConsultation The current object (for fluent API support)
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setDatefin($v)
    {
        $v = Atexo_Util::getDateTimeValideFromDateXml($v);

        return parent::setDatefin($v);
    }

    // setDatefin()

    /**
     * Get the value of [datefin] column.
     *
     * @return CommonConsultation The current object (for fluent API support)
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getDatefinConsultation()
    {
        return Atexo_Util::getDateXmlFromDateTimeVal(parent::getDatefin());
    }

    // setDatefin()

    /**
     * Set the value of [chiffrement_offre] column.
     *
     * @param string $v new value
     *
     * @return CommonConsultation The current object (for fluent API support)
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setChiffrementOffre($v)
    {
        $v = Atexo_Util::getIntFromValboolean(strtolower($v));

        return parent::setChiffrementOffre($v);
    }

    // setChiffrementOffre()

    /**
     * Get the value of [chiffrement_offre] column.
     *
     * @return string la valeur de chiffrement de l'offre
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getChiffrementOffre()
    {
        $v = parent::getChiffrementOffre();
        $v = Atexo_Util::getBooleanStringFromValInt($v);

        return $v;
    }

    // getChiffrementOffre()

    /**
     * Set the value of [signature_offre] column.
     *
     * @param string $v new value
     *
     * @return CommonConsultation The current object (for fluent API support)
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setSignatureOffre($v)
    {
        $v = Atexo_Util::getIntFromValboolean(strtolower($v));

        return parent::setSignatureOffre($v);
    }

    // setSignatureOffre()

    /**
     * Get the value of [signature_offre] column.
     *
     * @return string la valeur de la signature de l'offre
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getSignatureOffre()
    {
        $v = parent::getSignatureOffre();

        return $v;
    }

    // getSignatureOffre()

    /**
     * Set the value of [env_candidature] column.
     *
     * @param int $v new value
     *
     * @return CommonConsultation The current object (for fluent API support)
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setEnvCandidature($v)
    {
        $v = Atexo_Util::getIntFromValboolean(strtolower($v));

        return parent::setEnvCandidature($v);
    }

    // setEnvCandidature()

    /**
     * Get the value of [env_candidature] column.
     *
     * @return string la valeur d'enveloppe candidature
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getEnvCandidature()
    {
        $v = parent::getEnvCandidature();
        $v = Atexo_Util::getBooleanStringFromValInt($v);

        return $v;
    }

    // getEnvCandidature()

    /**
     * Set the value of [env_offre] column.
     *
     * @param int $v new value
     *
     * @return CommonConsultation The current object (for fluent API support)
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setEnvOffre($v)
    {
        $v = Atexo_Util::getIntFromValboolean(strtolower($v));

        return parent::setEnvOffre($v);
    }

    // setEnvOffre()

    /**
     * Get the value of [env_candidature] column.
     *
     * @return string la valeur d'enveloppe offre
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getEnvOffre()
    {
        $v = parent::getEnvOffre();
        $v = Atexo_Util::getBooleanStringFromValInt($v);

        return $v;
    }

    // getEnvOffre()

    /**
     * Set the value of [env_anonymat] column.
     *
     * @param int $v new value
     *
     * @return CommonConsultation The current object (for fluent API support)
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setEnvAnonymat($v)
    {
        $v = Atexo_Util::getIntFromValboolean(strtolower($v));

        return parent::setEnvAnonymat($v);
    }

    // setEnvAnonymat()

    /**
     * Get the value of [env_candidature] column.
     *
     * @return string la valeur d'enveloppe anonymat
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getEnvAnonymat()
    {
        $v = parent::getEnvAnonymat();
        $v = Atexo_Util::getBooleanStringFromValInt($v);

        return $v;
    }

    // getEnvAnonymat()

    /**
     * Set the value of [env_offre_technique] column.
     *
     * @param int $v new value
     *
     * @return CommonConsultation The current object (for fluent API support)
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setEnvOffreTechnique($v)
    {
        $v = Atexo_Util::getIntFromValboolean(strtolower($v));

        return parent::setEnvOffreTechnique($v);
    }

    // setEnvOffreTechnique()

    /**
     * Get the value of [env_candidature] column.
     *
     * @return string la valeur d'enveloppe offre technique
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getEnvOffreTechnique()
    {
        $v = parent::getEnvOffreTechnique();
        $v = Atexo_Util::getBooleanStringFromValInt($v);

        return $v;
    }

    // getEnvAnonymat()

    /**
     * Set the value of [signature_acte_engagement] column.
     *
     * @param string $v new value
     *
     * @return CommonConsultation The current object (for fluent API support)
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setSignatureActeEngagement($v)
    {
        $v = Atexo_Util::getIntFromValboolean(strtolower($v));

        return parent::setSignatureActeEngagement($v);
    }

    // setSignatureActeEngagement()

    /**
     * Get the value of [signature_acte_engagement] column.
     *
     * @return string la valeur de la signature d'acte d'engagement
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getSignatureActeEngagement()
    {
        $v = parent::getSignatureActeEngagement();
        $v = Atexo_Util::getBooleanStringFromValInt($v);

        return $v;
    }

    // getSignatureActeEngagement()

    /**
     * Set the value of [mode_ouverture_reponse] column.
     *
     * @param string $v new value
     *
     * @return CommonConsultation The current object (for fluent API support)
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setModeOuvertureReponse($v)
    {
        $v = Atexo_Util::getIntFromValboolean(strtolower($v));

        return parent::setModeOuvertureReponse($v);
    }

    // setModeOuvertureReponse()

    /**
     * Get the value of [mode_ouverture_reponse] column.
     *
     * @return string la valeur du mode d'ouverture reponse
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getModeOuvertureReponse()
    {
        $v = parent::getModeOuvertureReponse();
        $v = Atexo_Util::getBooleanStringFromValInt($v);

        return $v;
    }

    // getModeOuvertureReponse()

    /**
     * Set the value of [clause_sociale_condition_execution] column.
     *
     * @param string $v new value
     *
     * @return CommonConsultation The current object (for fluent API support)
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setClauseSocialeConditionExecution($v)
    {
        $v = Atexo_Util::getIntFromValboolean(strtolower($v));

        return parent::setClauseSocialeConditionExecution($v);
    }

    // setClauseSocialeConditionExecution()

    /**
     * Set the value of [clause_sociale_insertion] column.
     *
     * @param string $v new value
     *
     * @return CommonConsultation The current object (for fluent API support)
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setClauseSocialeInsertion($v)
    {
        $v = Atexo_Util::getIntFromValboolean(strtolower($v));

        return parent::setClauseSocialeInsertion($v);
    }

    // setClauseSocialeInsertion()

    /**
     * Set the value of [clause_sociale_ateliers_proteges] column.
     *
     * @param string $v new value
     *
     * @return CommonConsultation The current object (for fluent API support)
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setClauseSocialeAteliersProteges($v)
    {
        $v = Atexo_Util::getIntFromValboolean(strtolower($v));

        return parent::setClauseSocialeAteliersProteges($v);
    }

    // setClauseSocialeAteliersProteges()

    /**
     * Set the value of [clause_sociale_ateliers_proteges] column.
     *
     * @param string $v new value
     *
     * @return CommonConsultation The current object (for fluent API support)
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setClauseSocialeSiae($v)
    {
        $v = Atexo_Util::getIntFromValboolean(strtolower($v));

        return parent::setClauseSocialeSiae($v);
    }

    // setClauseSocialeSiae()

    /**
     * Set the value of [clause_sociale_ateliers_proteges] column.
     *
     * @param string $v new value
     *
     * @return CommonConsultation The current object (for fluent API support)
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setClauseSocialeEess($v)
    {
        $v = Atexo_Util::getIntFromValboolean(strtolower($v));

        return parent::setClauseSocialeEss($v);
    }

    // setClauseSocialeEss()

    /**
     * Set the value of [clause_env_specs_techniques] column.
     *
     * @param string $v new value
     *
     * @return CommonConsultation The current object (for fluent API support)
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setClauseEnvSpecsTechniques($v)
    {
        $v = Atexo_Util::getIntFromValboolean(strtolower($v));

        return parent::setClauseEnvSpecsTechniques($v);
    }

    // setClauseEnvSpecsTechniques()

    /**
     * Set the value of [clause_env_cond_execution] column.
     *
     * @param string $v new value
     *
     * @return CommonConsultation The current object (for fluent API support)
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setClauseEnvCondExecution($v)
    {
        $v = Atexo_Util::getIntFromValboolean(strtolower($v));

        return parent::setClauseEnvCondExecution($v);
    }

    // setClauseEnvCondExecution()

    /**
     * Set the value of [clause_env_criteres_select] column.
     *
     * @param string $v new value
     *
     * @return CommonConsultation The current object (for fluent API support)
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setClauseEnvCriteresSelect($v)
    {
        $v = Atexo_Util::getIntFromValboolean(strtolower($v));

        return parent::setClauseEnvCriteresSelect($v);
    }

    // setClauseEnvCriteresSelect()

    /**
     * Set the value of [Categorie] column.
     *
     * @param string $v new value
     *
     * @return CommonConsultation The current object (for fluent API support)
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setLibelleCategorieConsultation($v)
    {
        $libelleCategorieCons = ucfirst(strtolower($v));
        $categorieCons = (new Atexo_Consultation_Category())->retrieveIdCategoryByLibelle($libelleCategorieCons);

        return $this->setCategorie($categorieCons);
    }

    /**
     * Set the value of [reponse_electronique] column.
     *
     * @param string $v new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setReponseElectronique($v)
    {
        $reponseElectronique = strtolower($v);
        $autoriseReponse = (new Atexo_Consultation())->getValAutoriseReponse($reponseElectronique);
        $this->setAutoriserReponseElectronique($autoriseReponse);
        $reponseObligatoire = (new Atexo_Consultation())->getValReponseObligatoire($reponseElectronique);
        $this->setReponseObligatoire($reponseObligatoire);
    }

    // setReponseElectronique()

    /**
     * Set the value of [cons_statut] column.
     *
     * @param string $v new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setConsStatut($v)
    {
        $idEtatConsultation = (new Atexo_Consultation())->getIdEtatConsByStatut(strtoupper($v));
        $this->_consStatut = $idEtatConsultation;
    }

    // setConsStatut()

    /**
     * Set the value of [code_cp1,code_cpv2,cpv] column.
     *
     * @param string $cpv new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setCpv($cpv)
    {
        if ($cpv instanceof Atexo_CPV) {
            $this->setCodeCpv1($cpv->getCodePrincipal());
            $codeCpv2 = '';
            if ($cpv->getCodeSecondaire1()) {
                $codeCpv2 .= $cpv->getCodeSecondaire1().'#';
            }
            if ($cpv->getCodeSecondaire2()) {
                $codeCpv2 .= $cpv->getCodeSecondaire2().'#';
            }
            if ($cpv->getCodeSecondaire3()) {
                $codeCpv2 .= $cpv->getCodeSecondaire3().'#';
            }
            $this->setCodeCpv2($codeCpv2);

            parent::setCpv($cpv);
        }
    }

    /**
     * Set the value of [_collCommonCategorieLot] column.
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function addCommonCategorieLot(Atexo_Rest_Action_LotField|\Application\Propel\Mpe\CommonCategorieLot $lot)
    {
        $this->_collCommonCategorieLot[] = $lot;
    }

    /**
     * Set the value of [id_createur] column.
     *
     * @param int $v new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setIdCreateur($v)
    {
        if (Atexo_Util::isEntier($v)) {
            $agent = (new Atexo_Agent())->retrieveAgent($v);
            if ($agent instanceof CommonAgent) {
                parent::setIdCreateur($v);
                $this->setNomCreateur($agent->getNom());
                $this->setPrenomCreateur($agent->getPrenom());
            }
        }
    }

    // setIdCreateur()

    /**
     * Set the value of [_collCommonCategorieLot] column.
     *
     * @param $certificat instanceof Atexo_Rest_Action_CertificatChiffrementField
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function addCertificats(Atexo_Rest_Action_CertificatChiffrementField $certificat)
    {
        $this->_collCertificats[] = $certificat;
    }

    /**
     * Get the value of [_collCertificats] column.
     *
     * @return array collCertificats instanceof Atexo_Rest_Action_CertificatChiffrementField
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getCollCertificats()
    {
        return $this->_collCertificats;
    }

    /**
     * Set the value of [IdTypeAvis] column.
     *
     * @param string $v new value
     *
     * @return CommonConsultation The current object (for fluent API support)
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setType($v)
    {
        $typeAvis = strtoupper($v);
        $typeAvis = (new Atexo_Consultation())->getValTypeAvis($typeAvis);

        return $this->setIdTypeAvis($typeAvis);
    }

    /**
     * Get the value of [_typeAvis] column.
     *
     * @return string typeAvis
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getType()
    {
        $typeAvis = (new Atexo_Consultation())->getValTypeAvisById($this->getIdTypeAvis());

        return $typeAvis;
    }

    /**
     * Set the value of [cons_statut] column.
     *
     * @param string $v new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setStatut($v)
    {
        $this->_annonceStatut = (new Atexo_Consultation())->getIdEtatConsByStatut(strtoupper($v));
    }

    // setStatut()

    /**
     * Set the value of [datemiseenligne] column.
     *
     * @param string $v new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setDateFinAffichage($v)
    {
        $v = Atexo_Util::getDateTimeValideFromDateXml($v);
        parent::setDateFin($v);
        parent::setDateFinAffichage($v);
    }

    // setDatemiseenligne()

    /**
     * Get the value of [datemiseenligne] column.
     *
     * @return date la date en format pour l'xml
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getDateFinAffichage()
    {
        $date = Atexo_Util::getDateXmlFromDateTimeVal(parent::getDateFinAffichage());

        return $date;
    }

    // getDatemiseenligne()

    /**
     * Set the value of [_dceContent] column.
     *
     * @param string $content new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setDceContent($content)
    {
        $this->_dceContent = $content;
    }

    /**
     * Get the value of [_dceContent] column.
     *
     * @return string le contenu du fichier DCE
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getDceContent()
    {
        return $this->_dceContent;
    }

    /**
     * Set the value of [_nomFichierDce] column.
     *
     * @param string $v new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setNomFichierDCE($v)
    {
        $this->_nomFichierDce = $v;
    }

    /**
     * Get the value of [_nomFichierDce] column.
     *
     * @return string nom du fichier Dce
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getNomFichierDCE()
    {
        return $this->_nomFichierDce;
    }

    /**
     * Set the value of [_rcContent] column.
     *
     * @param string $v new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setRcContent($content)
    {
        $this->_rcContent = $content;
    }

    /**
     * Get the value of [_rcContent] column.
     *
     * @return string le contenu du fichier RC
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getRcContent()
    {
        return $this->_rcContent;
    }

    /**
     * Set the value of [_nomFichierRc] column.
     *
     * @param string $v new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setNomFichierRc($v)
    {
        $this->_nomFichierRc = $v;
    }

    /**
     * Get the value of [_nomFichierRc] column.
     *
     * @return string le nom du fichier RC
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getNomFichierRc()
    {
        return $this->_nomFichierRc;
    }

    /**
     * @return mixed
     */
    public function getNomFichierPublicite()
    {
        return $this->_nomFichierPublicite;
    }

    /**
     * @param mixed $nomFichierPublicite
     */
    public function setNomFichierPublicite($nomFichierPublicite)
    {
        $this->_nomFichierPublicite = $nomFichierPublicite;
    }

    /**
     * Set the value of [_fichierPubliciteContent] column.
     *
     * @param string $v new value
     *
     * @return void
     *
     * @author SDR <salwa.drissi@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2015
     */
    public function setFichierPubliciteContent($content)
    {
        $this->_fichierPubliciteContent = $content;
    }

    /**
     * Get the value of [_fichierPubliciteContent] column.
     *
     * @return string le contenu du fichier FichierPublicite
     *
     * @author SDR <salwa.drissi@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2015
     */
    public function getFichierPubliciteContent()
    {
        return $this->_fichierPubliciteContent;
    }

    /**
     * Set the value of [_nomFichiers] column.
     *
     * @param string $v new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function addNomFichiers($v)
    {
        $this->_nomFichiers[] = $v;
    }

    /**
     * Set the value of [_nomFichiers] column.
     *
     * @param string $v new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setNewNomFichiers($v)
    {
        $this->_nomFichiers = $v;
    }

    /**
     * Get the value of [_nomFichierAnnonce] column.
     *
     * @return string le nom du fichier de l'annonce
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getNomFichiers()
    {
        return $this->_nomFichiers;
    }

    /**
     * Get the value of [ServiceId] column.
     *
     * @return CommonConsultation The current object (for fluent API support)
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getIdDirectionService()
    {
        return parent::getServiceId();
    }

    /*
     * retourne l'abreviation de type de procedure
     *
     * @return string abreviation de type de procedure
     * @author LEZ <loubna.ezziani@atexo.com>
     * @version 1.0
     * @since 4.6.0
     * @copyright Atexo 2014
     */
    public function getTypeProcedure()
    {
        $idTypeProcedurePortail = $this->getIdTypeProcedureOrg();
        $organisme = $this->getOrganisme();
        $typeProcedure = (new Atexo_Consultation_ProcedureType())->retrieveTypeProcedureByIdTypeProcedurePortail($idTypeProcedurePortail, $organisme);
        if ($typeProcedure) {
            return $typeProcedure->getAbreviationInterface();
        } else {
            return '';
        }
    }

    /**
     * Retourn le satatut de la consultation.
     *
     * @return string statut de la consultation
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getConsStatut()
    {
        $idStatut = Atexo_Consultation::getStatus($this, false);
        $statut = (new Atexo_Consultation())->getStatutByIdEtatCons($idStatut);

        return $statut;
    }

    /**
     * Get the value of [_consStatut] column.
     *
     * @return string statut de la consultation
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getConsultationStatut()
    {
        return $this->_consStatut;
    }

    /**
     * Get the value of [_annonceStatut] column.
     *
     * @return string statut de l'annonce
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getFullAnnonceStatut()
    {
        return $this->_annonceStatut;
    }

    /**
     * Get the value of [_annonceStatut] column.
     *
     * @return string statut de l'annonce aprés un formatage selon les dates
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getAnnonceStatut()
    {
        $idStatut = Atexo_Consultation::getStatus($this, false);
        $statut = (new Atexo_Consultation())->getStatutByIdEtatAnnonce($idStatut);

        return $statut;
    }

    /**
     * Get the value of [reponse_electronique] column.
     *
     * @return string le type de reponse
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getReponseElectronique()
    {
        $autoriseReponse = $this->getAutoriserReponseElectronique();
        $reponseObligatoire = $this->getReponseObligatoire();
        $typeReponse = (new Atexo_Consultation())->getTypeReponse($autoriseReponse, $reponseObligatoire);

        return $typeReponse;
    }

    // getReponseElectronique()

    /**
     * Get the value of [CodeProcedure] column.
     *
     * @return string le code procedure
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getCodeRestreint()
    {
        return $this->getCodeProcedure();
    }

    /**
     * Get the value of [_collCommonCategorieLot] column.
     *
     * @return array CollCommonCategorieLot instanceof LotFiled
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getCollCommonCategorieLot()
    {
        $lots = $this->getCommonCategorieLots();
        if ($lots) {
            return $lots;
        } else {
            return [];
        }
    }

    /**
     * Retourne les clauses sociales au format stdClass
     *
     * @return stdClass|null
     */
    public function getClauseSociale(): ?stdClass
    {
        if (!Atexo_Module::isEnabled('ConsultationClause')) {
            return null;
        }

        $std = $this->getInitializedClauseSocialeStdClass();


        /** @var ConsultationRepository $consultationRepository */
        $consultationRepository = Atexo_Util::getSfService(ConsultationRepository::class);
        /** @var Consultation $consultation */
        $consultation = $consultationRepository->find($this->getId());

        return self::populateClauseSocialeData($std, $consultation->getClausesN1());
    }

    public function getInitializedClauseSocialeStdClass(): stdClass
    {
        $std = new stdClass();

        $clauseCondition = $this->getClauseSocialeConditionExecution();
        $clauseCondition = Atexo_Util::getBooleanStringFromValInt($clauseCondition);
        $std->conditionExecution = $clauseCondition;
        $clauseInsertion = $this->getClauseSocialeInsertion();
        $clauseInsertion = Atexo_Util::getBooleanStringFromValInt($clauseInsertion);
        $std->insertion = $clauseInsertion;
        $clauseMarcheReserv = $this->getMarcheReserve() ? 'true' : 'false';
        $std->marcheReserve = $clauseMarcheReserv;
        $clauseAtelierProteges = $this->getClauseSocialeAteliersProteges();
        $clauseAtelierProteges = Atexo_Util::getBooleanStringFromValInt($clauseAtelierProteges);
        $std->ESAT_EA = $clauseAtelierProteges;
        $clauseSiae = $this->getClauseSocialeSiae();
        $clauseSiae = Atexo_Util::getBooleanStringFromValInt($clauseSiae);
        $std->SIAE = $clauseSiae;
        $clauseEss = $this->getClauseSocialeEss();
        $clauseEss = Atexo_Util::getBooleanStringFromValInt($clauseEss);
        $std->EESS = $clauseEss;

        return $std;
    }

    public static function populateClauseSocialeData(stdClass $std, Collection $clausesN1): stdClass
    {
        $clauseSociale = null;

        foreach ($clausesN1 as $clauseN1) {
            if ($clauseN1->getReferentielClauseN1()->getSlug() == 'clausesSociales') {
                $clauseSociale = $clauseN1;
                break;
            }
        }

        if (! $clauseSociale instanceof ClausesN1) {
            return $std;
        }

        $clausesN2 = $clauseSociale->getClausesN2();

        foreach ($clausesN2 as $clauseN2) {
            switch ($clauseN2->getReferentielClauseN2()->getSlug()) {
                case 'conditionExecution':
                    $std->conditionExecution = 'true';
                    break;
                case 'insertion':
                    $std->insertion = 'true';
                    break;
                case 'marcheReserve':
                    $std->marcheReserve = 'true';
                    $clausesN3 = $clauseN2->getClausesN3();

                    foreach ($clausesN3 as $clauseN3) {
                        switch ($clauseN3->getReferentielClauseN3()->getSlug()) {
                            case 'clauseSocialeReserveAtelierProtege':
                                $std->ESAT_EA = 'true';
                                break;
                            case 'clauseSocialeSIAE':
                                $std->SIAE = 'true';
                                break;
                            case 'clauseSocialeEESS':
                                $std->EESS = 'true';
                                break;
                        }
                    }

                    break;
            }
        }

        return $std;
    }

    /**
     * Retourne les clauses environnementales au format stdClass
     *
     * @return stdClass|null
     */
    public function getClauseEnvironnementale(): ?stdClass
    {
        if (!Atexo_Module::isEnabled('ConsultationClause')) {
            return null;
        }

        $std = $this->getInitializedClauseEnvironnementaleStdClass();

        /** @var ConsultationRepository $consultationRepository */
        $consultationRepository = Atexo_Util::getSfService(ConsultationRepository::class);
        /** @var Consultation $consultation */
        $consultation = $consultationRepository->find($this->getId());

        return self::populateClauseEnvironnementaleData($std, $consultation->getClausesN1());
    }

    public function getInitializedClauseEnvironnementaleStdClass(): stdClass
    {
        $std = new stdClass();

        $specTechn = $this->getClauseEnvSpecsTechniques();
        $specTechn = Atexo_Util::getBooleanStringFromValInt($specTechn);
        $std->specificationsTechniques = $specTechn;
        $clauseCondE =  $this->getClauseEnvCondExecution();
        $clauseCondE = Atexo_Util::getBooleanStringFromValInt($clauseCondE);
        $std->conditionExecution = $clauseCondE;
        $clauseCriterS =  $this->getClauseEnvCriteresSelect();
        $clauseCriterS = Atexo_Util::getBooleanStringFromValInt($clauseCriterS);
        $std->criteresSelections = $clauseCriterS;

        return $std;
    }

    public static function populateClauseEnvironnementaleData(stdClass $std, Collection $clausesN1): stdClass
    {
        $clauseEnvironnementale = null;

        foreach ($clausesN1 as $clauseN1) {
            if ($clauseN1->getReferentielClauseN1()->getSlug() == 'clauseEnvironnementale') {
                $clauseEnvironnementale = $clauseN1;
                break;
            }
        }

        if (! $clauseEnvironnementale instanceof ClausesN1) {
            return $std;
        }

        $clausesN2 = $clauseEnvironnementale->getClausesN2();

        foreach ($clausesN2 as $clauseN2) {
            switch ($clauseN2->getReferentielClauseN2()->getSlug()) {
                case 'specificationsTechniques':
                case 'specificationTechnique':
                    $std->specificationsTechniques = 'true';
                    break;
                case 'conditionExecution':
                case 'conditionsExecutions':
                    $std->conditionExecution = 'true';
                    break;
                case 'criteresSelections':
                case 'critereSelection':
                    $std->criteresSelections = 'true';
                    break;
            }
        }

        return $std;
    }

    /**
     * Set the value of [_collLieuExecutions] column.
     *
     * @param Atexo_Rest_Action_LieuExecutionField $lieuExecution new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function addLieuExecutions($lieuExecution)
    {
        $geoLieuExecution = (new Atexo_Geolocalisation_GeolocalisationN2())->retrieveGeoN2ByCodeInterface($lieuExecution);
        if ($geoLieuExecution instanceof CommonGeolocalisationN2) {
            $lieuExecution = $geoLieuExecution->getId();
            if (!in_array($lieuExecution, $this->_collLieuExecutions)) {
                $this->_collLieuExecutions[] = $lieuExecution;
            }
        }
    }

    // setLieuExecutions()

    /**
     * Get the value of [lieuExecutions] column.
     *
     * @return array $lieuExecutions of code des lieux d'execution
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     * @copyright Atexo 2014
     *
     * @version 1.0
     *
     * @since 4.6.0
     */
    public function getCollLieuExecutions()
    {
        $lieuExecutions = [];
        if (is_array($this->_collLieuExecutions)) {
            foreach ($this->_collLieuExecutions as $idLieu) {
                $geoLieuExecution = (new Atexo_Geolocalisation_GeolocalisationN2())->retrieveGeolocalisationN2ById($idLieu);
                if ($geoLieuExecution[0] instanceof CommonGeolocalisationN2) {
                    $lieuExecutions[] = $geoLieuExecution[0]->getCodeInterface();
                }
            }
        }

        return $lieuExecutions;
    }

    // getLieuExecutions()

    /**
     * Get the value of [_collLieuExecutions] column.
     *
     * @return array of collLieuExecutions
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     * @copyright Atexo 2014
     *
     * @version 1.0
     *
     * @since 4.6.0
     */
    public function getIdsLieuExecutions()
    {
        return $this->_collLieuExecutions;
    }

    // getIdsLieuExecutions()

    /**
     * Set the value of [_collLieuExecutions] column.
     *
     * @param string $v new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setNewLieuExecutions($value)
    {
        $this->_collLieuExecutions = $value;
    }

    // setNewLieuExecutions()

    /**
     * Get the value of [_libelleCategorieConsultation] column.
     *
     * @return string LibelleCategorieConsultation
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getLibelleCategorieConsultation()
    {
        $v = parent::getLibelleCategorieConsultation();

        return strtoupper($v);
    }

    /**
     * Get the value of [_acronymeOrg] column.
     *
     * @return string acronymeOrg
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getAcronymeOrg()
    {
        return parent::getOrganisme();
    }

    // getAcronymeOrg()

    /**
     * Affecte la valeur de la colonne [intitule].
     *
     * @param string $intitule
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setIntitule($intitule)
    {
        parent::setIntitule($intitule);
    }

    /**
     * Affecte la valeur de la colonne [objet].
     *
     * @param string $objet
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setObjet($objet)
    {
        parent::setObjet($objet);
    }

    /**
     * Set the value of [clause_sociale] column.
     *
     * @param string $v new value
     *
     * @author OKO <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setClauseSociale($v)
    {
        if ('0' !== Atexo_Util::getIntFromValboolean($this->getClauseSocialeConditionExecution())
            || '0' !== Atexo_Util::getIntFromValboolean($this->getClauseSocialeInsertion())
            || '0' !== Atexo_Util::getIntFromValboolean($this->getClauseSocialeAteliersProteges())
            || '0' !== Atexo_Util::getIntFromValboolean($this->getClauseSocialeSiae())
            || '0' !== Atexo_Util::getIntFromValboolean($this->getClauseSocialeEss())
        ) {
            $v = '1';
        } else {
            $v = '2';
        }

        return parent::setClauseSociale($v);
    }

    // setClauseSociale

    /**
     * Set the value of [clause_environnementale] column.
     *
     * @param string $v new value
     *
     * @author OKO <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setClauseEnvironnementale($v)
    {
        if ('0' !== Atexo_Util::getIntFromValboolean($this->getClauseEnvSpecsTechniques())
            || '0' !== Atexo_Util::getIntFromValboolean($this->getClauseEnvCondExecution())
            || '0' !== Atexo_Util::getIntFromValboolean($this->getClauseEnvCriteresSelect())) {
            $v = '1';
        } else {
            $v = '2';
        }

        return parent::setClauseEnvironnementale($v);
    }

    // setClauseEnvironnementale

    /**
     * Set the value of [_collAnnonceUrlExternes] column.
     *
     * @param string $_collAnnonceUrlExternes new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function addAnnonceUrlExternes($urlExterne)
    {
        $this->_collAnnonceUrlExternes[] = $urlExterne;
    }

    // addAnnoncesUrlsExternes()

    /**
     * Get the value of [_collAnnoncesUrlsExternes] column.
     *
     * @return array $_collAnnoncesUrlsExternes of url externes
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getCollAnnonceUrlExternes()
    {
        return $this->_collAnnonceUrlExternes;
    }

    // getCollAnnoncesUrlsExternes()

    /**
     * Set the value of [_synchroSub] column.
     *
     * @param Atexo_Rest_Action_SynchroSubField $_synchroSub new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function setSynchroSub(Atexo_Rest_Action_SynchroSubField $synchroSub)
    {
        $this->_synchroSub = $synchroSub;
    }

    // setSynchroSub()

    /**
     * Get the value of [_synchroSub] column.
     *
     * @return array $_synchroSub
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getSynchroSub()
    {
        return $this->_synchroSub;
    }

    // getSynchroSub()

    /**
     * Set the value of [marche_public_simplifie] column.
     *
     * @param int $v new value
     *
     * @return CommonConsultation The current object (for fluent API support)
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since ESR-2015
     *
     * @copyright Atexo 2015
     */
    public function setMarchePublicSimplifie($v)
    {
        $v = Atexo_Util::getIntFromValboolean(strtolower($v));

        return parent::setMarchePublicSimplifie($v);
    }

    // setMarchePublicSimplifie()

    /**
     * Get the value of [marche_public_simplifie] column.
     *
     * @return string la valeur de marche public simplifie
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since ESR-2015
     *
     * @copyright Atexo 2015
     */
    public function getMarchePublicSimplifie()
    {
        $v = parent::getMarchePublicSimplifie();
        $v = Atexo_Util::getBooleanStringFromValInt($v);

        return $v;
    }

    // getMarchePublicSimplifie()

    /**
     * Retourne le libelle de type de marche.
     *
     * @return string le libelle de type de marche
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-palce
     *
     * @copyright Atexo 2016
     */
    public function getTypeMarche()
    {
        return (new Atexo_Consultation_Contrat())->getLibelleTypeContratById(parent::getTypeMarche(), false) ? str_replace(self::PREFIX_TYPE_CONTRAT, '', (new Atexo_Consultation_Contrat())->getLibelleTypeContratById(parent::getTypeMarche(), false)) : false;
    }

    /**
     * @param mixed $typeContrat
     */
    public function setTypeContrat($typeContrat)
    {
        $this->typeContrat = self::PREFIX_TYPE_CONTRAT.$typeContrat;
    }

    /**
     * Retourne le libelle de type de marche.
     *
     * @return string le libelle de type de marche
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-palce
     *
     * @copyright Atexo 2016
     */
    public function getTypeContrat()
    {
        return $this->typeContrat;
    }

    /**
     * Get the value of [datemiseenligne] column.
     *
     * @return CommonConsultation The current object (for fluent API support)
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getDateModification()
    {
        return Atexo_Util::getDateXmlFromDateTimeVal(parent::getDateModification());
    }

    /**
     * @return mixed
     */
    public function getHeritageModalitesReponseProcedure()
    {
        return $this->heritageModalitesReponseProcedure;
    }

    /**
     * @param mixed $heritageModalitesReponseProcedure
     */
    public function setHeritageModalitesReponseProcedure($v)
    {
        $v = Atexo_Util::getIntFromValboolean(strtolower($v));
        $this->heritageModalitesReponseProcedure = $v;
    }

    /**
     * @return mixed
     */
    public function getFichierDCE()
    {
        return $this->fichierDCE;
    }

    /**
     * @param mixed $fichierDCE
     */
    public function setFichierDCE($fichierDCE)
    {
        if ($fichierDCE instanceof Atexo_Rest_ObjectField_DceField) {
            $this->fichierDCE = $fichierDCE;
        }
    }

    /**
     * @return mixed
     */
    public function getFichierRC()
    {
        return $this->fichierRC;
    }

    /**
     * @param mixed $fichierRC
     */
    public function setFichierRC($fichierRC)
    {
        if ($fichierRC instanceof Atexo_Rest_ObjectField_RcField) {
            $this->fichierRC = $fichierRC;
        }
    }

    /**
     * @return array
     */
    public function getCollFichierPublicites()
    {
        return $this->_collFichierPublicites;
    }

    /**
     * @param array $fichiersPublicite
     */
    public function setCollFichierPublicites($fichiersPublicites)
    {
        $this->_collFichierPublicites = $fichiersPublicites;
    }

    // getDatemiseenligne()

    /**
     * @param array $fichiersPublicite
     */
    public function addFichiersPublicite($fichierPublicite)
    {
        if ($fichierPublicite instanceof Atexo_Rest_ObjectField_PubliciteField) {
            $this->_collFichierPublicites[] = $fichierPublicite;
        }
    }

    // getDatemiseenligne()

    /**
     * @param mixed $codeExterne
     */
    public function setCodeExterne($codeExterne)
    {
        parent::setCodeExterne($codeExterne);
    }

    public function setDecision($decision)
    {
        if ($decision instanceof Atexo_Rest_ObjectField_ConsultationDecisionField) {
            $this->decision = $decision;
        }

        return $this;
    }

    public function getDecision()
    {
        return $this->decision;
    }
}
