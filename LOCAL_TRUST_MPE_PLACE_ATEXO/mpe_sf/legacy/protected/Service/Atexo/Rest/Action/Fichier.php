<?php

namespace Application\Service\Atexo\Rest\Action;

use Application\Propel\Mpe\CommonConsultation;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Archive;
use Exception;

/**
 * permet de gérer le contenu des fichiers via ws.
 *
 * @author LEZ <loubna.ezziani@atexo.com>
 *
 * @version 1.0
 *
 * @since 2015-megalis
 *
 * @copyright Atexo 2016
 */
class Atexo_Rest_Action_Fichier extends Atexo_Rest_Action_Actions
{
    /**
     * permet de retourner le contenu d'un fichier.
     *
     * @param $typeDoc BORDEREAU_ARCHIVE
     * @param $id
     * @param $organisme
     * @param null $lot
     *
     * @return string xml contenant le statut de reponse et le fichier
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 2.0
     *
     * @since 2015-megalis
     *
     * @copyright Atexo 2016
     */
    public function get($typeDoc, $id, $organisme, $lot = null)
    {
        $messageErreur = null;
        try {
            $logger = Atexo_LoggerManager::getLogger('crypto');
            $logger->info('Debut de la récupération du contenu du fichier....');
            $retour = $this->validateTicket($logger);
            if (true === $retour) {
                $msgInfoDocument = ' le type => '.$typeDoc.", l'id => ".$id.", l'organisme => ".$organisme;
                if ($lot === $_GET['ticket']) {
                    $lot = 0;
                }
                if ($lot !== $_GET['ticket'] && null !== $lot) {
                    $msgInfoDocument .= ' et lot => '.$lot;
                }
                $logger->info('La récupération du contenu du fichier. ('.$msgInfoDocument.')');
                $xmlResponse = '';
                $consultation = (new Atexo_Consultation())->getConsultationByReference($id, $organisme);
                if ($consultation instanceof CommonConsultation) {
                    if ($this->verifieAccesToInfo($_GET['ticket'], $organisme, $consultation->getServiceId())) {
                        $contentType = 'application/octet-stream';
                        $typeDoc = strtoupper($typeDoc);
                        switch ($typeDoc) {
                            case 'BORDEREAU_ARCHIVE':
                                $messageErreur = (new Atexo_Consultation_Archive())->setBordereauForWs($id, $organisme, $lot, $xmlResponse, $logger, $contentType);
                                break;
                        }
                    } else {
                        $logger->info("Acces à cette ressource n'est pas permis : ".$consultation->getId());
                        $messageErreur = $this->getMessageErrorForAccessDenied($organisme, $consultation->getServiceId());
                    }
                } else {
                    $messageErreur = 'La consultation dont la référence est  '.$id." et l'organisme ".$organisme." n'existe pas";
                }
                if (!$messageErreur && $xmlResponse) {
                    $log = date('Y-m-d H:i:s').' IP '.$_SERVER['REMOTE_ADDR'].' output : '.
                        print_r($xmlResponse, true)." \n ";
                    Atexo_Util::writeFile(Atexo_Config::getParameter('BASE_ROOT_DIR')
                        .'/logs/wsRestFichier.log', $log, 'a');
                    $logger->info('Fin de la récupération du contenu du fichier....');
                    $this->getHeaders('200');

                    return $xmlResponse;
                }
                if ($messageErreur) {
                    $logger->error($messageErreur);
                    $this->getHeaders('400');
                    $logger->info('Fin de la récupération du contenu du fichier....');

                    return static::generateXmlError($messageErreur);
                }
            } else {
                return $retour;
            }
            $logger->info('Fin de la récupération du contenu du fichier....');
        } catch (Exception $e) {
            $logger->error('Erreur technique = '.$e->getMessage().' '.$e->getTraceAsString());
            $this->getHeaders('500');

            return static::generateXmlError('erreur technique');
        }
    }
}
