<?php

namespace Application\Service\Atexo\Rest\Action;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonService;
use Application\Propel\Mpe\CommonServiceQuery;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Oversight\Atexo_Oversight_Oversight;
use Application\Service\Atexo\Serializer\Atexo_Serializer_Serializer;
use DateTime;
use Exception;

/**
 * permet de créer ou modifier une annonce
 * à partir d'un xml contenant les données de l'annonce.
 *
 * @author LEZ <loubna.ezziani@atexo.com>
 *
 * @version 1.0
 *
 * @since 4.6.0
 *
 * @copyright Atexo 2014
 */
class Atexo_Rest_Action_AnnonceService extends Atexo_Rest_Action_Actions
{
    /**
     * permet de créer ou modifier une annonce
     * à partir d'un xml poster contenant les données de l'annonce.
     *
     * @param string xml contenant les infos de l'annonce passer en POST
     *
     * @return string xml contenant les infos de l'annonce
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function post()
    {
        $xmlResult = null;
        $params = [];
        try {
            $params['nomBatch'] = 'WS-Annonce-création';
            $params['service'] = 'Annonce';
            $params['typeFlux'] = 'WebService';
            $params['poids'] = 0;
            $params['nbFlux'] = 1;
            $params['ticket'] = Atexo_Util::atexoHtmlEntities($_GET['ticket']);
            self::oversight($params);
            $params['code'] = Atexo_Oversight_Oversight::$codeSuccess;
        } catch (Exception $exceptionOversight) {
            $logger = Atexo_LoggerManager::getLogger('supervision');
            $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
        }

        try {
            $logger = Atexo_LoggerManager::getLogger('consultation');
            $logger->info('La vérification du ticket ');
            $retour = $this->validateTicket($logger);
            if (true === $retour) {
                $logger->info('Vérification OK.');
                $xml = $_POST['xml'];
                $params['informationMetier'] = serialize($xml);
                $log = date('Y-m-d H:i:s').' IP '.$_SERVER['REMOTE_ADDR'].' input : '.print_r($xml, true)." \n POST ".print_r($_POST, true)." \n FILES ".print_r($_FILES, true)." \n ";
                Atexo_Util::writeFile(Atexo_Config::getParameter('BASE_ROOT_DIR').'/logs/wsRestAnnonce.log', $log, 'a');
                if ($xml) {
                    $xmlValide = Atexo_Util::validateXmlBySchema($xml, Atexo_Config::getParameter('PATH_FILE_XSD_WS'), $logger);
                    if ($xmlValide) {
                        $xmlElements = simplexml_load_string($xml);
                        $reference = (int) $xmlElements->envoi->annonce->id;
                        $organisme = $xmlElements->envoi->annonce->organisme;
                        $serviceId = $xmlElements->envoi->annonce->idDirectionService;
                        if ($serviceId != null) {
                            list($serviceId, $oldServiceId) = $this->getIdService($serviceId, $organisme);
                        }

                        $logger->info('Vérification de droit acces ...');
                        if ($this->verifieAccesToInfo($_GET['ticket'], $organisme, $serviceId)) {
                            $logger->info('Vérification de droit acces OK organisme '.$organisme);
                            $id = $this->getRightIdConsultation($reference, $organisme);
                            $annonceOld = false;
                            $logger->info("La récupération de l'annonce dont l'id = ".$id." et l'organiseme= ".$organisme);
                            if ($id && $organisme) {
                                $annonceOld = (new Atexo_Consultation())->retrieveConsultation($id, $organisme);
                            }
                            if ($annonceOld instanceof CommonConsultation) {
                                $logger->info("L'annonce dont la reference = ".$reference." et l'organiseme= ".$organisme.' existe');
                                $consultationFiled = new Atexo_Rest_Action_ConsultationField();
                                $logger->info("Copier les information de l'annonce dans l'objet consultationField");
                                $annonceOld->copyInto($consultationFiled);
                                $serilizer = new Atexo_Serializer_Serializer($consultationFiled, Atexo_Rest_Action_ConsultationField::class);
                                $params['nomBatch'] = 'WS-Annonce-modification';
                            } else {
                                $serilizer = new Atexo_Serializer_Serializer();
                            }
                            $xmlConsultation = $this->getNodeFromXml($xml, 'annonce');
                            $serilizer = new Atexo_Serializer_Serializer();
                            $xmlProperties = Atexo_Config::getParameter('PATH_FILE_PROPERTIES_ANNONCE');
                            $logger->info("La récupération de l'objet annonce à partir du xml");
                            $consultationFiled = $serilizer->DeserializeClass($xmlConsultation, $xmlProperties);
                            $logger->info('mettre le bon id '.$id);
                            $consultationFiled->setId($id);
                            $files = [];
                            if (isset($_FILES) && is_array($_FILES) && count($_FILES)) {
                                $i = 0;
                                $logger->info("La récupération des fichiers de l'annonce");
                                foreach ($_FILES as $key => $file) {
                                    $logger->info('Debut du traitement du fichier '.($i + 1).' nom fichier =  '.$file['name']);
                                    if ($file['size'] <= Atexo_Config::getParameter('MAX_SIZE_FILE_WS_REST')) {
                                        $infileAvis = Atexo_Config::getParameter('COMMON_TMP').'avis_'.($i + 1).'_'.session_id().time();
                                        if (move_uploaded_file($file['tmp_name'], $infileAvis)) {
                                            $files[$i]['filePath'] = $infileAvis;
                                            $files[$i]['fileName'] = $file['name'];
                                            ++$i;
                                            $logger->info('Fin du traitement du fichier '.$i.' nom fichier =  '.$file['name']);
                                        } else {
                                            $params['code'] = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
                                            $logger->info('move_uploaded_file probleme reception  AVIS _FILE : '.$file['tmp_name'].' Destination : '.$infileAvis);
                                        }
                                    } else {
                                        $this->getHeaders('413');
                                        $logger->error('La taille du fichier '.$file['name']." d'avis depasse la taille maximum");

                                        $params['code'] = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;

                                        return static::generateXmlError('La taille du fichier '.$file['name']." d'avis depasse la taille maximum");
                                    }
                                }
                            }
                            $consultationFiled->setNewNomFichiers($files);
                            $resultat = (new Atexo_Consultation())->createAnnonce($consultationFiled, $oldServiceId);
                            if ($resultat instanceof Atexo_Rest_Action_ConsultationField) {
                                $xmlProperties = Atexo_Config::getParameter('PATH_FILE_MAPPING_ANNONCE');
                                $xmlHiddenField = Atexo_Config::getParameter('PATH_FILE_HIDDEN_FIELD_ANNONCE');
                                $listOptioanlField = Atexo_Config::getParameter('PATH_FILE_OPTIONAL_FIELD_ANNONCE');
                                $logger->info("La récupération de l'xml à partir de l'objet annonce ");
                                $xmlResult = $serilizer->SerializeClass($resultat, Atexo_Rest_Action_ConsultationField::class, $xmlProperties, $xmlHiddenField, $listOptioanlField);
                                $xmlResult = $this->encapsulerNodeResponseWsMpe($xmlResult, 'annonce');
                                $this->getHeaders('200');
                            } else {
                                $logger->error($resultat);
                                $xmlResult = static::generateXmlError($resultat);
                                $this->getHeaders('400');
                            }
                        } else {
                            $this->getHeaders('403');
                            $logger->error('vérification acces KO pr ticket '.
                                $_GET['ticket'].'organisme '.$organisme.' service '.$serviceId);
                            $xmlResult = static::generateXmlError($this->getMessageErrorForAccessDenied($organisme, $serviceId));
                        }
                    } else {
                        $this->getHeaders('400');
                        $xsd = file_get_contents(Atexo_Config::getParameter('PATH_FILE_XSD_WS'));

                        return static::generateXmlError('La structure du fichier xml ne respecte pas le schema approprie'.$xsd);
                    }
                } else {
                    $logger->error("Il n'ya pas du fichier xml à traiter");
                    $this->getHeaders('400');
                }
            } else {
                $params['code'] = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;

                return $retour;
            }
        } catch (Exception $e) {
            $logger->error('Erreur technique = '.$e->getMessage().' '.$e->getTraceAsString());
            $xmlResult = static::generateXmlError('Erreur technique');
            $params['code'] = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
            $this->getHeaders('500');
        }
        try {
            //@todo : Rajouter dans la nomenclature une erreur d'identification !

            $params['oversight'] = self::oversight($params);
        } catch (Exception $exceptionOversight) {
            $logger = Atexo_LoggerManager::getLogger('supervision');
            $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
        }
        $log = date('Y-m-d H:i:s').' IP '.$_SERVER['REMOTE_ADDR'].' output : '.print_r($xmlResult, true)." \n ";
        Atexo_Util::writeFile(Atexo_Config::getParameter('BASE_ROOT_DIR').'/logs/wsRestAnnonce.log', $log, 'a');

        return $xmlResult;
    }

    /**
     * permet recuperer une anonce
     * à partir de sa reference et l'organisme.
     *
     * @param string $organisme l'organisme,Integer $refCons la reference de l'annince
     *
     * @return string xml contenant les infos de l'annonce
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function get($organisme, $consultationId)
    {
        $params = [];
        try {
            $params['debutExecution'] = new DateTime('now');
            $params['nomBatch'] = 'WS-Annonce-récupération';
            $params['service'] = 'Annonce';
            $entree = serialize(['organisme' => $organisme, 'refConsutation' => $consultationId]);
            $params['variablesEntree'] = $entree;
            $params['informationMetier'] = $entree;
            $params['typeFlux'] = 'WebService';
            $params['poids'] = 0;
            $params['nbFlux'] = 1;
            $params['ticket'] = Atexo_Util::atexoHtmlEntities($_GET['ticket']);
            $params['code'] = Atexo_Oversight_Oversight::$codeSuccess;
        } catch (Exception $exceptionOversight) {
            $logger = Atexo_LoggerManager::getLogger('supervision');
            $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
        }

        try {
            $logger = Atexo_LoggerManager::getLogger('consultation');
            $logger->info('Debut GET Annonce....');
            $logger->info('vérification du ticket ');
            $retour = $this->validateTicket($logger);
            if (true === $retour) {
                $consultationId = $this->getRightIdConsultation($consultationId, $organisme);
                $logger->info("La recuperation de l'annonce dont la id = ".$consultationId." et l'organisme = ".$organisme.'....');
                $consultation = (new Atexo_Consultation())->retrieveConsultation($consultationId, $organisme);
                if (($consultation instanceof CommonConsultation) && $consultation->getIdTypeAvis() != Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION')) {
                    $logger->info('Vérification de droit acces ...');
                    if ($this->verifieAccesToInfo($_GET['ticket'], $organisme, $consultation->getServiceId())) {
                        $logger->info('Vérification de droit acces OK organisme '.$organisme);
                        // On renvoi le oldIdService pour clients spécifiques
                        if (true === (bool) getenv('SPECIFIQUE_PLACE')) {
                            $oldIdService = $this->getOldIdServiceByIdService($consultation->getServiceId());
                            $consultation->setServiceId($oldIdService);
                        }
                        $consultationFiled = new Atexo_Rest_Action_ConsultationField($consultation);
                        $xmlProperties = Atexo_Config::getParameter('PATH_FILE_MAPPING_ANNONCE');
                        $xmlHiddenField = Atexo_Config::getParameter('PATH_FILE_HIDDEN_FIELD_ANNONCE');
                        $listOptioanlField = Atexo_Config::getParameter('PATH_FILE_OPTIONAL_FIELD_ANNONCE');
                        $logger->info("La récupération de l'xm à partir de l'objet annonce ");
                        $serilizer = new Atexo_Serializer_Serializer();
                        $xmlResult = $serilizer->SerializeClass($consultationFiled, Atexo_Rest_Action_ConsultationField::class, $xmlProperties, $xmlHiddenField, $listOptioanlField);
                        $xmlResult = (new Atexo_Rest_Action_Actions())->encapsulerNodeResponseWsMpe($xmlResult, 'annonce');
                        $this->getHeaders('200');
                    } else {
                        $this->getHeaders('403');
                        $logger->error('vérification acces KO pr ticket '.
                            $_GET['ticket'].'organisme '.$organisme.' service '.$consultation->getServiceId());
                        $xmlResult = static::generateXmlError($this->getMessageErrorForAccessDenied($organisme, $consultation->getServiceId()));
                    }
                } else {
                    $this->getHeaders('400');
                    $logger->error("L'annonce n'existe pas");
                    $xmlResult = static::generateXmlError("L'annonce n'existe pas");
                }
            } else {
                $params['code'] = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;

                return $retour;
            }
        } catch (Exception $e) {
            $params['code'] = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
            $logger->error('Erreur technique = '.$e->getMessage().$e->getTraceAsString());
            $this->getHeaders('500');
            $xmlResult = static::generateXmlError('Erreur technique');
        }
        $logger->info('Fin  GET Annonce....');

        try {
            $params['oversight'] = self::oversight($params);
        } catch (Exception $exceptionOversight) {
            $logger = Atexo_LoggerManager::getLogger('supervision');
            $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
        }

        return $xmlResult;
    }
}
