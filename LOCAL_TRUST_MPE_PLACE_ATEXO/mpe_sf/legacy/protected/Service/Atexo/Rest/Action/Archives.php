<?php

namespace Application\Service\Atexo\Rest\Action;

use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Oversight\Atexo_Oversight_Oversight;
use Exception;

/*
 * Classe de gestion de la recuperation des archives de consultations
 *
 * @author : Oumar KONATE <oumar.konate@atexo.com>
 * @version : 1.0
 * @since : 4.8.0
 * @copyright Atexo 2014
 * @package  Atexo_Rest_Action
 */
class Atexo_Rest_Action_Archives extends Atexo_Rest_Action_Actions
{
    /**
     * Permet de recuperer les ids des consultations.
     *
     * @param string $date: date de generation des archives
     *
     * @return string: xml
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function get($date)
    {
        $params = [];
        $params['nomBatch'] = 'WS-archives-by-date';
        $params['service'] = 'Archives';
        $params['typeFlux'] = 'WebService';
        $params['ticket'] = Atexo_Util::atexoHtmlEntities($_GET['ticket']);
        self::oversight($params);
        try {
            $logger = Atexo_LoggerManager::getLogger();
            $logger->info('Verification du ticket');
            $retour = $this->validateTicket($logger);
            if (true === $retour) {
                $logger->info('Ticket valide');
                $acronymeOrgs = $this->getAllOrganismesAllowed($_GET['ticket']);
                $xmlResponse = $this->generateXmlIdsConsultations($date, $acronymeOrgs, $logger);
                $logger->info("Encapsulation de l'xml: ".$xmlResponse);
                $xml = $this->encapsulerNodeResponseWsMpe($xmlResponse, 'archives');
                $this->getHeaders('200');
                $logger->info('Xml a retourner: '.$xml);
                $params['code'] = Atexo_Oversight_Oversight::$codeSuccess;
                $res = $xml;
            } else {
                $params['code'] = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
                $res = $retour;
            }
            try {
                self::oversight($params);
            } catch (Exception $exceptionOversight) {
                $logger = Atexo_LoggerManager::getLogger('supervision');
                $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
            }

            return $res;
        } catch (Exception $e) {
            $logger->error('Erreur technique = '.$e->getMessage().' '.$e->getTraceAsString());
            $this->getHeaders('500');

            return static::generateXmlError('Erreur technique'.$e->getMessage().' '.$e->getTraceAsString());
        }
    }

    /**
     * Permet de generer la structure de l'xml retour.
     *
     * @param string $date:   date de generation des archives
     * @param Logger $logger: logger
     *
     * @return string: xml
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function generateXmlIdsConsultations($date, $acronymeOrgs, $logger)
    {
        try {
            $date = strftime('%F', $date);
            $logger->info("Recuperation de la liste des consultations archives le : $date");
            $consultations = (new Atexo_Consultation())->retrieveConsultationArchiveeByDate($date, $acronymeOrgs, $logger);
            $xml = '<archives>';
            if (is_array($consultations) && count($consultations)) {
                $logger->info('Consultation(s) trouve(es)');
                $logger->info("Construction de la structure de l'xml");
                foreach ($consultations as $consultation) {
                    $xml .= '<archive>';
                    $xml .= '<id>'.$consultation['id'].'</id>';
                    $xml .= '<organisme>'.$consultation['organisme'].'</organisme>';
                    $xml .= '<dateArchivage>'.Atexo_Util::getDateXmlFromDateTimeVal($consultation['dateArchivage']).'</dateArchivage>';
                    $xml .= '</archive>';
                }
            } else {
                $logger->info('Consultation non trouvee');
            }
            $xml .= '</archives>';

            return $xml;
        } catch (Exception $e) {
            $logger->error('Erreur technique = '.$e->getMessage().' '.$e->getTraceAsString());
            $this->getHeaders('500');

            return static::generateXmlError('Erreur technique '.$e->getMessage().' '.$e->getTraceAsString());
        }
    }
}
