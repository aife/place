<?php

namespace Application\Service\Atexo\Rest\Action;

use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_LoggerManager;
use GuzzleHttp\Client as guzzleClient;

class Atexo_Rest_Action_WebService extends Atexo_Rest_Action_Actions
{
    public function post($service, $fonction)
    {
        return $this->doTraitement('POST', $service, $fonction);
    }

    public function get($service, $fonction)
    {
        return $this->doTraitement('GET', $service, $fonction);
    }

    public function put($service, $fonction)
    {
        return $this->doTraitement('PUT', $service, $fonction);
    }

    public function doTraitement($methode, $service, $fonction)
    {
        $return = null;
        try {
            $response = null;
            $logger = Atexo_LoggerManager::getLogger('webservices');
            $ticket = '';
            if (isset($_GET['ticket'])) {
                $ticket = $_GET['ticket'];
                unset($_GET['ticket']);
                $_GET['token'] = $ticket;
            }
            $options = [];

            $uri = Atexo_Config::getParameter('URL_LT_MPE_WS_SF').'/'.Atexo_Config::getParameter('VERSION_LT_MPE_WS_SF').'/'.$service.'/'.$fonction;
            $params = '';
            if (!empty($_GET)) {
                foreach ($_GET as $key => $value) {
                    $params .= '&'.$key.'='.$ticket;
                }
            }
            if (!empty($params)) {
                $uri .= '?'.ltrim($params, '&');
            }

            if (!empty($_FILES)) {
                foreach ($_FILES as $key => $fileInfo) {
                    $file = [
                        'name' => $key,
                        'contents' => fopen($fileInfo['tmp_name'], 'r'),
                        'filename' => $fileInfo['name'],
                        'headers' => [
                            'Content-Type' => $fileInfo['type'],
                        ],
                    ];
                    $options['multipart'][] = $file;
                }
                if (!empty($_POST)) {
                    foreach ($_POST as $key => $post) {
                        $param = [
                           'name' => $key,
                           'contents' => $post,
                        ];
                        $options['multipart'][] = $param;
                    }
                }
            } else {
                if (!empty($_POST)) {
                    $options['form_params'] = $_POST;
                }
            }

            $logger->info("Debut requete: methode = $methode, uri = $uri, options = ".print_r($options, true));
            $client = new guzzleClient();
            if (method_exists($client, 'request')) {
                $response = $client->request($methode, $uri, $options);
            } else {
                $response = $client->$methode($uri, $options);
            }

            $return = $response->getBody()->getContents();
            $codeStatus = $response->getStatusCode();
            $contentType = $response->getHeader('Content-Type');
            if (method_exists($client, 'request')) {
                $this->getHeaders($codeStatus, $contentType[0]);
            } else {
                $this->getHeaders($codeStatus, $contentType);
            }

            $logger->info("fin requete: methode = $methode, uri = $uri, options = ".print_r($options, true).", resultat :  StatusCode = $codeStatus, body = ".$return);
        } catch (\Exception $e) {
            $logger->error("Error requete: methode = $methode, uri = $uri, options =".print_r($options, true).'erreur : '.$e->getMessage().' '.$e->getTraceAsString());
            if ($response) {
                $codeStatus = $response->getStatusCode();
                $contentType = $response->getHeader('Content-Type');
                $this->getHeaders($codeStatus, $contentType[0]);
            }
        }

        return $return;
    }
}
