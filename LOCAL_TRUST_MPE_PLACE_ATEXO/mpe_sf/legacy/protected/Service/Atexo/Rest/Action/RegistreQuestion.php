<?php

namespace Application\Service\Atexo\Rest\Action;

use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonQuestionsDce;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Oversight\Atexo_Oversight_Oversight;
use Application\Service\Atexo\Registres\Atexo_Registres_Questions;
use Application\Service\Atexo\Rest\ObjectField\Atexo_Rest_ObjectField_QuestionFichierField;
use Application\Service\Atexo\Serializer\Atexo_Serializer_Serializer;
use DateTime;
use Exception;

/*
 * permet de recuperer les infos concernant l'objet questionsDce dont l'id est fourni
 *
 * @author ASO <ayoub.souidahmed@atexo.com>
 * @version 1.0
 * @since 4.6.0
 * @copyright Atexo 2014
 * @package Atexo_Rest_Action
 */
class Atexo_Rest_Action_RegistreQuestion extends Atexo_Rest_Action_Actions
{
    /*
     * genere un xml (valide ws MPE) contenant les informations de la question dont l'id est fourni
     *
     * @param Integer $idQuestion l'id de l'objet du CommonquestionsDce
     * @return string xml contenant les infos de la question
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @version 1.0
     * @since 4.6.0
     * @copyright Atexo 2014
     */
    public function get($idQuestion)
    {
        $blob = [];
        $params = [];
        try {
            $params['debutExecution'] = new DateTime('now');
            $params['nomBatch'] = 'WS-question-récupération';
            $params['service'] = 'Registre';
            $entree = serialize(['question' => $idQuestion]);
            $params['variablesEntree'] = $entree;
            $params['informationMetier'] = $entree;
            $params['typeFlux'] = 'WebService';
            $params['poids'] = 0;
            $params['nbFlux'] = 1;
            $params['ticket'] = Atexo_Util::atexoHtmlEntities($_GET['ticket']);
            self::oversight($params);
            $params['code'] = Atexo_Oversight_Oversight::$codeSuccess;
        } catch (Exception $exceptionOversight) {
            $logger = Atexo_LoggerManager::getLogger('supervision');
            $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
        }

        try {
            $logger = Atexo_LoggerManager::getLogger();
            $logger->info('Debut GET Registre Question....');

            $logger->info("La recuperation de la question dont l'id = ".$idQuestion);
            $questionObject = (new Atexo_Registres_Questions())->getQuestionById($idQuestion);
            $acronymeOrg = null;
            $ref = null;

            if ($questionObject instanceof CommonQuestionsDce) {
                $logger->info('La question existe');
                $acronymeOrg = $questionObject->getOrganisme();
                $ref = $questionObject->getConsultationId();
            }
            $logger->info('La vérification du ticket');
            $retour = $this->validateTicket($logger);
            if (true === $retour) {
                $consultation = (new Atexo_Consultation())->retrieveConsultation($ref, $acronymeOrg);
                if (!($consultation instanceof CommonConsultation)
                    ||
                    !($this->verifieAccesToInfo($_GET['ticket'], $consultation->getOrganisme(), $consultation->getServiceId()))
                ) {
                    $consultationExist = ($consultation instanceof CommonConsultation);
                    $log = $consultationExist ?
                        ('Vérification de droit acces KO organisme '.$consultation->getOrganisme().' service '.$consultation->getServiceId()) :
                        ("consultation n'existe pas pr qst id ".$idQuestion);
                    $logger->info($log);
                    $serviceId = $consultationExist ? $consultation->getServiceId() : null;
                    $organisme = $consultationExist ? $consultation->getOrganisme() : null;

                    return self::generateXmlError($this->getMessageErrorForAccessDenied($organisme, $serviceId));
                }
                if ($questionObject instanceof CommonQuestionsDce) {
                    $logger->info('La question existe');
                    $questionObject->setDateDepot(Atexo_Util::getDateXmlFromDateTimeVal($questionObject->getDateDepot()));
                    $contentType = 'application/octet-stream';
                    if ($questionObject->getIdFichier() && $questionObject->getNomFichier()) {
                        $fichier = new Atexo_Rest_ObjectField_QuestionFichierField();
                        $fichier->setId($questionObject->getIdFichier());
                        $fichier->setNomFichierQuestion($questionObject->getNomFichier());
                        $contentType = Atexo_Util::getContentTypeByExtension(Atexo_Util::getExtension($questionObject->getNomFichier()));
                        $questionObject->setFichier($fichier);
                    }
                    $xmlResponse = self::getXmlInfosQuestion($questionObject);
                    $xml = self::encapsulerNodeResponseWsMpe($xmlResponse, 'registreQuestion');
                    $logger->info("Creer l'xml à partir de l'objet common question (serialise)");
                    if ($questionObject->getIdFichier()) {
                        $logger->info('Recuperation du fichier joint à la question');
                        $blob = Atexo_Blob::getBlobById($questionObject->getIdFichier(), $questionObject->getOrganisme(), $questionObject->getNomFichier());
                        $logger->info('La verfication de la taille du fichier joint à la question');
                        if ($blob['sizeBlob'] && $blob['sizeBlob'] > Atexo_Config::getParameter('MAX_SIZE_FILE_WS_REST')) {
                            $this->getHeaders('413');
                            $logger->error('La taille du fichier  joint à la question depasse la taille maximum');
                            try {
                                $params['finExecution'] = 'endScript';
                                $params['oversight'] = self::oversight($params);
                                unset($params['finExecution']);
                                unset($params['oversight']);
                            } catch (Exception $exceptionOversight) {
                                $logger = Atexo_LoggerManager::getLogger('supervision');
                                $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
                            }

                            return static::generateXmlError('La taille du fichier joint à la question dépasse la taille maximum');
                        }
                    }
                    if ($blob['content']) {
                        $arrayBlob = [$blob];
                        self::sendDataWithFile($arrayBlob, $xml, 'binary', $contentType, false);
                        $logger->info('Ajout du fichier à la reponse');

                        try {
                            self::oversight($params);
                        } catch (Exception $exceptionOversight) {
                            $logger = Atexo_LoggerManager::getLogger('supervision');
                            $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
                        }

                        return '';
                    } else {
                        try {
                            self::oversight($params);
                        } catch (Exception $exceptionOversight) {
                            $logger = Atexo_LoggerManager::getLogger('supervision');
                            $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
                        }

                        $this->getHeaders('200');

                        return $xml;
                    }
                } else {
                    try {
                        self::oversight($params);
                    } catch (Exception $exceptionOversight) {
                        $logger = Atexo_LoggerManager::getLogger('supervision');
                        $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
                    }

                    $msgError = 'Aucune Question ne correspond à cet Id';
                    $logger->error($msgError);
                    $this->getHeaders('404');

                    return self::generateXmlError($msgError);
                }
            } else {
                try {
                    self::oversight($params);
                } catch (Exception $exceptionOversight) {
                    $logger = Atexo_LoggerManager::getLogger('supervision');
                    $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
                }

                return $retour;
            }
        } catch (Exception $e) {
            try {
                $params['code'] = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
                self::oversight($params);
            } catch (Exception $exceptionOversight) {
                $logger = Atexo_LoggerManager::getLogger('supervision');
                $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
            }

            $logger->error('Erreur technique = '.$e->getMessage().' '.$e->getTraceAsString());
            $this->getHeaders('500');

            return self::generateXmlError('erreur techniques');
        }
    }

    /*
     * genere un xml contenant les infos d'une question
     *
     * @param CommonquestionsDce $questionObject objet du CommonquestionsDce
     * @return string xml contenant les infos d'une question
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @version 1.0
     * @since 4.6.0
     * @copyright Atexo 2014
     */
    public function getXmlInfosQuestion($questionObject)
    {
        $serializer = new Atexo_Serializer_Serializer();
        $mappingFile = Atexo_Config::getParameter('PATH_FILE_MAPPING_QUESTION');
        $hiddenField = Atexo_Config::getParameter('PATH_FILE_HIDDEN_FIELD_QUESTION');
        $listOptioanlField = Atexo_Config::getParameter('PATH_FILE_OPTIONAL_FIELD_REGISTRE_QUESTION');
        $xml = $serializer->SerializeClass($questionObject, 'CommonQuestionsDce', $mappingFile, $hiddenField, $listOptioanlField);

        return $xml;
    }
}
