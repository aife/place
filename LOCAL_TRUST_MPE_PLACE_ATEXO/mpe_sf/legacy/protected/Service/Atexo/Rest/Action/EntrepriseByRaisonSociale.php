<?php

namespace Application\Service\Atexo\Rest\Action;

use Application\Propel\Mpe\Entreprise;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Entreprise;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Oversight\Atexo_Oversight_Oversight;
use Application\Service\Atexo\Serializer\Atexo_Serializer_Serializer;
use DateTime;
use Exception;

/*
 * permet d'avoir  une entreprise à partir de son id
 *
 * @author LEZ <loubna.ezziani@atexo.com>
 * @version 1.0
 * @since 4.7.0
 * @copyright Atexo 2014
 * @package Atexo_Rest_Action
 *
 */
class Atexo_Rest_Action_EntrepriseByRaisonSociale extends Atexo_Rest_Action_Actions
{
    /*
     * permet de recuperer une entreprise à partir de son raison sociale
     *
     * @param String $raisonS le raison sociale de l'entreprise
     * @return string xml contenant les infos de l'entreprise
     * @author LEZ <loubna.ezziani@atexo.com>
     * @version 1.0
     * @since 4.7.0
     * @copyright Atexo 2014
     */
    public function get($raisonS)
    {
        try {
            $params = [];

            try {
                $params['debutExecution'] = new DateTime('now');
                $params['nomBatch'] = 'WS-entreprise-by-raison-sociale';
                $params['service'] = 'Entreprise';
                $entree = serialize(['siren' => $raisonS]);
                $params['variablesEntree'] = $entree;
                $params['informationMetier'] = $entree;
                $params['typeFlux'] = 'WebService';
                self::oversight($params);
                $params['poids'] = 0;
                $params['nbFlux'] = 1;
                $params['ticket'] = Atexo_Util::atexoHtmlEntities($_GET['ticket']);
                self::oversight($params);
                $params['code'] = Atexo_Oversight_Oversight::$codeSuccess;
            } catch (Exception $exceptionOversight) {
                $logger = Atexo_LoggerManager::getLogger('supervision');
                $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
            }

            $logger = Atexo_LoggerManager::getLogger();
            $logger->info('Debut GET Entreprise by Raison sociale....');
            $retour = $this->validateTicket($logger);

            if (true === $retour) {
                $nameSociete = base64_decode($raisonS);
                $logger->info("La recuperation de l'entreprise dont le raison sociale = ".$nameSociete);
                $entreprise = (new Atexo_Entreprise())->retrieveCompanyBdeByName($nameSociete);

                if ($entreprise instanceof Entreprise) {
                    $xmlProperties = Atexo_Config::getParameter('PATH_FILE_MAPPING_ENTREPRISE');
                    $xmlHiddenField = Atexo_Config::getParameter('PATH_FILE_HIDDEN_FIELD_ENTREPRISE');
                    $listOptioanlField = Atexo_Config::getParameter('PATH_FILE_OPTIONAL_FIELD_ENTREPRISE');
                    $logger->info("La récupération de l'xml à partir de l'objet entreprise ");
                    $serilizer = new Atexo_Serializer_Serializer();
                    $xmlResult = $serilizer->SerializeClass($entreprise, 'Entreprise', $xmlProperties, $xmlHiddenField, $listOptioanlField);
                    $xmlResult = Atexo_Rest_Action_Actions::encapsulerNodeResponseWsMpe($xmlResult, 'entreprise');
                    $this->getHeaders('200');
                    try {
                        self::oversight($params);
                    } catch (Exception $exceptionOversight) {
                        $logger = Atexo_LoggerManager::getLogger('supervision');
                        $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
                    }
                } else {
                    try {
                        $params['code'] = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
                        self::oversight($params);
                    } catch (Exception $exceptionOversight) {
                        $logger = Atexo_LoggerManager::getLogger('supervision');
                        $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
                    }
                    $this->getHeaders('400');
                    $logger->error("L'entreprise n'existe pas");
                    $xmlResult = static::generateXmlError("L'entreprise : ".$nameSociete." n'existe pas");
                }
            } else {
                try {
                    $params['code'] = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
                    self::oversight($params);
                } catch (Exception $exceptionOversight) {
                    $logger = Atexo_LoggerManager::getLogger('supervision');
                    $logger->error('Error sur la supervision : '.$exceptionOversight->getMessage());
                }

                return $retour;
            }
        } catch (Exception $e) {
            $params['code'] = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
            $logger->error('Erreur technique = '.$e->getMessage().' '.$e->getTraceAsString());
            $this->getHeaders('500');
            $xmlResult = static::generateXmlError('Erreur technique');
        }

        $logger->info('Fin  GET  Entreprise by Raison sociale....');

        return $xmlResult;
    }
}
