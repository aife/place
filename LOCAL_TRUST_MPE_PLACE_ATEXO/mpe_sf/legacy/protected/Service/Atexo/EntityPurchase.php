<?php

namespace Application\Service\Atexo;

use App\Entity\Service;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonAffiliationService;
use Application\Propel\Mpe\CommonAffiliationServicePeer;
use Application\Propel\Mpe\CommonConsultationPeer;
use Application\Propel\Mpe\CommonOrganisme;
use Application\Propel\Mpe\CommonService;
use Application\Propel\Mpe\CommonServicePeer;
use Application\Propel\Mpe\CommonServiceQuery;
use Application\Propel\Mpe\CommonTFusionnerServicesQuery;
use Application\Service\Atexo\EntityPurchase\Atexo_EntityPurchase_RPA;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_HistoriqueSynchroSGMAP;
use Application\Service\Atexo\Entreprise\InterfaceSynchroApiGouvEntreprise;
use PDO;
use Prado\Prado;

/**
 * Classe de manipulation des entités d'achat.
 *
 * @author ZAKI Anas <anas.zaki@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_EntityPurchase
{
    /**
     * Retourne la liste des entités d'achat.
     *
     * @param bool $organisme si false on récupére toutes les entités d'achats de l'organisme de l'agent connecté,
     *                        sinon elle retourne les entités d'achats de l'organisme passé en paramétre
     * @param bool $withPath  si true on affiche le chemin complet du service
     *
     * @return array
     */
    public static function getEntityPurchase($organisme, $withPath = null, $langue = null, $force = false)
    {
        $entityPurchaseArray = [];
        $entityPurchase = self::getCachedEntities($organisme, $force);
        if (is_array($entityPurchase)) {
            foreach ($entityPurchase as $entity) {
                if (true == $withPath) {
                    $entityPurchaseArray[$entity->getId()] = $entity->getPathServiceTraduit();
                } else {
                    $entityPurchaseArray[$entity->getId()] = $entity->getSigle();
                }
            }
        }
        $organismeO = Atexo_Organismes::retrieveOrganismeByAcronyme($organisme);
        $getDenominationOrg = 'getDenominationOrg'.Atexo_Languages::getLanguageAbbreviation($langue);
        if ($organismeO instanceof CommonOrganisme) {
            if (0 == strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE'))
                || !$organismeO->$getDenominationOrg()) {
                $entityPurchaseArray = Atexo_Util::arrayUnshift($entityPurchaseArray, $organismeO->getSigle().' - '.$organismeO->getDenominationOrg());
            } else {
                $entityPurchaseArray = Atexo_Util::arrayUnshift($entityPurchaseArray, $organismeO->getSigle().' - '.$organismeO->$getDenominationOrg());
            }
        }

        return $entityPurchaseArray;
    }

    /**
     * Retourne le service passé en paramétre et la liste de ses fils sous la forme : (service1, service2,...).
     *
     * @param $idService : l'ID du service
     * @param $org Organisme
     *
     * @return string (service1, service2,...)
     */
    public static function retrieveAllChildrenServices($idService, $org, $returnArray = false)
    {
        $arrayChilds = [];
        if ($idService) {
            $arrayChilds = self::getSubServices($idService, $org, true);
        } else {
            $services = self::getCachedEntities($org);
            if (!$idService) {
                $arrayChilds[] = null;
                $arrayChilds[] = '0';
            }
            if ($services) {
                foreach ($services as $id => $service) {
                    $arrayChilds[] = $id;
                }
            }
        }

        if($returnArray){
            return $arrayChilds;
        }

        $stringIdsOfServicesWithPermission = '';
        if ($arrayChilds) {
            $stringIdsOfServicesWithPermission = ' ( ';
            $indice = 0;
            foreach ($arrayChilds as $id) {
                if (isset($id)) {
                    $stringIdsOfServicesWithPermission .= (($indice > 0) ? ',' : '').$id;
                    ++$indice;
                }
            }

            $stringIdsOfServicesWithPermission .= ' ) ';
        }

        return $stringIdsOfServicesWithPermission;
    }

    /**
     * retourne l'objet service é partir de l'id passé en paramétre.
     *
     * @param  $idService id du service recherché
     * @param $org Organismes
     *
     * @return $service
     */
    public static function retrieveEntityById($idService, $org, $force = false)
    {
        $entityPurchase = self::getCachedEntities($org, $force);
        if ($entityPurchase[$idService] instanceof CommonService) {
            return $entityPurchase[$idService];
        } else {
            return false;
        }
    }

    /**
     * retourne le id_service dont service est passé en paramètre.
     *
     * @deprecated
     *
     * @param  $service du service rechercher
     *
     * @return $idService
     */
    public static function retrieveIdByEntity($service)
    {
        //TODO : le commentaire ne correspond pas au traitement fait par la fonctions
        //$connexionOrg = Propel::getConnection(Atexo_CurrentUser::getCurrentOrganism());
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonServicePeer::SERVICE, $service, Criteria::EQUAL);
        $c->add(CommonServicePeer::ORGANISME, Atexo_CurrentUser::getCurrentOrganism());
        $idService = CommonServicePeer::doSelectOne($c, $connexion);

        return $idService;
    }

    /**
     * teste si entity1 est père direct de entity2.
     *
     * @param int $idEntity1 l'id de l'entité 1
     * @param int $idEntity2 l'id de l'entité 2
     *
     * @deprecated
     */
    public static function isDirectParent($idEntity1, $idEntity2)
    {
        $c3 = new Criteria();
        $c3->add(CommonAffiliationServicePeer::SERVICE_PARENT_ID, $idEntity1, Criteria::EQUAL);
        $c3->addJoin(CommonAffiliationServicePeer::SERVICE_PARENT_ID, CommonServicePeer::ID);
        $servicesAffectes = CommonAffiliationServicePeer::doSelect($c3);

        if (is_array($servicesAffectes)) {
            if (in_array($idEntity2, $servicesAffectes)) {
                return true;
            }
        } else {
            // Pas de service affecté au péle
            return false;
        }
    }

    /**
     * teste si entity1 est un enfant direct de entity2.
     *
     * @param int $idEntity1 l'id de l'entité 1
     * @param int $idEntity2 l'id de l'entité 2
     *
     * @deprecated
     */
    public static function isDirectChild($idEntity1, $idEntity2)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $organisme = Atexo_CurrentUser::getCurrentOrganism();
        $c1 = new Criteria();
        $c1->add(CommonAffiliationServicePeer::SERVICE_PARENT_ID, $idEntity1, Criteria::EQUAL);
        $c1->add(CommonAffiliationServicePeer::ORGANISME, $organisme, Criteria::EQUAL);
        $c1->addJoin(CommonAffiliationServicePeer::SERVICE_ID, CommonServicePeer::ID);
        $polesService = CommonServicePeer::doSelect($c1, $connexion);
        if (is_array($polesService)) {
            if (in_array($idEntity2, $polesService)) {
                return true;
            }
        } else {
            return false;
        }
    }

    public function hasParent($service, $org)
    {
        if (self::getParent($service, $org)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * retourne l'id du service parent ou NULL s'il
     * n'y en a pas.
     *
     * @param $serviceId
     * @param $org
     *
     * @return (id, libelle)
     */
    public static function getParent($serviceId, $org)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonAffiliationServicePeer::SERVICE_ID, $serviceId, CRITERIA::EQUAL);
        $c->add(CommonAffiliationServicePeer::ORGANISME, $org, CRITERIA::EQUAL);
        $result = CommonAffiliationServicePeer::doSelectOne($c, $connexion);
        if ($result) {
            return $result->getServiceParentId();
        } else {
            return false;
        }
    }

    /**
     * retourne un tableau ordonné de tous les parents
     * du service $service.
     *
     * @param $serviceId
     * @param $org
     *
     * @return array(id, libelle)
     */
    public static function getAllParents($serviceId, $org)
    {
        $listParent = [];
        $allParentsIds = [];
        $idParent = self::getParent($serviceId, $org);
        $index = 0;
        while ($idParent) {
            if (in_array($idParent, $allParentsIds)) {
                break;
            }
            $listParent[$index]['id'] = $idParent;
            $allParentsIds[] = $idParent;
            $entityParent = self::retrieveEntityById($idParent, $org);
            if ($entityParent) {
                $listParent[$index]['libelle'] = $entityParent->getSigle();
            }
            ++$index;
            $idParent = self::getParent($idParent, $org);
        }
        $listParent[$index]['id'] = 0;
        $listParent[$index]['libelle'] = '';

        return $listParent;
    }

    /**
     * retourne un tableau ordonné des id des parents
     * du service $serviceId.
     *
     * @param int  $serviceId
     * @param bool $includeCurrService si true, le service $serviceId est inclus dans le résultat
     *
     * @return array(id)
     */
    public static function getArParentsId($serviceId, $org, $includeCurrService = false)
    {
        $listParent = [];
        $idParent = self::getParent($serviceId, $org);
        while ($idParent && !in_array($idParent, $listParent)) {
            $listParent[] = $idParent;
            $idParent = self::getParent($idParent, $org);
        }
        $listParent[] = null;

        if ($includeCurrService) {
            if (!in_array($serviceId, $listParent)) {
                $listParent = [...[
                    $serviceId,
                ], ...$listParent];
            }
        }

        return $listParent;
    }

    /**
     * retourne le libelle de l'entite d'achat ou - pour l'entite vide.
     *
     * @param $service objet de la classe Service
     *
     * @return string|-
     */
    public static function retrieveLibelleService($service)
    {
        if ($service && $service instanceof CommonService) {
            return $service->getSigle();
        }

        return Atexo_Config::getParameter('VOID_ENTITY_PURCHASE_VALUE');
    }

    /**
     * retourne le nom service en fonction de l'id du service.
     *
     * @param int    $idService l'id du service
     * @param string $org       acronyme de l'organisme
     *
     * @return string le nom du service
     */
    public static function getServiceAgentById($idService, $org)
    {
        $entityPurchase = self::getCachedEntities($org);
        if ($entityPurchase[$idService] instanceof CommonService) {
            return $entityPurchase[$idService]->getSigle();
        } else {
            return false;
        }
    }

    public static function getLibelleServiceAgentById($idService, $org)
    {
        $entityPurchase = self::getCachedEntities($org);
        if ($entityPurchase[$idService] instanceof CommonService) {
            return $entityPurchase[$idService]->getLibelle();
        } else {
            return false;
        }
    }

    /**
     * retourne le nom service en fonction de l'id du service.
     *
     * @param int $idService l'id du service
     *
     * @return string le nom du service
     */
    public static function getServiceById($idService, $org)
    {
        if (0 != $idService) {
            return self::getServiceAgentById($idService, $org);
        } else {
            $organismeO = Atexo_Organismes::retrieveOrganismeByAcronyme($org);

            return $organismeO->getSigle();
        }
    }

    /**
     * retourne un tableau des entités filles de l'entité passée en paramétre
     * n'y en a pas.
     *
     * @param $idPurchaseEntity Service Id
     * @param $org Acronyme de l'organisme'
     *
     * @return (id, libelle)
     */
    public static function getLevelOneChilds($idPurchaseEntity, $org)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $resServices = [];
        if (0 === $idPurchaseEntity) {
            // Récupération des fils niveau 1
            return (new Atexo_EntityPurchase())->getLevelOneServices($org);
        } else {
            $c->add(CommonAffiliationServicePeer::SERVICE_PARENT_ID, $idPurchaseEntity);
            $c->add(CommonAffiliationServicePeer::ORGANISME, $org);
        }
        $c->addAscendingOrderByColumn(CommonServicePeer::SIGLE);
        $resAffServices = (new CommonAffiliationService())->doSelectJoinCommonServiceRelatedByIdService($c, $connexion);
        if (is_array($resAffServices) && count($resAffServices) > 0) {
            foreach ($resAffServices as $affService) {
                $resServices[] = $affService->getCommonServiceRelatedByIdService();
            }
        }

        return $resServices;
    }

    /**
     * retourne un tableau ordonné de tous les fils.
     *
     * @param $idPurchaseEntity Service Id
     * @param $arrayChilds output array
     * @param $org String Acronyme de l'organisme
     *
     * @return void
     */
    public static function getAllChilds($idPurchaseEntity, &$arrayChilds, $org)
    {
        if (is_array($arrayChilds)) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
            $levelOneChilds = self::getLevelOneChilds($idPurchaseEntity, $org);
            $parents = self::getArParentsId($idPurchaseEntity, $org);

            $depth = count($parents);
            if (0 != $idPurchaseEntity) {
                $purchaseEntity = CommonServicePeer::retrieveByPK($idPurchaseEntity, $connexion);
                $purchaseEntity->setDepth($depth);
                $arrayChilds[] = $purchaseEntity;
            }
            if (is_array($levelOneChilds) && 0 != count($levelOneChilds)) {
                foreach ($levelOneChilds as $child) {
                    self::getAllChilds($child->getId(), $arrayChilds, $org);
                }
            }
        }

        return $arrayChilds;
    }

    /**
     * retourne tout les sous services d'un autre.
     *
     * @param int $idServiceAgent l'id de l'agent
     *
     * @return la liste des sous services avec leurs chemins
     */
    public static function getSubServices($idServiceAgent, $org, $justIds = false, $withLibelle = false)
    {
        $serviceAgent = self::retrieveEntityById($idServiceAgent, $org);
        if ($serviceAgent) {
            $result = [];
            self::getAllChilds($serviceAgent->getId(), $result, $org);
            if ($justIds) {
                $idsArray = [];
                foreach ($result as $service) {
                    $idsArray[] = $service->getId();
                }

                return $idsArray;
            }

            return self::getAllChildsWithPath($result, $org, $withLibelle);
        } else {
            return [];
        }
    }

    /**
     * retourne un tableau de fils.
     *
     * @param array $listChilds
     * @param $org
     */
    public static function getAllChildsWithPath($listChilds, $org, $withLibelle = false)
    {
        $childArrayWithPath = [];
        array_shift($listChilds);
        if (is_array($listChilds)) {
            foreach ($listChilds as $child) {
                $childArrayWithPath['id'] = $child->getId();
                $childArrayWithPath['libelle'] = self::getServiceById($child->getId(), $org);
                $parents = self::getAllParents($child->getId(), $org);
                $childArrayWithPath['path'][$child->getId()] = Atexo_Util::implodeArrayWithSeparator($childArrayWithPath['libelle'], $parents, ' / ').(($withLibelle) ? ' - '.self::getLibelleServiceAgentById($child->getId(), $org) : '');
            }
        }

        return $childArrayWithPath['path'];
    }

    /**
     * retourne true si $idService2 est un descendant de $idService1.
     *
     * @param $idService1 id du parent
     * @param $idService2 id du descendant
     */
    public static function isParent($idService1, $idService2, $organisme): bool
    {
        $arParentsId = self::getArParentsId($idService2, $organisme);

        return in_array($idService1, $arParentsId);
    }

    /**
     * retourne la liste des entités d'achat mises en cache.
     *
     * @param string $organisme
     *
     * @return array
     */
    public static function getCachedEntities($organisme, $force = false)
    {
        //si c'est un agent qui est connecté et  qu'il demande les services de son organisme, on va les chercher dans la session
        //ce mécanisme est mis en place pour prendre en compte le fait que l'on peut avoir plusieurs frontaux web
        //dans ce cas, il faut qu'il y ait une consistance des données dans le cas ou l'agent créé des entités et qu'il bascule sur un autre frontal qu'il les retrouve bien
        //en effet, le cache APC est en mémoire et n'est pas partagé entre les frontaux
        if (
            (Atexo_CurrentUser::isAgent() || Atexo_CurrentUser::isAgentSocle())
            && Atexo_CurrentUser::isConnected() && Atexo_CurrentUser::getOrganismAcronym() == $organisme
            && $force === false
        ) {
            //d'abord on récupère les services de la session
            $allServices = Atexo_CurrentUser::readFromSession('cachedServices');

            //si les services ne sont pas dans la session, on va les récupérer de la BD et le mettre en cache dans la session de l'utilisateur
            if (!$allServices || !is_array($allServices)) {
                $allServices = self::retrieveEntities($organisme);
                Atexo_CurrentUser::writeToSession('cachedServices', $allServices);
            }

            return $allServices;
        }

        return self::retrieveEntities($organisme);
    }

    /**
     * Méthode qui permet d'invalider le cache et de recharger la liste des entités d'achat pour l'organisme passé en paramètre.
     *
     * @param $organisme
     */
    public function reloadCachedEntities($organisme)
    {
        //si c'est un agent qui est connecté, il faut supprimer les services qui sont stockés en session
        if ((Atexo_CurrentUser::isAgent() || Atexo_CurrentUser::isAgentSocle()) && Atexo_CurrentUser::isConnected() && Atexo_CurrentUser::getOrganismAcronym() == $organisme) {
            Atexo_CurrentUser::deleteFromSession('cachedServices');
        }
        self::getCachedEntities($organisme);
    }

    /**
     * méthode qui permet de récupérer la liste des entités d'achat d'un organisme donné en parmètre
     * le tri se fait sur le chemin complet asc.
     *
     * @param string $organisme
     *
     * @return array
     */
    public static function retrieveEntities($organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonServicePeer::ORGANISME, $organisme);
        $c->addAscendingOrderByColumn(CommonServicePeer::CHEMIN_COMPLET);
        $entityPurchase = CommonServicePeer::doSelect($c, $connexion);
        $allServices = [];
        foreach ($entityPurchase as $oneEntity) {
            $allServices[$oneEntity->getId()] = $oneEntity;
        }

        return $allServices;
    }

    /**
     * retourne le des services de niveau 1 lié directement a l'organisme racine (id=0).
     *
     * @param $org la connexion
     *
     * @return Service[]
     */
    public function getLevelOneServices($org)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonServicePeer::ORGANISME, $org);
        $c->addAscendingOrderByColumn(CommonServicePeer::SIGLE);
        $arrayServices = CommonServicePeer::doSelect($c, $connexion);
        $result = [];
        foreach ($arrayServices as $service) {
            if (!self::hasParent($service->getId(), $org)) {
                $result[] = $service;
            }
        }

        return $result;
    }

    /**
     * retourne le chemin de l'entité d'achat.
     */
    public function getEntityPath($consultationId, $organisme)
    {
        $entityPath = null;
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $consultation = CommonConsultationPeer::retrieveByPK($consultationId, $connexion);
        if ($consultation) {
            $entityId = $consultation->getServiceId();
        }
        $entity = CommonServicePeer::retrieveByPk($entityId, $connexion);
        $parents = self::getAllParents($entityId, $organisme);
        if ($parents && $entity) {
            $entityPath = Atexo_Util::implodeArrayWithSeparator($entity->getSigle(), $parents, ' / ');
        }

        return $entityPath;
    }

    /**
     * retourne le libellé de l'entité d'achat.
     */
    public function getEntityLibelle($idService, $organisme, $langue = null)
    {
        if ($idService) {
            $entityId = $idService;
            if (0 == $entityId) {
                return '';
            }

            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
            $entity = CommonServicePeer::retrieveByPk($entityId, $connexion);
            if ($entity) {
                if (!$langue) {
                    return $entity->getLibelle();
                } else {
                    $getLibelleTraduit = 'getLibelle'.Atexo_Languages::getLanguageAbbreviation($langue);
                    if (0 == strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE'))
                        || !$entity->$getLibelleTraduit()) {
                        return $entity->getLibelle();
                    } else {
                        return $entity->$getLibelleTraduit();
                    }
                }
            }
        }

        return '';
    }

    /**
     *  retourne le chemin de l'entité publique.
     */
    public function getEntityPathById($idEntity, $organisme, $withLibelle = false, $global = false)
    {
        $entityPath = null;
        $orgSigle = null;
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $organismeObj = Atexo_Organismes::retrieveOrganismeByAcronyme($organisme);
        if (0 == $idEntity) {
            if ($organismeObj) {
                return $organismeObj->getSigle().' - '.$organismeObj->getDenominationOrg();
            }
        }
        $entity = CommonServicePeer::retrieveByPk($idEntity, $connexion);
        if ($entity instanceof CommonService) {
            $parents = self::getAllParents($entity->getId(), $organisme);
            if ($parents) {
                $entityPath = Atexo_Util::implodeArrayWithSeparator($entity->getSigle(), $parents, ' / ');
            }
            if ($entityPath) {
                if (true == $global) {
                    $orgSigle = $organismeObj->getSigle();
                }
                if ($withLibelle) {
                    return $orgSigle.' / '.$entityPath.' - '.$entity->getLibelleTraduit();
                } else {
                    return $orgSigle.' / '.$entityPath;
                }
            }
        }

        return '-';
    }

    /**
     * retourne la liste des entités d'achat.
     *
     * @param string $organisme
     *
     * @return array
     */
    public static function retrieveEntityPurchaseByOrganisme($organisme, $withPath = null, $langue = null)
    {
        return self::getEntityPurchase($organisme, $withPath, $langue);
    }

    public static function getPathEntityById($id, $organisme, $langue = null)
    {
        if (!empty($id)) {
            $entity = self::retrieveEntityById($id, $organisme, true);
            if ($entity) {
                return $entity->getPathServiceTraduit();
            }
        } else {
            $organismeO = Atexo_Organismes::retrieveOrganismeByAcronyme($organisme);
            if ($organismeO instanceof CommonOrganisme) {
                $getDenominationOrg = 'getDenominationOrg'.Atexo_Languages::getLanguageAbbreviation($langue);
                if (0 == strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE'))
                    || !$organismeO->$getDenominationOrg()) {
                    return $organismeO->getSigle().' - '.$organismeO->getDenominationOrg();
                } else {
                    return $organismeO->getSigle().' - '.$organismeO->$getDenominationOrg();
                }
            } else {
                return '-';
            }
        }
    }

    public static function getSigleLibelleEntityById($id, $organisme)
    {
        if (0 != $id) {
            $entity = self::retrieveEntityById($id, $organisme);
            if ($entity) {
                return $entity->getSigle().' - '.$entity->getLibelle();
            }
        } else {
            $organismeO = Atexo_Organismes::retrieveOrganismeByAcronyme($organisme);
            if ($organismeO) {
                return $organismeO->getSigle().' - '.$organismeO->getDenominationOrg();
            }
        }

        return '';
    }

    public static function getEntitePublicEntityAchatById($id, $organisme = null)
    {
        if ($organisme) {
            $organismeAcronyme = $organisme;
        } else {
            $organismeAcronyme = Atexo_CurrentUser::getOrganismAcronym();
        }
        $organismeO = Atexo_Organismes::retrieveOrganismeByAcronyme($organismeAcronyme);

        if (0 != $id) {
            $entity = self::retrieveEntityById($id, $organismeAcronyme);
            if ($entity) {
                return $organismeO->getSigle().' - '.$organismeO->getDenominationOrg().'<br> '.$entity->getSigle().' - '.$entity->getLibelle();
            }
        } else {
            return $organismeO->getSigle().' - '.$organismeO->getDenominationOrg();
        }
    }

    /**
     * retourne tout les sous services d'un autre.
     *
     * @param int $idServiceAgent l'id de l'agent
     *
     * @return la liste des sous services avec leurs chemins
     */
    public static function getListSubServices($idServiceAgent, $org)
    {
        $serviceAgent = self::retrieveEntityById($idServiceAgent, $org);
        if ($serviceAgent) {
            $result = [];
            self::getAllChilds($serviceAgent->getId(), $result, $org);

            return self::getAllChildsWithPaths($result, $org);
        } else {
            return false;
        }
    }

    /**
     * retourne un tableau de fils.
     *
     * @param array $listChilds
     * @param $org
     */
    public static function getAllChildsWithPaths($listChilds, $org)
    {
        $childArrayWithPath = [];
        if (is_array($listChilds)) {
            foreach ($listChilds as $child) {
                $childArrayWithPath['id'] = $child->getId();
                $childArrayWithPath['libelle'] = self::getServiceById($child->getId(), $org);
                $parents = self::getAllParents($child->getId(), $org);
                $childArrayWithPath['path'][$child->getId()] = Atexo_Util::implodeArrayWithSeparator($childArrayWithPath['libelle'], $parents, ' / ');
            }
        }

        return $childArrayWithPath['path'];
    }

    public function getAllServices($organisme)
    {
        return self::getCachedEntities($organisme);
    }

    /**
     * retourne la denimination de l'organisme sinon false.
     *
     * @param $idEntity l'id du service
     * @param $organisme l'acronyme de l'organisme
     *
     * @return Service : $entity
     */
    public function getEntityById($idEntity, $organisme)
    {
        if ($idEntity) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
            $c = new Criteria();
            $c->add(CommonServicePeer::ID, $idEntity, Criteria::EQUAL);
            $c->add(CommonServicePeer::ORGANISME, $organisme, Criteria::EQUAL);
            $entity = CommonServicePeer::doSelectOne($c, $connexion);

            return $entity;
        } else {
            return Atexo_Organismes::retrieveOrganismeByAcronyme($organisme);
        }
    }

    public function deleteEntity($idEntity, $organisme)
    {
        $entity = $this->getEntityById($idEntity, $organisme);
        if ($entity && $entity instanceof CommonService) {
            $consultations = (new Atexo_Consultation())->retrieveConsultationsByService($idEntity, $organisme);
            $agents = Atexo_Agent::retriveServiceAgents($organisme, $idEntity);
            $childs = self::getSubServices($idEntity, $organisme);
            $rpas = Atexo_EntityPurchase_RPA::getRpaByIdService($idEntity, $organisme);
            if (($consultations && (is_countable($consultations) ? count($consultations) : 0)) || ($agents && (is_countable($agents) ? count($agents) : 0)) || ($childs && (is_countable($childs) ? count($childs) : 0)) || ($rpas && count($rpas))) {
                return false;
            } else {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
                $entity->delete($connexion);

                return true;
            }
        } else {
            return false;
        }
    }

    public function hasChild($idEntity, $organisme)
    {
        return (self::getLevelOneChilds($idEntity, $organisme)) ? true : false;
    }

    /**
     * Mettre les infos récupérer depuis le web service dans les champs de la table service du projet associé.
     *
     * @param string $serv, array $wsService
     */
    public function remplirService($serv, $wsService)
    {
        $serv->setIdExterne($wsService->id);
        $serv->setTypeService($wsService->type_service);
        $serv->setLibelle(utf8_decode($wsService->service));
        $serv->setSigle(utf8_decode($wsService->sigle));
        $serv->setPays('France');
    }

    /**
     * Récupération des service par id_intial.
     */
    public function retrieveEntityByIdInitial($idExterne, $org, $connexion)
    {
        $cr = new Criteria();
        $cr->add(CommonServicePeer::ID_EXTERNE, $idExterne);
        $cr->add(CommonServicePeer::ORGANISME, $org);
        $service = CommonServicePeer::doSelectOne($cr, $connexion);

        return $service;
    }

    /**
     * Retourne la liste des entités d'achat avec uniquement les identifiants.
     *
     * @param $organisme organisme sur lequel porte la recherche
     *
     * @return array
     */
    public static function getEntityPurchaseById($organisme)
    {
        $entityPurchaseArray = [];
        $entityPurchase = self::getCachedEntities($organisme);
        if (is_array($entityPurchase)) {
            foreach ($entityPurchase as $entity) {
                $entityPurchaseArray[] = $entity->getId();
            }

            return $entityPurchaseArray;
        } else {
            return '';
        }
    }

    public static function getAllServicesAndPoles($org, $dateModif = null, $objOrganisme = null)
    {

        $sql = "select s.id as alias_service_id, s.organisme as alias_organisme, s.*, afs.* from Service s left join AffiliationService afs on s.id=afs.service_id AND s.organisme=afs.organisme WHERE s.organisme='".$org."' ";
        if ($dateModif) {
            $sql .= " AND s.date_modification >='".(new Atexo_Db())->quote($dateModif)."'";
        }
        $statement = Atexo_Db::getLinkCommon()->query($sql);
        $arrayServices = [];
        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            $service = [];
            $service['id'] = $row['alias_service_id'];
            $service['parentId'] = $row['service_parent_id'];
            $service['title'] = $row['libelle'];
            $service['acronym'] = $row['sigle'];
            $service['address'] = $row['adresse'].' '.$row['adresse_suite'];
            $service['postalCode'] = $row['cp'];
            $service['city'] = $row['ville'];
            $service['phone'] = $row['telephone'];
            $service['fax'] = $row['fax'];
            $service['country'] = $row['pays'];
            $service['email'] = $row['mail'];
            $service['id_initial'] = $row['id_initial']; // pour le socle PPP
            $service['acronymOrganism'] = $row['alias_organisme']; // pour le socle PPP
            $service['siren'] = $row['siren']; // pour le socle PPP

            if ($objOrganisme) {
                $service['id_initial_organism'] = $objOrganisme->getIdInitial(); // pour le socle PPP
                $service['id_organism'] = $objOrganisme->getId(); // pour le socle MPE
                $service['denominationOrganism'] = $objOrganisme->getDenominationOrg(); // pour le socle MPE
                $service['sigleOrganism'] = $objOrganisme->getSigle(); // pour le socle MPE
            }
            $arrayServices[$row['alias_organisme'].'#'.$row['alias_service_id']] = $service;
        }
        Atexo_Db::closeCommon();

        return $arrayServices;
    }

    /***
     * retourne le nombre des services de la plate forme
     */
    public function getCountAllServicesPortail()
    {
        $nbr = 0;
        $allOrganisme = Atexo_Organismes::retrieveOrganismes();
        foreach ($allOrganisme as $OneOrganisme) {
            $nbr = $nbr + self::getCountService($OneOrganisme->getAcronyme());
        }

        return $nbr;
    }

    /****
     * récupérer le path d'un service donné
     */
    public static function getPathServiceById($id, $organisme, $langue = null, $trimSigle = false)
    {
        $organismeO = Atexo_Organismes::retrieveOrganismeByAcronyme($organisme);
        $getLibelle = 'getLibelle'.Atexo_Languages::getLanguageAbbreviation($langue);
        if (0 != $id) {
            $entity = self::retrieveEntityById($id, $organisme);
            if ($entity) {
                $parents = self::getAllParents($entity->getId(), $organisme);
                if ($parents) {
                    $sigleEntite = $entity->getSigle();
                    $sigleOrganisme = $organismeO->getSigle();

                    if ($trimSigle) {
                        $sigleEntite = str_replace(' - ', '-', $entity->getSigle());
                        $sigleOrganisme = str_replace(' - ', '-', $organismeO->getSigle());
                    }

                    $entityPath = Atexo_Util::implodeArrayWithSeparator($sigleEntite, $parents, ' / ');
                    if ($langue) {
                        return $sigleOrganisme.' / '.$entityPath.' - '.$entity->$getLibelle();
                    } else {
                        return $sigleOrganisme.' / '.$entityPath.' - '.$entity->getLibelle();
                    }
                } else {
                    $sigleEntite = $entity->getSigle();
                    $sigleOrganisme = $organismeO->getSigle();

                    if ($trimSigle) {
                        $sigleEntite = str_replace(' - ', '-', $entity->getSigle());
                        $sigleOrganisme = str_replace(' - ', '-', $organismeO->getSigle());
                    }
                    if ($langue) {
                        return $sigleOrganisme.' / '.$sigleEntite.' - '.$entity->$getLibelle();
                    } else {
                        return $sigleOrganisme.' / '.$sigleEntite.' - '.$entity->getLibelle();
                    }
                }
            }
        } else {
            $sigleOrganisme = $organismeO->getSigle();

            if ($trimSigle) {
                $sigleOrganisme = str_replace(' - ', '-', $organismeO->getSigle());
            }
            if ($langue) {
                return $sigleOrganisme.' - '.$organismeO->getDenominationOrg();
            } else {
                $getDenominationOrg = 'getDenominationOrg'.Atexo_Languages::getLanguageAbbreviation($langue);

                return $sigleOrganisme.' - '.$organismeO->$getDenominationOrg();
            }
        }
    }

    /****
     * calcule le nombre des service pour chauque organisme donné
     * @retuen : le nombre de service(s)
     */
    public function getCountService($organisme = null)
    {
        $countService = null;
        $sql = "Select count(*) as nombreService FROM Service where Service.organisme='".$organisme."'";
        $statement = Atexo_Db::getLinkCommon(true)->query($sql);
        foreach ($statement as $row) {
            $countService = $row['nombreService'];
        }
        Atexo_Db::closeCommon();

        return $countService;
    }

    public function getAllServicesPlateForme($fromDate = null)
    {
        $arrayGlobal = [];
        $allOrganisme = (new Atexo_Organismes())->retrieveAllEntity();
        foreach ($allOrganisme as $OneOrganisme) {
            $services = null;
            $services = self::getAllServicesAndPoles($OneOrganisme->getAcronyme(), $fromDate, $OneOrganisme);
            if (is_array($services)) {
                $arrayGlobal = array_merge($arrayGlobal, $services);
            }
        }
        return $arrayGlobal;
    }

    public function isEntityAbleToValidate($idServiceValidateur, $idService, $organisme)
    {
        if ($idServiceValidateur == $idService) {
            return true;
        }

        if (Atexo_Module::isEnabled('OrganisationCentralisee', $organisme)) {
            $listeEntitesParents = self::getAllParents($idServiceValidateur, $organisme);
            if (is_array($listeEntitesParents)) {
                $id = '';
                foreach ($listeEntitesParents as $id => $unParent) {
                    if ($unParent['id'] == $idService) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    public function retrieveServiceByCodeAcheteur($siren, $organisme = null)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $cr = new Criteria();
        $cr->add(CommonServicePeer::SIREN, $siren, Criteria::EQUAL);
        if ($organisme) {
            $cr->add(CommonServicePeer::ORGANISME, $organisme, Criteria::EQUAL);
        }
        $service = CommonServicePeer::doSelect($cr, $connexion);

        return $service;
    }

    public function getPathByEntityAndOrganismeObject($entity, $OrganismeObjet)
    {
        $langue = Atexo_CurrentUser::readFromSession('lang');
        $getLibelle = 'getLibelle'.Atexo_Languages::getLanguageAbbreviation($langue);
        if ($entity instanceof Service) {
            $parents = self::getAllParents($entity->getId(), $OrganismeObjet->getAcronyme());
            if ($parents) {
                $entityPath = Atexo_Util::implodeArrayWithSeparator($entity->getSigle(), $parents, ' / ');
                if ($langue) {
                    return $OrganismeObjet->getSigle().' / '.$entityPath.' - '.$entity->$getLibelle();
                } else {
                    return $OrganismeObjet->getSigle().' / '.$entityPath.' - '.$entity->getLibelle();
                }
            } else {
                if ($langue) {
                    return $OrganismeObjet->getSigle().' / '.$entity->getSigle().' - '.$entity->$getLibelle();
                } else {
                    return $OrganismeObjet->getSigle().' / '.$entity->getSigle().' - '.$entity->getLibelle();
                }
            }
        } else {
            if ($langue) {
                return $OrganismeObjet->getSigle().' - '.$OrganismeObjet->getDenominationOrg();
            } else {
                $getDenominationOrg = 'getDenominationOrg'.Atexo_Languages::getLanguageAbbreviation($langue);

                return $OrganismeObjet->getSigle().' - '.$OrganismeObjet->$getDenominationOrg();
            }
        }
    }

    /**
     * Fonction de récupération du sigle d'une entité d'achat.
     *
     * @param $id de l'entité d'achat
     * @param $organisme de l'entité d'achat
     * return $sigle
     */
    public function getSigleByEntityId($id, $organisme)
    {
        $sigle = '';
        if ('0' == $id || null === $id) {
            $sigle = $organisme;
        } else {
            $sigle = self::retrieveEntityById($id, $organisme)->getSigle();
        }

        return $sigle;
    }

    /**
     * Permet de determiner l'état de l'activation du fuseau horaire d'un service.
     *
     * @param $id: l'identifiant du service
     * @param $organisme: l'organisme du service
     *
     * @return : true si le champ 'activation_fuseau_horaire' de la table 'service' est é '1' pour ce service (passé en parametre), false sinon
     */
    public function getEtatActivationFuseauHoraire($idService, $organisme)
    {
        //Cas de l'organisme
        if (0 == $idService) {
            $objetOrganisme = Atexo_Organismes::retrieveOrganismeByAcronyme($organisme);
            if ($objetOrganisme instanceof CommonOrganisme && '1' == $objetOrganisme->getActivationFuseauHoraire()) {
                return true;
            }
        } else {
            $service = $this->getEntityById($idService, $organisme);
            if ($service instanceof CommonService && '1' == $service->getActivationFuseauHoraire()) {
                return true;
            }
        }

        return false;
    }

    public static function getSubServicesAndMyService($id_service, $org)
    {
        $listeChilds = [];
        if ($id_service) {
            $listeChilds = self::getSubServices($id_service, $org, false, true);
            $serviceAgent = self::retrieveEntityById($id_service, $org);
            if ($serviceAgent) {
                $path = self::getPathServiceById($serviceAgent->getId(), $org);
                if (is_array($listeChilds) && count($listeChilds)) {
                    $arrayEntity = [];
                    $arrayEntity[$serviceAgent->getId()] = $path;
                    foreach ($listeChilds as $key => $value) {
                        $arrayEntity[$key] = $value;
                    }
                    $listeChilds = $arrayEntity;
                } else {
                    $listeChilds = [$serviceAgent->getId() => $path];
                }
            }
        } else {
            // Si l'agent n'est pas affecté é un service on affiche tout les services
            $listeChilds = self::getEntityPurchase($org, true);
        }

        return $listeChilds;
    }

    /*
     * Permet de retourner le libelle de service by siret( siren+complement)
     */
    public function getLibelleServiceBySiret($siret, $org)
    {
        $sql = "select * from Service WHERE organisme='".$org."' AND CONCAT(siren, '', complement)='".$siret."'  ";
        $statement = Atexo_Db::getLinkCommon()->query($sql);
        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            return $row['libelle'];
        }

        return '';
    }

    /*
  * Permet de retourner array des ids des sous services avec leur service pere
 */
    public function getArrayIdServiceWithChild($idService, $organisme)
    {
        $arrayIds = [];
        $sericeChild = self::retrieveAllChildrenServices($idService, $organisme);
        if ($sericeChild) {
            $sericeChild = str_replace('(', '', $sericeChild);
            $sericeChild = str_replace(')', '', $sericeChild);
            $arrayIds = explode(',', $sericeChild);

            return $arrayIds;
        } else {
            $arrayIds[] = $idService;
        }

        return $arrayIds;
    }

    /**
     * Permet de retourner le service by id et organisme.
     *
     * @param int    $idService
     * @param string $acronyme  l'acronyme de l'organisme
     *
     * @return CommonService
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-megalis
     *
     * @copyright Atexo 2016
     */
    public function retrieveServiceBYId($idService, $acronyme)
    {
        $serviceQuery = new CommonServiceQuery();

        return $serviceQuery->getServiceById($idService, $acronyme);
    }

    public static function getAllEntityPurchase($organisme, $withPath = null, $listeIdsToIgnore = [], $langue = null)
    {
        $entityPurchaseArray = [];
        $entityPurchase = self::getCachedEntities($organisme);
        $organismeO = Atexo_Organismes::retrieveOrganismeByAcronyme($organisme);

        if (is_array($entityPurchase)) {
            foreach ($entityPurchase as $entity) {
                if (!in_array($entity->getId(), $listeIdsToIgnore)) {
                    $idEntity = $entity->getId();
                    $entityPurchaseArray[$idEntity]['id'] = $entity->getId();
                    if (true == $withPath) {
                        $entityPurchaseArray[$idEntity]['libelle'] = $entity->getPathServiceTraduit();
                    } else {
                        $entityPurchaseArray[$idEntity]['libelle'] = $entity->getSigle();
                    }
                }
            }
        }

        return $entityPurchaseArray;
    }

    public function retrievefusionServiceNotMergedByIdService($idServiceSource, $idServiceCible, $organisme, $connexion = null)
    {
        $fusionServiceQuery = new CommonTFusionnerServicesQuery();

        return $fusionServiceQuery->getFusionServiceByIdServiceSource($idServiceSource, $idServiceCible, $organisme, '0', $connexion);
    }

    public function retrievefusionServiceNotMerged($connexion = null)
    {
        $fusionServiceQuery = new CommonTFusionnerServicesQuery();

        return $fusionServiceQuery->getFusionServiceNotMerged($connexion);
    }

    public function retrieveServiceBySiren($siren, $siret, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $serviceQuery = new CommonServiceQuery();

        return $serviceQuery->getServiceBySiren($siren, $siret, $organisme, $connexion);
    }

    /**
     * Permet de recuperer une entreprise selon le siren.
     *
     * @param int       $siren             le siren d'enetreprise
     * @param bool      $withEtablissement si on veut entreprise avec etablissement
     * @param connexion $connexion         l'objet de la connexion
     * @param int       $idObjet           l'id de l'objet pour le stoquer dans l'historique
     *
     * @return mix Atexo_Entreprise_EntrepriseVo l'entreprise s'il existe sinon boolean false
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function recupererServiceApiGouvEntrepriseBySiren($siren, $connexion = null, $idObjet = null)
    {
        $token = Atexo_Config::getParameter('API_ENTREPRISE_TOKEN');

        $paramsWs = [
            'token' => $token,
            'apiEntrepriseUrl' => Atexo_Config::getParameter('URL_API_ENTREPRISE_UNITE_LEGALE'),
            'apiEtablissementUrl' => Atexo_Config::getParameter('URL_API_ENTREPRISE_ETABLISSEMENT'),
        ];
        $loggerWs = Atexo_LoggerManager::getLogger();
        $interfaceWS = new InterfaceSynchroApiGouvEntreprise($loggerWs, $paramsWs);
        $resultat = $interfaceWS->getEntrepriseBySiren($siren);
        if (is_array($resultat) && InterfaceSynchroApiGouvEntreprise::isResponseWsApiGouvEntrepriseSucces($resultat) && $resultat[1]) {
            $jeton = $resultat[2];
            $loggerWs->info("Debut de l'enregistrement de l'historique de la synchronisation du service");
            (new Atexo_Entreprise_HistoriqueSynchroSGMAP())->enregistrerHistroique('SERVICE', $siren, $jeton, $connexion, $idObjet);
            $loggerWs->info("Fin de l'enregistrement de l'historique de la synchronisation du service");
            $service = $resultat[1];

            return $service;
        }

        return false;
    }

    /**
     * Permet de recuperer une entreprise selon le siren.
     *
     * @param int       $siren             le siren d'enetreprise
     * @param bool      $withEtablissement si on veut entreprise avec etablissement
     * @param connexion $connexion         l'objet de la connexion
     * @param int       $idObjet           l'id de l'objet pour le stoquer dans l'historique
     *
     * @return mix Atexo_Entreprise_EntrepriseVo l'entreprise s'il existe sinon boolean false
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function recupererServiceApiGouvEntreprise($siren, &$message, $connexion = null, $idObjet = null)
    {
        $token = Atexo_Config::getParameter('API_ENTREPRISE_TOKEN');
        $paramsWs = [
                'token' => $token,
                'apiEntrepriseUrl' => Atexo_Config::getParameter('URL_API_ENTREPRISE_UNITE_LEGALE'),
                'apiEtablissementUrl' => Atexo_Config::getParameter('URL_API_ENTREPRISE_ETABLISSEMENT'),
        ];
        $loggerWs = Atexo_LoggerManager::getLogger();
        $interfaceWS = new InterfaceSynchroApiGouvEntreprise($loggerWs, $paramsWs);
        $resultat = $interfaceWS->getEntrepriseBySiren($siren);
        if (is_array($resultat)) {
            if (InterfaceSynchroApiGouvEntreprise::serviceIsIndisponible($resultat)) {
                $message = Prado::localize('AVERTISSEMENT_DISPONIBLITE_IDENTIFICATION_AUTOMATIQUE_ENTREPRISE');
            } elseif (InterfaceSynchroApiGouvEntreprise::isResponseWsApiGouvEntrepriseSucces($resultat) && $resultat[1]) {
                $jeton = $resultat[2];
                $loggerWs->info("Debut de l'enregistrement de l'historique de la synchronisation de l'entreprise");
                (new Atexo_Entreprise_HistoriqueSynchroSGMAP())->enregistrerHistroique('SERVICE', $siren, $jeton, $connexion, $idObjet);
                $loggerWs->info("Fin de l'enregistrement de l'historique de la synchronisation de l'entreprise");
                $entrepriseVo = $resultat[1];
                mb_convert_variables('utf-8', 'ISO-8859-1', $entrepriseVo);

                return $entrepriseVo;
            }
        }

        return false;
    }

    /**
     * Permet de recuperer une entreprise selon le siren.
     *
     * @param int       $siren             le siren d'enetreprise
     * @param bool      $withEtablissement si on veut entreprise avec etablissement
     * @param connexion $connexion         l'objet de la connexion
     * @param int       $idObjet           l'id de l'objet pour le stoquer dans l'historique
     *
     * @return mix Atexo_Entreprise_EntrepriseVo l'entreprise s'il existe sinon boolean false
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function recupererServiceApiGouvEntrepriseBySiret($siret, &$message, $connexion = null, $idObjet = null)
    {
        $token = Atexo_Config::getParameter('API_ENTREPRISE_TOKEN');

        $paramsWs = [
            'token' => $token,
            'apiEtablissementUrl' => Atexo_Config::getParameter('URL_API_ENTREPRISE_ETABLISSEMENT'),
        ];
        $loggerWs = Atexo_LoggerManager::getLogger();
        $interfaceWS = new InterfaceSynchroApiGouvEntreprise($loggerWs, $paramsWs);
        $resultat = $interfaceWS->getEtablissementBySiret($siret);

        if (is_array($resultat)) {
            if (InterfaceSynchroApiGouvEntreprise::serviceIsIndisponible($resultat)) {
                $message = Prado::localize('AVERTISSEMENT_DISPONIBLITE_IDENTIFICATION_AUTOMATIQUE_ENTREPRISE');
            } elseif (InterfaceSynchroApiGouvEntreprise::isResponseWsApiGouvEntrepriseSucces($resultat) && $resultat[1]) {
                $jeton = $resultat[2];
                $loggerWs->info("Debut de l'enregistrement de l'historique de la synchronisation du service");
                (new Atexo_Entreprise_HistoriqueSynchroSGMAP())->enregistrerHistroique('SERVICE', $siret, $jeton, $connexion, $idObjet);
                $loggerWs->info("Fin de l'enregistrement de l'historique de la synchronisation du service");
                $etablissements = $resultat[1];
                mb_convert_variables('utf-8', 'ISO-8859-1', $etablissements);
                if (is_array($etablissements)) {
                    return $etablissements[$siret];
                }
            }
        }

        return false;
    }
}
