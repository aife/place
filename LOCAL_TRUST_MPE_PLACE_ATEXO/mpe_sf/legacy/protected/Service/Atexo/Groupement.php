<?php

namespace Application\Service\Atexo;

use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonTGroupementEntreprise;
use Application\Propel\Mpe\CommonTGroupementEntrepriseQuery;
use Application\Propel\Mpe\CommonTMembreGroupementEntrepriseQuery;

class Atexo_Groupement
{
    /**
     * Permet de verifier si un membre existe deja.
     *
     * @param $membres
     * @param $idEntreprise
     *
     * @return bool
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function isMembreGroupementExiste($membres, $idEntreprise)
    {
        $membreExiste = false;
        if (is_array($membres)) {
            if (array_key_exists($idEntreprise, $membres)) {
                $membreExiste = true;
            } else {
                foreach ($membres as $membre) {
                    $sousMembres = $membre->getSousMembres();
                    if (is_array($sousMembres) && array_key_exists($idEntreprise, $sousMembres)) {
                        $membreExiste = true;
                    }
                }
            }
        }

        return $membreExiste;
    }

    /**
     * Permet de retourner le groupement by id conadidature mps.
     *
     * @param $idCandidature
     * @param $connexion
     *
     * @return CommonTGroupementEntreprise
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function getGroupementByIdOffre($idOffre, $returnObject = true, $connexion = null)
    {
        $groupementQuery = new CommonTGroupementEntrepriseQuery();
        $groupemnt = $groupementQuery->getGroupementByIdOffre($idOffre, $connexion);
        if ($groupemnt instanceof CommonTGroupementEntreprise) {
            return $returnObject ? $groupemnt : true;
        }

        return false;
    }

    /**
     * Permet de retourner le groupement by id conadidature mps.
     *
     * @param $idCandidature
     * @param $connexion
     *
     * @return CommonTGroupementEntreprise
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function getSousGroupementByIdParent($idParent, $connexion = null)
    {
        $groupementQuery = new CommonTMembreGroupementEntrepriseQuery();

        return $groupementQuery->getSousGroupementByIdParent($idParent, $connexion);
    }

    /**
     * Permet de retourner le groupement by id conadidature mps.
     *
     * @param int $idGroupement
     * @param $connexion
     *
     * @return array
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-developp
     *
     * @copyright Atexo 2016
     */
    public function getMembresGroupement($idGroupement, $connexion = null)
    {
        $groupementQuery = new CommonTMembreGroupementEntrepriseQuery();

        return $groupementQuery->filterByIdGroupementEntreprise($idGroupement)->filterByIdMembreParent(null, Criteria::ISNULL)->find($connexion);
    }

    /**
     * Permet de retourner les memebre d'un groupement d'une offre le groupement entreprise.
     *
     * @param $type
     * @param $idOffre
     * @param null      $connexion
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     * @copyright Atexo 2016
     */
    public function retrieveMemebreGroupement($idOffre, $idRole = null, bool $justNombre = true, $connexion = null)
    {
        $groupemntQuery = new CommonTGroupementEntrepriseQuery();

        return $groupemntQuery->getMemebreGroupement($idOffre, $idRole, $justNombre, $connexion);
    }

    /**
     * Permet de retourner les memebre d'un groupement d'une offre le groupement entreprise.
     *
     * @param $type
     * @param $idOffre
     * @param bool $justNombre
     * @param null      $connexion
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function getNombreSousMemebreGroupement($idOffre, $connexion = null)
    {
        $memebreQuery = new CommonTMembreGroupementEntrepriseQuery();

        return $memebreQuery->getNombreSousMemebreGroupement($idOffre, $connexion);
    }

    /**
     * Permet de retourner les memebre d'un groupement d'une offre le groupement entreprise.
     *
     * @param $type
     * @param $idOffre
     * @param bool $justNombre
     * @param null      $connexion
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function retrieveMemebreGroupementSansMondataire($idOffre, $idRoleMondataire, $connexion = null)
    {
        $membreGroupemntQuery = new CommonTMembreGroupementEntrepriseQuery();

        return $membreGroupemntQuery->getMemebreGroupementSansMondataire($idOffre, $idRoleMondataire, $connexion);
    }
}
