<?php

namespace Application\Service\Atexo;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonTIllustrationFondPeer;

/**
 * commentaires.
 *
 * @author loubna EZZIANI <loubna.ezziani@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 1.0
 *
 * @since MPE-4.0
 */
class Atexo_IllustrationFond
{
    /*
     * retourne tout les illustrations de fond
     * @param $actif : illustration actif (optionnel)
     * @param $oneObject : pour selectionner un seul objet
     */
    public function getIllustrationFondByAgent($idAgent, $actif = null, $oneObject = false)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonTIllustrationFondPeer::ID_AGENT, $idAgent, Criteria::EQUAL);
        if (null !== $actif) {
            $c->add(CommonTIllustrationFondPeer::ACTIF, $actif, Criteria::EQUAL);
        }
        if ($oneObject) {
            $illustrationObject = CommonTIllustrationFondPeer::doSelectOne($c, $connexionCom);
        } else {
            $illustrationObject = CommonTIllustrationFondPeer::doSelect($c, $connexionCom);
        }

        return $illustrationObject;
    }

    /*
     * désactiver l'illustration active d'un agent
     */
    public function desactiverAgentIllustrationFond($idAgent, $connexion)
    {
        $illustrationsObject = self::getIllustrationFondByAgent($idAgent, Atexo_Config::getParameter('ILLUSTRATION_DE_FOND_ACTIVER'));
        if (is_array($illustrationsObject)) {
            foreach ($illustrationsObject as $illustrationObject) {
                $illustrationObject->setActif(Atexo_Config::getParameter('ILLUSTRATION_DE_FOND_DESACTIVER'));
                $illustrationObject->save($connexion);
            }
        }
    }
}
