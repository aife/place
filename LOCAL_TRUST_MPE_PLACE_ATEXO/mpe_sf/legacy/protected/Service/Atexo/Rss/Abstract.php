<?php

namespace Application\Service\Atexo\Rss;

use Application\Service\Atexo\Atexo_Util;

/**
 * Classe abstraite de base pour le Flux Rss.
 * Fournit les getter/setter commun au <channel> et aux <item>.
 *
 * @copyright Atexo 2009
 *
 * @since 3.0.1
 *
 * @category Atexo
 */
abstract class Atexo_Rss_Abstract
{
    protected $_description = '';
    protected $_title = '';
    protected string $_link = '';
    protected string $_pubDate = '';
    protected string|array $_categories = [];

    /**
     * get categories of the feed / item.
     *
     * @return string Catégorie du flux
     */
    public function getCategories()
    {
        return $this->_categories;
    }

    /**
     * get the description element of the feed / item.
     *
     * @return string the description element of the feed / item
     */
    public function getDescription()
    {
        return $this->_description;
    }

    /**
     * get the link element of the feed / item.
     *
     * @return string the link element of the feed / item
     */
    public function getLink()
    {
        return $this->_link;
    }

    /**
     * get the pubDate element of the feed / item.
     *
     * @return string the pubDate element of the feed / item
     */
    public function getPubDate()
    {
        return $this->_pubDate;
    }

    /**
     * get the title element of the feed / item.
     *
     * @return string the title element of the feed / item
     */
    public function getTitle()
    {
        return $this->_title;
    }

    /**
     * set a new category element for the feed / item.
     *
     * @param string Domain of the category
     * @param string Content of the category
     */
    public function setCategory($domain, $content)
    {
        $this->_categories[] = ['domain' => $domain, 'content' => $content];
    }

    /**
     * set the description element of the feed / item.
     *
     * @param string Description of the element
     */
    public function setDescription($description)
    {
        $this->_description = $description;
    }

    /**
     * set the link element of the feed / item.
     *
     * @param string link element of the feed / item
     */
    public function setLink($link)
    {
        $this->_link = Atexo_Util::checkUrl($link);
    }

    /**
     * set the pubDate element of the feed / item.
     *
     * @param string pubDate element of the feed / item
     */
    public function setPubDate($pubDate)
    {
        $this->_pubDate = Atexo_Util::getDateRFC2822($pubDate);
    }

    /**
     * set the title element of the feed / item.
     *
     * @param string title element of the feed / item
     */
    public function setTitle($title)
    {
        $this->_title = $title;
    }
}
