<?php

namespace Application\Service\Atexo;

/**
 * Classe de manipulation des entreprises de la base qualifiée.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_BaseQualifee
{
    /**
     * retourne un objet entreprise.
     */
    public function retrieveCompanyQualifeeBySiren($siret)
    {
        return true;
    }
}
