<?php

namespace Application\Service\Atexo\Message;

use Application\Propel\Mpe\CommonAdministrateur;
use Application\Propel\Mpe\CommonAgent;
use Application\Propel\Mpe\CommonInscrit;
use Application\Propel\Mpe\Entreprise;
use Application\Service\Atexo\Atexo_Admin;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Entreprise;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Inscrit;
use Prado\Prado;

/**
 * classe UserCompte.
 *
 * @author BOUKATAYA Fatima Zahra <fatima.boukataya@atexo.com>
 * @copyright Atexo 2010
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Message_UserCompte
{
    /**
     * @param $corps
     * @param $user
     *
     * @return html file
     */
    protected static function getTexteTemplate($corps, $user)
    {
        $corps = str_replace('IMAGE_BANDEAU', Atexo_Message::getImageBandeau(), $corps);

        $corps = str_replace('DEFINE_TEXT_BONJOUR', Prado::localize('DEFINE_TEXT_BONJOUR'), $corps);

        $corps = str_replace('DEFINE_TEXT_INTRODUCTION_MESSAGE', Prado::localize('LE_COMPTE').' '.$user->getType().' '.Prado::localize('CORPS_MESSAGE_DESACTIVATION'), $corps);

        $corps = str_replace('DEFINE_PRENOM', Prado::localize('DEFINE_PRENOM'), $corps);

        $corps = str_replace('DEFINE_NOM_MAJ', Prado::localize('DEFINE_NOM_MAJ'), $corps);

        $corps = str_replace('TEXT_ADRESSE_ELECTRONIQUE', Prado::localize('TEXT_ADRESSE_ELECTRONIQUE'), $corps);

        $corps = str_replace('DEFINE_TEXT_IDENTIFIANT', Prado::localize('DEFINE_TEXT_IDENTIFIANT'), $corps);

        $corps = str_replace('DEFINE_TEXT_FIN_MESSAGE', Prado::localize('PIED_MESSAGE_DESACTIVATION'), $corps);

        $corps = str_replace('CORDIALEMENT', Prado::localize('CORDIALEMENT'), $corps);

        $corps = str_replace('PLACE_MARCHE_PUBLIC_INTERMINISTERE', Prado::localize('PLACE_MARCHE_PUBLIC_INTERMINISTERE'), $corps);

        //$corps =  str_replace("COULEUR_TITRE", "#fcfcfc",$corps);

        return $corps;
    }

    public static function getCorpsMessageAlerteDesactivationCompte($userVo)
    {
        $userO = $entreprise = null;

        // Get template html file.
        $corps = file_get_contents(Atexo_Config::getParameter('PATH_FILE_MAIL_DESACTIVATION_COMPTE'));
        $corps = str_replace('IMAGE_BANDEAU', Atexo_Message::getImageBandeau(), $corps);
        // retrieve $userO from database.
        if ('entreprise' == strtolower($userVo->getType())) {
            $userO = (new Atexo_Entreprise_Inscrit())->retrieveInscritByLogin($userVo->getLogin());
            $entreprise = (new Atexo_Entreprise())->retrieveCompanyBdeById($userO->getEntrepriseId());
        } elseif ('admin' == strtolower($userVo->getType())) {
            $userO = (new Atexo_Admin())->retrieveAdminByLogin($userVo->getLogin());
        } else {
            $userO = (new Atexo_Agent())->retrieveAgentByLogin($userVo->getLogin());
        }

        if ($userO instanceof Entreprise || $userO instanceof CommonAdministrateur || $userO instanceof CommonAgent || $userO instanceof CommonInscrit) {
            if ($entreprise) {
                $corps = str_replace('DEFINE_ENTITE_ACHAT', Prado::localize('TEXT_ENTREPRISE'), $corps);
                $corps = str_replace('VALEUR_ENTITE_ACHAT', $entreprise->getNom(), $corps);

                $corps = str_replace('DEFINE_TEXT_ENTITE_PUBLIC', '', $corps);
                $corps = str_replace('VALEUR_ENTITE_PUBLIC', '', $corps);
            }
            if ('entreprise' != strtolower($userVo->getType())) {
                if ($userO->getOrganisme()) {
                    $entitePublic = Atexo_EntityPurchase::getPathEntityById('0', $userO->getOrganisme());
                    $corps = str_replace('DEFINE_TEXT_ENTITE_PUBLIC', Prado::localize('DEFINE_TEXT_ENTITE_PUBLIC'), $corps);
                    $corps = str_replace('VALEUR_ENTITE_PUBLIC', $entitePublic, $corps);
                }
                if ('admin' != strtolower($userVo->getType())) {
                    $entiteAchat = Atexo_EntityPurchase::getPathEntityById($userO->getServiceId(), $userO->getOrganisme());
                    $corps = str_replace('VALEUR_ENTITE_ACHAT', $entiteAchat, $corps);
                }
            }
        }

        $corps = self::getTexteTemplate($corps, $userVo);
        //valeurs
        $corps = str_replace('VALEUR_PRENOM', $userO->getPrenom(), $corps);
        $corps = str_replace('VALEUR_NOM_MAJ', $userO->getNom(), $corps);
        $corps = str_replace('VALEUR_ADRESSE_ELECTRONIQUE', $userO->getEmail(), $corps);
        $corps = str_replace('VALEUR_IDENTIFIANT', $userO->getLogin(), $corps);
        $corps = str_replace('PF_ACHAT_D_ETAT', Prado::localize('PF_ACHAT_D_ETAT'), $corps);

        return $corps;
    }

    public function getInfoPersonneConnecte($userVo)
    {
        $string = null;
        $userO = null;
        $entreprise = null;
        if ('entreprise' == strtolower($userVo->getType())) {
            $userO = (new Atexo_Entreprise_Inscrit())->retrieveInscritByLogin($userVo->getLogin());
            $entreprise = (new Atexo_Entreprise())->retrieveCompanyBdeById($userO->getEntrepriseId());
        } elseif ('admin' == strtolower($userVo->getType())) {
            $userO = (new Atexo_Admin())->retrieveAdminByLogin($userVo->getLogin());
        } else {
            $userO = (new Atexo_Agent())->retrieveAgentByLogin($userVo->getLogin());
        }
        if ($userO instanceof Entreprise || $userO instanceof CommonAdministrateur || $userO instanceof CommonAgent || $userO instanceof CommonInscrit) {
            if ($entreprise) {
                $string .= Prado::localize('TEXT_ENTREPRISE').' : '.$entreprise->getNom().'
';
            }
            $string .= Prado::localize('DEFINE_PRENOM').' : '.$userO->getPrenom().'
';
            $string .= Prado::localize('DEFINE_NOM').' : '.$userO->getNom().'
';
            $string .= Prado::localize('TEXT_ADRESSE_ELECTRONIQUE').' : '.$userO->getEmail().'
';
            $string .= Prado::localize('TEXT_IDENTIFIANT').' : '.$userVo->getLogin().'
';
            $string .= '
';

            if ('entreprise' != strtolower($userVo->getType())) {
                if ($userO->getOrganisme()) {
                    $entitePublic = Atexo_EntityPurchase::getPathEntityById('0', $userO->getOrganisme());

                    $string .= Prado::localize('DEFINE_TEXT_ENTITE_PUBLIC').' : '.$entitePublic.'
';
                }
                if ('admin' != strtolower($userVo->getType())) {
                    $entiteAchat = Atexo_EntityPurchase::getPathEntityById($userO->getServiceId(), $userO->getOrganisme());
                    $string .= Prado::localize('DEFINE_TEXT_ENTITE_ACHAT').' : '.$entiteAchat.'
	';
                }
            }
        }

        return $string;
    }

    public function sendAlerteDesactivationCompte($userVo, $agent)
    {
        //DEBUT envoi du mail de desactivation de compte a l'agent
        $corpsMail = self::getCorpsMessageAlerteDesactivationCompte($userVo);
        //$corpsMail = self::getInfoPersonneConnecte($userVo);
        if ($agent->getEmail()) {
            Atexo_Message::simpleMail(Atexo_Config::getParameter('PF_MAIL_FROM'), $agent->getEmail(), Prado::localize('OBJET_DESACTIVATION_COMPTE'), $corpsMail, '', '', false, true);
        }
        //FIN envoi du mail de desactivation de compte a l'agent
    }
}
