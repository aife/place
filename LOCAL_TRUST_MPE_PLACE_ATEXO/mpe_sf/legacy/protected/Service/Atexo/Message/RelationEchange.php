<?php

namespace Application\Service\Atexo\Message;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonRelationEchange;
use Application\Propel\Mpe\CommonRelationEchangePeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;

/**
 * classe RelationEchange.
 *
 * @author Khalid Atlassi  <khalid.atlassi@atexo.com>
 * @copyright Atexo 2011
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Message_RelationEchange
{
    public function save($idEchange, $idExterne, $typeRelation)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $relationEchangeBean = new CommonRelationEchange();
        $relationEchangeBean->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
        $relationEchangeBean->setIdEchange($idEchange);
        $relationEchangeBean->setIdExterne($idExterne);
        $relationEchangeBean->setTypeRelation($typeRelation);
        $relationEchangeBean->setDateEnvoi(date('Y-m-d H:i:s'));
        $relationEchangeBean->save($connexionCom);
    }

    public function getRelationByIdEchangeAndTypeRelation($idEchange, $typeRelation, $organisme)
    {
        $propelConnection = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonRelationEchangePeer::ID_ECHANGE, $idEchange);
        $c->add(CommonRelationEchangePeer::TYPE_RELATION, $typeRelation);
        $c->add(CommonRelationEchangePeer::ORGANISME, $organisme);
        $relation = CommonRelationEchangePeer::doSelectOne($c, $propelConnection);

        return $relation;
    }

    /*
     * recupère les relations échanges par idTypeExterne et par TypeRelation
     */
    public function getRelationEchangeByIdExterneAndTypeRelation($idExterne, $typeRelation, $org = null)
    {
        if (!$org) {
            $org = Atexo_CurrentUser::getCurrentOrganism();
        }
        $propelConnection = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonRelationEchangePeer::ID_EXTERNE, $idExterne);
        $c->add(CommonRelationEchangePeer::TYPE_RELATION, $typeRelation);
        $c->add(CommonRelationEchangePeer::ORGANISME, $org);
        $relationeEchange = CommonRelationEchangePeer::doSelect($c, $propelConnection);

        return $relationeEchange;
    }
}
