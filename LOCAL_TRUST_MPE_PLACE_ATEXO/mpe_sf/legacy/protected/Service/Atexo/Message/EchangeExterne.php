<?php

namespace Application\Service\Atexo\Message;

use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_MultiDomaine;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_Xml;
use DOMDocument;
use Laminas\Http\Client;
use Laminas\Http\Client\Adapter\Curl;
use Laminas\Http\Client\Adapter\Proxy;
use Laminas\Http\Request;

/**
 * Classe EchangeExterne.
 *
 * @author Khadija CHOUIKA <khadija.chouika@atexo.com>
 * @copyright Atexo 2012
 *
 * @version 1.0
 *
 * @since MPE-4.0
 */
class Atexo_Message_EchangeExterne
{
    private string $encoding = 'UTF-8';

    public function intialiseEchangeRep($codelien, $inscrit, $entreprise)
    {
        $xmlInitialisation = $this->generateXmlInitialisation($codelien, $inscrit, $entreprise);
        $config = [
            'adapter' => Curl::class,
            'curloptions' => [
                CURLOPT_SSL_VERIFYHOST => 0,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_TIMEOUT => 30,
            ],
        ];

        if ('' != Atexo_Config::getParameter('URL_PROXY')) {
            $config = array(
                'proxy_host' => Atexo_Config::getParameter('URL_PROXY'),
                'proxy_port' => Atexo_Config::getParameter('PORT_PROXY'),
            );
        }
        $client = new Client(null, $config);
        $client->setUri(Atexo_Config::getParameter('URL_MESSAGERIE_SECURISE_INITIALISATION'));
        $client->setMethod(Request::METHOD_POST);
        $client->setEncType('application/xml');
        $client->setRawBody($xmlInitialisation);
        $res = $client->send();

        return $res->getBody();
    }

    public function generateXmlInitialisation($codelien, $inscrit, $entreprise)
    {
        $nomPlatf = Atexo_Config::getParameter('PF_LONG_NAME');
        $urlPlatf = Atexo_MultiDomaine::getDomaine(Atexo_Config::getParameter('PF_URL_REFERENCE'));
        //Pour les entreprises creees par SGMAP pas d encodage du & alors que pour les saisie manuelle oui c est encode
        $nomExpediteur = htmlentities(html_entity_decode($entreprise->getNom()));
        $siren = $entreprise->getSiren();
        if (!$siren) {
            $siren = $entreprise->getSirenEtranger();
        }
        $nomContactDest = $inscrit->getPrenom().' '.$inscrit->getNom();
        $mailContactD = $inscrit->getEmail();
        $idContact = $inscrit->getId();
        $domDocument = new DOMDocument('1.0', $this->encoding);

        $messagsecure = (new Atexo_Xml())->addElement($domDocument, $domDocument, 'message_securise_init');
        (new Atexo_Xml())->addAttribute($domDocument, $messagsecure, 'id_objet_metier_pf_emetteur', 'NAN');
        (new Atexo_Xml())->addAttribute($domDocument, $messagsecure, 'id_pf_emetteur', Atexo_Config::getParameter('UID_PF_MPE'));
        (new Atexo_Xml())->addAttribute($domDocument, $messagsecure, 'xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
        (new Atexo_Xml())->addAttribute($domDocument, $messagsecure, 'xsi:noNamespaceSchemaLocation', 'messageIntialisation.xsd');
        (new Atexo_Xml())->addElement($domDocument, $messagsecure, 'nom_pf_emetteur', Atexo_Util::toUtf8($nomPlatf));
        (new Atexo_Xml())->addElement($domDocument, $messagsecure, 'url_pf_emetteur', $urlPlatf);
        (new Atexo_Xml())->addElement($domDocument, $messagsecure, 'ref_objet_metier', $codelien);
        (new Atexo_Xml())->addElement($domDocument, $messagsecure, 'nom_complet_expediteur', Atexo_Util::toUtf8($nomExpediteur));
        (new Atexo_Xml())->addElement($domDocument, $messagsecure, 'email_expediteur', $mailContactD);
        $destinatairesPfEmetteur = (new Atexo_Xml())->addElement($domDocument, $messagsecure, 'destinataires_pf_emetteur');
        $destinatairePfEmetteur = (new Atexo_Xml())->addElement($domDocument, $destinatairesPfEmetteur, 'destinataire_pf_emetteur');
        (new Atexo_Xml())->addElement($domDocument, $messagsecure, 'destinataires_pf_destinataire');
        (new Atexo_Xml())->addElement($domDocument, $messagsecure, 'types_interdits');
        (new Atexo_Xml())->addElement($domDocument, $destinatairePfEmetteur, 'id_entreprise_dest', $siren);
        (new Atexo_Xml())->addElement($domDocument, $destinatairePfEmetteur, 'id_contact_dest', $idContact);
        (new Atexo_Xml())->addElement($domDocument, $destinatairePfEmetteur, 'nom_entreprise_dest', Atexo_Util::toUtf8($nomExpediteur));
        (new Atexo_Xml())->addElement($domDocument, $destinatairePfEmetteur, 'nom_contact_dest', Atexo_Util::toUtf8($nomContactDest));
        (new Atexo_Xml())->addElement($domDocument, $destinatairePfEmetteur, 'mail_contact_destinataire', $mailContactD);

        return $domDocument->saveXml();
    }
}
