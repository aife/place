<?php

namespace Application\Service\Atexo;

use Application\Pages\Commun\DownloadFile;
use Application\Propel\Mpe\CommonConsultation;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureType;
use Application\Service\Atexo\Consultation\Atexo_Consultation_TypeAvis;
use AtexoPdf\PdfGeneratorClient;
use Prado\Prado;

/**
 * Classe de génération de l'avis.
 *
 * @author Khadija CHOUIKA <khalid.benamar@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 4.0
 *
 * @since MPE-3.0
 */
class Atexo_CalendrierPdf
{
    public function generateCalendrierPrevisionel($consultation, $modele, $langue, $fileName)
    {
        $typeAnnonce = null;
        $modele = $modele.$langue.'.odt';
        $encoding = Atexo_Config::getParameter('HTTP_ENCODING');
        //verifier si le fichier existe pour cette langue.
        $calendrier = (new Atexo_GestionCalendrier())->retrieveCalendierByrefConsultationOrg($consultation->getId(), $consultation->getOrganisme());
        if (($consultation instanceof CommonConsultation)) {
            if (is_file($modele)) {
                if (!is_dir(Atexo_Config::getParameter('COMMON_TMP').'/tmpOdt')) {
                    mkdir(Atexo_Config::getParameter('COMMON_TMP').'/tmpOdt');
                }
                $config = [
                                'ZIP_PROXY' => \PhpZipProxy::class,
                                'DELIMITER_LEFT' => '{',
                                'DELIMITER_RIGHT' => '}',
                                'PATH_TO_TMP' => Atexo_Config::getParameter('COMMON_TMP'),
                                ];
                $odf = new Atexo_Odf($modele, $config);
                //*********//
                $abbreviationLangue = Atexo_Languages::getLanguageAbbreviation($langue);
                $typeProcedure = 'getLibelleTypeProcedure'.$abbreviationLangue;
                $getIntituleAvis = 'getIntituleAvis'.$abbreviationLangue;
                $getDenominationOrg = 'getDenominationOrg'.$abbreviationLangue;

                $org = $consultation->getOrganisme();
                $reference = $consultation->getReferenceUtilisateur();
                $organismeO = Atexo_Organismes::retrieveOrganismeByAcronyme($org);

                $objet = $consultation->getObjetTraduit();
                $service = Atexo_EntityPurchase::getPathEntityById($consultation->getServiceId(), $org, $langue);
                if (0 == strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE'))
                        || !$organismeO->$getDenominationOrg()) {
                    $organisme = $organismeO->getSigle().' - '.$organismeO->getDenominationOrg();
                } else {
                    $organisme = $organismeO->getSigle().' - '.$organismeO->$getDenominationOrg();
                }
                $typeAvis = (new Atexo_Consultation_TypeAvis())->retrieveTypeAnnonce($consultation->getIdTypeAvis());
                if ($typeAvis) {
                    if (0 == strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE'))
                        || !$typeAvis->$getIntituleAvis()) {
                        $typeAnnonce = $typeAvis->getIntituleAvis();
                    } else {
                        $typeAnnonce = $typeAvis->$getIntituleAvis();
                    }
                }
                $typeProcedure = (new Atexo_Consultation_ProcedureType())->retrieveTypeProcedure($consultation->getIdTypeProcedureOrg(), false, null, $langue);
                $intitule = $consultation->getIntituleTraduit();
                $datelimite = Atexo_Util::iso2frnDateTime($consultation->getDatefin());
                $dateMiseEnLigne = Atexo_Util::iso2frnDateTime($consultation->getDatemiseenligne());
                $allotissement = $consultation->getAlloti() ? 'Oui' : 'Non'; // a gere par langue
                //remplacement des variables dans le fichier odt
                $odf->setVars('entitePublic', $organisme, true, $encoding);
                $odf->setVars('entiteAchat', $service, true, $encoding);
                $odf->setVars('reference', $reference, true, $encoding);
                $odf->setVars('intitule', html_entity_decode($intitule, ENT_QUOTES), true, $encoding); //ENT_QUOTES Convertit les guillemets doubles et les guillemets simples.
                $odf->setVars('objet', html_entity_decode(Atexo_Util::replaceRetourALaLigne($objet, ' '), ENT_QUOTES), true, $encoding);
                $odf->setVars('typeAnnonce', $typeAnnonce, true, $encoding);
                $odf->setVars('typeProcedure', $typeProcedure, true, $encoding);
                $odf->setVars('dateLimiteRemisePlis', $datelimite, true);
                $odf->setVars('dateMiseEnLigne', $dateMiseEnLigne, true);
                $odf->setVars('allotissement', $allotissement, true, $encoding);
                if ('2' == $consultation->getTypeAcces()) {
                    $typeAcces = prado::localize('DEFINE_ACCES_RESTREINT');
                } else {
                    $typeAcces = prado::localize('TEXT_ACCES_PUBLIC');
                }
                $odf->setVars('accespublic', $typeAcces, true, $encoding);
                if ($calendrier) {
                    $idCalendrier = $calendrier->getIdCalendrier();
                    $arrayEtapes = (new Atexo_GestionCalendrier())->getEtapesAndTransitionByIdcalendrier($idCalendrier);
                    $entetTab = $odf->setSegment('ENTETE_TABLEAU_ETAPES_CALENDRIER');
                    if (is_array($arrayEtapes) && count($arrayEtapes) > 0) {
                        foreach ($arrayEtapes as $etape) {
                            $entetTab->ligneFichier->setVars('ref', $etape['ref'], true, $encoding);
                            $entetTab->ligneFichier->setVars('etapes', $etape['libelle'], true, $encoding);
                            $entetTab->ligneFichier->setVars('duree', $etape['duree'], true, $encoding);
                            $entetTab->ligneFichier->setVars('echeance', Atexo_Util::getFormatedDate($etape['echeance'], '.').' '.Atexo_Util::iso2frnDate($etape['echeance']), true, $encoding);
                            //dans le cas d'un calendrier réel
                            if (isset($_GET['reel'])) {
                                $entetTab->ligneFichier->setVars('reel', Atexo_Util::getFormatedDate($etape['dateReel'], '.').' '.Atexo_Util::iso2frnDate($etape['dateReel']), true, $encoding);
                            }
                            $entetTab->ligneFichier->merge();
                        }
                    }
                    $entetTab->merge();
                    $odf->mergeSegment($entetTab);
                }
                $pathFile = Atexo_Config::getParameter('COMMON_TMP').'Calendrier'.uniqid().'.odt';
                $odf->saveToDisk($pathFile);
                self::generatePdf($pathFile, $fileName);
            }
        }
    }

    public function generatePdf($fileOdt, $fileName)
    {
        if ($fileOdt) {
            $file = $fileName.'.pdf';
            $pdfContent = (new PdfGeneratorClient())->genererPdf($fileOdt);
            DownloadFile::downloadFileContent($file, $pdfContent);
            @unlink($fileOdt);
        }
    }
}
