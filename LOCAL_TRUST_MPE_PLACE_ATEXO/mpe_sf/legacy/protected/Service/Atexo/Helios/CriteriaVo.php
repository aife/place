<?php

namespace Application\Service\Atexo\Helios;

/**
 * Vo pour définir les critères de recherche des teletransmissions.
 *
 * @author Othmane ABISOUROUR <othmane.abisourour@atexo.com>
 * @copyright Atexo 2009
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Helios_CriteriaVo
{
    private string $_numTeletransmission = '';
    private string $_refConsultation = '';
    private string $_numMarche = '';
    private string $_statutEnvoi = '';
    private string $_dateEnvoi = '';
    private string $_dateFin = '';
    private int $offset = 0;
    private int $limit = 0;
    private string $secondConditionOnEtatTeletransmission = '';
    private string $sortByElement = '';
    private string $sensOrderBy = 'DESC';
    private string $keyWordAdvancedSearch = '';

    public function getNumTeletransmission()
    {
        return $this->_numTeletransmission;
    }

    public function setNumTeletransmission($value)
    {
        $this->_numTeletransmission = $value;
    }

    public function getRefConsultation()
    {
        return $this->_refConsultation;
    }

    public function setRefConsultation($value)
    {
        $this->_refConsultation = $value;
    }

    public function getNumMarche()
    {
        return $this->_numMarche;
    }

    public function setNumMarche($value)
    {
        $this->_numMarche = $value;
    }

    public function getStatutEnvoi()
    {
        return $this->_statutEnvoi;
    }

    public function setStatutEnvoi($value)
    {
        $this->_statutEnvoi = $value;
    }

    public function getDateEnvoiStart()
    {
        return $this->_dateEnvoi;
    }

    public function setDateEnvoiStart($value)
    {
        $this->_dateEnvoi = $value;
    }

    public function getDateEnvoiEnd()
    {
        return $this->_dateFin;
    }

    public function setDateEnvoiEnd($value)
    {
        $this->_dateFin = $value;
    }

    public function getOffset()
    {
        return $this->offset;
    }

    public function setOffset($offset)
    {
        $this->offset = $offset;
    }

    public function getLimit()
    {
        return $this->limit;
    }

    public function setLimit($limit)
    {
        $this->limit = $limit;
    }

    public function getSecondConditionOnEtatTeletransmission()
    {
        return $this->secondConditionOnEtatTeletransmission;
    }

    public function setSecondConditionOnEtatTeletransmission($secondConditionOnEtatTeletransmission)
    {
        $this->secondConditionOnEtatTeletransmission = $secondConditionOnEtatTeletransmission;
    }

    public function getSortByElement()
    {
        return $this->sortByElement;
    }

    public function setSortByElement($sortByElement)
    {
        $this->sortByElement = $sortByElement;
    }

    public function getSensOrderBy()
    {
        return $this->sensOrderBy;
    }

    public function setSensOrderBy($sensOrderBy)
    {
        $this->sensOrderBy = $sensOrderBy;
    }

    public function getKeyWordAdvancedSearch()
    {
        return $this->keyWordAdvancedSearch;
    }

    public function setKeyWordAdvancedSearch($keyWordAdvancedSearch)
    {
        $this->keyWordAdvancedSearch = $keyWordAdvancedSearch;
    }
}
