<?php

namespace Application\Service\Atexo\Helios;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonHeliosPiecePublicitePeer;
use Application\Propel\Mpe\CommonHeliosPvConsultationPeer;
use Application\Propel\Mpe\CommonHeliosRapportPrefetPeer;
use Application\Propel\Mpe\CommonHeliosTableauArPeer;
use Application\Propel\Mpe\CommonHeliosTeletransmissionLotPeer;
use Application\Propel\Mpe\CommonHeliosTeletransmissionPeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_Util;
use PDO;

/**
 * Classe de manipulation des teletransmission.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Helios_Teletransmission
{
    /**
     * @todo la fonction ne semble pas être utilisé (non appelé)
     */
    public function retreiveTransByRefCons($consultationId)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonHeliosTeletransmissionPeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonHeliosTeletransmissionPeer::ORGANISME, Atexo_CurrentUser::getCurrentOrganism());
        $c->addDescendingOrderByColumn(CommonHeliosTeletransmissionPeer::ID);
        $trans = CommonHeliosTeletransmissionPeer::doSelectOne($c, $connexion);
        if ($trans) {
            return $trans;
        } else {
            return false;
        }
    }

    public function buildFormatedTransmissionId($identifiant)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $trans = CommonHeliosTeletransmissionPeer::retrieveByPK($identifiant, Atexo_CurrentUser::getCurrentOrganism(), $connexion);
        $shownId = date('Ym').$identifiant;
        $trans->setShownId($shownId);
        $trans->save($connexion);

        return $shownId;
    }

    public function reteivePiecesPubliciteConsByrefCons($consultationId)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonHeliosPiecePublicitePeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonHeliosPiecePublicitePeer::ORGANISME, Atexo_CurrentUser::getCurrentOrganism());
        $piecesPub = CommonHeliosPiecePublicitePeer::doSelect($c, $connexion);
        if ($piecesPub) {
            return $piecesPub;
        } else {
            return [];
        }
    }

    public function reteivePiecesPubliciteConsByIds($idsPieces)
    {
        $piecesPub = null;
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        if ($idsPieces) {
            $arrayIds = explode(';', $idsPieces);
            $c->add(CommonHeliosPiecePublicitePeer::ID, $arrayIds, Criteria::IN);
            $c->add(CommonHeliosPiecePublicitePeer::ORGANISME, Atexo_CurrentUser::getCurrentOrganism());
            $piecesPub = CommonHeliosPiecePublicitePeer::doSelect($c, $connexion);
        }
        if ($piecesPub) {
            return $piecesPub;
        } else {
            return [];
        }
    }

    public function reteivePiecesPvConsultationByrefCons($consultationId)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonHeliosPvConsultationPeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonHeliosPvConsultationPeer::ORGANISME, Atexo_CurrentUser::getCurrentOrganism());
        $pieces = CommonHeliosPvConsultationPeer::doSelect($c, $connexion);
        if ($pieces) {
            return $pieces;
        } else {
            return [];
        }
    }

    public function reteivePiecesPvConsultationByIds($idsPieces)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $arrayIds = explode(';', $idsPieces);
        $c->add(CommonHeliosPvConsultationPeer::ID, $arrayIds, Criteria::IN);
        $c->add(CommonHeliosPvConsultationPeer::ORGANISME, Atexo_CurrentUser::getCurrentOrganism());
        $pieces = CommonHeliosPvConsultationPeer::doSelect($c, $connexion);
        if ($pieces) {
            return $pieces;
        } else {
            return [];
        }
    }

    public function reteivePiecesRapportByRefCons($consultationId)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonHeliosRapportPrefetPeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonHeliosRapportPrefetPeer::ORGANISME, Atexo_CurrentUser::getCurrentOrganism());
        $pieces = CommonHeliosRapportPrefetPeer::doSelect($c, $connexion);
        if ($pieces) {
            return $pieces;
        } else {
            return [];
        }
    }

    public function reteivePiecesRapportByIds($idsPieces)
    {
        $pieces = null;
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        if ($idsPieces) {
            $arrayIds = explode(';', $idsPieces);
            $c->add(CommonHeliosRapportPrefetPeer::ID, $arrayIds, Criteria::IN);
            $c->add(CommonHeliosRapportPrefetPeer::ORGANISME, Atexo_CurrentUser::getCurrentOrganism());
            $pieces = CommonHeliosRapportPrefetPeer::doSelect($c, $connexion);
        }
        if ($pieces) {
            return $pieces;
        } else {
            return [];
        }
    }

    public function reteivePiecesArByrefCons($consultationId)
    {
        $connexionOrg = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonHeliosTableauArPeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonHeliosTableauArPeer::ORGANISME, Atexo_CurrentUser::getCurrentOrganism());
        $pieces = CommonHeliosTableauArPeer::doSelect($c, $connexionOrg);
        if ($pieces) {
            return $pieces;
        } else {
            return [];
        }
    }

    public function reteivePiecesArByIds($idsPieces)
    {
        $pieces = null;
        $connexionOrg = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        if ($idsPieces) {
            $arrayIds = explode(';', $idsPieces);
            $c->add(CommonHeliosTableauArPeer::ID, $arrayIds, Criteria::IN);
            $c->add(CommonHeliosTableauArPeer::ORGANISME, Atexo_CurrentUser::getCurrentOrganism());
            $pieces = CommonHeliosTableauArPeer::doSelect($c, $connexionOrg);
        }
        if ($pieces) {
            return $pieces;
        } else {
            return [];
        }
    }

    /**
     * Fonction qui retourne le resultat de la requete.
     *
     * @param : $criteriaVo                : objet contenant les criteres de recherches
     * @param : $returnJustNumberOfElement
     */
    public function searchTeletransmission(Atexo_Helios_CriteriaVo $criteriaVo, $returnJustNumberOfElement = false)
    {
        //print_r($criteriaVo);exit;
        $queryParams = [];
        if ($returnJustNumberOfElement) {
            $query = 'Select DISTINCT Helios_teletransmission.id ';
        } else {
            $query = 'SELECT DISTINCT Helios_teletransmission.id ,';
            $query .= 'Helios_teletransmission.id,Helios_teletransmission.shown_id,Helios_teletransmission.id_agent_envoi,Helios_teletransmission.consultation_id,Helios_teletransmission.date_envoi,';
            $query .= 'consultation.id,consultation.reference_utilisateur';
        }
        $query .= ' FROM Helios_teletransmission ,Helios_teletransmission_lot ,consultation ';
        $query .= " WHERE Helios_teletransmission.organisme = '".Atexo_CurrentUser::getCurrentOrganism()."' ";
        $query .= ' AND Helios_teletransmission_lot.organisme=Helios_teletransmission.organisme  ';
        $query .= ' AND consultation.organisme=Helios_teletransmission.organisme  ';
        $query .= ' AND Helios_teletransmission_lot.id_teletransmission = Helios_teletransmission.id';
        $query .= ' AND Helios_teletransmission.consultation_id=consultation.id';
        if ('' != $criteriaVo->getKeyWordAdvancedSearch()) {
            $query .= ' AND ( Helios_teletransmission.shown_id like :numTran ';
            $queryParams[':numTran'] = '%'.$criteriaVo->getKeyWordAdvancedSearch().'%';

            $query .= ' OR consultation.reference_utilisateur like :refCons';
            $queryParams[':refCons'] = '%'.$criteriaVo->getKeyWordAdvancedSearch().'%';

            $query .= ' OR Helios_teletransmission_lot.numero_marche like :numMarche )';
            $queryParams[':numMarche'] = '%'.$criteriaVo->getKeyWordAdvancedSearch().'%';
        }

        if ('' != $criteriaVo->getNumTeletransmission()) {
            $query .= ' AND Helios_teletransmission.shown_id like :numTran';
            $queryParams[':numTran'] = '%'.$criteriaVo->getNumTeletransmission().'%';
        }

        if ('' != $criteriaVo->getRefConsultation()) {
            $query .= ' AND consultation.reference_utilisateur like :refCons';
            $queryParams[':refCons'] = '%'.$criteriaVo->getRefConsultation().'%';
        }
        //print_r($criteriaVo);exit;
        if ('' != $criteriaVo->getNumMarche()) {
            $query .= ' AND Helios_teletransmission_lot.numero_marche like '.
             "'%".$criteriaVo->getNumMarche()."%'";
        }

        if ('' != $criteriaVo->getStatutEnvoi()) {
            if ('2' == $criteriaVo->getStatutEnvoi()) {
                $query .= ' AND (Helios_teletransmission_lot.statut_envoi=:statut )';
                $queryParams[':statut'] = Atexo_Config::getParameter('TRANSMISSION_SEND');
            } elseif ('1' == $criteriaVo->getStatutEnvoi()) {
                $query .= ' AND (Helios_teletransmission_lot.statut_envoi=:statut )';
                $queryParams[':statut'] = Atexo_Config::getParameter('TRANSMISSION_SAVE');
            }
        }

        if ('' != $criteriaVo->getDateEnvoiStart()) {
            $query .= ' AND Helios_teletransmission.date_envoi>=:dateEnvoiStart';
            $queryParams[':dateEnvoiStart'] = Atexo_Util::frnDateTime2iso($criteriaVo->getDateEnvoiStart());
        }

        if ('' != $criteriaVo->getDateEnvoiEnd()) {
            $query .= ' AND Helios_teletransmission.date_envoi<=:dateEnvoiEnd';
            $queryParams[':dateEnvoiEnd'] = Atexo_Util::frnDateTime2iso($criteriaVo->getDateEnvoiEnd());
        }

        if ($criteriaVo->getSortByElement()) {
            $query .= ' ORDER BY '.$criteriaVo->getSortByElement().' '.$criteriaVo->getSensOrderBy();
        } else {
            $query .= ' ORDER BY Helios_teletransmission_lot.id_teletransmission DESC ';
        }

        if ($criteriaVo->getLimit()) {
            $query .= ' limit '.$criteriaVo->getOffset().','.$criteriaVo->getLimit();
        }
        $statement = Atexo_Db::getLinkCommon(true)->prepare($query);
        $statement->execute($queryParams);
        if ($returnJustNumberOfElement) {
            $nombreElement = 0;
            while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                ++$nombreElement;
            }

            return $nombreElement;
        } else {
            $arrayTrans = [];
            while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                $arrayTrans[] = $row;
            }
        }
        if ($arrayTrans) {
            //print_r($arrayTrans);exit;
            return $arrayTrans;
        } else {
            return [];
        }
    }

    public function retreiveTransLotByIdtrans($idTrans)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonHeliosTeletransmissionLotPeer::ID_TELETRANSMISSION, $idTrans, Criteria::EQUAL);
        $c->add(CommonHeliosTeletransmissionLotPeer::ORGANISME, Atexo_CurrentUser::getCurrentOrganism());
        $trans = CommonHeliosTeletransmissionLotPeer::doSelect($c, $connexion);
        if ($trans) {
            return $trans;
        } else {
            return [];
        }
    }

    /**
     * Fonction qui retourne le nobmre total des télétransmissions.
     *
     * @param : $idAgent   : l'id de l'agent connecté
     * @param : $organisme : l'organisme de l'agent
     */
    public function retrieveCountAllTeletransmissionByAgent($idAgent, $organisme)
    {
        $temps_debut = microtime(true);
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));

        $c = new Criteria();
        $c->add(CommonHeliosTeletransmissionPeer::ID_AGENT_CREATION, $idAgent);
        $c->add(CommonHeliosTeletransmissionPeer::ORGANISME, $organisme);
        $allTransmission = CommonHeliosTeletransmissionPeer::doSelect($c, $connexion);
        if ($allTransmission) {
            $temps_fin = microtime(true);
            $txt = 'Debut retrieveCountAllTeletransmissionByAgent et fin : '.round($temps_fin - $temps_debut, 4)."\n\n";
            $fp = fopen(Atexo_Config::getParameter('LOG_DIR').session_id().'timer.txt', 'a');
            fwrite($fp, $txt);
            fclose($fp);

            return count($allTransmission);
        }
    }

    /***
     * Rerourner le nombre des transmissions déja envoyées
     * @param : $organisme : l'organisme de l'agent connecté
     */
    public function retirieveCountTransmissionTransmitted($organisme)
    {
        $temps_debut = microtime(true);
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonHeliosTeletransmissionLotPeer::STATUT_ENVOI, Atexo_Config::getParameter('TRANSMISSION_SEND'));
        $c->add(CommonHeliosTeletransmissionLotPeer::ORGANISME, $organisme);
        $transmissionTransmitted = CommonHeliosTeletransmissionLotPeer::doSelect($c, $connexion);
        if ($transmissionTransmitted) {
            $temps_fin = microtime(true);
            $txt = 'Debut retirieveCountTransmissionTransmitted et fin : '.round($temps_fin - $temps_debut, 4)."\n\n";
            $fp = fopen(Atexo_Config::getParameter('LOG_DIR').session_id().'timer.txt', 'a');
            fwrite($fp, $txt);
            fclose($fp);

            return count($transmissionTransmitted);
        }
    }

    /***
     * Rerourner le nombre des transmissions a envoyées
     * @param : $organisme : l'organisme de l'agent connecté
     */
    public function retirieveCountTransmissionToSend($organisme)
    {
        $temps_debut = microtime(true);
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonHeliosTeletransmissionLotPeer::STATUT_ENVOI, Atexo_Config::getParameter('TRANSMISSION_SAVE'));
        $c->add(CommonHeliosTeletransmissionLotPeer::ORGANISME, $organisme);
        $TransmissionToSend = CommonHeliosTeletransmissionLotPeer::doSelect($c, $connexion);
        if ($TransmissionToSend) {
            $temps_fin = microtime(true);
            $txt = 'Debut retirieveCountTransmissionToSend et fin : '.round($temps_fin - $temps_debut, 4)."\n\n";
            $fp = fopen(Atexo_Config::getParameter('LOG_DIR').session_id().'timer.txt', 'a');
            fwrite($fp, $txt);
            fclose($fp);

            return count($TransmissionToSend);
        }
    }

    public function retirieveCountAllTransmission($organisme)
    {
        $temps_debut = microtime(true);
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonHeliosTeletransmissionLotPeer::ORGANISME, $organisme);
        $allTransmission = CommonHeliosTeletransmissionLotPeer::doSelect($c, $connexion);
        if ($allTransmission) {
            $temps_fin = microtime(true);
            $txt = 'Debut retirieveCountAllTransmission et fin : '.round($temps_fin - $temps_debut, 4)."\n\n";
            $fp = fopen(Atexo_Config::getParameter('LOG_DIR').session_id().'timer.txt', 'a');
            fwrite($fp, $txt);
            fclose($fp);

            return count($allTransmission);
        }
    }
}
