<?php

namespace Application\Service\Atexo\Helios;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonFichierEnveloppePeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Crypto\Atexo_Crypto_Certificat;
use PDO;

/**
 * Classe de recuperation des pieces des attributaires.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Helios_PiecesAttributaires
{
    public function retreivePiecesAttributaires($idEnveloppe, $typeEnveloppe, $organisme = null)
    {
        if ($typeEnveloppe == Atexo_Config::getParameter('DEPOT_ELECTRONIQUE')) {
            if (!$organisme) {
                $organisme = Atexo_CurrentUser::getCurrentOrganism();
            }
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            $c = new Criteria();
            $c->add(CommonFichierEnveloppePeer::ID_ENVELOPPE, $idEnveloppe);
            $c->add(CommonFichierEnveloppePeer::ORGANISME, $organisme);
            $reslut = CommonFichierEnveloppePeer::doSelect($c, $connexionCom);
            if ($reslut) {
                return $reslut;
            } else {
                return [];
            }
        } else {
            return [];
        }
    }

    public function retreivePiecesAttributairesWithLot($idEnveloppe, $typeEnveloppe, $lot, $signatureOffre)
    {
        $organisme = Atexo_CurrentUser::getCurrentOrganism();
        $reslut = [];
        if ('' != $idEnveloppe && $typeEnveloppe == Atexo_Config::getParameter('DEPOT_ELECTRONIQUE')) {
            $sql = 'SELECT fichierEnveloppe.ID_FICHIER, fichierEnveloppe.ID_ENVELOPPE,'.
                  ' fichierEnveloppe.TYPE_FICHIER,fichierEnveloppe.SIGNATURE_FICHIER, '.
                  'fichierEnveloppe.NOM_FICHIER, '.
                  ' fichierEnveloppe.ID_BLOB FROM fichierEnveloppe'.
                  ' WHERE fichierEnveloppe.ID_ENVELOPPE='.(new Atexo_Db())->quote($idEnveloppe).
                  " AND fichierEnveloppe.organisme = '".$organisme."' ";
            //echo $sql;exit;
            $statement = Atexo_Db::getLinkCommon()->query($sql);
            $index = 0;
            while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                $reslut[$index]['ID_FICHIER'] = $row['ID_FICHIER'];
                $reslut[$index]['ID_ENVELOPPE'] = $row['ID_ENVELOPPE'];
                $reslut[$index]['TYPE_FICHIER'] = $row['TYPE_FICHIER'];
                $reslut[$index]['NOM_FICHIER'] = $row['NOM_FICHIER'];
                $reslut[$index]['ID_BLOB'] = $row['ID_BLOB'];
                $reslut[$index]['ID_ENVELOPPE'] = $row['ID_ENVELOPPE'];
                $reslut[$index]['CN_SUBJECT'] = (new Atexo_Crypto_Certificat())->getCnSubject($row['SIGNATURE_FICHIER']);
                $reslut[$index]['LOT'] = $lot;
                ++$index;
            }
        }

        return $reslut;
    }
}
