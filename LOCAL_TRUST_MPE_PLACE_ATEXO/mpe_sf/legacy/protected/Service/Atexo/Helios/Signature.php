<?php

namespace Application\Service\Atexo\Helios;

use AtexoCrypto\Dto\Signature;

/**
 * Classe de Signature.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Helios_Signature
{
    public function getLastSignatureIndex($zipFileName, $nameFileSigne)
    {
        $result = 0;
        $zip = zip_open($zipFileName);
        if ($zip) {
            while ($zip_entry = zip_read($zip)) {
                $entry_name = zip_entry_name($zip_entry);
                $entry_name.'<br>';
                $expression = "/^(.+) - Signature ([0-9]+)\.p7s/";
                preg_match($expression, $entry_name, $tmp_result);
                if (($tmp_result[1] == $nameFileSigne) && ($tmp_result[2] > $result)) {
                    $result = $tmp_result[2];
                }
            }
        }

        return $result + 1;
    }
}
