<?php

namespace Application\Service\Atexo;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonJmsJobs;
use DateTime;

/**
 * Created by PhpStorm.
 * User: atexo
 * Date: 05/03/2018
 * Time: 09:54.
 */
class Atexo_JmsJobs
{
    const DEFAULT_QUEUE = 'default';
    const STATE_PENDING = 'pending';
    const STATE_FINISHED = 'finished';

    public static function createJmsJobs(
        $command,
        $args = [],
        $queue = self::DEFAULT_QUEUE,
        DateTime $dateExecuteAfter = null
    ) {
        $cnx = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $job = new CommonJmsJobs();
        $job->setCommand($command);
        $job->setArgs(json_encode($args, JSON_THROW_ON_ERROR));
        $job->setState(self::STATE_PENDING);
        $job->setQueue($queue);
        $job->setPriority(0);
        $job->setCreatedat(date('Y-m-d H:i:s'));
        if (!empty($dateExecuteAfter)) {
            $job->setExecuteafter($dateExecuteAfter->format('Y-m-d H:i:s'));
        } else {
            $dateExecuteafter = date_create($job->getCreatedat('Y-m-d H:i:s'));
            date_sub($dateExecuteafter, date_interval_create_from_date_string('1 second'));
            $job->setExecuteafter(date_format($dateExecuteafter, 'Y-m-d H:i:s'));
        }
        $job->save($cnx);
    }
}
