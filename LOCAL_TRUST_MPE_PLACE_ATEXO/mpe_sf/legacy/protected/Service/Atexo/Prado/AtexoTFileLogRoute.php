<?php

namespace Application\Service\Atexo\Prado;

use Application\Service\Atexo\Atexo_Util;
use Prado\Util\TLogger;
use Prado\Util\TLogRoute;

class AtexoTFileLogRoute extends TLogRoute
{
    protected static  $_levelValues = [
        TLogger::DEBUG => 'DEBUG',
        TLogger::INFO => 'INFO',
        TLogger::NOTICE =>'NOTICE',
        TLogger::WARNING =>'WARNING',
        TLogger::ERROR =>'ERROR',
        TLogger::ALERT =>'ALERT',
        TLogger::FATAL => 'EMERGENCY'
    ];
    protected function processLogs($logs)
    {
        $logger = Atexo_Util::getSfService('mpe.logger');
        foreach ($logs as $log) {
          $logger->log(self::$_levelValues[$log[1]], $log[0], [$log[2], @date('M d H:i:s',$log[3])]);
        }
    }
}
