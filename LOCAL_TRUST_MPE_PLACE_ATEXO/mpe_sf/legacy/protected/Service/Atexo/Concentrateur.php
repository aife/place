<?php

namespace Application\Service\Atexo;

use Application\Propel\Mpe\CommonTAnnonceConsultation;
use Exception;
use Laminas\Http\Client;
use Laminas\Http\Client\Adapter\Proxy;
use Laminas\Http\Request;

/**
 * permet de gérer l'enchange entre MPE et le concentrateur.
 *
 * @author AME <amal.elbekkaoui@atexo.com>
 *
 * @version 1.0
 *
 * @since roadmap-2015
 *
 * @copyright Atexo 2015
 */
class Atexo_Concentrateur
{
    /**
     * permet de mettre à jour l'annonce depuis le concentrateur.
     *
     * @param CommonTAnnonceConsultation $annonce annonce
     * @param Atexo_LoggerManager        $logger  log de trace
     *
     * @return bool annonce est à jour
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2015
     */
    public function updateAnnonce(&$annonce, $logger)
    {
        if (!$logger) {
            $logger = Atexo_LoggerManager::getLogger('publicite');
        }
        $miseAJour = false;
        try {
            if ($annonce instanceof CommonTAnnonceConsultation) {
                $jsonReq = self::sendFlux($annonce->getIdDossierSub());
                $infoAnnonce = json_decode(Atexo_Util::httpEncodingToUtf8($jsonReq), true, 512, JSON_THROW_ON_ERROR);
                $statut = self::getCorrespondantStatutAnnonce($infoAnnonce['statut']);
                $oldStatut = $annonce->getStatut();
                $annonce->setStatut($statut);
                $logger->info("l'annonce à changer du statut ".$oldStatut.' au '.$statut);
                $miseAJour = true;
            } else {
                $logger->error("Erreur Atexo_Concentrateur::updateAnnonce : l'objet envoyé en parametre n'est pas CommonTAnnonceConsultation ".print_r($annonce, true));
            }
        } catch (Exception $e) {
            $logger->error('Erreur Atexo_Concentrateur::updateAnnonce : '.$e->getMessage().' '.$e->getTraceAsString());
        }

        return $miseAJour;
    }

    /**
     * permet d'envoyer le flux au concentrateur.
     *
     * @param int  $id     identifiant SUB de l'annonce
     * @param null $logger log de trace
     *
     * @return \Laminas\Http\Response contenant les informations du suivi de l'annonce
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2015
     */
    public function sendFlux($id, $logger = null)
    {
        $res = null;
        if (!$logger) {
            $logger = Atexo_LoggerManager::getLogger('publicite');
        }
        try {
            $client = new Client();
            if ('' != Atexo_Config::getParameter('URL_PROXY')) {
                $config = array(
                    'proxy_host' => Atexo_Config::getParameter('URL_PROXY'),
                    'proxy_port' => Atexo_Config::getParameter('PORT_PROXY'),
                );
                $client = new Client(null, $config);
            }
            $client->setParameterGet(['id' => $id]);
            $client->setUri(Atexo_Config::getParameter('URL_CONCENTRATEUR_ANNONCE'));
            $client->setMethod(Request::METHOD_GET);
            $res = $client->send();
            $logger->info("resultat retourne par BOAMP pour l'annonce ".$id.' : '.print_r($res, true));
        } catch (Exception $e) {
            $logger->error("Erreur Atexo_Concentrateur::sendFlux l'annonce ".$id.' : '.$e->getMessage().' '.$e->getTraceAsString());
        }

        return $res;
    }

    /**
     * permet de faire la correspondance entre statut BOAMP et statut MPE.
     *
     * @param string $statutBoamp statut BOAMP
     *
     * @return json contenant les informations du suivi de l'annonce
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2015
     */
    public function getCorrespondantStatutAnnonce($statutBoamp)
    {
        $statut = false;
        $statut = match ($statutBoamp) {
            Atexo_Config::getParameter('ANNONCE_STATUT_CONCENTRATEUR_EN_ATTENTE') => Atexo_Config::getParameter('ANNONCE_STATUT_ENVOYE'),
            Atexo_Config::getParameter('ANNONCE_STATUT_CONCENTRATEUR_EN_COURS_DE_PUBLICATION') => Atexo_Config::getParameter('ANNONCE_STATUT_EN_ATTENTE_PUBLICATION'),
            Atexo_Config::getParameter('ANNONCE_STATUT_CONCENTRATEUR_REJETER_SUPPORT') => Atexo_Config::getParameter('ANNONCE_STATUT_REJETE'),
            Atexo_Config::getParameter('ANNONCE_STATUT_CONCENTRATEUR_REJETER_CONCENTRATEUR') => Atexo_Config::getParameter('ANNONCE_STATUT_REJETE'),
            Atexo_Config::getParameter('ANNONCE_STATUT_CONCENTRATEUR_PUBLIER') => Atexo_Config::getParameter('ANNONCE_STATUT_PUBLIE'),
            default => $statut,
        };

        return $statut;
    }

    /**
     * permet de recuppere le formulaire à proposer et la liste des supports.
     *
     * @param string $typeOrganisme
     * @param string $sousTypeOrg
     * @param string $categorie
     * @param string $article
     * @param string $seuil
     *
     * @return array contenant les informations du formulaire à proposer et la liste des supports
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2016
     */
    public static function getSupportFormulaire($typeOrganisme, $sousTypeOrg, $categorie, $article, $seuil)
    {
        $infoFormulaire = null;
        try {
            $logger = Atexo_LoggerManager::getLogger('publicite');
            $infoFormulaire = null;
            $client = new Client();
            if ('' != Atexo_Config::getParameter('URL_PROXY')) {
                $config = array(
                    'proxy_host' => Atexo_Config::getParameter('URL_PROXY'),
                    'proxy_port' => Atexo_Config::getParameter('PORT_PROXY'),
                );
                $client = new Client(null, $config);
            }
            $client->setParameterGet(['typeOrganisme' => $typeOrganisme]);
            if ($sousTypeOrg) {
                $client->setParameterGet(['sousTypeOrganisme' => $sousTypeOrg]);
            }
            $client->setParameterGet(['natureMarche' => $categorie]);
            if ($article) {
                $client->setParameterGet(['articleApplicable' => $article]);
            }
            $client->setParameterGet(['montantEstime' => $seuil]);
            $client->setParameterGet(['millesime' => date('Y')]);
            $client->setUri(Atexo_Config::getParameter('URL_CONCENTRATEUR_DEMANDE_FORMULAIRE'));

            $client->setMethod(Request::METHOD_GET);
            $res = $client->send();
            $logger->info('uri : '.Atexo_Config::getParameter('URL_CONCENTRATEUR_DEMANDE_FORMULAIRE').' requete envoyée  '.print_r($client->getLastRequest(), true));
            $infoFormulaire = json_decode(Atexo_Util::httpEncodingToUtf8($res), true, 512, JSON_THROW_ON_ERROR);
            $logger->info('resultat retourne getSupportFormulaire  '.print_r($res, true));
        } catch (Exception $e) {
            $logger->error('Erreur Atexo_Concentrateur::getSupportFormulaire paramettre  '.$typeOrganisme.' - '.$sousTypeOrg.' - '.$categorie.' - '.$article.' - '.$seuil.' : '.$e->getMessage().' '.$e->getTraceAsString());
        }

        return $infoFormulaire;
    }
}
