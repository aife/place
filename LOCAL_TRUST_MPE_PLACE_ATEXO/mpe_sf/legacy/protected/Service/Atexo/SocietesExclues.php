<?php

namespace Application\Service\Atexo;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonSocietesExclues;
use Application\Propel\Mpe\CommonSocietesExcluesPeer;
use PDO;
use Prado\Web\UI\TPage;

/**
 * Classe de gestion des sociétés exclues.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2010
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_SocietesExclues extends TPage
{
    public function retrieveSocietesExcluesByPk($id)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonSocietesExcluesPeer::ID_SOCIETES_EXCLUES, $id);
        $societesExclues = CommonSocietesExcluesPeer::doSelectOne($c, $connexion);
        if ($societesExclues) {
            return $societesExclues;
        } else {
            return false;
        }
    }

    public function retrieveSocietesExclues()
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $societesExclues = CommonSocietesExcluesPeer::doSelect($c, $connexion);
        if ($societesExclues) {
            return $societesExclues;
        } else {
            return false;
        }
    }

    public function deleteSocieteExclues($idSocieteExclue)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $c = new Criteria();
        $c->add(CommonSocietesExcluesPeer::ID_SOCIETES_EXCLUES, $idSocieteExclue);
        CommonSocietesExcluesPeer::doDelete($c, $connexion);
    }

    public function search($critere, $forNombreResultats, $idSocieteExclue, $limit = 0, $offset = 0)
    {
        $listeSocieteExclues = [];
        $requete = ' SELECT * FROM Societes_Exclues WHERE 1=1 ';
        if ($idSocieteExclue) {
            $requete .= 'AND id_societes_exclues = '.(new Atexo_Db())->quote($idSocieteExclue);
        }
        if (is_array($critere) && count($critere)) {
            foreach ($critere as $unCritere) {
                $requete .= $unCritere;
            }
        }
        if ($limit) {
            $requete .= ' LIMIT '.$offset.' , '.$limit;
        }
        //echo $requete.'<br>';
        $statement = Atexo_Db::getLinkCommon(true)->query($requete, PDO::FETCH_ASSOC);

        if ($statement) {
            $listeSocieteExclues = [];
            foreach ($statement as $ligne) {
                $societeExclues = new CommonSocietesExclues();
                $societeExclues->setIdSocietesExclues($ligne['id_societes_exclues']);
                $societeExclues->setOrganismeAcronyme($ligne['organisme_acronyme']);
                $societeExclues->setServiceId($ligne['id_service']);
                $societeExclues->setIdAgent($ligne['id_agent']);
                $societeExclues->setNomDocument($ligne['nom_document']);
                $societeExclues->setIdBlob($ligne['id_blob']);
                $societeExclues->setTailleDocument($ligne['taille_document']);
                $societeExclues->setIdentifiantEntreprise($ligne['identifiant_entreprise']);
                $societeExclues->setRaisonSociale($ligne['raison_sociale']);
                $societeExclues->setMotif($ligne['motif']);
                $societeExclues->setRaisonSocialeFr($ligne['raison_sociale_fr']);
                $societeExclues->setMotifFr($ligne['motif_fr']);
                $societeExclues->setRaisonSocialeAr($ligne['raison_sociale_ar']);
                $societeExclues->setMotifAr($ligne['motif_ar']);
                $societeExclues->setTypeExclusion($ligne['type_exclusion']);
                $societeExclues->setDateDebutExclusion($ligne['date_debut_exclusion']);
                $societeExclues->setDateFinExclusion($ligne['date_fin_exclusion']);
                $societeExclues->setTypePortee($ligne['type_portee']);
                $listeSocieteExclues[] = $societeExclues;
            }
        }

        if ($forNombreResultats) {
            return count($listeSocieteExclues);
        } else {
            return $listeSocieteExclues;
        }
    }

    public function getdernierSocieteExclue()
    {
        $societesExclues = self::retrieveSocietesExclues();
        if (is_array($societesExclues) && count($societesExclues)) {
            return $societesExclues[count($societesExclues) - 1];
        } else {
            return false;
        }
    }
}
