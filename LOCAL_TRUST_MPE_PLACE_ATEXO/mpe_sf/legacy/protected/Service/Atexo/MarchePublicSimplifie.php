<?php

namespace Application\Service\Atexo;

use App\Traits\ConfigProxyTrait;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonTCandidatureMps;
use Application\Propel\Mpe\CommonTCandidatureMpsPeer;
use Application\Propel\Mpe\CommonTCandidatureMpsQuery;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Lots;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_CandidatureMPS;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Inscrit;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Leaders;
use Application\Service\Atexo\Rest\Action\Atexo_Rest_Action_Actions;
use Application\Service\Atexo\Rest\ObjectField\Atexo_Rest_ObjectField_DocumentField;
use Prado\Prado;
use Laminas\Http\Client;
use Laminas\Http\Request;

/**
 * Classe de manipulation de l'interface avec le site MPS.
 *
 * @author Mohamed WAZNI <mohamed.wazni@atexo.com>
 * @copyright Atexo 2014
 *
 * @version 1.0
 *
 * @since MPE-4.0
 */
class Atexo_MarchePublicSimplifie
{
    use ConfigProxyTrait;

    /**
     * methode qui permet de factoriser la construction de la variable appel_offre_id.
     */
    public function getAppelOffreId($consRefUser, $consultationId, $consOrg)
    {
        return str_replace('.', '_', str_replace(' ', '', Atexo_Util::replaceCharactersMsWordWithRegularCharacters(substr($consRefUser, 0, 230).'_'.$consultationId.'_'.$consOrg.'_'.Atexo_Config::getParameter('PF_SHORT_NAME'))));
    }

    /**
     * Permet de recuperer un objet CommonTCandidatureMps.
     *
     * @param int $idOffre id de l'offre, int $consultationId reference de la consultation
     *
     * @return CommonTCandidatureMps $candidatureMpsObject objet de CommonTCandidatureMps ayant l'id offre = $idOffre et la reference de la consultation = $refCons
     *
     * @author ASO <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function retrieveCandidatureMps($idOffre, $consultationId)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonTCandidatureMpsPeer::ID_OFFRE, $idOffre, Criteria::EQUAL);
        $c->add(CommonTCandidatureMpsPeer::CONSULTATION_ID, $consultationId, Criteria::EQUAL);
        $candidatureMpsObject = CommonTCandidatureMpsPeer::doSelectOne($c, $connexionCom);
        if ($candidatureMpsObject) {
            return $candidatureMpsObject;
        }

        return false;
    }

    /**
     * Permet de recuperer un objet CommonTCandidatureMps.
     *
     * @param int $idCandidature id de la candidature, int $consultationId reference de la consultation,$organisme l'organisme de la consultation,$idInscrit l'id de l'inscrit,$idEntreprise l'id entreprise
     *
     * @return CommonTCandidatureMps $candidatureMpsObject objet de CommonTCandidatureMps
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function retrieveCandidatureMpsSansOffre($idCandidature, $consultationId, $organisme, $idInscrit, $idEntreprise)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonTCandidatureMpsPeer::ID_OFFRE, null);
        $c->add(CommonTCandidatureMpsPeer::ID_CANDIDATURE, $idCandidature);
        $c->add(CommonTCandidatureMpsPeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonTCandidatureMpsPeer::ORGANISME, $organisme);
        $c->add(CommonTCandidatureMpsPeer::ID_INSCRIT, $idInscrit);
        $c->add(CommonTCandidatureMpsPeer::ID_ENTREPRISE, $idEntreprise);
        $candidatureMpsObject = CommonTCandidatureMpsPeer::doSelectOne($c, $connexionCom);
        if ($candidatureMpsObject) {
            return $candidatureMpsObject;
        }

        return false;
    }

    /**
     * Permet de retourner le chemin du fichier odt contennat les informations de la candidature MPS.
     *
     * @param CommonTCandidatureMps $candidatureMPS la candidature MPS,
     * @param bool                  $brouillon      true pour le cas d'impression brouillon,false pour si le fichier est valide
     *
     * @return string le chemin du fichier generer
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function generateOdtCandidatureMPS($candidatureMPS, $consultation, $brouillon = false)
    {
        $prenom = null;
        $nom = null;
        $qualite = null;
        $lang = null;
        if ($candidatureMPS instanceof Atexo_Entreprise_CandidatureMPS) {
            $fileModel = Atexo_Config::getParameter('CHEMIN_MODELE_FORMULAIRE_MPS');
            if (is_file($fileModel)) {
                if (!is_dir(Atexo_Config::getParameter('COMMON_TMP').'/tmpOdt')) {
                    mkdir(Atexo_Config::getParameter('COMMON_TMP').'/tmpOdt');
                }
                $config = [
                                'ZIP_PROXY' => \PhpZipProxy::class,
                                'DELIMITER_LEFT' => '{',
                                'DELIMITER_RIGHT' => '}',
                                'PATH_TO_TMP' => Atexo_Config::getParameter('COMMON_TMP').'/tmpOdt',
                                ];
                $odf = new Atexo_Odf($fileModel, $config);
                $odf->setVars('RefConsultation', Atexo_Util::atexoHtmlEntitiesDecode($candidatureMPS->getReferenceUtilisateur()), true, Atexo_Config::getParameter('HTTP_ENCODING'));
                $odf->setVars('ObjetConsultation', $candidatureMPS->getObjetConsultation(), true, Atexo_Config::getParameter('HTTP_ENCODING'));
                $odf->setVars('DateLimite', Atexo_Util::iso2frnDateTime($candidatureMPS->getDateLimiteRemise()), true, Atexo_Config::getParameter('HTTP_ENCODING'));
                $odf->setVars('HorodatageDepot', date('d/m/Y H:i:s'), true, Atexo_Config::getParameter('HTTP_ENCODING'));
                $entreprise = $candidatureMPS->getEntreprise();
                if ($entreprise) {
                    $odf->setVars('RaisonSocEse', Atexo_Util::atexoHtmlEntitiesDecode($entreprise->getNom()), true, Atexo_Config::getParameter('HTTP_ENCODING'));
                    $odf->setVars('FormeJurEse', $entreprise->getFormejuridique(), true, Atexo_Config::getParameter('HTTP_ENCODING'));
                    $activite = '';
                    if ($entreprise->getDescriptionActivite()) {
                        $activite = $entreprise->getDescriptionActivite();
                    } else {
                        $activite = $entreprise->getCodeape();
                    }
                    $odf->setVars('ActiviteEse', Atexo_Util::atexoHtmlEntitiesDecode($activite), true, Atexo_Config::getParameter('HTTP_ENCODING'));
                    $etablissement = $candidatureMPS->getEtablissement();
                    $siretEse = $entreprise->getSiren();
                    if ($etablissement) {
                        $siretEse .= $etablissement->getCodeEtablissement();
                        $odf->setVars('AdresseEse', Atexo_Util::atexoHtmlEntitiesDecode($etablissement->getAdresse()), true, Atexo_Config::getParameter('HTTP_ENCODING'));
                        $odf->setVars('AdresseEseSuite', Atexo_Util::atexoHtmlEntitiesDecode($etablissement->getAdresse2()), true, Atexo_Config::getParameter('HTTP_ENCODING'));
                        $odf->setVars('CodePostalEse', $etablissement->getCodepostal(), true, Atexo_Config::getParameter('HTTP_ENCODING'));
                        $odf->setVars('VilleEse', Atexo_Util::atexoHtmlEntitiesDecode($etablissement->getVille()), true, Atexo_Config::getParameter('HTTP_ENCODING'));
                    }
                    $odf->setVars('SiretEse', $siretEse, true, Atexo_Config::getParameter('HTTP_ENCODING'));
                    $odf->setVars('PaysEse', Atexo_Util::atexoHtmlEntitiesDecode($entreprise->getPaysadresse()), true, Atexo_Config::getParameter('HTTP_ENCODING'));
                }
                $libelleTranche = (new Atexo_ValeursReferentielles())->retrieveByIdValAndIdRef($candidatureMPS->getTrancheEffectifs(), Atexo_Config::getParameter('ID_REFERENTIEL_TRANCHE_EFFECTIF'), true);
                $odf->setVars('TranchesEffectifs', $libelleTranche, true, Atexo_Config::getParameter('HTTP_ENCODING'));
                if ('true' == $brouillon) {
                    $odf->setVars('PathImg', 'brouillon.png');
                }
                self::checkeElement($odf, 'StateControl1', $candidatureMPS->getAssuranceContracte());
                self::checkeElement($odf, 'StateControl2', $candidatureMPS->getObligDeclaration());
                self::checkeElement($odf, 'StateControl5', $candidatureMPS->getAutorisationVerification());
                self::checkeElement($odf, 'StateControl7', $candidatureMPS->getPouvoirEngage());
                if ('autre' == $candidatureMPS->getRepresentantLegal()) {
                    $prenom = $candidatureMPS->getPrenomRepresentantLegal();
                    $nom = $candidatureMPS->getNomRepresentantLegal();
                    $qualite = $candidatureMPS->getQualiteRepresentantLegal();
                } elseif ($candidatureMPS->getRepresentantLegal()) {
                    $represantant = (new Atexo_Entreprise_Leaders())->retrieveLeaderById($candidatureMPS->getRepresentantLegal());
                    if ($represantant) {
                        $prenom = $represantant->getPrenom();
                        $nom = $represantant->getNom();
                        $qualite = $represantant->getQualite();
                    }
                }
                $odf->setVars('PrenomRepresantant', Atexo_Util::atexoHtmlEntitiesDecode($prenom), true, Atexo_Config::getParameter('HTTP_ENCODING'));
                $odf->setVars('NomRepresantant', Atexo_Util::atexoHtmlEntitiesDecode($nom), true, Atexo_Config::getParameter('HTTP_ENCODING'));
                $odf->setVars('QualiteRepresantant', Atexo_Util::atexoHtmlEntitiesDecode($qualite), true, Atexo_Config::getParameter('HTTP_ENCODING'));
                $segment = $odf->setSegment('ligneExcercice');
                $arrayCapacites = $candidatureMPS->getCapacites();
                if (is_array($arrayCapacites) && count($arrayCapacites) > 0) {
                    foreach ($arrayCapacites as $oneCapacity) {
                        $segment->setVars('Annee', Atexo_Util::httpEncodingToUtf8($oneCapacity->getAnneeCloture()), true, Atexo_Config::getParameter('HTTP_ENCODING'));
                        $segment->setVars('DateDu', Atexo_Util::httpEncodingToUtf8($oneCapacity->getDateDebut()), true, Atexo_Config::getParameter('HTTP_ENCODING'));
                        $segment->setVars('DateAu', Atexo_Util::httpEncodingToUtf8($oneCapacity->getDateFin()), true, Atexo_Config::getParameter('HTTP_ENCODING'));
                        $segment->setVars('ChiffreAffaire', Atexo_Util::httpEncodingToUtf8($oneCapacity->getChiffreAffaire()).' '.Prado::localize('DEFINE_EURO_HT'), true, Atexo_Config::getParameter('HTTP_ENCODING'));
                        $segment->setVars('PourcentSignificatif', Atexo_Util::httpEncodingToUtf8($oneCapacity->getSignificatif()), true, Atexo_Config::getParameter('HTTP_ENCODING'));
                        $segment->setVars('Commentaire', Atexo_Util::httpEncodingToUtf8($oneCapacity->getCommentaire()), true, Atexo_Config::getParameter('HTTP_ENCODING'));
                        $segment->merge();
                    }
                }
                $odf->mergeSegment($segment);
                $segmentLot = $odf->setSegment('ligneLot');
                $segmentBlocObjetCandidature = $odf->setSegment('blocObjetCandidature');
                $segmentLibelleBlocObjetCandidature = $odf->setSegment('libelleBlocObjetCandidature');
                $arrayLots = explode(',', $candidatureMPS->getListeLots());
                if (is_array($arrayLots) && count($arrayLots) > 0) {
                    $lots = (new Atexo_Consultation_Lots())->retrieveListeLotsByArray($candidatureMPS->getRefConsultation(), $candidatureMPS->getOrganisme(), $arrayLots);
                    if (is_array($lots) && count($lots)) {
                        $segmentBlocObjetCandidature->merge();
                        $segmentLibelleBlocObjetCandidature->merge();
                        foreach ($lots as $lot) {
                            $detailLot = Prado::localize('TEXT_LOT').' '.$lot->getLot().' - '.$lot->getDescription();
                            $segmentLot->setVars('detailLot', Atexo_Util::httpEncodingToUtf8($detailLot), true, Atexo_Config::getParameter('HTTP_ENCODING'));
                            $segmentLot->merge();
                        }
                    }
                }
                $odf->mergeSegment($segmentBlocObjetCandidature);
                $odf->mergeSegment($segmentLibelleBlocObjetCandidature);
                $odf->mergeSegment($segmentLot);
                $msgSignificatif = Prado::Localize('SIGNIFICATIF_POUR_CONSULTATION');
                $msgCommentaires = Prado::Localize('DEFINE_TEXT_COMMENTAIRES');
                if ($consultation instanceof CommonConsultation && $consultation->getAlloti()) {
                    $msgSignificatif = Prado::Localize('SIGNIFICATIF_POUR_CONSULTATION_ALLOTI');
                    $msgCommentaires = Prado::Localize('DEFINE_TEXT_COMMENTAIRES_CAS_ALLOTI');
                }
                $odf->setVars('Significatif', Atexo_Util::atexoHtmlEntitiesDecode($msgSignificatif), true, Atexo_Config::getParameter('HTTP_ENCODING'));
                $odf->setVars('Commentaires', Atexo_Util::atexoHtmlEntitiesDecode($msgCommentaires), true, Atexo_Config::getParameter('HTTP_ENCODING'));
                $pathFile = Atexo_Config::getParameter('COMMON_TMP').'CandidatureMPS_'.$lang.'_'.session_id().time().'_fichier.odt';
                $odf->saveToDisk($pathFile);

                return $pathFile;
            } else {
                return '';
            }
        }
    }

    /**
     * Permet de checker ou nn la valeur d'un element dans odt.
     *
     * @param Atexo_Odf $odf l'odt,String $codeElement le code de l'element,boolean $checked
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function checkeElement(&$odf, $codeElement, $checked)
    {
        if ($checked) {
            $odf->setVars($codeElement, 'checked', true, Atexo_Config::getParameter('HTTP_ENCODING'));
        } else {
            $odf->setVars($codeElement, '', true, Atexo_Config::getParameter('HTTP_ENCODING'));
        }
    }

    /**
     * Permet de recuperer un objet CommonTCandidatureMps by id offre.
     *
     * @param int    $idOffre   id de l'offre
     * @param string $organisme Acronyme de l'organisme
     *
     * @return CommonTCandidatureMps $candidatureMpsObject objet de CommonTCandidatureMps ayant l'id offre = $idOffre
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function retrieveCandidatureMpsByIdOffre($idOffre, $organisme = null)
    {
        $candidatureQuery = new CommonTCandidatureMpsQuery();
        $candidatureMpsObject = $candidatureQuery->getCandidatureMpsByOffre($idOffre, $organisme);
        if ($candidatureMpsObject instanceof CommonTCandidatureMps) {
            return $candidatureMpsObject;
        }

        return false;
    }

    /**
     * Permet d'ajouter les informations de la candidature MPS pour le ws.
     *
     * @param $id
     * @param $xmlResponse
     * @param $arrayBlob
     * @param $logger
     * @param string $organisme Acronyme de l'organisme connecte au WS
     *
     * @return bool|string un message dans le cas d'erreur
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function setInfoCandidatureMPSForWs($id, &$xmlResponse, &$arrayBlob, &$logger, $organisme = null, $typeChecksum = 'md5'): bool|string
    {
        $logger->info("La récupération de la candidature MPS dont l'id offre= ".$id);
        $candidatureMps = self::retrieveCandidatureMpsByIdOffre($id, $organisme);
        if ($candidatureMps instanceof CommonTCandidatureMps) {
            if (Atexo_Module::isEnabled('MarchePublicSimplifie', $candidatureMps->getOrganisme())) {
                $logger->info('La candidature MPS existe');
                $blobInfo = Atexo_Blob::getBlobById($candidatureMps->getIdBlob(), $candidatureMps->getOrganisme(), 'Candidature_MPS');
                $arrayBlob = [$blobInfo];
                $logger->info("Mettre les informations de la candidature dans l'objet Atexo_Rest_ObjectField_DocumentField");
                $documentField = self::getObjetcFieldFromCandidatureMps($candidatureMps, $blobInfo, $typeChecksum);
                $logger->info("La récupération de l'xml contenant les informations de la candidature");
                $xmlResponse = (new Atexo_Rest_Action_Actions())->getXmlInfosObjectField($documentField);

                return false;
            } else {
                return 'Le module marché public simplifié est désactivé';
            }
        } else {
            return "La candidature MPS dont l'id offre= ".$id." n'existe pas";
        }
    }

    /*
     * Permet de setter les information d'une candidature MPS dans documentField
     *
     * @param CommonTCandidatureMps $candidatureMps
     * @param array $blobInfo
     * @return objet Atexo_Rest_ObjectField_DocumentField
     * @author LEZ <loubna.ezziani@atexo.com>
     * @version 1.0
     * @since esr-2015
     * @copyright Atexo 2015
     */
    public function getObjetcFieldFromCandidatureMps($candidatureMps, $blobInfo, $typeChecksum = 'md5')
    {
        $documentField = new Atexo_Rest_ObjectField_DocumentField();
        if ($candidatureMps instanceof CommonTCandidatureMps) {
            $documentField->setIdFichier($candidatureMps->getIdCandidature());
            $documentField->setNomFichier('Candidature_MPS_'.base64_encode($candidatureMps->getConsultationId()).'_'.base64_encode($candidatureMps->getIdOffre()).'.pdf');
            $documentField->setTailleFichier($candidatureMps->getTailleFichier());
            $documentField->setChecksum(hash($typeChecksum, $blobInfo['content']));
            $documentField->setTypeChecksum(strtoupper($typeChecksum));
            $documentField->setTypeFichier('CANDIDATURE_MPS');
            $documentField->setJetonHorodatage($candidatureMps->getHorodatage());
        }

        return $documentField;
    }
}
