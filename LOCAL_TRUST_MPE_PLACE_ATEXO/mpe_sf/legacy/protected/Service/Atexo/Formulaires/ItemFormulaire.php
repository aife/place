<?php

namespace Application\Service\Atexo\Formulaires;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonItemFormulairePeer;
use Application\Service\Atexo\Atexo_Config;

class Atexo_Formulaires_ItemFormulaire
{
    public function retriveItemsByIdFormulaire($idFormulaire)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonItemFormulairePeer::IDMODELEFORMULAIRE, $idFormulaire);
        $c->addAscendingOrderByColumn(CommonItemFormulairePeer::NUMERO);
        $items = CommonItemFormulairePeer::doSelect($c, $connexionCom);

        return $items;
    }

    public function deleteItems($idFormulaire, $connexionCom)
    {
        $items = self::retriveItemsByIdFormulaire($idFormulaire);
        foreach ($items as $oneItem) {
            if (is_object($oneItem)) {
                $oneItem->delete($connexionCom);
            }
        }
    }
}
