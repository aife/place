<?php

namespace Application\Service\Atexo\Formulaires;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonModeleFormulairePeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;

/**
 * Classe de gestion des modèles de formulaire.
 *
 * @author Maatalla Houriya <houriya.maatalla@atexo.com>
 * @copyright Atexo 2011
 *
 * @version 1.0
 *
 * @since MPE-4.0
 */
class Atexo_Formulaires_ModelesFormulaire
{
    /**
     * Fonction qui retourne la liste des modèles de formulaire liées nom passé en paramètre.
     *
     * @param : $nom le nom du modèle
     *
     * @return : un tableau d'objet modèles de formulaire ou false si aucun élement trouvé
     */
    public function retrieveFormByNom($nom, $org = false)
    {
        if (!$org) {
            $org = Atexo_CurrentUser::getCurrentOrganism();
        }
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonModeleFormulairePeer::NOM, $nom);
        $c->add(CommonModeleFormulairePeer::ORGANISME, $org);
        $modeleFormulaire = CommonModeleFormulairePeer::doSelect($c, $connexionCom);
        if ($modeleFormulaire) {
            return $modeleFormulaire;
        } else {
            return false;
        }
    }

    /**
     * Fonction qui retourne la liste des modèles de formulaire liées à l'id et l'organisme passé en paramètre.
     *
     * @param : $id        l'id du modèle
     * @param : $organisme l'organisme du modèle
     *
     * @return : un tableau d'objet modèles de formulaire ou false si aucun élement trouvé
     */
    public function retrieveModeleFormulaireById($id, $org = false)
    {
        if (!$org) {
            $org = Atexo_CurrentUser::getCurrentOrganism();
        }
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonModeleFormulairePeer::ID, $id);
        $c->add(CommonModeleFormulairePeer::ORGANISME, $org);
        $modeleFormulaire = CommonModeleFormulairePeer::doSelectOne($c, $connexionCom);
        if ($modeleFormulaire) {
            return $modeleFormulaire;
        } else {
            return false;
        }
    }

    /**
     * Fonction qui retourne la liste des modèles de formulaire liées à la référence  passée en paramètre.
     *
     * @param : $reference la référence du modèle
     *
     * @return : un tableau d'objet modèles de formulaire ou false si aucun élement trouvé
     */
    public function retrieveModeleFormulaireByref($consultationId)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonModeleFormulairePeer::CONSULTATION_ID, $consultationId);
        $modeleFormulaire = CommonModeleFormulairePeer::doSelect($c, $connexionCom);
        if ($modeleFormulaire) {
            return $modeleFormulaire;
        } else {
            return false;
        }
    }
}
