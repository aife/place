<?php

namespace Application\Service\Atexo\Marche;

/**
 * Classe contenant le détail d'un marche.
 *
 * @author Thibaut DECAUDAIN <thibaut.decaudain@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Marche_CriteriaVo
{
    // base de données
    private $_acronymOrganisme;

    // Champs de la table Marche
    private int $_idService = 0;
    private int $_numeroMarcheAnnee = 0;
    private int $_idMarcheTrancheBudgetaire = 0;
    private string $_natureMarche = '';
    private string $_nomAttributaire = '';
    private string $_valide = '0';
    private int $_callFromEntreprise = 0;

    private string $_motclef = '';

    private bool $_sortDefault = false;

    private string $_date1 = '';
    private string $_date2 = '';

    private int $offset = 0;
    private int $limit = 0;
    private string $sortByElement = '';
    private string $sensOrderBy = '';
    private bool $_exportXls = false;

    public function setIdService($idService)
    {
        $this->_idService = $idService;
    }

    public function getIdService()
    {
        return $this->_idService;
    }

    public function setIdsService($idsService)
    {
        $this->_idsService = $idsService;
    }

    public function getIdsService()
    {
        return $this->_idsService;
    }

    public function setNumeroMarcheAnnee($numeroMarcheAnnee)
    {
        $this->_numeroMarcheAnnee = $numeroMarcheAnnee;
    }

    public function getNumeroMarcheAnnee()
    {
        return $this->_numeroMarcheAnnee;
    }

    public function setIdMarcheTrancheBudgetaire($id)
    {
        $this->_idMarcheTrancheBudgetaire = $id;
    }

    public function getIdMarcheTrancheBudgetaire()
    {
        return $this->_idMarcheTrancheBudgetaire;
    }

    public function setNatureMarche($idNature)
    {
        $this->_natureMarche = $idNature;
    }

    public function getNatureMarche()
    {
        return $this->_natureMarche;
    }

    public function getNomAttributaire()
    {
        return $this->_nomAttributaire;
    }

    public function setNomAttributaire($nom)
    {
        $this->_nomAttributaire = $nom;
    }

    public function getDate1()
    {
        return $this->_date1;
    }

    public function setDate1($date)
    {
        $this->_date1 = $date;
    }

    public function getDate2()
    {
        return $this->_date2;
    }

    public function setDate2($date)
    {
        $this->_date2 = $date;
    }

    public function setMotclef($motclef)
    {
        $this->_motclef = $motclef;
    }

    public function getMotclef()
    {
        return $this->_motclef;
    }

    public function setSortDefault($bool)
    {
        $this->_sortDefault = $bool;
    }

    public function getSortDefault()
    {
        return $this->_sortDefault;
    }

    public function setValide($valide)
    {
        $this->_valide = $valide;
    }

    public function getValide()
    {
        return $this->_valide;
    }

    public function getOffset()
    {
        return $this->offset;
    }

    public function setOffset($offset)
    {
        $this->offset = $offset;
    }

    public function getLimit()
    {
        return $this->limit;
    }

    public function setLimit($limit)
    {
        $this->limit = $limit;
    }

    public function setAcronymeOrganisme($acronym)
    {
        $this->_acronymOrganisme = $acronym;
    }

    public function getAcronymeOrganisme()
    {
        return $this->_acronymOrganisme;
    }

    public function getSortByElement()
    {
        return $this->sortByElement;
    }

    public function setSortByElement($sortByElement)
    {
        $this->sortByElement = $sortByElement;
    }

    public function getSensOrderBy()
    {
        return $this->sensOrderBy;
    }

    public function setSensOrderBy($sensOrderBy)
    {
        $this->sensOrderBy = $sensOrderBy;
    }

    public function setCallFromEntreprise($callFromEntreprise)
    {
        $this->_callFromEntreprise = $callFromEntreprise;
    }

    public function getCallFromEntreprise()
    {
        return $this->_callFromEntreprise;
    }

    public function setExportXls($bool)
    {
        $this->_exportXls = $bool;
    }

    public function getExportXls()
    {
        return $this->_exportXls;
    }
}
