<?php

namespace Application\Service\Atexo\Marche;

use Application\Service\Atexo\Atexo_Config;

/**
 * Classe affichant la liste des années.
 *
 * @author Othmane ABISOUROUR <othmane.abisourour@atexo.com>
 * @copyright Atexo 2009
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Marche_Annee
{
    public function displayAnnee($panelChoixAnnee, $panelEtapeResultats, $value)
    {
        $this->$panelChoixAnnee->setVisible(true);
        $this->$panelEtapeResultats->setVisible(false);

        $listeAnneesVisibles = [date('Y', strtotime('-1Year')) => date('Y', strtotime('-1Year')), date('Y', strtotime('-2Year')) => date('Y', strtotime('-2Year')), date('Y', strtotime('-3Year')) => date('Y', strtotime('-3Year'))];
        $this->$value->setDataSource($listeAnneesVisibles);
        $this->$value->dataBind();
    }

    public function getYearToDisplay()
    {
        $dataYear = [];
        $anneDebut = date('Y');
        $dateFin = Atexo_Config::getParameter('ANNEE_FIN_AFFICHAGE_PUBLICATION_CONTRAT');
        for ($i = $anneDebut; $i >= ($dateFin); --$i) {
            $dataYear[$i] = $i;
        }

        return $dataYear;
    }
}
