<?php

namespace Application\Service\Atexo;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonEnchereEntreprisePmi;
use Application\Propel\Mpe\CommonEncherePmiPeer;
use Application\Propel\Mpe\CommonEnchereReference;
use Application\Propel\Mpe\CommonEnchereTrancheBaremeReference;
use Application\Propel\Mpe\CommonEnchereTranchesBaremeNETC;
use Application\Propel\Mpe\CommonEnveloppePeer;
use Application\Propel\Mpe\CommonOffresPeer;
use Application\Propel\Mpe\EntreprisePeer;
use Application\Propel\Mpe\Om\BaseCommonEncherePmiPeer;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Responses;
use Application\Service\Atexo\Enchere\Atexo_Enchere_CriteriaVo;

/**
 * Classe de manipulation des encheres.
 *
 * @author Thibaut DECAUDAIN <thibaut.decaudain@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Enchere
{
    public static function populateEntrepriseEnchere($consultationId, $idEnchere)
    {
        // SELECT des entreprises ayant répondus (dont les enveloppes d'offre on été ouvertes)
        $c = new Criteria();
        $connexionCommon = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $c->add(CommonOffresPeer::CONSULTATION_ID, $consultationId, Criteria::EQUAL);
        $c->add(CommonOffresPeer::ORGANISME, Atexo_CurrentUser::getCurrentOrganism(), Criteria::EQUAL);
        $c->addJoin(CommonEnveloppePeer::OFFRE_ID, CommonOffresPeer::ID);
        $c->addAlias('Offres', CommonOffresPeer::TABLE_NAME);
        $c->addJoin(CommonEnveloppePeer::ORGANISME, CommonOffresPeer::alias('Offres', CommonOffresPeer::ORGANISME));
        //      $c->addJoin(CommonEnveloppePeer::ORGANISME,CommonOffresPeer::ORGANISME);
        $c->add(CommonEnveloppePeer::TYPE_ENV, 2, Criteria::EQUAL);
        $c->add(CommonEnveloppePeer::DATEHEURE_OUVERTURE, '0000-00-00 00:00:00', Criteria::NOT_EQUAL);
        $c->setDistinct();
        $res = CommonOffresPeer::doSelect($c, $connexionCommon);

        // On ajoute chaque entreprise trouvée à la base EntrepriseEnchere
        foreach ($res as $rowOffre) {
            $Entreprise = EntreprisePeer::retrieveByPK($rowOffre->getEntrepriseId(), $connexionCommon);

            $enchereEntreprise = new CommonEnchereEntreprisePmi();
            $enchereEntreprise->setEmail(trim($Entreprise->getEmail()));
            $enchereEntreprise->setNom($Entreprise->getNom());
            $enchereEntreprise->setIdenchere($idEnchere);
            $enchereEntreprise->setIdentreprise($Entreprise->getId()); // TODO : pas implementé ds base...
            $enchereEntreprise->setIsselectionnee(true);
            $enchereEntreprise->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());

            $enchereEntreprise->save($connexionCommon);
        }

        return true;
    }

    public static function getEntreprisesConsultation($consultationId, $selected, $sousPli)
    {
        $res = (new Atexo_Consultation_Responses())->retrieveReponse($consultationId, $sousPli, Atexo_CurrentUser::getCurrentOrganism());

        $entreprises = [];
        foreach ($res as $rowOffre) {
            $entreprise = new CommonEnchereEntreprisePmi();
            $entreprise->setIdentreprise($rowOffre->getEntrepriseId());
            $entreprise->setNom($rowOffre->getNomEntrepriseInscrit());
            $entreprise->setEmail(trim($rowOffre->getMailsignataire()));
            $entreprise->setIsselected($selected);
            $entreprise->setIdenchere($idEnchere);
            $entreprise->setIsfromconsultation(true);
            $entreprise->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());

            $entreprises[$rowOffre->getEntrepriseId()] = $entreprise;
        }

        return $entreprises;
    }

    public function search(Atexo_Enchere_CriteriaVo $criteriaVo)
    {
        $select = 'SELECT * FROM EncherePmi ';
        $join = '';
        $otherQueries = '';
        $orderBy = '';

        if ($criteriaVo->getRefConsultation()) {
            $refConsultation = (new Atexo_Db())->quote('%'.$criteriaVo->getRefConsultation().'%');
            $join .= ' JOIN consultation ON consultation.id = EncherePmi.consultation_id AND consultation.organisme = EncherePmi.organisme ';
            $otherQueries .= " AND consultation.reference_utilisateur LIKE '".$refConsultation."' ";
        }
        if ($criteriaVo->getAcronymeOrganisme()) {
            $organisme = (new Atexo_Db())->quote($criteriaVo->getAcronymeOrganisme());
            $otherQueries .= " AND EncherePmi.organisme = '".$organisme."' ";
        }
        if ($criteriaVo->getReferenceUtilisateur()) {
            $refUtilisateur = (new Atexo_Db())->quote('%'.$criteriaVo->getReferenceUtilisateur().'%');
            $otherQueries .= " AND EncherePmi.referenceUtilisateur LIKE '".$refUtilisateur."' ";
        }

        if ($criteriaVo->getMotclef()) {
            $motClef = (new Atexo_Db())->quote('%'.$criteriaVo->getMotclef().'%');
            $otherQueries .= " AND (EncherePmi.referenceUtilisateur LIKE '".$motClef."' 
            					OR EncherePmi.objet LIKE '".$motClef."') ";
        }

        if ($criteriaVo->getDateDebut1()) {
            $dateDebut1 = (new Atexo_Db())->quote($criteriaVo->getDateDebut1());
            $otherQueries .= " AND EncherePmi.dateDebut >= '".$dateDebut1."' ";
        }

        if ($criteriaVo->getDateDebut2()) {
            $dateDebut2 = (new Atexo_Db())->quote($criteriaVo->getDateDebut2());
            $otherQueries .= " AND EncherePmi.dateDebut <= '".$dateDebut2."' ";
        }
        if ($criteriaVo->getDateFin1()) {
            $dateFin1 = (new Atexo_Db())->quote($criteriaVo->getDateFin1());
            $otherQueries .= " AND EncherePmi.dateFin >= '".$dateFin1."' ";
        }
        if ($criteriaVo->getDateFin2()) {
            $dateFin2 = (new Atexo_Db())->quote($criteriaVo->getDateFin2());
            $otherQueries .= " AND EncherePmi.dateFin <= '".$dateFin2."' ";
        }

        if ($criteriaVo->getStatuts()) {
            if ($criteriaVo->getStatuts() == Atexo_Config::getParameter('ENCHERE_SUSPENDUE')) {
                $otherQueries .= " AND EncherePmi.dateSuspension != '000-00-00 00:00:00' ";
            } elseif ($criteriaVo->getStatuts() == Atexo_Config::getParameter('ENCHERE_EN_ATTENTE')) {
                $otherQueries .= ' AND EncherePmi.dateDebut > NOW() ';
            } elseif ($criteriaVo->getStatuts() == Atexo_Config::getParameter('ENCHERE_CLOTUREE')) {
                $otherQueries .= ' AND EncherePmi.dateFin < NOW() ';
            } elseif ($criteriaVo->getStatuts() == Atexo_Config::getParameter('ENCHERE_EN_COURS')) {
                $otherQueries .= ' AND EncherePmi.dateDebut <= NOW() 
                					AND EncherePmi.dateFin >= NOW() ';
            } else {
                $otherQueries = '';
            }
        }

        //Cas de l'organisation centralisée, on affiche que les encheres du service de l'agent ou ceux des services descendants
        if (!Atexo_Module::isEnabled('OrganisationCentralisee', $criteriaVo->getAcronymeOrganisme())) {
            if ($criteriaVo->getIdsServices()) {
                $otherQueries .= ' AND ( EncherePmi.service_id IN  '.$criteriaVo->getIdsServices();
                if (null == $criteriaVo->getIdService()) {
                    $otherQueries .= ' OR EncherePmi.service_id IS NULL ';
                }
                $otherQueries .= ') ';
            }
        } else {
            if (null == $criteriaVo->getIdService()) {
                $otherQueries .= ' AND EncherePmi.service_id IS NULL ';
            } else {
                $otherQueries .= ' AND EncherePmi.service_id = '.$criteriaVo->getIdService();
            }
        }

        if ($criteriaVo->getSensOrderBy()) {
            $orderBy .= ' ORDER BY EncherePmi.'.$criteriaVo->getSortByElement().' '.$criteriaVo->getSensOrderBy();
        }

        $finalQuery = $select.$join.' WHERE 1 '.$otherQueries.$orderBy;

        if ($criteriaVo->getLimit()) {
            $finalQuery .= ' LIMIT '.$criteriaVo->getOffset().','.$criteriaVo->getLimit();
        }

        $stmt = Atexo_Db::getLinkCommon(true)->prepare($finalQuery);
        $stmt->execute();
        $arrayEncheres = BaseCommonEncherePmiPeer::populateObjects($stmt);

        return $arrayEncheres;
    }

    /*
     * Récuperation de l'enchere par la reference utilisateur
     */
    public static function retrieveEnchereByRefUtilisateur($refUtilisateur)
    {
        $c = new Criteria();
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonEncherePmiPeer::REFERENCEUTILISATEUR, $refUtilisateur, Criteria::EQUAL);
        $res = CommonEncherePmiPeer::doSelectOne($c, $connexion);
        if ($res) {
            return $res;
        } else {
            return false;
        }
    }

    /**
     * Charge un objet EncherePmi par un objet ParametrageEnchere.
     *
     * @param $$idService : identifiant du service de l'agent connecté
     * @param $encherePmi : objet EncherePmi
     *
     * @return $encherePmi : objet EncherePmi chargé
     */
    public function fillObjetEncherePmi($encherePmi, $idParametrageEnchere)
    {
        $sql = "SELECT * FROM `Parametrage_Enchere` WHERE `id` ='".(new Atexo_Db())->quote($idParametrageEnchere)."' AND organisme='".Atexo_CurrentUser::getCurrentOrganism()."'";
        //$statement = Atexo_Db::getLinkCommon(false)->query($sql, PDO::FETCH_ASSOC);
        $statement = Atexo_Db::getLinkCommon(true)->query($sql);
        foreach ($statement as $res) {
            foreach ($res as $key => $value) {
                $list = explode('_', $key);
                $fct = '';
                foreach ($list as $l) {
                    if ('id' == $l) {
                        break;
                    }
                    $fct .= ucfirst($l);
                }
                if ('' != $fct && strlen($fct) > 2 && 'NoteEntreprises' != $fct) {
                    $set = 'set'.$fct;
                    $encherePmi->$set($value);
                }
            }
        }

        return $encherePmi;
    }

    /**
     * Charge un objet EncherePmi par un objet ParametrageEnchere.
     *
     * @param $idEnchere : identifiant de l'enchère
     * @param $enchereReference : objet EnchereReference
     *
     * @return $enchereReference : objet EnchereReference chargé
     */
    public function fillObjetEnchereReference($idEnchere)
    {
        $sql = "SELECT * FROM `Parametrage_Enchere_Reference` WHERE `idEnchere` ='".(new Atexo_Db())->quote($idEnchere)."' AND organisme='".Atexo_CurrentUser::getCurrentOrganism()."'";

        $statement = Atexo_Db::getLinkCommon(true)->query($sql);
        $arrayEnchereReference = [];
        foreach ($statement as $res) {
            $enchereReference = new CommonEnchereReference();
            foreach ($res as $key => $value) {
                $list = explode('_', $key);
                $fct = '';
                foreach ($list as $l) {
                    $fct .= ucfirst($l);
                }
                if (('' != $fct && strlen($fct) > 2)) {
                    $set = 'set'.$fct;
                    $enchereReference->$set($value);
                }
            }
            $arrayEnchereReference[] = $enchereReference;
        }

        return $arrayEnchereReference;
    }

    /**
     * Charge un objet EncherePmi par un objet ParametrageEnchere.
     *
     * @param $idEnchere : identifiant de l'enchère
     *
     * @return $enchereTranchesBaremeNetcs : objet EnchereTranchesBaremeNETC chargé
     */
    public function fillObjetEnchereTranchesBaremeNetcs($idEnchere)
    {
        $sql = "SELECT * FROM `Parametrage_Enchere_Tranches_Bareme_NETC` WHERE `idEnchere` ='".(new Atexo_Db())->quote($idEnchere)."' AND organisme='".Atexo_CurrentUser::getCurrentOrganism()."'";

        $statement = Atexo_Db::getLinkCommon(true)->query($sql);
        $arrayTranchesBaremeNetc = [];
        foreach ($statement as $res) {
            $enchereTranchesBaremeNetcs = new CommonEnchereTranchesBaremeNETC();
            foreach ($res as $key => $value) {
                $list = explode('_', $key);
                $fct = '';
                foreach ($list as $l) {
                    $fct .= ucfirst($l);
                }
                if (('' != $fct && strlen($fct) > 2)) {
                    $set = 'set'.$fct;
                    $enchereTranchesBaremeNetcs->$set($value);
                }
            }
            $arrayTranchesBaremeNetc[] = $enchereTranchesBaremeNetcs;
        }

        return $arrayTranchesBaremeNetc;
    }

    /**
     * Charge un objet EncherePmi par un objet ParametrageEnchere.
     *
     * @param $idEnchere : identifiant de l'enchère
     *
     * @return $enchereTranchesBaremeNetcs : objet EnchereTranchesBaremeNETC chargé
     */
    public function fillObjetEnchereTranchesBaremeReference($idReference)
    {
        $sql = "SELECT * FROM `Parametrage_Enchere_Tranche_Bareme_Reference` WHERE `idReference` ='".(new Atexo_Db())->quote($idReference)."' AND organisme='".Atexo_CurrentUser::getCurrentOrganism()."'";

        $statement = Atexo_Db::getLinkCommon(true)->query($sql);
        $arrayTranchesBaremeRef = [];
        foreach ($statement as $res) {
            $enchereTranchesBaremeRef = new CommonEnchereTrancheBaremeReference();
            foreach ($res as $key => $value) {
                $list = explode('_', $key);
                $fct = '';
                foreach ($list as $l) {
                    $fct .= ucfirst($l);
                }
                if (('' != $fct && strlen($fct) > 2)) {
                    $set = 'set'.$fct;
                    $enchereTranchesBaremeRef->$set($value);
                }
            }
            $arrayTranchesBaremeRef[] = $enchereTranchesBaremeRef;
        }

        return $arrayTranchesBaremeRef;
    }
}
