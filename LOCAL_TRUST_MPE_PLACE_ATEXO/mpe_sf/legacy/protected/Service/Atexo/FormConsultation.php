<?php

namespace Application\Service\Atexo;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonCategorieLot;
use Application\Propel\Mpe\CommonCategorieLotPeer;
use Application\Propel\Mpe\CommonConsultationFormulaire;
use Application\Propel\Mpe\CommonConsultationFormulairePeer;
use Application\Propel\Mpe\CommonEnveloppeFormulaireConsultation;
use Application\Propel\Mpe\CommonEnveloppeFormulaireConsultationPeer;
use Application\Propel\Mpe\CommonEnveloppeItemFormulaireConsultationValues;
use Application\Propel\Mpe\CommonEnveloppeItemFormulaireConsultationValuesPeer;
use Application\Propel\Mpe\CommonItemFormulaireConsultationPeer;
use Application\Propel\Mpe\CommonReponseInscritFormulaireConsultation;
use Application\Propel\Mpe\CommonReponseInscritFormulaireConsultationPeer;
use Application\Propel\Mpe\CommonReponseInscritItemFormulaireConsultationValues;
use Application\Propel\Mpe\CommonReponseInscritItemFormulaireConsultationValuesPeer;
use Prado\Prado;

/*
 * Created on 3 oct. 2011
 *
 * by BOUKATAYA Fatima Zahra
 */

class Atexo_FormConsultation
{
    public function deleteCommonFormsConsultation($organisme, $consultationId, $connexion)
    {
        $c = new Criteria();
        $c->add(CommonConsultationFormulairePeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonConsultationFormulairePeer::ORGANISME, $organisme);
        CommonConsultationFormulairePeer::doDelete($c, $connexion);
    }

    public function isFormAccesExists($organisme, $consultationId, $typeEnveloppe, $typeForm)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonConsultationFormulairePeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonConsultationFormulairePeer::ORGANISME, $organisme);
        $c->add(CommonConsultationFormulairePeer::TYPE_ENVELOPPE, $typeEnveloppe);
        $c->add(CommonConsultationFormulairePeer::TYPE_FORMULAIRE, $typeForm);
        $formAccesAgnt = CommonConsultationFormulairePeer::doSelectOne($c, $connexion);
        if ($formAccesAgnt) {
            return true;
        }

        return false;
    }

    public function getListeFormsByTypeEnveloppe($consultationId, $organisme, $typeEnv, $others = false)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonConsultationFormulairePeer::CONSULTATION_ID, $consultationId, Criteria::EQUAL);
        $c->add(CommonConsultationFormulairePeer::ORGANISME, $organisme, Criteria::EQUAL);
        $c->addAscendingOrderByColumn(CommonConsultationFormulairePeer::LOT);
        $c->addAscendingOrderByColumn(CommonConsultationFormulairePeer::TYPE_ENVELOPPE);
        if ($others) {
            $c->add(CommonConsultationFormulairePeer::TYPE_ENVELOPPE, $typeEnv, Criteria::NOT_EQUAL);
        } else {
            $c->add(CommonConsultationFormulairePeer::TYPE_ENVELOPPE, $typeEnv, Criteria::EQUAL);
        }
        $c->add(CommonConsultationFormulairePeer::TYPE_FORMULAIRE, Atexo_Config::getParameter('SAISIE_UNITAIRE_DE_FICHIERS'), Criteria::NOT_EQUAL);
        $c->add(CommonConsultationFormulairePeer::TYPE_FORMULAIRE, Atexo_Config::getParameter('ACTE_D_ENGAGEMENT_DISSOCIE'), Criteria::NOT_EQUAL);
        $arrayForms = CommonConsultationFormulairePeer::doSelect($c, $connexionCom);

        return $arrayForms;
    }

    public function getDescriptionLot($lot, $organisme, $consultationId)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonCategorieLotPeer::CONSULTATION_ID, $consultationId, Criteria::EQUAL);
        $c->add(CommonCategorieLotPeer::ORGANISME, $organisme, Criteria::EQUAL);
        $c->add(CommonCategorieLotPeer::LOT, $lot, Criteria::EQUAL);
        $categorieLot = CommonCategorieLotPeer::doSelectOne($c, $connexionCom);
        if ($categorieLot instanceof CommonCategorieLot) {
            return $categorieLot->getDescriptionTraduite();
        }
    }

    public function retrieveFormulaireConsultationById($id, $org = false)
    {
        if (!$org) {
            $org = Atexo_CurrentUser::getCurrentOrganism();
        }
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonConsultationFormulairePeer::ID, $id);
        $c->add(CommonConsultationFormulairePeer::ORGANISME, $org);
        $consultationFormulaire = CommonConsultationFormulairePeer::doSelectOne($c, $connexionCom);
        if ($consultationFormulaire) {
            return $consultationFormulaire;
        } else {
            return false;
        }
    }

    public function updateStatutFormulaireConsultation($statut, $id, $org = false)
    {
        $formCons = self::retrieveFormulaireConsultationById($id, $org);
        if ($formCons) {
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $formCons->setStatut($statut);
            $formCons->save($connexionCom);

            return true;
        }

        return false;
    }

    public function retreiveItemsByIdFormulaire($idFormulaireConsultation)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonItemFormulaireConsultationPeer::IDFORMULAIRECONSULTATION, $idFormulaireConsultation);
        $c->addAscendingOrderByColumn(CommonItemFormulaireConsultationPeer::NUMERO);
        $items = CommonItemFormulaireConsultationPeer::doSelect($c, $connexionCom);

        return $items;
    }

    public function deleteItems($idFormulaireConsultation, $connexionCom)
    {
        $items = self::retreiveItemsByIdFormulaire($idFormulaireConsultation);
        foreach ($items as $oneItem) {
            if (is_object($oneItem)) {
                $oneItem->delete($connexionCom);
            }
        }
    }

    public function getListItemsByIdFormConsultation($idFormCons, $evaluation = false, $idEnv = null, $idInscrit = null)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonItemFormulaireConsultationPeer::IDFORMULAIRECONSULTATION, $idFormCons, Criteria::EQUAL);
        $arrayItems = CommonItemFormulaireConsultationPeer::doSelect($c, $connexionCom);
        $results = [];
        if ($evaluation) {
            if ($idEnv) {
                $c = new Criteria();
                $c->add(CommonEnveloppeFormulaireConsultationPeer::ID_FORMULAIRE_CONSULTATION, $idFormCons, Criteria::EQUAL);
                $c->add(CommonEnveloppeFormulaireConsultationPeer::ID_ENVELOPPE, $idEnv, Criteria::EQUAL);
                $envFormCons = CommonEnveloppeFormulaireConsultationPeer::doSelectOne($c, $connexionCom);
                if ($envFormCons instanceof CommonEnveloppeFormulaireConsultation) {
                    foreach ($arrayItems as $one) {
                        $itemenvValues = self::retreiveEnvItemformconsValuesByIdFormConsItem($one->getId(), $envFormCons->getId());
                        if ($itemenvValues instanceof CommonEnveloppeItemFormulaireConsultationValues) {
                            $one->setValeur($itemenvValues->getValeur());
                            $one->setTva($itemenvValues->getTva());
                            $one->setPrixUnitaireValue($itemenvValues->getPrixUnitaire());
                            $one->setValuePrecisionentreprise($itemenvValues->getPrecisionentreprise());
                        }
                        $results[] = $one;
                    }
                }
            } else {
                $results = $arrayItems;
            }
        }

        if ($idInscrit) {
            $c = new Criteria();
            $c->add(CommonReponseInscritFormulaireConsultationPeer::ID_FORMULAIRE_CONSULTATION, $idFormCons, Criteria::EQUAL);
            $c->add(CommonReponseInscritFormulaireConsultationPeer::ID_INSCRIT, $idInscrit, Criteria::EQUAL);
            $inscritFormCons = CommonReponseInscritFormulaireConsultationPeer::doSelectOne($c, $connexionCom);
            //             var_dump($inscritFormCons);exit;
            if ($inscritFormCons instanceof CommonReponseInscritFormulaireConsultation) {
                foreach ($arrayItems as $one) {
                    $itemValues = self::retreiveItemformconsValuesByIdFormConsItem($one->getId(), $inscritFormCons->getId());
                    if ($itemValues instanceof CommonReponseInscritItemFormulaireConsultationValues) {
                        $one->setValeur($itemValues->getValeur());
                        $one->setTva($itemValues->getTva());
                        $one->setPrixUnitaireValue($itemValues->getPrixUnitaire());
                        $one->setValuePrecisionentreprise($itemValues->getPrecisionentreprise());
                    }
                    $one->setIdReponseInscritItemFormulaireConsultation($inscritFormCons->getId());
                    $results[] = $one;
                }
            } else {
                $results = $arrayItems;
            }
        } else {
            $results = $arrayItems;
        }

        return $results;
    }

    public function retreiveItemformconsValuesByIdFormConsItem($idItem, $idReponseInscrit)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonReponseInscritItemFormulaireConsultationValuesPeer::IDITEMFORMULAIRECONSULTATION, $idItem, Criteria::EQUAL);
        $c->add(CommonReponseInscritItemFormulaireConsultationValuesPeer::IDREPONSEINSCRITITEMFORMULAIRECONSULTATION, $idReponseInscrit, Criteria::EQUAL);
        $itemValues = CommonReponseInscritItemFormulaireConsultationValuesPeer::doSelectOne($c, $connexionCom);

        return $itemValues ?: false;
    }

    public function retreiveEnvItemformconsValuesByIdFormConsItem($idItem, $idEnveloppeFormulaireConsultation)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonEnveloppeItemFormulaireConsultationValuesPeer::IDITEMFORMULAIRECONSULTATION, $idItem, Criteria::EQUAL);
        $c->add(CommonEnveloppeItemFormulaireConsultationValuesPeer::IDENVELOPPEFORMULAIRECONSULTATION, $idEnveloppeFormulaireConsultation, Criteria::EQUAL);
        $itemEnvValues = CommonEnveloppeItemFormulaireConsultationValuesPeer::doSelectOne($c, $connexionCom);

        return $itemEnvValues ?: false;
    }

    public function updateStatutFormCons($statut, $idFormCons, $idInscrit, $idEntreprise, $totalHt = false)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $reponseFormCons = self::retreiveReponseInscritformulaireCons($idFormCons, $idInscrit);
        if (!$reponseFormCons) {
            $reponseFormCons = new CommonReponseInscritFormulaireConsultation();
        }
        if ($totalHt) {
            $reponseFormCons->setTotalBdHt($totalHt);
        }
        $reponseFormCons->setStatut($statut);
        $reponseFormCons->setIdFormulaireConsultation($idFormCons);
        $reponseFormCons->setIdInscrit($idInscrit);
        $reponseFormCons->setIdEntreprise($idEntreprise);
        $reponseFormCons->save($connexionCom);

        return $reponseFormCons->getId();
    }

    public function updateStatutFormReponseCons($statut, $idFormCons)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $reponseFormCons = CommonReponseInscritFormulaireConsultationPeer::retrieveByPK($idFormCons, $connexionCom);
        if ($reponseFormCons) {
            $reponseFormCons->setStatut($statut);
            $reponseFormCons->save($connexionCom);

            return true;
        }

        return false;
    }

    public function retreiveReponseInscritformulaireCons($idFormCons, $idInscrit)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonReponseInscritFormulaireConsultationPeer::ID_FORMULAIRE_CONSULTATION, $idFormCons, Criteria::EQUAL);
        $c->add(CommonReponseInscritFormulaireConsultationPeer::ID_INSCRIT, $idInscrit, Criteria::EQUAL);
        $reponseFormCons = CommonReponseInscritFormulaireConsultationPeer::doSelectOne($c, $connexionCom);

        return $reponseFormCons ?: false;
    }

    public function retreiveEnveloppeformulaireCons($idFormCons, $idEnv)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonEnveloppeFormulaireConsultationPeer::ID_FORMULAIRE_CONSULTATION, $idFormCons, Criteria::EQUAL);
        $c->add(CommonEnveloppeFormulaireConsultationPeer::ID_ENVELOPPE, $idEnv, Criteria::EQUAL);
        $reponseFormCons = CommonEnveloppeFormulaireConsultationPeer::doSelectOne($c, $connexionCom);

        return $reponseFormCons ?: false;
    }

    public function getPictoStatutForm($formConsultation = false, $idInscrit = false, $calledFrom = false, $statut = false)
    {
        if (!$statut) {
            if ('agent' == $calledFrom) {
                $statut = $formConsultation->getStatut();
            } elseif ('entreprise' == $calledFrom) {
                $formInscrit = self::retreiveReponseInscritformulaireCons($formConsultation->getId(), $idInscrit);
                if ($formInscrit instanceof CommonReponseInscritFormulaireConsultation) {
                    $statut = $formInscrit->getStatut();
                } else {
                    $statut = Atexo_Config::getParameter('STATUT_BROUILLON_NON_CONFORME');
                }
            }
        }
        if ($statut == Atexo_Config::getParameter('STATUT_BROUILLON_NON_CONFORME')) {
            return Atexo_Config::getParameter('PICTO_FORM_BROUILLON_NON_CONFORME');
        }
        if ($statut == Atexo_Config::getParameter('STATUT_EN_COURS')) {
            return Atexo_Config::getParameter('PICTO_FORM_EN_COURS');
        }
        if ($statut == Atexo_Config::getParameter('STATUT_FINI_VALIDE')) {
            return Atexo_Config::getParameter('PICTO_FORM_FINI_VALIDE');
        }
    }

    public function getTitleStatutForm($formConsultation = false, $idInscrit = false, $calledFrom = false, $statut = false)
    {
        if (!$statut) {
            if ('agent' == $calledFrom) {
                $statut = $formConsultation->getStatut();
            } elseif ('entreprise' == $calledFrom) {
                $formInscrit = self::retreiveReponseInscritformulaireCons($formConsultation->getId(), $idInscrit);
                if ($formInscrit instanceof CommonReponseInscritFormulaireConsultation) {
                    $statut = $formInscrit->getStatut();
                } else {
                    $statut = Atexo_Config::getParameter('STATUT_BROUILLON_NON_CONFORME');
                }
            }
        }
        if ($statut == Atexo_Config::getParameter('STATUT_BROUILLON_NON_CONFORME')) {
            return Prado::localize('TEXT_BROUILLON_NON_CONFORME');
        }
        if ($statut == Atexo_Config::getParameter('STATUT_EN_COURS')) {
            return Prado::localize('EN_COURS');
        }
        if ($statut == Atexo_Config::getParameter('STATUT_FINI_VALIDE')) {
            return Prado::localize('TEXT_FINI_VALIDE');
        }
    }

    public function getFormsByTypeEnveloppeAndLot($consultationId, $organisme, $typeEnv, $lot, $idInscrit)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonConsultationFormulairePeer::CONSULTATION_ID, $consultationId, Criteria::EQUAL);
        $c->add(CommonConsultationFormulairePeer::ORGANISME, $organisme, Criteria::EQUAL);
        $c->add(CommonConsultationFormulairePeer::LOT, $lot, Criteria::EQUAL);
        $c->add(CommonConsultationFormulairePeer::TYPE_ENVELOPPE, $typeEnv, Criteria::EQUAL);
        $c->addJoin(CommonConsultationFormulairePeer::ID, CommonReponseInscritFormulaireConsultationPeer::ID_FORMULAIRE_CONSULTATION);
        $c->add(CommonReponseInscritFormulaireConsultationPeer::ID_INSCRIT, $idInscrit, Criteria::EQUAL);
        $arrayForms = CommonConsultationFormulairePeer::doSelect($c, $connexionCom);
        foreach ($arrayForms as $one) {
            $one->setTotalBdHt(self::getInscritTotalHtBordereauPrix($one->getId(), $idInscrit));
        }

        return $arrayForms;
    }

    public function getInscritTotalHtBordereauPrix($idFormCons, $idInscrit)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonReponseInscritFormulaireConsultationPeer::ID_FORMULAIRE_CONSULTATION, $idFormCons, Criteria::EQUAL);
        $c->add(CommonReponseInscritFormulaireConsultationPeer::ID_INSCRIT, $idInscrit, Criteria::EQUAL);
        $areponseInscrit = CommonReponseInscritFormulaireConsultationPeer::doSelectOne($c, $connexionCom);
        if ($areponseInscrit instanceof CommonReponseInscritFormulaireConsultation) {
            return $areponseInscrit->getTotalBdHt();
        }
    }

    public function getListeFormConsForInscrit($consultationId, $idIscrit, $typeEnv = false, $lot = false)
    {
        $ArrayFormsInscrit = [];
        $sql = 'select *, ReponseInscritFormulaireConsultation.statut as statutFormInscrit, ReponseInscritFormulaireConsultation.total_bd_ht as totalHtBd, ConsultationFormulaire.id as idFormCons from ConsultationFormulaire , ReponseInscritFormulaireConsultation  '.
               ' where ConsultationFormulaire.id  = ReponseInscritFormulaireConsultation.id_Formulaire_consultation'.
               ' AND ReponseInscritFormulaireConsultation.id_inscrit = '.$idIscrit;
        if ($typeEnv) {
            $sql .= ' AND ConsultationFormulaire.type_enveloppe = '.$typeEnv;
        }
        if ($lot) {
            $sql .= ' AND ConsultationFormulaire.lot  = '.$lot;
        }
        $sql .= ' AND ConsultationFormulaire.consultation_id='.$consultationId;

        $results = Atexo_Db::getLinkCommon(true)->query($sql);
        foreach ($results as $row) {
            $formcons = new CommonConsultationFormulaire();
            $formcons->setId($row['idFormCons']);
            $formcons->setStatut($row['statutFormInscrit']);
            $formcons->setTypeFormulaire($row['type_formulaire']);
            $ArrayFormsInscrit[] = $formcons;
        }

        return $ArrayFormsInscrit;
    }

    public function getFormconsByTypeFormRefConsLotAndTypeEnv($consultationId, $organisme, $typeEnv, $typeForm, $lot)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonConsultationFormulairePeer::CONSULTATION_ID, $consultationId, Criteria::EQUAL);
        $c->add(CommonConsultationFormulairePeer::ORGANISME, $organisme, Criteria::EQUAL);
        $c->add(CommonConsultationFormulairePeer::LOT, $lot, Criteria::EQUAL);
        $c->add(CommonConsultationFormulairePeer::TYPE_ENVELOPPE, $typeEnv, Criteria::EQUAL);
        $c->add(CommonConsultationFormulairePeer::TYPE_FORMULAIRE, $typeForm, Criteria::EQUAL);
        $formCons = CommonConsultationFormulairePeer::doSelectOne($c, $connexionCom);

        return $formCons;
    }

    public function getTotalHtBordereauPrixForFormCons($idFormCons, $idEnv)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonEnveloppeFormulaireConsultationPeer::ID_FORMULAIRE_CONSULTATION, $idFormCons, Criteria::EQUAL);
        $c->add(CommonEnveloppeFormulaireConsultationPeer::ID_ENVELOPPE, $idEnv, Criteria::EQUAL);
        $areponseInscrit = CommonEnveloppeFormulaireConsultationPeer::doSelectOne($c, $connexionCom);
        if ($areponseInscrit instanceof CommonEnveloppeFormulaireConsultation) {
            return $areponseInscrit->getTotalBdHt();
        }
    }

    /*Supprime les les formulaires d'evaluation pat type de formulaire           */

    public function deleteFormsConsultationByTypForm($org, $lot, $typeEnv, $typeForm, $consultationId)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonConsultationFormulairePeer::ORGANISME, $org);
        if ($lot) {
            $c->add(CommonConsultationFormulairePeer::LOT, $lot);
        }
        $c->add(CommonConsultationFormulairePeer::TYPE_ENVELOPPE, $typeEnv);
        $c->add(CommonConsultationFormulairePeer::TYPE_FORMULAIRE, $typeForm);
        $c->add(CommonConsultationFormulairePeer::CONSULTATION_ID, $consultationId);
        CommonConsultationFormulairePeer::doDelete($c, $connexion);
    }

    /*Supprime les les formulaires d'evaluation pat type d'enveloppe             */

    public function deleteFormsConsultationByTypEnv($org, $typeEnv, $consultationId)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonConsultationFormulairePeer::ORGANISME, $org);
        $c->add(CommonConsultationFormulairePeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonConsultationFormulairePeer::TYPE_ENVELOPPE, $typeEnv);
        CommonConsultationFormulairePeer::doDelete($c, $connexion);
    }

    /*Supprime les les formulaires d'evaluation que leurs lots ont éte supprimés */

    public function deleteformConsultationHasntlot($allLots, $consultationId, $org = null)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $chaineLot = [];
        $c = new Criteria();
        foreach ($allLots as $oneLot) {
            $chaineLot[] = $oneLot->getLot();
        }
        $chaineLot[] = '0';
        $query = 'lot not in ('.implode(',', $chaineLot).')';
        $c->add(CommonConsultationFormulairePeer::ORGANISME, $org);
        $c->add(CommonConsultationFormulairePeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonConsultationFormulairePeer::LOT, $query, Criteria::CUSTOM);
        CommonConsultationFormulairePeer::doDelete($c, $connexion); //print_r($connexion);exit;
    }
}
