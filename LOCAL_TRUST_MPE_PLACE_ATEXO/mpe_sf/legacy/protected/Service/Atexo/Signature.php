<?php

namespace Application\Service\Atexo;

use Application\Service\Atexo\Signature\Atexo_Signature_InfosVo;
/*
 * Classe de CurrentUser
 *
 * @author Adil El KANABI <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 * @version 1.0
 * @since MPE-3.0
 * @package library
 * @subpackage Atexo
 */
use AtexoCrypto\Dto\InfosSignature;

class Atexo_Signature
{
    public static function fillInfoVoByInfosSiganture(&$infoVo, $infoSignature)
    {
        if ($infoVo instanceof Atexo_Signature_InfosVo && $infoSignature instanceof InfosSignature) {
            if (0 === $infoSignature->getAbsenceRevocationCRL()) {
                $infoVo->setAbsenceRevocationCRL(true);
            } elseif (2 === $infoSignature->getAbsenceRevocationCRL()) {
                $infoVo->setAbsenceRevocationCRL(false);
            }
            if (0 === $infoSignature->getChaineDeCertificationValide()) {
                $infoVo->setChaineDeCertificationValide(true);
            } elseif (2 === $infoSignature->getChaineDeCertificationValide()) {
                $infoVo->setChaineDeCertificationValide(false);
            }
        }
    }
}
