<?php

namespace Application\Service\Atexo\Contrat;

use Application\Propel\Mpe\CommonTContratTitulaire;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_TrancheBudgetaire;
use Application\Service\Atexo\Atexo_Util;

/**
 * Classe de gestion des controles et traitements métier des contrats.
 *
 * @author YEL <youssef.elalaoui@atexo.com>
 *
 * @version 1.0
 *
 * @since 2015-01.01.02
 *
 * @copyright Atexo 2016
 */
class Atexo_Contrat_Gestion
{
    /**
     * Permet de change les tranches budgetaire selon l'annee et le montant.
     *
     * @param CommonTContratTitulaire $contrat
     *
     * @return void
     *
     * @author youssef EL ALAOUI <youssef.elalaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function changeTrancheBudgetaire($contrat)
    {
        $montant = Atexo_Util::getMontantArronditSansEspace($contrat->getMontantContrat());
        $dateNotif = $contrat->getDateNotification('Y-m-d');
        if (!$dateNotif || '0000-00-00' == $dateNotif) {
            $dateNotif = $contrat->getDatePrevueNotification('Y-m-d');
        }
        if (!$dateNotif || '0000-00-00' == $dateNotif) {
            $dateNotif = $contrat->getDateAttribution('Y-m-d');
        }
        if (!$dateNotif || '0000-00-00' == $dateNotif) {
            $dateNotif = date('Y-m-d');
        }

        $annee = Atexo_Util::getAnneeFromDate($dateNotif);
        if (Atexo_Module::isEnabled('DecisionTrancheBudgetaire') && $annee) {
            $trancheId = (new Atexo_TrancheBudgetaire())->getIdTrancheByMontantAndAnnee($montant, $annee, $contrat->getOrganisme());
            if (!$trancheId) {
                $trancheId = (new Atexo_TrancheBudgetaire())->getIdTrancheByMontant($montant, $contrat->getOrganisme());
            }
            $contrat->setIdTrancheBudgetaire($trancheId);
        }
    }
}
