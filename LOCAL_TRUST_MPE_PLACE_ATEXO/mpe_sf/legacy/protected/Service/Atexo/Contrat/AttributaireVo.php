<?php

namespace Application\Service\Atexo\Contrat;

use Application\Service\Atexo\Entreprise\Atexo_Entreprise_EntrepriseVo;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_EtablissementVo;

/**
 * Classe objet virtuel pour l'attributaire.
 *
 * @author Lobna EZZIANI <loubna.ezziani@atexo.com>
 *
 * @version 1.0
 *
 * @since 2015-roadmap
 *
 * @copyright Atexo 2015
 */
class Atexo_Contrat_AttributaireVo
{
    protected $entrepriseVo;
    protected $etablissementVo;
    protected $nom_contact;
    protected $prenom_contact;
    protected $email_contact;
    protected $telephone_contact;
    protected $fax_contact;
    protected $index;

    /**
     * @return mixed
     */
    public function getEntrepriseVo()
    {
        return $this->entrepriseVo;
    }

    /**
     * @param mixed $entrepriseVo
     */
    public function setEntrepriseVo($entrepriseVo)
    {
        $this->entrepriseVo = $entrepriseVo;
    }

    /**
     * @return mixed
     */
    public function getEtablissementVo()
    {
        return $this->etablissementVo;
    }

    /**
     * @param mixed $etablissementVo
     */
    public function setEtablissementVo($etablissementVo)
    {
        $this->etablissementVo = $etablissementVo;
    }

    /**
     * @return mixed
     */
    public function getNomContact()
    {
        return $this->nom_contact;
    }

    /**
     * @param mixed $nom_contact
     */
    public function setNomContact($nom_contact)
    {
        $this->nom_contact = $nom_contact;
    }

    /**
     * @return mixed
     */
    public function getPrenomContact()
    {
        return $this->prenom_contact;
    }

    /**
     * @param mixed $prenom_contact
     */
    public function setPrenomContact($prenom_contact)
    {
        $this->prenom_contact = $prenom_contact;
    }

    /**
     * @return mixed
     */
    public function getEmailContact()
    {
        return $this->email_contact;
    }

    /**
     * @param mixed $email_contact
     */
    public function setEmailContact($email_contact)
    {
        $this->email_contact = $email_contact;
    }

    /**
     * @return mixed
     */
    public function getTelephoneContact()
    {
        return $this->telephone_contact;
    }

    /**
     * @param mixed $telephone_contact
     */
    public function setTelephoneContact($telephone_contact)
    {
        $this->telephone_contact = $telephone_contact;
    }

    /**
     * @return mixed
     */
    public function getFaxContact()
    {
        return $this->fax_contact;
    }

    /**
     * @param mixed $fax_contact
     */
    public function setFaxContact($fax_contact)
    {
        $this->fax_contact = $fax_contact;
    }

    public function getNomEntreprise()
    {
        $entreprise = $this->getEntrepriseVo();
        if ($entreprise instanceof Atexo_Entreprise_EntrepriseVo) {
            return $entreprise->getNom();
        }

        return '-';
    }

    public function getAdresseEtablissement()
    {
        $etablissement = $this->getEtablissementVo();
        if ($etablissement instanceof Atexo_Entreprise_EtablissementVo) {
            $adresse = $etablissement->getAdresse();
            $adresse2 = $etablissement->getAdresse2();

            return $adresse.' '.$adresse2;
        }

        return '-';
    }

    public function getCodePostalEtablissement()
    {
        $etablissement = $this->getEtablissementVo();
        if ($etablissement instanceof Atexo_Entreprise_EtablissementVo) {
            return $etablissement->getCodePostal();
        }

        return '-';
    }

    public function getVilleEtablissement()
    {
        $etablissement = $this->getEtablissementVo();
        if ($etablissement instanceof Atexo_Entreprise_EtablissementVo) {
            return $etablissement->getVille();
        }

        return '-';
    }

    public function getPaysEtablissement()
    {
        $etablissement = $this->getEtablissementVo();
        if ($etablissement instanceof Atexo_Entreprise_EtablissementVo) {
            return $etablissement->getPays();
        }

        return '-';
    }

    public function getIdentifiantEntrepriseEtablissement()
    {
        $codeEtab = null;
        $codeEntreprise = null;
        $etablissement = $this->getEtablissementVo();
        if ($etablissement instanceof Atexo_Entreprise_EtablissementVo) {
            $codeEtab = ($etablissement->getCodeEtablissement() && '1' !== $etablissement->getCodeEtablissement()) ? $etablissement->getCodeEtablissement() : '';
        }
        $entreprise = $this->getEntrepriseVo();
        if ($entreprise instanceof Atexo_Entreprise_EntrepriseVo) {
            $codeEntreprise = $entreprise->getSiren() ?: ($entreprise->getSirenetranger() ?: '-');
        }

        if ($codeEtab) {
            $codeEntreprise .= ' '.$codeEtab;
        }

        return $codeEntreprise;
    }

    /**
     * @return mixed
     */
    public function getIndex()
    {
        return $this->index;
    }

    /**
     * @param mixed $index
     */
    public function setIndex($index)
    {
        $this->index = $index;
    }
}
