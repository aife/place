<?php

namespace Application\Service\Atexo\Contrat;

/**
 * @author Fatima Ezzahra IZGUA <fatima.izgua@atexo.com>
 * @copyright Atexo 2015
 *
 * @since 4.11.0
 */
class Atexo_Contrat_CriteriaVo
{
    private $numeroContrat;
    private $motsCles;
    private $idContratTitulaire;
    private $typeAttribution = 0;
    private $status = [];
    private $idContratMulti = null;
    private $organisme;
    private $serviceId = null;
    private $idAgentConnecte;
    private $lienAcSad;
    private $contratOrganismeInvite = '1';
    private $types = [];
    private $favorite = 0;
    private $champOrderBy = '';
    private $sensOrderBy = 'ASC';
    private $limit = '';
    private $offset = 0;
    private $count = false;
    private $distinct = false;
    private $findOne = false;
    private $idTitulaire;
    private ?string $referenceLibre = null;
    private ?string $clauseSociales = null;
    private ?string $clausesEnvironnementales = null;
    private ?string $dateNotificationStart = null;
    private ?string $dateNotificationEnd = null;
    private $datePrevNotificationStart;
    private $datePrevNotificationEnd;
    private ?string $dateFinMarcheStart = null;
    private ?string $dateFinMarcheEnd = null;
    private ?string $dateFinMaxMarcheStart = null;
    private ?string $dateFinMaxMarcheEnd = null;
    private $searchModeExact = false;
    private $anneeNotification = false;
    private $idTrancheBudgetaire = false;
    private $montantMin = false;
    private $montantMax = false;
    private $categorie = false;
    private $nomAttributaire = false;
    private $contratClassKey = false;
    private $publicationContrat = false;
    private $dateNotificationNotNull = false;
    private $codeCpv = '';
    private $lieuExecution = '';
    private $statutPublicationSn = '';
    private $concession = '';
    private $searchModule = 0;
    private $uuid = null;
    private $siretAcheteurs = [];
    private $sensEtalabOrderBy = 'ASC';
    private $champEtalabOrderBy = '';

    public function getChampEtalabOrderBy(): string
    {
        return $this->champEtalabOrderBy;
    }

    public function setChampEtalabOrderBy(string $champEtalabOrderBy): void
    {
        $this->champEtalabOrderBy = $champEtalabOrderBy;
    }

    public function getSensEtalabOrderBy(): string
    {
        return $this->sensEtalabOrderBy;
    }

    public function setSensEtalabOrderBy(string $sensEtalabOrderBy): void
    {
        $this->sensEtalabOrderBy = $sensEtalabOrderBy;
    }

    public function getSiretAcheteurs(): array
    {
        return $this->siretAcheteurs;
    }

    public function setSiretAcheteurs(array $siretAcheteurs): void
    {
        $this->siretAcheteurs = $siretAcheteurs;
    }

    /**
     * @return mixed
     */
    public function getMotsCles()
    {
        return $this->motsCles;
    }

    /**
     * @param mixed $motsCles
     */
    public function setMotsCles($motsCles)
    {
        $this->motsCles = $motsCles;
    }

    /**
     * @return mixed
     */
    public function getTypeAttribution()
    {
        return $this->typeAttribution;
    }

    /**
     * @param mixed $typeAttribution
     */
    public function setTypeAttribution($typeAttribution)
    {
        $this->typeAttribution = $typeAttribution;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    /**
     * @param mixed $organisme
     */
    public function setOrganisme($organisme)
    {
        $this->organisme = $organisme;
    }

    /**
     * @return array
     */
    public function getServiceId()
    {
        return $this->serviceId;
    }

    /**
     * @param array $serviceIds
     */
    public function setServiceId($serviceId)
    {
        $this->serviceId = $serviceId;
    }

    /**
     * @return string
     */
    public function getChampOrderBy()
    {
        return $this->champOrderBy;
    }

    /**
     * @param string $champOrderBy
     */
    public function setChampOrderBy($champOrderBy)
    {
        $this->champOrderBy = $champOrderBy;
    }

    /**
     * @return string
     */
    public function getSensOrderBy()
    {
        return $this->sensOrderBy;
    }

    /**
     * @param string $sensOrderBy
     */
    public function setSensOrderBy($sensOrderBy)
    {
        $this->sensOrderBy = $sensOrderBy;
    }

    /**
     * @return string
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @param string $limit
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;
    }

    /**
     * @return int
     */
    public function getOffset()
    {
        return $this->offset;
    }

    /**
     * @param int $offset
     */
    public function setOffset($offset)
    {
        $this->offset = $offset;
    }

    /**
     * @return bool
     */
    public function isCount()
    {
        return $this->count;
    }

    /**
     * @param bool $count
     */
    public function setCount($count)
    {
        $this->count = $count;
    }

    /**
     * @return bool
     */
    public function isDistinct()
    {
        return $this->distinct;
    }

    /**
     * @param bool $distinct
     */
    public function setDistinct($distinct)
    {
        $this->distinct = $distinct;
    }

    /**
     * @return bool
     */
    public function isFindOne()
    {
        return $this->findOne;
    }

    /**
     * @param bool $findOne
     */
    public function setFindOne($findOne)
    {
        $this->findOne = $findOne;
    }

    /**
     * @return null
     */
    public function getIdContratMulti()
    {
        return $this->idContratMulti;
    }

    /**
     * @param null $idContratMulti
     */
    public function setIdContratMulti($idContratMulti)
    {
        $this->idContratMulti = $idContratMulti;
    }

    /**
     * @return mixed
     */
    public function getIdContratTitulaire()
    {
        return $this->idContratTitulaire;
    }

    /**
     * @param mixed $idContratTitulaire
     */
    public function setIdContratTitulaire($idContratTitulaire)
    {
        $this->idContratTitulaire = $idContratTitulaire;
    }

    /**
     * @return int
     */
    public function getFavorite()
    {
        return $this->favorite;
    }

    /**
     * @param int $favorite
     */
    public function setFavorite($favorite)
    {
        $this->favorite = $favorite;
    }

    /**
     * @return mixed
     */
    public function getIdAgentConnecte()
    {
        return $this->idAgentConnecte;
    }

    /**
     * @param mixed $idAgentConnecte
     */
    public function setIdAgentConnecte($idAgentConnecte)
    {
        $this->idAgentConnecte = $idAgentConnecte;
    }

    /**
     * @return mixed
     */
    public function getLienAcSad()
    {
        return $this->lienAcSad;
    }

    /**
     * @param mixed $lienAcSad
     */
    public function setLienAcSad($lienAcSad)
    {
        $this->lienAcSad = $lienAcSad;
    }

    /**
     * @return mixed
     */
    public function getNumeroContrat()
    {
        return $this->numeroContrat;
    }

    /**
     * @param mixed $numeroContrat
     */
    public function setNumeroContrat($numeroContrat)
    {
        $this->numeroContrat = $numeroContrat;
    }

    /**
     * @return array
     */
    public function getTypes()
    {
        return $this->types;
    }

    /**
     * @param array $types
     */
    public function setTypes($types)
    {
        $this->types = $types;
    }

    /**
     * @return bool
     */
    public function isContratOrganismeInvite()
    {
        return $this->contratOrganismeInvite;
    }

    /**
     * @param bool $contratOrganismeInvite
     */
    public function setContratOrganismeInvite($contratOrganismeInvite)
    {
        $this->contratOrganismeInvite = $contratOrganismeInvite;
    }

    /**
     * @return mixed
     */
    public function getIdTitulaire()
    {
        return $this->idTitulaire;
    }

    /**
     * @param mixed $idTitulaire
     */
    public function setIdTitulaire($idTitulaire)
    {
        $this->idTitulaire = $idTitulaire;
    }

    /**
     * @return string
     */
    public function getReferenceLibre()
    {
        return $this->referenceLibre;
    }

    /**
     * @param string $referenceLibre
     */
    public function setReferenceLibre($referenceLibre)
    {
        $this->referenceLibre = $referenceLibre;
    }

    /**
     * @return string
     */
    public function getClauseSociales()
    {
        return $this->clauseSociales;
    }

    /**
     * @param string $clauseSociales
     */
    public function setClauseSociales($clauseSociales)
    {
        $this->clauseSociales = $clauseSociales;
    }

    /**
     * @return string
     */
    public function getClausesEnvironnementales()
    {
        return $this->clausesEnvironnementales;
    }

    /**
     * @param string $clausesEnvironnementales
     */
    public function setClausesEnvironnementales($clausesEnvironnementales)
    {
        $this->clausesEnvironnementales = $clausesEnvironnementales;
    }

    /**
     * @return string
     */
    public function getDateNotificationStart()
    {
        return $this->dateNotificationStart;
    }

    /**
     * @param string $dateNotificationStart
     */
    public function setDateNotificationStart($dateNotificationStart)
    {
        $this->dateNotificationStart = $dateNotificationStart;
    }

    /**
     * @return string
     */
    public function getDateNotificationEnd()
    {
        return $this->dateNotificationEnd;
    }

    /**
     * @param string $dateNotificationEnd
     */
    public function setDateNotificationEnd($dateNotificationEnd)
    {
        $this->dateNotificationEnd = $dateNotificationEnd;
    }

    /**
     * @return mixed
     */
    public function getDatePrevNotificationStart()
    {
        return $this->datePrevNotificationStart;
    }

    /**
     * @param mixed $datePrevNotificationStart
     */
    public function setDatePrevNotificationStart($datePrevNotificationStart)
    {
        $this->datePrevNotificationStart = $datePrevNotificationStart;
    }

    /**
     * @return mixed
     */
    public function getDatePrevNotificationEnd()
    {
        return $this->datePrevNotificationEnd;
    }

    /**
     * @param mixed $datePrevNotificationEnd
     */
    public function setDatePrevNotificationEnd($datePrevNotificationEnd)
    {
        $this->datePrevNotificationEnd = $datePrevNotificationEnd;
    }

    /**
     * @return string
     */
    public function getDateFinMarcheStart()
    {
        return $this->dateFinMarcheStart;
    }

    /**
     * @param string $dateFinMarcheStart
     */
    public function setDateFinMarcheStart($dateFinMarcheStart)
    {
        $this->dateFinMarcheStart = $dateFinMarcheStart;
    }

    /**
     * @return string
     */
    public function getDateFinMarcheEnd()
    {
        return $this->dateFinMarcheEnd;
    }

    /**
     * @param string $dateFinMarcheEnd
     */
    public function setDateFinMarcheEnd($dateFinMarcheEnd)
    {
        $this->dateFinMarcheEnd = $dateFinMarcheEnd;
    }

    /**
     * @return string
     */
    public function getDateFinMaxMarcheStart()
    {
        return $this->dateFinMaxMarcheStart;
    }

    /**
     * @param string $dateFinMaxMarcheStart
     */
    public function setDateFinMaxMarcheStart($dateFinMaxMarcheStart)
    {
        $this->dateFinMaxMarcheStart = $dateFinMaxMarcheStart;
    }

    /**
     * @return string
     */
    public function getDateFinMaxMarcheEnd()
    {
        return $this->dateFinMaxMarcheEnd;
    }

    /**
     * @param string $dateFinMaxMarcheEnd
     */
    public function setDateFinMaxMarcheEnd($dateFinMaxMarcheEnd)
    {
        $this->dateFinMaxMarcheEnd = $dateFinMaxMarcheEnd;
    }

    /**
     * @return bool
     */
    public function getSearchModeExact()
    {
        return $this->searchModeExact;
    }

    /**
     * @param bool $searchModeExact
     */
    public function setSearchModeExact($searchModeExact)
    {
        $this->searchModeExact = $searchModeExact;
    }

    /**
     * @return int
     */
    public function getAnneeNotification()
    {
        return $this->anneeNotification;
    }

    /**
     * @param int $annee
     */
    public function setAnneeNotification($annee)
    {
        $this->anneeNotification = $annee;
    }

    /**
     * @return int
     */
    public function getIdTrancheBudgetaire()
    {
        return $this->idTrancheBudgetaire;
    }

    /**
     * @param int $idTrancheBudgetaire
     */
    public function setIdTrancheBudgetaire($idTrancheBudgetaire)
    {
        $this->idTrancheBudgetaire = $idTrancheBudgetaire;
    }

    /**
     * @return int
     */
    public function getMontantMin()
    {
        return $this->montantMin;
    }

    /**
     * @param int $montantMin
     */
    public function setMontantMin($montantMin)
    {
        $this->montantMin = $montantMin;
    }

    /**
     * @return int
     */
    public function getMontantMax()
    {
        return $this->montantMax;
    }

    /**
     * @param int $montantMax
     */
    public function setMontantMax($montantMax)
    {
        $this->montantMax = $montantMax;
    }

    /**
     * @return int
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * @param int $categorie
     */
    public function setCategorie($categorie)
    {
        $this->categorie = $categorie;
    }

    /**
     * @return string
     */
    public function getNomAttributaire()
    {
        return $this->nomAttributaire;
    }

    /**
     * @param string $nom
     */
    public function setNomAttributaire($nom)
    {
        $this->nomAttributaire = $nom;
    }

    /**
     * @return string
     */
    public function getContratClasskey()
    {
        return $this->contratClassKey;
    }

    /**
     * @param string $contratClassKey
     */
    public function setContratClasskey($contratClassKey)
    {
        $this->contratClassKey = $contratClassKey;
    }

    /**
     * @return string
     */
    public function getPublicationContrat()
    {
        return $this->publicationContrat;
    }

    /**
     * @param string $publicationContrat
     */
    public function setPublicationContrat($publicationContrat)
    {
        $this->publicationContrat = $publicationContrat;
    }

    /**
     * @return string
     */
    public function getDateNotificationNotNull()
    {
        return $this->dateNotificationNotNull;
    }

    /**
     * @param string $dateNotificationNotNull
     */
    public function setDateNotificationNotNull($dateNotificationNotNull)
    {
        $this->dateNotificationNotNull = $dateNotificationNotNull;
    }

    /**
     * @return string
     */
    public function getCodeCpv()
    {
        return $this->codeCpv;
    }

    /**
     * @param string $codeCpv
     */
    public function setCodeCpv($codeCpv)
    {
        $this->codeCpv = $codeCpv;
    }

    /**
     * @return string
     */
    public function getLieuExecution()
    {
        return $this->lieuExecution;
    }

    /**
     * @param string $lieuExecution
     */
    public function setLieuExecution($lieuExecution)
    {
        $this->lieuExecution = $lieuExecution;
    }

    /**
     * @return string
     */
    public function getStatutPublicationSn()
    {
        return $this->statutPublicationSn;
    }

    /**
     * @param string $statutPublicationSn
     */
    public function setStatutPublicationSn($statutPublicationSn)
    {
        $this->statutPublicationSn = $statutPublicationSn;
    }

    /**
     * @return string
     */
    public function getConcession()
    {
        return $this->concession;
    }

    /**
     * @param string $concession
     */
    public function setConcession($concession)
    {
        $this->concession = $concession;
    }

    /**
     * @return int
     */
    public function getSearchModule()
    {
        return $this->searchModule;
    }

    /**
     * @param int $searchModule
     */
    public function setSearchModule($searchModule)
    {
        $this->searchModule = $searchModule;
    }

    /**
     * @param $uuid
     * @return void
     */
    public function setUuid($uuid = null)
    {
        $this->uuid = $uuid;
    }

    /**
     * @return null
     */
    public function getUuid()
    {
        return $this->uuid;
    }

}
