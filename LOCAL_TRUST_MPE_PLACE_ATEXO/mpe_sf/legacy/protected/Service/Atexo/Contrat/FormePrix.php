<?php

namespace Application\Service\Atexo\Contrat;

/**
 * Class FormePrix.
 */
class Atexo_Contrat_FormePrix
{
    const CONTRAT_PRIX_FERME = 'Ferme';
    const CONTRAT_PRIX_FERME_ACTUALISABLE = 'Ferme et actualisable';
    const CONTRAT_PRIX_REVISABLE = 'Révisable';

    public static function getFormes()
    {
        return [
            1 => self::CONTRAT_PRIX_FERME,
            2 => self::CONTRAT_PRIX_FERME_ACTUALISABLE,
            3 => self::CONTRAT_PRIX_REVISABLE,
        ];
    }
}
