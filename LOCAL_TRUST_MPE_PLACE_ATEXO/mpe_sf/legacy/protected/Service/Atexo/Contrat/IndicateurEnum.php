<?php

namespace Application\Service\Atexo\Contrat;

/**
 * Class IndicateurEnum.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 *
 * @since 2017-roadmap
 *
 * @copyright Atexo 2017
 */
abstract class Atexo_Contrat_IndicateurEnum
{
    const CONTRAT_SUPPRESSION_AUTORISEE = 'cas1';
    const CONTRAT_SUPPRESSION_NON_AUTORISEE = 'cas2';
    const CONTRAT_SUPPRESSION_NON_AUTORISEE_AC_OU_SAD_LIE = 'cas3';
    const CONTRAT_SUPPRESSION_NON_AUTORISEE_ENVOI_MAIL = 'cas4';
}
