<?php

namespace Application\Service\Atexo\Xml;

use Application\Service\Atexo\Atexo_Exception;
use Exception;

/**
 * Exception personnalisée du module XML.
 *
 * @author Guillaume Ponçon <guillaume.poncon@openstates.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Xml_Exception extends Atexo_Exception
{
}
