<?php

namespace Application\Service\Atexo\Insee;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonEntrepriseInsee;
use Application\Propel\Mpe\CommonEntrepriseInseePeer;
use Application\Service\Atexo\Atexo_Config;

/**
 * Classe de manipulation des entreprises.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Insee_Entreprise
{
    /**
     * recherche entreprise dans la base insee local.
     *
     * @param $siren
     *
     * @return EntrepriseInsee
     */
    public function retrieveCompanyInseeLocalBySiren($siren)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonEntrepriseInseePeer::SIREN, $siren);
        $c->add(CommonEntrepriseInseePeer::NICSIEGE, '00000', Criteria::NOT_EQUAL);
        $company = CommonEntrepriseInseePeer::doSelectOne($c, $connexionCom);
        if ($company instanceof CommonEntrepriseInsee) {
            return $company;
        } else {
            return false;
        }
    }

    /**
     * recherche entreprise dans la base insee local.
     *
     * @param $siret
     *
     * @return EntrepriseInsee
     */
    public function retrieveCompanyInseeLocalBySiret($siret)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonEntrepriseInseePeer::SIREN, substr($siret, 0, 9));
        $c->add(CommonEntrepriseInseePeer::NIC, substr($siret, 9, 14));
        $company = CommonEntrepriseInseePeer::doSelectOne($c, $connexionCom);
        if ($company instanceof CommonEntrepriseInsee) {
            return $company;
        } else {
            return false;
        }
    }

    /**
     * recherche entreprise par web service.
     *
     * @return xml de reponse
     */
    public function webGetXmlCompanyInsee($siren, $nic = null)
    {
        $xml = null;
        $filePathRequette = Atexo_Config::getParameter('INSEE_FILE_REQUTTE_PATH');
        $filePathCertif = Atexo_Config::getParameter('INSEE_CURLOPT_SSLCERT');
        $password = Atexo_Config::getParameter('INSEE_CURLOPT_SSLCERTPASSWD');
        $content = file_get_contents($filePathRequette);
        if (('' != $siren) && (null == $nic)) {
            $xml = str_replace('<!--SIREN-->', '<siren>'.$siren.'</siren>', $content);
        } elseif (('' != $siren) && (null != $nic)) {
            $xml = str_replace('<!--SIREN-->', '<siret>'.$siren.$nic.'</siret>', $content);
        }
        $url = Atexo_Config::getParameter('INSEE_URL');
        $ch = curl_init();    // initialize curl handle
        curl_setopt($ch, CURLOPT_URL, $url); // set url to post to
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Accept: application/xml', 'Content-Type: application/xml']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSLCERT, $filePathCertif);
        curl_setopt($ch, CURLOPT_SSLCERTPASSWD, $password);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 4); // times out after 4s
        //definition du proxy si existe
        $urlProxy = Atexo_Config::getParameter('URL_PROXY');
        $portProxy = Atexo_Config::getParameter('PORT_PROXY');
        $curlOptProxy = $urlProxy.':'.$portProxy;
        if ('' != $urlProxy) {
            curl_setopt($ch, CURLOPT_PROXY, $curlOptProxy);
        }

        $result = curl_exec($ch); // run the whole process
        if ('' != $result) {
            return $result;
        }
    }

    /**
     * ajouter l'entreprise dans la base insee local.
     */
    public function addCompanyInsee($xmlResponse, $nic = null)
    {
        $xmlObject = simplexml_load_string($xmlResponse);
        // Si l'entreprise n'existe pas en base distante

        if (9 != strlen((string) $xmlObject->resultat->siru->entreprise->siren)) {
            return;
        }
        $newComapny = new CommonEntrepriseInsee();
        $newComapny->setSiren((string) $xmlObject->resultat->siru->entreprise->siren);
        $newComapny->setRaisonsociale((string) $xmlObject->resultat->siru->entreprise->raisonSociale);
        $newComapny->setNbeta((string) $xmlObject->resultat->siru->entreprise->nbEtA);
        $newComapny->setEtat((string) $xmlObject->resultat->siru->entreprise->etat);
        $newComapny->setEtatlib((string) $xmlObject->resultat->siru->entreprise->etatLib);
        $newComapny->setEtatdebdate((string) $xmlObject->resultat->siru->entreprise->etatDebDate);
        $newComapny->setEff3112tr((string) $xmlObject->resultat->siru->entreprise->eff3112Tr);
        $newComapny->setEff3112trlib((string) $xmlObject->resultat->siru->entreprise->eff3112TrLib);
        $newComapny->setEtatdebdate((string) $xmlObject->resultat->siru->entreprise->eff3112TrLib);
        $newComapny->setEffan((string) $xmlObject->resultat->siru->entreprise->effAn);
        $newComapny->setApen((string) $xmlObject->resultat->siru->entreprise->apen);
        $newComapny->setApenLib((string) $xmlObject->resultat->siru->entreprise->apenLib);
        $newComapny->setCj((string) $xmlObject->resultat->siru->entreprise->cj);
        $newComapny->setCjLib(((string) $xmlObject->resultat->siru->entreprise->cjLib));
        $newComapny->setIndndc((string) $xmlObject->resultat->siru->entreprise->indNDC);
        $newComapny->setInddoublon((string) $xmlObject->resultat->siru->entreprise->indDoublon);
        $newComapny->setIndpurge((string) $xmlObject->resultat->siru->entreprise->indPurge);

        $newComapny->setNicsiege((string) $xmlObject->resultat->siru->siege->nicSiege);
        $newComapny->setEtatsiege((string) $xmlObject->resultat->siru->siege->etatSiege);
        $newComapny->setEtatsiegelib((string) $xmlObject->resultat->siru->siege->etatSiegeLib);
        $newComapny->setEff3112trsiege((string) $xmlObject->resultat->siru->siege->eff3112TrSiege);
        $newComapny->setEff3112trsiegelib((string) $xmlObject->resultat->siru->siege->eff3112TrSiegeLib);
        $newComapny->setEffansiege((string) $xmlObject->resultat->siru->siege->effAnSiege);
        $newComapny->setApetsiege((string) $xmlObject->resultat->siru->siege->apetSiege);
        $newComapny->setApetsiegelib((string) $xmlObject->resultat->siru->siege->apetSiegeLib);
        $newComapny->setTrtderdatesiege((string) $xmlObject->resultat->siru->siege->trtDerDateSiege);

        if (null != $nic) {
            // Si l'etablissement n'existe pas en base distante
            if ('' == (string) $xmlObject->resultat->siru->etablissement->nic) {
                return;
            }
            $newComapny->setNic((string) $xmlObject->resultat->siru->etablissement->nic);
            $newComapny->setEtatet((string) $xmlObject->resultat->siru->etablissement->etatEt);
            $newComapny->setEtatdebdate((string) $xmlObject->resultat->siru->etablissement->etatDebDateEt);

            $newComapny->setCatet((string) $xmlObject->resultat->siru->etablissement->catEt);
            $newComapny->setCatetlib((string) $xmlObject->resultat->siru->etablissement->catEtLib);
            $newComapny->setEff3112tret((string) $xmlObject->resultat->siru->etablissement->eff3112TrEt);
            $newComapny->setEff3112tretlib((string) $xmlObject->resultat->siru->etablissement->eff3112TrEtLib);
            $newComapny->setEffanet((string) $xmlObject->resultat->siru->etablissement->eff3112TrEtLib);
            $newComapny->setApet((string) $xmlObject->resultat->siru->etablissement->effAnEt);
            $newComapny->setApetlib((string) $xmlObject->resultat->siru->etablissement->apet);
            $newComapny->setTrtderdateet((string) $xmlObject->resultat->siru->etablissement->apetLib);
            $newComapny->setEtatetlib((string) $xmlObject->resultat->siru->etablissement->trtDerDateEt);
        }
        $newComapny->setAdretvoienum((string) $xmlObject->resultat->siru->adresse->adrEtVoieNum);
        $newComapny->setAdretvoietype((string) $xmlObject->resultat->siru->adresse->adrEtVoieType);
        $newComapny->setAdretvoielib((string) $xmlObject->resultat->siru->adresse->adrEtVoieLib);
        $newComapny->setAdretdepcom((string) $xmlObject->resultat->siru->adresse->adrEtDepCom);
        $newComapny->setAdretcomlib((string) $xmlObject->resultat->siru->adresse->adrEtComLib);
        $newComapny->setAdretcodepost((string) $xmlObject->resultat->siru->adresse->adrEtCodePost);
        $newComapny->setAdretpost1((string) $xmlObject->resultat->siru->adresse->adrEtPost1);
        $newComapny->setAdretpost2((string) $xmlObject->resultat->siru->adresse->adrEtPost2);
        $newComapny->setAdretpost3((string) $xmlObject->resultat->siru->adresse->adrEtPost3);
        //on est dans une methode qui fait une modification de la base donc on accede a la base lecture/ecriture
        $newComapny->save(Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE')));
    }

    public function retreiveCompanyInsee($siren, $nic = null)
    {
        $xmlResponse = '';
        if (null == $nic) {
            $company = self::retrieveCompanyInseeLocalBySiren($siren);
            if ($company) {
                return $company;
            } else {
                $xmlResponse = self::webGetXmlCompanyInsee($siren, $nic = null);

                self::addCompanyInsee($xmlResponse);
            }
        } else {
            $company = self::retrieveCompanyInseeLocalBySiret($siren.$nic);

            if ($company) {
                return $company;
            }
            $xmlResponse = self::webGetXmlCompanyInsee($siren, $nic);

            self::addCompanyInsee($xmlResponse, $nic);
        }
        if (null == $nic) {
            return self::retrieveCompanyInseeLocalBySiren($siren);
        } else {
            return self::retrieveCompanyInseeLocalBySiret($siren.$nic);
        }
    }

    public function retreiveEtablissementInsee($siren, $nic)
    {
        $xmlResponse = '';
        $company = self::retrieveCompanyInseeLocalBySiret($siren.$nic);
        if ($company) {
            return $company;
        }
        $xmlResponse = self::webGetXmlCompanyInsee($siren, $nic);

        self::addCompanyInsee($xmlResponse, $nic);

        return self::retrieveCompanyInseeLocalBySiret($siren.$nic);
    }
}
