<?php

namespace Application\Service\Atexo\Insee;

use Application\Propel\Mpe\Entreprise;

/**
 * Vo pour définir les info d'un objet common pour entreprise BDE et INSEE.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Insee_EntrepriseVo extends Entreprise
{
    private string $_typeobject = 'BDE';
    private string $_nic = '';

    public function getTypeObject()
    {
        return $this->_typeobject;
    }

    public function setTypeObject($typeobject)
    {
        $this->_typeobject = $typeobject;
    }

    public function getNic()
    {
        return $this->_nic;
    }

    public function setNic($nic)
    {
        $this->_nic = $nic;
    }
}
