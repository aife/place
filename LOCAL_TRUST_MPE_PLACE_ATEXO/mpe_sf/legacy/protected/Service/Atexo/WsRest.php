<?php

namespace Application\Service\Atexo;

use DOMDocument;
use Zend_Http_Client;

/**
 * Classe de manipulation u web service REST.
 *
 * @author LEZ <loubna.ezziani@atexo.com>
 *
 * @version 1.0
 *
 * @since MPE-4.6
 *
 * @copyright Atexo 2014
 */
class Atexo_WsRest
{
    /*
     *  Permet de verfier que l'authentification a un ws a echoué
     *
     *  @param String $uri url de web service,string $xml l'xml post ,$attendu variable vide ,$constate variable vide
     *  @return void
     *  @author LEZ <loubna.ezziani@atexo.com>
     *  @version 1.0
     *  @since 4.6.0
     *  @copyright Atexo 2014
     */
    public function testPostAvecTicketInvalide($uri, $xml, &$attendu, &$constate)
    {
        $ticket = '1234';
        $config = [
        'adapter' => 'Zend_Http_Client_Adapter_Curl',
        'curloptions' => [CURLOPT_SSL_VERIFYHOST => 0, CURLOPT_SSL_VERIFYPEER => false],
        ];
        $uri = $uri.'?ticket='.$ticket;
        $client = new Zend_Http_Client($uri, $config);
        $client->setParameterPost('xml', $xml);
        $response = $client->request('POST')->getBody();
        $attendu = new DOMDocument();
        $attendu->loadXML($response);
        $xmlErreur = file_get_contents(Atexo_Config::getParameter('PATH_FILE_ERROR_WS_REST'));
        $constate = new DOMDocument();
        $constate->loadXML($xmlErreur);
    }

    /*
     *  Permet de tester la methode get d'un ws avec ticket invalide
     *
     *  @param String $uri,url de web service,$valeur ,$attendu variable vide ,$constate variable vide
     *  @return void
     *  @author LEZ <loubna.ezziani@atexo.com>
     *  @version : 1.0
     *  @since : 4.6.0
     *  @copyright Atexo 2014
     */
    public function testGetAvecTicketInvalide($uri, $valeur, &$attendu, &$constate)
    {
        $ticket = '1234';
        $config = [
        'adapter' => 'Zend_Http_Client_Adapter_Curl',
        'curloptions' => [CURLOPT_SSL_VERIFYHOST => 0, CURLOPT_SSL_VERIFYPEER => false],
        ];
        $uri = $uri.$valeur.'?ticket='.$ticket;
        $client = new Zend_Http_Client($uri, $config);
        $response = $client->request('GET')->getBody();
        $attendu = new DOMDocument();
        $attendu->loadXML($response);
        $xmlErreur = file_get_contents(Atexo_Config::getParameter('PATH_FILE_ERROR_WS_REST'));
        $constate = new DOMDocument();
        $constate->loadXML($xmlErreur);
    }

    /*
     *  Permet de tester la methode post d'un ws avec bad request
     *
     *  @param string $uri url de web service,string $xml l'xml à poster,string $ticket ticket(sso)
     *  @retrun header avec statut  bad request
     *  @author LEZ <loubna.ezziani@atexo.com>
     *  @version 1.0
     *  @since 4.6.0
     *  @copyright Atexo 2014
     */
    public function testPostAvecBadRequest($uri, $xml, $ticket)
    {
        $config = [
        'adapter' => 'Zend_Http_Client_Adapter_Curl',
        'curloptions' => [CURLOPT_SSL_VERIFYHOST => 0, CURLOPT_SSL_VERIFYPEER => false],
        ];
        $uri = $uri.'?ticket='.$ticket;
        $client = new Zend_Http_Client($uri, $config);
        $client->setParameterPost('xml', $xml);
        $response = $client->request('POST')->getStatus();

        return $response;
    }

    /*
     *  Permet de tester la methode post d'un ws avec erreur
     *
     *  @param string $uri url de web service,string $xml l'xml à poster ,string $attendu variable vide ,string $constate variable vide ,string $dce optionnel
     *  @retrun string $response,body of response
     *  @author LEZ <loubna.ezziani@atexo.com>
     *  @version 1.0
     *  @since 4.6.0
     *  @copyright Atexo 2014
     */
    public function testPostWithError($uri, $xml, $ticket, &$attendu, &$constate, $dce = null)
    {
        $config = [
        'adapter' => 'Zend_Http_Client_Adapter_Curl',
        'curloptions' => [CURLOPT_SSL_VERIFYHOST => 0, CURLOPT_SSL_VERIFYPEER => false],
        ];
        $uri = $uri.'?ticket='.$ticket;
        $client = new Zend_Http_Client($uri, $config);
        $client->setParameterPost('xml', $xml);
        if ($dce) {
            $client->setFileUpload('/tmp/testlezdce.txt', 'DCE', $dce, 'application/xml');
        }
        $response = $client->request('POST')->getBody();

        $attendu = new DOMDocument();
        $attendu->loadXML($response);
        $xmlErreur = file_get_contents(Atexo_Config::getParameter('PATH_FILE_ERROR_WS_REST'));
        $constate = new DOMDocument();
        $constate->loadXML($xmlErreur);

        return $response;
    }

    /*
     *  Permet de tester la methode get d'un ws avec erreur
     *
     *  @param string $uri url de web service,string $id, string $ticket ticket d'authentification, string $attendu variable vide, string $constate variable vide
     *  @return void
     *  @author LEZ <loubna.ezziani@atexo.com>
     *  @version 1.0
     *  @since 4.6.0
     *  @copyright Atexo 2014
     */
    public function testGetWithError($uri, $id, $ticket, &$attendu, &$constate)
    {
        $config = [
        'adapter' => 'Zend_Http_Client_Adapter_Curl',
        'curloptions' => [CURLOPT_SSL_VERIFYHOST => 0, CURLOPT_SSL_VERIFYPEER => false],
        ];
        $uri = $uri.$id.'?ticket='.$ticket;
        $client = new Zend_Http_Client($uri, $config);
        $response = $client->request('GET')->getBody();
        $attendu = new DOMDocument();
        $attendu->loadXML($response);
        $xmlErreur = file_get_contents(Atexo_Config::getParameter('PATH_FILE_ERROR_WS_REST'));
        $constate = new DOMDocument();
        $constate->loadXML($xmlErreur);
    }

    /*
     *  Permet de tester la methode post d'un ws
     *
     *  @param string $uri,url de web service,string $xml l'xml à poster.string $ticket ticket d'authentification.string $dce optionnel.string $rc optionnel.string $avis optionnel
     *  @retrun string $response,body of response
     *  @author LEZ <loubna.ezziani@atexo.com>
     *  @version 1.0
     *  @since 4.6.0
     *  @copyright Atexo 2014
     */
    public function getResponsePostRequest($uri, $xml, $ticket, $dce = null, $rc = null, $avis = null)
    {
        $config = [
        'adapter' => 'Zend_Http_Client_Adapter_Curl',
        'curloptions' => [CURLOPT_SSL_VERIFYHOST => 0, CURLOPT_SSL_VERIFYPEER => false],
        ];
        $uri = $uri.'?ticket='.$ticket;
        $client = new Zend_Http_Client($uri, $config);
        $client->setParameterPost('xml', $xml);
        if ($dce) {
            $client->setFileUpload('/tmp/testlez.txt', 'DCE', $dce, 'application/xml');
        }
        if ($rc) {
            $client->setFileUpload('/tmp/testlezRc.txt', 'RC', $rc, 'application/xml');
        }
        if ($avis) {
            $client->setFileUpload('/tmp/testlezavis.txt', 'FILE', $avis, 'application/xml');
        }
        $response = $client->request('POST')->getBody();

        return $response;
    }

    /*
     *  Permet de tester la methode get d'un ws avec un registre vide
     *
     *  @param string $uri,url de web service.string $id.string $ticket pour l'authentification.string $attendu variable vide,string $constate varibale vide
     *  @return void
     *  @author LEZ <loubna.ezziani@atexo.com>
     *  @version 1.0
     *  @since 4.6.0
     *  @copyright Atexo 2014
     */
    public function testGetXmlRegistreVide($uri, $id, $ticket, &$attendu, &$constate)
    {
        $config = [
        'adapter' => 'Zend_Http_Client_Adapter_Curl',
        'curloptions' => [CURLOPT_SSL_VERIFYHOST => 0, CURLOPT_SSL_VERIFYPEER => false],
        ];
        $uri = $uri.$id.'?ticket='.$ticket;
        $client = new Zend_Http_Client($uri, $config);
        $response = $client->request('GET')->getBody();
        $attendu = new DOMDocument();
        $attendu->loadXML($response);
        $xmlErreur = file_get_contents(Atexo_Config::getParameter('PATH_FILE_REGISTRE_VIDE'));
        $constate = new DOMDocument();
        $constate->loadXML($xmlErreur);
    }

    /*
     *  Permet de tester la methode get d'un ws
     *
     *  @param string $uri,url de web service.string $id.string $ticket pour l'authentification
     *  @retrun string $response body of response
     *  @author LEZ <loubna.ezziani@atexo.com>
     *  @version 1.0
     *  @since 4.6.0
     *  @copyright Atexo 2014
     */
    public function getResponseGetRequest($uri, $id, $ticket)
    {
        $config = [
        'adapter' => 'Zend_Http_Client_Adapter_Curl',
        'curloptions' => [CURLOPT_SSL_VERIFYHOST => 0, CURLOPT_SSL_VERIFYPEER => false],
        ];
        $uri = $uri.$id.'?ticket='.$ticket;
        $client = new Zend_Http_Client($uri, $config);
        $response = $client->request('GET')->getBody();

        return $response;
    }

    /*
     *  Permet de tester la methode get d'un ws
     *
     *  @param string $uri,url de web service.string $id.string $ticket sso pour l'authentification
     *  @retrun string $response = response
     *  @author LEZ <loubna.ezziani@atexo.com>
     *  @version 1.0
     *  @since 4.6.0
     *  @copyright Atexo 2014
     */
    public function getResponseRequestionOfGetMethode($uri, $id, $ticket)
    {
        $config = [
        'adapter' => 'Zend_Http_Client_Adapter_Curl',
        'curloptions' => [CURLOPT_SSL_VERIFYHOST => 0, CURLOPT_SSL_VERIFYPEER => false],
        ];
        $uri = $uri.$id.'?ticket='.$ticket;
        $client = new Zend_Http_Client($uri, $config);
        $response = $client->request('GET');

        return $response;
    }

    /**
     * Parse raw HTTP request data.
     *
     * Pass in $a_data as an array. This is done by reference to avoid copying
     * the data around too much.
     *
     * Any files found in the request will be added by their field name to the
     * $data['files'] array.
     *
     * @param   array  Empty array to fill with data
     *
     * @return array Associative array of request data
     */
    public function parseRawHttpRequest($response, $header)
    {
        $a_data = [];
        // grab multipart boundary from content type header
        preg_match('/boundary=(.*)$/', $header, $matches);

        // content type is probably regular form-encoded
        if (!count($matches)) {
            // we expect regular puts to containt a query string containing data
            parse_str(urldecode($response), $a_data);

            return $a_data;
        }

        $boundary = $matches[1];

        // split content by boundary and get rid of last -- element
        $a_blocks = preg_split('/-+'.$boundary.'/', $response);
        array_pop($a_blocks);
        // loop data blocks
        foreach ($a_blocks as $id => $block) {
            if ('' == trim($block)) {
                continue;
            }

            // you'll have to var_dump $block to understand this and maybe replace \n or \r with a visibile char
            // parse uploaded files
            if (str_contains($block, 'application/octet-stream')) {
                // match "name", then everything after "stream" (optional) except for prepending newlines
                preg_match('/filename="([^"]*)"/', $block, $matches);
                preg_match("/[\n|\r][\n|\r](.*)([\n|\r]+(.*))*/s", $block, $matches_content);
                $a_data['files'][$matches[1]] = $matches_content[0];
            }
            // parse all other fields
            else {
                // match "name" and optional value in between newline sequences
                preg_match('/name=\"([^\"]*)\"/', $block, $matches);
                preg_match('/[\n|\r][\n|\r](.*)([\n|\r]+(.*))*/', $block, $matches_content);
                $a_data[$matches[1]] = $matches_content[0];
            }
            unset($matches);
            unset($matches_content);
            unset($block);
        }

        return $a_data;
    }
}
