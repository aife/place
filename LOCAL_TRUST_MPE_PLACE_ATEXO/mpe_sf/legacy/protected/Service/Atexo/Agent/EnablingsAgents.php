<?php

namespace Application\Service\Atexo\Agent;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonHabilitationAgent;
use Application\Propel\Mpe\CommonHabilitationAgentPeer;
use Application\Propel\Mpe\CommonHabilitationProfilPeer;
use Application\Propel\Mpe\CommonSocleHabilitationAgent;
use Application\Propel\Mpe\CommonSocleHabilitationAgentPeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Guests;
use PDO;

/**
 * Classe de manipulation des habilitations des agents.
 *
 * @author ZAKI Anas <anas.zaki@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Agent_EnablingsAgents
{
    /**
     * affecte les habilitations à un agent selon le profile.
     *
     * @param int $idProfile l'id du profile de l'agent
     *
     * @return CommonHabilitationAgent $newHablitation habilitations des agents
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public static function addOrModifyEnablingsOfTheAgents($idProfile, $idAgent = null)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        //$profil = CommonHabilitationProfilPeer::retrieveByPk($idProfile, $connexionCom);

        if ($idAgent) {
            $newHablitation = CommonHabilitationAgentPeer::retrieveByPK($idAgent, $connexionCom);
            if (!$newHablitation) {
                $newHablitation = new CommonHabilitationAgent();
                $newHablitation->setIdAgent($idAgent);
            }
        } else {
            $newHablitation = new CommonHabilitationAgent();
        }
        $sql = 'SELECT * FROM `HabilitationProfil` WHERE `id` ='.$idProfile;
        $statement = Atexo_Db::getLinkCommon(false)->query($sql, PDO::FETCH_ASSOC);
        foreach ($statement as $res) {
            foreach ($res as $key => $value) {
                $list = explode('_', $key);
                $fct = '';
                foreach ($list as $l) {
                    if ('libelle' == $l || 'id' == $l) {
                        break;
                    }
                    $fct .= ucfirst($l);
                }

                if ('' != $fct) {
                    $set = 'set'.$fct;
                    if (method_exists($newHablitation, $set)) {
                        $newHablitation->$set($value);
                    }
                }
            }
        }

        return $newHablitation;
    }

    /**
     *  ajoute l'habilitation socle pour un agent.
     *
     * @param int $idAgent l'id de l'agent
     *
     * @return CommonSocleHabilitationAgent $newHablitationSocle
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function addOrModifyEnablingsSocleOfAgent($idAgent = null)
    {
        if ($idAgent) {
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            $newHablitationSocle = CommonSocleHabilitationAgentPeer::retrieveByPK($idAgent, $connexionCom);
            if (!$newHablitationSocle) {
                $newHablitationSocle = new CommonSocleHabilitationAgent();
                $newHablitationSocle->setIdAgent($idAgent);
            }
        } else {
            $newHablitationSocle = new CommonSocleHabilitationAgent();
        }
        $newHablitationSocle->setGestionAgentPoleSocle('1');
        $newHablitationSocle->setGestionAgentsSocle('1');
        $newHablitationSocle->setDroitGestionServicesSocle('1');

        return $newHablitationSocle;
    }

    /**
     *  Permet de recuperer les habililitation d'un agent.
     *
     * @param int $idAgent : id de l'agent
     *
     * @return CommonHabilitationAgent $habilitationsAgent objet qui contient la liste des habilitation de l'agent
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function retreiveHabilitationByAgent($idAgent)
    {
        $connection = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonHabilitationAgentPeer::ID_AGENT, $idAgent, Criteria::EQUAL);
        $habilitationsAgent = CommonHabilitationAgentPeer::doSelectOne($c, $connection);
        if ($habilitationsAgent instanceof CommonHabilitationAgent) {
            return $habilitationsAgent;
        }

        return false;
    }

    /**
     *  Permet de savoir si un agent a une habilitation donné.
     *
     * @param int    $idAgent          : id de l'agent
     * @param string $habilitationName : le nom de l'habilitation
     *
     * @return bool true si l'habilitation existe,sinon false
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function isHabilitationAgentExiste($idAgent, $habilitationName)
    {
        $habilitationExiste = false;
        $habilitationsAgent = self::retreiveHabilitationByAgent($idAgent);
        if ($habilitationsAgent) {
            $getHabilitation = 'get'.ucfirst($habilitationName);
            $habilitationExiste = $habilitationsAgent->$getHabilitation();

            return $habilitationExiste;
        }

        return $habilitationExiste;
    }

    /**
     * Permet de recuperer un agent invite a la consultation dont la reference = $Reference et qui a l'habilitation $habilitaion.
     *
     * @param int    $Reference   : la reference de la consultation
     * @param string $organisme   : l'accronyme de l'organisme
     * @param string $habilitaion : le nom de l'habilitation
     *
     * @return array $GuestWithHabilitation liste des agents invite a la consultation et qui ont l'habilitation $habilitaion
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function invatedWithHabilitation($Reference, $organisme, $habilitaion)
    {
        $guests = (new Atexo_Consultation_Guests())->getAllInvitedAgents($Reference, $organisme);
        $GuestWithHabilitation = false;
        foreach ($guests as $guest) {
            if ($guest->getPermanentGuest() || (!$guest->getPermanentGuest() && 1 == $guest->getTypeInvitation())) {
                if ((new Atexo_Agent_EnablingsAgents())->isHabilitationAgentExiste($guest->getId(), $habilitaion)) {
                    $GuestWithHabilitation[] = $guest;
                }
            }
        }

        return $GuestWithHabilitation;
    }
}
