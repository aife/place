<?php

namespace Application\Service\Atexo\Agent;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonHabilitationProfil;
use Application\Propel\Mpe\CommonHabilitationProfilPeer;
use Application\Service\Atexo\Atexo_Config;
use Prado\Prado;

/**
 * Classe de manipulation des profiles des agents.
 *
 * @author ZAKI Anas <anas.zaki@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Agent_Profiles
{
    /**
     * permet de recupèrer la liste des profils agents.
     *
     * @param bool $isAdminGlobal true pour les profils administrateur ministeriel,sinon false
     *
     * @return array $result liste des objets CommonHabilitationProfil
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public static function getProfiles($isAdminGlobal = false)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        if (!$isAdminGlobal) {
            $idAdminMinsteriel = Atexo_Config::getParameter('ID_ADMINISTRATEUR_MINISTERIEL');
            $c->add(CommonHabilitationProfilPeer::ID, $idAdminMinsteriel, Criteria::NOT_EQUAL);
        }
        $profiles = CommonHabilitationProfilPeer::doSelect($c, $connexionCom);
        $result = [];
        $result[' '] = Prado::localize('TEXT_SELECTIONNER');
        if (is_array($profiles)) {
            foreach ($profiles as $profile) {
                $result[$profile->getId()] = $profile->getLibelle();
            }
        }

        return $result;
    }
}
