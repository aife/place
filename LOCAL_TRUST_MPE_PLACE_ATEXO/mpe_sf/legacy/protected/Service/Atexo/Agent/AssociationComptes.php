<?php

namespace Application\Service\Atexo\Agent;

use App\Repository\ComptesAgentsAssociesRepository;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonComptesAgentsAssocies;
use Application\Propel\Mpe\CommonComptesAgentsAssociesPeer;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Util;
use Prado\Web\UI\TTemplateControl;

/*
 * Permet de gerer les associations des comptes des agents
 * @author khadija Chouika <khadija.chouika@atexo.com>
 * @copyright Atexo 2013
 * @version 0
 * @since MPE-4.4
 * @package Library
 * @subPackage Atexo_Agent
 */

class Atexo_Agent_AssociationComptes extends TTemplateControl
{
    /**
     * permt d'associer un compte principale a un compte secondaire.
     *
     * @param int id compte principale
     * @param int id compte secondaire
     * @param int uid (idu compte associe)
     *
     * @return void
     *
     * @author khadija Chouika <khadija.chouika@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.4.4
     *
     * @copyright Atexo 2013
     */
    public function associeCompte($idPrincipale, $idCompteSecondaire, $uid)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $compteAssocie = new CommonComptesAgentsAssocies();
        $compteAssocie->setComptePrincipal($idPrincipale);
        $compteAssocie->setCompteSecondaire($idCompteSecondaire);
        $compteAssocie->setUidEchange($uid);
        $compteAssocie->setStatutActivationCompteSecondaire(0);
        $compteAssocie->save($connexion);
    }

    /**
     * permt de chercher un compte par id agent et le type de l'association.
     *
     * @param int $idAgent         id compte principale / secondaire
     * @param int $typeAssociation type d'association
     *
     * @return array CommonComptesAgentsAssociessoit si existe sinon false
     *
     * @author khadija Chouika <khadija.chouika@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.4.4
     *
     * @copyright Atexo 2013
     */
    public function findComptesAssocies($idAgent, $typeAssociation)
    {
        $arrayComptesAgentsAssocies_key = 'arrayComptesAgentsAssocies';

        if ($typeAssociation) {
            $arrayComptesAgentsAssocies_key = $arrayComptesAgentsAssocies_key.'COMPTE_PRINCIPAL';
        } else {
            $arrayComptesAgentsAssocies_key = $arrayComptesAgentsAssocies_key.'COMPTE_SECONDAIRE';
        }
        $arrayComptesAgentsAssocies = Atexo_CurrentUser::readFromSession($arrayComptesAgentsAssocies_key);

        if (!$arrayComptesAgentsAssocies) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            $c = new Criteria();
            if ($typeAssociation) {
                $c->add(CommonComptesAgentsAssociesPeer::COMPTE_PRINCIPAL, $idAgent);
            } else {
                $c->add(CommonComptesAgentsAssociesPeer::COMPTE_SECONDAIRE, $idAgent);
            }
            $comptes = CommonComptesAgentsAssociesPeer::doSelect($c, $connexion);
            if ($comptes) {
                $arrayComptesAgentsAssocies = $comptes;
            } else {
                $arrayComptesAgentsAssocies = 'AUCUN_COMPTE';
            }
            Atexo_CurrentUser::writeToSession($arrayComptesAgentsAssocies_key, $arrayComptesAgentsAssocies);
        }

        if ('AUCUN_COMPTE' == $arrayComptesAgentsAssocies) {
            return false;
        } else {
            return $arrayComptesAgentsAssocies;
        }
    }

    /**
     * permt de supprimer une association compte.
     *
     * @param int $idPrincipal  id compte principale
     * @param int $idsecondaire id compte secondaire
     *
     * @return array CommonComptesAgentsAssociessoit si existe sinon false
     *
     * @author khadija Chouika <khadija.chouika@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.4.4
     *
     * @copyright Atexo 2013
     */
    public function rompreAssociation($idPrincipal, $idsecondaire)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $c = new Criteria();
        $c->add(CommonComptesAgentsAssociesPeer::COMPTE_PRINCIPAL, $idPrincipal);
        $c->add(CommonComptesAgentsAssociesPeer::COMPTE_SECONDAIRE, $idsecondaire);
        CommonComptesAgentsAssociesPeer::doDelete($c, $connexion);
    }

    /**
     * permt de recuperer toute les association d'un compte.
     *
     * @param int $idAgent id de l'agent
     *
     * @return array CommonComptesAgentsAssociessoit si existe sinon false
     *
     * @author khadija Chouika <khadija.chouika@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.4.4
     *
     * @copyright Atexo 2013
     */
    public function getAllActiveComptesAssocies($idAgent)
    {
        $arrayAllActiveComptesAssocies = Atexo_CurrentUser::readFromSession('arrayAllActiveComptesAssocies');

        if (!$arrayAllActiveComptesAssocies) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            $c = new Criteria();
            $c->add(CommonComptesAgentsAssociesPeer::STATUT_ACTIVATION_COMPTE_SECONDAIRE, 1);
            $c->add(CommonComptesAgentsAssociesPeer::COMPTE_PRINCIPAL, $idAgent);
            $comptes = CommonComptesAgentsAssociesPeer::doSelect($c, $connexion);
            if ($comptes) {
                $arrayAllActiveComptesAssocies = $comptes;
            } else {
                $arrayAllActiveComptesAssocies = 'AUCUN_COMPTE';
            }
            Atexo_CurrentUser::writeToSession('arrayAllActiveComptesAssocies', $arrayAllActiveComptesAssocies);
        }

        if ('AUCUN_COMPTE' == $arrayAllActiveComptesAssocies) {
            return false;
        } else {
            return $arrayAllActiveComptesAssocies;
        }
    }

    /**
     *  permt d'activer une association.
     *
     * @param int $uid unuque id du compte associe
     *
     * @return void
     *
     * @author khadija Chouika <khadija.chouika@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.4.4
     *
     * @copyright Atexo 2013
     */
    public function activerAssociation($uid)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $c = new Criteria();
        $c->add(CommonComptesAgentsAssociesPeer::UID_ECHANGE, $uid);
        $compte = CommonComptesAgentsAssociesPeer::doSelectOne($c, $connexion);
        if ($compte) {
            $compte->setStatutActivationCompteSecondaire(1);
            $compte->save($connexion);
        }
    }

    /**
     * permt de supprimer une association suite à une action de l'utilisateur on cliquant sur le lien qui la reçu par email.
     *
     * @param int $uid unuque id du compte associe
     *
     * @return void
     *
     * @author khadija Chouika <khadija.chouika@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.4.4
     *
     * @copyright Atexo 2013
     */
    public function supprimerAssociation($uid)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $c = new Criteria();
        $c->add(CommonComptesAgentsAssociesPeer::UID_ECHANGE, $uid);
        CommonComptesAgentsAssociesPeer::doDelete($c, $connexion);
    }

    /**
     * permt de recuperer un agent associe.
     *
     * @param int $uid unuque id du CommonComptesAgentsAssociesPeer
     *
     * @return CommonComptesAgentsAssociesPeer $compte si existe sinon false
     *
     * @author khadija Chouika <khadija.chouika@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.4.4
     *
     * @copyright Atexo 2013
     */
    public function getAgentByUI($uid)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonComptesAgentsAssociesPeer::UID_ECHANGE, $uid);
        $compte = CommonComptesAgentsAssociesPeer::doSelectOne($c, $connexion);
        if ($compte) {
            return $compte;
        }

        return false;
    }

    public function deleteCompteAssocie(string $agentId): void
    {
        /** @var ComptesAgentsAssociesRepository $compteAgentAssocieRepo */
        $compteAgentAssocieRepo = Atexo_Util::getSfService(ComptesAgentsAssociesRepository::class);
        $compteAgentAssocieRepo->deleteAssociationByAgent($agentId);
    }
}
