<?php

namespace Application\Service\Atexo\Agent;

/**
 * Vo pour définir les critères de Access::search().
 *
 * @author ZAKI Anas <zaki.anas@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Agent_CriteriaVo
{
    private ?string $nom = null;
    private ?string $prenom = null;
    private ?string $email = null;
    private ?int $service = null;
    private ?string $dateDebut = null;
    private ?string $dateFin = null;
    private ?string $organisme = null;
    private ?int $offset = null;
    private ?int $limit = null;
    private ?string $identifiant = null;
    private bool $inclureDesc = false;
    private string $sortByElement = '';
    private string $sensOrderBy = 'DESC';
    //tableau d'objet referentielVo
    private array $referentielVo = [];
    private $listeIdsAgents = [];
    //lié à la rcherche par code/libelle acheteur (module :AfficherCodeService)
    private $codeAcheteur = false;
    private $libelleAcheteur = false;
    private $actif = null;

    /**
     * recupere la valeur du [nom].
     *
     * @return string la valeur courante [nom]
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * modifie la valeur de [nom].
     *
     * @param string $nom la valeur a mettre
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * recupere la valeur du [prenom].
     *
     * @return string la valeur courante [prenom]
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * modifie la valeur de [prenom].
     *
     * @param string $prenom la valeur a mettre
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;
    }

    /**
     * recupere la valeur du [email].
     *
     * @return string la valeur courante [email]
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * modifie la valeur de [email].
     *
     * @param string $email la valeur a mettre
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * recupere la valeur du [dateDebut].
     *
     * @return string la valeur courante [dateDebut]
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getDateDebut()
    {
        return $this->dateDebut;
    }

    /**
     * modifie la valeur de [dateDebut].
     *
     * @param string $dateDebut la valeur a mettre
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setDateDebut($dateDebut)
    {
        $this->dateDebut = $dateDebut;
    }

    /**
     * recupere la valeur du [dateFin].
     *
     * @return string la valeur courante [dateFin]
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getDateFin()
    {
        return $this->dateFin;
    }

    /**
     * modifie la valeur de [dateFin].
     *
     * @param string $dateFin la valeur a mettre
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setDateFin($dateFin)
    {
        $this->dateFin = $dateFin;
    }

    /**
     * recupere la valeur du [service].
     *
     * @return int la valeur courante [service]
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * modifie la valeur de [service].
     *
     * @param int $service la valeur a mettre
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setService($service)
    {
        $this->service = $service;
    }

    /**
     * recupere la valeur du [organisme].
     *
     * @return string la valeur courante [organisme]
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    /**
     * modifie la valeur de [organisme].
     *
     * @param string $organisme la valeur a mettre
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setOrganisme($organisme)
    {
        $this->organisme = $organisme;
    }

    /**
     * recupere la valeur du [offset].
     *
     * @return int la valeur courante [offset]
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getOffset()
    {
        return $this->offset;
    }

    /**
     * modifie la valeur de [offset].
     *
     * @param int $offset la valeur a mettre
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setOffset($offset)
    {
        $this->offset = $offset;
    }

    /**
     * recupere la valeur du [limit].
     *
     * @return int la valeur courante [limit]
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * modifie la valeur de [limit].
     *
     * @param int $limit la valeur a mettre
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;
    }

    /**
     * recupere la valeur du [identifiant]. ('identifiant' correspond au champ 'login' en BDD)
     * @return string la valeur courante [identifiant]
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getIdentifiant()
    {
        return $this->identifiant;
    }

    /**
     * modifie la valeur de [identifiant]. ('identifiant' correspond au champ 'login' en BDD)
     *
     * @param string $identifiant la valeur a mettre
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setIdentifiant($identifiant)
    {
        $this->identifiant = $identifiant;
    }

    /**
     * recupere la valeur du [inclureDesc].
     *
     * @return bool la valeur courante [inclureDesc]
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getInclureDesc()
    {
        return $this->inclureDesc;
    }

    /**
     * modifie la valeur de [inclureDesc].
     *
     * @param bool $inclureDesc la valeur a mettre
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setInclureDesc($inclureDesc)
    {
        $this->inclureDesc = $inclureDesc;
    }

    /**
     * recupere la valeur du [sortByElement].
     *
     * @return string la valeur courante [sortByElement]
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getSortByElement()
    {
        return $this->sortByElement;
    }

    /**
     * modifie la valeur de [sortByElement].
     *
     * @param string $sortByElement la valeur a mettre
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setSortByElement($sortByElement)
    {
        $this->sortByElement = $sortByElement;
    }

    /**
     * recupere la valeur du [sensOrderBy].
     *
     * @return string la valeur courante [sensOrderBy]
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getSensOrderBy()
    {
        return $this->sensOrderBy;
    }

    /**
     * modifie la valeur de [sensOrderBy].
     *
     * @param string $sensOrderBy la valeur a mettre
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setSensOrderBy($sensOrderBy)
    {
        $this->sensOrderBy = $sensOrderBy;
    }

    /**
     * modifie la valeur de [ReferentielVo].
     *
     * @param ReferentielVo $ReferentielVo la valeur a mettre
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setReferentielVo($value)
    {
        $this->referentielVo = $value;
    }

    /**
     * recupere la valeur du [ReferentielVo].
     *
     * @return ReferentielVo la valeur courante [ReferentielVo]
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getReferentielVo()
    {
        return $this->referentielVo;
    }

    /**
     * modifie la valeur de [listeIdsAgents].
     *
     * @param array $listeIdsAgents la valeur a mettre
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setListeIdsAgents($value)
    {
        $this->listeIdsAgents = $value;
    }

    /**
     * recupere la valeur du [listeIdsAgents].
     *
     * @return array la valeur courante [listeIdsAgents]
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getListeIdsAgents()
    {
        return $this->listeIdsAgents;
    }

    /**
     * modifie la valeur de [codeAcheteur].
     *
     * @param int $codeAcheteur la valeur a mettre
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setCodeAcheteur($value)
    {
        $this->codeAcheteur = $value;
    }

    /**
     * recupere la valeur du [codeAcheteur].
     *
     * @return int la valeur courante [codeAcheteur]
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getCodeAcheteur()
    {
        return $this->codeAcheteur;
    }

    /**
     * modifie la valeur de [libelleAcheteur].
     *
     * @param string $libelleAcheteur la valeur a mettre
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setLibelleAcheteur($value)
    {
        $this->libelleAcheteur = $value;
    }

    /**
     * recupere la valeur du [libelleAcheteur].
     *
     * @return string la valeur courante [libelleAcheteur]
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getLibelleAcheteur()
    {
        return $this->libelleAcheteur;
    }

    /**
     * @return null
     */
    public function getActif()
    {
        return $this->actif;
    }

    /**
     * @param null $actif
     */
    public function setActif($actif)
    {
        $this->actif = $actif;
    }
}
