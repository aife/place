<?php

namespace Application\Service\Atexo\Agent;

use Application\Propel\Mpe\CommonTNotificationAgent;
use Application\Propel\Mpe\CommonTNotificationAgentQuery;

/**
 * permet de gerer les notifications.
 *
 * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
 *
 * @version 1.0
 *
 * @since 2016-developp
 *
 * @copyright Atexo 2016
 */
class Atexo_Agent_Notifications
{
    /**
     * permet d'ajouter une notification.
     *
     * @param $idAgent
     * @param $organisme
     * @param $idType
     * @param $libelle
     * @param $description
     * @param $connexion
     * @param null $url
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function ajouterNotification($idAgent, $organisme, $idType, $libelle, $description, $reference, $connexion, $url = null)
    {
        $notification = new CommonTNotificationAgent();
        $notification->setIdAgent($idAgent);
        $notification->setOrganisme($organisme);
        $notification->setIdType($idType);
        $notification->setLibelleNotification($libelle);
        $notification->setDescription($description);
        $notification->setReferenceObjet($reference);
        $notification->setDateCreation(date('Y-m-d H:i:s'));
        $notification->setUrlNotification($url);
        $notification->save($connexion);
    }

    /**
     * permet de recuperer les notifications actifs d'un agent.
     *
     * @param $idAgent
     * @param $connexion
     *
     * @return array CommonTNotificationAgent
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function getNotificationActif($idAgent, $connexion = null)
    {
        $notificationQuery = new CommonTNotificationAgentQuery();

        return $notificationQuery->getNotificationActifByIdAgent($idAgent);
    }

    /**
     * permet de recuperer le nombre des notifications actifs d'un agent.
     *
     * @param $idAgent
     * @param $connexion
     *
     * @return int nombre des notification actif
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function getNombreNotificationActif($idAgent, $notificationLue = null, $connexion = null)
    {
        $notificationQuery = new CommonTNotificationAgentQuery();

        return $notificationQuery->getNombreNotificationActifByIdAgent($idAgent, $notificationLue, $connexion);
    }

    /**
     * permet de metre à jour la notification.
     *
     * @param $idNotification
     * @param $connexion
     *
     * @return int nombre des notification actif
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function getNotificationById($idNotification, $connexion = null)
    {
        $notificationQuery = new CommonTNotificationAgentQuery();

        return $notificationQuery->getNotificationById($idNotification, $connexion);
    }
}
