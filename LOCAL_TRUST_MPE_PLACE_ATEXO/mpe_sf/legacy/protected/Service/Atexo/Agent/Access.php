<?php

namespace Application\Service\Atexo\Agent;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonSuiviAccesPeer;
use Application\Service\Atexo\Atexo_Config;

/**
 * Classe de manipulation de l'accès des agents.
 *
 * @author ZAKI Anas <anas.zaki@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Agent_Access
{
    /**
     * recherche les accès agents.
     *
     * @return array CommonSuiviAccesPeer tableau des acces
     *
     * @author ZAKI Anas <anas.zaki@atexo.com>
     *
     * @version 1.0
     *
     * @since MPE-3.0
     *
     * @copyright Atexo 2014
     */
    public static function search(Atexo_Agent_CriteriaVo $CriteriaVo)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $acces = [];
        $c = new Criteria();

        if ($CriteriaVo->getNom()) {
            $c->add(CommonSuiviAccesPeer::NOM, '%'.$CriteriaVo->getNom().'%', Criteria::LIKE);
        }

        if ($CriteriaVo->getPrenom()) {
            $c->add(CommonSuiviAccesPeer::PRENOM, '%'.$CriteriaVo->getPrenom().'%', Criteria::LIKE);
        }

        if ($CriteriaVo->getEmail()) {
            $c->add(CommonSuiviAccesPeer::EMAIL, $CriteriaVo->getEmail(), Criteria::EQUAL);
        }

        if ($CriteriaVo->getDateDebut()) {
            $c->addAnd(CommonSuiviAccesPeer::DATE_ACCES, $CriteriaVo->getDateDebut(), Criteria::GREATER_EQUAL);
        }

        if ($CriteriaVo->getDateFin()) {
            $c->addAnd(CommonSuiviAccesPeer::DATE_ACCES, $CriteriaVo->getDateFin(), Criteria::LESS_EQUAL);
        }

        if (0 != $CriteriaVo->getService()) {
            $c->add(CommonSuiviAccesPeer::SERVICE_ID, $CriteriaVo->getService(), Criteria::EQUAL);
        }

        if ($CriteriaVo->getOrganisme()) {
            $c->add(CommonSuiviAccesPeer::ORGANISME, $CriteriaVo->getOrganisme(), Criteria::EQUAL);
        }
        $c->addDescendingOrderByColumn(CommonSuiviAccesPeer::DATE_ACCES);
        $acces = CommonSuiviAccesPeer::doSelect($c, $connexionCom);

        return $acces;
    }
}
