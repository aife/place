<?php

namespace Application\Service\Atexo\Agent;

use App\Service\User;
use Application\Controls\MpeTPage;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_ThemeGraphique;
use Application\Service\Atexo\Atexo_Util;

/**
 * Page d'authentification.
 *
 * @author khadija Chouika <khadija.chouika@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 0
 *
 * @since MPE-4.4
 */
class Atexo_Agent_Authentifier extends MpeTPage
{
    /**
     *  permet de verifier l'authentification d'un agent.
     *
     * @param string $login l'identifiant de l'agent
     * @param string $pwd   le mot de passe de l'agent
     *
     * @return bool true si Ok, sinon false
     *
     * @author khadija Chouika <khadija.chouika@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.4.0
     *
     * @copyright Atexo 2013
     */
    public function VerifyLoginPwd($login, $pwd)
    {
        /** @var User $userService */
        $userService = Atexo_Util::getSfService(User::class);
        $sfUser = $userService->getUser();

        if (!is_null($sfUser)) {
            $idAgent = $sfUser->getId();
            $agent = (new Atexo_Agent())->retrieveAgent($idAgent);
            (new Atexo_ThemeGraphique())->setCookieThemeGraphiquePersonnel($agent);

            if ($_GET['goto'] && Atexo_Util::urlIsAllow($_GET['goto'])) {
                $this->response->redirect(urldecode(Atexo_Util::atexoHtmlEntities($_GET['goto'])));
            } else {
                $langueEnSession = Atexo_CurrentUser::readFromSession('lang');
                if ('' == $langueEnSession || !isset($langueEnSession)) {
                    Atexo_CurrentUser::writeToSession('lang', 'fr');
                }

                return true;
            }
        }
        return false;
    }
}
