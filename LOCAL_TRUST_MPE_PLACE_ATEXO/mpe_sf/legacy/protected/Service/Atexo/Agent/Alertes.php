<?php

namespace Application\Service\Atexo\Agent;

use Application\Propel\Mpe\CommonPlateformeVirtuelle;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Contrat;
use Exception;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonCategorieLot;
use Application\Propel\Mpe\CommonCategorieLotPeer;
use Application\Propel\Mpe\CommonChorusEchange;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonOffrePapier;
use Application\Propel\Mpe\CommonOffrePapierPeer;
use Application\Propel\Mpe\CommonOffres;
use Application\Propel\Mpe\CommonOffresPeer;
//use Application\Propel\Mpe\CommonPlateformeVirtuelle;
use Application\Propel\Mpe\CommonTContratTitulaire;
use Application\Propel\Mpe\CommonTContratTitulaireQuery;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_CategorieLot;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Chorus\Atexo_Chorus_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Guests;
use Application\Service\Atexo\Consultation\Atexo_Consultation_GuestVo;
use Prado\Prado;

/**
 * permet de gerer les alertes (mails).
 *
 * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
 *
 * @version 1.0
 *
 * @since 4.8.0
 *
 * @copyright Atexo 2014
 */
class Atexo_Agent_Alertes
{
    /**
     * permet de recuperer le corp du mail de notification de publication sur BOAMP ou MONITEUR.
     *
     * @param string             $typeAnnonce  ,BOAMP ou MONITEUR
     * @param CommonConsultation $consultation la consultation concernee
     * @param Booloean           $withDateFin  :true pour ajouter la datefin de la consultation au mail,sinn false
     *
     * @return string $corpsMail le contenu du mail
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function getCorpsMessageAlertePublication($typeAnnonce, $consultation, $withDateFin)
    {
        $mail = (new Atexo_Message())->getContentMailForPublicite($consultation);
        $replaced = ['TEXT_TITRE', 'TEXT_MAJ_PUB'];
        $replacedBy = [Prado::localize('PUBLICATION_BOAMP_EFFECTIVE'), Prado::localize('CORPS_PUBLICATION_' . $typeAnnonce . '_EFFECTIVE')];
        $corpsMail = str_replace($replaced, $replacedBy, $mail);
        if ($withDateFin) {
            if ($consultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION')) {
                $textDateLimite = Prado::localize('DEFINE_DATE_LIMITE_REMISE_PLI');
            } else {
                $textDateLimite = Prado::localize('DEFINE_TEXT_DATE_FIN_AFFICHAGE');
            }
            $replaced = ['<!--DATELIMITE', 'DATELIMITE-->', 'DEFINE_LIMITE_REMISE_PLIS', 'VALEUR_LIMITE_REMISE_PLIS'];
            $replacedBy = ['', '', $textDateLimite, Atexo_Util::iso2frnDateTime($consultation->getDatefin())];
            $corpsMail = str_replace($replaced, $replacedBy, $corpsMail);
        }

        return $corpsMail;
    }

    /**
     * permet de recuperer le corp du mail de notification d'echec de publication sur BOAMP ou MONITEUR.
     *
     * @param string             $typeAnnonce  ,BOAMP ou MONITEUR
     * @param CommonConsultation $consultation la consultation concernee
     *
     * @return string $corpsMail le contenu du mail
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function getCorpsMessageAlerteEchecPublication($typeAnnonce, $consultation)
    {
        $mail = (new Atexo_Message())->getContentMailForPublicite($consultation);
        $replaced = ['TEXT_TITRE', 'TEXT_MAJ_PUB'];
        $replacedBy = [Prado::localize('PUBLICATION_BOAMP_ECHEC'), Prado::localize('CORPS_ECHEC_PUBLICATION_' . $typeAnnonce)];
        $corpsMail = str_replace($replaced, $replacedBy, $mail);

        return $corpsMail;
    }

    /**
     * permet d'envoyer le mail de notification de publication sur BOAMP ou MONITEUR.
     *
     * @param string             $typeAnnonce  BOAMP ou MONITEUR
     * @param CommonConsultation $consultation la consultation concernee
     * @param Booloean           $withDateFin  :true pour ajouter la datefin de la consultation au mail,sinn false
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function sendAlertePublication($typeAnnonce, $commonConsultation, $withDateFin = false)
    {
        $params = [];
        //DEBUT envoi du mail de notification aux agent concerne
        $guests = (new Atexo_Consultation_Guests())->getAllInvitedAgents($commonConsultation->getId(), $commonConsultation->getOrganisme());
        $listeIdsAgents = (new Atexo_Consultation_Guests())->getIdsAgentFromArrayGuestsVo($guests);
        $agents = (new Atexo_Agent())->retrieveAgentsAvecAlertePublicationBOAMP($listeIdsAgents);

        $params['sendAlertePublicationBoamp'] = true;
        $listIdAgents = Atexo_Consultation_Guests::listAgentConsultationFavoris(
            $commonConsultation->getId(),
            $params
        );

        if ($agents && is_array($agents)) {
            $corpsMail = self::getCorpsMessageAlertePublication($typeAnnonce, $commonConsultation, $withDateFin);
            foreach ($agents as $agent) {
                if (
                    $agent->getEmail()
                    && in_array($agent->getId(), $listIdAgents) !== false
                ) {
                    $replaced = array("VALEUR_NOM_DEST", "VALEUR_PRENOM_DEST");
                    $replacedBy = array($agent->getNom(), $agent->getPrenom());
                    $mail = str_replace($replaced, $replacedBy, $corpsMail);
                    $from = Atexo_Config::getParameter('PF_MAIL_FROM', $commonConsultation->getOrganisme());
                    $pfName = null;

                    if (
                        $commonConsultation instanceof CommonConsultation
                        && $commonConsultation->getCommonPlateformeVirtuelle() instanceof CommonPlateformeVirtuelle
                    ) {
                        $plateformeVirtuelle = $commonConsultation->getCommonPlateformeVirtuelle();
                        $from = $plateformeVirtuelle->getNoReply();
                        $pfName = $plateformeVirtuelle->getFromPfName();
                    }

                    $to = $agent->getEmail();
                    $subject = Prado::localize('PUBLICATION_' . $typeAnnonce . '_EFFECTIVE')
                        . ' - ' . Prado::localize('TEXT_REF')
                        . ' : ' . $commonConsultation->getReferenceUtilisateur();
                    Atexo_Message::simpleMail(
                        $from,
                        $to,
                        $subject,
                        $mail,
                        '',
                        '',
                        false,
                        true,
                        true,
                        $pfName
                    );
                }
            }
        }
        //FIN envoi du mail de notification aux agent concerne
    }

    /**
     * permet d'envoyer le mail de notification d'echec de publication sur BOAMP ou MONITEUR.
     *
     * @param string             $typeAnnonce  BOAMP ou MONITEUR
     * @param CommonConsultation $consultation la consultation concernee
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function sendAlerteEchecPublication($typeAnnonce, $commonConsultation)
    {
        $params = [];
        //DEBUT envoi du mail de notification aux agent concerne
        $guests = (new Atexo_Consultation_Guests())->getAllInvitedAgents($commonConsultation->getId(), $commonConsultation->getOrganisme());
        $listeIdsAgents = (new Atexo_Consultation_Guests())->getIdsAgentFromArrayGuestsVo($guests);
        $agents = (new Atexo_Agent())->retrieveAgentsAvecAlerteEchecPublicationBOAMP($listeIdsAgents);

        $params['sendAlerteEchecPublicationBoamp'] = true;
        $listIdAgents = Atexo_Consultation_Guests::listAgentConsultationFavoris(
            $commonConsultation->getId(),
            $params
        );

        if ($agents && is_array($agents)) {
            $corpsMail = self::getCorpsMessageAlerteEchecPublication($typeAnnonce, $commonConsultation);
            foreach ($agents as $agent) {
                if (
                    $agent->getEmail()
                    && false !== in_array($agent->getId(), $listIdAgents)
                ) {
                    $replaced = ['VALEUR_NOM_DEST', 'VALEUR_PRENOM_DEST'];
                    $replacedBy = [$agent->getNom(), $agent->getPrenom()];
                    $mail = str_replace($replaced, $replacedBy, $corpsMail);
                    $from = Atexo_Config::getParameter('PF_MAIL_FROM', $commonConsultation->getOrganisme());
                    $pfName = null;

                    if (
                        $commonConsultation instanceof CommonConsultation
                        && $commonConsultation->getCommonPlateformeVirtuelle() instanceof CommonPlateformeVirtuelle
                    ) {
                        $plateformeVirtuelle = $commonConsultation->getCommonPlateformeVirtuelle();
                        $from = $plateformeVirtuelle->getNoReply();
                        $pfName = $plateformeVirtuelle->getFromPfName();
                    }

                    $to = $agent->getEmail();
                    $subject = Prado::localize('PUBLICATION_' . $typeAnnonce . '_ECHEC') . ' - ' . prado::localize('TEXT_REF') . ' : ' . $commonConsultation->getReferenceUtilisateur();
                    Atexo_Message::simpleMail(
                        $from,
                        $to,
                        $subject,
                        $mail,
                        '',
                        '',
                        false,
                        true,
                        true,
                        $pfName
                    );
                }
            }
        }
        //FIN envoi du mail de notification aux agent concerne
    }

    /**
     * Permet de recuperer le coprs du message pour les marches commandes ou clotures dans CHORUS.
     *
     * @param $contrat
     * @param $statut
     *
     * @return mixed|string
     */
    public function getCorpsMessageAlerteChorusMarchesCommandesOuClotures($contrat, $statut)
    {
        if (is_array($contrat)) {
            $numEj = (!empty($contrat['numero'])) ? $contrat['numero'] : '-';
            $intitule = (!empty($contrat['intitule'])) ? $contrat['intitule'] : '-';
            $statut = (!empty($statut)) ? $statut : '-';

            return str_replace('[__NUM_EJ__]', '<strong>' . $numEj . '</strong>', str_replace('[__INTITULE_MARCHE__]', '<strong>&quot;' . $intitule . '&quot;</strong>', str_replace('[__STATUT__]', '<strong>' . $statut . '</strong>', Prado::localize('DEFINE_CORPS_MESSAGE_MARCHE_CLOTURE_OU_COMMANDE_DANS_CHORUS'))));
        }

        return '';
    }

    /**
     * Permet de recuperer le corps du message pour les marches integres ou supprimes dans CHORUS.
     *
     * @param $contrat
     * @param $statut
     *
     * @return mixed|string
     */
    public function getCorpsMessageAlerteChorusMarchesIntegresOuSupprimes($contrat, $statut)
    {
        if (is_array($contrat)) {
            $numEj = (!empty($contrat['numero'])) ? $contrat['numero'] : '-';
            $numLongAc = (!empty($contrat['numeroLong'])) ? $contrat['numeroLong'] : '-';
            $intitule = (!empty($contrat['intitule'])) ? $contrat['intitule'] : '-';
            $statut = (!empty($statut)) ? $statut : '-';
            if ((new Atexo_Consultation_Contrat())->isTypeContratAcSad($contrat['type']['codeExterne'])) {
                return str_replace('[__NUM_AC_SAD__]', '<strong>' . $numLongAc . '</strong>', str_replace('[__INTITULE_MARCHE__]', '<strong>&quot;' . $intitule . '&quot;</strong>', Prado::localize('DEFINE_CORPS_MESSAGE_MARCHE_INTEGRE_DANS_CHORUS_FEN211')));
            } else {
                return str_replace('[__NUM_EJ__]', '<strong>' . $numEj . '</strong>', str_replace('[__INTITULE_MARCHE__]', '<strong>&quot;' . $intitule . '&quot;</strong>', str_replace('[__STATUT__]', '<strong>' . $statut . '</strong>', Prado::localize('DEFINE_CORPS_MESSAGE_MARCHE_INTEGRE_OU_SUPPRIME_DANS_CHORUS_FEN111'))));
            }
        }

        return '';
    }

    /**
     * Permet de construire le titre du message pour les marches integres ou supprimes dans CHORUS.
     *
     * @param $contrat
     * @param $echange
     *
     * @return mixed|string
     */
    public function getTitreMessageAlerteChorusMarchesIntegresOuSupprimes($contrat, $echange)
    {
        if (is_array($contrat) && $echange instanceof CommonChorusEchange) {
            $numEj = (!empty($contrat['numero'])) ? $contrat['numero'] : '-';
            $numLongAc = (!empty($contrat['numeroLong'])) ? $contrat['numeroLong'] : '-';
            $numOrdre = (!empty($echange->getNumOrdre())) ? $echange->getNumOrdre() : '-';
            $refLibre = (!empty($contrat['referenceLibre'])) ? $contrat['referenceLibre'] : '-';
            if ((new Atexo_Consultation_Contrat())->isTypeContratAcSad($contrat['type']['codeExterne'])) {
                return str_replace('[__NUM_LONG_AC_SAD__]', '"' . $numLongAc . '"', str_replace('[__NUM_ENVOI__]', $numOrdre, str_replace('[__REFERENCE_CONTRAT__]', '"' . $refLibre . '"', Prado::localize('RETOUR_CHORUS_MARCHE_INTEGRE_FEN211'))));
            } else {
                return str_replace('[__NUM_MARCHE__]', $numEj, str_replace('[__NUM_ENVOI__]', $numOrdre, str_replace('[__REFERENCE_CONTRAT__]', '"' . $refLibre . '"', Prado::localize('RETOUR_CHORUS_MARCHE_INTEGRE_FEN111'))));
            }
        }

        return '';
    }

    /**
     * Permet de construire le titre du message pour les marches commandes, clotures ou supprimes dans CHORUS.
     *
     * @param $contrat
     * @param $statut
     *
     * @return mixed|string
     */
    public function getTitreMessageAlerteChorusMarchesCommandesCloturesSupprimes($contrat, $statut)
    {
        if (is_array($contrat)) {
            $numEj = (!empty($contrat['numero'])) ? $contrat['numero'] : '-';
            $refLibre = (!empty($contrat['referenceLibre'])) ? $contrat['referenceLibre'] : '-';

            return str_replace('[__NUM_MARCHE__]', $numEj, str_replace('[__REFERENCE_CONTRAT__]', '"' . $refLibre . '"', $statut));
        }

        return '';
    }

    /**
     * Permet de recuperer le corps du mail d'alertes chorus aux agents pour les cas d'envois "rejetes".
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function getCorpsMessageAlerteChorusEnvoiRejete($contrat)
    {
        if ($contrat) {
            $intitule = (!empty($contrat['intitule'])) ? $contrat['intitule'] : '-';

            return str_replace('[__INTITULE_MARCHE__]', '<strong>&quot;' . $intitule . '&quot;</strong>', Prado::localize('DEFINE_CORPS_MESSAGE_ENVOI_REJETE_DANS_CHORUS'));
        }

        return '';
    }

    public function getIntituleMessageAlerteChorusEnvoiRejete($contrat, $echange)
    {
        if ($contrat && $echange instanceof CommonChorusEchange) {
            $numOrdre = (!empty($echange->getNumOrdre())) ? $echange->getNumOrdre() : '-';
            $refLibre = (!empty($contrat['referenceLibre'])) ? $contrat['referenceLibre'] : '-';

            return str_replace('[__NUM_ENVOI__]', $numOrdre, str_replace('[__REFERENCE_CONTRAT__]', '"' . $refLibre . '"', Prado::localize('RETOUR_CHORUS_ENVOI_REJETE')));
        }

        return '';
    }

    /**
     * Permet de recuperer la partie des infos de la consultation du corps du mail d'alertes chorus.
     *
     * @param string $consultationId      la reference de la consultation
     * @param string $acronyme            l'accronyme de l'organisme
     * @param int    $numeroLot           numero du lot (champs lot de la table CategorieLot)
     * @param int    $idDecisionEnveloppe l'id de la table decisionEnveloppe
     * @param int    $numeroOrdre         (champs num_ordre de la table Chorus_echange)
     * @param string $numeroMarche        = valeur du champs numero_marche de la table decisionEnveloppe
     * @param bool   $withRefCons         true pour recupere la reference utilisateur de la consultation,sinon false
     *
     * @return string $corpsMail le contenu du mail
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function getInfosPassationEnvoieChorus($consultationId, $acronyme, $lots, $contrat, $numeroOrdre, $numeroMarche, $withRefCons = false)
    {
        if ($contrat instanceof CommonTContratTitulaire) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));
            $consultation = (new Atexo_Consultation())->retrieveConsultation($consultationId, $acronyme);
            if ($consultation instanceof CommonConsultation) {
                $nomEntreprise = '';
                if ($contrat->getTypeDepotReponse() == Atexo_Config::getParameter('DEPOT_ELECTRONIQUE')) {
                    $offre = CommonOffresPeer::retrieveByPK($contrat->getIdOffre(), $acronyme, $connexion);
                    if ($offre instanceof CommonOffres) {
                        $nomEntreprise = $offre->getNomEntrepriseInscrit();
                    }
                } elseif ($contrat->getTypeDepotReponse() == Atexo_Config::getParameter('DEPOT_PAPIER')) {
                    $offre = CommonOffrePapierPeer::retrieveByPK($contrat->getIdOffre(), $acronyme, $connexion);
                    if ($offre instanceof CommonOffrePapier) {
                        $nomEntreprise = $offre->getNomEntreprise();
                    }
                }

                $libelle = '';

                $corpsMail = '';

                if ($withRefCons) {
                    $corpsMail .= Prado::localize('REFERENCE_CONSULTATION') . ' : ' . $consultation->getReferenceUtilisateur();
                    $corpsMail .= '
';
                }
                $corpsMail .= Prado::localize('DEFINE_OBJET_MARCHE') . ' : ' . $contrat->getObjetContrat();
                $corpsMail .= '
';
                if ('' != $nomEntreprise) {
                    $corpsMail .= Prado::localize('TEXT_TITULAIRE_MARCHE') . ' ' . $nomEntreprise;
                    $corpsMail .= '
';
                }
                $corpsMail .= Prado::localize('DATE_NOTIFICATION') . ' : ' . Atexo_Util::iso2frnDate($contrat->getDateNotification());
                $corpsMail .= '
';
                if ('0' == $consultation->getAlloti()) {
                    $corpsMail .= Prado::localize('TEXT_LOT') . ' : ' . Prado::localize('TEXT_NON_ALLOTIE');
                    $corpsMail .= '
';
                } else {
                    foreach ($lots as $idLot) {
                        $categorieLot = (new Atexo_CategorieLot())->retrieveByOrganismeConsultationIdNumero($acronyme, $consultationId, $idLot);
                        //$categorieLot = CommonCategorieLotPeer::retrieveByPK($acronyme, $consultationId, $idLot, $connexion);
                        if ($categorieLot instanceof CommonCategorieLot) {
                            $libelle = $categorieLot->getDescription();
                            $corpsMail .= Prado::localize('TEXT_LOT') . ' : ';
                            $corpsMail .= '
      ';
                            $corpsMail .= $idLot . ' - ' . $libelle;
                        }
                    }
                    $corpsMail .= '
';
                }

                if ($numeroOrdre) {
                    $corpsMail .= Prado::localize('TEXT_NUMERO_ORDRE') . ' : ' . $numeroOrdre;
                    $corpsMail .= '
';
                }
                $corpsMail .= Prado::localize('TEXT_NUM_MARCHE') . ' : ' . $numeroMarche;
                $corpsMail .= '
';

                return $corpsMail;
            } else {
                (new Atexo_Chorus_Util())->loggerErreurReception("Consultation non trouvee : reference_cons = $consultationId, acronyme = $acronyme, numero_ordre = $numeroOrdre, numero_marche = $numeroMarche, Methode = Alertes::getInfosPassationEnvoieChorus");
            }
        } else {
            (new Atexo_Chorus_Util())->loggerErreurReception("Contrat non trouve : reference_cons = $consultationId, acronyme = $acronyme, numero_ordre = $numeroOrdre, numero_marche = $numeroMarche, Methode = Alertes::getInfosPassationEnvoieChorus");
        }
    }

    /**
     * Permet de construire et d'envoyer le mail d'alertes des retours chorus.
     *
     * @param string                     $from     : expediteur
     * @param Atexo_Consultation_GuestVo $guest    : objet agent Vo
     * @param string                     $titre    : titre du message
     * @param string                     $corpsMsg : coprs du message
     * @param CommonTContratTitulaire    $contrat  : objet contrat titulaire
     * @param string                     $numOrdre : numero d'ordre
     * @param Logger|null                $logger   : logger
     *
     * @return bool|void : envoi le message, false si une erreur survient
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function sendMailAlertesAgentsChorus($from, $guest, $titre, $corpsMsg, $contrat, $numOrdre, $logger = null)
    {
        if (empty($logger)) {
            $logger = Atexo_LoggerManager::getLogger('chorus');
        }
        if ($contrat) {
            try {
                $corps = (new Atexo_Util())->toHttpEncoding(file_get_contents(Atexo_Config::getParameter('CHEMIN_FICHIER_MAIL_ALERTES_AGENTS_RETOURS_CHORUS')));

                //content BLOC TABLEAU
                $infosCartouche = (new Atexo_Message())->getInfosMailsChorus($contrat, $numOrdre, $guest, $corpsMsg, $titre, $logger);
                $corps = str_replace('ENTITE_PUBLIQUE_INFO', $infosCartouche['ENTITE_PUBLIQUE']['INFO'], $corps);
                $corps = str_replace('ENTITE_PUBLIQUE_LIB', $infosCartouche['ENTITE_PUBLIQUE']['LIB'], $corps);
                $corps = str_replace('ENTITE_ACHAT_LIB', $infosCartouche['ENTITE_ACHAT']['LIB'], $corps);
                $corps = str_replace('ENTITE_ACHAT_INFO', $infosCartouche['ENTITE_ACHAT']['INFO'], $corps);
                $corps = str_replace('INTITULE_MARCHE_INFO', $infosCartouche['INTITULE_MARCHE']['INFO'], $corps);
                $corps = str_replace('INTITULE_MARCHE_LIB', $infosCartouche['INTITULE_MARCHE']['LIB'], $corps);
                $corps = str_replace('REFERENCE_MARCHE_INFO', $infosCartouche['REFERENCE_MARCHE']['INFO'], $corps);
                $corps = str_replace('REFERENCE_MARCHE_LIB', $infosCartouche['REFERENCE_MARCHE']['LIB'], $corps);
                $corps = str_replace('OBJET_MARCHE_INFO', $infosCartouche['OBJET_MARCHE']['INFO'], $corps);
                $corps = str_replace('OBJET_MARCHE_LIB', $infosCartouche['OBJET_MARCHE']['LIB'], $corps);
                $corps = str_replace('TITULAIRE_MARCHE_INFO', $infosCartouche['TITULAIRE_MARCHE']['INFO'], $corps);
                $corps = str_replace('TITULAIRE_MARCHE_LIB', $infosCartouche['TITULAIRE_MARCHE']['LIB'], $corps);
                $corps = str_replace('DATE_NOTIFICATION_INFO', $infosCartouche['DATE_NOTIFICATION']['INFO'], $corps);
                $corps = str_replace('DATE_NOTIFICATION_LIB', $infosCartouche['DATE_NOTIFICATION']['LIB'], $corps);
                $corps = str_replace('NUMERO_ENVOI_INFO', $infosCartouche['NUMERO_ENVOI']['INFO'], $corps);
                $corps = str_replace('NUMERO_ENVOI_LIB', $infosCartouche['NUMERO_ENVOI']['LIB'], $corps);
                $corps = str_replace('NUMERO_EJ_INFO', $infosCartouche['NUMERO_EJ']['INFO'], $corps);
                $corps = str_replace('NUMERO_EJ_LIB', $infosCartouche['NUMERO_EJ']['LIB'], $corps);

                //Texte FOOTER
                $corps = str_replace('LIEN_ACCES_ECHANGE_LIB', $infosCartouche['LIEN_ACCES_ECHANGE']['LIB'], $corps);
                $corps = str_replace('LIEN_ACCES_ECHANGE_INFO', $infosCartouche['LIEN_ACCES_ECHANGE']['INFO'], $corps);

                //Texte FOOTER
                $corps = str_replace('CORDIALEMENT', $infosCartouche['CORDIALEMENT'], $corps);
                $corps = str_replace('BAS_PAGE_MAIL', $infosCartouche['BAS_PAGE_MAIL'], $corps);

                //HEADER
                $corps = str_replace('IMAGE_BANDEAU', $infosCartouche['IMAGE_BANDEAU'], $corps);

                //Texte CONTENU
                $corps = str_replace('SALUTATION', $infosCartouche['SALUTATION'], $corps);
                $corps = str_replace('CORPS_MAIL', $infosCartouche['CORPS_MAIL'], $corps);

                //Titre OBJET
                $corps = str_replace('TITRE', $infosCartouche['TITRE'], $corps);

                //Titre
                $corps = str_replace('PLATEFORME_ACHAT_ETAT', $infosCartouche['PLATEFORME_ACHAT_ETAT'], $corps);

                if ($guest instanceof Atexo_Consultation_GuestVo) {
                    Atexo_Message::simpleMail($from, $guest->getMailAgent(), $titre, $corps, '', '', false, true);
                } else {
                    $logger->error("Le destinataire n'est pas une instance de Atexo_Consultation_GuestVo : " . print_r($guest, true));

                    return false;
                }
            } catch (\Exception $e) {
                $erreur = 'Erreur lors de la construction du mail a envoyer aux agents : [id_contrat=' . $contrat['uuid'] . ' , org=' . $contrat['organisme'] . " , numOrdre=$numOrdre , from='$from' , subjet='$titre']" . PHP_EOL . "Corps message : $corpsMsg" . PHP_EOL . 'Agent destinataire : ' . print_r($guest, true);
                $erreur .= PHP_EOL . 'Erreur: ' . $e->getMessage() . PHP_EOL . 'Trace:' . $e->getTraceAsString();
                $logger->error($erreur);

                return false;
            }
        } else {
            $logger->error("Le contrat n'est pas une instance de CommonTContratTitulaire : " . print_r($contrat, true));

            return false;
        }
    }

    /**
     * Permet d'envoyer le message d'alerte a l'agent en cas d'erreur de generation de flux CHORUS.
     *
     * @param $echange
     * @param $connexion
     * @param null $logger
     *
     * @return bool
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function sendCourrierErreurGenerationFluxChorusAgent($echange, $connexion, $logger = null)
    {
        if (empty($logger)) {
            $logger = Atexo_LoggerManager::getLogger('chorus');
        }

        if ($echange instanceof CommonChorusEchange) {
            try {
                $contrat = CommonTContratTitulaireQuery::create()->filterByIdContratTitulaire($echange->getIdDecision())->filterByOrganisme($echange->getOrganisme())->findOne($connexion);
                if ($contrat instanceof CommonTContratTitulaire) {
                    $corpsMessage = (new Atexo_Agent_Alertes())->getCorpsMessageAlerteChorusEnvoiFluxNonGenere($contrat);
                    $objetEchange = (new Atexo_Agent_Alertes())->getIntituleMessageAlerteChorusEnvoiFluxNonGenere($contrat, $echange);

                    $guests = (new Atexo_Agent())->retrieveGuestSubscribersChorusAlerts($contrat, $echange->getOrganisme(), $connexion, $logger);
                    if (is_array($guests) && count($guests)) {
                        foreach ($guests as $guest) {
                            //Envoie du mail
                            if ($guest instanceof Atexo_Consultation_GuestVo) {
                                (new Atexo_Agent_Alertes())->sendMailAlertesAgentsChorus(Atexo_Config::getParameter('PF_MAIL_FROM', $echange->getOrganisme()), $guest, $objetEchange, $corpsMessage, $contrat, $echange->getNumOrdre());
                            }
                        }
                    }
                } else {
                    $logger->error("Le contrat n'est pas une instance de CommonTContratTitulaire : " . print_r($contrat, true));

                    return false;
                }
            } catch (\Exception $e) {
                $erreur = "Erreur lors de l'envoi du mail de probleme de generation de flux aux agents : [id_echange = " . $echange->getId() . ' , org = ' . $echange->getOrganisme() . ' , numOrdre = ' . $echange->getNumOrdre();
                $erreur .= PHP_EOL . 'Erreur: ' . $e->getMessage() . PHP_EOL . 'Trace:' . $e->getTraceAsString();
                $logger->error($erreur);

                return false;
            }
        } else {
            $logger->error("L'echange CHORUS n'est pas une instance de CommonChorusEchange : " . print_r($echange, true));

            return false;
        }
    }

    /**
     * Permet de recuperer le corps du mail d'alertes chorus aux agents pour les cas d'envois "flux xml non generes".
     */
    public function getCorpsMessageAlerteChorusEnvoiFluxNonGenere($contrat)
    {
        if ($contrat instanceof CommonTContratTitulaire) {
            return str_replace('[__INTITULE_MARCHE__]', '<strong>&quot;' . $contrat->getIntitule() . '&quot;</strong>', Prado::localize('DEFINE_CORPS_MESSAGE_ENVOI_CHORUS_XML_NON_VALIDE'));
        }

        return '';
    }

    /**
     * Permet de recuperer l'intitule du mail d'alertes chorus aux agents pour les cas d'envois "flux xml non generes".
     */
    public function getIntituleMessageAlerteChorusEnvoiFluxNonGenere($contrat, $echange)
    {
        if ($contrat instanceof CommonTContratTitulaire && $echange instanceof CommonChorusEchange) {
            return str_replace('[__NUM_ENVOI__]', $echange->getNumOrdre(), str_replace('[__REFERENCE_CONTRAT__]', '"' . $contrat->getReferenceLibre() . '"', Prado::localize('DEFINE_INTITULE_MESSAGE_ENVOI_CHORUS_XML_NON_VALIDE')));
        }

        return '';
    }
}
