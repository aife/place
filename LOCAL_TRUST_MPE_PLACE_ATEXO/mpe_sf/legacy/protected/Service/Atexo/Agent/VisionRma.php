<?php

namespace Application\Service\Atexo\Agent;

use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Module;

/**
 * Classe de gestion du profil RMA.
 *
 * @author youssef EL ALAOUI <youssef.elalaoui@atexo.com>
 * @copyright Atexo 2015
 *
 * @version 2015-place
 *
 * @since MPE-4.9
 */
class Atexo_Agent_VisionRma
{
    /**
     * Methode qui gere la visibilite du module vision RMA.
     *
     * @return bool
     */
    public static function isProfilRmaVisible()
    {
        return Atexo_Module::isEnabled('ProfilRma', Atexo_CurrentUser::getCurrentOrganism()) && Atexo_CurrentUser::hasHabilitation('ProfilRma');
    }
}
