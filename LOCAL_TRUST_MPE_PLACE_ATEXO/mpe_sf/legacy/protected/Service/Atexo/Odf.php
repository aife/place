<?php

namespace Application\Service\Atexo;

/**
 * Classe de génération de l'avis.
 *
 * @author Khalid BENAMAR <khalid.benamar@atexo.com>
 * @copyright Atexo 2010
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Odf extends \Odf
{
}
