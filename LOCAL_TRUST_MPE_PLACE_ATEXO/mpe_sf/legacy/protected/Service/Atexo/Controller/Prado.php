<?php

namespace Application\Service\Atexo\Controller;

use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Mobile;
use Prado\Prado;
use Prado\TApplication;
use Prado\TShellApplication;
use Prado\Util\TLogger;

/**
 * Classe de chargement (bootstrapper) du framework prado.
 *
 * @author Guillaume Ponçon <guillaume.poncon@openstates.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Controller_Prado
{
    /**
     * Chargement de Prado.
     *
     * @return bool
     */
    public static function run()
    {
        if (!self::checkWorkspace()) {
            return false;
        }

        $mobile = new Atexo_Mobile();

        if (Atexo_Config::getParameter('MPE_MOBILE') && $mobile->isMpeMobile()) {
            require_once Atexo_Config::getParameter('PRADO_MOBILE_FRAMEWORK_PATH').'/'.Atexo_Config::getParameter('PRADO').'.php';
        } else {
            require_once Atexo_Config::getParameter('PRADO_FRAMEWORK_PATH').'/'.Atexo_Config::getParameter('PRADO').'.php';
        }
        try {
            if (is_file('protected/application.xml')) {
                define('SERVEUR_INDEX', Atexo_Config::getParameter('URL_OOO'));
            }
        } catch (\Exception $e) {
            Prado::log('Erreur Prado.php '.$e->getMessage().$e->getTraceAsString(), TLogger::ERROR, 'Prado.php');
        }

        $application = new TApplication();
        $application->run();

        return true;
    }

    /**
     * Effectue des vérifications sur l'espace de travail afin de voir s'il est compatible Prado.
     *
     * @return bool
     */
    private static function checkWorkspace()
    {
        $basePath = Atexo_Controller_Front::getRootPath().'/mpe';
        $assetsPath = $basePath.'/assets';
        $runtimePath = $basePath.'/protected/runtime';

        $retVal = true;
        if (!is_writable($assetsPath)) {
            trigger_error('Please make sure that the directory '.$assetsPath.' is writable by Web server process.');
            $retVal = false;
        }
        if (!is_writable($runtimePath)) {
            trigger_error('Please make sure that the directory '.$runtimePath.' is writable by Web server process.');
            $retVal = false;
        }

        return $retVal;
    }

    /**
     * Chargement de Prado en mode CLI.
     *
     * @param string $configFile
     *
     * @return TShellApplication
     *
     * @throws \Prado\Exceptions\TConfigurationException
     */
    public static function runCli($configFile = '')
    {
        if ('' == $configFile) {
            $configFile = realpath(__DIR__.'/../../../application.xml');
        }
        $application = new TShellApplication($configFile);
        $application->run();

        return $application;
    }
}
