<?php

namespace Application\Service\Atexo\Controller;

use Application\Service\Atexo\Atexo_Config;

/** Atexo_Controller_Front */
require_once 'Front.php';

/**
 * Gestion du projet mpe en ligne de commandes.
 */
class Atexo_Controller_RestServer
{
    public static function run()
    {
        $infos = self::getInformationsFromUri($_SERVER['REQUEST_URI']);
        //print_r($infos); exit;
        $restAction = self::loadRoutes($infos);
        $requestMethod = strtolower($_SERVER['REQUEST_METHOD']);
        // Gestion TimeZone
//         if (Atexo_Config::getParameter('TIMEZONE') !== 'Europe/Paris') {
        if (Atexo_Config::getParameter('TIMEZONE') !== date_default_timezone_get()) {
            date_default_timezone_set(Atexo_Config::getParameter('TIMEZONE'));
        }
        // On lance une application prado en mode Shell
        $app = Atexo_Controller_Prado::runCli();

        // On force la langue par défaut à FR
        $globalization = $app->Modules['globalization'];
        $globalization->setTranslationCatalogue('messages.fr');

        $ressource = new $restAction();
        $result = $ressource->$requestMethod($infos['request']);
        echo $result;
        exit;
    }

    public function loadRoutes($infos)
    {
        return 'Atexo_Rest_Action_'.$infos['action'];
    }

    public function getInformationsFromUri($uri)
    {
        $reqUril = explode('/rest', $uri);
        $urlarray = explode('?', $reqUril[1]);
        $url = explode('/', trim($urlarray[0], '/'));
        //print_r($url); exit;
        $action = $url[0];
        array_shift($url);
        //print_r($url); exit;

        $request = $url;

        return ['action' => $action, 'request' => $request];
    }
}
