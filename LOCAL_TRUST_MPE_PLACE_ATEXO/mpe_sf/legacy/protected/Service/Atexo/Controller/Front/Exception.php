<?php

namespace Application\Service\Atexo\Controller\Front;

use Application\Service\Atexo\Controller\Atexo_Controller_Exception;

/**
 * Exception personnalisée du contrôleur frontal.
 *
 * @author Guillaume Ponçon <guillaume.poncon@openstates.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Controller_Front_Exception extends Atexo_Controller_Exception
{
}
