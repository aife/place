<?php

namespace Application\Service\Atexo\Controller;

use Application\Service\Atexo\Atexo_Exception;
use Exception;

/**
 * Exception spécifique du composant contrôleur d'Atexo.
 *
 * @author Guillaume Ponçon <guillaume.poncon@openstates.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Controller_Exception extends Atexo_Exception
{
}
