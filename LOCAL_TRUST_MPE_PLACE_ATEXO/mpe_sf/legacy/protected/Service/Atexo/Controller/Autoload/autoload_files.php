<?php

namespace Application\Service\Atexo\Controller\Autoload;

$vendorDir = dirname(__DIR__, 3);
$baseDir = dirname($vendorDir);

return [
    'c964ee0ededf28c96ebd9db5099ef910' => $vendorDir.'/guzzlehttp/promises/src/functions_include.php',
    'a0edc8309cc5e1d60e3047b5df6b7052' => $vendorDir.'/guzzlehttp/psr7/src/functions_include.php',
    '37a3dc5111fe8f707ab4c132ef1dbc62' => $vendorDir.'/guzzlehttp/guzzle/src/functions_include.php',
    '2c102faa651ef8ea5874edb585946bce' => $vendorDir.'/swiftmailer/swift_required.php',
];
