<?php

namespace Application\Service\Atexo\Controller\Autoload;

$vendorDir = dirname(__DIR__, 3);
$baseDir = dirname($vendorDir);

return [
    'Psr\\Http\\Message\\' => [$vendorDir.'/psr/http-message/src'],
    'GuzzleHttp\\Psr7\\' => [$vendorDir.'/guzzlehttp/psr7/src'],
    'GuzzleHttp\\Promise\\' => [$vendorDir.'/guzzlehttp/promises/src'],
    'GuzzleHttp\\' => [$vendorDir.'/guzzlehttp/guzzle/src'],
    'Atexo\\WebServiceApiGouvEntreprise\\' => [$vendorDir.'/Atexo/WebServiceApiGouvEntreprise'],
    'Atexo\\Traits\\' => [$vendorDir.'/Atexo/Traits'],
    'Symfony\\Component\\Yaml\\' => [$vendorDir.'/yaml'],
    'Symfony\\Component\\Yaml\\Exception\\' => [$vendorDir.'/yaml/Exception'],
    'Symfony\\Component\\Filesystem\\' => [$vendorDir.'/filesystem'],
    'Symfony\\Component\\Dotenv' => [$vendorDir.'/dotenv'],
    'Symfony\\Component\\Filesystem\\Exception\\' => [$vendorDir.'/filesystem/Exception'],
];
