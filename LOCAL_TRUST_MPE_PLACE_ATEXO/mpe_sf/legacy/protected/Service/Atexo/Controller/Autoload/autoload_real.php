<?php

namespace Application\Service\Atexo\Controller\Autoload;

class AtexoControllerAutoloaderInit
{
    private static $loader;

    public static function loadClassLoader($class)
    {
        if ('Atexo\Controller\Autoload\ClassLoader' === $class) {
            require __DIR__.'/ClassLoader.php';
        }
    }

    public static function getLoader()
    {
        if (null !== self::$loader) {
            return self::$loader;
        }

        spl_autoload_register(['AtexoControllerAutoloaderInit', 'loadClassLoader'], true, true);
        self::$loader = $loader = new \Atexo\Controller\Autoload\ClassLoader();
        spl_autoload_unregister(['AtexoControllerAutoloaderInit', 'loadClassLoader']);

        $useStaticLoader = false; //PHP_VERSION_ID >= 50600 && !defined('HHVM_VERSION');
        if ($useStaticLoader) {
            require_once __DIR__.'/autoload_static.php';

            call_user_func(\Atexo\Controller\Autoload\AtexoControllerAutoloaderInit::getInitializer($loader));
        } else {
            $map = require __DIR__.'/autoload_namespaces.php';
            foreach ($map as $namespace => $path) {
                $loader->set($namespace, $path);
            }

            $map = require __DIR__.'/autoload_psr4.php';
            foreach ($map as $namespace => $path) {
                $loader->setPsr4($namespace, $path);
            }

            $classMap = require __DIR__.'/autoload_classmap.php';
            if ($classMap) {
                $loader->addClassMap($classMap);
            }
        }

        $loader->register(true);

        if ($useStaticLoader) {
            $includeFiles = Atexo\Controller\Autoload\AtexoControllerAutoloaderInit::$files;
        } else {
            $includeFiles = require __DIR__.'/autoload_files.php';
        }
        foreach ($includeFiles as $fileIdentifier => $file) {
            atexoRequire($fileIdentifier, $file);
        }

        return $loader;
    }
}

function atexoRequire($fileIdentifier, $file)
{
    if (empty($GLOBALS['__composer_autoload_files'][$fileIdentifier])) {
        require $file;

        $GLOBALS['__composer_autoload_files'][$fileIdentifier] = true;
    }
}
