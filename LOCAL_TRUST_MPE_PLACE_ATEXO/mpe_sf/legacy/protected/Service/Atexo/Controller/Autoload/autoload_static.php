<?php

namespace Application\Service\Atexo\Controller\Autoload;

class AtexoStaticInit
{
    public static array $files = [
        'c964ee0ededf28c96ebd9db5099ef910' => __DIR__.'/..'.'/guzzlehttp/promises/src/functions_include.php',
        'a0edc8309cc5e1d60e3047b5df6b7052' => __DIR__.'/..'.'/guzzlehttp/psr7/src/functions_include.php',
        '37a3dc5111fe8f707ab4c132ef1dbc62' => __DIR__.'/..'.'/guzzlehttp/guzzle/src/functions_include.php',
        '2c102faa651ef8ea5874edb585946bce' => __DIR__.'/..'.'/swiftmailer/swift_required.php',
    ];

    public static array $prefixLengthsPsr4 = [
        'P' => [
            'Psr\\Http\\Message\\' => 17,
        ],
        'G' => [
            'GuzzleHttp\\Psr7\\' => 16,
            'GuzzleHttp\\Promise\\' => 19,
            'GuzzleHttp\\' => 11,
        ],
    ];

    public static array $prefixDirsPsr4 = [
        'Psr\\Http\\Message\\' => [
            0 => __DIR__.'/..'.'/psr/http-message/src',
        ],
        'GuzzleHttp\\Psr7\\' => [
            0 => __DIR__.'/..'.'/guzzlehttp/psr7/src',
        ],
        'GuzzleHttp\\Promise\\' => [
            0 => __DIR__.'/..'.'/guzzlehttp/promises/src',
        ],
        'GuzzleHttp\\' => [
            0 => __DIR__.'/..'.'/guzzlehttp/guzzle/src',
        ],
    ];

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = AtexoStaticInit::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = AtexoStaticInit::$prefixDirsPsr4;
        }, null, ClassLoader::class);
    }
}
