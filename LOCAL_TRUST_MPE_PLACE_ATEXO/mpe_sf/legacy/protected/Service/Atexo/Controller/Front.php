<?php

namespace Application\Service\Atexo\Controller;

use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_MultiDomaine;
use Application\Service\Atexo\Atexo_Util;

/**
 * Contrôleur frontal de toute application mpe.
 *
 * @author Guillaume Ponçon <guillaume.poncon@openstates.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Controller_Front
{
    // Codes d'information du routeur de fichiers statiques
    const THEMES_NOT_FOUND = false;
    const THEMES_ORGANISM = 0x01;
    const THEMES_PORTAL = 0x02;
    const THEMES_MPE = 0x04;

    private static $currentOrganism = null;
    private static $languesThemeSpecifique = null;
    private static $mpePath = null;

    /**
     * Fonction de raccourcis pour le chemin dynamique de l'image.
     *
     * @param bool $isRelatif => permet de définir si l'on retourne le chemin vers le theme en relatif si == true, ou en absolue si == false
     * @param int  $cdnCible  => lorsque CDN est activé, permet de définir que url de CDN utilisée dans celle fournie, si la cible n'existe pas ou n'est pas fournie le premier CDN fourni est alors utilisé
     *
     * @return string
     *
     * @author Grégory CHEVRET <gregory.chevret@atexo.com>
     */
    public static function t($isRelatif = false, $cdnCible = null)
    {
        $option = ['slash' => true];
        $pfUrl = Atexo_MultiDomaine::getDomaine(Atexo_Config::getParameter('PF_URL_REFERENCE'), $option);
        if (Atexo_Config::getParameter('ATEXO_MPE_PATH') && !$isRelatif && 'HM' != Atexo_Config::getParameter('PF_TYPE')) {
            return $pfUrl.Atexo_Controller_Front::getThemesPath();
        } elseif ($isRelatif || 'HM' == Atexo_Config::getParameter('PF_TYPE')) {
            return '/'.Atexo_Controller_Front::getThemesPath();
        }

        return Atexo_Controller_Front::getThemesPath();
    }

    public static function urlPf()
    {
        $url = Atexo_Config::getParameter('PF_URL_ENTREPRISE');
        if (1 === preg_match('/'.$_SERVER['SERVER_NAME'].'/', Atexo_Config::getParameter('PF_URL_AGENT'))) {
            $url = Atexo_Config::getParameter('PF_URL_AGENT');
        }

        return $url;
    }

    /**
     * Retourne le chemin pour le theme
     * Si un dossier organisme existe dans /themes alors on va chercher dans /themes/orga sinon /themes
     * TODO : portal.
     */
    public static function getThemesPath()
    {
        $langChoisie = null;
        static $initialized = false;
        static $returnPath = 'themes';
        if (true === $initialized) {
            return $returnPath;
        }
        if ($_GET['lang']) {
            $langChoisie = Atexo_Util::atexoHtmlEntities($_GET['lang']);
        } elseif (Atexo_CurrentUser::readFromSession('lang')) {
            $langChoisie = Atexo_CurrentUser::readFromSession('lang');
        }
        $paramLang = false;
        if (self::hasThemeSpecifique($langChoisie)) {
            $paramLang = $langChoisie;
        }
        if (false !== $paramLang && file_exists(self::getRootPath().'/themes/'.$paramLang)) {
            $sub = '/'.$paramLang;

            return "themes$sub";
        }
        if (null === self::$currentOrganism) {
            $currentOrganism = Atexo_CurrentUser::getCurrentOrganism();
            if ('' == trim($currentOrganism)) {
                $currentOrganism = false;
            }
            self::$currentOrganism = $currentOrganism;
        }
        if (false !== self::$currentOrganism && file_exists(self::getRootPath().'/themes/'.self::$currentOrganism)) {
            $sub = '/'.self::$currentOrganism;
        } elseif (isset($_COOKIE['design'])) {
            $sub = '/'.$_COOKIE['design'];
        } elseif ('' != Atexo_Config::getParameter('PF_URL_DEMAT') &&
            Atexo_Config::getParameter('PF_URL_DEMAT') == Atexo_Util::getAbsoluteUrl() && file_exists(self::getRootPath().'/themes/'.'demat')) {
            $sub = '/demat';
        } elseif (1 == Atexo_Config::getParameter('CONFIG_REDAC') && isset($_COOKIE['themeGraphiquePersonnel'])) {
            $sub = $sub = '/'.$_COOKIE['themeGraphiquePersonnel'];
        } else {
            $sub = '';
        }
        $initialized = true;
        $returnPath = "themes$sub";

        return "themes$sub";
    }

    /**
     * Retourne le chemin absolu vers la racine de l'application MPE.
     *
     * @return string
     */
    public static function getRootPath()
    {
        if (null === self::$mpePath) {
            self::$mpePath = realpath(__DIR__.'/../../../../..');
        }

        return self::$mpePath;
    }

    public static function hasThemeSpecifique($langue)
    {
        $langueThemeSpecifique = false;
        if (function_exists('apc_fetch')) {
            $langueThemeSpecifique = apc_fetch('langueThemeSpecifique');
        } else {
            $langueThemeSpecifique = self::$languesThemeSpecifique;
        }
        if (!$langueThemeSpecifique) {
            $sql = "Select langue, theme_specifique from Langue where active='1'";
            $statement = Atexo_Db::getLinkCommon(true)->query($sql);
            while ($row = $statement->fetch(\PDO::FETCH_ASSOC)) {
                $langueThemeSpecifique[$row['langue']] = $row['theme_specifique'];
            }
            if (function_exists('apc_add')) {
                apc_add('langueThemeSpecifique', $langueThemeSpecifique, Atexo_Config::getParameter('TTL_PRADO_CACHE'));
            } else {
                self::$languesThemeSpecifique = $langueThemeSpecifique;
            }
        }

        if ($langueThemeSpecifique[$langue]) {
            return true;
        } else {
            return false;
        }
    }
}
