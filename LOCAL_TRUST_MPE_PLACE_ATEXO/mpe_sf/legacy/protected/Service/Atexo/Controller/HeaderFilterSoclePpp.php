<?php

namespace Application\Service\Atexo\Controller;

/**
 * Classe de verification des headers socle PPP.
 */
$old_include_path = get_include_path();
set_include_path($old_include_path.PATH_SEPARATOR.realpath(__DIR__.'/../../../var/mapping-propel/mpe/').PATH_SEPARATOR.realpath(__DIR__.'/../../../var/mapping-propel/'));
include_once 'propel/Propel.php';

function myautoload($class)
{
    include strtr($class, '_', DIRECTORY_SEPARATOR).'.php';
}
spl_autoload_register('myautoload');

if (!session_id()) {
    session_start();
}

$headersArray = array_change_key_case(getallheaders(), CASE_UPPER);
if (isset($headersArray['EXTERNALID']) && isset($headersArray['USERTYPE'])) {
    if (isset($_SESSION['EXTERNALID']) && isset($_SESSION['USERTYPE'])) {
        if ($headersArray['EXTERNALID'] != $_SESSION['EXTERNALID'] || $headersArray['USERTYPE'] != $_SESSION['USERTYPE']) {
            $_SESSION = [];
            session_unset();
            session_destroy();
            session_regenerate_id(true);
        }
    }
}

spl_autoload_unregister('myautoload');
set_include_path($old_include_path);
