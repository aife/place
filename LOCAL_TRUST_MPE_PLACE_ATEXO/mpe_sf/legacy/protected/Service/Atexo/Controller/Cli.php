<?php

namespace Application\Service\Atexo\Controller;

use Application\Service\Atexo\Atexo_Cli;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * Gestion du projet mpe en ligne de commandes.
 */
class Cli extends Atexo_Cli
{
    /**
     * Bootstrap de la fonctionnalite.
     */
    public static function run()
    {
        $app = null;
        $isPrado = false;
        if ('install' != $_SERVER['argv'][1]) {
            $isPrado = true;
        }
        if ($isPrado) {
            // Gestion TimeZone
            if (Atexo_Config::getParameter('TIMEZONE') !== date_default_timezone_get()) {
                date_default_timezone_set(Atexo_Config::getParameter('TIMEZONE'));
            }
            session_start(); // Car on a enlevé l'initialisation de session dans MpeTHttpSession

            // On lance une application prado en mode Shell
            $app = Atexo_Controller_Prado::runCli();
            // On force la langue par defaut a  FR
            $globalization = $app->Modules['globalization'];
            $globalization->setTranslationCatalogue('messages.fr');
        } else {
            chdir(realpath(__DIR__.'/../../../../'));
        }

        $command = ucfirst($_SERVER['argv'][1]);
        if (self::isLocked()) {
            Prado::log('Le script est en cours d\'execution ... Veuillez patienter ...', TLogger::ERROR, 'cli');
            echo 'Le script est en cours d\'execution ... Veuillez patienter ...';

            return;
        }
        if ('Install' != $command
            && 'Clean' != $command
        ) {
            self::lock();
        }

        // Si on a pas de parametres passe en argument, on affiche l'aide (qui force le exit)
        if ($_SERVER['argc'] < 2) {
            self::helpAction();
        }

        $className = 'Application\Cli\Cli_'.$command;
        $return_code = 0;
        $cliFile = realpath(__DIR__.'/../../../Cli/'.$command.'.php');
        if (is_file($cliFile)) {
            include $cliFile;

            try {
                $class = new \ReflectionMethod($className, 'run');
                if ($isPrado) {
                    Prado::log("Lancement commande $command ", TLogger::INFO, 'cli');
                }

                $time_start = microtime(true);
                $class->invoke(null);
                $time_end = microtime(true);
                $time = round($time_end - $time_start, 4);
                if ($isPrado) {
                    Prado::log(
                        "Fin commande $command in ".Atexo_Util::GetElapsedTime($time),
                        TLogger::INFO,
                        'cli'
                    );
                }
            } catch (\SoapFault $e) {
                Prado::log(
                    "Uncaught Exception ! Script $className interrompu ! Erreur WebService : ".$e,
                    TLogger::FATAL,
                    'cli'
                );
                //Deverrouillage du ficher $fileLock
                self::unlock();

                $return_code = 1;
            } catch (\Exception $e) {
                if ($isPrado) {
                    Prado::log(
                        "Uncaught Exception ! Script $className interrompu ! Exception : ".$e,
                        TLogger::FATAL,
                        'cli'
                    );
                    //Deverrouillage du ficher $fileLock
                    self::unlock();
                } else {
                    echo $e;
                }
                $return_code = 2;
            }

            //Deverrouillage du ficher $fileLock
            self::unlock();
        } else {
            //Deverrouillage du ficher $fileLock
            self::unlock();
            self::displayError('command unknown', true);
        }
        if ($isPrado) {
            // On logge les infos memoire pour savoir combien le script a consomme
            self::logMemoryInfo();
            // On force la fin de l'appel prado pour forcer l'ecriture des logs prado par exemple
            $app->onEndRequest();
            self::changeOwners();
        }
        exit($return_code);
    }

    /**
     * Permet de verifier si un fichier est verrouille.
     *
     * @return bool
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public static function isLocked()
    {
        return is_file(self::getPathFileLock());
    }

    /**
     * Permet de construire le nom du fichier de verrouillage.
     *
     * @return string : chemin complet du fichier de verrouillage
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public static function getPathFileLock()
    {
        try {
            $lockRootDir = Atexo_Config::getParameter('BASE_ROOT_DIR').'/lock-files/';
            $arguments = $_SERVER['argv'];
            $command = ucfirst($_SERVER['argv'][1]);
            if ('Purge' == $command) {
                $arguments[0] = php_uname('n');
            } else {
                unset($arguments[0]);
            }

            return realpath($lockRootDir).DIRECTORY_SEPARATOR.'cli_'.
                Atexo_Util::clean(str_replace('/', '_', implode('_', $arguments))).'.lock';
        } catch (\Exception $e) {
            echo 'Erreur construction nom du fichier lock : '.$e->getMessage();
        }
    }

    /**
     * Permet de verrouiller un fichier.
     *
     * @return bool
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public static function lock()
    {
        $file = null;
        if (1 == Atexo_Config::getParameter('USE_LOCK_FILE_CRON')) {
            try {
                $lockRootDir = Atexo_Config::getParameter('BASE_ROOT_DIR').'/lock-files/';
                if (!file_exists($lockRootDir)) {
                    mkdir($lockRootDir, 0755, true);
                }
                $file = self::getPathFileLock();
                if (!file_put_contents($file, 'verrouillage du fichier')) {
                    Prado::log("Impossible de creer/ecrire le fichier $file", TLogger::ERROR, 'cli');
                    echo "Impossible de creer/ecrire le fichier $file";

                    return false;
                }
                Prado::log('-> Lock "'.$file.'" cree.', TLogger::INFO, 'cli');
                echo '-> Lock "'.$file.'" cree.'.PHP_EOL;

                return true;
            } catch (\Exception $e) {
                Prado::log("Erreur verrouillage du fichier $file : ".$e->getMessage(), TLogger::ERROR, 'cli');
                echo "Erreur verrouillage du fichier $file : ".$e->getMessage();
            }
        }
    }

    /**
     * Permet de deverrouiller un fichier.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public static function unlock()
    {
        $file = self::getPathFileLock();
        if (1 == Atexo_Config::getParameter('USE_LOCK_FILE_CRON')) {
            if (is_file($file)) {
                unlink($file);
                Prado::log('-> Lock "'.$file.'" supprime.', TLogger::INFO, 'cli');
                echo '-> Lock "'.$file.'" supprime.'.PHP_EOL;
            }
        }
    }

    public static function logMemoryInfo()
    {
        if (!function_exists('memory_get_usage')) {
            return '';
        }
        if (!function_exists('memory_get_peak_usage')) {
            return '';
        }
        Prado::log(
            'Memoire: '.Atexo_Util::GetSizeName(memory_get_usage())
            .' (reelle: '.Atexo_Util::GetSizeName(memory_get_usage(true)).')'
            .' - Peak: '.Atexo_Util::GetSizeName(memory_get_peak_usage())
            .' (reelle: '.Atexo_Util::GetSizeName(memory_get_peak_usage(true)).')',
            TLogger::INFO,
            'cli'
        );
    }

    private static function changeOwners()
    {
        // Change les droits du dossier de logs si jamais le script est lance en root
        if (0 == posix_getuid()) {
            $user = posix_getpwuid(fileowner(Atexo_Config::getParameter('BASE_ROOT_DIR')));
            if ('root' != $user['name']) {
                if (is_file('./protected/application.xml')) {
                    shell_exec("chown -R {$user['name']} ".'./protected/application.xml');
                }
                if (is_dir(Atexo_Config::getParameter('BASE_ROOT_DIR'))) {
                    shell_exec(
                        "chown -R {$user['name']} ".Atexo_Config::getParameter(
                            'BASE_ROOT_DIR'
                        ).'/common/indexLucene'
                    );
                    shell_exec("chown {$user['name']} ".Atexo_Config::getParameter('BASE_ROOT_DIR').'/logs');
                    shell_exec('mkdir -p '.Atexo_Config::getParameter('BASE_ROOT_DIR').'/tmp');
                    shell_exec("chown {$user['name']} ".Atexo_Config::getParameter('BASE_ROOT_DIR').'/tmp');
                }
                if (is_dir(Atexo_Config::getParameter('LOG_DIR'))) {
                    shell_exec("chown -R {$user['name']} ".Atexo_Config::getParameter('LOG_DIR'));
                }
                if (is_dir('./protected/'.Atexo_Config::getParameter('LOG_DIR'))) {
                    shell_exec("chown -R {$user['name']} ".'./protected/'.Atexo_Config::getParameter('LOG_DIR'));
                }
                if (is_file('./protected/runtime/application.xml-3.1.1/sqlite.cache')) {
                    shell_exec("chown -R {$user['name']} ".'./protected/runtime/application.xml-3.1.1/*.cache');
                }
                if (is_file('./protected/runtime/application.xml-3.1.10/sqlite.cache')) {
                    shell_exec("chown -R {$user['name']} ".'./protected/runtime/application.xml-3.1.10/*.cache');
                }
            }
        }
    }

    /**
     * Permet de catcher les fatal error
     * (Source: http://insomanic.me.uk/post/229851073/php-trick-catching-fatal-errors-eerror-with-a).
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public static function fatalErrorShutdownHandler()
    {
        $last_error = error_get_last();
        if (in_array($last_error['type'], [1, 4, 16, 64, 256, 4096])) {
            $message = '#### Erreur Technique #### Cron ['.self::getPathFileLock().']['.date('Y-m-d H:i:s').']';
            $message .= PHP_EOL.print_r($last_error, true);
            Prado::log($message, TLogger::ERROR, 'cli');
            echo $message;
            Cli::unlock();
        }
    }
}
