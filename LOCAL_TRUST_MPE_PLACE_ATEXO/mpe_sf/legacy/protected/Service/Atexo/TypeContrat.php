<?php

namespace Application\Service\Atexo;

use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonTTypeContrat;
use Application\Propel\Mpe\CommonTTypeContratQuery;

/**
 * Class Atexo_TypeContrat.
 */
class Atexo_TypeContrat
{
    /**
     * @param $idTypeContrat
     *
     * @return CommonTTypeContrat
     *
     * @throws PropelException
     */
    public function retrieveContratOneByIdTypeContrat($idTypeContrat)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $commonTTypeContratQuery = new CommonTTypeContratQuery(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $typeContrat = $commonTTypeContratQuery->findOneByIdTypeContrat($idTypeContrat, $connexion);

        return $typeContrat;
    }

    public function retrieveContratConcession($concession = 0)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $commonTTypeContratQuery = new CommonTTypeContratQuery(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $typeContrat = $commonTTypeContratQuery->findByConcession($concession);

        return $typeContrat;
    }

    public function retrieveTypeContratByLibelle($label)
    {
        $commonTTypeContratQuery = new CommonTTypeContratQuery(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $typeContrat = $commonTTypeContratQuery->findOneByLibelleTypeContrat($label);

        return $typeContrat;
    }
}
