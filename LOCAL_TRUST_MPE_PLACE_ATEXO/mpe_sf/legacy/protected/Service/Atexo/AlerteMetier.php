<?php

namespace Application\Service\Atexo;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonAlerteMetier;
use Application\Propel\Mpe\CommonAlerteMetierPeer;
use Application\Propel\Mpe\CommonConfigurationAlertePeer;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use PhpRules\PhpRules_RulesCalculator;
use Prado\Prado;

/**
 * Classe de manipulation des alertes metiers.
 *
 * @author Ezziani Loubna <loubna.ezziani@atexo.com>
 * @copyright Atexo 2012
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_AlerteMetier
{
    /**
     * lancer l'alerte en fonction du critère proposé.
     *
     * @param  $operation, $object
     *
     * @return array of alerte object
     */
    public static function lancerAlerte($operation, $object, $connexionCom = null)
    {
        // retourne les alertes active d'une operation
        $alertes = Atexo_AlerteMetier::getAlerteActiveByOperation($operation);
        $alertesBloquante = [];
        if (null == $connexionCom) {
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        }
        if (is_array($alertes)) {
            foreach ($alertes as $alerte) {
                $result = [];
                $xml = $alerte->getXml();
                $rules = new PhpRules_RulesCalculator();
                $rules->setXml($xml);
                $rules->addVar('OBJECT', $object);

                $result = $rules->getResultFromXmlRules();

                if (is_array($result) && count($result)) {
                    // tester s'il y a une boucle
                    $key = '';
                    $resultBoucle = [];
                    if ($result['keyBoucle']) {
                        $key = $result['keyBoucle'];
                        $resultBoucle = $result[$key];
                    } else {
                        $key = 'IdAlerte';
                        $resultBoucle = [$alerte->getId()];
                    }
                    $resultInsert = [];
                    $resultI = [];
                    foreach ($resultBoucle as $v) {
                        foreach ($result as $set => $value) {
                            if ($set == $key) {
                                $resultI[$set] = $v;
                            } elseif ('alerteExiste' == $set) {
                                if (is_array($result[$set])) {
                                    // dans le cas où on a une boucle result['alerteExiste'] est un array
                                    $resultI[$set] = $result[$set][$v];
                                } else {
                                    $resultI[$set] = $value;
                                }
                            } else {
                                $resultI[$set] = $value;
                            }
                        }
                        $resultInsert[] = $resultI;
                    }
                    foreach ($resultInsert as $valeur) {
                        $alerteExisteOpen = Atexo_AlerteMetier::retrieveAlerteMetierOpen($alerte->getId(), $valeur['organisme'], $valeur['reference'], $valeur['lot'], $valeur['serviceId']);
                        // vérifier s'il y a une alerte ouverte et l'alerte est déclenchée
                        if (!$alerteExisteOpen && $valeur['alerteExiste']) {
                            if ($alerte->getBloquant()) {
                                $alertesBloquante[] = Prado::localize($alerte->getMessage());
                            } else {
                                $newAlerte = new CommonAlerteMetier();
                                $newAlerte->setIdAlerte($alerte->getId());
                                $newAlerte->setDateCreation(date('Y-m-d H:i:s'));
                                $newAlerte->setOrganisme($valeur['organisme']);
                                $newAlerte->setReferenceCons($valeur['reference']);
                                $newAlerte->setServiceId($valeur['serviceId']);
                                $newAlerte->setMessage($alerte->getMessage());
                                $newAlerte->setLot($valeur['lot']);
                                $newAlerte->save($connexionCom);
                            }
                        } elseif (!$valeur['alerteExiste'] && $alerteExisteOpen) {
                            // Fermer l'alerte s'il existe on état ouvert
                            $alerteExisteOpen[0]->setCloturee(1);
                            $alerteExisteOpen[0]->setDateCloture(date('Y-m-d H:i:s'));
                            $alerteExisteOpen[0]->save($connexionCom);
                        }
                    }
                }
            }
        }
        $open = true;
        if ($object) {
            $alertesObjet = $object->getAlertes($open);
            if (is_array($alertesObjet) && count($alertesObjet)) {
                $object->setAlerte(1);
            } else {
                $object->setAlerte(0);
            }
            $object->save($connexionCom);
        }

        return $alertesBloquante;
    }

    public static function getAlerteActiveByOperation($operation)
    {
        $c = new Criteria();
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonConfigurationAlertePeer::OPERATION, '%'.$operation.'#%', Criteria::LIKE);
        $c->add(CommonConfigurationAlertePeer::ACTIVE, 1);
        $confAlerte = CommonConfigurationAlertePeer::doSelect($c, $connexion);
        if ($confAlerte) {
            return $confAlerte;
        }

        return false;
    }

    public static function retrieveAlerteMetierOpen($id, $organisme, $reference, $lot, $service_id)
    {
        $c = new Criteria();
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonAlerteMetierPeer::ID_ALERTE, $id);
        $c->add(CommonAlerteMetierPeer::ORGANISME, $organisme);
        $c->add(CommonAlerteMetierPeer::REFERENCE_CONS, $reference);
        $c->add(CommonAlerteMetierPeer::LOT, $lot);
        $c->add(CommonAlerteMetierPeer::SERVICE_ID, $service_id);
        $c->add(CommonAlerteMetierPeer::CLOTUREE, 2);

        $alertesMetier = CommonAlerteMetierPeer::doSelect($c, $connexion);
        if ($alertesMetier) {
            return $alertesMetier;
        }

        return false;
    }

    // retourne le nobmre des alertes ouvertes concerne les consultations
    public function getNombreAlerteOfConsultation($idAlerte)
    {
        $criteriaVo = new Atexo_Consultation_CriteriaVo();
        $criteriaVo->setIdService(Atexo_CurrentUser::getCurrentServiceId());
        $criteriaVo->setConnectedAgentId(Atexo_CurrentUser::getId());
        $criteriaVo->setAcronymeOrganisme(Atexo_CurrentUser::getCurrentOrganism());
        $criteriaVo->setIdAlerteConsultation($idAlerte);
        $criteriaVo->setAlerteConsultationCloturee(2);
        $nombreOfCons = (new Atexo_Consultation())->search($criteriaVo, true);

        return $nombreOfCons;
    }

    // retourne le nobmre des alertes ouvertes concerne les services
    public function getNombreAlerteOfService($idAlerte)
    {
        $c = new Criteria();
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonAlerteMetierPeer::ID_ALERTE, $idAlerte);
        $c->add(CommonAlerteMetierPeer::ORGANISME, Atexo_CurrentUser::getCurrentOrganism());
        $c->add(CommonAlerteMetierPeer::SERVICE_ID, Atexo_CurrentUser::getIdServiceAgentConnected());
        $c->add(CommonAlerteMetierPeer::CLOTUREE, 2);

        $alertesMetier = CommonAlerteMetierPeer::doSelect($c, $connexion);
        if (is_array($alertesMetier)) {
            return count($alertesMetier);
        }

        return 0;
    }

    // retourne le nobmre des alertes ouvertes concerne la plate forme
    public function getNombreAlerteOfPlateForme($idAlerte)
    {
        $c = new Criteria();
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonAlerteMetierPeer::ID_ALERTE, $idAlerte);
        $c->add(CommonAlerteMetierPeer::CLOTUREE, 2);
        $alertesMetier = CommonAlerteMetierPeer::doSelect($c, $connexion);
        if (is_array($alertesMetier)) {
            return count($alertesMetier);
        }

        return 0;
    }
}
