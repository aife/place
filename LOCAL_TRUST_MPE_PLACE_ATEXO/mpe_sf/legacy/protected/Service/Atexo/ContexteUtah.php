<?php

namespace Application\Service\Atexo;

/**
 * Classe Contexte.
 *
 * Un contexte est un ensemble d?information d?intérêt pour le support ? extraites de l?application
 * cliente lors de la création d?une demande. Ces contextes peuvent être des contextes utilisateur,
 * métier ou technique. Par exemple un contexte technique contient des informations sur le système
 * d?exploitation, le navigateur, la version de Java, etc. de l?utilisateur. Un contexte métier contiendra
 * des informations « métier » tel qu?une référence de consultation, son intitulé, l?URL de la page
 * consultée, etc.
 *
 * @author David Delobel <david.delobel@atexo.com>
 */
class Atexo_ContexteUtah
{
    /**
     * Nom interne du contexte.
     */
    private ?string $nom = null;

    /**
     * Label du contexte (32 chars max).
     */
    private ?string $label = null;

    /**
     * Description du contexte.
     */
    private ?string $description = null;

    /**
     * Tableau associatif de champs du contexte (stocké en Json dans le SGBD).
     */
    private array $champs;

    /**
     * Nombre d'élements à afficher avant de masquer le reste du contexte.
     */
    private ?int $maxVisible = null;

    /**
     * Constructeur de Contexte.
     *
     * @param string|null $nom         Nom interne du contexte
     * @param string|null $label       Label du contexte
     * @param string|null $description Description du contexte
     * @param int|null    $max_visible Nombre d'élements à afficher avant de masquer le reste
     */
    public function __construct($nom = null, $label = null, $description = null, $max_visible = null)
    {
        $this->setNom($nom);
        $this->setLabel($label);
        $this->setDescription($description);
        $this->setMaxVisible($max_visible);
    }

    /**
     * Ajouter un champ au contexte.
     *
     * @param string      $nom_champ           Nom interne du champ
     * @param string      $label               Label du champ
     * @param mixed       $valeur              Valeur du champ
     * @param string      $type_valeur         Type de valeur
     * @param bool        $visible_utilisateur L'utilisateur peut-il voir cette information dans l'interface ?
     * @param bool        $visible_support     Le support peut-il voir cette information dans l'interface ?
     * @param string|null $description         Description du champ
     * @param $extras
     *
     * @return $this
     */
    public function add($nom_champ, $label, $valeur, $type_valeur, $visible_utilisateur = true, $visible_support = true, $description = null, $extras = null)
    {
        if (empty($nom_champ)) {
            throw new \InvalidArgumentException('Le nom du champ doit être défini');
        }
        if (empty($label)) {
            throw new \InvalidArgumentException('Le label du champ doit être défini');
        }
        if (!in_array($type_valeur, self::getTypesValeurs())) {
            throw new \InvalidArgumentException("Le type de valeur '$type_valeur' n'existe pas");
        }
        if (!$visible_utilisateur && !$visible_support) {
            throw new \InvalidArgumentException("Le champ n'est visible à personne");
        }

        $this->champs[$nom_champ] = [
            'label' => $label,
            'description' => $description,
            'valeur' => $valeur,
            'type_valeur' => $type_valeur,
            'visible_utilisateur' => $visible_utilisateur,
            'visible_support' => $visible_support,
            'extras' => $extras,
        ];

        return $this;
    }

    /**
     * Obtenir la valeur de la clé d'un champ.
     *
     * @return mixed
     */
    public function get(string $nom_champ, string $cle)
    {
        if (!array_key_exists($nom_champ, $this->champs)) {
            throw new \InvalidArgumentException("Le champ '$nom_champ' n'est pas défini");
        }

        return $this->champs[$nom_champ][$cle];
    }

    /**
     * Obtenir un champ et toutes ses clés.
     *
     * @param $nom_champ
     *
     * @return mixed
     */
    public function getChamp($nom_champ)
    {
        if (!array_key_exists($nom_champ, $this->champs)) {
            throw new \InvalidArgumentException("Le champ '$nom_champ' n'est pas défini");
        }

        return $this->champs[$nom_champ];
    }

    /**
     * Set nom.
     *
     * @param string $nom
     *
     * @return $this
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom.
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set label.
     *
     * @param string $label
     *
     * @return $this
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label.
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Set description.
     *
     * @param string $description
     *
     * @return $this
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Vérifier si un champ est visible par le support.
     *
     * @param $champ
     *
     * @return bool
     */
    public function isVisibleSupport($champ)
    {
        return true == $champ['visible_support'];
    }

    /**
     * Vérifier si un champ est visible par l'utilisateur.
     *
     * @param $champ
     *
     * @return bool
     */
    public function isVisibleUtilisateur($champ)
    {
        return true == $champ['visible_utilisateur'];
    }

    /**
     * Obtenir tous les champs visibles de l'utilisateur uniquement.
     *
     * @return array
     */
    public function getChampsUtilisateur()
    {
        return array_filter($this->champs, [self::class, 'isVisibleUtilisateur']);
    }

    /**
     * Obtenir tous les champs visibles du support uniquement.
     *
     * @return array
     */
    public function getChampsSupport()
    {
        return array_filter($this->champs, [self::class, 'isVisibleSupport']);
    }

    /**
     * @param int $maxVisible
     */
    public function setMaxVisible($maxVisible)
    {
        $this->maxVisible = $maxVisible;
    }

    /**
     * @return int
     */
    public function getMaxVisible()
    {
        return $this->maxVisible;
    }

    /**
     * Types de valeurs acceptées dans les informations de contexte (extensible, UTAH doit implémenter un renderer selon le type de donnée).
     *
     * @return array
     */
    private function getTypesValeurs()
    {
        return [
            'boolean',  // Booléen true, false
            'integer',  // Nombre entier
            'double',   // Nombre flottant avec un point comme séparateur
            'string',   // Chaîne de caractères
            'array',    // Tableau de données
            'object',   // Objet
            'datetime', // Date/heure au format ISO 8601

            'url', // lien web
            'mailto', // Adresse email (pour faire un href mailto)

            // métier
            'entreprise', // array [nom, siren, code_postal]
            'etablissement',  // array [nom, siret]
            'organisme_achat', // array [entite_publique, entite_achat]
            'adresse', // array [, , ...] (ligne 1, ligne 2, ...)
        ];
    }

    /**
     * Permet de construire un tableau du contexte.
     *
     * @return array
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function toArray()
    {
        $array = [
            'nom' => $this->getNom(),
            'label' => $this->getLabel(),
            'description' => $this->getDescription(),
            'max_visible' => $this->getMaxVisible(),
            'champs' => $this->champs,
        ];

        return $array;
    }
}
