<?php

namespace Application\Service\Atexo;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonValeurReferentiel;
use Application\Propel\Mpe\CommonValeurReferentielPeer;
use Application\Propel\Mpe\CommonValeurReferentielQuery;

/**
 * Classe de manipulation de referentiel base commune.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_CommonReferentiel
{
    /**
     * @param $idReferentiel
     * @param false $ordre
     * @param false $alpha
     * @return array
     */
    public function retrieveValeurReferentielCCAG($idReferentiel, $ordre = false, $alpha = false)
    {
        $valeurReferentielQuery = new CommonValeurReferentielQuery();


        $commonValeurReferentielList = $valeurReferentielQuery->findCachedValeurReferentielCCAG($alpha);

        $resultsArray = [];

        foreach ($commonValeurReferentielList as $item) {
            if ($item->getIdReferentiel() == $idReferentiel) {
                $resultsArray[] = $item;
            }
        }

        if ($ordre) {
            usort($resultsArray, fn($a, $b) => $a->getOrdreAffichage() - $b->getOrdreAffichage());
        }

        return $resultsArray;
    }



    /**
     * Permet de recuperer une valeur referentielle.
     *
     * @param string $idReferentiel : identifiant technique du referentiel
     * @param $ordre
     *
     * @return array
     */
    public function retrieveValeurReferentiel($idReferentiel, $ordre = false)
    {
        $valeurReferentielQuery = new CommonValeurReferentielQuery();
        $commonValeurReferentielList = $valeurReferentielQuery->findCachedValeurReferentiel();

        $resultsArray = [];

        foreach ($commonValeurReferentielList as $item) {
            if ($item->getIdReferentiel() == $idReferentiel) {
                $resultsArray[] = $item;
            }
        }

        if ($ordre) {
            usort($resultsArray, fn($a, $b) => $a->getOrdreAffichage() - $b->getOrdreAffichage());
        }

        return $resultsArray;
    }

    /**
     * @param $arReferentiel
     *
     * @return array
     *
     * @deprecated do not use
     */
    public function retrieveValeurReferentielByArrayRef($arReferentiel)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonValeurReferentielPeer::ID_REFERENTIEL, $arReferentiel, Criteria::IN);
        $result = CommonValeurReferentielPeer::doSelect($c, $connexion);
        if ($result) {
            return $result;
        } else {
            return [];
        }
    }

    public function retrieveValeurReferentielByArrayId($arIds)
    {
        $valeurReferentielQuery = new CommonValeurReferentielQuery();
        $commonValeurReferentielList = $valeurReferentielQuery->findCachedValeurReferentiel();
        $resultsArray = [];
        foreach ($commonValeurReferentielList as $item) {
            if (in_array($item->getId(), $arIds)) {
                $resultsArray[] = $item;
            }
        }

        return $resultsArray;
    }

    /*
     * return ValeurReferentiel By idReferentiel et libelle
     */
    public function retrieveValeurReferentielByLibelle2($idReferentiel, $libelle2)
    {
        $valeurReferentielQuery = new CommonValeurReferentielQuery();
        $commonValeurReferentielList = $valeurReferentielQuery->findCachedValeurReferentiel();

        foreach ($commonValeurReferentielList as $item) {
            if (($item->getIdReferentiel() == $idReferentiel) && ($item->getLibelle2() == $libelle2)) {
                return $item;
            }
        }

        return false;
    }

    /*
     * return ValeurReferentiel By id et le idreferentiel
     * @param $id
     * @param $idReferentiel
     * return CommonValeurReferentiel
     */
    public function retrieveValeurReferentielById($idReferentiel, $id)
    {
        $valeurReferentielQuery = new CommonValeurReferentielQuery();
        $commonValeurReferentielList = $valeurReferentielQuery->findCachedValeurReferentiel();

        foreach ($commonValeurReferentielList as $item) {
            if (($item->getIdReferentiel() == $idReferentiel) && ($item->getId() == $id)) {
                return $item;
            }
        }

        return false;
    }

    /*
     * return ValeurReferentiel By id et le idreferentiel
     * @param $listValeurSub
     * @param $idReferentiel
     * return CommonValeurReferentiel
     */
    public function retrieveValeurReferentielByValeurSub($idReferentiel, $listValeurSub)
    {
        if (is_array($listValeurSub)) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            $c = new Criteria();
            $c->add(CommonValeurReferentielPeer::ID_REFERENTIEL, $idReferentiel, Criteria::EQUAL);
            $c->add(CommonValeurReferentielPeer::VALEUR_SUB, $listValeurSub, Criteria::IN);
            $result = CommonValeurReferentielPeer::doSelect($c, $connexion);

            return $result;
        }
    }

    /*
  * Remplit la liste des Justification
  * @return void
  * @author Fatima Ezzahra IZGUA <fatima.izgua@atexo.com>
  * @version 1.0
  * @since 4.11.0
  * @copyright Atexo 2015
  */
    public function fillListeReferentielKeyLibelle2($referentiel, $selectionner = false)
    {
        $valeur = $this->retrieveValeurReferentiel($referentiel);
        $data = [];
        if ($selectionner) {
            $data['0'] = $selectionner;
        }
        if ($valeur) {
            foreach ($valeur as $oneVal) {
                $data[$oneVal->getLibelle2()] = $oneVal->getLibelleValeurReferentiel();
            }
        }

        return $data;
    }

    /**
     * Permet de recuperer les valeurs du referentiel des statistiques.
     *
     * @param bool $withoutChorus : permet de préciser si on doit récupérer la valeur qui correspond aux stats chorus ou non
     *
     * @return array
     */
    public function retrieveValeurReferentielStats($withoutChorus = true)
    {
        $idReferentiel = Atexo_Config::getParameter('ID_REFERENTIEL_CATEGORIES_STATISTIQUES');
        $idValeurRefCatStatsInterfaceChorus = Atexo_Config::getParameter('ID_VALEUR_REFERENTIEL_CATEGORIES_STATISTIQUES_INTERFACE_CHORUS');

        $commonValeurReferentielList = $this->retrieveValeurReferentiel($idReferentiel);
        $resultsArray = [];

        if ($withoutChorus) {
            foreach ($commonValeurReferentielList as $item) {
                if ($item->getId() != $idValeurRefCatStatsInterfaceChorus) {
                    $resultsArray[] = $item;
                }
            }

            return $resultsArray;
        }

        return $commonValeurReferentielList;
    }
}
