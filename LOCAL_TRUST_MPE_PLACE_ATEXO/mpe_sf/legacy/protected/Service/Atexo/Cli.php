<?php

namespace Application\Service\Atexo;

use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonTLancementUniqueCli;
use Application\Propel\Mpe\CommonTLancementUniqueCliPeer;
use Application\Service\Atexo\Controller\Cli;
use Application\Service\Atexo\Oversight\Atexo_Oversight_Oversight;

/**
 * Scripts en ligne de commande.
 *
 * @category Atexo
 */
abstract class Atexo_Cli
{
    private static $token;

    abstract public static function run();

    /**
     * Affichage d'une erreur avec ou sans arrêt.
     *
     * @param string $errorMessage
     * @param bool   $displayHelp
     * @param bool   $exit
     */
    protected static function displayError($errorMessage, $displayHelp = false, $exit = true)
    {
        echo 'Error : '.$errorMessage."\n";
        if ($displayHelp) {
            self::helpAction();
        }
        if ($exit) {
            exit(1);
        }
    }

    /**
     * Affichage d'un texte.
     *
     * @param string $message
     */
    protected static function display($message = null)
    {
        if (null != $message) {
            echo '-> '.$message;
        }
        echo "\n";
    }

    protected static function getRootPath()
    {
        return realpath(__DIR__.'/../../../../');
    }

    /**
     * Affiche le synopsys et l'aide.
     */
    protected static function helpAction()
    {
        $crons = [];
        $classes = [];
        $comment = [];
        if ($handle = opendir(realpath(__DIR__).'/../../bin/Cli/')) {
            $commandLen = 0;
            while (false !== ($file = readdir($handle))) {
                if ('php' == substr($file, -3)) {
                    $className = substr($file, 0, -4);
                    $commandLen = max(strlen($className), $commandLen);
                    $class = new \ReflectionClass('Cli_'.$className);

                    if (str_contains($class->getDocComment(), '@subpackage Cron')) {
                        $crons[] = $className;
                    } else {
                        $classes[] = $className;
                    }
                    $comment[$className] = trim(str_replace(' *', '', preg_replace('/^[^a]+ \* ([^@]+)@?.*\*\/$/s', '\1', $class->getDocComment())));
                }
            }
            closedir($handle);
            $commandLen = $commandLen + 1;
            $spaces = '       ';
            for ($i = 0; $i < $commandLen; ++$i ) {
                $spaces .= ' ';
            }

            if (is_file(__DIR__.'/../../application.xml')) {
                echo ' PF: '.Atexo_Config::getParameter('PF_SHORT_NAME')."\n";
                echo '  Path: '.self::getRootPath()."\n";
            }
            echo "  Synopsys : mpe <command> [options]\n\n";
            echo "  Commands list :\n";
            foreach ($classes as $class) {
                $comment[$class] = str_replace("\n ", "\n$spaces", $comment[$class]);
                printf("    %' ".$commandLen."s : %s\n", strtolower(substr($class, 0, 1)).substr($class, 1), $comment[$class]);
            }
            echo "\n";
            echo "  Cron list :\n";
            foreach ($crons as $class) {
                $comment[$class] = str_replace("\n ", "\n$spaces", $comment[$class]);
                printf("    %' ".$commandLen."s : %s\n", strtolower(substr($class, 0, 1)).substr($class, 1), $comment[$class]);
            }
        }

        /*
                $class = new ReflectionClass(__CLASS__);
                $methods = $class->getMethods();
                $commands = $commands2 = array();
                $comments = $comments2 = array();
                $commandLen = 0;
                foreach ($methods as $method) {
                    $methodName = (string) $method->getName();
                    if ($methodName =='cronAction') continue;
                    if (substr($methodName, -6, 6) == 'Action') {
                        $command = substr($methodName, 0, strlen($methodName) - 6);
                        $comment = preg_replace('#^[^a-zA-Z0-9_.()-]*(.*?)[^a-zA-Z0-9_.()-]*$#', '\1', $method->getDocComment());
                        $commandLen = max(strlen($command), $commandLen);
                        $commands[] = $command;
                        $comments[] = $comment;
                    }
                    if (substr($methodName, -4, 4) == 'Cron') {
                        $command = substr($methodName, 0, strlen($methodName) - 4);
                        $command = "cron " . $command;
                        $comment = preg_replace('#^[^a-zA-Z0-9_.()-]*(.*?)[^a-zA-Z0-9_.()-]*$#', '\1', $method->getDocComment());
                        $commandLen = max(strlen($command), $commandLen);
                        $commands2[] = $command;
                        $comments2[] = $comment;
                    }
                }
                $commandLen = $commandLen + 1;
                if (is_file(dirname(__FILE__) . '/../../../application.xml')) {
                    echo "\n  PF: " . Atexo_Config::getParameter("PF_SHORT_NAME");
                }
                echo "\n\n  Synopsys : mpe <command> [options]\n\n  Commands list :\n";
                foreach ($commands as $key => $command) {
                    printf("    %' " . $commandLen . "s : %s\n", $command, $comments[$key]);
                }
                echo "\n  Cron list :\n";
                foreach ($commands2 as $key => $command) {
                    printf("    %' " . $commandLen . "s : %s\n", $command, $comments2[$key]);
                }
        */
        echo "\n";

        exit(1);
    }

    /**
     * Lancement d'un Cli, flag + date dans la table t_lancement_unique_cli, pour garantir un lancement unique.
     */
    public static function startLancementUniqueCli($nomCli)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $c = new Criteria();
        $c->add(CommonTLancementUniqueCliPeer::NOM_CLI, $nomCli, Criteria::EQUAL);
        $cli = CommonTLancementUniqueCliPeer::doSelectOne($c, $connexion);

        if ($cli instanceof CommonTLancementUniqueCli) {
            if ('0' == $cli->getEnCours()) {
                $cli->setEnCours('1');
                $cli->setDateLancement(date('Y-m-d H:i:s'));
                $cli->save($connexion);

                return true;
            }
        } else {
            $cli = new CommonTLancementUniqueCli();
            $cli->setNomCli($nomCli);
            $cli->setEnCours('1');
            $cli->setDateLancement(date('Y-m-d H:i:s'));
            $cli->save($connexion);

            return true;
        }

        return false;
    }

    /**
     * Fin de traitement d'un Cli, flag à 0 + date de fin de traitement.
     */
    public static function stopLancementUniqueCli($nomCli)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $c = new Criteria();
        $c->add(CommonTLancementUniqueCliPeer::NOM_CLI, $nomCli, Criteria::EQUAL);
        $cli = CommonTLancementUniqueCliPeer::doSelectOne($c, $connexion);

        if ($cli instanceof CommonTLancementUniqueCli) {
            $cli->setEnCours('0');
            $cli->setDateFin(date('Y-m-d H:i:s'));
            $cli->save($connexion);
        }
    }

    /**
     * @param array $params
     *
     *
     * @throws PropelException
     */
    public static function oversight($params = []): bool|string
    {
        return Atexo_Oversight_Oversight::oversight($params);
    }
}

/**
 * sfFormatter provides methods to format text to be displayed on a console.
 *
 * @author     Fabien Potencier <fabien.potencier@symfony-project.com>
 *
 * @version    SVN: $Id: Cli.php,v 1.5 2014-06-17 17:25:04 lezziani Exp $
 */
class sfFormatter
{
    protected $size = 65;

    public function __construct($maxLineSize = 65)
    {
        $this->size = $maxLineSize;
    }

    /**
     * Formats a text according to the given parameters.
     *
     * @param string $text       The test to style
     * @param mixed  $parameters An array of parameters
     * @param stream $stream     A stream (default to STDOUT)
     *
     * @return string The formatted text
     */
    public function format($text = '', $parameters = [], $stream = STDOUT)
    {
        return $text;
    }

    /**
     * Formats a message within a section.
     *
     * @param string $section The section name
     * @param string $text    The text message
     * @param int    $size    The maximum size allowed for a line (65 by default)
     */
    public function formatSection($section, $text, $size = null)
    {
        return sprintf('>> %-$9s %s', $section, $this->excerpt($text, $size));
    }

    /**
     * Truncates a line.
     *
     * @param string $text The text
     * @param int    $size The maximum size of the returned string (65 by default)
     *
     * @return string The truncated string
     */
    public function excerpt($text, $size = null)
    {
        if (!$size) {
            $size = $this->size;
        }

        if (strlen($text) < $size) {
            return $text;
        }

        $subsize = floor(($size - 3) / 2);

        return substr($text, 0, $subsize).'...'.substr($text, -$subsize);
    }

    /**
     * Sets the maximum line size.
     *
     * @param int $size The maximum line size for a message
     */
    public function setMaxLineSize($size)
    {
        $this->size = $size;
    }
}

/**
 * sfAnsiColorFormatter provides methods to colorize text to be displayed on a console.
 *
 * @author     Fabien Potencier <fabien.potencier@symfony-project.com>
 *
 * @version    SVN: $Id: Cli.php,v 1.5 2014-06-17 17:25:04 lezziani Exp $
 */
class sfAnsiColorFormatter extends sfFormatter
{
    protected array $styles = [
      'ERROR' => ['bg' => 'red', 'fg' => 'white', 'bold' => true],
      'INFO' => ['fg' => 'green', 'bold' => true],
      'COMMENT' => ['fg' => 'yellow'],
      'QUESTION' => ['bg' => 'cyan', 'fg' => 'black', 'bold' => false],
    ];
    protected array $options = ['bold' => 1, 'underscore' => 4, 'blink' => 5, 'reverse' => 7, 'conceal' => 8];
    protected array $foreground = ['black' => 30, 'red' => 31, 'green' => 32, 'yellow' => 33, 'blue' => 34, 'magenta' => 35, 'cyan' => 36, 'white' => 37];
    protected array $background = ['black' => 40, 'red' => 41, 'green' => 42, 'yellow' => 43, 'blue' => 44, 'magenta' => 45, 'cyan' => 46, 'white' => 47];

    /**
     * Sets a new style.
     *
     * @param string $name    The style name
     * @param array  $options An array of options
     */
    public function setStyle($name, $options = [])
    {
        $this->styles[$name] = $options;
    }

    /**
     * Formats a text according to the given style or parameters.
     *
     * @param string   $text       The test to style
     * @param mixed    $parameters An array of options or a style name
     * @param resource $stream     The stream to format for
     *
     * @return string The styled text
     */
    public function format($text = '', $parameters = [], $stream = STDOUT)
    {
        if (!$this->supportsColors($stream)) {
            return $text;
        }

        if (!is_array($parameters) && 'NONE' == $parameters) {
            return $text;
        }

        if (!is_array($parameters) && isset($this->styles[$parameters])) {
            $parameters = $this->styles[$parameters];
        }

        $codes = [];
        if (isset($parameters['fg'])) {
            $codes[] = $this->foreground[$parameters['fg']];
        }
        if (isset($parameters['bg'])) {
            $codes[] = $this->background[$parameters['bg']];
        }
        foreach ($this->options as $option => $value) {
            if (isset($parameters[$option]) && $parameters[$option]) {
                $codes[] = $value;
            }
        }

        return "\033[".implode(';', $codes).'m'.$text."\033[0m";
    }

    /**
     * Formats a message within a section.
     *
     * @param string $section The section name
     * @param string $text    The text message
     * @param int    $size    The maximum size allowed for a line (65 by default)
     * @param string $style   The color scheme to apply to the section string (INFO, ERROR, or COMMAND)
     */
    public function formatSection($section, $text, $size = null, $style = 'INFO')
    {
        $width = null;
        $style = !array_key_exists($style, $this->styles) ? 'INFO' : $style;

//    $width = 9 + strlen($this->format('', $style));

        return sprintf(">> %-${width}s %s", $this->format($section, $style), $this->excerpt($text, $size));
    }

    /**
     * Truncates a line.
     *
     * @param string $text The text
     * @param int    $size The maximum size of the returned string (65 by default)
     *
     * @return string The truncated string
     */
    public function excerpt($text, $size = null)
    {
        if (!$size) {
            $size = $this->size;
        }

        if (strlen($text) < $size) {
            return $text;
        }

        $subsize = floor(($size - 3) / 2);

        return substr($text, 0, $subsize).$this->format('...', 'INFO').substr($text, -$subsize);
    }

    /**
     * Returns true if the stream supports colorization.
     *
     * Colorization is disabled if not supported by the stream:
     *
     *  -  windows
     *  -  non tty consoles
     *
     * @param mixed $stream A stream
     *
     * @return bool true if the stream supports colorization, false otherwise
     */
    public function supportsColors($stream)
    {
        return DIRECTORY_SEPARATOR != '\\' && function_exists('posix_isatty') && @posix_isatty($stream);
    }
}
