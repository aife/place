<?php

namespace Application\Service\Atexo;

use Application\Propel\Mpe\CommonValeurReferentiel;
use Application\Propel\Mpe\CommonValeurReferentielOrg;
use Application\Propel\Mpe\CommonValeurReferentielOrgQuery;
use Application\Propel\Mpe\CommonValeurReferentielQuery;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_ValeursReferentielles
{
    public function retrieveValeursReferentiellesByReference($referentiel, $organisme, $valeurFr = null, $defautlElement = null)
    {
        $valeurReferentielOrgQuery = new CommonValeurReferentielOrgQuery();
        $commonValeurReferentielOrgList = $valeurReferentielOrgQuery->findValeurReferentielOrg();

        $valeursRef = [];

        foreach ($commonValeurReferentielOrgList as $item) {
            if (($item->getIdReferentiel() == $referentiel) && ($item->getOrganisme() == $organisme)) {
                $valeursRef[] = $item;
            }
        }

        $arrayReturn = [];
        if ($defautlElement) {
            $arrayReturn[0] = $defautlElement;
        }
        foreach ($valeursRef as $uneValeurRef) {
            if ($uneValeurRef instanceof CommonValeurReferentielOrg) {
                if ($valeurFr && $uneValeurRef->getLibelleValeurReferentielFr()) {
                    $arrayReturn[$uneValeurRef->getId()] = $uneValeurRef->getLibelleValeurReferentielFr();
                } else {
                    $arrayReturn[$uneValeurRef->getId()] = $uneValeurRef->getLibelleValeurReferentiel();
                }
            }
        }

        return $arrayReturn;
    }

    public function getValeurRefByIdRef($referentiel, $valeursReferentiels = null)
    {
        $valeurReferentielQuery = new CommonValeurReferentielQuery();
        $commonValeurReferentielList = $valeurReferentielQuery->findCachedValeurReferentiel();

        $valeursReferentielsObject = [];

        foreach ($commonValeurReferentielList as $item) {
            if ($item->getIdReferentiel() == $referentiel) {
                $valeursReferentielsObject[] = $item;
            }
        }

        if (!$valeursReferentiels) {
            $valeursReferentiels = [];
        }
        foreach ($valeursReferentielsObject as $valRef) {
            $valeursReferentiels[$valRef->getId()] = $valRef->getLibelleValeurReferentielTraduit();
        }

        return $valeursReferentiels;
    }

    /**
     * Permet de recuperer un type de contrat dans les valeurs referentiels de l'organisme.
     *
     * @param string $referentiel
     * @param string $organisme
     *
     * @deprecated not used
     */
    public function getTypeContratByReferentiel($referentiel, $organisme)
    {
        $arrayTypeContrat = (new Atexo_Referentiel())->retrieveArrayValEurReferentielByReferentiel($referentiel, $this->organisme);
        $listeContrats = [];
        if (is_array($arrayTypeContrat) && count($arrayTypeContrat)) {
            $listeContrats = $arrayTypeContrat;
        }

        return $listeContrats;
    }

    /**
     * Permet de recuperer un objet valeur referentiel.
     *
     * @param string libelle 2
     * @param string id referentiel
     */
    public function retrieveByPk($libelle2, $referentiel, $JustId = false)
    {
        $valRefObjet = (new Atexo_CommonReferentiel())->retrieveValeurReferentielByLibelle2($referentiel, $libelle2);
        if ($valRefObjet instanceof CommonValeurReferentiel) {
            if ($JustId) {
                return $valRefObjet->getId();
            }

            return $valRefObjet;
        }

        return false;
    }

    public function retrieveByIdValAndIdRef($idVal, $referentiel, $JustLibelle = false)
    {
        $valRefObjet = (new Atexo_CommonReferentiel())->retrieveValeurReferentielById($referentiel, $idVal);

        if ($JustLibelle && $valRefObjet) {
            return $valRefObjet->getLibelleValeurReferentielTraduit();
        }

        return $valRefObjet;
    }

    /**
     * Permet de recuperer un objet valeur referentiel organisme
     * ou juste son libelle.
     *
     * @param id valeur referentiel
     * @param  id referentiel
     * @param  organisme
     * @param  just libelle Boolean
     */
    public function retrieveByIdValAndIdRefAndOrg($idVal, $referentiel, $organisme, $JustLibelle = false)
    {
        $valeurReferentielOrgQuery = new CommonValeurReferentielOrgQuery();
        $commonValeurReferentielOrgList = $valeurReferentielOrgQuery->findValeurReferentielOrg();

        $valRefObjet = null;

        foreach ($commonValeurReferentielOrgList as $item) {
            if (($item->getIdReferentiel() == $referentiel) && ($item->getOrganisme() == $organisme) && ($item->getId() == $idVal)) {
                $valRefObjet = $item;
            }
        }
        if ($JustLibelle && $valRefObjet) {
            return $valRefObjet->getLibelleValeurReferentiel();
        }

        return $valRefObjet;
    }
}
