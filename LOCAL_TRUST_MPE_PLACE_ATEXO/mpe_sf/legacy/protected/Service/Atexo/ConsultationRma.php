<?php

namespace Application\Service\Atexo;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationPeer;
use Application\Propel\Mpe\CommonOrganismePeer;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Exception;
use Application\Service\Atexo\Recherche\Atexo_Recherche_Search;
use Exception;
use PDO;

/**
 * Classe de manipulation des consultations.
 *
 * @author Guillaume Poncon <guillaume.poncon@openstates.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_ConsultationRma extends Atexo_Consultation
{
    /**
     * Effectue une recherche parametree en fonction du critere propose.
     *
     * @param Atexo_Consultation_CriteriaVo $criteria, $returnJustNumberOfElement : a true pour avoir uniquement le nombre d'elements
     *
     * @return array of consultation object
     */
    public function search(
        Atexo_Consultation_CriteriaVo $criteriaVo,
        $returnJustNumberOfElement = false,
        $returnReferences = false,
        $forAlertesEntreprises = false
    ) {
        $keys = [];
        $queryParams = [];
        $condition = true;
        // Test des parametres obligatoires
        if ('' === $criteriaVo->getAcronymeOrganisme()
                || '' === $criteriaVo->getIdService()) {
            throw new Atexo_Consultation_Exception('Les parametres AcronymeOrganisme et IdService sont obligatoires');
        }

        //requete sql
        if ($returnJustNumberOfElement || $returnReferences) {
            $querySelect = ' Select   DISTINCT consultation.id  ';
        } else {
            $querySelect = ' Select  DISTINCT consultation.id, consultation.datefin, reference_utilisateur, nom_createur,'
                         .' prenom_createur, intitule, objet, id_type_avis, alloti, consultation.organisme, consultation.service_id, consultation.service_associe_id ';
        }

        $queryFrom = ' FROM consultation LEFT JOIN CategorieLot ON (CategorieLot.consultation_id = consultation.id ';
        $queryFrom .= ' AND CategorieLot.organisme = consultation.organisme ';
        $queryFrom .= ')';

        if (0 != $criteriaVo->getIdAlerteConsultation() && 0 != $criteriaVo->getAlerteConsultationCloturee()) {
            $queryFrom .= ' LEFT JOIN alerte_metier ON (alerte_metier.reference_cons = consultation.id ';
            $queryFrom .= ' AND alerte_metier.organisme = consultation.organisme ';
            $queryFrom .= ')';
        }

        //on parcourt l'objet CriteriaVo passe en parametre pour ajouter a la clause where
        $otherConditions = '';
        $otherConditions .= Atexo_Recherche_Search::otherConditions($criteriaVo, $queryParams);

        $stateCondition = Atexo_Recherche_Search::stateConditions($criteriaVo, $queryParams);
        $otherConditions .= $stateCondition;

        $finalQuery = $querySelect.' , 0 as invitedWithHisHabilitation, 1 as invitedReadOnly, 0 as invitedValidationOnly '
                      .$queryFrom.' WHERE consultation.consultation_achat_publique=:achatPublique '.$otherConditions;
        if (!$criteriaVo->getAcronymeOrganisme()) {
            $finalQuery = ' AND consultation.organisme =:organismeAgent ';
            $queryParams[':organismeAgent'] = Atexo_CurrentUser::getOrganismAcronym();
        }

        $queryParams[':achatPublique'] = '0';

        if (!$finalQuery) {
            if ($returnJustNumberOfElement) {
                return 0;
            } else {
                return [];
            }
        }
        if (!$returnJustNumberOfElement && !$returnReferences) {
            if ($criteriaVo->getSortByElement()) {
                $finalQuery .= ' order by '.$criteriaVo->getSortByElement().' '.$criteriaVo->getSensOrderBy().' ';
                if ('datefin' != strtolower($criteriaVo->getSortByElement())) {
                    $finalQuery .= ' , datefin DESC ';
                }
            } else {
                $finalQuery .= ' order by datefin DESC ';
            }

            /*if ($criteriaVo->getLimit()) {
            $finalQuery .= " limit " . $criteriaVo->getOffset() . "," . $criteriaVo->getLimit();
            }*/
        }

        $finalQuery = preg_replace("#\([ ]+OR[ ]+\(#", '( (', $finalQuery);

        $statement = Atexo_Db::getLinkCommon(true)->prepare($finalQuery);

        /*try {
            foreach($queryParams as $a => $b) {
                if($finalQuery) {
                    $finalQuery = str_replace($a."", "'".$b."' ", $finalQuery);
                }

            }
            echo $finalQuery;exit;
        } catch(Exception $e ) {

        }*/

        $statement->execute($queryParams);

        if ($returnJustNumberOfElement || $returnReferences) {
            $nombreElement = 0;
            $arrayReferences = [];
            if ($returnJustNumberOfElement && $criteriaVo->getcalledFromPortail()) {
                $rows = $statement->fetchAll(PDO::FETCH_ASSOC);
                $nombreElement = is_countable($rows) ? count($rows) : 0;
                if ($criteriaVo->getTypeAcces() == Atexo_Config::getParameter('TYPE_PROCEDURE_RESTREINTE')) {
                    foreach ($rows as $row) {
                        if ($row['code_procedure'] != $criteriaVo->getCodeAccesProcedureRestreinte()) {
                            --$nombreElement;
                        }
                    }
                }

                return $nombreElement;
            } else {
                while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                    if (!in_array($row['id'], $arrayReferences)) {
                        ++$nombreElement;
                        $arrayReferences[] = $row['id'];
                    }
                }
                //print_r($arrayReferences);exit;
                if ($returnReferences) {
                    return $arrayReferences;
                }
                //echo $nombreElement;
                return $nombreElement;
            }
        } else {
            //ajouter la clause order by pour le tri : elle peut etre passe en parametre dans le criteriaVo
            $arrayConsultation = [];
            $atLeastOneConsultationFound = false;
            $readOnlyGuest = [];
            $validationOnlyGuest = [];
            $c = new Criteria();

            //on parcours le PDO record set pour recuperer l'objet Consultation en utilisant propel
            while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                $atLeastOneConsultationFound = true;

                //$arrayReferences[]=$row['id'];
                $c->addOr(CommonConsultationPeer::ID, $row['id']);
                //Si l'agent connecte est inivite par defaut ou avec ces habilitations, $readOnlyGuest prend la valeur false
                //Sinon s'il est invite en lecture seul, $readOnlyGuest prend la valeur true.
                //(la cle du tableau est la id)
                //print_r($row);
                if ('0' === $row['invitedValidationOnly']) {
                    if ('1' === $row['invitedWithHisHabilitation']) {
                        $readOnlyGuest[$row['id']] = false;
                    } else {
                        $readOnlyGuest[$row['id']] = true;
                    }
                } else {
                    $validationOnlyGuest[$row['id']] = true;
                    if (!isset($readOnlyGuest[$row['id']])) {
                        $readOnlyGuest[$row['id']] = true;
                    }
                }
            }
            //print_r($validationOnlyGuest);exit;
            $arrayConsultation = [];
            $organisme = '';
            if ($atLeastOneConsultationFound) {
                if ($criteriaVo->getcalledFromPortail()) {
                    $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
                    $c->addJoin(CommonConsultationPeer::ORGANISME, CommonOrganismePeer::ACRONYME, Criteria::LEFT_JOIN);
                    if ($criteriaVo->getSortByElement()) {
                        if ('ASC' == $criteriaVo->getSensOrderBy()) {
                            $c->addAscendingOrderByColumn('consultation.'.$criteriaVo->getSortByElement());
                        } else {
                            $c->addDescendingOrderByColumn('consultation.'.$criteriaVo->getSortByElement());
                        }
                        $c->addAscendingOrderByColumn('Organisme.ordre');
                        if ('datefin' != strtolower($criteriaVo->getSortByElement())) {
                            $c->addDescendingOrderByColumn(CommonConsultationPeer::DATEFIN);
                        }
                    } else {
                        $c->addAscendingOrderByColumn('Organisme.ordre');
                        $c->addDescendingOrderByColumn(CommonConsultationPeer::DATEFIN);
                    }
                    $arrayConsultation = CommonConsultationPeer::doSelect($c, $connexion);
                } else {
                    $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
                    //$c->add(ConsultationPeer::id,$arrayReferences,Criteria::IN);
                    if ($criteriaVo->getSortByElement()) {
                        if ('ASC' == $criteriaVo->getSensOrderBy()) {
                            $c->addAscendingOrderByColumn('consultation.'.$criteriaVo->getSortByElement());
                        } else {
                            $c->addDescendingOrderByColumn('consultation.'.$criteriaVo->getSortByElement());
                        }
                        if ('datefin' != strtolower($criteriaVo->getSortByElement())) {
                            $c->addDescendingOrderByColumn(CommonConsultationPeer::DATEFIN);
                        }
                    } else {
                        $c->addDescendingOrderByColumn(CommonConsultationPeer::DATEFIN);
                    }
                    $c->add(CommonConsultationPeer::ORGANISME, $criteriaVo->getAcronymeOrganisme());
                    if ($criteriaVo->getLimit()) {
                        $c->setLimit($criteriaVo->getLimit());
                        $c->setOffset($criteriaVo->getOffset());
                    }

                    $arrayConsultation = CommonConsultationPeer::doSelect($c, $connexion);
                    $organisme = $criteriaVo->getAcronymeOrganisme();
                }
            }
            $arrayReturn = [];
            foreach ($arrayConsultation as $consultation) {
                if ($consultation instanceof CommonConsultation && !$criteriaVo->getcalledFromPortail()) {
                    $consultation->setCurrentUserReadOnly($readOnlyGuest[$consultation->getId()]);
                    $consultation->setCurrentUserValidationOnly($validationOnlyGuest[$consultation->getId()]);
                    $idEtatConsultation = self::getStatus($consultation, $criteriaVo->getcalledFromPortail());
                    $consultation->setStatusConsultation($idEtatConsultation);
                    $consultation->setPublicationHabilitation(Atexo_CurrentUser::hasHabilitation('PublierConsultation'));
                    $consultation->setRedactionDocumentsRedac(Atexo_CurrentUser::hasHabilitation('RedactionDocumentsRedac'));
                    if (((($consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_CONSULTATION')
                        || $consultation->getDepouillablePhaseConsultation() == Atexo_Config::getParameter('CONSULTATION_DEPOUIABLE_PHASE_1')) && $consultation->getDatefin() > date('Y-m-d H:i:s'))
                            || (($consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_PREPARATION')
                            || $consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_ELABORATION'))
                                && ($consultation->getDatefin() > date('Y-m-d H:i:s') || strstr($consultation->getDatefin(), '0000-00-00 00:00:00'))))) {
                        if ((
                                    Atexo_CurrentUser::hasHabilitation('ModifierConsultationAvantValidation')
                                    &&
                                    (
                                        $consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_PREPARATION')
                                        || $consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_ELABORATION')
                                    )
                                )
                                ||
                                (
                                    $consultation->hasHabilitationModifierApresValidation()
                                    &&
                                    (
                                        $consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_CONSULTATION')
                                        ||
                                        $consultation->getDepouillablePhaseConsultation() == Atexo_Config::getParameter('CONSULTATION_DEPOUIABLE_PHASE_1')
                                    )
                                )
                           ) {
                            $consultation->setModifHabilitation(true);
                        } else {
                            $consultation->setModifHabilitation(false);
                        }
                    }

                    if ($consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_PREPARATION')
                            || $consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_ELABORATION')) {
                        $chiffrement = $consultation->getChiffrementOffre();

                        if (1 == $chiffrement) {
                            $consultation->setValidateWithChiffrement(true);
                        } else {
                            $consultation->setValidateWithoutChiffrement(true);
                        }
                    }

                    // Attribution possible si status decision, ou (status != de preparation et consultation depouillable phase consultation)
                    if ($consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_DECISION')
                        || ($consultation->getStatusConsultation() !== Atexo_Config::getParameter('STATUS_ELABORATION')
                            && $consultation->getStatusConsultation() !== Atexo_Config::getParameter('STATUS_PREPARATION')
                            && $consultation->getDepouillablePhaseConsultation() == Atexo_Config::getParameter('CONSULTATION_DEPOUIABLE_PHASE_1'))
                        || ($consultation->getDecisionPartielle() == Atexo_Config::getParameter('DECISION_PARTIELLE_OUI')
                                && $consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE'))
                        ) {
                        $consultation->setAttributionHabilitation(Atexo_CurrentUser::hasHabilitation('AttributionMarche'));
                        $consultation->setDecisionSuiviSeulHabilitation(Atexo_CurrentUser::hasHabilitation('DecisionSuiviSeul'));
                    }

                    $consultation->setDepotHabilitation(Atexo_CurrentUser::hasHabilitation('AccesRegistreDepotsPapier'));
                    $consultation->setRetraiteHabilitation(Atexo_CurrentUser::hasHabilitation('AccesRegistreRetraitsPapier'));
                    $consultation->setQuestionHabilitation(Atexo_CurrentUser::hasHabilitation('AccesRegistreQuestionsPapier'));

                    // Depouillement possible si status decision, ou (status != de preparation et consultation depouillable phase consultation)
                    if ($consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE')
                        || ($consultation->getStatusConsultation() !== Atexo_Config::getParameter('STATUS_PREPARATION')
                                && $consultation->getStatusConsultation() !== Atexo_Config::getParameter('STATUS_ELABORATION')
                                && $consultation->getDepouillablePhaseConsultation() == Atexo_Config::getParameter('CONSULTATION_DEPOUIABLE_PHASE_1'))
                        ) {
                        $consultation->setDepouillerHabilitation(Atexo_CurrentUser::hasHabilitation('AccesReponses'));
                        $consultation->setResultatAnalyseHabilitation(Atexo_CurrentUser::hasHabilitation('ResultatAnalyse'));
                    }

                    if ($consultation->getDecisionPartielle() == Atexo_Config::getParameter('DECISION_PARTIELLE_OUI')
                        && $consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE')) {
                        $consultation->setStatusConsultation(Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE').Atexo_Config::getParameter('STATUS_DECISION'));
                    }

                    if ($consultation->getStatusConsultation() !== Atexo_Config::getParameter('STATUS_PREPARATION')
                        && $consultation->getStatusConsultation() !== Atexo_Config::getParameter('STATUS_ELABORATION')
                        && $consultation->getDepouillablePhaseConsultation() == Atexo_Config::getParameter('CONSULTATION_DEPOUIABLE_PHASE_1')) {
                        if ($consultation->getDatefin() > date('Y-m-d H:i:s')) {
                            $consultation->setStatusConsultation(Atexo_Config::getParameter('STATUS_CONSULTATION').
                              Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE').
                            Atexo_Config::getParameter('STATUS_DECISION'));
                        } else {
                            $consultation->setStatusConsultation(Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE').
                                                                 Atexo_Config::getParameter('STATUS_DECISION'));
                        }
                    }
                } elseif ($consultation instanceof CommonConsultation) {
                    if ($criteriaVo->getTypeAcces() == Atexo_Config::getParameter('TYPE_PROCEDURE_RESTREINTE') && $consultation->getCodeProcedure() != $criteriaVo->getCodeAccesProcedureRestreinte()) {
                        $condition = false;
                    }
                }
                if ($condition) {
                    $key = $consultation->getId().$consultation->getOrganisme();
                    if (!in_array($key, $keys)) {
                        $keys[] = $key;
                        $arrayReturn[] = $consultation;
                    }
                }
            }

            return $arrayReturn;
        }
    }
}
