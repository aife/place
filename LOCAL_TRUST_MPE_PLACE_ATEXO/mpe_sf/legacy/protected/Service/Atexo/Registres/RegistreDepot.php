<?php

namespace Application\Service\Atexo\Registres;

use Application\Service\Atexo\Rest\Action\Atexo_Rest_Action_EnveloppeField;

class Atexo_Registres_RegistreDepot
{
    protected $id;
    protected ?int $idEntreprise = null;
    protected ?int $idEtablissement = null;
    protected $idInscrit;
    protected $loginInscrit;
    protected $numero;
    protected $date;
    protected $type;
    protected $nomInscrit;
    protected $prenomInscrit;
    protected $rueInscrit;
    protected $codePostalInscrit;
    protected $villeInscrit;
    protected $paysInscrit;
    protected ?string $raisonSocialeInscrit = null;
    protected $raisonSocialeEntreprise;
    protected $emailInscrit;
    protected $telephoneInscrit;
    protected $faxInscrit;
    protected $sirenInscrit;
    protected $siretInscrit;
    protected $codeEtablissementInscrit;
    protected $typeIdentifiantEntrepriseInscrit;
    protected $actifInscrit;
    protected $annuaireDefenseInscrit;
    protected $dateModificationRgpdInscrit;
    protected $rgpdInscrit;
    protected $sirenEntreprise;
    protected $emailEntreprise;
    protected $formeJuridiqueEntreprise;
    protected $codeAPEEntreprise;
    protected $rueEntreprise;
    protected $codePostalEntreprise;
    protected $villeEntreprise;
    protected $paysEntreprise;
    protected $telephoneEntreprise;
    protected $faxEntreprise;
    protected ?array $enveloppes = null;
    protected $nombrePlis;
    protected $marchePublicSimplifie;
    protected $organisme;
    protected $reponseAnnonceBase64;
    protected $statutOffres;

    /*
     * Set the value of [id] column.
     *
     * @param int $value new value
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the [$id] column value.
     *
     * @return int
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * recupere la valeur de l'attribut "_idEntreprise".
     *
     * @return int la valeur de l'attribut "_idEntreprise"
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function getIdEntreprise()
    {
        return $this->idEntreprise;
    }

    /**
     * affecte la valeur en param a l'attribut "_idEntreprise".
     *
     * @param int $_idEntreprise
     *
     * @author  LEZ : <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function setIdEntreprise($_idEntreprise)
    {
        $this->idEntreprise = $_idEntreprise;
    }

    /*
     * Set the value of [numero] column.
     *
     * @param int $value new value
     */
    public function getNumero()
    {
        return $this->numero;
    }

    /**
     * Get the [numero] column value.
     *
     * @return int
     */
    public function setNumero($value)
    {
        $this->numero = $value;
    }

    /*
     * Set the value of [date] column.
     *
     * @param int $value new value
     */
    public function setDate($value)
    {
        $this->date = $value;
    }

    /**
     * Get the [date] column value.
     *
     * @return int
     */
    public function getDate()
    {
        return $this->date;
    }

    /*
     * Set the value of [numero] column.
     *
     * @param int $value new value
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Get the [numero] column value.
     *
     * @return int
     */
    public function setType($value)
    {
        $this->type = $value;
    }

    /*
     * Set the value of [nomInscrit] column.
     *
     * @param int $value new value
     */
    public function setNomInscrit($nomInscrit)
    {
        $this->nomInscrit = $nomInscrit;
    }

    /**
     * Get the [nomInscrit] column value.
     *
     * @return int
     */
    public function getNomInscrit()
    {
        return $this->nomInscrit;
    }

    /*
     * Set the value of [prenomInscrit] column.
     *
     * @param int $value new value
     */
    public function setPrenomInscrit($prenomInscrit)
    {
        $this->prenomInscrit = $prenomInscrit;
    }

    /**
     * Get the [prenomInscrit] column value.
     *
     * @return int
     */
    public function getPrenomInscrit()
    {
        return $this->prenomInscrit;
    }

    /*
     * Set the value of [rueInscrit] column.
     *
     * @param int $value new value
     */
    public function setRueInscrit($rueInscrit)
    {
        $this->rueInscrit = $rueInscrit;
    }

    /**
     * Get the [rueInscrit] column value.
     *
     * @return int
     */
    public function getRueInscrit()
    {
        return $this->rueInscrit;
    }

    /*
     * Set the value of [codePostalInscrit] column.
     *
     * @param int $value new value
     */
    public function setCodePostalInscrit($codePostalInscrit)
    {
        $this->codePostalInscrit = $codePostalInscrit;
    }

    /**
     * Get the [codePostalInscrit] column value.
     *
     * @return int
     */
    public function getCodePostalInscrit()
    {
        return $this->codePostalInscrit;
    }

    /*
     * Set the value of [villeInscrit] column.
     *
     * @param int $value new value
     */
    public function setVilleInscrit($villeInscrit)
    {
        $this->villeInscrit = $villeInscrit;
    }

    /**
     * Get the [villeInscrit] column value.
     *
     * @return int
     */
    public function getVilleInscrit()
    {
        return $this->villeInscrit;
    }

    /*
     * Set the value of [paysInscrit] column.
     *
     * @param int $value new value
     */
    public function setPaysInscrit($paysInscrit)
    {
        $this->paysInscrit = $paysInscrit;
    }

    /**
     * Get the [paysInscrit] column value.
     *
     * @return int
     */
    public function getPaysInscrit()
    {
        return $this->paysInscrit;
    }

    /**
     * Set the value of [raisonSocialeInscrit] column.
     *
     * @param string $raisonSocialeInscrit
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setRaisonSocialeInscrit($raisonSocialeInscrit)
    {
        $this->raisonSocialeInscrit = $raisonSocialeInscrit;
    }

    /**
     * Set the value of [raisonSocialeInscrit] column.
     *
     * @return string $raisonSocialeInscrit
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getRaisonSocialeInscrit()
    {
        return $this->raisonSocialeInscrit;
    }

    /*
     * Set the value of [raisonSocialeEntreprise] column.
     *
     * @param int $value new value
     */
    public function setRaisonSocialeEntreprise($raisonSocialeEntreprise)
    {
        $this->raisonSocialeEntreprise = $raisonSocialeEntreprise;
    }

    /**
     * Get the [raisonSocialeEntreprise] column value.
     *
     * @return int
     */
    public function getRaisonSocialeEntreprise()
    {
        return $this->raisonSocialeEntreprise;
    }

    /*
     * Set the value of [emailInscrit] column.
     *
     * @param int $value new value
     */
    public function setEmailInscrit($value)
    {
        $this->emailInscrit = $value;
    }

    /**
     * Get the [emailInscrit] column value.
     *
     * @return int
     */
    public function getEmailInscrit()
    {
        return $this->emailInscrit;
    }

    /*
     * Set the value of [telephoneInscrit] column.
     *
     * @param int $value new value
     */
    public function setTelephoneInscrit($value)
    {
        $this->telephoneInscrit = $value;
    }

    /**
     * Get the [telephoneInscrit] column value.
     *
     * @return int
     */
    public function getTelephoneInscrit()
    {
        return $this->telephoneInscrit;
    }

    /*
     * Set the value of [faxInscrit] column.
     *
     * @param int $value new value
     */
    public function setFaxInscrit($value)
    {
        $this->faxInscrit = $value;
    }

    /**
     * Get the [faxInscrit] column value.
     *
     * @return int
     */
    public function getFaxInscrit()
    {
        return $this->faxInscrit;
    }

    public function getSirenEntreprise()
    {
        return $this->sirenEntreprise;
    }

    public function setSirenEntreprise($sirenEntreprise)
    {
        $this->sirenEntreprise = $sirenEntreprise;
    }

    public function getEmailEntreprise()
    {
        return $this->emailEntreprise;
    }

    public function setEmailEntreprise($emailEntreprise)
    {
        $this->emailEntreprise = $emailEntreprise;
    }

    public function getFormeJuridiqueEntreprise()
    {
        return $this->formeJuridiqueEntreprise;
    }

    public function setFormeJuridiqueEntreprise($formeJuridiqueEntreprise)
    {
        $this->formeJuridiqueEntreprise = $formeJuridiqueEntreprise;
    }

    public function getCodeAPEEntreprise()
    {
        return $this->codeAPEEntreprise;
    }

    public function setCodeAPEEntreprise($codeAPEEntreprise)
    {
        $this->codeAPEEntreprise = $codeAPEEntreprise;
    }

    public function getRueEntreprise()
    {
        return $this->rueEntreprise;
    }

    public function setRueEntreprise($rueEntreprise)
    {
        $this->rueEntreprise = $rueEntreprise;
    }

    public function getCodePostalEntreprise()
    {
        return $this->codePostalEntreprise;
    }

    public function setCodePostalEntreprise($codePostalEntreprise)
    {
        $this->codePostalEntreprise = $codePostalEntreprise;
    }

    public function getVilleEntreprise()
    {
        return $this->villeEntreprise;
    }

    public function setVilleEntreprise($villeEntreprise)
    {
        $this->villeEntreprise = $villeEntreprise;
    }

    public function getPaysEntreprise()
    {
        return $this->paysEntreprise;
    }

    public function setPaysEntreprise($paysEntreprise)
    {
        $this->paysEntreprise = $paysEntreprise;
    }

    public function getTelephoneEntreprise()
    {
        return $this->telephoneEntreprise;
    }

    public function setTelephoneEntreprise($telephoneEntreprise)
    {
        $this->telephoneEntreprise = $telephoneEntreprise;
    }

    public function getFaxEntreprise()
    {
        return $this->faxEntreprise;
    }

    public function setFaxEntreprise($faxEntreprise)
    {
        $this->faxEntreprise = $faxEntreprise;
    }

    public function addEnveloppes(Atexo_Rest_Action_EnveloppeField $enveloppe)
    {
        $this->enveloppes[] = $enveloppe;
    }

    public function getEnveloppes()
    {
        return $this->enveloppes;
    }

    public function getNombrePlis()
    {
        return $this->nombrePlis;
    }

    public function setNombrePlis($nombrePlis)
    {
        $this->nombrePlis = $nombrePlis;
    }

    /**
     * @return mixed
     */
    public function getMarchePublicSimplifie()
    {
        return $this->marchePublicSimplifie;
    }

    /**
     * @param mixed $marchePublicSimplifie
     */
    public function setMarchePublicSimplifie($marchePublicSimplifie)
    {
        $this->marchePublicSimplifie = $marchePublicSimplifie;
    }

    /*
    * recupere la valeur de l'attribut "organisme"
    * @return $this->_organisme (varchar) la valeur de l'attribut "Organisme"
    * @author  YEL : <youssef.elalaoui@atexo.com>
    * @copyright Atexo 2016
    * @version : 1.0
    * @since : 2015-place
    */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    /*
     * affecte la valeur en param a l'attribut "organisme"
     * @param  $organisme(string)
     * @author  YEL : <youssef.elalaoui@atexo.com>
     * @copyright Atexo 2016
     * @version : 1.0
     * @since : 2015-place
     */
    public function setOrganisme($organisme)
    {
        $this->organisme = $organisme;
    }

    /**
     * @return mixed
     */
    public function getReponseAnnonceBase64()
    {
        return $this->reponseAnnonceBase64;
    }

    /**
     * @param mixed $reponseAnnonceBase64
     */
    public function setReponseAnnonceBase64($reponseAnnonceBase64)
    {
        $this->reponseAnnonceBase64 = $reponseAnnonceBase64;
    }

    /**
     * @return mixed
     */
    public function getStatutOffres()
    {
        return $this->statutOffres;
    }

    /**
     * @param mixed $statutOffres
     */
    public function setStatutOffres($statutOffres)
    {
        $this->statutOffres = $statutOffres;
    }

    /**
     * @return mixed
     */
    public function getIdInscrit()
    {
        return $this->idInscrit;
    }

    /**
     * @param mixed $idInscrit
     */
    public function setIdInscrit($idInscrit): void
    {
        $this->idInscrit = $idInscrit;
    }

    /**
     * @return mixed
     */
    public function getDateModificationRgpdInscrit()
    {
        return $this->dateModificationRgpdInscrit;
    }

    /**
     * @param mixed $dateModificationRgpdInscrit
     */
    public function setDateModificationRgpdInscrit($dateModificationRgpdInscrit): void
    {
        $this->dateModificationRgpdInscrit = $dateModificationRgpdInscrit;
    }

    /**
     * @return mixed
     */
    public function getRgpdInscrit()
    {
        return $this->rgpdInscrit;
    }

    /**
     * @param mixed $rgpdInscrit
     */
    public function setRgpdInscrit($rgpdInscrit): void
    {
        $this->rgpdInscrit = $rgpdInscrit;
    }

    /**
     * @return mixed
     */
    public function getAnnuaireDefenseInscrit()
    {
        return $this->annuaireDefenseInscrit;
    }

    /**
     * @param mixed $annuaireDefenseInscrit
     */
    public function setAnnuaireDefenseInscrit($annuaireDefenseInscrit): void
    {
        $this->annuaireDefenseInscrit = $annuaireDefenseInscrit;
    }

    /**
     * @return int|null
     */
    public function getIdEtablissement(): ?int
    {
        return $this->idEtablissement;
    }

    /**
     * @param int|null $idEtablissement
     */
    public function setIdEtablissement(?int $idEtablissement): void
    {
        $this->idEtablissement = $idEtablissement;
    }

    /**
     * @return mixed
     */
    public function getLoginInscrit()
    {
        return $this->loginInscrit;
    }

    /**
     * @param mixed $loginInscrit
     */
    public function setLoginInscrit($loginInscrit): void
    {
        $this->loginInscrit = $loginInscrit;
    }

    /**
     * @return mixed
     */
    public function getActifInscrit()
    {
        return $this->actifInscrit;
    }

    /**
     * @param mixed $actifInscrit
     */
    public function setActifInscrit($actifInscrit): void
    {
        $this->actifInscrit = $actifInscrit;
    }

    /**
     * @return mixed
     */
    public function getTypeIdentifiantEntrepriseInscrit()
    {
        return $this->typeIdentifiantEntrepriseInscrit;
    }

    /**
     * @param mixed $typeIdentifiantEntrepriseInscrit
     */
    public function setTypeIdentifiantEntrepriseInscrit($typeIdentifiantEntrepriseInscrit): void
    {
        $this->typeIdentifiantEntrepriseInscrit = $typeIdentifiantEntrepriseInscrit;
    }

    /**
     * @return mixed
     */
    public function getSirenInscrit()
    {
        return $this->sirenInscrit;
    }

    /**
     * @param mixed $sirenInscrit
     */
    public function setSirenInscrit($sirenInscrit): void
    {
        $this->sirenInscrit = $sirenInscrit;
    }

    /**
     * @return mixed
     */
    public function getCodeEtablissementInscrit()
    {
        return $this->codeEtablissementInscrit;
    }

    /**
     * @param mixed $codeEtablissementInscrit
     */
    public function setCodeEtablissementInscrit($codeEtablissementInscrit): void
    {
        $this->codeEtablissementInscrit = $codeEtablissementInscrit;
    }

    /**
     * @return mixed
     */
    public function getSiretInscrit()
    {
        return $this->siretInscrit;
    }

    /**
     * @param mixed $siretInscrit
     */
    public function setSiretInscrit($siretInscrit): void
    {
        $this->siretInscrit = $siretInscrit;
    }
}
