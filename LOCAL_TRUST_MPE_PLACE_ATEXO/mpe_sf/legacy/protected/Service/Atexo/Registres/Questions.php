<?php

namespace Application\Service\Atexo\Registres;

use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonQuestionsDce;
use Application\Propel\Mpe\CommonQuestionsDcePeer;
use Application\Propel\Mpe\CommonQuestionsDceQuery;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Document;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_MultiDomaine;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Config\Atexo_Config_Exception;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Rest\Action\Atexo_Rest_Action_Actions;
use Prado\Prado;

/**
 * Classe contenant le search des questions a partir d'un Vo (idem que pour les consultations).
 *
 * @author Adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2009
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Registres_Questions
{
    /**
     * Recherche les question selon des criteres preciser dans le Vo.
     *
     * @param : critere de recherche. Criteres obligatoires : setAcronymeOrganisme() setIdService(),
     * pour autres criteres voir Atexo_Registres_CriteriaVo et Atexo_Consultation_CriteriaVo
     * @param : $returnNombreQuestions               : pour retourner le nombre de resultat
     * @param : $returnNombreConsultationsConcernees : retourne le nombre de consultations pour lesquelles une question a ete pose (selon les criteres)
     */
    public function getCorpsMessageQuestionRecuePourAgent($reference, $organisme, $question)
    {
        $corpsMail = Prado::localize('CORPS_DEBUT').','.'

';
        $corpsMail .= Prado::localize('QUESTION_DEPOSE_POUR_CONSULTATION').' :'.'

';
        $corpsMail .= (new Atexo_Message())->getInfoConsultation($reference, $organisme, false).'
';
        $corpsMail .= Prado::localize('TEXT_LIEN_ACCES_DIRECT_CONSULTATION');
        $corpsMail .= '
';
        $pfUrl = Atexo_MultiDomaine::replaceDomain(
            Atexo_Config::getParameter('PF_URL_MESSAGERIE'),
            Atexo_Config::getParameter('PF_ACTIVE_MULTI_DOMAINE')
        );
        $corpsMail .= $pfUrl.'?page=Agent.TableauDeBord&AS=0&id='.$reference;
        $corpsMail .= '

';
        $corpsMail .= (new Atexo_Message())->getBodyForQuestionEntreprise($question);
        $corpsMail .= '

';
        $corpsMail .= Prado::localize('CORPS_FIN').','.'

';
        $corpsMail .= Prado::localize('PLACE_MARCHE_PUBLIC_INTERMINISTERE');

        return $corpsMail;
    }

    public function getCorpsMessageQuestionAccuseRecepPourAgent($reference, $organisme, $question)
    {
        $corpsMail = Prado::localize('CORPS_DEBUT').','.'

';
        $corpsMail .= Prado::localize('AR_QUESTION_DEPOSE_POUR_CONSULTATION').' :'.'

';
        $corpsMail .= (new Atexo_Message())->getInfoConsultation($reference, $organisme, false).'
';

        $corpsMail .= (new Atexo_Message())->getBodyForQuestionEntreprise($question);
        $corpsMail .= '

';
        $corpsMail .= Prado::localize('CORPS_FIN').','.'getId

';
        $corpsMail .= Prado::localize('PLACE_MARCHE_PUBLIC_INTERMINISTERE');

        return $corpsMail;
    }

    public function getAllQuestionsByConsultation($consultationId, $org, $typeDepot = false, $connexion = null, $justIds = false)
    {
        $arrayIds = [];
        if (null == $connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        }
        $c = new Criteria();
        $c->add(CommonQuestionsDcePeer::CONSULTATION_ID, $consultationId);
        if ($org) {
            $c->add(CommonQuestionsDcePeer::ORGANISME, $org);
        }
        if ($typeDepot) {
            $c->add(CommonQuestionsDcePeer::TYPE_DEPOT, $typeDepot);
        }
        $questions = CommonQuestionsDcePeer::doSelect($c, $connexion);
        if ($justIds) {
            foreach ($questions as $oneQuestion) {
                $arrayIds[] = $oneQuestion->getId();
            }

            return $arrayIds;
        }

        return $questions;
    }

    /**
     * @param $consultationId
     * @param $org
     * @param bool $typeDepot
     * @param null $connexion
     *
     * @return int
     *
     * @throws Atexo_Config_Exception
     * @throws PropelException
     */
    public function getNbQuestionsByConsultation($consultationId, $org, $typeDepot = false, $connexion = null)
    {
        if (null == $connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        }
        $c = new Criteria();
        $c->add(CommonQuestionsDcePeer::CONSULTATION_ID, $consultationId);
        if ($org) {
            $c->add(CommonQuestionsDcePeer::ORGANISME, $org);
        }
        if ($typeDepot) {
            $c->add(CommonQuestionsDcePeer::TYPE_DEPOT, $typeDepot);
        }

        return CommonQuestionsDcePeer::doCount($c, $connexion);
    }

    public function getNbReponseByConsultation($consultationId, $org, $connexion = null)
    {
        if (null == $connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        }
        $c = new Criteria();
        $c->add(CommonQuestionsDcePeer::CONSULTATION_ID, $consultationId);
        if ($org) {
            $c->add(CommonQuestionsDcePeer::ORGANISME, $org);
        }
        $c->add(CommonQuestionsDcePeer::DATE_REPONSE, '0000-00-00');
        $nbQuestions = CommonQuestionsDcePeer::doCount($c, $connexion);

        return $nbQuestions;
    }

    /*
     * retourne un objet CommonQuestionsDce a partir de son id
     *
     * @param $id(int) : id du QuestionsDce
     * @return $questionObject(CommonQuestionsDce) : objet de CommonQuestionsDce
     * @author : ASO : <ayoub.souidahmed@atexo.com>
     * @version : depuis 4.6.0
     */
    public function getQuestionById($id)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonQuestionsDcePeer::ID, $id);
        $questionObject = CommonQuestionsDcePeer::doSelectOne($c, $connexion);

        return $questionObject;
    }

    /**
     * retourne un objet CommonQuestionsDce a partir de son id fichier.
     *
     * @param $id(int) : id fichier du QuestionsDce
     *
     * @return $questionObject(CommonQuestionsDce) : objet de CommonQuestionsDce
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function getQuestionByIdFichier($id, $organisme = null)
    {
        $questioQuery = new CommonQuestionsDceQuery();

        return $questioQuery->getQuestionByIdFichier($id, $organisme);
    }

    /**
     * Permet d'ajouter les informations d'une question pour le ws.
     *
     * @param $id
     * @param $xmlResponse
     * @param $arrayBlob
     * @param $logger
     * @param $contentType
     * @param string $organisme Acronyme de l'organisme connecte au WS
     *
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function setInfoQuestionForWs($id, &$xmlResponse, &$arrayBlob, &$logger, &$contentType, $organisme = null, $typeChecksum = 'md5'): bool|string
    {
        $logger->info("La récupération de la question dont l'id fichier = ".$id);
        $question = self::getQuestionByIdFichier($id, $organisme);
        if ($question instanceof CommonQuestionsDce) {
            $logger->info('La question existe');
            $blobInfo = Atexo_Blob::getBlobById($question->getIdFichier(), $question->getOrganisme(), $question->getNomFichier());
            $arrayBlob = [$blobInfo];
            $logger->info("Mettre les informations du fichier question  dans l'objet Atexo_Rest_ObjectField_DocumentField");
            $documentField = (new Atexo_Document())->getDocumentFieldByInfo($question->getIdFichier(), $question->getNomFichier(), $question->getOrganisme(), $question->getIdFichier(), $blobInfo['content'], 'QUESTION', null, null, null, null, null, null, $typeChecksum);
            $contentType = Atexo_Util::getContentTypeByExtension(Atexo_Util::getExtension($question->getNomFichier()));
            $logger->info("La récupération de l'xml contenant les informations du fichier de la question ");
            $xmlResponse = (new Atexo_Rest_Action_Actions())->getXmlInfosObjectField($documentField);

            return false;
        } else {
            return "La question dont l'id fichier = ".$id." n'existe pas";
        }
    }
}
