<?php

namespace Application\Service\Atexo\Registres;

use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;

/**
 * Vo pour définir les critères de Atexo_Consultation::search().
 *
 * @author Adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2009
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Registres_CriteriaVo extends Atexo_Consultation_CriteriaVo
{
    private string $_registreFrom = '';

    public function getRegistreFrom()
    {
        return $this->_registreFrom;
    }

    public function setRegistreFrom($value)
    {
        $this->_registreFrom = $value;
    }
}
