<?php

namespace Application\Service\Atexo\Recherche;

/**
 * Vo pour définir les critères de Mes recherches.
 *
 * @author LEZ <loubna.ezziani@atexo.com>
 *
 * @version 1.0
 *
 * @since 4.6.0
 *
 * @copyright Atexo 2014
 */
class Atexo_Recherche_CriteriaVo
{
    private ?int $idRecherche = null;
    private ?string $nomRecherche = null;
    private ?int $idCreateur = null;
    private ?string $typeCreateur = null;
    private string $recherche = '0';
    private string|int $alerte = '0';
    private ?string $periodicite = null;
    private ?int $format = null;

    /**
     * recupere la valeur du [idRecherche].
     *
     * @return int la valeur courante [idRecherche]
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function getIdRecherche()
    {
        return $this->idRecherche;
    }

    /**
     * modifie la valeur de [idRecherche].
     *
     * @param int $idRecherche la valeur a mettre
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function setIdRecherche($idRecherche)
    {
        $this->idRecherche = $idRecherche;
    }

    /**
     * recupere la valeur du [nomRecherche].
     *
     * @return string la valeur courante [nomRecherche]
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function getNomRecherche()
    {
        return $this->nomRecherche;
    }

    /**
     * modifie la valeur de [nomRecherche].
     *
     * @param string $nomRecherche la valeur a mettre
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function setNomRecherche($nomRecherche)
    {
        $this->nomRecherche = $nomRecherche;
    }

    /**
     * recupere la valeur du [idCreateur].
     *
     * @return int la valeur courante [idCreateur]
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function getIdCreateur()
    {
        return $this->idCreateur;
    }

    /**
     * modifie la valeur de [idCreateur].
     *
     * @param int $idCreateur la valeur a mettre
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function setIdCreateur($idCreateur)
    {
        $this->idCreateur = $idCreateur;
    }

    /**
     * recupere la valeur du [typeCreateur].
     *
     * @return string la valeur courante [typeCreateur]
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function getTypeCreateur()
    {
        return $this->typeCreateur;
    }

    /**
     * modifie la valeur de [typeCreateur].
     *
     * @param string $typeCreateur la valeur a mettre
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function setTypeCreateur($typeCreateur)
    {
        $this->typeCreateur = $typeCreateur;
    }

    /**
     * recupere la valeur du [recherche].
     *
     * @return string la valeur courante [recherche]
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function getRecherche()
    {
        return $this->recherche;
    }

    /**
     * modifie la valeur de [recherche].
     *
     * @param string $recherche la valeur a mettre
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function setRecherche($recherche)
    {
        $this->recherche = $recherche;
    }

    /**
     * recupere la valeur du [alerte].
     *
     * @return int la valeur courante [alerte]
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function getAlerte()
    {
        return $this->alerte;
    }

    /**
     * modifie la valeur de [alerte].
     *
     * @param int $alerte la valeur a mettre
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function setAlerte($alerte)
    {
        $this->alerte = $alerte;
    }

    /**
     * recupere la valeur du [periodicite].
     *
     * @return string la valeur courante [periodicite]
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function getPeriodicite()
    {
        return $this->periodicite;
    }

    /**
     * modifie la valeur de [periodicite].
     *
     * @param string $periodicite la valeur a mettre
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function setPeriodicite($periodicite)
    {
        $this->periodicite = $periodicite;
    }

    /**
     * recupere la valeur du [format].
     *
     * @return int la valeur courante [format]
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * modifie la valeur de [format].
     *
     * @param int $format la valeur a mettre
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function setFormat($format)
    {
        $this->format = $format;
    }
}
