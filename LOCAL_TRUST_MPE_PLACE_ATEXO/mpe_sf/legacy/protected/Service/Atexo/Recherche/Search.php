<?php

namespace Application\Service\Atexo\Recherche;

use Application\Propel\Mpe\CommonInvitePermanentTransverseQuery;
use Application\Propel\Mpe\CommonOrganismePeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Exception;
use Application\Service\Atexo\Consultation\Atexo_Consultation_SearchLucene;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_PaniersEntreprises;
use Application\Service\Atexo\Referentiel\Atexo_Referentiel_Referentiel;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Recherche_Search
{
    /**
     * construit la requete des conditions de la recherche.
     *
     * @param array $queryParams
     *
     * @return string sql
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public static function otherConditions(Atexo_Consultation_CriteriaVo $criteriaVo, &$queryParams)
    {
        $texteRecherche = null;
        $otherConditions = '';
        if ('tout' != $criteriaVo->getTypeSearch()) {
            if ($criteriaVo->getIdReference()) {
                $otherConditions .= ' AND consultation.id=:idReference';
                $queryParams[':idReference'] = $criteriaVo->getIdReference();
            }
            if ($criteriaVo->getReferenceConsultation()) {
                $otherConditions .= ' AND consultation.reference_utilisateur LIKE :referUser ';
                $queryParams[':referUser'] = '%'.$criteriaVo->getReferenceConsultation().'%';
            }
            if ($criteriaVo->getReferenceConsultationRestreinte()) {
                $otherConditions .= ' AND consultation.reference_utilisateur =:referUser ';
                $queryParams[':referUser'] = $criteriaVo->getReferenceConsultationRestreinte();
            }

            // Debut du filtre des consultations à archivées
            if ($criteriaVo->getConsultationAArchiver()) {
                //Exclure les consultations SAD  AC
                $exclusionIdSadAc = <<<QUERY

AND id_type_procedure_org NOT IN (
SELECT
	Type_Procedure_Organisme.id_type_procedure
FROM
	Type_Procedure_Organisme
WHERE
	Type_Procedure_Organisme.organisme = :organismeAgent
	AND (( Type_Procedure_Organisme.sad = '1') OR (Type_Procedure_Organisme.accord_cadre ='1'))

)
QUERY;

                //Condition Préparation
                $conditionPreparation = <<<QUERY

 (
ID_ETAT_CONSULTATION = 0
AND ( DATEFIN > CURDATE() OR DATEFIN = '0000-00-00 00:00:00')
AND (
	 DATE_MISE_EN_LIGNE_CALCULE = ''
	 OR DATE_MISE_EN_LIGNE_CALCULE = '0000-00-00 00:00:00'
	 OR DATE_MISE_EN_LIGNE_CALCULE > SYSDATE()
	 OR DATE_MISE_EN_LIGNE_CALCULE IS NULL
)
AND DATEDEBUT != '' AND DATEDEBUT != '0000-00-00 00:00:00'
AND DATEDEBUT < DATE_SUB(SYSDATE(),INTERVAL :duree_rappel_archivage MONTH )
 )
QUERY;
                //Condition Ouverture et Analyse
                $conditionOuvertureAnalyse = <<<QUERY

OR (
	ID_ETAT_CONSULTATION = 0 AND DATEFIN != ''
	AND DATEFIN != '0000-00-00 00:00:00'
	AND DATEFIN < DATE_SUB(SYSDATE(),INTERVAL :duree_rappel_archivage MONTH)
)
QUERY;
                $conditionDecision = <<<QUERY

OR (
	ID_ETAT_CONSULTATION = :status_decision
	AND  DATE_DECISION != ''
	AND DATE_DECISION != '0000-00-00 00:00:00'
	AND DATE_DECISION < DATE_SUB(CURDATE(),INTERVAL :duree_rappel_archivage MONTH)
 )
QUERY;
                $queryParams[':duree_rappel_archivage'] = Atexo_Config::getParameter('DUREE_RAPPEL_ARCHIVAGE');
                $queryParams[':status_decision'] = Atexo_Config::getParameter('STATUS_DECISION');

                //Condition Decision
                $conditionCommun = <<<QUERY

DEPOUILLABLE_PHASE_CONSULTATION = '0'
AND ID_TYPE_AVIS = :type_avis_consultation
$exclusionIdSadAc
QUERY;
                $queryParams[':type_avis_consultation'] = Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION');

                $otherConditions .= <<<QUERY
 AND ( $conditionCommun  ) AND (  $conditionPreparation  $conditionOuvertureAnalyse  $conditionDecision  )
QUERY;
            }
            // Fin du filtre des consultations à archivées

            if (true === $criteriaVo->getcalledFromPortail()) {
                if ($criteriaVo->getAcronymeOrganisme()) {
                    $otherConditions .= ' AND consultation.organisme=:acronymeOrg ';
                    $queryParams[':acronymeOrg'] = $criteriaVo->getAcronymeOrganisme();
                }
                $idsService = $criteriaVo->getIdsService();
                if (!$idsService && $criteriaVo->getIdService() && $criteriaVo->getInclureDescendance()) {
                    $idsService = Atexo_EntityPurchase::retrieveAllChildrenServices($criteriaVo->getIdService(), $criteriaVo->getAcronymeOrganisme());
                }
                if ('' != $idsService) {//On inclut la descendance dans la recherche
                    $otherConditions .= ' AND (consultation.service_id IN '.$idsService.' )';
                } else {
                    if ('' != $criteriaVo->getIdService()) {
                        $otherConditions .= ' AND ((consultation.service_id=:serviceId) OR  (consultation.service_associe_id=:serviceId))';
                        $queryParams[':serviceId'] = $criteriaVo->getIdService();
                    }
                }

                if ($criteriaVo->getTypeAcces() != Atexo_Config::getParameter('TYPE_PROCEDURE_RESTREINTE')) {
                    if (Atexo_Module::isEnabled('PanierEntreprise') && true === $criteriaVo->getcalledFromPortail() && $criteriaVo->getForPanierEntreprise()) {
                        $otherConditions .= ' AND ( consultation.type_acces = :typeProc1 OR consultation.type_acces = :typeProc2 )';
                        $queryParams[':typeProc1'] = Atexo_Config::getParameter('TYPE_PROCEDURE_RESTREINTE');
                        $queryParams[':typeProc2'] = Atexo_Config::getParameter('TYPE_PROCEDURE_PUBLICITE');
                    } else {
                        $otherConditions .= ' AND consultation.type_acces=:typeProc ';
                        $queryParams[':typeProc'] = Atexo_Config::getParameter('TYPE_PROCEDURE_PUBLICITE');
					}
				}

				if(trim($criteriaVo->getOrgDenomination())) {
					$otherConditions.= self::searchOrgDenominationLikeMotcle(trim($criteriaVo->getOrgDenomination()),$queryParams);
				}

                if($criteriaVo->getDenominationAdapte()) {
                    $list = $criteriaVo->getDenominationAdapte();
                    if (is_array($criteriaVo->getDenominationAdapte())){
                        $denominationAdapte = array_map('addslashes',$criteriaVo->getDenominationAdapte()) ;
                        $list = implode("','", $denominationAdapte);
                    }
                    $otherConditions.=" AND denomination_adapte in( '" . addslashes($list) . "') ";

                }
            } else {
                if (Atexo_Module::isEnabled('OrganisationCentralisee', $criteriaVo->getAcronymeOrganisme())) {
                    $idsService = $criteriaVo->getIdsService();
                    if ('' != $idsService) {
                        $otherConditions .= ' AND (consultation.service_id IN '.$idsService.' )';
                    }
                }
            }
            if ($criteriaVo->getDateFinStart()) {
                $otherConditions .= ' AND consultation.datefin >= :dateFinStart ';
                $queryParams[':dateFinStart'] = Atexo_Util::frnDate2iso($criteriaVo->getDateFinStart()).' 00:00:00';
            }
            if ($criteriaVo->getDateFinEnd()) {
                $otherConditions .= ' AND consultation.datefin <= :dateFinEnd  ';
                $queryParams[':dateFinEnd'] = Atexo_Util::frnDate2iso($criteriaVo->getDateFinEnd()).' 23:59:59';
            }
            if ($criteriaVo->getDateTimeFin()) {
                $otherConditions .= ' AND consultation.datefin <= :dateFinEnd  ';
                $queryParams[':dateFinEnd'] = $criteriaVo->getDateTimeFin();
            }
            $cpvConditions = '';
            if ($criteriaVo->getIdCodeCpv2()) {
                $codesCPV = explode('#', $criteriaVo->getIdCodeCpv2());
                if (is_array($codesCPV) && $codesCPV) {
                    $indexCodeCpv = 0;
                    foreach ($codesCPV as $cpv) {
                        //si jamais le code CPV est codé sur 10 car. on enlève les 2 derniers. ex : 30000000-0
                        if (10 == strlen($cpv)) {
                            $cpv = substr($cpv, 0, -2);
                        }
                        if ($cpv) {
                            $cpvConditions .= <<<QUERY

OR consultation.code_cpv_1 like :code_cpv_1$indexCodeCpv
OR consultation.code_cpv_2 like :codeCpv$indexCodeCpv

OR CategorieLot.code_cpv_1 like :code_cpv_1$indexCodeCpv
OR CategorieLot.code_cpv_2 like :codeCpv$indexCodeCpv
QUERY;
                            $queryParams[':codeCpv'.$indexCodeCpv] = '%#'.rtrim($cpv, '0').'%';
                            $queryParams[':code_cpv_1'.$indexCodeCpv] = rtrim($cpv, '0').'%';
                        }
                        ++$indexCodeCpv;
                    }
                }
            }
            if ($cpvConditions) {
                $otherConditions .= " AND (0 $cpvConditions)";
            }

            if ($criteriaVo->getCategorieConsultation()) {
                $otherConditions .= ' AND consultation.categorie=:categorie ';
                $queryParams[':categorie'] = $criteriaVo->getCategorieConsultation();
            }
            if ($criteriaVo->getIdTypeProcedure()) {
                if ($criteriaVo->getCalledFromHelios() || false === $criteriaVo->getcalledFromPortail()) {
                    $otherConditions .= ' AND consultation.id_type_procedure_org=:idtypeProc ';
                    $queryParams[':idtypeProc'] = $criteriaVo->getIdTypeProcedure();
                } else {
                    $otherConditions .= ' AND consultation.id_type_procedure=:idtypeProc ';
                    $queryParams[':idtypeProc'] = $criteriaVo->getIdTypeProcedure();
                }
            }
            if (Atexo_Module::isEnabled('LieuxExecution')) {
                if ($criteriaVo->getLieuxexecution()) {
                    $arrayLieuExecution = explode(',', $criteriaVo->getLieuxexecution());
                    $indiceLieuExecution = 1;
                    $lieuExecQuery = '';
                    foreach ($arrayLieuExecution as $unLieuExec) {
                        if ($unLieuExec) {
                            $lieuExecQuery .= ((1 == $indiceLieuExecution) ? '' : ' OR ').' consultation.lieu_execution LIKE :lieuExec'.$indiceLieuExecution.' ';
                            $queryParams[':lieuExec'.$indiceLieuExecution] = '%,'.$unLieuExec.',%';
                            ++$indiceLieuExecution;
                        }
                    }
                    $otherConditions .= ($lieuExecQuery) ? ' AND ('.$lieuExecQuery.')' : '';
                }
            }
            //codes nuts
            if (Atexo_Module::isEnabled('CodeNutLtReferentiel')) {
                $codesNutsQuery = '';
                if ($criteriaVo->getCodesNuts()) {
                    $listeCodesNuts = explode('#', $criteriaVo->getCodesNuts());
                    if ($listeCodesNuts[1]) {
                        $indiceCodesNuts = 1;
                        foreach ($listeCodesNuts as $codesNuts) {
                            if ($codesNuts) {
                                //Recherche des descendants complets
                                $codesNutsQuery .= ((1 == $indiceCodesNuts) ? '' : ' OR ').' consultation.codes_nuts LIKE :codesNuts'.$indiceCodesNuts.' ';
                                $queryParams[':codesNuts'.$indiceCodesNuts] = '%#'.$codesNuts.'%';

                                $indiceCodesNutsAsc = 1;
                                //Recherche des ascendants directs
                                while (strlen($codesNuts) > Atexo_Config::getParameter('NOMBRE_MINIMUM_CARACTERE_ASCENDANT_DIRECT_CODE_NUT')) {
                                    //Lorsque l'ascendant cherché se trouve entre 2 codes
                                    $codesNutsQuery .= ' OR consultation.codes_nuts LIKE :codesNutsAscMedium'.$indiceCodesNuts.$indiceCodesNutsAsc.' ';
                                    $queryParams[':codesNutsAscMedium'.$indiceCodesNuts.$indiceCodesNutsAsc] = '%#'.self::supprimerDernierCaractere($codesNuts).'#%';

                                    //Lorsque l'ascendant cherché se trouve au à l'extremité, ce cas inclus lorsque l'ascendant direct est le seul selectionné
                                    $codesNutsQuery .= ' OR consultation.codes_nuts LIKE :codesNutsAscLast'.$indiceCodesNuts.$indiceCodesNutsAsc.' ';
                                    $queryParams[':codesNutsAscLast'.$indiceCodesNuts.$indiceCodesNutsAsc] = '%#'.self::supprimerDernierCaractere($codesNuts);

                                    $codesNuts = self::supprimerDernierCaractere($codesNuts);
                                    ++$indiceCodesNutsAsc;
                                }

                                ++$indiceCodesNuts;
                            }
                        }
                        if ($codesNutsQuery) {
                            $otherConditions .= ' AND ('.$codesNutsQuery.')';
                        }
                    }
                }
            }
            //fin codes nuts
        }

        if ($criteriaVo->getTypeAcces() == Atexo_Config::getParameter('TYPE_PROCEDURE_RESTREINTE')) {
            $otherConditions .= ' AND consultation.type_acces=:typeProc ';
            $queryParams[':typeProc'] = $criteriaVo->getTypeAcces();
            $otherConditions .= ' AND consultation.organisme=:acronymeOrg ';
            $queryParams[':acronymeOrg'] = $criteriaVo->getAcronymeOrganismeProcRest();
            $otherConditions .= ' AND consultation.code_procedure=:codeProc ';
            $queryParams[':codeProc'] = $criteriaVo->getCodeAccesProcedureRestreinte();
        }

        if ($criteriaVo->getIdTypeAvis()) {
            $otherConditions .= ' AND consultation.id_type_avis=:typeAvis ';
            $queryParams[':typeAvis'] = $criteriaVo->getIdTypeAvis();
        }

        if ($criteriaVo->getNotIdTypeAvis()) {
            $otherConditions .= ' AND consultation.id_type_avis!=:notTypeAvis ';
            $queryParams[':notTypeAvis'] = $criteriaVo->getNotIdTypeAvis();
        }

        if ($criteriaVo->getPublicationEurope()) {
            $otherConditions .= ' AND publication_europe=:publicationEurop ';
            $queryParams[':publicationEurop'] = $criteriaVo->getPublicationEurope();
        }
        //Début recherche date mise en ligne calculée
        if ($criteriaVo->getEnLigneDepuis()) {
            $otherConditions .= ' AND date_mise_en_ligne_calcule>=:dateMiseEnLigneCalculeStar';
            $queryParams[':dateMiseEnLigneCalculeStar'] = Atexo_Util::frnDate2iso($criteriaVo->getEnLigneDepuis()).' 00:00:00';
        }
        if ($criteriaVo->getEnLigneJusquau()) {
            $otherConditions .= ' AND date_mise_en_ligne_calcule<=:dateMiseEnLigneCalculeEnd';
            $queryParams[':dateMiseEnLigneCalculeEnd'] = Atexo_Util::frnDate2iso($criteriaVo->getEnLigneJusquau()).' 23:59:59';
        }
        //Fin recherche date mise en ligne calculée
        if ($criteriaVo->getEtatPublicationToBePublished() || $criteriaVo->getEtatPublicationPublished()
          || $criteriaVo->getEtatPublicationToBeUpdated() || $criteriaVo->getEtatPublicationUpdated()) {
            $etatPublicationCondition = '';
            if ($criteriaVo->getEtatPublicationToBePublished()) {
                $etatPublicationCondition = (($etatPublicationCondition) ? ' OR ' : '')." etat_publication='".Atexo_Config::getParameter('TO_BE_PUBLISHED')."' ";
            }
            if ($criteriaVo->getEtatPublicationPublished()) {
                $etatPublicationCondition .= (($etatPublicationCondition) ? ' OR ' : '')." etat_publication='".Atexo_Config::getParameter('PUBLISHED')."' ";
            }
            if ($criteriaVo->getEtatPublicationToBeUpdated()) {
                $etatPublicationCondition .= (($etatPublicationCondition) ? ' OR ' : '')." etat_publication='".Atexo_Config::getParameter('TO_BE_UPDATED')."' ";
            }
            if ($criteriaVo->getEtatPublicationUpdated()) {
                $etatPublicationCondition .= (($etatPublicationCondition) ? ' OR ' : '')." etat_publication='".Atexo_Config::getParameter('UPDATED')."' ";
            }
            if ($etatPublicationCondition) {
                $otherConditions .= ' AND ('.$etatPublicationCondition.' ) ';
            }
        }

        $rechercheExacte = false;
        $rechercheApproximative = false;
        $searchChaqueMot = true;
        if ($criteriaVo->getKeyWordAdvancedSearch()) {
            $texteRecherche = str_replace('&quot;', '"', $criteriaVo->getKeyWordAdvancedSearch());
            $keyWord = strtolower($texteRecherche);

            $arrayWorrd = explode('"', $texteRecherche);
            if ($criteriaVo->getSearchModeExact()) {
                if (3 == count($arrayWorrd)) {
                    $searchChaqueMot = false;
                    $texteRecherche = $arrayWorrd[1];
                }
                $rechercheExacte = true;
            } else {
                if (3 != count($arrayWorrd)) {
                    $rechercheApproximative = true;
                    $precision = Atexo_Config::getParameter('PRECISION_LUCENE');
                    if (false === $criteriaVo->getcalledFromPortail()) {
                        $organisme = $criteriaVo->getAcronymeOrganisme();
                        if ($criteriaVo->getFromArchive()) {
                            $pathDirIndexLucene = Atexo_Config::getParameter('BASE_ROOT_DIR').$organisme.'/'.Atexo_Config::getParameter('DIR_INDEX_LUCENE_ARCHIVE');
                        } else {
                            $pathDirIndexLucene = Atexo_Config::getParameter('BASE_ROOT_DIR').$organisme.'/'.Atexo_Config::getParameter('DIR_INDEX_LUCENE_AGENT');
                        }
                        $searchLucene = new Atexo_Consultation_SearchLucene($pathDirIndexLucene);
                        $arrayIdToFilter = $searchLucene->searchByOrganisme($criteriaVo->getAcronymeOrganisme(), $keyWord, $precision);
                        if (!empty($arrayIdToFilter)) {
                            $strIdToFilter = implode("', '", $arrayIdToFilter);
                            $strIdToFilter = "('".$strIdToFilter."')";
                        } else {
                            $strIdToFilter = '(0)';
                        }
                        // Que les résultats de Lucene :
                        $otherConditions .= " AND (consultation.id IN $strIdToFilter ";
                    } else {
                        $searchLucene = new Atexo_Consultation_SearchLucene(Atexo_Config::getParameter('DIR_INDEX_LUCENE_PORTAIL'));
                        $arrayIdToFilter = $searchLucene->searchInAllOrganisme($keyWord, $precision);
                        if (!empty($arrayIdToFilter)) {
                            $otherConditions .= ' AND ((0 ';
                            foreach ($arrayIdToFilter as $org => $oneRef) {
                                $strIdToFilter = implode("', '", $oneRef);
                                $strIdToFilter = "('".$strIdToFilter."')";
                                $otherConditions .= " OR (consultation.organisme = '".$org."' AND consultation.id IN ".$strIdToFilter.') ';
                            }
                            $otherConditions .= ' )';
                        } else {
                            // Si Lucene ne retourne aucuns reusltats :
                            $otherConditions .= ' AND (0 ';
                        }
                    }
                } else {
                    $searchChaqueMot = false;
                    $texteRecherche = $arrayWorrd[1];
                    $rechercheExacte = true;
                }
            }
        }
        if ($rechercheExacte || $rechercheApproximative || $criteriaVo->getKeyWordRechercheRapide()) {
            if ($criteriaVo->getKeyWordRechercheRapide()) {
                $texteRecherche = $criteriaVo->getKeyWordRechercheRapide();
            }

            if ($texteRecherche) {
                $querySelectKeyWord = self::searchLikeMotcle($texteRecherche, $criteriaVo->getcalledFromPortail(), $searchChaqueMot, $queryParams);
                $otherConditions .= ' '.($rechercheApproximative ? ' OR ' : '  AND').' ( '.' 0 '.(($querySelectKeyWord) ? ' OR '.$querySelectKeyWord : '').' ) '.($rechercheApproximative ? ')' : '');
            }
        }

        if (Atexo_Module::isEnabled('ConsultationDomainesActivites')) {
            if ($criteriaVo->getDomaineActivite()) {
                $domainesActivites = explode('#', trim($criteriaVo->getDomaineActivite(), '#'));
                if (is_array($domainesActivites) && count($domainesActivites) >= 1) {
                    $conditions = [];
                    foreach ($domainesActivites as $activite) {
                        $conditions[] = "( consultation.`domaines_activites` like '%#".(new Atexo_Db())->quote($activite)."%' OR  consultation.`domaines_activites` like '".(new Atexo_Db())->quote($activite)."%' )";
                    }
                    $otherConditions .= ' AND ( '.implode(' OR ', $conditions).' ) ';
                }
            }
        }

        // Domaines d'activités Lt-Référentiel
        if (Atexo_Module::isEnabled('ConsultationDomainesActivitesLtReferentiel')) {
            $domainesActivitesLtRef = '';
            if ($criteriaVo->getDomaineActivite()) {
                $listeDomainesActivitesLtRef = explode('#', $criteriaVo->getDomaineActivite());
                if ($listeDomainesActivitesLtRef[1]) {
                    $indiceDomainesActivitesLtRef = 1;
                    foreach ($listeDomainesActivitesLtRef as $domainesActivites) {
                        if ($domainesActivites) {
                            $domainesActivitesLtRef .= ((1 == $indiceDomainesActivitesLtRef) ? '' : ' OR ').' consultation.domaines_activites LIKE :domaines_activites'.$indiceDomainesActivitesLtRef.' ';
                            $queryParams[':domaines_activites'.$indiceDomainesActivitesLtRef] = '%#'.rtrim($domainesActivites, '0').'%';
                            ++$indiceDomainesActivitesLtRef;
                        }
                    }
                    if ($domainesActivitesLtRef) {
                        $otherConditions .= ' AND ('.$domainesActivitesLtRef.')';
                    }
                }
            }
        }

        if ($criteriaVo->getQualification()) {
            $qualifications = explode('#', trim($criteriaVo->getQualification(), '#'));
            if (is_array($qualifications) && count($qualifications) >= 1) {
                $conditions = [];
                foreach ($qualifications as $qualification) {
                    $conditions[] = "(  consultation.`qualification` like '%#".(new Atexo_Db())->quote($qualification)."#%' OR  consultation.`qualification` like '".(new Atexo_Db())->quote($qualification)
                               ."#%' OR  consultation.`qualification` like '%#".(new Atexo_Db())->quote($qualification)."' )";
                }
                $otherConditions .= ' AND ( '.implode(' OR ', $conditions).' ) ';
            }
        }

        if ($criteriaVo->getAgrements()) {
            $agrements = explode(',', trim($criteriaVo->getAgrements(), ','));
            if (is_array($agrements) && count($agrements) >= 1) {
                $conditions = [];
                foreach ($agrements as $agrement) {
                    $conditions[] = "(  consultation.`agrements` like '%,".(new Atexo_Db())->quote($agrement).",%' OR  consultation.`agrements` like '".
                                   (new Atexo_Db())->quote($agrement).",%' OR  consultation.`agrements` like '%,".(new Atexo_Db())->quote($agrement)."' )";
                }
                $otherConditions .= ' AND ( '.implode(' OR ', $conditions).' ) ';
            }
        }

        //jointure inner join on Organisme
        if (true === $criteriaVo->getcalledFromPortail() && $criteriaVo->getTypeOrganisme()) {
            $otherConditions .= " AND Organisme.categorie_insee='".(new Atexo_Db())->quote($criteriaVo->getTypeOrganisme())."'";
        }

        $referenceByReferentiel = Atexo_Referentiel_Referentiel::searchConsultation($criteriaVo->getReferentielVo(), $criteriaVo->getAcronymeOrganisme());
        if ($referenceByReferentiel) {
            $otherConditions .= ' AND (consultation.id IN ( '.implode(',', $referenceByReferentiel).' ) )';
        }
        //     // les alertes metiers

        if (0 != $criteriaVo->getIdAlerteConsultation() && 0 != $criteriaVo->getAlerteConsultationCloturee()) {
            $otherConditions .= " AND alerte_metier.cloturee ='".(new Atexo_Db())->quote($criteriaVo->getAlerteConsultationCloturee())."' ";
            $otherConditions .= " AND alerte_metier.id_alerte ='".(new Atexo_Db())->quote($criteriaVo->getIdAlerteConsultation())."'";
        } elseif (0 == $criteriaVo->getIdAlerteConsultation() && 2 == $criteriaVo->getAlerteConsultationCloturee()) {
            $otherConditions .= " AND consultation.alerte='1' ";
        }

        // Les clauses achat responsable
        if (Atexo_Module::isEnabled('ConsultationClause')) {
            $hasConditionCC = false;
            $arrayCond = [];

            if (0 != $criteriaVo->getClauseSociale()) {
                $hasConditionCC = true;
                $arrayCond[] = "clause_sociale='".$criteriaVo->getClauseSociale()."'";
            }

            if (0 != $criteriaVo->getAtelierProtege()) {
                $hasConditionCC = true;
                $arrayCond[] = "clause_sociale_ateliers_proteges='".($criteriaVo->getAtelierProtege() == Atexo_Config::getParameter('CLAUSES_SOCIALES_ACTIVER') ? '1' : '0')."'";
            }

            if (0 != $criteriaVo->getSiae()) {
                $hasConditionCC = true;
                $arrayCond[] = "clause_sociale_siae='".($criteriaVo->getSiae() == Atexo_Config::getParameter('CLAUSES_SOCIALES_ACTIVER') ? '1' : '0')."'";
            }

            if (0 != $criteriaVo->getEss()) {
                $hasConditionCC = true;
                $arrayCond[] = "clause_sociale_ess='".($criteriaVo->getEss() == Atexo_Config::getParameter('CLAUSES_SOCIALES_ACTIVER') ? '1' : '0')."'";
            }

            if (0 != $criteriaVo->getClauseEnv()) {
                $hasConditionCC = true;
                $arrayCond[] = "clause_environnementale='".$criteriaVo->getClauseEnv()."'";
            }

            if (0 != $criteriaVo->getSocialeCommerceEquitable()) {
                $not = '';
                if (2 == $criteriaVo->getSocialeCommerceEquitable()) {
                    $not = ' NOT ';
                }
                //$hasConditionCC = true;
                $otherConditions .= <<<QUERY
                
AND
(
	( 
	    consultation.clause_specification_technique $not like '%:1:"4"%' 
	    OR consultation.clause_sociale_condition_execution $not like '%:1:"4"%' 
	    OR consultation.clause_sociale_insertion $not like '%:1:"4"%'
	)
	OR
	(
		SELECT
			COUNT(*)
		FROM
			CategorieLot as catLot,
			consultation as cons
		WHERE
			catLot.consultation_id = cons.id
			AND consultation.id = cons.id
			AND catLot.organisme = cons.organisme
			AND consultation.organisme=cons.organisme
			AND ( catLot.clause_specification_technique $not like '%:1:"4"%' 
	    OR catLot.clause_sociale_condition_execution $not like '%:1:"4"%' 
	    OR catLot.clause_sociale_insertion $not like '%:1:"4"%')
	)<>0
)
QUERY;
            }

            if ('' != $criteriaVo->getSocialeInsertionActiviterEconomique()) {
                $not = '';
                if (2 == $criteriaVo->getSocialeInsertionActiviterEconomique()) {
                    $not = ' NOT ';
                }
                //$hasConditionCC = true;
                $otherConditions .= <<<QUERY
                
AND
(
	( 
	    consultation.clause_specification_technique $not like '%:1:"1"%' 
	    OR consultation.clause_sociale_condition_execution $not like '%:1:"1"%' 
	    OR consultation.clause_sociale_insertion $not like '%:1:"1"%'
	)
	OR
	(
		SELECT
			COUNT(*)
		FROM
			CategorieLot as catLot,
			consultation as cons
		WHERE
			catLot.consultation_id = cons.id
			AND consultation.id = cons.id
			AND catLot.organisme = cons.organisme
			AND consultation.organisme=cons.organisme
			AND ( catLot.clause_specification_technique $not like '%:1:"1"%' 
	    OR catLot.clause_sociale_condition_execution $not like '%:1:"1"%' 
	    OR catLot.clause_sociale_insertion $not like '%:1:"1"%')
	)<>0
)
QUERY;
            }

            if ($hasConditionCC) {
                $conditionCCConsultation = 'consultation.'.implode(' AND consultation.', $arrayCond);
                $conditionCCLots = 'catLot.'.implode(' AND catLot.', $arrayCond);
                $otherConditions .= <<<QUERY

AND
(
	( $conditionCCConsultation )
	OR
	(
		SELECT
			COUNT(*)
		FROM
			CategorieLot as catLot,
			consultation as cons
		WHERE
			catLot.consultation_id = cons.id
			AND consultation.id = cons.id
			AND catLot.organisme = cons.organisme
			AND consultation.organisme=cons.organisme
			AND ( $conditionCCLots )
	)<>0
)
QUERY;
            }
        }
        //Recherche dans le panier de l'entreprise
        if (true === $criteriaVo->getcalledFromPortail() && $criteriaVo->getForPanierEntreprise()) {
            $arrayIdConsPanier = (new Atexo_Entreprise_PaniersEntreprises())->searchPanier($criteriaVo);

            $conditionForPanier = ' AND (0 ';
            if (!empty($arrayIdConsPanier)) {
                foreach ($arrayIdConsPanier as $org => $oneRef) {
                    $listeRefCons = implode("', '", $oneRef);
                    $listeRefCons = "('".$listeRefCons."')";
                    $conditionForPanier .= " OR (consultation.organisme = '".(new Atexo_Db())->quote($org)."' AND consultation.id IN ".$listeRefCons.') ';
                }
            }
            $conditionForPanier .= ' )';
            $otherConditions .= $conditionForPanier;
        }
        //jointure inner join on Organisme
        if ($criteriaVo->getExclureConsultationExterne()) {
            $otherConditions .= " AND (consultation.ref_org_partenaire is NULL OR  consultation.ref_org_partenaire = '')";
            $otherConditions .= ' AND Organisme.id_client_ANM!=:id_client_ANM';
            $queryParams[':id_client_ANM'] = 1;
        }
        if (false !== $criteriaVo->getAfficherDoublon()) {
            $otherConditions .= ' AND consultation.doublon=:doublon';
            $queryParams[':doublon'] = $criteriaVo->getAfficherDoublon();
        }

        if (true == $criteriaVo->getPublieHier()) {
            $date = strftime('%Y-%m-%d %H:%M:%S', mktime(0, 0, 0, date('m'), date('d') - 1, date('y')));
            $otherConditions .= " AND date_mise_en_ligne_calcule >= '".$date."' ";
        }

        if (true == $criteriaVo->getInformationMarche()) {
            $otherConditions .= " AND (id_type_avis = '".Atexo_Config::getParameter('TYPE_AVIS_INFORMATION')."' Or id_type_avis = '".Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION')."' )";
        }

        if (true == $criteriaVo->getWithDce()) {
            $otherConditions .= ' AND consultation.id IN ( SELECT distinct(DCE.consultation_id) FROM DCE, consultation WHERE DCE.organisme=consultation.organisme'
                            ." AND DCE.consultation_id = consultation.id AND DCE.dce != '0')";
        }
        if (true == $criteriaVo->getConsultaionPublier()) {
            $otherConditions .= " AND (date_mise_en_ligne_calcule!='' AND date_mise_en_ligne_calcule IS NOT NULL AND date_mise_en_ligne_calcule != '0000-00-00 00:00:00' "
                            .' AND date_mise_en_ligne_calcule<=now()) ';
        }
        if (true == $criteriaVo->getConsultationAnnulee()) {
            $otherConditions .= " AND consultation_annulee = '1'";
        }
        if (Atexo_Module::isEnabled('gestionOperations', $criteriaVo->getAcronymeOrganisme())) {
            if ($criteriaVo->getIdOperation()) {
                $otherConditions .= " AND idOperation = '".$criteriaVo->getIdOperation()."'";
            }
        }

        //marche public simplifie
        //Nous n'avons pas mis le test Atexo_Module::isEnabled('MarchePublicSimplifie', $criteriaVo->getAcronymeOrganisme()) car ce bloc est appele depuis un Web Service
        if ($criteriaVo->getMps()) {
            $otherConditions .= ' AND consultation.marche_public_simplifie=:marche_public_simplifie ';
            if ('1' === $criteriaVo->getMps()) {//Consultation taggué MPS
                $queryParams[':marche_public_simplifie'] = '1';
            } elseif ('2' === $criteriaVo->getMps()) {//Consultation non taggué MPS
                $queryParams[':marche_public_simplifie'] = '0';
            }
        }
        if ($criteriaVo->getBourseCotraitance()) {
            $idEntreprise = $criteriaVo->getIdEntreprise();
            $comp = ('1' === $criteriaVo->getBourseCotraitance()) ? ' IN ' : (('2' === $criteriaVo->getBourseCotraitance()) ? ' NOT IN ' : '');
            if ($comp) {
                $otherConditions .= <<<QUERY
 AND consultation.id $comp
	(
		SELECT
			DISTINCT(t_bourse_cotraitance.reference_consultation)
		FROM
			t_bourse_cotraitance
		WHERE
		t_bourse_cotraitance.id_Entreprise = $idEntreprise
	)
QUERY;
            }
        }
        if ($criteriaVo->getVisionRma()) {
            if ('' != $criteriaVo->getIdServiceRma()) {
                $otherConditions .= ' AND ((consultation.service_id=:serviceId) OR  (consultation.service_associe_id=:serviceId))';
                $queryParams[':serviceId'] = $criteriaVo->getIdServiceRma();
            }
            if ($criteriaVo->getAcronymeOrganisme()) {
                $otherConditions .= ' AND consultation.organisme=:acronymeOrg ';
                $queryParams[':acronymeOrg'] = $criteriaVo->getAcronymeOrganisme();
            }
        }

        if (Atexo_CurrentUser::isSessionSet('organismes_eligibles')) {
            $orgs = Atexo_CurrentUser::readFromSession('organismes_eligibles');
            if (!empty($orgs)) {
                $orgEligibles = implode("','", $orgs);
                $otherConditions .= " AND (consultation.organisme in ('".$orgEligibles."'))";
            }
        }

        return $otherConditions;
    }

    public function searchLikeMotcle($motCle, $calledFromPortail, $searchChaqueMot, &$queryParams)
    {
        $queryWhere = ' reference_utilisateur LIKE :referUser ';
        $queryParams[':referUser'] = '%'.(new Atexo_Db())->quote($motCle).'%';
        if ($searchChaqueMot) {
            $arrayCar = ['«', '»', ',', ';', '.', '&', '!', '?', "'", '"', '(', ')', '[', ']', '{', '}', '°', '+', '*', '/', '\\', '|', ':', '%'];
            $motCle = str_replace($arrayCar, ' ', $motCle);
            $arrayMotsCle = explode(' ', $motCle);
        } else {
            $arrayMotsCle = ['0' => $motCle];
        }

        $arrayMotInterdit = ' and or et ou le la les un une du de des a à au aux son sa ses ne ni non sauf ce ces cet je tu il elle on nous vous ils elles etc mon ma ton ta vos se y en sur ';
        $indice = 0;
        foreach ($arrayMotsCle as $unMotCle) {
            $unMotCle = trim((new Atexo_Db())->quote($unMotCle));
            if ($unMotCle && !strstr($arrayMotInterdit, $unMotCle) && strlen($unMotCle) >= 2) {
                $queryWhere .= ' OR '
                   .' intitule like :unMotCle'.$indice
                       .' OR '
                   .' objet like :unMotCle'.$indice
                   .(($calledFromPortail) ? '' : ' OR champ_supp_invisible like :unMotCle'.$indice)
                   .' OR description LIKE :unMotCle'.$indice
                   .' OR description_detail LIKE :unMotCle'.$indice;
                $queryParams[':unMotCle'.$indice] = '%'.$unMotCle.'%';
                ++$indice;
            }
        }

        return $queryWhere;
    }

    public static function stateConditions($criteriaVo, &$queryParams)
    {
        $conditionEtatPreparation = " ( ( datefin>now() OR datefin = '0000-00-00 00:00:00' ) AND ";
        $conditionEtatPreparation .= " (date_mise_en_ligne_calcule='' OR date_mise_en_ligne_calcule IS NULL OR date_mise_en_ligne_calcule = '0000-00-00 00:00:00' OR date_mise_en_ligne_calcule>now()) ) ";
        $stateCondition = ''; //si getEtatConsultation() vide alors => recupérer les consultations quelques soient leurs états
        if ('' !== $criteriaVo->getEtatConsultation() || '' !== $criteriaVo->getSecondConditionOnEtatConsultation()) {
            //si getEtatConsultation() égale à 'DECISION' alors => recupérer toutes les consultations avec un id_etat_consultation != 0 (c'est à dire qu'un agent a positionné la consultation à un état donné)
            if ($criteriaVo->getEtatConsultation() == Atexo_Config::getParameter('STATUS_DECISION') || $criteriaVo->getSecondConditionOnEtatConsultation() == Atexo_Config::getParameter('STATUS_DECISION')) {
                $stateCondition .= ' AND ( consultation.id_etat_consultation=:EtatConsultation OR ( depouillable_phase_consultation=:phaseSAD AND NOT ('.$conditionEtatPreparation.'))'.')';
                $queryParams[':EtatConsultation'] = Atexo_Config::getParameter('STATUS_DECISION');
                $queryParams[':phaseSAD'] = Atexo_Config::getParameter('CONSULTATION_DEPOUIABLE_PHASE_1');
            }
            if ($criteriaVo->getEtatConsultation() == Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE')
            || $criteriaVo->getSecondConditionOnEtatConsultation() == Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE')) {
                $stateCondition .= ' AND ( (consultation.id_etat_consultation=:EtatConsultation OR (consultation.id_etat_consultation=:Decision AND  ';
                $queryParams[':EtatConsultation'] = Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE');
                $queryParams[':Decision'] = '0';
                $queryParams[':phaseSAD'] = Atexo_Config::getParameter('CONSULTATION_DEPOUIABLE_PHASE_1');
                $stateCondition .= " ( datefin <= now()  AND datefin != '0000-00-00 00:00:00')  ) )";
                $stateCondition .= ' OR ( depouillable_phase_consultation=:phaseSAD AND NOT ( '.$conditionEtatPreparation."  ) AND (datefin<=now() AND datefin != '0000-00-00 00:00:00') ) )";
            }
            if ($criteriaVo->getEtatConsultation() == Atexo_Config::getParameter('STATUS_CONSULTATION')
               || $criteriaVo->getSecondConditionOnEtatConsultation() == Atexo_Config::getParameter('STATUS_CONSULTATION')) {
                if ($criteriaVo->getcalledFromPortail()) {
                    //utilise pour afficher le contenu synthetique d'une consultation cloturee
                    if ($criteriaVo->getAvecConsClotureesSansPoursuivreAffichage()) {
                        $stateCondition .= ' AND ( datefin < now() AND ( ';
                    } else {
                        if ('1' == Atexo_Config::getParameter('ANNONCES_MARCHES') || $criteriaVo->getSansCritereActive()) {
                            $stateCondition .= ' AND ( ( DATE_FIN_UNIX > unix_timestamp() ) AND ( ';
                        } elseif ($criteriaVo->getOrganismeTest()) {
                            $listOrganismeInactifs = (new CommonOrganismePeer())->retrieveAcronymeAllInactifsOrganismes();
                            $list = implode("','", $listOrganismeInactifs);
                            $stateCondition .= " AND organisme IN ('".$list."') AND ( ( DATE_FIN_UNIX > unix_timestamp() ) AND consultation.id_etat_consultation='0' AND consultation_annulee='0' AND ( ";
                        } else {
                            $stateCondition .= " AND Organisme.active = '".$criteriaVo->getOrgActive()."' AND ( ( DATE_FIN_UNIX > unix_timestamp() ) AND consultation.id_etat_consultation='0' AND consultation_annulee='0' AND ( ";
                        }
                    }
                } else {
                    $stateCondition .= " AND ( datefin > now() AND consultation.id_etat_consultation='0' AND ( ";
                }
                if ('' != $criteriaVo->getPeriodicite()) {
                    $dateMinMiseEnligne = strftime('%Y-%m-%d %H:%M:%S', mktime(0, 0, 0, date('m'), date('d') - $criteriaVo->getPeriodicite(), date('y')));
                    $dateMaxMiseEnligne = date('Y-m-d');
                    $stateCondition .= " date_mise_en_ligne_calcule <'".$dateMaxMiseEnligne."' ";
                    $stateCondition .= " AND date_mise_en_ligne_calcule >=  '".$dateMinMiseEnligne."' ";
                    $stateCondition .= " AND date_mise_en_ligne_calcule!='' AND date_mise_en_ligne_calcule IS NOT NULL AND date_mise_en_ligne_calcule != '0000-00-00 00:00:00'";
                    $stateCondition .= ' ))';
                } else {
                    $stateCondition .= " (date_mise_en_ligne_calcule!='' AND date_mise_en_ligne_calcule IS NOT NULL AND date_mise_en_ligne_calcule != '0000-00-00 00:00:00' ";
                    $stateCondition .= ' AND date_mise_en_ligne_calcule<=now())) )';
                }
            }
            if ($criteriaVo->getEtatConsultation() == Atexo_Config::getParameter('STATUS_PREPARATION')
            || $criteriaVo->getSecondConditionOnEtatConsultation() == Atexo_Config::getParameter('STATUS_PREPARATION')) {
                $stateCondition .= ' AND '.$conditionEtatPreparation." AND consultation.id_etat_consultation='0' AND consultation.etat_en_attente_validation ='1' ";
            }
            if ($criteriaVo->getEtatConsultation() === Atexo_Config::getParameter('STATUS_ELABORATION')
            || $criteriaVo->getSecondConditionOnEtatConsultation() == Atexo_Config::getParameter('STATUS_ELABORATION')) {
                $stateCondition .= ' AND '.$conditionEtatPreparation." AND consultation.id_etat_consultation='0' AND consultation.etat_en_attente_validation ='0' ";
            }
            if ($criteriaVo->getEtatConsultation() == Atexo_Config::getParameter('STATUS_A_ARCHIVER')
            || $criteriaVo->getSecondConditionOnEtatConsultation() == Atexo_Config::getParameter('STATUS_A_ARCHIVER')) {
                $stateCondition .= ' AND consultation.id_etat_consultation=:EtatConsultation AND depouillable_phase_consultation=:notSad  ';
                $queryParams[':EtatConsultation'] = Atexo_Config::getParameter('STATUS_A_ARCHIVER');
                $queryParams[':notSad'] = Atexo_Config::getParameter('CONSULTATION_DEPOUIABLE_PHASE_0');
            }
            if ($criteriaVo->getEtatConsultation() == Atexo_Config::getParameter('STATUS_ARCHIVE_REALISEE')
            || $criteriaVo->getSecondConditionOnEtatConsultation() == Atexo_Config::getParameter('STATUS_ARCHIVE_REALISEE')) {
                $stateCondition .= ' AND consultation.id_etat_consultation=:EtatConsultation AND depouillable_phase_consultation=:notSad  ';
                $queryParams[':EtatConsultation'] = Atexo_Config::getParameter('STATUS_ARCHIVE_REALISEE');
                $queryParams[':notSad'] = Atexo_Config::getParameter('CONSULTATION_DEPOUIABLE_PHASE_0');
            }
            if ($criteriaVo->getEtatConsultation() == Atexo_Config::getParameter('STATUS_APRES_DECISION')
            || $criteriaVo->getSecondConditionOnEtatConsultation() == Atexo_Config::getParameter('STATUS_APRES_DECISION')) {
                $stateCondition .= ' AND (consultation.id_etat_consultation>=:EtatConsultation1 AND consultation.id_etat_consultation<=:EtatConsultation2) AND depouillable_phase_consultation=:notSad ';
                $queryParams[':EtatConsultation1'] = Atexo_Config::getParameter('STATUS_DECISION');
                $queryParams[':EtatConsultation2'] = Atexo_Config::getParameter('STATUS_ARCHIVE_REALISEE');
                $queryParams[':notSad'] = Atexo_Config::getParameter('CONSULTATION_DEPOUIABLE_PHASE_0');
            }
            if ($criteriaVo->getEtatConsultation() == Atexo_Config::getParameter('STATUS_AVANT_ARCHIVE')
            || $criteriaVo->getSecondConditionOnEtatConsultation() == Atexo_Config::getParameter('STATUS_AVANT_ARCHIVE')) {
                $stateCondition .= ' AND ( consultation.id_etat_consultation<=:EtatConsultation1 ) ';
                $queryParams[':EtatConsultation1'] = Atexo_Config::getParameter('STATUS_DECISION');
                if ($criteriaVo->getVisionRma()) {
                    $stateCondition .= " AND NOT ( $conditionEtatPreparation  ) ";
                }
            }
            if ($criteriaVo->getEtatConsultation() == Atexo_Config::getParameter('STATUS_JUSTE_DECISION')
            || $criteriaVo->getSecondConditionOnEtatConsultation() == Atexo_Config::getParameter('STATUS_JUSTE_DECISION')) {
                $stateCondition .= ' AND consultation.id_etat_consultation=:EtatConsultation1 AND depouillable_phase_consultation=:notSad ';
                $queryParams[':EtatConsultation1'] = Atexo_Config::getParameter('STATUS_DECISION');
                $queryParams[':notSad'] = Atexo_Config::getParameter('CONSULTATION_DEPOUIABLE_PHASE_0');
            }
        }

        return $stateCondition;
    }

    /*
      * Permet de supprimer le dernier caractère d'une chaine de caractère passée en parametre
      * @Param: chaine de caractère
      */
    public function supprimerDernierCaractere($chaine)
    {
        if (strlen($chaine) > Atexo_Config::getParameter('NOMBRE_MINIMUM_CARACTERE_ASCENDANT_DIRECT_CODE_NUT')) {
            $newChaine = substr($chaine, 0, -1);
        } else {
            $newChaine = $chaine;
        }

        return $newChaine;
    }

    /*
     * Permet de supprimer les caratectes interdites dans une chaine ainsi que les mots interdits
     * @Param chaine de caractère et querParam
     */
    public function searchOrgDenominationLikeMotcle($motCle, &$queryParams)
    {
        $arrayMots = [];
        $queryWhere = ' AND org_denomination like :orgDenomination';
        $arrayMotsCle = Atexo_Util::deleteSpecialCharacteresFromChaine($motCle);
        $arrayMotInterdit = ' and or et ou le la les un une du de des a à au aux son sa ses ne ni non sauf ce ces cet je tu il elle on nous vous ils elles etc mon ma ton ta vos se y en sur ';
        foreach ($arrayMotsCle as $unMotCle) {
            if ($unMotCle && !strstr($arrayMotInterdit, (string) $unMotCle) && strlen($unMotCle) >= 2) {
                $arrayMots[] = $unMotCle;
            }
        }
        if ($arrayMots) {
            $chaineDenominationOrg = implode('%', $arrayMots);
            $queryParams[':orgDenomination'] = '%'.(new Atexo_Db())->quote($chaineDenominationOrg).'%';
        } else {
            $queryWhere = '';
        }

        return $queryWhere;
    }

    /**
     * recherche des consultation via requete sql.
     *
     *
     * @return PDOStatement
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function getConsultationsByQuery(Atexo_Consultation_CriteriaVo $criteriaVo, bool $returnJustNumberOfElement = false, bool $returnReferences = false, $forAlertesEntreprises = false)
    {
        $hasHabilitationInvitePermanent = null;
        $QueryForValidationOnlyAgent = null;
        $QueryForAgentInvitedWithTheirHabilitation = null;
        $QueryForReadOnlyAgent = null;
        $queryParams = [];
        $queryServiceNon = '';
        $queryEtatAttenteValidation = '';
        $queryLeftJoinOrganisme = '';
        $queryJoinCondition = '';
        $queryJoinConditionValidation = '';
        $queryValidationIntermediaire = '';
        $queryValidationFinale = '';
        $queryValidationSimple = '';
        $listOfServicesAllowed = null;
        $choix3 = Atexo_Config::getParameter('REGLE_VALIDATION_TYPE_3');
        $choix4 = Atexo_Config::getParameter('REGLE_VALIDATION_TYPE_4');

        if (false === $criteriaVo->getcalledFromPortail()) {
            // Test des parametres obligatoires
            if ('' === $criteriaVo->getAcronymeOrganisme()
                    || '' === $criteriaVo->getIdService()) {
                throw new Atexo_Consultation_Exception('Les parametres AcronymeOrganisme et IdService sont obligatoires');
            }
            if ($returnJustNumberOfElement || $returnReferences) {
                $querySelect = <<<QUERY

SELECT
   DISTINCT consultation.id
QUERY;
            } else {
                $querySelect = <<<QUERY

SELECT
   DISTINCT consultation.id,
   consultation.datefin,
   reference_utilisateur,
   nom_createur,
   prenom_createur,
   intitule,
   objet,
   id_type_avis,
   alloti
QUERY;
            }

            if (!$criteriaVo->getCalledFromHelios() && '' !== $criteriaVo->getIdService()) {
                //On recupere la liste des entités qui ont le droit de voir la consultation organisation_centralisee

                if (Atexo_Module::isEnabled('OrganisationCentralisee', $criteriaVo->getAcronymeOrganisme())) {
                    $listOfServicesAllowed = null;
                    $hasHabilitationInvitePermanent = false;

                    if (Atexo_CurrentUser::hasHabilitation('InvitePermanentMonEntite')) {
                        $hasHabilitationInvitePermanent = true;
                        $listOfServicesAllowed = [Atexo_CurrentUser::getIdServiceAgentConnected()];
                    }
                    if (Atexo_CurrentUser::hasHabilitation('InvitePermanentEntiteDependante')) {
                        $hasHabilitationInvitePermanent = true;
                        $sousServicesIncluantLeService = Atexo_EntityPurchase::retrieveAllChildrenServices($criteriaVo->getIdService(), $criteriaVo->getAcronymeOrganisme(), true);
                        $sousServices = array_diff($sousServicesIncluantLeService, [Atexo_CurrentUser::getIdServiceAgentConnected()]);
                        if (null === $listOfServicesAllowed) {
                            $listOfServicesAllowed = [];
                        }
                        $listOfServicesAllowed = array_merge($sousServices, $listOfServicesAllowed);
                        if (0 == count($listOfServicesAllowed)) {
                            $hasHabilitationInvitePermanent = false;
                            $listOfServicesAllowed = null;
                        }
                    }
                    if (Atexo_CurrentUser::hasHabilitation('InvitePermanentTransverse')) {
                        $hasHabilitationInvitePermanent = true;
                        if (null === $listOfServicesAllowed) {
                            $listOfServicesAllowed = [];
                        }
                        $listOfServicesAllowed = array_merge((new CommonInvitePermanentTransverseQuery())->getServicesIdByAgentId(Atexo_CurrentUser::getId()), $listOfServicesAllowed);
                        if (0 == count($listOfServicesAllowed)) {
                            $hasHabilitationInvitePermanent = false;
                            $listOfServicesAllowed = null;
                        }
                    }

                    if ($hasHabilitationInvitePermanent) {
                        $listOfServicesAllowed = '('.str_replace(',,', ',', implode(',', $listOfServicesAllowed)).')';
                    } else {
                        $listOfServicesAllowed = '( )';
                    }

                    $queryServiceNon = $hasHabilitationInvitePermanent ? 'consultation.service_id IN '.$listOfServicesAllowed.' ' : '';

                    if (Atexo_CurrentUser::hasHabilitation('ValidationFinale')) {
                        //en mode centralisé : mon service et mes parents sinon en décentralisé mon service
                            $allParent = Atexo_EntityPurchase::getAllParents(Atexo_CurrentUser::getIdServiceAgentConnected(), Atexo_CurrentUser::getOrganismAcronym()); //ou bien $criteriaVo->getIdService();
                            $serviceValidationFinale = Atexo_CurrentUser::getIdServiceAgentConnected().'';
                        if (count($allParent) > 0) {
                            foreach ($allParent as $parent) {
                                $serviceValidationFinale .= ', '.$parent['id'];
                            }
                        }

                        $queryValidationFinale = <<<QUERY
(
	(
		(
			   consultation.id_regle_validation='$choix3'
		)
		AND (
				service_validation_intermediaire IS NULL
				AND service_validation IN  ('$serviceValidationFinale')
				AND (
						datevalidation IS NULL
						OR datevalidation = '0000-00-00 00:00:00'
					)
			)
	)
	OR
	(
		(
			   consultation.id_regle_validation='$choix4'
		)
		AND (
				service_validation_intermediaire IS NOT NULL
				AND service_validation IN  ('$serviceValidationFinale')
				AND (
					datevalidation IS NULL
					OR datevalidation = '0000-00-00 00:00:00'
					)
				AND date_validation_intermediaire IS NOT NULL
				AND date_validation_intermediaire != '0000-00-00 00:00:00'
				AND etat_approbation='1'
			)
	)
)
QUERY;
                    }
                } else {
                    if ('' !== $criteriaVo->getIdService()) {
                        $listOfServicesAllowed = $criteriaVo->getIdService();
                        $idService = Atexo_CurrentUser::getUserSf()->getServiceId();
                        if (Atexo_CurrentUser::hasHabilitation('InvitePermanentMonEntite')) {
                            if (is_null($idService)) {
                                $queryServiceNon = " consultation.service_id is null and consultation.organisme = '"
                                    . Atexo_CurrentUser::getUserSf()->getOrganisme()->getAcronyme() . "' ";
                            } else {
                                $queryServiceNon = " consultation.service_id = '" . $idService . "' ";
                            }
                        }

                        if (Atexo_CurrentUser::hasHabilitation('ValidationFinale')) {
                            $queryValidationFinale = <<<QUERY
(
	(
		(
			   consultation.id_regle_validation='$choix3'
		)
		AND (
				service_validation_intermediaire IS NULL
				AND service_validation='$idService'
				AND (
						datevalidation IS NULL
						OR datevalidation = '0000-00-00 00:00:00'
					)
			)
	)
	OR
	(
		(
			   consultation.id_regle_validation='$choix4'
		)
		AND (
				service_validation_intermediaire IS NOT NULL
				AND service_validation = '$idService'
				AND (
					datevalidation IS NULL
					OR datevalidation = '0000-00-00 00:00:00'
					)
				AND date_validation_intermediaire IS NOT NULL
				AND date_validation_intermediaire != '0000-00-00 00:00:00'
				AND etat_approbation='1'
			)
	)
)
QUERY;
                        }

                        $queryEtatAttenteValidation = "AND consultation.etat_en_attente_validation ='1'";
                    }
                }
                if (null !== $listOfServicesAllowed) {
                    if ($criteriaVo->getNoTemporaryCOnsultation()) {
                        $queryValidationIntermediaire = '';
                        $queryValidationFinale = '';
                        $queryValidationSimple = '';
                    } else {
                        //Si et SSi une des deux options actives : cons a approuver ou cons a valider
                        if ($criteriaVo->getConsultationAValiderSeulement() || $criteriaVo->getConsultationAApprouverSeulement()) {
                            //Si les consultations a retourner sont a valider ou a approuver seulement, la requete sur la liste des
                            //services autorise par defaut supprimee.
                            $queryServiceNon = '';
                            if (!$criteriaVo->getConsultationAApprouverSeulement()) {
                                //Si l'option 'consulation a approuver' n'est pas a true => ne pas remonter les cons a approuver
                                $queryValidationIntermediaire = '';
                            }
                            if (!$criteriaVo->getConsultationAValiderSeulement()) {
                                //Si l'option 'consulation a valider' n'est pas a true => ne pas remonter les cons a valider
                                $queryValidationSimple = '';
                                $queryValidationFinale = '';
                            }
                        }
                    }
                    $queryJoinCondition = ($queryServiceNon) ? ' AND ( '.$queryServiceNon.')' : ' AND 0 ';

                    $queryJoinConditionValidation = '';
                    if ($hasHabilitationInvitePermanent || Atexo_CurrentUser::hasHabilitation('ValidationFinale')) {
                        $queryValidationFinale = $queryValidationFinale ? ' OR '.$queryValidationFinale : '';
                        $queryValidationIntermediaire = $queryValidationIntermediaire ? ' OR '.$queryValidationIntermediaire : '';
                        $queryValidationSimple = $queryValidationSimple ? ' OR '.$queryValidationSimple : '';
                    } else {
                        $queryValidationFinale = '';
                        $queryValidationIntermediaire = '';
                        $queryValidationSimple = '';
                    }
                    $queryJoinConditionValidation .= <<<QUERY

AND ( 0 $queryValidationFinale $queryValidationIntermediaire $queryValidationSimple )
$queryEtatAttenteValidation
QUERY;
                }
            }
        } else {
            if ($returnJustNumberOfElement || $returnReferences) {
                $querySelect = <<<QUERY

SELECT
	DISTINCT CONCAT(consultation.id,	consultation.organisme)  as id ,
	consultation.code_procedure as code_procedure
QUERY;
            } else {
                $querySelect = <<<QUERY

SELECT
	DISTINCT CONCAT(consultation.id, consultation.organisme),
	consultation.id,
	consultation.organisme,
	consultation.datefin,
	consultation.reference_utilisateur,
	consultation.intitule,
	consultation.objet,
	datemiseenligne ,
	datevalidation,
	regle_mise_en_ligne ,
	consultation.categorie,
	date_format(consultation.dateDebut,'%d/%m/%Y') as datedebut,
	consultation.code_procedure as code_procedure

QUERY;
            }
            if (!$criteriaVo->getOrganismeTest()) {
                $queryLeftJoinOrganisme = ' INNER JOIN Organisme on Organisme.acronyme =consultation.organisme ';
            }
        }

        $queryLeftJoinAlerte = <<<QUERY
LEFT JOIN
	alerte_metier ON (alerte_metier.reference_cons = consultation.id AND alerte_metier.organisme = consultation.organisme )
QUERY;
        $queryLeftJoinAlerte = (0 != $criteriaVo->getIdAlerteConsultation() && 0 != $criteriaVo->getAlerteConsultationCloturee()) ? $queryLeftJoinAlerte : '';
        $queryLeftJoinCategorieLot = <<<QUERY
LEFT JOIN
		CategorieLot ON (CategorieLot.consultation_id = consultation.id AND CategorieLot.organisme = consultation.organisme)
QUERY;
        $queryLeftJoinCategorieLot = ($criteriaVo->getKeyWordAdvancedSearch() || $criteriaVo->getKeyWordRechercheRapide() || $criteriaVo->getIdCodeCpv2()) ? $queryLeftJoinCategorieLot : '';
        $queryFrom = <<<QUERY
FROM consultation
		$queryLeftJoinCategorieLot
		$queryLeftJoinAlerte
		$queryLeftJoinOrganisme
QUERY;

        //on parcourt l'objet CriteriaVo passe en parametre pour ajouter a la clause where
        $otherConditions = Atexo_Recherche_Search::otherConditions($criteriaVo, $queryParams);
        $stateCondition = Atexo_Recherche_Search::stateConditions($criteriaVo, $queryParams);
        $otherConditions .= $stateCondition;
        $idAgentConnecte = $criteriaVo->getConnectedAgentId();

        $conditionFromAcheteur = ((false == $criteriaVo->getcalledFromPortail()) ? ' consultation.organisme =:organismeAgent AND consultation.consultation_achat_publique=:achatPublique ' : '1 ');
        $querySelectAvecHabilitation = (!$returnJustNumberOfElement && !$returnReferences) ? ', 1 as invitedWithHisHabilitation, 0 as invitedReadOnly, 0 as invitedValidationOnly' : '';
        $queryForPole = <<<QUERY
$querySelect $querySelectAvecHabilitation
$queryFrom
WHERE
	$conditionFromAcheteur
	$queryJoinCondition
	$otherConditions

QUERY;
        //requete 2 : la liste des consultations auxquelles il est invite avec ses habilitations
        if (!$criteriaVo->getConsultationAValiderSeulement() && !$criteriaVo->getConsultationAApprouverSeulement()) {
            $QueryForAgentInvitedWithTheirHabilitation = <<<QUERY
$querySelect  $querySelectAvecHabilitation
$queryFrom , InterneConsultation
WHERE
	 $conditionFromAcheteur
	 AND InterneConsultation.interne_id = '$idAgentConnecte'
	 AND InterneConsultation.consultation_id = consultation.id
	 AND InterneConsultation.organisme = consultation.organisme
	 $otherConditions

QUERY;

            $querySelectInvitedReadOnly = (!$returnJustNumberOfElement && !$returnReferences) ? ', 0 as invitedWithHisHabilitation, 1 as invitedReadOnly, 0 as invitedValidationOnly' : '';
            //requete 3 : la liste des consultations auxquelles l'agent est invite en lecture seule
            $QueryForReadOnlyAgent = <<<QUERY
$querySelect $querySelectInvitedReadOnly
$queryFrom , InterneConsultationSuiviSeul
WHERE
	$conditionFromAcheteur
	AND InterneConsultationSuiviSeul.interne_id = '$idAgentConnecte'
	AND InterneConsultationSuiviSeul.consultation_id = consultation.id
	AND InterneConsultationSuiviSeul.organisme = consultation.organisme
	$otherConditions

QUERY;
            $querySelectInvitedValidationOnly = (!$returnJustNumberOfElement && !$returnReferences) ? ' , 0 as invitedWithHisHabilitation, 0 as invitedReadOnly, 1 as invitedValidationOnly' : '';
            $QueryForValidationOnlyAgent = <<<QUERY
$querySelect $querySelectInvitedValidationOnly
$queryFrom
WHERE
	$conditionFromAcheteur
	$queryJoinConditionValidation
	$otherConditions

QUERY;
        }

        if (false === $criteriaVo->getcalledFromPortail()) {
            $queryParams[':achatPublique'] = '0';
            $queryParams[':organismeAgent'] = $criteriaVo->getAcronymeOrganisme();
        }

        if (false === $criteriaVo->getcalledFromPortail()) {
            //Fusionner les 3 requetes
            $finalQueryPart1 = $queryForPole.($QueryForValidationOnlyAgent ? ' UNION '.$QueryForValidationOnlyAgent : '');

            $finalQueryPart2 = ($QueryForAgentInvitedWithTheirHabilitation ? $QueryForAgentInvitedWithTheirHabilitation.' UNION ' : '').$QueryForReadOnlyAgent;
            $finalQuery = ($finalQueryPart1) ? $finalQueryPart1.(($finalQueryPart2) ? ' UNION '.$finalQueryPart2 : ' ') : $finalQueryPart2;

            if (!$finalQuery) {
                if ($returnJustNumberOfElement) {
                    return 0;
                } else {
                    return [];
                }
            }
            if (!$returnJustNumberOfElement && !$returnReferences) {
                if ($criteriaVo->getSortByElement()) {
                    $finalQuery .= ' ORDER BY '.$criteriaVo->getSortByElement().' '.$criteriaVo->getSensOrderBy().' '.(('datefin' != strtolower($criteriaVo->getSortByElement())) ? ' , datefin DESC ' : ' ');
                } else {
                    $finalQuery .= ' ORDER BY datefin DESC ';
                }
            }
        } else {
            $finalQuery = $queryForPole;
            if (!$returnJustNumberOfElement && !$returnReferences) {
                if ($criteriaVo->getSortByElement()) {
                    $finalQuery .= ' ORDER BY '.$criteriaVo->getSortByElement().' '.$criteriaVo->getSensOrderBy().(('datefin' != strtolower($criteriaVo->getSortByElement())) ? ' , datefin DESC ' : ' ');
                } else {
                    $finalQuery .= ' ORDER BY datefin DESC ';
                }

                if ($criteriaVo->getLimit()) {
                    $finalQuery .= ' LIMIT '.$criteriaVo->getOffset().','.$criteriaVo->getLimit();
                }
            }
        }
        $finalQuery = preg_replace("#\([ ]+OR[ ]+\(#", '( (', $finalQuery);
        if ($forAlertesEntreprises) {
            $finalQuery = str_replace('consultation.', 'consultation_alertes.', $finalQuery);
            $finalQuery = str_replace('FROM consultation', 'FROM consultation_alertes', $finalQuery);
        }

        $statement = Atexo_Db::getLinkCommon(true)->prepare($finalQuery);

        $statement->execute($queryParams);

        return $statement;
    }
}
