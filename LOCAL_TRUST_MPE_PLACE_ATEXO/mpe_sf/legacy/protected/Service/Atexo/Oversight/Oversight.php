<?php

namespace Application\Service\Atexo\Oversight;

use Application\Library\Propel\Exception\PropelException;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_TiersAuthentification;
use Application\Service\Atexo\Config\Atexo_Config_Exception;
use Application\Service\Atexo\TokenManager\Atexo_TokenManager_CurlWrapper;
use Exception;

/**
 * Class oversight.
 *
 * @author florian MARTIN <florian.martin@atexo.com>
 * @copyright Atexo 2018
 *
 * @version 1.0
 *
 * @since MPE-
 */
class Atexo_Oversight_Oversight
{
    public static int $codeSuccess = 0;
    public static int $codeError = 1;
    public static int $codeErrorMisuseShell = 2;
    public static int $codeWarning = 3;
    public static int $codeErrorCommandInvokedCannotExecute = 126;
    public static int $codeErrorcommandNotFound = 127;
    public static int $codeErrorInvalidArgumentToExit = 128;
    public static int $codeErrorRemoteServiceDoesNotRespond = 20;
    public static int $codeErrorRemoteServiceReturnsAnError = 21;
    public static int $codeErrorCanNotWriteToSystems = 10;
    public static int $codeErrorSomeFilesOrFoldersAreNotFound = 11;
    public static int $codeErrorApplicationHasEncounteredProblem = 30;

    private static $token;

    /**
     * @param array $params
     *
     *
     * @throws Atexo_Config_Exception
     * @throws PropelException
     */
    public static function oversight($params = []): bool|string
    {
        $token = self::getToken();

        if (!$token) {
            return false;
        }

        $params['debutExecution'] = '';
        $params['propel'] = '';

        if (array_key_exists('ticket', $params)) {
            $params['interface'] = (new Atexo_TiersAuthentification())->getLoginByIdSsoTiers($params['ticket']);
        } else {
            $params['interface'] = $params['service'];
        }

        if (isset($params['nomBatch'])) {
            $params['webserviceBatch'] = $params['nomBatch'];
        }

        $params['statut'] = 'INIT';
        if (isset($params['code'])) {
            $params['statut'] = 'NOK';
            if ($params['code'] === self::$codeSuccess) {
                $params['statut'] = 'OK';
            }
        }

        try {
            return self::request($params, $token);
        } catch (Exception $exceptionCLI) {
            $oversightError = "Une erreur est survenue lors de l'historisation de la tâche.".PHP_EOL.'Erreur : '.$exceptionCLI->getMessage().PHP_EOL.'Trace : '.$exceptionCLI->getTraceAsString().PHP_EOL;
            $logger = Atexo_LoggerManager::getLogger('supervision');
            $logger->error($oversightError);
        }
    }

    public static function request($params, $token)
    {
        $url = Atexo_Config::getParameter('PF_URL_REFERENCE').'app.php/api/v1/statistiques/interfaces.json?ticket=' . $token;
        $curlWrapper = new Atexo_TokenManager_CurlWrapper();
        $curlWrapper->dontVerifySSLCACert();
        $curlWrapper->setPostDataUrlEncode($params);
        $result = $curlWrapper->get($url);
        if (403 === $curlWrapper->getHTTPCode()) {
            $oversightError = "Erreur d'authorisation avec le token : ".$token;
            $logger = Atexo_LoggerManager::getLogger('supervision');
            $logger->error($oversightError);
        }

        return $result;
    }

    /**
     *
     * @throws Atexo_Config_Exception
     * @throws PropelException
     */
    private static function getToken(): bool|string
    {
        $login = Atexo_Config::getParameter('MPE_WS_LOGIN_STATISTIQUES');
        $password = Atexo_Config::getParameter('MPE_WS_PASSWORD_STATISTIQUES');

        if (empty($login) || empty($password)) {
            return false;
        }

        if (empty(self::$token)) {
            self::$token = Atexo_TiersAuthentification::getTicket($login, $password);
        }

        return self::$token;
    }
}
