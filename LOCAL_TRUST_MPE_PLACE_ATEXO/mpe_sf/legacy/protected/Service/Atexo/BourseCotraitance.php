<?php

namespace Application\Service\Atexo;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonTBourseCotraitance;
use Application\Propel\Mpe\CommonTBourseCotraitanceQuery;
use Prado\Prado;

/**
 * Created by PhpStorm.
 * User: AsoAtexo
 * Date: 25/08/2015
 * Time: 18:05.
 */
class Atexo_BourseCotraitance
{
    /**
     * Permet de retourner un tableau contenant le cas du groupement et le nombre d'entreprises inscrites.
     *
     * @return array('cas'=>cas du groupement, 'count'=>nbre d'entreprise du groupement)
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public static function getCasGroupement($refCons, $idEntreprise = false)
    {
        return self::getCachedCasGroupement($refCons, $idEntreprise);
    }

    /**
     * Permet de gerer la visibilité des actions possible pour la bourse à la cotraitance.
     *
     * @return string
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public static function gererActionBourseCotraitance($refConsultation)
    {
        $groupement = self::getCasGroupement($refConsultation);
        switch ($groupement['cas']) {
            case 'cas1':
                return 'nouveau';
            case 'cas2':
                return 'inscrit';
            case 'cas3':
                return 'nouveau';
            case 'cas4':
                return 'inscrit';
        }
    }

    private static function getCachedCasGroupement($refCons, $idEntreprise = false)
    {
        if (!$idEntreprise) {
            $idEntreprise = Atexo_CurrentUser::getIdEntreprise();
        }

        $cachedCasGroupementForConsultation = Prado::getApplication()->Cache->get('cachedCasGroupementForConsultation_'.$refCons.'_'.$idEntreprise);

        if (!$cachedCasGroupementForConsultation) {
            $cachedCasGroupementForConsultation = self::initCachedCasGroupemen($refCons, $idEntreprise);
        }

        return $cachedCasGroupementForConsultation;
    }

    public static function initCachedCasGroupemen($refCons, $idEntreprise, $connexion = false)
    {
        $cachedCasGroupementForConsultation = [];
        if (!$connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
        $ArrayBourse = CommonTBourseCotraitanceQuery::create()->filterByReferenceConsultation($refCons)->find($connexion);
        if (!(is_countable($ArrayBourse) ? count($ArrayBourse) : 0)) {
            //aucun candidat inscrit
            $cachedCasGroupementForConsultation['cas'] = 'cas1';
        } else {
            //cas 3 = il y a des candidats inscrits et c'est le cas par défaut
            $cachedCasGroupementForConsultation['cas'] = 'cas3';
            $cachedCasGroupementForConsultation['count'] = is_countable($ArrayBourse) ? count($ArrayBourse) : 0;
            //si l'utilisateur est connecté on cherche à lui afficher s'il est inscrit seul (cas 2) ou s'il fait parti des inscrits (cas 4)
            if ($idEntreprise) {
                $monInscriptionBourse = CommonTBourseCotraitanceQuery::create()->filterByReferenceConsultation($refCons)->filterByIdEntreprise($idEntreprise)->findOne($connexion);
                if ($monInscriptionBourse instanceof CommonTBourseCotraitance) {
                    if (1 == (is_countable($ArrayBourse) ? count($ArrayBourse) : 0)) {
                        $cachedCasGroupementForConsultation['cas'] = 'cas2';
                    } else {
                        $cachedCasGroupementForConsultation['cas'] = 'cas4';
                        $cachedCasGroupementForConsultation['count'] = is_countable($ArrayBourse) ? count($ArrayBourse) : 0;
                    }
                }
            }
        }

        Prado::getApplication()->Cache->set('cachedCasGroupementForConsultation_'.$refCons.'_'.$idEntreprise, $cachedCasGroupementForConsultation, Atexo_Config::getParameter('TTL_PRADO_CACHE_COURT'));

        return $cachedCasGroupementForConsultation;
    }
}
