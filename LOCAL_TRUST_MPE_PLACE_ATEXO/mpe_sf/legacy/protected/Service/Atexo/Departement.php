<?php

namespace Application\Service\Atexo;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonDepartementPeer;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Departement
{
    /** Fonction  qui retourne a listes de tous les départements.
     * @param : $returnPropelObject à true (par défaut) pour retourner un tableau d'objet propel,
     *                              à false pour retourner la liste des departement sous le formes array [id departement] = libelle
     *
     * @return : retourne un tableau vide si aucun département n'est trouvé, sinon elle retroune au tableau des département au format
     *           défini dans la paramètre
     */
    public function getListDepartements($returnPropelObject = true)
    {
        $connexionBaseCommune = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $departement = CommonDepartementPeer::doSelect($c, $connexionBaseCommune);
        if ($departement) {
            if ($returnPropelObject) {
                return $departement;
            } else {
                $arrayDepartement = [];
                foreach ($departement as $unDep) {
                    $arrayDepartement[$unDep->getIdDepartement()] = $unDep->getNomDepartement();
                }

                return $arrayDepartement;
            }
        } else {
            return [];
        }
    }
}
