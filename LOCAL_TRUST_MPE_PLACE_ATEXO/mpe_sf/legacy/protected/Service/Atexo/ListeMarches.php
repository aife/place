<?php

namespace Application\Service\Atexo;

use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonFichiersListeMarchesPeer;
use Application\Propel\Mpe\CommonMarchePublie;

/*
 * Created on 23 nov. 2009
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */

class Atexo_ListeMarches
{
    public const SERVICE_ORGANISME_CENTRALISEE = null;

    /**
     * retourne l liste des marchés d'organisme.
     */
    public function retreiveListeMarches($organisme)
    {
        if (!$organisme) {
            return [];
        }
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonFichiersListeMarchesPeer::ORGANISME, $organisme);
        $result = CommonFichiersListeMarchesPeer::doSelect($c, $connexionCom);
        if ($result) {
            return $result;
        } else {
            return [];
        }
    }

    /**
     * suppression d'un marché.
     */
    public function doDeleteMarche($identifiant, $organisme)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        CommonFichiersListeMarchesPeer::doDelete([$identifiant, $organisme], $connexionCom);
    }

    /**
     * @param mixed $connexion
     *
     * @throws PropelException
     */
    public function setMarchePublieOrganismeCentralisee(
        string $acronymeOrganisme,
        string $year,
        $connexion,
        bool $isChecked
    ): void {
        $marchePublie = (new Atexo_Marche())->getMarchePublieByYearAndOrganisme($acronymeOrganisme, self::SERVICE_ORGANISME_CENTRALISEE, $year, $connexion);

        if (!($marchePublie instanceof CommonMarchePublie)) {
            $marchePublie = new CommonMarchePublie();
            $marchePublie->setOrganisme($acronymeOrganisme);
            $marchePublie->setServiceId(self::SERVICE_ORGANISME_CENTRALISEE);
            $marchePublie->setNumeromarcheannee($year);
        }
        $marchePublie->setIspubliee((int) $isChecked);

        $marchePublie->save($connexion);
    }

    /**
     * @param mixed $connexion
     *
     * @throws PropelException
     */
    public function setDataSourceOrganismeCentralisee(
        string $acronymeOrganisme,
        string $year,
        string $sourceDonnees,
        string $isChecked,
        $connexion
    ): void {
        $marchePublie = (new Atexo_Marche())->getMarchePublieByYearAndOrganisme($acronymeOrganisme, self::SERVICE_ORGANISME_CENTRALISEE, $year, $connexion);

        if (!($marchePublie instanceof CommonMarchePublie)) {
            $marchePublie = new CommonMarchePublie();
            $marchePublie->setOrganisme($acronymeOrganisme);
            $marchePublie->setServiceId(self::SERVICE_ORGANISME_CENTRALISEE);
            $marchePublie->setNumeromarcheannee($year);
        }
        $marchePublie->setDatasource($sourceDonnees);
        $marchePublie->setIspubliee((int) $isChecked);

        $marchePublie->save($connexion);
    }
}
