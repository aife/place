<?php

namespace Application\Service\Atexo;

use Application\Service\Atexo\Interfaces\Atexo_Interfaces_Consultation;
use Application\Service\Atexo\Interfaces\Atexo_Interfaces_Entreprise;
use Application\Service\Atexo\Interfaces\Atexo_Interfaces_GenerateXml;
use Application\Service\Atexo\Interfaces\Atexo_Interfaces_InterfaceAuthentification;
use Application\Service\Atexo\Interfaces\Atexo_Interfaces_InterfacesUtilities;
use Application\Service\Atexo\Interfaces\Atexo_Interfaces_Offre;
use Application\Service\Atexo\Interfaces\Atexo_Interfaces_Question;
use Application\Service\Atexo\Interfaces\Atexo_Interfaces_Telechargement;
use AtexoCrypto\Dto\Fichier;
use Prado\Web\UI\TPage;

/**
 * Gestion de la configuration.
 *
 * Cette classe permet d'acceder aux directives de configuration
 * n'importe ou dans le code ainsi que de gerer la generation du
 * fichier de configuration application.xml en tenant compte de
 * la configuration specifique de l'application en cours.
 *
 * @author Mounir LAMANE <mounir.lamane@atexo.com>
 * @copyright Atexo 2009
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Connecteur extends TPage
{
    private $_typeInterface;

    public function getTypeInterface()
    {
        return $this->_typeInterface;
    }

    public function setTypeInterface($value)
    {
        $this->_typeInterface = $value;
    }

    public function getXmlResponse($xmlInput, $afgOrg)
    {
        $xmlRegistres = null;
        $interfaceConsultation = new Atexo_Interfaces_Consultation();
        $xmlGenerator = new Atexo_Interfaces_GenerateXml('UTF-8');
        $xmlError = '';
        $interfaceConsultation->initInterface();
        $xml = base64_decode($xmlInput);
        $action = $interfaceConsultation->getMessageActionFromXml($xml);
        if (isset($xmlInput)) {
            Atexo_CurrentUser::writeToSession('xml', htmlentities($xmlInput));
            $autentificationSucceed = false;
            $userVo = $interfaceConsultation->getUserVoFromXml($xml);
            if ($userVo && $userVo instanceof Atexo_UserVo) {
                if ($this->getApplication()->getModule('auth')->login($userVo, null)) {
                    $autentificationSucceed = true;
                }
            }
            if ($autentificationSucceed) {
                switch ($action) {
                    case 'CREER_CONSULTATION':
                        $this->response->redirect('index.php?page=Agent.FormulaireConsultation');
                        break;
                    case 'MODIFIER_CONSULTATION':
                        $refConsultation = $interfaceConsultation->getIdConsultationByRefCon($xml);
                        if ($refConsultation) {
                            $this->response->redirect('index.php?page=Agent.FormulaireConsultation&id='.base64_encode($refConsultation));
                        } else {
                            $xmlError = $xmlGenerator->createXmlErreurConnecteur('5', "L'utilisateur n'a pas les droits necessaire pour acceder a cette consultation");
                        }
                        break;
                    case 'EXPORT_REGISTRES_OFFRES':
                        $refConsultation = $interfaceConsultation->getIdConsultationByRefCon($xml);
                        if ($refConsultation) {
                            $consultationsVo = (new Atexo_Interfaces_Offre())->getOffresVoFromXmlConsultation($xml, Atexo_CurrentUser::getCurrentOrganism());
                            $interfaceAuth = (new Atexo_Interfaces_InterfaceAuthentification())->getAthentificationVoFromXml($xml);
                            $interfaceAuth->setAfgOrg($afgOrg);
                            $xmlOffres = $xmlGenerator->generateInterfaceXml($interfaceAuth, $consultationsVo, Atexo_Config::getParameter('GENERATE_XML_OFFRE'));
                            (new Atexo_Interfaces_InterfacesUtilities())->saveLog($xmlOffres, $action, $this->getTypeInterface(), 'interface_registre_depots');
                            $consultationsVo = (new Atexo_Interfaces_Consultation())->getConsultationsVoFromXmlRegistreDepot($xmlOffres);
                            if ($this->getTypeInterface() == Atexo_Config::getParameter('CONNECTEUR_MARCO')) {
                                $xmlRegistres = $xmlGenerator->generateXmlRegistreDepotMarco($interfaceAuth, $consultationsVo);
                            } else {
                                $xmlRegistres = $xmlGenerator->generateXmlRegistreDepotSis($interfaceAuth, $consultationsVo);
                            }
                            $xmlError = $xmlGenerator->createXmlErreurConnecteur('0', '');
                            (new Atexo_Interfaces_InterfacesUtilities())->saveLog($xmlRegistres, $action, $this->getTypeInterface(), 'registre_depots');
                        } else {
                            $xmlError = $xmlGenerator->createXmlErreurConnecteur('5', "L'utilisateur n'a pas les droits necessaire pour acceder a cette consultation");
                            (new Atexo_Interfaces_InterfacesUtilities())->saveLog($xmlError, $action, $this->getTypeInterface(), 'registre_depots_erreur');
                        }
                        break;
                    case 'EXPORT_REGISTRES_TELECHARGEMENTS':
                        $refConsultation = $interfaceConsultation->getIdConsultationByRefCon($xml);
                        if ($refConsultation) {
                            $interfaceAuthVo = (new Atexo_Interfaces_InterfaceAuthentification())->getAthentificationVoFromXml($xml);
                            $interfaceAuthVo->setAfgOrg($afgOrg);
                            $consultationsVo = (new Atexo_Interfaces_Telechargement())->getInfoTelechargement($xml);
                            $xmlTelechargement = $xmlGenerator->generateInterfaceXml($interfaceAuthVo, $consultationsVo, 'TELECHARGEMENT');
                            (new Atexo_Interfaces_InterfacesUtilities())->saveLog($xmlTelechargement, $action, $this->getTypeInterface(), 'interface_registre_retraits');
                            if ($this->getTypeInterface() == Atexo_Config::getParameter('CONNECTEUR_MARCO')) {
                                $xmlRegistres = $xmlGenerator->generateXmlRegistreRetraitMarco($interfaceAuthVo, $consultationsVo);
                            } else {
                                $xmlRegistres = $xmlGenerator->generateXmlRegistreRetraitSis($interfaceAuthVo, $consultationsVo);
                            }
                            $xmlError = $xmlGenerator->createXmlErreurConnecteur('0', '');
                            (new Atexo_Interfaces_InterfacesUtilities())->saveLog($xmlRegistres, $action, $this->getTypeInterface(), 'registre_retraits');
                        } else {
                            $xmlError = $xmlGenerator->createXmlErreurConnecteur('5', "L'utilisateur n'a pas les droits necessaire pour acceder a cette consultation");
                            (new Atexo_Interfaces_InterfacesUtilities())->saveLog($xmlError, $action, $this->getTypeInterface(), 'registre_retraits_erreur');
                        }
                        break;
                    case 'EXPORT_REGISTRES_ENTREPRISES':
                        $interfaceAuthVo = (new Atexo_Interfaces_InterfaceAuthentification())->getAthentificationVoFromXml($xml);
                                                          $consultation = (new Atexo_Interfaces_Entreprise())->getInfoEntreprise($xml);
                                                          $xmlString = '';
                                                          $xmlString = $xmlGenerator->generateInterfaceXml($interfaceAuthVo, $consultation, 'ENTREPRISE');
                                                          $refConsultation = $interfaceConsultation->getIdConsultationByRefCon($xml);
                                                          //$xmlRegistres = $xmlGenerator->generateXmlExportEntrepriseSISOld($xmlString,$refConsultation);
                                                          $xmlRegistres = $xmlGenerator->generateXmlExportEntrepriseSIS($interfaceAuthVo, $consultation);
                                                          $xmlError = $xmlGenerator->createXmlErreurConnecteur('0', '');
                        break;
                    case 'EXPORT_REGISTRES_QUESTIONS':
                        $refConsultation = $interfaceConsultation->getIdConsultationByRefCon($xml);
                        if ($refConsultation) {
                            $interfaceAuthVo = (new Atexo_Interfaces_InterfaceAuthentification())->getAthentificationVoFromXml($xml);
                            $consultation = (new Atexo_Interfaces_Question())->getInfoQuestion($xml);
                            $xmlRegistres = $xmlGenerator->generateInterfaceXml($interfaceAuthVo, $consultation, 'QUESTION');
                        } else {
                            $xmlError = $xmlGenerator->createXmlErreurConnecteur('5', "L'utilisateur n'a pas les droits necessaire pour acceder a cette consultation");
                        }
                        break;
                    default:
                        $xmlError = $xmlGenerator->createXmlErreurConnecteur('5', "L'utilisateur n'a pas les droits necessaire pour acceder a cette consultation");
                }
            } else {
                $xmlError = $xmlGenerator->createXmlErreurConnecteur('3', 'Identifiant ou mot de passe incorrect');
            }
        } else {
            $xmlError = $xmlGenerator->createXmlErreurConnecteur('1', 'Fichier XML introuvable');
        }
        //echo  base64_encode($xmlRegistres)."<br>". base64_encode($xmlError); exit;
        if ($xmlRegistres || $xmlError) {
            //$this->js->Text="<script>document.main_form.from.value='".$_REQUEST['from']."';document.main_form.xmlErr.value='".base64_encode(htmlentities($xmlError))."';document.main_form.xmlReg.value='".base64_encode(htmlentities($xmlRegistres))."';document.main_form.action='index.php?page=Agent.EnvoiConnecteur';document.main_form.send.click();</script>";
            echo '<html> '
             .'<body> '
             .'	<form name="the_form"> '
             .'		<input type="hidden" name="xml_registre" value="'.base64_encode($xmlRegistres).'"> '
             .'		<input type="hidden" name="xml_error" value="'.base64_encode($xmlError).'">'
             .'	</form> '
             .'</body>'
             .'</html> ';
            exit;
        }
    }
}
