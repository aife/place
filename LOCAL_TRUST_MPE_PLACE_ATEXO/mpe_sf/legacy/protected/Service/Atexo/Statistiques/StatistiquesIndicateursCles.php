<?php

namespace Application\Service\Atexo\Statistiques;

use Application\Propel\Mpe\CommonAgent;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Util;

/**
 * @author Mohamed Wazni <mohamed.wazni@atexo.com>
 * @copyright Atexo 2010
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Statistiques_StatistiquesIndicateursCles
{
    /**
     * Retourne sous format d'un tableau la liste des indicateurs clés calculés pour affichage du côté agent
     * Cette fonction prend en paramètre l'émail de l'agent pour calculer les indicateurs.
     *
     * @param : $agentEmail email de l'agent pour lequel on calcule les indicateurs
     *
     * @return : tableau de valeur dans l'ordre suivant
     *           array($nombreAnnoncesTotal,$nombreAnnoncesConsultation,$nombreAnnoncesInformation,$nombreAnnoncesAttribution,$nombreAnnoncesAApprouver,$nombreAnnoncesAValider,$nombreAnnoncesEnAttenteValidation,$nombreCommissions,$nombreQuestions,$nombreAnnoncesConcerneeQ,$nombreReponsesADepouiller,$nombreAnnoncesConcerneesR,$nombreDeMois)
     */
    public function getStatistiquesIndicateursClesAgents($agentEmail)
    {
        $temps_debut = microtime(true);
        $agent = (new Atexo_Agent())->retrieveAgentByMail($agentEmail);
        if ($agent instanceof CommonAgent) {
            $criteria = new Atexo_Statistiques_CriteriaVo();
            $criteria->setOrganisme($agent->getOrganisme());
            $criteria->setConsultationEnLigne(true);
            $criteria->setDateRemisePlisStart(date('Y-m-d'));

            if (Atexo_Module::isEnabled('OrganisationCentralisee', $agent->getOrganisme())) {
                $services = Atexo_EntityPurchase::retrieveAllChildrenServices($agent->getServiceId(), $agent->getOrganisme());
                if ($services) {
                    $service = ' IN '.$services.' ';
                } else {
                    $service = ' IN ('.$agent->getServiceId().') ';
                }
            } else {
                $service = ' IN ('.$agent->getServiceId().') ';
            }
            $criteria->setIdService($service);

            //nombre total d'avis en ligne
            $resultat = (new Atexo_Statistiques_RequetesStatistiques())->getNbreProcedures($criteria, false, true);
            $nombreAnnoncesTotal = $resultat['total'] + $resultat['totalMapa'] + $resultat['accordcadre'] + $resultat['sad'];

            //nombre de consultation en ligne
            $criteria->setIdTypeAvis('3');
            $resultat = (new Atexo_Statistiques_RequetesStatistiques())->getNbreProcedures($criteria, false, true);
            $nombreAnnoncesConsultation = $resultat['total'] + $resultat['totalMapa'] + $resultat['accordcadre'] + $resultat['sad'];

            //nombre d'avis d'information en ligne
            $criteria->setIdTypeAvis('2');
            $resultat = (new Atexo_Statistiques_RequetesStatistiques())->getNbreProcedures($criteria, false, true);
            $nombreAnnoncesInformation = $resultat['total'] + $resultat['totalMapa'] + $resultat['accordcadre'] + $resultat['sad'];

            //nombre d'avis d'attribution en lign
            $criteria->setIdTypeAvis('4');
            $resultat = (new Atexo_Statistiques_RequetesStatistiques())->getNbreProcedures($criteria, false, true);
            $nombreAnnoncesAttribution = $resultat['total'] + $resultat['totalMapa'] + $resultat['accordcadre'] + $resultat['sad'];

            $nombreAnnoncesAApprouver = (new Atexo_Statistiques_RequetesStatistiques())->getNombreConsultationAAprouver($agent->getId(), $agent->getServiceId(), $agent->getOrganisme());
            $nombreAnnoncesAValider = (new Atexo_Statistiques_RequetesStatistiques())->getNombreConsultationAValider($agent->getId(), $agent->getServiceId(), $agent->getOrganisme(), false);
            $nombreAnnoncesEnAttenteValidation = (new Atexo_Statistiques_RequetesStatistiques())->getNombreConsultationEnAttente($agent->getId(), $agent->getServiceId(), $agent->getOrganisme());
            $nombreCommissions = (new Atexo_Statistiques_RequetesStatistiques())->getNombreCommission($agent->getId(), $agent->getOrganisme());

            $criteria->setDateDepotRegistre(Atexo_Util::getDateAWeekAgo());
            $resultat = (new Atexo_Statistiques_RequetesStatistiques())->getNombreQuestionPosees($criteria, false, true);
            $nombreQuestions = $resultat['total'] + $resultat['totalMapa'] + $resultat['accordcadre'] + $resultat['sad'];

            $nombreAnnoncesConcerneeQ = (new Atexo_Statistiques_RequetesStatistiques())->getNombreQuestions($agent->getId(), $agent->getServiceId(), $agent->getOrganisme(), true);
            $nombreReponsesADepouiller1 = (new Atexo_Statistiques_RequetesStatistiques())->getNombreReponsesADepouiller($agent->getId(), $agent->getServiceId(), $agent->getOrganisme(), Atexo_Config::getParameter('DEPOT_ELECTRONIQUE'), false);
            $nombreReponsesADepouiller2 = (new Atexo_Statistiques_RequetesStatistiques())->getNombreReponsesADepouiller($agent->getId(), $agent->getServiceId(), $agent->getOrganisme(), Atexo_Config::getParameter('DEPOT_PAPIER'), false);
            $nombreReponsesADepouiller = $nombreReponsesADepouiller1 + $nombreReponsesADepouiller2;
            $nombreAnnoncesConcerneesR1 = (new Atexo_Statistiques_RequetesStatistiques())->getNombreReponsesADepouiller($agent->getId(), $agent->getServiceId(), $agent->getOrganisme(), Atexo_Config::getParameter('DEPOT_ELECTRONIQUE'), true);
            $nombreAnnoncesConcerneesR2 = (new Atexo_Statistiques_RequetesStatistiques())->getNombreReponsesADepouiller($agent->getId(), $agent->getServiceId(), $agent->getOrganisme(), Atexo_Config::getParameter('DEPOT_PAPIER'), true);
            $nombreAnnoncesConcerneesR = $nombreAnnoncesConcerneesR1 + $nombreAnnoncesConcerneesR2;
            $nombreDeMois = Atexo_Config::getParameter('SOCLE_EXTERNE_NOMBRE_MOIS');
            $temps_fin = microtime(true);
            $txt = 'Debut getStatistiquesIndicateursClesAgents et fin : '.round($temps_fin - $temps_debut, 4)."\n\n";
            $fp = fopen(Atexo_Config::getParameter('COMMON_TMP').session_id().'Atexo_Statistiques_StatistiquesIndicateursCles_getStatistiquesIndicateursClesAgents_timer.txt', 'a');
            fwrite($fp, $txt);
            fclose($fp);

            return [$nombreAnnoncesTotal, $nombreAnnoncesConsultation, $nombreAnnoncesInformation, $nombreAnnoncesAttribution, $nombreAnnoncesAApprouver, $nombreAnnoncesAValider, $nombreAnnoncesEnAttenteValidation, $nombreCommissions, $nombreQuestions, $nombreAnnoncesConcerneeQ, $nombreReponsesADepouiller, $nombreAnnoncesConcerneesR, $nombreDeMois];
        }
    }

    /**
     * Retourne sous format d'un tableau la liste des indicateurs clés calculés pour affichage du côté entreprise
     * Cette fonction prend en paramètre la date de début pour calculer les indicateurs. Sinon, elle prend la valeur
     * qui est paramétrée dans application.xml.php.
     *
     * @param : $dateDebut date de début pour calculer l'ensemble des indicateurs
     *
     * @return : tableau de valeur dans l'ordre suivant
     *           array($dateDebut,$nombreConsultationsEnLigne,$nombreConsultationsPubliees,$nombreEntitesPubliques,$nombreEntreprisesInscrites,$nombreReponsesElectroniques,$nombreTelechargementsDCE)
     */
    public function getStatistiquesIndicateursClesEntreprises($dateDebut)
    {
        $temps_debut = microtime(true);
        if (!$dateDebut) {
            $dateDebut = Atexo_Config::getParameter('DATE_DEBUT_PORTAIL');
        }

        $nombreConsultationsEnLigne = 0;
        $nombreConsultationsPubliees = 0;
        $nombreTelechargementsDCE = 0;
        $nombreReponsesElectroniques = 0;
        $nombreEntreprisesInscrites = 0;
        $nombreEntitesPubliques = 0;
        if (Atexo_Module::isEnabled('AnnoncesMarches')) {//Cas d'une plate-forme de type Annonces Marches
            $criteria = new Atexo_Statistiques_CriteriaVo();
            //ne récupérer que les consultations qui ont été mises en lignes depuis la date début
            $criteria->setDateMiseEnLigneStart(substr($dateDebut, 0, 10));
            $nombreConsultationsPubliees = (new Atexo_Statistiques_RequetesStatistiques())->getNbreProceduresForPortailAnnoncesMarches($criteria);

            $criteria = new Atexo_Statistiques_CriteriaVo();
            $criteria->setIdTypeAvis(Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION'));
            $criteria->setConsultationEnLigne(true);
            $criteria->setDateMiseEnLigneAujourdhui(true);
            //ne récupérer que les consultations qui ont été mises en lignes depuis la date début et dont la date de fin est sup. à auj.
            $criteria->setDateRemisePlisStart(date('Y-m-d'));
            $nombreConsultationsEnLigne = (new Atexo_Statistiques_RequetesStatistiques())->getNbreProceduresForPortailAnnoncesMarches($criteria);

            $temps_fin = microtime(true);
            $txt = 'Debut getStatistiquesIndicateursClesEntreprises for Portail Annonces Marches et fin : '.round($temps_fin - $temps_debut, 4)."\n\n";
            $fp = fopen(Atexo_Config::getParameter('COMMON_TMP').session_id().'Atexo_Statistiques_StatistiquesIndicateursCles_getStatistiquesIndicateursClesEntreprises_timer.txt', 'a');
            fwrite($fp, $txt);
            fclose($fp);
        } else {//Cas d'une plate-forme MPE
            $organismes = Atexo_Organismes::retrieveOrganismes();
            if (is_array($organismes)) {
                foreach ($organismes as $organisme) {
                    $criteria = new Atexo_Statistiques_CriteriaVo();
                    $criteria->setOrganisme($organisme->getAcronyme());
                    //ne récupérer que les consultations qui ont été mises en lignes depuis la date début
                    $criteria->setDateMiseEnLigneStart(substr($dateDebut, 0, 10));
                    $resultat = (new Atexo_Statistiques_RequetesStatistiques())->getNbreProcedures($criteria, true, true);
                    if (is_array($resultat)) {
                        $nombreConsultationsPubliees += array_sum($resultat);
                    }

                    $criteria = new Atexo_Statistiques_CriteriaVo();
                    $criteria->setOrganisme($organisme->getAcronyme());
                    //ne récuperer que le nombre de téléchargement depuis la date de début
                    $criteria->setDateDepotRegistre(substr($dateDebut, 0, 10));
                    $resultat = (new Atexo_Statistiques_RequetesStatistiques())->getNombreTelechargement($criteria, true, true);
                    if (is_array($resultat)) {
                        $nombreTelechargementsDCE += array_sum($resultat);
                    }

                    $criteria = new Atexo_Statistiques_CriteriaVo();
                    $criteria->setOrganisme($organisme->getAcronyme());
                    //ne récuperer que le nombre d'offres déposées depuis la date de début
                    $resultat = (new Atexo_Statistiques_RequetesStatistiques())->getNombreReponsesElectronique($criteria, true, true);
                    if (is_array($resultat)) {
                        $nombreReponsesElectroniques += array_sum($resultat);
                    }

                    $criteria = new Atexo_Statistiques_CriteriaVo();
                    $criteria->setOrganisme($organisme->getAcronyme());
                    $criteria->setIdTypeAvis(Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION'));
                    $criteria->setConsultationEnLigne(true);
                    $criteria->setDateMiseEnLigneAujourdhui(true);
                    //ne récupérer que les consultations qui ont été mises en lignes depuis la date début et dont la date de fin est sup. à auj.
                    $criteria->setDateRemisePlisStart(date('Y-m-d'));
                    $resultat = (new Atexo_Statistiques_RequetesStatistiques())->getNbreProcedures($criteria, true, true);
                    if (is_array($resultat)) {
                        $nombreConsultationsEnLigne += array_sum($resultat);
                    }
                }
            }
            $nombreEntitesPubliques = (new Atexo_Statistiques_RequetesStatistiques())->getNombreOrganismesActive();

            $nombreEntrepriseFrance = (new Atexo_Statistiques_RequetesStatistiques())->getNbreEntrepriseEnregistre(true);
            $nombreEntrepriseEtranger = (new Atexo_Statistiques_RequetesStatistiques())->getNbreEntrepriseEnregistre(false);
            $nombreEntreprisesInscrites = $nombreEntrepriseFrance + $nombreEntrepriseEtranger;
            $temps_fin = microtime(true);
            $txt = 'Debut getStatistiquesIndicateursClesEntreprises for PF MPE et fin : '.round($temps_fin - $temps_debut, 4)."\n\n";
            $fp = fopen(Atexo_Config::getParameter('COMMON_TMP').session_id().'Atexo_Statistiques_StatistiquesIndicateursCles_getStatistiquesIndicateursClesEntreprises_timer.txt', 'a');
            fwrite($fp, $txt);
            fclose($fp);
        }

        return [$dateDebut, $nombreConsultationsEnLigne, $nombreConsultationsPubliees, $nombreEntitesPubliques, $nombreEntreprisesInscrites, $nombreReponsesElectroniques, $nombreTelechargementsDCE];
    }
}
