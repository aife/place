<?php

namespace Application\Service\Atexo\Statistiques;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationPeer;
use Application\Propel\Mpe\CommonTTypeContrat;
use Application\Propel\Mpe\CommonTTypeContratQuery;
use Application\Propel\Mpe\CommonTypeProcedureOrganisme;
use Application\Propel\Mpe\CommonTypeProcedureOrganismePeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureType;
use Exception;
use PDO;
use Prado\Prado;

/**
 * @author Yassine SBAI <yassine.sbai@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Statistiques_RequetesStatistiques
{
    /*-----------------------------------Debut suivi données entreprises--------------------------------------*/

    /**
     * Retourne le nombre d'entreprises enregistrées.
     *
     * @param : $org
     *
     * @return : Integer
     */
    public function getNbreEntrepriseEnregistre($siegeFr = false)
    {
        $sql = 'Select count(*) as nbr From Entreprise WHERE paysenregistrement';
        if ($siegeFr) {
            $sql .= "='FR'";
        } else {
            $sql .= "!='FR'";
        }

        return self::getNbElementQuery($sql);
    }

    public function getEntrepriseByCategorie($idCategorie)
    {
        $sql = 'Select count(distinct(AL.id_inscrit))  From Alerte AL '.
               'WHERE AL.categorie=:categorie';

        $statement = Atexo_Db::getLinkCommon(true)->prepare($sql);
        $params = [':categorie' => $idCategorie];
        $statement->execute($params);
        while ($row = $statement->fetch(PDO::FETCH_NUM)) {
            return $row[0];
        }
    }

    public function getNbrResponsaleEntreprise($value = false)
    {
        $sql = 'Select count(*) as nbr From Inscrit WHERE profil';
        if ($value) {
            $sql .= "='2'";
        } else {
            $sql .= "='1'";
        }

        return self::getNbElementQuery($sql);
    }

    public function getNbrInscritAlerteElectronique()
    {
        $sql = 'Select count(distinct(id_inscrit)) From Alerte ';

        return self::getNbElementQuery($sql);
    }

    public function getCountQuestionDCE()
    {
        $sql = 'Select count(*) as nbr  From questions_dce '.
               'WHERE date_depot > \''.date('Y').'-01-01 00:00\'';

        $statement = Atexo_Db::getLinkCommon(true)->prepare($sql);
        $statement->execute();

        while ($row = $statement->fetch(PDO::FETCH_NUM)) {
            return $row[0];
        }
    }

    public function getCountReponseElectronique()
    {
        $sql = 'Select count(*) as nbr  From Offres '.
               'WHERE untrusteddate > \''.date('Y').'-01-01 00:00\'';

        $statement = Atexo_Db::getLinkCommon(true)->prepare($sql);
        $statement->execute();

        while ($row = $statement->fetch(PDO::FETCH_NUM)) {
            return $row[0];
        }
    }

    public function getCountReponseElectroniqueSignature($signature)
    {
        $sql = 'Select count(OFF.id) as nbr  From Offres OFF , consultation CON '.
               'WHERE OFF.untrusteddate > \''.date('Y').'-01-01 00:00\' '.
               'AND CON.id = OFF.consultation_id '.
               'AND CON.organisme = OFF.organisme '.
               'AND CON.signature_offre=:signature';

        $statement = Atexo_Db::getLinkCommon(true)->prepare($sql);
        $params = [':signature' => $signature];
        $statement->execute($params);
        while ($row = $statement->fetch(PDO::FETCH_NUM)) {
            return $row[0];
        }
    }

    public function getCountjustificatifs()
    {
        $sql = 'Select count(distinct(id_entreprise))  From Justificatifs  ';

        return self::getNbElementQuery($sql);
    }

    /*-----------------------------------Fin suivi données entreprises--------------------------------------*/

    /*-----------------------------------Debut suivi données metiers--------------------------------------*/

    public function search($critere, $ligne = 1)
    {
        $sql = null;
        $resultat = [];

        if (1 == $ligne) {
            $sql = 'SELECT count(*) as nbr , id_type_procedure_org FROM `consultation` ';
        } elseif (2 == $ligne) {
            $sql = 'SELECT sum(nbr_telechargement_dce) as nbr , id_type_procedure_org FROM `consultation` ';
        } elseif (3 == $ligne) {
            $sql = 'SELECT sum(nbr_reponse) as nbr , id_type_procedure_org FROM `consultation` ';
        }

        $sql .= 'WHERE categorie = '.(new Atexo_Db())->quote($critere->getCategorieConsultation());
        $sql .= ' AND organisme = \''.(new Atexo_Db())->quote($critere->getOrganisme()).'\' ';

        if ($critere) {
            if ($critere->getIdService()) {
                $sql .= ' AND (service_id '.(new Atexo_Db())->quote($critere->getIdService());
                $sql .= ' OR service_associe_id '.(new Atexo_Db())->quote($critere->getIdService()).')';
            }
            if ($critere->getDateMiseEnLigneStart()) {
                $sql .= ' AND date_mise_en_ligne_calcule >= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneStart()).' 00:00\' ';
            }
            if ($critere->getDateMiseEnLigneEnd()) {
                $sql .= ' AND date_mise_en_ligne_calcule <= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneEnd()).' 23:59\' ';
            }
            if ($critere->getDateMiseEnLigneStart() || $critere->getDateMiseEnLigneEnd()) {
                $sql .= ' AND date_mise_en_ligne_calcule is not null ';
            }
        }
        $sql .= ' GROUP BY (`id_type_procedure_org`)';

        $statement = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_ASSOC);

        foreach ($statement as $row) {
            if ($row['nbr']) {
                $resultat[$row['id_type_procedure_org']] = $row['nbr'];
            } else {
                $resultat[$row['id_type_procedure_org']] = 0;
            }
        }

        return $resultat;
    }

    public function searchProcedureAdapte($critere, $ligne = 1, $column = 1)
    {
        $sql = null;
        //les id typesprocedures mapa
        $idtypeProcsMAPA = [];
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));

        $c = new Criteria();
        $c->add(CommonTypeProcedureOrganismePeer::MAPA, 1, Criteria::EQUAL);
        $c->add(CommonTypeProcedureOrganismePeer::ORGANISME, $critere->getOrganisme(), Criteria::EQUAL);
        $objectsTypeProcedure = CommonTypeProcedureOrganismePeer::doSelect($c, $connexion);

        if (is_array($objectsTypeProcedure) && 0 != count($objectsTypeProcedure)) {
            foreach ($objectsTypeProcedure as $objectTypeProcedure) {
                $idtypeProcsMAPA[] = $objectTypeProcedure->getIdTypeProcedure();
            }
        }
        $idtypeProcsMAPA = implode(',', $idtypeProcsMAPA);

        if (1 == $ligne) {
            $sql = 'SELECT count(*) as nbr FROM `consultation` ';
        } elseif (2 == $ligne) {
            $sql = 'SELECT sum(nbr_telechargement_dce) as nbr FROM `consultation` ';
        } elseif (3 == $ligne) {
            $sql = 'SELECT sum(nbr_reponse) as nbr FROM `consultation` ';
        }

        $sql .= 'WHERE categorie = '.(new Atexo_Db())->quote($critere->getCategorieConsultation());
        $sql .= ' AND organisme = \''.(new Atexo_Db())->quote($critere->getOrganisme()).'\' ';
        $sql .= ' AND id_type_procedure_org IN('.(new Atexo_Db())->quote($idtypeProcsMAPA).')';

        if (1 == $column) {
            $sql .= ' AND autoriser_reponse_electronique = 0';
        } elseif (2 == $column) {
            $sql .= ' AND autoriser_reponse_electronique = 1';
            $sql .= ' AND signature_offre = 0';
        } elseif (3 == $column) {
            $sql .= ' AND autoriser_reponse_electronique = 1';
            $sql .= ' AND signature_offre = 1';
        }

        if ($critere) {
            if ($critere->getIdService()) {
                $sql .= ' AND (service_id '.(new Atexo_Db())->quote($critere->getIdService());
                $sql .= ' OR service_associe_id '.(new Atexo_Db())->quote($critere->getIdService()).')';
            }
            if ($critere->getDateMiseEnLigneStart()) {
                $sql .= ' AND date_mise_en_ligne_calcule >= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneStart()).' 00:00\' ';
            }
            if ($critere->getDateMiseEnLigneEnd()) {
                $sql .= ' AND date_mise_en_ligne_calcule <= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneEnd()).' 23:59\' ';
            }
            if ($critere->getDateMiseEnLigneStart() || $critere->getDateMiseEnLigneEnd()) {
                $sql .= ' AND date_mise_en_ligne_calcule is not null ';
            }
        }

        $statement = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_ASSOC);

        foreach ($statement as $row) {
            if ($row['nbr']) {
                return $row['nbr'];
            } else {
                return 0;
            }
        }
    }

    public function setNbrOffres()
    {
        $resultats = [];
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));

        $sql = 'SELECT distinct OFF.organisme ,OFF.consultation_id FROM consultation CON , Offres OFF ';
        $sql .= 'WHERE OFF.organisme = CON.organisme ';
        $sql .= 'AND OFF.consultation_id = CON.id ';
        //$sql .= 'AND CON.date_mise_en_ligne_calcule <=\''.date("Y-m-d h:i:s").'\'';
        //$sql .= 'AND CON.datefin >=\''.date("Y-m-d h:i:s").'\'';

        $statement = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_ASSOC);

        foreach ($statement as $row) {
            $resultats[] = [$row['organisme'], $row['consultation_id']];
        }

        foreach ($resultats as $resultat) {
            $sql = 'SELECT count(*) FROM Offres';
            $sql .= ' WHERE organisme = \''.(new Atexo_Db())->quote($resultat[0]).'\'';
            $sql .= ' AND consultation_id = '.(new Atexo_Db())->quote($resultat[1]);

            $statement = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_NUM);
            $consultation = CommonConsultationPeer::retrieveByPK($resultat[1], $connexionCom);

            foreach ($statement as $row) {
                $consultation->setNbrReponse($row[0]);
                $consultation->save($connexionCom);
            }
        }
    }

    public function setNbrTelechargement()
    {
        $resultats = [];
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));

        $sql = 'SELECT distinct Tel.organisme ,Tel.consultation_id FROM consultation CON ,Telechargement Tel ';
        $sql .= 'WHERE Tel.organisme = CON.organisme ';
        $sql .= 'AND Tel.consultation_id = CON.id ';
        //$sql .= 'AND CON.date_mise_en_ligne_calcule <=\''.date("Y-m-d h:i:s").'\'';
        //$sql .= 'AND CON.datefin >=\''.date("Y-m-d h:i:s").'\'';

        $statement = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_ASSOC);

        foreach ($statement as $row) {
            $resultats[] = [$row['organisme'], $row['consultation_id']];
        }

        foreach ($resultats as $resultat) {
            $sql = 'SELECT count(*) FROM Telechargement';
            $sql .= ' WHERE organisme = \''.(new Atexo_Db())->quote($resultat[0]).'\'';
            $sql .= ' AND consultation_id = '.(new Atexo_Db())->quote($resultat[1]);

            $statement = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_NUM);
            $consultation = CommonConsultationPeer::retrieveByPK($resultat[1], $connexionCom);

            foreach ($statement as $row) {
                $consultation->setNbrTelechargementDce($row[0]);
                $consultation->save($connexionCom);
            }
        }
    }

    public function setDateMiseEnLigneCalcule()
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $c = new Criteria();
        $arrayConsultations = CommonConsultationPeer::doSelect($c, $connexionCom);
        foreach ($arrayConsultations as $Consultation) {
            try {
                if ($Consultation instanceof CommonConsultation) {
                    $dateMiseEnLigneCalcule = Atexo_Consultation::getDateMiseEnLigne($Consultation);
                    $Consultation->setDateMiseEnLigneCalcule($dateMiseEnLigneCalcule);
                    $Consultation->save($connexionCom);
                    echo 'Update consultation : '.$Consultation->getOrganisme().' '.$Consultation->getId().' '.$dateMiseEnLigneCalcule.'<br>';
                }
            } catch (Exception $e) {
                echo $e;
            }
        }
    }

    public function getAllTypeProcedures($acrOrg)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $Result = [];
        $Result['mapinf'] = [];
        $Result['mapsup'] = [];
        $Result['ao'] = [];
        $Result['mn'] = [];
        $Result['dc'] = [];
        $Result['autre'] = [];
        $Result['sad'] = [];
        $Result['accordcadre'] = [];
        $c->add(CommonTypeProcedureOrganismePeer::ORGANISME, $acrOrg, Criteria::EQUAL);
        $tabObjectTypeProcs = CommonTypeProcedureOrganismePeer::doSelect($c, $connexion);
        if (is_array($tabObjectTypeProcs)) {
            foreach ($tabObjectTypeProcs as $tabObjectTypeProc) {
                if (1 == $tabObjectTypeProc->getMapa()) {
                    if ($tabObjectTypeProc->getIdMontantMapa() == Atexo_Config::getParameter('MONTANT_MAPA_INF_90')) {
                        array_push($Result['mapinf'], $tabObjectTypeProc->getIdTypeProcedure());
                    } elseif ($tabObjectTypeProc->getIdMontantMapa() == Atexo_Config::getParameter('MONTANT_MAPA_SUP_90')) {
                        array_push($Result['mapsup'], $tabObjectTypeProc->getIdTypeProcedure());
                    }
                } elseif (1 == $tabObjectTypeProc->getAo()) {
                    array_push($Result['ao'], $tabObjectTypeProc->getIdTypeProcedure());
                } elseif (1 == $tabObjectTypeProc->getMn()) {
                    array_push($Result['mn'], $tabObjectTypeProc->getIdTypeProcedure());
                } elseif (1 == $tabObjectTypeProc->getDc()) {
                    array_push($Result['dc'], $tabObjectTypeProc->getIdTypeProcedure());
                } elseif (1 == $tabObjectTypeProc->getAutre()) {
                    array_push($Result['autre'], $tabObjectTypeProc->getIdTypeProcedure());
                } elseif (1 == $tabObjectTypeProc->getSad()) {
                    array_push($Result['sad'], $tabObjectTypeProc->getIdTypeProcedure());
                } elseif (1 == $tabObjectTypeProc->getAccordCadre()) {
                    array_push($Result['accordcadre'], $tabObjectTypeProc->getIdTypeProcedure());
                }
            }
        }

        return $Result;
    }

    /*-----------------------------------Fin suivi données metiers--------------------------------------*/

    /*-----------------------------------Début statistiques avis publiés--------------------------------*/
    public function retrieveConsultationPublieBoamp(
        $organisme,
        $typeAnnonce,
        $service,
        $valeursCumules,
        $dateDebut,
        $dateFin,
        $byTypeProcedure = false,
        $dontCountMapas = false
    ) {
        $sql = ' SELECT count(consultation.id) as nombreConsultation, consultation.id_type_procedure_org as id_type_procedure, '
           .' ao, mn, pn, dc, autre, Type_Procedure_Organisme.mapa, id_montant_mapa '
           .' from consultation left join Type_Procedure_Organisme on consultation.id_type_procedure_org=Type_Procedure_Organisme.id_type_procedure '
           .' AND consultation.organisme=Type_Procedure_Organisme.organisme '
           .' ,AnnonceBoamp '
           .' where AnnonceBoamp.consultation_id=consultation.id and AnnonceBoamp.organisme=consultation.organisme '
           ." and AnnonceBoamp.statut_destinataire='".Atexo_Config::getParameter('DESTINATAIRE_STATUT_PU')."' "
           ." AND id_type_avis='".(new Atexo_Db())->quote($typeAnnonce)."' AND consultation.organisme='".$organisme."' ";

        if (!$valeursCumules) {
            $sql .= " AND consultation.service_id='".(new Atexo_Db())->quote($service)."' ";
        } else {
            if ($service && '0' !== $service && 0 !== $service) {
                $services = Atexo_EntityPurchase::retrieveAllChildrenServices($service, $organisme);
                $sql .= ' AND consultation.service_id IN '.$services;
            }
        }

        if ($dateDebut) {
            $sql .= " AND date_mise_en_ligne_calcule >= '".(new Atexo_Db())->quote(Atexo_Util::frnDate2iso($dateDebut))." 00:00' ";
        }
        if ($dateFin) {
            $sql .= " AND date_mise_en_ligne_calcule <= '".(new Atexo_Db())->quote(Atexo_Util::frnDate2iso($dateFin))." 23:59' ";
        }

        $sql .= ' GROUP BY id_type_procedure ';

        $statement = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_ASSOC);
        if ($byTypeProcedure) {
            if ($dontCountMapas) {
                $typesProcedures = (new Atexo_Consultation_ProcedureType())->rerieveTypeProcedureSaufMapa(true, false, $organisme);
                $arrayResultat = Atexo_Util::initializeArray($typesProcedures, 0);
                $arrayResultat['MAPA_INF'] = 0;
                $arrayResultat['MAPA_SUPP'] = 0;
            } else {
                $typesProcedures = (new Atexo_Consultation_ProcedureType())->rerieveAllProcedure(true, false, $organisme);
                $arrayResultat = Atexo_Util::initializeArray($typesProcedures, 0);
            }
        } else {
            $arrayResultat = ['mapaInf' => 0, 'mapaSupp' => 0, 'totalMapa' => 0, 'ao' => 0, 'mn' => 0, 'pn' => 0, 'dc' => 0, 'autre' => 0, 'total' => 0];
        }
        self::getArrayParamFromStatement($arrayResultat, $statement, $byTypeProcedure, $dontCountMapas);
        Atexo_Db::closeCommon();

        return $arrayResultat;
    }

    public function retrieveConsultationPublieJal(
        $organisme,
        $typeAnnonce,
        $service,
        $valeursCumules,
        $dateDebut,
        $dateFin,
        $byTypeProcedure = false,
        $dontCountMapas = false
    ) {
        $sql = ' SELECT  count( consultation.id ) as nombreConsultation ,consultation.id,consultation.date_mise_en_ligne_calcule ,consultation.id_type_procedure_org as id_type_procedure, AnnonceJAL.id, '
           .' ao, mn, pn, dc, autre, Type_Procedure_Organisme.mapa, id_montant_mapa '
           .' from consultation left join Type_Procedure_Organisme on consultation.id_type_procedure_org=Type_Procedure_Organisme.id_type_procedure '
           .'AND consultation.organisme=Type_Procedure_Organisme.organisme '
           .' ,AnnonceJAL, DestinataireAnnonceJAL  '
           .' where AnnonceJAL.consultation_id=consultation.id and AnnonceJAL.organisme=consultation.organisme and  AnnonceJAL.id=DestinataireAnnonceJAL.idAnnonceJAL '
           .' and  AnnonceJAL.organisme=DestinataireAnnonceJAL.organisme '
           ." AND DestinataireAnnonceJAL.statut='".Atexo_Config::getParameter('STATUT_PUBLIE_JAL')."' AND consultation.id_type_avis='".(new Atexo_Db())->quote($typeAnnonce)
           ."' AND consultation.organisme='".$organisme."' ";

        if (!$valeursCumules) {
            $sql .= " AND consultation.service_id='".(new Atexo_Db())->quote($service)."' ";
        } else {
            if ($service && '0' !== $service && 0 !== $service) {
                $services = Atexo_EntityPurchase::retrieveAllChildrenServices($service, $organisme);
                $sql .= ' AND consultation.service_id IN '.$services;
            }
        }

        if ($dateDebut) {
            $sql .= " AND date_mise_en_ligne_calcule >= '".(new Atexo_Db())->quote(Atexo_Util::frnDate2iso($dateDebut))." 00:00'";
        }
        if ($dateFin) {
            $sql .= " AND date_mise_en_ligne_calcule <= '".(new Atexo_Db())->quote(Atexo_Util::frnDate2iso($dateFin))." 23:59'";
        }

        $sql .= ' GROUP BY id_type_procedure ';

        $statement = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_ASSOC);
        if ($byTypeProcedure) {
            if ($dontCountMapas) {
                $typesProcedures = (new Atexo_Consultation_ProcedureType())->rerieveTypeProcedureSaufMapa(true, false, $organisme);
                $arrayResultat = Atexo_Util::initializeArray($typesProcedures, 0);
                $arrayResultat['MAPA_INF'] = 0;
                $arrayResultat['MAPA_SUPP'] = 0;
            } else {
                $typesProcedures = (new Atexo_Consultation_ProcedureType())->rerieveAllProcedure(true, false, $organisme);
                $arrayResultat = Atexo_Util::initializeArray($typesProcedures, 0);
            }
        } else {
            $arrayResultat = ['mapaInf' => 0, 'mapaSupp' => 0, 'totalMapa' => 0, 'ao' => 0, 'mn' => 0, 'pn' => 0, 'dc' => 0, 'autre' => 0, 'total' => 0];
        }

        self::getArrayParamFromStatement($arrayResultat, $statement, $byTypeProcedure, $dontCountMapas);
        Atexo_Db::closeCommon();

        return $arrayResultat;
    }

    public function retrieveConsultationPmiSeule(
        $organisme,
        $typeAnnonce,
        $service,
        $valeursCumules,
        $dateDebut,
        $dateFin,
        $byTypeProcedure = false,
        $dontCountMapas = false
    ) {
        $sql = ' SELECT count(consultation.id) as nombreConsultation, consultation.id_type_procedure_org as id_type_procedure, ao, mn, dc, autre, Type_Procedure_Organisme.mapa, '
           .' id_montant_mapa, sad, accord_cadre as accordcadre '
           .' from consultation left join Type_Procedure_Organisme on consultation.id_type_procedure_org=Type_Procedure_Organisme.id_type_procedure '
           .' AND consultation.organisme=Type_Procedure_Organisme.organisme '
           .' ,AVIS, DestinataireAnnonceJAL  '
           .' where AVIS.consultation_id=consultation.id AND AVIS.organisme=consultation.organisme '
           ." AND AVIS.statut='".Atexo_Config::getParameter('STATUT_AVIS_PUBLIE')."' AND id_type_avis='".(new Atexo_Db())->quote($typeAnnonce)
           ."' consultation.organisme='".$organisme."' "
           ." AND AVIS.date_pub IS NOT NULL AND AVIS.date_pub NOT LIKE '0000-00-00' ";

        if (!$valeursCumules) {
            $sql .= " AND consultation.service_id='".(new Atexo_Db())->quote($service)."' ";
        } else {
            if ($service && '0' !== $service && 0 !== $service) {
                $services = Atexo_EntityPurchase::retrieveAllChildrenServices($service, $organisme);
                $sql .= ' AND consultation.service_id IN '.$services;
            }
        }

        if ($dateDebut) {
            $sql .= " AND date_mise_en_ligne_calcule >= '".(new Atexo_Db())->quote(Atexo_Util::frnDate2iso($dateDebut))." 00:00'";
        }
        if ($dateFin) {
            $sql .= " AND date_mise_en_ligne_calcule <= '".(new Atexo_Db())->quote(Atexo_Util::frnDate2iso($dateFin))." 23:59'";
        }

        $sql .= ' GROUP BY id_type_procedure ';

        $statement = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_ASSOC);
        if ($byTypeProcedure) {
            if ($dontCountMapas) {
                $typesProcedures = (new Atexo_Consultation_ProcedureType())->rerieveTypeProcedureSaufMapa(true, false, $organisme);
                $arrayResultat = Atexo_Util::initializeArray($typesProcedures, 0);
                $arrayResultat['MAPA_INF'] = 0;
                $arrayResultat['MAPA_SUPP'] = 0;
            } else {
                $typesProcedures = (new Atexo_Consultation_ProcedureType())->rerieveAllProcedure(true, false, $organisme);
                $arrayResultat = Atexo_Util::initializeArray($typesProcedures, 0);
            }
        } else {
            $arrayResultat = ['mapaInf' => 0, 'mapaSupp' => 0, 'totalMapa' => 0, 'ao' => 0, 'mn' => 0, 'dc' => 0, 'autre' => 0, 'total' => 0, 'sad' => 0, 'accordcadre' => '0'];
        }
        self::getArrayParamFromStatement($arrayResultat, $statement, $byTypeProcedure, $dontCountMapas);
        Atexo_Db::closeCommon();

        return $arrayResultat;
    }

    public function retrieveConsultationJoue($organisme, $byTypeProcedure = false, $dontCountMapas = false)
    {
        if ($byTypeProcedure) {
            if ($dontCountMapas) {
                $typesProcedures = (new Atexo_Consultation_ProcedureType())->rerieveTypeProcedureSaufMapa(true, false, $organisme);
                $arrayResultat = Atexo_Util::initializeArray($typesProcedures, 0);
                $arrayResultat['MAPA_INF'] = 0;
                $arrayResultat['MAPA_SUPP'] = 0;
            } else {
                $typesProcedures = (new Atexo_Consultation_ProcedureType())->rerieveAllProcedure(true, false, $organisme);
                $arrayResultat = Atexo_Util::initializeArray($typesProcedures, 0);
            }
        } else {
            $arrayResultat = ['mapaInf' => 0, 'mapaSupp' => 0, 'totalMapa' => 0, 'ao' => 0, 'mn' => 0, 'pn' => 0, 'dc' => 0, 'autre' => 0, 'total' => 0];
        }

        return $arrayResultat;
    }

    /*-----------------------------------Fin statistiques avis publiés--------------------------------*/

    /*-------------Début tableau de bord--------------------*/

    /**
     * Commentaires.
     *
     * @param Atexo_Statistiques_CriteriaVo $critere:              objet criteres de recherche
     * @param bool                          $groupByTypeProcedure: precise si l'on doit groupe par type de procedure
     * @param bool                          $dontCountMapas:       precise si l'on doit compter le nombre de MAPA
     *
     * @return array: tableau des donnees des statistiques
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function getNbreProcedures($critere, $groupByTypeProcedure = false, $dontCountMapas = false)
    {
        $sql = ' SELECT count(consultation.id) as nombreConsultation, consultation.id_type_procedure_org as id_type_procedure, '
           .' Type_Procedure_Organisme.id_type_procedure_portail as id_type_procedure_portail, ao, mn, pn, dc, autre, Type_Procedure_Organisme.mapa, id_montant_mapa, '
           .' sad, accord_cadre as accordcadre '
            .' FROM consultation left join Type_Procedure_Organisme on consultation.id_type_procedure_org=Type_Procedure_Organisme.id_type_procedure '
            .' AND consultation.organisme=Type_Procedure_Organisme.organisme '
            ." WHERE consultation.organisme='".$critere->getOrganisme()."' ";

        if ($critere) {
            if (0 == $critere->getAlloti()) {
                $sql .= " AND consultation.alloti='0' ";
            } elseif (1 == $critere->getAlloti()) {
                $sql .= " AND consultation.alloti='1' ";
            }
            if ($critere->getIdService()) {
                $sql .= ' AND (service_id '.(new Atexo_Db())->quote($critere->getIdService()).')';
            }
            if ($critere->getDateMiseEnLigneStart()) {
                $sql .= " AND date_mise_en_ligne_calcule >= '".(new Atexo_Db())->quote($critere->getDateMiseEnLigneStart())." 00:00' ";
            }
            if ($critere->getDateMiseEnLigneEnd()) {
                $sql .= " AND date_mise_en_ligne_calcule <= '".(new Atexo_Db())->quote($critere->getDateMiseEnLigneEnd())." 23:59' ";
            }
            if ($critere->getConsultationEnLigne() || $critere->getDateMiseEnLigneStart() || $critere->getDateMiseEnLigneEnd()) {
                $sql .= " AND date_mise_en_ligne_calcule is not null AND date_mise_en_ligne_calcule NOT LIKE '0000-00-00%' ";
            }
            if ($critere->getDateRemisePlisStart()) {
                $sql .= " AND datefin >= '".(new Atexo_Db())->quote($critere->getDateRemisePlisStart())." 00:00' ";
            }
            if ($critere->getDateRemisePlisEnd()) {
                $sql .= " AND datefin <= '".(new Atexo_Db())->quote($critere->getDateRemisePlisEnd())." 23:59' ";
            }
            if ($critere->getDateRemisePlisStart() || $critere->getDateRemisePlisEnd()) {
                $sql .= ' AND datefin is not null ';
            }
            if ($critere->getCategorieConsultation()) {
                $sql .= " AND categorie='".(new Atexo_Db())->quote($critere->getCategorieConsultation())."' ";
            }
            if (null !== $critere->getReponsesAutotisee()) {
                if ($critere->getReponsesAutotisee()) {
                    $sql .= " AND autoriser_reponse_electronique='1' ";
                } else {
                    $sql .= " AND autoriser_reponse_electronique!='1' ";
                }
            }
            if (null !== $critere->getReponsesAvecSignature()) {
                if ($critere->getReponsesAvecSignature()) {
                    $sql .= " AND signature_offre='1' ";
                } else {
                    $sql .= " AND signature_offre!='1' ";
                }
            }

            /*if($critere->getConsultationExterne()) {
                $sql .= " AND consultation_externe='1' ";
            } else {
                $sql .= " AND consultation_externe!='1' ";
            }*/

            if ($critere->getIdTypeAvis()) {
                $sql .= " AND consultation.id_type_avis='".(new Atexo_Db())->quote($critere->getIdTypeAvis())."' ";
            }
            if ($critere->getDateMiseEnLigneAujourdhui()) {
                $sql .= ' AND consultation.date_mise_en_ligne_calcule<= NOW() ';
                $sql .= ' AND consultation.datefin> NOW() ';
            }

            if ($critere->getConsultationEnLigne()) {
                $sql .= " AND consultation.type_acces = '1' ";
            }

            $queryClause = self::getClauseQuery($critere);
            $sql .= ' AND '.$queryClause;
        }

        $sql .= ' GROUP BY id_type_procedure ';

        $statement = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_ASSOC);

        if ($groupByTypeProcedure) {
            if ($dontCountMapas) {
                $typesProcedures = (new Atexo_Consultation_ProcedureType())->rerieveTypeProcedureSaufMapa(true, false, $critere->getOrganisme());
                $arrayResultat = Atexo_Util::initializeArray($typesProcedures, 0);
                $arrayResultat['MAPA_INF'] = 0;
                $arrayResultat['MAPA_SUPP'] = 0;
            } else {
                $typesProcedures = (new Atexo_Consultation_ProcedureType())->rerieveAllProcedure(true, false, $critere->getOrganisme());
                $arrayResultat = Atexo_Util::initializeArray($typesProcedures, 0);
            }
        } else {
            $arrayResultat = ['mapaInf' => 0, 'mapaSupp' => 0, 'totalMapa' => 0, 'ao' => 0, 'mn' => 0, 'pn' => 0, 'dc' => 0, 'autre' => 0, 'total' => 0, 'sad' => 0, 'accordcadre' => '0', 'totalAccordCadreSad' => 0, 'grandTotal' => 0];
        }

        foreach ($statement as $row) {
            if ($groupByTypeProcedure) {
                if ($dontCountMapas) {
                    if ('1' == $row['mapa']) {
                        if ($row['id_montant_mapa'] == Atexo_Config::getParameter('MONTANT_MAPA_INF_90')) {
                            $arrayResultat['MAPA_INF'] += $row['nombreConsultation'];
                        } else {
                            $arrayResultat['MAPA_SUPP'] += $row['nombreConsultation'];
                        }
                    } else {
                        $arrayResultat[$row['id_type_procedure']] = $row['nombreConsultation'];
                    }
                } else {
                    $arrayResultat[$row['id_type_procedure']] = $row['nombreConsultation'];
                }
            } else {
                if ('1' == $row['mapa']) {
                    if ($row['id_montant_mapa'] == Atexo_Config::getParameter('MONTANT_MAPA_INF_90')) {
                        $arrayResultat['mapaInf'] += $row['nombreConsultation'];
                    } else {
                        $arrayResultat['mapaSupp'] += $row['nombreConsultation'];
                    }
                    $arrayResultat['totalMapa'] += $row['nombreConsultation'];
                    $arrayResultat['grandTotal'] += $row['nombreConsultation'];
                } else {
                    if ('1' == $row['ao']) {
                        $arrayResultat['ao'] += $row['nombreConsultation'];
                        $arrayResultat['total'] += $row['nombreConsultation'];
                        $arrayResultat['grandTotal'] += $row['nombreConsultation'];
                    } elseif ('1' == $row['mn']) {
                        $arrayResultat['mn'] += $row['nombreConsultation'];
                        $arrayResultat['total'] += $row['nombreConsultation'];
                        $arrayResultat['grandTotal'] += $row['nombreConsultation'];
                    } elseif ('1' == $row['pn']) {
                        $arrayResultat['pn'] += $row['nombreConsultation'];
                        $arrayResultat['total'] += $row['nombreConsultation'];
                        $arrayResultat['grandTotal'] += $row['nombreConsultation'];
                    } elseif ('1' == $row['dc']) {
                        $arrayResultat['dc'] += $row['nombreConsultation'];
                        $arrayResultat['total'] += $row['nombreConsultation'];
                        $arrayResultat['grandTotal'] += $row['nombreConsultation'];
                    } elseif ('1' == $row['sad']) {
                        $arrayResultat['sad'] += $row['nombreConsultation'];
                        $arrayResultat['totalAccordCadreSad'] += $row['nombreConsultation'];
                        $arrayResultat['grandTotal'] += $row['nombreConsultation'];
                    } elseif ('1' == $row['accordcadre']) {
                        $arrayResultat['accordcadre'] += $row['nombreConsultation'];
                        $arrayResultat['totalAccordCadreSad'] += $row['nombreConsultation'];
                        $arrayResultat['grandTotal'] += $row['nombreConsultation'];
                    } elseif ('1' == $row['autre']) {
                        $arrayResultat['autre'] += $row['nombreConsultation'];
                        $arrayResultat['total'] += $row['nombreConsultation'];
                        $arrayResultat['grandTotal'] += $row['nombreConsultation'];
                    }
                }
            }
        }
        Atexo_Db::closeCommon();

        return $arrayResultat;
    }

    public function getNbreLots($critere, $groupByTypeProcedure = false, $dontCountMapas = false)
    {
        $sql = ' SELECT count(consultation.id) as nombreConsultation, consultation.id_type_procedure_org as id_type_procedure, lot, ao, mn,pn, dc, autre, Type_Procedure_Organisme.mapa,'
           .' id_montant_mapa, sad, accord_cadre as accordcadre '
            .' FROM consultation left join Type_Procedure_Organisme on consultation.id_type_procedure_org=Type_Procedure_Organisme.id_type_procedure '
            .' AND consultation.organisme=Type_Procedure_Organisme.organisme, '
            .'      CategorieLot '
            ." WHERE consultation.id=CategorieLot.consultation_id AND  consultation.organisme=CategorieLot.organisme AND  consultation.organisme='".$critere->getOrganisme()."' ";

        if ($critere) {
            if ($critere->getIdService()) {
                $sql .= ' AND (consultation.service_id '.(new Atexo_Db())->quote($critere->getIdService()).')';
            }
            if ($critere->getDateMiseEnLigneStart()) {
                $sql .= ' AND consultation.date_mise_en_ligne_calcule >= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneStart()).' 00:00\' ';
            }
            if ($critere->getDateMiseEnLigneEnd()) {
                $sql .= ' AND consultation.date_mise_en_ligne_calcule <= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneEnd()).' 23:59\' ';
            }
            if ($critere->getDateMiseEnLigneStart() || $critere->getDateMiseEnLigneEnd()) {
                $sql .= ' AND consultation.date_mise_en_ligne_calcule is not null ';
            }
            if ($critere->getDateRemisePlisStart()) {
                $sql .= ' AND consultation.datefin >= \''.(new Atexo_Db())->quote($critere->getDateRemisePlisStart()).' 00:00\' ';
            }
            if ($critere->getDateRemisePlisEnd()) {
                $sql .= ' AND consultation.datefin <= \''.(new Atexo_Db())->quote($critere->getDateRemisePlisEnd()).' 23:59\' ';
            }
            if ($critere->getDateRemisePlisStart() || $critere->getDateRemisePlisEnd()) {
                $sql .= ' AND consultation.datefin is not null ';
            }

            if ($critere->getIdTypeAvis()) {
                $sql .= " AND consultation.id_type_avis='".(new Atexo_Db())->quote($critere->getIdTypeAvis())."' ";
            }
            if ($critere->getDateMiseEnLigneAujourdhui()) {
                $sql .= ' AND consultation.date_mise_en_ligne_calcule<= NOW() ';
                $sql .= ' AND consultation.datefin> NOW() ';
            }

            $queryClause = self::getClauseQuery($critere);
            $sql .= ' AND '.$queryClause;
        }

        $sql .= ' GROUP BY id_type_procedure ';

        $statement = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_ASSOC);

        if ($groupByTypeProcedure) {
            if ($dontCountMapas) {
                $typesProcedures = (new Atexo_Consultation_ProcedureType())->rerieveTypeProcedureSaufMapa(true, false, $critere->getOrganisme());
                $arrayResultat = Atexo_Util::initializeArray($typesProcedures, 0);
                $arrayResultat['MAPA_INF'] = 0;
                $arrayResultat['MAPA_SUPP'] = 0;
            } else {
                $typesProcedures = (new Atexo_Consultation_ProcedureType())->rerieveAllProcedure(true, false, $critere->getOrganisme());
                $arrayResultat = Atexo_Util::initializeArray($typesProcedures, 0);
            }
        } else {
            $arrayResultat = ['mapaInf' => 0, 'mapaSupp' => 0, 'totalMapa' => 0, 'ao' => 0, 'mn' => 0, 'pn' => 0, 'dc' => 0, 'autre' => 0, 'total' => 0, 'sad' => 0, 'accordcadre' => '0', 'totalAccordCadreSad' => 0, 'grandTotal' => 0];
        }
        $arrayResultat = self::populateData($statement, $arrayResultat, $groupByTypeProcedure, $dontCountMapas);
        Atexo_Db::closeCommon();

        return $arrayResultat;
    }

    public function getNbreAvis($critere, $idTypeAvis, $groupByTypeProcedure = false, $dontCountMapas = false)
    {
        $sql = ' SELECT count(consultation.id) as nombreConsultation, consultation.id_type_procedure_org as id_type_procedure, '
           .' ao, mn, pn, dc, autre, Type_Procedure_Organisme.mapa, id_montant_mapa, sad, accord_cadre as accordcadre  '
            .' FROM consultation left join Type_Procedure_Organisme on consultation.id_type_procedure_org=Type_Procedure_Organisme.id_type_procedure '
            .' AND consultation.organisme=Type_Procedure_Organisme.organisme '
            ." WHERE consultation.organisme='".$critere->getOrganisme()."' ";

        if ($critere) {
            if ($critere->getIdService()) {
                $sql .= ' AND (service_id '.(new Atexo_Db())->quote($critere->getIdService()).')';
            }
            if ($critere->getDateMiseEnLigneStart()) {
                $sql .= ' AND date_mise_en_ligne_calcule >= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneStart()).' 00:00\' ';
            }
            if ($critere->getDateMiseEnLigneEnd()) {
                $sql .= ' AND date_mise_en_ligne_calcule <= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneEnd()).' 23:59\' ';
            }
            if ($critere->getDateMiseEnLigneStart() || $critere->getDateMiseEnLigneEnd()) {
                $sql .= ' AND date_mise_en_ligne_calcule is not null ';
            }
            if ($critere->getDateRemisePlisStart()) {
                $sql .= ' AND datefin >= \''.(new Atexo_Db())->quote($critere->getDateRemisePlisStart()).' 00:00\' ';
            }
            if ($critere->getDateRemisePlisEnd()) {
                $sql .= ' AND datefin <= \''.(new Atexo_Db())->quote($critere->getDateRemisePlisEnd()).' 23:59\' ';
            }
            if ($critere->getDateRemisePlisStart() || $critere->getDateRemisePlisEnd()) {
                $sql .= ' AND datefin is not null ';
            }
            if ($critere->getDateMiseEnLigneAujourdhui()) {
                $sql .= ' AND consultation.date_mise_en_ligne_calcule<= NOW() ';
                $sql .= ' AND consultation.datefin> NOW() ';
            }

            $queryClause = self::getClauseQuery($critere);
            $sql .= ' AND '.$queryClause;
        }

        $sql .= ' AND id_type_avis = '.$idTypeAvis;

        $sql .= ' GROUP BY id_type_procedure ';
        $statement1 = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_ASSOC);

        if ($groupByTypeProcedure) {
            if ($dontCountMapas) {
                $typesProcedures = (new Atexo_Consultation_ProcedureType())->rerieveTypeProcedureSaufMapa(true, false, $critere->getOrganisme());
                $arrayResultat = Atexo_Util::initializeArray($typesProcedures, 0);
                $arrayResultat['MAPA_INF'] = 0;
                $arrayResultat['MAPA_SUPP'] = 0;
            } else {
                $typesProcedures = (new Atexo_Consultation_ProcedureType())->rerieveAllProcedure(true, false, $critere->getOrganisme());
                $arrayResultat = Atexo_Util::initializeArray($typesProcedures, 0);
            }
        } else {
            $arrayResultat = ['mapaInf' => 0, 'mapaSupp' => 0, 'totalMapa' => 0, 'ao' => 0, 'mn' => 0, 'pn' => 0, 'dc' => 0, 'autre' => 0, 'total' => 0, 'sad' => 0, 'accordcadre' => '0', 'totalAccordCadreSad' => 0, 'grandTotal' => 0];
        }
        foreach ($statement1 as $row) {
            if ($groupByTypeProcedure) {
                if ($dontCountMapas) {
                    if ('1' == $row['mapa']) {
                        if ($row['id_montant_mapa'] == Atexo_Config::getParameter('MONTANT_MAPA_INF_90')) {
                            $arrayResultat['MAPA_INF'] += $row['nombreConsultation'];
                        } else {
                            $arrayResultat['MAPA_SUPP'] += $row['nombreConsultation'];
                        }
                    } else {
                        $arrayResultat[$row['id_type_procedure']] = $row['nombreConsultation'];
                    }
                } else {
                    $arrayResultat[$row['id_type_procedure']] = $row['nombreConsultation'];
                }
            } else {
                if ('1' == $row['mapa']) {
                    if ($row['id_montant_mapa'] == Atexo_Config::getParameter('MONTANT_MAPA_INF_90')) {
                        $arrayResultat['mapaInf'] += $row['nombreConsultation'];
                    } else {
                        $arrayResultat['mapaSupp'] += $row['nombreConsultation'];
                    }
                    $arrayResultat['totalMapa'] += $row['nombreConsultation'];
                    $arrayResultat['grandTotal'] += $row['nombreConsultation'];
                } else {
                    if ('1' == $row['ao']) {
                        $arrayResultat['ao'] += $row['nombreConsultation'];
                        $arrayResultat['total'] += $row['nombreConsultation'];
                        $arrayResultat['grandTotal'] += $row['nombreConsultation'];
                    } elseif ('1' == $row['mn']) {
                        $arrayResultat['mn'] += $row['nombreConsultation'];
                        $arrayResultat['total'] += $row['nombreConsultation'];
                        $arrayResultat['grandTotal'] += $row['nombreConsultation'];
                    } elseif ('1' == $row['pn']) {
                        $arrayResultat['pn'] += $row['nombreConsultation'];
                        $arrayResultat['total'] += $row['nombreConsultation'];
                        $arrayResultat['grandTotal'] += $row['nombreConsultation'];
                    } elseif ('1' == $row['dc']) {
                        $arrayResultat['dc'] += $row['nombreConsultation'];
                        $arrayResultat['total'] += $row['nombreConsultation'];
                        $arrayResultat['grandTotal'] += $row['nombreConsultation'];
                    } elseif ('1' == $row['autre']) {
                        $arrayResultat['autre'] += $row['nombreConsultation'];
                        $arrayResultat['total'] += $row['nombreConsultation'];
                        $arrayResultat['grandTotal'] += $row['nombreConsultation'];
                    } elseif ('1' == $row['accordcadre']) {
                        $arrayResultat['accordcadre'] += $row['nombreConsultation'];
                        $arrayResultat['totalAccordCadreSad'] += $row['nombreConsultation'];
                        $arrayResultat['grandTotal'] += $row['nombreConsultation'];
                    } elseif ('1' == $row['autre']) {
                        $arrayResultat['autre'] += $row['nombreConsultation'];
                        $arrayResultat['total'] += $row['nombreConsultation'];
                        $arrayResultat['grandTotal'] += $row['nombreConsultation'];
                    } elseif ('1' == $row['sad']) {
                        $arrayResultat['sad'] += $row['nombreConsultation'];
                        $arrayResultat['totalAccordCadreSad'] += $row['nombreConsultation'];
                        $arrayResultat['grandTotal'] += $row['nombreConsultation'];
                    }
                }
            }
        }
        Atexo_Db::closeCommon();

        return $arrayResultat;
    }

    public function getNombreDces($critere, $groupByTypeProcedure = false, $dontCountMapas = false)
    {
        $sql = ' SELECT  count(DISTINCT consultation.id) as nombreConsultation, consultation.id_type_procedure_org as id_type_procedure, '
           .' ao, mn, pn, dc, autre, Type_Procedure_Organisme.mapa, id_montant_mapa '
            .' , sad, accord_cadre as accordcadre '
            .' FROM consultation left join Type_Procedure_Organisme on consultation.id_type_procedure_org=Type_Procedure_Organisme.id_type_procedure '
            .' AND consultation.organisme=Type_Procedure_Organisme.organisme, '
            .'      DCE'
            ." WHERE consultation.id=DCE.consultation_id AND consultation.organisme=DCE.organisme AND consultation.organisme='".$critere->getOrganisme()."' ";

        if ($critere) {
            if ($critere->getIdService()) {
                $sql .= ' AND (service_id '.(new Atexo_Db())->quote($critere->getIdService()).')';
            }
            if ($critere->getDateMiseEnLigneStart()) {
                $sql .= ' AND date_mise_en_ligne_calcule >= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneStart()).' 00:00\' ';
            }
            if ($critere->getDateMiseEnLigneEnd()) {
                $sql .= ' AND date_mise_en_ligne_calcule <= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneEnd()).' 23:59\' ';
            }
            if ($critere->getDateMiseEnLigneStart() || $critere->getDateMiseEnLigneEnd()) {
                $sql .= ' AND date_mise_en_ligne_calcule is not null ';
            }
            if ($critere->getDateRemisePlisStart()) {
                $sql .= ' AND datefin >= \''.(new Atexo_Db())->quote($critere->getDateRemisePlisStart()).' 00:00\' ';
            }
            if ($critere->getDateRemisePlisEnd()) {
                $sql .= ' AND datefin <= \''.(new Atexo_Db())->quote($critere->getDateRemisePlisEnd()).' 23:59\' ';
            }
            if ($critere->getDateRemisePlisStart() || $critere->getDateRemisePlisEnd()) {
                $sql .= ' AND datefin is not null ';
            }
            if ($critere->getDateMiseEnLigneAujourdhui()) {
                $sql .= ' AND date_mise_en_ligne_calcule<= NOW() ';
                $sql .= ' AND datefin> NOW() ';
            }

            $queryClause = self::getClauseQuery($critere);
            $sql .= ' AND '.$queryClause;
        }
        $sql .= ' GROUP BY id_type_procedure ';
        $statement1 = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_ASSOC);

        if ($groupByTypeProcedure) {
            if ($dontCountMapas) {
                $typesProcedures = (new Atexo_Consultation_ProcedureType())->rerieveTypeProcedureSaufMapa(true, false, $critere->getOrganisme());
                $arrayResultat = Atexo_Util::initializeArray($typesProcedures, 0);
                $arrayResultat['MAPA_INF'] = 0;
                $arrayResultat['MAPA_SUPP'] = 0;
            } else {
                $typesProcedures = (new Atexo_Consultation_ProcedureType())->rerieveAllProcedure(true, false, $critere->getOrganisme());
                $arrayResultat = Atexo_Util::initializeArray($typesProcedures, 0);
            }
        } else {
            $arrayResultat = ['mapaInf' => 0, 'mapaSupp' => 0, 'totalMapa' => 0, 'ao' => 0, 'mn' => 0, 'pn' => 0, 'dc' => 0, 'autre' => 0, 'total' => 0, 'sad' => 0, 'accordcadre' => '0', 'totalAccordCadreSad' => 0, 'grandTotal' => 0];
        }

        $arrayResultat = self::populateData($statement1, $arrayResultat, $groupByTypeProcedure, $dontCountMapas);
        Atexo_Db::closeCommon();

        return $arrayResultat;
    }

    public function getNombreTelechargement($critere, $groupByTypeProcedure = false, $dontCountMapas = false, $nombreConsultationsConcernes = true)
    {
        $sql = ' SELECT ';
        if ($nombreConsultationsConcernes) {
            $sql .= 'count(consultation.id) as nombreConsultation ';
        } else {
            $sql .= 'count(Telechargement.id) as nombreConsultation ';
        }
        $sql .= ', consultation.id_type_procedure_org as id_type_procedure, ao, mn, pn, dc, autre, sad, accord_cadre as accordcadre, Type_Procedure_Organisme.mapa, id_montant_mapa '
            .' FROM consultation left join Type_Procedure_Organisme on consultation.id_type_procedure_org=Type_Procedure_Organisme.id_type_procedure, '
            .'      Telechargement'
            .' WHERE consultation.id=Telechargement.consultation_id '
            .' AND consultation.organisme=Telechargement.organisme '
            ." AND consultation.organisme='".(new Atexo_Db())->quote($critere->getOrganisme())."' "
            .' AND Type_Procedure_Organisme.organisme = consultation.organisme';

        if ($critere) {
            if ($critere->getIdService()) {
                $sql .= ' AND (service_id '.(new Atexo_Db())->quote($critere->getIdService()).')';
            }
            if ($critere->getDateMiseEnLigneStart()) {
                $sql .= ' AND date_mise_en_ligne_calcule >= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneStart()).' 00:00\' ';
            }
            if ($critere->getDateMiseEnLigneEnd()) {
                $sql .= ' AND date_mise_en_ligne_calcule <= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneEnd()).' 23:59\' ';
            }
            if ($critere->getDateMiseEnLigneStart() || $critere->getDateMiseEnLigneEnd()) {
                $sql .= ' AND date_mise_en_ligne_calcule is not null ';
            }
            if ($critere->getDateRemisePlisStart()) {
                $sql .= ' AND datefin >= \''.(new Atexo_Db())->quote($critere->getDateRemisePlisStart()).' 00:00\' ';
            }
            if ($critere->getDateRemisePlisEnd()) {
                $sql .= ' AND datefin <= \''.(new Atexo_Db())->quote($critere->getDateRemisePlisEnd()).' 23:59\' ';
            }
            if ($critere->getDateRemisePlisStart() || $critere->getDateRemisePlisEnd()) {
                $sql .= ' AND datefin is not null ';
            }
            if (null !== $critere->getReponsesAutotisee()) {
                if ($critere->getReponsesAutotisee()) {
                    $sql .= " AND autoriser_reponse_electronique='1' ";
                } else {
                    $sql .= " AND autoriser_reponse_electronique!='1' ";
                }
            }
            if (null !== $critere->getReponsesAvecSignature()) {
                if ($critere->getReponsesAvecSignature()) {
                    $sql .= " AND signature_offre='1' ";
                } else {
                    $sql .= " AND signature_offre!='1' ";
                }
            }
            if ($critere->getCategorieConsultation()) {
                $sql .= " AND categorie='".(new Atexo_Db())->quote($critere->getCategorieConsultation())."'";
            }

            if ($critere->getDateDepotRegistre()) {
                $sql .= " AND datetelechargement>='".(new Atexo_Db())->quote($critere->getDateDepotRegistre())."'";
            }
            if ($critere->getDateMiseEnLigneAujourdhui()) {
                $sql .= ' AND date_mise_en_ligne_calcule<= NOW() ';
                $sql .= ' AND datefin> NOW() ';
            }

            $queryClause = self::getClauseQuery($critere);
            $sql .= ' AND '.$queryClause;
        }

        $sql .= ' GROUP BY  id_type_procedure ';

        $statement1 = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_ASSOC);

        if ($groupByTypeProcedure) {
            if ($dontCountMapas) {
                $typesProcedures = (new Atexo_Consultation_ProcedureType())->rerieveTypeProcedureSaufMapa(true, false, $critere->getOrganisme());
                $arrayResultat = Atexo_Util::initializeArray($typesProcedures, 0);
                $arrayResultat['MAPA_INF'] = 0;
                $arrayResultat['MAPA_SUPP'] = 0;
            } else {
                $typesProcedures = (new Atexo_Consultation_ProcedureType())->rerieveAllProcedure(true, false, $critere->getOrganisme());
                $arrayResultat = Atexo_Util::initializeArray($typesProcedures, 0);
            }
        } else {
            $arrayResultat = ['mapaInf' => 0, 'mapaSupp' => 0, 'totalMapa' => 0, 'ao' => 0, 'mn' => 0, 'pn' => 0, 'dc' => 0, 'autre' => 0, 'total' => 0, 'sad' => 0, 'accordcadre' => '0', 'totalAccordCadreSad' => 0, 'grandTotal' => 0];
        }

        $arrayResultat = self::populateData($statement1, $arrayResultat, $groupByTypeProcedure, $dontCountMapas);
        Atexo_Db::closeCommon();

        return $arrayResultat;
    }

    public function getNombreQuestionPosees($critere, $groupByTypeProcedure = false, $dontCountMapas = false, $nombreConsultationsConcernes = true)
    {
        $sql = ' SELECT ';
        if ($nombreConsultationsConcernes) {
            $sql .= 'count(consultation.id) as nombreConsultation ';
        } else {
            $sql .= 'count(questions_dce.id) as nombreConsultation ';
        }
        $sql .= ', consultation.id_type_procedure_org as id_type_procedure, ao, mn,pn, dc, autre, Type_Procedure_Organisme.mapa, id_montant_mapa, sad, accord_cadre as accordcadre '
            .' FROM consultation left join Type_Procedure_Organisme on consultation.id_type_procedure_org=Type_Procedure_Organisme.id_type_procedure '.
              'AND consultation.organisme=Type_Procedure_Organisme.organisme, '
            .'      questions_dce'
            .' WHERE consultation.id=questions_dce.consultation_id '.
                   'AND consultation.organisme=questions_dce.organisme '.
                   "AND consultation_externe!='1' ".
                   "AND consultation.organisme='".(new Atexo_Db())->quote($critere->getOrganisme())."' ";

        if ($critere) {
            if ($critere->getIdService()) {
                $sql .= ' AND (service_id '.(new Atexo_Db())->quote($critere->getIdService()).')';
            }
            if ($critere->getDateMiseEnLigneStart()) {
                $sql .= ' AND date_mise_en_ligne_calcule >= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneStart()).' 00:00\' ';
            }
            if ($critere->getDateMiseEnLigneEnd()) {
                $sql .= ' AND date_mise_en_ligne_calcule <= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneEnd()).' 23:59\' ';
            }
            if ($critere->getConsultationEnLigne() || $critere->getDateMiseEnLigneStart() || $critere->getDateMiseEnLigneEnd()) {
                $sql .= " AND date_mise_en_ligne_calcule is not null AND date_mise_en_ligne_calcule NOT LIKE '0000-00-00%' ";
            }
            if ($critere->getDateRemisePlisStart()) {
                $sql .= ' AND datefin >= \''.(new Atexo_Db())->quote($critere->getDateRemisePlisStart()).' 00:00\' ';
            }
            if ($critere->getDateRemisePlisEnd()) {
                $sql .= ' AND datefin <= \''.(new Atexo_Db())->quote($critere->getDateRemisePlisEnd()).' 23:59\' ';
            }
            if ($critere->getDateRemisePlisStart() || $critere->getDateRemisePlisEnd()) {
                $sql .= ' AND datefin is not null ';
            }

            if ($critere->getDateDepotRegistre()) {
                $sql .= "AND date_depot>='".(new Atexo_Db())->quote($critere->getDateDepotRegistre())."'";
            }

            if ($critere->getConsultationExterne()) {
                $sql .= " AND consultation_externe='1' ";
            } else {
                $sql .= " AND consultation_externe!='1' ";
            }
            if ($critere->getDateMiseEnLigneAujourdhui()) {
                $sql .= ' AND date_mise_en_ligne_calcule<= NOW() ';
                $sql .= ' AND datefin> NOW() ';
            }

            if ($critere->getIdTypeAvis()) {
                $sql .= " AND consultation.id_type_avis='".(new Atexo_Db())->quote($critere->getIdTypeAvis())."' ";
            }
            $queryClause = self::getClauseQuery($critere);
            $sql .= ' AND '.$queryClause;
        }

        $sql .= ' GROUP BY id_type_procedure ';

        $statement1 = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_ASSOC);

        if ($groupByTypeProcedure) {
            if ($dontCountMapas) {
                $typesProcedures = (new Atexo_Consultation_ProcedureType())->rerieveTypeProcedureSaufMapa(true, false, $critere->getOrganisme());
                $arrayResultat = Atexo_Util::initializeArray($typesProcedures, 0);
                $arrayResultat['MAPA_INF'] = 0;
                $arrayResultat['MAPA_SUPP'] = 0;
            } else {
                $typesProcedures = (new Atexo_Consultation_ProcedureType())->rerieveAllProcedure(true, false, $critere->getOrganisme());
                $arrayResultat = Atexo_Util::initializeArray($typesProcedures, 0);
            }
        } else {
            $arrayResultat = ['mapaInf' => 0, 'mapaSupp' => 0, 'totalMapa' => 0, 'ao' => 0, 'mn' => 0, 'pn' => 0, 'dc' => 0, 'autre' => 0, 'total' => 0, 'sad' => 0, 'accordcadre' => '0', 'totalAccordCadreSad' => 0, 'grandTotal' => 0];
        }
        foreach ($statement1 as $row) {
            if ($groupByTypeProcedure) {
                if ($dontCountMapas) {
                    if ('1' == $row['mapa']) {
                        if ($row['id_montant_mapa'] == Atexo_Config::getParameter('MONTANT_MAPA_INF_90')) {
                            $arrayResultat['MAPA_INF'] += $row['nombreConsultation'];
                        } else {
                            $arrayResultat['MAPA_SUPP'] += $row['nombreConsultation'];
                        }
                    } else {
                        $arrayResultat[$row['id_type_procedure']] = $row['nombreConsultation'];
                    }
                } else {
                    $arrayResultat[$row['id_type_procedure']] = $row['nombreConsultation'];
                }
            } else {
                if ('1' == $row['mapa']) {
                    if ($row['id_montant_mapa'] == Atexo_Config::getParameter('MONTANT_MAPA_INF_90')) {
                        $arrayResultat['mapaInf'] += $row['nombreConsultation'];
                    } else {
                        $arrayResultat['mapaSupp'] += $row['nombreConsultation'];
                    }
                    $arrayResultat['totalMapa'] += $row['nombreConsultation'];
                    $arrayResultat['grandTotal'] += $row['nombreConsultation'];
                } else {
                    if ('1' == $row['ao']) {
                        $arrayResultat['ao'] += $row['nombreConsultation'];
                        $arrayResultat['total'] += $row['nombreConsultation'];
                        $arrayResultat['grandTotal'] += $row['nombreConsultation'];
                    } elseif ('1' == $row['mn']) {
                        $arrayResultat['mn'] += $row['nombreConsultation'];
                        $arrayResultat['total'] += $row['nombreConsultation'];
                        $arrayResultat['grandTotal'] += $row['nombreConsultation'];
                    } elseif ('1' == $row['pn']) {
                        $arrayResultat['pn'] += $row['nombreConsultation'];
                        $arrayResultat['total'] += $row['nombreConsultation'];
                        $arrayResultat['grandTotal'] += $row['nombreConsultation'];
                    } elseif ('1' == $row['dc']) {
                        $arrayResultat['dc'] += $row['nombreConsultation'];
                        $arrayResultat['total'] += $row['nombreConsultation'];
                        $arrayResultat['grandTotal'] += $row['nombreConsultation'];
                    } elseif ('1' == $row['sad']) {
                        $arrayResultat['sad'] += $row['nombreConsultation'];
                        $arrayResultat['totalAccordCadreSad'] += $row['nombreConsultation'];
                        $arrayResultat['grandTotal'] += $row['nombreConsultation'];
                    } elseif ('1' == $row['accordcadre']) {
                        $arrayResultat['accordcadre'] += $row['nombreConsultation'];
                        $arrayResultat['totalAccordCadreSad'] += $row['nombreConsultation'];
                        $arrayResultat['grandTotal'] += $row['nombreConsultation'];
                    } elseif ('1' == $row['autre']) {
                        $arrayResultat['autre'] += $row['nombreConsultation'];
                        $arrayResultat['total'] += $row['nombreConsultation'];
                        $arrayResultat['grandTotal'] += $row['nombreConsultation'];
                    }
                }
            }
        }
        Atexo_Db::closeCommon();

        return $arrayResultat;
    }

    public function getNombreReponsesElectronique($critere, $groupByTypeProcedure = false, $dontCountMapas = false, $nombreConsultationsConcernes = true)
    {
        $sql = ' SELECT ';

        if ($nombreConsultationsConcernes) {
            $sql .= 'count(consultation.id) as nombreConsultation ';
        } else {
            $sql .= 'count(Offres.id) as nombreConsultation ';
        }

        $sql .= ' , consultation.id_type_procedure_org as id_type_procedure, ao, mn, pn, dc, autre, Type_Procedure_Organisme.mapa, id_montant_mapa, sad, accord_cadre as accordcadre '
            .' FROM consultation left join Type_Procedure_Organisme on consultation.id_type_procedure_org=Type_Procedure_Organisme.id_type_procedure '
            .' AND consultation.organisme=Type_Procedure_Organisme.organisme, '
            .'      Offres'
            .' WHERE consultation.id=Offres.consultation_id '
            .' AND consultation.organisme=Offres.organisme '
            ." AND consultation.organisme='".(new Atexo_Db())->quote($critere->getOrganisme())."' "
            ." AND Offres.statut_offres != '".Atexo_Config::getParameter('STATUT_ENV_BROUILLON')."' ";

        if ($critere) {
            if ($critere->getIdService()) {
                $sql .= ' AND (service_id '.(new Atexo_Db())->quote($critere->getIdService()).')';
            }
            if ($critere->getDateMiseEnLigneStart()) {
                $sql .= ' AND date_mise_en_ligne_calcule >= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneStart()).' 00:00\' ';
            }
            if ($critere->getDateMiseEnLigneEnd()) {
                $sql .= ' AND date_mise_en_ligne_calcule <= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneEnd()).' 23:59\' ';
            }
            if ($critere->getDateMiseEnLigneStart() || $critere->getDateMiseEnLigneEnd()) {
                $sql .= ' AND date_mise_en_ligne_calcule is not null ';
            }
            if ($critere->getDateRemisePlisStart()) {
                $sql .= ' AND datefin >= \''.(new Atexo_Db())->quote($critere->getDateRemisePlisStart()).' 00:00\' ';
            }
            if ($critere->getDateRemisePlisEnd()) {
                $sql .= ' AND datefin <= \''.(new Atexo_Db())->quote($critere->getDateRemisePlisEnd()).' 23:59\' ';
            }
            if ($critere->getDateRemisePlisStart() || $critere->getDateRemisePlisEnd()) {
                $sql .= ' AND datefin is not null ';
            }

            if (null !== $critere->getReponsesAutotisee()) {
                if ($critere->getReponsesAutotisee()) {
                    $sql .= " AND autoriser_reponse_electronique='1' ";
                } else {
                    $sql .= " AND autoriser_reponse_electronique!='1' ";
                }
            }
            if (null !== $critere->getReponsesAvecSignature()) {
                if ($critere->getReponsesAvecSignature()) {
                    $sql .= " AND signature_offre='1' ";
                } else {
                    $sql .= " AND signature_offre!='1' ";
                }
            }

            if ($critere->getCategorieConsultation()) {
                $sql .= " AND categorie='".(new Atexo_Db())->quote($critere->getCategorieConsultation())."'";
            }
            if ($critere->getDateMiseEnLigneAujourdhui()) {
                $sql .= ' AND date_mise_en_ligne_calcule<= NOW() ';
                $sql .= ' AND datefin> NOW() ';
            }

            $queryClause = self::getClauseQuery($critere);
            $sql .= ' AND '.$queryClause;
        }

        $sql .= ' GROUP BY id_type_procedure ';
        $statement1 = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_ASSOC);

        if ($groupByTypeProcedure) {
            if ($dontCountMapas) {
                $typesProcedures = (new Atexo_Consultation_ProcedureType())->rerieveTypeProcedureSaufMapa(true, false, $critere->getOrganisme());
                $arrayResultat = Atexo_Util::initializeArray($typesProcedures, 0);
                $arrayResultat['MAPA_INF'] = 0;
                $arrayResultat['MAPA_SUPP'] = 0;
            } else {
                $typesProcedures = (new Atexo_Consultation_ProcedureType())->rerieveAllProcedure(true, false, $critere->getOrganisme());
                $arrayResultat = Atexo_Util::initializeArray($typesProcedures, 0);
            }
        } else {
            $arrayResultat = ['mapaInf' => 0, 'mapaSupp' => 0, 'totalMapa' => 0, 'ao' => 0, 'mn' => 0, 'pn' => 0, 'dc' => 0, 'autre' => 0, 'total' => 0, 'sad' => 0, 'accordcadre' => '0', 'totalAccordCadreSad' => 0, 'grandTotal' => 0];
        }

        $arrayResultat = self::populateData($statement1, $arrayResultat, $groupByTypeProcedure, $dontCountMapas);
        Atexo_Db::closeCommon();

        return $arrayResultat;
    }

    public function getNombreConsultationSansSuite($critere, $groupByTypeProcedure = false, $dontCountMapas = false)
    {
        $sql = ' SELECT count(consultation.id) as nombreConsultation, consultation.id_type_procedure_org as id_type_procedure, lot, ao, mn, dc, autre, Type_Procedure_Organisme.mapa, id_montant_mapa '
            .' , sad, accord_cadre as accordcadre '
            .' FROM consultation left join Type_Procedure_Organisme on consultation.id_type_procedure_org=Type_Procedure_Organisme.id_type_procedure '
            .'    AND consultation.organisme=Type_Procedure_Organisme.organisme ,   DecisionLot '
        ." WHERE consultation.id=DecisionLot.consultation_id AND consultation.organisme=DecisionLot.organisme AND consultation.organisme='".(new Atexo_Db())->quote($critere->getOrganisme())."' "
            ."       AND id_type_decision='".Atexo_Config::getParameter('DECISION_DECLARATION_SANS_SUITE')."'";

        if ($critere) {
            if ($critere->getIdService()) {
                $sql .= ' AND (service_id '.(new Atexo_Db())->quote($critere->getIdService()).')';
            }
            if ($critere->getDateMiseEnLigneStart()) {
                $sql .= ' AND date_mise_en_ligne_calcule >= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneStart()).' 00:00\' ';
            }
            if ($critere->getDateMiseEnLigneEnd()) {
                $sql .= ' AND date_mise_en_ligne_calcule <= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneEnd()).' 23:59\' ';
            }
            if ($critere->getDateMiseEnLigneStart() || $critere->getDateMiseEnLigneEnd()) {
                $sql .= ' AND date_mise_en_ligne_calcule is not null ';
            }
            if ($critere->getDateRemisePlisStart()) {
                $sql .= ' AND datefin >= \''.(new Atexo_Db())->quote($critere->getDateRemisePlisStart()).' 00:00\' ';
            }
            if ($critere->getDateRemisePlisEnd()) {
                $sql .= ' AND datefin <= \''.(new Atexo_Db())->quote($critere->getDateRemisePlisEnd()).' 23:59\' ';
            }
            if ($critere->getDateRemisePlisStart() || $critere->getDateRemisePlisEnd()) {
                $sql .= ' AND datefin is not null ';
            }
            if ($critere->getDateMiseEnLigneAujourdhui()) {
                $sql .= ' AND date_mise_en_ligne_calcule<= NOW() ';
                $sql .= ' AND datefin> NOW() ';
            }

            $queryClause = self::getClauseQuery($critere);
            $sql .= ' AND '.$queryClause;
        }

        $sql .= ' GROUP BY id_type_procedure ';
        $statement1 = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_ASSOC);

        if ($groupByTypeProcedure) {
            if ($dontCountMapas) {
                $typesProcedures = (new Atexo_Consultation_ProcedureType())->rerieveTypeProcedureSaufMapa(true, false, $critere->getOrganisme());
                $arrayResultat = Atexo_Util::initializeArray($typesProcedures, 0);
                $arrayResultat['MAPA_INF'] = 0;
                $arrayResultat['MAPA_SUPP'] = 0;
            } else {
                $typesProcedures = (new Atexo_Consultation_ProcedureType())->rerieveAllProcedure(true, false, $critere->getOrganisme());
                $arrayResultat = Atexo_Util::initializeArray($typesProcedures, 0);
            }
        } else {
            $arrayResultat = ['mapaInf' => 0, 'mapaSupp' => 0, 'totalMapa' => 0, 'ao' => 0, 'mn' => 0, 'dc' => 0, 'autre' => 0, 'total' => 0, 'sad' => 0, 'accordcadre' => '0', 'totalAccordCadreSad' => 0, 'grandTotal' => 0];
        }

        $arrayResultat = self::populateData($statement1, $arrayResultat, $groupByTypeProcedure, $dontCountMapas);
        Atexo_Db::closeCommon();

        return $arrayResultat;
    }

    public function getNombreConsultationInfructueuse($critere, $groupByTypeProcedure = false, $dontCountMapas = false)
    {
        $sql = ' SELECT count(consultation.id) as nombreConsultation, consultation.id_type_procedure_org as id_type_procedure, lot, '
           .'ao, mn, dc, autre, Type_Procedure_Organisme.mapa, id_montant_mapa '
            .' , sad, accord_cadre as accordcadre '
            .' FROM consultation left join Type_Procedure_Organisme on consultation.id_type_procedure_org=Type_Procedure_Organisme.id_type_procedure '
            .'  AND consultation.organisme=Type_Procedure_Organisme.organisme,     DecisionLot '
        ." WHERE consultation.id=DecisionLot.consultation_id AND consultation.organisme=DecisionLot.organisme AND consultation.organisme='".(new Atexo_Db())->quote($critere->getOrganisme())."' "
            ."    AND id_type_decision='".Atexo_Config::getParameter('DECISION_DECLARATION_INFRUCTUEUX')."'";

        if ($critere) {
            if ($critere->getIdService()) {
                $sql .= ' AND (service_id '.(new Atexo_Db())->quote($critere->getIdService()).')';
            }
            if ($critere->getDateMiseEnLigneStart()) {
                $sql .= ' AND date_mise_en_ligne_calcule >= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneStart()).' 00:00\' ';
            }
            if ($critere->getDateMiseEnLigneEnd()) {
                $sql .= ' AND date_mise_en_ligne_calcule <= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneEnd()).' 23:59\' ';
            }
            if ($critere->getDateMiseEnLigneStart() || $critere->getDateMiseEnLigneEnd()) {
                $sql .= ' AND date_mise_en_ligne_calcule is not null ';
            }
            if ($critere->getDateRemisePlisStart()) {
                $sql .= ' AND datefin >= \''.(new Atexo_Db())->quote($critere->getDateRemisePlisStart()).' 00:00\' ';
            }
            if ($critere->getDateRemisePlisEnd()) {
                $sql .= ' AND datefin <= \''.(new Atexo_Db())->quote($critere->getDateRemisePlisEnd()).' 23:59\' ';
            }
            if ($critere->getDateRemisePlisStart() || $critere->getDateRemisePlisEnd()) {
                $sql .= ' AND datefin is not null ';
            }
            if ($critere->getDateMiseEnLigneAujourdhui()) {
                $sql .= ' AND date_mise_en_ligne_calcule<= NOW() ';
                $sql .= ' AND datefin> NOW() ';
            }

            $queryClause = self::getClauseQuery($critere);
            $sql .= ' AND '.$queryClause;
        }

        $sql .= ' GROUP BY  id_type_procedure ';
        $statement1 = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_ASSOC);

        if ($groupByTypeProcedure) {
            if ($dontCountMapas) {
                $typesProcedures = (new Atexo_Consultation_ProcedureType())->rerieveTypeProcedureSaufMapa(true, false, $critere->getOrganisme());
                $arrayResultat = Atexo_Util::initializeArray($typesProcedures, 0);
                $arrayResultat['MAPA_INF'] = 0;
                $arrayResultat['MAPA_SUPP'] = 0;
            } else {
                $typesProcedures = (new Atexo_Consultation_ProcedureType())->rerieveAllProcedure(true, false, $critere->getOrganisme());
                $arrayResultat = Atexo_Util::initializeArray($typesProcedures, 0);
            }
        } else {
            $arrayResultat = ['mapaInf' => 0, 'mapaSupp' => 0, 'totalMapa' => 0, 'ao' => 0, 'mn' => 0, 'dc' => 0, 'autre' => 0, 'total' => 0, 'sad' => 0, 'accordcadre' => '0', 'totalAccordCadreSad' => 0, 'grandTotal' => 0];
        }

        $arrayResultat = self::populateData($statement1, $arrayResultat, $groupByTypeProcedure, $dontCountMapas);
        Atexo_Db::closeCommon();

        return $arrayResultat;
    }

    public function getNombreProcedureSansReponse($critere, $groupByTypeProcedure = false, $dontCountMapas = false)
    {
        $sql = ' SELECT count(consultation.id) as nombreConsultation, consultation.id_type_procedure_org ,ao, mn, pn, dc, autre, Type_Procedure_Organisme.mapa, id_montant_mapa '
            .' , sad, accord_cadre as accordcadre, Type_Procedure_Organisme.id_type_procedure as idTypeProcedureOrg '
            .' FROM consultation left join Type_Procedure_Organisme on consultation.id_type_procedure_org=Type_Procedure_Organisme.id_type_procedure'
            .'  AND consultation.organisme=Type_Procedure_Organisme.organisme '
            ." WHERE  consultation.id not in (Select consultation_id from Offres where organisme='".$critere->getOrganisme()
            ."' AND Offres.statut_offres != '".Atexo_Config::getParameter('STATUT_ENV_BROUILLON')."' ) AND  consultation.organisme='".(new Atexo_Db())->quote($critere->getOrganisme())."' ";

        if ($critere) {
            if ($critere->getIdService()) {
                $sql .= ' AND (service_id '.(new Atexo_Db())->quote($critere->getIdService()).')';
            }
            if ($critere->getDateMiseEnLigneStart()) {
                $sql .= ' AND date_mise_en_ligne_calcule >= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneStart()).' 00:00\' ';
            }
            if ($critere->getDateMiseEnLigneEnd()) {
                $sql .= ' AND date_mise_en_ligne_calcule <= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneEnd()).' 23:59\' ';
            }
            if ($critere->getDateMiseEnLigneStart() || $critere->getDateMiseEnLigneEnd()) {
                $sql .= ' AND date_mise_en_ligne_calcule is not null ';
            }
            if ($critere->getDateRemisePlisStart()) {
                $sql .= ' AND datefin >= \''.(new Atexo_Db())->quote($critere->getDateRemisePlisStart()).' 00:00\' ';
            }
            if ($critere->getDateRemisePlisEnd()) {
                $sql .= ' AND datefin <= \''.(new Atexo_Db())->quote($critere->getDateRemisePlisEnd()).' 23:59\' ';
            }
            if ($critere->getDateRemisePlisStart() || $critere->getDateRemisePlisEnd()) {
                $sql .= ' AND datefin is not null ';
            }
            if ($critere->getDateMiseEnLigneAujourdhui()) {
                $sql .= ' AND date_mise_en_ligne_calcule<= NOW() ';
                $sql .= ' AND datefin> NOW() ';
            }

            $queryClause = self::getClauseQuery($critere);
            $sql .= ' AND '.$queryClause;
        }

        $sql .= ' GROUP BY idTypeProcedureOrg ';
        $statement1 = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_ASSOC);

        if ($groupByTypeProcedure) {
            if ($dontCountMapas) {
                $typesProcedures = (new Atexo_Consultation_ProcedureType())->rerieveTypeProcedureSaufMapa(true, false, $critere->getOrganisme());
                $arrayResultat = Atexo_Util::initializeArray($typesProcedures, 0);
                $arrayResultat['MAPA_INF'] = 0;
                $arrayResultat['MAPA_SUPP'] = 0;
            } else {
                $typesProcedures = (new Atexo_Consultation_ProcedureType())->rerieveAllProcedure(true, false, $critere->getOrganisme());
                $arrayResultat = Atexo_Util::initializeArray($typesProcedures, 0);
            }
        } else {
            $arrayResultat = ['mapaInf' => 0, 'mapaSupp' => 0, 'totalMapa' => 0, 'ao' => 0, 'mn' => 0, 'pn' => 0, 'dc' => 0, 'autre' => 0, 'total' => 0, 'sad' => 0, 'accordcadre' => '0', 'totalAccordCadreSad' => 0, 'grandTotal' => 0];
        }

        $arrayResultat = self::populateData($statement1, $arrayResultat, $groupByTypeProcedure, $dontCountMapas);
        Atexo_Db::closeCommon();

        return $arrayResultat;
    }

    public function getNombreProcedureAvecAuMoinsUneReponse($critere, $groupByTypeProcedure = false, $dontCountMapas = false)
    {
        $sql = ' SELECT count(consultation.id) as nombreConsultation, consultation.id_type_procedure_org as id_type_procedure, '
            .'  ao, mn, pn, dc, autre, Type_Procedure_Organisme.mapa, id_montant_mapa , sad, accord_cadre as accordcadre'
            .' FROM consultation left join Type_Procedure_Organisme on consultation.id_type_procedure_org=Type_Procedure_Organisme.id_type_procedure '
            .' AND consultation.organisme=Type_Procedure_Organisme.organisme '
            ." WHERE  consultation.id in (Select consultation_id from Offres where organisme='".$critere->getOrganisme()
            ."' AND Offres.statut_offres != '".Atexo_Config::getParameter('STATUT_ENV_BROUILLON')."' ) AND  consultation.organisme='".(new Atexo_Db())->quote($critere->getOrganisme())."' ";

        if ($critere) {
            if ($critere->getIdService()) {
                $sql .= ' AND (service_id '.(new Atexo_Db())->quote($critere->getIdService()).')';
            }
            if ($critere->getDateMiseEnLigneStart()) {
                $sql .= ' AND date_mise_en_ligne_calcule >= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneStart()).' 00:00\' ';
            }
            if ($critere->getDateMiseEnLigneEnd()) {
                $sql .= ' AND date_mise_en_ligne_calcule <= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneEnd()).' 23:59\' ';
            }
            if ($critere->getDateMiseEnLigneStart() || $critere->getDateMiseEnLigneEnd()) {
                $sql .= ' AND date_mise_en_ligne_calcule is not null ';
            }
            if ($critere->getDateRemisePlisStart()) {
                $sql .= ' AND datefin >= \''.(new Atexo_Db())->quote($critere->getDateRemisePlisStart()).' 00:00\' ';
            }
            if ($critere->getDateRemisePlisEnd()) {
                $sql .= ' AND datefin <= \''.(new Atexo_Db())->quote($critere->getDateRemisePlisEnd()).' 23:59\' ';
            }
            if ($critere->getDateRemisePlisStart() || $critere->getDateRemisePlisEnd()) {
                $sql .= ' AND datefin is not null ';
            }
            if ($critere->getDateMiseEnLigneAujourdhui()) {
                $sql .= ' AND date_mise_en_ligne_calcule<= NOW() ';
                $sql .= ' AND datefin> NOW() ';
            }

            $queryClause = self::getClauseQuery($critere);
            $sql .= ' AND '.$queryClause;
        }

        $sql .= ' GROUP BY id_type_procedure ';
        $statement1 = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_ASSOC);

        if ($groupByTypeProcedure) {
            if ($dontCountMapas) {
                $typesProcedures = (new Atexo_Consultation_ProcedureType())->rerieveTypeProcedureSaufMapa(true, false, $critere->getOrganisme());
                $arrayResultat = Atexo_Util::initializeArray($typesProcedures, 0);
                $arrayResultat['MAPA_INF'] = 0;
                $arrayResultat['MAPA_SUPP'] = 0;
            } else {
                $typesProcedures = (new Atexo_Consultation_ProcedureType())->rerieveAllProcedure(true, false, $critere->getOrganisme());
                $arrayResultat = Atexo_Util::initializeArray($typesProcedures, 0);
            }
        } else {
            $arrayResultat = ['mapaInf' => 0, 'mapaSupp' => 0, 'totalMapa' => 0, 'ao' => 0, 'mn' => 0, 'pn' => 0, 'dc' => 0, 'autre' => 0, 'total' => 0, 'sad' => 0, 'accordcadre' => '0', 'totalAccordCadreSad' => 0, 'grandTotal' => 0];
        }

        $arrayResultat = self::populateData($statement1, $arrayResultat, $groupByTypeProcedure, $dontCountMapas);
        Atexo_Db::closeCommon();

        return $arrayResultat;
    }

    public function nombreTelechargementParOeAvecCompte($critere, $groupByTypeProcedure = false, $dontCountMapas = false, $sansComptePf = false)
    {
        $sql = ' SELECT distinct Telechargement.id, consultation.organisme, consultation.id, consultation.id_type_procedure_org '
           .' FROM consultation,Telechargement '
           .' WHERE consultation.organisme=Telechargement.organisme AND consultation.id=Telechargement.consultation_id '
           .(($sansComptePf) ? '' : ' AND (id_inscrit!=0 AND id_inscrit IS NOT NULL)  ')
           ." AND Telechargement.organisme='".(new Atexo_Db())->quote($critere->getOrganisme())."'"
           ;

        if ($critere) {
            if ($critere->getIdService()) {
                $sql .= ' AND (service_id '.(new Atexo_Db())->quote($critere->getIdService()).')';
            }
            if ($critere->getDateMiseEnLigneStart()) {
                $sql .= ' AND date_mise_en_ligne_calcule >= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneStart()).' 00:00\' ';
            }
            if ($critere->getDateMiseEnLigneEnd()) {
                $sql .= ' AND date_mise_en_ligne_calcule <= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneEnd()).' 23:59\' ';
            }
            if ($critere->getDateMiseEnLigneStart() || $critere->getDateMiseEnLigneEnd()) {
                $sql .= ' AND date_mise_en_ligne_calcule is not null ';
            }
            if ($critere->getDateRemisePlisStart()) {
                $sql .= ' AND datefin >= \''.(new Atexo_Db())->quote($critere->getDateRemisePlisStart()).' 00:00\' ';
            }
            if ($critere->getDateRemisePlisEnd()) {
                $sql .= ' AND datefin <= \''.(new Atexo_Db())->quote($critere->getDateRemisePlisEnd()).' 23:59\' ';
            }
            if ($critere->getDateRemisePlisStart() || $critere->getDateRemisePlisEnd()) {
                $sql .= ' AND datefin is not null ';
            }
            if ($critere->getDateMiseEnLigneAujourdhui()) {
                $sql .= ' AND date_mise_en_ligne_calcule<= NOW() ';
                $sql .= ' AND datefin> NOW() ';
            }

            $queryClause = self::getClauseQuery($critere);
            $sql .= ' AND '.$queryClause;
        }

        $statement1 = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_ASSOC);

        if ($groupByTypeProcedure) {
            $arrayResultat = [];
        } else {
            $arrayResultat = ['mapaInf' => 0, 'mapaSupp' => 0, 'totalMapa' => 0, 'ao' => 0, 'mn' => 0, 'pn' => 0, 'dc' => 0, 'autre' => 0, 'total' => 0, 'sad' => 0, 'accordcadre' => '0', 'totalAccordCadreSad' => 0, 'grandTotal' => 0];
        }

        foreach ($statement1 as $row) {
            $typeProcedure = Atexo_Consultation_ProcedureType::retrieveProcedureById($row['id_type_procedure_org'], $row['organisme']);
            if ($groupByTypeProcedure) {
                if (!isset($arrayResultat['id_type_procedure_org'])) {
                    $arrayResultat['id_type_procedure_org'] = 0;
                }
                ++$arrayResultat['id_type_procedure_org'];
            } else {
                if ($typeProcedure instanceof CommonTypeProcedureOrganisme) {
                    if ('1' == $typeProcedure->getMapa()) {
                        if ($typeProcedure->getIdMontantMapa() == Atexo_Config::getParameter('MONTANT_MAPA_INF_90')) {
                            ++$arrayResultat['mapaInf'];
                        } else {
                            ++$arrayResultat['mapaSupp'];
                        }
                        ++$arrayResultat['totalMapa'];
                        ++$arrayResultat['grandTotal'];
                    } else {
                        if ('1' == $typeProcedure->getAo()) {
                            ++$arrayResultat['ao'];
                            ++$arrayResultat['total'];
                            ++$arrayResultat['grandTotal'];
                        } elseif ('1' == $typeProcedure->getMn()) {
                            ++$arrayResultat['mn'];
                            ++$arrayResultat['total'];
                            ++$arrayResultat['grandTotal'];
                        } elseif ('1' == $typeProcedure->getPn()) {
                            ++$arrayResultat['pn'];
                            ++$arrayResultat['total'];
                            ++$arrayResultat['grandTotal'];
                        } elseif ('1' == $typeProcedure->getDc()) {
                            ++$arrayResultat['dc'];
                            ++$arrayResultat['total'];
                            ++$arrayResultat['grandTotal'];
                        } elseif ('1' == $typeProcedure->getAutre()) {
                            ++$arrayResultat['autre'];
                            ++$arrayResultat['total'];
                            ++$arrayResultat['grandTotal'];
                        } elseif ('1' == $typeProcedure->getAccordCadre()) {
                            ++$arrayResultat['accordcadre'];
                            ++$arrayResultat['totalAccordCadreSad'];
                            ++$arrayResultat['grandTotal'];
                        } elseif ('1' == $typeProcedure->getSad()) {
                            ++$arrayResultat['sad'];
                            ++$arrayResultat['totalAccordCadreSad'];
                            ++$arrayResultat['grandTotal'];
                        }
                    }
                }
            }
        }

        return $arrayResultat;
    }

    /*-------------Fin tableau de bord--------------------*/

    public function getNombreConsultationEnLigne($idAgent, $idService, $organisme, $typeAvis = null)
    {
        if (Atexo_Module::isEnabled('OrganisationCentralisee', $organisme)) {
            $listOfServicesAllowed = Atexo_EntityPurchase::retrieveAllChildrenServices($idService, $organisme);
        } else {
            $listOfServicesAllowed = '('.$idService.')';
        }

        $sql = '	Select DISTINCT consultation.id , 1 as invitedWithHisHabilitation, 0 as invitedReadOnly '.
           ' FROM consultation LEFT OUTER JOIN Annonce ON (consultation.id = AnnonceBoamp.consultation_id AND consultation.organisme = AnnonceBoamp.organisme )'.
           ' LEFT JOIN CategorieLot ON (CategorieLot.consultation_id = consultation.id AND CategorieLot.organisme = consultation.organisme ) '.
           " WHERE consultation.consultation_achat_publique='0' AND consultation.organisme='".(new Atexo_Db())->quote($organisme)."' ".
           (($typeAvis) ? "	AND consultation.id_type_avis='".(new Atexo_Db())->quote($typeAvis)."' " : '').
           ' AND ( service_id IN '.$listOfServicesAllowed.' '.
           " OR ( (service_validation_intermediaire IS NULL AND service_validation='".(new Atexo_Db())->quote($idService)."' AND (datevalidation IS NULL OR datevalidation LIKE '0000-00-00%'))".
           " OR (service_validation_intermediaire IS NOT NULL AND service_validation = '".(new Atexo_Db())->quote($idService)."' ".
           " AND (datevalidation IS NULL OR datevalidation like '0000-00-00%') AND date_validation_intermediaire IS NOT NULL".
           " AND date_validation_intermediaire NOT LIKE '0000-00-00%' AND etat_approbation='1') ) OR (service_validation_intermediaire='".(new Atexo_Db())->quote($idService)."'".
           " AND (date_validation_intermediaire IS NULL OR date_validation_intermediaire LIKE '0000-00-00%') ) OR (service_validation='".(new Atexo_Db())->quote($idService)."' ".
           " AND (datevalidation IS NULL OR datevalidation LIKE '0000-00-00%') ) ) AND ( datefin > now() AND ( (date_mise_en_ligne_calcule!=''".
           " AND date_mise_en_ligne_calcule IS NOT NULL AND date_mise_en_ligne_calcule not like '0000-00-00%' AND date_mise_en_ligne_calcule<=now()) ) )".
           ' UNION Select DISTINCT consultation.id , 1 as invitedWithHisHabilitation, 0 as invitedReadOnly '.
           ' FROM consultation LEFT OUTER JOIN Annonce ON (consultation.id = Annonce.consultation_id )'.
           ' LEFT JOIN CategorieLot ON (CategorieLot.consultation_id = consultation.id AND CategorieLot.organisme = consultation.organisme ) , InterneConsultation'.
           " WHERE consultation.consultation_achat_publique='0' AND InterneConsultation.interne_id = '".(new Atexo_Db())->quote($idAgent)."' AND consultation.organisme='".(new Atexo_Db())->quote($organisme)."' ".
           " AND InterneConsultation.consultation_id = consultation.id AND ( datefin > now() AND ( (date_mise_en_ligne_calcule!=''".
           " AND date_mise_en_ligne_calcule IS NOT NULL AND date_mise_en_ligne_calcule not like '0000-00-00%' AND date_mise_en_ligne_calcule<=now()) ) )".
           (($typeAvis) ? "	AND consultation.id_type_avis='".(new Atexo_Db())->quote($typeAvis)."' " : '').
           ' UNION Select DISTINCT consultation.id , 0 as invitedWithHisHabilitation, 1 as invitedReadOnly '.
           ' FROM consultation LEFT OUTER JOIN AnnonceBoamp ON (consultation.id = AnnonceBoamp.consultation_id AND consultation.organisme = AnnonceBoamp.organisme )'.
           ' LEFT JOIN CategorieLot ON (CategorieLot.consultation_id = consultation.id AND CategorieLot.organisme = consultation.organisme ) , InterneConsultationSuiviSeul'.
           " WHERE consultation.consultation_achat_publique='0' AND InterneConsultationSuiviSeul.interne_id = '".(new Atexo_Db())->quote($idAgent)."' AND consultation.organisme='"
           .(new Atexo_Db())->quote($organisme)."' ".
           " AND InterneConsultationSuiviSeul.consultation_id = consultation.id AND ( datefin > now() AND ( (date_mise_en_ligne_calcule!=''".
           " AND date_mise_en_ligne_calcule IS NOT NULL AND date_mise_en_ligne_calcule not like '0000-00-00%' AND date_mise_en_ligne_calcule<=now()) ) )".
           (($typeAvis) ? "	AND consultation.id_type_avis='".(new Atexo_Db())->quote($typeAvis)."' " : '');

        $nombreConsultationEnLigne = 0;
        $statement = Atexo_Db::getLinkCommon(true)->query($sql);
        while ($statement->fetch(PDO::FETCH_ASSOC)) {
            ++$nombreConsultationEnLigne;
        }
        Atexo_Db::closeCommon();

        return $nombreConsultationEnLigne;
    }

    public function getNombreConsultationAAprouver($idAgent, $idService, $organisme)
    {
        $sql = 'Select DISTINCT consultation.id , 1 as invitedWithHisHabilitation, 0 as invitedReadOnly '
           .' FROM consultation LEFT OUTER JOIN AnnonceBoamp ON (consultation.id = AnnonceBoamp.consultation_id '
           .' AND consultation.organisme = AnnonceBoamp.organisme )LEFT JOIN CategorieLot ON '
           .'(CategorieLot.consultation_id = consultation.id AND CategorieLot.organisme = consultation.organisme ) '
           ." WHERE consultation.consultation_achat_publique='0' AND consultation_externe!='1' AND consultation.organisme='".(new Atexo_Db())->quote($organisme)."' "
           ." AND ( 0 OR (service_validation_intermediaire='".(new Atexo_Db())->quote($idService)."' AND (date_validation_intermediaire IS NULL "
           ."OR date_validation_intermediaire LIKE '0000-00-00%') ) )";
        $nombreConsultationAApprouver = 0;
        $statement = Atexo_Db::getLinkCommon(true)->query($sql);
        while ($statement->fetch(PDO::FETCH_ASSOC)) {
            ++$nombreConsultationAApprouver;
        }
        Atexo_Db::closeCommon();

        return $nombreConsultationAApprouver;
    }

    public function getNombreConsultationAValider($idAgent, $idService, $organisme, $enAttente = false)
    {
        $sql = 'Select DISTINCT consultation.id , 1 as invitedWithHisHabilitation, 0 as invitedReadOnly '
           .'	FROM consultation LEFT OUTER JOIN AnnonceBoamp ON (consultation.id = AnnonceBoamp.consultation_id AND consultation.organisme = AnnonceBoamp.organisme )'
           .'		LEFT JOIN CategorieLot ON (CategorieLot.consultation_id = consultation.id AND CategorieLot.organisme = consultation.organisme ) '
           ."	WHERE consultation.consultation_achat_publique='0' AND consultation_externe!='1' AND consultation.organisme='".(new Atexo_Db())->quote($organisme)."' "
           ."		  AND ( 0 OR ( (service_validation_intermediaire IS NULL AND service_validation='".(new Atexo_Db())->quote($idService)."' AND "
           ." 		 (datevalidation IS NULL OR datevalidation LIKE '0000-00-00%')) OR (service_validation_intermediaire IS NOT NULL AND service_validation = '"
           .(new Atexo_Db())->quote($idService)."' "
           ."		 AND (datevalidation IS NULL OR datevalidation like '0000-00-00%') AND date_validation_intermediaire IS NOT NULL "
           ."		 AND date_validation_intermediaire NOT LIKE '0000-00-00%' AND etat_approbation='1') ) OR (service_validation='"
           .(new Atexo_Db())->quote($idService)."' AND (datevalidation IS NULL OR datevalidation LIKE '0000-00-00%') ) )";

        $nombreConsultationAValider = 0;
        $statement = Atexo_Db::getLinkCommon(true)->query($sql);
        while ($statement->fetch(PDO::FETCH_ASSOC)) {
            ++$nombreConsultationAValider;
        }
        Atexo_Db::closeCommon();

        return $nombreConsultationAValider;
    }

    /**
     * Nombre de consultation crée par l'agent transmis en paramètre et nécissitant une validation par un agent d'un service
     * autre que celui de l'agent créateur.
     */
    public function getNombreConsultationEnAttente($idAgent, $idService, $organisme)
    {
        $query = "SELECT count(id) as nombreAnnonce FROM consultation WHERE consultation_externe!='1' AND id_createur='".(new Atexo_Db())->quote($idAgent)."'"
              ." AND service_validation!='".(new Atexo_Db())->quote($idService)."' AND organisme='".$organisme."' "
              ." AND datefin>now() AND (datevalidation IS NULL OR datevalidation like '0000-00-00%' OR datevalidation='') ";

        $nombreAnnonceEnAttente = 0;
        $statement = Atexo_Db::getLinkCommon(true)->query($query, PDO::FETCH_OBJ);
        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            $nombreAnnonceEnAttente = $row['nombreAnnonce'];
        }
        Atexo_Db::closeCommon();

        return $nombreAnnonceEnAttente;
    }

    public function getNombreCommission($idAgent, $organisme)
    {
        $query = 'SELECT count(b.id) as nombreCommission FROM Agent_Commission a , Commission b WHERE UNIX_TIMESTAMP(b.date) > UNIX_TIMESTAMP(NOW()) '.
        " AND a.id_agent='".(new Atexo_Db())->quote($idAgent)."' AND a.id_commission=b.id AND a.organisme=b.organisme ";

        $nombreCommission = 0;
        $statement = Atexo_Db::getLinkCommon(true)->query($query, PDO::FETCH_OBJ);
        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            $nombreCommission = $row['nombreCommission'];
        }
        Atexo_Db::closeCommon();

        return $nombreCommission;
    }

    public function getNombreConsultationEnLignePortail()
    {
        $sql = ' SELECT DISTINCT CONCAT(consultation.id, consultation.organisme)   '
           .' FROM consultation LEFT OUTER JOIN AnnonceBoamp ON (consultation.id = AnnonceBoamp.consultation_id AND consultation.organisme = AnnonceBoamp.organisme ) '
           .'		LEFT JOIN CategorieLot ON (CategorieLot.consultation_id = consultation.id '
           .'		AND CategorieLot.organisme = consultation.organisme )LEFT OUTER JOIN Organisme on Organisme.acronyme =consultation.organisme '
           ." WHERE 1 AND consultation.type_acces='1' AND consultation.id_type_avis = '3' AND Organisme.active = '1' "
           .' AND ( DATE_FIN_UNIX > unix_timestamp() ) '
           ." AND ( (date_mise_en_ligne_calcule!='' AND date_mise_en_ligne_calcule IS NOT NULL AND date_mise_en_ligne_calcule not like '0000-00-00%' "
           ." AND date_mise_en_ligne_calcule<=now()) OR ( depouillable_phase_consultation='1' AND ( ( datefin>now() "
           ." AND (date_mise_en_ligne_calcule='' OR date_mise_en_ligne_calcule IS NULL OR date_mise_en_ligne_calcule like '0000-00-00%' "
           .' OR date_mise_en_ligne_calcule>now()) ) ) AND datefin<=now() ) )';

        $nombreElement = 0;
        $statement = Atexo_Db::getLinkCommon(true)->query($sql);
        while ($statement->fetch(PDO::FETCH_ASSOC)) {
            ++$nombreElement;
        }

        return $nombreElement;
    }

    public function getNombreConsultationEnLigneDepuis($dateMiseEnLigne)
    {
        $sql = ' SELECT DISTINCT CONCAT(consultation.id, consultation.organisme) '
           .' FROM consultation LEFT OUTER JOIN Annonce ON (consultation.id = Annonce.consultation_id ) '
           .'		LEFT JOIN CategorieLot ON (CategorieLot.consultation_id = consultation.id '
           .'		AND CategorieLot.organisme = consultation.organisme )LEFT OUTER JOIN Organisme on Organisme.acronyme =consultation.organisme '
           ." WHERE 1 AND consultation.type_acces='1' AND consultation.id_type_avis = '3' AND Organisme.active = '1' "
           ."  AND date_mise_en_ligne_calcule>='".(new Atexo_Db())->quote($dateMiseEnLigne)."'";

        $nombreElement = 0;
        $statement = Atexo_Db::getLinkCommon(true)->query($sql);
        while ($statement->fetch(PDO::FETCH_ASSOC)) {
            ++$nombreElement;
        }

        return $nombreElement;
    }

    public function getNombreQuestions($idAgent, $idService, $organisme, $nombreAnnonceConcernee = false)
    {
        /* la date il y'a une semaine*/

        $lastWeek = Atexo_Util::getDateAWeekAgo();

        if (Atexo_Module::isEnabled('OrganisationCentralisee', $organisme)) {
            $listOfServicesAllowed = Atexo_EntityPurchase::retrieveAllChildrenServices($idService, $organisme);
        } else {
            $listOfServicesAllowed = '('.$idService.')';
        }

        $sqlConsultation = ' Select DISTINCT consultation.id  '
           .' FROM consultation LEFT OUTER JOIN AnnonceBoamp ON (consultation.id = AnnonceBoamp.consultation_id AND consultation.organisme = AnnonceBoamp.organisme )'
           .'		LEFT JOIN CategorieLot ON (CategorieLot.consultation_id = consultation.id AND CategorieLot.organisme = consultation.organisme ) '
           ." WHERE consultation.consultation_achat_publique='0' AND consultation_externe!='1' AND consultation.organisme='".(new Atexo_Db())->quote($organisme)."' AND ( "
           .'		 service_id IN '.$listOfServicesAllowed.' ) '
           .' UNION '
           .' Select DISTINCT consultation.id '
           .' FROM consultation LEFT OUTER JOIN AnnonceBoamp ON (consultation.id = AnnonceBoamp.consultation_id AND consultation.organisme = AnnonceBoamp.organisme ) '
           .'		LEFT JOIN CategorieLot ON (CategorieLot.consultation_id = consultation.id AND CategorieLot.organisme = consultation.organisme ) , InterneConsultation '
           ." WHERE consultation.consultation_achat_publique='0' AND consultation_externe!='1' AND InterneConsultation.interne_id = '".(new Atexo_Db())->quote($idAgent)."' "
           ." 		 AND InterneConsultation.consultation_id = consultation.id AND InterneConsultation.organisme = consultation.organisme AND consultation.organisme='"
           .(new Atexo_Db())->quote($organisme)."' ";

        if ($nombreAnnonceConcernee) {
            $query = 'consultation_id';
        } else {
            $query = 'id';
        }

        $sql = 'Select distinct questions_dce.'.$query.' from questions_dce Where consultation_id in ('.$sqlConsultation.') '
           ." AND date_depot>='".$lastWeek."' AND organisme='".$organisme."' ";

        $nombreElement = 0;
        $statement = Atexo_Db::getLinkCommon(true)->query($sql);
        while ($statement->fetch(PDO::FETCH_ASSOC)) {
            ++$nombreElement;
        }
        Atexo_Db::closeCommon();

        return $nombreElement;
    }

    public function getNombreReponsesADepouiller($idAgent, $idService, $organisme, $typeRegistre, $nombreAnnonceConcernee = false)
    {
        $last_2_months = Atexo_Util::getDateMonthsAgo(Atexo_Config::getParameter('SOCLE_EXTERNE_NOMBRE_MOIS'));
        $last_week = Atexo_Util::getDateAWeekAgo();

        if (Atexo_Module::isEnabled('OrganisationCentralisee', $organisme)) {
            $listOfServicesAllowed = Atexo_EntityPurchase::retrieveAllChildrenServices($idService, $organisme);
        } else {
            $listOfServicesAllowed = '('.$idService.')';
        }

        $sqlConsultation = ' Select DISTINCT consultation.id  '
           .' FROM consultation LEFT OUTER JOIN AnnonceBoamp ON (consultation.id = AnnonceBoamp.consultation_id AND consultation.organisme = AnnonceBoamp.organisme ) '
           .' 		LEFT JOIN CategorieLot ON (CategorieLot.consultation_id = consultation.id AND CategorieLot.organisme = consultation.organisme ) '
           ." WHERE consultation.consultation_achat_publique='0' AND consultation_externe!='1' AND consultation.organisme='".(new Atexo_Db())->quote($organisme)."' ( "
           .'		 service_id IN '.$listOfServicesAllowed.' ) '
           ."		 AND consultation.datefin >= '".(new Atexo_Db())->quote($last_2_months)." 00:00:00' "
           .' UNION '
           .' Select DISTINCT consultation.id  '
           .' FROM consultation LEFT OUTER JOIN AnnonceBoamp ON (consultation.id = AnnonceBoamp.consultation_id AND consultation.organisme = AnnonceBoamp.organisme )'
           .' 		LEFT JOIN CategorieLot ON (CategorieLot.consultation_id = consultation.id AND CategorieLot.organisme = consultation.organisme ) , InterneConsultation '
           ." WHERE consultation.consultation_achat_publique='0' AND consultation_externe!='1' AND InterneConsultation.interne_id = '2' "
           .'		 AND InterneConsultation.consultation_id = consultation.id AND InterneConsultation.organisme = consultation.organisme'
           ."  AND consultation.datefin >= '2009-03-19 00:00:00' AND consultation.organisme='".(new Atexo_Db())->quote($organisme)."' ";

        if ($nombreAnnonceConcernee) {
            $query = 'consultation_id';
        } else {
            $query = 'id';
        }

        if ($typeRegistre == Atexo_Config::getParameter('DEPOT_ELECTRONIQUE')) {
            $sql = 'Select distinct Offres.'.$query.' from Offres Where consultation_id in ('.$sqlConsultation.') '
               ." AND untrusteddate >='".(new Atexo_Db())->quote($last_week)."' AND organisme='".$organisme."' ";
        } else {
            $sql = 'Select distinct Offre_papier.'.$query.' from Offre_papier Where consultation_id in ('.$sqlConsultation.') '
               ." AND date_depot>='".(new Atexo_Db())->quote($last_week)."' AND organisme='".$organisme."' ";
        }

        /* la date il y'a une semaine*/

        $nombreElement = 0;
        $statement = Atexo_Db::getLinkCommon(true)->query($sql);
        while ($statement->fetch(PDO::FETCH_ASSOC)) {
            ++$nombreElement;
        }
        Atexo_Db::closeCommon();

        return $nombreElement;
    }

    public function getNombreOrganismesActive()
    {
        $query = "SELECT COUNT(id) as nombreOrganisme FROM Organisme Where active='1' ";
        $nombreOrganisme = 0;
        $statement = Atexo_Db::getLinkCommon(true)->query($query, PDO::FETCH_OBJ);
        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            $nombreOrganisme = $row['nombreOrganisme'];
        }

        return $nombreOrganisme;
    }

    public function getNombreEntreprises()
    {
        $query = 'SELECT COUNT(id) as nombreEntreprise FROM Entreprise';
        $nombreEntreprise = 0;
        $statement = Atexo_Db::getLinkCommon(true)->query($query, PDO::FETCH_OBJ);
        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            $nombreEntreprise = $row['nombreEntreprise'];
        }

        return $nombreEntreprise;
    }

    public function getNombreAllTelechargement($enLigneDepuis = null)
    {
        $query = 'SELECT COUNT(Telechargement.id) as nombreTelechargement FROM consultation,Telechargement, Organisme '
             .' WHERE Telechargement.consultation_id=consultation.id '
             .'		 AND Telechargement.organisme=Organisme.acronyme '
             .'       AND consultation.organisme=Organisme.acronyme '
             ."		 AND active='1' ";

        if ($enLigneDepuis) {
            $query .= " AND consultation.date_mise_en_ligne_calcule>='".(new Atexo_Db())->quote($enLigneDepuis)."' ";
        }

        $nombreTelechargement = 0;
        $statement = Atexo_Db::getLinkCommon(true)->query($query, PDO::FETCH_OBJ);
        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            $nombreTelechargement = $row['nombreTelechargement'];
        }

        return $nombreTelechargement;
    }

    public function getNombreAllReponseElectronique($enLigneDepuis = null)
    {
        $query = 'SELECT COUNT(Offres.id) as nombreReponseElectronique FROM consultation,Offres, Organisme '
             .' WHERE Offres.consultation_id=consultation.id '
             .'		 AND Offres.organisme=Organisme.acronyme '
             .'       AND consultation.organisme=Organisme.acronyme '
             ."		 AND active='1'  AND Offres.statut_offres != '".Atexo_Config::getParameter('STATUT_ENV_BROUILLON')."'";

        if ($enLigneDepuis) {
            $query .= " AND consultation.date_mise_en_ligne_calcule>='".(new Atexo_Db())->quote($enLigneDepuis)."' ";
        }

        $nombreReponseElectronique = 0;
        $statement = Atexo_Db::getLinkCommon(true)->query($query, PDO::FETCH_OBJ);
        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            $nombreReponseElectronique = $row['nombreReponseElectronique'];
        }

        return $nombreReponseElectronique;
    }

    public function getNombreOffrePapier($critere)
    {
        $sql = ' SELECT count(id) as nombreOffrePapier, id_type_procedure '
            .' FROM consultation , Offre_papier '
            ." WHERE consultation.id=Offre_papier.consultation_id AND  consultation.organisme=Offre_papier.organisme AND consultation.organisme='"
            .(new Atexo_Db())->quote($critere->getOrganisme())."' ";

        if ($critere) {
            if ($critere->getIdService()) {
                $sql .= ' AND (consultation.service_id '.(new Atexo_Db())->quote($critere->getIdService()).')';
            }
            if ($critere->getDateMiseEnLigneStart()) {
                $sql .= ' AND consultation.date_mise_en_ligne_calcule >= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneStart()).' 00:00\' ';
            }
            if ($critere->getDateMiseEnLigneEnd()) {
                $sql .= ' AND consultation.date_mise_en_ligne_calcule <= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneEnd()).' 23:59\' ';
            }
            if ($critere->getDateMiseEnLigneStart() || $critere->getDateMiseEnLigneEnd()) {
                $sql .= ' AND consultation.date_mise_en_ligne_calcule is not null ';
            }
            if ($critere->getDateRemisePlisStart()) {
                $sql .= ' AND consultation.datefin >= \''.(new Atexo_Db())->quote($critere->getDateRemisePlisStart()).' 00:00\' ';
            }
            if ($critere->getDateRemisePlisEnd()) {
                $sql .= ' AND consultation.datefin <= \''.(new Atexo_Db())->quote($critere->getDateRemisePlisEnd()).' 23:59\' ';
            }
            if ($critere->getDateRemisePlisStart() || $critere->getDateRemisePlisEnd()) {
                $sql .= ' AND consultation.datefin is not null ';
            }
            if ($critere->getIdTypeAvis()) {
                $sql .= " AND consultation.id_type_avis='".(new Atexo_Db())->quote($critere->getIdTypeAvis())."' ";
            }
        }

        $sql .= ' GROUP BY id_type_procedure ';

        $statement = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_ASSOC);

        $typesProcedures = (new Atexo_Consultation_ProcedureType())->rerieveAllProcedure(true, false, $critere->getOrganisme());
        $arrayResultat = Atexo_Util::initializeArray($typesProcedures, 0);

        foreach ($statement as $row) {
            $arrayResultat[$row['id_type_procedure']] = $row['nombreOffrePapier'];
        }

        Atexo_Db::closeCommon();

        return $arrayResultat;
    }

    public function getNbrAvisDematAAPC($critere)
    {
        $Consultations = [];
        $sql = ' SELECT count(consultation.id) as nombreConsultation, consultation.id_type_procedure_org '
                .' FROM consultation , AnnonceBoamp, CategorieLot '
                .' WHERE consultation.id=CategorieLot.consultation_id and consultation.organisme=CategorieLot.organisme '
                ." and AnnonceBoamp.consultation_id=consultation.id and AnnonceBoamp.organisme=consultation.organisme AND consultation.organisme='"
                .(new Atexo_Db())->quote($critere->getOrganisme())."' ";
        $sql .= " AND statut_destinataire IN ('CP','PU','AP') ";

        // il faut ajouter la partie JOUE
        if ($critere) {
            if ($critere->getIdService()) {
                $sql .= ' AND (consultation.service_id = '.(new Atexo_Db())->quote($critere->getIdService()).')';
            }
            if ($critere->getCategorieConsultation()) {
                $sql .= ' AND consultation.categorie = '.(new Atexo_Db())->quote($critere->getCategorieConsultation());
            }
            if ($critere->getDateMiseEnLigneStart()) {
                $sql .= ' AND consultation.date_mise_en_ligne_calcule >= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneStart()).' 00:00\' ';
            }
            if ($critere->getDateMiseEnLigneEnd()) {
                $sql .= ' AND consultation.date_mise_en_ligne_calcule <= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneEnd()).' 23:59\' ';
            }
            if ($critere->getDateMiseEnLigneStart() || $critere->getDateMiseEnLigneEnd()) {
                $sql .= ' AND consultation.date_mise_en_ligne_calcule is not null ';
            }
            if ($critere->getIdTypeAvis()) {
                $sql .= " AND consultation.id_type_avis='".(new Atexo_Db())->quote($critere->getIdTypeAvis())."' ";
            }
        }

        $sql .= ' GROUP BY id_type_procedure ';

        $statement = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_ASSOC);
        foreach ($statement as $row) {
            if (isset($Consultations[$arraytags[$row['id_type_procedure']]]['NbrAvisDemat'])) {
                $Consultations[$arraytags[$row['id_type_procedure']]]['NbrAvisDemat'] += $row['nombreConsultation'];
            } else {
                $Consultations[$arraytags[$row['id_type_procedure']]]['NbrAvisDemat'] = $row['nombreConsultation'];
            }
        }

        return $Consultations;
    }

    public function getNombreDceComplets($critere, $groupByTypeProcedure = false, $dontCountMapas = false)
    {
        $sql = ' SELECT  count(DISTINCT consultation.id) as nombreConsultation, consultation.id_type_procedure_org as id_type_procedure,  '
            .' ao, mn, pn, dc, autre, Type_Procedure_Organisme.mapa, id_montant_mapa , sad, accord_cadre as accordcadre '
            .' FROM consultation left join Type_Procedure_Organisme on consultation.id_type_procedure_org=Type_Procedure_Organisme.id_type_procedure'
            .'  AND consultation.organisme=Type_Procedure_Organisme.organisme, '
            .'      DCE'
            ." WHERE consultation.id=DCE.consultation_id AND consultation.organisme=DCE.organisme AND tirage_plan = 0 AND consultation.organisme='"
            .(new Atexo_Db())->quote($critere->getOrganisme())."' ";
        if ($critere) {
            if ($critere->getIdService()) {
                $sql .= ' AND (service_id '.(new Atexo_Db())->quote($critere->getIdService()).')';
            }
            if ($critere->getDateMiseEnLigneStart()) {
                $sql .= ' AND date_mise_en_ligne_calcule >= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneStart()).' 00:00\' ';
            }
            if ($critere->getDateMiseEnLigneEnd()) {
                $sql .= ' AND date_mise_en_ligne_calcule <= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneEnd()).' 23:59\' ';
            }
            if ($critere->getDateMiseEnLigneStart() || $critere->getDateMiseEnLigneEnd()) {
                $sql .= ' AND date_mise_en_ligne_calcule is not null ';
            }
            if ($critere->getDateRemisePlisStart()) {
                $sql .= ' AND datefin >= \''.(new Atexo_Db())->quote($critere->getDateRemisePlisStart()).' 00:00\' ';
            }
            if ($critere->getDateRemisePlisEnd()) {
                $sql .= ' AND datefin <= \''.(new Atexo_Db())->quote($critere->getDateRemisePlisEnd()).' 23:59\' ';
            }
            if ($critere->getDateRemisePlisStart() || $critere->getDateRemisePlisEnd()) {
                $sql .= ' AND datefin is not null ';
            }
            if ($critere->getDateMiseEnLigneAujourdhui()) {
                $sql .= ' AND date_mise_en_ligne_calcule< NOW() ';
                $sql .= ' AND datefin> NOW() ';
            }

            $queryClause = self::getClauseQuery($critere);
            $sql .= ' AND '.$queryClause;
        }
        $sql .= ' GROUP BY id_type_procedure ';

        $statement1 = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_ASSOC);

        if ($groupByTypeProcedure) {
            if ($dontCountMapas) {
                $typesProcedures = (new Atexo_Consultation_ProcedureType())->rerieveTypeProcedureSaufMapa(true, false, $critere->getOrganisme());
                $arrayResultat = Atexo_Util::initializeArray($typesProcedures, 0);
                $arrayResultat['MAPA_INF'] = 0;
                $arrayResultat['MAPA_SUPP'] = 0;
            } else {
                $typesProcedures = (new Atexo_Consultation_ProcedureType())->rerieveAllProcedure(true, false, $critere->getOrganisme());
                $arrayResultat = Atexo_Util::initializeArray($typesProcedures, 0);
            }
        } else {
            $arrayResultat = ['mapaInf' => 0, 'mapaSupp' => 0, 'totalMapa' => 0, 'ao' => 0, 'mn' => 0, 'pn' => 0, 'dc' => 0, 'autre' => 0, 'total' => 0, 'sad' => 0, 'accordcadre' => '0', 'totalAccordCadreSad' => 0, 'grandTotal' => 0];
        }

        $arrayResultat = self::populateData($statement1, $arrayResultat, $groupByTypeProcedure, $dontCountMapas);
        Atexo_Db::closeCommon();

        return $arrayResultat;
    }

    public function correspondanceTypeProcedure($values, $arrayTypeProcedure, $arrayInitResults)
    {
        $results = $arrayInitResults;
        foreach ($values as $idTypeProcedure => $nombreConsultation) {
            if (!isset($results[$arrayTypeProcedure[$idTypeProcedure]])) {
                $results[$arrayTypeProcedure[$idTypeProcedure]] = 0;
            }
            $results[$arrayTypeProcedure[$idTypeProcedure]] += $nombreConsultation;
        }

        return $results;
    }

    public function correspondanceTypeProcedureAvisPublicite($valuesTabs, $arrayTypeProcedure, $arrayInitResults)
    {
        $results = $arrayInitResults;
        $values = $valuesTabs;
        foreach ($values as $idTypeProcedure => $arrayStatsParAvis) {
            foreach ($arrayStatsParAvis as $typeAvis => $arrayStatsParAnnonce) {
                foreach ($arrayStatsParAnnonce as $typeAnnonce => $value) {
                    if (!isset($results[$arrayTypeProcedure[$idTypeProcedure]])) {
                        $results[$arrayTypeProcedure[$idTypeProcedure]] = 0;
                    }
                    $results[$arrayTypeProcedure[$idTypeProcedure]] += $value;
                    $valuesTabs[$idTypeProcedure][$typeAvis][$typeAnnonce] = $results;
                }
            }
        }

        return $valuesTabs;
    }

    public function getNbreProceduresForPortailAnnoncesMarches($critere)
    {
        $sql = ' SELECT count(consultation.id) as nombreConsultation '
            .' FROM consultation '
            .' WHERE 1=1 ';

        if ($critere) {
            if ($critere->getDateMiseEnLigneStart()) {
                $sql .= " AND date_mise_en_ligne_calcule >= '".(new Atexo_Db())->quote($critere->getDateMiseEnLigneStart())." 00:00' ";
            }
            if ($critere->getDateMiseEnLigneEnd()) {
                $sql .= " AND date_mise_en_ligne_calcule <= '".(new Atexo_Db())->quote($critere->getDateMiseEnLigneEnd())." 23:59' ";
            }
            if ($critere->getConsultationEnLigne() || $critere->getDateMiseEnLigneStart() || $critere->getDateMiseEnLigneEnd()) {
                $sql .= " AND date_mise_en_ligne_calcule is not null AND date_mise_en_ligne_calcule NOT LIKE '0000-00-00%' ";
            }
            if ($critere->getDateRemisePlisStart()) {
                $sql .= " AND datefin >= '".(new Atexo_Db())->quote($critere->getDateRemisePlisStart())." 00:00' ";
            }
            if ($critere->getDateRemisePlisEnd()) {
                $sql .= " AND datefin <= '".(new Atexo_Db())->quote($critere->getDateRemisePlisEnd())." 23:59' ";
            }
            if ($critere->getDateRemisePlisStart() || $critere->getDateRemisePlisEnd()) {
                $sql .= ' AND datefin is not null ';
            }

            if ($critere->getIdTypeAvis()) {
                $sql .= " AND consultation.id_type_avis='".(new Atexo_Db())->quote($critere->getIdTypeAvis())."' ";
            }
            if ($critere->getDateMiseEnLigneAujourdhui()) {
                $sql .= ' AND consultation.date_mise_en_ligne_calcule<= NOW() ';
                $sql .= ' AND consultation.datefin> NOW() ';
            }

            if ($critere->getConsultationEnLigne()) {
                $sql .= " AND consultation.type_acces = '1' ";
            }
        }

        $statement = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_NUM);
        foreach ($statement as $row) {
            return $row[0];
        }
    }

    public function getNombreEntreprisesAyantFait1SeulEtUniqueDepotOffreConcernantAuPlus1eConsultation($plsrsDepots = false)
    {
        $query = " SELECT count(entreprise_id) as nbreEntrp FROM Offres WHERE organisme IN (SELECT  acronyme FROM Organisme WHERE active='1') ";
        $query .= " AND Offres.statut_offres != '".Atexo_Config::getParameter('STATUT_ENV_BROUILLON')."' GROUP BY entreprise_id";
        $statement = Atexo_Db::getLinkCommon(true)->query($query, PDO::FETCH_ASSOC);
        $nbreEntrp = 0;
        foreach ($statement as $row) {
            if ($plsrsDepots) {
                if ($row['nbreEntrp'] >= 1) {
                    ++$nbreEntrp;
                }
            } else {
                if (1 == $row['nbreEntrp']) {
                    ++$nbreEntrp;
                }
            }
        }

        return $nbreEntrp;
    }

    public function getNbreReponseObligatoire($critere, $groupByTypeProcedure = false, $dontCountMapas = false)
    {
        $sql = ' SELECT count(consultation.id) as nombreConsultation, consultation.id_type_procedure_org as id_type_procedure,'
           .' Type_Procedure_Organisme.id_type_procedure_portail as id_type_procedure_portail, ao, mn, pn, dc, autre, Type_Procedure_Organisme.mapa, id_montant_mapa, sad, accord_cadre as accordcadre '
            .' FROM consultation left join Type_Procedure_Organisme on consultation.id_type_procedure_org=Type_Procedure_Organisme.id_type_procedure'
            .'  AND consultation.organisme=Type_Procedure_Organisme.organisme '
            ." WHERE consultation.organisme='".(new Atexo_Db())->quote($critere->getOrganisme())."' AND consultation.reponse_obligatoire=1";

        if ($critere) {
            if ($critere->getIdService()) {
                $sql .= ' AND (service_id '.(new Atexo_Db())->quote($critere->getIdService()).')';
            }
            if ($critere->getDateMiseEnLigneStart()) {
                $sql .= " AND date_mise_en_ligne_calcule >= '".(new Atexo_Db())->quote($critere->getDateMiseEnLigneStart())." 00:00' ";
            }
            if ($critere->getDateMiseEnLigneEnd()) {
                $sql .= " AND date_mise_en_ligne_calcule <= '".(new Atexo_Db())->quote($critere->getDateMiseEnLigneEnd())." 23:59' ";
            }
            if ($critere->getConsultationEnLigne() || $critere->getDateMiseEnLigneStart() || $critere->getDateMiseEnLigneEnd()) {
                $sql .= " AND date_mise_en_ligne_calcule is not null AND date_mise_en_ligne_calcule NOT LIKE '0000-00-00%' ";
            }
            if ($critere->getDateRemisePlisStart()) {
                $sql .= " AND datefin >= '".(new Atexo_Db())->quote($critere->getDateRemisePlisStart())." 00:00' ";
            }
            if ($critere->getDateRemisePlisEnd()) {
                $sql .= " AND datefin <= '".(new Atexo_Db())->quote($critere->getDateRemisePlisEnd())." 23:59' ";
            }
            if ($critere->getDateRemisePlisStart() || $critere->getDateRemisePlisEnd()) {
                $sql .= ' AND datefin is not null ';
            }
            if ($critere->getCategorieConsultation()) {
                $sql .= " AND categorie='".(new Atexo_Db())->quote($critere->getCategorieConsultation())."' ";
            }
            if (null !== $critere->getReponsesAutotisee()) {
                if ($critere->getReponsesAutotisee()) {
                    $sql .= " AND autoriser_reponse_electronique='1' ";
                } else {
                    $sql .= " AND autoriser_reponse_electronique!='1' ";
                }
            }
            if (null !== $critere->getReponsesAvecSignature()) {
                if ($critere->getReponsesAvecSignature()) {
                    $sql .= " AND signature_offre='1' ";
                } else {
                    $sql .= " AND signature_offre!='1' ";
                }
            }

            if ($critere->getIdTypeAvis()) {
                $sql .= " AND consultation.id_type_avis='".(new Atexo_Db())->quote($critere->getIdTypeAvis())."' ";
            }
            if ($critere->getDateMiseEnLigneAujourdhui()) {
                $sql .= ' AND consultation.date_mise_en_ligne_calcule<= NOW() ';
                $sql .= ' AND consultation.datefin> NOW() ';
            }

            if ($critere->getConsultationEnLigne()) {
                $sql .= " AND consultation.type_acces = '1' ";
            }

            $queryClause = self::getClauseQuery($critere);
            $sql .= ' AND '.$queryClause;
        }

        $sql .= ' GROUP BY id_type_procedure ';

        $statement = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_ASSOC);

        if ($groupByTypeProcedure) {
            if ($dontCountMapas) {
                $typesProcedures = (new Atexo_Consultation_ProcedureType())->rerieveTypeProcedureSaufMapa(true, false, $critere->getOrganisme());
                $arrayResultat = Atexo_Util::initializeArray($typesProcedures, 0);
                $arrayResultat['MAPA_INF'] = 0;
                $arrayResultat['MAPA_SUPP'] = 0;
            } else {
                $typesProcedures = (new Atexo_Consultation_ProcedureType())->rerieveAllProcedure(true, false, $critere->getOrganisme());
                $arrayResultat = Atexo_Util::initializeArray($typesProcedures, 0);
            }
        } else {
            $arrayResultat = ['mapaInf' => 0, 'mapaSupp' => 0, 'totalMapa' => 0, 'ao' => 0, 'mn' => 0, 'pn' => 0, 'dc' => 0, 'autre' => 0, 'total' => 0, 'sad' => 0, 'accordcadre' => '0', 'totalAccordCadreSad' => 0, 'grandTotal' => 0];
        }

        foreach ($statement as $row) {
            if ($groupByTypeProcedure) {
                if ($dontCountMapas) {
                    if ('1' == $row['mapa']) {
                        if ($row['id_montant_mapa'] == Atexo_Config::getParameter('MONTANT_MAPA_INF_90')) {
                            $arrayResultat['MAPA_INF'] += $row['nombreConsultation'];
                        } else {
                            $arrayResultat['MAPA_SUPP'] += $row['nombreConsultation'];
                        }
                    } else {
                        $arrayResultat[$row['id_type_procedure']] = $row['nombreConsultation'];
                    }
                } else {
                    $arrayResultat[$row['id_type_procedure']] = $row['nombreConsultation'];
                }
            } else {
                if ('1' == $row['mapa']) {
                    if ($row['id_montant_mapa'] == Atexo_Config::getParameter('MONTANT_MAPA_INF_90')) {
                        $arrayResultat['mapaInf'] += $row['nombreConsultation'];
                    } else {
                        $arrayResultat['mapaSupp'] += $row['nombreConsultation'];
                    }
                    $arrayResultat['totalMapa'] += $row['nombreConsultation'];
                    $arrayResultat['grandTotal'] += $row['nombreConsultation'];
                } else {
                    if ('1' == $row['ao']) {
                        $arrayResultat['ao'] += $row['nombreConsultation'];
                        $arrayResultat['total'] += $row['nombreConsultation'];
                        $arrayResultat['grandTotal'] += $row['nombreConsultation'];
                    } elseif ('1' == $row['mn']) {
                        $arrayResultat['mn'] += $row['nombreConsultation'];
                        $arrayResultat['total'] += $row['nombreConsultation'];
                        $arrayResultat['grandTotal'] += $row['nombreConsultation'];
                    } elseif ('1' == $row['pn']) {
                        $arrayResultat['pngetNombreDces'] += $row['nombreConsultation'];
                        $arrayResultat['total'] += $row['nombreConsultation'];
                        $arrayResultat['grandTotal'] += $row['nombreConsultation'];
                    } elseif ('1' == $row['dc']) {
                        $arrayResultat['dc'] += $row['nombreConsultation'];
                        $arrayResultat['total'] += $row['nombreConsultation'];
                        $arrayResultat['grandTotal'] += $row['nombreConsultation'];
                    } elseif ('1' == $row['sad']) {
                        $arrayResultat['sad'] += $row['nombreConsultation'];
                        $arrayResultat['totalAccordCadreSad'] += $row['nombreConsultation'];
                        $arrayResultat['grandTotal'] += $row['nombreConsultation'];
                    } elseif ('1' == $row['accordcadre']) {
                        $arrayResultat['accordcadre'] += $row['nombreConsultation'];
                        $arrayResultat['totalAccordCadreSad'] += $row['nombreConsultation'];
                        $arrayResultat['grandTotal'] += $row['nombreConsultation'];
                    } elseif ('1' == $row['autre']) {
                        $arrayResultat['autre'] += $row['nombreConsultation'];
                        $arrayResultat['total'] += $row['nombreConsultation'];
                        $arrayResultat['grandTotal'] += $row['nombreConsultation'];
                    }
                }
            }
        }
        Atexo_Db::closeCommon();

        return $arrayResultat;
    }

    public function getNombreTelechargementDCERaportNombreDCE($critere, $groupByTypeProcedure = false, $dontCountMapas = false, $nombreConsultationsConcernes = true)
    {
        $arrayResultat = [];
        $tabResTelechargement = self::getNombreTelechargement($critere, $groupByTypeProcedure, $dontCountMapas, $nombreConsultationsConcernes);
        $tabResDce = self::getNombreDces($critere, $groupByTypeProcedure, $dontCountMapas);

        foreach ($tabResTelechargement as $key => $value) {
            if (0 != $tabResDce[$key]) {
                $res = $value / $tabResDce[$key];
                if (!Atexo_Util::isEntier($res)) {
                    $res = ceil($res);
                }
                $arrayResultat[$key] = $res;
            } else {
                $arrayResultat[$key] = 0;
            }
        }

        return $arrayResultat;
    }

    public function getNombreConsultationNotifies($critere, $groupByTypeProcedure = false, $dontCountMapas = false)
    {
        $sql = 'SELECT COUNT( DISTINCT (`consultation_id`) ) as nombreConsultation ';
        $sql .= ', consultation.id_type_procedure_org as id_type_procedure, ao, mn, dc, autre, sad, accord_cadre as accordcadre, Type_Procedure_Organisme.mapa, id_montant_mapa '
            .' FROM consultation left join Type_Procedure_Organisme on consultation.id_type_procedure_org=Type_Procedure_Organisme.id_type_procedure, '
            .'      decisionEnveloppe'
            ." WHERE decisionEnveloppe.`date_notification` IS NOT NULL AND decisionEnveloppe.`date_notification` NOT LIKE '00-00-0000%' "
            ." AND consultation.id=decisionEnveloppe.consultation_id AND consultation.organisme=decisionEnveloppe.organisme AND consultation.organisme='"
            .(new Atexo_Db())->quote($critere->getOrganisme())."' ";

        if ($critere) {
            if ($critere->getIdService()) {
                $sql .= ' AND (service_id '.(new Atexo_Db())->quote($critere->getIdService()).')';
            }
            if ($critere->getDateMiseEnLigneStart()) {
                $sql .= ' AND date_mise_en_ligne_calcule >= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneStart()).' 00:00\' ';
            }
            if ($critere->getDateMiseEnLigneEnd()) {
                $sql .= ' AND date_mise_en_ligne_calcule <= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneEnd()).' 23:59\' ';
            }
            if ($critere->getDateMiseEnLigneStart() || $critere->getDateMiseEnLigneEnd()) {
                $sql .= ' AND date_mise_en_ligne_calcule is not null ';
            }
            if ($critere->getDateRemisePlisStart()) {
                $sql .= ' AND datefin >= \''.(new Atexo_Db())->quote($critere->getDateRemisePlisStart()).' 00:00\' ';
            }
            if ($critere->getDateRemisePlisEnd()) {
                $sql .= ' AND datefin <= \''.(new Atexo_Db())->quote($critere->getDateRemisePlisEnd()).' 23:59\' ';
            }
            if ($critere->getDateRemisePlisStart() || $critere->getDateRemisePlisEnd()) {
                $sql .= ' AND datefin is not null ';
            }
            if (null !== $critere->getReponsesAutotisee()) {
                if ($critere->getReponsesAutotisee()) {
                    $sql .= " AND autoriser_reponse_electronique='1' ";
                } else {
                    $sql .= " AND autoriser_reponse_electronique!='1' ";
                }
            }
            if (null !== $critere->getReponsesAvecSignature()) {
                if ($critere->getReponsesAvecSignature()) {
                    $sql .= " AND signature_offre='1' ";
                } else {
                    $sql .= " AND signature_offre!='1' ";
                }
            }
            if ($critere->getCategorieConsultation()) {
                $sql .= " AND categorie='".(new Atexo_Db())->quote($critere->getCategorieConsultation())."'";
            }

            if ($critere->getDateDepotRegistre()) {
                $sql .= " AND datetelechargement>='".(new Atexo_Db())->quote($critere->getDateDepotRegistre())."'";
            }
            if ($critere->getDateMiseEnLigneAujourdhui()) {
                $sql .= ' AND date_mise_en_ligne_calcule<= NOW() ';
                $sql .= ' AND datefin> NOW() ';
            }

            $queryClause = self::getClauseQuery($critere);
            $sql .= ' AND '.$queryClause;
        }

        $sql .= ' GROUP BY  id_type_procedure ';
        $statement1 = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_ASSOC);
        if ($groupByTypeProcedure) {
            if ($dontCountMapas) {
                $typesProcedures = (new Atexo_Consultation_ProcedureType())->rerieveTypeProcedureSaufMapa(true, false, $critere->getOrganisme());
                $arrayResultat = Atexo_Util::initializeArray($typesProcedures, 0);
                $arrayResultat['MAPA_INF'] = 0;
                $arrayResultat['MAPA_SUPP'] = 0;
            } else {
                $typesProcedures = (new Atexo_Consultation_ProcedureType())->rerieveAllProcedure(true, false, $critere->getOrganisme());
                $arrayResultat = Atexo_Util::initializeArray($typesProcedures, 0);
            }
        } else {
            $arrayResultat = ['mapaInf' => 0, 'mapaSupp' => 0, 'totalMapa' => 0, 'ao' => 0, 'mn' => 0, 'dc' => 0, 'autre' => 0, 'total' => 0, 'sad' => 0, 'accordcadre' => '0', 'totalAccordCadreSad' => 0, 'grandTotal' => 0];
        }
        $arrayResultat = self::populateData($statement1, $arrayResultat, $groupByTypeProcedure, $dontCountMapas);
        Atexo_Db::closeCommon();

        return $arrayResultat;
    }

    public function getNombreConsultationNotifiesPlatForme($critere, $groupByTypeProcedure = false, $dontCountMapas = false)
    {
        $sql = 'SELECT COUNT( DISTINCT (`consultation_id`) ) as nombreConsultation ';
        $sql .= ', consultation.id_type_procedure_org as id_type_procedure, ao, mn, dc, autre, sad, accord_cadre as accordcadre, Type_Procedure_Organisme.mapa, id_montant_mapa '
            .' FROM consultation left join Type_Procedure_Organisme on consultation.id_type_procedure_org=Type_Procedure_Organisme.id_type_procedure, '
            .' decisionEnveloppe left join Relation_Echange on decisionEnveloppe.id_decision_enveloppe=Relation_Echange.id_externe AND decisionEnveloppe.organisme=Relation_Echange.organisme'
            ." WHERE consultation.id=decisionEnveloppe.consultation_id AND consultation.organisme=decisionEnveloppe.organisme AND consultation.organisme='"
            .(new Atexo_Db())->quote($critere->getOrganisme())."' "
            ." AND Relation_Echange.type_relation='".Atexo_Config::getParameter('TYPE_RELATION_NOTIFICATION')."' ";

        if ($critere) {
            if ($critere->getIdService()) {
                $sql .= ' AND (service_id '.(new Atexo_Db())->quote($critere->getIdService()).')';
            }
            if ($critere->getDateMiseEnLigneStart()) {
                $sql .= ' AND date_mise_en_ligne_calcule >= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneStart()).' 00:00\' ';
            }
            if ($critere->getDateMiseEnLigneEnd()) {
                $sql .= ' AND date_mise_en_ligne_calcule <= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneEnd()).' 23:59\' ';
            }
            if ($critere->getDateMiseEnLigneStart() || $critere->getDateMiseEnLigneEnd()) {
                $sql .= ' AND date_mise_en_ligne_calcule is not null ';
            }
            if ($critere->getDateRemisePlisStart()) {
                $sql .= ' AND datefin >= \''.(new Atexo_Db())->quote($critere->getDateRemisePlisStart()).' 00:00\' ';
            }
            if ($critere->getDateRemisePlisEnd()) {
                $sql .= ' AND datefin <= \''.(new Atexo_Db())->quote($critere->getDateRemisePlisEnd()).' 23:59\' ';
            }
            if ($critere->getDateRemisePlisStart() || $critere->getDateRemisePlisEnd()) {
                $sql .= ' AND datefin is not null ';
            }
            if (null !== $critere->getReponsesAutotisee()) {
                if ($critere->getReponsesAutotisee()) {
                    $sql .= " AND autoriser_reponse_electronique='1' ";
                } else {
                    $sql .= " AND autoriser_reponse_electronique!='1' ";
                }
            }
            if (null !== $critere->getReponsesAvecSignature()) {
                if ($critere->getReponsesAvecSignature()) {
                    $sql .= " AND signature_offre='1' ";
                } else {
                    $sql .= " AND signature_offre!='1' ";
                }
            }
            if ($critere->getCategorieConsultation()) {
                $sql .= " AND categorie='".(new Atexo_Db())->quote($critere->getCategorieConsultation())."'";
            }

            if ($critere->getDateDepotRegistre()) {
                $sql .= " AND datetelechargement>='".(new Atexo_Db())->quote($critere->getDateDepotRegistre())."'";
            }
            if ($critere->getDateMiseEnLigneAujourdhui()) {
                $sql .= ' AND date_mise_en_ligne_calcule<= NOW() ';
                $sql .= ' AND datefin> NOW() ';
            }

            $queryClause = self::getClauseQuery($critere);
            $sql .= ' AND '.$queryClause;
        }

        $sql .= ' GROUP BY  id_type_procedure ';
        $statement1 = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_ASSOC);

        if ($groupByTypeProcedure) {
            if ($dontCountMapas) {
                $typesProcedures = (new Atexo_Consultation_ProcedureType())->rerieveTypeProcedureSaufMapa(true, false, $critere->getOrganisme());
                $arrayResultat = Atexo_Util::initializeArray($typesProcedures, 0);
                $arrayResultat['MAPA_INF'] = 0;
                $arrayResultat['MAPA_SUPP'] = 0;
            } else {
                $typesProcedures = (new Atexo_Consultation_ProcedureType())->rerieveAllProcedure(true, false, $critere->getOrganisme());
                $arrayResultat = Atexo_Util::initializeArray($typesProcedures, 0);
            }
        } else {
            $arrayResultat = ['mapaInf' => 0, 'mapaSupp' => 0, 'totalMapa' => 0, 'ao' => 0, 'mn' => 0, 'dc' => 0, 'autre' => 0, 'total' => 0, 'sad' => 0, 'accordcadre' => '0', 'totalAccordCadreSad' => 0, 'grandTotal' => 0];
        }

        $arrayResultat = self::populateData($statement1, $arrayResultat, $groupByTypeProcedure, $dontCountMapas);
        Atexo_Db::closeCommon();

        return $arrayResultat;
    }

    public function populateData($statement1, $arrayResultat, $groupByTypeProcedure = false, $dontCountMapas = false)
    {
        foreach ($statement1 as $row) {
            if ($groupByTypeProcedure) {
                if ($dontCountMapas) {
                    if ('1' == $row['mapa']) {
                        if ($row['id_montant_mapa'] == Atexo_Config::getParameter('MONTANT_MAPA_INF_90')) {
                            $arrayResultat['MAPA_INF'] += $row['nombreConsultation'];
                        } else {
                            $arrayResultat['MAPA_SUPP'] += $row['nombreConsultation'];
                        }
                    } else {
                        if(array_key_exists('id_type_procedure', $row)) {
                            $arrayResultat[$row['id_type_procedure']]=$row['nombreConsultation'];
                        } else {
                            $arrayResultat[$row['id_type_procedure_org']]=$row['nombreConsultation'];
                        }
                    }
                } else {
                    if(array_key_exists('id_type_procedure', $row)) {
                        $arrayResultat[$row['id_type_procedure']]=$row['nombreConsultation'];
                    } else {
                        $arrayResultat[$row['id_type_procedure_org']]=$row['nombreConsultation'];
                    }
                }
            } else {
                if ('1' == $row['mapa']) {
                    if ($row['id_montant_mapa'] == Atexo_Config::getParameter('MONTANT_MAPA_INF_90')) {
                        $arrayResultat['mapaInf'] += $row['nombreConsultation'];
                    } else {
                        $arrayResultat['mapaSupp'] += $row['nombreConsultation'];
                    }
                    $arrayResultat['totalMapa'] += $row['nombreConsultation'];
                    $arrayResultat['grandTotal'] += $row['nombreConsultation'];
                } else {
                    if ('1' == $row['ao']) {
                        $arrayResultat['ao'] += $row['nombreConsultation'];
                        $arrayResultat['total'] += $row['nombreConsultation'];
                        $arrayResultat['grandTotal'] += $row['nombreConsultation'];
                    } elseif ('1' == $row['mn']) {
                        $arrayResultat['mn'] += $row['nombreConsultation'];
                        $arrayResultat['total'] += $row['nombreConsultation'];
                        $arrayResultat['grandTotal'] += $row['nombreConsultation'];
                    } elseif ('1' == $row['pn']) {
                        $arrayResultat['pn'] += $row['nombreConsultation'];
                        $arrayResultat['total'] += $row['nombreConsultation'];
                        $arrayResultat['grandTotal'] += $row['nombreConsultation'];
                    } elseif ('1' == $row['dc']) {
                        $arrayResultat['dc'] += $row['nombreConsultation'];
                        $arrayResultat['total'] += $row['nombreConsultation'];
                        $arrayResultat['grandTotal'] += $row['nombreConsultation'];
                    } elseif ('1' == $row['autre']) {
                        $arrayResultat['autre'] += $row['nombreConsultation'];
                        $arrayResultat['total'] += $row['nombreConsultation'];
                        $arrayResultat['grandTotal'] += $row['nombreConsultation'];
                    } elseif ('1' == $row['sad']) {
                        $arrayResultat['sad'] += $row['nombreConsultation'];
                        $arrayResultat['totalAccordCadreSad'] += $row['nombreConsultation'];
                        $arrayResultat['grandTotal'] += $row['nombreConsultation'];
                    } elseif ('1' == $row['accordcadre']) {
                        $arrayResultat['accordcadre'] += $row['nombreConsultation'];
                        $arrayResultat['totalAccordCadreSad'] += $row['nombreConsultation'];
                        $arrayResultat['grandTotal'] += $row['nombreConsultation'];
                    }
                }
            }
        }

        return $arrayResultat;
    }

    public function getNombreTelechargementDCEStatEntreprise($critere, $avecCompte = true)
    {
        $sql = ' SELECT count(distinct Telechargement.`id`) as nbTelechargement FROM consultation,Telechargement '
            .' WHERE consultation.id=Telechargement.consultation_id '
            .' AND Telechargement.organisme= consultation.organisme '
            ." AND Telechargement.organisme='".(new Atexo_Db())->quote($critere->getOrganisme())."'";

        if ($avecCompte) {
            $sql .= ' AND id_inscrit !=0 ';
        }

        if ($critere) {
            if ($critere->getIdService()) {
                $sql .= ' AND (consultation.service_id '.(new Atexo_Db())->quote($critere->getIdService()).')';
            }
            if ($critere->getDateMiseEnLigneStart()) {
                $sql .= ' AND consultation.date_mise_en_ligne_calcule >= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneStart()).' 00:00\' ';
            }
            if ($critere->getDateMiseEnLigneEnd()) {
                $sql .= ' AND consultation.date_mise_en_ligne_calcule <= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneEnd()).' 23:59\' ';
            }
            if ($critere->getDateMiseEnLigneStart() || $critere->getDateMiseEnLigneEnd()) {
                $sql .= ' AND consultation.date_mise_en_ligne_calcule is not null ';
            }
            if ($critere->getDateRemisePlisStart()) {
                $sql .= ' AND consultation.datefin >= \''.(new Atexo_Db())->quote($critere->getDateRemisePlisStart()).' 00:00\' ';
            }
            if ($critere->getDateRemisePlisEnd()) {
                $sql .= ' AND consultation.datefin <= \''.(new Atexo_Db())->quote($critere->getDateRemisePlisEnd()).' 23:59\' ';
            }
            if ($critere->getDateRemisePlisStart() || $critere->getDateRemisePlisEnd()) {
                $sql .= ' AND consultation.datefin is not null ';
            }
            if ($critere->getDateMiseEnLigneAujourdhui()) {
                $sql .= ' AND consultation.date_mise_en_ligne_calcule<= NOW() ';
                $sql .= ' AND datefin> NOW() ';
            }
        }

        $statement = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_ASSOC);
        foreach ($statement as $row) {
            return $row;
        }
    }

    public function getPourcentageDepotElectronique($critere, $groupByTypeProcedure, $dontCountMapas, $instantSigned = '2')
    {
        $sql = 'SELECT count( id ) AS nbreDepotsElectronique, consultation.id_type_procedure_org, ao, mn, pn, dc, autre, sad, accord_cadre AS accordcadre,'.
                   'Type_Procedure_Organisme.mapa, id_montant_mapa '.
                   ' FROM Offres, consultation, Type_Procedure_Organisme '.
                   ' WHERE Offres.consultation_id = consultation.id '.
                   ' AND consultation.id_type_procedure_org = Type_Procedure_Organisme.id_type_procedure '.
                   ' AND consultation.organisme = Type_Procedure_Organisme.organisme '.
                   ' AND Offres.organisme = consultation.organisme '.
                   " AND consultation.organisme = '".(new Atexo_Db())->quote($critere->getOrganisme())."'".
                   " AND consultation.autoriser_reponse_electronique='1' ".
                   " AND Offres.statut_offres != '".Atexo_Config::getParameter('STATUT_ENV_BROUILLON')."'";

        $req = ' SELECT count( id ) AS nbreDepotsPapier, consultation.id_type_procedure_org, ao, mn, pn, dc, autre, sad, accord_cadre AS accordcadre,'.
                   ' Type_Procedure_Organisme.mapa, id_montant_mapa '.
                   ' FROM Offre_papier, consultation, Type_Procedure_Organisme '.
                   ' WHERE Offre_papier.consultation_id = consultation.id '.
                   ' AND consultation.id_type_procedure_org = Type_Procedure_Organisme.id_type_procedure '.
                   ' AND consultation.organisme = Type_Procedure_Organisme.organisme '.
                   ' AND Offre_papier.organisme = consultation.organisme '.
                   " AND consultation.organisme = '".(new Atexo_Db())->quote($critere->getOrganisme())."'".
                   " AND consultation.autoriser_reponse_electronique='1' ";

        // $instantSigned = 0 : pour les consultations dont la signature électronique des plis est requise au moment de la réponse.
        // $instantSigned = 1 : pour les consultations dont la signature électronique des plis des plis n?est pas requise au moment de la réponse.
        // $instantSigned = 2 : Indifferent.
        if ('1' == $instantSigned) {
            $sql .= " AND consultation.signature_offre = '1'";
            $req .= " AND consultation.signature_offre = '1'";
        }
        if ('0' == $instantSigned) {
            $sql .= " AND consultation.signature_offre = '0'";
            $req .= " AND consultation.signature_offre = '0'";
        }

        if ($critere) {
            if ($critere->getIdService()) {
                $sql .= ' AND (consultation.service_id '.(new Atexo_Db())->quote($critere->getIdService()).')';
                $req .= ' AND (consultation.service_id '.(new Atexo_Db())->quote($critere->getIdService()).')';
            }
            if ($critere->getDateMiseEnLigneStart()) {
                $sql .= ' AND consultation.date_mise_en_ligne_calcule >= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneStart()).' 00:00\' ';
                $req .= ' AND consultation.date_mise_en_ligne_calcule >= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneStart()).' 00:00\' ';
            }
            if ($critere->getDateMiseEnLigneEnd()) {
                $sql .= ' AND consultation.date_mise_en_ligne_calcule <= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneEnd()).' 23:59\' ';
                $req .= ' AND consultation.date_mise_en_ligne_calcule <= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneEnd()).' 23:59\' ';
            }
            if ($critere->getDateMiseEnLigneStart() || $critere->getDateMiseEnLigneEnd()) {
                $sql .= ' AND consultation.date_mise_en_ligne_calcule is not null ';
                $req .= ' AND consultation.date_mise_en_ligne_calcule is not null ';
            }
            if ($critere->getDateRemisePlisStart()) {
                $sql .= ' AND consultation.datefin >= \''.(new Atexo_Db())->quote($critere->getDateRemisePlisStart()).' 00:00\' ';
                $req .= ' AND consultation.datefin >= \''.(new Atexo_Db())->quote($critere->getDateRemisePlisStart()).' 00:00\' ';
            }
            if ($critere->getDateRemisePlisEnd()) {
                $sql .= ' AND consultation.datefin <= \''.(new Atexo_Db())->quote($critere->getDateRemisePlisEnd()).' 23:59\' ';
                $req .= ' AND consultation.datefin <= \''.(new Atexo_Db())->quote($critere->getDateRemisePlisEnd()).' 23:59\' ';
            }
            if ($critere->getDateRemisePlisStart() || $critere->getDateRemisePlisEnd()) {
                $sql .= ' AND consultation.datefin is not null ';
                $req .= ' AND consultation.datefin is not null ';
            }
            if ($critere->getDateMiseEnLigneAujourdhui()) {
                $sql .= ' AND consultation.date_mise_en_ligne_calcule<= NOW() ';
                $sql .= ' AND datefin> NOW() ';
                $req .= ' AND consultation.date_mise_en_ligne_calcule<= NOW() ';
                $req .= ' AND datefin> NOW() ';
            }
            $queryClause = self::getClauseQuery($critere);
            $sql .= ' AND '.$queryClause;
            $req .= ' AND '.$queryClause;
        }
        $sql .= ' GROUP BY consultation.id_type_procedure_org ';
        $req .= ' GROUP BY consultation.id_type_procedure_org ';

        $statement = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_ASSOC);
        $statement1 = Atexo_Db::getLinkCommon(true)->query($req, PDO::FETCH_ASSOC);

        if ($groupByTypeProcedure) {
            if ($dontCountMapas) {
                $typesProcedures = (new Atexo_Consultation_ProcedureType())->rerieveTypeProcedureSaufMapa(true, false, $critere->getOrganisme());
                $arrayOffresElectronique = Atexo_Util::initializeArray($typesProcedures, 0);
                $arrayOffresPapier = Atexo_Util::initializeArray($typesProcedures, 0);
                $arrayResultat = Atexo_Util::initializeArray($typesProcedures, 0);
                $arrayResultat['MAPA_INF'] = 0;
                $arrayResultat['MAPA_SUPP'] = 0;
                $arrayResultat['total'] = 0;
                $arrayResultat['grandTotal'] = 0;
                $arrayResultat['totalMapa'] = 0;
                $arrayOffresPapier['MAPA_INF'] = 0;
                $arrayOffresPapier['MAPA_SUPP'] = 0;
                $arrayOffresPapier['total'] = 0;
                $arrayOffresPapier['grandTotal'] = 0;
                $arrayOffresPapier['totalMapa'] = 0;
                $arrayOffresElectronique['MAPA_INF'] = 0;
                $arrayOffresElectronique['MAPA_SUPP'] = 0;
                $arrayOffresElectronique['total'] = 0;
                $arrayOffresElectronique['grandTotal'] = 0;
                $arrayOffresElectronique['totalMapa'] = 0;
            } else {
                $typesProcedures = (new Atexo_Consultation_ProcedureType())->rerieveAllProcedure(true, false, $critere->getOrganisme());
                $arrayOffresElectronique = Atexo_Util::initializeArray($typesProcedures, 0);
                $arrayOffresPapier = Atexo_Util::initializeArray($typesProcedures, 0);
                $arrayResultat = Atexo_Util::initializeArray($typesProcedures, 0);
                $arrayOffresElectronique['total'] = 0;
                $arrayOffresElectronique['grandTotal'] = 0;
                $arrayOffresElectronique['totalMapa'] = 0;
                $arrayOffresPapier['total'] = 0;
                $arrayOffresPapier['grandTotal'] = 0;
                $arrayOffresPapier['totalMapa'] = 0;
                $arrayResultat['total'] = 0;
                $arrayResultat['grandTotal'] = 0;
                $arrayResultat['totalMapa'] = 0;
            }
        } else {
            $arrayOffresElectronique = ['mapaInf' => 0, 'mapaSupp' => 0, 'totalMapa' => 0, 'ao' => 0, 'mn' => 0, 'pn' => 0, 'dc' => 0, 'autre' => 0, 'total' => 0, 'sad' => 0, 'accordcadre' => '0', 'totalAccordCadreSad' => 0, 'grandTotal' => '0'];
            $arrayOffresPapier = ['mapaInf' => 0, 'mapaSupp' => 0, 'totalMapa' => 0, 'ao' => 0, 'mn' => 0, 'pn' => 0, 'dc' => 0, 'autre' => 0, 'total' => 0, 'sad' => 0, 'accordcadre' => '0', 'totalAccordCadreSad' => 0, 'grandTotal' => '0'];
            $arrayResultat = ['mapaInf' => 0, 'mapaSupp' => 0, 'totalMapa' => 0, 'ao' => 0, 'mn' => 0, 'pn' => 0, 'dc' => 0, 'autre' => 0, 'total' => 0, 'sad' => 0, 'accordcadre' => '0', 'totalAccordCadreSad' => 0, 'grandTotal' => '0'];
        }

        foreach ($statement as $row) {
            if ($groupByTypeProcedure) {
                if ($dontCountMapas) {
                    if ('1' == $row['mapa']) {
                        if ($row['id_montant_mapa'] == Atexo_Config::getParameter('MONTANT_MAPA_INF_90')) {
                            $arrayOffresElectronique['MAPA_INF'] += $row['nbreDepotsElectronique'];
                        } else {
                            $arrayOffresElectronique['MAPA_SUPP'] += $row['nbreDepotsElectronique'];
                        }
                        $arrayOffresElectronique['totalMapa'] += $row['nbreDepotsElectronique'];
                        $arrayOffresElectronique['grandTotal'] += $row['nbreDepotsElectronique'];
                    } else {
                        $arrayOffresElectronique[$row['id_type_procedure_org']] = $row['nbreDepotsElectronique'];
                        $arrayOffresElectronique['total'] += $row['nbreDepotsElectronique'];
                        $arrayOffresElectronique['grandTotal'] += $row['nbreDepotsElectronique'];
                    }
                } else {
                    $arrayOffresElectronique[$row['id_type_procedure_org']] = $row['nbreDepotsElectronique'];
                    $arrayOffresElectronique['total'] += $row['nbreDepotsElectronique'];
                    $arrayOffresElectronique['grandTotal'] += $row['nbreDepotsElectronique'];
                }
            } else {
                if ('1' == $row['mapa']) {
                    if ($row['id_montant_mapa'] == Atexo_Config::getParameter('MONTANT_MAPA_INF_90')) {
                        $arrayOffresElectronique['mapaInf'] += $row['nbreDepotsElectronique'];
                    } else {
                        $arrayOffresElectronique['mapaSupp'] += $row['nbreDepotsElectronique'];
                    }
                    $arrayOffresElectronique['totalMapa'] += $row['nbreDepotsElectronique'];
                    $arrayOffresElectronique['grandTotal'] += $row['nbreDepotsElectronique'];
                } else {
                    if ('1' == $row['ao']) {
                        $arrayOffresElectronique['ao'] += $row['nbreDepotsElectronique'];
                        $arrayOffresElectronique['total'] += $row['nbreDepotsElectronique'];
                        $arrayOffresElectronique['grandTotal'] += $row['nbreDepotsElectronique'];
                    } elseif ('1' == $row['mn']) {
                        $arrayOffresElectronique['mn'] += $row['nbreDepotsElectronique'];
                        $arrayOffresElectronique['total'] += $row['nbreDepotsElectronique'];
                        $arrayOffresElectronique['grandTotal'] += $row['nbreDepotsElectronique'];
                    } elseif ('1' == $row['pn']) {
                        $arrayOffresElectronique['pn'] += $row['nbreDepotsElectronique'];
                        $arrayOffresElectronique['total'] += $row['nbreDepotsElectronique'];
                        $arrayOffresElectronique['grandTotal'] += $row['nbreDepotsElectronique'];
                    } elseif ('1' == $row['dc']) {
                        $arrayOffresElectronique['dc'] += $row['nbreDepotsElectronique'];
                        $arrayOffresElectronique['total'] += $row['nbreDepotsElectronique'];
                        $arrayOffresElectronique['grandTotal'] += $row['nbreDepotsElectronique'];
                    } elseif ('1' == $row['autre']) {
                        $arrayOffresElectronique['autre'] += $row['nbreDepotsElectronique'];
                        $arrayOffresElectronique['total'] += $row['nbreDepotsElectronique'];
                        $arrayOffresElectronique['grandTotal'] += $row['nbreDepotsElectronique'];
                    } elseif ('1' == $row['sad']) {
                        $arrayOffresElectronique['sad'] += $row['nbreDepotsElectronique'];
                        $arrayOffresElectronique['totalAccordCadreSad'] += $row['nbreDepotsElectronique'];
                        $arrayOffresElectronique['grandTotal'] += $row['nbreDepotsElectronique'];
                    } elseif ('1' == $row['accordcadre']) {
                        $arrayOffresElectronique['accordcadre'] += $row['nbreDepotsElectronique'];
                        $arrayOffresElectronique['totalAccordCadreSad'] += $row['nbreDepotsElectronique'];
                        $arrayOffresElectronique['grandTotal'] += $row['nbreDepotsElectronique'];
                    }
                }
            }
        }

        foreach ($statement1 as $row) {
            if ($groupByTypeProcedure) {
                if ($dontCountMapas) {
                    if ('1' == $row['mapa']) {
                        if ($row['id_montant_mapa'] == Atexo_Config::getParameter('MONTANT_MAPA_INF_90')) {
                            $arrayOffresPapier['MAPA_INF'] += $row['nbreDepotsPapier'];
                        } else {
                            $arrayOffresPapier['MAPA_SUPP'] += $row['nbreDepotsPapier'];
                        }
                        $arrayOffresPapier['totalMapa'] += $row['nbreDepotsPapier'];
                        $arrayOffresPapier['grandTotal'] += $row['nbreDepotsPapier'];
                    } else {
                        $arrayOffresPapier[$row['id_type_procedure_org']] = $row['nbreDepotsPapier'];
                        $arrayOffresPapier['total'] += $row['nbreDepotsPapier'];
                        $arrayOffresPapier['grandTotal'] += $row['nbreDepotsPapier'];
                    }
                } else {
                    $arrayOffresPapier[$row['id_type_procedure_org']] = $row['nbreDepotsPapier'];
                    $arrayOffresPapier['total'] += $row['nbreDepotsPapier'];
                    $arrayOffresPapier['grandTotal'] += $row['nbreDepotsPapier'];
                }
            } else {
                if ('1' == $row['mapa']) {
                    if ($row['id_montant_mapa'] == Atexo_Config::getParameter('MONTANT_MAPA_INF_90')) {
                        $arrayOffresPapier['mapaInf'] += $row['nbreDepotsPapier'];
                    } else {
                        $arrayOffresPapier['mapaSupp'] += $row['nbreDepotsPapier'];
                    }
                    $arrayOffresPapier['totalMapa'] += $row['nbreDepotsPapier'];
                    $arrayOffresPapier['grandTotal'] += $row['nbreDepotsPapier'];
                } else {
                    if ('1' == $row['ao']) {
                        $arrayOffresPapier['ao'] += $row['nbreDepotsPapier'];
                        $arrayOffresPapier['total'] += $row['nbreDepotsPapier'];
                        $arrayOffresPapier['grandTotal'] += $row['nbreDepotsPapier'];
                    } elseif ('1' == $row['mn']) {
                        $arrayOffresPapier['mn'] += $row['nbreDepotsPapier'];
                        $arrayOffresPapier['total'] += $row['nbreDepotsPapier'];
                        $arrayOffresPapier['grandTotal'] += $row['nbreDepotsPapier'];
                    } elseif ('1' == $row['pn']) {
                        $arrayOffresPapier['pn'] += $row['nbreDepotsPapier'];
                        $arrayOffresPapier['total'] += $row['nbreDepotsPapier'];
                        $arrayOffresPapier['grandTotal'] += $row['nbreDepotsPapier'];
                    } elseif ('1' == $row['dc']) {
                        $arrayOffresPapier['dc'] += $row['nbreDepotsPapier'];
                        $arrayOffresPapier['total'] += $row['nbreDepotsPapier'];
                        $arrayOffresPapier['grandTotal'] += $row['nbreDepotsPapier'];
                    } elseif ('1' == $row['autre']) {
                        $arrayOffresPapier['autre'] += $row['nbreDepotsPapier'];
                        $arrayOffresPapier['total'] += $row['nbreDepotsPapier'];
                        $arrayOffresPapier['grandTotal'] += $row['nbreDepotsPapier'];
                    } elseif ('1' == $row['sad']) {
                        $arrayOffresPapier['sad'] += $row['nbreDepotsPapier'];
                        $arrayOffresPapier['totalAccordCadreSad'] += $row['nbreDepotsPapier'];
                        $arrayOffresPapier['grandTotal'] += $row['nbreDepotsPapier'];
                    } elseif ('1' == $row['accordcadre']) {
                        $arrayOffresPapier['accordcadre'] += $row['nbreDepotsPapier'];
                        $arrayOffresPapier['totalAccordCadreSad'] += $row['nbreDepotsPapier'];
                        $arrayOffresPapier['grandTotal'] += $row['nbreDepotsPapier'];
                    }
                }
            }
        }

        foreach ($arrayOffresElectronique as $key => $Value) {
            if ($arrayOffresElectronique[$key] + $arrayOffresPapier[$key] > 0) {
                $arrayResultat[$key] = ($arrayOffresElectronique[$key] * 100) / ($arrayOffresElectronique[$key] + $arrayOffresPapier[$key]);
            } else {
                $arrayResultat[$key] = '-';
            }
        }

        return $arrayResultat;
    }

    public function getPourcentageDepotElectroniqueWithInstantSignature($critere, $groupByTypeProcedure, $dontCountMapas)
    {
        $arrayResultat = self::getPourcentageDepotElectronique($critere, $groupByTypeProcedure, $dontCountMapas, '1');

        return $arrayResultat;
    }

    public function getPourcentageReponseObligatoireSansSignature($critere, $groupByTypeProcedure, $dontCountMapas)
    {
        $arrayResultat = self::getPourcentageDepotElectronique($critere, $groupByTypeProcedure, $dontCountMapas, '0');

        return $arrayResultat;
    }

    public function getNombreConsultationsWithReponsesElectroniqueRequise($critere, $groupByTypeProcedure, $dontCountMapas)
    {
        $sql = ' SELECT COUNT( DISTINCT (consultation.id) ) as nombreConsultation , consultation.id_type_procedure_org as id_type_procedure, '
               .'ao, mn, dc, autre, sad, accord_cadre as accordcadre, Type_Procedure_Organisme.mapa, id_montant_mapa '
            .' FROM consultation left join Type_Procedure_Organisme on consultation.id_type_procedure_org=Type_Procedure_Organisme.id_type_procedure '
            ." WHERE consultation.autoriser_reponse_electronique='1' "
            ." AND consultation.organisme='".(new Atexo_Db())->quote($critere->getOrganisme())."' ";

        if ($critere) {
            if ($critere->getIdService()) {
                $sql .= ' AND (service_id '.(new Atexo_Db())->quote($critere->getIdService()).')';
            }
            if ($critere->getDateMiseEnLigneStart()) {
                $sql .= ' AND date_mise_en_ligne_calcule >= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneStart()).' 00:00\' ';
            }
            if ($critere->getDateMiseEnLigneEnd()) {
                $sql .= ' AND date_mise_en_ligne_calcule <= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneEnd()).' 23:59\' ';
            }
            if ($critere->getDateMiseEnLigneStart() || $critere->getDateMiseEnLigneEnd()) {
                $sql .= ' AND date_mise_en_ligne_calcule is not null ';
            }
            if ($critere->getDateRemisePlisStart()) {
                $sql .= ' AND datefin >= \''.(new Atexo_Db())->quote($critere->getDateRemisePlisStart()).' 00:00\' ';
            }
            if ($critere->getDateRemisePlisEnd()) {
                $sql .= ' AND datefin <= \''.(new Atexo_Db())->quote($critere->getDateRemisePlisEnd()).' 23:59\' ';
            }
            if ($critere->getDateRemisePlisStart() || $critere->getDateRemisePlisEnd()) {
                $sql .= ' AND datefin is not null ';
            }
            if (null !== $critere->getReponsesAutotisee()) {
                if ($critere->getReponsesAutotisee()) {
                    $sql .= " AND autoriser_reponse_electronique='1' ";
                } else {
                    $sql .= " AND autoriser_reponse_electronique!='1' ";
                }
            }
            if (null !== $critere->getReponsesAvecSignature()) {
                if ($critere->getReponsesAvecSignature()) {
                    $sql .= " AND signature_offre='1' ";
                } else {
                    $sql .= " AND signature_offre!='1' ";
                }
            }
            if ($critere->getCategorieConsultation()) {
                $sql .= " AND categorie='".(new Atexo_Db())->quote($critere->getCategorieConsultation())."'";
            }

            if ($critere->getDateDepotRegistre()) {
                $sql .= " AND datetelechargement>='".(new Atexo_Db())->quote($critere->getDateDepotRegistre())."'";
            }
            if ($critere->getDateMiseEnLigneAujourdhui()) {
                $sql .= ' AND date_mise_en_ligne_calcule<= NOW() ';
                $sql .= ' AND datefin> NOW() ';
            }
            $queryClause = self::getClauseQuery($critere);
            $sql .= ' AND '.$queryClause;
        }

        $sql .= ' GROUP BY  id_type_procedure ';
        $statement = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_ASSOC);
        if ($groupByTypeProcedure) {
            if ($dontCountMapas) {
                $typesProcedures = (new Atexo_Consultation_ProcedureType())->rerieveTypeProcedureSaufMapa(true, false, $critere->getOrganisme());
                $arrayResultat = Atexo_Util::initializeArray($typesProcedures, 0);
                $arrayResultat['MAPA_INF'] = 0;
                $arrayResultat['MAPA_SUPP'] = 0;
            } else {
                $typesProcedures = (new Atexo_Consultation_ProcedureType())->rerieveAllProcedure(true, false, $critere->getOrganisme());
                $arrayResultat = Atexo_Util::initializeArray($typesProcedures, 0);
            }
        } else {
            $arrayResultat = ['mapaInf' => 0, 'mapaSupp' => 0, 'totalMapa' => 0, 'ao' => 0, 'mn' => 0, 'pn' => 0, 'dc' => 0, 'autre' => 0, 'total' => 0, 'sad' => 0, 'accordcadre' => '0', 'totalAccordCadreSad' => 0, 'grandTotal' => 0];
        }
        $arrayResultat = self::populateData($statement, $arrayResultat, $groupByTypeProcedure);
        Atexo_Db::closeCommon();

        return $arrayResultat;
    }

    public function getNombreConsultationsWithReponsesElectroniqueAutorisee($critere, $groupByTypeProcedure, $dontCountMapas)
    {
        $sql = ' SELECT COUNT( DISTINCT (consultation.id) ) as nombreConsultation , consultation.id_type_procedure_org as id_type_procedure, '
            .'ao, mn, dc, autre, sad, accord_cadre as accordcadre, Type_Procedure_Organisme.mapa, id_montant_mapa '
            .' FROM consultation left join Type_Procedure_Organisme on consultation.id_type_procedure_org=Type_Procedure_Organisme.id_type_procedure '
            ." WHERE consultation.signature_offre='1' "
            ." AND consultation.organisme='".(new Atexo_Db())->quote($critere->getOrganisme())."' ";

        if ($critere) {
            if ($critere->getIdService()) {
                $sql .= ' AND (service_id '.(new Atexo_Db())->quote($critere->getIdService()).')';
            }
            if ($critere->getDateMiseEnLigneStart()) {
                $sql .= ' AND date_mise_en_ligne_calcule >= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneStart()).' 00:00\' ';
            }
            if ($critere->getDateMiseEnLigneEnd()) {
                $sql .= ' AND date_mise_en_ligne_calcule <= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneEnd()).' 23:59\' ';
            }
            if ($critere->getDateMiseEnLigneStart() || $critere->getDateMiseEnLigneEnd()) {
                $sql .= ' AND date_mise_en_ligne_calcule is not null ';
            }
            if ($critere->getDateRemisePlisStart()) {
                $sql .= ' AND datefin >= \''.(new Atexo_Db())->quote($critere->getDateRemisePlisStart()).' 00:00\' ';
            }
            if ($critere->getDateRemisePlisEnd()) {
                $sql .= ' AND datefin <= \''.(new Atexo_Db())->quote($critere->getDateRemisePlisEnd()).' 23:59\' ';
            }
            if ($critere->getDateRemisePlisStart() || $critere->getDateRemisePlisEnd()) {
                $sql .= ' AND datefin is not null ';
            }
            if (null !== $critere->getReponsesAutotisee()) {
                if ($critere->getReponsesAutotisee()) {
                    $sql .= " AND autoriser_reponse_electronique='1' ";
                } else {
                    $sql .= " AND autoriser_reponse_electronique!='1' ";
                }
            }
            if (null !== $critere->getReponsesAvecSignature()) {
                if ($critere->getReponsesAvecSignature()) {
                    $sql .= " AND signature_offre='1' ";
                } else {
                    $sql .= " AND signature_offre!='1' ";
                }
            }
            if ($critere->getCategorieConsultation()) {
                $sql .= " AND categorie='".(new Atexo_Db())->quote($critere->getCategorieConsultation())."'";
            }

            if ($critere->getDateDepotRegistre()) {
                $sql .= " AND datetelechargement>='".(new Atexo_Db())->quote($critere->getDateDepotRegistre())."'";
            }
            if ($critere->getDateMiseEnLigneAujourdhui()) {
                $sql .= ' AND date_mise_en_ligne_calcule<= NOW() ';
                $sql .= ' AND datefin> NOW() ';
            }
            if ($critere->getIdTypeAvis()) {
                $sql .= " AND consultation.id_type_avis='".(new Atexo_Db())->quote($critere->getIdTypeAvis())."' ";
            }
            $queryClause = self::getClauseQuery($critere);
            $sql .= ' AND '.$queryClause;
        }

        $sql .= ' GROUP BY  id_type_procedure ';
        $statement = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_ASSOC);
        if ($groupByTypeProcedure) {
            if ($dontCountMapas) {
                $typesProcedures = (new Atexo_Consultation_ProcedureType())->rerieveTypeProcedureSaufMapa(true, false, $critere->getOrganisme());
                $arrayResultat = Atexo_Util::initializeArray($typesProcedures, 0);
                $arrayResultat['MAPA_INF'] = 0;
                $arrayResultat['MAPA_SUPP'] = 0;
            } else {
                $typesProcedures = (new Atexo_Consultation_ProcedureType())->rerieveAllProcedure(true, false, $critere->getOrganisme());
                $arrayResultat = Atexo_Util::initializeArray($typesProcedures, 0);
            }
        } else {
            $arrayResultat = ['mapaInf' => 0, 'mapaSupp' => 0, 'totalMapa' => 0, 'ao' => 0, 'mn' => 0, 'pn' => 0, 'dc' => 0, 'autre' => 0, 'total' => 0, 'sad' => 0, 'accordcadre' => '0', 'totalAccordCadreSad' => 0, 'grandTotal' => 0];
        }
        $arrayResultat = self::populateData($statement, $arrayResultat, $groupByTypeProcedure);
        Atexo_Db::closeCommon();

        return $arrayResultat;
    }

    /*
    * Nombre de téléchargements anonymes de DCE
    */
    public function getNombreTelechargementsAnonymesDCE($critere, $groupByTypeProcedure = false, $dontCountMapas = false, $nombreConsultationsConcernes = true)
    {
        $sql = ' SELECT ';
        if ($nombreConsultationsConcernes) {
            $sql .= 'count(consultation.id) as nombreConsultation ';
        } else {
            $sql .= 'count(TelechargementAnonyme.id) as nombreConsultation ';
        }
        $sql .= ', consultation.id_type_procedure_org as id_type_procedure, ao, mn, pn, dc, autre, sad, accord_cadre as accordcadre, Type_Procedure_Organisme.mapa, id_montant_mapa '
            .' FROM consultation left join Type_Procedure_Organisme on consultation.id_type_procedure_org=Type_Procedure_Organisme.id_type_procedure, '
            .'      TelechargementAnonyme'
            .' WHERE consultation.id=TelechargementAnonyme.consultation_id '
            .' AND consultation.organisme=TelechargementAnonyme.organisme '
            ." AND consultation.organisme='".(new Atexo_Db())->quote($critere->getOrganisme())."' "
            .' AND Type_Procedure_Organisme.organisme = consultation.organisme';

        if ($critere) {
            if ($critere->getIdService()) {
                $sql .= ' AND (service_id '.(new Atexo_Db())->quote($critere->getIdService()).')';
            }
            if ($critere->getDateMiseEnLigneStart()) {
                $sql .= ' AND date_mise_en_ligne_calcule >= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneStart()).' 00:00\' ';
            }
            if ($critere->getDateMiseEnLigneEnd()) {
                $sql .= ' AND date_mise_en_ligne_calcule <= \''.(new Atexo_Db())->quote($critere->getDateMiseEnLigneEnd()).' 23:59\' ';
            }
            if ($critere->getDateMiseEnLigneStart() || $critere->getDateMiseEnLigneEnd()) {
                $sql .= ' AND date_mise_en_ligne_calcule is not null ';
            }
            if ($critere->getDateRemisePlisStart()) {
                $sql .= ' AND datefin >= \''.(new Atexo_Db())->quote($critere->getDateRemisePlisStart()).' 00:00\' ';
            }
            if ($critere->getDateRemisePlisEnd()) {
                $sql .= ' AND datefin <= \''.(new Atexo_Db())->quote($critere->getDateRemisePlisEnd()).' 23:59\' ';
            }
            if ($critere->getDateRemisePlisStart() || $critere->getDateRemisePlisEnd()) {
                $sql .= ' AND datefin is not null ';
            }
            if (null !== $critere->getReponsesAutotisee()) {
                if ($critere->getReponsesAutotisee()) {
                    $sql .= " AND autoriser_reponse_electronique='1' ";
                } else {
                    $sql .= " AND autoriser_reponse_electronique!='1' ";
                }
            }
            if (null !== $critere->getReponsesAvecSignature()) {
                if ($critere->getReponsesAvecSignature()) {
                    $sql .= " AND signature_offre='1' ";
                } else {
                    $sql .= " AND signature_offre!='1' ";
                }
            }
            if ($critere->getCategorieConsultation()) {
                $sql .= " AND categorie='".(new Atexo_Db())->quote($critere->getCategorieConsultation())."'";
            }

            if ($critere->getDateDepotRegistre()) {
                $sql .= " AND datetelechargement>='".(new Atexo_Db())->quote($critere->getDateDepotRegistre())."'";
            }
            if ($critere->getDateMiseEnLigneAujourdhui()) {
                $sql .= ' AND date_mise_en_ligne_calcule<= NOW() ';
                $sql .= ' AND datefin> NOW() ';
            }

            $queryClause = self::getClauseQuery($critere);
            $sql .= ' AND '.$queryClause;
        }

        $sql .= ' GROUP BY  id_type_procedure ';

        $statement1 = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_ASSOC);

        if ($groupByTypeProcedure) {
            if ($dontCountMapas) {
                $typesProcedures = (new Atexo_Consultation_ProcedureType())->rerieveTypeProcedureSaufMapa(true, false, $critere->getOrganisme());
                $arrayResultat = Atexo_Util::initializeArray($typesProcedures, 0);
                $arrayResultat['MAPA_INF'] = 0;
                $arrayResultat['MAPA_SUPP'] = 0;
            } else {
                $typesProcedures = (new Atexo_Consultation_ProcedureType())->rerieveAllProcedure(true, false, $critere->getOrganisme());
                $arrayResultat = Atexo_Util::initializeArray($typesProcedures, 0);
            }
        } else {
            $arrayResultat = ['mapaInf' => 0, 'mapaSupp' => 0, 'totalMapa' => 0, 'ao' => 0, 'mn' => 0, 'pn' => 0, 'dc' => 0, 'autre' => 0, 'total' => 0, 'sad' => 0, 'accordcadre' => '0', 'totalAccordCadreSad' => 0, 'grandTotal' => 0];
        }
        foreach ($statement1 as $row) {
            $comptabilise = true;

            if ($comptabilise) {
                if ($groupByTypeProcedure) {
                    if ($dontCountMapas) {
                        if ('1' == $row['mapa']) {
                            if ($row['id_montant_mapa'] == Atexo_Config::getParameter('MONTANT_MAPA_INF_90')) {
                                $arrayResultat['MAPA_INF'] += $row['nombreConsultation'];
                            } else {
                                $arrayResultat['MAPA_SUPP'] += $row['nombreConsultation'];
                            }
                        } else {
                            $arrayResultat[$row['id_type_procedure']] = $row['nombreConsultation'];
                        }
                    } else {
                        $arrayResultat[$row['id_type_procedure']] = $row['nombreConsultation'];
                    }
                } else {
                    if ('1' == $row['mapa']) {
                        if ($row['id_montant_mapa'] == Atexo_Config::getParameter('MONTANT_MAPA_INF_90')) {
                            $arrayResultat['mapaInf'] += $row['nombreConsultation'];
                        } else {
                            $arrayResultat['mapaSupp'] += $row['nombreConsultation'];
                        }
                        $arrayResultat['totalMapa'] += $row['nombreConsultation'];
                        $arrayResultat['grandTotal'] += $row['nombreConsultation'];
                    } else {
                        if ('1' == $row['ao']) {
                            $arrayResultat['ao'] += $row['nombreConsultation'];
                            $arrayResultat['total'] += $row['nombreConsultation'];
                            $arrayResultat['grandTotal'] += $row['nombreConsultation'];
                        } elseif ('1' == $row['mn']) {
                            $arrayResultat['mn'] += $row['nombreConsultation'];
                            $arrayResultat['total'] += $row['nombreConsultation'];
                            $arrayResultat['grandTotal'] += $row['nombreConsultation'];
                        } elseif ('1' == $row['pn']) {
                            $arrayResultat['pn'] += $row['nombreConsultation'];
                            $arrayResultat['total'] += $row['nombreConsultation'];
                            $arrayResultat['grandTotal'] += $row['nombreConsultation'];
                        } elseif ('1' == $row['dc']) {
                            $arrayResultat['dc'] += $row['nombreConsultation'];
                            $arrayResultat['total'] += $row['nombreConsultation'];
                            $arrayResultat['grandTotal'] += $row['nombreConsultation'];
                        } elseif ('1' == $row['autre']) {
                            $arrayResultat['autre'] += $row['nombreConsultation'];
                            $arrayResultat['total'] += $row['nombreConsultation'];
                            $arrayResultat['grandTotal'] += $row['nombreConsultation'];
                        } elseif ('1' == $row['sad']) {
                            $arrayResultat['sad'] += $row['nombreConsultation'];
                            $arrayResultat['totalAccordCadreSad'] += $row['nombreConsultation'];
                            $arrayResultat['grandTotal'] += $row['nombreConsultation'];
                        } elseif ('1' == $row['accordcadre']) {
                            $arrayResultat['accordcadre'] += $row['nombreConsultation'];
                            $arrayResultat['totalAccordCadreSad'] += $row['nombreConsultation'];
                            $arrayResultat['grandTotal'] += $row['nombreConsultation'];
                        }
                    }
                }
            }
        }
        Atexo_Db::closeCommon();

        return $arrayResultat;
    }

    /**
     * Retourne le nombre d'entreprises EA ou ESAT.
     *
     * @return : Integer
     */
    public function getNbreEntrepriseEA()
    {
        $sql = "Select count(*) as nbrEntreprise From Entreprise WHERE entreprise_EA='1' ";

        $statement = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_NUM);
        foreach ($statement as $row) {
            return $row[0];
        }
    }

    /**
     * Retourne le nombre d'entreprises SIAE.
     *
     * @return : Integer
     */
    public function getNbreEntrepriseSIAE()
    {
        $sql = "Select count(*) as nbrEntreprise From Entreprise WHERE entreprise_SIAE='1' ";

        $statement = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_NUM);
        foreach ($statement as $row) {
            return $row[0];
        }
    }

    /**
     * Retourne la requete de recherche par les clauses.
     */
    public function getClauseQuery($critere)
    {
        $arrayCond = [];
        $query = 1;
        if (Atexo_Module::isEnabled('ConsultationClause')) {
            $hasConditionCC = false;
            if ('0' != $critere->getClauseSociale()) {
                $hasConditionCC = true;
                $arrayCond[] = "clause_sociale='".$critere->getClauseSociale()."'";
            }
            if ('0' != $critere->getClauseEnvironnement()) {
                $hasConditionCC = true;
                $arrayCond[] = "clause_environnementale='".$critere->getClauseEnvironnement()."'";
            }
            if ($hasConditionCC) {
                $conditionCCConsultation = 'consultation.'.implode(' AND consultation.', $arrayCond);
                $conditionCCLots = 'catLot.'.implode(' AND catLot.', $arrayCond);
                $query = <<<QUERY
(
	( $conditionCCConsultation  AND consultation.alloti = '0')
	OR
	((
		SELECT
			COUNT(*)
		FROM
			CategorieLot as catLot
		WHERE
			catLot.consultation_id = consultation.id
			AND catLot.organisme = consultation.organisme
			AND consultation.alloti = '1'
			AND ( $conditionCCLots )
	)<>0)
)
QUERY;
            }
        }

        return $query;
    }

    /**
     * retourne le nombre des elements dans une requete de type count.
     */
    public function getNbElementQuery($sql)
    {
        $statement = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_NUM);
        foreach ($statement as $row) {
            return $row[0];
        }
    }

    public function getArrayParamFromStatement(&$arrayResultat, $statement, $byTypeProcedure, $dontCountMapas)
    {
        foreach ($statement as $row) {
            if ($byTypeProcedure) {
                if ($dontCountMapas) {
                    if ('1' == $row['mapa']) {
                        if ($row['id_montant_mapa'] == Atexo_Config::getParameter('MONTANT_MAPA_INF_90')) {
                            $arrayResultat['MAPA_INF'] += $row['nombreConsultation'];
                        } else {
                            $arrayResultat['MAPA_SUPP'] += $row['nombreConsultation'];
                        }
                    } else {
                        $arrayResultat[$row['id_type_procedure']] = $row['nombreConsultation'];
                    }
                } else {
                    $arrayResultat[$row['id_type_procedure']] = $row['nombreConsultation'];
                }
            } else {
                if ('1' == $row['mapa']) {
                    if ($row['id_montant_mapa'] == Atexo_Config::getParameter('MONTANT_MAPA_INF_90')) {
                        $arrayResultat['mapaInf'] += $row['nombreConsultation'];
                    } else {
                        $arrayResultat['mapaSupp'] += $row['nombreConsultation'];
                    }
                    $arrayResultat['totalMapa'] += $row['nombreConsultation'];
                } else {
                    if ('1' == $row['ao']) {
                        $arrayResultat['ao'] += $row['nombreConsultation'];
                        $arrayResultat['total'] += $row['nombreConsultation'];
                    } elseif ('1' == $row['mn']) {
                        $arrayResultat['mn'] += $row['nombreConsultation'];
                        $arrayResultat['total'] += $row['nombreConsultation'];
                    } elseif ('1' == $row['pn']) {
                        $arrayResultat['pn'] += $row['nombreConsultation'];
                        $arrayResultat['total'] += $row['nombreConsultation'];
                    } elseif ('1' == $row['dc']) {
                        $arrayResultat['dc'] += $row['nombreConsultation'];
                        $arrayResultat['total'] += $row['nombreConsultation'];
                    } elseif (array_key_exists('sad', $row) && '1' == $row['sad']) {
                        $arrayResultat['sad'] += $row['nombreConsultation'];
                        $arrayResultat['totalAccordCadreSad'] += $row['nombreConsultation'];
                    } elseif (array_key_exists('accordcadre', $row) && '1' == $row['accordcadre']) {
                        $arrayResultat['accordcadre'] += $row['nombreConsultation'];
                        $arrayResultat['totalAccordCadreSad'] += $row['nombreConsultation'];
                    } elseif ('1' == $row['autre']) {
                        $arrayResultat['autre'] += $row['nombreConsultation'];
                        $arrayResultat['total'] += $row['nombreConsultation'];
                    }
                }
            }
        }
    }

    /**
     * Recuperation des donnees et calcul du nombre de réponses MPS reçues pour les consultations MPS.
     *
     * @param Atexo_Statistiques_CriteriaVo $critere
     *
     * @return array
     *
     * @author Oumar KONATE <oumar.konate@atexom.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function getNbreReponsesMps($critere)
    {
        $autresCriteres = self::getCriteresRechercheMps($critere, 'c');
        $sqlAutresCriteres = '';
        if (is_array($autresCriteres) && count($autresCriteres)) {
            $sqlAutresCriteres .= ' AND '.implode(' AND ', $autresCriteres);
        }

        $sql = " 
 				SELECT
					c.categorie as categorie_consultation,
					count(o.id) as nombre_offres
				FROM
					consultation c, 
					Offres o, 
					t_candidature_mps cand
				WHERE
					c.id = o.consultation_id 
					AND c.organisme = o.organisme
					AND o.id = cand.id_offre 
					AND o.organisme = cand.organisme
					AND cand.id_offre IS NOT NULL 
 					AND c.categorie in (1,2,3) 
 					AND c.marche_public_simplifie = '1'
 					AND o.statut_offres != '".Atexo_Config::getParameter('STATUT_ENV_BROUILLON')."'
 			   		$sqlAutresCriteres
				GROUP BY categorie_consultation WITH ROLLUP
				";
        $statement = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_ASSOC);
        $listeResultat = [];
        foreach ($statement as $row) {
            $listeResultat[$row['categorie_consultation']] = $row['nombre_offres'];
        }
        Atexo_Db::closeCommon();

        return $listeResultat;
    }

    /**
     * Retourne de nombre de consultations passées en mode MPS
     * (si consultation non allotie alors nombre de consultations = 1, sinon nombre de consultations = nombre de lots)
     * (pour 1 consultation non allotie et 1 consultation ayant 2 lots alors nombre de consultations = 3).
     *
     * @param Atexo_Statistiques_CriteriaVo $critere
     *
     * @return array
     *
     * @author Oumar KONATE <oumar.konate@atexom.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function getNombreConsultationsMps($critere)
    {
        $autresCriteres = self::getCriteresRechercheMps($critere, 'c');
        $sqlAutresCriteres = '';
        if (is_array($autresCriteres) && count($autresCriteres)) {
            $sqlAutresCriteres .= ' AND '.implode(' AND ', $autresCriteres);
        }

        $sql = "
				SELECT
					categorie AS categorie_consultation,
					count( c.id ) AS nombre_consultation
				FROM 
					consultation c,
					Organisme o
				WHERE 
					c.organisme = o.acronyme
					AND o.active = 1
					AND c.categorie in (1,2,3)
					AND c.marche_public_simplifie = '1'
				    $sqlAutresCriteres
				GROUP BY  categorie_consultation WITH ROLLUP
			   ";
        $statement = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_ASSOC);
        $listeResultat = [];
        foreach ($statement as $row) {
            $listeResultat[$row['categorie_consultation']] += $row['nombre_consultation'];
        }
        Atexo_Db::closeCommon();

        return $listeResultat;
    }

    /**
     * Retourne de nomdre de consultations MPS ayant reçu au moins une réponse en mode MPS.
     *
     * @param Atexo_Statistiques_CriteriaVo $critere
     *
     * @return array
     *
     * @author Oumar KONATE <oumar.konate@atexom.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function getNbreConsultationsMpsAvecReponsesMps($critere)
    {
        $autresCriteres = self::getCriteresRechercheMps($critere, 'c');
        $sqlAutresCriteres = '';
        if (is_array($autresCriteres) && count($autresCriteres)) {
            $sqlAutresCriteres .= ' AND '.implode(' AND ', $autresCriteres);
        }

        $sql = "
		 		SELECT
		 		categorie AS categorie_consultation,
		 		count( distinct(c.id) ) AS nombre_consultation
		 		FROM 
		 			consultation c, 
					Offres o, 
					t_candidature_mps cand,
					Organisme org
		 		WHERE 
		 			c.id = o.consultation_id 
		 			AND c.organisme = o.organisme
					AND o.id = cand.id_offre 
					AND o.organisme = cand.organisme
					AND c.organisme = org.acronyme
					AND org.active = 1
		 			AND c.categorie in (1,2,3)
		 			AND c.marche_public_simplifie = '1'
		 			AND o.statut_offres != '".Atexo_Config::getParameter('STATUT_ENV_BROUILLON')."'
		 			$sqlAutresCriteres
		 		GROUP BY categorie_consultation WITH ROLLUP
 			  ";
        $statement = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_ASSOC);
        $listeResultat = [];
        foreach ($statement as $row) {
            $listeResultat[$row['categorie_consultation']] += $row['nombre_consultation'];
        }
        Atexo_Db::closeCommon();

        return $listeResultat;
    }

    /**
     * Permet de recuperer les criteres de recherche.
     *
     * @param Atexo_Statistiques_CriteriaVo $criteres
     * @param string                        $alias:   l'alias de la consultation dans la requete sql
     *
     * @return array
     *
     * @author Oumar KONATE <oumar.konate@atexom.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function getCriteresRechercheMps($criteres, $alias)
    {
        $autresCriteres = [];
        if ($criteres) {
            if ($criteres->getIdService()) {
                $autresCriteres[] = ' ('.$alias.'.service_id '.(new Atexo_Db())->quote($criteres->getIdService()).')';
            }
            if ($criteres->getDateMiseEnLigneStart()) {
                $autresCriteres[] = ' '.$alias.".date_mise_en_ligne_calcule >= '".(new Atexo_Db())->quote($criteres->getDateMiseEnLigneStart())." 00:00' ";
            }
            if ($criteres->getDateMiseEnLigneEnd()) {
                $autresCriteres[] = ' '.$alias.".date_mise_en_ligne_calcule <= '".(new Atexo_Db())->quote($criteres->getDateMiseEnLigneEnd())." 23:59' ";
            }
            if (($criteres->getDateMiseEnLigneStart() || $criteres->getDateMiseEnLigneEnd())) {
                $autresCriteres[] = ' '.$alias.'.date_mise_en_ligne_calcule is not null AND '.$alias.".date_mise_en_ligne_calcule NOT LIKE '0000-00-00%' ";
            }
            if ($criteres->getDateRemisePlisStart()) {
                $autresCriteres[] = ' '.$alias.".datefin >= '".(new Atexo_Db())->quote($criteres->getDateRemisePlisStart())." 00:00' ";
            }
            if ($criteres->getDateRemisePlisEnd()) {
                $autresCriteres[] = ' '.$alias.".datefin <= '".(new Atexo_Db())->quote($criteres->getDateRemisePlisEnd())." 23:59' ";
            }
            if (($criteres->getDateRemisePlisStart()) || $criteres->getDateRemisePlisEnd()) {
                $autresCriteres[] = ' '.$alias.'.datefin is not null ';
            }
            if ($criteres->getOrganisme()) {
                $autresCriteres[] = ' '.$alias.".organisme = '".(new Atexo_Db())->quote($criteres->getOrganisme())."'";
            }
            $autresCriteres[] = ' '.$alias.'.date_mise_en_ligne_calcule IS NOT NULL 
 									AND '.$alias.".date_mise_en_ligne_calcule not like '0000-00-00%' 
 					  				AND ".$alias.'.date_mise_en_ligne_calcule<=now() ';
            $autresCriteres[] = ' '.$alias.".id_type_avis = '3' ";
        }

        return $autresCriteres;
    }

    public function nombreContrats(Atexo_Statistiques_CriteriaVo $critere)
    {
        $sql = self::requeteSqlNombreContrats($critere);
        $statement = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_ASSOC);
        $listeResultat = ['Public' => 0, 'MS' => 0, 'Autres' => 0, 'sousTotal1' => 0, 'ac' => 0, 'sad' => 0, 'sousTotal2' => 0, 'total' => 0];
        foreach ($statement as $row) {
            $listeResultat['Public'] = match ($row['type_contrat_statistique']) {
                0 => $row['nombreContrats'],
                1 => $row['nombreContrats'],
                3 => $row['nombreContrats'],
                4 => $row['nombreContrats'],
                default => $row['nombreContrats'],
            };
        }
        Atexo_Db::closeCommon();
        $listeResultat['sousTotal1'] = $listeResultat['Public'] + $listeResultat['MS'] + $listeResultat['Autres'];
        $listeResultat['sousTotal2'] = $listeResultat['ac'] + $listeResultat['sad'];
        $listeResultat['total'] = $listeResultat['sousTotal1'] + $listeResultat['sousTotal2'];

        return $listeResultat;
    }

    /**
     * @return string
     */
    public static function requeteSqlNombreContrats(Atexo_Statistiques_CriteriaVo $critere, $forExportExcel = false)
    {
        $sqlClauseFromSupp = ($critere->isContratTitulairePme()) ? ' , Entreprise ' : ' ';
        $sqlClauseWhereSupp = ($critere->isContratTitulairePme()) ? " AND Entreprise.id = t_contrat_titulaire.id_titulaire AND Entreprise.categorie_entreprise = 'PME' " : ' ';
        $sql = <<<QUERY
				SELECT COUNT(t_contrat_titulaire.id_contrat_titulaire) AS nombreContrats, t_contrat_titulaire.id_type_contrat, t_type_contrat.type_contrat_statistique, libelle_type_contrat, organisme
				FROM t_contrat_titulaire LEFT JOIN t_type_contrat ON t_contrat_titulaire.id_type_contrat = t_type_contrat.id_type_contrat $sqlClauseFromSupp
			    WHERE 1 $sqlClauseWhereSupp
QUERY;
        if ($critere->estRechercheObjetContratChorus()) {
            $sql = <<<QUERY
				SELECT COUNT(t_contrat_titulaire.id_contrat_titulaire) AS nombreContrats, t_contrat_titulaire.id_type_contrat, t_type_contrat.type_contrat_statistique, libelle_type_contrat, organisme
				FROM t_contrat_titulaire LEFT JOIN t_type_contrat ON ( t_contrat_titulaire.id_type_contrat = t_type_contrat.id_type_contrat and t_type_contrat.mode_echange_chorus='1')
			    WHERE t_type_contrat.mode_echange_chorus='1'
QUERY;
        }
        self::ajouterCriteresRequeteSqlNombreContrats($critere, $sql);

        $groupByColumnName = (true === $forExportExcel) ? 'id_type_contrat' : 'type_contrat_statistique';
        $sql .= " GROUP BY t_type_contrat.$groupByColumnName ";

        return $sql;
    }

    /**
     * Permet d'ajouter des criteres supplementaires aux requetes de statistiques des contrats.
     *
     * @param $sql
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since 2017-place
     *
     * @copyright Atexo 2017
     */
    public static function ajouterCriteresRequeteSqlNombreContrats(Atexo_Statistiques_CriteriaVo $critere, &$sql)
    {
        if ($critere->getOrganisme()) {
            $sql .= " AND organisme = '".$critere->getOrganisme()."' ";
        }
        if ($critere->getIdService()) {
            $sql .= ' AND (service_id '.(new Atexo_Db())->quote($critere->getIdService()).')';
        }
        if ($critere->getDatePrevisionnelleNotificationStart()) {
            $sql .= " AND date_prevue_notification >= '".(new Atexo_Db())->quote($critere->getDatePrevisionnelleNotificationStart())." 00:00' ";
        }
        if ($critere->getDatePrevisionnelleNotificationEnd()) {
            $sql .= " AND date_prevue_notification <= '".(new Atexo_Db())->quote($critere->getDatePrevisionnelleNotificationEnd())." 23:59' ";
        }
        if ($critere->getDatePrevisionnelleFinMarcheStart()) {
            $sql .= " AND date_prevue_fin_contrat >= '".(new Atexo_Db())->quote($critere->getDatePrevisionnelleFinMarcheStart())." 00:00' ";
        }
        if ($critere->getDatePrevisionnelleFinMarcheEnd()) {
            $sql .= " AND date_prevue_fin_contrat <= '".(new Atexo_Db())->quote($critere->getDatePrevisionnelleFinMarcheEnd())." 23:59' ";
        }
        if ($critere->getDatePrevisionnelleFinMaximaleMarcheStart()) {
            $sql .= " AND date_prevue_max_fin_contrat >= '".(new Atexo_Db())->quote($critere->getDatePrevisionnelleFinMaximaleMarcheStart())." 00:00' ";
        }
        if ($critere->getDatePrevisionnelleFinMaximaleMarcheEnd()) {
            $sql .= " AND date_prevue_max_fin_contrat <= '".(new Atexo_Db())->quote($critere->getDatePrevisionnelleFinMaximaleMarcheEnd())." 23:59' ";
        }
        if ($critere->getDateDefinitiveNotificationStart()) {
            $sql .= " AND date_notification >= '".(new Atexo_Db())->quote($critere->getDateDefinitiveNotificationStart())." 00:00' ";
        }
        if ($critere->getDateDefinitiveNotificationEnd()) {
            $sql .= " AND date_notification <= '".(new Atexo_Db())->quote($critere->getDateDefinitiveNotificationEnd())." 23:59' ";
        }
        if ($critere->getDateDefinitiveFinMarcheStart()) {
            $sql .= " AND date_fin_contrat >= '".(new Atexo_Db())->quote($critere->getDateDefinitiveFinMarcheStart())." 00:00' ";
        }
        if ($critere->getDateDefinitiveFinMarcheEnd()) {
            $sql .= " AND date_fin_contrat <= '".(new Atexo_Db())->quote($critere->getDateDefinitiveFinMarcheEnd())." 23:59' ";
        }
        if ($critere->getDateMaximaleFinMarcheStart()) {
            $sql .= " AND date_max_fin_contrat >= '".(new Atexo_Db())->quote($critere->getDateMaximaleFinMarcheStart())." 00:00' ";
        }
        if ($critere->getDateMaximaleFinMarcheEnd()) {
            $sql .= " AND date_max_fin_contrat <= '".(new Atexo_Db())->quote($critere->getDateMaximaleFinMarcheEnd())." 23:59' ";
        }
        if ($critere->isContratMulti()) {
            $sql .= ' AND id_contrat_multi IS NOT  NULL ';
        }
        if (!empty($critere->getMontantMin())) {
            $montantMin = str_replace(' ', '', str_replace(',', '.', $critere->getMontantMin()));
            $sql .= " AND ((montant_contrat IS NOT NULL AND montant_contrat >=  '$montantMin') OR (montant_max_estime IS NOT NULL AND montant_max_estime >=  '$montantMin'))";
        }
        if (!empty($critere->getMontantMax())) {
            $montantMax = str_replace(' ', '', str_replace(',', '.', $critere->getMontantMax()));
            $sql .= " AND ((montant_contrat IS NOT NULL AND montant_contrat <=  '$montantMax') OR (montant_max_estime IS NOT NULL AND montant_max_estime <=  '$montantMax'))";
        }
        if (!empty($critere->getStatusContrat())) {
            $sql .= " AND statut_contrat =  '{$critere->getStatusContrat()}' ";
        }
        if (!empty($critere->getObjetRecherche())) {
            if (($critere->getObjetRecherche() == Prado::localize('DEFINE_NBRE_CONTRATS_NOTIFIES')
                || $critere->getObjetRecherche() == Prado::localize('DEFINE_NBRE_CONTRATS_NOTIFIES_PME'))) {
                $sql .= ' AND statut_contrat = '.Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT_EFFECTUEE');
            }
            if ($critere->getObjetRecherche() == Prado::localize('DEFINE_NBRE_CONTRATS_AVEC_EJ_NON_COMMANDES')) {
                //un contrat avec EJ existant mais non commandé <=> numero contrat rempli et num_EJ rempli avec nommage spécifique Chorus et statutEJ vide
                // en regardant la colonne num_EJ avec remplissage valeur # on exclut les contrats des organismes ou services non raccordés à Chorus
                $sql .= " AND (numero_contrat != '' OR numero_contrat is NOT NULL) AND (num_EJ like '%#') AND (statutEJ IS NULL OR statutEJ = '')";
            }
            if ($critere->getObjetRecherche() == Prado::localize('DEFINE_NBRE_CONTRATS_AVEC_EJ_COMMANDES')) {
                //un contrat avec EJ existant et statut commandé <=> numero contrat rempli et num_EJ rempli avec nommage spécifique Chorus et statutEJ se termine par Commandé
                // en regardant la colonne num_EJ avec remplissage valeur # on exclut les contrats des organismes ou services non raccordés à Chorus
                $sql .= " AND (numero_contrat != '' OR numero_contrat is NOT NULL) AND (num_EJ like '%#') AND (statutEJ like '%".Atexo_Config::getParameter('CHORUS_STATUT_EJ_COMMANDE')."#')";
            }
            if ($critere->getObjetRecherche() == Prado::localize('DEFINE_NBRE_CONTRATS_AVEC_EJ_CLOTURES')) {
                //un contrat avec EJ existant et statut commandé <=> numero contrat rempli et num_EJ rempli avec nommage spécifique Chorus et statutEJ se termine par Clôturé
                // en regardant la colonne num_EJ avec remplissage valeur # on exclut les contrats des organismes ou services non raccordés à Chorus
                $sql .= " AND (numero_contrat != '' OR numero_contrat is NOT NULL) AND (num_EJ like '%#') AND (statutEJ like '%".Atexo_Config::getParameter('CHORUS_STATUT_EJ_CLOTURE')."#')";
            }
            if ($critere->getObjetRecherche() == Prado::localize('DEFINE_NBRE_CONTRATS_AVEC_EJ_SUPPRIMES')) {
                //un contrat avec EJ existant et statut commandé <=> numero contrat rempli et num_EJ rempli avec nommage spécifique Chorus et statutEJ se termine par Supprimé
                // en regardant la colonne num_EJ avec remplissage valeur # on exclut les contrats des organismes ou services non raccordés à Chorus
                $sql .= " AND (numero_contrat != '' OR numero_contrat is NOT NULL) AND (num_EJ like '%#') AND (statutEJ like '%".Atexo_Config::getParameter('CHORUS_STATUT_EJ_SUPPRIME')."#')";
            }
        }

        if (Atexo_Module::isEnabled('ConsultationClause')) {
            if ('0' != $critere->getClauseSociale()) {
                $sql .= ' AND clause_sociale = '."'".$critere->getClauseSociale()."'";
            }
            if ('0' != $critere->getClauseEnvironnement()) {
                $sql .= ' AND clause_environnementale = '."'".$critere->getClauseEnvironnement()."'";
            }
        }
    }

    /**
     * Permet de calculer le nombre de contrat pour l'export excel.
     *
     * @return array
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since 2017-place
     *
     * @copyright Atexo 2017
     */
    public static function nombreContratsStatsExportExcel(Atexo_Statistiques_CriteriaVo $critere, $addColsSupplementaires = false)
    {
        try {
            $statement = Atexo_Db::getLinkCommon(true)->query(self::requeteSqlNombreContrats($critere, true), PDO::FETCH_ASSOC);

            $donneesStatistiques = [];
            if (true === $addColsSupplementaires) {
                $listeCp = [];
            }
            $dataStatsExport = [];
            foreach ($statement as $row) {
                $dataStatsExport[$row['id_type_contrat']]['nombreContrats'] = $row['nombreContrats'];
                $dataStatsExport[$row['id_type_contrat']]['libelleTypeContrat'] = $row['libelle_type_contrat'];
            }

            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            $listeTypesContrats = CommonTTypeContratQuery::create()->find($connexion);
            if (!empty($listeTypesContrats)) {
                foreach ($listeTypesContrats as $typesContrats) {
                    if ($typesContrats instanceof CommonTTypeContrat) {
                        $donneesStatistiques[$typesContrats->getIdTypeContrat()] = [
                            'nombreContrats' => (!empty($dataStatsExport[$typesContrats->getIdTypeContrat()]['nombreContrats'])) ? $dataStatsExport[$typesContrats->getIdTypeContrat()]['nombreContrats'] : 0,
                            'libelleTypeContrat' => $typesContrats->getLibelleTypeContrat(),
                        ];
                        if (true === $addColsSupplementaires) {
                            $donneesStatistiques[$typesContrats->getIdTypeContrat()]['organisme'] = $critere->getOrganisme();
                            if (!array_key_exists($critere->getOrganisme(), $listeCp)) {
                                $listeCp[$critere->getOrganisme()] = Atexo_Organismes::getCodePostalOrganisme($critere->getOrganisme());
                            }
                            $donneesStatistiques[$typesContrats->getIdTypeContrat()]['codePostal'] = $listeCp[$critere->getOrganisme()];
                        }
                    }
                }
            }
            Atexo_Db::closeCommon();
        } catch (\Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('app');
            $logger->error('Erreur lors du calcul du nombre de contrats par type de contrat. '.PHP_EOL.'Exception : '.$e->getMessage().PHP_EOL.' Objet Critere: '.PHP_EOL.print_r($critere, true));
        }

        return $donneesStatistiques;
    }

    /**
     * Permet de recuperer les criteres de recherche.
     *
     * @param Atexo_Statistiques_CriteriaVo $criteres
     * @param string                        $alias:   l'alias de la consultation dans la requete sql
     *
     * @return array
     *
     * @author Amal EL BEKKAOUI <amal@atexom.com>
     *
     * @version 1.0
     *
     * @since 2017-esr
     *
     * @copyright Atexo 2017
     */
    public function getCriteresRechercheConsutation($criteres, $alias)
    {
        $autresCriteres = [];
        if ($criteres) {
            if ($criteres->getIdService()) {
                $autresCriteres[] = ' ('.$alias.'.service_id '.(new Atexo_Db())->quote($criteres->getIdService()).')';
            }
            if ($criteres->getDateMiseEnLigneStart()) {
                $autresCriteres[] = ' '.$alias.".date_mise_en_ligne_calcule >= '".(new Atexo_Db())->quote($criteres->getDateMiseEnLigneStart())." 00:00' ";
            }
            if ($criteres->getDateMiseEnLigneEnd()) {
                $autresCriteres[] = ' '.$alias.".date_mise_en_ligne_calcule <= '".(new Atexo_Db())->quote($criteres->getDateMiseEnLigneEnd())." 23:59' ";
            }
            if (($criteres->getDateMiseEnLigneStart() || $criteres->getDateMiseEnLigneEnd())) {
                $autresCriteres[] = ' '.$alias.'.date_mise_en_ligne_calcule is not null AND '.$alias.".date_mise_en_ligne_calcule NOT LIKE '0000-00-00%' ";
            }
            if ($criteres->getDateRemisePlisStart()) {
                $autresCriteres[] = ' '.$alias.".datefin >= '".(new Atexo_Db())->quote($criteres->getDateRemisePlisStart())." 00:00' ";
            }
            if ($criteres->getDateRemisePlisEnd()) {
                $autresCriteres[] = ' '.$alias.".datefin <= '".(new Atexo_Db())->quote($criteres->getDateRemisePlisEnd())." 23:59' ";
            }
            if (($criteres->getDateRemisePlisStart()) || $criteres->getDateRemisePlisEnd()) {
                $autresCriteres[] = ' '.$alias.'.datefin is not null ';
            }
            if ($criteres->getOrganisme()) {
                $autresCriteres[] = ' '.$alias.".organisme = '".(new Atexo_Db())->quote($criteres->getOrganisme())."'";
            }
            $autresCriteres[] = ' '.$alias.'.date_mise_en_ligne_calcule IS NOT NULL
 									AND '.$alias.".date_mise_en_ligne_calcule not like '0000-00-00%'
 					  				AND ".$alias.'.date_mise_en_ligne_calcule<=now() ';
            $autresCriteres[] = ' '.$alias.".id_type_avis = '3' ";
        }

        return $autresCriteres;
    }

    /**
     * Permet de calculer le nbre inscription d'entreprises à la bourse à la cotraitente.
     *
     * @param Atexo_Statistiques_CriteriaVo $criteres
     *
     * @return int
     *
     * @author Amal EL BEKKAOUI <amal@atexom.com>
     *
     * @version 1.0
     *
     * @since 2017-esr
     *
     * @copyright Atexo 2017
     */
    public function getNombreEntreprisesInscritesBourse($criteres)
    {
        $autresCriteres = self::getCriteresRechercheConsutation($criteres, 'c');
        $sqlAutresCriteres = '';
        if (is_array($autresCriteres) && count($autresCriteres)) {
            $sqlAutresCriteres .= implode(' AND ', $autresCriteres);
        }

        $sql = <<<QUERY
 				SELECT
					count(tbc.id_entreprise) as nombre_entreprise
				FROM
					consultation c,
					t_bourse_cotraitance tbc,
					Organisme org
				WHERE
					c.id = tbc.reference_consultation
					AND
						c.organisme = org.acronyme
					AND
						org.active = 1
					AND
 			   		$sqlAutresCriteres
QUERY;
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $statement = $connexion->prepare($sql);
        $statement->execute();
        $res = $statement->fetch(PDO::FETCH_ASSOC);

        return $res['nombre_entreprise'];
    }

    /**
     * Permet de calculer le nbre d'entreprises inscrites à la bourse à la cotraitente.
     *
     * @param Atexo_Statistiques_CriteriaVo $criteres
     *
     * @return int
     *
     * @author Amal EL BEKKAOUI <amal@atexom.com>
     *
     * @version 1.0
     *
     * @since 2017-esr
     *
     * @copyright Atexo 2017
     */
    public function getNombreEntreprisesUniquesInscritesBourse($criteres)
    {
        $autresCriteres = self::getCriteresRechercheConsutation($criteres, 'c');
        $sqlAutresCriteres = '';
        if (is_array($autresCriteres) && count($autresCriteres)) {
            $sqlAutresCriteres .= implode(' AND ', $autresCriteres);
        }

        $sql = <<<QUERY
 				SELECT
					count(distinct (tbc.id_entreprise)) as nombre_entreprise
				FROM
					consultation c,
					t_bourse_cotraitance tbc,
					Organisme org
				WHERE
					c.id = tbc.reference_consultation
					AND
						c.organisme = org.acronyme
					AND
						org.active = 1
					AND
 			   		$sqlAutresCriteres
QUERY;
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $statement = $connexion->prepare($sql);
        $statement->execute();
        $res = $statement->fetch(PDO::FETCH_ASSOC);

        return $res['nombre_entreprise'];
    }

    /**
     * Permet de calculer le nbre consultations concernées par la bourse à la cotraitente.
     *
     * @param Atexo_Statistiques_CriteriaVo $criteres
     *
     * @return int
     *
     * @author Amal EL BEKKAOUI <amal@atexom.com>
     *
     * @version 1.0
     *
     * @since 2017-esr
     *
     * @copyright Atexo 2017
     */
    public function getNombreConsultationsConcerneParBourse($criteres)
    {
        $autresCriteres = self::getCriteresRechercheConsutation($criteres, 'c');
        $sqlAutresCriteres = '';
        if (is_array($autresCriteres) && count($autresCriteres)) {
            $sqlAutresCriteres .= implode(' AND ', $autresCriteres);
        }

        $sql = <<<QUERY
 				SELECT
					count(distinct (c.id)) as nombre_cons
				FROM
					consultation c,
					t_bourse_cotraitance tbc,
					Organisme org
				WHERE
						c.id = tbc.reference_consultation
					AND
						c.organisme = org.acronyme
					AND
						org.active = 1
					AND
 			   		$sqlAutresCriteres
QUERY;
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $statement = $connexion->prepare($sql);
        $statement->execute();
        $res = $statement->fetch(PDO::FETCH_ASSOC);

        return $res['nombre_cons'];
    }

    /**
     * Permet de calculer le nbre reponses par groupement.
     *
     * @param Atexo_Statistiques_CriteriaVo $criteres
     *
     * @return int
     *
     * @author Amal EL BEKKAOUI <amal@atexom.com>
     *
     * @version 1.0
     *
     * @since 2017-esr
     *
     * @copyright Atexo 2017
     */
    public function getNombreReponseEnGroupement($criteres)
    {
        $params = [];
        $autresCriteres = self::getCriteresRechercheConsutation($criteres, 'c');
        $sqlAutresCriteres = '';
        if (is_array($autresCriteres) && count($autresCriteres)) {
            $sqlAutresCriteres .= implode(' AND ', $autresCriteres);
        }

        $sql = <<<QUERY
 				SELECT
		 		count( distinct(o.id) ) AS nombre_reponse
		 		FROM
		 			consultation c,
					Offres o,
					t_groupement_entreprise tge,
					Organisme org
		 		WHERE
		 				c.id = o.consultation_id
		 			AND
		 				c.organisme = o.organisme
					AND
						o.statut_offres != :statutBrouillon
					AND
						o.id = tge.id_offre
					AND
						c.organisme = org.acronyme
					AND
						org.active = 1
					AND
			 			$sqlAutresCriteres
QUERY;
        $params[':statutBrouillon'] = Atexo_Config::getParameter('STATUT_ENV_BROUILLON');
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $statement = $connexion->prepare($sql);
        $statement->execute($params);
        $res = $statement->fetch(PDO::FETCH_ASSOC);

        return $res['nombre_reponse'];
    }
}
