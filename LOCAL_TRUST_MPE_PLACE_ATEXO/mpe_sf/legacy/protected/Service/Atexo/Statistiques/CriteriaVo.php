<?php

namespace Application\Service\Atexo\Statistiques;

/**
 * Vo pour définir les critères de Atexo_Statistique::search().
 *
 * @author Yassine SBAI <yassine.sbai@gmail.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 * @TODO commentaires à compléter
 */
class Atexo_Statistiques_CriteriaVo
{
    private $_selectedEntity;
    private $_valeurCumule;
    private string $idTypeAvis = '';
    private $organisme;
    private string $idTypeProcedure = '';
    private string $idService = '';
    private string $categorieConsultation = '';
    private string $dateMiseEnLigneStart = '';
    private string $dateMiseEnLigneEnd = '';
    private string $dateRemisePlisStart = '';
    private string $dateRemisePlisEnd = '';
    private bool $_dateMiseEnLigneAujourdhui = false;
    private $_reponsesAvecSignature = null;
    private $_reponsesAutotisee = null;
    private $_reponsesObligatoires = null;
    private $_dateDepotRegistre = null;
    private bool $_consultationEnLigne = false;
    private bool $_consultationExterne = false;

    private int $_clauseSocialeConditionExecution = 0;
    private int $_atelierProtege = 0;
    private int $_siae = 0;
    private int $_ess = 0;
    private int $_clauseSocialeInsertion = 0;
    private int $_clauseEnvSpecsTechniques = 0;
    private int $_clauseEnvCondExecution = 0;
    private int $_clauseEnvCriteresSelect = 0;
    // 0= non alloti,1 = alloti, 2= indifferent
    private int $_alloti = 2;
    // Accesseurs
    //Marches publics simplifies, 1=taggué MPS, 0=non taggué MPS
    private int|string $_Mps = 0;
    //Permet de stocker l'objet de la recherche pour le bloc "Recapitulatif"
    private string $_objetRecherche = '';
    //Permet de stocker l'intervalle de recherche pour le bloc "Recapitulatif"
    private string $_intervalleRechercheBlocRecap = '';
    //Permet de stocker la valeur cumulee pour le bloc "Recapitulatif"
    private string $_valeursCumuleesBlocRecap = '';
    //Permet de stocker la valeur du service pour le bloc "Recapitulatif"
    private string $_entiteAchatBlocRecap = '';

    private string $_datePrevisionnelleNotificationStart = '';
    private string $_datePrevisionnelleNotificationEnd = '';
    private string $_datePrevisionnelleFinMarcheStart = '';
    private string $_datePrevisionnelleFinMarcheEnd = '';
    private string $_datePrevisionnelleFinMaximaleMarcheStart = '';
    private string $_datePrevisionnelleFinMaximaleMarcheEnd = '';
    private string $_dateDefinitiveNotificationStart = '';
    private string $_dateDefinitiveNotificationEnd = '';
    private string $_dateDefinitiveFinMarcheStart = '';
    private string $_dateDefinitiveFinMarcheEnd = '';
    private string $_dateMaximaleFinMarcheStart = '';
    private string $_dateMaximaleFinMarcheEnd = '';

    /**
     * @var null
     */
    private $_montantMin = null;
    /**
     * @var null
     */
    private $_montantMax = null;
    /**
     * @var null
     */
    private $statusContrat = null;

    private ?string $rechercheTag = null;

    private bool $contratTitulairePme = false;

    private ?bool $contratMulti = null;

    private int $_clauseSociale = 0;
    private int $_clauseEnvironnement = 0;

    public function getClauseEnvironnement()
    {
        return $this->_clauseEnvironnement;
    }

    public function setClauseEnvironnement($clauseEnvironnement)
    {
        $this->_clauseEnvironnement = $clauseEnvironnement;
    }

    public function getClauseSociale()
    {
        return $this->_clauseSociale;
    }

    public function setClauseSociale($clauseSociale)
    {
        $this->_clauseSociale = $clauseSociale;
    }

    public function getSelectedEntity()
    {
        return $this->_selectedEntity;
    }

    public function setSelectedEntity($selectedEnity)
    {
        $this->_selectedEntity = $selectedEnity;
    }

    public function getValeurCumule()
    {
        return $this->_valeurCumule;
    }

    public function setValeurCumule($valeurCumule)
    {
        $this->_valeurCumule = $valeurCumule;
    }

    public function getIdTypeAvis()
    {
        return $this->idTypeAvis;
    }

    public function setIdTypeAvis($idTypeAvis)
    {
        $this->idTypeAvis = $idTypeAvis;
    }

    public function getOrganisme()
    {
        return $this->organisme;
    }

    public function setOrganisme($organisme)
    {
        $this->organisme = $organisme;
    }

    public function getIdTypeProcedure()
    {
        return $this->idTypeProcedure;
    }

    public function setIdTypeProcedure($idTypeProcedure)
    {
        $this->idTypeProcedure = $idTypeProcedure;
    }

    public function getIdService()
    {
        return $this->idService;
    }

    public function setIdService($idService)
    {
        $this->idService = $idService;
    }

    public function getCategorieConsultation()
    {
        return $this->categorieConsultation;
    }

    public function setCategorieConsultation($categorieConsultation)
    {
        $this->categorieConsultation = $categorieConsultation;
    }

    public function getDateMiseEnLigneStart()
    {
        return $this->dateMiseEnLigneStart;
    }

    public function setDateMiseEnLigneStart($dateMiseEnLigneStart)
    {
        $this->dateMiseEnLigneStart = $dateMiseEnLigneStart;
    }

    public function getDateMiseEnLigneEnd()
    {
        return $this->dateMiseEnLigneEnd;
    }

    public function setDateMiseEnLigneEnd($dateMiseEnLigneEnd)
    {
        $this->dateMiseEnLigneEnd = $dateMiseEnLigneEnd;
    }

    public function getDateRemisePlisStart()
    {
        return $this->dateRemisePlisStart;
    }

    public function setDateRemisePlisStart($dateRemisePlisStart)
    {
        $this->dateRemisePlisStart = $dateRemisePlisStart;
    }

    public function getDateRemisePlisEnd()
    {
        return $this->dateRemisePlisEnd;
    }

    public function setDateRemisePlisEnd($dateRemisePlisEnd)
    {
        $this->dateRemisePlisEnd = $dateRemisePlisEnd;
    }

    public function toString()
    {
        $res = null;
        if ('' !== $this->getCategorieConsultation()) {
            $res .= ' / Categorie='.$this->getCategorieConsultation();
        }

        return $res;
    }

    public function getDateMiseEnLigneAujourdhui()
    {
        return $this->_dateMiseEnLigneAujourdhui;
    }

    public function setDateMiseEnLigneAujourdhui($dateMiseEnLigne)
    {
        $this->_dateMiseEnLigneAujourdhui = $dateMiseEnLigne;
    }

    public function getReponsesAvecSignature()
    {
        return $this->_reponsesAvecSignature;
    }

    public function setReponsesAvecSignature($value)
    {
        $this->_reponsesAvecSignature = $value;
    }

    public function getReponsesAutotisee()
    {
        return $this->_reponsesAutotisee;
    }

    public function setReponsesAutotisee($value)
    {
        $this->_reponsesAutotisee = $value;
    }

    public function getReponsesObligatoires()
    {
        return $this->_reponsesObligatoires;
    }

    public function setReponsesObligatoires($value)
    {
        $this->_reponsesObligatoires = $value;
    }

    public function setDateDepotRegistre($value)
    {
        $this->_dateDepotRegistre = $value;
    }

    public function getDateDepotRegistre()
    {
        return $this->_dateDepotRegistre;
    }

    public function setConsultationEnLigne($value)
    {
        $this->_consultationEnLigne = $value;
    }

    public function getConsultationEnLigne()
    {
        return $this->_consultationEnLigne;
    }

    public function setConsultationExterne($value)
    {
        $this->_consultationExterne = $value;
    }

    public function getConsultationExterne()
    {
        return $this->_consultationExterne;
    }

    public function setClauseSocialeConditionExecution($value)
    {
        $this->_clauseSocialeConditionExecution = $value;
    }

    public function getClauseSocialeConditionExecution()
    {
        return $this->_clauseSocialeConditionExecution;
    }

    public function setClauseSocialeInsertion($value)
    {
        $this->_clauseSocialeInsertion = $value;
    }

    public function getClauseSocialeInsertion()
    {
        return $this->_clauseSocialeInsertion;
    }

    public function setAtelierProtege($value)
    {
        $this->_atelierProtege = $value;
    }

    public function getAtelierProtege()
    {
        return $this->_atelierProtege;
    }

    public function setClauseEnvSpecsTechniques($value)
    {
        $this->_clauseEnvSpecsTechniques = $value;
    }

    public function getClauseEnvSpecsTechniques()
    {
        return $this->_clauseEnvSpecsTechniques;
    }

    public function setClauseEnvCondExecution($value)
    {
        $this->_clauseEnvCondExecution = $value;
    }

    public function getClauseEnvCondExecution()
    {
        return $this->_clauseEnvCondExecution;
    }

    public function setClauseEnvCriteresSelect($value)
    {
        $this->_clauseEnvCriteresSelect = $value;
    }

    public function getClauseEnvCriteresSelect()
    {
        return $this->_clauseEnvCriteresSelect;
    }

    public function setAlloti($value)
    {
        $this->_alloti = $value;
    }

    public function getAlloti()
    {
        return $this->_alloti;
    }

    /**
     * Attributes la valeur de MPS.
     *
     * @param string $value
     *
     * @author Oumar KONATE <oumar.konate@atexom.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function setMps($value)
    {
        $this->_Mps = $value;
    }

    /**
     * Recupère la valeur de MPS.
     *
     * @author Oumar KONATE <oumar.konate@atexom.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function getMps()
    {
        return $this->_Mps;
    }

    /**
     * Permet de recuperer la valeur de l'objet de la recherche.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function getObjetRecherche()
    {
        return $this->_objetRecherche;
    }

    /**
     * Permet d'affecter la valeur de l'objet de la recherche.
     *
     * @param string $value
     *
     * @return string
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function setObjetRecherche($value)
    {
        return $this->_objetRecherche = $value;
    }

    /**
     * Permet d'affecter la valeur de l'intervalle de recherche.
     *
     * @param string $intervalle: intervalle de recherche
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function setIntervalleRechercheBlocRecap($intervalle)
    {
        $this->_intervalleRechercheBlocRecap = $intervalle;
    }

    /**
     * Permet de recuperer la valeur de l'intervalle de recherche.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function getIntervalleRechercheBlocRecap()
    {
        return $this->_intervalleRechercheBlocRecap;
    }

    /**
     * Permet d'affecter la valeur cumulee pour la recherche.
     *
     * @param string $valCumul: valeur cumulee pour la recherche
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function setValeursCumuleesBlocRecap($valCumul)
    {
        $this->_valeursCumuleesBlocRecap = $valCumul;
    }

    /**
     * Permet de recuperer la valeur cumulee pour la recherche.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function getValeursCumuleesBlocRecap()
    {
        return $this->_valeursCumuleesBlocRecap;
    }

    /**
     * Permet d'affecter la valeur de l'entite d'achat pour le bloc recapitulatif.
     *
     * @param string $entite: entite d'achat
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function setEntiteAchatBlocRecap($entite)
    {
        $this->_entiteAchatBlocRecap = $entite;
    }

    /**
     * Permet de recuperer la valeur de l'entite d'achat pour le bloc "Recapitulatif".
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function getEntiteAchatBlocRecap()
    {
        return $this->_entiteAchatBlocRecap;
    }

    /**
     * @return int
     */
    public function getSiae()
    {
        return $this->_siae;
    }

    /**
     * @param int $siae
     */
    public function setSiae($siae)
    {
        $this->_siae = $siae;
    }

    /**
     * @return int
     */
    public function getEss()
    {
        return $this->_ess;
    }

    /**
     * @param int $ess
     */
    public function setEss($ess)
    {
        $this->_ess = $ess;
    }

    /**
     * @return string
     */
    public function getDateDefinitiveFinMarcheEnd()
    {
        return $this->_dateDefinitiveFinMarcheEnd;
    }

    /**
     * @param string $dateDefinitiveFinMarcheEnd
     */
    public function setDateDefinitiveFinMarcheEnd($dateDefinitiveFinMarcheEnd)
    {
        $this->_dateDefinitiveFinMarcheEnd = $dateDefinitiveFinMarcheEnd;
    }

    /**
     * @return string
     */
    public function getDateDefinitiveFinMarcheStart()
    {
        return $this->_dateDefinitiveFinMarcheStart;
    }

    /**
     * @param string $dateDefinitiveFinMarcheStart
     */
    public function setDateDefinitiveFinMarcheStart($dateDefinitiveFinMarcheStart)
    {
        $this->_dateDefinitiveFinMarcheStart = $dateDefinitiveFinMarcheStart;
    }

    /**
     * @return string
     */
    public function getDateDefinitiveNotificationEnd()
    {
        return $this->_dateDefinitiveNotificationEnd;
    }

    /**
     * @param string $dateDefinitiveNotificationEnd
     */
    public function setDateDefinitiveNotificationEnd($dateDefinitiveNotificationEnd)
    {
        $this->_dateDefinitiveNotificationEnd = $dateDefinitiveNotificationEnd;
    }

    /**
     * @return string
     */
    public function getDateDefinitiveNotificationStart()
    {
        return $this->_dateDefinitiveNotificationStart;
    }

    /**
     * @param string $dateDefinitiveNotificationStart
     */
    public function setDateDefinitiveNotificationStart($dateDefinitiveNotificationStart)
    {
        $this->_dateDefinitiveNotificationStart = $dateDefinitiveNotificationStart;
    }

    /**
     * @return string
     */
    public function getDateMaximaleFinMarcheEnd()
    {
        return $this->_dateMaximaleFinMarcheEnd;
    }

    /**
     * @param string $dateMaximaleFinMarcheEnd
     */
    public function setDateMaximaleFinMarcheEnd($dateMaximaleFinMarcheEnd)
    {
        $this->_dateMaximaleFinMarcheEnd = $dateMaximaleFinMarcheEnd;
    }

    /**
     * @return string
     */
    public function getDateMaximaleFinMarcheStart()
    {
        return $this->_dateMaximaleFinMarcheStart;
    }

    /**
     * @param string $dateMaximaleFinMarcheStart
     */
    public function setDateMaximaleFinMarcheStart($dateMaximaleFinMarcheStart)
    {
        $this->_dateMaximaleFinMarcheStart = $dateMaximaleFinMarcheStart;
    }

    /**
     * @return string
     */
    public function getDatePrevisionnelleFinMarcheEnd()
    {
        return $this->_datePrevisionnelleFinMarcheEnd;
    }

    /**
     * @param string $datePrevisionnelleFinMarcheEnd
     */
    public function setDatePrevisionnelleFinMarcheEnd($datePrevisionnelleFinMarcheEnd)
    {
        $this->_datePrevisionnelleFinMarcheEnd = $datePrevisionnelleFinMarcheEnd;
    }

    /**
     * @return string
     */
    public function getDatePrevisionnelleFinMarcheStart()
    {
        return $this->_datePrevisionnelleFinMarcheStart;
    }

    /**
     * @param string $datePrevisionnelleFinMarcheStart
     */
    public function setDatePrevisionnelleFinMarcheStart($datePrevisionnelleFinMarcheStart)
    {
        $this->_datePrevisionnelleFinMarcheStart = $datePrevisionnelleFinMarcheStart;
    }

    /**
     * @return string
     */
    public function getDatePrevisionnelleFinMaximaleMarcheEnd()
    {
        return $this->_datePrevisionnelleFinMaximaleMarcheEnd;
    }

    /**
     * @param string $datePrevisionnelleFinMaximaleMarcheEnd
     */
    public function setDatePrevisionnelleFinMaximaleMarcheEnd($datePrevisionnelleFinMaximaleMarcheEnd)
    {
        $this->_datePrevisionnelleFinMaximaleMarcheEnd = $datePrevisionnelleFinMaximaleMarcheEnd;
    }

    /**
     * @return string
     */
    public function getDatePrevisionnelleFinMaximaleMarcheStart()
    {
        return $this->_datePrevisionnelleFinMaximaleMarcheStart;
    }

    /**
     * @param string $datePrevisionnelleFinMaximaleMarcheStart
     */
    public function setDatePrevisionnelleFinMaximaleMarcheStart($datePrevisionnelleFinMaximaleMarcheStart)
    {
        $this->_datePrevisionnelleFinMaximaleMarcheStart = $datePrevisionnelleFinMaximaleMarcheStart;
    }

    /**
     * @return string
     */
    public function getDatePrevisionnelleNotificationEnd()
    {
        return $this->_datePrevisionnelleNotificationEnd;
    }

    /**
     * @param string $datePrevisionnelleNotificationEnd
     */
    public function setDatePrevisionnelleNotificationEnd($datePrevisionnelleNotificationEnd)
    {
        $this->_datePrevisionnelleNotificationEnd = $datePrevisionnelleNotificationEnd;
    }

    /**
     * @return string
     */
    public function getDatePrevisionnelleNotificationStart()
    {
        return $this->_datePrevisionnelleNotificationStart;
    }

    /**
     * @param string $datePrevisionnelleNotificationStart
     */
    public function setDatePrevisionnelleNotificationStart($datePrevisionnelleNotificationStart)
    {
        $this->_datePrevisionnelleNotificationStart = $datePrevisionnelleNotificationStart;
    }

    /**
     * @return null
     */
    public function getMontantMax()
    {
        return $this->_montantMax;
    }

    /**
     * @param null $montantMax
     */
    public function setMontantMax($montantMax)
    {
        $this->_montantMax = $montantMax;
    }

    /**
     * @return null
     */
    public function getMontantMin()
    {
        return $this->_montantMin;
    }

    /**
     * @param null $montantMin
     */
    public function setMontantMin($montantMin)
    {
        $this->_montantMin = $montantMin;
    }

    /**
     * @return null
     */
    public function getStatusContrat()
    {
        return $this->statusContrat;
    }

    /**
     * @param null $statusContrat
     */
    public function setStatusContrat($statusContrat)
    {
        $this->statusContrat = $statusContrat;
    }

    public function estRechercheObjetContrat($output = false)
    {
        if (4 == $this->getRechercheTag()) {
            $output = true;
        }

        return $output;
    }

    public function estRechercheObjetContratChorus($output = false)
    {
        if (5 == $this->getRechercheTag()) {
            $output = true;
        }

        return $output;
    }

    /**
     * @return string
     */
    protected function getRechercheTag()
    {
        return $this->rechercheTag;
    }

    /**
     * @param string $rechercheTag
     */
    public function setRechercheTag($rechercheTag)
    {
        $this->rechercheTag = $rechercheTag;
    }

    /**
     * @return bool
     */
    public function isContratMulti()
    {
        return $this->contratMulti;
    }

    /**
     * @param bool $contratMulti
     */
    public function setContratMulti($contratMulti)
    {
        $this->contratMulti = $contratMulti;
    }

    /**
     * @return bool
     */
    public function isContratTitulairePme()
    {
        return $this->contratTitulairePme;
    }

    /**
     * @param bool $contratTitulairePme
     */
    public function setContratTitulairePme($contratTitulairePme)
    {
        $this->contratTitulairePme = $contratTitulairePme;
    }
}
