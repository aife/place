<?php

namespace Application\Service\Atexo\Statistiques;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonMesureAvancementPeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Util;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Statistiques_MesuresAvancement
{
    public function retreiveMesureAvancementByServices($idService, $organisme, $annee, $trimestre)
    {
        $listeMesures = [];
        $sql = 'Select Mesure_avancement.id_mesure as id, service_id, annee, trimestre, etat, '
           .' Mesure_avancement.date_creation, Mesure_avancement.date_envoi, Mesure_avancement.date_accuse, '
           .' Service.libelle,Mesure_avancement.siren,nic,identifiant_service,Mesure_avancement.mail,type_pouvoir_adjudicateur,departement '
           .' from Mesure_avancement left join Service on Mesure_avancement.service_id=Service.id AND Mesure_avancement.organisme=Service.organisme '
           ." where Mesure_avancement.service_id ='".(new Atexo_Db())->quote($idService)."' AND Mesure_avancement.organisme='".$organisme."' "
           ." and annee ='".(new Atexo_Db())->quote($annee)."'"
           ." and trimestre='T".(new Atexo_Db())->quote($trimestre)."'";

        $statement = Atexo_Db::getLinkCommon(true)->query($sql);
        foreach ($statement as $row) {
            if ($row[1]) {
                $libelleService = $row[8];
            } else {
                $organismeO = Atexo_Organismes::retrieveOrganismeByAcronyme($organisme);
                $libelleService = $organismeO->getDenominationOrg();
            }
            $mesure['id'] = $row[0];
            $mesure['service_id'] = $row[1];
            $mesure['annee'] = $row[2];
            $mesure['trimestre'] = $row[3];
            $mesure['etat'] = $row[4];

            if ($row[4] == Atexo_Config::getParameter('ETAT_ENVOYE_BOAMP_MESURE_DEMAT')) {
                $mesure['libelleEtat'] = 'Envoyé';
            } elseif ($row[4] == Atexo_Config::getParameter('ETAT_BROUILLON_BOAMP_MESURE_DEMAT') || $row[4] = ' ') {
                $mesure['libelleEtat'] = 'Brouillon';
            }
            $mesure['dateCreation'] = $row[5];
            $mesure['dateEnvoi'] = ($row[6]) ? Atexo_Util::iso2frnDateTime($row[6]) : '-';
            $mesure['dateAccuse'] = ($row[7]) ? Atexo_Util::iso2frnDateTime($row[7]) : '-';
            $mesure['libelleService'] = $libelleService;
            $mesure['siren'] = $row[9];
            $mesure['nic'] = $row[10];
            $mesure['identifiant_service'] = $row[11];
            $mesure['mail'] = $row[12];
            $mesure['type_pouvoir_adjudicateur'] = $row[13];
            $mesure['departement'] = $row[14];
            $listeMesures[] = $mesure;
        }

        return $listeMesures;
    }

    public function retreiveMesureAvancementById($id, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $mesure = CommonMesureAvancementPeer::retrieveByPK($id, $organisme, $connexion);

        return $mesure;
    }

    public function retreiveMesureAvancementByServiceAnneeTrimestre($idService, $organisme, $annee, $trimestre)
    {
        $criteria = new Criteria();
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $criteria->add(CommonMesureAvancementPeer::ORGANISME, $organisme);
        $criteria->add(CommonMesureAvancementPeer::SERVICE_ID, $idService);
        $criteria->add(CommonMesureAvancementPeer::ANNEE, $annee);
        $criteria->add(CommonMesureAvancementPeer::TRIMESTRE, 'T'.$trimestre);
        $v = CommonMesureAvancementPeer::doSelect($criteria, $connexion);

        return !empty($v) > 0 ? $v[0] : null;
    }

    public function retreiveMesureAvancementMultiCritere($organisme, $arrayCrit)
    {
        $criteria = new Criteria();
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));

        $criteria->add(CommonMesureAvancementPeer::ORGANISME, $organisme);
        $criteria->add(CommonMesureAvancementPeer::SERVICE_ID, $arrayCrit['idService']);
        $criteria->add(CommonMesureAvancementPeer::ANNEE, $arrayCrit['annee']);
        $criteria->add(CommonMesureAvancementPeer::TRIMESTRE, 'T'.$arrayCrit['trimestre']);

        $criteria->add(CommonMesureAvancementPeer::SIREN, $arrayCrit['siren']);
        $criteria->add(CommonMesureAvancementPeer::NIC, $arrayCrit['nic']);
        $criteria->add(CommonMesureAvancementPeer::MAIL, $arrayCrit['mail']);
        $criteria->add(CommonMesureAvancementPeer::IDENTIFIANT_SERVICE, $arrayCrit['identifiantservice']);
        $criteria->add(CommonMesureAvancementPeer::TYPE_POUVOIR_ADJUDICATEUR, $arrayCrit['typepouvoir']);
        $criteria->add(CommonMesureAvancementPeer::DEPARTEMENT, $arrayCrit['depart']);
        $v = CommonMesureAvancementPeer::doSelect($criteria, $connexion);

        return !empty($v) > 0 ? $v[0] : null;
    }

    // mise a jour de l'etat d'envois Boamp mesure demat.
    public function UpdateEtatFormulaire($organisme, $idMesure, $etat, $nameFichierXmlGenere)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $mesure = CommonMesureAvancementPeer::retrieveByPK($idMesure, $organisme, $connexion);
        if ($mesure) {
            $mesure->setEtat($etat);
            $mesure->setDateEnvoi(date('Y-m-d H:s'));
            $mesure->setNameXmlGenere($nameFichierXmlGenere);
            $mesure->save($connexion);

            return true;
        } else {
            return false;
        }
    }

    public function IsMesureAvancementExist($organisme, $arrayCrit)
    {
        $criteria = new Criteria();
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $criteria->add(CommonMesureAvancementPeer::SERVICE_ID, $arrayCrit['idService']);
        $criteria->add(CommonMesureAvancementPeer::ORGANISME, $organisme);
        $criteria->add(CommonMesureAvancementPeer::ANNEE, $arrayCrit['annee']);
        $criteria->add(CommonMesureAvancementPeer::TRIMESTRE, $arrayCrit['trimestre']);
        $v = CommonMesureAvancementPeer::doSelect($criteria, $connexion);

        return !empty($v) > 0 ? $v[0] : null;
    }
}
