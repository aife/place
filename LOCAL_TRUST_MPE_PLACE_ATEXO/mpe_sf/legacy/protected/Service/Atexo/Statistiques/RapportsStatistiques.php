<?php

namespace Application\Service\Atexo\Statistiques;

use Application\Service\Atexo\Atexo_Config;
use Jaspersoft\Client\Client;

/**
 * Permet de gerer les rapports de statistiques.
 *
 * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
 *
 * @version 1.0
 *
 * @since 4.11.0
 *
 * @copyright Atexo 2015
 */
class Atexo_Statistiques_RapportsStatistiques
{
    public static function getCritereStatistiquesByUri($uri)
    {
        $c = self::construireClient();

        return $c->reportService()->getReportInputControls($uri);
    }

    public static function construireClient()
    {
        return new Client(
            Atexo_Config::getParameter('URL_JASPER'),
            Atexo_Config::getParameter('JASPER_LOGIN'),
            Atexo_Config::getParameter('JASPER_PASSWORD')
        );
    }

    public static function getResultatRechercheRapport($controls, $uri, $type = 'html')
    {
        $c = self::construireClient();
        $data = $c->reportService()->runReport($uri, $type, null, null, $controls, true, false, true, false, null, !('pdf' == $type));
        if ('html' != $type) {
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Description: File Transfer');
            header('Content-Disposition: attachment; filename=report.'.$type);
            header('Content-Transfer-Encoding: binary');
            header('Content-Length: '.strlen($data));
            header('Content-Type: application/'.$type);

            echo $data;

            return;
        }
        $data = str_replace('<td width="50%">&nbsp;</td>', '', $data);
        $data = preg_replace('#<img (.*)>#', '<img src="/themes/images/report/logo-republique-francaise.png" style="height: 52px" alt="">', $data);

        return $data;
    }
}
