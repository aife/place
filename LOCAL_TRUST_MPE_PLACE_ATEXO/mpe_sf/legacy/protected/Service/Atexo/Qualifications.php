<?php

namespace Application\Service\Atexo;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonQualificationPeer;
use Prado\Prado;

/**
 * Classe de.
 *
 * @author Mouslim MITALI
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Qualifications
{
    /**
     * mets les qualifications  dans le cache de l'application.
     */
    public function setQualificationsInCache($active = false)
    {
        $cachedQualifications_key = 'cachedQualifications';
        if ($active) {
            $cachedQualifications_key = $cachedQualifications_key.'_active';
        }
        $cachedQualifications = Prado::getApplication()->Cache->get($cachedQualifications_key);

        if (!$cachedQualifications) {
            $c = new Criteria();
            if ($active) {
                $c->add(CommonQualificationPeer::ACTIVE, 1, Criteria::EQUAL);
            }
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            $listeQualif = CommonQualificationPeer::doSelect($c, $connexionCom);
            $dataNiveau0 = [];
            $dataNiveau1 = [];
            $dataNiveau2 = [];
            $dataNiveau3 = [];
            $arId0 = [];
            $arId1 = [];
            $arId2 = [];
            $arId3 = [];
            $indexCat = '';
            foreach ($listeQualif as $indexCat => $oneQualif) {
                $langueActive = Atexo_Languages::retrieveActiveLangages();
                $coefIdSousCat = explode('.', $oneQualif->getIdInterne());
                if (1 == count($coefIdSousCat)) {
                    $dataNiveau0[$oneQualif->getIdInterne()]['id'] = $oneQualif->getIdInterne();
                    $dataNiveau0[$oneQualif->getIdInterne()]['libelle'] = $oneQualif->getLibelle();
                    foreach ($langueActive as $lang) {
                        $langue = $lang->getLangue();
                        $getLibelleTraduit = 'getLibelle'.ucwords($langue);
                        $dataNiveau0[$oneQualif->getIdInterne()]["libelle_$langue"] = $oneQualif->$getLibelleTraduit();
                    }
                    $arId0[$oneQualif->getIdInterne()]['id'] = $oneQualif->getIdInterne();
                }
                if (2 == count($coefIdSousCat)) {
                    $dataNiveau1[$oneQualif->getIdInterne()]['id'] = $oneQualif->getIdInterne();
                    $dataNiveau1[$oneQualif->getIdInterne()]['libelle'] = $oneQualif->getLibelle();
                    foreach ($langueActive as $lang) {
                        $langue = $lang->getLangue();
                        $getLibelleTraduit = 'getLibelle'.ucwords($langue);
                        $dataNiveau1[$oneQualif->getIdInterne()]["libelle_$langue"] = $oneQualif->$getLibelleTraduit();
                    }
                    $dataNiveau1[$oneQualif->getIdInterne()]['niv0'] = $coefIdSousCat[0];

                    $arId1[$oneQualif->getId()] = $coefIdSousCat[0] * 100 + $coefIdSousCat[1];
                } elseif (3 == count($coefIdSousCat)) {
                    $dataNiveau2[$oneQualif->getIdInterne()]['id'] = $oneQualif->getIdInterne();
                    $dataNiveau2[$oneQualif->getIdInterne()]['libelle'] = $oneQualif->getLibelle();
                    foreach ($langueActive as $lang) {
                        $langue = $lang->getLangue();
                        $getLibelleTraduit = 'getLibelle'.ucwords($langue);
                        $dataNiveau2[$oneQualif->getIdInterne()]["libelle_$langue"] = $oneQualif->$getLibelleTraduit();
                    }
                    $dataNiveau2[$oneQualif->getIdInterne()]['niv1'] = $coefIdSousCat[0].'.'.$coefIdSousCat[1];
                    $dataNiveau2[$oneQualif->getIdInterne()]['niv0'] = $coefIdSousCat[0];

                    $arId2[$oneQualif->getIdInterne()] = $coefIdSousCat[0] * 10000 + $coefIdSousCat[1] * 100 + $coefIdSousCat[2];
                } elseif (4 == count($coefIdSousCat)) {
                    $dataNiveau3[$oneQualif->getIdInterne()]['id'] = $oneQualif->getIdInterne();
                    $dataNiveau3[$oneQualif->getIdInterne()]['libelle'] = $oneQualif->getLibelle();
                    foreach ($langueActive as $lang) {
                        $langue = $lang->getLangue();
                        $getLibelleTraduit = 'getLibelle'.ucwords($langue);
                        $dataNiveau3[$oneQualif->getIdInterne()]["libelle_$langue"] = $oneQualif->$getLibelleTraduit();
                    }
                    $dataNiveau3[$oneQualif->getIdInterne()]['niv2'] = $coefIdSousCat[0].'.'.$coefIdSousCat[1].'.'.$coefIdSousCat[2];
                    $dataNiveau3[$oneQualif->getIdInterne()]['niv1'] = $coefIdSousCat[0].'.'.$coefIdSousCat[1];
                    $dataNiveau3[$oneQualif->getIdInterne()]['niv0'] = $coefIdSousCat[0];

                    $arId3[$oneQualif->getIdInterne()] = $coefIdSousCat[0] * 10000 + $coefIdSousCat[1] * 100 + $coefIdSousCat[2] * 10 + $coefIdSousCat[1];
                }
            }
            // tri
            array_multisort($arId1, SORT_ASC, $dataNiveau1);
            array_multisort($arId2, SORT_ASC, $dataNiveau2);
            array_multisort($arId3, SORT_ASC, $dataNiveau3);
            // imbriquation des tableaux

            foreach ($dataNiveau3 as $catNiv) {
                $dataNiveau2[$catNiv['niv2']]['SousNiveau'][$catNiv['id']] = $catNiv;
            }
            foreach ($dataNiveau2 as $catNiv) {
                $dataNiveau1[$catNiv['niv1']]['SousNiveau'][$catNiv['id']] = $catNiv;
            }
            foreach ($dataNiveau1 as $catNiv) {
                $dataNiveau0[$catNiv['niv0']]['SousNiveau'][$catNiv['id']] = $catNiv;
            }
            $cachedQualifications = $dataNiveau0;
            Prado::getApplication()->Cache->set($cachedQualifications_key, $cachedQualifications, Atexo_Config::getParameter('TTL_PRADO_CACHE'));
        }

        return $cachedQualifications;
    }

    public function getCachedQualifications($active = false)
    {
        return self::setQualificationsInCache($active);
    }

    /**
     * retourne la liste des qualifications identifiés dans le tableau en param.
     */
    public function reteiveQualificationsByArrayIds($arrayId)
    {
        $c = new Criteria();
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonQualificationPeer::ID_INTERNE, $arrayId, Criteria::IN);
        $liste = CommonQualificationPeer::doSelect($c, $connexionCom);
        if ($liste) {
            return $liste;
        } else {
            return [];
        }
    }

    /**
     * retourne un tableau qui contient tout les niveaux possible d'un id interne donné.
     */
    public function getLibelleQualificationByIdInterne($idInterne, $dataSource)
    {
        $coefIdSousCat = explode('.', $idInterne);
        $count = count($coefIdSousCat);
        $langue = Atexo_CurrentUser::readFromSession('lang');
        $champ_libelle = 'libelle_'.$langue;
        $libelle = '';
        //type
        if (1 == $count) {
            $idParent = $coefIdSousCat[0];
            if (0 == strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE'))
                || !$dataSource[$coefIdSousCat[0]][$champ_libelle]) {
                $libelle = $dataSource[$coefIdSousCat[0]]['libelle'];
            } else {
                $libelle = $dataSource[$coefIdSousCat[0]][$champ_libelle];
            }
        }
        //secteur
        elseif (2 == $count) {
            $idParent = $coefIdSousCat[0].'.'.$coefIdSousCat[1];
            if (0 == strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE'))
                || !$dataSource[$coefIdSousCat[0]]['SousNiveau'][$idParent][$champ_libelle]) {
                $libelle = $dataSource[$coefIdSousCat[0]]['SousNiveau'][$idParent]['libelle'];
            } else {
                $libelle = $dataSource[$coefIdSousCat[0]]['SousNiveau'][$idParent][$champ_libelle];
            }
        }
        //qualification
        elseif (3 == $count) {
            $idParent = $coefIdSousCat[0].'.'.$coefIdSousCat[1].'.'.$coefIdSousCat[2];
            if (0 == strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE'))
                || !$dataSource[$coefIdSousCat[0]]['SousNiveau'][$coefIdSousCat[0].'.'.$coefIdSousCat[1]]['SousNiveau'][$idParent][$champ_libelle]) {
                $libelle = $dataSource[$coefIdSousCat[0]]['SousNiveau'][$coefIdSousCat[0].'.'.$coefIdSousCat[1]]['SousNiveau'][$idParent]['libelle'];
            } else {
                $libelle = $dataSource[$coefIdSousCat[0]]['SousNiveau'][$coefIdSousCat[0].'.'.$coefIdSousCat[1]]['SousNiveau'][$idParent][$champ_libelle];
            }
        }
        //classe
        elseif (4 == $count) {
            $idParent = $coefIdSousCat[0].'.'.$coefIdSousCat[1].'.'.$coefIdSousCat[2].'.'.$coefIdSousCat[3];
            if (0 == strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE'))
                || !$dataSource[$coefIdSousCat[0]]['SousNiveau'][$coefIdSousCat[0].'.'.$coefIdSousCat[1]]['SousNiveau'][$coefIdSousCat[0].'.'.$coefIdSousCat[1].'.'.$coefIdSousCat[2]]['SousNiveau'][$idParent][$champ_libelle]) {
                $libelle = $dataSource[$coefIdSousCat[0]]['SousNiveau'][$coefIdSousCat[0].'.'.$coefIdSousCat[1]]['SousNiveau'][$coefIdSousCat[0].'.'.$coefIdSousCat[1].'.'.$coefIdSousCat[2]]['SousNiveau'][$idParent]['libelle'];
            } else {
                $libelle = $dataSource[$coefIdSousCat[0]]['SousNiveau'][$coefIdSousCat[0].'.'.$coefIdSousCat[1]]['SousNiveau'][$coefIdSousCat[0].'.'.$coefIdSousCat[1].'.'.$coefIdSousCat[2]]['SousNiveau'][$idParent][$champ_libelle];
            }
        }

        return $libelle;
    }

    /**
     * prend en paramettre des id séparés avec des #.
     */
    public function displayListeLibelleDomaineByArrayId($idsQualification, $returnArray = false)
    {
        $libelleQualif = '';
        $arrayQualif = [];
        $dataSource = self::getCachedQualifications();
        if ($idsQualification) {
            $arIds = explode('#', $idsQualification);
            foreach ($arIds as $oneId) {
                if ($oneId) {
                    $type = '-';
                    $secteur = '-';
                    $qualification = '-';
                    $classe = '-';
                    $coefIdSousCat = explode('.', $oneId);
                    $count = count($coefIdSousCat);
                    $type = '-';
                    if (1 == $count) {
                        $idParent = $coefIdSousCat[0];
                        $type = self::getLibelleQualificationByIdInterne($idParent, $dataSource);
                    }
                    //secteur
                    elseif (2 == $count) {
                        $idParent = $coefIdSousCat[0].'.'.$coefIdSousCat[1];
                        $type = self::getLibelleQualificationByIdInterne($coefIdSousCat[0], $dataSource);
                        $secteur = self::getLibelleQualificationByIdInterne($idParent, $dataSource);
                    }
                    //qualification
                    elseif (3 == $count) {
                        $idParent = $coefIdSousCat[0].'.'.$coefIdSousCat[1].'.'.$coefIdSousCat[2];
                        $type = self::getLibelleQualificationByIdInterne($coefIdSousCat[0], $dataSource);
                        $secteur = self::getLibelleQualificationByIdInterne($coefIdSousCat[0].'.'.$coefIdSousCat[1], $dataSource);
                        $qualification = self::getLibelleQualificationByIdInterne($idParent, $dataSource);
                    }
                    //classe
                    elseif (4 == $count) {
                        $idParent = $coefIdSousCat[0].'.'.$coefIdSousCat[1].'.'.$coefIdSousCat[2].'.'.$coefIdSousCat[3];
                        $type = self::getLibelleQualificationByIdInterne($coefIdSousCat[0], $dataSource);
                        $secteur = self::getLibelleQualificationByIdInterne($coefIdSousCat[0].'.'.$coefIdSousCat[1], $dataSource);
                        $qualification = self::getLibelleQualificationByIdInterne($coefIdSousCat[0].'.'.$coefIdSousCat[1].'.'.$coefIdSousCat[2], $dataSource);
                        $classe = self::getLibelleQualificationByIdInterne($idParent, $dataSource);
                    }
                    if (!$returnArray) {
                        $type = ('-' == $type) ? '' : $type;
                        $secteur = ('-' == $secteur) ? '' : ' / '.$secteur;
                        $qualification = ('-' == $qualification) ? '' : ' / '.$qualification;
                        $classe = ('-' == $classe) ? '' : ' / Classe '.$classe;
                        $libelleQualif .= $type.$secteur.$qualification.$classe.' , ';
                    } else {
                        $type = ('-' == $type) ? '' : $type;
                        $secteur = ('-' == $secteur) ? '' : $secteur;
                        $qualification = ('-' == $qualification) ? '' : $qualification;
                        $classe = ('-' == $classe) ? '' : $classe;
                        $arrayQualif[] = ['type' => $type, 'secteur' => $secteur, 'qualification' => $qualification, 'classe' => $classe];
                    }
                }
            }
        }
        if (!$returnArray) {
            return substr($libelleQualif, 0, -2);
        } else {
            return $arrayQualif;
        }
    }

    public function reteiveQualificationsByLibelle($libelle)
    {
        $c = new Criteria();
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonQualificationPeer::LIBELLE, $libelle);
        $qualification = CommonQualificationPeer::doSelectOne($c, $connexionCom);
        if ($qualification) {
            return $qualification;
        } else {
            return false;
        }
    }

    public function retrieveListeAllQualifications()
    {
        $c = new Criteria();
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $qualification = CommonQualificationPeer::doSelect($c, $connexionCom);

        return $qualification;
    }
}
