<?php

namespace Application\Service\Atexo;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonTReferentielCertificat;
use Application\Propel\Mpe\CommonTReferentielCertificatPeer;

/**
 * Classe de manipulation des referentiels des cerificats.
 *
 * @author Ezziani Loubna <loubna.ezziani@atexo.com>
 * @copyright Atexo 2012
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_ReferentielCertificat
{
    /*
     * Permet de retourner un referentiel de certificat
     * @param : $name = le nom de referentiel
     */
    public function getReferentielCertifByName($name)
    {
        $c = new Criteria();
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonTReferentielCertificatPeer::NOM_REFERENTIEL_CERTIFICAT, $name);
        $referentiel = CommonTReferentielCertificatPeer::doSelectOne($c, $connexion);
        if ($referentiel) {
            return $referentiel;
        } else {
            return false;
        }
    }

    public function getStatutReferentielCertificat($referentielName)
    {
        $statut = 1;
        $referentielCertif = self::getReferentielCertifByName($referentielName);
        if ($referentielCertif instanceof CommonTReferentielCertificat) {
            $statut = $referentielCertif->getStatutReferentielCertificat();
        }

        return $statut;
    }
}
