<?php

namespace Application\Service\Atexo;

use DOMDocument;

/**
 * Classe de.
 *
 * @author Khalid BENAMAR <khalid.benamar@atexo.com>
 * @copyright Atexo 2009
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_InterfaceWs
{
    private string $encoding = 'utf-8';

    public function __construct($encoding = 'utf-8')
    {
        $this->encoding = $encoding;
    }

    public function getXmlTemplate($extractionDonneesSocle = false, $modeFonctionnementSocleExterne = false)
    {
        $domDocument = new DOMDocument('1.0', $this->encoding);

        //<Transaction>
        // |
        $transaction = $domDocument->createElement('Transaction', '');
        $domDocument->appendChild($transaction);
        // |
        // |___ Attributs Transaction :
        //          |
        //          |____ attribut Version
        (new Atexo_Xml())->addAttribute($domDocument, $transaction, 'Version', '1.0');
        //          |____ attribut TimeStamp
        (new Atexo_Xml())->addAttribute($domDocument, $transaction, 'TimeStamp', date('c'));
        //          |____ attribut xmlns:xsi
        (new Atexo_Xml())->addAttribute($domDocument, $transaction, 'xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
        //          |____ attribut xsi:noNamespaceSchemaLocation
        if ($extractionDonneesSocle) {
            if ($modeFonctionnementSocleExterne) {
                $nameSpaceSchemaLocation = 'PPP-EB-extraction-reprise-donnees.xsd';
            } else {
                $nameSpaceSchemaLocation = 'interfaces-socle-interne.xsd';
            }
        } else {
            $nameSpaceSchemaLocation = 'InterfacesDefense.xsd';
        }
        (new Atexo_Xml())->addAttribute($domDocument, $transaction, 'xsi:noNamespaceSchemaLocation', $nameSpaceSchemaLocation);
        // |
        // |
        // |___ <Control> :
        $response = $domDocument->createElement('Response');
        $transaction->appendChild($response);

        //          |
        return $domDocument;
    }
}
