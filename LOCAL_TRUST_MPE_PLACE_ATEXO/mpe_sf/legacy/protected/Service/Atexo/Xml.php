<?php

namespace Application\Service\Atexo;

/**
 * Outils de manipulation de flux XML pour MPE.
 *
 * @author Guillaume Ponçon <guillaume.poncon@openstates.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Xml
{
    /**
     * Retourne le contenu d'un fichier XML, sans les commentaires.
     *
     * @todo supprimer cette fonction après migration en php 5.2
     *
     * @deprecated à partir de php 5.2.x
     *
     * @param string $xmlFile
     *
     * @return string
     */
    public static function getXmlContent($xmlFile)
    {
        $contentTab = file($xmlFile);
        $xmlContent = '';
        foreach ($contentTab as $line) {
            $line = trim($line);
            if (!$line || '<!--' == substr($line, 0, 4)) {
                continue;
            }
            $xmlContent .= utf8_encode($line)."\n";
        }

        return $xmlContent;
    }

    public function addAttribute($domDocument, $masterTag, $attributeName, $attributeContent)
    {
        $attribute = $domDocument->createAttribute($attributeName);
        $masterTag->appendChild($attribute);
        $attributeValue = $domDocument->createTextNode($attributeContent);
        $attribute->appendChild($attributeValue);
    }

    public function addElement($domDocument, $masterTag, $elementName, $content = '')
    {
        $element = $domDocument->createElement($elementName, $content);
        $masterTag->appendChild($element);

        return $element;
    }
}
