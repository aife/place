<?php

namespace Application\Service\Atexo;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonAdministrateurPeer;

class Atexo_Admin
{
    public function retrieveAdmin($idAdmin)
    {
        $c = new Criteria();
        $c->add(CommonAdministrateurPeer::ID, $idAdmin, Criteria::EQUAL);
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $adminObject = CommonAdministrateurPeer::doSelectOne($c, $connexionCom);

        return $adminObject ?: false;
    }

    public function retrieveAdminByLogin($login)
    {
        $c = new Criteria();
        $c->add(CommonAdministrateurPeer::LOGIN, $login, Criteria::EQUAL);
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $adminObject = CommonAdministrateurPeer::doSelectOne($c, $connexionCom);

        return $adminObject ?: false;
    }

    public function retrieveAdminByLoginAndMdp($login, $password)
    {
        $c = new Criteria();
        $c->add(CommonAdministrateurPeer::LOGIN, $login, Criteria::EQUAL);
        $c->add(CommonAdministrateurPeer::MDP, $password, Criteria::EQUAL);
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $adminObject = CommonAdministrateurPeer::doSelectOne($c, $connexionCom);

        return $adminObject ?: false;
    }

    /*
     * Recuperer Admin par Mail.
     */
    public function retrieveAdminByMail($email)
    {
        $c = new Criteria();
        $c->add(CommonAdministrateurPeer::EMAIL, $email, Criteria::EQUAL);
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $adminObject = CommonAdministrateurPeer::doSelectOne($c, $connexionCom);

        return $adminObject ?: false;
    }
}
