<?php

namespace Application\Service\Atexo;

use Exception;
use Laminas\Http\Client;
use Laminas\Http\Request;

/**
 * Classe de gestion de  l'Utilitaire de Transmission et suivi d'Avancement Hotline.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 *
 * @version 1.0
 *
 * @since 2015-place
 *
 * @copyright Atexo 2016
 */
class Atexo_Utah
{
    /**
     * Permet de recuperer le token d'acces a l'application UTAH.
     *
     * @param string $contexte : contexte en json
     *
     * @return mixed
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     *
     * @return bool
     */
    public function recupererToken($contexte)
    {
        $params = '[uri : '.Atexo_Config::getParameter('URL_FAQ_RECUPERATION_TOKEN')."] , [contextes: $contexte]";

        $logger = Atexo_LoggerManager::getLogger();
        $logger->info('Debut requete http POST pour recuperation du token');
        $logger->info("Params de la requete : $params");

        try {
            $client = new Client();
            $key_assistance = 'URL_UTAH_RECUPERATION_TOKEN';
            $contentTypeJson = 'application/json';
            $rawData = $contexte;
            if (Atexo_Config::getParameter('UTILISER_FAQ')) {
                $key_assistance = 'URL_FAQ_RECUPERATION_TOKEN';
                $rawData = json_encode(['data' => $contexte, 'ip' => ''], JSON_THROW_ON_ERROR);
            }
            $client->setUri(Atexo_Config::getParameter($key_assistance));
            $client->setMethod(Request::METHOD_POST);
            $client->setEncType($contentTypeJson);
            $client->setRawBody($rawData);
            $reponse = $client->send();
            $reponse = $reponse->getBody();
            if (!empty($reponse)) {
                $decodeReponse = json_decode($reponse, null, 512, JSON_THROW_ON_ERROR);
                if (is_object($decodeReponse)) {
                    if (!empty($decodeReponse->token)) {
                        $token = $decodeReponse->token;
                        $logger->info("Token recupere avec SUCCES: $token");

                        return $token;
                    } else {
                        $logger->info('Echec recuperation du token. Details: '.$decodeReponse->error);
                    }
                }
            } else {
                $logger->info('Echec recuperation du token. La reponse du ws est vide.');
            }
            $logger->info('Fin requete http POST pour recuperation du token');
        } catch (Exception $e) {
            $logger->error('Erreur lors de la recuperation du token UTAH'.PHP_EOL."Params: $params".PHP_EOL.'Exception: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString());
        }
        $logger->info('Fin requete http POST pour recuperation du token');

        return false;
    }

    public static function getUrlAssistance()
    {
        $key_assistance = 'URL_UTAH';
        if (Atexo_Config::getParameter('URL_FAQ')) {
            $key_assistance = 'URL_FAQ';
        }

        return Atexo_MultiDomaine::replaceDomain(Atexo_Config::getParameter($key_assistance));
    }
}
