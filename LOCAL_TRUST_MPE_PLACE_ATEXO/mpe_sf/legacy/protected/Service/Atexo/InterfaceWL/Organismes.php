<?php

namespace Application\Service\Atexo\InterfaceWL;

use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_InterfaceWs;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_Xml;

/**
 * Classe de generation des exports XML des organismes pour la migration des socles Worldline.
 *
 * @author JAP <ameline.placide@atexo.com>
 *
 * @since 4.6.4
 *
 * @version $Id: Organismes.php,v 1.1.2.1 2014-12-31 09:01:24 japlacide Exp $
 *
 * @copyright Atexo 2014
 */
class Atexo_InterfaceWL_Organismes extends Atexo_InterfaceWs
{
    public function getXmlResponse(&$t_elements, &$a_count)
    {
        $domDocument = self::getXmlTemplate(true, true);
        $transaction = $domDocument->documentElement;
        $response = $transaction->getElementsByTagName('Response')->item(0);
        $parent_node = $domDocument->createElement('Organisms');
        $response->appendChild($parent_node);

        foreach ($t_elements as &$elm) {
            $node = $domDocument->createElement('Organism');
            $parent_node->appendChild($node);
            (new Atexo_Xml())->addAttribute($domDocument, $node, 'organismId', $elm['id_initial']); //$organismId

            $organismeTest = '1';
            if ($elm['active']) {
                $organismeTest = '0';
            }

            (new Atexo_Xml())->addAttribute($domDocument, $node, 'article', $elm['type_article_org']);
            (new Atexo_Xml())->addAttribute($domDocument, $node, 'acronym', $elm['acronyme']);
            (new Atexo_Xml())->addAttribute($domDocument, $node, 'denomination', Atexo_Util::toUtf8($elm['denomination_org']));
            (new Atexo_Xml())->addAttribute($domDocument, $node, 'categorieJuridique', $elm['categorie_insee']);
            (new Atexo_Xml())->addAttribute($domDocument, $node, 'organismeTest', $organismeTest);
            (new Atexo_Xml())->addAttribute($domDocument, $node, 'description', Atexo_Util::toUtf8($elm['description_org']));

            (new Atexo_Xml())->addAttribute($domDocument, $node, 'siren', $elm['siren']);
            (new Atexo_Xml())->addAttribute($domDocument, $node, 'nicCode', $elm['complement']);

            (new Atexo_Xml())->addAttribute($domDocument, $node, 'address', Atexo_Util::toUtf8($elm['adresse'].' '.$elm['adresse2']));
            (new Atexo_Xml())->addAttribute($domDocument, $node, 'postalCode', $elm['cp']);
            (new Atexo_Xml())->addAttribute($domDocument, $node, 'city', Atexo_Util::toUtf8($elm['ville']));

            //Reccuperation de l'email
            $email = $elm['email'];
            //Filtrage de l'adresse email avec la fonction native de PHP pour Worldline
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $email = '';
            }
            (new Atexo_Xml())->addAttribute($domDocument, $node, 'email', Atexo_Util::toUtf8($email));
            (new Atexo_Xml())->addAttribute($domDocument, $node, 'urlSite', (base64_encode($elm['url']))); //urlSite

            /**
             * Recuperation des logos via le filesystem.
             */
            $dirPath = Atexo_Config::getParameter('BASE_ROOT_DIR').'/'.$elm['acronyme'].Atexo_Config::getParameter('PATH_ORGANISME_IMAGE');

            if (is_dir($dirPath)) {
                $bigLogo = $dirPath.'logo-organisme-grand.jpg';
                $smallLogo = $dirPath.'logo-organisme-petit.jpg';
                if (is_file($smallLogo)) {
                    (new Atexo_Xml())->addAttribute($domDocument, $node, 'petitLogo', base64_encode(file_get_contents($smallLogo)));
                }
                if (is_file($bigLogo)) {
                    (new Atexo_Xml())->addAttribute($domDocument, $node, 'grandLogo', base64_encode(file_get_contents($bigLogo)));
                }
            }

            //Incrementation du compteur de noeuds XML
            ++$a_count;
        }

        $buffer = $domDocument->saveXml();

        //Liberation espace memoire
        unset($domDocument);
        unset($t_elements);

        return $buffer;
    }
}
