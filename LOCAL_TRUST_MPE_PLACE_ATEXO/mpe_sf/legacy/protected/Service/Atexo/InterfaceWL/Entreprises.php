<?php

namespace Application\Service\Atexo\InterfaceWL;

use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Entreprise;
use Application\Service\Atexo\Atexo_InterfaceWs;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_Xml;

/**
 * Classe de generation des exports XML des entreprises et des utilisateurs pour la migration des socles Worldline.
 *
 * @author JAP <ameline.placide@atexo.com>
 *
 * @since 4.6.4
 *
 * @version $Id: Entreprises.php,v 1.1.2.1 2014-12-31 09:01:24 japlacide Exp $
 *
 * @copyright Atexo 2014
 */
class Atexo_InterfaceWL_Entreprises extends Atexo_InterfaceWs
{
    public function getXmlEntreprises(&$t_elements, &$a_count)
    {
        $domDocument = self::getXmlTemplate(true, true);

        $transaction = $domDocument->documentElement;
        $response = $transaction->getElementsByTagName('Response')->item(0);
        $companies = $domDocument->createElement('Companies');
        $response->appendChild($companies);

        foreach ($t_elements as &$elm) {
            $companie = $domDocument->createElement('Company');
            $companies->appendChild($companie);

            if (Atexo_Util::toUtf8($elm['paysenregistrement']) == Atexo_Config::getParameter('DENOMINATION2_GEON2_FRANCE') || Atexo_Util::toUtf8($elm['paysenregistrement']) == Atexo_Config::getParameter('DENOMINATION1_GEON2_FRANCE')) {
                $paysEnregistrement = '1';
                if ('' != $elm['siren']) {
                    (new Atexo_Xml())->addAttribute($domDocument, $companie, 'siren', $elm['siren']);
                }
                if ('' != $elm['nicsiege']) {
                    (new Atexo_Xml())->addAttribute($domDocument, $companie, 'nicCode', $elm['nicsiege']);
                }
                if ('' != $elm['codeape']) {
                    (new Atexo_Xml())->addAttribute($domDocument, $companie, 'apeNafNaceCode', Atexo_Util::toUtf8($elm['codeape']));
                }
            } else {
                $paysEnregistrement = '0';
                (new Atexo_Xml())->addAttribute($domDocument, $companie, 'nationalId', Atexo_Util::toUtf8($elm['sirenetranger']));
            }

            (new Atexo_Xml())->addAttribute($domDocument, $companie, 'paysEnregistrementFrance', $paysEnregistrement); // test sur les valeurs a saisir(boolean)

            (new Atexo_Xml())->addAttribute($domDocument, $companie, 'id', Atexo_Util::toUtf8($elm['id_initial']));
            (new Atexo_Xml())->addAttribute($domDocument, $companie, 'name', Atexo_Util::toUtf8($elm['nom']));

            (new Atexo_Xml())->addAttribute($domDocument, $companie, 'address', Atexo_Util::toUtf8($elm['adresse'].' '.$elm['adresse2']));
            (new Atexo_Xml())->addAttribute($domDocument, $companie, 'postalCode', Atexo_Util::toUtf8($elm['codepostal']));
            (new Atexo_Xml())->addAttribute($domDocument, $companie, 'city', Atexo_Util::toUtf8($elm['villeadresse']));
            (new Atexo_Xml())->addAttribute($domDocument, $companie, 'country', Atexo_Util::toUtf8($elm['paysadresse']));
            (new Atexo_Xml())->addAttribute($domDocument, $companie, 'phone', Atexo_Util::toUtf8($elm['telephone']));
            if ('' != $elm['fax']) {
                (new Atexo_Xml())->addAttribute($domDocument, $companie, 'fax', Atexo_Util::toUtf8($elm['fax']));
            }
            (new Atexo_Xml())->addAttribute($domDocument, $companie, 'legalStatus', Atexo_Util::toUtf8($elm['formejuridique']));

            // Managers
            // Manager
            $dirigents = (new Atexo_Entreprise())->retreiveDirigentsCompany($elm['id']); //$elm['id']
            if ($dirigents) {
                $managers = $domDocument->createElement('Managers');
                $companie->appendChild($managers);
                foreach ($dirigents as $oneDirigent) {
                    $manager = $domDocument->createElement('Manager');
                    $managers->appendChild($manager);
                    (new Atexo_Xml())->addAttribute($domDocument, $manager, 'firstname', Atexo_Util::toUtf8($oneDirigent->getPrenom()));
                    (new Atexo_Xml())->addAttribute($domDocument, $manager, 'lastname', Atexo_Util::toUtf8($oneDirigent->getNom()));
                    (new Atexo_Xml())->addAttribute($domDocument, $manager, 'jobFunction', Atexo_Util::toUtf8($oneDirigent->getQualite()));
                }
            }

            if ($elm['site_internet']) {
                $webSite = $domDocument->createElement('WebSite');
                $companie->appendChild($webSite);
                $site = $domDocument->createCDATASection(Atexo_Util::toUtf8($elm['site_internet']));
                $webSite->appendChild($site);
            }
            if ($elm['description_activite']) {
                $activityDescription = $domDocument->createElement('ActivityDescription');
                $companie->appendChild($activityDescription);
                $activity = $domDocument->createCDATASection(Atexo_Util::toUtf8($elm['description_activite']));
                $activityDescription->appendChild($activity);
            }

            //Incrementation du compteur de noeuds XML
            ++$a_count;
        }

        return $domDocument->saveXml();
    }

    public function getXmlEtablissements(&$t_elements, &$a_count)
    {
        $domDocument = self::getXmlTemplate(true, true);

        $transaction = $domDocument->documentElement;
        $response = $transaction->getElementsByTagName('Response')->item(0);
        $companies = $domDocument->createElement('Companies');
        $response->appendChild($companies);

        foreach ($t_elements as &$elm) {
            $establishment = $domDocument->createElement('Establishment');
            $companies->appendChild($establishment);

            (new Atexo_Xml())->addAttribute($domDocument, $establishment, 'id', $elm['id_initial']);

            (new Atexo_Xml())->addAttribute($domDocument, $establishment, 'companyId', $elm['entreprise_id']);

            (new Atexo_Xml())->addAttribute($domDocument, $establishment, 'nicCode', Atexo_Util::toUtf8($elm['code_etablissement']));

            (new Atexo_Xml())->addAttribute($domDocument, $establishment, 'estSiege', Atexo_Util::toUtf8($elm['est_siege']));

            (new Atexo_Xml())->addAttribute($domDocument, $establishment, 'address', Atexo_Util::toUtf8($elm['adresse'].' '.$elm['adresse2']));
            (new Atexo_Xml())->addAttribute($domDocument, $establishment, 'postalCode', Atexo_Util::toUtf8($elm['code_postal']));
            (new Atexo_Xml())->addAttribute($domDocument, $establishment, 'city', Atexo_Util::toUtf8($elm['ville']));
            (new Atexo_Xml())->addAttribute($domDocument, $establishment, 'country', Atexo_Util::toUtf8($elm['pays']));

            (new Atexo_Xml())->addAttribute($domDocument, $establishment, 'saisieManuelle', Atexo_Util::toUtf8($elm['saisie_manuelle']));
            (new Atexo_Xml())->addAttribute($domDocument, $establishment, 'statutActif', Atexo_Util::toUtf8($elm['statut_actif']));
            (new Atexo_Xml())->addAttribute($domDocument, $establishment, 'tvaIntracommunautaire', Atexo_Util::toUtf8($elm['tva_intracommunautaire']));

            //Incrementation du compteur de noeuds XML
            ++$a_count;
        }

        return $domDocument->saveXml();
    }

    public function getXmlInscrits(&$t_elements, &$a_count)
    {
        $domDocument = self::getXmlTemplate(true, true);

        $transaction = $domDocument->documentElement;
        $response = $transaction->getElementsByTagName('Response')->item(0);
        $companies = $domDocument->createElement('Companies');
        $response->appendChild($companies);

        foreach ($t_elements as &$elm) {
            $employee = $domDocument->createElement('Employee');
            $companies->appendChild($employee);

            (new Atexo_Xml())->addAttribute($domDocument, $employee, 'id', $elm['id_initial']);

            (new Atexo_Xml())->addAttribute($domDocument, $employee, 'companyId', $elm['entreprise_id']);
            (new Atexo_Xml())->addAttribute($domDocument, $employee, 'establishmentId', $elm['etablissement_id']);

            (new Atexo_Xml())->addAttribute($domDocument, $employee, 'nicCode', Atexo_Util::toUtf8($elm['siret']));

            (new Atexo_Xml())->addAttribute($domDocument, $employee, 'login', Atexo_Util::toUtf8($elm['login']));
            (new Atexo_Xml())->addAttribute($domDocument, $employee, 'password', sha1($elm['mdp']));

            (new Atexo_Xml())->addAttribute($domDocument, $employee, 'title', Atexo_Util::toUtf8('NA')); //civile, pas renseignee
            (new Atexo_Xml())->addAttribute($domDocument, $employee, 'firstname', Atexo_Util::toUtf8($elm['prenom']));
            (new Atexo_Xml())->addAttribute($domDocument, $employee, 'lastname', Atexo_Util::toUtf8($elm['nom']));

            $compteActif = '1';
            if ($elm['bloque']) {
                $compteActif = '0';
            }
            (new Atexo_Xml())->addAttribute($domDocument, $employee, 'compteActif', $compteActif);
            (new Atexo_Xml())->addAttribute($domDocument, $employee, 'profilSDM', $elm['profil']);

            //Verification du format de l'email
            $email = $elm['email'];
            //Filtrage de l'adresse email avec la fonction native de PHP pour Worldline
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $email = '';
            }
            (new Atexo_Xml())->addAttribute($domDocument, $employee, 'email', Atexo_Util::toUtf8($email));

            (new Atexo_Xml())->addAttribute($domDocument, $employee, 'phone', Atexo_Util::toUtf8($elm['telephone']));
            (new Atexo_Xml())->addAttribute($domDocument, $employee, 'fax', Atexo_Util::toUtf8($elm['fax']));

            (new Atexo_Xml())->addAttribute($domDocument, $employee, 'address', Atexo_Util::toUtf8($elm['adresse'].' '.$elm['adresse2']));
            (new Atexo_Xml())->addAttribute($domDocument, $employee, 'postalCode', Atexo_Util::toUtf8($elm['codepostal']));
            (new Atexo_Xml())->addAttribute($domDocument, $employee, 'city', Atexo_Util::toUtf8($elm['ville']));
            (new Atexo_Xml())->addAttribute($domDocument, $employee, 'country', Atexo_Util::toUtf8($elm['pays']));

            //Incrementation du compteur de noeuds XML
            ++$a_count;
        }

        return $domDocument->saveXml();
    }
}
