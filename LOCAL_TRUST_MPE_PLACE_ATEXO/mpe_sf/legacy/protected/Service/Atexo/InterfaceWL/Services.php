<?php

namespace Application\Service\Atexo\InterfaceWL;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonAgentServiceMetierPeer;
use Application\Propel\Mpe\CommonServiceMertierProfilsQuery;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_InterfaceWs;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_Xml;

/**
 * Classe de generation des exports XML des entites et des agents pour la migration des socles Worldline.
 *
 * @author JAP <ameline.placide@atexo.com>
 *
 * @since 4.6.4
 *
 * @version $Id: Services.php,v 1.1.2.1 2014-12-31 09:01:24 japlacide Exp $
 *
 * @copyright Atexo 2014
 */
class Atexo_InterfaceWL_Services extends Atexo_InterfaceWs
{
    public function getXmlServices(&$t_elements, &$a_count)
    {
        $domDocument = self::getXmlTemplate(true, true);
        $transaction = $domDocument->documentElement;
        $response = $transaction->getElementsByTagName('Response')->item(0);
        $entities = $domDocument->createElement('Entities');

        $response->appendChild($entities);

        foreach ($t_elements as &$elm) {
            $entity = $domDocument->createElement('Entity');
            $entities->appendChild($entity);

            if ($elm['parentId']) {
                $idParent = $elm['parentId'];
            } else {
                $idParent = '0';
            }

            (new Atexo_Xml())->addAttribute($domDocument, $entity, 'organismId', $elm['id_initial_organism']);
            if ('' != $elm['siren']) { // on ajoute l'attribut siren juste s'il est renseigne
                (new Atexo_Xml())->addAttribute($domDocument, $entity, 'siren', $elm['siren']);
            }
            (new Atexo_Xml())->addAttribute($domDocument, $entity, 'acronymOrganism', $elm['acronymOrganism']);

            (new Atexo_Xml())->addAttribute($domDocument, $entity, 'id', $elm['id_initial']);

            (new Atexo_Xml())->addAttribute($domDocument, $entity, 'parentId', $idParent);
            (new Atexo_Xml())->addAttribute($domDocument, $entity, 'title', Atexo_Util::toUtf8($elm['title']));
            (new Atexo_Xml())->addAttribute($domDocument, $entity, 'sigle', $elm['acronym']);

            (new Atexo_Xml())->addAttribute($domDocument, $entity, 'address', Atexo_Util::toUtf8($elm['address']));
            (new Atexo_Xml())->addAttribute($domDocument, $entity, 'postalCode', $elm['postalCode']);
            (new Atexo_Xml())->addAttribute($domDocument, $entity, 'city', $elm['city']);
            (new Atexo_Xml())->addAttribute($domDocument, $entity, 'phone', $elm['phone']);
            (new Atexo_Xml())->addAttribute($domDocument, $entity, 'fax', $elm['fax']);
            (new Atexo_Xml())->addAttribute($domDocument, $entity, 'country', $elm['country']);

            $email = $elm['email'];
            //Filtrage de l'adresse email avec la fonction native de PHP
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $email = '';
            }
            (new Atexo_Xml())->addAttribute($domDocument, $entity, 'email', Atexo_Util::toUtf8($email));

            //Incrementation du compteur de noeuds XML
            ++$a_count;
        }

        return $domDocument->saveXml();
    }

    public function getXmlAgents(&$t_elements, &$a_count)
    {
        $domDocument = self::getXmlTemplate(true, true);
        $transaction = $domDocument->documentElement;
        $response = $transaction->getElementsByTagName('Response')->item(0);
        $entities = $domDocument->createElement('Entities');

        $response->appendChild($entities);

        //Tableaux temporaires pour stocker les identifiants
        $_t_services = [];
        $_t_organismes = [];

        foreach ($t_elements as &$elm) {
            $agent = $domDocument->createElement('Agent');
            $entities->appendChild($agent);

            (new Atexo_Xml())->addAttribute($domDocument, $agent, 'id', Atexo_Util::toUtf8($elm['id_initial']));
            (new Atexo_Xml())->addAttribute($domDocument, $agent, 'firstname', Atexo_Util::toUtf8($elm['prenom']));
            (new Atexo_Xml())->addAttribute($domDocument, $agent, 'lastname', Atexo_Util::toUtf8($elm['nom']));

            (new Atexo_Xml())->addAttribute($domDocument, $agent, 'login', Atexo_Util::toUtf8($elm['login']));
            (new Atexo_Xml())->addAttribute($domDocument, $agent, 'password', $elm['password']);

            if ($elm['tentatives_mdp'] >= '3') {
                $tentative = 0;
            } else {
                $tentative = 1;
            }
            (new Atexo_Xml())->addAttribute($domDocument, $agent, 'compteActif', $tentative);

            //Verification du format de l'email
            $email = $elm['email'];
            if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
                //Filtrage de l'adresse email avec la fonction native de PHP pour Worldline
                $email = '';
            }
            (new Atexo_Xml())->addAttribute($domDocument, $agent, 'email', Atexo_Util::toUtf8($email));

            (new Atexo_Xml())->addAttribute($domDocument, $agent, 'phone', Atexo_Util::toUtf8($elm['num_tel']));
            (new Atexo_Xml())->addAttribute($domDocument, $agent, 'fax', Atexo_Util::toUtf8($elm['num_fax']));

            //Recherche de l'id_initial du service
            if ($elm['service_id']) {
                $idService = $elm['service_id'];
            } else {
                $idService = 0;
            }
            (new Atexo_Xml())->addAttribute($domDocument, $agent, 'entityId', Atexo_Util::toUtf8($idService));

            (new Atexo_Xml())->addAttribute($domDocument, $agent, 'acronymOrganism', $elm['organisme']);
            (new Atexo_Xml())->addAttribute($domDocument, $agent, 'organismId', $elm['organisme_id']);

            (new Atexo_Xml())->addAttribute($domDocument, $agent, 'elu', $elm['elu']);

            if (2 == $elm['type_comm']) { // TYPE COMMUNICATION EMAIL
                $typeCommEmail = '1';
            } else {
                $typeCommEmail = '0';
            }
            (new Atexo_Xml())->addAttribute($domDocument, $agent, 'typeCommunicationCAOPrefEmail', $typeCommEmail);
            (new Atexo_Xml())->addAttribute($domDocument, $agent, 'jobFunction', Atexo_Util::toUtf8($elm['nom_fonction']));

            //l'ajout de la balise 'Applications' d'acces aux applications pour l'agent
            $applications = $domDocument->createElement('Applications');
            $agent->appendChild($applications);
            $cr = new Criteria();

            $cr->add(CommonAgentServiceMetierPeer::ID_AGENT, $elm['id']);
            $lesApplications = CommonAgentServiceMetierPeer::doSelect($cr, Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY')));

            if ($lesApplications) {
                foreach ($lesApplications as $oneApplication) {
                    $applicationId = $oneApplication->getIdServiceMetier();
                    $profilId = $oneApplication->getIdProfilService();

                    $profilIdExterne = CommonServiceMertierProfilsQuery::create()
                        ->filterByIdServiceMetier($applicationId)
                        ->filterByIdInterne($profilId)
                        ->select(['id_externe'])
                        ->findOne();

                    $application = $domDocument->createElement('Application');
                    $applications->appendChild($application);

                    (new Atexo_Xml())->addAttribute($domDocument, $application, 'applicationId', $applicationId);
                    (new Atexo_Xml())->addAttribute($domDocument, $application, 'profilId', $profilId);
                }
            }

            //Incrementation du compteur de noeuds XML
            ++$a_count;
        }

        return $domDocument->saveXml();
    }
}
