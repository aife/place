<?php

namespace Application\Service\Atexo\Dume;

use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonCategorieLot;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonOffres;
use Application\Propel\Mpe\CommonOrganisme;
use Application\Propel\Mpe\CommonService;
use Application\Propel\Mpe\CommonTCandidature;
use Application\Propel\Mpe\CommonTCandidatureQuery;
use Application\Propel\Mpe\CommonTDumeContexte;
use Application\Propel\Mpe\CommonTDumeContexteQuery;
use Application\Propel\Mpe\CommonTDumeNumero;
use Application\Propel\Mpe\CommonTDumeNumeroQuery;
use Application\Propel\Mpe\CommonTTypeProcedureDume;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_JmsJobs;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Config\Atexo_Config_Exception;
use AtexoDume\Client\Client;
use AtexoDume\Dto\Acheteur\Consultation;
use AtexoDume\Dto\Acheteur\Metadata;
use AtexoDume\Dto\Lot;
use AtexoDume\Dto\NumeroDume;
use AtexoDume\Dto\ReponseListPublishedDume;
use DateTime;
use Exception;
use Prado\Prado;

/**
 * Class Atexo_Dume_AtexoDume.
 */
class Atexo_Dume_AtexoDume
{
    const INTERFACE_DUME_LOGGER = 'dume';

    /**
     * Permet de retourner l'objet a envoyé à lt_dume.
     *
     * @param CommonConsultation $consultation
     * @param $lotToDelete
     * @param $lotToAdd
     * @param $modification
     *
     * @return AtexoDume\Dto\Acheteur\Consultation
     *
     * @throws Atexo_Config_Exception
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1.0
     *
     * @since 2017-esr
     *
     * @copyright Atexo 2018
     */
    public static function getObjetConsultationDume($consultation, $lotToDelete = null, $lotToAdd = null, $modification = null)
    {
        $consDume = new Consultation();
        if ($consultation instanceof CommonConsultation) {
            $consDume->setIntitule(Atexo_Util::toUtf8(Atexo_Util::atexoHtmlEntitiesDecode($consultation->getIntitule())));
            $consDume->setObjet(Atexo_Util::toUtf8(Atexo_Util::atexoHtmlEntitiesDecode($consultation->getObjet())));

            if(Atexo_Config::getParameter('MODULE_DUME_NOM_COMPLET')) {
                $nomOrganisme = $consultation->getOrganisme();
                $organismeObjet = Atexo_Organismes::retrieveOrganismeByAcronyme($nomOrganisme);
                if ($organismeObjet instanceof CommonOrganisme) {
                    $nomOfficiel = $organismeObjet->getDenominationOrg();
                    $CpVille =  null;
                    if ($organismeObjet->getCp() && $organismeObjet->getVille()) {
                        $CpVille = ' ('.$organismeObjet->getCp() . ' - ' . $organismeObjet->getVille() .')';
                    }

                    if (null !== $consultation->getServiceId())  {
                        $service = (new Atexo_EntityPurchase())->getEntityById($consultation->getServiceId(), $nomOrganisme);
                        if ($service instanceof CommonService && $service->getCp() && $service->getVille()) {
                            $nomOfficiel .= ' - ' . $service->getCheminComplet();
                            $CpVille = ' ('.$service->getCp() . ' - ' . $service->getVille() .')';
                        }
                    }
                    $nomOfficiel .= $CpVille;
                }
            } else {
                if ($consultation->getOrgDenomination() && Atexo_Module::isEnabled('RechercheAutoCompletion')) {
                    $nomOfficiel = $consultation->getOrgDenomination();
                } else {
                    $nomOfficiel = $consultation->getDenominationOrganisme(null, true);
                }
            }

            $consDume->setNomOfficiel(Atexo_Util::toUtf8(Atexo_Util::atexoHtmlEntitiesDecode($nomOfficiel)));
            $consDume->setReferenceConsultation(Atexo_Util::toUtf8(Atexo_Util::atexoHtmlEntitiesDecode($consultation->getReferenceUtilisateur())));

            $typeProcedueDumeCons = $consultation->getTypeProcedureDumeObject();
            $typeProcedueDumeCode = '';
            $typeProcedueDumeLabel = '';

            if ($typeProcedueDumeCons instanceof CommonTTypeProcedureDume) {
                $typeProcedueDumeCode = $typeProcedueDumeCons->getCodeDume();
                $typeProcedueDumeLabel = Atexo_Util::toUtf8(Atexo_Util::atexoHtmlEntitiesDecode($typeProcedueDumeCons->getLibelle()));
            }

            $consDume->setTypeProcedure($typeProcedueDumeLabel);

            $metadata = new Metadata();
            $dateFin = $consultation->getDatefin();

            if (null != $dateFin && '0000-00-00 00:00:00' != $dateFin) {
                $metadata->setDateLimiteRemisePlis(Atexo_Util::getDateTimeByFormat($dateFin, 'c'));
            }

            $metadata->setIdConsultation($consultation->getId());
            $metadata->setIdentifiantPlateforme(Atexo_Config::getParameter('UID_PF_MPE'));
            $metadata->setOrganisme($consultation->getOrganisme());
            $metadata->setSiret($consultation->getSiretAcheteur());
            $metadata->setIdTypeProcedure($typeProcedueDumeCode);
            $metadata->setIdTypeMarche($consultation->getCodeDumeTypeContrat());
            $metadata->setIdNatureMarket($consultation->getCodeDumeCategorieConsultation());
            $metadata->setSimplifie((1 == $consultation->getTypeFormulaireDume()));
            $consDume->setMetadata($metadata);

            if ($consultation->getAlloti()) {
                $lots = $consultation->getAllLots();

                $lotsDume = [];

                if ('deleteAll' != $lotToDelete) {
                    foreach ($lots as $lot) {
                        if ($lot instanceof CommonCategorieLot) {
                            if ($lotToDelete != $lot->getLot()) {
                                if (true === $modification && $lotToAdd['idLot'] == $lot->getLot()) {
                                    $numLot = $lotToAdd['numLot'];
                                    $intituleLot = Atexo_Util::toUtf8(Atexo_Util::atexoHtmlEntitiesDecode($lotToAdd['intituleLot']));
                                } else {
                                    $numLot = $lot->getLot();
                                    $intituleLot = Atexo_Util::toUtf8(Atexo_Util::atexoHtmlEntitiesDecode($lot->getDescription()));
                                }

                                $lotDume = new Lot();
                                $lotDume->setIntituleLot($intituleLot);
                                $lotDume->setNumeroLot($numLot);
                                $lotsDume[] = $lotDume;
                            }
                        }
                    }

                    if (false === $modification && !empty($lotToAdd)) {
                        $lotDume = new Lot();
                        $lotDume->setIntituleLot(Atexo_Util::toUtf8(Atexo_Util::atexoHtmlEntitiesDecode($lotToAdd['intituleLot'])));
                        $lotDume->setNumeroLot($lotToAdd['numLot']);
                        $lotsDume[] = $lotDume;
                    }
                }

                $consDume->setLots($lotsDume);
            }
        }

        return $consDume;
    }

    /**
     * Permet de retourner l'id contexte dume.
     *
     * @param CommonConsultation $consultation
     * @param int                $idContexteDume
     *
     * @return int
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1.0
     *
     * @since 2017-esr
     *
     * @copyright Atexo 2018
     */
    public static function getIdContexteDume($consultation, $idContexteDume = 0)
    {
        $logger = Atexo_LoggerManager::getLogger(self::INTERFACE_DUME_LOGGER);
        $results = 0;
        try {
            $c = new Client(
                Atexo_Config::getParameter('URL_PF_DUME_API'),
                Atexo_Config::getParameter('LOGIN_DUME_API'),
                Atexo_Config::getParameter('PASSWORD_DUME_API'),
                $logger,
                null
            );
            $logger->info('DEBUT fct getIdContexteDume : appel à la library mpe_dume : consultation ' . $consultation->getId() . ' , idContexte  ' . $idContexteDume);
            $consDume = self::getObjetConsultationDume($consultation);
            $logger->info("fct getIdContexteDume : creation de l'objet consultation dume : " . print_r($consDume, true) . ' de la consultation ' . $consultation->getId());

            if ($idContexteDume) {
                $logger->info('fct getIdContexteDume : update dume acheteur encours pour consultation ' . $consultation->getId() . ' =>  params : consultation ' . print_r($consDume, true) . ' , idContextLtDume ' . $idContexteDume);
                $results = $c->acheteurService()->updateDume($consDume, $idContexteDume);
                $logger->info('fct getIdContexteDume : update dume acheteur  effectue pour consultation ' . $consultation->getId() . ' et idContexte  ' . $idContexteDume . ' =>  retour :' . $results);
            } else {
                $logger->info('fct getIdContexteDume : creation dume acheteur encours pour consultation ' . $consultation->getId() . ' =>  params : consultation ' . print_r($consDume, true));
                $results = $c->acheteurService()->createDume($consDume);
                $logger->info('fct getIdContexteDume : creation dume acheteur effectue pour consultation ' . $consultation->getId() . ' => retour : idContextLtDume ' . $results);
            }
            $logger->info('FIN fct getIdContexteDume : appel à la library mpe_dume : consultation ' . $consultation->getId() . ' , idContexte  ' . $idContexteDume . '=> return ' . $results);
        } catch (Exception $e) {
            $logger->error('FIN fct getIdContexteDume : appel à la library mpe_dume : consultation ' . $consultation->getId() . ' , idContexte  ' . $idContexteDume . '=> exception ' . $e->getMessage() . ' ' . $e->getTraceAsString());
        }

        return $results;
    }

    /**
     * Permet de lancer l'updateDLRO.
     *
     * @param CommonConsultation $consultation
     * @param int                $idContexteDume
     *
     * @return int
     *
     * @author Anthony PECRIAUX <anthony.pecriaux@atexo.com>
     *
     * @version 1.0
     *
     * @since 2017-esr
     *
     * @copyright Atexo 2018
     */
    public static function updateDLRO($consultation, $idContexteDume)
    {
        $logger = Atexo_LoggerManager::getLogger(self::INTERFACE_DUME_LOGGER);
        $results = 0;

        try {
            $c = new Client(
                Atexo_Config::getParameter('URL_PF_DUME_API'),
                Atexo_Config::getParameter('LOGIN_DUME_API'),
                Atexo_Config::getParameter(
                    'PASSWORD_DUME_API'
                ),
                $logger,
                null
            );

            $logger->info('DEBUT fct getIdContexteDume : appel à la library mpe_dume : consultation ' . $consultation->getId() . ' , idContexte  ' . $idContexteDume);

            $consDume = self::getObjetConsultationDume($consultation);
            $logger->info("fct getIdContexteDume : création de l'objet consultation dume : " . print_r($consDume, true) . ' de la consultation ' . $consultation->getId());

            if ($idContexteDume) {
                $logger->info('fct getIdContexteDume : update dume acheteur en cours pour consultation ' . $consultation->getId() . ' =>  params : consultation ' . print_r($consDume, true) . ' , idContextLtDume ' . $idContexteDume);

                $results = $c->acheteurService()->updateDLRODume($consDume, $idContexteDume);
                $logger->info('fct getIdContexteDume : update dume acheteur effectue pour consultation ' . $consultation->getId() . ' et idContexte  ' . $idContexteDume . ' =>  retour :' . $results);
            } else {
                $logger->info('fct getIdContexteDume : echec update dume acheteur pour consultation ' . $consultation->getId() . ' et idContexte  ' . $idContexteDume . ' =>  retour :' . $results);
            }

            $logger->info('FIN fct getIdContexteDume : appel à la library mpe_dume : consultation ' . $consultation->getId() . ' , idContexte  ' . $idContexteDume . '=> mauvais idContexte');
        } catch (Exception $e) {
            $logger->error('FIN fct getIdContexteDume : appel à la library mpe_dume : consultation ' . $consultation->getId() . ' , idContexte  ' . $idContexteDume . '=> exception ' . $e->getMessage() . ' ' . $e->getTraceAsString());
        }

        return $results;
    }

    /**
     * Permet de retourner l'objet t_dume_contexte.
     *
     * @param CommonConsultation $consultation
     *
     * @return CommonTDumeContexte
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1.0
     *
     * @since 2017-esr
     *
     * @copyright Atexo 2018
     */
    public static function getTDumeContexte($consultation)
    {
        $tDumeContext = null;
        if ($consultation instanceof CommonConsultation) {
            $cnx = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
            $tDumeContext = CommonTDumeContexteQuery::create()
                ->filterByConsultationId($consultation->getId())
                ->filterByOrganisme($consultation->getOrganisme())
                ->filterByTypeDume(Atexo_Config::getParameter('TYPE_DUME_ACHETEUR'))
                ->addDescendingOrderByColumn('t_dume_contexte.id')
                ->findOne($cnx);
        }

        return $tDumeContext;
    }

    /**
     * Permet de retourner l'objet t_dume_contexte au statut publie.
     *
     * @param CommonConsultation $consultation
     *
     * @return CommonTDumeContexte
     *
     * @author Anthony PECRIAUX <anthony.pecriaux@atexo.com>
     *
     * @version 1.0
     *
     * @since 2017-esr
     *
     * @copyright Atexo 2018
     */
    public static function getTDumeContexteStatutPublie($consultation)
    {
        $tDumeContext = null;

        if ($consultation instanceof CommonConsultation) {
            $cnx = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));

            $tDumeContext = CommonTDumeContexteQuery::create()
                ->filterByConsultationId($consultation->getId())
                ->filterByOrganisme($consultation->getOrganisme())
                ->filterByStatus(Atexo_Config::getParameter('STATUT_DUME_CONTEXTE_PUBLIE'))
                ->filterByTypeDume(Atexo_Config::getParameter('TYPE_DUME_ACHETEUR'))
                ->findOne($cnx);
        }

        return $tDumeContext;
    }

    /**
     * Permet de creer l'objet t_dume_contexte.
     *
     * @param CommonConsultation $consultation
     * @param bool               $forceCreate
     *
     * @return int
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1.0
     *
     * @since 2017-esr
     *
     * @copyright Atexo 2018
     */
    public static function createDumeContexte($consultation, $forceCreate = false)
    {
        $idContexteDume = 0;
        if ($consultation instanceof CommonConsultation && 1 == $consultation->getDumeDemande()) {
            $logger = Atexo_LoggerManager::getLogger(self::INTERFACE_DUME_LOGGER);
            $logger->info('DEBUT fct createDumeContexte : consultation ' . $consultation->getId());
            $tDumeContexte = self::getTDumeContexte($consultation);
            if ($forceCreate || !$tDumeContexte instanceof CommonTDumeContexte) {
                if (!$tDumeContexte instanceof CommonTDumeContexte) {
                    $tDumeContexte = new CommonTDumeContexte();
                }
                $idContexteDume = $tDumeContexte->getContexteLtDumeId();
                $tDumeContexte->setOrganisme($consultation->getOrganisme());
                $tDumeContexte->setConsultationId($consultation->getId());
                $tDumeContexte->setTypeDume(Atexo_Config::getParameter('TYPE_DUME_ACHETEUR'));
                $statut = Atexo_Config::getParameter('STATUT_DUME_CONTEXTE_BROUILLON');
                if (1 == $consultation->getTypeFormulaireDume()) {
                    $statut = Atexo_Config::getParameter('STATUT_DUME_CONTEXTE_VALIDE');
                }
                $tDumeContexte->setStatus($statut);
                $idContexteDume = self::getIdContexteDume($consultation, $idContexteDume);
                if ($idContexteDume) {
                    $tDumeContexte->setContexteLtDumeId($idContexteDume);
                    $cnx = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));
                    $tDumeContexte->save($cnx);
                }
            } else {
                $idContexteDume = $tDumeContexte->getContexteLtDumeId();
            }
            $logger->info('FIN fct createDumeContexte : consultation ' . $consultation->getId() . ' retour => ' . $idContexteDume);
        }

        return $idContexteDume;
    }

    /**
     * Permet de mettre à jour ou creer l'objet t_dume_contexte.
     *
     * @param CommonConsultation $consultation
     * @param bool               $returnObjet
     *
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1.0
     *
     * @since 2017-esr
     * @copyright Atexo 2018
     */
    public static function createOrUpdateDumeContexte($consultation, $returnObjet = false): int|\Application\Propel\Mpe\CommonTDumeContexte
    {
        $idContexteDume = 0;
        if (Atexo_Module::isEnabled('InterfaceDume') && $consultation instanceof CommonConsultation && 1 == $consultation->getDumeDemande()) {
            $logger = Atexo_LoggerManager::getLogger(self::INTERFACE_DUME_LOGGER);
            $logger->info('DEBUT fct createOrUpdateDumeContexte : consultation ' . $consultation->getId() . ' with retour ' . ($returnObjet) ? 'objet' : 'idContexteDume');
            $create = false;
            $tDumeContexte = self::getTDumeContexte($consultation);
            if ($tDumeContexte instanceof CommonTDumeContexte) {
                $idContexteDume = $tDumeContexte->getContexteLtDumeId();
            } else {
                $tDumeContexte = new CommonTDumeContexte();
                $tDumeContexte->setOrganisme($consultation->getOrganisme());
                $tDumeContexte->setConsultationId($consultation->getId());
                $tDumeContexte->setTypeDume(Atexo_Config::getParameter('TYPE_DUME_ACHETEUR'));
                $statut = Atexo_Config::getParameter('STATUT_DUME_CONTEXTE_BROUILLON');
                if (1 == $consultation->getTypeFormulaireDume()) {
                    $statut = Atexo_Config::getParameter('STATUT_DUME_CONTEXTE_VALIDE');
                }
                $tDumeContexte->setStatus($statut);
                $create = true;
            }
            $idContexteDume = self::getIdContexteDume($consultation, $idContexteDume);
            if ($create && $idContexteDume) {
                $tDumeContexte->setContexteLtDumeId($idContexteDume);
                $tDumeContexte->save();
            }
            $logger->info('FIN fct createOrUpdateDumeContexte : consultation ' . $consultation->getId() . ' retour => ' . $idContexteDume);
            if ($returnObjet) {
                return $tDumeContexte;
            }
        }

        return $idContexteDume;
    }

    /**
     * Permet de valider le contexte dume via library mpe-dume.
     *
     * @param CommonConsultation $consultation
     * @param int                $idContexteDume
     *
     * @return \AtexoDume\Dto\ReponseValidPubDume
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1.0
     *
     * @since 2017-esr
     *
     * @copyright Atexo 2018
     */
    public static function validerDume($consultation, $idContexteDume)
    {
        $logger = Atexo_LoggerManager::getLogger(self::INTERFACE_DUME_LOGGER);
        $results = false;
        try {
            $c = new Client(
                Atexo_Config::getParameter('URL_PF_DUME_API'),
                Atexo_Config::getParameter('LOGIN_DUME_API'),
                Atexo_Config::getParameter('PASSWORD_DUME_API'),
                $logger,
                null
            );
            $logger->info('DEBUT fct validerDume : appel à la library mpe_dume : consultation ' . $consultation->getId() . ' , idContexte  ' . $idContexteDume);
            if ($idContexteDume) {
                $logger->info('fct validerDume : validate dume acheteur encours  pour consultation ' . $consultation->getId() . ' =>  params : idContextLtDume ' . $idContexteDume);
                $results = $c->acheteurService()->validateDume($idContexteDume);
                $logger->info('fct validerDume : validate dume acheteur  effectue pour consultation ' . $consultation->getId() . ' et idContexte  ' . $idContexteDume . ' =>  retour :' . print_r($results, true));
            }
            $logger->info('FIN fct validerDume : appel à la library mpe_dume : consultation ' . $consultation->getId() . ' , idContexte  ' . $idContexteDume . '=> return ' . print_r($results, true));
        } catch (Exception $e) {
            $logger->error('FIN fct validerDume : appel à la library mpe_dume : consultation ' . $consultation->getId() . ' , idContexte  ' . $idContexteDume . '=> exception ' . $e->getMessage() . ' ' . $e->getTraceAsString());
        }

        return $results;
    }

    /**
     * Permet de valider le contexte dume via library mpe-dume.
     *
     * @param CommonConsultation $consultation
     * @param string             $from
     *
     * @return array
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1.0
     *
     * @since 2017-esr
     *
     * @copyright Atexo 2018
     */
    public static function validerDumeAcheteur($consultation, $from = 'DEMANDE_VALIDATION')
    {
        $results = ['erreur' => false, 'messageErreur' => ''];

        if (Atexo_Module::isEnabled('InterfaceDume')) {
            $logger = Atexo_LoggerManager::getLogger(self::INTERFACE_DUME_LOGGER);

            $idContexteDume = 0;
            if ('DEMANDE_VALIDATION' == $from) {
                $serviceIndisponibleMessage = Prado::localize('DUME_ACHETEUR_DEMANDE_VALIDATION_SERVICE_INDISPONIBLE');
            } elseif ('VALIDATION' == $from) {
                $serviceIndisponibleMessage = Prado::localize('DUME_ACHETEUR_VALIDATION_SERVICE_INDISPONIBLE');
            } else {
                $serviceIndisponibleMessage = Prado::localize('DUME_ACHETEUR_SERVICE_INDISPONIBLE');
            }

            try {
                if ($consultation instanceof CommonConsultation && 1 == $consultation->getDumeDemande()) {
                    $logger->info('DEBUT fct validerDumeAcheteur : consultation ' . $consultation->getId());
                    $logger->info('fct validerDumeAcheteur : consultation ' . $consultation->getId() . ' : update ou creation du dume acheteur encours');
                    $tDumeContexte = self::createOrUpdateDumeContexte($consultation, true);
                    $idContexteDume = ($tDumeContexte instanceof CommonTDumeContexte) ? $tDumeContexte->getContexteLtDumeId() : 0;
                    $logger->info('fct validerDumeAcheteur : consultation ' . $consultation->getId() . ' : update ou creation du dume acheteur effectue =>  retour :' . $idContexteDume);

                    if ($tDumeContexte instanceof CommonTDumeContexte && $idContexteDume) {
                        $logger->info('fct validerDumeAcheteur consultation ' . $consultation->getId() . ' : validate dume acheteur encours =>  params : idContextLtDume ' . $idContexteDume);
                        $resultsValidateDume = self::validerDume($consultation, $idContexteDume);
                        $logger->info('fct validerDumeAcheteur consultation ' . $consultation->getId() . ' : validate dume acheteur effectue idContexte  ' . $idContexteDume . ' =>  retour :' . print_r($resultsValidateDume, true));

                        $infoDume = self::getInfoDume($idContexteDume);
                        if (!empty($infoDume)) {
                            $standard = 0;
                            if (true == $infoDume['default']) {
                                $standard = 1;
                            }
                            $tDumeContexte->setIsStandard($standard);
                        }

                        if ($resultsValidateDume instanceof \AtexoDume\Dto\ReponseValidPubDume) {
                            if ($resultsValidateDume->getStatut() == Atexo_Config::getParameter('STATUT_VALIDATION_DUME_OK')) {
                                $results = ['erreur' => false, 'messageErreur' => ''];
                                $tDumeContexte->setStatus(Atexo_Config::getParameter('STATUT_DUME_CONTEXTE_VALIDE'));
                                $cnx = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));
                                $tDumeContexte->save($cnx);
                                $logger->info('fct validerDumeAcheteur consultation ' . $consultation->getId() . ' : Mettre à jour le statut du t_dume_contexte à valide  id ' . $tDumeContexte->getId());
                            } else {
                                $serviceIndisponibleMessage = (new Atexo_Config())->toPfEncoding($resultsValidateDume->getMessage());
                                $results = ['erreur' => true, 'messageErreur' => $serviceIndisponibleMessage];
                            }
                        } else {
                            $results = ['erreur' => true, 'messageErreur' => $serviceIndisponibleMessage];

                            if ('VALIDATION' == $from) {
                                $contentMail = str_replace('{_REF_CONS_}', $consultation->getReferenceUtilisateur(), Prado::localize('DUME_ACHETEUR_MAIL_AGENT_SERVICE_INDISPONIBLE'));
                                Atexo_Message::sendMailProblemePubDumeToAgent($contentMail, $consultation->getMailAgentCreateur());
                            }
                        }
                    } else {
                        $results = ['erreur' => true, 'messageErreur' => $serviceIndisponibleMessage];

                        if ('VALIDATION' == $from) {
                            $contentMail = str_replace('{_REF_CONS_}', $consultation->getReferenceUtilisateur(), Prado::localize('DUME_ACHETEUR_MAIL_AGENT_SERVICE_INDISPONIBLE'));
                            Atexo_Message::sendMailProblemePubDumeToAgent($contentMail, $consultation->getMailAgentCreateur());
                        }
                    }

                    $logger->info('FIN fct validerDumeAcheteur : appel à la library mpe_dume : consultation ' . $consultation->getId() . ' , idContexte  ' . $idContexteDume . '=> return ' . print_r($results, true));
                }
            } catch (\Exception $e) {
                $logger->error('FIN fct validerDumeAcheteur : appel à la library mpe_dume : consultation ' . $consultation->getId() . ' , idContexte  ' . $idContexteDume . '=> exception ' . $e->getMessage() . ' ' . $e->getTraceAsString());
                $results = ['erreur' => true, 'messageErreur' => $serviceIndisponibleMessage];

                if ('VALIDATION' == $from) {
                    $contentMail = str_replace('{_REF_CONS_}', $consultation->getReferenceUtilisateur(), Prado::localize('DUME_ACHETEUR_MAIL_AGENT_SERVICE_INDISPONIBLE'));
                    Atexo_Message::sendMailProblemePubDumeToAgent($contentMail, $consultation->getMailAgentCreateur());
                }
            }
        }

        return $results;
    }

    /**
     * Permet de publier le dume Acheteur.
     *
     * @param CommonConsultation $consultation
     * @param int                $idContexteDume
     *
     * @return \AtexoDume\Dto\ReponseValidPubDume
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1.0
     *
     * @since 2017-esr
     *
     * @copyright Atexo 2018
     */
    public static function publierDume($consultation, $idContexteDume)
    {
        $logger = Atexo_LoggerManager::getLogger(self::INTERFACE_DUME_LOGGER);
        $results = false;

        try {
            $c = new Client(
                Atexo_Config::getParameter('URL_PF_DUME_API'),
                Atexo_Config::getParameter('LOGIN_DUME_API'),
                Atexo_Config::getParameter('PASSWORD_DUME_API'),
                $logger,
                null
            );

            $logger->info('DEBUT fct publierDume : appel à la library mpe_dume : consultation ' . $consultation->getId() . ' , idContexte  ' . $idContexteDume);

            if ($idContexteDume) {
                $logger->info('fct publierDume : publication dume acheteur en cours  pour consultation ' . $consultation->getId() . ' =>  params : idContextLtDume ' . $idContexteDume);

                if (
                    Atexo_Module::isEnabled('interfaceDume') &&
                    1 == $consultation->getDumeDemande() &&
                    Atexo_Util::isSiretValide($consultation->getSiretAcheteur())
                ) {
                    $results = $c->acheteurService()->publishDume($idContexteDume);

                    $logger->info('fct publierDume : publication dume acheteur  effectue pour consultation ' . $consultation->getId() . ' et idContexte  ' . $idContexteDume . ' =>  retour :' . print_r($results, true));
                } else {
                    $logger->info('fct publierDume : publication dume acheteur non effectue car siret invalide pour consultation ' . $consultation->getId());
                }
            }

            $logger->info('FIN fct publierDume : appel à la library mpe_dume : consultation ' . $consultation->getId() . ' , idContexte  ' . $idContexteDume . '=> return ' . print_r($results, true));
        } catch (Exception $e) {
            $logger->error('FIN fct publierDume : appel à la library mpe_dume : consultation ' . $consultation->getId() . ' , idContexte  ' . $idContexteDume . '=> exception ' . $e->getMessage() . ' ' . $e->getTraceAsString());
        }

        return $results;
    }

    /**
     * Permet de valider le contexte dume via library mpe-dume.
     *
     * @param CommonConsultation $consultation
     *
     * @return array
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1.0
     *
     * @since 2017-esr
     *
     * @copyright Atexo 2018
     */
    public static function publierDumeAcheteur($consultation)
    {
        $logger = Atexo_LoggerManager::getLogger(self::INTERFACE_DUME_LOGGER);
        $results = ['erreur' => false, 'messageErreur' => ''];

        if (Atexo_Module::isEnabled('InterfaceDume')) {
            $idContexteDume = 0;
            $serviceIndisponibleMessage = Prado::localize('DUME_ACHETEUR_VALIDATION_SERVICE_INDISPONIBLE');

            try {
                if ($consultation instanceof CommonConsultation && 1 == $consultation->getDumeDemande()) {
                    $logger->info('DEBUT fct publierDumeAcheteur : consultation ' . $consultation->getId());
                    $tDumeContexte = self::getTDumeContexte($consultation);
                    $idContexteDume = ($tDumeContexte instanceof CommonTDumeContexte) ? $tDumeContexte->getContexteLtDumeId() : 0;

                    if (
                        $tDumeContexte instanceof CommonTDumeContexte &&
                        $idContexteDume &&
                        $tDumeContexte->getStatus() == Atexo_Config::getParameter('STATUT_DUME_CONTEXTE_VALIDE')
                    ) {
                        $logger->info('fct publierDumeAcheteur consultation ' . $consultation->getId() . ' : publication dume acheteur encours =>  params : idContextLtDume ' . $idContexteDume);
                        $resultsValidateDume = self::publierDume($consultation, $idContexteDume);
                        $logger->info('fct publierDumeAcheteur consultation ' . $consultation->getId() . ' : publication dume acheteur effectue idContexte  ' . $idContexteDume . ' =>  retour :' . print_r($resultsValidateDume, true));

                        if ($resultsValidateDume instanceof \AtexoDume\Dto\ReponseValidPubDume) {
                            $results = [
                                'erreur' => false,
                                'messageErreur' => '',
                                'isStandard' => $tDumeContexte->getIsStandard(),
                            ];

                            if ($resultsValidateDume->getStatut() == Atexo_Config::getParameter('STATUT_PUBLICATION_DUME_OK')) {
                                $tDumeContexte->setStatus(Atexo_Config::getParameter('STATUT_DUME_CONTEXTE_EN_ATTENTE_PUBLICATION'));
                                $cnx = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));
                                $tDumeContexte->save($cnx);
                                $logger->info('fct publierDumeAcheteur consultation ' . $consultation->getId() . ' : Mettre à jour le statut du t_dume_contexte à Publie id ' . $tDumeContexte->getId());
                            } elseif ($resultsValidateDume->getStatut() == Atexo_Config::getParameter('STATUT_PUBLICATION_DUME_KO')) {
                                $contentMail = str_replace('{_REF_CONS_}', $consultation->getReferenceUtilisateur(), Prado::localize('DUME_ACHETEUR_MAIL_AGENT_DUME_NATIONAL_NON_PUBLIE'));
                                Atexo_Message::sendMailProblemePubDumeToAgent($contentMail, $consultation->getMailAgentCreateur());
                            } else {
                                $results = ['erreur' => true, 'messageErreur' => (new Atexo_Config())->toPfEncoding($resultsValidateDume->getMessage())];
                                $contentMail = str_replace('{_REF_CONS_}', $consultation->getReferenceUtilisateur(), Prado::localize('DUME_ACHETEUR_MAIL_AGENT_SERVICE_INDISPONIBLE'));
                                Atexo_Message::sendMailProblemePubDumeToAgent($contentMail, $consultation->getMailAgentCreateur());
                            }
                        } else {
                            $results = ['erreur' => true, 'messageErreur' => $serviceIndisponibleMessage];
                            $contentMail = str_replace('{_REF_CONS_}', $consultation->getReferenceUtilisateur(), Prado::localize('DUME_ACHETEUR_MAIL_AGENT_SERVICE_INDISPONIBLE'));
                            Atexo_Message::sendMailProblemePubDumeToAgent($contentMail, $consultation->getMailAgentCreateur());
                        }
                    } elseif (
                        $tDumeContexte instanceof CommonTDumeContexte &&
                        $tDumeContexte->getStatus() == Atexo_Config::getParameter('STATUT_DUME_CONTEXTE_BROUILLON')
                    ) {
                        $results = ['erreur' => true, 'messageErreur' => $serviceIndisponibleMessage];
                    }

                    $tDumeContexte->setStatus(Atexo_Config::getParameter('STATUT_DUME_CONTEXTE_PUBLIE'));
                    $cnx = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));
                    $tDumeContexte->save($cnx);

                    $logger->info('FIN fct publierDumeAcheteur : appel à la library mpe_dume : consultation ' . $consultation->getId() . ' , idContexte  ' . $idContexteDume . '=> return ' . print_r($results, true));
                }
            } catch (\Exception $e) {
                $logger->error('FIN fct publierDumeAcheteur : appel à la library mpe_dume : consultation ' . $consultation->getId() . ' , idContexte  ' . $idContexteDume . '=> exception ' . $e->getMessage() . ' ' . $e->getTraceAsString());
                $results = ['erreur' => true, 'messageErreur' => Prado::localize('DUME_ACHETEUR_SERVICE_INDISPONIBLE')];
            }
        }

        return $results;
    }

    /**
     * Permet de recuperer les numero des dumes depuis idContexteDUME.
     *
     * @param int $idContexteDume
     *
     * @return \AtexoDume\Dto\ReponseListPublishedDume
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1.0
     *
     * @since 2017-esr
     *
     * @copyright Atexo 2018
     */
    public static function getNumerosDumes($idContexteDume)
    {
        $logger = Atexo_LoggerManager::getLogger('dume');
        $results = false;
        try {
            $c = new Client(
                Atexo_Config::getParameter('URL_PF_DUME_API'),
                Atexo_Config::getParameter('LOGIN_DUME_API'),
                Atexo_Config::getParameter('PASSWORD_DUME_API'),
                $logger,
                null
            );
            if ($idContexteDume) {
                $logger->info('DEBUT fct getNumerosDumes : recuperation des numeros dume acheteur encours =>  params : idContextLtDume ' . $idContexteDume);
                $results = $c->acheteurService()->getPublishedDume($idContexteDume);
                $logger->info('FIN fct getNumerosDumes : recuperation des numeros dume acheteur effectue pour idContexte  ' . $idContexteDume . ' =>  retour :' . print_r($results, true));
            }
        } catch (Exception $e) {
            $logger->error('FIN fct getNumerosDumes : idContexte  ' . $idContexteDume . '=> exception ' . $e->getMessage() . ' ' . $e->getTraceAsString());
        }

        return $results;
    }

    /**
     * Permet de recuperer les numero des dumes depuis idContexteDUME.
     *
     * @param int $idContexteDume
     *
     * @return \AtexoDume\Dto\ReponseListPublishedDume
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1.0
     *
     * @since 2017-esr
     *
     * @copyright Atexo 2018
     */
    public static function getPdfDume($numDume)
    {
        $logger = Atexo_LoggerManager::getLogger('dume');
        $results = false;
        try {
            $c = new Client(
                Atexo_Config::getParameter('URL_PF_DUME_API'),
                Atexo_Config::getParameter('LOGIN_DUME_API'),
                Atexo_Config::getParameter('PASSWORD_DUME_API'),
                $logger,
                null
            );
            if ($numDume) {
                $logger->info('DEBUT fct getPdfDume : recuperation des numeros dume acheteur encours =>  params : numero DUME  ' . $numDume);
                $results = $c->acheteurService()->getPdf($numDume);
                $logger->info(' FIN fct getPdfDume : publication dume acheteur  effectue pour numero DUME  ' . $numDume . ' =>  retour :' . print_r($results, true));
            }
        } catch (Exception $e) {
            $logger->error('FIN fct getPdfDume : numero DUME  ' . $numDume . '=> exception ' . $e->getMessage() . ' ' . $e->getTraceAsString());
        }

        return $results;
    }

    /**
     * Permet de recuperer les infos du dume nationnal.
     *
     * @return bool
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1.0
     *
     * @since 2017-esr
     *
     * @copyright Atexo 2018
     */
    public static function getInfoDumeNationnal(CommonTDumeContexte $tDumeContexte)
    {
        $succes = true;
        if (Atexo_Module::isEnabled('InterfaceDume')) {
            $logger = Atexo_LoggerManager::getLogger('dume');
            $logger->info('DEBUT fct getInfoDumeNationnal : recuperation des numeros dume acheteur =>  params : idContexteDume  ' . $tDumeContexte->getContexteLtDumeId());
            $reponseListPublishedDume = self::getNumerosDumes($tDumeContexte->getContexteLtDumeId());
            if ($reponseListPublishedDume instanceof ReponseListPublishedDume) {
                if ($reponseListPublishedDume->getStatut() == Atexo_Config::getParameter('STATUT_NUMEROS_DUME_OK')) {
                    $listsNumerosDume = $reponseListPublishedDume->getNumerosDume();
                    if (!empty($listsNumerosDume) && is_array($listsNumerosDume)) {
                        foreach ($listsNumerosDume as $numDume) {
                            if ($numDume instanceof NumeroDume) {
                                if ($numDume->getNumeroDumeSN()) {
                                    $tNumDume = new CommonTDumeNumero();
                                    $tNumDume->setNumeroDumeNational($numDume->getNumeroDumeSN());
                                    $tNumDume->setListLot($numDume->getNumeroLot());
                                    //recuperation du pdf
                                    $logger->info('fct getInfoDumeNationnal : recuperation pdf dume acheteur idContexteDume  ' . $tDumeContexte->getContexteLtDumeId() . ' : params => numeroDume ' . $numDume->getNumeroDumeSN());
                                    $idBlob = self::insertPdfDumeDemande($tDumeContexte->getContexteLtDumeId(), $numDume->getNumeroDumeSN(), $tDumeContexte->getOrganisme());
                                    $tNumDume->setBlobId($idBlob);
                                    $logger->info('fct getInfoDumeNationnal : insertion pdf dume acheteur idContexteDume  ' . $tDumeContexte->getContexteLtDumeId() . ' , numeroDume ' . $numDume->getNumeroDumeSN() . ' : idBlob : ' . $idBlob);
                                    $tDumeContexte->addCommonTDumeNumero($tNumDume);
                                } else {
                                    $logger->error('fct getInfoDumeNationnal : ERREUR recuperation des numeros dume acheteur =>  params : idContexteDume  ' . $tDumeContexte->getContexteLtDumeId() . ' : numero vide ' . print_r($reponseListPublishedDume, true));
                                }
                            }
                        }
                    } else {
                        $logger->error('fct getInfoDumeNationnal : ERREUR recuperation des numeros dume acheteur =>  params : idContexteDume  ' . $tDumeContexte->getContexteLtDumeId() . ' : liste numeros vide ' . print_r($reponseListPublishedDume, true));
                    }
                } else {
                    $logger->error('fct getInfoDumeNationnal : Erreur recuperation des numeros dume acheteur =>  params : idContexteDume  ' . $tDumeContexte->getContexteLtDumeId() . ' : ' . print_r($reponseListPublishedDume, true));
                }
            } else {
                $logger->error('fct getInfoDumeNationnal : ERREUR recuperation des numeros dume acheteur =>  params : idContexteDume   ' . $tDumeContexte->getContexteLtDumeId());
            }
        }

        return $succes;
    }

    /**
     * Permet de retourner l'objet t_dume_contexte.
     *
     * @param CommonConsultation $consultation
     *
     * @return CommonTDumeContexte
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1.0
     *
     * @since 2017-esr
     *
     * @copyright Atexo 2018
     */
    public static function getTDumeNumeros($consultation)
    {
        $tDumeNumeros = [];
        if ($consultation instanceof CommonConsultation) {
            $cnx = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
            $tDumeContext = CommonTDumeContexteQuery::create()
                ->filterByConsultationId($consultation->getId())
                ->filterByOrganisme($consultation->getOrganisme())
                ->filterByTypeDume(Atexo_Config::getParameter('TYPE_DUME_ACHETEUR'))
                ->filterByStatus(Atexo_Config::getParameter('STATUT_DUME_CONTEXTE_PUBLIE'))
                ->findOne($cnx);
            if ($tDumeContext instanceof CommonTDumeContexte) {
                $tDumeNumeros = (array) CommonTDumeNumeroQuery::create()
                                ->filterByIdDumeContexte($tDumeContext->getId())
                                ->orderById(Criteria::ASC)
                                ->find($cnx);
            }
        }

        return $tDumeNumeros;
    }

    /**
     * Permet de retourner un tableau contenant la liste des anciens DUME Acheteur.
     *
     * @param CommonConsultation $consultation
     *
     * @return array
     */
    public static function getAncienTDumeNumeros($consultation)
    {
        $tDumeNumeros = [];

        if ($consultation instanceof CommonConsultation) {
            $cnx = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));

            $tDumeContext = CommonTDumeContexteQuery::create()
                ->filterByConsultationId($consultation->getId())
                ->filterByOrganisme($consultation->getOrganisme())
                ->filterByTypeDume(Atexo_Config::getParameter('TYPE_DUME_ACHETEUR'))
                ->filterByStatus(Atexo_Config::getParameter('STATUT_DUME_CONTEXTE_REMPLACER'))
                ->find($cnx);

            foreach ($tDumeContext as $value) {
                if ($value instanceof CommonTDumeContexte) {
                    $tDumeNumeros[] = CommonTDumeNumeroQuery::create()
                        ->filterByIdDumeContexte($value->getId())
                        ->orderById(Criteria::ASC)
                        ->findOne($cnx);
                }
            }
        }

        return $tDumeNumeros;
    }

    public static function insertPdfDumeDemande($idContexteDume, $numDumeSN, $organisme)
    {
        $idBlob = 0;
        $logger = Atexo_LoggerManager::getLogger('dume');
        $logger->info('fct insertPdfDumeDemande : recuperation pdf dume acheteur idContexteDume  ' . $idContexteDume . ' : params => numeroDume ' . $numDumeSN);
        $pdfContent = self::getPdfDume($numDumeSN);
        if (!empty($pdfContent)) {
            $filePdf = Atexo_Config::getParameter('COMMON_TMP') . 'dume_acheteur_' . $numDumeSN . date('_Y_m_d_h_i_s') . '.pdf';
            Atexo_Util::write_file($filePdf, $pdfContent);
            $atexoBlob = new Atexo_Blob();
            $idBlob = $atexoBlob->insert_blob('dume_acheteur_' . $numDumeSN . '.pdf', $filePdf, $organisme);
            $logger->info('fct insertPdfDumeDemande : insertion pdf dume acheteur idContexteDume  ' . $idContexteDume . ' , numeroDume ' . $numDumeSN . ' : idBlob : ' . $idBlob);
        }

        return $idBlob;
    }

    /**
     * Retourne un token pour l'accès côté navigateur.
     *
     * @param $idContexteDume
     *
     * @return string
     *
     * @throws \AtexoDume\Exception\ServiceException
     */
    public static function getTokenDume($idContexteDume)
    {
        $logger = Atexo_LoggerManager::getLogger(self::INTERFACE_DUME_LOGGER);
        $token = '';
        try {
            $client = new Client(
                Atexo_Config::getParameter('URL_PF_DUME_API'),
                Atexo_Config::getParameter('LOGIN_DUME_API'),
                Atexo_Config::getParameter('PASSWORD_DUME_API'),
                $logger,
                null
            );

            $logger->info('DEBUT fct getTokenDume : appel à la library mpe_dume : getClientAccessToken, idContexte  ' . $idContexteDume);
            $token = $client->acheteurService()->getClientAccessToken($client, $idContexteDume);
            $logger->info('Fin fct getTokenDume : appel à la library mpe_dume : getClientAccessToken, idContexte  ' . $idContexteDume . ' => token : ' . $token);
        } catch (Exception $e) {
            $logger->error('Fin fct getTokenDume : appel à la library mpe_dume : getClientAccessToken, idContexte ' . $idContexteDume . ' => exception ' . $e->getMessage() . ' ' . $e->getTraceAsString());
        }

        return $token;
    }

    /**
     * Permet de retourner l'objet t_candidature.
     *
     * @param CommonOffres $offre
     *
     * @return CommonTCandidature
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1.0
     *
     * @since 2017-esr
     *
     * @copyright Atexo 2018
     */
    public static function retrieveCandidatureDume($offre)
    {
        $tCandidature = null;
        if ($offre instanceof CommonOffres) {
            $cnx = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
            $tCandidatureQuery = new CommonTCandidatureQuery();
            $tCandidatureQuery->filterByIdOffre($offre->getId());
            $tCandidatureQuery->filterByConsultationId($offre->getConsultationId());
            $tCandidatureQuery->filterByOrganisme($offre->getOrganisme());
            $tCandidatureQuery->filterByTypeCandidature(Atexo_Config::getParameter('TYPE_CANDIDATUE_DUME'));
            $tCandidatureQuery->filterByTypeCandidatureDume(Atexo_Config::getParameter('TYPE_CANDIDATUE_DUME_ONLINE'));
            $tCandidatureQuery->filterByStatus(Atexo_Config::getParameter('STATUT_CANDIDATUE_DUME_BROUILLON'), Criteria::NOT_LIKE);
            $tCandidature = $tCandidatureQuery->findOne($cnx);
        }

        return $tCandidature;
    }

    public static function getTDumeNumerosEntreprise($idContexte, $lot = 0)
    {
        $tDumeNumeros = [];
        if ($idContexte) {
            $cnx = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
            $tDumeNumerosQuery = new CommonTDumeNumeroQuery();
            $tDumeNumerosQuery->filterByIdDumeContexte($idContexte);
            if ($lot) {
                $tDumeNumerosQuery->condition('condLot1', 'CommonTDumeNumero.listLot like ? ', $lot . ',');
                $tDumeNumerosQuery->condition('condLot2', 'CommonTDumeNumero.listLot like ? ', ',' . $lot . ',');
                $tDumeNumerosQuery->condition('condLot3', 'CommonTDumeNumero.listLot like ? ', ',' . $lot);
                $tDumeNumerosQuery->condition('condLot4', 'CommonTDumeNumero.listLot like ? ', $lot);
                $tDumeNumerosQuery->where(['condLot1', 'condLot2', 'condLot3', 'condLot4'], 'OR');
            }
            $tDumeNumeros = (array) $tDumeNumerosQuery->find($cnx);
        }

        return $tDumeNumeros;
    }

    /**
     * Permet de creer un job pr la recuperation du SN et pdf.
     *
     * @param CommonConsultation $commonCons
     *
     * @return void
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1.0
     *
     * @since 2017-esr
     *
     * @copyright Atexo 2018
     */
    public static function createJmsJobToPublishDume($commonCons)
    {
        if ($commonCons instanceof CommonConsultation) {
            $tDumeContexte = Atexo_Dume_AtexoDume::getTDumeContexte($commonCons);
            if ($tDumeContexte instanceof CommonTDumeContexte) {
                $serviceMessageBus = Atexo_Util::getSfService('app.publish.messageBus');
                $serviceMessageBus->publishDumePublication(
                    'acheteur',
                    true,
                    $tDumeContexte->getContexteLtDumeId()
                );
            }
        }
    }

    /**
     * Permet de retourner une candidature dume online.
     *
     * @param int    $reference la reference de la consultation
     * @param string $organisme l'acronyme de l'organisme
     * @param int    $statut    le statut de dume,defaul null
     * @param $connexion
     *
     * @return array of CommonTcandidature
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2017-esr
     *
     * @copyright Atexo 2018
     */
    public static function getCandidatureDumeOnlineByConsultation($reference, $organisme, $statut = null, $connexion = null)
    {
        if (!$connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
        $tCandidatureQuery = new CommonTCandidatureQuery();

        return $tCandidatureQuery->getCandidatureDumeByConsultationEtType($reference, $organisme, Atexo_Config::getParameter('TYPE_CANDIDATUE_DUME_ONLINE'), $statut, $connexion);
    }

    /**
     * Permet de lancer le replacePublished lors de la modification d'une consultation déjà en ligne.
     *
     * @param CommonConsultation $consultation
     * @param int                $idContexteDume
     *
     * @return int
     *
     * @author Anthony PECRIAUX <anthony.pecriaux@atexo.com>
     *
     * @version 1.0
     *
     * @since 2017-esr
     *
     * @copyright Atexo 2018
     */
    public static function replacePublished($consultation, $idContexteDume)
    {
        $logger = Atexo_LoggerManager::getLogger(self::INTERFACE_DUME_LOGGER);
        $results = 0;

        try {
            $c = new Client(
                Atexo_Config::getParameter('URL_PF_DUME_API'),
                Atexo_Config::getParameter('LOGIN_DUME_API'),
                Atexo_Config::getParameter('PASSWORD_DUME_API'),
                $logger,
                null
            );

            $logger->info('DEBUT fct replacePublished : appel à la library mpe_dume : consultation ' . $consultation->getId() . ' , idContexte  ' . $idContexteDume);

            if ($idContexteDume) {
                $logger->info('fct replacePublished : update dume acheteur encours pour consultation ' . $consultation->getId() . ' =>  params : idContextLtDume ' . $idContexteDume);
                $results = $c->acheteurService()->replacePublishedDume($idContexteDume);
                $logger->info('fct replacePublished : update dume acheteur  effectue pour consultation ' . $consultation->getId() . ' et idContexte  ' . $idContexteDume . ' =>  retour :' . $results);
            }

            $logger->info('FIN fct replacePublished : appel à la library mpe_dume : consultation ' . $consultation->getId() . ' , idContexte  ' . $idContexteDume . '=> return ' . $results);
        } catch (Exception $e) {
            $logger->error('FIN fct replacePublished : appel à la library mpe_dume : consultation ' . $consultation->getId() . ' , idContexte  ' . $idContexteDume . '=> exception ' . $e->getMessage() . ' ' . $e->getTraceAsString());
        }

        return $results;
    }

    /**
     * Permet de lancer le checkPurge lors de la modification des lots d'une consultation.
     *
     * @param CommonConsultation $consultation
     * @param int                $idContexteDume
     * @param $lotToDelete
     * @param $lot
     * @param $modification
     *
     * @return mixed
     *
     * @author Anthony PECRIAUX <anthony.pecriaux@atexo.com>
     *
     * @version 1.0
     *
     * @since 2017-esr
     *
     * @copyright Atexo 2018
     */
    public static function checkPurge($consultation, $idContexteDume, $lotToDelete = null, $lot = null, $modification = null)
    {
        $logger = Atexo_LoggerManager::getLogger(self::INTERFACE_DUME_LOGGER);
        $results = 0;

        try {
            $c = new Client(
                Atexo_Config::getParameter('URL_PF_DUME_API'),
                Atexo_Config::getParameter('LOGIN_DUME_API'),
                Atexo_Config::getParameter('PASSWORD_DUME_API'),
                $logger,
                null
            );

            $logger->info('DEBUT fct getIdContexteDume : appel à la library mpe_dume : consultation ' . $consultation->getId() . ' , idContexte  ' . $idContexteDume);

            $consDume = self::getObjetConsultationDume($consultation, $lotToDelete, $lot, $modification);

            $logger->info("fct getIdContexteDume : creation de l'objet consultation dume : " . print_r($consDume, true) . ' de la consultation ' . $consultation->getId());

            if ($idContexteDume) {
                $logger->info('fct getIdContexteDume : checkPurge dume acheteur encours pour consultation ' . $consultation->getId() . ' =>  params : consultation ' . print_r($consDume, true) . ' , idContextLtDume ' . $idContexteDume);

                $results = $c->acheteurService()->checkPurgeDume($consDume, $idContexteDume);

                $logger->info('fct getIdContexteDume : checkPurge dume acheteur  effectue pour consultation ' . $consultation->getId() . ' et idContexte  ' . $idContexteDume . ' =>  retour :' . $results);
            }

            $logger->info('FIN fct getIdContexteDume : appel à la library mpe_dume : consultation ' . $consultation->getId() . ' , idContexte  ' . $idContexteDume . '=> return ' . $results);
        } catch (Exception $e) {
            $logger->error('FIN fct getIdContexteDume : appel à la library mpe_dume : consultation ' . $consultation->getId() . ' , idContexte  ' . $idContexteDume . '=> exception ' . $e->getMessage() . ' ' . $e->getTraceAsString());
        }

        return $results;
    }

    /**
     * Permet de lancer le getStatutPublication afin de récupérer le statut de publication du contrat.
     *
     * @param $idContrat
     * @param $nbModification
     *
     * @return mixed
     *
     * @author Anthony PECRIAUX <anthony.pecriaux@atexo.com>
     *
     * @version 1.0
     *
     * @since 2018-esr
     *
     * @copyright Atexo 2018
     */
    public static function getStatutPublication($idContrat, $nbModification)
    {
        $logger = Atexo_LoggerManager::getLogger(self::INTERFACE_DUME_LOGGER);
        $results = 0;

        try {
            $c = new Client(
                Atexo_Config::getParameter('URL_PF_DUME_API'),
                Atexo_Config::getParameter('LOGIN_DUME_API'),
                Atexo_Config::getParameter('PASSWORD_DUME_API'),
                $logger,
                null
            );

            $logger->info('DEBUT fct getStatutPublication : appel à la library mpe_dume : idContrat ' . $idContrat . ' , nbModification ' . $nbModification);

            if (isset($idContrat) && isset($nbModification)) {
                $logger->info('fct getStatutPublication : données essentielles en cours pour idContrat ' . $idContrat . ' , nbModification ' . $nbModification);

                $results = $c->donneesEssentiellesService()->getStatutPublication($idContrat, $nbModification);

                $logger->info('fct getStatutPublication : données essentielles en cours pour idContrat ' . $idContrat . ' , nbModification ' . $nbModification . ' =>  retour : ' . $results);
            }

            $logger->info('FIN fct getStatutPublication : appel à la library mpe_dume : idContrat ' . $idContrat . ' , nbModification ' . $nbModification . ' => return ' . $results);
        } catch (Exception $e) {
            $logger->error('FIN fct getStatutPublication : appel à la library mpe_dume : idContrat ' . $idContrat . ' , nbModification ' . $nbModification . ' => exception ' . $e->getMessage() . ' ' . $e->getTraceAsString());
        }

        return $results;
    }

    /**
     * @param $numeroSn
     * @param null $connexion
     *
     * @return CommonTDumeNumero
     *
     * @throws Atexo_Config_Exception
     * @throws PropelException
     */
    public function getDumeNumeroByNumeroSn($numeroSn, $connexion = null)
    {
        if (!$connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
        $tdumeNumeroQuery = new CommonTDumeNumeroQuery();

        return $tdumeNumeroQuery->retrieveDumeNumeroByNumeroSn($numeroSn, $connexion);
    }

    /**
     * @param $consultation
     * @param null $typeDume (Atexo_Config::getParameter('TYPE_DUME_ACHETEUR') ou Atexo_Config::getParameter('TYPE_DUME_OE'))
     *
     * @return array
     *
     * @throws Atexo_Config_Exception
     * @throws PropelException
     */
    public static function getListTDumeNumeros($consultation, $typeDume = null)
    {
        $tDumeNumeros = [];
        if ($consultation instanceof CommonConsultation) {
            $cnx = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
            $tDumeContexts = CommonTDumeContexteQuery::create()
                ->filterByConsultationId($consultation->getId())
                ->filterByOrganisme($consultation->getOrganisme());
            if (!is_null($typeDume)) {
                $tDumeContexts = $tDumeContexts->filterByTypeDume($typeDume);
            }
            $tDumeContexts = $tDumeContexts->filterByStatus(Atexo_Config::getParameter('STATUT_DUME_CONTEXTE_PUBLIE'))
                ->find($cnx);
            $tabIdDumeContexte = [];
            foreach ($tDumeContexts as $tDumeContext) {
                if ($tDumeContext instanceof CommonTDumeContexte) {
                    $tabIdDumeContexte[] = $tDumeContext->getId();
                }
            }

            $tDumeNumeros = (array) CommonTDumeNumeroQuery::create()
                ->filterByIdDumeContexte($tabIdDumeContexte)
                ->orderById(Criteria::ASC)
                ->find($cnx);
        }

        return (count($tDumeNumeros) > 0) ? $tDumeNumeros : null;
    }

    /**
     * Permet de retourner l'objet t_dume_contexte.
     *
     * @param CommonConsultation $consultation
     *
     * @return CommonTDumeContexte
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @copyright Atexo 2018
     */
    public static function getTDumeContexteStandard($consultation)
    {
        $tDumeContext = null;
        if ($consultation instanceof CommonConsultation) {
            $cnx = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
            $tDumeContextQuery = new CommonTDumeContexteQuery();
            $tDumeContext = $tDumeContextQuery->getTDumeContexteByType($consultation->getId(), $consultation->getOrganisme(), Atexo_Config::getParameter('TYPE_DUME_ACHETEUR'), 1, $cnx);
        }

        return $tDumeContext;
    }

    /**
     * Permet de retourner une candidature dume online by role insrit.
     *
     * @param int    $reference la reference de la consultation
     * @param string $organisme l'acronyme de l'organisme
     * @param int    $statut    le statut de dume,defaul null
     * @param $connexion
     *
     * @return array of CommonTcandidature
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2018-esr
     *
     * @copyright Atexo 2019
     */
    public static function getCandidaturseCoTraitantDumeOnline($reference, $organisme, $idEntreprise, $idInscrit, $roleInscrit, $statut = null, $connexion = null)
    {
        if (!$connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
        $tCandidatureQuery = new CommonTCandidatureQuery();

        return $tCandidatureQuery->getCandidatureDumeByRoleIscrit($reference, $organisme, $idEntreprise, $idInscrit, $roleInscrit, Atexo_Config::getParameter('TYPE_CANDIDATUE_DUME_ONLINE'), $statut, $connexion);
    }

    public static function getInfoDume($idContexteDume)
    {
        $logger = Atexo_LoggerManager::getLogger(self::INTERFACE_DUME_LOGGER);
        $results = 0;
        try {
            $c = new Client(
                Atexo_Config::getParameter('URL_PF_DUME_API'),
                Atexo_Config::getParameter('LOGIN_DUME_API'),
                Atexo_Config::getParameter('PASSWORD_DUME_API'),
                $logger,
                null
            );
            $logger->info('DEBUT fct get : appel à la library mpe_dume : idContexteDume ' . $idContexteDume);
            $results = $c->acheteurService()->getInfoDume($idContexteDume);
            $logger->info('FIN fct get : appel à la library mpe_dume : idContexteDume ' . $idContexteDume . ' => return ' . $results);
        } catch (Exception $e) {
            $logger->error('FIN fct get : appel à la library mpe_dume : idContexteDume ' . $idContexteDume . ' => exception ' . $e->getMessage() . ' ' . $e->getTraceAsString());
        }

        return $results;
    }
}
