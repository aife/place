<?php

namespace Application\Service\Atexo;

class Atexo_Menu
{
    public function activateMenuEntreprise($menuVo)
    {
        $idSessFile = Atexo_CurrentUser::dumpSession();
        $cmd = $menuVo->getCmd();
        $org = $menuVo->getOrg();
        $ref = $menuVo->getReference();
        $refParamName = $menuVo->getRefParamName();

        $redirectUrl = Atexo_Config::getParameter('URL_MPE_ORGANISME').'/'.$org.'/index.php?'
                    .'cmd2='.base64_encode($cmd)
                    .'&sess='.$idSessFile
                    .'&'.$refParamName.'='.$ref
                    ;
        $this->response->redirect($redirectUrl);
    }
}
