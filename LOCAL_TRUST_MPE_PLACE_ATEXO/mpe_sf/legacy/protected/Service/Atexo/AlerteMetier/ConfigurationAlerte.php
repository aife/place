<?php

namespace Application\Service\Atexo\AlerteMetier;

use Application\Propel\Mpe\CommonConfigurationAlerte;
use Application\Propel\Mpe\CommonConfigurationAlerteQuery;
use Prado\Prado;

/**
 * Classe de manipulation des alertes.
 *
 * @author Ezziani Loubna <loubna.ezziani@atexo.com>
 * @copyright Atexo 2012
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class ConfigurationAlerte
{
    /**
     * Permet de recuperer le corps du mail d'alertes chorus aux agents pour les cas de marchés "commandés" ou "cloturés".
     *
     * @param int $typeObjet le type d'objet pour lequel on veut recuperer les alertes(1=>Consultation,2=>Service,3=>PlateForme...parametres dans Application.xml.php)
     *
     * @return array $arrayAlertesLibelle liste des alerte pour l'objet donne en parametre
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function retrieveActiveAlerteLibelle($typeObjet)
    {
        $configurationAlerteQuery = new CommonConfigurationAlerteQuery();
        $confAlertes = $configurationAlerteQuery->findAllActiveAlertes();

        $arrayAlertesLibelle = [];

        if ($confAlertes) {
            foreach ($confAlertes as $confAlerte) {
                if ($confAlerte instanceof CommonConfigurationAlerte && $confAlerte->getTypeObjet() == $typeObjet) {
                    $arrayAlertesLibelle[$confAlerte->getId()] = Prado::localize($confAlerte->getLibelle());
                }
            }
        }

        return $arrayAlertesLibelle;
    }

    /**
     * Permet de recuperer toutes les alertes activees.
     *
     * @return array $confAlertes liste des objet CommonConfigurationAlerte
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function retrieveAllActiveAlertes()
    {
        $configurationAlerteQuery = new CommonConfigurationAlerteQuery();

        return $configurationAlerteQuery->findAllActiveAlertes();
    }
}
