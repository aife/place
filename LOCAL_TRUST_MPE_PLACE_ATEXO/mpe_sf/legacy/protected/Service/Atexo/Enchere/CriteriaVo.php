<?php

namespace Application\Service\Atexo\Enchere;

/**
 * Classe contenant le détail d'une Enchere.
 *
 * @author Thibaut DECAUDAIN <thibaut.decaudain@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Enchere_CriteriaVo
{
    // Champs de la table EncherePmi
    private $_acronymOrganisme;
    private string $_refConsultation = '';
    private string $_referenceUtilisateur = '';
    private int $_statuts = 0;
    private string $_motclef = '';

    private string $_dateDebut1 = '';
    private string $_dateDebut2 = '';
    private string $_dateFin1 = '';
    private string $_dateFin2 = '';

    private int $offset = 0;
    private int $limit = 0;
    private string $sortByElement = 'dateFin';
    private string $sensOrderBy = 'DESC';

    private ?string $_idService = '';
    private string $_idsServices = '';

    public function setRefConsultation($RefConsultation)
    {
        $this->_refConsultation = $RefConsultation;
    }

    public function getRefConsultation()
    {
        return $this->_refConsultation;
    }

    public function setReferenceUtilisateur($referenceUtilisateur)
    {
        $this->_referenceUtilisateur = $referenceUtilisateur;
    }

    public function getReferenceUtilisateur()
    {
        return $this->_referenceUtilisateur;
    }

    public function setStatuts($idStatuts)
    {
        $this->_statuts = $idStatuts;
    }

    public function getStatuts()
    {
        return $this->_statuts;
    }

    public function setMotclef($motclef)
    {
        $this->_motclef = $motclef;
    }

    public function getMotclef()
    {
        return $this->_motclef;
    }

    public function getDateDebut1()
    {
        return $this->_dateDebut1;
    }

    public function setDateDebut1($date)
    {
        $this->_dateDebut1 = $date;
    }

    public function getDateDebut2()
    {
        return $this->_dateDebut2;
    }

    public function setDateDebut2($date)
    {
        $this->_dateDebut2 = $date;
    }

    public function getDateFin1()
    {
        return $this->_dateFin1;
    }

    public function setDateFin1($date)
    {
        $this->_dateFin1 = $date;
    }

    public function getDateFin2()
    {
        return $this->_dateFin2;
    }

    public function setDateFin2($date)
    {
        $this->_dateFin2 = $date;
    }

    public function getOffset()
    {
        return $this->offset;
    }

    public function setOffset($offset)
    {
        $this->offset = $offset;
    }

    public function getLimit()
    {
        return $this->limit;
    }

    public function setLimit($limit)
    {
        $this->limit = $limit;
    }

    public function setAcronymeOrganisme($acronym)
    {
        $this->_acronymOrganisme = $acronym;
    }

    public function getAcronymeOrganisme()
    {
        return $this->_acronymOrganisme;
    }

    public function getSortByElement()
    {
        return $this->sortByElement;
    }

    public function setSortByElement($sortByElement)
    {
        $this->sortByElement = $sortByElement;
    }

    public function getSensOrderBy()
    {
        return $this->sensOrderBy;
    }

    public function setSensOrderBy($sensOrderBy)
    {
        $this->sensOrderBy = $sensOrderBy;
    }

    public function getIdService()
    {
        return $this->_idService;
    }

    public function setIdService($idService)
    {
        $this->_idService = $idService;
    }

    public function getIdsServices()
    {
        return $this->_idsServices;
    }

    public function setIdsServices($idsService)
    {
        $this->_idsServices = $idsService;
    }
}
