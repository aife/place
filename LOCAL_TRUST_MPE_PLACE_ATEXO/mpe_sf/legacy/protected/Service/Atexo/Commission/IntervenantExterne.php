<?php

namespace Application\Service\Atexo\Commission;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonTCAOCommissionIntervenantExterne;
use Application\Propel\Mpe\CommonTCAOCommissionIntervenantExternePeer;
use Application\Propel\Mpe\CommonTCAOCommissionIntervenantExterneQuery;
use Application\Propel\Mpe\CommonTCAOIntervenantExterne;
use Application\Propel\Mpe\CommonTCAOIntervenantExternePeer;
use Application\Propel\Mpe\CommonTCAOIntervenantExterneQuery;
use Application\Propel\Mpe\CommonTCAOOrdreDePassage;
use Application\Propel\Mpe\CommonTCAOSeance;
use Application\Propel\Mpe\CommonTCAOSeanceIntervenantExterne;
use Application\Propel\Mpe\CommonTCAOSeanceIntervenantExternePeer;
use Application\Propel\Mpe\CommonTCAOSeanceIntervenantExterneQuery;
use Application\Propel\Mpe\CommonTCAOSeanceInvite;
use Application\Propel\Mpe\CommonTCAOSeanceInviteQuery;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_ValeursReferentielles;
use PDO;

/**
 * Librairie de manipulation des objets Intervenants Externes pour le module Commission (CAO).
 *
 * @author Amal EL BEKKAOUI <amal.elbekkaoui@atexo.com>
 *
 * @version $Id: IntervenantExterne.php,v 1.22 2014-10-29 11:23:33 japlacide Exp $
 *
 * @since 4.2
 *
 * @copyright Atexo 2012
 */
class Atexo_Commission_IntervenantExterne
{
    /**
     * Genere le critere specifiant l'agencement des resultats d'une requete SQL recherchant un ensemble d'intervenants externes.
     *
     * @param string $org     : acronyme de l'organisme courant
     * @param string $tri     : nom du type d'information sur lequel porte le tri
     * @param string $sensTri : code du sens du tri : "ASC" ou "DESC"
     * @param array  $listIds : liste des identifiants des enregistrements CommonTCAOIntervenantExterne (intervenants externes) sur lesquels le tri doit etre effectue
     * @param bool   $all     : indique si le critere tient compte de la liste des identifiants donnes en parametre ou s'il doit s'appliquer sur l'ensemble des enregistrements
     *
     * @return object(Criteria) : objet contenant les informations pour trier les resultats de la requete recherchant un ensemble d'intervenants externes
     *
     * @author JAP <ameline.placide@atexo.com>
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function generateCriteria($org, $tri, $sensTri, $listIds, $all = false)
    {
        $c = new Criteria();
        $c->add(CommonTCAOIntervenantExternePeer::ORGANISME, $org);
        if (!$all) {
            $c->add(CommonTCAOIntervenantExternePeer::ID_INTERVENANT_EXTERNE, $listIds, Criteria::IN);
        }
        if (!empty($sensTri)) {
            if ('ASC' == $sensTri) {
                switch ($tri) {
                    case 'nom':
                        $c->addAscendingOrderByColumn(CommonTCAOIntervenantExternePeer::NOM);
                        break;
                    case 'prenom':
                        $c->addAscendingOrderByColumn(CommonTCAOIntervenantExternePeer::PRENOM);
                        break;
                    case 'email':
                        $c->addAscendingOrderByColumn(CommonTCAOIntervenantExternePeer::EMAIL);
                        break;
                    case 'organisation':
                        $c->addAscendingOrderByColumn(CommonTCAOIntervenantExternePeer::ORGANISATION);
                        break;
                    case 'fonction':
                        $c->addAscendingOrderByColumn(CommonTCAOIntervenantExternePeer::FONCTION);
                        break;
                    case 'typeVoix':
                        $c->addAscendingOrderByColumn(CommonTCAOIntervenantExternePeer::ID_REF_VAL_TYPE_VOIX_DEFAUT);
                        break;
                    default:
                        $c->addAscendingOrderByColumn(CommonTCAOIntervenantExternePeer::NOM);
                        break;
                }
            } else {
                switch ($tri) {
                    case 'nom':
                        $c->addDescendingOrderByColumn(CommonTCAOIntervenantExternePeer::NOM);
                        break;
                    case 'prenom':
                        $c->addDescendingOrderByColumn(CommonTCAOIntervenantExternePeer::PRENOM);
                        break;
                    case 'email':
                        $c->addAscendingOrderByColumn(CommonTCAOIntervenantExternePeer::EMAIL);
                        break;
                    case 'organisation':
                        $c->addDescendingOrderByColumn(CommonTCAOIntervenantExternePeer::ORGANISATION);
                        break;
                    case 'fonction':
                        $c->addDescendingOrderByColumn(CommonTCAOIntervenantExternePeer::FONCTION);
                        break;
                    case 'typeVoix':
                        $c->addDescendingOrderByColumn(CommonTCAOIntervenantExternePeer::ID_REF_VAL_TYPE_VOIX_DEFAUT);
                        break;
                    default:
                        $c->addDescendingOrderByColumn(CommonTCAOIntervenantExternePeer::NOM);
                        break;
                }
            }
        } else {
            $c->addAscendingOrderByColumn(CommonTCAOIntervenantExternePeer::NOM);
        }

        return $c;
    }

    /**
     * Ajoute un intervenant externe a une commission (creation d'un enregistrement CommonTCAOCommissionIntervenantExterne).
     *
     * @param int    $idIntExterne : identifiant de l'objet CommonTCAOIntervenantExterne (intervenant externe)
     * @param int    $idCommission : identifiant de l'objet CommonTCAOCommission (commission)
     * @param string $org          : acronyme de l'organisme courant
     *
     * @return void
     *
     * @author JAP <ameline.placide@atexo.com>
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function addIntervenantToCommission($idIntExterne, $idCommission, $org)
    {
        //Recuperation de l'intervenant
        $intervenant = CommonTCAOIntervenantExterneQuery::create()
           ->findPk([$idIntExterne, $org]);

        if ($intervenant instanceof CommonTCAOIntervenantExterne) {
            //Type de voix defini dans le profil de l'intervenant
            $idTypeVoix = $intervenant->getIdRefValTypeVoixDefaut();
            //Type de voix par defaut
            if (empty($idTypeVoix)) {
                $_t_typeVoix = (new Atexo_ValeursReferentielles())->getValeurRefByIdRef(Atexo_Config::getParameter('ID_REFERENTIEL_TYPE_VOIX_INTERVENANTS'));
                $last_item = array_slice($_t_typeVoix, -1, 1, true);
                $idTypeVoix = key($last_item);
            }
            //Verification de l'existence de cet intervenant externe pour cette commission
            $commission_int_ext = self::getCommissionIntervenantExterne($idIntExterne, $idCommission, $org);

            if (!($commission_int_ext instanceof CommonTCAOCommissionIntervenantExterne)) {
                $commission_int_ext = new CommonTCAOCommissionIntervenantExterne();
            }

            //Mise a jour des champs de l'enregistrement
            $commission_int_ext->setOrganisme($org);
            $commission_int_ext->setIdCommission($idCommission);
            $commission_int_ext->setIdIntervenantExterne($intervenant->getIdIntervenantExterne());
            $commission_int_ext->setIdRefValTypeVoixDefaut($idTypeVoix);
            //Sauvegarde de l'enregistrement
            $commission_int_ext->save();
        }
    }

    /**
     * Ajoute un intervenant externe a une seance de commission (creation d'un enregistrement CommonTCAOSeanceIntervenantExterne).
     *
     * @param int    $idIntExterne : identifiant de l'objet CommonTCAOIntervenantExterne (intervenant externe)
     * @param int    $idSeance     : identifiant de l'objet CommonTCAOSeance (seance de commission)
     * @param string $org          : acronyme de l'organisme courant
     *
     * @return void
     *
     * @author JAP <ameline.placide@atexo.com>
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function addIntervenantToSeance($idIntExterne, $idSeance, $org)
    {
        //Recuperation de l'intervenant
        $intervenant = CommonTCAOIntervenantExterneQuery::create()
        ->findPk([$idIntExterne, $org]);

        if ($intervenant instanceof CommonTCAOIntervenantExterne) {
            //Type de voix defini dans le profil de l'intervenant
            $idTypeVoix = $intervenant->getIdRefValTypeVoixDefaut();
            //Type de voix par defaut
            if (empty($idTypeVoix)) {
                $_t_typeVoix = (new Atexo_ValeursReferentielles())->getValeurRefByIdRef(Atexo_Config::getParameter('ID_REFERENTIEL_TYPE_VOIX_INTERVENANTS'));
                $last_item = array_slice($_t_typeVoix, -1, 1, true);
                $idTypeVoix = key($last_item);
            }

            //Verification de l'existence de cet intervenant externe pour cette seance
            $seance_int_ext = (new Atexo_Commission_IntervenantExterne())->getSeanceIntervenantExterne($idIntExterne, $idSeance, $org);

            if (!($seance_int_ext instanceof CommonTCAOSeanceIntervenantExterne)) {
                $seance_int_ext = new CommonTCAOSeanceIntervenantExterne();
            }

            //Mise a jour des champs de l'enregistrement
            $seance_int_ext->setOrganisme($org);
            $seance_int_ext->setIdSeance($idSeance);
            $seance_int_ext->setIdIntervenantExterne($intervenant->getIdIntervenantExterne());
            $seance_int_ext->setIdRefValTypeVoix($idTypeVoix);
            //Sauvegarde de l'enregistrement
            $seance_int_ext->save();
        }
    }

    /**
     * Ajoute un intervenant externe a une consultation a l'ordre du jour d'une seance de commission (creation d'un enregistrement CommonTCAOSeanceInvite : invite ponctuel).
     *
     * @param int    $idIntExterne     : identifiant de l'objet CommonTCAOIntervenantExterne (intervenant externe)
     * @param int    $idOrdreDePassage : identifiant de l'objet CommonTCAOOrdreDePassage ("ligne de l'ordre du jour" de la seance de commission)
     * @param string $org              : acronyme de l'organisme courant
     *
     * @return void
     *
     * @author JAP <ameline.placide@atexo.com>
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function addIntervenantToOrdreDePassage($idIntExterne, $idOrdreDePassage, $org)
    {
        //Recuperation de l'intervenant
        $intervenant = CommonTCAOIntervenantExterneQuery::create()
        ->findPk([$idIntExterne, $org]);

        if ($intervenant instanceof CommonTCAOIntervenantExterne) {
            //Type de voix defini dans le profil de l'intervenant
            $idTypeVoix = $intervenant->getIdRefValTypeVoixDefaut();
            //Type de voix par defaut
            if (empty($idTypeVoix)) {
                $_t_typeVoix = (new Atexo_ValeursReferentielles())->getValeurRefByIdRef(Atexo_Config::getParameter('ID_REFERENTIEL_TYPE_VOIX_INTERVENANTS'));
                $last_item = array_slice($_t_typeVoix, -1, 1, true);
                $idTypeVoix = key($last_item);
            }

            //Verification de l'existence de cet intervenant externe pour cette seance
            $invite = self::getInviteOrdreDePassage($idIntExterne, $idOrdreDePassage, $org);

            if (!($invite instanceof CommonTCAOSeanceInvite)) {
                $invite = new CommonTCAOSeanceInvite();
            }

            //Mise a jour des champs de l'enregistrement
            $invite->setOrganisme($org);
            $invite->setIdOrdreDePassage($idOrdreDePassage);
            $invite->setIdIntervenantExterne($intervenant->getIdIntervenantExterne());
            $invite->setIdRefValTypeVoix($idTypeVoix);
            //Sauvegarde de l'enregistrement
            $invite->save();
        }
    }

    /**
     * Recupere le nombre ou la liste des objets CommonTCAOIntervenantExterne (intervenants externes) selon un ensemble de criteres.
     *
     * @param object(Criteria) $c           : objet contenant les informations pour agencer les resultats de la requete recherchant l'ensemble des intervenants externes
     * @param bool             $nbreElement : indique si le resultat a renvoyer doit etre la liste des intervenants externes respectant les informations contenues dans l'objet Criteria donne en parametre ou bien le nombre d'intervenants externes respectant les informations contenues dans l'objet Criteria donne en parametre
     *
     * @return int | array : nombre ou liste d'intervenants externes respectant les informations contenues dans l'objet Criteria donne en parametre
     *
     * @author JAP <ameline.placide@atexo.com>
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getIntervenants($c, $nbreElement = false): int|array
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        if (!$c) {
            $c = new Criteria();
        }
        if ($nbreElement) {
            $nbre = CommonTCAOIntervenantExternePeer::doCount($c, false, $connexion);

            return $nbre;
        } else {
            $intervenants = CommonTCAOIntervenantExternePeer::doSelect($c, $connexion);

            return $intervenants;
        }
    }

    /**
     * Recupere le nombre d'intervenants externes membres permanents d'une seance de commission.
     *
     * @param int    $idSeance : identifiant de l'objet CommonTCAOSeance (seance de commission)
     * @param string $org      : acronyme de l'organisme courant
     *
     * @return int : nombre d'intervenants externes membres permanents de la seance de commission dont l'identifiant est donne en parametre
     *
     * @author JAP <ameline.placide@atexo.com>
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getNombreSeanceIntervenants($idSeance, $org)
    {
        $count = CommonTCAOSeanceIntervenantExterneQuery::create()
           ->filterByIdSeance($idSeance)
           ->filterByOrganisme($org)
           ->count();

        return $count;
    }

    /**
     * Supprime un intervenant externe.
     *
     * @param $id: identifiant de l'objet CommonTCAOIntervenantExterne (intervenant externe) devant etre supprime
     * @param $organisme: acronyme de l'organisme courant
     *
     * @return int : statut de la requete SQL de suppression
     *
     * @author OKO <oumar.konate@atexo.com>
     *
     * @since 4.2
     *
     * @copyright Atexo 2012
     */
    public function deleteIntervenant($id, $org)
    {
        $intervenant = CommonTCAOIntervenantExterneQuery::create()
           ->findPk([$id, $org]);

        return $intervenant->delete();
    }

    /**
     * Supprime un intervenant externe de la liste des intervenants externes membres d'une commission.
     *
     * @param int    $id           : identifiant de l'objet CommonTCAOIntervenantExterne (intervenant externe)
     * @param int    $idCommission : identifiant de l'objet CommonTCAOCommission (commission)
     * @param string $org          : acronyme de l'organisme courant
     *
     * @return int : statut de la requete SQL de suppression
     *
     * @author OKO <oumar.konate@atexo.com>
     *
     * @since 4.2
     *
     * @copyright Atexo 2012
     */
    public function deleteIntervenantCommission($id, $idCommission, $org)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $intervenant = self::getCommissionIntervenantExterne($id, $idCommission, $org);
        if ($intervenant instanceof CommonTCAOCommissionIntervenantExterne) {
            $intervenant->delete($connexion);
        }
    }

    /**
     * Supprime un intervenant externe de la liste des intervenants externes membres permanents d'une seance de commission.
     *
     * @param int    $id       : identifiant de l'objet CommonTCAOIntervenantExterne (intervenant externe)
     * @param int    $idSeance : identifiant de l'objet CommonTCAOSeance (seance de commission)
     * @param string $org      : acronyme de l'organisme courant
     *
     * @return int : statut de la requete SQL de suppression
     *
     * @author JAP <ameline.placide@atexo.com>
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function deleteSeanceIntervenant($id, $idSeance, $org)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $intervenant = self::getSeanceIntervenantExterne($id, $idSeance, $org, $connexion);
        if ($intervenant instanceof CommonTCAOSeanceIntervenantExterne) {
            $intervenant->delete($connexion);
        }
    }

    /**
     * Supprime un intervenant externe de la liste des intervenants externes invites ponctuels pour une consultation a l'ordre du jour d'une seance de commission.
     *
     * @param int    $id          : identifiant de l'objet CommonTCAOIntervenantExterne (intervenant externe)
     * @param int    $idOrdreJour : identifiant de l'objet CommonTCAOOrdreDePassage ("ligne de l'ordre du jour" de la seance de commission)
     * @param string $org         : acronyme de l'organisme courant
     *
     * @return int : statut de la requete SQL de suppression
     *
     * @author JAP <ameline.placide@atexo.com>
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function deleteOrdreDePassageIntervenant($id, $idOrdreJour, $org, $type = false)
    {
        $intervenant = self::getInviteOrdreDePassage($id, $idOrdreJour, $org, $type);
        if ($intervenant instanceof CommonTCAOSeanceInvite) {
            $intervenant->delete();
        }
    }

    /**
     * Recupere l'ensemble des intervenants externes membres permanents d'une commission.
     *
     * @param int         $idCommission : identifiant de l'objet CommonTCAOCommission (commission)
     * @param string      $org          : acronyme de l'organisme courant
     * @param object(PDO) $connexion    : objet de connexion a la base de donnees
     * @param bool        $returnObjet  : indique si les objets renvoyes doivent contenir l'ensemble des attributs ou si seulement les identifiants sont renvoyes
     *
     * @return array : liste des intervenants externes membres permanents de la commission donnee en parametres contenant l'ensemble des attributs ou juste les identifiants
     *
     * @author ABE <amal.elbekkaoui@atexo.com>
     *
     * @since 4.2
     *
     * @copyright Atexo 2012
     */
    public function getAllIdsIntervenantsCommission($idCommission, $org, $connexion = null, $returnObjet = false)
    {
        if (!$connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        }
        $c = new Criteria();
        $c->add(CommonTCAOCommissionIntervenantExternePeer::ID_COMMISSION, $idCommission);
        $c->add(CommonTCAOCommissionIntervenantExternePeer::ORGANISME, $org);
        $intervenantsComm = CommonTCAOCommissionIntervenantExternePeer::doSelect($c, $connexion);

        if ($returnObjet) {
            return $intervenantsComm;
        }

        $arrayIds = [];

        foreach ($intervenantsComm as $intervenant) {
            $arrayIds[$intervenant->getIdIntervenantExterne()] = $intervenant->getIdIntervenantExterne();
        }

        return $arrayIds;
    }

    /**
     * Recupere l'ensemble des intervenants externes membres permanents d'une seance de commission.
     *
     * @param int         $idSeance    : identifiant de l'objet CommonTCAOSeance (seance de commission)
     * @param string      $org         : acronyme de l'organisme courant
     * @param object(PDO) $connexion   : objet de connexion a la base de donnees
     * @param bool        $returnObjet : indique si les objets renvoyes doivent contenir l'ensemble des attributs ou si seulement les identifiants sont renvoyes
     *
     * @return array : liste des intervenants externes membres permanents de la seance de commission donnee en parametres contenant l'ensemble des attributs ou juste les identifiants
     *
     * @author JAP <ameline.placide@atexo.com>
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getAllIdsIntervenantsSeance($idSeance, $org, $connexion = null, $returnObjet = false)
    {
        if (!$connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        }
        $c = new Criteria();
        $c->add(CommonTCAOSeanceIntervenantExternePeer::ID_SEANCE, $idSeance);
        $c->add(CommonTCAOSeanceIntervenantExternePeer::ORGANISME, $org);
        $_t_intervenants = CommonTCAOSeanceIntervenantExternePeer::doSelect($c, $connexion);

        if ($returnObjet) {
            return $_t_intervenants;
        }

        $arrayIds = [];

        foreach ($_t_intervenants as $intervenant) {
            $arrayIds[$intervenant->getIdIntervenantExterne()] = $intervenant->getIdIntervenantExterne();
        }

        return $arrayIds;
    }

    /**
     * Attribue automatiquement a une seance tous les intervenants externes membres permanents de la commission de la seance.
     *
     * @param int    $idSeance     : identifiant de l'objet CommonTCAOSeance (seance de commission)
     * @param int    $idCommission : identifiant de l'objet CommonTCAOCommission (commission)
     * @param string $org          : acronyme de l'organisme courant
     *
     * @return void
     *
     * @author JAP <ameline.placide@atexo.com>
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function addCommissionIntervenantsToSeance($idSeance, $idCommission, $org)
    {
        $connexion = null;
        ///Recuperation des intervenants de la commission
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $_t_intervenants = self::getAllIdsIntervenantsCommission($idCommission, $org, $connexion) ;
        //Ajout par defaut des intervenants de la commission
        foreach ($_t_intervenants as $key) {
            $intervenant = CommonTCAOIntervenantExterneQuery::create()
            ->findPk([$_t_intervenants[$key], $org]);

            if ($intervenant instanceof CommonTCAOIntervenantExterne) {
                $intervenantSeance = new CommonTCAOSeanceIntervenantExterne();
                $intervenantSeance->setIdSeance($idSeance);
                $intervenantSeance->setOrganisme($org);
                $intervenantSeance->setIdIntervenantExterne($intervenant->getIdIntervenantExterne());
                $intervenantSeance->setIdRefValTypeVoix($intervenant->getIdRefValTypeVoixDefaut());
                $intervenantSeance->save();
            }
        }
    }

    /**
     * Retire tous les intervenants externes membres permanents d'une seance de commission (reinitialisation).
     *
     * @param int         $idSeance  : identifiant de l'objet CommonTCAOSeance (seance de commission)
     * @param string      $org       : acronyme de l'organisme courant
     * @param object(PDO) $connexion : objet de connexion a la base de donnees
     *
     * @return void
     *
     * @author JAP <ameline.placide@atexo.com>
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function removeIntervenantsFromSeance($idSeance, $org, $connexion = null)
    {
        if (!$connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        }
        $_t_intervenants = self::getAllIdsIntervenantsSeance($idSeance, $org, $connexion);

        foreach ($_t_intervenants as $key) {
            $intervenantSeance = self::getSeanceIntervenantExterne($_t_intervenants[$key], $idSeance, $org, $connexion);
            if ($intervenantSeance instanceof CommonTCAOSeanceIntervenantExterne) {
                $intervenantSeance->delete($connexion);
            }
        }
    }

    /**
     * Recupere la liste des invites ponctuels pour une consultation a l'ordre du jour d'une seance de commission.
     *
     * @param int    $idOrdreDePassage : identifiant de l'objet CommonTCAOOrdreDePassage ("ligne de l'ordre du jour" de la seance de commission)
     * @param string $org              : acronyme de l'organisme courant
     * @param bool   $returnListe      : indique si la liste a renvoyer doit contenir l'ensemble des attributs des objets ou uniquement les identifiants
     * @param bool   $typeAgents       : indique si l'identifiant a recupere est celui des intervenants externes ou celui des agents
     *
     * @return array : liste d'objets ou d'identifiants de CommonTCAOSeanceInvite representant l'ensemble des agents appartenant a la seance de commission dont l'identifiant est donne en parametre
     *
     * @author JAP <ameline.placide@atexo.com>
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getAllIdInvitesOrdreDePassage($idOrdreDePassage, $org, $returnListe = null, $typeAgents = false)
    {
        $invites = CommonTCAOSeanceInviteQuery::create()
           ->filterByIdOrdreDePassage($idOrdreDePassage)
           ->filterByOrganisme($org)
           ->find();

        if ($returnListe) {
            return $invites;
        }
        $arrayIds = [];
        if ($typeAgents) {
            foreach ($invites as $intervenant) {
                $arrayIds[$intervenant->getIdIntervenantExterne()] = $intervenant->getIdIntervenantExterne();
            }
        } else {
            foreach ($invites as $intervenant) {
                $arrayIds[$intervenant->getIdAgent()] = $intervenant->getIdAgent();
            }
        }

        return $arrayIds;
    }

    /**
     * Recupere l'intervenant externe appartenant a une commission.
     *
     * @param int    $id           : identifiant de l'objet CommonTCAOIntervenantExterne
     * @param int    $idCommission : identifiant de l'objet CommonTCAOCommission (commission)
     * @param string $org          : acronyme de l'organisme courant
     *
     * @return object(CommonTCAOCommissionIntervenantExterne) : objet representant un intervenant externe membre permanent de la commission dont l'identifiant est donne en parametre
     *
     * @author JAP <ameline.placide@atexo.com>
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getCommissionIntervenantExterne($id, $idCommission, $org)
    {
        $intervenant = CommonTCAOCommissionIntervenantExterneQuery::create()
           ->filterByOrganisme($org)
           ->filterByIdCommission($idCommission)
           ->filterByIdIntervenantExterne($id)
           ->findOne();

        return $intervenant;
    }

    /**
     * Recupere l'intervenant externe appartenant a une seance de commission.
     *
     * @param int         $id        : identifiant de l'objet CommonTCAOIntervenantExterne
     * @param int         $idSeance  : identifiant de l'objet CommonTCAOSeance (seance de commission)
     * @param string      $org       : acronyme de l'organisme courant
     * @param object(PDO) $connexion : objet de connexion a la base de donnees
     *
     * @return object(CommonTCAOSeanceIntervenantExterne) : objet representant un intervenant externe membre de la seance de commission dont l'identifiant est donne en parametre
     *
     * @author JAP <ameline.placide@atexo.com>
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getSeanceIntervenantExterne($id, $idSeance, $org, $connexion = null)
    {
        if (!$connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        }
        $c = new Criteria();
        $c->add(CommonTCAOSeanceIntervenantExternePeer::ID_INTERVENANT_EXTERNE, $id);
        $c->add(CommonTCAOSeanceIntervenantExternePeer::ID_SEANCE, $idSeance);
        $c->add(CommonTCAOSeanceIntervenantExternePeer::ORGANISME, $org);
        $intervenant = CommonTCAOSeanceIntervenantExternePeer::doSelectOne($c, $connexion);

        return $intervenant;
    }

    /**
     * Recupere l'invite ponctuel pour une consultation a l'ordre du jour d'une seance de commission.
     *
     * @param int    $id               : identifiant de l'objet CommonTCAOIntervenantExterne
     * @param int    $idOrdreDePassage : identifiant de l'objet CommonTCAOOrdreDePassage ("ligne de l'ordre du jour" de la seance de commission)
     * @param string $org              : acronyme de l'organisme courant
     * @param bool   $typeAgent        : indique si l'invite dont l'identifiant est donne en parametre est un agent ou un intervenant externe
     *
     * @return object(CommonTCAOSeanceInvite) : objet representant un agent ou un intervenant externe invite ponctuellement pour la consultation a l'ordre du jour de la seance de commission dont l'identifiant est donne en parametre
     *
     * @author JAP <ameline.placide@atexo.com>
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getInviteOrdreDePassage($id, $idOrdreDePassage, $org, $typeAgent = false)
    {
        if ($typeAgent) {
            $invite = CommonTCAOSeanceInviteQuery::create()
               ->filterByIdAgent($id)
               ->filterByIdOrdreDePassage($idOrdreDePassage)
               ->filterByOrganisme($org)
               ->useCommonAgentQuery()
                   ->filterById($id)
                   ->filterByOrganisme($org)
               ->endUse()
               ->findOne();
        } else {
            $invite = CommonTCAOSeanceInviteQuery::create()
            ->filterByIdIntervenantExterne($id)
            ->filterByIdOrdreDePassage($idOrdreDePassage)
            ->filterByOrganisme($org)
            ->useCommonTCAOIntervenantExterneQuery()
               ->filterByIdIntervenantExterne($id)
               ->filterByOrganisme($org)
            ->endUse()
            ->findOne();
        }

        return $invite;
    }

    /**
     * Recupere la liste des intervenants externes membres d'une seance de commission.
     *
     * @param int    $idSeance : identifiant de l'objet CommonTCAOSeance
     * @param string $org      : acronyme de l'organisme courant
     *
     * @return array : liste d'objets partiels (champs nom, prenom, civilite, email) de CommonTCAOSeanceIntervenantExterne representant l'ensemble des intervenants externes membres de la seance de commission dont l'identifiant est donne en parametre
     *
     * @author JAP <ameline.placide@atexo.com>
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getAllIntervenantsExternesSeance($idSeance, $org)
    {
        $_t_intervenants = CommonTCAOSeanceIntervenantExterneQuery::create()
        ->filterByIdSeance($idSeance)
        ->filterByOrganisme($org)
        ->useCommonTCAOIntervenantExterneQuery()
        ->filterByOrganisme($org)
        ->endUse()
        ->withColumn('t_CAO_Intervenant_Externe.nom', 'nom')
        ->withColumn('t_CAO_Intervenant_Externe.prenom', 'prenom')
        ->withColumn('t_CAO_Intervenant_Externe.id_ref_val_civilite', 'civilite')
        ->withColumn('t_CAO_Intervenant_Externe.email', 'email')
        ->find();

        return $_t_intervenants;
    }

    /**
     * Recupere la liste des intervenants externes invites ponctuellement pour une consultation a l'ordre du jour d'une seance de commission.
     *
     * @param int    $idOrdreDePassage : identifiant de l'objet CommonTCAOOrdreDePassage (consultation en "passage" de l'ordre du jour)
     * @param string $org              : acronyme de l'organisme courant
     *
     * @return array : liste d'objets partiels (champs nom, prenom, civilite, email) de CommonTCAOSeanceInvite representant l'ensemble des intervenants externes invites ponctuellement pour la consultation a l'ordre du jour de la seance de commission dont l'identifiant est donne en parametre
     *
     * @author JAP <ameline.placide@atexo.com>
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getAllIntervenantsExternesOrdreDePassage($idOrdreDePassage, $org)
    {
        $_t_intervenants = CommonTCAOSeanceInviteQuery::create()
           ->filterByIdOrdreDePassage($idOrdreDePassage)
           ->filterByOrganisme($org)
           ->useCommonTCAOIntervenantExterneQuery()
               ->filterByOrganisme($org)
           ->endUse()
           ->withColumn('t_CAO_Intervenant_Externe.nom', 'nom')
           ->withColumn('t_CAO_Intervenant_Externe.prenom', 'prenom')
           ->withColumn('t_CAO_Intervenant_Externe.id_ref_val_civilite', 'civilite')
           ->withColumn('t_CAO_Intervenant_Externe.email', 'email')
           ->find();

        return $_t_intervenants;
    }

    /**
     * Recupere la liste des intervenants externes, invites ponctuels d'une seance de commission.
     *
     * @param int    $idSeance : identifiant de l'objet CommonTCAOSeance (seance de commission)
     * @param string $org      : acronyme de l'organisme courant
     *
     * @return array : liste d'objets partiels (champs nom, prenom, civilite, email) de CommonTCAOSeanceInvite representant l'ensemble des agents rattaches ponctuellement a la seance de commission dont l'identifiant est donne en parametre
     *
     * @author JAP <ameline.placide@atexo.com>
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getAllInvitesSeance($idSeance, $org)
    {
        $_t_invites = CommonTCAOSeanceInviteQuery::create()
           ->filterByOrganisme($org)
           ->useCommonTCAOOrdreDePassageQuery()
               ->filterByIdSeance($idSeance)
               ->filterByOrganisme($org)
           ->endUse()
           ->useCommonTCAOIntervenantExterneQuery()
               ->filterByOrganisme($org)
           ->endUse()
           ->withColumn('t_CAO_Intervenant_Externe.nom', 'nom')
           ->withColumn('t_CAO_Intervenant_Externe.prenom', 'prenom')
           ->withColumn('t_CAO_Intervenant_Externe.id_ref_val_civilite', 'civilite')
           ->withColumn('t_CAO_Intervenant_Externe.email', 'email')
           ->find();

        return $_t_invites;
    }

    /**
     * Recupere la liste des intervenants externes d'un organisme pouvant etre invites ponctuellement a une seance de commission pour une consultation a l'ordre du jour et dont les attributs contiennent l'expression recherchee.
     *
     * @param int    $id    : identifiant de l'objet CommonTCAOOrdreDePassage representant la consultation a l'ordre du jour ("ligne de l'ordre du jour")
     * @param string $org   : acronyme de l'organisme courant
     * @param string $token : expression recherchee parmi les attributs des agents
     *
     * @return array : liste des intervenants externes d'un organisme pouvant etre invites ponctuellement a la seance de commission pour une consultation a l'ordre du jour et dont les attributs contiennent l'expression recherchee
     *
     * @author JAP <ameline.placide@atexo.com>
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function searchIntervenantExterneForOrdreDuJour($id, $org, $token)
    {
        $query = <<<EOL
	 		SELECT t_CAO_Intervenant_Externe.id_intervenant_externe,
	 			   t_CAO_Intervenant_Externe.nom,
	 			   t_CAO_Intervenant_Externe.prenom,
	 			   t_CAO_Intervenant_Externe.organisation,
	 			   t_CAO_Intervenant_Externe.fonction,
	 			   t_CAO_Intervenant_Externe.email
	 		FROM t_CAO_Intervenant_Externe
	 		INNER JOIN Organisme ON t_CAO_Intervenant_Externe.organisme = Organisme.acronyme
	 		WHERE Organisme.acronyme = "$org"
	 		AND (
	 				t_CAO_Intervenant_Externe.nom LIKE "%$token%"
	 				OR t_CAO_Intervenant_Externe.prenom LIKE "%$token%"
	 				OR t_CAO_Intervenant_Externe.organisation LIKE "%$token%"
	 				OR t_CAO_Intervenant_Externe.fonction LIKE "%$token%"
	 				OR t_CAO_Intervenant_Externe.email LIKE "%$token%"
	 			)
	 		AND t_CAO_Intervenant_Externe.id_intervenant_externe
	 		NOT IN (
				 		SELECT t_CAO_Seance_Invite.id_intervenant_externe
				 		FROM t_CAO_Seance_Invite
				 		INNER JOIN t_CAO_Ordre_De_Passage ON t_CAO_Seance_Invite.id_ordre_de_passage = t_CAO_Ordre_De_Passage.id_ordre_de_passage
				 		WHERE t_CAO_Ordre_De_Passage.id_ordre_de_passage = $id
				 		AND 1<> 1
			 	   )
 		    AND t_CAO_Intervenant_Externe.id_intervenant_externe
	 		NOT IN (
				 		SELECT t_CAO_Seance_Intervenant_Externe.id_intervenant_externe
				 		FROM t_CAO_Seance_Intervenant_Externe
				 		INNER JOIN t_CAO_Seance ON t_CAO_Seance.id_seance = t_CAO_Seance_Intervenant_Externe.id_seance
				 		INNER JOIN t_CAO_Ordre_De_Passage ON t_CAO_Seance.id_seance = t_CAO_Ordre_De_Passage.id_seance
				 		WHERE t_CAO_Ordre_De_Passage.id_ordre_de_passage = $id
				 		
			 	   )
EOL;
        $statement = Atexo_Db::getLinkCommon(true)->query($query);
        $arrayIntExternes = [];
        $i = 0;
        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            $arrayIntExternes[$i]['id'] = $row['id_intervenant_externe'];
            $arrayIntExternes[$i]['nom'] = $row['nom'];
            $arrayIntExternes[$i]['prenom'] = $row['prenom'];
            $arrayIntExternes[$i]['email'] = $row['email'];
            $arrayIntExternes[$i]['organisation'] = $row['organisation'];
            $arrayIntExternes[$i]['fonction'] = $row['fonction'];
            ++$i;
        }

        return $arrayIntExternes;
    }

    /**
     * Recupere la liste des intervenants externes d'un organisme pouvant etre invites a une commission ou une seance de commission et dont les attributs contiennent l'expression recherchee.
     *
     * @param int    $id     : identifiant de l'objet CommonTCAOCommission (commission) ou bien de l'objet CommonTCAOSeance (seance de commission) (depend de la valeur du parametre $seance)
     * @param string $org    : acronyme de l'organisme courant
     * @param string $token  : expression recherchee parmi les attributs des agents
     * @param bool   $seance : indique si la liste des agents se rapporte a une seance ($seance = true) ou bien a une commission ($seance = false)
     *
     * @return array : liste des intervenants externes d'un organisme pouvant etre invites a une commission ou une seance de commission et dont les attributs contiennent l'expression recherchee
     *
     * @author JAP <ameline.placide@atexo.com>
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function searchIntervenantExterneByOrganisme($id, $org, $token, $seance = false)
    {
        $query = <<<EOL
	 		SELECT t_CAO_Intervenant_Externe.id_intervenant_externe,
	 			   t_CAO_Intervenant_Externe.nom,
	 			   t_CAO_Intervenant_Externe.prenom,
	 			   t_CAO_Intervenant_Externe.organisation,
	 			   t_CAO_Intervenant_Externe.fonction,
	 			   t_CAO_Intervenant_Externe.email
	 		FROM t_CAO_Intervenant_Externe
	 		INNER JOIN Organisme ON t_CAO_Intervenant_Externe.organisme = Organisme.acronyme
	 		WHERE Organisme.acronyme = "$org" 
	 		AND (
	 				t_CAO_Intervenant_Externe.nom LIKE "%$token%"
	 				OR t_CAO_Intervenant_Externe.prenom LIKE "%$token%"
	 				OR t_CAO_Intervenant_Externe.organisation LIKE "%$token%"
	 				OR t_CAO_Intervenant_Externe.fonction LIKE "%$token%"
	 				OR t_CAO_Intervenant_Externe.email LIKE "%$token%"
	 			)
EOL;

        if (!$seance) {
            $query_com = <<<EOL
 			AND t_CAO_Intervenant_Externe.id_intervenant_externe
 				NOT IN (
 							SELECT t_CAO_Commission_Intervenant_Externe.id_intervenant_externe
 							FROM t_CAO_Commission_Intervenant_Externe
 							WHERE t_CAO_Commission_Intervenant_Externe.id_commission = $id
 						)
EOL;
            $query .= $query_com;
        } else {
            $query_seance = <<<EOL
 			AND t_CAO_Intervenant_Externe.id_intervenant_externe
 				NOT IN (
 							SELECT t_CAO_Seance_Intervenant_Externe.id_intervenant_externe
 							FROM t_CAO_Seance_Intervenant_Externe
 							WHERE t_CAO_Seance_Intervenant_Externe.id_seance = $id
 						)
EOL;
            $query .= $query_seance;
        }

        $query .= <<<EOL
 			ORDER BY t_CAO_Intervenant_Externe.nom ASC, t_CAO_Intervenant_Externe.prenom ASC;
EOL;

        $statement = Atexo_Db::getLinkCommon(true)->query($query);
        $arrayIntExternes = [];
        $i = 0;
        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            $arrayIntExternes[$i]['id'] = $row['id_intervenant_externe'];
            $arrayIntExternes[$i]['nom'] = $row['nom'];
            $arrayIntExternes[$i]['prenom'] = $row['prenom'];
            $arrayIntExternes[$i]['email'] = $row['email'];
            $arrayIntExternes[$i]['organisation'] = $row['organisation'];
            $arrayIntExternes[$i]['fonction'] = $row['fonction'];
            ++$i;
        }

        return $arrayIntExternes;
    }

    /** @TODO method did not existed  */
    public function getAllIdIntervenantsCommission($idCommission, $org, $connexion = null){
        return [];
    }

    public function getIntervenantOrdreJour($idCommission, $org, $connexion = null){
        return [];
    }
    public function getIntervenantCommission($id, $idCommission, $org, $connexion = null){
        return [];
    }

    public function getAllIdIntervenantsOrdreDuJour($idOrdreJour, $org, $connexion, $data = null, $bool = false){
        return [];
    }


}
