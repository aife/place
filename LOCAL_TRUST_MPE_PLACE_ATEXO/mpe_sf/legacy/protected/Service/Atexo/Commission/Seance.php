<?php

namespace Application\Service\Atexo\Commission;

use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonTCAOCommissionConsultation;
use Application\Propel\Mpe\CommonTCAOOrdreDePassage;
use Application\Propel\Mpe\CommonTCAOOrdreDePassageQuery;
use Application\Propel\Mpe\CommonTCAOSeance;
use Application\Propel\Mpe\CommonTCAOSeanceInviteQuery;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_ValeursReferentielles;
use PDO;
use Prado\Prado;

/**
 * Librairie de manipulation des objets "Seances" pour le module Commission (CAO).
 *
 * @author JAP <ameline.placide@atexo.com>
 *
 * @version $Id: Seance.php,v 1.18 2014-11-12 13:41:02 japlacide Exp $
 *
 * @since 4.6.0
 *
 * @copyright Atexo 2014
 */
class Atexo_Commission_Seance
{
    /**
     * Recupere le nombre ou la liste des objets CommonTCAOSeance (seances de commissions) selon un ensemble de criteres.
     *
     * @param object(Criteria) $criteriaVo        : objet contenant les informations pour agencer les resultats de la requete recherchant l'ensemble des seances de commissions
     * @param bool             $returnCountResult : indique si le resultat a renvoyer doit etre le nombre de commissions ou bien la liste des commission agencees selon les informations contenues dans l'objet Criteria donne en parametre
     *
     * @return int | array : nombre ou liste de seances de commissions agencees selon les informations contenues dans l'objet Criteria donne en parametre
     *
     * @author JAP <ameline.placide@atexo.com>
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getSeances($criteriaVo, $returnCountResult = false): int|array
    {
        $commandeName = $criteriaVo->getCommandeName();
        $sensTri = $criteriaVo->getSensTri();

        if ($returnCountResult) {
            $sql = ' SELECT count(t_CAO_Seance.id_seance) as nbrSeances ';
        } else {
            $sql = ' SELECT id_seance, id_commission, date, lieu, salle, id_ref_val_statut ';
        }
        $sql .= ' FROM t_CAO_Seance ';
        $sql .= " WHERE organisme = '".(new Atexo_Db())->quote($criteriaVo->getOrganisme())."'";
        if ($criteriaVo->getIdSeance()) {
            $sql .= " AND id_seance = '".(new Atexo_Db())->quote($criteriaVo->getIdSeance())."'";
        }

        if (!$returnCountResult) {
            if ('typeCom' == $commandeName) {
                $sql .= " ORDER BY id_commission $sensTri ";
            } elseif ('statut' == $commandeName) {
                $sql .= " ORDER BY id_ref_val_statut $sensTri ";
            } elseif ('date' == $commandeName) {
                $sql .= " ORDER BY date $sensTri ";
            } elseif ('lieu' == $commandeName) {
                $sql .= " ORDER BY lieu $sensTri ";
            } elseif ('salle' == $commandeName) {
                $sql .= " ORDER BY salle $sensTri ";
            } else {
                $sql .= ' ORDER BY date desc ';
            }
        }

        if (!$returnCountResult && $criteriaVo->getLimit()) {
            $sql .= ' limit '.$criteriaVo->getOffset().','.($criteriaVo->getLimit() + $criteriaVo->getOffset());
        }
        $statement = Atexo_Db::getLinkCommon(true)->query($sql);
        $array = [];
        $i = 0;

        $_t_type_commission = (new Atexo_Commission_Commissions())->getAllTypesCommissions($criteriaVo);
        $_t_statut = self::getStatut();

        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            if ($returnCountResult) {
                return $row['nbrSeances'];
            }
            $array[$i]['id'] = $row['id_seance'];
            $array[$i]['typeCom'] = $_t_type_commission[$row['id_commission']];
            $array[$i]['statut'] = $_t_statut[$row['id_ref_val_statut']];
            $array[$i]['date'] = $row['date'];
            $array[$i]['lieu'] = Atexo_Util::atexoHtmlEntitiesDecode($row['lieu']);
            $array[$i]['salle'] = Atexo_Util::atexoHtmlEntitiesDecode($row['salle']);
            $array[$i]['picto'] = self::getPictoStatut($row['id_ref_val_statut']);
            ++$i;
        }

        return $array;
    }

    /**
     * Genere la liste des types de commissions selon un ensemble de criteres.
     *
     * @param object(Criteria) $criteriaVo : objet contenant les informations pour agencer les resultats de la requete recherchant l'ensemble des seances de commissions
     *
     * @return array : liste des types de commissions agencees selon les informations contenues dans l'objet Criteria donne en parametre
     *
     * @author JAP <ameline.placide@atexo.com>
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getCommissionType($criteriaVo)
    {
        $raw_data = (new Atexo_Commission_Commissions())->getCommissions($criteriaVo);
        //Formatage des resultats pour l'affichage dans le menu deroulant
        $data = [];
        $data[0] = Prado::localize('DEFINE_CHOIX_COMMISSION').'...';
        foreach ($raw_data as $row) {
            $data[$row['id']] = $row['sigle'].'&nbsp;-&nbsp;'.$row['intitule'];
        }

        return $data;
    }

    /**
     * Genere la liste de l'ensemble des statuts des seances de commissions disponibles.
     *
     * @param void
     *
     * @return array : liste de l'ensemble des statuts des seances de commissions disponibles
     *
     * @author JAP <ameline.placide@atexo.com>
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getStatut()
    {
        $data = (new Atexo_ValeursReferentielles())->getValeurRefByIdRef(Atexo_Config::getParameter('ID_REFERENTIEL_STATUT_SEANCE'));

        return $data;
    }

    /**
     * Recupere le nom du picto associe au statut de seance de commission.
     *
     * @param int $idEtat : statut de seance de commission
     *
     * @return string : nom du picto associe au statut de seance de commission donne en parametre
     *
     * @author JAP <ameline.placide@atexo.com>
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getPictoStatut($idEtat)
    {
        if ($idEtat == Atexo_Config::getParameter('SEANCE_EN_ATTENTE_EDITION')) {
            return Atexo_Config::getParameter('PICTO_SEANCE_EN_ATTENTE_EDITION');
        }
        if ($idEtat == Atexo_Config::getParameter('SEANCE_ENVOYE')) {
            return Atexo_Config::getParameter('PICTO_SEANCE_ENVOYE');
        }
        if ($idEtat == Atexo_Config::getParameter('SEANCE_A_ARCHIVER')) {
            return Atexo_Config::getParameter('PICTO_SEANCE_A_ARCHIVER');
        }
        if ($idEtat == Atexo_Config::getParameter('SEANCE_ARCHIVE')) {
            return Atexo_Config::getParameter('PICTO_SEANCE_ARCHIVE');
        }
    }

    /**
     * Recupere le libelle associe au statut de seance de commission.
     *
     * @param int $idEtat : statut de seance de commission
     *
     * @return string : libelle associe au statut de seance de commission donne en parametre
     *
     * @author JAP <ameline.placide@atexo.com>
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getLibelleStatut($idEtat)
    {
        $data = self::getStatut();

        if (isset($data[$idEtat])) {
            return $data[$idEtat];
        } else {
            return '';
        }
    }

    /**
     * Supprime un passage a l'ordre du jour d'une seance de commission.
     *
     * @param int    $idOrdreDePassage : identifiant de l'ordre de passage de la consultation a l'ordre du jour d'une seance de commission
     * @param string $organisme        : acronyme de l'organisme courant
     *
     * @return string : resultat de la requete SQL de suppression
     *
     * @author JAP <ameline.placide@atexo.com>
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function supprimerOrdreDePassage($idOrdreDePassage, $organisme)
    {
        $item = CommonTCAOOrdreDePassageQuery::create()
           ->findPk([$idOrdreDePassage, $organisme]);

        if ($item) {
            //Suppression des invites rattaches uniquement a cette consultation de l'ordre du jour
            $nbDel = CommonTCAOSeanceInviteQuery::create()
               ->filterByIdOrdreDePassage($idOrdreDePassage)
               ->filterByOrganisme($organisme)
               ->delete();

            //Suppression de de l'ordre de passage
            $nbDel = $item->delete();
        }
    }

    /**
     * Recupere l'ensemble des ordres de passage d'une seance de commission (consultations a l'ordre du jour d'une seance).
     *
     * @param int    $idSeance  : identifiant d'une seance de commission
     * @param string $organisme : acronyme de l'organisme courant
     *
     * @return array : liste des ordres de passage de la seance de commission (consultations a l'ordre du jour d'une seance) dont l'identifiant est donne en parametre
     *
     * @author JAP <ameline.placide@atexo.com>
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getOrdreDuJour($idSeance, $organisme)
    {
        $ordreDuJour = CommonTCAOOrdreDePassageQuery::create()
           ->joinWith('CommonTCAOCommissionConsultation')
           ->filterByIdSeance($idSeance)
           ->filterByOrganisme($organisme)
           ->useCommonTCAOSeanceRelatedByIdSeanceQuery()
               ->filterByOrganisme($organisme)
               ->useCommonTCAOCommissionQuery()
                   ->filterByOrganisme($organisme)
               ->endUse()
           ->endUse()
           ->where('t_CAO_Seance.date = t_CAO_Ordre_De_Passage.date_seance')
           ->orderByOrdreDePassage('asc')
           ->orderByDatePassage('asc')
           ->find();

        return $ordreDuJour;
    }

    /**
     * Recupere le libelle d'une etape d'une seance de commission ou bien l'ensemble des libelles des etapes de seances de commission disponibles.
     *
     * @param int $idEtat : etape de seance de commission
     *
     * @return string | array : libelle d'une etape d'une seance de commission dont la valeur est donnee en parametre ou bien la liste de l'ensemble des libelles des etapes de seances de commission disponibles
     *
     * @author JAP <ameline.placide@atexo.com>
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getEtapeListe($idEtape = null): string|array
    {
        $dataSource = (new Atexo_ValeursReferentielles())->getValeurRefByIdRef(Atexo_Config::getParameter('ID_REFERENTIEL_ETAPES_ORDRES_DU_JOUR'));
        if ($idEtape) {
            return $dataSource[$idEtape];
        }

        return $dataSource;
    }

    /**
     * Recupere l'objet CommonTCAOOrdreDePassage ("ligne de l'ordre du jour") representant le precedant passage en commission d'une consultation.
     *
     * @param int                                      $idSeance     : identifiant de la seance de commission
     * @param int                                      $dateSeance   : date de la seance de commission courante
     * @param object(CommonTCAOCommissionConsultation) $consultation : consultation associee a une commission
     *
     * @return object(CommonTCAOOrdreDePassage) : passage en commission de la consultation donnee en parametre et dont la date est anterieure a celle du passage en commission donnee egalement en parametre
     *
     * @author JAP <ameline.placide@atexo.com>
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getPrecedenteSeance($idSeance, $dateSeance, CommonTCAOCommissionConsultation $CAOConsultation)
    {
        if ($CAOConsultation->getIdCommission()) {
            $ordre_de_passage = CommonTCAOOrdreDePassageQuery::create()
               ->filterByIdCommission($CAOConsultation->getIdCommission())
               ->useCommonTCAOCommissionConsultationQuery()
                   ->filterByOrganisme($CAOConsultation->getOrganisme())
                   ->filterByIdCommission($CAOConsultation->getIdCommission())
               ->endUse()
               ->where('t_CAO_Ordre_De_Passage.id_seance <> "'.$idSeance.'"')
               ->where('t_CAO_Ordre_De_Passage.date_seance < DATE("'.$dateSeance.'")')
               ->orderByDateSeance('desc')
               ->findOne();
        } else {
            $ordre_de_passage = CommonTCAOOrdreDePassageQuery::create()
               ->filterByOrganisme($CAOConsultation->getOrganisme())
               //->filterByIdCommission($consultation->getIdCommission())
               ->useCommonTCAOCommissionConsultationQuery()
                   ->filterByOrganisme($CAOConsultation->getOrganisme())
                   //->filterByIdCommission($consultation->getIdCommission())
                   ->filterByRefLibre($CAOConsultation->getRefLibre())
               ->endUse()
               ->where('t_CAO_Ordre_De_Passage.id_seance <> "'.$idSeance.'"')
               ->where('t_CAO_Ordre_De_Passage.date_seance < DATE("'.$dateSeance.'")')
               ->orderByDateSeance('desc')
               ->findOne();
        }

        return $ordre_de_passage;
    }

    /**
     * Recupere la liste de l'ensemble des agents et intervenants externes membres permanents d'une seance et membres ponctuels pour une consultation a l'ordre du jour d'une seance de commission.
     *
     * @param int    $idSeance         : identifiant de la seance de commission
     * @param int    $idOrdreDePassage : identifiant de l'ordre de passage d'une seance de commission ("ligne de l'ordre du jour")
     * @param string $org              : acronyme de l'organisme courant
     *
     * @return array : liste de l'ensemble des agents et intervenants externes membres permanents de la seance dont l'identifiant est donne en parametre et membres ponctuels pour une consultation a l'ordre du jour d'une seance de commission dont l'identifiant de l'ordre de passage est donne en parametre
     *
     * @author JAP <ameline.placide@atexo.com>
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getAllMembresOrdreDePassage($idSeance, $idOrdreDePassage, $org)
    {
        //Recuperation des invites permanents
        $_t_permanents = self::getAllPermanentsSeance($idSeance, $org);

        //Recuperation des invites ponctuels
        $_t_ponctuels = self::getAllInvitesOrdreDePassage($idOrdreDePassage, $org);

        //Fusion des deux listes
        $_t_invites = array_merge((array) $_t_permanents, (array) $_t_ponctuels);

        return $_t_invites;
    }

    /**
     * Recupere la liste de l'ensemble des agents et intervenants externes membres ponctuels pour une consultation a l'ordre du jour d'une seance de commission.
     *
     * @param int    $idOrdreDePassage : identifiant de l'ordre de passage d'une seance de commission ("ligne de l'ordre du jour")
     * @param string $org              : acronyme de l'organisme courant
     *
     * @return array : liste de l'ensemble des agents et intervenants externes membres ponctuels pour une consultation a l'ordre du jour d'une seance de commission dont l'identifiant de l'ordre de passage est donne en parametre
     *
     * @author JAP <ameline.placide@atexo.com>
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getAllInvitesOrdreDePassage($idOrdreDePassage, $org)
    {
        //Recuperation des agents
        $_t_agents = (new Atexo_Commission_Agent())->getAllAgentsOrdreDePassage($idOrdreDePassage, $org);

        //Recuperation des intervenants
        $_t_intervenants = (new Atexo_Commission_IntervenantExterne())->getAllIntervenantsExternesOrdreDePassage($idOrdreDePassage, $org);

        //Fusion des deux listes
        $_t_invites = array_merge((array) $_t_agents, (array) $_t_intervenants);

        return $_t_invites;
    }

    /**
     * Recupere la liste de l'ensemble des agents et intervenants externes membres permanents d'une seance de commission.
     *
     * @param int    $idSeance : identifiant de la seance de commission
     * @param string $org      : acronyme de l'organisme courant
     *
     * @return array : liste de l'ensemble des agents et intervenants externes membres permanents de la seance de commission dont l'identifiant est donne en parametre
     *
     * @author JAP <ameline.placide@atexo.com>
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getAllPermanentsSeance($idSeance, $org)
    {
        //Recuperation des agents
        $_t_agents = (new Atexo_Commission_Agent())->getAllAgentsSeance($idSeance, $org);

        //Recuperation des intervenants
        $_t_intervenants = (new Atexo_Commission_IntervenantExterne())->getAllIntervenantsExternesSeance($idSeance, $org);

        //Fusion des deux listes
        $_t_permanents = array_merge((array) $_t_agents, (array) $_t_intervenants);

        return $_t_permanents;
    }

    /**
     * Recupere la liste de l'ensemble des agents et intervenants externes membres ponctuels d'une seance de commission.
     *
     * @param int    $idSeance : identifiant de la seance de commission
     * @param string $org      : acronyme de l'organisme courant
     *
     * @return array : liste de l'ensemble des agents et intervenants externes membres ponctuels de la seance de commission dont l'identifiant est donne en parametre
     *
     * @author JAP <ameline.placide@atexo.com>
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getAllInvitesSeance($idSeance, $org)
    {
        //Recuperation des agents
        $_t_agents = (new Atexo_Commission_Agent())->getAllInvitesSeance($idSeance, $org);

        //Recuperation des intervenants
        $_t_intervenants = (new Atexo_Commission_IntervenantExterne())->getAllInvitesSeance($idSeance, $org);

        //Fusion des deux listes
        $_t_invites = array_merge((array) $_t_agents, (array) $_t_intervenants);

        return $_t_invites;
    }
}
