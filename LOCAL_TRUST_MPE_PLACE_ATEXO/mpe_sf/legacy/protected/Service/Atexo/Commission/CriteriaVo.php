<?php

namespace Application\Service\Atexo\Commission;

/**
 * Vo pour definir les criteres de Recherche.
 *
 * @author KONATE Oumar <oumar.konate@atexo.com>
 * @copyright Atexo 2012
 *
 * @since MPE4
 */
class Atexo_Commission_CriteriaVo
{
    private $organisme;
    private $offset;
    private $limit;
    private $_commandeName;
    private $_sensTri;
    private $_idCommission;
    private $_idSeance;

    public function getOrganisme()
    {
        return $this->organisme;
    }

    public function setOrganisme($organisme)
    {
        $this->organisme = $organisme;
    }

    public function getOffset()
    {
        return $this->offset;
    }

    public function setOffset($offset)
    {
        $this->offset = $offset;
    }

    public function getLimit()
    {
        return $this->limit;
    }

    public function setLimit($limit)
    {
        $this->limit = $limit;
    }

    public function getCommandeName()
    {
        return $this->_commandeName;
    }

    public function setCommandeName($commandeName)
    {
        $this->_commandeName = $commandeName;
    }

    public function getSensTri()
    {
        return $this->_sensTri;
    }

    public function setSensTri($sensTri)
    {
        $this->_sensTri = $sensTri;
    }

    public function getIdCommission()
    {
        return $this->_idCommission;
    }

    public function setIdCommission($value)
    {
        $this->_idCommission = $value;
    }

    public function getIdSeance()
    {
        return $this->_idSeance;
    }

    public function setIdSeance($value)
    {
        $this->_idSeance = $value;
    }
}
