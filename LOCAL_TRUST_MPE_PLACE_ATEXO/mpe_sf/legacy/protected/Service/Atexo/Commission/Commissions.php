<?php

namespace Application\Service\Atexo\Commission;

use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonTCAOCommissionQuery;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_Util;
use PDO;

/**
 * Librairie de manipulation des objets "Commission" pour le module Commission (CAO).
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 *
 * @version $Id: Commissions.php,v 1.21 2014-11-05 10:22:42 japlacide Exp $
 *
 * @since 4.2
 *
 * @copyright Atexo 2012
 */
class Atexo_Commission_Commissions
{
    /**
     * Recupere le nombre ou la liste des objets CommonTCAOCommission (commissions) selon un ensemble de criteres.
     *
     * @param object(Criteria) $criteriaVo        : objet contenant les informations pour agencer les resultats de la requete recherchant l'ensemble des commissions
     * @param bool             $returnCountResult : indique si le resultat a renvoyer doit etre le nombre de commissions ou bien la liste des commission agencees selon les informations contenues dans l'objet Criteria donne en parametre
     *
     * @return int | array : nombre ou liste de commissions agencees selon les informations contenues dans l'objet Criteria donne en parametre
     *
     * @author JAP <ameline.placide@atexo.com>
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getCommissions($criteriaVo, $returnCountResult = false): int|array
    {
        $commandeName = $criteriaVo->getCommandeName();
        $sensTri = $criteriaVo->getSensTri();

        if ($returnCountResult) {
            $sql = ' SELECT count(t_CAO_Commission.id_commission) as nbrCommissions ';
        } else {
            $sql = ' SELECT id_commission, sigle, intitule ';
        }
        $sql .= ' FROM t_CAO_Commission ';
        $sql .= " WHERE organisme = '".(new Atexo_Db())->quote($criteriaVo->getOrganisme())."'";
        if ($criteriaVo->getIdCommission()) {
            $sql .= " AND id_commission = '".(new Atexo_Db())->quote($criteriaVo->getIdCommission())."'";
        }

        if (!$returnCountResult) {
            if ('sigle' == $commandeName) {
                $sql .= ' ORDER BY sigle '.$sensTri;
            } elseif ('intitule' == $commandeName) {
                $sql .= ' ORDER BY intitule '.$sensTri;
            }
        }

        if (!$returnCountResult && $criteriaVo->getLimit()) {
            $sql .= ' limit '.$criteriaVo->getOffset().','.($criteriaVo->getLimit() + $criteriaVo->getOffset());
        }
        $statement = Atexo_Db::getLinkCommon(true)->query($sql);
        $arrayCommissions = [];
        $i = 0;
        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            if ($returnCountResult) {
                return $row['nbrCommissions'];
            }
            $arrayCommissions[$i]['id'] = $row['id_commission'];
            $arrayCommissions[$i]['sigle'] = Atexo_Util::atexoHtmlEntitiesDecode($row['sigle']);
            $arrayCommissions[$i]['intitule'] = Atexo_Util::atexoHtmlEntitiesDecode($row['intitule']);
            ++$i;
        }

        return $arrayCommissions;
    }

    /**
     * Recupere le nombre de seances de commission liees a une commission.
     *
     * @param int    $idCommission: identifiant de l'objet CommonTCAOCommission (commission)
     * @param string $organisme:    acronyme de l'organisme courant
     *
     * @return int : nombre de seances de commission liees a la commission dont l'identifiant est donne en parametre
     *
     * @author JAP <ameline.placide@atexo.com>
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function hasDependantSeances($idCommission, $organisme)
    {
        $query = <<<EOL
			SELECT COUNT(*)
			AS numb
 			FROM t_CAO_Seance
 			WHERE t_CAO_Seance.id_commission = "{$idCommission}"
 			AND t_CAO_Seance.organisme = "{$organisme}" 					
EOL;
        $statement = Atexo_Db::getLinkCommon(true)->query($query);
        $rows = $statement->fetch(PDO::FETCH_ASSOC);

        return $rows['numb'];
    }

    /**
     * Supprime une commission et les agents/intervenants membres de la commission.
     *
     * @param $id: identifiant de l'objet CommonTCAOCommission (commission)
     * @param $organisme: acronyme de l'organisme courant
     *
     * @return int : statut de la requete SQL de suppression
     *
     * @author JAP <ameline.placide@atexo.com>
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function supprimer($id, $organisme)
    {
        //Recuperation de la commission a supprimer
        $commission = CommonTCAOCommissionQuery::create()
           ->findPk([$id, $organisme]);

        //Supprimer les agents rattaches a la commission
        $_t_agents = (new Atexo_Commission_Agent())->getAllIdsAgentsCommission($id, $organisme, null, true);
        foreach ($_t_agents as $_agent) {
            $_agent->delete();
        }

        //Supprimer les intervenants externes rattaches a la commission
        $_t_intervenants = (new Atexo_Commission_IntervenantExterne())->getAllIdsIntervenantsCommission($id, $organisme, null, true);
        foreach ($_t_intervenants as $_intervenant) {
            $_intervenant->delete();
        }

        return $commission->delete();
    }

    /**
     * Permet de recuperer tous les types de commission
     * Formate les donnees sous la forme [idCom] => [typeCom formate].
     *
     * @param Atexo_Commission_CriteriaVo $criteriaVo: objet virtuel critere
     *
     * @return array : liste des types de commission
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2015
     */
    public function getAllTypesCommissions($criteriaVo)
    {
        $sql = ' SELECT id_commission, sigle, intitule ';
        $sql .= ' FROM t_CAO_Commission ';
        $sql .= " WHERE organisme = '".(new Atexo_Db())->quote($criteriaVo->getOrganisme())."'";

        $statement = Atexo_Db::getLinkCommon(true)->query($sql);
        $data = [];
        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            $data[$row['id_commission']] = $row['sigle'].'&nbsp;-&nbsp;'.$row['intitule'];
        }

        return $data;
    }

    public function remplirListeOrdreDuJour($id, $organisme){
        return [];
    }

    public function remplirListeStatutCommission(){
        return [];
    }

    public function remplirListeTypeCommission(){
        return [];
    }

}
