<?php

namespace Application\Service\Atexo\Commission;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonAgent;
use Application\Propel\Mpe\CommonAgentPeer;
use Application\Propel\Mpe\CommonTCAOCommissionAgent;
use Application\Propel\Mpe\CommonTCAOCommissionAgentPeer;
use Application\Propel\Mpe\CommonTCAOOrdreDePassage;
use Application\Propel\Mpe\CommonTCAOSeance;
use Application\Propel\Mpe\CommonTCAOSeanceAgent;
use Application\Propel\Mpe\CommonTCAOSeanceAgentPeer;
use Application\Propel\Mpe\CommonTCAOSeanceAgentQuery;
use Application\Propel\Mpe\CommonTCAOSeanceInvite;
use Application\Propel\Mpe\CommonTCAOSeanceInviteQuery;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_EntityPurchase;
use PDO;

/**
 * Librairie de manipulation des agents pour le module Commission (CAO).
 *
 * @author JAP <ameline.placide@atexo.com>
 *
 * @version 1.8
 *
 * @since MPE 4.6
 *
 * @copyright Atexo 2014
 */
class Atexo_Commission_Agent
{
    /**
     * Genere le critere specifiant l'agencement des resultats d'une requete SQL recherchant un ensemble d'agents.
     *
     * @param string $org     : acronyme de l'organisme courant
     * @param string $tri     : nom du type d'information sur lequel porte le tri
     * @param string $sensTri : code du sens du tri : "ASC" ou "DESC"
     * @param array  $listIds : liste des identifiants des enregistrements CommonAgent sur lesquels le tri doit etre effectue
     *
     * @return object(Criteria) : objet contenant les informations pour trier les resultats de la requete recherchant un ensemble d'agents
     *
     * @author JAP <ameline.placide@atexo.com>
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function generateCriteria($org, $tri, $sensTri, $listIds)
    {
        $stri = null;
        $c = new Criteria();
        $c->add(CommonAgentPeer::ORGANISME, $org);
        $c->add(CommonAgentPeer::ID, $listIds, Criteria::IN);
        if (!empty($sensTri)) {
            if ('typeVoix' == $stri) {
                //@TODO : implementer ce cas (le type de voix n'est pas defini au niveau de la meme table
            } else {
                if ('ASC' == $sensTri) {
                    switch ($tri) {
                        case 'nom':
                            $c->addAscendingOrderByColumn(CommonAgentPeer::NOM);
                            break;
                        case 'prenom':
                            $c->addAscendingOrderByColumn(CommonAgentPeer::PRENOM);
                            break;
                        case 'service':
                            $c->addAscendingOrderByColumn(CommonAgentPeer::SERVICE_ID);
                            break;
                        case 'typeCompte':
                            $c->addAscendingOrderByColumn(CommonAgentPeer::ELU);
                            break;
                        default:
                            $c->addAscendingOrderByColumn(CommonAgentPeer::NOM);
                            break;
                    }
                } else {
                    switch ($tri) {
                        case 'nom':
                            $c->addDescendingOrderByColumn(CommonAgentPeer::NOM);
                            break;
                        case 'prenom':
                            $c->addDescendingOrderByColumn(CommonAgentPeer::PRENOM);
                            break;
                        case 'service':
                            $c->addDescendingOrderByColumn(CommonAgentPeer::SERVICE_ID);
                            break;
                        case 'typeCompte':
                            $c->addDescendingOrderByColumn(CommonAgentPeer::ELU);
                            break;
                        default:
                            $c->addDescendingOrderByColumn(CommonAgentPeer::NOM);
                            break;
                    }
                }
            }
        } else {
            $c->addAscendingOrderByColumn(CommonAgentPeer::NOM);
        }

        return $c;
    }

    /**
     * Recupere la liste des agents appartenant a une commission.
     *
     * @param int         $idCommission : identifiant de l'objet CommonTCAOCommission
     * @param string      $org          : acronyme de l'organisme courant
     * @param object(PDO) $connexion    : objet de connexion a la base de donnees
     * @param booleanean  $returnObjet  : type de liste a retourner : identifiants ou objets complets
     *
     * @return array : liste d'objets ou d'identifiants de CommonTCAOCommissionAgent representant l'ensemble des agents appartenant a la commission dont l'identifiant est donne en parametre
     *
     * @author JAP <ameline.placide@atexo.com>
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getAllIdsAgentsCommission($idCommission, $org, $connexion = null, $returnObjet = false)
    {
        if (!$connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        }
        $c = new Criteria();
        $c->add(CommonTCAOCommissionAgentPeer::ID_COMMISSION, $idCommission);
        $c->add(CommonTCAOCommissionAgentPeer::ORGANISME, $org);
        $agentsComm = CommonTCAOCommissionAgentPeer::doSelect($c, $connexion);

        if ($returnObjet) {
            return $agentsComm;
        }

        $arrayIds = [];

        foreach ($agentsComm as $agentComm) {
            $arrayIds[$agentComm->getIdAgent()] = $agentComm->getIdAgent();
        }

        return $arrayIds;
    }

    /**
     * Recupere la liste des agents appartenant a une seance de commission.
     *
     * @param int         $idSeance  : identifiant de l'objet CommonTCAOSeance
     * @param string      $org       : acronyme de l'organisme courant
     * @param object(PDO) $connexion : objet de connexion a la base de donnees
     * @param bool : type de liste a retourner : identifiants ou objets complets
     *
     * @return array : liste d'objets ou d'identifiants de CommonTCAOSeanceAgent representant l'ensemble des agents appartenant a la seance de commission dont l'identifiant est donne en parametre
     *
     * @author JAP <ameline.placide@atexo.com>
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getAllIdsAgentsSeance($idSeance, $org, $returnObjet = false)
    {
        $_t_agents = CommonTCAOSeanceAgentQuery::create()
           ->filterByIdSeance($idSeance)
           ->filterByOrganisme($org)
           ->find();

        if ($returnObjet) {
            return $_t_agents;
        }

        $arrayIds = [];

        foreach ($_t_agents as $agent) {
            $arrayIds[$agent->getIdAgent()] = $agent->getIdAgent();
        }

        return $arrayIds;
    }

    /**
     * Recupere la liste des agents membres d'une seance de commission.
     *
     * @param int    $idSeance : identifiant de l'objet CommonTCAOSeance
     * @param string $org      : acronyme de l'organisme courant
     *
     * @return array : liste d'objets partiels (champs nom, prenom, civilite, email) de CommonTCAOSeanceAgent representant l'ensemble des agents membres de la seance de commission dont l'identifiant est donne en parametre
     *
     * @author JAP <ameline.placide@atexo.com>
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getAllAgentsSeance($idSeance, $org)
    {
        $_t_agents = CommonTCAOSeanceAgentQuery::create()
           ->filterByIdSeance($idSeance)
           ->filterByOrganisme($org)
           ->useCommonAgentQuery()
               ->filterByOrganisme($org)
           ->endUse()
           ->withColumn('Agent.nom', 'nom')
           ->withColumn('Agent.prenom', 'prenom')
           ->withColumn('Agent.civilite', 'civilite')
           ->withColumn('Agent.email', 'email')
           ->find();

        return $_t_agents;
    }

    /**
     * Recupere la liste des agents invites ponctuellement pour une consultation a l'ordre du jour d'une seance de commission.
     *
     * @param int    $idOrdreDePassage : identifiant de l'objet CommonTCAOOrdreDePassage (consultation en "passage" de l'ordre du jour)
     * @param string $org              : acronyme de l'organisme courant
     *
     * @return array : liste d'objets partiels (champs nom, prenom, civilite, email) de CommonTCAOSeanceInvite representant l'ensemble des agents invites ponctuellement pour une consultation a l'ordre du jour de la seance de commission dont l'identifiant est donne en parametre
     *
     * @author JAP <ameline.placide@atexo.com>
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getAllAgentsOrdreDePassage($idOrdreDePassage, $org)
    {
        $_t_agents = CommonTCAOSeanceInviteQuery::create()
           ->filterByIdOrdreDePassage($idOrdreDePassage)
           ->filterByOrganisme($org)
           ->useCommonAgentQuery()
               ->filterByOrganisme($org)
           ->endUse()
           ->withColumn('Agent.nom', 'nom')
           ->withColumn('Agent.prenom', 'prenom')
           ->withColumn('Agent.email', 'email')
           ->withColumn('Agent.civilite', 'civilite')
           ->find();

        return $_t_agents;
    }

    /**
     * Recupere la liste de tous les agents invites ponctuellement pour une ou plusieurs consultations a l'ordre du jour d'une seance de commission.
     *
     * @param int    $idSeance : identifiant de l'objet CommonTCAOSeance (seance de commission)
     * @param string $org      : acronyme de l'organisme courant
     *
     * @return array : liste d'objets partiels (champs nom, prenom, civilite, email) de CommonTCAOSeanceInvite representant l'ensemble des agents rattaches ponctuellement a la seance de commission dont l'identifiant est donne en parametre
     *
     * @author JAP <ameline.placide@atexo.com>
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getAllInvitesSeance($idSeance, $org)
    {
        $_t_invites = CommonTCAOSeanceInviteQuery::create()
           ->filterByOrganisme($org)
           ->useCommonTCAOOrdreDePassageQuery()
               ->filterByIdSeance($idSeance)
               ->filterByOrganisme($org)
           ->endUse()
           ->useCommonAgentQuery()
               ->filterByOrganisme($org)
           ->endUse()
           ->withColumn('Agent.nom', 'nom')
           ->withColumn('Agent.prenom', 'prenom')
           ->withColumn('Agent.email', 'email')
           ->withColumn('Agent.civilite', 'civilite')
           ->find();

        return $_t_invites;
    }

    /**
     * Recupere l'agent appartenant a une commission.
     *
     * @param int         $id           : identifiant de l'objet CommonAgent
     * @param int         $idCommission : identifiant de l'objet CommonTCAOCommission (commission)
     * @param string      $org          : acronyme de l'organisme courant
     * @param object(PDO) $connexion    : objet de connexion a la base de donnees
     *
     * @return object(CommonTCAOCommissionAgent) : objet representant un agent appartenant a la commission dont l'identifiant est donne en parametre
     *
     * @author JAP <ameline.placide@atexo.com>
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getCommissionAgent($id, $idCommission, $org, $connexion = null)
    {
        if (!$connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        }
        $c = new Criteria();
        $c->add(CommonTCAOCommissionAgentPeer::ID_AGENT, $id);
        $c->add(CommonTCAOCommissionAgentPeer::ID_COMMISSION, $idCommission);
        $c->add(CommonTCAOCommissionAgentPeer::ORGANISME, $org);
        $agentComm = CommonTCAOCommissionAgentPeer::doSelectOne($c, $connexion);

        return $agentComm;
    }

    /**
     * Recupere l'agent invite a une seance de commission.
     *
     * @param int         $id        : identifiant de l'objet CommonAgent
     * @param int         $idSeance  : identifiant de l'objet CommonTCAOSeance (seance de commission)
     * @param string      $org       : acronyme de l'organisme courant
     * @param object(PDO) $connexion : objet de connexion a la base de donnees
     *
     * @return object(CommonTCAOCommissionAgent) : objet representant un agent invite a la seance de commission dont l'identifiant est donne en parametre
     *
     * @author JAP <ameline.placide@atexo.com>
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getSeanceAgent($id, $idSeance, $org, $connexion = null)
    {
        if (!$connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        }
        $c = new Criteria();
        $c->add(CommonTCAOSeanceAgentPeer::ID_AGENT, $id);
        $c->add(CommonTCAOSeanceAgentPeer::ID_SEANCE, $idSeance);
        $c->add(CommonTCAOSeanceAgentPeer::ORGANISME, $org);
        $agent = CommonTCAOSeanceAgentPeer::doSelectOne($c, $connexion);

        return $agent;
    }

    /**
     * Recupere le nombre d'agents permanents invites a une seance de commission.
     *
     * @param int         $idSeance  : identifiant de l'objet CommonTCAOSeance (seance de commission)
     * @param string      $org       : acronyme de l'organisme courant
     * @param object(PDO) $connexion : objet de connexion a la base de donnees
     *
     * @return int : nombre d'agents permanents invites a la seance de commission dont l'identifiant est donne en parametre
     *
     * @author JAP <ameline.placide@atexo.com>
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getNombreSeanceAgents($idSeance, $org, $connexion = null)
    {
        if (!$connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        }

        $count = CommonTCAOSeanceAgentQuery::create()
        ->filterByIdSeance($idSeance)
        ->filterByOrganisme($org)
        ->count($connexion);

        return $count;
    }

    /**
     * Attribue automatiquement a une seance tous les agents membres permanents de la commission de la seance.
     *
     * @param int         $idSeance     : identifiant de l'objet CommonTCAOSeance (seance de commission)
     * @param int         $idCommission : identifiant de l'objet CommonTCAOCommission (commission)
     * @param string      $org          : acronyme de l'organisme courant
     * @param object(PDO) $connexion    : objet de connexion a la base de donnees
     *
     * @return void
     *
     * @author JAP <ameline.placide@atexo.com>
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function addCommissionAgentsToSeance($idSeance, $idCommission, $org, $connexion = null)
    {
        if (!$connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        }
        //Recuperation des agents de la commission
        $_t_agents = self::getAllIdsAgentsCommission($idCommission, $org, $connexion);
        //Ajout par defaut des agents de la commission
        foreach ($_t_agents as $key) {
            $agentCommission = self::getCommissionAgent($_t_agents[$key], $idCommission, $org, $connexion);

            if ($agentCommission instanceof CommonTCAOCommissionAgent) {
                $agentSeance = new CommonTCAOSeanceAgent();
                $agentSeance->setIdSeance($idSeance);
                $agentSeance->setOrganisme($org);
                $agentSeance->setIdAgent($agentCommission->getIdAgent());
                $agentSeance->setTypeCompte($agentCommission->getTypeCompte());
                $agentSeance->setIdRefValTypeVoix($agentCommission->getIdRefValTypeVoixDefaut());
                $agentSeance->save($connexion);
            }
        }
    }

    /**
     * Retire tous les agents membres permanents d'une seance de commission (reinitialisation).
     *
     * @param int         $idSeance  : identifiant de l'objet CommonTCAOSeance (seance de commission)
     * @param string      $org       : acronyme de l'organisme courant
     * @param object(PDO) $connexion : objet de connexion a la base de donnees
     *
     * @return void
     *
     * @author JAP <ameline.placide@atexo.com>
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function removeAgentsFromSeance($idSeance, $org, $connexion = null)
    {
        if (!$connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        }
        $_t_agents = self::getAllIdsAgentsSeance($idSeance, $org, $connexion);

        foreach ($_t_agents as $key) {
            $agentSeance = self::getSeanceAgent($_t_agents[$key], $idSeance, $org, $connexion);
            if ($agentSeance instanceof CommonTCAOSeanceAgent) {
                $agentSeance->delete($connexion);
            }
        }
    }

    /**
     * Supprime un agent de la liste des agents appartenant a une commission.
     *
     * @param int    $id           : identifiant de l'objet CommonAgent (agent)
     * @param int    $idCommission : identifiant de l'objet CommonTCAOCommission (commission)
     * @param string $org          : acronyme de l'organisme courant
     *
     * @return void
     *
     * @author JAP <ameline.placide@atexo.com>
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function deleteAgentCommission($id, $idCommission, $org)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $agent = self::getCommissionAgent($id, $idCommission, $org, $connexion);
        if ($agent instanceof CommonTCAOCommissionAgent) {
            $agent->delete($connexion);
        }
    }

    /**
     * Supprime un agent de la liste des agents permanents invites a une seance de commission.
     *
     * @param int    $id       : identifiant de l'objet CommonAgent (agent)
     * @param int    $idSeance : identifiant de l'objet CommonTCAOSeance (seance de commission)
     * @param string $org      : acronyme de l'organisme courant
     *
     * @return void
     *
     * @author JAP <ameline.placide@atexo.com>
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function deleteSeanceAgent($id, $idSeance, $org)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $agent = self::getSeanceAgent($id, $idSeance, $org, $connexion);
        if ($agent instanceof CommonTCAOSeanceAgent) {
            $agent->delete($connexion);
        }
    }

    /**
     * Recupere la liste des agents d'un organisme pouvant etre invites ponctuellement a une seance de commission pour une consultation a l'ordre du jour et dont les attributs contiennent l'expression recherchee.
     *
     * @param int    $id    : identifiant de l'objet CommonTCAOOrdreDePassage representant la consultation a l'ordre du jour ("ligne de l'ordre du jour")
     * @param string $org   : acronyme de l'organisme courant
     * @param string $token : expression recherchee parmi les attributs des agents
     *
     * @return array : liste des agents d'un organisme pouvant etre invites ponctuellement a la seance de commission pour une consultation a l'ordre du jour et dont les attributs contiennent l'expression recherchee
     *
     * @author JAP <ameline.placide@atexo.com>
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function searchAgentsForOrdreDuJour($id, $org, $token)
    {
        $query = <<<EOL
             SELECT
                Agent.id,
                Agent.nom,
                Agent.prenom,
                Agent.service_id,
                Agent.elu as type_compte,
                Agent.email
             FROM Service, Agent
             INNER JOIN Organisme ON Agent.organisme = Organisme.acronyme 
             WHERE Organisme.acronyme = "$org" 
             AND Agent.service_id = Service.id
             AND Service.organisme = Agent.organisme
             AND (
                Agent.nom LIKE "%$token%"
                OR Agent.prenom LIKE "%$token%"
                OR Agent.email LIKE "%$token%"
                OR Service.sigle LIKE "%$token%"
                OR Service.libelle LIKE "%$token%"
             )
             AND Agent.id
             NOT IN (
                SELECT t_CAO_Seance_Invite.id_agent
                FROM t_CAO_Seance_Invite
                INNER JOIN t_CAO_Ordre_De_Passage ON t_CAO_Seance_Invite.id_ordre_de_passage = t_CAO_Ordre_De_Passage.id_ordre_de_passage
                WHERE t_CAO_Ordre_De_Passage.id_ordre_de_passage = $id
                AND 1 <> 1
            )
            AND Agent.id
            NOT IN (
                SELECT t_CAO_Seance_Agent.id_agent
                FROM t_CAO_Seance_Agent
                INNER JOIN t_CAO_Seance ON t_CAO_Seance.id_seance = t_CAO_Seance_Agent.id_seance
                INNER JOIN t_CAO_Ordre_De_Passage ON t_CAO_Seance.id_seance = t_CAO_Ordre_De_Passage.id_seance
                WHERE t_CAO_Ordre_De_Passage.id_ordre_de_passage = $id
            )
EOL;
        $statement = Atexo_Db::getLinkCommon(true)->query($query);
        $arrayAgents = [];
        $i = 0;
        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            $arrayAgents[$i] = $row;
            $arrayAgents[$i]['service_path'] = Atexo_EntityPurchase::getSigleLibelleEntityById($row['service_id'], $org);
            ++$i;
        }

        return $arrayAgents;
    }

    /**
     * Permet de faire une recherche des agents par organisme et/ou service.
     *
     * @param int    $id:    identifiant de la commission
     * @param string $org:   organisme
     * @param string $token: texte a rechercher
     * @param bool   $seance
     *
     * @return array
     */
    public function searchAgentsByOrganisme($id, $org, $token, $seance = false)
    {
        if (!$seance) {
            $query_seance = 'SELECT t_CAO_Commission_Agent.id_agent FROM t_CAO_Commission_Agent WHERE t_CAO_Commission_Agent.id_commission = "$id"';
        } else {
            $query_seance = 'SELECT t_CAO_Seance_Agent.id_agent  FROM t_CAO_Seance_Agent WHERE t_CAO_Seance_Agent.id_seance = "$id"';
        }
        $query = <<<EOL
             (SELECT
                Agent.id,
                Agent.nom,
                Agent.prenom,
                Agent.service_id,
                Agent.elu as type_compte,
                Agent.email
             FROM Service, Agent
             WHERE Agent.organisme = "$org"
             AND Agent.service_id = Service.id
             AND Service.organisme = "$org"
             AND Service.id != '0'
             AND Agent.deleted_at is null
             AND (
                    Agent.nom LIKE "%$token%"
                    OR Agent.prenom LIKE "%$token%"
                    OR Agent.email LIKE "%$token%"
                    OR Service.sigle LIKE "%$token%"
                    OR Service.libelle LIKE "%$token%"
                 )
                 AND Agent.id NOT IN ($query_seance)
            ORDER BY Agent.nom ASC, Agent.prenom ASC)
            UNION
                (SELECT
                    Agent.id,
                    Agent.nom,
                    Agent.prenom,
                    Agent.service_id,
                    Agent.elu as type_compte,
                    Agent.email
                FROM Agent
                WHERE Agent.organisme = "$org"
                AND Agent.service_id is null
                AND (
                    Agent.nom LIKE "%$token%"
                    OR Agent.prenom LIKE "%$token%"
                    OR Agent.email LIKE "%$token%"
                )
                AND Agent.id NOT IN ($query_seance)
                ORDER BY Agent.nom ASC, Agent.prenom ASC);
EOL;
        $statement = Atexo_Db::getLinkCommon(true)->query($query);
        $arrayAgents = [];
        $i = 0;
        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            $arrayAgents[$i] = $row;
            $arrayAgents[$i]['service_path'] = Atexo_EntityPurchase::getSigleLibelleEntityById($row['service_id'], $org);
            ++$i;
        }

        return $arrayAgents;
    }
}
