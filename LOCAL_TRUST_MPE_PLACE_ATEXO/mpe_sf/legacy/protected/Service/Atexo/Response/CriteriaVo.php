<?php

namespace Application\Service\Atexo\Response;

/*
 * permet d'instancier un objet Atexo_Response_CriteriaVo
 * @author ASO <ayoub.souidahmed@atexo.com>
 * @version 1.0
 * @since 4.7.0
 * @copyright Atexo 2014
 * @package Atexo_Response
 */
class Atexo_Response_CriteriaVo
{
    protected $reference;
    protected string $idOffre = '';
    protected $typesEnveloppes = null;
    protected string $trier = '';
    protected ?string $sensTri = '';
    protected $organisme;
    protected bool $searchByIdOffre = false;
    protected $limit = null;
    protected $offset = null;
    protected bool $modeUnique = false;
    protected string $objet = '';
    protected bool $admissible = true;
    protected bool $count = false;
    protected $arrayIdsGuests = null;
    protected $sousPli = null; //lot
    protected bool $returnReponses = true;
    protected bool $nomEntrepriseVisible = false;
    protected bool $enveloppeStatutOuverte = false;
    protected $constitutionDossierReponse; //a verifier si # $typesEnveloppes
    protected $id;
    protected bool $returnEnveloppe = true;
    protected $enveloppePostule;
    protected $arrayTri;

    /**
     * Recupere la valeur de l'attribut [reference] .
     *
     * @return la valeur de l'attribut
     *
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     *
     * @version 1.0
     *
     * @since 4.7.0
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Affecte une valleur a l'attribut [reference] .
     *
     * @param string $v new value
     *
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     *
     * @version 1.0
     *
     * @since 4.7.0
     */
    public function setReference($value)
    {
        $this->reference = $value;
    }

    /**
     * Recupere la valeur de l'attribut [idOffre] .
     *
     * @return la valeur de l'attribut
     *
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     *
     * @version 1.0
     *
     * @since 4.7.0
     */
    public function getIdOffre()
    {
        return $this->idOffre;
    }

    /**
     * Affecte une valleur a l'attribut [idOffre] .
     *
     * @param string $v new value
     *
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     *
     * @version 1.0
     *
     * @since 4.7.0
     */
    public function setIdOffre($value)
    {
        $this->idOffre = $value;
    }

    /**
     * Recupere la valeur de l'attribut [typesEnveloppes] .
     *
     * @return la valeur de l'attribut
     *
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     *
     * @version 1.0
     *
     * @since 4.7.0
     */
    public function getTypesEnveloppes()
    {
        return $this->typesEnveloppes;
    }

    /**
     * Affecte une valleur a l'attribut [typesEnveloppes] .
     *
     * @param string $v new value
     *
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     *
     * @version 1.0
     *
     * @since 4.7.0
     */
    public function setTypesEnveloppes($value)
    {
        $this->typesEnveloppes = $value;
    }

    /**
     * Recupere la valeur de l'attribut [trier] .
     *
     * @return la valeur de l'attribut
     *
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     *
     * @version 1.0
     *
     * @since 4.7.0
     */
    public function getTrier()
    {
        return $this->trier;
    }

    /**
     * Affecte une valleur a l'attribut [trier] .
     *
     * @param string $v new value
     *
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     *
     * @version 1.0
     *
     * @since 4.7.0
     */
    public function setTrier($value)
    {
        $this->trier = $value;
    }

    /**
     * Recupere la valeur de l'attribut [sensTri] .
     *
     * @return la valeur de l'attribut
     *
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     *
     * @version 1.0
     *
     * @since 4.7.0
     */
    public function getSensTri()
    {
        return $this->sensTri;
    }

    /**
     * Affecte une valleur a l'attribut [sensTri] .
     *
     * @param string $v new value
     *
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     *
     * @version 1.0
     *
     * @since 4.7.0
     */
    public function setSensTri($value)
    {
        $this->sensTri = $value;
    }

    /**
     * Recupere la valeur de l'attribut [organisme] .
     *
     * @return la valeur de l'attribut
     *
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     *
     * @version 1.0
     *
     * @since 4.7.0
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    /**
     * Affecte une valleur a l'attribut [organisme] .
     *
     * @param string $v new value
     *
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     *
     * @version 1.0
     *
     * @since 4.7.0
     */
    public function setOrganisme($value)
    {
        $this->organisme = $value;
    }

    /**
     * Recupere la valeur de l'attribut [searchByIdOffre] .
     *
     * @return la valeur de l'attribut
     *
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     *
     * @version 1.0
     *
     * @since 4.7.0
     */
    public function getSearchByIdOffre()
    {
        return $this->searchByIdOffre;
    }

    /**
     * Affecte une valleur a l'attribut [searchByIdOffre] .
     *
     * @param string $v new value
     *
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     *
     * @version 1.0
     *
     * @since 4.7.0
     */
    public function setSearchByIdOffre($value)
    {
        $this->searchByIdOffre = $value;
    }

    /**
     * Recupere la valeur de l'attribut [limit] .
     *
     * @return la valeur de l'attribut
     *
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     *
     * @version 1.0
     *
     * @since 4.7.0
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * Affecte une valleur a l'attribut [limit] .
     *
     * @param string $v new value
     *
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     *
     * @version 1.0
     *
     * @since 4.7.0
     */
    public function setLimit($value)
    {
        $this->limit = $value;
    }

    /**
     * Recupere la valeur de l'attribut [offset] .
     *
     * @return la valeur de l'attribut
     *
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     *
     * @version 1.0
     *
     * @since 4.7.0
     */
    public function getOffset()
    {
        return $this->offset;
    }

    /**
     * Affecte une valleur a l'attribut [offset] .
     *
     * @param string $v new value
     *
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     *
     * @version 1.0
     *
     * @since 4.7.0
     */
    public function setOffset($value)
    {
        $this->offset = $value;
    }

    /**
     * Recupere la valeur de l'attribut [modeUnique] .
     *
     * @return la valeur de l'attribut
     *
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     *
     * @version 1.0
     *
     * @since 4.7.0
     */
    public function getModeUnique()
    {
        return $this->modeUnique;
    }

    /**
     * Affecte une valleur a l'attribut [modeUnique] .
     *
     * @param string $v new value
     *
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     *
     * @version 1.0
     *
     * @since 4.7.0
     */
    public function setModeUnique($value)
    {
        $this->modeUnique = $value;
    }

    /**
     * Recupere la valeur de l'attribut [objet] .
     *
     * @return la valeur de l'attribut
     *
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     *
     * @version 1.0
     *
     * @since 4.7.0
     */
    public function getObjet()
    {
        return $this->objet;
    }

    /**
     * Affecte une valleur a l'attribut [objet] .
     *
     * @param string $v new value
     *
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     *
     * @version 1.0
     *
     * @since 4.7.0
     */
    public function setObjet($value)
    {
        $this->objet = $value;
    }

    /**
     * Recupere la valeur de l'attribut [admissible] .
     *
     * @return la valeur de l'attribut
     *
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     *
     * @version 1.0
     *
     * @since 4.7.0
     */
    public function getAdmissible()
    {
        return $this->admissible;
    }

    /**
     * Affecte une valleur a l'attribut [admissible] .
     *
     * @param string $v new value
     *
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     *
     * @version 1.0
     *
     * @since 4.7.0
     */
    public function setAdmissible($value)
    {
        $this->admissible = $value;
    }

    /**
     * Recupere la valeur de l'attribut [count] .
     *
     * @return la valeur de l'attribut
     *
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     *
     * @version 1.0
     *
     * @since 4.7.0
     */
    public function getCount()
    {
        return $this->count;
    }

    /**
     * Affecte une valleur a l'attribut [count] .
     *
     * @param string $v new value
     *
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     *
     * @version 1.0
     *
     * @since 4.7.0
     */
    public function setCount($value)
    {
        $this->count = $value;
    }

    /**
     * Recupere la valeur de l'attribut [arrayIdsGuests] .
     *
     * @return la valeur de l'attribut
     *
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     *
     * @version 1.0
     *
     * @since 4.7.0
     */
    public function getArrayIdsGuests()
    {
        return $this->arrayIdsGuests;
    }

    /**
     * Affecte une valleur a l'attribut [arrayIdsGuests] .
     *
     * @param string $v new value
     *
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     *
     * @version 1.0
     *
     * @since 4.7.0
     */
    public function setArrayIdsGuests($value)
    {
        $this->arrayIdsGuests = $value;
    }

    /**
     * Recupere la valeur de l'attribut [sousPli] .
     *
     * @return la valeur de l'attribut
     *
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     *
     * @version 1.0
     *
     * @since 4.7.0
     */
    public function getSousPli()
    {
        return $this->sousPli;
    }

    /**
     * Affecte une valleur a l'attribut [sousPli] .
     *
     * @param string $v new value
     *
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     *
     * @version 1.0
     *
     * @since 4.7.0
     */
    public function setSousPli($value)
    {
        $this->sousPli = $value;
    }

    /**
     * Recupere la valeur de l'attribut [returnReponses] .
     *
     * @return la valeur de l'attribut
     *
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     *
     * @version 1.0
     *
     * @since 4.7.0
     */
    public function getReturnReponses()
    {
        return $this->returnReponses;
    }

    /**
     * Affecte une valleur a l'attribut [returnReponses] .
     *
     * @param string $v new value
     *
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     *
     * @version 1.0
     *
     * @since 4.7.0
     */
    public function setReturnReponses($value)
    {
        $this->returnReponses = $value;
    }

    /**
     * Recupere la valeur de l'attribut [nomEntrepriseVisible] .
     *
     * @return la valeur de l'attribut
     *
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     *
     * @version 1.0
     *
     * @since 4.7.0
     */
    public function getNomEntrepriseVisible()
    {
        return $this->nomEntrepriseVisible;
    }

    /**
     * Affecte une valleur a l'attribut [nomEntrepriseVisible] .
     *
     * @param string $v new value
     *
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     *
     * @version 1.0
     *
     * @since 4.7.0
     */
    public function setNomEntrepriseVisible($value)
    {
        $this->nomEntrepriseVisible = $value;
    }

    /**
     * Recupere la valeur de l'attribut [enveloppeStatutOuverte] .
     *
     * @return la valeur de l'attribut
     *
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     *
     * @version 1.0
     *
     * @since 4.7.0
     */
    public function getEnveloppeStatutOuverte()
    {
        return $this->enveloppeStatutOuverte;
    }

    /**
     * Affecte une valleur a l'attribut [enveloppeStatutOuverte] .
     *
     * @param string $v new value
     *
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     *
     * @version 1.0
     *
     * @since 4.7.0
     */
    public function setEnveloppeStatutOuverte($value)
    {
        $this->enveloppeStatutOuverte = $value;
    }

    /**
     * Recupere la valeur de l'attribut [constitutionDossierReponse] .
     *
     * @return la valeur de l'attribut
     *
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     *
     * @version 1.0
     *
     * @since 4.7.0
     */
    public function getConstitutionDossierReponse()
    {
        return $this->constitutionDossierReponse;
    }

    /**
     * Affecte une valleur a l'attribut [constitutionDossierReponse] .
     *
     * @param string $v new value
     *
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     *
     * @version 1.0
     *
     * @since 4.7.0
     */
    public function setConstitutionDossierReponse($value)
    {
        $this->constitutionDossierReponse = $value;
    }

    /**
     * Recupere la valeur de l'attribut [id] .
     *
     * @return la valeur de l'attribut
     *
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     *
     * @version 1.0
     *
     * @since 4.7.0
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Affecte une valleur a l'attribut [id] .
     *
     * @param string $v new value
     *
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     *
     * @version 1.0
     *
     * @since 4.7.0
     */
    public function setId($value)
    {
        $this->id = $value;
    }

    /**
     * Recupere la valeur de l'attribut [returnEnveloppe] .
     *
     * @return la valeur de l'attribut
     *
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     *
     * @version 1.0
     *
     * @since 4.7.0
     */
    public function getReturnEnveloppe()
    {
        return $this->returnEnveloppe;
    }

    /**
     * Affecte une valleur a l'attribut [returnEnveloppe] .
     *
     * @param string $v new value
     *
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     *
     * @version 1.0
     *
     * @since 4.7.0
     */
    public function setReturnEnveloppe($value)
    {
        $this->returnEnveloppe = $value;
    }

    /**
     * Recupere la valeur de l'attribut [enveloppePostule] .
     *
     * @return la valeur de l'attribut
     *
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     *
     * @version 1.0
     *
     * @since 4.7.0
     */
    public function getEnveloppePostule()
    {
        return $this->enveloppePostule;
    }

    /**
     * Affecte une valleur a l'attribut [enveloppePostule] .
     *
     * @param string $v new value
     *
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     *
     * @version 1.0
     *
     * @since 4.7.0
     */
    public function setEnveloppePostule($value)
    {
        $this->enveloppePostule = $value;
    }

    /**
     * Recupere la valeur de l'attribut [arrayTri] .
     *
     * @return la valeur de l'attribut
     *
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     *
     * @version 1.0
     *
     * @since 4.7.0
     */
    public function getArrayTri()
    {
        return $this->arrayTri;
    }

    /**
     * Affecte une valleur a l'attribut [arrayTri] .
     *
     * @param string $v new value
     *
     * @author ASO <ayoub.souidahmed@atexo.com>
     * @copyright Atexo 2014
     *
     * @version 1.0
     *
     * @since 4.7.0
     */
    public function setArrayTri($value)
    {
        $this->arrayTri = $value;
    }
}
