<?php

namespace Application\Service\Atexo\Chorus;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonChorusActeJuridique;
use Application\Propel\Mpe\CommonChorusActeJuridiquePeer;
use Application\Propel\Mpe\CommonChorusCodeCalculInterets;
use Application\Propel\Mpe\CommonChorusCodeCalculInteretsPeer;
use Application\Propel\Mpe\CommonChorusConditionsPaiements;
use Application\Propel\Mpe\CommonChorusConditionsPaiementsPeer;
use Application\Propel\Mpe\CommonChorusEchange;
use Application\Propel\Mpe\CommonChorusEchangePeer;
use Application\Propel\Mpe\CommonChorusEchangeQuery;
use Application\Propel\Mpe\CommonChorusFicheNavette;
use Application\Propel\Mpe\CommonChorusFicheNavettePeer;
use Application\Propel\Mpe\CommonChorusFormePrix;
use Application\Propel\Mpe\CommonChorusFormePrixPeer;
use Application\Propel\Mpe\CommonChorusGroupementAchat;
use Application\Propel\Mpe\CommonChorusGroupementAchatPeer;
use Application\Propel\Mpe\CommonChorusGroupementAchatQuery;
use Application\Propel\Mpe\CommonChorusNomsFichiers;
use Application\Propel\Mpe\CommonChorusNomsFichiersPeer;
use Application\Propel\Mpe\CommonChorusNomsFichiersQuery;
use Application\Propel\Mpe\CommonChorusNumeroSequence;
use Application\Propel\Mpe\CommonChorusNumeroSequencePeer;
use Application\Propel\Mpe\CommonChorusNumeroSequenceQuery;
use Application\Propel\Mpe\CommonChorusNumerosMarchesPeer;
use Application\Propel\Mpe\CommonChorusNumerosMarchesQuery;
use Application\Propel\Mpe\CommonChorusOrganisationAchat;
use Application\Propel\Mpe\CommonChorusOrganisationAchatPeer;
use Application\Propel\Mpe\CommonChorusOrganisationAchatQuery;
use Application\Propel\Mpe\CommonChorusPj;
use Application\Propel\Mpe\CommonChorusPjPeer;
use Application\Propel\Mpe\CommonChorusPjQuery;
use Application\Propel\Mpe\CommonChorusRegroupementComptable;
use Application\Propel\Mpe\CommonChorusRegroupementComptablePeer;
use Application\Propel\Mpe\CommonChorusTypeGroupement;
use Application\Propel\Mpe\CommonChorusTypeGroupementPeer;
use Application\Propel\Mpe\CommonChorusTypeMarche;
use Application\Propel\Mpe\CommonChorusTypeMarchePeer;
use Application\Propel\Mpe\CommonChorusTypeProcedure;
use Application\Propel\Mpe\CommonChorusTypeProcedurePeer;
use Application\Propel\Mpe\CommonDocumentExternePeer;
use Application\Propel\Mpe\CommonEchangePeer;
use Application\Propel\Mpe\CommonEnveloppe;
use Application\Propel\Mpe\CommonEnveloppePeer;
use Application\Propel\Mpe\CommonOffres;
use Application\Propel\Mpe\CommonTChorusFicheModificative;
use Application\Propel\Mpe\CommonTChorusFicheModificativePeer;
use Application\Propel\Mpe\CommonTChorusFicheModificativeQuery;
use Application\Propel\Mpe\CommonTChorusTierRejete;
use Application\Propel\Mpe\CommonTChorusTierRejetePeer;
use Application\Propel\Mpe\CommonTChorusTierRejeteQuery;
use Application\Propel\Mpe\CommonTContratTitulaire;
use Application\Propel\Mpe\CommonValeurReferentiel;
use Application\Propel\Mpe\CommonValeurReferentielQuery;
use Application\Propel\Mpe\Om\BaseCommonChorusRegroupementComptablePeer;
use Application\Propel\Mpe\Om\BaseCommonChorusTypeGroupementPeer;
use Application\Service\Atexo\Agent\Atexo_Agent_Alertes;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_SendFiles;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Responses;
use Exception;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * commentaires.
 *
 * @author mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Chorus_Echange
{
    public function retreiveEchangesByIdDecision($organisme, $idDecision, $inOrdre = false)
    {
        if (!$organisme) {
            $organisme = Atexo_CurrentUser::getOrganismAcronym();
        }
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonChorusEchangePeer::ID_DECISION, $idDecision);
        $c->add(CommonChorusEchangePeer::ORGANISME, $organisme);
        if ($inOrdre) {
            $c->addDescendingOrderByColumn(CommonChorusEchangePeer::ID);
        }
        $echanges = CommonChorusEchangePeer::doSelect($c, $connexion);
        if ($echanges) {
            return $echanges;
        }

        return [];
    }

    public function retreiveLastEchangeCreated($organisme, $idDecision)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonChorusEchangePeer::ID_DECISION, $idDecision);
        $c->add(CommonChorusEchangePeer::ORGANISME, $organisme);
        $c->addDescendingOrderByColumn(CommonChorusEchangePeer::ID);
        $echange = CommonChorusEchangePeer::doSelectOne($c, $connexion);
        if ($echange) {
            return $echange;
        }

        return false;
    }

    public function retreiveLastEchangeCreatedWithUuidExec(?string $organisme, string $uuidExec)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonChorusEchangePeer::UUID_EXTERNE_EXEC, $uuidExec);
        $c->add(CommonChorusEchangePeer::ORGANISME, $organisme);
        $c->addDescendingOrderByColumn(CommonChorusEchangePeer::ID);
        $echange = CommonChorusEchangePeer::doSelectOne($c, $connexion);
        if ($echange) {
            return $echange;
        }

        return false;
    }

    public function retrievePenultimateEchangeCreated($organisme, $uuidExec)
    {
        $lastEchange = (new Atexo_Chorus_Echange())->retreiveLastEchangeCreatedWithUuidExec($organisme, $uuidExec);
        if ($lastEchange) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            $c = new Criteria();
            $c->add(CommonChorusEchangePeer::UUID_EXTERNE_EXEC, $uuidExec);
            $c->add(CommonChorusEchangePeer::ORGANISME, $organisme);
            $c->add(CommonChorusEchangePeer::NUM_ORDRE, intval($lastEchange->getNumOrdre()) - 1);
            $echange = CommonChorusEchangePeer::doSelectOne($c, $connexion);
            if ($echange) {
                return $echange;
            }

            return false;
        }

        return false;
    }

    public function retreiveEchangeByIdJoinDecisionenveloppe($idEchange, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonChorusEchangePeer::ID, $idEchange);
        $c->add(CommonChorusEchangePeer::ORGANISME, $organisme);
        $echange = CommonChorusEchangePeer::doSelect($c, $connexion);
        if ($echange[0]) {
            return $echange[0];
        }

        return false;
    }

    /**
     * retourne la liste des pièces éxterne d'un échange.
     */
    public function retreivePiecesExternes($idEchange, $organisme)
    {
        $logger = Atexo_LoggerManager::getLogger('app');
        try {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            $c = new Criteria();
            $c->add(CommonChorusPjPeer::ID_ECHANGE, $idEchange);
            $c->add(CommonChorusPjPeer::ORGANISME, $organisme);
            $echange = CommonChorusPjPeer::doSelect($c, $connexion);
            if ($echange) {
                return $echange;
            }

            return [];
        } catch (Exception $e) {
            $logger->error("Erreur lors de la recuperation des pieces externes : idEchange = $idEchange , organisme = $organisme".PHP_EOL.'Erreur: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString());
        }
    }

    public function retreiveNatureActeJuridique()
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $acte = CommonChorusActeJuridiquePeer::doSelect($c, $connexion);
        if ($acte) {
            return $acte;
        }

        return [];
    }

    public function retreiveFormePrix()
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $formePrix = CommonChorusFormePrixPeer::doSelect($c, $connexion);
        if ($formePrix) {
            return $formePrix;
        }

        return [];
    }

    /**
    * Permet de recuperer les organisation d'achat
    *
    * @param string $organisme l'organisme
    * @param string $fluxFen111
    * @param string $fluxFen211
    * @param int $actif
    * @return array CommonChorusOrganisationAchat
    * @author loubna EZZIANI <loubna.ezziani@atexo.com>
    * @version 1.0
    * @since 2015-roadmap
    * @copyright Atexo 2016
    */
    public static function retreiveOrganisationAchat($organisme, $fluxFen111 = null, $fluxFen211 = null, $actif = 1)
    {
        $organismastionQuery = new CommonChorusOrganisationAchatQuery();

        return $organismastionQuery->getOrganisationAchat($organisme, $fluxFen111, $fluxFen211, $actif);
    }

    public static function retreiveGroupementAchatByIdOA(
        ?string $idOA,
        ?string $organisme,
        string|int|null $idService = 0,
        ?int $actif = 1
    ): array {
        $connexionOrg = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));

        $c = new Criteria();
        if($actif !== null) {
            $c->add(CommonChorusGroupementAchatPeer::ACTIF, $actif);
        }
        $c->add(CommonChorusGroupementAchatPeer::ID_OA, $idOA);
        $c->add(CommonChorusGroupementAchatPeer::ORGANISME, $organisme);
        if (is_null($idService) || $idService === 0) {
            $c->add(CommonChorusGroupementAchatPeer::SERVICE_ID, null, Criteria::ISNULL);
        } else {
            $c->add(CommonChorusGroupementAchatPeer::SERVICE_ID, $idService);
        }
        $oa = CommonChorusGroupementAchatPeer::doSelect($c, $connexionOrg);
        if($oa) {
            return $oa;
        }

        return [];
    }

    public function retreiveListeTypeMarche()
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $typeMarche = CommonChorusTypeMarchePeer::doSelect($c, $connexion);
        if($typeMarche) {
            return $typeMarche;
        }
        return array();
    }

    public function retreiveEchangeById($organisme, $idEchange)
    {
        $connexionOrg = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonChorusEchangePeer::ID, $idEchange);
        $c->add(CommonChorusEchangePeer::ORGANISME, $organisme);
        $echange = CommonChorusEchangePeer::doSelectOne($c, $connexionOrg);
        if($echange) {
            return $echange;
        }
        return false;
    }

    public function retreiveSelectedDocumentExterne($organisme, $idEchange)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $echange = self::retreiveEchangeById($organisme, $idEchange);
        $data = array();
        $idDocument = array();
        if($echange) {
            $idsDoc = $echange->getIdsPiecesExternes();
            if($idsDoc != '') {
                $arIds = explode(';', $idsDoc);
                foreach($arIds as $oneId) {
                    $idDoc = explode('_', $oneId);
                    $idDocument[] = $idDoc[0];
                }
                foreach(array_unique($idDocument) as $oneId) {
                    $doc = CommonDocumentExternePeer::retrieveByPK($oneId, $organisme, $connexion);
                    if ($doc) {
                        $data[] = $doc;
                    }
                }
            }
        }

        return $data;
    }

    /**
     * Permet de mettre a jour le statut de l'echange Chorus.
     *
     * @param string $idEchange
     * @param string $statutEchange
     * @param string $organisme
     */
    public function updateStatutEnvoi($idEchange, $statutEchange, $organisme)
    {
        try {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $echange = CommonChorusEchangePeer::retrieveByPK($idEchange, $organisme, $connexion);
            $echange->setStatutechange($statutEchange);
            $echange->save($connexion);
            (new Atexo_Chorus_Util())->loggerInfos("Statut Echange mis a jour avec SUCCES: Echange [Id = $idEchange , Organisme = $organisme , Statut = $statutEchange]");
        } catch (\Exception $e) {
            (new Atexo_Chorus_Util())->loggerErreur("Erreur lors de la mise a jour du statut de l'Echange Chorus [Id_echange = $idEchange , Organisme = $organisme , Statut = $statutEchange] , \n Methode = ChorusDetailEchanges::updateStatutEnvoi");
            Prado::log("Impossible d'envoyer le flux vers CHORUS-".$e->__toString().'-', TLogger::ERROR, 'ERROR');
        }
    }

    public function retrieveOrganisationAchatById($idOrganisationAchat, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $organisationAchat = CommonChorusOrganisationAchatPeer::retrieveByPK($idOrganisationAchat, $organisme, $connexion);

        return $organisationAchat;
    }

    public function retrieveGroupementAchatById($idGroupementAchat, $organisme, $idService = null)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $groupementAchatQuery = new CommonChorusGroupementAchatQuery();

        return $groupementAchatQuery->getGroupementAchatById($idGroupementAchat, $organisme, $idService, $connexion);
    }

    public function retrieveGroupementAchatByCode($codeGa, $organisme, $idOa, $idService = 0)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $groupementAchatQuery = new CommonChorusGroupementAchatQuery();

        return $groupementAchatQuery->getGroupementAchatByCode($codeGa, $organisme, $idOa, $idService, $connexion);
    }

    public function retrieveFormePrixById($idFormePrix)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $c = new Criteria();
        $c->add(CommonChorusFormePrixPeer::ID, $idFormePrix);
        $formePrix = CommonChorusFormePrixPeer::doSelectOne($c, $connexion);

        return $formePrix;
    }

    public function retreiveTypeMarche($idTypeMarche)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonChorusTypeMarchePeer::ID, $idTypeMarche);
        $typeMarche = CommonChorusTypeMarchePeer::doSelectOne($c, $connexion);
        if ($typeMarche instanceof CommonChorusTypeMarche) {
            return $typeMarche;
        } else {
            return false;
        }
    }

    public function retreiveTypeProcedure()
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $typeProc = CommonChorusTypeProcedurePeer::doSelect($c, $connexion);
        if ($typeProc) {
            return $typeProc;
        } else {
            return [];
        }
    }

    public function getNbrFileInEchange($idEchange)
    {
        $organisme = Atexo_CurrentUser::getOrganismAcronym();
        $nbrTotal = 0;
        $echange = self::retreiveEchangeById($organisme, $idEchange);
        if ('' != $echange->getDceItems()) {
            $nbrTotal += count(explode(';', $echange->getDceItems()));
        }
        if ('' != $echange->getIdsEnvItems()) {
            $arrayIdsEnvItems = [];
            $explode1 = explode('#', $echange->getIdsEnvItems());
            if (is_array($explode1) && count($explode1)) {
                foreach ($explode1 as $exp1) {
                    if ($exp1 != $explode1[0]) {
                        $explode2 = explode('_', $exp1);
                        if (is_array($explode2) && count($explode2)) {
                            foreach ($explode2 as $exp2) {
                                if ($exp2 != $explode2[0]) {
                                    $explode3 = explode(';', $exp2);
                                    if (is_array($explode3) && count($explode3)) {
                                        foreach ($explode3 as $exp3) {
                                            $arrayIdsEnvItems[] = $exp3;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            $nbrTotal += count($arrayIdsEnvItems);
        }
        if ('' != $echange->getIdsBlobEnv()) {
            $arrayBlobIdsItems = explode(';', $echange->getIdsBlobEnv());
            $nbrTotal += count($arrayBlobIdsItems);
        }
        if ('' != $echange->getIdsEnvAe()) {
            ++$nbrTotal;
        }
        if ('0' != $echange->getSignace()) {
            ++$nbrTotal;
        }
        if ('' != $echange->getIdsPiecesExternes()) {
            $nbrTotal += count(explode(';', $echange->getIdsPiecesExternes())) - 1;
        }
        if ('' != $echange->getPiecesNotifItems()) {
            $nbrTotal += count(explode(';', $echange->getPiecesNotifItems()));
        }
        //Ajout des signatures des fichiers
        if ('' != $echange->getIdsEnvSignItems()) {
            $arrayBlobEnvSignItems = explode('#', $echange->getIdsEnvSignItems());
            $nbrTotal += count($arrayBlobEnvSignItems) - 1;
        }
        if ('' != $echange->getIdsBlobSignEnv()) {
            $arrayBlobIdsItems = explode(';', $echange->getIdsBlobSignEnv());
            $nbrTotal += count($arrayBlobIdsItems);
        }
        $pj = self::retreivePiecesExternes($idEchange, $organisme);
        if ($pj) {
            $nbrTotal += is_countable($pj) ? count($pj) : 0;
        }
        $pieces = self::retriveListeRapportSignatureEchange($echange, $organisme, true);
        if (is_array($pieces)) {
            $nbrTotal += count($pieces);
        }
        $ficheModificative = $echange->getCommonTChorusFicheModificatives()[0];
        if ($ficheModificative instanceof CommonTChorusFicheModificative) {
            $pjsFicheModificative = $ficheModificative->getCommonTChorusFicheModificativePjs();
            $nbrTotal += count($pjsFicheModificative);
        }

        //Dume Acheteur
        if ('' != $echange->getDumeAcheteurItems()) {
            $arrayBlobDumeAcheteurItems = explode(';', $echange->getDumeAcheteurItems());
            $nbrTotal += count($arrayBlobDumeAcheteurItems);
        }

        //Dume OE
        if ('' != $echange->getDumeOeItems()) {
            $arrayBlobDumeOeItems = explode(';', $echange->getDumeOeItems());
            $nbrTotal += count($arrayBlobDumeOeItems);
        }

        return $nbrTotal;
    }

    public function retreiveTypeProcedureById($idTypeProcedure)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonChorusTypeProcedurePeer::ID, $idTypeProcedure);
        $typeProc = CommonChorusTypeProcedurePeer::doSelectOne($c, $connexion);

        return $typeProc;
    }

    public function retreiveActeJuridique($idActe)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonChorusActeJuridiquePeer::ID, $idActe);
        $acteJuridique = CommonChorusActeJuridiquePeer::doSelectOne($c, $connexion);
        if ($acteJuridique instanceof CommonChorusActeJuridique) {
            return $acteJuridique;
        } else {
            return false;
        }
    }

    public function retreiveEchangesByIdDecisionJoinDecisionenveloppe($idDecision, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonChorusEchangePeer::ID_DECISION, $idDecision);
        $c->add(CommonChorusEchangePeer::ORGANISME, $organisme);
        $c->addDescendingOrderByColumn(CommonChorusEchangePeer::ID);
        $echanges = (new CommonChorusEchangePeer())->doSelectJoinDecisionenveloppe($c, $connexion);
        if ($echanges) {
            return $echanges;
        } else {
            return false;
        }
    }

    public function retreiveCommonChorusNumerosMarches($numsMarche)
    {
        try {
            $commonConnexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $c = new Criteria();
            $c->add(CommonChorusNumerosMarchesPeer::NUMERO_MARCHE, '%'.$numsMarche.'%', Criteria::LIKE);
            $numeros = CommonChorusNumerosMarchesPeer::doSelectOne($c, $commonConnexion);
            if ($numeros) {
                return $numeros;
            } else {
                return false;
            }
        } catch (Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('chorus');
            $logger->error("Erreur : numsMarche = $numsMarche \n\nErreur : ".$e->getMessage());
        }
    }

    public function retreiveEchangesByRefConsultation($consultationId, $org)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonEchangePeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonEchangePeer::ORGANISME, $org);
        $echanges = CommonEchangePeer::doSelect($c, $connexion);
        if ($echanges) {
            return $echanges;
        } else {
            return [];
        }
    }

    /**
     * Permet de recuperer l'objet chorus_fiche_navette par l'identifiant echange chorus et l'organisme.
     *
     * @param $idChorusEchange: identifiant echange chorus
     * @param $organisme: organisme
     */
    public function retrieveFicheNavetteByIdChorusEchangeAndOrg($idChorusEchange, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonChorusFicheNavettePeer::ID_CHORUS_ECHANGE, $idChorusEchange);
        $c->add(CommonChorusFicheNavettePeer::ORGANISME, $organisme);
        $ficheNavette = CommonChorusFicheNavettePeer::doSelectOne($c, $connexion);
        if ($ficheNavette instanceof CommonChorusFicheNavette) {
            return $ficheNavette;
        }

        return false;
    }

    /**
     * Permet de recuperer les regroupements comptables.
     *
     * @param $id
     * @param $organisme
     */
    public function retrieveRegroupementCompatable($id = null, $organisme = null)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        if (!$organisme) {
            $organisme = Atexo_CurrentUser::getCurrentOrganism();
        }
        $c = new Criteria();
        if ($id) {
            $c->add(BaseCommonChorusRegroupementComptablePeer::ID, $id);
        }
        $c->add(BaseCommonChorusRegroupementComptablePeer::ORGANISME, $organisme);
        $c->addAscendingOrderByColumn(BaseCommonChorusRegroupementComptablePeer::LIBELLE);

        $regroupementComptable = BaseCommonChorusRegroupementComptablePeer::doSelect($c, $connexion);
        if (is_array($regroupementComptable) && count($regroupementComptable)) {
            return $regroupementComptable;
        }

        return false;
    }

    /**
     * Permet de recuperer les types de groupement.
     *
     * @param $id
     * @param $organisme
     */
    public function retrieveTypeGroupement($id = null, $organisme = null)
    {
        $logger = Atexo_LoggerManager::getLogger('app');
        try {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            if (!$organisme) {
                $organisme = Atexo_CurrentUser::getCurrentOrganism();
            }
            $c = new Criteria();
            if ($id) {
                $c->add(BaseCommonChorusTypeGroupementPeer::ID, $id);
            }
            $c->add(BaseCommonChorusTypeGroupementPeer::ORGANISME, $organisme);
            $typeGroupement = BaseCommonChorusTypeGroupementPeer::doSelect($c, $connexion);
            if (is_array($typeGroupement) && count($typeGroupement)) {
                return $typeGroupement;
            }

            return false;
        } catch (Exception $e) {
            $logger->error('Erreur lors de la recuperation du type de groupement.'.PHP_EOL.'Erreur: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString());
        }
    }

    /**
     * Permet de construire le nom de la fiche de navette.
     *
     * @param $referenceUtilisateur
     */
    public function getNomFicheNavette($referenceUtilisateur)
    {
        return Prado::localize('FICHE_NAVETTE').'-'.date('YmdHi').'-'.Prado::localize('TEXT_MARCHE').$referenceUtilisateur;
    }

    /*
     * retourne les rapports de signature
     * param $idsRapportSignature suite de sous format (idEnveloppe-checked-tailleFichier)
     */
    public function retreivePiecesOuvertureAnlyse($idsRapportSignature, $acronymeOrganisme)
    {
        $i = 0;
        $pieces = '';
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $dataRapportSignature = explode('#', $idsRapportSignature);
        if (is_array($dataRapportSignature)) {
            foreach ($dataRapportSignature as $rapportSignature) {
                $res = explode('-', $rapportSignature);
                if (is_array($res) && count($res)) {
                    $idEnv = $res[0];
                    $enveloppe = CommonEnveloppePeer::retrieveByPK($idEnv, $acronymeOrganisme, $connexion);
                    if ($enveloppe instanceof CommonEnveloppe) {
                        $offre = $enveloppe->getOffre($connexion);
                        if ($offre instanceof CommonOffres) {
                            $consultation = $offre->getCommonConsultation($connexion);
                            $typeSignature = $offre->getTypeSignature();
                            if ('1' == $res[1]) {
                                $pieces[$i]['nomFichier'] = $enveloppe->getNomRapportSignature();
                                $path = (new Atexo_Consultation_Responses())->getRapportSignatureEnveloppe($enveloppe, $consultation, $typeSignature, false, 'agent');
                                $pieces[$i]['pathFile'] = $path;
                                ++$i;
                            }
                        }
                    }
                }
            }
        }

        return $pieces;
    }

    /*
     * Permet d'avoir liste des rappor signature d'un echange
     */
    public function retriveListeRapportSignatureEchange($echange, $organisme, $checked = null)
    {
        $logger = Atexo_LoggerManager::getLogger('app');
        if ($echange instanceof CommonChorusEchange) {
            try {
                $i = 0;
                $pieces = [];
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
                $dataRapportSignature = explode('#', $echange->getIdsRapportSignature());
                if (is_array($dataRapportSignature)) {
                    foreach ($dataRapportSignature as $rapportSignature) {
                        $res = explode('-', $rapportSignature);
                        if (is_array($res) && count($res)) {
                            $idEnv = $res[0];
                            $enveloppe = CommonEnveloppePeer::retrieveByPK($idEnv, $organisme, $connexion);
                            if ($enveloppe instanceof CommonEnveloppe) {
                                if (!$checked) {
                                    $pieces[$i]['id'] = $idEnv;
                                    $pieces[$i]['nomFichier'] = $enveloppe->getNomRapportSignature();
                                    $pieces[$i]['checked'] = $res[1];
                                    $pieces[$i]['sizeFile'] = $res[2];
                                    ++$i;
                                } elseif ($checked && '1' == $res[1]) {
                                    $pieces[$i]['id'] = $idEnv;
                                    $pieces[$i]['nomFichier'] = $enveloppe->getNomRapportSignature();
                                    $pieces[$i]['checked'] = $res[1];
                                    $pieces[$i]['sizeFile'] = $res[2];
                                    ++$i;
                                }
                            }
                        }
                    }
                }

                return $pieces;
            } catch (\Exception $e) {
                $logger->error("Erreur lors de la recuperation du rapport de signature d'un echange CHORUS : idEchange = ".$echange->getId()."organisme = $organisme".PHP_EOL.'Erreur: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString());
            }
        } else {
            $logger->error("L'echange chorus n'est pas une instance de CommonChorusEchange : ".print_r($echange, true));
        }
    }

    /*
     * Permet d'avoir le numero de sequence par type fichier
     */
    public function getNumeroSequenceByType($type, &$connexion = null, &$erreursEnvoiFlux = null)
    {
        (new Atexo_Chorus_Util())->loggerInfosEnvoiHttpFlux("Debut construction du numero de sequence du flux : type de flux = $type");
        if (!$connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        }
        try {
            $c = new Criteria();
            $c->add(CommonChorusNumeroSequencePeer::TYPE_FICHIER, $type);
            $numSequance = CommonChorusNumeroSequencePeer::doSelectOne($c, $connexion);
            if ($numSequance instanceof CommonChorusNumeroSequence) {
                $numSeq = $numSequance->getNumero() + 1;
            } else {
                $numSequance = new CommonChorusNumeroSequence();
                $numSequance->setTypeFichier($type);
                $numSeq = 1;
            }
            $newNumSeq = Atexo_Util::validateNumeroSequance($numSeq, $erreursEnvoiFlux);
            if (!empty($newNumSeq)) {
                $numSequance->setNumero($newNumSeq);
                $numSequance->save($connexion);

                //Permet de supprimer l'objet $numSequance du cache de propel
                CommonChorusNumeroSequencePeer::removeInstanceFromPool($numSequance);

                (new Atexo_Chorus_Util())->loggerInfosEnvoiHttpFlux("Numero de sequence genere : $newNumSeq");
            }
            return $newNumSeq;
        } catch (\Exception $e) {
            $erreur = "Erreur lors de la generation du numero de sequence du flux CHORUS : {type de flux = $type}".PHP_EOL.'Erreur: '.$e->getMessage().'Trace: '.$e->getTraceAsString().PHP_EOL.'Methode = Atexo_Chorus_Echange::getNumeroSequenceByType';
            $erreursEnvoiFlux[] = $erreur;
            (new Atexo_Chorus_Util())->loggerErreursEnvoiHttpFlux($erreur);
        }
        (new Atexo_Chorus_Util())->loggerInfosEnvoiHttpFlux("Fin construction du numero de sequence du flux : type de flux = $type");
    }

    /*
     * Permet d'envoyer un flux d'echange à chorus
     */
    public function sendFluxToCHorus($nameFile, $flux, &$erreursEnvoiFlux = null)
    {
        (new Atexo_Chorus_Util())->loggerInfosEnvoiHttpFlux("Debut transmission flux vers CHORUS. Fichier = '".$nameFile."'");
        try {
            $dirName = Atexo_Config::getParameter('DIR_FILE_CHORUS_ENVOI');
            if (!is_dir($dirName)) {
                Atexo_Util::createDirectory($dirName);
            }
            Atexo_Util::write_file($dirName.$nameFile, $flux);
            (new Atexo_Chorus_Util())->loggerInfosEnvoiHttpFlux('Fichier stocke sur le serveur : '.$dirName.$nameFile);

            $logger = Atexo_LoggerManager::getLogger('chorus');
            $result = Atexo_SendFiles::sendFile(
                $dirName.$nameFile,
                Atexo_Config::getParameter('URL_ACCES_CHORUS'),
                true,
                Atexo_Config::getParameter('CHORUS_SSLCERT'),
                Atexo_Config::getParameter('CHORUS_SSLCERTPASSWD'),
                $logger,
                $erreursEnvoiFlux
            );
            (new Atexo_Chorus_Util())->loggerInfosEnvoiHttpFlux('Resultat transmission : '.((false === $result) ? 'KO' : 'OK'));

            return $result;
        } catch (\Exception $e) {
            $erreur = "Impossible d'envoyer le flux vers CHORUS : ".PHP_EOL."Nom_flux = '$nameFile'".PHP_EOL."Flux: $flux ".PHP_EOL.'Erreur: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString().PHP_EOL.'Methode = Atexo_Chorus_Echange::sendFluxToCHorus';
            $erreursEnvoiFlux[] = $erreur;
            (new Atexo_Chorus_Util())->loggerErreursEnvoiHttpFlux($erreur);

            return false;
        }
        (new Atexo_Chorus_Util())->loggerInfosEnvoiHttpFlux("Fin transmission flux vers CHORUS. Fichier = '".$nameFile."'");
    }

    /*
     * Permet d'enregistrer un echange dans le cas FIR et FEN
     */
    public function saveEchange(&$echange, $nameFile, $statutEchange, $retourEchange, $typeFichier, $idAgent = null, $firstName = null, $lastName = null, $idEjAppliExtValue = null, &$connexion = null, &$erreursEnvoiFlux = null)
    {
        try {
            if ($echange instanceof CommonChorusEchange) {
                (new Atexo_Chorus_Util())->loggerInfosEnvoiHttpFlux("Debut sauvegarde de l'echange CHORUS apres envoi du flux a CHORUS : id_echange = ".$echange->getId().' , Org = '.$echange->getOrganisme());
                if (!$connexion) {
                    $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                }

                if ($idEjAppliExtValue) {
                    $echange->setIdEjAppliExt($idEjAppliExtValue);
                }
                $echange->setNomFichier($nameFile);
                $echange->setStatutechange($statutEchange);
                $echange->setDateEnvoi(date('Y-m-d H:i:s'));
                $echange->setRetourChorus($retourEchange);
                if ($idAgent) {
                    $echange->setIdAgentEnvoi($idAgent);
                }
                if ($firstName) {
                    $echange->setNomAgent($firstName);
                }
                if ($lastName) {
                    $echange->setPrenomAgent($lastName);
                }
                $echange->save($connexion);

                $chorusNomsFichies = new CommonChorusNomsFichiers();
                $chorusNomsFichies->setIdEchange($echange->getId());
                $chorusNomsFichies->setNomFichier($nameFile);
                $chorusNomsFichies->setAcronymeOrganisme($echange->getOrganisme());
                $chorusNomsFichies->setTypeFichier($typeFichier);
                $chorusNomsFichies->setDateAjout(date('Y-m-d H:i:s'));
                $chorusNomsFichies->save($connexion);

                (new Atexo_Chorus_Util())->loggerInfosEnvoiHttpFlux("Nom fichier mis a jour dans Chorus_echange avec *** SUCCES ***. Valeur sauvegardee = '$nameFile'");
                (new Atexo_Chorus_Util())->loggerInfosEnvoiHttpFlux("Statut mis a jour dans Chorus_echange avec *** SUCCES ***. Valeur sauvegardee = '$statutEchange' (libelle statut utilisateur = '".$echange->getLibelleStatutEchange()."'");
                (new Atexo_Chorus_Util())->loggerInfosEnvoiHttpFlux("Retour Chorus mis a jour dans Chorus_echange avec *** SUCCES ***. Valeur sauvegardee = '$retourEchange'");
                (new Atexo_Chorus_Util())->loggerInfosEnvoiHttpFlux("Type fichier mis a jour dans Chorus_Noms_Fichiers avec *** SUCCES ***. Valeur sauvegardee = '$typeFichier' (libelle type fichier = ".$echange->getLibelleTypeFlux().')');
                if (!empty($idAgent)) {
                    (new Atexo_Chorus_Util())->loggerInfosEnvoiHttpFlux("Id de l'Agent mis a jour dans Chorus_echange avec *** SUCCES ***. Valeur sauvegardee = '$idAgent'");
                }
                if (!empty($firstName)) {
                    (new Atexo_Chorus_Util())->loggerInfosEnvoiHttpFlux("Prenom de l'agent mis a jour dans Chorus_echange avec *** SUCCES ***. Valeur sauvegardee = '$firstName'");
                }
                if (!empty($lastName)) {
                    (new Atexo_Chorus_Util())->loggerInfosEnvoiHttpFlux("Nom de l'agent mis a jour dans Chorus_echange avec *** SUCCES ***. Valeur sauvegardee = '$lastName'");
                }

                (new Atexo_Chorus_Util())->loggerInfosEnvoiHttpFlux("Fin sauvegarde de l'echange CHORUS apres envoi du flux a CHORUS : id_echange = ".$echange->getId().' , Org = '.$echange->getOrganisme());
            } else {
                $erreur = "L'echange Chorus n'est pas une instance de CommonChorusEchange : ".print_r($echange, true);
                $erreursEnvoiFlux[] = $erreur;
                (new Atexo_Chorus_Util())->loggerErreursEnvoiHttpFlux($erreur);

                return false;
            }

            return true;
        } catch (Exception $e) {
            $erreur = "Erreur lors de la sauvegarde de l'echange Chorus : [id_echange = ".$echange->getId().', org = '.$echange->getOrganisme().", nomFichier = $nameFile, statut = $statutEchange, retourChorus = $retourEchange, ";
            $erreur .= "typeFichier = $typeFichier, idAgent = $idAgent, agentLastName = $lastName, agentFirstName = $firstName, idUnique = $idEjAppliExtValue".PHP_EOL;
            $erreur .= 'Erreur : '.$e->getMessage().PHP_EOL.'Trace : '.$e->getTraceAsString().PHP_EOL.'Methode = Atexo_Chorus_Echange::saveEchange';
            $erreursEnvoiFlux[] = $erreur;
            (new Atexo_Chorus_Util())->loggerErreursEnvoiHttpFlux($erreur);

            return false;
        }
    }

    /*
     * Retourne l'echange by statut passé en paramettre
     */
    public function retreiveEchangesByStatut($statut, $connexion = null)
    {
        if (!$connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
        $c = new Criteria();
        $c->add(CommonChorusEchangePeer::STATUTECHANGE, $statut);
        $echanges = CommonChorusEchangePeer::doSelect($c, $connexion);
        if ($echanges) {
            return $echanges;
        } else {
            return [];
        }
    }

    /**
     * Permet de desactivé les tiers déjà rejeté pour un echange (dans le cas de renvoi de flux initialisation).
     *
     * @param array $erreursFluxChorus : liste des erreurs flux Chorus
     */
    public function desactiverMessageRejetEchange($echange, &$connexion = null, $logger = null, &$erreursFluxChorus = null)
    {
        if (!$connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        }
        try {
            $tiersRejete = self::retreiveTiersRejeteByIdEchange($echange->getId(), $connexion);
            if (is_array($tiersRejete)) {
                foreach ($tiersRejete as $tierRejete) {
                    $tierRejete->setActive('0');
                    $tierRejete->save($connexion);
                }
            }

            return true;
        } catch (Exception $e) {
            $erreurMessage = 'Erreur lors de la desactivation du message de rejet.'.PHP_EOL.'Erreur : '.$e->getMessage().PHP_EOL.'Trace : '.$e->getTraceAsString().PHP_EOL.'Methode = Atexo_Chorus_Echange::desactiverMessageRejetEchange';
            $logger->error($erreurMessage);
            $erreursFluxChorus[] = 'Erreur lors de la desactivation du message de rejet'.PHP_EOL.$erreurMessage;

            return false;
        }
    }

    /*
     * Permet d'avoir les tiers rejete par idEchange
     */
    public function retreiveTiersRejeteByIdEchange($idEchange, $connexion = null, $active = null)
    {
        if (!$connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
        $c = new Criteria();
        $c->add(CommonTChorusTierRejetePeer::ID_ECHANGE, $idEchange);
        if (null !== $active) {
            $c->add(CommonTChorusTierRejetePeer::ACTIVE, $active);
        }

        $tiers = CommonTChorusTierRejetePeer::doSelect($c, $connexion);
        if ($tiers) {
            return $tiers;
        } else {
            return false;
        }
    }

    /*
     * Permet d'ajouter un rejet d'un tier
     */
    public function creerTierRejete($idEchange, $idFonctionnel, $codeRejet, $organisationAchat, $connexion)
    {
        try {
            $newTierRejete = new CommonTChorusTierRejete();
            $newTierRejete->setIdEchange($idEchange);
            $newTierRejete->setIdTier($idFonctionnel);
            $newTierRejete->setCodeRejet($codeRejet); //Atexo_Config::getParameter('STATUT_TIER_CHORUS_N_EXISTE_PAS'));
            $newTierRejete->setOrganisationAchat($organisationAchat);
            $newTierRejete->setActive(1);
            $newTierRejete->setDateCreation(date('Y-m-d H:i:s'));
            $newTierRejete->save($connexion);
        } catch (Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('chorus');
            $logger->error("Erreur : idEchange= $idEchange, idFonctionnel = $idFonctionnel, codeRejet = $codeRejet, organisationAchat = $organisationAchat \n\nErreur : ".$e->getMessage());
        }
    }

    /*
     * Permet d'avoir le libelle d'un type marche chorus
     */
    public function getLibelleTypeMarcheByCode($code)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonChorusTypeMarchePeer::CODE, $code);
        $typeMarche = CommonChorusTypeMarchePeer::doSelectOne($c, $connexion);
        if ($typeMarche instanceof CommonChorusTypeMarche) {
            return $typeMarche->getLibelle();
        } else {
            return '';
        }
    }

    /*
     * Permet d'avoir le libelle d'un type de groupement chorus
     */
    public function getLibelleTypeGroupementByCode($code, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonChorusTypeGroupementPeer::CODE, $code);
        $c->add(CommonChorusTypeGroupementPeer::ORGANISME, $organisme);
        $typeG = CommonChorusTypeGroupementPeer::doSelectOne($c, $connexion);
        if ($typeG instanceof CommonChorusTypeGroupement) {
            return $typeG->getLibelle();
        } else {
            return '';
        }
    }

    /*
     * Permet d'avoir le libelle d'un type de pocedure chorus
     */
    public function getLibelleTypeProcedureByCode($code)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonChorusTypeProcedurePeer::CODE, $code);
        $typeProc = CommonChorusTypeProcedurePeer::doSelectOne($c, $connexion);
        if ($typeProc instanceof CommonChorusTypeProcedure) {
            return $typeProc->getLibelle();
        } else {
            return '';
        }
    }

    /*
     * Permet d'avoir le libelle d'un forme prix chorus
     */
    public function getLibelleFormePrixByCode($code)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonChorusFormePrixPeer::CODE, $code);
        $formePrix = CommonChorusFormePrixPeer::doSelectOne($c, $connexion);
        if ($formePrix instanceof CommonChorusFormePrix) {
            return $formePrix->getLibelle();
        } else {
            return '';
        }
    }

    /*
     * Permet d'avoir le libelle d'un forme prix chorus
     */
    public function getLibelleActeJuridiqueByCode($code)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonChorusActeJuridiquePeer::CODE, $code);
        $acteJ = CommonChorusActeJuridiquePeer::doSelectOne($c, $connexion);
        if ($acteJ instanceof CommonChorusActeJuridique) {
            return $acteJ->getLibelle();
        } else {
            return '';
        }
    }

    /*
     * Permet d'avoir le libelle d'un organisation d'achat
     */
    public function getLibelleOrganisationAchatByCode($code, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonChorusOrganisationAchatPeer::ORGANISME, $organisme);
        $c->add(CommonChorusOrganisationAchatPeer::CODE, $code);
        $oa = CommonChorusOrganisationAchatPeer::doSelectOne($c, $connexion);
        if ($oa instanceof CommonChorusOrganisationAchat) {
            return $oa->getLibelle();
        } else {
            return '';
        }
    }

    /*
     * Permet d'avoir le libelle d'un groupement d'achat
     */
    public function getLibelleGroupementAchatByCode($code, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonChorusGroupementAchatPeer::ORGANISME, $organisme);
        $c->add(CommonChorusGroupementAchatPeer::CODE, $code);
        $groupementAchat = CommonChorusGroupementAchatPeer::doSelectOne($c, $connexion);
        if ($groupementAchat instanceof CommonChorusGroupementAchat) {
            return $groupementAchat->getLibelle();
        } else {
            return '';
        }
    }

    /*
     * Permet d'avoir le libelle d'un regroupement comptable
     */
    public function getLibelleRegroupementComptableByCode($code, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonChorusRegroupementComptablePeer::ORGANISME, $organisme);
        $c->add(CommonChorusRegroupementComptablePeer::CODE, $code);
        $regroupementC = CommonChorusRegroupementComptablePeer::doSelectOne($c, $connexion);
        if ($regroupementC instanceof CommonChorusRegroupementComptable) {
            return $regroupementC->getLibelle();
        } else {
            return '';
        }
    }

    /*
     * Permet d'avoir le libelle d'un code de calcul d'interet
     */
    public function getLibelleCodeInteretByCode($code)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonChorusCodeCalculInteretsPeer::CODE, $code);
        $codeInt = CommonChorusCodeCalculInteretsPeer::doSelectOne($c, $connexion);
        if ($codeInt instanceof CommonChorusCodeCalculInterets) {
            return $codeInt->getLibelle();
        } else {
            return '';
        }
    }

    /*
     * Permet d'avoir le libelle d'une condition du paiment
     */
    public function getLibelleConditionPaiementByCode($code)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonChorusConditionsPaiementsPeer::CODE, $code);
        $condP = CommonChorusConditionsPaiementsPeer::doSelectOne($c, $connexion);
        if ($condP instanceof CommonChorusConditionsPaiements) {
            return $condP->getLibelle();
        } else {
            return '';
        }
    }

    /**
     * Permet de vérifier la fiche navette d'un echange est la même que celle de l'echange précedent.
     *
     * @param array $erreursFluxChorus : liste des erreurs flux Chorus
     */
    public function ficheNavetteNotDuplicated($currentEchange, &$erreursFluxChorus = null, $logger = null)
    {
        $echange = null;
        if (empty($logger)) {
            $logger = Atexo_LoggerManager::getLogger('chorus');
        }
        $logger->info('Verification de la duplication de la fiche navette');
        try {
            $resultat = true;
            $numOrdre = (int) $currentEchange->getNumOrdre();
            if ($numOrdre > 1) {
                $idDecision = $currentEchange->getIdDecision();
                $organisme = $currentEchange->getOrganisme();
                $criteriaVo = new Atexo_Chorus_CriteriaVo();
                $criteriaVo->setOrganisme($organisme);
                $criteriaVo->setIdDecision($idDecision);
                $criteriaVo->setNumOrdre(($numOrdre - 1));
                $echange = self::retreiveEchangeByCriteria($criteriaVo);
                if ($echange instanceof CommonChorusEchange) {
                    $currentFicheNavette = (new Atexo_Chorus_Echange())->retrieveFicheNavetteByIdChorusEchangeAndOrg($currentEchange->getId(), $organisme);
                    $ficheNavette = (new Atexo_Chorus_Echange())->retrieveFicheNavetteByIdChorusEchangeAndOrg($echange->getId(), $organisme);
                    if ($currentFicheNavette && $ficheNavette) {
                        if ($currentFicheNavette->getRpa() === $ficheNavette->getRpa() && $currentFicheNavette->getObjet() === $ficheNavette->getObjet()
                           && $currentFicheNavette->getFicheImmobilisation() === $ficheNavette->getFicheImmobilisation() && $currentFicheNavette->getAttentePj() === $ficheNavette->getAttentePj()
                           && $currentFicheNavette->getMarchePartage() === $ficheNavette->getMarchePartage() && $currentFicheNavette->getIdAccordCadre() === $ficheNavette->getIdAccordCadre()
                           && $currentFicheNavette->getLibAccordCadre() === $ficheNavette->getLibAccordCadre() && $currentFicheNavette->getAnneeCreationAccordCadre() === $ficheNavette->getAnneeCreationAccordCadre()
                           && $currentFicheNavette->getMontantHt() === $ficheNavette->getMontantHt() && $currentFicheNavette->getReconductible() === $ficheNavette->getReconductible()
                           && $currentFicheNavette->getVisaAccf() === $ficheNavette->getVisaAccf() && $currentFicheNavette->getVisaPrefet() === $ficheNavette->getVisaPrefet()
                           && $currentFicheNavette->getRetenueGarantie() === $ficheNavette->getRetenueGarantie() && $currentFicheNavette->getMontantMini() === $ficheNavette->getMontantMini()
                           && $currentFicheNavette->getMontantMaxi() === $ficheNavette->getMontantMaxi() && $currentFicheNavette->getMontantForfaitaire() === $ficheNavette->getMontantForfaitaire()
                           && $currentFicheNavette->getMontantBc() === $ficheNavette->getMontantBc() && $currentFicheNavette->getTauxTva() === $ficheNavette->getTauxTva()
                           && $currentFicheNavette->getCentreCout() === $ficheNavette->getCentreCout() && $currentFicheNavette->getCentreFinancier() === $ficheNavette->getCentreFinancier()
                           && $currentFicheNavette->getActivite() === $ficheNavette->getActivite() && $currentFicheNavette->getDomaineFonctionnel() === $ficheNavette->getDomaineFonctionnel()
                           && $currentFicheNavette->getFond() === $ficheNavette->getFond() && $currentFicheNavette->getLocalisationInterministerielle() === $ficheNavette->getLocalisationInterministerielle()
                           && $currentFicheNavette->getNature() === $ficheNavette->getNature() && $currentFicheNavette->getAxeMinisteriel1() === $ficheNavette->getAxeMinisteriel1()
                           && $currentFicheNavette->getProjetAnalytique() === $ficheNavette->getProjetAnalytique() && $currentFicheNavette->getLocalisationMinisterielle() === $ficheNavette->getLocalisationMinisterielle()
                           && $currentFicheNavette->getAxeMinisteriel2() === $ficheNavette->getAxeMinisteriel2() && $currentFicheNavette->getRemarques() === $ficheNavette->getRemarques()
                        ) {
                            $logger->info("Verification KO : la fiche navette de echange est la meme que celle de l'echange precedent");

                            return false;
                        }
                    }
                }
            }
            $logger->info('Verification fiche navette : OK');

            return $resultat;
        } catch (Exception $ex) {
            $erreurMessage = 'Erreur lors de la verification de la fiche navette : {id_echange = '.$echange->getId().' , org = '.$echange->getOrganisme().'}'.PHP_EOL.'Erreur : '.$ex->getMessage().PHP_EOL.'Trace : '.$ex->getTraceAsString().PHP_EOL.'Methode = Atexo_Chorus_Echange::ficheNavetteNotDuplicated';
            $logger->error($erreurMessage);
            $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;
        }
    }

    /*
     * Retourne Echange
     * @param criteria
     */
    public function retreiveEchangeByCriteria($criteriaVo)
    {
        if ($criteriaVo instanceof Atexo_Chorus_CriteriaVo) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            $c = new Criteria();
            if ($criteriaVo->getIdDecision()) {
                $c->add(CommonChorusEchangePeer::ID_DECISION, $criteriaVo->getIdDecision());
            }
            if ($criteriaVo->getOrganisme()) {
                $c->add(CommonChorusEchangePeer::ORGANISME, $criteriaVo->getOrganisme());
            }
            if ($criteriaVo->getNumOrdre()) {
                $c->add(CommonChorusEchangePeer::NUM_ORDRE, $criteriaVo->getNumOrdre());
            }
            $echange = CommonChorusEchangePeer::doSelectOne($c, $connexion);
            if ($echange) {
                return $echange;
            } else {
                return false;
            }
        }
    }

    /*
     * Permet de retourner le fichie echange by id et type s'il existe
     */
    public function retreiveNomFichierByIdEchangeType($idEchange, $typeFichier)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonChorusNomsFichiersPeer::ID_ECHANGE, $idEchange);
        $c->add(CommonChorusNomsFichiersPeer::TYPE_FICHIER, $typeFichier);
        $fichierEchange = CommonChorusNomsFichiersPeer::doSelectOne($c, $connexion);
        if ($fichierEchange) {
            return $fichierEchange;
        } else {
            return false;
        }
    }

    /**
     * Permet de recuperer l'objet t_chorus_fiche_modificative par l'identifiant echange chorus et l'organisme.
     *
     * @param int    $idChorusEchange: identifiant echange chorus
     * @param string $organisme:       organisme
     *
     * @return CommonTChorusFicheModificative
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since place-2015
     *
     * @copyright Atexo 2015
     */
    public function retrieveFicheModificativeByIdChorusEchangeAndOrg($idChorusEchange, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonTChorusFicheModificativePeer::ID_ECHANGE, $idChorusEchange);
        $c->add(CommonTChorusFicheModificativePeer::ORGANISME, $organisme);
        $ficheModificative = CommonTChorusFicheModificativePeer::doSelectOne($c, $connexion);
        if ($ficheModificative instanceof CommonTChorusFicheModificative) {
            return $ficheModificative;
        }

        return false;
    }

    /**
     * Permet de vérifier si on a au moins un echange chorus integre.
     *
     * @param $idContrat
     * @param $organisme
     *
     * @return bool true si le retour est integre si non false
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function isRetourChorusIntegreByIdContrat($idContrat, $organisme)
    {
        $echangeQuery = new CommonChorusEchangeQuery();
        $fichierEchange = $echangeQuery->getRetourChorusByType($organisme, $idContrat, Atexo_Config::getParameter('CHORUS_RETOUR_INTEGRE'));
        if ($fichierEchange instanceof CommonChorusEchange) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Permet de preciser le type de flux a envoyer a CHORUS
     *     => 0 : Envoi FEN
     *     => 1 : Envoi FIR
     *     => 2 : Changement de statut.
     *
     * @param CommonChorusEchange     $echange  : objet Chorus Echange
     * @param CommonTContratTitulaire $contrat  : objet contrat titulaire
     * @param string                  $numOrdre : numero d'ordre
     *
     * @return string
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function typeActionEnvoiChorus($echange, $contrat, $numOrdre)
    {
        if (
            $contrat instanceof CommonTContratTitulaire
                || (Atexo_Config::getParameter('ACTIVE_EXEC_V2')
                    && is_array($contrat)
                    && !empty($contrat))
        ) {
            if ($echange instanceof CommonChorusEchange) {
                try {
                    (new Atexo_Chorus_Util())->loggerInfos('Precision du type de flux : Echange [Id = '.$echange->getId().' , Organisme = '.$echange->getOrganisme().']');
                    if ((new Atexo_Chorus_Util())->isTypeEnvoiFicheModificative($contrat, $echange) || $echange->isTypeFluxFen211() || $echange->hasToutPaysEtranger()) {
                        // envoyer FEN
                        (new Atexo_Chorus_Util())->loggerInfos('Type de flux a envoyer = FEN , Echange [Id = '.$echange->getId().' , Organisme = '.$echange->getOrganisme().']');

                        return '0';
                    } else {
                        $codePayFr = $echange->hasCodePaysByType(Atexo_Config::getParameter('DENOMINATION2_GEON2_FRANCE'));
                        if ($numOrdre > 1 && !$codePayFr) {
                            (new Atexo_Chorus_Util())->loggerInfos('Type de flux = FEN (mise a jour de statut pour envoi via la routine), Echange [Id = '.$echange->getId().' , Organisme = '.$echange->getOrganisme().']');

                            return '2'; //change statut
                        } else {
                            (new Atexo_Chorus_Util())->loggerInfos('Type de flux a envoyer = FIR , Echange [Id = '.$echange->getId().' , Organisme = '.$echange->getOrganisme().']');

                            return '1'; //envoyer FIR
                        }
                    }
                } catch (\Exception $e) {
                    (new Atexo_Chorus_Util())->loggerErreur('Erreur lors de la precision du type de flux , Echange [Id = '.$echange->getId().' , Organisme = '.$echange->getOrganisme()."], Methode = ChorusDetailEchanges::echangeSendFluxFir , \n Exception = [ ".$e->getMessage().' ]');
                }
            } else {
                (new Atexo_Chorus_Util())->loggerErreur('Echange Chorus introuvable');

                return false;
            }
        } else {
            (new Atexo_Chorus_Util())->loggerErreur('Contrat Chorus introuvable');

            return false;
        }
    }

    /**
     * Permet de mettre a jour un echange Chorus pour la transmission.
     *
     * @param string    $idEchange         : identifiant de l'echange CHORUS
     * @param string    $organisme         : organisme
     * @param string    $statutEchange     : statut de l'echange CHORUS
     * @param string    $typeFlux          : type de flux
     * @param array     $erreursFluxChorus : liste des erreurs flux Chorus
     * @param PropelPDO $connexion
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function updateChorusEchangeForTransmission($idEchange, $organisme, $statutEchange, $typeFlux = null, $connexion = null, $logger = null, &$erreursFluxChorus = null, $nomFluxTmp = null, $idUnique = null)
    {
        if (empty($logger)) {
            $logger = Atexo_LoggerManager::getLogger('chorus');
        }
        $logger->info("Debut mis a jour de la table 'Chorus_Echange'");
        try {
            if (empty($connexion)) {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            }
            $echange = CommonChorusEchangeQuery::create()->filterById($idEchange)->filterByOrganisme($organisme)->findOne($connexion);
            if ($echange instanceof CommonChorusEchange) {
                $echange->setStatutechange($statutEchange);
                if (!empty($typeFlux)) {
                    $echange->setTypeFluxAEnvoyer($typeFlux);
                }
                if (!empty($nomFluxTmp)) {
                    $echange->setTmpFileName($nomFluxTmp);
                }
                if (!empty($idUnique)) {
                    $echange->setIdEjAppliExt($idUnique);
                }
                $echange->save($connexion);
                $logger->info("Statut mis a jour avec SUCCES: Echange [Id = $idEchange , Organisme = $organisme , Statut = '$statutEchange' (libelle statut utilisateur = '".$echange->getLibelleStatutEchange()."')]");
                if (!empty($typeFlux)) {
                    $logger->info("Type Flux mis a jour avec SUCCES: Echange [Id = $idEchange , Organisme = $organisme , type Flux = '$typeFlux' (".$echange->getLibelleTypeFlux().')]');
                }
                if (!empty($nomFluxTmp)) {
                    $logger->info("Chemin Flux Tmp mis a jour avec SUCCES: Echange [Id = $idEchange , Organisme = $organisme , Chemin Flux Tmp = '$nomFluxTmp']");
                }
                if (!empty($idUnique)) {
                    $logger->info("Identifiant unique mis a jour avec SUCCES: Echange [Id = $idEchange , Organisme = $organisme , idUnique = '$idUnique']");
                }
            }

            return true;
        } catch (\Exception) {
            $connexion->rollBack();
            $erreurMessage = "Erreur lors de la mise a jour du statut de l'Echange Chorus [Id_echange = $idEchange , Organisme = $organisme , Statut = $statutEchange , nomFluxTmp = $nomFluxTmp , idUnique = $idUnique] , \n Methode = Atexo_Chorus_Echange::updateChorusEchangeForTransmission";
            $logger->error($erreurMessage);
            if (null !== $erreursFluxChorus) {
                $erreursFluxChorus[] = "Erreur lors de la mise a jour du statut de l'Echange Chorus".PHP_EOL.$erreurMessage;
            }

            return false;
        }
        $logger->info("Fin mis a jour de la table 'Chorus_Echange'");
    }

    /**
     * Permet de construire le nom du flux a envoyer a CHORUS.
     *
     * @param CommonChorusEchange $echange           : objet chorus echange
     * @param PropelPDO           $connexion         : connexion
     * @param array               $erreursFluxChorus : liste des erreurs flux Chorus
     *
     * @return string
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function construireNomFluxChorus($echange, &$connexion, &$erreursEnvoiFlux = null)
    {
        if ($echange instanceof CommonChorusEchange) {
            (new Atexo_Chorus_Util())->loggerInfosEnvoiHttpFlux('Debut construction du nom du flux : Echange [id = '.$echange->getId().' , org = '.$echange->getOrganisme().' type de flux = '.$echange->getLibelleTypeFlux().']');
            try {
                $numSequence = (new Atexo_Chorus_Echange())->getNumeroSequenceByType($echange->getLibelleTypeFlux(), $connexion, $erreursEnvoiFlux);
                if (empty($numSequence)) {
                    return false;
                }
                if ($echange->getTypeFluxAEnvoyer() == Atexo_Config::getParameter('TYPE_FLUX_CHORUS_FIR')) {
                    $nameFile = Atexo_Config::getParameter('TYPE_FLUX_FIR_CHORUS').Atexo_Config::getParameter('CODE_INTERFACE_FIR_CHORUS').Atexo_Config::getParameter('VERSION_INTERFACE_FIR_CHORUS')
                                   .'_'.Atexo_Config::getParameter('CODE_APPLICATION_FIR_CHORUS').Atexo_Config::getParameter('NUMERO_INSTANCE_FIR_CHORUS').'_'.Atexo_Config::getParameter('CODE_APPLICATION_FIR_CHORUS')
                                       .Atexo_Config::getParameter('NUMERO_INSTANCE_FIR_CHORUS').Atexo_Config::getParameter('CODE_INTERFACE_FIR_CHORUS').$numSequence;
                } else {
                    $nameFile = (new Atexo_Chorus_Util())->getFilePrefix($echange->isTypeFluxFen211()).Atexo_Config::getParameter('CODE_APPLICATION_FIR_CHORUS').'001_'
                                   .Atexo_Config::getParameter('CODE_APPLICATION_FIR_CHORUS').'001'.(new Atexo_Chorus_Util())->getCodeInterface($echange->isTypeFluxFen211()).$numSequence;
                }

                (new Atexo_Chorus_Util())->loggerInfosEnvoiHttpFlux("Nom du flux a envoyer : $nameFile");

                return $nameFile;
            } catch (\Exception $e) {
                $erreur = 'Erreur lors de la construction du nom du flux CHORUS : {type de flux = '.$echange->getLibelleTypeFlux().'}'.PHP_EOL.'Erreur: '.$e->getMessage().'Trace: '.$e->getTraceAsString().PHP_EOL.'Methode = Atexo_Chorus_Echange::construireNomFluxChorus';
                $erreursEnvoiFlux[] = $erreur;
                (new Atexo_Chorus_Util())->loggerErreursEnvoiHttpFlux($erreur);

                return false;
            }
            (new Atexo_Chorus_Util())->loggerInfosEnvoiHttpFlux('Fin construction du nom du flux : Echange [id = '.$echange->getId().' , org = '.$echange->getOrganisme().' type de flux = '.$echange->getLibelleTypeFlux().']');
        } else {
            $erreur = 'Construction du nom du flux. '.PHP_EOL."L'echange Chorus n'est pas une instance de CommonChorusEchange : ".print_r($echange, true);
            $erreursEnvoiFlux[] = $erreur;
            (new Atexo_Chorus_Util())->loggerErreursEnvoiHttpFlux($erreur);

            return false;
        }
    }

    /**
     * Permet de generer l'xml du flux FIR.
     *
     * @param CommonChorusEchange     $echange           : objet Chorus Echange
     * @param PropelPDO               $connexion         : connexion
     * @param array                   $erreursFluxChorus : liste des erreurs flux Chorus
     * @param CommonTContratTitulaire $contrat           : objet contrat
     *
     * @return string|bool : le chemin du flux genere, false sinon
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function genererXmlFluxFIR($echange, &$connexion, &$erreursFluxChorus, $logger = null): string|bool
    {
        if (empty($logger)) {
            $logger = Atexo_LoggerManager::getLogger('chorus');
        }
        if ($echange instanceof CommonChorusEchange) {
            $logger->info('Debut generation XML FIR - Echange [Id = '.$echange->getId().' , Organisme = '.$echange->getOrganisme().']');
            try {
                $xml = (new Atexo_Chorus_Transmission())->createFluxFir($echange, $erreursFluxChorus, $logger);
                $tmpFilePath = self::stockerFichierSurServeur(self::getTmpDirFile($echange, Atexo_Config::getParameter('TYPE_FLUX_CHORUS_FIR'), $logger), self::getFileNameEnvoiChorus($echange, $logger), $xml, $erreursFluxChorus, $logger);

                return $tmpFilePath;
            } catch (\Exception $e) {
                $connexion->rollBack();
                $erreur = "Erreur lors de la generation de l'XML du flux FIR : {id_echange = ".$echange->getId().' , org = '.$echange->getOrganisme().'}'.PHP_EOL.'Erreur : '.$e->getMessage().PHP_EOL.'Trace : '.$e->getTraceAsString().PHP_EOL.'Methode = Atexo_Chorus_Echange::genererXmlFluxFIR';
                $logger->error($erreur);
                $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreur;

                return false;
            }

            $logger->info('Fin generation XML FIR - Echange [Id = '.$echange->getId().' , Organisme = '.$echange->getOrganisme().']');
        } else {
            $connexion->rollBack();
            $erreur = "L'echange Chorus n'est pas une instance de CommonChorusEchange : ".print_r($echange, true);
            $logger->error($erreur);
            $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreur;

            return false;
        }
    }

    /**
     * Permet de generer l'XML du flux FEN211.
     *
     * @param CommonChorusEchange $echange           : objet Chorus Echange
     * @param PropelPDO           $connexion         : connexion
     * @param array               $erreursFluxChorus : liste des erreurs flux Chorus
     *
     * @return string|bool : le chemin du flux genere, false sinon
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function genererXmlFluxFEN211($echange, &$connexion, &$erreursFluxChorus, $logger = null): string|bool
    {
        if (empty($logger)) {
            $logger = Atexo_LoggerManager::getLogger('chorus');
        }
        if ($echange instanceof CommonChorusEchange) {
            $logger->info('Debut generation XML FEN211 - Echange [Id = '.$echange->getId().' , Organisme = '.$echange->getOrganisme().']');
            try {
                $xml = (new Atexo_Chorus_Transmission())->createFluxFenModeIntegre($echange, $echange->getOrganisme(), $erreursFluxChorus, $logger);

                if (false !== $xml) {
                    //Validation XSD
                    if (Atexo_Config::getParameter('USE_VALIDATION_XSD_WS_CHORUS_FEN211')) {
                        $logger->info('Debut de la validation XSD du flux FEN211 sortant');
                        $xmlValide = Atexo_Util::validateXmlBySchema($xml, Atexo_Config::getParameter('PATH_FILE_XSD_WS_CHORUS_FEN211'), $logger, $messageErreur);
                        if ($xmlValide) {
                            $logger->info('*** Le flux FEN211 est valide ***');
                        } else {
                            $messageErreurValidation = 'Validation XSD : La structure du flux FEN211 est invalide : '.PHP_EOL."Message erreur : $messageErreur ".PHP_EOL.PHP_EOL.' Xml:'.PHP_EOL."$xml";
                            $logger->info($messageErreurValidation);
                            $connexion->rollBack();
                            $logger->error($messageErreurValidation);
                            $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$messageErreurValidation;

                            return false;
                        }
                        $logger->info('Fin de la validation XSD du flux FEN211 sortant');
                    }

                    $tmpFilePath = self::stockerFichierSurServeur(self::getTmpDirFile($echange, Atexo_Config::getParameter('TYPE_FLUX_CHORUS_FEN211'), $logger), self::getFileNameEnvoiChorus($echange, $logger), $xml, $erreursFluxChorus, $logger);

                    $logger->info('Fin generation XML FEN211 - Echange [Id = '.$echange->getId().' , Organisme = '.$echange->getOrganisme().']');

                    return $tmpFilePath;
                } else {
                    $codeErreur = Atexo_Config::getParameter('CODE_ERREUR_REJET_CHORUS');
                    $libelleErreur = prado::localize('LIBELLE_MESSAGE_ERREUR_GENERATION_FLUX_CHORUS');
                    $connexionForError = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                    self::actionXmlNonValideFluxFen($echange, $connexionForError, $codeErreur, $libelleErreur, $erreursFluxChorus, $logger);
                    unset($connexionForError);

                    $connexion->rollBack();
                    $erreurMessage = "Code Erreur : $codeErreur".PHP_EOL.'Erreur: '.PHP_EOL.$libelleErreur.PHP_EOL.'XML: '.print_r($xml, true);
                    $logger->error($erreurMessage);
                    $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;

                    return false;
                }
            } catch (\Exception $e) {
                $connexion->rollBack();
                $erreur = "Erreur lors de la generation de l'XML du flux FEN211 : {id_echange = ".$echange->getId().' , org = '.$echange->getOrganisme().'}'.PHP_EOL.'Erreur : '.$e->getMessage().PHP_EOL.'Trace : '.$e->getTraceAsString().PHP_EOL.'Methode = Atexo_Chorus_Echange::genererFluxFEN211';
                $logger->error($erreur);
                $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreur;

                return false;
            }
            $logger->info('Fin generation XML FEN211 - Echange [Id = '.$echange->getId().' , Organisme = '.$echange->getOrganisme().']');
        } else {
            $connexion->rollBack();
            $erreurMessage = "L'echange Chorus n'est pas une instance de CommonChorusEchange : ".print_r($echange, true);
            $logger->error($erreurMessage);
            $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;

            return false;
        }
    }

    /**
     * Permet de generer l'XML du flux FEN111.
     *
     * @param CommonChorusEchange     $echange           : objet Chorus Echange
     * @param PropelPDO               $connexion         : connexion
     * @param array                   $erreursFluxChorus : liste des erreurs flux Chorus
     * @param CommonTContratTitulaire $contrat           : objet contrat
     *
     * @return string|bool : le chemin du flux genere, false sinon
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function genererXmlFluxFEN111($echange, $contrat, &$connexion, &$erreursFluxChorus, $logger = null): string|bool
    {
        if (empty($logger)) {
            $logger = Atexo_LoggerManager::getLogger('chorus');
        }
        if ($echange instanceof CommonChorusEchange) {
            if ($contrat) {
                $logger->info('Debut generation XML FEN111 - Echange [Id = '.$echange->getId().' , Organisme = '.$echange->getOrganisme().']');
                try {
                    //Generation Id unique (uniquement si la valeur n'existe pas)
                    $idUnique = $echange->getIdEjAppliExt();
                    if (empty($idUnique)) {
                        $organismeObject = Atexo_Organismes::retrieveOrganismeByAcronyme($echange->getOrganisme());
                        $idUnique = (new Atexo_Chorus_Util())->generateChorusUniqueId($organismeObject->getId(), $echange->getIdDecision(), 'Z', $erreursFluxChorus, $logger, $echange->getUuidExterneExec());
                        if (false !== $idUnique) {
                            $echange->setIdEjAppliExt($idUnique);
                            $echange->save($connexion);
                            $logger->info("Identifiant unique mis a jour avec *** SUCCES *** dans la table 'Chorus_Echange': Valeur = $idUnique");
                        } else {
                            $erreurMessage = "L'identifiant unique n'a pas été genere - Echange [Id = ".$echange->getId().' , Organisme = '.$echange->getOrganisme().']';
                            $logger->error($erreurMessage);
                            $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;

                            return false;
                        }
                    }

                    //Generation du flux XML
                    $sendFicheNavette = (new Atexo_Chorus_Echange())->ficheNavetteNotDuplicated($echange, $erreursFluxChorus, $logger);
                    $isFicheModificative = ((new Atexo_Chorus_Util())->isTypeEnvoiFicheModificative($contrat, $echange, $logger, $erreursFluxChorus)) ? true : false;
                    $xml = (new Atexo_Chorus_Transmission())->createFluxFen($echange, $echange->getOrganisme(), $idUnique, $sendFicheNavette, $isFicheModificative, $erreursFluxChorus, $logger);

                    if (false !== $xml) {
                        //Verification du format de l'XML
                        if (!strstr($xml, '<IdEJAppliExt>')) { //si jamais la balise <IdEJAppliExt> n'est pas dans le flux on affiche une erreur
                            $connexion->rollBack();
                            $erreurMessage = "La balise 'IdEJAppliExt' n'est pas dans le flux : Echange [Id = ".$echange->getId().' , Organisme = '.$echange->getOrganisme()."] : Le flux n'est pas valide : ".$xml;
                            $logger->error($erreurMessage);
                            $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;

                            return false;
                        }
                        if (!strstr($xml, '<IdEJAppliExt></IdEJAppliExt>')) { //si jamais la balise <IdEJAppliExt></IdEJAppliExt> n'est pas dans le flux alors c'est que le flux est potentiellement bon
                            $logger->info('*** Le flux FEN111 est valide ***');

                            $tmpFilePath = self::stockerFichierSurServeur(self::getTmpDirFile($echange, Atexo_Config::getParameter('TYPE_FLUX_CHORUS_FEN111'), $logger), self::getFileNameEnvoiChorus($echange, $logger), $xml, $erreursFluxChorus, $logger);

                            $logger->info('Fin generation XML FEN111 - Echange [Id = '.$echange->getId().' , Organisme = '.$echange->getOrganisme().']');

                            return $tmpFilePath;
                        } else {
                            $connexion->rollBack();
                            $erreurMessage = "La balise 'IdEJAppliExt' est vide dans le flux : Echange [Id = ".$echange->getId().' , Organisme = '.$echange->getOrganisme()."] : Le flux n'est pas valide : ".$xml;
                            $logger->error($erreurMessage);
                            $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;

                            return false;
                        }
                    } else {
                        $codeErreur = Atexo_Config::getParameter('CODE_ERREUR_REJET_CHORUS');
                        $libelleErreur = Prado::localize('LIBELLE_MESSAGE_ERREUR_GENERATION_FLUX_CHORUS');
                        $connexionForError = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                        self::actionXmlNonValideFluxFen($echange, $connexionForError, $codeErreur, $libelleErreur, $erreursFluxChorus, $logger);
                        unset($connexionForError);

                        $connexion->rollBack();
                        $erreurMessage = "Le flux n'est pas valide : ".PHP_EOL."Code Erreur : $codeErreur".PHP_EOL."IdUnique(idEjAppliExtValue) = $idUnique ".PHP_EOL.'Echange [Id = '.$echange->getId().' , Organisme = '.$echange->getOrganisme().PHP_EOL.'Erreur: '.$libelleErreur.PHP_EOL.'XML: '.print_r($xml, true);
                        $logger->error($erreurMessage);
                        $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;

                        return false;
                    }
                } catch (\Exception $e) {
                    $connexion->rollBack();
                    $erreur = 'Erreur lors de la generation du flux FEN111 : {id_echange = '.$echange->getId().' , org = '.$echange->getOrganisme().'}'.PHP_EOL.'Erreur : '.$e->getMessage().PHP_EOL.'Trace : '.$e->getTraceAsString().PHP_EOL.'Methode = Atexo_Chorus_Echange::genererFluxFEN111';
                    $logger->error($erreur);
                    $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreur;

                    return false;
                }
                $logger->info('Fin generation XML FEN111 - Echange [Id = '.$echange->getId().' , Organisme = '.$echange->getOrganisme().']');
            } else {
                $connexion->rollBack();
                $erreurMessage = "Le contrat n'est pas une instance de CommonTContratTitulaire : ".print_r($contrat, true);
                $logger->error($erreurMessage);
                $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;

                return false;
            }
        } else {
            $connexion->rollBack();
            $erreurMessage = "L'echange Chorus n'est pas une instance de CommonChorusEchange : ".print_r($echange, true);
            $logger->error($erreurMessage);
            $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;

            return false;
        }
    }

    /**
     * Permet de stocker le fichier sur le serveur.
     *
     * @param string     $dirName           : nom du repertoire
     * @param string     $filename          : nom du fichier
     * @param string     $flux              : nom du flux
     * @param array|null $erreursFluxChorus : liste des erreurs flux Chorus
     *
     * @return string|bool : chemin du fichier , false sinon
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function stockerFichierSurServeur($dirName, $filename, $flux, &$erreursFluxChorus = null, $logger = null): string|bool
    {
        if (empty($logger)) {
            $logger = Atexo_LoggerManager::getLogger('chorus');
        }
        $logger->info("Debut stockage du fichier temporaire sur le serveur dans le repertoire '$dirName'");
        try {
            if (!is_dir($dirName)) {
                $mode = 0755;
                if (true !== Atexo_Util::createDirectory($dirName, $mode, Atexo_LoggerManager::getLogger('chorus'))) {
                    $erreurMessage = "Impossible de creer le repertoire $dirName : droit = $mode ";
                    $logger->error($erreurMessage);
                    $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;

                    return false;
                }
            }
            $dirName = realpath($dirName);
            Atexo_Util::write_file($dirName.DIRECTORY_SEPARATOR.$filename, $flux);
            $logger->info('Fichier stocke sur le serveur avec SUCCES : chemin = '.$dirName.DIRECTORY_SEPARATOR.$filename);

            return $dirName.DIRECTORY_SEPARATOR.$filename;
        } catch (\Exception $e) {
            $erreur = "Erreur lors du stockage du fichier sur le repertoire $dirName : [nom fichier = $filename] ".PHP_EOL.'Erreur : '.$e->getMessage().PHP_EOL.'Trace : '.$e->getTraceAsString().PHP_EOL.'Methode = Atexo_Chorus_Echange::stockerFichierSurServeur';
            $logger->error($erreur);
            if (null !== $erreursFluxChorus) {
                $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreur;
            }

            return false;
        }
        $logger->info("Fin stockage du fichier sur le serveur dans le repertoire temporaire '$dirName'");
    }

    /**
     * Permet de construire le nom du flux a stocker dans le repertoire temporaire.
     *
     * @param CommonChorusEchange $echange : objet Chorus Echange
     *
     * @return string
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function getFileNameEnvoiChorus($echange, $logger)
    {
        if ($echange instanceof CommonChorusEchange) {
            $nomFichier = $echange->getId().'.xml';
            $logger->info("Nom du fichier temporaire : $nomFichier ");

            return $nomFichier;
        }
    }

    /**
     * Permet de construire le nom du repertoire de stockage des fichiers temporaires.
     *
     * @param CommonChorusEchange $echange  : objet Chorus Echange
     * @param string              $typeFlux : type de flux
     *
     * @return string
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function getTmpDirFile($echange, $typeFlux, $logger)
    {
        $dir = null;
        if ($echange instanceof CommonChorusEchange) {
            switch ($typeFlux) {
                case Atexo_Config::getParameter('TYPE_FLUX_CHORUS_FIR'):
                    $dir = Atexo_Config::getParameter('DIR_FILE_CHORUS_ENVOI_TMP').DIRECTORY_SEPARATOR.Atexo_Config::getParameter('TYPE_FLUX_FIR_CHORUS');
                    break;
                case Atexo_Config::getParameter('TYPE_FLUX_CHORUS_FEN211'):
                    $dir = Atexo_Config::getParameter('DIR_FILE_CHORUS_ENVOI_TMP').DIRECTORY_SEPARATOR.Atexo_Config::getParameter('TYPE_FLUX_FEN_CHORUS_FEN211');
                    break;
                case Atexo_Config::getParameter('TYPE_FLUX_CHORUS_FEN111'):
                    $dir = Atexo_Config::getParameter('DIR_FILE_CHORUS_ENVOI_TMP').DIRECTORY_SEPARATOR.Atexo_Config::getParameter('TYPE_FLUX_FEN_CHORUS_FEN111');
                    break;
            }
            $logger->info("Nom du repertoire de stockage du fichier temporaire : $dir");

            return $dir;
        }
    }

    /**
     * Effectue les actions qui doivent suivre lorsque le flux xml (FEN111 et FEN211) est invalide.
     *
     * @param $echange
     * @param $connexion
     * @param $codeErreur
     * @param $libelleErreur
     * @param array $erreursFluxChorus : liste des erreurs flux Chorus
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function actionXmlNonValideFluxFen($echange, &$connexion, $codeErreur, $libelleErreur, &$erreursFluxChorus, $logger = null)
    {
        if (empty($logger)) {
            $logger = Atexo_LoggerManager::getLogger('chorus');
        }
        try {
            if ($echange instanceof CommonChorusEchange) {
                $echange->setRetourChorus(Atexo_Config::getParameter('CHORUS_RETOUR_SANS_OBJET'));
                $echange->setErreurRejet('('.$codeErreur.')  '.$libelleErreur);
                $echange->setStatutechange(Atexo_Config::getParameter('STATUS_ECHANGE_CHORUS_NON_ENVOYER'));
                $echange->setDateEnvoi(date('Y-m-d H:i:s'));
                $echange->save($connexion);

                (new Atexo_Chorus_Echange())->desactiverMessageRejetEchange($echange, $connexion);
                (new Atexo_Agent_Alertes())->sendCourrierErreurGenerationFluxChorusAgent($echange, $connexion, $logger);

                $connexion->commit();
            }
        } catch (\Exception $e) {
            $connexion->rollBack();
            $erreur = 'Une erreur est survenue : '.$e->getMessage().PHP_EOL.'Trace : '.$e->getTraceAsString().PHP_EOL.'Methode = Atexo_Chorus_Echange::actionXmlNonValideFluxFen';
            $logger->error($erreur);
            $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreur;
        }
    }

    /**
     * Permet de recuperer le dernier numero de sequence.
     *
     * @param string    $typeFlux  type de flux
     * @param PropelPDO $connexion connexion
     * @param Logger    $logger    logger
     *
     * @return CommonChorusNumeroSequence
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2016
     *
     * @copyright Atexo 2017
     */
    public function getLastSequenceFlux($typeFlux, $connexion = null, $logger = null)
    {
        try {
            if (empty($connexion)) {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            }

            return CommonChorusNumeroSequenceQuery::create()->filterByTypeFichier($typeFlux)->findOne($connexion);
        } catch (Exception $e) {
            if (empty($logger)) {
                $logger = Atexo_LoggerManager::getLogger('chorus');
            }
            $logger->error('Erreur recuperation dernier numero de sequence.'.PHP_EOL.'Error : '.PHP_EOL.$e->getMessage().PHP_EOL.'Trace : '.PHP_EOL.$e->getTraceAsString());
        }
    }

    /**
     * Permet de supprimer les echanges Chorus et objets lies
     * Les blob sont supprimes via des Hooks (postDelete).
     *
     * @param CommonTContratTitulaire $contrat   objet contrat titulaire
     * @param PropelPDO               $connexion objet connexion
     * @param Logger                  $logger    logger
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since 2017-roadmap
     *
     * @copyright Atexo 2017
     */
    public static function supprimerEchangesChorusEtObjetsLiesParContrat(CommonTContratTitulaire $contrat, &$connexion, $logger = null)
    {
        try {
            if (empty($logger)) {
                $logger = Atexo_LoggerManager::getLogger('chorus');
            }

            $listeEchangesChorus = (new Atexo_Chorus_Echange())->retreiveEchangesByIdDecision($contrat->getOrganisme(), $contrat->getIdContratTitulaire());
            if (is_array($listeEchangesChorus) && !empty($listeEchangesChorus)) {
                foreach ($listeEchangesChorus as $echangeChorus) {
                    if ($echangeChorus instanceof CommonChorusEchange) {
                        //Fiche navette
                        $ficheNavette = (new Atexo_Chorus_Echange())->retrieveFicheNavetteByIdChorusEchangeAndOrg($echangeChorus->getId(), $echangeChorus->getOrganisme());
                        if ($ficheNavette instanceof CommonChorusFicheNavette) {
                            $ficheNavette->delete($connexion);
                            $logger->info('[echange_id = '.$echangeChorus->getId().' | contrat_id = '.$contrat->getIdContratTitulaire().' | org = '.$contrat->getOrganisme().' | fiche_navette_id = '.$ficheNavette->getId().'] fiche navette supprimee');
                        }

                        //chorus_noms_fichiers pour les flux FEN, CEN, CIR, FIR
                        $chorusNomsFichiersQuery = CommonChorusNomsFichiersQuery::create();
                        $listeTypesFlux = [Atexo_Config::getParameter('TYPE_FLUX_FEN_CHORUS'), 'CEN', Atexo_Config::getParameter('TYPE_FLUX_CIR_CHORUS'), Atexo_Config::getParameter('TYPE_FLUX_FIR_CHORUS')];
                        foreach ($listeTypesFlux as $typesFlux) {
                            $chorusNomsFichiersQuery->filterByIdEchange($echangeChorus->getId())->filterByAcronymeOrganisme($echangeChorus->getOrganisme())->filterByTypeFichier($typesFlux)->delete($connexion);
                            $logger->info('[echange_id = '.$echangeChorus->getId().' | contrat_id = '.$contrat->getIdContratTitulaire().' | org = '.$contrat->getOrganisme().' | type_flux = '.$typesFlux.'] ChorusNomsFichiers supprime');
                        }

                        //chorus_noms_fichiers pour le flux FSO
                        $chorusNomsFichiersQuery->filterByIdEchange($contrat->getIdContratTitulaire())->filterByAcronymeOrganisme($contrat->getOrganisme())->filterByTypeFichier('FSO')->delete($connexion);
                        $logger->info('[contrat_id = '.$contrat->getIdContratTitulaire().' | org = '.$contrat->getOrganisme().' | type_fichier = FSO] ChorusNomsFichiers supprime');

                        //chorus_numeros_marches
                        CommonChorusNumerosMarchesQuery::create()->filterByIdDecision($contrat->getIdContratTitulaire())->filterByAcronymeOrganisme($contrat->getOrganisme())->delete($connexion);
                        $logger->info('[contrat_id = '.$contrat->getIdContratTitulaire().' | org = '.$contrat->getOrganisme().'] ChorusNumerosMarches supprime');

                        //Chorus_pj (le choix de charger les objets avec find a pour but de declencher l'evenement (postDelete) de suppression des blobs sur le serveur
                        $chorusPj = CommonChorusPjQuery::create()->filterByIdEchange($echangeChorus->getId())->filterByOrganisme($echangeChorus->getOrganisme())->find($connexion);
                        if (!empty($chorusPj)) {
                            foreach ($chorusPj as $pj) {
                                if ($pj instanceof CommonChorusPj) {
                                    $pj->delete($connexion);
                                    $logger->info('[echange_id = '.$echangeChorus->getId().' | contrat_id = '.$contrat->getIdContratTitulaire().' | org = '.$echangeChorus->getOrganisme().' | chorus_pj_id = '.$pj->getId().'] ChorusPj supprime');
                                }
                            }
                        }

                        //t_chorus_tier_rejete
                        CommonTChorusTierRejeteQuery::create()->filterByIdEchange($echangeChorus->getId())->delete($connexion);
                        $logger->info('[echange_id = '.$echangeChorus->getId().' | contrat_id = '.$contrat->getIdContratTitulaire().' | org = '.$echangeChorus->getOrganisme().'] t_chorus_tier_rejete supprime');

                        //Chorus_echange, t_chorus_fiche_modificative, t_chorus_fiche_modificative_pj (en DELETE CASCADE)
                        $echangeChorus->delete($connexion);
                    }
                }
            }
        } catch (Exception $e) {
            $logger->info('[contrat_id = '.$contrat->getIdContratTitulaire().' | org = '.$contrat->getOrganisme().'] Erreur suppression ChorusEchange et objets lies: '.$e->getMessage());
            throw $e;
        }
    }

    /**
     * Permet de verifier les conditions d'acces a la page des details de l'echange CHorus.
     *
     * @param $statut
     * @param $retour
     *
     * @return bool : true si page details accessible, false sinon
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since 2017-roadmap
     *
     * @copyright Atexo 2017
     */
    public static function VerifierAccesChorusDetailsEchange($statut, $retour)
    {
        if ($statut == Atexo_Config::getParameter('STATUS_ECHANGE_CHORUS_BROUILLON') || $statut == Atexo_Config::getParameter('STATUT_ECHANGE_CHORUS_REJETE_PROBLEME_FOURNISSEUR')
            || $statut == Atexo_Config::getParameter('STATUT_ECHANGE_CHORUS_REJETE_PROBLEME_TECHNIQUE') || $statut == Atexo_Config::getParameter('STATUS_ECHANGE_CHORUS_NON_ENVOYER')) {
            return true;
        }
        if ($statut == Atexo_Config::getParameter('STATUS_ECHANGE_CHORUS_ENVOYER')
            && ($retour == Atexo_Config::getParameter('CHORUS_RETOUR_REJETE') || $retour == Atexo_Config::getParameter('CHORUS_RETOUR_IRRECEVABLE'))) {
            return true;
        }

        return false;
    }

    /**
     * Permet de recuperer les organisation d'achat.
     *
     * @param string $organisme l'organisme
     * @param int    $actif
     *
     * @return array CommonChorusOrganisationAchat
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2017-develop
     *
     * @copyright Atexo 2017
     */
    public function retreiveOrganisationAchatByOrganisme($organisme, $actif = 1)
    {
        $organismastionQuery = new CommonChorusOrganisationAchatQuery();

        return $organismastionQuery->getOrganisationAchatByOrganisme($organisme, $actif);
    }

    public function retrieveFicheModificativeAGenerer($dateDebut, $dateFin, $connexion = null)
    {
        if (!$connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
        $ficheModifcativeQuery = new CommonTChorusFicheModificativeQuery();
        $resultat = $ficheModifcativeQuery->getFichesModificativeAGenerer($dateDebut, $dateFin, $connexion);

        return $resultat;
    }

    public function retrieveFicheModificativeByIdEchange($idEchange, $organisme, $connexion = null)
    {
        if (!$connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
        $ficheModifcativeQuery = new CommonTChorusFicheModificativeQuery();

        return $ficheModifcativeQuery->getFichesModificativeByIdEchange($idEchange, $organisme, $connexion);
    }

    /**
     * permet de generer le document de la fiche Modificative.
     *
     * @param CommonTChorusFicheModificative $ficheModificative
     * @param CommonTContratTitulaire        $contrat
     * @param PropelPDO                      $connexion
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function generatePdfFicheModificative($ficheModificative, $contrat, $connexion)
    {
        $idBlobFicheModificative = null;
        if ($contrat instanceof CommonTContratTitulaire) {
            (new Atexo_Chorus_Util())->loggerInfos(" debut generation du pdf de la Fiche Modificative \n");
            $htmlContent = self::generateHtmlFicheModificative($ficheModificative, $contrat);
            $nomFichier = (new Atexo_Chorus_Util())->getNomPdfActeModificatif(false, $contrat, true);

            //Generation pdf acte modificatif. Parametre "CHORUS_NUM_OF_ATTEMPTS_GEN_ACTE_MODIFICATIF" : nbr de tentatives de generation de l'acte modificatif
            $attempts = 0;
            do {
                try {
                    $idBlobFicheModificative = self::genererPdfFromHtml($htmlContent, $nomFichier, $contrat->getIdContratTitulaire());
                    $fileActeModifOnBlob = Atexo_Config::getParameter('BASE_ROOT_DIR').$contrat->getOrganisme().Atexo_Config::getParameter('FILES_DIR').$idBlobFicheModificative.'-0';
                    if (empty($idBlobFicheModificative) || !is_file($fileActeModifOnBlob) || false === file_get_contents($fileActeModifOnBlob) || empty(file_get_contents($fileActeModifOnBlob))) {
                        ++$attempts;
                        //sleep(1);
                        continue;
                    }
                } catch (\Exception $e) {
                    ++$attempts;
                    //sleep(1);
                    continue;
                }
                break;
            } while ($attempts < Atexo_Config::getParameter('CHORUS_NUM_OF_ATTEMPTS_GEN_ACTE_MODIFICATIF'));

            if (!empty($idBlobFicheModificative)) {
                (new Atexo_Chorus_Util())->loggerInfos(" MAJ Fiche Modificative, ajout IdBlob ( = $idBlobFicheModificative) du fichier pdf genere \n");
                try {
                    $ficheModificative->setIdBlobFicheModificative($idBlobFicheModificative);
                    $ficheModificative->save($connexion);
                } catch (\Exception $e) {
                    $msgErreur = "Erreur lors du MAJ de la Fiche Modificative. Erreur \n".$e->getMessage()."\n".'Fiche Modificative Objet : '.print_r($ficheModificative, true);
                    Prado::log($msgErreur, TLogger::ERROR, 'ERROR');
                    (new Atexo_Chorus_Util())->loggerErreur($msgErreur);
                }
            } else {
                (new Atexo_Chorus_Util())->loggerInfos("La fiche modificative n'a pas ete mise a jour: car la generation a echoue. Pour plus de details voir le fichier 'BASE_ROOT_DIR/logs/interfacePlaceChorus/aaaa/mm/jj/interfacePlaceChorus.log'");
            }
        } else {
            (new Atexo_Chorus_Util())->loggerInfos("Impossible de generer la fiche modificative. Le contrat n'est pas une instance de CommonTContratTitulaire : ".print_r($contrat, true));
        }
    }

    /**
     * permet de generer le contenu HTML de la ficheModificative.
     *
     * @param CommonTChorusFicheModificative $ficheModificative
     * @param CommonTContratTitulaire        $contrat
     *
     * @return string $content
     *
     * @author    Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version   1.0
     *
     * @since     2015-place
     *
     * @copyright Atexo 2015
     */
    public function generateHtmlFicheModificative($ficheModificative, $contrat)
    {
        $visaAccfText = null;
        $visaPrefetText = null;
        $arrayFiles = [];
        try {
            if ($ficheModificative instanceof CommonTChorusFicheModificative) {
                (new Atexo_Chorus_Util())->loggerInfos(" creation du contenu html pour la Fiche Modificative \n");
                $modelhtmlFicheMofificative = Atexo_Config::getParameter('PATH_MODELE_CHORUS_FICH_MODIFICATIF');
                $content = file_get_contents($modelhtmlFicheMofificative);
                if ($contrat instanceof CommonTContratTitulaire) {
                    $content = str_replace('NUMERO_CONTRAT', $contrat->getReferenceLibre(), $content);
                    $content = str_replace('NUMERO_EJ', (new Atexo_Chorus_Util())->retournLastNumeroMarcheDecision($contrat->getNumEJ()), $content);
                    $content = str_replace('MONTANT_MARCHE', $contrat->getMontantContrat(), $content);
                }
                $idTypeModification = $ficheModificative->getTypeModification();
                $refTypeFournisseur = Atexo_Config::getParameter('REFERENTIEL_TYPE_MODIFICATION');
                $referentielTypeModification = CommonValeurReferentielQuery::create()->filterById($idTypeModification)->filterByIdReferentiel($refTypeFournisseur)->findOne();
                if ($referentielTypeModification instanceof CommonValeurReferentiel) {
                    $libelleTypeModification = Atexo_Util::toUtf8($referentielTypeModification->getLibelleValeurReferentielFr());
                    $content = str_replace('TYPE_MODIFICATION', $libelleTypeModification, $content);
                }
                $content = str_replace('DATE_PREVISIONNELLE_NOTIFICATION', $ficheModificative->getDatePrevueNotification(), $content);
                $content = str_replace('DATE_FIN_MARCHE', $ficheModificative->getDateFinMarche(), $content);
                if ($ficheModificative->getDateFinMarcheModifie()) {
                    $content = str_replace('BOOL_DATE_FIN_MODIFIE', Prado::localize('DEFINE_OUI'), $content);
                    $content = str_replace('DATE_FIN_MODIFIE', $ficheModificative->getDateFinMarcheModifie(), $content);
                } else {
                    $content = str_replace('BOOL_DATE_FIN_MODIFIE', Prado::localize('TEXT_NON'), $content);
                    $content = str_replace('DATE_FIN_MODIFIE', '-', $content);
                }
                $content = str_replace('MONTANT_ACTE', $ficheModificative->getMontantActe(), $content);
                if ($ficheModificative->getTauxTva()) {
                    $content = str_replace('TVA_MODIFIE', $ficheModificative->getTauxTva(), $content);
                } else {
                    $content = str_replace('TVA_MODIFIE', '-', $content);
                }
                $nbrFournisseurs = $ficheModificative->getNombreFournisseurCotraitant();
                $content = str_replace('NOMBRE_FOURNISSEURS', $nbrFournisseurs, $content);
                $listeFournisseurs = '';
                if ($nbrFournisseurs) {
                    $listeFournisseurs = self::getCodeListeFournisseurs($ficheModificative);
                }
                $content = str_replace('CODE_LISTE_NOUVEAUX_FOURNISSEURS', $listeFournisseurs, $content);
                $visaAccfValue = $ficheModificative->getVisaAccf();
                switch ($visaAccfValue) {
                    case '0':
                        $visaAccfText = Atexo_Util::toUtf8(Prado::localize('NON_RENSEIGNE'));
                        break;
                    case '1':
                        $visaAccfText = Prado::localize('DEFINE_OUI');
                        break;
                    case '2':
                        $visaAccfText = Prado::localize('TEXT_NON');
                        break;
                }
                $content = str_replace('VISA_AACF', $visaAccfText, $content);

                $visaPrefetValue = $ficheModificative->getVisaPrefet();
                switch ($visaPrefetValue) {
                    case '0':
                        $visaPrefetText = Atexo_Util::toUtf8(Prado::localize('NON_RENSEIGNE'));
                        break;
                    case '1':
                        $visaPrefetText = Prado::localize('DEFINE_OUI');
                        break;
                    case '2':
                        $visaPrefetText = Prado::localize('TEXT_NON');
                        break;
                }
                $content = str_replace('VISA_PREFET', $visaPrefetText, $content);
                $content = str_replace('VALUE_REMARQUE', Atexo_Util::toUtf8($ficheModificative->getRemarque()), $content);
                $Pjs = $ficheModificative->getCommonTChorusFicheModificativePjs();
                if ($Pjs) {
                    foreach ($Pjs as $unePj) {
                        $arrayFiles[] = $unePj->getNomFichier();
                    }
                }
                $listeNameFile = '- '.Atexo_Util::toUtf8(implode(',<br/>- ', $arrayFiles));
                $content = str_replace('LISTE_PJ', $listeNameFile, $content);

                return $content;
            }
        } catch (Exception $e) {
            $msgErreur = "Erreur lors de la generation de l'html Fiche Modificative \n".$e->getMessage()."\n";
            Prado::log($msgErreur, TLogger::ERROR, 'ERROR');
            (new Atexo_Chorus_Util())->loggerErreur($msgErreur);
        }
    }

    /**
     * permet de generer le PDF de la fiche modificative à partir du contenu HTML.
     *
     * @param CommonTChorusFicheModificative $htmlContent
     * @param string                         $nomFichier
     * @param int                            $idContrat   : identifiant technique du contrat
     *
     * @return int $idBlobFicheModificative
     *
     * @author    Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version   1.0
     *
     * @since     2015-place
     *
     * @copyright Atexo 2015
     */
    public function genererPdfFromHtml($htmlContent, $nomFichier, $idContrat)
    {
        try {
            (new Atexo_Chorus_Util())->loggerInfos(" generation du pdf de la Fiche Modificative a partir du contenu HTML \n");
            $idGen = $idContrat.session_id();
            $nomFichierPdf = $nomFichier.'.pdf';
            $tmpHtmlFile = Atexo_Config::getParameter('COMMON_TMP').DIRECTORY_SEPARATOR.$idGen.'.html';
            $tmpPdfFile = Atexo_Config::getParameter('COMMON_TMP').DIRECTORY_SEPARATOR.$idGen.'.pdf';
            Atexo_Util::write_file($tmpHtmlFile, $htmlContent);
            $cmd = Atexo_Config::getParameter('PATH_BIN_WKHTMLPDF').' '.$tmpHtmlFile.' '.$tmpPdfFile.'  > /tmp/logPrintFile 2>&1 ';
            system($cmd);

            if (is_file($tmpPdfFile)) {
                $atexoBlob = new Atexo_Blob();
                $idBlobFicheModificative = $atexoBlob->insert_blob($nomFichierPdf, $tmpPdfFile, Atexo_CurrentUser::getOrganismAcronym());
            } else {
                $idBlobFicheModificative = null;
                $contenuLogGenerationPdf = file_get_contents('/tmp/logPrintFile');
                (new Atexo_Chorus_Util())->loggerErreur('Erreur: la generation de la fiche modificative a echoue.'.PHP_EOL."Details erreur : $contenuLogGenerationPdf ".PHP_EOL."Commande executee = '$cmd'");
            }

            return $idBlobFicheModificative;
        } catch (Exception $e) {
            $msgErreur = "Erreur lors de la generation du pdf de la Fiche Modificative \n".$e->getMessage()."\n";
            Prado::log($msgErreur, TLogger::ERROR, 'ERROR');
            (new Atexo_Chorus_Util())->loggerErreur($msgErreur);
        }
    }

    public function retreiveEchangesByUiidDecisionContractExterne(
        ?string $organisme,
        string $uuidExterne,
        bool $inOrdre = false
    ): array {
        if (!$organisme) {
            $organisme = Atexo_CurrentUser::getOrganismAcronym();
        }

        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB')
            .Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonChorusEchangePeer::UUID_EXTERNE_EXEC, $uuidExterne);
        $c->add(CommonChorusEchangePeer::ORGANISME, $organisme);

        if ($inOrdre) {
            $c->addDescendingOrderByColumn(CommonChorusEchangePeer::ID);
        }

        $echanges = CommonChorusEchangePeer::doSelect($c, $connexion);

        if ($echanges) {
            return $echanges;
        }

        return [];
    }
}
