<?php

namespace Application\Service\Atexo\Chorus;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonChorusEchange;
use Application\Propel\Mpe\CommonChorusFicheNavette;
use Application\Propel\Mpe\CommonDCE;
use Application\Propel\Mpe\CommonDocumentExterne;
use Application\Propel\Mpe\CommonDocumentExternePeer;
use Application\Propel\Mpe\CommonFichierEnveloppe;
use Application\Propel\Mpe\CommonFichierEnveloppePeer;
use Application\Propel\Mpe\CommonTChorusFicheModificative;
use Application\Propel\Mpe\CommonTChorusFicheModificativeQuery;
use Application\Propel\Mpe\CommonTConsLotContrat;
use Application\Propel\Mpe\CommonTContratTitulaire;
use Application\Propel\Mpe\CommonTypeDecision;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_CategorieLot;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_Zip;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Contrat;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Dce;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Decision;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Responses;
use Application\Service\Atexo\Helios\Atexo_Helios_PiecesAttributaires;
use AtexoCrypto\Dto\Fichier;
use DomDocument;
use Exception;

class Atexo_Chorus_GestionFichierChorus
{
    public function getFileDceForTransmitionChorus($echange, $contrat, $acronymeOrganisme, $getPathFichier = true, &$domDocument = null, &$engagementJuridique = null, &$erreursFluxChorus = null, $logger = null)
    {
        try {
            if (empty($logger)) {
                $logger = Atexo_LoggerManager::getLogger('chorus');
            }
            if ($echange instanceof CommonChorusEchange && is_array($contrat)) {
                $filesReturn = [];
                if ($echange->getDceItems()) {
                    $atexoBlob = new Atexo_Blob();

                    $itemsDce = explode(';', $echange->getDceItems());
                    if (is_array($itemsDce) && count($itemsDce) > 0) {
                        $consultationId = $contrat['idConsultation'];
                        $dce = (new Atexo_Consultation_Dce())->getDce($consultationId, $acronymeOrganisme);
                        if ($dce instanceof CommonDCE) {
                            $tmpPath = Atexo_Config::getParameter('COMMON_TMP').'/'.session_name().session_id().time().date('Y_m_d_h_i_s').$dce->getDce();

                            if (!is_dir($tmpPath)) {
                                system('mkdir '.escapeshellarg($tmpPath));
                            }

                            $fileContent = $atexoBlob->return_blob_to_client($dce->getDce(), $acronymeOrganisme);

                            $fp = fopen($tmpPath.'/dce.zip', 'w');
                            fwrite($fp, $fileContent);
                            fclose($fp);

                            $arrayZipArboresence = Atexo_Zip::getOrderedFilesForZip($tmpPath.'/dce.zip', false);

                            $array = [];
                            foreach ($arrayZipArboresence as $arboresence) {
                                $array = array_merge($array, $arboresence);
                            }

                            foreach ($itemsDce as $item) {
                                if ($item) {
                                    $fileContent = '';
                                    foreach ($array as $files) {
                                        if ($item == $files['index']) {
                                            $nomFichier = $files['name'];
                                            $type = $files['type'];
                                            break;
                                        }
                                    }
                                    if ('file' == $type) {
                                        $filetmp = uniqid().$item;
                                        if (!is_dir($tmpPath.'/'.$filetmp)) {
                                            system('mkdir '.escapeshellarg($tmpPath.'/'.$filetmp));
                                        }

                                        $fileName = Atexo_Zip::extractZippedFile($tmpPath.'/dce.zip', $tmpPath.'/'.$filetmp, $item, true, false);

                                        $contenuZip = null;
                                        if ($fileName) {
                                            if (filesize($fileName) <= Atexo_Config::getParameter('CHORUS_MAX_UPLOAD_FILE_SIZE')) {
                                                (new Atexo_Zip())->addFileToZip($fileName, $fileName.'.zip');

                                                $contenuZip = file_get_contents($fileName.'.zip');
                                                if ($getPathFichier) {
                                                    $filesReturn[] = [$nomFichier, $fileName];
                                                } else {
                                                    unlink($fileName);
                                                }

                                                unlink($fileName.'.zip');
                                            } else {
                                                unlink($fileName);

                                                return false;
                                            }
                                        }

                                        if ($contenuZip && !$getPathFichier) {
                                            $pj = $domDocument->createElement('PJ', '');
                                            $engagementJuridique->appendChild($pj);
                                            $nomFichier = (new Atexo_Chorus_Util())->convertToAscii(utf8_encode($nomFichier));
                                            $nomFichier = Atexo_Util::OterAccents($nomFichier);
                                            $nomFichier = Atexo_Util::clean($nomFichier);

                                            $typeFichier = substr(strrchr($nomFichier, '.'), 1);

                                            if ('' == $typeFichier || ' ' == $typeFichier) {
                                                $typeFichier = 'txt';
                                            }
                                            $data = explode('#', str_replace(' ', '', Atexo_Config::getParameter('EXTENTION_PIECES_JOINTES_CHORUS')));
                                            if (!in_array(strtoupper($typeFichier), $data)) {
                                                return false;
                                            }
                                            if ($echange->isTypeFluxFen211($contrat)) {
                                                $typeFichier = strtoupper($typeFichier);
                                            }
                                            $type = $domDocument->createElement('Type', $typeFichier);
                                            $pj->appendChild($type);

                                            $descriptionFichier = substr(substr($nomFichier, 0, -4), 0, 64);
                                            if ('' == $descriptionFichier || ' ' == $descriptionFichier) {
                                                $descriptionFichier = 'NON_RENSEIGNE';
                                            }
                                            $description = $domDocument->createElement('Description', $descriptionFichier);
                                            $pj->appendChild($description);
                                            $data = $domDocument->createElement('Data', base64_encode($contenuZip));
                                            $pj->appendChild($data);
                                        } else {
                                            if (empty($contenuZip)) {
                                                $erreurMessage = 'Le contenu du fichier DCE est vide ';
                                            } else {
                                                $erreurMessage = "Le fichier DCE n'est pas valide ";
                                            }
                                            $erreurMessage .= ', Echange [Id = '.$echange->getId().' , Organisme = '.$acronymeOrganisme.', contratId = '.$contrat['id'].' ]'.PHP_EOL.'Fichier: '.PHP_EOL.print_r($dce, true);
                                            $logger->error($erreurMessage);
                                            $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if ($getPathFichier) {
                    return $filesReturn;
                }
            } else {
                $erreurMessage = 'Erreur Objet invalide -  Echange : '.print_r($echange, true)." , \n Contrat : ".print_r($contrat, true)." ] , \n Methode = Atexo_Chorus_GestionFichierChorus::getFileDceForTransmitionChorus";
                $logger->error($erreurMessage);
                $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;
            }
        } catch (Exception $ex) {
            $erreurMessage = 'Erreur lors de la recuperation des pieces du DCE pour le flux vers CHORUS, Echange [Id = '.$echange->getId().' , Org = '.$acronymeOrganisme.' , ContratId = '.$contrat['uuid'].' ] , Methode = Atexo_Chorus_GestionFichierChorus::getFileDceForTransmitionChorus , Exception :[ '.$ex->getMessage().' ]';
            $logger->error($erreurMessage);
            $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;
        }
    }

    public function getFileEnvAeForTransmitionChorus($echange, $acronymeOrganisme, $getPathFichier = true, &$domDocument = null, &$engagementJuridique = null, &$erreursFluxChorus = null, $logger = null, $contrat = null)
    {
        try {
            if (empty($logger)) {
                $logger = Atexo_LoggerManager::getLogger('chorus');
            }
            if ($echange instanceof CommonChorusEchange) {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                $filesReturn = [];
                if ($echange->getIdsEnvAe()) {
                    $atexoBlob = new Atexo_Blob();
                    $idsActesEngagement = explode(';', $echange->getIdsEnvAe());
                    if (is_array($idsActesEngagement) && count($idsActesEngagement) > 0) {
                        foreach ($idsActesEngagement as $idAe) {
                            $fichierEnveloppe = CommonFichierEnveloppePeer::retrieveByPK($idAe, $acronymeOrganisme, $connexion);
                            if ($fichierEnveloppe instanceof CommonFichierEnveloppe && $fichierEnveloppe->getIdBlob()) {
                                $nomFichier = $fichierEnveloppe->getNomFichier();
                                $nomFichier = (new Atexo_Chorus_Util())->convertToAscii($nomFichier);

                                $nomFichier = Atexo_Util::OterAccents($nomFichier);
                                $nomFichier = Atexo_Util::clean($nomFichier);
                                $typeFichier = substr(strrchr($nomFichier, '.'), 1);
                                if ('' == $typeFichier || ' ' == $typeFichier) {
                                    $typeFichier = 'txt';
                                }
                                $descriptionFichier = substr(substr($nomFichier, 0, -4), 0, 64);
                                if ('' == $descriptionFichier || ' ' == $descriptionFichier) {
                                    $descriptionFichier = 'NON_RENSEIGNE';
                                }
                                $tmpPath = Atexo_Config::getParameter('COMMON_TMP').'/'.session_name().session_id().time().date('Y_m_d_h_i_s').$idAe;
                                if (!is_dir($tmpPath)) {
                                    system('mkdir '.escapeshellarg($tmpPath));
                                }
                                $fileContent = $atexoBlob->return_blob_to_client($fichierEnveloppe->getIdBlob(), $acronymeOrganisme);

                                Atexo_Util::write_file($tmpPath.'/'.$nomFichier, $fileContent);
                                (new Atexo_Zip())->addFileToZip($tmpPath.'/'.$nomFichier, $tmpPath.'/'.$nomFichier.'zip');
                                if (filesize($tmpPath.'/'.$nomFichier) <= Atexo_Config::getParameter('CHORUS_MAX_UPLOAD_FILE_SIZE')) {
                                    $fileContent = file_get_contents($tmpPath.'/'.$nomFichier.'zip');

                                    if ($getPathFichier) {
                                        $filesReturn[] = [$nomFichier, $tmpPath.'/'.$nomFichier];
                                    } else {
                                        unlink($tmpPath.'/'.$nomFichier);
                                    }

                                    unlink($tmpPath.'/'.$nomFichier.'zip');
                                } else {
                                    unlink($tmpPath.'/'.$nomFichier);

                                    return false;
                                }

                                if ($fileContent && !$getPathFichier) {
                                    $pj = $domDocument->createElement('PJ', '');
                                    $engagementJuridique->appendChild($pj);
                                    $data = explode('#', str_replace(' ', '', Atexo_Config::getParameter('EXTENTION_PIECES_JOINTES_CHORUS')));
                                    if (!in_array(strtoupper($typeFichier), $data)) {
                                        return false;
                                    }
                                    if ($echange->isTypeFluxFen211($contrat)) {
                                        $typeFichier = strtoupper($typeFichier);
                                    }
                                    $type = $domDocument->createElement('Type', $typeFichier);
                                    $pj->appendChild($type);

                                    $description = $domDocument->createElement('Description', $descriptionFichier);
                                    $pj->appendChild($description);

                                    $data = $domDocument->createElement('Data', base64_encode($fileContent));
                                    $pj->appendChild($data);
                                } else {
                                    $erreurMessage = "Le fichier enveloppe acte d'engagement n'existe pas ou son contenu est vide , Echange [Id = ".$echange->getId().' , Organisme = '.$acronymeOrganisme.' ]'.PHP_EOL.'Fichier: '.PHP_EOL.print_r($fichierEnveloppe, true);
                                    $logger->error($erreurMessage);
                                    $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;
                                }
                            }
                        }
                    }
                }

                if ($getPathFichier) {
                    return $filesReturn;
                }
            } else {
                $erreurMessage = 'Erreur Objet invalide -  Echange : '.print_r($echange, true)." ] , \n Methode = Atexo_Chorus_GestionFichierChorus::getFileEnvAeForTransmitionChorus";
                $logger->error($erreurMessage);
                $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;
            }
        } catch (Exception $ex) {
            $erreurMessage = 'Erreur lors de la recuperation des pieces Env. AE pour le flux vers CHORUS, Echange [Id = '.$echange->getId().' , Org = '.$acronymeOrganisme.' ] , Methode = Atexo_Chorus_GestionFichierChorus::getFileEnvAeForTransmitionChorus , Exception :[ '.$ex->getMessage().' ]';
            $logger->error($erreurMessage);
            $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;
        }
    }

    public function getFileSignaceForTransmitionChorus($echange, $contrat, $acronymeOrganisme, $getPathFichier = true, &$domDocument = null, &$engagementJuridique = null, &$erreursFluxChorus = null, $logger = null)
    {
        try {
            if (empty($logger)) {
                $logger = Atexo_LoggerManager::getLogger('chorus');
            }
            if ($echange instanceof CommonChorusEchange && is_array($contrat)) {
                $filesReturn = [];
                if ($echange->getSignace()) {
                    $offre = (new Atexo_Consultation_Responses())->retrieveOffreById($contrat['idOffre'], $acronymeOrganisme);

                    $litesLots = [];
                    if (!empty($contrat['numeroLot'])) {
                        $categorieLot = (new Atexo_CategorieLot())->retrieveByOrganismeConsultationIdNumero(
                            $contrat['organisme'],
                            $contrat['idConsultation'],
                            $contrat['numeroLot']
                        );
                        if ($categorieLot) {
                            $litesLots[] = $categorieLot->getLot();
                        }
                    }
                    $consultation = (new Atexo_Consultation())->retrieveConsultation($contrat['idConsultation'], $acronymeOrganisme);
                    foreach ($litesLots as $lot) {
                        $enveloppeOffre = (new Atexo_Consultation_Responses())->retrieveEnveloppesByIdOffre($offre->getId(), $acronymeOrganisme, Atexo_Config::getParameter('TYPE_ENV_OFFRE'), $lot);
                        $enveloppeOffre = array_shift($enveloppeOffre);
                        $fichiersEnveloppe = (new Atexo_Consultation_Responses())->retrieveEnveloppeFiles($enveloppeOffre->getIdEnveloppeElectro(), $acronymeOrganisme);
                        foreach ($fichiersEnveloppe as $fichierEnveloppe) {
                            if ('ACE' == $fichierEnveloppe->getTypeFichier()) {
                                $signature = trim($fichierEnveloppe->getSignatureFichier());
                                $signatureFileName = Atexo_Config::getParameter('COMMON_TMP').'/signature_'.session_name().session_id().time().date('Y_m_d_h_i_s');
                                $typeSignature = $fichierEnveloppe->getTypeSignature();

                                $nomFichier = Atexo_Util::OterAccents($fichierEnveloppe->getCnSubject($consultation->getSignatureOffre(), $typeSignature));
                                $nomFichier = Atexo_Util::clean($nomFichier);

                                Atexo_Util::write_file($signatureFileName, $signature);
                                if (filesize($signatureFileName) <= Atexo_Config::getParameter('CHORUS_MAX_UPLOAD_FILE_SIZE')) {
                                    (new Atexo_Zip())->addFileToZip($signatureFileName, $signatureFileName.'.zip');

                                    $fileContent = file_get_contents($signatureFileName.'.zip');

                                    if ($getPathFichier) {
                                        $file = $nomFichier.Atexo_Util::getExtensionSignature($fichierEnveloppe->getSignatureFichier());
                                        $filesReturn[] = [$file, $signatureFileName];
                                    } else {
                                        unlink($signatureFileName);
                                    }

                                    unlink($signatureFileName.'.zip');
                                } else {
                                    unlink($signatureFileName);

                                    return false;
                                }

                                if ($fileContent && !$getPathFichier) {
                                    $descriptionFichier = 'Signature_titulaire_Acte_Engagement : '.$nomFichier;

                                    $descriptionFichier = substr(substr($descriptionFichier, 0, -4), 0, 64);
                                    if ('' == $descriptionFichier || ' ' == $descriptionFichier) {
                                        $descriptionFichier = 'NON_RENSEIGNE';
                                    }

                                    $pj = $domDocument->createElement('PJ', '');
                                    $engagementJuridique->appendChild($pj);
                                    $typeFichier = substr(Atexo_Util::getExtensionSignature($fichierEnveloppe->getSignatureFichier()), 1);
                                    $data = explode('#', str_replace(' ', '', Atexo_Config::getParameter('EXTENTION_PIECES_JOINTES_CHORUS')));
                                    if (!in_array(strtoupper($typeFichier), $data)) {
                                        return false;
                                    }
                                    if ($echange->isTypeFluxFen211($contrat)) {
                                        $typeFichier = strtoupper($typeFichier);
                                    }
                                    $type = $domDocument->createElement('Type', $typeFichier);
                                    $pj->appendChild($type);

                                    $description = $domDocument->createElement('Description', $descriptionFichier);
                                    $pj->appendChild($description);

                                    $data = $domDocument->createElement('Data', base64_encode($fileContent));
                                    $pj->appendChild($data);
                                } else {
                                    $erreurMessage = "Le fichier signature acte d'engagement n'existe pas ou son contenu est vide , Echange [Id = ".$echange->getId().' , Organisme = '.$acronymeOrganisme.', contratId = '.$contrat['uuid'].' ]'.PHP_EOL.'Fichier: '.print_r($fichierEnveloppe, true);
                                    $logger->error($erreurMessage);
                                    $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;
                                }
                            }
                        }
                    }
                }

                if ($getPathFichier) {
                    return $filesReturn;
                }
            } else {
                $erreurMessage = 'Erreur Objet invalide -  Echange : '.print_r($echange, true)." , \n Contrat : ".print_r($contrat, true)." ] , \n Methode = Atexo_Chorus_GestionFichierChorus::getFileSignaceForTransmitionChorus";
                $logger->error($erreurMessage);
                $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;
            }
        } catch (Exception $ex) {
            $erreurMessage = 'Erreur lors de la recuperation des pieces de signature AE pour le flux vers CHORUS, Echange [Id = '.$echange->getId().' , Org = '.$acronymeOrganisme.' , ContratId = '.$contrat['uuid'].' ] , Methode = Atexo_Chorus_GestionFichierChorus::getFileSignaceForTransmitionChorus , Exception :[ '.$ex->getMessage().' ]';
            $logger->error($erreurMessage);
            $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;
        }
    }

    public function getFileEnvForTransmitionChorus($echange, $contrat, $acronymeOrganisme, $getPathFichier = true, &$domDocument = null, &$engagementJuridique = null, &$erreursFluxChorus = null, $logger = null)
    {
        try {
            if ($echange instanceof CommonChorusEchange && is_array($contrat)) {
                $filesReturn = [];
                if ($echange->getIdsEnvItems()) {
                    $atexoBlob = new Atexo_Blob();
                    $listeEnveloppesIds = explode('#', $echange->getIdsEnvItems()); //on prend la liste des enveloppes et on les paracourt
                    if (is_array($listeEnveloppesIds) && count($listeEnveloppesIds) > 0) {
                        foreach ($listeEnveloppesIds as $enveloppeIds) {//pour chaque enveloppe on regarde ce que l'acheteur à sélectionné
                            $idsEnveloppes = [];
                            $listeIndex = [];
                            $idBlobEnregistre = null;
                            $listeIndex = explode('_', $enveloppeIds);
                            $idBlobEnregistre = $listeIndex[0];
                            $idsEnveloppes = explode(';', $listeIndex[1]);
                            if (is_array($idsEnveloppes) && count($idsEnveloppes) > 0) {
                                $litesLots = [];
                                if (!empty($contrat['numeroLot'])) {
                                    $categorieLot = (new Atexo_CategorieLot())->retrieveByOrganismeConsultationIdNumero(
                                        $contrat['organisme'],
                                        $contrat['idConsultation'],
                                        $contrat['numeroLot']
                                    );
                                    if ($categorieLot) {
                                        $litesLots[] = $categorieLot->getLot();
                                    }
                                }
                                foreach ($litesLots as $lot) {
                                    //Determination de la reference de la consultation
                                    $consultationId = $contrat['idConsultation'];
                                    /*$lotConsContrat = (new Atexo_Consultation_Contrat())->getListeConsLotsContratByIdContrat($contrat->getIdContratTitulaire());
                                    if (is_array($lotConsContrat)) {
                                        foreach ($lotConsContrat as $oneEl) {
                                            if ($oneEl instanceof CommonTConsLotContrat) {
                                                $consultationId = $oneEl->getConsultationId();
                                            }
                                        }
                                    }*/
                                    if (empty($consultationId)) {
                                        $erreurMessage = "Valeur vide pour reference consultation : refCons = '$consultationId' ".PHP_EOL.'Methode = Atexo_Chorus_GestionFichierChorus::getFileEnvForTransmitionChorus';
                                        $logger->error($erreurMessage);
                                        $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;
                                    }

                                    $idEnveloppe = (new Atexo_Consultation_Decision())->getIdEnveloppeOffreContrat($acronymeOrganisme, $consultationId, $contrat['id'], $lot);
                                    $piecesAttributaires = (new Atexo_Helios_PiecesAttributaires())->retreivePiecesAttributaires($idEnveloppe, Atexo_Config::getParameter('DEPOT_ELECTRONIQUE'), $acronymeOrganisme);
                                    foreach ($piecesAttributaires as $OnePiece) {
                                        $fileContent = '';
                                        $contenuZip = '';
                                        $idBlob = $OnePiece->getIdBlob();
                                        if ($idBlobEnregistre == $idBlob) {
                                            $tmpPath = Atexo_Config::getParameter('COMMON_TMP').'/'.session_name().session_id().time().date('Y_m_d_h_i_s').$idBlob;
                                            if (!is_dir($tmpPath)) {
                                                system('mkdir '.escapeshellarg($tmpPath));
                                            }
                                            $fileContent = $atexoBlob->return_blob_to_client($idBlob, $acronymeOrganisme);
                                            $nomPiece = Atexo_Util::OterAccents($OnePiece->getNomFichier());
                                            $fp = fopen($tmpPath.'/env'.$idBlob.$nomPiece, 'w');
                                            fwrite($fp, $fileContent);
                                            fclose($fp);
                                            $arrayZipArboresence = Atexo_Zip::getOrderedFilesForZip($tmpPath.'/env'.$idBlob.$nomPiece, false);
                                            $array = [];
                                            foreach ($arrayZipArboresence as $arboresence) {
                                                $array = array_merge($array, $arboresence);
                                            }
                                            foreach ($idsEnveloppes as $idEnv) {
                                                $nomFichier = '';
                                                $type = '';
                                                foreach ($array as $files) {
                                                    if ($idEnv == $files['index']) {
                                                        $nomFichier = $files['name'];
                                                        $type = $files['type'];
                                                    }
                                                }
                                                if ('file' == $type && $nomFichier) {
                                                    $filetmp = uniqid();
                                                    if (!is_dir($tmpPath.'/'.$filetmp)) {
                                                        system('mkdir '.escapeshellarg($tmpPath.'/'.$filetmp));
                                                    }
                                                    $fileName = Atexo_Zip::extractZippedFile($tmpPath.'/'.'env'.$idBlob.$nomPiece, $tmpPath.'/'.$filetmp, $idEnv, true, false);
                                                    if (filesize($fileName) > Atexo_Config::getParameter('CHORUS_MAX_UPLOAD_FILE_SIZE')) {
                                                        return false;
                                                    }
                                                    (new Atexo_Zip())->addFileToZip($fileName, $fileName.'.zip');
                                                    $contenuZip = file_get_contents($fileName.'.zip');
                                                    if ($getPathFichier) {
                                                        $filesReturn[] = [$nomFichier, $fileName];
                                                    } else {
                                                        unlink($fileName);
                                                    }
                                                    unlink($fileName.'.zip');
                                                    if ($contenuZip && !$getPathFichier) {
                                                        $pj = $domDocument->createElement('PJ', '');
                                                        $engagementJuridique->appendChild($pj);
                                                        $nomFichier = (new Atexo_Chorus_Util())->convertToAscii(utf8_encode($nomFichier));
                                                        $nomFichier = Atexo_Util::OterAccents($nomFichier);
                                                        $nomFichier = Atexo_Util::clean($nomFichier);

                                                        $typeFichier = substr(strrchr($nomFichier, '.'), 1);
                                                        if ('' == $typeFichier || ' ' == $typeFichier) {
                                                            $typeFichier = 'txt';
                                                        }
                                                        $data = explode('#', str_replace(' ', '', Atexo_Config::getParameter('EXTENTION_PIECES_JOINTES_CHORUS')));
                                                        if (!in_array(strtoupper($typeFichier), $data)) {
                                                            return false;
                                                        }
                                                        if ($echange->isTypeFluxFen211($contrat)) {
                                                            $typeFichier = strtoupper($typeFichier);
                                                        }
                                                        $type = $domDocument->createElement('Type', $typeFichier);
                                                        $pj->appendChild($type);
                                                        $descriptionFichier = substr(substr($nomFichier, 0, -4), 0, 64);
                                                        if ('' == $descriptionFichier || ' ' == $descriptionFichier) {
                                                            $descriptionFichier = 'NON_RENSEIGNE';
                                                        }
                                                        $description = $domDocument->createElement('Description', $descriptionFichier);
                                                        $pj->appendChild($description);
                                                        $data = $domDocument->createElement('Data', base64_encode($contenuZip));
                                                        $pj->appendChild($data);
                                                    } else {
                                                        $erreurMessage = "Le fichier enveloppe n'existe pas ou son contenu est vide , Echange [Id = ".$echange->getId().' , Organisme = '.$acronymeOrganisme.', contratId = '.$contrat['uuid'].' ]'.PHP_EOL.'Fichier: '.print_r($OnePiece, true);
                                                        $logger->error($erreurMessage);
                                                        $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if ($getPathFichier) {
                    return $filesReturn;
                }
            } else {
                $erreurMessage = 'Erreur Objet invalide -  Echange : '.print_r($echange, true)." , \n Contrat : ".print_r($contrat, true)." ] , \n Methode = Atexo_Chorus_GestionFichierChorus::getFileEnvForTransmitionChorus";
                $logger->error($erreurMessage);
                $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;
            }
        } catch (Exception $ex) {
            $erreurMessage = 'Erreur lors de la recuperation des pieces enveloppes pour le flux vers CHORUS, Echange [Id = '.$echange->getId().' , Org = '.$acronymeOrganisme.' , ContratId = '.$contrat['uuid'].' ] , Methode = Atexo_Chorus_GestionFichierChorus::getFileEnvForTransmitionChorus , Exception :[ '.$ex->getMessage().' ]';
            $logger->error($erreurMessage);
            $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;
        }
    }

    public function getFilePiecesExternesForTransmitionChorus($echange, $acronymeOrganisme, $getPathFichier = true, &$domDocument = null, &$engagementJuridique = null, &$erreursFluxChorus = null, $logger = null, $contrat = null)
    {
        try {
            if (empty($logger)) {
                $logger = Atexo_LoggerManager::getLogger('chorus');
            }
            if ($echange instanceof CommonChorusEchange) {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
                $filesReturn = [];
                if ($echange->getIdsPiecesExternes()) {
                    $atexoBlob = new Atexo_Blob();
                    $idDocExternes = explode(';', $echange->getIdsPiecesExternes());
                    if (is_array($idDocExternes) && count($idDocExternes) > 0) {
                        foreach ($idDocExternes as $idDoc) {
                            $piece = explode('_', $idDoc);
                            $idPiece = $piece[0];
                            $documentExterne = CommonDocumentExternePeer::retrieveByPK($idPiece, $acronymeOrganisme, $connexion);
                            if ($documentExterne instanceof CommonDocumentExterne) {
                                $sousPiece = $piece[1];
                                $fileContent = '';
                                $contenuZip = '';
                                $tmpPath = Atexo_Config::getParameter('COMMON_TMP').'/'.session_name().session_id().time().date('Y_m_d_h_i_s').$idDoc;
                                if (!is_dir($tmpPath)) {
                                    system('mkdir '.escapeshellarg($tmpPath));
                                }
                                $fileContent = $atexoBlob->return_blob_to_client($documentExterne->getIdBlob(), $acronymeOrganisme);
                                $fp = fopen($tmpPath.'/piecesExternes.zip', 'w');
                                fwrite($fp, $fileContent);
                                fclose($fp);
                                if ($sousPiece) {
                                    $arrayZipArboresence = [];
                                    $arrayZipArboresence = Atexo_Zip::getOrderedFilesForZip($tmpPath.'/piecesExternes.zip', false);
                                    $fileName = '';
                                    $type = '';
                                    foreach ($arrayZipArboresence as $key => $arboresence) {
                                        foreach ($arboresence as $index => $file) {
                                            if ($file['index'] == $sousPiece) {
                                                $fileName = $file['name'];
                                                $type = $file['type'];
                                            }
                                        }
                                    }
                                    if ('file' == $type && $fileName) {
                                        $filetmp = uniqid().$idDoc;
                                        if (!is_dir($tmpPath.'/'.$filetmp)) {
                                            system('mkdir '.escapeshellarg($tmpPath.'/'.$filetmp));
                                        }
                                        $nomFichier = Atexo_Zip::extractZippedFile($tmpPath.'/'.'piecesExternes.zip', $tmpPath.'/'.$filetmp, $sousPiece, true, false);
                                        (new Atexo_Zip())->addFileToZip($nomFichier, $nomFichier.'.zip');
                                        if (filesize($nomFichier.'.zip') > Atexo_Config::getParameter('CHORUS_MAX_UPLOAD_FILE_SIZE')) {
                                            unlink($nomFichier);
                                            unlink($nomFichier.'.zip');

                                            return false;
                                        }
                                        $contenuZip = file_get_contents($nomFichier.'.zip');
                                        if ($getPathFichier) {
                                            $filesReturn[] = [$fileName, $nomFichier];
                                        } else {
                                            unlink($nomFichier);
                                        }
                                        unlink($nomFichier.'.zip');
                                    }
                                } else {
                                    $fileName = $documentExterne->getNom();
                                    $fileName = Atexo_Util::OterAccents($fileName);
                                    $fileName = Atexo_Util::clean($fileName);
                                    $fileContent = $atexoBlob->return_blob_to_client($documentExterne->getIdBlob(), $acronymeOrganisme);
                                    $fp = fopen($tmpPath.'/'.$fileName, 'w');
                                    fwrite($fp, $fileContent);
                                    fclose($fp);
                                    (new Atexo_Zip())->addFileToZip($tmpPath.'/'.$fileName, $tmpPath.'/'.$fileName.'.zip');
                                    if (filesize($tmpPath.'/'.$fileName.'.zip') > Atexo_Config::getParameter('CHORUS_MAX_UPLOAD_FILE_SIZE')) {
                                        unlink($tmpPath.'/'.$fileName);
                                        unlink($tmpPath.'/'.$fileName.'.zip');

                                        return false;
                                    }
                                    $contenuZip = file_get_contents($tmpPath.'/'.$fileName.'.zip');
                                    if ($getPathFichier) {
                                        $filesReturn[] = [$fileName, $tmpPath.'/'.$fileName];
                                    }
                                }
                                if ($contenuZip && !$getPathFichier) {
                                    $pj = $domDocument->createElement('PJ', '');
                                    $engagementJuridique->appendChild($pj);
                                    $fileName = Atexo_Util::OterAccents($fileName);
                                    $fileName = Atexo_Util::clean($fileName);
                                    $typeFichier = substr(strrchr($fileName, '.'), 1);
                                    if ('' == $typeFichier || ' ' == $typeFichier) {
                                        $typeFichier = 'txt';
                                    }
                                    $data = explode('#', str_replace(' ', '', Atexo_Config::getParameter('EXTENTION_PIECES_JOINTES_CHORUS')));
                                    if (!in_array(strtoupper($typeFichier), $data)) {
                                        return false;
                                    }
                                    if ($echange->isTypeFluxFen211($contrat)) {
                                        $typeFichier = strtoupper($typeFichier);
                                    }
                                    $type = $domDocument->createElement('Type', $typeFichier);
                                    $pj->appendChild($type);
                                    $descriptionFichier = substr(substr($fileName, 0, -4), 0, 64);
                                    if ('' == $descriptionFichier || ' ' == $descriptionFichier) {
                                        $descriptionFichier = 'NON_RENSEIGNE';
                                    }
                                    $description = $domDocument->createElement('Description', $descriptionFichier);
                                    $pj->appendChild($description);
                                    $data = $domDocument->createElement('Data', (base64_encode($contenuZip)));
                                    $pj->appendChild($data);
                                } else {
                                    $erreurMessage = "Le fichier piece externe n'existe pas ou son contenu est vide , Echange [Id = ".$echange->getId().' , Organisme = '.$acronymeOrganisme.' ]'.PHP_EOL.'Fichier: '.print_r($documentExterne, true);
                                    $logger->error($erreurMessage);
                                    $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;
                                }
                            }
                        }
                    }
                }
                if ($getPathFichier) {
                    return $filesReturn;
                }
            } else {
                $erreurMessage = 'Erreur Objet invalide -  Echange : '.print_r($echange, true)." ] , \n Methode = Atexo_Chorus_GestionFichierChorus::getFilePiecesExternesForTransmitionChorus";
                $logger->error($erreurMessage);
                $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;
            }
        } catch (Exception $ex) {
            $erreurMessage = 'Erreur lors de la recuperation des pieces externes pour le flux vers CHORUS, Echange [Id = '.$echange->getId().' , Org = '.$acronymeOrganisme.' ] , Methode = Atexo_Chorus_GestionFichierChorus::getFilePiecesExternesForTransmitionChorus , Exception :[ '.$ex->getMessage().' ]';
            $logger->error($erreurMessage);
            $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;
        }
    }

    public function getFilePiecesJointesForTransmitionChorus($echange, $acronymeOrganisme, $getPathFichier = true, &$domDocument = null, &$engagementJuridique = null, &$erreursFluxChorus = null, $logger = null, $contrat = null)
    {
        try {
            if (empty($logger)) {
                $logger = Atexo_LoggerManager::getLogger('chorus');
            }
            if ($echange instanceof CommonChorusEchange) {
                $filesReturn = [];
                $piecesJointes = (new Atexo_Chorus_Echange())->retreivePiecesExternes($echange->getId(), $acronymeOrganisme);
                if (is_array($piecesJointes) && count($piecesJointes) > 0) {
                    $atexoBlob = new Atexo_Blob();
                    foreach ($piecesJointes as $pieceJointe) {
                        $fileContent = $atexoBlob->return_blob_to_client($pieceJointe->getFichier(), $acronymeOrganisme);
                        $nomPiece = $pieceJointe->getNomFichier();
                        $nomPiece = (new Atexo_Chorus_Util())->convertToAscii($nomPiece);

                        $nomPiece = Atexo_Util::OterAccents($nomPiece);
                        $nomPiece = Atexo_Util::clean($nomPiece);

                        Atexo_Util::write_file(Atexo_Config::getParameter('COMMON_TMP').'/'.$nomPiece, $fileContent);
                        (new Atexo_Zip())->addFileToZip(Atexo_Config::getParameter('COMMON_TMP').'/'.$nomPiece, Atexo_Config::getParameter('COMMON_TMP').'/'.$nomPiece.'.zip');
                        if (filesize(Atexo_Config::getParameter('COMMON_TMP').'/'.$nomPiece.'.zip') > Atexo_Config::getParameter('CHORUS_MAX_UPLOAD_FILE_SIZE')) {
                            unlink(Atexo_Config::getParameter('COMMON_TMP').'/'.$nomPiece);
                            unlink(Atexo_Config::getParameter('COMMON_TMP').'/'.$nomPiece.'.zip');

                            return false;
                        }

                        $fileContent = file_get_contents(Atexo_Config::getParameter('COMMON_TMP').'/'.$nomPiece.'.zip');

                        if ($getPathFichier) {
                            $filesReturn[] = [$nomPiece, Atexo_Config::getParameter('COMMON_TMP').'/'.$nomPiece];
                        } else {
                            unlink(Atexo_Config::getParameter('COMMON_TMP').'/'.$nomPiece);
                        }

                        unlink(Atexo_Config::getParameter('COMMON_TMP').'/'.$nomPiece.'.zip');

                        if ($fileContent && !$getPathFichier) {
                            $pj = $domDocument->createElement('PJ', '');
                            $engagementJuridique->appendChild($pj);

                            $typeFichier = substr(strrchr($nomPiece, '.'), 1);
                            if ('' == $typeFichier || ' ' == $typeFichier) {
                                $typeFichier = 'txt';
                            }
                            $data = explode('#', str_replace(' ', '', Atexo_Config::getParameter('EXTENTION_PIECES_JOINTES_CHORUS')));
                            if (!in_array(strtoupper($typeFichier), $data)) {
                                return false;
                            }
                            if ($echange->isTypeFluxFen211($contrat)) {
                                $typeFichier = strtoupper($typeFichier);
                            }
                            $type = $domDocument->createElement('Type', $typeFichier);
                            $pj->appendChild($type);

                            $descriptionFichier = substr(substr($nomPiece, 0, -4), 0, 64);
                            if ('' == $descriptionFichier || ' ' == $descriptionFichier) {
                                $descriptionFichier = 'NON_RENSEIGNE';
                            }
                            $description = $domDocument->createElement('Description', $descriptionFichier);
                            $pj->appendChild($description);
                            $data = $domDocument->createElement('Data', base64_encode($fileContent));
                            $pj->appendChild($data);
                        } else {
                            $erreurMessage = "Le fichier piece jointe n'existe pas ou son contenu est vide , Echange [Id = ".$echange->getId().' , Organisme = '.$acronymeOrganisme.' ]'.PHP_EOL.'Fichier: '.print_r($pieceJointe, true);
                            $logger->error($erreurMessage);
                            $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;
                        }
                        unset($fileContent);
                    }
                }
                if ($getPathFichier) {
                    return $filesReturn;
                }
            } else {
                $erreurMessage = 'Erreur Objet invalide -  Echange : '.print_r($echange, true)." ] , \n Methode = Atexo_Chorus_GestionFichierChorus::getFilePiecesJointesForTransmitionChorus";
                $logger->error($erreurMessage);
                $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;
            }
        } catch (Exception $ex) {
            $erreurMessage = 'Erreur lors de la recuperation des pieces jointes pour le flux vers CHORUS, Echange [Id = '.$echange->getId().' , Org = '.$acronymeOrganisme.' ] , Methode = Atexo_Chorus_GestionFichierChorus::getFilePiecesJointesForTransmitionChorus , Exception :[ '.$ex->getMessage().' ]';
            $logger->error($erreurMessage);
            $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;
        }
    }

    public function getFileNotificationForTransmitionChorus($echange, $decision, $acronymeOrganisme, $getPathFichier = true, &$domDocument = null, &$engagementJuridique = null, &$erreursFluxChorus = null, $logger = null, $contrat = null)
    {
        $tmpPath = null;
        try {
            if (empty($logger)) {
                $logger = Atexo_LoggerManager::getLogger('chorus');
            }
            if ($echange instanceof CommonChorusEchange && $decision instanceof CommonTContratTitulaire) {
                $filesReturn = [];
                if ($echange->getPiecesNotifItems()) {
                    $atexoBlob = new Atexo_Blob();
                    $itemsNotif = explode(';', $echange->getPiecesNotifItems());
                    if (is_array($itemsNotif) && count($itemsNotif) > 0) {
                        if ($decision->getIdBlobPiecesNotification()) {
                            $tmpPath = Atexo_Config::getParameter('COMMON_TMP').'/'.session_name().session_id().time().$decision->getIdBlobPiecesNotification();
                            if (!is_dir($tmpPath)) {
                                system('mkdir '.escapeshellarg($tmpPath));
                            }

                            $fileContent = $atexoBlob->return_blob_to_client($decision->getIdBlobPiecesNotification(), $acronymeOrganisme);

                            $fp = fopen($tmpPath.'/pieces_notification.zip', 'w');
                            fwrite($fp, $fileContent);
                            fclose($fp);

                            $arrayZipArboresence = Atexo_Zip::getOrderedFilesForZip($tmpPath.'/pieces_notification.zip', false);

                            $array = [];
                            foreach ($arrayZipArboresence as $arboresence) {
                                $array = array_merge($array, $arboresence);
                            }

                            foreach ($itemsNotif as $item) {
                                if ($item) {
                                    $fileContent = '';
                                    foreach ($array as $files) {
                                        if ($item == $files['index']) {
                                            $nomFichier = $files['name'];
                                            $type = $files['type'];

                                            break;
                                        }
                                    }

                                    if ('file' == $type) {
                                        $filetmp = uniqid().$item;
                                        if (!is_dir($tmpPath.'/'.$filetmp)) {
                                            system('mkdir '.escapeshellarg($tmpPath.'/'.$filetmp));
                                        }

                                        $fileName = Atexo_Zip::extractZippedFile($tmpPath.'/pieces_notification.zip', $tmpPath.'/'.$filetmp, $item, true, false);

                                        (new Atexo_Zip())->addFileToZip($fileName, $fileName.'.zip');
                                        if (filesize($fileName.'.zip') > Atexo_Config::getParameter('CHORUS_MAX_UPLOAD_FILE_SIZE')) {
                                            unlink($fileName);
                                            unlink($fileName.'.zip');

                                            return false;
                                        }
                                        $contenuZip = file_get_contents($fileName.'.zip');

                                        if ($getPathFichier) {
                                            $filesReturn[] = [$nomFichier, $fileName];
                                        } else {
                                            unlink($fileName);
                                        }

                                        unlink($fileName.'.zip');

                                        if ($contenuZip && !$getPathFichier) {
                                            $pj = $domDocument->createElement('PJ', '');
                                            $engagementJuridique->appendChild($pj);

                                            $nomFichier = Atexo_Util::OterAccents($nomFichier);
                                            $nomFichier = Atexo_Util::clean($nomFichier);

                                            $typeFichier = substr(strrchr($nomFichier, '.'), 1);
                                            if ('' == $typeFichier || ' ' == $typeFichier) {
                                                $typeFichier = 'txt';
                                            }
                                            $data = explode('#', str_replace(' ', '', Atexo_Config::getParameter('EXTENTION_PIECES_JOINTES_CHORUS')));
                                            if (!in_array(strtoupper($typeFichier), $data)) {
                                                return false;
                                            }
                                            if ($echange->isTypeFluxFen211($contrat)) {
                                                $typeFichier = strtoupper($typeFichier);
                                            }
                                            $type = $domDocument->createElement('Type', $typeFichier);
                                            $pj->appendChild($type);
                                            $descriptionFichier = substr(substr($nomFichier, 0, -4), 0, 64);
                                            if ('' == $descriptionFichier || ' ' == $descriptionFichier) {
                                                $descriptionFichier = 'NON_RENSEIGNE';
                                            }
                                            $description = $domDocument->createElement('Description', $descriptionFichier);
                                            $pj->appendChild($description);
                                            $data = $domDocument->createElement('Data', base64_encode($contenuZip));
                                            $pj->appendChild($data);
                                        } else {
                                            $erreurMessage = "Le fichier piece de notification n'existe pas ou son contenu est vide , Echange [Id = ".$echange->getId().' , Organisme = '.$acronymeOrganisme.' ]'.PHP_EOL.'Fichier: '.print_r($item, true);
                                            $logger->error($erreurMessage);
                                            $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    unlink($tmpPath.'/pieces_notification.zip');
                }
                if ($getPathFichier) {
                    return $filesReturn;
                }
            } else {
                $erreurMessage = 'Erreur Objet invalide -  Echange : '.print_r($echange, true).', Decision : '.print_r($decision, true)." ] , \n Methode = Atexo_Chorus_GestionFichierChorus::getFileNotificationForTransmitionChorus";
                $logger->error($erreurMessage);
                $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;
            }
        } catch (Exception $ex) {
            $erreurMessage = 'Erreur lors de la recuperation des pieces de notification pour le flux vers CHORUS, Echange [Id = '.$echange->getId().' , Org = '.$acronymeOrganisme.' ] , Methode = Atexo_Chorus_GestionFichierChorus::getFileNotificationForTransmitionChorus , Exception :[ '.$ex->getMessage().' ]';
            $logger->error($erreurMessage);
            $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;
        }
    }

    public function getFichierEnvoyeAChorus($echange, $decision, $organisme, $connexion)
    {
        $files = self::getFileDceForTransmitionChorus($echange, $decision, $organisme, true);
        $files = array_merge($files, self::getFileEnvAeForTransmitionChorus($echange, $organisme, true));
        $files = array_merge($files, self::getFileSignaceForTransmitionChorus($echange, $decision, $organisme, true));
        $files = array_merge($files, self::getFileEnvForTransmitionChorus($echange, $decision, $organisme, true));
        $files = array_merge($files, self::getFilePiecesExternesForTransmitionChorus($echange, $organisme, true));
        $files = array_merge($files, self::getFilePiecesJointesForTransmitionChorus($echange, $organisme, true));
        $files = array_merge($files, self::getFileNotificationForTransmitionChorus($echange, $decision, $organisme, true));
        $true = true;

        //File dume Acheteur
        $files = array_merge(
            $files,
            self::getFilePiecesDumeForTransmitionChorus(
                $echange,
                Atexo_Config::getParameter('TYPE_DUME_ACHETEUR'),
                $decision,
                $organisme,
                $true
            )
        );
        $true2 = true;

        //File dume Oe
        $files = array_merge(
            $files,
            self::getFilePiecesDumeForTransmitionChorus(
                $echange,
                Atexo_Config::getParameter('TYPE_DUME_OE'),
                $decision,
                $organisme,
                $true2
            )
        );

        return $files;
    }

    public function getFileSignEnvForTransmitionChorus($echange, $contrat, $acronymeOrganisme, $getPathFichier = true, &$domDocument = null, &$engagementJuridique = null, &$erreursFluxChorus = null, $logger = null)
    {
        try {
            if (empty($logger)) {
                $logger = Atexo_LoggerManager::getLogger('chorus');
            }
            if ($echange instanceof CommonChorusEchange && is_array($contrat)) {
                $filesReturn = [];
                if ($echange->getIdsEnvSignItems() || $echange->getIdsBlobSignEnv()) {
                    $atexoBlob = new Atexo_Blob();

                    $idsEnveloppes = [];
                    $idsEnveloppes1 = [];

                    if ($echange->getIdsBlobSignEnv()) {//Cas des traitements existants
                        $idsEnveloppes1 = explode(';', $echange->getIdsBlobSignEnv());
                    }
                    if ($echange->getIdsEnvSignItems()) {//Nouveaux cas
                        //exemple de valeurs : #4031_1#4032_1
                        $listeItems = explode('_1', $echange->getIdsEnvSignItems()); //(#4031,#4032)
                        foreach ($listeItems as $item) {
                            if ($item) {
                                $arrayExploded = explode('#', $item);
                                $idsEnveloppes = [...$idsEnveloppes, ...$arrayExploded];
                            }
                        }
                    }

                    $idsEnveloppes = [...$idsEnveloppes, ...$idsEnveloppes1];

                    if (is_array($idsEnveloppes) && count($idsEnveloppes) > 0) {
                        $litesLots = [];
                        if (!empty($contrat['numeroLot'])) {
                            $categorieLot = (new Atexo_CategorieLot())->retrieveByOrganismeConsultationIdNumero(
                                $contrat['organisme'],
                                $contrat['idConsultation'],
                                $contrat['numeroLot']
                            );
                            if ($categorieLot) {
                                $litesLots[] = $categorieLot->getLot();
                            }
                        }
                        foreach ($litesLots as $lot) {
                            $idEnveloppe = (new Atexo_Consultation_Decision())->getIdEnveloppeOffreContrat($acronymeOrganisme, $contrat['idConsultation'], $contrat['id'], $lot);
                            $piecesAttributaires = (new Atexo_Helios_PiecesAttributaires())->retreivePiecesAttributaires($idEnveloppe, Atexo_Config::getParameter('DEPOT_ELECTRONIQUE'), $acronymeOrganisme);

                            foreach ($piecesAttributaires as $OnePiece) {
                                $fileContent = '';
                                $contenuZip = '';

                                if ('ACE' != $OnePiece->getTypeFichier() && 'AE' != $OnePiece->getTypeFichier()
                                    && ('.xml' == Atexo_Util::getExtensionSignature($OnePiece->getSignatureFichier())
                                        || '.p7s' == Atexo_Util::getExtensionSignature($OnePiece->getSignatureFichier()))
                                ) {
                                    $idBlob = $OnePiece->getIdBlobSignature();

                                    $tmpPath = Atexo_Config::getParameter('COMMON_TMP').'/'.session_name().session_id().time().date('Y_m_d_h_i_s').$idBlob;
                                    if (!is_dir($tmpPath)) {
                                        system('mkdir '.escapeshellarg($tmpPath));
                                    }

                                    $fileContent = $atexoBlob->return_blob_to_client($idBlob, $acronymeOrganisme);

                                    $nomPiece = $OnePiece->getNomFichier().' - Signature 1'.Atexo_Util::getExtensionSignature($OnePiece->getSignatureFichier());
                                    $nomFichier = (new Atexo_Chorus_Util())->convertToAscii(utf8_encode($nomPiece));
                                    $nomFichier = Atexo_Util::OterAccents($nomFichier);
                                    $nomFichier = Atexo_Util::clean($nomFichier);
                                    $nomPiece = $nomFichier;
                                    $nomPiece = str_replace('-', '_', str_replace(' ', '_', $nomPiece));

                                    $fileName = $tmpPath.'/envSign'.$idBlob.$nomPiece.'.zip';
                                    $fp = fopen($fileName, 'w');
                                    fwrite($fp, $fileContent);
                                    fclose($fp);
                                    if (filesize($fileName) > Atexo_Config::getParameter('CHORUS_MAX_UPLOAD_FILE_SIZE')) {
                                        unlink($fileName);

                                        return false;
                                    }
                                    $contenuZip = file_get_contents($fileName);

                                    foreach ($idsEnveloppes as $uneEnv) {
                                        if ($uneEnv == $idBlob) {
                                            if ($contenuZip) {
                                                $pj = $domDocument->createElement('PJ', '');
                                                $engagementJuridique->appendChild($pj);
                                                $typeFichier = 'zip';
                                                $data = explode('#', str_replace(' ', '', Atexo_Config::getParameter('EXTENTION_PIECES_JOINTES_CHORUS')));
                                                if (!in_array(strtoupper($typeFichier), $data)) {
                                                    return false;
                                                }
                                                if ($echange->isTypeFluxFen211($contrat)) {
                                                    $typeFichier = strtoupper($typeFichier);
                                                }
                                                $type = $domDocument->createElement('Type', $typeFichier);
                                                $pj->appendChild($type);

                                                $descriptionFichier = substr(substr($nomFichier, 0, -4), 0, 64);
                                                if ('' == $descriptionFichier || ' ' == $descriptionFichier) {
                                                    $descriptionFichier = 'NON_RENSEIGNE';
                                                }

                                                $description = $domDocument->createElement('Description', $descriptionFichier);
                                                $pj->appendChild($description);

                                                $data = $domDocument->createElement('Data', base64_encode($contenuZip));
                                                $pj->appendChild($data);
                                            } else {
                                                $erreurMessage = "Le fichier signature enveloppe n'existe pas ou son contenu est vide , Echange [Id = ".$echange->getId().' , Organisme = '.$acronymeOrganisme.', contratId = '.$contrat['uuid'].' ]'.PHP_EOL.'Fichier: '.print_r($OnePiece, true);
                                                $logger->error($erreurMessage);
                                                $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                if ($getPathFichier) {
                    return $filesReturn;
                }
            } else {
                $erreurMessage = 'Erreur Objet invalide -  Echange : '.print_r($echange, true)." , \n Contrat : ".print_r($contrat, true)." ] , \n Methode = Atexo_Chorus_GestionFichierChorus::getFileSignEnvForTransmitionChorus";
                $logger->error($erreurMessage);
                $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;
            }
        } catch (Exception $ex) {
            $erreurMessage = "Erreur lors de la recuperation des pieces de signature d'env pour le flux vers CHORUS, Echange [Id = ".$echange->getId().' , Org = '.$acronymeOrganisme.' , ContratId = '.$contrat['uuid'].' ] , Methode = Atexo_Chorus_GestionFichierChorus::getFileSignEnvForTransmitionChorus , Exception :[ '.$ex->getMessage().' ]';
            $logger->error($erreurMessage);
            $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;
        }
    }

    /**
     * Permet d'envoyer les pièces ajoutées dans la reponse pas à pas (autre que l'acte d'engagement).
     *
     * @param CommonChorusEchange $echange
     * @param CommonTypeDecision  $decision
     * @param $acronymeOrganisme: acronyme de l'organisme
     * @param $connexion: Objet connexion
     * @param $getPathFichier: Permet de retourner le chemin du fichier contenant les pièces
     * @param DomDocument $domDocument
     * @param $engagementJuridique: Engagement juridique
     */
    public function getFileEnvRepPasAPasForTransmitionChorus($echange, $contrat, $acronymeOrganisme, $getPathFichier = true, &$domDocument = null, &$engagementJuridique = null, &$erreursFluxChorus = null, $logger = null)
    {
        try {
            if (empty($logger)) {
                $logger = Atexo_LoggerManager::getLogger('chorus');
            }
            if ($echange instanceof CommonChorusEchange && is_array($contrat)) {
                $filesReturn = [];
                if ($echange->getIdsBlobEnv()) {
                    $atexoBlob = new Atexo_Blob();
                    $idsEnveloppes = explode(';', $echange->getIdsBlobEnv());
                    if (is_array($idsEnveloppes) && count($idsEnveloppes) > 0) {
                        $litesLots = [];
                        if (!empty($contrat['numeroLot'])) {
                            $categorieLot = (new Atexo_CategorieLot())->retrieveByOrganismeConsultationIdNumero(
                                $contrat['organisme'],
                                $contrat['idConsultation'],
                                $contrat['numeroLot']
                            );
                            if ($categorieLot) {
                                $litesLots[] = $categorieLot->getLot();
                            }
                        }
                        foreach ($litesLots as $lot) {
                            $idEnveloppe = (new Atexo_Consultation_Decision())->getIdEnveloppeOffreContrat($acronymeOrganisme, $contrat['idConsultation'], $contrat['id'], $lot);
                            $piecesAttributaires = (new Atexo_Helios_PiecesAttributaires())->retreivePiecesAttributaires($idEnveloppe, Atexo_Config::getParameter('DEPOT_ELECTRONIQUE'), $acronymeOrganisme);
                            foreach ($piecesAttributaires as $OnePiece) {
                                $fileContent = '';
                                $contenuZip = '';
                                if ('AE' != $OnePiece->getTypeFichier() && 'ACE' != $OnePiece->getTypeFichier()) {
                                    $idBlob = $OnePiece->getIdBlob();
                                    $tmpPath = Atexo_Config::getParameter('COMMON_TMP').'/'.session_name().session_id().time().date('Y_m_d_h_i_s').$idBlob;
                                    if (!is_dir($tmpPath)) {
                                        system('mkdir '.escapeshellarg($tmpPath));
                                    }
                                    $fileContent = $atexoBlob->return_blob_to_client($idBlob, $acronymeOrganisme);
                                    $nomPiece = Atexo_Util::OterAccents($OnePiece->getNomFichier());
                                    //Cas des fichiers zip
                                    //Début traitement des fichiers zip
                                    if ('zip' == Atexo_Util::getExtension($OnePiece->getNomFichier())) {
                                        $fp = fopen($tmpPath.'/env'.$idBlob.$nomPiece, 'w');
                                        fwrite($fp, $fileContent);
                                        fclose($fp);
                                        $arrayZipArboresence = Atexo_Zip::getOrderedFilesForZip($tmpPath.'/env'.$idBlob.$nomPiece, false);
                                        $array = [];
                                        foreach ($arrayZipArboresence as $arboresence) {
                                            $array = array_merge($array, $arboresence);
                                        }
                                        foreach ($idsEnveloppes as $idEnv) {
                                            $nomFichier = '';
                                            $type = '';
                                            foreach ($array as $files) {
                                                if (str_contains(ltrim($idEnv, '#'), '_')) {
                                                    $arrayIdEnvLtrim = explode('_', ltrim($idEnv, '#'));
                                                    $idEnv = $arrayIdEnvLtrim[1];
                                                }
                                                if ($idEnv == $files['index']) {
                                                    $nomFichier = $files['name'];
                                                    $type = $files['type'];
                                                }
                                            }
                                            if ('file' == $type && $nomFichier) {
                                                $filetmp = uniqid();
                                                if (!is_dir($tmpPath.'/'.$filetmp)) {
                                                    system('mkdir '.escapeshellarg($tmpPath.'/'.$filetmp));
                                                }
                                                $fileName = Atexo_Zip::extractZippedFile($tmpPath.'/'.'env'.$idBlob.$nomPiece, $tmpPath.'/'.$filetmp, $idEnv, true, false);
                                                (new Atexo_Zip())->addFileToZip($fileName, $fileName.'.zip');
                                                if (filesize($fileName.'.zip') > Atexo_Config::getParameter('CHORUS_MAX_UPLOAD_FILE_SIZE')) {
                                                    unlink($fileName);
                                                    unlink($fileName.'.zip');

                                                    return false;
                                                }
                                                $contenuZip = file_get_contents($fileName.'.zip');
                                                if ($getPathFichier) {
                                                    $filesReturn[] = [$nomFichier, $fileName];
                                                } else {
                                                    unlink($fileName);
                                                }
                                                unlink($fileName.'.zip');
                                                if ($contenuZip && !$getPathFichier) {
                                                    $pj = $domDocument->createElement('PJ', '');
                                                    $engagementJuridique->appendChild($pj);
                                                    $nomFichier = (new Atexo_Chorus_Util())->convertToAscii(utf8_encode($nomFichier));
                                                    $nomFichier = Atexo_Util::OterAccents($nomFichier);
                                                    $nomFichier = Atexo_Util::clean($nomFichier);

                                                    $typeFichier = substr(strrchr($nomFichier, '.'), 1);
                                                    if ('' == $typeFichier || ' ' == $typeFichier) {
                                                        $typeFichier = 'txt';
                                                    }
                                                    $data = explode('#', str_replace(' ', '', Atexo_Config::getParameter('EXTENTION_PIECES_JOINTES_CHORUS')));
                                                    if (!in_array(strtoupper($typeFichier), $data)) {
                                                        return false;
                                                    }
                                                    if ($echange->isTypeFluxFen211($contrat)) {
                                                        $typeFichier = strtoupper($typeFichier);
                                                    }
                                                    $type = $domDocument->createElement('Type', $typeFichier);
                                                    $pj->appendChild($type);
                                                    $descriptionFichier = substr(substr($nomFichier, 0, -4), 0, 64);
                                                    if ('' == $descriptionFichier || ' ' == $descriptionFichier) {
                                                        $descriptionFichier = 'NON_RENSEIGNE';
                                                    }
                                                    $description = $domDocument->createElement('Description', $descriptionFichier);
                                                    $pj->appendChild($description);
                                                    $data = $domDocument->createElement('Data', base64_encode($contenuZip));
                                                    $pj->appendChild($data);
                                                } else {
                                                    $erreurMessage = "Le fichier reponse enveloppe pas a pas n'existe pas ou son contenu est vide , Echange [Id = ".$echange->getId().' , Organisme = '.$acronymeOrganisme.', contratId = '.$contrat['uuid'].' ]'.PHP_EOL.'Fichier: '.print_r($OnePiece, true);
                                                    $logger->error($erreurMessage);
                                                    $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;
                                                }
                                            }
                                        }//Fin traitement des zip
                                    } else {//Début traitement des non zip
                                        if ($fileContent) {
                                            foreach ($idsEnveloppes as $idEnv) {
                                                if ($idEnv == $idBlob) {
                                                    $nomFichier = (new Atexo_Chorus_Util())->convertToAscii(utf8_encode($nomPiece));
                                                    $nomFichier = Atexo_Util::OterAccents($nomFichier);
                                                    $nomFichier = Atexo_Util::clean($nomFichier);

                                                    $tmpPath = Atexo_Config::getParameter('COMMON_TMP').'/'.session_name().session_id().time().date('Y_m_d_h_i_s').$idBlob;
                                                    if (!is_dir($tmpPath)) {
                                                        system('mkdir '.escapeshellarg($tmpPath));
                                                    }

                                                    Atexo_Util::write_file($tmpPath.'/'.$nomFichier, $fileContent);
                                                    (new Atexo_Zip())->addFileToZip($tmpPath.'/'.$nomFichier, $tmpPath.'/'.$nomFichier.'zip');

                                                    $contenuZip = file_get_contents($tmpPath.'/'.$nomFichier.'zip');

                                                    unlink($tmpPath.'/'.$nomFichier.'zip');
                                                    $typeFichier = Atexo_Util::getExtension($OnePiece->getNomFichier());
                                                    if ('' == $typeFichier || ' ' == $typeFichier) {
                                                        $typeFichier = 'txt';
                                                    }
                                                    $pj = $domDocument->createElement('PJ', '');
                                                    $engagementJuridique->appendChild($pj);
                                                    if ($echange->isTypeFluxFen211($contrat)) {
                                                        $typeFichier = strtoupper($typeFichier);
                                                    }
                                                    $type = $domDocument->createElement('Type', $typeFichier);
                                                    $pj->appendChild($type);
                                                    $descriptionFichier = substr(substr($nomFichier, 0, -4), 0, 64);
                                                    $descriptionFichier = utf8_decode($descriptionFichier);
                                                    if ('' == $descriptionFichier || ' ' == $descriptionFichier) {
                                                        $descriptionFichier = 'NON_RENSEIGNE';
                                                    }
                                                    $description = $domDocument->createElement('Description', $descriptionFichier);
                                                    $pj->appendChild($description);
                                                    $data = $domDocument->createElement('Data', base64_encode($contenuZip));
                                                    $pj->appendChild($data);
                                                }
                                            }
                                        }
                                    }//Fin traitement des non zip
                                }
                            }
                        }
                    }
                }
                if ($getPathFichier) {
                    return $filesReturn;
                }
            } else {
                $erreurMessage = 'Erreur Objet invalide -  Echange : '.print_r($echange, true)." , \n Contrat : ".print_r($contrat, true)." ] , \n Methode = Atexo_Chorus_GestionFichierChorus::getFileEnvRepPasAPasForTransmitionChorus";
                $logger->error($erreurMessage);
                $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;
            }
        } catch (Exception $ex) {
            $erreurMessage = "Erreur lors de la recuperation des pieces d'env reponse pas a pas pour le flux vers CHORUS, Echange [Id = ".$echange->getId().' , Org = '.$acronymeOrganisme.' , ContratId = '.$contrat['uuid'].' ] , Methode = Atexo_Chorus_GestionFichierChorus::getFileEnvRepPasAPasForTransmitionChorus , Exception :[ '.$ex->getMessage().' ]';
            $logger->error($erreurMessage);
            $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;
        }
    }

    /**
     * Permet de recuperer et transmettre la fiche de navette.
     *
     * @param $connexion : objet connexion
     * @param $getPathFichier
     * @param DomDocument $domDocument
     * @param $engagementJuridique : engagement juridique
     */
    public function getFileNavetteChorus(CommonChorusEchange $echange, $getPathFichier = true, &$domDocument = null, &$engagementJuridique = null, &$erreursFluxChorus = null, $logger = null, $contrat = null)
    {
        try {
            if (empty($logger)) {
                $logger = Atexo_LoggerManager::getLogger('chorus');
            }
            if ($echange instanceof CommonChorusEchange) {
                $ficheNavette = (new Atexo_Chorus_Echange())->retrieveFicheNavetteByIdChorusEchangeAndOrg($echange->getId(), $echange->getOrganisme());
                if ($ficheNavette instanceof CommonChorusFicheNavette) {
                    $idBlob = $ficheNavette->getIdDocument();
                    $atexoBlob = new Atexo_Blob();
                    $fileContent = $atexoBlob->return_blob_to_client($idBlob, $ficheNavette->getOrganisme());

                    if (empty($fileContent)) {
                        $erreurMessage = "Le fichier fiche navette n'existe pas ou son contenu est vide , Echange [Id = ".$echange->getId().' ]'.PHP_EOL.'Fichier: '.print_r($ficheNavette, true);
                        $logger->error($erreurMessage);
                    }
                    $nomFichier = Atexo_Util::OterAccents($ficheNavette->getNomDocument());
                    $nomFichier = Atexo_Util::clean($nomFichier);
                    //FIX MPE-10843
                    $nomFichierPdfSurDisque = $ficheNavette->getId().'_'.$ficheNavette->getIdChorusEchange().'.pdf';

                    $descriptionFichier = substr(substr($nomFichier, 0, -4), 0, 64);
                    if ('' == $descriptionFichier || ' ' == $descriptionFichier) {
                        $descriptionFichier = 'NON_RENSEIGNE';
                    }

                    $tmpPath = Atexo_Config::getParameter('COMMON_TMP').'/'.session_name().session_id().time().date('Y_m_d_h_i_s').$idBlob.$echange->getId();
                    if (!is_dir($tmpPath)) {
                        system('mkdir '.escapeshellarg($tmpPath));
                    }

                    Atexo_Util::write_file($tmpPath.'/'.$nomFichierPdfSurDisque, $fileContent);
                    (new Atexo_Zip())->addFileToZip($tmpPath.'/'.$nomFichierPdfSurDisque, $tmpPath.'/'.$nomFichierPdfSurDisque.'zip');
                    if (filesize($tmpPath.'/'.$nomFichierPdfSurDisque.'zip') > Atexo_Config::getParameter('CHORUS_MAX_UPLOAD_FILE_SIZE')) {
                        unlink($tmpPath.'/'.$nomFichierPdfSurDisque);
                        unlink($tmpPath.'/'.$nomFichierPdfSurDisque.'zip');

                        return false;
                    }
                    $fileContent = file_get_contents($tmpPath.'/'.$nomFichierPdfSurDisque.'zip');
                    $fileContentSize = filesize($tmpPath.'/'.$nomFichierPdfSurDisque.'zip') / 1024; //taille du fichier en Ko

                    if (!$getPathFichier) {
                        unlink($tmpPath.'/'.$nomFichierPdfSurDisque);
                    }
                    unlink($tmpPath.'/'.$nomFichierPdfSurDisque.'zip');

                    if ('pdf' == Atexo_Util::getExtension($nomFichierPdfSurDisque)) {
                        //on teste si le poids de la fiche navette est supérieure à 1 Ko
                        //fix MPE-6950
                        if ($fileContent && !$getPathFichier && $fileContentSize > 1) {
                            $pj = $domDocument->createElement('PJ', '');
                            $engagementJuridique->appendChild($pj);
                            $typeFichier = 'pdf';
                            $data = explode('#', str_replace(' ', '', Atexo_Config::getParameter('EXTENTION_PIECES_JOINTES_CHORUS')));
                            if (!in_array(strtoupper($typeFichier), $data)) {
                                return false;
                            }
                            if ($echange->isTypeFluxFen211($contrat)) {
                                $typeFichier = strtoupper($typeFichier);
                            }
                            $type = $domDocument->createElement('Type', $typeFichier);
                            $pj->appendChild($type);

                            $description = $domDocument->createElement('Description', $descriptionFichier);
                            $pj->appendChild($description);

                            $data = $domDocument->createElement('Data', base64_encode($fileContent));
                            $pj->appendChild($data);
                        } else {
                            $erreurMessage = "Le fichier fiche navette n'existe pas ou son contenu est vide , Echange [Id = ".$echange->getId().' ]'.PHP_EOL.'Fichier: '.print_r($ficheNavette, true);
                            $logger->error($erreurMessage);
                        }
                    }
                }
            } else {
                $erreurMessage = 'Erreur Objet invalide -  Echange : '.print_r($echange, true)." ] , \n Methode = Atexo_Chorus_GestionFichierChorus::getFileNavetteChorus";
                $logger->error($erreurMessage);
                $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;
            }
        } catch (Exception $ex) {
            $erreurMessage = 'Erreur lors de la recuperation de la fiche navette pour le flux vers CHORUS, Echange [Id = '.$echange->getId().' , Org = '.$echange->getOrganisme().' ] , Methode = Atexo_Chorus_GestionFichierChorus::getFileNavetteChorus , Exception :[ '.$ex->getMessage().' ]';
            $logger->error($erreurMessage);
            $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;
        }
    }

    /*
     * Permet d'envoyer les pièces d'ouverture analyse dans le flux chorus
     */
    public function getFilePiecesOuvertureAnlyseChorus($echange, $acronymeOrganisme, $getPathFichier = true, &$domDocument = null, &$engagementJuridique = null, &$erreursFluxChorus = null, $logger = null, $contrat = null)
    {
        try {
            if (empty($logger)) {
                $logger = Atexo_LoggerManager::getLogger('chorus');
            }
            if ($echange instanceof CommonChorusEchange) {
                $filesReturn = [];
                $piecesRapport = (new Atexo_Chorus_Echange())->retreivePiecesOuvertureAnlyse($echange->getIdsRapportSignature(), $acronymeOrganisme);
                if (is_array($piecesRapport) && count($piecesRapport) > 0) {
                    foreach ($piecesRapport as $pieceRapport) {
                        $fileContent = file_get_contents($pieceRapport['pathFile']);
                        $nomPiece = Atexo_Util::OterAccents($pieceRapport['nomFichier']);
                        $nomPiece = Atexo_Util::clean($nomPiece);

                        Atexo_Util::write_file(Atexo_Config::getParameter('COMMON_TMP').'/'.$nomPiece, $fileContent);
                        (new Atexo_Zip())->addFileToZip(Atexo_Config::getParameter('COMMON_TMP').'/'.$nomPiece, Atexo_Config::getParameter('COMMON_TMP').'/'.$nomPiece.'.zip');
                        if (filesize(Atexo_Config::getParameter('COMMON_TMP').'/'.$nomPiece.'.zip') > Atexo_Config::getParameter('CHORUS_MAX_UPLOAD_FILE_SIZE')) {
                            unlink(Atexo_Config::getParameter('COMMON_TMP').'/'.$nomPiece);
                            unlink(Atexo_Config::getParameter('COMMON_TMP').'/'.$nomPiece.'.zip');

                            return false;
                        }
                        $fileContent = file_get_contents(Atexo_Config::getParameter('COMMON_TMP').'/'.$nomPiece.'.zip');

                        if ($getPathFichier) {
                            $filesReturn[] = [$nomPiece, Atexo_Config::getParameter('COMMON_TMP').'/'.$nomPiece];
                        } else {
                            unlink(Atexo_Config::getParameter('COMMON_TMP').'/'.$nomPiece);
                        }

                        unlink(Atexo_Config::getParameter('COMMON_TMP').'/'.$nomPiece.'.zip');

                        if ($fileContent && !$getPathFichier) {
                            $pj = $domDocument->createElement('PJ', '');
                            $engagementJuridique->appendChild($pj);

                            $typeFichier = substr(strrchr($nomPiece, '.'), 1);
                            if ('' == $typeFichier || ' ' == $typeFichier) {
                                $typeFichier = 'txt';
                            }
                            $data = explode('#', str_replace(' ', '', Atexo_Config::getParameter('EXTENTION_PIECES_JOINTES_CHORUS')));
                            if (!in_array(strtoupper($typeFichier), $data)) {
                                return false;
                            }
                            if ($echange->isTypeFluxFen211($contrat)) {
                                $typeFichier = strtoupper($typeFichier);
                            }
                            $type = $domDocument->createElement('Type', $typeFichier);
                            $pj->appendChild($type);

                            $descriptionFichier = substr(substr($nomPiece, 0, -4), 0, 64);
                            if ('' == $descriptionFichier || ' ' == $descriptionFichier) {
                                $descriptionFichier = 'NON_RENSEIGNE';
                            }
                            $description = $domDocument->createElement('Description', $descriptionFichier);
                            $pj->appendChild($description);
                            $data = $domDocument->createElement('Data', base64_encode($fileContent));
                            $pj->appendChild($data);
                        } else {
                            $erreurMessage = "Le fichier ouverture et analyse n'existe pas ou son contenu est vide , Echange [Id = ".$echange->getId().' , Organisme = '.$acronymeOrganisme.' ]'.PHP_EOL.'Fichier: '.print_r($pieceRapport, true);
                            $logger->error($erreurMessage);
                            $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;
                        }
                        unset($fileContent);
                    }
                }
                if ($getPathFichier) {
                    return $filesReturn;
                }
            } else {
                $erreurMessage = 'Erreur Objet invalide -  Echange : '.print_r($echange, true)." ] , \n Methode = Atexo_Chorus_GestionFichierChorus::getFilePiecesOuvertureAnlyseChorus";
                $logger->error($erreurMessage);
                $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;
            }
        } catch (Exception $ex) {
            $erreurMessage = "Erreur lors de la recuperation des pieces d'ouverture et analyse pour le flux vers CHORUS, Echange [Id = ".$echange->getId().' , Org = '.$acronymeOrganisme.' ] , Methode = Atexo_Chorus_GestionFichierChorus::getFilePiecesOuvertureAnlyseChorus , Exception :[ '.$ex->getMessage().' ]';
            $logger->error($erreurMessage);
            $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;
        }
    }

    /**
     * permet d'ajouter la piece FicheModificative ainsi que ses Pjs au flux Chorus.
     *
     * @param DomDocument $domDocument
     * @param DomElement  $engagementJuridique
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function getFileFicheModificativeChorus(CommonChorusEchange $echange, &$domDocument = null, &$engagementJuridique = null, &$erreursFluxChorus = null, $logger = null, $contrat = null)
    {
        if (empty($logger)) {
            $logger = Atexo_LoggerManager::getLogger('chorus');
        }
        $logger->info('Debut recuperation de la fiche Modificative pour transmission Chorus');
        try {
            if ($echange instanceof CommonChorusEchange) {
                $logger->info('Objet trouve : Echange [Id = '.$echange->getId().' , Organisme = '.$echange->getOrganisme().' ] , Methode = Atexo_Chorus_Transmission::getFileNavetteChorus');
                $ficheModificative = CommonTChorusFicheModificativeQuery::create()->filterByIdEchange($echange->getId())->findOne();
                if ($ficheModificative instanceof CommonTChorusFicheModificative) {
                    $idBlob = $ficheModificative->getIdBlobFicheModificative();
                    $atexoBlob = new Atexo_Blob();
                    $fileContent = $atexoBlob->return_blob_to_client($idBlob, $ficheModificative->getOrganisme());
                    $nomFichier = (new Atexo_Chorus_Util())->getNomPdfActeModificatif($echange);
                    if (false == Atexo_Util::isUTF8($nomFichier)) {
                        $nomFichier = utf8_encode($nomFichier);
                    }
                    $nomFichier = (new Atexo_Chorus_Util())->convertToAscii($nomFichier);
                    $descriptionFichier = substr(substr($nomFichier, 0, -4), 0, 64);
                    if (!empty($fileContent)) {
                        $pathFileTmp = Atexo_Config::getParameter('COMMON_TMP').DIRECTORY_SEPARATOR.$nomFichier;
                        Atexo_Util::write_file($pathFileTmp, $fileContent);
                        (new Atexo_Zip())->addFileToZip($pathFileTmp, $pathFileTmp.'.zip');
                        if (filesize($pathFileTmp.'.zip') > Atexo_Config::getParameter('CHORUS_MAX_UPLOAD_FILE_SIZE')) {
                            unlink($pathFileTmp);
                            unlink($pathFileTmp.'.zip');

                            return false;
                        }

                        $zipFileContent = file_get_contents($pathFileTmp.'.zip');

                        if (!empty($zipFileContent)) {
                            $pj = $domDocument->createElement('PJ', '');
                            $engagementJuridique->appendChild($pj);
                            $typeFichier = Atexo_Util::getExtension($nomFichier); //'pdf';
                            if ($echange->isTypeFluxFen211($contrat)) {
                                $typeFichier = strtoupper($typeFichier);
                            }
                            $type = $domDocument->createElement('Type', $typeFichier);
                            $pj->appendChild($type);

                            $description = $domDocument->createElement('Description', $descriptionFichier);
                            $pj->appendChild($description);

                            $data = $domDocument->createElement('Data', base64_encode($zipFileContent));
                            $pj->appendChild($data);

                            $logger->info('Fiche modificative ajoutee au flux avec succes - Echange [Id = '.$echange->getId().' , Organisme = '.$echange->getOrganisme().' , idFicheModif = '.$ficheModificative->getIdFicheModificative().']');
                        } else {
                            $logger->info('Contenu vide pour zip fiche modificative - Echange [Id = '.$echange->getId().' , Organisme = '.$echange->getOrganisme().' , idFicheModif = '.$ficheModificative->getIdFicheModificative().']');
                        }
                        unlink($pathFileTmp);
                        unlink($pathFileTmp.'.zip');
                        unset($zipFileContent);
                    } else {
                        $erreurMessage = "La fiche modificative n'existe pas ou son contenu est vide , Echange [Id = ".$echange->getId().' ] , fiche_modificative_id = ['.$ficheModificative->getIdFicheModificative().']';
                        $erreurMessage .= PHP_EOL.PHP_EOL.'Contenu de la fiche modificative = '.$fileContent.PHP_EOL."Veuillez consulter le fichier 'BASE_ROOT_DIR/logs/interfacePlaceChorus/aaaa/mm/jj/interfacePlaceChorus.log' pour en savoir plus sur les details de la generation de la fiche modificative";
                        $erreurMessage .= PHP_EOL.PHP_EOL.'Objet fiche modificative : '.PHP_EOL.print_r($ficheModificative, true);
                        $logger->error($erreurMessage);
                        //$erreursFluxChorus[] = "Erreur lors de la generation du flux Chorus" . PHP_EOL . $erreurMessage; //IMPORTANT: Decommenter cette ligne pour bloquer l'envoi de la fiche modificative en cas d'erreur de generation
                    }
                    $Pjs = $ficheModificative->getCommonTChorusFicheModificativePjs();
                    if ($Pjs) {
                        foreach ($Pjs as $id => $unePj) {
                            $idBlob = $unePj->getFichier();
                            $atexoBlob = new Atexo_Blob();
                            $fileContent = $atexoBlob->return_blob_to_client($idBlob, $ficheModificative->getOrganisme());
                            $zipFileContent = '';

                            if (!empty($fileContent)) {
                                $nomPj = $unePj->getNomFichier();
                                if (false == Atexo_Util::isUTF8($nomPj)) {
                                    $nomPj = utf8_encode($nomPj);
                                }
                                $nomPj = (new Atexo_Chorus_Util())->convertToAscii($nomPj);
                                $nomPj = Atexo_Util::clean(Atexo_Util::OterAccents(($nomPj)));
                                $pathFileTmp = Atexo_Config::getParameter('COMMON_TMP').DIRECTORY_SEPARATOR.$id;

                                Atexo_Util::write_file($pathFileTmp, $fileContent);
                                (new Atexo_Zip())->addFileToZip($pathFileTmp, $pathFileTmp.'.zip');
                                if (filesize($pathFileTmp.'.zip') > Atexo_Config::getParameter('CHORUS_MAX_UPLOAD_FILE_SIZE')) {
                                    unlink($pathFileTmp);
                                    unlink($pathFileTmp.'.zip');

                                    return false;
                                }

                                $zipFileContent = file_get_contents($pathFileTmp.'.zip');

                                if (!empty($zipFileContent)) {
                                    $pj = $domDocument->createElement('PJ', '');
                                    $engagementJuridique->appendChild($pj);
                                    $descriptionPj = substr($nomPj, 0, 64);
                                    $typePj = Atexo_Util::getExtension($nomPj);
                                    $data = explode('#', str_replace(' ', '', Atexo_Config::getParameter('EXTENTION_PIECES_JOINTES_CHORUS')));
                                    if (!in_array(strtoupper($typePj), $data)) {
                                        //c'est gerer lors de l'enregistrement mais on gere la modif sur la bd
                                        return false;
                                    }

                                    if ($echange->isTypeFluxFen211($contrat)) {
                                        $typePj = strtoupper($typePj);
                                    }
                                    $type = $domDocument->createElement('Type', $typePj);
                                    $pj->appendChild($type);

                                    $description = $domDocument->createElement('Description', $descriptionPj);
                                    $pj->appendChild($description);

                                    $data = $domDocument->createElement('Data', base64_encode($zipFileContent));
                                    $pj->appendChild($data);

                                    $logger->info('Piece jointe de fiche modificative ajoutee avec succes - Echange [Id = '.$echange->getId().' , Organisme = '.$echange->getOrganisme()." , idPj = $id , nomPj = $nomPj]");
                                } else {
                                    $logger->info('Contenu vide pour zip fiche Pj fiche modificative - Echange [Id = '.$echange->getId().' , Organisme = '.$echange->getOrganisme()." , idPj = $id , nomPj = $nomPj]");
                                }
                                unlink($pathFileTmp);
                                unlink($pathFileTmp.'.zip');
                            }
                            unset($zipFileContent);
                        }
                    }
                }
            } else {
                $erreurMessage = 'Erreur Objet invalide -  Echange : '.print_r($echange, true)." ] , \n Methode = Atexo_Chorus_GestionFichierChorus::getFileNavetteChorus";
                $logger->error($erreurMessage);
                $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;
            }
        } catch (Exception $ex) {
            $erreurMessage = 'Erreur lors de la recuperation de la fiche modificative pour le flux vers CHORUS, Echange [Id = '.$echange->getId().' , Org = '.$echange->getOrganisme().' ] , Methode = Atexo_Chorus_GestionFichierChorus::getFileNavetteChorus , Exception :[ '.$ex->getMessage().' ]';
            $logger->error($erreurMessage);
            $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;
        }
        $logger->info('Fin recuperation de la fiche modificative pour la transmission Chorus');
    }

    public function getFilePiecesDumeForTransmitionChorus(
        $echange,
        $acronymeOrganisme,
        $typeDume = null,
        $getPathFichier = true,
        &$domDocument = null,
        &$engagementJuridique = null,
        &$erreursFluxChorus = null,
        $logger = null,
        $contrat = null
    ) {
        try {
            if (empty($logger)) {
                $logger = Atexo_LoggerManager::getLogger('chorus');
            }

            if ($echange instanceof CommonChorusEchange) {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').
                    Atexo_Config::getParameter('CONST_READ_ONLY'));
                $filesReturn = [];

                $nameXml = null;
                $dumeItems = null;
                if ($typeDume == Atexo_Config::getParameter('TYPE_DUME_ACHETEUR')) {
                    $dumeItems = $echange->getDumeAcheteurItems();
                    $nameXml = 'dumeAcheteur';
                } elseif ($typeDume == Atexo_Config::getParameter('TYPE_DUME_OE')) {
                    $dumeItems = $echange->getDumeOeItems();
                    $nameXml = 'dumeOe';
                }

                if ($dumeItems) {
                    $atexoBlob = new Atexo_Blob();
                    $idDocDume = explode(';', $dumeItems);
                    if (is_array($idDocDume) && count($idDocDume) > 0) {
                        foreach ($idDocDume as $idDoc) {
                            $fileContent = '';
                            $contenuZip = '';
                            $tmpPath = Atexo_Config::getParameter('COMMON_TMP').'/'.
                                session_name().session_id().
                                time().date('Y_m_d_h_i_s').$idDoc;
                            if (!is_dir($tmpPath)) {
                                system('mkdir '.escapeshellarg($tmpPath));
                            }

                            $blobResource = Atexo_Blob::acquire_lock_on_blob(
                                $idDoc,
                                $acronymeOrganisme
                            );

                            $fileName = $blobResource['blob']['name'];
                            $fileName = Atexo_Util::OterAccents($fileName);
                            $fileName = Atexo_Util::clean($fileName);
                            $fileContent = $atexoBlob->return_blob_to_client($idDoc, $acronymeOrganisme);
                            $fp = fopen($tmpPath.'/'.$fileName, 'w');
                            fwrite($fp, $fileContent);
                            fclose($fp);
                            (new Atexo_Zip())->addFileToZip($tmpPath.'/'.$fileName, $tmpPath.'/'.$fileName.'.zip');
                            if (filesize($tmpPath.'/'.$fileName.'.zip') > Atexo_Config::getParameter('CHORUS_MAX_UPLOAD_FILE_SIZE')) {
                                unlink($tmpPath.'/'.$fileName);
                                unlink($tmpPath.'/'.$fileName.'.zip');

                                return false;
                            }
                            $contenuZip = file_get_contents($tmpPath.'/'.$fileName.'.zip');
                            if ($getPathFichier) {
                                $filesReturn[] = [$fileName, $tmpPath.'/'.$fileName];
                            }

                            if ($contenuZip && !$getPathFichier) {
                                $pj = $domDocument->createElement('PJ', '');
                                $engagementJuridique->appendChild($pj);
                                $fileName = Atexo_Util::OterAccents($fileName);
                                $fileName = Atexo_Util::clean($fileName);
                                $typeFichier = substr(strrchr($fileName, '.'), 1);
                                if ('' == $typeFichier || ' ' == $typeFichier) {
                                    $typeFichier = 'txt';
                                }

                                if ($echange->isTypeFluxFen211($contrat)) {
                                    $typeFichier = strtoupper($typeFichier);
                                }
                                $type = $domDocument->createElement('Type', $typeFichier);
                                $pj->appendChild($type);
                                $descriptionFichier = substr(substr($fileName, 0, -4), 0, 64);
                                if ('' == $descriptionFichier || ' ' == $descriptionFichier) {
                                    $descriptionFichier = 'NON_RENSEIGNE';
                                }
                                $description = $domDocument->createElement('Description', $descriptionFichier);
                                $pj->appendChild($description);
                                $data = $domDocument->createElement('Data', (base64_encode($contenuZip)));
                                $pj->appendChild($data);
                            } else {
                                $erreurMessage = "Le fichier piece dume n'existe pas ou son contenu est vide , 
                                Echange [Id = ".$echange->getId().' , 
                                Organisme = '.$acronymeOrganisme.' ]'.PHP_EOL.
                                    'Fichier: '.print_r($blobResource, true);
                                $logger->error($erreurMessage);
                                $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.
                                    $erreurMessage;
                            }
                        }
                    }
                }

                if ($getPathFichier) {
                    return $filesReturn;
                }
            } else {
                $erreurMessage = 'Erreur Objet invalide -  Echange : '.print_r($echange, true)." ] , \n 
                Methode = Atexo_Chorus_GestionFichierChorus::getFilePiecesDumeForTransmitionChorus";
                $logger->error($erreurMessage);
                $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;
            }
        } catch (\Exception $ex) {
            $erreurMessage = 'Erreur lors de la recuperation des pieces externes pour le flux vers CHORUS, 
            Echange [Id = '.$echange->getId().' , Org = '.$acronymeOrganisme.' ] , 
            Methode = Atexo_Chorus_GestionFichierChorus::getFilePiecesDumeForTransmitionChorus , 
            Exception :[ '.$ex->getMessage().' ]';
            $logger->error($erreurMessage);
            $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;
        }
    }
}
