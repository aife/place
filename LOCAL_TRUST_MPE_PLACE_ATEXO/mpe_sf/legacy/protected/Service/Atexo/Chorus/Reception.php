<?php

namespace Application\Service\Atexo\Chorus;

use App\Service\WebServices\WebServicesExec;
use \DOMDocument;
use AtexoCrypto\Dto\Fichier;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Contrat;
use Application\Service\Atexo\Chorus\Atexo_Chorus_Util;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonChorusEchange;
use Application\Propel\Mpe\CommonChorusEchangePeer;
use Application\Propel\Mpe\CommonChorusEchangeQuery;
use Application\Propel\Mpe\CommonChorusNomsFichiers;
use Application\Propel\Mpe\CommonChorusNomsFichiersPeer;
use Application\Propel\Mpe\CommonTContratTitulaire;
use Application\Propel\Mpe\CommonTContratTitulaireQuery;
use Application\Service\Atexo\Agent\Atexo_Agent_Alertes;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_GuestVo;
use Exception;
use Prado\Prado;

class Atexo_Chorus_Reception
{
    public function receptionCompteRendu($xml, $nomFichier = null)
    {
        $idEchange = null;
        $acronymeOrganisme = null;
        $idDecision = null;
        $log = '';
        $return = true;
        $logger = Atexo_LoggerManager::getLogger('chorus');
        try {
            (new Atexo_Chorus_Util())->loggerInfosReception('Debut integration Compte Rendu , [ fichier '.$nomFichier.' ]');

            $log .= "Intégration CEN : fichier $nomFichier, le ".date('Y/m/d H:i:s')." \r\n\r\n";
            $domDocument = new DOMDocument();
            $log .= "Chargement de l'xml ... ";
            (new Atexo_Chorus_Util())->loggerInfosReception("Chargement de l'xml ... , [ fichier ".$nomFichier.' ]');
            if (!$domDocument->loadXML($xml)) {
                $erreur = "Erreur de chargement du flux $nomFichier , Methode: Atexo_Chorus_Reception::receptionCompteRendu";
                (new Atexo_Chorus_Util())->loggerErreurReception($erreur);
                $log .= $erreur;
                self::writeFailedFilesInCustomLog($log, $nomFichier);

                return false;
            }
            $log .= "xml chargé \r\n\r\n";
            (new Atexo_Chorus_Util())->loggerInfosReception('xml chargé , [ fichier '.$nomFichier.' ]');

            $compteRendu = $domDocument->getElementsByTagName('COMPTE_RENDU')->item(0);
            if (!is_object($compteRendu)) {
                $erreur = "La balise 'COMPTE_RENDU' n'existe pas dans le flux $nomFichier ou Erreur de chargement du flux $nomFichier , Methode: Atexo_Chorus_Reception::receptionCompteRendu";
                (new Atexo_Chorus_Util())->loggerErreurReception($erreur);
                $log .= $erreur;
                self::writeFailedFilesInCustomLog($log, $nomFichier);

                return false;
            }

            $commonConnexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $enteteIrrecevable = $compteRendu->getElementsByTagName('ENTETE_IRRECEVABLE')->item(0);

            if ($enteteIrrecevable) {
                $log .= "\r\n\tEntete irrecevable : \r\n";
                (new Atexo_Chorus_Util())->loggerInfosReception('Entete irrecevable , [ fichier '.$nomFichier.' ]');
                $idFichierEmetteur = $enteteIrrecevable->getElementsByTagName('ID_FICHIER_EMETTEUR')->item(0)->nodeValue;

                $log .= "\t-> Recherche de l'echange où nom_fichier like %".$idFichierEmetteur.' dans la table common_db.chorus_noms_fichiers ... ';
                (new Atexo_Chorus_Util())->loggerInfosReception("Recherche de l'echange où nom_fichier like %".$idFichierEmetteur.' dans la table common_db.chorus_noms_fichiers ...  , [ fichier '.$nomFichier.' ]');

                $c = new Criteria();
                $c->add(CommonChorusNomsFichiersPeer::NOM_FICHIER, '%'.$idFichierEmetteur, Criteria::LIKE);
                $c->add(CommonChorusNomsFichiersPeer::TYPE_FICHIER, 'FEN', Criteria::EQUAL);
                $chorusNomsFichier = CommonChorusNomsFichiersPeer::doSelectOne($c, $commonConnexion);

                if ($chorusNomsFichier instanceof CommonChorusNomsFichiers) {
                    $log .= 'Fichier '.$idFichierEmetteur."  Trouvé.\r\n";
                    (new Atexo_Chorus_Util())->loggerInfosReception('Fichier '.$idFichierEmetteur.'  Trouvé , [ fichier '.$nomFichier.' ]');

                    $acronymeOrganisme = $chorusNomsFichier->getAcronymeOrganisme();
                    $organisme = Atexo_Organismes::retrieveOrganismeByAcronyme($acronymeOrganisme);
                    $idEchange = $chorusNomsFichier->getIdEchange();

                    $log .= "\t\t-> Id de l'échange : $idEchange\r\n";
                    $log .= "\t\t-> Id de l'organisme : ".$organisme->getId()."\r\n";
                    $log .= "\t\t-> Acronyme de l'organisme : $acronymeOrganisme\r\n";
                    (new Atexo_Chorus_Util())->loggerInfosReception('Id_Echange = '.$idEchange.' , Organisme = '.$acronymeOrganisme.' , [ fichier '.$nomFichier.' ]');

                    $log .= "\t-> Recherche de l'échange $idEchange ...";
                    (new Atexo_Chorus_Util())->loggerInfosReception("Recherche de l'échange ... (Id_Echange = ".$idEchange.' , Organisme = '.$acronymeOrganisme.') , [ fichier '.$nomFichier.' ]');

                    $echange = CommonChorusEchangePeer::retrieveByPK($idEchange, $acronymeOrganisme, $commonConnexion);

                    if ($echange instanceof CommonChorusEchange) {
                        $log .= " Echange Trouvé.\r\n";
                        (new Atexo_Chorus_Util())->loggerInfosReception('Echange trouve (Id_Echange = '.$idEchange.' , Organisme = '.$acronymeOrganisme.') , [ fichier '.$nomFichier.' ]');
                    } else {
                        $log .= " Echange Non Trouvé.\r\n";
                        (new Atexo_Chorus_Util())->loggerInfosReception('Echange non trouve (Id_Echange = '.$idEchange.' , Organisme = '.$acronymeOrganisme.') , [ fichier '.$nomFichier.' ]');
                    }

                    $log .= "\t-> Recherche du fichier en cours dans la table chorus_noms_fichiers ... ";
                    (new Atexo_Chorus_Util())->loggerInfosReception("Recherche du fichier $nomFichier : en cours dans la table chorus_noms_fichiers ...");
                    $chorusNomsFichier = CommonChorusNomsFichiersPeer::retrieveByPK($idEchange, $nomFichier, $commonConnexion);
                    if (!$chorusNomsFichier instanceof CommonChorusNomsFichiers) {
                        $log .= ' Fichier '.$nomFichier.' Non Trouvé.';
                        (new Atexo_Chorus_Util())->loggerInfosReception("Fichier $nomFichier non Trouvé.");
                        $newChorusNomsFichier = new CommonChorusNomsFichiers();
                        $newChorusNomsFichier->setIdEchange($idEchange);
                        $newChorusNomsFichier->setNomFichier($nomFichier);
                        $newChorusNomsFichier->setAcronymeOrganisme($acronymeOrganisme);
                        $newChorusNomsFichier->setTypeFichier('CEN');
                        $newChorusNomsFichier->setDateAjout(date('Y-m-d H:i:s'));
                        $newChorusNomsFichier->save($commonConnexion);
                        $log .= "\r\n\t-> Insertion d'un nouvel enregistrement dans la table chorus_noms_fichies ->";
                        $log .= "\t-> INSERT  INTO chorus_noms_fichies Values($idEchange, $nomFichier, $acronymeOrganisme, CEN)\r\n";
                    } else {
                        $log .= 'Fichier '.$nomFichier.' Trouvé.';
                        (new Atexo_Chorus_Util())->loggerInfosReception("Fichier $nomFichier Trouvé.");
                    }

                    $erreur = Atexo_Util::utf8ToIso($enteteIrrecevable->getElementsByTagName('CODE_ERREUR')->item(0)->nodeValue);
                    $libelleErreur = Atexo_Util::utf8ToIso($enteteIrrecevable->getElementsByTagName('LIBELLE_ERREUR')->item(0)->nodeValue);

                    if ($echange instanceof CommonChorusEchange) {
                        if ($erreur) {
                            $idDecision = $echange->getIdDecision();
                            $log .= "\r\n\t echange $idEchange de la décision $idDecision, organisme $acronymeOrganisme  rejeté pour motif : $erreur / $libelleErreur ... Aucune action à faire.\r\n";
                            (new Atexo_Chorus_Util())->loggerInfosReception("Echange (id : $idEchange) de la décision $idDecision, organisme $acronymeOrganisme  rejeté pour motif : $erreur / $libelleErreur ... Aucune action à faire");
                            $echange->setRetourChorus('IRR');
                            $echange->setErreurRejet($erreur.'  '.$libelleErreur);
                            $echange->save($commonConnexion);
                            //Début envoi mail aux agents invités et abonnés aux alertes

                            (new Atexo_Chorus_Util())->loggerInfosReception('Recherche du contrat , [id_decision = '.$echange->getIdDecision()." , Fichier = $nomFichier]");
                            $contrat = null;
                            try {
                                $execWS = Atexo_Util::getSfService(WebServicesExec::class);
                                if ($echange->getUuidExterneExec()) {
                                    $contrat = $execWS->getContent('getContratByUuid', $echange->getUuidExterneExec());
                                }
                            } catch (\Exception $exception) {
                                $erreur = "Erreur au moment de comunication avec Exec: : " . print_r($exception->getMessage(), true).PHP_EOL . PHP_EOL . "flux $nomFichier , Echange [ Id = ".$idEchange.' , Org = '.$acronymeOrganisme.' , idContrat = '.$idDecision." ] , Methode: Atexo_Chorus_Reception::receptionCompteRendu \r\n";
                                (new Atexo_Chorus_Util())->loggerErreurReception($erreur);
                                $log .= $erreur;
                                self::writeFailedFilesInCustomLog($log, $nomFichier);

                                return false;
                            }

                            if ($contrat) {
                                (new Atexo_Chorus_Util())->loggerInfosReception('Contrat trouve , [id_decision = '.$echange->getIdDecision()." , Fichier = $nomFichier]");

                                $corpsMessage = (new Atexo_Agent_Alertes())->getCorpsMessageAlerteChorusEnvoiRejete($contrat);
                                $objetEchange = (new Atexo_Agent_Alertes())->getIntituleMessageAlerteChorusEnvoiRejete($contrat, $echange);
                                $guests = (new Atexo_Agent())->retrieveGuestSubscribersChorusAlerts($contrat, $acronymeOrganisme, $commonConnexion, $logger);
                                if (is_array($guests) && count($guests)) {
                                    (new Atexo_Chorus_Util())->loggerInfosReception("Envoi emails aux agents (agents trouves) , [ Fichier = $nomFichier ]");
                                    foreach ($guests as $guest) {
                                        //Envoie du mail
                                        if ($guest instanceof Atexo_Consultation_GuestVo) {
                                            (new Atexo_Agent_Alertes())->sendMailAlertesAgentsChorus(Atexo_Config::getParameter('PF_MAIL_FROM', $acronymeOrganisme), $guest, $objetEchange, $corpsMessage, $contrat, $echange->getNumOrdre());
                                        }
                                    }
                                }
                            } else {
                                $erreur = "Contrat non trouve, Reception du compte rendu, flux $nomFichier , Echange [ Id = ".$idEchange.' , Org = '.$acronymeOrganisme.' , idContrat = '.$idDecision." ] , Methode: Atexo_Chorus_Reception::receptionCompteRendu \r\n";
                                (new Atexo_Chorus_Util())->loggerErreurReception($erreur);
                                $log .= $erreur;
                                self::writeFailedFilesInCustomLog($log, $nomFichier);

                                return false;
                            }

                            $corpsMessage = (new Atexo_Agent_Alertes())->getCorpsMessageAlerteChorusEnvoiRejete($contrat);
                            $objetEchange = (new Atexo_Agent_Alertes())->getIntituleMessageAlerteChorusEnvoiRejete($contrat, $echange);

                            $guests = (new Atexo_Agent())->retrieveGuestSubscribersChorusAlerts($contrat, $acronymeOrganisme, $commonConnexion, $logger);
                            if (is_array($guests) && count($guests)) {
                                foreach ($guests as $guest) {
                                    //Envoie du mail
                                    if ($guest instanceof Atexo_Consultation_GuestVo) {
                                        (new Atexo_Agent_Alertes())->sendMailAlertesAgentsChorus(Atexo_Config::getParameter('PF_MAIL_FROM', $acronymeOrganisme), $guest, $objetEchange, $corpsMessage, $contrat, $echange->getNumOrdre());
                                    }
                                }
                            }
                            //Fin envoi mail aux agents invités et abonnés aux alertes
                        }
                    }
                } else {
                    $log .= 'Echange où nom_fichier like %'.$idFichierEmetteur." Non trouvé.\r\n";
                    (new Atexo_Chorus_Util())->loggerInfosReception('Echange où nom_fichier like %'.$idFichierEmetteur." Non trouvé , [ Fichier = $nomFichier ]");
                }
            }
            $entetes = $compteRendu->getElementsByTagName('ENTETE_RECEVABLE');
            $lignesDossiers = $domDocument->getElementsByTagName('LIGNE_DOSSIER');

            $arrayFichiersEmmetteur = [];

            $log .= "Parcours des entetes recevables ... \r\n";
            (new Atexo_Chorus_Util())->loggerInfosReception("Parcours des entetes recevables ... , [ Fichier = $nomFichier ]");
            $i = 1;

            foreach ($entetes as $entete) {
                if (is_object($entete)) {
                    $log .= "\r\n\tEntete $i : \r\n";
                    (new Atexo_Chorus_Util())->loggerInfosReception("Entete $i , [ Fichier = $nomFichier ]");
                    ++$i;
                    $typeLigne = ($entete->getElementsByTagName('TYPE_LIGNE')->item(0)->nodeValue);

                    if ($typeLigne != Atexo_Config::getParameter('CHORUS_RETOUR_REJETE')) {
                        $log .= "\t-> Entete rejeté (ne signifie rien), Tag ENTETE_RECEVABLE ignoré...\r\nTraitement de l'entête suivante ...\r\n";
                        (new Atexo_Chorus_Util())->loggerInfosReception("Entete rejeté (ne signifie rien), Tag ENTETE_RECEVABLE ignoré...\r\nTraitement de l'entête suivante ...");
                        continue;
                    }

                    $idFichierEmetteur = $entete->getElementsByTagName('ID_FICHIER_EMETTEUR')->item(0)->nodeValue;

                    $log .= "\t-> Recherche de l'echange où nom_fichier like %".$idFichierEmetteur." dans la table common_db.chorus_noms_fichiers ... \r\n";
                    (new Atexo_Chorus_Util())->loggerInfosReception("Recherche de l'echange où nom_fichier like %".$idFichierEmetteur.' dans la table common_db.chorus_noms_fichiers ...');
                    $c = new Criteria();
                    $c->add(CommonChorusNomsFichiersPeer::NOM_FICHIER, '%'.$idFichierEmetteur, Criteria::LIKE);
                    $c->add(CommonChorusNomsFichiersPeer::TYPE_FICHIER, 'FEN', Criteria::EQUAL);
                    $chorusNomsFichier = CommonChorusNomsFichiersPeer::doSelectOne($c, $commonConnexion);
                    if ($chorusNomsFichier instanceof CommonChorusNomsFichiers) {
                        $log .= 'nom_fichier '.$idFichierEmetteur." Trouvé.\r\n";
                        (new Atexo_Chorus_Util())->loggerInfosReception('nom_fichier '.$idFichierEmetteur.' Trouvé');

                        $acronymeOrganisme = $chorusNomsFichier->getAcronymeOrganisme();
                        $organisme = Atexo_Organismes::retrieveOrganismeByAcronyme($acronymeOrganisme);
                        $idEchange = $chorusNomsFichier->getIdEchange();

                        $log .= "\t\t-> Id de l'échange : $idEchange\r\n";
                        $log .= "\t\t-> Id de l'organisme : ".$organisme->getId()."\r\n";
                        $log .= "\t\t-> Acronyme de l'organisme : $acronymeOrganisme\r\n";
                        (new Atexo_Chorus_Util())->loggerInfosReception("Recherche de l'échange (id : $idEchange , organisme : $acronymeOrganisme)");

                        $log .= "\t-> Recherche de l'échange $idEchange ...";

                        $echange = CommonChorusEchangePeer::retrieveByPK($idEchange, $acronymeOrganisme, $commonConnexion);

                        if ($echange instanceof CommonChorusEchange) {
                            $log .= $idEchange." Trouvé.\r\n";
                            (new Atexo_Chorus_Util())->loggerInfosReception("Echange (id : $idEchange , organisme : $acronymeOrganisme) Trouve");
                            $arrayFichiersEmmetteur[$organisme->getId().'-'.$echange->getId().'-'.$echange->getIdDecision()] = $idFichierEmetteur;
                        } else {
                            $log .= $idEchange." Non Trouvé.\r\n";
                            (new Atexo_Chorus_Util())->loggerInfosReception("Echange (id : $idEchange , organisme : $acronymeOrganisme) Non Trouvé");
                        }

                        $log .= "\t-> Recherche du fichier en cours dans la table chorus_noms_fichiers ... ";
                        (new Atexo_Chorus_Util())->loggerInfosReception("Recherche du fichier en cours dans la table chorus_noms_fichiers ... , [Fichier : $nomFichier]");
                        $chorusNomsFichier = CommonChorusNomsFichiersPeer::retrieveByPK($idEchange, $nomFichier, $commonConnexion);

                        if (!$chorusNomsFichier instanceof CommonChorusNomsFichiers) {
                            $log .= $nomFichier." Non Trouvé.\r\n";
                            (new Atexo_Chorus_Util())->loggerInfosReception("Fichier : $nomFichier Non trouve");
                            $newChorusNomsFichier = new CommonChorusNomsFichiers();
                            $newChorusNomsFichier->setIdEchange($idEchange);
                            $newChorusNomsFichier->setNomFichier($nomFichier);
                            $newChorusNomsFichier->setAcronymeOrganisme($acronymeOrganisme);
                            $newChorusNomsFichier->setTypeFichier('CEN');
                            $newChorusNomsFichier->setDateAjout(date('Y-m-d H:i:s'));
                            $newChorusNomsFichier->save($commonConnexion);
                            $log .= "\r\n\t-> Insertion d'un nouvel enregistrement dans la table chorus_noms_fichies ->";
                            $log .= "\t-> INSERT  INTO chorus_noms_fichies Values($idEchange, $nomFichier, $acronymeOrganisme, CEN)\r\n";
                            (new Atexo_Chorus_Util())->loggerInfosReception("Insertion d'un nouvel enregistrement dans la table chorus_noms_fichies : \n INSERT  INTO chorus_noms_fichies Values($idEchange, $nomFichier, $acronymeOrganisme, CEN) ");
                        } else {
                            $log .= $nomFichier." trouvé.\r\n";
                            (new Atexo_Chorus_Util())->loggerInfosReception("Fichier : $nomFichier trouve");
                        }
                    } else {
                        $log .= 'nom_fichier '.$idFichierEmetteur." Non trouvé.\r\n";
                        (new Atexo_Chorus_Util())->loggerInfosReception('nom_fichier '.$idFichierEmetteur.' Non trouvé');
                    }
                }
            }
            $log .= "\r\nParcours des LIGNES_DOSSIER ... \r\n";
            (new Atexo_Chorus_Util())->loggerInfosReception("Parcours des LIGNES_DOSSIER ... [Fichier $nomFichier] ");
            foreach ($lignesDossiers as $ligneDossier) {
                if (is_object($ligneDossier)) {
                    $idDae = $ligneDossier->getElementsByTagName('ID_DAE')->item(0)->nodeValue; //17Z1290ZZZZZ
                        $idDest = $ligneDossier->getElementsByTagName('ID_DEST')->item(0)->nodeValue; //1200003817
                        $erreur = Atexo_Util::utf8ToIso($ligneDossier->getElementsByTagName('CODE_ERREUR')->item(0)->nodeValue);
                    $libelleErreur = Atexo_Util::utf8ToIso($ligneDossier->getElementsByTagName('LIBELLE_ERREUR')->item(0)->nodeValue);

                    $log .= "\t Ligne dossier 1 \r\n";
                    (new Atexo_Chorus_Util())->loggerInfosReception("Ligne dossier 1 , [Fichier $nomFichier] ");

                    if (is_array($arrayFichiersEmmetteur) && count($arrayFichiersEmmetteur)) {
                        $log .= "\t Recherche de la décision lié au fichier ... ";
                        (new Atexo_Chorus_Util())->loggerInfosReception("Recherche de la décision lié au fichier ... , [Fichier $nomFichier] ");

                        foreach ($arrayFichiersEmmetteur as $idOrgIdEchangeIdDecision => $fichierEmetteur) {
                            $arrayKey = explode('-', $idOrgIdEchangeIdDecision);
                            $idOrganisme = $arrayKey[0];
                            $idEchange = $arrayKey[1];
                            $idDecision = $arrayKey[2];
                            $acronyme = (new Atexo_Organismes())->retrieveAccronymeOrganisme($idOrganisme);

                            $echange = CommonChorusEchangePeer::retrieveByPK($idEchange, $acronyme, $commonConnexion);
                            $isAccordCadreSad = false;
                            $pmiUniqueIdWithDecision = null;
                            if ($echange instanceof CommonChorusEchange) {
                                try {
                                    $execWS = Atexo_Util::getSfService(WebServicesExec::class);
                                    if ($echange->getUuidExterneExec()) {
                                        $contrat = $execWS->getContent('getContratByUuid', $echange->getUuidExterneExec());
                                    }
                                } catch (\Exception $exception) {
                                }
                            }
                            if ($echange instanceof CommonChorusEchange && is_array($contrat) && $echange->isTypeFluxFen211($contrat)) {
                                $isAccordCadreSad = true;
                                $pmiUniqueIdWithDecision = $echange->getIdentifiantAccordCadre();
                            }
                            if (false === $isAccordCadreSad) {
                                $pmiUniqueIdWithDecision = $echange->getIdEjAppliExt();
                            }

                            if (strpos($idDae, (string) $fichierEmetteur) || $pmiUniqueIdWithDecision == $idDae) {
                                if ($echange instanceof CommonChorusEchange) {
                                    if ($erreur) {
                                        $log .= "\r\n\t echange $idEchange de la décision $idDecision, organisme $acronyme  rejeté pour motif : $erreur / $libelleErreur ... Aucune action à faire.\r\n";
                                        (new Atexo_Chorus_Util())->loggerInfosReception("Echange (id : $idEchange , organisme : $acronyme) de la décision $idDecision rejeté pour motif : $erreur / $libelleErreur ... Aucune action à faire.");
                                        $echange->setRetourChorus('REJ');
                                        $echange->setErreurRejet($erreur.'  '.$libelleErreur);
                                        $echange->save($commonConnexion);
                                        //Début envoi mail aux agents invités et abonnés aux alertes

                                        $contrat = null;
                                        try {
                                            $execWS = Atexo_Util::getSfService(WebServicesExec::class);
                                            if ($echange->getUuidExterneExec()) {
                                                $contrat = $execWS->getContent('getContratByUuid', $echange->getUuidExterneExec());
                                            }

                                        } catch (\Exception $exception) {
                                            $erreur = "\nContrat non trouve, Reception du compte rendu, flux $nomFichier , Echange [ Id = ".$idEchange.' , Org = '.$acronymeOrganisme.' , idContrat = '.$idDecision." ] , Methode: Atexo_Chorus_Reception::receptionCompteRendu \r\n\n";
                                            $erreur .= "\nTraitement dossier suivant ...\r\n\n";
                                            (new Atexo_Chorus_Util())->loggerErreurReception($erreur);
                                            $log .= $erreur;
                                            self::writeFailedFilesInCustomLog($log, $nomFichier);
                                            $return = false;
                                            continue;
                                        }
                                        //call contrat from exec
                                        (new Atexo_Chorus_Util())->loggerInfosReception("Recherche contrat (id : $idDecision , org : $acronyme");
                                        if (is_array($contrat)) {
                                            (new Atexo_Chorus_Util())->loggerInfosReception("Contrat trouve (id : $idDecision , org : $acronyme");

                                            $message = (new Atexo_Agent_Alertes())->getCorpsMessageAlerteChorusEnvoiRejete($contrat);
                                            $objetEchange = (new Atexo_Agent_Alertes())->getIntituleMessageAlerteChorusEnvoiRejete($contrat, $echange);

                                            $guests = (new Atexo_Agent())->retrieveGuestSubscribersChorusAlerts($contrat, $acronymeOrganisme, $commonConnexion, $logger);
                                            if (is_array($guests) && count($guests)) {
                                                (new Atexo_Chorus_Util())->loggerInfosReception('Envoi email aux agents (agents trouves)');
                                                foreach ($guests as $guest) {
                                                    //Envoie du mail
                                                    if ($guest instanceof Atexo_Consultation_GuestVo) {
                                                        (new Atexo_Agent_Alertes())->sendMailAlertesAgentsChorus(Atexo_Config::getParameter('PF_MAIL_FROM', $acronymeOrganisme), $guest, $objetEchange, $message, $contrat, $echange->getNumOrdre());
                                                    }
                                                }
                                            }
                                        } else {
                                            $erreur = "\nContrat non trouve, Reception du compte rendu, flux $nomFichier , Echange [ Id = ".$idEchange.' , Org = '.$acronymeOrganisme.' , idContrat = '.$idDecision." ] , Methode: Atexo_Chorus_Reception::receptionCompteRendu \r\n\n";
                                            $erreur .= "\nTraitement dossier suivant ...\r\n\n";
                                            (new Atexo_Chorus_Util())->loggerErreurReception($erreur);
                                            $log .= $erreur;
                                            self::writeFailedFilesInCustomLog($log, $nomFichier);
                                            $return = false;
                                            continue;
                                        }

                                        $message = (new Atexo_Agent_Alertes())->getCorpsMessageAlerteChorusEnvoiRejete($contrat);
                                        $objetEchange = (new Atexo_Agent_Alertes())->getIntituleMessageAlerteChorusEnvoiRejete($contrat, $echange);

                                        $guests = (new Atexo_Agent())->retrieveGuestSubscribersChorusAlerts($contrat, $acronymeOrganisme, $commonConnexion, $logger);
                                        if (is_array($guests) && count($guests)) {
                                            foreach ($guests as $guest) {
                                                //Envoie du mail
                                                if ($guest instanceof Atexo_Consultation_GuestVo) {
                                                    (new Atexo_Agent_Alertes())->sendMailAlertesAgentsChorus(Atexo_Config::getParameter('PF_MAIL_FROM', $acronymeOrganisme), $guest, $objetEchange, $message, $contrat, $echange->getNumOrdre());
                                                }
                                            }
                                        }
                                        //Fin envoi mail aux agents invités et abonnés aux alertes
                                        (new Atexo_Chorus_Util())->loggerInfosReception("Traitement fichier suivant ...\r\n");
                                        continue;
                                    }
                                }

                                if (!$idDest) {
                                    $log .= "\t Numéro de marché (ID_DEST) non renseigné. Impossible d'accepter l'EJ\r\n";
                                    (new Atexo_Chorus_Util())->loggerInfosReception("Numéro de marché (ID_DEST) non renseigné. Impossible d'accepter l'EJ \n Traitement fichier suivant ...");
                                    continue;
                                }

                                $log .= " Trouvé\r\n";
                                (new Atexo_Chorus_Util())->loggerInfosReception("Decision trouve , [Fichier $nomFichier]");

                                $contrat = null;
                                try {
                                    $execWS = Atexo_Util::getSfService(WebServicesExec::class);
                                    if ($echange?->getUuidExterneExec()) {
                                        $contrat = $execWS->getContent('getContratByUuid', $echange->getUuidExterneExec());
                                    }
                                } catch (\Exception $exception) {
                                    $erreur = "\nContrat non trouve, Reception du compte rendu, flux $nomFichier , Echange [ Id = ".$idEchange.' , Org = '.$acronymeOrganisme.' , idContrat = '.$idDecision." ] , Methode: Atexo_Chorus_Reception::receptionCompteRendu \r\n\n";
                                    $erreur .= "\nTraitement dossier suivant ...\r\n\n";
                                    (new Atexo_Chorus_Util())->loggerErreurReception($erreur);
                                    $log .= $erreur;
                                    self::writeFailedFilesInCustomLog($log, $nomFichier);
                                    $return = false;
                                    continue;
                                }

                                #### put Contrat exec
                                $marcheInitial = false;
                                if (is_array($contrat)) {
                                    $log .= "\t Mise à jour du numéro de marché de la décision $idDecision : $idDest \r\n";
                                    (new Atexo_Chorus_Util())->loggerInfosReception("Mise à jour du numéro de marché de la décision (id = $idDecision) : $idDest");
                                    $bodyRequest = [];
                                    if ($idDest) {
                                        //TODO NUM_MARCHE
                                        $numsMArche = $contrat['numEj'];
                                        //si idDest est déjà enregistré alors il ne faut pas toucher sinon il faut rajouter le numéro de marché
                                        if (!strstr($numsMArche, (string) $idDest)) {
                                            $arrayNumMarche = explode('#', $numsMArche);
                                            if (is_array($arrayNumMarche) && count($arrayNumMarche) > 1) {
                                                $mixedS = count($arrayNumMarche);
                                                $numsMArche .= $mixedS.'_'.$idDest.'#';
                                            } else {
                                                $numsMArche = '1_'.$idDest.'#';
                                            }
                                            $bodyRequest['numEj'] = $numsMArche;
                                            if ($echange->getTypeFlux() == Atexo_Config::getParameter('TYPE_FLUX_CHORUS_FEN111')) {
                                                $bodyRequest['numero'] = $idDest;
                                            }
                                            $marcheInitial = true;
                                        }
                                    }
                                    if (!empty($bodyRequest)) {
                                        try {
                                            $bodyRequest['id'] = $echange->getId();
                                            $bodyRequest['uuidContrat'] = $echange->getUuidExterneExec();
                                            $bodyRequest['statut'] = 'COM';
                                            $execWS->getContent('updateContrat', $bodyRequest);
                                            (new Atexo_Chorus_Util())->loggerInfosReception("Contrat mis a jour avec SUCCES (idcontrat = $idDecision) : Numero contrat = $idDest , Numero EJ = $numsMArche");
                                        } catch (\Exception $exception) {
                                            (new Atexo_Chorus_Util())->loggerInfosReception(
                                                sprintf(
                                                    "Erreur lors mis a jour du contrat %s : %s , %s",
                                                    $idDecision,
                                                    $exception->getMessage(),
                                                    $exception->getTraceAsString()
                                                )
                                            );
                                        }
                                    }
                                }

                                if ($echange instanceof CommonChorusEchange && is_array($contrat)) {
                                    $log .= "\t Echange trouvé : mise à jour du statut de l'échange en 'COM'.\r\n";
                                    (new Atexo_Chorus_Util())->loggerInfosReception('Echange (id: '.$echange->getId().' , org: '.$echange->getOrganisme().') trouvé');
                                    $echange->setRetourChorus('COM');
                                    $echange->save($commonConnexion);
                                    (new Atexo_Chorus_Util())->loggerInfosReception('Echange (id: '.$echange->getId().' , org: '.$echange->getOrganisme().") : mise à jour du statut en 'COM'");
                                    //Début envoi mail aux agents invités et abonnés aux alertes
                                    if ($marcheInitial) {
                                        if ($contrat) {
                                            $objetMessage = (new Atexo_Agent_Alertes())->getTitreMessageAlerteChorusMarchesIntegresOuSupprimes($contrat, $echange);
                                            $message = (new Atexo_Agent_Alertes())->getCorpsMessageAlerteChorusMarchesIntegresOuSupprimes($contrat, Prado::localize('DEFINE_INTEGRE'));
                                            $guests = (new Atexo_Agent())->retrieveGuestSubscribersChorusAlerts($contrat, $acronymeOrganisme, $commonConnexion, $logger);
                                            if (is_array($guests) && count($guests)) {
                                                (new Atexo_Chorus_Util())->loggerInfosReception('Envoi email aux agents (agents trouves)');
                                                foreach ($guests as $guest) {
                                                    //Envoie du mail
                                                    if ($guest instanceof Atexo_Consultation_GuestVo) {
                                                        (new Atexo_Agent_Alertes())->sendMailAlertesAgentsChorus(Atexo_Config::getParameter('PF_MAIL_FROM', $acronymeOrganisme), $guest, $objetMessage, $message, $contrat, $echange->getNumOrdre());
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    //Fin envoi mail aux agents invités et abonnés aux alertes
                                } else {
                                    $erreur = "\nEchange ou Contrat non trouve, Reception du compte rendu, flux $nomFichier, Echange [ Id = ".$idEchange.' , Org = '.$acronymeOrganisme.' , idContrat = '.$idDecision."] , Methode: Atexo_Chorus_Reception::receptionCompteRendu \n Echange: ".print_r($echange, true)." \n Contrat: ".print_r($contrat, true)."\r\n\n";
                                    $erreur .= "\nTraitement dossier suivant ...\r\n\n";
                                    (new Atexo_Chorus_Util())->loggerErreurReception($erreur);
                                    $log .= $erreur;
                                    self::writeFailedFilesInCustomLog($log, $nomFichier);
                                    $return = false;
                                    continue;
                                }
                            } else {
                                $log .= <<<EOT
Non trouvé.

Erreur de coherence de donnees : L'identifiant renvoyé par CHORUS ne correspond pas à l'identifiant unique envoyé par MPE ET le contenu de la balise 'ID_FICHIER_EMETTEUR' n'est pas incluse dans l'identifiant unique.
Identifiant unique envoye par PLACE : $pmiUniqueIdWithDecision
Identifiant unique retourne par CHORUS : $idDae
Le contenu de la balise 'ID_FICHIER_EMETTEUR' retourne par CHORUS : $fichierEmetteur
EOT;

                                $log .= $erreur;
                                (new Atexo_Chorus_Util())->loggerInfosReception($erreur);
                                self::writeFailedFilesInCustomLog($log, $nomFichier);
                                $return = false;
                                continue;
                            }
                        }
                    }
                }
            }
            (new Atexo_Chorus_Util())->loggerInfosReception('Fin integration Compte Rendu , fichier '.$nomFichier);
        } catch (Exception $e) {
            if ($log) {
                $log .= "\r\n\r\n".$e;
                Atexo_Util::writeLogFile(Atexo_Config::getParameter('DIR_FILE_CHORUS_LOGS'), Atexo_Config::getParameter('DIR_FILE_CHORUS_LOGS').$nomFichier.'.log', $log);
                (new Atexo_Chorus_Util())->loggerErreurReception("Erreur survenue lors du traitement du fichier $nomFichier , Echange [ Id = ".$idEchange.' , Org = '.$acronymeOrganisme.' , idContrat = '.$idDecision." ] , Methode: Atexo_Chorus_Reception::receptionCompteRendu , \n Exception = [ ".$e->getMessage().']');
                $message = Atexo_Message::getGenericEmailFormat($log);
                Atexo_Message::simpleMail(Atexo_Config::getParameter('PF_MAIL_FROM'), 'applications.mpe@atexo.com', 'PLACE : Erreur intégration CEN', $message, '', '', false, true);
            }

            return false;
        }

        if ($log) {
            Atexo_Util::writeLogFile(Atexo_Config::getParameter('DIR_FILE_CHORUS_LOGS'), Atexo_Config::getParameter('DIR_FILE_CHORUS_LOGS').$nomFichier.'.log', $log);
        }
        echo $log;

        return $return;
    }

    public function receptionMessageSuivi($xml, $nomFichier = null)
    {
        $idContratTitulaire = null;
        $acronymeOrganisme = null;
        $log = '';
        $return = true;
        $logger = Atexo_LoggerManager::getLogger('chorus');
        try {
            (new Atexo_Chorus_Util())->loggerInfosReception('Debut integration du message FSO de suivi (fichier '.$nomFichier.')');
            $log .= "Intégration FSO  : fichier $nomFichier, le ".date('Y/m/d H:i:s')." \r\n\r\n";
            $domDocument = new DOMDocument();
            $log .= "Chargement de l'xml ... ";
            (new Atexo_Chorus_Util())->loggerInfosReception("Chargement de l'xml ... (fichier ".$nomFichier.')');
            if (!$domDocument->loadXML($xml)) {
                $erreur = "Erreur de chargement du flux $nomFichier , Methode: Atexo_Chorus_Reception::receptionMessageSuivi";
                (new Atexo_Chorus_Util())->loggerErreurReception($erreur);
                $log .= $erreur;
                self::writeFailedFilesInCustomLog($log, $nomFichier);

                return false;
            }
            $log .= "xml chargé \r\n\r\n";
            (new Atexo_Chorus_Util())->loggerInfosReception('xml chargé (fichier '.$nomFichier.')');
            $compteRendu = $domDocument->getElementsByTagName('FSO0028A')->item(0);

            if (is_object($compteRendu)) {
                $engagementsJuridiques = $compteRendu->getElementsByTagName('ENGAGEMENT_JURIDIQUE');

                foreach ($engagementsJuridiques as $ej) {
                    if (is_object($ej)) {
                        $entete = $ej->getElementsByTagName('ENTETE')->item(0);
                        if (is_object($entete)) {
                            $numeroMarche = $entete->getElementsByTagName('ID_EJ')->item(0)->nodeValue; //1200003817
                                    $numeroLongMarche = $entete->getElementsByTagName('ID_LONG')->item(0)->nodeValue; //2015.1200001032
                                    $statut = Atexo_Util::toUtf8($entete->getElementsByTagName('STATUT')->item(0)->nodeValue); //Commandé/Supprimé/Clôturé
                                    $identifiantUniquePmi = $entete->getElementsByTagName('ID_AE')->item(0)->nodeValue; //17Z1290ZZZZZ
                        }

                        $commonConnexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));

                        $log .= "\t Recuperation de l'identifiant unique : $identifiantUniquePmi , (fichier = \" . $nomFichier . \" numero marche = $numeroMarche) \r\n ";
                        (new Atexo_Chorus_Util())->loggerInfosReception("Recuperation de l'identifiant unique : $identifiantUniquePmi , (fichier = ".$nomFichier." numero marche = $numeroMarche)");

                        //$identifiantUniquePmi = trim($identifiantUniquePmi, "Z");
                        //$explodeIdentifiantUniquePmi = explode("Z", $identifiantUniquePmi);

                        //Recherche de Chorus_Echange
                        $idContratTitulaire = '';
                        $acronymeOrganisme = '';
                        $chorusEchangePourNomsFichiers = CommonChorusEchangeQuery::create()->filterByIdEjAppliExt($identifiantUniquePmi)->findOne($commonConnexion);
                        $log .= "\t Recherche de l'echange a partir de l'identifiant unique : $identifiantUniquePmi \r\n";
                        if ($chorusEchangePourNomsFichiers instanceof CommonChorusEchange) {
                            $log .= "\t Echange trouve \r\n";
                            $idContratTitulaire = $chorusEchangePourNomsFichiers->getIdDecision();
                            $acronymeOrganisme = $chorusEchangePourNomsFichiers->getOrganisme();
                        }

                        $log .= "\t Organisme = ".$acronymeOrganisme." \r\n";
                        $log .= "\t id_contrat = ".$idContratTitulaire."\r\n";
                        (new Atexo_Chorus_Util())->loggerInfosReception("Recherche du fichier pour nom_fichier = $nomFichier , id_contrat = $idContratTitulaire , organisme = $acronymeOrganisme ");

                        $c = new Criteria();
                        $c->add(CommonChorusNomsFichiersPeer::NOM_FICHIER, '%'.$nomFichier, Criteria::LIKE);
                        $c->add(CommonChorusNomsFichiersPeer::TYPE_FICHIER, 'FSO', Criteria::EQUAL);
                        $c->add(CommonChorusNomsFichiersPeer::ACRONYME_ORGANISME, $acronymeOrganisme, Criteria::EQUAL);
                        $c->add(CommonChorusNomsFichiersPeer::ID_ECHANGE, $idContratTitulaire, Criteria::EQUAL);

                        $chorusNomsFichier = CommonChorusNomsFichiersPeer::doSelectOne($c, $commonConnexion);

                        if (!$chorusNomsFichier instanceof CommonChorusNomsFichiers) {
                            (new Atexo_Chorus_Util())->loggerInfosReception("Fichier non trouve (nom_fichier = $nomFichier , id_contrat = $idContratTitulaire , organisme = $acronymeOrganisme)");
                            (new Atexo_Chorus_Util())->loggerInfosReception("Sauvegarde du fichier dans  chorus_noms_fichiers($idContratTitulaire, $nomFichier, $acronymeOrganisme, FSO)");
                            $log .= "\t Sauvegarde du fichier dans  chorus_noms_fichiers($idContratTitulaire, $nomFichier, $acronymeOrganisme, FSO) \r\n ";
                            $newChorusNomsFichier = new CommonChorusNomsFichiers();
                            $newChorusNomsFichier->setIdEchange($idContratTitulaire);
                            $newChorusNomsFichier->setNomFichier($nomFichier);
                            $newChorusNomsFichier->setAcronymeOrganisme($acronymeOrganisme);
                            $newChorusNomsFichier->setTypeFichier('FSO');
                            $newChorusNomsFichier->setDateAjout(date('Y-m-d H:i:s'));
                            $newChorusNomsFichier->save($commonConnexion);
                        } else {
                            $log .= "\t Fichier déjà traité : problème lors du traitement précédent (se référer aux logs), nouvelle tentative d'integration \r\n";
                            (new Atexo_Chorus_Util())->loggerInfosReception("Fichier déjà traité (nom_fichier = $nomFichier) : problème lors du traitement précédent (se référer aux logs), nouvelle tentative d'integration ...");
                            $newChorusNomsFichier = $chorusNomsFichier;
                        }

                        $contrat = null;
                        if ($chorusEchangePourNomsFichiers instanceof CommonChorusEchange) {
                            try {
                                $execWS = Atexo_Util::getSfService(WebServicesExec::class);
                                if ($chorusEchangePourNomsFichiers?->getUuidExterneExec()) {
                                    $contrat = $execWS->getContent('getContratByUuid', $chorusEchangePourNomsFichiers->getUuidExterneExec());
                                }
                            } catch (\Exception $exception) {
                                $erreur = "\nContrat non trouve, Fonction d'integration des Messages de suivi, flux $nomFichier, ( id_contrat = ".$idContratTitulaire.' , Org = '.$acronymeOrganisme." ) , Methode: Atexo_Chorus_Reception::receptionMessageSuivi \r\n\n";
                                $erreur .= "\nTraitement EJ suivant ...\r\n\n";
                                (new Atexo_Chorus_Util())->loggerErreurReception($erreur);
                                $log .= $erreur;
                                self::writeFailedFilesInCustomLog($log, $nomFichier);
                                $return = false;
                                continue;
                            }
                        }

                        //$echange = CommonChorusEchangePeer::retrieveByPK($idContratTitulaire, $acronymeOrganisme, $commonConnexion);

                        if (is_array($contrat)) {
                            // TODO STATUT_EJ
                            $message = '';
                            $bodyRequest = [];
                            $objetMessage = '';
                            if ($statut) {
                                $statutEjAvantMaj = $contrat['statutEJ'];
                                $arrayStatut = explode('#', $statutEjAvantMaj);
                                $mixedS = count($arrayStatut);
                                if (!(is_array(($arrayStatut)) && ($mixedS > 1))) {
                                    $mixedS = '1';
                                }
                                $statutEj = $statutEjAvantMaj.$mixedS.'_'.$numeroMarche.';'.$statut.'#';
                                $bodyRequest['statutEj'] = $statutEj;
                                if ((new Atexo_Consultation_Contrat())->isTypeContratAcSad($contrat['type']['codeExterne'])) {
                                    $bodyRequest['numeroLong'] = $numeroLongMarche;
                                }

                                //Stocker la valeur de $mixedS dans la table 'ChorusNomsFichiers'
                                $newChorusNomsFichier->setNumeroOrdre($mixedS);
                                $newChorusNomsFichier->save($commonConnexion);
                                (new Atexo_Chorus_Util())->loggerInfosReception("Fichier mis a jour avec SUCCES (numero d'ordre : $mixedS ");
                                $log .= "Fichier mis a jour avec SUCCES (numero d'ordre : $mixedS ";
                            }
                            if (!empty($bodyRequest)) {
                                try {
                                    $bodyRequest['id'] = $chorusEchangePourNomsFichiers->getId();
                                    $bodyRequest['uuidContrat'] = $chorusEchangePourNomsFichiers->getUuidExterneExec();
                                    $execWS = Atexo_Util::getSfService(WebServicesExec::class);
                                    $execWS->getContent('updateContrat', $bodyRequest);
                                    (new Atexo_Chorus_Util())->loggerInfosReception("Contrat mis a jour avec SUCCES (idcontrat = " . $chorusEchangePourNomsFichiers->getUuidExterneExec());
                                } catch (\Exception $exception) {
                                    (new Atexo_Chorus_Util())->loggerInfosReception(
                                        sprintf(
                                            "Erreur lors mis a jour du contrat %s : %s , %s",
                                            $chorusEchangePourNomsFichiers->getUuidExterneExec(),
                                            $exception->getMessage(),
                                            $exception->getTraceAsString()
                                        )
                                    );
                                    throw $exception;
                                }
                            }

                            //Début envoi mail aux agents invités et abonnés aux alertes
                            if (
                                Atexo_Module::isEnabled('InterfaceChorusPmi', $acronymeOrganisme) &&
                                self::verifierConditionsEnvoiMailFso($statutEjAvantMaj, $numeroMarche, $statut)
                            ) {
                                //Début saisie message confirmation pour agents
                                if (strstr($statut, (string) Atexo_Util::toUtf8(Atexo_Config::getParameter('CHORUS_STATUT_EJ_COMMANDE')))
                                            || strstr($statut, (string) Atexo_Util::toUtf8(Atexo_Config::getParameter('CHORUS_STATUT_EJ_CLOTURE')))
                                        ) {
                                    $message = (new Atexo_Agent_Alertes())->getCorpsMessageAlerteChorusMarchesCommandesOuClotures($contrat, $statut);
                                    if (strstr($statut, (string) Atexo_Util::toUtf8(Atexo_Config::getParameter('CHORUS_STATUT_EJ_COMMANDE')))) {
                                        $objetMessage = (new Atexo_Agent_Alertes())->getTitreMessageAlerteChorusMarchesCommandesCloturesSupprimes($contrat, Prado::localize('RETOUR_CHORUS_MARCHE_COMMANDE'));
                                    } elseif (strstr($statut, (string) Atexo_Util::toUtf8(Atexo_Config::getParameter('CHORUS_STATUT_EJ_CLOTURE')))) {
                                        $objetMessage = (new Atexo_Agent_Alertes())->getTitreMessageAlerteChorusMarchesCommandesCloturesSupprimes($contrat, Prado::localize('RETOUR_CHORUS_MARCHE_CLOTURE'));
                                    }
                                } elseif (strstr($statut, (string) Atexo_Util::toUtf8(Atexo_Config::getParameter('CHORUS_STATUT_EJ_SUPPRIME')))) {
                                    $message = (new Atexo_Agent_Alertes())->getCorpsMessageAlerteChorusMarchesIntegresOuSupprimes($contrat, Prado::localize('DEFINE_SUPPRIME'));
                                    $objetMessage = (new Atexo_Agent_Alertes())->getTitreMessageAlerteChorusMarchesCommandesCloturesSupprimes($contrat, Prado::localize('RETOUR_CHORUS_MARCHE_SUPPRIME'));
                                } else {
                                    $message = (new Atexo_Agent_Alertes())->getCorpsMessageAlerteChorusMarchesIntegresOuSupprimes($contrat, $statut);
                                    $objetMessage = (new Atexo_Agent_Alertes())->getTitreMessageAlerteChorusMarchesCommandesCloturesSupprimes($contrat, $statut);
                                }
                                //Fin saisie message confirmation pour agents

                                $guests = (new Atexo_Agent())->retrieveGuestSubscribersChorusAlerts($contrat, $acronymeOrganisme, $commonConnexion, $logger);
                                if (is_array($guests) && count($guests)) {
                                    $log .= "\nEnvoi email aux agents (agents trouves)";
                                    (new Atexo_Chorus_Util())->loggerInfosReception('Envoi email aux agents (agents trouves)');
                                    foreach ($guests as $guest) {
                                        //Envoie du mail
                                        if ($guest instanceof Atexo_Consultation_GuestVo) {
                                            (new Atexo_Agent_Alertes())->sendMailAlertesAgentsChorus(Atexo_Config::getParameter('PF_MAIL_FROM', $acronymeOrganisme), $guest, $objetMessage, $message, $contrat, '');
                                        }
                                    }
                                }
                            }

                            //Fin envoi mail aux agents invités et abonnés aux alertes
                        } else {
                            $erreur = "\nContrat non trouve, Fonction d'integration des Messages de suivi, flux $nomFichier, ( id_contrat = ".$idContratTitulaire.' , Org = '.$acronymeOrganisme." ) , Methode: Atexo_Chorus_Reception::receptionMessageSuivi \r\n\n";
                            $erreur .= "\nTraitement EJ suivant ...\r\n\n";
                            (new Atexo_Chorus_Util())->loggerErreurReception($erreur);
                            $log .= $erreur;
                            self::writeFailedFilesInCustomLog($log, $nomFichier);
                            $return = false;
                            continue;
                        }
                    }
                }
            } else {
                $erreur = "La balise 'FSO0028A' n'a pas ete trouvee dans l'xml.\n\nVerifier que l'xml a ete correctement chargee. \n\nNom flux : $nomFichier";
                (new Atexo_Chorus_Util())->loggerErreurReception($erreur);
                $log .= $erreur;
                self::writeFailedFilesInCustomLog($log, $nomFichier);

                return false;
            }
            (new Atexo_Chorus_Util())->loggerInfosReception('Fin integration du message de suivi (fichier '.$nomFichier.')');
        } catch (Exception $e) {
            if ($log) {
                $log .= "\r\n\r\n".$e;
                (new Atexo_Chorus_Util())->loggerErreurReception("Erreur survenue lors du traitement du fichier $nomFichier, ( id_contrat = ".$idContratTitulaire.' , Org = '.$acronymeOrganisme." ) , Methode: Atexo_Chorus_Reception::receptionMessageSuivi , \n Exception : ".$e->getMessage());
                Atexo_Util::writeLogFile(Atexo_Config::getParameter('DIR_FILE_CHORUS_LOGS'), Atexo_Config::getParameter('DIR_FILE_CHORUS_LOGS').$nomFichier.'.log', $log);
                $message = Atexo_Message::getGenericEmailFormat($log);
                Atexo_Message::simpleMail(Atexo_Config::getParameter('PF_MAIL_FROM'), 'applications.mpe@atexo.com', 'PLACE : Erreur intégration FSO', $message, '', '', false, true);
            }

            return false;
        }

        if ($log) {
            Atexo_Util::writeLogFile(Atexo_Config::getParameter('DIR_FILE_CHORUS_LOGS'), Atexo_Config::getParameter('DIR_FILE_CHORUS_LOGS').$nomFichier.'.log', $log);
        }
        echo $log;
        //return $return;
        return true; //le but de cette modification est de forcer le deplacement de tous les FSO dans le repertoire Reception/Archives
    }

    public function receptionCompteRenduCIR($xml, $nomFichier = null)
    {
        $idEchange = null;
        $acronymeOrganisme = null;
        $idDecision = null;
        $log = '';

        try {
            (new Atexo_Chorus_Util())->loggerInfosReception('Debut integration du compte rendu CIR [fichier '.$nomFichier.']');
            $log .= "Intégration CIR  : fichier $nomFichier, le ".date('Y/m/d H:i:s')." \r\n\r\n";
            $domDocument = new DOMDocument();
            $log .= "Chargement de l'xml ... ";
            (new Atexo_Chorus_Util())->loggerInfosReception("Chargement de l'xml ...  [fichier ".$nomFichier.']');
            if (!$domDocument->loadXML($xml)) {
                $erreur = "Erreur de chargement du flux $nomFichier , Methode: Atexo_Chorus_Reception::receptionCompteRenduCIR";
                (new Atexo_Chorus_Util())->loggerErreurReception($erreur);
                $log .= $erreur;
                self::writeFailedFilesInCustomLog($log, $nomFichier);

                return false;
            }
            $log .= "xml chargé \r\n\r\n";
            (new Atexo_Chorus_Util())->loggerInfosReception('xml chargé  [fichier '.$nomFichier.']');
            $compteRendu = $domDocument->getElementsByTagName('COMPTE_RENDU')->item(0);
            if (!is_object($compteRendu)) {
                $erreur = "La balise 'COMPTE_RENDU' n'existe pas dans le flux $nomFichier, Methode: Atexo_Chorus_Reception::receptionCompteRenduCIR";
                (new Atexo_Chorus_Util())->loggerErreurReception($erreur);
                $log .= $erreur;
                self::writeFailedFilesInCustomLog($log, $nomFichier);

                return false;
            }

            $enteteIrrecevable = $compteRendu->getElementsByTagName('ENTETE_IRRECEVABLE')->item(0);
            if (is_object($enteteIrrecevable)) {
                $log .= "\r\n\tEntete irrecevable : \r\n";
                (new Atexo_Chorus_Util())->loggerInfosReception('Entete irrecevable :  [fichier '.$nomFichier.']');
                $idFichierEmetteur = $enteteIrrecevable->getElementsByTagName('ID_FICHIER_EMETTEUR')->item(0)->nodeValue;

                $commonConnexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));

                $log .= "\t-> Recherche de l'echange où nom_fichier like %".$idFichierEmetteur.' dans la table common_db.chorus_noms_fichiers ... ';
                (new Atexo_Chorus_Util())->loggerInfosReception("Recherche de l'echange où nom_fichier like %".$idFichierEmetteur.' dans la table common_db.chorus_noms_fichiers ...');
                $c = new Criteria();
                $c->add(CommonChorusNomsFichiersPeer::NOM_FICHIER, '%'.$idFichierEmetteur, Criteria::LIKE);
                $c->add(CommonChorusNomsFichiersPeer::TYPE_FICHIER, Atexo_Config::getParameter('TYPE_FLUX_FIR_CHORUS'), Criteria::EQUAL);
                $chorusNomsFichier = CommonChorusNomsFichiersPeer::doSelectOne($c, $commonConnexion);
                if ($chorusNomsFichier instanceof CommonChorusNomsFichiers) {
                    $log .= 'Fichier '.$idFichierEmetteur."  Trouvé.\r\n";
                    (new Atexo_Chorus_Util())->loggerInfosReception('Fichier '.$idFichierEmetteur.'  Trouvé.');
                    $acronymeOrganisme = $chorusNomsFichier->getAcronymeOrganisme();
                    $organisme = Atexo_Organismes::retrieveOrganismeByAcronyme($acronymeOrganisme);
                    $idEchange = $chorusNomsFichier->getIdEchange();

                    $log .= "\t\t-> Id de l'échange : $idEchange\r\n";
                    $log .= "\t\t-> Id de l'organisme : ".$organisme->getId()."\r\n";
                    $log .= "\t\t-> Acronyme de l'organisme : $acronymeOrganisme\r\n";

                    $log .= "\t-> Recherche de l'échange $idEchange ...";
                    (new Atexo_Chorus_Util())->loggerInfosReception("Recherche de l'échange (id = $idEchange , organisme = $acronymeOrganisme)");

                    $echange = CommonChorusEchangePeer::retrieveByPK($idEchange, $acronymeOrganisme, $commonConnexion);

                    if ($echange instanceof CommonChorusEchange) {
                        $log .= " Echange Trouvé.\r\n";
                        (new Atexo_Chorus_Util())->loggerInfosReception("Echange (id = $idEchange , organisme = $acronymeOrganisme) Trouve");
                    } else {
                        $log .= " Echange Non Trouvé.\r\n";
                        (new Atexo_Chorus_Util())->loggerInfosReception("Echange (id = $idEchange , organisme = $acronymeOrganisme) Non Trouve");
                        (new Atexo_Chorus_Util())->loggerErreurReception("Echange (id = $idEchange , organisme = $acronymeOrganisme) Non Trouve , Methode: Atexo_Chorus_Reception::receptionCompteRenduCIR");
                    }
                    $log .= "\t-> Recherche du fichier en cours dans la table chorus_noms_fichiers ... ";
                    (new Atexo_Chorus_Util())->loggerInfosReception("Recherche du fichier en cours dans la table chorus_noms_fichiers ... [ fichier : $nomFichier ]");
                    $chorusNomsFichier = CommonChorusNomsFichiersPeer::retrieveByPK($idEchange, $nomFichier, $commonConnexion);
                    if (!$chorusNomsFichier instanceof CommonChorusNomsFichiers) {
                        $log .= ' Fichier '.$nomFichier.' Non Trouvé.';
                        (new Atexo_Chorus_Util())->loggerInfosReception('Fichier '.$nomFichier.' Non Trouvé.');
                        $newChorusNomsFichier = new CommonChorusNomsFichiers();
                        $newChorusNomsFichier->setIdEchange($idEchange);
                        $newChorusNomsFichier->setNomFichier($nomFichier);
                        $newChorusNomsFichier->setAcronymeOrganisme($acronymeOrganisme);
                        $newChorusNomsFichier->setTypeFichier(Atexo_Config::getParameter('TYPE_FLUX_CIR_CHORUS'));
                        $newChorusNomsFichier->setDateAjout(date('Y-m-d H:i:s'));
                        $newChorusNomsFichier->save($commonConnexion);
                        $log .= "\r\n\t-> Insertion d'un nouvel enregistrement dans la table chorus_noms_fichies ->";
                        $log .= "\t-> INSERT  INTO chorus_noms_fichies Values($idEchange, $nomFichier, $acronymeOrganisme, CIR)\r\n";
                        (new Atexo_Chorus_Util())->loggerInfosReception("Insertion d'un nouvel enregistrement dans la table chorus_noms_fichies");
                        (new Atexo_Chorus_Util())->loggerInfosReception("INSERT  INTO chorus_noms_fichies Values($idEchange, $nomFichier, $acronymeOrganisme, CIR)");
                    } else {
                        $log .= "\t Fichier déjà traité : problème lors du traitement précédent (se référer aux logs), nouvelle tentative d'integration \r\n";
                        (new Atexo_Chorus_Util())->loggerInfosReception("Fichier déjà traité : problème lors du traitement précédent (se référer aux logs), nouvelle tentative d'integration");
                    }
                    $erreur = Prado::localize('MESSAGE_REJETE_PROBLEME_TECHNIQUE');
                    $libelleErreur = Atexo_Util::utf8ToIso($enteteIrrecevable->getElementsByTagName('LIBELLE_ERREUR')->item(0)->nodeValue);
                    if ($echange instanceof CommonChorusEchange) {
                        $idDecision = $echange->getIdDecision();
                        $log .= "\r\n\t echange $idEchange de la décision $idDecision, organisme $acronymeOrganisme  rejeté pour motif :  ".$libelleErreur." /$erreur .\r\n";
                        (new Atexo_Chorus_Util())->loggerInfosReception("Echange (id = $idEchange , organisme = $acronymeOrganisme) de la décision $idDecision rejeté pour motif :  ".$libelleErreur." /$erreur ");
                        $echange->setStatutechange(Atexo_Config::getParameter('STATUT_ECHANGE_CHORUS_REJETE_PROBLEME_TECHNIQUE'));
                        $echange->setRetourChorus(Atexo_Config::getParameter('CHORUS_RETOUR_SANS_OBJET'));
                        $echange->setErreurRejet($erreur);
                        $echange->save($commonConnexion);
                        self::sendCourrierRejetChorusAgent($echange, $idDecision, $acronymeOrganisme, $commonConnexion);
                    }
                } else {
                    $log .= 'Echange où nom_fichier like %'.$idFichierEmetteur." Non trouvé.\r\n";
                    (new Atexo_Chorus_Util())->loggerInfosReception('Echange où nom_fichier like %'.$idFichierEmetteur.' Non trouvé.');
                }
            }
            (new Atexo_Chorus_Util())->loggerInfosReception('Fin integration du compte rendu CIR [fichier '.$nomFichier.']');
        } catch (\Exception $e) {
            (new Atexo_Chorus_Util())->loggerErreurReception("Erreur survenue lors du traitement du fichier $nomFichier, Echange [ Id = ".$idEchange.' , Org = '.$acronymeOrganisme.' , idContrat = '.$idDecision." ] , Methode: Atexo_Chorus_Reception::receptionCompteRenduCIR , \n Exception : ".$e->getMessage());
            if ($log) {
                $log .= "\r\n\r\n".$e->getMessage();
                Atexo_Util::writeLogFile(Atexo_Config::getParameter('DIR_FILE_CHORUS_LOGS'), Atexo_Config::getParameter('DIR_FILE_CHORUS_LOGS').$nomFichier.'.log', $log);
                $message = Atexo_Message::getGenericEmailFormat($log);
                Atexo_Message::simpleMail(Atexo_Config::getParameter('PF_MAIL_FROM'), 'applications.mpe@atexo.com', 'PLACE : Erreur intégration FIR', $message, '', '', false, true);
            }

            return false;
        }
        if ($log) {
            Atexo_Util::writeLogFile(Atexo_Config::getParameter('DIR_FILE_CHORUS_LOGS'), Atexo_Config::getParameter('DIR_FILE_CHORUS_LOGS').$nomFichier.'.log', $log);
        }
        echo $log;

        return true;
    }

    /*
     * Permet d'envoyer au agent invité un message de rejet
     */
    public function sendCourrierRejetChorusAgent($echange, $idDecision, $acronymeOrganisme, $commonConnexion, $corpsMessagePerso = false, $mailToSupport = false)
    {
        $idEchange = null;
        $logger = Atexo_LoggerManager::getLogger('chorus');
        try {
            if ($echange instanceof CommonChorusEchange) {
                $idEchange = $echange->getId();

                $contrat = null;
                try {
                    $execWS = Atexo_Util::getSfService(WebServicesExec::class);
                    if ($echange?->getUuidExterneExec()) {
                        $contrat = $execWS->getContent('getContratByUuid', $echange->getUuidExterneExec());
                    }
                } catch (\Exception $exception) {
                    (new Atexo_Chorus_Util())->loggerErreurReception('Contrat non trouve, Fonction: Envoi courrier rejet Chorus, Echange [ id = '.$idEchange.' , Org = '.$acronymeOrganisme.' , ContratId = '.$idDecision.'] , Methode: Atexo_Chorus_Reception::sendCourrierRejetChorusAgent');

                    return false;
                }

                if (is_array($contrat)) {
                    $corpsMessage = $corpsMessage = (new Atexo_Agent_Alertes())->getCorpsMessageAlerteChorusEnvoiRejete($contrat);
                    $objetEchange = (new Atexo_Agent_Alertes())->getIntituleMessageAlerteChorusEnvoiRejete($contrat, $echange);

                    $guests = (new Atexo_Agent())->retrieveGuestSubscribersChorusAlerts($contrat, $acronymeOrganisme, $commonConnexion, $logger);
                    if (is_array($guests) && count($guests)) {
                        foreach ($guests as $guest) {
                            //Envoie du mail
                            if ($guest instanceof Atexo_Consultation_GuestVo) {
                                (new Atexo_Agent_Alertes())->sendMailAlertesAgentsChorus(Atexo_Config::getParameter('PF_MAIL_FROM', $acronymeOrganisme), $guest, $objetEchange, $corpsMessage, $contrat, $echange->getNumOrdre());
                            }
                        }
                    }
                    if ($mailToSupport) {
                        //Envoie du mail au support
                        Atexo_Message::simpleMail(Atexo_Config::getParameter('PF_MAIL_FROM', $acronymeOrganisme), Atexo_Config::getParameter('MAIL_SUPPORT_CHORUS'), $objetEchange, $corpsMessage, '', '', false, true);
                    }
                } else {
                    (new Atexo_Chorus_Util())->loggerErreurReception('Contrat non trouve, Fonction: Envoi courrier rejet Chorus, Echange [ id = '.$idEchange.' , Org = '.$acronymeOrganisme.' , ContratId = '.$idDecision.'] , Methode: Atexo_Chorus_Reception::sendCourrierRejetChorusAgent');

                    return false;
                }
            } else {
                (new Atexo_Chorus_Util())->loggerErreurReception('Echange non trouve, Fonction: Envoi courrier rejet Chorus, Contrat Echange [ Org = '.$acronymeOrganisme.' , ContratId = '.$idDecision.'] , Methode: Atexo_Chorus_Reception::sendCourrierRejetChorusAgent');
            }
        } catch (\Exception $e) {
            (new Atexo_Chorus_Util())->loggerErreurReception("Erreur survenue lors de l'envoi du mail aux agents : id = ".$idEchange.' , Org = '.$acronymeOrganisme.' , ContratId = '.$idDecision." ] , Methode: Atexo_Chorus_Reception::sendCourrierRejetChorusAgent , \n Exception : ".$e->getMessage());

            return false;
        }
    }

    public function receptionCompteRenduFIR($xml, $nomFichier = null)
    {
        $erreur = null;
        $erreurExiste = null;
        $idFonctionnel = null;
        $organisationAchat = null;
        $rIdFonctionnel = null;
        $rIdTechnique = null;
        $rOA = null;
        $rTbOA = null;
        $idEchange = null;
        $acronymeOrganisme = null;
        $idDecision = null;
        $log = '';
        try {
            (new Atexo_Chorus_Util())->loggerInfosReception('Debut integration du compte rendu FIR [fichier '.$nomFichier.']');
            $longName = strlen($nomFichier);
            $positionPart = strpos($nomFichier, '.xml');
            if ($positionPart) {
                $nomFichier = substr($nomFichier, 0, -($longName - $positionPart));
            }
            $nomFichier = substr($nomFichier, 0, -6);
            $commonConnexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $log .= '-> Recherche du fichier en cours dans la table chorus_noms_fichiers ... ';
            (new Atexo_Chorus_Util())->loggerInfosReception('Recherche du fichier en cours dans la table chorus_noms_fichiers ... , [fichier '.$nomFichier.']');
            $c = new Criteria();
            $c->add(CommonChorusNomsFichiersPeer::NOM_FICHIER, '%'.$nomFichier, Criteria::LIKE);
            $c->add(CommonChorusNomsFichiersPeer::TYPE_FICHIER, Atexo_Config::getParameter('TYPE_FLUX_FIR_CHORUS'), Criteria::EQUAL);
            $chorusNomsFichier = CommonChorusNomsFichiersPeer::doSelectOne($c, $commonConnexion);
            if ($chorusNomsFichier instanceof CommonChorusNomsFichiers) {
                $log .= ' Fichier '.$nomFichier.' Trouvé.';
                (new Atexo_Chorus_Util())->loggerInfosReception('Fichier '.$nomFichier.' Trouvé.');
                $acronymeOrganisme = $chorusNomsFichier->getAcronymeOrganisme();
                $organisme = Atexo_Organismes::retrieveOrganismeByAcronyme($acronymeOrganisme);
                $idEchange = $chorusNomsFichier->getIdEchange();

                $log .= "\t\t-> Id de l'échange : $idEchange\r\n";
                $log .= "\t\t-> Id de l'organisme : ".$organisme->getId()."\r\n";
                $log .= "\t\t-> Acronyme de l'organisme : $acronymeOrganisme\r\n";

                $log .= "\t-> Recherche de l'échange $idEchange ...";
                (new Atexo_Chorus_Util())->loggerInfosReception("Recherche de l'échange (id = $idEchange , organisme = $acronymeOrganisme)");

                $echange = CommonChorusEchangePeer::retrieveByPK($idEchange, $acronymeOrganisme, $commonConnexion);

                if ($echange instanceof CommonChorusEchange) {
                    $log .= " Echange Trouvé.\r\n";
                    (new Atexo_Chorus_Util())->loggerInfosReception("Echange (id = $idEchange , organisme = $acronymeOrganisme) Trouvé.");
                    $log .= "Intégration FIR  : fichier $nomFichier, le ".date('Y/m/d H:i:s')." \r\n\r\n";
                    (new Atexo_Chorus_Util())->loggerInfosReception("Intégration FIR  : fichier $nomFichier, le ".date('Y/m/d H:i:s'));
                    $domDocument = new DOMDocument();
                    $log .= "Chargement de l'xml ... ";
                    (new Atexo_Chorus_Util())->loggerInfosReception("Chargement de l'xml ... , [fichier ".$nomFichier.']');
                    if (!$domDocument->loadXML($xml)) {
                        $erreur = "Erreur de chargement du flux $nomFichier , Methode: Atexo_Chorus_Reception::receptionCompteRenduFIR";
                        (new Atexo_Chorus_Util())->loggerErreurReception($erreur);
                        $log .= $erreur;
                        self::writeFailedFilesInCustomLog($log, $nomFichier);

                        return false;
                    }
                    $log .= "xml chargé \r\n\r\n";
                    (new Atexo_Chorus_Util())->loggerInfosReception('xml chargé , [fichier '.$nomFichier.']');
                    $controleTiers = $domDocument->getElementsByTagName('ControleTiers')->item(0);
                    if (is_object($controleTiers)) {
                        $tiers = $controleTiers->getElementsByTagName('Tiers');
                        $erreurExiste = false;
                        $erreur = '';
                        foreach ($tiers as $tier) {
                            if (is_object($tier)) {
                                $tag = $tier->getElementsByTagName('IdFonctionnel')->item(0);
                                $idFonctionnel = Atexo_Util::atexoHtmlEntities((new Atexo_Config())->toPfEncoding($tag->nodeValue));
                                $tag = $tier->getElementsByTagName('OA')->item(0);
                                $organisationAchat = Atexo_Util::atexoHtmlEntities((new Atexo_Config())->toPfEncoding($tag->nodeValue));
                                $tag = $tier->getElementsByTagName('R_IdFonctionnel')->item(0);
                                $rIdFonctionnel = Atexo_Util::atexoHtmlEntities((new Atexo_Config())->toPfEncoding($tag->nodeValue));
                                $tag = $tier->getElementsByTagName('R_IdTechnique')->item(0);
                                $rIdTechnique = Atexo_Util::atexoHtmlEntities((new Atexo_Config())->toPfEncoding($tag->nodeValue));
                                $tag = $tier->getElementsByTagName('R_OA')->item(0);
                                $rOA = Atexo_Util::atexoHtmlEntities((new Atexo_Config())->toPfEncoding($tag->nodeValue));
                                $tag = $tier->getElementsByTagName('R_TbOA')->item(0);
                            }
                            $rTbOA = Atexo_Util::atexoHtmlEntities((new Atexo_Config())->toPfEncoding($tag->nodeValue));
                            (new Atexo_Chorus_Util())->loggerInfosReception("Tier (IdFonctionnel : $idFonctionnel , OA : $organisationAchat , R_IdFonctionnel : $rIdFonctionnel , R_IdTechnique : $rIdTechnique , R_OA : $rOA , R_TbOA : $rTbOA ) , [fichier ".$nomFichier.']');

                            if ($idFonctionnel != $rIdFonctionnel) {
                                if ($rIdFonctionnel > 1) {
                                    (new Atexo_Chorus_Echange())->creerTierRejete($idEchange, $idFonctionnel, Atexo_Config::getParameter('STATUT_TIER_CHORUS_EXISTE_PLUSIEURS'), $organisationAchat, $commonConnexion);
                                    $erreur .= "\n->Plusieurs tiers fournisseurs sont référencés dans Chorus pour le SIRET : ".$idFonctionnel." sur l'organisation d'achat ".$organisationAchat;
                                    (new Atexo_Chorus_Util())->loggerInfosReception("Plusieurs tiers fournisseurs sont référencés dans Chorus pour le SIRET :  $idFonctionnel sur l'organisation d'achat $organisationAchat (IdFonctionnel : $idFonctionnel , OA : $organisationAchat , R_IdFonctionnel : $rIdFonctionnel , R_IdTechnique : $rIdTechnique , R_OA : $rOA , R_TbOA : $rTbOA )");
                                    $erreurExiste = true;
                                } else {
                                    (new Atexo_Chorus_Echange())->creerTierRejete($idEchange, $idFonctionnel, Atexo_Config::getParameter('STATUT_TIER_CHORUS_N_EXISTE_PAS'), $organisationAchat, $commonConnexion);
                                    $erreur .= "\n->Le tier : ".$idFonctionnel." n'existe pas dans CHORUS";
                                    (new Atexo_Chorus_Util())->loggerInfosReception("Le tier : $idFonctionnel  n'existe pas dans CHORUS (IdFonctionnel : $idFonctionnel , OA : $organisationAchat , R_IdFonctionnel : $rIdFonctionnel , R_IdTechnique : $rIdTechnique , R_OA : $rOA , R_TbOA : $rTbOA )");
                                    $erreurExiste = true;
                                }
                            } elseif ($rIdTechnique && '1' === $rOA) {
                                (new Atexo_Chorus_Echange())->creerTierRejete($idEchange, $idFonctionnel, Atexo_Config::getParameter('STATUT_TIER_CHORUS_NON_ETENDU'), $organisationAchat, $commonConnexion);
                                $erreurExiste = true;
                                $erreur .= "\n->Le tier : ".$idFonctionnel." existe  dans CHORUS mais n'est pas étendu à l'organisation d'achat ".$organisationAchat;
                                (new Atexo_Chorus_Util())->loggerInfosReception("Le tier : $idFonctionnel existe  dans CHORUS mais n'est pas étendu à l'organisation d'achat $organisationAchat (IdFonctionnel : $idFonctionnel , OA : $organisationAchat , R_IdFonctionnel : $rIdFonctionnel , R_IdTechnique : $rIdTechnique , R_OA : $rOA , R_TbOA : $rTbOA )");
                            } elseif ($rIdTechnique && '0' == $rOA && 'Y' == $rTbOA) {
                                (new Atexo_Chorus_Echange())->creerTierRejete($idEchange, $idFonctionnel, Atexo_Config::getParameter('STATUT_TIER_CHORUS_BLOQUE'), $organisationAchat, $commonConnexion);
                                $erreurExiste = true;
                                $erreur .= "\n->Le tier : ".$idFonctionnel." existe  dans CHORUS mais il est bloqué dans l'organisation d'achat ".$organisationAchat;
                                (new Atexo_Chorus_Util())->loggerInfosReception("Le tier : $idFonctionnel existe  dans CHORUS mais il est bloqué dans l'organisation d'achat $organisationAchat (IdFonctionnel : $idFonctionnel , OA : $organisationAchat , R_IdFonctionnel : $rIdFonctionnel , R_IdTechnique : $rIdTechnique , R_OA : $rOA , R_TbOA : $rTbOA )");
                            } elseif ($rIdTechnique && '0' == $rOA && 'N' == $rTbOA) {
                                //Bien
                            } elseif (($idFonctionnel != $rIdFonctionnel) || !$rIdTechnique || ('0' != $rOA) || ('N' != $rTbOA)) {
                                (new Atexo_Chorus_Echange())->creerTierRejete($idEchange, $idFonctionnel, Atexo_Config::getParameter('STATUT_TIER_CHORUS_ERRONE'), $organisationAchat, $commonConnexion);
                                $erreurExiste = true;
                                $erreur .= "\n->Le tier : ".$idFonctionnel."  n'a pa pu être vérifiés  dans CHORUS ";
                                (new Atexo_Chorus_Util())->loggerInfosReception("Le tier : $idFonctionnel n'a pa pu être vérifié  dans CHORUS (IdFonctionnel : $idFonctionnel , OA : $organisationAchat , R_IdFonctionnel : $rIdFonctionnel , R_IdTechnique : $rIdTechnique , R_OA : $rOA , R_TbOA : $rTbOA )");
                            }
                        }
                    }
                    $log .= $erreur;
                    if ($erreurExiste) {
                        $idDecision = $echange->getIdDecision();
                        $log .= "\r\n\t Mise à jour du statut echange à rejeté problème fournisseur idEchange  $idEchange de la décision $idDecision, organisme $acronymeOrganisme  \r\n";
                        (new Atexo_Chorus_Util())->loggerInfosReception("Mise à jour du statut echange à rejeté problème fournisseur idEchange  $idEchange de la décision $idDecision, organisme $acronymeOrganisme (IdFonctionnel : $idFonctionnel , OA : $organisationAchat , R_IdFonctionnel : $rIdFonctionnel , R_IdTechnique : $rIdTechnique , R_OA : $rOA , R_TbOA : $rTbOA )");
                        $echange->setStatutechange(Atexo_Config::getParameter('STATUT_ECHANGE_CHORUS_REJETE_PROBLEME_FOURNISSEUR'));
                        $echange->setRetourChorus(Atexo_Config::getParameter('CHORUS_RETOUR_SANS_OBJET'));
                        $echange->save($commonConnexion);
                        self::sendCourrierRejetChorusAgent($echange, $idDecision, $acronymeOrganisme, $commonConnexion);
                    } else {
                        $log .= "\n Mise à jour du statut echange à envoi planifié ";
                        (new Atexo_Chorus_Util())->loggerInfosReception("Mise à jour du statut echange à envoi planifié (IdFonctionnel : $idFonctionnel , OA : $organisationAchat , R_IdFonctionnel : $rIdFonctionnel , R_IdTechnique : $rIdTechnique , R_OA : $rOA , R_TbOA : $rTbOA )");
                        $echange->setStatutechange(Atexo_Config::getParameter('STATUT_ECHANGE_CHORUS_FLUX_A_GENERER'));
                        $echange->setTypeFluxAEnvoyer(Atexo_Config::getParameter('TYPE_FLUX_CHORUS_FEN111'));
                        $echange->setRetourChorus(Atexo_Config::getParameter('CHORUS_RETOUR_SANS_OBJET'));
                        $echange->save($commonConnexion);
                    }
                } else {
                    $log .= " Echange Non Trouvé.\r\n";
                    (new Atexo_Chorus_Util())->loggerErreurReception("Echange [ Id = $idEchange , Org =  $acronymeOrganisme ] Non Trouve , Methode: Atexo_Chorus_Reception::receptionCompteRenduFIR ");
                }
            } else {
                $log .= "\n File $nomFichier Non Trouvé.\r\n";
                (new Atexo_Chorus_Util())->loggerInfosReception("Fichier $nomFichier Non Trouvé.");
            }
            (new Atexo_Chorus_Util())->loggerInfosReception('Fin integration du compte rendu FIR [fichier '.$nomFichier.']');
        } catch (Exception $e) {
            (new Atexo_Chorus_Util())->loggerErreurReception("Erreur survenue lors du traitement du fichier $nomFichier, Echange [ Id = ".$idEchange.' , Org = '.$acronymeOrganisme.' , idContrat = '.$idDecision." ] , Methode: Atexo_Chorus_Reception::receptionCompteRenduFIR , \n Exception : ".$e->getMessage());
            if ($log) {
                $log .= "\r\n\r\n".$e->getMessage();
                Atexo_Util::writeLogFile(Atexo_Config::getParameter('DIR_FILE_CHORUS_LOGS'), Atexo_Config::getParameter('DIR_FILE_CHORUS_LOGS').$nomFichier.'.log', $log);
                $message = Atexo_Message::getGenericEmailFormat($log);
                Atexo_Message::simpleMail(Atexo_Config::getParameter('PF_MAIL_FROM'), 'applications.mpe@atexo.com', 'PLACE : Erreur intégration CR_FIR', $message, '', '', false, true);
            }

            return false;
        }

        if ($log) {
            Atexo_Util::writeLogFile(Atexo_Config::getParameter('DIR_FILE_CHORUS_LOGS'), Atexo_Config::getParameter('DIR_FILE_CHORUS_LOGS').$nomFichier.'.log', $log);
        }

        echo $log;

        return true;
    }

    /**
     * Permet d'ecrire les fichiers ayant echoues lors du traitement dans les logs.
     *
     * @param string $log : les lors
     * @param $nomFichier : nom du fichier
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place-objet-contrat
     *
     * @copyright Atexo 2016
     */
    private function writeFailedFilesInCustomLog($log, $nomFichier)
    {
        try {
            if ($log) {
                Atexo_Util::writeLogFile(Atexo_Config::getParameter('DIR_FILE_CHORUS_LOGS'), Atexo_Config::getParameter('DIR_FILE_CHORUS_LOGS').$nomFichier.'.log', $log);
                $message = Atexo_Message::getGenericEmailFormat($log);
                Atexo_Message::simpleMail(Atexo_Config::getParameter('PF_MAIL_FROM'), 'applications.mpe@atexo.com', "PLACE : Erreur intégration flux $nomFichier", $message, '', '', false, true);
            }
        } catch (Exception $e) {
            (new Atexo_Chorus_Util())->loggerErreurReception('Erreur ecriture du fichier dans les logs : '.$e->getMessage());
        }
    }

    /**
     * Permet de verifier si le mail de mise a jour du FSO doit etre envoye ou pas.
     * La regle est: le mail FSO est envoyé si et seulement si : le dernier statut EJ, valeur de statut_ej dans la table t_contrat_titulaire, est différent du statut de l'EJ qui est dans le FSO.
     *
     * @param string $statutEjBd   : contenu du champ `statutEJ` de la table `t_contrat_titulaire`
     * @param string $numeroMarche : numero de marche, correspond au champ `numero_contrat` de la table `t_contrat_titulaire`
     * @param string $statutEjFlux : nouveau statut EJ envoye par Chorus
     *
     * @return bool : true si les conditions d'envoi de mail sont verifiees, false sinon
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-plac
     *
     * @copyright Atexo 2016
     */
    public function verifierConditionsEnvoiMailFso($statutEjBd, $numeroMarche, $statutEjFlux)
    {
        $dernierStatutEjBd = (new Atexo_Chorus_Util())->retournLastStatutEJDecision($statutEjBd, $numeroMarche);
        if (empty($statutEjBd) || (string) $dernierStatutEjBd != (string) $statutEjFlux) {
            return true;
        }

        return false;
    }
}
