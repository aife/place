<?php

namespace Application\Service\Atexo\Chorus;

use App\Service\WebServices\WebServicesExec;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonChorusActeJuridique;
use Application\Propel\Mpe\CommonChorusEchange;
use Application\Propel\Mpe\CommonChorusFormePrix;
use Application\Propel\Mpe\CommonChorusGroupementAchat;
use Application\Propel\Mpe\CommonChorusOrganisationAchat;
use Application\Propel\Mpe\CommonChorusTypeMarche;
use Application\Propel\Mpe\CommonChorusTypeProcedure;
use Application\Propel\Mpe\CommonTContratTitulaire;
use Application\Propel\Mpe\CommonTContratTitulaireQuery;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_Xml;
use DOMDocument;
use Exception;
use Prado\Prado;
use Prado\Util\TLogger;
use Psr\Log\LoggerInterface;

class Atexo_Chorus_Transmission
{
    public function createFluxFen(CommonChorusEchange $echange, $acronymeOrganisme, $idEjAppliExtValue, $sendFicheNavette = true, $isFicheModificative = false, &$erreursFluxChorus = null, $logger = null)
    {
        $dernierStatutEJDecision = null;
        $numeroMarche = null;
        if (empty($logger)) {
            $logger = Atexo_LoggerManager::getLogger('chorus');
        }
        $logger->info('Debut creation flux FEN111: Echange [Id = '.$echange->getId().' , Organisme = '.$acronymeOrganisme.'] , Methode = Atexo_Chorus_Transmission::createFluxFen');
        try {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $domDocument = new DOMDocument('1.0', 'ISO-8859-1');

            $engagementsJuridiques = $domDocument->createElement('ej:EngagementsJuridiques', '');
            $domDocument->appendChild($engagementsJuridiques);

            $attribute = $domDocument->createAttribute('xmlns:ej');
            $engagementsJuridiques->appendChild($attribute);
            $attributeValue = $domDocument->createTextNode('http://www.finances.gouv.fr/aife/chorus/EJ/');
            $attribute->appendChild($attributeValue);

            $attribute = $domDocument->createAttribute('xmlns:xsi');
            $engagementsJuridiques->appendChild($attribute);
            $attributeValue = $domDocument->createTextNode('http://www.w3.org/2001/XMLSchema-instance');
            $attribute->appendChild($attributeValue);

            $attribute = $domDocument->createAttribute('xsi:schemaLocation');
            $engagementsJuridiques->appendChild($attribute);
            $attributeValue = $domDocument->createTextNode('http://www.finances.gouv.fr/aife/chorus/EJ/EngagementsJuridiques.xsd');
            $attribute->appendChild($attributeValue);

            $version = $domDocument->createElement('Version', '1.0');
            $engagementsJuridiques->appendChild($version);

            $codeAppli = $domDocument->createElement('CodeAppli', 'PMI001');
            $engagementsJuridiques->appendChild($codeAppli);

            $engagementJuridique = $domDocument->createElement('EngagementJuridique', '');
            $engagementsJuridiques->appendChild($engagementJuridique);

            // Entete
            $entete = $domDocument->createElement('Entete', '');
            $engagementJuridique->appendChild($entete);

            $typeMarche = (new Atexo_Chorus_Echange())->retreiveTypeMarche($echange->getIdTypeMarche());
            if (($typeMarche instanceof CommonChorusTypeMarche) && $typeMarche->getCode()) {
                $typeEj = $domDocument->createElement('TypeEJ', $typeMarche->getCode());
                $entete->appendChild($typeEj);
            } else {
                Prado::log('Chorus Construction du flux: ERREUR Valeur vide pour TypeEJ, identifiant echange '.$echange->getId(), TLogger::ERROR, 'Atexo.Chorus.Transmission.php');
                $erreurMessage = 'Valeur vide pour TypeEJ : ERREUR Valeur vide pour IdACPMI : Echange [Id = '.$echange->getId().' , Organisme = '.$acronymeOrganisme.'] , Methode = Atexo_Chorus_Transmission::createFluxFen';
                $logger->error($erreurMessage);
                $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;

                return false;
            }

            $organisationAchat = (new Atexo_Chorus_Echange())->retrieveOrganisationAchatById($echange->getIdOa(), $acronymeOrganisme);
            if (($organisationAchat instanceof CommonChorusOrganisationAchat) && $organisationAchat->getCode()) {
                $oa = $domDocument->createElement('OrganisationAchat', $organisationAchat->getCode());
                $entete->appendChild($oa);
            } else {
                Prado::log('Chorus Construction du flux: ERREUR Valeur vide pour OrganisationAchat, identifiant echange '.$echange->getId(), TLogger::ERROR, 'Atexo.Chorus.Transmission.php');
                $erreurMessage = 'Chorus Construction du flux: ERREUR Valeur vide pour OrganisationAchat : Echange [Id = '.$echange->getId().' , Organisme = '.$acronymeOrganisme.'] , Methode = Atexo_Chorus_Transmission::createFluxFen';
                $logger->error($erreurMessage);
                $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;

                return false;
            }

            $groupementAchat = (new Atexo_Chorus_Echange())->retrieveGroupementAchatById($echange->getIdGa(), $acronymeOrganisme);
            if (($groupementAchat instanceof CommonChorusGroupementAchat) && $groupementAchat->getCode()) {
                $ga = $domDocument->createElement('GroupeAcheteur', $groupementAchat->getCode());
                $entete->appendChild($ga);
            } else {
                Prado::log('Chorus Construction du flux: ERREUR Valeur vide pour GroupeAcheteur, identifiant echange '.$echange->getId(), TLogger::ERROR, 'Atexo.Chorus.Transmission.php');
                $erreurMessage = 'Chorus Construction du flux: Erreur Valeur vide pour GroupeAcheteur : Echange [Id = '.$echange->getId().' , Organisme = '.$acronymeOrganisme.'] , Methode = Atexo_Chorus_Transmission::createFluxFen';
                $logger->error($erreurMessage);
                $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;

                return false;
            }

            // @TODO LEZ changer decision to contrat fait attention au numero de marche
            $contrat = null;
            try {
                $execWS = Atexo_Util::getSfService(WebServicesExec::class);
                if ($echange->getUuidExterneExec()) {
                    $contrat = $execWS->getContent('getContratByUuid', $echange->getUuidExterneExec());
                }

            } catch (\Exception $exception) {
                Prado::log("Contrat n'existe pas : "  . $echange->getUuidExterneExec());
            }
            if (is_array($contrat)) {
                $numeroMarche = $contrat['numero'];
                $dernierStatutEJDecision = (new Atexo_Chorus_Util())->retournLastStatutEJDecision($contrat['statutEj'], $numeroMarche);
            }

            //le numéro de marché doit être renseigné si la dernière décision n'est pas supprimé
            if (!strstr($dernierStatutEJDecision, Atexo_Config::getParameter('CHORUS_STATUT_EJ_SUPPRIME')) && '' != $numeroMarche) {
                $idEjChorus = $domDocument->createElement('IdEJChorus', $numeroMarche);
                $entete->appendChild($idEjChorus);
            }

            if ($idEjAppliExtValue) {
                $idEjAppliExt = $domDocument->createElement('IdEJAppliExt', $idEjAppliExtValue);
                $entete->appendChild($idEjAppliExt);
            } else {
                Prado::log('Chorus Construction du flux: ERREUR Valeur vide pour IdEJAppliExt, identifiant echange '.$echange->getId(), TLogger::ERROR, 'Atexo.Chorus.Transmission.php');
                $erreurMessage = 'Chorus Construction du flux: Erreur Valeur vide pour IdEJAppliExt : Echange [Id = '.$echange->getId().' , Organisme = '.$acronymeOrganisme.'] , Methode = Atexo_Chorus_Transmission::createFluxFen';
                $logger->error($erreurMessage);
                $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;
                return false;
            }
            //condition de paiement
            $pme = '';
            if ((int) $echange->getConditionsPaiement() > 0) {
                $pme = '0001';
            }
            $condPaiement = $domDocument->createElement('CondPaiement', $pme);
            $entete->appendChild($condPaiement);

            //Regroupement comptable
            if ($echange->getIdRegroupementComptable() && '0' != $echange->getIdRegroupementComptable()) {
                $regroupementComptableEtp = $domDocument->createElement('CodeSociete', $echange->getIdRegroupementComptable());
                $entete->appendChild($regroupementComptableEtp);
            }

            //Type de groupement
            if ($echange->getIdTypeGroupement() && '0' != $echange->getIdTypeGroupement()) {
                $typeGroupementEtp = $domDocument->createElement('SchemaPartenaire', $echange->getIdTypeGroupement());
                $entete->appendChild($typeGroupementEtp);
            } else {
                Prado::log('Chorus Construction du flux: ERREUR Valeur vide pour SchemaPartenaire, identifiant echange '.$echange->getId(), TLogger::ERROR, 'Atexo.Chorus.Transmission.php');
                $erreurMessage = 'Chorus Construction du flux: Erreur Valeur vide pour SchemaPartenaire : Echange [Id = '.$echange->getId().' , Organisme = '.$acronymeOrganisme.'] , Methode = Atexo_Chorus_Transmission::createFluxFen';
                $logger->error($erreurMessage);
                $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;

                return false;
            }

            //Type d'avance
            $accompte = ' ';
            if ($echange->getTypeAvance() && '0' !== $echange->getTypeAvance()) {
                $accompte = $echange->getTypeAvance();
            }
            $codeAcompte = $domDocument->createElement('CodeAcompte', $accompte);
            $entete->appendChild($codeAcompte);

            //Pourcentage de l'avance
            if ($echange->getPourcentageAvance() && '' != $echange->getPourcentageAvance()) {
                if ((int) $echange->getPourcentageAvance() > 0) {
                    $pourcentageAvance = $domDocument->createElement('PourcentageAcompte', $echange->getPourcentageAvance());
                    $entete->appendChild($pourcentageAvance);
                } else {
                    Prado::log('Chorus Construction du flux: ERREUR Valeur vide pour PourcentageAcompte, identifiant echange '.$echange->getId(), TLogger::ERROR, 'Atexo.Chorus.Transmission.php');
                    $erreurMessage = 'Chorus Construction du flux: Erreur vide pour PourcentageAcompte : Echange [Id = '.$echange->getId().' , Organisme = '.$acronymeOrganisme.'] , Methode = Atexo_Chorus_Transmission::createFluxFen';
                    $logger->error($erreurMessage);
                    $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;

                    return false;
                }
            }

            // EJPartenaireEntete
            $blocJPartenaireEntete = false;
            $eJPartenaireEntete = $domDocument->createElement('EJPartenaireEntete', '');
            $engagementJuridique->appendChild($eJPartenaireEntete);

            //N° SIRET titulaire ou mandataire
            if ('FR' == $echange->getCodePaysTitulaire() && $echange->getNumeroSiretTitulaire() && $echange->getNumeroSirenTitulaire()) {
                $titCotraitMandat = $domDocument->createElement('TitCotraitMandat', '');
                if (Atexo_Util::isSiretValide($echange->getNumeroSirenTitulaire().$echange->getNumeroSiretTitulaire())) {
                    $eJPartenaireEntete->appendChild($titCotraitMandat);
                    $siretpart = $domDocument->createElement('SIRETPart', $echange->getNumeroSirenTitulaire().$echange->getNumeroSiretTitulaire());
                    $titCotraitMandat->appendChild($siretpart);
                    $blocJPartenaireEntete = true;
                } else {
                    Prado::log('Chorus Construction du flux: ERREUR Valeur vide pour TitCotraitMandat.SIRETPart, identifiant echange '.$echange->getId(), TLogger::ERROR, 'Atexo.Chorus.Transmission.php');
                    $erreurMessage = 'Chorus Construction du flux: Erreur Valeur vide pour TitCotraitMandat.SIRETPart : Echange [Id = '.$echange->getId().' , Organisme = '.$acronymeOrganisme.'] , Methode = Atexo_Chorus_Transmission::createFluxFen';
                    $logger->error($erreurMessage);
                    $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;

                    return false;
                }
            }

            //N° de SIRET des co-titulaires
            if ((int) $echange->getNbrEntreprisesCotraitantes() > 0) {
                $arrayCodesPaysCoTitulaire = explode('#', $echange->getCodesPaysCoTitulaire());
                $arraySirenCoTitulaire = explode('#', $echange->getNumeroSirenCoTitulaire());
                $arraySiretCoTitulaire = explode('#', $echange->getNumeroSiretCoTitulaire());
                $increment = 0;
                if (count($arrayCodesPaysCoTitulaire)) {
                    foreach ($arrayCodesPaysCoTitulaire as $code) {
                        if ($code) {
                            if ('FR' == $code && $arraySirenCoTitulaire[$increment] && $arraySiretCoTitulaire[$increment]) {
                                if (Atexo_Util::isSiretValide($arraySirenCoTitulaire[$increment].$arraySiretCoTitulaire[$increment])) {
                                    $titulaireCotraitant = $domDocument->createElement('CoTraitant', '');
                                    $eJPartenaireEntete->appendChild($titulaireCotraitant);
                                    $siretpart = $domDocument->createElement('SIRETPart', $arraySirenCoTitulaire[$increment].$arraySiretCoTitulaire[$increment]);
                                    $titulaireCotraitant->appendChild($siretpart);
                                    $blocJPartenaireEntete = true;
                                } else {
                                    Prado::log('Chorus Construction du flux: ERREUR Valeur vide pour CoTraitant.SIRETPart, identifiant echange '.$echange->getId(), TLogger::ERROR, 'Atexo.Chorus.Transmission.php');
                                    $erreurMessage = 'Chorus Construction du flux: Erreur Valeur vide pour CoTraitant.SIRETPart : Echange [Id = '.$echange->getId().' , Organisme = '.$acronymeOrganisme.'] , Methode = Atexo_Chorus_Transmission::createFluxFen';
                                    $logger->error($erreurMessage);
                                    $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;

                                    return false;
                                }
                            }
                        }
                        ++$increment;
                    }
                }
            }

            //Fiche de recensement
            $ficheRecensement = $domDocument->createElement('FicheRecensement', '');
            $engagementJuridique->appendChild($ficheRecensement);

            $tagFicheRecensementVide = true;

            if ($echange->getDateNotification() || $echange->getDateNotificationReelle()) {
                if ($echange->getDateNotificationReelle()) {
                    $dateNotification = $echange->getDateNotificationReelle();
                } else {
                    $dateNotification = $echange->getDateNotification();
                }
                if (!Atexo_Util::isDate(Atexo_Util::iso2frnDate($dateNotification))) {
                    Prado::log('Chorus Construction du flux: ERREUR Valeur vide pour DateNotification, identifiant echange '.$echange->getId(), TLogger::ERROR, 'Atexo.Chorus.Transmission.php');
                    $erreurMessage = 'Chorus Construction du flux: Erreur Valeur vide pour DateNotification : Echange [Id = '.$echange->getId().' , Organisme = '.$acronymeOrganisme.'] , Methode = Atexo_Chorus_Transmission::createFluxFen';
                    $logger->error($erreurMessage);
                    $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;

                    return false;
                }
                $dateNotificationElement = $domDocument->createElement('DateNotification', $dateNotification);
                $ficheRecensement->appendChild($dateNotificationElement);
                $tagFicheRecensementVide = false;
            } else {
                Prado::log('Chorus Construction du flux: ERREUR Valeur vide pour DateNotification, identifiant echange '.$echange->getId(), TLogger::ERROR, 'Atexo.Chorus.Transmission.php');
                $erreurMessage = 'Chorus Construction du flux: Erreur Valeur vide pour DateNotification : Echange [Id = '.$echange->getId().' , Organisme = '.$acronymeOrganisme.'] , Methode = Atexo_Chorus_Transmission::createFluxFen';
                $logger->error($erreurMessage);
                $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;

                return false;
            }

            if ($echange->getDateFinMarche() || $echange->getDateFinMarcheReelle()) {
                if ($echange->getDateFinMarcheReelle()) {
                    $dateFinMarche = $echange->getDateFinMarcheReelle();
                } else {
                    $dateFinMarche = $echange->getDateFinMarche();
                }
                if (!Atexo_Util::isDate(Atexo_Util::iso2frnDate($dateFinMarche))) {
                    Prado::log('Chorus Construction du flux: ERREUR Valeur vide pour DateFinMarche, identifiant echange '.$echange->getId(), TLogger::ERROR, 'Atexo.Chorus.Transmission.php');
                    $erreurMessage = 'Chorus Construction du flux: Erreur format de DateFinMarche incorrect  : Echange [Id = '.$echange->getId().' , Organisme = '.$acronymeOrganisme.'] , Methode = Atexo_Chorus_Transmission::createFluxFen';
                    $logger->error($erreurMessage);
                    $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;

                    return false;
                }
                $dateFinMarcheElement = $domDocument->createElement('DateFinMarche', $dateFinMarche);
                $ficheRecensement->appendChild($dateFinMarcheElement);
                $tagFicheRecensementVide = false;
            } else {
                Prado::log('Chorus Construction du flux: ERREUR Valeur vide pour DateFinMarche, identifiant echange '.$echange->getId(), TLogger::ERROR, 'Atexo.Chorus.Transmission.php');
                $erreurMessage = 'Chorus Construction du flux: Erreur  Valeur vide pour DateFinMarche : Echange [Id = '.$echange->getId().' , Organisme = '.$acronymeOrganisme.'] , Methode = Atexo_Chorus_Transmission::createFluxFen';
                $logger->error($erreurMessage);
                $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;

                return false;
            }

            if ($echange->getSiren() && $echange->getSiret()) {
                if (Atexo_Util::isSiretValide($echange->getSiren().$echange->getSiret())) {
                    $siretAcheteur = $domDocument->createElement('SIRETAcheteur', $echange->getSiren().$echange->getSiret());
                    $ficheRecensement->appendChild($siretAcheteur);
                    $tagFicheRecensementVide = false;
                } else {
                    Prado::log('Chorus Construction du flux: ERREUR Valeur vide pour SIRETAcheteur, identifiant echange '.$echange->getId(), TLogger::ERROR, 'Atexo.Chorus.Transmission.php');
                    $erreurMessage = 'Chorus Construction du flux: Erreur Valeur vide pour SIRETAcheteur : Echange [Id = '.$echange->getId().' , Organisme = '.$acronymeOrganisme.'] , Methode = Atexo_Chorus_Transmission::createFluxFen';
                    $logger->error($erreurMessage);
                    $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;

                    return false;
                }
            }

            $acteJuridique = (new Atexo_Chorus_Echange())->retreiveActeJuridique($echange->getIdActeJuridique());
            if (($acteJuridique instanceof CommonChorusActeJuridique) && $acteJuridique->getCode()) {
                $typeMarche = $domDocument->createElement('TypeMarche', $acteJuridique->getCode());
                $ficheRecensement->appendChild($typeMarche);
                $tagFicheRecensementVide = false;
            } else {
                Prado::log('Chorus Construction du flux: ERREUR Valeur vide pour TypeMarche, identifiant echange '.$echange->getId(), TLogger::ERROR, 'Atexo.Chorus.Transmission.php');
                $erreurMessage = 'Chorus Construction du flux: Erreur Valeur vide pour TypeMarche : Echange [Id = '.$echange->getId().' , Organisme = '.$acronymeOrganisme.'] , Methode = Atexo_Chorus_Transmission::createFluxFen';
                $logger->error($erreurMessage);
                $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;

                return false;
            }

            $typeProcedure = (new Atexo_Chorus_Echange())->retreiveTypeProcedureById($echange->getIdTypeProcedure());
            if (($typeProcedure instanceof CommonChorusTypeProcedure) && $typeProcedure->getCode()) {
                $procedure = $domDocument->createElement('Procedure', $typeProcedure->getCode());
                $ficheRecensement->appendChild($procedure);
                $tagFicheRecensementVide = false;
            } else {
                Prado::log('Chorus Construction du flux: ERREUR Valeur vide pour Procedure, identifiant echange '.$echange->getId(), TLogger::ERROR, 'Atexo.Chorus.Transmission.php');
                $erreurMessage = 'Chorus Construction du flux: Erreur Valeur vide pour Procedure : Echange [Id = '.$echange->getId().' , Organisme = '.$acronymeOrganisme.'] , Methode = Atexo_Chorus_Transmission::createFluxFen';
                $logger->error($erreurMessage);
                $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;

                return false;
            }
            //Sous-traitance déclarée
            $sousTraitanceDeclareeValue = ' ';
            if ((int) $echange->getSousTraitanceDeclaree() > 0) {
                $sousTraitanceDeclareeValue = 'X';
            }
            $sousTraitanceDeclaree = $domDocument->createElement('SousTraitance', $sousTraitanceDeclareeValue);
            $ficheRecensement->appendChild($sousTraitanceDeclaree);
            $tagFicheRecensementVide = false;
            //CCAG de rattachement
            if ($echange->getCcagReference() && '0' !== $echange->getCcagReference()) {
                $ccagRef = $domDocument->createElement('CodeCCAG', $echange->getCcagReference());
                $ficheRecensement->appendChild($ccagRef);
                $tagFicheRecensementVide = false;
            } else {
                Prado::log('Chorus Construction du flux: ERREUR Valeur vide pour CodeCCAG, identifiant echange '.$echange->getId(), TLogger::ERROR, 'Atexo.Chorus.Transmission.php');
                $erreurMessage = 'Chorus Construction du flux: Erreur Valeur vide pour CodeCCAG : Echange [Id = '.$echange->getId().' , Organisme = '.$acronymeOrganisme.'] , Methode = Atexo_Chorus_Transmission::createFluxFen';
                $logger->error($erreurMessage);
                $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;

                return false;
            }
            //Identifiant accord cadre
            if ('' != $echange->getIdentifiantAccordCadre()) {
                $refAccordCadre = $domDocument->createElement('RefAccordCadre', $echange->getIdentifiantAccordCadre());
                $ficheRecensement->appendChild($refAccordCadre);
                $tagFicheRecensementVide = false;
            }
            if ($echange->getCpv1()) {
                $cpv = $echange->getCpv1();
                $cpvPrincipal = $domDocument->createElement('CPVPrincipal', $cpv.'-0');
                $ficheRecensement->appendChild($cpvPrincipal);
                $tagFicheRecensementVide = false;
            } else {
                Prado::log('Chorus Construction du flux: ERREUR Valeur vide pour CPVPrincipal, identifiant echange '.$echange->getId(), TLogger::ERROR, 'Atexo.Chorus.Transmission.php');
                $erreurMessage = 'Chorus Construction du flux: Erreur Valeur vide pour CPVPrincipal : Echange [Id = '.$echange->getId().' , Organisme = '.$acronymeOrganisme.'] , Methode = Atexo_Chorus_Transmission::createFluxFen';
                $logger->error($erreurMessage);
                $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;

                return false;
            }

            if ($echange->getCpv2()) {
                $arrayCpvSec = explode('#', $echange->getCpv2());
                if ($arrayCpvSec[1]) {
                    $cpv = $arrayCpvSec[1];
                    $cpv1 = $domDocument->createElement('CPVSecond1', $cpv.'-0');
                    $ficheRecensement->appendChild($cpv1);
                    $tagFicheRecensementVide = false;
                }
                if ($arrayCpvSec[2]) {
                    $cpv = $arrayCpvSec[2];
                    $cpv2 = $domDocument->createElement('CPVSecond2', $cpv.'-0');
                    $ficheRecensement->appendChild($cpv2);
                    $tagFicheRecensementVide = false;
                }
                if ($arrayCpvSec[3]) {
                    $cpv = $arrayCpvSec[3];
                    $cpv3 = $domDocument->createElement('CPVSecond3', $cpv.'-0');
                    $ficheRecensement->appendChild($cpv3);
                    $tagFicheRecensementVide = false;
                }
            }
            if ('' !== $echange->getNbrEntreprisesCotraitantes()) {
                if (Atexo_Util::isEntier($echange->getNbrEntreprisesCotraitantes())) {
                    $nombreEntrepriseCotraitante = $domDocument->createElement('NbEntrCotrait', trim($echange->getNbrEntreprisesCotraitantes()));
                    $ficheRecensement->appendChild($nombreEntrepriseCotraitante);
                    $tagFicheRecensementVide = false;
                } else {
                    Prado::log('Chorus Construction du flux: ERREUR Valeur vide pour NbEntrCotrait, identifiant echange '.$echange->getId(), TLogger::ERROR, 'Atexo.Chorus.Transmission.php');
                    $erreurMessage = 'Chorus Construction du flux: Erreur Valeur vide pour NbEntrCotrait : Echange [Id = '.$echange->getId().' , Organisme = '.$acronymeOrganisme.'] , Methode = Atexo_Chorus_Transmission::createFluxFen';
                    $logger->error($erreurMessage);
                    $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;

                    return false;
                }
            }

            if ('0' === $echange->getCarteAchat() || '1' === $echange->getCarteAchat()) {
                $carteAchat = $domDocument->createElement('CarteAchat', $echange->getCarteAchat());
                $ficheRecensement->appendChild($carteAchat);
                $tagFicheRecensementVide = false;
            } else {
                Prado::log('Chorus Construction du flux: ERREUR Valeur vide pour CarteAchat, identifiant echange '.$echange->getId(), TLogger::ERROR, 'Atexo.Chorus.Transmission.php');
                $erreurMessage = 'Chorus Construction du flux: Erreur Valeur vide pour CarteAchat : Echange [Id = '.$echange->getId().' , Organisme = '.$acronymeOrganisme.'] , Methode = Atexo_Chorus_Transmission::createFluxFen';
                $logger->error($erreurMessage);
                $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;

                return false;
            }

            if ('0' === $echange->getClauseSociale() || '1' === $echange->getClauseSociale()) {
                $clauseSocial = $domDocument->createElement('ClauseSociale', $echange->getClauseSociale());
                $ficheRecensement->appendChild($clauseSocial);
                $tagFicheRecensementVide = false;
            } else {
                Prado::log('Chorus Construction du flux: ERREUR Valeur vide pour ClauseSociale, identifiant echange '.$echange->getId(), TLogger::ERROR, 'Atexo.Chorus.Transmission.php');
                $erreurMessage = 'Chorus Construction du flux: Erreur Valeur vide pour ClauseSociale : Echange [Id = '.$echange->getId().' , Organisme = '.$acronymeOrganisme.'] , Methode = Atexo_Chorus_Transmission::createFluxFen';
                $logger->error($erreurMessage);
                $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;

                return false;
            }

            if ($echange->getNbrPropositionRecues()) {
                if (Atexo_Util::isEntier($echange->getNbrPropositionRecues())) {
                    $nombreProposition = $domDocument->createElement('NbTotalProp', trim($echange->getNbrPropositionRecues()));
                    $ficheRecensement->appendChild($nombreProposition);
                    $tagFicheRecensementVide = false;
                } else {
                    Prado::log('Chorus Construction du flux: ERREUR Valeur vide pour NbTotalProp, identifiant echange '.$echange->getId(), TLogger::ERROR, 'Atexo.Chorus.Transmission.php');
                    $erreurMessage = 'Chorus Construction du flux: Erreur Valeur vide pour NbTotalProp : Echange [Id = '.$echange->getId().' , Organisme = '.$acronymeOrganisme.'] , Methode = Atexo_Chorus_Transmission::createFluxFen';
                    $logger->error($erreurMessage);
                    $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;

                    return false;
                }
            }
            if ($echange->getNbrPropositionDematerialisees()) {
                if (Atexo_Util::isEntier($echange->getNbrPropositionDematerialisees())) {
                    $nombrePropositionDemat = $domDocument->createElement('NbTotalPropDemat', trim($echange->getNbrPropositionDematerialisees()));
                    $ficheRecensement->appendChild($nombrePropositionDemat);
                    $tagFicheRecensementVide = false;
                } else {
                    Prado::log('Chorus Construction du flux: ERREUR Valeur vide pour NbTotalPropDemat, identifiant echange '.$echange->getId(), TLogger::ERROR, 'Atexo.Chorus.Transmission.php');
                    $erreurMessage = 'Chorus Construction du flux: Erreur Valeur vide pour NbTotalPropDemat : Echange [Id = '.$echange->getId().' , Organisme = '.$acronymeOrganisme.'] , Methode = Atexo_Chorus_Transmission::createFluxFen';
                    $logger->error($erreurMessage);
                    $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;

                    return false;
                }
            }

            if ('0' === $echange->getClauseEnvironnementale() || '1' === $echange->getClauseEnvironnementale()) {
                $clauseEnv = $domDocument->createElement('ClauseEnvironnementale', $echange->getClauseEnvironnementale());
                $ficheRecensement->appendChild($clauseEnv);
                $tagFicheRecensementVide = false;
            } else {
                Prado::log('Chorus Construction du flux: ERREUR Valeur vide pour ClauseEnvironnementale, identifiant echange '.$echange->getId(), TLogger::ERROR, 'Atexo.Chorus.Transmission.php');
                $erreurMessage = 'Chorus Construction du flux: Erreur Valeur vide pour ClauseEnvironnementale : Echange [Id = '.$echange->getId().' , Organisme = '.$acronymeOrganisme.'] , Methode = Atexo_Chorus_Transmission::createFluxFen';
                $logger->error($erreurMessage);
                $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;

                return false;
            }

            $chorusFormePrix = (new Atexo_Chorus_Echange())->retrieveFormePrixById($echange->getIdFormePrix());
            if (($chorusFormePrix instanceof CommonChorusFormePrix) && $chorusFormePrix->getCode()) {
                $formePrix = $domDocument->createElement('FormePrix', $chorusFormePrix->getCode());
                $ficheRecensement->appendChild($formePrix);
                $tagFicheRecensementVide = false;
            } else {
                Prado::log('Chorus Construction du flux: ERREUR Valeur vide pour FormePrix, identifiant echange '.$echange->getId(), TLogger::ERROR, 'Atexo.Chorus.Transmission.php');
                $erreurMessage = 'Chorus Construction du flux: Erreur Valeur vide pour FormePrix : Echange [Id = '.$echange->getId().' , Organisme = '.$acronymeOrganisme.'] , Methode = Atexo_Chorus_Transmission::createFluxFen';
                $logger->error($erreurMessage);
                $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;

                return false;
            }

            if ($tagFicheRecensementVide) {
                $engagementJuridique->removeChild($ficheRecensement);
            }
            if (!$blocJPartenaireEntete) {
                $engagementJuridique->removeChild($eJPartenaireEntete);
            }
            $contrat = null;
            try {
                $execWS = Atexo_Util::getSfService(WebServicesExec::class);
                if ($echange->getUuidExterneExec()) {
                    $contrat = $execWS->getContent('getContratByUuid', $echange->getUuidExterneExec());
                }
            } catch (\Exception $exception) {
                $erreurMessage = 'Erreur Objet invalide -  Echange : '.print_r($echange, true)." , \n Contrat : ".print_r($contrat, true)." ] , \n Methode = Atexo_Chorus_GestionFichierChorus::createFluxFenModeIntegre";
                $logger->error($erreurMessage);
                $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus' . PHP_EOL . $erreurMessage;
                return false;
            }
            if ($echange instanceof CommonChorusEchange && is_array($contrat)) {
                (new Atexo_Chorus_GestionFichierChorus())->getFileDceForTransmitionChorus($echange, $contrat, $acronymeOrganisme, false, $domDocument, $engagementJuridique, $erreursFluxChorus, $logger);

                (new Atexo_Chorus_GestionFichierChorus())->getFileEnvAeForTransmitionChorus($echange, $acronymeOrganisme, false, $domDocument, $engagementJuridique, $erreursFluxChorus, $logger, $contrat);

                (new Atexo_Chorus_GestionFichierChorus())->getFileSignaceForTransmitionChorus($echange, $contrat, $acronymeOrganisme, false, $domDocument, $engagementJuridique, $erreursFluxChorus, $logger);

                (new Atexo_Chorus_GestionFichierChorus())->getFileEnvForTransmitionChorus($echange, $contrat, $acronymeOrganisme, false, $domDocument, $engagementJuridique, $erreursFluxChorus, $logger);

                (new Atexo_Chorus_GestionFichierChorus())->getFilePiecesExternesForTransmitionChorus($echange, $acronymeOrganisme, false, $domDocument, $engagementJuridique, $erreursFluxChorus, $logger, $contrat);

                (new Atexo_Chorus_GestionFichierChorus())->getFilePiecesJointesForTransmitionChorus($echange, $acronymeOrganisme, false, $domDocument, $engagementJuridique, $erreursFluxChorus, $logger, $contrat);

                (new Atexo_Chorus_GestionFichierChorus())->getFilePiecesOuvertureAnlyseChorus($echange, $acronymeOrganisme, false, $domDocument, $engagementJuridique, $erreursFluxChorus, $logger, $contrat);

                //dume Acheteur
                (new Atexo_Chorus_GestionFichierChorus())->getFilePiecesDumeForTransmitionChorus($echange, $acronymeOrganisme, Atexo_Config::getParameter('TYPE_DUME_ACHETEUR'), false, $domDocument, $engagementJuridique, $erreursFluxChorus, $logger, $contrat);

                //dume OE
                (new Atexo_Chorus_GestionFichierChorus())->getFilePiecesDumeForTransmitionChorus($echange, $acronymeOrganisme, Atexo_Config::getParameter('TYPE_DUME_OE'), false, $domDocument, $engagementJuridique, $erreursFluxChorus, $logger, $contrat);

                //TODO : envoi pieces de notification
                //Atexo_Chorus_GestionFichierChorus::getFileNotificationForTransmitionChorus($echange, $contrat, $acronymeOrganisme, false, $domDocument, $engagementJuridique);
                //Transmission des signatures des fichiers autres que l'acte d'engagement
                (new Atexo_Chorus_GestionFichierChorus())->getFileSignEnvForTransmitionChorus($echange, $contrat, $acronymeOrganisme, false, $domDocument, $engagementJuridique, $erreursFluxChorus, $logger);
                //Transmission des fichiers enveloppes de la fonctionnalité: réponse pas à pas
                (new Atexo_Chorus_GestionFichierChorus())->getFileEnvRepPasAPasForTransmitionChorus($echange, $contrat, $acronymeOrganisme, false, $domDocument, $engagementJuridique, $erreursFluxChorus, $logger);
                if ($sendFicheNavette) {
                    //Transmission de la fiche de navette
                    (new Atexo_Chorus_GestionFichierChorus())->getFileNavetteChorus($echange, false, $domDocument, $engagementJuridique, $erreursFluxChorus, $logger);
                }
                if ($isFicheModificative) {
                    ///Transmission de la fiche Modificative et ses PJs
                    $logger->info('Ajout donnees Fiche modificative  Echange : [Id = '.$echange->getId().' , Organisme = '.$echange->getOrganisme().'] : ');
                    (new Atexo_Chorus_GestionFichierChorus())->getFileFicheModificativeChorus($echange, $domDocument, $engagementJuridique, $erreursFluxChorus, $logger);
                }
            } else {
                $erreurMessage = 'Erreur Objet invalide -  Echange : '.print_r($echange, true)." , \n Contrat : ".print_r($contrat, true)." ] , \n Methode = Atexo_Chorus_Transmission::createFluxFen";
                $logger->error($erreurMessage);
                $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;

                return false;
            }

            return $domDocument->saveXML();
        } catch (Exception $ex) {
            $erreurMessage = 'Atexo_Chorus_Transmission::createFluxFen : Erreur lors de la generation du flux Chorus FEN111 [ '.$ex->getMessage().' ]';
            $logger->error($erreurMessage);
            $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;

            return false;
        }
        $logger->info('Atexo_Chorus_Transmission::createFluxFen - Fin creation flux FEN111: Echange [Id = '.$echange->getId().' , Organisme = '.$acronymeOrganisme.']');
    }

    /*
     * Permet de creer le flux FIR
     */
    public function createFluxFir($echange, &$erreursFluxChorus = null, $logger = null)
    {
        if (empty($logger)) {
            $logger = Atexo_LoggerManager::getLogger('chorus');
        }
        $logger->info('Debut creation flux FIR: Echange [Id = '.$echange->getId().'] , Atexo_Chorus_Transmission::createFluxFir');
        try {
            $domDocument = new DOMDocument('1.0', 'ISO-8859-1');
            $controleTiers = (new Atexo_Xml())->addElement($domDocument, $domDocument, 'ControleTiers');
            $type = Atexo_Config::getParameter('DENOMINATION2_GEON2_FRANCE');
            if ($echange->getCodePaysTitulaire() == $type) {
                if ($echange->getNumeroSiretTitulaire() && $echange->getNumeroSirenTitulaire()) {
                    $siret = $echange->getNumeroSirenTitulaire().$echange->getNumeroSiretTitulaire();
                    $tiers = (new Atexo_Xml())->addElement($domDocument, $controleTiers, 'Tiers');
                    self::addDonneesTier($echange, $domDocument, $tiers, $siret);
                }
            }
            if (($echange->getIdTypeGroupement() != Atexo_Config::getParameter('ID_TYPE_GROUPEMEN_SANS_CO_TITULAIRE_CHORUS')) && '0' != $echange->getNbrEntreprisesCotraitantes()) {
                $listeCodesPays = explode('#', $echange->getCodesPaysCoTitulaire());
                if (in_array($type, $listeCodesPays)) {
                    if ($echange->getNumeroSirenCoTitulaire() && $echange->getNumeroSiretCoTitulaire()) {
                        $arraySirenTitulaires = explode('#', $echange->getNumeroSirenCoTitulaire());
                        $arraySiretTitulaires = explode('#', $echange->getNumeroSiretCoTitulaire());
                        if (is_array($arraySirenTitulaires)) {
                            for ($i = 0; $i < count($arraySirenTitulaires); ++$i) {
                                if ($arraySirenTitulaires[$i] && $arraySiretTitulaires[$i]) {
                                    $siret = $arraySirenTitulaires[$i].$arraySiretTitulaires[$i];
                                    $tiers = (new Atexo_Xml())->addElement($domDocument, $controleTiers, 'Tiers');
                                    self::addDonneesTier($echange, $domDocument, $tiers, $siret);
                                }
                            }
                        }
                    }
                }
            }

            return $domDocument->saveXML();
        } catch (Exception $ex) {
            $erreur = 'Erreur lors de la generation du flux Chorus FIR : '.PHP_EOL.'Erreur : '.$ex->getMessage().PHP_EOL.'Trace : '.$ex->getTraceAsString().PHP_EOL.'Methode = Atexo_Chorus_Transmission::createFluxFir';
            $logger->error($erreur);
            if (null !== $erreursFluxChorus) {
                $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreur;
            }

            return false;
        }
        $logger->info('Fin creation flux FIR: Echange [Id = '.$echange->getId().'] , Atexo_Chorus_Transmission::createFluxFir');
    }

    /*
     * Permet d'ajouter les données d'un tier
     */
    public function addDonneesTier($echange, &$domDocument, &$tag, $siret)
    {
        (new Atexo_Xml())->addElement($domDocument, $tag, 'ModeTraitement', Atexo_Config::getParameter('MODE_TRAITEMET_CHORUS'));
        (new Atexo_Xml())->addElement($domDocument, $tag, 'IdFonctionnel', $siret);
        (new Atexo_Xml())->addElement($domDocument, $tag, 'IdTechnique', '');
        (new Atexo_Xml())->addElement($domDocument, $tag, 'GroupeCompte', Atexo_Config::getParameter('FIR_GROUPE_COMPTE_CHORUS'));
        (new Atexo_Xml())->addElement($domDocument, $tag, 'CodeSociete', $echange->getIdRegroupementComptable());
        (new Atexo_Xml())->addElement($domDocument, $tag, 'OA', $echange->getCodeOrganisationAchat());
        (new Atexo_Xml())->addElement($domDocument, $tag, 'DC', '');
        (new Atexo_Xml())->addElement($domDocument, $tag, 'RIB', '');
        (new Atexo_Xml())->addElement($domDocument, $tag, 'IBAN', '');
        (new Atexo_Xml())->addElement($domDocument, $tag, 'BIC', '');
    }

    /**
     * Permet de creer le flux xml pour le FEN211.
     *
     * @param CommonChorusEchange $echange           : objet echange chorus
     * @param string              $acronymeOrganisme : organisme
     *
     * @return string
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 2.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function createFluxFenModeIntegre(CommonChorusEchange $echange, $acronymeOrganisme, &$erreursFluxChorus = null, $logger = null)
    {
        if (empty($logger)) {
            $logger = Atexo_LoggerManager::getLogger('chorus');
        }
        $logger->info('Debut creation flux FEN 211: Echange [Id = '.$echange->getId().' , Organisme = '.$acronymeOrganisme.'] , Atexo_Chorus_Transmission::createFluxFenModeIntegre');
        try {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $domDocument = new DOMDocument('1.0', 'ISO-8859-1');

            $engagementsJuridiques = $domDocument->createElement('ac:AccordsCadres', '');
            $domDocument->appendChild($engagementsJuridiques);

            $attribute = $domDocument->createAttribute("xmlns:ac");
            $engagementsJuridiques->appendChild($attribute);
            $attributeValue = $domDocument->createTextNode("FEN0211A");
            $attribute->appendChild($attributeValue);

			$attribute = $domDocument->createAttribute('xmlns:xsi');
            $engagementsJuridiques->appendChild($attribute);
            $attributeValue = $domDocument->createTextNode('http://www.w3.org/2001/XMLSchema-instance');
            $attribute->appendChild($attributeValue);

			$attribute = $domDocument->createAttribute("xsi:schemaLocation");
			$engagementsJuridiques->appendChild($attribute);
			$attributeValue = $domDocument->createTextNode("FEN0211A FEN0211A/AccordsCadresPLACE_v1.2.xsd");
			$attribute->appendChild($attributeValue);

            $version = $domDocument->createElement('Version', '1.2');
            $engagementsJuridiques->appendChild($version);

            $codeAppli = $domDocument->createElement('CodeAppli', 'PMI001');
            $engagementsJuridiques->appendChild($codeAppli);

            $engagementJuridique = $domDocument->createElement('AccordCadre', '');
            $engagementsJuridiques->appendChild($engagementJuridique);

            // Entete
            $entete = $domDocument->createElement('ACEntete', '');
            $engagementJuridique->appendChild($entete);

            //Identifiant accord cadre titulaire
            if ($echange->getIdentifiantAccordCadre()) {
                $idACPMI = $domDocument->createElement('IdACPMI', Atexo_Util::toUtf8($echange->getIdentifiantAccordCadre()));
                $entete->appendChild($idACPMI);
            } else {
                Prado::log('Chorus Construction du flux: ERREUR Valeur vide pour IdACPMI, identifiant echange '.$echange->getId(), TLogger::ERROR, 'Atexo.Chorus.Transmission.php');
                $erreurMessage = 'Chorus Construction du flux: ERREUR Valeur vide pour IdACPMI : Echange [Id = '.$echange->getId().' , Organisme = '.$acronymeOrganisme.'] , Atexo_Chorus_Transmission::createFluxFenModeIntegre';
                $logger->error($erreurMessage);
                $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;

                return false;
            }

            //Identifiant accord cadre chapeau
            if ($echange->getIdentifiantAccordCadreChapeau()) {
                $idACSuperieur = $domDocument->createElement('IdACSuperieur', Atexo_Util::toUtf8($echange->getIdentifiantAccordCadreChapeau()));
                $entete->appendChild($idACSuperieur);
            } else {
                Prado::log('Chorus Construction du flux: ERREUR Valeur vide pour IdACSuperieur, identifiant echange '.$echange->getId(), TLogger::ERROR, 'Atexo.Chorus.Transmission.php');
                $erreurMessage = 'Chorus Construction du flux: ERREUR Valeur vide pour IdACSuperieur : Echange [Id = '.$echange->getId().' , Organisme = '.$acronymeOrganisme.'] , Atexo_Chorus_Transmission::createFluxFenModeIntegre';
                $logger->error($erreurMessage);
                $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;

                return false;
            }

            //Code action
            $valeurCodeAction = '';
            if ('1' == (string) $echange->getNumOrdre()) {
                $valeurCodeAction = 'C';
            } elseif ((int) $echange->getNumOrdre() > 1) {
                $valeurCodeAction = 'M';
            }
            if (!empty($valeurCodeAction)) {
                $codeAction = $domDocument->createElement('CodeAction', Atexo_Util::toUtf8($valeurCodeAction));
                $entete->appendChild($codeAction);
            } else {
                Prado::log('Chorus Construction du flux: ERREUR Valeur vide pour CodeAction, identifiant echange '.$echange->getId(), TLogger::ERROR, 'Atexo.Chorus.Transmission.php');
                $erreurMessage = 'Chorus Construction du flux: ERREUR Valeur vide pour CodeAction : Echange [Id = '.$echange->getId().' , Organisme = '.$acronymeOrganisme.'] , Atexo_Chorus_Transmission::createFluxFenModeIntegre';
                $logger->error($erreurMessage);
                $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;

                return false;
            }

            //Date d'action
            $valeurDateAction = date('Y-m-d');
            if (!empty($valeurDateAction)) {
                $dateAction = $domDocument->createElement('DateAction', Atexo_Util::toUtf8($valeurDateAction));
                $entete->appendChild($dateAction);
            } else {
                Prado::log('Chorus Construction du flux: ERREUR Valeur vide pour DateAction, identifiant echange '.$echange->getId(), TLogger::ERROR, 'Atexo.Chorus.Transmission.php');
                $erreurMessage = 'Chorus Construction du flux: ERREUR Valeur vide pour DateAction : Echange [Id = '.$echange->getId().' , Organisme = '.$acronymeOrganisme.'] , Atexo_Chorus_Transmission::createFluxFenModeIntegre';
                $logger->error($erreurMessage);
                $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;

                return false;
            }

            //Type de contrat
            if ($echange->getTypeContrat()) {
                $valeurTypeContrat = null;
                if ($echange->getTypeContrat() == Atexo_Config::getParameter('TYPE_CONTRAT_ACCORD_CADRE')) {
                    $valeurTypeContrat = 'ACA';
                } elseif ($echange->getTypeContrat() == Atexo_Config::getParameter('TYPE_CONTRAT_SAD')) {
                    $valeurTypeContrat = 'SAD';
                }
                $typeContrat = $domDocument->createElement('TypeContrat', $valeurTypeContrat);
                $entete->appendChild($typeContrat);
            } else {
                Prado::log('Chorus Construction du flux: ERREUR Valeur vide pour TypeContrat, identifiant echange '.$echange->getId(), TLogger::ERROR, 'Atexo.Chorus.Transmission.php');
                $erreurMessage = 'Chorus Construction du flux: ERREUR Valeur vide pour TypeContrat : Echange [Id = '.$echange->getId().' , Organisme = '.$acronymeOrganisme.'] , Atexo_Chorus_Transmission::createFluxFenModeIntegre';
                $logger->error($erreurMessage);
                $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;

                return false;
            }

            //Siret acheteur
            if ($echange->getSiren() && $echange->getSiret()) {
                $siretAcheteur = $domDocument->createElement('SIRETAcheteur', Atexo_Util::toUtf8($echange->getSiren().$echange->getSiret()));
                $entete->appendChild($siretAcheteur);
            } else {
                Prado::log('Chorus Construction du flux: ERREUR Valeur vide pour SIRETAcheteur, identifiant echange '.$echange->getId(), TLogger::ERROR, 'Atexo.Chorus.Transmission.php');
                $erreurMessage = 'Chorus Construction du flux: ERREUR Valeur vide pour SIRETAcheteur : ERREUR Valeur vide pour TypeContrat : Echange [Id = '.$echange->getId().' , Organisme = '.$acronymeOrganisme.'] , Atexo_Chorus_Transmission::createFluxFenModeIntegre';
                $logger->error($erreurMessage);
                $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;

                return false;
            }

            //Organisation achat
            $organisationAchat = (new Atexo_Chorus_Echange())->retrieveOrganisationAchatById($echange->getIdOa(), $acronymeOrganisme);
            if (($organisationAchat instanceof CommonChorusOrganisationAchat) && $organisationAchat->getCode()) {
                $oa = $domDocument->createElement('OrganisationAchat', Atexo_Util::toUtf8($organisationAchat->getCode()));
                $entete->appendChild($oa);
            }

            //Date de notification
            if ($echange->getDateNotification() || $echange->getDateNotificationReelle()) {
                if ($echange->getDateNotificationReelle()) {
                    $dateNotification = $echange->getDateNotificationReelle();
                } else {
                    $dateNotification = $echange->getDateNotification();
                }
                if (!Atexo_Util::isDate(Atexo_Util::iso2frnDate($dateNotification))) {
                    Prado::log('Chorus Construction du flux: ERREUR mauvais format pour DateNotification, identifiant echange '.$echange->getId(), TLogger::ERROR, 'Atexo.Chorus.Transmission.php');
                    $erreurMessage = 'Chorus Construction du flux: ERREUR mauvais format pour DateNotification : ERREUR Valeur vide pour TypeContrat : Echange [Id = '.$echange->getId().' , Organisme = '.$acronymeOrganisme.'] , Atexo_Chorus_Transmission::createFluxFenModeIntegre';
                    $logger->error($erreurMessage);
                    $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;

                    return false;
                }
                $dateNotificationElement = $domDocument->createElement('DateNotification', Atexo_Util::toUtf8(Atexo_Util::frnDate2iso($dateNotification)));
                $entete->appendChild($dateNotificationElement);
            } else {
                Prado::log('Chorus Construction du flux: ERREUR Valeur vide pour DateNotification, identifiant echange '.$echange->getId(), TLogger::ERROR, 'Atexo.Chorus.Transmission.php');
                $erreurMessage = 'Chorus Construction du flux: ERREUR Valeur vide pour DateNotification : ERREUR Valeur vide pour TypeContrat : Echange [Id = '.$echange->getId().' , Organisme = '.$acronymeOrganisme.'] , Atexo_Chorus_Transmission::createFluxFenModeIntegre';
                $logger->error($erreurMessage);
                $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;

                return false;
            }

            //Code cpv
            if ($echange->getCpv1()) {
                $cpv = $echange->getCpv1();
                $cpvPrincipal = $domDocument->createElement('CodeCPV', Atexo_Util::toUtf8($cpv.'-0'));
                $entete->appendChild($cpvPrincipal);
            } else {
                Prado::log('Chorus Construction du flux: ERREUR Valeur vide pour CodeCPV, identifiant echange '.$echange->getId(), TLogger::ERROR, 'Atexo.Chorus.Transmission.php');
                $erreurMessage = 'Chorus Construction du flux: ERREUR Valeur vide pour CodeCPV : ERREUR Valeur vide pour TypeContrat : Echange [Id = '.$echange->getId().' , Organisme = '.$acronymeOrganisme.'] , Atexo_Chorus_Transmission::createFluxFenModeIntegre';
                $logger->error($erreurMessage);
                $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;

                return false;
            }

            //Date fin marche
            $dateReelleFinMarche = $echange->getDateFinMarcheReelle();
            if (!$dateReelleFinMarche) {
                $dateReelleFinMarche = $echange->getDateFinMarche();
            }
            if ($dateReelleFinMarche) {
                if (!Atexo_Util::isDate(Atexo_Util::iso2frnDate($dateReelleFinMarche))) {
                    Prado::log('Chorus Construction du flux: Mauvais format pour DateFinAccordCadre, identifiant echange '.$echange->getId(), TLogger::ERROR, 'Atexo.Chorus.Transmission.php');
                    $erreurMessage = 'Chorus Construction du flux: Mauvais format pour DateFinAccordCadre : ERREUR Valeur vide pour TypeContrat : Echange [Id = '.$echange->getId().' , Organisme = '.$acronymeOrganisme.'] , Atexo_Chorus_Transmission::createFluxFenModeIntegre';
                    $logger->error($erreurMessage);
                    $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;

                    return false;
                }
                $dateFinMarche = $domDocument->createElement('DateFinAccordCadre', Atexo_Util::toUtf8(Atexo_Util::frnDate2iso($dateReelleFinMarche)));
                $entete->appendChild($dateFinMarche);
            } else {
                Prado::log('Chorus Construction du flux: ERREUR Valeur vide pour DateFinAccordCadre, identifiant echange '.$echange->getId(), TLogger::ERROR, 'Atexo.Chorus.Transmission.php');
                $erreurMessage = 'Chorus Construction du flux: ERREUR Valeur vide pour DateFinAccordCadre : ERREUR Valeur vide pour TypeContrat : Echange [Id = '.$echange->getId().' , Organisme = '.$acronymeOrganisme.'] , Atexo_Chorus_Transmission::createFluxFenModeIntegre';
                $logger->error($erreurMessage);
                $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;

                return false;
            }

		if($echange->getIntituleContrat()) {
            $intitule = self::getChaineTronqueeEnvoiChorus($echange->getIntituleContrat(), 50, $logger, $erreursFluxChorus);
            $intituleAC = $domDocument->createElement(
                'IntituleAC',
                htmlspecialchars(Atexo_Util::toUtf8($intitule))
            );
			$entete->appendChild($intituleAC);
		}else{
			Prado::log("Chorus Construction du flux: ERREUR Valeur vide pour IntituleAC, identifiant echange " . $echange->getId() ,TLogger::ERROR, "Atexo.Chorus.Transmission.php");
			$erreurMessage = "Chorus Construction du flux: ERREUR Valeur vide pour IntituleAC : ERREUR Valeur vide pour TypeContrat : Echange [Id = " . $echange->getId(). " , Organisme = " . $acronymeOrganisme . "] , Atexo_Chorus_Transmission::createFluxFenModeIntegre";
			$logger->error($erreurMessage);
			$erreursFluxChorus[] = "Erreur lors de la generation du flux Chorus" . PHP_EOL . $erreurMessage;
			return false;
		}

		if($echange->getObjetContrat()) {
			$objet = self::getChaineTronqueeEnvoiChorus($echange->getObjetContrat(), 50, $logger, $erreursFluxChorus);
			$objetAC = $domDocument->createElement(
                'ObjetAC',
                htmlspecialchars(Atexo_Util::toUtf8($objet))
            );
			$entete->appendChild($objetAC);
		}else{
			Prado::log("Chorus Construction du flux: ERREUR Valeur vide pour ObjetAC, identifiant echange " . $echange->getId() ,TLogger::ERROR, "Atexo.Chorus.Transmission.php");
			$erreurMessage = "Chorus Construction du flux: ERREUR Valeur vide pour ObjetAC : ERREUR Valeur vide pour TypeContrat : Echange [Id = " . $echange->getId(). " , Organisme = " . $acronymeOrganisme . "] , Atexo_Chorus_Transmission::createFluxFenModeIntegre";
			$logger->error($erreurMessage);
			$erreursFluxChorus[] = "Erreur lors de la generation du flux Chorus" . PHP_EOL . $erreurMessage;
			return false;
		}

            //Montant
            if ($echange->getMontantHt()) {
                $acAmont = $domDocument->createElement('AC_AMOUNT', Atexo_Util::toUtf8($echange->getMontantHt()));
                $entete->appendChild($acAmont);
            }

            $contratQuery = new CommonTContratTitulaireQuery();
            $contrat = $contratQuery->getTContratTitulaireById($echange->getIdDecision(), $connexion);
            $contrat = null;
            $contrat = null;
            try {
                $execWS = Atexo_Util::getSfService(WebServicesExec::class);
                if ($echange->getUuidExterneExec()) {
                    $contrat = $execWS->getContent('getContratByUuid', $echange->getUuidExterneExec());
                }
            } catch (\Exception $exception) {
                $erreurMessage = 'Erreur Objet invalide -  Echange : '.print_r($echange, true)." , \n Contrat : ".print_r($contrat, true)." ] , \n Methode = Atexo_Chorus_GestionFichierChorus::createFluxFenModeIntegre";
                $logger->error($erreurMessage);
                $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus' . PHP_EOL . $erreurMessage;
                return false;
            }

            if ($echange instanceof CommonChorusEchange && is_array($contrat)) {
                (new Atexo_Chorus_GestionFichierChorus())->getFileDceForTransmitionChorus($echange, $contrat, $acronymeOrganisme, false, $domDocument, $engagementJuridique, $erreursFluxChorus, $logger);

                (new Atexo_Chorus_GestionFichierChorus())->getFileEnvAeForTransmitionChorus($echange, $acronymeOrganisme, false, $domDocument, $engagementJuridique, $erreursFluxChorus, $logger, $contrat);

                (new Atexo_Chorus_GestionFichierChorus())->getFileSignaceForTransmitionChorus($echange, $contrat, $acronymeOrganisme, false, $domDocument, $engagementJuridique, $erreursFluxChorus, $logger);

                (new Atexo_Chorus_GestionFichierChorus())->getFileEnvForTransmitionChorus($echange, $contrat, $acronymeOrganisme, false, $domDocument, $engagementJuridique, $erreursFluxChorus, $logger);

                (new Atexo_Chorus_GestionFichierChorus())->getFilePiecesExternesForTransmitionChorus($echange, $acronymeOrganisme, false, $domDocument, $engagementJuridique, $erreursFluxChorus, $logger, $contrat);

                (new Atexo_Chorus_GestionFichierChorus())->getFilePiecesJointesForTransmitionChorus($echange, $acronymeOrganisme, false, $domDocument, $engagementJuridique, $erreursFluxChorus, $logger, $contrat);

                (new Atexo_Chorus_GestionFichierChorus())->getFilePiecesOuvertureAnlyseChorus($echange, $acronymeOrganisme, false, $domDocument, $engagementJuridique, $erreursFluxChorus, $logger, $contrat);

                //dume Acheteur
                (new Atexo_Chorus_GestionFichierChorus())->getFilePiecesDumeForTransmitionChorus($echange, $acronymeOrganisme, Atexo_Config::getParameter('TYPE_DUME_ACHETEUR'), false, $domDocument, $engagementJuridique, $erreursFluxChorus, $logger, $contrat);

                //dume OE
                (new Atexo_Chorus_GestionFichierChorus())->getFilePiecesDumeForTransmitionChorus($echange, $acronymeOrganisme, Atexo_Config::getParameter('TYPE_DUME_OE'), false, $domDocument, $engagementJuridique, $erreursFluxChorus, $logger, $contrat);

                //TODO : envoi pieces de notification
                //Atexo_Chorus_GestionFichierChorus::getFileNotificationForTransmitionChorus($echange, $contrat, $acronymeOrganisme, false, $domDocument, $engagementJuridique);
                //Transmission des signatures des fichiers autres que l'acte d'engagement
                (new Atexo_Chorus_GestionFichierChorus())->getFileSignEnvForTransmitionChorus($echange, $contrat, $acronymeOrganisme, false, $domDocument, $engagementJuridique, $erreursFluxChorus, $logger);
                //Transmission des fichiers enveloppes de la fonctionnalité: réponse pas à pas
                (new Atexo_Chorus_GestionFichierChorus())->getFileEnvRepPasAPasForTransmitionChorus($echange, $contrat, $acronymeOrganisme, false, $domDocument, $engagementJuridique, $erreursFluxChorus, $logger);
            } else {
                $erreurMessage = 'Erreur Objet invalide -  Echange : '.print_r($echange, true)." , \n Contrat : ".print_r($contrat, true)." ] , \n Methode = Atexo_Chorus_GestionFichierChorus::createFluxFenModeIntegre";
                $logger->error($erreurMessage);
                $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus' . PHP_EOL . $erreurMessage;
				return false;
			}

            // Attributaire
            $attributaire = $domDocument->createElement('Attributaire', '');
            $engagementJuridique->appendChild($attributaire);

            if ($echange->getRaisonSocialeAttributaire()) {
                $raisonSocialeAttributaire = $domDocument->createElement(
                    'RaisonSocialeAttributaire',
                    Atexo_Util::toUtf8($echange->getRaisonSocialeAttributaire())
                );
                $attributaire->appendChild($raisonSocialeAttributaire);
            } else {
                 self::logInfo($echange, $acronymeOrganisme, $logger, 'Attributaire');
            }

            if ($echange->getSiretAttributaire()) {
                $siretAttributaire = $domDocument->createElement(
                    'SIRETAttributaire',
                    (Atexo_Util::toUtf8($echange->getSiretAttributaire()) != 1) ? Atexo_Util::toUtf8($echange->getSiretAttributaire()) : null
                );
                $attributaire->appendChild($siretAttributaire);
            } else {
               self::logInfo($echange, $acronymeOrganisme, $logger, 'SIRETAttributaire');
            }

            if ($echange->getCodeApe()) {
                $codeApe = $domDocument->createElement(
                    'CodeAPE',
                    Atexo_Util::toUtf8($echange->getCodeApe())
                );
                $attributaire->appendChild($codeApe);
            } else {
                self::logInfo($echange, $acronymeOrganisme, $logger, 'CodeAPE');
            }

            if ($echange->getFormeJuridique()) {
                $formeJuridique = $domDocument->createElement(
                    'FormeJuridique',
                    Atexo_Util::toUtf8($echange->getFormeJuridique())
                );
                $attributaire->appendChild($formeJuridique);
            } else {
                 self::logInfo($echange, $acronymeOrganisme, $logger, 'FormeJuridique');
            }
            $pme = 0;
            if ($echange->getPme() === true) {
                $pme = 1;
            }

            $pme = $domDocument->createElement(
                'PME',
                $pme
            );
            $attributaire->appendChild($pme);

            if ($echange->getPaysTerritoire()) {
                $paysTerritoire = $domDocument->createElement(
                    'PaysTerritoire',
                    Atexo_Util::toUtf8($echange->getPaysTerritoire())
                );
                $attributaire->appendChild($paysTerritoire);
            } else {
                self::logInfo($echange, $acronymeOrganisme, $logger, 'PaysTerritoire');
            }

            if ($echange->getNumeroNationalAttributaire()) {
                $numNationalAttributaire = $domDocument->createElement(
                    'NumeroNationalAttributaire',
                    Atexo_Util::toUtf8($echange->getNumeroNationalAttributaire())
                );
                $attributaire->appendChild($numNationalAttributaire);
            } else {
                self::logInfo($echange, $acronymeOrganisme, $logger, 'NumeroNationalAttributaire');
            }

            $logger->info('Fin creation flux FEN 211: Echange [Id = '.$echange->getId().' , Organisme = '.$acronymeOrganisme.'] , Atexo_Chorus_Transmission::createFluxFenModeIntegre');

            return $domDocument->saveXML();
        } catch (Exception $ex) {
            $erreurMessage = 'Erreur lors de la generation du flux Chorus FEN211 [ '.$ex->getMessage().' ] , Atexo_Chorus_Transmission::createFluxFenModeIntegre';
            $logger->error($erreurMessage);
            $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;

            return false;
        }
    }

    private static function logErrors(
        CommonChorusEchange $echange,
        string $acronymeOrganisme,
        \Logger $logger,
        string $element
    ): string {
        Prado::log(
            "Chorus Construction du flux: ERREUR Valeur vide pour ". $element . ", identifiant echange " .
            $echange->getId() ,TLogger::ERROR, "Atexo.Chorus.Transmission.php"
        );
        $erreurMessage =
            "Chorus Construction du flux: ERREUR Valeur vide pour " . $element
            . ": ERREUR Valeur vide pour TypeContrat : Echange [Id = "
            . $echange->getId()
            . " , Organisme = " . $acronymeOrganisme . "] , Atexo_Chorus_Transmission::createFluxFenModeIntegre";
        $logger->error($erreurMessage);

        return "Erreur lors de la generation du flux Chorus" . PHP_EOL . $erreurMessage;
    }

    private static function logInfo(
        CommonChorusEchange $echange,
        string $acronymeOrganisme,
        LoggerInterface $logger,
        string $element
    ): string {
        Prado::log(
            "Chorus Construction du flux: INFO Valeur vide pour ". $element . ", identifiant echange " .
            $echange->getId() ,TLogger::INFO, "Atexo.Chorus.Transmission.php"
        );
        $infoMessage =
            "Chorus Construction du flux: Info Valeur vide pour " . $element
            . ": INFO Valeur vide pour TypeContrat : Echange [Id = "
            . $echange->getId()
            . " , Organisme = " . $acronymeOrganisme . "] , Atexo_Chorus_Transmission::createFluxFenModeIntegre";
        $logger->info($infoMessage);

        return "INFO lors de la generation du flux Chorus" . PHP_EOL . $infoMessage;
    }

	/**
	 * Permet de tronquer une chaine de caractere
	 * Nettoie la fin de la chaine des caractères spéciaux.
     *
     * @param string $chaine:           chaine de caractere a tronquer et nettoyer
     * @param int    $longueurMax:      longueur max de la chaine
     * @param array  $erreursFluxChorus : liste des erreurs flux Chorus
     *
     * @return mixed
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place-objet-contrat
     *
     * @copyright Atexo 2016
     */
    private function getChaineTronqueeEnvoiChorus($chaine, $longueurMax, $logger = null, &$erreursFluxChorus = null)
    {
        if (empty($logger)) {
            $logger = Atexo_LoggerManager::getLogger('chorus');
        }
        try {
            $chaine = substr($chaine, 0, $longueurMax);

            return preg_replace('#^(.*)(\&|\&\#|\&\#0|\&\#03|\&\#039|\&a|\&am|\&amp|\&q|\&qu|\&quo|\&quot|\&ap|\&apo|\&apos|\&l|\&lt|\&g|\&gt)$#i', '$1', $chaine);
        } catch (Exception $e) {
            $erreurMessage = 'Erreur: '.PHP_EOL.$e->getMessage().'Trace: '.PHP_EOL.$e->getTraceAsString();
            $logger->error($erreurMessage);
            if (null !== $erreursFluxChorus) {
                $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;
            }
        }
    }
}
