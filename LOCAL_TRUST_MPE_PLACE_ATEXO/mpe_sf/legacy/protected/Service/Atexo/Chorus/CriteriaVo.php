<?php

namespace Application\Service\Atexo\Chorus;

/**
 * Vo pour définir les critères de recherche concernant CHORUS.
 *
 * @author KONATE Oumar <oumar.konate@atexo.com>
 * @copyright Atexo 2011
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Chorus_CriteriaVo
{
    private $referenceConsultation;
    private $typeProcedure;
    private $categorie;
    private $organisme;
    private $typeFichierEchange;
    private $periodeEchangeStart;
    private $periodeEchangeEnd;
    private $retourChorus;
    private $statutEj;
    private $numeroMarche;
    private $idsService;
    private $idService;
    private $idDecision;
    private $numOrdre;

    private $limit;
    private $offset;
    private $commandeName;
    private $sensTri;
    private $count;
    private $typeContrat;
    private $referenceLibre;

    public function getReferenceConsultation()
    {
        return $this->referenceConsultation;
    }

    public function setReferenceConsultation($reference)
    {
        $this->referenceConsultation = $reference;
    }

    public function getTypeProcedure()
    {
        return $this->typeProcedure;
    }

    public function setTypeProcedure($typeProcedure)
    {
        $this->typeProcedure = $typeProcedure;
    }

    public function getCategorie()
    {
        return $this->categorie;
    }

    public function setCategorie($categorie)
    {
        $this->categorie = $categorie;
    }

    public function getOrganisme()
    {
        return $this->organisme;
    }

    public function setOrganisme($organisme)
    {
        $this->organisme = $organisme;
    }

    public function getTypeFichierEchange()
    {
        return $this->typeFichierEchange;
    }

    public function setTypeFichierEchange($typeFichierEchange)
    {
        $this->typeFichierEchange = $typeFichierEchange;
    }

    public function getPeriodeEchangeStart()
    {
        return $this->periodeEchangeStart;
    }

    public function setPeriodeEchangeStart($periodeEchangeStart)
    {
        $this->periodeEchangeStart = $periodeEchangeStart;
    }

    public function getPeriodeEchangeEnd()
    {
        return $this->periodeEchangeEnd;
    }

    public function setPeriodeEchangeEnd($periodeEchangeEnd)
    {
        $this->periodeEchangeEnd = $periodeEchangeEnd;
    }

    public function getRetourChorus()
    {
        return $this->retourChorus;
    }

    public function setRetourChorus($retourChorus)
    {
        $this->retourChorus = $retourChorus;
    }

    public function getStatutEj()
    {
        return $this->statutEj;
    }

    public function setStatutEj($statutEj)
    {
        $this->statutEj = $statutEj;
    }

    public function getNumeroMarche()
    {
        return $this->numeroMarche;
    }

    public function setNumeroMarche($numeroMarche)
    {
        $this->numeroMarche = $numeroMarche;
    }

    public function setIdsService($idsService)
    {
        $this->idsService = $idsService;
    }

    public function getIdsService()
    {
        return $this->idsService;
    }

    public function setIdService($idsService)
    {
        $this->idService = $idsService;
    }

    public function getIdService()
    {
        return $this->idService;
    }

    public function setIdDecision($idDecision)
    {
        $this->idDecision = $idDecision;
    }

    public function getIdDecision()
    {
        return $this->idDecision;
    }

    public function setNumOrdre($numOrdre)
    {
        $this->numOrdre = $numOrdre;
    }

    public function getNumOrdre()
    {
        return $this->numOrdre;
    }

    public function setLimit($limit)
    {
        $this->limit = $limit;
    }

    public function getLimit()
    {
        return $this->limit;
    }

    public function setOffset($offset)
    {
        $this->offset = $offset;
    }

    public function getOffset()
    {
        return $this->offset;
    }

    public function setCommandeName($commandeName)
    {
        $this->commandeName = $commandeName;
    }

    public function getCommandeName()
    {
        return $this->commandeName;
    }

    public function setSensTri($sensTri)
    {
        $this->sensTri = $sensTri;
    }

    public function getSensTri()
    {
        return $this->sensTri;
    }

    public function setCount($v)
    {
        $this->count = $v;
    }

    public function getCount()
    {
        return $this->count;
    }

    /**
     * @return mixed
     */
    public function getTypeContrat()
    {
        return $this->typeContrat;
    }

    /**
     * @param mixed $typeContrat
     */
    public function setTypeContrat($typeContrat)
    {
        $this->typeContrat = $typeContrat;
    }

    /**
     * @return mixed
     */
    public function getReferenceLibre()
    {
        return $this->referenceLibre;
    }

    /**
     * @param mixed $referenceLibre
     */
    public function setReferenceLibre($referenceLibre)
    {
        $this->referenceLibre = $referenceLibre;
    }
}
