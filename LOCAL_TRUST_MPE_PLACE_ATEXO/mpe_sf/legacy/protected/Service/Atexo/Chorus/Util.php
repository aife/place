<?php

namespace Application\Service\Atexo\Chorus;

use App\Service\WebServices\WebServicesExec;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonChorusEchange;
use Application\Propel\Mpe\CommonChorusEchangePeer;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonDCE;
use Application\Propel\Mpe\CommonFichierEnveloppe;
use Application\Propel\Mpe\CommonTChorusFicheModificative;
use Application\Propel\Mpe\CommonTChorusFicheModificativeQuery;
use Application\Propel\Mpe\CommonTContratTitulaire;
use Application\Propel\Mpe\CommonTContratTitulaireQuery;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Exception;
use Application\Service\Atexo\Atexo_Files;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_Zip;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Decision;
use Application\Service\Atexo\Helios\Atexo_Helios_PiecesAttributaires;
use Exception;
use PDO;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * Contient des fonctions Utiles pour Chorus.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2009
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Chorus_Util
{
    public function generateChorusUniqueId($idOrganisme, $idDecision, $charPad = 'Z', &$erreursFluxChorus = null, $logger = null, string $uuidExec = '')
    {
        if (empty($logger)) {
            $logger = Atexo_LoggerManager::getLogger('chorus');
        }
        $logger->info('Debut Generation uniqueID [id_contrat = '.$idDecision.' , IdOrganisme = '.$idOrganisme.']');
        try {
            $uniqueId = $idOrganisme.$charPad.$idDecision;
            $acronymeOrganisme = (new Atexo_Organismes())->retrieveAccronymeOrganisme($idOrganisme);

            //fonction de calcul standard de l'identifiant unique
            //id_ministereZid_decision complété par des Z de manière à obtenir une longueur fixe de CHORUS_PAD_LENGTH caractères
            $uniqueId = str_pad($uniqueId, Atexo_Config::getParameter('CHORUS_PAD_LENGTH'), $charPad, STR_PAD_RIGHT);

            $dernierEchange = (new Atexo_Chorus_Echange())->retreiveLastEchangeCreatedWithUuidExec($acronymeOrganisme, $uuidExec);
            if ($dernierEchange instanceof CommonChorusEchange) {
                $numOrdre = $dernierEchange->getNumOrdre();

                //cas num 1 : si jamais c le premier échange alors renvoyer le calcul standard
                if (intval($numOrdre) < 2) {
                    $logger->info('Generation uniqueID [id_contrat = '.$idDecision.' , IdOrganisme = '.$idOrganisme."] : numero d'ordre = $numOrdre , unique ID = $uniqueId");

                    return $uniqueId;
                }

                //cas num 2 : il y a n+1 échange avec n>0 et dans ce cas il faut regarder le dernier statut de l'EJ
                //si le dernier statut est supprimé alors il faut incrementer, sinon il faut prendre celui qui a été enregistré dans l'avant dernier échange (échange n)
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
                $contratQuery = new CommonTContratTitulaireQuery();
                $contrat = $contratQuery->getTContratTitulaireById($idDecision, $connexion);
                $contrat = null;
                try {
                    $execWS = Atexo_Util::getSfService(WebServicesExec::class);
                    if ($dernierEchange->getUuidExterneExec()) {
                        $contrat = $execWS->getContent('getContratByUuid', $dernierEchange->getUuidExterneExec());
                    }

                } catch (\Exception $exception) {
                    $erreurMessage = 'Atexo_Chorus_Util::generateChorusUniqueId : Contrat non trouve [id_contrat = '.$idDecision.', Organisme = '.$acronymeOrganisme.']';
                    $logger->error($erreurMessage);
                    $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;

                    return false;
                }
                //recherche du dernier statut EJ
                if (is_array($contrat)) {
                    $numeroMarche = $contrat['numero'];
                    $dernierStatutEJDecision = self::retournLastStatutEJDecision($contrat['statutEj'], $numeroMarche);
                } else {
                    $erreurMessage = 'Atexo_Chorus_Util::generateChorusUniqueId : Contrat non trouve [id_contrat = '.$idDecision.', Organisme = '.$acronymeOrganisme.']';
                    $logger->error($erreurMessage);
                    $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;

                    return false;
                }

                //on teste, si le dernier statut EJ de l'échange est Supprimé alors il faut regénérer un unique avec incrément
                //le dernier digit de l'identifiant ne sera pas un Z mais un chiffre identifiant le numéro d'echange suivant le numéro d'echange
                //correspondant à la suppression
                if (strstr($dernierStatutEJDecision, Atexo_Config::getParameter('CHORUS_STATUT_EJ_SUPPRIME'))) {
                    $lenghtNumOrdre = strlen($numOrdre);
                    //on prend les X premiers caractères de l'unique id générique et on rajoute le numéro d'ordre de l'échange de manière à avoir
                    //au total toujours CHORUS_PAD_LENGTH caractères
                    $uniqueId = substr($uniqueId, 0, strlen($uniqueId) - intval($lenghtNumOrdre)).$numOrdre;
                    $logger->info('Generation uniqueID [id_contrat = '.$idDecision.' , IdOrganisme = '.$idOrganisme."] : Cas suppression EJ, unique ID = $uniqueId");

                    return $uniqueId;
                } else {
                    $avantDernierEchange = (new Atexo_Chorus_Echange())->retrievePenultimateEchangeCreated($acronymeOrganisme, $uuidExec);
                    if ($avantDernierEchange instanceof CommonChorusEchange) {
                        $logger->info('Generation uniqueID [id_contrat = '.$idDecision.' , IdOrganisme = '.$idOrganisme."] : recuperation unique ID de l'echange precedent = ".$avantDernierEchange->getIdEjAppliExt());

                        return $avantDernierEchange->getIdEjAppliExt();
                    } else {
                        $erreurMessage = "Erreur lors de la generation de l'unique id [id_contrat = ".$idDecision.', Organisme = '.$acronymeOrganisme.'] , Methode = Atexo_Chorus_Echange::retrievePenultimateEchangeCreated non trouvé';
                        $logger->error($erreurMessage);
                        $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;

                        return false;
                    }
                }
            } else {
                $erreurMessage = "Erreur : Aucun Echange n'est trouvé [id_contrat = ".$idDecision.', Organisme = '.$acronymeOrganisme.'] , Methode = Atexo_Chorus_Echange::retreiveLastEchangeCreated non trouvé';
                $logger->error($erreurMessage);
                $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;

                return false;
            }
        } catch (\Exception $e) {
            $erreurMessage = 'Erreur lors de la generation de l\'unique id [id_contrat = '.$idDecision.', Organisme = '.$acronymeOrganisme.'] , Methode = Atexo_Chorus_Util::generateChorusUniqueId , Erreur = ['.$e->getMessage().']'.PHP_EOL.'Trace : '.$e->getTraceAsString();
            $logger->error($erreurMessage);
            $erreursFluxChorus[] = 'Erreur lors de la generation du flux Chorus'.PHP_EOL.$erreurMessage;

            return false;
        }
        $logger->info('Debut Generation uniqueID [id_contrat = '.$idDecision.' , IdOrganisme = '.$idOrganisme.']');
    }

    /**
     * retourn un tableau avec les num de marché et leurs statuts correspendants
     * $numMarche = valeur du champs numero_marche de la table decisionEnveloppe
     * $statutej = valeur du champs statutEJ de la table decisionEnveloppe.
     */
    public function returnInformationRetourChorus($numMarche, $statutej)
    {
        $dataSource = [];
        $occurence = 0;
        if ($numMarche || $statutej) {
            $arrayNumMarche = explode('#', $numMarche);
            foreach ($arrayNumMarche as $oneNumMarche) {
                if (null != $oneNumMarche || '' != $oneNumMarche) {
                    $arrayOneNumMarche = explode('_', $oneNumMarche);
                    $numero = $arrayOneNumMarche[1];
                    $ordre = $arrayOneNumMarche[0];
                    $dataSource[$ordre]['numero'] = $numero;
                }
            }
            $arrayStatutej = explode('#', $statutej);
            foreach ($arrayStatutej as $oneStatut) {
                if (null != $oneStatut || '' != $oneStatut) {
                    $arrayOneStatut = explode('_', $oneStatut);
                    $statut = $arrayOneStatut[1];
                    $ordre = $arrayOneStatut[0];
                    $dataSource[$ordre]['statut'] = $statut;
                }
            }
        } else {
            $dataSource[$occurence]['numero'] = Prado::localize('TEXT_NON_GENERE');
            $dataSource[$occurence]['statut'] = Prado::localize('TEXT_SANS_OBJET');
        }

        return $dataSource;
    }

    /**
     * retourn le dernier numero de marche correspondant à une decision
     * si jamais le numero marche ne contient pas de # à ce moment renvoi la valeur passée en paramètre
     * $numerosMarche = valeur du champs numero_marche de la table decisionEnveloppe.
     */
    public function retournLastNumeroMarcheDecision($numerosMarche)
    {
        //il faut faire -2 car dans le explode on a 2 éléments au moins
        $arrayNumMarche = explode('#', $numerosMarche);
        if (count($arrayNumMarche) > 1) {
            return str_replace('_', '', strstr($arrayNumMarche[count($arrayNumMarche) - 2], '_'));
        }

        return $numerosMarche;
    }

    /**
     * retourn le statut de l'EJ correspondant à une decision et un numéro de marché donné
     * si jamais le statut EJ ne contient pas de # à ce moment renvoi la valeur passée en paramètre
     * $statutEJ = valeur du champs statut_EJ de la table decisionEnveloppe
     * il peut avoir la forme
     *     vide
     *     1_1234568;Supprimé#2_123468910;Commandé#
     *  1_1234561;Supprimé#
     *  1_1234562;Commandé#.
     */
    public function retournLastStatutEJDecision($statutEJ, $numMarche)
    {
        //explode renvoi vide si jamais statutEJ est vide
        $arrayStatutEJ = explode('#', $statutEJ);
        $nbreStatutEJ = count($arrayStatutEJ);

        if ($nbreStatutEJ > 1) {
            //il faut faire -2 car dans le explode on a 2 éléments au moins
            //cette opération donne pour chaque exemple avec le dernier numero marché
            //123468910 --> 123468910;Commandé
            //1234561 --> 1234561;Supprimé
            //1234562 --> 1234562;Commandé
            $statutComplet = str_replace('_', '', strstr($arrayStatutEJ[$nbreStatutEJ - 2], '_'));
            $arrayStatutComplet = explode(';', $statutComplet);
            if (count($arrayStatutComplet) > 1) {
                if ($arrayStatutComplet[0] == $numMarche) {
                    return $arrayStatutComplet[1];
                }

                return '';
            }

            return '';
        }

        return $statutEJ;
    }

    public function getNomPiecesReponceEnvoyeAChorus($consultationId, $organisme, $decision, $IdsEnvItems)
    {
        $attibutaires = (new Atexo_Consultation_Decision())->retrieveAttributaireInformation($organisme, $consultationId, $decision->getLot());
        $piecesAttributaires = (new Atexo_Helios_PiecesAttributaires())->retreivePiecesAttributaires($attibutaires[0]['id_enveloppe'], Atexo_Config::getParameter('DEPOT_ELECTRONIQUE'));
        $pj = [];
        foreach ($piecesAttributaires as $OnePiece) {
            if ($OnePiece instanceof CommonFichierEnveloppe) {
                if ('ACE' == $OnePiece->getTypeFichier()) {
                    $pj[] = $OnePiece->getNomFichier();
                } else {
                    $IdsEnvItems = explode(';', $IdsEnvItems);
                    $idBlob = $OnePiece->getIdBlob();
                    $blobResource = Atexo_Blob::acquire_lock_on_blob($idBlob, $organisme);
                    $blob_file = $blobResource['blob']['pointer'];
                    if ((new Atexo_Zip())->verifyZipIntegrity($blob_file)) {
                        $piecesEnv = (new Atexo_Files())->getFilesFromZip($blob_file, $IdsEnvItems);
                        foreach ($piecesEnv as $pieceEnv) {
                            $pj[] = $pieceEnv['name'];
                        }
                    }
                }
            }
        }

        return $pj;
    }

    public function getNomFichierDceEnvoyeAChorus($dce, $Item, $organisme)
    {
        $pj = [];
        if ($dce instanceof CommonDCE && '0' != $dce->getDce()) {
            $Item = explode(';', $Item);
            $blobResource = Atexo_Blob::acquire_lock_on_blob($dce->getDce(), $organisme);
            $blob_file = $blobResource['blob']['pointer'];
            $piecesDce = (new Atexo_Files())->getFilesFromZip($blob_file, $Item);
            foreach ($piecesDce as $pieceDce) {
                $pj[] = $pieceDce['name'];
            }
        }

        return $pj;
    }

    public function retrieveLotsForDecisionChorus($reference, $lot, $organisme, $commandeName = null, $sensTri = null)
    {
        $queryParams = [];
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $consultation = (new Atexo_Consultation())->retrieveConsultation($reference, $organisme);
        if (!($consultation instanceof CommonConsultation)) {
            throw new Atexo_Exception('No Consultation  found');
        }
        $req1 = '';
        $req2 = '';
        //Cas des offres electroniques
        if ('1' == $consultation->getAlloti()) {
            $req1 = 'SELECT DE.id_decision_enveloppe, DE.numero_marche, DE.statutEJ, DL.id_type_decision, DL.date_decision, O.nom_entreprise_inscrit as nom_entreprise, CL.description as description, ';
            $req1 .= ' CL.lot as numLot, TD.code_type_decision FROM `decisionEnveloppe` as DE, Offres as O, DecisionLot as DL, CategorieLot as CL, TypeDecision as TD ';
            $req1 .= '  WHERE DE.id_offre = O.id AND DE.`consultation_id` = DL.consultation_id AND DE.`lot` = DL.lot AND CL.consultation_id = :consultationId ';
            $req1 .= "   AND CL.lot = DE.`lot` AND TD.id_type_decision = DL.id_type_decision AND DE.type_enveloppe = '1' AND DE.`consultation_id` = :consultationId ";
            $req1 .= '   AND  DE.`organisme` = DL.organisme AND CL.organisme = DE.`organisme` AND DE.organisme = O.organisme AND DE.organisme = :organisme ';
        } else {
            $req1 = 'SELECT DE.id_decision_enveloppe, DE.numero_marche, DE.statutEJ, DL.id_type_decision, DL.date_decision, O.nom_entreprise_inscrit as nom_entreprise, C.objet as description, ';
            $req1 .= ' DE.`lot` as numLot, TD.code_type_decision FROM `decisionEnveloppe` as DE, Offres as O, DecisionLot as DL, consultation as C, TypeDecision as TD ';
            $req1 .= '  WHERE DE.id_offre = O.id AND DE.`consultation_id` = DL.consultation_id AND DE.`lot` = DL.lot ';
            $req1 .= "  AND TD.id_type_decision = DL.id_type_decision AND DE.type_enveloppe = '1' AND C.id = :consultationId AND DE.`consultation_id` = :consultationId ";
            $req1 .= '   AND  DE.organisme = O.organisme AND DE.`organisme` = DL.organisme AND DE.`organisme` = DL.organisme AND DE.organisme = :organisme AND C.`organisme` = DE.`organisme` ';
        }
        //Cas des offres papiers
        if ('1' == $consultation->getAlloti()) {
            $req2 = 'SELECT DE.id_decision_enveloppe, DE.numero_marche, DE.statutEJ, DL.id_type_decision, DL.date_decision, P.nom_entreprise as nom_entreprise, CL.description as description, ';
            $req2 .= ' CL.lot as numLot, TD.code_type_decision FROM `decisionEnveloppe` as DE, Offre_papier as P, DecisionLot as DL, CategorieLot as CL, TypeDecision as TD ';
            $req2 .= '  WHERE DE.id_offre = P.id AND DE.`consultation_id` = DL.consultation_id AND DE.`lot` = DL.lot AND CL.consultation_id = :consultationId ';
            $req2 .= "   AND CL.lot = DE.`lot` AND TD.id_type_decision = DL.id_type_decision AND DE.type_enveloppe = '2' AND DE.`consultation_id` = :consultationId ";
            $req2 .= '   AND  DE.`organisme` = DL.organisme AND CL.organisme = DE.`organisme` AND DE.organisme = P.organisme AND DE.organisme = :organisme ';
        } else {
            $req2 = 'SELECT DE.id_decision_enveloppe, DE.numero_marche, DE.statutEJ, DL.id_type_decision, DL.date_decision, P.nom_entreprise as nom_entreprise, C.objet as description, ';
            $req2 .= ' DE.`lot` as numLot, TD.code_type_decision FROM `decisionEnveloppe` as DE, Offre_papier as P, DecisionLot as DL, consultation as C, TypeDecision as TD ';
            $req2 .= '  WHERE DE.id_offre = P.id AND DE.`consultation_id` = DL.consultation_id AND DE.`lot` = DL.lot ';
            $req2 .= "  AND TD.id_type_decision = DL.id_type_decision AND DE.type_enveloppe = '2' AND C.id = :consultationId AND DE.`consultation_id` = :consultationId ";
            $req2 .= '   AND  DE.organisme = P.organisme AND DE.`organisme` = DL.organisme AND DE.`organisme` = DL.organisme AND DE.organisme = :organisme  AND C.`organisme` = DE.`organisme` ';
        }

        $req = " SELECT * FROM ( $req1 UNION $req2 ) T WHERE 1 = 1 ";
        if ('' != $commandeName) {
            $req .= ' ORDER BY ';
            if ('lot' == $commandeName) {
                $req .= " numLot $sensTri ";
            } elseif ('entrepriseAttributaire' == $commandeName) {
                $req .= " nom_entreprise $sensTri ";
            } elseif ('statutEj' == $commandeName) {
                $req .= " statutEJ $sensTri ";
            } elseif ('numeroMarche' == $commandeName) {
                $req .= " numero_marche $sensTri ";
            }
        }
        $queryParams[':consultationId'] = $reference;
        $queryParams[':organisme'] = $organisme;

        /*try {
            foreach($queryParams as $a => $b) {
                if($req) {
                    $req = str_replace($a."", "'".$b."' ", $req);
                }

            }
            echo $req;exit;
        } catch(Exception $e ) {

        }*/

        $statement = Atexo_Db::getLinkCommon(true)->prepare($req);
        try {
            $statement->execute($queryParams);
        } catch (Exception $e) {
            //print_r($statement);exit;
            Prado::log('Erreur Util.php'.$e->getMessage().$e->getTraceAsString(), TLogger::ERROR, 'Util.php');
        }
        $arrayResult = [];
        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            $c = new Criteria();
            $c->add(CommonChorusEchangePeer::ID_DECISION, $row['id_decision_enveloppe']);
            $c->add(CommonChorusEchangePeer::ORGANISME, $organisme);
            $chorusEchanges = CommonChorusEchangePeer::doSelect($c, $connexion);
            if (is_array($chorusEchanges) && count($chorusEchanges)) {
                $id = $row['id_decision_enveloppe'];
                $arrayResult[$id]['idDecisionEnv'] = $row['id_decision_enveloppe'];
                $arrayResult[$id]['numeroMarche'] = $row['numero_marche'] ?: Prado::localize('TEXT_NON_GENERE');
                $arrayResult[$id]['statutEj'] = $row['statutEJ'] ?: Prado::localize('TEXT_SANS_OBJET');
                $arrayResult[$id]['entrepriseAttributaire'] = $row['nom_entreprise'];
                $arrayResult[$id]['lot'] = $row['numLot'];
                $arrayResult[$id]['descriptionTraduite'] = $row['description'];
                $arrayResult[$id]['libelleTypeDecision'] = Prado::localize($row['code_type_decision ']);
                $arrayResult[$id]['idTypeDecision'] = $row['id_type_decision'];
                $arrayResult[$id]['decisionAutre'] = $row['autre_a_preciser'];
                $arrayResult[$id]['dateDecisionAffiche'] = self::getDateDecisionAffiche('-', $row['date_decision']);
                $arrayResult[$id]['consultationId'] = $reference;
            }
        }

        return $arrayResult;
    }

    public function getDateDecisionAffiche($valeurRetourSiDateVide, $dateDecision)
    {
        if ($dateDecision && '0000-00-00' != $dateDecision) {
            return Atexo_Util::iso2frnDate($dateDecision);
        } else {
            return $valeurRetourSiDateVide;
        }
    }

    /**
     * ramène le tableau des echanges avec fichiers demultipliés.
     *
     * @param $criteriaVo: l'objet critère de recherche
     * @param $forNbrResult: à true, on retourne le nombre de resultat, sinon  le tableau des échanges
     * @param $forFso: le cas des messages de suivi
     *
     * @return le tableau des échanges demultipliés par le nombre de fichiers
     */
    public function remplirTableauEchanges($criteriaVo, $forNbrResult = false, $forFso = false)
    {
        $criteria = null;
        $queryParams = [];
        //$connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));
        $commandeName = $criteriaVo->getCommandeName();
        $sensTri = $criteriaVo->getSensTri();

        if ($criteriaVo) {
            $criteria = '';
            if ($criteriaVo->getRetourChorus() == Atexo_Config::getParameter('CHORUS_RETOUR_INTEGRE')) {
                $criteria .= " AND CE.retour_chorus = '".Atexo_Config::getParameter('CHORUS_RETOUR_INTEGRE')."'";
            } elseif ($criteriaVo->getRetourChorus() == Atexo_Config::getParameter('CHORUS_RETOUR_REJETE')) {
                $criteria .= " AND CE.retour_chorus = '".Atexo_Config::getParameter('CHORUS_RETOUR_REJETE')."'";
            } elseif ($criteriaVo->getRetourChorus() == Atexo_Config::getParameter('CHORUS_RETOUR_IRRECEVABLE')) {
                $criteria .= " AND CE.retour_chorus = '".Atexo_Config::getParameter('CHORUS_RETOUR_IRRECEVABLE')."'";
            }
            if ('' != $criteriaVo->getNumeroMarche()) {
                $criteria .= " AND CT.numero_contrat LIKE '%".$criteriaVo->getNumeroMarche()."%' ";
            }

            ///
            if ('' != $criteriaVo->getReferenceLibre()) {
                $criteria .= " AND CT.reference_libre LIKE '%".$criteriaVo->getReferenceLibre()."%' ";
            }
            if ($criteriaVo->getTypeContrat()) {
                $criteria .= ' AND CT.id_type_contrat = '.$criteriaVo->getTypeContrat();
            }
            if ('' != $criteriaVo->getCategorie() && '0' != $criteriaVo->getCategorie()) {
                $criteria .= ' AND CT.categorie = '.$criteriaVo->getCategorie();
            }

            if ('' != $criteriaVo->getIdsService()) {
                $criteria .= ' AND CT.service_id IN '.$criteriaVo->getIdsService().' ';
            } elseif ('' != $criteriaVo->getIdService()) {
                $criteria .= " AND CT.service_id = '".$criteriaVo->getIdService()."' ";
            }
            if ('' != $criteriaVo->getPeriodeEchangeStart()) {
                $criteria .= " AND date_ajout >= '".substr(Atexo_Util::frnDate2iso($criteriaVo->getPeriodeEchangeStart()), 0, 10)." 00:00:00'";
            }
            if ('' != $criteriaVo->getPeriodeEchangeEnd()) {
                $criteria .= " AND date_ajout <= '".substr(Atexo_Util::frnDate2iso($criteriaVo->getPeriodeEchangeEnd()), 0, 10)." 23:59:59'";
            }
            if ('' != $criteriaVo->getTypeFichierEchange()) {
                if ('FEN' == $criteriaVo->getTypeFichierEchange()) {
                    $criteria .= " AND CNF.type_fichier = 'FEN' ";
                } elseif ('CEN' == $criteriaVo->getTypeFichierEchange()) {
                    $criteria .= " AND CNF.type_fichier = 'CEN' ";
                } elseif ('FSO' == $criteriaVo->getTypeFichierEchange()) {
                    $criteria .= " AND CNF.type_fichier = 'FSO' ";
                }
            }
            if ('' != $criteriaVo->getRetourChorus() && 'INDIF' != $criteriaVo->getRetourChorus()) {
                if ('REJ' == $criteriaVo->getRetourChorus() || 'IRR' == $criteriaVo->getRetourChorus() || 'COM' == $criteriaVo->getRetourChorus()) {
                    $criteria .= " AND CNF.type_fichier = 'CEN' ";
                }
            }
            if ('' != $criteriaVo->getStatutEj() && 'INDIF' != $criteriaVo->getStatutEj() && 'SANS_OBJET' != $criteriaVo->getStatutEj()) {
                $criteria .= " AND CNF.	type_fichier = 'FSO' ";
            }
            //echo $criteria;exit;
        }

        $querySql = '';
        if ($forNbrResult) {
            $querySql .= ' SELECT count(*) as sumNbFichiers ';
        } else {
            $querySql .= <<<EOQUERY1
SELECT CNF.*, CT.numero_contrat as numero_marche, CT.num_long_OEAP, CT.num_EJ, CT.statutEJ, CT.reference_libre, CT.service_id, CT.id_contrat_titulaire
EOQUERY1;
            if (!$forFso) {
                $querySql .= <<<EOQUERY2
, CE.`id`, CE.`retour_chorus` as statut_chorus, CE.`statutEchange`, CE.`nom_createur`, CE.`prenom_createur`, CE.`num_ordre`, CE.`retour_chorus` as statut_chorus, CE.`date_envoi`
EOQUERY2;
            }
        }
        $querySql .= ' FROM `chorus_noms_fichiers` CNF, `t_contrat_titulaire` CT ';
        if (!$forFso) {
            $querySql .= ' , `Chorus_echange` CE ';
        }
        if (!$forFso) {
            $querySql .= <<<EOQUERY3
WHERE CNF.`id_echange` = CE.`id`
AND CNF.`acronyme_organisme` = CE.`organisme`
AND CE.`id_decision` = CT.`id_contrat_titulaire`
AND CE.`organisme` = CT.`organisme`
AND CE.statutEchange = :statutEchange
AND CNF.acronyme_organisme = :organisme
AND CNF.type_fichier != 'FSO'
EOQUERY3;
        } else {
            $querySql .= <<<EOQUERY4
WHERE CNF.`id_echange` = CT.`id_contrat_titulaire`
AND CNF.`acronyme_organisme` = CT.`organisme`
AND CNF.acronyme_organisme = :organisme
AND CNF.type_fichier = 'FSO'
EOQUERY4;
        }

        $querySql .= " AND CNF.type_fichier != 'CIR' AND  CNF.type_fichier != 'FIR' ";
        $querySql .= ' AND CT.organisme = :organisme ';
        $querySql .= " $criteria ";
        $queryParams[':statutEchange'] = Atexo_Config::getParameter('STATUS_ECHANGE_CHORUS_ENVOYER');
        $queryParams[':organisme'] = $criteriaVo->getOrganisme();

        if (!$forNbrResult && '' != $commandeName && 'num_flux' != $commandeName && 'date_heure' != $commandeName
           && 'num_marche' != $commandeName && 'statut_ej' != $commandeName && 'retour_chorus' != $commandeName) {
            if ('num_echange' == $commandeName) {
                $querySql .= " ORDER BY id $sensTri ";
            } elseif ('ref_libre' == $commandeName) {
                $querySql .= " ORDER BY reference_libre $sensTri ";
            } elseif ('agent_emetteur' == $commandeName) {
                $querySql .= " ORDER BY nom_agent $sensTri ";
            } elseif ('entite_achat' == $commandeName) {
                $querySql .= " ORDER BY service_id $sensTri ";
            } elseif ('num_decision' == $commandeName) {
                $querySql .= " ORDER BY id_contrat_titulaire $sensTri ";
            }
        }

        if (!$forNbrResult && ($criteriaVo->getOffset() || $criteriaVo->getLimit())) {
            $querySql .= ' limit '.$criteriaVo->getOffset().','.($criteriaVo->getOffset() + $criteriaVo->getLimit());
        }
        /*
        if($forFso) {

            try {
                foreach($queryParams as $a => $b) {
                    if($querySql) {
                        $finalQuery = str_replace($a."", "'".$b."' ", $querySql);
                    }

                }
                echo $finalQuery;exit;
            } catch(Exception $e ) {

            }
        }
*/
        $statement = Atexo_Db::getLinkCommon(true)->prepare($querySql);
        try {
            $statement->execute($queryParams);
        } catch (Exception $e) {
            //print_r($statement);exit;
            Prado::log($e->getMessage(), TLogger::INFO, __METHOD__);
        }

        $arrayResult = [];
        $arrayNumOrdre = [];
        $nombreResultat = 1;
        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            ++$arrayNumOrdre[$row['id_contrat_titulaire']]['num_ordre'];

            if ($forNbrResult) {
                return $row['sumNbFichiers'];
            } else {
                if (!$forFso) {
                    $arrayResult[$nombreResultat]['id'] = $row['id'];
                } else {
                    $arrayResult[$nombreResultat]['id'] = $row['id_contrat_titulaire'];
                }
                $arrayResult[$nombreResultat]['nom_fichier'] = $row['nom_fichier'];
                $arrayResult[$nombreResultat]['type_fichier'] = $row['type_fichier'];
                $arrayResult[$nombreResultat]['date_heure'] = $row['date_ajout'];
                $arrayResult[$nombreResultat]['retour_chorus'] = ('CEN' == $row['type_fichier'] || 'FSO' == $row['type_fichier']) ? self::getRetourChorus($row['statut_chorus']) : '<center> - </center>';
                $numeroMarche = $row['numero_marche'];

                if ($forFso) {
                    $arrayInfosEj = explode(';', self::getInfosEjByNumOrdre($row['statutEJ'], $arrayNumOrdre[$row['id_contrat_titulaire']]['num_ordre']));
                    $statutEj = $arrayInfosEj[1];
                }
                $arrayResult[$nombreResultat]['numero_marche'] = $numeroMarche;
                $arrayResult[$nombreResultat]['statut_ej'] = $statutEj;
                $arrayResult[$nombreResultat]['numero_marche'] = $numeroMarche;
                //$arrayResult[$nombreResultat]['statut_ej'] = $statutEj;
                $arrayResult[$nombreResultat]['reference_libre'] = $row['reference_libre'];
                //$arrayResult[$nombreResultat]['ref_consultation_technique'] = $row['reference'];
                $arrayResult[$nombreResultat]['nom_prenom_agent'] = ($row['nom_createur'] || $row['prenom_createur']) ?
                       ($row['nom_createur'].' '.$row['prenom_createur']) : ' - ';
                $arrayResult[$nombreResultat]['entite_achat'] = (new Atexo_EntityPurchase())->getEntityPathById($row['service_id'], $criteriaVo->getOrganisme(), true, true);
                $arrayResult[$nombreResultat]['id_contrat_titulaire'] = $row['id_contrat_titulaire'];
                $arrayResult[$nombreResultat]['organisme'] = $row['acronyme_organisme'];
                ++$nombreResultat;
            }
        }
        if ($forNbrResult) {
            return $nombreResultat;
        }

        if ('' != $commandeName && ('num_flux' == $commandeName || 'date_heure' == $commandeName
            || 'num_marche' == $commandeName || 'statut_ej' == $commandeName || 'retour_chorus' == $commandeName) && $arrayResult) {
            $champ = '';
            if ('num_flux' == $commandeName) {
                $champ = 'nom_fichier';
            } elseif ('date_heure' == $commandeName) {
                $champ = 'date_heure';
            } elseif ('num_marche' == $commandeName) {
                $champ = 'numero_marche';
            } elseif ('statut_ej' == $commandeName) {
                $champ = 'statut_ej';
            } elseif ('retour_chorus' == $commandeName) {
                $champ = 'retour_chorus';
            }
            $arrayResult = Atexo_Util::trierTableauxMultiDimensionnels($arrayResult, $champ, $sensTri, 'str');
        } elseif ('' == $commandeName && $arrayResult) {
            $arrayResult = Atexo_Util::trierTableauxMultiDimensionnels($arrayResult, 'date_heure', 'DESC', 'str');
        }

        return $arrayResult;
    }

    /**
     * ramène le retour Chorus pour les échanges ayant les types de fichiers CEN.
     *
     * @param $statut: le champ 'statutEchange' de la table 'Chorus_echange'
     * @param $numOrdre: le champ 'num_ordre' de la table 'Chorus_echange'
     *
     * @return le retour chorus de l'échange
     */
    public function getRetourChorus($statut)
    {
        if ($statut == Atexo_Config::getParameter('CHORUS_RETOUR_SANS_OBJET')) {
            return Prado::localize('TEXT_SANS_OBJET');
        } elseif ($statut == Atexo_Config::getParameter('CHORUS_RETOUR_INTEGRE')) {
            return Prado::localize('TEXT_INTEGRE');
        } elseif ($statut == Atexo_Config::getParameter('CHORUS_RETOUR_REJETE')) {
            return Prado::localize('TEXT_REJETE');
        } elseif ($statut == Atexo_Config::getParameter('CHORUS_RETOUR_IRRECEVABLE')) {
            return Prado::localize('TEXT_IRRECEVABLE');
        }
    }

    /**
     * Permet de remplir le tableau des informations chorus.
     *
     * @param CommonTContratTitulaire $contrat : objet contrat
     *
     * @return array : tableau des resultats
     *
     * @author OKO <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function remplirTableauInformationsChorus(CommonTContratTitulaire $contrat)
    {
        $queryParams = [];
        $req = ' SELECT CT.num_EJ as numero_marche, CT.statutEJ as statutEJ, CNF.* ';
        $req .= ' FROM  t_contrat_titulaire CT, chorus_noms_fichiers CNF ';
        $req .= ' WHERE CT.id_contrat_titulaire = CNF.id_echange AND CT.organisme = CNF.acronyme_organisme ';
        $req .= ' AND CT.organisme = :organisme ';
        $req .= ' AND CT.id_contrat_titulaire = :idContrat ';
        $req .= " AND CNF.type_fichier = 'FSO' ";
        $req .= ' ORDER BY date_ajout ASC ';

        $queryParams[':idContrat'] = $contrat->getIdContratTitulaire();
        $queryParams[':organisme'] = $contrat->getOrganisme();

        $statement = Atexo_Db::getLinkCommon(true)->prepare($req);
        try {
            $statement->execute($queryParams);
        } catch (Exception $e) {
            Prado::log($e->getMessage(), TLogger::INFO, __METHOD__);
            $logger = Atexo_LoggerManager::getLogger('app');
            $logger->error("Erreur lors de l'execution de la requete : ".$e->getMessage());
        }

        try {
            $arrayResult = [];
            $nombreResultat = 1;
            while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                $arrayResult[$nombreResultat]['nom_fichier'] = $row['nom_fichier'];
                $arrayResult[$nombreResultat]['date_heure'] = $row['date_ajout'];
                $arrayResult[$nombreResultat]['organisme'] = $row['acronyme_organisme'];
                $numeroOrdre = $row['numero_ordre'];
                if (empty($numeroOrdre)) {
                    $numeroOrdre = $nombreResultat;
                }
                $arrayInfosEj = explode(';', self::getInfosEjByNumOrdre($row['statutEJ'], $numeroOrdre));
                $arrayResult[$nombreResultat]['numero_marche'] = $arrayInfosEj[0];
                $arrayResult[$nombreResultat]['statut_ej'] = $arrayInfosEj[1];
                ++$nombreResultat;
            }
        } catch (Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('app');
            $logger->error('Erreur lors du remplissage du tableau des informations CHORUS : '.$e->getMessage());
        }

        return $arrayResult;
    }

    public function remplirTableauInformationsChorusExec($contratExec)
    {
        $queryParams = [];
        $req = ' SELECT DISTINCT cnf.* ';
        $req .= ' FROM chorus_echange ce, chorus_noms_fichiers cnf ';
        $req .= ' WHERE ce.id_decision = cnf.id_echange AND ce.organisme = cnf.acronyme_organisme ';
        $req .= ' AND ce.organisme = :organisme ';
        $req .= ' AND ce.uuid_externe_exec = :uuidContrat ';
        $req .= " AND cnf.type_fichier = 'FSO' ";
        $req .= ' ORDER BY date_ajout ASC ';

        $queryParams[':uuidContrat'] = $contratExec['uuid'];
        $queryParams[':organisme'] = $contratExec['organisme'];

        $statement = Atexo_Db::getLinkCommon(true)->prepare($req);

        try {
            $statement->execute($queryParams);
        } catch (Exception $e) {
            Prado::log($e->getMessage(), TLogger::INFO, __METHOD__);
            $logger = Atexo_LoggerManager::getLogger('app');
            $logger->error("Erreur lors de l'execution de la requete : ".$e->getMessage());
        }

        try {
            $arrayResult = [];
            $nombreResultat = 1;
            while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                $arrayResult[$nombreResultat]['nom_fichier'] = $row['nom_fichier'];
                $arrayResult[$nombreResultat]['date_heure'] = $row['date_ajout'];
                $arrayResult[$nombreResultat]['organisme'] = $row['acronyme_organisme'];
                $numeroOrdre = $row['numero_ordre'];
                if (empty($numeroOrdre)) {
                    $numeroOrdre = $nombreResultat;
                }
                $arrayInfosEj = explode(';', self::getInfosEjByNumOrdre($contratExec['statutEJ'], $numeroOrdre));
                $arrayResult[$nombreResultat]['numero_marche'] = $arrayInfosEj[0];
                $arrayResult[$nombreResultat]['statut_ej'] = $arrayInfosEj[1];
                ++$nombreResultat;
            }
        } catch (Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('app');
            $logger->error('Erreur lors du remplissage du tableau des informations CHORUS : '.$e->getMessage());
        }

        return $arrayResult;
    }

    /**
     * Permet de recuperer les infos de l'EJ correspondant a un numero d'ordre
     * Retourne un tableau sous la forme de chaine de caracteres "numeroEj;statutEj"
     * Si le parametre $numOrdre n'est pas renseigne, la requete retourne tous les resultats.
     *
     * @param string $statutEJ  : les statuts EJ
     * @param string $numOrdre: numero d'ordre
     *
     * @return string
     *
     * @author OKO <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function getInfosEjByNumOrdre($statutEJ, $numOrdre = null)
    {
        try {
            $arrayInfosEj = explode('#', $statutEJ);
            //Exemple
            //1_123468910;Commandé
            //2_1234561;Supprimé
            //3_1234562;Commandé
            $resultat = [];
            if (is_array($arrayInfosEj) && count($arrayInfosEj)) {
                foreach ($arrayInfosEj as $infoEj) {
                    $arrayInfosMarches = explode('_', $infoEj);
                    //Exemple
                    //1 => 123468910;Commandé
                    //2 => 1234561;Supprimé
                    //3 => 1234562;Commandé
                    $resultat[$arrayInfosMarches[0]] = $arrayInfosMarches[1];
                }
            }
            if (null == $numOrdre) {
                return $resultat;
            }

            return $resultat[$numOrdre];
        } catch (\Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('app');
            $logger->error("Erreur lors de la recuperation des infos de l'EJ par son numero d'ordre : ".$e->getMessage());
        }
    }

    /**
     * Permet de logger les infos.
     *
     * @param string $infos
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function loggerInfos($infos)
    {
        if (!is_dir(Atexo_Config::getParameter('LOG_DIR_PATH_INTERFACE_CHORUS'))) {
            mkdir(Atexo_Config::getParameter('LOG_DIR_PATH_INTERFACE_CHORUS'));
        }
        $logger = Atexo_LoggerManager::getLogger('chorus');
        $logger->info($infos);
    }

    /**
     * Permet de logger les erreurs.
     *
     * @param string $erreur
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function loggerErreur($erreur)
    {
        if (!is_dir(Atexo_Config::getParameter('LOG_DIR_PATH_INTERFACE_CHORUS'))) {
            mkdir(Atexo_Config::getParameter('LOG_DIR_PATH_INTERFACE_CHORUS'));
        }
        $logger = Atexo_LoggerManager::getLogger('chorus');
        $logger->error($erreur);
    }

    /**
     * Permet de logger les infos du flux Chorus a la reception.
     *
     * @param string $infos
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function loggerInfosReception($infos)
    {
        if (!is_dir(Atexo_Config::getParameter('LOG_DIR_PATH_INTERFACE_CHORUS'))) {
            mkdir(Atexo_Config::getParameter('LOG_DIR_PATH_INTERFACE_CHORUS'));
        }
        $logger = Atexo_LoggerManager::getLogger('chorus');
        $logger->info($infos);
        echo $infos.PHP_EOL;
    }

    /**
     * Permet de logger les erreurs du flux Chorus a la reception.
     *
     * @param string $erreur
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function loggerErreurReception($erreur)
    {
        if (!is_dir(Atexo_Config::getParameter('LOG_DIR_PATH_INTERFACE_CHORUS'))) {
            mkdir(Atexo_Config::getParameter('LOG_DIR_PATH_INTERFACE_CHORUS'));
        }
        $logger = Atexo_LoggerManager::getLogger('chorus');
        $logger->error($erreur);
        echo $erreur.PHP_EOL;
    }

    /**
     * Permet de recuperer la liste des numeros de marche a partir du champ 'numEJ' de la table 't_contrat_titulaire'.
     *
     * @param string $numEj : numero de l'engagement juridique
     *
     * @return array : tableau contenant la liste des numero de marche
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function getListeNumMarcheByNumEj($numEj)
    {
        try {
            $arrayResult = [];
            $tableauNumMarche = explode('#', $numEj);
            if (is_array($tableauNumMarche) && count($tableauNumMarche)) {
                foreach ($tableauNumMarche as $numMarcheNumOrdre) {
                    if (!empty($numMarcheNumOrdre)) {
                        $arrayNumMarcheNumOrdre = explode('_', $numMarcheNumOrdre);
                        if (is_array($arrayNumMarcheNumOrdre) && count($arrayNumMarcheNumOrdre)) {
                            if (!empty($arrayNumMarcheNumOrdre[1])) {
                                $arrayResult[] = $arrayNumMarcheNumOrdre[1];
                            }
                        }
                    }
                }
            }

            return $arrayResult;
        } catch (\Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('app');
            $logger->error('Erreur lors de la recuperation de la liste des numeros de marche : '.$e->getMessage());
        }
    }

    /**
     * Permet de contruire le prefixe du nom de fichier FEN.
     *
     * @param bool $isAcSad : precise si le contrat est accord cadre/Sad ou pas
     *
     * @return string prefixe
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function getFilePrefix($isAcSad)
    {
        try {
            return 'FEN'.self::getCodeInterface($isAcSad).'A_';
        } catch (\Exception $e) {
            (new Atexo_Chorus_Util())->loggerErreur('Erreur contruction du prefixe du nom du fichier : '.$e->getMessage());
        }
    }

    /**
     * Permet de contruire le code interface.
     *
     * @param bool $isAcSad : precise si le contrat est accord cadre/Sad ou pas
     *
     * @return string code interface
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function getCodeInterface($isAcSad)
    {
        try {
            if ($isAcSad) {
                return '0211';
            }

            return '0111';
        } catch (\Exception $e) {
            (new Atexo_Chorus_Util())->loggerErreur('Erreur contruction du code interface dans le nom du fichier : '.$e->getMessage());
        }
    }

    /**
     * Permet de preciser si le type d'envoi est une fiche modificative.
     *
     * @param CommonTContratTitulaire $contrat : objet contrat
     * @param CommonChorusEchange     $echange : objet Chorus echange
     *
     * @return bool
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     * @copyright Atexo 2015
     */
    public function isTypeEnvoiFicheModificative($contrat, $echange, $logger = null, &$erreursFluxChorus = null)
    {
        if (empty($logger)) {
            $logger = Atexo_LoggerManager::getLogger('chorus');
        }
        try {
            if ($contrat instanceof CommonTContratTitulaire && $echange instanceof CommonChorusEchange) {
                $isTypeEnvoiFicheModificative = $contrat->isContratNotifieEtEjCommande() && $echange->getTypeEnvoi() == Atexo_Config::getParameter('TYPE_ENVOI_CHORUS_ACTE_MODIF');
                if ($isTypeEnvoiFicheModificative) {
                    $logger->info("Le type d'envoi est un acte modificatif [Id_echange = ".$echange->getId().' , Organisme = '.$echange->getOrganisme().', id_contrat = '.$contrat->getIdContratTitulaire().']');

                    return true;
                }

                return false;
            } elseif (is_array($contrat) && $echange instanceof CommonChorusEchange) {
                $isActeModificatif = $echange->getTypeEnvoi() == Atexo_Config::getParameter('TYPE_ENVOI_CHORUS_ACTE_MODIF');
                $statutContratAccepte = ['notifie', 'encours', 'clos'];
                $isStatutContratOK = in_array(strtolower($contrat['statut']), $statutContratAccepte);
                $isStatutEJCommande = strstr($contrat['statutEJ'], Atexo_Config::getParameter('CHORUS_STATUT_EJ_COMMANDE'));
                if ($isActeModificatif && $isStatutContratOK && $isStatutEJCommande) {
                    $logger->info(
                        'Le type d\'envoi est un acte modificatif [Id_echange = ' . $echange->getId() . ' , Organisme = '
                        . $echange->getOrganisme() . ', uuid contrat = ' . $contrat['uuid'] . ']'
                    );
                    return true;
                }
            }

            return false;
        } catch (\Exception $e) {
            $erreurMessage = "Erreur verification du type d'envoi 'fiche modificative' : ".$e->getMessage()." \n\nTrace : ".$e->getTraceAsString()." \n\nMethode = Atexo_Chorus_Util::isTypeEnvoiFicheModificative";
            $logger->error($erreurMessage);
            $erreursFluxChorus[] = "Erreur verification du type d'envoi 'fiche modificative'".PHP_EOL.$erreurMessage;
        }
    }

    /**
     * Permet de preciser si le type d'envoi est un envoi de pieces jointes.
     *
     * @param CommonTContratTitulaire $contrat : objet contrat
     * @param CommonChorusEchange     $echange : objet Chorus echange
     *
     * @return bool
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     * @copyright Atexo 2015
     */
    public function isTypeEnvoiPiecesJointes($contrat, $echange)
    {
        try {
            if ($contrat instanceof CommonTContratTitulaire && $echange instanceof CommonChorusEchange) {
                return $contrat->isContratNotifieEtEjCommande() && $echange->getTypeEnvoi() == Atexo_Config::getParameter('TYPE_ENVOI_CHORUS_PJ');
            }

            return false;
        } catch (\Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('chorus');
            $logger->error('Erreur : '.$e->getMessage()." \n\nTrace : ".$e->getTraceAsString()." \n\nMethode = Atexo_Chorus_Util::isTypeEnvoiPiecesJointes");
        }
    }

    /**
     * Permet de preciser si le type d'envoi est un envoi avec contrat non notifie et EJ au statut "Commande".
     *
     * @param CommonTContratTitulaire $contrat : objet contrat
     *
     * @return bool
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     * @copyright Atexo 2015
     */
    public function isTypeEnvoiContratANotifieEtEjCommande($contrat)
    {
        try {
            if ($contrat instanceof CommonTContratTitulaire) {
                return $contrat->isContratANotifieEtEjCommande();
            } elseif (!empty($contrat) && is_array($contrat) && Atexo_Config::getParameter('ACTIVE_EXEC_V2')) {
                return strstr($contrat['statutEJ'],Atexo_Config::getParameter('CHORUS_STATUT_EJ_COMMANDE'))
                    && $contrat['statut'] == Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT_EFFECTUEE');
            }

            return false;
        } catch (\Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('chorus');
            $logger->error('Erreur : '.$e->getMessage()." \n\nTrace : ".$e->getTraceAsString()." \n\nMethode = Atexo_Chorus_Util::isTypeEnvoiContratANotifieEtEjCommande");
        }
    }

    /**
     * Permet de recuperer la fiche modificative pour un echange chorus.
     *
     * @param string $idEchange : identifiant de l'echange CHORUS
     *
     * @return CommonTChorusFicheModificative
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function recupererFicheModificative($idEchange)
    {
        try {
            return CommonTChorusFicheModificativeQuery::create()->filterByIdEchange($idEchange)->findOne();
        } catch (\Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('chorus');
            $logger->error("Erreur recuperation fiche modificative : ID Echange = $idEchange \n\nErreur = ".$e->getMessage()." \n\nTrace : ".$e->getTraceAsString()." \n\nMethode = Atexo_Chorus_Util::recupererFicheModificative");
        }
    }

    /**
     * Permet de construire le nom du PDF d'acte modificatif.
     *
     * @param CommonChorusEchange     $echange       : echange Chorus
     * @param CommonTContratTitulaire $contrat
     * @param bool                    $sansExtention
     *
     * @return string
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function getNomPdfActeModificatif($echange = false, $contrat = null, $sansExtention = false)
    {
        $numContrat = null;
        try {
            if ($contrat instanceof CommonTContratTitulaire) {
                $numContrat = $contrat->getNumeroContrat();
            } elseif ($echange instanceof CommonChorusEchange) {
                $contrat = $echange->getContrat();
                if ($contrat instanceof CommonTContratTitulaire) {
                    $numContrat = $contrat->getNumeroContrat();
                }
            }
            if ($numContrat) {
                $nomFichier = 'Fiche_modificative_'.date('Ymd').'_'.date('Hi').'_'.(new Atexo_Chorus_Util())->retournLastNumeroMarcheDecision($numContrat);
                if (!$sansExtention) {
                    $nomFichier = $nomFichier.'.pdf';
                }

                return $nomFichier;
            }

            return '';
        } catch (\Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('chorus');
            $logger->error('Erreur = '.$e->getMessage()." \n\nTrace : ".$e->getTraceAsString()." \n\nMethode = Atexo_Chorus_Util::getNomPdfActeModificatif");
        }
    }

    /**
     * Permet de construire une chaine de caracteres d'informations pour les extensions des pieces jointes autorisees pour l'envoi a Chorus.
     *
     * @return mixed
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function getInfosExtensionsAutoriseesEnvoiChorus()
    {
        return str_replace('[__EXTENSION_FILES__]', str_replace(' #', ', ', Atexo_Config::getParameter('EXTENTION_PIECES_JOINTES_CHORUS')), Prado::localize('TEXT_INFO_BULLES_EXTENSIONS_VALIDES_PJ_CHORUS'));
    }

    /**
     * Permet de logger les infos generation flux CHORUS (routine Cli_GenerationFluxChorus).
     *
     * @param string $infos
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function loggerInfosGenerationFlux($infos)
    {
        if (!is_dir(Atexo_Config::getParameter('LOG_DIR_PATH_INTERFACE_CHORUS'))) {
            mkdir(Atexo_Config::getParameter('LOG_DIR_PATH_INTERFACE_CHORUS'));
        }
        $logger = Atexo_LoggerManager::getLogger('chorus');
        $logger->info($infos);
    }

    /**
     * Permet de logger les erreurs generation flux CHORUS (routine Cli_GenerationFluxChorus).
     *
     * @param string $erreur
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function loggerErreursGenerationFlux($erreur)
    {
        if (!is_dir(Atexo_Config::getParameter('LOG_DIR_PATH_INTERFACE_CHORUS'))) {
            mkdir(Atexo_Config::getParameter('LOG_DIR_PATH_INTERFACE_CHORUS'));
        }
        $logger = Atexo_LoggerManager::getLogger('chorus');
        $logger->error($erreur);
    }

    /**
     * Permet de logger les infos d'envoi HTTP des flux CHORUS (routine Cli_EnvoyerFluxChorus).
     *
     * @param string $infos
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function loggerInfosEnvoiHttpFlux($infos)
    {
        if (!is_dir(Atexo_Config::getParameter('LOG_DIR_PATH_INTERFACE_CHORUS'))) {
            mkdir(Atexo_Config::getParameter('LOG_DIR_PATH_INTERFACE_CHORUS'));
        }
        $logger = Atexo_LoggerManager::getLogger('chorus');
        $logger->info($infos);
    }

    /**
     * Permet de logger les erreurs d'envoi HTTP des flux CHORUS (routine Cli_EnvoyerFluxChorus).
     *
     * @param string $erreur
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function loggerErreursEnvoiHttpFlux($erreur)
    {
        if (!is_dir(Atexo_Config::getParameter('LOG_DIR_PATH_INTERFACE_CHORUS'))) {
            mkdir(Atexo_Config::getParameter('LOG_DIR_PATH_INTERFACE_CHORUS'));
        }
        $logger = Atexo_LoggerManager::getLogger('chorus');
        $logger->error($erreur);
    }

    /**
     * Permet de logger les erreurs de tentative de regeneration du flux CHORUS.
     *
     * @param string $erreur
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function loggerErreursEnvoiTentativeRegenerationFlux($erreur)
    {
        if (!is_dir(Atexo_Config::getParameter('LOG_DIR_PATH_INTERFACE_CHORUS'))) {
            mkdir(Atexo_Config::getParameter('LOG_DIR_PATH_INTERFACE_CHORUS'));
        }
        $logger = Atexo_LoggerManager::getLogger('chorus');
        $logger->error($erreur);
    }

    /**
     * Permet de logger les $infos de tentative de regeneration du flux CHORUS.
     *
     * @param string $erreur
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function loggerInfosEnvoiTentativeRegenerationFlux($infos)
    {
        if (!is_dir(Atexo_Config::getParameter('LOG_DIR_PATH_INTERFACE_CHORUS'))) {
            mkdir(Atexo_Config::getParameter('LOG_DIR_PATH_INTERFACE_CHORUS'));
        }
        $logger = Atexo_LoggerManager::getLogger('chorus');
        $logger->info($infos);
    }

    /**
     * Permet d'envoyer un mail au support Chorus.
     *
     * @param string $objet  : objet du mail
     * @param string $erreur : corps du mail
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function envoyerMailSupportChorus($objet, $erreur)
    {
        $message = Atexo_Message::getGenericEmailFormat($erreur);
        Atexo_Message::simpleMail(Atexo_Config::getParameter('PF_MAIL_FROM'), Atexo_Config::getParameter('MAIL_SUPPORT_CHORUS'), $objet, $message, '', '', false, true);
    }

    /**
     * Permet de logger les erreurs de la routine de generation et d'envoi (Cli_SendChorusFluxV2) des flux vers CHORUS.
     *
     * @param string $erreur
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function loggerErreursGenerationEnvoiFlux($erreur)
    {
        if (!is_dir(Atexo_Config::getParameter('LOG_DIR_PATH_INTERFACE_CHORUS'))) {
            mkdir(Atexo_Config::getParameter('LOG_DIR_PATH_INTERFACE_CHORUS'));
        }
        $logger = Atexo_LoggerManager::getLogger('chorus');
        $logger->error($erreur);
    }

    /**
     * Permet de transformer les noms des fichiers en de UT8 àSCII avant transmission àhorus.
     *
     * @param string $nomFichier
     *
     * @author Mohamed Wazni <mohamed.wazni@atexo.com>
     *
     * @version 1.0
     *
     * @since 2018-place
     *
     * @copyright Atexo 2018
     */
    public function convertToAscii($nomFichier)
    {
        $fileName = $nomFichier;
        if (function_exists('iconv')) {
            $fileName = iconv('UTF-8', 'ASCII//TRANSLIT//IGNORE', "$nomFichier");

            if (!$fileName) {
                $fileName = iconv('CP850', 'ASCII//TRANSLIT//IGNORE', $nomFichier);
            }
        }

        return $fileName;
    }

    /**
     * Permet de logger les infos de la routine de generation et d'envoi (Cli_SendChorusFluxV2) des flux vers CHORUS.
     *
     * @param string $erreur
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function loggerInfosGenerationEnvoiFlux($infos)
    {
        if (!is_dir(Atexo_Config::getParameter('LOG_DIR_PATH_INTERFACE_CHORUS'))) {
            mkdir(Atexo_Config::getParameter('LOG_DIR_PATH_INTERFACE_CHORUS'));
        }
        $logger = Atexo_LoggerManager::getLogger('chorus');
        $logger->info($infos);
    }
}
