<?php

namespace Application\Service\Atexo;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonContactEntreprisePeer;

/**
 * Classe de manipulation des entreprises.
 *
 * @author Khadija CHOUIKA <khadija.chouika@atexo.com>
 * @copyright Atexo 2012
 *
 * @version 1.2
 *
 * @since MPE-4.0
 */
class Atexo_ContactEntreprise
{
    /**
     * retourne un objet entreprise.
     *
     * @param  $siren de l'entrepirse
     */
    public function retrieveContactByIdEntreprise($id_entreprise)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonContactEntreprisePeer::ID_ENTREPRISE, $id_entreprise);
        $contactCompany = CommonContactEntreprisePeer::doSelectOne($c, $connexionCom);
        if ($contactCompany) {
            return $contactCompany;
        } else {
            return false;
        }
    }
}
