<?php

namespace Application\Service\Atexo;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonAgent;
use Application\Propel\Mpe\CommonTThemeGraphiquePeer;

/**
 * commentaires.
 *
 * @author loubna EZZIANI <loubna.ezziani@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 1.0
 *
 * @since MPE-4.0
 */
class Atexo_ThemeGraphique
{
    /*
     * retourne tout les thémes
     * @param $actif : theme actif (optionnel)
     */
    public function getAllThemeGraphique($actif = null)
    {
        $c = new Criteria();
        if (null != $actif) {
            $c->add(CommonTThemeGraphiquePeer::ACTIF, $actif, Criteria::EQUAL);
        }
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $themeObject = CommonTThemeGraphiquePeer::doSelect($c, $connexionCom);

        return $themeObject;
    }

    /*
     * retourne tout le théme par code
     */
    public function getThemeGraphiqueByCode($code)
    {
        $c = new Criteria();
        $c->add(CommonTThemeGraphiquePeer::CODE, $code);
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $themeObject = CommonTThemeGraphiquePeer::doSelectOne($c, $connexionCom);

        return $themeObject ?: false;
    }

    public function setCookieThemeGraphiquePersonnel($agent)
    {
        if ($agent instanceof CommonAgent) {
            $sufix = '';
            $defaultThemeGraphiquePersonnel = '';
            if (Atexo_Config::getParameter('CONFIG_REDAC') && Atexo_CurrentUser::hasHabilitation('ModuleRedactionUniquement')) {
                $sufix = Atexo_Config::getParameter('SUFIX_THEMES_CONFIG_REDAC');
                $defaultThemeGraphiquePersonnel = Atexo_Config::getParameter('CODE_DEFAULT_THEME').$sufix;
            }
            if ('0' == $agent->getCodeTheme()) {
                setcookie('themeGraphiquePersonnel', $defaultThemeGraphiquePersonnel);
                $_COOKIE['themeGraphiquePersonnel'] = $defaultThemeGraphiquePersonnel;
            } else {
                setcookie('themeGraphiquePersonnel', $agent->getCodeTheme().$sufix);
                $_COOKIE['themeGraphiquePersonnel'] = $agent->getCodeTheme().$sufix;
            }
        }
    }
}
