<?php

namespace Application\Service\Atexo;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonDonneesAnnuellesConcessionQuery;
use Application\Propel\Mpe\CommonDonneesAnnuellesConcessionTarifQuery;
use Application\Propel\Mpe\CommonTContratTitulaireQuery;
use Application\Propel\Mpe\CommonTypeContratConcessionPivotQuery;
use Prado\Prado;

class Atexo_Contrat
{
    public function retrieveContratOneByIdContratTitulaire($idContrat, $connexion = null)
    {
        if (!$connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
        $commonTContratTitulaireQuery = new CommonTContratTitulaireQuery();
        $contratTitulaire = $commonTContratTitulaireQuery->findOneByIdContratTitulaire($idContrat, $connexion);

        return $contratTitulaire;
    }

    public function getDonneesAnnuellesConcessionContrat($idContrat, $limit, $offset, $connexion = null)
    {
        if (!$connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
        $commoDonneesAnnuellesConcessionQuery = new CommonDonneesAnnuellesConcessionQuery();

        return $commoDonneesAnnuellesConcessionQuery->getDonneesAnnuellesConcession($idContrat, $limit, $offset, $connexion);
    }

    public static function getListeNatureContratConcession()
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $allNatureContratProcedureConcessionPivot = [];
        $typeContratConcession = new CommonTypeContratConcessionPivotQuery();
        $rowTypesContratConcessionPivot = (array) $typeContratConcession->find($connexion);

        $allNatureContratProcedureConcessionPivot[0] = '---'.Prado::localize('DEFINE_NATURE_CONTRAT').'---';

        foreach ($rowTypesContratConcessionPivot as $oneTypeContratConcessionPivot) {
            $allNatureContratProcedureConcessionPivot[$oneTypeContratConcessionPivot->getId()] = $oneTypeContratConcessionPivot->getLibelle();
        }

        return $allNatureContratProcedureConcessionPivot;
    }

    public function getOneDonneesAnnuellesConcessionById($id, $connexion = null)
    {
        if (!$connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
        $commoDonneesAnnuellesConcessionQuery = new CommonDonneesAnnuellesConcessionQuery();

        return $commoDonneesAnnuellesConcessionQuery->getDonneesAnnuellesConcessionById($id, $connexion);
    }

    public function getLastIdTarif($idDonneesAnnuelles, $connexion = null)
    {
        if (!$connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
        $commoDonneesAnnuellesConcessionTarifQuery = new CommonDonneesAnnuellesConcessionTarifQuery();

        return $commoDonneesAnnuellesConcessionTarifQuery->getLastIdDonneesAnnuellesConcessionTarif($idDonneesAnnuelles, $connexion);
    }

    public function getLastNumordreDonneesAnnuelles($idContrat, $connexion = null)
    {
        if (!$connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
        $commoDonneesAnnuellesConcessionQuery = new CommonDonneesAnnuellesConcessionQuery();

        return $commoDonneesAnnuellesConcessionQuery->getLastNumOrdreDonneesAnnuellesConcession($idContrat, $connexion);
    }
}
