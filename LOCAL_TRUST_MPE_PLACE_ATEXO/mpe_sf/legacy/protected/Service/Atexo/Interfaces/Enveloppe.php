<?php

namespace Application\Service\Atexo\Interfaces;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonEnveloppe;
use Application\Propel\Mpe\CommonEnveloppePeer;
use Application\Propel\Mpe\CommonFichierEnveloppe;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Responses;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Interfaces_Enveloppe
{
    public function getEnveloppeVoByIdOffre($idOffre, $organisme)
    {
        $enveloppesVo = [];
        $enveloppes = (new Atexo_Consultation_Responses())->retrieveEnveloppesByIdOffre($idOffre, $organisme);
        if (is_array($enveloppes)) {
            foreach ($enveloppes as $enveloppe) {
                if ($enveloppe instanceof CommonEnveloppe) {
                    $nameFile = ' ';
                    $fichierEnveloppe = (new Atexo_Consultation_Responses())->retrieveFichierEnveloppeByIdEnveloppe($enveloppe->getIdEnveloppeElectro(), $organisme);
                    if (!$fichierEnveloppe instanceof CommonFichierEnveloppe) {
                        $fichierEnveloppe = (new Atexo_Consultation_Responses())->retrieveFichierEnveloppeByIdEnveloppe($enveloppe->getIdEnveloppeElectro(), $organisme, 'SEC');
                    }
                    if ($fichierEnveloppe instanceof CommonFichierEnveloppe) {
                        $nameFile = $fichierEnveloppe->getNomFichier();
                    }
                    $enveloppeVo = new Atexo_Interfaces_EnveloppeVo();
                    $enveloppeVo->setAttribue($enveloppe->getAttribue());
                    $enveloppeVo->setCryptage($enveloppe->getCryptage());
                    $enveloppeVo->setDateOuverture($enveloppe->getDateheureOuverture());
                    $enveloppeVo->setEstSousPli($enveloppe->getSousPli());
                    $enveloppeVo->setTypeEnveloppe($enveloppe->getTypeEnv());
                    $enveloppeVo->setStatutEnveloppe($enveloppe->getStatutEnveloppe());
                    $enveloppeVo->setNumeroSousPli($enveloppe->getSousPli());
                    $enveloppeVo->setNomFichier($nameFile);

                    $enveloppesVo[] = $enveloppeVo;
                }
            }
        }

        return $enveloppesVo;
    }

    public function getEnveloppesVoFromTagEnveloppe(\DOMNodeList $enveloppesTag)
    {
        $enveloppesVo = [];
        foreach ($enveloppesTag as $enveloppe) {
            $enveloppeVo = new Atexo_Interfaces_EnveloppeVo();

            $enveloppeVo->setAttribue($enveloppe->getAttribute('attribue'));
            $enveloppeVo->setCryptage($enveloppe->getAttribute('cryptage'));
            $enveloppeVo->setDateOuverture($enveloppe->getAttribute('dateOuverture'));
            $enveloppeVo->setEstSousPli($enveloppe->getAttribute('estSousPli'));
            $enveloppeVo->setTypeEnveloppe($enveloppe->getAttribute('typeEnveloppe'));
            $enveloppeVo->setStatutEnveloppe($enveloppe->getAttribute('statutEnveloppe'));
            $enveloppeVo->setNomFichier(utf8_decode($enveloppe->getAttribute('nomFichier')));

            $enveloppesVo[] = $enveloppeVo;
        }

        return $enveloppesVo;
    }

    public function UpdateStatutEnveloppe($idEnveloppe)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $organisme = Atexo_CurrentUser::getCurrentOrganism();
        $enveloppe = CommonEnveloppePeer::retrieveByPK($idEnveloppe, $organisme, $connexion);
        if ($enveloppe instanceof CommonEnveloppe) {
            $enveloppe->setStatutEnveloppe(Atexo_Config::getParameter('STATUT_ENV_OUVERTE_EN_LIGNE'));
            $enveloppe->save($connexion);
        }
    }
}
