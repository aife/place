<?php

namespace Application\Service\Atexo\Interfaces;

use Application\Service\Atexo\Atexo_Config;
use DOMDocument;

/**
 * commentaires.
 *
 * @author othmane abisourour <othmane.abisourour@atexo.com>
 * @copyright Atexo 2009
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Interfaces_GenerateXmlStats
{
    public function getDataFromXml($xml)
    {
        $temps_debut = microtime(true);
        $data = [];
        $xml_content = simplexml_load_string($xml);
        $data['login'] = (string) $xml_content->Control->From['login'];
        $data['pass'] = (string) $xml_content->Control->From['password'];
        $data['email'] = (string) $xml_content->Control->From['email'];
        $data['purposeCode'] = (string) $xml_content->Message['PurposeCode'];
        $data['messageAction'] = (string) $xml_content->Message['MessageAction'];

        $temps_fin = microtime(true);
        $txt = 'Debut getDataFromXml et fin : '.round($temps_fin - $temps_debut, 4)."\n\n";
        $fp = fopen(Atexo_Config::getParameter('COMMON_TMP').session_id().'Atexo_Interfaces_GenerateXmlStats_getDataFromXml_timer.txt', 'a');
        fwrite($fp, $txt);
        fclose($fp);

        return $data;
    }

    public function generateXmlAgentMPE(array $dataXml)
    {
        $temps_debut = microtime(true);
        $domDocument = new DOMDocument('1.0', 'UTF-8');

        //<Transaction>
        // |
        $transaction = $domDocument->createElement('Transaction', '');
        $domDocument->appendChild($transaction);
        // |
        // |___ Attributs Transaction :

        //         |____ attribut xmlns:xsi
        (new Atexo_Interfaces_GenerateXml())->addAttribute($domDocument, $transaction, 'xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
        //         |____ attribut xsi:noNamespaceSchemaLocation
        (new Atexo_Interfaces_GenerateXml())->addAttribute($domDocument, $transaction, 'xsi:noNamespaceSchemaLocation', 'creation-consultation.xsd');
        //         |____ attribut Version
        (new Atexo_Interfaces_GenerateXml())->addAttribute($domDocument, $transaction, 'Version', '1.0');
        //         |____ attribut TimeStamp
        (new Atexo_Interfaces_GenerateXml())->addAttribute($domDocument, $transaction, 'TimeStamp', date('c'));
        // |
        // |___ <Control> :
        $control = $domDocument->createElement('Control', '');
        $transaction->appendChild($control);
        //             |
        //             |___<From>
        $from = $domDocument->createElement('From', '');
        $control->appendChild($from);
        //             |
        //             |___ Attributs From :
        //                         |
        //                         |___login
        (new Atexo_Interfaces_GenerateXml())->addAttribute($domDocument, $from, 'login', $dataXml[0]);
        //                         |___password
        (new Atexo_Interfaces_GenerateXml())->addAttribute($domDocument, $from, 'password', $dataXml[1]);
        //                         |___email
        (new Atexo_Interfaces_GenerateXml())->addAttribute($domDocument, $from, 'email', $dataXml[2]);
        // |
        // |___ <Message> :
        $message = $domDocument->createElement('Message', '');
        $transaction->appendChild($message);
        //             |
        //             |___ Attributs Message :
        //                         |
        //                         |___PurposeCode
        (new Atexo_Interfaces_GenerateXml())->addAttribute($domDocument, $message, 'PurposeCode', $dataXml[3]);
        //                         |___MessageAction
        (new Atexo_Interfaces_GenerateXml())->addAttribute($domDocument, $message, 'MessageAction', $dataXml[4]);
        // |
        // |___ <StatistiquesAgentMPE> :
        $statistiquesAgentMPE = $domDocument->createElement('StatistiquesAgentMPE', '');
        $message->appendChild($statistiquesAgentMPE);
        // |
        // |___ <AnnoncesEnLigne> :
        $annoncesEnLigne = $domDocument->createElement('AnnoncesEnLigne', '');
        $statistiquesAgentMPE->appendChild($annoncesEnLigne);
        //             |
        //             |___ Attributs AnnoncesEnLigne :
        //                         |
        //                         |___NombreAnnoncesTotal
        (new Atexo_Interfaces_GenerateXml())->addAttribute($domDocument, $annoncesEnLigne, 'NombreAnnoncesTotal', $dataXml[5]);
        //                         |___NombreAnnoncesConsultation
        (new Atexo_Interfaces_GenerateXml())->addAttribute($domDocument, $annoncesEnLigne, 'NombreAnnoncesConsultation', $dataXml[6]);
        //                         |___NombreAnnoncesInformation
        (new Atexo_Interfaces_GenerateXml())->addAttribute($domDocument, $annoncesEnLigne, 'NombreAnnoncesInformation', $dataXml[7]);
        //                         |___NombreAnnoncesAttribution
        (new Atexo_Interfaces_GenerateXml())->addAttribute($domDocument, $annoncesEnLigne, 'NombreAnnoncesAttribution', $dataXml[8]);

        // |
        // |___ <AnnoncesAApprouver> :
        $annoncesAApprouver = $domDocument->createElement('AnnoncesAApprouver', '');
        $statistiquesAgentMPE->appendChild($annoncesAApprouver);
        //             |
        //             |___ Attributs AnnoncesAApprouver :
        //                         |
        //                         |___NombreAnnoncesAApprouver
        (new Atexo_Interfaces_GenerateXml())->addAttribute($domDocument, $annoncesAApprouver, 'NombreAnnoncesAApprouver', $dataXml[9]);

        // |
        // |___ <AnnoncesAValider> :
        $annoncesAValider = $domDocument->createElement('AnnoncesAValider', '');
        $statistiquesAgentMPE->appendChild($annoncesAValider);
        //             |
        //             |___ Attributs AnnoncesAValider :
        //                         |
        //                         |___NombreAnnoncesAValider
        (new Atexo_Interfaces_GenerateXml())->addAttribute($domDocument, $annoncesAValider, 'NombreAnnoncesAValider', $dataXml[10]);
        //                         |___NombreAnnoncesConsultation
        (new Atexo_Interfaces_GenerateXml())->addAttribute($domDocument, $annoncesAValider, 'NombreAnnoncesEnAttenteValidation', $dataXml[11]);

        // |
        // |___ <Commissions> :
        $commissions = $domDocument->createElement('Commissions', '');
        $statistiquesAgentMPE->appendChild($commissions);
        //             |
        //             |___ Attributs Commissions :
        //                         |
        //                         |___NombreCommissions
        (new Atexo_Interfaces_GenerateXml())->addAttribute($domDocument, $commissions, 'NombreCommissions', $dataXml[12]);

        // |
        // |___ <Questions> :
        $questions = $domDocument->createElement('Questions', '');
        $statistiquesAgentMPE->appendChild($questions);
        //             |
        //             |___ Attributs Commissions :
        //                         |
        //                         |___NombreQuestions
        (new Atexo_Interfaces_GenerateXml())->addAttribute($domDocument, $questions, 'NombreQuestions', $dataXml[13]);
        //                         |___NombreAnnoncesConcernees
        (new Atexo_Interfaces_GenerateXml())->addAttribute($domDocument, $questions, 'NombreAnnoncesConcernees', $dataXml[14]);

        // |
        // |___ <Reponses> :
        $reponses = $domDocument->createElement('Reponses', '');
        $statistiquesAgentMPE->appendChild($reponses);
        //             |
        //             |___ Attributs Reponses :
        //                         |
        //                         |___NombreReponsesADepouiller
        (new Atexo_Interfaces_GenerateXml())->addAttribute($domDocument, $reponses, 'NombreReponsesADepouiller', $dataXml[15]);
        //                         |___NombreAnnoncesConcernees
        (new Atexo_Interfaces_GenerateXml())->addAttribute($domDocument, $reponses, 'NombreAnnoncesConcernees', $dataXml[16]);
        //                         |___NombreDeMois
        (new Atexo_Interfaces_GenerateXml())->addAttribute($domDocument, $reponses, 'NombreDeMois', $dataXml[17]);
        $temps_fin = microtime(true);
        $txt = 'Debut generateXmlAgentMPE et fin : '.round($temps_fin - $temps_debut, 4)."\n\n";
        $fp = fopen(Atexo_Config::getParameter('COMMON_TMP').session_id().'Atexo_Interfaces_GenerateXmlStats_generateXmlAgentMPE_timer.txt', 'a');
        fwrite($fp, $txt);
        fclose($fp);

        return $domDocument->saveXML();
    }

    public function generateXmlPortail(array $dataXml)
    {
        $temps_debut = microtime(true);
        $domDocument = new DOMDocument('1.0', 'UTF-8');

        //<Transaction>
        // |
        $transaction = $domDocument->createElement('Transaction', '');
        $domDocument->appendChild($transaction);
        // |
        // |___ Attributs Transaction :

        //         |____ attribut xmlns:xsi
        (new Atexo_Interfaces_GenerateXml())->addAttribute($domDocument, $transaction, 'xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
        //         |____ attribut xsi:noNamespaceSchemaLocation
        (new Atexo_Interfaces_GenerateXml())->addAttribute($domDocument, $transaction, 'xsi:noNamespaceSchemaLocation', 'creation-consultation.xsd');
        //         |____ attribut Version
        (new Atexo_Interfaces_GenerateXml())->addAttribute($domDocument, $transaction, 'Version', '1.0');
        //         |____ attribut TimeStamp
        (new Atexo_Interfaces_GenerateXml())->addAttribute($domDocument, $transaction, 'TimeStamp', date('c'));
        // |
        // |___ <Control> :
        $control = $domDocument->createElement('Control', '');
        $transaction->appendChild($control);
        //             |
        //             |___<From>
        $from = $domDocument->createElement('From', '');
        $control->appendChild($from);
        //             |
        //             |___ Attributs From :
        //                         |
        //                         |___login
        (new Atexo_Interfaces_GenerateXml())->addAttribute($domDocument, $from, 'login', $dataXml[0]);
        //                         |___password
        (new Atexo_Interfaces_GenerateXml())->addAttribute($domDocument, $from, 'password', $dataXml[1]);
        //                         |___email
        (new Atexo_Interfaces_GenerateXml())->addAttribute($domDocument, $from, 'email', $dataXml[2]);
        // |
        // |___ <Message> :
        $message = $domDocument->createElement('Message', '');
        $transaction->appendChild($message);
        //             |
        //             |___ Attributs Message :
        //                         |
        //                         |___PurposeCode
        (new Atexo_Interfaces_GenerateXml())->addAttribute($domDocument, $message, 'PurposeCode', $dataXml[3]);
        //                         |___MessageAction
        (new Atexo_Interfaces_GenerateXml())->addAttribute($domDocument, $message, 'MessageAction', $dataXml[4]);

        // |
        // |___ <StatistiquesPortail> :
        $statistiquesPortail = $domDocument->createElement('StatistiquesPortail', '');
        $message->appendChild($statistiquesPortail);

        //         |
        //         |___ Attributs AnnoncesEnLigne :
        //             |
        //             |___DateDebut
        (new Atexo_Interfaces_GenerateXml())->addAttribute($domDocument, $statistiquesPortail, 'DateDebut', $dataXml[5]);
        //             |___NombreConsultationsEnLigne
        (new Atexo_Interfaces_GenerateXml())->addAttribute($domDocument, $statistiquesPortail, 'NombreConsultationsEnLigne', $dataXml[6]);
        //             |___NombreConsultationsPubliees
        (new Atexo_Interfaces_GenerateXml())->addAttribute($domDocument, $statistiquesPortail, 'NombreConsultationsPubliees', $dataXml[7]);
        //             |___NombreEntitesPubliques
        (new Atexo_Interfaces_GenerateXml())->addAttribute($domDocument, $statistiquesPortail, 'NombreEntitesPubliques', $dataXml[8]);
        //             |___NombreEntreprisesInscrites
        (new Atexo_Interfaces_GenerateXml())->addAttribute($domDocument, $statistiquesPortail, 'NombreEntreprisesInscrites', $dataXml[9]);
        //             |___NombreReponsesElectroniques
        (new Atexo_Interfaces_GenerateXml())->addAttribute($domDocument, $statistiquesPortail, 'NombreReponsesElectroniques', $dataXml[10]);
        //             |___NombreTelechargementsDCE
        (new Atexo_Interfaces_GenerateXml())->addAttribute($domDocument, $statistiquesPortail, 'NombreTelechargementsDCE', $dataXml[11]);
        $temps_fin = microtime(true);
        $txt = 'Debut generateXmlPortail et fin : '.round($temps_fin - $temps_debut, 4)."\n\n";
        $fp = fopen(Atexo_Config::getParameter('COMMON_TMP').session_id().'Atexo_Interfaces_GenerateXmlStats_generateXmlPortail_timer.txt', 'a');
        fwrite($fp, $txt);
        fclose($fp);

        return $domDocument->saveXML();
    }
}
