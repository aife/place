<?php

namespace Application\Service\Atexo\Interfaces;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonInscrit;
use Application\Propel\Mpe\CommonInscritPeer;
use Application\Propel\Mpe\CommonQuestionsDcePeer;
use Application\Propel\Mpe\Entreprise;
use Application\Propel\Mpe\EntreprisePeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;

/**
 * commentaires.
 *
 * @author Othmane ABISOUROUR <othmane.abisourour@atexo.com>
 * @copyright Atexo 2009
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Interfaces_Question
{
    public function getInfoQuestion($xml)
    {
        $consultation = new Atexo_Interfaces_Consultation();

        $consultationsVo = $consultation->createConsultationVoFromXml($xml);
        $consultationVo = array_shift($consultationsVo);
        if (!$consultationVo instanceof Atexo_Interfaces_ConsultationVo) {
            $consultationVo = new Atexo_Interfaces_ConsultationVo();
        }

        $consultationId = $consultation->getIdConsultationByRefCon($xml);

        //Question
        $questionsOrg = (new Atexo_Interfaces_Question())->getOrgQuestionByRefConsultation($consultationId, Atexo_CurrentUser::getCurrentOrganism());
        $interfacesVo = [];
        if (is_array($questionsOrg)) {
            foreach ($questionsOrg as $oneOrg) {
                $interfaceVo = new Atexo_Interfaces_QuestionVo();

                $interfaceVo->setDateQuestion($oneOrg->getDateDepot());
                $interfaceVo->setNomFichier(utf8_decode($oneOrg->getNomFichier()));
                $interfaceVo->setContenu(utf8_decode($oneOrg->getQuestion()));
                $interfaceVo->setDateReponse($oneOrg->getDateReponse());
                //Entreprise
                $entrepriseVo = (new Atexo_Interfaces_Entreprise())->getEntrepriseVoByQuestion($oneOrg);
                //Inscrit
                $inscritVo = (new Atexo_Interfaces_Inscrit())->getInscritVoByRegistre($oneOrg);
                $entrepriseVo->setInscrit($inscritVo);
                $interfaceVo->setEntrepriseVo($entrepriseVo);
                $interfacesVo[] = $interfaceVo;
            }
            $consultationVo->setQuestion($interfacesVo);
        }
        $consultation = [];
        $consultation[] = $consultationVo;

        return $consultation;
        //fin question
    }

    //Base Organisme
    public function getOrgQuestionByRefConsultation($consultationId, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));

        $c = new Criteria();
        $c->add(CommonQuestionsDcePeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonQuestionsDcePeer::ORGANISME, $organisme);
        $questionOrg = CommonQuestionsDcePeer::doSelect($c, $connexion);

        return $questionOrg;
    }

    public function retrieveEntrepriseById($idEntreprise)
    {
        if (0 == $idEntreprise) {
            return false;
        }

        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));

        $c = new Criteria();
        $c->add(EntreprisePeer::ID, $idEntreprise);
        $entreprise = EntreprisePeer::doSelectOne($c, $connexionCom);

        if ($entreprise instanceof Entreprise) {
            return $entreprise;
        }

        return false;
    }

    public function retrieveInscritByIdInscrit($idInscrit)
    {
        if (0 == $idInscrit) {
            return false;
        }

        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));

        $c = new Criteria();
        $c->add(CommonInscritPeer::ID, $idInscrit);
        $inscrit = CommonInscritPeer::doSelectOne($c, $connexionCom);

        if ($inscrit instanceof CommonInscrit) {
            return $inscrit;
        }

        return false;
    }
}
