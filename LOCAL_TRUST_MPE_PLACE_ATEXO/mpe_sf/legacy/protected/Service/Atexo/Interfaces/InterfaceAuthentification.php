<?php

namespace Application\Service\Atexo\Interfaces;

use DOMDocument;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Interfaces_InterfaceAuthentification
{
    public function getAthentificationVoFromXml($xml)
    {
        $domDocument = new DOMDocument();
        $domDocument->loadXML($xml);
        $transaction = $domDocument->documentElement;
        $control = $transaction->getElementsByTagName('Control')->item(0);
        $from = $control->getElementsByTagName('From')->item(0);

        $login = $from->getAttribute('login');
        $passW = $from->getAttribute('password');
        $email = $from->getAttribute('email');

        $message = $transaction->getElementsByTagName('Message')->item(0);

        $purpose = $message->getAttribute('PurposeCode');
        $action = $message->getAttribute('MessageAction');

        $interfaceAuthVo = new Atexo_Interfaces_InterfaceAuthentificationVo();
        $interfaceAuthVo->setLogin($login);
        $interfaceAuthVo->setPassWord($passW);
        $interfaceAuthVo->setEmail(trim($email));
        $interfaceAuthVo->setPurpose($purpose);
        $interfaceAuthVo->setAction($action);

        return $interfaceAuthVo;
    }
}
