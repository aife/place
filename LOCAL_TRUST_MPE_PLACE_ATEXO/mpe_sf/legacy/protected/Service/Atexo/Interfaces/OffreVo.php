<?php

namespace Application\Service\Atexo\Interfaces;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Interfaces_OffreVo
{
    private ?\Application\Service\Atexo\Interfaces\Atexo_Interfaces_EntrepriseVo $_entrepriseVo = null;
    private $_dateDepot;
    private $_identifiantOffre;
    private array $_colEnveloppes = [];

    public function getConsultationVo()
    {
        return $this->_consultationVo;
    }

    public function setConsultationVo(Atexo_Interfaces_ConsultationVo $consultationVo)
    {
        $this->_consultationVo = $consultationVo;
    }

    public function getEntrepriseVo()
    {
        return $this->_entrepriseVo;
    }

    public function setEntrepriseVo(Atexo_Interfaces_EntrepriseVo $entrepriseVo)
    {
        $this->_entrepriseVo = $entrepriseVo;
    }

    public function getDateDepot()
    {
        return $this->_dateDepot;
    }

    public function setDateDepot($value)
    {
        $this->_dateDepot = $value;
    }

    public function getIdentifiantOffre()
    {
        return $this->_identifiantOffre;
    }

    public function setIdentifiantOffre($value)
    {
        $this->_identifiantOffre = $value;
    }

    public function addEnveloppeVo(Atexo_Interfaces_EnveloppeVo $enveloppeVo)
    {
        $this->_colEnveloppes[] = $enveloppeVo;
    }

    public function getEnveloppeVo()
    {
        return $this->_colEnveloppes;
    }

    public function setEnveloppeVo(array $enveloppesVo)
    {
        $this->_colEnveloppes = $enveloppesVo;
    }
}
