<?php

namespace Application\Service\Atexo\Interfaces;

use Application\Service\Atexo\Atexo_Config;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Interfaces_EnveloppeVo
{
    private ?string $_cryptage = null;
    private ?string $_typeEnveloppe = null;
    private ?string $_estSousPli = null;
    private ?string $_attribue = null;
    private ?string $_statutEnveloppe = null;
    private $_dateOuverture;
    private $_nomFichier;
    private $_numeroSousPli;

    public function getCryptage()
    {
        return $this->_cryptage;
    }

    public function setCryptage($value)
    {
        $this->_cryptage = match ($value) {
            1 => 'OUI',
            0 => 'NON',
            default => '',
        };
    }

    public function getTypeEnveloppe()
    {
        return $this->_typeEnveloppe;
    }

    public function setTypeEnveloppe($value)
    {
        $this->_typeEnveloppe = match ($value) {
            Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE') => 'CANDIDATURE',
            Atexo_Config::getParameter('TYPE_ENV_OFFRE') => 'OFFRE',
            Atexo_Config::getParameter('TYPE_ENV_ANONYMAT') => 'ANONYMAT',
            default => '',
        };
    }

    public function getEstSousPli()
    {
        return $this->_estSousPli;
    }

    public function setEstSousPli($value)
    {
        if ($value > 0) {
            $this->_estSousPli = 'OUI';
        } else {
            $this->_estSousPli = 'NON';
        }
    }

    public function getAttribue()
    {
        return $this->_attribue;
    }

    public function setAttribue($value)
    {
        if ($value) {
            $this->_attribue = 'OUI';
        } else {
            $this->_attribue = 'NON';
        }
    }

    public function getStatutEnveloppe()
    {
        return $this->_statutEnveloppe;
    }

    public function setStatutEnveloppe($value)
    {
        $this->_statutEnveloppe = match ($value) {
            Atexo_Config::getParameter('STATUT_ENV_OUVERTE_EN_LIGNE'), Atexo_Config::getParameter('STATUT_ENV_OUVERTE_HORS_LIGNE'), Atexo_Config::getParameter('STATUT_ENV_OUVERTE'), Atexo_Config::getParameter('STATUT_ENV_OUVERTE_A_DISTANCE') => 'OUVERTE',
            Atexo_Config::getParameter('STATUT_ENV_FERME') => 'FERMEE',
            Atexo_Config::getParameter('STATUT_ENV_REFUSEE') => 'REFUSEE',
            Atexo_Config::getParameter('STATUT_ENV_SUPPRIMER') => 'SUPPRIMEE',
        };
    }

    public function getDateOuverture()
    {
        return $this->_dateOuverture;
    }

    public function setDateOuverture($value)
    {
        if ($value && '0000-00-00' != substr($value, 0, 10)) {
            $this->_dateOuverture = $value;
        } else {
            $this->_dateOuverture = '';
        }
    }

    public function getNomFichier()
    {
        return $this->_nomFichier;
    }

    public function setNomFichier($value)
    {
        $this->_nomFichier = $value;
    }

    public function setNumeroSousPli($value)
    {
        $this->_numeroSousPli = $value;
    }

    public function getNumeroSousPli()
    {
        return $this->_numeroSousPli;
    }
}
