<?php

namespace Application\Service\Atexo\Interfaces;

/**
 * commentaires.
 *
 * @author Othmane ABISOUROUR <othmane.abisourour@atexo.com>
 * @copyright Atexo 2009
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Interfaces_TelechargementVo
{
    private $_dateTelechargement;
    private $_nomFichier;
    private ?\Application\Service\Atexo\Interfaces\Atexo_Interfaces_EntrepriseVo $_entrepriseVo = null;

    public function setConsultationVo(Atexo_Interfaces_ConsultationVo $consultationVo)
    {
        $this->_consultationVo = $consultationVo;
    }

    public function getConsultationVo()
    {
        return $this->_consultationVo;
    }

    public function setDateTelechargement($value)
    {
        $this->_dateTelechargement = $value;
    }

    public function getDateTelechargement()
    {
        return $this->_dateTelechargement;
    }

    public function setNomFichier($value)
    {
        $this->_nomFichier = $value;
    }

    public function getNomFichier()
    {
        return $this->_nomFichier;
    }

    public function setEntrepriseVo(Atexo_Interfaces_EntrepriseVo $entrepriseVo)
    {
        $this->_entrepriseVo = $entrepriseVo;
    }

    public function getEntrepriseVo()
    {
        return $this->_entrepriseVo;
    }
}
