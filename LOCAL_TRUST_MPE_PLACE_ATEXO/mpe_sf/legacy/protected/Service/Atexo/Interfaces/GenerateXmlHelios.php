<?php

namespace Application\Service\Atexo\Interfaces;

use Application\Service\Atexo\Atexo_Config;
use DOMDocument;

/**
 * commentaires.
 *
 * @author Mounir LAMANE <mounir.lamane@atexo.com>
 * @copyright Atexo 2009
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Interfaces_GenerateXmlHelios
{
    private string $encoding = 'UTF-8';

    public function __construct($encoding = 'UTF-8')
    {
        $this->encoding = $encoding;
    }

    public function generateInterfaceXmlHelios($agentObject, $countTeletransmissionByAgent, $countTransmissionTransmitted, $countTransmissionToSend, $countAllTransmission)
    {
        $temps_debut = microtime(true);
        $domDocument = new DOMDocument('1.0', $this->encoding);

        //<Transaction>
        // |
        $transaction = $domDocument->createElement('Transaction', '');
        $domDocument->appendChild($transaction);
        // |
        // |___ Attributs Transaction :
        //         |

        //         |____ attribut xmlns:xsi
        self::addAttribute($domDocument, $transaction, 'xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
        //         |____ attribut xsi:noNamespaceSchemaLocation
        self::addAttribute($domDocument, $transaction, 'xsi:noNamespaceSchemaLocation', ".\interface-generique.xsd");
        //         |____ attribut Version
        self::addAttribute($domDocument, $transaction, 'Version', '1.0');
        //         |____ attribut TimeStamp
        self::addAttribute($domDocument, $transaction, 'TimeStamp', date('c'));
        // |
        // |
        // |___ <Control> :
        $control = $domDocument->createElement('Control', '');
        $transaction->appendChild($control);
        //             |
        //             |
        //             |___<From>
        $from = $domDocument->createElement('From', '');
        $control->appendChild($from);
        //             |
        //             |___ Attributs From :
        //                         |
        //                         |___login
        self::addAttribute($domDocument, $from, 'login', $agentObject->getLogin());
        //                         |___password
        self::addAttribute($domDocument, $from, 'password', '');
        //                         |___email
        self::addAttribute($domDocument, $from, 'email', $agentObject->getEmail());
        // |
        // |
        // |___ <Message> :
        $message = $domDocument->createElement('Message', '');
        $transaction->appendChild($message);
        //             |
        //             |___ Attributs Message :
        //                         |
        //                         |___PurposeCode
        self::addAttribute($domDocument, $message, 'PurposeCode', 'REPONSE');
        //                         |___MessageAction
        self::addAttribute($domDocument, $message, 'MessageAction', 'EXPORT_STATISTIQUES_AGENT_HELIOS');

        // |
        // |
        // |___ <StatistiquesAgentHelios> :
        $statistiquesAgentHelios = $domDocument->createElement('StatistiquesAgentHelios', '');
        $message->appendChild($statistiquesAgentHelios);

        // |
        // |
        // |___ <TeletransmissionsEtat> :
        $teletransmissionsEtat = $domDocument->createElement('TeletransmissionsEtat', '');
        $statistiquesAgentHelios->appendChild($teletransmissionsEtat);

        //             |
        //             |___ Attributs TeletransmissionsEtat :
        //                         |
        //                         |___NombreTeletransmissionsCreesParLagent
        self::addAttribute($domDocument, $teletransmissionsEtat, 'NombreTeletransmissionsCreesParLagent', $countTeletransmissionByAgent);
        //                         |___NombreTeletransmissionsTotal
        self::addAttribute($domDocument, $teletransmissionsEtat, 'NombreTeletransmissionsTotal', $countAllTransmission);
        //                         |___NombreTeletransmissionsEnvoyees
        self::addAttribute($domDocument, $teletransmissionsEtat, 'NombreTeletransmissionsEnvoyees', $countTransmissionTransmitted);
        //                         |___NombreTeletransmissionsEnAttente
        self::addAttribute($domDocument, $teletransmissionsEtat, 'NombreTeletransmissionsEnAttente', $countTransmissionToSend);
        $temps_fin = microtime(true);
        $txt = 'Debut generateInterfaceXmlHelios et fin : '.round($temps_fin - $temps_debut, 4)."\n\n";
        $fp = fopen(Atexo_Config::getParameter('LOG_DIR').session_id().'timer.txt', 'a');
        fwrite($fp, $txt);
        fclose($fp);

        return $domDocument->saveXML();
    }

    public function addAttribute($domDocument, $masterTag, $attributeName, $attributeContent)
    {
        $attribute = $domDocument->createAttribute($attributeName);
        $masterTag->appendChild($attribute);
        $attributeValue = $domDocument->createTextNode($attributeContent);
        $attribute->appendChild($attributeValue);
    }
}
