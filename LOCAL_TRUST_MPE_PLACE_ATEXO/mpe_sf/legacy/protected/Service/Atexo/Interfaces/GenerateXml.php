<?php

namespace Application\Service\Atexo\Interfaces;

use Application\Propel\Mpe\Entreprise;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Util;
use AtexoCrypto\Dto\Enveloppe;
use AtexoCrypto\Dto\Offre;
use AtexoDume\Dto\Lot;
use DOMDocument;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Interfaces_GenerateXml
{
    private string $encoding = 'ISO-8859-1';

    public function __construct($encoding = 'ISO-8859-1')
    {
        $this->encoding = $encoding;
    }

    public function generateInterfaceXml(Atexo_Interfaces_InterfaceAuthentificationVo $interfaceAuthentificationVo, array $interfacesVo, $xmlToGenerate = null)
    {
        $domDocument = new DOMDocument('1.0', $this->encoding);
        $domDocument->__construct();
        //<Transaction>
        // |
        $transaction = $domDocument->createElement('Transaction', '');
        $domDocument->appendChild($transaction);
        // |
        // |___ Attributs Transaction :
        //         |
        //         |____ attribut Version
        self::addAttribute($domDocument, $transaction, 'Version', '1.0');
        //         |____ attribute encoding
        self::addAttribute($domDocument, $transaction, 'encoding', $this->encoding);
        //         |____ attribut TimeStamp
        self::addAttribute($domDocument, $transaction, 'TimeStamp', date('c'));
        //         |____ attribut xmlns:xsi
        self::addAttribute($domDocument, $transaction, 'xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
        //         |____ attribut xsi:noNamespaceSchemaLocation
        self::addAttribute($domDocument, $transaction, 'xsi:noNamespaceSchemaLocation', 'creation-consultation.xsd');
        // |
        // |
        // |___ <Control> :
        $control = $domDocument->createElement('Control', '');
        $transaction->appendChild($control);
        //             |
        //             |
        //             |___<From>
        $from = $domDocument->createElement('From', '');
        $control->appendChild($from);
        //             |
        //             |___ Attributs From :
        //                         |
        //                         |___login
        self::addAttribute($domDocument, $from, 'login', $interfaceAuthentificationVo->getLogin());
        //                         |___password
        self::addAttribute($domDocument, $from, 'password', $interfaceAuthentificationVo->getPassWord());
        //                         |___email
        self::addAttribute($domDocument, $from, 'email', $interfaceAuthentificationVo->getEmail());
        //                         |___reference
        self::addAttribute($domDocument, $from, 'reference', $interfaceAuthentificationVo->getReference());

        // |
        // |
        // |___ <Message> :
        $message = $domDocument->createElement('Message', '');
        $transaction->appendChild($message);
        //             |
        //             |___ Attributs Message :
        //                         |
        //                         |___PurposeCode
        self::addAttribute($domDocument, $message, 'PurposeCode', Atexo_Util::toUtf8($interfaceAuthentificationVo->getPurpose()));
        //                         |___MessageAction
        self::addAttribute($domDocument, $message, 'MessageAction', Atexo_Util::toUtf8($interfaceAuthentificationVo->getAction()));
        if ($xmlToGenerate) {
            foreach ($interfacesVo as $interfaceVo) {
                if ($interfaceVo instanceof Atexo_Interfaces_ConsultationVo) {
                    if ($xmlToGenerate == Atexo_Config::getParameter('GENERATE_XML_CONSULTATION')) {
                        //          |
                        //             |___<Tender>
                        $tender = $domDocument->createElement('Tender', '');
                        $message->appendChild($tender);
                        //             |___ Attributs Tender :
                        //                         |
                        //                         |___Reference
                        self::addAttribute($domDocument, $tender, 'Reference', $interfaceVo->getReferenceUtilisateur());
                        //                         |___ReferenceConnecteur
                        self::addAttribute($domDocument, $tender, 'ReferenceConnecteur', $interfaceVo->getReferenceConnecteur());
                        //                         |___TenderProcessType
                        self::addAttribute($domDocument, $tender, 'TenderProcessType', $interfaceVo->getTypeAcces());
                        //                         |___TenderType
                        self::addAttribute($domDocument, $tender, 'TenderType', $interfaceVo->getAvis());
                        //                         |___AccessPhase
                        self::addAttribute($domDocument, $tender, 'AccessPhase', $interfaceVo->getPhaseAcces());
                        //                         |___Categorie
                        self::addAttribute($domDocument, $tender, 'Categorie', $interfaceVo->getCategorie());
                        //                         |___ClosureDate
                        self::addAttribute($domDocument, $tender, 'ClosureDate', $interfaceVo->getDateFin());
                        //                         |___PubDate
                        self::addAttribute($domDocument, $tender, 'PubDate', $interfaceVo->getDateMiseenligne());
                        //                         |___Acronyme
                        self::addAttribute($domDocument, $tender, 'Acronym', $interfaceVo->getAcronymeOrganisme());
                        //                         |___Delivery
                        self::addAttribute($domDocument, $tender, 'Delivery', $interfaceVo->getLieuxExecution());
                        //                         |___Organisme
                        self::addAttribute($domDocument, $tender, 'Organisme', $interfaceVo->getOrganisme());
                        //             |
                        //             |___<Intitule>
                        $intitule = $domDocument->createElement('Intitule', '');
                        $tender->appendChild($intitule);
                        //                     |
                        //                     |___<Description>
                        $description = $domDocument->createElement('Description', '');
                        $intitule->appendChild($description);
                        //                             |
                        //                             |___<Content>
                        $content = $domDocument->createElement('Content', '');
                        $description->appendChild($content);
                        //                                     |
                        //                                     |__<![CDATA[]]>
                        $cdataTitre = $domDocument->createCDATASection(Atexo_Util::toUtf8($interfaceVo->getIntitule()));
                        $content->appendChild($cdataTitre);

                        //             |
                        //             |___<Objet>
                        $objet = $domDocument->createElement('Objet', '');
                        $tender->appendChild($objet);
                        //                     |
                        //                     |___<Description>
                        $description = $domDocument->createElement('Description', '');
                        $objet->appendChild($description);
                        //                             |
                        //                             |___<Content>
                        $content = $domDocument->createElement('Content', '');
                        $description->appendChild($content);
                        //                                     |
                        //                                     |__<![CDATA[]]>
                        $cdataObjet = $domDocument->createCDATASection(Atexo_Util::toUtf8($interfaceVo->getObjet()));
                        $content->appendChild($cdataObjet);

                        //             |___<URL>
                        $url = $domDocument->createElement('URL', '');
                        $tender->appendChild($url);
                        //                     |
                        //                     |___<Description>
                        $description = $domDocument->createElement('Description', '');
                        $url->appendChild($description);
                        //                             |
                        //                             |___<Content>
                        $content = $domDocument->createElement('Content', '');
                        $description->appendChild($content);
                        //                                     |
                        //                                     |__<![CDATA[]]>
                        $cdataUrl = $domDocument->createCDATASection($interfaceVo->getUrl());
                        $content->appendChild($cdataUrl);

                        //             |___<Lots>
                        if (is_array($interfaceVo->getLots()) && count($interfaceVo->getLots()) > 0) {
                            $lots = $domDocument->createElement('Lots', '');
                            $tender->appendChild($lots);
                            $lotsConsultation = $interfaceVo->getLots();
                            foreach ($lotsConsultation as $lotConsultation) {
                                //                         |
                                //                         |___<Lot>
                                $lot = $domDocument->createElement('Lot', '');
                                $lots->appendChild($lot);
                                //                                 |___ Attributs Lot :
                                //                                         |
                                //                                         |___Numero
                                self::addAttribute($domDocument, $lot, 'Numero', $lotConsultation['numero']);
                                //                                         |___Categorie
                                self::addAttribute($domDocument, $lot, 'Categorie', $lotConsultation['categorie']);
                                //                                 |
                                //                                 |___<Intitule>
                                $intitule = $domDocument->createElement('Intitule', '');
                                $lot->appendChild($intitule);
                                //                                         |
                                //                                         |___<Description>
                                $description = $domDocument->createElement('Description', '');
                                $intitule->appendChild($description);
                                //                                                 |
                                //                                                 |___<Content>
                                $content = $domDocument->createElement('Content', '');
                                $description->appendChild($content);
                                //                                                         |
                                //                                                         |__<![CDATA[]]>
                                $cdataTitre = $domDocument->createCDATASection(Atexo_Util::toUtf8($lotConsultation['libelle']));
                                $content->appendChild($cdataTitre);
                                //                                 |
                                //                                 |___<Objet>
                                $intitule = $domDocument->createElement('Objet', '');
                                $lot->appendChild($intitule);
                                //                                         |
                                //                                         |___<Description>
                                $description = $domDocument->createElement('Description', '');
                                $intitule->appendChild($description);
                                //                                                 |
                                //                                                 |___<Content>
                                $content = $domDocument->createElement('Content', '');
                                $description->appendChild($content);
                                //                                                         |
                                //                                                         |__<![CDATA[]]>
                                $cdataTitre = $domDocument->createCDATASection(Atexo_Util::toUtf8($lotConsultation['description']));
                                $content->appendChild($cdataTitre);
                            }
                        }
                    } elseif ($xmlToGenerate == Atexo_Config::getParameter('GENERATE_XML_OFFRE')) {
                        //         |
                        //             |___<TenderDefinition>
                        $tender = $domDocument->createElement('TenderDefinition', '');
                        $message->appendChild($tender);
                        //             |___ Attributs Tender :
                        //                         |
                        //                         |___Reference
                        self::addAttribute($domDocument, $tender, 'Reference', Atexo_Util::toUtf8($interfaceVo->getReferenceUtilisateur()));
                        //                         |___ReferenceConnecteur
                        self::addAttribute($domDocument, $tender, 'ReferenceConnecteur', Atexo_Util::toUtf8($interfaceVo->getReferenceConnecteur()));
                        //                         |___AccessPhase
                        self::addAttribute($domDocument, $tender, 'AccessPhase', Atexo_Util::toUtf8($interfaceVo->getPhaseAcces()));
                        //             |
                        //             |__<RegistreOffres>
                        $registreOffres = $domDocument->createElement('RegistreOffres', '');
                        $tender->appendChild($registreOffres);

                        $offresVo = $interfaceVo->getOffresVo();

                        if (is_array($offresVo)) {
                            foreach ($offresVo as $offreVo) {
                                if ($offreVo instanceof Atexo_Interfaces_OffreVo) {
                                    //                             |
                                    //                             |__<Offre>
                                    $offre = $domDocument->createElement('Offre', '');
                                    $registreOffres->appendChild($offre);
                                    //                                     |___ Attributs Offre :
                                    //                                                 |
                                    //                                                 |___dateDepot
                                    self::addAttribute($domDocument, $offre, 'dateDepot', Atexo_Util::toUtf8($offreVo->getDateDepot()));
                                    //                                                 |___dateDepot
                                    self::addAttribute($domDocument, $offre, 'identifiantOffre', Atexo_Util::toUtf8($offreVo->getIdentifiantOffre()));

                                    $entrepriseVo = $offreVo->getEntrepriseVo();
                                    if ($entrepriseVo instanceof Atexo_Interfaces_EntrepriseVo) {
                                        //                                     |
                                        //                                     |__<Entreprise>
                                        $entreprise = $domDocument->createElement('Entreprise', '');
                                        $offre->appendChild($entreprise);
                                        //                                                 |
                                        //                                                 |___ Attributs Offre :
                                        //                                                                 |
                                        //                                                                 |___id
                                        self::addAttribute($domDocument, $entreprise, 'id', $entrepriseVo->getId());
                                        //                                                                 |___nom
                                        self::addAttribute($domDocument, $entreprise, 'nom', Atexo_Util::toUtf8($entrepriseVo->getNom()));
                                        //                                                                 |___adresse
                                        self::addAttribute($domDocument, $entreprise, 'adresse', Atexo_Util::toUtf8($entrepriseVo->getAdresse()));
                                        //                                                                 |___adresse2
                                        self::addAttribute($domDocument, $entreprise, 'adresse2', Atexo_Util::toUtf8($entrepriseVo->getAdresse2()));
                                        //                                                                 |___codePostal
                                        self::addAttribute($domDocument, $entreprise, 'codePostal', $entrepriseVo->getCodePostal());
                                        //                                                                 |___ville
                                        self::addAttribute($domDocument, $entreprise, 'ville', Atexo_Util::toUtf8($entrepriseVo->getVille()));
                                        //                                                                 |___pays
                                        self::addAttribute($domDocument, $entreprise, 'pays', Atexo_Util::toUtf8($entrepriseVo->getPays()));
                                        //                                                                 |___telephone
                                        self::addAttribute($domDocument, $entreprise, 'telephone', $entrepriseVo->getTelephone());
                                        //                                                                 |___siret
                                        self::addAttribute($domDocument, $entreprise, 'siret', $entrepriseVo->getSiret());
                                        //                                                                 |___formejuridique
                                        self::addAttribute($domDocument, $entreprise, 'formejuridique', Atexo_Util::toUtf8($entrepriseVo->getFormejuridique()));
                                        $inscritVo = $entrepriseVo->getInscrit();
                                        //                                                                 |___nomInscrit
                                        self::addAttribute($domDocument, $entreprise, 'nomInscrit', Atexo_Util::toUtf8($inscritVo->getNom()));
                                        //                                                                 |___emailInscrit
                                        self::addAttribute($domDocument, $entreprise, 'emailInscrit', Atexo_Util::toUtf8($inscritVo->getEmail()));
                                        //                                                                 |___prenomInscrit
                                        self::addAttribute($domDocument, $entreprise, 'prenomInscrit', Atexo_Util::toUtf8($inscritVo->getPrenom()));
                                    }
                                    $enveloppesVo = $offreVo->getEnveloppeVo();
                                    if (is_array($enveloppesVo)) {
                                        foreach ($enveloppesVo as $enveloppeVo) {
                                            //                                     |
                                            //                                     |__<Enveloppe>
                                            $enveloppe = $domDocument->createElement('Enveloppe', '');
                                            $offre->appendChild($enveloppe);
                                            //                                             |
                                            //                                             |___ Attributs Offre :
                                            //                                                         |
                                            //                                                         |___nom
                                            self::addAttribute($domDocument, $enveloppe, 'nomFichier', Atexo_Util::toUtf8($enveloppeVo->getNomFichier()));
                                            //                                                         |___cryptage
                                            self::addAttribute($domDocument, $enveloppe, 'cryptage', Atexo_Util::toUtf8($enveloppeVo->getCryptage()));
                                            //                                                         |___typeEnveloppe
                                            self::addAttribute($domDocument, $enveloppe, 'typeEnveloppe', Atexo_Util::toUtf8($enveloppeVo->getTypeEnveloppe()));
                                            //                                                         |___estSousPli
                                            self::addAttribute($domDocument, $enveloppe, 'estSousPli', Atexo_Util::toUtf8($enveloppeVo->getEstSousPli()));
                                            //                                                         |___numeroSousPli
                                            self::addAttribute($domDocument, $enveloppe, 'numeroSousPli', Atexo_Util::toUtf8($enveloppeVo->getEstSousPli()));
                                            //                                                         |___attribue
                                            self::addAttribute($domDocument, $enveloppe, 'attribue', Atexo_Util::toUtf8($enveloppeVo->getAttribue()));
                                            //                                                         |___statutEnveloppe
                                            self::addAttribute($domDocument, $enveloppe, 'statutEnveloppe', Atexo_Util::toUtf8($enveloppeVo->getStatutEnveloppe()));
                                            if ($enveloppeVo->getDateOuverture()) {
                                                //                                                         |___dateOuverture
                                                self::addAttribute($domDocument, $enveloppe, 'dateOuverture', str_replace(' ', 'T', $enveloppeVo->getDateOuverture()));
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } elseif ('TELECHARGEMENT' == $xmlToGenerate) {
                        $telechargements = $interfaceVo->getTelechargement();
                        //         |
                        //             |___<TenderDefinition>
                        $tenderDefinition = $domDocument->createElement('TenderDefinition', '');
                        $message->appendChild($tenderDefinition);
                        //             |___ Attributs TenderDefinition :
                        //                         |
                        //                         |___Reference
                        $reference = $domDocument->createAttribute('Reference');
                        $tenderDefinition->appendChild($reference);
                        $referenceValue = $domDocument->createTextNode($interfaceVo->getReferenceUtilisateur());
                        $reference->appendChild($referenceValue);
                        //                         |
                        //                         |___ReferenceConnecteur
                        $referenceConnecteur = $domDocument->createAttribute('ReferenceConnecteur');
                        $tenderDefinition->appendChild($referenceConnecteur);
                        $referenceConnecteurValue = $domDocument->createTextNode($interfaceVo->getReferenceConnecteur());
                        $referenceConnecteur->appendChild($referenceConnecteurValue);
                        //                 |___<RegistreRetraits>
                        $tenderRetraits = $domDocument->createElement('RegistreRetraits', '');
                        $tenderDefinition->appendChild($tenderRetraits);

                        foreach ($telechargements as $telechargement) {
                            //                     |___<Retrait>
                            $retraits = $domDocument->createElement('Retrait', '');
                            $tenderRetraits->appendChild($retraits);
                            //                         |___ Attributs Retrait :
                            //                             |
                            //                             |___DateTelechargement
                            $dateTelechargement = $domDocument->createAttribute('dateTelechargement');
                            $retraits->appendChild($dateTelechargement);
                            $dateTelechargementValue = $domDocument->createTextNode($telechargement->getDateTelechargement());
                            $dateTelechargement->appendChild($dateTelechargementValue);
                            //                             |
                            //                             |___NomFichier
                            $nomFichier = $domDocument->createAttribute('nomFichier');
                            $retraits->appendChild($nomFichier);
                            $nomFichierValue = $domDocument->createTextNode(Atexo_Util::toUtf8($telechargement->getNomFichier()));
                            $nomFichier->appendChild($nomFichierValue);
                            //                             |___<Entreprise>
                            $entreprise = $domDocument->createElement('Entreprise', '');
                            $retraits->appendChild($entreprise);
                            $entrepriseVo = $telechargement->getEntrepriseVo();
                            if (!$entrepriseVo instanceof Atexo_Interfaces_EntrepriseVo) {
                                $entrepriseVo = new Atexo_Interfaces_EntrepriseVo();
                            }

                            //                                 |___ Attributs Retrait :
                            //                                     |
                            //                                     |___Id
                            $id = $domDocument->createAttribute('id');
                            $entreprise->appendChild($id);
                            $idValue = $domDocument->createTextNode($entrepriseVo->getId());
                            $id->appendChild($idValue);
                            //                                     |
                            //                                     |___Nom
                            $nom = $domDocument->createAttribute('nom');
                            $entreprise->appendChild($nom);
                            $nomValue = $domDocument->createTextNode(Atexo_Util::toUtf8($entrepriseVo->getNom()));
                            $nom->appendChild($nomValue);
                            //                                     |
                            //                                     |___Adresse
                            $adresse = $domDocument->createAttribute('adresse');
                            $entreprise->appendChild($adresse);
                            $adresseValue = $domDocument->createTextNode(Atexo_Util::toUtf8($entrepriseVo->getAdresse()));
                            $adresse->appendChild($adresseValue);
                            //                                     |
                            //                                     |___Adresse2
                            $adresse2 = $domDocument->createAttribute('adresse2');
                            $entreprise->appendChild($adresse2);
                            $adresse2Value = $domDocument->createTextNode(Atexo_Util::toUtf8($entrepriseVo->getAdresse2()));
                            $adresse2->appendChild($adresse2Value);
                            //                                     |
                            //                                     |___CodePostal
                            $codePostal = $domDocument->createAttribute('codePostal');
                            $entreprise->appendChild($codePostal);
                            $codePostalValue = $domDocument->createTextNode($entrepriseVo->getCodePostal());
                            $codePostal->appendChild($codePostalValue);
                            //                                     |
                            //                                     |___Ville
                            $ville = $domDocument->createAttribute('ville');
                            $entreprise->appendChild($ville);
                            $villeValue = $domDocument->createTextNode(Atexo_Util::toUtf8($entrepriseVo->getVille()));
                            $ville->appendChild($villeValue);
                            //                                     |
                            //                                     |___Pays
                            $pays = $domDocument->createAttribute('pays');
                            $entreprise->appendChild($pays);
                            $paysValue = $domDocument->createTextNode(Atexo_Util::toUtf8($entrepriseVo->getPays()));
                            $pays->appendChild($paysValue);
                            //                                     |
                            //                                     |___Telephone
                            $telephone = $domDocument->createAttribute('telephone');
                            $entreprise->appendChild($telephone);
                            $telephoneValue = $domDocument->createTextNode(Atexo_Util::toUtf8($entrepriseVo->getTelephone()));
                            $telephone->appendChild($telephoneValue);
                            //                                     |
                            //                                     |___Siret
                            $siret = $domDocument->createAttribute('siret');
                            $entreprise->appendChild($siret);
                            $siretValue = $domDocument->createTextNode(Atexo_Util::toUtf8($entrepriseVo->getSiret()));
                            $siret->appendChild($siretValue);
                            //                                     |
                            //                                     |___Formejuridique
                            $formejuridique = $domDocument->createAttribute('formejuridique');
                            $entreprise->appendChild($formejuridique);
                            $formejuridiqueValue = $domDocument->createTextNode(Atexo_Util::toUtf8($entrepriseVo->getFormejuridique()));
                            $formejuridique->appendChild($formejuridiqueValue);

                            $inscritVo = $entrepriseVo->getInscrit();
                            if (!$inscritVo instanceof Atexo_Interfaces_InscritVo) {
                                $inscritVo = new Atexo_Interfaces_InscritVo();
                            }
                            //                                     |
                            //                                     |___NomInscrit
                            $nomInscrit = $domDocument->createAttribute('nomInscrit');
                            $entreprise->appendChild($nomInscrit);
                            $nomInscritValue = $domDocument->createTextNode(Atexo_Util::toUtf8($inscritVo->getNom()));
                            $nomInscrit->appendChild($nomInscritValue);
                            //                                     |
                            //                                     |___EmailInscrit
                            $emailInscrit = $domDocument->createAttribute('emailInscrit');
                            $entreprise->appendChild($emailInscrit);
                            $emailInscritValue = $domDocument->createTextNode(Atexo_Util::toUtf8($inscritVo->getEmail()));
                            $emailInscrit->appendChild($emailInscritValue);
                            //                                     |
                            //                                     |___PrenomInscrit
                            $prenomInscrit = $domDocument->createAttribute('prenomInscrit');
                            $entreprise->appendChild($prenomInscrit);
                            $prenomInscritValue = $domDocument->createTextNode(Atexo_Util::toUtf8($inscritVo->getPrenom()));
                            $prenomInscrit->appendChild($prenomInscritValue);
                        }//fin foreach
                    }//fin elseif
                    elseif ('ENTREPRISE' == $xmlToGenerate) {
                        $entreprises = $interfaceVo->getEntreprise();
                        //          |
                        //          |___<TenderDefinition>
                        $tenderDefinition = $domDocument->createElement('TenderDefinition', '');
                        $message->appendChild($tenderDefinition);

                        //              |___ Attributs TenderDefinition :
                        //                          |
                        //                          |___Reference
                        $reference = $domDocument->createAttribute('Reference');
                        $tenderDefinition->appendChild($reference);
                        $referenceValue = $domDocument->createTextNode(Atexo_Util::toUtf8($interfaceVo->getReferenceUtilisateur()));
                        $reference->appendChild($referenceValue);
                        //                          |
                        //                          |___ReferenceConnecteur
                        $referenceConnecteur = $domDocument->createAttribute('ReferenceConnecteur');
                        $tenderDefinition->appendChild($referenceConnecteur);
                        $referenceConnecteurValue = $domDocument->createTextNode(Atexo_Util::toUtf8($interfaceVo->getReferenceConnecteur()));
                        $referenceConnecteur->appendChild($referenceConnecteurValue);
                        //              |___<RegistreQuestions>
                        $tenderEntreprises = $domDocument->createElement('RegistreEntprises', '');
                        $tenderDefinition->appendChild($tenderEntreprises);

                        foreach ($entreprises as $entreprise) {
                            //                  |___<Entreprise>
                            $registreEntreprise = $domDocument->createElement('Entreprise', '');
                            $tenderEntreprises->appendChild($registreEntreprise);
                            //                          |___ Attributs Entreprise :
                            //                              |
                            //                              |___Id
                            $id = $domDocument->createAttribute('id');
                            $registreEntreprise->appendChild($id);
                            $idValue = $domDocument->createTextNode($entreprise->getId());
                            $id->appendChild($idValue);
                            //                              |
                            //                              |___Nom
                            $nom = $domDocument->createAttribute('nom');
                            $registreEntreprise->appendChild($nom);
                            $nomValue = $domDocument->createTextNode(Atexo_Util::toUtf8($entreprise->getNom()));
                            $nom->appendChild($nomValue);
                            //                              |
                            //                              |___Adresse
                            $adresse = $domDocument->createAttribute('adresse');
                            $registreEntreprise->appendChild($adresse);
                            $adresseValue = $domDocument->createTextNode(Atexo_Util::toUtf8($entreprise->getAdresse()));
                            $adresse->appendChild($adresseValue);
                            //                              |
                            //                              |___Adresse2
                            $adresse2 = $domDocument->createAttribute('adresse2');
                            $registreEntreprise->appendChild($adresse2);
                            $adresse2Value = $domDocument->createTextNode(Atexo_Util::toUtf8($entreprise->getAdresse2()));
                            $adresse2->appendChild($adresse2Value);
                            //                              |
                            //                              |___CodePostal
                            $codePostal = $domDocument->createAttribute('codePostal');
                            $registreEntreprise->appendChild($codePostal);
                            $codePostalValue = $domDocument->createTextNode($entreprise->getCodePostal());
                            $codePostal->appendChild($codePostalValue);
                            //                              |
                            //                              |___Ville
                            $ville = $domDocument->createAttribute('ville');
                            $registreEntreprise->appendChild($ville);
                            $villeValue = $domDocument->createTextNode(Atexo_Util::toUtf8($entreprise->getVille()));
                            $ville->appendChild($villeValue);
                            //                              |
                            //                              |___Pays
                            $pays = $domDocument->createAttribute('pays');
                            $registreEntreprise->appendChild($pays);
                            $paysValue = $domDocument->createTextNode($entreprise->getPays());
                            $pays->appendChild($paysValue);
                            //                              |
                            //                              |___Telephone
                            $telephone = $domDocument->createAttribute('telephone');
                            $registreEntreprise->appendChild($telephone);
                            $telephoneValue = $domDocument->createTextNode($entreprise->getTelephone());
                            $telephone->appendChild($telephoneValue);
                            //                              |
                            //                              |___Siret
                            $siret = $domDocument->createAttribute('siret');
                            $registreEntreprise->appendChild($siret);
                            $siretValue = $domDocument->createTextNode($entreprise->getSiret());
                            $siret->appendChild($siretValue);
                            //                              |
                            //                              |___Formejuridique
                            $formejuridique = $domDocument->createAttribute('formejuridique');
                            $registreEntreprise->appendChild($formejuridique);
                            $formejuridiqueValue = $domDocument->createTextNode($entreprise->getFormejuridique());
                            $formejuridique->appendChild($formejuridiqueValue);

                            $inscrits = $entreprise->getInscrit();
                            if (!$inscrits instanceof Atexo_Interfaces_InscritVo) {
                                $inscrits = new Atexo_Interfaces_InscritVo();
                            }
                            //                                      |
                            //                                      |___NomInscrit
                            $nomInscrit = $domDocument->createAttribute('nomInscrit');
                            $registreEntreprise->appendChild($nomInscrit);
                            $nomInscritValue = $domDocument->createTextNode(Atexo_Util::toUtf8($inscrits->getNom()));
                            $nomInscrit->appendChild($nomInscritValue);
                            //                                      |
                            //                                      |___EmailInscrit
                            $emailInscrit = $domDocument->createAttribute('emailInscrit');
                            $registreEntreprise->appendChild($emailInscrit);
                            $emailInscritValue = $domDocument->createTextNode(Atexo_Util::toUtf8($inscrits->getEmail()));
                            $emailInscrit->appendChild($emailInscritValue);
                            //                                      |
                            //                                      |___PrenomInscrit
                            $prenomInscrit = $domDocument->createAttribute('prenomInscrit');
                            $registreEntreprise->appendChild($prenomInscrit);
                            $prenomInscritValue = $domDocument->createTextNode(Atexo_Util::toUtf8($inscrits->getPrenom()));
                            $prenomInscrit->appendChild($prenomInscritValue);
                        }
                    } elseif ('QUESTION' == $xmlToGenerate) {
                        $questions = $interfaceVo->getQuestion();
                        //          |
                        //          |___<TenderDefinition>
                        $tenderDefinition = $domDocument->createElement('TenderDefinition', '');
                        $message->appendChild($tenderDefinition);

                        //              |___ Attributs TenderDefinition :
                        //                          |
                        //                          |___Reference
                        $reference = $domDocument->createAttribute('Reference');
                        $tenderDefinition->appendChild($reference);
                        $referenceValue = $domDocument->createTextNode(Atexo_Util::toUtf8($interfaceVo->getReferenceUtilisateur()));
                        $reference->appendChild($referenceValue);
                        //                          |
                        //                          |___ReferenceConnecteur
                        $referenceConnecteur = $domDocument->createAttribute('ReferenceConnecteur');
                        $tenderDefinition->appendChild($referenceConnecteur);
                        $referenceConnecteurValue = $domDocument->createTextNode(Atexo_Util::toUtf8($interfaceVo->getReferenceConnecteur()));
                        $referenceConnecteur->appendChild($referenceConnecteurValue);
                        //              |___<RegistreQuestions>
                        $tenderQuestions = $domDocument->createElement('RegistreQuestions', '');
                        $tenderDefinition->appendChild($tenderQuestions);

                        foreach ($questions as $question) {
                            //                  |___<Question>
                            $registreQuestion = $domDocument->createElement('Question', '');
                            $tenderQuestions->appendChild($registreQuestion);
                            //                          |___ Attributs Question :
                            //                              |
                            //                              |___DateQuestion
                            $dateQuestion = $domDocument->createAttribute('dateQuestion');
                            $registreQuestion->appendChild($dateQuestion);
                            $dateQuestionValue = $domDocument->createTextNode($question->getDateQuestion());
                            $dateQuestion->appendChild($dateQuestionValue);
                            //                              |
                            //                              |___nomFichier
                            $nomFichier = $domDocument->createAttribute('nomFichier');
                            $registreQuestion->appendChild($nomFichier);
                            $nomFichierValue = $domDocument->createTextNode(Atexo_Util::toUtf8($question->getNomFichier()));
                            $nomFichier->appendChild($nomFichierValue);

                            //                          |___<Content>
                            $content = $domDocument->createElement('Content', '');
                            $registreQuestion->appendChild($content);
                            //                              |
                            //                              |__<![CDATA[]]>
                            $cdataContent = $domDocument->createCDATASection(Atexo_Util::toUtf8($question->getContenu()));
                            $content->appendChild($cdataContent);

                            //                          |___<Reponse>
                            $reponse = $domDocument->createElement('Reponse', '');
                            $registreQuestion->appendChild($reponse);
                            //                                  |___ Attributs Reponse :
                            //                                      |
                            //                                      |___dateReponse
                            $dateReponse = $domDocument->createAttribute('dateReponse');
                            $reponse->appendChild($dateReponse);
                            $dateReponseValue = $domDocument->createTextNode($question->getDateReponse());
                            $dateReponse->appendChild($dateReponseValue);

                            //                          |___<Entreprise>
                            $entreprise = $domDocument->createElement('Entreprise', '');
                            $registreQuestion->appendChild($entreprise);
                            $entrepriseVo = $question->getEntrepriseVo();
                            if (!$entrepriseVo instanceof Atexo_Interfaces_EntrepriseVo) {
                                $entrepriseVo = new Atexo_Interfaces_EntrepriseVo();
                            }

                            //                                  |___ Attributs Retrait :
                            //                                      |
                            //                                      |___Id
                            $id = $domDocument->createAttribute('id');
                            $entreprise->appendChild($id);
                            $idValue = $domDocument->createTextNode($entrepriseVo->getId());
                            $id->appendChild($idValue);
                            //                                      |
                            //                                      |___Nom
                            $nom = $domDocument->createAttribute('nom');
                            $entreprise->appendChild($nom);
                            $nomValue = $domDocument->createTextNode(Atexo_Util::toUtf8($entrepriseVo->getNom()));
                            $nom->appendChild($nomValue);
                            //                                      |
                            //                                      |___Adresse
                            $adresse = $domDocument->createAttribute('adresse');
                            $entreprise->appendChild($adresse);
                            $adresseValue = $domDocument->createTextNode(Atexo_Util::toUtf8($entrepriseVo->getAdresse()));
                            $adresse->appendChild($adresseValue);
                            //                                      |
                            //                                      |___Adresse2
                            $adresse2 = $domDocument->createAttribute('adresse2');
                            $entreprise->appendChild($adresse2);
                            $adresse2Value = $domDocument->createTextNode(Atexo_Util::toUtf8($entrepriseVo->getAdresse2()));
                            $adresse2->appendChild($adresse2Value);
                            //                                      |
                            //                                      |___CodePostal
                            $codePostal = $domDocument->createAttribute('codePostal');
                            $entreprise->appendChild($codePostal);
                            $codePostalValue = $domDocument->createTextNode($entrepriseVo->getCodePostal());
                            $codePostal->appendChild($codePostalValue);
                            //                                      |
                            //                                      |___Ville
                            $ville = $domDocument->createAttribute('ville');
                            $entreprise->appendChild($ville);
                            $villeValue = $domDocument->createTextNode(Atexo_Util::toUtf8($entrepriseVo->getVille()));
                            $ville->appendChild($villeValue);
                            //                                      |
                            //                                      |___Pays
                            $pays = $domDocument->createAttribute('pays');
                            $entreprise->appendChild($pays);
                            $paysValue = $domDocument->createTextNode(Atexo_Util::toUtf8($entrepriseVo->getPays()));
                            $pays->appendChild($paysValue);
                            //                                      |
                            //                                      |___Telephone
                            $telephone = $domDocument->createAttribute('telephone');
                            $entreprise->appendChild($telephone);
                            $telephoneValue = $domDocument->createTextNode($entrepriseVo->getTelephone());
                            $telephone->appendChild($telephoneValue);
                            //                                      |
                            //                                      |___Siret
                            $siret = $domDocument->createAttribute('siret');
                            $entreprise->appendChild($siret);
                            $siretValue = $domDocument->createTextNode($entrepriseVo->getSiret());
                            $siret->appendChild($siretValue);
                            //                                      |
                            //                                      |___Formejuridique
                            $formejuridique = $domDocument->createAttribute('formejuridique');
                            $entreprise->appendChild($formejuridique);
                            $formejuridiqueValue = $domDocument->createTextNode(Atexo_Util::toUtf8($entrepriseVo->getFormejuridique()));
                            $formejuridique->appendChild($formejuridiqueValue);

                            $inscritVo = $entrepriseVo->getInscrit();
                            if (!$inscritVo instanceof Atexo_Interfaces_InscritVo) {
                                $inscritVo = new Atexo_Interfaces_InscritVo();
                            }
                            //                                      |
                            //                                      |___NomInscrit
                            $nomInscrit = $domDocument->createAttribute('nomInscrit');
                            $entreprise->appendChild($nomInscrit);
                            $nomInscritValue = $domDocument->createTextNode(Atexo_Util::toUtf8($inscritVo->getNom()));
                            $nomInscrit->appendChild($nomInscritValue);
                            //                                      |
                            //                                      |___EmailInscrit
                            $emailInscrit = $domDocument->createAttribute('emailInscrit');
                            $entreprise->appendChild($emailInscrit);
                            $emailInscritValue = $domDocument->createTextNode(Atexo_Util::toUtf8($inscritVo->getEmail()));
                            $emailInscrit->appendChild($emailInscritValue);
                            //                                      |
                            //                                      |___PrenomInscrit
                            $prenomInscrit = $domDocument->createAttribute('prenomInscrit');
                            $entreprise->appendChild($prenomInscrit);
                            $prenomInscritValue = $domDocument->createTextNode(Atexo_Util::toUtf8($inscritVo->getPrenom()));
                            $prenomInscrit->appendChild($prenomInscritValue);
                        }//fin foreach
                    }//fin else
                }
            }
        }

        return $domDocument->saveXML();
    }

    public function generateXmlRegistreDepotMarco(Atexo_Interfaces_InterfaceAuthentificationVo $authentificationVo, array $consultationsVo)
    {
        //les donnees recues sont deja en utf-8 car passee par la fonction generatexml qui le fait
        $domDocument = new DOMDocument('1.0', $this->encoding);
        $listeAffaire = $domDocument->createElement('ListeAffaire', '');
        $domDocument->appendChild($listeAffaire);
        self::addAttribute($domDocument, $listeAffaire, 'xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
        self::addAttribute($domDocument, $listeAffaire, 'xsi:noNamespaceSchemaLocation', './MARCO_DEMAT.XSD');
        self::addAttribute($domDocument, $listeAffaire, 'Version', '1.00');
        self::addAttribute($domDocument, $listeAffaire, 'Emetteur', 'ATEXO');
        $ordre = 0;
        foreach ($consultationsVo as $consultationVo) {
            if ($consultationVo instanceof Atexo_Interfaces_ConsultationVo) {
                $offresVo = $consultationVo->getOffresVo();
                if (is_array($offresVo)) {
                    foreach ($offresVo as $offreVo) {
                        ++$ordre;
                        $mafwrepg = $domDocument->createElement('MAFWREPG', '');
                        $listeAffaire->appendChild($mafwrepg); // |___Attributs MAFWREPG :
                        //                                 |__AFG_ORG
                        self::addAttribute($domDocument, $mafwrepg, 'AFG_ORG', $authentificationVo->getAfgOrg());
                        //                                 |__AFG_NUM
                        $referenceConnecteur = $consultationVo->getReferenceConnecteur();
                        $arrayReference = explode('-', $referenceConnecteur);
                        $refUtilisateur = $arrayReference[0];
                        $afdDos = $arrayReference[count($arrayReference) - 1];
                        self::addAttribute($domDocument, $mafwrepg, 'AFG_NUM', $refUtilisateur);
                        //                                 |__AFD_DOS
                        self::addAttribute($domDocument, $mafwrepg, 'AFD_DOS', $afdDos);
                        //                                 |__RPG_DATE
                        self::addAttribute($domDocument, $mafwrepg, 'RPG_DATE', substr($offreVo->getDateDepot(), 0, 10));
                        //                                 |__RPG_HHMN
                        self::addAttribute($domDocument, $mafwrepg, 'RPG_HHMN', trim(str_replace(':', '', substr($offreVo->getDateDepot(), 11, 5))));
                        //                                 |__AFL_LOT
                        self::addAttribute($domDocument, $mafwrepg, 'AFL_LOT', ' ');
                        //                                 |__FOW_COD
                        self::addAttribute($domDocument, $mafwrepg, 'FOW_COD', ' ');
                        //                                 |__RPG_ORD
                        self::addAttribute($domDocument, $mafwrepg, 'RPG_ORD', $ordre);
                        //                     |__MAFFOUW
                        $entrepriseVo = $offreVo->getEntrepriseVo();
                        if (!$entrepriseVo instanceof Atexo_Interfaces_EntrepriseVo) {
                            $entrepriseVo = new Atexo_Interfaces_EntrepriseVo();
                        }
                        $inscritVo = $entrepriseVo->getInscrit();
                        if (!$inscritVo instanceof Atexo_Interfaces_InscritVo) {
                            $inscritVo = new Atexo_Interfaces_InscritVo();
                        }
                        $maffouw = $domDocument->createElement('MAFFOUW', '');
                        $mafwrepg->appendChild($maffouw); //    |___Attributs MAFFOUW :
                        //                                 |__FOW_DEN
                        self::addAttribute($domDocument, $maffouw, 'FOW_DEN', $entrepriseVo->getNom());
                        //                                 |__FOW_REP
                        self::addAttribute($domDocument, $maffouw, 'FOW_REP', $inscritVo->getNom().' '.$inscritVo->getPrenom());
                        //                                 |__FOW_EMAIL
                        self::addAttribute($domDocument, $maffouw, 'FOW_EMAIL', $inscritVo->getEmail());
                        //                                 |__FOW_ADR
                        self::addAttribute($domDocument, $maffouw, 'FOW_ADR', $entrepriseVo->getAdresse());
                        //                                 |__FOW_CP
                        self::addAttribute($domDocument, $maffouw, 'FOW_CP', $entrepriseVo->getCodePostal());
                        //                                 |__FOW_VILLE
                        self::addAttribute($domDocument, $maffouw, 'FOW_VILLE', $entrepriseVo->getVille());
                        //                                 |__FOW_PAYS
                        self::addAttribute($domDocument, $maffouw, 'FOW_PAYS', $entrepriseVo->getPays());
                        //                                 |__FOW_FJUR
                        self::addAttribute($domDocument, $maffouw, 'FOW_FJUR', $entrepriseVo->getFormeJuridique());
                        //                                 |__FOW_TEL
                        self::addAttribute($domDocument, $maffouw, 'FOW_TEL', $entrepriseVo->getTelephone());
                        //                                 |__FOW_COD
                        self::addAttribute($domDocument, $maffouw, 'FOW_COD', $entrepriseVo->getId());
                        //                                 |__FOW_MDP
                        self::addAttribute($domDocument, $maffouw, 'FOW_MDP', ' ');
                        $enveloppeVos = $offreVo->getEnveloppeVo();
                        foreach ($enveloppeVos as $enveloppeVo) {
                            $typeConsultation = 'O';
                            $nomNoeud = 'MAFWREPO'; //    |__MAFWREPC ou  MAFWREPO selon num_procedure de la consultation
                            $mafwrep = $domDocument->createElement($nomNoeud, '');
                            $mafwrepg->appendChild($mafwrep);
                            if (!$enveloppeVo instanceof Atexo_Interfaces_EnveloppeVo) {
                                $enveloppeVo = new Atexo_Interfaces_EnveloppeVo();
                            }
                            if ('MAFWREPC' == $nomNoeud) {
                                self::addAttribute($domDocument, $mafwrep, 'AFL_LOT', ' ');
                            } else {
                                self::addAttribute($domDocument, $mafwrep, 'AFL_LOT', $enveloppeVo->getNumeroSousPli());
                            }
                            self::addAttribute($domDocument, $mafwrep, 'AFG_ORG', $authentificationVo->getAfgOrg());
                            self::addAttribute($domDocument, $mafwrep, 'AFD_DOS', $afdDos);
                            self::addAttribute($domDocument, $mafwrep, 'AFG_NUM', $refUtilisateur);
                            self::addAttribute($domDocument, $mafwrep, 'STR_TYPE', $typeConsultation);
                            self::addAttribute($domDocument, $mafwrep, 'FOW_COD', $entrepriseVo->getId());
                            self::addAttribute($domDocument, $mafwrep, 'RPG_ORD', $ordre);
                            self::addAttribute($domDocument, $mafwrep, 'STR_LIB', $enveloppeVo->getNomFichier());
                        }
                    }
                }
            }
        }
        $xmlDepot = $domDocument->saveXML();

        return Atexo_Util::httpEncodingToUtf8($xmlDepot);
    }

    public function generateXmlRegistreRetraitMarco(Atexo_Interfaces_InterfaceAuthentificationVo $authentificationVo, array $consultationsVo)
    {
        //les donnees recues ne sont pâs en utf-8 il faut realiser la transformation
        //solution temporaire, à faire passer par generatexml comme c'est le cas pour les depots
        $domDocument = new DOMDocument('1.0', $this->encoding); //Création fichier xml
        //<ListeAffaire>
        // |
           $listeAffaires = $domDocument->createElement('ListeAffaire', '');
        //___ Attributs ListeAffaire
        self::addAttribute($domDocument, $listeAffaires, 'xmlns:xsi', 'http://www.w3.org/2001/XMLSchema-instance');
        self::addAttribute($domDocument, $listeAffaires, 'xsi:noNamespaceSchemaLocation', './MARCO_DEMAT.XSD');
        self::addAttribute($domDocument, $listeAffaires, 'Version', '1.00');
        self::addAttribute($domDocument, $listeAffaires, 'Emetteur', 'ATEXO');
        $domDocument->appendChild($listeAffaires);
        $ordre = 0;
        foreach ($consultationsVo as $consultationVo) {
            $referenceConnecteur = $consultationVo->getReferenceConnecteur();
            $arrayReference = explode('-', $referenceConnecteur);
            $refUtilisateur = $arrayReference[0];
            $afdDos = $arrayReference[count($arrayReference) - 1];
            if ($consultationVo instanceof Atexo_Interfaces_ConsultationVo) {
                $telechargementsVo = $consultationVo->getTelechargement();
                if (is_array($telechargementsVo)) {
                    foreach ($telechargementsVo as $telechargementVo) {
                        ++$ordre;
                        $nomFichier = $telechargementVo->getNomFichier();
                        $dateTelechargement = $telechargementVo->getDateTelechargement();
                        // |___ <MAFWRETD> :
                        $mafwretd = $domDocument->createElement('MAFWRETD', '');
                        $listeAffaires->appendChild($mafwretd);
                        //     |___ Attributs MAFWRETD :
                        //             |
                        //             |___AFG_ORG (max 15 car)
                        self::addAttribute($domDocument, $mafwretd, 'AFG_ORG', $authentificationVo->getAfgOrg());
                        //             |___AFG_NUM (max 18 car)
                        self::addAttribute($domDocument, $mafwretd, 'AFG_NUM', $refUtilisateur);
                        //             |__AFD_DOS
                        self::addAttribute($domDocument, $mafwretd, 'AFD_DOS', $afdDos);
                        //             |___DCU_TYPE
                        self::addAttribute($domDocument, $mafwretd, 'DCU_TYPE', '19');
                        //             |___RTD_DATE (sur 10 caracteres)
                        self::addAttribute($domDocument, $mafwretd, 'RTD_DATE', substr($dateTelechargement, 0, 10));
                        //             |___RTD_HHMN (sur 4 caracteres)
                        self::addAttribute($domDocument, $mafwretd, 'RTD_HHMN', trim(str_replace(':', '', substr($dateTelechargement, 11, 5))));
                        //             |___AFL_LOT
                        self::addAttribute($domDocument, $mafwretd, 'AFL_LOT', ' ');
                        //             |___DCU_LIB (max 250)
                        self::addAttribute($domDocument, $mafwretd, 'DCU_LIB', substr($nomFichier, 0, 254));
                        $entrepriseVo = $telechargementVo->getEntrepriseVo();
                        if (!($entrepriseVo instanceof Atexo_Interfaces_EntrepriseVo)) {
                            $entrepriseVo = new Atexo_Interfaces_EntrepriseVo();
                        }
                        //             |___FOW_COD (10 chiffres)
                        self::addAttribute($domDocument, $mafwretd, 'FOW_COD', $entrepriseVo->getId());
                        //             |___RTD_ORD
                        self::addAttribute($domDocument, $mafwretd, 'RTD_ORD', $ordre);
                        $maffouw = $domDocument->createElement('MAFFOUW', ''); //         |___ <MAFFOUW> :
                        $mafwretd->appendChild($maffouw);
                        $nomEntreprise = $entrepriseVo->getNom();
                        $insritVo = $entrepriseVo->getInscrit();
                        $nomInscrit = $insritVo->getNom();
                        $prenomInscrit = $insritVo->getPrenom();
                        $nomPrenomInscrit = $nomInscrit.' '.$prenomInscrit;
                        $emailInscrit = $insritVo->getEmail();
                        $adresse = $entrepriseVo->getAdresse();
                        $adresse2 = $entrepriseVo->getAdresse2();
                        $adresseEntreprise = $adresse.' '.$adresse2;
                        $codePostal = $entrepriseVo->getCodePostal();
                        $villeEntreprise = $entrepriseVo->getVille();
                        $paysEntreprise = $entrepriseVo->getPays();
                        $siretEntreprise = $entrepriseVo->getSiret();
                        $telephoneEntreprise = $entrepriseVo->getTelephone();
                        //         |___ Attributs MAFFOUW :
                        self::addAttribute($domDocument, $maffouw, 'FOW_COD', $entrepriseVo->getId());
                        self::addAttribute($domDocument, $maffouw, 'FOW_MDP', ' ');
                        //             |___entreprise (max 80 cara)
                        self::addAttribute($domDocument, $maffouw, 'FOW_DEN', Atexo_Util::toUtf8($nomEntreprise));
                        //             |___nom (max 80 cara)
                        self::addAttribute($domDocument, $maffouw, 'FOW_REP', Atexo_Util::toUtf8($nomPrenomInscrit));
                        //             |___email (max 50 cara)
                        self::addAttribute($domDocument, $maffouw, 'FOW_EMAIL', $emailInscrit);
                        //             |___adresse (max 250 cara)
                        self::addAttribute($domDocument, $maffouw, 'FOW_ADR', Atexo_Util::toUtf8($adresseEntreprise));
                        //             |___codepostal(max 15 cara)
                        self::addAttribute($domDocument, $maffouw, 'FOW_CP', $codePostal);
                        //             |___ville(max 50 cara)
                        self::addAttribute($domDocument, $maffouw, 'FOW_VILLE', Atexo_Util::toUtf8($villeEntreprise));
                        //             |___pays(max 80 cara)
                        self::addAttribute($domDocument, $maffouw, 'FOW_PAYS', Atexo_Util::toUtf8($paysEntreprise));
                        //             |___siret
                        self::addAttribute($domDocument, $maffouw, 'FOW_SIRET', $siretEntreprise);
                        //             |dateCreation
                        self::addAttribute($domDocument, $maffouw, 'FOW_DATEC', date('Y').'-'.date('m').'-'.date('d'));
                        //             |___telephone
                        self::addAttribute($domDocument, $maffouw, 'FOW_TEL', $telephoneEntreprise);
                    }
                }
            }
        }

        return $domDocument->saveXML();
    }

    public function addAttribute($domDocument, $masterTag, $attributeName, $attributeContent)
    {
        $attribute = $domDocument->createAttribute($attributeName);
        $masterTag->appendChild($attribute);
        $attributeValue = $domDocument->createTextNode($attributeContent);
        $attribute->appendChild($attributeValue);
    }

    public function createXmlErreursConsultation($xml, array $messageErreur)
    {
        $domDocument = new DOMDocument();
        $domDocument->loadXML($xml);
        $transaction = $domDocument->getElementsByTagName('Transaction')->item(0);
        $control = $transaction->getElementsByTagName('Control')->item(0);

        $domDocument = self::createErrorsTags($domDocument, $control, $messageErreur);
        $xml = $domDocument->saveXML();

        return $xml;
    }

    public function createErrorsTags($domDocument, $tagContainingError, array $messageErreur)
    {
        $errors = $domDocument->createElement('Errors', '');
        $tagContainingError->appendChild($errors);

        foreach ($messageErreur as $codeErreur => $descriptionErreur) {
            $error = $domDocument->createElement('Error', '');
            $errors->appendChild($error);

            $code = $domDocument->createAttribute('code');
            $error->appendChild($code);
            $codeErreurValue = $domDocument->createTextNode($codeErreur);
            $code->appendChild($codeErreurValue);

            $description = $domDocument->createElement('Description', '');
            $error->appendChild($description);

            $content = $domDocument->createElement('Content', '');
            $description->appendChild($content);
            $cdataErreur = $domDocument->createCDATASection($descriptionErreur);
            $content->appendChild($cdataErreur);
        }

        return $domDocument;
    }

    public function createXmlErreurConnecteur($numeroErreur, $libelleErreur)
    {
        $domDocument = new DOMDocument('1.0', $this->encoding);
        $LocalTrustError = $domDocument->createElement('Local_Trust_ERROR', '');
        $domDocument->appendChild($LocalTrustError);
        $error = $domDocument->createElement('ERROR', '');
        $LocalTrustError->appendChild($error);
        self::addAttribute($domDocument, $error, 'NUM_ERROR', $numeroErreur);
        self::addAttribute($domDocument, $error, 'LIB_ERROR', $libelleErreur);

        return $domDocument->saveXML();
    }

    public function generateXmlRegistreDepotSis(Atexo_Interfaces_InterfaceAuthentificationVo $authentificationVo, array $consultationsVo)
    {
        $domDocument = new DOMDocument('1.0', $this->encoding);

        $listeAffaire = $domDocument->createElement('ListeAffaire', '');
        $domDocument->appendChild($listeAffaire);

        $ordre = 0;
        foreach ($consultationsVo as $consultationVo) {
            if ($consultationVo instanceof Atexo_Interfaces_ConsultationVo) {
                $offresVo = $consultationVo->getOffresVo();
                if (is_array($offresVo)) {
                    foreach ($offresVo as $offreVo) {
                        ++$ordre;

                        $mafwrepg = $domDocument->createElement('MAFWREPG', '');
                        $listeAffaire->appendChild($mafwrepg);

                        $entrepriseVo = $offreVo->getEntrepriseVo();
                        if (!$entrepriseVo instanceof Atexo_Interfaces_EntrepriseVo) {
                            $entrepriseVo = new Atexo_Interfaces_EntrepriseVo();
                        }
                        $inscritVo = $entrepriseVo->getInscrit();
                        if (!$inscritVo instanceof Atexo_Interfaces_InscritVo) {
                            $inscritVo = new Atexo_Interfaces_InscritVo();
                        }

                        //                     |___Attributs MAFWREPG :
                        //                                 |__REF_CONS
                        self::addAttribute($domDocument, $mafwrepg, 'REF_CONS', $consultationVo->getReferenceUtilisateur());
                        //                                 |__RPG_DATETIME
                        self::addAttribute($domDocument, $mafwrepg, 'RPG_DATETIME', base64_encode($offreVo->getDateDepot()));
                        //                                 |__FOW_COD
                        self::addAttribute($domDocument, $mafwrepg, 'FOW_COD', $entrepriseVo->getId());
                        //                                 |__RPG_ORD
                        self::addAttribute($domDocument, $mafwrepg, 'RPG_ORD', $ordre);
                        //                     |
                        //                     |__MAFFOUW
                        $maffouw = $domDocument->createElement('MAFFOUW', '');
                        $mafwrepg->appendChild($maffouw);
                        //                         |___Attributs MAFFOUW :
                        //                                 |__FOW_DEN
                        self::addAttribute($domDocument, $maffouw, 'FOW_DEN', base64_encode(utf8_decode($entrepriseVo->getNom())));
                        //                                 |__FOW_PERS
                        self::addAttribute($domDocument, $maffouw, 'FOW_PERS', base64_encode(utf8_decode($inscritVo->getNom().' '.$inscritVo->getPrenom())));
                        //                                 |__FOW_EMAIL
                        self::addAttribute($domDocument, $maffouw, 'FOW_EMAIL', base64_encode(utf8_decode($inscritVo->getEmail())));
                        //                                 |__FOW_ADR
                        self::addAttribute($domDocument, $maffouw, 'FOW_ADR', base64_encode(utf8_decode($entrepriseVo->getAdresse())));
                        //                                 |__FOW_CP
                        self::addAttribute($domDocument, $maffouw, 'FOW_CP', base64_encode($entrepriseVo->getCodePostal()));
                        //                                 |__FOW_VILLE
                        self::addAttribute($domDocument, $maffouw, 'FOW_VILLE', base64_encode(utf8_decode($entrepriseVo->getVille())));
                        //                                 |__FOW_PAYS
                        self::addAttribute($domDocument, $maffouw, 'FOW_PAYS', base64_encode($entrepriseVo->getPays()));
                        //                                 |__FOW_FJUR
                        self::addAttribute($domDocument, $maffouw, 'FOW_FJUR', base64_encode($entrepriseVo->getFormeJuridique()));
                        //                                 |__FOW_COD
                        self::addAttribute($domDocument, $maffouw, 'FOW_COD', $entrepriseVo->getId());
                        //                                 |__FOW_TEL
                        self::addAttribute($domDocument, $maffouw, 'FOW_TEL', base64_encode($entrepriseVo->getTelephone()));
                        //                                 |__FOW_TEL
                        self::addAttribute($domDocument, $maffouw, 'FOW_SIRET', base64_encode($entrepriseVo->getSiret()));

                        $enveloppeVos = $offreVo->getEnveloppeVo();
                        foreach ($enveloppeVos as $enveloppeVo) {
                            $nomNoeud = 'MAFWREPO';
                            //                     |
                            //                     |__MAFWREPO
                            $mafwrep = $domDocument->createElement($nomNoeud, '');
                            $mafwrepg->appendChild($mafwrep);

                            if (!$enveloppeVo instanceof Atexo_Interfaces_EnveloppeVo) {
                                $enveloppeVo = new Atexo_Interfaces_EnveloppeVo();
                            }
                            if ($enveloppeVo->getTypeEnveloppe() === Atexo_Config::getParameter('TYPE_ENV_OFFRE')) {
                                self::addAttribute($domDocument, $mafwrep, 'AFL_LOT', base64_encode($enveloppeVo->getNumeroSousPli()));
                            } else {
                                self::addAttribute($domDocument, $mafwrep, 'AFL_LOT', ' ');
                            }
                            self::addAttribute($domDocument, $mafwrep, 'AFL_LIB', utf8_decode(base64_encode($enveloppeVo->getNomFichier())));
                        }
                    }
                }
            }
        }

        $xmlDepot = $domDocument->saveXML();

        return $xmlDepot;
    }

    public function generateXmlRegistreRetraitSis(Atexo_Interfaces_InterfaceAuthentificationVo $authentificationVo, array $consultationsVo)
    {
        $domDocument = new DOMDocument('1.0', $this->encoding);
        //<ListeAffaire>
        // |
        $listeAffaires = $domDocument->createElement('ListeAffaire', '');

        $domDocument->appendChild($listeAffaires);

        $ordre = 1;

        foreach ($consultationsVo as $consultationVo) {
            $referenceConnecteur = $consultationVo->getReferenceUtilisateur();

            if ($consultationVo instanceof Atexo_Interfaces_ConsultationVo) {
                $telechargementsVo = $consultationVo->getTelechargement();
                if (is_array($telechargementsVo)) {
                    foreach ($telechargementsVo as $telechargementVo) {
                        $nomFichier = $telechargementVo->getNomFichier();
                        $dateTelechargement = $telechargementVo->getDateTelechargement();

                        // |___ <MAFWRETD> :
                        $mafwretd = $domDocument->createElement('MAFWRETD', '');
                        $listeAffaires->appendChild($mafwretd);
                        //     |___ Attributs MAFWRETD :
                        //             |
                        //             |___AFG_ORG (max 15 car)
                        //             |___REF_CONS
                        self::addAttribute($domDocument, $mafwretd, 'REF_CONS', $referenceConnecteur);
                        //             |___DCU_LIB
                        self::addAttribute($domDocument, $mafwretd, 'DCU_LIB', base64_encode(utf8_decode($nomFichier)));
                        //             |___RTD_ORD
                        self::addAttribute($domDocument, $mafwretd, 'RTD_ORD', $ordre++);
                        //             |___RTD_DATETIME
                        self::addAttribute($domDocument, $mafwretd, 'RTD_DATETIME', base64_encode($dateTelechargement));

                        $entrepriseVo = $telechargementVo->getEntrepriseVo();
                        if (!($entrepriseVo instanceof Atexo_Interfaces_EntrepriseVo)) {
                            $entrepriseVo = new Atexo_Interfaces_EntrepriseVo();
                        }
                        //         |___ <MAFFOUW> :
                        $maffouw = $domDocument->createElement('MAFFOUW', '');
                        $mafwretd->appendChild($maffouw);

                        $nomEntreprise = $entrepriseVo->getNom();
                        $insritVo = $entrepriseVo->getInscrit();
                        $nomInscrit = $insritVo->getNom();
                        //$prenomInscrit = $insritVo->getPrenom();
                        //$nomPrenomInscrit = $nomInscrit." ".$prenomInscrit;
                        $emailInscrit = $insritVo->getEmail();
                        $adresse = $entrepriseVo->getAdresse();
                        //$adresse2 = $entrepriseVo->getAdresse2();
                        //$adresseEntreprise = $adresse." ".$adresse2;
                        $codePostal = $entrepriseVo->getCodePostal();
                        $villeEntreprise = $entrepriseVo->getVille();
                        $paysEntreprise = $entrepriseVo->getPays();
                        $siretEntreprise = $entrepriseVo->getSiret();
                        $telephoneEntreprise = $entrepriseVo->getTelephone();

                        //         |___ Attributs MAFFOUW :
                        //             |
                        //             |___entreprise
                        self::addAttribute($domDocument, $maffouw, 'FOW_DEN', base64_encode(utf8_decode($nomEntreprise)));
                        //             |___nom
                        self::addAttribute($domDocument, $maffouw, 'FOW_PERS', base64_encode(utf8_decode($nomInscrit)));
                        //             |___email
                        self::addAttribute($domDocument, $maffouw, 'FOW_EMAIL', base64_encode(utf8_decode($emailInscrit)));
                        //             |___adresse
                        self::addAttribute($domDocument, $maffouw, 'FOW_ADR', base64_encode(utf8_decode($adresse)));
                        //             |___codepostal
                        self::addAttribute($domDocument, $maffouw, 'FOW_CP', base64_encode($codePostal));
                        //             |___ville
                        self::addAttribute($domDocument, $maffouw, 'FOW_VILLE', base64_encode(utf8_decode($villeEntreprise)));
                        //             |___pays
                        self::addAttribute($domDocument, $maffouw, 'FOW_PAYS', base64_encode($paysEntreprise));
                        //             |___siret
                        self::addAttribute($domDocument, $maffouw, 'FOW_SIRET', base64_encode($siretEntreprise));
                        //             |___telephone
                        self::addAttribute($domDocument, $maffouw, 'FOW_TEL', base64_encode($telephoneEntreprise));
                    }
                }
            }
        }

        return $domDocument->saveXML();
    }

    public function generateXmlExportEntrepriseSIS(Atexo_Interfaces_InterfaceAuthentificationVo $authentificationVo, array $consultationsVo)
    {
        //Création fichier xml
        $domDocument = new DOMDocument('1.0', $this->encoding);
        //<ListeAffaire>
        // |
        $listeAffaires = $domDocument->createElement('ListeAffaire', '');
        $domDocument->appendChild($listeAffaires);
        foreach ($consultationsVo as $consultationVo) {
            if ($consultationVo instanceof Atexo_Interfaces_ConsultationVo) {
                $entrepriseVo = $consultationVo->getEntreprise();
                if (is_array($entrepriseVo)) {
                    foreach ($entrepriseVo as $oneEntreprise) {
                        //         |___ <MAFFOUW> :
                        $maffouw = $domDocument->createElement('MAFFOUW', '');
                        $listeAffaires->appendChild($maffouw);
                        $nomEntreprise = $oneEntreprise->getNom();
                        $inscritVo = $oneEntreprise->getInscrit();
                        if (!$inscritVo instanceof Atexo_Interfaces_InscritVo) {
                            $inscritVo = new Atexo_Interfaces_InscritVo();
                        }
                        $nomInscrit = $inscritVo->getNom();
                        $prenomInscrit = $inscritVo->getPrenom();
                        $nomPrenomInscrit = $nomInscrit.' '.$prenomInscrit;
                        $emailInscrit = $inscritVo->getEmail();
                        $adresse = $oneEntreprise->getAdresse();
                        $adresse2 = $oneEntreprise->getAdresse2();
                        $adresseEntreprise = $adresse.' '.$adresse2;
                        $codePostal = $oneEntreprise->getCodePostal();
                        $villeEntreprise = $oneEntreprise->getVille();
                        $paysEntreprise = $oneEntreprise->getPays();
                        $siretEntreprise = $oneEntreprise->getSiret();
                        $telephoneEntreprise = $oneEntreprise->getTelephone();
                        $formeJuridique = $oneEntreprise->getFormeJuridique();

                        //         |___ Attributs MAFFOUW :
                        //             |
                        //             |___FOW_DEN
                        self::addAttribute($domDocument, $maffouw, 'FOW_DEN', base64_encode(utf8_decode($nomEntreprise)));
                        //             |
                        //             |___FOW_PERS
                        self::addAttribute($domDocument, $maffouw, 'FOW_PERS', base64_encode(utf8_decode($nomPrenomInscrit)));
                        //             |
                        //             |___FOW_EMAIL
                        self::addAttribute($domDocument, $maffouw, 'FOW_EMAIL', base64_encode(utf8_decode($emailInscrit)));
                        //             |
                        //             |___FOW_ADR
                        self::addAttribute($domDocument, $maffouw, 'FOW_ADR', base64_encode(utf8_decode($adresseEntreprise)));
                        //             |
                        //             |___FOW_CP
                        self::addAttribute($domDocument, $maffouw, 'FOW_CP', base64_encode($codePostal));
                        //             |
                        //             |___FOW_VILLE
                        self::addAttribute($domDocument, $maffouw, 'FOW_VILLE', base64_encode(utf8_decode($villeEntreprise)));
                        //             |
                        //             |___FOW_PAYS
                        self::addAttribute($domDocument, $maffouw, 'FOW_PAYS', base64_encode(utf8_decode($paysEntreprise)));
                        //             |___FOW_SIRET
                        self::addAttribute($domDocument, $maffouw, 'FOW_SIRET', base64_encode($siretEntreprise));
                        //             |___FOW_TEL
                        self::addAttribute($domDocument, $maffouw, 'FOW_TEL', base64_encode($telephoneEntreprise));
                        //             |__FOW_FJUR
                        self::addAttribute($domDocument, $maffouw, 'FOW_FJUR', base64_encode($formeJuridique));
                    }
                }
            }
        }

        return $domDocument->saveXML();
    }
}
