<?php

namespace Application\Service\Atexo\Interfaces;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonInterfaceTypeProcedure;
use Application\Propel\Mpe\CommonInterfaceTypeProcedurePeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureType;
use DOMDocument;
use Exception;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Interfaces_InterfacesUtilities
{
    public function validate($xml, $returnMessageErreur = false)
    {
        $domDocument = new DOMDocument();
        $domDocument->loadXML($xml);
        try {
            return $domDocument->schemaValidate(Atexo_Config::getParameter('XSD_INTERFACES'));
        } catch (Exception $e) {
            if ($returnMessageErreur) {
                return $e->getMessage();
            } else {
                return false;
            }
        }
    }

    public function getActionFromCommande($commande, $connecteur)
    {
        if ($connecteur == Atexo_Config::getParameter('CONNECTEUR_MARCO')) {
            return match ($commande) {
                Atexo_Config::getParameter('INTERFACE_MARCO_CODE_ACTION_EXPORT_REGISTRES_DEPOTS') => Atexo_Config::getParameter('INTERFACE_NOM_ACTION_EXPORT_REGISTRES_DEPOTS'),
                Atexo_Config::getParameter('INTERFACE_MARCO_CODE_ACTION_EXPORT_REGISTRES_RETRAITS') => Atexo_Config::getParameter('INTERFACE_NOM_ACTION_EXPORT_REGISTRES_RETRAITS'),
                Atexo_Config::getParameter('INTERFACE_MARCO_CODE_ACTION_CREER_CONSULTATIONS') => Atexo_Config::getParameter('INTERFACE_NOM_ACTION_CREER_CONSULTATIONS'),
                Atexo_Config::getParameter('INTERFACE_MARCO_CODE_ACTION_EXPORT_QUESTION') => Atexo_Config::getParameter('INTERFACE_NOM_ACTION_EXPORT_REGISTRES_QUESTIONS'),
                default => 'UNKOWN',
            };
        } elseif ($connecteur == Atexo_Config::getParameter('CONNECTEUR_SIS')) {
            return match ($commande) {
                Atexo_Config::getParameter('INTERFACE_SIS_CODE_ACTION_EXPORT_REGISTRES_DEPOTS') => Atexo_Config::getParameter('INTERFACE_NOM_ACTION_EXPORT_REGISTRES_DEPOTS'),
                Atexo_Config::getParameter('INTERFACE_SIS_CODE_ACTION_EXPORT_REGISTRES_RETRAITS') => Atexo_Config::getParameter('INTERFACE_NOM_ACTION_EXPORT_REGISTRES_RETRAITS'),
                Atexo_Config::getParameter('INTERFACE_SIS_CODE_ACTION_CREER_CONSULTATIONS') => Atexo_Config::getParameter('INTERFACE_NOM_ACTION_CREER_CONSULTATIONS'),
                Atexo_Config::getParameter('INTERFACE_SIS_CODE_ACTION_EXPORT_MODIFIER_CONSULTATIONS') => Atexo_Config::getParameter('INTERFACE_NOM_ACTION_MODIFIER_CONSULTATIONS'),
                Atexo_Config::getParameter('INTERFACE_SIS_CODE_ACTION_EXPORT_QUESTION') => Atexo_Config::getParameter('INTERFACE_NOM_ACTION_EXPORT_REGISTRES_QUESTIONS'),
                Atexo_Config::getParameter('INTERFACE_SIS_CODE_ACTION_EXPORT_ENTREPRISE') => Atexo_Config::getParameter('INTERFACE_NOM_ACTION_EXPORT_ENTREPRISES'),
                default => 'UNKOWN',
            };
        }
    }

    public function getCategorieFromCode($codeCategorie, $connecteur)
    {
        if ($connecteur == Atexo_Config::getParameter('CONNECTEUR_MARCO')) {
            return match ($codeCategorie) {
                Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX') => Atexo_Config::getParameter('PRESTATION_TRAVAUX'),
                Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES') => Atexo_Config::getParameter('PRESTATION_FOURNITURES'),
                Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES') => Atexo_Config::getParameter('PRESTATION_SERVICES'),
                default => '',
            };
        } elseif ($connecteur == Atexo_Config::getParameter('CONNECTEUR_SIS')) {
            return match ($codeCategorie) {
                Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX_SIS') => Atexo_Config::getParameter('PRESTATION_TRAVAUX'),
                Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES_SIS') => Atexo_Config::getParameter('PRESTATION_FOURNITURES'),
                Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES_SIS') => Atexo_Config::getParameter('PRESTATION_SERVICES'),
                default => '',
            };
        }
    }

    public function getIdToCodeForTypeProcedure($code, $reference)
    {
        $suffix = '';
        if ('O' == $reference[1]) {
            $suffix = '-O';
        } elseif ('C' == $reference[1]) {
            $suffix = '-C';
        }
        $abreviation = $code.$suffix;
        $id_type_procedure = (new Atexo_Consultation_ProcedureType())->retrieveIdTypeProcedureByAbreviation($abreviation);

        return $id_type_procedure;
    }

    public function getCategorie($categorie)
    {
        return match ($categorie) {
            'FOURNITURES' => Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES'),
            'TRAVAUX' => Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX'),
            'SERVICES' => Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES'),
            default => 0,
        };
    }

    public function saveLog($logContent, $action, $nomConnecteur, $description = 'erreur')
    {
        /**
         * Arboresence logs :
         *      logs.
                                    |_i.e marco
         */
        $pathLog = Atexo_Config::getParameter('LOG_FILE_INTERFACES');
        $fileName = '';
        switch ($nomConnecteur) {
            case Atexo_Config::getParameter('CONNECTEUR_MARCO'):
                 $pathLog .= Atexo_Config::getParameter('LOG_FILE_INTERFACES_DOSSIER_MARCO');
                                                                  $fileName .= 'xml_'.($description ? $description.'_' : '').'marco_';
                break;
            case Atexo_Config::getParameter('CONNECTEUR_SIS'):
                 $pathLog .= Atexo_Config::getParameter('LOG_FILE_INTERFACES_DOSSIER_SIS');
                                                                  $fileName .= 'xml_'.($description ? $description.'_' : '').'sis_';
                break;
        }

        switch ($action) {
            case Atexo_Config::getParameter('INTERFACE_NOM_ACTION_CREER_CONSULTATIONS'):
                 $pathLog .= Atexo_Config::getParameter('LOG_FILE_INTERFACES_CONSULTATION');
                                                                                                 $fileName .= 'creer_consultation_';
                break;
            case Atexo_Config::getParameter('INTERFACE_NOM_ACTION_MODIFIER_CONSULTATIONS'):
                 $pathLog .= Atexo_Config::getParameter('LOG_FILE_INTERFACES_MODIFICATION_CONSULTATION');
                                                                                                 $fileName .= 'modifier_consultation_';
                break;
            case Atexo_Config::getParameter('INTERFACE_NOM_ACTION_EXPORT_REGISTRES_DEPOTS'):
                 $pathLog .= Atexo_Config::getParameter('LOG_FILE_INTERFACES_REGISTRE_DEPOT');
                                                                                                 $fileName .= 'registre_depot_';
                break;
            case Atexo_Config::getParameter('INTERFACE_NOM_ACTION_EXPORT_REGISTRES_RETRAITS'):
                 $pathLog .= Atexo_Config::getParameter('LOG_FILE_INTERFACES_REGISTRE_RETRAIT');
                                                                                                 $fileName .= 'registre_retrait_';
                break;
            case Atexo_Config::getParameter('INTERFACE_NOM_ACTION_EXPORT_REGISTRES_QUESTIONS'):
                 $pathLog .= Atexo_Config::getParameter('LOG_FILE_INTERFACES_REGISTRE_QUESTIONS');
                                                                                                 $fileName .= 'registre_depot_';
                break;
            case Atexo_Config::getParameter('INTERFACE_NOM_ACTION_EXPORT_ENTREPRISES'):
                 $pathLog .= Atexo_Config::getParameter('LOG_FILE_INTERFACES_REGISTRE_ENTREPRISES');
                                                                                                 $fileName .= 'export_entreprise_';
                break;
            case 'fichier_xml':
                 $pathLog .= Atexo_Config::getParameter('LOG_FILE_INTERFACES_FICHIER_RECUS');
                                 $fileName = 'donnees_'.$fileName;
                break;
            case 'demande_xml':
                 $pathLog .= Atexo_Config::getParameter('LOG_FILE_INTERFACES_FICHIER_RECUS');
                                 $fileName = 'donnees_Authentification_'.$fileName;
                break;
            default:
                 $pathLog .= Atexo_Config::getParameter('LOG_FILE_INTERFACES_ACTIONS_INCONNU');
                      $fileName .= 'action_inconnu';
                break;
        }

        $fileName .= date('Y_m_d-H_i_s').'.xml';

        Atexo_Util::writeLogFile($pathLog, $pathLog.$fileName, $logContent);
    }

    public function getInterfaceTypeProcedureById($idTypeProcedure, $interface)
    {
        $c = new Criteria();
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        if ($interface == Atexo_Config::getParameter('CONNECTEUR_MARCO')) {
            $c->add(CommonInterfaceTypeProcedurePeer::TYPE_PROCEDURE_MARCO, $idTypeProcedure);
        } elseif ($interface == Atexo_Config::getParameter('CONNECTEUR_SIS')) {
            $c->add(CommonInterfaceTypeProcedurePeer::TYPE_PROCEDURE_SIS, $idTypeProcedure);
        }
        $interfaceTypeProcedure = CommonInterfaceTypeProcedurePeer::doSelectOne($c, $connexion);
        if ($interfaceTypeProcedure instanceof CommonInterfaceTypeProcedure) {
            return $interfaceTypeProcedure->getTypeProcedure();
        } else {
            return false;
        }
    }
}
