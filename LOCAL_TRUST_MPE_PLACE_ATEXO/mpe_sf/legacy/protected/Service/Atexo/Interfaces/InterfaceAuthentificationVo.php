<?php

namespace Application\Service\Atexo\Interfaces;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Interfaces_InterfaceAuthentificationVo
{
    private $_login;
    private $_passWord;
    private $_email;
    private string $_purpose = 'REQUETE';
    private $_action;
    private $_afgOrg;
    private $_reference;

    public function setLogin($value)
    {
        $this->_login = $value;
    }

    public function getLogin()
    {
        return $this->_login;
    }

    public function setPassWord($value)
    {
        $this->_passWord = $value;
    }

    public function getPassWord()
    {
        return $this->_passWord;
    }

    public function setEmail($value)
    {
        $this->_email = $value;
    }

    public function getEmail()
    {
        return $this->_email;
    }

    public function setPurpose($value)
    {
        $this->_purpose = $value;
    }

    public function getPurpose()
    {
        return $this->_purpose;
    }

    public function setAction($value)
    {
        $this->_action = $value;
    }

    public function getAction()
    {
        return $this->_action;
    }

    public function setAfgOrg($value)
    {
        $this->_afgOrg = $value;
    }

    public function getAfgOrg()
    {
        return $this->_afgOrg;
    }

    public function getReference()
    {
        return $this->_reference;
    }

    public function setReference($value)
    {
        $this->_reference = $value;
    }
}
