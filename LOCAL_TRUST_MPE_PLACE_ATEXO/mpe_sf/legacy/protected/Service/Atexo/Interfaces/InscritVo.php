<?php

namespace Application\Service\Atexo\Interfaces;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Interfaces_InscritVo
{
    private $_nom;
    private $_prenom;
    private $_email;

    public function getNom()
    {
        return $this->_nom;
    }

    public function setNom($value)
    {
        $this->_nom = $value;
    }

    public function getPrenom()
    {
        return $this->_prenom;
    }

    public function setPrenom($value)
    {
        $this->_prenom = $value;
    }

    public function getEmail()
    {
        return $this->_email;
    }

    public function setEmail($value)
    {
        $this->_email = $value;
    }
}
