<?php

namespace Application\Service\Atexo\Interfaces;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Interfaces_ConsultationVo
{
    private $referenceUtilisateur;
    private $referenceConnecteur;
    private $typeAcces;
    private $categorie;
    private $dateFin;
    private $intitule;
    private $objet;
    private array $colLots;
    private $phaseAcces;
    private array $colOffres = [];
    private $url;
    private $avis;
    private $dateMiseenligne;
    private $acronymeOrganisme;
    private $organisme;

    public function getOrganisme()
    {
        return $this->organisme;
    }

    public function setOrganisme($organisme)
    {
        $this->organisme = $organisme;
    }

    private $lieuxExecution;
    private array $colTelechargements = [];
    private array $colQuestions = [];
    private array $colEntreprises = [];

    public function setReferenceUtilisateur($value)
    {
        $this->referenceUtilisateur = $value;
    }

    public function getReferenceUtilisateur()
    {
        return $this->referenceUtilisateur;
    }

    public function setReferenceConnecteur($value)
    {
        $this->referenceConnecteur = $value;
    }

    public function getReferenceConnecteur()
    {
        return $this->referenceConnecteur;
    }

    public function setTypeAcces($value)
    {
        $this->typeAcces = $value;
    }

    public function getTypeAcces()
    {
        return $this->typeAcces;
    }

    public function setCategorie($value)
    {
        $this->categorie = $value;
    }

    public function getCategorie()
    {
        return $this->categorie;
    }

    public function setDateFin($value)
    {
        $this->dateFin = $value;
    }

    public function getDateFin()
    {
        return $this->dateFin;
    }

    public function setIntitule($value)
    {
        $this->intitule = $value;
    }

    public function getIntitule()
    {
        return $this->intitule;
    }

    public function setObjet($value)
    {
        $this->objet = $value;
    }

    public function getObjet()
    {
        return $this->objet;
    }

    public function getPhaseAcces()
    {
        return $this->phaseAcces;
    }

    public function setPhaseAcces($value)
    {
        $this->phaseAcces = $value;
    }

    public function addLot($numero, $categorie, $libelle, $description)
    {
        $lot = [];
        $lot['numero'] = $numero;
        $lot['categorie'] = $categorie;
        $lot['libelle'] = $libelle;
        $lot['description'] = $description;
        $this->colLots[] = $lot;
    }

    public function getLots()
    {
        return $this->colLots;
    }

    public function addOffreVo(Atexo_Interfaces_OffreVo $offreVo)
    {
        $this->colOffres[] = $offreVo;
    }

    public function setOffresVo(array $offresVo)
    {
        $this->colOffres = $offresVo;
    }

    public function getOffresVo()
    {
        return $this->colOffres;
    }

    public function getUrl()
    {
        return $this->url;
    }

    public function setUrl($Url)
    {
        $this->url = $Url;
    }

    public function getAvis()
    {
        return $this->avis;
    }

    public function setAvis($avis)
    {
        $this->avis = $avis;
    }

    public function getDatemiseenligne()
    {
        return $this->dateMiseenligne;
    }

    public function setDatemiseenligne($dateMiseenligne)
    {
        $this->dateMiseenligne = $dateMiseenligne;
    }

    public function getAcronymeOrganisme()
    {
        return $this->acronymeOrganisme;
    }

    public function setAcronymeOrganisme($acronymeOrganisme)
    {
        $this->acronymeOrganisme = $acronymeOrganisme;
    }

    public function getTelechargement()
    {
        return $this->colTelechargements;
    }

    public function setTelechargement(array $telechargements)
    {
        $this->colTelechargements = $telechargements;
    }

    public function getLieuxExecution()
    {
        return $this->lieuxExecution;
    }

    public function setLieuxExecution($lieuxExecution)
    {
        $this->lieuxExecution = $lieuxExecution;
    }

    public function getQuestion()
    {
        return $this->colQuestions;
    }

    public function setQuestion(array $question)
    {
        $this->colQuestions = $question;
    }

    public function getEntreprise()
    {
        return $this->colEntreprises;
    }

    public function setEntreprise(array $entreprises)
    {
        $this->colEntreprises = $entreprises;
    }
}
