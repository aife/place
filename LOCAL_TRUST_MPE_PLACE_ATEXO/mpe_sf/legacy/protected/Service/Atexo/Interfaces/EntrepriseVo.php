<?php

namespace Application\Service\Atexo\Interfaces;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Interfaces_EntrepriseVo
{
    private $_id;
    private $_nom;
    private $_adresse;
    private $_adresse2;
    private $_codePostal;
    private $_ville;
    private $_pays;
    private $_telephone;
    private $_siret;
    private $_formeJuridique;
    private ?\Application\Service\Atexo\Interfaces\Atexo_Interfaces_InscritVo $_inscritVo = null;

    public function getId()
    {
        return $this->_id;
    }

    public function setId($value)
    {
        $this->_id = $value;
    }

    public function getNom()
    {
        return $this->_nom;
    }

    public function setNom($value)
    {
        $this->_nom = $value;
    }

    public function getAdresse()
    {
        return $this->_adresse;
    }

    public function setAdresse($value)
    {
        $this->_adresse = $value;
    }

    public function getAdresse2()
    {
        return $this->_adresse2;
    }

    public function setAdresse2($value)
    {
        $this->_adresse2 = $value;
    }

    public function getCodePostal()
    {
        return $this->_codePostal;
    }

    public function setCodePostal($value)
    {
        $this->_codePostal = $value;
    }

    public function getVille()
    {
        return $this->_ville;
    }

    public function setVille($value)
    {
        $this->_ville = $value;
    }

    public function getPays()
    {
        return $this->_pays;
    }

    public function setPays($value)
    {
        $this->_pays = $value;
    }

    public function getTelephone()
    {
        return $this->_telephone;
    }

    public function setTelephone($value)
    {
        $this->_telephone = $value;
    }

    public function getSiret()
    {
        return $this->_siret;
    }

    public function setSiret($value)
    {
        $this->_siret = $value;
    }

    public function getFormeJuridique()
    {
        return $this->_formeJuridique;
    }

    public function setFormeJuridique($value)
    {
        $this->_formeJuridique = $value;
    }

    public function getInscrit()
    {
        return $this->_inscritVo;
    }

    public function setInscrit(Atexo_Interfaces_InscritVo $value)
    {
        $this->_inscritVo = $value;
    }
}
