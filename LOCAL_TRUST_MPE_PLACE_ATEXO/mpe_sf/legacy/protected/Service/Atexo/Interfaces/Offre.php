<?php

namespace Application\Service\Atexo\Interfaces;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonOffresPeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use DOMDocument;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Interfaces_Offre
{
    private $_transaction = null;

    public function initInterface()
    {
        $this->_transaction = null;
    }

    public function getOffresVoFromXmlConsultation($xml, $organisme)
    {
        $interfaceConsultation = new Atexo_Interfaces_Consultation();

        $consultationVo = $interfaceConsultation->createConsultationVoFromXml($xml);
        $consultationVo = array_shift($consultationVo);
        if (!$consultationVo instanceof Atexo_Interfaces_ConsultationVo) {
            $consultationVo = new Atexo_Interfaces_ConsultationVo();
        }

        $consultationId = $interfaceConsultation->getIdConsultationByRefCon($xml);

        $offresVo = [];

        if ($consultationId) {
            $identifiantOffre = 1;
            $offresOrg = (new Atexo_Interfaces_Offre())->getOrgOffreByRefConsultation($consultationId, Atexo_CurrentUser::getCurrentOrganism());
            if (is_array($offresOrg)) {
                foreach ($offresOrg as $oneOrg) {
                    $offreVo = new Atexo_Interfaces_OffreVo();
                    $offreVo->setDateDepot(str_replace('T', ' ', $oneOrg->getUntrusteddate()));
                    $offreVo->setIdentifiantOffre($identifiantOffre++);

                    $enveloppesVo = (new Atexo_Interfaces_Enveloppe())->getEnveloppeVoByIdOffre($oneOrg->getId(), $organisme);
                    $offreVo->setEnveloppeVo($enveloppesVo);

                    $entrepriseVo = (new Atexo_Interfaces_Entreprise())->getEntrepriseVoByOffre($oneOrg);

                    $inscritVo = (new Atexo_Interfaces_Inscrit())->getInscritVoByRegistre($oneOrg);
                    $entrepriseVo->setInscrit($inscritVo);

                    $offreVo->setEntrepriseVo($entrepriseVo);

                    $offresVo[] = $offreVo;
                }
            }
        }

        $consultationVo->setOffresVo($offresVo);

        $consultations = [];
        $consultations[] = $consultationVo;

        return $consultations;
    }

    public function getOffreVoFromXmlRegistreDepot($xml)
    {
        $xml = str_ireplace('UTF-8', 'ISO-8859-1', $xml);

        $domDocument = new DOMDocument();
        $domDocument->loadXML($xml);
        $transaction = $domDocument->documentElement;

        $message = $transaction->getElementsByTagName('Message')->item(0);
        $tenderDefinitionTag = $message->getElementsByTagName('TenderDefinition')->item(0);

        $registreOffres = $tenderDefinitionTag->getElementsByTagName('RegistreOffres')->item(0);
        $offres = $registreOffres->getElementsByTagName('Offre');

        $offresVo = [];

        foreach ($offres as $offre) {
            $offreVo = new Atexo_Interfaces_OffreVo();
            $offreVo->setDateDepot($offre->getAttribute('dateDepot'));
            $offreVo->setIdentifiantOffre($offre->getAttribute('identifiantOffre'));

            $enveloppes = $offre->getElementsByTagName('Enveloppe');
            $enveloppesVo = (new Atexo_Interfaces_Enveloppe())->getEnveloppesVoFromTagEnveloppe($enveloppes);
            $offreVo->setEnveloppeVo($enveloppesVo);

            $entreprises = $offre->getElementsByTagName('Entreprise');
            $entreprisesVo = (new Atexo_Interfaces_Entreprise())->getEntrepriseVoFromTagEntreprise($entreprises);
            $entrepriseVo = array_shift($entreprisesVo);
            if ($entrepriseVo instanceof Atexo_Interfaces_EntrepriseVo) {
                $offreVo->setEntrepriseVo($entrepriseVo);
            }

            $offresVo[] = $offreVo;
        }

        return $offresVo;
    }

    public function getOrgOffreByRefConsultation($consultationId, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));

        $c = new Criteria();
        $c->add(CommonOffresPeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonOffresPeer::ORGANISME, $organisme);
        $offreOrg = CommonOffresPeer::doSelect($c, $connexion);

        return $offreOrg;
    }

    public function getOffreByRefConsultation($consultationId, $organisme, $connexion = null)
    {
        if (!$connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
        $c = new Criteria();
        $c->add(CommonOffresPeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonOffresPeer::ORGANISME, $organisme);
        $offres = CommonOffresPeer::doSelect($c, $connexion);
        if ($offres) {
            return $offres;
        } else {
            return [];
        }
    }

    public function getOffreByIdOffre($idOffre, $connexion = null)
    {
        if (!$connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
        $c = new Criteria();
        $c->add(CommonOffresPeer::ID, $idOffre);
        $offre = CommonOffresPeer::doSelectone($c, $connexion);
        if ($offre) {
            return $offre;
        } else {
            return [];
        }
    }
}
