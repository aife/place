<?php

namespace Application\Service\Atexo\Interfaces;

/**
 * commentaires.
 *
 * @author Othmane ABISOUROUR <othmane.abisourour@atexo.com>
 * @copyright Atexo 2009
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Interfaces_QuestionVo
{
    private $_dateQuestion;
    private $_contenu;
    private $_dateReponse;
    private $_nomFichier;
    private ?\Application\Service\Atexo\Interfaces\Atexo_Interfaces_EntrepriseVo $_entrepriseVo = null;

    public function setConsultationVo(Atexo_Interfaces_ConsultationVo $consultationVo)
    {
        $this->_consultationVo = $consultationVo;
    }

    public function getConsultationVo()
    {
        return $this->_consultationVo;
    }

    public function setDateQuestion($value)
    {
        $this->_dateQuestion = $value;
    }

    public function getDateQuestion()
    {
        return $this->_dateQuestion;
    }

    public function setContenu($value)
    {
        $this->_contenu = $value;
    }

    public function getContenu()
    {
        return $this->_contenu;
    }

    public function setDateReponse($value)
    {
        $this->_dateReponse = $value;
    }

    public function getDateReponse()
    {
        return $this->_dateReponse;
    }

    public function setNomFichier($value)
    {
        $this->_nomFichier = $value;
    }

    public function getNomFichier()
    {
        return $this->_nomFichier;
    }

    public function setEntrepriseVo(Atexo_Interfaces_EntrepriseVo $entrepriseVo)
    {
        $this->_entrepriseVo = $entrepriseVo;
    }

    public function getEntrepriseVo()
    {
        return $this->_entrepriseVo;
    }
}
