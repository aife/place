<?php

namespace Application\Service\Atexo\Interfaces;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonCategorieLot;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonProcedureEquivalencePeer;
use Application\Propel\Mpe\CommonTypeProcedureOrganisme;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_UserVo;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureType;
use DOMDocument;
use PDO;
use Prado\Prado;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Interfaces_Consultation
{
    private ?\DOMElement $_transaction = null;

    public function initInterface()
    {
        $this->response = Prado::getApplication()->getResponse();
        $this->_transaction = null;
    }

    public function createConsultationObjectFromXml($xml, $organisme)
    {
        $consultationsVo = $this->createConsultationVoFromXml($xml);
        $consultations = [];
        if (is_array($consultationsVo)) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            foreach ($consultationsVo as $consultationVo) {
                $consultation = new CommonConsultation();
                $consultation->setOrganisme($organisme);
                $abreviationInterfaceTypeProcedure = $consultationVo->getTypeAcces();
                $typeProcedureObject = (new Atexo_Consultation_ProcedureType())->retrieveTypeProcedureObjectByAbreviationInterface($abreviationInterfaceTypeProcedure, $organisme);
                if ($typeProcedureObject instanceof CommonTypeProcedureOrganisme) {
                    $typePocedure = $typeProcedureObject->getIdTypeProcedure();
                    $typeValidation = $typeProcedureObject->getIdTypeValidation();
                    $c = new Criteria();
                    $c->add(CommonProcedureEquivalencePeer::ID_TYPE_PROCEDURE, $typePocedure);
                    $c->add(CommonProcedureEquivalencePeer::ORGANISME, $organisme);
                    $procedureEquivalence = CommonProcedureEquivalencePeer::doSelectOne($c, $connexion);
                } else {
                    $typePocedure = 0;
                    $typeValidation = 0;
                    $procedureEquivalence = Atexo_Config::getProcedureEquivalenceDefault();
                }
                $consultation->setIdTypeProcedureOrg($typePocedure);
                if ('1' == substr($procedureEquivalence->getMiseEnLigne1(), -1, 1)) {
                    $consultation->setRegleMiseEnLigne('1');
                } elseif ('1' == substr($procedureEquivalence->getMiseEnLigne2(), -1, 1)) {
                    $consultation->setRegleMiseEnLigne('2');
                } elseif ('1' == substr($procedureEquivalence->getMiseEnLigne2(), -1, 1)) {
                    $consultation->setRegleMiseEnLigne('3');
                } elseif ('1' == substr($procedureEquivalence->getMiseEnLigne2(), -1, 1)) {
                    $consultation->setRegleMiseEnLigne('4');
                }
                if ('1' == substr($procedureEquivalence->getProcedurePublicite(), -1, 1)) {
                    $consultation->setTypeAcces('1');
                    $consultation->setNumProcedure('1');
                } elseif ('1' == substr($procedureEquivalence->getProcedureRestreinte(), -1, 1)) {
                    $consultation->setTypeAcces('2');
                    $consultation->setNumProcedure('2');
                }
                if ('1' == substr($procedureEquivalence->getCipherEnabled(), -1, 1)) {
                    $consultation->setChiffrementOffre('1');
                } elseif ('1' == substr($procedureEquivalence->getCipherDisabled(), -1, 1)) {
                    $consultation->setChiffrementOffre('0');
                }
                if ('1' == substr($procedureEquivalence->getSignatureEnabled(), -1, 1)) {
                    $consultation->setSignatureOffre('1');
                } elseif ('1' == substr($procedureEquivalence->getSignatureDisabled(), -1, 1)) {
                    $consultation->setSignatureOffre('0');
                }
                if ('1' == substr($procedureEquivalence->getElecResp(), -1, 1)) {
                    $consultation->setAutoriserReponseElectronique('1');
                } elseif ('1' == substr($procedureEquivalence->getNoElecResp(), -1, 1)) {
                    $consultation->setAutoriserReponseElectronique('0');
                }
                if ('1' == substr($procedureEquivalence->getRepObligatoire(), -1, 1)) {
                    $consultation->setReponseObligatoire('1');
                } elseif ('1' == substr($procedureEquivalence->getNoRepObligatoire(), -1, 1)) {
                    $consultation->setReponseObligatoire('0');
                }
                $consultation->setServiceId(Atexo_CurrentUser::getCurrentServiceId());
                $consultation->setEnvCandidature(substr($procedureEquivalence->getEnvCandidature(), -1, 1));
                $consultation->setEnvOffre(substr($procedureEquivalence->getEnvOffre(), -1, 1));
                $consultation->setEnvAnonymat(substr($procedureEquivalence->getEnvAnonymat(), -1, 1));
                $consultation->setEnvOffreTechnique(substr($procedureEquivalence->getEnvOffreTechnique(), -1, 1));
                $consultation->setIdTypeAvis(Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION'));
                $consultation->setPartialDceDownload(substr($procedureEquivalence->getPartialDceDownload(), -1, 1));
                $tiragePlan = substr($procedureEquivalence->getTireurPlanPapier(), -1, 1);
                if ('1' == substr($procedureEquivalence->getTireurPlanPapier(), -1, 1)) {
                    $consultation->setTiragePlan($tiragePlan);
                }
                if ('1' == substr($procedureEquivalence->getTireurPlanCdrom(), -1, 1)) {
                    $consultation->setTiragePlan($tiragePlan + 2);
                }
                $consultation->setTirageDescriptif(substr($procedureEquivalence->getConstitutionDossierReponse(), -1, 1));
                $consultation->setTireurPlan(substr($procedureEquivalence->getConstitutionDossierReponse(), -1, 1));
                $consultation->setPoursuivreAffichage(substr($procedureEquivalence->getDelaiDateLimiteRemisePli(), -1, 1));
                $consultation->setIdRegleValidation($typeValidation);
                $consultation->setIdCreateur(Atexo_CurrentUser::getIdServiceAgentConnected());
                $consultation->setNomCreateur(Atexo_CurrentUser::getFirstNameAgentConnected());
                $consultation->setPrenomCreateur(Atexo_CurrentUser::getLastNameAgentConnected());
                $consultation->setSignatureActeEngagement(substr($procedureEquivalence->getSignaturePropre(), -1, 1));
                $consultation->setSignatureActeEngagement(substr($procedureEquivalence->getSignaturePropre(), -1, 1));
                $consultation->setPoursuivreAffichage(substr($procedureEquivalence->getDelaiDateLimiteRemisePli(), -1, 1));
                $consultation->setTypeDecisionARenseigner((('+0' === $procedureEquivalence->getTypeDecisionARenseigner()) ? '1' : '0'));
                $consultation->setTypeDecisionAttributionMarche((('+0' === $procedureEquivalence->getTypeDecisionAttributionMarche()) ? '1' : '0'));
                $consultation->setTypeDecisionDeclarationSansSuite((('+0' === $procedureEquivalence->getTypeDecisionDeclarationSansSuite()) ? '1' : '0'));
                $consultation->setTypeDecisionDeclarationInfructueux((('+0' === $procedureEquivalence->getTypeDecisionDeclarationInfructueux()) ? '1' : '0'));
                $consultation->setTypeDecisionSelectionEntreprise((('+0' === $procedureEquivalence->getTypeDecisionSelectionEntreprise()) ? '1' : '0'));
                $consultation->setTypeDecisionAttributionAccordCadre((('+0' === $procedureEquivalence->getTypeDecisionAttributionAccordCadre()) ? '1' : '0'));
                $consultation->setTypeDecisionAdmissionSad((('+0' === $procedureEquivalence->getTypeDecisionAdmissionSad()) ? '1' : '0'));
                $consultation->setTypeDecisionAutre((('+0' === $procedureEquivalence->getTypeDecisionAutre()) ? '1' : '0'));
                $consultation->setReferenceUtilisateur($consultationVo->getReferenceUtilisateur());
                $consultation->setReferenceConnecteur($consultationVo->getReferenceConnecteur());
                $consultation->setDatefin(str_replace('T', '', $consultationVo->getDateFin()));
                $categorie = (new Atexo_Interfaces_InterfacesUtilities())->getCategorie($consultationVo->getCategorie());
                $consultation->setCategorie($categorie);
                $consultation->setIntitule($consultationVo->getIntitule());
                $consultation->setObjet($consultationVo->getObjet());
                $consultation->setAlloti('0');
                $lots = $consultationVo->getLots();
                if ($lots) {
                    $consultation->setAlloti('1');
                    foreach ($lots as $lot) {
                        $lotCons = new CommonCategorieLot();
                        $lotCons->setOrganisme($organisme);
                        $lotCons->setLot($lot['numero']);
                        $lotCons->setCategorie($lot['categorie']);
                        $lotCons->setDescription(utf8_decode($lot['libelle']));
                        $lotCons->setDescriptionDetail(utf8_decode($lot['description']));
                        $consultation->addCommonCategorieLot($lotCons);
                    }
                }
                $consultations[] = $consultation;
            }
        }

        return $consultations;
    }

    public function createConsultationVoFromXml($xml)
    {
        $xml = Atexo_Util::toUtf8($xml);
        if (null === $this->_transaction) {
            $domDocument = new DOMDocument();
            $domDocument->loadXML($xml);
            $this->_transaction = $domDocument->documentElement;
        }

        $consultationsVo = [];

        $message = $this->_transaction->getElementsByTagName('Message')->item(0);
        $tenders = $message->getElementsByTagName('Tender');

        //dans ce cas on cree la consultation a partir de tender
        if (($message->getAttribute('MessageAction') == Atexo_Config::getParameter('INTERFACE_NOM_ACTION_CREER_CONSULTATIONS'))
               || $message->getAttribute('MessageAction') == Atexo_Config::getParameter('INTERFACE_NOM_ACTION_MODIFIER_CONSULTATIONS')) {
            foreach ($tenders as $tender) {
                $consultation = new Atexo_Interfaces_ConsultationVo();

                $tag = $tender->getElementsByTagName('Intitule')->item(0);
                $tag = $tag->getElementsByTagName('Description')->item(0);
                $tag = $tag->getElementsByTagName('Content')->item(0);
                $titre = $tag->nodeValue;

                $tag = $tender->getElementsByTagName('Objet')->item(0);
                $tag = $tag->getElementsByTagName('Description')->item(0);
                $tag = $tag->getElementsByTagName('Content')->item(0);
                $objet = $tag->nodeValue;

                $tag = $tender->getElementsByTagName('URL')->item(0);
                $tag = $tag->getElementsByTagName('Description')->item(0);
                $tag = $tag->getElementsByTagName('Content')->item(0);
                $url = $tag->nodeValue;

                $consultation->setReferenceUtilisateur($tender->getAttribute('Reference'));
                $consultation->setReferenceConnecteur($tender->getAttribute('ReferenceConnecteur'));
                $consultation->setTypeAcces($tender->getAttribute('TenderProcessType'));
                $consultation->setPhaseAcces($tender->getAttribute('AccessPhase'));
                $consultation->setCategorie($tender->getAttribute('Categorie'));
                $consultation->setAcronymeOrganisme($tender->getAttribute('Acronym'));
                $consultation->setDateFin(str_replace('T', ' ', $tender->getAttribute('ClosureDate')));
                $consultation->setDatemiseenligne(date('Y-m-d H:i:s', strtotime($tender->getAttribute('PubDate'))));
                $consultation->setIntitule(utf8_decode($titre));
                $consultation->setObjet(utf8_decode($objet));
                $consultation->setLieuxExecution($tender->getAttribute('Delivery'));
                $consultation->setUrl($url);
                $consultation->setAvis($tender->getAttribute('TenderType'));

                $lots = $tender->getElementsByTagName('Lots')->item(0);
                if ($lots) {
                    $lots = $lots->getElementsByTagName('Lot');
                    foreach ($lots as $baliseLot) {
                        $numeroLot = $baliseLot->getAttribute('Numero');
                        if ($numeroLot) {
                            $categorieLot = $baliseLot->getAttribute('Categorie');
                            $categorieLot = (new Atexo_Interfaces_InterfacesUtilities())->getCategorie($categorieLot);

                            $tag = $baliseLot->getElementsByTagName('Intitule')->item(0);
                            $tag = $tag->getElementsByTagName('Description')->item(0);
                            $tag = $tag->getElementsByTagName('Content')->item(0);
                            $titreLot = utf8_decode($tag->nodeValue);

                            $tag = $baliseLot->getElementsByTagName('Objet')->item(0);
                            $tag = $tag->getElementsByTagName('Description')->item(0);
                            $tag = $tag->getElementsByTagName('Content')->item(0);
                            $objetLot = utf8_decode($tag->nodeValue);

                            $consultation->addLot($numeroLot, $categorieLot, $titreLot, $objetLot);
                        }
                    }
                }

                $consultationsVo[] = $consultation;
            }
        } else {
            $control = $this->_transaction->getElementsByTagName('Control')->item(0);
            $froms = $control->getElementsByTagName('From');
            foreach ($froms as $from) {
                $consultation = new Atexo_Interfaces_ConsultationVo();
                $consultation->setReferenceConnecteur($from->getAttribute('reference'));
                $consultationsVo[] = $consultation;
            }
        }

        return $consultationsVo;
    }

    public function getUserVoFromXml($xml)
    {
        if (null === $this->_transaction) {
            $domDocument = new DOMDocument();
            $domDocument->loadXML($xml);
            $this->_transaction = $domDocument->documentElement;
        }

        $control = $this->_transaction->getElementsByTagName('Control')->item(0);
        $from = $control->getElementsByTagName('From')->item(0);

        $login = $from->getAttribute('login');
        $passWord = $from->getAttribute('password');
        $email = $from->getAttribute('email');

        if (!$passWord && !$email) {
            return false;
        }

        $sql = "Select  `login`,  `password`,  `email` from Agent where `login`='".(new Atexo_Db())->quote($login)."' AND ";
        if ($passWord) {
            $passwordHashed = hash('sha256', Atexo_Config::getParameter('SALT_POUR_MOT_DE_PASSE').$passWord);
            $sql .= " ( ( `type_hash`='SHA1' AND `password`=sha1('".(new Atexo_Db())->quote($passWord)."') ) OR  ( `type_hash`='SHA256' AND `password`='".(new Atexo_Db())->quote($passwordHashed)."' )) ";
        }
        if ($email) {
            $sql .= " `email`='".(new Atexo_Db())->quote($email)."'";
        }

        $statement = Atexo_Db::getLinkCommon(true)->query($sql);
        if ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            $userVo = new Atexo_UserVo();
            $action = $this->getMessageActionFromXml($xml);
            if (Atexo_Module::isEnabled('SocleExternePpp')) {
                if ('CREER_CONSULTATION' == $action || 'MODIFIER_CONSULTATION' == $action) {
                    $headersArray = array_change_key_case(getallheaders(), CASE_UPPER);
                    if ((isset($headersArray['externalId']) && isset($headersArray['userType'])) || (isset($headersArray['EXTERNALID']) && isset($headersArray['USERTYPE']))) {
                        $externalId = $headersArray['externalId'] ?? $headersArray['EXTERNALID'];
                        $agent = (new Atexo_Agent())->retrieveAgentByIdExterne($externalId);
                        if (strtoupper($agent->getLogin()) == strtoupper($login)) {
                            $userVo->setLogin($agent->getLogin());
                            $userVo->setPassword('PPP');
                            $userVo->setType('agent');
                            $userVo->setAuthenticatePpp(true);
                        }
                    } else {
                        $this->response->redirect('index.php?page=Agent.AuthentificationProxyForMarcoAndSISForPPPEB');
                    }
                } else {
                    if ('' != $passWord && '' != $login) {
                        $urlgoto = Atexo_Config::getParameter('PF_URL_REFERENCE').'index.php?page=Agent.TestAuthentificationProxyForMarcoAndSISForPPPEB';
                        $pathretour = Atexo_Config::getParameter('COMMON_TMP').session_id().date('Yms').'_cookie.html';
                        $cmd = 'wget --no-check-certificate --output-document='.$pathretour." '".Atexo_Config::getParameter('URL_PPP_OPENSSO_LOGIN')."'?IDToken1=".$login.'&IDToken2='.$passWord.'&IDButton=Connexion&goto='.$urlgoto."&gotoOnFail=&encoded=false'";
                        system($cmd, $res);
                        if ('0' != $res) {
                            $contentFile = file_get_contents($pathretour);
                            if ('' != $contentFile && 'FAIL' != $contentFile) {
                                $headersArray = explode('#', $contentFile);
                                $externalId = $headersArray[0];
                                $userType = $headersArray[1];
                                if ('' != $externalId && 'AGENT' == $userType) {
                                    $agent = (new Atexo_Agent())->retrieveAgentByIdExterne($externalId);
                                    if ($agent) {
                                        $userVo->setLogin($login);
                                        $userVo->setPassword('PPP');
                                        $userVo->setType('agent');
                                        $userVo->setAuthenticatePpp(true);
                                    }
                                }
                            } else {
                                return false;
                            }
                        }
                        @unlink($pathretour);
                    }
                }
            } else {
                $userVo->setLogin($row['login']);
                $userVo->setPassword($row['password']);
                $userVo->setType('agent');
            }

            return $userVo;
        } else {
            return false;
        }
    }

    public function getMessageActionFromXml($xml)
    {
        $xml = Atexo_Util::toUtf8($xml);
        if (null === $this->_transaction) {
            $domDocument = new DOMDocument();
            $domDocument->loadXML($xml);
            $this->_transaction = $domDocument->documentElement;
        }

        $message = $this->_transaction->getElementsByTagName('Message')->item(0);
        $action = $message->getAttribute('MessageAction');

        return $action;
    }

    public function getIdConsultationByRefCon($xml)
    {
        if (null === $this->_transaction) {
            $domDocument = new DOMDocument();
            $domDocument->loadXML($xml);
            $this->_transaction = $domDocument->documentElement;
        }

        /*$message= $this->_transaction->getElementsByTagName('Message')->item(0);
        $tender= $message->getElementsByTagName('Tender')->item(0);

        $refCons=$tender->getAttribute("ReferenceConnecteur");
        */

        $message = $this->_transaction->getElementsByTagName('Control')->item(0);
        $tender = $message->getElementsByTagName('From')->item(0);
        $refCons = $tender->getAttribute('reference');
        $consultation = (new Atexo_Consultation())->getConsultationByReferenceConnecteur($refCons, Atexo_CurrentUser::getCurrentOrganism());
        if ($consultation) {
            $reference = $consultation->getId();
            $criteriaVo = new Atexo_Consultation_CriteriaVo();
            $criteriaVo->setIdReference($reference);
            $criteriaVo->setIdService(Atexo_CurrentUser::getCurrentServiceId());
            $criteriaVo->setConnectedAgentId(Atexo_CurrentUser::getId());
            $criteriaVo->setAcronymeOrganisme(Atexo_CurrentUser::getCurrentOrganism());
            $arrayConsultation = (new Atexo_Consultation())->search($criteriaVo);
            if (is_array($arrayConsultation) && $arrayConsultation[0] instanceof CommonConsultation) {
                return $reference;
            }
        }

        return false;
    }

    public function getDateClotureFromXml($xml)
    {
        if (null === $this->_transaction) {
            $domDocument = new DOMDocument();
            $domDocument->loadXML($xml);
            $this->_transaction = $domDocument->documentElement;
        }
        $message = $this->_transaction->getElementsByTagName('Message')->item(0);
        $tender = $message->getElementsByTagName('Tender')->item(0);

        $dateFin = $tender->getAttribute('ClosureDate');

        return $dateFin;
    }

    public function getObjetFromXml($xml)
    {
        if (null === $this->_transaction) {
            $domDocument = new DOMDocument();
            $domDocument->loadXML($xml);
            $this->_transaction = $domDocument->documentElement;
        }

        $message = $this->_transaction->getElementsByTagName('Message')->item(0);
        $tender = $message->getElementsByTagName('Tender')->item(0);
        $objet = $tender->getElementsByTagName('Objet')->item(0);
        $description = $objet->getElementsByTagName('Description')->item(0);
        $content = $description->getElementsByTagName('Content')->item(0);
        $objetContent = utf8_decode($content->nodeValue);

        return $objetContent;
    }

    public function getCategorieFromXml($xml)
    {
        if (null === $this->_transaction) {
            $domDocument = new DOMDocument();
            $domDocument->loadXML($xml);
            $this->_transaction = $domDocument->documentElement;
        }

        $message = $this->_transaction->getElementsByTagName('Message')->item(0);
        $tender = $message->getElementsByTagName('Tender')->item(0);

        $categorie = $tender->getAttribute('Categorie');

        return $categorie;
    }

    public function getConsultationsVoFromXmlRegistreDepot($xmlOffres)
    {
        $xmlOffres = str_ireplace('UTF-8', 'ISO-8859-1', $xmlOffres);

        $domDocument = new DOMDocument();
        $domDocument->loadXML($xmlOffres);
        $transaction = $domDocument->documentElement;

        $message = $transaction->getElementsByTagName('Message')->item(0);
        $tenderDefinitionTags = $message->getElementsByTagName('TenderDefinition');

        $consultationsVo = [];

        foreach ($tenderDefinitionTags as $tenderDefinitionTag) {
            $consultationVo = new Atexo_Interfaces_ConsultationVo();
            $consultationVo->setReferenceUtilisateur($tenderDefinitionTag->getAttribute('Reference'));
            $consultationVo->setReferenceConnecteur($tenderDefinitionTag->getAttribute('ReferenceConnecteur'));
            $consultationVo->setPhaseAcces($tenderDefinitionTag->getAttribute('AccessPhase'));

            $offresVo = (new Atexo_Interfaces_Offre())->getOffreVoFromXmlRegistreDepot($xmlOffres);
            $consultationVo->setOffresVo($offresVo);
            $consultationsVo[] = $consultationVo;
        }

        return $consultationsVo;
    }

    public function getConsultationsVoFromXmlRegistreRetrait($xmlRetraits)
    {
        $xmlRetraits = str_ireplace('UTF-8', 'ISO-8859-1', $xmlRetraits);

        $domDocument = new DOMDocument();
        $domDocument->loadXML($xmlRetraits);
        $transaction = $domDocument->documentElement;

        $message = $transaction->getElementsByTagName('Message')->item(0);
        $tenderDefinitionTags = $message->getElementsByTagName('TenderDefinition');

        $consultationsVo = [];

        foreach ($tenderDefinitionTags as $tenderDefinitionTag) {
            $consultationVo = new Atexo_Interfaces_ConsultationVo();
            $consultationVo->setReferenceUtilisateur($tenderDefinitionTag->getAttribute('Reference'));
            $consultationVo->setReferenceConnecteur($tenderDefinitionTag->getAttribute('ReferenceConnecteur'));
            $consultationVo->setPhaseAcces($tenderDefinitionTag->getAttribute('AccessPhase'));

            $retraitsVo = (new Atexo_Interfaces_Telechargement())->getTelechargementVoFromXmlRegistreRetrait($xmlRetraits);
            $consultationVo->setTelechargement($retraitsVo);
            $consultationsVo[] = $consultationVo;
        }

        return $consultationsVo;
    }

    public function getIntutileConsultationFromXml($xml)
    {
        if (null === $this->_transaction) {
            $domDocument = new DOMDocument();
            $domDocument->loadXML($xml);
            $this->_transaction = $domDocument->documentElement;
        }

        $message = $this->_transaction->getElementsByTagName('Message')->item(0);
        $tender = $message->getElementsByTagName('Tender')->item(0);
        $intutileCons = $tender->getElementsByTagName('Intitule')->item(0);
        $description = $intutileCons->getElementsByTagName('Description')->item(0);
        $content = $description->getElementsByTagName('Content')->item(0);
        $intutileConsContent = utf8_decode($content->nodeValue);

        return $intutileConsContent;
    }

    public function getLotsConsultationFromXml($xml)
    {
        $lotsConsultation = [];
        if (null === $this->_transaction) {
            $domDocument = new DOMDocument();
            $domDocument->loadXML($xml);
            $this->_transaction = $domDocument->documentElement;
        }

        $message = $this->_transaction->getElementsByTagName('Message')->item(0);
        $tender = $message->getElementsByTagName('Tender')->item(0);
        $lots = $tender->getElementsByTagName('Lots')->item(0);
        if ($lots) {
            $lots = $lots->getElementsByTagName('Lot');
            foreach ($lots as $baliseLot) {
                $numeroLot = abs($baliseLot->getAttribute('Numero'));
                if ($numeroLot) {
                    $lotsConsultation[$numeroLot]['numeroLot'] = $numeroLot;

                    $categorieLot = $baliseLot->getAttribute('Categorie');
                    $lotsConsultation[$numeroLot]['categorieLot'] = (new Atexo_Interfaces_InterfacesUtilities())->getCategorie($categorieLot);

                    $tag = $baliseLot->getElementsByTagName('Intitule')->item(0);
                    $tag = $tag->getElementsByTagName('Description')->item(0);
                    $tag = $tag->getElementsByTagName('Content')->item(0);
                    $lotsConsultation[$numeroLot]['titreLot'] = utf8_decode($tag->nodeValue);

                    $tag = $baliseLot->getElementsByTagName('Objet')->item(0);
                    $tag = $tag->getElementsByTagName('Description')->item(0);
                    $tag = $tag->getElementsByTagName('Content')->item(0);
                    $lotsConsultation[$numeroLot]['objetLot'] = utf8_decode($tag->nodeValue);
                }
            }
        }

        return $lotsConsultation;
    }
}
