<?php

namespace Application\Service\Atexo\Interfaces;

use Application\Propel\Mpe\CommonOffres;
use Application\Propel\Mpe\CommonQuestionsDce;
use Application\Propel\Mpe\CommonTelechargement;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Interfaces_Inscrit
{
    public function getInscritVoByRegistre($registre)
    {
        $inscritVo = new Atexo_Interfaces_InscritVo();
        if ($registre instanceof CommonTelechargement || $registre instanceof CommonQuestionsDce) {
            $array = explode(' ', $registre->getNom());
            $inscritVo->setNom($array[1]);
            $inscritVo->setPrenom($array[0]);
            $inscritVo->setEmail(trim($registre->getEmail()));
        } elseif ($registre instanceof CommonOffres) {
            $inscritVo->setNom($registre->getNomInscrit());
            $inscritVo->setPrenom($registre->getPrenomInscrit());
            $inscritVo->setEmail(trim($registre->getEmailInscrit()));
        }

        return $inscritVo;
    }

    public function getInscritVoFromTagEnteprise(\DOMElement $entrepriseTag)
    {
        $inscritVo = new Atexo_Interfaces_InscritVo();
        $inscritVo->setNom(utf8_decode($entrepriseTag->getAttribute('nomInscrit')));
        $inscritVo->setPrenom(utf8_decode($entrepriseTag->getAttribute('prenomInscrit')));
        $inscritVo->setEmail(trim(utf8_decode($entrepriseTag->getAttribute('emailInscrit'))));

        return $inscritVo;
    }
}
