<?php

namespace Application\Service\Atexo\Interfaces;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonInscrit;
use Application\Propel\Mpe\CommonInscritPeer;
use Application\Propel\Mpe\CommonTelechargement;
use Application\Propel\Mpe\CommonTelechargementPeer;
use Application\Propel\Mpe\Entreprise;
use Application\Propel\Mpe\EntreprisePeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;

/**
 * commentaires.
 *
 * @author Othmane ABISOUROUR <othmane.abisourour@atexo.com>
 * @copyright Atexo 2009
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Interfaces_Telechargement
{
    public function getInfoTelechargement($xml)
    {
        $consultation = new Atexo_Interfaces_Consultation();

        $consultationsVo = $consultation->createConsultationVoFromXml($xml);
        $consultationVo = array_shift($consultationsVo);
        if (!$consultationVo instanceof Atexo_Interfaces_ConsultationVo) {
            $consultationVo = new Atexo_Interfaces_ConsultationVo();
        }

        $consultationId = $consultation->getIdConsultationByRefCon($xml);

        //Telechargement
        $telechargements = (new Atexo_Interfaces_Telechargement())->getComTelechargementByRefConsultation($consultationId, Atexo_CurrentUser::getCurrentOrganism());
        $interfacesVo = [];

        if (is_array($telechargements)) {
            foreach ($telechargements as $one) {
                $interfaceVo = new Atexo_Interfaces_TelechargementVo();

                $interfaceVo->setDateTelechargement($one->getDatetelechargement());
                $interfaceVo->setNomFichier(utf8_decode($one->getNomsFichiersDce()));
                //$telechargementsCom = Atexo_Interfaces_Telechargement::getCommonTelechargementByIdTelechargement($one->getId());
                //Entreprise
                $entrepriseVo = (new Atexo_Interfaces_Entreprise())->getEntrepriseVoByTelechargement($one);
                //Inscrit
                $inscritVo = (new Atexo_Interfaces_Inscrit())->getInscritVoByRegistre($one);
                $entrepriseVo->setInscrit($inscritVo);
                $interfaceVo->setEntrepriseVo($entrepriseVo);
                $interfacesVo[] = $interfaceVo;
            }
            $consultationVo->setTelechargement($interfacesVo);
        }
        $consultation = [];
        $consultation[] = $consultationVo;

        return $consultation;
        //fin telechargement
    }

    //Base Commune
    public function getComTelechargementByRefConsultation($consultationId, $organisme)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));

        $c = new Criteria();
        $c->add(CommonTelechargementPeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonTelechargementPeer::ORGANISME, $organisme);
        $telechargementCom = CommonTelechargementPeer::doSelect($c, $connexionCom);

        return $telechargementCom;
    }

    //Base Commune
    public function getCommonTelechargementByIdTelechargement($idTelechargement)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));

        $c = new Criteria();
        $c->add(CommonTelechargementPeer::ID, $idTelechargement);
        $telechargementCom = CommonTelechargementPeer::doSelectOne($c, $connexionCom);

        if ($telechargementCom instanceof CommonTelechargement) {
            return $telechargementCom;
        }

        return false;
    }

    public function retrieveEntrepriseById($idEntreprise)
    {
        if (0 == $idEntreprise) {
            return false;
        }

        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));

        $c = new Criteria();
        $c->add(EntreprisePeer::ID, $idEntreprise);
        $entreprise = EntreprisePeer::doSelectOne($c, $connexionCom);

        if ($entreprise instanceof Entreprise) {
            return $entreprise;
        }

        return false;
    }

    public function retrieveInscritByIdInscrit($idInscrit)
    {
        if (0 == $idInscrit) {
            return false;
        }

        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));

        $c = new Criteria();
        $c->add(CommonInscritPeer::ID, $idInscrit);
        $inscrit = CommonInscritPeer::doSelectOne($c, $connexionCom);

        if ($inscrit instanceof CommonInscrit) {
            return $inscrit;
        }

        return false;
    }
}
