<?php

namespace Application\Service\Atexo\Interfaces;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonOffres;
use Application\Propel\Mpe\CommonQuestionsDce;
use Application\Propel\Mpe\CommonTelechargement;
use Application\Propel\Mpe\EntreprisePeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Inscrit;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Interfaces_Entreprise
{
    public function getEntrepriseVoByTelechargement($telechargement)
    {
        $entrepriseVo = new Atexo_Interfaces_EntrepriseVo();

        //$entreprise=Atexo_Entreprise::retrieveCompanyBdeById($idEntreprise);

        if ($telechargement instanceof CommonTelechargement) {
            if ($telechargement->getId()) {
                $entrepriseVo->setId($telechargement->getId());
            } else {
                $entrepriseVo->setId('0');
            }

            if ($telechargement->getEntreprise()) {
                $entrepriseVo->setNom($telechargement->getEntreprise());
            } else {
                $entrepriseVo->setNom('NA');
            }
            if ($telechargement->getAdresse()) {
                $entrepriseVo->setAdresse($telechargement->getAdresse());
            } else {
                $entrepriseVo->setAdresse('NA');
            }
            if ($telechargement->getAdresse2()) {
                $entrepriseVo->setAdresse2($telechargement->getAdresse2());
            }/*else {
                   $entrepriseVo->setAdresse2("NA");
               }*/
            if ($telechargement->getCodepostal()) {
                $entrepriseVo->setCodePostal($telechargement->getCodepostal());
            } else {
                $entrepriseVo->setCodePostal('NA');
            }
            if ($telechargement->getVille()) {
                $entrepriseVo->setVille($telechargement->getVille());
            } else {
                $entrepriseVo->setVille('NA');
            }

            if ($telechargement->getPays()) {
                $entrepriseVo->setPays($telechargement->getPays());
            } else {
                $entrepriseVo->setPays('NA');
            }
            if ($telechargement->getTelephone()) {
                $entrepriseVo->setTelephone($telechargement->getTelephone());
            } else {
                $entrepriseVo->setTelephone('NA');
            }

            if (trim($telechargement->getSirenetranger())) {
                $entrepriseVo->setSiret($telechargement->getSirenetranger());
            } else {
                $entrepriseVo->setSiret($telechargement->getSiret());
            }
            $entrepriseVo->setFormeJuridique('NA');
        }

        return $entrepriseVo;
    }

    public function getEntrepriseVoByQuestion($question)
    {
        $entrepriseVo = new Atexo_Interfaces_EntrepriseVo();

        //$entreprise=Atexo_Entreprise::retrieveCompanyBdeById($idEntreprise);

        if ($question instanceof CommonQuestionsDce) {
            if ($question->getEntreprise()) {
                $entrepriseVo->setNom($question->getEntreprise());
            } else {
                $entrepriseVo->setNom('NA');
            }
            if ($question->getAdresse()) {
                $entrepriseVo->setAdresse($question->getAdresse());
            } else {
                $entrepriseVo->setAdresse('NA');
            }
            if ($question->getAdresse2()) {
                $entrepriseVo->setAdresse2($question->getAdresse2());
            }/*else {
                   $entrepriseVo->setAdresse2("NA");
               }*/
            if ($question->getCp()) {
                $entrepriseVo->setCodePostal($question->getCp());
            } else {
                $entrepriseVo->setCodePostal('NA');
            }
            if ($question->getVille()) {
                $entrepriseVo->setVille($question->getVille());
            } else {
                $entrepriseVo->setVille('NA');
            }

            if ($question->getPays()) {
                $entrepriseVo->setPays($question->getPays());
            } else {
                $entrepriseVo->setPays('NA');
            }
            if ($question->getTel()) {
                $entrepriseVo->setTelephone($question->getTel());
            } else {
                $entrepriseVo->setTelephone('NA');
            }

            if (trim($question->getIdentifiantNational())) {
                $entrepriseVo->setSiret($question->getIdentifiantNational());
            } else {
                $entrepriseVo->setSiret($question->getSiret());
            }
            $entrepriseVo->setFormeJuridique('NA');
        }

        return $entrepriseVo;
    }

    public function getEntrepriseVoByOffre($offre)
    {
        $entrepriseVo = new Atexo_Interfaces_EntrepriseVo();

        //$entreprise=Atexo_Entreprise::retrieveCompanyBdeById($idEntreprise);

        if ($offre instanceof CommonOffres) {
            $entrepriseVo->setId($offre->getEntrepriseId());

            if ($offre->getNomEntrepriseInscrit()) {
                $entrepriseVo->setNom($offre->getNomEntrepriseInscrit());
            } else {
                $entrepriseVo->setNom('NA');
            }
            if ($offre->getAdresseInscrit()) {
                $entrepriseVo->setAdresse($offre->getAdresseInscrit());
            } else {
                $entrepriseVo->setAdresse('NA');
            }
            if ($offre->getAdresse2Inscrit()) {
                $entrepriseVo->setAdresse2($offre->getAdresse2Inscrit());
            }/*else {
                   $entrepriseVo->setAdresse2("NA");
               }*/
            if ($offre->getCodePostalInscrit()) {
                $entrepriseVo->setCodePostal($offre->getCodePostalInscrit());
            } else {
                $entrepriseVo->setCodePostal('NA');
            }
            if ($offre->getVilleInscrit()) {
                $entrepriseVo->setVille($offre->getVilleInscrit());
            } else {
                $entrepriseVo->setVille('NA');
            }

            if ($offre->getPaysInscrit()) {
                $entrepriseVo->setPays($offre->getPaysInscrit());
            } else {
                $entrepriseVo->setPays('NA');
            }
            if ($offre->getTelephoneInscrit()) {
                $entrepriseVo->setTelephone($offre->getTelephoneInscrit());
            } else {
                $entrepriseVo->setTelephone('NA');
            }

            if (trim($offre->getIdentifiantNational())) {
                $entrepriseVo->setSiret($offre->getIdentifiantNational());
            } else {
                $entrepriseVo->setSiret($offre->getSiretEntreprise());
            }
            $entrepriseVo->setFormeJuridique('NA');
        }

        return $entrepriseVo;
    }

    public function getEntrepriseVoFromTagEntreprise(\DOMNodeList $EntreprisesTag)
    {
        $entreprisesVo = [];
        foreach ($EntreprisesTag as $entreprise) {
            $entrepriseVo = new Atexo_Interfaces_EntrepriseVo();

            $entrepriseVo->setId($entreprise->getAttribute('id'));
            $entrepriseVo->setNom(utf8_decode($entreprise->getAttribute('nom')));
            $entrepriseVo->setAdresse(utf8_decode($entreprise->getAttribute('adresse')));
            $entrepriseVo->setAdresse2(utf8_decode($entreprise->getAttribute('adresse2')));
            $entrepriseVo->setCodePostal($entreprise->getAttribute('codePostal'));
            $entrepriseVo->setVille(utf8_decode($entreprise->getAttribute('ville')));
            $entrepriseVo->setPays(utf8_decode($entreprise->getAttribute('pays')));
            $entrepriseVo->setTelephone($entreprise->getAttribute('telephone'));
            $entrepriseVo->setSiret($entreprise->getAttribute('siret'));
            $entrepriseVo->setFormeJuridique(utf8_decode($entreprise->getAttribute('formejuridique')));

            $inscritVo = (new Atexo_Interfaces_Inscrit())->getInscritVoFromTagEnteprise($entreprise);
            $entrepriseVo->setInscrit($inscritVo);

            $entreprisesVo[] = $entrepriseVo;
        }

        return $entreprisesVo;
    }

    public function getInfoEntreprise($xml)
    {
        $interfacesVo = [];
        $consultation = new Atexo_Interfaces_Consultation();
        $consultationsVo = $consultation->createConsultationVoFromXml($xml);
        $consultationVo = array_shift($consultationsVo);
        $entreprises = self::getAllEntreprise();

        if (is_array($entreprises)) {
            foreach ($entreprises as $entreprise) {
                $inscrits = (new Atexo_Entreprise_Inscrit())->retrieveInscritByIdCompnay($entreprise->getId());

                if (is_array($inscrits)) {
                    foreach ($inscrits as $inscrit) {
                        $entrepriseVo = new Atexo_Interfaces_EntrepriseVo();
                        $inscritVo = new Atexo_Interfaces_InscritVo();

                        $entrepriseVo->setId($entreprise->getId());
                        $entrepriseVo->setNom($entreprise->getNom());
                        $entrepriseVo->setAdresse($entreprise->getAdresse());
                        $entrepriseVo->setAdresse2($entreprise->getAdresse2());
                        $entrepriseVo->setCodePostal($entreprise->getCodepostal());
                        $entrepriseVo->setVille($entreprise->getVilleadresse());
                        $entrepriseVo->setPays($entreprise->getPaysadresse());
                        $entrepriseVo->setTelephone($entreprise->getTelephone());
                        if ($entreprise->getSirenEtranger()) {
                            $entrepriseVo->setSiret($entreprise->getSirenEtranger());
                        } else {
                            $entrepriseVo->setSiret($entreprise->getSiren().$entreprise->getNicsiege());
                        }
                        $entrepriseVo->setFormeJuridique($entreprise->getFormeJuridique());

                        if ($inscrit) {
                            $inscritVo->setNom($inscrit->getNom());
                            $inscritVo->setPrenom($inscrit->getPrenom());
                            $inscritVo->setEmail(trim($inscrit->getEmail()));
                        } else {
                            $inscritVo = new Atexo_Interfaces_InscritVo();
                        }
                        $entrepriseVo->setInscrit($inscritVo);
                        $interfacesVo[] = $entrepriseVo;
                    }
                }
            }
            $consultationVo->setEntreprise($interfacesVo);
        }
        $consultation = [];
        $consultation[] = $consultationVo;

        return $consultation;
    }

    public function getAllEntreprise()
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $entreprise = EntreprisePeer::doSelect($c, $connexion);
        if ($entreprise) {
            return $entreprise;
        } else {
            return false;
        }
    }
}
