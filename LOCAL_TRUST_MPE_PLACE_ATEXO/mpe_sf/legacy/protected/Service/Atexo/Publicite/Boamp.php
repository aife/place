<?php

namespace Application\Service\Atexo\Publicite;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonAcheteurPublicPeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Util;

/**
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Publicite_Boamp
{
    /**
     * retourne l'identifiant et le mot de passe.
     */
    public function retreiveAcheteurPublic($identifiant, $organisme)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonAcheteurPublicPeer::ID, $identifiant);
        $c->add(CommonAcheteurPublicPeer::ORGANISME, $organisme);
        $result = CommonAcheteurPublicPeer::doSelectOne($c, $connexionCom);
        if ($result) {
            return $result; //->getBoampLogin() . ":" . $result->getBoampLogin();
        } else {
            return '';
        }
    }

    /**
     * retourne la liste des comptes BOAMP d'un organisme.
     *
     * @param  $organisme acronyme de l'organisme
     *
     * @return Acheteurpublic
     */
    public function retrieveAllAcheteurPublic($organisme = null)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        if (!$organisme) {
            $organisme = Atexo_CurrentUser::getCurrentOrganism();
        }
        $c = new Criteria();
        $c->add(CommonAcheteurPublicPeer::ORGANISME, $organisme);

        return CommonAcheteurPublicPeer::doSelect($c, $connexion);
    }

    /* Fonction qui le nom du fichier XML envoye au BOAMP
     * @return $xml_name nom du fichier xml a envoyer au boamp
     */
    public function getBoampXmlName($identifiant, $organisme)
    {
        $year = getdate();
        $year = $year['year'];
        $acheteurPub = self::retreiveAcheteurPublic($identifiant, $organisme);
        //xml_name = login au boamp + quantieme du jour (+ id envoiXML ds BDD (a faire!!))
        $nb = microtime(true);
        $nb .= random_int(103, 9891);
        $nb = substr($nb, strpos($nb, '.') + 1, 5);

        return strtoupper($acheteurPub->getBoampLogin().'_'.$year.self::getQuantieme().$nb.'.xml');
    }

    /* Fonction qui retourne le quantieme du jour sur 3 digits
     * Utile pour la creation du nom de fichier envoye au forpro
     */
    public function getQuantieme()
    {
        $dateArray = getdate();
        $quantieme = $dateArray['yday'] + 1;
        $quantLength = strlen($quantieme);
        if ($quantLength > 0 && $quantLength < 3) {
            for ($i = $quantLength; $i < 3; ++$i) {
                $quantieme = '0'.$quantieme;
            }
        }

        return $quantieme;
    }

    public function boampSendPath($achteurPub)
    {
        if ($achteurPub) {
            if ('1' == $achteurPub->getBoampTarget()) {
                return '/XMLs/envoi_fichiers';
            } else {
                return '/XMLs/envoi_test';
            }
        } else {
            return '';
        }
    }

    public function boampHost()
    {
        return 'concentrateur-annonces-boamp.local-trust.com';
    }

    /* Fonction qui renvoie l'objet du xml contenu dans la reponse du boamp
     * @param $respIn Reponse du boamp suite a l'envoi du xml AAPC
     * @return $domxml Objet DOM contetant les infos de la reponse
     */
    public function getXmlDocument($strIn)
    {
        $domxml = null;
        //Recuperation de la reponse XML
        $xml_resp = strstr($strIn, '<?xml');
        if ($xml_resp) {
            $domxml = new \DOMDocument();
            $domxml->loadXML($xml_resp);
        }
        if (!is_object($domxml)) {
            echo 'Impossible de charger le document xml.';

            return;
        }

        return $domxml;
    }

    /* Fonction qui renvoie le statut de l'envoi au boamp
     * @param $respIn Reponse du boamp suite a l'envoi du xml AAPC
     * @return $statut_envoi Statut de l'envoi au BOAMP OK si reussi ERREUR_XXX sinon
     */
    public function parseBoampStatusResponse($respIn)
    {
        if ($domIn = self::getXmlDocument($respIn)) {
            //Recuperation de 'element ETAT
            $etatElt = $domIn->get_elements_by_tagname('ETAT');
            if ([] == $etatElt) {
                echo "Impossible de recuperer l'element ETAT dans la reponse xml.";

                return;
            }

            return $etatElt[0]->get_content();
        } else {
            return false;
        }
    }

    /**
     * Envoi de l'annonce au BOAMP
     * parametre boampMesureAvancem permets de générer le fichier log pr l'envois BOAMP du Mesure avancement
     * sinon (boampMesureAvancem=false) on génére l'envois du BOAMP.
     */
    public function sendXml(Atexo_Publicite_FormulaireVo $formulaireVo, $boampMesureAvancem = false)
    {
        $boundary = Atexo_Config::getParameter('BOAMP_BOUNDARY');
        $data = '--'.$boundary."\r\n";
        $data .= 'Content-Disposition: form-data; name="'.$formulaireVo->getNameChamp().'"; filename="'.$formulaireVo->getNameFile()."\"\r\n";
        $data .= "Content-Type: text/xml\r\n";
        $data .= "\r\n";
        $data .= $formulaireVo->getXml()."\r\n";
        $data .= '--'.$boundary."--\r\n";

        $urlProxy = Atexo_Config::getParameter('URL_PROXY');
        $portProxy = Atexo_Config::getParameter('PORT_PROXY');

        if ('' != $urlProxy) {
            $msg = 'POST http://'.$formulaireVo->getUrlHost().$formulaireVo->getUrlSendPath()." HTTP/1.1\r\n";
        } else {
            $msg = 'POST '.$formulaireVo->getUrlSendPath()." HTTP/1.1\r\n";
        }

        $msg .= 'Authorization: Basic '.$formulaireVo->getAuthentificate()."\r\n";
        $msg .= 'Host: '.$formulaireVo->getUrlHost()."\r\n";
        $msg .= "Pragma: no-cache\r\n";
        $msg .= "Accept: image/gif, image/x-xbitmap, image/jpeg, image/pjpeg, */*\r\n";
        $msg .= 'Content-Length: '.strlen($data)."\r\n";
        $msg .= 'Content-Type: multipart/form-data; boundary='.$boundary."\r\n";
        $msg .= "\r\n";
        $msg .= $data;
        $result = '';

        if ('' != $urlProxy) {
            $fd = fsockopen($urlProxy, $portProxy, $errno, $errstr);
            if (!$fd) {
                echo "$errstr ($errno)<br />\n";
                exit();
            }
        } else {
            $fd = fsockopen($formulaireVo->getUrlHost(), 80, $errno, $errstr);
            if (!$fd) {
                echo "$errstr ($errno)<br />\n";

                exit();
            }
        }
        if ($fd) {
            fputs($fd, $msg);
            while (!feof($fd)) {
                $result .= fgets($fd, 4096);
            }
            fclose($fd);
        }
        if ($boampMesureAvancem) {
            self::generateFileLogMesureAvancement($formulaireVo, $result, $msg);
        } else {
            self::generateFileLog($formulaireVo, $result, $msg);
        }
        $status = self::parseBoampStatusResponse($result);

        if (0 != strcmp('OK', $status)) {
            // echo "L'envoi du fichier au BOAMP a echou&eacute; pour la raison suivante:<BR /><BR /><em>".$status.
            //": ".self::parse_boamp_failure_cause($result)."</em>";
            return [false, $result];
        } else {
            //       echo "L'envoi de l'AAPC au BOAMP a &eacute;t&eacute; effectu&eacute; avec succ&egrave;s<BR />"
            //  ."Vous recevrez un courrier &eacute;lectronique vous informant de la validation de l'AAPC par le BOAMP.<BR/><BR />";
            return [true, '', $formulaireVo->getNameFile()];
        }
    }

    /* Fonction qui renvoie la raison de l'echec de l'envoi au boamp
     * @param $respIn Reponse du boamp suite a l'envoi du xml AAPC
     * @return $fail_cause Raison de l'echec de l'envoi
     */
    public function parse_boamp_failure_cause($respIn)
    {
        if ($domIn = self::getXmlDocument($respIn)) {
            //Recuperation de 'element ETAT
            $msg_elt = $domIn->get_elements_by_tagname('MESSAGE');
            if ([] == $msg_elt) {
                echo "Impossible de recuperer l'element MESSAGE dans la reponse xml.";

                return;
            }
            $fail_cause = $msg_elt[0]->get_content();

            return htmlentities(utf8_decode($fail_cause));
        } else {
            return false;
        }
    }

    /**
     * resourne le statut de l'annonce selon l'attribut etat du xml reponse.
     */
    public function statutAnnonce($statutIn)
    {
        switch ($statutIn) {
            case 'RG':
                return Atexo_Config::getParameter('DESTINATAIRE_REFUSE');
            case 'RX':
                return Atexo_Config::getParameter('DESTINATAIRE_REFUSE');
            case 'RE':
                return Atexo_Config::getParameter('DESTINATAIRE_REFUSE');
            case 'AV':
                return Atexo_Config::getParameter('DESTINATAIRE_PUBLIE');
            case 'CP':
                return Atexo_Config::getParameter('DESTINATAIRE_PUBLIE');
            case 'PU':
                return Atexo_Config::getParameter('DESTINATAIRE_PUBLIE_AVEC_LIEN');
        }
    }

    public function getBoampSuiviUrl($achteurPub)
    {
        if ($achteurPub) {
            if ('1' == $achteurPub->getBoampTarget()) {
                return (new Atexo_Publicite_Boamp())->boampHost().'/XMLs/fichier_suivi';
            } else {
                return (new Atexo_Publicite_Boamp())->boampHost().'/XMLs/test_suivi';
            }
        } else {
            return '';
        }
    }

    /**
     *   Création d'un fichier de log qui contien les information sur le projet d'annonce ,
     *   le flux envoyer au BOAMP et le flux de retour.
     */
    public function generateFileLog($formulaireVo, $result, $msg)
    {
        $log = null;
        $nameFileXML = $formulaireVo->getNameFile();

        $log .= "\n\n";
        $log .= "\n\t____________________________________\n";
        $log .= "\n\n";
        $log .= "\t Requète envoyée : \n".$msg." \n";
        $log .= "\n\n";
        $log .= "\n\n";
        $log .= "\n\n";
        $log .= "\n\t____________________________________\n";
        $log .= "\n\n";
        $log .= "\t Reponse BOAMP : \n\n ".$result." \n";
        $filePath = Atexo_Config::getParameter('LOG_FILE_PATH_BOAMP').'Annonce_'.$nameFileXML.'_'.date('YmdHis').'.log';
        Atexo_Util::writeLogFile(Atexo_Config::getParameter('LOG_FILE_PATH_BOAMP'), $filePath, $log);
    }

    public function generateFileLogMesureAvancement($formulaireVo, $result, $msg)
    {
        $log = null;
        $nameFile = $formulaireVo->getNameFile();
        $annee = $formulaireVo->getAnnee();
        $trimestre = $formulaireVo->getTrimestre();

        $log .= "\n\n";
        $log .= "\n\t____________________________________\n";
        $log .= "\n\n";
        $log .= "\t Service : ".$formulaireVo->getLibelleService()."\n";
        $log .= "\t Année : ".$annee."\n";
        $log .= "\t Trimestre : ".$trimestre."\n";
        $log .= "\n\n";
        $log .= "\n\t____________________________________\n";
        $log .= "\n\n";
        //$log .= "\t Requète envoyée : \n" . $msg ." \n";
        $log .= "\n\n";
        $log .= "\n\n";
        $log .= "\n\n";
        $log .= "\n\t____________________________________\n";
        $log .= "\n\n";
        $log .= "\t Reponse BOAMP : \n\n ".$result." \n";
        $filePath = Atexo_Config::getParameter('LOG_FILE_PATH_BOAMP_MESUREDEMAT').'MesureAvancementDemat_'.$annee.'_'.$trimestre.'_'.$nameFile.'.log';
        Atexo_Util::writeLogFile(Atexo_Config::getParameter('LOG_FILE_PATH_BOAMP_MESUREDEMAT'), $filePath, $log);
    }
}
