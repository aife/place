<?php

namespace Application\Service\Atexo\Publicite;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonAnnonceJALPeer;
use Application\Propel\Mpe\CommonDestinataireAnnonceJALPeer;
use Application\Propel\Mpe\CommonDestinataireAnnonceJALQuery;
use Application\Propel\Mpe\CommonJALPeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;

/**
 * Classe de manipulation des destinataire.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Publicite_DestinataireJAL
{
    /**
     * retourne la liste des destinataires.
     */
    public function retreiveJals($consultation)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonJALPeer::ORGANISME, Atexo_CurrentUser::getCurrentOrganism());
        $c->add(CommonJALPeer::SERVICE_ID, $consultation[0]->getServiceId());
        $c->addOr(CommonJALPeer::SERVICE_ID, $consultation[0]->getServiceAssocieId());
        $lisDest = CommonJALPeer::doSelect($c, $connexionCom);
        if ($lisDest) {
            return $lisDest;
        } else {
            return [];
        }
    }

    /**
     * Retourne la liste des destinataires identifié par l'id formulaire xml.
     *
     * @param string $idAnnonce: identifiant technique de l'annonce JAL
     * @param string $org:       organisme
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function retreiveListDestinataire($idAnnonce, $org = null)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        if (!$org) {
            $org = Atexo_CurrentUser::getCurrentOrganism();
        }
        $destinataireJalQuery = new CommonDestinataireAnnonceJALQuery(Atexo_Config::getParameter('COMMON_DB'));
        $listeDestinataires = $destinataireJalQuery->recupererListeDestinatairesJal($idAnnonce, $org);
        if ($listeDestinataires) {
            return $listeDestinataires;
        }

        return [];
    }

    /**
     * retourne la liste des destinataires identifié par l'id de l'annonce.
     */
    public function retreiveListDestinataireByIdAnnonceJal($idDestAnnonce, $org = null)
    {
        if (!$org) {
            $org = Atexo_CurrentUser::getCurrentOrganism();
        }
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonDestinataireAnnonceJALPeer::IDANNONCEJAL, $idDestAnnonce);
        $c->add(CommonDestinataireAnnonceJALPeer::ORGANISME, $org);
        $listDest = CommonDestinataireAnnonceJALPeer::doSelect($c, $connexionCom);
        if ($listDest) {
            return $listDest;
        } else {
            return [];
        }
    }

    /**
     * retourne l'objet destinataire identifié par l'id formulaire.
     */
    public function retreiveDestinataireById($idDestinataireAnnonce, $org = null)
    {
        if (!$org) {
            $org = Atexo_CurrentUser::getCurrentOrganism();
        }
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));

        $c = new Criteria();
        $c->add(CommonDestinataireAnnonceJALPeer::ID, $idDestinataireAnnonce);
        $c->add(CommonDestinataireAnnonceJALPeer::ORGANISME, $org);
        //$c->addJoin(CommonDestinataireAnnonceJALPeer::ORGANISME, CommonJALPeer::ORGANISME);
        $destObject = CommonDestinataireAnnonceJALPeer::doSelectJoinCommonJal($c, $connexionCom);

        return $destObject;
    }

    /**
     * Supprimer destinataireS d'une annonce Format libre.
     */
    public function deleteDestinataireJalAnnonce($idAnnonceJal)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $c = new Criteria();
        $c->add(CommonDestinataireAnnonceJALPeer::IDANNONCEJAL, $idAnnonceJal);
        $c->add(CommonDestinataireAnnonceJALPeer::ORGANISME, Atexo_CurrentUser::getCurrentOrganism());
        CommonDestinataireAnnonceJALPeer::doDelete($c, $connexionCom);
    }

    public function retreiveSendAnnonceJal($consultationId)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $c = new Criteria();
        $c->add(CommonAnnonceJALPeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonAnnonceJALPeer::ORGANISME, Atexo_CurrentUser::getCurrentOrganism());
        $c->add(CommonDestinataireAnnonceJALPeer::STATUT, '4');
        //$c->addJoin(CommonDestinataireAnnonceJALPeer::ORGANISME, CommonAnnonceJALPeer::ORGANISME);
        $result = CommonDestinataireAnnonceJALPeer::doSelectJoinCommonAnnonceJAL($c, $connexion);
        if ($result) {
            return $result;
        } else {
            return false;
        }
    }

    public function retrieveDestinataireAnnonceJalByEchangeDestinataire($echangeDest, $org = null)
    {
        if (!$org) {
            $org = Atexo_CurrentUser::getCurrentOrganism();
        }
        if ($echangeDest) {
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            $c = new Criteria();
            $c->add(CommonDestinataireAnnonceJALPeer::ID_ECHANGE, $echangeDest->getIdEchange());
            $c->add(CommonDestinataireAnnonceJALPeer::ORGANISME, $org);
            $objetDestAnnonceJal = CommonDestinataireAnnonceJALPeer::doSelectOne($c, $connexionCom);

            return $objetDestAnnonceJal;
        }
    }
}
