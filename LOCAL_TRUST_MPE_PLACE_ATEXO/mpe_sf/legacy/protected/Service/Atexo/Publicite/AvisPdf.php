<?php

namespace Application\Service\Atexo\Publicite;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonAVIS;
use Application\Propel\Mpe\CommonAvisPub;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonDestinatairePub;
use Application\Propel\Mpe\CommonFormXmlDestinataireOpoce;
use Application\Propel\Mpe\CommonTDonneeComplementaire;
use Application\Service\Atexo\Atexo_Agrement;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_Crypto;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_MultiDomaine;
use Application\Service\Atexo\Atexo_Odf;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Qualifications;
use Application\Service\Atexo\Atexo_Referentiel;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Category;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureType;
use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2;
use Application\Service\Atexo\Referentiel\Atexo_Referentiel_Referentiel;
use AtexoPdf\PdfGeneratorClient;
use DomDocument;
use Prado\Prado;

/**
 * Classe de génération de l'avis.
 *
 * @author Khalid BENAMAR <khalid.benamar@atexo.com>
 * @copyright Atexo 2010
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Publicite_AvisPdf
{
    //Atexo_Config::getParameter('FICHIER_TEMPLATE_AVIS')
    public function generateAvis($consultation, $modele, $lang, $toFile = false, $idAvisPub = null)
    {
        $modePassationConsultation = null;
        $texteEchantillon = null;
        $texteReunion = null;
        $modeles = [];
        $encoding = Atexo_Config::getParameter('HTTP_ENCODING');
        $fileModel = $modele.$lang.'.odt';
        if ($consultation instanceof CommonConsultation) {
            $donnesComplementaire = $consultation->getDonneComplementaire();
            if (is_file($fileModel)) {//verifier si le fichier existe pour cette langue.
                $oldLang = Atexo_CurrentUser::readFromSession('lang');
                $globalization = $this->getApplication()->Modules['globalization'];
                $globalization->setTranslationCatalogue('messages.'.$lang);
                Atexo_CurrentUser::writeToSession('lang', $lang);

                //$consultation = new CommonConsultation();
                //print_r($consultation); exit;

                if (!is_dir(Atexo_Config::getParameter('COMMON_TMP').'/tmpOdt')) {
                    mkdir(Atexo_Config::getParameter('COMMON_TMP').'/tmpOdt');
                }
                $config = [
                                'ZIP_PROXY' => \PhpZipProxy::class,
                                'DELIMITER_LEFT' => '{',
                                'DELIMITER_RIGHT' => '}',
                                'PATH_TO_TMP' => Atexo_Config::getParameter('COMMON_TMP').'/tmpOdt',
                                ];
                $odf = new Atexo_Odf($fileModel, $config);
                $organismeO = Atexo_Organismes::retrieveOrganismeByAcronyme($consultation->getOrganisme());
                $odf->setVars('ORGANISME', $organismeO->getDenominationOrgTraduit(), true, $encoding);
                if ($consultation->getServiceId()) {
                    $serviceO = Atexo_EntityPurchase::retrieveEntityById($consultation->getServiceId(), $consultation->getOrganisme());
                    $libelleService = $serviceO->getLibelleTraduit(); //getLibelle();
                } else {
                    $libelleService = $organismeO->getDenominationOrgTraduit(); //getDenominationOrg();
                }
                $odf->setVars('SERVICE', $libelleService, true, $encoding);
                $idServiceParent = Atexo_EntityPurchase::getParent($consultation->getServiceId(), $consultation->getOrganisme());
                $libelleServiceParent = '';
                if ($idServiceParent) {
                    $serviceParentObj = Atexo_EntityPurchase::retrieveEntityById($idServiceParent, $consultation->getOrganisme());
                    $libelleServiceParent = $serviceParentObj->getLibelleTraduit();
                }
                $odf->setVars('DIVISION', $libelleServiceParent, true, $encoding);

                $odf->setVars('REFERENCE', $consultation->getReferenceUtilisateur(), true, $encoding);

                $odf->setVars('DATE_HEURE_LIMITE_REMISE_PLIS', Atexo_Util::iso2frnDateTime($consultation->getDatefin()), true, $encoding);

                $odf->setVars('OBJET', $consultation->getObjetTraduit(), true, $encoding);

                $odf->setVars('INTITULE_MARCHE', $consultation->getReferenceUtilisateur(), true, $encoding);

                $odf->setVars('HEURE', date_format(date_create($consultation->getDatefin()), 'H:i'), true, $encoding);

                $odf->setVars('DATE_OUVERTURE', date_format(date_create($consultation->getDatefin()), 'd/m/Y'), true, $encoding);

                $categorieConsObject = Atexo_Consultation_Category::retrieveCategorie($consultation->getCategorie());
                $odf->setVars('TYPE_MARCHE', $categorieConsObject->getLibelleTraduit(), true, $encoding);

                $odf->setVars('NUMERO_AVIS', $consultation->getReferenceUtilisateur(), true, $encoding);

                $odf->setVars('DATE_PUBLICATION', Atexo_Util::iso2frnDateTime($consultation->getDateMiseEnLigneCalcule()), true, $encoding);

                $acronyme = $consultation->getOrganisme();
                $autreInformation = self::getInfoFromReferentielConsultation(Atexo_Config::getParameter('AUTRE_INFOS'), $consultation->getId(), 0, $acronyme);
                $odf->setVars('AUTRES_INFOS', $autreInformation, true, $encoding);
                $conditionObtentionDossierSoumission = '';
                $adresseRetraitDossiersTraduit = '';
                $adresseDepotOffresTraduit = '';
                $lieuOuverturePliTraduit = '';
                $conditionObtentionDossierSoumission = $consultation->getAdresseRetraisDossiers();
                $adresseRetraitDossiersTraduit = $consultation->getAdresseRetraisDossiersTraduit();
                $adresseDepotOffresTraduit = $consultation->getAdresseDepotOffresTraduit();
                $lieuOuverturePliTraduit = $consultation->getLieuOuverturePlisTraduit();
                $odf->setVars('CONDITION_OBTENTION_DOSSIER_SOUMISSION', $conditionObtentionDossierSoumission, true, $encoding);
                $pouvoirAdjudicateur = self::getInfoFromReferentielConsultation(Atexo_Config::getParameter('POUVOIR_ADJUDICATEUR'), $consultation->getId(), 0, $acronyme);
                $odf->setVars('POUVOIRS_ADJUDICATEURS', $pouvoirAdjudicateur, true, $encoding);

                $conditionParticipations = self::getInfoFromReferentielConsultation(Atexo_Config::getParameter('CONDITION_PARTICIPATION'), $consultation->getId(), 0, $acronyme);
                $odf->setVars('CONDITION_PARTICIPATION', $conditionParticipations, true, $encoding);
                $odf->setVars('CRITERES_SELECTION', $conditionParticipations, true, $encoding);

                $odf->setVars('adresseRetraitDossiers', Atexo_Util::nl2Chaine($adresseRetraitDossiersTraduit, ' '), true, $encoding); //getAdresseRetraisDossiers()

                $odf->setVars('adresseDepotOffres', Atexo_Util::nl2Chaine($adresseDepotOffresTraduit, ' '), true, $encoding); //getAdresseDepotOffres()

                $odf->setVars('lieuOuverturePlis', Atexo_Util::nl2Chaine($lieuOuverturePliTraduit, ' '), true, $encoding); //getLieuOuverturePlis()

                $odf->setVars('PF_URL', Atexo_Config::getParameter('PF_URL_REFERENCE'));

                $idAvisRectif = '';
                $datePublicationAvis = '';
                if ($idAvisPub) {
                    $avisPub = (new Atexo_Publicite_AvisPub())->getAvisById($idAvisPub, $acronyme, $consultation->getId());
                    if ($avisPub) {
                        $idAvisRectif = $consultation->getId().'-'.$idAvisPub;
                        $datePublicationAvis = Atexo_Util::iso2frnDateTime($avisPub->getDatePublication());
                    }
                }
                $odf->setVars('NUMERO_AVIS_PUB', $idAvisRectif, true, $encoding);
                $odf->setVars('DATE_PUBLICATION_AVIS_PUB', $datePublicationAvis, true, $encoding);

                $lieuExecution = Atexo_Geolocalisation_GeolocalisationN2::getLibelleLieuExecution(explode(',', $consultation->getLieuExecution()), $lang);
                $lieuExecution = substr($lieuExecution, 0, -1);
                $odf->setVars('lieuExecution', $lieuExecution, true, $encoding);

                if ($consultation->getModePassation() == Atexo_Config::getParameter('MODE_PASSATION_AU_RABAIS')) {
                    $modePassationConsultation = Prado::localize('AU_RABAIS');
                } elseif ($consultation->getModePassation() == Atexo_Config::getParameter('MODE_PASSATION_OFFRE_PRIX')) {
                    $modePassationConsultation = Prado::localize('SUR_OFFRE_DE_PRIX');
                }
                $odf->setVars('modePassation', $modePassationConsultation, true, $encoding);

                $formuleSignature = self::getInfoFromReferentielConsultation(Atexo_Config::getParameter('FORMULE_SIGNATURE'), $consultation->getId(), 0, $acronyme);
                $odf->setVars('FORMULE_SIGNATURE', $formuleSignature, true, $encoding);

                $texteDossierAdministratif = $odf->setSegment('TexteDossierAdministratif');
                $lignePieceDossierAdministratif = $odf->setSegment('lignePieceDossierAdmin');
                if ($consultation->getPiecesDossierAdminTraduit()) {
                    $lignePieceDossierAdministratif->setVars('piece', $consultation->getPiecesDossierAdminTraduit(), true, $encoding);
                    $lignePieceDossierAdministratif->merge();
                    $texteDossierAdministratif->merge();
                }
                $odf->mergeSegment($texteDossierAdministratif);
                $odf->mergeSegment($lignePieceDossierAdministratif);

                $texteDossierTech = $odf->setSegment('TexteDossierTech');
                $lignePieceDossierTech = $odf->setSegment('lignePieceDossierTech');
                if ($consultation->getPiecesDossierTechTraduit()) {
                    $lignePieceDossierTech->setVars('piece', $consultation->getPiecesDossierTechTraduit(), true, $encoding);
                    $lignePieceDossierTech->merge();
                    $texteDossierTech->merge();
                }
                $odf->mergeSegment($texteDossierTech);
                $odf->mergeSegment($lignePieceDossierTech);

                $texteDossierAditif = $odf->setSegment('TexteDossierAditif');
                $lignePieceDossierAditif = $odf->setSegment('lignePieceDossierAditif');
                if ($consultation->getPiecesDossierAdditifTraduit()) {
                    $lignePieceDossierAditif->setVars('piece', $consultation->getPiecesDossierAdditifTraduit(), true, $encoding);
                    $lignePieceDossierAditif->merge();
                    $texteDossierAditif->merge();
                }
                $odf->mergeSegment($texteDossierAditif);
                $odf->mergeSegment($lignePieceDossierAditif);

                $ligneReponseElectronique = $odf->setSegment('ligneReponseElectronique');
                if ($consultation->getAutoriserReponseElectronique()) {
                    $urlPf = Atexo_MultiDomaine::getDomaine(Atexo_Config::getParameter('PF_URL_REFERENCE'));
                    $ligneReponseElectronique->setVars('PF_URL', $urlPf, true, $encoding);
                    $ligneReponseElectronique->merge();
                }
                $odf->mergeSegment($ligneReponseElectronique);

                if ($donnesComplementaire instanceof CommonTDonneeComplementaire) {
                    $prixAcquisition = $donnesComplementaire->getPrixAquisitionPlans();
                    $lignePrixAcquisition = $odf->setSegment('lignePrixAcquisition');
                    if ($prixAcquisition > 0 || '' != $prixAcquisition) {
                        $lignePrixAcquisition->setVars('prixAcquisition', $prixAcquisition, true, $encoding);
                        $lignePrixAcquisition->merge();
                    }
                    $odf->mergeSegment($lignePrixAcquisition);

                    $cautionProvisoireConsultation = $donnesComplementaire->getCautionProvisoire();
                    $ligneCautionProvisoireConsultation = $odf->setSegment('ligneCautionProvisoireConsultation');
                    if ($cautionProvisoireConsultation > 0 || '' != $cautionProvisoireConsultation) {
                        $ligneCautionProvisoireConsultation->setVars('cautionProvisoire', $cautionProvisoireConsultation, true, $encoding);
                        $ligneCautionProvisoireConsultation->merge();
                    }
                    $odf->mergeSegment($ligneCautionProvisoireConsultation);
                }
                $intituleLots = $odf->setSegment('INTITULES_LOTS');
                $infosLots = $odf->setSegment('INFOS_LOTS');
                $ligneCautionProvisoireLot = $odf->setSegment('ligneCautionProvisoireLot');
                $ligneEchantillon = $odf->setSegment('ligneEchantillon');
                $ligneReunion = $odf->setSegment('ligneReunion');
                $ligneAgrement = $odf->setSegment('ligneAgrement');
                $arrayLotObject = $consultation->getAllLots();
                $hasCautionLot = false;
                $hasEchantillonLot = false;
                $haslot = false;
                $hasReunionLot = false;
                $hasAgrements = false;
                $arrayQualifications = [];
                $texteVisite = $odf->setSegment('TexteVisite');
                $ligneVisite = $odf->setSegment('ligneVisite');
                $arrayVisiteLieu = (new Atexo_Consultation())->retreiveVisitesLieuxByRef($consultation->getId(), 0, $consultation->getOrganisme());
                if (is_array($arrayVisiteLieu) && count($arrayVisiteLieu) > 0) {
                    foreach ($arrayVisiteLieu as $onViste) {
                        if ($onViste->getLot()) {
                            $ligneVisite->setVars('numLot', Prado::localize('LOT_NUM').$onViste->getLot().' : ', true, $encoding);
                        } else {
                            $ligneVisite->setVars('numLot', '', true, $encoding);
                        }
                        $ligneVisite->setVars('dateVisite', Atexo_Util::iso2frnDateTime($onViste->getDate()), true, $encoding);
                        $ligneVisite->setVars('addVisite', Atexo_Util::nl2Chaine($onViste->getAdresseTraduit(), ' ').'', true, $encoding);
                        $ligneVisite->merge();
                    }
                    $texteVisite->merge();
                }
                $odf->mergeSegment($texteVisite);
                $odf->mergeSegment($ligneVisite);

                if (is_array($arrayLotObject) && count($arrayLotObject) > 0) {
                    foreach ($arrayLotObject as $onLotObject) {
                        $infosComplementaireLot = self::getInfoFromReferentielConsultation(Atexo_Config::getParameter('INFOS_CMPLT_LOT'), $consultation->getId(), $onLotObject->getLot(), $acronyme);
                        $infosLots->setVars('numLot', $onLotObject->getLot(), true, $encoding);
                        $infosLots->setVars('intituleLot', $onLotObject->getDescription($lang), true, $encoding);
                        $infosLots->setVars('descriptionLot', $onLotObject->getDescriptionDetail(), true, $encoding);
                        $infosLots->setVars('infosComplementaire', $infosComplementaireLot, true, $encoding);
                        $infosLots->merge();

                        $intituleLots->setVars('numLot', $onLotObject->getLot(), true, $encoding);
                        $intituleLots->setVars('intituleLot', $onLotObject->getDescription($lang), true, $encoding);
                        $intituleLots->merge();
                        $donnesComplementaireLot = $onLotObject->getDonneComplementaire();
                        if ($donnesComplementaireLot instanceof CommonTDonneeComplementaire) {
                            $cautionProvisoireLot = $donnesComplementaireLot->getCautionProvisoire();
                            if ($cautionProvisoireLot > 0) {
                                $hasCautionLot = true;
                                $ligneCautionProvisoireLot->setVars('numLot', Prado::localize('LOT_NUM').$onLotObject->getLot(), true, $encoding);
                                $ligneCautionProvisoireLot->setVars('cautionProvisoireLot', $cautionProvisoireLot, true, $encoding);
                                $ligneCautionProvisoireLot->merge();
                            }
                            if ($donnesComplementaireLot->getEchantillon()) {
                                $hasEchantillonLot = true;
                                $ligneEchantillon->setVars('numLot', Prado::localize('LOT_NUM').$onLotObject->getLot().' : ', true, $encoding);
                                $ligneEchantillon->setVars('adEchantillon', Atexo_Util::nl2Chaine($donnesComplementaireLot->getAddEchantillonTraduit(), ' '), true, $encoding); //getAddEchantillion()
                                $ligneEchantillon->setVars('dateEchantillon', Atexo_Util::iso2frnDateTime($donnesComplementaireLot->getDateLimiteEchantillon()), true, $encoding);
                                $ligneEchantillon->merge();
                            }
                            if ($donnesComplementaireLot->getReunion()) {
                                $hasReunionLot = true;
                                $ligneReunion->setVars('numLot', Prado::localize('LOT_NUM').$onLotObject->getLot().' : ', true, $encoding);
                                $ligneReunion->setVars('addReunion', Atexo_Util::nl2Chaine($donnesComplementaireLot->getAddReunionTraduit(), ' '), true, $encoding); //getAddReunion()
                                $ligneReunion->setVars('dateReunion', Atexo_Util::iso2frnDateTime($donnesComplementaireLot->getDateReunion()), true, $encoding);
                                $ligneReunion->merge();
                            }
                        }
                        $idsAgrements = $onLotObject->getAgrements();
                        if ($idsAgrements) {
                            $hasAgrements = true;
                            $ligneAgrement->setVars('numLot', Prado::localize('LOT_NUM').$onLotObject->getLot().' : ', true, $encoding);
                            $ligneAgrement->setVars('agrement', (new Atexo_Agrement())->retrieveLibelleAgrementsByIds($idsAgrements), true, $encoding);
                            $ligneAgrement->merge();
                        }
                        if ($onLotObject->getQualification()) {
                            $arrayQualifications[$onLotObject->getLot()] = (new Atexo_Qualifications())->displayListeLibelleDomaineByArrayId($onLotObject->getQualification(), true);
                        }
                    }
                    $haslot = true;
                }
                if ($donnesComplementaire instanceof CommonTDonneeComplementaire) {
                    if ($donnesComplementaire->getEchantillon()) {
                        $ligneEchantillon->setVars('numLot', '');
                        $ligneEchantillon->setVars('adEchantillon', Atexo_Util::nl2Chaine($donnesComplementaire->getAddEchantillonTraduit(), ' '), true, $encoding);
                        $ligneEchantillon->setVars('dateEchantillon', Atexo_Util::iso2frnDateTime($donnesComplementaire->getDateLimiteEchantillon()), true, $encoding);
                        $ligneEchantillon->merge();
                    }
                    if ($donnesComplementaire->getReunion()) {
                        $ligneReunion->setVars('numLot', '');
                        $ligneReunion->setVars('addReunion', Atexo_Util::nl2Chaine($donnesComplementaire->getAddReunionTraduit(), ' '), true, $encoding);
                        $ligneReunion->setVars('dateReunion', Atexo_Util::iso2frnDateTime($donnesComplementaire->getDateReunion()), true, $encoding);
                        $ligneReunion->merge();
                    }
                    $texteEchantillon = $odf->setSegment('TexteEchantillon');
                    if ($donnesComplementaire->getEchantillon() || $hasEchantillonLot) {
                        $texteEchantillon->merge();
                    }
                    $texteReunion = $odf->setSegment('TexteReunion');
                    if ($donnesComplementaire->getReunion() || $hasReunionLot) {
                        $texteReunion->merge();
                    }
                }
                $idsAgrements = $consultation->getAgrements();
                if ($idsAgrements) {
                    $hasAgrements = true;
                    $ligneAgrement->setVars('numLot', ' ', true, $encoding);
                    $ligneAgrement->setVars('agrement', (new Atexo_Agrement())->retrieveLibelleAgrementsByIds($idsAgrements), true, $encoding);
                    $ligneAgrement->merge();
                }
                $texteAgrement = $odf->setSegment('TexteAgrement');
                if ($hasAgrements) {
                    $texteAgrement->merge();
                }
                $qualifications = $odf->setSegment('qualifications');
                $texteQualifications = $odf->setSegment('TexteQualifications');
                if ($consultation->getQualification()) {
                    $arrayQualifications['0'] = (new Atexo_Qualifications())->displayListeLibelleDomaineByArrayId($consultation->getQualification(), true);
                }
                if (is_array($arrayQualifications) && count($arrayQualifications) > 0) {
                    foreach ($arrayQualifications as $key => $arrayQualification) {
                        //$Qualifs = $qualifications->Qualifs;
                        if (is_array($arrayQualification) && count($arrayQualification) > 0) {
                            foreach ($arrayQualification as $onQualif) {
                                $qualifications->Qualifs->setVars('type', $onQualif['type'], true, $encoding);
                                $qualifications->Qualifs->setVars('secteur', $onQualif['secteur'], true, $encoding);
                                $qualifications->Qualifs->setVars('qualification', $onQualif['qualification'], true, $encoding);
                                $qualifications->Qualifs->setVars('classe', $onQualif['classe'], true, $encoding);
                                $qualifications->Qualifs->merge();
                            }
                            if ($key) {
                                $qualifications->setVars('lot', Prado::localize('LOT_NUM')."  $key : \n", true, $encoding);
                            } else {
                                $qualifications->setVars('lot', '', true, $encoding);
                            }
                            $qualifications->merge();
                        }
                    }
                    //$qualifications->merge();
                    $texteQualifications->merge();
                    //$odf->mergeSegment($qualifications);
                }
                $odf->mergeSegment($infosLots);
                $odf->mergeSegment($texteQualifications);
                $odf->mergeSegment($qualifications);
                $odf->mergeSegment($texteEchantillon);
                $odf->mergeSegment($ligneEchantillon);
                $odf->mergeSegment($texteReunion);
                $odf->mergeSegment($ligneReunion);
                $odf->mergeSegment($texteAgrement);
                $odf->mergeSegment($ligneAgrement);
                $texteCautionLot = $odf->setSegment('TexteCautionProvisoireLot');
                if ($hasCautionLot) {
                    $texteCautionLot->merge();
                }
                if ($haslot) {
                    $odf->setVars('ligneVideintituleLot', "\n", true, $encoding);
                } else {
                    $odf->setVars('ligneVideintituleLot', '', true, $encoding);
                }
                $odf->mergeSegment($texteCautionLot);
                $odf->mergeSegment($ligneCautionProvisoireLot);
                $odf->mergeSegment($intituleLots);
                $typeProcedure = (new Atexo_Consultation_ProcedureType())->getCommonTypeProcedureById($consultation->getIdTypeProcedure());
                $libelleTypeProc = $typeProcedure->getLibeleTypeProcedureTraduit(); //getLibelleTypeProcedure();
                if ('fr' == trim($lang)) {
                    $libelleTypeProc = strtoupper(Atexo_Util::OterAccents((Atexo_Util::isFirstCharVowel($libelleTypeProc) ? "' " : 'e ').$libelleTypeProc));
                    $odf->setVars('TYPE_PROCEDURE', $libelleTypeProc, $encoding);
                } else {
                    $odf->setVars('TYPE_PROCEDURE', $libelleTypeProc, true, $encoding);
                }
                $modele = $typeProcedure->getIdModele();
                switch ($modele) {
                    case Atexo_Config::getParameter('MODELE_AVIS_APPEL_OFFRE'):
                        $modeles = (new Atexo_Referentiel())->retrieveValeurReferentielByReferentiel(Atexo_Config::getParameter('MODELE_AVIS_APPEL_OFFRE'), $groupBy = 'libelle2');
                        break;

                    case Atexo_Config::getParameter('MODELE_AVIS_APPEL_OFFRE_AVEC_SELECTION'):
                        $modeles = (new Atexo_Referentiel())->retrieveValeurReferentielByReferentiel(Atexo_Config::getParameter('MODELE_AVIS_APPEL_OFFRE_AVEC_SELECTION'), $groupBy = 'libelle2');
                        break;

                    case Atexo_Config::getParameter('MODELE_AVIS_CONCOURS'):
                        $modeles = (new Atexo_Referentiel())->retrieveValeurReferentielByReferentiel(Atexo_Config::getParameter('MODELE_AVIS_CONCOURS'), $groupBy = 'libelle2');
                        break;
                }
                $odf->setVars('nomTypeProcedure', $modeles['01']['libelle_valeur_referentiel'], true, $encoding);
                $odf->setVars('dossierAvis', $modeles['02']['libelle_valeur_referentiel'], true, $encoding);
                $odf->setVars('articles', $modeles['03']['libelle_valeur_referentiel'], true, $encoding);
                if ($oldLang) {
                    $globalization = $this->getApplication()->Modules['globalization'];
                    $globalization->setTranslationCatalogue('messages.'.$oldLang);
                    Atexo_CurrentUser::writeToSession('lang', $oldLang);
                }
                if ($toFile) {
                    $pathFile = Atexo_Config::getParameter('COMMON_TMP').'Avis_'.$lang.'_'.session_id().time().'_fichier.odt';
                    $odf->saveToDisk($pathFile);
                    /*$pdfContent = PdfGeneratorClient::genererPdf($pathFile);
                    DownloadFile::downloadFileContent(Atexo_Config::getParameter('NOM_FICHIER_DETAIL_CONSULTATION').'.pdf', $pdfContent);
                        @unlink($odtFilePath);exit;*/
                    return $pathFile;
                } else {
                    $odf->exportAsAttachedFile('test.odt');
                    exit;
                }
            } else {
                return -1;
            }
        }
    }

    public function AddAvisToConsultation($file, $commonConsultation, $lang, $returnAvis = false, $pdfContent = '', $fileName = '', $callFromCron = false)
    {
        if (is_file($file) && $commonConsultation instanceof CommonConsultation) {
            // la création
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            if (!$returnAvis) {
                $ancienAvis = (new Atexo_Publicite_Avis())->retreiveListFormulaireLibreByTypeDocGenere($commonConsultation->getId(), $commonConsultation->getOrganisme, Atexo_Config::getParameter('TYPE_DOC_GENERE_AVIS'), $lang);
                if (is_array($ancienAvis) && count($ancienAvis)) {
                    foreach ($ancienAvis as $oneAvis) {
                        $oneAvis->setStatut(Atexo_Config::getParameter('DESTINATAIRE_SUSPENDU'));
                        $oneAvis->save($connexionCom);
                    }
                }
            }
            //echo $file;exit;
            $newAvis = new CommonAVIS();
            $newAvis->setOrganisme($commonConsultation->getOrganisme());
            $newAvis->setConsultationId($commonConsultation->getId());
            $newAvis->setDateCreation(date('Y-m-d H:i'));
            if ($pdfContent) {
                $newAvis->setType(Atexo_Config::getParameter('TYPE_FORMAT_PDF_TED'));
            } else {
                $newAvis->setType(Atexo_Config::getParameter('TYPE_FORMAT_LIBRE_FICHIER_JOINT'));
            }
            //enregistrement
            if (!$pdfContent) {
                $pdfContent = (new PdfGeneratorClient())->genererPdf($file);
            }
            $filePdf = $file.'.pdf';
            Atexo_Util::write_file($filePdf, $pdfContent);

            $atexoBlob = new Atexo_Blob();
            $atexoCrypto = new Atexo_Crypto();
            $arrayTimeStampAvis = $atexoCrypto->timeStampFile($filePdf);
            if (is_array($arrayTimeStampAvis)) {
                $newAvis->setHorodatage($arrayTimeStampAvis['horodatage']);
                $newAvis->setUntrusteddate($arrayTimeStampAvis['untrustedDate']);
            }
            if (!$fileName) {
                $fileName = 'Avis.pdf';
            }
            $avisIdBlob = $atexoBlob->insert_blob($fileName, $filePdf, $commonConsultation->getOrganisme());
            if (!$callFromCron) {
                $newAvis->setAgentId(Atexo_CurrentUser::getIdAgentConnected());
            }
            $newAvis->setAvis($avisIdBlob);
            $newAvis->setNomFichier($fileName);
            @unlink($file);
            //Module PubliciteOpoce active
            if (Atexo_Module::isEnabled('PubliciteOpoce')) {
                $newAvis->setStatut(Atexo_Config::getParameter('DESTINATAIRE_EN_ATTENTE'));
            } else {
                $newAvis->setStatut(Atexo_Config::getParameter('DESTINATAIRE_PUBLIE'));
            }
            $newAvis->setDatePub(date('Y-m-d'));
            $newAvis->setTypeDocGenere(Atexo_Config::getParameter('TYPE_DOC_GENERE_AVIS'));
            $newAvis->setLangue($lang);
            $newAvis->save($connexionCom);
            if ($returnAvis) {
                return $newAvis;
            }

            return true;
        }

        return false;

        //la publicite
    }

    public function getInfoFromReferentielConsultation($codeLibelle, $consultationId, $lot, $organisme, $afficherLibelle = false)
    {
        $ltRef = (new Atexo_Referentiel_Referentiel())->getLtReferentielByLibelle($codeLibelle);
        if ($ltRef) {
            $valeurRef = (new Atexo_Referentiel_Referentiel())->getCommonLtReferentielByRefConsultationAndLot($consultationId, $lot, $ltRef->getId(), $organisme);
            if ($valeurRef) {
                $value = '';
                if ($afficherLibelle) {
                    $value .= Prado::localize($codeLibelle)."  :\n";
                }
                $value .= $valeurRef->getValeurPrincipaleLtReferentiel();
                if ($valeurRef->getValeurSecondaireLtReferentiel()) {
                    $value .= ', '.$valeurRef->getValeurSecondaireLtReferentiel();
                }

                return $value;
            }
        }

        return '';
    }

    /**
     * Génère les avis de publicité pour la presse et le portail.
     */
    public function generationAvisPublicite($consultation, $modele, $toFile, $lang, $idAvisPub, $datePubConsultation = false)
    {
        $avisPub = null;
        $fileModel = $modele.$lang.'.odt';
        $encoding = Atexo_Config::getParameter('HTTP_ENCODING');
        if ($consultation instanceof CommonConsultation) {
            if (is_file($fileModel)) {//verifier si le fichier existe pour cette langue.
                /*$oldLang = Atexo_CurrentUser::readFromSession("lang");
                $globalization = $this->getApplication()->Modules["globalization"];
                $globalization->setTranslationCatalogue("messages.".$lang);
                Atexo_CurrentUser::writeToSession("lang", $lang);*/

                //$consultation = new CommonConsultation();
                //print_r($consultation); exit;

                if (!is_dir(Atexo_Config::getParameter('COMMON_TMP').'/tmpOdt')) {
                    mkdir(Atexo_Config::getParameter('COMMON_TMP').'/tmpOdt');
                }
                $config = [
                                'ZIP_PROXY' => \PhpZipProxy::class,
                                'DELIMITER_LEFT' => '{',
                                'DELIMITER_RIGHT' => '}',
                                'PATH_TO_TMP' => Atexo_Config::getParameter('COMMON_TMP').'/tmpOdt',
                                ];
                $odf = new Atexo_Odf($fileModel, $config);
                $acronyme = $consultation->getOrganisme();
                $datePublicationAvis = '';

                $datePublicationAvis = substr(Atexo_Util::iso2frnDateTime($datePubConsultation), 0, 10);
                if ($idAvisPub && !$datePublicationAvis) {
                    $avisPub = (new Atexo_Publicite_AvisPub())->getAvisById($idAvisPub, $acronyme, $consultation->getId());
                    if ($avisPub) {
                        $datePublicationAvis = Atexo_Util::iso2frnDateTime($avisPub->getDatePublication());
                    }
                }

                $serviceConsultation = $consultation->getServiceId();
                if (0 != $serviceConsultation) {
                    $libellePereServiceAgentConnecte = '';
                    $libellePremierFilsServiceAgentConnecte = '';
                    $libelleDivision = '';
                    $listeParents = Atexo_EntityPurchase::getArParentsId($serviceConsultation, $consultation->getOrganisme(), false);
                    if (1 == count($listeParents)) {//Cas n=2, on doit voir seulement le service de l'agent connecté
                        $libelleServiceParentDeAgentConnecte = Atexo_EntityPurchase::getLibelleServiceAgentById($listeParents[1], $consultation->getOrganisme());
                        $libelleDivision = $libelleServiceParentDeAgentConnecte;
                    } elseif (2 == count($listeParents)) {//Cas n=3, on doit voir le service de l'agent connecté et le père
                        $libelleServiceParentDeAgentConnecte = Atexo_EntityPurchase::getLibelleServiceAgentById($listeParents[0], $consultation->getOrganisme());
                        $libelleDivision = $libelleServiceParentDeAgentConnecte;
                    } elseif (count($listeParents) >= 3) {//Cas n>3, on doit voir le service de l'agent connecté et les deux premiers parents de l'arborescence
                        $libellePereServiceAgentConnecte = Atexo_EntityPurchase::getLibelleServiceAgentById($listeParents[count($listeParents) - 2], $consultation->getOrganisme());
                        $libellePremierFilsServiceAgentConnecte = Atexo_EntityPurchase::getLibelleServiceAgentById($listeParents[count($listeParents) - 3], $consultation->getOrganisme());
                        $libelleDivision = $libellePremierFilsServiceAgentConnecte;
                    }
                    $libelleServiceAgentConnecte = Atexo_EntityPurchase::getLibelleServiceAgentById($serviceConsultation, $consultation->getOrganisme());
                    $odf->setVars('ORGANISME', Atexo_Util::atexoHtmlEntitiesDecode($libellePereServiceAgentConnecte), true, $encoding);
                    $odf->setVars('DIVISION', Atexo_Util::atexoHtmlEntitiesDecode($libelleDivision), true, $encoding);
                    $odf->setVars('SERVICE', Atexo_Util::atexoHtmlEntitiesDecode($libelleServiceAgentConnecte), true, $encoding);
                } else {
                    $odf->setVars('ORGANISME', '', true, $encoding);
                    $odf->setVars('DIVISION', '', true, $encoding);
                    $odf->setVars('SERVICE', '', true, $encoding);
                }

                $hasAutresInfos = false;
                $hasConditionsParticipation = false;
                $hasVisiteLieuxReunionInfos = false;
                $hasConditionsObtentionDossierSoumission = false;
                $hasReceptionOffres = false;
                $hasDateOuverture = false;
                $hasLieuOuverture = false;
                $hasNumJo = false;
                $hasDateJo = false;

                // Type de procedure
                $typeProcedure = (new Atexo_Consultation_ProcedureType())->getCommonTypeProcedureById($consultation->getIdTypeProcedure());
                $libelleTypeProc = $typeProcedure->getLibeleTypeProcedureTraduit(); //getLibelleTypeProcedure();
                //On enleve les trois premiers caractères du type de procedure et on reconstruit la chaine
                $arrayLibellesProcedures = explode(' ', $libelleTypeProc);
                unset($arrayLibellesProcedures[0]);
                $odf->setVars('TYPE_PROCEDURE', implode(' ', $arrayLibellesProcedures), true, $encoding);
                //Type de marché
                $categorieConsObject = Atexo_Consultation_Category::retrieveCategorie($consultation->getCategorie());
                $odf->setVars('TYPE_MARCHE', $categorieConsObject->getLibelleTraduit(), true, $encoding);
                if ($consultation->getIdTypeAvis() != Atexo_Config::getParameter('TYPE_AVIS_ATTRIBUTION') && $consultation->getIdTypeAvis() != Atexo_Config::getParameter('TYPE_AVIS_INFORMATION')) {
                    try {
                        //Bloc date et heure d'ouverture
                        $ligneTexteBlocDateOuv = $odf->setSegment('TexteBlocDateOuv');
                    } catch (\OdfException $e) {
                        // On ne fait rien si le segment n'existe pas
                    }

                    try {
                        ///Date et heure d'ouverture
                        $ligneDateOuverture = $odf->setSegment('dateOuv');
                        if ($consultation->getDatefin() && '0000-00-00 00:00:00' != $consultation->getDatefin()) {
                            $ligneDateOuverture->setVars('HEURE', date_format(date_create($consultation->getDatefin()), 'H:i'), true, $encoding);
                            $ligneDateOuverture->setVars('DATE_OUVERTURE', date_format(date_create($consultation->getDatefin()), 'd/m/Y'), true, $encoding);
                            $ligneDateOuverture->merge();
                            $hasDateOuverture = true;
                        }
                        $odf->mergeSegment($ligneDateOuverture);
                    } catch (\OdfException $e) {
                        // On ne fait rien si le segment n'existe pas
                    }

                    try {
                        //Lieu d'ouverture
                        $ligneLieuOuverture = $odf->setSegment('lieuOuv');
                        if ($consultation->getLieuOuverturePlisTraduit()) {
                            $ligneLieuOuverture->setVars('lieuOuverturePlis', Atexo_Util::nl2Chaine(self::corrigerCaracteresSpeciaux($consultation->getLieuOuverturePlisTraduit()), ' '), true, $encoding); //getLieuOuverturePlis()
                            $ligneLieuOuverture->merge();
                            $hasLieuOuverture = true;
                        }
                        $odf->mergeSegment($ligneLieuOuverture);
                    } catch (\OdfException $e) {
                        // On ne fait rien si le segment n'existe pas
                    }

                    try {
                        if (
                            $ligneTexteBlocDateOuv
                            && ($hasDateOuverture || $hasLieuOuverture)
                        ) {
                            $ligneTexteBlocDateOuv->merge();
                            $odf->mergeSegment($ligneTexteBlocDateOuv);
                        }
                    } catch (\OdfException $e) {
                        // On ne fait rien si le segment n'existe pas
                    }

                } else {
                    try {
                        //Bloc date et heure d'ouverture
                        $ligneTexteBlocDateOuv = $odf->setSegment('TexteBlocDateOuv');
                        $odf->mergeSegment($ligneTexteBlocDateOuv);
                    } catch (\OdfException $e) {
                        // On ne fait rien si le segment n'existe pas
                    }

                    try {
                        //Date et heure d'ouverture
                        $ligneDateOuverture = $odf->setSegment('dateOuv');
                        $odf->mergeSegment($ligneDateOuverture);
                    } catch (\OdfException $e) {
                        // On ne fait rien si le segment n'existe pas
                    }

                    try {
                        //Lieu d'ouverture
                        $ligneLieuOuverture = $odf->setSegment('lieuOuv');
                        $odf->mergeSegment($ligneLieuOuverture);
                    } catch (\OdfException $e) {
                        // On ne fait rien si le segment n'existe pas
                    }
                }
                //Intitulé marché
                $ligneIntitule = $odf->setSegment('intMarche');
                if ($consultation->getIntituleTraduit()) {
                    $ligneIntitule->setVars('INTITULE_MARCHE', self::corrigerCaracteresSpeciaux($consultation->getIntituleTraduit()), true, $encoding);
                    $ligneIntitule->merge();
                }
                $odf->mergeSegment($ligneIntitule);
                //Objet
                if ($consultation->getObjetTraduit()) {
                    $odf->setVars('OBJET', self::corrigerCaracteresSpeciaux($consultation->getObjetTraduit()), true, $encoding);
                } else {
                    $odf->setVars('OBJET', '', true, $encoding);
                }

                try {
                    //Informations lot
                    $infosLots = $odf->setSegment('INFOS_LOTS');
                    $arrayLotObject = $consultation->getAllLots();
                    if (is_array($arrayLotObject) && count($arrayLotObject) > 0) {
                        foreach ($arrayLotObject as $onLotObject) {
                            $infosComplementaireLot = self::getInfoFromReferentielConsultation(Atexo_Config::getParameter('INFOS_CMPLT_LOT'), $consultation->getId(), $onLotObject->getLot(), $acronyme);
                            $infosLots->setVars('numLot', self::corrigerCaracteresSpeciaux($onLotObject->getLot()), true, $encoding);
                            $infosLots->setVars('intituleLot', self::corrigerCaracteresSpeciaux($onLotObject->getDescription($lang)), true, $encoding);
                            if ($onLotObject->getDescriptionDetail()) {
                                $infosLots->setVars('descriptionLot', '- '.Prado::localize('DEFINE_DESCRIPTION_SUCCINTE').' : '.self::corrigerCaracteresSpeciaux($onLotObject->getDescriptionDetail()), true, $encoding);
                            } else {
                                $infosLots->setVars('descriptionLot', '', true);
                            }
                            if ('' != $infosComplementaireLot) {
                                $infosLots->setVars('infosComplementaire', '- '.Prado::localize('DEFINE_INFORMATIONS_COMPLEMENTAIRES').' : '.self::corrigerCaracteresSpeciaux($infosComplementaireLot), true, $encoding);
                            } else {
                                $infosLots->setVars('infosComplementaire', '', true);
                            }
                            $infosLots->merge();
                        }
                        //$haslot = true;
                    }
                    $odf->mergeSegment($infosLots);
                } catch (\OdfException $e) {
                    // On ne fait rien si le segment n'existe pas
                }

                //Autres pouvoirs adjudicateurs
                $pouvoirAdjudicateur = self::getInfoFromReferentielConsultation(Atexo_Config::getParameter('POUVOIR_ADJUDICATEUR'), $consultation->getId(), 0, $acronyme);
                if ('' != $pouvoirAdjudicateur) {
                    $odf->setVars('POUVOIRS_ADJUDICATEURS',
                        self::corrigerCaracteresSpeciaux($pouvoirAdjudicateur), true, $encoding);
                } else {
                    $odf->setVars('POUVOIRS_ADJUDICATEURS', '', true, $encoding);
                }

                try {
                    //Conditions de participation
                    $ligneConditionParticipation = $odf->setSegment('condPart');
                    $conditionParticipations = self::getInfoFromReferentielConsultation(Atexo_Config::getParameter('CONDITION_PARTICIPATION'), $consultation->getId(), 0, $acronyme);
                    if ($conditionParticipations) {
                        $ligneConditionParticipation->setVars('CONDITION_PARTICIPATION', self::corrigerCaracteresSpeciaux($conditionParticipations), true, $encoding);
                        $ligneConditionParticipation->merge();
                        $hasConditionsParticipation = true;
                    }
                    $odf->mergeSegment($ligneConditionParticipation);  } catch (\OdfException $e) {
                    // On ne fait rien si le segment n'existe pas
                }

                try {
                    //Conditions d'obtention du dossier de soumission
                    $ligneConditionObtentionDossierSoum = $odf->setSegment('dossierSoum');
                } catch (\OdfException $e) {
                    // On ne fait rien si le segment n'existe pas
                }

                try {
                    $ligneConditionObtentionCahierCharge = $odf->setSegment('dossier_sou');
                    $ligneConditionDocsContractuels = $odf->setSegment('dossier_doc_contr');
                } catch (\OdfException $e) {
                    // On ne fait rien si le segment n'existe pas
                }

                $conditionObtentionCahierCharge = '';
                $conditionObtentionDossierSoumission = '';
                $adresseDepotOffres = '';
                if ($consultation instanceof CommonConsultation) {
                    $conditionObtentionCahierCharge .= $consultation->getAdresseRetraisDossiersTraduit();
                    if ($conditionObtentionCahierCharge && $ligneConditionObtentionCahierCharge) {
                        $ligneConditionObtentionCahierCharge->setVars('CONDITION_OBTENTION_CAHIER_CHARGE', self::corrigerCaracteresSpeciaux($conditionObtentionCahierCharge), true, $encoding);
                        $ligneConditionObtentionCahierCharge->merge();
                    }
                    $conditionObtentionDossierSoumission = $consultation->getAdresseRetraisDossiersTraduit();
                    if ($conditionObtentionDossierSoumission && $ligneConditionObtentionDossierSoum) {
                        $ligneConditionObtentionDossierSoum->setVars('CONDITION_OBTENTION_DOSSIER_SOUMISSION', self::corrigerCaracteresSpeciaux($conditionObtentionDossierSoumission), true, $encoding);
                        $ligneConditionObtentionDossierSoum->merge();
                        $hasConditionsObtentionDossierSoumission = true;
                    }
                    if ($conditionObtentionDossierSoumission && $ligneConditionDocsContractuels) {
                        $ligneConditionDocsContractuels->setVars('CONDITION_OBTENTION_DOCUMENTS_CONTRACTUELS', self::corrigerCaracteresSpeciaux($conditionObtentionDossierSoumission), true, $encoding);
                        $ligneConditionDocsContractuels->merge();
                    }
                    $adresseDepotOffres = $consultation->getAdresseDepotOffresTraduit();
                }

                if ($ligneConditionObtentionDossierSoum) {
                    $odf->mergeSegment($ligneConditionObtentionDossierSoum);
                }

                if ($ligneConditionObtentionCahierCharge) {
                    $odf->mergeSegment($ligneConditionObtentionCahierCharge);
                }

                if ($ligneConditionDocsContractuels) {
                    $odf->mergeSegment($ligneConditionDocsContractuels);
                }

                try {
                    //Adresse de depot des offres
                    $ligneAdrDepotCandidature = $odf->setSegment('adrDepCand');
                    if ($adresseDepotOffres) {
                        $ligneAdrDepotCandidature->setVars('adresseDepotOffres', Atexo_Util::nl2Chaine(self::corrigerCaracteresSpeciaux($adresseDepotOffres), ' '), true, $encoding); //getAdresseDepotOffres()
                        $ligneAdrDepotCandidature->merge();
                    }
                    $odf->mergeSegment($ligneAdrDepotCandidature);
                } catch (\OdfException $e) {
                    // On ne fait rien si le segment n'existe pas
                }

                try {
                    //Informations complémentaires
                    $ligneAutresInfos = $odf->setSegment('autreInf');
                    $autreInformation = self::getInfoFromReferentielConsultation(
                        Atexo_Config::getParameter('AUTRE_INFOS'),
                        $consultation->getId(),
                        0,
                        $acronyme
                    );
                    if ($autreInformation) {
                        $ligneAutresInfos->setVars(
                            'AUTRES_INFOS',
                            self::corrigerCaracteresSpeciaux($autreInformation),
                            true,
                            $encoding
                        );
                        $ligneAutresInfos->merge();
                        $hasAutresInfos = true;
                    }
                    $odf->mergeSegment($ligneAutresInfos);
                } catch (\OdfException $e) {
                    // On ne fait rien si le segment n'existe pas
                }

                //Formule de signature
                $formuleSignature = self::getInfoFromReferentielConsultation(Atexo_Config::getParameter('FORMULE_SIGNATURE'), $consultation->getId(), 0, $acronyme);
                if ($formuleSignature) {
                    $odf->setVars('FORMULE_SIGNATURE', self::corrigerCaracteresSpeciaux($formuleSignature), true, $encoding);
                } else {
                    $odf->setVars('FORMULE_SIGNATURE', '', true, $encoding);
                }
                //Numéro de l'avis
                if ($consultation->getReferenceUtilisateur()) {
                    $odf->setVars('NUMERO_AVIS', self::corrigerCaracteresSpeciaux($consultation->getReferenceUtilisateur()), true, $encoding);
                } else {
                    $odf->setVars('NUMERO_AVIS', '', true, $encoding);
                }

                try {
                    //Texte à rectifier dans l'avis original
                    $ligneTexteRectif = $odf->setSegment('texteRect');
                    $texteARectifierAvisOriginal = self::getInfoFromReferentielConsultation(Atexo_Config::getParameter('TEXTE_A_RECTIFIER_AVIS_ORIGINAL'), $consultation->getId(), 0, $acronyme);
                    if ($texteARectifierAvisOriginal) {
                        $ligneTexteRectif->setVars('TEXTE_A_RECTIFIER_DANS_AVIS_ORIGINAL', self::corrigerCaracteresSpeciaux($texteARectifierAvisOriginal), true, $encoding);
                        $ligneTexteRectif->merge();
                    }
                    $odf->mergeSegment($ligneTexteRectif);
                } catch (\OdfException $e) {
                    // On ne fait rien si le segment n'existe pas
                }

                try {
                    //Date à rectifier dans l'avis original
                    $ligneDateRectif = $odf->setSegment('dateRect');
                    $dateARectifierAvisOriginal = self::getInfoFromReferentielConsultation(Atexo_Config::getParameter('DATE_A_RECTIFIER_AVIS_ORIGINAL'), $consultation->getId(), 0, $acronyme);
                    if ($dateARectifierAvisOriginal) {
                        $ligneDateRectif->setVars('DATE_A_RECTIFIER_DANS_AVIS_ORIGINAL', self::corrigerCaracteresSpeciaux($dateARectifierAvisOriginal), true, $encoding);
                        $ligneDateRectif->merge();
                    }
                    $odf->mergeSegment($ligneDateRectif);
                } catch (\OdfException $e) {
                    // On ne fait rien si le segment n'existe pas
                }

                try {
                    //Texte à ajouter dans l'avis original
                    $ligneTexteAjout = $odf->setSegment('texteAjout');
                    $texteAAjouterAvisOriginal = self::getInfoFromReferentielConsultation(Atexo_Config::getParameter('TEXTE_A_AJOUTER_AVIS_ORIGINAL'), $consultation->getId(), 0, $acronyme);
                    if ($texteAAjouterAvisOriginal) {
                        $ligneTexteAjout->setVars('TEXTE_A_AJOUTER_DANS_AVIS_ORIGINAL', self::corrigerCaracteresSpeciaux($texteAAjouterAvisOriginal), true, $encoding);
                        $ligneTexteAjout->merge();
                    }
                    $odf->mergeSegment($ligneTexteAjout);
                } catch (\OdfException $e) {
                    // On ne fait rien si le segment n'existe pas
                }

                try {
                    //Modalités de visites des lieux et reunions d'information
                    $ligneVisiteLieuxReunion = $odf->setSegment('visLieu');
                    $visiteLieuxReunionsInformation = self::getInfoFromReferentielConsultation(Atexo_Config::getParameter('MODALITES_VISITES_LIEUX_REUNIONS'), $consultation->getId(), 0, $acronyme);
                    if ($visiteLieuxReunionsInformation) {
                        $ligneVisiteLieuxReunion->setVars('VISITE_LIEUX_REUNION_INFORMATION', self::corrigerCaracteresSpeciaux($visiteLieuxReunionsInformation), true, $encoding);
                        $ligneVisiteLieuxReunion->merge();
                        $hasVisiteLieuxReunionInfos = true;
                    }
                    $odf->mergeSegment($ligneVisiteLieuxReunion);
                } catch (\OdfException $e) {
                    // On ne fait rien si le segment n'existe pas
                }

                try {
                    $odf->setVars('NUMERO_AVIS_ORIGINAL', self::corrigerCaracteresSpeciaux($consultation->getReferenceUtilisateur()), true, $encoding);
                } catch (\OdfException $e) {
                    // On ne fait rien si le segment n'existe pas
                }

                try {
                    //Date de publication
                    $ligneDatePublication = $odf->setSegment('datePub');
                    if ($datePublicationAvis) {
                        $ligneDatePublication->setVars('DATE_PUBLICATION', $datePublicationAvis, true, $encoding);
                        $ligneDatePublication->merge();
                    }
                    $odf->mergeSegment($ligneDatePublication);
                } catch (\OdfException $e) {
                    // On ne fait rien si le segment n'existe pas
                }

                try {
                    $ligneDatePublicationJoue = $odf->setSegment('datePubJoue');
                    if ($datePublicationAvis) {
                        $ligneDatePublicationJoue->setVars('DATE_PUBLICATION_JOUE', $datePublicationAvis, true, $encoding);
                        $ligneDatePublicationJoue->merge();
                    }
                    $odf->mergeSegment($ligneDatePublicationJoue);
                } catch (\OdfException $e) {
                    // On ne fait rien si le segment n'existe pas
                }

                try {
                    $odf->setVars('DATE_PUBLICATION_CONSULTATION', substr(Atexo_Util::iso2frnDateTime($consultation->getDateMiseEnLigneCalcule()), 0, 10), true, $encoding);
                } catch (\OdfException $e) {
                    // On ne fait rien si le segment n'existe pas
                }

                try {
                    //Adresse de reception des offres
                    //Conditions d'obtention du dossier de soumission
                    $ligneAdrReceptionOffres = $odf->setSegment('recOffre');
                    if ($consultation->getAdresseDepotOffresTraduit()) {
                        $ligneAdrReceptionOffres->setVars('RECEPTION_OFFRES', Atexo_Util::nl2Chaine(self::corrigerCaracteresSpeciaux($consultation->getAdresseDepotOffresTraduit()), ' '), true, $encoding);
                        $ligneAdrReceptionOffres->merge();
                        $hasReceptionOffres = true;
                    }
                    $odf->mergeSegment($ligneAdrReceptionOffres);
                } catch (\OdfException $e) {
                    // On ne fait rien si le segment n'existe pas
                }

                try {
                    //Informations complémentaires offres
                    $ligneInfosComplementaires = $odf->setSegment('infoComp');
                    if ($autreInformation) {
                        $ligneInfosComplementaires->setVars('INFOS_COMPLEMENTAIRES_OFFRES', self::corrigerCaracteresSpeciaux($autreInformation), true, $encoding);
                        $ligneInfosComplementaires->merge();
                    }
                    $odf->mergeSegment($ligneInfosComplementaires);
                } catch (\OdfException $e) {
                    // On ne fait rien si le segment n'existe pas
                }

                if (!$avisPub instanceof CommonAvisPub) {
                    $avisPub = (new Atexo_Publicite_AvisPub())->getAvisById($idAvisPub, $acronyme, $consultation->getId());
                }
                if ($avisPub instanceof CommonAvisPub && $avisPub->getTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_PUB_RECTIFICATIF')) {
                    $xml = '';
                    $destinataireOpoce = (new Atexo_Publicite_AvisPub())->getListeDestinataires($idAvisPub, $acronyme, Atexo_Config::getParameter('TYPE_SUPPORT_OPOCE'));
                    if ($destinataireOpoce instanceof CommonDestinatairePub) {
                        $avisOpoce = (new Atexo_Publicite_AvisPub())->getObjetAvisOpoceByIdDest($destinataireOpoce->getId(), $acronyme);
                        $dateAvisJo = $odf->setSegment('dateJo');
                        $numAvisJo = $odf->setSegment('numJo');
                        if ($avisOpoce instanceof CommonFormXmlDestinataireOpoce) {
                            $xml = base64_decode($avisOpoce->getXml());
                            $dom = new DomDocument();
                            //Chargement du fichier XML
                            $dom->loadXML($xml);
                            $additionalInformationCorrigendums = $dom->getElementsByTagName('ADDITIONAL_INFORMATION_CORRIGENDUM');
                            foreach ($additionalInformationCorrigendums as $additionalInformationCorrigendum) {//print_r($additionalInformationCorrigendum);exit;
                                $fdAdditionalInformationCorrigendums = $additionalInformationCorrigendum->getElementsByTagName('FD_ADDITIONAL_INFORMATION_CORRIGENDUM');
                                foreach ($fdAdditionalInformationCorrigendums as $fdAdditionalInformationCorrigendum) {
                                    $proceduresIcars = $fdAdditionalInformationCorrigendum->getElementsByTagName('PROCEDURES_ICAR');
                                    foreach ($proceduresIcars as $proceduresIcar) {
                                        $administrativeInformations = $proceduresIcar->getElementsByTagName('ADMINISTRATIVE_INFORMATION');
                                        foreach ($administrativeInformations as $administrativeInformation) {
                                            $noticePublications = $administrativeInformation->getElementsByTagName('NOTICE_PUBLICATION');
                                            foreach ($noticePublications as $noticePublication) {
                                                $dateOjs = $noticePublication->getElementsByTagName('DATE_OJ');
                                                foreach ($dateOjs as $dateOj) {
                                                    $days = $dateOj->getElementsByTagName('DAY');
                                                    $jour = '';
                                                    foreach ($days as $day) {
                                                        $jour = $day->nodeValue;
                                                    }
                                                    $months = $dateOj->getElementsByTagName('MONTH');
                                                    $mois = '';
                                                    foreach ($months as $month) {
                                                        $mois = $month->nodeValue;
                                                    }
                                                    $years = $dateOj->getElementsByTagName('YEAR');
                                                    $annee = '';
                                                    foreach ($years as $year) {
                                                        $annee = $year->nodeValue;
                                                    }

                                                    if ($jour || $mois || $annee) {
                                                        $dateAvisJo->setVars('DATE_JO', $jour.'/'.$mois.'/'.$annee, true);
                                                        $dateAvisJo->merge();
                                                        $hasDateJo = true;
                                                    }
                                                }
                                                $noticeNumberOjs = $noticePublication->getElementsByTagName('NOTICE_NUMBER_OJ');
                                                foreach ($noticeNumberOjs as $noticeNumberOj) {
                                                    if ($noticeNumberOj->nodeValue) {
                                                        $numAvisJo->setVars('NUMERO_AVIS_JO', $noticeNumberOj->nodeValue, true, $encoding);
                                                        $numAvisJo->merge();
                                                        $hasNumJo = true;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        $odf->mergeSegment($dateAvisJo);
                        $odf->mergeSegment($numAvisJo);
                    }
                }

                try {
                    $ligneTexteAutresInfos = $odf->setSegment('texte_infos');
                    if ($hasAutresInfos || $hasConditionsObtentionDossierSoumission ||
                        $hasConditionsParticipation || $hasReceptionOffres || $hasVisiteLieuxReunionInfos) {
                        $ligneTexteAutresInfos->merge();
                    }
                    $odf->mergeSegment($ligneTexteAutresInfos);
                } catch (\OdfException $e) {
                    // On ne fait rien si le segment n'existe pas
                }

                try {
                    if ($consultation->getIdTypeAvis() != Atexo_Config::getParameter('TYPE_AVIS_ATTRIBUTION') && $consultation->getIdTypeAvis() != Atexo_Config::getParameter('TYPE_AVIS_INFORMATION')) {
                        $ligneTexteModalitesOuverture = $odf->setSegment('texteModaliteOuv');
                        if ($hasDateOuverture || $hasLieuOuverture) {
                            $ligneTexteModalitesOuverture->merge();
                        }
                        $odf->mergeSegment($ligneTexteModalitesOuverture);
                    } else {
                        $ligneTexteModalitesOuverture = $odf->setSegment('texteModaliteOuv');
                        $odf->mergeSegment($ligneTexteModalitesOuverture);
                    }
                } catch (\OdfException $e) {
                    // On ne fait rien si le segment n'existe pas
                }

                try {
                    $ligneNumDateJo = $odf->setSegment('texteJo');
                    if ($hasNumJo || $hasDateJo) {
                        $ligneNumDateJo->merge();
                    }
                    $odf->mergeSegment($ligneNumDateJo);
                } catch (\OdfException $e) {
                    // On ne fait rien si le segment n'existe pas
                }

                /*if ($oldLang) {
                   $globalization = $this->getApplication()->Modules["globalization"];
                   $globalization->setTranslationCatalogue("messages.".$oldLang);
                   Atexo_CurrentUser::writeToSession("lang", $oldLang);
                }*/
                if ($toFile) {
                    $pathFile = Atexo_Config::getParameter('COMMON_TMP').'Avis_'.$lang.'_'.session_id().time().'_fichier.odt';
                    $odf->saveToDisk($pathFile);
                    /*$pdfContent = PdfGeneratorClient::genererPdf($pathFile);
                    DownloadFile::downloadFileContent(Atexo_Config::getParameter('NOM_FICHIER_DETAIL_CONSULTATION').'.pdf', $pdfContent);
                        @unlink($odtFilePath);exit;*/
                    return $pathFile;
                } else {
                    $odf->exportAsAttachedFile('test.odt');
                    exit;
                }
            } else {
                return -1;
            }
        }
    }

    /**
     * Permet de corriger les caractères spéciaux pour la génération de l'avis rtf/pdf.
     *
     * @param string $chaine: chaine à corriger
     *
     * @return chaine corrigée
     */
    private function corrigerCaracteresSpeciaux($chaine)
    {
        $chaine = Atexo_Util::atexoHtmlEntitiesDecode($chaine);
        $carac = ['&amp;', '&lt;', '&gt;', '&apos;'];
        $valCarac = ['&', '<', '>', "'"];

        return Atexo_Util::atexoHtmlEntitiesDecode(str_replace($carac, $valCarac, $chaine));
    }
}
