<?php

namespace Application\Service\Atexo\Publicite;

use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonAgent;
use Application\Propel\Mpe\CommonAnnoncePressPeer;
use Application\Propel\Mpe\CommonAnnoncePressPieceJointe;
use Application\Propel\Mpe\CommonAnnoncePressPieceJointePeer;
use Application\Propel\Mpe\CommonTypeAvisPubProcedurePeer;
use Application\Propel\Mpe\CommonAVIS;
use Application\Propel\Mpe\CommonAvisPub;
use Application\Propel\Mpe\CommonAvisPubPeer;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonDestinataireAnnoncePress;
use Application\Propel\Mpe\CommonDestinataireAnnoncePressPeer;
use Application\Propel\Mpe\CommonDestinatairePub;
use Application\Propel\Mpe\CommonDestinatairePubPeer;
use Application\Propel\Mpe\CommonFormXmlDestinataireOpoce;
use Application\Propel\Mpe\CommonFormXmlDestinataireOpocePeer;
use Application\Propel\Mpe\CommonHistoriqueAvisPub;
use Application\Propel\Mpe\CommonHistoriqueAvisPubPeer;
use Application\Propel\Mpe\CommonTProfilJoue;
use Application\Propel\Mpe\CommonTProfilJoueQuery;
use Application\Propel\Mpe\CommonTypeAvisPub;
use Application\Propel\Mpe\CommonTypeAvisPubPeer;
use Application\Propel\Mpe\CommonTypeAvisPubQuery;
use Application\Propel\Mpe\CommonTypeSupportPeer;
use Application\Propel\Mpe\Om\BaseCommonAnnoncePressPieceJointePeer;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureType;
use Application\Service\Atexo\FormulaireSub\Atexo_FormulaireSub_AnnonceEchange;
use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2;
use AtexoPdf\PdfGeneratorClient;
use AtexoPdf\RtfGeneratorClient;
use PDO;
use Prado\Prado;

/*
 * Created on 12 sept. 2011
 *
 * by BOUKATAYA Fatima Zahra
 */
class Atexo_Publicite_AvisPub
{
    public function createAvisPub($organisme, $consultationId, $idAgent, $idTypeAvis)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $connexionCom->beginTransaction();
        $avisPub = new CommonAvisPub();
        $avisPub->setOrganisme($organisme);
        $avisPub->setConsultationId($consultationId);
        $avisPub->setStatut(Atexo_Config::getParameter('STATUT_AVIS_A_TRANSMETTRE'));
        $avisPub->setIdAgent($idAgent);
        $avisPub->setDateCreation(date('Y-m-d H:i:s'));
        $avisPub->setTypeAvis($idTypeAvis);
        $avisPub->save($connexionCom);
        //Creation de l'historique de l'avis
        self::createHistorique($organisme, $avisPub->getId(), date('Y-m-d H:i:s'), Atexo_Config::getParameter('STATUT_AVIS_A_TRANSMETTRE'), $connexionCom);

        $erreur = self::createDestinatairesPub($idTypeAvis, $avisPub->getId(), $organisme, $connexionCom);
        if ($erreur) {
            $connexionCom->rollBack();
        } else {
            $connexionCom->commit();
        }

        return $erreur;
    }

    public function createDestinatairesPub($idTypeAvis, $idAvis, $organisme, $connexion)
    {
        $erreur = false;
        $typesSupports = self::getTypesSupports($idTypeAvis);
        //$listeSupport [] =  Atexo_Config::getParameter('CODE_SUPPORT_SIMAP');
        foreach ($typesSupports as $one) {
            $consultation = (new Atexo_Consultation())->retrieveConsultation(Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id'])));
            if ($consultation && $one->getIdTypeSupport() == Atexo_Config::getParameter('TYPE_SUPPORT_PRESSE')
                            && ($consultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_ATTRIBUTION')
                            || $consultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_INFORMATION'))) {
                continue;
            }
            $typeSupport = new CommonDestinatairePub();
            $typeSupport->setOrganisme($organisme);
            $typeSupport->setIdAvis($idAvis);
            $typeSupport->setEtat(self::getEtatInitialDestinataire($one->getIdTypeSupport()));
            $typeSupport->setIdSupport($one->getIdTypeSupport());
            if ($one->getIdTypeSupport() == Atexo_Config::getParameter('TYPE_SUPPORT_OPOCE')) {
                $typeAvis = (new CommonTypeAvisPub())->getTypeAvisById($idTypeAvis);
                $listeSupport[] = Atexo_Config::getParameter('CODE_SUPPORT_OPOCE');
                if ($typeAvis instanceof CommonTypeAvisPub) {
                    $typeSupport->setTypePub($typeAvis->getTypePub());
                    if ($typeAvis->getTypePub() == Atexo_Config::getParameter('TYPE_PUB_AVIS_SUB')) {
                        $typeSupport->setIdDispositif($typeAvis->getIdDispositif());
                        $profilJoue = CommonTProfilJoueQuery::create()
                            ->filterByIdAgent(
                                Atexo_CurrentUser::getId()
                            )
                            ->filterByOrganisme(
                                $organisme
                            )
                            ->findOne($connexion);
                        $xml = Atexo_FormulaireSub_AnnonceEchange::getXmlInitialisationDossierSIMAP($consultation, $typeSupport, $profilJoue, $listeSupport);
                        $idDossierSub = Atexo_FormulaireSub_AnnonceEchange::creerDossier($xml);
                        $typeSupport->setIdDossier($idDossierSub);
                        if (!$idDossierSub) {
                            $erreur = true;
                        }
                    }
                }
            }
            $typeSupport->save($connexion);
        }

        return $erreur;
    }

    public function getEtatInitialDestinataire($idTypeSupport)
    {
        if ($idTypeSupport == Atexo_Config::getParameter('TYPE_SUPPORT_OPOCE')) {
            return Atexo_Config::getParameter('STATUT_DESTINATAIRE_AVIS_A_COMPLETER');
        } elseif ($idTypeSupport == Atexo_Config::getParameter('TYPE_SUPPORT_PRESSE')) {
            return Atexo_Config::getParameter('STATUT_DESTINATAIRE_AVIS_A_COMPLETER');
        } else {
            return Atexo_Config::getParameter('STATUT_DESTINATAIRE_AVIS_COMPLET');
        }
    }

    public function getAvisByRefConsultation($consultationId, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonAvisPubPeer::ORGANISME, $organisme);
        $c->add(CommonAvisPubPeer::CONSULTATION_ID, $consultationId);

        return CommonAvisPubPeer::doSelect($c, $connexion);
    }

    public function getAvisByRefConsultationEtTypeAvis($consultationId, $typeAvis, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonAvisPubPeer::ORGANISME, $organisme);
        $c->add(CommonAvisPubPeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonAvisPubPeer::TYPE_AVIS, $typeAvis);

        return CommonAvisPubPeer::doSelect($c, $connexion);
    }

    public function getTypesSupports($idTypeAvis)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonTypeSupportPeer::ID_TYPE_AVIS, $idTypeAvis);

        return CommonTypeSupportPeer::doSelect($c, $connexion);
    }

    public function getListeDestinataires($idAvis, $organisme, $idSupport = null)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonDestinatairePubPeer::ORGANISME, $organisme);
        $c->add(CommonDestinatairePubPeer::ID_AVIS, $idAvis);
        if ($idSupport) {
            $c->add(CommonDestinatairePubPeer::ID_SUPPORT, $idSupport);

            return CommonDestinatairePubPeer::doSelectOne($c, $connexion);
        }
        $tlisteDestinataire = CommonDestinatairePubPeer::doSelect($c, $connexion);

        return $tlisteDestinataire;
    }

    public function getListeDestinatairesHistorique($idAvis, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonHistoriqueAvisPubPeer::ORGANISME, $organisme);
        $c->add(CommonHistoriqueAvisPubPeer::ID_AVIS, $idAvis);
        $tlisteDestinataire = CommonHistoriqueAvisPubPeer::doSelect($c, $connexion);

        return $tlisteDestinataire;
    }

    public function getLibelleTypeDestinataire($typeDestinataire)
    {
        if ($typeDestinataire == Atexo_Config::getParameter('TYPE_SUPPORT_OPOCE')) {
            return Prado::localize('TEXT_SUPPORT_OPOCE');
        } elseif ($typeDestinataire == Atexo_Config::getParameter('TYPE_SUPPORT_PRESSE')) {
            return Prado::localize('TEXT_SUPPORT_PRESSE');
        } elseif ($typeDestinataire == Atexo_Config::getParameter('TYPE_SUPPORT_SIMAP2')) {
            return Prado::localize('TEXT_SUPPORT_SIMAP2');
        }
    }

    public function getLibelleEtatDestinataire(string $idEtat)
    {
        $libelle = match ($idEtat) {
            Atexo_Config::getParameter('STATUT_DESTINATAIRE_AVIS_A_COMPLETER') => Prado::localize('TEXT_STATUT_DESTINATAIRE_A_COMPLETER'),
            Atexo_Config::getParameter('STATUT_DESTINATAIRE_AVIS_BROUILLON') => Prado::localize('TEXT_BROUILLON'),
            Atexo_Config::getParameter('STATUT_DESTINATAIRE_AVIS_COMPLET') => Prado::localize('TEXT_STATUT_DESTINATAIRE_COMPLET'),
            Atexo_Config::getParameter('STATUT_DESTINATAIRE_AVIS_REJETE_ECO') => Prado::localize('TEXT_REJETE_ENTITE_COORDINATRICE'),
            Atexo_Config::getParameter('STATUT_DESTINATAIRE_AVIS_REJETE_SIP') => Prado::localize('TEXT_REJETE_SIP'),
            Atexo_Config::getParameter('STATUT_DESTINATAIRE_AVIS_OPOCE_EN_ATTENTE_INTEGRATION') => Prado::localize('ATTENTE_INTEGRATION'),
            Atexo_Config::getParameter('STATUT_DESTINATAIRE_AVIS_OPOCE_INTEGRE') => Prado::localize('DEFINE_INTEGRE'),
            Atexo_Config::getParameter('STATUT_DESTINATAIRE_AVIS_OPOCE_PUBLIE') => Prado::localize('DESTINATAIRE_PUBLIE'),
            Atexo_Config::getParameter('STATUT_DESTINATAIRE_AVIS_PRESSE_ENVOYE') => Prado::localize('TEXT_ENVOYE'),
            Atexo_Config::getParameter('STATUT_DESTINATAIRE_AVIS_PORTAIL_PUBLIE') => Prado::localize('DESTINATAIRE_PUBLIE'),
            Atexo_Config::getParameter('STATUT_DESTINATAIRE_AVIS_OPOCE_REJETE') => Prado::localize('DESTINATAIRE_REJETE'),
            default => Prado::localize('NON_RENSEIGNE'),
        };

        return $libelle;
    }

    public function activateSendButton($idAvis, $organisme)
    {
        $avis = self::retreiveAvis($idAvis, $organisme);
        //Afficher picto si statut= 0(à transmettre), 2(rejeté ECO), 3(rejeté SIP)
        if ($avis->getStatut() == Atexo_Config::getParameter('STATUT_AVIS_A_TRANSMETTRE')
           || $avis->getStatut() == Atexo_Config::getParameter('STATUT_AVIS_REJETE_ECO')
               || $avis->getStatut() == Atexo_Config::getParameter('STATUT_AVIS_REJETE_SIP')) {
            return true;
        }

        return false;
    }

    public function activateDeleteButton($idAvis, $organisme)
    {
        $avis = self::retreiveAvis($idAvis, $organisme);
        if ($avis->getStatut() >= Atexo_Config::getParameter('STATUT_AVIS_EN_ATTENTE_VALIDATION_ECO')) {
            return false;
        }

        return true;
    }

    public function getAvisById($idAvis, $organisme, $consultationId)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonAvisPubPeer::ORGANISME, $organisme);
        $c->add(CommonAvisPubPeer::ID, $idAvis);
        $c->add(CommonAvisPubPeer::CONSULTATION_ID, $consultationId);
        $avisPubs = CommonAvisPubPeer::doSelectOne($c, $connexion);

        return $avisPubs;
    }

    public function retreiveAvis($idAvis, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonAvisPubPeer::ORGANISME, $organisme);
        $c->add(CommonAvisPubPeer::ID, $idAvis);
        $avisPubs = CommonAvisPubPeer::doSelectOne($c, $connexion);

        return $avisPubs;
    }

    /*
     * Retourne la liste de tous les avis
     */
    public function retreiveAvisEnvoiPlanifie()
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonAvisPubPeer::STATUT, Atexo_Config::getParameter('STATUT_AVIS_ENVOI_PLANIFIE'));
        $avisPubs = CommonAvisPubPeer::doSelect($c, $connexion);

        return $avisPubs;
    }

    /**
     * Si le destinataire sera affiché en consultation.
     *
     * @return true: avis en consultation, false sinon
     *
     * @param $idAvis: identifiant de l'avis
     * @param $organisme: l'organisme de l'avis
     * @param $consultationId: la référence de la consultation
     */
    public function isDetailSupport($idAvis, $organisme, $consultationId)
    {
        $avis = self::getAvisById($idAvis, $organisme, $consultationId);
        if ($avis instanceof CommonAvisPub) {
            if ((new self())->isAccesGestionPubMpe()) {
                if ($avis->getStatut() == Atexo_Config::getParameter('STATUT_AVIS_A_TRANSMETTRE') || $avis->getStatut() == Atexo_Config::getParameter('STATUT_AVIS_REJETE_ECO')
                   || $avis->getStatut() == Atexo_Config::getParameter('STATUT_AVIS_REJETE_SIP')) {
                    return false;
                } else {
                    return true;
                }
            } else {
                if ((new self())->isAccesGestionPubSip()) {
                    if ($avis->getStatut() == Atexo_Config::getParameter('STATUT_AVIS_A_TRANSMETTRE') || $avis->getStatut() == Atexo_Config::getParameter('STATUT_AVIS_EN_ATTENTE_VALIDATION_SIP')
                       || $avis->getStatut() == Atexo_Config::getParameter('STATUT_AVIS_REJETE_SIP')) {
                        return false;
                    } elseif ($avis->getStatut() == Atexo_Config::getParameter('STATUT_AVIS_ENVOI_PLANIFIE') || $avis->getStatut() == Atexo_Config::getParameter('STATUT_AVIS_ENVOYE')) {
                        return true;
                    }

                    return true;
                } else {
                    if ($avis->getStatut() == Atexo_Config::getParameter('STATUT_AVIS_A_TRANSMETTRE') || $avis->getStatut() == Atexo_Config::getParameter('STATUT_AVIS_EN_ATTENTE_VALIDATION_ECO')
                       || $avis->getStatut() == Atexo_Config::getParameter('STATUT_AVIS_REJETE_ECO') || $avis->getStatut() == Atexo_Config::getParameter('STATUT_AVIS_REJETE_SIP')) {
                        return false;
                    } elseif ($avis->getStatut() == Atexo_Config::getParameter('STATUT_AVIS_ENVOI_PLANIFIE') || $avis->getStatut() == Atexo_Config::getParameter('STATUT_AVIS_ENVOYE')) {
                        return true;
                    }

                    return true;
                }
            }
        }
    }

    /**
     * Supprime un avis par on Id.
     */
    public function deleteAvis($idAvis, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        self::deleteHistoriqueAvis($idAvis, $organisme);
        self::deleteDestinataires($idAvis, $organisme);
        $criteria = new Criteria();
        $criteria->add(CommonAvisPubPeer::ID, $idAvis);
        $criteria->add(CommonAvisPubPeer::ORGANISME, $organisme);
        CommonAvisPubPeer::doDelete($criteria, $connexion);
    }

    /**
     * Supprime l historique d un avis par on Id.
     */
    public function deleteHistoriqueAvis($idAvis, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        self::deleteHistoriqueDestinataires($idAvis, $organisme);
        $criteria = new Criteria();
        $criteria->add(CommonHistoriqueAvisPubPeer::ID, $idAvis);
        $criteria->add(CommonHistoriqueAvisPubPeer::ORGANISME, $organisme);
        CommonHistoriqueAvisPubPeer::doDelete($criteria, $connexion);
    }

    /**
     * Supprime la liste des destinataires pour un avis.
     */
    public function deleteDestinataires($idAvis, $organisme)
    {
        $dest = self::getListeDestinataires($idAvis, $organisme);
        if (is_array($dest) && count($dest)) {
            foreach ($dest as $oneDest) {
                self::deleteOneDestinataire($oneDest->getId(), $organisme);
            }
        }
    }

    /**
     * Supprime l historique de la liste des destinataires pour un avis.
     */
    public function deleteHistoriqueDestinataires($idAvis, $organisme)
    {
        $dest = self::getListeDestinatairesHistorique($idAvis, $organisme);
        if (is_array($dest) && count($dest)) {
            foreach ($dest as $oneDest) {
                self::deleteOneHistoriqueForDestinataire($oneDest->getId());
            }
        }
    }

    /**
     * supprime un destinataire.
     */
    public function deleteOneDestinataire($id, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $formXml = self::getXmlDestOpoce($id, $organisme);
        if ($formXml) {
            self::deleteFormXmlDestOpoce($formXml->getId(), $organisme);
        }
        $annoncePress = self::getAnnoncePress($id, $organisme);
        if ($annoncePress) {
            self::deleteAnnoncePress($annoncePress->getIdDestPress(), $organisme);
            self::deletePJAnnoncePress($annoncePress->getId(), $organisme);
            self::deleteDestAnnoncePress($annoncePress->getId(), $organisme);
        }
        $criteria = new Criteria();
        $criteria->add(CommonDestinatairePubPeer::ID, $id);
        $criteria->add(CommonDestinatairePubPeer::ORGANISME, $organisme);
        CommonDestinatairePubPeer::doDelete($criteria, $connexion);
    }

    /**
     * supprime l historique d un destinataire.
     */
    public function deleteOneHistoriqueForDestinataire($id)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $criteria = new Criteria();
        $criteria->add(CommonHistoriqueAvisPubPeer::ID, $id);
        $criteria->add(CommonHistoriqueAvisPubPeer::ORGANISME, Atexo_CurrentUser::getCurrentOrganism());
        CommonHistoriqueAvisPubPeer::doDelete($criteria, $connexion);
    }

    public function deleteFormXmlDestOpoce($idDestOpoce, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $criteria = new Criteria();
        $criteria->add(CommonFormXmlDestinataireOpocePeer::ORGANISME, $organisme);
        $criteria->add(CommonFormXmlDestinataireOpocePeer::ID_DESTINATAIRE_OPOCE, $idDestOpoce);
        CommonFormXmlDestinataireOpocePeer::doDelete($criteria, $connexion);
    }

    public function deleteAnnoncePress($idDest, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $criteria = new Criteria();
        $criteria->add(CommonAnnoncePressPeer::ORGANISME, $organisme);
        $criteria->add(CommonAnnoncePressPeer::ID_DEST_PRESS, $idDest);
        CommonAnnoncePressPeer::doDelete($criteria, $connexion);
    }

    public function deletePJAnnoncePress($id, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $criteria = new Criteria();
        $criteria->add(CommonAnnoncePressPieceJointePeer::ORGANISME, $organisme);
        $criteria->add(CommonAnnoncePressPieceJointePeer::ID_ANNONCE_PRESS, $id);
        CommonAnnoncePressPieceJointePeer::doDelete($criteria, $connexion);
    }

    public function deleteDestAnnoncePress($id, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $criteria = new Criteria();
        $criteria->add(CommonDestinataireAnnoncePressPeer::ORGANISME, $organisme);
        $criteria->add(CommonDestinataireAnnoncePressPeer::ID_ANNONCE_PRESS, $id);
        CommonDestinataireAnnoncePressPeer::doDelete($criteria, $connexion);
    }

    public function sendAvis($idAvis, $organisme, $consultationId)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $avis = self::getAvisById($idAvis, $organisme, $consultationId);
        if ($avis instanceof CommonAvisPub) {
            $avis->setDateEnvoi(date('Y-m-d H:i:s'));
            $avis->setStatut(Atexo_Config::getParameter('STATUT_AVIS_EN_ATTENTE_VALIDATION_ECO'));
            $avis->setIdAgentValidateur(Atexo_CurrentUser::getIdAgentConnected());
            $avis->save($connexion);
            self::createHistorique($organisme, $avis->getId(), $avis->getDateEnvoi(), $avis->getStatut(), $connexion);
            //Début génération et stockage des avis JAL et portail
            $consultation = (new Atexo_Consultation())->retrieveConsultationByRefConsultation($consultationId);
            if ($consultation instanceof CommonConsultation) {
                $modele = self::getModeleDocumentPresse($avis->getTypeAvis());
                if ($modele) {
                    //Stockage avis JAL
                    $pieceJointe = self::CommonAnnoncePressPieceJointeByIdAndOrganisme($avis->getIdAvisPresse(), $avis->getOrganisme(), $connexion);
                    if ($pieceJointe instanceof CommonAnnoncePressPieceJointe) {
                        $tableauContenuGenerationDocPresse = self::genererAvisPressePortail($modele, $connexion, $avis->getId(), $consultation);
                        $pieceJointe->setTaille(filesize($tableauContenuGenerationDocPresse[1]));
                        $pieceJointe->setPiece($tableauContenuGenerationDocPresse[0]);
                        $pieceJointe->save($connexion);
                    }
                    //Stockage avis portail
                    $tableauContenuGenerationPortail = self::genererAvisPressePortail($modele, $connexion, $avis->getId(), $consultation, 'fr', true);
                    $atexoBlob = new Atexo_Blob();
                    $blobContent = $atexoBlob->return_blob_to_client($tableauContenuGenerationPortail[0], $avis->getOrganisme());
                    $tmpFile = Atexo_Config::getParameter('COMMON_TMP').'/'.session_name().session_id().time().$tableauContenuGenerationPortail[0].'.pdf';
                    $fp = fopen($tmpFile, 'w');
                    fwrite($fp, $blobContent);
                    fclose($fp);

                    $avisPortail = (new Atexo_Publicite_AvisPdf())->AddAvisToConsultation($tmpFile, $consultation, 'fr', true);
                    if ($avisPortail instanceof CommonAVIS) {
                        $avisPortail->setAvis($tableauContenuGenerationPortail[0]);
                        $avisPortail->setDatePub(date('Y-m-d'));
                        $avisPortail->save($connexion);
                        //Mise à jour de l'avis global
                        $avis->setIdAvisPortail($avisPortail->getId());
                        $avis->save($connexion);
                    }
                }
            }
            //Fin génération et stockage des avis JAL et portail
        }
    }

    /**
     * Permet de créer une ligne dans la table historique.
     *
     * @param $typeDestinataire: type de support (opoce, presse ou plateforme). La valeur par defaut '0' correspond à l'avis global
     */
    public function createHistorique($organisme, $idAvis, $dateModif, $statut, $connexion, $motifRejet = null, $typeDestinataire = '0', $callFromCron = false)
    {
        $historiqueAvis = new CommonHistoriqueAvisPub();
        $historiqueAvis->setOrganisme($organisme);
        $historiqueAvis->setIdAvis($idAvis);
        $historiqueAvis->setDateModification($dateModif);
        $historiqueAvis->setDetailStatut($statut);
        if (($statut == Atexo_Config::getParameter('STATUT_AVIS_REJETE_ECO')
           || $statut == Atexo_Config::getParameter('STATUT_AVIS_REJETE_SIP'))
               && null != $motifRejet && '' != $motifRejet) {
            $historiqueAvis->setMotifRejet($motifRejet);
        }
        if (!$callFromCron) {
            $agentAction = (new Atexo_Agent())->retrieveAgent(Atexo_CurrentUser::getIdAgentConnected());
            if ($agentAction instanceof CommonAgent) {
                $historiqueAvis->setPrenomAgent($agentAction->getPrenom());
                $historiqueAvis->setNomAgent($agentAction->getNom());
            }
        }
        $historiqueAvis->setTypeHistorique($typeDestinataire);
        $historiqueAvis->save($connexion);
    }

    public function getXmlDestOpoce($idDest, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonFormXmlDestinataireOpocePeer::ORGANISME, $organisme);
        $c->add(CommonFormXmlDestinataireOpocePeer::ID_DESTINATAIRE_OPOCE, $idDest);
        $xmlOpoce = CommonFormXmlDestinataireOpocePeer::doSelectOne($c, $connexion);

        return $xmlOpoce;
    }

    public function getAnnoncePress($idDest, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonAnnoncePressPeer::ORGANISME, $organisme);
        $c->add(CommonAnnoncePressPeer::ID_DEST_PRESS, $idDest);
        $annoncePress = CommonAnnoncePressPeer::doSelectOne($c, $connexion);

        return $annoncePress;
    }

    public function createNewXmlDestOpoce($idDest, $organisme, $xml, $noDocExt)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $formXml = new CommonFormXmlDestinataireOpoce();
        $formXml->setOrganisme($organisme);
        $formXml->setIdDestinataireOpoce($idDest);
        $formXml->setXml($xml);
        $formXml->setNoDocExt($noDocExt);
        $formXml->save($connexionCom);

        return $formXml;
    }

    public function getDestinatairePub($id, $organisme = null)
    {
        if (!$organisme) {
            $organisme = Atexo_CurrentUser::getCurrentOrganism();
        }
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $criteria = new Criteria();
        $criteria->add(CommonDestinatairePubPeer::ID, $id);
        $criteria->add(CommonDestinatairePubPeer::ORGANISME, $organisme);
        $dstPub = CommonDestinatairePubPeer::doSelectOne($criteria, $connexion);

        return $dstPub ?: false;
    }

    public function updateEtatDestPub($idDest, $newEtat)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $dest = self::getDestinatairePub($idDest);
        if ($dest instanceof CommonDestinatairePub) {
            $dest->setDateModification(date('Y-m-d H:i:s'));
            $dest->setEtat($newEtat);
            $dest->save($connexionCom);
            //Création de l'historique
            self::createHistorique($dest->getOrganisme(), $dest->getIdAvis(), date('Y-m-d H:i:s'), $newEtat, $connexionCom, null, Atexo_Config::getParameter('TYPE_SUPPORT_PRESSE'));
            //On met à jour le statut du destinataire SIMAP2
            $destinatairePortail = self::getDestinataireByIdAvisAndTypeSupport($dest->getIdAvis(), Atexo_Config::getParameter('TYPE_SUPPORT_SIMAP2'));
            if ($destinatairePortail) {
                if ($destinatairePortail->getEtat() == Atexo_Config::getParameter('STATUT_DESTINATAIRE_AVIS_REJETE_ECO')
                       || Atexo_Config::getParameter('STATUT_DESTINATAIRE_AVIS_REJETE_SIP')) {
                    $destinatairePortail->setEtat(Atexo_Config::getParameter('STATUT_DESTINATAIRE_AVIS_COMPLET'));
                    $destinatairePortail->save($connexionCom);
                }
            }
        }
    }

    public function updateDestAvis($idDest, $newEtat, $connexionCom = null)
    {
        if (!$connexionCom) {
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        }
        $dest = self::getDestinatairePub($idDest);
        if ($dest instanceof CommonDestinatairePub) {
            $dest->setDateModification(date('Y-m-d H:i:s'));
            $dest->setEtat($newEtat);
            $dest->save($connexionCom);
            //Création de l'historique
            self::createHistorique($dest->getOrganisme(), $dest->getIdAvis(), date('Y-m-d H:i:s'), $newEtat, $connexionCom, null, Atexo_Config::getParameter('TYPE_SUPPORT_OPOCE'));
            //On met à jour le statut du destinataire SIMAP2
            $destinatairePortail = self::getDestinataireByIdAvisAndTypeSupport($dest->getIdAvis(), Atexo_Config::getParameter('TYPE_SUPPORT_SIMAP2'));
            if ($destinatairePortail) {
                if ($destinatairePortail->getEtat() == Atexo_Config::getParameter('STATUT_DESTINATAIRE_AVIS_REJETE_ECO')
                       || Atexo_Config::getParameter('STATUT_DESTINATAIRE_AVIS_REJETE_SIP')) {
                    $destinatairePortail->setEtat(Atexo_Config::getParameter('STATUT_DESTINATAIRE_AVIS_COMPLET'));
                    $destinatairePortail->save($connexionCom);
                }
            }
        }
    }

    public function construireXmlInitialisation($consultationId, $idDestinataire)
    {
        $typeContract = null;
        $cons = (new Atexo_Consultation())->retrieveConsultationByRefConsultation($consultationId);
        $lots = $cons->getAllLots();
        $noDocExt = date('Y').'-'.substr(str_pad($idDestinataire, 6, '0', STR_PAD_LEFT), -6, 6);

        $serviceId = Atexo_CurrentUser::getCurrentServiceId();
        if (empty($serviceId)) {
            $entite = Atexo_Organismes::retrieveOrganismeByAcronyme(Atexo_CurrentUser::getOrganismAcronym());
            $mail = $entite->getEmail();
            $organistation = $entite->getDenominationOrgTraduit();
            $adresse = $entite->getAdresseTraduit();
            $ville = $entite->getVilleTraduit();
            $tel = $entite->getTel();
            $fax = '';
        } else {
            $entite = Atexo_EntityPurchase::retrieveEntityById($serviceId, Atexo_CurrentUser::getOrganismAcronym());
            $mail = $entite->getMail();
            $organistation = $entite->getLibelleTraduit();
            $adresse = $entite->getAdresseTraduit();
            $ville = $entite->getVilleTraduit();
            $tel = $entite->getTelephone();
            $fax = $entite->getFax();
        }
        //Informations complémentaires
        $infosComplementaires = (new Atexo_Publicite_AvisPdf())->getInfoFromReferentielConsultation(Atexo_Config::getParameter('AUTRE_INFOS'), $cons->getId(), 0, Atexo_CurrentUser::getOrganismAcronym());
        //Modalités d'ouverture des offres
        $heureOuverture = date_format(date_create($cons->getDatefin()), 'H:i');
        $dateOuverture = date_format(date_create($cons->getDatefin()), 'd/m/Y');
        $arrayDateOuverture = explode('/', $dateOuverture);
        $lieuOuverture = $cons->getLieuOuverturePlisTraduit();
        //Variante
        $variante = (Atexo_Util::replaceSpecialCharacters(Atexo_Util::toUtf8($cons->getVarianteCalcule()))) ? 'YES' : 'NO';
        //Code Nut
        $listeCodeNut = explode('#', substr($cons->getCodesNuts(), 1));
        $codeNutPrincipal = $listeCodeNut[0];
        //Lieux d'execution
        $arrayLieuExecution = explode(',', $cons->getLieuExecution());
        $lieuExecution = self::retrieveGeolocalisationN2($arrayLieuExecution[1]);
        //Recupération du profilJOUE
        $profilJoue = null;
        $profilJoueQuery = new CommonTProfilJoueQuery(Atexo_Config::getParameter('COMMON_DB'));
        $collectionProfilJoue = $profilJoueQuery->recupererProfilJoueByIdAgentAndOrganisme(Atexo_CurrentUser::getIdAgentConnected(), Atexo_CurrentUser::getOrganismAcronym());
        if ($collectionProfilJoue instanceof PropelObjectCollection && $collectionProfilJoue->getData()) {
            $profilsJoue = $collectionProfilJoue->getData();
            if (is_array($profilsJoue) && count($profilsJoue)) {
                $profilJoue = $profilsJoue[0];
            }
        }

        $xml = '<?xml version="1.0" encoding="UTF-8"?>';
        $xml .= '<INITIALISATION CATEGORY="ORIGINAL" FORM="'.self::getIdFormFromResource(Atexo_Util::atexoHtmlEntities($_GET['resourceFormulaire'])).'" LG="'.
               strtoupper(Atexo_CurrentUser::readFromSession('lang')).'" VERSION="R2.0.7.S03" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="initialisation.xsd">';
        $xml .= '<SENDER><LOGIN CLASS="'.Atexo_Config::getParameter('CLASS_ESENDER').'"><ESENDER_LOGIN>'.Atexo_Config::getParameter('LOGIN_ESENDER').'</ESENDER_LOGIN></LOGIN><USER>
				<USER_E_MAIL TYPE="CONTENT">'.Atexo_Util::replaceSpecialCharacters(Atexo_Util::toUtf8((new Atexo_Agent())->getMailAgentById(Atexo_CurrentUser::getIdAgentConnected()))).'</USER_E_MAIL>
				<USER_E_MAIL TYPE="TECHNICAL">'.Atexo_Util::replaceSpecialCharacters(Atexo_Util::toUtf8(Atexo_Config::getParameter('MAIL_TECHNIQUE_ESENDER'))).'</USER_E_MAIL></USER><NO_DOC_EXT>'.$noDocExt.'</NO_DOC_EXT></SENDER>';
        $xml .= '<NAME_ADDRESSES_CONTACT><CA_CE_CONCESSIONAIRE_PROFILE>
			    <ORGANISATION>'.Atexo_Util::replaceSpecialCharacters(Atexo_Util::toUtf8(self::getOrganisationNameAdressesContact($profilJoue, $organistation))).'</ORGANISATION><ADDRESS>'.
               Atexo_Util::replaceSpecialCharacters(Atexo_Util::toUtf8(self::getAdreseNameAdressesContact($profilJoue, $adresse))).'</ADDRESS>
			    <TOWN>'.Atexo_Util::replaceSpecialCharacters(Atexo_Util::toUtf8(self::getVilleNameAdressesContact($profilJoue, $ville))).'</TOWN><POSTAL_CODE>'.
               Atexo_Util::replaceSpecialCharacters(Atexo_Util::toUtf8(self::getCodePostalNameAdressesContact($profilJoue, $entite->getCp()))).'</POSTAL_CODE>
			    <COUNTRY VALUE="'.Atexo_Util::replaceSpecialCharacters(Atexo_Util::toUtf8(self::getCodePaysNameAdressesContact($profilJoue, Atexo_Config::getParameter('DENOMINATION2_GEON2_FRANCE')))).'"/><CONTACT_POINT>'.
               Atexo_Util::replaceSpecialCharacters(Atexo_Util::toUtf8(self::getPointContactNameAdressesContact($profilJoue))).'</CONTACT_POINT>
			    <PHONE>'.Atexo_Util::replaceSpecialCharacters(Atexo_Util::toUtf8(self::getTelephoneNameAdressesContact($profilJoue, $tel))).'</PHONE><E_MAIL>'.
               Atexo_Util::replaceSpecialCharacters(Atexo_Util::toUtf8(self::getEmailNameAdressesContact($profilJoue, $mail))).'</E_MAIL><FAX>'.
               Atexo_Util::replaceSpecialCharacters(Atexo_Util::toUtf8(self::getFaxNameAdressesContact($profilJoue, $fax))).'</FAX><ATTENTION>'.
               Atexo_Util::replaceSpecialCharacters(Atexo_Util::toUtf8(self::getAtentionNameAdressesContact($profilJoue))).'</ATTENTION></CA_CE_CONCESSIONAIRE_PROFILE>'.
               self::getAdressesInternetsNameAdressesContact($profilJoue).self::getAdressesComplementairesNameAdressesContact($profilJoue).'</NAME_ADDRESSES_CONTACT>';
        $xml .= self::getTypeOrganismeAcheteur($profilJoue).
               '<AUTHORITY_ENTITY>
                  <TYPE_AND_ACTIVITIES>
                    '.self::getTypePouvoirAdjudicateur($profilJoue).'
                    '.self::getActivitesPrincipales($profilJoue).'
                  </TYPE_AND_ACTIVITIES>
                  '.self::getPouvoirAdjudicateurAgit($profilJoue).'
                </AUTHORITY_ENTITY>';
        $objet = '<P>'.str_replace(["\r\n", "\n"], '</P><P>', Atexo_Util::replaceSpecialCharacters(Atexo_Util::toUtf8($cons->getObjetTraduit()))).'</P>';

        $xml .= '<REFERENCE>'.Atexo_Util::replaceSpecialCharacters(Atexo_Util::toUtf8($cons->getReferenceUtilisateur())).'</REFERENCE><INTITULE>'.'<P>'.str_replace(
            ["\r\n", "\n"],
            '</P><P>',
            Atexo_Util::replaceSpecialCharacters(Atexo_Util::toUtf8($cons->getIntituleTraduit()))
        ).'</P>'.'</INTITULE><DESCRIPTION_MARCHE>'.$objet.'</DESCRIPTION_MARCHE>';
        //lots
        foreach ($lots as $un) {
            $infosComplementairesLot = (new Atexo_Publicite_AvisPdf())->getInfoFromReferentielConsultation(Atexo_Config::getParameter('INFOS_CMPLT_LOT'), $cons->getId(), $un->getLot(), Atexo_CurrentUser::getOrganismAcronym());
            $xml .= '<LOT_INFORMATION><LOT_NUMBER>'.Atexo_Util::replaceSpecialCharacters(Atexo_Util::toUtf8($un->getLot())).'</LOT_NUMBER>';
            $xml .= '<LOT_TITLE>'.Atexo_Util::replaceSpecialCharacters(Atexo_Util::toUtf8($un->getDescription())).'</LOT_TITLE><LOT_DESCRIPTION>'.'<P>'.str_replace(
                ["\r\n", "\n"],
                '</P><P>',
                Atexo_Util::replaceSpecialCharacters(Atexo_Util::toUtf8($un->getDescriptionDetail()))
            ).'</P>'.'</LOT_DESCRIPTION>';
            $xml .= '<CPV><CPV_MAIN><CPV_CODE CODE="'.Atexo_Util::replaceSpecialCharacters(Atexo_Util::toUtf8($un->getCodeCpv1())).'"/></CPV_MAIN></CPV><ADDITIONAL_INFORMATION_ABOUT_LOTS>'.'<P>'.
                       str_replace(["\r\n", "\n"], '</P><P>', Atexo_Util::replaceSpecialCharacters(Atexo_Util::toUtf8($infosComplementairesLot))).'</P>'.'</ADDITIONAL_INFORMATION_ABOUT_LOTS></LOT_INFORMATION>';
        }
        $typeProcedue = Atexo_Consultation_ProcedureType::retrieveProcedureById($cons->getIdTypeProcedure(), Atexo_CurrentUser::getOrganismAcronym());
        $xml .= '<PROCEDURE>'.Atexo_Util::replaceSpecialCharacters(Atexo_Util::toUtf8($typeProcedue->getEquivalentOpoce())).'</PROCEDURE>';
        //Variantes
        $xml .= '<VARIANTE VALUE="'.$variante.'"/>';
        //Informations complémentaires
        $conditionParticipation = (new Atexo_Publicite_AvisPdf())->getInfoFromReferentielConsultation(Atexo_Config::getParameter('CONDITION_PARTICIPATION'), $cons->getId(), 0, Atexo_CurrentUser::getOrganismAcronym());
        $modaliteRetraitDossier = $cons->getAdresseRetraisDossiersTraduit();
        $modaliteVisiteLieuxReunionInformation = (new Atexo_Publicite_AvisPdf())->getInfoFromReferentielConsultation(Atexo_Config::getParameter('MODALITES_VISITES_LIEUX_REUNIONS'), $cons->getId(), 0, Atexo_CurrentUser::getOrganismAcronym());
        $modaliteReceptionOffre = $cons->getAdresseDepotOffresTraduit();

        $autresInformations = '<P>';
        $autresInformations .= str_replace(["\r\n", "\n"], '</P><P>', Atexo_Util::replaceSpecialCharacters(Atexo_Util::toUtf8($infosComplementaires)));
        if ($conditionParticipation) {
            $autresInformations .= '</P>'.'<P></P>'.'<P> '.Atexo_Util::replaceSpecialCharacters(Atexo_Util::toUtf8(Prado::localize('CONDITION_DE_PARTICIPATION'))).' : ';
            $autresInformations .= '</P>'.'<P>'.str_replace(["\r\n", "\n"], '</P><P>', Atexo_Util::replaceSpecialCharacters(Atexo_Util::toUtf8($conditionParticipation)));
        }
        if ($modaliteRetraitDossier) {
            $autresInformations .= '</P>'.'<P>'.'</P>'.'<P>'.Atexo_Util::replaceSpecialCharacters(Atexo_Util::toUtf8(Prado::localize('RETRAIT_DOSSIERS'))).' :';
            $autresInformations .= '</P>'.'<P>'.str_replace(["\r\n", "\n"], '</P><P>', Atexo_Util::replaceSpecialCharacters(Atexo_Util::toUtf8($modaliteRetraitDossier)));
        }
        if ($modaliteVisiteLieuxReunionInformation) {
            $autresInformations .= '</P>'.'<P>'.'</P>'.'<P> '.Atexo_Util::replaceSpecialCharacters(Atexo_Util::toUtf8(Prado::localize('MODALITES_VISITE_LIEUX_REUNION_INFORMATION'))).' :';
            $autresInformations .= '</P>'.'<P>'.str_replace(["\r\n", "\n"], '</P><P>', Atexo_Util::replaceSpecialCharacters(Atexo_Util::toUtf8($modaliteVisiteLieuxReunionInformation)));
        }
        if ($modaliteReceptionOffre) {
            $autresInformations .= '</P>'.'<P>'.'</P>'.'<P> '.Atexo_Util::replaceSpecialCharacters(Atexo_Util::toUtf8(Prado::localize('ADRESSE_DEPOT_OFFRE'))).' :';
            $autresInformations .= '</P>'.'<P>'.str_replace(["\r\n", "\n"], '</P><P>', Atexo_Util::replaceSpecialCharacters(Atexo_Util::toUtf8($modaliteReceptionOffre)));
        }
        $autresInformations .= '</P>';
        $xml .= '<ADDITIONAL_INFORMATION>'.$autresInformations.'</ADDITIONAL_INFORMATION>';
        //Modalités d'ouverture des offres
        $xml .= '<CONDITIONS_FOR_OPENING_TENDERS>';
        $xml .= '<CONDITIONS_FOR_OPENING_TENDERS>';
        $xml .= '<DATE_TIME><DAY>'.$arrayDateOuverture[0].'</DAY><MONTH>'.$arrayDateOuverture[1].'</MONTH><YEAR>'.$arrayDateOuverture[2].'</YEAR><TIME>'.$heureOuverture.'</TIME></DATE_TIME>';
        $xml .= '<PLACE_OPENING><PLACE_NOT_STRUCTURED>'.Atexo_Util::replaceSpecialCharacters(Atexo_Util::toUtf8($lieuOuverture)).'</PLACE_NOT_STRUCTURED></PLACE_OPENING>';
        $xml .= '</CONDITIONS_FOR_OPENING_TENDERS>';
        $xml .= '</CONDITIONS_FOR_OPENING_TENDERS>';
        //Recupération des informations JOUE
        $tableauInfosJoue = self::recupererInfosJoue($cons, $idDestinataire);
        $arrayDateJoue = explode('-', $tableauInfosJoue['date_pub_joue']);
        if (is_array($tableauInfosJoue)) {
            $xml .= '<PREVIOUS_PUBLICATION_OJ>';
            $xml .= '<NOTICE_NUMBER_OJ>'.Atexo_Util::replaceSpecialCharacters(Atexo_Util::toUtf8($tableauInfosJoue['id_joue'])).'</NOTICE_NUMBER_OJ>';
            $xml .= '<DATE_OJ>';
            $xml .= '<DAY>'.Atexo_Util::replaceSpecialCharacters(Atexo_Util::toUtf8($arrayDateJoue[2])).'</DAY>';
            $xml .= '<MONTH>'.Atexo_Util::replaceSpecialCharacters(Atexo_Util::toUtf8($arrayDateJoue[1])).'</MONTH>';
            $xml .= '<YEAR>'.Atexo_Util::replaceSpecialCharacters(Atexo_Util::toUtf8($arrayDateJoue[0])).'</YEAR>';
            $xml .= '</DATE_OJ>';
            $xml .= '</PREVIOUS_PUBLICATION_OJ>';
        }
        $xml .= '<DESCRIPTION><CPV><CPV_MAIN><CPV_CODE CODE="'.Atexo_Util::replaceSpecialCharacters(Atexo_Util::toUtf8($cons->getCodeCpv1())).'"/></CPV_MAIN>';

        if (null != $cons->getCodeCpv2()) {
            $adCpv = explode('#', $cons->getCodeCpv2());
            foreach ($adCpv as $ad) {
                if (null != $ad) {
                    $xml .= '<CPV_ADDITIONAL>';
                    $xml .= '<CPV_CODE CODE="'.Atexo_Util::replaceSpecialCharacters(Atexo_Util::toUtf8($ad)).'"/>';
                    $xml .= '</CPV_ADDITIONAL>';
                }
            }
        }

        $xml .= '</CPV>';
        if ('1' == $cons->getCategorie()) {
            $typeContract = Atexo_Config::getParameter('TYPE_CONTRACT_WORKS');
        } elseif ('2' == $cons->getCategorie()) {
            $typeContract = Atexo_Config::getParameter('TYPE_CONTRACT_SUPPLIES');
        } elseif ('3' == $cons->getCategorie()) {
            $typeContract = Atexo_Config::getParameter('TYPE_CONTRACT_SERVICES');
        }
        $xml .= '<TYPE_CONTRACT_LOCATION><TYPE_CONTRACT VALUE="'.$typeContract.'"/></TYPE_CONTRACT_LOCATION>';
        //Lieu d'execution
        $xml .= '<LOCATION_NUTS><LOCATION>'.$lieuExecution.'</LOCATION><NUTS CODE="'.$codeNutPrincipal.'"/></LOCATION_NUTS>';
        $xml .= '</DESCRIPTION>';

        $xml .= '</INITIALISATION>'; //echo $xml;exit;
        $xml = base64_encode($xml);

        return $xml;
    }

    public function getIdFormFromResource($resourceForm)
    {
        $tab = explode('_', $resourceForm);

        return intval($tab[1]);
    }

    public function getListeAvis($idTypeProcedure, $organisme)
    {
        $tabTypeAvis = self::getTypeAvis($idTypeProcedure, $organisme);
        $listAvis = [];
        foreach ($tabTypeAvis as $one) {
            $listAvis[$one->getIdTypeAvis()] = (new CommonTypeAvisPub())->getLibelleAvisById($one->getIdTypeAvis());
        }

        return $listAvis;
    }

    public function getAvisJoinTypesAvisByRegion($region)
    {
        $sql = ' SELECT Type_Avis_Pub.id as idTypeAvis, count(Type_Avis_Pub.id) as nbrTypeAvis, Avis_Pub.statut, Type_Avis_Pub.libelle, consultation.id_etat_consultation ';
        $sql .= ' FROM Type_Avis_Pub, Avis_Pub, consultation ';
        $sql .= ' WHERE Type_Avis_Pub.id = Avis_Pub.type_avis AND Avis_Pub.consultation_id = consultation.id ';
        $sql .= ' AND consultation.id_etat_consultation <> '.(new Atexo_Db())->quote(Atexo_Config::getParameter('STATUS_ARCHIVE_REALISEE'));

        $sql .= " AND Type_Avis_Pub.region = '".$region."'";

        if ((new self())->isAccesGestionPubSip()) {
            $sql .= " AND Avis_Pub.statut in
			(
			'".Atexo_Config::getParameter('STATUT_AVIS_EN_ATTENTE_VALIDATION_SIP')."',
			'".Atexo_Config::getParameter('STATUT_AVIS_ENVOI_PLANIFIE')."',
			'".Atexo_Config::getParameter('STATUT_AVIS_ENVOYE')."',
			'".Atexo_Config::getParameter('STATUT_AVIS_REJETE_SIP')."'
			)";
            $sql .= " AND Avis_Pub.Sip = '1' ";
        }
        if (!(new self())->isAccesGestionPubSip()) {
            $sql .= " AND Avis_Pub.statut in
			(
			'".Atexo_Config::getParameter('STATUT_AVIS_EN_ATTENTE_VALIDATION_ECO')."',
			'".Atexo_Config::getParameter('STATUT_AVIS_REJETE_ECO')."',
			'".Atexo_Config::getParameter('STATUT_AVIS_EN_ATTENTE_VALIDATION_SIP')."',
			'".Atexo_Config::getParameter('STATUT_AVIS_ENVOI_PLANIFIE')."',
			'".Atexo_Config::getParameter('STATUT_AVIS_ENVOYE')."',
			'".Atexo_Config::getParameter('STATUT_AVIS_REJETE_SIP')."'
			)";
        }

        $sql .= ' GROUP BY libelle, statut ';
        //echo $sql."<br/><br/>";
        $statement = Atexo_Db::getLinkCommon(true)->query($sql);
        $arrayReturn = [];

        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            $arrayReturn[$row['idTypeAvis']]['libelle_avis'] = $row['libelle'];

            if (!(new self())->isAccesGestionPubSip()) {
                if ($row['statut'] == Atexo_Config::getParameter('STATUT_AVIS_EN_ATTENTE_VALIDATION_ECO') && !$arrayReturn[$row['idTypeAvis']]['statut_avis_a_traiter']) {
                    $arrayReturn[$row['idTypeAvis']]['statut_avis_a_traiter'] = $row['nbrTypeAvis'];
                }
                if (($row['statut'] == Atexo_Config::getParameter('STATUT_AVIS_REJETE_ECO') || $row['statut'] == Atexo_Config::getParameter('STATUT_AVIS_REJETE_SIP'))
                                       && !$arrayReturn[$row['idTypeAvis']]['statut_avis_rejete']) {
                    $arrayReturn[$row['idTypeAvis']]['statut_avis_rejete'] = $row['nbrTypeAvis'];
                }
                if ($row['statut'] == Atexo_Config::getParameter('STATUT_AVIS_EN_ATTENTE_VALIDATION_SIP') && !$arrayReturn[$row['idTypeAvis']]['statut_avis_attente_validation_sip']) {
                    $arrayReturn[$row['idTypeAvis']]['statut_avis_attente_validation_sip'] = $row['nbrTypeAvis'];
                }
            } else {
                if ($row['statut'] == Atexo_Config::getParameter('STATUT_AVIS_EN_ATTENTE_VALIDATION_SIP') && !$arrayReturn[$row['idTypeAvis']]['statut_avis_a_traiter']) {
                    $arrayReturn[$row['idTypeAvis']]['statut_avis_a_traiter'] = $row['nbrTypeAvis'];
                }
                if ($row['statut'] == Atexo_Config::getParameter('STATUT_AVIS_REJETE_SIP') && !$arrayReturn[$row['idTypeAvis']]['statut_avis_rejete']) {
                    $arrayReturn[$row['idTypeAvis']]['statut_avis_rejete'] = $row['nbrTypeAvis'];
                }
            }
            if ($row['statut'] == Atexo_Config::getParameter('STATUT_AVIS_ENVOI_PLANIFIE') && !$arrayReturn[$row['idTypeAvis']]['statut_avis_envoi_planifie']) {
                $arrayReturn[$row['idTypeAvis']]['statut_avis_envoi_planifie'] = $row['nbrTypeAvis'];
            }
            if ($row['statut'] == Atexo_Config::getParameter('STATUT_AVIS_ENVOYE') && !$arrayReturn[$row['idTypeAvis']]['statut_avis_envoye']) {
                $arrayReturn[$row['idTypeAvis']]['statut_avis_envoye'] = $row['nbrTypeAvis'];
            }

            $arrayReturn[$row['idTypeAvis']]['idTypeAvis'] = $row['idTypeAvis'];
        }

        return $arrayReturn;
    }

    public function getInfosTableauSuiviAvis($criteriaVo, $forNbrResult)
    {
        $temps_debut = microtime(true);
        $temps_debut_req1 = microtime(true);
        $txt = "------------------------------------------ \n\n";
        $txt .= 'Debut fonction  '."\n\n";
        $fp = fopen(Atexo_Config::getParameter('COMMON_TMP').session_id().'timer.txt', 'a');
        fwrite($fp, $txt);
        fclose($fp);



        $autresCriteres = '';
        $commandeName = $criteriaVo->getCommandeName();
        $sensTri = $criteriaVo->getSensTri();

        if (is_array($criteriaVo->getStatut())) {
            if (count($criteriaVo->getStatut())) {
                $autresCriteres .= " AND AP.statut IN ('".implode("','", $criteriaVo->getStatut())."')";
            }
        } else {
            if ('' != $criteriaVo->getStatut() && '0' != $criteriaVo->getStatut()) {
                $autresCriteres .= " AND AP.statut = '".$criteriaVo->getStatut()."'";
            }
        }
        if ('' != $criteriaVo->getIdTypeAvis() && '0' != $criteriaVo->getIdTypeAvis()) {
            $autresCriteres .= " AND TA.id = '".$criteriaVo->getIdTypeAvis()."' ";
        }

        if ('' != $criteriaVo->getRegionAvis()) {
            $autresCriteres .= " AND TA.region = '".$criteriaVo->getRegionAvis()."' ";
        }

        /*if($criteriaVo->getReferenceAvis()) {
            $autresCriteres .= " AND  ";
        }*/

        if ('' != $criteriaVo->getIdService() && '' != $criteriaVo->getOrganisme()) {
            $autresCriteres .= " AND C.organisme = '".$criteriaVo->getOrganisme()."' AND C.service_id = '".$criteriaVo->getIdService()."'";
        }

        if ('' != $criteriaVo->getIdTypeProcedure() && '0' != $criteriaVo->getIdTypeProcedure()) {
            $autresCriteres .= " AND C.id_type_procedure = '".$criteriaVo->getIdTypeProcedure()."' ";
        }

        if ('' != $criteriaVo->getDateRemisePlisStart()) {
            $autresCriteres .= " AND C.datefin >= '".substr(Atexo_Util::frnDateTime2iso($criteriaVo->getDateRemisePlisStart()), 0, 10)." 00:00:00' ";
        }

        if ('' != $criteriaVo->getDateRemisePlisEnd()) {
            $autresCriteres .= " AND C.datefin <= '".substr(Atexo_Util::frnDateTime2iso($criteriaVo->getDateRemisePlisEnd()), 0, 10)." 23:59:59' ";
        }

        if ('' != $criteriaVo->getDateReceptionStart()) {
            $autresCriteres .= " AND AP.date_envoi >= '".substr(Atexo_Util::frnDateTime2iso($criteriaVo->getDateReceptionStart()), 0, 10)." 00:00:00' ";
        }

        if ('' != $criteriaVo->getDateReceptionEnd()) {
            $autresCriteres .= " AND AP.date_envoi <= '".substr(Atexo_Util::frnDateTime2iso($criteriaVo->getDateReceptionEnd()), 0, 10)." 23:59:59' ";
        }

        if ('' != $criteriaVo->getIdSupport() && '0' != $criteriaVo->getIdSupport()) {
            $autresCriteres .= " AND DP.id_support = '".$criteriaVo->getIdSupport()."' ";
        }
        if ((new self())->isAccesGestionPubSip()) {
            if ('' != $criteriaVo->getIdAgentValidateurAvisPub()) {
                $autresCriteres .= ' AND AP.id_agent_validateur_sip IN '.$criteriaVo->getIdAgentValidateurAvisPub();
            }
        } else {
            if ('' != $criteriaVo->getIdAgentValidateurAvisPub()) {
                $autresCriteres .= ' AND AP.id_agent_validateur_eco IN '.$criteriaVo->getIdAgentValidateurAvisPub();
            }
        }

        if ('' != $criteriaVo->getReferenceAvis()) {
            $autresCriteres .= " AND C.reference_utilisateur LIKE '%".$criteriaVo->getReferenceAvis()."%'";
        }

        if (true == $criteriaVo->getValidationSip()) {
            $autresCriteres .= " AND AP.Sip = '1' ";
        }

        if ($forNbrResult) {
            $sql = ' SELECT count(distinct(AP.id)) as nbrAvisPub ';
        } else {
            $sql = ' SELECT distinct(AP.id), C.reference_utilisateur , C.intitule , C.Objet , C.organisme , C.service_id , C.datefin, C.id_type_procedure ,AP.id_agent as agentCreateur, ';
            $sql .= ' C.etat_validation, AP.id as idAvisPub, AP.type_avis, C.date_mise_en_ligne_souhaitee, AP.statut as statutAvis, C.datefin, C.date_mise_en_ligne_calcule, C.id,  ';
            $sql .= ' C.id_createur as id_agent_createur_cons, AP.id_agent_validateur_eco as id_agent_validateur_pub_eco, AP.id_agent_validateur_sip as id_agent_validateur_pub_sip ';
        }
        $sql .= ' FROM consultation C, Avis_Pub AP, Type_Avis_Pub TA, Destinataire_Pub DP ';
        $sql .= ' WHERE C.id = AP.CONSULTATION_ID ';

        $sql .= ' AND TA.id = AP.type_avis ';
        $sql .= ' AND DP.id_avis = AP.id ';
        //$sql .= " AND AP.statut != '".Atexo_Config::getParameter('STATUT_AVIS_A_TRANSMETTRE')."'" ;

        if ((new self())->isAccesGestionPubSip()) {
            $sql .= " AND AP.statut IN
			(
			'".Atexo_Config::getParameter('STATUT_AVIS_EN_ATTENTE_VALIDATION_SIP')."',
			'".Atexo_Config::getParameter('STATUT_AVIS_ENVOI_PLANIFIE')."',
			'".Atexo_Config::getParameter('STATUT_AVIS_ENVOYE')."',
			'".Atexo_Config::getParameter('STATUT_AVIS_REJETE_SIP')."'
			)";
        } else {
            $sql .= " AND AP.statut IN
			(
			'".Atexo_Config::getParameter('STATUT_AVIS_EN_ATTENTE_VALIDATION_ECO')."',
			'".Atexo_Config::getParameter('STATUT_AVIS_REJETE_ECO')."',
			'".Atexo_Config::getParameter('STATUT_AVIS_EN_ATTENTE_VALIDATION_SIP')."',
			'".Atexo_Config::getParameter('STATUT_AVIS_ENVOI_PLANIFIE')."',
			'".Atexo_Config::getParameter('STATUT_AVIS_ENVOYE')."',
			'".Atexo_Config::getParameter('STATUT_AVIS_REJETE_SIP')."'
			)";
        }
        $sql .= ' AND C.organisme = AP.organisme ';
        $sql .= ' AND C.id_etat_consultation != '.(new Atexo_Db())->quote(Atexo_Config::getParameter('STATUS_ARCHIVE_REALISEE'));

        $sql .= $autresCriteres;

        if (!$forNbrResult) {
            if (!$commandeName) {
                $sql .= ' ORDER BY C.date_mise_en_ligne_souhaitee DESC ';
            } else {
                if ('agent_createur' == $commandeName) {
                    $sql .= ' ORDER BY agentCreateur '.$sensTri;
                } elseif ('ref_cons' == $commandeName) {
                    $sql .= ' ORDER BY C.reference_utilisateur '.$sensTri;
                } elseif ('intitule_cons' == $commandeName) {
                    $sql .= ' ORDER BY C.intitule '.$sensTri;
                } elseif ('objet_cons' == $commandeName) {
                    $sql .= ' ORDER BY C.Objet '.$sensTri;
                } elseif ('entite_achat' == $commandeName) {
                    $sql .= ' ORDER BY C.service_id '.$sensTri;
                } elseif ('date_cloture' == $commandeName) {
                    $sql .= ' ORDER BY C.datefin '.$sensTri;
                } elseif ('date_mise_ligne' == $commandeName) {
                    $sql .= ' ORDER BY C.date_mise_en_ligne_souhaitee '.$sensTri;
                } elseif ('type_avis' == $commandeName) {
                    $sql .= ' ORDER BY AP.type_avis '.$sensTri;
                } elseif ('statut_avis' == $commandeName) {
                    $sql .= ' ORDER BY AP.statut '.$sensTri;
                } elseif ('agent_createur_cons' == $commandeName) {
                    $sql .= ' ORDER BY id_agent_createur_cons '.$sensTri;
                } elseif ('agent_validateur_pub' == $commandeName) {
                    if ((new self())->isAccesGestionPubSip()) {
                        $sql .= ' ORDER BY id_agent_validateur_pub_sip '.$sensTri;
                    } else {
                        $sql .= ' ORDER BY id_agent_validateur_pub_eco '.$sensTri;
                    }
                }
            }
        }

        /*if(!$forNbrResult) {
        echo $sql;exit;
        }  */
        if (!$forNbrResult && $criteriaVo->getLimit()) {
            $sql .= ' limit '.$criteriaVo->getOffset().','.($criteriaVo->getLimit() + $criteriaVo->getOffset());
        }

        $statement = Atexo_Db::getLinkCommon(true)->query($sql);

        $temps_fin_req1 = microtime(true);
        $txt = 'Temps exécution requete : '.round($temps_fin_req1 - $temps_debut_req1, 4)."\n\n";
        $fp = fopen(Atexo_Config::getParameter('COMMON_TMP').session_id().'timer.txt', 'a');
        fwrite($fp, $txt);
        fclose($fp);

        $arrayInfos = [];
        $i = 0;

        $temps_debut_iteration = microtime(true);

        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            if ($forNbrResult) {
                return $row['nbrAvisPub'];
            }
            $arrayInfos[$i]['id_avis'] = $row['idAvisPub'];
            $arrayInfos[$i]['type_avis'] = $row['type_avis'];
            $arrayInfos[$i]['libelle_type_avis'] = self::retrieveLibelleTypeAvisByIdAvis($row['type_avis']);
            $arrayInfos[$i]['reference'] = $row['reference_utilisateur'];
            $arrayInfos[$i]['intitule'] = $row['intitule'];
            $arrayInfos[$i]['Objet'] = $row['Objet'];
            $arrayInfos[$i]['organisme'] = $row['organisme'];
            $arrayInfos[$i]['libelleService'] = (new Atexo_EntityPurchase())->getEntityPathById($row['service_id'], $row['organisme']);
            $arrayInfos[$i]['datefin'] = $row['datefin'];
            $arrayInfos[$i]['libelle_type_procedure'] = (new Atexo_Consultation_ProcedureType())->retrieveTypeProcedure($row['id_type_procedure'], false, $row['organisme'], Atexo_CurrentUser::readFromSession('lang'));
            $arrayInfos[$i]['code_type_procedure'] = (new Atexo_Consultation_ProcedureType())->retrieveAcronymeTypeProcedure($row['id_type_procedure'], false);
            //$arrayInfos[$i]['nom_createur'] = $row['agentCreateur'];
            $agent = (new Atexo_Agent())->retrieveAgent($row['agentCreateur']);
            $nomAgent = '';
            $prenomAgent = '';
            $email = '';
            $numTel = '';
            if ($agent instanceof CommonAgent) {
                $nomAgent = $agent->getNom();
                $prenomAgent = $agent->getPrenom();
                $email = $agent->getEmail();
                $numTel = $agent->getNumTel();
            }

            $arrayInfos[$i]['nom_createur'] = ($agent instanceof CommonAgent) ? $nomAgent.' '.$prenomAgent : '-';
            $arrayInfos[$i]['infos_compl_createur'] = Prado::localize('DEFINE_ADRESSE_ELECTRONIQUE').' : '.$email.'<br/> '.Prado::localize('TEXT_TELEPHONE').' : '.$numTel;
            $arrayInfos[$i]['etat_validation'] = $row['etat_validation'];
            $arrayInfos[$i]['date_mise_en_ligne_souhaitee'] = $row['date_mise_en_ligne_souhaitee'];
            $arrayInfos[$i]['statut_avis'] = $row['statutAvis'];
            $arrayInfos[$i]['ref_cons'] = $row['id'];
            $arrayInfos[$i]['sigleOrg'] = (new Atexo_Organismes())->getSigleOrganisme($row['organisme']);

            $agentCreateurCons = (new Atexo_Agent())->retrieveAgent($row['id_agent_createur_cons']);
            $nomAgentCreateurCons = '';
            $prenomAgentCreateurCons = '';
            $emailAgentCreateurCons = '';
            $numTelAgentCreateurCons = '';
            if ($agentCreateurCons instanceof CommonAgent) {
                $nomAgentCreateurCons = $agentCreateurCons->getNom();
                $prenomAgentCreateurCons = $agentCreateurCons->getPrenom();
                $emailAgentCreateurCons = $agentCreateurCons->getEmail();
                $numTelAgentCreateurCons = $agentCreateurCons->getNumTel();
            }
            $arrayInfos[$i]['agent_createur_cons'] = ($agentCreateurCons instanceof CommonAgent) ? $nomAgentCreateurCons.' '.$prenomAgentCreateurCons : '-';
            $arrayInfos[$i]['infos_compl_agent_createur_cons'] = Prado::localize('DEFINE_ADRESSE_ELECTRONIQUE').' : '.$emailAgentCreateurCons.'<br/> '.
                                               Prado::localize('TEXT_TELEPHONE').' : '.$numTelAgentCreateurCons;

            $agentValidateurPub = '';
            if ((new self())->isAccesGestionPubSip()) {
                $agentValidateurPub = (new Atexo_Agent())->retrieveAgent($row['id_agent_validateur_pub_sip']);
            } else {
                $agentValidateurPub = (new Atexo_Agent())->retrieveAgent($row['id_agent_validateur_pub_eco']);
            }

            $nomAgentValidateurPub = '';
            $prenomAgentValidateurPub = '';
            $emailAgentValidateurPub = '';
            $numTelAgentValidateurPub = '';
            if ($agentValidateurPub instanceof CommonAgent) {
                $nomAgentValidateurPub = $agentValidateurPub->getNom();
                $prenomAgentValidateurPub = $agentValidateurPub->getPrenom();
                $emailAgentValidateurPub = $agentValidateurPub->getEmail();
                $numTelAgentValidateurPub = $agentValidateurPub->getNumTel();
            }
            $arrayInfos[$i]['agent_validateur_pub'] = ($agentValidateurPub instanceof CommonAgent) ? $nomAgentValidateurPub.' '.$prenomAgentValidateurPub : '-';
            $arrayInfos[$i]['infos_compl_agent_validateur_pub'] = Prado::localize('DEFINE_ADRESSE_ELECTRONIQUE').' : '.$emailAgentValidateurPub.'<br/> '.
                                               Prado::localize('TEXT_TELEPHONE').' : '.$numTelAgentValidateurPub;

            ++$i;
        }
        $temps_fin_iteration = microtime(true);
        $txt = 'Temps itération: '.round($temps_fin_iteration - $temps_debut_iteration, 4)."\n\n";
        $fp = fopen(Atexo_Config::getParameter('COMMON_TMP').session_id().'timer.txt', 'a');
        fwrite($fp, $txt);
        fclose($fp);
        if ($forNbrResult) {
            return $i;
        }

        $temps_fin_fonction = microtime(true);
        $txt = 'Fin fonction: '.round($temps_fin_fonction - $temps_debut, 4)."\n\n ------------------------------------------ \n\n";
        $fp = fopen(Atexo_Config::getParameter('COMMON_TMP').session_id().'timer.txt', 'a');
        fwrite($fp, $txt);
        fclose($fp);

        return $arrayInfos;
    }

    public function retrieveListeDestinatairesAvis($idAvis, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $criteria = new Criteria();
        $criteria->add(CommonDestinatairePubPeer::ID_AVIS, $idAvis);
        $criteria->add(CommonDestinatairePubPeer::ORGANISME, $organisme);
        $destPub = CommonDestinatairePubPeer::doSelect($criteria, $connexion);

        return $destPub;
    }

    public function retrieveLibelleTypeAvisByIdAvis($idTypeAvis, $returnObject = false)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $criteria = new Criteria();
        $criteria->add(CommonTypeAvisPubPeer::ID, $idTypeAvis);
        $typeAvis = CommonTypeAvisPubPeer::doSelectOne($criteria, $connexion);
        if ($returnObject) {
            return $typeAvis;
        } else {
            return ($typeAvis) ? $typeAvis->getLibelle() : '';
        }
    }

    /**
     * valide un avis par on Id.
     */
    public function validerAvis($idAvis, $organisme)
    {
        $avisPub = self::retreiveAvis($idAvis, $organisme);
        $statutAvis = '';
        if ($avisPub instanceof CommonAvisPub) {
            $oldStatutAvis = $avisPub->getStatut();
            if (($oldStatutAvis == Atexo_Config::getParameter('STATUT_AVIS_EN_ATTENTE_VALIDATION_ECO') && !(new self())->isAccesGestionPubSip())
            || ($oldStatutAvis == Atexo_Config::getParameter('STATUT_AVIS_EN_ATTENTE_VALIDATION_SIP') && (new self())->isAccesGestionPubSip())) {
                if ((new self())->isAccesGestionPubSip() || '0' == $avisPub->getSip() || !$avisPub->getSip()) {
                    $statutAvis = Atexo_Config::getParameter('STATUT_AVIS_ENVOI_PLANIFIE');
                } elseif (!(new self())->isAccesGestionPubSip() && '1' == $avisPub->getSip()) {
                    $statutAvis = Atexo_Config::getParameter('STATUT_AVIS_EN_ATTENTE_VALIDATION_SIP');
                }
                self::modifierStatutDestinataire($idAvis, $organisme, $statutAvis);
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                self::createHistorique($organisme, $idAvis, date('Y-m-d H:i:s'), $statutAvis, $connexion);
            }
        }
    }

    /**
     * rejete un avis par on Id.
     */
    public function rejeterAvis($idAvis, $organisme, $motifRejet = null)
    {
        //Ne pas rejetter un avis deja rejetté
        $avis = self::retreiveAvis($idAvis, $organisme);
        if ($avis instanceof CommonAvisPub) {
            if ($avis->getStatut() != Atexo_Config::getParameter('STATUT_AVIS_ENVOYE')) {
                if ($avis->getStatut() == Atexo_Config::getParameter('STATUT_AVIS_REJETE_ECO')
                   || $avis->getStatut() == Atexo_Config::getParameter('STATUT_AVIS_REJETE_SIP')) {
                    return;
                }
                $statut = '';
                if ((new self())->isAccesGestionPubSip()) {
                    $statut = Atexo_Config::getParameter('STATUT_AVIS_REJETE_SIP');
                } else {
                    $statut = Atexo_Config::getParameter('STATUT_AVIS_REJETE_ECO');
                }
                self::modifierStatutDestinataire($idAvis, $organisme, $statut);
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                self::createHistorique($organisme, $idAvis, date('Y-m-d H:i:s'), $statut, $connexion, $motifRejet);
            }
        }
    }

    public function modifierStatutDestinataire($idAvis, $organisme, $statut)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $avis = self::retreiveAvis($idAvis, $organisme);
        $avis->setStatut($statut);
        if ($statut == Atexo_Config::getParameter('STATUT_AVIS_ENVOI_PLANIFIE')) {
            $avis->setDateValidation(date('Y-m-d H:i:s'));
        }
        if ((new self())->isAccesGestionPubSip()) {
            $avis->setIdAgentValidateurSip(Atexo_CurrentUser::getIdAgentConnected());
        } else {
            $avis->setIdAgentValidateurEco(Atexo_CurrentUser::getIdAgentConnected());
        }
        $avis->save($connexion);
        if ($statut == Atexo_Config::getParameter('STATUT_AVIS_REJETE_ECO')
           || $statut == Atexo_Config::getParameter('STATUT_AVIS_REJETE_SIP')) {
            $listeDestinataires = self::getListeDestinataires($idAvis, $organisme);
            if (is_array($listeDestinataires) && count($listeDestinataires)) {
                foreach ($listeDestinataires as $destinataire) {
                    $statutDestinataire = '';
                    if ($statut == Atexo_Config::getParameter('STATUT_AVIS_REJETE_ECO')) {
                        $statutDestinataire = Atexo_Config::getParameter('STATUT_DESTINATAIRE_AVIS_REJETE_ECO');
                    } elseif ($statut == Atexo_Config::getParameter('STATUT_AVIS_REJETE_SIP')) {
                        $statutDestinataire = Atexo_Config::getParameter('STATUT_DESTINATAIRE_AVIS_REJETE_SIP');
                    }
                    $destinataire->setEtat($statutDestinataire);
                    $destinataire->save($connexion);
                }
            }
        }
    }

    public function getHistoriqueAvis($idAvis, $organisme, $commandeName = null, $sensTri = null)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $criteria = new Criteria();
        $criteria->add(CommonHistoriqueAvisPubPeer::ID_AVIS, $idAvis);
        $criteria->add(CommonHistoriqueAvisPubPeer::ORGANISME, $organisme);
        if ($commandeName && 'date' == $commandeName) {
            if ('ASC' == $sensTri) {
                $criteria->addAscendingOrderByColumn(CommonHistoriqueAvisPubPeer::DATE_MODIFICATION);
            } elseif ('DESC' == $sensTri) {
                $criteria->addDescendingOrderByColumn(CommonHistoriqueAvisPubPeer::DATE_MODIFICATION);
            }
        }

        return CommonHistoriqueAvisPubPeer::doSelect($criteria, $connexion);
    }

    public function getTypeAvis($idTypeProcedure, $organisme)
    {
        $c = new Criteria();
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonTypeAvisPubProcedurePeer::ID_TYPE_PROCEDURE, $idTypeProcedure);
        $c->add(CommonTypeAvisPubProcedurePeer::ORGANISME, $organisme);
        $typeProcedure = CommonTypeAvisPubProcedurePeer::doSelect($c, $connexion);

        return $typeProcedure;
    }

    public function addXmlDestOpoce($idDest, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonFormXmlDestinataireOpocePeer::ORGANISME, $organisme);
        $c->add(CommonFormXmlDestinataireOpocePeer::ID_DESTINATAIRE_OPOCE, $idDest);
        $xmlOpoce = CommonFormXmlDestinataireOpocePeer::doSelectOne($c, $connexion);

        return $xmlOpoce;
    }

    public function getDestinatairesValidesEcoEtNonPublieOpoce($connexion = null, $hasDossierSUB = false)
    {
        if (!$connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        }
        $c = new Criteria();
        $c->add(CommonDestinatairePubPeer::ETAT, Atexo_Config::getParameter('STATUT_DESTINATAIRE_AVIS_OPOCE_EN_ATTENTE_INTEGRATION'), Criteria::EQUAL);
        $c->addOr(CommonDestinatairePubPeer::ETAT, Atexo_Config::getParameter('STATUT_DESTINATAIRE_AVIS_OPOCE_INTEGRE'), Criteria::EQUAL);
        $c->add(CommonDestinatairePubPeer::ID_SUPPORT, Atexo_Config::getParameter('TYPE_SUPPORT_OPOCE'));
        if ($hasDossierSUB) {
            $c->add(CommonDestinatairePubPeer::ID_DOSSIER, 0, Criteria::NOT_EQUAL);
        } else {
            $c->add(CommonDestinatairePubPeer::ID_DOSSIER, 0);
        }
        $listeDestinataire = CommonDestinatairePubPeer::doSelect($c, $connexion);

        return $listeDestinataire;
    }

    public function getDestinatairesValidesEtPubliesOpoce($hasDossierSUB = false)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonDestinatairePubPeer::ETAT, Atexo_Config::getParameter('STATUT_DESTINATAIRE_AVIS_OPOCE_PUBLIE'));
        $c->add(CommonDestinatairePubPeer::ID_SUPPORT, Atexo_Config::getParameter('TYPE_SUPPORT_OPOCE'));
        if ($hasDossierSUB) {
            $c->add(CommonDestinatairePubPeer::ID_DOSSIER, 0, Criteria::NOT_EQUAL);
        } else {
            $c->add(CommonDestinatairePubPeer::ID_DOSSIER, 0);
        }
        $c->add(CommonFormXmlDestinataireOpocePeer::LIEN_PUBLICATION, null);
        $c->addOr(CommonFormXmlDestinataireOpocePeer::LIEN_PUBLICATION, '');
        $c->addJoin(CommonDestinatairePubPeer::ID, CommonFormXmlDestinataireOpocePeer::ID_DESTINATAIRE_OPOCE);
        $listeDestinataire = CommonDestinatairePubPeer::doSelect($c, $connexion);

        return $listeDestinataire;
    }

    public function retrieveFormXmlDestPubByRefCons($refCons, $organisme)
    {
        $sql = ' SELECT FX.lien_publication, DP.etat as statutDestinataire ';
        $sql .= ' FROM Avis_Pub AP, Destinataire_Pub DP, FormXmlDestinataireOpoce FX ';
        $sql .= ' WHERE AP.id= DP.id_avis AND AP.organisme = DP.organisme ';
        $sql .= ' AND DP.id= FX.id_destinataire_opoce AND DP.organisme = FX.organisme ';
        $sql .= " AND DP.id_support= '".Atexo_Config::getParameter('TYPE_SUPPORT_OPOCE')."' AND DP.etat= '".Atexo_Config::getParameter('STATUT_DESTINATAIRE_AVIS_OPOCE_PUBLIE')."' ";
        $sql .= " AND FX.lien_publication IS NOT NULL AND FX.lien_publication !='' ";
        $sql .= " AND AP.statut != '".Atexo_Config::getParameter('STATUT_AVIS_REJETE_ECO')."'";
        $sql .= " AND AP.consultation_id = '".(new Atexo_Db())->quote($refCons)."' AND AP.organisme = '".(new Atexo_Db())->quote($organisme)."'";

        $statement = Atexo_Db::getLinkCommon(true)->query($sql);
        $resultat = [];
        $increment = 0;
        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            $resultat[$increment]['lien_pub'] = $row['lien_publication'];
            $resultat[$increment]['statut_destinataire'] = $row['statutDestinataire'];
            ++$increment;
        }

        return $resultat;
    }

    /**
     * Permet de precuperer un support au statut complet.
     *
     * @param $idAvis : l'identifiant de l'avis global
     * @param $organisme: l'organisme de l'avis
     * @param $connexion: l'objet connexion
     * @param $support: le support de publication
     */
    public function getDestinataireComplet($idAvis, $organisme, $connexion, $support)
    {
        $criteria = new Criteria();
        $criteria->add(CommonDestinatairePubPeer::ID_AVIS, $idAvis);
        $criteria->add(CommonDestinatairePubPeer::ORGANISME, $organisme);
        $criteria->add(CommonDestinatairePubPeer::ID_SUPPORT, $support);
        $criteria->add(CommonDestinatairePubPeer::ETAT, Atexo_Config::getParameter('STATUT_DESTINATAIRE_AVIS_COMPLET'));
        $destinataire = CommonDestinatairePubPeer::doSelectOne($criteria, $connexion);

        return $destinataire;
    }

    public function getNoDocExt($idDestinataire, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonDestinatairePubPeer::ID, $idDestinataire);
        $c->add(CommonDestinatairePubPeer::ORGANISME, $organisme);
        $c->addJoin(CommonDestinatairePubPeer::ID, CommonFormXmlDestinataireOpocePeer::ID_DESTINATAIRE_OPOCE);
        $c->addAlias('FormXml', CommonFormXmlDestinataireOpocePeer::TABLE_NAME);
        $c->addJoin(CommonDestinatairePubPeer::ORGANISME, CommonFormXmlDestinataireOpocePeer::alias('FormXml', CommonFormXmlDestinataireOpocePeer::ORGANISME));
        //     $c->addJoin(CommonDestinatairePubPeer::ORGANISME, CommonFormXmlDestinataireOpocePeer::ORGANISME);

        $formlXml = CommonFormXmlDestinataireOpocePeer::doSelectOne($c, $connexion);
        if ($formlXml instanceof CommonFormXmlDestinataireOpoce) {
            return $formlXml->getNoDocExt();
        }

        return '';
    }

    public function getCodeRetour($idDestinataire, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonDestinatairePubPeer::ID, $idDestinataire);
        $c->add(CommonDestinatairePubPeer::ORGANISME, $organisme);
        $c->addJoin(CommonDestinatairePubPeer::ID, CommonFormXmlDestinataireOpocePeer::ID_DESTINATAIRE_OPOCE);
        $c->addAlias('FormXml', CommonFormXmlDestinataireOpocePeer::TABLE_NAME);
        $c->addJoin(CommonDestinatairePubPeer::ORGANISME, CommonFormXmlDestinataireOpocePeer::alias('FormXml', CommonFormXmlDestinataireOpocePeer::ORGANISME));
        //     $c->addJoin(CommonDestinatairePubPeer::ORGANISME, CommonFormXmlDestinataireOpocePeer::ORGANISME);
        $formlXml = CommonFormXmlDestinataireOpocePeer::doSelectOne($c, $connexion);
        if ($formlXml instanceof CommonFormXmlDestinataireOpoce) {
            return $formlXml->getCodeRetour();
        }

        return '';
    }

    public function getRefConsultationByIdDestPub($idDest, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $criteria = new Criteria();
        $criteria->add(CommonDestinatairePubPeer::ID, $idDest);
        $criteria->add(CommonDestinatairePubPeer::ORGANISME, $organisme);
        $criteria->addJoin(CommonAvisPubPeer::ID, CommonDestinatairePubPeer::ID_AVIS);
        //  $criteria->addJoin(CommonAvisPubPeer::ORGANISME, CommonDestinatairePubPeer::ORGANISME);
        $criteria->addAlias('DestinatairePub', CommonDestinatairePubPeer::TABLE_NAME);
        $criteria->addJoin(CommonAvisPubPeer::ORGANISME, CommonDestinatairePubPeer::alias('DestinatairePub', CommonDestinatairePubPeer::ORGANISME));

        $avis = CommonAvisPubPeer::doSelectOne($criteria, $connexion);

        return $avis ? $avis->getConsultationId() : false;
    }

    public function getInfosIndentifiantsOpoce($idDestinataire, $organisme)
    {
        $texte = '';
        $formXmlDest = self::getObjetAvisOpoceByIdDest($idDestinataire, $organisme);
        if ($formXmlDest) {
            $texte = Prado::localize('IDENTIFIANT_UNIQUE_AVIS').$formXmlDest->getNoDocExt();
            $texte .= ' | ';
            $texte .= Prado::localize('IDENTIFIANT_TED').$formXmlDest->getCodeRetour();
            $texte .= ' | ';
            $texte .= Prado::localize('DEFINE_IDENTIFIANT_JOUE').$formXmlDest->getIdJoue();
        }

        return $texte;
    }

    public function getLastMotifRejet($idAvis, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonAvisPubPeer::ORGANISME, $organisme);
        $c->add(CommonAvisPubPeer::ID, $idAvis);
        $c->add(CommonAvisPubPeer::STATUT, Atexo_Config::getParameter('STATUT_AVIS_REJETE_ECO'));
        $c->addOr(CommonAvisPubPeer::STATUT, Atexo_Config::getParameter('STATUT_AVIS_REJETE_SIP'));
        $c->addJoin(CommonAvisPubPeer::ID, CommonHistoriqueAvisPubPeer::ID_AVIS);
        $c->addDescendingOrderByColumn('id');
        $c->setLimit(1);

        $historiques = CommonHistoriqueAvisPubPeer::doSelect($c, $connexion);
        if ($historiques && count($historiques)) {
            return $historiques[count($historiques) - 1]->getMotifRejet();
        }

        return '';
    }

    /**
     * Retourne le libellé du statut de l'avis de publicité.
     *
     * @param $idAvis: identifiant de l'avis de publicité
     * @param $organisme: l'organisme de l'avis de publicité
     *
     * @return le libelle de l'avis de publicité
     */
    public function getLibelleStatutAvisPub($idAvis, $organisme)
    {
        $avisPub = self::retreiveAvis($idAvis, $organisme);
        if ($avisPub instanceof CommonAvisPub) {
            return $avisPub->getLibelleStatut();
        }
    }

    /**
     * Recupere le destinataire par l'identifiant de l'avis et le type de support.
     *
     * @param $idAvis: identifiant de l'avis de publicité
     * @param $typeSupport: le type de support de publicité
     *
     * @return : destinataire de l'avis de publicité
     */
    public function getDestinataireByIdAvisAndTypeSupport($idAvis, $typeSupport)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $criteria = new Criteria();
        $criteria->add(CommonDestinatairePubPeer::ID_AVIS, $idAvis);
        $criteria->add(CommonDestinatairePubPeer::ID_SUPPORT, $typeSupport);
        $criteria->add(CommonDestinatairePubPeer::ORGANISME, Atexo_CurrentUser::getCurrentOrganism());
        $dstPub = CommonDestinatairePubPeer::doSelectOne($criteria, $connexion);

        return $dstPub ?: false;
    }

    /**
     * Permet de verifier le profil d'accès à la fonctionnalité de suivi de la publicité.
     *
     * @return : true si accès agent SIP, false si accès agent ECO
     */
    public function isAccesGestionPubSip()
    {
        if (Atexo_CurrentUser::hasHabilitation('GestionPubEntiteCoordinatrice')
               && Atexo_CurrentUser::readFromSession('acces_gestion_publicite') == Atexo_Config::getParameter('ID_SERVCE_METIER_ENTITE_COORDINATRICE')) {
            return false;
        } elseif (Atexo_CurrentUser::hasHabilitation('GestionPubSip')
           && Atexo_CurrentUser::readFromSession('acces_gestion_publicite') == Atexo_Config::getParameter('ID_SERVCE_METIER_SIP')) {
            return true;
        }
    }

    /**
     * Permet de verifier le profil d'accès à la fonctionnalité de la publicité.
     *
     * @return : true si accès agent MPE, false sinon
     */
    public function isAccesGestionPubMpe()
    {
        return !in_array(
            Atexo_CurrentUser::readFromSession('acces_gestion_publicite'),
            [
                Atexo_Config::getParameter('ID_SERVCE_METIER_SIP'),
                Atexo_Config::getParameter('ID_SERVCE_METIER_ENTITE_COORDINATRICE')
            ]
        );
    }

    /**
     * Permet de recuperer un enregistrement de la table 'AnnoncePressePieceJointe'.
     *
     * @param $idAnnoncePresse: identifiant de la table Annonce_Press
     * @param $organisme: l'organisme de l'avis
     *
     * @return : l'objet de la table Annonce_Press_PieceJointe
     */
    public function getAnnoncePressPjByIdAnnonceAndOrg($idAnnoncePresse, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonAnnoncePressPieceJointePeer::ORGANISME, $organisme);
        $c->add(CommonAnnoncePressPieceJointePeer::ID_ANNONCE_PRESS, $idAnnoncePresse);
        $annoncePressPj = CommonAnnoncePressPieceJointePeer::doSelect($c, $connexion);

        return $annoncePressPj;
    }

    /**
     * Recupère le modèle de document en fonction du type.
     *
     * @param $typeAvis: le type de l'avis de publicité
     *
     * @return le modele de document généré
     */
    public function getModeleDocumentPresse($typeAvis)
    {
        $typeAvisPub = self::retrieveLibelleTypeAvisByIdAvis($typeAvis, true);
        if ($typeAvisPub instanceof CommonTypeAvisPub) {
            return Atexo_Config::getParameter('kernel.project_dir').'/legacy/'.$typeAvisPub->getRessourceDocPresse();
        }

        return '';
    }

    /**
     * Permet de generer le document presse en fonction du modele.
     *
     * @param $modeleAvis: modèle de document à générer
     * @param $connexion: l'objet connexion
     * @param $idAvis: identifiant de l'avis de publicité
     * @param $consultation: l'objet consultation
     *
     * @return : un tableau d'elements respectifs: idBlob, fichier odt et fichier rtf
     */
    public function genererAvisPressePortail($modeleAvis, $connexion, $idAvis, $consultation, $langue = 'fr', $genererPdf = false, $datePubCon = false)
    {
        $organisme = $consultation->getOrganisme();
        $file = (new Atexo_Publicite_AvisPdf())->generationAvisPublicite($consultation, $modeleAvis, true, $langue, $idAvis, $datePubCon);
        if (!$genererPdf) {
            $rtfContent = (new RtfGeneratorClient())->genererRtf($file);
            $fichier = $file.'.rtf';
            Atexo_Util::write_file($fichier, $rtfContent);
            $nameFile = explode('_', $file);
            $atexoBlob = new Atexo_Blob();
            $idBlob = $atexoBlob->insert_blob($nameFile[3], $fichier, $organisme);
            $fileNew = self::getNomFichierAnnoncePresse($consultation->getId(), $idAvis).'.rtf';

            return [0 => $idBlob, 1 => $file, 2 => $fileNew];
        } else {
            $filePdf = Atexo_Config::getParameter('COMMON_TMP').'avis_'.date('Y_m_d_h_i_s').'.pdf';
            system('touch '.$filePdf);
            $pdfContent = (new PdfGeneratorClient())->genererPdf($file);
            Atexo_Util::write_file($filePdf, $pdfContent);
            $fileNew = 'Avis.pdf';
            $atexoBlob = new Atexo_Blob();
            $idBlob = $atexoBlob->insert_blob($fileNew, $filePdf, $consultation->getOrganisme());

            return [0 => $idBlob, 1 => $filePdf, 2 => $fileNew];
        }
    }

    /**
     * Permet de recuperer une pièce jointe à l'annonce presse par son identifiant.
     *
     * @param $idPj: l'identifiant de la pièce jointe
     * @param $organisme: l'organisme de la publicité
     * @param $connexion: l'objet connexion
     *
     * @return un objet de la classe CommonAnnoncePressPieceJointe
     */
    public function CommonAnnoncePressPieceJointeByIdAndOrganisme($idPj, $organisme, $connexion)
    {
        return BaseCommonAnnoncePressPieceJointePeer::retrieveByPK($idPj, $organisme, $connexion);
    }

    /**
     * Permet de recuperer un enregistrement de la table 'AnnoncePressePieceJointe'.
     *
     * @param $idPj: identifiant de la pièce jointe (réfère à l'identifiant du blob)
     * @param $organisme: l'organisme de l'avis
     *
     * @return : l'objet de la table Annonce_Press_PieceJointe
     */
    public function getAnnoncePressPjByIdPj($idPj, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonAnnoncePressPieceJointePeer::ORGANISME, $organisme);
        $c->add(CommonAnnoncePressPieceJointePeer::PIECE, $idPj);
        $annoncePressPj = CommonAnnoncePressPieceJointePeer::doSelectOne($c, $connexion);

        return $annoncePressPj;
    }

    /**
     * Permet de recuperer le contenu du document à générer à la volée.
     *
     * @param $avisPub: l'avis de publicité
     * @param $consultation: la consultation
     *
     * @return le contenu du fichier à générer
     */
    public function getFileContent(CommonAvisPub $avisPub, CommonConsultation $consultation, $fileRtf = false)
    {
        $modele = self::getModeleDocumentPresse($avisPub->getTypeAvis());
        if ($modele) {
            //Génération du document à envoyer à la presse
            $file = (new Atexo_Publicite_AvisPdf())->generationAvisPublicite($consultation, $modele, true, 'fr', $avisPub->getId());
            if (!$fileRtf) {
                $filePdf = Atexo_Config::getParameter('COMMON_TMP').'avis_'.date('Y_m_d_h_i_s').'.pdf';
                $pdfContent = (new PdfGeneratorClient())->genererPdf($file);
                Atexo_Util::write_file($filePdf, $pdfContent);

                return $filePdf;
            } else {
                $fileRtf = Atexo_Config::getParameter('COMMON_TMP').'avis_'.date('Y_m_d_h_i_s').'.rtf';
                $rtfContent = (new RtfGeneratorClient())->genererRtf($file);
                Atexo_Util::write_file($fileRtf, $rtfContent);

                return $fileRtf;
            }
        }
    }

    /**
     * Permet de recuperer l'avis OPOCE contenant le formulaire xml.
     *
     * @param $idDestinataire: identifiant du destinataire
     * @param $organisme: organisme concerné
     *
     * @return objet avis Opoce, sinon false
     */
    public function getObjetAvisOpoceByIdDest($idDestinataire, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonFormXmlDestinataireOpocePeer::ID_DESTINATAIRE_OPOCE, $idDestinataire);
        $c->add(CommonFormXmlDestinataireOpocePeer::ORGANISME, $organisme);
        $formlXml = CommonFormXmlDestinataireOpocePeer::doSelectOne($c, $connexion);
        if ($formlXml instanceof CommonFormXmlDestinataireOpoce) {
            return $formlXml;
        }

        return false;
    }

    /**
     * Permet de determiner le nom à donner à l'avis de publicité à envoyer à la presse.
     *
     * @param $consultationId : la référence de la consultation
     * @param $idAvis : l'identifiant de l'avis de publicité (allusion à la table avis_pub)
     *
     * @return : nom de l'avis presse
     */
    public function getNomFichierAnnoncePresse($consultationId, $idAvis)
    {
        return 'Annonce_Presse_'.$consultationId.'_'.$idAvis;
    }

    /**
     * Permet de recuperer l'avis global de publicité.
     *
     * @param $idAvisPortail : identifiant de l'avis portail
     * @param refConsultation : la référence de la consultation
     * @param $organisme : l'organisme de l'avis
     *
     * @return : objet avis global de publicité
     */
    public function getAvisByIdAvisPortailAndRefConsultation($idAvisPortail, $consultationId, $organisme, $typeAvis)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonAvisPubPeer::ORGANISME, $organisme);
        $c->add(CommonAvisPubPeer::CONSULTATION_ID, $consultationId);
        if ($typeAvis == Atexo_Config::getParameter('TYPE_FORMAT_PDF_TED')) {
            $c->add(CommonAvisPubPeer::ID_AVIS_PDF_OPOCE, $idAvisPortail);
        } elseif ($typeAvis == Atexo_Config::getParameter('TYPE_FORMAT_LIBRE_FICHIER_JOINT')) {
            $c->add(CommonAvisPubPeer::ID_AVIS_PORTAIL, $idAvisPortail);
        }
        $avisPub = CommonAvisPubPeer::doSelectOne($c, $connexion);

        return $avisPub;
    }

    /**
     * Permet de recuperer la géolocalisation N2.
     */
    public function retrieveGeolocalisationN2($idGeoN2)
    {
        $geoLocalisationN2 = (new Atexo_Geolocalisation_GeolocalisationN2())->retrieveGeolocalisationN2ById($idGeoN2);
        if ($geoLocalisationN2) {
            return $geoLocalisationN2[0]->getDenomination1Traduit();
        }
    }

    /**
     * Permet de recuperer le dernier historique pour un statut donné de l'avis.
     *
     * @param $idAvis: l'identifiant de l'avis de publicité
     * @param $statut: statut de l'avis de publicité, dont on veut chercher l'historique
     * @param $organisme: l'organisme
     *
     * @return l'objet historique correspondant au dernier historique pour le statut
     */
    public function getLastHistoriqueByStatut($idAvis, $statut, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonHistoriqueAvisPubPeer::ORGANISME, $organisme);
        $c->add(CommonHistoriqueAvisPubPeer::ID_AVIS, $idAvis);
        $c->add(CommonAvisPubPeer::STATUT, $statut);
        $c->addJoin(CommonAvisPubPeer::ID, CommonHistoriqueAvisPubPeer::ID_AVIS);
        $c->addAlias('HistoriqueAvisPub', CommonHistoriqueAvisPubPeer::TABLE_NAME);
        $historiques = CommonHistoriqueAvisPubPeer::doSelect($c, $connexion);
        if ($historiques && count($historiques)) {
            return $historiques[count($historiques) - 1];
        }

        return '';
    }

    /**
     * Permet de determiner le libellé du statut de l'avis global et des destinataires.
     *
     * @param $statutATester: le statut de l'avis
     *
     * @return le libellé du statut
     */
    public function getStatutAvisEtSupports(string $statutATester)
    {
        $statut = match ($statutATester) {
            Atexo_Config::getParameter('STATUT_AVIS_A_TRANSMETTRE') => Prado::localize('TEXT_STATUT_AVIS_A_TRANSMETTRE'),
            Atexo_Config::getParameter('STATUT_AVIS_EN_ATTENTE_VALIDATION_ECO') => Prado::localize('TEXT_ATTENTE_VALIDATION_ECO'),
            Atexo_Config::getParameter('STATUT_AVIS_REJETE_ECO') => Prado::localize('TEXT_REJETE_ENTITE_COORDINATRICE'),
            Atexo_Config::getParameter('STATUT_AVIS_EN_ATTENTE_VALIDATION_SIP') => Prado::localize('TEXT_ATTENTE_VALIDATION_SIP'),
            Atexo_Config::getParameter('STATUT_AVIS_ENVOI_PLANIFIE') => Prado::localize('TEXT_ENVOI_PLANIFIE'),
            Atexo_Config::getParameter('STATUT_AVIS_ENVOYE') => Prado::localize('TEXT_STATUT_AVIS_ENVOYE'),
            Atexo_Config::getParameter('STATUT_AVIS_REJETE_SIP') => Prado::localize('TEXT_REJETE_SIP'),
            default => Prado::localize('NON_RENSEIGNE'),
        };

        return $statut;
    }

    /**
     * @param $consultation: objet consultation
     * @param $type:le type d'avis dont il faut recuperer les infos JOUE ( = AVIS_PUBLICITE_PREINFORMATION pour la création des avis initiaux, AVIS_PUBLICITE_INITIAL pour la création des autres avis)
     *
     * @return array: tableau des informations JOUE: le premier élément est l'identifiant et le second élément est la date de publication
     */
    public function recupererInfosJoueBd($consultation, $typeAvisARecuperer)
    {
        $sql = 'SELECT FX.id_joue, FX.date_pub_joue ';
        $sql .= 'FROM `consultation` C, `Avis_Pub` AP, `Destinataire_Pub` DP, `FormXmlDestinataireOpoce` FX, `Type_Avis_Pub` TAP ';
        $sql .= 'WHERE C.`id` = AP.`consultation_id` AND C.`organisme` = AP.`organisme` ';
        $sql .= 'AND AP.`id` = DP.`id_avis` AND AP.`organisme` = DP.`organisme` ';
        $sql .= 'AND DP.`id` = FX.`id_destinataire_opoce` AND DP.`organisme` = FX.`organisme` ';

        $sql .= 'AND AP.`type_avis` = TAP.`id` ';

        if ($typeAvisARecuperer == Atexo_Config::getParameter('TAG_AVIS_PREINFORMATION')) {//Cas création avis initial
            $sql .= "AND TAP.`nature_avis` = '".Atexo_Config::getParameter('TAG_AVIS_PREINFORMATION')."'";
        } elseif ($typeAvisARecuperer == Atexo_Config::getParameter('TAG_AVIS_INITIAL')) {//Cas création autres avis
            $sql .= "AND TAP.`nature_avis` = '".Atexo_Config::getParameter('TAG_AVIS_INITIAL')."'";
        }
        $sql .= " AND C.`id` = '".(new Atexo_Db())->quote($consultation->getId())."' ";
        $sql .= " AND FX.`id_joue` IS NOT NULL AND FX.`id_joue` <> '' ";

        $statement = Atexo_Db::getLinkCommon(true)->query($sql);
        $resultat = [];
        $increment = 0;
        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            $resultat[$increment]['id_joue'] = $row['id_joue'];
            $resultat[$increment]['date_pub_joue'] = $row['date_pub_joue'];
            ++$increment;
        }
        if (is_array($resultat) && $resultat[0]) {
            return $resultat[0];
        }

        return false;
    }

    /**
     * Permet de recuperer les infos JOUE.
     *
     * @param $consultation: objet consultation
     * @param idDestinataire: identifiant du destinataire
     *
     * @return les informations JOUE
     */
    public function recupererInfosJoue($consultation, $idDestinataire, $destinataire = null)
    {
        if (!($destinataire instanceof CommonDestinatairePub)) {
            $destinataire = self::getDestinatairePub($idDestinataire, $consultation->getOrganisme());
        }
        if ($destinataire instanceof CommonDestinatairePub) {
            $avisPub = self::getAvisById($destinataire->getIdAvis(), $consultation->getOrganisme(), $consultation->getId());
            if ($avisPub instanceof CommonAvisPub) {
                $typeAvis = self::retrieveLibelleTypeAvisByIdAvis($avisPub->getTypeAvis(), true);
                $typeAvisARecuperer = '';
                if ($typeAvis instanceof CommonTypeAvisPub) {
                    if ($typeAvis->getNatureAvis() == Atexo_Config::getParameter('TAG_AVIS_INITIAL') || $typeAvisARecuperer == Atexo_Config::getParameter('TAG_AVIS_PREINFORMATION')) {
                        //Création avis initial ou pré-information
                        $typeAvisARecuperer = Atexo_Config::getParameter('TAG_AVIS_PREINFORMATION');
                    } elseif ($typeAvis->getNatureAvis() != Atexo_Config::getParameter('TAG_AVIS_INITIAL') && $typeAvis->getNatureAvis() != Atexo_Config::getParameter('TAG_AVIS_PREINFORMATION')) {
                        //Création autres avis
                        $typeAvisARecuperer = Atexo_Config::getParameter('TAG_AVIS_INITIAL');
                    }

                    return self::recupererInfosJoueBd($consultation, $typeAvisARecuperer);
                }
            }
        }

        return false;
    }

    public function getDestinataireByIdJalAndIdAnnonce($idJal, $idAnnonce, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $criteria = new Criteria();
        $criteria->add(CommonDestinataireAnnoncePressPeer::ID_JAL, $idJal);
        $criteria->add(CommonDestinataireAnnoncePressPeer::ID_ANNONCE_PRESS, $idAnnonce);
        $criteria->add(CommonDestinataireAnnoncePressPeer::ORGANISME, $organisme);
        $dstPub = CommonDestinataireAnnoncePressPeer::doSelectOne($criteria, $connexion);
        if ($dstPub instanceof CommonDestinataireAnnoncePress) {
            return $dstPub;
        }

        return false;
    }

    /**
     * Permet de recuperer les informations de la balise <ORGANISATION/> du bloc <NAME_ADDRESSES_CONTACT_CONTRACT/>.
     *
     * @param CommonTProfilJoue $profilJoue:     objet profil Joue
     * @param string            $organisation:la valeur de l'organisation si l'on n'a pas le profil JOUE
     */
    public function getOrganisationNameAdressesContact($profilJoue, $organisation)
    {
        if ($profilJoue instanceof CommonTProfilJoue) {
            $organisation = $profilJoue->getNomOfficiel();
        }

        return $organisation;
    }

    /**
     * Permet de recuperer les informations de la balise <ADDRESS/> du bloc <NAME_ADDRESSES_CONTACT_CONTRACT/>.
     *
     * @param CommonTProfilJoue $profilJoue: objet profil Joue
     * @param string            $adresse:    valeur de l'adresse si l'on n'a pas le profil JOUE
     */
    public function getAdreseNameAdressesContact($profilJoue, $adresse)
    {
        if ($profilJoue instanceof CommonTProfilJoue) {
            $adresse = $profilJoue->getAdresse();
        }

        return $adresse;
    }

    /**
     * Permet de recuperer les informations de la balise <TOWN/> du bloc <NAME_ADDRESSES_CONTACT_CONTRACT/>.
     *
     * @param CommonTProfilJoue $profilJoue: objet profil Joue
     * @param string            $adresse:    ville si l'on n'a pas le profil JOUE
     */
    public function getVilleNameAdressesContact($profilJoue, $ville)
    {
        if ($profilJoue instanceof CommonTProfilJoue) {
            $ville = $profilJoue->getVille();
        }

        return $ville;
    }

    /**
     * Permet de recuperer les informations de la balise <POSTAL_CODE/> du bloc <NAME_ADDRESSES_CONTACT_CONTRACT/>.
     *
     * @param CommonTProfilJoue $profilJoue: objet profil Joue
     * @param string            $codePostal: valeur du code postal si l'on n'a pas le profil JOUE
     */
    public function getCodePostalNameAdressesContact($profilJoue, $codePostal)
    {
        if ($profilJoue instanceof CommonTProfilJoue) {
            $codePostal = $profilJoue->getCodePostal();
        }

        return $codePostal;
    }

    /**
     * Permet de recuperer les informations de la balise <COUNTRY/> du bloc <NAME_ADDRESSES_CONTACT_CONTRACT/>.
     *
     * @param CommonTProfilJoue $profilJoue: objet profil Joue
     * @param string            $codePays:   code pays si l'on n'a pas le profil JOUE
     */
    public function getCodePaysNameAdressesContact($profilJoue, $codePays)
    {
        if ($profilJoue instanceof CommonTProfilJoue) {
            $listeCodesPays = Atexo_Util::listeCodesPays();
            if (is_array($listeCodesPays) && count($listeCodesPays)) {
                $codePays = $listeCodesPays[$profilJoue->getPays()];
            }
        }

        return $codePays;
    }

    /**
     * Permet de recuperer les informations de la balise <FAX/> du bloc <NAME_ADDRESSES_CONTACT_CONTRACT/>.
     *
     * @param CommonTProfilJoue $profilJoue: objet profil Joue
     * @param string            $fax:        fax si l'on n'a pas le profil JOUE
     */
    public function getFaxNameAdressesContact($profilJoue, $fax)
    {
        if ($profilJoue instanceof CommonTProfilJoue) {
            $fax = $profilJoue->getFax();
        }

        return $fax;
    }

    /**
     * Permet de recuperer les informations de la balise <PHONE/> du bloc <NAME_ADDRESSES_CONTACT_CONTRACT/>.
     *
     * @param CommonTProfilJoue $profilJoue: objet profil Joue
     * @param string            $phone:      numéro de téléphone si l'on n'a pas le profil JOUE
     */
    public function getTelephoneNameAdressesContact($profilJoue, $phone)
    {
        if ($profilJoue instanceof CommonTProfilJoue) {
            $phone = $profilJoue->getTelephone();
        }

        return $phone;
    }

    /**
     * Permet de recuperer les informations de la balise <E_MAIL/> du bloc <NAME_ADDRESSES_CONTACT_CONTRACT/>.
     *
     * @param CommonTProfilJoue $profilJoue: objet profil Joue
     * @param string            $email:      email si l'on n'a pas le profil JOUE
     */
    public function getEmailNameAdressesContact($profilJoue, $email)
    {
        if ($profilJoue instanceof CommonTProfilJoue) {
            $email = $profilJoue->getEmail();
        }

        return $email;
    }

    /**
     * Permet de recuperer les informations de la balise <CONTACT_POINT/> du bloc <NAME_ADDRESSES_CONTACT_CONTRACT/>.
     *
     * @param CommonTProfilJoue $profilJoue: objet profil Joue
     *
     * @return string: le point de contact
     */
    public function getPointContactNameAdressesContact($profilJoue)
    {
        $pointContact = '';
        if ($profilJoue instanceof CommonTProfilJoue) {
            $pointContact = $profilJoue->getPointContact();
        }

        return $pointContact;
    }

    /**
     * Permet de recuperer les informations de la balise <ATTENTION/> du bloc <NAME_ADDRESSES_CONTACT_CONTRACT/>.
     *
     * @param CommonTProfilJoue $profilJoue: objet profil Joue
     *
     * @return string: les informations relatives a "a l'attention de"
     */
    public function getAtentionNameAdressesContact($profilJoue)
    {
        $attentionDe = '';
        if ($profilJoue instanceof CommonTProfilJoue) {
            $attentionDe = $profilJoue->getAAttentionDe();
        }

        return $attentionDe;
    }

    /**
     * Permet de recuperer les informations de la balise <INTERNET_ADDRESSES_PRIOR_INFORMATION/> du bloc <NAME_ADDRESSES_CONTACT_CONTRACT/>.
     *
     * @param CommonTProfilJoue $profilJoue: objet profil Joue
     *
     * @return string: la balise xml correspondant aux adresses internets
     */
    public function getAdressesInternetsNameAdressesContact($profilJoue)
    {
        if ($profilJoue instanceof CommonTProfilJoue) {
            $adressesInternets = '<URL_GENERAL>'.Atexo_Util::replaceSpecialCharacters(Atexo_Util::toUtf8($profilJoue->getAdressePouvoirAdjudicateur())).'</URL_GENERAL>';
            $adressesInternets .= '<URL_BUYER>'.Atexo_Util::replaceSpecialCharacters(Atexo_Util::toUtf8($profilJoue->getAdresseProfilAcheteur())).'</URL_BUYER>';

            return $adressesInternets;
        }
    }

    /**
     * Permet de recuperer les informations du bloc: Activités principales.
     *
     * @param CommonTProfilJoue $profilJoue
     *
     * @return string: bloc xml
     */
    public function getActivitesPrincipales($profilJoue)
    {
        if ($profilJoue instanceof CommonTProfilJoue) {
            $activitePrincipale = self::getServicesGeneraux($profilJoue);
            $activitePrincipale .= self::getProtectionSociale($profilJoue);
            $activitePrincipale .= self::getEduction($profilJoue);
            $activitePrincipale .= self::getSante($profilJoue);
            $activitePrincipale .= self::getEnvironnement($profilJoue);
            $activitePrincipale .= self::getSecuritePublic($profilJoue);
            $activitePrincipale .= self::getDeveloppementCollectif($profilJoue);
            $activitePrincipale .= self::getDefense($profilJoue);
            $activitePrincipale .= self::getAffairesEconomiques($profilJoue);
            $activitePrincipale .= self::getLoisirs($profilJoue);
            $activitePrincipale .= self::getAutreActivitesPrincipales($profilJoue);

            return $activitePrincipale;
        }
    }

    /**
     * Permet de recuperer les informations de sevices generaux.
     *
     * @param CommonTProfilJoue $profilJoue
     *
     * @return string: bloc xml
     */
    public function getServicesGeneraux($profilJoue)
    {
        if ($profilJoue instanceof CommonTProfilJoue && $profilJoue->getServicesGeneraux()) {
            return '<TYPE_OF_ACTIVITY VALUE="GENERAL_PUBLIC_SERVICES"/>';
        }
    }

    /**
     * Permet de recuperer les informations de protection sociale.
     *
     * @param CommonTProfilJoue $profilJoue
     *
     * @return string: bloc xml
     */
    public function getProtectionSociale($profilJoue)
    {
        if ($profilJoue instanceof CommonTProfilJoue && $profilJoue->getProtectionSociale()) {
            return '<TYPE_OF_ACTIVITY VALUE="SOCIAL_PROTECTION"/>';
        }
    }

    /**
     * Permet de recuperer les informations de Defense.
     *
     * @param CommonTProfilJoue $profilJoue
     *
     * @return string: bloc xml
     */
    public function getDefense($profilJoue)
    {
        if ($profilJoue instanceof CommonTProfilJoue && $profilJoue->getDefense()) {
            return '<TYPE_OF_ACTIVITY VALUE="DEFENCE"/>';
        }
    }

    /**
     * Permet de recuperer les informations de Education.
     *
     * @param CommonTProfilJoue $profilJoue
     *
     * @return string: bloc xml
     */
    public function getEduction($profilJoue)
    {
        if ($profilJoue instanceof CommonTProfilJoue && $profilJoue->getEduction()) {
            return '<TYPE_OF_ACTIVITY VALUE="EDUCATION"/>';
        }
    }

    /**
     * Permet de recuperer les informations de Santé.
     *
     * @param CommonTProfilJoue $profilJoue
     *
     * @return string: bloc xml
     */
    public function getSante($profilJoue)
    {
        if ($profilJoue instanceof CommonTProfilJoue && $profilJoue->getSante()) {
            return '<TYPE_OF_ACTIVITY VALUE="HEALTH"/>';
        }
    }

    /**
     * Permet de recuperer les informations de Environnement.
     *
     * @param CommonTProfilJoue $profilJoue
     *
     * @return string: bloc xml
     */
    public function getEnvironnement($profilJoue)
    {
        if ($profilJoue instanceof CommonTProfilJoue && $profilJoue->getEnvironnement()) {
            return '<TYPE_OF_ACTIVITY VALUE="ENVIRONMENT"/>';
        }
    }

    /**
     * Permet de recuperer les informations de Affaires économiques.
     *
     * @param CommonTProfilJoue $profilJoue
     *
     * @return string: bloc xml
     */
    public function getAffairesEconomiques($profilJoue)
    {
        if ($profilJoue instanceof CommonTProfilJoue && $profilJoue->getAffairesEconomiques()) {
            return '<TYPE_OF_ACTIVITY VALUE="ECONOMIC_AND_FINANCIAL_AFFAIRS"/>';
        }
    }

    /**
     * Permet de recuperer les informations de Logement et developpement collectif.
     *
     * @param CommonTProfilJoue $profilJoue
     *
     * @return string: bloc xml
     */
    public function getDeveloppementCollectif($profilJoue)
    {
        if ($profilJoue instanceof CommonTProfilJoue && $profilJoue->getDeveloppementCollectif()) {
            return '<TYPE_OF_ACTIVITY VALUE="HOUSING_AND_COMMUNITY_AMENITIES"/>';
        }
    }

    /**
     * Permet de recuperer les informations de Loisirs.
     *
     * @param CommonTProfilJoue $profilJoue
     *
     * @return string: bloc xml
     */
    public function getLoisirs($profilJoue)
    {
        if ($profilJoue instanceof CommonTProfilJoue && $profilJoue->getLoisirs()) {
            return '<TYPE_OF_ACTIVITY VALUE="RECREATION_CULTURE_AND_RELIGION"/>';
        }
    }

    /**
     * Permet de recuperer les informations de Sécurité public.
     *
     * @param CommonTProfilJoue $profilJoue
     *
     * @return string: bloc xml
     */
    public function getSecuritePublic($profilJoue)
    {
        if ($profilJoue instanceof CommonTProfilJoue && $profilJoue->getSecuritePublic()) {
            return '<TYPE_OF_ACTIVITY VALUE="PUBLIC_ORDER_AND_SAFETY"/>';
        }
    }

    /**
     * Permet de recuperer les informations de Activités principales.
     *
     * @param CommonTProfilJoue $profilJoue
     *
     * @return string: bloc xml
     */
    public function getAutreActivitesPrincipales($profilJoue)
    {
        if ($profilJoue instanceof CommonTProfilJoue && $profilJoue->getAutreActivitesPrincipales()) {
            $libelleAutre = '';
            if ($profilJoue->getAutreLibelleActivitesPrincipales()) {
                $libelleAutre = $profilJoue->getAutreLibelleActivitesPrincipales();
            }

            return '<TYPE_OF_ACTIVITY_OTHER VALUE="OTHER">'.Atexo_Util::replaceSpecialCharacters(Atexo_Util::toUtf8($libelleAutre)).'</TYPE_OF_ACTIVITY_OTHER>';
        }
    }

    /**
     * Permet de recuperer les informations du bloc: Type pouvoir adjudicateur.
     *
     * @param CommonTProfilJoue $profilJoue
     *
     * @return string: bloc xml
     */
    public function getTypePouvoirAdjudicateur($profilJoue)
    {
        if ($profilJoue instanceof CommonTProfilJoue) {
            $typePouvoirAdjudicateur = self::getMinistereAutoriteNationale($profilJoue);
            $typePouvoirAdjudicateur .= self::getOfficeNationale($profilJoue);
            $typePouvoirAdjudicateur .= self::getOfficeRegionale($profilJoue);
            $typePouvoirAdjudicateur .= self::getOrganisationEuropeenne($profilJoue);
            $typePouvoirAdjudicateur .= self::getOrganismePublic($profilJoue);
            $typePouvoirAdjudicateur .= self::getAutreTypePouvoirAdjudicateur($profilJoue);

            return $typePouvoirAdjudicateur;
        }
    }

    /**
     * Permet de charger les informations de minitères et autorités nationales.
     *
     * @param CommonTProfilJoue $profilJoue
     *
     * @return string: bloc xml
     */
    public function getMinistereAutoriteNationale($profilJoue)
    {
        if ($profilJoue instanceof CommonTProfilJoue && '1' == $profilJoue->getAutoriteNationale()) {
            return '<TYPE_OF_CONTRACTING_AUTHORITY VALUE="MINISTRY"/>';
        }
    }

    /**
     * Permet de charger le bloc "Agence/office national(e) ou fédéral(e)".
     *
     * @param CommonTProfilJoue $profilJoue
     *
     * @return string: bloc xml
     */
    public function getOfficeNationale($profilJoue)
    {
        if ($profilJoue instanceof CommonTProfilJoue && '1' == $profilJoue->getOfficeNationale()) {
            return '<TYPE_OF_CONTRACTING_AUTHORITY VALUE="NATIONAL_AGENCY"/>';
        }
    }

    /**
     * Permet de charger le bloc "Agence/office régional(e) ou local(e)".
     *
     * @param CommonTProfilJoue $profilJoue
     *
     * @return string: bloc xml
     */
    public function getOfficeRegionale($profilJoue)
    {
        if ($profilJoue instanceof CommonTProfilJoue && '1' == $profilJoue->getOfficeRegionale()) {
            return '<TYPE_OF_CONTRACTING_AUTHORITY VALUE="REGIONAL_AGENCY"/>';
        }
    }

    /**
     * Permet de charger le bloc "Institution/agence européenne ou organisation européenne".
     *
     * @param CommonTProfilJoue $profilJoue
     *
     * @return string: bloc xml
     */
    public function getOrganisationEuropeenne($profilJoue)
    {
        if ($profilJoue instanceof CommonTProfilJoue && '1' == $profilJoue->getOrganisationEuropenne()) {
            return '<TYPE_OF_CONTRACTING_AUTHORITY VALUE="EU_INSTITUTION"/>';
        }
    }

    /**
     * Permet de charger le bloc "Organisme de droit public".
     *
     * @param CommonTProfilJoue $profilJoue
     *
     * @return string: bloc xml
     */
    public function getOrganismePublic($profilJoue)
    {
        if ($profilJoue instanceof CommonTProfilJoue && '1' == $profilJoue->getOrganismePublic()) {
            return '<TYPE_OF_CONTRACTING_AUTHORITY VALUE="BODY_PUBLIC"/>';
        }
    }

    /**
     * Permet de charger le bloc "Type de pouvoir adjudicateur - Autre, veuillez préciser".
     *
     * @param CommonTProfilJoue $profilJoue
     *
     * @return string: bloc xml
     */
    public function getAutreTypePouvoirAdjudicateur($profilJoue)
    {
        if ($profilJoue instanceof CommonTProfilJoue && '1' == $profilJoue->getAutreTypePouvoirAdjudicateur()) {
            $libellePouvoirAdj = '';
            if ($profilJoue->getAutreLibelleTypePouvoirAdjudicateur()) {
                $libellePouvoirAdj = $profilJoue->getAutreLibelleTypePouvoirAdjudicateur();
            }

            return '<TYPE_OF_CONTRACTING_AUTHORITY_OTHER VALUE="OTHER">'.Atexo_Util::replaceSpecialCharacters(Atexo_Util::toUtf8($libellePouvoirAdj)).'</TYPE_OF_CONTRACTING_AUTHORITY_OTHER>';
        }
    }

    /**
     * Permet de charger le  bloc "Le pouvoir adjudicateur agit pour le compte d'autres personnes".
     *
     * @param CommonTProfilJoue $profilJoue
     *
     * @return string: bloc xml
     */
    public function getPouvoirAdjudicateurAgit($profilJoue)
    {
        if ($profilJoue instanceof CommonTProfilJoue) {
            if ('1' == $profilJoue->getPouvoirAdjudicateurAgit()) {
                return '<PURCHASING_ON_BEHALF VALUE="YES"/>';
            } elseif ('0' == $profilJoue->getPouvoirAdjudicateurAgit()) {
                return '<PURCHASING_ON_BEHALF VALUE="NO"/>';
            }
        }
    }

    /**
     * Permet de charger le bloc "Type d'organisme acheteur".
     *
     * @param CommonTProfilJoue $profilJoue
     *
     * @return string: bloc xml
     */
    public function getTypeOrganismeAcheteur($profilJoue)
    {
        if ($profilJoue instanceof CommonTProfilJoue) {
            if ('1' == $profilJoue->getPouvoirAdjudicateurMarcheCouvert()) {
                return '<DIRECTIVE>DIRECTIVE_2004_18</DIRECTIVE>';
            }
            if ('1' == $profilJoue->getEntiteAdjudicatriceMarcheCouvert()) {
                return '<DIRECTIVE>DIRECTIVE_2004_17</DIRECTIVE>';
            }
        }
    }

    /**
     * Permet de construire l'xml du bloc des adresses complémentaires.
     *
     * @param CommonTProfilJoue $profilJoue
     *
     * @return string: bloc xml
     */
    public function getAdressesComplementairesNameAdressesContact($profilJoue)
    {
        if ($profilJoue instanceof CommonTProfilJoue) {
            $adressesComplementaires = '<FURTHER_INFORMATION><IDEM/></FURTHER_INFORMATION>';
            $adressesComplementaires .= '<SPECIFICATIONS_AND_ADDITIONAL_DOCUMENTS><IDEM/></SPECIFICATIONS_AND_ADDITIONAL_DOCUMENTS>';
            $adressesComplementaires .= '<TENDERS_REQUESTS_APPLICATIONS_MUST_BE_SENT_TO><IDEM/></TENDERS_REQUESTS_APPLICATIONS_MUST_BE_SENT_TO>';

            return $adressesComplementaires;
        }
    }

    public function getAllAvisPubNonPublie($consultation)
    {
        if ($consultation instanceof CommonConsultation) {
            $statuts = [Atexo_Config::getParameter('STATUT_AVIS_A_TRANSMETTRE'), Atexo_Config::getParameter('STATUT_AVIS_REJETE_ECO'), Atexo_Config::getParameter('STATUT_AVIS_REJETE_SIP')];
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
            $c = new Criteria();
            $c->add(CommonAvisPubPeer::ORGANISME, $consultation->getOrganisme());
            $c->add(CommonAvisPubPeer::CONSULTATION_ID, $consultation->getId());
            $c->add(CommonAvisPubPeer::STATUT, $statuts, Criteria::IN);

            return CommonAvisPubPeer::doSelect($c, $connexion);
        }
    }

    public function getAllDestinatairesPub($idAvis, $organisme, $idSupport)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonDestinatairePubPeer::ORGANISME, $organisme);
        $c->add(CommonDestinatairePubPeer::ID_AVIS, $idAvis);
        $c->add(CommonDestinatairePubPeer::ID_SUPPORT, $idSupport);
        $tlisteDestinataire = CommonDestinatairePubPeer::doSelect($c, $connexion);

        return $tlisteDestinataire;
    }

    public function isTypeAvisEuropean($idTypeProcedure, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $typeAvisPubQuery = new CommonTypeAvisPubQuery();
        $typeAvisPub = $typeAvisPubQuery->getTypeAvisPubByTypePub($idTypeProcedure, $organisme, [Atexo_Config::getParameter('TYPE_PUB_AVIS_SUB'), Atexo_Config::getParameter('TYPE_PUB_AVIS_TED')], $connexion);
        if ($typeAvisPub) {
            return true;
        }

        return false;
    }
}
