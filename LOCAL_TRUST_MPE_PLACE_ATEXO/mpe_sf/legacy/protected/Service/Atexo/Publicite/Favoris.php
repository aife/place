<?php

namespace Application\Service\Atexo\Publicite;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonTConsultationComptePub;
use Application\Propel\Mpe\CommonTPubliciteFavoris;
use Application\Propel\Mpe\CommonTPubliciteFavorisPeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;

/**
 * Created by PhpStorm.
 * User: kbe
 * Date: 27/03/2017
 * Time: 12:11.
 */
class Atexo_Publicite_Favoris
{
    public function retreivePublicieFavorisByService($idService, $organisme)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonTPubliciteFavorisPeer::ORGANISME, $organisme);
        $c->add(CommonTPubliciteFavorisPeer::SERVICE_ID, $idService);

        return CommonTPubliciteFavorisPeer::doSelect($c, $connexionCom);
    }

    public function initConsultationComptePubParFavorisPublicite(&$consComptePub, $favoris)
    {
        if ($consComptePub instanceof CommonTConsultationComptePub && $favoris instanceof CommonTPubliciteFavoris) {
            //Bloc "Adresse de l'organisme qui passe le marche"
            $consComptePub->setPrm($favoris->getAcheteurCorrespondant());
            $consComptePub->setDenomination($favoris->getAcheteurNomOrganisme());
            $consComptePub->setAdresse($favoris->getAcheteurAdresse());
            $consComptePub->setCp($favoris->getAcheteurCp());
            $consComptePub->setVille($favoris->getAcheteurVille());
            $consComptePub->setEmail($favoris->getAcheteurEmail());
            $consComptePub->setTelephone($favoris->getAcheteurTelephone());
            $consComptePub->setUrl($favoris->getAcheteurUrl());

            //Bloc "Adresse de facturation de la publicité"
            $consComptePub->setFactureDenomination($favoris->getFactureDenomination());
            $consComptePub->setFactureAdresse($favoris->getFactureAdresse());
            $consComptePub->setFactureCp($favoris->getFactureCp());
            $consComptePub->setFactureVille($favoris->getFactureVille());

            //Bloc "Adresse de l'instance chargée des procédures de recours"
            $consComptePub->setInstanceRecoursOrganisme($favoris->getInstanceRecoursOrganisme());
            $consComptePub->setInstanceRecoursAdresse($favoris->getInstanceRecoursAdresse());
            $consComptePub->setInstanceRecoursCp($favoris->getInstanceRecoursCp());
            $consComptePub->setInstanceRecoursVille($favoris->getInstanceRecoursVille());
            $consComptePub->setInstanceRecoursUrl($favoris->getInstanceRecoursUrl());
        }
    }

    public function retreivePublicieFavorisById($id, $connexion)
    {
        return CommonTPubliciteFavorisPeer::retrieveByPK($id, $connexion);
    }

    /**
     * Recherche des favoris.
     */
    public static function search($idService = null, $org = null)
    {
        $c = new Criteria();
        $favories = [];
        if (!$org) {
            $org = Atexo_CurrentUser::getCurrentOrganism();
        }
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonTPubliciteFavorisPeer::SERVICE_ID, $idService, Criteria::EQUAL);
        $c->add(CommonTPubliciteFavorisPeer::ORGANISME, $org, Criteria::EQUAL);
        $c->addAscendingOrderByColumn(CommonTPubliciteFavorisPeer::LIBELLE);
        $favories = CommonTPubliciteFavorisPeer::doSelect($c, $connexionCom);

        return $favories;
    }

    /**
     * Supprime un jal.
     *
     * @param int $idFavori l'id du favori
     */
    public static function delete($idFavori)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));

        $favori = CommonTPubliciteFavorisPeer::retrieveByPk($idFavori, $connexionCom);
        CommonTPubliciteFavorisPeer::doDelete($favori, $connexionCom);
    }
}
