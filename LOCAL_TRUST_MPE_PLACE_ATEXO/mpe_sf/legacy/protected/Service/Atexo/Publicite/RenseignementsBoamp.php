<?php

namespace Application\Service\Atexo\Publicite;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonRenseignementsBoampPeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;

/**
 * commentaires.
 *
 * @author Mouslim Mitali <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Publicite_RenseignementsBoamp
{
    public function retrieveRenseignementsBoampByIdType($idCompte)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonRenseignementsBoampPeer::ID_COMPTE, $idCompte);
        $c->add(CommonRenseignementsBoampPeer::ACRONYMEORG, Atexo_CurrentUser::getCurrentOrganism());
        $c->addAscendingOrderByColumn(CommonRenseignementsBoampPeer::ID_TYPE);
        $result = CommonRenseignementsBoampPeer::doSelect($c, $connexionCom);
        if ($result) {
            return $result;
        } else {
            return [];
        }
    }

    public function deleteRenseignementsBoampByIdCompte($idCompte)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $results = self::retrieveRenseignementsBoampByIdType($idCompte);
        if ($results && (is_countable($results) ? count($results) : 0)) {
            foreach ($results as $oneRes) {
                $c = new Criteria();
                $c->add(CommonRenseignementsBoampPeer::ID, $oneRes->getId());
                $c->add(CommonRenseignementsBoampPeer::ACRONYMEORG, Atexo_CurrentUser::getCurrentOrganism());
                CommonRenseignementsBoampPeer::doDelete($c, $connexionCom);
            }
        }
    }

    public function retrieveOneRenseignementBoamp($idCompte, $idType)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonRenseignementsBoampPeer::ID_COMPTE, $idCompte);
        $c->add(CommonRenseignementsBoampPeer::ID_TYPE, $idType);
        $c->add(CommonRenseignementsBoampPeer::ACRONYMEORG, Atexo_CurrentUser::getCurrentOrganism());
        $result = CommonRenseignementsBoampPeer::doSelectOne($c, $connexionCom);
        if ($result) {
            return $result;
        } else {
            return null;
        }
    }
}
