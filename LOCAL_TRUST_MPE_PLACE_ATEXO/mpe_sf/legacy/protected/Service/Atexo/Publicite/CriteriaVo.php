<?php

namespace Application\Service\Atexo\Publicite;

/**
 * @author KONATE Oumar<oumar.konate@atexo.com>
 * @copyright Atexo 2011
 *
 * @version 1.0
 *
 * @since MPE-4.0
 * @TODO commentaires à compléter
 */
class Atexo_Publicite_CriteriaVo
{
    private $_referenceAvis;
    private $_idService;
    private $_idTypeAvis;
    private $_idSupport;
    private $_idTypeProcedure;
    private $_statut;
    private $_dateReceptionStart;
    private $_dateReceptionEnd;
    private $_dateRemisePlisStart;
    private $_dateRemisePlisEnd;
    private $_limit;
    private $_offSet;
    private $_organisme;
    private $_regionAvis;
    private $_commandeName;
    private $_sensTri;
    private $_idAgentValidateurAvisPub;
    private $_validationSip;
    private $_critereNomPrenomAgentValidateur;

    public function getReferenceAvis()
    {
        return $this->_referenceAvis;
    }

    public function setReferenceAvis($refAvis)
    {
        $this->_referenceAvis = $refAvis;
    }

    public function getIdService()
    {
        return $this->_idService;
    }

    public function setIdService($idService)
    {
        $this->_idService = $idService;
    }

    public function getIdTypeAvis()
    {
        return $this->_idTypeAvis;
    }

    public function setIdTypeAvis($idTypeAvis)
    {
        $this->_idTypeAvis = $idTypeAvis;
    }

    public function getIdSupport()
    {
        return $this->_idSupport;
    }

    public function setIdSupport($idSupport)
    {
        $this->_idSupport = $idSupport;
    }

    public function getIdTypeProcedure()
    {
        return $this->_idTypeProcedure;
    }

    public function setIdTypeProcedure($idTypeProc)
    {
        $this->_idTypeProcedure = $idTypeProc;
    }

    public function getStatut()
    {
        return $this->_statut;
    }

    public function setStatut($statut)
    {
        $this->_statut = $statut;
    }

    public function getDateReceptionStart()
    {
        return $this->_dateReceptionStart;
    }

    public function setDateReceptionStart($dateReceptionStart)
    {
        $this->_dateReceptionStart = $dateReceptionStart;
    }

    public function getDateReceptionEnd()
    {
        return $this->_dateReceptionEnd;
    }

    public function setDateReceptionEnd($dateReceptionEnd)
    {
        $this->_dateReceptionEnd = $dateReceptionEnd;
    }

    public function getDateRemisePlisStart()
    {
        return $this->_dateRemisePlisStart;
    }

    public function setDateRemisePlisStart($dateRemisePlisStart)
    {
        $this->_dateRemisePlisStart = $dateRemisePlisStart;
    }

    public function getDateRemisePlisEnd()
    {
        return $this->_dateRemisePlisEnd;
    }

    public function setDateRemisePlisEnd($dateRemisePlisEnd)
    {
        $this->_dateRemisePlisEnd = $dateRemisePlisEnd;
    }

    public function getLimit()
    {
        return $this->_limit;
    }

    public function setLimit($limit)
    {
        $this->_limit = $limit;
    }

    public function getOffset()
    {
        return $this->_offSet;
    }

    public function setOffset($offSet)
    {
        $this->_offSet = $offSet;
    }

    public function getOrganisme()
    {
        return $this->_organisme;
    }

    public function setOrganisme($organisme)
    {
        $this->_organisme = $organisme;
    }

    public function getRegionAvis()
    {
        return $this->_regionAvis;
    }

    public function setRegionAvis($region)
    {
        $this->_regionAvis = $region;
    }

    public function getCommandeName()
    {
        return $this->_commandeName;
    }

    public function setCommandeName($commandeName)
    {
        $this->_commandeName = $commandeName;
    }

    public function getSensTri()
    {
        return $this->_sensTri;
    }

    public function setSensTri($sensTri)
    {
        $this->_sensTri = $sensTri;
    }

    public function getIdAgentValidateurAvisPub()
    {
        return $this->_idAgentValidateurAvisPub;
    }

    public function setIdAgentValidateurAvisPub($value)
    {
        $this->_idAgentValidateurAvisPub = $value;
    }

    public function getValidationSip()
    {
        return $this->_validationSip;
    }

    public function setValidationSip($value)
    {
        $this->_validationSip = $value;
    }

    public function getCritereNomPrenomAgentValidateur()
    {
        return $this->_critereNomPrenomAgentValidateur;
    }

    public function setCritereNomPrenomAgentValidateur($value)
    {
        $this->_critereNomPrenomAgentValidateur = $value;
    }
}
