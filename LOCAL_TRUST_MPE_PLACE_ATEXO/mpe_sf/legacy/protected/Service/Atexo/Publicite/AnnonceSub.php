<?php

namespace Application\Service\Atexo\Publicite;

use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Pages\Commun\DownloadFile;
use Application\Propel\Mpe\CommonAcheteurPublic;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationPeer;
use Application\Propel\Mpe\CommonDestinatairePub;
use Application\Propel\Mpe\CommonOrganisme;
use Application\Propel\Mpe\CommonTAnnonceConsultation;
use Application\Propel\Mpe\CommonTAnnonceConsultationPeer;
use Application\Propel\Mpe\CommonTAnnonceConsultationQuery;
use Application\Propel\Mpe\CommonTConsultationComptePubQuery;
use Application\Propel\Mpe\CommonTDonneeComplementaire;
use Application\Propel\Mpe\CommonTHistoriqueAnnonce;
use Application\Propel\Mpe\CommonTHistoriqueAnnonceQuery;
use Application\Propel\Mpe\CommonTOffreSupportPublicite;
use Application\Propel\Mpe\CommonTOffreSupportPubliciteQuery;
use Application\Propel\Mpe\CommonTParametrageFormulairePublicite;
use Application\Propel\Mpe\CommonTParametrageFormulairePubliciteQuery;
use Application\Propel\Mpe\CommonTPreferenceSupportPublicationQuery;
use Application\Propel\Mpe\CommonTSupportAnnonceConsultation;
use Application\Propel\Mpe\CommonTSupportAnnonceConsultationPeer;
use Application\Propel\Mpe\CommonTSupportAnnonceConsultationQuery;
use Application\Propel\Mpe\CommonTSupportPublication;
use Application\Propel\Mpe\CommonTSupportPublicationPeer;
use Application\Propel\Mpe\CommonTSupportPublicationQuery;
use Application\Propel\Mpe\CommonTTypeAnnonceConsultationQuery;
use Application\Service\Atexo\Atexo_Concentrateur;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Controller\Atexo_Controller_Front;
use Application\Service\Atexo\FormulaireSub\Atexo_FormulaireSub_AnnonceEchange;
use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2;
use AtexoCrypto\Dto\Consultation;
use Exception;
use PDO;
use Prado\Prado;

/**
 * permet de gérer la recherche des annonces.
 *
 * @author AME <amal.elbekkaoui@atexo.com>
 *
 * @version 1.0
 *
 * @since roadmap-2015
 *
 * @copyright Atexo 2015
 */
class Atexo_Publicite_AnnonceSub
{
    /**
     * permet de chercher les annonces encours.
     *
     * @param int             $ref       ref consultation
     * @param string          $org       organisme consultation
     * @param PropelConnexion $connexion connexion BD
     *
     * @return array liste des annonces encours
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2015
     */
    public static function getAnnoncesEncours($consultationId, $org, $connexion = null)
    {
        if (!$connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
        $annonceQuery = new CommonTAnnonceConsultationQuery();
        $annonceQuery->filterByConsultationId($consultationId);
        $annonceQuery->filterByOrganisme($org);
        $annonceQuery->filterByStatut(Atexo_Config::getParameter('ANNONCE_STATUT_ENVOYE'), Criteria::NOT_LIKE);

        return (array) $annonceQuery->find($connexion);
    }

    /**
     * permet de chercher les annonces envoyes.
     *
     * @param int             $ref       ref consultation
     * @param string          $org       organisme consultation
     * @param PropelConnexion $connexion connexion BD
     *
     * @return array liste des annonces envoyes
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2015
     */
    public static function getAnnoncesEnvoyer($consultationId, $org, $connexion = null)
    {
        if (!$connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
        $annonceQuery = new CommonTAnnonceConsultationQuery();
        $annonceQuery->filterByConsultationId($consultationId);
        $annonceQuery->filterByOrganisme($org);
        $annonceQuery->filterByStatut(Atexo_Config::getParameter('ANNONCE_STATUT_ENVOYE'));

        return (array) $annonceQuery->find($connexion);
    }

    /**
     * permet de chercher l'annonce simplifie.
     *
     * @param int             $ref       ref consultation
     * @param string          $org       organisme consultation
     * @param PropelConnexion $connexion connexion BD
     *
     * @return CommonTAnnonceConsultation l'annonce simplifie
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public static function getAnnonceSimplifie($consultationId, $org, $connexion = null)
    {
        if (!$connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
        $annonceQuery = new CommonTAnnonceConsultationQuery();
        $annonceQuery->filterByConsultationId($consultationId);
        $annonceQuery->filterByOrganisme($org);
        $annonceQuery->filterByPubliciteSimplifie(1);

        return $annonceQuery->findOne($connexion);
    }

    /**
     * permet de chercher l'annonce simplifie.
     *
     * @param int             $ref       ref consultation
     * @param string          $org       organisme consultation
     * @param PropelConnexion $connexion connexion BD
     *
     * @return CommonTAnnonceConsultation l'annonce simplifie
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public static function getAnnonceSimplifieEncours($consultationId, $org, $connexion = null)
    {
        if (!$connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
        $annonceQuery = new CommonTAnnonceConsultationQuery();
        $annonceQuery->filterByConsultationId($consultationId);
        $annonceQuery->filterByOrganisme($org);
        $annonceQuery->filterByPubliciteSimplifie(1);
        $annonceQuery->filterByStatut(Atexo_Config::getParameter('ANNONCE_STATUT_ENVOYE'), Criteria::NOT_LIKE);

        return $annonceQuery->findOne($connexion);
    }

    /**
     * permet de chercher une annonce par sont id.
     *
     * @param int             $id        identifiant annonce
     * @param int             $ref       ref consultation
     * @param string          $org       organisme consultation
     * @param PropelConnexion $connexion connexion BD
     *
     * @return CommonTAnnonceConsultation annonce recherche
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2015
     */
    public static function getAnnonceById($id, $consultationId = null, $org = null, $connexion = null)
    {
        if (!$connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
        $annonceQuery = new CommonTAnnonceConsultationQuery();
        $annonceQuery->filterById($id);
        if ($consultationId) {
            $annonceQuery->filterByConsultationId($consultationId);
        }
        if ($org) {
            $annonceQuery->filterByOrganisme($org);
        }

        return $annonceQuery->findOne($connexion);
    }

    /**
     * permet d'ajouter un support a l'annonce.
     *
     * @param int             $idAnnonce      identifiant annonce
     * @param int             $idSupport      identifiant support
     * @param int             $idAgent        id agent createur
     * @param string          $prenomNomAgent prenom nom agent createur
     * @param PropelConnexion $connexion      connexion BD
     *
     * @return void
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2015
     */
    public static function ajouterSupport($idAnnonce, $idSupport, $idAgent, $prenomNomAgent, $statut, $date, $connexion)
    {
        try {
            $support = new CommonTSupportAnnonceConsultation();
            $support->setIdAnnonceCons($idAnnonce);
            $support->setIdSupport($idSupport);
            $support->setIdAgent($idAgent);
            $support->setPrenomNomAgentCreateur($prenomNomAgent);
            $support->setDateCreation(date('Y-m-d H:i:s'));
            $support->getStatut();
            $support->getDateStatut($date);
            $support->save($connexion);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * permet de chercher un support a l'annonce.
     *
     * @param int             $idAnnonce identifiant annonce
     * @param int             $idSupport identifiant support annonce consultation
     * @param PropelConnexion $connexion connexion BD
     *
     * @return CommonTSupportAnnonceConsultation support cherche
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2015
     */
    public static function getSupportAnnonce($idAnnonce, $idSupport, $connexion)
    {
        return CommonTSupportAnnonceConsultationQuery::create()->filterById($idSupport)->filterByIdAnnonceCons($idAnnonce)->findOne($connexion);
    }

    /**
     * permet de chercher les supports par leur ids.
     *
     * @param array           $ids       identifiants supports
     * @param PropelConnexion $connexion connexion BD
     *
     * @return array CommonTSupportAnnonceConsultation support cherche
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public static function getSupportsAnnonceByIds($ids, $connexion = null)
    {
        if (!$connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        }

        return CommonTSupportAnnonceConsultationQuery::create()->filterById($ids, Criteria::IN)->find($connexion);
    }

    /**
     * permet de retouner le nombre des supports à l'annonce.
     *
     * @param int             $idAnnonce identifiant annonce
     * @param PropelConnexion $connexion connexion BD
     *
     * @return int nombre supports
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2015
     */
    public static function getCountSupportAnnonce($idAnnonce, $connexion)
    {
        return CommonTSupportAnnonceConsultationQuery::create()->filterByIdAnnonceCons($idAnnonce)->count($connexion);
    }

    /**
     * permet de chercher un support a l'annonce.
     *
     * @param int             $idAnnonce identifiant annonce
     * @param int             $idSupport identifiant support pub
     * @param PropelConnexion $connexion connexion BD
     *
     * @return CommonTSupportAnnonceConsultation support cherche
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2015
     */
    public static function getSupportAnnonceBySupportPub($idAnnonce, $idSupport, $connexion)
    {
        return CommonTSupportAnnonceConsultationQuery::create()->filterByIdSupport($idSupport)->filterByIdAnnonceCons($idAnnonce)->findOne($connexion);
    }

    /**
     * permet de chercher un support a l'annonce.
     *
     * @param int             $idAnnonce identifiant annonce
     * @param int             $idSupport identifiant support pub
     * @param PropelConnexion $connexion connexion BD
     *
     * @return CommonTSupportAnnonceConsultation support cherche
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2015
     */
    public static function getSupportAnnonceByRefConsAndSupportPub($consultationId, $idSupport, $statut, $connexion)
    {
        $sacQuery = new CommonTSupportAnnonceConsultationQuery();
        $sacQuery->filterByIdSupport($idSupport);
        $sacQuery->filterByStatut($statut);
        $sacQuery->addJoin(CommonTSupportAnnonceConsultationPeer::ID_ANNONCE_CONS, CommonTAnnonceConsultationPeer::ID, Criteria::JOIN);
        $sacQuery->add(CommonTAnnonceConsultationPeer::CONSULTATION_ID, $consultationId);

        return (array) $sacQuery->find($connexion);
    }

    /**
     * Permet de recuperer les types d'annonces de consultation.
     *
     * @param PropelConnexion $connexion : connexion
     *
     * @return array|null : liste des types d'annonces
     *
     * @author OKO <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public static function getTypesAnnonces($connexion)
    {
        try {
            if (empty($connexion)) {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            }
            $typeAnnoncesQuery = new CommonTTypeAnnonceConsultationQuery();
            $typeAnnonces = $typeAnnoncesQuery->getTypesAnnonces($connexion);
            if (!empty($typeAnnonces) && $typeAnnonces instanceof PropelObjectCollection) {
                return $typeAnnonces->getData();
            }

            return null;
        } catch (\Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('app');
            $logger->error("Erreur lors de la recuperation des types d'annonces: ".$e->getMessage()." \n Connexion = ".print_r($connexion, true)."\n Methode : Atexo_Publicite_AnnonceSub::getTypesAnnonces");
        }
    }

    /**
     * permet de mettre à jour tous les statut/date supports annonce.
     *
     * @param int             $idAnnonce  identifiant annonce
     * @param string          $statut     statut
     * @param Date            $dateStatut date statut
     * @param PropelConnexion $connexion  connexion BD
     *
     * @return bool support mise à jour
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2015
     */
    public static function updateAllStatutSupport($idAnnonce, $statut, $dateStatut, $connexion)
    {
        try {
            $listeSupportAnnonce = (array) CommonTSupportAnnonceConsultationQuery::create()->findByIdAnnonceCons($idAnnonce);
            $return = false;
            if (is_array($listeSupportAnnonce) && count($listeSupportAnnonce)) {
                foreach ($listeSupportAnnonce as $supportAnnonce) {
                    if ($supportAnnonce instanceof CommonTSupportAnnonceConsultation) {
                        $supportAnnonce->setDateStatut($dateStatut);
                        $supportAnnonce->setStatut($statut);
                        $supportAnnonce->save($connexion);
                        $return = true;
                    }
                }
            }

            return $return;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Permet de logger les infos de suivi des annonces.
     *
     * @param string $message : message a logger
     *
     * @return void
     *
     * @author OKO <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function loggerInfosSuiviAnnonces($message)
    {
        $logger = Atexo_LoggerManager::getLogger('publicite');
        $logger->info($message);
    }

    /**
     * Permet de logger les erreurs de suivi des annonces.
     *
     * @param string $message : message a logger
     *
     * @return void
     *
     * @author OKO <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function loggerErreursSuiviAnnonces($message)
    {
        $logger = Atexo_LoggerManager::getLogger('publicite');
        $logger->error($message);
    }

    /**
     * Permet de recuperer les annonces de publicite pour le suivi concentrateur
     * Les status concernes pour les annonces sont: annonces envoyees atexo, annonces non rejetees concentrateur, annonces non publiees concentrateur.
     *
     * @param PropelConnexion $connexion : connexion
     *
     * @return array : tableau des annonces
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function recupererAnnoncesSuiviPubliciteConcentrateur($connexion = null)
    {
        try {
            if (empty($connexion)) {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            }
            $annoncesConsultationQuery = new CommonTAnnonceConsultationQuery();
            $annonces = $annoncesConsultationQuery->recupererAnnoncesSuiviPubliciteConcentrateur($connexion);
            if (!empty($annonces) && $annonces instanceof PropelObjectCollection) {
                $dataAnnonces = $annonces->getData();
                (new Atexo_Publicite_AnnonceSub())->loggerInfosSuiviAnnonces('Annonces recuperees : '.print_r($dataAnnonces, true));

                return $dataAnnonces;
            } else {
                (new Atexo_Publicite_AnnonceSub())->loggerInfosSuiviAnnonces('Annonces non trouvees : '.print_r($annonces, true));
            }
        } catch (\Exception $e) {
            (new Atexo_Publicite_AnnonceSub())->loggerErreursSuiviAnnonces('Erreur recuperation des annonces pour suivi concentrateur :Erreur = '.$e->getMessage().' , Connexion = '.print_r($connexion, true)."\nMethode : Atexo_Publicite_AnnonceSub::recupererAnnoncesSuiviPubliciteConcentrateur");
        }
    }

    /**
     * Recupere les donnees de mise a jour des annonces depuis le concentrateur
     * Traite les donnees recues pour faciliter leurs utilisations.
     *
     * @param array $arrayIdAnnonces : liste des identifiants sub des annonces
     *
     * @return array
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function recupererDonneesMajConcentrateur($arrayIdAnnonces)
    {
        try {
            $listeIdAnnonces = implode(',', $arrayIdAnnonces);
            (new Atexo_Publicite_AnnonceSub())->loggerInfosSuiviAnnonces('Debut appel ws pour interroger le concentrateur');
            $dataMajAnnonces = (new Atexo_FormulaireSub_AnnonceEchange())->recupererDonneesMajConcentrateur($listeIdAnnonces);
            (new Atexo_Publicite_AnnonceSub())->loggerInfosSuiviAnnonces('Fin appel ws pour interroger le concentrateur');
            if (false !== $dataMajAnnonces) {
                return self::traiterDonneesReponseWsConcentrateur($dataMajAnnonces);
            }
        } catch (\Exception $e) {
            (new Atexo_Publicite_AnnonceSub())->loggerErreursSuiviAnnonces('Erreur survenue lors de la recuperation des donnees de mise a jour des annonces : Liste des Ids = '.print_r($arrayIdAnnonces, true).' , Erreur = '.$e->getMessage()." \nMethode : Atexo_Publicite_AnnonceSub::recupererDonneesMajConcentrateur");
        }
    }

    /**
     * Traite les donnees de retour du ws de mise a jour des annonces depuis le concentrateur
     * Fait la correspondance entre les donnees du concentrateur et celles de mpe.
     *
     * @param string $dataMajAnnoncesFormatJson : les donnees au format json
     *
     * @return array : donnees traitees et pretes pour etre mises a jour dans MPE
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function traiterDonneesReponseWsConcentrateur($dataMajAnnoncesFormatJson)
    {
        try {
            (new Atexo_Publicite_AnnonceSub())->loggerInfosSuiviAnnonces('Debut traitement des donnees retour du ws');
            if (!empty($dataMajAnnoncesFormatJson)) {
                (new Atexo_Publicite_AnnonceSub())->loggerInfosSuiviAnnonces("Donnees format json : $dataMajAnnoncesFormatJson");

                return self::preparerDonneesMiseAJourMpe(json_decode($dataMajAnnoncesFormatJson, null, 512, JSON_THROW_ON_ERROR));
            } else {
                (new Atexo_Publicite_AnnonceSub())->loggerInfosSuiviAnnonces('Aucune donnee a traiter : '.print_r($dataMajAnnoncesFormatJson, true));
            }
            (new Atexo_Publicite_AnnonceSub())->loggerInfosSuiviAnnonces('Fin traitement des donnees retour du ws');
        } catch (\Exception $e) {
            (new Atexo_Publicite_AnnonceSub())->loggerErreursSuiviAnnonces('Erreur survenue lors du traitement des donnees recues depuis le concentrateur : Donnees = '.print_r($dataMajAnnoncesFormatJson, true).' , Erreur = '.$e->getMessage()." \nMethode : Atexo_Publicite_AnnonceSub::traiterDonneesReponseWsConcentrateur");
        }
    }

    /**
     * Permet de preparer les donnees pour mettre a jour les annonces sur MPE.
     *
     * @param array $dataMajAnnonces : donnees de mise a jour recuperees depuis le concentrateur
     *
     * @return array : donnees de mise a jour sur mpe
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function preparerDonneesMiseAJourMpe($dataMajAnnonces)
    {
        (new Atexo_Publicite_AnnonceSub())->loggerInfosSuiviAnnonces('Debut preparation des donnees pour maj MPE');
        try {
            $dataMaj = [];
            if (!empty($dataMajAnnonces)) {
                foreach ($dataMajAnnonces as $data) {
                    if (!empty($data) && is_object($data)) {
                        $dataMaj[$data->idAnnonce][$data->supportPublication]['numeroAvis'] = $data->numeroAvis;
                        $dataMaj[$data->idAnnonce][$data->supportPublication]['idDossierSub'] = $data->idAnnonce;
                        $dataMaj[$data->idAnnonce][$data->supportPublication]['supportPublication'] = $data->supportPublication;
                        $dataMaj[$data->idAnnonce][$data->supportPublication]['statut'] = self::correspondanceStatutsConcentrateurMpe($data->statut);
                        $dataMaj[$data->idAnnonce][$data->supportPublication]['messageStatut'] = (new Atexo_Config())->toPfEncoding($data->messageStatut);
                        $dataMaj[$data->idAnnonce][$data->supportPublication]['dateTraitement'] = $data->dateTraitement;
                        $dataMaj[$data->idAnnonce][$data->supportPublication]['numeroJoue'] = $data->numeroJoue;
                        $dataMaj[$data->idAnnonce][$data->supportPublication]['lienPublication'] = $data->lienPublication;
                        $dataMaj[$data->idAnnonce][$data->supportPublication]['noDocExt'] = self::getNoDocExtFromXmlSub($data->xmlTransform);
                        $dataMaj[$data->idAnnonce][$data->supportPublication]['xml'] = base64_encode($data->xmlTransform);
                    }
                }
            }

            return $dataMaj;
        } catch (\Exception $e) {
            (new Atexo_Publicite_AnnonceSub())->loggerErreursSuiviAnnonces('Erreur survenue lors de la preparation des donnees pour maj mpe : Donnees = '.print_r($dataMajAnnonces, true).' , Erreur = '.$e->getMessage()." \nMethode : Atexo_Publicite_AnnonceSub::preparerDonneesMiseAJourMpe");
        }
        (new Atexo_Publicite_AnnonceSub())->loggerInfosSuiviAnnonces('Fin preparation des donnees pour maj MPE');
    }

    /**
     * @param $xml
     *
     * @return string
     */
    public function getNoDocExtFromXmlSub($xml)
    {
        $noDocExt = '';
        try {
            $domDoc = new \DomDocument();
            $domDoc->loadXML($xml);
            $tedEsenders = $domDoc->getElementsByTagName('TED_ESENDERS');
            if ($tedEsenders instanceof \DOMNodeList) {
                foreach ($tedEsenders as $tedEsender) {
                    if ($tedEsender) {
                        $senders = $tedEsender->getElementsByTagName('SENDER');
                        if ($senders instanceof \DOMNodeList) {
                            foreach ($senders as $sender) {
                                if ($sender) {
                                    $identifications = $sender->getElementsByTagName('IDENTIFICATION');
                                    if ($identifications instanceof \DOMNodeList) {
                                        foreach ($identifications as $identification) {
                                            if ($identification) {
                                                $noDocExts = $identification->getElementsByTagName('NO_DOC_EXT');
                                                if ($noDocExts instanceof \DOMNodeList) {
                                                    foreach ($noDocExts as $nde) {
                                                        if ($nde) {
                                                            $noDocExt = $nde->nodeValue;
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (\Exception $e) {
            (new Atexo_Publicite_AnnonceSub())->loggerErreursSuiviAnnonces(
                'Erreur survenue lors de la lecture du xml sub pr récuperer le N§O_DOC_EXT, Erreur = '.$e->getMessage().$e->getTraceAsString()
            );
        }

        return $noDocExt;

    }

    /**
     * Permet de faire la correspondance entre les statuts CONCENTRATEUR et MPE  des annonces de publicite.
     *
     * @param string $statut : statut Concentrateur de l'annonce
     *
     * @return string|null : equivalent MPE du statut concentrateur
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function correspondanceStatutsConcentrateurMpe($statutConcentrateur)
    {
        try {
            $statutMpe = (new Atexo_Concentrateur())->getCorrespondantStatutAnnonce($statutConcentrateur);
            (new Atexo_Publicite_AnnonceSub())->loggerInfosSuiviAnnonces("Equivalence de statut annonce: [ CONCENTRATEUR : $statutConcentrateur => MPE : $statutMpe ]");

            return $statutMpe;
        } catch (\Exception $e) {
            (new Atexo_Publicite_AnnonceSub())->loggerErreursSuiviAnnonces("Erreur survenue lors de la correspondance des statuts MPE et CONCENTRATEUR : Statut Concentrateur = $statutConcentrateur , Erreur = ".$e->getMessage()." \nMethode : Atexo_Publicite_AnnonceSub::correspondanceStatutsConcentrateurMpe");
        }
    }

    /**
     * permet d'enregistrer l'hitorique d'une annonce.
     *
     * @param int      $idAnnonce id annonce
     * @param string   $statut    statut annonce
     * @param dateTime $date      date historique
     * @param string   $motif     motif
     * @param int      $idSupport id support
     *
     * @return void
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2015
     */
    public static function saveHistorique($idAnnonce, $statut, $date, $idSupport = 0, $connexion = null, $motif = '')
    {
        try {
            if (!$connexion) {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
            }
            $historique = new CommonTHistoriqueAnnonce();
            $historique->setIdAnnonce($idAnnonce);
            $historique->setStatut($statut);
            $historique->setDateModification($date);
            $historique->setMotif($motif);
            $historique->setIdSupport($idSupport);
            $historique->save($connexion);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * permet d'ajouter l'hitorique d'une annonce avant le statut envoyé.
     *
     * @param CommonTAnnonceConsultation $annonce objet annonce
     *
     * @return void
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2015
     */
    public static function ajouterHistorique($annonce, $connexion)
    {
        try {
            if ($annonce instanceof CommonTAnnonceConsultation) {
                self::saveHistorique($annonce->getId(), $annonce->getStatut(), $annonce->getDateStatut());
                $supportAnnonces = (array) $annonce->getCommonTSupportAnnonceConsultations();
                if (is_array($supportAnnonces) && count($supportAnnonces)) {
                    foreach ($supportAnnonces as $support) {
                        if ($support instanceof CommonTSupportAnnonceConsultation) {
                            self::saveHistorique($annonce->getId(), $annonce->getStatut(), $annonce->getDateStatut(), $support->getId(), $connexion);
                        }
                    }
                }
            }
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * permet retourner l'hitorique d'une annonce.
     *
     * @param int $idAnnonce id annonce
     * @param int $idSupport id support
     *
     * @return array historique annonce
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2015
     */
    public static function getHistoriquesAnnonce($idAnnonce, $idSupport = 0, $sensOrderByDate = Criteria::DESC)
    {
        try {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            $historiqueQ = new CommonTHistoriqueAnnonceQuery();
            $historiqueQ->filterByIdAnnonce($idAnnonce);
            $historiqueQ->filterByIdSupport($idSupport);
            $historiqueQ->orderByDateModification($sensOrderByDate);

            return (array) $historiqueQ->find($connexion);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * permet retourner libelle du statut d'une annonce.
     *
     * @param string $statut statut annonce
     *
     * @return string statut annonce
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2015
     */
    public function getLibelleStatut($statut)
    {
        $libelleStatut = '';
        $libelleStatut = match ($statut) {
            Atexo_Config::getParameter('ANNONCE_STATUT_A_COMPLETER') => Prado::localize('STATUT_ANNONCE_A_COMPLETER'),
            Atexo_Config::getParameter('ANNONCE_STATUT_COMPLET') => Prado::localize('STATUT_ANNONCE_COMPLET'),
            Atexo_Config::getParameter('ANNONCE_STATUT_ENVOYE') => Prado::localize('STATUT_ANNONCE_ENVOYER'),
            Atexo_Config::getParameter('ANNONCE_STATUT_EN_ATTENTE_PUBLICATION') => Prado::localize('STATUT_ANNONCE_EN_ATTENTE_PUBLICATION'),
            Atexo_Config::getParameter('ANNONCE_STATUT_PUBLIE') => Prado::localize('STATUT_ANNONCE_PUBLIE'),
            Atexo_Config::getParameter('ANNONCE_STATUT_REJETE') => Prado::localize('STATUT_ANNONCE_REJETE'),
            default => $libelleStatut,
        };

        return $libelleStatut;
    }

    /**
     * Permet de recuperer la liste des supports de publication.
     *
     * @return array|mixed|PropelObjectCollection
     *
     * @author OKO <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function getSupportsPublication()
    {
        try {
            $supportsQuery = new CommonTSupportPublicationQuery();
            $listeSupports = $supportsQuery->getSupportsPublication();
            if (!empty($listeSupports) && $listeSupports instanceof PropelObjectCollection) {
                return $listeSupports->getData();
            }

            return [];
        } catch (\Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('publicite');
            $logger->error('Erreur lors de la recuperation de la liste des supports : '.$e->getMessage()."\n Methode = Atexo_Publicite_AnnonceSub::getSupportsPublication");
        }
    }

    /**
     * Recupere la liste des formulaires.
     *
     * @param string $idsFormulaire : identifiants des formulaires
     *
     * @return array : liste des formulaires
     *
     * @author OKO <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function getListeFormulaires($idsFormulaire = null)
    {
        try {
            $sql = '
                    SELECT
                      `id`, `libelle`, `type`, if(`type` ='.Atexo_Config::getParameter('TYPE_FORMULAIRE_NATIONAL_STANDARD').", '".Prado::localize('FORMULAIRE_NATIONAL_STANDARD')."', if(`type`=".Atexo_Config::getParameter('TYPE_FORMULAIRE_JOUE').", '".Prado::localize('FORMULAIRE_JOUE')."', if(`type`=".Atexo_Config::getParameter('TYPE_FORMULAIRE_MAPA').", '".Prado::localize('FORMULAIRE_MAPA')."', if(`type`=".Atexo_Config::getParameter('TYPE_FORMULAIRE_DSP').", '".Prado::localize('FORMULAIRE_DSP')."', '')))) as typeFormulaire
                    FROM
                      `t_dispositif_annonce`
                    ";
            $sql .= '
                    WHERE 1
                    ';
            if (!empty($idsFormulaire)) {
                $sql .= "
                        AND `id` IN ($idsFormulaire)
                        ";
            }
            $sql .= " AND visible = '1'";
            $sql .= ' Order By typeFormulaire; ';

            $statement = Atexo_Db::getLinkCommon(true)->prepare($sql);
            $statement->execute();

            $dataFormulaire = ['0' => Prado::localize('TEXT_SELECTIONNER').' ...'];
            while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                $dataFormulaire[$row['id']]['id'] = $row['id'];
                $dataFormulaire[$row['id']]['libelle'] = $row['libelle'];
                $dataFormulaire[$row['id']]['type'] = $row['type'];
                $dataFormulaire[$row['id']]['typeFormulaire'] = $row['typeFormulaire'];
            }

            return $dataFormulaire;
        } catch (\Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('publicite');
            $logger->error("Erreur lors de la recuperation de la liste des formulaires : Requete Sql = $sql , Erreur = ".$e->getMessage()."\n Methode = Atexo_Publicite_AnnonceSub::getListeFormulaires");
        }
    }

    /**
     * Permet de recuperer les preferences des supports de publication de l'agent.
     *
     * @param string $idAgent   : identifiant de l'agent
     * @param string $organisme : organisme de l'agent
     *
     * @return array : liste des preferences des supports de l'agent
     *
     * @author OKO <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function getPreferencesSupportsAgent($idAgent, $organisme)
    {
        try {
            $preferencesSupportsQuery = new CommonTPreferenceSupportPublicationQuery();
            $preferencesSupports = $preferencesSupportsQuery->getPreferencesSupportsAgent($idAgent, $organisme);
            if (!empty($preferencesSupports) && $preferencesSupports instanceof PropelObjectCollection) {
                return $preferencesSupports->getData();
            }

            return [];
        } catch (\Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('publicite');
            $logger->error("Erreur lors de la recuperation des preferences des supports de publication de l'agent : Id_Agent = ".$idAgent." , Organisme = $organisme , Erreur = ".$e->getMessage()."\n Methode = Atexo_Publicite_AnnonceSub::getPreferencesSupportsAgent");
        }
    }

    /**
     * * Permet de recuperer les ids des formulaires en fonction du type d'annonce et des supports.
     *
     * @param array  $listeSupport  : liste des supports
     * @param string $idTypeAnnonce : identifiant du type d'annonce
     *
     * @return array : liste des choix des formulaires a proposer a l'utilisateur
     *
     * @author OKO <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function getChoixIdsFormulairesByTypesAnnoncesAndSupports($listeSupport, $idTypeAnnonce)
    {
        try {
            $parametrageFormulairePubQuery = new CommonTParametrageFormulairePubliciteQuery();
            $listeIdsFormulaires = $parametrageFormulairePubQuery->getChoixIdsFormulairesByTypesAnnoncesAndSupports($listeSupport, $idTypeAnnonce);
            if (!empty($listeIdsFormulaires) && $listeIdsFormulaires instanceof PropelObjectCollection) {
                $idsFormulaires = $listeIdsFormulaires->getData();
                if (!empty($idsFormulaires) && $idsFormulaires[0] instanceof CommonTParametrageFormulairePublicite) {
                    return "'".implode("','", explode(',', $idsFormulaires[0]->getIdsFormulaire()))."'";
                }
            }

            return null;
        } catch (\Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('publicite');
            $logger->error("Erreur lors de la recuperation des choix des ids des formulaires a proposer a l'agent : id type annonce selectionne =  $idTypeAnnonce  , Liste Supports selectionnes = ".print_r($listeSupport, true).' , Erreur = '.$e->getMessage()."\n Methode = Atexo_Publicite_AnnonceSub::getChoixIdsFormulairesByTypesAnnoncesAndSupports");
        }
    }

    /**
     * Permet de creer une annonce de publicite.
     *
     * @param string $idDispositif       : identifiant du dispositif SUB
     * @param string $idDossier          : identifiant du dossier
     * @param string $refCons            : reference de la consultation
     * @param string $org                : organisme de la consultation
     * @param string $idTypeAnnonce      : identifiant technique du type d'annonce
     * @param string $idCompteBoamp      : identifiant du compte BOAMP
     * @param string $idDossierParent    : identifiant du dossier parent
     * @param string $idDispositifParent : identifiant du dispositif parent
     *
     * @return CommonTAnnonceConsultation : objet annonce de publicite
     *
     * @author OKO <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function creerAnnoncePublicite($idDispositif, $idDossier, $consultationId, $org, $idTypeAnnonce, $idCompteBoamp, $idDossierParent = null, $idDispositifParent = null)
    {
        $refCons = null;
        $logger = Atexo_LoggerManager::getLogger('publicite');
        try {
            $logger->info('Debut creation annonce de publicite');
            $annonce = new CommonTAnnonceConsultation();
            $annonce->setIdDispositif($idDispositif);
            $annonce->setIdDossierSub($idDossier);
            $annonce->setOrganisme($org);
            $annonce->setConsultationId($consultationId);
            $annonce->setStatut(Atexo_Config::getParameter('ANNONCE_STATUT_A_COMPLETER'));
            $annonce->setDateStatut(date('Y-m-d H:i:s'));
            $annonce->setIdTypeAnnonce($idTypeAnnonce);
            $annonce->setIdAgent(Atexo_CurrentUser::getIdAgentConnected());
            $annonce->setPrenomNomAgent(Atexo_CurrentUser::getFirstName().' '.Atexo_CurrentUser::getLastName());
            $annonce->setIdCompteBoamp($idCompteBoamp);
            if (!empty($idDossierParent)) {
                $annonce->setIdDossierParent($idDossierParent);
            }
            if (!empty($idDispositifParent)) {
                $annonce->setIdDispositifParent($idDispositifParent);
            }
            $logger->info('Fin creation annonce de publicite : '.print_r($annonce, true));

            return $annonce;
        } catch (\Exception $e) {
            $logger->error("Erreur creation annonce de publicite : ID Dispositif = $idDispositif , ID Dossier SUB = $idDossier , RefCons = $refCons , Org = $org , ID Type Annonce = $idTypeAnnonce , ID Compte BOAMP = $idCompteBoamp \n\nErreur = ".$e->getMessage()."\n\n Methode = Atexo_Publicite_AnnonceSub::ajouterAnnoncePublicite");
        }
    }

    /**
     * Permet d'ajouter des supports de publication a une annonce de publicite.
     *
     * @param CommonTAnnonceConsultation $annonce           : annonce de publicite
     * @param string                     $idSupport         : identifiant technique du support
     * @param string                     $numeroAvisInitial : numero de l'avis initial
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function addSupportsToAnnonce(&$annonce, $idSupport, $numeroAvisInitial = null, $idOffre = null, $statut = null, $message = null, $departementsParution = null)
    {
        $logger = Atexo_LoggerManager::getLogger('publicite');
        try {
            $logger->info("Debut ajout support : ID = $idSupport");

            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $supportAnnonce = Atexo_Publicite_AnnonceSub::getSupportAnnonceBySupportPub($annonce->getId(), $idSupport, $connexion);
            if (!$supportAnnonce instanceof CommonTSupportAnnonceConsultation) {
                $supportAnnonce = new CommonTSupportAnnonceConsultation();
                $supportAnnonce->setIdSupport($idSupport);
                $supportAnnonce->setIdAgent(Atexo_CurrentUser::getIdAgentConnected());
                $supportAnnonce->setPrenomNomAgentCreateur(Atexo_CurrentUser::getFirstName().' '.Atexo_CurrentUser::getLastName());
                $supportAnnonce->setDateCreation(date('Y-m-d H:i:s'));
            }
            if (empty($statut)) {
                $statut = Atexo_Config::getParameter('ANNONCE_STATUT_A_COMPLETER');
            }
            $oldStatut = $supportAnnonce->getStatut();
            if ($oldStatut != $statut) {
                $supportAnnonce->setStatut($statut);
                $supportAnnonce->setDateStatut(date('Y-m-d H:i:s'));
            }
            $supportAnnonce->setIdOffre($idOffre);
            $supportAnnonce->setMessageAcheteur($message);
            $supportAnnonce->setDepartementsParutionAnnonce($departementsParution);
            if (!empty($numeroAvisInitial)) {
                $supportAnnonce->setNumeroAvisParent($numeroAvisInitial);
            }

            $annonce->addCommonTSupportAnnonceConsultation($supportAnnonce);

            $logger->info('Support ajoute avec succes : Support = '.print_r($supportAnnonce, true));
        } catch (\Exception $e) {
            $logger->error('Erreur ajout support de publication : '.$e->getMessage()."\n\nMethode = Atexo_Publicite_AnnonceSub::addSupportsToAnnonce");
        }
    }

    /**
     * permet de mettre à jour tous les annonces créées.
     *
     * @param CommonConsultation $consultation objet consultation
     *
     * @return void
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2015
     */
    public function updateAnnonceSubfromConsultation($consultation, $statutPub = null)
    {
        $annonce = null;
        try {
            $logger = Atexo_LoggerManager::getLogger('publicite');
            if ($consultation instanceof CommonConsultation) {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
                $annonces = self::getAnnoncesEncours($consultation->getId(), $consultation->getOrganisme(), $connexion);
                if (is_array($annonces)) {
                    foreach ($annonces as $annonce) {
                        if ($annonce instanceof CommonTAnnonceConsultation) {
                            $comptePub = CommonTConsultationComptePubQuery::create()->recupererComptePubByIdDonneesCompAndOrg($consultation->getIdDonneeComplementaire(), $consultation->getOrganisme());
                            $xml = Atexo_FormulaireSub_AnnonceEchange::getXmlInitialisationDossier($consultation, $annonce, $comptePub, null);
                            $update = Atexo_FormulaireSub_AnnonceEchange::updateAnnonce($xml);
                            if ($update) {
                                $logger->info('Consultation '.$consultation->getId().' Annonce '.$annonce->getId().' est mise à jour avec succes');
                            } else {
                                $logger->info('Consultation '.$consultation->getId().' Annonce '.$annonce->getId()." n'est pas mise à jour");
                            }
                            if ($annonce->getPubliciteSimplifie() && !empty($statutPub) && $annonce->getStatut() != $statutPub) {
                                $dateStatut = date('Y-m-d H:i:s');
                                $annonce->setStatut($statutPub);
                                $annonce->setDateStatut($dateStatut);
                                $annonce->save($connexion);
                                Atexo_Publicite_AnnonceSub::updateAllStatutSupport($annonce->getId(), $statutPub, $dateStatut, $connexion);
                                Atexo_Publicite_AnnonceSub::ajouterHistorique($annonce, $connexion);
                                $logger->info('MAj statut Annonce '.$annonce->getId().' statut : '.$statutPub);
                            }
                        }
                    }
                }
            }
        } catch (\Exception $e) {
            $logger->error('Consultation '.$consultation->getId().(($annonce instanceof CommonTAnnonceConsultation) ? 'Annonce '.$annonce->getId() : ' ')
                ."Erreur s'est produite lors de la mise à jour de dossier annonce  Atexo_FormulaireSub_AnnonceSub::updateAnnonceSubfromConsultation : ".$e->getMessage().' '.$e->getTraceAsString());
        }
    }

    /**
     * permet de chosir le formulaire à proposer.
     *
     * @param CommonConsultation          $consultation            consultation
     * @param CommonTDonneeComplementaire $donneesCompelementaires donnees de la consultation
     *
     * @return array
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2016
     */
    public static function getFormulaireAndSupports($consultation)
    {
        $logger = null;
        try {
            $logger = Atexo_LoggerManager::getLogger('publicite');
            if ($consultation instanceof CommonConsultation) {
                $donneesCompelementaires = $consultation->getDonneComplementaire();
                $sousTypeOrg = null;
                if (!$consultation->getEntiteAdjudicatrice()) {
                    $typeOrganisme = Atexo_Config::getParameter('PARAMMETTRE_CONCETRATEUR_POUVOIRE_ADJUDICATEUR');
                    $org = $consultation->getOrganismeObject();
                    if ($org instanceof CommonOrganisme) {
                        if (1 == $org->getSousTypeOrganisme()) {
                            $sousTypeOrg = Atexo_Config::getParameter('PARAMMETTRE_CONCETRATEUR_ETAT_ETABLISSEMENT');
                        } else {
                            $sousTypeOrg = Atexo_Config::getParameter('PARAMMETTRE_CONCETRATEUR_AUTRE');
                        }
                    }
                } else {
                    $typeOrganisme = Atexo_Config::getParameter('PARAMMETTRE_CONCETRATEUR_ENTITE_ADJUDICATRICE');
                }
                $categorie = strtolower($consultation->getLibelleCategorieConsultation());
                $article = null;
                $seuil = 0;
                if ($donneesCompelementaires instanceof CommonTDonneeComplementaire) {
                    $seuil = $donneesCompelementaires->getMontantMarche();
                }

                return self::formaterDonneesSupports(Atexo_Concentrateur::getSupportFormulaire($typeOrganisme, $sousTypeOrg, $categorie, $article, $seuil));
            }
        } catch (\Exception $e) {
            $logger->error('Consultation '.$consultation->getId()."Erreur s'est produite lors de la proposition du formulaire : Atexo_FormulaireSub_AnnonceSub::getFormulairePropose : ".$e->getMessage().' '.$e->getTraceAsString());
        }
    }

    /**
     * permet de chosir le formulaire à proposer.
     *
     * @param CommonConsultation $consultation objet consultation
     *
     * @return array
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2015
     */
    public static function getChampsManquantsConsultation($consultation)
    {
        $array = [];
        $pourcentageCons = Atexo_Config::getParameter('POURCENTAGE_CHAMPS_CONSULTATION');
        $nbreChpRempli = 0;
        $nbreChpVide = 0;
        $donneesComplementaires = null;
        $i = 0;
        if ($consultation instanceof CommonConsultation) {
            if ($consultation->getIdTypeProcedureOrg()) {
                ++$nbreChpRempli;
            } else {
                ++$nbreChpVide;
                $array[$i]['LIBELLE_CHAMPS'] = Prado::localize('TEXT_TYPE_PROCEDURE');
                $array[$i++]['ONGLET'] = 'IDENTIFICATION';
            }
            if ($consultation->getCategorie()) {
                ++$nbreChpRempli;
            } else {
                ++$nbreChpVide;
                $array[$i]['LIBELLE_CHAMPS'] = Prado::localize('DEFINE_CATEGORIE_PRINCIPALE');
                $array[$i++]['ONGLET'] = 'IDENTIFICATION';
            }
            if ($consultation->getObjetTraduit()) {
                ++$nbreChpRempli;
            } else {
                ++$nbreChpVide;
                $array[$i]['LIBELLE_CHAMPS'] = Prado::localize('DEFINE_OBJET_CONSULTATION');
                $array[$i++]['ONGLET'] = 'IDENTIFICATION';
            }
            if ($consultation->getCodeCpv1() || $consultation->getCodeCpv2()) {
                ++$nbreChpRempli;
            } else {
                ++$nbreChpVide;
                $array[$i]['LIBELLE_CHAMPS'] = Prado::localize('TEXT_CODE_CPV');
                $array[$i++]['ONGLET'] = 'IDENTIFICATION';
            }
            if ($consultation->getDatefin() && '0000-00-00 00:00:00' != $consultation->getDatefin()) {
                ++$nbreChpRempli;
            } else {
                ++$nbreChpVide;
                $array[$i]['LIBELLE_CHAMPS'] = Prado::localize('DEFINE_DATE_LIMITE_REMIS_PLIS');
                $array[$i++]['ONGLET'] = 'CALENDRIER';
            }
            $donneesComplementaires = $consultation->getDonneComplementaire();
        } else {
            $array[$i]['LIBELLE_CHAMPS'] = Prado::localize('TEXT_TYPE_PROCEDURE');
            $array[$i++]['ONGLET'] = 'IDENTIFICATION';
            $array[$i]['LIBELLE_CHAMPS'] = Prado::localize('DEFINE_CATEGORIE_PRINCIPALE');
            $array[$i++]['ONGLET'] = 'IDENTIFICATION';
            $array[$i]['LIBELLE_CHAMPS'] = Prado::localize('DEFINE_OBJET_CONSULTATION');
            $array[$i++]['ONGLET'] = 'IDENTIFICATION';
            $array[$i]['LIBELLE_CHAMPS'] = Prado::localize('TEXT_CODE_CPV');
            $array[$i++]['ONGLET'] = 'IDENTIFICATION';
            $array[$i]['LIBELLE_CHAMPS'] = Prado::localize('DEFINE_DATE_LIMITE_REMIS_PLIS');
            $array[$i++]['ONGLET'] = 'CALENDRIER';
            $nbreChpVide += 5;
        }

        if ($donneesComplementaires instanceof CommonTDonneeComplementaire) {
            if ($donneesComplementaires->getMotsCles()) {
                ++$nbreChpRempli;
            } else {
                ++$nbreChpVide;
                $array[$i]['LIBELLE_CHAMPS'] = Prado::localize('MOTS_CLES_CONSULTATION');
                $array[$i++]['ONGLET'] = 'IDENTIFICATION';
            }
            if ($donneesComplementaires->getIdCritereAttribution()) {
                ++$nbreChpRempli;
            } else {
                ++$nbreChpVide;
                $array[$i]['LIBELLE_CHAMPS'] = Prado::localize('DEFINE_CRITERE_ATTRIBUTION');
                $onglet = 'DONNEES_REDAC';
                if ((Atexo_Module::isEnabled('Publicite') && !Atexo_Module::isEnabled('DonneesRedac'))) {
                    $onglet = 'IDENTIFICATION';
                }
                $array[$i++]['ONGLET'] = $onglet;
            }
        } else {
            $array[$i]['LIBELLE_CHAMPS'] = Prado::localize('DEFINE_NATURE_MARCHE');
            $array[$i++]['ONGLET'] = 'IDENTIFICATION';
            $array[$i]['LIBELLE_CHAMPS'] = Prado::localize('PRODUIT_ASSOCIE_ANNONCE');
            $array[$i++]['ONGLET'] = 'IDENTIFICATION';
            $array[$i]['LIBELLE_CHAMPS'] = Prado::localize('MOTS_CLES_CONSULTATION');
            $array[$i++]['ONGLET'] = 'IDENTIFICATION';
            $array[$i]['LIBELLE_CHAMPS'] = Prado::localize('DEFINE_CRITERE_ATTRIBUTION');
            $onglet = 'DONNEES_REDAC';
            if ((Atexo_Module::isEnabled('Publicite') && !Atexo_Module::isEnabled('DonneesRedac'))) {
                $onglet = 'IDENTIFICATION';
            }
            $array[$i++]['ONGLET'] = $onglet;
            $nbreChpVide += 4;
        }

        $pourcentage = $nbreChpRempli * $pourcentageCons;

        return ['pourcentage' => $pourcentage, 'nbreChampsManquants' => $nbreChpVide, 'libellesChampsManquants' => $array];
    }

    /**
     * permet de chosir le formulaire à proposer.
     *
     * @param CommonConsultation $consultation objet consultation
     *
     * @return array
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2015
     */
    public static function getChampsManquantsCompteBoamp($compteBoamp)
    {
        $array = [];
        $pourcentageCons = Atexo_Config::getParameter('POURCENTAGE_CHAMPS_BOAMP');
        $nbreChpRempli = 0;
        $nbreChpVide = 0;
        $donneesComplementaires = null;
        if ($compteBoamp instanceof CommonAcheteurPublic) {
            if ($compteBoamp->getPrm()) {
                ++$nbreChpRempli;
            } else {
                ++$nbreChpVide;
                $array[]['LIBELLE_CHAMPS'] = Prado::localize('DEFINE_CORRESPONDANT');
            }
            if ($compteBoamp->getDenomination()) {
                ++$nbreChpRempli;
            } else {
                ++$nbreChpVide;
                $array[]['LIBELLE_CHAMPS'] = Prado::localize('DEFINE_NOM_ORG');
            }
            if ($compteBoamp->getDepartementMiseEnLigne()) {
                ++$nbreChpRempli;
            } else {
                ++$nbreChpVide;
                $array[]['LIBELLE_CHAMPS'] = Prado::localize('DEPARTEMENT_MISE_EN_LIGNE');
            }
            if ($compteBoamp->getReferenceCommande()) {
                ++$nbreChpRempli;
            } else {
                ++$nbreChpVide;
                $array[]['LIBELLE_CHAMPS'] = Prado::localize('REFERENCE_COMMANDE');
            }
            if ($compteBoamp->getAdresse() || $compteBoamp->getCp() || $compteBoamp->getVille() || $compteBoamp->getFactureAdresse() || $compteBoamp->getFactureCp() || $compteBoamp->getFactureVille()) {
                $nbreChpRempli += 2;
            } else {
                $nbreChpVide += 2;
                $array[]['LIBELLE_CHAMPS'] = Prado::localize('ADRESSE_ORGANISME_BOAMP');
                $array[]['LIBELLE_CHAMPS'] = Prado::localize('ADRESSE_FACTURATION_BOAMP');
            }
            if ($compteBoamp->getTypeOrg()) {
                ++$nbreChpRempli;
            } else {
                ++$nbreChpVide;
                $array[]['LIBELLE_CHAMPS'] = Prado::localize('TEXT_TYPE_ORG');
            }

            if ($compteBoamp->getFactureDenomination()) {
                ++$nbreChpRempli;
            } else {
                ++$nbreChpVide;
                $array[]['LIBELLE_CHAMPS'] = Prado::localize('DEFINE_DENOMINATION');
            }
        } else {
            $array[]['LIBELLE_CHAMPS'] = Prado::localize('DEFINE_CORRESPONDANT');
            $array[]['LIBELLE_CHAMPS'] = Prado::localize('DEFINE_NOM_ORG');
            $array[]['LIBELLE_CHAMPS'] = Prado::localize('DEPARTEMENT_MISE_EN_LIGNE');
            $array[]['LIBELLE_CHAMPS'] = Prado::localize('REFERENCE_COMMANDE');
            $array[]['LIBELLE_CHAMPS'] = Prado::localize('ADRESSE_ORGANISME_BOAMP');
            $array[]['LIBELLE_CHAMPS'] = Prado::localize('ADRESSE_FACTURATION_BOAMP');
            $array[]['LIBELLE_CHAMPS'] = Prado::localize('TEXT_TYPE_ORG');
            $array[]['LIBELLE_CHAMPS'] = Prado::localize('DEFINE_DENOMINATION');
            $nbreChpVide += 8;
        }

        $pourcentage = $nbreChpRempli * $pourcentageCons;

        return ['pourcentage' => $pourcentage, 'nbreChampsManquants' => $nbreChpVide, 'libellesChampsManquants' => $array];
    }

    public function formaterDonneesSupports($retourConcentrateur)
    {
        $return = ['idFormulaire' => null, 'listeSupports' => []];
        if (is_array($retourConcentrateur)) {
            if (isset($retourConcentrateur['idDispositif'])) {
                $return['idFormulaire'] = $retourConcentrateur['idDispositif'];
            }

            if (isset($retourConcentrateur['listeSupportAsPrix']) && is_array($retourConcentrateur['listeSupportAsPrix'])) {
                foreach ($retourConcentrateur['listeSupportAsPrix'] as $supportConcentrateur) {
                    if (is_array($supportConcentrateur) && isset($supportConcentrateur['support']['libelle'])) {
                        $supportPub = CommonTSupportPublicationQuery::create()->getSupportByMotCles($supportConcentrateur['support']['libelle']);
                        if ($supportPub instanceof CommonTSupportPublication) {
                            $support = new Atexo_Publicite_SupportVo();
                            if (isset($supportConcentrateur['prix']['montant'])) {
                                $support->setTarif($supportConcentrateur['prix']['montant']);
                            }
                            $support->setCode($supportConcentrateur['support']['libelle']);
                            $support->setIdSupportPublication($supportPub->getId());
                            $support->setLibelle($supportPub->getNom());
                            $support->setLogo(Atexo_Controller_Front::t().'/images/'.$supportPub->getImageLogo());
                            $return['listeSupports'][] = $support;
                        }
                    }
                }
            }
        }

        return $return;
    }

    public static function getTypeAvisDispositif($idDispositif)
    {
        return match ($idDispositif) {
            Atexo_Config::getParameter('ID_DISPOSITIF_JOUE2') => Atexo_Config::getParameter('ID_TYPE_AVIS_EUROPEENS'),
            Atexo_Config::getParameter('ID_DISPOSITIF_FNS1') => Atexo_Config::getParameter('ID_TYPE_AVIS_NATIONAL'),
            Atexo_Config::getParameter('ID_DISPOSITIF_MAPA1') => Atexo_Config::getParameter('ID_TYPE_AVIS_NATIONAL'),
            default => null,
        };
    }

    public static function downLoadPdfAnnonce($idDossier)
    {
        $logger = null;
        try {
            $logger = Atexo_LoggerManager::getLogger('publicite');
            $pdfContent = Atexo_FormulaireSub_AnnonceEchange::getPdfAnnonce($idDossier);
            DownloadFile::downloadFileContent(Atexo_Config::getParameter('NOM_TELECHARGEMENT_PDF_ANNONCE'), $pdfContent);
        } catch (Exception $e) {
            $logger->error("Erreur s'est produite lors du téléchargement du dossier".$idDossier.' : Atexo_FormulaireSub_AnnonceSub::getFormulairePropose : '.$e->getMessage().' '.$e->getTraceAsString());
        }
    }

    public static function envoyerAnnonce($annonce)
    {
        $connexion = null;
        $save = false;
        if ($annonce instanceof CommonTAnnonceConsultation) {
            try {
                $logger = Atexo_LoggerManager::getLogger('publicite');
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));

                /** KBE Suite à la demande de aurélien on met à jour l'annonce avant de publier */
                $consultation = CommonConsultationPeer::retrieveByPK($annonce->getConsultationId(), $connexion);
                $comptePub = CommonTConsultationComptePubQuery::create()->recupererComptePubByIdDonneesCompAndOrg($consultation->getIdDonneeComplementaire(), $consultation->getOrganisme());
                $xml = Atexo_FormulaireSub_AnnonceEchange::getXmlInitialisationDossier($consultation, $annonce, $comptePub, null, true, 'false');
                $update = Atexo_FormulaireSub_AnnonceEchange::updateAnnonce($xml);
                if ($update) {
                    $logger->info('Consultation '.$consultation->getId().' Annonce '.$annonce->getId().' est mise à jour avec succes');
                } else {
                    $logger->info('Consultation '.$consultation->getId().' Annonce '.$annonce->getId()." n'est pas mise à jour");
                }
                /* KBE Suite à la demande de aurélien on met à jour l'annonce avant de publier */
                $logger->info("Envoi de l'annonce ".$annonce->getId().' de la consultation '.$annonce->getConsultationId().' '.$annonce->getOrganisme());
                $envoi = Atexo_FormulaireSub_AnnonceEchange::publierDossier($annonce->getIdDossierSub());

                if ($envoi) {
                    $connexion->beginTransaction();
                    $dateStatut = date('Y-m-d H:i:s');
                    $statut = Atexo_Config::getParameter('ANNONCE_STATUT_ENVOYE');
                    $annonce->setStatut($statut);
                    $annonce->setDateStatut($dateStatut);
                    $annonce->save($connexion);
                    $save = Atexo_Publicite_AnnonceSub::updateAllStatutSupport($annonce->getId(), $statut, $dateStatut, $connexion);
                    if ($save) {
                        $connexion->commit();
                        //historique apres le commite car il ya des info qui doivent s'enregistrer en base avant de creer l'historique
                        Atexo_Publicite_AnnonceSub::ajouterHistorique($annonce, $connexion);
                    }
                } else {
                    $logger->error('Erreur envoi de le destinataire '.$annonce->getId().' de la consultation '.$annonce->getConsultationId().' '.$annonce->getOrganisme());
                }
            } catch (\Exception $e) {
                $connexion->rollBack();
                $logger->error('Erreur envoi de le destinataire '.$annonce->getId().' de la consultation '.$annonce->getConsultationId().' '.$annonce->getOrganisme().' '.$e->getMessage().' '.$e->getTraceAsString());
                throw $e;
            }
        }

        return $save;
    }

    public static function envoyerPubSimap($destinataire)
    {
        $connexion = null;
        try {
            $datePublication = false;
            if ($destinataire instanceof CommonDestinatairePub) {
                if ($destinataire->getIdSupport() == Atexo_Config::getParameter('TYPE_SUPPORT_OPOCE')
                    && $destinataire->getEtat() == Atexo_Config::getParameter('STATUT_DESTINATAIRE_AVIS_COMPLET')
                ) {
                    $logger = Atexo_LoggerManager::getLogger('publicite');
                    $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));

                    $logger->info('Envoi du destinataire '.$destinataire->getId().' '.$destinataire->getOrganisme());
                    $envoi = Atexo_FormulaireSub_AnnonceEchange::publierDossier($destinataire->getIdDossier());

                    if ($envoi) {
                        $statut = Atexo_Config::getParameter('STATUT_DESTINATAIRE_AVIS_OPOCE_EN_ATTENTE_INTEGRATION');
                        $datePublication = date('Y-m-d H:i:s');
                        $connexion->beginTransaction();
                        $destinataire->setEtat($statut);
                        $destinataire->setDatePublication($datePublication);
                        $destinataire->save($connexion);
                        (new Atexo_Publicite_AvisPub())->createHistorique($destinataire->getOrganisme(), $destinataire->getIdAvis(), $datePublication, $statut, $connexion, null, Atexo_Config::getParameter('TYPE_SUPPORT_OPOCE'));
                        $connexion->commit();
                    } else {
                        $logger->error('Erreur envoi de le destinataire '.$destinataire->getId().' '.$destinataire->getOrganisme());
                    }
                }
            }

            return $datePublication;
        } catch (\Exception $e) {
            $connexion->rollBack();
            $logger->error('Erreur envoi de le destinataire '.$destinataire->getId().'  '.$destinataire->getOrganisme().' '.$e->getMessage().' '.$e->getTraceAsString());
            throw $e;
        }
    }

    /**
     * permet de chercher les supports possible pour l'agent.
     *
     * @param int                        $idAgent   id agent createur
     * @param string                     $organisme organisme de l'agent
     * @param PDO                        $connexion connexion BD
     * @param CommonTAnnonceConsultation $annonce   annonce
     *
     * @return array
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 2017-develop
     *
     * @copyright Atexo 2017
     */
    public static function getSupportsAgent($idAgent, $organisme, $connexion, $annonce = null, $lieuxExecution = [])
    {
        try {
            $lieuxExecutionAdapter = [];
            if (is_array($lieuxExecution) && !empty($lieuxExecution)) {
                foreach ($lieuxExecution as $l) {
                    $denm2 = (new Atexo_Geolocalisation_GeolocalisationN2())->retrieveDenomination2ByIdGeoN2($l);
                    if (!empty($denm2)) {
                        $lieuxExecutionAdapter[] = '%#'.$denm2.'#%';
                    }
                }
            }
            $hasPref = (array) CommonTPreferenceSupportPublicationQuery::create()->filterByIdAgent($idAgent)->filterByOrganisme($organisme)->find($connexion);
            if (!empty($hasPref)) {
                $param = [];
                $querySupports = <<<QUERY
	SELECT
			t_support_publication.*
	FROM
			t_preference_support_publication,
			t_support_publication
	WHERE
			t_preference_support_publication.id_agent = :idAgent
		AND
			t_preference_support_publication.organisme = :organisme
		AND
			t_preference_support_publication.active = '1'
		AND
			t_preference_support_publication.id_support = t_support_publication.id
		AND
			t_support_publication.visible = '1'

QUERY;
                if (is_array($lieuxExecutionAdapter) && !empty($lieuxExecutionAdapter)) {
                    $sqllieuExecution = '';
                    foreach ($lieuxExecutionAdapter as $l) {
                        $sqllieuExecution .= <<<QUERY

                        OR
                            t_support_publication.lieux_execution LIKE '$l'
QUERY;
                    }
                    $querySupports .= <<<QUERY
AND
			(
			    t_support_publication.lieux_execution IS NULL
			    $sqllieuExecution
			)
QUERY;
                }

                if ($annonce instanceof CommonTAnnonceConsultation) {
                    $querySupports .= <<<QUERY
UNION
		SELECT
				t_support_publication.*
		FROM
				t_support_publication, t_support_annonce_consultation
		WHERE
				t_support_publication.id = t_support_annonce_consultation.id_support
			AND
				t_support_annonce_consultation.id_annonce_cons = :idAnnonce

QUERY;
                    $param[':idAnnonce'] = $annonce->getId();
                }

                $querySupports .= <<<QUERY

ORDER BY
        ordre,
        groupe,
        tag_debut_fin_groupe
QUERY;

                $param[':idAgent'] = $idAgent;
                $param[':organisme'] = $organisme;

                $statment = $connexion->prepare($querySupports);
                $statment->execute($param);
                $res = CommonTSupportPublicationPeer::populateObjects($statment);
            } else {
                $criteria = new Criteria();
                if (is_array($lieuxExecutionAdapter) && !empty($lieuxExecutionAdapter)) {
                    $crit0 = $criteria->getNewCriterion(CommonTSupportPublicationPeer::LIEUX_EXECUTION, null, Criteria::ISNULL);
                    foreach ($lieuxExecutionAdapter as $l) {
                        $crit1 = $criteria->getNewCriterion(CommonTSupportPublicationPeer::LIEUX_EXECUTION, $l, Criteria::LIKE);
                        $crit0->addOr($crit1);
                    }
                    $criteria->add($crit0);
                }
                $res = self::getSupportsPublicationActifs($criteria);
            }

            return $res;
        } catch (\Exception $e) {
            throw $e;
        }
    }

    /**
     * Permet de recuperer un support de publication par code.
     *
     * @param string $code
     *
     * @return CommonTSupportPublication
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1.0
     *
     * @since 2017-develop
     *
     * @copyright Atexo 2017
     */
    public static function getSupportPublicationByCode($code)
    {
        try {
            $supportsQuery = new CommonTSupportPublicationQuery();

            return $supportsQuery->findOneByCode($code);
        } catch (\Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('publicite');
            $logger->error('Erreur lors de la recuperation du support : '.$e->getMessage()."\n Methode = Atexo_Publicite_AnnonceSub::getSupportPublicationByCode");
        }
    }

    /**
     * Permet de recuperer un support de publication par code.
     *
     * @param string $id
     *
     * @return CommonTOffreSupportPublicite
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1.0
     *
     * @since 2017-develop
     *
     * @copyright Atexo 2017
     */
    public static function getOffreSupportPublicationById($id)
    {
        try {
            $offreSupportsQuery = new CommonTOffreSupportPubliciteQuery();

            return $offreSupportsQuery->findOneById($id);
        } catch (\Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('publicite');
            $logger->error('Erreur lors de la recuperation du support : '.$e->getMessage()."\n Methode = Atexo_Publicite_AnnonceSub::getSupportPublicationByCode");
        }
    }

    /**
     * permet de recupperer tous les supports actif.
     *
     * @return array un tableau qui contient l'ensemble des supports
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2015
     */
    public static function getSupportsPublicationActifs($criteria = null)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $criteria = (empty($criteria)) ? new Criteria() : $criteria;
        $criteria->add(CommonTSupportPublicationPeer::VISIBLE, 1);
        $criteria->addAscendingOrderByColumn(CommonTSupportPublicationPeer::ORDRE);
        $criteria->addAscendingOrderByColumn(CommonTSupportPublicationPeer::GROUPE);
        $criteria->addAscendingOrderByColumn(CommonTSupportPublicationPeer::TAG_DEBUT_FIN_GROUPE);

        return CommonTSupportPublicationPeer::doSelect($criteria, $connexion);
    }

    /**
     * permet de retouner les supports de l'annonce.
     *
     * @param int $idAnnonce identifiant annonce
     *
     * @return array supports
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 2017-esr
     *
     * @copyright Atexo 2017
     */
    public static function getSupportAnnonceByIdAnnonceCons($idAnnonce)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));

        return (array) CommonTSupportAnnonceConsultationQuery::create()->filterByIdAnnonceCons($idAnnonce)->find($connexion);
    }

    /**
     * Permet de recuperer le support de publication by id.
     *
     * @param int $id
     *
     * @return mixed|CommonTSupportPublication|false
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2017-esr
     *
     * @copyright Atexo 2018
     */
    public function getSupportsPublicationById($id)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $supportQuery = new CommonTSupportPublicationQuery();

        return $supportQuery->getSupportsPublicationByPk($id, $connexion);
    }
}
