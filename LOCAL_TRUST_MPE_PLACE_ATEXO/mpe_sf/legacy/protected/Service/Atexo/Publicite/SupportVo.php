<?php

namespace Application\Service\Atexo\Publicite;

/**
 * objet permet de gérer l'enchange entre MPE et le concentrateur.
 *
 * @author AME <amal.elbekkaoui@atexo.com>
 *
 * @version 1.0
 *
 * @since roadmap-2015
 *
 * @copyright Atexo 2015
 */
class Atexo_Publicite_SupportVo
{
    private $libelle;
    private $logo;
    private $tarif = 0;
    private $selectionner;
    private $url;
    private $idSupportPublication;
    private $ordre;
    private $code;

    /**
     * @return mixed
     */
    public function getLibelle()
    {
        return $this->libelle;
    }

    /**
     * @param mixed $libelle
     */
    public function setLibelle($libelle)
    {
        $this->libelle = $libelle;
    }

    /**
     * @return mixed
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * @param mixed $logo
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;
    }

    /**
     * @param mixed $tarif
     */
    public function setTarif($tarif)
    {
        $this->tarif = $tarif;
    }

    /**
     * @return int
     */
    public function getTarif()
    {
        return $this->tarif;
    }

    /**
     * @return mixed
     */
    public function getSelectionner()
    {
        return $this->selectionner;
    }

    /**
     * @param mixed $selectionner
     */
    public function setSelectionner($selectionner)
    {
        $this->selectionner = $selectionner;
    }

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return mixed
     */
    public function getIdSupportPublication()
    {
        return $this->idSupportPublication;
    }

    /**
     * @param mixed $idSupportPublication
     */
    public function setIdSupportPublication($idSupportPublication)
    {
        $this->idSupportPublication = $idSupportPublication;
    }

    /**
     * @return mixed
     */
    public function getOrdre()
    {
        return $this->ordre;
    }

    /**
     * @param mixed $ordre
     */
    public function setOrdre($ordre)
    {
        $this->ordre = $ordre;
    }

    /**
     * @return mixed
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param mixed $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }
}
