<?php

namespace Application\Service\Atexo\Publicite;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonReferentielFormXmlPeer;
use Application\Propel\Mpe\CommonReferentielTypeXmlPeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;

/**
 * Classe de manipulation des formulaires XML.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Publicite_ReferentielFormXml
{
    /**
     * retourne la liste des types des formulmaire xml.
     */
    public function retreiveListTypeFormulaireXml()
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonReferentielTypeXmlPeer::ORGANISME, Atexo_CurrentUser::getCurrentOrganism());
        $listTypeFormXml = CommonReferentielTypeXmlPeer::doselect($c, $connexion);
        if ($listTypeFormXml) {
            return $listTypeFormXml;
        } else {
            return [];
        }
    }

    /**
     * retourne la liste des fomrulaires d'une consutation.
     */
    public function retreiveListFormulaireXml($consultationId, $organisme = null)
    {
        if (!$organisme) {
            $organisme = Atexo_CurrentUser::getCurrentOrganism();
        }
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonReferentielFormXmlPeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonReferentielFormXmlPeer::ORGANISME, $organisme);
        //$c->addJoin(CommonReferentielFormXmlPeer::ORGANISME, CommonReferentielTypeXmlPeer::ORGANISME);
        $listForm = CommonReferentielFormXmlPeer::doSelectJoinCommonReferentielTypeXml($c, $connexion);
        if ($listForm) {
            return $listForm;
        } else {
            return [];
        }
    }

    /**
     * retourne un objet Referentielformxml identifié par 'identifiant.
     */
    public function retreiveReferentielFormXmlById($identifiant)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonReferentielFormXmlPeer::ID, $identifiant);
        $c->add(CommonReferentielFormXmlPeer::ORGANISME, Atexo_CurrentUser::getCurrentOrganism());
        //$c->addJoin(CommonReferentielFormXmlPeer::ORGANISME, CommonReferentielTypeXmlPeer::ORGANISME);
        $formXml = CommonReferentielFormXmlPeer::doSelectJoinCommonReferentielTypeXml($c, $connexion);
        if ($formXml) {
            return $formXml;
        } else {
            return [];
        }
    }

    /**
     * supprime un type de formulaire.
     */
    public function deleteFormulaireXml($idForm)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        (new Atexo_Publicite_Destinataire())->deleteAllDestinataireForOneTypeForm($idForm);
        $criteria = new Criteria();
        $criteria->add(CommonReferentielFormXmlPeer::ID, $idForm);
        $criteria->add(CommonReferentielFormXmlPeer::ORGANISME, Atexo_CurrentUser::getCurrentOrganism());
        CommonReferentielFormXmlPeer::doDelete($criteria, $connexion);
    }

    /** retourne identifiant de formulaire.
     *
     */
    public function retreiveIdTypeFormXmlByIdAvisMarche($identifiant)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonReferentielTypeXmlPeer::ID, $identifiant);
        $c->add(CommonReferentielTypeXmlPeer::ORGANISME, Atexo_CurrentUser::getCurrentOrganism());
        $typeFormXml = CommonReferentielTypeXmlPeer::doselectOne($c, $connexion);
        if ($typeFormXml) {
            return $typeFormXml;
        } else {
            return false;
        }
    }

    /**
     * retourne le type formulmaire xml.
     *
     * @param: idTypeFormulaireXml
     * @return: objet type formulaire xml
     */
    public function retrieveTypeFormXmlById($idTypeFormXml, $organisme = null)
    {
        if (!$organisme) {
            $organisme = Atexo_CurrentUser::getCurrentOrganism();
        }
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonReferentielTypeXmlPeer::ID, $idTypeFormXml);
        $c->add(CommonReferentielTypeXmlPeer::ORGANISME, $organisme);
        $typeFormXml = CommonReferentielTypeXmlPeer::doSelectOne($c, $connexion);
        if ($typeFormXml) {
            return $typeFormXml;
        } else {
            return false;
        }
    }
}
