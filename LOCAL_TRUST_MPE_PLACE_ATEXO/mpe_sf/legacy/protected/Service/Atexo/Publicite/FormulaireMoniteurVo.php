<?php

namespace Application\Service\Atexo\Publicite;

/**
 * Vo pour définir les critères de la fonction d'envoi du ficher xml au Groupe Moniteur.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Publicite_FormulaireMoniteurVo
{
    private string $_xml = '';
    private $_authentificate;
    private string $_annonceFile = '';
    private string $_urlSendPath = '';
    private string $_urlHost = '';
    private string $_nameFile = '';

    public function getXml()
    {
        return $this->_xml;
    }

    public function setXml($xml)
    {
        $this->_xml = $xml;
    }

    public function getAuthentificate()
    {
        return $this->_authentificate;
    }

    public function setAuthentificate($xml)
    {
        $this->_authentificate = $xml;
    }

    public function getAnnonceFile()
    {
        return $this->_annonceFile;
    }

    public function setAnnonceFile($nameFile)
    {
        $this->_annonceFile = $nameFile;
    }

    public function getUrlHost()
    {
        return $this->_urlHost;
    }

    public function setUrlHost($urlHost)
    {
        $this->_urlHost = $urlHost;
    }

    public function getUrlSendPath()
    {
        return $this->_urlSendPath;
    }

    public function setUrlSendPath($urlSendPath)
    {
        $this->_urlSendPath = $urlSendPath;
    }

    public function getNameFile()
    {
        return $this->_nameFile;
    }

    public function setNameFile($nameChamp)
    {
        $this->_nameFile = $nameChamp;
    }
}
