<?php

namespace Application\Service\Atexo\Publicite;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonAnnonceBoamp;
use Application\Propel\Mpe\CommonAnnonceMoniteurPeer;
use Application\Propel\Mpe\CommonCompteMoniteur;
use Application\Propel\Mpe\CommonCompteMoniteurPeer;
use Application\Propel\Mpe\CommonGroupeMoniteurPeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Util;
use AtexoCrypto\Dto\Fichier;

/**
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Publicite_Moniteur
{
    //----- Fonctions d'envoi du XML au MONITEUR  -----
    /* Fonction d'envoi du xml avis au MONITEUR
    * @param Atexo_Publicite_FormulaireMoniteurVo $formMoniteurVo
    * @return array(true|false, error_msg)
    */
    public function sendXmlMoniteur(Atexo_Publicite_FormulaireMoniteurVo $formMoniteurVo)
    {
        $boundary = Atexo_Config::getParameter('MONITEUR_BOUNDARY');
        $moniteurHost = $formMoniteurVo->getUrlHost();
        $annonceFile = $formMoniteurVo->getAnnonceFile();
        $fileName = $formMoniteurVo->getNameFile();
        $xml = $formMoniteurVo->getXml();
        $sendPath = $formMoniteurVo->getUrlSendPath();
        $auth = $formMoniteurVo->getAuthentificate();
        if ('' == $moniteurHost) {
            echo 'Erreur de configuration du compte MONITEUR';

            return;
        }

        $data = "--$boundary\r\n";
        $data .= "Content-Disposition: form-data; name=\"annonce\"; filename=\"$fileName\"\r\n";
        $data .= "Content-Type: application/octet-stream\r\n\r\n";
        $data .= $annonceFile."\r\n";
        $data .= "--$boundary\r\n";
        $data .= "Content-Disposition: form-data; name=\"publication\"; filename=\"moniteur.xml\"\r\n";
        $data .= "Content-Type: text/xml\r\n\r\n";
        $data .= $xml."\r\n";
        $data .= '--'.$boundary."--\r\n";

        $urlProxy = Atexo_Config::getParameter('URL_PROXY');
        $portProxy = Atexo_Config::getParameter('PORT_PROXY');

        if ('' != $urlProxy) {
            $msg = 'POST http://'.$moniteurHost.$sendPath." HTTP/1.1\r\n";
        } else {
            $msg = 'POST '.$sendPath." HTTP/1.1\r\n";
        }

        $msg .= 'Host: '.$moniteurHost."\r\n";
        $msg .= 'Authorization: Basic '.base64_encode($auth)."\r\n";
        $msg .= "Content-Type: multipart/form-data; boundary=$boundary\r\n";
        $msg .= 'Content-Length: '.strlen($data)."\r\n\r\n";
        $msg .= $data;

        $log = $msg;
        $log .= "\n\n";
        $log .= "\n\t____________________________________\n";
        $log .= 'Reponse de retour :';
        $log .= "\n\n";

        if ('' != $urlProxy) {
            $fd = fsockopen($urlProxy, $portProxy, $errno, $errstr);
            if (!$fd) {
                echo "$errstr ($errno)<br />\n";
                exit();
            }
        } else {
            $fd = fsockopen($moniteurHost, 80, $errno, $errstr);
            if (!$fd) {
                echo "$errstr ($errno)<br />\n";
                exit();
            }
        }

        $result = '';
        if ($fd) {
            fputs($fd, $msg);
            while (!feof($fd)) {
                $result .= fgets($fd, 4320);
            }
            fclose($fd);
        } else {
            echo "$errstr ($errno)<br />\n";
        }
        $log .= $result;
        $filePath = Atexo_Config::getParameter('LOG_FILE_PATH_MONITEUR').'EnvoiMoniteur_'.date('YmdHisu').'.log';
        Atexo_Util::writeLogFile(Atexo_Config::getParameter('LOG_FILE_PATH_MONITEUR'), $filePath, $log);
        //DEBUG : affichage de l'accusé de réponse
        //display_xml_in_html($result);  die;
        $status = self::parseMoniteurStatusResponse($result);
        if (0 != strcmp('OK', $status)) {
            // Envoi échoué
            return [false, $result];
        } else {
            // Envoi réussi
            return [true, $result];
        }
    }

    /**
     * retourne le compte moniteur pour l'organisme.
     *
     *@return  Comptemoniteur
     */
    public function retrieveCompteMoniteur($organisme)
    {
        // Parametrage de la Table CompteMoniteur
        //  Dev Préprod dans la table CompteMoniteur moniteur_target = 0
        //  PROD dans la table CompteMoniteur moniteur_target = 1
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonCompteMoniteurPeer::ID, Atexo_Config::getParameter('ID_COMPTE_MONITEUR'));
        $c->add(CommonCompteMoniteurPeer::ORGANISME, $organisme);
        $compteMoniteur = CommonCompteMoniteurPeer::doSelectOne($c, $connexion);
        if ($compteMoniteur instanceof CommonCompteMoniteur) {
            return $compteMoniteur;
        } else {
            return false;
        }
    }

    /**
     * retourne l'url du groupe moniteur.
     */
    public function retrieveMoniteurHost($compteMoniteur)
    {
        if ($compteMoniteur) {
            if ('1' == $compteMoniteur->getMoniteurTarget()) {
                // Plateforme Cible PROD
                return 'concentrateur-annonces-moniteur.local-trust.com';
            } else {
                // Plateforme Cible DEV ET PREPROD
                return 'concentrateur-annonces-moniteur-test.local-trust.com';
            }
        } else {
            return '';
        }
    }

    /**
     * retourne login et password du compte moniteur.
     */
    public function retrieveMoniteurLoginPassword($compteMoniteur)
    {
        if ($compteMoniteur) {
            return $compteMoniteur->getMoniteurLogin().':'.$compteMoniteur->getMoniteurPassword();
        } else {
            return '';
        }
    }

    /*
     * Fonction qui renvoi Le nom du fichier d'annonce
     * du Moniteur
     * @param   $idBoamp    identifient de l'annonce boamp
     * @param  $organisme acronyme de l'organisme
     * @return      nom du fichier d'annonce xml
     *
     */

    public function getMoniteurXmlName($idBoamp, $organisme)
    {
        $annonce = (new Atexo_Publicite_Destinataire())->retrieveDestinataireById($idBoamp, $organisme);
        if ($annonce instanceof CommonAnnonceBoamp) {
            return $annonce->getNomFichierXml();
        } else {
            return false;
        }
    }

    /*
     * Fonction qui renvoi L'adresse d'envoi des fichiers d'annonce
     * du Moniteur
     * @param   $compteMoniteur objet du compte Moniteur
     * @return  url     de dépôt d'annonce
     *
     */

    public function retrieveMoniteurSendPath($compteMoniteur)
    {
        if ($compteMoniteur instanceof CommonCompteMoniteur) {
            if ('1' == $compteMoniteur->getMoniteurTarget()) {  // Plateforme Cible PROD
                return '/publication/depot/publicationMoniteur.do';
            } else {   // Plateforme Cible DEV ET PREPROD
                return '/publicationpreprod/depot/publicationMoniteur.do';
            }
        } else {
            return '';
        }
    }

    /**
     * Fonction qui retourne l'URL de la plateforme de suivi
     * du Moniteur en fonction
     * de la configuration du Compte Moniteur
     * id=1 URL de test
     * id=2 URL de PROD.
     *
     * @return url url de suivi du Moniteur
     */
    public function retrieveMoniteurSuiviUrl($compteMoniteur)
    {
        $semaine_derniere = Atexo_Util::DateInXDays(14);
        if (7 == strlen($semaine_derniere)) {
            $semaine_derniere = '0'.$semaine_derniere;
        }
        if ($compteMoniteur) {
            if ('1' == $compteMoniteur->getMoniteurTarget()) {
                return 'concentrateur-annonces-moniteur.local-trust.com/publication/depot/suiviPublication.do?dateMin='.$semaine_derniere;
            } else {
                // Debug TEST
                return 'concentrateur-annonces-moniteur-test.local-trust.com/publicationpreprod/depot/suiviPublication.do?dateMin='.$semaine_derniere;
            }
        } else {
            return '';
        }
    }

    /* Fonction qui renvoie le statut de l'envoi au moniteur
     * @param $respIn Reponse du moniteur suite a l'envoi du xml avis
     * @return $statut_envoi Statut de l'envoi au moniteur OK si reussi ERREUR_XXX sinon
     */
    public function parseMoniteurStatusResponse($strIn)
    {
        $strIn = self::getJustXmlInString($strIn);
        if (!$strIn) {
            return false;
        }
        $respIn = simplexml_load_string($strIn);
        $etat = (string) $respIn->Etat;
        if ('' != $etat) {
            return $etat;
        //Recuperation de 'element Etat
        } else {
            return false;
        }
    }

    /**
     * Fonction qui recupère l'attribut d'un XML.
     *
     * @param       xmlIn                       Chaine XML
     * @param       arg_element         Nom de la balise
     * @param       arg_attrib          Nom de l'attribut
     * @param       attrib                  Contenu de l'attribut
     */
    public function parseMoniteurNumeroAnnonce($strIn)
    {
        $strIn = self::getJustXmlInString($strIn);
        if (!$strIn) {
            return false;
        }
        $respIn = simplexml_load_string($strIn);
        $idMoniteur = (string) $respIn->Fichier['idMoniteur'];
        if ('' != $idMoniteur) {
            return $idMoniteur;
        //Recuperation de 'element Etat
        } else {
            return false;
        }
    }

    /* Fonction qui renvoie la raison de l'echec de l'envoi au moniteur
     * @param $respIn Reponse du moniteur suite a l'envoi du xml avis
     * @return $fail_cause Raison de l'echec de l'envoi
     */
    public function parseMoniteurFailureCause($strIn)
    {
        $strIn = self::getJustXmlInString($strIn);
        if (!$strIn) {
            return false;
        }
        $respIn = simplexml_load_string($strIn);
        $failCause = (string) $respIn->Message;
        if ('' != $failCause) {
            //Recuperation de 'element Etat
            //$failCause = (string)$respIn->Message;
            return $failCause;
        } else {
            return false;
        }
    }

    /**
     * Fonction qui rempli la table AnnonceMoniteur
     * par les informations de retour du Moniteur.
     *
     * @param       resultIn                    XML de retour de la publication
     * @param $statutEnvoi       Statut d'envoi de l'avis
     * @param $msgError          message d'erreur de la plateforme moniteur
     * @param $idAnnonceMoniteur l'id du Moniteur
     */
    public function saveEnvoiMoniteurResponse($response, $statutEnvoi, $idAnnMoniteur, $connexion)
    {
        //FIXME
        $date_envoie = date('Y-m-d H:i:s');
        $date_maj = $date_envoie;
        $annonceMoniteur = CommonAnnonceMoniteurPeer::retrieveByPK($idAnnMoniteur, Atexo_CurrentUser::getCurrentOrganism(), $connexion);
        $annonceMoniteur->setDateEnvoi($date_envoie);
        $annonceMoniteur->setDateMaj($date_maj);
        if ('OK' == $statutEnvoi) {
            $annonceMoniteur->setNumAnnonce($response);
            $annonceMoniteur->setStatutDestinataire(Atexo_Config::getParameter('STATUT_MONITEUR_AVIS_EMIS'));
        } else {
            $annonceMoniteur->setMessageError(htmlspecialchars($response, ENT_QUOTES));
            $annonceMoniteur->setStatutDestinataire(Atexo_Config::getParameter('STATUT_MONITEUR_ENVOI_ECHOUE'));
        }

        return $annonceMoniteur->save($connexion);
    }

    /**
     * Recherche des groupe moniteur.
     */
    public static function search($idService = null)
    {
        $c = new Criteria();
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonGroupeMoniteurPeer::SERVICE_ID, $idService, Criteria::EQUAL);
        $c->add(CommonGroupeMoniteurPeer::ORGANISME, Atexo_CurrentUser::getCurrentOrganism(), Criteria::EQUAL);
        $c->addAscendingOrderByColumn(CommonGroupeMoniteurPeer::IDENTIFIANT);
        $moniteurs = CommonGroupeMoniteurPeer::doSelect($c, $connexion);

        return $moniteurs;
    }

    /**
     * Supprime un Groupe de moniteur.
     *
     * @param int $idGroupe l'id du groupe
     */
    public static function delete($idGroupe)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $moniteurs = CommonGroupeMoniteurPeer::retrieveByPk($idGroupe, Atexo_CurrentUser::getCurrentOrganism(), $connexion);
        $criteria = new Criteria();
        $criteria->add(CommonGroupeMoniteurPeer::ORGANISME, Atexo_CurrentUser::getCurrentOrganism());
        $criteria->add(CommonGroupeMoniteurPeer::ID, $moniteurs->getId());
        CommonGroupeMoniteurPeer::doDelete($criteria, $connexion);
    }

    public static function retrieveGroupeById($idGroupe)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $moniteurs = CommonGroupeMoniteurPeer::retrieveByPk($idGroupe, Atexo_CurrentUser::getCurrentOrganism(), $connexion);

        return $moniteurs;
    }

    /**
     * returne tout les compte moniteur ratachés a un service.
     */
    public function retrieveAllGroups($idService, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonGroupeMoniteurPeer::SERVICE_ID, $idService);
        $c->add(CommonGroupeMoniteurPeer::ORGANISME, Atexo_CurrentUser::getCurrentOrganism());
        $groups = CommonGroupeMoniteurPeer::doSelect($c, $connexion);
        if ($groups) {
            return $groups;
        } else {
            return [];
        }
    }

    /**
     * retourne le flux xml à partir du texte donné en param.
     */
    public function getJustXmlInString($strIn)
    {
        if ($strIn) {
            $strIn = strstr($strIn, '<?xml');
            $TagAccuse = explode('</Accuse>', $strIn);
            if (is_array($TagAccuse)) {
                $xml = $TagAccuse[0].'</Accuse>';
                $xml = str_replace("\r\n", '', str_replace("\n", '', $xml));
                if ($xml) {
                    return $xml;
                }
            }

            return false;
        }
    }
}
