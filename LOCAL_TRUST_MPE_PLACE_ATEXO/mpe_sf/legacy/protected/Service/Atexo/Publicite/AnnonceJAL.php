<?php

namespace Application\Service\Atexo\Publicite;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonAnnonceJALPeer;
use Application\Propel\Mpe\CommonAnnonceJALPieceJointePeer;
use Application\Propel\Mpe\CommonJALPeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;

/**
 * Classe de manipulation des destinataire.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Publicite_AnnonceJAL
{
    /**
     * retourne la liste des fomrulaires libre d'une consutation.
     */
    public function retreiveListFormulaireLibre($consultationId, $organisme = null)
    {
        if (!$organisme) {
            $organisme = Atexo_CurrentUser::getCurrentOrganism();
        }
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonAnnonceJALPeer::CONSULTATION_ID, $consultationId);
        if ($organisme) {
            $c->add(CommonAnnonceJALPeer::ORGANISME, $organisme);
        }
        $listForm = CommonAnnonceJALPeer::doSelect($c, $connexionCom);
        if ($listForm) {
            return $listForm;
        } else {
            return [];
        }
    }

    /**
     * retourne la liste des fomrulaires libre d'une consutation.
     */
    public function retreiveListFormulaireLibreById($id, $org = null)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        if (!$org) {
            $org = Atexo_CurrentUser::getCurrentOrganism();
        }
        $c = new Criteria();
        $c->add(CommonAnnonceJALPeer::ID, $id);
        if ($org) {
            $c->add(CommonAnnonceJALPeer::ORGANISME, $org);
        }
        $listForm = CommonAnnonceJALPeer::doSelectOne($c, $connexionCom);

        if ($listForm) {
            return $listForm;
        } else {
            return [];
        }
    }

    /**
     * supprime l'objet pj à partir de l'id passé en paramètre.
     *
     * @param  $id id de la pj à supprimer
     * @param $org Organismes
     *
     * @return bool
     */
    public static function DeletePjAnnonceJAL($idpj, $org = null)
    {
        if (!$org) {
            $org = Atexo_CurrentUser::getCurrentOrganism();
        }
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $PJ = CommonAnnonceJALPieceJointePeer::retrieveByPk($idpj, $org, $connexionCom);
        if (is_object($PJ)) {
            $PJ->delete($connexionCom);
        }
    }

    /**
     * retourne l'objet pj.
     */
    public function retreiveFormulairePJByIdPiece($id, $org = null)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonAnnonceJALPieceJointePeer::PIECE, $id);
        if ($org) {
            $c->add(CommonAnnonceJALPieceJointePeer::ORGANISME, $org);
        }

        return CommonAnnonceJALPieceJointePeer::doSelectOne($c, $connexionCom);
    }

    /**
     * Supprimer une annonce Format libre.
     */
    public function deleteAnnonce($id)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $c = new Criteria();
        $c->add(CommonAnnonceJALPeer::ID, $id);
        $c->add(CommonAnnonceJALPeer::ORGANISME, Atexo_CurrentUser::getCurrentOrganism());
        CommonAnnonceJALPeer::doDelete($c, $connexionCom);
    }

    public function getEmailByIdJAl($idJal, $org = null)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        if (!$org) {
            $org = Atexo_CurrentUser::getCurrentOrganism();
        }
        $c = new Criteria();
        $c->add(CommonJALPeer::ID, $idJal);
        if ($org) {
            $c->add(CommonJALPeer::ORGANISME, $org);
        }
        $listDest = CommonJALPeer::doSelectOne($c, $connexionCom);

        return $listDest;
    }
}
