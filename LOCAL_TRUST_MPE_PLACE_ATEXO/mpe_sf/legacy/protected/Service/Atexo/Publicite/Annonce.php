<?php

namespace Application\Service\Atexo\Publicite;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonAnnonce;
use Application\Propel\Mpe\CommonAnnoncePeer;
use Application\Propel\Mpe\CommonCategorieLot;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonOrganisme;
use Application\Propel\Mpe\CommonRenseignementsBoamp;
use Application\Propel\Mpe\CommonTConsultationComptePub;
use Application\Propel\Mpe\CommonTConsultationComptePubQuery;
use Application\Propel\Mpe\CommonTCritereAttribution;
use Application\Propel\Mpe\CommonTDonneeComplementaire;
use Application\Propel\Mpe\CommonValeurReferentiel;
use Application\Propel\Mpe\CommonValeurReferentielPeer;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_CommonReferentiel;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_PublicPurchaser;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Category;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureType;
use DomDocument;

/**
 * Classe de manipulation des destinataire.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Publicite_Annonce
{
    public function retreiveCommonAnnoncebyFileName($fileName, $criterLike = false)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        if ($criterLike) {
            $c->add(CommonAnnoncePeer::NOM_FICHIER_XML, '%_'.$fileName, Criteria::LIKE);
        } else {
            $c->add(CommonAnnoncePeer::NOM_FICHIER_XML, $fileName);
        }
        $annonce = CommonAnnoncePeer::doSelectOne($c, $connexionCom);
        if ($annonce instanceof CommonAnnonce) {
            return $annonce;
        } else {
            return false;
        }
    }

    public function retreiveCommonAnnoncebyFileNameAndRefConsultation($consultationId, $fileName, $criterLike = false)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        if ($criterLike) {
            $c->add(CommonAnnoncePeer::NOM_FICHIER_XML, '%_'.$fileName, Criteria::LIKE);
        } else {
            $c->add(CommonAnnoncePeer::NOM_FICHIER_XML, $fileName);
        }
        $c->add(CommonAnnoncePeer::CONSULTATION_ID, $consultationId);
        $annonce = CommonAnnoncePeer::doSelectOne($c, $connexionCom);
        if ($annonce instanceof CommonAnnonce) {
            return $annonce;
        } else {
            return false;
        }
    }

    public function generateXml($consultation)
    {
        $lots = $consultation->getAllLots();
        $filePath = Atexo_Config::getParameter('CHEMIN_MODEL_INITIALISATION_XML_GM');
        $handle = fopen($filePath, 'r');
        $contents = fread($handle, filesize($filePath));
        fclose($handle);
        $lotsHasVariantes = false;
        if ((is_countable($lots) ? count($lots) : 0) > 0) {
            $scriptLotHeader = '<lots> 
        			  				<tousLots></tousLots>';
            $scriptLot = '';
            foreach ($lots as $oneLot) {
                $scriptLot .= '<lot>';
                $scriptLot .= '<numLot>'.Atexo_Util::escapeXmlChars($oneLot->getLot()).'</numLot> ';
                $scriptLot .= '<intituleLot><![CDATA['.$oneLot->getDescription().']]></intituleLot>';
                if ($oneLot->getCodeCpv1() == null) {
                    $scriptLot .= '<CPV>
								  <ObjetPrincipal>
								  	  <ClassPrincipale>' . Atexo_Util::escapeXmlChars($oneLot->getCodeCpv1()) . '</ClassPrincipale>
								  </ObjetPrincipal>';
                    $arrayCodeCpvSec = explode('#', $oneLot->getCodeCpv2());
                    if (is_array($arrayCodeCpvSec) && $arrayCodeCpvSec[1]) {
                        foreach ($arrayCodeCpvSec as $codeCpvSec) {
                            if ($codeCpvSec != $arrayCodeCpvSec[0]) {
                                $scriptLot .= '<ObjetComplementaire><ClassPrincipale>' . Atexo_Util::escapeXmlChars($codeCpvSec) . '</ClassPrincipale></ObjetComplementaire>';
                            }
                        }
                    }
                    $scriptLot .= '</CPV>';
                }
                $scriptLot .= '<description><![CDATA['.$oneLot->getDescriptionDetail().']]></description>';
                //Autres données complémentaires
                $scriptLot .= self::genererXmlAutresDonneesComplementairesConsultationLots($oneLot);
                $scriptLot .= '</lot>';
                if (self::objetHasVariante($oneLot)) {
                    $lotsHasVariantes = true;
                }
            }
            $scriptLotFooter = '</lots>';
            $contents = Atexo_Util::xmlStrReplace($contents, '<!--allotissement-->', ($scriptLotHeader.$scriptLot.$scriptLotFooter));
        } else {
            $contents = Atexo_Util::xmlStrReplace($contents, '<!--allotissement-->', '<MarcheUnique/>');
        }///caterogiePrincipale
        if ('' != $consultation->getCategorie()) {
            $categorie = Atexo_Consultation_Category::retrieveLibelleCategorie($consultation->getCategorie(), false);
            $contents = Atexo_Util::xmlStrReplace($contents, '<!--caterogiePrincipale-->', '<'.$categorie.'/>');
        }
        ///TypeProcedure
        $typeProcedureObject = (new Atexo_Consultation_ProcedureType())->retrieveTypeProcedureObject($consultation->getIdTypeProcedureOrg());
        $contents = Atexo_Util::xmlStrReplace($contents, '<!--typeProcedure-->', $typeProcedureObject->getTagBoamp());
        // Ajout des codes CPV Complementaires
        $xmlCpv = '';
        $arrayCodeCpvSec = explode('#', $consultation->getCodeCpv2());
        if (is_array($arrayCodeCpvSec) && $arrayCodeCpvSec[1]) {
            foreach ($arrayCodeCpvSec as $codeCpvSec) {
                if ($codeCpvSec != $arrayCodeCpvSec[0]) {
                    $xmlCpv .= '<ObjetComplementaire><ClassPrincipale>'.$codeCpvSec.'</ClassPrincipale></ObjetComplementaire>';
                }
            }
        }
        $contents = Atexo_Util::xmlStrReplace($contents, '<!--cpvComplementaire-->', $xmlCpv);
        if (0 != $consultation->getEnvOffre()) {
            $xmlDateRecetion = '<ReceptionOffres>'.Atexo_Util::escapeXmlChars(str_replace(' ', 'T', $consultation->getDateFin())).'</ReceptionOffres>';
        } else {
            $xmlDateRecetion = '<ReceptCandidatures>'.Atexo_Util::escapeXmlChars(str_replace(' ', 'T', $consultation->getDateFin())).'</ReceptCandidatures>';
        }
        $contents = Atexo_Util::xmlStrReplace($contents, '<!--dateReception-->', $xmlDateRecetion);
        if ('' != $consultation->getObjet()) {
            $contents = Atexo_Util::xmlStrReplace($contents, 'OBJET_CONSULTATION', Atexo_Util::escapeXmlChars(($consultation->getObjet())));
        } else {
            $contents = Atexo_Util::xmlStrReplace($contents, 'OBJET_CONSULTATION', '');
        }
        if ('' != $consultation->getCodeCpv1()) {
            $contents = Atexo_Util::xmlStrReplace($contents, 'CODE_CPV', Atexo_Util::escapeXmlChars(($consultation->getCodeCpv1())));
        } else {
            $contents = Atexo_Util::xmlStrReplace($contents, 'CODE_CPV', '');

            //Fixe  de suppréssion de la balise vide du BOAMP MPE-18036
            $dropBalise = [
                '<CPV>', '</CPV>', '<ObjetPrincipal>', '</ObjetPrincipal>', '<ClassPrincipale>', '</ClassPrincipale>'
            ];
            foreach ($dropBalise as $balise) {
                $contents = Atexo_Util::xmlStrReplace($contents, $balise, '');
            }
            //Fin du fixe
        }
        if ('' != $consultation->getReferenceUtilisateur()) {
            $contents = Atexo_Util::xmlStrReplace($contents, 'REF_CONS', Atexo_Util::escapeXmlChars(($consultation->getReferenceUtilisateur())));
        } else {
            $contents = Atexo_Util::xmlStrReplace($contents, 'REF_CONS', '');
        }
        //adressesComplt
        //Données complémentaires de la consultation
        self::genererXmlDonneesComplementairesConsultation($consultation, $contents, $lotsHasVariantes);

        // Ajout de l'url d'accés direct de la consultation
        $urlAccesDirect = (new Atexo_Consultation())->getUrlAcceeDirect($consultation, Atexo_CurrentUser::getOrganismAcronym());
        if ('' != $urlAccesDirect) {
            $urlAccesDirect = str_replace('/?page=', '/index.php?page=', $urlAccesDirect);
            $urlEncode = str_replace('&', '&amp;', $urlAccesDirect);
        } else {
            $urlEncode = '';
        }

        return $contents;
    }

    /**
     * @param $idAcheteur
     * @param $contents
     * @param $consultation
     *
     * @return mixed
     */
    public function addCompteBoampInfoInXml($idAcheteur, $contents, $consultation)
    {
        $agentObj = (new Atexo_Agent())->retrieveAgent(Atexo_CurrentUser::getIdAgentConnected());
        if (!$agentObj) {
            $this->response->redirect('?page=Agent.AgentHome');
        }
        $acheteurPublic = (new Atexo_Publicite_Boamp())->retreiveAcheteurPublic($idAcheteur, Atexo_CurrentUser::getOrganismAcronym());
        $typeOrg = '';
        $telAcheteur = '';
        $faxAcheteur = '';
        $mailAcheteur = '';
        $nomOrganismeAcheteur = '';
        $adresseVoieAcheteur = '';
        $adresseCPAcheteur = '';
        $adresseVilleAcheteur = '';
        $adresseURLAcheteur = '';
        $adresseURLProfilAcheteur = '';

        $factureNumero = '';
        $nomOrganismeFacture = '';
        $facturationService = '';
        $adresseVoieFacture = '';
        $adresseCPFacture = '';
        $adresseVilleFacture = '';
        $facturePays = '';

        $livraisonService = '';
        $livraisonAdresse = '';
        $livraisonCodePostal = '';
        $livraisonVille = '';
        $livraisonPays = '';
        $codeNuts = '';
        $modalitesFinancement = '';

        $nomOrganismePC = '';
        $adresseVoiePC = '';
        $adresseCPPC = '';
        $adresseVillePC = '';
        $adresseTelPC = '';
        $adresseFaxPC = '';
        $adresseMailPC = '';
        $adresseURLPC = '';

        $nomOrganismeRR = '';
        $adresseVoieRR = '';
        $adresseCPRR = '';
        $adresseVilleRR = '';
        $adresseTelRR = '';
        $adresseFaxRR = '';
        $adresseMailRR = '';
        $adresseURLRR = '';

        if (Atexo_Module::isEnabled('Publicite')) {
            if ($consultation instanceof CommonConsultation) {
                $tConsultationComptePubQuery = new CommonTConsultationComptePubQuery();
                $comptePublicite = $tConsultationComptePubQuery->recupererComptePubByIdDonneesCompAndOrg($consultation->getIdDonneeComplementaire(), $consultation->getOrganisme());
                if ($comptePublicite instanceof CommonTConsultationComptePub) {
                    $nomOrganismeAcheteur = $comptePublicite->getDenomination();
                    $adresseVoieAcheteur = $comptePublicite->getAdresse();
                    $adresseCPAcheteur = $comptePublicite->getCp();
                    $adresseVilleAcheteur = $comptePublicite->getVille();
                    $adresseURLAcheteur = $comptePublicite->getUrl();
                    $adresseURLProfilAcheteur = $comptePublicite->getUrl();

                    $nomOrganismeFacture = $comptePublicite->getFactureDenomination();
                    $adresseVoieFacture = $comptePublicite->getFactureAdresse();
                    $adresseCPFacture = $comptePublicite->getFactureCp();
                    $adresseVilleFacture = $comptePublicite->getFactureVille();

                    $nomOrganismePC = $comptePublicite->getInstanceRecoursOrganisme();
                    $adresseVoiePC = $comptePublicite->getInstanceRecoursAdresse();
                    $adresseCPPC = $comptePublicite->getInstanceRecoursCp();
                    $adresseVillePC = $comptePublicite->getInstanceRecoursVille();
                }
                $contents = Atexo_Util::xmlStrReplace($contents, '<activites>TEXT_ACTIVITES</activites>', '');
                $contents = Atexo_Util::xmlStrReplace($contents, '<activitesSecteursSpeciaux>SECTEURS_SPECIAUX</activitesSecteursSpeciaux>', '');
                $urlAccesDirect = (new Atexo_Consultation())->getUrlAcceeDirect($consultation, $consultation->getOrganisme());
                $urlAccesDirect = str_replace('/?page=', '/index.php?page=', $urlAccesDirect);
                $adressesComplt = '
<adressesComplt>
	<document>
		<coord>
			<url><![CDATA['.$urlAccesDirect.']]></url>
		</coord>
	</document>
	<envoi>
		<coord>
			<url><![CDATA['.$urlAccesDirect.']]></url>
		</coord>
	</envoi>
</adressesComplt>
';
                $contents = Atexo_Util::xmlStrReplace($contents, '<!--adressesComplt-->', $adressesComplt);
            }
        } else {
            $nomOrganismeAcheteur = $acheteurPublic->getDenomination() ?: '';
            $adresseVoieAcheteur = $acheteurPublic->getAdresse() ?: '';
            $adresseCPAcheteur = $acheteurPublic->getCp() ?: '';
            $adresseVilleAcheteur = $acheteurPublic->getVille() ?: '';
            $telAcheteur = $acheteurPublic->getTelephone() ?: '';
            $faxAcheteur = $acheteurPublic->getFax() ?: '';
            $mailAcheteur = $acheteurPublic->getMail() ?: '';
            $adresseURLAcheteur = $acheteurPublic->getUrl() ?: '';
            $adresseURLProfilAcheteur = $acheteurPublic->getUrlAcheteur() ?: '';

            $factureNumero = $acheteurPublic->getFactureNumero() ?: '';
            $nomOrganismeFacture = $acheteurPublic->getFactureDenomination() ?: '';
            $facturationService = $acheteurPublic->getFacturationService() ?: '';
            $adresseVoieFacture = $acheteurPublic->getFactureAdresse() ?: '';
            $adresseCPFacture = $acheteurPublic->getFactureCp() ?: '';
            $adresseVilleFacture = $acheteurPublic->getFactureVille() ?: '';
            $facturePays = $acheteurPublic->getFacturePays() ?: '';

            $livraisonService = $acheteurPublic->getLivraisonService() ?: '';
            $livraisonAdresse = $acheteurPublic->getLivraisonAdresse() ?: '';
            $livraisonCodePostal = $acheteurPublic->getLivraisonCodePostal() ?: '';
            $livraisonVille = ($acheteurPublic->getLivraisonVille()) ? ucfirst(strtolower($acheteurPublic->getLivraisonVille())) : '';
            $livraisonPays = $acheteurPublic->getLivraisonPays() ?: '';
            $codeNuts = ($acheteurPublic->getCodeNuts()) ? '<lieuCodeNUTS>'.Atexo_Util::escapeXmlChars(($acheteurPublic->getCodeNuts())).'</lieuCodeNUTS>' : '';
            $modalitesFinancement = $acheteurPublic->getModalitesFinancement() ?: '';

            $infosICR = (new Atexo_Publicite_RenseignementsBoamp())->retrieveOneRenseignementBoamp($acheteurPublic->getId(), Atexo_Config::getParameter('PROCEDURE_RECOURS_1'));
            if ($infosICR instanceof CommonRenseignementsBoamp) {
                $nomOrganismePC = $infosICR->getOrganisme() ?: '';
                $adresseVoiePC = $infosICR->getAdresse() ?: '';
                $adresseCPPC = $infosICR->getCp() ?: '';
                $adresseVillePC = $infosICR->getVille() ?: '';
                $adresseTelPC = $infosICR->getTelephone() ?: '';
                $adresseFaxPC = $infosICR->getFax() ?: '';
                $adresseMailPC = $infosICR->getMail() ?: '';
            }

            $infosIRR = (new Atexo_Publicite_RenseignementsBoamp())->retrieveOneRenseignementBoamp($acheteurPublic->getId(), Atexo_Config::getParameter('PROCEDURE_RECOURS_4'));
            if ($infosIRR instanceof CommonRenseignementsBoamp) {
                $nomOrganismeRR = $infosIRR->getOrganisme() ?: '';
                $adresseVoieRR = $infosIRR->getAdresse() ?: '';
                $adresseCPRR = $infosIRR->getCp() ?: '';
                $adresseVilleRR = $infosIRR->getVille() ?: '';
                $adresseTelRR = $infosIRR->getTelephone() ?: '';
                $adresseFaxRR = $infosIRR->getFax() ?: '';
                $adresseMailRR = $infosIRR->getMail() ?: '';
            }
            $contents = self::addAdresseInformation($acheteurPublic, $contents, $consultation);
        }

        $contents = Atexo_Util::xmlStrReplace($contents, 'TEXT_NOM', $agentObj->getNom());
        $contents = Atexo_Util::xmlStrReplace($contents, 'TEXT_PRENOM', $agentObj->getPrenom());

        $contents = Atexo_Util::xmlStrReplace($contents, 'TEXT_ID_EMMETEUR', Atexo_CurrentUser::getIdAgentConnected() ?: '');
        $contents = Atexo_Util::xmlStrReplace($contents, 'TEXT_PASSWORD', Atexo_Util::escapeXmlChars(($acheteurPublic->getBoampPassword() ?: '')));
        $contents = Atexo_Util::xmlStrReplace($contents, 'TEXT_LOGIN', Atexo_Util::escapeXmlChars(($acheteurPublic->getBoampLogin() ?: '')));
        $contents = Atexo_Util::xmlStrReplace($contents, 'TEXT_MAIL', Atexo_Util::escapeXmlChars(($acheteurPublic->getBoampMail() ?: '')));

        $typeOrg = $acheteurPublic->getTypeOrg() ? '<ClassificationOrganisme>'.Atexo_Util::escapeXmlChars($acheteurPublic->getTypeOrg()).'</ClassificationOrganisme>' : '';
        $contents = Atexo_Util::xmlStrReplace($contents, 'TYPEORG', $typeOrg);
        $contents = Atexo_Util::xmlStrReplace($contents, 'TEXT_ACHETEUR_PUBLIC', Atexo_Util::escapeXmlChars($nomOrganismeAcheteur));
        $contents = Atexo_Util::xmlStrReplace($contents, 'TEXT_CP', Atexo_Util::escapeXmlChars($adresseCPAcheteur));
        $contents = Atexo_Util::xmlStrReplace($contents, 'TEXT_LIEU_DIT', Atexo_Util::escapeXmlChars($adresseVoieAcheteur));
        $contents = Atexo_Util::xmlStrReplace($contents, 'TEXT_VILLE', Atexo_Util::escapeXmlChars(ucfirst(strtolower($adresseVilleAcheteur))));
        $contents = Atexo_Util::xmlStrReplace($contents, 'TEXT_TEL', Atexo_Util::escapeXmlChars($telAcheteur));
        $contents = Atexo_Util::xmlStrReplace($contents, 'TEXT_FAX', Atexo_Util::escapeXmlChars($faxAcheteur));
        $contents = Atexo_Util::xmlStrReplace($contents, 'TEXT_MEL', Atexo_Util::escapeXmlChars($mailAcheteur));
        $contents = Atexo_Util::xmlStrReplace($contents, 'TEXT_URL', Atexo_Util::escapeXmlChars($adresseURLAcheteur));
        $contents = Atexo_Util::xmlStrReplace($contents, 'PROFILURL', Atexo_Util::escapeXmlChars($adresseURLProfilAcheteur));

        ///////////////////////////Service Facturation////////////////////////////
        $contents = Atexo_Util::xmlStrReplace($contents, 'CODE_FACT', Atexo_Util::escapeXmlChars($factureNumero));
        $contents = Atexo_Util::xmlStrReplace($contents, 'DENOM_FACT', Atexo_Util::escapeXmlChars($nomOrganismeFacture));
        $contents = Atexo_Util::xmlStrReplace($contents, 'NOM_SERVICE_FACT', Atexo_Util::escapeXmlChars($facturationService));
        $contents = Atexo_Util::xmlStrReplace($contents, 'ADRESSE_FACT', Atexo_Util::escapeXmlChars($adresseVoieFacture));
        $contents = Atexo_Util::xmlStrReplace($contents, 'CP_FACT', Atexo_Util::escapeXmlChars($adresseCPFacture));
        $contents = Atexo_Util::xmlStrReplace($contents, 'VILLE_FACT', Atexo_Util::escapeXmlChars(ucfirst(strtolower($adresseVilleFacture))));
        $contents = Atexo_Util::xmlStrReplace($contents, 'PAYS_FACT', Atexo_Util::escapeXmlChars($facturePays));
        ///////////////////////////Adresse de livraison ////////////////////////////

        $contents = Atexo_Util::xmlStrReplace($contents, 'NOM_SERVICE_LIV', Atexo_Util::escapeXmlChars($livraisonService));
        $contents = Atexo_Util::xmlStrReplace($contents, 'ADRESSE_LIV', Atexo_Util::escapeXmlChars($livraisonAdresse));
        $contents = Atexo_Util::xmlStrReplace($contents, 'CP_LIV', Atexo_Util::escapeXmlChars($livraisonCodePostal));
        $contents = Atexo_Util::xmlStrReplace($contents, 'VILLE_LIV', Atexo_Util::escapeXmlChars($livraisonVille));
        $contents = Atexo_Util::xmlStrReplace($contents, 'PAYS_LIV', Atexo_Util::escapeXmlChars($livraisonPays));

        $contents = Atexo_Util::xmlStrReplace($contents, '<!--codesNuts-->', $codeNuts);
        $contents = Atexo_Util::xmlStrReplace($contents, 'MODALITES_FINANCEMENT', Atexo_Util::escapeXmlChars($modalitesFinancement));

        $contents = Atexo_Util::xmlStrReplace($contents, 'TEXT_INSTANCE_CHARGEE_RECOURS', Atexo_Util::escapeXmlChars($nomOrganismePC));
        $contents = Atexo_Util::xmlStrReplace($contents, 'TEXT_ICR_LIEU_DIT', Atexo_Util::escapeXmlChars($adresseVoiePC));
        $contents = Atexo_Util::xmlStrReplace($contents, 'TEXT_ICR_CP', Atexo_Util::escapeXmlChars($adresseCPPC));
        $contents = Atexo_Util::xmlStrReplace($contents, 'TEXT_ICR_VILLE', Atexo_Util::escapeXmlChars(ucfirst(strtolower($adresseVillePC))));
        $contents = Atexo_Util::xmlStrReplace($contents, 'TEXT_ICR_TEL', Atexo_Util::escapeXmlChars($adresseTelPC));
        $contents = Atexo_Util::xmlStrReplace($contents, 'TEXT_ICR_FAX', Atexo_Util::escapeXmlChars($adresseFaxPC));
        $contents = Atexo_Util::xmlStrReplace($contents, 'TEXT_ICR_MEL', Atexo_Util::escapeXmlChars($adresseMailPC));

        $contents = Atexo_Util::xmlStrReplace($contents, 'TEXT_INTRODUCTION_RECOURS_RENSEIGNEMENTS', Atexo_Util::escapeXmlChars($nomOrganismeRR));
        $contents = Atexo_Util::xmlStrReplace($contents, 'TEXT_IRR_LIEU_DIT', Atexo_Util::escapeXmlChars($adresseVoieRR));
        $contents = Atexo_Util::xmlStrReplace($contents, 'TEXT_IRR_CP', Atexo_Util::escapeXmlChars($adresseCPRR));
        $contents = Atexo_Util::xmlStrReplace($contents, 'TEXT_IRR_VILLE', Atexo_Util::escapeXmlChars(ucfirst(strtolower($adresseVilleRR))));
        $contents = Atexo_Util::xmlStrReplace($contents, 'TEXT_IRR_TEL', Atexo_Util::escapeXmlChars($adresseTelRR));
        $contents = Atexo_Util::xmlStrReplace($contents, 'TEXT_IRR_FAX', Atexo_Util::escapeXmlChars($adresseFaxRR));
        $contents = Atexo_Util::xmlStrReplace($contents, 'TEXT_IRR_MEL', Atexo_Util::escapeXmlChars($adresseMailRR));

        return utf8_decode($contents);
    }

    public function addAdresseInformation($acheteurPublic, $contents, $consultation)
    {
        // Type de pouvoir adjudicateur et activité(s) principale(s)
        if ('' != $acheteurPublic->getTypePouvoirActivite()) {
            $idsPouvoirActivite = explode('#', $acheteurPublic->getTypePouvoirActivite());
            foreach ($idsPouvoirActivite as $oneId) {
                $idsArray[] = str_replace('_', '', strstr($oneId, '_'));
            }
            $typePouvoirActivite = (new Atexo_CommonReferentiel())->retrieveValeurReferentielByArrayId($idsArray);
            $activites = '';
            $activitesSecteursSpeciaux = '';

            foreach ($typePouvoirActivite as $oneType) {
                if ($oneType->getIdReferentiel() == Atexo_Config::getParameter('REFERENTIEL_PRINCIPAL_ACTIVITES_POUVOIR')) {
                    $activites .= $oneType->getLibelle2();
                } elseif ($oneType->getIdReferentiel() == Atexo_Config::getParameter('REFERENTIEL_PRINCIPAL_ACTIVITES_ENTITE')) {
                    $activitesSecteursSpeciaux .= $oneType->getLibelle2();
                }
            }
            if ('' != $activites) {
                $contents = Atexo_Util::xmlStrReplace($contents, 'TEXT_ACTIVITES', $activites);
            } else {
                $contents = Atexo_Util::xmlStrReplace($contents, '<activites>TEXT_ACTIVITES</activites>', '');
            }
            if ('' != $activitesSecteursSpeciaux) {
                $contents = Atexo_Util::xmlStrReplace($contents, 'SECTEURS_SPECIAUX', $activitesSecteursSpeciaux);
            } else {
                $contents = Atexo_Util::xmlStrReplace($contents, '<activitesSecteursSpeciaux>SECTEURS_SPECIAUX</activitesSecteursSpeciaux>', '');
            }
        } else {
            $contents = Atexo_Util::xmlStrReplace($contents, '<activites>TEXT_ACTIVITES</activites>', '');
            $contents = Atexo_Util::xmlStrReplace($contents, '<activitesSecteursSpeciaux>SECTEURS_SPECIAUX</activitesSecteursSpeciaux>', '');
        }

        // Adresses complémentaires  &  Procédures de recours
        $renseignementsBoamp = (new Atexo_Publicite_RenseignementsBoamp())->retrieveRenseignementsBoampByIdType($acheteurPublic->getId());
        if ((is_countable($renseignementsBoamp) ? count($renseignementsBoamp) : 0) > 0) {
            $adressesComplt = '<adressesComplt>';
            $admOuTech = '<AdmOuTech>';
            //$adresseRecours = "<adresseRecours>";
            $blocComplt = '';
            $typeRensA = '';
            foreach ($renseignementsBoamp as $oneType) {
                $blocComplt .= '<PersonnePhysique><nom><![CDATA['.$oneType->getCorrespondant().']]></nom></PersonnePhysique>';
                $blocComplt .= '<PersonneMorale><![CDATA['.$oneType->getOrganisme().']]></PersonneMorale>';
                $blocComplt .= '<adr>';
                $blocComplt .= '<cp>'.$oneType->getCp().'</cp>';
                $blocComplt .= '<lieu_dit><![CDATA['.$oneType->getAdresse().']]></lieu_dit>';
                $blocComplt .= '<ville><![CDATA['.ucfirst(strtolower($oneType->getVille())).']]></ville>';
                //$blocComplt .= "<pays>".$oneType->getPays()."</pays>";
                $blocComplt .= '</adr>';
                $blocComplt .= '<coord>';
                $blocComplt .= '<tel>'.$oneType->getTelephone().'</tel>';
                $blocComplt .= '<fax>'.$oneType->getFax().'</fax>';
                $blocComplt .= '<poste>'.$oneType->getPoste().'</poste>';
                $blocComplt .= '<mel>'.$oneType->getMail().'</mel>';

                $urlAccesDirect = '';
                //type 3 et 4 : adresses obtenir les documents et déposer les offres
                if (($oneType->getIdType() == Atexo_Config::getParameter('ADRESSE_COMPLEMENTAIRES_3'))
                    || ($oneType->getIdType() == Atexo_Config::getParameter('ADRESSE_COMPLEMENTAIRES_4'))
                    ) {
                    // Ajout de l'url d'accès direct de la consultation par défaut
                    $urlAccesDirect = (new Atexo_Consultation())->getUrlAcceeDirect($consultation, Atexo_CurrentUser::getOrganismAcronym());
                }
                //type 1,2,5: autres adresses administratif et technique
                else {
                    if ('' != $oneType->getUrl()) {
                        $urlAccesDirect = $oneType->getUrl();
                    } else {
                        if ('' != $oneType->getCorrespondant() ||
                            '' != $oneType->getOrganisme() ||
                            '' != $oneType->getCp() ||
                            '' != $oneType->getAdresse() ||
                            '' != $oneType->getVille() ||
                            '' != $oneType->getTelephone() ||
                            '' != $oneType->getFax() ||
                            '' != $oneType->getPoste() ||
                            '' != $oneType->getMail()
                        ) {
                            // Ajout de l'url d'accès direct de la consultation si elle est vide uniquement s'il existe une donnée saisie dans le bloc d'info
                            $urlAccesDirect = (new Atexo_Consultation())->getUrlAcceeDirect($consultation, Atexo_CurrentUser::getOrganismAcronym());
                        }
                    }
                }
                //bug fix en attendant v1.9 module BOAMP car problème de validation des URLs
                $urlAccesDirect = str_replace('/?page=', '/index.php?page=', $urlAccesDirect);
                $urlEncode = str_replace('&', '&amp;', $urlAccesDirect);
                $blocComplt .= '<url>'.$urlEncode.'</url>';
                $blocComplt .= '</coord>';
                // Obtenir des renseignements d'ordre administratif
                if ($oneType->getIdType() == Atexo_Config::getParameter('ADRESSE_COMPLEMENTAIRES_1')) {
                    $admOuTech .= '<adm>';
                    $admOuTech .= $blocComplt;
                    $admOuTech .= '</adm>';
                }

                // Obtenir des renseignements d'ordre technique
                elseif ($oneType->getIdType() == Atexo_Config::getParameter('ADRESSE_COMPLEMENTAIRES_2')) {
                    $admOuTech .= '<tech>';
                    $admOuTech .= $blocComplt;
                    $admOuTech .= '</tech>';
                }

                /////////////////////////////////////////////////////////////////
                // Obtenir les documents
                elseif ($oneType->getIdType() == Atexo_Config::getParameter('ADRESSE_COMPLEMENTAIRES_3')) {
                    $typeRensA .= '<document>';
                    $typeRensA .= $blocComplt;
                    $typeRensA .= '</document>';
                }

                // Envoyer les offres/candidatures/projets/demandes de participation
                elseif ($oneType->getIdType() == Atexo_Config::getParameter('ADRESSE_COMPLEMENTAIRES_4')) {
                    $typeRensA .= '<envoi>';
                    $typeRensA .= $blocComplt;
                    $typeRensA .= '</envoi>';
                }

                // Obtenir des renseignements complémentaires
                elseif ($oneType->getIdType() == Atexo_Config::getParameter('ADRESSE_COMPLEMENTAIRES_5')) {
                    $typeRensA .= '<rensComplementaires>';
                    $typeRensA .= $blocComplt;
                    $typeRensA .= '</rensComplementaires>';
                }
                /////////////////////////////////////////////////////////////////
                //Commentaire ASO
                /*
                                // Instance chargée des procédures de recours
                                elseif ($oneType->getIdType() == Atexo_Config::getParameter('PROCEDURE_RECOURS_1')) {
                                    $adresseRecours .= "<instanceRecours>";
                                    $adresseRecours .= $blocComplt;
                                    $adresseRecours .= "</instanceRecours>";
                                }


                                // Organe chargé des procédures de médiation
                                elseif ($oneType->getIdType() == Atexo_Config::getParameter('PROCEDURE_RECOURS_2')) {
                                    $adresseRecours .= "<mediation>";
                                    $adresseRecours .= $blocComplt;
                                    $adresseRecours .= "</mediation>";
                                }

                                // Service auprès duquel des renseignements
                                elseif ($oneType->getIdType() == Atexo_Config::getParameter('PROCEDURE_RECOURS_4')) {
                                    $adresseRecours .= "<serviceIntroduction>";
                                    $adresseRecours .= $blocComplt;
                                    $adresseRecours .= "</serviceIntroduction>";
                                }

                                // Introduction des recours
                                elseif ($oneType->getIdType() == Atexo_Config::getParameter('PROCEDURE_RECOURS_3')) {
                                    $adresseRecours .= "<introductionRecours>";
                                    $adresseRecours .= $oneType->getOrganeChargeProcedure();
                                    $adresseRecours .= "</introductionRecours>";
                                }
                            */
                //commentaire ASO pour test
                $blocComplt = '';
            }
            $admOuTech .= '</AdmOuTech>';
            $adressesComplt .= $admOuTech;
            $adressesComplt .= $typeRensA;
            $adressesComplt .= '</adressesComplt>';

            $contents = Atexo_Util::xmlStrReplace($contents, '<!--adressesComplt-->', $adressesComplt);
        //$adresseRecours .= "</adresseRecours>";
            //$contents = Atexo_Util::xmlStrReplace($contents, '<!--adresseRecours-->', $adresseRecours);
        }
        //ancien cas, les informations des adresses ne sont pas complétées
        else {
            $urlAccesDirect = (new Atexo_Consultation())->getUrlAcceeDirect($consultation, Atexo_CurrentUser::getOrganismAcronym());
            if ('' != $urlAccesDirect) {
                $urlAccesDirect = str_replace('/?page=', '/index.php?page=', $urlAccesDirect);
                $urlEncode = str_replace('&', '&amp;', $urlAccesDirect);
            } else {
                $urlEncode = '';
            }
            //commentaire ASO pour test
            $adressesComplt = '';
            $adressesComplt .= '<adressesComplt>
	        						<document>
	        							<coord>
	        								<url>'.$urlEncode.'</url>
	        							</coord>
	        						</document>';
            $adressesComplt .= '<envoi>
		    							<coord>
		    								<url>'.$urlEncode.'</url>
		    							</coord>
		    				   		</envoi>';
            $adressesComplt .= '</adressesComplt>';

            $contents = Atexo_Util::xmlStrReplace($contents, '<!--adressesComplt-->', $adressesComplt);
        }

        return $contents;
    }

    public function generateXmlContexte($consultation, $uidPFDouble = false)
    {
        $serviceObj = null;
        $organismeObj = null;
        $consultationId = !empty($consultation->getReference()) ? $consultation->getReference() : $consultation->getId();
        ($uidPFDouble) ? $uidPF = $uidPFDouble : $uidPF = Atexo_Config::getParameter('UID_PF_MPE');
        $uidPF .= '#'.Atexo_CurrentUser::getOrganismAcronym();
        $achteurPublic = (new Atexo_PublicPurchaser())->retrievePublicPurchasersById($consultation->getCompteBoampAssocie());
        $uidPF .= '#'.$achteurPublic->getBoampLogin();
        $filePath = Atexo_Config::getParameter('CHEMIN_MODEL_INITIALISATION_XML_CONTEXTE_MONITEUR');
        if (Atexo_CurrentUser::getCurrentServiceId()) {
            $serviceObj = Atexo_EntityPurchase::retrieveEntityById(
                Atexo_CurrentUser::getCurrentServiceId(),
                Atexo_CurrentUser::getOrganismAcronym(),
                true
            );
        } else {
            $organismeObj = Atexo_Organismes::retrieveOrganismeByAcronyme(Atexo_CurrentUser::getOrganismAcronym());
        }
        $agentObj = (new Atexo_Agent())->retrieveAgent(Atexo_CurrentUser::getIdAgentConnected());
        if (!$agentObj) {
            $this->response->redirect('?page=Agent.AgentHome');
        }

        $handle = fopen($filePath, 'r');
        $contents = fread($handle, filesize($filePath));
        fclose($handle);
        $contents = Atexo_Util::xmlStrReplace($contents, 'CONST_DATE', (date('d').'/'.date('m').'/'.(date('Y') + 1).' '.date('H:i:s')));
        $contents = Atexo_Util::xmlStrReplace($contents, 'CONST_LOGIN', $uidPF.'##'.Atexo_CurrentUser::getIdAgentConnected());
        $contents = Atexo_Util::xmlStrReplace($contents, 'CONST_PWD', $uidPF.'##'.Atexo_CurrentUser::getIdAgentConnected());
        $contents = Atexo_Util::xmlStrReplace($contents, 'CONST_UID_CONSULATATION', $uidPF.'##'.$consultationId);
        $contents = Atexo_Util::xmlStrReplace($contents, 'CONST_NOM_AGENT', Atexo_Util::escapeXmlChars($agentObj->getNom()));
        $contents = Atexo_Util::xmlStrReplace($contents, 'CONST_PRENOM_AGENT', Atexo_Util::escapeXmlChars($agentObj->getPrenom()));
        $contents = Atexo_Util::xmlStrReplace($contents, 'CONST_EMAIL_AGENT', $agentObj->getEmail());
        if ($agentObj->getNumTel()) {
            $contents = Atexo_Util::xmlStrReplace($contents, 'CONST_TEL_AGENT', $agentObj->getNumTel());
        } else {
            $contents = Atexo_Util::xmlStrReplace($contents, 'CONST_TEL_AGENT', '0000000000');
        }

        $contents = Atexo_Util::xmlStrReplace($contents, 'CONST_IDSERVICE', Atexo_CurrentUser::getIdServiceAgentConnected());
        $contents = Atexo_Util::xmlStrReplace($contents, 'CONST_ID_ORGANISME', Atexo_CurrentUser::getOrganismAcronym());
        
        if ($serviceObj) {
            $libelleService = $serviceObj->getLibelle();
            $adresseService = $serviceObj->getAdresse();
            $adresseSuiteService = $serviceObj->getAdresseSuite();
            $cpService = $serviceObj->getCp();
            $serviceVille = $serviceObj->getVille();
            $serviceSiren = $serviceObj->getSiren();
        } else {
            $libelleService = $organismeObj->getDenominationOrg();
            $adresseService = $organismeObj->getAdresse();
            $adresseSuiteService = $organismeObj->getAdresse2();
            $cpService = $organismeObj->getCp();
            $serviceVille = $organismeObj->getVille();
            $serviceSiren = $organismeObj->getSiren();
        }

        if ($libelleService) {
            $contents = Atexo_Util::xmlStrReplace($contents, 'CONST_LIBELLE_SERVICE', Atexo_Util::escapeXmlChars($libelleService));
        } else {
            $contents = Atexo_Util::xmlStrReplace($contents, 'CONST_LIBELLE_SERVICE', 'A RENSEIGNER');
        }
        if ($adresseService) {
            $contents = Atexo_Util::xmlStrReplace($contents, 'CONST_ADRESSE_SERVICE', Atexo_Util::escapeXmlChars($adresseService));
        } else {
            $contents = Atexo_Util::xmlStrReplace($contents, 'CONST_ADRESSE_SERVICE', 'A RENSEIGNER');
        }
        if ($adresseSuiteService) {
            $contents = Atexo_Util::xmlStrReplace($contents, 'CONST_ADRESSE2_SERVICE', Atexo_Util::escapeXmlChars($adresseSuiteService));
        } else {
            $contents = Atexo_Util::xmlStrReplace($contents, 'CONST_ADRESSE2_SERVICE', 'A RENSEIGNER');
        }
        if ($cpService) {
            $contents = Atexo_Util::xmlStrReplace($contents, 'CONST_CP_SERVICE', $cpService);
        } else {
            $contents = Atexo_Util::xmlStrReplace($contents, 'CONST_CP_SERVICE', '00000');
        }
        if ($serviceVille) {
            $contents = Atexo_Util::xmlStrReplace($contents, 'CONST_VILLE_SERVICE', Atexo_Util::escapeXmlChars(ucfirst(strtolower($serviceVille))));
        } else {
            $contents = Atexo_Util::xmlStrReplace($contents, 'CONST_VILLE_SERVICE', 'A RENSEIGNER');
        }
        if ($serviceSiren) {
            $contents = Atexo_Util::xmlStrReplace($contents, 'CONST_SIRET_SERVICE', $serviceSiren);
        } else {
            $contents = Atexo_Util::xmlStrReplace($contents, 'CONST_SIRET_SERVICE', '00000000000000');
        }
        $contents = Atexo_Util::xmlStrReplace($contents, 'CONST_REF_USER', $consultation->getReferenceUtilisateur());
        $contents = Atexo_Util::xmlStrReplace($contents, 'CONST_TITRE_CONS', Atexo_Util::escapeXmlChars($consultation->getIntitule()));
        $contents = Atexo_Util::xmlStrReplace($contents, 'CONST_UID_PF', Atexo_Config::getParameter('UID_PF_MPE'));
        //adresse acces direct consultation

        $urlAccesDirect = (new Atexo_Consultation())->getUrlAcceeDirect($consultation, Atexo_CurrentUser::getOrganismAcronym());
        /* if($urlAccesDirect!='') {
             $urlAccesDirect = str_replace('/?page=','/index.php?page=',$urlAccesDirect);
             $urlEncode = str_replace('&','&amp;',$urlAccesDirect);
         }*/
        $contents = Atexo_Util::xmlStrReplace($contents, 'CONST_URL_ACCES_DIRECT_CONS', $urlAccesDirect);

        if (Atexo_CurrentUser::hasHabilitation('EnvoyerPublicite')) {
            $valWkf = '1';
        } else {
            $valWkf = '0';
        }
        $contents = Atexo_Util::xmlStrReplace($contents, 'CONST_HABILITATION', $valWkf);
        $contents = Atexo_Util::xmlStrReplace($contents, 'CONST_THEME_GRAPHIQUE', Atexo_Config::getParameter('THEME_GRAPHIQUE'));
        if (!$organismeObj instanceof CommonOrganisme) {
            $organismeObj = Atexo_Organismes::retrieveOrganismeByAcronyme(Atexo_CurrentUser::getOrganismAcronym());
        }
        if ('' != $achteurPublic->getMoniteurProvenance()) {
            $contents = Atexo_Util::xmlStrReplace($contents, 'CONST_PROV', $achteurPublic->getMoniteurProvenance());
        } else {
            $contents = Atexo_Util::xmlStrReplace($contents, 'CONST_PROV', Atexo_Config::getParameter('MONITEUR_PROV'));
        }
        $contents = Atexo_Util::xmlStrReplace($contents, 'CONST_ORGANISME', Atexo_CurrentUser::getOrganismAcronym());
        if ($consultation->getMarchePublicSimplifie()) {
            $contents = Atexo_Util::xmlStrReplace($contents, 'CONST_MPS', '<BUSINESS><MPS/></BUSINESS>');
        } else {
            $contents = Atexo_Util::xmlStrReplace($contents, 'CONST_MPS', '');
        }

        $contents = utf8_decode($contents);

        $dom = new DomDocument();
        $dom->loadXML($contents);
        $contents = $dom->saveXML();

        return $contents;
    }

    /**
     * Permet de générer l'xml des données complémentaires de la consultation.
     *
     * @param $consultation: Objet consultation
     *  @param $contents
     */
    public function genererXmlDonneesComplementairesConsultation($consultation, &$contents, $lotsHasVariantes)
    {
        if (Atexo_Module::isEnabled('DonneesRedac')) {
            //Critères d'attribution de la consultation
            $xmlCriteres = self::getXmlCriteresAttribution($consultation);
            $contents = Atexo_Util::xmlStrReplace($contents, '<!--criteresAttribution-->', $xmlCriteres);

            //Nombre de candidats admis à présenter une offre
            $xmlNbrCandidats = self::getXmlNbrCandidats($consultation);
            $contents = Atexo_Util::xmlStrReplace($contents, '<!--nbrCandidats-->', $xmlNbrCandidats);

            //Délai de validité des offres
            $xmlDelaiValiditeOffres = self::getXmlDelaiValiditeOffres($consultation);
            $contents = Atexo_Util::xmlStrReplace($contents, '<!--delaiValiditeOffres-->', $xmlDelaiValiditeOffres);

            //Clauses sociales et environnementales
            $xmlClausesSocialesEnv = self::getXmlClausesSocialesEnv($consultation);
            $contents = Atexo_Util::xmlStrReplace($contents, '<!--clausesSocialesEnvironnementales-->', $xmlClausesSocialesEnv);

            //Variantes
            $xmlVariantes = self::getXmlVariantes($consultation, Atexo_Util::escapeXmlChars($consultation->getIntitule()), $lotsHasVariantes);
            $contents = Atexo_Util::xmlStrReplace($contents, '<!--variantes-->', $xmlVariantes);

            if ('0' == $consultation->getAlloti()) {
                self::genererXmlAutresDonneesComplementairesConsultationLots($consultation, $contents);
            }
        }
    }

    /**
     * Permet de générer l'xml des autres des autres données complémentaires pour la consultation et pour les lots.
     *
     * @param $consultation: Objet consultation
     * @param $contents
     */
    public function genererXmlAutresDonneesComplementairesConsultationLots($consultation, &$contents = null)
    {
        if (Atexo_Module::isEnabled('DonneesRedac')) {
            $xmlAutresDonneesComplementaires = '';
            //Durée du marchée
            $xmlDuree = self::getXmlDureeMarche($consultation);
            if ($contents) {
                $contents = Atexo_Util::xmlStrReplace($contents, '<!--dureeMarche-->', $xmlDuree);
            }
            $xmlAutresDonneesComplementaires .= $xmlDuree;

            //Modalités de reconduction
            $xmlModalitesReconduc = self::getXmlModalitesReconduction($consultation);
            if ($contents) {
                $contents = Atexo_Util::xmlStrReplace($contents, '<!--modalitesReconduction-->', $xmlModalitesReconduc);
            }
            $xmlAutresDonneesComplementaires .= $xmlModalitesReconduc;

            return $xmlAutresDonneesComplementaires;
        }
    }

    /**
     * Permet de générer l'xml des critères d'attribution.
     *
     * @param $objet: Objet consultation ou lot
     */
    public function getXmlCriteresAttribution($objet)
    {
        $xmlCritere = '';
        if ($objet->getDonneComplementaire() instanceof CommonTDonneeComplementaire && '0' != $objet->getDonneComplementaire()->getIdCritereAttribution()) {
            $refCriteresAttribution = (new Atexo_CommonReferentiel())->retrieveValeurReferentiel(Atexo_Config::getParameter('REFERENTIEL_CRITERE_ATTRIBUTION'));
            if (is_array($refCriteresAttribution) && count($refCriteresAttribution)) {
                foreach ($refCriteresAttribution as $oneRefCritere) {
                    if ($oneRefCritere->getId() == $objet->getDonneComplementaire()->getIdCritereAttribution()) {
                        $xmlCritere .= self::construireXmlCriteresAttribution($oneRefCritere->getLibelle2(), $objet);
                    }
                }
            }
        }

        return $xmlCritere;
    }

    /**
     * Permet construire la chaine xml des critères d'attribution.
     *
     * @param $libelleCritereRef
     * @param $objet: consultation ou lot
     */
    public function construireXmlCriteresAttribution($libelleCritereRef, $objet)
    {
        $xmlCritere = null;
        $criteresAttr = $objet->getDonneComplementaire()->getAllCriteresAttributions();
        $xmlCritere .= '<criteres>';
        if (is_array($criteresAttr) && count($criteresAttr)) {
            foreach ($criteresAttr as $unCritere) {
                if ($unCritere instanceof CommonTCritereAttribution) {
                    if ('layerDefinitionCriteres_1' == $libelleCritereRef) {//Critères énoncés ci après (par ordre de priorité décroissante)
                        $xmlCritere .= '<critereLibre>'.$unCritere->getEnonce().'</critereLibre>';
                    } elseif ('layerDefinitionCriteres_2' == $libelleCritereRef) {//Critères énoncés ci après avec leur pondération
                        $xmlCritere .= '<criterePondere>';
                        $xmlCritere .= '<criterePCT>'.$unCritere->getPonderation().'</criterePCT>';
                        $xmlCritere .= '<critere>'.$unCritere->getEnonce().'</critere>';
                        $xmlCritere .= '</criterePondere>';
                    }
                }
            }
        }
        if ('layerDefinitionVide' == $libelleCritereRef) {//Critère unique du prix le plus bas
            $xmlCritere .= '<criterePrix />';
        }
        if ('layerDefinitionVide_2' == $libelleCritereRef) {//Critères énoncés dans le cahier des charges
            $xmlCritere .= '<critereCDC />';
        }
        $xmlCritere .= '</criteres>';

        return $xmlCritere;
    }

    /**
     * Permet de verifier si consultation avec variante ou non.
     *
     * @param $consultation: consultation
     * @param $varianteLots: si au moins variantes selectionnées dans un lot
     */
    public function isConsWithVariante($consultation, $varianteLots)
    {
        if (self::objetHasVariante($consultation)) {
            return true;
        } else {
            if ('1' == $consultation->getAlloti()) {
                if ($varianteLots) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        }
    }

    /**
     * Permet de verifier si variante selectionnée au niveau de la consultation ou au niveau des lots.
     *
     * @param $objet: consultation ou lot
     */
    public function objetHasVariante($objet)
    {
        if (($objet->getDonneComplementaire() instanceof CommonTDonneeComplementaire)
                        && $objet->getDonneComplementaire()->getVariantesAutorisees()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Permet de générer l'xml des variantes.
     *
     * @param $objet: consultation ou lots
     * @param $intituleCons: intitulé de la consultation
     */
    public function getXmlVariantes($objet, $intituleCons, $varianteLots = false)
    {
        $xmlVariantes = null;
        if ($objet->getDonneComplementaire() instanceof CommonTDonneeComplementaire) {
            $xmlVariantes = '<caracteristiques>';
            if (self::isConsWithVariante($objet, $varianteLots)) {//Variantes selectionnées
                $varianteOuiNon = '<OUI />';
            } else {//Variantes non selectionnées
                $varianteOuiNon = '<NON />';
            }
            $xmlVariantes .= '<variantes>';
            $xmlVariantes .= $varianteOuiNon;
            $xmlVariantes .= '</variantes>';
            $xmlVariantes .= '<principales>'.$intituleCons.'</principales>';

            $xmlVariantes .= '</caracteristiques>';
        }

        return $xmlVariantes;
    }

    /**
     * Permet de générer l'xml de la durée du marchée.
     *
     * @param $objet: objet consultation ou lot
     */
    public function getXmlDureeMarche($objet)
    {
        $xmlDureeMarche = '';
        $xmlOuvertureBaliseDuree = '';
        $xmlFermetureBaliseDuree = '';
        if ($objet instanceof CommonConsultation) {
            $xmlOuvertureBaliseDuree .= '<duree>';
            $xmlFermetureBaliseDuree .= '</duree>';
        } elseif ($objet instanceof CommonCategorieLot) {
            $xmlOuvertureBaliseDuree .= '<dureeLot>';
            $xmlFermetureBaliseDuree .= '</dureeLot>';
        }
        if ($objet->getDonneComplementaire() instanceof CommonTDonneeComplementaire && $objet->getDonneComplementaire()->getIdDureeDelaiDescription()) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            $refDureeDelai = CommonValeurReferentielPeer::retrieveByPK($objet->getDonneComplementaire()->getIdDureeDelaiDescription(), Atexo_Config::getParameter('REFERENTIEL_DUREE_DELAI'), $connexion);
            if ($refDureeDelai instanceof CommonValeurReferentiel) {
                if ('DUREE_MARCHEE' == $refDureeDelai->getLibelle2()) {
                    if ('0' != $objet->getDonneComplementaire()->getIdDureeDelaiDescription()) {
                        $refDureeMarche = (new Atexo_CommonReferentiel())->retrieveValeurReferentiel(Atexo_Config::getParameter('REFERENTIEL_UNITE_DUREE_MARCHE'));
                        if (is_array($refDureeMarche) && count($refDureeMarche)) {
                            $xmlDureeMarche .= $xmlOuvertureBaliseDuree;
                            foreach ($refDureeMarche as $oneRefDureeMarche) {
                                if ($oneRefDureeMarche->getId() == $objet->getDonneComplementaire()->getIdChoixMoisJour()) {
                                    if ('dureeJours' == $oneRefDureeMarche->getLibelle2()) {
                                        $xmlDureeMarche .= '<nbjours>'.$objet->getDonneComplementaire()->getDureeMarche().'</nbjours>';
                                    } elseif ('dureeMois' == $oneRefDureeMarche->getLibelle2()) {
                                        $xmlDureeMarche .= '<nbmois>'.$objet->getDonneComplementaire()->getDureeMarche().'</nbmois>';
                                    }
                                }
                            }
                            $xmlDureeMarche .= $xmlFermetureBaliseDuree;
                        }
                    }
                } elseif ('DELAI_EXECUTION' == $refDureeDelai->getLibelle2()) {
                    $xmlDureeMarche .= $xmlOuvertureBaliseDuree;
                    $xmlDureeMarche .= '<dateDebutPrestation>'.Atexo_Util::iso2frnDate($objet->getDonneComplementaire()->getDureeDateDebut()).'</dateDebutPrestation>';
                    $xmlDureeMarche .= '<dateDebutTravaux>'.Atexo_Util::iso2frnDate($objet->getDonneComplementaire()->getDureeDateFin()).'</dateDebutTravaux>';
                    $xmlDureeMarche .= $xmlFermetureBaliseDuree;
                } elseif ('DESCRIPTION_LIBRE' == $refDureeDelai->getLibelle2()) {
                    $xmlDureeMarche .= $xmlOuvertureBaliseDuree;
                    $xmlDureeMarche .= '<txtLibre>'.$objet->getDonneComplementaire()->getDureeDescription().'</txtLibre>';
                    $xmlDureeMarche .= $xmlFermetureBaliseDuree;
                }
            }
        }

        return $xmlDureeMarche;
    }

    /**
     * Permet de générer l'xml du nombre de candidats admis.
     *
     * @param $consultation: objet consultation
     */
    public function getXmlNbrCandidats($consultation)
    {
        $xmlNbrCandidats = '';
        $nbrCandidatsMin = '';
        $nbrCandidatsMax = '';
        $xmlProcedure = '';
        $nbrCandidatsChoisis = false;
        if ($consultation->getDonneComplementaire() instanceof CommonTDonneeComplementaire) {
            if ($consultation->getDonneComplementaire()->getIdNbCandidatsAdmis() == Atexo_Config::getParameter('ID_NB_CANDIDATS_ADMIS_FIXE')) {
                $nbrCandidatsMin = $consultation->getDonneComplementaire()->getNombreCandidatsFixe();
                $nbrCandidatsMax = $consultation->getDonneComplementaire()->getNombreCandidatsFixe();
                $nbrCandidatsChoisis = true;
            } elseif ($consultation->getDonneComplementaire()->getIdNbCandidatsAdmis() == Atexo_Config::getParameter('ID_NB_CANDIDATS_ADMIS_FOURCHETTE')) {
                $nbrCandidatsMin = $consultation->getDonneComplementaire()->getNombreCandidatsMin();
                $nbrCandidatsMax = $consultation->getDonneComplementaire()->getNombreCandidatsMax();
                $nbrCandidatsChoisis = true;
            }
            if ($nbrCandidatsChoisis) {
                $xmlNbrCandidats .= '<nombreMax>'.$nbrCandidatsMax.'</nombreMax>';
                $xmlNbrCandidats .= '<nombreMin>'.$nbrCandidatsMin.'</nombreMin>';
            }
            if ($consultation->getDonneComplementaire()->getPhaseSuccessive()) {
                $reductionProg = '<OUI/>';
            } else {
                $reductionProg = '<NON/>';
            }
            $xmlProcedure .= '<nbcandidat>';
            $xmlProcedure .= $xmlNbrCandidats;
            $xmlProcedure .= '<reductionProgessive>';
            $xmlProcedure .= $reductionProg;
            $xmlProcedure .= '</reductionProgessive>';
            $xmlProcedure .= '</nbcandidat>';
        }

        return $xmlProcedure;
    }

    /**
     * Permet de générer l'xml du Délai de validité des offres.
     *
     * @param $consultation: objet consultation
     */
    public function getXmlDelaiValiditeOffres($consultation)
    {
        $xmlDelaiValiditeOffres = '';
        if ($consultation instanceof CommonConsultation) {
            if ($consultation->getDonneComplementaire() instanceof CommonTDonneeComplementaire) {
                $xmlDelaiValiditeOffres .= '<validite>';
                $xmlDelaiValiditeOffres .= '<nbmois>'.$consultation->getDonneComplementaire()->getDelaiValiditeOffres().'</nbmois>';
                $xmlDelaiValiditeOffres .= '</validite>';
            }
        }

        return $xmlDelaiValiditeOffres;
    }

    /**
     * Permet de générer l'xml des clauses sociales et environnementales.
     *
     * @param $consultation: objet consultation
     */
    public function getXmlClausesSocialesEnv($consultation)
    {
        $xmlClausesSocialesEnv = '';
        if ($consultation instanceof CommonConsultation) {
            if ($consultation->getClauseSociale() == Atexo_Config::getParameter('CLAUSES_SOCIALES_ACTIVER')) {
                $xmlClausesSocialesEnv .= '<criteresSociaux />';
            }
            if ($consultation->getClauseEnvironnementale() == Atexo_Config::getParameter('CLAUSES_ENVIRONNEMENTALES_ACTIVER')) {
                $xmlClausesSocialesEnv .= '<criteresEnvironnementaux />';
            }
        }

        return $xmlClausesSocialesEnv;
    }

    /*
     * Permet de générer l'xml des modalités de réconduction
     * @param $objet: objet consultation ou categorieLot
     */
    public function getXmlModalitesReconduction($objet)
    {
        $xmlModalitesReconduction = '';
        if ($objet->getDonneComplementaire() instanceof CommonTDonneeComplementaire) {
            if ($objet->getDonneComplementaire()->getReconductible() && $objet->getDonneComplementaire()->getModalitesReconduction()) {
                $xmlModalitesReconduction .= '<infosComplementaires>';
                $xmlModalitesReconduction .= $objet->getDonneComplementaire()->getModalitesReconduction();
                $xmlModalitesReconduction .= '</infosComplementaires>';
            }
        }

        return $xmlModalitesReconduction;
    }
}
