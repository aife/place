<?php

namespace Application\Service\Atexo\Publicite;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonAVIS;
use Application\Propel\Mpe\CommonAVISPeer;
use Application\Propel\Mpe\CommonAVISQuery;
use Application\Propel\Mpe\CommonDCE;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Document;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Rest\Action\Atexo_Rest_Action_Actions;
use Exception;

/**
 * Classe de manipulation des destinataire format libre.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Publicite_Avis
{
    /**
     * retourne la liste des fomrulaires libre d'une consutation.
     */
    public function retreiveListFormulaireLibre($consultationId, $organisme = null)
    {
        if (!$organisme) {
            $organisme = Atexo_CurrentUser::getCurrentOrganism();
        }
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonAVISPeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonAVISPeer::ORGANISME, $organisme);
        $listForm = CommonAVISPeer::doSelect($c, $connexionCom);
        if ($listForm) {
            return $listForm;
        } else {
            return [];
        }
    }

    /**
     * retourne l'avis du fomrulaires libre d'une consutation.
     *
     * @return Avis
     */
    public function retreiveAvisById($idAvis, $connexion, $organisme = null)
    {
        if (!$organisme) {
            $organisme = Atexo_CurrentUser::getCurrentOrganism();
        }
        $avis = CommonAVISPeer::retrieveByPK($idAvis, $organisme, $connexion);
        if ($avis) {
            return $avis;
        } else {
            return false;
        }
    }

    /**
     * retourne la liste des fomrulaires libre d'une consutation.
     */
    public static function retreiveListAvisEnvoye($consultationId, $organisme)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonAVISPeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonAVISPeer::ORGANISME, $organisme);
        $c->add(CommonAVISPeer::STATUT, Atexo_Config::getParameter('DESTINATAIRE_PUBLIE'));
        $c->addDescendingOrderByColumn(CommonAVISPeer::DATE_PUB);
        $listForm = CommonAVISPeer::doSelect($c, $connexionCom);
        if ($listForm) {
            return $listForm;
        } else {
            return [];
        }
    }

    /**
     * retourne l'avis du fomrulaires libre d'une consutation.
     *
     * @return Avis
     */
    public function retreiveAvisByIdAndRefCons($idAvis, $consultationId, $organisme, $connexion)
    {
        $c = new Criteria();
        $c->add(CommonAVISPeer::ID, $idAvis);
        $c->add(CommonAVISPeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonAVISPeer::ORGANISME, $organisme);
        $avis = CommonAVISPeer::doSelectOne($c, $connexion);
        if ($avis) {
            return $avis;
        } else {
            return false;
        }
    }

    /**
     * Supprimer un avis Format libre.
     */
    public function deleteAvis($id)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $criteria = new Criteria();
        $criteria->add(CommonAVISPeer::ORGANISME, Atexo_CurrentUser::getCurrentOrganism());
        $criteria->add(CommonAVISPeer::ID, $id);
        CommonAVISPeer::doDelete($criteria, $connexionCom);
    }

    /**
     * retourne la liste des fomrulaires libre d'une consutation.
     */
    public function retreiveListFormulaireLibreByTypeDocGenere($consultationId, $organisme = null, $typeDocGenere = null, $lang = null, $idAvis = null)
    {
        if (!$organisme) {
            $organisme = Atexo_CurrentUser::getCurrentOrganism();
        }
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        if ($idAvis) {
            $c->add(CommonAVISPeer::ID, $idAvis);
        }
        $c->add(CommonAVISPeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonAVISPeer::ORGANISME, $organisme);
        if ($typeDocGenere) {
            $c->add(CommonAVISPeer::TYPE_DOC_GENERE, $typeDocGenere);
        }
        if ($lang) {
            $c->add(CommonAVISPeer::LANGUE, $lang);
        }
        $listForm = CommonAVISPeer::doSelect($c, $connexionCom);
        if ($listForm) {
            return $listForm;
        } else {
            return [];
        }
    }

    //Récupérer les avis d'une consultation
    public function retreiveListAvis($consultationId, $org, $typeFormat = null, $typeAvisPub = null)
    {
        $connection = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonAVISPeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonAVISPeer::ORGANISME, $org);
        if ($typeFormat) {
            $c->add(CommonAVISPeer::TYPE, $typeFormat);
        }
        if ($typeAvisPub) {
            $c->add(CommonAVISPeer::TYPE_AVIS_PUB, $typeAvisPub);
        }
        $listForm = CommonAVISPeer::doSelect($c, $connection);
        if ($listForm) {
            return $listForm;
        } else {
            return [];
        }
    }

    /**
     * @todo Non utilisé dans le code à supprimer?
     */
    public function retreiveListAvisByType($consultationId, $org)
    {
        $connection = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonAVISPeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonAVISPeer::ORGANISME, $org);
        $c->add(CommonAVISPeer::   TYPE_DOC_GENERE, Atexo_Config::getParameter('TYPE_DOC_GENERE_AVIS'));
        $c->add(CommonAVISPeer::TYPE, Atexo_Config::getParameter('TYPE_FORMAT_LIBRE_FICHIER_JOINT'));
        $listForm = CommonAVISPeer::doSelect($c, $connection);
        if ($listForm) {
            return $listForm;
        } else {
            return [];
        }
    }

    /**
     * Permet de retourner les avis avec le type format libre URL.
     *
     * @param int    $consultationId la reference de la consultation
     * @param string $organisme      l'organisme
     * @param string $url            l'url externe
     *
     * @return array CommonAVIS tableau des avis
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function retreiveAvisFormatLibreUrlByUrl($consultationId, $organisme, $url = null)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonAVISPeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonAVISPeer::ORGANISME, $organisme);
        $c->add(CommonAVISPeer::TYPE, Atexo_Config::getParameter('TYPE_FORMAT_LIBRE_URL'));
        if ($url) {
            $c->add(CommonAVISPeer::URL, $url);
        }
        $listForm = CommonAVISPeer::doSelect($c, $connexionCom);
        if ($listForm) {
            return $listForm;
        } else {
            return false;
        }
    }

    /**
     * Permet de suspendre les avis par consultation et par type.
     *
     * @param int    $consultationId la reference de la consultation
     * @param string $organisme      l'organisme
     * @param int    $type           type avis
     *
     * @return bool
     *
     * @author SDR <salwa.drissi@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2015
     */
    public function suspendreAvisByConsultation($consultationId, $organisme, $type = false, $connexionCom = false)
    {
        try {
            if (!$connexionCom) {
                $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            }

            $c = new Criteria();
            $c->add(CommonAVISPeer::CONSULTATION_ID, $consultationId);
            $c->add(CommonAVISPeer::ORGANISME, $organisme);
            $c->add(CommonAVISPeer::TYPE, $type);
            $listAvis = CommonAVISPeer::doSelect($c, $connexionCom);

            if (is_array($listAvis)) {
                foreach ($listAvis as $avis) {
                    $avis->setStatut(Atexo_Config::getParameter('DESTINATAIRE_SUSPENDU'));
                    $avis->save($connexionCom);
                }
            }

            return true;
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Retourne le dernier avis par consultation et par type.
     *
     * @param int    $consultationId la reference de la consultation
     * @param string $organisme      l'organisme
     * @param int    $type           type avis
     *
     * @return bool / instanceof
     *
     * @author SDR <salwa.drissi@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2015
     */
    public function retreiveAvisPublieByConsultation($consultationId, $organisme, $type = false, $connexionCom = false)
    {
        try {
            if (!$connexionCom) {
                $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            }

            $c = new Criteria();
            $c->add(CommonAVISPeer::CONSULTATION_ID, $consultationId);
            $c->add(CommonAVISPeer::ORGANISME, $organisme);
            $c->add(CommonAVISPeer::TYPE, $type);
            $c->add(CommonAVISPeer::STATUT, Atexo_Config::getParameter('DESTINATAIRE_PUBLIE'));
            $c->addDescendingOrderByColumn(CommonAVISPeer::ID);
            $avis = CommonAVISPeer::doSelect($c, $connexionCom);

            if ($avis) {
                return $avis;
            }

            return false;
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Permet de reourner l'avis by id et type.
     *
     * @param int    $id        l'id de regelemnt
     * @param string $type      type d'avis
     * @param string $statut    statut d'avis
     * @param string $organisme Acronyme de l'organisme
     *
     * @return CommonDCE
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function retrieveAvisPublieById($id, $type = null, $statut = null, $organisme = null)
    {
        $avisQuery = new CommonAVISQuery();

        return $avisQuery->getAvisById($id, $type, $statut, $organisme);
    }

    /**
     * Permet d'ajouter les informations du reglement de la consultation pour le ws.
     *
     * @param $id
     * @param $xmlResponse
     * @param $arrayBlob
     * @param $logger
     * @param $contentType
     * @param string $organisme Acronyme de l'organisme connecte au WS
     *
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function setInfoAvisForWs($id, &$xmlResponse, &$arrayBlob, &$logger, &$contentType, $organisme = null, $typeChecksum = 'md5'): bool|string
    {
        $logger->info("La récupération d'avis de publicité dont l'id = ".$id);
        $avis = self::retrieveAvisPublieById($id, Atexo_Config::getParameter('TYPE_FORMAT_LIBRE_FICHIER_JOINT'), Atexo_Config::getParameter('DESTINATAIRE_PUBLIE'), $organisme);
        if ($avis instanceof CommonAVIS) {
            $logger->info("L'avis de publicité existe");
            $blobInfo = Atexo_Blob::getBlobById($avis->getAvis(), $avis->getOrganisme(), $avis->getNomFichier());
            $arrayBlob = [$blobInfo];
            $logger->info("Mettre les informations de l'avis de publicité dans l'objet Atexo_Rest_ObjectField_DocumentField");
            $documentField = (new Atexo_Document())->getDocumentFieldByInfo($avis->getId(), $avis->getNomFichier(), $avis->getOrganisme(), $avis->getAvis(), $blobInfo['content'], 'AVIS_PUBLICITE', null, null, null, null, null, null, $typeChecksum, $avis->getHorodatage());
            $contentType = Atexo_Util::getContentTypeByExtension(Atexo_Util::getExtension($avis->getNomFichier()));
            $logger->info("La récupération de l'xml contenant les informations de l'avis de publicité ");
            $xmlResponse = (new Atexo_Rest_Action_Actions())->getXmlInfosObjectField($documentField);

            return false;
        } else {
            return "L'avis de publicité dont l'id = ".$id." n'existe pas";
        }
    }
}
