<?php

namespace Application\Service\Atexo\Publicite;

/**
 * Vo pour définir les critères de la fonction d'envoi du ficher xml au BOAMP.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Publicite_FormulaireVo
{
    private string $_xml = '';
    private $_authentificate;
    private string $_urlHost = '';
    private string $_urlSendPath = '';
    private string $_nameFile = '';
    private string $_nameChamp = '';

    private string $_LibelleService = '';
    private string $_Trimestre = '';
    private string $_Annee = '';

    public function getXml()
    {
        return $this->_xml;
    }

    public function setXml($xml)
    {
        $this->_xml = $xml;
    }

    public function getAuthentificate()
    {
        return $this->_authentificate;
    }

    public function setAuthentificate($xml)
    {
        $this->_authentificate = $xml;
    }

    public function getUrlHost()
    {
        return $this->_urlHost;
    }

    public function setUrlHost($urlHost)
    {
        $this->_urlHost = $urlHost;
    }

    public function getUrlSendPath()
    {
        return $this->_urlSendPath;
    }

    public function setUrlSendPath($urlSendPath)
    {
        $this->_urlSendPath = $urlSendPath;
    }

    public function getNameFile()
    {
        return $this->_nameFile;
    }

    public function setNameFile($nameFile)
    {
        $this->_nameFile = $nameFile;
    }

    public function getNameChamp()
    {
        return $this->_nameChamp;
    }

    public function setNameChamp($nameChamp)
    {
        $this->_nameChamp = $nameChamp;
    }

    public function getLibelleService()
    {
        return $this->_LibelleService;
    }

    public function setLibelleService($nameChamp)
    {
        $this->_LibelleService = $nameChamp;
    }

    public function getAnnee()
    {
        return $this->_Annee;
    }

    public function setAnnee($nameChamp)
    {
        $this->_Annee = $nameChamp;
    }

    public function getTrimestre()
    {
        return $this->_Trimestre;
    }

    public function setTrimestre($nameChamp)
    {
        $this->_Trimestre = $nameChamp;
    }
}
