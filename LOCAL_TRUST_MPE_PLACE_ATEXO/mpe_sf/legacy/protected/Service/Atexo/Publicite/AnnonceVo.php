<?php

namespace Application\Service\Atexo\Publicite;

use Application\Propel\Mpe\CommonAnnonceBoamp;

/**
 * Vo pour définir les critères de la fonction d'envoi du ficher xml au BOAMP.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Publicite_AnnonceVo extends CommonAnnonceBoamp
{
    private string $_libelle = '';
    private string $_statut = '';
    private string $_dateEnvoi = '';
    private string $_datePub = '';
    private string $_type = '';
    private string $_id = '';
    private string $_idDestinataireFormXml = '';
    private string $_statutBoamp = '';
    private string $_xml = '';
    private string $_commentaire = '';

    public function getId()
    {
        return $this->_id;
    }

    public function setId($id)
    {
        $this->_id = $id;
    }

    public function getLibelle()
    {
        return $this->_libelle;
    }

    public function setLibelle($libelle)
    {
        $this->_libelle = $libelle;
    }

    public function getStatutDestinataire()
    {
        return $this->_statut;
    }

    public function setStatutDestinataire($statut)
    {
        $this->_statut = $statut;
    }

    public function getDateEnvoiAnnonce()
    {
        return $this->_dateEnvoi;
    }

    public function setDateEnvoiAnnonce($dateEnvoi)
    {
        $this->_dateEnvoi = $dateEnvoi;
    }

    public function getDatePubAnnonce()
    {
        return $this->_datePub;
    }

    public function setDatePubAnnonce($datePub)
    {
        $this->_datePub = $datePub;
    }

    public function getTypeAnnonce()
    {
        return $this->_type;
    }

    public function setTypeAnnonce($type)
    {
        $this->_type = $type;
    }

    public function getIdDestinataireFormXml()
    {
        return $this->_idDestinataireFormXml;
    }

    public function setIdDestinataireFormXml($idDestinataireFormXml)
    {
        $this->_idDestinataireFormXml = $idDestinataireFormXml;
    }

    public function getStatutBoamp()
    {
        return $this->_statutBoamp;
    }

    public function setStatutBoamp($sStatutBoamp)
    {
        $this->_statutBoamp = $sStatutBoamp;
    }

    public function getXml()
    {
        return $this->_xml;
    }

    public function setXml($xml)
    {
        $this->_xml = $xml;
    }

    public function getCommentaire()
    {
        return $this->_commentaire;
    }

    public function setCommentaire($com)
    {
        $this->_commentaire = $com;
    }
}
