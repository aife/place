<?php

namespace Application\Service\Atexo\Publicite;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonAnnonceBoampPeer;
use Application\Propel\Mpe\CommonAnnoncePeer;
use Application\Propel\Mpe\CommonConsultationPeer;
use Application\Propel\Mpe\CommonReferentielFormXmlPeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use DOMDocument;
use Prado\Prado;

/**
 * Classe d'envoi des annonces au boamp et au groupe moniteur.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Publicite_SendAnnonce
{
    public function send($typeAnnonce, $idFormXml, $idAnnonce, $organisme, $refCons)
    {
        if ('BOAMP' == $typeAnnonce) {
            self::boampSend($idFormXml, $idAnnonce, $organisme, $refCons);
        } elseif ('MONITEUR' == $typeAnnonce) {
            self::moniteurSend($idFormXml, $idAnnonce, $organisme, $refCons);
        }
    }

    /**
     * Fonction qui permet d'envoyer le flux au boamp.
     *
     * @param $idFormXml identifiant du formaulaire
     * @param $idAnnonce identifiant de l'annonce
     * @param $organisme acronyme de l'organisme
     * @param  $refCons refenrence de la consultation
     */
    public function boampSend($idFormXml, $idAnnonce, $organisme, $refCons)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $formXml = CommonReferentielFormXmlPeer::retrieveByPK($idFormXml, Atexo_CurrentUser::getCurrentOrganism(), $connexion);
        $acheteurPublic = (new Atexo_Publicite_Boamp())->retreiveAcheteurPublic($formXml->getIdCompteBoamp(), $organisme);
        $formulaireVo = new Atexo_Publicite_FormulaireVo();
        $identifiant = $acheteurPublic->getId();
        $authCode = base64_encode($acheteurPublic->getBoampLogin().':'.$acheteurPublic->getBoampPassword());
        $formulaireVo->setAuthentificate($authCode);
        $nameChamp = 'File';
        $formulaireVo->setNameChamp($nameChamp);
        $fileName = (new Atexo_Publicite_Boamp())->getBoampXmlName($identifiant, $organisme);
        $formulaireVo->setNameFile($fileName);
        $formulaireVo->setUrlHost((new Atexo_Publicite_Boamp())->boampHost());
        $formulaireVo->setUrlSendPath((new Atexo_Publicite_Boamp())->boampSendPath($acheteurPublic));
        $xml = simplexml_load_string($formXml->getXml());
        //Ajout de l'identifiant du formulaire dans le xml avec la reference de la consultation et la reference utilisateur
        $idAnnonceEmetteur = $idAnnonce.'-'.$refCons;
        //Attention on limite à 30 caracteres l'idAnnonce car sinon refus du BOAMP
        $xml->ann->id_AnnEmetteur = substr($idAnnonceEmetteur, 0, 30);
        $newXml = $xml->asXML();
        //Mettre la date du jour de l'envoi dans le flux xml
        $newXml = self::upDateDateEnvoiBoamp($newXml);
        //rajout des CDATA dans les Urls pour ne pas faire planter le BOAMP et suppression des &amp;
        //nous n'avons pas besoin de stocker en base le contenu exact de ce qui est envoyé pour éviter
        //de mettre plusieurs fois le CDATA dans les URLs
        $newXmlToSend = self::setUrlInCdata($newXml);
        if ($newXmlToSend) {
            $formXml->setXml($newXml);
            $formXml->save($connexion);
            $formulaireVo->setXml($newXmlToSend);
        }

        $response = [];
        $response = (new Atexo_Publicite_Boamp())->sendXml($formulaireVo);
        self::updateDestFormXml($idAnnonce, $idFormXml, $response, $organisme, $refCons);
    }

    /**
     * Cette fonction permet de mettre à jour  la date d'envoi.
     */
    public function upDateDateEnvoiBoamp($strIn)
    {
        $dateEnvoi = null;
        $domXml = (new Atexo_Publicite_Boamp())->getXmlDocument($strIn);
        $dateEnvoiElement = $domXml->getElementsByTagName('dateEnvoi');
        if ($dateEnvoiElement) {
            $dateEnvoi = $dateEnvoiElement[0]->nodeValue;
        }
        $dateDuJour = date('Y-m-d');
        $newXml = str_replace('<dateEnvoi>'.$dateEnvoi.'</dateEnvoi>', '<dateEnvoi>'.$dateDuJour.'</dateEnvoi>', $strIn);

        return $newXml;
    }

    /**
     * Cette fonction permet d'encapsuler l'URL
     * d'accès direct dans un CDATA.
     */
    public function setUrlInCdata($strIn)
    {
        $tabUrlTraite = [];
        $domXml = new DOMDocument();
        $domXml->preserveWhiteSpace = false;
        $domXml->loadXML($strIn);
        $domElement = $domXml->getElementsByTagName('url');
        $newXml = $strIn;
        $nombreUrl = $domElement->length;
        for ($i = 0; $i < $nombreUrl; ++$i) {
            $url = $domElement->item($i)->nodeValue;

            //le dom a remplacer &amp; par & dans url, donc on la modifie car dans le flux c toujours avec &amp;
            $urlAvecAmp = str_replace('&', '&amp;', $url);

            //on fait expres de rajouter dans la delimitation > et </ pour ne prendre en compte que les urls,
            // car il ne faut pas supprimer les &amp; qui figure dans des urls que le client saisit dans des blocs de texte
            //sinon cela ne passe pas au BOAMP, il interprete comme des entites
            $newXml = str_replace('>'.$urlAvecAmp.'</', '><![CDATA['.$url.']]></', $newXml);

            $tabUrlTraite[$url] = 1;
        }
        //permet le remplacement des caracteres '&' dans une phrase ex : Atexo & Cie devient Atexo et Cie
        return str_replace(' &amp; ', ' et ', $newXml);
    }

    /**
     * Actualiser le tableau des formulaire Xml
     * (Statut des destinataire si click sur envoyer).
     */
    public function updateDestFormXml($idAnnonce, $idFormXml, $response, $organisme, $refCons)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $annonce = CommonAnnonceBoampPeer::retrieveByPK($idAnnonce, Atexo_CurrentUser::getCurrentOrganism(), $connexionCom);
        $commonAnnonce = CommonAnnoncePeer::retrieveByPK($idAnnonce, $organisme, $connexionCom);
        if ($response[0]) {
            $annonce->setStatutDestinataire(Atexo_Config::getParameter('DESTINATAIRE_ENVOYE'));
            $annonce->setNomFichierXml($response[2]);
            $annonce->setDateEnvoi(date('Y-m-d H:i:s'));
            $commonAnnonce->setNomFichierXml($response[2]);
            $commonAnnonce->setDateEnvoi(date('Y-m-d H:i:s'));
        } else {
            $xmlRetour = strstr($response[1], '<?xml');
            //$xmlRetour vaut false si on a du html en retour

            if (!$xmlRetour) {
                $msg = Prado::localize('MSG_ERREUR_COMPTE_BOAMP').
                          '<a href="'.Atexo_Config::getParameter('PF_URL').'?page=Agent.PopupInfosCreationCompteBOAMP">'.Prado::localize('DEFINE_CLIQUEZ_ICI').'</a>'.
                Prado::localize('MSG_ERREUR_COMPTE_BOAMP_SUITE');
                $annonce->setAnnError($msg);
            } else {
                $xmlObject = simplexml_load_string($xmlRetour);
                $msgErreur = (string) $xmlObject->MESSAGE;
                $annonce->setAnnError(utf8_decode($msgErreur));
            }
            $annonce->setStatutDestinataire(Atexo_Config::getParameter('DESTINATAIRE_ENVOI_ECHOUE'));
        }
        $annonce->save($connexionCom);
        $commonAnnonce->save($connexionCom);
        $CommonConsultation = CommonConsultationPeer::retrieveByPK($refCons, $connexionCom);
        $dateMiseEnligneCalcule = Atexo_Consultation::getDateMiseEnLigne($CommonConsultation);
        $CommonConsultation->setDateMiseEnLigneCalcule($dateMiseEnligneCalcule);
        $CommonConsultation->save($connexionCom);
    }

    /**
     * Fonction d'envoi au groupe moniteur.
     *
     * @param $idFormXml identifiant du formaulaire
     * @param $idAnnonce identifiant de l'annonce
     * @param $organisme acronyme de l'organisme
     */
    public function moniteurSend($idFormXml, $idAnnMoniteur, $organisme, $refCons)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $compteMoniteur = (new Atexo_Publicite_Moniteur())->retrieveCompteMoniteur($organisme);
        $formXml = CommonReferentielFormXmlPeer::retrieveByPK($idFormXml, Atexo_CurrentUser::getCurrentOrganism(), $connexionCom);
        $moniteurHost = (new Atexo_Publicite_Moniteur())->retrieveMoniteurHost($compteMoniteur);
        $auth = (new Atexo_Publicite_Moniteur())->retrieveMoniteurLoginPassword($compteMoniteur);
        $annMoniteur = (new Atexo_Publicite_Destinataire())->retrieveAnnonceMoniteurById($idAnnMoniteur, $organisme);
        $formulaireVo = new Atexo_Publicite_FormulaireMoniteurVo();

        $xmlBoamp = simplexml_load_string($formXml->getXml());
        //Ajout de l'identifiant du formulaire dans le xml avec la reference de la consultation et la reference utilisateur
        $idAnnonceEmetteur = $idAnnMoniteur.'-'.$refCons;
        //Attention on limite à 30 caracteres l'idAnnonce car sinon refus du BOAMP
        $xmlBoamp->ann->id_AnnEmetteur = substr($idAnnonceEmetteur, 0, 30);
        $newXmlBoamp = $xmlBoamp->asXML();
        //Mettre la date du jour de l'envoi dans le flux xml
        $newXmlBoamp = self::upDateDateEnvoiBoamp($newXmlBoamp);
        //rajout des CDATA dans les Urls pour ne pas faire planter le BOAMP et suppression des &amp;
        //nous n'avons pas besoin de stocker en base le contenu exact de ce qui est envoyé pour éviter
        //de mettre plusieurs fois le CDATA dans les URLs
        $newXmlBoampToSend = self::setUrlInCdata($newXmlBoamp);
        $formulaireVo->setAnnonceFile($newXmlBoampToSend);

        $formulaireVo->setAuthentificate($auth);
        $formulaireVo->setNameFile(Atexo_Config::getParameter('DB_PREFIX').'_'.$organisme.'_'.date('YmdHisu').'_annonce.xml');
        $formulaireVo->setUrlHost($moniteurHost);
        $formulaireVo->setUrlSendPath((new Atexo_Publicite_Moniteur())->retrieveMoniteurSendPath($compteMoniteur));

        $formulaireVo->setXml($annMoniteur->getXmlMoniteur());
        $arrayReponse = (new Atexo_Publicite_Moniteur())->sendXmlMoniteur($formulaireVo);
        if (false == $arrayReponse[0]) {
            // Echec d'envoi
            $msgErreur = (new Atexo_Publicite_Moniteur())->parseMoniteurFailureCause($arrayReponse[1]);
            (new Atexo_Publicite_Moniteur())->saveEnvoiMoniteurResponse((utf8_decode($msgErreur)), 'ECHEC', $idAnnMoniteur, $connexionCom);
        } else {
            // Envoi réussi
            $numAnnonce = (new Atexo_Publicite_Moniteur())->parseMoniteurNumeroAnnonce($arrayReponse[1]);
            (new Atexo_Publicite_Moniteur())->saveEnvoiMoniteurResponse($numAnnonce, 'OK', $idAnnMoniteur, $connexionCom);
        }
    }
}
