<?php

namespace Application\Service\Atexo\Publicite;

/**
 * Vo pour définir les critères d'un formulaire libre.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Publicite_FormatLibreVo
{
    private string $_id = '';
    private string $_libelleType = '';
    private $_dateCreation;
    private string $_destinataire = '';
    private string $_statut = '1';
    private string $_dateEnvoi = '';
    private ?string $_datePub = '';
    private string $_accuse = '1';
    private string $_typeFormat = '0';
    private ?string $_url = '';
    private string $_taille = '';
    private string $_type = '';

    //Gestion de l'url libre
    private string $_urlPieceLibre = '';

    public function getId()
    {
        return $this->_id;
    }

    public function setType($type)
    {
        $this->_type = $type;
    }

    public function getType()
    {
        return $this->_type;
    }

    public function setId($id)
    {
        $this->_id = $id;
    }

    public function getLibelleType()
    {
        return $this->_libelleType;
    }

    public function setLibelleType($libelleType)
    {
        $this->_libelleType = $libelleType;
    }

    public function getDateCreation()
    {
        return $this->_dateCreation;
    }

    public function setDateCreation($dateCreation)
    {
        $this->_dateCreation = $dateCreation;
    }

    public function getDestinataire()
    {
        return $this->_destinataire;
    }

    public function setDestinataire($destinataire)
    {
        $this->_destinataire = $destinataire;
    }

    public function getStatut()
    {
        return $this->_statut;
    }

    public function setStatut($statut)
    {
        $this->_statut = $statut;
    }

    public function getDateEnvoi()
    {
        return $this->_dateEnvoi;
    }

    public function setDateEnvoi($dateEnvoi)
    {
        $this->_dateEnvoi = $dateEnvoi;
    }

    public function getDatePub()
    {
        return $this->_datePub;
    }

    public function setDatePub($datePub)
    {
        $this->_datePub = $datePub;
    }

    public function getAccuse()
    {
        return $this->_accuse;
    }

    public function setAccuse($accuse)
    {
        $this->_accuse = $accuse;
    }

    public function getTypeFormat()
    {
        return $this->_typeFormat;
    }

    public function setTypeFormat($typeFormat)
    {
        $this->_typeFormat = $typeFormat;
    }

    public function getUrl()
    {
        return $this->_url;
    }

    public function setUrl($url)
    {
        $this->_url = $url;
    }

    public function getTaille()
    {
        return $this->_taille;
    }

    public function setTaille($taille)
    {
        $this->_taille = $taille;
    }

    public function getUrlPieceLibre()
    {
        return $this->_urlPieceLibre;
    }

    public function setUrlPieceLibre($urlPieceLibre)
    {
        $this->_urlPieceLibre = $urlPieceLibre;
    }
}
