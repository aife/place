<?php

namespace Application\Service\Atexo\Publicite;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonAnnonce;
use Application\Propel\Mpe\CommonAnnonceBoamp;
use Application\Propel\Mpe\CommonAnnonceBoampPeer;
use Application\Propel\Mpe\CommonAnnonceMoniteur;
use Application\Propel\Mpe\CommonAnnonceMoniteurPeer;
use Application\Propel\Mpe\CommonOrganisme;
use Application\Propel\Mpe\CommonReferentielDestinationFormXmlPeer;
use Application\Propel\Mpe\CommonReferentielFormXmlPeer;
use Application\Propel\Mpe\CommonReferentielTypeXmlPeer;
use Application\Propel\Mpe\CommonService;
use Application\Propel\Mpe\CommonServicePeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Util;

/**
 * Classe de manipulation des destinataire.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Publicite_Destinataire
{
    /**
     * retourne la liste des destinataires.
     */
    public function retreiveDestinataireType()
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonReferentielDestinationFormXmlPeer::ORGANISME, Atexo_CurrentUser::getCurrentOrganism());
        $lisDestType = CommonReferentielDestinationFormXmlPeer::doSelect($c, $connexion);
        if ($lisDestType) {
            return $lisDestType;
        } else {
            return [];
        }
    }

    /**
     * retourne la liste des destinataires identifié par l'id formulaire xml.
     */
    public function retreiveListDestinataire($idFormXml)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonAnnonceBoampPeer::ID_FORM_XML, $idFormXml);
        $c->add(CommonAnnonceBoampPeer::ORGANISME, Atexo_CurrentUser::getCurrentOrganism());
        //$c->addJoin(CommonAnnonceBoampPeer::ORGANISME, CommonReferentielDestinationFormXmlPeer::ORGANISME);
        $listDest = CommonAnnonceBoampPeer::doSelectJoinCommonReferentielDestinationFormXml($c, $connexion, Atexo_CurrentUser::getCurrentOrganism());
        if ($listDest) {
            return $listDest;
        } else {
            return [];
        }
    }

    /**
     * @return Annonce
     */
    public function retrieveDestinataireById($idBoamp, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonAnnonceBoampPeer::ID_BOAMP, $idBoamp);
        $c->add(CommonAnnonceBoampPeer::ORGANISME, $organisme);

        return CommonAnnonceBoampPeer::doSelectOne($c, $connexion);
    }

    public function retrieveAnnonceByFileName($fileName, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonAnnonceBoampPeer::NOM_FICHIER_XML, '%_'.$fileName, Criteria::LIKE);
        $c->add(CommonAnnonceBoampPeer::ORGANISME, $organisme);

        return CommonAnnonceBoampPeer::doSelectOne($c, $connexion);
    }

    public function retrieveAnnonceByFileNameAndRefConsultataion($consultationId, $fileName, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonAnnonceBoampPeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonAnnonceBoampPeer::NOM_FICHIER_XML, '%_'.$fileName, Criteria::LIKE);
        $c->add(CommonAnnonceBoampPeer::ORGANISME, $organisme);

        return CommonAnnonceBoampPeer::doSelectOne($c, $connexion);
    }

    /**
     * supprime un destinataire.
     */
    public function deleteOneDestinataire($id)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $criteria = new Criteria();
        $criteria->add(CommonAnnonceBoampPeer::ID_BOAMP, $id);
        $criteria->add(CommonAnnonceBoampPeer::ORGANISME, Atexo_CurrentUser::getCurrentOrganism());
        CommonAnnonceBoampPeer::doDelete($criteria, $connexion);
    }

    /**
     * retourne la liste des destinataire corrspendont à un type de formulaire XML.
     */
    public function retreiveListeDestinataireByIdFormXml($idFormXml)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonAnnonceBoampPeer::ID_FORM_XML, $idFormXml);
        $c->add(CommonAnnonceBoampPeer::ORGANISME, Atexo_CurrentUser::getCurrentOrganism());
        $form = CommonAnnonceBoampPeer::doSelect($c, $connexion);
        if ($form) {
            return $form;
        } else {
            return [];
        }
    }

    public function deleteAllDestinataireForOneTypeForm($idFormXml)
    {
        $dest = self::retreiveListeDestinataireByIdFormXml($idFormXml);
        if (is_array($dest) && count($dest)) {
            foreach ($dest as $oneDest) {
                self::deleteOneDestinataire($oneDest->getIdBoamp());
            }
        }
        $destMoniteur = self::retreiveListeDestinataireMoniteurByIdFormXml($idFormXml);
        if (is_array($destMoniteur) && count($destMoniteur)) {
            foreach ($destMoniteur as $oneDest) {
                self::deleteOneAnnonceMoniteur($oneDest->getId());
            }
        }
    }

    public static function retrieveAnnonceByRefCons($consultationId, $organisme)
    {
        $c = new Criteria();
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonAnnonceBoampPeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonAnnonceBoampPeer::ORGANISME, $organisme);
        $annonces = CommonAnnonceBoampPeer::doSelect($c, $connexion);
        if ('' != $annonces) {
            return $annonces;
        }

        return false;
    }

    /**
     * retourne la liste des destinataires identifié par l'id formulaire xml.
     */
    public function retreiveListDestinataireMoniteur($idFormXml, $connexion = null)
    {
        if (null == $connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
        $c = new Criteria();
        $c->add(CommonAnnonceMoniteurPeer::ID_FORM_XML, $idFormXml);
        $c->add(CommonAnnonceMoniteurPeer::ORGANISME, Atexo_CurrentUser::getCurrentOrganism());
        //$c->addJoin(CommonAnnonceMoniteurPeer::ORGANISME, CommonReferentielDestinationFormXmlPeer::ORGANISME);
        $listDest = CommonAnnonceMoniteurPeer::doSelectJoinCommonReferentielDestinationFormXml($c, $connexion, Atexo_CurrentUser::getCurrentOrganism());
        if ($listDest) {
            return $listDest;
        } else {
            return [];
        }
    }

    public function retreiveListeDestinataireMoniteurByIdFormXml($idFormXml)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $c = new Criteria();
        $c->add(CommonAnnonceMoniteurPeer::ID_FORM_XML, $idFormXml);
        $c->add(CommonAnnonceMoniteurPeer::ORGANISME, Atexo_CurrentUser::getCurrentOrganism());
        $form = CommonAnnonceMoniteurPeer::doSelect($c, $connexion);
        if ($form) {
            return $form;
        } else {
            return [];
        }
    }

    /**
     * supprime un destinataire.
     */
    public function deleteOneAnnonceMoniteur($id)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $criteria = new Criteria();
        $criteria->add(CommonAnnonceMoniteurPeer::ID, $id);
        $criteria->add(CommonAnnonceMoniteurPeer::ORGANISME, Atexo_CurrentUser::getCurrentOrganism());
        CommonAnnonceMoniteurPeer::doDelete($criteria, $connexion);
    }

    /**
     * @return Annoncemoniteur
     */
    public function retrieveAnnonceMoniteurById($idAnn, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonAnnonceMoniteurPeer::ID, $idAnn);
        $c->add(CommonAnnonceMoniteurPeer::ORGANISME, $organisme);

        return CommonAnnonceMoniteurPeer::doSelectOne($c, $connexion);
    }

    /**
     * retourne un objet AnnonceMoniteur identifié par un idMoniteur.
     */
    public function retreiveAnnonceMoniteurByNumAnnonce($numAnnonce, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonAnnonceMoniteurPeer::NUM_ANNONCE, $numAnnonce);
        $c->add(CommonAnnonceMoniteurPeer::ORGANISME, $organisme);

        return CommonAnnonceMoniteurPeer::doSelectOne($c, $connexion);
    }

    public function retreiveAnnonceWithFormXml($consultationId, $organisme, $idTypeAvis = null)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        if ($idTypeAvis) {
            $c->add(CommonAnnonceBoampPeer::ID_TYPE_XML, $idTypeAvis);
        }
        $c->add(CommonAnnonceBoampPeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonAnnonceBoampPeer::ORGANISME, $organisme);
        $criterion = $c->getNewCriterion(CommonAnnonceBoampPeer::STATUT_DESTINATAIRE, Atexo_Config::getParameter('DESTINATAIRE_STATUT_CP'));
        $criterion2 = $c->getNewCriterion(CommonAnnonceBoampPeer::STATUT_DESTINATAIRE, Atexo_Config::getParameter('DESTINATAIRE_STATUT_AV'));
        $criterion3 = $c->getNewCriterion(CommonAnnonceBoampPeer::STATUT_DESTINATAIRE, Atexo_Config::getParameter('DESTINATAIRE_STATUT_PU'));
        $criterion->addOr($criterion2);
        $criterion->addOr($criterion3);
        $c->addAscendingOrderByColumn(CommonAnnonceBoampPeer::DATE_ENVOI);
        $c->addAnd($criterion);
        $annonceForm = CommonAnnonceBoampPeer::doSelect($c, $connexion);
        if ('' != $annonceForm) {
            return $annonceForm;
        } else {
            return false;
        }
    }

    /* retourne un objet AnnonceMoniteur identifié identifiant du fomulaire xml
    *
    */
    public function retreiveAnnonceMoniteurByIdFormXml($idFormXml, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonAnnonceMoniteurPeer::ID_FORM_XML, $idFormXml);
        $c->add(CommonAnnonceMoniteurPeer::ORGANISME, $organisme);
        $criterion = $c->getNewCriterion(CommonAnnonceMoniteurPeer::STATUT_DESTINATAIRE, Atexo_Config::getParameter('STATUT_MONITEUR_PUBLIE'));
        $criterion2 = $c->getNewCriterion(CommonAnnonceMoniteurPeer::STATUT_DESTINATAIRE, Atexo_Config::getParameter('STATUT_MONITEUR_EN_ATTENTE_REEMISSION'));
        $criterion->addOr($criterion2);
        $c->addAnd($criterion);

        return CommonAnnonceMoniteurPeer::doSelectOne($c, $connexion);
    }

    /**
     * retourne la liste des annonces conforme et validé et en attente d'envoi.
     */
    public function retreiveAnnoncesBoampEnAttente($organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonAnnonceBoampPeer::DATE_ENVOI, '0000-00-00 00:00:00');
        $c->add(CommonAnnonceBoampPeer::ORGANISME, $organisme);
        $c->add(CommonAnnonceBoampPeer::STATUT_DESTINATAIRE, Atexo_Config::getParameter('DESTINATAIRE_EN_ATTENTE'));
        $c->add(CommonReferentielFormXmlPeer::STATUT, Atexo_Config::getParameter('ANNONCE_CONFORME_ET_VALIDITE'));
        $c->add(CommonReferentielFormXmlPeer::ID_COMPTE_BOAMP, '0', Criteria::NOT_EQUAL);
//        $c->addJoin(CommonAnnonceBoampPeer::ORGANISME, CommonReferentielFormXmlPeer::ORGANISME);
        $listDest = (new CommonAnnonceBoampPeer())->doSelectJoinCommonReferentielformxml($c, $connexion);
        if ($listDest) {
            return $listDest;
        } else {
            return [];
        }
    }

    /**
     * retourne la liste des annonces  en attente d'envoi au groupe moniteur.
     */
    public function retreiveAnnoncesMoniteurEnAttente($organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonAnnonceMoniteurPeer::DATE_ENVOI, '0000-00-00 00:00:00');
        $c->add(CommonAnnonceMoniteurPeer::ORGANISME, $organisme);
        $c->add(CommonAnnonceMoniteurPeer::DATE_ENVOI, '0000-00-00');
        $c->add(CommonAnnonceMoniteurPeer::STATUT_DESTINATAIRE, Atexo_Config::getParameter('STATUT_MONITEUR_EN_ATTENTE'));
        $c->add(CommonReferentielFormXmlPeer::STATUT, Atexo_Config::getParameter('ANNONCE_CONFORME_ET_VALIDITE'));
//        $c->addJoin(CommonAnnonceMoniteurPeer::ORGANISME, CommonReferentielFormXmlPeer::ORGANISME);
        $listDest = CommonAnnonceMoniteurPeer::doSelectJoinCommonReferentielformxml($c, $connexion);
        if ($listDest) {
            return $listDest;
        } else {
            return [];
        }
    }

    /**
     * retourne la liste des annonces envoyées au moniteur pour lesquelles on attend un retour.
     */
    public function retreiveAnnoncesMoniteurEtatEmis($organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonAnnonceMoniteurPeer::STATUT_DESTINATAIRE, Atexo_Config::getParameter('STATUT_MONITEUR_AVIS_EMIS'));
        $c->add(CommonAnnonceMoniteurPeer::ORGANISME, $organisme);
        $listDest = CommonAnnonceMoniteurPeer::doSelect($c, $connexion);
        if ($listDest) {
            return $listDest;
        } else {
            return [];
        }
    }

    /**
     * retourne  les annonces publiés au boamp avec les types de formulaire correspondants.
     */
    public function retreiveAnnoncePublieWithTypeForm($consultationId, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonAnnonceBoampPeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonAnnonceBoampPeer::ORGANISME, $organisme);
        $criterion = $c->getNewCriterion(CommonAnnonceBoampPeer::STATUT_DESTINATAIRE, Atexo_Config::getParameter('DESTINATAIRE_STATUT_CP'));
        $criterion2 = $c->getNewCriterion(CommonAnnonceBoampPeer::STATUT_DESTINATAIRE, Atexo_Config::getParameter('DESTINATAIRE_STATUT_AV'));
        $criterion3 = $c->getNewCriterion(CommonAnnonceBoampPeer::STATUT_DESTINATAIRE, Atexo_Config::getParameter('DESTINATAIRE_STATUT_PU'));
        $criterion->addOr($criterion2);
        $criterion->addOr($criterion3);
        $c->addAnd($criterion);
        $c->addJoin(CommonAnnonceBoampPeer::ID_TYPE_XML, CommonReferentielTypeXmlPeer::ID);
        $annonceForm = CommonAnnonceBoampPeer::doSelect($c, $connexion);
        if ('' != $annonceForm) {
            return $annonceForm;
        } else {
            return false;
        }
    }

    /**
     * ajouter une ligne dans la table AnnonceMoniteur.
     */
    public function addAnnonceMoniteur($idFormXml, $idDestinataire)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $annonceMoniteur = new CommonAnnonceMoniteur();
        $annonceMoniteur->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
        $annonceMoniteur->setIdFormXml($idFormXml);
        $annonceMoniteur->setIdDestinationFormXml($idDestinataire);
        $annonceMoniteur->setDateCreation(date('Y-m-d H:i'));
        $annonceMoniteur->setStatutDestinataire(Atexo_Config::getParameter('STATUT_MONITEUR_EN_ATTENTE'));
        $annonceMoniteur->setAccuseReception(Atexo_Config::getParameter('ACCUSE_RECEPTION_NA'));
        $annonceMoniteur->setXmlMoniteur(self::generateXmlMoniteur($idFormXml));
        $annonceMoniteur->save($connexionCom);
    }

    /*
     * Pour l'insertion dans la table annonce
     */
    public function addAnnonceBoamp($idFormXml, $idDestinataire)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $newAnnonce = new CommonAnnonceBoamp();
        $newAnnonce->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
        $formXml = (new Atexo_Publicite_ReferentielFormXml())->retreiveReferentielFormXmlById($idFormXml);
        $newAnnonce->setIdDestinationFormXml($idDestinataire);
        $newAnnonce->setIdFormXml($idFormXml);
        $newAnnonce->setConsultationId($formXml[0]->getConsultationId());
        $newAnnonce->setIdTypeXml($formXml[0]->getIdTypeXml());
        $newAnnonce->setStatutDestinataire(Atexo_Config::getParameter('DESTINATAIRE_EN_ATTENTE'));
        $newAnnonce->setAccuseReception(Atexo_Config::getParameter('ACCUSE_RECEPTION_NA'));
        //FIXME ajout transaction
        $newAnnonce->save($connexionCom);
        $newCommonAnnonce = new CommonAnnonce();
        $newCommonAnnonce->setIdBoamp($newAnnonce->getIdBoamp());
        $newCommonAnnonce->setConsultationId($formXml[0]->getConsultationId());
        $newCommonAnnonce->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
        $newCommonAnnonce->save($connexionCom);
    }

    /**
     * permet de generer un flux initial.
     */
    public function generateXmlMoniteur($idFormXml)
    {
        $nature = null;
        $contents = file_get_contents(Atexo_Config::getParameter('CHEMIN_MODEL_INITIALISATION_MONITEUR'));
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $formXml = CommonReferentielFormXmlPeer::retrieveByPK($idFormXml, Atexo_CurrentUser::getCurrentOrganism(), $connexion);
        //BOAMP6
        if ('2' == $formXml->getIdTypeXml()) {
            $nature = '<annonceSimple nature="APU"/>';
        }
        // BOAMP3 ou JOUE_02
        elseif ('1' == $formXml->getIdTypeXml() || '3' == $formXml->getIdTypeXml()) {
            $nature = '<annonceSimple nature="AAPC"/>';
        }
        // JOUE_03, BOAMP4 ou BOAMP7
        elseif ('4' == $formXml->getIdTypeXml() || '5' == $formXml->getIdTypeXml() || '7' == $formXml->getIdTypeXml()) {
            $nature = '<annonce nature="AM"><avisInitial/></annonce>';
        }
        $contents = Atexo_Util::xmlStrReplace($contents, '<!--Annonce-->', $nature);
        $objXml = simplexml_load_string($contents);
        $currentServiceId = Atexo_CurrentUser::getCurrentServiceId();
        if (!empty($currentServiceId)) {
            $service = CommonServicePeer::retrieveByPK($currentServiceId, $connexion);
            $libelle = $service->getSigle();
            $adresse = $service->getAdresse();
            $cp = $service->getCp();
            $ville = $service->getVille();
        } else {
            $organisme = Atexo_Organismes::retrieveOrganismeByAcronyme(Atexo_CurrentUser::getOrganismAcronym());
            if ($organisme instanceof CommonOrganisme) {
                $libelle = $organisme->getDenominationOrg();
                $adresse = $organisme->getAdresse();
                $cp = $organisme->getCp();
                $ville = $organisme->getVille();
            }
        }
        $objXml->facturation->libelle = utf8_encode($libelle);
        $objXml->facturation->adresse = utf8_encode($adresse);
        $objXml->facturation->cp = $cp;
        $objXml->facturation->ville = utf8_encode($ville);

        return Atexo_Util::fixCDATA($objXml->asXML());
    }

    /**
     * Verifie si un destinataire est possible.
     */
    public function existingDestinataire($idFormXml, $idDestinataire)
    {
        $formXml = (new Atexo_Publicite_ReferentielFormXml())->retreiveReferentielFormXmlById($idFormXml);
        $arDest = explode('#', $formXml[0]->getCommonReferentielTypeXml()->getIdDestinataire());
        $listDest = (new Atexo_Publicite_Destinataire())->retreiveListDestinataire($idFormXml);
        $listDestMoniteur = (new Atexo_Publicite_Destinataire())->retreiveListDestinataireMoniteur($idFormXml);
        $array = array_merge($listDest, $listDestMoniteur);
        if (in_array($idDestinataire, $arDest)) {
            foreach ($array as $oneDestinataire) {
                if ($idDestinataire == $oneDestinataire->getIdDestinationFormXml()) {
                    return false;
                }
            }

            return true;
        } else {
            return false;
        }
    }
}
