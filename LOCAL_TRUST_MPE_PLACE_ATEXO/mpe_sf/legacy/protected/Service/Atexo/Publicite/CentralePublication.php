<?php

namespace Application\Service\Atexo\Publicite;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonCentralePublicationPeer;
use Application\Propel\Mpe\CommonCompteCentralePeer;
use Application\Propel\Mpe\CommonDestinataireCentralePubPeer;
use Application\Propel\Mpe\CommonJournauxPeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;

/**
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Publicite_CentralePublication
{
    public function retreiveListCentralePub()
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonCentralePublicationPeer::ORGANISME, Atexo_CurrentUser::getCurrentOrganism());
        $centralPub = CommonCentralePublicationPeer::doSelect($c, $connexionCom);
        if ($centralPub) {
            return $centralPub;
        } else {
            return [];
        }
    }

    public function retreiveListeComptesCentraleByService($idService, $org = null)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        if (!$org) {
            $org = Atexo_CurrentUser::getCurrentOrganism();
        }
        $c = new Criteria();
        $c->add(CommonCompteCentralePeer::ID_SERVICE, $idService);
        $c->add(CommonCompteCentralePeer::ORGANISME, $org);
        $c->add(CommonCentralePublicationPeer::ORGANISME, $org);
        //$c->addJoin(CommonCompteCentralePeer::ORGANISME, CommonCentralePublicationPeer::ORGANISME);
        $comptesPub = CommonCompteCentralePeer::doSelectJoinCommonCentralePublication($c, $connexionCom);
        if ($comptesPub) {
            return $comptesPub;
        } else {
            return [];
        }
    }

    public function retreiveListeDestinataireCentraleByIdAnnonce($idAnnonce, $org = null)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        if (!$org) {
            $org = Atexo_CurrentUser::getCurrentOrganism();
        }
        $c = new Criteria();
        $c->add(CommonDestinataireCentralePubPeer::ID_ANNONCE_JAL, $idAnnonce);
        $c->add(CommonDestinataireCentralePubPeer::ORGANISME, $org);
        $destPub = CommonDestinataireCentralePubPeer::doSelect($c, $connexionCom);
        // print_r($destPub);exit;
        if ($destPub) {
            return $destPub;
        } else {
            return [];
        }
    }

    public function retreiveComptePubJoinCentraleById($id, $org = null)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        if (!$org) {
            $org = Atexo_CurrentUser::getCurrentOrganism();
        }
        $c = new Criteria();
        $c->add(CommonCompteCentralePeer::ID_COMPTE, $id);
        $c->add(CommonCompteCentralePeer::ORGANISME, $org);
        //$c->addJoin(CommonCompteCentralePeer::ORGANISME, CommonCentralePublicationPeer::ORGANISME);
        $centralePub = CommonCompteCentralePeer::doSelectJoinCommonCentralePublication($c, $connexionCom);
        if ($centralePub) {
            return $centralePub;
        } else {
            return [];
        }
    }

    public function retreiveJournauxCentrale($idCentrale)
    {
        //echo $idCentrale;
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonJournauxPeer::ID_CENTRALE, $idCentrale);
        $c->add(CommonJournauxPeer::ORGANISME, Atexo_CurrentUser::getCurrentOrganism());
        $journaux = CommonJournauxPeer::doSelect($c, $connexionCom);
        //print_r($destPub);exit;
        if ($journaux) {
            return $journaux;
        } else {
            return [];
        }
    }

    /**
     * $param $chaineIds liste stockée en base des ids dans la table des destinataires.
     */
    public function retreiveJournauxCentraleByIds($chaineIds)
    {
        $arrayId = explode(',', $chaineIds);
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonJournauxPeer::ID_JOURNAL, $arrayId, Criteria::IN);
        $c->add(CommonJournauxPeer::ORGANISME, Atexo_CurrentUser::getCurrentOrganism());
        $journaux = CommonJournauxPeer::doSelect($c, $connexionCom);
        if ($journaux) {
            return $journaux;
        } else {
            return [];
        }
    }

    public function retreiveNamesJournauxCentraleByIds($chaineIds)
    {
        $chaine = '';
        $journaux = self::retreiveJournauxCentraleByIds($chaineIds);
        foreach ($journaux as $journal) {
            $chaine .= $journal->getNomJournal().', ';
        }

        return substr($chaine, 0, -1);
    }

    public function deleteDestinataireCentralePub($idAnnJal)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $c = new Criteria();
        $c->add(CommonDestinataireCentralePubPeer::ID_ANNONCE_JAL, $idAnnJal);
        $c->add(CommonDestinataireCentralePubPeer::ORGANISME, Atexo_CurrentUser::getCurrentOrganism());
        CommonDestinataireCentralePubPeer::doDelete($c, $connexionCom);
    }

    public function retreiveDestinataireById($idDest, $org = null)
    {
        if (!$org) {
            $org = Atexo_CurrentUser::getCurrentOrganism();
        }
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonDestinataireCentralePubPeer::ID, $idDest);
        $c->add(CommonDestinataireCentralePubPeer::ORGANISME, $org);
        $dest = CommonDestinataireCentralePubPeer::doSelect($c, $connexionCom);
        if ($dest) {
            return $dest;
        } else {
            return false;
        }
    }

    public function retreiveDestinataireByIdCompte($idCompte, $idAnnoceJal)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonDestinataireCentralePubPeer::ID_COMPTE, $idCompte);
        $c->add(CommonDestinataireCentralePubPeer::ORGANISME, Atexo_CurrentUser::getCurrentOrganism());
        $c->add(CommonDestinataireCentralePubPeer::ID_ANNONCE_JAL, $idAnnoceJal);
        $destPub = CommonDestinataireCentralePubPeer::doSelectOne($c, $connexionCom);
        //print_r($destPub);exit;
        if ($destPub) {
            return $destPub;
        } else {
            return false;
        }
    }

    public function retrieveDestinataireCentralPubByEchangeDestinataire($echangeDest, $org = null)
    {
        if (!$org) {
            $org = Atexo_CurrentUser::getCurrentOrganism();
        }
        if ($echangeDest) {
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            $c = new Criteria();
            $c->add(CommonDestinataireCentralePubPeer::ID_ECHANGE, $echangeDest->getIdEchange());
            $c->add(CommonDestinataireCentralePubPeer::ORGANISME, $org);
            $objetDestAnnonceCentralePub = CommonDestinataireCentralePubPeer::doSelectOne($c, $connexionCom);

            return $objetDestAnnonceCentralePub;
        }
    }
}
