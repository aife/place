<?php

namespace Application\Service\Atexo;

use \PDO;
use App\Service\DossierVolumineux\DossierVolumineuxService;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonConfigurationOrganisme;
use Application\Propel\Mpe\CommonConfigurationOrganismeQuery;
use Application\Propel\Mpe\CommonConfigurationPlateformePeer;
use Application\Propel\Mpe\CommonOrganisme;
use Application\Propel\Mpe\CommonService;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * Classe de gestion des modules.
 *
 * @author Khalid BENAMAR <khalid.benamar@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Module
{
    /**
     * Vérifier si un module est activer ou non.
     *
     * @param nom du module      $module
     * @param nom de l'organisme $organisme
     *
     * @return bool
     */
    public static function isEnabled($moduleName, $organisme = '')
    {
        $result = false;
        $modules = Atexo_CurrentUser::getModules();

        if (!$organisme && isset($modules[$moduleName]) && '' != $modules[$moduleName]) {
            $result = $modules[$moduleName];
        } else {
            $res = CommonConfigurationPlateformePeer::getConfigurationPlateforme();
            $object = 'get'.$moduleName;
            if (method_exists($res, $object)) {
                if ($res->$object()) {
                    $result = true;
                }
            } else {
                if ($organisme) {
                    $targetOrganisme = $organisme;
                } else {
                    //vérfier si l'organisme est en majuscule ou non
                    if (1 == Atexo_Config::getParameter('DB_NAME_IN_UPPERCASE')) {
                        $targetOrganismes = Atexo_CurrentUser::getOrganismAcronym();
                        $targetOrganisme = strtolower($targetOrganismes);
                    } else {
                        $targetOrganisme = Atexo_CurrentUser::getOrganismAcronym();
                    }
                }

                $connexion = Propel::getConnection(
                    Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY')
                );
                $configurationOrgQuery = new CommonConfigurationOrganismeQuery();
                $resultOrg = $configurationOrgQuery->findOneByOrganisme($targetOrganisme, $connexion);

                $commonConfigurationOrganisme = new CommonConfigurationOrganisme();
                $objectOrg = 'get'.$moduleName;
                if (method_exists($commonConfigurationOrganisme, $objectOrg)) {
                    if ($resultOrg) {
                        if ($resultOrg->$objectOrg()) {
                            $result = true;
                        }
                    }
                }
            }
        }

        return $result;
    }

    /**
     * Retourne la liste des modules de la table ModuleCommon.
     */
    public static function retrieveModules()
    {
        $modules = CommonConfigurationPlateformePeer::getConfigurationPlateforme();
        if ($modules) {
            return $modules;
        } else {
            return false;
        }
    }

    /**
     * Retourne la liste des organismes pour les quel le module 'ARCADE' est activé.
     */
    public function getOrganismesActivedArcadeModule()
    {
        $connexion = Propel::getConnection(
            Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY')
        );
        $configurationOrgQuery = new CommonConfigurationOrganismeQuery();
        $modules = $configurationOrgQuery->findByInterfaceArchiveArcadePmi('1');
        if ($modules) {
            return $modules;
        } else {
            return false;
        }
    }

    public function getModuleOrganismeByAcronyme($acronyme)
    {
        $connexion = Propel::getConnection(
            Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY')
        );
        $configurationOrgQuery = new CommonConfigurationOrganismeQuery();
        $modules = $configurationOrgQuery->findOneByOrganisme($acronyme, $connexion);
        if ($modules) {
            return $modules;
        } else {
            return false;
        }
    }

    public function copyModuleOrganisme($organismeCible, $organismeReference = 'a1a')
    {
        try {
            $organismes = [];
            $sql = "SELECT * FROM configuration_organisme WHERE organisme='".$organismeReference."'";
            $statementValueModule = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_ASSOC);

            $req = '';
            foreach ($statementValueModule as $valueModule) {
                foreach ($valueModule as $module => $valeur) {
                    if ($valeur != $organismeReference) {
                        $req .= $module."='".$valeur."' , ";
                    }
                }
            }
            $req = substr($req, 0, -2);

            if ($organismeCible) {
                $org = new CommonOrganisme();
                $org->setAcronyme($organismeCible);
                $organismes[] = $org;
            } else {
                $organismes = (new Atexo_Organismes())->retrieveAllOrganismes();
            }

            foreach ($organismes as $organisme) {
                if ($organisme->getAcronyme() != $organismeReference) {
                    if (!self::getModuleOrganismeByAcronyme($organisme->getAcronyme())) {
                        $modules = new CommonConfigurationOrganisme();
                        $modules->setOrganisme($organismeCible);
                        $modules->save();
                    }
                    $sql = 'UPDATE  configuration_organisme SET '.$req." WHERE organisme='".$organisme->getAcronyme(
                        )."'";
                    Atexo_Db::getLinkCommon(true)->query($sql);
                }
            }
        } catch (\Exception $e) {
            Prado::log('Erreur Module.php'.$e->getMessage().$e->getTraceAsString(), TLogger::ERROR, 'Module.php');
        }
    }

    /**
     * Verifie si le module CHORUS est active par service.
     *
     * @param string      $serviceId: identifiant du service
     * @param string|null $organisme: organisme
     *
     * @return bool : true si module active, false sinon
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function isChorusAccessActivate($serviceId, $organisme = null)
    {
        try {
            if (null == $organisme) {
                $organisme = Atexo_CurrentUser::getOrganismAcronym();
            }
            if (!$serviceId) {
                return Atexo_Module::isEnabled('InterfaceChorusPmi', $organisme);
            } else {
                $service = Atexo_EntityPurchase::retrieveEntityById($serviceId, $organisme);
                if ($service instanceof CommonService && ((int) $service->getAccesChorus()) != 0) {
                    return true;
                }

                return false;
            }
        } catch (\Exception) {
            $logger = Atexo_LoggerManager::getLogger('app');
            $logger->error(
                "Erreur lors de l'activation du module 'InterfaceChorusPmi' : id_service = ".$serviceId.' , Organisme = '.$organisme
            );
        }
    }

    /**
     * @param $organisme
     */
    public function isSaisieManuelleActive($organisme): false|string
    {
        if(!Atexo_Config::getParameter('SPECIFIQUE_PLACE')){
            $configurationOrganisme = (new Atexo_Module())->getModuleOrganismeByAcronyme($organisme);
            try {
            if ($configurationOrganisme instanceof CommonConfigurationOrganisme) {
                return $configurationOrganisme->getSaisieManuelleIdExterne();
            }
        } catch (\Exception) {
                $logger = Atexo_LoggerManager::getLogger('app');
                $logger->error('Erreur lors de la récupération de la configuration de la saisie manuelle du Id externe');
            }
        }

        return false;
    }

    public static function isEnvolModuleActive(): bool
    {
        return Atexo_Util::getSfService(DossierVolumineuxService::class)->isEnable();
    }
}
