<?php

namespace Application\Service\Atexo;

use Application\Propel\Mpe\CommonTFormePrix;
use Prado\Prado;

/**
 * Classe de manipulation des formes prix.
 *
 * @author Ezziani Loubna <loubna.ezziani@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 2.0
 *
 * @since MPE-4.0
 */
class Atexo_FormePrix
{
    /**
     * Permet de valider les formes de prix.
     *
     * @param $formePrix
     * @param $variationPrixForfaitaire
     * @param $variationPrixUnitaire
     * @param $typePrix
     * @param $isValid
     * @param $arrayMsgErreur
     * @param $lot: lot de la consultation, égal à 0 pour une consultation non allotie
     */
    public function validerFormePrix($formePrix, $variationPrixForfaitaire, $variationPrixUnitaire, $typePrix, &$isValid, &$arrayMsgErreur, $lot = 0, $trancheMsg = null)
    {
        if (0 === $lot) {
            $msg = '';
        } else {
            $msg = Prado::localize('TEXT_LOT').' '.$lot.' - ';
        }
        if ($trancheMsg) {
            $msg .= $trancheMsg.' - ';
        }
        if ($formePrix instanceof CommonTFormePrix) {
            //Forme de prix unitaire
            if ($formePrix->getFormePrix() == Atexo_Config::getParameter('KEY_PRIX_UNITAIRE')) {
                //Variation prix
                if (!is_array($variationPrixUnitaire) || 1 !== count($variationPrixUnitaire)) {
                    $isValid = false;
                    $arrayMsgErreur[] = $msg.Prado::localize('TEXT_FORME_DE_PRIX').' - '.Prado::localize('TEXT_PRIX_UNITAIRE').' - '.Prado::localize('DEFINE_VARIATION_PRIX');
                }
                //Type de prix
                if (!is_array($typePrix) || !count($typePrix)) {
                    $isValid = false;
                    $arrayMsgErreur[] = $msg.Prado::localize('TEXT_FORME_DE_PRIX').' - '.Prado::localize('TEXT_PRIX_UNITAIRE').' - '.Prado::localize('DEFINE_TYPE_PRIX');
                }
            }//Forme de prix forfaitaire
            elseif ($formePrix->getFormePrix() == Atexo_Config::getParameter('KEY_PRIX_FORFAITAIRE') && (!is_array($variationPrixForfaitaire) || 1 !== count($variationPrixForfaitaire))) {
                $isValid = false;
                $arrayMsgErreur[] = $msg.Prado::localize('TEXT_FORME_DE_PRIX').' - '.Prado::localize('DEFINE_PRIX_FORFAITE').' - '.Prado::localize('DEFINE_VARIATION_PRIX');
            } elseif ($formePrix->getFormePrix() == Atexo_Config::getParameter('KEY_PRIX_MIXTE')) {
                //Variation prix
                if (!is_array($variationPrixForfaitaire) || 1 !== count($variationPrixForfaitaire)) {
                    $isValid = false;
                    $arrayMsgErreur[] = $msg.Prado::localize('TEXT_FORME_DE_PRIX').' - '.Prado::localize('DEFINE_PRIX_MIXTE').' - '.Prado::localize('DEFINE_PARTIE_FORFAITAIRE').' - '.Prado::localize('DEFINE_VARIATION_PRIX');
                }

                //Variation prix
                if (!is_array($variationPrixUnitaire) || 1 !== count($variationPrixUnitaire)) {
                    $isValid = false;
                    $arrayMsgErreur[] = $msg.Prado::localize('TEXT_FORME_DE_PRIX').' - '.Prado::localize('DEFINE_PRIX_MIXTE').' - '.Prado::localize('DEFINE_PARTIE_UNITAIRE').' - '.Prado::localize('DEFINE_VARIATION_PRIX');
                }
                //Type de prix
                if (!is_array($typePrix) || !count($typePrix)) {
                    $isValid = false;
                    $arrayMsgErreur[] = $msg.Prado::localize('TEXT_FORME_DE_PRIX').' - '.Prado::localize('DEFINE_PRIX_MIXTE').' - '.Prado::localize('DEFINE_PARTIE_UNITAIRE').' - '.Prado::localize('DEFINE_TYPE_PRIX');
                }
            }
        } else {
            $isValid = false;
            $arrayMsgErreur[] = $msg.Prado::localize('TEXT_FORME_DE_PRIX');
        }
    }
}
