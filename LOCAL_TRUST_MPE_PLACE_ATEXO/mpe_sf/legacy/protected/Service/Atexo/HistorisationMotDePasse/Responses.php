<?php

namespace Application\Service\Atexo\HistorisationMotDePasse;

use Application\Propel\Mpe\CommonHistorisationMotDePasseQuery;

class Atexo_HistorisationMotDePasse_Responses
{
    /**
     * récupère la liste des X dernier mot de passe modifié pour un inscrit.
     *
     * @param $idInscrit
     * @param int $limit
     *
     * @return array
     */
    public function retrieveInscritLastPasswordUpdate($idInscrit, $limit = 1)
    {
        $query = new CommonHistorisationMotDePasseQuery();

        return $query->getLastUpdate($idInscrit, $limit);
    }
}
