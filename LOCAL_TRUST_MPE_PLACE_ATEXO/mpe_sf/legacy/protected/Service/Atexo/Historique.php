<?php

namespace Application\Service\Atexo;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonDATEFINPeer;
use Application\Propel\Mpe\CommonDCEPeer;
use Application\Propel\Mpe\CommonHistoriquesConsultationPeer;
use Application\Propel\Mpe\CommonRGPeer;
use Application\Service\Atexo\Referentiel\Atexo_Referentiel_Referentiel;

/**
 * commentaires.
 *
 * @author SBAI Yassine <yassine.sbai@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Historique
{
    public function getHistoriqueDce($refCons, $org)
    {
        $c = new Criteria();
        $arrayDce = [];
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c->add(CommonDCEPeer::CONSULTATION_ID, $refCons, Criteria::EQUAL);
        $c->add(CommonDCEPeer::ORGANISME, $org, Criteria::EQUAL);
        $Dces = CommonDCEPeer::doSelect($c, $connexionCom);

        if (is_array($Dces) && 0 != count($Dces)) {
            foreach ($Dces as $Dce) {
                $agent = (new Atexo_Agent())->retrieveAgent($Dce->getAgentId());
                if ($agent) {
                    $Dce->setNomAgent($agent->getPrenom().' '.$agent->getNom());
                }

                $arrayDce[] = $Dce;
            }
        }

        if (is_array($arrayDce) && 0 != count($arrayDce)) {
            return $arrayDce;
        } else {
            return false;
        }
    }

    public function getHistoriqueRG($refCons, $org)
    {
        $c = new Criteria();
        $arrayRg = [];
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c->add(CommonRGPeer::CONSULTATION_ID, $refCons, Criteria::EQUAL);
        $c->add(CommonRGPeer::ORGANISME, $org, Criteria::EQUAL);
        $Rgs = CommonRGPeer::doSelect($c, $connexionCom);

        if (is_array($Rgs) && 0 != count($Rgs)) {
            foreach ($Rgs as $Rg) {
                $agent = (new Atexo_Agent())->retrieveAgent($Rg->getAgentId());
                if ($agent) {
                    $Rg->setNomAgent($agent->getPrenom().' '.$agent->getNom());
                }

                $arrayRg[] = $Rg;
            }
        }

        if (is_array($arrayRg) && 0 != count($arrayRg)) {
            return $arrayRg;
        } else {
            return false;
        }
    }

    public function getHistoriqueDateFin($refCons, $org)
    {
        $c = new Criteria();
        $arrayDateFin = [];
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c->add(CommonDATEFINPeer::CONSULTATION_ID, $refCons, Criteria::EQUAL);
        $c->add(CommonDATEFINPeer::ORGANISME, $org, Criteria::EQUAL);
        $dateFins = CommonDATEFINPeer::doSelect($c, $connexionCom);

        if (is_array($dateFins) && 0 != count($dateFins)) {
            foreach ($dateFins as $dateFin) {
                $agent = (new Atexo_Agent())->retrieveAgent($dateFin->getAgentId());
                if ($agent) {
                    $dateFin->setNomAgent($agent->getPrenom().' '.$agent->getNom());
                }

                $arrayDateFin[] = $dateFin;
            }
        }

        if (is_array($arrayDateFin) && 0 != count($arrayDateFin)) {
            return $arrayDateFin;
        } else {
            return false;
        }
    }

    public function getHistoriqueConsultation($refCons, $numElement, $org)
    {
        $c = new Criteria();
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c->add(CommonHistoriquesConsultationPeer::CONSULTATION_ID, $refCons, Criteria::EQUAL);
        $c->add(CommonHistoriquesConsultationPeer::ORGANISME, $org, Criteria::EQUAL);
        $c->add(CommonHistoriquesConsultationPeer::NOM_ELEMENT, $numElement, Criteria::EQUAL);

        $historique = CommonHistoriquesConsultationPeer::doSelect($c, $connexionCom);

        if ($historique) {
            return $historique;
        } else {
            return false;
        }
    }

    public function getHistoriqueLtReferentielConsultation($refCons, $org, $page)
    {
        //$arrayReferentiels = Atexo_Referentiel_Referentiel::getAllLtReferentielByLotouConsulation($org,$refCons,0,1);
        $referentiels = Atexo_Referentiel_Referentiel::getLtReferentiels(false, 0, 0, '-1', $page, $org);
        $resultat = [];
        $i = 0;
        foreach ($referentiels as $referentiel) {
            $numElement = Atexo_Config::getParameter('ID_LT_REFERENTIEL').'_'.$referentiel->getCodeLibelle();
            $historiques = (new Atexo_Historique())->getHistoriqueConsultation($refCons, $numElement, $org);
            if ($historiques) {
                $resultat[$i++] = $historiques;
            }
        }
        if (count($resultat)) {
            return $resultat;
        }

        return false;
    }
}
