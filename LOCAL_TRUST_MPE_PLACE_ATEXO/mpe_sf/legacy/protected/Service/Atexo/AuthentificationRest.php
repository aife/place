<?php

namespace Application\Service\Atexo;

use DOMDocument;
use Laminas\Http\Client;
use Laminas\Http\Request;

/**
 * Classe de manipulation de l'authentification REST.
 *
 * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
 * @copyright Atexo 2014
 *
 * @version 1.0
 *
 * @since MPE-4.6
 */
class Atexo_AuthentificationRest
{
    /*
     *  Permet de verfier que l'authentification a echoué
     *  @param String $uri,url de web service de l'authentification,string $attendu variable vide.string $constate variable vide
     *  @author loubna.ezziani@atexom.com
     *  @copyright Atexo 2014
     *  @version 1.0
     *  @since 4.6.0
     */
    public function testAuthentification($uri, &$attendu, &$constate)
    {
        $config = [
        'adapter' => 'Zend_Http_Client_Adapter_Curl',
        'curloptions' => [CURLOPT_SSL_VERIFYHOST => 0, CURLOPT_SSL_VERIFYPEER => false],
        ];
        $client = new Client($uri, $config);
        $client->setMethod(Request::METHOD_GET);
        $ticketXml = $client->send();
        $attendu = new DOMDocument();
        $attendu->loadXML($ticketXml);
        $constate = new DOMDocument();
        $constate->loadXML('<ticket/>');
    }
}
