<?php

namespace Application\Service\Atexo;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonProgrammePrevisionnel;
use Application\Propel\Mpe\CommonProgrammePrevisionnelPeer;

/*
 * Created on 23 nov. 2009
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */

class Atexo_ProgrammesPrevisionnels
{
    /**
     * retourne l liste des marchés d'organisme.
     */
    public function retreivePPs($organisme, $serviceId, $annee = false, $justLast = false, $connexionCom = false, $calledFromAgent = false, $classification = false)
    {
        if (!$connexionCom) {
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        }
        $c = new Criteria();
        if ($classification) {
            $arrayAcronymes = Atexo_Organismes::retrieveOrganismes(false, null, true, $classification, true);
            if (is_array($arrayAcronymes)) {
                $c->add(CommonProgrammePrevisionnelPeer::ORGANISME, $arrayAcronymes, Criteria::IN);
            }
        }
        if ($organisme) {
            $c->add(CommonProgrammePrevisionnelPeer::ORGANISME, $organisme);
            if ('' != $serviceId || 0 == $serviceId) {
                $c->add(CommonProgrammePrevisionnelPeer::SERVICE_ID, $serviceId, Criteria::EQUAL);
            }
        }
        if ($annee) {
            $c->add(CommonProgrammePrevisionnelPeer::ANNEE, $annee, Criteria::EQUAL);
        }
        $c->addDescendingOrderByColumn(CommonProgrammePrevisionnelPeer::UNTRUSTEDDATE);

        if ('false' == $calledFromAgent) {
            $c->add(CommonProgrammePrevisionnelPeer::AFFICHER, 1, Criteria::EQUAL);
        }
        if ($justLast) {
            $result = CommonProgrammePrevisionnelPeer::doSelectOne($c, $connexionCom);
        } else {
            $result = CommonProgrammePrevisionnelPeer::doSelect($c, $connexionCom);
        }
        if ($result) {
            return $result;
        } else {
            return [];
        }
    }

    /**
     * suppression d'un marché.
     */
    public function doDeletePP($identifiant, $organisme)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonProgrammePrevisionnelPeer::ORGANISME, $organisme, Criteria::EQUAL);
        $c->add(CommonProgrammePrevisionnelPeer::ID, $identifiant, Criteria::EQUAL);
        CommonProgrammePrevisionnelPeer::doDelete($c, $connexionCom);
    }

    /**
     * Ajouter le path du service du PP.
     */
    public function addPathServicePPs($PPs)
    {
        if (is_array($PPs) && count($PPs)) {
            foreach ($PPs as $onePP) {
                $service = $onePP->getServiceId();
                $org = $onePP->getOrganisme();
                $entityPath = Atexo_EntityPurchase::getSigleLibelleEntityById($service, $org);
                $onePP->setPathService($entityPath);
            }
        }

        return $PPs;
    }

    /**
     * Activer le dernier PP ajouté.
     */
    public function activateLastPP($org, $serviceId, $annee, $connexionCom = false)
    {
        if (!$connexionCom) {
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        }
        $lastPP = self::retreivePPs($org, $serviceId, $annee, true, $connexionCom);
        if ($lastPP instanceof CommonProgrammePrevisionnel) {
            $idLastPP = $lastPP->getId();
        }
        $PPs = self::retreivePPs($org, $serviceId, $annee, false, $connexionCom);
        if (is_array($PPs) && count($PPs)) {
            foreach ($PPs as $onePP) {
                if ($onePP->getId() != $idLastPP) {
                    $onePP->setAfficher(0);
                    $onePP->save($connexionCom);
                } else {
                    $onePP->setAfficher(1);
                    $onePP->save($connexionCom);
                }
            }
        }
    }
}
