<?php

namespace Application\Service\Atexo\WebService;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonAgent;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Message;
use Exception;
use Prado\Prado;

/*
 * Created on 3 nov. 2011
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 *
 */

class InterfaceAddAgentIam
{
    /**
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @param string $idIam  id
     * @param string $email  email
     * @param string $nom    nom
     * @param string $prenom prenom
     * @param string $tel    tel
     * @param string $fax    fax
     *
     * @return string $retour
     * @soapmethod
     */
    public function AddAgentIam($idIam, $email = null, $nom = null, $prenom = null, $tel = null, $fax = null)
    {
        $message = null;
        $parametreRecus = '';

        try {
            Atexo_Languages::setLanguageCatalogue('agent');
            $parametreRecus .= "\n\r idIam='" . $idIam . "' email='" . $email . "' nom='" . utf8_decode($nom) . "' prenom='" . utf8_decode($prenom) . "' tel='" . $tel . "' fax='" . $fax . "'";
            $infos = null;
            $infos = self::verifyInfos($idIam, $email, $nom, $prenom);
            if ($infos) {
                $message = Prado::localize('MESSAGE_ERREUR_PARAMS_TAM') . ' ' . Prado::localize('MESSAGE_MANQUE_PARAMS_TAM');
                $content = Atexo_Message::getGenericEmailFormat($message . $parametreRecus);
                Atexo_Message::simpleMail(
                    Atexo_Config::getParameter('PF_MAIL_FROM'),
                    Atexo_Config::getParameter('MAIL_GESTIONNAIRE_PORTAIL'),
                    Prado::localize('SUBJECT_MAIL_IAM'),
                    $content,
                    '',
                    '',
                    false,
                    true
                );

                return $infos;
            }
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));
            $agent = (new Atexo_Agent())->retrieveAgentByLogin($idIam);
            $new = false;
            if ($agent instanceof CommonAgent) {
                $AgentObject = $agent;
                $agentMail = (new Atexo_Agent())->retrieveAgentByMail($email);
                if ($agentMail instanceof CommonAgent) {
                    //cas d'une mise à jour de l'agent A1 avec un email existant sur un autre agent A2
                    if ($AgentObject->getLogin() != $agentMail->getLogin()) {
                        $message2 = Prado::localize('MESSAGE_ERREUR_MAIL_TAM');
                        $content = Atexo_Message::getGenericEmailFormat($message2 . ' ' . $parametreRecus);
                        Atexo_Message::simpleMail(
                            Atexo_Config::getParameter('PF_MAIL_FROM'),
                            Atexo_Config::getParameter('MAIL_GESTIONNAIRE_PORTAIL'),
                            Prado::localize('SUBJECT_MAIL_IAM'),
                            $content,
                            '',
                            '',
                            false,
                            true
                        );

                        return 5;
                    }
                } else {
                    $AgentObject->setEmail(trim($email));
                }
                $AgentObject->setNom($nom);
                $AgentObject->setPrenom($prenom);
                if ($tel) {
                    $AgentObject->setNumTel($tel);
                }
                if ($fax) {
                    $AgentObject->setNumFax($fax);
                }
                $AgentObject->setDateModification(date('Y-m-d H:i:s'));
            } else {
                $new = true;
                $AgentObject = new CommonAgent();
                $AgentObject->setMdp(Atexo_Config::getParameter('MDP_AGENT_TAM'));
                Atexo_Agent::updateNewAgentPasswordByArgon2($AgentObject, Atexo_Config::getParameter('MDP_AGENT_TAM'));
                $AgentObject->setTentativesMdp('0');
                $AgentObject->setOrganisme(Atexo_Config::getParameter('ORGANISME_AGENT_TAM'));
                $AgentObject->setServiceId(null);
                $AgentObject->setActif('0');
                $AgentObject->setAlerteReponseElectronique('1');
                $AgentObject->setAlerteClotureConsultation('1');
                $AgentObject->setAlerteReceptionMessage('1');
                $AgentObject->setAlertePublicationBoamp('0');
                $AgentObject->setAlerteEchecPublicationBoamp('0');
                $AgentObject->setAlerteCreationModificationAgent('1');
                $AgentObject->setAlerteQuestionEntreprise('1');
                $AgentObject->setDateCreation(date('Y-m-d H:i:s'));
                $AgentObject->setDateModification(date('Y-m-d H:i:s'));
                $AgentObject->setLogin($idIam);
                $agentMail = (new Atexo_Agent())->retrieveAgentByMail($email);
                if ($agentMail instanceof CommonAgent) {
                    //ccas de la creation d'un agent avec un email déj)à existant en base de donnee
                    $message2 = Prado::localize('MESSAGE_ERREUR_MAIL_TAM');
                    $content = Atexo_Message::getGenericEmailFormat($message2 . ' ' . $parametreRecus);
                    Atexo_Message::simpleMail(
                        Atexo_Config::getParameter('PF_MAIL_FROM'),
                        Atexo_Config::getParameter('MAIL_GESTIONNAIRE_PORTAIL'),
                        Prado::localize('SUBJECT_MAIL_IAM'),
                        $content,
                        '',
                        '',
                        false,
                        true
                    );

                    return 5;
                } else {
                    $AgentObject->setEmail(trim($email));
                }
                $AgentObject->setNom($nom);
                $AgentObject->setPrenom($prenom);
                if ($tel) {
                    $AgentObject->setNumTel($tel);
                }
                if ($fax) {
                    $AgentObject->setNumFax($fax);
                }
            }
            if ($AgentObject->save($connexionCom)) {
                if ($new) {
                    $message = Prado::localize('MESSAGE_CONFIRMATION_CREATION_AGENT_TAM');
                } else {
                    $message = Prado::localize('MESSAGE_CONFIRMATION_MAJ_AGENT_TAM');
                }
                $content = Atexo_Message::getGenericEmailFormat($message . ' ' . $parametreRecus);
                Atexo_Message::simpleMail(
                    Atexo_Config::getParameter('PF_MAIL_FROM'),
                    Atexo_Config::getParameter('MAIL_GESTIONNAIRE_PORTAIL'),
                    Prado::localize('SUBJECT_MAIL_IAM'),
                    $content,
                    '',
                    '',
                    false,
                    true
                );

                return 0;
            }
        } catch (Exception $e) {
            $content = Atexo_Message::getGenericEmailFormat($message . ' ' . $parametreRecus . ' Exception :' . $e->getMessage());
            Atexo_Message::simpleMail(
                Atexo_Config::getParameter('PF_MAIL_FROM'),
                Atexo_Config::getParameter('MAIL_GESTIONNAIRE_PORTAIL'),
                Prado::localize('SUBJECT_MAIL_IAM'),
                $content,
                '',
                '',
                false,
                true
            );

            return 7;
        }
    }

    public function verifyInfos($idIam, $email, $nom, $prenom)
    {
        if (null == $idIam) {
            return 1;
        }
        if (null == $email) {
            return 2;
        }
        if (null == $nom) {
            return 3;
        }
        if (null == $prenom) {
            return 4;
        }
    }
}
