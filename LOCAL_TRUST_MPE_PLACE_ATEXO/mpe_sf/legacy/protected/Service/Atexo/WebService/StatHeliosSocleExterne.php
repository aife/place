<?php

namespace Application\Service\Atexo\WebService;

use Application\Propel\Mpe\CommonAgent;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Helios\Atexo_Helios_Teletransmission;
use Application\Service\Atexo\Interfaces\Atexo_Interfaces_GenerateXmlHelios;
use Application\Service\Atexo\Interfaces\Atexo_Interfaces_InterfacesUtilities;

class StatHeliosSocleExterne
{
    /**
     * Retourne un xml qui contient la reponse helios.
     *
     * @param string $xml requete socle
     *
     * @return string $xmlHelios contient le nombre de teletransmissions par statut
     * @soapmethod
     */
    public function xmlStatHelios($xml)
    {
        $temps_debut = microtime(true);
        if ($xml) {
            $xmlReqContent = simplexml_load_string($xml);
            if ($xmlReqContent) {
                $attrubutAgent = $xmlReqContent->Control->From->attributes();
                $agentObject = (new Atexo_Agent())->retrieveAgentByLogin(utf8_decode((string) $attrubutAgent->login));
                if ($agentObject instanceof CommonAgent) {
                    $countTeletransmissionByAgent = (new Atexo_Helios_Teletransmission())->retrieveCountAllTeletransmissionByAgent($agentObject->getId(), $agentObject->getOrganisme());
                    $countTransmissionTransmitted = (new Atexo_Helios_Teletransmission())->retirieveCountTransmissionTransmitted($agentObject->getOrganisme());
                    $countTransmissionToSend = (new Atexo_Helios_Teletransmission())->retirieveCountTransmissionToSend($agentObject->getOrganisme());
                    $countAllTransmission = (new Atexo_Helios_Teletransmission())->retirieveCountAllTransmission($agentObject->getOrganisme());
                    $generateXmlHelios = new Atexo_Interfaces_GenerateXmlHelios();
                    $xmlHelios = $generateXmlHelios->generateInterfaceXmlHelios($agentObject, $countTeletransmissionByAgent, $countTransmissionTransmitted, $countTransmissionToSend, $countAllTransmission);
                    //Atexo_Interfaces_InterfacesUtilities::validate($xmlHelios);
                    $temps_fin = microtime(true);
                    $txt = 'Debut xmlStatHelios et fin : '.round($temps_fin - $temps_debut, 4)."\n\n";
                    $fp = fopen(Atexo_Config::getParameter('LOG_DIR').session_id().'timer.txt', 'a');
                    fwrite($fp, $txt);
                    fclose($fp);

                    return $xmlHelios;
                }
            }
        }

        return false;
    }
}
