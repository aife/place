<?php

namespace Application\Service\Atexo\WebService;

use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_Zip;
use Application\Service\Atexo\InterfaceWs\Atexo_InterfaceWs_Authentication;
use Application\Service\Atexo\InterfaceWs\Atexo_InterfaceWs_Entites;
use Application\Service\Atexo\InterfaceWs\Atexo_InterfaceWs_Error;
use Exception;
use Prado\Prado;

class InterfacesSocleMPE
{
    /**
     * @author Mounir LAMANE <mounir.lamane@atexo.com>
     * Retourne un xml qui contient la reponse PMI.
     *
     * @param string $xml requete
     *
     * @return string $return contient la réponse PMI
     * @soapmethod
     */
    public function processXml($xml)
    {
        $xmlRetour = null;
        if ($xml) {
            try {
                $xmlReqContent = simplexml_load_string($xml);
                if (Atexo_Util::validateXml($xml, Atexo_Config::getParameter('XSD_INTERFACES_SOCLE_INTERNE'), false)) {
                    if ($xmlReqContent) {
                        $request = $xmlReqContent->Request;
                        if (isset($request->Authentication)) {
                            $authentication = new Atexo_InterfaceWs_Authentication();
                            $typeUser = $request->Authentication->attributes()->type;
                            if (isset($request->Authentication->Password)) {
                                $xmlRetour = $authentication->getXmlByPassword($request->Authentication->Password, $typeUser);
                            } elseif (isset($request->Authentication->Token)) {
                                $xmlRetour = $authentication->getXmlByToken($request->Authentication->Token, $typeUser);
                            }
                        } elseif (isset($request->Entities)) {
                            $entities = new Atexo_InterfaceWs_Entites();
                            $xmlRetour = $entities->getXmlEntities($request->Entities);
                        } else {
                            $errorsWs = new Atexo_InterfaceWs_Error();
                            $xmlRetour = $errorsWs->getXmlError('INVALID_MESSAGE', Atexo_Util::validateXml($xml, Atexo_Config::getParameter('XSD_INTERFACES_SOCLE_INTERNE'), true));
                        }
                    }
                } else {
                    $errorsWs = new Atexo_InterfaceWs_Error();
                    $xmlRetour = $errorsWs->getXmlError('INVALID_MESSAGE', Atexo_Util::validateXml($xml, Atexo_Config::getParameter('XSD_INTERFACES_SOCLE_INTERNE'), true));
                }
            } catch (Exception $e) {
                $errorsWs = new Atexo_InterfaceWs_Error();
                $xmlRetour = $errorsWs->getXmlError('TECHNICAL_ERROR', $e->getMessage());
            }
        } else {
            $errorsWs = new Atexo_InterfaceWs_Error();
            $xmlRetour = $errorsWs->getXmlError('MISSING_PARAMETERS', Prado::localize('TEXT_FICHIER_XML_TRAMSMIS_VIDE'));
        }

        if ($xmlRetour) {
            do {
                $fileName = 'InterfacesSocleMPE'.uniqid();
            } while (is_file($fileName));

            Atexo_Util::write_file(Atexo_Config::getParameter('COMMON_TMP').$fileName, $xmlRetour);
            (new Atexo_Zip())->addFileToZip(Atexo_Config::getParameter('COMMON_TMP').$fileName, Atexo_Config::getParameter('COMMON_TMP').$fileName.'.zip');
            $xmlZip = file_get_contents(Atexo_Config::getParameter('COMMON_TMP').$fileName.'.zip');
            unlink(Atexo_Config::getParameter('COMMON_TMP').$fileName.'.zip');

            return base64_encode($xmlZip);
        }

        return $xmlRetour;
    }
}
