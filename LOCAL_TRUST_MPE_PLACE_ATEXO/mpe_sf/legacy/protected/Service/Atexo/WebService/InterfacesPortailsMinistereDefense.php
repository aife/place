<?php

namespace Application\Service\Atexo\WebService;

use Application\Library\Propel\Exception\PropelException;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_Zip;
use Application\Service\Atexo\Config\Atexo_Config_Exception;
use Application\Service\Atexo\InterfaceWs\Atexo_InterfaceWs_Authentication;
use Application\Service\Atexo\InterfaceWs\Atexo_InterfaceWs_Company;
use Application\Service\Atexo\InterfaceWs\Atexo_InterfaceWs_Consultation;
use Application\Service\Atexo\InterfaceWs\Atexo_InterfaceWs_Entites;
use Application\Service\Atexo\InterfaceWs\Atexo_InterfaceWs_Error;
use Application\Service\Atexo\Oversight\Atexo_Oversight_Oversight;
use Exception;
use Prado\Prado;

class InterfacesPortailsMinistereDefense
{
    /**
     * @author Mounir LAMANE <mounir.lamane@atexo.com>
     * Retourne un xml qui contient la reponse PMI.
     *
     * @param string $xml requete
     *
     * @return string $return contient la réponse PMI
     *
     * @throws Atexo_Config_Exception
     * @soapmethod
     */
    public function processXml($xml)
    {
        $exception = null;
        $xmlRetour = null;
        $params = [];

        try {
            $params['service'] = 'IXARM';
            if ('ORME' == Atexo_Config::getParameter('PF_SHORT_NAME')) {
                $params['service'] = 'IXARM_ALPHA';
            }
            $params['nomBatch'] = 'InterfacesPortailsMinistereDefense';
            $params['code'] = Atexo_Oversight_Oversight::$codeSuccess;
        } catch (Exception $exceptionOversight) {
            $oversightError = "Une erreur est survenue lors de l'historisation de la tâche.".PHP_EOL.'Erreur : '.$exceptionOversight->getMessage().PHP_EOL.'Trace : '.$exception->getTraceAsString().PHP_EOL;
            $logger = Atexo_LoggerManager::getLogger('supervision');
            $logger->error($oversightError);
        }
        if ($xml) {
            try {
                $xmlReqContent = simplexml_load_string($xml);

                if (Atexo_Util::validateXml($xml, Atexo_Config::getParameter('XSD_INTERFACES_DEFENSE'), false)) {
                    if ($xmlReqContent) {
                        $request = $xmlReqContent->Request;

                        if (isset($request->Authentication)) {
                            $authentication = new Atexo_InterfaceWs_Authentication();
                            $typeUser = $request->Authentication->attributes()->type;

                            if ('ENTITY' == $typeUser) {
                                $params['variablesEntree'] = json_encode(['info' => 'Authentification des Agents'], JSON_THROW_ON_ERROR);
                            } elseif ('COMPANY' == $typeUser) {
                                $params['variablesEntree'] = json_encode(['info' => 'Authentification des Entreprises'], JSON_THROW_ON_ERROR);
                            }

                            if (isset($request->Authentication->Password)) {
                                $xmlRetour = $authentication->getXmlByPassword(
                                    $request->Authentication->Password,
                                    $typeUser
                                );
                            } elseif (isset($request->Authentication->Token)) {
                                $xmlRetour = $authentication->getXmlByToken(
                                    $request->Authentication->Token,
                                    $typeUser
                                );
                            }
                        } elseif (isset($request->Companies)) {
                            $params['variablesEntree'] = json_encode([
                                'info' => 'Récupération de la liste des entreprises et de comptes inscrits',
                            ], JSON_THROW_ON_ERROR);
                            $companies = new Atexo_InterfaceWs_Company();
                            $xmlRetour = $companies->getXmlCompanies($request->Companies);
                        } elseif (isset($request->Tender)) {
                            $params['variablesEntree'] = json_encode([
                                'info' => 'Récupération de la liste des consultations en ligne',
                            ], JSON_THROW_ON_ERROR);
                            $tenders = new Atexo_InterfaceWs_Consultation();
                            $xmlRetour = $tenders->getXmlTender($request->Tender);
                        } elseif (isset($request->Entities)) {
                            $params['variablesEntree'] = json_encode([
                                'info' => "Récupération de la liste des entités d'achats et des comptes agents",
                            ], JSON_THROW_ON_ERROR);
                            $entities = new Atexo_InterfaceWs_Entites();
                            $xmlRetour = $entities->getXmlEntities($request->Entities, false);
                        } else {
                            $errorsWs = new Atexo_InterfaceWs_Error();
                            $xmlRetour = $errorsWs->getXmlError(
                                'INVALID_MESSAGE',
                                Atexo_Util::validateXml(
                                    $xml,
                                    Atexo_Config::getParameter('XSD_INTERFACES_DEFENSE'),
                                    true
                                )
                            );
                        }
                    }
                } else {
                    $params['code'] = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
                    $errorsWs = new Atexo_InterfaceWs_Error();
                    $xmlRetour = $errorsWs->getXmlError(
                        'INVALID_MESSAGE',
                        Atexo_Util::validateXml(
                            $xml,
                            Atexo_Config::getParameter('XSD_INTERFACES_DEFENSE'),
                            true
                        )
                    );
                }
            } catch (\Exception $e) {
                $params['code'] = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
                $errorsWs = new Atexo_InterfaceWs_Error();
                $xmlRetour = $errorsWs->getXmlError('TECHNICAL_ERROR', $e->getMessage());
            }
        } else {
            $params['code'] = Atexo_Oversight_Oversight::$codeErrorApplicationHasEncounteredProblem;
            $errorsWs = new Atexo_InterfaceWs_Error();
            $xmlRetour = $errorsWs->getXmlError(
                'MISSING_PARAMETERS',
                Prado::localize('TEXT_FICHIER_XML_TRAMSMIS_VIDE')
            );
        }

        if ($xmlRetour) {
            do {
                $fileName = 'WebServiceInterfaceDefense'.uniqid();
            } while (is_file($fileName));

            Atexo_Util::write_file(Atexo_Config::getParameter('COMMON_TMP').$fileName, $xmlRetour);
            (new Atexo_Zip())->addFileToZip(Atexo_Config::getParameter('COMMON_TMP').$fileName, Atexo_Config::getParameter('COMMON_TMP').$fileName.'.zip');
            $xmlZip = file_get_contents(Atexo_Config::getParameter('COMMON_TMP').$fileName.'.zip');
            unlink(Atexo_Config::getParameter('COMMON_TMP').$fileName.'.zip');

            try {
                $params['oversight'] = self::oversight($params);
            } catch (Exception $exceptionOversight) {
                $oversightError = "Une erreur est survenue lors de l'historisation de la tâche.".PHP_EOL.'Erreur : '.$exceptionOversight->getMessage().PHP_EOL.'Trace : '.$exception->getTraceAsString().PHP_EOL;
                $logger = Atexo_LoggerManager::getLogger('supervision');
                $logger->error($oversightError);
            }

            return base64_encode($xmlZip);
        }

        return $xmlRetour;
    }

    /**
     * @param array $params
     *
     *
     * @throws PropelException
     */
    public function oversight($params = []): bool|string
    {
        return Atexo_Oversight_Oversight::oversight($params);
    }
}
