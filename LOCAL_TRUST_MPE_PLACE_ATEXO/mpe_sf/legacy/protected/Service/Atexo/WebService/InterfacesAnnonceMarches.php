<?php

namespace Application\Service\Atexo\WebService;

use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_Zip;
use Application\Service\Atexo\InterfaceWs\Atexo_InterfaceWs_Consultation;
use Application\Service\Atexo\InterfaceWs\Atexo_InterfaceWs_Error;
use Exception;
use Prado\Prado;

class InterfacesAnnonceMarches
{
    /**
     * @author Mouslim MITALI <mouslim.mitali@atexo.com>
     * Retourne un xml qui contient la reponse PMI.
     *
     * @param string $xml requete
     *
     * @return string $return contient la réponse PMI
     * @soapmethod
     */
    public function processXml($xml)
    {
        $xmlRetour = null;
        if (true || Atexo_Util::validateXml($xml, Atexo_Config::getParameter('XSD_INTERFACES_ANNONCE_MARCHES'), false)) {
            try {
                $xmlReqContent = simplexml_load_string($xml);
                if ($xmlReqContent) {
                    $request = $xmlReqContent->Request;
                    if (isset($request->Tender)) {
                        $tenders = new Atexo_InterfaceWs_Consultation();
                        $xmlRetour = $tenders->getXmlTender($request->Tender, true);
                    }
                }
            } catch (Exception $e) {
                $errorsWs = new Atexo_InterfaceWs_Error();
                $xmlRetour = $errorsWs->getXmlError('TECHNICAL_ERROR', $e->getMessage());
            }
        } else {
            $errorsWs = new Atexo_InterfaceWs_Error();
            $xmlRetour = $errorsWs->getXmlError('MISSING_PARAMETERS', Prado::localize('TEXT_FICHIER_XML_TRAMSMIS_VIDE'));
        }
        if ($xmlRetour) {
            do {
                $fileName = 'WebServiceAnnoncesMarches.xml';
            } while (is_file($fileName));
            if (!is_dir(Atexo_Config::getParameter('COMMON_TMP').'/testAnnonceMarche/')) {
                mkdir(Atexo_Config::getParameter('COMMON_TMP').'/testAnnonceMarche/');
            }

            $dirSave = Atexo_Config::getParameter('COMMON_TMP').'/testAnnonceMarche/tmpWSAnnoncesMarches'.date('Y_d_m_H_i_s').'_'.uniqid();
            mkdir($dirSave);
            Atexo_Util::write_file($dirSave.'/'.$fileName, $xmlRetour);
            (new Atexo_Zip())->addFileToZip($dirSave.'/'.$fileName, $dirSave.'/'.$fileName.'.zip');
            $xmlZip = file_get_contents($dirSave.'/'.$fileName.'.zip');
            unlink($dirSave.'/'.$fileName.'.zip');

            return base64_encode($xmlZip);
        }
    }
}
