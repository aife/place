<?php

namespace Application\Service\Atexo\WebService;

use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_Zip;
use Application\Service\Atexo\InterfaceWs\Atexo_InterfaceWs_ConsultationAgent;
use Application\Service\Atexo\InterfaceWs\Atexo_InterfaceWs_Error;
use Exception;
use Prado\Prado;

class InterfacesSDL
{
    /**
     * @author Mouslim MITALI <mouslim.mitali@atexo.com>
     * Retourne un xml qui contient la reponse PMI.
     *
     * @param string $xml requete
     *
     * @return string $return contient la réponse PMI
     * @soapmethod
     */
    public function processXml($xml)
    {
        $xmlRetour = null;
        //return $xml;
        if (true || Atexo_Util::validateXml($xml, Atexo_Config::getParameter('XSD_INTERFACES_SOCLE_INTERNE'), false)) {
            try {
                $xmlReqContent = simplexml_load_string($xml);
                if ($xmlReqContent) {
                    $request = $xmlReqContent->Request;
                    if (isset($request->Tender)) {
                        $tenders = new Atexo_InterfaceWs_ConsultationAgent();
                        $xmlRetour = $tenders->getXmlTenderAgent($request->Tender);
                    }
                }
            } catch (Exception $e) {
                $errorsWs = new Atexo_InterfaceWs_Error();
                $xmlRetour = $errorsWs->getXmlError('TECHNICAL_ERROR', $e->getMessage());
            }
        } else {
            $errorsWs = new Atexo_InterfaceWs_Error();
            $xmlRetour = $errorsWs->getXmlError('MISSING_PARAMETERS', Prado::localize('TEXT_FICHIER_XML_TRAMSMIS_VIDE'));
        }
        if ($xmlRetour) {
            do {
                $fileName = 'WebServiceInterfaceDefense'.uniqid();
            } while (is_file($fileName));

            Atexo_Util::write_file(Atexo_Config::getParameter('COMMON_TMP').$fileName, $xmlRetour);
            (new Atexo_Zip())->addFileToZip(Atexo_Config::getParameter('COMMON_TMP').$fileName, Atexo_Config::getParameter('COMMON_TMP').$fileName.'.zip');
            $xmlZip = file_get_contents(Atexo_Config::getParameter('COMMON_TMP').$fileName.'.zip');
            unlink(Atexo_Config::getParameter('COMMON_TMP').$fileName.'.zip');

            return base64_encode($xmlZip);
        }
    }
}
