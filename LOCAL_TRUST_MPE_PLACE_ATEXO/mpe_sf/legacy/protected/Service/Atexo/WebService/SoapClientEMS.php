<?php

namespace Application\Service\Atexo\WebService;

use SoapVar;

/*
 * Created on 28 nov. 2012
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */

class Atexo_WebService_SoapClientEMS extends \SoapClient
{
    const EMS_WS_NAMESPACE = 'http://ws.ems6.net/';

    public function __construct($Login, $Password, $Idmlist, $wsdl, $options = [])
    {
        parent::__construct($wsdl, $options);
        $this->__setSoapHeaders($this->createHeader($Login, $Password, $Idmlist));
    }

    public function createHeader($Login, $Password, $Idmlist)
    {
        $struct = null;
        $struct->UserName = new SoapVar($Login, XSD_STRING, null, null, null, self::EMS_WS_NAMESPACE);
        $struct->Password = new SoapVar($Password, XSD_STRING, null, null, null, self::EMS_WS_NAMESPACE);
        $struct->IdMlist = new SoapVar($Idmlist, XSD_INTEGER, null, null, null, self::EMS_WS_NAMESPACE);
        $header_values = new SoapVar($struct, SOAP_ENC_OBJECT, null, null, null, self::EMS_WS_NAMESPACE);
        $header = new \SoapHeader(self::EMS_WS_NAMESPACE, 'AuthHeader', $header_values); //print_r($header);exit;

        return $header;
    }
}
