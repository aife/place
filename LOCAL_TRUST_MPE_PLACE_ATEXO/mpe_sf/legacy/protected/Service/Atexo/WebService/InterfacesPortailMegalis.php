<?php

namespace Application\Service\Atexo\WebService;

use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_Zip;
use Application\Service\Atexo\InterfaceWs\Atexo_InterfaceWs_Authentication;
use Application\Service\Atexo\InterfaceWs\Atexo_InterfaceWs_Entites;
use Application\Service\Atexo\InterfaceWs\Atexo_InterfaceWs_Error;
use Exception;
use Prado\Prado;

class InterfacesPortailMegalis
{
    /**
     * @author Mounir LAMANE <mounir.lamane@atexo.com>
     * Retourne un xml qui contient la reponse PMI.
     *
     * @param string $xml requete
     *
     * @return string $return contient la réponse PMI
     * @soapmethod
     */
    public function processXml($xml)
    {
        $xmlRetour = null;
        if ($xml) {
            try {
                $xmlReqContent = simplexml_load_string($xml);
                if (Atexo_Util::validateXml($xml, Atexo_Config::getParameter('XSD_INTERFACES_SOCLE_INTERNE'), false)) {
                    if ($xmlReqContent) {
                        $request = $xmlReqContent->Request;
                        if (isset($request->Authentication)) {
                            $authentication = new Atexo_InterfaceWs_Authentication();
                            $typeUser = $request->Authentication->attributes()->type;
                            if (isset($request->Authentication->Password)) {
                                $xmlRetour = $authentication->getXmlByPassword($request->Authentication->Password, $typeUser);
                            } elseif (isset($request->Authentication->Token)) {
                                $xmlRetour = $authentication->getXmlByToken($request->Authentication->Token, $typeUser);
                            }
                        } elseif (isset($request->Entities)) {
                            $entities = new Atexo_InterfaceWs_Entites();
                            $xmlRetour = $entities->getXmlEntities($request->Entities);
                        } else {
                            $message = 'XML non valide : '.Atexo_Util::validateXml($xml, Atexo_Config::getParameter('XSD_INTERFACES_SOCLE_INTERNE'));
                            $this->setError($message);
                            $errorsWs = new Atexo_InterfaceWs_Error();
                            $xmlRetour = $errorsWs->getXmlError('INVALID_MESSAGE', Atexo_Util::validateXml($xml, Atexo_Config::getParameter('XSD_INTERFACES_SOCLE_INTERNE'), true));
                        }
                    }
                } else {
                    $message = 'XML non valide : '.Atexo_Util::validateXml($xml, Atexo_Config::getParameter('XSD_INTERFACES_SOCLE_INTERNE'));
                    $this->setError($message);
                    $errorsWs = new Atexo_InterfaceWs_Error();
                    $xmlRetour = $errorsWs->getXmlError('INVALID_MESSAGE', Atexo_Util::validateXml($xml, Atexo_Config::getParameter('XSD_INTERFACES_SOCLE_INTERNE'), true));
                }
            } catch (Exception $e) {
                $message = 'Erreur technique : '.$e->getMessage();
                $this->setError($message);
                $errorsWs = new Atexo_InterfaceWs_Error();
                $xmlRetour = $errorsWs->getXmlError('TECHNICAL_ERROR', $e->getMessage());
            }
        } else {
            $message = 'Paramètre manquant : '.Prado::localize('TEXT_FICHIER_XML_TRAMSMIS_VIDE');
            $this->setError($message);
            $errorsWs = new Atexo_InterfaceWs_Error();
            $xmlRetour = $errorsWs->getXmlError('MISSING_PARAMETERS', Prado::localize('TEXT_FICHIER_XML_TRAMSMIS_VIDE'));
        }
        if ($xmlRetour) {
            do {
                $fileName = 'InterfacesPortailMegalis'.uniqid();
            } while (is_file($fileName));
            $tmpFileName = Atexo_Config::getParameter('COMMON_TMP').$fileName;
            $success = Atexo_Util::write_file($tmpFileName, $xmlRetour);
            if (empty($success)) {
                $message = "Le fichier '".$tmpFileName."' ne peut pas être écrit !";
                $this->setError($message);
                $errorsWs = new Atexo_InterfaceWs_Error();

                return $xmlRetour = $errorsWs->getXmlError('TECHNICAL_ERROR', "Erreur technique 1 s'est produit veuillez vérifier les logs !");
            }
            $successZip = (new Atexo_Zip())->addFileToZip($tmpFileName, $tmpFileName.'.zip');
            if (empty($successZip)) {
                $message = "Le fichier zip '".$tmpFileName.".zip' ne peut pas être écrit !";
                $this->setError($message);
                $errorsWs = new Atexo_InterfaceWs_Error();

                return $xmlRetour = $errorsWs->getXmlError('TECHNICAL_ERROR', "Erreur technique 2 s'est produit veuillez vérifier les logs !");
            }
            if (!is_dir(Atexo_Config::getParameter('COMMON_TMP'))) {
                $message = "Le répertoire '".Atexo_Config::getParameter('COMMON_TMP')."' n'existe pas !";
                $this->setError($message);
                $errorsWs = new Atexo_InterfaceWs_Error();

                return $xmlRetour = $errorsWs->getXmlError('TECHNICAL_ERROR', "Erreur technique 3 s'est produit veuillez vérifier les logs !");
            }
            $xmlZip = file_get_contents($tmpFileName.'.zip');
            if (false == $xmlZip) {
                $message = "Le fichier '".$tmpFileName.".zip' n'a pas pu être ouvert !";
                $this->setError($message);
                $errorsWs = new Atexo_InterfaceWs_Error();

                return $errorsWs->getXmlError('TECHNICAL_ERROR', "Erreur technique 4 s'est produit veuillez vérifier les logs !");
            }
            if (!unlink($tmpFileName.'.zip')) {
                $message = "Le fichier '".$tmpFileName.".zip' n'a pas pu être supprimé !";
                $this->setError($message);
                $errorsWs = new Atexo_InterfaceWs_Error();

                return $errorsWs->getXmlError('TECHNICAL_ERROR', "Erreur technique 5 s'est produit veuillez vérifier les logs !");
            }
            if (!unlink($tmpFileName)) {
                $message = "Le fichier '".$tmpFileName.".zip' n'a pas pu être ouvert !";
                $this->setError($message);
                $errorsWs = new Atexo_InterfaceWs_Error();

                return $errorsWs->getXmlError('TECHNICAL_ERROR', "Erreur technique 6 s'est produit veuillez vérifier les logs !");
            }
            $xmlRetour = base64_encode($xmlZip);
        }

        return $xmlRetour;
    }

    private function setError($message)
    {
        $error = "Erreur lors de l'appel de l'interfacesPortailMegalis :".PHP_EOL.$message.PHP_EOL;
        $logger = Atexo_LoggerManager::getLogger('webservices');
        $logger->error($error);
    }
}
