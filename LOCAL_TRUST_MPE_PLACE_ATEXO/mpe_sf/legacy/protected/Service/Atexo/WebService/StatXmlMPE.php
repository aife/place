<?php

namespace Application\Service\Atexo\WebService;

use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Interfaces\Atexo_Interfaces_GenerateXmlStats;
use Application\Service\Atexo\Statistiques\Atexo_Statistiques_StatistiquesIndicateursCles;

/**
 * commentaires.
 *
 * @author othmane abisourour <othmane.abisourour@atexo.com>
 * @copyright Atexo 2009
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_WebService_StatXmlMPE
{
    /**
     * Retourne un xml qui contient les donnees a affiche sur le socle.
     *
     * @param string $xml requete socle
     *
     * @return string $xmlStats
     * @soapmethod
     */
    public function xmlInterfaceStats($xml)
    {
        $temps_debut = microtime(true);
        if ($xml) {
            $data = (new Atexo_Interfaces_GenerateXmlStats())->getDataFromXml($xml);
            if ($data['messageAction'] == Atexo_Config::getParameter('EXPORT_STATISTIQUES_AGENT_MPE')) {
                $statsArray = (new Atexo_Statistiques_StatistiquesIndicateursCles())->getStatistiquesIndicateursClesAgents($data['email']);
                $donnee = array_merge([$data['login'], $data['pass'], $data['email'], 'REPONSE', $data['messageAction']], $statsArray);
                $xmlStats = (new Atexo_Interfaces_GenerateXmlStats())->generateXmlAgentMPE($donnee);
                $temps_fin = microtime(true);
                $txt = 'Debut xmlInterfaceStats for messageAction=EXPORT_STATISTIQUES_AGENT_MPE et fin : '.round($temps_fin - $temps_debut, 4)."\n\n";
                $fp = fopen(Atexo_Config::getParameter('LOG_DIR').session_id().'timer.txt', 'a');
                fwrite($fp, $txt);
                fclose($fp);

                return $xmlStats;
            } elseif ($data['messageAction'] == Atexo_Config::getParameter('EXPORT_STATISTIQUES_PORTAIL')) {
                $dateDebut = Atexo_Config::getParameter('DATE_DEBUT_PORTAIL');
                $statsArray = (new Atexo_Statistiques_StatistiquesIndicateursCles())->getStatistiquesIndicateursClesEntreprises($dateDebut);
                $donnee = array_merge([$data['login'], $data['pass'], $data['email'], 'REPONSE', $data['messageAction']], $statsArray);
                $xmlStats = (new Atexo_Interfaces_GenerateXmlStats())->generateXmlPortail($donnee);
                $temps_fin = microtime(true);
                $txt = 'Debut xmlInterfaceStats for messageAction=EXPORT_STATISTIQUES_PORTAIL et fin : '.round($temps_fin - $temps_debut, 4)."\n\n";
                $fp = fopen(Atexo_Config::getParameter('LOG_DIR').session_id().'timer.txt', 'a');
                fwrite($fp, $txt);
                fclose($fp);

                return $xmlStats;
            }
        }

        return false;
    }
}
