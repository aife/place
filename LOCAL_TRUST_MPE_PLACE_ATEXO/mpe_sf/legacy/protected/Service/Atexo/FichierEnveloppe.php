<?php

namespace Application\Service\Atexo;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonBlocFichierEnveloppe;
use Application\Propel\Mpe\CommonBlocFichierEnveloppePeer;
use Application\Propel\Mpe\CommonFichierEnveloppe;
use Application\Propel\Mpe\CommonFichierEnveloppePeer;
use Application\Propel\Mpe\CommonFichierEnveloppeQuery;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Responses;
use Application\Service\Atexo\Rest\Action\Atexo_Rest_Action_Actions;
use Exception;
use PDO;

class Atexo_FichierEnveloppe
{
    /*
     * retourne un objet CommonFichierEnveloppe a partir de son id
     * @param $id(int) : id du FichierEnveloppe
     * @param $acronymeOrg(string) : acronyme de l'organisme authentifie
     * @return $fichierEnveloppe(CommonFichierEnveloppe) : objet de CommonFichierEnveloppe
     * @author : ASO : <ayoub.souidahmed@atexo.com>
     * @version : depuis 4.6.0
     */
    public function getFichierEnveloppeById($id, $acronymeOrg = null)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $c = new Criteria();
        $c->add(CommonFichierEnveloppePeer::ID_FICHIER, $id);
        if (null !== $acronymeOrg) {
            $c->add(CommonFichierEnveloppePeer::ORGANISME, $acronymeOrg, Criteria::EQUAL);
        }
        $fichierEnveloppe = CommonFichierEnveloppePeer::doSelectOne($c, $connexion);
        if ($fichierEnveloppe) {
            return $fichierEnveloppe;
        } else {
            return false;
        }
    }

    /**
     * Permet d'ajouter les informations du pli pour le ws.
     *
     * @param $id
     * @param $xmlResponse
     * @param $arrayBlob
     * @param $logger
     * @param string $organisme Acronyme de l'organisme connecte au WS
     *
     * @return bool|string un message dans le cas d'erreur
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function setInfoPliForWs($id, &$xmlResponse, &$arrayBlob, &$logger, $organisme = null, $chiffrer = true, &$contentType = 'application/octet-stream', $withFile = true, $typeChecksum = 'md5'): bool|string
    {
        $logger->info("La récupération du pli dont l'id = ".$id);
        $pli = self::getFichierEnveloppeById($id, $organisme);
        if ($pli instanceof CommonFichierEnveloppe) {
            $logger->info('Le pli existe');
            if ($withFile) {
                $logger->info('la recuperation de pli avec fichiers');
                if ($chiffrer) {
                    $logger->info('La recuperation du Pli chiffrer');
                    $blocsFichierEnveloppe = (new Atexo_Consultation_Responses())->getBlocsFichiersEnveloppesByIdEnveloppe($id, $organisme);
                    if (is_array($blocsFichierEnveloppe) && count($blocsFichierEnveloppe)) {
                        $logger->info('Début de la recuperation des blocs du Pli');
                        foreach ($blocsFichierEnveloppe as $blocFichierEnveloppe) {
                            $arrayBlob[] = Atexo_Blob::getBlobById($blocFichierEnveloppe->getIdBlobChiffre(), $blocFichierEnveloppe->getOrganisme(), null, false);
                        }
                        $logger->info('Fin de la recuperation des blocs du Pli');
                    } else {
                        return "les blocs fichiers du pli dont l'id = ".$id." n'existent pas";
                    }
                } else {
                    $logger->info('La recuperation du Pli en clair');
                    if ($pli->getIdBlob()) {
                        $arrayBlob[] = Atexo_Blob::getBlobById($pli->getIdBlob(), $pli->getOrganisme(), $pli->getNomFichier(), false);
                        $contentType = Atexo_Util::getContentTypeByExtension(Atexo_Util::getExtension($pli->getNomFichier()));
                    } else {
                        return "le fichier en clair du pli dont l'id = ".$id." n'existe pas";
                    }
                }
            } else {
                $logger->info("la recuperation d'info pli sans fichiers");
            }
            $chiffrementOffre = Atexo_Util::getBooleanStringFromValInt((new Atexo_Consultation_Responses())->getChiffrementConsultationByIdEnveloppe($pli->getIdEnveloppe(), $pli->getOrganisme()));
            $logger->info("Mettre les informations du pli dans l'objet Atexo_Rest_ObjectField_DocumentField");

            $documentField = (new Atexo_Document())->getDocumentFieldByInfo($pli->getIdFichier(), $pli->getNomFichier(), $pli->getOrganisme(), $pli->getIdBlob(), null, $pli->getLibelleTypeFichier(), $pli->getTailleFichier(), null, $chiffrementOffre, $pli->getSignatureFichier(), $pli->getHash(), $pli->getVerificationCertificat(), $typeChecksum);
            $logger->info("La récupération de l'xml contenant les informations du pli");
            $xmlResponse = (new Atexo_Rest_Action_Actions())->getXmlInfosObjectField($documentField);

            return false;
        } else {
            return "Le pli dont l'id = ".$id." n'existe pas";
        }
    }

    /**
     * permet d'insérer les blocs chiffres.
     *
     * @param int    $idFichier   l'id du fichier
     * @param string $organisme   organisme
     * @param int    $numeroOrdre numero ordre bloc
     * @param string $bloc        bloc chiffre
     *
     * @return mixed id bloc ou false cas d'erreur
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public static function insertBlocChiffre($idFichier, $organisme, $numeroOrdre, $bloc)
    {
        try {
            $logger = Atexo_LoggerManager::getLogger('crypto');
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            if (!is_dir(Atexo_Config::getParameter('COMMON_TMP'))) {
                mkdir(Atexo_Config::getParameter('COMMON_TMP'));
            }
            $file = 'bloc_'.$idFichier.$organisme.$numeroOrdre.time();
            $atexoBlob = new Atexo_Blob();
            $idBlocBlob = $atexoBlob->insert_blob(
                $file,
                $bloc,
                $organisme,
                null,
                Atexo_Config::getParameter('EXTENSION_CHIFFRE')
            );
            if ($idBlocBlob) {
                $commonBlocFichier = Atexo_FichierEnveloppe::getBlocFichierEnveloppe($idFichier, $organisme, $numeroOrdre);
                if (!$commonBlocFichier instanceof CommonBlocFichierEnveloppe) {
                    $commonBlocFichier = new CommonBlocFichierEnveloppe();
                } else {
                    (new Atexo_Blob())->deleteBlobFile($commonBlocFichier->getIdBlobChiffre(), $organisme);
                }
                $commonBlocFichier->setIdFichier($idFichier);
                $commonBlocFichier->setNumeroOrdreBloc($numeroOrdre);
                $commonBlocFichier->setOrganisme($organisme);
                $commonBlocFichier->setIdBlobChiffre($idBlocBlob);
                $commonBlocFichier->save($connexion);

                return $idBlocBlob;
            } else {
                $logger->error("une erreur s'est produite lors de l'insertion du blob pour le fichier ".$idFichier);

                return false;
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * cherche un objet blocFichierEnveloppe.
     *
     * @param int    $idFichier    l'id du fichier
     * @param string $organisme    organisme
     * @param int    $numOrdreBloc numero ordre bloc
     *
     * @return CommonBlocFichierEnveloppePeer| NULL
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public static function getBlocFichierEnveloppe($idFichier, $organisme, $numOrdreBloc)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonBlocFichierEnveloppePeer::ID_FICHIER, $idFichier);
        $c->add(CommonBlocFichierEnveloppePeer::ORGANISME, $organisme, Criteria::EQUAL);
        $c->add(CommonBlocFichierEnveloppePeer::NUMERO_ORDRE_BLOC, $numOrdreBloc);

        return CommonBlocFichierEnveloppePeer::doSelectOne($c, $connexion);
    }

    /**
     * cherche un objet blocFichierEnveloppe.
     *
     * @param int    $idFichier    l'id du fichier
     * @param string $organisme    organisme
     * @param int    $numOrdreBloc numero ordre bloc
     *
     * @return CommonBlocFichierEnveloppePeer| NULL
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public static function deleteBlobsFichiersNonChiffres($idOffre, $organisme)
    {
        $id = null;
        try {
            $logger = Atexo_LoggerManager::getLogger('crypto');
            $logger->info('suppression fichiers id offre  :  '.$idOffre.' organisme : '.$organisme);

            $sql = <<<QUERY
SELECT
	fichierEnveloppe.id_blob From fichierEnveloppe , Enveloppe
WHERE
		Enveloppe.offre_id = :idOffre
	AND
		Enveloppe.organisme = :organisme
	AND
		fichierEnveloppe.id_enveloppe = Enveloppe.id_enveloppe_electro
	AND
		fichierEnveloppe.organisme = Enveloppe.organisme
	AND
		fichierEnveloppe.id_blob is not null
	AND
		fichierEnveloppe.type_fichier != 'SIG'
	

QUERY;
            $link = Atexo_Db::getLinkCommon();
            $statement = $link->prepare($sql);
            $statement->execute([':idOffre' => $idOffre, ':organisme' => $organisme]);
            $result = $statement->fetchAll(PDO::FETCH_ASSOC);
            $id = 0;
            foreach ($result as $idBlob) {
                $id = $idBlob['id_blob'];
                $logger->info('debut suppression fichier :  '.$id);
                (new Atexo_Blob())->deleteBlobFile($id, $organisme);
                $logger->info('fin suppression fichier :  '.$id);
            }
            $sqlUpdate = <<<QUERY
UPDATE
	fichierEnveloppe, Enveloppe
SET
	fichierEnveloppe.id_blob = null
WHERE
		Enveloppe.offre_id = :idOffre
	AND
		Enveloppe.organisme = :organisme
	AND
		fichierEnveloppe.id_enveloppe = Enveloppe.id_enveloppe_electro
	AND
		fichierEnveloppe.organisme = Enveloppe.organisme
	AND
		fichierEnveloppe.type_fichier != 'SIG'
QUERY;
            $statement = $link->prepare($sqlUpdate);

            return $statement->execute([':idOffre' => $idOffre, ':organisme' => $organisme]);
        } catch (Exception) {
            $logger->error('Erreur  deleteBlobsFichiersNonChiffres  id blob :  '.$id);
        }
    }

    /**
     * permet d'insérer les blocs chiffres.
     *
     * @param CommonFichierEnveloppe $fichierEnveloppe
     * @param string                 $file             chemin du fichier
     *
     * @return mixed id bloc ou false cas d'erreur
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public static function insertFichierDeciffre(&$fichierEnveloppe, $file)
    {
        try {
            if ($fichierEnveloppe instanceof CommonFichierEnveloppe) {
                $logger = Atexo_LoggerManager::getLogger('crypto');
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                $atexoBlob = new Atexo_Blob();
                $idBlob = $atexoBlob->insert_blob($fichierEnveloppe->getNomFichier(), $file, $fichierEnveloppe->getOrganisme(), null, Atexo_Config::getParameter('EXTENSION_DECHIFFRE'));
                $logger->info('Dechifrement insertion idFichier '.$fichierEnveloppe->getIdFichier().' path : '.$file.' dans idBlob : '.$idBlob);
                if ($idBlob) {
                    $fichierEnveloppe->setIdBlob($idBlob);
                    $logger->info('Dechifrement verification du hash du fichier '.$fichierEnveloppe->getIdFichier().' dans idBlob : '.$idBlob);
                    $verificationHash = (new Atexo_Crypto())->verifyHashValidity($idBlob, $fichierEnveloppe->getHash(), $fichierEnveloppe->getOrganisme()) ? '1' : '0';
                    $logger->info('Dechifrement resultat de la verification du hash du fichier '.$fichierEnveloppe->getIdFichier().' dans idBlob : '.$idBlob.' => '.$verificationHash);
                    $fichierEnveloppe->setResultatVerificationHash($verificationHash);
                    $fichierEnveloppe->save($connexion);
                    if (0 == $fichierEnveloppe->getTypePiece()) {
                        $fichierOrgine = self::getFichierEnveloppeByIdFichierSignature($fichierEnveloppe->getIdFichier(), $connexion);
                        if ($fichierOrgine instanceof CommonFichierEnveloppe) {
                            $fichierOrgine->setIdBlobSignature($idBlob);
                            $fichierOrgine->save($connexion);
                        }
                    }

                    return true;
                } else {
                    $logger->error("une erreur s'est produite lors de l'insertion du fichier ".$file);

                    return false;
                }
            } else {
                return false;
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function getFichierEnveloppeByIdFichierSignature($idFichierSignature, $connexion = null)
    {
        $fichierEnvQUery = new CommonFichierEnveloppeQuery();

        return $fichierEnvQUery->getFichierEnveloppeByIdFichierSignature($idFichierSignature, $connexion);
    }

    public static function getIdBlobFichiersByIdEnveloppe($idEnveloppe, $org)
    {
        $sql = <<<QUERY
SELECT
	fichierEnveloppe.id_blob From fichierEnveloppe , Enveloppe
WHERE
		Enveloppe.id_enveloppe_electro = :idEnveloppeElectro
	AND
		Enveloppe.organisme = :organisme
	AND
		fichierEnveloppe.id_enveloppe = Enveloppe.id_enveloppe_electro
	AND
		fichierEnveloppe.organisme = Enveloppe.organisme
QUERY;
        $link = Atexo_Db::getLinkCommon();
        $statement = $link->prepare($sql);
        $statement->execute([':idEnveloppeElectro' => $idEnveloppe, ':organisme' => $org]);

        return $statement->fetchAll(PDO::FETCH_COLUMN);
    }

    /**
     * @param $idOffre
     * @param $organisme
     */
    public static function changeExtensionJeton($idOffre, $organisme)
    {
        try {
            $logger = Atexo_LoggerManager::getLogger('crypto');
            $logger->info(
                'Changement de l\'extension des jetons de signatures  :  '
                .$idOffre
                .' organisme : '
                .$organisme
            );

            $sql = <<<QUERY
SELECT
	fichierEnveloppe.id_blob From fichierEnveloppe , Enveloppe
WHERE
		Enveloppe.offre_id = :idOffre
	AND
		Enveloppe.organisme = :organisme
	AND
		fichierEnveloppe.id_enveloppe = Enveloppe.id_enveloppe_electro
	AND
		fichierEnveloppe.organisme = Enveloppe.organisme
	AND
		fichierEnveloppe.id_blob is not null
	AND
		fichierEnveloppe.type_fichier = 'SIG'
	

QUERY;
            $link = Atexo_Db::getLinkCommon();
            $statement = $link->prepare($sql);
            $statement->execute([':idOffre' => $idOffre, ':organisme' => $organisme]);
            $result = $statement->fetchAll(PDO::FETCH_ASSOC);
            $id = 0;
            foreach ($result as $idBlob) {
                $id = $idBlob['id_blob'];
                $subFolder = null;
                $logger->info('Début changement de l\'extension du jeton  :  '.$id);
                (new Atexo_Blob())->changeExtensionBlob($id, $organisme, Atexo_Config::getParameter('EXTENSION_SIGNATURE'), $subFolder);
                $logger->info('Fin changement de l\'extension du jeton  :  '.$id);
            }
        } catch (Exception) {
            $logger->error('Erreur  lors du changement de l\'extension sur les jeton de l\'offres  :  '
                .$idOffre);
        }
    }
}
