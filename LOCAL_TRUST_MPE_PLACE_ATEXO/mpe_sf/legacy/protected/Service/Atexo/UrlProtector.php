<?php

namespace Application\Service\Atexo;

class Atexo_UrlProtector
{
    private static int $secret_key = 12;
    private static string $url_list_file = 'protected/var/cache/urllist.txt';

    private static function generateUrlHash($url)
    {
        return md5($_SERVER['DOCUMENT_ROOT'].$_SERVER['SERVER_NAME'].$_SERVER['HTTP_COOKIE'].$url.self::$secret_key);
    }

    private static function storeProtectedUrl($url)
    {
        $query = explode('&', $url);
        foreach ($query as $q) {
            [$key] = explode('=', $q);
            if ('page' != $key) {
                $url .= '&'.$key;
            } else {
                $url .= $q;
            }
        }
        $list = file(self::$url_list_file, FILE_IGNORE_NEW_LINES);
        if (false === array_search($url, $list)) {
            file_put_contents(self::$url_list_file, $url."\n", FILE_APPEND);
        }
    }

    public static function protectUrl($query_string)
    {
        self::storeProtectedUrl($query_string);

        return "$query_string&h=".self::generateUrlHash($query_string);
    }

    public static function isUrlProtected($url)
    {
        $listing = [];
        $list = file(self::$url_list_file, FILE_IGNORE_NEW_LINES);
        foreach ($list as $line) {
            $page = '';
            $qs = [];
            $query = explode('&', $line);
            foreach ($query as $q) {
                [$key, $value] = explode('=', $q);
                if ('page' == $key) {
                    $page = $value;
                } elseif ('h' != $key) {
                    $qs[] = $key;
                }
            }
            $listing[$page][] = $qs;
        }
        if (isset($listing[$url['page']])) {
            foreach ($listing[$url['page']] as $elt) {
                $all_ok = 1;
                foreach ($elt as $param) {
                    if (!isset($url[$param])) {
                        $all_ok = 0;
                    }
                }
                if (1 == $all_ok) {
                    return true;
                }
            }
        }

        return false;
    }

    public static function validateUrl($query_string)
    {
        $url = null;
        foreach ($query_string as $q => $v) {
            if ('h' != $q) {
                $url .= '&'.$q;
                if ('' != $v) {
                    $url .= '='.$v;
                }
            }
        }
        if ($_GET['h'] != self::generateUrlHash(str_replace('&page', 'page', $url))) {
            echo 'Ressource interdite';
            exit();
        }
    }
}

/*if (isset($_GET) && count($_GET) > 1) {
    if (Atexo_UrlProtector::isUrlProtected($_GET)) {return Atexo_UrlProtector::validateUrl($_GET);}
}*/
