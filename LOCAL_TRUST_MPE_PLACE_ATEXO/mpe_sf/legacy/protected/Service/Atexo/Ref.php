<?php

namespace Application\Service\Atexo;

use DOMDocument;
use Prado\TPropertyValue;

/**
 * Class Atexo_Ref.
 *
 * @author LCH
 */
class Atexo_Ref
{
    private string $_clef = '';
    /**
     * @var string
     */
    private $_locale = '';
    private string $_cheminFichierConfigXML = '';
    private string $_styleCSS = '';
    /**
     * @var string
     */
    private $_urlBase = '';
    /**
     * @var string
     */
    private $_principal = '';
    /**
     * @var string
     */
    private $_secondaires = '';
    /**
     * @var string
     */
    private $_urlLogo = '';
    /**
     * @var string
     */
    private $_urlPf = '';

    /**
     * Constructeur.
     *
     * @param $local
     * @param $styleCss
     * @param $urlBase
     *
     * @return unknown_type
     */
    public function __construct(
        $local = '',
        $styleCss = '/themes/cpv/css/cpv.css',
        $urlBase = ''
    ) {
        $urlPF = ('/' == substr(Atexo_Config::getParameter('PF_URL'), -1)) ?
        substr(Atexo_Config::getParameter('PF_URL'), 0, -1) : Atexo_Config::getParameter('PF_URL');
        if (!$local) {
            $local = Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE');
        }
        //par défaut si urlBase non renseignée on prend l'url centrale qui met à disposition les référentiels
        //si jamais le contenu des référentiels est à spécialiser pour le client soit mettre les fichiers
        // spécifiques au niveau du serveur central soit les mettre dans le projet client et changer
        //le paramètre dans application.xml.php
        if (!$urlBase) {
            $urlBase = ('/' == substr(Atexo_Config::getParameter('URL_CENTRALE_REFERENTIELS'), -1)) ?
                substr(Atexo_Config::getParameter('URL_CENTRALE_REFERENTIELS'), 0, -1) : Atexo_Config::getParameter('URL_CENTRALE_REFERENTIELS');
            if (Atexo_Config::getParameter('SPECIFIQUE_PLACE')) {
                $urlBase = ('/' == substr(Atexo_Config::getParameter('URL_DOCS'), -1)) ?
                    substr(Atexo_Config::getParameter('URL_DOCS'), 0, -1) : Atexo_Config::getParameter('URL_DOCS');
            }
        }
        $this->_locale = $local;

        //l'url de la css dépend du client et doit toujours être prise depuis la plate-forme appellante
        $this->_styleCSS = $urlPF.$styleCss;

        $this->_urlBase = $urlBase;
        $this->_urlPf = $urlPF;
    }

    /**
     * Destructeur.
     *
     * @return unknown_type
     */
    public function __destruct()
    {
        unset($this->_locale);
        unset($this->_styleCSS);
        unset($this->_urlBase);
    }

    /**
     * @return string defaults to ''
     */
    public function getClef()
    {
        return $this->_clef;
    }

    /**
     * @param string
     */
    public function setClef($value)
    {
        $this->_clef = TPropertyValue::ensureString($value);
    }

    /**
     * @return string defaults to ''
     */
    public function getLocale()
    {
        return $this->_locale;
    }

    /**
     * @param string
     */
    public function setLocale($value)
    {
        $this->_locale = TPropertyValue::ensureString($value);
    }

    /**
     * @return string defaults to ''
     */
    public function getCheminFichierConfigXML()
    {
        return $this->_cheminFichierConfigXML;
    }

    /**
     * @param string
     */
    public function setCheminFichierConfigXML($value)
    {
        $this->_cheminFichierConfigXML = TPropertyValue::ensureString($value);
    }

    /**
     * @return string defaults to ''
     */
    public function getStyleCSS()
    {
        return $this->_styleCSS;
    }

    /**
     * @param string
     */
    public function setStyleCSS($value)
    {
        $this->_styleCSS = TPropertyValue::ensureString($value);
    }

    /**
     * @return string defaults to ''
     */
    public function getUrlBase()
    {
        return $this->_urlBase;
    }

    /**
     * @param string
     */
    public function setUrlBase($value)
    {
        $this->_urlBase = TPropertyValue::ensureString($value);
    }

    /**
     * @return string defaults to ''
     */
    public function getPrincipal()
    {
        return $this->_principal;
    }

    /**
     * @param string
     */
    public function setPrincipal($value)
    {
        $this->_principal = $value;
    }

    /**
     * @return string defaults to ''
     */
    public function getSecondaires()
    {
        return $this->_secondaires;
    }

    /**
     * @param string
     */
    public function setSecondaires($value)
    {
        $this->_secondaires = $value;
    }

    /**
     * @return string defaults to ''
     */
    public function getUrlLogo()
    {
        return $this->_urlLogo;
    }

    /**
     * @param string
     */
    public function setUrlLogo($value)
    {
        $this->_urlLogo = $value;
    }

    /**
     * @return string defaults to ''
     */
    public function getUrlPf()
    {
        return $this->_urlPf;
    }

    /**
     * @param string
     */
    public function setUrlPf($value)
    {
        $this->_urlPf = $value;
    }

    public function getUrlReferentiel()
    {
        $urlRef = Atexo_Config::getParameter('URL_REFERENTIEL');
        $urlRef .= '&clef='.$this->getClef();
        $urlRef .= '&locale='.$this->getLocale();
        $urlRef .= '&cheminFichierConfigXML='.$this->getCheminFichierConfigXML();
        $urlRef .= '&urlBase='.$this->getUrlBase();
        $urlRef .= '&styleCSS='.$this->getStyleCSS();
        //logo du referentiel
        if ($this->getUrlLogo()) {
            $urlRef .= '&logo='.$this->_urlPf.'/'.$this->getUrlLogo();
        }

        return $urlRef;
    }

    /**
     * @param $file
     * @param $delimiter
     * @param $rangCode
     * @param $rangLibelle
     * @param $codeRef
     *
     * @return string
     */
    public function getLibelleRef($codeRef, $file, $delimiter = '|', $rangCode = '1', $rangLibelle = '3')
    {
        if (null != $codeRef) {
            $data = self::searchCsvFile($codeRef, $file, $delimiter, $rangCode);
            if (is_array($data) && $data) {
                //le code recherché n'existe que sur un seule ligne du fichier csv
                $arrayCsvLine = $data[0];
                if (is_array($arrayCsvLine) && $arrayCsvLine) {
                    return Atexo_Util::setInIsoIfEncodingUTF8($arrayCsvLine[$rangLibelle]);
                }
            }
            //si nous arrivons à ce niveau c'est que le code recherche n'a pas été trouvé dans la 1ere colonne
            //on cherche dans la seconde colonne dans ce cas pour prendre en compte le cas |A.1.2|A1|XXXXX|
            $rangCode = '2';
            $data = self::searchCsvFile($codeRef, $file, $delimiter, $rangCode);
            if (is_array($data) && $data) {
                //le code recherché n'existe que sur un seule ligne du fichier csv
                $arrayCsvLine = $data[0];
                if (is_array($arrayCsvLine) && $arrayCsvLine) {
                    return Atexo_Util::setInIsoIfEncodingUTF8($arrayCsvLine[$rangLibelle]);
                }
            }
        }
    }

    /**
     * reads a csv file and returns an two-dimensional array of lines/fields.
     *
     * @param unknown_type $file
     * @param string       $delimiter
     * @param string       $field
     * @param string       $codeRef
     *
     * @return array
     */
    public function searchCsvFile($codeRef, $file, $delimiter, $field)
    {
        $partsArray = [];
        $data = file($file);
        $results = [];
        if (is_array($data)) {
            for ($i = 0; $i < count($data); ++$i) {
                $partsArray[$i] = explode($delimiter, $data[$i]);
                if (strstr($partsArray[$i][$field], '-')) {
                    $arrayCode = explode('-', $partsArray[$i][$field]);
                    $code = $arrayCode[0];
                } else {
                    $code = $partsArray[$i][$field];
                }
                if (trim(strtolower($code)) == trim(strtolower($codeRef))) {
                    $results[] = $partsArray[$i];
                }
            }
        }

        return $results;
    }

    /**
     * @param $clef
     * @param $principal
     * @param $secondaire
     * @param $cas
     * @param $display
     *
     * @return unknown_type
     */
    public function retournerReferentiels($clef, $principal, $secondaire, $cas, $display)
    {
        $script_Ref = '';
        $script_Ref .= "<div id='bloc$clef' name='bloc$cas' style=\"display:$display\" >";
        $script_Ref .= "   <div class='champ-cpv' id='$cas".$clef."'></div>";
        $script_Ref .= '</div>';
        $script_Ref .= "<script type='text/javascript'> var principal = new Array(); var secondaires = new Array();";
        if (is_countable($principal) ? count($principal) : 0) {
            $script_Ref .= 'principal[0] = "'.$principal[0].'";';
            $script_Ref .= 'principal[1] = "'.$principal[1].'";';
        }
        if (is_countable($secondaire) ? count($secondaire) : 0) {
            for ($indexsecondaire = 0; $indexsecondaire < (is_countable($secondaire) ? count($secondaire) : 0); ++$indexsecondaire) {
                $script_Ref .= "secondaires[ $indexsecondaire] = new Array();";
                $script_Ref .= "secondaires[ $indexsecondaire][0] = '".$secondaire[$indexsecondaire][0]."';";
                $script_Ref .= 'secondaires[ '.$indexsecondaire.'][1] = "'.$secondaire[$indexsecondaire][1].'";';
            }
        }
        $script_Ref .= " retournerREFERENTIELs('".$clef."',principal,secondaires);";
        $script_Ref = str_replace(["\r", "\n"], '', $script_Ref);
        $script_Ref .= ' </script>';

        return $script_Ref;
    }

    /**
     * permet de parser les info xml dans un tableau.
     *
     * @return array info xml
     */
    public function getArrayConfigXml()
    {
        $array = [];
        $cheminFichierConfigXML = self::getCheminFichierConfigXML();
        $domDocument = new DOMDocument();
        $domDocument->load($cheminFichierConfigXML);
        /*if (!$domDocument->validate()) {
            return array();
            }*/
        $config = $domDocument->getElementsByTagName('configuration')->item(0);
        if (is_object($config)) {
            $array['typeReferentiel'] = $config->getElementsByTagName('typeReferentiel')->item(0)->getAttribute('valeur');
            $array['fichierDonnee'] = $config->getElementsByTagName('fichierDonnee')->item(0)->getAttribute('prefix');
            $array['cheminData'] = $config->getElementsByTagName('cheminData')->item(0)->getAttribute('valeur');
            $array['rechercheMotCle'] = $config->getElementsByTagName('rechercheMotCle')->item(0)->getAttribute('valeur');
            $array['navigationParCode'] = $config->getElementsByTagName('navigationParCode')->item(0)->getAttribute('valeur');
            $array['localeParDefaut'] = $config->getElementsByTagName('localeParDefaut')->item(0)->getAttribute('valeur');
            $array['favorisActiver'] = $config->getElementsByTagName('favorisActiver')->item(0)->getAttribute('valeur');
            $array['codeClassement'] = $config->getElementsByTagName('codeClassement')->item(0)->getAttribute('valeur');
            $array['minimumCodes'] = $config->getElementsByTagName('minimumCodes')->item(0)->getAttribute('valeur');
            $array['maximumCodes'] = $config->getElementsByTagName('maximumCodes')->item(0)->getAttribute('valeur');
            $array['demandePrincipal'] = $config->getElementsByTagName('demandePrincipal')->item(0)->getAttribute('valeur');
            $array['doublon'] = $config->getElementsByTagName('doublon')->item(0)->getAttribute('valeur');
            $array['niveauArbre'] = $config->getElementsByTagName('niveauArbre')->item(0)->getAttribute('valeur');

            return $array;
        }
    }

    /*
     * Permet d'avoir le data file
     */
    public function getDataFile($chemin, $lang)
    {
        $dataRefFile = null;
        $atexo_ref = new Atexo_Ref();
        $atexo_ref->setCheminFichierConfigXML($chemin);
        $atexo_ref->setLocale($lang);
        //fichier de données
        $arrayConfigXml = $atexo_ref->getArrayConfigXml();
        if ($arrayConfigXml) {
            $dataRefFile = './'.$arrayConfigXml['cheminData'].$arrayConfigXml['fichierDonnee'].$lang.'.csv';
        }

        return $dataRefFile;
    }
}
