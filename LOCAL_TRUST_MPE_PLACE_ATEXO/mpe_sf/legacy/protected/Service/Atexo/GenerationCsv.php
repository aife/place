<?php

namespace Application\Service\Atexo;

use Application\Propel\Mpe\CommonAgent;
use Application\Propel\Mpe\CommonDecisionLot;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Category;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Decision;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureType;
use Application\Service\Atexo\SuiviPassation\Atexo_SuiviPassation_Avenant;
use Application\Service\Atexo\SuiviPassation\Atexo_SuiviPassation_Passation;
use AtexoCrypto\Dto\Signature;
use PDO;
use Prado\Prado;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_GenerationCsv
{
    public function genererCvsSuiviPassation($organisme, $separateur = '|', $reference = null, $lot = null, $withEntete = true, $calledFromMarchesConclus = false, $idDecision = null)
    {
        $csvContent = '';
        if ($withEntete) {
            $csvContent = 'date_mise_en_ligne_calcule|reference_utilisateur|libelle_type_procedure (TypeProcedure)|libelle (CategorieConsultation)|resume|lieu_execution|code_cpv_1|alloti|donnée calculée|lot|description|cpvLot|libelle (unite)|libelle (Mandataire)|calculé à partir de service_id|calculé à partir de service_associe_id|libelle (nature acte juridique)|forme_groupement|libelle (mode execution contrat)|montant_estime|min_bon_commande|max_bon_commande|duree_execution_marche_hors_reconduction|nombre_reconduction|duree_total_marche|ccag_applicable|numero_deliberation_financiere|date_deliberation_financiere|numero_deliberation_autorisant_signature_marche|date_deliberation_autorisant_signature_marche|date_notification_previsionnelle|date_reception_projet_DCE_Service_Validateur|date_formulations_premieres_observations|date_retour_projet_DCE_finalise|date_validation_projet_DCE_par_service_validateur|date_validation_projet_DCE_vue_par|date_reception_projet_AAPC_par_Service_Validateur|date_formulations_premieres_observations_AAPC|date_retour_projet_AAPC_finalise|date_validation_projet_AAPC_par_Service_Validateur|date_validation_projet_AAPC_par_Service_Validateur_vu_par|date_envoi_publicite|date_envoi_invitations_remettre_offre|date_limite_remise_offres|datefin|Délai de remise des offres (donnée calculée)|delai_validite_offres|Date de péremption des offres (donnée calculée)|date_reunion_ouverture_candidatures|date_reunion_ouverture_offres|date_reception_analyse_offre|date_formulation_observation_projet_rapport|date_retour_projet_rapport_finalise|date_validation_projet_rapport|projet_rapport_vu_par|date_reunion_attribution|date_decision|decision|date_envoi_courrier_condidat_non_retenu|date_signature_marche_pa|date_reception_controle_legalite|date_formulation_observation_dossier|date_retour_dossier_finalise|date_transmission_prefecture|dossier_vu_par|date_validation_rapport_information|id_consultation|id_passation_consultation|id_passation_marche_a_venir';
            if (Atexo_Module::isEnabled('ConsultationClause')) {
                $csvContent .= '|clause_sociale|clause_environnementale';
            }
            $csvContent .= "\r\n";
            $csvContent .= "Date de mise en ligne|Référence|Type de procédure|Catégorie principale|Objet de la consultation|Lieu d'exécution |Code principal du CPV de la consultation|Allotissement|Nombre de lots|N° du lot|Intitulé du lot|Code principal du CPV du lot|Unité|Mandataire|Service de rattachement|Service associé|Nature de l'acte juridique|Forme du groupement|Mode d'exécution du contrat|montant estimé du marché|Montant Mini HT (MBC)|Montant Maxi HT (MBC)|Durée d'exécution prévisionnelle du marché hors reconduction|Nombre de reconduction|Durée totale du marché|CCAG applicable|N° de délibération financière|Date de la délibération financière|N° de délibération autorisant la signature du marché|Date de la délibération autorisant la signature du marché|Date de notification prévisionnelle|Date de réception du projet de DCE par le Service Valideur|Date des formulations des premières observations sur le projet de DCE par le Service Validateur |Date de retour du projet de DCE finalisé |Date de validation du projet de DCE par le Service Validateur |Vu par :|Date de réception du projet d'AAPC par le Service Validateur |Date des formulations des premières observations sur le projet d'AAPC par le Service Validateur |Date de retour du projet d'AAPC finalisé |Date de validation du projet d'AAPC par le Service Validateur |Vu par :|Date d'envoi de la publicité|Date d'envoi des invitations à remettre une offre|Date limite de remise des offres|Date limite de remise des plis|Délai de remise des offres (en jours francs)|Délai de validité des offres|Date de péremption des offres|Réunion d'ouverture des candidatures |Réunion d'ouverture des offres|Réception du projet de rapport d'analyse des offres par le Service Validateur |Formulations des premières observations sur le projet de rapport par le Service Validateur |Retour du projet de rapport finalisé |Validation du projet de rapport finalisé |Vu par :|Date de la réunion d'attribution |Date de la décision|Nature de la décision|Envoi des courriers aux candidats non retenus |Signature du marché par le Pouvoir Adjudicateur |Réception du dossier 'contrôle de légalité' parle Service Validateur :|Formulations des premières observations sur le dossier par le Service Validateur|Retour du dossier finalisé|Transmission en Préfecture|Vu par :|Si MAPA, Validation du rapport d'information destiné à la CP|identifiant de la consultation|identifiant de la passation|ide du marche à venir";
            if (Atexo_Module::isEnabled('ConsultationClause')) {
                $csvContent .= '|clause sociale|clause environnementale';
            }
            $csvContent .= "\r\n";
        }

        $statement = (new Atexo_SuiviPassation_Passation())->retrieveSuiviPassationInformation($organisme, $reference, $lot);
        $arrayVerif = [];
        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            $key = $row['id_passation_consultation'].'#'.$row['numLot'];
            if (!in_array($key, $arrayVerif)) {
                $csvContent .= self::creerLigneCsvSuiviPassation($row, $separateur, $organisme, $calledFromMarchesConclus, $idDecision);
                $csvContent .= "\r\n";
                $arrayVerif[] = $key;
            }
        }

        return $csvContent;
    }

    public function genererCvsMarchesConclus($organisme, $separateur = '|', $withEntete = true)
    {
        $csvContent = '';
        $decisions = (new Atexo_Consultation_Decision())->retrieveAttributaireInformation($organisme);
        if ($withEntete) {
            $csvContent = 'nom_entreprise|numero_marche|date_notification|montant_marche|date_mise_en_ligne_calcule|reference_utilisateur|libelle_type_procedure (TypeProcedure)|libelle (CategorieConsultation)|resume|lieu_execution|code_cpv_1|alloti|donnée calculée|lot|description|cpvLot|libelle (unite)|libelle (Mandataire)|calculé à partir de service_id|calculé à partir de service_associe_id|libelle (nature acte juridique)|forme_groupement|libelle (mode execution contrat)|montant_estime|min_bon_commande|max_bon_commande|duree_execution_marche_hors_reconduction|nombre_reconduction|duree_total_marche|ccag_applicable|numero_deliberation_financiere|date_deliberation_financiere|numero_deliberation_autorisant_signature_marche|date_deliberation_autorisant_signature_marche|date_notification_previsionnelle|date_reception_projet_DCE_Service_Validateur|date_formulations_premieres_observations|date_retour_projet_DCE_finalise|date_validation_projet_DCE_par_service_validateur|date_validation_projet_DCE_vue_par|date_reception_projet_AAPC_par_Service_Validateur|date_formulations_premieres_observations_AAPC|date_retour_projet_AAPC_finalise|date_validation_projet_AAPC_par_Service_Validateur|date_validation_projet_AAPC_par_Service_Validateur_vu_par|date_envoi_publicite|date_envoi_invitations_remettre_offre|date_limite_remise_offres|datefin|Délai de remise des offres (donnée calculée)|delai_validite_offres|Date de péremption des offres (donnée calculée)|date_reunion_ouverture_candidatures|date_reunion_ouverture_offres|date_reception_analyse_offre|date_formulation_observation_projet_rapport|date_retour_projet_rapport_finalise|date_validation_projet_rapport|projet_rapport_vu_par|date_reunion_attribution|date_decision|decision|date_envoi_courrier_condidat_non_retenu|date_signature_marche_pa|date_reception_controle_legalite|date_formulation_observation_dossier|date_retour_dossier_finalise|date_transmission_prefecture|dossier_vu_par|date_validation_rapport_information|id_consultation|id_passation_consultation|id_passation_marche_a_venir|id_decision_enveloppe';
            if (Atexo_Module::isEnabled('ConsultationClause')) {
                $csvContent .= '|clause_sociale|clause_environnementale';
            }
            $csvContent .= "\r\n";

            $csvContent .= "Nom du Titulaire du marché|N° de marché|Date de notification|Montant du marché|Date de mise en ligne|Référence|Type de procédure|Catégorie principale|Objet de la consultation|Lieu d'exécution |Code principal du CPV de la consultation|Allotissement|Nombre de lots|N° du lot|Intitulé du lot|Code principal du CPV du lot|Unité|Mandataire|Service de rattachement|Service associé|Nature de l'acte juridique|Forme du groupement|Mode d'exécution du contrat|montant estimé du marché|Montant Mini HT (MBC)|Montant Maxi HT (MBC)|Durée d'exécution prévisionnelle du marché hors reconduction|Nombre de reconduction|Durée totale du marché|CCAG applicable|N° de délibération financière|Date de la délibération financière|N° de délibération autorisant la signature du marché|Date de la délibération autorisant la signature du marché|Date de notification prévisionnelle|Date de réception du projet de DCE par le Service Valideur|Date des formulations des premières observations sur le projet de DCE par le Service Validateur |Date de retour du projet de DCE finalisé |Date de validation du projet de DCE par le Service Validateur |Vu par :|Date de réception du projet d'AAPC par le Service Validateur |Date des formulations des premières observations sur le projet d'AAPC par le Service Validateur |Date de retour du projet d'AAPC finalisé |Date de validation du projet d'AAPC par le Service Validateur |Vu par :|Date d'envoi de la publicité|Date d'envoi des invitations à remettre une offre|Date limite de remise des offres|Date limite de remise des plis|Délai de remise des offres (en jours francs)|Délai de validité des offres|Date de péremption des offres|Réunion d'ouverture des candidatures |Réunion d'ouverture des offres|Réception du projet de rapport d'analyse des offres par le Service Validateur |Formulations des premières observations sur le projet de rapport par le Service Validateur |Retour du projet de rapport finalisé |Validation du projet de rapport finalisé |Vu par :|Date de la réunion d'attribution|Date de la décision|Nature de la décision|Envoi des courriers aux candidats non retenus |Signature du marché par le Pouvoir Adjudicateur |Réception du dossier 'contrôle de légalité' parle Service Validateur :|Formulations des premières observations sur le dossier par le Service Validateur|Retour du dossier finalisé|Transmission en Préfecture|Vu par :|Si MAPA, Validation du rapport d'information destiné à la CP|identifiant de la consultation|identifiant de la passation|identifiant marche|identifiant de la décision";
            if (Atexo_Module::isEnabled('ConsultationClause')) {
                $csvContent .= '|clause sociale|clause environnementale';
            }
            $csvContent .= "\r\n";
        }
        foreach ($decisions as $decision) {
            $csvDecision = self::creerLigneMarchesConclusInfosDecision($decision, $separateur);
            $csvPassation = self::genererCvsSuiviPassation($organisme, $separateur, $decision['consultation_id'], $decision['lot'], false, true, $decision['id_decision']);
            if ('' != $csvPassation) {
                $csvContent = $csvContent.$csvDecision.$csvPassation;
            }
        }

        return $csvContent;
    }

    public function creerLigneMarchesConclusInfosDecision(array $arrayData, $separateur)
    {
        $csvContent = '';
        //Nom du Titulaire du marché
        $csvContent .= $arrayData['nom_entreprise'];
        $csvContent .= $separateur;
        //N° de marché
        $csvContent .= $arrayData['numero_marche'];
        $csvContent .= $separateur;
        //Date de notification
        $csvContent .= ($arrayData['date_notification'] ? Atexo_Util::iso2frnDateTime($arrayData['date_notification']) : '');
        $csvContent .= $separateur;
        //Montant du marché
        $csvContent .= $arrayData['montant_marche'];
        $csvContent .= $separateur;

        return $csvContent;
    }

    public function creerLigneCsvSuiviPassation(array $arrayData, $separateur, $organisme, $calledFromMarchesConclus, $idDecision)
    {
        $csvContent = '';
        //Date de mise en ligne *
        $csvContent .= ($arrayData['date_mise_en_ligne_calcule'] ? Atexo_Util::iso2frnDateTime($arrayData['date_mise_en_ligne_calcule']) : '');
        $csvContent .= $separateur;
        //Référence *
        $csvContent .= $arrayData['reference_utilisateur'];
        $csvContent .= $separateur;
        //Type de procédure *
        $libelleTypeProcedure = (new Atexo_Consultation_ProcedureType())->retrieveTypeProcedure($arrayData['id_type_procedure_org'], false, $organisme);
        $libelleTypeProcedure = str_replace('&#8804;', Prado::localize('MAPA_INF_OU_EGALE'), $libelleTypeProcedure);
        $libelleTypeProcedure = str_replace('&#8805;', Prado::localize('MAPA_SUPP_OU_EGALE'), $libelleTypeProcedure);
        $csvContent .= $libelleTypeProcedure;
        $csvContent .= $separateur;
        //Catégorie principale *
        $libelleCategorie = Atexo_Consultation_Category::retrieveLibelleCategorie($arrayData['categorie'], false, $organisme);
        $csvContent .= $libelleCategorie;
        $csvContent .= $separateur;
        //Objet de la consultation
        $csvContent .= str_replace("\r", '', str_replace("\n", '', ($arrayData['objet'] ?: ' ')));
        $csvContent .= $separateur;
        //Lieu d'exécution
        $lieuxExecutions = ' ';
        $listeLieuxExecution = explode(',', $arrayData['lieu_execution']);
        if (is_array($listeLieuxExecution)) {
            foreach ($listeLieuxExecution as $unLieu) {
                $libelleLieu = (new Atexo_Consultation())->getLibelleLieuExecution($unLieu);
                $lieuxExecutions .= $libelleLieu.'#';
            }
        }
        $lieuxExecutions = trim($lieuxExecutions, '#');
        $csvContent .= $lieuxExecutions;
        $csvContent .= $separateur;
        //Code principal du CPV de la consultation
        $csvContent .= ($arrayData['code_cpv_1'] ?: ' ');
        $csvContent .= $separateur;
        //Allotissement
        $csvContent .= ('1' == $arrayData['alloti']) ? Prado::localize('DEFINE_OUI') : Prado::localize('TEXT_NON');
        $csvContent .= $separateur;
        //Nombre de lots
        $nombreLot = (new Atexo_Consultation())->getNombreLot($arrayData['reference'], $organisme);
        $csvContent .= $nombreLot;
        $csvContent .= $separateur;
        //N° du lot
        $csvContent .= $arrayData['numLot'];
        $csvContent .= $separateur;
        //Intitulé du lot
        $csvContent .= ($arrayData['description'] ? str_replace("\r", '', str_replace("\n", '', $arrayData['description'])) : ' ');
        $csvContent .= $separateur;
        //Code principal du CPV du lot
        $csvContent .= ($arrayData['cpvLot'] ?: ' ');
        $csvContent .= $separateur;
        //Unité
        $libelleUnite = (new Atexo_SuiviPassation_Passation())->retrieveLibelleFcspUnite($arrayData['unite'], $organisme);
        $libelleUnite = str_replace('Sélectionnez...', '', str_replace("\r", '', str_replace("\n", '', $libelleUnite)));
        $libelleUnite = $libelleUnite ?: ' ';
        $csvContent .= $libelleUnite;
        $csvContent .= $separateur;
        //Mandataire
        $libelleMandataire = (new Atexo_SuiviPassation_Passation())->retrieveLibelleFcspMandataire($arrayData['mandataire'], $organisme);
        $libelleMandataire = str_replace('Sélectionnez...', '', str_replace("\r", '', str_replace("\n", '', $libelleMandataire)));
        $libelleMandataire = $libelleMandataire ?: ' ';
        $csvContent .= $libelleMandataire;
        $csvContent .= $separateur;
        //Service de rattachement
        $libelleService = (new Atexo_EntityPurchase())->getEntityPathById($arrayData['service_id'], $organisme, true, false);
        $csvContent .= $libelleService;
        $libelleService = $libelleService ?: ' ';
        $csvContent .= $separateur;
        //Service associé
        $libelleService = (new Atexo_EntityPurchase())->getEntityPathById($arrayData['service_associe_id'], $organisme, true, false);
        $csvContent .= $libelleService;
        $libelleService = $libelleService ?: ' ';
        $csvContent .= $separateur;
        //Nature de l'acte juridique
        $libelleNatureActeJuridique = (new Atexo_SuiviPassation_Passation())->retrieveLibelleNatureActeJuridique($arrayData['id_nature_acte_juridique'], $organisme);
        $libelleNatureActeJuridique = str_replace('Sélectionnez...', '', str_replace("\r", '', str_replace("\n", '', $libelleNatureActeJuridique)));
        $libelleNatureActeJuridique = $libelleNatureActeJuridique ?: ' ';
        $csvContent .= $libelleNatureActeJuridique;
        $csvContent .= $separateur;
        //Forme du groupement
        if (null !== $arrayData['forme_groupement']) {
            if ('1' == $arrayData['forme_groupement']) {
                $csvContent .= Prado::localize('FCSP_CONJOINT');
            } elseif ('2' == $arrayData['forme_groupement']) {
                $csvContent .= Prado::localize('FCSP_SOLIDAIRE');
            } else {
                $csvContent .= Prado::localize('FCSP_AU_CHOIX');
            }
        }
        $csvContent .= $separateur;
        //Mode d'exécution du contrat
        $libelleModeExecution = (new Atexo_SuiviPassation_Passation())->retrieveLibelleModeExecution($arrayData['mode_execution_contrat'], $organisme);
        $libelleModeExecution = str_replace('Sélectionnez...', '', str_replace("\r", '', str_replace("\n", '', $libelleModeExecution)));
        $libelleModeExecution = $libelleModeExecution ?: ' ';
        $csvContent .= $libelleModeExecution;
        $csvContent .= $separateur;
        //Montant estimé
        $csvContent .= ($arrayData['montant_estime'] ?: ' ');
        $csvContent .= $separateur;
        //Montant Mini HT (MBC)|Montant Maxi HT (MBC)
        if ('2' == $arrayData['mode_execution_contrat']) {
            $csvContent .= $arrayData['min_bon_commande'];
            $csvContent .= $separateur;
            $csvContent .= $arrayData['max_bon_commande'];
            $csvContent .= $separateur;
        } else {
            $csvContent .= ' ';
            $csvContent .= $separateur;
            $csvContent .= ' ';
            $csvContent .= $separateur;
        }
        //Durée d'exécution prévisionnelle du marché hors reconduction
        $csvContent .= ($arrayData['duree_execution_marche_hors_reconduction'] ?: '0');
        $csvContent .= $separateur;
        //Nombre de reconduction
        $csvContent .= ($arrayData['nombre_reconduction'] ?: '0');
        $csvContent .= $separateur;
        //Durée totale du marché
        $csvContent .= ($arrayData['duree_total_marche'] ?: '0');
        $csvContent .= $separateur;
        //CCAG applicable
        $libelleCcagApplicable = (new Atexo_SuiviPassation_Passation())->retrieveLibelleCcag($arrayData['ccag_applicable'], $organisme);
        $libelleCcagApplicable = strstr($libelleCcagApplicable, '0-') ? '' : $libelleCcagApplicable;
        $csvContent .= ($libelleCcagApplicable ?: ' ');
        $csvContent .= $separateur;
        //N° de délibération financière
        $csvContent .= ($arrayData['numero_deliberation_financiere'] ?: ' ');
        $csvContent .= $separateur;
        //Date de la délibération financière
        $csvContent .= ($arrayData['date_deliberation_financiere'] ? Atexo_Util::iso2frnDateTime($arrayData['date_deliberation_financiere']) : '');
        $csvContent .= $separateur;
        //N° de délibération autorisant la signature du marché
        $csvContent .= ($arrayData['numero_deliberation_autorisant_signature_marche'] ?: '0');
        $csvContent .= $separateur;
        //Date de la délibération autorisant la signature du marché
        $csvContent .= ($arrayData['date_deliberation_autorisant_signature_marche'] ? Atexo_Util::iso2frnDate($arrayData['date_deliberation_autorisant_signature_marche']) : '');
        $csvContent .= $separateur;
        //Date de notification prévisionnelle
        $csvContent .= ($arrayData['date_notification_previsionnelle'] ? Atexo_Util::iso2frnDate($arrayData['date_notification_previsionnelle']) : '');
        $csvContent .= $separateur;
        //Date de réception du projet de DCE par le Service Valideur
        $csvContent .= ($arrayData['date_reception_projet_DCE_Service_Validateur'] ? Atexo_Util::iso2frnDate($arrayData['date_reception_projet_DCE_Service_Validateur']) : '');
        $csvContent .= $separateur;
        //Date des formulations des premières observations sur le projet de DCE par le Service Validateur
        $csvContent .= ($arrayData['date_formulations_premieres_observations'] ? Atexo_Util::iso2frnDate($arrayData['date_formulations_premieres_observations']) : '');
        $csvContent .= $separateur;
        //Date de retour du projet de DCE finalisé
        $csvContent .= ($arrayData['date_retour_projet_DCE_finalise'] ? Atexo_Util::iso2frnDate($arrayData['date_retour_projet_DCE_finalise']) : '');
        $csvContent .= $separateur;
        //Date de validation du projet de DCE par le Service Validateur
        $csvContent .= ($arrayData['date_validation_projet_DCE_par_service_validateur'] ? Atexo_Util::iso2frnDate($arrayData['date_validation_projet_DCE_par_service_validateur']) : '');
        $csvContent .= $separateur;
        //Vu par :
        $nomAgent = '';
        $agent = (new Atexo_Agent())->retrieveAgentById($arrayData['date_validation_projet_DCE_vue_par']);
        if ($agent instanceof CommonAgent) {
            $nomAgent = $agent->getPrenom().' '.$agent->getNom();
        }
        $nomAgent = $nomAgent ?: ' ';
        $csvContent .= $nomAgent;
        $csvContent .= $separateur;
        //Date de réception du projet d'AAPC par le Service Validateur
        $csvContent .= ($arrayData['date_reception_projet_AAPC_par_Service_Validateur'] ? Atexo_Util::iso2frnDate($arrayData['date_reception_projet_AAPC_par_Service_Validateur']) : '');
        $csvContent .= $separateur;
        //Date des formulations des premières observations sur le projet d'AAPC par le Service Validateur
        $csvContent .= ($arrayData['date_formulations_premieres_observations_AAPC'] ? Atexo_Util::iso2frnDate($arrayData['date_formulations_premieres_observations_AAPC']) : '');
        $csvContent .= $separateur;
        //Date de retour du projet d'AAPC finalisé
        $csvContent .= ($arrayData['date_retour_projet_AAPC_finalise'] ? Atexo_Util::iso2frnDate($arrayData['date_retour_projet_AAPC_finalise']) : '');
        $csvContent .= $separateur;
        //Date de validation du projet d'AAPC par le Service Validateur
        $csvContent .= ($arrayData['date_validation_projet_AAPC_par_Service_Validateur'] ? Atexo_Util::iso2frnDate($arrayData['date_validation_projet_AAPC_par_Service_Validateur']) : '');
        $csvContent .= $separateur;
        //Vu par :
        $nomAgent = '';
        $agent = (new Atexo_Agent())->retrieveAgentById($arrayData['date_validation_projet_AAPC_par_Service_Validateur_vu_par']);
        if ($agent instanceof CommonAgent) {
            $nomAgent = $agent->getPrenom().' '.$agent->getNom();
        }
        $nomAgent = $nomAgent ?: ' ';
        $csvContent .= $nomAgent;
        $csvContent .= $separateur;
        //Date d'envoi de la publicité
        $csvContent .= ($arrayData['date_envoi_publicite'] ? Atexo_Util::iso2frnDate($arrayData['date_envoi_publicite']) : '');
        $csvContent .= $separateur;
        //Date d'envoi des invitations à remettre une offre
        $csvContent .= ($arrayData['date_envoi_invitations_remettre_offre'] ? Atexo_Util::iso2frnDate($arrayData['date_envoi_invitations_remettre_offre']) : '');
        $csvContent .= $separateur;
        //Date limite de remise des offres
        $csvContent .= ($arrayData['date_limite_remise_offres'] ? Atexo_Util::iso2frnDate($arrayData['date_limite_remise_offres']) : '');
        $csvContent .= $separateur;
        //Date limite de remise des plis
        $csvContent .= ($arrayData['datefin'] ? Atexo_Util::iso2frnDateTime($arrayData['datefin']) : '');
        $csvContent .= $separateur;
        //Délai de remise des offres (en jours francs)
        if ($arrayData['date_limite_remise_offres'] && $arrayData['date_envoi_invitations_remettre_offre']) {
            $delaiRemiseOffre = ceil(Atexo_Util::diffJours(Atexo_Util::iso2frnDate($arrayData['date_limite_remise_offres']), Atexo_Util::iso2frnDate($arrayData['date_envoi_invitations_remettre_offre']))) - 1;
        } elseif ($arrayData['date_limite_remise_offres'] && $arrayData['date_envoi_publicite']) {
            $delaiRemiseOffre = ceil(Atexo_Util::diffJours(Atexo_Util::iso2frnDate($arrayData['date_limite_remise_offres']), Atexo_Util::iso2frnDate($arrayData['date_envoi_publicite']))) - 1;
        } else {
            $delaiRemiseOffre = '';
        }
        $csvContent .= $delaiRemiseOffre;
        $csvContent .= $separateur;
        //Délai de validité des offres
        $csvContent .= ($arrayData['delai_validite_offres'] ?: '0');
        $csvContent .= $separateur;
        //Date de péremption des offres
        $date = $arrayData['date_limite_remise_offres'];
        $nombreJours = $arrayData['delai_validite_offres'] ?: '0';
        if ($date) {
            [$j, $m, $y] = explode('/', $date);
            $timeStamp = mktime(0, 0, 0, $m, $j, $y) + ($nombreJours * 24 * 60 * 60);
            $resultat = date('d/m/Y', $timeStamp);
        } else {
            $resultat = ' ';
        }
        $csvContent .= $resultat;
        $csvContent .= $separateur;
        //Réunion d'ouverture des candidatures
        $csvContent .= ($arrayData['date_reunion_ouverture_candidatures'] ? Atexo_Util::iso2frnDate($arrayData['date_reunion_ouverture_candidatures']) : '');
        $csvContent .= $separateur;
        //Réunion d'ouverture des offres
        $csvContent .= ($arrayData['date_reunion_ouverture_offres'] ? Atexo_Util::iso2frnDate($arrayData['date_reunion_ouverture_offres']) : '');
        $csvContent .= $separateur;
        //Réception du projet de rapport d'analyse des offres par le Service Validateur
        $csvContent .= ($arrayData['date_reception_analyse_offre'] ? Atexo_Util::iso2frnDate($arrayData['date_reception_analyse_offre']) : '');
        $csvContent .= $separateur;
        //Formulations des premières observations sur le projet de rapport par le Service Validateur
        $csvContent .= ($arrayData['date_formulation_observation_projet_rapport'] ? Atexo_Util::iso2frnDate($arrayData['date_formulation_observation_projet_rapport']) : '');
        $csvContent .= $separateur;
        //Retour du projet de rapport finalisé
        $csvContent .= ($arrayData['date_retour_projet_rapport_finalise'] ? Atexo_Util::iso2frnDate($arrayData['date_retour_projet_rapport_finalise']) : '');
        $csvContent .= $separateur;
        //Validation du projet de rapport finalisé
        $csvContent .= ($arrayData['date_validation_projet_rapport'] ? Atexo_Util::iso2frnDate($arrayData['date_validation_projet_rapport']) : '');
        $csvContent .= $separateur;
        //Vu par :
        $nomAgent = '';
        $agent = ((new Atexo_Agent())->retrieveAgentById($arrayData['projet_rapport_vu_par']));
        if ($agent instanceof CommonAgent) {
            $nomAgent = $agent->getPrenom().' '.$agent->getNom();
        }
        $nomAgent = $nomAgent ?: ' ';
        $csvContent .= $nomAgent;
        $csvContent .= $separateur;
        //Date de la réunion d'attribution
        $csvContent .= ($arrayData['date_reunion_attribution'] ? Atexo_Util::iso2frnDate($arrayData['date_reunion_attribution']) : '');
        $csvContent .= $separateur;
        //Date de la décision
        $lot = $arrayData['numLot'] ?: '0';
        $decisionLot = (new Atexo_Consultation_Decision())->retrieveDecisionLotByReferenceAndLot($arrayData['reference'], $lot, $organisme);
        if ($decisionLot instanceof CommonDecisionLot) {
            $csvContent .= Atexo_Util::iso2frnDate($decisionLot->getDateDecision());
        } else {
            $csvContent .= ' ';
        }
        $csvContent .= $separateur;
        //----
        //Nature de la décision
        $decision = (new Atexo_SuiviPassation_Passation())->retrieveLibelleDecision($arrayData['decision'], $organisme);
        $decision = $decision ?: ' ';
        $csvContent .= $decision;
        $csvContent .= $separateur;
        //Envoi des courriers aux candidats non retenus
        $csvContent .= ($arrayData['date_envoi_courrier_condidat_non_retenu'] ? Atexo_Util::iso2frnDate($arrayData['date_envoi_courrier_condidat_non_retenu']) : '');
        $csvContent .= $separateur;
        //Signature du marché par le Pouvoir Adjudicateur
        $csvContent .= ($arrayData['date_signature_marche_pa'] ? Atexo_Util::iso2frnDate($arrayData['date_signature_marche_pa']) : '');
        $csvContent .= $separateur;
        //Réception du dossier 'contrôle de légalité' parle Service Validateur :
        $csvContent .= ($arrayData['date_reception_controle_legalite'] ? Atexo_Util::iso2frnDate($arrayData['date_reception_controle_legalite']) : '');
        $csvContent .= $separateur;
        //"Formulations des premières observations sur le dossier par le Service Validateur "
        $csvContent .= ($arrayData['date_formulation_observation_dossier'] ? Atexo_Util::iso2frnDate($arrayData['date_formulation_observation_dossier']) : '');
        $csvContent .= $separateur;
        //Retour du dossier finalisé
        $csvContent .= ($arrayData['date_retour_dossier_finalise'] ? Atexo_Util::iso2frnDate($arrayData['date_retour_dossier_finalise']) : '');
        $csvContent .= $separateur;
        //Transmission en Préfecture
        $csvContent .= ($arrayData['date_transmission_prefecture'] ? Atexo_Util::iso2frnDate($arrayData['date_transmission_prefecture']) : '');
        $csvContent .= $separateur;
        //Vu par :
        $nomAgent = '';
        $agent = (new Atexo_Agent())->retrieveAgentById($arrayData['dossier_vu_par']);
        if ($agent instanceof CommonAgent) {
            $nomAgent = $agent->getPrenom().' '.$agent->getNom();
        }
        $nomAgent = $nomAgent ?: ' ';
        $csvContent .= $nomAgent;
        $csvContent .= $separateur;
        //"Si MAPA, Validation du rapport d'information destiné à la CP"
        if ((new Atexo_Consultation_ProcedureType())->isMapa($arrayData['id_type_procedure'], false, $organisme)) {
            $csvContent .= ($arrayData['date_validation_rapport_information'] ? Atexo_Util::iso2frnDate($arrayData['date_validation_rapport_information']) : '');
        } else {
            $csvContent .= '';
        }
        $csvContent .= $separateur;

        $csvContent .= $arrayData['reference'];
        $csvContent .= $separateur;

        $csvContent .= $arrayData['idPassation'];

        //if($calledFromMarchesConclus) {
        $csvContent .= $separateur;
        $csvContent .= $arrayData['idMarche'];

        //}

        if ($idDecision) {
            $csvContent .= $separateur;
            $csvContent .= $idDecision;
        }

        if (Atexo_Module::isEnabled('ConsultationClause')) {
            if ('0' == $arrayData['alloti']) {
                $csvContent .= $separateur;
                if (2 == $arrayData['clause_sociale']) {
                    $csvContent .= Prado::localize('TEXT_NON');
                } elseif (1 == $arrayData['clause_sociale']) {
                    $csvContent .= Prado::localize('TEXT_OUI');
                } else {
                    $csvContent .= Prado::localize('NON_RENSEIGNE');
                }

                $csvContent .= $separateur;
                if (2 == $arrayData['clause_environnementale']) {
                    $csvContent .= Prado::localize('TEXT_NON');
                } elseif (1 == $arrayData['clause_environnementale']) {
                    $csvContent .= Prado::localize('TEXT_OUI');
                } else {
                    $csvContent .= Prado::localize('NON_RENSEIGNE');
                }
            } else {
                $csvContent .= $separateur;
                if (2 == $arrayData['clause_sociale_lot']) {
                    $csvContent .= Prado::localize('TEXT_NON');
                } elseif (1 == $arrayData['clause_sociale_lot']) {
                    $csvContent .= Prado::localize('TEXT_OUI');
                } else {
                    $csvContent .= Prado::localize('NON_RENSEIGNE');
                }

                $csvContent .= $separateur;
                if (1 == $arrayData['clause_environnementale_lot']) {
                    $csvContent .= Prado::localize('TEXT_OUI');
                } elseif (2 == $arrayData['clause_environnementale_lot']) {
                    $csvContent .= Prado::localize('TEXT_NON');
                } else {
                    $csvContent .= Prado::localize('NON_RENSEIGNE');
                }
            }
        }

        return $csvContent;
    }

    public function creerLigneAvenant(array $arrayData, $separateur, $organisme)
    {
        $csvContent = '';
        //Référence de la consultation
        $csvContent .= $arrayData['reference_utilisateur'];
        $csvContent .= $separateur;
        //Type de procédure
        $typeProcedure = Atexo_Consultation_ProcedureType::retrieveProcedureById($arrayData['type_procedure'], $organisme);
        $libelleTypeProcedure = $typeProcedure->getLibelleTypeProcedure();
        $libelleTypeProcedure = str_replace('&#8804;', Prado::localize('MAPA_INF_OU_EGALE'), $libelleTypeProcedure);
        $libelleTypeProcedure = str_replace('&#8805;', Prado::localize('MAPA_SUPP_OU_EGALE'), $libelleTypeProcedure);
        $csvContent .= $libelleTypeProcedure;
        $csvContent .= $separateur;
        //Catégorie principale de la consultation
        $csvContent .= Atexo_Consultation_Category::retrieveLibelleCategorie($arrayData['categorie']);
        $csvContent .= $separateur;
        //N° de lot (si pas de lot alors on met 0)
        $csvContent .= $arrayData['lot'];

        $csvContent .= $separateur;
        //Objet du marché

        $csvContent .= ($arrayData['objet_marche'] ? str_replace("\r", '', str_replace("\n", '', $arrayData['objet_marche'])) : ' ');
        //$csvContent.=$arrayData['objet_marche'];
        $csvContent .= $separateur;
        //Montant du marché EUR HT
        $csvContent .= $arrayData['montant_marche'];
        $csvContent .= $separateur;
        //Nom de l'attributaire
        $csvContent .= $arrayData['nom_entreprise'];
        $csvContent .= $separateur;
        //Date de notification de l'atributaire
        $csvContent .= Atexo_Util::iso2frnDate($arrayData['date_notification']);
        $csvContent .= $separateur;
        //N° d'avenant
        $csvContent .= $arrayData['numero_avenant'];
        $csvContent .= $separateur;
        //Type d'avenant
        $avenant = new Atexo_SuiviPassation_Avenant();
        $csvContent .= $avenant->retriveTypesAvenantById($arrayData['type_avenant'], $organisme);
        //$csvContent.=$arrayData['type_avenant'];
        $csvContent .= $separateur;
        //Objet de l'avenant
        $csvContent .= $arrayData['objet_avenant'];
        $csvContent .= $separateur;
        //Montant de l'avenant HT
        $csvContent .= $arrayData['montant_avenant_ht'];
        $csvContent .= $separateur;
        //Montant de l'avenant TTC
        $csvContent .= $arrayData['montant_avenant_ttc'];
        $csvContent .= $separateur;
        //Pourcentage d'augmentation / marché initial
        $csvContent .= $arrayData['pourcentage_augmentation_marche_initial'];
        $csvContent .= $separateur;
        //Pourcentage d'augmentation cumulé (tous avenants)
        $csvContent .= $arrayData['pourcentage_augmentation_cumule'];
        $csvContent .= $separateur;
        //Montant total du marché tous avenants cumulés en HT
        $csvContent .= $arrayData['montant_total_marche_tout_avenant_cumule'];
        $csvContent .= $separateur;
        //Date de réception du projet de présentation de l'avenant par le secrétariat de la CAO
        $csvContent .= Atexo_Util::iso2frnDate($arrayData['date_reception_projet_par_secretaire_cao']);
        $csvContent .= $separateur;
        //Date de réception du projet de présentation de l'avenant par le chargé d'études du Service Validateur
        $csvContent .= Atexo_Util::iso2frnDate($arrayData['date_reception_projet_par_charge_etude']);
        $csvContent .= $separateur;
        //Date des formulations des premières observations par le Service Validateur sur le projet de rapport
        $csvContent .= Atexo_Util::iso2frnDate($arrayData['date_observation_par_sv']);
        $csvContent .= $separateur;
        //Date de retour du projet de rapport finalisé
        $csvContent .= Atexo_Util::iso2frnDate($arrayData['date_retour_projet']);
        $csvContent .= $separateur;
        //Date de validation du projet de rapport par le Service Validateur
        $csvContent .= Atexo_Util::iso2frnDate($arrayData['date_validation_projet']);
        $csvContent .= $separateur;
        //Vu par
        $nomAgent1 = '';
        $agent = (new Atexo_Agent())->retrieveAgentById($arrayData['date_validation_projet_vu_par']);
        if ($agent instanceof CommonAgent) {
            $nomAgent1 = $agent->getPrenom().' '.$agent->getNom();
        }
        $csvContent .= $nomAgent1;
        $csvContent .= $separateur;
        //Date de la CAO
        $csvContent .= Atexo_Util::iso2frnDate($arrayData['date_cao']);
        $csvContent .= $separateur;
        //Avis de la CAO
        $passation = new Atexo_SuiviPassation_Passation();
        $csvContent .= $passation->retrieveAvisCaoById($arrayData['avis_cao'], $organisme);
        $csvContent .= $separateur;
        //Date de la CP
        $csvContent .= Atexo_Util::iso2frnDate($arrayData['date_cp']);
        $csvContent .= $separateur;
        //Date de signature de l'avenant par le Pouvoir Adjudicateur
        $csvContent .= Atexo_Util::iso2frnDate($arrayData['date_signature_avenant']);
        $csvContent .= $separateur;
        //Date de réception du dossier 'contrôle de légalité' par le Service Validateur
        $csvContent .= Atexo_Util::iso2frnDate($arrayData['date_reception_dossier']);
        $csvContent .= $separateur;
        //Date des formulations des premières observations par le Service Validateur sur le dosier
        $csvContent .= Atexo_Util::iso2frnDate($arrayData['date_formulation_observation_par_sv_sur_sdossier']);
        $csvContent .= $separateur;
        //Date de retour du dossier finalisé
        $csvContent .= Atexo_Util::iso2frnDate($arrayData['date_retour_dossier_finalise']);
        $csvContent .= $separateur;
        // Date de validation du dossier finalisé par le Service Validateur
        $csvContent .= Atexo_Util::iso2frnDate($arrayData['date_validation_dossier_finalise']);
        $csvContent .= $separateur;
        // Date de transmission en Préfecture
        $csvContent .= Atexo_Util::iso2frnDate($arrayData['date_transmission_prefecture']);
        $csvContent .= $separateur;
        // Vu par
        $nomAgent2 = '';
        $agent2 = (new Atexo_Agent())->retrieveAgentById($arrayData['operation_validation_vues_par']);
        if ($agent2 instanceof CommonAgent) {
            $nomAgent2 = $agent2->getPrenom().' '.$agent2->getNom();
        }
        $csvContent .= $nomAgent2;
        $csvContent .= $separateur;
        // Date de notification
        $csvContent .= Atexo_Util::iso2frnDate($arrayData['date_notification']);
        $csvContent .= $separateur;
        // Commentaires
        $csvContent .= ($arrayData['commentaires'] ? str_replace("\r", '', str_replace("\n", '', $arrayData['commentaires'])) : ' ');
        $csvContent .= $separateur;

        //Unité
        $libelleUnite = (new Atexo_SuiviPassation_Passation())->retrieveLibelleFcspUnite($arrayData['unite'], $organisme);
        $libelleUnite = str_replace('Sélectionnez...', '', str_replace("\r", '', str_replace("\n", '', $libelleUnite)));
        $libelleUnite = $libelleUnite ?: ' ';
        $csvContent .= $libelleUnite;
        $csvContent .= $separateur;
        //Mandataire
        $libelleMandataire = (new Atexo_SuiviPassation_Passation())->retrieveLibelleFcspMandataire($arrayData['mandataire'], $organisme);
        $libelleMandataire = str_replace('Sélectionnez...', '', str_replace("\r", '', str_replace("\n", '', $libelleMandataire)));
        $libelleMandataire = $libelleMandataire ?: ' ';
        $csvContent .= $libelleMandataire;
        $csvContent .= $separateur;
        //identifiant de la consultation
        $csvContent .= $arrayData['id_consultation'];
        $csvContent .= $separateur;
        //identifiant de la passation
        $csvContent .= $arrayData['id_passation_consultation'];
        $csvContent .= $separateur;
        //identifiant de la decision
        $csvContent .= $arrayData['id_decision_enveloppe'];
        $csvContent .= $separateur;
        //identifiant de l'avenant
        $csvContent .= $arrayData['id_avenant'];
        $csvContent .= $separateur;

        return $csvContent;
    }

    public function genererCsvExportAvenants($organisme, $separateur = '|', $withEntete = true)
    {
        $csvContent = '';
        $avenants = (new Atexo_SuiviPassation_Avenant())->retrieveSuiviPassationAvenants($organisme);
        if ($withEntete) {
            $csvContent = "reference_utilisateur|type_procedure|categorie|lot|objet_marche|montant_marche|nom_entreprise|date_notification|numero_avenant|type_avenant|objet_avenant|montant_avenant_ht|montant_avenant_ttc|pourcentage_augmentation_marche_initial|pourcentage_augmentation_cumule|montant_total_marche_tout_avenant_cumule|date_reception_projet_par_secretaire_cao|date_reception_projet_par_charge_etude|date_observation_par_sv|date_retour_projet|date_validation_projet|  	date_validation_projet_vu_par|date_cao|avis_cao|date_cp|date_signature_avenant|date_reception_dossier|date_formulation_observation_par_sv_sur_sdossier|date_retour_dossier_finalise|date_validation_dossier_finalise|date_transmission_prefecture|operation_validation_vues_par|date_notification|commentaires|libelle (unite)|libelle (Mandataire)|id_consultation|id_passation_consultation|id_decision_enveloppe|id_avenant \r\n";
            $csvContent .= "Référence de la consultation|Type de procédure|Catégorie principale de la consultation|N° de lot|Objet du marché|Montant du marché EUR HT|Nom de l'attributaire|Date de notification de l'atributaire|N° d'avenant|Type d'avenant|Objet de l'avenant|Montant de l'avenant HT|Montant de l'avenant TTC|Pourcentage d'augmentation / marché initial|Pourcentage d'augmentation cumulé (tous avenants)|Montant total du marché tous avenants cumulés en HT|Date de réception du projet de présentation de l'avenant par le secrétariat de la CAO|Date de réception du projet de présentation de l'avenant par le chargé d'études du Service Validateur|Date des formulations des premières observations par le Service Validateur sur le projet de rapport|Date de retour du projet de rapport finalisé|Date de validation du projet de rapport par le Service Validateur|Vu par|Date de la CAO|Avis de la CAO|Date de la CP|Date de signature de l'avenant par le Pouvoir Adjudicateur|Date de réception du dossier 'contrôle de légalité' par le Service Validateur|Date des formulations des premières observations par le Service Validateur sur le dosier|Date de retour du dossier finalisé|Date de validation du dossier finalisé par le Service Validateur|Date de transmission en Préfecture|Vu par|Date de notification|Commentaires |Unité|Mandataire responsable du marché|identifiant de la consultation|identifiant de la passation|identifiant de la décision|identifiant de l'avenant \r\n";
        }
        foreach ($avenants as $avenant) {
            $csvAvenant = self::creerLigneAvenant($avenant, $separateur, $organisme);
            $csvContent .= $csvAvenant;
            $csvContent .= "\r\n";
        }

        return $csvContent;
    }
}
