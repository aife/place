<?php

namespace Application\Service\Atexo\Consultation;

use App\Entity\Consultation;
use App\Repository\ConsultationRepository;
use App\Service\ClausesService;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonCategorieLot;
use Application\Propel\Mpe\CommonCategorieLotPeer;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonReferentielConsultation;
use Application\Propel\Mpe\CommonTCritereAttribution;
use Application\Propel\Mpe\CommonTDonneeComplementaire;
use Application\Propel\Mpe\CommonTFormePrix;
use Application\Propel\Mpe\CommonTFormePrixHasRefTypePrix;
use Application\Propel\Mpe\CommonTFormePrixPfHasRefVariation;
use Application\Propel\Mpe\CommonTFormePrixPuHasRefVariation;
use Application\Propel\Mpe\CommonTLotTechnique;
use Application\Propel\Mpe\CommonTLotTechniqueHasTranche;
use Application\Propel\Mpe\CommonTSousCritereAttribution;
use Application\Propel\Mpe\CommonTTranche;
use Application\Propel\Mpe\CommonVisiteLieux;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_Exception;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Referentiel\Atexo_Referentiel_Referentiel;
use Application\Service\Atexo\Response\Atexo_Response_CriteriaVo;
use AtexoDume\Dto\Lot;
use Exception;
use PDO;
use Prado\Prado;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Consultation_Lots
{
    /**
     * Fonction qui supprime -d'une base de donnée d'un Organisme- tous les lots de la consultation dont la référence est passée en paramètre.
     *
     * @param : $reference la reférence de la consultation, $connexion : la connexion propel à utiliser
     */
    public function deleteLots($consultationId, $org)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $c = new Criteria();
        $c->add(CommonCategorieLotPeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonCategorieLotPeer::ORGANISME, $org);
        CommonCategorieLotPeer::doDelete($c, $connexion);
    }

    /**
     * Fonction qui supprime de la base commune tous les lots de la consultation dont la référence est passée en paramètre et qui apprentient
     * à l'organisme passé en paramètre.
     *
     * @param : $organisme : l'organisme propriétaire de la consultation , $reference la reférence de la consultation, $connexion : la connexion propel à utiliser
     */
    public function deleteCommonLots($organisme, $consultationId, $connexion = null)
    {
        if (!is_null($consultationId)) {
            $this->deleteClausesLot($consultationId);
        }
        if (!$connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        }
        $c = new Criteria();
        $c->add(CommonCategorieLotPeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonCategorieLotPeer::ORGANISME, $organisme);
        CommonCategorieLotPeer::doDelete($c, $connexion);
    }

    /**
     * @deprecated use retrieveLot and getArrayLotAndNombreEnveloppeFromObject instead
     * Focntion qui retourne la lise des lots d'une consultation passé en paramètre sous la forme :
     * array (
     *           [numLot1]="Lot ".numLot1 ,  [numLot2]="Lot ".numLot2  ...
     *          )
     *
     * @param $consultation : l'objet consultation ou sa référence,$organime : le nom de l'organisme,
     * $idService : id du sevice de l'agent utilisateur
     *
     * @return array() sous la forme tableau[numLot]="Lot ".numLot , si aucune consultation trouvé retourne un tableau vide.
     */
    public function retrieveArrayLot($consultation, $organisme, $idService, $typeEnv)
    {
        if (!($consultation instanceof CommonConsultation) && !is_numeric($consultation)) {
            throw new Atexo_Exception('Wrong Parameter : Expected Consultation Object or a reference.');
        }
        if (is_numeric($consultation)) {
            $criteriaVo = new Atexo_Consultation_CriteriaVo();
            $criteriaVo->setReference($consultation);
            $criteriaVo->setAcronymeOrganisme($organisme);
            $criteriaVo->setIdService($idService);
            $arrayConsultation = (new Atexo_Consultation())->search($criteriaVo);
            if (is_array($arrayConsultation) && 1 == count($arrayConsultation)) {
                $consultation = array_shift($arrayConsultation);
            } else {
                throw new Atexo_Exception('No Consultation was found');
            }
        }

        if ($consultation instanceof CommonConsultation) {
            if (!is_numeric($consultation->getId())) {
                throw new Atexo_Exception("Error : Consultation Object doesn't contain a valide Reference");
            }
        }
        $typeEnveloppe = (new Atexo_Consultation_Responses())->getTypesEnveloppes($consultation->getEnvCandidature(), $consultation->getEnvOffre(), $consultation->getEnvAnonymat(), $consultation->getEnvOffreTechnique());
        $lots = $consultation->getAllLots();
        $arrayLots = [];
        if ($lots) {
            foreach ($lots as $lot) {
                $criteriaResponse = new Atexo_Response_CriteriaVo();
                $criteriaResponse->setReference($consultation->getId());
                $criteriaResponse->setSousPli($lot->getLot());
                $criteriaResponse->setTypesEnveloppes($typeEnveloppe);
                $criteriaResponse->setOrganisme($organisme);
                if ($typeEnv == Atexo_Config::getParameter('TYPE_ENV_OFFRE')) {
                    $offresElectroniques = (new Atexo_Consultation_Responses())->retrieveEnveloppesOffresElectroniques($criteriaResponse);
                    $nbreReponseElct = is_countable($offresElectroniques) ? count($offresElectroniques) : 0;
                    $offrePapier = (new Atexo_Consultation_Responses())->retrieveEnveloppesOffrePapier($consultation->getId(), $typeEnveloppe, $lot->getLot(), '', '', $organisme, false);
                    $nbreReponsePapi = is_countable($offrePapier) ? count($offrePapier) : 0;
                } elseif ($typeEnv == Atexo_Config::getParameter('TYPE_ENV_ANONYMAT')) {
                    $anonymatElectroniques = (new Atexo_Consultation_Responses())->retrieveEnveloppesAnonymatElectroniques($consultation->getId(), $lot->getLot(), $typeEnveloppe, '', '', $organisme);
                    $nbreReponseElct = is_countable($anonymatElectroniques) ? count($anonymatElectroniques) : 0;
                    $anonymatPapier = (new Atexo_Consultation_Responses())->retrieveEnveloppesAnonymatPapier($consultation->getId(), $typeEnveloppe, $lot->getLot(), '', '', $organisme, false);
                    $nbreReponsePapi = is_countable($anonymatPapier) ? count($anonymatPapier) : 0;
                } elseif ($typeEnv == Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE')) {
                    $offresElectroniques = (new Atexo_Consultation_Responses())->retrieveEnveloppesOffresElectroniques($criteriaResponse);
                    $nbreReponseElct = is_countable($offresElectroniques) ? count($offresElectroniques) : 0;
                    $offrePapier = (new Atexo_Consultation_Responses())->retrieveEnveloppesOffrePapier($consultation->getId(), $typeEnveloppe, $lot->getLot(), '', '', $organisme, false);
                    $nbreReponsePapi = is_countable($offrePapier) ? count($offrePapier) : 0;
                }
                $arrayLots[$lot->getLot()] = 'Lot '.$lot->getLot()." : Electronique = $nbreReponseElct // Papier = $nbreReponsePapi";
            }
        }

        return $arrayLots;
    }

    public function retrieveIntituleLot($consultationId, $idLot, $organisme, $valueToReturnIfNoLotFound = '-')
    {
        $categorieLot = self::retrieveCategorieLot($consultationId, $idLot, $organisme);

        if (is_numeric($idLot) && is_numeric($consultationId)) {
            $categorieLot = self::retrieveCategorieLot($consultationId, $idLot, $organisme);
            if ($categorieLot instanceof CommonCategorieLot) {
                return $categorieLot->getDescriptionTraduite();
            } else {
                return $valueToReturnIfNoLotFound;
            }
        } else {
            throw new Atexo_Exception('Parameters idReference and idLot should be an int');
        }
    }

    public function retrieveNumLot($consultationId, $idLot, $organisme, $valueToReturnIfNoLotFound = '')
    {
        $categorieLot = self::retrieveCategorieLot($consultationId, $idLot, $organisme);
        if (is_numeric($idLot) && is_numeric($consultationId)) {
            $categorieLot = self::retrieveCategorieLot($consultationId, $idLot, $organisme);
            if ($categorieLot instanceof CommonCategorieLot) {
                return $categorieLot->getLot();
            } else {
                return $valueToReturnIfNoLotFound;
            }
        } else {
            throw new Atexo_Exception('Parameters idReference and idLot should be an int');
        }
    }

    public function retrieveArrayIntituleLot($consultationId, $organisme, $addNumLot = false)
    {
        if (is_numeric($consultationId)) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            $c = new Criteria();
            $c->add(CommonCategorieLotPeer::CONSULTATION_ID, $consultationId);
            $c->add(CommonCategorieLotPeer::ORGANISME, $organisme);
            $categoriesLot = CommonCategorieLotPeer::doSelect($c, $connexion);
            if ($categoriesLot) {
                $arrayLots = [];
                foreach ($categoriesLot as $categorieLot) {
                    if ($categorieLot instanceof CommonCategorieLot) {
                        $arrayLots[$categorieLot->getLot()] = ($addNumLot ? Prado::localize('DEFINE_LOT_NUMERO').$categorieLot->getLot().' - ' : '').(new Atexo_Config())->toPfEncoding($categorieLot->getDescriptionTraduite());
                    }
                }

                return $arrayLots;
            } else {
                return [];
            }
        } else {
            throw new Atexo_Exception('Parameters idReference and idLot should be an int');
        }
    }

    /**
     * Enter description here...
     *
     * @param unknown_type $consultationId
     * @param unknown_type $idLot
     * @param unknown_type $organisme
     *
     * @return CategorieLot $categorieLot
     */
    public function retrieveCategorieLot($consultationId, $idLot, $organisme)
    {
        if (is_numeric($idLot) && is_numeric($consultationId) && (int) $idLot >= 0) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            $c = new Criteria();
            $c->add(CommonCategorieLotPeer::CONSULTATION_ID, $consultationId);
            $c->add(CommonCategorieLotPeer::ORGANISME, $organisme);
            $c->add(CommonCategorieLotPeer::LOT, $idLot);
            $categorieLot = CommonCategorieLotPeer::doSelectOne($c, $connexion);
            if ($categorieLot instanceof CommonCategorieLot) {
                return $categorieLot;
            } else {
                return null;
            }
        } else {
            throw new Atexo_Exception('Parameters idReference and idLot should be an int');
        }
    }

    /**
     * Enter description here...
     *
     * @param unknown_type $consultationId
     * @param unknown_type $idLot
     * @param unknown_type $organisme
     *
     * @return CategorieLot $categorieLot
     */
    public static function retrieveCategorieLotCom($consultationId, $idLot, $organisme)
    {
        if (is_numeric($idLot) && is_numeric($consultationId)) {
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            $c = new Criteria();
            $c->add(CommonCategorieLotPeer::ORGANISME, $organisme);
            $c->add(CommonCategorieLotPeer::CONSULTATION_ID, $consultationId);
            $c->add(CommonCategorieLotPeer::LOT, $idLot);
            $categorieLot = CommonCategorieLotPeer::doSelectOne($c, $connexionCom);
            if ($categorieLot instanceof CommonCategorieLot) {
                return $categorieLot;
            } else {
                return null;
            }
        } else {
            throw new Atexo_Exception('Parameters idReference and idLot should be an int');
        }
    }

    public function retrieveLot($consultation, $organisme, $idService)
    {
        if (!($consultation instanceof CommonConsultation) && !is_numeric($consultation)) {
            throw new Atexo_Exception('Wrong Parameter : Expected Consultation Object or a reference.');
        }
        if (is_numeric($consultation)) {
            $criteriaVo = new Atexo_Consultation_CriteriaVo();
            $criteriaVo->setReference($consultation);
            $criteriaVo->setAcronymeOrganisme($organisme);
            $criteriaVo->setIdService($idService);
            $arrayConsultation = (new Atexo_Consultation())->search($criteriaVo);
            if (is_array($arrayConsultation) && 1 == count($arrayConsultation)) {
                $consultation = array_shift($arrayConsultation);
            } else {
                throw new Atexo_Exception('No Consultation was found');
            }
        }

        if ($consultation instanceof CommonConsultation) {
            if (!is_numeric($consultation->getId())) {
                throw new Atexo_Exception("Error : Consultation Object doesn't contain a valide Reference");
            }
        }

        $lots = $consultation->getAllLots();

        return (is_array($lots)) ? $lots : [];
    }

    public function getArrayLotAndNombreEnveloppeFromObject($lots, $typeEnv, $constitutionDossierReponse, $organisme, $statutEnvOuvert = false, $admissibile = false)
    {
        $arrayLots = [];
        if (is_array($lots)) {
            foreach ($lots as $lot) {
                $criteriaResponse = new Atexo_Response_CriteriaVo();
                $criteriaResponse->setReference($lot->getConsultationId());
                $criteriaResponse->setSousPli($lot->getLot());
                $criteriaResponse->setConstitutionDossierReponse($constitutionDossierReponse);
                $criteriaResponse->setTypesEnveloppes($typeEnveloppe);
                $criteriaResponse->setOrganisme($organisme);
                $criteriaResponse->setEnveloppeStatutOuverte($statutEnvOuvert);
                $criteriaResponse->setAdmissible($admissibile);
                if ($typeEnv == Atexo_Config::getParameter('TYPE_ENV_OFFRE')) {
                    $offresElectroniques = (new Atexo_Consultation_Responses())->retrieveEnveloppesOffresElectroniques($criteriaResponse);
                    $nbreReponseElct = is_countable($offresElectroniques) ? count($offresElectroniques) : 0;
                    $offrePapier = (new Atexo_Consultation_Responses())->retrieveEnveloppesOffrePapier($lot->getConsultationId(), $constitutionDossierReponse, $lot->getLot(), '', '', $organisme, false, true, false, $statutEnvOuvert, $admissibile);
                    $nbreReponsePapi = is_countable($offrePapier) ? count($offrePapier) : 0;
                } elseif ($typeEnv == Atexo_Config::getParameter('TYPE_ENV_ANONYMAT')) {
                    $anonymatElectroniques = (new Atexo_Consultation_Responses())->retrieveEnveloppesAnonymatElectroniques($lot->getConsultationId(), $lot->getLot(), $constitutionDossierReponse, '', '', $organisme);
                    $nbreReponseElct = is_countable($anonymatElectroniques) ? count($anonymatElectroniques) : 0;
                    $anonymatPapier = (new Atexo_Consultation_Responses())->retrieveEnveloppesAnonymatPapier($lot->getConsultationId(), $constitutionDossierReponse, $lot->getLot(), '', '', $organisme, false);
                    $nbreReponsePapi = is_countable($anonymatPapier) ? count($anonymatPapier) : 0;
                } elseif ($typeEnv == Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE')) {
                    $offresTechniqueElectroniques = (new Atexo_Consultation_Responses())->retrieveEnveloppesOffresTechniqueElectroniques($lot->getConsultationId(), $lot->getLot(), $constitutionDossierReponse, '', '', $organisme, true, false, $statutEnvOuvert, $admissibile);
                    $nbreReponseElct = is_countable($offresTechniqueElectroniques) ? count($offresTechniqueElectroniques) : 0;
                    $offreTechniquePapier = (new Atexo_Consultation_Responses())->retrieveEnveloppesOffreTechniquePapier($lot->getConsultationId(), $constitutionDossierReponse, $lot->getLot(), '', '', $organisme, false, true, false, $statutEnvOuvert, $admissibile);
                    $nbreReponsePapi = is_countable($offreTechniquePapier) ? count($offreTechniquePapier) : 0;
                }
                $arrayLots[$lot->getLot()] = Prado::localize('DEFINE_LOT_NUMERO').' '.$lot->getLot().' : '.Prado::localize('TEXT_ELECTRONIQUE')." = $nbreReponseElct // ".Prado::localize('TEXT_PAPIER')." = $nbreReponsePapi - ".$lot->getDescriptionTraduite();
            }
        }

        return $arrayLots;
    }

    /**
     * Permet de sauvegarder les lots.
     *
     * @param $listeLots: liste des lots
     * @param $consultation: instance de CommonConsultation
     */
    public function saveLots(&$listeLots, $consultation, $connexion, $copierDonneesComplementaires = false, $refLotsConsParent = false)
    {
        try {
            if (is_array($listeLots) && count($listeLots) && $consultation instanceof CommonConsultation) {
                foreach ($listeLots as $lot) {
                    $lot->setConsultationId($consultation->getId());
                    $lot->setOrganisme($consultation->getOrganisme());
                    $lot->save($connexion);
                    if ($copierDonneesComplementaires) {
                        (new Atexo_Consultation())->copierDonneesComplementaires($lot, $connexion);
                    }
                    //creation suite => recuperer les referentiel des lots de la consultation mere
                    if (is_array($refLotsConsParent[$lot->getLot()]) && count($refLotsConsParent[$lot->getLot()])) {
                        foreach ($refLotsConsParent[$lot->getLot()] as $unReferentiel) {
                            $codeLibelle = $unReferentiel['code_libelle'];
                            $ltRef = (new Atexo_Referentiel_Referentiel())->getLtReferentielByLibelle($codeLibelle);
                            $idReferentiel = $ltRef->getId();

                            $RefLot = new CommonReferentielConsultation();
                            $RefLot->setOrganisme($lot->getOrganisme());
                            $RefLot->setConsultationId($lot->getConsultationId());
                            $RefLot->setIdLtReferentiel($idReferentiel);
                            $RefLot->setLot($lot->getLot());
                            $RefLot->setValeurPrincipaleLtReferentiel($unReferentiel['valeur_Principale_Lt_Referentiel']);
                            $RefLot->setValeurSecondaireLtReferentiel($unReferentiel['valeur_Secondaire_Lt_Referentiel']);
                            $RefLot->save($connexion);
                        }
                    }
                }
            }
        } catch (\Exception $ex) {
            throw $ex;
        }
    }

    /**
     * Retourne un tableau d'objet CategorieLot de la consultation appellante.
     */
    public function retrieveAllLots($organisme, $consultationId, $connexion, $limit = null, $offset = null)
    {
        if (!$connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
        $c = new Criteria();

        if ($consultationId && $organisme) {
            $c->add(CommonCategorieLotPeer::CONSULTATION_ID, $consultationId);
            $c->add(CommonCategorieLotPeer::ORGANISME, $organisme);
        }
        if (null !== $limit) {
            $c->setLimit($limit);
        }
        if (null !== $offset) {
            $c->setOffset($offset);
        }
        $categorieLots = CommonCategorieLotPeer::doSelect($c, $connexion);
        if (is_array($categorieLots) && count($categorieLots)) {
            return $categorieLots;
        } else {
            return false;
        }
    }

    /**
     * Permet de retourner la liste des catégoris lots.
     *
     * @param int    $consultationId la reference de la consultation
     * @param string $organisme      l'organisme de la consultation
     * @param array  $lots           liste des ids lots
     *
     * @return array of objet CommonCategorieLot
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function retrieveListeLotsByArray($consultationId, $organisme, $lots, $typeCritere = Criteria::IN, $limit = null, $offset = null)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonCategorieLotPeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonCategorieLotPeer::ORGANISME, $organisme);

        $c->add(CommonCategorieLotPeer::LOT, $lots, $typeCritere);
        if (null !== $limit) {
            $c->setLimit($limit);
        }
        if (null !== $offset) {
            $c->setOffset($offset);
        }
        $categorieLots = CommonCategorieLotPeer::doSelect($c, $connexion);
        if ($categorieLots) {
            return $categorieLots;
        } else {
            return [];
        }
    }

    /**
     * Permet de duplique un  lot.
     *
     * @param CommonCatgorieLot $lot
     * @param int               $newNumLot
     * @param string            $newDescription
     *
     * @return CommonCatgorieLot of objet CommonCategorieLot
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since  2016-develop
     *
     * @copyright Atexo 2016
     */
    public static function dupliquerLot($lot, $newNumLot, $newDescription)
    {
        $connexion = null;
        try {
            $logger = Atexo_LoggerManager::getLogger('app');
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $connexion->beginTransaction();
            if ($lot instanceof CommonCategorieLot) {
                $logger->info("Duplication de l'objet CommonCategorieLot");
                $newLot = $lot->copy();
                $newLot->setLot($newNumLot);
                $newLot->setIdTrDescription(null);
                $newLot->setIdTrDescriptionDetail(null);
                $newLot->setDescriptionTraduite(null, $newDescription);
                $newLot->setDescriptionDetailTraduite(null, $lot->getDescriptionDetailTraduite());
                if (Atexo_Module::isEnabled('DonneesComplementaires') && $lot->getIdDonneeComplementaire()) {// Données complementaire
                    $donneesComplementaires = $lot->getDonneComplementaire();
                    if ($donneesComplementaires instanceof CommonTDonneeComplementaire) {
                        $logger->info("Duplication de l'objet CommonTDonneeComplementaire");
                        $newDonneesComplementaires = $donneesComplementaires->copy();
                        $newDonneesComplementaires->setIdTrPiecesDossierAdmin(null);
                        $newDonneesComplementaires->setIdTrPiecesDossierTech(null);
                        $newDonneesComplementaires->setIdTrPiecesDossierAdditif(null);
                        $newDonneesComplementaires->setIdTrAdresseRetraisDossiers(null);
                        $newDonneesComplementaires->setIdTrAdresseDepotOffres(null);
                        $newDonneesComplementaires->setIdTrLieuOuverturePlis(null);
                        $newDonneesComplementaires->setIdTrAddEchantillon(null);
                        $newDonneesComplementaires->setIdTrAddReunion(null);

                        $newDonneesComplementaires->setPiecesDossierAdminTraduit($donneesComplementaires->getPiecesDossierAdminTraduit());
                        $newDonneesComplementaires->setPiecesDossierTechTraduit($donneesComplementaires->getPiecesDossierTechTraduit());
                        $newDonneesComplementaires->setPiecesDossierAdditifTraduit($donneesComplementaires->getPiecesDossierAdditifTraduit());
                        $newDonneesComplementaires->setAdresseRetraisDossiersTraduit($donneesComplementaires->getAdresseRetraisDossiersTraduit());
                        $newDonneesComplementaires->setAdresseDepotOffresTraduit($donneesComplementaires->getAdresseDepotOffresTraduit());
                        $newDonneesComplementaires->setLieuOuverturePlisTraduit($donneesComplementaires->getLieuOuverturePlisTraduit());
                        $newDonneesComplementaires->setAddEchantillonTraduit(null, $donneesComplementaires->getAddEchantillonTraduit());
                        $newDonneesComplementaires->setAddReunionTraduit(null, $donneesComplementaires->getAddReunionTraduit());
                        //Visites des lieux
                        if (Atexo_Module::isEnabled('ConsultationVisiteDesLieux')) {
                            $logger->info('Duplication des VisitesLieux');
                            $visitesLieux = (new Atexo_Consultation())->retreiveVisitesLieuxByRef($lot->getConsultationId(), $lot->getLot(), $lot->getOrganisme());
                            if (is_array($visitesLieux)) {
                                foreach ($visitesLieux as $visite) {
                                    if ($visite instanceof CommonVisiteLieux) {
                                        $visiteLieu = $visite->copy();
                                        $visiteLieu->setIdTrAdresse(null);
                                        $visiteLieu->setAdresseTraduit(null, $visite->getAdresseTraduit());
                                        $visiteLieu->setLot($lot);
                                        $visiteLieu->setConsultationId($lot->getConsultationId());
                                        $visiteLieu->save($connexion);
                                    }
                                }
                            }
                        }
                        $newDonneesComplementaires->save($connexion);
                        $newLot->setIdDonneeComplementaire($newDonneesComplementaires->getIdDonneeComplementaire());
                        if (Atexo_Module::isEnabled('DonneesRedac')) { //Données Redac
                            $logger->info('Duplication des Formes prix');
                            (new self())->dupliquerFormePrix($donneesComplementaires, $newDonneesComplementaires, $connexion);
                            $logger->info('Duplication des Critères');
                            (new self())->dupliquerCriteres($donneesComplementaires, $newDonneesComplementaires, $connexion);
                        }
                        $newDonneesComplementaires->save($connexion);
                    }
                }

                $logger->info('Duplication des LtReferentiel');
                (new self())->dupliquerLtRefentiel($lot, $newLot, $connexion);
                $logger->info('enregistrement du lot');
                $newLot->save($connexion);
                $connexion->commit();

                Atexo_Consultation::duplicateLotAchatsResponsablesFromConsultation(
                    $lot,
                    $newLot->getId()
                );
                return true;
            } else {
                $logger->info("L'objet fourni n'est pas un lot ".print_r($lot, true));
            }
        } catch (Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('app');
            $logger->error('Erreur lors de la duplication du lot '.$e->getMessage().'   '.$e->getTraceAsString());
            if ($connexion instanceof PDO) {
                $connexion->rollBack();
            }

            return false;
        }
    }

    /**
     * Permet de duplique forme prix.
     *
     * @param CommonTDonneeComplementaire $donneesComplementaires
     * @param CommonTDonneeComplementaire $newDonneesComplementaires
     * @param PDO                         $connexion
     *
     * @return void
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since  2016-develop
     *
     * @copyright Atexo 2016
     */
    public function dupliquerFormePrix($donneesComplementaires, $newDonneesComplementaires, $connexion)
    {
        if ($donneesComplementaires instanceof CommonTDonneeComplementaire && $newDonneesComplementaires instanceof CommonTDonneeComplementaire) {
            $coresspondanceNewAndOld = [];
            // supprimer tout les tranche et et les formes de rpix lié à ceette donnee complementaire
            $formePrix = $donneesComplementaires->getFormePrix();
            if ($formePrix instanceof CommonTFormePrix) {
                $listeTypesPrix = $formePrix->getAllTypesPrix();
                $listeVariationsPrixForfaitaire = $formePrix->getAllVariationsPrixForfaitaire();
                $listeVariationsPrixUnitaire = $formePrix->getAllVariationsPrixUnitaire();

                $formePrixNew = $formePrix->copy();
                $formePrixNew->save($connexion);

                if (is_array($listeTypesPrix)) {
                    foreach ($listeTypesPrix as $typePrix) {
                        $typePrixNew = new CommonTFormePrixHasRefTypePrix();
                        $typePrixNew->setIdTypePrix($typePrix->getIdTypePrix());
                        $typePrixNew->setIdFormePrix($formePrixNew->getIdFormePrix());
                        $typePrixNew->save($connexion);
                    }
                }
                if (is_array($listeVariationsPrixForfaitaire)) {
                    foreach ($listeVariationsPrixForfaitaire as $variationPF) {
                        $variationPFNew = new CommonTFormePrixPfHasRefVariation();
                        $variationPFNew->setIdVariation($variationPF->getIdVariation());
                        $variationPFNew->setIdFormePrix($formePrixNew->getIdFormePrix());
                        $variationPFNew->save($connexion);
                    }
                }

                if (is_array($listeVariationsPrixUnitaire)) {
                    foreach ($listeVariationsPrixUnitaire as $variationPU) {
                        $variationPUNew = new CommonTFormePrixPuHasRefVariation();
                        $variationPUNew->setIdVariation($variationPU->getIdVariation());
                        $variationPUNew->setIdFormePrix($formePrixNew->getIdFormePrix());
                        $variationPUNew->save($connexion);
                    }
                }
                $newDonneesComplementaires->setIdFormePrix($formePrixNew->getIdFormePrix());
            } else {
                $listeTranches = $donneesComplementaires->getAllTranches();
                $coresspondanceNewAndOld = [];
                if (is_array($listeTranches) && count($listeTranches)) {
                    foreach ($listeTranches as $tranche) {
                        if ($tranche instanceof CommonTTranche) {
                            $newTranche = new CommonTTranche();
                            $newTranche->setNatureTranche($tranche->getNatureTranche());
                            $newTranche->setCodeTranche($tranche->getCodeTranche());
                            $newTranche->setIntituleTranche($tranche->getIntituleTranche());
                            $newTranche->setIdDonneeComplementaire($newDonneesComplementaires->getIdDonneeComplementaire());
                            $formePrix = $tranche->getFormePrix();
                            if ($formePrix instanceof CommonTFormePrix) {
                                $listeTypesPrix = $formePrix->getAllTypesPrix();
                                $listeVariationsPrixForfaitaire = $formePrix->getAllVariationsPrixForfaitaire();
                                $listeVariationsPrixUnitaire = $formePrix->getAllVariationsPrixUnitaire();

                                $formePrixNew = $formePrix->copy();
                                $formePrixNew->save($connexion);

                                if (is_array($listeTypesPrix)) {
                                    foreach ($listeTypesPrix as $typePrix) {
                                        $typePrixNew = new CommonTFormePrixHasRefTypePrix();
                                        $typePrixNew->setIdTypePrix($typePrix->getIdTypePrix());
                                        $typePrixNew->setIdFormePrix($formePrixNew->getIdFormePrix());
                                        $typePrixNew->save($connexion);
                                    }
                                }

                                if (is_array($listeVariationsPrixForfaitaire)) {
                                    foreach ($listeVariationsPrixForfaitaire as $variationPF) {
                                        $variationPFNew = new CommonTFormePrixPfHasRefVariation();
                                        $variationPFNew->setIdVariation($variationPF->getIdVariation());
                                        $variationPFNew->setIdFormePrix($formePrixNew->getIdFormePrix());
                                        $variationPFNew->save($connexion);
                                    }
                                }

                                if (is_array($listeVariationsPrixUnitaire)) {
                                    foreach ($listeVariationsPrixUnitaire as $variationPU) {
                                        $variationPUNew = new CommonTFormePrixPuHasRefVariation();
                                        $variationPUNew->setIdVariation($variationPU->getIdVariation());
                                        $variationPUNew->setIdFormePrix($formePrixNew->getIdFormePrix());
                                        $variationPUNew->save($connexion);
                                    }
                                }

                                $newTranche->setIdFormePrix($formePrixNew->getIdFormePrix());
                            }

                            $newTranche->save($connexion);
                            $coresspondanceNewAndOld[$tranche->getIdTranche()] = $newTranche->getIdTranche();
                        }
                    }
                }
            }
            $lotsTechniques = (new CommonTLotTechnique())->retrieveLotTechniqueByDonneComplementaire($donneesComplementaires->getIdDonneeComplementaire());
            if (is_array($lotsTechniques)) {
                foreach ($lotsTechniques as $oneLot) {
                    if ($oneLot instanceof CommonTLotTechnique) {
                        $newLotTechnique = $oneLot->copy();
                        $newLotTechnique->setIdDonneeComplementaire($newDonneesComplementaires->getIdDonneeComplementaire());
                        $newLotTechnique->save($connexion);
                        $lotHasTranche = $oneLot->getAllLotTechniqueTranche();
                        foreach ($lotHasTranche as $oneLotHasTranche) {
                            if ($oneLotHasTranche instanceof CommonTLotTechniqueHasTranche) {
                                $lotTranche = new CommonTLotTechniqueHasTranche();
                                $lotTranche->setIdLotTechnique($newLotTechnique->getIdLotTechnique());
                                $lotTranche->setIdTranche($coresspondanceNewAndOld[$oneLotHasTranche->getIdTranche()]);
                                $lotTranche->save($connexion);
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Permet de dupliquer les critères.
     *
     * @param CommonTDonneeComplementaire $donneesComplementaires
     * @param CommonTDonneeComplementaire $newDonneesComplementaires
     * @param PDO                         $connexion
     *
     * @return void
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since  2016-develop
     *
     * @copyright Atexo 2016
     */
    public function dupliquerCriteres($donneesComplementaires, $newDonneesComplementaires, $connexion)
    {
        if ($donneesComplementaires instanceof CommonTDonneeComplementaire && $newDonneesComplementaires instanceof CommonTDonneeComplementaire) {
            $critereAttributions = $donneesComplementaires->getAllCriteresAttributions();
            if (is_array($critereAttributions)) {
                foreach ($critereAttributions as $oneCritere) {
                    if ($oneCritere instanceof CommonTCritereAttribution) {
                        $newCritere = $oneCritere->copy();
                        $newCritere->setIdDonneeComplementaire($newDonneesComplementaires->getIdDonneeComplementaire());
                        $newCritere->save($connexion);
                        $sousCriters = $oneCritere->getSousCriteres($connexion);
                        foreach ($sousCriters as $sousCriter) {
                            if ($sousCriter instanceof CommonTSousCritereAttribution) {
                                $newSousCritere = $sousCriter->copy();
                                $newSousCritere->setIdCritereAttribution($newCritere->getIdCritereAttribution());
                                $newSousCritere->save($connexion);
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Permet de dupliquer les critères.
     *
     * @param CommonCategorieLot $lot
     * @param CommonCategorieLot $newLot
     * @param PDO                $connexion
     *
     * @return void
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since  2016-develop
     *
     * @copyright Atexo 2016
     */
    public function dupliquerLtRefentiel($lot, $newLot, $connexion)
    {
        if ($lot instanceof CommonCategorieLot && $newLot instanceof CommonCategorieLot) {
            $commonRefsCons = (new Atexo_Referentiel_Referentiel())->getLtReferentielByRefConsultationAndLot($lot->getConsultationId(), $lot->getLot(), $lot->getOrganisme());
            if (is_array($commonRefsCons)) {
                foreach ($commonRefsCons as $commonRefCons) {
                    if ($commonRefCons instanceof CommonReferentielConsultation) {
                        $newCommonRefCons = $commonRefCons->copy();
                        $newCommonRefCons->setLot($newLot->getLot());
                        $newCommonRefCons->save($connexion);
                    }
                }
            }
        }
    }

    private function deleteClausesLot(int $consultationId): void
    {
        /** @var ConsultationRepository $consultationRepository */
        $consultationRepository = Atexo_Util::getSfService(ConsultationRepository::class);
        /** @var Consultation $consultationSf */
        $consultationSf = $consultationRepository->find($consultationId);

        if ($consultationSf instanceof Consultation) {
            $lots = $consultationSf->getLots();

            /** @var ClausesService $clausesService */
            $clausesService = Atexo_Util::getSfService(ClausesService::class);

            foreach ($lots as $lot) {
                $clausesService->deleteClausesRelatedTo($lot);
            }
        }
    }


    public function deleteClausesForLot(int $consultationId, int $lotId): void
    {
        /** @var ConsultationRepository $consultationRepository */
        $consultationRepository = Atexo_Util::getSfService(ConsultationRepository::class);
        /** @var Consultation $consultationSf */
        $consultationSf = $consultationRepository->find($consultationId);

        if ($consultationSf instanceof Consultation) {
            $lots = $consultationSf->getLots();

            /** @var ClausesService $clausesService */
            $clausesService = Atexo_Util::getSfService(ClausesService::class);

            foreach ($lots as $lot) {
                if ($lotId == $lot->getId()) {
                    $clausesService->deleteClausesRelatedTo($lot);
                }
            }
        }
    }
}
