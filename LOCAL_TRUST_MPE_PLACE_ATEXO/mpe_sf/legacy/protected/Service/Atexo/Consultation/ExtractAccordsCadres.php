<?php

namespace Application\Service\Atexo\Consultation;

use Application\Propel\Mpe\CommonTTypeContratQuery;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Util;
use AtexoCrypto\Dto\Consultation;
use Exception;
use PDO;
use Prado\Prado;

/*
 * Created on 23 déc. 2013
 *
 * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
 * @package
 */
class Atexo_Consultation_ExtractAccordsCadres
{
    public function genererCsvAccordCadres($arrayOrgToExclude, $logger)
    {
        $listOrg = null;
        $i = 0;
        foreach ($arrayOrgToExclude as $oneOrg) {
            if ($i > 0) {
                $listOrg .= ',';
            }
            $listOrg .= '"'.$oneOrg.'"';
            ++$i;
        }
        $typeQuery = new CommonTTypeContratQuery();
        $arrayIdsTypeContrat = $typeQuery->getTypeContratACSAD(Atexo_Config::getParameter('TYPE_CONTRAT_ACCORD_CADRE'));
        if (is_array($arrayIdsTypeContrat) && count($arrayIdsTypeContrat)) {
            $idsTypeContrat = implode(',', $arrayIdsTypeContrat);

            $typeAvis = Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION');
            $sql = <<<SQL
				SELECT `reference_utilisateur` ,`intitule`, `datefin`, `code_cpv_1`, `Service`.`siren`, `Service`.`complement`, `Organisme`.siren, `Organisme`.complement, `consultation`.service_id
				FROM `consultation` LEFT JOIN  `Service` ON (`consultation`.organisme =`Service`.organisme and `consultation`.service_id = `Service`.id), `Organisme`
				WHERE `consultation`.`organisme` = `Organisme`.`acronyme`
				AND `consultation`.`organisme` NOT IN ($listOrg)
				AND `Organisme`.`active` = 1
				AND `consultation`.`id_type_avis` = $typeAvis
				AND consultation.type_marche IN ($idsTypeContrat)

SQL;

            $logger->info('La requete SQL pour récuperer les consultations de type accord cadre '.$sql);
            $statement = Atexo_Db::getLinkCommon(true)->prepare($sql);
            $statement->execute();

            $separateur = ';';
            $escapeChar = '"';
            $entete = Prado::localize('DEFINE_TEXT_REFERENCE').' '.Prado::localize('DEFINE_ACCORD_CADRES').$separateur;
            $entete .= Prado::localize('DEFINE_INTITULE').' '.Prado::localize('DEFINE_ACCORD_CADRES').$separateur;
            $entete .= Prado::localize('DEFINE_DATE_REMISE').' '.Prado::localize('DEFINE_ACCORD_CADRES').$separateur;
            $entete .= Prado::localize('TEXT_CODE_CPV').' '.Prado::localize('DEFINE_ACCORD_CADRES').$separateur;
            $entete .= Prado::localize('TEXT_SIRET').' '.Prado::localize('DEFINE_ACHETEUR_ACCORDD_CADRE').$separateur;
            $data = '';
            while ($row = $statement->fetch(PDO::FETCH_NUM)) {
                $row[0] = self::addEscapeChar($row[0], $escapeChar);
                $row[1] = self::addEscapeChar($row[1], $escapeChar);

                $data .= $row[0].$separateur;
                $data .= $row[1].$separateur;
                $data .= $row[2].$separateur;
                $data .= $row[3].$separateur;
                if (0 == $row[4]) {//consultation attaché à l'organisme=> siren de l'organisme
                    $data .= $row[6].$row[7].$separateur;
                } else {//siren du service créateur
                    $data .= $row[4].$row[5].$separateur;
                }
                $data .= "\n";
            }
            $contenuCsv = $entete."\n".$data;
            if (is_dir(Atexo_Config::getParameter('REPERTOIRE_CSV_ACCORD_CADRES'))) {
                $pointeur = opendir(Atexo_Config::getParameter('REPERTOIRE_CSV_ACCORD_CADRES'));
                while ($fichier = readdir($pointeur)) {
                    if (('.' != $fichier) && ('..' != $fichier)) {
                        if (is_file(Atexo_Config::getParameter('REPERTOIRE_CSV_ACCORD_CADRES').'/'.$fichier)) {
                            system('mv '.Atexo_Config::getParameter('REPERTOIRE_CSV_ACCORD_CADRES').'/'.$fichier.' '.Atexo_Config::getParameter('REPERTOIRE_ARCHIVES_CSV_ACCORD_CADRES').'/');
                        }
                    }
                }
                $fileName = Atexo_Config::getParameter('NAME_FILE_CSV_ACCORD_CADRES').'_'.date('Y_m_d_H_i').'.csv';
                $pathFile = Atexo_Config::getParameter('REPERTOIRE_CSV_ACCORD_CADRES').'/'.$fileName;
                Atexo_Util::writeFile($pathFile, $contenuCsv);
                $code = $contenuCsv;
                self::sendFileCsvAccordCadres($fileName, $code);
            }
        } else {
            throw new Exception("Il n'y a pas un type de contrat Accord Cadre");
        }
    }

    public function sendFileCsvAccordCadres($fileName, $code)
    {
        $to = Atexo_Config::getParameter('MAIL_SEND_CSV_ACCORD_CADRES');
        if ('' != $to) {
            $from = Atexo_Config::getParameter('PF_MAIL_FROM');
            $subject = Atexo_Config::getParameter('PF_SHORT_NAME').' - '.Prado::localize('DEFINE_LISTE_ACCORDD_CADRE').' - '.date('d/m/Y');

            $message = file_get_contents(Atexo_Config::getParameter('PATH_FILE_MAIL_GENERIC_TEMPLATE'));
            $message = str_replace('IMAGE_BANDEAU', Atexo_Message::getImageBandeau(), $message);
            $message = str_replace('SALUTATION', Prado::localize('CORPS_DEBUT'), $message);
            $message = str_replace('TITRE', Prado::localize('TITRE'), $message);
            $message = str_replace('CORPS_MAIL', Prado::localize('DEFINE_MESSAGE_MAIL_LISTE_ACCORD_CADRES').date('d/m/Y'), $message);
            $message = str_replace('PLACE_MARCHE_PUBLIC_INTERMINISTERE', Prado::localize('PLACE_MARCHE_PUBLIC_INTERMINISTERE').date('d/m/Y'), $message);
            $message = str_replace('CORDIALEMENT', Prado::localize('CORDIALEMENT'), $message);

            (new Atexo_Message())->mailWithAttachement($from, $to, $subject, $message, '', $code, $fileName, '', true);
        }
    }

    public function addEscapeChar($value, $escapeChar)
    {
        if (strstr($value, '"') || strstr($value, ',') || strstr($value, ';')) {
            $value = str_replace('"', '""', $value);
            $value = $escapeChar.$value.$escapeChar;
        }

        return $value;
    }
}
