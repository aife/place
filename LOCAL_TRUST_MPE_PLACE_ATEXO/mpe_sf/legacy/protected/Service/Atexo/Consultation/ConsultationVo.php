<?php

namespace Application\Service\Atexo\Consultation;

use AtexoCrypto\Dto\Consultation;

/**
 * Contient le détail d'une consultation.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 *
 * @todo à compléter
 */
class Atexo_Consultation_ConsultationVo
{
    private int $idReference = 0;
    private string $referenceUser = '';
    private string $objetConsultation = '';
    private string $entitePublic = '';
    private int $idService = 0;
    private int $idTypeAnnonce = 0;
    private int $idTypeProcedure = 0;
    private string $typeAcces = '';
    private string $intituleConsultation = '';
    private string $categoriePrincipale = '';
    private string $dateLimiteRemisePlis = '';
    private string $dateMiseEnLigne = '';
    private string $allotissement = '';
    private array $intituleLot = [];
    private int $dceId = 0;
    private string $dceSize = '';
    private int $reglementConsultationId = 0;
    private string $reglementConsultationSize = '';
    private string $entiteAchatRatahcement = '';
    private string $entiteAchatAssocie = '';
    private string $commentaireInterne = '';
    private $autoriserreponseElectronique;
    private string $signatureElectroniquerequise = '';
    private string $chiffrementPlis = '';
    private string $etatConsultation = '';
    private int $idEtatConsulation = 0;
    private string $auteurConsultation = '';
    private int $nombreRegistreRetraitElectronique = 0;
    private int $nombreRegistreRetraitPapier = 0;
    private int $nombreQuestionsElectronique = 0;
    private int $nombreQuestionsPapier = 0;
    private int $nombreRegistreDepotElectronique = 0;
    private int $nombreRegistreDepotPapier = 0;
    private string $dateFin = '';
    private string $dateValidation = '';
    private bool $habilitatedToChangeTender = false;
    private bool $habilitatedToSeeAdvertizeTender = false;
    private bool $habilitatedToValidateTender = false;
    private bool $habilitatedToGoThroughTender = false;
    private bool $habilitatedToAscribe = false;

    //////////////////////////////////////////////

    public function getIdReference()
    {
        return $this->idReference;
    }

    public function setIdReference($idReference)
    {
        $this->idReference = $idReference;
    }

    public function getReferenceUser()
    {
        return $this->referenceUser;
    }

    public function setReferenceUser($referenceUser)
    {
        $this->referenceUser = $referenceUser;
    }

    public function getObjetConsultation()
    {
        return $this->objetConsultation;
    }

    public function setObjetConsultation($objetConsultation)
    {
        $this->objetConsultation = $objetConsultation;
    }

    public function getEntitePublic()
    {
        return $this->entitePublic;
    }

    public function setEntitePublic($value)
    {
        $this->entitePublic = $value;
    }

    public function getServiceId()
    {
        return $this->idService;
    }

    public function setServiceId($value)
    {
        $this->idService = $value;
    }

    public function getIdTypeAnnonce()
    {
        return $this->idTypeAnnonce;
    }

    public function setIdTypeAnnonce($value)
    {
        $this->idTypeAnnonce = $value;
    }

    public function getIdTypeProcedure()
    {
        return $this->idTypeProcedure;
    }

    public function setIdTypeProcedure($value)
    {
        $this->idTypeProcedure = $value;
    }

    public function getTypeAnnonce()
    {
        return $this->typeProcedure;
    }

    public function setTypeAnnonce($value)
    {
        $this->typeProcedure = $value;
    }

    public function getIntituleConsultation()
    {
        return $this->intituleConsultation;
    }

    public function setIntituleConsultation($value)
    {
        $this->intituleConsultation = $value;
    }

    public function getCategoriePrincipale()
    {
        return $this->categoriePrincipale;
    }

    public function getDateLimiteRemisePlis()
    {
        return $this->dateLimiteRemisePlis;
    }

    public function getDateMiseEnLigne()
    {
        return $this->dateMiseEnLigne;
    }

    public function getAllotissement()
    {
        return $this->allotissement;
    }

    public function getDceId()
    {
        return $this->dceId;
    }

    public function getDceSize()
    {
        return $this->dceSize;
    }

    public function getReglementConsultation()
    {
        return $this->reglementConsultationId;
    }

    public function getReglementConsultationSize()
    {
        return $this->reglementConsultationSize;
    }

    public function getEntiteAchatRatachement()
    {
        return $this->entiteAchatRatahcement;
    }

    public function getEntiteAchatAssocie()
    {
        return $this->entiteAchatAssocie;
    }

    public function getCommentaireInterne()
    {
        return $this->commentaireInterne;
    }

    public function getAutoriserReponseElectronique()
    {
        return $this->autoriserreponseElectronique;
    }

    public function getSignatureElectroniquerequise()
    {
        return $this->signatureElectroniquerequise;
    }

    public function getChiffrementPlis()
    {
        return $this->chiffrementPlis;
    }

    public function getIntituleLot()
    {
        return $this->intituleLot;
    }

    //////////////////////////////////////
    public function getReference()
    {
        return $this->reference;
    }

    public function getObjet()
    {
        return $this->objet;
    }

    public function getEntiteAchat()
    {
        return $this->entiteAchat;
    }

    public function getTypeAcces()
    {
        return $this->typeAcces;
    }

    public function setTypeAcces($value)
    {
        $this->typeAcces = $value;
    }

    public function setCategoriePrincipale($value)
    {
        $this->categoriePrincipale = $value;
    }

    public function setDateLimiteRemisePlis($value)
    {
        $this->dateLimiteRemisePlis = $value;
    }

    public function setDateMiseEnLigne($value)
    {
        $this->dateMiseEnLigne = $value;
    }

    public function setAllotissement($value)
    {
        $this->allotissement = $value;
    }

    public function setDceId($value)
    {
        $this->dceId = $value;
    }

    public function setDceSize($value)
    {
        $this->dceSize = $value;
    }

    public function setReglementConsultationId($value)
    {
        $this->reglementConsultationId = $value;
    }

    public function getEntiteAchatRatahcement()
    {
        return $this->entiteAchatRatahcement;
    }

    public function setEntiteAchatRatahcement($value)
    {
        $this->entiteAchatRatahcement = $value;
    }

    public function setEntiteAchatAssocie($value)
    {
        $this->entiteAchatAssocie = $value;
    }

    public function setCommentaireInterne($value)
    {
        $this->entitePcommentaireInterneublic = $value;
    }

    public function setAutoriserReponseElectronique($value)
    {
        $this->autoriserreponseElectronique = $value;
    }

    public function setSignatureElectroniquerequise($value)
    {
        $this->signatureElectroniquerequise = $value;
    }

    public function setChiffrementPlis($value)
    {
        $this->chiffrementPlis = $value;
    }

    public function setIntituleLot($value)
    {
        $this->intituleLot = $value;
    }

    public function getEtatConsultation()
    {
        return $this->etatConsultation;
    }

    public function setEtatConsultation($etatConsultation)
    {
        $this->etatConsultation = $etatConsultation;
    }

    public function getIdEtatConsultation()
    {
        return $this->idEtatConsulation;
    }

    public function setIdEtatConsulation($idEtatConsulation)
    {
        $this->idEtatConsulation = $idEtatConsulation;
    }

    public function getAuteurConsultation()
    {
        return $this->auteurConsultation;
    }

    public function setAuteurConsultation($auteurConsultation)
    {
        $this->auteurConsultation = $auteurConsultation;
    }

    public function getNombreRegistreRetraitElectronique()
    {
        return $this->nombreRegistreRetraitElectronique;
    }

    public function setNombreRegistreRetraitElectronique($nombreRegistreRetraitElectronique)
    {
        $this->nombreRegistreRetraitElectronique = $nombreRegistreRetraitElectronique;
    }

    public function getNombreRegistreRetraitPapier()
    {
        return $this->nombreRegistreRetraitPapier;
    }

    public function setNombreRegistreRetraitPapier($nombreRegistreRetraitPapier)
    {
        $this->nombreRegistreRetraitPapier = $nombreRegistreRetraitPapier;
    }

    public function getNombreQuestionsElectronique()
    {
        return $this->nombreQuestionsElectronique;
    }

    public function setNombreQuestionsElectronique($nombreQuestionsElectronique)
    {
        $this->nombreQuestionsElectronique = $nombreQuestionsElectronique;
    }

    public function getNombreQuestionsPapier()
    {
        return $this->nombreQuestionsPapier;
    }

    public function setNombreQuestionsPapier($nombreQuestionsPapier)
    {
        $this->nombreQuestionsPapier = $nombreQuestionsPapier;
    }

    public function getNombreRegistreDepotElectronique()
    {
        return $this->nombreRegistreDepotElectronique;
    }

    public function setNombreRegistreDepotElectronique($nombreRegistreDepotElectronique)
    {
        $this->nombreRegistreDepotElectronique = $nombreRegistreDepotElectronique;
    }

    public function getNombreRegistreDepotPapier()
    {
        return $this->nombreRegistreDepotPapier;
    }

    public function setNombreRegistreDepotPapier($nombreRegistreDepotPapier)
    {
        $this->nombreRegistreDepotPapier = $nombreRegistreDepotPapier;
    }

    public function getDateFin()
    {
        return $this->dateFin;
    }

    public function setDateFin($dateFin)
    {
        $this->dateFin = $dateFin;
    }

    public function getDateValidation()
    {
        return $this->dateValidation;
    }

    public function setDateValidation($dateValidation)
    {
        $this->dateValidation = $dateValidation;
    }

    public function getHabilitatedToChangeTender()
    {
        return $this->habilitatedToChangeTender;
    }

    public function setHabilitatedToChangeTender($habilitatedToChangeTender)
    {
        $this->habilitatedToChangeTender = $habilitatedToChangeTender;
    }

    public function getHabilitatedToSeeAdvertizeTender()
    {
        return $this->habilitatedToSeeAdvertizeTender;
    }

    public function setHabilitatedToSeeAdvertizeTender($habilitatedToSeeAdvertizeTender)
    {
        $this->habilitatedToSeeAdvertizeTender = $habilitatedToSeeAdvertizeTender;
    }

    public function getHabilitatedToValidateTender()
    {
        return $this->habilitatedToValidateTender;
    }

    public function setHabilitatedToValidateTender($habilitatedToValidateTender)
    {
        $this->habilitatedToValidateTender = $habilitatedToValidateTender;
    }

    public function getHabilitatedToGoThroughTender()
    {
        return $this->habilitatedToGoThroughTender;
    }

    public function setHabilitatedToGoThroughTender($habilitatedToGoThroughTender)
    {
        $this->habilitatedToGoThroughTender = $habilitatedToGoThroughTender;
    }

    public function getHabilitatedToAscribe()
    {
        return $this->habilitatedToAscribe;
    }

    public function setHabilitatedToAscribe($habilitatedToAscribe)
    {
        $this->habilitatedToAscribe = $habilitatedToAscribe;
    }
}
