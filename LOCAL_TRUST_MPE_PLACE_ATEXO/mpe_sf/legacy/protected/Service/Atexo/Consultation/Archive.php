<?php

namespace Application\Service\Atexo\Consultation;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonTDocument;
use Application\Propel\Mpe\CommonTDocumentQuery;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Document;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Tree\Atexo_Consultation_Tree_Arborescence;
use Application\Service\Atexo\Consultation\Tree\Atexo_Consultation_Tree_Directory;
use Application\Service\Atexo\Consultation\Tree\Atexo_Consultation_Tree_File;
use Application\Service\Atexo\Consultation\Tree\Atexo_Consultation_Tree_Group;
use Application\Service\Atexo\Consultation\Tree\Atexo_Consultation_Tree_Item;
use Application\Service\Atexo\Consultation\Tree\Atexo_Consultation_Tree_Signature;
use Application\Service\Atexo\Consultation\Tree\Atexo_Consultation_Tree_String;
use Application\Service\Atexo\Consultation\Tree\Atexo_Consultation_Tree_TimeStamp;
use Application\Service\Atexo\Rest\Action\Atexo_Rest_Action_Actions;
use Exception;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * Gestion des archives.
 *
 * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Consultation_Archive
{
    const DIR_DOCUMENTS_EXTERNES = 'documents_externes';
    const DIR_CONSULTATION = 'consultation';
    const DIR_DOSSIER_CONSULTATION = 'dossier_consultation';
    const DIR_PUBLICITES = 'publicites';
    const DIR_MESSAGES_ECHANGES = 'messages_echanges';
    const DIR_REGISTRES = 'registres';
    const DIR_CANDIDATURES_OFFRES_DECISION = 'candidatures_offres_decision';
    const DIR_CANDIDATURES = 'candidatures';
    const DIR_CANDIDATURES_RETENUES = 'candidatures_retenues';
    const DIR_CANDIDATURES_NON_RETENUES = 'candidatures_non_retenues';
    const DIR_RECEPTION_EXAMEN_OFFRES = 'reception_et_examen_offres';
    const DIR_OUVERTURE_PLIS_ANALYSE = 'ouverture_plis_et_analyse';
    const DIR_REFERENCE_CONSULTATION_LOT = 'reference_consultation_lot';
    const DIR_OFFRES_NON_RETENUES = 'offres_non_retenues';
    const DIR_OFFRES_RETENUES = 'offres_retenues';
    const DIR_DECISIONS = 'decisions';
    const DIR_DOCUMENTS_DECISION = 'documents_decision';
    const DIR_ADMISSIBILITE = 'admissibilite';
    const DIR_LETTRE_ADMISSION = 'lettre_admission';
    const DIR_DEMANDES_COMPLEMENTS_CANDIDATURES = 'demandes_complements_candidatures';
    const DIR_DEMANDES_COMPLEMENTS_OFFRES = 'demandes_complements_offres';
    const DIR_REJET_CANDIDATURES = 'rejet_candidatures';
    const DIR_REJET_OFFRES = 'rejet_offres';

    /**
     * Permet de retourner les chemin des documents externes.
     *
     * @return array $paths les chemins des documents externes
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public static function getPathsDocExternes()
    {
        $paths = [];
        if (empty(Atexo_Config::getParameter('STRUCTURE_ARCHIVE'))) {
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_PUBLICITES')] = Atexo_Config::getParameter('DIR_DOC_EXTERNE_PUBLICITES').'/Documents_Externes';
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_CONSULTATION')] = Atexo_Config::getParameter('DIR_DOC_EXTERNE_CONSULTATION').'/Documents_Externes';
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_REGISTRES')] = Atexo_Config::getParameter('DIR_DOC_EXTERNE_REGISTRES').'/Documents_Externes';
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_COMMISSION_APPEL_OFFRE')] = Atexo_Config::getParameter('DIR_DOC_EXTERNE_COMMISSION_APPEL_OFFRE').'/Documents_Externes';
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_NOTIFICATION')] = Atexo_Config::getParameter('DIR_DOC_EXTERNE_NOTIFICATION').'/Documents_Externes';
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_CANDIDATURE_NON_RET')] = Atexo_Config::getParameter('DIR_DOC_EXTERNE_CANDIDATURE_NON_RET').'/Documents_Externes';
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_CANDIDATURES_RET')] = Atexo_Config::getParameter('DIR_DOC_EXTERNE_CANDIDATURES_RET').'/Documents_Externes';
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_MESSAGE_ECHANGE')] = Atexo_Config::getParameter('DIR_DOC_EXTERNE_MESSAGE_ECHANGE').'/Documents_Externes';
            if ((new Atexo_Module())->isChorusAccessActivate(Atexo_CurrentUser::getIdServiceAgentConnected())) {
                $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_CHORUS_ECHANGE')] = Atexo_Config::getParameter('DIR_DOC_EXTERNE_CHORUS_ECHANGE').'/Documents_Externes';
            }
        } elseif ('MEGALIS' == Atexo_Config::getParameter('STRUCTURE_ARCHIVE')) {
            $arrayPaths = self::getPathDocumentsExternes();
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_CONSULTATION')] = $arrayPaths[self::DIR_DOSSIER_CONSULTATION].DIRECTORY_SEPARATOR.self::DIR_DOCUMENTS_EXTERNES;
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_PUBLICITES')] = $arrayPaths[self::DIR_PUBLICITES].DIRECTORY_SEPARATOR.self::DIR_DOCUMENTS_EXTERNES;
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_MESSAGE_ECHANGE')] = $arrayPaths[self::DIR_MESSAGES_ECHANGES].DIRECTORY_SEPARATOR.self::DIR_DOCUMENTS_EXTERNES;
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_REGISTRES')] = $arrayPaths[self::DIR_REGISTRES].DIRECTORY_SEPARATOR.self::DIR_DOCUMENTS_EXTERNES;
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_CANDIDATURE_NON_RET')] = $arrayPaths[self::DIR_CANDIDATURES_NON_RETENUES].DIRECTORY_SEPARATOR.self::DIR_DOCUMENTS_EXTERNES;
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_CANDIDATURES_RET')] = $arrayPaths[self::DIR_CANDIDATURES_RETENUES].DIRECTORY_SEPARATOR.self::DIR_DOCUMENTS_EXTERNES;
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_OUVERTURE_PLIS_ANALYSE')] = $arrayPaths[self::DIR_OUVERTURE_PLIS_ANALYSE].DIRECTORY_SEPARATOR.self::DIR_DOCUMENTS_EXTERNES;
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_OFFRES_NON_RETENUES')] = $arrayPaths[self::DIR_OFFRES_NON_RETENUES].DIRECTORY_SEPARATOR.self::DIR_DOCUMENTS_EXTERNES;
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_OFFRES_RETENUES')] = $arrayPaths[self::DIR_OFFRES_RETENUES].DIRECTORY_SEPARATOR.self::DIR_DOCUMENTS_EXTERNES;
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_DOCUMENTS_DECISION')] = $arrayPaths[self::DIR_DOCUMENTS_DECISION].DIRECTORY_SEPARATOR.self::DIR_DOCUMENTS_EXTERNES;
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_DEMANDES_COMPLEMENTS_CANDIDATURES')] = $arrayPaths[self::DIR_DEMANDES_COMPLEMENTS_CANDIDATURES].DIRECTORY_SEPARATOR.self::DIR_DOCUMENTS_EXTERNES;
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_REJET_CANDIDATURES')] = $arrayPaths[self::DIR_REJET_CANDIDATURES].DIRECTORY_SEPARATOR.self::DIR_DOCUMENTS_EXTERNES;
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_LETTRE_ADMISSION')] = $arrayPaths[self::DIR_LETTRE_ADMISSION].DIRECTORY_SEPARATOR.self::DIR_DOCUMENTS_EXTERNES;
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_DEMANDES_COMPLEMENTS_OFFRES')] = $arrayPaths[self::DIR_DEMANDES_COMPLEMENTS_OFFRES].DIRECTORY_SEPARATOR.self::DIR_DOCUMENTS_EXTERNES;
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_REJET_OFFRES')] = $arrayPaths[self::DIR_REJET_OFFRES].DIRECTORY_SEPARATOR.self::DIR_DOCUMENTS_EXTERNES;

            //@TODO documents externes: repertoires 'DIR_DECISIONS'
        }

        return $paths;
    }

    /**
     * Permet de construire un tableau de chemins de documents externes pour les differents repertoires.
     *
     * @return array
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-megalis
     *
     * @copyright Atexo 2016
     */
    public static function getPathDocumentsExternes()
    {
        $arrayKey = [];
        $arrayKey[self::DIR_DOSSIER_CONSULTATION] = self::DIR_CONSULTATION.DIRECTORY_SEPARATOR.self::DIR_DOSSIER_CONSULTATION;
        $arrayKey[self::DIR_PUBLICITES] = self::DIR_CONSULTATION.DIRECTORY_SEPARATOR.self::DIR_PUBLICITES;
        $arrayKey[self::DIR_MESSAGES_ECHANGES] = self::DIR_CONSULTATION.DIRECTORY_SEPARATOR.self::DIR_MESSAGES_ECHANGES;
        $arrayKey[self::DIR_REGISTRES] = self::DIR_CONSULTATION.DIRECTORY_SEPARATOR.self::DIR_REGISTRES;
        $arrayKey[self::DIR_CANDIDATURES_NON_RETENUES] = self::DIR_CANDIDATURES_OFFRES_DECISION.DIRECTORY_SEPARATOR.self::DIR_CANDIDATURES.DIRECTORY_SEPARATOR.self::DIR_CANDIDATURES_NON_RETENUES;
        $arrayKey[self::DIR_CANDIDATURES_RETENUES] = self::DIR_CANDIDATURES_OFFRES_DECISION.DIRECTORY_SEPARATOR.self::DIR_CANDIDATURES.DIRECTORY_SEPARATOR.self::DIR_CANDIDATURES_RETENUES;
        $arrayKey[self::DIR_OUVERTURE_PLIS_ANALYSE] = self::DIR_CANDIDATURES_OFFRES_DECISION.DIRECTORY_SEPARATOR.self::DIR_REFERENCE_CONSULTATION_LOT.DIRECTORY_SEPARATOR.self::DIR_RECEPTION_EXAMEN_OFFRES.DIRECTORY_SEPARATOR.self::DIR_OUVERTURE_PLIS_ANALYSE;
        $arrayKey[self::DIR_OFFRES_NON_RETENUES] = self::DIR_CANDIDATURES_OFFRES_DECISION.DIRECTORY_SEPARATOR.self::DIR_REFERENCE_CONSULTATION_LOT.DIRECTORY_SEPARATOR.self::DIR_RECEPTION_EXAMEN_OFFRES.DIRECTORY_SEPARATOR.self::DIR_OFFRES_NON_RETENUES;
        $arrayKey[self::DIR_OFFRES_RETENUES] = self::DIR_CANDIDATURES_OFFRES_DECISION.DIRECTORY_SEPARATOR.self::DIR_REFERENCE_CONSULTATION_LOT.DIRECTORY_SEPARATOR.self::DIR_RECEPTION_EXAMEN_OFFRES.DIRECTORY_SEPARATOR.self::DIR_OFFRES_RETENUES;
        $arrayKey[self::DIR_DOCUMENTS_DECISION] = self::DIR_CANDIDATURES_OFFRES_DECISION.DIRECTORY_SEPARATOR.self::DIR_DOCUMENTS_DECISION;
        $arrayKey[self::DIR_DEMANDES_COMPLEMENTS_CANDIDATURES] = self::DIR_CANDIDATURES_OFFRES_DECISION.DIRECTORY_SEPARATOR.self::DIR_CANDIDATURES.DIRECTORY_SEPARATOR.self::DIR_ADMISSIBILITE.DIRECTORY_SEPARATOR.self::DIR_DEMANDES_COMPLEMENTS_CANDIDATURES;
        $arrayKey[self::DIR_REJET_CANDIDATURES] = self::DIR_CANDIDATURES_OFFRES_DECISION.DIRECTORY_SEPARATOR.self::DIR_CANDIDATURES.DIRECTORY_SEPARATOR.self::DIR_ADMISSIBILITE.DIRECTORY_SEPARATOR.self::DIR_REJET_CANDIDATURES;
        $arrayKey[self::DIR_LETTRE_ADMISSION] = self::DIR_CANDIDATURES_OFFRES_DECISION.DIRECTORY_SEPARATOR.self::DIR_CANDIDATURES.DIRECTORY_SEPARATOR.self::DIR_ADMISSIBILITE.DIRECTORY_SEPARATOR.self::DIR_LETTRE_ADMISSION;
        $arrayKey[self::DIR_DEMANDES_COMPLEMENTS_OFFRES] = self::DIR_CANDIDATURES_OFFRES_DECISION.DIRECTORY_SEPARATOR.self::DIR_REFERENCE_CONSULTATION_LOT.DIRECTORY_SEPARATOR.self::DIR_DECISIONS.DIRECTORY_SEPARATOR.self::DIR_DEMANDES_COMPLEMENTS_OFFRES;
        $arrayKey[self::DIR_REJET_OFFRES] = self::DIR_CANDIDATURES_OFFRES_DECISION.DIRECTORY_SEPARATOR.self::DIR_REFERENCE_CONSULTATION_LOT.DIRECTORY_SEPARATOR.self::DIR_DECISIONS.DIRECTORY_SEPARATOR.self::DIR_REJET_OFFRES;

        return $arrayKey;
    }

    /**
     * Permet de retourner les chemins des documents.
     *
     * @return array $paths les chemin des documents
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public static function getPathsDoc()
    {
        $paths = [];
        $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_PUBLICITES')] = Atexo_Config::getParameter('DIR_DOC_EXTERNE_PUBLICITES');
        $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_CONSULTATION')] = Atexo_Config::getParameter('DIR_DOC_EXTERNE_CONSULTATION');
        $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_REGISTRES')] = Atexo_Config::getParameter('DIR_DOC_EXTERNE_REGISTRES');
        $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_COMMISSION_APPEL_OFFRE')] = Atexo_Config::getParameter('DIR_DOC_EXTERNE_COMMISSION_APPEL_OFFRE');
        $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_NOTIFICATION')] = Atexo_Config::getParameter('DIR_DOC_EXTERNE_NOTIFICATION');
        $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_CANDIDATURE_NON_RET')] = Atexo_Config::getParameter('DIR_DOC_EXTERNE_CANDIDATURE_NON_RET');
        $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_CANDIDATURES_RET')] = Atexo_Config::getParameter('DIR_DOC_EXTERNE_CANDIDATURES_RET');
        $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_MESSAGE_ECHANGE')] = Atexo_Config::getParameter('DIR_DOC_EXTERNE_MESSAGE_ECHANGE');
        if ((new Atexo_Module())->isChorusAccessActivate(Atexo_CurrentUser::getIdServiceAgentConnected())) {
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_CHORUS_ECHANGE')] = Atexo_Config::getParameter('DIR_DOC_EXTERNE_CHORUS_ECHANGE');
        }

        return $paths;
    }

    /**
     * Permet de retourner les chemins des documents à partir de la description.
     *
     * @param bool $docExt si true on retourne les chemin des documents externes si non les chemin des 'autre documents
     *
     * @return array $paths les chemin des documents
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public static function getDescriptionDoc($docExt = false)
    {
        //Récuperer les éléments de la config spécifique
        $arrayDescriptionDoc = [];
        if ($docExt) {
            $arrayDescriptionDoc = self::getArrayArchiveConfigTree('DescriptionDocExt');
        } else {
            $arrayDescriptionDoc = self::getArrayArchiveConfigTree('DescriptionDoc');
        }

        $paths = [];
        if ($arrayDescriptionDoc['TYPE_DOC_EXTERNE_PUBLICITES']) {
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_PUBLICITES')] = $arrayDescriptionDoc['TYPE_DOC_EXTERNE_PUBLICITES'];
        } else {
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_PUBLICITES')] = 'Documents relatifs à la publicité';
        }
        if ($arrayDescriptionDoc['TYPE_DOC_EXTERNE_CONSULTATION']) {
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_CONSULTATION')] = $arrayDescriptionDoc['TYPE_DOC_EXTERNE_CONSULTATION'];
        } else {
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_CONSULTATION')] = 'Documents relatifs au Dossier de Consultation des Entreprises';
        }
        if ($arrayDescriptionDoc['TYPE_DOC_EXTERNE_REGISTRES']) {
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_REGISTRES')] = $arrayDescriptionDoc['TYPE_DOC_EXTERNE_REGISTRES'];
        } else {
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_REGISTRES')] = 'Documents relatifs aux registres de dépots, de retraits et de questions';
        }
        if ($arrayDescriptionDoc['TYPE_DOC_EXTERNE_COMMISSION_APPEL_OFFRE']) {
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_COMMISSION_APPEL_OFFRE')] = $arrayDescriptionDoc['TYPE_DOC_EXTERNE_COMMISSION_APPEL_OFFRE'];
        } else {
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_COMMISSION_APPEL_OFFRE')] = "Documents relatifs à la commission d'Appel d'Offres";
        }
        if ($arrayDescriptionDoc['TYPE_DOC_EXTERNE_NOTIFICATION']) {
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_NOTIFICATION')] = $arrayDescriptionDoc['TYPE_DOC_EXTERNE_NOTIFICATION'];
        } else {
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_NOTIFICATION')] = "Documents relatifs à la Notification du marché (Acte d'engagement, accusé de réception)";
        }
        if ($arrayDescriptionDoc['TYPE_DOC_EXTERNE_CANDIDATURE_NON_RET']) {
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_CANDIDATURE_NON_RET')] = $arrayDescriptionDoc['TYPE_DOC_EXTERNE_CANDIDATURE_NON_RET'];
        } else {
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_CANDIDATURE_NON_RET')] = 'Document relatifs aux candidatures et aux offres des entreprises non retenues';
        }
        if ($arrayDescriptionDoc['TYPE_DOC_EXTERNE_CANDIDATURES_RET']) {
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_CANDIDATURES_RET')] = $arrayDescriptionDoc['TYPE_DOC_EXTERNE_CANDIDATURES_RET'];
        } else {
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_CANDIDATURES_RET')] = 'Document relatifs aux candidatures et aux offres des entreprises retenues';
        }
        if ($arrayDescriptionDoc['TYPE_DOC_EXTERNE_MESSAGE_ECHANGE']) {
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_MESSAGE_ECHANGE')] = $arrayDescriptionDoc['TYPE_DOC_EXTERNE_MESSAGE_ECHANGE'];
        } else {
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_MESSAGE_ECHANGE')] = '';
        }
        if ((new Atexo_Module())->isChorusAccessActivate(Atexo_CurrentUser::getIdServiceAgentConnected())) {
            if ($arrayDescriptionDoc['TYPE_DOC_EXTERNE_CHORUS_ECHANGE']) {
                $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_CHORUS_ECHANGE')] = $arrayDescriptionDoc['TYPE_DOC_EXTERNE_CHORUS_ECHANGE'];
            } else {
                $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_CHORUS_ECHANGE')] = '';
            }
        }

        return $paths;
    }

    /**
     * Permet de retourner les chemins des documents à partir de la configuration AccessDoc.
     *
     * @return array $paths les chemin des documents
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public static function getAccessDoc()
    {
        //Récuperer les éléments de la config spécifique
        $arrayDescriptionDoc = self::getArrayArchiveConfigTree('AccessDoc');
        $paths = [];
        if ($arrayDescriptionDoc['TYPE_DOC_EXTERNE_PUBLICITES']) {
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_PUBLICITES')] = $arrayDescriptionDoc['TYPE_DOC_EXTERNE_PUBLICITES'];
        } else {
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_PUBLICITES')] = Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_1');
        }
        if ($arrayDescriptionDoc['TYPE_DOC_EXTERNE_CONSULTATION']) {
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_CONSULTATION')] = $arrayDescriptionDoc['TYPE_DOC_EXTERNE_CONSULTATION'];
        } else {
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_CONSULTATION')] = Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_1');
        }
        if ($arrayDescriptionDoc['TYPE_DOC_EXTERNE_REGISTRES']) {
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_REGISTRES')] = $arrayDescriptionDoc['TYPE_DOC_EXTERNE_REGISTRES'];
        } else {
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_REGISTRES')] = Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_2');
        }
        if ($arrayDescriptionDoc['TYPE_DOC_EXTERNE_COMMISSION_APPEL_OFFRE']) {
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_COMMISSION_APPEL_OFFRE')] = $arrayDescriptionDoc['TYPE_DOC_EXTERNE_COMMISSION_APPEL_OFFRE'];
        } else {
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_COMMISSION_APPEL_OFFRE')] = Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_2');
        }
        if ($arrayDescriptionDoc['TYPE_DOC_EXTERNE_NOTIFICATION']) {
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_NOTIFICATION')] = $arrayDescriptionDoc['TYPE_DOC_EXTERNE_NOTIFICATION'];
        } else {
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_NOTIFICATION')] = Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_2');
        }
        if ($arrayDescriptionDoc['TYPE_DOC_EXTERNE_CANDIDATURE_NON_RET']) {
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_CANDIDATURE_NON_RET')] = $arrayDescriptionDoc['TYPE_DOC_EXTERNE_CANDIDATURE_NON_RET'];
        } else {
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_CANDIDATURE_NON_RET')] = Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_2');
        }
        if ($arrayDescriptionDoc['TYPE_DOC_EXTERNE_CANDIDATURES_RET']) {
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_CANDIDATURES_RET')] = $arrayDescriptionDoc['TYPE_DOC_EXTERNE_CANDIDATURES_RET'];
        } else {
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_CANDIDATURES_RET')] = Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_2');
        }
        if ($arrayDescriptionDoc['TYPE_DOC_EXTERNE_MESSAGE_ECHANGE']) {
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_MESSAGE_ECHANGE')] = $arrayDescriptionDoc['TYPE_DOC_EXTERNE_MESSAGE_ECHANGE'];
        } else {
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_MESSAGE_ECHANGE')] = Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_2');
        }
        if ((new Atexo_Module())->isChorusAccessActivate(Atexo_CurrentUser::getIdServiceAgentConnected())) {
            if ($arrayDescriptionDoc['TYPE_DOC_EXTERNE_CHORUS_ECHANGE']) {
                $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_CHORUS_ECHANGE')] = $arrayDescriptionDoc['TYPE_DOC_EXTERNE_CHORUS_ECHANGE'];
            } else {
                $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_CHORUS_ECHANGE')] = Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_2');
            }
        }

        return $paths;
    }

    /**
     * Permet de retourner les chemins des documents à partir de la configuration AppraisalDoc.
     *
     * @return array $paths les chemin des documents
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public static function getAppraisalDoc()
    {
        //Récuperer les éléments de la config spécifique
        $arrayDescriptionDoc = self::getArrayArchiveConfigTree('AppraisalDoc');
        $paths = [];
        if ($arrayDescriptionDoc['TYPE_DOC_EXTERNE_PUBLICITES']) {
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_PUBLICITES')] = $arrayDescriptionDoc['TYPE_DOC_EXTERNE_PUBLICITES'];
        } else {
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_PUBLICITES')] = 'detruire';
        }
        if ($arrayDescriptionDoc['TYPE_DOC_EXTERNE_CONSULTATION']) {
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_CONSULTATION')] = $arrayDescriptionDoc['TYPE_DOC_EXTERNE_CONSULTATION'];
        } else {
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_CONSULTATION')] = 'detruire';
        }
        if ($arrayDescriptionDoc['TYPE_DOC_EXTERNE_REGISTRES']) {
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_REGISTRES')] = $arrayDescriptionDoc['TYPE_DOC_EXTERNE_REGISTRES'];
        } else {
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_REGISTRES')] = 'detruire';
        }
        if ($arrayDescriptionDoc['TYPE_DOC_EXTERNE_COMMISSION_APPEL_OFFRE']) {
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_COMMISSION_APPEL_OFFRE')] = $arrayDescriptionDoc['TYPE_DOC_EXTERNE_COMMISSION_APPEL_OFFRE'];
        } else {
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_COMMISSION_APPEL_OFFRE')] = 'detruire';
        }
        if ($arrayDescriptionDoc['TYPE_DOC_EXTERNE_NOTIFICATION']) {
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_NOTIFICATION')] = $arrayDescriptionDoc['TYPE_DOC_EXTERNE_NOTIFICATION'];
        } else {
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_NOTIFICATION')] = 'detruire';
        }
        if ($arrayDescriptionDoc['TYPE_DOC_EXTERNE_CANDIDATURE_NON_RET']) {
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_CANDIDATURE_NON_RET')] = $arrayDescriptionDoc['TYPE_DOC_EXTERNE_CANDIDATURE_NON_RET'];
        } else {
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_CANDIDATURE_NON_RET')] = 'detruire';
        }
        if ($arrayDescriptionDoc['TYPE_DOC_EXTERNE_CANDIDATURES_RET']) {
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_CANDIDATURES_RET')] = $arrayDescriptionDoc['TYPE_DOC_EXTERNE_CANDIDATURES_RET'];
        } else {
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_CANDIDATURES_RET')] = 'conserver';
        }
        if ($arrayDescriptionDoc['TYPE_DOC_EXTERNE_MESSAGE_ECHANGE']) {
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_MESSAGE_ECHANGE')] = $arrayDescriptionDoc['TYPE_DOC_EXTERNE_MESSAGE_ECHANGE'];
        } else {
            $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_MESSAGE_ECHANGE')] = 'detruire';
        }
        if ((new Atexo_Module())->isChorusAccessActivate(Atexo_CurrentUser::getIdServiceAgentConnected())) {
            if ($arrayDescriptionDoc['TYPE_DOC_EXTERNE_CHORUS_ECHANGE']) {
                $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_CHORUS_ECHANGE')] = $arrayDescriptionDoc['TYPE_DOC_EXTERNE_CHORUS_ECHANGE'];
            } else {
                $paths[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_CHORUS_ECHANGE')] = 'detruire';
            }
        }

        return $paths;
    }

    /**
     * Permet d'executer une commande systeme.
     *
     * @param string $commande la commande
     *
     * @return string le rsultat de l'execution de la commande
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function executeSystemeCommand($commande)
    {
        system($commande.' > '.Atexo_Config::getParameter('COMMON_TMP').'zip_error 2>&1', $sys_answer);
//        echo "<br>" . $commande . " > ".Atexo_Config::getParameter('COMMON_TMP')."zip_error";
//        echo "<br>" . $sys_answer . "<br>";

        return $sys_answer;
    }
	 /**
	 * genere un zip contenant les fichier de l'arborescence donnée en parametre ($tree)
	 *
	 * @param Atexo_Consultation_Tree_Arborescence $tree l'arborescence auquelle on va generer le zip
	 * @param string $tmpZipArbo le repertoire ou stoquer temporairement le zip
	 * @param CommonConsultation $consultation la reference de la Consultation
	 * @param string $organisme l'accronyme de l'organisme
	 * @param string $xmlFileName le nom du fichier Xml
	 * @param boolean $useLangageCatalogue si on utilise le language du catalogue ou pas(sinn on utlise la langue passée en param 'lang' de l'url
	 * @param int $numLot numero du lot
	 * @return string url de telechargement du DIC selon le parametrage
	 * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
	 * @version 1.0
	 * @since 3.0.0
	 * @copyright Atexo 2014
	 */
     public function generateZipArborescence(Atexo_Consultation_Tree_Arborescence $tree, $tmpZipArbo, CommonConsultation $consultation, $organisme, $xmlFileName="archive.xml",
     										$useLangageCatalogue=true,$numLot = false,$connexion = null,$fromArchiver = false)
     {
         try {
             if($useLangageCatalogue) {
                 Atexo_Languages::setLanguageCatalogue("agent");
             }

         $zipDirectoryWithoutPath = "tmpZip" . $numLot . session_id() . time() . uniqid();
         $zipDirectory = $tmpZipArbo . $zipDirectoryWithoutPath;

         self::executeSystemeCommand("mkdir --mode=a+rwx " . escapeshellarg($zipDirectory) );
         self::executeSystemeCommand("mkdir --mode=a+rwx " . escapeshellarg($zipDirectory . "/" . $tree->getNom()));

             foreach ($tree->getFils() as $directory) {
                 self::addItem($tree->getNom(), $zipDirectory, $directory, $organisme);
             }

             // Inculsion de l'XML dans le zip :
             $sys_answer = self::getDirectorySize($zipDirectory);
             $archiveXml = new Atexo_Consultation_ArchiveXml($tree, $consultation, $organisme, $sys_answer['size']);
             if(empty(Atexo_Config::getParameter('STRUCTURE_ARCHIVE'))) {
                 $tmpPathXmlFile = $archiveXml->generateXml(false, $numLot);
             } else {
                 $tmpPathXmlFile = $archiveXml->generateXmlStructureArchive( $consultation->getId(), $numLot);
             }
             $fileXmlArchive = Atexo_Config::getParameter("COMMON_TMP") . "archive_xml_" . session_id() . time();
             if($fromArchiver && copy($tmpPathXmlFile, $fileXmlArchive)){
                 self::saveArchiveXml($fileXmlArchive,$xmlFileName,$organisme,$consultation->getId(),$numLot,$connexion);
             }
             if(empty(Atexo_Config::getParameter('STRUCTURE_ARCHIVE'))) {
                 self::executeSystemeCommand("mv " . escapeshellarg($tmpPathXmlFile) . " " . escapeshellarg($zipDirectory . "/" . $xmlFileName));
                 $tmpPathXslFile =  Atexo_Config::getParameter('FILE_PATH_ARCHIVE_XSL');
                 $tmpPathStyleFile =   Atexo_Config::getParameter('FILE_PATH_ARCHIVE_STYLE');

                 self::executeSystemeCommand("cp " . escapeshellarg($tmpPathXslFile) . " " . escapeshellarg($zipDirectory . "/archive.xsl"));
                 self::executeSystemeCommand("cp -R " . escapeshellarg($tmpPathStyleFile) . " " . escapeshellarg($zipDirectory . "/"));
             }

             chdir($zipDirectory);
             self::executeSystemeCommand("nice zip -r "  . escapeshellarg( "../" . $zipDirectoryWithoutPath . ".zip") . " ./");

             chdir($tmpZipArbo);
             self::executeSystemeCommand("rm -rf " . escapeshellarg($zipDirectoryWithoutPath . "/" ));



             return $zipDirectory . ".zip";

         }catch (\Exception $e) {
            throw $e;
         }
     }
     
	/**
     * Permet d'ajouter un element à l'archive
     * 
     * @param String $pathParent le chemin pére ou on va ajouter le fichier
     * @param String $zipDirectory le chemin du zip du dossier de l'archive
     * @param Atexo_Consultation_Tree_Item $item l'element à ajouter
     * @param String $organisme l'organisme
     * @return void
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function addItem($pathParent, $zipDirectory, Atexo_Consultation_Tree_Item $item, $organisme)
    {
        if ($item instanceof Atexo_Consultation_Tree_Directory) {
            $currentDir = $pathParent.'/'.$item->getNom();

            self::executeSystemeCommand('mkdir --mode=a+rwx '.escapeshellarg($zipDirectory.'/'.$currentDir));
            foreach ($item->getFils() as $fils) {
                self::addItem($currentDir, $zipDirectory, $fils, $organisme);
            }
        } elseif ($item instanceof Atexo_Consultation_Tree_Group) {
            foreach ($item->getFils() as $fils) {
                self::addItem($pathParent, $zipDirectory, $fils, $organisme);
            }
        } elseif ($item instanceof Atexo_Consultation_Tree_File) {
            if (is_numeric($item->getIdBlob())) {
                if ($item->getFromCommon()) {
                    $dbConnexion = Atexo_Config::getParameter('COMMON_BLOB');
                } else {
                    $dbConnexion = $organisme;
                }
                $atexoBlob = new Atexo_Blob();
                $fileToAdd = $pathParent.'/'.$item->getNom();
                $atexoBlob->copyBlob($item->getIdBlob(), $zipDirectory.'/'.$fileToAdd, $dbConnexion);
            } else {
                // C'est un fichier généré dynamiquement comme les registres
                $fileToAdd = $pathParent.'/'.$item->getNom();
                self::executeSystemeCommand('cp '.escapeshellarg($item->getIdBlob()).' '.escapeshellarg($zipDirectory.'/'.$fileToAdd));
                // On supprime les fichiers sur le TMP
                if (str_contains($item->getIdBlob(), Atexo_Config::getParameter('COMMON_TMP'))) {
                    @unlink($item->getIdBlob());
                }
            }
        } elseif ($item instanceof Atexo_Consultation_Tree_TimeStamp) {
            if ('0' != $item->getHorodatage()) {
                $fileToAdd = $pathParent.'/'.$item->getNom();
                $res = $item->generateFile($fileToAdd, $zipDirectory);
            }
        } elseif ($item instanceof Atexo_Consultation_Tree_Signature) {
            if ($item->getSignature()) {
                $fileToAdd = $pathParent.'/'.$item->getNom();
                $res = $item->generateFile($fileToAdd, $zipDirectory);
            }
        } elseif ($item instanceof Atexo_Consultation_Tree_String) {
            if ($item->getString()) {
                $fileToAdd = $pathParent.'/'.$item->getNom();
                $res = $item->generateFile($fileToAdd, $zipDirectory);
            }
        }
    }

    /**
     * Permet d'envoyer un zip.
     *
     * @param string $nomZip  le nom du dossier zip
     * @param string $pathZip le chemmin du dossier zip
     *
     * @return void
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function send($nomZip, $pathZip)
    {
        header('Pragma: public');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Cache-Control: private', false);
        header('Content-Type: application/octet-stream');
        header('Content-Disposition: attachment; filename="'.$nomZip.'";');
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: '.@filesize($pathZip));
        $fp = @fopen($pathZip, 'r');
        $block_count = floor(@filesize($pathZip) / Atexo_Config::getParameter('BLOB_BLOCK_SIZE'));
        $bytes_remaining = @filesize($pathZip) - ($block_count * Atexo_Config::getParameter('BLOB_BLOCK_SIZE'));
        for ($i = 0; $i < $block_count; ++$i) {
            echo @fread($fp, 8192);
            flush();
            ob_flush();
        }
        echo @fread($fp, $bytes_remaining);
        @fclose($fp);
        flush();
        system('rm -f '.escapeshellarg($pathZip).' > '.escapeshellarg(Atexo_Config::getParameter('COMMON_TMP').'zip_error').' 2>&1');
        exit;
    }

    /**
     * Permet de retourner la taille d'un dossier.
     *
     * @param string $path le chemmin du dossier
     *
     * @return array tableau contient les information sur la taille du dossier
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function getDirectorySize($path)
    {
        $total = [];
        $totalsize = 0;
        $totalcount = 0;
        $dircount = 0;
        if ($handle = opendir($path)) {
            while (false !== ($file = readdir($handle))) {
                $nextpath = $path.'/'.$file;
                if ('.' != $file && '..' != $file && !is_link($nextpath)) {
                    if (is_dir($nextpath)) {
                        ++$dircount;
                        $result = self::getDirectorySize($nextpath);
                        $totalsize += $result['size'];
                        $totalcount += $result['count'];
                        $dircount += $result['dircount'];
                    } elseif (is_file($nextpath)) {
                        $totalsize += filesize($nextpath);
                        ++$totalcount;
                    }
                }
            }
        }
        closedir($handle);
        $total['size'] = $totalsize;
        $total['count'] = $totalcount;
        $total['dircount'] = $dircount;

        return $total;
    }

    /**
     * Permet la description de l'archive selon une configuration.
     *
     * @param string $propriete la propiete
     *
     * @return array tableau contient les information sur l'archivage
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public static function getArrayArchiveConfigTree($propriete = null)
    {
        $arrayArchive = Atexo_Consultation_ArchiveXml::getArchiveSpecificConfig();
        if (is_array($arrayArchive) && count($arrayArchive)) {
            return ($propriete) ? $arrayArchive['Tree'][$propriete] : $arrayArchive['Tree'];
        }
    }

    /**
     * Retourne de type de dossier auquel on fait reference (publicité, notification, registres, ...).
     *
     * @param string $dossier le dossier
     *
     * @return string le type de dossier
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function getType($dossier)
    {
        $type = '';
        if ($dossier == Atexo_Config::getParameter('DIR_DOC_EXTERNE_PUBLICITES')) {
            $type = 'TYPE_DOC_EXTERNE_PUBLICITES';
        } elseif ($dossier == Atexo_Config::getParameter('DIR_DOC_EXTERNE_CONSULTATION')) {
            $type = 'TYPE_DOC_EXTERNE_CONSULTATION';
        } elseif ($dossier == Atexo_Config::getParameter('DIR_DOC_EXTERNE_REGISTRES')) {
            $type = 'TYPE_DOC_EXTERNE_REGISTRES';
        } elseif ($dossier == Atexo_Config::getParameter('DIR_DOC_EXTERNE_COMMISSION_APPEL_OFFRE')) {
            $type = 'TYPE_DOC_EXTERNE_COMMISSION_APPEL_OFFRE';
        } elseif ($dossier == Atexo_Config::getParameter('DIR_DOC_EXTERNE_NOTIFICATION')) {
            $type = 'TYPE_DOC_EXTERNE_NOTIFICATION';
        } elseif ($dossier == Atexo_Config::getParameter('DIR_DOC_EXTERNE_CANDIDATURE_NON_RET')) {
            $type = 'TYPE_DOC_EXTERNE_CANDIDATURE_NON_RET';
        } elseif ($dossier == Atexo_Config::getParameter('DIR_DOC_EXTERNE_CANDIDATURES_RET')) {
            $type = 'TYPE_DOC_EXTERNE_CANDIDATURES_RET';
        } elseif ($dossier == Atexo_Config::getParameter('DIR_DOC_EXTERNE_MESSAGE_ECHANGE')) {
            $type = 'TYPE_DOC_EXTERNE_MESSAGE_ECHANGE';
        } elseif ($dossier == Atexo_Config::getParameter('DIR_DOC_EXTERNE_CHORUS_ECHANGE')) {
            $type = 'TYPE_DOC_EXTERNE_CHORUS_ECHANGE';
        }

        return $type;
    }

    /**
     * permet de supprimer des telechargements (CommonTTelechargementAsynchrone).
     *
     * @param $archivesToDelete array tableau des telechargements a supprimer
     * @param string $connexion PropelPDO
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function deleteArchive(&$telechargementSupprime, $connexion)
    {
        $FileArchiveDir = Atexo_Config::getParameter('FICHIERS_ARCHIVE_ASYNCHRONE_DIR').$telechargementSupprime->getOrganismeAgent();
        $nameFileDisq = $telechargementSupprime->getId();
        $pathFile = $FileArchiveDir.'/'.$nameFileDisq;
        $fileDeleted = Atexo_Util::deleteLocalfile($pathFile);
        if (!$fileDeleted) {
            Prado::log(' Erreur lors de la suppresion du Fichier '.$pathFile, TLogger::ERROR, 'AtexoTelechargement.php');
        }
        $telechargementSupprime->setTagFichierSupprime(1);
        $telechargementSupprime->save($connexion);
    }

    /**
     * Permet d'ajouter les informations de l'archive pour le ws.
     *
     * @param $id
     * @param $xmlResponse
     * @param $arrayBlob
     * @param $logger
     * @param $contentType
     *
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function setInfoArchiveForWs($id, $organisme, &$xmlResponse, &$arrayBlob, &$logger, &$contentType, $typeChecksum = 'md5'): bool|string
    {
        if ($id && $organisme) {
            $logger->info("La récupération de l'archive de la consultation dont la référence est  ".$id." et l'organisme ".$organisme);
            $consultation = (new Atexo_Consultation())->getConsultationByReference($id, $organisme);
            if ($consultation instanceof CommonConsultation) {
                $ref = $consultation->getReference() ?: $consultation->getId();
                $logger->info('La consultation existe');
                $archive = (new Atexo_Rest_Action_Actions())->getArchive($consultation, $logger);
                if (is_array($archive) && count($archive)) {
                    $arrayBlob = [$archive];
                    $logger->info("Mettre les informations de l'archive dans l'objet Atexo_Rest_ObjectField_DocumentField");
                    $documentField = (new Atexo_Document())->getDocumentFieldByInfo($ref, $archive['name'], $organisme, null, $archive['content'], 'AUTRE_PIECE', null, $archive['pathFile'], null, null, null, null, $typeChecksum);
                    $contentType = Atexo_Util::getContentTypeByExtension(Atexo_Util::getExtension($archive['name']));
                    $logger->info("La récupération de l'xml contenant les informations de l'archive ");
                    $xmlResponse = (new Atexo_Rest_Action_Actions())->getXmlInfosObjectField($documentField);

                    return false;
                } else {
                    return "L'archive de la consultation dont la référence est  ".$ref." et l'organisme ".$organisme." n'existe pas";
                }
            } else {
                return 'La consultation dont la référence est  '.$id." et l'organisme ".$organisme." n'existe pas";
            }
        } else {
            return "Les paremetres d'appel au ws sont invalide";
        }
    }

    /**
     * Suppression des fichier temporaire à partir de la liste de fichier générée par generateZipArborescence.
     *
     * @param $listeNomFichier
     * @param $tmpDir
     */
    public function removeTempFileAfterDownload($listeNomFichier, $tmpDir)
    {
        foreach ($listeNomFichier as $categorieFichier => $listeFichierCategorie) {
            if (!empty($listeFichierCategorie)) {
                foreach ($listeFichierCategorie as $nomFichier => $numOccurence) {
                    $fileFullyPath = $tmpDir.$nomFichier;
                    if (file_exists($fileFullyPath)) {
                        @unlink($fileFullyPath);
                    }
                }
            }
        }
    }

    /**
     * Permet d'enregistrer le bordereau d'archive.
     *
     * @param $file
     * @param $fileName
     * @param $organisme
     * @param $referenceCons
     * @param null $connexion
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-megalis
     *
     * @copyright Atexo 2016
     */
    public function saveArchiveXml($file, $fileName, $organisme, $consultationId, $numLot, $connexion = null)
    {
        if (!$connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        }
        $document = new CommonTDocument();
        $document->setIdTypeDocument(Atexo_Config::getParameter('TYPE_DOCUMENT_BORDEREAU_ARCHIVE'));
        $document->setNom($fileName);
        $document->setOrganisme($organisme);
        $document->setConsultationId($consultationId);
        $document->setEmpreinte(md5_file($file));
        $document->setTypeEmpreinte(Atexo_Config::getParameter('TYPE_CHECKSUM'));
        if ($numLot) {
            $document->setLot($numLot);
        }
        $atexoBlob = new Atexo_Blob();
        $idBlob = $atexoBlob->insert_blob($fileName, $file, $organisme);
        if ($idBlob) {
            $document->setIdFichier($idBlob);
            $sizeFile = $atexoBlob->getTailFile($idBlob, $organisme);
            $document->setTaille($sizeFile);
            $document->setDateCreation(date('Y-m-d H:i:s'));
            $document->save($connexion);
        } else {
            $logger = Atexo_LoggerManager::getLogger('app');
            $logger->error("Erreur lors de l'enregistrement du bordereaux d'archive : impossible d'enregistrer le fichier  Ref: ".$consultationId.'- Org: '.$organisme."\n");
        }
    }

    /**
     * Permet d'ajouter les informations du bordereau de l'archive pour le ws.
     *
     * @param $id
     * @param $xmlResponse
     * @param $arrayBlob
     * @param $logger
     * @param $contentType
     *
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-megalis
     *
     * @copyright Atexo 2016
     */
    public function setInfoBordereauForWs($id, $organisme, $lot, &$xmlResponse, &$arrayBlob, &$logger, &$contentType): bool|string
    {
        if ($id && $organisme) {
            if (!$lot) {
                $lot = 0;
            }
            $logger->info('La récupération du bordereau de la consultation dont la référence est  '.$id." et l'organisme ".$organisme.' et lot '.$lot);
            $consultation = (new Atexo_Consultation())->getConsultationByReference($id, $organisme);
            if ($consultation instanceof CommonConsultation) {
                $ref = $consultation->getReference() ?: $consultation->getId();
                $logger->info('La consultation existe');
                $criteria = (new Atexo_Document())->getCriteraiDocument($id, $organisme, $lot, Atexo_Config::getParameter('TYPE_DOCUMENT_BORDEREAU_ARCHIVE'));
                $documentQuery = new CommonTDocumentQuery();
                $document = $documentQuery->getDocument($criteria, true);
                if ($document instanceof CommonTDocument) {
                    $logger->info('Le document existe son id est '.$document->getIdDocument());
                    $logger->info('La récupération des informations du blob du fichier');
                    $blobInfo = Atexo_Blob::getBlobById($document->getIdFichier(), $document->getOrganisme(), $document->getNom(), false);
                    $arrayBlob = [$blobInfo];
                    $logger->info("Mettre les informations du bordereau dans l'objet Atexo_Rest_ObjectField_DocumentField");
                    $documentField = (new Atexo_Document())->getDocumentFieldByInfo($document->getIdDocument(), $document->getNom(), $organisme, $document->getIdFichier(), null, 'BORDEREAU_ARCHIVE', $document->getTaille());
                    $contentType = Atexo_Util::getContentTypeByExtension(Atexo_Util::getExtension($document->getNom()));
                    $logger->info("La récupération de l'xml contenant les informations de l'archive ");
                    $xmlResponse = (new Atexo_Rest_Action_Actions())->getXmlInfosObjectField($documentField);

                    return false;
                } else {
                    return 'Le bordereau de la consultation dont la référence est  '.$ref." ,l'organisme ".$organisme.' et lot '.$lot." n'existe pas";
                }
            } else {
                return 'La consultation dont la référence est  '.$id." et l'organisme ".$organisme." n'existe pas";
            }
        } else {
            return "Les paremetres d'appel au ws sont invalide";
        }
    }

    /**
     * Permet de recuperer le bordereau de l'archive pour le ws.
     *
     * @param $id
     * @param $xmlResponse
     * @param $logger
     * @param $contentType
     *
     * @return bool|string un message dans le cas d'erreur
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-megalis
     *
     * @copyright Atexo 2016
     */
    public function setBordereauForWs($id, $organisme, $lot, &$xmlResponse, &$logger, &$contentType): bool|string
    {
        if ($id && $organisme) {
            if (!$lot) {
                $lot = 0;
            }
            $logger->info('La récupération du bordereau de la consultation dont la référence est  '.$id." et l'organisme ".$organisme.' et lot '.$lot);
            $consultation = (new Atexo_Consultation())->getConsultationByReference($id, $organisme);
            if ($consultation instanceof CommonConsultation) {
                $logger->info('La consultation existe');
                $criteria = (new Atexo_Document())->getCriteraiDocument($id, $organisme, $lot, Atexo_Config::getParameter('TYPE_DOCUMENT_BORDEREAU_ARCHIVE'));
                $documentQuery = new CommonTDocumentQuery();
                $document = $documentQuery->getDocument($criteria, true);
                if ($document instanceof CommonTDocument) {
                    $logger->info('Le document existe son id est '.$document->getIdDocument());
                    $logger->info('La récupération du contenu du blob du fichier');
                    $atexoBlob = new Atexo_Blob();
                    $contentBlob = $atexoBlob->return_blob_to_client($document->getIdFichier(), $document->getOrganisme());
                    $contentType = Atexo_Util::getContentTypeByExtension(Atexo_Util::getExtension($document->getNom()));
                    $logger->info('Fin de la récupération du contenu du blob du fichier');
                    if ($contentBlob) {
                        $xmlResponse = $contentBlob;

                        return false;
                    } else {
                        throw new Exception("Le fichier n'existe pas sur le serveur");
                    }
                } else {
                    return 'Le bordereau de la consultation dont la référence est  '.$id." ,l'organisme ".$organisme.' et lot '.$lot." n'existe pas";
                }
            } else {
                return 'La consultation dont la référence est  '.$id." et l'organisme ".$organisme." n'existe pas";
            }
        } else {
            return "Les paremetres d'appel au ws sont invalide";
        }
    }

    /**
     * Cette méthode permet de factoriser le nommage des archives pour chaque lot.
     *
     * @param $nomArchive nom de l'archive racine mère sans le ".zip" à la fin
     * @param $numeroLot l'objet qui correspond au lot
     *
     * @return string le nom de l'archive du lot, retourne $nomArchive.zip si le lot passé en paramètre n'est pas de type CommonLot
     */
    public static function getNomArchiveLot($nomArchive, $numeroLot = 0)
    {
        if ($numeroLot > 0) {
            $nomArchiveLot = $nomArchive.'_Lot_'.$numeroLot.'.zip';
        } else {
            $nomArchiveLot = $nomArchive.'.zip';
        }

        return $nomArchiveLot;
    }
}
