<?php

namespace Application\Service\Atexo\Consultation;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonInvitePermanentContratPeer;
use Application\Service\Atexo\Atexo_Config;

class InvitePermanentContrat
{
    /**
     * @param array $criteres
     *
     *
     * @throws \Application\Library\Propel\Exception\PropelException
     */
    public function retreiveInvitePermanentConsultationTransverse($criteres = []): array|false
    {
        $connexionCommon = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB')
            .Atexo_Config::getParameter('CONST_READ_ONLY'));
        $list = false;
        if (count($criteres) > 0) {
            $c = new Criteria();
            if (array_key_exists('idConsultation', $criteres)) {
                $c->add(CommonInvitePermanentContratPeer::ID_CONSULTATION, $criteres['idConsultation']);
            }
            if (array_key_exists('lot', $criteres)) {
                $c->add(CommonInvitePermanentContratPeer::LOT, $criteres['lot']);
            }
            if (array_key_exists('organisme', $criteres)) {
                $c->add(CommonInvitePermanentContratPeer::ORGANISME, $criteres['organisme']);
            }
            if (array_key_exists('idContratTitulaire', $criteres)) {
                $c->add(CommonInvitePermanentContratPeer::ID_CONTRAT_TITULAIRE, $criteres['idContratTitulaire']);
            }

            $list = CommonInvitePermanentContratPeer::doSelect($c, $connexionCommon);
        }

        return $list;
    }

    public function deleteForCriteres($criteres = [])
    {
        $connexionCommon = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $listInv = (new InvitePermanentContrat())->retreiveInvitePermanentConsultationTransverse($criteres);
        if ($listInv) {
            foreach ($listInv as $oneInv) {
                CommonInvitePermanentContratPeer::doDelete($oneInv->getid(), $connexionCommon);
            }
        }
    }
}
