<?php

namespace Application\Service\Atexo\Consultation;

use Application\Propel\Mpe\CommonOrganisme;
use Application\Propel\Mpe\CommonService;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Organismes;
use Prado\TPropertyValue;

/**
 * Contient le détail d'un invité.
 *
 * @author ZAKI Anas <anas.zaki@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Consultation_GuestVo
{
    private int $_id = 0;
    private int $_permanentGuest = 0;
    private string $_firstName = '';
    private string $_lastName = '';
    private string $_entityPurchase = '';
    private int|string $_typeInvitation = 0;
    private string $_mailAgent = '';
    private int $_alerteReponseElectronique = 0;
    private int $_alerteClotureConsultation = 0;
    private int $_alerteReceptionMessage = 0;
    private int $_alertePublicationBoamp = 0;
    private int $_alerteEchecPublicationBoamp = 0;
    private int $_alerteCreationModificationAgent = 0;
    private int $_actif = 1;
    private int $_alerteChorus = 0;

    /**
     * @return string defaults to ''
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @param string
     */
    public function setId($value)
    {
        $this->_id = TPropertyValue::ensureInteger($value);
    }

    /**
     * @return string defaults to ''
     */
    public function getPermanentGuest()
    {
        return $this->_permanentGuest;
    }

    /**
     * @param int
     */
    public function setPermanentGuest($value)
    {
        $this->_permanentGuest = TPropertyValue::ensureInteger($value);
    }

    /**
     * @return string defaults to ''
     */
    public function getFirstName()
    {
        return $this->_firstName;
    }

    /**
     * @param string
     */
    public function setFirstName($value)
    {
        $this->_firstName = TPropertyValue::ensureString($value);
    }

    /**
     * @return string defaults to ''
     */
    public function getLastName()
    {
        return $this->_lastName;
    }

    /**
     * @param string
     */
    public function setLastName($value)
    {
        $this->_lastName = TPropertyValue::ensureString($value);
    }

    /**
     * @return string defaults to ''
     */
    public function getEntityPurchase()
    {
        return $this->_entityPurchase;
    }

    /**
     * @param string
     */
    public function setEntityPurchase($value)
    {
        $this->_entityPurchase = TPropertyValue::ensureString($value);
    }

    /**
     * @param string
     */
    public function getTypeInvitation()
    {
        return $this->_typeInvitation;
    }

    /**
     * @param string
     */
    public function setTypeInvitation($value)
    {
        $this->_typeInvitation = TPropertyValue::ensureString($value);
    }

    /**
     * @param string
     */
    public function getMailAgent()
    {
        return $this->_mailAgent;
    }

    /**
     * @param string
     */
    public function setMailAgent($value)
    {
        $this->_mailAgent = TPropertyValue::ensureString($value);
    }

    /**
     * @return string defaults to ''
     */
    public function getAlerteReponseElectronique()
    {
        return $this->_alerteReponseElectronique;
    }

    /**
     * @param int
     */
    public function setAlerteReponseElectronique($value)
    {
        $this->_alerteReponseElectronique = TPropertyValue::ensureInteger($value);
    }

    /**
     * @return string defaults to ''
     */
    public function getAlerteClotureConsultation()
    {
        return $this->_alerteClotureConsultation;
    }

    /**
     * @param int
     */
    public function setAlerteClotureConsultation($value)
    {
        $this->_alerteClotureConsultation = TPropertyValue::ensureInteger($value);
    }

    /**
     * @return string defaults to ''
     */
    public function getAlerteReceptionMessage()
    {
        return $this->_alerteReceptionMessage;
    }

    /**
     * @param int
     */
    public function setAlerteReceptionMessage($value)
    {
        $this->_alerteReceptionMessage = TPropertyValue::ensureInteger($value);
    }

    /**
     * @return string defaults to ''
     */
    public function getAlertePublicationBoamp()
    {
        return $this->_alertePublicationBoamp;
    }

    /**
     * @param int
     */
    public function setAlertePublicationBoamp($value)
    {
        $this->_alertePublicationBoamp = TPropertyValue::ensureInteger($value);
    }

    /**
     * @return string defaults to ''
     */
    public function getAlerteEchecPublicationBoamp()
    {
        return $this->_alerteEchecPublicationBoamp;
    }

    /**
     * @param int
     */
    public function setAlerteEchecPublicationBoamp($value)
    {
        $this->_alerteEchecPublicationBoamp = TPropertyValue::ensureInteger($value);
    }

    /**
     * @return string defaults to ''
     */
    public function getAlerteCreationModificationAgent()
    {
        return $this->_alerteCreationModificationAgent;
    }

    /**
     * @return string defaults to ''
     */
    public function getActif()
    {
        return $this->_actif;
    }

    /**
     * @param string
     */
    public function setActif($value)
    {
        $this->_actif = TPropertyValue::ensureInteger($value);
    }

    /**
     * @param int
     */
    public function setAlerteCreationModificationAgent($value)
    {
        $this->_alerteCreationModificationAgent = TPropertyValue::ensureInteger($value);
    }

    public function populate($agent)
    {
        $servicesArray = Atexo_EntityPurchase::getCachedEntities($agent->getOrganisme());
        $organisme = Atexo_Organismes::retrieveOrganismeByAcronyme($agent->getOrganisme());
        $this->setId($agent->getId());
        $this->setLastName($agent->getNom());
        $this->setFirstName($agent->getPrenom());
        $this->setMailAgent($agent->getEmail());
        $this->setActif($agent->getActif());
        $this->setAlerteReponseElectronique($agent->getAlerteReponseElectronique());
        $this->setAlerteClotureConsultation($agent->getAlerteClotureConsultation());
        $this->setAlerteReceptionMessage($agent->getAlerteReceptionMessage());
        $this->setAlertePublicationBoamp($agent->getAlertePublicationBoamp());
        $this->setAlerteEchecPublicationBoamp($agent->getAlerteEchecPublicationBoamp());
        $this->setAlerteCreationModificationAgent($agent->getAlerteCreationModificationAgent());
        $this->setAlerteChorus($agent->getAlerteChorus());

        if (null !== $agent->getServiceId()) {
            $service = $servicesArray[$agent->getServiceId()];
            if ($service instanceof CommonService) {
                $this->setEntityPurchase($service->getSigle());
            } else {
                $this->setEntityPurchase('-');
            }
        } else {
            if ($organisme instanceof CommonOrganisme) {
                $this->setEntityPurchase($organisme->getSigle());
            } else {
                $this->setEntityPurchase('-');
            }
        }
    }

    public function display()
    {
        echo $this->getId().' : '.$this->getFirstName().' '.$this->getLastName().' ('.$this->getEntityPurchase().') : Permanent = '.
        $this->getPermanentGuest().'; TypeInvitation = '.$this->getTypeInvitation().'<br/>';
    }

    /**
     * @param int
     */
    public function setAlerteChorus($value)
    {
        $this->_alerteChorus = TPropertyValue::ensureInteger($value);
    }

    /**
     * @return string defaults to ''
     */
    public function getAlerteChorus()
    {
        return $this->_alerteChorus;
    }
}
