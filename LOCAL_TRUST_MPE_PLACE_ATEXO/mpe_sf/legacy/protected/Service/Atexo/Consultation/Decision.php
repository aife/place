<?php

namespace Application\Service\Atexo\Consultation;

use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonDecisionEnveloppe;
use Application\Propel\Mpe\CommonDecisionEnveloppePeer;
use Application\Propel\Mpe\CommonDecisionEnveloppeQuery;
use Application\Propel\Mpe\CommonDecisionLot;
use Application\Propel\Mpe\CommonDecisionLotPeer;
use Application\Propel\Mpe\CommonDecisionLotQuery;
use Application\Propel\Mpe\CommonEnveloppe;
use Application\Propel\Mpe\CommonOffrePapier;
use Application\Propel\Mpe\CommonOffres;
use Application\Propel\Mpe\CommonTypeDecision;
use Application\Propel\Mpe\CommonTypeDecisionPeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_Entreprise;
use Application\Service\Atexo\Atexo_TrancheBudgetaire;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Config\Atexo_Config_Exception;
use Application\Service\Atexo\Rest\ObjectField\Atexo_Rest_ObjectField_DecisionField;
use Application\Service\Atexo\Rest\ObjectField\Atexo_Rest_ObjectField_DecisionLotField;
use Application\Service\Atexo\Serializer\Atexo_Serializer_Serializer;
use Exception;
use PDO;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Consultation_Decision
{
    public function retrieveDecisionById($idDecisionAttribution, $organisme, $statutEj = null)
    {
        $propelConnection = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonDecisionEnveloppePeer::ID_DECISION_ENVELOPPE, $idDecisionAttribution);
        $c->add(CommonDecisionEnveloppePeer::ORGANISME, $organisme);
        if ($statutEj) {
            $c->add(CommonDecisionEnveloppePeer::STATUTEJ, $statutEj, Criteria::LIKE);
        }
        $decisionEnveloppe = CommonDecisionEnveloppePeer::doSelectOne($c, $propelConnection);

        return $decisionEnveloppe;
    }

    public function retrieveDecisionLotByReferenceAndLot($consultationId, $lot, $organisme)
    {
        $propelConnection = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonDecisionLotPeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonDecisionLotPeer::ORGANISME, $organisme);
        $c->add(CommonDecisionLotPeer::LOT, $lot);
        $decisionLot = CommonDecisionLotPeer::doSelectOne($c, $propelConnection);

        return $decisionLot;
    }

    public function retrieveAttributaireInformation($organisme, $consultationId = null, $lot = null)
    {
        $sql = 'SELECT '
           .'		t_contrat_titulaire.id_contrat_titulaire, t_contrat_titulaire.id_offre, t_cons_lot_contrat.lot, t_cons_lot_contrat.consultation_id, t_contrat_titulaire.numero_contrat, t_contrat_titulaire.date_notification,'
           .'		t_contrat_titulaire.montant_contrat, t_contrat_titulaire.type_depot_reponse '
           .' FROM '
           .'		t_contrat_titulaire ,  t_cons_lot_contrat '
           .' WHERE t_contrat_titulaire.id_contrat_titulaire= t_cons_lot_contrat.id_contrat_titulaire '
           ." AND t_contrat_titulaire.contrat_class_key = '".Atexo_Config::getParameter('TYPE_OBJET_CONTRAT_TITULAIRE')."'";
        if ($consultationId) {
            $sql .= " AND t_cons_lot_contrat.consultation_id='".(new Atexo_Db())->quote($consultationId)."' AND t_contrat_titulaire.organisme = '".(new Atexo_Db())->quote($organisme)."'";

            //Garder !==null pour rechercher le lot 0
            if (null !== $lot) {
                $sql .= " AND t_cons_lot_contrat.lot='".$lot."' ";
            }
        }
        $statement = Atexo_Db::getLinkCommon(true)->query($sql);

        $decisions = [];
        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            $decision = [];
            $decision['id_decision'] = $row['id_contrat_titulaire'];
            $decision['numero_marche'] = $row['numero_contrat'];
            $decision['date_notification'] = $row['date_notification'];
            $decision['montant_marche'] = $row['montant_contrat'];
            //$decision['nom_entreprise']='';
            $decision['type_enveloppe'] = $row['type_depot_reponse'];
            if ($decision['type_enveloppe'] == Atexo_Config::getParameter('DEPOT_ELECTRONIQUE')) {
                $offre = (new Atexo_Consultation_Responses())->retrieveOffreById($row['id_offre'], $organisme);
                if ($offre instanceof CommonOffres) {
                    $entreprise = (new Atexo_Entreprise())->retrieveCompanyBdeById($offre->getEntrepriseId());
                    if ($entreprise) {
                        $decision['nom_entreprise'] = $entreprise->getNom();
                    }
                }
            } else {
                $offre = (new Atexo_Consultation_Responses())->retrieveOffrePapierById($row['id_offre'], $organisme);
                if ($offre instanceof CommonOffrePapier) {
                    $decision['nom_entreprise'] = $offre->getNomEntreprise();
                }
            }

            $decision['consultation_id'] = $row['consultation_id'];
            $decision['lot'] = $row['lot'];
            $decision['id_offre'] = $row['id_offre'];
            $enveloppeOffre = (new Atexo_Consultation_Responses())->retrieveEnveloppesByIdOffre($decision['id_offre'], $organisme, Atexo_Config::getParameter('TYPE_ENV_OFFRE'), $decision['lot']);
            if (is_array($enveloppeOffre)) {
                $enveloppeOffre = array_shift($enveloppeOffre);
                if ($enveloppeOffre instanceof CommonEnveloppe) {
                    $decision['id_enveloppe'] = $enveloppeOffre->getIdEnveloppeElectro();
                    $decision['statut_enveloppe'] = $enveloppeOffre->getStatutEnveloppe();
                }
            }

            $decisions[] = $decision;
        }

        return $decisions;
    }

    public function getIdEnveloppeOffreContrat($organisme, $consultationId, $idContrat, $lot)
    {
        $sql = 'SELECT '
            .'		E.id_enveloppe_electro '
            .' FROM '
            .'	 t_contrat_titulaire c, t_cons_lot_contrat LC, Enveloppe E, Offres O'
            .' WHERE  c.id_contrat_titulaire= LC.id_contrat_titulaire '
            ." AND c.id_contrat_titulaire='".$idContrat."' "
            //TODO enlever le commentaire pour avoir la modification du modele de donnee aprés le merge
           // ." AND c.contrat_class_key = '" . Atexo_Config::getParameter('TYPE_OBJET_CONTRAT_TITULAIRE') ."' "
            ." AND c.organisme = '".(new Atexo_Db())->quote($organisme)."'"
            ." AND LC.consultation_id='".(new Atexo_Db())->quote($consultationId)."' AND LC.organisme = '".(new Atexo_Db())->quote($organisme)."'"
            ." AND LC.lot='".$lot."' "
            .'AND E.OFFRE_ID = O.ID  '
            .' AND E.OFFRE_ID=c.id_offre '
            ." AND E.ORGANISME = O.ORGANISME AND E.ORGANISME = '".(new Atexo_Db())->quote($organisme)."'"
            .' AND E.SOUS_PLI=LC.lot ';

        $statement = Atexo_Db::getLinkCommon(true)->query($sql);

        $decisions = [];
        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            return $row['id_enveloppe_electro'];
        }
    }

    public function retrieveDecisionEnvoloppeByReference($consultationId, $organisme)
    {
        $propelConnection = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonDecisionEnveloppePeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonDecisionEnveloppePeer::ORGANISME, $organisme);
        $decisionLot = CommonDecisionEnveloppePeer::doSelect($c, $propelConnection);

        return $decisionLot;
    }

    public function getTypeDecision()
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $typeDecision = CommonTypeDecisionPeer::doSelect($c, $connexion);
        $allTypeDecision = [];
        foreach ($typeDecision as $unType) {
            $allTypeDecision[] = $unType->getCodeTypeDecision();
        }

        return $allTypeDecision;
    }

    public function recupererInfosOffresElectroniques($organisme)
    {
        $queryParams = [];
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        //Enveloppes candidatures
        $sql1 = ' SELECT O.id, DL.lot, C.reference_utilisateur as reference, C.titre, C.resume, C.datefin, C.date_mise_en_ligne_calcule as date_mise_ligne, C.id_type_procedure, C.intitule, C.objet, ';
        $sql1 .= ' O.uid_offre as num, E.offre_id, dateheure_ouverture, DL.date_maj, DL.id_type_decision, C.alloti, C.champ_supp_invisible, type_env, C.id as ref_id ';
        $sql1 .= " FROM `DecisionLot` DL, `consultation` C LEFT OUTER JOIN `Offres` O ON (C.id = O.consultation_id AND C.organisme = O.organisme) LEFT OUTER JOIN `Enveloppe` E ON (O.id = E.offre_id AND O.organisme = E.organisme AND E.type_env = '".Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE')."') ";
        $sql1 .= ' WHERE DL.consultation_id = C.id AND DL.organisme = C.organisme AND C.organisme = :organisme ';
        $sql1 .= " AND DL.date_maj >= '".Atexo_Util::getDateMinutesAgo(Atexo_Config::getParameter('INTERVALLE_DUREE_RECUP_DONNEES_INTERFACE_PLEIADE'))."'";

        //Enveloppes Offre
        $sql2 = ' SELECT O.id, DL.lot, C.reference_utilisateur as reference, C.titre, C.resume, C.datefin, C.date_mise_en_ligne_calcule as date_mise_ligne, C.id_type_procedure, C.intitule, C.objet, ';
        $sql2 .= ' O.uid_offre as num, E.offre_id, dateheure_ouverture, DL.date_maj, DL.id_type_decision, C.alloti, C.champ_supp_invisible, type_env, C.id as ref_id ';
        $sql2 .= " FROM `DecisionLot` DL, `consultation` C LEFT OUTER JOIN `Offres` O ON (C.id = O.consultation_id AND C.organisme = O.organisme) LEFT OUTER JOIN `Enveloppe` E ON (O.id = E.offre_id AND O.organisme = E.organisme AND E.type_env = '".Atexo_Config::getParameter('TYPE_ENV_OFFRE')."') ";
        $sql2 .= ' WHERE DL.consultation_id = C.id AND DL.organisme = C.organisme AND C.organisme = :organisme ';
        $sql2 .= ' AND E.sous_pli = DL.lot ';
        $sql2 .= " AND DL.date_maj >= '".Atexo_Util::getDateMinutesAgo(Atexo_Config::getParameter('INTERVALLE_DUREE_RECUP_DONNEES_INTERFACE_PLEIADE'))."'";

        $sql = " SELECT T.* FROM ( $sql1 UNION $sql2 ) T WHERE 1 = 1 ORDER BY offre_id, type_env";

        $statement = Atexo_Db::getLinkCommon(true)->prepare($sql);
        $queryParams[':organisme'] = $organisme;
        try {
            $statement->execute($queryParams);
        } catch (Exception $e) {
            Prado::log('Erreur Decision.php'.$e->getMessage().$e->getTraceAsString(), TLogger::ERROR, 'Decision.php');
        }
        $dataOffreElec = [];
        $arrayId = [];
        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            $idArray = $row['ref_id'].'_'.$row['lot'].'_'.$organisme;
            if (!isset($dataOffreElec[$idArray])) {
                if (!in_array($idArray, $arrayId)) {
                    $arrayId[] = $idArray;
                } else {
                    continue;
                }
                //Data DecisionLot
                $dataOffreElec[$idArray]['date_decision'] = $row['date_maj'];
                if ('1' == $row['alloti']) {
                    $dataOffreElec[$idArray]['numero_lot'] = $row['lot'];
                } else {
                    $dataOffreElec[$idArray]['numero_lot'] = '';
                }

                $typeDecision = CommonTypeDecisionPeer::retrieveByPK($row['id_type_decision'], $connexion);
                if ($typeDecision instanceof CommonTypeDecision) {
                    $dataOffreElec[$idArray]['type_decision'] = Prado::localize($typeDecision->getCodeTypeDecision());
                } else {
                    $dataOffreElec[$idArray]['type_decision'] = '';
                }

                //Data consultation
                $dataOffreElec[$idArray]['reference'] = $row['reference'];
                $dataOffreElec[$idArray]['objet'] = $row['objet'];
                $dataOffreElec[$idArray]['intitule'] = $row['intitule'];
                $dataOffreElec[$idArray]['type_procedure'] = (new Atexo_Consultation_ProcedureType())->retrieveTypeProcedure($row['id_type_procedure'], false, $organisme);
                $dataOffreElec[$idArray]['commentaire'] = $row['champ_supp_invisible'];
                $dataOffreElec[$idArray]['dateFin'] = $row['datefin'];
                $dataOffreElec[$idArray]['dateMiseEnLigne'] = $row['date_mise_ligne'];
            }
            //Data Enveloppe
            if (!isset($dataOffreElec[$idArray]['min_date_ouverture'][$row['type_env']]) || $row['dateheure_ouverture'] < $dataOffreElec[$idArray]['min_date_ouverture'][$row['type_env']]) {
                $dataOffreElec[$idArray]['min_date_ouverture'][$row['type_env']] = $row['dateheure_ouverture'];
            }
            $dataOffreElec[$idArray]['type_env'][$row['type_env']] = $row['type_env'];
        }

        return $dataOffreElec;
    }

    public function recupererInfosOffresPapiers($organisme)
    {
        $queryParams = [];
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        //Enveloppes candidatures
        $sql1 = ' SELECT OP.id, DL.lot, C.reference_utilisateur as reference, C.titre, C.resume, C.datefin, C.date_mise_en_ligne_calcule as date_mise_ligne, C.id_type_procedure, C.intitule, C.objet, ';
        $sql1 .= ' OP.id as num, EP.offre_papier_id, dateheure_ouverture, DL.date_maj, DL.id_type_decision, C.alloti, C.champ_supp_invisible, type_env, C.id as ref_id ';
        $sql1 .= " FROM `DecisionLot` DL, `consultation` C LEFT OUTER JOIN `Offre_papier` OP ON (C.id = OP.consultation_id AND C.organisme = OP.organisme) LEFT OUTER JOIN `Enveloppe_papier` EP ON (OP.id = EP.offre_papier_id AND OP.organisme = EP.organisme AND EP.type_env = '".Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE')."') ";
        $sql1 .= ' WHERE DL.consultation_id = C.id AND DL.organisme = C.organisme AND C.organisme = :organisme ';
        $sql1 .= " AND DL.date_maj >= '".Atexo_Util::getDateMinutesAgo(Atexo_Config::getParameter('INTERVALLE_DUREE_RECUP_DONNEES_INTERFACE_PLEIADE'))."'";

        //Enveloppes Offre
        $sql2 = ' SELECT OP.id, DL.lot, C.reference_utilisateur as reference, C.titre, C.resume, C.datefin, C.date_mise_en_ligne_calcule as date_mise_ligne, C.id_type_procedure, C.intitule, C.objet, ';
        $sql2 .= ' OP.id as num, EP.offre_papier_id, dateheure_ouverture, DL.date_maj, DL.id_type_decision, C.alloti, C.champ_supp_invisible, type_env, C.id as ref_id ';
        $sql2 .= " FROM `DecisionLot` DL, `consultation` C LEFT OUTER JOIN  `Offre_papier` OP ON (C.id = OP.consultation_id AND C.organisme = OP.organisme) LEFT OUTER JOIN `Enveloppe_papier` EP ON (OP.id = EP.offre_papier_id AND OP.organisme = EP.organisme AND EP.type_env = '".Atexo_Config::getParameter('TYPE_ENV_OFFRE')."') ";
        $sql2 .= ' WHERE DL.consultation_id = C.id AND DL.organisme = C.organisme AND C.organisme = :organisme ';
        $sql2 .= ' AND EP.sous_pli = DL.lot ';
        $sql2 .= " AND DL.date_maj >= '".Atexo_Util::getDateMinutesAgo(Atexo_Config::getParameter('INTERVALLE_DUREE_RECUP_DONNEES_INTERFACE_PLEIADE'))."'";

        $sql = " SELECT T.* FROM ( $sql1 UNION $sql2 ) T WHERE 1 = 1 ORDER BY offre_papier_id, type_env";

        $statement = Atexo_Db::getLinkCommon(true)->prepare($sql);
        $queryParams[':organisme'] = $organisme;
        try {
            $statement->execute($queryParams);
        } catch (Exception $e) {
            Prado::log('Erreur Decision.php'.$e->getMessage().$e->getTraceAsString(), TLogger::ERROR, 'Decision.php');
        }
        $dataOffrePapier = [];
        $arrayId = [];
        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            $idArray = $row['ref_id'].'_'.$row['lot'].'_'.$organisme;
            if (!isset($dataOffrePapier[$idArray])) {
                if (!in_array($idArray, $arrayId)) {
                    $arrayId[] = $idArray;
                } else {
                    continue;
                }
                //Data DecisionLot
                $dataOffrePapier[$idArray]['date_decision'] = $row['date_maj'];
                if ('1' == $row['alloti']) {
                    $dataOffrePapier[$idArray]['numero_lot'] = $row['lot'];
                } else {
                    $dataOffrePapier[$idArray]['numero_lot'] = '';
                }

                $typeDecision = CommonTypeDecisionPeer::retrieveByPK($row['id_type_decision'], $connexion);
                if ($typeDecision instanceof CommonTypeDecision) {
                    $dataOffrePapier[$idArray]['type_decision'] = Prado::localize($typeDecision->getCodeTypeDecision());
                } else {
                    $dataOffrePapier[$idArray]['type_decision'] = '';
                }

                //Data consultation
                $dataOffrePapier[$idArray]['reference'] = $row['reference'];
                $dataOffrePapier[$idArray]['objet'] = $row['objet'];
                $dataOffrePapier[$idArray]['intitule'] = $row['intitule'];
                $dataOffrePapier[$idArray]['type_procedure'] = (new Atexo_Consultation_ProcedureType())->retrieveTypeProcedure($row['id_type_procedure'], false, $organisme);
                $dataOffrePapier[$idArray]['commentaire'] = $row['champ_supp_invisible'];
                $dataOffrePapier[$idArray]['dateFin'] = $row['datefin'];
                $dataOffrePapier[$idArray]['dateMiseEnLigne'] = $row['date_mise_ligne'];
            }
            //Data Enveloppe
            if (!isset($dataOffrePapier[$idArray]['min_date_ouverture'][$row['type_env']]) || $row['dateheure_ouverture'] < $dataOffrePapier[$idArray]['min_date_ouverture'][$row['type_env']]) {
                $dataOffrePapier[$idArray]['min_date_ouverture'][$row['type_env']] = $row['dateheure_ouverture'];
            }
            $dataOffrePapier[$idArray]['type_env'][$row['type_env']] = $row['type_env'];
        }

        return $dataOffrePapier;
    }

    /**
     * retourne les infos d'une attributaire.
     *
     * @param id decision
     * return array infos
     */
    public function getEntrepriseInfosByIdTypeDecision($idDecision)
    {
        $arrayInfos = [];
        $requete = 'SELECT e. * FROM  `decisionEnveloppe` d, Offres o, Entreprise e WHERE d.id_offre = o.id AND o.entreprise_id = e.id AND d.id_decision_enveloppe ='.$idDecision.' ;';
        $statement = Atexo_Db::getLinkCommon(true)->prepare($requete);
        $statement->execute();
        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            $arrayInfos['nom'] = $row['nom'];
            $arrayInfos['adresse'] = $row['adresse'];
            $arrayInfos['siren'] = $row['siren'];
            $arrayInfos['villeadresse'] = $row['villeadresse'];
            $arrayInfos['codepostal'] = $row['codepostal'];
            $arrayInfos['email'] = $row['email'];
            $arrayInfos['telephone'] = $row['telephone'];
            $arrayInfos['fax'] = $row['fax'];
        }

        return $arrayInfos;
    }

    /**
     * Permet de setter les information de decision pour le WS.
     *
     * @param $organisme
     * @param $id
     * @param $typeDepotOffre
     * @param $xmlResponse
     * @param $logger
     * return void|string
     * @param $consultation
     *
     * @return string
     *
     * @throws Atexo_Config_Exception
     * @throws PropelException
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-megalis
     *
     * @copyright Atexo 2016
     */
    public function setInfoDecisionForWs($organisme, $id, $typeDepotOffre, &$xmlResponse, $logger, $consultation)
    {
        $logger->info("La récupération d'info decision");
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $messageErreur = '';
        if ($consultation instanceof CommonConsultation) {
            $consultationId = $consultation->getId();
            $logger->info('La récupération des decision enveloppe ');
            $decisionEnveloppe = self::retrieveDecisionEnvoloppeByReferenceAndOffre($consultationId, $organisme, $id, $typeDepotOffre, $connexion);
            if (is_array($decisionEnveloppe) && count($decisionEnveloppe)) {
                $decisionField = new Atexo_Rest_ObjectField_DecisionField();
                $decisionField->setId($id);
                $decisionField->setReference($consultationId);
                $decisionField->setOrganisme($organisme);
                $decisionField->setType(Atexo_Util::getLibelleTypeDepot($typeDepotOffre));
                $decisionLotExiste = false;
                foreach ($decisionEnveloppe as $decision) {
                    if ($decision instanceof CommonDecisionEnveloppe) {
                        $lot = $decision->getLot();
                        $logger->info('La récupération de decision lot => '.$lot.' reference => '.$consultationId.' organisme => '.$organisme);
                        $decisionLot = self::retrieveDecisionLot($consultationId, $organisme, $lot, $connexion);
                        if ($decisionLot instanceof CommonDecisionLot) {
                            $decisionLotExiste = true;
                            $logger->info('La decision lot existe');
                            $decisionLotField = new Atexo_Rest_ObjectField_DecisionLotField();
                            $decisionLotField->setNumeroLot($lot);

                            $decisionLotField->setDecision(Atexo_Util::getLibelleDecision($decisionLot->getIdTypeDecision()));
                            $decisionLotField->setDateDecision($decisionLot->getDateDecision('Y-m-d'));
                            $decisionLotField->setDateNotification($decision->getDateNotification());
                            if ($decision->getTrancheBudgetaire()) {
                                $logger->info('La recuperation de libelle de tranche budgetaire');
                                $libelleTranche = (new Atexo_TrancheBudgetaire())->getLibelleTrancheBudgetaire($decision->getTrancheBudgetaire(), $organisme);
                                $decisionLotField->setTrancheBudgetaire($libelleTranche);
                            } else {
                                $logger->info("il n'y a pas des tranche budgetaire pour cette decision");
                            }
                            $decisionLotField->setMontantMarche(Atexo_Util::getMontantArronditSansEspace($decision->getMontantMarche()));
                            $decisionLotField->setObjetMarche($decision->getObjetMarche());
                            $decisionLotField->setNaturePrestation(strtoupper($decision->getLibelleCategorieConsultation()));
                            $decisionLotField->setNumeroMarche($decision->getNumeroMarche());
                            $decisionLotField->setDateFinMarchePrevisionnelle($decision->getDateFinMarchePrevisionnel());
                            $decisionLotField->setDateFinMarcheReelle($decision->getDateFinMarcheReelle());
                            $decisionLotField->setCodePostalAttributaire($decision->getCodePostal());
                            $decisionLotField->setSirenAttributaire($decision->getSirenAttributaire());
                            $decisionLotField->setNicAttributaire($decision->getNicAttributaire());
                            $decisionField->addDecisionLot($decisionLotField);
                        } else {
                            $logger->error('La decision lot => '.$lot.' reference => '.$consultationId.' organisme => '.$organisme." n'existe pas");
                        }
                    }
                }
                if ($decisionLotExiste) {
                    $serializer = new Atexo_Serializer_Serializer();
                    $mappingFile = Atexo_Config::getParameter('PATH_FILE_MAPPING_DECISION');
                    $listOptioanlField = Atexo_Config::getParameter('PATH_FILE_OPTIONAL_FIELD_DECISION');
                    $logger->info("La recuperation de l'xml reponse");
                    $xmlResponse = $serializer->SerializeClass($decisionField, $decisionField::class, $mappingFile, false, $listOptioanlField);
                } else {
                    $messageErreur = "Ce dépôt ne dispose pas d'information de décision";
                }
            } else {
                $messageErreur = "Ce dépôt ne dispose pas d'information de décision";
            }
        } else {
            $messageErreur = 'Ce depot ne correspond pas à une consultation valide';
        }

        return $messageErreur;
    }

    /**
     * Retourne les decision enveloppe d'une offre.
     *
     * @param $reference
     * @param $organisme
     * @param $idOffre
     * @param $typeEnv
     * @param null $connexion
     *
     * @return array
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-megalis
     *
     * @copyright Atexo 2016
     */
    public function retrieveDecisionEnvoloppeByReferenceAndOffre($consultationId, $organisme, $idOffre, $typeEnv, $connexion = null)
    {
        $decisionEnveloppeQuery = new CommonDecisionEnveloppeQuery();

        return $decisionEnveloppeQuery->getDecisionsEnveloppesByOffre($consultationId, $organisme, $idOffre, $typeEnv, $connexion);
    }

    /**
     * Retourne les decision lot d'un lot.
     *
     * @param $reference
     * @param $organisme
     * @param $lot
     * @param null $connexion
     *
     * @return CommonDecisionLot
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-megalis
     *
     * @copyright Atexo 2016
     */
    public function retrieveDecisionLot($consultationId, $organisme, $lot, $connexion = null)
    {
        $decisionLotsQuery = new CommonDecisionLotQuery();

        return $decisionLotsQuery->getDecisionsLot($consultationId, $organisme, $lot, $connexion);
    }
}
