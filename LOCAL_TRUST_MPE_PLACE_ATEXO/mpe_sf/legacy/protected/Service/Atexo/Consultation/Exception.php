<?php

namespace Application\Service\Atexo\Consultation;

use Application\Service\Atexo\Atexo_Exception;
use AtexoCrypto\Dto\Consultation;
use Exception;

/**
 * Exception personnalisée de la librairie Consultation.
 *
 * @author Guillaume Ponçon <guillaume.poncon@openstates.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Consultation_Exception extends Atexo_Exception
{
}
