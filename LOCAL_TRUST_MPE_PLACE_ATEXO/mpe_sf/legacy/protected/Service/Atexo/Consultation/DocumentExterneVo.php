<?php

namespace Application\Service\Atexo\Consultation;

class Atexo_Consultation_DocumentExterneVo
{
    private $refConsultation;
    private $idBlob;
    private $idEntreprise;
    private $type;
    private $nom;
    private $description;
    private $date;
    private string $defaultSortByElement = 'date';
    private string $sortByElement = 'nom';
    private string $sensOrderBy = 'DESC';
    private int $offset = 0;
    private int $limit = 0;

    public function getRefConsultation()
    {
        return $this->refConsultation;
    }

    public function setRefConsultation($refConsultation)
    {
        $this->refConsultation = $refConsultation;
    }

    public function getIdBlob()
    {
        return $this->idBlob;
    }

    public function setIdBlob($idBlob)
    {
        $this->idBlob = $idBlob;
    }

    public function getIdEntreprise()
    {
        return $this->idEntreprise;
    }

    public function setIdEntreprise($idEntreprise)
    {
        $this->idEntreprise = $idEntreprise;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($type)
    {
        $this->type = $type;
    }

    public function getNom()
    {
        return $this->nom;
    }

    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function setDate($date)
    {
        $this->date = $date;
    }

    public function getDefaultSortByElement()
    {
        return $this->defaultSortByElement;
    }

    public function setDefaultSortByElement($defaultSortByElement)
    {
        $this->defaultSortByElement = $defaultSortByElement;
    }

    public function getSortByElement()
    {
        return $this->sortByElement;
    }

    public function setSortByElement($sortByElement)
    {
        $this->sortByElement = $sortByElement;
    }

    public function getSensOrderBy()
    {
        return $this->sensOrderBy;
    }

    public function setSensOrderBy($sensOrderBy)
    {
        $this->sensOrderBy = $sensOrderBy;
    }

    public function getOffset()
    {
        return $this->offset;
    }

    public function setOffset($offset)
    {
        $this->offset = $offset;
    }

    public function getLimit()
    {
        return $this->limit;
    }

    public function setLimit($limit)
    {
        $this->limit = $limit;
    }
}
