<?php

namespace Application\Service\Atexo\Consultation;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonBlocFichierEnveloppePeer;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonEnveloppePeer;
use Application\Propel\Mpe\CommonOffresPeer;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Exception;
use PDO;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Consultation_OuverturePlis
{
    public function receptionBlocDechiffre($idBlob, $contenuBlocDechiffre, $organisme)
    {
        try {
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));

            //Insertion du bloc dans le fichier
            $fileName = 'bloc_'.session_name().session_id().$idBlob.time();
            $filePath = Atexo_Config::getParameter('COMMON_TMP').'/'.$fileName;
            $fp = fopen($filePath, 'w');
            if ('' != $contenuBlocDechiffre) {
                if (!fwrite($fp, base64_decode($contenuBlocDechiffre))) {
                    throw new Exception("Impossible d'ecrire dans le fichier");
                }
            }
            fclose($fp);

            $atexoBlob = new Atexo_Blob();
            $idBlocBlob = $atexoBlob->insert_blob($fileName, $filePath, $organisme);

            $c = new Criteria();
            $c->add(CommonBlocFichierEnveloppePeer::ID_BLOB_CHIFFRE, $idBlob);
            $c->add(CommonBlocFichierEnveloppePeer::ORGANISME, $organisme);
            $bloc = CommonBlocFichierEnveloppePeer::doSelectOne($c, $connexionCom);
            $bloc->setIdBlobDechiffre($idBlocBlob);
            $bloc->save($connexionCom);
            @unlink($fileName);
        } catch (Exception $e) {
            throw $e;
        }
    }

    public function blocsDechiffres($idEnveloppe, $organisme)
    {
        try {
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $enveloppe = CommonEnveloppePeer::retrieveByPK($idEnveloppe, $organisme, $connexionCom);
            $fichierEnveloppes = $enveloppe->getCommonFichierEnveloppes($connexionCom);
            $fileContent = '';

            $atexoBlob = new Atexo_Blob();
            $fichierDechiffre = Atexo_Config::getParameter('COMMON_TMP').'bloc_dechiffre_'.session_name().session_id().time();

            foreach ($fichierEnveloppes as $unFichier) {
                $blocs = $unFichier->getBlocfichierenveloppes(null, $connexionCom);
                foreach ($blocs as $bloc) {
                    if ($bloc && $bloc->getIdBlobDechiffre()) {
                        $fileContent = $atexoBlob->return_blob_to_client($bloc->getIdBlobDechiffre(), $organisme);
                        $fp = fopen($fichierDechiffre, 'a');
                        if ('' != $fileContent) {
                            if (!fwrite($fp, $fileContent)) {
                                throw new Exception("impossible d'ecrire dans le fichier");
                            }
                        }
                        fclose($fp);
                    }
                }

                $idFichierBlob = $atexoBlob->insert_blob($unFichier->getNomFichier(), $fichierDechiffre, $organisme);

                if ($idFichierBlob) {
                    $unFichier->setIdBlob($idFichierBlob);
                    $unFichier->save($connexionCom);
                } else {
                    throw new Exception('Erreur insert_blob Pour le fichier id => '.$unFichier->getIdFichier());
                }
                if (is_file($fichierDechiffre)) {
                    unlink($fichierDechiffre);
                }
            }

            $enveloppe->setStatutEnveloppe(Atexo_Config::getParameter('STATUT_ENV_OUVERTE_EN_LIGNE'));
            $enveloppe->setCryptage('0');

            if (Atexo_Module::isEnabled('fourEyes')) {
                $enveloppe->setDateheureOuvertureAgent2(date('Y-m-d H:i:s'));
                $enveloppe->setAgentIdOuverture2(Atexo_CurrentUser::getIdAgentConnected());
            } else {
                $enveloppe->setDateheureOuverture(date('Y-m-d H:i:s'));
                $enveloppe->setAgentIdOuverture(Atexo_CurrentUser::getIdAgentConnected());
            }

            $enveloppe->save($connexionCom);

            $query = ' SELECT count(*) as nbEnveloppes From Enveloppe WHERE Enveloppe.offre_id=:offreId AND Enveloppe.statut_enveloppe=:statutFerme ';
            $params = [];
            $params[':offreId'] = $enveloppe->getOffreId();
            $params[':statutFerme'] = Atexo_Config::getParameter('STATUT_ENV_FERME');

            $statement = Atexo_Db::getLinkCommon(true)->prepare($query);

            $results = $statement->execute($params);
            $nbEnveloppes = 0;
            if ($results) {
                while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                    $nbEnveloppes = $row['nbEnveloppes'];
                }
            }
            if (!$nbEnveloppes) {
                self::ouvrirOffres($enveloppe->getOffreId(), $enveloppe->getOrganisme(), Atexo_Config::getParameter('STATUT_ENV_OUVERTE_EN_LIGNE'), $connexionCom);
            }
        } catch (Exception $e) {
            throw $e;
        }
    }

    public static function enveloppeOuverteAdistance($idEnv, $organisme)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $enveloppe = CommonEnveloppePeer::retrieveByPK($idEnv, $organisme, $connexionCom);

        if (!$enveloppe) {
            throw new Exception('Enveloppe '.$idEnv.' introuvable');
        }

        if (Atexo_Module::isEnabled('fourEyes')) {
            $enveloppe->setDateheureOuvertureAgent2(date('Y-m-d H:i:s'));
            $enveloppe->setAgentIdOuverture2(Atexo_CurrentUser::getIdAgentConnected());
        } else {
            $enveloppe->setDateheureOuverture(date('Y-m-d H:i:s'));
            $enveloppe->setAgentIdOuverture(Atexo_CurrentUser::getIdAgentConnected());
        }
        $enveloppe->setStatutEnveloppe(Atexo_Config::getParameter('STATUT_ENV_OUVERTE_A_DISTANCE'));
        $enveloppe->save($connexionCom);

        $query = ' SELECT count(*) as nbEnveloppes From Enveloppe WHERE Enveloppe.offre_id=:offreId AND Enveloppe.statut_enveloppe=:statutFerme ';
        $params = [];
        $params[':offreId'] = $enveloppe->getOffreId();
        $params[':statutFerme'] = Atexo_Config::getParameter('STATUT_ENV_FERME');
        $statement = Atexo_Db::getLinkCommon(true)->prepare($query);

        $results = $statement->execute($params);
        $nbEnveloppes = 0;
        if ($results) {
            while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                $nbEnveloppes = $row['nbEnveloppes'];
            }
        }

        if (!$nbEnveloppes) {
            self::ouvrirOffres($enveloppe->getOffreId(), $enveloppe->getOrganisme(), Atexo_Config::getParameter('STATUT_ENV_OUVERTE_A_DISTANCE'), $connexionCom);
        }
    }

    public function AgentInviteConsultation($idBlob, $organisme, $idEnv = null)
    {
        $sql = 'Select consultation_id from Offres, Enveloppe, fichierEnveloppe, blocFichierEnveloppe where 1 '
        .' AND blocFichierEnveloppe.id_fichier=fichierEnveloppe.id_fichier AND fichierEnveloppe.id_enveloppe=Enveloppe.id_enveloppe_electro AND Enveloppe.offre_id=Offres.id  '
        ." AND Offres.organisme=Enveloppe.organisme AND fichierEnveloppe.organisme=blocFichierEnveloppe.organisme AND Enveloppe.organisme=fichierEnveloppe.organisme AND Offres.organisme= '".$organisme."'";

        if ($idBlob) {
            $sql .= " AND blocFichierEnveloppe.id_blob_chiffre='".(new Atexo_Db())->quote($idBlob)."'";
        }
        if ($idEnv) {
            $sql .= " AND fichierEnveloppe.id_enveloppe='".(new Atexo_Db())->quote($idEnv)."'";
        }
        $statement = Atexo_Db::getLinkCommon(true)->query($sql);
        $consultationId = '';
        foreach ($statement as $row) {
            $reference = $row[0];
        }

        if ($consultationId) {
            $critere = new Atexo_Consultation_CriteriaVo();
            $critere->setIdReference($consultationId);
            $critere->setAcronymeOrganisme($organisme);
            $critere->setIdService(Atexo_CurrentUser::getCurrentServiceId());
            $critere->setConnectedAgentId(Atexo_CurrentUser::getId());
            $consultationArray = (new Atexo_Consultation())->search($critere);
            if (is_array($consultationArray)) {
                $consultation = array_shift($consultationArray);
                if ($consultation instanceof CommonConsultation && !$consultation->getCurrentUserReadOnly()) {
                    return true;
                } else {
                    return false;
                }
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /*
     * permet d'ouvrir une offre
     */
    public function ouvrirOffres($idOffre, $organisme, $statut, $connexionCom)
    {
        Atexo_Util::write_file(Atexo_Config::getParameter('BASE_ROOT_DIR').'/logs/Enveloppe.txt', "\n idOffre ".$idOffre.' statut = '.$statut.' ', 'a');
        $offre = CommonOffresPeer::retrieveByPK($idOffre, Atexo_CurrentUser::getCurrentOrganism(), $connexionCom);
        $offre->setStatutOffres($statut);
        $offre->setCryptageReponse('0');
        if (Atexo_Module::isEnabled('fourEyes')) {
            $offre->setDateHeureOuvertureAgent2(date('Y-m-d H:i:s'));
            $offre->setAgentidOuverture2(Atexo_CurrentUser::getIdAgentConnected());
        } else {
            $offre->setDateHeureOuverture(date('Y-m-d H:i:s'));
            $offre->setAgentidOuverture(Atexo_CurrentUser::getIdAgentConnected());
        }
        $offre->save($connexionCom);
    }
}
