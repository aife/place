<?php

namespace Application\Service\Atexo\Consultation;

use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonCategoriesConsiderationsSocialesPeer;
use Application\Propel\Mpe\CommonCategoriesConsiderationsSocialesQuery;
use Application\Service\Atexo\Atexo_Config;
use Exception;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * Class Atexo_Consultation_CategoriesConsiderationsSociales.
 */
class Atexo_Consultation_CategoriesConsiderationsSociales
{
    /**
     * @return mixed
     *
     * @throws PropelException
     */
    public static function getAllArray()
    {
        $propelConnection = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $categoriesConsiderationsSocialesArray = [];
        try {
            $c = new Criteria();
            $categoriesConsiderationsSociales = CommonCategoriesConsiderationsSocialesPeer::doSelect($c, $propelConnection);
            foreach ($categoriesConsiderationsSociales as $categoriesConsiderationsSociale) {
                //$categoriesConsiderationsSocialesArray[$categoriesConsiderationsSociale->getId()] = $categoriesConsiderationsSociale->getLabel();
                $categoriesConsiderationsSocialesArray[] = ['id' => $categoriesConsiderationsSociale->getId(), 'label' => $categoriesConsiderationsSociale->getLabel()];
            }
        } catch (Exception $exception) {
            Prado::log(' Erreur lors de la requête ou de la construction du tableau / '.$exception->getMessage().$exception->getTraceAsString(), TLogger::ERROR);
        }

        return $categoriesConsiderationsSocialesArray;
    }

    /**
     * Permet de recuperer la liste des CategoriesConsiderationsSociales par des ids.
     *
     * @param array ids
     *
     * @return liste of object CategoriesConsiderationsSociales
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2018-esr
     *
     * @copyright Atexo 2018
     */
    public static function retrieveCategorieConsiderationsSocialesByIds($ids)
    {
        $propelConnection = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $categorieQuery = new CommonCategoriesConsiderationsSocialesQuery();

        return $categorieQuery->getCategorieConsiderationsSocialesByIds($ids, $propelConnection);
    }
}
