<?php

namespace Application\Service\Atexo\Consultation\Guests;

/**
 * Vo pour définir les critères de Atexo_Consultation::search().
 *
 * @author Guillaume Ponçon <guillaume.poncon@openstates.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Consultation_Guests_CriteriaVo
{
    private string $_name = '';
    private string $_orgAcronyme = '';
    private $_associatedServiceId = '';
    private $_curAgentServiceId = '';
    private array $_excludedAgents = [];
    private $_idReference = '';
    private $_readOnlyGuests = false;
    private $_centralizedOrganization = true;
    private $_permanentNotInMyEntity = '';
    private $offset;
    private $limit;
    // Accesseurs

    /**
     * recupere la valeur du [_name].
     *
     * @return string la valeur courante [_name]
     *
     * @author Guillaume Ponçon <guillaume.poncon@openstates.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2014
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * modifie la valeur de [_name].
     *
     * @param string $value la valeur a mettre
     *
     * @return void
     *
     * @author Guillaume Ponçon <guillaume.poncon@openstates.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2014
     */
    public function setName($value)
    {
        $this->_name = $value;
    }

    /**
     * recupere la valeur du [_orgAcronyme].
     *
     * @return string la valeur courante [_orgAcronyme]
     *
     * @author Guillaume Ponçon <guillaume.poncon@openstates.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2014
     */
    public function getOrgAcronym()
    {
        return $this->_orgAcronyme;
    }

    /**
     * modifie la valeur de [_orgAcronyme].
     *
     * @param string $value la valeur a mettre
     *
     * @return void
     *
     * @author Guillaume Ponçon <guillaume.poncon@openstates.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2014
     */
    public function setOrgAcronym($value)
    {
        $this->_orgAcronyme = $value;
    }

    /**
     * recupere la valeur du [_associatedServiceId].
     *
     * @return int la valeur courante [_associatedServiceId]
     *
     * @author Guillaume Ponçon <guillaume.poncon@openstates.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2014
     */
    public function getAssociatedServiceId()
    {
        return $this->_associatedServiceId;
    }

    /**
     * modifie la valeur de [_associatedServiceId].
     *
     * @param int $value la valeur a mettre
     *
     * @return void
     *
     * @author Guillaume Ponçon <guillaume.poncon@openstates.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2014
     */
    public function setAssociatedServiceId($value)
    {
        $this->_associatedServiceId = $value;
    }

    /**
     * recupere la valeur du [_curAgentServiceId].
     *
     * @return int la valeur courante [_curAgentServiceId]
     *
     * @author Guillaume Ponçon <guillaume.poncon@openstates.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2014
     */
    public function getCurAgentServiceId()
    {
        return $this->_curAgentServiceId;
    }

    /**
     * modifie la valeur de [_curAgentServiceId].
     *
     * @param int $value la valeur a mettre
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setCurAgentServiceId($value)
    {
        $this->_curAgentServiceId = $value;
    }

    /**
     * recupere la valeur du [_excludedAgents].
     *
     * @return array la valeur courante [_excludedAgents]
     *
     * @author Guillaume Ponçon <guillaume.poncon@openstates.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2014
     */
    public function getExcludedAgents()
    {
        return $this->_excludedAgents;
    }

    /**
     * modifie la valeur de [_excludedAgents].
     *
     * @param array $value la valeur a mettre
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setExludedAgents($value)
    {
        $this->_excludedAgents = $value;
    }

    /**
     * recupere la valeur du [_idReference].
     *
     * @return int la valeur courante [_idReference]
     *
     * @author Guillaume Ponçon <guillaume.poncon@openstates.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2014
     */
    public function getIdReference()
    {
        return $this->_idReference;
    }

    /**
     * modifie la valeur de [_idReference].
     *
     * @param int $value la valeur a mettre
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setIdReference($idReference)
    {
        $this->_idReference = $idReference;
    }

    /**
     * recupere la valeur du [_readOnlyGuests].
     *
     * @return array la valeur courante [_readOnlyGuests]
     *
     * @author Guillaume Ponçon <guillaume.poncon@openstates.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2014
     */
    public function getReadOnlyGuests()
    {
        return $this->_readOnlyGuests;
    }

    /**
     * modifie la valeur de [_readOnlyGuests].
     *
     * @param array $value la valeur a mettre
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setReadOnlyGuests($readOnlyGuests)
    {
        $this->_readOnlyGuests = $readOnlyGuests;
    }

    /**
     * recupere la valeur du [_centralizedOrganization].
     *
     * @return bool la valeur courante [_centralizedOrganization]
     *
     * @author Guillaume Ponçon <guillaume.poncon@openstates.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2014
     */
    public function getCentralizedOrganization()
    {
        return $this->_centralizedOrganization;
    }

    /**
     * modifie la valeur de [_centralizedOrganization].
     *
     * @param bool $value la valeur a mettre
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setCentralizedOrganization($centralizedOrganization)
    {
        $this->_centralizedOrganization = $centralizedOrganization;
    }

    /**
     * recupere la valeur du [_permanentNotInMyEntity].
     *
     * @return string la valeur courante [_permanentNotInMyEntity]
     *
     * @author Guillaume Ponçon <guillaume.poncon@openstates.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2014
     */
    public function getPermanentNotInMyEntity()
    {
        return $this->_permanentNotInMyEntity;
    }

    /**
     * modifie la valeur de [_permanentNotInMyEntity].
     *
     * @param string $value la valeur a mettre
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setPermanentNotInMyEntity($permanentNotInMyEntity)
    {
        $this->_permanentNotInMyEntity = $permanentNotInMyEntity;
    }

    /**
     * @return mixed
     */
    public function getOffset()
    {
        return $this->offset;
    }

    /**
     * @param mixed $offset
     */
    public function setOffset($offset)
    {
        $this->offset = $offset;
    }

    /**
     * @return mixed
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * @param mixed $limit
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;
    }
}
