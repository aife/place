<?php

namespace Application\Service\Atexo\Consultation\Guests;

use Application\Service\Atexo\Atexo_Exception;

/**
 * Exception spécifique du composant contrôleur d'Atexo.
 *
 * @author Guillaume Ponçon <guillaume.poncon@openstates.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Consultation_Guests_Exception extends Atexo_Exception
{
}
