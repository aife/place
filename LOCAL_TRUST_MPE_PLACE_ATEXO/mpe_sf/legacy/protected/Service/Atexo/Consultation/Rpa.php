<?php

namespace Application\Service\Atexo\Consultation;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonRPAPeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Util;

/**
 * Classe de manipulation des représentants du pouvoir adjudicateur.
 *
 * @author ZAKI Anas <anas.zaki@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Consultation_Rpa
{
    /**
     * Retourne la liste des représentants du pouvoir adjudicateur (RPA).
     */
    public static function representativesCapacityAdjudicator()
    {
        $c = new Criteria();
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonRPAPeer::ACRONYMEORG, Atexo_CurrentUser::getCurrentOrganism(), Criteria::EQUAL);
        $representatives = CommonRPAPeer::doSelect($c, $connexionCom);
        $arrayRepresentatifs = [];

        if (is_array($representatives)) {
            foreach ($representatives as $representative) {
                $arrayRepresentatifs[$representative->getId()] = Atexo_Util::StrToMaj($representative->getNom()).' '.Atexo_Util::FirstMaj($representative->getPrenom());
            }
        }

        return $arrayRepresentatifs;
    }
}
