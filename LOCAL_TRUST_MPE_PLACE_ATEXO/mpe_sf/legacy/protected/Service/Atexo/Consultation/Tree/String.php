<?php

namespace Application\Service\Atexo\Consultation\Tree;

use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Util;
use AtexoCrypto\Dto\Consultation;

/**
 * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Consultation_Tree_String extends Atexo_Consultation_Tree_Item
{
    private $_string = '';
    private $groupeName = '';
    private $emprunte = '';
    private $typeEmprunte = '';

    /**
     * Get the value of [_string] column.
     *
     * @return string la valeur du [_string]
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function getString()
    {
        return $this->_string;
    }

    /**
     * Permet de setter les attribut du Atexo_Consultation_Tree_Item et la valeur du _string.
     *
     * @param string $_nom le nom, String $_description la description, String $_accessRestriction le type acces,String $string
     *
     * @return void
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function __construct(
        $_nom,
        $_description,
        $_accessRestriction,
        $string,
        $nomDirParent = '',
        $groupeName = '',
        $emprunte = '',
        $typeEmprunte = ''
    ) {
        parent::__construct(
            $_nom,
            $_description,
            $_accessRestriction,
            '',
            $nomDirParent
        );

        $this->_string = $string;
        $this->groupeName = $groupeName;
        $this->emprunte = $emprunte;
        $this->typeEmprunte = $typeEmprunte;
    }

    /**
     * Permet de mettre la chaine de caractere dans un fichier.
     *
     * @param string $fileToAdd le fichier ou on va stoquer la signature, String $directory le chemin du dossier ou se trouve le fichier
     *
     * @return mixed String le chemin du fichier si l'ajout de la chaine a reussi si non boolean false
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function generateFile($fileToAdd, $zipDirectory)
    {
        try {
            if ($this->_string) {
                if (Atexo_Util::writeFile($zipDirectory.'/'.$fileToAdd, $this->_string)) {
                    return $zipDirectory.'/'.$fileToAdd;
                } else {
                    return false;
                }
            }
        } catch (\Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('archivage');
            $logger->error("Erreur generation de fichier : {fileToAdd = $fileToAdd} , {zipDirectory = $zipDirectory}".PHP_EOL.'Erreur : '.$e->getMessage().PHP_EOL.'Trace : '.$e->getTraceAsString());
        }
    }

    /**
     * @return string
     */
    public function getGroupeName()
    {
        return $this->groupeName;
    }

    /**
     * @param string $groupeName
     */
    public function setGroupeName($groupeName)
    {
        $this->groupeName = $groupeName;
    }

    /**
     * @return string
     */
    public function getTypeEmprunte()
    {
        return $this->typeEmprunte;
    }

    /**
     * @param string $typeEmprunte
     */
    public function setTypeEmprunte($typeEmprunte)
    {
        $this->typeEmprunte = $typeEmprunte;
    }

    /**
     * @return string
     */
    public function getEmprunte()
    {
        return $this->emprunte;
    }

    /**
     * @param string $emprunte
     */
    public function setEmprunte($emprunte)
    {
        $this->emprunte = $emprunte;
    }
}
