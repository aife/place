<?php

namespace Application\Service\Atexo\Consultation\Tree;

use AtexoCrypto\Dto\Consultation;

/**
 * Dossier ou fichier de l'archive.
 *
 * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Consultation_Tree_File extends Atexo_Consultation_Tree_Item
{
    private $idBlob = 0;
    private int|string $fromCommon = 0;
    private $emprunte = '';
    private $typeEmprunte = '';
    private $groupeName = '';

    /**
     * recupere la valeur du [idBlob].
     *
     * @return int la valeur courante [idBlob]
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function getIdBlob()
    {
        return $this->idBlob;
    }

    /**
     * recupere la valeur du [fromCommon].
     *
     * @return int la valeur courante [fromCommon]
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function getFromCommon()
    {
        return $this->fromCommon;
    }

    /**
     * Enter description here...
     *
     * @param string $_nom
     * @param bool   $_isDossier
     *
     * @return Atexo_Consultation_ItemTree
     */

    /**
     * permet d'ajouter un fichier a une arborescence.
     *
     * @param string $_nom
     * @param string $_description
     * @param string $_accessRestriction
     * @param string $idBlob
     * @param int    $fromCommon
     * @param array  $_autresProprietes
     * @param $algo string
     * @param $hash string
     *
     * @return string url de telechargement du DIC selon le parametrage
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function __construct($_nom, $_description, $_accessRestriction, $idBlob, $fromCommon = 0, $_autresProprietes = [], $_nomDirParent = '', $algo = '', $hash = '', $groupeName = '', $dateCreation = '')
    {
        parent::__construct($_nom, $_description, $_accessRestriction, $_autresProprietes, $_nomDirParent, false, $dateCreation);
        $this->idBlob = $idBlob;
        $this->fromCommon = $fromCommon;
        $this->emprunte = $hash;
        $this->typeEmprunte = $algo;
        $this->groupeName = $groupeName;
    }

    /**
     * Retourne la valeur courante de [emprunte].
     *
     * @return string
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-megalis
     *
     * @copyright Atexo 2016
     */
    public function getEmprunte()
    {
        return $this->emprunte;
    }

    /**
     * Retourne la valeur courante de [typeEmprunte].
     *
     * @return string
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-megalis
     *
     * @copyright Atexo 2016
     */
    public function getTypeEmprunte()
    {
        return $this->typeEmprunte;
    }

    /**
     * Retourne la valeur courante de [groupeName].
     *
     * @return string
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-megalis
     *
     * @copyright Atexo 2016
     */
    public function getGroupeName()
    {
        return $this->groupeName;
    }
}
