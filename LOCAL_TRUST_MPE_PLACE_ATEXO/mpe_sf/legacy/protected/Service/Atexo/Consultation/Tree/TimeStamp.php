<?php

namespace Application\Service\Atexo\Consultation\Tree;

use Application\Service\Atexo\Atexo_Util;
use AtexoCrypto\Dto\Consultation;

/**
 * Permet de traiter le dossier ou fichier de l'archive.
 *
 * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Consultation_Tree_TimeStamp extends Atexo_Consultation_Tree_Item
{
    private $horodatage = 0;
    private $emprunte = '';
    private $typeEmprunte = '';
    private $groupeName = '';

    /**
     * Get the value of [horodatage] column.
     *
     * @return string la valeur du [horodatage]
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function getHorodatage()
    {
        return $this->horodatage;
    }

    /**
     * Permet de setter les attribut du Atexo_Consultation_Tree_Item et la valeur de l'horodatage.
     *
     * @param string $_nom le nom, String $_description la description, String $_accessRestriction le type acces,String $horodatage l'horodatage, Array $_autresProprietes
     *
     * @return void
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function __construct(
        $_nom,
        $_description,
        $_accessRestriction,
        $horodatage,
        $_autresProprietes = [],
        $_nomDirParent = '',
        $algo = '',
        $hash = '',
        $groupeName = ''
    ) {
        parent::__construct(
            $_nom,
            $_description,
            $_accessRestriction,
            $_autresProprietes,
            $_nomDirParent
        );

        $this->horodatage = $horodatage;
        $this->emprunte = $hash;
        $this->typeEmprunte = $algo;
        $this->groupeName = $groupeName;
    }

    /**
     * Permet de mettre l'horodatage dans un fichier.
     *
     * @param string $fileToAdd le fichier ou on va stoquer la signature, String $directory le chemin du dossier ou se trouve le fichier
     *
     * @return mixed String le chemin du fichier si l'ajout de l'horodatage a reussi si non boolean false
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function generateFile($fileToAdd, $zipDirectory)
    {
        if ($this->horodatage) {
            if (Atexo_Util::writeFile($zipDirectory.'/'.$fileToAdd, base64_decode($this->horodatage))) {
                return $zipDirectory.'/'.$fileToAdd;
            } else {
                return false;
            }
        }
    }

    /**
     * Retourne la valeur courante de [emprunte].
     *
     * @return string
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-megalis
     *
     * @copyright Atexo 2016
     */
    public function getEmprunte()
    {
        return $this->emprunte;
    }

    /**
     * Retourne la valeur courante de [typeEmprunte].
     *
     * @return string
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-megalis
     *
     * @copyright Atexo 2016
     */
    public function getTypeEmprunte()
    {
        return $this->typeEmprunte;
    }

    /**
     * Retourne la valeur courante de [groupeName].
     *
     * @return string
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-megalis
     *
     * @copyright Atexo 2016
     */
    public function getGroupeName()
    {
        return $this->groupeName;
    }
}
