<?php

namespace Application\Service\Atexo\Consultation\Tree;

use Application\Service\Atexo\Atexo_Util;
use AtexoCrypto\Dto\Consultation;

/**
 * Permet de traiter le dossier ou fichier de l'archive.
 *
 * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Consultation_Tree_Signature extends Atexo_Consultation_Tree_Item
{
    private $signature = '';
    private $emprunte = '';
    private $typeEmprunte = '';
    private $groupeName = '';

    /**
     * Get the value of [signature] column.
     *
     * @return string la signature
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2014
     */
    public function getSignature()
    {
        return $this->signature;
    }

    /**
     * Permet de setter les attribut du Atexo_Consultation_Tree_Item et la valeur de la signature.
     *
     * @param string $_nom le nom, String $_description la description, String $_accessRestriction le type acces,String $_signature la signature, Array $_autresProprietes
     *
     * @return void
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function __construct(
        $_nom,
        $_description,
        $_accessRestriction,
        $_signature,
        $_arrayAutresProprietes = [],
        $_nomDirParent = '',
        $algo = '',
        $hash = '',
        $groupeName = '',
        $dateCreation = ''
    ) {
        parent::__construct(
            $_nom,
            $_description,
            $_accessRestriction,
            $_arrayAutresProprietes,
            $_nomDirParent,
            false,
            $dateCreation
        );

        $this->signature = $_signature;
        $this->emprunte = $hash;
        $this->typeEmprunte = $algo;
        $this->groupeName = $groupeName;
    }

    /**
     * Permet de mettre la signature dans un fichier.
     *
     * @param string $fileToAdd le fichier ou on va stoquer la signature, String $directory le chemin du dossier ou se trouve le fichier
     *
     * @return mixed String le nom du fichier si l'ajout de la signature a reussi si non boolean false
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function generateFile($fileToAdd, $directory)
    {
        if (Atexo_Util::writeFile($directory.'/'.$fileToAdd, stripslashes($this->signature))) {
            return basename($fileToAdd);
        } else {
            return false;
        }
    }

    /**
     * @return string
     */
    public function getEmprunte()
    {
        return $this->emprunte;
    }

    /**
     * @param string $emprunte
     */
    public function setEmprunte($emprunte)
    {
        $this->emprunte = $emprunte;
    }

    /**
     * @return string
     */
    public function getTypeEmprunte()
    {
        return $this->typeEmprunte;
    }

    /**
     * @param string $typeEmprunte
     */
    public function setTypeEmprunte($typeEmprunte)
    {
        $this->typeEmprunte = $typeEmprunte;
    }

    /**
     * @return string
     */
    public function getGroupeName()
    {
        return $this->groupeName;
    }

    /**
     * @param string $groupeName
     */
    public function setGroupeName($groupeName)
    {
        $this->groupeName = $groupeName;
    }
}
