<?php

namespace Application\Service\Atexo\Consultation\Tree;

use App\Service\DocGen\DocGenForArchive;
use App\Service\GenererDocumentModele;
use App\Service\Messec\ArchiveService;
use App\Service\WebServices\WebServicesExec;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonAnnonceJAL;
use Application\Propel\Mpe\CommonAnnonceMoniteur;
use Application\Propel\Mpe\CommonCategorieLot;
use Application\Propel\Mpe\CommonComplement;
use Application\Propel\Mpe\CommonComplementPeer;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationPeer;
use Application\Propel\Mpe\CommonDCE;
use Application\Propel\Mpe\CommonDCEPeer;
use Application\Propel\Mpe\CommonDocumentExterne;
use Application\Propel\Mpe\CommonEnveloppe;
use Application\Propel\Mpe\CommonFichierEnveloppe;
use Application\Propel\Mpe\CommonFichierEnveloppePeer;
use Application\Propel\Mpe\CommonOffrePapier;
use Application\Propel\Mpe\CommonOffres;
use Application\Propel\Mpe\CommonReferentielFormXml;
use Application\Propel\Mpe\CommonRG;
use Application\Propel\Mpe\CommonRGPeer;
use Application\Propel\Mpe\CommonTAttestationsOffres;
use Application\Propel\Mpe\CommonTAttestationsOffresQuery;
use Application\Propel\Mpe\CommonTCandidature;
use Application\Propel\Mpe\CommonTDecisionSelectionEntreprise;
use Application\Propel\Mpe\CommonTDecisionSelectionEntrepriseQuery;
use Application\Propel\Mpe\CommonTDocumentEntreprise;
use Application\Propel\Mpe\CommonTDumeNumero;
use Application\Propel\Mpe\CommonTEntrepriseDocumentVersion;
use Application\Propel\Mpe\CommonTMembreGroupementEntreprise;
use Application\Propel\Mpe\Entreprise;
use Application\Propel\Mpe\Om\BaseCommonConsultationQuery;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_Crypto;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Entreprise;
use Application\Service\Atexo\Atexo_GenerationExcel;
use Application\Service\Atexo\Atexo_Groupement;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_MarchePublicSimplifie;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Chorus\Atexo_Chorus_Echange;
use Application\Service\Atexo\Chorus\Atexo_Chorus_GestionFichierChorus;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Archive;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ArchivePublicite;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Complements;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Dce;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Decision;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Documentexterne;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Responses;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Rg;
use Application\Service\Atexo\Dume\Atexo_Dume_AtexoDume;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Attestation;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_CoffreFort;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Reponses;
use Application\Service\Atexo\Publicite\Atexo_Publicite_AnnonceJAL;
use Application\Service\Atexo\Publicite\Atexo_Publicite_Avis;
use Application\Service\Atexo\Publicite\Atexo_Publicite_Destinataire;
use Exception;
use Logger;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * Classe d'organisation des répertoires de l'archive.
 *
 * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Consultation_Tree_Arborescence extends Atexo_Consultation_Tree_Directory
{
    const DIR_CONSULTATION = 'consultation';
    const DIR_DOSSIER_CONSULTATION = 'dossier_consultation';
    const DIR_PUBLICITES = 'publicites';
    const DIR_MESSAGES_ECHANGES = 'messages_echanges';
    const DIR_REGISTRES = 'registres';
    const DIR_ARCHIVES_DCE = 'archive_dce';
    const DIR_ARCHIVES_RC = 'archive_rc';
    const DIR_PIECES_COMPLEMENTAIRES = 'pieces_complementaires';
    const DIR_DOCUMENTS_EXTERNES = 'documents_externes';
    const DIR_PIECES_JOINTES = 'pieces_jointes';
    const DIR_CANDIDATURES_OFFRES_DECISION = 'candidatures_offres_decision';
    const DIR_CANDIDATURES = 'candidatures';
    const DIR_CANDIDATURES_RETENUES = 'candidatures_retenues';
    const DIR_CANDIDATURES_NON_RETENUES = 'candidatures_non_retenues';
    const DIR_RECEPTION_EXAMEN_OFFRES = 'reception_et_examen_offres';
    const DIR_OUVERTURE_PLIS_ANALYSE = 'ouverture_plis_et_analyse';
    const DIR_OFFRES_NON_RETENUES = 'offres_non_retenues';
    const DIR_REPONSE = 'reponse';
    const DIR_ACTE_ENGAGEMENT = 'acte_engagement';
    const DIR_ENVELOPPE_OFFRE = 'enveloppe_offre';
    const DIR_OFFRES_RETENUES = 'offres_retenues';
    const DIR_DECISIONS = 'decisions';
    const DIR_DOCUMENTS_DECISION = 'documents_decision';
    const DIR_ADMISSIBILITE = 'admissibilite';
    const PREFIX_NOM_ENTREPRISE_OFFRE_RETENUE = 'offre_retenue_entreprise_';
    const PREFIX_NOM_ENTREPRISE_OFFRE_NON_RETENUE = 'offre_non_retenue_entreprise_';
    const DIR_LETTRE_ADMISSION = 'lettre_admission';
    const DIR_DEMANDES_COMPLEMENTS_CANDIDATURES = 'demandes_complements';
    const DIR_DEMANDES_COMPLEMENTS_OFFRES = 'demandes_complements';
    const DIR_REJET_CANDIDATURES = 'rejet';
    const DIR_REJET_OFFRES = 'rejet';
    const DIR_NOTIFICATION_OFFRES = 'notification_acte_engagement_entreprise';
    const DIR_ATTRIBUTION_OFFRES = 'attribution_entreprise';
    const ALGORITHME_HASHAGE = 'sha256';
    const CODE_DEP_TEMPLATE = 'DEP';

    private $consultationId;
    private array $tableNomFichier = [];

    /**
     * permet de creer une arborescence pour une archive de consultation.
     *
     * @param int       $consultationId      la reference de la Consultation
     * @param PropelPDO $connexion
     * @param string    $organisme           l'accronysme de l'organisme
     * @param bool      $useLangageCatalogue mettre a true pour utiliser la langue de la config,sinon false
     * @param int       $lot                 le numero du lot
     *
     * @return void
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function __construct($consultationId, $connexion, $organisme = null, $useLangageCatalogue = true, $lot = 0)
    {
        /**
         * Archive Norme : SEDA
         */
        if (empty(Atexo_Config::getParameter('STRUCTURE_ARCHIVE')) || (Atexo_Config::getParameter('STRUCTURE_ARCHIVE') == '0')) {
            self::generateDefaultTree($consultationId, $connexion, $organisme, $useLangageCatalogue, $lot);
        } else {
            /**
             * Archive pour mégalis
             */
            self::generateTree($consultationId, $connexion, $organisme, $useLangageCatalogue, $lot);
        }
    }

    /**
     * permet d'ajouter l'horodatage (timeStamp) a un fichier.
     *
     * @param string $pathDossier           le chemin du fichier
     * @param string $nom
     * @param string $description
     * @param string $restriction
     * @param string $content
     * @param array  $arrayAutresProprietes
     *
     * @return void
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function addTimeStamp($pathDossier, $nom, $description, $restriction, $content, $arrayAutresProprietes = [])
    {
        $arrayPath = explode('/', $pathDossier);
        $dir = $this->searchItem($arrayPath);
        if ($dir) {
            $timeStampTreeCons = new Atexo_Consultation_Tree_TimeStamp($nom, $description, $restriction, $content, $arrayAutresProprietes);
            $dir->addFils($timeStampTreeCons);
        }
    }

    /**
     * permet d'ajouter un item (fichier) a une arborescence.
     *
     * @param string $pathDossier la reference de la Consultation
     * @param string $nom
     *
     * @return void
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function addItem($pathDossier, Atexo_Consultation_Tree_Item $item)
    {
        $arrayPath = explode('/', $pathDossier);
        $dir = $this->searchItem($arrayPath);
        if ($dir) {
            $dir->addFils($item);

            return 'OK';
        } else {
            return null;
        }
    }

    /**
     * permet d'ajouter un fichier a une arborescence.
     *
     * @param string $pathDossier           le chemin du fichier
     * @param string $nom
     * @param string $description
     * @param string $restriction
     * @param string $idBlob
     * @param array  $arrayAutresProprietes
     *
     * @return void
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function addFile($pathDossier, $nom, $description, $restriction, $idBlob = 0, $arrayAutresProprietes = [], $dateCreation = '', $organisme = null)
    {
        $arrayPath = explode('/', $pathDossier);
        $dir = $this->searchItem($arrayPath);
        if ($dir) {
            $fileTreeCons = new Atexo_Consultation_Tree_File($nom, $description, $restriction, $idBlob, '', $arrayAutresProprietes, '', self::getAlgorithmeHashage(), self::getHashFile(
                $idBlob,
                $organisme,
                false,
                $nom
            ), '', $dateCreation);
            $dir->addFils($fileTreeCons);
        }
    }

    /**
     * permet d'ajouter un fichier d'horodatage a une arborescence.
     *
     * @param string $pathDossier le chemin du fichier
     * @param string $nom
     * @param string $description
     * @param string $restriction
     * @param string $horodatage
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2015
     */
    public function addJetonFile($pathDossier, $nom, $description, $restriction, $horodatage)
    {
        $arrayPath = explode('/', $pathDossier);
        $dir = $this->searchItem($arrayPath);
        if ($dir) {
            $fileTreeCons = new Atexo_Consultation_Tree_TimeStamp($nom, '', '', $horodatage);
            $dir->addFils($fileTreeCons);
        }
    }

    /**
     * permet d'ajouter un dossier une arborescence.
     *
     * @param string $pathDossier           le chemin du fichier
     * @param string $nom
     * @param string $description
     * @param string $restriction
     * @param string $appraisal
     * @param array  $arrayAutresProprietes
     *
     * @return void
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function addFolder($pathDossier, $nom, $description = '', $restriction = '', $appraisal = null, $arrayAutresProprietes = [], $nomDirParent = '', $lot = false)
    {
        $arrayPath = explode('/', $pathDossier);
        $dir = $this->searchItem($arrayPath);
        if ($dir) {
            if ($dir->hasFils($nom)) {
                return;
            }
            $folderTreeCons = new Atexo_Consultation_Tree_Directory($nom, $description, $restriction, $appraisal, $arrayAutresProprietes, $nomDirParent, $lot);
            $dir->addFils($folderTreeCons);
        }
    }

    /**
     * permet d'ajouter un dossier une arborescence.
     *
     * @param string $pathDossier le chemin du fichier
     * @param string $nom
     * @param string $description
     * @param string $restriction
     *
     * @return Atexo_Consultation_Tree_Group $groupe
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function addGroup($pathDossier, $nom, $description, $restriction)
    {
        $arrayPath = explode('/', $pathDossier);
        $dir = $this->searchItem($arrayPath);
        $groupe = new Atexo_Consultation_Tree_Group($nom, $description, $restriction);
        if ($dir) {
            $dir->addFils($groupe);
        }

        return $groupe;
    }

    /**
     * permet d'ajouter un fichier externe a une arborescence.
     *
     * @param string $pathDossier le chemin du fichier
     * @param string $nom
     *
     * @return void
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function addExternFile($pathDossier, $nom)
    {
        $pathDossier .= '/Documents_Externes';
        $this->addFile($pathDossier, $nom);
    }

    /**
     * permet de verifier l'existance d'un fichier dans un emplacement
     * (eviter d'ecraser un fichier si existant).
     *
     * @param string $emplacement
     * @param string $nomFichier
     *
     * @return void
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function verifierNameFile($emplacement, $nomFichier)
    {
        $nomFichier = Atexo_Util::formaterNomFileForZip($nomFichier);
        if ($this->tableNomFichier[$emplacement][$nomFichier]) {
            $fichier = explode('.', $nomFichier);
            $extention = '.' . $fichier[count($fichier) - 1];
            $ext = strlen($extention);
            $nomFile = substr($nomFichier, 0, -$ext);
            $nameFile = $nomFile . '-' . $this->tableNomFichier[$emplacement][$nomFichier] . $extention;
            ++$this->tableNomFichier[$emplacement][$nomFichier];
        } else {
            $nameFile = $nomFichier;
            $this->tableNomFichier[$emplacement][$nomFichier] = 1;
        }

        return $nameFile;
    }

    /**
     * Permet de generer l'arborescence d'une archive. Cette structure est l'arborescence par defaut.
     *
     * @param int       $consultationId      : la reference de la Consultation
     * @param PropelPDO $connexion           : connexion
     * @param string    $organisme           : l'accronysme de l'organisme
     * @param bool      $useLangageCatalogue : precise s'il faut utiliser la langue de la config ou non
     *                                       => true pour utiliser la langue de la config
     *                                       => false sinon
     * @param int       $lot                 le numero du lot
     */
    public function generateDefaultTree($consultationId, $connexion, $organisme = null, $useLangageCatalogue = true, $lot = 0)
    {
        try {
            if ($useLangageCatalogue) {
                Atexo_Languages::setLanguageCatalogue('agent');
            }

            if (!$connexion) {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));
            }

            if (!$organisme) {
                $organisme = Atexo_CurrentUser::getOrganismAcronym();
            }

            //$arrayAutresProprietes
            $arrayAutresProprietes = Atexo_Consultation_Archive::getArrayArchiveConfigTree();

            $this->idConsultation = $consultationId;
            $consultation = CommonConsultationPeer::retrieveByPK($this->idConsultation, $connexion);
            $nomRep = $consultation->getReferenceUtilisateur(true);
            if (Atexo_Module::isEnabled('ArchiveParLot') && $lot) {
                $nomRep .= '_Lot' . $lot;
            }
            parent::__construct($nomRep, '', '', '', $arrayAutresProprietes);

            // Arborescence des dossiers
            $arrayTopDirectories = Atexo_Consultation_Archive::getPathsDoc();
            $arrayDecriptionsDirectories = Atexo_Consultation_Archive::getDescriptionDoc();
            $arrayDecriptionsDirectoriesExternes = Atexo_Consultation_Archive::getDescriptionDoc(true);
            $arrayAccessDirectories = Atexo_Consultation_Archive::getAccessDoc();
            $arrayAppraisal = Atexo_Consultation_Archive::getAppraisalDoc();
            foreach ($arrayTopDirectories as $type => $nom) {
                $topDir = new Atexo_Consultation_Tree_Directory($nom, $arrayDecriptionsDirectories[$type], $arrayAccessDirectories[$type], $arrayAppraisal[$type], $arrayAutresProprietes);
                $this->addFils($topDir);
            }

            $nameArchiveDce = 'archive_dce';
            $nameArchiveRc = 'archive_rc';
            $nameArchiveComp = 'archive_complements';
            $docDescriptionDce = '';
            $docDescriptionRc = '';
            $docDescriptionComp = '';
            if (is_countable($arrayAutresProprietes['OldVersion']['TYPE_DOC_EXTERNE_CONSULTATION']) ? count($arrayAutresProprietes['OldVersion']['TYPE_DOC_EXTERNE_CONSULTATION']) : 0) {
                $docDescriptionDce = Atexo_Util::toUtf8($arrayAutresProprietes['OldVersion']['TYPE_DOC_EXTERNE_CONSULTATION']['Dce']);
                $docDescriptionRc = Atexo_Util::toUtf8($arrayAutresProprietes['OldVersion']['TYPE_DOC_EXTERNE_CONSULTATION']['Rc']);
                $docDescriptionComp = Atexo_Util::toUtf8($arrayAutresProprietes['OldVersion']['TYPE_DOC_EXTERNE_CONSULTATION']['Complement']);
            }
            $this->addFolder(
                Atexo_Config::getParameter('DIR_DOC_EXTERNE_CONSULTATION'),
                $nameArchiveRc,
                $docDescriptionRc,
                Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_1'),
                $arrayAppraisal[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_CONSULTATION')]
            );
            $this->addFolder(
                Atexo_Config::getParameter('DIR_DOC_EXTERNE_CONSULTATION'),
                $nameArchiveDce,
                $docDescriptionDce,
                Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_1'),
                $arrayAppraisal[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_CONSULTATION')]
            );
            $this->addFolder(
                Atexo_Config::getParameter('DIR_DOC_EXTERNE_CONSULTATION'),
                $nameArchiveComp,
                $docDescriptionComp,
                Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_1'),
                $arrayAppraisal[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_CONSULTATION')]
            );

            // === Récupération des fichier "internes" ===
            $arrayPropFiles = $arrayAutresProprietes['Files']['TYPE_DOC_EXTERNE_CONSULTATION']['Contains'];
            $arrayPropGroup = $arrayAutresProprietes['Groups']['TYPE_DOC_EXTERNE_CONSULTATION']['Contains'];
            $arrayPropGroup['dossier'] = 'TYPE_DOC_EXTERNE_CONSULTATION';
            // DCEs :
            $arrayPropDce = $arrayPropFiles[0]['Document'];
            $arrayPropDceOldVers = $arrayPropFiles[2]['Contains']['Document'];
            $c = new Criteria();
            $c->addDescendingOrderByColumn(CommonDCEPeer::ID);
            $dces = (new Atexo_Consultation_Dce())->getDces($consultation->getId(), $consultation->getOrganisme(), $c);
            if ($dces) {
                $lastDCE = $dces[0];
                $group = new Atexo_Consultation_Tree_Group(
                    'DCE',
                    Prado::localize('DECRIPTION_DOCUMENT_DE_CONSULTATION_ENTREPRISES'),
                    Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_1'),
                    $arrayPropGroup[0]
                );
                $this->addItem(Atexo_Config::getParameter('DIR_DOC_EXTERNE_CONSULTATION'), $group);
                $nomFichier = self::verifierNameFile(Atexo_Config::getParameter('DIR_DOC_EXTERNE_CONSULTATION'), Atexo_Util::OterAccents($lastDCE->getNomDce()));
                $atexoConsTreeFile = new Atexo_Consultation_Tree_File(
                    stripslashes($nomFichier),
                    'fichier du ' . Atexo_Util::iso2frnDateTimeUnderScore($lastDCE->getUntrusteddate()) . ' du DCE',
                    '',
                    $lastDCE->getDce(),
                    '',
                    $arrayPropDce[0]
                );
                $group->addFils($atexoConsTreeFile);
                $atexoConsTreeTimeStamp = new Atexo_Consultation_Tree_TimeStamp(stripslashes($nomFichier) . ' - Jeton - ' .
                    Atexo_Util::iso2frnDateTimeNoSep($lastDCE->getUntrusteddate()) . '.tsr', "Jeton d'horodatage", '', $lastDCE->getHorodatage(), $arrayPropDce[1]);
                $group->addFils($atexoConsTreeTimeStamp);
                //$this->addItem(Atexo_Config::getParameter('DIR_DOC_EXTERNE_CONSULTATION'), $group);

                //on dépile le premier DCE de la consultation (le plus récent)
                array_shift($dces);

                //pour des questions d'optimisations de poids des archives, on ne mets dans archive_dce que l'avant dernière version du DCE
                if ((is_countable($dces) ? count($dces) : 0) > 0) {
                    $dce = $dces[0];
                    $group = new Atexo_Consultation_Tree_Group(
                        'dce',
                        Prado::localize('ARCHIVE_DECRIPTION_DOCUMENT_DE_CONSULTATION_ENTREPRISES'),
                        Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_2'),
                        $arrayPropGroup[2]
                    );
                    $path = Atexo_Config::getParameter('DIR_DOC_EXTERNE_CONSULTATION') . "/$nameArchiveDce";
                    $this->addItem($path, $group);
                    $nomFichier = self::verifierNameFile($path, Atexo_Util::OterAccents($dce->getNomDce()));
                    $atexoConsTreeFileDce = new Atexo_Consultation_Tree_File(
                        stripslashes($nomFichier),
                        'fichier du ' . Atexo_Util::iso2frnDateTimeUnderScore($dce->getUntrusteddate()) . ' du DCE',
                        '',
                        $dce->getDce(),
                        '',
                        $arrayPropDceOldVers[0]
                    );
                    $group->addFils($atexoConsTreeFileDce);
                    $atexoConsTreeTimeStampDce = new Atexo_Consultation_Tree_TimeStamp(
                        stripslashes($nomFichier) . ' - Jeton - ' . Atexo_Util::iso2frnDateTimeNoSep($dce->getUntrusteddate()) . '.tsr',
                        "Jeton d'horodatage",
                        '',
                        $dce->getHorodatage(),
                        $arrayPropDceOldVers[1]
                    );
                    $group->addFils($atexoConsTreeTimeStampDce);
                }
            }

            if (Atexo_Module::isEnabled('interfaceDume')) {
                if (1 == $consultation->getDumeDemande()) {
                    //DUME Acheteur
                    $arrayPropDume = $arrayPropFiles[1]['Document'];
                    $tDumeNumeros = Atexo_Dume_AtexoDume::getAncienTDumeNumeros($consultation);
                    if (is_array($tDumeNumeros) && !empty($tDumeNumeros)) {
                        $group = new Atexo_Consultation_Tree_Group(
                            'DUME_ACHETEUR',
                            Prado::localize('ARCHIVE_DECRIPTION_DUME_ACHETEUR'),
                            Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_1'),
                            $arrayPropGroup[1]
                        );
                        $this->addItem(Atexo_Config::getParameter('DIR_DOC_EXTERNE_CONSULTATION') . "/$nameArchiveDce", $group);
                        $indexDume = 0;
                        $u = new Atexo_Util();
                        foreach ($tDumeNumeros as $dumeNumero) {
                            if ($dumeNumero instanceof CommonTDumeNumero && $dumeNumero->getNumeroDumeNational() && $dumeNumero->getBlobId()) {
                                $nomFileDume = $dumeNumero->getNumeroDumeNational() . '.pdf';
                                $nomFichier = self::verifierNameFile(Atexo_Config::getParameter('DIR_DOC_EXTERNE_CONSULTATION'), $u->OterAccents($nomFileDume));
                                $fileTreeDume = new Atexo_Consultation_Tree_File(stripslashes($nomFichier), 'DUME Acheteur', '', $dumeNumero->getBlobId(), '', $arrayPropDume[$indexDume++]);
                                $group->addFils($fileTreeDume);
                            }
                        }
                    }
                }
            }

            // RCs :
            $arrayPropRc = $arrayPropFiles[1]['Document'];
            $arrayPropOldRc = $arrayPropFiles[3]['Contains']['Document'];
            $c = new Criteria();
            $c->addDescendingOrderByColumn(CommonRGPeer::ID);
            $rcs = (new Atexo_Consultation_Rg())->getRgs($consultation->getId(), $consultation->getOrganisme(), $c);
            if ($rcs) {
                $lastRC = $rcs[0];
                $group = new Atexo_Consultation_Tree_Group(
                    'Reglement_de_consultation',
                    Prado::localize('DECRIPTION_REGLEMENT_CONSULTATION'),
                    Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_1'),
                    $arrayPropGroup[1]
                );
                $this->addItem(Atexo_Config::getParameter('DIR_DOC_EXTERNE_CONSULTATION'), $group);
                $nomFichier = self::verifierNameFile(Atexo_Config::getParameter('DIR_DOC_EXTERNE_CONSULTATION'), Atexo_Util::OterAccents($lastRC->getNomFichier()));
                $fileTreeRC = new Atexo_Consultation_Tree_File(stripslashes($nomFichier), 'fichier du ' . Atexo_Util::iso2frnDateTimeUnderScore($lastRC->getUntrusteddate()) .
                    ' du RC', '', $lastRC->getRg(), '', $arrayPropRc[0]);
                $group->addFils($fileTreeRC);
                $TimeStampTreeRC = new Atexo_Consultation_Tree_TimeStamp(stripslashes($nomFichier) . ' - Jeton - ' . Atexo_Util::iso2frnDateTimeNoSep($lastRC->getUntrusteddate()) .
                    '.tsr', '', '', $lastRC->getHorodatage(), $arrayPropRc[1]);
                $group->addFils($TimeStampTreeRC);
                array_shift($rcs);
                foreach ($rcs as $rc) {
                    $group = new Atexo_Consultation_Tree_Group(
                        'Reglement_de_consultation',
                        Prado::localize('ARCHIVE_DECRIPTION_REGLEMENT_CONSULTATION'),
                        Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_1'),
                        $arrayPropGroup[3]
                    );
                    $path = Atexo_Config::getParameter('DIR_DOC_EXTERNE_CONSULTATION') . "/$nameArchiveRc";
                    $this->addItem($path, $group);
                    $nomFichier = self::verifierNameFile($path, Atexo_Util::OterAccents($rc->getNomFichier()));
                    $fileTreeRCHistorique = new Atexo_Consultation_Tree_File(stripslashes($nomFichier), 'fichier du ' .
                        Atexo_Util::iso2frnDateTimeUnderScore($rc->getUntrusteddate()) . ' du RC', '', $rc->getRg(), '', $arrayPropOldRc[0]);
                    $group->addFils($fileTreeRCHistorique);
                    $timeStampTreeRCHistorique = new Atexo_Consultation_Tree_TimeStamp(stripslashes($nomFichier) . ' - Jeton - ' .
                        Atexo_Util::iso2frnDateTimeNoSep($rc->getUntrusteddate()) . '.tsr', '', '', $rc->getHorodatage(), $arrayPropOldRc[1]);
                    $group->addFils($timeStampTreeRCHistorique);
                }
            }

            // Complements :
            $c = new Criteria();
            $c->addDescendingOrderByColumn(CommonComplementPeer::ID);
            $complements = (new Atexo_Consultation_Complements())->getComplements($consultation->getId(), $consultation->getOrganisme(), $c);
            if ($complements) {
                $lastComplement = $complements[0];
                $group = new Atexo_Consultation_Tree_Group('Compléments', Prado::localize('DESCRIPTION_COMPLEMENTS'), Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_1'), $arrayPropGroup[4]);
                $this->addItem(Atexo_Config::getParameter('DIR_DOC_EXTERNE_CONSULTATION'), $group);
                $nomFichier = self::verifierNameFile(Atexo_Config::getParameter('DIR_DOC_EXTERNE_CONSULTATION'), Atexo_Util::OterAccents($lastComplement->getNomFichier()));
                $fileTreeCP = new Atexo_Consultation_Tree_File(stripslashes($nomFichier), 'fichier du ' .
                    Atexo_Util::iso2frnDateTimeUnderScore($lastComplement->getUntrusteddate()), '', $lastComplement->getComplement());
                $group->addFils($fileTreeCP);
                $timeStampTreeCP = new Atexo_Consultation_Tree_TimeStamp(stripslashes($nomFichier) . ' - Jeton - ' .
                    Atexo_Util::iso2frnDateTimeNoSep($lastComplement->getUntrusteddate()) . '.tsr', '', '', $lastComplement->getHorodatage());
                $group->addFils($timeStampTreeCP);

                array_shift($complements);
                foreach ($complements as $complement) {
                    $path = Atexo_Config::getParameter('DIR_DOC_EXTERNE_CONSULTATION') . "/$nameArchiveComp";
                    $nomFichier = self::verifierNameFile($path, Atexo_Util::OterAccents($complement->getNomFichier()));
                    if ('MEGALIS' == strtoupper(Atexo_Config::getParameter('PROFIL_ARCHIVE'))) {
                        $this->addFile(
                            $path,
                            stripslashes($nomFichier),
                            'fichier du ' . Atexo_Util::iso2frnDateTimeUnderScore($complement->getUntrusteddate()),
                            '',
                            $complement->getComplement()
                        );

                        $this->addJetonFile(
                            $path,
                            stripslashes($nomFichier) . ' - Jeton - ' .
                            Atexo_Util::iso2frnDateTimeNoSep($complement->getUntrusteddate()) . '.tsr',
                            '',
                            '',
                            $complement->getHorodatage()
                        );
                    } else {
                        $group = new Atexo_Consultation_Tree_Group(
                            'Compléments',
                            Prado::localize('DECRIPTION_ARCHIVE_COMPLEMENTS'),
                            Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_1'),
                            $arrayPropGroup[4]
                        );
                        $this->addItem($path, $group);
                        $fileTreeCPHist = new Atexo_Consultation_Tree_File(stripslashes($nomFichier), 'fichier du ' .
                            Atexo_Util::iso2frnDateTimeUnderScore($complement->getUntrusteddate()), '', $complement->getComplement());
                        $group->addFils($fileTreeCPHist);
                        $timeStimpTreeCPHist = new Atexo_Consultation_Tree_TimeStamp(stripslashes($nomFichier) . ' - Jeton - ' .
                            Atexo_Util::iso2frnDateTimeNoSep($complement->getUntrusteddate()) . '.tsr', '', '', $complement->getHorodatage());
                        $group->addFils($timeStimpTreeCPHist);
                    }
                }
            }

            if (Atexo_Module::isEnabled('interfaceDume')) {
                if (1 == $consultation->getDumeDemande()) {
                    //DUME Acheteur
                    $arrayPropDume = $arrayPropFiles[1]['Document'];
                    $tDumeNumeros = Atexo_Dume_AtexoDume::getTDumeNumeros($consultation);
                    if (is_array($tDumeNumeros) && !empty($tDumeNumeros)) {
                        $group = new Atexo_Consultation_Tree_Group(
                            'DUME_ACHETEUR',
                            Prado::localize('ARCHIVE_DECRIPTION_DUME_ACHETEUR'),
                            Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_1'),
                            $arrayPropGroup[1]
                        );
                        $this->addItem(Atexo_Config::getParameter('DIR_DOC_EXTERNE_CONSULTATION'), $group);
                        $indexDume = 0;
                        $u = new Atexo_Util();
                        foreach ($tDumeNumeros as $dumeNumero) {
                            if ($dumeNumero instanceof CommonTDumeNumero && $dumeNumero->getNumeroDumeNational() && $dumeNumero->getBlobId()) {
                                $nomFileDume = $dumeNumero->getNumeroDumeNational() . '.pdf';
                                $nomFichier = self::verifierNameFile(Atexo_Config::getParameter('DIR_DOC_EXTERNE_CONSULTATION'), $u->OterAccents($nomFileDume));
                                $fileTreeDume = new Atexo_Consultation_Tree_File(stripslashes($nomFichier), 'DUME Acheteur', '', $dumeNumero->getBlobId(), '', $arrayPropDume[$indexDume++]);
                                $group->addFils($fileTreeDume);
                            }
                        }
                    }
                }
            }
            //Files de registres
            $arrayPropFilesRegistres = $arrayAutresProprietes['Files']['TYPE_DOC_EXTERNE_REGISTRES'];
            $arrayPropGroupsRegistres = $arrayAutresProprietes['Groups']['TYPE_DOC_EXTERNE_REGISTRES'];
            $arrayPropGroupsRegistres['dossier'] = 'TYPE_DOC_EXTERNE_REGISTRES';
            // Registres des questions électroniques :
            $pathResgistre = (new Atexo_GenerationExcel())->generateExcelRegistre($consultationId, Atexo_Config::getParameter('REGISTRE_QUESTION_ELECTRONIQUE'), true, $organisme, false);
            $nomRegistre = Atexo_Util::OterAccents(basename($pathResgistre));
            $group = new Atexo_Consultation_Tree_Group(
                'Questions',
                Prado::localize('DESCRIPTION_REGISTRE_QUESTION'),
                Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_2'),
                $arrayAutresProprietes
            );
            $this->addItem(Atexo_Config::getParameter('DIR_DOC_EXTERNE_REGISTRES'), $group);
            $nomRegistre = self::verifierNameFile(Atexo_Config::getParameter('DIR_DOC_EXTERNE_REGISTRES'), $nomRegistre);
            $registeQuestionTree = new Atexo_Consultation_Tree_File(
                stripslashes($nomRegistre),
                Prado::localize('DESCRIPTION_REGISTRE_QUESTION_FILE'),
                Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_2'),
                $pathResgistre,
                '',
                $arrayPropFilesRegistres['Contains'][1]['Document']
            );
            $group->addFils($registeQuestionTree);

            // Registres des retraits :
            $pathResgistre = (new Atexo_GenerationExcel())->generateExcelRegistre($consultationId, Atexo_Config::getParameter('REGISTRE_RETRAIT_ELECTRONIQUE'), true, $organisme, false);
            $nomRegistre = Atexo_Util::OterAccents(basename($pathResgistre));
            $group = new Atexo_Consultation_Tree_Group(
                'Retraits',
                Prado::localize('DESCRIPTION_REGISTRE_RETRAIT'),
                Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_2'),
                $arrayPropGroupsRegistres['Contains'][0]
            );
            $this->addItem(Atexo_Config::getParameter('DIR_DOC_EXTERNE_REGISTRES'), $group);
            $nomRegistre = self::verifierNameFile(Atexo_Config::getParameter('DIR_DOC_EXTERNE_REGISTRES'), $nomRegistre);
            $registeRetraitTree = new Atexo_Consultation_Tree_File(
                stripslashes($nomRegistre),
                Prado::localize('DESCRIPTION_REGISTRE_RETRAIT_FILE'),
                Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_2'),
                $pathResgistre,
                '',
                $arrayPropFilesRegistres['Contains'][0]['Document']
            );
            $group->addFils($registeRetraitTree);

            // Registres des dépots
            $pathResgistre = (new Atexo_GenerationExcel())->generateExcelRegistre($consultationId, Atexo_Config::getParameter('REGISTRE_DEPOT_ELECTRONIQUE'), true, $organisme, false);
            $nomRegistre = Atexo_Util::OterAccents(basename($pathResgistre));
            $group = new Atexo_Consultation_Tree_Group(
                'Dépôts',
                Prado::localize('DESCRIPTION_REGISTRE_DEPOT'),
                Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_2'),
                $arrayAutresProprietes
            );
            $this->addItem(Atexo_Config::getParameter('DIR_DOC_EXTERNE_REGISTRES'), $group);
            $nomRegistre = self::verifierNameFile(Atexo_Config::getParameter('DIR_DOC_EXTERNE_REGISTRES'), $nomRegistre);
            $registeDepottTree = new Atexo_Consultation_Tree_File(
                stripslashes($nomRegistre),
                Prado::localize('DESCRIPTION_REGISTRE_DEPOT_FILE'),
                Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_2'),
                $pathResgistre,
                '',
                $arrayPropFilesRegistres['Contains'][2]['Document']
            );
            $group->addFils($registeDepottTree);

            // Registre suivi de messages
            $result = Atexo_Message::getAllEchangesDestinataireByIdRef($consultationId, $organisme, null, true, false);
            $pathResgistre = (new Atexo_GenerationExcel())->generateExcelRegistreEchange($consultationId, $result, true, $organisme, false, true);
            $nomRegistre = Atexo_Util::OterAccents(basename($pathResgistre));
            $group = new Atexo_Consultation_Tree_Group('Suivi des messages', Prado::localize('DESCRIPTION_REGISTRE_SUIVI_MESSAGE'), Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_2'));
            $this->addItem(Atexo_Config::getParameter('DIR_DOC_EXTERNE_REGISTRES'), $group);
            $nomRegistre = self::verifierNameFile(Atexo_Config::getParameter('DIR_DOC_EXTERNE_REGISTRES'), $nomRegistre);
            $registeMessageTree = new Atexo_Consultation_Tree_File(
                stripslashes($nomRegistre),
                Prado::localize('DESCRIPTION_REGISTRE_SUIVI_MESSAGE_FILE'),
                Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_2'),
                $pathResgistre,
                '',
                $arrayPropFilesRegistres['Contains'][3]['Document']
            );
            $group->addFils($registeMessageTree);

            // Messages et pièces jointes aux messages
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
            $c = new Criteria();
            $c->add(CommonConsultationPeer::ID, $consultationId);
            $c->add(CommonConsultationPeer::ORGANISME, $organisme);
            $commonConsultation = CommonConsultationPeer::doSelectOne($c, $connexion);

            $arrayPropGroupEchanges = $arrayAutresProprietes['Groups']['TYPE_DOC_EXTERNE_MESSAGE_ECHANGE']['Contains'];
            $arrayPropGroupEchanges['dossier'] = 'TYPE_DOC_EXTERNE_MESSAGE_ECHANGE';

            if($commonConsultation instanceof  CommonConsultation && $commonConsultation->getVersionMessagerie() == 2){
                $service = Atexo_Util::getSfService(ArchiveService::class);
                $echanges = $service->getMessecEchanges($consultationId);

                $Messges = (new Atexo_Message())->generateMessageEchangeFilesForMessec($echanges);

                foreach ($Messges as $key => $message) {
                    $dossier = Atexo_Config::getParameter('DIR_DOC_EXTERNE_MESSAGE_ECHANGE');
                    $idMessage = $key;
                    $dateMessage = substr($message['DateEchange'], '0', '10');
                    $ssDossier = 'Echange_' . $idMessage . '_' . $dateMessage;

                    //Création du dossier Message
                    $this->addFolder(
                        $dossier,
                        $ssDossier,
                        Prado::localize('DESCRIPTION_MESSAGES_PIECES_JOINTES'),
                        Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_2'),
                        $arrayAppraisal[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_MESSAGE_ECHANGE')]
                    );

                    //Création des sous dossiers Message
                    $group = new Atexo_Consultation_Tree_Group('Message', Prado::localize('DESCRIPTION_MESSAGE_DIC_DAC'), Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_2'));
                    $this->addItem($dossier . '/' . $ssDossier, $group);

                    //Création des pièces jointes
                    if (!empty($message['PDF_ECHANGE'])) {
                        $pdf = $message['PDF_ECHANGE'];

                        $group = new Atexo_Consultation_Tree_Group(
                            'PieceJointe',
                            Prado::localize('DESCRIPTION_MESSAGES_PIECES_JOINTES'),
                            Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_2')
                        );
                        $path = $dossier . '/' . $ssDossier;
                        $this->addItem($path, $group);
                        $nomPj = self::verifierNameFile($path, Atexo_Util::OterAccents($pdf[1]));
                        $pjMessageTree = new Atexo_Consultation_Tree_File(
                            stripslashes($nomPj),
                            Prado::localize('DESCRIPTION_MESSAGES_PIECES_JOINTES'),
                            Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_2'),
                            $pdf[0]
                        );
                        $group->addFils($pjMessageTree);
                    }

                    for ($i = 1; $i <= 2; ++$i) {
                        $nomCollection = 'PJ_MESSAGE_' . $i;
                        $messagesCollection = $message[$nomCollection];
                        if (!empty($messagesCollection)) {
                            $this->addFolder(
                                $dossier . '/' . $ssDossier,
                                $nomCollection,
                                Prado::localize('DESCRIPTION_PIECES_JOINTES_DIC_DAC'),
                                Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_2'),
                                $arrayAppraisal[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_MESSAGE_ECHANGE')]
                            );

                            if (is_array($messagesCollection) && !empty($messagesCollection)) {
                                foreach ($messagesCollection as $pj) {

                                    $u = new Atexo_Util();
                                    $pathFileZip = $u->clean($u->OterAccents($pj[0]));
                                    $explodePath = explode('/', $pathFileZip);
                                    $lastKey = array_key_last($explodePath);
                                    $pathFileZip = str_ireplace($explodePath[$lastKey], '', $pathFileZip);
                                    $nameFileZip = preg_replace(
                                        '/([^.a-z0-9]+)/i',
                                        '_',
                                        $explodePath[$lastKey]
                                    );
                                    $pj[0] = $pathFileZip.$nameFileZip;

                                    $nomPjZip = self::verifierNameFile($path, Atexo_Util::OterAccents($pj[1]));
                                    $pj[1] = preg_replace('/([^.a-z0-9]+)/i', '_', $nomPjZip);

                                    $group = new Atexo_Consultation_Tree_Group(
                                        'PieceJointe',
                                        Prado::localize('DESCRIPTION_MESSAGES_PIECES_JOINTES'),
                                        Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_2')
                                    );
                                    $path = $dossier . '/' . $ssDossier . '/' . $nomCollection;
                                    $this->addItem($path, $group);
                                    $nomPj = self::verifierNameFile($path, Atexo_Util::OterAccents($pj[1]));
                                    $pjMessageTree = new Atexo_Consultation_Tree_File(
                                        stripslashes($nomPj),
                                        Prado::localize('DESCRIPTION_MESSAGES_PIECES_JOINTES'),
                                        Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_2'),
                                        $pj[0]
                                    );
                                    $group->addFils($pjMessageTree);
                                }
                            }
                        }
                    }
                }
            } else {
                $Messges = (new Atexo_Message())->generateMessageEchangeFiles($result, $organisme);
                foreach ($Messges as $key => $message) {
                    $dossier = Atexo_Config::getParameter('DIR_DOC_EXTERNE_MESSAGE_ECHANGE');
                    $idMessage = $key;
                    $dateMessage = substr($message['DateEchange'], '0', '10');
                    $ssDossier = 'Echange_' . $idMessage . '_' . $dateMessage;

                    //Création du dossier Message
                    $this->addFolder(
                        $dossier,
                        $ssDossier,
                        Prado::localize('DESCRIPTION_MESSAGES_PIECES_JOINTES'),
                        Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_2'),
                        $arrayAppraisal[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_MESSAGE_ECHANGE')]
                    );

                    //Création des sous dossiers Message
                    $group = new Atexo_Consultation_Tree_Group('Message', Prado::localize('DESCRIPTION_MESSAGE_DIC_DAC'), Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_2'));
                    $this->addItem($dossier . '/' . $ssDossier, $group);
                    $contentMessageTree = new Atexo_Consultation_Tree_File(
                        'MessageEchange.txt',
                        Prado::localize('DESCRIPTION_MESSAGES_PIECES_JOINTES'),
                        Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_2'),
                        $message['MessageEchangeFile']
                    );
                    $group->addFils($contentMessageTree);

                    //Création des pièces jointes
                    $this->addFolder(
                        $dossier . '/' . $ssDossier,
                        'Pieces_jointes',
                        Prado::localize('DESCRIPTION_PIECES_JOINTES_DIC_DAC'),
                        Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_2'),
                        $arrayAppraisal[Atexo_Config::getParameter('TYPE_DOC_EXTERNE_MESSAGE_ECHANGE')]
                    );
                    $pathPJ = $message['PiecesJointes'];
                    if ($pathPJ) {
                        foreach ($pathPJ as $onePJ) {
                            $group = new Atexo_Consultation_Tree_Group(
                                'PieceJointe',
                                Prado::localize('DESCRIPTION_MESSAGES_PIECES_JOINTES'),
                                Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_2')
                            );
                            $path = $dossier . '/' . $ssDossier . '/' . 'Pieces_jointes';
                            $this->addItem($path, $group);
                            $nomPj = self::verifierNameFile($path, Atexo_Util::OterAccents($onePJ[1]));
                            $pjMessageTree = new Atexo_Consultation_Tree_File(
                                stripslashes($nomPj),
                                Prado::localize('DESCRIPTION_MESSAGES_PIECES_JOINTES'),
                                Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_2'),
                                $onePJ[0]
                            );
                            $group->addFils($pjMessageTree);
                        }
                    }
                }
            }

            // Candidatures :

            //Appel de la fct qui retourne les offres électroniques
            $offresElectroniques = $consultation->getOffres();
            //Appel de la fct qui retourne les offres papiers
            $offresPapiers = $consultation->getOffrePapiers();

            //fusion des deux tableaux
            $offres = array_merge($offresElectroniques, $offresPapiers);
            $idsOffesWithContrat = $this->getOffresWithContrat($consultation->getId());

            foreach ($offres as $offre) {
                $idEntrep = '';
                $nomEntrep = '';
                if ($offre instanceof CommonOffres) {
                    $idEntrep = $offre->getEntrepriseId();
                    $nomEntrep = Atexo_Entreprise::getNomEntreprise($idEntrep, null);
                } else {
                    //instanceof OffrePapier
                    $entreprise = (new Atexo_Entreprise())->retrieveCompanyBdeBySiren(substr($offre->getSiret(), 0, 9));
                    if ($entreprise instanceof Entreprise) {
                        $idEntrep = $entreprise->getId();
                    }
                    $nomEntrep = $offre->getNomEntreprise();
                }
                $nomEntrep = Atexo_Util::toUtf8($nomEntrep);
                $numeroEntrep = $offre->getNumeroReponse();
                $idOffre = $offre->getId();
                if ($offre instanceof CommonOffres) {
                    $typeDepot = Atexo_Config::getParameter('DEPOT_ELECTRONIQUE');
                } else {
                    $typeDepot = Atexo_Config::getParameter('DEPOT_PAPIER');
                }

                $selectionEntrepriseQuery = new CommonTDecisionSelectionEntrepriseQuery();
                //echo $idOffre.",".$organisme.",".$typeDepot.",".$lot;exit;
                if (Atexo_Module::isEnabled('ArchiveParLot') && $lot) {
                    $selectionEse = $selectionEntrepriseQuery->getDecisionSelectionEntrepriseByOffre($idOffre, $organisme, $typeDepot, $lot);
                } else {
                    $selectionEse = $selectionEntrepriseQuery->getDecisionSelectionEntrepriseByOffre($idOffre, $organisme, $typeDepot);
                }
                $arrayPropGroupsCand = [];
                $arrayPropFilesCand = $arrayAutresProprietes['Files']['TYPE_DOC_EXTERNE_CANDIDATURES_RET'];
                $propFilesCand = $arrayPropFilesCand['Contains']['Contains'][1]['Document'];
                $propFilesCandAe = $arrayPropFilesCand['Contains']['Contains'][0]['Document'];

                if (in_array($offre->getId(), $idsOffesWithContrat) || $selectionEse instanceof CommonTDecisionSelectionEntreprise) { // Si offre retenue :
                    $dossier = Atexo_Config::getParameter('DIR_DOC_EXTERNE_CANDIDATURES_RET');
                    $typeDossier = Atexo_Config::getParameter('TYPE_DOC_EXTERNE_CANDIDATURES_RET');
                    $arrayPropGroupsCand = $arrayAutresProprietes['Groups']['TYPE_DOC_EXTERNE_CANDIDATURES_RET'];
                    $arrayPropGroupsCand['dossier'] = 'TYPE_DOC_EXTERNE_CANDIDATURES_RET';
                } else {
                    $dossier = Atexo_Config::getParameter('DIR_DOC_EXTERNE_CANDIDATURE_NON_RET');
                    $typeDossier = Atexo_Config::getParameter('TYPE_DOC_EXTERNE_CANDIDATURE_NON_RET');
                    $arrayPropGroupsCand = $arrayAutresProprietes['Groups']['TYPE_DOC_EXTERNE_CANDIDATURE_NON_RET'];
                    $arrayPropGroupsCand['dossier'] = 'TYPE_DOC_EXTERNE_CANDIDATURE_NON_RET';
                }

                if ($offre instanceof CommonOffres) {
                    $cheminDossierReponse = Prado::localize('DEFINE_TEXT_STATISTIQUES_ENTREPRISE') . '_' . $numeroEntrep . '_El';
                } else {
                    $cheminDossierReponse = Prado::localize('DEFINE_TEXT_STATISTIQUES_ENTREPRISE') . '_' . $numeroEntrep . '_Pa';
                }

                $this->addFolder(
                    $dossier,
                    $cheminDossierReponse,
                    "Réponses de l'entreprise " . $nomEntrep,
                    Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_2'),
                    $arrayAppraisal[$typeDossier]
                );
                if ($offre instanceof CommonOffres) {
                    $auMoinsUneEnvOuvert = false;
                        $enveloppes = $offre->getCommonEnveloppes();

                        $xmlStr = $offre->getXmlString();
                        $pos = strpos($xmlStr, 'typeSignature');
                        if ($pos > 0) {
                            $typeSignature = 'XML';
                        }

                        $numeroFichier = 1;
                        //regroupement de tous les AE des lots
                        foreach ($enveloppes as $enveloppe) {
                        if (
                            $enveloppe instanceof CommonEnveloppe
                            && in_array(
                                $enveloppe->getStatutEnveloppe(),
                                [
                                    Atexo_Config::getParameter('STATUT_ENV_OUVERTE_EN_LIGNE'),
                                    Atexo_Config::getParameter('STATUT_ENV_OUVERTE_HORS_LIGNE'),
                                    Atexo_Config::getParameter('STATUT_ENV_OUVERTE'),
                                    Atexo_Config::getParameter('STATUT_ENV_OUVERTE_A_DISTANCE'),

                                ]
                            )
                        ) {
                            $auMoinsUneEnvOuvert = true;
                            $c = new Criteria();
                            $c->add(CommonFichierEnveloppePeer::ID_ENVELOPPE, $enveloppe->getIdEnveloppeElectro());
                            $c->add(CommonFichierEnveloppePeer::ORGANISME, $organisme);
                            $fichiers = CommonFichierEnveloppePeer::doSelect($c, $connexion);

                            foreach ($fichiers as $fichier) {
                                $nomFichier = str_pad($numeroFichier, 2, '0', STR_PAD_LEFT) . '-' . Atexo_Util::OterAccents($fichier->getNomFichier());
                                if ('ACE' == $fichier->getTypeFichier() || 'AE' == $fichier->getTypeFichier()) {
                                    $group = new Atexo_Consultation_Tree_Group(
                                        Prado::localize('NOM_ARCHIVE_FICHIER_ENVELOPPE'),
                                        Prado::localize('DESCRIPTION_ARCHIVE_FICHIER_AE'),
                                        Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_1'),
                                        $arrayPropGroupsCand['Contains']['Contains'][0]
                                    );
                                    $this->addItem($dossier . '/' . $cheminDossierReponse, $group);

                                    $nomFichier = self::verifierNameFile($dossier . '/' . $cheminDossierReponse, $nomFichier);
                                    $blobFichierEnveloppeTree = new Atexo_Consultation_Tree_File(
                                        stripslashes($nomFichier),
                                        Prado::localize('DEFINE_FICHIER'),
                                        'Fichier',
                                        $fichier->getIdBlob(),
                                        '',
                                        $propFilesCandAe[0]
                                    );
                                    $group->addFils($blobFichierEnveloppeTree);
                                    if ('' != $fichier->getSignatureFichier()) {
                                        $signatureFichierEnveloppeTree = new Atexo_Consultation_Tree_Signature(
                                            $fichier->getJetonFileName(false, $nomFichier) . '.sig',
                                            'Signature',
                                            '',
                                            $fichier->getSignatureFichier(),
                                            $propFilesCandAe[1]
                                        );
                                        $group->addFils($signatureFichierEnveloppeTree);
                                    }
                                }

                                ++$numeroFichier;
                            }
                        }
                    }
                        //regroupement de tous les fichiers de réponses des lots
                        foreach ($enveloppes as $enveloppe) {
                        if (
                            $enveloppe instanceof CommonEnveloppe
                            && in_array(
                                $enveloppe->getStatutEnveloppe(),
                                [
                                    Atexo_Config::getParameter('STATUT_ENV_OUVERTE_EN_LIGNE'),
                                    Atexo_Config::getParameter('STATUT_ENV_OUVERTE_HORS_LIGNE'),
                                    Atexo_Config::getParameter('STATUT_ENV_OUVERTE'),
                                    Atexo_Config::getParameter('STATUT_ENV_OUVERTE_A_DISTANCE'),

                                ]
                            )
                        ) {
                            $c = new Criteria();
                            $c->add(CommonFichierEnveloppePeer::ID_ENVELOPPE, $enveloppe->getIdEnveloppeElectro());
                            $c->add(CommonFichierEnveloppePeer::ORGANISME, $organisme);
                            $fichiers = CommonFichierEnveloppePeer::doSelect($c, $connexion);

                            foreach ($fichiers as $fichier) {
                                $nomFichier = str_pad($numeroFichier, 2, '0', STR_PAD_LEFT) . '-' . Atexo_Util::OterAccents($fichier->getNomFichier());
                                if (!('ACE' == $fichier->getTypeFichier() || 'AE' == $fichier->getTypeFichier())) {
                                    $group = new Atexo_Consultation_Tree_Group(
                                        Prado::localize('NOM_ARCHIVE_FICHIER_ENVELOPPE'),
                                        Prado::localize('DESCRIPTION_ARCHIVE_FICHIER_ENVELOPPE'),
                                        Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_1'),
                                        $arrayPropGroupsCand['Contains']['Contains'][1]
                                    );
                                    $this->addItem($dossier . '/' . $cheminDossierReponse, $group);
                                    $nomFichier = self::verifierNameFile($dossier . '/' . $cheminDossierReponse, $nomFichier);
                                    $blobFichierEnveloppeTree = new Atexo_Consultation_Tree_File(stripslashes($nomFichier), Prado::localize('DEFINE_FICHIER'), 'Fichier', $fichier->getIdBlob(), '', $propFilesCand[0]);
                                    $group->addFils($blobFichierEnveloppeTree);
                                    if ('' != $fichier->getSignatureFichier()) {
                                        $signatureFichierEnveloppeTree = new Atexo_Consultation_Tree_Signature(
                                            $fichier->getJetonFileName(false, $nomFichier) . '.sig',
                                            'Signature',
                                            '',
                                            $fichier->getSignatureFichier()
                                        );
                                        $group->addFils($signatureFichierEnveloppeTree);
                                    }
                                }
                                ++$numeroFichier;
                            }
                            try {
                                $arrayInfosCons = (new Atexo_Consultation())->getInfosConsPrRapportSignature(null, $consultation);
                                $arrayInfosPli = (new Atexo_Consultation_Responses())->GetDetailsPlis($enveloppe->getIdEnveloppeElectro(), $enveloppe->getTypeEnv(), null, null, $consultation);
                                $avecSignature = $consultation->getSignatureOffre();
                                $enveloppeCrypte = $enveloppe->getCryptage();
                                $arrayInfosFichierSoumis = (new Atexo_Consultation_Responses())->getInfosCertif($fichiers, $avecSignature, $typeSignature, $enveloppeCrypte, 'agent');
                                if (0 != $enveloppe->getSousPli()) {
                                    $nomFichier = Atexo_Config::getParameter('NOM_FICHIER_RAPPORT_VERIF_SIGNATURE') . '_' .
                                        (new CommonEnveloppe())->getLibelleTypeEnveloppe($enveloppe->getTypeEnv()) . '_lot_n' . $enveloppe->getSousPli() . '.pdf';
                                } else {
                                    $nomFichier = Atexo_Config::getParameter('NOM_FICHIER_RAPPORT_VERIF_SIGNATURE') . '_' .
                                        (new CommonEnveloppe())->getLibelleTypeEnveloppe($enveloppe->getTypeEnv()) . '.pdf'; //"_" . uniqid() .
                                }
                                $nomFichier = self::verifierNameFile($dossier . '/' . $cheminDossierReponse, $nomFichier);
                                $pathFile = Atexo_Config::getParameter('COMMON_TMP') . '/' . Atexo_Config::getParameter('NOM_FICHIER_RAPPORT_VERIF_SIGNATURE') . '_' . uniqid() . '.pdf';
                                if (is_array($arrayInfosPli) && is_array($arrayInfosCons) && is_array($arrayInfosFichierSoumis)) {
                                    $path = (new Atexo_Consultation_Responses())->printRapportSignaturePdf($arrayInfosCons, $arrayInfosPli, $arrayInfosFichierSoumis, true, $pathFile);
                                }
                                $group = new Atexo_Consultation_Tree_Group(
                                    Prado::localize('NOM_ARCHIVE_FICHIER_ENVELOPPE'),
                                    Prado::localize('DESCRIPTION_ARCHIVE_FICHIER_ENVELOPPE'),
                                    Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_1')
                                );
                                $this->addItem($dossier . '/' . $cheminDossierReponse, $group);
                                $verifSignatureFichierEnveloppeTree = new Atexo_Consultation_Tree_File(
                                    stripslashes($nomFichier),
                                    Prado::localize('DESCRIPTION_FICHIER_VERIFICATION_SIGNATURE'),
                                    Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_1'),
                                    $path
                                );
                                $group->addFils($verifSignatureFichierEnveloppeTree);
                            } catch (\Exception $e) {
                                Prado::log($e->getMessage() . '-', TLogger::ERROR, 'ERROR');
                            }
                        }
                    }
                    if ($auMoinsUneEnvOuvert) {
                        // Ajout Attestation
                        $attesttionOffreQuery = new CommonTAttestationsOffresQuery();
                        $attesttionsOffre = $attesttionOffreQuery->getAllAttestationsOffre($offre->getId(), $connexion);
                        if (is_array($attesttionsOffre)) {
                            foreach ($attesttionsOffre as $attestationOffre) {
                                if ($attestationOffre instanceof CommonTAttestationsOffres) {
                                    $version = (new Atexo_Entreprise_Attestation())->retrieveLastVersionDocumentEntreprise($attestationOffre->getIdDocumentVersion());
                                    if ($version instanceof CommonTEntrepriseDocumentVersion) {
                                        $document = $version->getCommonTDocumentEntreprise();
                                        if ($document instanceof CommonTDocumentEntreprise) {
                                            if (!$version->getIdBlob()) {
                                                (new Atexo_Entreprise_Attestation())->genererAttestationFromJson($version, $document);
                                            }
                                            if ($version->getIdBlob()) {
                                                $fileName = $document->getNomDocument() . '_' . date('Y_m_d_H_i_s') . '.' . $version->getExtensionDocument();
                                                $nomFichier = self::verifierNameFile($dossier . '/' . $cheminDossierReponse, $fileName);
                                                $group = new Atexo_Consultation_Tree_Group(
                                                    Prado::localize('NOM_ARCHIVE_FICHIER_ENVELOPPE'),
                                                    Prado::localize('DESCRIPTION_ARCHIVE_FICHIER_ENVELOPPE'),
                                                    Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_1'),
                                                    $document->getNomDocument()
                                                );
                                                $this->addItem($dossier . '/' . $cheminDossierReponse, $group);
                                                $blobFichierEnveloppeTree = new Atexo_Consultation_Tree_File(stripslashes($nomFichier), Prado::localize('DEFINE_FICHIER'), 'Fichier', $version->getIdBlob(), 1);
                                                $group->addFils($blobFichierEnveloppeTree);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        // Fin Ajout Attestation
                        if (Atexo_Module::isEnabled('MarchePublicSimplifie') && $consultation->getMarchePublicSimplifie()) {//consultation MPS et module MPS activé
                            $candidatureMpsObject = (new Atexo_MarchePublicSimplifie())->retrieveCandidatureMps($offre->getId(), $consultation->getId());
                            if ($candidatureMpsObject) {
                                $nomFichier = Prado::localize('DEFINE_CANDIDATURE_MPS');
                                $group = new Atexo_Consultation_Tree_Group(
                                    Prado::localize('NOM_ARCHIVE_FICHIER_ENVELOPPE'),
                                    Prado::localize('DESCRIPTION_ARCHIVE_FICHIER_ENVELOPPE'),
                                    Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_1')
                                );
                                $this->addItem($dossier . '/' . $cheminDossierReponse, $group);
                                $nomFichier = self::verifierNameFile($dossier . '/' . $cheminDossierReponse, $nomFichier);
                                $blobFichierCandidatureMpsTree = new Atexo_Consultation_Tree_File(stripslashes($nomFichier), Prado::localize('DEFINE_FICHIER'), 'Fichier', $candidatureMpsObject->getIdBlob(), '', $propFilesCand[0]);
                                $group->addFils($blobFichierCandidatureMpsTree);
                            }
                        }

                        if (Atexo_Module::isEnabled('interfaceDume')) {
                            if (1 == $consultation->getDumeDemande()) {
                                //DUME OE
                                $candidature = Atexo_Dume_AtexoDume::retrieveCandidatureDume($offre);

                                if ($candidature instanceof CommonTCandidature) {
                                    $tDumeNumeros = Atexo_Dume_AtexoDume::getTDumeNumerosEntreprise(
                                        $candidature->getIdDumeContexte()
                                    );

                                    if (is_array($tDumeNumeros) && !empty($tDumeNumeros)) {
                                        $group = new Atexo_Consultation_Tree_Group(
                                            'DUME OE',
                                            Prado::localize('DESCRIPTION_ARCHIVE_FICHIER_ENVELOPPE'),
                                            Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_1')
                                        );

                                        $this->addItem($dossier . '/' . $cheminDossierReponse, $group);
                                        $indexDume = 0;
                                        $u = new Atexo_Util();

                                        foreach ($tDumeNumeros as $dumeNumero) {
                                            if (
                                                $dumeNumero instanceof CommonTDumeNumero &&
                                                $dumeNumero->getNumeroDumeNational()
                                            ) {
                                                if ($dumeNumero->getBlobId()) {
                                                    $nomFileDume = $dumeNumero->getNumeroDumeNational() . '.pdf';

                                                    $nomFichier = self::verifierNameFile(
                                                        $dossier . '/' . $cheminDossierReponse,
                                                        $u->OterAccents($nomFileDume)
                                                    );

                                                    $fileTreeDume = new Atexo_Consultation_Tree_File(
                                                        stripslashes($nomFichier),
                                                        'DUME OE',
                                                        '',
                                                        $dumeNumero->getBlobId(),
                                                        '',
                                                        $propFilesCand[$indexDume++]
                                                    );

                                                    $group->addFils($fileTreeDume);
                                                }

                                                if ($dumeNumero->getBlobIdXml()) {
                                                    $nomFileDume = $dumeNumero->getNumeroDumeNational() . '.xml';

                                                    $nomFichier = self::verifierNameFile(
                                                        $dossier . '/' . $cheminDossierReponse,
                                                        $u->OterAccents($nomFileDume)
                                                    );

                                                    $fileTreeDume = new Atexo_Consultation_Tree_File(
                                                        stripslashes($nomFichier),
                                                        'DUME OE',
                                                        '',
                                                        $dumeNumero->getBlobIdXml(),
                                                        '',
                                                        $propFilesCand[$indexDume++]
                                                    );

                                                    $group->addFils($fileTreeDume);
                                                }
                                            }
                                        }

                                        if (Atexo_Module::isEnabled('Groupement')) {
                                            $memebreGroupements = (new Atexo_Groupement())->retrieveMemebreGroupementSansMondataire($offre->getId(), Atexo_Config::getParameter('ID_ROLE_JURIDIQUE_MANDATAIRE'));
                                            if (is_array($memebreGroupements) && count($memebreGroupements)) {
                                                foreach ($memebreGroupements as $memebreGroupement) {
                                                    if ($memebreGroupement instanceof CommonTMembreGroupementEntreprise && $memebreGroupement->getNumerosn()) {
                                                        $dumeNumeroMembreGroupement = (new Atexo_Dume_AtexoDume())->getDumeNumeroByNumeroSn($memebreGroupement->getNumerosn());
                                                        if ($dumeNumeroMembreGroupement instanceof CommonTDumeNumero) {
                                                            if ($dumeNumeroMembreGroupement->getBlobId()) {
                                                                $nomFileDume = $dumeNumeroMembreGroupement->getNumeroDumeNational() . '.pdf';

                                                                $nomFichier = self::verifierNameFile(
                                                                    $dossier . '/' . $cheminDossierReponse,
                                                                    $u->OterAccents($nomFileDume)
                                                                );

                                                                $fileTreeDume = new Atexo_Consultation_Tree_File(
                                                                    stripslashes($nomFichier),
                                                                    'DUME OE',
                                                                    '',
                                                                    $dumeNumeroMembreGroupement->getBlobId(),
                                                                    '',
                                                                    $propFilesCand[$indexDume++]
                                                                );

                                                                $group->addFils($fileTreeDume);
                                                            }
                                                            if ($dumeNumeroMembreGroupement->getBlobIdXml()) {
                                                                $nomFileDume = $dumeNumeroMembreGroupement->getNumeroDumeNational() . '.xml';

                                                                $nomFichier = self::verifierNameFile(
                                                                    $dossier . '/' . $cheminDossierReponse,
                                                                    $u->OterAccents($nomFileDume)
                                                                );

                                                                $fileTreeDume = new Atexo_Consultation_Tree_File(
                                                                    stripslashes($nomFichier),
                                                                    'DUME OE',
                                                                    '',
                                                                    $dumeNumeroMembreGroupement->getBlobIdXml(),
                                                                    '',
                                                                    $propFilesCand[$indexDume++]
                                                                );

                                                                $group->addFils($fileTreeDume);
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                $group = new Atexo_Consultation_Tree_Group(
                    Prado::localize('NOM_ARCHIVE_FICHIER_ENVELOPPE'),
                    Prado::localize('DESCRIPTION_ARCHIVE_FICHIER_ENVELOPPE'),
                    Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_1')
                );
                $this->addItem($dossier . '/' . $cheminDossierReponse, $group);
                if ($offre instanceof CommonOffres) {
                    $timeStampXmlReponse = new Atexo_Consultation_Tree_TimeStamp(
                        'Reponse_Annonce - Jeton - ' . Atexo_Util::iso2frnDateTimeNoSep($offre->getUntrusteddate()) . '.tsr',
                        "Jeton d'horodatage",
                        '',
                        $offre->getHorodatage()
                    );
                    $group->addFils($timeStampXmlReponse);
                    if ((new Atexo_Crypto())->verifyHorodatage($offre->getHorodatage(), $offre->getXmlString())) {
                        $xmlReponseTree = new Atexo_Consultation_Tree_String('Reponse_Annonce.xml', 'Enveloppe XML', '', $offre->getXmlString());
                        $group->addFils($xmlReponseTree);
                    } else {
                        $xml = (new Atexo_Entreprise_Reponses())->createOldXmlFromNewOne($offre->getXmlString());
                        $xmlReponseTree = new Atexo_Consultation_Tree_String('Reponse_Annonce.xml', 'Enveloppe XML', '', $xml);
                        $group->addFils($xmlReponseTree);
                    }
                }

                //Permet d'archiver les documents du coffre-fort associés à la consultation
                if (Atexo_Module::isEnabled('AssocierDocumentsCfeConsultation')) {
                    $documents = new Atexo_Entreprise_CoffreFort();
                    $documentsCfe = $documents->retrievePiecesConsultationDocumentCfeByIdEntreprise($idEntrep, $consultationId, $organisme);
                    if (is_array($documentsCfe) && count($documentsCfe)) {
                        $cheminDossierDocCfe = $dossier . '/' . $cheminDossierReponse;
                        $this->addFolder(
                            $cheminDossierDocCfe,
                            str_replace(' ', '_', Prado::localize('DOCUMENTS_COFFRE_FORT')),
                            Prado::localize('DOCUMENTS_COFFRE_FORT_ASSOCIE_CONSULTATION'),
                            Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_2'),
                            $arrayAppraisal[$typeDossier]
                        );
                        foreach ($documentsCfe as $unDocumentCfe) {
                            $nomFichier = Atexo_Util::OterAccents(str_replace(' ', '_', Atexo_Util::utf8ToIso(stripslashes($unDocumentCfe->getNomFichier()))));
                            $group = new Atexo_Consultation_Tree_Group(
                                Prado::localize('TEXT_CONSULTATION_DOCUMENTS_COFFRE_FORT'),
                                Prado::localize('TEXT_CONSULTATION_DOCUMENTS_COFFRE_FORT'),
                                Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_1'),
                                $arrayAutresProprietes
                            );
                            $path = $cheminDossierDocCfe . '/' . str_replace(' ', '_', Prado::localize('DOCUMENTS_COFFRE_FORT'));
                            $this->addItem($path, $group);
                            $nomFichier = self::verifierNameFile($path, $nomFichier);
                            $docAssocieConsTree = new Atexo_Consultation_Tree_File(
                                stripslashes($nomFichier),
                                $unDocumentCfe->getTypeDocument(),
                                Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_1'),
                                $unDocumentCfe->getIdBlob(),
                                1
                            );
                            $group->addFils($docAssocieConsTree);
                            $timeStampDocAssocieConsTree = new Atexo_Consultation_Tree_TimeStamp(stripslashes($nomFichier) . ' - Jeton - ' .
                                Atexo_Util::iso2frnDateTimeNoSep($unDocumentCfe->getUntrusteddate()) . '.tsr', "Jeton d'horodatage", '', $unDocumentCfe->getHorodatage());
                            $group->addFils($timeStampDocAssocieConsTree);
                        }
                    }
                }
//            $numeroEntrep++;
            }

            // Publicite
            $arrayPropFilesPub = $arrayAutresProprietes['Files']['TYPE_DOC_EXTERNE_PUBLICITES'];
            $listeAnnonce = (new Atexo_Publicite_Destinataire())->retreiveAnnonceWithFormXml($consultation->getId(), $consultation->getOrganisme());
            if ($listeAnnonce) {
                $index = 0;
                foreach ($listeAnnonce as $oneAnnonce) {
                    $annonceMoniteur = false;
                    $pathFileFormulaire = '';
                    ++$index;
                    //$xml = $oneAnnonce->getCommonReferentielFormXml()->getXml();
                    $group = new Atexo_Consultation_Tree_Group(
                        'Publicites',
                        Prado::localize('DESCRIPTION_FICHIER_PUBLICITE'),
                        Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_1'),
                        $arrayPropFilesPub
                    );
                    $this->addItem(Atexo_Config::getParameter('DIR_DOC_EXTERNE_PUBLICITES'), $group);
                    $nameFile = Atexo_Util::OterAccents($oneAnnonce->getNomFichierXml());
                    $referentielFormXml = $oneAnnonce->getCommonReferentielFormXml();
                    if ($referentielFormXml instanceof CommonReferentielFormXml) {
                        $annonceMoniteur = (new Atexo_Publicite_Destinataire())->retreiveAnnonceMoniteurByIdFormXml($referentielFormXml->getId(), $consultation->getOrganisme());
                        $pathFileFormulaire = (new Atexo_Consultation_ArchivePublicite())->getFormulaireXml($referentielFormXml, $connexion);
                        if ($annonceMoniteur instanceof CommonAnnonceMoniteur) {
                            if ('2' == $annonceMoniteur->getStatutDestinataire() || '3' == $annonceMoniteur->getStatutDestinataire() || '4' == $annonceMoniteur->getStatutDestinataire()) {
                                if ('2' == $annonceMoniteur->getStatutDestinataire()) {
                                    $date = $annonceMoniteur->getDatepub();
                                } elseif ('3' == $annonceMoniteur->getStatutDestinataire() || '4' == $annonceMoniteur->getStatutDestinataire()) {
                                    $date = $annonceMoniteur->getDateMaj();
                                }
                                $nameFileMoniteur = 'Annonce_Moniteur_BOAMP_' . $date . '.xml';
                                $pathFileFormulaire2 = (new Atexo_Consultation_ArchivePublicite())->getFileXmlMoniteur($annonceMoniteur->getXmlMoniteur(), $organisme);
                            }
                        }

                        $nameFile = self::verifierNameFile(Atexo_Config::getParameter('DIR_DOC_EXTERNE_PUBLICITES'), $nameFile);
                        $nameFileMoniteur = self::verifierNameFile(Atexo_Config::getParameter('DIR_DOC_EXTERNE_PUBLICITES'), $nameFileMoniteur);
                        $xmlPubTreeCons = new Atexo_Consultation_Tree_File(
                            stripslashes($nameFile),
                            Prado::localize('DESCRIPTION_FICHIER_XML_PUBLICITE'),
                            Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_1'),
                            $pathFileFormulaire
                        );
                        $group->addFils($xmlPubTreeCons);
                        $xmlMonPubTreeCons = new Atexo_Consultation_Tree_File(
                            stripslashes($nameFileMoniteur),
                            Prado::localize('DESCRIPTION_FICHIER_XML_PUBLICITE'),
                            Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_1'),
                            $pathFileFormulaire2
                        );
                        $group->addFils($xmlMonPubTreeCons);
                    }
                }
            }
            // Formats Libres : Annonce
            $listeFormAnnonceJal = (new Atexo_Publicite_AnnonceJAL())->retreiveListFormulaireLibre($consultation->getId(), $organisme);
            if ((is_countable($listeFormAnnonceJal) ? count($listeFormAnnonceJal) : 0) > 0) {
                $index = 0;
                foreach ($listeFormAnnonceJal as $oneAnnoneJal) {
                    ++$index;
                    $pathFileFormatLibre = Atexo_Consultation_ArchivePublicite::getFormatLibreAnnonceXml($oneAnnoneJal, $organisme);
                    if ('MEGALIS' == strtoupper(Atexo_Config::getParameter('PROFIL_ARCHIVE'))) {
                        $this->addFile(
                            Atexo_Config::getParameter('DIR_DOC_EXTERNE_PUBLICITES'),
                            'Formulaire_Libre_Annonce_' . $index . '.xml',
                            Prado::localize('DESCRIPTION_FICHIER_PUBLICITE_LIBRE_ANNONCE'),
                            Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_1'),
                            $pathFileFormatLibre
                        );
                    } else {
                        $group = new Atexo_Consultation_Tree_Group(
                            'Publicite',
                            Prado::localize('DESCRIPTION_FICHIER_PUBLICITE_LIBRE'),
                            Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_1'),
                            $arrayPropFilesPub
                        );
                        $this->addItem(Atexo_Config::getParameter('DIR_DOC_EXTERNE_PUBLICITES'), $group);
                        $annonceLibreTreeCons = new Atexo_Consultation_Tree_File(
                            'Formulaire_Libre_Annonce_' . $index . '.xml',
                            Prado::localize('DESCRIPTION_FICHIER_PUBLICITE_LIBRE_ANNONCE'),
                            Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_1'),
                            $pathFileFormatLibre,
                            ''
                        );
                        $group->addFils($annonceLibreTreeCons);
                    }
                }
            }

            //Files de Commission_Appel_Offre
            $arrayPropFilesCom = $arrayAutresProprietes['Files']['TYPE_DOC_EXTERNE_COMMISSION_APPEL_OFFRE']['Contains'];
            $arrayPropGroupsCom = $arrayAutresProprietes['Groups']['TYPE_DOC_EXTERNE_COMMISSION_APPEL_OFFRE']['Contains'];
            // Commission_Appel_Offre
            if (Atexo_Config::getParameter('ACTIVE_GENERE_REGISTRE_VIA_DOC_GEN')) {
                $docGenArchive = Atexo_Util::getSfService(DocGenForArchive::class);
                $pathRtfResgistreOuverture = $docGenArchive->generate($consultation->getId(), self::CODE_DEP_TEMPLATE);
            } else {
                $pathRtfResgistreOuverture = Atexo_Consultation::generateRegistreCommission($consultation, $organisme);
            }
            $group = new Atexo_Consultation_Tree_Group(
                'Commission',
                Prado::localize('DESCRIPTION_COMMISSION_APPEL_OFFRES'),
                Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_2'),
                $arrayPropGroupsCom[0]
            );
            $this->addItem(Atexo_Config::getParameter('DIR_DOC_EXTERNE_COMMISSION_APPEL_OFFRE'), $group);
            
            $nomResgistreOuverture = Atexo_Config::getParameter('NOM_FICHIER_DEPOUILLEMENT_N_LOTS_CONSULTATION') . '_' . $consultation->getReferenceUtilisateur(true) . '.pdf';
            $nomResgistreOuverture = self::verifierNameFile(Atexo_Config::getParameter('DIR_DOC_EXTERNE_COMMISSION_APPEL_OFFRE'), $nomResgistreOuverture);
            $commissionConstree = new Atexo_Consultation_Tree_File(
                stripslashes($nomResgistreOuverture),
                Prado::localize('DESCRIPTION_COMMISSION_APPEL_OFFRES'),
                Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_2'),
                $pathRtfResgistreOuverture,
                '',
                $arrayPropFilesCom[0]['Document']
            );
            $group->addFils($commissionConstree);

            //Files de Notification
            $arrayPropFilesNotif = $arrayAutresProprietes['Files']['TYPE_DOC_EXTERNE_NOTIFICATION']['Contains'];
            $arrayPropGroupsNotif = $arrayAutresProprietes['Groups']['TYPE_DOC_EXTERNE_NOTIFICATION']['Contains'];
            $arrayPropGroupsNotif['dossier'] = 'TYPE_DOC_EXTERNE_NOTIFICATION';
            // Notification
            $objetMessage = Atexo_Message::retreiveNotificationByIdRef($consultation->getId(), $consultation->getOrganisme());

            $pathResgistre = (new Atexo_GenerationExcel())->generateExcelRegistreNotification($consultationId, $objetMessage, true, $organisme, false, true);
            $nomRegistre = Atexo_Util::OterAccents(basename($pathResgistre));

            $group = new Atexo_Consultation_Tree_Group(
                'Suivi des messages',
                Prado::localize('DESCRIPTION_NOTIFICATION'),
                Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_2'),
                $arrayPropGroupsNotif[0]
            );
            $this->addItem(Atexo_Config::getParameter('DIR_DOC_EXTERNE_NOTIFICATION'), $group);
            $nomRegistre = self::verifierNameFile(Atexo_Config::getParameter('DIR_DOC_EXTERNE_NOTIFICATION'), $nomRegistre);
            $registreNotifTreeCons = new Atexo_Consultation_Tree_File(
                stripslashes($nomRegistre),
                Prado::localize('DESCRIPTION_NOTIFICATION'),
                Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_2'),
                $pathResgistre,
                '',
                $arrayPropFilesNotif[0]['Document']
            );
            $group->addFils($registreNotifTreeCons);

            if ($objetMessage) {
                foreach ($objetMessage as $one) {
                    $pathEnvoiCourier = (new Atexo_Message())->generateNotificationFile($one, $consultation->getOrganisme());
                    $dossier = Atexo_Config::getParameter('DIR_DOC_EXTERNE_NOTIFICATION');
                    $typeDossier = Atexo_Config::getParameter('TYPE_DOC_EXTERNE_NOTIFICATION');
                    $idMessage = $one->getId();
                    $dateMessage = substr($one->getDateMessage(), '0', '10');
                    $ssDossier = 'Notification_' . $idMessage . '_' . $dateMessage;

                    //Création du dossier Notification
                    $this->addFolder(
                        $dossier,
                        $ssDossier,
                        "Documents relatifs à la Notification du marché (Acte d'engagement, accusé de réception)",
                        Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_2'),
                        $arrayAppraisal[$typeDossier]
                    );
                    //TODO : traiter la notification "$arrayPropFilesNotif['1']['Contains']['Document'][0]"
                    //Création des sous dossiers Notification
                    $group = new Atexo_Consultation_Tree_Group(
                        'Notifications',
                        "Documents relatifs à la Notification du marché (Acte d'engagement, accusé de réception)",
                        Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_1'),
                        $arrayPropGroupsNotif[1]
                    );
                    $this->addItem($dossier . '/' . $ssDossier, $group);
                    $messageNotifTreeCons = new Atexo_Consultation_Tree_File(
                        'Notice.txt',
                        Prado::localize('DESCRIPTION_NOTIFICATION'),
                        Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_2'),
                        $pathEnvoiCourier['mailNotification'],
                        '',
                        $arrayPropFilesNotif['1']['Contains']['Document'][0]
                    );
                    $group->addFils($messageNotifTreeCons);

                    //Creation des pièces jointes
                    $this->addFolder($dossier . '/' . $ssDossier, 'Pieces_jointes', '', Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_2'), $arrayAppraisal[$typeDossier]);
                    $pathPJ = $pathEnvoiCourier['piecesjointes'];
                    foreach ($pathPJ as $onePJ) {
                        $group = new Atexo_Consultation_Tree_Group(
                            'PieceJointe',
                            "Documents relatifs à la Notification du marché (Acte d'engagement, accusé de réception)",
                            Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_1'),
                            $arrayPropGroupsNotif[1]
                        );
                        $path = $dossier . '/' . $ssDossier . '/' . 'Pieces_jointes';
                        $this->addItem($path, $group);
                        $nomPj = self::verifierNameFile($path, Atexo_Util::OterAccents($onePJ[1]));
                        $descNotifTreeCons = new Atexo_Consultation_Tree_File(
                            stripslashes($nomPj),
                            Prado::localize('DESCRIPTION_NOTIFICATION'),
                            Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_2'),
                            $onePJ[0]
                        );
                        $group->addFils($descNotifTreeCons);
                    }
                }
            }

            //Echange Chorus
            if ((new Atexo_Module())->isChorusAccessActivate(Atexo_CurrentUser::getIdServiceAgentConnected())) {
                $decisions = (new Atexo_Consultation_Decision())->retrieveDecisionEnvoloppeByReference($consultationId, $consultation->getOrganisme());
                if ($decisions && (is_countable($decisions) ? count($decisions) : 0) > 0) {
                    foreach ($decisions as $decision) {
                        $dossier = Atexo_Config::getParameter('DIR_DOC_EXTERNE_CHORUS_ECHANGE');
                        $typeDossier = Atexo_Config::getParameter('TYPE_DOC_EXTERNE_CHORUS_ECHANGE');
                        $group = new Atexo_Consultation_Tree_Group('Echanges Chorus', Prado::localize('DESCRIPTION_ECHANGE_CHORUS'), Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_2'));
                        $this->addItem(Atexo_Config::getParameter('DIR_DOC_EXTERNE_CHORUS_ECHANGE'), $group);

                        $sDossier = 'Echange_Chorus_lot' . $decision->getLot();

                        //Création du dossier Chorus
                        $this->addFolder($dossier, $sDossier, '', Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_2'), $arrayAppraisal[$typeDossier]);

                        $path = $dossier . '/' . $sDossier;

                        $group = new Atexo_Consultation_Tree_Group($sDossier, '', Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_2'));
                        $this->addItem($path, $group);

                        $echangeResult = (new Atexo_Chorus_Echange())->retreiveEchangesByIdDecision($organisme, $decision->getIdDecisionEnveloppe());
                        $pathResgistre = (new Atexo_GenerationExcel())->generateExcelEchangeChorus($consultationId, $echangeResult, $decision, true, $organisme, false, true);
                        $nomRegistre = Atexo_Util::OterAccents(basename($pathResgistre));
                        $nomRegistre = self::verifierNameFile($path, $nomRegistre);
                        $registreChorusTreeCons = new Atexo_Consultation_Tree_File(
                            stripslashes($nomRegistre),
                            Prado::localize('DESCRIPTION_ECHANGE_CHORUS'),
                            Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_2'),
                            $pathResgistre
                        );
                        $group->addFils($registreChorusTreeCons);

                        if ($echangeResult) {
                            foreach ($echangeResult as $echange) {
                                $idEchange = $echange->getId();
                                $dateEchange = substr($echange->getDateCreation(), '0', '10');
                                $ssDossier = 'Chorus_' . $idEchange . '_' . $dateEchange;

                                //Création du dossier Chorus
                                $this->addFolder($path, $ssDossier, '', Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_2'), $arrayAppraisal[$typeDossier]);

                                $path = $path . '/' . $ssDossier;

                                $group = new Atexo_Consultation_Tree_Group('Chorus', '', Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_2'));
                                $this->addItem($path, $group);
                                $files = (new Atexo_Chorus_GestionFichierChorus())->getFichierEnvoyeAChorus($echange, $decision, $organisme, $connexion);

                                foreach ($files as $file) {
                                    $nomPj = self::verifierNameFile($path, Atexo_Util::OterAccents($file[0]));
                                    $pjChorusTreeCons = new Atexo_Consultation_Tree_File(
                                        stripslashes($nomPj),
                                        Prado::localize('DESCRIPTION_ECHANGE_CHORUS'),
                                        Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_2'),
                                        $file[1]
                                    );
                                    $group->addFils($pjChorusTreeCons);
                                }
                            }
                        }
                    }
                }
            }//fin Echange Chorus

            // Arborescence des dossiers externes
            foreach ($arrayTopDirectories as $type => $nom) {
                $this->addFolder(
                    $arrayTopDirectories[$type],
                    'Documents_Externes',
                    'Documents externes - ' . $arrayDecriptionsDirectoriesExternes[$type],
                    $arrayAccessDirectories[$type],
                    $arrayAppraisal[$type]
                );
            }

            // === Récupération des fichiers externes de la base ===
            $arrayDocuments = (new Atexo_Consultation_Documentexterne())->getDocumentsExternes($this->idConsultation, $organisme, $lot);
            $mappingTypeDocPath = Atexo_Consultation_Archive::getPathsDocExternes();
            foreach ($arrayDocuments as $documentExterne) {
                $path = $mappingTypeDocPath[$documentExterne->getType()];
                // LIGNE COMMENTEE : $group = new Atexo_Consultation_Tree_Group(Atexo_Util::OterAccents($documentExterne->getNom()), "Document Externe",
                // $arrayAccessDirectories[$documentExterne->getType()]);
                // LIGNE COMMENTEE : $this->addItem($path, $group);
                $nomFichier = self::verifierNameFile($path, Atexo_Util::OterAccents($documentExterne->getNom()));
                // LIGNE COMMENTEE : $group->addFils(new Atexo_Consultation_Tree_File( $nomFichier, $documentExterne->getDescription(), "Restriction", $documentExterne->getIdblob()));
                $this->addFile($path, $nomFichier, $documentExterne->getDescription(), 'Restriction', $documentExterne->getIdblob());
            }
            //Avis publiés
            $avis = (new Atexo_Publicite_Avis())->retreiveListFormulaireLibre($consultation->getId(), $organisme);
            if (is_array($avis) && count($avis)) {
                foreach ($avis as $oneAvis) {
                    if ($oneAvis instanceof \CommonAVIS && $oneAvis->getType() == Atexo_Config::getParameter('TYPE_FORMAT_LIBRE_FICHIER_JOINT')) {
                        $nomFichier = Atexo_Util::OterAccents(str_replace(' ', '_', Atexo_Util::utf8ToIso(stripslashes($oneAvis->getNomFichier()))));
                        $path = Atexo_Config::getParameter('DIR_DOC_EXTERNE_PUBLICITES') . '/Documents_Externes';
                        $nomFichier = self::verifierNameFile($path, $nomFichier);
                        $this->addFile(
                            $path,
                            $nomFichier,
                            Prado::localize('TEXT_FICHIER_JOINT_AVIS_COMPLEMENTAIRE'),
                            Atexo_Config::getParameter('ACCESS_RESTRICTION_DOC_ADMIN_1'),
                            $oneAvis->getAvis()
                        );
                        $this->addTimeStamp($path, $nomFichier . ' - Jeton - ' . Atexo_Util::iso2frnDateTimeNoSep($oneAvis->getUntrusteddate()) . '.tsr', "Jeton d'horodatage", '', $oneAvis->getHorodatage());
                    }
                }
            }
        }//fin try
        catch(Exception $e) {
            Prado::log($e->getMessage()."-",TLogger::ERROR, "ERROR");
            throw $e;
        }
    }

    /**
     * Permet de centraliser la suppression d'un fichier (temporaire généralement)
     * au lieu de mettre des unlink partout.
     *
     * @param $pathFile
     */
    public function releaseFile($pathFile)
    {
        @unlink($pathFile);
    }

    /**
     * Permet de generer l'arborescence de la consultation specifique a la config 'STRUCTURE_ARCHIVE'.
     *
     * @param string    $consultationId      : reference de la consultation
     * @param PropelPDO $connexion           : connexion
     * @param string    $organisme           : acronyme de l'organisme
     * @param bool      $useLangageCatalogue : precise s'il faut utiliser la langue de la config ou non
     *                                       => true pour utiliser la langue de la config
     *                                       => false sinon
     * @param int       $lot                 : numero du lot
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-megalis
     *
     * @copyright Atexo 2016
     */
    public function generateTree($consultationId, $connexion, $organisme = null, $useLangageCatalogue = true, $lot = 0)
    {
        $logger = Atexo_LoggerManager::getLogger('archivage');
        $logger->info("Debut generation de l'arborescence: {ref_cons = $consultationId}, {organisme = $organisme}, {lot = $lot}");
        try {
            if (true === $useLangageCatalogue) {
                Atexo_Languages::setLanguageCatalogue('agent');
            }
            if (empty($connexion)) {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));
            }
            if (empty($organisme)) {
                $organisme = Atexo_CurrentUser::getOrganismAcronym();
            }

            self::buildNameFileArchive($consultationId, $organisme, $connexion, $logger);
            self::creerArborescenceDossierConsultation($consultationId, $organisme, $useLangageCatalogue, $logger, $connexion);
            self::creerArborescenceDossierCandidaturesOffresDecision($consultationId, $organisme, $logger, $connexion);
        } catch (Exception $e) {
            $erreurMsg = 'Erreur = ' . $e->getMessage() . PHP_EOL . 'Trace = ' . $e->getTraceAsString();
            $logger->error("Erreur lors de la generation de l'arborescence de l'archive: {ref_cons = $consultationId}, {organisme = $organisme}, {lot = $lot}, {useLangageCatalogue = " . print_r($useLangageCatalogue, true) . '}' . PHP_EOL . $erreurMsg);
        }
        $logger->info("Fin generation de l'arborescence: {ref_cons = $consultationId}, {organisme = $organisme}, {lot = $lot}");
    }

    /**
     * Permet d'ajouter un fichier a un repertoire de l'arborescence.
     *
     * @param string $dir        : chemin du repertoire
     * @param string $groupName  : nom du groupe
     * @param string $pathFile   : chemin du fichier
     * @param string $nomFichier : nom du fichier
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-megalis
     *
     * @copyright Atexo 2016
     */
    public function addFilesToDirTree($dir, $groupName, $pathFile, $nomFichier, $org = null, $hashContent = false, $dateCreation = '')
    {
        try {
            if ($pathFile) {
                $group = new Atexo_Consultation_Tree_Group($groupName);
                self::addItem($dir, $group);
                $filename = stripslashes($nomFichier);
                $fileTree = new Atexo_Consultation_Tree_File(stripslashes($nomFichier), '', '', $pathFile, 0, [], basename($dir), self::getAlgorithmeHashage(), self::getHashFile(
                    $pathFile,
                    $org,
                    $hashContent,
                    $filename
                ), $groupName, $dateCreation);
                $group->addFils($fileTree);
            }
        } catch (Exception $e) {
            $erreurMsg = 'Erreur = ' . $e->getMessage() . PHP_EOL . 'Trace = ' . $e->getTraceAsString();
            $logger = Atexo_LoggerManager::getLogger('archivage');
            $logger->error("Erreur lors de l'ajout de fichier a l'arborescence de l'archive: {repertoire = $dir}, {groupeName = $groupName}, {pathFile = $pathFile}, {nameFile = $nomFichier}" . PHP_EOL . $erreurMsg);
        }
    }

    /**
     * Permet d'ajouter un fichier d'horodatage a un repertoire de l'arborescence.
     *
     * @param string $dir           : chemin du repertoire
     * @param string $groupName     : nom du groupe
     * @param string $pathFile      : chemin du fichier
     * @param string $nomFichier    : nom du fichier
     * @param string $untrusteddate
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-megalis
     *
     * @copyright Atexo 2016
     */
    public function addFilesTimestampToDirTree($dir, $groupName, $pathFile, $nomFichier, $untrusteddate, $org = null)
    {
        try {
            $group = new Atexo_Consultation_Tree_Group($groupName);
            $this->addItem($dir, $group);
            $filename = stripslashes($nomFichier) . ' - Jeton - ' . Atexo_Util::iso2frnDateTimeNoSep($untrusteddate) . '.tsr';
            $fileTreeTimeStampTree = new Atexo_Consultation_Tree_TimeStamp(
                $filename,
                '',
                '',
                $pathFile,
                [],
                basename($dir),
                self::getAlgorithmeHashage(),
                self::getHashFile($pathFile, $org, true, $filename),
                $groupName
            );
            $group->addFils($fileTreeTimeStampTree);
        } catch (Exception $e) {
            $erreurMsg = 'Erreur = ' . $e->getMessage() . PHP_EOL . 'Trace = ' . $e->getTraceAsString();
            $logger = Atexo_LoggerManager::getLogger('archivage');
            $logger->error("Erreur lors de l'ajout de fichier timestamp a l'arborescence de l'archive: {repertoire = $dir}, {groupeName = $groupName}, {pathFile = $pathFile}, {nameFile = $nomFichier}" . PHP_EOL . $erreurMsg);
        }
    }

    /**
     * Permet d'ajouter une chaine de caractere a un repertoire de l'arborescence.
     *
     * @param string $dir           : chemin du repertoire
     * @param string $groupName     : nom du groupe
     * @param string $pathFile      : chemin du fichier
     * @param string $nomFichier    : nom du fichier
     * @param string $untrusteddate
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-megalis
     *
     * @copyright Atexo 2016
     */
    public function addStringToDirTree($dir, $groupName, $string, $nomFichier, $description)
    {
        try {
            $group = new Atexo_Consultation_Tree_Group($groupName);
            $this->addItem($dir, $group);
            $emprunte = self::getHashFile($string, null, true, $nomFichier);
            $typeEmprunte = self::getAlgorithmeHashage();
            $xmlReponseTree = new Atexo_Consultation_Tree_String($nomFichier, $description, '', $string, basename($dir), $groupName, $emprunte, $typeEmprunte);
            $group->addFils($xmlReponseTree);
        } catch (Exception $e) {
            $erreurMsg = 'Erreur = ' . $e->getMessage() . PHP_EOL . 'Trace = ' . $e->getTraceAsString();
            $logger = Atexo_LoggerManager::getLogger('archivage');
            $logger->error("Erreur lors de l'ajout d'une chaine de caractère a l'arborescence de l'archive: {repertoire = $dir}, {groupeName = $groupName},  {nameFile =
            $nomFichier}" . PHP_EOL . $erreurMsg);
        }
    }

    /**
     * Permet d'ajouter une signature a un repertoire de l'arborescence.
     *
     * @param string     $dir
     * @param string     $groupName
     * @param string     $signature
     * @param string     $nomFichier
     * @param null       $org
     * @param string     $dateCreation
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2017-megalis
     * @copyright Atexo 2017
     */
    public function addSignatureToDirTree($dir, $groupName, $signature, $nomFichier, $org = null, bool $hashContent = false, $dateCreation = '')
    {
        try {
            if ($signature) {
                $group = new Atexo_Consultation_Tree_Group($groupName);
                self::addItem($dir, $group);
                $filename = stripslashes($nomFichier);
                $fileTree = new Atexo_Consultation_Tree_Signature(stripslashes($nomFichier), 'Signature', '', $signature, [], basename($dir), self::getAlgorithmeHashage(), self::getHashFile(
                    $signature,
                    $org,
                    $hashContent,
                    $filename
                ), $groupName, $dateCreation);
                $group->addFils($fileTree);
            }
        } catch (Exception $e) {
            $erreurMsg = 'Erreur = ' . $e->getMessage() . PHP_EOL . 'Trace = ' . $e->getTraceAsString();
            $logger = Atexo_LoggerManager::getLogger('archivage');
            $logger->error("Erreur lors de l'ajout de fichier a l'arborescence de l'archive: {repertoire = $dir}, {groupeName = $groupName}, {signature = $signature}, {nameFile = $nomFichier}" . PHP_EOL . $erreurMsg);
        }
    }

    /**
     * Permet de creer un repertoire au niveau superieur de l'arborescence.
     *
     * @param string $nomDossier : nom du dossier
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-megalis
     *
     * @copyright Atexo 2016
     */
    public function addTreeFolder($nomDossier)
    {
        try {
            $topDir = new Atexo_Consultation_Tree_Directory($nomDossier);
            self::addFils($topDir);
        } catch (Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('archivage');
            $logger->error("Erreur lors de la construction du repertoire d'arborescence, {nameFolder = $nomDossier}" . PHP_EOL . 'Erreur = ' . $e->getMessage() . PHP_EOL . 'Trace = ' . $e->getTraceAsString());
        }
    }

    /**
     * Permet de creer l'arborescence du repertoire 'consultation' au niveau superieur.
     *
     * @param string $consultationId      : reference de la consultation
     * @param string $organisme           : acronyme de l'organisme
     * @param bool   $useLangageCatalogue : precise s'il faut utiliser la langue de la config ou non
     *                                    => true pour utiliser la langue de la config
     *                                    => false sinon
     * @param Logger $logger              : logger
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-megalis
     *
     * @copyright Atexo 2016
     */
    public function creerArborescenceDossierConsultation($consultationId, $organisme = null, $useLangageCatalogue = true, $logger = null, $connexion = null)
    {
        try {
            if (empty($logger)) {
                $logger = Atexo_LoggerManager::getLogger('archivage');
            }
            $logger->info("Debut creation de l'arborescence de l'archive de la consultation {reference = '$consultationId'}");

            if (empty($organisme)) {
                $organisme = Atexo_CurrentUser::getOrganismAcronym();
            }

            //Creation du dossier parent 'consultation
            self::addTreeFolder(self::DIR_CONSULTATION);

            //Creation des sous dossiers au dossier 'consultation'
            self::addFolder(self::DIR_CONSULTATION, self::DIR_DOSSIER_CONSULTATION);

            //Creation de l'arborescence du repertoire 'publicites'
            self::ajouterFichiersPublicites($consultationId, $organisme, self::DIR_CONSULTATION, $logger, $connexion);

            // Creation de l'arborescence du repertoire des questions, depots, retraits et suivi des messages
            self::ajouterFichiersRegistres($consultationId, $organisme, self::DIR_CONSULTATION, $logger);

            //Creation de l'arborescence du repertoire des messages echanges 'DIR_MESSAGES_ECHANGES'
            self::creerArborescenceMessagesEchanges($consultationId, $organisme, self::DIR_CONSULTATION, self::DIR_MESSAGES_ECHANGES, self::DIR_PIECES_JOINTES, null, $logger, Atexo_Config::getParameter('TYPE_DOC_EXTERNE_MESSAGE_ECHANGE'));

            //Creation des fichiers DCE, RC, Complements
            self::genererArborescenceDocsConsultation($consultationId, $organisme, self::DIR_CONSULTATION . DIRECTORY_SEPARATOR . self::DIR_DOSSIER_CONSULTATION, $logger);


            //DUME Acheteur
            $consultation = BaseCommonConsultationQuery::create()->filterById($consultationId)->filterByOrganisme($organisme)->findOne($connexion);
            if (Atexo_Module::isEnabled('interfaceDume') && $consultation instanceof CommonConsultation) {
                if (1 == $consultation->getDumeDemande()) {
                    $tDumeNumeros = Atexo_Dume_AtexoDume::getTDumeNumeros($consultation);
                    if (is_array($tDumeNumeros) && !empty($tDumeNumeros)) {
                        $u = new Atexo_Util();
                        foreach ($tDumeNumeros as $dumeNumero) {
                            if ($dumeNumero instanceof CommonTDumeNumero && $dumeNumero->getNumeroDumeNational() && $dumeNumero->getBlobId()) {
                                $nomFileDume = $dumeNumero->getNumeroDumeNational().'.pdf';
                                $nomFichier = self::verifierNameFile(Atexo_Config::getParameter('DIR_DOC_EXTERNE_CONSULTATION'), $u->OterAccents($nomFileDume));
                                self::addFilesToDirTree(
                                    self::DIR_CONSULTATION . DIRECTORY_SEPARATOR . self::DIR_DOSSIER_CONSULTATION,
                                    'DUME_ACHETEUR',
                                    $dumeNumero->getBlobId(),
                                    $nomFichier
                                );
                            }
                        }
                    }
                }
            }

        } catch (Exception $e) {
            $erreurMsg = 'Erreur = ' . $e->getMessage() . PHP_EOL . 'Trace = ' . $e->getTraceAsString();
            $logger->error("Erreur lors de la generation de l'arborescence du dossier '" . self::DIR_CONSULTATION . "': {ref_cons = $consultationId}, {organisme = $organisme}, {useLangageCatalogue = " . print_r($useLangageCatalogue, true) . '}' . PHP_EOL . $erreurMsg);
        }
        $logger->info("Fin creation de l'arborescence de l'archive de la consultation {reference = '$consultationId'}");
    }

    /**
     * Permet de construire l'arborescence du repertoire des messages.
     *
     * @param string      $consultationId       : reference de la consultation
     * @param string      $organisme            : acronyme de l'organisme
     * @param string      $repertoireParent     : chemin du repertoire parent
     * @param string      $nomRepertoireMsg     : nom du repertoire qui doit contenir les messages
     * @param string      $nomRepPj             : nom repertoire des pieces jointes
     * @param string|null $typeMessage          : identifiant du type de message
     * @param Logger      $logger
     * @param string      $typeDocExtRepMessage : type document externe repertoire parent du repertoire des messages
     * @param string      $lot                  : numero du lot
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-megalis
     *
     * @copyright Atexo 2016
     */
    public function creerArborescenceMessagesEchanges($consultationId, $organisme, $repertoireParent, $nomRepertoireMsg, $nomRepPj, $typeMessage = null, $logger = null, $typeDocExtRepMessage = null, $lot = null)
    {
        $cheminRepertoireMsgEchange = $repertoireParent . DIRECTORY_SEPARATOR . $nomRepertoireMsg;
        if (empty($logger)) {
            $logger = Atexo_LoggerManager::getLogger('archivage');
        }
        $logger->info("Debut creation de l'arborescence du repertoire '$cheminRepertoireMsgEchange' de la consultation {reference = '$consultationId'}, {organisme = $organisme}");
        try {
            // Creation du repertoire '$nomRepertoireMsg'
            self::addFolder($repertoireParent, $nomRepertoireMsg);
            if (!empty($typeDocExtRepMessage)) {
                self::CreerRepertoireDocumentsExternes($cheminRepertoireMsgEchange, $consultationId, $organisme, $lot, $typeDocExtRepMessage, $logger);
            }

            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
            $c = new Criteria();
            $c->add(CommonConsultationPeer::ID, $consultationId);
            $c->add(CommonConsultationPeer::ORGANISME, $organisme);
            $commonConsultation = CommonConsultationPeer::doSelectOne($c, $connexion);

            if($commonConsultation instanceof  CommonConsultation && $commonConsultation->getVersionMessagerie() == 2){
                $service = Atexo_Util::getSfService(ArchiveService::class);
                $echanges = $service->getMessecEchanges($consultationId);
                $Messges = (new Atexo_Message())->generateMessageEchangeFilesForMessec($echanges);
                self::creerArborescenceDossierMessagesEchangesForMessec($Messges, $cheminRepertoireMsgEchange, $logger);
            } else {
                $result = Atexo_Message::getAllEchangesDestinataireByIdRef($consultationId, $organisme, null, true, false, null, false, $typeMessage);
                $Messges = (new Atexo_Message())->generateMessageEchangeFiles($result, $organisme);
                self::creerArborescenceDossierMessagesEchanges($Messges, $cheminRepertoireMsgEchange, $nomRepPj, $logger);
            }
        } catch (Exception $e) {
            $erreurMsg = 'Erreur = ' . $e->getMessage() . PHP_EOL . 'Trace = ' . $e->getTraceAsString();
            $logger->error("Erreur lors de la generation de l'arborescence du dossier '$cheminRepertoireMsgEchange': {ref_cons = $consultationId}, {organisme = $organisme}" . PHP_EOL . $erreurMsg);
        }
        $logger->info("Fin creation de l'arborescence du repertoire '$cheminRepertoireMsgEchange' de la consultation {reference = '$consultationId'}, {organisme = $organisme}");
    }

    /**
     * Permet d'ajouter les pieces de la consultation (DCE, RC, COMPLEMENTS).
     *
     * @param string $consultationId : reference de la consultation
     * @param string $org            : organisme de la consultation
     * @param Logger $logger         : logger
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-megalis
     *
     * @copyright Atexo 2016
     */
    public function genererArborescenceDocsConsultation($consultationId, $org, $pathRepertoireParent, $logger = null)
    {
        if (empty($logger)) {
            $logger = Atexo_LoggerManager::getLogger('archivage');
        }
        try {
            $logger->info("Debut ajout des pieces de DCE, RC et COMPLEMENT a l'archive");

            $pathDirArchiveDce = $pathRepertoireParent . DIRECTORY_SEPARATOR . self::DIR_ARCHIVES_DCE;
            $pathDirArchiveRc = $pathRepertoireParent . DIRECTORY_SEPARATOR . self::DIR_ARCHIVES_RC;
            $pathDirArchiveComp = $pathRepertoireParent . DIRECTORY_SEPARATOR . self::DIR_PIECES_COMPLEMENTAIRES;

            // Creation des repertoires
            self::addFolder($pathRepertoireParent, self::DIR_ARCHIVES_DCE);
            self::addFolder($pathRepertoireParent, self::DIR_ARCHIVES_RC);
            self::addFolder($pathRepertoireParent, self::DIR_PIECES_COMPLEMENTAIRES);
            self::CreerRepertoireDocumentsExternes($pathRepertoireParent, $consultationId, $org, false, Atexo_Config::getParameter('TYPE_DOC_EXTERNE_CONSULTATION'), $logger);

            // Fichiers DCE
            $c = new Criteria();
            $c->addDescendingOrderByColumn(CommonDCEPeer::ID);
            $dces = (new Atexo_Consultation_Dce())->getDces($consultationId, $org, $c);
            if (is_array($dces) && !empty($dces)) {
                $lastDCE = $dces[0];
                if ($lastDCE instanceof CommonDCE) {
                    $nomFichier = self::verifierNameFile($pathRepertoireParent, Atexo_Util::OterAccents($lastDCE->getNomDce()));

                    self::addFilesToDirTree($pathRepertoireParent, 'LAST_DCE', $lastDCE->getDce(), $nomFichier);
                    self::addFilesTimestampToDirTree($pathRepertoireParent, 'LAST_DCE', $lastDCE->getHorodatage(), $nomFichier, $lastDCE->getUntrusteddate());
                }
                //on dépile le premier DCE de la consultation (le plus récent)
                array_shift($dces);

                if (!empty($dces)) {
                    //pour des questions d'optimisations de poids des archives, on ne mets dans archive_dce que l'avant dernière version du DCE
                    $dce = $dces[0];
                    if ($dce instanceof CommonDCE) {
                        $nomFichier = self::verifierNameFile($pathDirArchiveDce, Atexo_Util::OterAccents($dce->getNomDce()));
                        self::addFilesToDirTree($pathDirArchiveDce, 'ARCHIVE_DCE', $dce->getDce(), $nomFichier);
                        self::addFilesTimestampToDirTree($pathDirArchiveDce, 'ARCHIVE_DCE', $dce->getHorodatage(), $nomFichier, $dce->getUntrusteddate());
                    }
                }
            }

            // Fichiers RC
            $c = new Criteria();
            $c->addDescendingOrderByColumn(CommonRGPeer::ID);
            $rcs = (new Atexo_Consultation_Rg())->getRgs($consultationId, $org, $c);
            if (is_array($rcs) && !empty($rcs)) {
                $lastRC = $rcs[0];
                if ($lastRC instanceof CommonRG) {
                    $nomFichier = self::verifierNameFile($pathRepertoireParent, Atexo_Util::OterAccents($lastRC->getNomFichier()));
                    self::addFilesToDirTree($pathRepertoireParent, 'LAST_RC', $lastRC->getRg(), $nomFichier);
                    self::addFilesTimestampToDirTree($pathRepertoireParent, 'LAST_RC', $lastRC->getHorodatage(), $nomFichier, $lastRC->getUntrusteddate());
                }
                array_shift($rcs);
                if (!empty($rcs)) {
                    foreach ($rcs as $rc) {
                        if ($rc instanceof CommonRG) {
                            $nomFichier = self::verifierNameFile($pathDirArchiveRc, Atexo_Util::OterAccents($rc->getNomFichier()));
                            self::addFilesToDirTree($pathDirArchiveRc, 'ARCHIVE_RC', $rc->getRg(), $nomFichier);
                            self::addFilesTimestampToDirTree($pathDirArchiveRc, 'ARCHIVE_RC', $rc->getHorodatage(), $nomFichier, $rc->getUntrusteddate());
                        }
                    }
                }
            }

            // Fichiers Complements
            $c = new Criteria();
            $c->addDescendingOrderByColumn(CommonComplementPeer::ID);
            $complements = (new Atexo_Consultation_Complements())->getComplements($consultationId, $org, $c);
            if (is_array($complements) && !empty($complements)) {
                foreach ($complements as $complement) {
                    if ($complement instanceof CommonComplement) {
                        $nomFichier = self::verifierNameFile($pathDirArchiveComp, Atexo_Util::OterAccents($complement->getNomFichier()));
                        self::addFilesToDirTree($pathDirArchiveComp, 'ARCHIVE_COMPLEMENT', $complement->getComplement(), $nomFichier);
                        self::addFilesTimestampToDirTree($pathDirArchiveComp, 'ARCHIVE_COMPLEMENT', $complement->getHorodatage(), $nomFichier, $complement->getUntrusteddate());
                    }
                }
            }
        } catch (Exception $e) {
            $erreurMsg = 'Erreur = ' . $e->getMessage() . PHP_EOL . 'Trace = ' . $e->getTraceAsString();
            $logger->error("Erreur lors de la generation de l'arborescence du dossier des pieces de consultation : {ref_cons = $consultationId}, {organisme = $org}, {pathRepertoireParent = $pathRepertoireParent}" . PHP_EOL . $erreurMsg);
        }
        $logger->info("Fin ajout des pieces de DCE, RC et COMPLEMENT a l'archive");
    }

    /**
     * Permet de creer l'arborescence du dossier message echange.
     *
     * @param array  $echanges : liste des echanges
     * @param string $pathDir  : repertoire des fichiers messages echanges
     * @param string $nomDirPj : nom du repertoire des pj des messages
     * @param Logger $logger   : logger
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-megalis
     *
     * @copyright Atexo 2016
     */
    public function creerArborescenceDossierMessagesEchanges($echanges, $pathDir, $nomDirPj, $logger = null)
    {
        if (empty($logger)) {
            $logger = Atexo_LoggerManager::getLogger('archivage');
        }
        try {
            $logger->info("Debut creation de l'arborescence du repertoire des echanges dans le repertoire '$pathDir'");
            if (is_array($echanges) && !empty($echanges)) {
                foreach ($echanges as $key => $echange) {
                    $idMessage = $key;
                    $dateMessage = substr($echange['DateEchange'], '0', '10');
                    $ssDossier = 'echange_' . $idMessage . '_' . $dateMessage;
                    $logger->info("- Debut ajout echange '$ssDossier'");
                    //Création des sous dossiers echange_[n?incr?mental]_[date:format AAAA-MM-JJ] au dossier $pathDir
                    self::addFolder($pathDir, $ssDossier);

                    //Ajout du fichier contenu des messages 'MessageEchange.txt'
                    $dateCreation = Atexo_Util::getDateTimeByFormat($echange['DateEchange']);
                    self::addFilesToDirTree($pathDir . DIRECTORY_SEPARATOR . $ssDossier, 'contenusMessagesEchanges', $echange['MessageEchangeFile'], 'MessageEchange.txt', null, false, $dateCreation);

                    //Création des sous dossiers '$nomDirPj' au dossier 'echange_[n?incr?mental]_[date:format AAAA-MM-JJ]' et ajout des pj au dossier '$nomDirPj'
                    self::addFolder($pathDir . DIRECTORY_SEPARATOR . $ssDossier, $nomDirPj);
                    $pathPj = $echange['PiecesJointes'];
                    if (is_array($pathPj) && !empty($pathPj)) {
                        foreach ($pathPj as $pj) {
                            $pathDossierPj = $pathDir . DIRECTORY_SEPARATOR . $ssDossier . DIRECTORY_SEPARATOR . $nomDirPj;
                            $nomPj = stripslashes(self::verifierNameFile($pathDossierPj, Atexo_Util::OterAccents($pj[1])));

                            self::addFilesToDirTree($pathDossierPj, 'piecesJointesEchanges', $pj[0], $nomPj, null, false, $dateCreation);
                        }
                    }
                }
            } else {
                $logger->info("Aucun echange a mettre dans le dossier '$pathDir'");
            }
        } catch (Exception $e) {
            $erreurMsg = 'Erreur = ' . $e->getMessage() . PHP_EOL . 'Trace = ' . $e->getTraceAsString();
            $logger->error("Erreur lors de la generation de l'arborescence du dossier des messages '$pathDir' : " . PHP_EOL . '{echanges = ' . print_r($echanges, true) . '}' . PHP_EOL . PHP_EOL . $erreurMsg);
        }
        $logger->info("Fin creation de l'arborescence du repertoire des echanges dans le repertoire '$pathDir'");
    }

    public function creerArborescenceDossierMessagesEchangesForMessec($echanges, $pathDir, $logger = null)
    {
        if (empty($logger)) {
            $logger = Atexo_LoggerManager::getLogger('archivage');
        }
        try {
            $logger->info("Debut creation de l'arborescence du repertoire des echanges dans le repertoire '$pathDir'");
            if (is_array($echanges) && !empty($echanges)) {
                foreach ($echanges as $key => $echange) {
                    $idMessage = $key;
                    $dateMessage = substr($echange['DateEchange'], '0', '10');
                    $ssDossier = 'echange_' . $idMessage . '_' . $dateMessage;
                    $logger->info("- Debut ajout echange '$ssDossier'");
                    //Création des sous dossiers echange_[n?incr?mental]_[date:format AAAA-MM-JJ] au dossier $pathDir
                    self::addFolder($pathDir, $ssDossier);
                    $dateCreation = Atexo_Util::getDateTimeByFormat($echange['DateEchange']);

                    if (!empty($echange['PDF_ECHANGE'])) {
                        $pdf = $echange['PDF_ECHANGE'];
                        $pathDossierPdf = $pathDir . DIRECTORY_SEPARATOR . $ssDossier;
                        $nomPj = stripslashes(self::verifierNameFile($pathDossierPdf, Atexo_Util::OterAccents($pdf[1])));
                        self::addFilesToDirTree($pathDossierPdf, 'piecesJointesEchanges', $pdf[0], $nomPj, null, false, $dateCreation);
                    }

                    if (!empty($echange['PJ_MESSAGE_1'])) {
                        self::addFolder($pathDir . DIRECTORY_SEPARATOR . $ssDossier, 'PJ_MESSAGE_1');

                        $pathPj = $echange['PJ_MESSAGE_1'];
                        if (is_array($pathPj) && !empty($pathPj)) {
                            foreach ($pathPj as $pj) {
                                $pathDossierPj = $pathDir . DIRECTORY_SEPARATOR . $ssDossier . DIRECTORY_SEPARATOR . 'PJ_MESSAGE_1';
                                $nomPj = stripslashes(self::verifierNameFile($pathDossierPj, Atexo_Util::OterAccents($pj[1])));
                                self::addFilesToDirTree($pathDossierPj,
                                    'piecesJointesEchanges', $pj[0], $nomPj, null, false, $dateCreation);
                            }
                        }
                    }

                    if (!empty($echange['PJ_MESSAGE_2'])) {
                        self::addFolder($pathDir . DIRECTORY_SEPARATOR . $ssDossier, 'PJ_MESSAGE_2');
                        $pathPj = $echange['PJ_MESSAGE_2'];
                        if (is_array($pathPj) && !empty($pathPj)) {
                            foreach ($pathPj as $pj) {
                                $pathDossierPj = $pathDir . DIRECTORY_SEPARATOR . $ssDossier . DIRECTORY_SEPARATOR . 'PJ_MESSAGE_2';

                                $nomPj = stripslashes(self::verifierNameFile($pathDossierPj, Atexo_Util::OterAccents($pj[1])));
                                self::addFilesToDirTree($pathDossierPj, 'piecesJointesEchanges', $pj[0], $nomPj, null, false, $dateCreation);
                            }
                        }
                    }
                }
            } else {
                $logger->info("Aucun echange a mettre dans le dossier '$pathDir'");
            }
        } catch (Exception $e) {
            $erreurMsg = 'Erreur = ' . $e->getMessage() . PHP_EOL . 'Trace = ' . $e->getTraceAsString();
            $logger->error("Erreur lors de la generation de l'arborescence du dossier des messages '$pathDir' : " . PHP_EOL . '{echanges = ' . print_r($echanges, true) . '}' . PHP_EOL . PHP_EOL . $erreurMsg);
        }
        $logger->info("Fin creation de l'arborescence du repertoire des echanges dans le repertoire '$pathDir'");
    }

    /**
     * Permet d'ajouter les fichiers registres au sous repertoire 'registres'.
     *
     * @param string $consultationId      : reference de la consultation
     * @param string $organisme           : acronyme de l'organisme
     * @param string $dirRepertoireParent : chemin du repertoire parent
     * @param Logger $logger              : logger
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-megalis
     *
     * @copyright Atexo 2016
     */
    public function ajouterFichiersRegistres($consultationId, $organisme, $dirRepertoireParent, $logger = null)
    {
        $dirRegistre = $dirRepertoireParent . DIRECTORY_SEPARATOR . self::DIR_REGISTRES;
        if (empty($logger)) {
            $logger = Atexo_LoggerManager::getLogger('archivage');
        }
        try {
            $logger->info("Debut ajout des fichiers registres au repertoire '$dirRegistre'");

            // Creation du repertoire
            self::addFolder(self::DIR_CONSULTATION, self::DIR_REGISTRES);

            //Creation du repertoire des documents externes
            self::CreerRepertoireDocumentsExternes($dirRegistre, $consultationId, $organisme, false, Atexo_Config::getParameter('TYPE_DOC_EXTERNE_REGISTRES'), $logger);

            // Registres des questions électroniques
            $pathResgistreQuestion = (new Atexo_GenerationExcel())->generateExcelRegistre($consultationId, Atexo_Config::getParameter('REGISTRE_QUESTION_ELECTRONIQUE'), true, $organisme, false);
            $nomRegistreQuestion = self::verifierNameFile($dirRegistre, Atexo_Util::OterAccents(basename($pathResgistreQuestion)));
            self::addFilesToDirTree($dirRegistre, 'registreQuestions', $pathResgistreQuestion, $nomRegistreQuestion);

            // Registres des retraits
            $pathResgistreRetrait = (new Atexo_GenerationExcel())->generateExcelRegistre($consultationId, Atexo_Config::getParameter('REGISTRE_RETRAIT_ELECTRONIQUE'), true, $organisme, false);
            $nomRegistreRetrait = self::verifierNameFile($dirRegistre, Atexo_Util::OterAccents(basename($pathResgistreRetrait)));
            self::addFilesToDirTree($dirRegistre, 'registreRetraits', $pathResgistreRetrait, $nomRegistreRetrait);

            // Registres des dépots
            $pathResgistreDepots = (new Atexo_GenerationExcel())->generateExcelRegistre($consultationId, Atexo_Config::getParameter('REGISTRE_DEPOT_ELECTRONIQUE'), true, $organisme, false);
            $nomRegistreDepots = self::verifierNameFile($dirRegistre, Atexo_Util::OterAccents(basename($pathResgistreDepots)));
            self::addFilesToDirTree($dirRegistre, 'registreDepots', $pathResgistreDepots, $nomRegistreDepots);

            // Registre des suivis de messages
            $result = Atexo_Message::getAllEchangesDestinataireByIdRef($consultationId, $organisme, null, true, false);
            $pathResgistreSuiviMessage = (new Atexo_GenerationExcel())->generateExcelRegistreEchange($consultationId, $result, true, $organisme, false, true);
            $nomRegistreSuiviMessage = self::verifierNameFile($dirRegistre, Atexo_Util::OterAccents(basename($pathResgistreSuiviMessage)));
            self::addFilesToDirTree($dirRegistre, 'registreSuiviMessage', $pathResgistreSuiviMessage, $nomRegistreSuiviMessage);
        } catch (Exception $e) {
            $erreurMsg = 'Erreur = ' . $e->getMessage() . PHP_EOL . 'Trace = ' . $e->getTraceAsString();
            $logger->error("Erreur lors de l'ajout des fichiers aux registres : {refCons = $consultationId} , {organisme = $organisme}" . PHP_EOL . PHP_EOL . $erreurMsg);
        }
        $logger->info("Fin ajout des fichiers registres au repertoire '$dirRegistre'");
    }

    /**
     * Permet d'ajouter les fichiers des publicites aux formats libres et xml.
     *
     * @param string    $consultationId : reference de la consultation
     * @param string    $organisme      : organisme de la consultation
     * @param string    $dirRepParent   : chemin du repertoire parent
     * @param Logger    $logger         : logger
     * @param PropelPDO $connexion      : connexion
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-megalis
     *
     * @copyright Atexo 2016
     */
    public function ajouterFichiersPublicites($consultationId, $organisme, $dirRepParent, $logger = null, $connexion = null)
    {
        $dirPub = $dirRepParent . DIRECTORY_SEPARATOR . self::DIR_PUBLICITES;
        if (empty($logger)) {
            $logger = Atexo_LoggerManager::getLogger('archivage');
        }
        if (empty($connexion)) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        }

        $logger->info("Debut creation de l'arborescence du repertoire des fichiers de publicite dans le repertoire '$dirPub'");
        try {
            // Creation du repertoire des publicites 'DIR_PUBLICITES'
            self::addFolder($dirRepParent, self::DIR_PUBLICITES);
            // Creation du repertoire des documents externes 'DIR_DOCUMENTS_EXTERNES'
            self::CreerRepertoireDocumentsExternes($dirPub, $consultationId, $organisme, false, Atexo_Config::getParameter('TYPE_DOC_EXTERNE_PUBLICITES'), $logger);

            //Publicite au format XML
            $listeAnnonce = (new Atexo_Publicite_Destinataire())->retreiveAnnonceWithFormXml($consultationId, $organisme);
            if (is_array($listeAnnonce) && !empty($listeAnnonce)) {
                foreach ($listeAnnonce as $oneAnnonce) {
                    $referentielFormXml = $oneAnnonce->getCommonReferentielFormXml();
                    if ($referentielFormXml instanceof CommonReferentielFormXml) {
                        $annonceMoniteur = (new Atexo_Publicite_Destinataire())->retreiveAnnonceMoniteurByIdFormXml($referentielFormXml->getId(), Atexo_CurrentUser::getOrganismAcronym());
                        if ($annonceMoniteur instanceof CommonAnnonceMoniteur) {
                            if ('2' == $annonceMoniteur->getStatutDestinataire() || '3' == $annonceMoniteur->getStatutDestinataire() || '4' == $annonceMoniteur->getStatutDestinataire()) {
                                if ('2' == $annonceMoniteur->getStatutDestinataire()) {
                                    $date = $annonceMoniteur->getDatepub();
                                } elseif ('3' == $annonceMoniteur->getStatutDestinataire() || '4' == $annonceMoniteur->getStatutDestinataire()) {
                                    $date = $annonceMoniteur->getDateMaj();
                                }
                                $nameFileMoniteur = self::verifierNameFile($dirPub, 'Annonce_Moniteur_BOAMP_' . $date . '.xml');
                                $pathFileFormulaireMoniteur = (new Atexo_Consultation_ArchivePublicite())->getFileXmlMoniteur($annonceMoniteur->getXmlMoniteur(), $organisme);

                                $dateCreation = Atexo_Util::getDateTimeByFormat($annonceMoniteur->getDateCreation());
                                self::addFilesToDirTree($dirPub, 'PUBLICITES_XML', $pathFileFormulaireMoniteur, $nameFileMoniteur, null, false, $dateCreation);
                            }
                        }
                        $nameFile = self::verifierNameFile($dirPub, Atexo_Util::OterAccents($oneAnnonce->getNomFichierXml()));
                        $pathFileFormulaire = (new Atexo_Consultation_ArchivePublicite())->getFormulaireXml($referentielFormXml, $connexion);
                        self::addFilesToDirTree($dirPub, 'PUBLICITES_XML', $pathFileFormulaire, $nameFile);
                    }
                }
            }

            // Publicite au format libre
            $listeFormAnnonceJal = (new Atexo_Publicite_AnnonceJAL())->retreiveListFormulaireLibre($consultationId, $organisme);
            if (is_array($listeFormAnnonceJal) && !empty($listeFormAnnonceJal)) {
                $index = 0;
                foreach ($listeFormAnnonceJal as $oneAnnoneJal) {
                    if ($oneAnnoneJal instanceof CommonAnnonceJAL) {
                        ++$index;
                        $pathFileFormatLibre = Atexo_Consultation_ArchivePublicite::getFormatLibreAnnonceXml($oneAnnoneJal, $organisme);

                        $dateCreation = Atexo_Util::getDateTimeByFormat($oneAnnoneJal->getDateCreation());
                        self::addFilesToDirTree($dirPub, 'PUBLICITES_LIBRE', $pathFileFormatLibre, 'Formulaire_Libre_Annonce_' . $index . '.xml', null, false, $dateCreation);
                    }
                }
            }
        } catch (Exception $e) {
            $erreurMsg = 'Erreur = ' . $e->getMessage() . PHP_EOL . 'Trace = ' . $e->getTraceAsString();
            $logger->error("Erreur lors de l'ajout des fichiers de publicite au repertoire '$dirPub': {refCons = $consultationId} , {organisme = $organisme}" . PHP_EOL . PHP_EOL . $erreurMsg);
        }
        $logger->info("Fin creation de l'arborescence du repertoire des fichiers de publicite dans le repertoire '$dirPub'");
    }

    /**
     * Permet de construire le nom de l'archive.
     *
     * @param string    $consultationId : reference de la consultation
     * @param string    $organisme      : organisme de la consultation
     * @param PropelPDO $connexion      : connexion
     * @param Logger    $logger
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-megalis
     *
     * @copyright Atexo 2016
     */
    public function buildNameFileArchive($consultationId, $organisme, $connexion = null, $logger = null)
    {
        try {
            if (empty($logger)) {
                $logger = Atexo_LoggerManager::getLogger('archivage');
            }
            if (empty($connexion)) {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
            }
            $consultation = BaseCommonConsultationQuery::create()->filterById($consultationId)->filterByOrganisme($organisme)->findOne($connexion);
            if ($consultation instanceof CommonConsultation) {
                    parent::__construct($consultation->getReferenceUtilisateur(true));
            }
        } catch (Exception $e) {
            $logger->error("Erreur lors de la creation du nom de l'archive: {refCons = $consultationId} , {organisme = $organisme}" . PHP_EOL . PHP_EOL . 'Erreur = ' . $e->getMessage() . PHP_EOL . 'Trace = ' . $e->getTraceAsString());
        }
    }

    /**
     * Permet de construire l'arborescence du repertoire 'DIR_CANDIDATURES_OFFRES_DECISION'.
     *
     * @param string    $consultationId : reference de la consultation
     * @param string    $organisme      : organisme de la consultation
     * @param PropelPDO $connexion      : connexion
     * @param Logger    $logger         : logger
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-megalis
     *
     * @copyright Atexo 2016
     */
    public function creerArborescenceDossierCandidaturesOffresDecision($consultationId, $organisme, $logger = null, $connexion = null)
    {
        if (empty($logger)) {
            $logger = Atexo_LoggerManager::getLogger('archivage');
        }
        $logger->info("Debut creation de l'arborescence du repertoire '" . self::DIR_CANDIDATURES_OFFRES_DECISION . "' de la consultation {reference = '$consultationId'} , {organisme = $organisme}");
        try {
            if (empty($connexion)) {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
            }

            //Creation du repertoire 'DIR_CANDIDATURES_OFFRES_DECISION'
            self::addTreeFolder(self::DIR_CANDIDATURES_OFFRES_DECISION);

            // Creation de l'arborescence du repertoire 'DIR_CANDIDATURES'
            self::creerArborescenceRepertoireCandidature($consultationId, $organisme, self::DIR_CANDIDATURES_OFFRES_DECISION, $logger, $connexion);

            //Creation de l'arborescence du repertoire 'DIR_DOCUMENTS_DECISION'
            self::creerArborescenceDossierDocumentsDecision($consultationId, $organisme, self::DIR_CANDIDATURES_OFFRES_DECISION, $logger);

            //Creation du repertoire des offres de la consultation et des lots
            self::creerArborescenceRepertoireOffres($consultationId, $organisme, self::DIR_CANDIDATURES_OFFRES_DECISION, $logger, $connexion);
        } catch (Exception $e) {
            $erreurMsg = 'Erreur = ' . $e->getMessage() . PHP_EOL . 'Trace = ' . $e->getTraceAsString();
            $logger->error("Erreur lors de la generation de l'arborescence du repertoire '" . self::DIR_CANDIDATURES_OFFRES_DECISION . "': {ref_cons = $consultationId}, {organisme = $organisme}" . PHP_EOL . $erreurMsg);
        }
        $logger->info("Fin creation de l'arborescence du repertoire '" . self::DIR_CANDIDATURES_OFFRES_DECISION . "' de la consultation {reference = '$consultationId'} , {organisme = $organisme}");
    }

    /**
     * Permet de construire l'arborescence du repertoire des offres de la consultation et lots.
     *
     * @param string    $consultationId : reference de la consultation
     * @param string    $organisme      : organisme de la consultation
     * @param PropelPDO $connexion      : connexion
     * @param Logger    $logger         : logger
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-megalis
     *
     * @copyright Atexo 2016
     */
    public function creerArborescenceRepertoireOffres($consultationId, $organisme, $cheminRepertoireParent, $logger = null, $connexion = null)
    {
        if (empty($logger)) {
            $logger = Atexo_LoggerManager::getLogger('archivage');
        }
        $logger->info("Debut creation de l'arborescence du repertoire '" . $cheminRepertoireParent . DIRECTORY_SEPARATOR . "[repertoire offres consultation et lots]' de la consultation {reference = '$consultationId'} , {organisme = $organisme}");
        try {
            //Recuperation de la consultation
            $consultation = BaseCommonConsultationQuery::create()->filterById($consultationId)->filterByOrganisme($organisme)->findOne($connexion);
            if ($consultation instanceof CommonConsultation) {
                if (0 == strcmp($consultation->getAlloti(), '1')) {
                    $lots = $consultation->getAllLots($connexion);
                    if (is_array($lots) && !empty($lots)) {
                        foreach ($lots as $lot) {
                            // Creation du repertoire d'offre pour le lot
                            $repertoireOffres = $consultation->getReferenceUtilisateur() . '_lot-' . $lot->getLot();
                            self::addFolder($cheminRepertoireParent, $repertoireOffres, '', '', null, [], basename($cheminRepertoireParent), $lot->getLot());

                            if ($lot instanceof CommonCategorieLot) {
                                $repOffresRefConsLot = $cheminRepertoireParent . DIRECTORY_SEPARATOR . $repertoireOffres;
                                self::creerRepertoireOffreConsultationLot($consultationId, $organisme, $lot->getLot(), $repOffresRefConsLot, $logger, $connexion);
                            }
                        }
                    }
                } else {
                    // Creation du repertoire d'offre pour la consultation
                    $repertoireOffres = $consultation->getReferenceUtilisateur();
                    self::addFolder($cheminRepertoireParent, $repertoireOffres, '', '', null, [], basename($cheminRepertoireParent));

                    $repOffresRefConsLot = $cheminRepertoireParent . DIRECTORY_SEPARATOR . $repertoireOffres;
                    self::creerRepertoireOffreConsultationLot($consultationId, $organisme, '0', $repOffresRefConsLot, $logger, $connexion);
                }
            }
        } catch (Exception $e) {
            $erreurMsg = 'Erreur = ' . $e->getMessage() . PHP_EOL . 'Trace = ' . $e->getTraceAsString();
            $logger->error("Erreur lors de la generation de l'arborescence du repertoire '" . $cheminRepertoireParent . DIRECTORY_SEPARATOR . "[repertoire offres consultation et lots]': {ref_cons = $consultationId}, {organisme = $organisme}" . PHP_EOL . $erreurMsg);
        }
        $logger->info("Fin creation de l'arborescence du repertoire '" . $cheminRepertoireParent . DIRECTORY_SEPARATOR . "[repertoire offres consultation et lots]' de la consultation {reference = '$consultationId'} , {organisme = $organisme}");
    }

    /**
     * Permet de construire l'arborescence du repertoire 'DIR_CANDIDATURES'.
     *
     * @param string    $consultationId : reference de la consultation
     * @param string    $organisme      : organisme de la consultation
     * @param PropelPDO $connexion      : connexion
     * @param Logger    $logger         : logger
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-megalis
     *
     * @copyright Atexo 2016
     */
    public function creerArborescenceRepertoireCandidature($consultationId, $organisme, $cheminRepertoireParent, $logger = null, $connexion = null)
    {
        if (empty($logger)) {
            $logger = Atexo_LoggerManager::getLogger('archivage');
        }
        $cheminRepertoireCandidature = $cheminRepertoireParent . DIRECTORY_SEPARATOR . self::DIR_CANDIDATURES;
        $logger->info("Debut creation de l'arborescence du repertoire '" . $cheminRepertoireCandidature . "' de la consultation {reference = '$consultationId'} , {organisme = $organisme}");
        try {
            if (empty($connexion)) {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
            }

            //Creation des sous repertoires au repertoire 'DIR_CANDIDATURES_OFFRES_DECISION'
            self::addFolder($cheminRepertoireParent, self::DIR_CANDIDATURES);

            //Creation de l'arborescence du repertoire 'DIR_ADMISSIBILITE'
            self::creerArborescenceRepertoireAdmissibilite($consultationId, $organisme, $cheminRepertoireCandidature, $logger);

            //Contenu du repertoire 'DIR_CANDIDATURES'
            self::CreerArborescenceDossierCandidaturesRetenuesEtNon($consultationId, $organisme, $cheminRepertoireCandidature, $connexion, $logger);
            self::CreerRepertoireDocumentsExternes($cheminRepertoireCandidature, $consultationId, $organisme, false, Atexo_Config::getParameter('TYPE_DOC_EXTERNE_CANDIDATURE'), $logger);
        } catch (Exception $e) {
            $erreurMsg = 'Erreur = ' . $e->getMessage() . PHP_EOL . 'Trace = ' . $e->getTraceAsString();
            $logger->error("Erreur lors de la generation de l'arborescence du repertoire '" . $cheminRepertoireCandidature . "': {ref_cons = $consultationId}, {organisme = $organisme}" . PHP_EOL . $erreurMsg);
        }
        $logger->info("Fin creation de l'arborescence du repertoire '" . $cheminRepertoireCandidature . "' de la consultation {reference = '$consultationId'} , {organisme = $organisme}");
    }

    /**
     * Permet de creer l'arborescence du repertoire 'DIR_ADMISSIBILITE'.
     *
     * @param string    $consultationId : reference de la consultation
     * @param string    $organisme      : organisme de la consultation
     * @param PropelPDO $connexion      : connexion
     * @param Logger    $logger         : logger
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-megalis
     *
     * @copyright Atexo 2016
     */
    public function creerArborescenceRepertoireAdmissibilite($consultationId, $organisme, $cheminRepertoireParent, $logger = null)
    {
        $cheminRepertoireAdmissibilite = $cheminRepertoireParent . DIRECTORY_SEPARATOR . self::DIR_ADMISSIBILITE;
        if (empty($logger)) {
            $logger = Atexo_LoggerManager::getLogger('archivage');
        }
        $logger->info("Debut creation de l'arborescence du repertoire '" . $cheminRepertoireAdmissibilite . "' de la consultation {reference = '$consultationId'} , {organisme = $organisme}");
        try {
            // Creation de l'arborescence du repertoire 'DIR_ADMISSIBILITE'
            self::addFolder($cheminRepertoireParent, self::DIR_ADMISSIBILITE);

            //Creation repertoire 'DIR_DEMANDES_COMPLEMENTS_CANDIDATURES' et son contenu
            self::creerArborescenceMessagesEchanges($consultationId, $organisme, $cheminRepertoireAdmissibilite, self::DIR_DEMANDES_COMPLEMENTS_CANDIDATURES, self::DIR_PIECES_JOINTES, Atexo_Config::getParameter('ID_MESSAGE_COURRIER_DEMANDE_COMPLEMENT_CANDIDATURE'), $logger);

            //Creation repertoire 'DIR_REJET_CANDIDATURES' et son contenu
            self::creerArborescenceMessagesEchanges($consultationId, $organisme, $cheminRepertoireAdmissibilite, self::DIR_REJET_CANDIDATURES, self::DIR_PIECES_JOINTES, Atexo_Config::getParameter('ID_MESSAGE_COURRIER_REJET_CANDIDATURE'), $logger);

            //Creation repertoire 'DIR_LETTRE_ADMISSION' et son contenu
            self::creerArborescenceMessagesEchanges($consultationId, $organisme, $cheminRepertoireAdmissibilite, self::DIR_LETTRE_ADMISSION, self::DIR_PIECES_JOINTES, Atexo_Config::getParameter('ID_MESSAGE_COURRIER_LETTRE_ADMISSION_CANDIDATURE'));
        } catch (Exception $e) {
            $erreurMsg = 'Erreur = ' . $e->getMessage() . PHP_EOL . 'Trace = ' . $e->getTraceAsString();
            $logger->error("Erreur lors de la generation de l'arborescence du repertoire '" . $cheminRepertoireAdmissibilite . "': {ref_cons = $consultationId}, {organisme = $organisme}" . PHP_EOL . $erreurMsg);
        }
        $logger->info("Fin creation de l'arborescence du repertoire '" . $cheminRepertoireAdmissibilite . "' de la consultation {reference = '$consultationId'} , {organisme = $organisme}");
    }

    /**
     * Permet de construire l'arborescence des repertoires 'DIR_CANDIDATURES_NON_RETENUES' et 'DIR_CANDIDATURES_RETENUES'.
     *
     * @param string    $consultationId   : reference de la consultation
     * @param string    $organisme        : organisme de la consultation
     * @param string    $repertoireParent : chemin du repertoire parent
     * @param PropelPDO $connexion        : connexion
     * @param Logger    $logger           : logger
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-megalis
     *
     * @copyright Atexo 2016
     */
    public function CreerArborescenceDossierCandidaturesRetenuesEtNon($consultationId, $organisme, $repertoireParent, $connexion = null, $logger = null)
    {
        $dossierCandidaturesRetenues = $repertoireParent . DIRECTORY_SEPARATOR . self::DIR_CANDIDATURES_RETENUES;
        $dossierCandidaturesNonRetenues = $repertoireParent . DIRECTORY_SEPARATOR . self::DIR_CANDIDATURES_NON_RETENUES;

        if (empty($logger)) {
            $logger = Atexo_LoggerManager::getLogger('archivage');
        }
        $logger->info("Debut creation de l'arborescence des repertoires '" . $dossierCandidaturesRetenues . "' et '" . $dossierCandidaturesNonRetenues . "' de la consultation {reference = '$consultationId'} , {organisme = $organisme}");
        try {
            if (empty($connexion)) {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
            }

            //Creation des repertoires 'DIR_CANDIDATURES_RETENUES' et 'DIR_CANDIDATURES_NON_RETENUES'
            self::addFolder($repertoireParent, self::DIR_CANDIDATURES_RETENUES);
            self::addFolder($repertoireParent, self::DIR_CANDIDATURES_NON_RETENUES);

            //Recuperation de la consultation
            $consultation = BaseCommonConsultationQuery::create()->filterById($consultationId)->filterByOrganisme($organisme)->findOne($connexion);
            if ($consultation instanceof CommonConsultation) {
                // Recuperation des offres electroniques
                $offresElectroniques = $consultation->getOffres();
                // Recuperation des offres papiers
                $offresPapiers = $consultation->getOffrePapiers();
                //fusion des deux offres
                $offres = array_merge($offresElectroniques, $offresPapiers);

                $idsOffesWithContrat = $this->getOffresWithContrat($consultation->getId());
                if (is_array($offres) && !empty($offres)) {
                    foreach ($offres as $offre) {
                        $idOffre = '';
                        $nomEntrep = '';
                        if ($offre instanceof CommonOffres) {//Offres electroniques
                            $idOffre = $offre->getId();
                            $nomEntrep = trim(Atexo_Entreprise::getNomEntreprise($offre->getEntrepriseId(), null));
                            $typeDepot = Atexo_Config::getParameter('DEPOT_ELECTRONIQUE');
                        } elseif ($offre instanceof CommonOffrePapier) {//Offres papiers
                            $nomEntrep = trim($offre->getNomEntreprise());
                            $typeDepot = Atexo_Config::getParameter('DEPOT_PAPIER');
                        }
                        $nomEntrep = Atexo_Util::toUtf8($nomEntrep);

                        $selectionEntrepriseQuery = new CommonTDecisionSelectionEntrepriseQuery();
                        $selectionEse = $selectionEntrepriseQuery->getDecisionSelectionEntrepriseByOffre($idOffre, $organisme, $typeDepot);
                        if (in_array($offre->getId(), $idsOffesWithContrat) || $selectionEse instanceof CommonTDecisionSelectionEntreprise) { // Si offre retenue :
                            $dossier = $dossierCandidaturesRetenues;
                        } else {
                            $dossier = $dossierCandidaturesNonRetenues;
                        }
                        //Creation du repertoire des fichiers des reponses
                        $cheminDossierReponse = $nomEntrep;
                        $this->addFolder($dossier, $cheminDossierReponse, '', '', null, [], basename($dossier));
                        if ($offre instanceof CommonOffres) {
                            $enveloppes = $offre->getCommonEnveloppes(false, Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE'));

                            $xmlStr = $offre->getXmlString();
                            $pos = strpos($xmlStr, 'typeSignature');
                            if ($pos > 0) {
                                $typeSignature = 'XML';
                            }

                            $numeroFichier = 1;
                            //Fichiers de la candidature
                            if (is_array($enveloppes) && !empty($enveloppes)) {
                                foreach ($enveloppes as $enveloppe) {
                                    $c = new Criteria();
                                    $c->add(CommonFichierEnveloppePeer::ID_ENVELOPPE, $enveloppe->getIdEnveloppeElectro());
                                    $c->add(CommonFichierEnveloppePeer::ORGANISME, $organisme);
                                    $fichiers = CommonFichierEnveloppePeer::doSelect($c, $connexion);
                                    if (is_array($fichiers) && !empty($fichiers)) {
                                        foreach ($fichiers as $fichier) {
                                            if ($fichier instanceof CommonFichierEnveloppe) {
                                                if ('ACE' != $fichier->getTypeFichier() && 'AE' != $fichier->getTypeFichier()) {
                                                    $nomFichier = self::verifierNameFile($dossier . '/' . $cheminDossierReponse, str_pad($numeroFichier, 2, '0', STR_PAD_LEFT) . '-' . Atexo_Util::OterAccents($fichier->getNomFichier()));
                                                    self::addFilesToDirTree($dossier . DIRECTORY_SEPARATOR . $cheminDossierReponse, 'FICHIER_ENV_CANDIDATURE', $fichier->getIdBlob(), $nomFichier);
                                                    if ('' != $fichier->getSignatureFichier()) {
                                                        self::addSignatureToDirTree($dossier . DIRECTORY_SEPARATOR . $cheminDossierReponse, 'FICHIER_ENV_CANDIDATURE', $fichier->getSignatureFichier(), Atexo_Util::OterAccents($fichier->getNomFichierSignature('sig', $nomFichier)), null, true);
                                                    }
                                                }
                                                ++$numeroFichier;
                                            }
                                        }
                                    }

                                    //Rapport de verification de la signature
                                    if ($consultation->getSignatureOffre()) {
                                        $arrayInfosCons = (new Atexo_Consultation())->getInfosConsPrRapportSignature(null, $consultation);
                                        $arrayInfosPli = (new Atexo_Consultation_Responses())->GetDetailsPlis($enveloppe->getIdEnveloppeElectro(), $enveloppe->getTypeEnv(), null, null, $consultation);
                                        $avecSignature = $consultation->getSignatureOffre();
                                        $enveloppeCrypte = $enveloppe->getCryptage();
                                        $arrayInfosFichierSoumis = (new Atexo_Consultation_Responses())->getInfosCertif($fichiers, $avecSignature, $typeSignature, $enveloppeCrypte);
                                        $nomFichier = self::verifierNameFile($dossier . DIRECTORY_SEPARATOR . $cheminDossierReponse, Atexo_Config::getParameter('NOM_FICHIER_RAPPORT_VERIF_SIGNATURE') . '_' . (new CommonEnveloppe())->getLibelleTypeEnveloppe($enveloppe->getTypeEnv()) . '.pdf');
                                        $pathFile = Atexo_Config::getParameter('COMMON_TMP') . DIRECTORY_SEPARATOR . Atexo_Config::getParameter('NOM_FICHIER_RAPPORT_VERIF_SIGNATURE') . '_' . uniqid() . '.pdf';
                                        if (is_array($arrayInfosPli) && is_array($arrayInfosCons) && is_array($arrayInfosFichierSoumis)) {
                                            $path = (new Atexo_Consultation_Responses())->printRapportSignaturePdf($arrayInfosCons, $arrayInfosPli, $arrayInfosFichierSoumis, true, $pathFile);
                                        }
                                        self::addFilesToDirTree($dossier . DIRECTORY_SEPARATOR . $cheminDossierReponse, 'FICHIER_ENV_CANDIDATURE', $path, $nomFichier);
                                    }
                                }
                            }
                        }

                        //Candidatures MPS
                        if (Atexo_Module::isEnabled('MarchePublicSimplifie') && $consultation->getMarchePublicSimplifie()) {
                            $candidatureMpsObject = (new Atexo_MarchePublicSimplifie())->retrieveCandidatureMps($offre->getId(), $consultation->getId());
                            if ($candidatureMpsObject) {
                                $nomFichier = self::verifierNameFile($dossier . DIRECTORY_SEPARATOR . $cheminDossierReponse, Prado::localize('DEFINE_CANDIDATURE_MPS'));
                                self::addFilesToDirTree($dossier . DIRECTORY_SEPARATOR . $cheminDossierReponse, 'FICHIER_ENV_CANDIDATURE', $candidatureMpsObject->getIdBlob(), $nomFichier);
                            }
                        }

                        /**
                         * Dume
                         */
                        if (Atexo_Module::isEnabled('interfaceDume')) {
                            if (1 == $consultation->getDumeDemande()) {
                                //DUME OE
                                $candidature = Atexo_Dume_AtexoDume::retrieveCandidatureDume($offre);

                                if ($candidature instanceof CommonTCandidature) {
                                    $tDumeNumeros = Atexo_Dume_AtexoDume::getTDumeNumerosEntreprise(
                                        $candidature->getIdDumeContexte()
                                    );

                                    if (is_array($tDumeNumeros) && !empty($tDumeNumeros)) {
                                        $u = new Atexo_Util();

                                        foreach ($tDumeNumeros as $dumeNumero) {
                                            if (
                                                $dumeNumero instanceof CommonTDumeNumero &&
                                                $dumeNumero->getNumeroDumeNational()
                                            ) {
                                                if ($dumeNumero->getBlobId()) {
                                                    $nomFileDume = $dumeNumero->getNumeroDumeNational() . '.pdf';

                                                    $nomFichier = self::verifierNameFile(
                                                        $dossier . '/' . $cheminDossierReponse,
                                                        $u->OterAccents($nomFileDume)
                                                    );

                                                    self::addFilesToDirTree(
                                                        $dossier . DIRECTORY_SEPARATOR . $cheminDossierReponse,
                                                        'FICHIER_ENV_CANDIDATURE',
                                                        $dumeNumero->getBlobId(),
                                                        $nomFichier
                                                    );
                                                }

                                                if ($dumeNumero->getBlobIdXml()) {
                                                    $nomFileDume = $dumeNumero->getNumeroDumeNational() . '.xml';

                                                    $nomFichier = self::verifierNameFile(
                                                        $dossier . '/' . $cheminDossierReponse,
                                                        $u->OterAccents($nomFileDume)
                                                    );

                                                    self::addFilesToDirTree(
                                                        $dossier . DIRECTORY_SEPARATOR . $cheminDossierReponse,
                                                        'FICHIER_ENV_CANDIDATURE',
                                                        $dumeNumero->getBlobIdXml(),
                                                        $nomFichier
                                                    );
                                                }
                                            }
                                        }

                                        if (Atexo_Module::isEnabled('Groupement')) {
                                            $membreGroupements = (new Atexo_Groupement())->retrieveMemebreGroupementSansMondataire($offre->getId(), Atexo_Config::getParameter('ID_ROLE_JURIDIQUE_MANDATAIRE'));
                                            if (is_array($membreGroupements) && count($membreGroupements)) {
                                                foreach ($membreGroupements as $memebreGroupement) {
                                                    if ($memebreGroupement instanceof CommonTMembreGroupementEntreprise && $memebreGroupement->getNumerosn()) {
                                                        $dumeNumeroMembreGroupement = (new Atexo_Dume_AtexoDume())->getDumeNumeroByNumeroSn($memebreGroupement->getNumerosn());
                                                        if ($dumeNumeroMembreGroupement instanceof CommonTDumeNumero) {
                                                            if ($dumeNumeroMembreGroupement->getBlobId()) {
                                                                $nomFileDume = $dumeNumeroMembreGroupement->getNumeroDumeNational() . '.pdf';

                                                                $nomFichier = self::verifierNameFile(
                                                                    $dossier . '/' . $cheminDossierReponse,
                                                                    $u->OterAccents($nomFileDume)
                                                                );

                                                                self::addFilesToDirTree(
                                                                    $dossier . DIRECTORY_SEPARATOR . $cheminDossierReponse,
                                                                    'DUME OE',
                                                                    $dumeNumero->getBlobId(),
                                                                    $nomFichier
                                                                );
                                                            }
                                                            if ($dumeNumeroMembreGroupement->getBlobIdXml()) {
                                                                $nomFileDume = $dumeNumeroMembreGroupement->getNumeroDumeNational() . '.xml';

                                                                $nomFichier = self::verifierNameFile(
                                                                    $dossier . '/' . $cheminDossierReponse,
                                                                    $u->OterAccents($nomFileDume)
                                                                );

                                                                self::addFilesToDirTree(
                                                                    $dossier . DIRECTORY_SEPARATOR . $cheminDossierReponse,
                                                                    'DUME OE',
                                                                    $dumeNumero->getBlobIdXml(),
                                                                    $nomFichier
                                                                );
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception $e) {
            $erreurMsg = 'Erreur = ' . $e->getMessage() . PHP_EOL . 'Trace = ' . $e->getTraceAsString();
            $logger->error("Erreur lors de la generation de l'arborescence du repertoire '" . $dossierCandidaturesRetenues . "' et '" . $dossierCandidaturesNonRetenues . "' : {ref_cons = $consultationId}, {organisme = $organisme}" . PHP_EOL . $erreurMsg);
        }
        $logger->info("Fin creation de l'arborescence des repertoires '" . $dossierCandidaturesRetenues . "' et '" . $dossierCandidaturesNonRetenues . "' de la consultation {reference = '$consultationId'} , {organisme = $organisme}");
    }

    /**
     * Permet de creer l'arborescence du repertoire des offres de la consultaton ou d'un lot donne.
     *
     * @param string    $consultationId      : reference de la consultation
     * @param string    $organisme           : organisme
     * @param string    $numLot              : numero du lot
     * @param string    $repOffresRefConsLot : repertoire des offres de la consultation ou du lot
     * @param Logger    $logger              : logger
     * @param PropelPDO $connexion           : connexion
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-megalis
     *
     * @copyright Atexo 2016
     */
    public function creerRepertoireOffreConsultationLot($consultationId, $organisme, $numLot, $repOffresRefConsLot, $logger = null, $connexion = null)
    {
        if (empty($logger)) {
            $logger = Atexo_LoggerManager::getLogger('archivage');
        }
        $logger->info("Debut creation de l'arborescence du repertoire '$repOffresRefConsLot' de la consultation {reference = '$consultationId'} , {organisme = $organisme}");
        try {
            if (empty($connexion)) {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
            }

            // Creation arborescence du repertoire 'DIR_RECEPTION_EXAMEN_OFFRES'
            self::creerArborescenceReceptionExamenOffres($consultationId, $organisme, $numLot, $repOffresRefConsLot, $logger, $connexion);

            // Creation arborescence repertoire 'DIR_DECISIONS'
            self::CreerArborescenceRepertoireDecisionsOffres($consultationId, $organisme, $numLot, $repOffresRefConsLot, $logger);
        } catch (Exception $e) {
            $erreurMsg = "Erreur lors de la generation de l'arborescence du repertoire '" . self::DIR_OFFRES_NON_RETENUES . "' et '" . self::DIR_OFFRES_RETENUES . "'";
            $erreurMsg .= " : {ref_cons = $consultationId}, {organisme = $organisme}, {lot = $numLot}, {repOffresRefConsLot = $repOffresRefConsLot}";
            $erreurMsg .= PHP_EOL . 'Erreur = ' . $e->getMessage() . PHP_EOL . 'Trace = ' . $e->getTraceAsString();
            $logger->error($erreurMsg);
        }
        $logger->info("Fin creation de l'arborescence du repertoire '$repOffresRefConsLot' de la consultation {reference = '$consultationId'} , {organisme = $organisme}");
    }

    /**
     * Permet de creer l'aborescence du repertoire 'DIR_RECEPTION_EXAMEN_OFFRES'.
     *
     * @param string    $consultationId         : reference de la consultation
     * @param string    $organisme              : organisme
     * @param string    $numLot                 : numero du lot
     * @param string    $cheminRepertoireParent : chemin du repertoire parent
     * @param Logger    $logger                 : logger
     * @param PropelPDO $connexion              : connexion
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-megalis
     *
     * @copyright Atexo 2016
     */
    public function creerArborescenceReceptionExamenOffres($consultationId, $organisme, $numLot, $cheminRepertoireParent, $logger = null, $connexion = null)
    {
        if (empty($logger)) {
            $logger = Atexo_LoggerManager::getLogger('archivage');
        }
        $logger->info("Debut creation de l'arborescence du repertoire '" . $cheminRepertoireParent . DIRECTORY_SEPARATOR . self::DIR_RECEPTION_EXAMEN_OFFRES . "' de la consultation {reference = '$consultationId'} , {organisme = $organisme}, {numLot = $numLot}");
        try {
            if (empty($connexion)) {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
            }

            // Creation repertoire 'DIR_RECEPTION_EXAMEN_OFFRES'
            self::addFolder($cheminRepertoireParent, self::DIR_RECEPTION_EXAMEN_OFFRES);

            // Creation de l'arborescence du repertoire 'DIR_OUVERTURE_PLIS_ANALYSE'
            self::creerArborescenceOuverturePlisAnalyse($consultationId, $organisme, $numLot, $cheminRepertoireParent, $logger, $connexion);

            //Creation de l'arborescence des repertoires 'DIR_OFFRES_RETENUES' et 'DIR_OFFRES_NON_RETENUES'
            self::creerRepertoireOffresRetenuesEtNonRetenues($consultationId, $organisme, $numLot, $cheminRepertoireParent, $logger, $connexion);
        } catch (Exception $e) {
            $erreurMsg = "Erreur lors de la generation de l'arborescence du repertoire '" . self::DIR_OFFRES_NON_RETENUES . "' et '" . self::DIR_OFFRES_RETENUES . "'";
            $erreurMsg .= " : {ref_cons = $consultationId}, {organisme = $organisme}, {lot = $numLot}, {repertoireParent = $cheminRepertoireParent}";
            $erreurMsg .= PHP_EOL . 'Erreur = ' . $e->getMessage() . PHP_EOL . 'Trace = ' . $e->getTraceAsString();
            $logger->error($erreurMsg);
        }
        $logger->info("Fin creation de l'arborescence du repertoire '" . $cheminRepertoireParent . DIRECTORY_SEPARATOR . self::DIR_RECEPTION_EXAMEN_OFFRES . "' de la consultation {reference = '$consultationId'} , {organisme = $organisme}, {numLot = $numLot}");
    }

    /**
     * Permet de creer l'aborescence du repertoire 'DIR_OUVERTURE_PLIS_ANALYSE'.
     *
     * @param string    $consultationId         : reference de la consultation
     * @param string    $organisme              : organisme
     * @param string    $numLot                 : numero du lot
     * @param string    $cheminRepertoireParent : chemin du repertoire parent
     * @param Logger    $logger                 : logger
     * @param PropelPDO $connexion              : connexion
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-megalis
     *
     * @copyright Atexo 2016
     */
    public function creerArborescenceOuverturePlisAnalyse($consultationId, $organisme, $numLot, $cheminRepertoireParent, $logger = null, $connexion = null)
    {
        if (empty($logger)) {
            $logger = Atexo_LoggerManager::getLogger('archivage');
        }
        $logger->info("Debut creation de l'arborescence du repertoire '" . $cheminRepertoireParent . DIRECTORY_SEPARATOR . self::DIR_OUVERTURE_PLIS_ANALYSE . "' de la consultation {reference = '$consultationId'} , {organisme = $organisme}, {numLot = $numLot}");
        try {
            if (empty($connexion)) {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
            }
            // Creation du repertoire 'DIR_OUVERTURE_PLIS_ANALYSE'
            $this->addFolder($cheminRepertoireParent . DIRECTORY_SEPARATOR . self::DIR_RECEPTION_EXAMEN_OFFRES, self::DIR_OUVERTURE_PLIS_ANALYSE);

            // Creation du repertoire des documents externes dans le repertoire 'DIR_OUVERTURE_PLIS_ANALYSE'
            $dirRepertoireOuvPlisAnalyse = $cheminRepertoireParent . DIRECTORY_SEPARATOR . self::DIR_RECEPTION_EXAMEN_OFFRES . DIRECTORY_SEPARATOR . self::DIR_OUVERTURE_PLIS_ANALYSE;
            self::CreerRepertoireDocumentsExternes($dirRepertoireOuvPlisAnalyse, $consultationId, $organisme, $numLot, Atexo_Config::getParameter('TYPE_DOC_EXTERNE_OUVERTURE_PLIS_ANALYSE'), $logger);
        } catch (Exception $e) {
            $erreurMsg = "Erreur lors de la generation de l'arborescence du repertoire '" . self::DIR_OUVERTURE_PLIS_ANALYSE;
            $erreurMsg .= " : {ref_cons = $consultationId}, {organisme = $organisme}, {lot = $numLot}, {repertoireParent = $cheminRepertoireParent}";
            $erreurMsg .= PHP_EOL . 'Erreur = ' . $e->getMessage() . PHP_EOL . 'Trace = ' . $e->getTraceAsString();
            $logger->error($erreurMsg);
        }
        $logger->info("Fin creation de l'arborescence du repertoire '" . $cheminRepertoireParent . DIRECTORY_SEPARATOR . self::DIR_OUVERTURE_PLIS_ANALYSE . "' de la consultation {reference = '$consultationId'} , {organisme = $organisme}, {numLot = $numLot}");
    }

    /**
     * Permet de creer l'arborescence des repertoires 'DIR_OFFRES_NON_RETENUES' et 'DIR_OFFRES_RETENUES'.
     *
     * @param string    $consultationId         : reference de la consultation
     * @param string    $organisme              : organisme
     * @param string    $numLot                 : numero du lot
     * @param string    $cheminRepertoireParent : chemin du repertoire parent
     * @param Logger    $logger                 : logger
     * @param PropelPDO $connexion              : connexion
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-megalis
     *
     * @copyright Atexo 2016
     */
    public function creerRepertoireOffresRetenuesEtNonRetenues($consultationId, $organisme, $numLot, $cheminRepertoireParent, $logger = null, $connexion = null)
    {
        if (empty($logger)) {
            $logger = Atexo_LoggerManager::getLogger('archivage');
        }
        $logger->info("Debut creation de l'arborescence des repertoires '" . self::DIR_OFFRES_NON_RETENUES . "' et '" . self::DIR_OFFRES_RETENUES . "' de la consultation {reference = '$consultationId'} , {organisme = $organisme}, {numLot = $numLot}");
        try {
            if (empty($connexion)) {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
            }

            //Recuperation de la consultation
            $consultation = BaseCommonConsultationQuery::create()->filterById($consultationId)->filterByOrganisme($organisme)->findOne($connexion);
            if ($consultation instanceof CommonConsultation) {
                // Recuperation des offres électroniques
                $offresElectroniques = $consultation->getOffres();
                // Recuperation des offres papiers
                $offresPapiers = $consultation->getOffrePapiers();
                // Fusion des deux offres
                $offres = array_merge($offresElectroniques, $offresPapiers);
                $idsOffesWithContrat = $this->getOffresWithContrat($consultation->getId());
                if (is_array($offres) && !empty($offres)) {
                    foreach ($offres as $offre) {
                        $idEntrep = '';
                        $nomEntrep = '';
                        if ($offre instanceof CommonOffres) {
                            $idEntrep = $offre->getEntrepriseId();
                            $nomEntrep = trim(Atexo_Entreprise::getNomEntreprise($idEntrep, null));
                            $typeDepot = Atexo_Config::getParameter('DEPOT_ELECTRONIQUE');
                        } else {
                            //instanceof OffrePapier
                            $entreprise = (new Atexo_Entreprise())->retrieveCompanyBdeBySiren(substr($offre->getSiret(), 0, 9));
                            if ($entreprise instanceof Entreprise) {
                                $idEntrep = $entreprise->getId();
                            }
                            $nomEntrep = trim($offre->getNomEntreprise());
                            $typeDepot = Atexo_Config::getParameter('DEPOT_PAPIER');
                        }
                        $nomEntrep = Atexo_Util::toUtf8($nomEntrep);
                        //$numeroEntrep = $offre->getNumeroReponse();

                        $idOffre = $offre->getId();

                        $selectionEntrepriseQuery = new CommonTDecisionSelectionEntrepriseQuery();
                        if (Atexo_Module::isEnabled('ArchiveParLot') && $numLot) {
                            $selectionEse = $selectionEntrepriseQuery->getDecisionSelectionEntrepriseByOffre($idOffre, $organisme, $typeDepot, $numLot);
                        } else {
                            $selectionEse = $selectionEntrepriseQuery->getDecisionSelectionEntrepriseByOffre($idOffre, $organisme, $typeDepot);
                        }
                        if (in_array($offre->getId(), $idsOffesWithContrat) || $selectionEse instanceof CommonTDecisionSelectionEntreprise) { // Si offre retenue :
                            $repOffresRetenuesNonRetenues = self::DIR_OFFRES_RETENUES;
                            $prefixNomEntreprise = self::PREFIX_NOM_ENTREPRISE_OFFRE_RETENUE;
                            $typeDoc = Atexo_Config::getParameter('TYPE_DOC_EXTERNE_OFFRES_RETENUES');
                        } else {
                            $repOffresRetenuesNonRetenues = self::DIR_OFFRES_NON_RETENUES;
                            $prefixNomEntreprise = self::PREFIX_NOM_ENTREPRISE_OFFRE_NON_RETENUE;
                            $typeDoc = Atexo_Config::getParameter('TYPE_DOC_EXTERNE_OFFRES_NON_RETENUES');
                        }

                        $enveloppeExiste = true;
                        if ($offre instanceof CommonOffres) {
                            $litseEnveloppes = $offre->getCommonEnveloppes(false, Atexo_Config::getParameter('TYPE_ENV_OFFRE'), $numLot);
                            if (is_array($litseEnveloppes) && count($litseEnveloppes)) {
                                $enveloppeExiste = true;
                            } else {
                                $enveloppeExiste = false;
                            }
                        }
                        // Creation du repertoire des offres retenues et non retenues
                        if (!is_dir($cheminRepertoireParent . DIRECTORY_SEPARATOR . self::DIR_RECEPTION_EXAMEN_OFFRES . DIRECTORY_SEPARATOR . $repOffresRetenuesNonRetenues)) {
                            $this->addFolder($cheminRepertoireParent . DIRECTORY_SEPARATOR . self::DIR_RECEPTION_EXAMEN_OFFRES, $repOffresRetenuesNonRetenues);
                        }

                        if ($enveloppeExiste) {
                            $cheminDossierReponse = $prefixNomEntreprise . $nomEntrep;
                            // Creation des repertoires dossiers des entreprises
                            $dossierFilesOffres = $cheminRepertoireParent . DIRECTORY_SEPARATOR . self::DIR_RECEPTION_EXAMEN_OFFRES . DIRECTORY_SEPARATOR . $repOffresRetenuesNonRetenues;
                            if (!is_dir($dossierFilesOffres . DIRECTORY_SEPARATOR . $cheminDossierReponse)) {
                                $this->addFolder($dossierFilesOffres, $cheminDossierReponse);
                            }
                            $dossierReponses = $dossierFilesOffres . DIRECTORY_SEPARATOR . $cheminDossierReponse;
                            $acteEngagement = $dossierReponses . DIRECTORY_SEPARATOR . self::DIR_ACTE_ENGAGEMENT;
                            if (!is_dir($acteEngagement)) {
                                $this->addFolder($dossierReponses, self::DIR_ACTE_ENGAGEMENT);
                            }

                            $dossierPiecesReponseOffre = $dossierReponses . DIRECTORY_SEPARATOR . self::DIR_REPONSE;
                            if (!is_dir($dossierPiecesReponseOffre)) {
                                $this->addFolder($dossierReponses, self::DIR_REPONSE);
                            }
                            $dossierEnveloppeOffre = $dossierReponses . DIRECTORY_SEPARATOR . self::DIR_ENVELOPPE_OFFRE;
                            if (!is_dir($dossierEnveloppeOffre)) {
                                $this->addFolder($dossierReponses, self::DIR_ENVELOPPE_OFFRE);
                            }

                            // Creation des repertoires des documents externes aux repertoires 'DIR_OFFRES_RETENUES' et 'DIR_OFFRES_NON_RETENUES'
                            if (!is_dir($dossierFilesOffres . DIRECTORY_SEPARATOR . self::DIR_DOCUMENTS_EXTERNES)) {
                                self::CreerRepertoireDocumentsExternes($dossierFilesOffres, $consultationId, $organisme, $numLot, $typeDoc, $logger);
                            }

                            if ($offre instanceof CommonOffres) {
                                $xmlStr = $offre->getXmlString();
                                $pos = strpos($xmlStr, 'typeSignature');
                                if ($pos > 0) {
                                    $typeSignature = 'XML';
                                }

                                $numeroFichier = 1;

                                //regroupement de tous les AE des lots
                                $enveloppes = $offre->getCommonEnveloppes(false, Atexo_Config::getParameter('TYPE_ENV_OFFRE'), $numLot);
                                if (is_array($enveloppes) && !empty($enveloppes)) {
                                    foreach ($enveloppes as $enveloppe) {
                                        if (
                                            $enveloppe instanceof CommonEnveloppe
                                            && in_array($enveloppe->getStatutEnveloppe(),
                                            [
                                                Atexo_Config::getParameter('STATUT_ENV_OUVERTE_EN_LIGNE'),
                                                Atexo_Config::getParameter('STATUT_ENV_OUVERTE_HORS_LIGNE'),
                                                Atexo_Config::getParameter('STATUT_ENV_OUVERTE'),
                                                Atexo_Config::getParameter('STATUT_ENV_OUVERTE_A_DISTANCE'),

                                            ])
                                            ) {
                                            $c = new Criteria();
                                            $c->add(CommonFichierEnveloppePeer::ID_ENVELOPPE, $enveloppe->getIdEnveloppeElectro());
                                            $c->add(CommonFichierEnveloppePeer::ORGANISME, $organisme);
                                            $fichiers = CommonFichierEnveloppePeer::doSelect($c, $connexion);
                                            if (is_array($fichiers) && !empty($fichiers)) {
                                                foreach ($fichiers as $fichier) {
                                                    if ($fichier instanceof CommonFichierEnveloppe) {
                                                        if ('ACE' == $fichier->getTypeFichier() || 'AE' == $fichier->getTypeFichier()) {
                                                            $nomFichier = self::verifierNameFile($acteEngagement, str_pad($numeroFichier, 2, '0', STR_PAD_LEFT) . '-' . Atexo_Util::OterAccents($fichier->getNomFichier()));
                                                            self::addFilesToDirTree($acteEngagement, 'ACTE_ENGAGEMENT', $fichier->getIdBlob(), $nomFichier);
                                                            if ('' != $fichier->getSignatureFichier()) {
                                                                self::addSignatureToDirTree($acteEngagement, 'SIGNATURE_ACTE_ENGAGEMENT', $fichier->getSignatureFichier(), Atexo_Util::OterAccents($fichier->getNomFichierSignature('sig', $nomFichier)), null, true);
                                                            }
                                                        } elseif ('ACE' != $fichier->getTypeFichier() && 'AE' != $fichier->getTypeFichier()) {
                                                            $nomFichier = self::verifierNameFile($dossierPiecesReponseOffre, str_pad($numeroFichier, 2, '0', STR_PAD_LEFT) . '-' . Atexo_Util::OterAccents($fichier->getNomFichier()));
                                                            self::addFilesToDirTree($dossierPiecesReponseOffre, 'FICHIER_ENV_OFFRE', $fichier->getIdBlob(), $nomFichier, null, false);
                                                            if ('' != $fichier->getSignatureFichier()) {
                                                                self::addSignatureToDirTree($dossierPiecesReponseOffre, 'FICHIER_ENV_OFFRE', $fichier->getSignatureFichier(), Atexo_Util::OterAccents($fichier->getNomFichierSignature('sig', $nomFichier)), null, true);
                                                            }
                                                        }
                                                        ++$numeroFichier;
                                                    }
                                                }

                                                if ($consultation->getSignatureOffre()) {
                                                    // Rapport de verification de la signature
                                                    $arrayInfosCons = (new Atexo_Consultation())->getInfosConsPrRapportSignature(null, $consultation);
                                                    $arrayInfosPli = (new Atexo_Consultation_Responses())->GetDetailsPlis($enveloppe->getIdEnveloppeElectro(), $enveloppe->getTypeEnv(), null, null, $consultation);
                                                    $arrayInfosFichierSoumis = (new Atexo_Consultation_Responses())->getInfosCertif($fichiers, $consultation->getSignatureOffre(), $typeSignature, $enveloppe->getCryptage());
                                                    if (0 != $enveloppe->getSousPli()) {
                                                        $nomFichier = Atexo_Config::getParameter('NOM_FICHIER_RAPPORT_VERIF_SIGNATURE') . '_' . (new CommonEnveloppe())->getLibelleTypeEnveloppe($enveloppe->getTypeEnv()) . '_lot_n' . $enveloppe->getSousPli() . '.pdf';
                                                    } else {
                                                        $nomFichier = Atexo_Config::getParameter('NOM_FICHIER_RAPPORT_VERIF_SIGNATURE') . '_' . (new CommonEnveloppe())->getLibelleTypeEnveloppe($enveloppe->getTypeEnv()) . '.pdf';
                                                    }
                                                    $nomFichier = self::verifierNameFile($dossierEnveloppeOffre, $nomFichier);
                                                    $pathFile = Atexo_Config::getParameter('COMMON_TMP') . DIRECTORY_SEPARATOR . Atexo_Config::getParameter('NOM_FICHIER_RAPPORT_VERIF_SIGNATURE') . '_' . uniqid() . '.pdf';
                                                    if (is_array($arrayInfosPli) && is_array($arrayInfosCons) && is_array($arrayInfosFichierSoumis)) {
                                                        $path = (new Atexo_Consultation_Responses())->printRapportSignaturePdf($arrayInfosCons, $arrayInfosPli, $arrayInfosFichierSoumis, true, $pathFile);
                                                    }
                                                    self::addFilesToDirTree($dossierEnveloppeOffre, 'FICHIER_RAPPORT_SIGNATURE', $path, $nomFichier);
                                                }
                                            }
                                        }
                                    }
                                }
                            }

                            if ($offre instanceof CommonOffres) {
                                self::addFilesTimestampToDirTree($dossierEnveloppeOffre, 'FICHIER_ENV_XML_OFFRE', $offre->getHorodatage(), 'Reponse_Annonce-' . $offre->getId(), $offre->getUntrusteddate());
                                $nameFileReponse = 'Reponse_Annonce-' . $offre->getId() . '-' . Atexo_Util::iso2frnDateTimeNoSep($offre->getUntrusteddate()) . '.xml';
                                if ((new Atexo_Crypto())->verifyHorodatage($offre->getHorodatage(), $offre->getXmlString())) {
                                    self::addStringToDirTree($dossierEnveloppeOffre, 'FICHIER_ENV_XML_OFFRE', $offre->getXmlString(), $nameFileReponse, 'Enveloppe XML');
                                } elseif (!empty($offre->getXmlString())) {
                                    $xml = (new Atexo_Entreprise_Reponses())->createOldXmlFromNewOne($offre->getXmlString());
                                    self::addStringToDirTree($dossierEnveloppeOffre, 'FICHIER_ENV_XML_OFFRE', $xml, $nameFileReponse, 'Enveloppe XML');
                                }
                            }
                        }
                    }
                }
            }
        } catch (Exception $e) {
            $erreurMsg = "Erreur lors de la generation de l'arborescence des repertoires '" . self::DIR_OFFRES_NON_RETENUES . "' et '" . self::DIR_OFFRES_RETENUES . "'";
            $erreurMsg .= " : {ref_cons = $consultationId}, {organisme = $organisme}, {lot = $numLot}, {repertoireParent = $cheminRepertoireParent}";
            $erreurMsg .= PHP_EOL . 'Erreur = ' . $e->getMessage() . PHP_EOL . 'Trace = ' . $e->getTraceAsString();
            $logger->error($erreurMsg);
        }
        $logger->info("Fin creation de l'arborescence des repertoires '" . self::DIR_OFFRES_NON_RETENUES . "' et '" . self::DIR_OFFRES_RETENUES . "' de la consultation {reference = '$consultationId'} , {organisme = $organisme}, {numLot = $numLot}");
    }

    /**
     * Permet de creer l'arborescence du repertoire 'DIR_DECISIONS'.
     *
     * @param string    $consultationId         : reference de la consultation
     * @param string    $organisme              : organisme
     * @param string    $numLot                 : numero du lot
     * @param string    $cheminRepertoireParent : chemin du repertoire parent
     * @param Logger    $logger                 : logger
     * @param PropelPDO $connexion              : connexion
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-megalis
     *
     * @copyright Atexo 2016
     */
    public function CreerArborescenceRepertoireDecisionsOffres($consultationId, $organisme, $numLot, $cheminRepertoireParent, $logger = null)
    {
        if (empty($logger)) {
            $logger = Atexo_LoggerManager::getLogger('archivage');
        }
        $logger->info("Debut creation de l'arborescence du repertoire '" . self::DIR_DECISIONS . "' de la consultation {reference = '$consultationId'} , {organisme = $organisme}, {numLot = $numLot}");
        try {
            // Creation repertoire 'DIR_DECISIONS'
            self::addFolder($cheminRepertoireParent, self::DIR_DECISIONS);

            $cheminRepDecisions = $cheminRepertoireParent . DIRECTORY_SEPARATOR . self::DIR_DECISIONS;

            // Creation repertoire 'DIR_DEMANDES_COMPLEMENTS_OFFRES' et son contenu
            self::creerArborescenceMessagesEchanges($consultationId, $organisme, $cheminRepDecisions, self::DIR_DEMANDES_COMPLEMENTS_OFFRES, self::DIR_PIECES_JOINTES, Atexo_Config::getParameter('ID_MESSAGE_COURRIER_DEMANDE_COMPLEMENT'), $logger, Atexo_Config::getParameter('TYPE_DOC_EXTERNE_DEMANDES_COMPLEMENTS_DECISION'), $numLot);

            // Creation repertoire 'DIR_REJET_OFFRES' et son contenu
            self::creerArborescenceMessagesEchanges($consultationId, $organisme, $cheminRepDecisions, self::DIR_REJET_OFFRES, self::DIR_PIECES_JOINTES, Atexo_Config::getParameter('ID_MESSAGE_COURRIER_REJET'), $logger, Atexo_Config::getParameter('TYPE_DOC_EXTERNE_REJET_DECISION'), $numLot);

            // Creation repertoire 'DIR_NOTIFICATION_OFFRE' et son contenu
            self::creerArborescenceMessagesEchanges($consultationId, $organisme, $cheminRepDecisions, self::DIR_NOTIFICATION_OFFRES, self::DIR_PIECES_JOINTES, Atexo_Config::getParameter('ID_MESSAGE_NOTIFICATION_AC'), $logger, Atexo_Config::getParameter('TYPE_DOC_EXTERNE_NOTIFICATION_DECISION'), $numLot);

            // Creation repertoire 'DIR_ATTRIBUTION_OFFRE' et son contenu
            self::creerArborescenceMessagesEchanges($consultationId, $organisme, $cheminRepDecisions, self::DIR_ATTRIBUTION_OFFRES, self::DIR_PIECES_JOINTES, Atexo_Config::getParameter('ID_MESSAGE_COURRIER_ATTRIBUTION_DECISION'), $logger, Atexo_Config::getParameter('TYPE_DOC_EXTERNE_ATTRIBUTION_DECISION'), $numLot);
        } catch (Exception $e) {
            $erreurMsg = PHP_EOL . 'Erreur = ' . $e->getMessage() . PHP_EOL . 'Trace = ' . $e->getTraceAsString();
            $logger->error("Erreur lors de la generation de l'arborescence du repertoire '" . $cheminRepertoireParent . DIRECTORY_SEPARATOR . self::DIR_DECISIONS . "' : {ref_cons = $consultationId}, {organisme = $organisme}, {lot = $numLot}" . PHP_EOL . $erreurMsg);
        }
        $logger->info("Fin creation de l'arborescence du repertoire '" . self::DIR_DECISIONS . "' de la consultation {reference = '$consultationId'} , {organisme = $organisme}, {numLot = $numLot}");
    }

    /**
     * Permet de creer l'arborescence du repertoire des documents de la decision.
     *
     * @param string $consultationId       : reference de la consultation
     * @param string $organisme            : organisme
     * @param string $pathRepertoireParent : chemin du repertoire parent
     * @param Logger $logger               : logger
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-megalis
     *
     * @copyright Atexo 2016
     */
    public function creerArborescenceDossierDocumentsDecision($consultationId, $organisme, $pathRepertoireParent, $logger = null)
    {
        if (empty($logger)) {
            $logger = Atexo_LoggerManager::getLogger('archivage');
        }
        $logger->info("Debut creation de l'arborescence du repertoire '" . $pathRepertoireParent . DIRECTORY_SEPARATOR . self::DIR_DOCUMENTS_DECISION . "' de la consultation {reference = '$consultationId'} , {organisme = $organisme}");
        try {
            self::addFolder($pathRepertoireParent, self::DIR_DOCUMENTS_DECISION);

            // Creation du repertoire des documents externes
            $pathRepertoireDocumentsDecision = $pathRepertoireParent . DIRECTORY_SEPARATOR . self::DIR_DOCUMENTS_DECISION;
            self::CreerRepertoireDocumentsExternes($pathRepertoireDocumentsDecision, $consultationId, $organisme, false, Atexo_Config::getParameter('TYPE_DOC_EXTERNE_DOCUMENTS_DECISION'), $logger);

            // Registre de notification
            $messagesNotif = Atexo_Message::retreiveNotificationByIdRef($consultationId, $organisme);
            $pathResgistre = (new Atexo_GenerationExcel())->generateExcelRegistreNotification($consultationId, $messagesNotif, true, $organisme, false, true);
            $nomRegistre = Atexo_Util::OterAccents(basename($pathResgistre));
            self::addFilesToDirTree($pathRepertoireDocumentsDecision, 'REGISTRE_NOTIFICATION', $pathResgistre, $nomRegistre);
        } catch (Exception $e) {
            $erreurMsg = PHP_EOL . 'Erreur = ' . $e->getMessage() . PHP_EOL . 'Trace = ' . $e->getTraceAsString();
            $logger->error("Erreur lors de la generation de l'arborescence du repertoire '" . $pathRepertoireParent . DIRECTORY_SEPARATOR . self::DIR_DOCUMENTS_DECISION . "' : {ref_cons = $consultationId}, {organisme = $organisme}" . PHP_EOL . $erreurMsg);
        }
        $logger->info("Fin creation de l'arborescence du repertoire '" . $pathRepertoireParent . DIRECTORY_SEPARATOR . self::DIR_DOCUMENTS_DECISION . "' de la consultation {reference = '$consultationId'} , {organisme = $organisme}");
    }

    /**
     * Permet de creer le repertoire des documents externes avec les fichiers.
     *
     * @param string   $repertoireParent : repertoire parent
     * @param string   $consultationId   : reference de la consultation
     * @param string   $organisme        : organisme
     * @param bool|int $lot              : numero du lot, false sinon
     * @param string   $typeDoc          : type de document
     * @param Logger   $logger           : Logger
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-megalis
     *
     * @copyright Atexo 2016
     */
    public function CreerRepertoireDocumentsExternes($repertoireParent, $consultationId, $organisme = null, bool|int|null $lot = false, $typeDoc = null, $logger = null)
    {
        if (empty($logger)) {
            $logger = Atexo_LoggerManager::getLogger('archivage');
        }
        $logger->info("Debut creation du document externe dans le repertoire '" . $repertoireParent . DIRECTORY_SEPARATOR . self::DIR_DOCUMENTS_EXTERNES);
        try {
            if (empty($organisme)) {
                $organisme = Atexo_CurrentUser::getOrganismAcronym();
            }
            self::addFolder($repertoireParent, self::DIR_DOCUMENTS_EXTERNES);
            $arrayDocumentsExternes = (new Atexo_Consultation_Documentexterne())->getDocumentsExternes($consultationId, $organisme, $lot, $typeDoc);
            if (is_array($arrayDocumentsExternes) && !empty($arrayDocumentsExternes)) {
                foreach ($arrayDocumentsExternes as $documentExterne) {
                    if ($documentExterne instanceof CommonDocumentExterne) {
                        $path = $repertoireParent . DIRECTORY_SEPARATOR . self::DIR_DOCUMENTS_EXTERNES;
                        if (false === self::verifierExistenceFichierArborescence($path, Atexo_Util::OterAccents($documentExterne->getNom()))) {
                            $dateCreation = Atexo_Util::getDateTimeByFormat($documentExterne->getDate('Y-m-d'));
                            $this->addFile(
                                $path,
                                self::verifierNameFile($path, Atexo_Util::OterAccents($documentExterne->getNom())),
                                '',
                                '',
                                $documentExterne->getIdblob(),
                                [],
                                $dateCreation
                            );
                        }
                    }
                }
            }
        } catch (Exception $e) {
            $erreurMsg = PHP_EOL . 'Erreur = ' . $e->getMessage() . PHP_EOL . 'Trace = ' . $e->getTraceAsString();
            $logger->error("Erreur lors de la creation du repertoire des documents externes dans le repertoire '" . $repertoireParent . DIRECTORY_SEPARATOR . self::DIR_DOCUMENTS_EXTERNES . "' : {ref_cons = $consultationId}, {organisme = $organisme}, {lot = $lot}, {typeDoc = $typeDoc}" . PHP_EOL . $erreurMsg);
        }
        $logger->info("Fin creation du document externe dans le repertoire '" . $repertoireParent . DIRECTORY_SEPARATOR . self::DIR_DOCUMENTS_EXTERNES);
    }

    /**
     * Permet de verifier l'existence d'un fichier dans l'arborescence.
     *
     * @param string $emplacement : emplacement du fichier
     * @param string $nomFichier  : nom du fichier
     *
     * @return bool : true si fichier deja ajoute, false sinon
     */
    public function verifierExistenceFichierArborescence($emplacement, $nomFichier)
    {
        if ($this->tableNomFichier[$emplacement][$nomFichier]) {
            return true;
        }

        return false;
    }

    /**
     * Permet de calculer l'algorithme de hashage a appliquer aux fichiers.
     *
     * @return string
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-megalis
     *
     * @copyright Atexo 2016
     */
    public function getAlgorithmeHashage()
    {
        return self::ALGORITHME_HASHAGE;
    }

    /**
     * Permet de calculer le l'emprunte des fichiers.
     *
     * @param string|int $file : chemin du fichier ou idBlob
     *
     * @return string
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-megalis
     *
     * @copyright Atexo 2016
     */
    public function getHashFile($file, $org = null, $data = false, $filename = null)
    {
        $content = null;
        if (preg_match('#.tsr#', $filename)) {
            $file = base64_decode($file);
        }
        try {
            if (empty($org)) {
                $org = Atexo_CurrentUser::getOrganismAcronym();
            }
            if (is_numeric($file)) {
                $atexoBlob = new Atexo_Blob();
                $file = $atexoBlob->getPathFile($file, $org);
            } elseif ($data) {
                $content = $file;
            }
            $tmpFile = null;
            if (!is_file($file)) {
                $tmpFile = Atexo_Config::getParameter('COMMON_TMP') . DIRECTORY_SEPARATOR . (is_null($filename) ? 'file_' . session_id() . uniqid() : (is_numeric($file) ? $file . '_' : '') . $filename);
                if (!is_file($tmpFile)) {
                    Atexo_Util::write_file($tmpFile, $content);
                }
                $pathFile = $tmpFile;
            } else {
                $pathFile = $file;
            }
            $hashAlgo = self::getAlgorithmeHashage();
            $hashFile = hash_file(self::getAlgorithmeHashage(), $pathFile);
            if (Atexo_Config::getParameter('ARCHIVE_AJOUTER_LOG_EMPREINTE')) {
                $hashSystem = shell_exec('sha256sum "' . $pathFile . '"');
                $hashSystemValues = preg_split('# #', $hashSystem);
                $hashSystemValue = preg_replace('#\n#', '', $hashSystemValues[0]);
                $statusHash = $hashFile == $hashSystemValue ? 'OK' : 'KO';
                $decimals = 2;
                $sz = 'BKMGTP';
                $factor = floor((strlen(filesize($pathFile)) - 1) / 3);
                $fileSize = sprintf("%.{$decimals}f", filesize($pathFile) / 1024 ** $factor) . @$sz[$factor];
                $hashSession = Atexo_CurrentUser::readFromSession('hashArborescence');
                if (!empty($hashSession[$hashFile])) {
                    ++$hashSession[$hashFile];
                    $existe = 'OUI';
                } else {
                    $hashSession ??= [];
                    $hashSession[$hashFile] = 1;
                    $existe = 'NON';
                }
                Atexo_CurrentUser::writeToSession('hashArborescence', $hashSession);
                $fileReport = (is_numeric($file) ? "PARAM FILE BLOB VALUE: $file" . PHP_EOL . "TMPFILE_PATH : $tmpFile" : '');
                echo <<<COMPARE
$pathFile status
$fileReport
HASHALGO: $hashAlgo
FILESIZE: $fileSize
HASH PHP: $hashFile
CMD SYS : sha256sum "$pathFile"
RES SYS : $hashSystem
HASH SYS: $hashSystemValue
STATUS  : $statusHash
EXISTE  : $existe
NB OCCUR: {$hashSession[$hashFile]}


COMPARE;
            }
            if (is_file($tmpFile)) {
                unlink($tmpFile);
            }

            return $hashFile;
        } catch (Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('archivage');
            $logger->error("Erreur lors du calcul du hash du fichier : {path_file ou id_blob = $file} " . PHP_EOL . 'Erreur = ' . $e->getMessage() . PHP_EOL . 'Trace = ' . $e->getTraceAsString());
        }
    }

    public function getTableNomFichier()
    {
        return $this->tableNomFichier;
    }
    public function getOffresWithContrat($consultationId): array
    {
        $idsOffres = [];
        if (Atexo_Config::getParameter('ACTIVE_EXEC_V2')) {
            /** @var $execWS WebServicesExec */
            $execWS = Atexo_Util::getSfService(WebServicesExec::class);
            $query = [
                "page"                      => 0,
                "size"                      => 5000,
                "idExternesConsultation"    => $consultationId,
            ];

            $response = $execWS->getContent('getContrats', $query);
            $contratsFromExec = $response['content'] ?? [];
            foreach ($contratsFromExec as $value) {
                if (!empty($value['idOffre']) || '0' != $value['idOffre']) {
                    $idsOffres [] = $value['idOffre'];
                }
            }
        }
        return $idsOffres;
    }
}
