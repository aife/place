<?php

namespace Application\Service\Atexo\Consultation\Tree;

use AtexoCrypto\Dto\Consultation;

/**
 * Décrit un élement de l'archive.
 *
 * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Consultation_Tree_Item
{
    protected string $nom = '';
    protected $description = '';
    protected $accessRestriction = '';
    protected $autresProprietes = [];
    protected string $algo = '';
    protected string $hash = '';
    protected $nomDirParent = '';
    protected array $arrayFils = [];
    public string $path = '';
    protected $numLot = 0;
    protected $dateCreation = '';

    /**
     * Permet dde retourner la liste des fichiers.
     *
     * @return array la valeur courante [arrayFils]
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function getFils()
    {
        return $this->arrayFils;
    }

    /**
     * Permet d'ajouter un fichier à la liste des fichiers.
     *
     * @return void
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function addFils(Atexo_Consultation_Tree_Item &$item)
    {
        $item->setPath($this->getPath().'/'.$this->getNom());
        $this->arrayFils[] = $item;
    }

    /**
     * Permet de retourner le nom du fichier.
     *
     * @return string la valeur courante [nom]
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Permet de retourner la description du fichier.
     *
     * @return string la valeur courante [description]
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Permet de retourner l'access du fichier.
     *
     * @return string la valeur courante [accessRestriction]
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function getAccess()
    {
        return $this->accessRestriction;
    }

    /**
     * Permet de retourner le chemin du fichier.
     *
     * @return string la valeur courante [path]
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * Permet de setter le chemin du fichier.
     *
     * @param string $path le chemin du fichier
     *
     * @return void
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * Permet de setter les attribut du Atexo_Consultation_Tree_Item.
     *
     * @param string $_nom le nom, String $_description la description, String $_accessRestriction le type acces, Array $_autresProprietes
     *
     * @return void
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function __construct(
        $_nom,
        $_description = '',
        $_accessRestriction = '',
        $_autresProprietes = null,
        $_nomDirParent = '',
        $_lot = false,
        $dateCreation = ''
    ) {
        $this->nom = $_nom;
        $this->nomDirParent = $_nomDirParent;
        $this->description = $_description;
        $this->accessRestriction = $_accessRestriction;
        $this->autresProprietes = $_autresProprietes;
        $this->numLot = $_lot;
        $this->dateCreation = $dateCreation;
    }

    /**
     * Permet de setter le chemin du fichier.
     *
     * @return array autresProprietes autre propiete
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function getAutresProprietes()
    {
        return $this->autresProprietes;
    }

    /**
     * Permet de setter autresProprietes.
     *
     * @param array $autresProprietes les autre proprietes
     *
     * @return array $autresProprietes les autre proprietes
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function setAutresProprietes($autresProprietes)
    {
        return $this->autresProprietes = $autresProprietes;
    }

    /**
     * Permet chercher un fichier dans la liste des fichiers.
     *
     * @param array $arrayPath
     *
     * @return bool le resultat de la rcherche
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function searchItem($arrayPath)
    {
        foreach ($this->arrayFils as $itemFils) {
            if ($itemFils->getNom() == $arrayPath[0]) {
                if (0 == strcmp($itemFils->getNom(), $arrayPath[count($arrayPath) - 1])) {
                    return $itemFils;
                } else {
                    array_shift($arrayPath);

                    return $itemFils->searchItem($arrayPath);
                }
            }
        }

        return false;
    }

    /**
     * Permet de retourner le nom du repertoire parent d'un item.
     *
     * @return string la valeur courante [nomDirParent]
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-megalis
     *
     * @copyright Atexo 2016
     */
    public function getNomDirParent()
    {
        return $this->nomDirParent;
    }

    /**
     * @return mixed
     */
    public function getNumLot()
    {
        return $this->numLot;
    }

    /**
     * @param mixed $numLot
     */
    public function setNumLot($numLot)
    {
        $this->numLot = $numLot;
    }

    /**
     * @return string
     */
    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    /**
     * @param string $dateCreation
     */
    public function setDateCreation($dateCreation)
    {
        $this->dateCreation = $dateCreation;
    }
}
