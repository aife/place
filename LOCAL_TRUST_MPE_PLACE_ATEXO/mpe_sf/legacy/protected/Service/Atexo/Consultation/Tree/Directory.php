<?php

namespace Application\Service\Atexo\Consultation\Tree;

use AtexoCrypto\Dto\Consultation;

/**
 * Dossier ou fichier de l'archive.
 *
 * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Consultation_Tree_Directory extends Atexo_Consultation_Tree_Item
{
    private ?string $appraisal = '';

    /**
     * permet d'ajouter un dossier a une arborescence.
     *
     * @param string $_nom
     * @param string $_description
     * @param string $_accessRestriction
     * @param string $_appraisal
     * @param array  $_autresProprietes
     *
     * @return string url de telechargement du DIC selon le parametrage
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function __construct($_nom, $_description = '', $_accessRestriction = '', $_appraisal = '', $_autresProprietes = [], $nomDirParent = '', $lot = false)
    {
        parent::__construct($_nom, $_description, $_accessRestriction, $_autresProprietes, $nomDirParent, $lot);
        //parent::Atexo_Consultation_Tree_Item($_nom, $_description, $_accessRestriction, $_autresProprietes, $nomDirParent,$lot);
        $this->appraisal = $_appraisal;
    }

    /**
     * recupere la valeur du [appraisal].
     *
     * @return int la valeur courante [appraisal]
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function getAppraisal()
    {
        return $this->appraisal;
    }

    /**
     * permet de verifier l'existance d'un fichier dans ce repertoire.
     *
     * @param string $nom
     *
     * @return bool true si le fichier est trouvé , sinon false
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function hasFils($nom)
    {
        foreach ($this->arrayFils as $fils) {
            if ($nom == $fils->getNom()) {
                return true;
            }
        }

        return false;
    }
}
