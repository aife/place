<?php

namespace Application\Service\Atexo\Consultation\Tree;

use AtexoCrypto\Dto\Consultation;

/**
 * Permet de traiter le dossier ou fichier de l'archive.
 *
 * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Consultation_Tree_Group extends Atexo_Consultation_Tree_Item
{
    /**
     * Permet d'ajouter un fichier ou dossier à l'arborescence.
     *
     * @param string $_nom
     * @param string $_description
     * @param string $_accessRestriction
     * @param array  $_autresProprietes
     *                                   return void
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function __construct($_nom, $_description = '', $_accessRestriction = '', $_autresProprietes = [])
    {
        parent::__construct($_nom, $_description, $_accessRestriction, $_autresProprietes);
    }

    /**
     * Permet d'ajouter un fichier.
     *
     * @param Atexo_Consultation_Tree_Item $item le fichier à ajouter
     *
     * @return void
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function addFils(Atexo_Consultation_Tree_Item &$item)
    {
        $item->setPath($this->getPath());
        $this->arrayFils[] = $item;
    }

    /**
     * Permet de retourner l'objet Atexo_Consultation_Tree_Group.
     *
     * @param Atexo_Consultation_Tree_Item $item le fichier à ajouter
     *
     * @return void
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public static function &newRef($_nom, $_description, $_accessRestriction)
    {
        return new Atexo_Consultation_Tree_Group($_nom, $_description, $_accessRestriction);
    }
}
