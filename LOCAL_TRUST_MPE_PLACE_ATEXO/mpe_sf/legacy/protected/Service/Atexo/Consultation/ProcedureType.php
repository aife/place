<?php

namespace Application\Service\Atexo\Consultation;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonArticles;
use Application\Propel\Mpe\CommonArticlesPeer;
use Application\Propel\Mpe\CommonTTypeContratEtProcedure;
use Application\Propel\Mpe\CommonTTypeContratEtProcedureQuery;
use Application\Propel\Mpe\CommonTypeProcedure;
use Application\Propel\Mpe\CommonTypeProcedureConcessionPivotQuery;
use Application\Propel\Mpe\CommonTypeProcedureMpePivotQuery;
use Application\Propel\Mpe\CommonTypeProcedureOrganisme;
use Application\Propel\Mpe\CommonTypeProcedureOrganismePeer;
use Application\Propel\Mpe\CommonTypeProcedureOrganismeQuery;
use Application\Propel\Mpe\CommonTypeProcedurePeer;
use Application\Propel\Mpe\CommonTypeProcedurePivotPeer;
use Application\Propel\Mpe\CommonTypeProcedurePivotQuery;
use Application\Propel\Mpe\CommonTypeProcedureQuery;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Util;
use PDO;
use Prado\Prado;

/**
 * Classe de ProcedureType.
 *
 * @author Khalid BENAMAR <khalid.benamar@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Consultation_ProcedureType
{
    /**
     * retourne la liste des type de procedures à partir de la base de donnees commune et les charge dans le dropdownlist.
     *
     * @param : $calledFromPortail à true pour récupérer les données à partir de la base commune à false pour la base organisme,
     *                             $returnPropelObject : par defaut à true si l'objet retourné doir être un tableau d'objet propel (indéxé par référence),
     *                             à false si l'objet retourné doit être un tableau de type array [id type procedure] = libelle
     *                             $indexePropelArray : à true si le tableau d'objets propel retourné doit être indexé (par id de type de procédure)
     *
     * @return la liste des types de procedures
     */
    public static function retrieveProcedureType($calledFromPortail = false, $returnPropelObjectsArray = true, $organisme = null, $langue = null)
    {
        $typeProcedures = self::getCachedTypeProcedure($calledFromPortail, $organisme);

        if ($returnPropelObjectsArray) {
            return $typeProcedures;
        } else {
            $arrayTypeProc = [];
            foreach ($typeProcedures as $typeProc) {
                if (!$langue) {
                    $arrayTypeProc[$typeProc->getIdTypeProcedure()] = $typeProc->getLibelleTypeProcedure();
                } else {
                    $getLibelleTypeProcedure = 'getLibelleTypeProcedure'.Atexo_Languages::getLanguageAbbreviation($langue);
                    if (0 == strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE'))
                        || !$typeProc->$getLibelleTypeProcedure()) {
                        $arrayTypeProc[$typeProc->getIdTypeProcedure()] = $typeProc->getLibelleTypeProcedure();
                    } else {
                        $arrayTypeProc[$typeProc->getIdTypeProcedure()] = $typeProc->$getLibelleTypeProcedure();
                    }
                }
            }

            return $arrayTypeProc;
        }
    }

    /**
     * retourne la liste des type de procedures PIVOT à partir de la base de donnees commune et les charge dans le dropdownlist.
     *
     * @param : $calledFromPortail à true pour récupérer les données à partir de la base commune à false pour la base organisme,
     *                             $returnPropelObject : par defaut à true si l'objet retourné doir être un tableau d'objet propel (indéxé par référence),
     *                             à false si l'objet retourné doit être un tableau de type array [id type procedure] = libelle
     *                             $indexePropelArray : à true si le tableau d'objets propel retourné doit être indexé (par id de type de procédure)
     *
     * @return la liste des types de procedures
     */
    public static function retrieveProcedureTypesPivot($calledFromPortail = false, $returnPropelObjectsArray = true, $langue = null)
    {
        $allTypesProcedurePivot = [];
        $categorieConsultationQuery = new CommonTypeProcedurePivotQuery();
        $rowTypesProcedurePivot = $categorieConsultationQuery->find();

        foreach ($rowTypesProcedurePivot as $oneTypeProcedurePivot) {
            $allTypesProcedurePivot[$oneTypeProcedurePivot->getId()] = $oneTypeProcedurePivot;
        }

        return $allTypesProcedurePivot;
    }

    /**
     * Retourne l'objet type procedure de la consultation.
     *
     * @param $idTypeProcedure identifiant de type procedure
     */
    public function retrieveTypeProcedureObject($idTypeProcedure, $calledFromPortail = false)
    {
        $listTypeProcedures = self::getCachedTypeProcedure($calledFromPortail);
        if ($listTypeProcedures[$idTypeProcedure] instanceof CommonTypeProcedureOrganisme) {
            return $listTypeProcedures[$idTypeProcedure];
        }
    }

    /**
     * Retourne l'objet type procedure Pivot de la consultation.
     *
     * @param $idTypeProcedure identifiant de type procedure
     */
    public function retrieveTypeProcedurePivotObject($idTypeProcedurePivot, $calledFromPortail = false)
    {
        $typeProcedurePivotQuery = new CommonTypeProcedurePivotQuery();

        return $typeProcedurePivotQuery->findOneById($idTypeProcedurePivot);
    }

    /**
     * Retourne l'objet type procedure Pivot de la consultation en fonction du type de procedure MPE.
     *
     * @param $idTypeProcedureMpe
     * @param bool $calledFromPortail
     *
     * @return CommonTypeProcedurePivot
     */
    public function retrieveTypeProcedurePivotObjectByIdTypeProcedureMpe($idTypeProcedureMpe, $calledFromPortail = false)
    {
        $typeProcedureMpePivotQuery = new CommonTypeProcedureMpePivotQuery();
        $typeProcedurePivotQuery = new CommonTypeProcedurePivotQuery();

        $typeProcedureMpePivot = $typeProcedureMpePivotQuery->findOneBy('IdTypeProcedureMpe', $idTypeProcedureMpe);

        return $typeProcedurePivotQuery->findOneById($typeProcedureMpePivot->getIdTypeProcedurePivot());
    }

    /**
     * Retourne le libelle de type procedure de la consultation.
     *
     * @param $idTypeProcedure identifiant de type procedure
     */
    public function retrieveTypeProcedure($idTypeProcedure, $calledFromPortail = false, $organisme = null, $langue = null)
    {
        $listTypeProcedures = self::getCachedTypeProcedure($calledFromPortail, $organisme);
        $getLibelleTypeProcedure = 'getLibelleTypeProcedure'.Atexo_Languages::getLanguageAbbreviation($langue);

        if ($listTypeProcedures[$idTypeProcedure] instanceof CommonTypeProcedureOrganisme || $listTypeProcedures[$idTypeProcedure] instanceof CommonTypeProcedure) {
            if (0 == strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE'))
                || !$listTypeProcedures[$idTypeProcedure]->$getLibelleTypeProcedure()) {
                return $listTypeProcedures[$idTypeProcedure]->getLibelleTypeProcedure();
            } else {
                return $listTypeProcedures[$idTypeProcedure]->$getLibelleTypeProcedure();
            }
        }
    }

    /**
     * @param $idTypeProcedure
     * @param false $calledFromPortail
     * @param null  $organisme
     * @param null  $langue
     *
     * @return mixed
     *
     * @throws Atexo_Config_Exception
     * @throws PropelException
     */
    public function retrieveTypeProcedureCli($idTypeProcedure, $calledFromPortail = false, $organisme = null, $langue = null)
    {
        $listTypeProcedures = [];
        $getLibelleTypeProcedure = 'getLibelleTypeProcedure'.Atexo_Languages::getLanguageAbbreviation($langue);
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();

        $c->add(CommonTypeProcedureOrganismePeer::ORGANISME, $organisme);
        $c->addDescendingOrderByColumn(CommonTypeProcedureOrganismePeer::ORDRE_AFFICHAGE);
        $arrayTypeProcedure = CommonTypeProcedureOrganismePeer::doSelect($c, $connexionCom);

        foreach ($arrayTypeProcedure as $oneProcedure) {
            $listTypeProcedures[$oneProcedure->getIdTypeProcedure()] = $oneProcedure;
        }

        if (0 == strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE'))
            || !$listTypeProcedures[$idTypeProcedure]->$getLibelleTypeProcedure()) {
            $libelle = $listTypeProcedures[$idTypeProcedure]->getLibelleTypeProcedure();
        } else {
            $libelle = $listTypeProcedures[$idTypeProcedure]->$getLibelleTypeProcedure();
        }

        return $libelle;
    }

    /**
     * Retourne le libelle de type procedure de la consultation.
     *
     * @param $idTypeProcedurePivot identifiant de type procedure Pivot
     */
    public function retrieveTypeProcedurePivot($idTypeProcedurePivot, $calledFromPortail = false, $langue = null)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();

        $c->add(CommonTypeProcedurePivotPeer::ID, $idTypeProcedurePivot);
        $typeProcedurePivot = CommonTypeProcedurePivotPeer::doSelect($c, $connexionCom);

        $listTypeProcedures = self::retrieveProcedureTypesPivot(true, true);

        return Prado::localize($listTypeProcedures[$idTypeProcedurePivot]->getLibelle());
    }

    /**
     * @deprecated use retrieveAcronymeTypeProcedure
     */
    public function retrieveAbreviationTypeProcedure($idTypeProcedure)
    {
        return self::retrieveAcronymeTypeProcedure($idTypeProcedure, false);
    }

    /**
     * Retourne l'acronyme du type procedure passé en paramètre.
     *
     * @param $idTypeProcedure identifiant de type procedure
     */
    public function retrieveAcronymeTypeProcedure($idTypeProcedure, $calledFromPortail = false)
    {
        $typesProcedures = self::getCachedTypeProcedure($calledFromPortail);
        if ((false == $calledFromPortail && $typesProcedures[$idTypeProcedure] instanceof CommonTypeProcedureOrganisme) ||
        (true == $calledFromPortail && $typesProcedures[$idTypeProcedure] instanceof CommonTypeProcedureOrganisme)) {
            return $typesProcedures[$idTypeProcedure]->getAbbreviation();
        } else {
            return '';
        }
    }

    /**
     * Retourne l'id Externe du type procedure passé en paramètre.
     *
     * @param $idTypeProcedure identifiant de type procedure
     */
    public function retreiveidExterneTypeProcedure($idTypeProcedure, $calledFromPortail = false)
    {
        $typesProcedures = self::getCachedTypeProcedure($calledFromPortail);
        if (isset($typesProcedures[$idTypeProcedure])) {
            return $typesProcedures[$idTypeProcedure]->getidExterne();
        } else {
            return '';
        }
    }

    private static function getCachedTypeProcedure($calledFromPortail, $organisme = null)
    {
        $allProcedures = [];
        if ($calledFromPortail || ('1' == Atexo_Config::getParameter('ARCH_AVEC_PORTAIL') && '1' == Atexo_Config::getParameter('TABLES_REFERENCES_PORTAIL'))) {
            $typeProcedureQuery = new CommonTypeProcedureQuery();
            $arrayTypeProcedure = $typeProcedureQuery->findTypeProcedure();
            foreach ($arrayTypeProcedure as $oneProcedure) {
                $allProcedures[$oneProcedure->getIdTypeProcedure()] = $oneProcedure;
            }

            return $allProcedures;
        } else {
            if (!Atexo_CurrentUser::readFromSession('cachedTypeProceduresOrganisme')) {
                $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
                $c = new Criteria();
                if (!$organisme) {
                    $organisme = Atexo_CurrentUser::getCurrentOrganism();
                }
                $c->add(CommonTypeProcedureOrganismePeer::ORGANISME, $organisme);
                $c->addDescendingOrderByColumn(CommonTypeProcedureOrganismePeer::ORDRE_AFFICHAGE);
                $arrayTypeProcedure = CommonTypeProcedureOrganismePeer::doSelect($c, $connexionCom);

                foreach ($arrayTypeProcedure as $oneProcedure) {
                    $allProcedures[$oneProcedure->getIdTypeProcedure()] = $oneProcedure;
                }
                Atexo_CurrentUser::writeToSession('cachedTypeProceduresOrganisme', $allProcedures);
            }

            return Atexo_CurrentUser::readFromSession('cachedTypeProceduresOrganisme');
        }
    }

    public function reloadCacheTypeProcedureOrganisme()
    {
        $allProcedures = [];

        Atexo_CurrentUser::deleteFromSession('cachedTypeProceduresOrganisme');
        $c = new Criteria();
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonTypeProcedureOrganismePeer::ORGANISME, Atexo_CurrentUser::getCurrentOrganism());
        $c->addDescendingOrderByColumn(CommonTypeProcedureOrganismePeer::ORDRE_AFFICHAGE);
        $arrayTypeProcedure = CommonTypeProcedureOrganismePeer::doSelect($c, $connexionCom);
        foreach ($arrayTypeProcedure as $oneProcedure) {
            $allProcedures[$oneProcedure->getIdTypeProcedure()] = $oneProcedure;
        }
        Atexo_CurrentUser::writeToSession('cachedTypeProceduresOrganisme', $allProcedures);
    }

    public static function getIdTypeProcedurePortail($idTypeProcedure, $organisme)
    {
        $c = new Criteria();
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c->add(CommonTypeProcedureOrganismePeer::ID_TYPE_PROCEDURE, $idTypeProcedure);
        $c->add(CommonTypeProcedureOrganismePeer::ORGANISME, $organisme);
        $typeProcedure = CommonTypeProcedureOrganismePeer::doSelectOne($c, $connexionCom);
        if ($typeProcedure) {
            return $typeProcedure->getIdTypeProcedurePortail();
        }

        return '';
    }

    public function retrieveAllMapa($organisme, $sortByLibelle = false, $sensTri = 'ASC')
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonTypeProcedureOrganismePeer::MAPA, Atexo_Config::getParameter('TYPE_PROCEDURE_MAPA'));
        $c->add(CommonTypeProcedureOrganismePeer::ORGANISME, $organisme);
        if ($sortByLibelle) {
            if ('ASC' == $sensTri) {
                $c->addAscendingOrderByColumn(CommonTypeProcedureOrganismePeer::LIBELLE_TYPE_PROCEDURE);
            } elseif ('DESC' == $sensTri) {
                $c->addDescendingOrderByColumn(CommonTypeProcedureOrganismePeer::LIBELLE_TYPE_PROCEDURE);
            }
        }
        $typesProceduresMapa = CommonTypeProcedureOrganismePeer::doSelect($c, $connexionCom);
        if ($typesProceduresMapa) {
            return $typesProceduresMapa;
        } else {
            return [];
        }
    }

    public function deleteProcedure($idTypeProcedure, $organisme)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonTypeProcedureOrganismePeer::ID_TYPE_PROCEDURE, $idTypeProcedure);
        $c->add(CommonTypeProcedureOrganismePeer::ORGANISME, $organisme);
        CommonTypeProcedureOrganismePeer::doDelete($c, $connexionCom);
    }

    public static function retrieveProcedureById($idTypeProcedure, $organisme)
    {
        $c = new Criteria();
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c->add(CommonTypeProcedureOrganismePeer::ID_TYPE_PROCEDURE, $idTypeProcedure);
        $c->add(CommonTypeProcedureOrganismePeer::ORGANISME, $organisme);
        $typeProcedure = CommonTypeProcedureOrganismePeer::doSelectOne($c, $connexionCom);

        return $typeProcedure;
    }

    /**
     * @deprecated 27 oct. 2008 use getLibelleTypeProcedurePortailById
     */
    public function getTypeProcedurePortailById($idTypeProcedure)
    {
        return self::getLibelleTypeProcedurePortailById($idTypeProcedure);
    }

    public function getLibelleTypeProcedurePortailById($idTypeProcedure)
    {
        $c = new Criteria();
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonTypeProcedurePeer::ID_TYPE_PROCEDURE, $idTypeProcedure);
        $typeProcedure = CommonTypeProcedurePeer::doSelectOne($c, $connexion);
        if ($typeProcedure) {
            return $typeProcedure->getLibeleTypeProcedureTraduit();
        }

        return '';
    }

    public function retreiveNewProcedureType($idTypeProcedure)
    {
        $allProcedures = [];
        $c = new Criteria();
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c->add(CommonTypeProcedureOrganismePeer::ORGANISME, Atexo_CurrentUser::getCurrentOrganism());
        $c->addDescendingOrderByColumn(CommonTypeProcedureOrganismePeer::ORDRE_AFFICHAGE);
        $arrayTypeProcedure = CommonTypeProcedureOrganismePeer::doSelect($c, $connexionCom);
        foreach ($arrayTypeProcedure as $oneProcedure) {
            $allProcedures[$oneProcedure->getIdTypeProcedure()] = $oneProcedure;
        }
        Atexo_CurrentUser::writeToSession('cachedTypeProceduresOrganisme', $allProcedures);
        if ($allProcedures[$idTypeProcedure] instanceof CommonTypeProcedureOrganisme) {
            return $allProcedures[$idTypeProcedure];
        }
    }

    public function retrieveCodeRecensementProcedureById($idTypeProcedure, $organisme)
    {
        $c = new Criteria();
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c->add(CommonTypeProcedureOrganismePeer::ORGANISME, $organisme);
        $c->add(CommonTypeProcedureOrganismePeer::ID_TYPE_PROCEDURE, $idTypeProcedure);
        $typeProcedure = CommonTypeProcedureOrganismePeer::doSelectOne($c, $connexionCom);
        if ($typeProcedure) {
            return $typeProcedure->getCodeRecensement();
        } else {
            return '';
        }
    }

    public function retrieveTypeProcedureByIdTypeProcedurePortail($idTypeProcedurePortail, $organisme)
    {
        $c = new Criteria();
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c->add(CommonTypeProcedureOrganismePeer::ID_TYPE_PROCEDURE_PORTAIL, $idTypeProcedurePortail);
        $c->add(CommonTypeProcedureOrganismePeer::ORGANISME, $organisme);
        $typeProcedure = CommonTypeProcedureOrganismePeer::doSelectOne($c, $connexionCom);
        if ($typeProcedure instanceof CommonTypeProcedureOrganisme) {
            return $typeProcedure;
        } else {
            return false;
        }
    }

    public function getCommonTypeProcedureById($idTypeProcedure)
    {
        $c = new Criteria();
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonTypeProcedurePeer::ID_TYPE_PROCEDURE, $idTypeProcedure);
        $typeProcedure = CommonTypeProcedurePeer::doSelectOne($c, $connexion);

        return $typeProcedure;
    }

    /**
     *  Test si un type de procédure donné est un(e) Mapa, retourne vrai si Mapa.
     *
     *  @param : $idTypeProcedure : id du type de procédure à vérifier
     *  @param : $calledFromPortail à true s'il faut rechercher le type de procédure dans la base commune,
     *           à false si dans la base organisme
     *  @param : $organisme : contient le nom de l'organisme où rechercher le type de procédure
     *           (utile uniquement si $calledFromPortail égale à false)
     */
    public function isMapa($idTypeProcedure, $calledFromPortail = false, $organisme = null)
    {
        $listTypeProcedures = self::getCachedTypeProcedure($calledFromPortail, $organisme);
        $typeProcedure = $listTypeProcedures[$idTypeProcedure];
        if ($typeProcedure instanceof CommonTypeProcedureOrganisme) {
            return (1 == $typeProcedure->getMapa()) ? true : false;
        }

        return false;
    }

    /**
     *  Test si un type de procédure donné est un(e) SAD, retourne vrai si SAD.
     *
     *  @param : $idTypeProcedure : id du type de procédure à vérifier
     *  @param : $calledFromPortail à true s'il faut rechercher le type de procédure dans la base commune,
     *           à false si dans la base organisme
     *  @param : $organisme : contient le nom de l'organisme où rechercher le type de procédure
     *           (utile uniquement si $calledFromPortail égale à false)
     */
    public function isSAD($idTypeProcedure, $calledFromPortail = false, $organisme = null)
    {
        $listTypeProcedures = self::getCachedTypeProcedure($calledFromPortail, $organisme);
        $typeProcedure = $listTypeProcedures[$idTypeProcedure];
        if ($typeProcedure instanceof CommonTypeProcedureOrganisme) {
            return (1 == $typeProcedure->getSad()) ? true : false;
        }

        return false;
    }

    /**
     *  Test si un type de procédure donné est un(e) AC, retourne vrai si AC.
     *
     *  @param : $idTypeProcedure : id du type de procédure à vérifier
     *  @param : $calledFromPortail à true s'il faut rechercher le type de procédure dans la base commune,
     *           à false si dans la base organisme
     *  @param : $organisme : contient le nom de l'organisme où rechercher le type de procédure
     *           (utile uniquement si $calledFromPortail égale à false)
     */
    public function isAC($idTypeProcedure, $calledFromPortail = false, $organisme = null)
    {
        $listTypeProcedures = self::getCachedTypeProcedure($calledFromPortail, $organisme);
        $typeProcedure = $listTypeProcedures[$idTypeProcedure];
        if ($typeProcedure instanceof CommonTypeProcedureOrganisme) {
            return (1 == $typeProcedure->getAccordCadre()) ? true : false;
        }

        return false;
    }

    /**
     *  Test si un type de procédure donné est une(e) Mapa de type Mapa inférieur à ...
     *  retourne vrai si Mapa inférieur.
     *
     *  @param : $idTypeProcedure : id du type de procédure à vérifier
     *  @param : $organisme : contient le nom de l'organisme où rechercher le type de procédure
     */
    public function isMapaInferieur($idTypeProcedure, $organisme)
    {
        $listTypeProcedures = self::getCachedTypeProcedure(false, $organisme);
        $typeProcedure = $listTypeProcedures[$idTypeProcedure];
        if ($typeProcedure instanceof CommonTypeProcedureOrganisme) {
            return (1 == $typeProcedure->getMapa() && $typeProcedure->getIdMontantMapa() == Atexo_Config::getParameter('MONTANT_MAPA_INF_90')) ? true : false;
        }

        return false;
    }

    /**
     *  Test si un type de procédure donné est un(e) Mapa de type Mapa supérieur à ...
     *  retourne vrai si Mapa supérieur.
     *
     *  @param : $idTypeProcedure : id du type de procédure à vérifier
     *  @param : $organisme : contient le nom de l'organisme où rechercher le type de procédure
     */
    public function isMapaSuperieur($idTypeProcedure, $organisme)
    {
        $listTypeProcedures = self::getCachedTypeProcedure(false, $organisme);
        $typeProcedure = $listTypeProcedures[$idTypeProcedure];
        if ($typeProcedure instanceof CommonTypeProcedureOrganisme) {
            return (1 == $typeProcedure->getMapa() && $typeProcedure->getIdMontantMapa() == Atexo_Config::getParameter('MONTANT_MAPA_SUP_90')) ? true : false;
        }

        return false;
    }

    public function rerieveTypeProcedureSaufMapa($returnIds = false, $calledFromPortail = false, $organisme = null)
    {
        $resultat = [];
        $listTypeProcedures = self::getCachedTypeProcedure($calledFromPortail, $organisme);
        ksort($listTypeProcedures);
        if (is_array($listTypeProcedures)) {
            foreach ($listTypeProcedures as $idTypeProcedure => $typePocedure) {
                if (!$this->isMapa($idTypeProcedure, $calledFromPortail, $organisme)) {
                    if ($returnIds) {
                        $resultat[$idTypeProcedure] = $typePocedure;
                    } else {
                        $resultat[$idTypeProcedure] = $idTypeProcedure;
                    }
                }
            }
        }

        return $resultat;
    }

    public function rerieveMapas($returnIds = false, $calledFromPortail = false, $organisme = null)
    {
        $resultat = [];
        $listTypeProcedures = self::getCachedTypeProcedure($calledFromPortail, $organisme);
        ksort($listTypeProcedures);
        if (is_array($listTypeProcedures)) {
            foreach ($listTypeProcedures as $idTypeProcedure => $typePocedure) {
                if ($this->isMapa($idTypeProcedure, $calledFromPortail, $organisme)) {
                    if ($returnIds) {
                        $resultat[$idTypeProcedure] = $typePocedure;
                    } else {
                        $resultat[$idTypeProcedure] = $idTypeProcedure;
                    }
                }
            }
        }

        return $resultat;
    }

    public function rerieveAllProcedure($returnIds = false, $calledFromPortail = false, $organisme = null)
    {
        $resultat = [];
        $listTypeProcedures = self::getCachedTypeProcedure($calledFromPortail, $organisme);
        ksort($listTypeProcedures);
        if ($returnIds) {
            if (is_array($listTypeProcedures)) {
                $typePocedure = '';
                foreach ($listTypeProcedures as $idTypeProcedure => $typePocedure) {
                    $resultat[$idTypeProcedure] = $idTypeProcedure;
                }
            }

            return $resultat;
        } else {
            return is_array($listTypeProcedures) ? $listTypeProcedures : [];
        }
    }

    /**
     * Retourne l'objet type procedure de la consultation.
     *
     * @param $idTypeProcedure identifiant de type procedure
     */
    public function retrieveTypeProcedureObjectByAbreviationInterface($abreviationInterface, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonTypeProcedureOrganismePeer::ABREVIATION_INTERFACE, $abreviationInterface);
        $c->add(CommonTypeProcedureOrganismePeer::ORGANISME, $organisme);
        $typeProcedure = CommonTypeProcedureOrganismePeer::doSelectOne($c, $connexion);
        if ($typeProcedure instanceof CommonTypeProcedureOrganisme) {
            return $typeProcedure;
        }

        return false;
    }

    public function retrieveIdTypeProcedureByAbreviation($abreviation)
    {
        $c = new Criteria();
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonTypeProcedureOrganismePeer::ORGANISME, Atexo_CurrentUser::getCurrentOrganism());
        $c->add(CommonTypeProcedureOrganismePeer::ABREVIATION_INTERFACE, $abreviation);
        $typeProcedure = CommonTypeProcedureOrganismePeer::doSelectOne($c, $connexion);
        if ($typeProcedure instanceof CommonTypeProcedureOrganisme) {
            return $typeProcedure->getIdTypeProcedure();
        } else {
            return false;
        }
    }

    public function retrieveTypeProcedureObjectByAbreviation($abreviation)
    {
        $c = new Criteria();
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonTypeProcedurePeer::ABBREVIATION, $abreviation);
        $typeProcedure = CommonTypeProcedurePeer::doSelectOne($c, $connexion);
        if ($typeProcedure instanceof CommonTypeProcedure) {
            return $typeProcedure;
        } else {
            return false;
        }
    }

    public function getIdTypeProcedureByAbbreviation($abbreviation)
    {
        $typeProcedure = (new Atexo_Consultation_ProcedureType())->retrieveTypeProcedureObjectByAbreviation($abbreviation);
        if ($typeProcedure instanceof CommonTypeProcedure) {
            return $typeProcedure->getIdTypeProcedure();
        } else {
            return false;
        }
    }

    public function getAbbreviationTypeProcedureById($idTypeProcedure)
    {
        $typeProcedure = (new Atexo_Consultation_ProcedureType())->getCommonTypeProcedureById($idTypeProcedure);
        if ($typeProcedure instanceof CommonTypeProcedure) {
            return $typeProcedure->getAbbreviation();
        } else {
            return false;
        }
    }

    public function rerieveAllTypeProcedureAndTypeProcedurePortail($organisme = null)
    {
        if (!$organisme) {
            $organisme = Atexo_CurrentUser::getCurrentOrganism();
        }
        $resultat = [];
        $sql = " SELECT `id_type_procedure` , `id_type_procedure_portail` FROM `Type_Procedure_Organisme` WHERE 1 AND organisme='".$organisme."'";
        //$stmt = $connexion->createStatement();
        //$res = $stmt->executeQuery($sql, ResultSet::FETCHMODE_NUM);
        $stmt = Atexo_Db::getLinkCommon(true)->prepare($sql);
        $stmt->execute();
        $res = $stmt->fetchAll(PDO::FETCH_NUM);
        foreach ($res as $rs) {
            $resultat[] = $rs;
        }
        $resultat[] = ['MAPA_INF', 'MAPA_INF'];
        $resultat[] = ['MAPA_SUPP', 'MAPA_SUPP'];
        foreach ($resultat as $un) {
            $resultatF[$un['0']] = $un['1'];
        }

        return is_array($resultatF) ? $resultatF : [];
    }

    public function initStatByTypeProcedure($portail, $org = null)
    {
        if (!$org) {
            $org = Atexo_CurrentUser::getCurrentOrganism();
        }
        $resultat = [];
        if ($portail) {
            $sql = ' SELECT `id_type_procedure` FROM `TypeProcedure` WHERE 1 ';
        } else {
            $sql = ' SELECT `id_type_procedure` ';
            $sql .= " FROM `Type_Procedure_Organisme` WHERE 1 AND organisme='".$org."' ";
        }

        $statement = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_OBJ);
        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            $resultat[$row['id_type_procedure']] = '0';
        }
        if ($portail) {
            $resultat['MAPA_INF'] = '0';
            $resultat['MAPA_SUPP'] = '0';
        }

        return is_array($resultat) ? $resultat : [];
    }

    public function retrieveArticleByProcedureType($idTypeProcedure, $modePassation)
    {
        $c = new Criteria();
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonArticlesPeer::ID_TYPE_PROCEDURE, $idTypeProcedure);
        $c->add(CommonArticlesPeer::MODE_PASSATION, $modePassation);
        $article = CommonArticlesPeer::doSelectOne($c, $connexion);
        if ($article instanceof CommonArticles) {
            return $article;
        } else {
            return false;
        }
    }

    public function retrieveByOrganismeTypeProcedureBoamp($org, $procedureBoamp)
    {
        $c = new Criteria();
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonTypeProcedureOrganismePeer::ORGANISME, $org);
        $c->add(CommonTypeProcedureOrganismePeer::EQUIVALENT_BOAMP, '%#'.$procedureBoamp.'#%', Criteria::LIKE);
        $typeProcedure = CommonTypeProcedureOrganismePeer::doSelectOne($c, $connexion);
        if ($typeProcedure instanceof CommonTypeProcedureOrganisme) {
            return $typeProcedure;
        } else {
            return false;
        }
    }

    public function retrieveTypeProcedureObjectByAbreviationAnm($abreviation, $justId = false)
    {
        $c = new Criteria();
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonTypeProcedurePeer::ABBREVIATION_PORTAIL_ANM, $abreviation);
        $typeProcedure = CommonTypeProcedurePeer::doSelectOne($c, $connexion);
        if ($typeProcedure instanceof CommonTypeProcedure) {
            if ($justId) {
                return $typeProcedure->getIdTypeProcedure();
            } else {
                return $typeProcedure;
            }
        } else {
            return false;
        }
    }

    public function getAbbreviationAnmTypeProcedureById($idTypeProcedure)
    {
        $typeProcedure = (new Atexo_Consultation_ProcedureType())->getCommonTypeProcedureById($idTypeProcedure);
        if ($typeProcedure instanceof CommonTypeProcedure) {
            return $typeProcedure->getAbbreviationPortailAnm();
        } else {
            return false;
        }
    }

    /**
     * retourne la liste des type de procedures by idType contrat et l'organimse.
     *
     * @param int    $idTypeContrat
     * @param string $organisme     l'organisme
     *
     * @return la liste des types de procedures
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public static function retrieveProcedureTypeByTypeContrat($idTypeContrat, $organisme)
    {
        $arrayIdProc = self::getArrayIdProcedureByTypeContrat($idTypeContrat, $organisme);
        $typeProcdures = self::getCommonTypeProcedureByArrayId($arrayIdProc, $organisme);

        return $typeProcdures;
    }

    /**
     * retourne la liste des ids de procedures by idType contrat et l'organimse.
     *
     * @param int    $idTypeContrat
     * @param string $organisme     l'organisme
     *
     * @return array la liste des ids de procedures
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public static function getArrayIdProcedureByTypeContrat($idTypeContrat, $organisme)
    {
        $arrayIdProc = [];
        $tTypeContratProcQuery = new CommonTTypeContratEtProcedureQuery();
        $tTypeContratProcQuery = (array) $tTypeContratProcQuery->getTypeContratEtProcedureByIdContrat($idTypeContrat, $organisme);
        if (is_array($tTypeContratProcQuery)) {
            foreach ($tTypeContratProcQuery as $oneObjet) {
                if ($oneObjet instanceof CommonTTypeContratEtProcedure) {
                    $arrayIdProc[] = $oneObjet->getIdTypeProcedure();
                }
            }
        }

        return $arrayIdProc;
    }

    /**
     * retourne la liste des type de procedures by array des id  et l'organimse.
     *
     * @param array la liste des ids de procedures
     * @param string $organisme l'organisme
     *
     * @return la liste des types de procedures
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public static function getCommonTypeProcedureByArrayId($arrayId, $organisme)
    {
        $typeProcOrgQuery = new CommonTypeProcedureOrganismeQuery();
        $procedureListe = (array) $typeProcOrgQuery->getTypeProcByIds($arrayId, $organisme);

        return $procedureListe;
    }

    /**
     * Permet de retourner les type des procédure d'un organisme.
     *
     * @param string $organisme l'organisme
     *
     * @return array type de procédure
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2016
     */
    public function getTypeProcedureByOrganisme($organisme = null, $withMapa = false)
    {
        if (!$organisme) {
            $organisme = Atexo_CurrentUser::getOrganismAcronym();
        }
        $arrayTypeProcedure = [];
        $procedureTypes = (array) self::retrieveProcedureTypeByTypeContrat(null, $organisme);
        if (is_array($procedureTypes) && count($procedureTypes)) {
            foreach ($procedureTypes as $procedureType) {
                $arrayTypeProcedure[$procedureType->getIdTypeProcedure()] = Atexo_Util::atexoHtmlEntitiesDecode($procedureType->getLibeleTypeProcedureTraduit());
            }
        }

        if (true === $withMapa) {
            foreach (self::retrieveAllMapa(Atexo_CurrentUser::getCurrentOrganism(), true) as $procedure) {
                if ('1' === $procedure->getMapa()) {
                    $arrayTypeProcedure[$procedure->getIdTypeProcedure()] = Atexo_Util::atexoHtmlEntitiesDecode(
                        $procedure->getLibeleTypeProcedureTraduit().' ('.$procedure->getAbbreviation().')'
                    );
                }
            }
        }

        return $arrayTypeProcedure;
    }

    public static function retrieveProcedureTypesConcessionPivot()
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $allTypesProcedureConcessionPivot = [];
        $typeProcuery = new CommonTypeProcedureConcessionPivotQuery();
        $rowTypesProcedureConcessionPivot = (array) $typeProcuery->find($connexion);

        foreach ($rowTypesProcedureConcessionPivot as $oneTypeProcedureConcessionPivot) {
            $allTypesProcedureConcessionPivot[$oneTypeProcedureConcessionPivot->getId()] = $oneTypeProcedureConcessionPivot;
        }

        return $allTypesProcedureConcessionPivot;
    }

    /**
     * Permet de retourner le type procedure organisme by abbrevation.
     *
     * @param string       $abbreviation
     * @param string       $organisme    l'organisme
     * @param optional int $idTypeProc   id ty procedure à exclure
     *
     * @return CommonTypeProcedureOrganisme
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2018-roadmap
     *
     * @copyright Atexo 2018
     */
    public static function getTypeProcedureOrganismeByAbbreviation($abbreviation, $organisme, $idTypeProc = null)
    {
        $typeProcOrgQuery = new CommonTypeProcedureOrganismeQuery();

        return $typeProcOrgQuery->getTypeProcByAbbreviation($abbreviation, $organisme, $idTypeProc);
    }
}
