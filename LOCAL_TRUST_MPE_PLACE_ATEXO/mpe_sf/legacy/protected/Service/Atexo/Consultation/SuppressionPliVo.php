<?php

namespace Application\Service\Atexo\Consultation;

use Application\Propel\Mpe\CommonConsultation;
use Application\Service\Atexo\Atexo_Config;
use Prado\Prado;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Consultation_SuppressionPliVo extends CommonConsultation
{
    protected $_idOffre;
    protected $_idEnveloppe;
    protected $_nomEntreprise;
    protected $_typeEnv;
    protected $_natureEnveloppe;
    protected $_enveloppesExistante;
    protected $_sousPli;
    protected $_statutEnveloppe;
    protected $_typeEnveloppe;
    protected ?string $_organisme = null;

    public function getIdOffre()
    {
        return $this->_idOffre;
    }

    public function setIdOffre($idOffre)
    {
        $this->_idOffre = $idOffre;
    }

    public function getIdEnveloppe()
    {
        return $this->_idEnveloppe;
    }

    public function setIdEnveloppe($idEnveloppe)
    {
        $this->_idEnveloppe = $idEnveloppe;
    }

    public function getNomEntreprise()
    {
        return $this->_nomEntreprise;
    }

    public function setNomEntreprise($nomEntreprise)
    {
        $this->_nomEntreprise = $nomEntreprise;
    }

    public function getTypeEnv()
    {
        return $this->_typeEnv;
    }

    public function setTypeEnv($typeEnv)
    {
        $this->_typeEnv = $typeEnv;
    }

    public function getLibelleTypeEnveloppe()
    {
        return match ($this->_typeEnv) {
            Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE') => Prado::localize('CANDIDATURE'),
            Atexo_Config::getParameter('TYPE_ENV_OFFRE') => Prado::localize('OFFRE'),
            Atexo_Config::getParameter('TYPE_ENV_ANONYMAT') => Prado::localize('ANONYMAT'),
            default => '',
        };
    }

    public function getNatureEnveloppe()
    {
        return $this->_natureEnveloppe;
    }

    public function setNatureEnveloppe($natureEnv)
    {
        $this->_natureEnveloppe = $natureEnv;
    }

    public function getEnveloppesExistante()
    {
        return $this->_enveloppesExistante;
    }

    public function setEnveloppesExistante($enveloppesExistante)
    {
        $this->_enveloppesExistante = $enveloppesExistante;
    }

    public function getSousPli()
    {
        return $this->_sousPli;
    }

    public function setSousPli($sousPli)
    {
        $this->_sousPli = $sousPli;
    }

    public function getStatutEnveloppe()
    {
        return $this->_statutEnveloppe;
    }

    public function setStatutEnveloppe($statut)
    {
        $this->_statutEnveloppe = $statut;
    }

    public function getTypeEnveloppe()
    {
        return $this->_typeEnveloppe;
    }

    public function setTypeEnveloppe($typeEnv)
    {
        $this->_typeEnveloppe = $typeEnv;
    }

    public function getOrganisme()
    {
        return $this->_organisme;
    }

    public function setOrganisme($organisme)
    {
        $this->_organisme = $organisme;
    }
}
