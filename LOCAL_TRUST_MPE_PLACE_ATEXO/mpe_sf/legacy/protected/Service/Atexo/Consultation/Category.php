<?php

namespace Application\Service\Atexo\Consultation;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonCategorieConsultation;
use Application\Propel\Mpe\CommonCategorieConsultationQuery;
use Application\Propel\Mpe\CommonSousCategorie;
use Application\Propel\Mpe\CommonSousCategoriePeer;
use Application\Propel\Mpe\CommonSousCategorieQuery;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_Languages;
use PDO;
use Prado\Prado;

/**
 * Classe de.
 *
 * @author Khalid BENAMAR <khalid.benamar@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Consultation_Category
{
    /**
     * retourne la liste des categories.
     *
     * @param $calledFromPortail a true pour recuperer les donnees a partir de la base commune a false pour la base organisme,
     * $returnPropelObject : par defaut a true si l'objet retourne doir etre un tableau d'objet propel, a false
     * si l'objet retourne doit etre un tableau de type array [id categorie] = libelle
     * $indexedPropelArray : a true si le tableau d'objet propel retourne doit etre indexe par id de type de categorie
     *
     * @return les categories
     */
    public static function retrieveCategories($calledFromPortail = false, $returnPropelObjectsArray = true, $langue = null)
    {
        $rowCategories = self::getCachedCategories($calledFromPortail);
        if ($rowCategories) {
            if ($returnPropelObjectsArray) {
                return $rowCategories;
            } else {
                $arrayCategorie = [];
                foreach ($rowCategories as $categorie) {
                    $getLibelle = 'getLibelle'.Atexo_Languages::getLanguageAbbreviation($langue);
                    if (!$langue) {
                        $arrayCategorie[$categorie->getId()] = $categorie->getLibelle();
                    } else {
                        if (0 == strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE'))
                        || !$categorie->$getLibelle()) {
                            $arrayCategorie[$categorie->getId()] = $categorie->getLibelle();
                        } else {
                            $arrayCategorie[$categorie->getId()] = $categorie->$getLibelle();
                        }
                    }
                }

                return $arrayCategorie;
            }
        } else {
            return [];
        }
    }

    /**
     * Calcule le nombre de separateur dans l'id de la categorie.
     *
     * @param  $idCategorie id de la categorie
     * @param  $separator le separateur
     *
     * @return nombre de separeteur dans l'id
     */
    public static function countSeparatorInCategorie($idCategorie, $separator)
    {
        /*
         ael : au lieu d'utiliser une strpos, une boucle et un substr,  utiliser :
         return substr_count($idCategorie,$separator);
         */
        $strToCountSeparator = $idCategorie;
        $nbNbsp = 0;

        while ($pos = strpos($strToCountSeparator, (string) $separator) > 0) {
            ++$nbNbsp;
            $strToCountSeparator = substr($strToCountSeparator, $pos + 1);
        }

        return $nbNbsp;
    }

    /**
     * Calcule le nombre de separateur dans l'id de la categorie.
     *
     * @param  $categories la liste des objets categories
     *
     * @return tableau des categories pour alimenter une liste
     */
    public static function retrieveDataCategories($categories, $langue = null)
    {
        $dataCategories = [];
        if ($categories) {
            $dataCategories[0] = '--- '.Prado::localize('DEFAULT_CATEGORY_SELECTED').' ---';
            foreach ($categories as $category) {
                $getLibelle = 'getLibelle'.Atexo_Languages::getLanguageAbbreviation($langue);
                if (!$langue) {
                    $dataCategories[$category->getId()] = $category->getId().' '.$category->getLibelle();
                } else {
                    if (0 == strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE'))
                        || !$category->$getLibelle()) {
                        $dataCategories[$category->getId()] = $category->getId().' '.$category->getLibelle();
                    } else {
                        $dataCategories[$category->getId()] = $category->getId().' '.$category->$getLibelle();
                    }
                }
                $categorie_ids = [];
                // on extrait de l'id de la categorie la racine XX.YY
                preg_match("/^(.*)\.[0-9]+/m", $category->getId(), $categorie_ids);
                // On construit la valeur de la categorie avec  &nbsp; + id + libelle

                if (!$langue) {
                    $dataCategories[$category->getId()] = $category->getLibelle();
                } else {
                    if (0 == strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE'))
                        || !$category->$getLibelle()) {
                        $dataCategories[$category->getId()] = $category->getLibelle();
                    } else {
                        $dataCategories[$category->getId()] = $category->$getLibelle();
                    }
                }
            }
        }

        return $dataCategories;
    }

    /**
     * Return l'objet CommonCategorieConsultation de la consultation.
     *
     * @param $idCategorie
     * @param $calledFromPortail true si appel depuis le portail (ex pmi.local-trust.com/),
     * false sinon (pmi.local-trust.com/ministere ou pmi.local-trust.com/agent)
     *
     * @return object CommonCategorieConsultation
     */
    public static function retrieveCategorie($idCategorie, $calledFromPortail = false)
    {
        $categorieConsultationQuery = new CommonCategorieConsultationQuery();

        return $categorieConsultationQuery->getCategorieConsultationById($idCategorie);
    }

    /**
     * Retourne le libelle de la categorie de la consultation.
     *
     * @param int  $idCategorie
     * @param bool $calledFromPortail true si appel depuis le portail (ex pmi.local-trust.com/),
     *                                false sinon (pmi.local-trust.com/ministere ou pmi.local-trust.com/agent)
     *
     * @return object Categorie
     */
    public static function retrieveLibelleCategorie($idCategorie, $calledFromPortail = false, $organisme = null, $langue = null)
    {
        $categories = self::getCachedCategories($calledFromPortail, $organisme);
        $getLibelle = 'getLibelle'.Atexo_Languages::getLanguageAbbreviation($langue);
        if ($categories[$idCategorie] instanceof CommonCategorieConsultation) {
            if (0 == strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE'))
            || !$categories[$idCategorie]->$getLibelle()) {
                return $categories[$idCategorie]->getLibelle();
            } else {
                return $categories[$idCategorie]->$getLibelle();
            }
        } else {
            return '';
        }
    }

    public static function getCachedCategories($calledFromPortail = false, $organisme = null)
    {
        $allCategories = [];
        $categorieConsultationQuery = new CommonCategorieConsultationQuery();
        $rowCategories = $categorieConsultationQuery->find();

        foreach ($rowCategories as $oneCategory) {
            $allCategories[$oneCategory->getId()] = $oneCategory;
        }

        return $allCategories;
    }

    /**
     *  retourne la categorie consultation traduite selon la langue passe en parametre.
     */
    public function retrieveCatgorieConsultationTraduit($idCategorie, $langue)
    {
        $categorie = Atexo_Consultation_Category::retrieveCategorie($idCategorie, true);
        $libelleTraduit = 'getLibelle'.Atexo_Languages::getLanguageAbbreviation($langue);
        if ($categorie) {
            return 0 == strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE'))
            || !$categorie->$libelleTraduit() ? $categorie->getLibelle() : $categorie->$libelleTraduit();
        } else {
            return '';
        }
    }

    public function retrieveCategoryObjectByLibelle($libelle)
    {
        $categorieConsultationQuery = new CommonCategorieConsultationQuery();

        return $categorieConsultationQuery->getCategorieConsultationByLibelle($libelle);
    }

    public function retrieveSousCategoryObjectByIdCat($idCategorie)
    {
        $c = new Criteria();
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonSousCategoriePeer::ID_CATEGORIE, $idCategorie);
        $category = CommonSousCategoriePeer::doSelect($c, $connexion);
        if ($category) {
            return $category;
        } else {
            return [];
        }
    }

    /**
     * mets es sous-categories dans le cache de l'application.
     */
    public static function setSousCategoriesInCache()
    {
        if (!Atexo_CurrentUser::readFromSession('cachedSousCategories')) {
            $sousCategorieQuery = new CommonSousCategorieQuery();
            $listeCategories = $sousCategorieQuery->getAllSousCategorie();
            if (1 == (is_countable($listeCategories) ? count($listeCategories) : 0) && !($listeCategories[0] instanceof CommonSousCategorie)) {
                unset($listeCategories[array_search('', $listeCategories)]);
            }

            $dataNiveau3 = [];
            $dataNiveau2 = [];

            $arId2 = [];
            $arId3 = [];
            $indexCat = '';
            foreach ($listeCategories as $indexCat => $oneSousCat) {
                if (2 == count(explode('.', $oneSousCat->getId()))) {
                    $coefIdSousCat = explode('.', $oneSousCat->getId());

                    $dataNiveau2[$oneSousCat->getId()]['id'] = $oneSousCat->getId();
                    $dataNiveau2[$oneSousCat->getId()]['libelle'] = $oneSousCat->getLibelleTraduit();
                    $dataNiveau2[$oneSousCat->getId()]['libelle_ar'] = $oneSousCat->getLibelleAr();
                    $dataNiveau2[$oneSousCat->getId()]['libelle_fr'] = $oneSousCat->getLibelleFr();
                    $dataNiveau2[$oneSousCat->getId()]['libelle_en'] = $oneSousCat->getLibelleEn();
                    $dataNiveau2[$oneSousCat->getId()]['libelle_es'] = $oneSousCat->getLibelleEs();
                    $dataNiveau2[$oneSousCat->getId()]['libelle_su'] = $oneSousCat->getLibelleSu();
                    $dataNiveau2[$oneSousCat->getId()]['libelle_du'] = $oneSousCat->getLibelleDu();
                    $dataNiveau2[$oneSousCat->getId()]['libelle_cz'] = $oneSousCat->getLibelleCz();
                    $dataNiveau2[$oneSousCat->getId()]['code'] = $oneSousCat->getCode();
                    $dataNiveau2[$oneSousCat->getId()]['node_id'] = $coefIdSousCat[0].'-'.$coefIdSousCat[1];
                    $dataNiveau2[$oneSousCat->getId()]['id_parent'] = $coefIdSousCat[0];
                    $dataNiveau2[$oneSousCat->getId()]['date_modification'] = $oneSousCat->getDateModification();
                    $dataNiveau2[$oneSousCat->getId()]['active'] = $oneSousCat->getActive();
                    $dataNiveau2[$oneSousCat->getId()]['niv1'] = $coefIdSousCat[0];

                    $arId2[$oneSousCat->getId()] = $coefIdSousCat[0] * 100 + $coefIdSousCat[1];
                } else {
                    $coefIdSousCat = explode('.', $oneSousCat->getId());

                    $dataNiveau3[$oneSousCat->getId()]['id'] = $oneSousCat->getId();
                    $dataNiveau3[$oneSousCat->getId()]['libelle'] = $oneSousCat->getLibelleTraduit();
                    $dataNiveau3[$oneSousCat->getId()]['libelle_ar'] = $oneSousCat->getLibelleAr();
                    $dataNiveau3[$oneSousCat->getId()]['libelle_fr'] = $oneSousCat->getLibelleFr();
                    $dataNiveau3[$oneSousCat->getId()]['libelle_en'] = $oneSousCat->getLibelleEn();
                    $dataNiveau3[$oneSousCat->getId()]['libelle_es'] = $oneSousCat->getLibelleEs();
                    $dataNiveau3[$oneSousCat->getId()]['libelle_su'] = $oneSousCat->getLibelleSu();
                    $dataNiveau3[$oneSousCat->getId()]['libelle_du'] = $oneSousCat->getLibelleDu();
                    $dataNiveau3[$oneSousCat->getId()]['libelle_cz'] = $oneSousCat->getLibelleCz();
                    $dataNiveau3[$oneSousCat->getId()]['code'] = $oneSousCat->getCode();
                    $dataNiveau3[$oneSousCat->getId()]['node_id'] = $coefIdSousCat[0].'-'.$coefIdSousCat[1].'-'.$coefIdSousCat[2];
                    $dataNiveau3[$oneSousCat->getId()]['id_parent'] = $coefIdSousCat[0].'-'.$coefIdSousCat[1];
                    $dataNiveau3[$oneSousCat->getId()]['date_modification'] = $oneSousCat->getDateModification();
                    $dataNiveau3[$oneSousCat->getId()]['active'] = $oneSousCat->getActive();
                    $dataNiveau3[$oneSousCat->getId()]['niv2'] = $coefIdSousCat[0].'.'.$coefIdSousCat[1];
                    $dataNiveau3[$oneSousCat->getId()]['niv1'] = $coefIdSousCat[0];

                    $arId3[$oneSousCat->getId()] = $coefIdSousCat[0] * 10000 + $coefIdSousCat[1] * 100 + $coefIdSousCat[2];
                }
            }

            // tri
            array_multisort($arId2, SORT_ASC, $dataNiveau2);
            array_multisort($arId3, SORT_ASC, $dataNiveau3);

            // imbriquation des tableaux
            foreach ($dataNiveau3 as $catNiv3) {
                $dataNiveau2[$catNiv3['niv2']]['SousNiveau'][] = $catNiv3;
            }
            Atexo_CurrentUser::writeToSession('cachedSousCategories', $dataNiveau2);
        }

        return Atexo_CurrentUser::readFromSession('cachedSousCategories');
    }

    public static function getCachedSousCategories()
    {
        return self::setSousCategoriesInCache();
    }

    public function retrieveListeSousCategorie($arrIds)
    {
        unset($arrIds[array_search('', $arrIds)]);
        if ((is_countable($arrIds) ? count($arrIds) : 0) > 0) {
            $c = new Criteria();
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            $c->add(CommonSousCategoriePeer::ID, $arrIds, Criteria::IN);
            $c->addAscendingOrderByColumn(CommonSousCategoriePeer::ID);
            $listeCategories = CommonSousCategoriePeer::doSelect($c, $connexionCom);
            if ($listeCategories) {
                return $listeCategories;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    /**
     * prend en paramettre des id separes avec des #.
     */
    public static function displayListeLibelleDomaineByArrayId($idsDomaines, $newLineSeparator = '<br/><br/>')
    {
        $dataSource = self::getCachedSousCategories();
        $libelleDomaine = '';
        if ($idsDomaines) {
            $arIds = explode('#', $idsDomaines);
            if (is_array($arIds)) {
                foreach ($arIds as $oneId) {
                    if ($oneId) {
                        $libelleDomaine .= self::getLibelleSousCategorieByIdInterne($oneId, $dataSource).$newLineSeparator;
                    }
                }
            }
        }

        return $libelleDomaine;
    }

    /**
     * retourne un tableau qui contient tout les niveaux possible d'un id interne donne.
     */
    public function getLibelleSousCategorieByIdInterne($idInterne, $dataSource)
    {
        $langue = Atexo_CurrentUser::readFromSession('lang');
        $champ_libelle = 'libelle_'.$langue;

        $listeCategories = Atexo_Consultation_Category::retrieveCategories(true, false, $langue);
        $coefIdSousCat = explode('.', $idInterne);
        $count = count($coefIdSousCat);
        $allLibelle = '';
        if (1 == $count) {
            $allLibelle = $listeCategories[$coefIdSousCat[0]];
        } elseif (2 == $count) {
            $idParent = $coefIdSousCat[0];
            $liballeCat = $listeCategories[$coefIdSousCat[0]];
            if (0 == strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE'))
            || !$dataSource[$idInterne][$champ_libelle]) {
                $libelle = $dataSource[$idInterne]['libelle'];
            } else {
                $libelle = $dataSource[$idInterne][$champ_libelle];
            }
            $allLibelle = $liballeCat.Atexo_Config::getParameter('SEPARATEUR_DOMAINES_ACTIVITES').$libelle;
        } elseif (3 == $count) {
            $idParent = $coefIdSousCat[0].'.'.$coefIdSousCat[1];
            $liballeCat = $listeCategories[$coefIdSousCat[0]];
            if (0 == strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE'))
            || !$dataSource[$idParent][$champ_libelle]) {
                $libelleParent = $dataSource[$idParent]['libelle'];
            } else {
                $libelleParent = $dataSource[$idParent][$champ_libelle];
            }

            if (0 == strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE'))
            || !$dataSource[$idParent]['SousNiveau'][$coefIdSousCat[2] - 1][$champ_libelle]) {
                $libelle = $dataSource[$idParent]['SousNiveau'][$coefIdSousCat[2] - 1]['libelle'];
            } else {
                $libelle = $dataSource[$idParent]['SousNiveau'][$coefIdSousCat[2] - 1][$champ_libelle];
            }

            $allLibelle = $liballeCat.Atexo_Config::getParameter('SEPARATEUR_DOMAINES_ACTIVITES').
                           $libelleParent.Atexo_Config::getParameter('SEPARATEUR_DOMAINES_ACTIVITES').$libelle;
        }

        return $allLibelle;
    }

    public function retrieveDomaineActiviteByLibelle($libelle)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonSousCategoriePeer::LIBELLE, '%'.$libelle.'%', Criteria::LIKE);
        $categorie = CommonSousCategoriePeer::doSelectOne($c, $connexionCom);

        return $categorie;
    }

    public function retreiveGategorieNiveau2($Idniv0, $idsousCat = false, $new = false, $active = false)
    {
        if ($idsousCat) {
            $elementsCat = explode('.', $idsousCat);
            if (3 == count($elementsCat) && (0 == $new || 1 == $new)) {
                $idsousCat = $elementsCat[0].'.'.$elementsCat[1];
            } elseif (2 == count($elementsCat) && 0 == $new) {
                $idsousCat = $elementsCat[0];
            }
        }
        $listeSousCat = self::getCachedSousCategories();
        $data = []; //[niv1]
        foreach ($listeSousCat as $oneSousCat) {
            if ($active && !$oneSousCat['active']) {
                continue;
            }
            if (($oneSousCat['niv1'] == $Idniv0 && !$idsousCat) || ($oneSousCat['niv1'] == $Idniv0 && $idsousCat && $oneSousCat['id'] === $idsousCat)) {
                $data[] = $oneSousCat;
            }
        }

        return $data;
    }

    /**
     * Permet de generer le nouveau code et id.
     */
    public function getNextIdEtCodeSousCategorie($idCat, $niv)
    {
        $sousCategorie = self::getCategorieFils($idCat, false);
        $resultat = [];
        if ($sousCategorie) {
            $elementsId = explode('.', $sousCategorie->getId());
            $elementsCode = $sousCategorie->getCode();
            if (1 == $niv) {
                $idFinal = $elementsId[0].'.'.($elementsId[1] + 1).'.'.'0';
                $codeFinal = $elementsCode + 1000;
                $codeFinal = substr_replace($codeFinal, '000', 3);
            } elseif (2 == $niv) {
                $idFinal = $elementsId[0].'.'.$elementsId[1].'.'.($elementsId[2] + 1);
                $codeFinal = $elementsCode + 1;
            }
            $resultat['id'] = $idFinal;
            $resultat['code'] = $codeFinal;
        }

        return $resultat;
    }

    /**
     * @param $idCat
     * @param $arrayObject
     * @param $active
     * Permet d'avoir les fils d'une categorie donnee
     */
    public function getCategorieFils($idCat, $arrayObject = true, $active = 3)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonSousCategoriePeer::ID, $idCat.'.%', Criteria::LIKE);
        if (3 != $active) {
            $c->add(CommonSousCategoriePeer::ACTIVE, $active);
        }
        $c->addDescendingOrderByColumn(CommonSousCategoriePeer::CODE);
        if ($arrayObject) {
            return $sousCategorie = CommonSousCategoriePeer::doSelect($c, $connexion);
        } else {
            return $sousCategorie = CommonSousCategoriePeer::doSelectOne($c, $connexion);
        }
    }

    public function retrieveDomaineActiviteByLibelleOrCode($element, $active = 3)
    {
        $element = (new Atexo_Db())->quote($element);
        $data = [];

        $querySelect = 'SELECT * FROM SousCategorie WHERE 1=1 ';
        if (3 != $active) {
            $querySelect .= " AND active='".$active."' ";
        }

        $querySelectFr = $querySelect;
        $querySelectFr .= " AND ( code like '%".$element."%'OR libelle_fr like '%".$element."%' )";
        $statement = Atexo_Db::getLinkCommon(true)->prepare($querySelectFr);
        $results = $statement->execute();
        if ($results) {
            $i = 0;
            while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                $data[$i]['id'] = $row['id'];
                $data[$i]['libelle'] = $row['libelle_fr'];
                $data[$i]['code'] = $row['code'];
                $data[$i]['active'] = $row['active'];
                ++$i;
            }
            if (0 != $i) {
                return $data;
            }
        }
        $querySelectAr = $querySelect;
        $querySelectAr .= " AND ( code like '%".$element."%' OR libelle_ar like '%".$element."%' )";
        $statement = Atexo_Db::getLinkCommon(true)->prepare($querySelectAr);
        $results = $statement->execute();
        $data = [];
        if ($results) {
            $i = 0;
            while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                $data[$i]['id'] = $row['id'];
                $data[$i]['libelle'] = $row['libelle_ar'];
                $data[$i]['code'] = $row['code'];
                $data[$i]['active'] = $row['active'];
                ++$i;
            }
            if (0 != $i) {
                return $data;
            }
        }
        $querySelectEn = $querySelect;
        $querySelectEn .= " AND ( code like '%".$element."%' OR libelle_en like '%".$element."%' )";
        $statement = Atexo_Db::getLinkCommon(true)->prepare($querySelectEn);
        $results = $statement->execute();
        $data = [];
        if ($results) {
            $i = 0;
            while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                $data[$i]['id'] = $row['id'];
                $data[$i]['libelle'] = $row['libelle_en'];
                $data[$i]['code'] = $row['code'];
                $data[$i]['active'] = $row['active'];
                ++$i;
            }
            if (0 != $i) {
                return $data;
            }
        }

        return $data;
    }

    /*
     * Permet de retourner l'id de categorie à partir d'une libelle
     *
     * @param $libelle(String) = libelle de la categorie
     * @return $id (Int) = l'id de la categorie s'il existe sinon on retourne false
     * @author : loubna.ezziani@atexom.com
     * @copyright Atexo 2014
     * @version : 1.0
     * @since : 4.6.0
     */
    public function retrieveIdCategoryByLibelle($libelle)
    {
        $category = self::retrieveCategoryObjectByLibelle($libelle);
        if ($category instanceof CommonCategorieConsultation) {
            return $category->getId();
        } else {
            return false;
        }
    }

    public function retrievePictoCategorie($idCategorie)
    {
        $categorieConsultationQuery = new CommonCategorieConsultationQuery();
        $categorie = $categorieConsultationQuery->getCategorieConsultationById($idCategorie);

        if ($categorie) {
            if ($categorie->getLibelle() == Prado::localize('DEFINE_TRAVAUX')) {
                return Atexo_Config::getParameter('PICTO_TRAVAUX');
            } elseif ($categorie->getLibelle() == Prado::localize('DEFINE_FOURNITURES')) {
                return Atexo_Config::getParameter('PICTO_FOURNITURES');
            } elseif ($categorie->getLibelle() == Prado::localize('DEFINE_SERVICES')) {
                return Atexo_Config::getParameter('PICTO_SERVICES');
            }
        }
    }
}
