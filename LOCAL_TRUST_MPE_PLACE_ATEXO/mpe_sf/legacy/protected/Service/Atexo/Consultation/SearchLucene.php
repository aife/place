<?php

namespace Application\Service\Atexo\Consultation;

use Application\Propel\Mpe\CommonConsultation;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;
use Prado\Util\TLogger;
use ZendSearch\Lucene\Analysis\Analyzer\Analyzer;
use ZendSearch\Lucene\Analysis\Analyzer\Common\Text\CaseInsensitive;
use ZendSearch\Lucene\Document;
use ZendSearch\Lucene\Document\Field;
use ZendSearch\Lucene\Exception as LuceneException;
use ZendSearch\Lucene\Index\Term as IndexTerm;
use ZendSearch\Lucene\Lucene;
use ZendSearch\Lucene\Search\Query\Boolean;
use ZendSearch\Lucene\Search\Query\Fuzzy;
use ZendSearch\Lucene\Search\Query\MultiTerm;
use ZendSearch\Lucene\Search\Query\Term as QueryTerm;
use ZendSearch\Lucene\Search\Query\Wildcard;

/**
 * Classe de Atexo_Consultation_SearchLucene.
 *
 * @author Thibaut DECAUDAIN <thibaut.decaudain@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Consultation_SearchLucene
{
    private $_dirIndex;
    private ?\ZendSearch\Lucene\SearchIndexInterface $_index = null;
    private ?array $_stopWords = null;
    public static array $stop = [' est ', ' par ', '"', ' s\'', ' d\'', ' l\'', ' c ', ' ht ', ' s ', ' t ', ' m ', ' d ',
                           ' n ', ' op ', ' b ', ' sur ', ' en ', ' des ', ' pour ', ' le ', ' la ', ' les ', ' de ',
                           ' des ', ' du ', ' au ', ' aux ', ' un ', ' une ', ' à ', ' où ', ' ou ', ' et ', ' l ',
                           ' d ', ' a ', ' dans ', ' mise ', ' avec ', ' il ', ' entre ', ' qui ', ' sont ', ' ce ',
                           ' ainsi ', ];

    /**
     * Constructeur : ouvre l'index Lucenne, ou le cré si il n'existe pas,
     * dans le répertoire DIR_INDEX_LUCENE.
     *
     * @throws Atexo_Consultation_Exception : si réperoire inaccessible
     */
    public function __construct($configDirLucene = null)
    {
        if (is_null($configDirLucene)) {
            $configDirLucene = Atexo_Config::getParameter('DIR_INDEX_LUCENE');
        }
        if (isset($configDirLucene)) {
            $this->_dirIndex = $configDirLucene;
        } else {
            $this->_dirIndex = Atexo_Config::getParameter('COMMON_TMP').'indexLucene'; // Par défault ?
        }

        // Ouvre l'index correspondant à la langue
        try {
            $this->_index = Lucene::open($this->_dirIndex);
        } catch (\Exception $e) {
            try {
                // L'index n'existe pas, on le cré
                $this->_index = Lucene::create($this->_dirIndex);
            } catch (\Exception $e) {
                throw new Atexo_Consultation_Exception('Unable to reach the index directory of Lucene. Please check acces rules of the directory. '.$this->_dirIndex.' '.$e->getMessage());
            }
        }
        $this->init();
    }

    /**
     * Initialisation de l'index et des filtres de recherche.
     */
    private function init()
    {
        Analyzer::setDefault(new CaseInsensitive());

        // Liste de mots à exclure des recherches (meilleurs performances...)
        // TODO : ajouter les mots inutiles de toutes les langues...
        $this->_stopWords = ['le', 'la', 'les', 'de', 'des', 'du', 'au', 'aux', 'un', 'une', 'à', 'où', 'ou', 'et', 'l', 'd', ''];
    }

    /**
     * Optimisation de l'index.
     */
    public function optimizeIndex()
    {
        $this->_index->optimize();
    }

    /*
     * Retourne l'id (de l'index de Lucene) ou -1 si la consultation n'est pas dans l'index
     */
    private function getIdConsultation($organisme, $idCons)
    {
        $query = new MultiTerm();
        $query->addTerm(new IndexTerm(utf8_encode($organisme), 'organisme'), true);
        $query->addTerm(new IndexTerm(utf8_encode($idCons), 'idCons'), true);
        $hits = $this->_index->find($query);

        if ((is_countable($hits) ? count($hits) : 0) > 0) {
            return $hits[0]->id;
        } else {
            return -1;
        }
    }

    /**
     * Ajoute une consultation à l'index
     * A utiliser également pour modifier les information de l'index.
     *
     * @param $commonConsultationO CommonConsultation la consultation à enregistrer dans l'index
     * @param $arrayComlots tableau de CommonCategorieLot (les lots de la consultation)
     * @param $avis champs réserver à l'avis de consultation BOAMP
     * @param $deleteIfExists Cherche la consultation dans Lucene pour la supprimer (pour pas indexer en double)
     *
     * @return : id de la consultation ajoutée
     */
    public function addConsultation(CommonConsultation $commonConsultationO, $arrayComlots, $avis, $deleteIfExists = true)
    {
        $intitulesLots = null;
        $detailsLots = null;
        // Concaténation des information de recherche par langue
        if (!empty($arrayComlots)) {
            foreach ($arrayComlots as $comLot) {
                $intitulesLots .= ' '.$comLot->getDescription();
                //$intitulesLotsFr .= " " . $comLot->getDescriptionFr();
                //$intitulesLotsEn .= " " . $comLot->getDescriptionEn();
                //$intitulesLotsEs .= " " . $comLot->getDescriptionEs();
                //$intitulesLotsSu .= " " . $comLot->getDescriptionSu();
                //$intitulesLotsDu .= " " . $comLot->getDescriptionDu();
                //$intitulesLotsCz .= " " . $comLot->getDescriptionCz();

                $detailsLots .= ' '.$comLot->getDescriptionDetail();
                //$detailsLotsFr .= " " . $comLot->getDescriptionDetailFr();
                //$detailsLotsEn .= " " . $comLot->getDescriptionDetailEn();
                //$detailsLotsEs .= " " . $comLot->getDescriptionDetailEs();
                //$detailsLotsSu .= " " . $comLot->getDescriptionDetailSu();
                //$detailsLotsDu .= " " . $comLot->getDescriptionDetailDu();
                //$detailsLotsCz .= " " . $comLot->getDescriptionDetailCz();
            }
        }

        // Si la consultation est déjà dans l'index Lucene, on la supprime
        // pour la full réindexation quotidienne, on met le param à false, car vu qu'on réindexe à vide, pas besoin de vérifier (performance killer)
        if (true === $deleteIfExists) {
            $idConsLucene = $this->getIdConsultation($commonConsultationO->getOrganisme(), $commonConsultationO->getId());
            if ($idConsLucene >= 0) {
                $this->deleteConsultationByIdLucene($idConsLucene);
            }
        }

        // Ajout de la consultation a l'index
        $consultation = new Document();
        $consultation->addField(Field::Keyword('organisme', $commonConsultationO->getOrganisme()));
        $consultation->addField(Field::Keyword('idCons', $commonConsultationO->getId()));
        $ref = strtolower($commonConsultationO->getReferenceUtilisateur());
        //$consultation->addField(Field::Keyword('reference', utf8_encode($ref)));
        $titre = $commonConsultationO->getIntitule();
        $resume = $commonConsultationO->getObjet();
        if ($resume === $titre) {
            $resume = '';
        }
        $text = strtolower($titre.' '.$resume);
        $text = str_replace($ref, '', $text);
        $consultation->addField(Field::UnStored('defaut', utf8_encode($this->formatString($text.' '.strtolower($intitulesLots.' '.$detailsLots.' '.$avis)))));
        //$consultation->addField(Field::Text('fr', utf8_encode($this->formatString($commonConsultationO->getTitreFr() . " " . $commonConsultationO->getResumeFr() . " " . $intitulesLotsFr . " " . $detailsLotsFr . " " . $avis))));
        //$consultation->addField(Field::Text('en', utf8_encode($this->formatString($commonConsultationO->getTitreEn() . " " . $commonConsultationO->getResumeEn() . " " . $intitulesLotsEn . " " . $detailsLotsEn . " " . $avis))));
        //$consultation->addField(Field::Text('es', utf8_encode($this->formatString($commonConsultationO->getTitreEs() . " " . $commonConsultationO->getResumeEs() . " " . $intitulesLotsEs . " " . $detailsLotsEs . " " . $avis))));
        //$consultation->addField(Field::Text('su', utf8_encode($this->formatString($commonConsultationO->getTitreSu() . " " . $commonConsultationO->getResumeSu() . " " . $intitulesLotsSu . " " . $detailsLotsSu . " " . $avis))));
        //$consultation->addField(Field::Text('du', utf8_encode($this->formatString($commonConsultationO->getTitreDu() . " " . $commonConsultationO->getResumeDu() . " " . $intitulesLotsDu . " " . $detailsLotsDu . " " . $avis))));
        //$consultation->addField(Field::Text('cz', utf8_encode($this->formatString($commonConsultationO->getTitreCz() . " " . $commonConsultationO->getResumeCz() . " " . $intitulesLotsCz . " " . $detailsLotsCz . " " . $avis))));
        $this->_index->addDocument($consultation);
        //Prado::log("Ajout de la consultation " . $commonConsultationO->getReferenceUtilisateur() . " (organisme = " . $commonConsultationO->getOrganisme() . ", id = " . $commonConsultationO->getReference() . ")"  ,TLogger::INFO, "Atexo.Consultation.SearchLucene");
    }

    /**
     * Suprime une consultation.
     *
     * @param : $idLucene : id Lucene (de l'index) de la consultation
     */
    private function deleteConsultationByIdLucene($idLucene)
    {
        try {
            $this->_index->delete($idLucene);
        } catch (LuceneException $e) {
            throw $e;
        }
    }

    /**
     * Suprime une consultation de l'index.
     *
     * @param : $organisme : acronyme de l'organisme
     * @param : $idCons    : id de la consultation
     *
     * @throws : Atexo_Consultation_Exception
     */
    public function deleteConsultation($organisme, $idCons)
    {
        $commonConsultationO = null;
        try {
            $idCons = $this->getIdConsultation($organisme, $idCons);
            $this->deleteConsultationByIdLucene($idCons);
        } catch (LuceneException $e) {
            Prado::log("Lucene : Echec de la supression d'une consultation : organisme = ".$commonConsultationO->getOrganisme().', id = '.$commonConsultationO->getReference(), TLogger::ERROR, 'Error');
            throw new Atexo_Consultation_Exception('Unable to delete the consultation from the Lucene index.'.$e->getMessage());
        }
    }

    /**
     * Effectue une recherche de consultation sur tous les champs de l'index de Lucene.
     *
     * @param : $requete    : chaine de caractères (mots à inclure dans la recherche)
     * @param : $précision : 0 < $précision < 1 (proche de 0 -> grande tolérance aux 'fautes')
     *
     * @return : tableau des clefs primaires , ie tableau de array(organisme, id)
     */
    public function searchInAllOrganisme($requete, $precision = 0.6)
    {
        $requete = $this->formatString($requete);
        $mots = $this->formatStringToArray($requete);

        // Création de la requète
        $query = new Boolean();

        $multipleQuery = new Boolean();
        // Pour chaque mot de la requete, on effectue une recherche floue
        foreach ($mots as $mot) {
            $term = new IndexTerm(utf8_encode($mot), null);
            $subQuery = new Fuzzy($term, $precision);
            $multipleQuery->addSubquery($subQuery, null);

            $pattern = new IndexTerm(utf8_encode($mot.'*'), null);
            $subQueryPattern = new Wildcard($pattern);
            $subQueryPattern::setMinPrefixLength(0);
            $multipleQuery->addSubquery($subQueryPattern, null);
        }
        $query->addSubquery($multipleQuery, true);
        $hits = $this->_index->find($query);

        // On récupère les consultations correspondants à la requète sous la forme
        // array(organisma => array(idConsultation))
        $listPrimaryKey = [];
        foreach ($hits as $hit) {
            $document = $hit->getDocument();
            $idCons = utf8_decode($document->idCons);
            $organisme = utf8_decode($document->organisme);
            $listPrimaryKey[$organisme][] = $idCons;
        }

        return $listPrimaryKey;
    }

    /**
     * Effectue une recherche sur les consultation d'un organisme.
     *
     * @param : $organisme: acronyme de l'organisme
     * @param : $requete    : chaine de caractères (mots à inclure dans la recherche)
     * @param : $précision : 0 < $précision < 1 (proche de 0 -> grande tolérance aux 'fautes')
     *
     * @return : tableau des ids des consultations trouvées
     */
    public function searchByOrganisme($organisme, $requete, $precision = 0.6)
    {
        $requete = $this->formatString($requete);
        $mots = $this->formatStringToArray($requete);

        // Création de la requète
        $query = new Boolean();

        // On ne recherche les consultations que pour l'organisme $organisme
        $term = new IndexTerm(utf8_encode($organisme), 'organisme');
        $subQuery = new QueryTerm($term);
        $query->addSubquery($subQuery, true);

        $multipleQuery = new Boolean();
        // Pour les mots de la requete, on effectue une recherche floue
        foreach ($mots as $mot) {
            $term = new IndexTerm(utf8_encode($mot), null);
            $subQuery = new Fuzzy($term, $precision);
            $multipleQuery->addSubquery($subQuery, null);

            $pattern = new IndexTerm(utf8_encode($mot.'*'), null);
            $subQueryPattern = new Wildcard($pattern);
            $subQueryPattern::setMinPrefixLength(0);

            $multipleQuery->addSubquery($subQueryPattern, null);
        }
        $query->addSubquery($multipleQuery, true);
        $hits = $this->_index->find($query);

        // On récupère les consultations correspondants à la requète sous la forme
        // d'un tableau d'Id de consultations
        $listPrimaryKey = [];
        foreach ($hits as $hit) {
            $document = $hit->getDocument();
            $idCons = utf8_decode($document->idCons);
            $listPrimaryKey[] = $idCons;
        }

        return array_unique($listPrimaryKey);
    }

    /**
     * Met une chaine en minuscule et enlève la ponctuation.
     */
    private function formatString($str)
    {
        $result = strtr($str, "àâäéèêëîïôöûùüç#²°,.;:!?()[]{}'\\", 'aaaeeeeiioouuuc                 ');
        $result = Atexo_Util::stripWordsCharacters($result);

        return trim(str_replace(self::$stop, ' ', $result));
    }

    /**
     * Transforme une chaine en tableau et supprime les mots non pertinents de la recherche
     * (stockés dans $_stopWords).
     */
    private function formatStringToArray($str)
    {
        $mots = explode(' ', $str);
        $mots = array_diff($mots, $this->_stopWords);
        $mots = array_unique($mots);

        return $mots;
    }

    /**
     * Fonctions de test.
     */
    public function printInfos($organisme, $idCons)
    {
        $idLucene = $this->getIdConsultation($organisme, $idCons);
        echo '<br>'.$idLucene;

        // Si la consultation n'est pas dans l'index
        if ($idLucene < 0) {
            return false;
        }

        // Si elle y est
        $consultation = $this->_index->getDocument($idLucene);
        echo '<br>';
        echo utf8_decode($consultation->fr);
        echo '<br>';
        echo utf8_decode($consultation->defaut);
    }

    public function printAll()
    {
        echo '<br><br>printall index : <br>';
        $hits = $this->_index->find('de');
        foreach ($hits as $hit) {
            $document = $hit->getDocument();
            $organisme = utf8_decode($document->organisme);
            $idCons = utf8_decode($document->idCons);
            $reference = utf8_decode($document->reference);
            $defaut = utf8_decode($document->defaut);

            echo '['.$organisme.']'.$hit->id.', '.$idCons.'/'.$reference.' : '.$defaut;
        }
    }
}
