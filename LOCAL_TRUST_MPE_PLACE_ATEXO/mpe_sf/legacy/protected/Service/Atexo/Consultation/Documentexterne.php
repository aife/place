<?php

namespace Application\Service\Atexo\Consultation;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonDocumentExternePeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use AtexoCrypto\Dto\Consultation;

class Atexo_Consultation_Documentexterne
{
    /*
     * Récupérer les fichiers documents externes d'une consultation
     */
    public function getDocumentsExternes($consultationId, $org, $lot = false, $typeDoc = null)
    {
        if (!$org) {
            $org = Atexo_CurrentUser::getCurrentOrganism();
        }
        $connection = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonDocumentExternePeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonDocumentExternePeer::ORGANISME, $org);
        if ($lot) {
            $c->add(CommonDocumentExternePeer::LOT, $lot);
        }
        if (!empty($typeDoc)) {
            $c->add(CommonDocumentExternePeer::TYPE, $typeDoc);
        }
        $results = CommonDocumentExternePeer::doSelect($c, $connection);
        if ($results) {
            return $results;
        } else {
            return [];
        }
    }
}
