<?php

namespace Application\Service\Atexo\Consultation;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Consultation_RegistreVo
{
    private $_idRegistre;
    private $_numRegistre;
    private $_horodatage;
    private $_nomEntreprise;
    private $_identifiantEntreprise;
    private $_sirenEntreprise;
    private $_nicEntreprise;
    private $_nomContact;
    private $_prenomContact;
    private $_mailContact;
    private $_telephoneContact;
    private $_faxContact;
    private $_adresseContact;
    private $_adresseContactSuite;
    private $_codePostalContact;
    private $_villeContact;
    private $_paysEntreprise;
    private $_accronymePaysEntreprise;
    private $_observation;
    private $_question;
    private $_dateReponse;
    private $_responder;
    private $_fichiers = [];
    private $_typeRegistre;
    private $_reference;
    private $_registreAnnule = false;
    private $_dateAnnulation = '';
    private $_offreVariante;
    private $_tiragePlan;
    private $_support;
    private $_idEntreprise;

    public function getIdRegistre()
    {
        return $this->_idRegistre;
    }

    public function setIdRegistre($idRegistre)
    {
        $this->_idRegistre = $idRegistre;
    }

    public function getNumRegistre()
    {
        return $this->_numRegistre;
    }

    public function setNumRegistre($numRegistre)
    {
        $this->_numRegistre = $numRegistre;
    }

    public function getHorodatage()
    {
        return $this->_horodatage;
    }

    public function setHorodatage($horodatage)
    {
        $this->_horodatage = $horodatage;
    }

    public function getNomEntreprise()
    {
        return $this->_nomEntreprise;
    }

    public function setNomEntreprise($nomEntreprise)
    {
        $this->_nomEntreprise = $nomEntreprise;
    }

    public function getIdentifiantEntreprise()
    {
        return $this->_identifiantEntreprise;
    }

    public function setIdentifiantEntreprise($identifiantEntreprise)
    {
        $this->_identifiantEntreprise = $identifiantEntreprise;
    }

    public function getSirenEntreprise()
    {
        return $this->_sirenEntreprise;
    }

    public function setSirenEntreprise($sirenEntreprise)
    {
        $this->_sirenEntreprise = $sirenEntreprise;
    }

    public function getNicEntreprise()
    {
        return $this->_nicEntreprise;
    }

    public function setNicEntreprise($nicEntreprise)
    {
        $this->_nicEntreprise = $nicEntreprise;
    }

    public function getNomContact()
    {
        return $this->_nomContact;
    }

    public function setNomContact($nomContact)
    {
        $this->_nomContact = $nomContact;
    }

    public function getPrenomContact()
    {
        return $this->_prenomContact;
    }

    public function setPrenomContact($prenomContact)
    {
        $this->_prenomContact = $prenomContact;
    }

    public function getMailContact()
    {
        return $this->_mailContact;
    }

    public function setMailContact($mailContact)
    {
        $this->_mailContact = $mailContact;
    }

    public function getTelephoneContact()
    {
        return $this->_telephoneContact;
    }

    public function setTelephoneContact($telephoneContact)
    {
        $this->_telephoneContact = $telephoneContact;
    }

    public function getFaxContact()
    {
        return $this->_faxContact;
    }

    public function setFaxContact($faxContact)
    {
        $this->_faxContact = $faxContact;
    }

    public function getAdresseContact()
    {
        return $this->_adresseContact;
    }

    public function setAdresseContact($adresseContact)
    {
        $this->_adresseContact = $adresseContact;
    }

    public function getAdresseContactSuite()
    {
        return $this->_adresseContactSuite;
    }

    public function setAdresseContactSuite($adresseContactSuite)
    {
        $this->_adresseContactSuite = $adresseContactSuite;
    }

    public function getPaysEntreprise()
    {
        return $this->_paysEntreprise;
    }

    public function setPaysEntreprise($paysEntreprise)
    {
        $this->_paysEntreprise = $paysEntreprise;
    }

    public function getAccronymePaysEntreprise()
    {
        return $this->_accronymePaysEntreprise;
    }

    public function setAccronymePaysEntreprise($accronymePaysEntreprise)
    {
        $this->_accronymePaysEntreprise = $accronymePaysEntreprise;
    }

    public function getCodePostalContact()
    {
        return $this->_codePostalContact;
    }

    public function setCodePostalContact($codePostalContact)
    {
        $this->_codePostalContact = $codePostalContact;
    }

    public function getVilleContact()
    {
        return $this->_villeContact;
    }

    public function setVilleContact($villeContact)
    {
        $this->_villeContact = $villeContact;
    }

    public function getObservation()
    {
        return $this->_observation;
    }

    public function setObservation($observation)
    {
        $this->_observation = $observation;
    }

    public function getQuestion()
    {
        return $this->_question;
    }

    public function setQuestion($question)
    {
        $this->_question = $question;
    }

    public function getDateReponse()
    {
        return $this->_dateReponse;
    }

    public function setDateReponse($dateReponse)
    {
        $this->_dateReponse = $dateReponse;
    }

    public function getResponder()
    {
        return $this->_responder;
    }

    public function setResponder($responder)
    {
        $this->_responder = $responder;
    }

    public function getFichiers()
    {
        return $this->_fichiers;
    }

    public function setFichiers(array $fichiersTelecharges)
    {
        $this->_fichiers = $fichiersTelecharges;
    }

    public function addFichiers($fichiersTelecharges)
    {
        $this->_fichiers[] = $fichiersTelecharges;
    }

    public function getTypeRegistre()
    {
        return $this->_typeRegistre;
    }

    public function setTypeRegistre($typeRegistre)
    {
        $this->_typeRegistre = $typeRegistre;
    }

    public function getReference()
    {
        return $this->_reference;
    }

    public function getId()
    {
        return $this->_reference;
    }

    public function setId($reference)
    {
        $this->_reference = $reference;
    }

    public function setReference($reference)
    {
        $this->_reference = $reference;
    }

    public function setRegistreAnnule($value)
    {
        $this->_registreAnnule = $value;
    }

    public function getRegistreAnnule()
    {
        return $this->_registreAnnule;
    }

    public function setDateAnnulation($value)
    {
        $this->_dateAnnulation = $value;
    }

    public function getDateAnnulation()
    {
        return $this->_dateAnnulation;
    }

    public function setOffreVariante($value)
    {
        $this->_offreVariante = $value;
    }

    public function getOffreVariante()
    {
        return $this->_offreVariante;
    }

    public function setTiragePlan($tiragePlan)
    {
        $this->_tiragePlan = $tiragePlan;
    }

    public function getTiragePlan()
    {
        return $this->_tiragePlan;
    }

    public function setSupport($support)
    {
        $this->_support = $support;
    }

    public function getSupport()
    {
        return $this->_support;
    }

    public function setIdEntreprise(?int $idEntreprise)
    {
        $this->_idEntreprise = $idEntreprise;
    }

    public function getIdEntreprise(): ?int
    {
        return $this->_idEntreprise;
    }
}
