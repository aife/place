<?php

namespace Application\Service\Atexo\Consultation;

use Application\Service\Atexo\Atexo_Consultation;

/**
 * Vo pour définir les critères de Atexo_Consultation::search().
 *
 * @author Guillaume Ponçon <guillaume.poncon@openstates.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 * @TODO commentaires à compléter
 */
class Atexo_Consultation_CriteriaVo
{
    private $referenceConsultation = '';
    private $referenceConsultationRestreinte = '';
    private $idReference;
    private $idTypeAvis = '';
    private $notIdTypeAvis = '';
    private $idTypeProcedure = '';
    private $idEtatConsultation = '';
    private $acronymeOrganisme = '';
    private $acronymeOrganismeProcRest = '';
    private $idService = '';
    private $idServiceRma = '';
    private $typeAcces = '';
    private $categorieConsultation = '';
    private $codeAccesProcedureRestreinte = '';
    private $idCodeCpv1 = '';
    private $idCodeCpv2 = '';
    private $idCodeCpv3 = '';
    private $idCodeCpv4 = '';
    private $dateFinStart = '';
    private $dateFinEnd = '';
    private $intituleConsultation = '';
    private $objetConsultation = '';
    private $commentaireInterne = '';
    private $intituleLot = '';
    private $description = '';
    private $typeSearch = '';
    private $keyWordRechercheRapide = '';
    private $keyWordAdvancedSearch = '';
    private $etatConsultation = '';
    private $defaultSortByElement = 'datefin';
    private $sortByElement = '';
    private $sensOrderBy = 'DESC';
    private $offset = 0;
    private $limit = 0;
    private $secondConditionOnEtatConsultation = '';
    private $searchModeExact = false;
    private $_lieuxExecution = '';
    private $_connectedAgentId;
    private $_calledFromPortail = false;
    private $_periodicite = '';
    private $_orgActive = '1';
    private $_publicationEurope = '';
    private $_etatPublicationToBePublished = false;
    private $_etatPublicationPublished = false;
    private $_etatPublicationToBeUpdated = false;
    private $_etatPublicationUpdated = false;
    private $_alertesFromYesterday = true;
    private $_calledFromElios = false;
    private $_sansCritereActive = false;
    private $_consultationAValiderSeulement = false;
    private $_consultationAApprouverSeulement = false;
    //A true pour banir les consultations visible temporairement pour validation ou attribution
    private $_noTemporaryCOnsultation = false;
    private $_enLigneDepuis;
    private $_enLigneJusquau;
    private $_orgDenomination;

    private $_typeOrganisme = '';
    private $_domaineActivite = '';
    private $_qualification = '';
    private $_agrements = '';
    private $_fromArchive = false;
    private $_codesNuts;
    private $_idsService = '';
    //tableau d'objet referentielVo
    private $referentielVo = [];
    private $_idInscrit = '';
    private $_idEntreprise = '';
    private $_forPanierEntreprise = false;
    private $_avecRetrait = false;
    private $_avecQuestion = false;
    private $_avecDepot = false;
    private $_avecEchange = false;
    private $_sansRetrait = false;
    private $_sansQuestion = false;
    private $_sansDepot = false;
    private $_sansEchange = false;
    private $idAlerteConsultation = 0;
    private $alerteConsultationCloturee = 0;
    //mettre a true pour recuperer les consultation publiees depuis hier
    private $publieHier = false;
    //mettre $withDce a true  pour chercher que les consultation ayant un DCE
    private $withDce = false;
    //mettre  $informationMarche a true pour retourner que les Avis de marches et les avis de pre-informations
    private $informationMarche = false;

    private $dateTimeFin = false;
    private $_avecConsClotureesSansPoursuivreAffichage = false;
    private $exclureConsultationExterne = false;
    private $afficherDoublon = false;

    private $denominationAdapte;
    private $clauseSociale = 0;
    private $atelierProtege = 0;
    private $ess = 0;
    private $siae = 0;
    private $clauseEnv = null;

    private $Mps = 0;

    private $consultaionPublier = false;
    //mettre a 1 pour recuperer les consultations annulees
    private $consultationAnnulee = false;
    // Accesseurs
    private $idOperation;
    private $inclureDescendance = true;
    private $rechercheAvanceeAutoCompletion = null;
    private $consultationAArchiver = false;
    private $bourseCotraitance = '0';
    private $visionRma = false;
    private $organismeTest = false;
    private $emailInscrit = '';

    private $consultationExterne = true;
    private $consultationExterneFilter = false;

    /**
     * @return bool
     */
    public function getOrganismeTest()
    {
        return $this->organismeTest;
    }

    /**
     * @param bool $organismeTest
     */
    public function setOrganismeTest($organismeTest)
    {
        $this->organismeTest = $organismeTest;
    }

    private $socialeCommerceEquitable;
    private $socialeInsertionActiviterEconomique;

    public function getSocialeCommerceEquitable()
    {
        return $this->socialeCommerceEquitable;
    }

    public function setSocialeCommerceEquitable($value)
    {
        $this->socialeCommerceEquitable = $value;
    }

    public function getSocialeInsertionActiviterEconomique()
    {
        return $this->socialeInsertionActiviterEconomique;
    }

    public function setSocialeInsertionActiviterEconomique($value)
    {
        $this->socialeInsertionActiviterEconomique = $value;
    }

    /**
     * @return string
     */
    public function getBourseCotraitance()
    {
        return $this->bourseCotraitance;
    }

    /**
     * @param string $bourseCotraitance
     */
    public function setBourseCotraitance($bourseCotraitance)
    {
        $this->bourseCotraitance = $bourseCotraitance;
    }

    public function getConsultationAnnulee()
    {
        return $this->consultationAnnulee;
    }

    public function setConsultationAnnulee($value)
    {
        $this->consultationAnnulee = $value;
    }

    public function getConsultaionPublier()
    {
        return $this->consultaionPublier;
    }

    public function setConsultaionPublier($value)
    {
        $this->consultaionPublier = $value;
    }

    public function getWithDce()
    {
        return $this->withDce;
    }

    public function setWithDce($value)
    {
        $this->withDce = $value;
    }

    public function getPublieHier()
    {
        return $this->publieHier;
    }

    public function setPublieHier($publieHier)
    {
        $this->publieHier = $publieHier;
    }

    public function getInformationMarche()
    {
        return $this->informationMarche;
    }

    public function setInformationMarche($informationMarche)
    {
        $this->informationMarche = $informationMarche;
    }

    public function getReferenceConsultation()
    {
        return $this->referenceConsultation;
    }

    public function setReferenceConsultation($referenceConsultation)
    {
        $this->referenceConsultation = $referenceConsultation;
    }

    public function getIdReference()
    {
        return $this->idReference;
    }

    public function setIdReference($idReference)
    {
        $this->idReference = $idReference;
    }

    public function getIdTypeAvis()
    {
        return $this->idTypeAvis;
    }

    public function setIdTypeAvis($idTypeAvis)
    {
        $this->idTypeAvis = $idTypeAvis;
    }

    public function getNotIdTypeAvis()
    {
        return $this->notIdTypeAvis;
    }

    public function setNotIdTypeAvis($notIdTypeAvis)
    {
        $this->notIdTypeAvis = $notIdTypeAvis;
    }

    public function getIdTypeProcedure()
    {
        return $this->idTypeProcedure;
    }

    public function setIdTypeProcedure($idTypeProcedure)
    {
        $this->idTypeProcedure = $idTypeProcedure;
    }

    public function getIdEtatConsultation()
    {
        return $this->idEtatConsultation;
    }

    public function setIdEtatConsultation($idEtatConsultation)
    {
        $this->idEtatConsultation = $idEtatConsultation;
    }

    public function getAcronymeOrganisme()
    {
        return $this->acronymeOrganisme;
    }

    public function setAcronymeOrganisme($acronymeOrganisme)
    {
        $this->acronymeOrganisme = $acronymeOrganisme;
    }

    public function getAcronymeOrganismeProcRest()
    {
        return $this->acronymeOrganismeProcRest;
    }

    public function setAcronymeOrganismeProcRest($acronymeOrganismeProRes)
    {
        $this->acronymeOrganismeProcRest = $acronymeOrganismeProRes;
    }

    public function getIdService()
    {
        return $this->idService;
    }

    public function setIdService($idService)
    {
        $this->idService = $idService;
    }

    public function getIdServiceRma()
    {
        return $this->idServiceRma;
    }

    public function setIdServiceRma($idService)
    {
        $this->idServiceRma = $idService;
    }

    public function getTypeAcces()
    {
        return $this->typeAcces;
    }

    public function setTypeAcces($typeAcces)
    {
        $this->typeAcces = $typeAcces;
    }

    public function getCodeAccesProcedureRestreinte()
    {
        return $this->codeAccesProcedureRestreinte;
    }

    public function setCodeAccesProcedureRestreinte($codeAccesProcedureRestreinte)
    {
        $this->codeAccesProcedureRestreinte = $codeAccesProcedureRestreinte;
    }

    public function getCategorieConsultation()
    {
        return $this->categorieConsultation;
    }

    public function setCategorieConsultation($categorieConsultation)
    {
        $this->categorieConsultation = $categorieConsultation;
    }

    public function getIdCodeCpv1()
    {
        return $this->idCodeCpv1;
    }

    public function setIdCodeCpv1($idCodeCpv1)
    {
        $this->idCodeCpv1 = $idCodeCpv1;
    }

    public function getIdCodeCpv2()
    {
        return $this->idCodeCpv2;
    }

    public function setIdCodeCpv2($idCodeCpv2)
    {
        $this->idCodeCpv2 = $idCodeCpv2;
    }

    public function getIdCodeCpv3()
    {
        return $this->idCodeCpv3;
    }

    public function setIdCodeCpv3($idCodeCpv3)
    {
        $this->idCodeCpv3 = $idCodeCpv3;
    }

    public function getIdCodeCpv4()
    {
        return $this->idCodeCpv4;
    }

    public function setIdCodeCpv4($idCodeCpv4)
    {
        $this->idCodeCpv4 = $idCodeCpv4;
    }

    public function getDateFinStart()
    {
        return $this->dateFinStart;
    }

    public function setDateFinStart($dateFinStart)
    {
        $this->dateFinStart = $dateFinStart;
    }

    public function getDateFinEnd()
    {
        return $this->dateFinEnd;
    }

    public function setDateFinEnd($dateFinEnd)
    {
        $this->dateFinEnd = $dateFinEnd;
    }

    public function getIntituleConsultation()
    {
        return $this->intituleConsultation;
    }

    public function setIntituleConsultation($intituleConsultation)
    {
        $this->intituleConsultation = $intituleConsultation;
    }

    public function getObjetConsultation()
    {
        return $this->objetConsultation;
    }

    public function setObjetConsultation($objetConsultation)
    {
        $this->objetConsultation = $objetConsultation;
    }

    public function getCommentaireInterne()
    {
        return $this->commentaireInterne;
    }

    public function setCommentaireInterne($commentaireInterne)
    {
        $this->commentaireInterne = $commentaireInterne;
    }

    public function getIntituleLot()
    {
        return $this->intituleLot;
    }

    public function setIntituleLot($intituleLot)
    {
        $this->intituleLot = $intituleLot;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function getTypeSearch()
    {
        return $this->typeSearch;
    }

    public function setTypeSearch($typeSearch)
    {
        $this->typeSearch = $typeSearch;
    }

    public function getKeyWordRechercheRapide()
    {
        return $this->keyWordRechercheRapide;
    }

    public function setKeyWordRechercheRapide($keyWordRechercheRapide)
    {
        $this->keyWordRechercheRapide = $keyWordRechercheRapide;
    }

    public function getKeyWordAdvancedSearch()
    {
        return $this->keyWordAdvancedSearch;
    }

    public function setKeyWordAdvancedSearch($keyWordAdvancedSearch)
    {
        $this->keyWordAdvancedSearch = $keyWordAdvancedSearch;
    }

    public function getEtatConsultation()
    {
        return $this->etatConsultation;
    }

    public function setEtatConsultation($etatConsultation)
    {
        $this->etatConsultation = $etatConsultation;
    }

    public function getDefaultSortByElement()
    {
        return $this->defaultSortByElement;
    }

    public function setDefaultSortByElement($defaultSortByElement)
    {
        $this->defaultSortByElement = $defaultSortByElement;
    }

    public function getSortByElement()
    {
        return $this->sortByElement;
    }

    public function setSortByElement($sortByElement)
    {
        $this->sortByElement = $sortByElement;
    }

    public function getSensOrderBy()
    {
        return $this->sensOrderBy;
    }

    public function setSensOrderBy($sensOrderBy)
    {
        $this->sensOrderBy = $sensOrderBy;
    }

    public function getOffset()
    {
        return $this->offset;
    }

    public function setOffset($offset)
    {
        $this->offset = $offset;
    }

    public function getLimit()
    {
        return $this->limit;
    }

    public function setLimit($limit)
    {
        $this->limit = $limit;
    }

    public function setSearchModeExact($boolean)
    {
        $this->searchModeExact = $boolean;
    }

    public function getSearchModeExact()
    {
        return $this->searchModeExact;
    }

    public function getSecondConditionOnEtatConsultation()
    {
        return $this->secondConditionOnEtatConsultation;
    }

    public function setSecondConditionOnEtatConsultation($secondConditionOnEtatConsultation)
    {
        $this->secondConditionOnEtatConsultation = $secondConditionOnEtatConsultation;
    }

    public function getLieuxexecution()
    {
        return $this->_lieuxExecution;
    }

    public function setLieuxexecution($lieuxExecution)
    {
        $this->_lieuxExecution = $lieuxExecution;
    }

    public function getConnectedAgentId()
    {
        return $this->_connectedAgentId;
    }

    public function setConnectedAgentId($connectedAgentId)
    {
        $this->_connectedAgentId = $connectedAgentId;
    }

    /**
     * @return int
     */
    public function getEss()
    {
        return $this->ess;
    }

    /**
     * @param int $ess
     */
    public function setEss($ess)
    {
        $this->ess = $ess;
    }

    /**
     * @return int
     */
    public function getSiae()
    {
        return $this->siae;
    }

    /**
     * @param int $siae
     */
    public function setSiae($siae)
    {
        $this->siae = $siae;
    }

    public function toString()
    {
        $res = '';
        if ('' !== $this->getAcronymeOrganisme()) {
            $res .= ' / Org='.$this->getAcronymeOrganisme();
        }
        if ('' !== $this->getCategorieConsultation()) {
            $res .= ' / idCategorie='.$this->getCategorieConsultation();
        }
        if ('' !== $this->getReferenceConsultation()) {
            $res .= ' / Reference utilisateur='.$this->getReferenceConsultation();
        }
        if ('' != $this->getcalledFromPortail()) {
            $res .= ' / call from ='.$this->getcalledFromPortail();
        }
        if ('' != $this->getIdCodeCpv1()) {
            $res .= ' /cpv ='.$this->getIdCodeCpv1();
        }
        if ('' != $this->getLieuxexecution()) {
            $res .= ' /Lieux execution ='.$this->getLieuxexecution();
        }
        if ('' != $this->getIdTypeProcedure()) {
            $res .= ' /Id Type Procedure ='.$this->getIdTypeProcedure();
        }
        if ('' != $this->getIdTypeAvis()) {
            $res .= ' /Id Type Avis ='.$this->getIdTypeAvis();
        }
        if ('' != $this->getTypeAcces()) {
            $res .= ' /Type Procedure ='.$this->getTypeAcces();
        }
        if ('' != $this->getCodeAccesProcedureRestreinte()) {
            $res .= ' /Code Acces Procedure Restreinte ='.$this->getCodeAccesProcedureRestreinte();
        }

        return $res;
    }

    public function getcalledFromPortail()
    {
        return $this->_calledFromPortail;
    }

    public function setcalledFromPortail($calledFromPortail)
    {
        $this->_calledFromPortail = $calledFromPortail;
    }

    public function getPeriodicite()
    {
        return $this->_periodicite;
    }

    public function setPeriodicite($periodicite)
    {
        $this->_periodicite = $periodicite;
    }

    public function getOrgActive()
    {
        return $this->_orgActive;
    }

    public function setOrgActive($value)
    {
        $this->_orgActive = $value;
    }

    public function setPublicationEurope($value)
    {
        $this->_publicationEurope = $value;
    }

    public function getPublicationEurope()
    {
        return $this->_publicationEurope;
    }

    public function setEtatPublicationToBePublished($bool)
    {
        $this->_etatPublicationToBePublished = $bool;
    }

    public function getEtatPublicationToBePublished()
    {
        return $this->_etatPublicationToBePublished;
    }

    public function setEtatPublicationPublished($bool)
    {
        $this->_etatPublicationPublished = $bool;
    }

    public function getEtatPublicationPublished()
    {
        return $this->_etatPublicationPublished;
    }

    public function setEtatPublicationToBeUpdated($bool)
    {
        $this->_etatPublicationToBeUpdated = $bool;
    }

    public function getEtatPublicationToBeUpdated()
    {
        return $this->_etatPublicationToBeUpdated;
    }

    public function setEtatPublicationUpdated($bool)
    {
        $this->_etatPublicationUpdated = $bool;
    }

    public function getEtatPublicationUpdated()
    {
        return $this->_etatPublicationUpdated;
    }

    public function getAlertesFromYesterday()
    {
        return $this->_alertesFromYesterday;
    }

    public function setAlertesFromYesterday($bool)
    {
        $this->_alertesFromYesterday = $bool;
    }

    public function getCalledFromHelios()
    {
        return $this->_calledFromElios;
    }

    public function setCalledFromHelios($bool)
    {
        $this->_calledFromElios = $bool;
    }

    /**
     * @deprecated use getCalledFromHelios
     */
    public function getCalledFromElios()
    {
        return $this->_calledFromElios;
    }

    /**
     * @deprecated use getCalledFromHelios
     */
    public function setCalledFromElios($bool)
    {
        $this->_calledFromElios = $bool;
    }

    public function getSansCritereActive()
    {
        return $this->_sansCritereActive;
    }

    public function setSansCritereActive($bool)
    {
        $this->_sansCritereActive = $bool;
    }

    public function getConsultationAValiderSeulement()
    {
        return $this->_consultationAValiderSeulement;
    }

    public function setConsultationAValiderSeulement($value)
    {
        $this->_consultationAValiderSeulement = $value;
    }

    public function getConsultationAApprouverSeulement()
    {
        return $this->_consultationAApprouverSeulement;
    }

    public function setConsultationAApprouverSeulement($value)
    {
        $this->_consultationAApprouverSeulement = $value;
    }

    /**
     * A true pour banir les consultations visible temporairement pour validation simple, finale ou interm.
     */
    public function getNoTemporaryCOnsultation()
    {
        return $this->_noTemporaryCOnsultation;
    }

    public function setNoTemporaryCOnsultation($value)
    {
        $this->_noTemporaryCOnsultation = $value;
    }

    public function getEnLigneDepuis()
    {
        return $this->_enLigneDepuis;
    }

    public function setEnLigneDepuis($value)
    {
        $this->_enLigneDepuis = $value;
    }

    public function getEnLigneJusquau()
    {
        return $this->_enLigneJusquau;
    }

    public function setEnLigneJusquau($value)
    {
        $this->_enLigneJusquau = $value;
    }

    public function setOrgDenomination($value)
    {
        $this->_orgDenomination = $value;
    }

    public function getOrgDenomination()
    {
        return $this->_orgDenomination;
    }

    public function getTypeOrganisme()
    {
        return $this->_typeOrganisme;
    }

    public function setTypeOrganisme($value)
    {
        $this->_typeOrganisme = $value;
    }

    public function getDomaineActivite()
    {
        return $this->_domaineActivite;
    }

    public function setDomaineActivite($value)
    {
        $this->_domaineActivite = $value;
    }

    public function getQualification()
    {
        return $this->_qualification;
    }

    public function setQualification($value)
    {
        $this->_qualification = $value;
    }

    public function getAgrements()
    {
        return $this->_agrements;
    }

    public function setAgrements($value)
    {
        $this->_agrements = $value;
    }

    public function getFromArchive()
    {
        return $this->_fromArchive;
    }

    public function setFromArchive($value)
    {
        $this->_fromArchive = $value;
    }

    public function getCodesNuts()
    {
        return $this->_codesNuts;
    }

    public function setCodesNuts($value)
    {
        $this->_codesNuts = $value;
    }

    public function setIdsService($value)
    {
        $this->_idsService = $value;
    }

    public function getIdsService()
    {
        return $this->_idsService;
    }

    public function setReferentielVo($value)
    {
        $this->referentielVo = $value;
    }

    public function getReferentielVo()
    {
        return $this->referentielVo;
    }

    public function setIdInscrit($value)
    {
        $this->_idInscrit = $value;
    }

    public function getIdInscrit()
    {
        return $this->_idInscrit;
    }

    public function setIdEntreprise($value)
    {
        $this->_idEntreprise = $value;
    }

    public function getIdEntreprise()
    {
        return $this->_idEntreprise;
    }

    public function setForPanierEntreprise($value)
    {
        $this->_forPanierEntreprise = $value;
    }

    public function getForPanierEntreprise()
    {
        return $this->_forPanierEntreprise;
    }

    public function setAvecRetrait($value)
    {
        $this->_avecRetrait = $value;
    }

    public function getAvecRetrait()
    {
        return $this->_avecRetrait;
    }

    public function setAvecQuestion($value)
    {
        $this->_avecQuestion = $value;
    }

    public function getAvecQuestion()
    {
        return $this->_avecQuestion;
    }

    public function setAvecDepot($value)
    {
        $this->_avecDepot = $value;
    }

    public function getAvecDepot()
    {
        return $this->_avecDepot;
    }

    public function setAvecEchange($value)
    {
        $this->_avecEchange = $value;
    }

    public function getAvecEchange()
    {
        return $this->_avecEchange;
    }

    public function getDateTimeFin()
    {
        return $this->dateTimeFin;
    }

    public function setDateTimeFin($dateTimeFin)
    {
        $this->dateTimeFin = $dateTimeFin;
    }

    public function getAvecConsClotureesSansPoursuivreAffichage()
    {
        return $this->_avecConsClotureesSansPoursuivreAffichage;
    }

    public function setAvecConsClotureesSansPoursuivreAffichage($value)
    {
        $this->_avecConsClotureesSansPoursuivreAffichage = $value;
    }

    public function getExclureConsultationExterne()
    {
        return $this->exclureConsultationExterne;
    }

    public function setExclureConsultationExterne($value)
    {
        $this->exclureConsultationExterne = $value;
    }

    public function getIdAlerteConsultation()
    {
        return $this->idAlerteConsultation;
    }

    public function setIdAlerteConsultation($value)
    {
        $this->idAlerteConsultation = $value;
    }

    public function getAlerteConsultationCloturee()
    {
        return $this->alerteConsultationCloturee;
    }

    public function setAlerteConsultationCloturee($value)
    {
        $this->alerteConsultationCloturee = $value;
    }

    public function getAfficherDoublon()
    {
        return $this->afficherDoublon;
    }

    public function setAfficherDoublon($value)
    {
        $this->afficherDoublon = $value;
    }

    public function setSansRetrait($value)
    {
        $this->_sansRetrait = $value;
    }

    public function getSansRetrait()
    {
        return $this->_sansRetrait;
    }

    public function setSansQuestion($value)
    {
        $this->_sansQuestion = $value;
    }

    public function getSansQuestion()
    {
        return $this->_sansQuestion;
    }

    public function setSansDepot($value)
    {
        $this->_sansDepot = $value;
    }

    public function getSansDepot()
    {
        return $this->_sansDepot;
    }

    public function setSansEchange($value)
    {
        $this->_sansEchange = $value;
    }

    public function getSansEchange()
    {
        return $this->_sansEchange;
    }

    public function setDenominationAdapte($value)
    {
        $this->denominationAdapte = $value;
    }

    public function getDenominationAdapte()
    {
        return $this->denominationAdapte;
    }

    public function getClauseSociale()
    {
        return $this->clauseSociale;
    }

    public function setClauseSociale($clauseSociale)
    {
        $this->clauseSociale = $clauseSociale;
    }

    public function getAtelierProtege()
    {
        return $this->atelierProtege;
    }

    public function setAtelierProtege($atelierProtege)
    {
        $this->atelierProtege = $atelierProtege;
    }

    public function getClauseEnv()
    {
        return $this->clauseEnv;
    }

    public function setClauseEnv($clauseEnv)
    {
        $this->clauseEnv = $clauseEnv;
    }

    public function getIdOperation()
    {
        return $this->idOperation;
    }

    public function setIdOperation($value)
    {
        $this->idOperation = $value;
    }

    public function getMps()
    {
        return $this->Mps;
    }

    public function setMps($mps)
    {
        $this->Mps = $mps;
    }

    /**
     * Permet de recuperer la valeur de l'attribut $inclureDescendance.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2014
     */
    public function getInclureDescendance()
    {
        return $this->inclureDescendance;
    }

    /**
     * Permet d'affecter la valeur de l'attribut $inclureDescendance.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @param string $value: valeur a affecter a l'attribut
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2014
     */
    public function setInclureDescendance($value)
    {
        $this->inclureDescendance = $value;
    }

    /**
     * Permet de recuperer la valeur de l'attribut $rechercheAvanceeAutoCompletion.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2014
     */
    public function getRechercheAvanceeAutoCompletion()
    {
        return $this->rechercheAvanceeAutoCompletion;
    }

    /**
     * Permet d'affecter la valeur de l'attribut $rechercheAvanceeAutoCompletion.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @param string $value: valeur a affecter a l'attribut
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2014
     */
    public function setRechercheAvanceeAutoCompletion($value)
    {
        $this->rechercheAvanceeAutoCompletion = $value;
    }

    /**
     * Permet de recuperer la valeur de l'attribut $consultationAArchiver.
     *
     * @author oubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getConsultationAArchiver()
    {
        return $this->consultationAArchiver;
    }

    /**
     * Permet d'affecter la valeur de l'attribut $consultationAArchiver.
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @param string $value: valeur a affecter a l'attribut
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function setConsultationAArchiver($value)
    {
        $this->consultationAArchiver = $value;
    }

    public function getReferenceConsultationRestreinte()
    {
        return $this->referenceConsultationRestreinte;
    }

    public function setReferenceConsultationRestreinte($referenceConsultationRestreinte)
    {
        $this->referenceConsultationRestreinte = $referenceConsultationRestreinte;
    }

    /**
     * @return bool
     */
    public function getVisionRma()
    {
        return $this->visionRma;
    }

    /**
     * @param bool $visionRma
     */
    public function setVisionRma($visionRma)
    {
        $this->visionRma = $visionRma;
    }

    /**
     * @return string
     */
    public function getEmailInscrit()
    {
        return $this->emailInscrit;
    }

    /**
     * @param string $emailInscrit
     */
    public function setEmailInscrit($emailInscrit)
    {
        $this->emailInscrit = $emailInscrit;
    }

    /**
     * @param bool $consultationExterne
     */
    public function setConsultationExterne(bool $consultationExterne): void
    {
        $this->consultationExterne = $consultationExterne;
    }

    /**
     * @param bool $consultationExterne
     */
    public function getConsultationExterne(): bool
    {
        return $this->consultationExterne;
    }

    /**
     * @param null $consultationExterneFilter
     */
    public function setConsultationExterneFilter($consultationExterneFilter): void
    {
        $this->consultationExterneFilter = $consultationExterneFilter;
    }

    /**
     * @return null
     */
    public function getConsultationExterneFilter()
    {
        return $this->consultationExterneFilter;
    }
}
