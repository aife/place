<?php

namespace Application\Service\Atexo\Consultation;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonProcedureEquivalencePeer;
use Application\Propel\Mpe\CommonTProcedureEquivalenceDume;
use Application\Propel\Mpe\CommonTProcedureEquivalenceDumeQuery;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use PDO;

/**
 * Classe de Procedure équivalence.
 *
 * @author Khalid BENAMAR <khalid.benamar@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Consultation_ProcedureEquivalence
{
    public static function retrieveProcedureEquivalence($org = null)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        if (null == $org) {
            $c->add(CommonProcedureEquivalencePeer::ORGANISME, Atexo_CurrentUser::getCurrentOrganism());
        } else {
            $c->add(CommonProcedureEquivalencePeer::ORGANISME, $org);
        }
        $rowsProcEquivalence = CommonProcedureEquivalencePeer::doSelect($c, $connexionCom);

        return $rowsProcEquivalence;
    }

    public static function retrieveProcedureEquivalenceById($idProcedure, $org = false)
    {
        if ($idProcedure > 0) {
            $c = new Criteria();
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
            $c->add(CommonProcedureEquivalencePeer::ID_TYPE_PROCEDURE, $idProcedure);
            $c->add(CommonProcedureEquivalencePeer::ORGANISME, (false == $org ? Atexo_CurrentUser::getCurrentOrganism() : $org));
            $procEquivalence = CommonProcedureEquivalencePeer::doSelectOne($c, $connexionCom);

            return $procEquivalence;
        } else {
            return false;
        }
    }

    public function saveProcedureEquivalence($procedureEquivalenceObject, $connexion)
    {
        $procedureEquivalenceObject->save($connexion);
    }

    public function displayElement($parentElement, $element, $values, $isChecked = true)
    {
        $panel = 'panel'.$element;

        if (!strcmp($values, '0')) {
            $parentElement->$panel->setStyle('display:');

            if ($isChecked) {
                $parentElement->$element->SetChecked(false);
                $parentElement->$element->SetEnabled(false);
            }
        } elseif (!strcmp($values, '1')) {
            $parentElement->$panel->setStyle('display:');
            if ($isChecked) {
                $parentElement->$element->SetChecked(true);
                $parentElement->$element->SetEnabled(false);
            }
        } elseif (!strcmp($values, '+1')) {
            $parentElement->$panel->setStyle('display:');
            if ($isChecked) {
                $parentElement->$element->SetChecked(true);
                $parentElement->$element->SetEnabled(true);
            }
        } elseif (!strcmp($values, '+0')) {
            $parentElement->$panel->setStyle('display:');
            if ($isChecked) {
                $parentElement->$element->SetChecked(false);
                $parentElement->$element->SetEnabled(true);
            }
        } elseif (!strcmp($values, '-1')) {
            $parentElement->$panel->setStyle('display:none');
            if ($isChecked) {
                $parentElement->$element->SetChecked(true);
            }
        } elseif (!strcmp($values, '-0')) {
            if ($isChecked) {
                $parentElement->$element->SetChecked(false);
            }
            $parentElement->$panel->setStyle('display:none');
        }
    }

    public function getProcedureAndParametrageDume($idTypeProcedure, $organisme)
    {
        $params = [];
        $sql = <<<QUERY
SELECT
t_type_procedure_dume.id,
t_type_procedure_dume.libelle,
t_procedure_equivalence_dume.afficher,
t_procedure_equivalence_dume.figer,
t_procedure_equivalence_dume.selectionner
FROM
	t_type_procedure_dume
		LEFT JOIN
		t_procedure_equivalence_dume
		on t_type_procedure_dume.id = t_procedure_equivalence_dume.id_type_procedure_dume
		AND
			t_procedure_equivalence_dume.organisme = :organisme
		AND
			t_procedure_equivalence_dume.id_type_procedure = :idTypeProcedure
WHERE
	t_type_procedure_dume.active = '1'
QUERY;

        $params[':organisme'] = $organisme;
        $params[':idTypeProcedure'] = $idTypeProcedure;

        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $statement = $connexion->prepare($sql);
        $statement->execute($params);

        return $statement->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Permet de retourner le id du type procedure dume par defaut selectionne.
     *
     * @param int    $idTypeProcedure
     * @param string $organisme
     *
     * @return int
     *             author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1.0
     *
     * @since 2017-esr
     *
     * @copyright Atexo 2018
     */
    public static function getDefaultSelectedTypeProcedureDume($idTypeProcedure, $organisme)
    {
        $tpdQuery = new CommonTProcedureEquivalenceDumeQuery();
        $tpd = $tpdQuery->filterByIdTypeProcedure($idTypeProcedure)
                        ->filterByOrganisme($organisme)
                        ->filterBySelectionner(1)
                        ->findOne(Propel::getConnection(Atexo_Config::getParameter('COMMON_DB')));
        $id = 0;

        if ($tpd instanceof CommonTProcedureEquivalenceDume) {
            $id = $tpd->getIdTypeProcedureDume();
        }

        return $id;
    }
}
