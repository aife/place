<?php

namespace Application\Service\Atexo\Consultation;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationPeer;
use Application\Propel\Mpe\CommonInvitationConsultationTransversePeer;
use Application\Propel\Mpe\CommonInvitationConsultationTransverseQuery;
use Application\Propel\Mpe\Om\BaseCommonConsultationPeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Db;
use PDO;

/**
 * Classe de manipulation des destinataire.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Consultation_InvitationConsTransverse
{
    public function retreiveInvitationConsultationTransverse($consultationId, $lot, $org = null, $idContrat = null)
    {
        $connexionCommon = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonInvitationConsultationTransversePeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonInvitationConsultationTransversePeer::LOT, $lot);
        if ($org) {
            $c->add(CommonInvitationConsultationTransversePeer::ORGANISME_EMETTEUR, $org);
        }
        if ($idContrat) {
            $c->add(CommonInvitationConsultationTransversePeer::ID_CONTRAT_TITULAIRE, $idContrat);
        }
        $listInv = CommonInvitationConsultationTransversePeer::doSelect($c, $connexionCommon);
        if ($listInv) {
            return $listInv;
        } else {
            return false;
        }
    }

    /**
     * Permet de recupérer la liste des entités éligibles depuis le idcontrat.
     *
     * @param int    $idTypeContrat
     * @param string $org
     *
     * @return array
     *
     * @author AMAL EL BEKKAOUI <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function retreiveInvitationConsultationTransverseByIdContrat($idContrat, $org = null)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        if ($org) {
            $c->add(CommonInvitationConsultationTransversePeer::ORGANISME_EMETTEUR, $org);
        }
        $c->add(CommonInvitationConsultationTransversePeer::ID_CONTRAT_TITULAIRE, $idContrat);

        return CommonInvitationConsultationTransversePeer::doSelect($c, $connexion);
    }

    public function deleteInvitationConsultationTransverse($consultationId, $lot, $org, $idContrat = null)
    {
        //on est dans une méthode qui fait une modification de la base donc on accède à la base lecture/ecriture
        $connexionCommon = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $listInv = (new Atexo_Consultation_InvitationConsTransverse())->retreiveInvitationConsultationTransverse($consultationId, $lot, $org, $idContrat);
        if ($listInv) {
            foreach ($listInv as $oneInv) {
                CommonInvitationConsultationTransversePeer::doDelete($oneInv->getid(), $connexionCommon);
            }
        }
        //exit;
    }

    /**
     * recherche de l'objet consultation a partir
     * de l'objet invitationConsultationTransverse.
     */
    public function reterieveConsultationTransverse($orgInvite, $returnJustNumberOfElement = false)
    {
        $queryParams = [];
        $querySelect = ' Select  DISTINCT consultation.id,consultation.organisme,InvitationConsultationTransverse.organisme_emetteur, InvitationConsultationTransverse.lot as numLot';
        $querySelect .= ' FROM consultation ,InvitationConsultationTransverse  ';
        $querySelect .= ' WHERE ((consultation.id = InvitationConsultationTransverse .consultation_id) AND (consultation.organisme = InvitationConsultationTransverse .organisme_emetteur) ';
        $querySelect .= ' AND consultation.consultation_transverse=:consultationTransverse) ';
        $querySelect .= ' AND InvitationConsultationTransverse.organisme_invite=:orgInvite ';
        $queryParams[':orgInvite'] = $orgInvite;
        $queryParams[':consultationTransverse'] = Atexo_Config::getParameter('CONSULTATION_TRANSVERSE');
        /*
        foreach($queryParams as $a => $b)
            {
                $querySelect = str_replace($a, $b, $querySelect);
            }
            echo $querySelect; exit;
        */
        $statement = Atexo_Db::getLinkCommon(true)->prepare($querySelect);
        $results = $statement->execute($queryParams);
        $nombreElement = 0;
        if ($returnJustNumberOfElement) {
            while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                ++$nombreElement;
            }

            return $nombreElement;
        } else {
            $arrayConsultation = [];
            if ($results) {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
                while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                    $c = new Criteria();
                    $c->addOr(CommonConsultationPeer::ID, $row['id']);
                    $commonConsultation = CommonConsultationPeer::doSelectOne($c, $connexion);
                    if ($commonConsultation instanceof CommonConsultation) {
                        $commonConsultation->setNumLotConsultationTransverse($row['numLot']);
                    }
                    $arrayConsultation[] = $commonConsultation;
                    BaseCommonConsultationPeer::removeInstanceFromPool($commonConsultation);
                }
            }

            return $arrayConsultation;
        }
    }

    public function reterieveConsultationTransverseByRefOrg($consultationId, $org)
    {
        $connexionCommon = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonInvitationConsultationTransversePeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonInvitationConsultationTransversePeer::ORGANISME_EMETTEUR, $org);
        $listInv = CommonInvitationConsultationTransversePeer::doSelectOne($c, $connexionCommon);
        if ($listInv) {
            return $listInv;
        }
    }

    /**
     * Permet d'avoir la liste des invitations transverse.
     *
     * @param null $idContrat l'id contrat
     * @param null $connexion
     *
     * @return array CommonInvitationConsultationTransverse
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since place-2015
     *
     * @copyright Atexo 2016
     */
    public function retrieveInvitationConsultationTransverseByIdContrat($idContrat = null, $connexion = null)
    {
        $invTransverseQuery = new CommonInvitationConsultationTransverseQuery();

        return $invTransverseQuery->getInvitationConsultationTransverseByIdContrat($idContrat, $connexion);
    }
}
