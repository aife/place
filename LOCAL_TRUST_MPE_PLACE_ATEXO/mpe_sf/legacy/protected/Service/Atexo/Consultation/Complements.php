<?php

namespace Application\Service\Atexo\Consultation;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonComplementPeer;
use Application\Service\Atexo\Atexo_Config;
use AtexoCrypto\Dto\Consultation;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Consultation_Complements
{
    public function getComplement($consultationId, $org)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonComplementPeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonComplementPeer::ORGANISME, $org);
        $c->addDescendingOrderByColumn(CommonComplementPeer::ID);
        $result = CommonComplementPeer::doSelectOne($c, $connexion);
        if ($result) {
            return $result;
        }
    }

    /*
     * Récupérer les fichiers compléments d'une consultation
     */
    public function getComplements($consultationId, $org, $c = null)
    {
        $connection = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        if (!$c) {
            $c = new Criteria();
        }
        $c->add(CommonComplementPeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonComplementPeer::ORGANISME, $org);
        $results = CommonComplementPeer::doSelect($c, $connection);
        if ($results) {
            return $results;
        } else {
            return [];
        }
    }
}
