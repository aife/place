<?php

namespace Application\Service\Atexo\Consultation;

use App\Entity\ContratTitulaire;
use App\Repository\ContratTitulaireRepository;
use App\Service\ClausesService;
use Application\Library\PhpRules\PhpRules_RulesCalculator;
use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonChorusEchange;
use Application\Propel\Mpe\CommonChorusEchangePeer;
use Application\Propel\Mpe\CommonChorusFicheNavette;
use Application\Propel\Mpe\CommonChorusNomsFichiersPeer;
use Application\Propel\Mpe\CommonConfigurationOrganisme;
use Application\Propel\Mpe\CommonConfigurationOrganismeQuery;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationQuery;
use Application\Propel\Mpe\CommonDecisionLotQuery;
use Application\Propel\Mpe\CommonInvitationConsultationTransverse;
use Application\Propel\Mpe\CommonInvitationConsultationTransversePeer;
use Application\Propel\Mpe\CommonInvitePermanentContratPeer;
use Application\Propel\Mpe\CommonOffrePapier;
use Application\Propel\Mpe\CommonOffres;
use Application\Propel\Mpe\CommonServicePeer;
use Application\Propel\Mpe\CommonTConsLotContrat;
use Application\Propel\Mpe\CommonTConsLotContratQuery;
use Application\Propel\Mpe\CommonTContactContrat;
use Application\Propel\Mpe\CommonTContratMulti;
use Application\Propel\Mpe\CommonTContratMultiQuery;
use Application\Propel\Mpe\CommonTContratTitulaire;
use Application\Propel\Mpe\CommonTContratTitulairePeer;
use Application\Propel\Mpe\CommonTContratTitulaireQuery;
use Application\Propel\Mpe\CommonTDonneesConsultation;
use Application\Propel\Mpe\CommonTNumerotationAutomatique;
use Application\Propel\Mpe\CommonTNumerotationAutomatiqueQuery;
use Application\Propel\Mpe\CommonTTypeContrat;
use Application\Propel\Mpe\CommonTTypeContratQuery;
use Application\Propel\Mpe\CommonTypeContratPivotQuery;
use Application\Propel\Mpe\Entreprise;
use Application\Propel\Mpe\Om\BaseCommonTConsLotContratQuery;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Chorus\Atexo_Chorus_Echange;
use Application\Service\Atexo\Chorus\Atexo_Chorus_Util;
use Application\Service\Atexo\Config\Atexo_Config_Exception;
use Application\Service\Atexo\Contrat\Atexo_Contrat_CriteriaVo;
use Application\Service\Atexo\Contrat\Atexo_Contrat_IndicateurEnum;
use Application\Service\Atexo\GenerationFichier\Atexo_GenerationFichier_GenerationDocumentsChorus;
use AtexoPdf\PdfGeneratorClient;
use Exception;
use PDO;
use Prado\Prado;

/**
 * Gerer les contrats.
 *
 * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
 *
 * @version 1.0
 *
 * @since 4.11.0
 *
 * @copyright Atexo 2015
 */
class Atexo_Consultation_Contrat
{
    /**
     * Permet d'e retourner cons lot contrat.
     *
     * @param int    $consultationId la reference de la consultation
     * @param string $organisme      l'organisme
     * @param int    $lot            l'id de lot concerne
     *
     * @return array des CommonTConsLotContrat
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function getListeConsLotsContrat($consultationId, $organisme, $lot)
    {
        $consLotsContrat = BaseCommonTConsLotContratQuery::create()->filterByConsultationId($consultationId)
            ->filterByOrganisme($organisme)
            ->filterByLot($lot)->find();

        return $consLotsContrat ? (array) $consLotsContrat : false;
    }

    /**
     * Permet de retourner liste des contrats titulaire.
     *
     * @param int    $consultationId la reference de la consultation
     * @param string $organisme      l'organisme
     * @param int    $lot            l'id de lot concerne
     *
     * @return array des CommonTContratTitulaire
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function getListeContrats($consultationId, $organisme, $lot)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $consLotsContrat = self::getListeConsLotsContrat($consultationId, $organisme, $lot);
        $contrats = [];
        if (is_array($consLotsContrat)) {
            foreach ($consLotsContrat as $consLotContrat) {
                $contrats[] = $consLotContrat->getCommonTContratTitulaire($connexion);
            }
        }

        return $contrats;
    }

    /**
     * Permet de recuperer la liste des lots d'un contrat.
     *
     * @param string $consultationId : reference de la consultation
     * @param string $organisme      :  organisme de la consultation
     * @param string $idcontrat      : identifiant du contrat
     *
     * @return string
     *
     * @author OKO <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function getListeLotsContrat($consultationId, $organisme, $idcontrat)
    {
        $consLotsContrat = BaseCommonTConsLotContratQuery::create()->filterByConsultationId($consultationId)
            ->filterByOrganisme($organisme)
            ->filterByIdContratTitulaire($idcontrat)->find();

        $listeLots = [];

        if ($consLotsContrat instanceof PropelObjectCollection) {
            $dataConsLotsContrat = $consLotsContrat->getData();
            if (is_array($dataConsLotsContrat) && count($dataConsLotsContrat)) {
                foreach ($dataConsLotsContrat as $consLotContrat) {
                    $listeLots[] = $consLotContrat->getLot();
                }
            }
        }

        return $listeLots;
    }

    /**
     * Permet de recuperer la liste des contrats pour la consultation.
     *
     * @param string $consultationId : reference de la consultation
     * @param string $organisme      : organisme de la consultation
     *
     * @return array
     *
     * @author OKO <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function recupererListeContratsParConsultation($consultationId, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $consLotsContrat = self::getListeConsLotsContratByConsultation($consultationId, $organisme);
        $contrats = [];
        if ($consLotsContrat instanceof PropelObjectCollection) {
            foreach ($consLotsContrat as $consLotContrat) {
                $contrats[] = $consLotContrat->getCommonTContratTitulaire($connexion);
            }
        }

        return $contrats;
    }

    /**
     * Permet de recuperer la liste des consultations lots contrats par consultation.
     *
     * @param string $reference : reference de la consultation
     * @param string $organisme : organisme
     *
     * @return array|bool|mixed|PropelObjectCollection
     *
     * @author OKO <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function getListeConsLotsContratByConsultation($consultationId, $organisme)
    {
        $consLotsContrat = BaseCommonTConsLotContratQuery::create()->filterByConsultationId($consultationId)
            ->filterByOrganisme($organisme)
            ->find();

        return $consLotsContrat ?: false;
    }

    /**
     * Permet de retourner la liste des types du contrat.
     *
     * @param string optionel le titre de la liste a afficher en premier
     *
     * @return array des liste de contrat sou format de (id_type_contrat=>libel_type_contrat,...)
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function getListeTypeContrat($titreListe = null)
    {
        $typeContrats = self::getCachedListeTypeContrat();

        $resultat = [];
        if ($typeContrats) {
            if ($titreListe) {
                $resultat[0] = Prado::localize($titreListe);
            } else {
                $resultat[0] = '---'.Prado::localize('DEFINE_TYPE_CONTRAT').'---';
            }
            foreach ($typeContrats as $typeContrat) {
                $resultat[$typeContrat->getIdTypeContrat()] = (new Atexo_Config())->toPfEncoding(Prado::Localize($typeContrat->getLibelleTypeContrat()));
            }
        }

        return $resultat;
    }

    /**
     * Permet de retourner la liste des types du contrat.
     *
     * @param string optionel le titre de la liste a afficher en premier
     *
     * @return array des liste de contrat sou format de (id_type_contrat=>libel_type_contrat,...)
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function getListeTypeContratPivot($titreListe = null)
    {
        $typesContratPivot = [];
        $typeContratQuery = new CommonTypeContratPivotQuery();
        $rowTypesContratPivot = $typeContratQuery->find();

        foreach ($rowTypesContratPivot as $typeContratPivot) {
            $typesContratPivot[$typeContratPivot->getId()] = $typeContratPivot;
        }

        $resultat = [];

        if ($typesContratPivot) {
            if ($titreListe) {
                $resultat[0] = Prado::localize($titreListe);
            } else {
                $resultat[0] = '---'.Prado::localize('DEFINE_TYPE_CONTRAT').'---';
            }

            foreach ($typesContratPivot as $typeContratPivot) {
                $resultat[$typeContratPivot->getId()] = (new Atexo_Config())->toPfEncoding(Prado::Localize($typeContratPivot->getLibelle()));
            }
        }

        return $resultat;
    }

    /**
     * Permet de retourner la liste des types du contrat caché.
     *
     * @return array des liste de contrats
     */
    public static function getCachedListeTypeContrat()
    {
        $typeContrats = Prado::getApplication()->Cache->get('cachedListeTypeContrat');

        if (!$typeContrats) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            $typeContratQuery = new CommonTTypeContratQuery();
            $typeContrats = $typeContratQuery->orderByOrdreAffichage(Criteria::ASC)->find($connexion);
            Prado::getApplication()->Cache->set('cachedListeTypeContrat', $typeContrats, Atexo_Config::getParameter('TTL_PRADO_CACHE'));
        }

        return $typeContrats;
    }

    /**
     * Permet de verifier si le type de contrat est multi.
     *
     * @param int $idTypeContrat l'id type contrat
     *
     * @return Bollean true si le type est multi si non false
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function isTypeContratMultiById($idTypeContrat)
    {
        $allContrat = self::getCachedListeTypeContrat();

        foreach ($allContrat as $item) {
            if (($item->getIdTypeContrat() == $idTypeContrat) && $item->getMulti()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Permet de verifier si le type de contrat accepte le chapeau.
     *
     * @param int $idTypeContrat l'id type contrat
     *
     * @return Bollean true si le type de contrat accepte le chapeau, false sinon
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function isTypeContratAvecChapeauById($idTypeContrat)
    {
        $allContrat = self::getCachedListeTypeContrat();

        foreach ($allContrat as $item) {
            if (($item->getIdTypeContrat() == $idTypeContrat) && $item->getAvecChapeau()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Permet dde génere le numéro de contrat selon la regle définit dans l'xml en parm.
     *
     * @param int    $idContrat             l'id de contrat
     * @param string $pathXmlNumContratLong le chemin de l'xml de generation du numero
     *
     * @return string le numero du contrat
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function genererNumeroContratTitulaire($pathXmlNumContratLong, $contrat)
    {
        $service = null;
        if ($contrat instanceof CommonTContratTitulaire || $contrat instanceof CommonTContratMulti) {
            $organisme = Atexo_Organismes::retrieveOrganismeByAcronyme($contrat->getOrganisme());
            if ($contrat->getServiceId()) {
                $service = CommonServicePeer::retrieveByPK($contrat->getServiceId());
            }
            $xml = file_get_contents($pathXmlNumContratLong);
            $rules = new PhpRules_RulesCalculator();
            $rules->setXml($xml);
            $rules->addVar('SERVICE', $service);
            $rules->addVar('CONTRAT', $contrat);
            $rules->addVar('ORGANISME', $organisme);
            $rules->addVar('T_TYPE_CONTRAT', $contrat->getTypeContrat());
            $result = $rules->getResultFromXmlRules();
            //var_export($result);exit(1);
            return self::getResultatGeneratedForContrat($result);
        }

        return '';
    }

    /**
     * Permet de formater le resultat aprés géneration.
     *
     * @param array $result le resultat de géeneration de numero
     *
     * @return string le numero du contrat
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function getResultatGeneratedForContrat($result)
    {
        return is_array($result) ? $result['resultat'] : '';
    }

    /**
     * Permet de génere le numéro autoincrement.
     *
     * @param string $organisme l'organisme
     * @param int    $service   l'id de service
     * @param string $annee     l'annee courante
     * @param string $champ     le nom du champ de recuperation du nombre
     *
     * @return string le nombre
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function getNumeroAutomatiqueContrat($organisme, $service, $annee, $champ, $longueurNumero, $connexion = null)
    {
        if (!$connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        }
        $numerotationQuery = new CommonTNumerotationAutomatiqueQuery();
        if (Atexo_Config::getParameter('NUMEROTATION_AUTOMATIQUE_CONTRAT_PAR_PLATEFORME') || 'ContratMulti' == $champ) {
            $numerotation = $numerotationQuery->getNumerotationAutomatiqueParService('', null, $annee, $connexion);
        } elseif (Atexo_Config::getParameter('NUMEROTATION_AUTOMATIQUE_CONTRAT_PAR_ORGANISME')) {
            $numerotation = $numerotationQuery->getNumerotationAutomatiqueParService($organisme, null, $annee, $connexion);
        } elseif (Atexo_Config::getParameter('NUMEROTATION_AUTOMATIQUE_CONTRAT_PAR_SERVICE')) {
            $numerotation = $numerotationQuery->getNumerotationAutomatiqueParService($organisme, $service, $annee, $connexion);
        } else {
            $numerotation = $numerotationQuery->getNumerotationAutomatiqueParService('', null, $annee, $connexion);
        }
        if (!$numerotation instanceof CommonTNumerotationAutomatique) {
            $numerotation = new CommonTNumerotationAutomatique();
            $numerotation->setAnnee($annee);
            if ('ContratMulti' != $champ) {
                if (Atexo_Config::getParameter('NUMEROTATION_AUTOMATIQUE_CONTRAT_PAR_ORGANISME')) {
                    $numerotation->setOrganisme($organisme);
                } elseif (Atexo_Config::getParameter('NUMEROTATION_AUTOMATIQUE_CONTRAT_PAR_SERVICE')) {
                    $numerotation->setOrganisme($organisme);
                    $numerotation->setServiceId($service);
                }
            }

            $newNum = 1;
        } else {
            $getMethode = 'get'.$champ;
            $newNum = $numerotation->$getMethode() + 1;
        }
        $setMethode = 'set'.$champ;
        $newNum = Atexo_Util::validateNumeroSequanceByLenghtElement($newNum, $longueurNumero);
        $numerotation->$setMethode($newNum);
        $numerotation->save($connexion);

        return $newNum;
    }

    /**
     * Permet d'e retourner cons lot contrat.
     *
     * @param int    $idContrat id du contrat
     * @param string $organisme l'organisme
     *
     * @return array des CommonTConsLotContrat
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function getListeConsLotsContratByIdContrat($idContrat, $organisme = null)
    {
        try {
            $consLotsContrat = BaseCommonTConsLotContratQuery::create()->findByIdContratTitulaire($idContrat);

            return $consLotsContrat ? (array) $consLotsContrat : false;
        } catch (Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('app');
            $logger->error("Erreur : idContrat = $idContrat , organisme = $organisme \n\nErreur : ".$e->getMessage());
        }
    }

    /**
     * Permet de retourner un objet CommonTConsLotContrat par idContrat et idLot.
     *
     * @param int $idContrat id du contrat
     * @param int $lot       idLot
     *
     * @return CommonTConsLotContrat $consLotsContrat
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function getConsLotsContratByIdContratAndLot($idContrat, $lot)
    {
        $consLotsContrat = CommonTConsLotContratQuery::create()->filterByIdContratTitulaire($idContrat)->filterByLot($lot)->findOne();

        return $consLotsContrat ?: false;
    }

    /**
     * Permet de recuperer un type de contrat.
     *
     * @param string $codeLibelle : le code du libelle du type de contrat
     *
     * @return array : tableau contenant le contrat, avec le format (id_type_contrat => libel_type_contrat)
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function getTypeContratByCodeLibelle($codeLibelle, $connexion = null)
    {
        $typeContratQuery = new CommonTTypeContratQuery();

        return $typeContratQuery->getTypeContratByCodeLibelle($codeLibelle, $connexion);
    }

    /**
     * Permet de generer le numero court du contrat.
     *
     * @param string                   $numeroContratCourt : numero court
     * @param CommonTContratTitulaire  $contratTitulaire   : contrat titulaire
     * @param CommonTContratMulti|null $contratMulti       : contrat multi
     *
     * @return string|null : le numero long
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function genererNumerotationCourtContrat($contratTitulaire, $connexion = null)
    {
        $numeroContratCourt = null;
        if ($contratTitulaire instanceof CommonTContratTitulaire || $contratTitulaire instanceof CommonTContratMulti) {
            $annee = (new Atexo_Consultation_Contrat())->getAnnePourNumerotation($contratTitulaire);
            $numeroSequence = (new Atexo_Consultation_Contrat())->getNumeroAutomatiqueContrat($contratTitulaire->getOrganisme(), $contratTitulaire->getServiceId(), $annee, 'ContratTitulaire', Atexo_Config::getParameter('LONGUEUR_NUMEROTATION_AUTOMATIQUE_NUMERO_COURT_CONTRAT'), $connexion);
            $pathXmlNumContratCourt = Atexo_Config::getParameter('PATH_XML_NUMEROTATION_COURT_CONTRAT');
            $numeroContratCourt = (new Atexo_Consultation_Contrat())->genererNumeroContratTitulaire($pathXmlNumContratCourt, $contratTitulaire).$numeroSequence;
        }

        return $numeroContratCourt;
    }

    /**
     * Permet de generer le numero long du contrat.
     *
     * @param string                   $numeroContratLong : numero long
     * @param CommonTContratTitulaire  $contratTitulaire  : contrat titulaire
     * @param CommonTContratMulti|null $contratMulti      : contrat multi
     *
     * @return string|null : le numero long
     * @return void
     *
     * @throws Atexo_Config_Exception
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function genererNumerotationLongContrat($contratTitulaire, $connexion = null)
    {
        $numeroContratLong = null;
        if ($contratTitulaire instanceof CommonTContratTitulaire || $contratTitulaire instanceof CommonTContratMulti) {
            $annee = (new Atexo_Consultation_Contrat())->getAnnePourNumerotation($contratTitulaire);
            $numeroSequence = (new Atexo_Consultation_Contrat())->getNumeroAutomatiqueContrat($contratTitulaire->getOrganisme(), $contratTitulaire->getServiceId(), $annee, 'ContratMulti', Atexo_Config::getParameter('LONGUEUR_NUMEROTATION_AUTOMATIQUE_NUMERO_LONG_CONTRAT'), $connexion);
            $pathXmlNumContratLong = Atexo_Config::getParameter('PATH_XML_NUMEROTATION_LONG_CONTRAT');
            $numeroContratLong = (new Atexo_Consultation_Contrat())->genererNumeroContratTitulaire($pathXmlNumContratLong, $contratTitulaire).$numeroSequence;
        }

        return $numeroContratLong;
    }

    /**
     * Permet de verifier si un contrat est multi attributaire.
     *
     * @param string $idContrat :  identifiant du contrat
     *
     * @return bool : true si contrat multi attributaire, false sinon
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function isContratMultiAttributaireByIdContrat($idContrat)
    {
        $contrat = self::getContratTitulaireById($idContrat);

        return $contrat instanceof CommonTContratTitulaire && $contrat->getIdContratMulti();
    }

    /**
     * Permet de recuperer un objet Contrat titulaire.
     *
     * @param string $idContrat :  identifiant technique du contrat titulaire
     *
     * @return CommonTContratTitulaire|CommonTContratTitulaire[]|mixed
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function getContratTitulaireById($idContrat)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $commonTContratTitulaireQuery = new CommonTContratTitulaireQuery(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $contrat = $commonTContratTitulaireQuery->getTContratTitulaireById($idContrat, $connexion);

        return ($contrat instanceof CommonTContratTitulaire) ? $contrat : false;
    }

    /**
     * Permet de verifier si le retour chorus est deja commande.
     *
     * @param CommonTContratTitulaire $contrat : contrat
     *
     * @return bool: true si le retour chorus est commandé, false sinon
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function isRetourChorusCommande($contrat)
    {
        if ($contrat instanceof CommonTContratTitulaire) {
            return $contrat->isStatusEjCommande();
        }

        return false;
    }

    /**
     * Permet de verifier si le retour chorus est deja integre.
     *
     * @param CommonTContratTitulaire $contrat : contrat
     *
     * @return bool: true si le retour chorus est integré, false sinon
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 2.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function isRetourChorusIntegre($contrat)
    {
        return (new Atexo_Chorus_Echange())->isRetourChorusIntegreByIdContrat($contrat->getIdContratTitulaire(), $contrat->getOrganisme());
    }

    /**
     * Permet de distinguer entre les differents types de marches lors de la numerotation du contrat
     *     Les retours:
     *         "cas1" => marches non accordes a Chorus
     *         "Cas2" => marches raccordes a Chorus, en mode commande
     *         "Cas3" => marches raccordes a Chorus, en mode integre.
     *
     * @param CommonTContratTitulaire $contrat : objet contrat
     *
     * @return string|null : le cas
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 2.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function getCasTypesMarchesPourNumerotation($contrat)
    {
        if ((new Atexo_Module())->isChorusAccessActivate(Atexo_CurrentUser::getIdServiceAgentConnected())) {
            if ($contrat instanceof CommonTContratTitulaire) {
                $typeContrat = $contrat->getTypeContrat();
                if ($typeContrat instanceof CommonTTypeContrat && $typeContrat->getModeEchangeChorus() == Atexo_Config::getParameter('TYPE_CONTRAT_MODE_ECHANGE_CHORUS_COMMANDE')) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Permet de retourner le libelle d'un type de contrat selon son id.
     *
     * @param int $idTypeContrat l'id de type de contrat
     *
     * @return string le libelle de contrat
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function getLibelleTypeContratById($idTypeContrat, $msgLibelle = true)
    {
        $allContrat = self::getCachedListeTypeContrat();

        foreach ($allContrat as $item) {
            if ($item->getIdTypeContrat() == $idTypeContrat) {
                $libelleContrat = $item->getLibelleTypeContrat();
                if ($msgLibelle) {
                    return Prado::Localize($libelleContrat);
                } else {
                    return $libelleContrat;
                }
            }
        }

        return false;
    }

    /**
     * Permet de verifier si le retour chorus est deja commande ou integre.
     *
     * @param CommonTContratTitulaire $contrat : contrat
     *
     * @return bool: true si le retour chorus est commandé, false sinon
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function isRetourChorus($contrat)
    {
        if ($contrat instanceof CommonTContratTitulaire) {
            if ($contrat->getModeEchangeChorus() == Atexo_Config::getParameter('TYPE_CONTRAT_MODE_ECHANGE_CHORUS_COMMANDE')) {
                return self::isRetourChorusCommande($contrat);
            } elseif ($contrat->getModeEchangeChorus() == Atexo_Config::getParameter('TYPE_CONTRAT_MODE_ECHANGE_CHORUS_INTEGRER')) {
                return self::isRetourChorusIntegre($contrat);
            }
        }

        return false;
    }

    /**
     * Permet de verifier si le type de contrat accepte un montant.
     *
     * @param int $idTypeContrat l'id type contrat
     *
     * @return Bollean true si le type de contrat accepte un montant, false sinon
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function isTypeContratAvecMontantById($idTypeContrat)
    {
        $allContrat = self::getCachedListeTypeContrat();

        foreach ($allContrat as $item) {
            if (($item->getIdTypeContrat() == $idTypeContrat) && $item->getAvecMontant()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Permet de verifier si le type de contrat est marche subsequent.
     *
     * @param CommonTContratTitulaire $contrat : contrat
     *
     * @return bool: true si le type de contrat est marche subsequent, false sinon
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function isTypeContratMarcheSubsequent($idTypeContrat)
    {
        $allContrat = self::getCachedListeTypeContrat();

        foreach ($allContrat as $item) {
            if (($item->getIdTypeContrat() == $idTypeContrat) && ($item->getMarcheSubsequent() == Atexo_Config::getParameter('TYPE_MARCHE_SUBSEQUENT'))) {
                return true;
            }
        }

        return false;
    }

    /**
     * Permet de retourner les donnees des contrat multi.
     *
     * @param string $numero    le numero du contrat
     * @param int    $idService l'id service
     * @param string $organisme l'irganisme
     *
     * @return array CommonTContratMulti
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function getDataContratAcByNumero($numero, $idService, $organisme, $typeACSad)
    {
        $criteriaVo = new Atexo_Contrat_CriteriaVo();
        $criteriaVo->setServiceId($idService);
        $criteriaVo->setOrganisme($organisme);
        $criteriaVo->setMotsCles($numero);
        $criteriaVo->setTypeAttribution(3);
        //on l'utilise juste pour les AC et SAD
        $criteriaVo->setContratOrganismeInvite(true);
        $typeQuery = new CommonTTypeContratQuery();
        $types = $typeQuery->getTypeContratACSAD($typeACSad, Criteria::IN);
        $criteriaVo->setTypes($types);
        $tContratTitulaireTitulaireQuery = new CommonTContratTitulaireQuery();
        $tContratTitulaireTitulaire = (array) $tContratTitulaireTitulaireQuery->getContratByCriteria($criteriaVo);

        $result = [];
        if (is_array($tContratTitulaireTitulaire)) {
            $i = 0;
            foreach ($tContratTitulaireTitulaire as $contrat) {
                if ($contrat instanceof CommonTContratTitulaire) {
                    if (2 == $contrat->getContratClassKey() && !$contrat->hasContratNotifie()) {
                        continue;
                    }
                    $result[$i]['id'] = $contrat->getIdContratTitulaire();
                    $result[$i]['numero'] = $contrat->getNumeroContrat();
                    $result[$i]['objet'] = $contrat->getObjetContrat();
                    ++$i;
                }
            }
        }

        return $result;
    }

    /**
     * Permet de retourner le contrat multi by id.
     *
     * @param int $id l'id du contrat
     *
     * @return CommonTContratMulti
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function getContratMultiById($id)
    {
        $contratQuery = new CommonTContratMultiQuery();
        $contrat = $contratQuery->getContratMultiByIdContrat($id);
        if ($contrat instanceof CommonTContratMulti) {
            return $contrat;
        }

        return false;
    }

    /**
     * Permet de retourner les contrat titulaire d'un contrat multi by id.
     *
     * @param $id l'id du contrat multi
     *
     * @return array CommonTContratTitulaire
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function getContratTitulaireByIdContratMulti($id)
    {
        $contratQuery = new CommonTContratTitulaireQuery();
        $contrats = (array) $contratQuery->getContratByIdContratMulti($id);

        return $contrats;
    }

    /**
     * Permet de retourner les contrat titulaire d'un contrat multi by id.
     *
     * @param $id l'id du contrat multi
     *
     * @return array CommonTContratTitulaire
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function getNumeroACForConsultation($id)
    {
        $contratQuery = new CommonTContratTitulaireQuery();
        $contrat = $contratQuery->getTContratTitulaireById($id);
        if ($contrat instanceof CommonTContratTitulaire) {
            return $contrat->getNumeroContrat().' - '.$contrat->getObjetContrat();
        }

        return '';
    }

    /**
     * Permet de verifier si le type de contrat accepte un montant max.
     *
     * @param int $idTypeContrat l'id type contrat
     *
     * @return Bollean true si le type de contrat accepte un montant max, false sinon
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function isTypeContratAvecMontantMaxById($idTypeContrat)
    {
        $allContrat = self::getCachedListeTypeContrat();

        foreach ($allContrat as $item) {
            if (($item->getIdTypeContrat() == $idTypeContrat) && $item->getAvecMontantMax()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Permet de verifier si le type de contrat accepte un article 133.
     *
     * @param int $idTypeContrat l'id type contrat
     *
     * @return Bollean true si le type de contrat accepte un article 133, false sinon
     *
     * @author AMAL EL BEKKAOUI <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function isTypeContratArticle133ById($idTypeContrat)
    {
        $allContrat = self::getCachedListeTypeContrat();

        foreach ($allContrat as $item) {
            if (($item->getIdTypeContrat() == $idTypeContrat) && $item->getArticle133()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Permet de tester si le retour chorus est en mode integre.
     *
     * @param string $idContrat : identifiant technique du contrat
     *
     * @return bool
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function isRetourChorusModeIntegre($idContrat)
    {
        $contrat = (new Atexo_Consultation_Contrat())->getContratTitulaireById($idContrat);
        if (($contrat instanceof CommonTContratTitulaire) && $contrat->getModeEchangeChorus() == Atexo_Config::getParameter('TYPE_CONTRAT_MODE_ECHANGE_CHORUS_INTEGRER')) {
            return true;
        }

        return false;
    }

    /**
     * Permet de tester si le retour chorus est en mode commande.
     *
     * @param string $idContrat : identifiant technique du contrat
     *
     * @return bool
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function isRetourChorusModeCommande($idContrat)
    {
        $contrat = (new Atexo_Consultation_Contrat())->getContratTitulaireById($idContrat);
        if (($contrat instanceof CommonTContratTitulaire) && $contrat->getModeEchangeChorus() == Atexo_Config::getParameter('TYPE_CONTRAT_MODE_ECHANGE_CHORUS_COMMANDE')) {
            return true;
        }

        return false;
    }

    /**
     * Permet de verifier qu'un type de contrat est accord cadre.
     *
     * @param string $idTypeContrat : identifiant du type de contrat
     *
     * @return bool : true si type de contrat est accord cadre, false sinon
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public static function isTypeContratAccordCadre($idTypeContrat)
    {
        $allContrat = self::getCachedListeTypeContrat();

        foreach ($allContrat as $item) {
            if (($item->getIdTypeContrat() == $idTypeContrat) && ($item->getAccordCadreSad() == Atexo_Config::getParameter('TYPE_CONTRAT_ACCORD_CADRE'))) {
                return true;
            }
        }

        return false;
    }

    /**
     * Permet de verifier qu'un type de contrat est sad.
     *
     * @param string $idTypeContrat : identifiant du type de contrat
     *
     * @return bool : true si type de contrat est sad, false sinon
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function isTypeContratSad($idTypeContrat)
    {
        $allContrat = self::getCachedListeTypeContrat();

        foreach ($allContrat as $item) {
            if (($item->getIdTypeContrat() == $idTypeContrat) && ($item->getAccordCadreSad() == Atexo_Config::getParameter('TYPE_CONTRAT_SAD'))) {
                return true;
            }
        }

        return false;
    }

    /**
     * Permet de verifier qu'un type de contrat est marche.
     *
     * @param string $idTypeContrat : identifiant du type de contrat
     *
     * @return bool : true si type de contrat est marche, false sinon
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function isTypeContratMarche($idTypeContrat)
    {
        try {
            $allContrat = self::getCachedListeTypeContrat();

            foreach ($allContrat as $item) {
                if (($item->getIdTypeContrat() == $idTypeContrat) && (!$item->getAccordCadreSad())) {
                    return true;
                }
            }

            return false;
        } catch (Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('app');
            $logger->error("Erreur : idTypeContrat = $idTypeContrat \n\nErreur : ".$e->getMessage());
        }
    }

    public function isTypeContratAcSad(?string $codeExternExec)
    {
        try {
            $allContrat = self::getCachedListeTypeContrat();

            foreach ($allContrat as $item) {
                if (($item->getLibelleTypeContrat() == 'TYPE_CONTRAT_' . $codeExternExec) && ($item->getAccordCadreSad())) {
                    return true;
                }
            }

            return false;
        } catch (Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('app');
            $logger->error("Erreur : idTypeContrat = $codeExternExec \n\nErreur : ".$e->getMessage());
        }
        return false;
    }

    /**
     * Permet de generer le numero long d'un contrat by type.
     *
     * @param objet CommonTContratTitulaire ou CommonTContratMulti $contrat  l'objet contrat
     * @param string                                               $nomChamp le nom du champ
     *
     * @return string le numero long du contrat
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function genererNumeroLongContratByType($contrat, $nomChamp, $connexion = null)
    {
        $numeroContratLong = null;
        if ($contrat instanceof CommonTContratTitulaire || $contrat instanceof CommonTContratMulti) {
            $annee = (new Atexo_Consultation_Contrat())->getAnnePourNumerotation($contrat);
            $numeroSequence = (new Atexo_Consultation_Contrat())->getNumeroAutomatiqueContrat($contrat->getOrganisme(), $contrat->getServiceId(), $annee, $nomChamp, Atexo_Config::getParameter('LONGUEUR_NUMEROTATION_AUTOMATIQUE_NUMERO_LONG_CONTRAT'), $connexion);
            $pathXmlNumContratLong = Atexo_Config::getParameter('PATH_XML_NUMEROTATION_LONG_CONTRAT');
            $numeroContratLong = (new Atexo_Consultation_Contrat())->genererNumeroContratTitulaire($pathXmlNumContratLong, $contrat).$numeroSequence;
        }

        return $numeroContratLong;
    }

    /**
     * Permet de generer le numero court d'un contrat by type.
     *
     * @param objet CommonTContratTitulaire ou CommonTContratMulti $contrat  l'objet contrat
     * @param string                                               $nomChamp le nom du champ
     *
     * @return string le numero court du contrat
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function genererNumeroCourtContratByType($contrat, $nomChamp, $connexion = null)
    {
        $numeroContratCourt = null;
        if ($contrat instanceof CommonTContratTitulaire || $contrat instanceof CommonTContratMulti) {
            $annee = (new Atexo_Consultation_Contrat())->getAnnePourNumerotation($contrat);
            $numeroSequence = (new Atexo_Consultation_Contrat())->getNumeroAutomatiqueContrat($contrat->getOrganisme(), $contrat->getServiceId(), $annee, $nomChamp, Atexo_Config::getParameter('LONGUEUR_NUMEROTATION_AUTOMATIQUE_NUMERO_COURT_CONTRAT'), $connexion);
            $pathXmlNumContratCourt = Atexo_Config::getParameter('PATH_XML_NUMEROTATION_COURT_CONTRAT');
            $numeroContratCourt = (new Atexo_Consultation_Contrat())->genererNumeroContratTitulaire($pathXmlNumContratCourt, $contrat).$numeroSequence;
        }

        return $numeroContratCourt;
    }

    /**TODO
     * Permet de generer le numero court d'un contrat by type
     *
     * @param objet CommonTContratTitulaire ou CommonTContratMulti $contrat l'objet contrat
     * @param string $nomChamp le nom du champ
     * @return string le numero court du contrat
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     * @version 1.0
     * @since 2015-roadmap
     * @copyright Atexo 2015
     */
    public function getLienACByIdContratAndIdTitulaire($idContrat, $idTitulaire)
    {
        $lienACSAD = null;
        $criteriaVo = new Atexo_Contrat_CriteriaVo();
        $criteriaVo->setIdContratMulti($idContrat);
        $criteriaVo->setServiceId(Atexo_CurrentUser::getCurrentServiceId());
        $criteriaVo->setOrganisme(Atexo_CurrentUser::getOrganismAcronym());
        $criteriaVo->setIdTitulaire($idTitulaire);
        //on l'utilise juste pour les AC et SAD
        $criteriaVo->setContratOrganismeInvite(true);
        $criteriaVo->setFindOne(true);
        $commonTContratTitulaireQuery = new CommonTContratTitulaireQuery();
        $contrat = $commonTContratTitulaireQuery->getContratByCriteria($criteriaVo);
        if ($contrat instanceof CommonTContratTitulaire) {
            $lienACSAD = $contrat->getIdContratTitulaire();
        }

        return $lienACSAD;
    }

    /**
     * Permet de verifier si le type de contrat est marche spécifique.
     *
     * @param CommonTContratTitulaire $contrat : contrat
     *
     * @return bool: true si le type de contrat est marche spécifique, false sinon
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function isTypeContratMarcheSpecifique($idTypeContrat)
    {
        $allContrat = self::getCachedListeTypeContrat();

        foreach ($allContrat as $item) {
            if (($item->getIdTypeContrat() == $idTypeContrat) && ($item->getMarcheSubsequent() == Atexo_Config::getParameter('TYPE_MARCHE_SPECIFIQUE'))) {
                return true;
            }
        }

        return false;
    }

    /**
     * Permet de retourner l'id de type de contrat suite.
     *
     * @param int $typeAcSad type de contrat
     *
     * @return int $idTypeContratSuite l'id type de contrat suite
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function getIdTypeContratSuiteACSAD($typeAcSad)
    {
        $idTypeContratSuite = 0;
        $typeContratSuite = false;

        $allContrat = self::getCachedListeTypeContrat();

        foreach ($allContrat as $item) {
            if ($item->getIdTypeContrat() == $typeAcSad) {
                if ($item->getAccordCadreSad() == Atexo_Config::getParameter('TYPE_CONTRAT_ACCORD_CADRE')) {
                    $typeContratSuite = Atexo_Config::getParameter('TYPE_MARCHE_SUBSEQUENT');
                } elseif ($item->getAccordCadreSad() == Atexo_Config::getParameter('TYPE_CONTRAT_SAD')) {
                    $typeContratSuite = Atexo_Config::getParameter('TYPE_MARCHE_SPECIFIQUE');
                }
                if ($typeContratSuite) {
                    $typeContratQuery = new CommonTTypeContratQuery();
                    $idTypeContratSuite = $typeContratQuery->getTypeContratMarcheS($typeContratSuite);
                }
            }
        }

        return $idTypeContratSuite;
    }

    /**
     * Permet de retourner l'annee pour generer le numero de sequence.
     *
     * @param mixed CommonTContratTitulaire ou  CommonTContratMulti $contrat le contrat
     *
     * @return string $annee l'anne
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function getAnnePourNumerotation($contrat)
    {
        $annee = date('Y');
        if ($contrat instanceof CommonTContratTitulaire || $contrat instanceof CommonTContratMulti) {
            $annee = $contrat->getDateAttribution() ? Atexo_Util::getAnneeFromDate(Atexo_Util::frnDate2iso($contrat->getDateAttribution())) : ($contrat->getDatePrevueNotification() ? Atexo_Util::getAnneeFromDate(Atexo_Util::frnDate2iso($contrat->getDatePrevueNotification())) : date('Y'));
        }

        return $annee;
    }

    /*
     * Permet de retourner le fichie echange by id et type s'il existe
     */
    public function isStatusCommandeByIdContrat($idContrat, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonChorusEchangePeer::ID_DECISION, $idContrat);
        $c->add(CommonChorusEchangePeer::ORGANISME, $organisme);
        $c->add(CommonChorusEchangePeer::STATUTECHANGE, Atexo_Config::getParameter('CHORUS_STATUT_EJ_COMMANDE'));
        $fichierEchange = CommonChorusNomsFichiersPeer::doSelectOne($c, $connexion);
        if ($fichierEchange) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Permet de verifier si au moins une contrat est multi.
     *
     * @param $contrats liste des contrats
     *
     * @return bool true si un contrat est multi si non false
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2015
     */
    public function isOneContratMulti($contrats)
    {
        $isContratMulti = false;
        if (is_array($contrats) && count($contrats)) {
            foreach ($contrats as $oneContrat) {
                if (($oneContrat instanceof CommonTContratTitulaire)
                    && self::isTypeContratMultiById($oneContrat->getIdTypeContrat())) {
                    $isContratMulti = true;
                }
            }
        }

        return $isContratMulti;
    }

    public function getContratByIdOffre($idOffre, $organisme, $typeDepot, $lot)
    {
        $contratQuery = new CommonTContratTitulaireQuery();
        $contrat = $contratQuery->getContratsByOffre($idOffre, $organisme, $typeDepot, $lot);
        if ($contrat instanceof CommonTContratTitulaire) {
            return $contrat;
        }

        return false;
    }

    /**
     * Permet de verifier si au moins un echange chorus existe.
     *
     * @param string $organisme l'organisme
     * @param int    $idContrat l'id de contrat
     *
     * @return bool true si un echange existe si non false
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2015
     */
    public function echangeChorusExiste($organisme, $idContrat)
    {
        $echange = (new Atexo_Chorus_Echange())->retreiveEchangesByIdDecision($organisme, $idContrat);

        return is_array($echange) && count($echange);
    }

    /**
     * Permet de recuperer la liste des cons_lot_contrat.
     *
     * @param array  $listeLots les id des lots
     * @param int    $idContrat l'id de contrat
     * @param string $criteria  Criteria
     * @param $connexion
     *
     * @return array of object t_cons_lot_contrat
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2016
     */
    public function getConsLotContratByContrat($consultationId, $organisme, $listeLots, $idContrat, $criteria, $connexion)
    {
        $consLotContratQuery = new CommonTConsLotContratQuery();

        return $consLotContratQuery->getConLotContratByIdContrat($consultationId, $organisme, $listeLots, $idContrat, $criteria, $connexion);
    }

    /**
     * Permet de recuperer la liste des cons_lot_contrat.
     *
     * @param array  $listeLots les id des lots
     * @param int    $idContrat l'id de contrat
     * @param string $criteria  Criteria
     * @param $connexion
     *
     * @return array of object t_cons_lot_contrat
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2016
     */
    public function getContratByContratMulti($idContratMulti, $idContrat, $connexion)
    {
        $contratQuery = new CommonTContratTitulaireQuery();
        $contratQuery->filterByIdContratMulti($idContratMulti);
        $contratQuery->where('t_contrat_titulaire.id_contrat_multi IS NOT NULL');
        $contratQuery->filterByIdContratTitulaire($idContrat, Criteria::NOT_EQUAL);

        return (array) $contratQuery->find($connexion);
    }

    /**
     * Permet de recuperer la liste des decision lot.
     *
     * @param int    $reference la reference de la consultation
     * @param string $organisme l'acronyme de l'organisme
     * @param array  $lots      les id des lots
     * @param $connexion
     *
     * @return array of object DecisionLot
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2016
     */
    public function retrieveDecisionLotByReference($consultationId, $organisme, $lots, $connexion)
    {
        $decisionlotQuery = new CommonDecisionLotQuery();

        return $decisionlotQuery->getDecisionLotByRefAndLots($consultationId, $organisme, $lots, $connexion);
    }

    /**
     * Permet de recuperer la liste des decision lot.
     *
     * @param int    $consultationId la reference de la consultation
     * @param string $organisme      l'acronyme de l'organisme
     * @param array  $lots           les id des lots
     * @param $connexion
     *
     * @return array of object DecisionLot
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2016
     */
    public function retrieveOneContratMultiByLot($consultationId, $organisme, $lots, $connexion)
    {
        $contratQuery = new CommonTContratTitulaireQuery();
        $contrat = $contratQuery->getContratTitulaireByLot($consultationId, $organisme, $lots, $connexion);
        if ($contrat instanceof CommonTContratTitulaire) {
            $contratMulti = $contrat->getCommonTContratMulti();
            if ($contratMulti instanceof CommonTContratMulti) {
                return $contratMulti;
            }
        }

        return false;
    }

    /**
     * Permet d'avoir l'inrement a ajoute pour le numero long du contrat.
     *
     * @param int    $reference la reference de la consultation
     * @param string $organisme
     * @param array  $lots      les lots
     * @param null   $connexion
     *
     * @return int
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2016
     */
    public function getNextIncrementNumeroLong($consultationId, $organisme, $lots, $connexion = null)
    {
        $increment = 0;
        if (!is_array($lots)) {
            $lots = [$lots];
        }
        $contratQuery = new CommonTContratTitulaireQuery();
        $contrats = $contratQuery->getContratTitulaireByLot($consultationId, $organisme, $lots, $connexion, false);
        if (is_array($contrats) && count($contrats)) {
            foreach ($contrats as $contrat) {
                if ($contrat instanceof CommonTContratTitulaire) {
                    $suffixNumeroLong = (int) ($contrat->getNumLongOeap() ? substr($contrat->getNumLongOeap(), -2) : 0);
                    if ($suffixNumeroLong && $suffixNumeroLong >= $increment) {
                        $increment = $suffixNumeroLong;
                    }
                }
            }
        }

        return ++$increment;
    }

    /**
     * @param $row
     * @param $connexion
     * @param $logger
     *
     * @throws Exception
     * @throws PropelException
     */
    public function createContrat($row, $connexion, $logger)
    {
        $contact = new CommonTContactContrat();
        $contact->setEmail($row['email_inscrit']);
        $contact->setFax($row['fax_inscrit']);
        $contact->setIdEntreprise($row['entreprise_id']);
        $contact->setIdEtablissement($row['id_etablissement']);
        $contact->setIdInscrit($row['inscrit_id']);
        $contact->setNom($row['nom_inscrit']);
        $contact->setPrenom($row['prenom_inscrit']);

        $contratTitulaire = new CommonTContratTitulaire();
        $contratTitulaire->setIdTypeContrat($row['type_contrat']);
        $contratTitulaire->setNumEj($row['num_EJ']);
        $contratTitulaire->setOrganisme($row['organisme']);
        $contratTitulaire->setServiceId($row['service_id']);
        $contratTitulaire->setIdTitulaire($row['entreprise_id']);
        $contratTitulaire->setIdTitulaireEtab($row['id_etablissement']);
        $contratTitulaire->setIdOffre($row['id_offre']);
        $contratTitulaire->setTypeDepotReponse($row['type_enveloppe']);
        $contratTitulaire->setObjetContrat($row['objet_marche']);
        $contratTitulaire->setMontantContrat($row['montant_marche']);
        $contratTitulaire->setIdTrancheBudgetaire($row['tranche_budgetaire']);
        $contratTitulaire->setPublicationMontant($row['publication_montant']);
        $contratTitulaire->setPublicationContrat($row['publication_contrat']);
        $contratTitulaire->setStatutej($row['statutEJ']);
        $contratTitulaire->setNumeroContrat($row['numero_marche']);
        $contratTitulaire->setNumLongOeap($row['num_long_OEAP']);
        $contratTitulaire->setStatutContrat($row['statut_contrat']);
        $contratTitulaire->setCategorie($row['categorie']);
        $contratTitulaire->setDatePrevueNotification($row['date_notification']);
        $contratTitulaire->setDatePrevueFinContrat($row['date_fin_marche_previsionnel']);
        $contratTitulaire->setDateNotification($row['date_notification_reelle']);
        $contratTitulaire->setDateFinContrat($row['date_fin_marche_reelle']);
        $contratTitulaire->setDateAttribution($row['date_decision']);
        $contratTitulaire->setDateCreation($row['date_creation']);
        $contratTitulaire->setDateModification($row['date_modification']);
        $contratTitulaire->setEnvoiInterface($row['envoi_interface']);
        $contratTitulaire->setReferenceLibre($row['reference_libre']);
        $contratTitulaire->setReferenceConsultation($row['reference_libre']);
        $contratTitulaire->setHorsPassation($row['hors_passation']);

        if ($row['genereNumerotation']) {
            if (!$contratTitulaire->getNumeroContrat()) {
                $numeroCourt = self::genererNumerotationCourtContrat($contratTitulaire, $connexion);
                $logger->info('La géneration du numero court '.$numeroCourt);
                $contratTitulaire->setNumeroContrat($numeroCourt);
            }
            $numeroLong = self::genererNumerotationLongContrat($contratTitulaire, $connexion).'01';
            $logger->info('La géneration du numero long '.$numeroLong);
            $contratTitulaire->setNumLongOeap($numeroLong);
        }

        $contratTitulaire->setCommonTContactContrat($contact);

        $contratTitulaire->save($connexion);
        if (!$row['hors_passation']) {
            $consLotContrat = new CommonTConsLotContrat();
            $consLotContrat->setOrganisme($row['organisme']);
            $consLotContrat->setConsultationId($row['id']);
            $consLotContrat->setLot($row['lot']);
            $consLotContrat->setCommonTContratTitulaire($contratTitulaire);
            $consLotContrat->save($connexion);
        }

        $logger->info('CSV|'.$row['cle_table_source'].'|'.$row['organisme'].'|'.$contratTitulaire->getIdContratTitulaire()."\n");
        $logger->info('Decision : '.$row['cle_table_source'].' organisme : '.$row['organisme'].' contrat id : '.$contratTitulaire->getIdContratTitulaire()."\n");

        return $contratTitulaire;
    }

    /**
     * Permet de supprimer un contrat.
     *
     * @param int $idContrat l'id du contrat
     *
     * @return void
     *
     * @author AMAL EL BEKKAOUI <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function deleteContrat($idContrat)
    {
        $connexion = null;
        $logger = Atexo_LoggerManager::getLogger('contrat');
        $logger->info("Demande de la suppression du contrat dont l'id = ".$idContrat);
        try {
            if ($idContrat) {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                $connexion->beginTransaction();
                $contratQuery = new CommonTContratTitulaireQuery();
                $contrat = $contratQuery->getTContratTitulaireById($idContrat, $connexion);
                if ($contrat instanceof CommonTContratTitulaire) {
                    $this->deleteClauses($idContrat);
                    $contratMulti = $contrat->getCommonTContratMulti();
                    $contactContrat = $contrat->getCommonTContactContrat($connexion);
                    $consLotContrats = (array) $contrat->getCommonTConsLotContrats($connexion);
                    $consultationId = '';
                    $organisme = '';
                    $listeLots = [];
                    $logger->info('suppression TConsLotContrat : '.var_export($consLotContrats, true));
                    if (is_array($consLotContrats) && count($consLotContrats)) {
                        foreach ($consLotContrats as $oneConsLotContrats) {
                            if ($oneConsLotContrats instanceof CommonTConsLotContrat) {
                                $consultationId = $oneConsLotContrats->getConsultationId();
                                $organisme = $oneConsLotContrats->getOrganisme();
                                $listeLots[] = $oneConsLotContrats->getLot();
                                $oneConsLotContrats->delete($connexion);
                            }
                        }
                    }
                    if ($contrat->getHorsPassation()) {
                        $contratToNotDelete = (new Atexo_Consultation_Contrat())->getContratByContratMulti($contrat->getIdContratMulti(), $idContrat, $connexion);
                    } else {
                        $contratToNotDelete = (new Atexo_Consultation_Contrat())->getConsLotContratByContrat($consultationId, $organisme, $listeLots, $idContrat, Criteria::NOT_EQUAL, $connexion);
                    }
                    $logger->info('contrat à ne pas supprimer  : '.var_export($contratToNotDelete, true));
                    if (is_array($contratToNotDelete) && !count($contratToNotDelete)) {
                        if ($contratMulti instanceof CommonTContratMulti) {
                            $listeInv = (new Atexo_Consultation_InvitationConsTransverse())->retreiveInvitationConsultationTransverseByIdContrat($contratMulti->getIdContratTitulaire(), $contratMulti->getOrganisme());
                            $logger->info('suppression des entités éligibles du contrat Multi'.$contratMulti->getIdContratTitulaire().' : '.var_export($listeInv, true));
                            $donneesConsultationsCM = (array) $contratMulti->getCommonTDonneesConsultations($connexion);
                            if (is_array($donneesConsultationsCM)) {
                                $logger->info('suppression des donnees consultations du contrat Multi '.$contratMulti->getIdContratTitulaire().' : '.var_export($donneesConsultationsCM, true));
                                foreach ($donneesConsultationsCM as $donneeCM) {
                                    if ($donneeCM instanceof CommonTDonneesConsultation) {
                                        $donneeCM->delete($connexion);
                                    }
                                }
                            }
                            if (is_array($listeInv) && count($listeInv)) {
                                foreach ($listeInv as $inv) {
                                    if ($inv instanceof CommonInvitationConsultationTransverse) {
                                        $inv->delete($connexion);
                                    }
                                }
                            }
                            $logger->info('suppression contrat multi : '.$contrat->getIdContratMulti());
                            $contratMulti->delete($connexion);
                        }
                        if (!$contrat->getHorsPassation()) {
                            $decisionsLots = (new Atexo_Consultation_Contrat())->retrieveDecisionLotByReference($consultationId, $organisme, $listeLots, $connexion);
                            $logger->info('suppression decision  : '.var_export($decisionsLots, true));
                            if (is_array($decisionsLots) && count($decisionsLots)) {
                                foreach ($decisionsLots as $decisionLot) {
                                    $decisionLot->delete($connexion);
                                }
                            }
                        }
                    }
                    $donneesConsultations = (array) $contrat->getCommonTDonneesConsultations($connexion);
                    $logger->info('suppression des donnees consultations du contrat '.$contrat->getIdContratTitulaire().' : '.var_export($donneesConsultations, true));
                    if (is_array($donneesConsultations)) {
                        foreach ($donneesConsultations as $donnee) {
                            if ($donnee instanceof CommonTDonneesConsultation) {
                                $donnee->delete($connexion);
                            }
                        }
                    }
                    $logger->info('suppression contact contrat : '.$contrat->getIdContactContrat());
                    if ($contactContrat instanceof CommonTContactContrat) {
                        $contactContrat->delete($connexion);
                    }

                    $listeInvContrat = (new Atexo_Consultation_InvitationConsTransverse())->retreiveInvitationConsultationTransverseByIdContrat($contrat->getIdContratTitulaire(), $contrat->getOrganisme());
                    $logger->info('suppression des entités éligibles du contrat '.$contrat->getIdContratTitulaire().' : '.var_export($listeInvContrat, true));
                    if (is_array($listeInvContrat) && count($listeInvContrat)) {
                        foreach ($listeInvContrat as $invC) {
                            if ($invC instanceof CommonInvitationConsultationTransverse) {
                                $invC->delete($connexion);
                            }
                        }
                    }

                    //Suppression des echanges avec CHORUS
                    if (Atexo_Module::isEnabled('InterfaceChorusPmi')) {
                        Atexo_Chorus_Echange::supprimerEchangesChorusEtObjetsLiesParContrat($contrat, $connexion, $logger);
                    }

                    $logger->info('suppression contrat : '.$contrat->getIdContratTitulaire());
                    $contrat->delete($connexion);
                    $connexion->commit();
                    $logger->info('le contrat '.$idContrat.' est supprimé');
                } else {
                    $logger->info("le contrat dont l'id = ".$idContrat." n'existe pas");
                }
            }
        } catch (Exception $e) {
            $connexion->rollBack();
            $logger->error('Erreur lors de la supression du contrat '.$idContrat.' => '.$e->getMessage().' '.$e->getTraceAsString());
        }
    }

    /**
     * Permet d'avoir liste des contrats.
     *
     * @param int    $reference        la reference de la consultation
     * @param string $organisme
     * @param array  $lots             les lots
     * @param array  $typeContratACSad liste des types Contrat
     * @param null   $connexion
     *
     * @return array CommonTContratTitulaire
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since place-2015
     *
     * @copyright Atexo 2016
     */
    public function getContratByRefConsEtLot($consultationId, $organisme, $lots, $typesContratACSad, $conenxion)
    {
        $contratsQuery = new CommonTContratTitulaireQuery();

        return $contratsQuery->getContratTitulaireByLot($consultationId, $organisme, $lots, $conenxion, false, $typesContratACSad);
    }

    /**
     * permet d'avoir l'ensemble des ids contrat dont l'organisme est invite.
     *
     * @param string    $organisme organisme invite
     * @param connexion $connexion
     *
     * @return array tableau des ids contrat dont l'organisme est invite
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function getIdsContratsByOrganismeInvite($organisme, $connexion = null)
    {
        if (!$connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
        $contratQuery = new CommonTContratTitulaireQuery();
        $contratQuery->select(CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE);
        $contratQuery->addJoin(CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE,CommonInvitationConsultationTransversePeer::ID_CONTRAT_TITULAIRE);
        $contratQuery->add(CommonInvitationConsultationTransversePeer::ORGANISME_INVITE, $organisme);
        $contratQuery->add(CommonTContratTitulairePeer::STATUT_CONTRAT, Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT_EFFECTUEE'));
        $contratQuery->add(CommonTContratTitulairePeer::CONTRAT_CLASS_KEY, CommonTContratTitulairePeer::CLASSKEY_1);
        return (array) $contratQuery->find($connexion);
    }

    /**
     * permet d'avoir l'ensemble des ids contrat dont le service est invite.
     *
     * @param int    $service  service invite
     * @param string    $organisme organisme invite
     * @param connexion $connexion
     *
     * @return array tableau des ids contrat dont le service est invite
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     * @version 1.0
     */
    public function getIdsContratsByServiceInvitePermanentEligible($idService, $organisme, $connexion = null)
    {
        if (!$connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
        $contratQuery = new CommonTContratTitulaireQuery();
        $contratQuery->select(CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE);
        $contratQuery->addJoin(CommonTContratTitulairePeer::ID_CONTRAT_TITULAIRE,CommonInvitePermanentContratPeer::ID_CONTRAT_TITULAIRE);
        $contratQuery->add(CommonInvitePermanentContratPeer::SERVICE, $idService);
        $contratQuery->add(CommonInvitePermanentContratPeer::ORGANISME, $organisme);
        $contratQuery->add(CommonTContratTitulairePeer::STATUT_CONTRAT, Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT_EFFECTUEE'));
        $contratQuery->add(CommonTContratTitulairePeer::CONTRAT_CLASS_KEY, CommonTContratTitulairePeer::CLASSKEY_1);
        return (array) $contratQuery->find($connexion);
    }

    /**
     * Permet d'avoir liste des contrats sans donnees consultation.
     *
     * @param int  $horsPassation
     * @param null $conenxion
     *
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since place-2015
     * @copyright Atexo 2016
     */
    public function getContratsSansDonneesConsultation($horsPassation = 0, $conenxion = null): array|\Application\Propel\Mpe\CommonTContratTitulaire
    {
        $contratsQuery = new CommonTContratTitulaireQuery();

        return $contratsQuery->getContratsSansDonneesConsultation($horsPassation, $conenxion);
    }

    /**
     * Permet de remplir donnees consultation.
     *
     * @param CommonTDonneesConsultation $donneesConsultation
     * @param CommonConsultation         $consultation
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since place-2015
     *
     * @copyright Atexo 2016
     */
    public function fillDonneesConsultation(&$donneesConsultation, $consultation)
    {
        if (($consultation instanceof CommonConsultation) && ($donneesConsultation instanceof CommonTDonneesConsultation)) {
            $donneesConsultation->setOrganisme($consultation->getOrganisme());
            $donneesConsultation->setServiceId($consultation->getServiceId());
            $donneesConsultation->setReferenceConsultation($consultation->getId());
            $donneesConsultation->setIdTypeProcedure($consultation->getIdTypeProcedureOrg());
            $donneesConsultation->setLibelleTypeProcedure($consultation->getLibelleTypeProcedureOrg());
            $nbrOffres = (new Atexo_Consultation_Register())->retrieveNumbreOfElectronicDepot($consultation->getId());
            $nbrOffrePapier = (new Atexo_Consultation_Register())->retrieveNumbreOfPaperDepot($consultation->getId());
            $nbrReponses = $nbrOffres + $nbrOffrePapier;
            $donneesConsultation->setNbreOffresRecues($nbrReponses);
            $donneesConsultation->setNbreOffresDematerialisees($nbrOffres);
            $donneesConsultation->setSignatureOffre($consultation->getSignatureOffre());
        }
    }

    /**
     * Permet de retourner la liste des lots d'un contrat.
     *
     * @param $idContrat
     *
     * @return array
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since place-2015
     *
     * @copyright Atexo 2016
     */
    public function getLotsContrat($idContrat)
    {
        $consLotQuery = new CommonTConsLotContratQuery();

        return $consLotQuery->getLotsByIdContrat($idContrat);
    }

    /**
     * Permet de retourner la liste des lots d'un contrat.
     *
     * @param $idContrat
     *
     * @return array
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since place-2015
     *
     * @copyright Atexo 2016
     */
    public function getLotsContrats($idcontrat)
    {
        $listeLots = (new Atexo_Consultation_Contrat())->getLotsContrat($idcontrat);
        if (is_array($listeLots) && count($listeLots)) {
            return implode(',', $listeLots);
        }

        return '-';
    }

    /**
     * Permet de creer et pre-remplir un objet CommonTContactContrat en fonction d'une offre entreprise.
     *
     * @param CommonOffres $offre : offre entreprise
     *
     * @return CommonTContactContrat
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function getContactByOffre($offre)
    {
        $contact = new CommonTContactContrat();
        if ($offre instanceof CommonOffres || $offre instanceof CommonOffrePapier) {
            $contact->setIdEtablissement($offre->getIdEtablissement());
            $contact->setNom($offre->getNom());
            $contact->setPrenom($offre->getPrenom());
            $contact->setEmail($offre->getEmail());
            $contact->setTelephone($offre->getTelephone());
        }

        return $contact;
    }

    /**
     * Permet de generer des contrats pour une consultation.
     *
     * @param CommonOffres $offre : offre entreprise
     *
     * @return CommonTContactContrat
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function generateContrat($consultation)
    {
        $logger = Atexo_LoggerManager::getLogger('contrat');
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        try {
            $connexion->beginTransaction();
            $sqlCritereConsultation = self::critereConsultation($consultation, 'C.id', 'C.organisme');
            $arrayOrganismeChorus = [];
            $arrayRetour = [];

            $sql = <<<SQL
SELECT  DE.id_decision_enveloppe AS cle_table_source,1 as type_contrat,DE.numero_marche as num_EJ, DE.organisme,C.service_id,O.entreprise_id,O.id_etablissement,O.id as id_offre,DE.type_enveloppe,
    DE.objet_marche,IF(DE.montant_marche IS NULL,NULL,CAST((REPLACE(REPLACE(DE.montant_marche,' ',''),',','.')) as DECIMAL(65,2))) as montant_marche,DE.tranche_budgetaire,0 as publication_montant,
    0 as publication_contrat,DE.statutEJ,DE.numero_marche , NULL as num_long_OEAP,1 as statut_contrat, C.categorie, DE.date_notification,DE.date_fin_marche_previsionnel,
    DE.date_notification_reelle,DE.date_fin_marche_reelle,DL.date_decision,NOW() as date_creation,NOW() as date_modification, DE.envoi_interface, C.reference_utilisateur as reference_libre,
    O.inscrit_id, O.nom_inscrit, O.prenom_inscrit, O.email_inscrit, O.telephone_inscrit, O.fax_inscrit, DE.lot, C.id,0 as hors_passation
  FROM decisionEnveloppe DE,Offres O,DecisionLot DL,consultation C
  WHERE DE.type_enveloppe = 1
	AND DE.id_offre = O.id
	AND DE.organisme = O.organisme
	AND DE.organisme = DL.organisme
	AND DE.consultation_id = DL.consultation_id
	AND DE.lot = DL.lot
	AND DL.id_type_decision = '2'
	AND C.id = DL.consultation_id
	AND C.organisme = DL.organisme
	AND $sqlCritereConsultation
UNION
SELECT  DE.id_decision_enveloppe AS cle_table_source,1 as type_contrat,DE.numero_marche as num_EJ, DE.organisme,C.service_id,O.entreprise_id,O.id_etablissement,O.id as id_offre,DE.type_enveloppe,
    DE.objet_marche,IF(DE.montant_marche IS NULL,NULL,CAST((REPLACE(REPLACE(DE.montant_marche,' ',''),',','.')) as DECIMAL(65,2))) as montant_marche,DE.tranche_budgetaire,0 as publication_montant,
    0 as publication_contrat,DE.statutEJ,DE.numero_marche , NULL as num_long_OEAP,1 as statut_contrat, C.categorie, DE.date_notification,DE.date_fin_marche_previsionnel,
    DE.date_notification_reelle,DE.date_fin_marche_reelle,DL.date_decision,NOW() as date_creation,NOW() as date_modification, DE.envoi_interface, C.reference_utilisateur as reference_libre,

    NULL as inscrit_id, O.nom as nom_inscrit, O.prenom as prenom_inscrit, O.email as email_inscrit, O.telephone as telephone_inscrit, O.fax as fax_inscrit, DE.lot, C.id,0 as hors_passation
  FROM decisionEnveloppe DE,Offre_papier O,DecisionLot DL,consultation C
  WHERE DE.type_enveloppe = 2
	AND DE.id_offre = O.id
	AND DE.organisme = O.organisme
	AND DE.organisme = DL.organisme
	AND DE.consultation_id = DL.consultation_id
	AND DE.lot = DL.lot
	AND DL.id_type_decision = '2'
	AND C.id = DL.consultation_id
	AND C.organisme = DL.organisme
	AND $sqlCritereConsultation
SQL;
            $logger->info('Sql de récupération des decisions enveloppes : '.$sql."\n");

            //return;
            $statement = Atexo_Db::getLinkCommon(true)->prepare($sql);
            $statement->execute();
            $nbreDecisionEnveloppe = $statement->rowCount();
            $logger->info('Nombre des decisions enveloppes : '.$nbreDecisionEnveloppe."\n");
            while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                $logger->info('Decision  : '.$row['cle_table_source'].' consultation ('.$row['organisme'].', '.$row['id'].") \n");
                $organisme = $row['organisme'];
                if (!isset($arrayOrganismeChorus[$organisme])) {
                    $configurationOrgQuery = new CommonConfigurationOrganismeQuery();
                    $configurationOrganisme = $configurationOrgQuery->findOneByOrganisme($organisme, $connexion);
                    if ($configurationOrganisme instanceof CommonConfigurationOrganisme) {
                        if ($configurationOrganisme->getInterfaceChorusPmi()) {
                            $arrayOrganismeChorus[$organisme] = true;
                        } else {
                            $arrayOrganismeChorus[$organisme] = false;
                        }
                    } else {
                        $arrayOrganismeChorus[$organisme] = false;
                    }
                }
                if (false == $arrayOrganismeChorus[$organisme]) {
                    $row['genereNumerotation'] = true;
                } else {
                    $row['genereNumerotation'] = true;
                }

                $row['numero_marche'] = (new Atexo_Chorus_Util())->retournLastNumeroMarcheDecision($row['numero_marche']);
                $contratTitulaire = (new Atexo_Consultation_Contrat())->createContrat($row, $connexion, $logger);
                $idContrat = $contratTitulaire->getIdContratTitulaire();
                $idDecision = $row['cle_table_source'];
                $arrayRetour['lots'][$row['lot']] = $row['lot'];
                $titulaire = $contratTitulaire->getTitulaireContrat();
                if ($titulaire instanceof Entreprise) {
                    $arrayRetour['attributaires'][] = $titulaire->getNom();
                }
                // mise a jour des autres tables
                $sqlUpdate = <<<SQLUPDATETABLES
UPDATE Chorus_echange C SET C.`id_decision`= $idContrat WHERE C.id_decision = $idDecision AND C.organisme='$organisme';

UPDATE chorus_numeros_marches C SET C.`id_decision`= $idContrat WHERE C.id_decision = $idDecision AND C.acronyme_organisme='$organisme';

UPDATE chorus_noms_fichiers C SET C.id_echange= $idContrat WHERE C.id_echange = $idDecision AND C.acronyme_organisme='$organisme' AND C.type_fichier='FSO';

UPDATE Autres_Pieces_Mise_Disposition AP SET AP.`id_decision_enveloppe`= $idContrat WHERE AP.id_decision_enveloppe = $idDecision AND AP.org='$organisme';

UPDATE Pieces_Mise_Disposition P SET P.`id_decision_enveloppe`= $idContrat WHERE P.id_decision_enveloppe = $idDecision AND P.org='$organisme';

UPDATE Marche M SET M.`id_decision_enveloppe`= $idContrat WHERE M.id_decision_enveloppe= $idDecision AND M.organisme='$organisme';

UPDATE Contrat C SET C.`id_decision`= $idContrat WHERE C.id_decision = $idDecision AND C.organisme='$organisme';

UPDATE t_contrat_titulaire T, Chorus_echange C SET T.`statut_contrat`=3 WHERE T.id_contrat_titulaire = C.id_decision AND T.organisme=C.organisme AND T.id_contrat_titulaire = $idContrat AND T.organisme = '$organisme';

UPDATE t_contrat_titulaire T SET T.`statut_contrat`='4' WHERE T.id_contrat_titulaire = $idContrat AND T.organisme = '$organisme' AND T.date_notification IS NOT NULL ;

SQLUPDATETABLES;
                $logger->info("Sql d'update des autres tables : ".$sqlUpdate."\n");
                $statementUpdate = $connexion->prepare($sqlUpdate);
                $statementUpdate->execute();
                $statementUpdate->closeCursor();

                $donneesConsultation = new CommonTDonneesConsultation();
                (new Atexo_Consultation_Contrat())->fillDonneesConsultation($donneesConsultation, $consultation);
                $donneesConsultation->setIdContratTitulaire($idContrat);
                $logger->info("l'enregistrement des donnees consultation");
                $donneesConsultation->save($connexion);
            }
            $connexion->commit();
            $arrayRetour['nbreDecisionEnveloppe'] = $nbreDecisionEnveloppe;

            return $arrayRetour;
        } catch (\Exception $e) {
            $connexion->rollback();
            $logger->error("Erreur migration contrat : $sql ");
            $logger->error('Erreur migration contrat : '.$e->getMessage().$e->getTraceAsString());
        }
    }

    public function critereConsultation($consultation, $champsReference, $champsOrganisme)
    {
        $sql = ' (0 ';
        if ($consultation instanceof CommonConsultation) {
            $sql .= "OR ($champsReference = '".$consultation->getId()."' AND $champsOrganisme = '".$consultation->getOrganisme()."')";
        }
        $sql .= ' ) ';

        return $sql;
    }

    public function consultationHasContrat($consultation)
    {
        $sqlCritereVerif = self::critereConsultation($consultation, 'consultation_id', 'organisme');
        $sqlverification = <<<SQLVERIF
SELECT * from t_cons_lot_contrat WHERE
$sqlCritereVerif
SQLVERIF;

        $statementVerif = Atexo_Db::getLinkCommon(true)->prepare($sqlverification);
        $statementVerif->execute();

        return $statementVerif->rowCount();
    }

    /**
     * Permet de definir les cas de suppression relatifs aux contrats
     *    |
     *    |---> Atexo_Contrat_IndicateurEnum::CONTRAT_SUPPRESSION_AUTORISEE: suppression autorisee
     *    |---> Atexo_Contrat_IndicateurEnum::CONTRAT_SUPPRESSION_NON_AUTORISEE: suppression non autorisee
     *    |---> Atexo_Contrat_IndicateurEnum::CONTRAT_SUPPRESSION_NON_AUTORISEE_AC_OU_SAD_LIE: suppression non autorisee, contrat avec AC/SAD lie
     *    |---> Atexo_Contrat_IndicateurEnum::CONTRAT_SUPPRESSION_NON_AUTORISEE_ENVOI_MAIL: suppression non autorisee, envoi de mail a l'agent.
     *
     * @param CommonTContratTitulaire $contrat
     *
     * @return string
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since 2017-roadmap
     *
     * @copyright Atexo 2017
     */
    public static function getCasSuppressionContrat($idContrat, $organisme)
    {
        $cas = null;
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $contrat = CommonTContratTitulaireQuery::create()->filterByIdContratTitulaire($idContrat)->filterByOrganisme($organisme)->findOne($connexion);
        if ($contrat) {
            if ($contrat->getStatutContrat() != Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT')  // le statut contrat est différent de "N orange"
                && $contrat->getStatutContrat() != Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT_EFFECTUEE')) { // le statut contrat est différent de "N vert"
                $cas = Atexo_Contrat_IndicateurEnum::CONTRAT_SUPPRESSION_AUTORISEE;
            } else {
                //e statut contrat est soit en cours de notification "N orange" ou notifié" N vert"
                $dernierEchangeChorus = self::recupererDernierEchangeChorusPourUnContrat($contrat);
                if ((new Atexo_Consultation_Contrat())->isTypeContratMarche($contrat->getIdTypeContrat())) { //Contrat de type marche
                    if (!$dernierEchangeChorus) { // Pas de raccordement avec Chorus
                        $cas = Atexo_Contrat_IndicateurEnum::CONTRAT_SUPPRESSION_AUTORISEE;
                    } else { // Raccordement avec Chorus
                        //il faut vérifier que le statut du dernier EJ est supprimé
                        if (strstr((new Atexo_Chorus_Util())->retournLastStatutEJDecision($contrat->getStatutej(), $contrat->getNumeroContrat()), Atexo_Config::getParameter('CHORUS_STATUT_EJ_SUPPRIME'))) { //dernier statut EJ supprime
                            $cas = self::getCasSuppressionEchangeChorus($dernierEchangeChorus, $contrat);
                        } else { // dernier EJ statut non supprime
                            $cas = Atexo_Contrat_IndicateurEnum::CONTRAT_SUPPRESSION_NON_AUTORISEE_ENVOI_MAIL;
                        }
                    }
                } elseif (self::isTypeContratAccordCadre($contrat->getIdTypeContrat()) ||(new Atexo_Consultation_Contrat())->isTypeContratSad($contrat->getIdTypeContrat())) { //Contrat de type AC/SAD
                    if (!$dernierEchangeChorus) { // Pas de raccordement avec Chorus
                        switch ($contrat->getStatutContrat()) {
                            case Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT'): // statut contrat "N orange"
                                $cas = Atexo_Contrat_IndicateurEnum::CONTRAT_SUPPRESSION_AUTORISEE;
                                break;
                            case Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT_EFFECTUEE'): // statut contrat "N vert"
                                if (self::nombreConsultationOuMarcheSubsequentLieContrat($contrat, $organisme, $connexion) > 0) { // marche subsequent ou consultation lie au contrat
                                    $cas = Atexo_Contrat_IndicateurEnum::CONTRAT_SUPPRESSION_NON_AUTORISEE_AC_OU_SAD_LIE;
                                } else { // aucune consultation ou marche subsequent lie au contrat
                                    $cas = Atexo_Contrat_IndicateurEnum::CONTRAT_SUPPRESSION_AUTORISEE;
                                }
                                break;
                        }
                    } else { // Raccordement avec Chorus
                        switch ($contrat->getStatutContrat()) {
                            case Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT'): // statut contrat "N orange"
                                $cas = self::getCasSuppressionEchangeChorus($dernierEchangeChorus, $contrat);
                                break;
                            case Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT_EFFECTUEE'): // statut contrat "N vert"
                                if (self::nombreConsultationOuMarcheSubsequentLieContrat($contrat, $organisme, $connexion) > 0) { // marche subsequent ou consultation lie au contrat
                                    $cas = Atexo_Contrat_IndicateurEnum::CONTRAT_SUPPRESSION_NON_AUTORISEE_AC_OU_SAD_LIE;
                                } else { // aucune consultation ou marche subsequent lie au contrat
                                    $cas = self::getCasSuppressionEchangeChorus($dernierEchangeChorus, $contrat);
                                }
                                break;
                        }
                    }
                }
            }

            return $cas;
        } else {
            throw new Exception('Contrat non trouvé');
        }
    }

    /**
     * Permet de definir les cas de suppression relatifs aux echanges Chorus.
     *
     * @param CommonChorusEchange $echange objet echange Chorus
     *
     * @return string
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since 2017-roadmap
     *
     * @copyright Atexo 2017
     */
    public static function getCasSuppressionEchangeChorus(CommonChorusEchange $echange, CommonTContratTitulaire $contratTitulaire)
    {
        $cas = null;
        //la fonction Atexo_Chorus_Echange::VerifierAccesChorusDetailsEchange permet de déterminer si on peut accéder à un échange
        //c'est le cas si l'échange est au statut rejeté, brouillon ou non encore envoyé ou bien si l'échange est envoyé mais le retour chorus est en erreur
        //si cette fonction renvoie true, alors la suppression du contrat est autorisée
        switch (Atexo_Chorus_Echange::VerifierAccesChorusDetailsEchange($echange->getStatutechange(), $echange->getRetourChorus())) {
            case true:
                $cas = Atexo_Contrat_IndicateurEnum::CONTRAT_SUPPRESSION_AUTORISEE;
                break;
            case false:
                //par défaut on considére que la suppression n'est pas autorisée
                $cas = Atexo_Contrat_IndicateurEnum::CONTRAT_SUPPRESSION_NON_AUTORISEE;
                //si la fonction renvoie false, il y a un cas possible de suppression de contrat qui est le suivant
                //le dernier échange a été envoyé et le retour chorus associé à cet échange est au statut intégré
                //et la date de réception du dernier FSO qui indique que l'EJ est supprimé est postérieure à la date d'envoi du dernier échange Chorus
                //en synthèse on a envoyé plusieurs échanges Chorus qui sont au statut intégré et il y a le dernier FSO qui indique que l'EJ est supprimé
                //on vérifie toutes les conditions pour dterminer si contrat supprimable cela évite de gérer les else du code à chaque fois
                if (($echange->getStatutechange() == Atexo_Config::getParameter('STATUS_ECHANGE_CHORUS_ENVOYER'))
                    && ($echange->getRetourChorus() == Atexo_Config::getParameter('CHORUS_RETOUR_INTEGRE'))) {
                    $dateEnvoiEchange = $echange->getDateEnvoi();
                    $dataSource = (new Atexo_Chorus_Util())->remplirTableauInformationsChorus($contratTitulaire);
                    if (is_array($dataSource) && count($dataSource) > 0) {
                        $lastFSO = end($dataSource); //on prend le dernier FSO reçu
                        if ($lastFSO) {
                            $dateReceptionDernierFSO = $lastFSO['date_ajout'];
                            if (Atexo_Util::diffDate($dateEnvoiEchange, $dateReceptionDernierFSO) > 0) {
                                $cas = Atexo_Contrat_IndicateurEnum::CONTRAT_SUPPRESSION_AUTORISEE;
                            }
                        }
                    }
                }
                break;
        }

        return $cas;
    }

    /**
     * Permet de construire le message de confirmation a afficher en fonction des differents cas de figure.
     *
     * @param $cas
     * @param int    $idContrat
     * @param string $organisme
     *
     * @return mixed|string
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since 2017-roadmap
     *
     * @copyright Atexo 2017
     */
    public static function getMessageConfirmationSuppressionContrat($cas, $idContrat, $organisme)
    {
        switch ($cas) {
            case Atexo_Contrat_IndicateurEnum::CONTRAT_SUPPRESSION_AUTORISEE:
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
                $contrat = CommonTContratTitulaireQuery::create()->filterByIdContratTitulaire($idContrat)->filterByOrganisme($organisme)->findOne($connexion);
                $dernierEchangeChorus = self::recupererDernierEchangeChorusPourUnContrat($contrat);
                if (!$dernierEchangeChorus) {//Pas de raccordement avec Chorus
                    $message = Prado::localize('CONFIRMATION_SUPPRESSION_CONTRAT');
                } else {//Raccordement avec Chorus
                    $message = Prado::localize('MESSAGE_CONFIRMATION_SUPPRESSION_CONTRAT_AUTORISEE');
                }
                break;
            case Atexo_Contrat_IndicateurEnum::CONTRAT_SUPPRESSION_NON_AUTORISEE:
                $message = Prado::localize('MESSAGE_CONFIRMATION_SUPPRESSION_CONTRAT_NON_AUTORISEE');
                break;
            case Atexo_Contrat_IndicateurEnum::CONTRAT_SUPPRESSION_NON_AUTORISEE_AC_OU_SAD_LIE:
                $message = Prado::localize('MESSAGE_CONFIRMATION_SUPPRESSION_CONTRAT_NON_AUTORISEE_AC_OU_SAD_LIE');
                break;
            case Atexo_Contrat_IndicateurEnum::CONTRAT_SUPPRESSION_NON_AUTORISEE_ENVOI_MAIL:
                $message = Prado::localize('MESSAGE_CONFIRMATION_SUPPRESSION_CONTRAT_NON_AUTORISEE_ENVOI_MAIL');
                break;
            default:
                $message = '';
                break;
        }

        return $message;
    }

    /**
     * Calcule le nombre de consultations ou marches subsequents lies a un contrat.
     *
     * @param string    $organisme
     * @param PropelPDO $connexion
     *
     * @return int
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since 2017-roadmap
     *
     * @copyright Atexo 2017
     */
    public static function nombreConsultationOuMarcheSubsequentLieContrat(CommonTContratTitulaire $contrat, $organisme, $connexion = null)
    {
        if (empty($connexion)) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
        $idContrat = $contrat->getIdContratMulti() ?: $contrat->getIdContratTitulaire();

        return CommonConsultationQuery::create()->filterByOrganisme($organisme)->filterByIdContrat($idContrat)->count($connexion);
    }

    /**
     * Permet d'envoyer le mail de suppression de l'EJ (suite a la demande de suppression du contrat) a l'agent.
     *
     * @param $idContrat
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since 2017-roadmap
     *
     * @copyright Atexo 2017
     */
    public static function envoiMailSuppressionEj($idContrat, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $contrat = CommonTContratTitulaireQuery::create()->filterByIdContratTitulaire($idContrat)->filterByOrganisme($organisme)->findOne($connexion);
        if ($contrat instanceof CommonTContratTitulaire) {
            $chorusEchange = (new Atexo_Chorus_Echange())->retreiveLastEchangeCreated($contrat->getOrganisme(), $contrat->getIdContratTitulaire());
            if ($chorusEchange instanceof CommonChorusEchange) {
                $ficheNavette = (new Atexo_Chorus_Echange())->retrieveFicheNavetteByIdChorusEchangeAndOrg($chorusEchange->getId(), $chorusEchange->getOrganisme());
                if ($ficheNavette instanceof CommonChorusFicheNavette) {
                    try {
                        $tmpPathPj = self::genererDocDemandeSuppressionEj($ficheNavette, $contrat, $chorusEchange);
                        $fileName = str_replace(
                            '[__NUMERO_EJ__]',
                            (new Atexo_Chorus_Util())->retournLastNumeroMarcheDecision($contrat->getNumeroContrat()),
                            Atexo_Util::toUtf8(Prado::localize('NOM_DOCUMENT_DEMANDE_SUPPRESSION_EJ_CHORUS'))
                        );
                        Atexo_Message::envoiMailAgentDemandeSuppressionEj($contrat, $tmpPathPj, $fileName);
                    } catch (Exception $exception) {
                        $logger = Atexo_LoggerManager::getLogger('app');
                        $logger->error(
                            "Erreur lors de la suppression du contrat : idContrat = "
                            . $idContrat
                            . " , organisme = "
                            . $organisme . " \n\nErreur : "
                            .$exception->getMessage()
                        );
                    }
                } else {
                    throw new \Exception("La fiche navette est introuvable: idContrat = $idContrat | idEchange = ".$chorusEchange->getId()." | organisme = $organisme");
                }
            } else {
                throw new \Exception("L'echange Chorus est introuvable: idContrat = $idContrat | idEchange = ".$chorusEchange->getId()." | organisme = $organisme");
            }
        } else {
            throw new \Exception("Contrat introuvable: idContrat = $idContrat | organisme = $organisme");
        }
    }

    /**
     * Permet de gerer le document de demande de suppression de l'Engagement Juridique.
     *
     * @return string
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since 2017-roadmap
     *
     * @copyright Atexo 2017
     */
    public static function genererDocDemandeSuppressionEj(CommonChorusFicheNavette $ficheNavette, CommonTContratTitulaire $contrat, CommonChorusEchange $chorusEchange)
    {
        $fileGenereOdt = (new Atexo_GenerationFichier_GenerationDocumentsChorus())->genererDocDemandeSuppressionEj($ficheNavette, $contrat, $chorusEchange);
        $fileGenerePdf = str_replace('.odt', '.pdf', $fileGenereOdt);
        Atexo_Util::write_file($fileGenerePdf, (new PdfGeneratorClient())->genererPdf($fileGenereOdt));

        return $fileGenerePdf;
    }

    /**
     * Permet de recuperer le dernier echange Chorus pour un contrat.
     *
     * @return bool|CommonChorusEchange : dernier echange chorus s'il existe, false sinon
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since 2017-roadmap
     *
     * @copyright Atexo 2017
     */
    public static function recupererDernierEchangeChorusPourUnContrat(CommonTContratTitulaire $contrat): bool|\Application\Propel\Mpe\CommonChorusEchange
    {
        return (Atexo_Module::isEnabled('InterfaceChorusPmi') && (new Atexo_Module())->isChorusAccessActivate(Atexo_CurrentUser::getIdServiceAgentConnected()) && $contrat->getModeEchangeChorus()) ? (new Atexo_Chorus_Echange())->retreiveLastEchangeCreated($contrat->getOrganisme(), $contrat->getIdContratTitulaire()) : false;
    }

    /**
     * Retourne les contrats sans donnees consultation.
     *
     * @param int  $horsPassation
     * @param null $connexion
     *
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since place-2015
     * @copyright Atexo 2016
     */
    public function recupererContratsByIdEchange($idEchange, $connexion = null): array|\Application\Propel\Mpe\CommonTContratTitulaire
    {
        if (!$connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
        $contratQuery = new CommonTContratTitulaireQuery();

        return $contratQuery->getContratsByIdEchange($idEchange, $connexion);
    }

    /**
     * @param $idTypeContrat
     *
     * @return bool
     */
    public function isTypeContratConcession($idTypeContrat)
    {
        $allContrat = self::getCachedListeTypeContrat();

        foreach ($allContrat as $item) {
            if (($item->getIdTypeContrat() == $idTypeContrat) && ($item->getConcession())) {
                return true;
            }
        }

        return false;
    }


    private function deleteClauses(int $contratId): void
    {
        $contratRepository = Atexo_Util::getSfService(ContratTitulaireRepository::class);
        $contratSf = $contratRepository->find($contratId);
        if ($contratSf instanceof ContratTitulaire) {
            $clausesService = Atexo_Util::getSfService(ClausesService::class);
            $clausesService->deleteClausesRelatedTo($contratSf);
        }
    }
}
