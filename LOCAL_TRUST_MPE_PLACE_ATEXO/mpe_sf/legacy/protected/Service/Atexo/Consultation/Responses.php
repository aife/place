<?php

namespace Application\Service\Atexo\Consultation;

use App\Utils\Encryption;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Pages\Commun\DownloadFile;
use Application\Propel\Mpe\CommonAdmissibiliteEnveloppeLot;
use Application\Propel\Mpe\CommonAdmissibiliteEnveloppeLotPeer;
use Application\Propel\Mpe\CommonAdmissibiliteEnveloppeLotQuery;
use Application\Propel\Mpe\CommonAdmissibiliteEnveloppePapierLot;
use Application\Propel\Mpe\CommonAdmissibiliteEnveloppePapierLotPeer;
use Application\Propel\Mpe\CommonAdmissibiliteEnveloppePapierLotQuery;
use Application\Propel\Mpe\CommonAvisMembresCAO;
use Application\Propel\Mpe\CommonBlocFichierEnveloppePeer;
use Application\Propel\Mpe\CommonCategorieConsultation;
use Application\Propel\Mpe\CommonCategorieLot;
use Application\Propel\Mpe\CommonCategorieLotPeer;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonDecisionEnveloppe;
use Application\Propel\Mpe\CommonDecisionEnveloppePeer;
use Application\Propel\Mpe\CommonDecisionLot;
use Application\Propel\Mpe\CommonDecisionLotPeer;
use Application\Propel\Mpe\CommonEnveloppe;
use Application\Propel\Mpe\CommonEnveloppePapier;
use Application\Propel\Mpe\CommonEnveloppePapierPeer;
use Application\Propel\Mpe\CommonEnveloppePeer;
use Application\Propel\Mpe\CommonEnveloppeQuery;
use Application\Propel\Mpe\CommonFichierEnveloppe;
use Application\Propel\Mpe\CommonFichierEnveloppePeer;
use Application\Propel\Mpe\CommonInvitationConsultationTransversePeer;
use Application\Propel\Mpe\CommonOffrePapier;
use Application\Propel\Mpe\CommonOffrePapierPeer;
use Application\Propel\Mpe\CommonOffres;
use Application\Propel\Mpe\CommonOffresPeer;
use Application\Propel\Mpe\CommonOffresQuery;
use Application\Propel\Mpe\CommonResultatAnalyse;
use Application\Propel\Mpe\CommonResultatAnalysePeer;
use Application\Propel\Mpe\CommonTCandidatureMps;
use Application\Propel\Mpe\EntreprisePeer;
use Application\Service\Atexo\Agent\Atexo_Agent_Notifications;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_AvisMembresCAO;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_Crypto;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_Entreprise;
use Application\Service\Atexo\Atexo_Exception;
use Application\Service\Atexo\Atexo_FichierEnveloppe;
use Application\Service\Atexo\Atexo_Groupement;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_MarchePublicSimplifie;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Odf;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Controller\Atexo_Controller_Front;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Inscrit;
use Application\Service\Atexo\Registres\Atexo_Registres_RegistreDepot;
use Application\Service\Atexo\Response\Atexo_Response_CriteriaVo;
use Application\Service\Atexo\Rest\Action\Atexo_Rest_Action_EnveloppeField;
use Application\Service\Atexo\Rest\ObjectField\Atexo_Rest_ObjectField_AdmissibiliteField;
use Application\Service\Atexo\Rest\ObjectField\Atexo_Rest_ObjectField_AdmissibiliteLotField;
use Application\Service\Atexo\Serializer\Atexo_Serializer_Serializer;
use AtexoPdf\PdfGeneratorClient;
use Exception;
use PDO;
use Prado\Prado;
use Prado\Util\TLogger;
use ReflectionClass;
use Symfony\Component\DependencyInjection\Container;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 *
 * @version 1.0
 *
 * @since MPE-3.0
 *
 * @copyright Atexo 2008
 */
class Atexo_Consultation_Responses
{
    public function retrieveEnveloppesCandidatureElectroniques($criteriaResponse)
    {
        $admissible = null;
        if ($criteriaResponse->getSearchByIdOffre() && !$criteriaResponse->getIdOffre()) {
            throw new Atexo_Exception('idOffre non fournie');
        }

        if (!$criteriaResponse->getReference()) {
            throw new Atexo_Exception('reference non fournie');
        }
        $arrayTri = [];
        if ($criteriaResponse->getTrier() && $criteriaResponse->getObjet()) {
            $arrayTri['tri'] = $criteriaResponse->getObjet() . '.' . $criteriaResponse->getTrier();
            $arrayTri['sens'] = $criteriaResponse->getSensTri();
        }
        if ($criteriaResponse->getSearchByIdOffre()) {
            $enveloppes = self::retrieveEnveloppesCandidatureElectroniquesByidOffre($criteriaResponse->getIdOffre(), $criteriaResponse->getOrganisme(), $criteriaResponse->getModeUnique());
        } else {
            if ($criteriaResponse->getLimit() && $criteriaResponse->getOffset()) {
                $enveloppes = self::retrieveEnveloppesCandidatureElectroniquesByReference(
                    $criteriaResponse->getReference(),
                    $criteriaResponse->getOrganisme(),
                    $criteriaResponse->getLimit(),
                    $criteriaResponse->getOffset(),
                    $criteriaResponse->getModeUnique(),
                    $arrayTri
                );
            } else {
                $enveloppes = self::retrieveEnveloppesCandidatureElectroniquesByReference(
                    $criteriaResponse->getReference(),
                    $criteriaResponse->getOrganisme(),
                    null,
                    null,
                    $criteriaResponse->getModeUnique(),
                    $arrayTri
                );
                if ($criteriaResponse->getCount()) {
                    return $enveloppes;
                }
            }
        }

        if ($enveloppes) {
            self::addAdmissibility($enveloppes, $criteriaResponse->getReference(), $criteriaResponse->getOrganisme(), Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE'), null, $admissible);
            self::addLibelleStatut($enveloppes);
            if (null !== $criteriaResponse->getTypesEnveloppes()) {
                if (
                    !self::enveloppeCandidatureExists($criteriaResponse->getTypesEnveloppes()) &&
                    !self::enveloppeOffreExists($criteriaResponse->getTypesEnveloppes()) &&
                    !self::enveloppeAnonymatExists($criteriaResponse->getTypesEnveloppes())
                ) {
                    throw new Atexo_Exception('Values not supported for typesEnveloppes ');
                }
                self::addCompanyName($enveloppes, $criteriaResponse->getTypesEnveloppes(), $criteriaResponse->getOrganisme());
            }
            if (Atexo_Module::isEnabled('AvisMembresCommision')) {
                self::addStatistiqueAvisCAO($criteriaResponse->getReference(), $criteriaResponse->getOrganisme(), $enveloppes, $criteriaResponse->getArrayIdsGuests());
            }

            return $enveloppes;
        } else {
            return [];
        }
    }

    private function retrieveEnveloppesCandidatureElectroniquesByReference($consultationId, $organisme, $limit = null, $offset = null, $modeUnique = false, $arrayTri = null)
    {
        $params = [];
        $query = <<<QUERY
SELECT 
	Enveloppe. * , 
	Offres. *  
FROM 
	Enveloppe, 
	Offres 
WHERE 
		Offres.CONSULTATION_ID =:consultation_id
	AND
	 	Enveloppe.TYPE_ENV =:typeEnv 
	AND 
		Offres.DEPOT_ANNULE =:depotAnnule
	AND
	 	Enveloppe.OFFRE_ID = Offres.ID
	AND 
		Enveloppe.ORGANISME = Offres.ORGANISME 
	AND 
		Enveloppe.ORGANISME =:organisme
	AND 
		Offres.statut_offres != :statutBrouillon
	AND 
		Enveloppe.statut_enveloppe != :statutBrouillon
QUERY;
        if ($modeUnique) {
            $query .= ' AND ( Enveloppe.statut_enveloppe=:ouverte1  OR '
                  . '		Enveloppe.statut_enveloppe=:ouverteEnLigne  OR '
                  . '		Enveloppe.statut_enveloppe=:ouverteHorsLigne  OR '
                  . '		Enveloppe.statut_enveloppe=:ouverteADistance  ) ';

            $params[':ouverte1'] = Atexo_Config::getParameter('STATUT_ENV_OUVERTE');
            $params[':ouverteEnLigne'] = Atexo_Config::getParameter('STATUT_ENV_OUVERTE_EN_LIGNE');
            $params[':ouverteHorsLigne'] = Atexo_Config::getParameter('STATUT_ENV_OUVERTE_HORS_LIGNE');
            $params[':ouverteADistance'] = Atexo_Config::getParameter('STATUT_ENV_OUVERTE_A_DISTANCE');
        }
        if (is_array($arrayTri) && count($arrayTri)) {
            $trier = $arrayTri['tri'];
            $sensTri = $arrayTri['sens'];
            $query .= ' ORDER BY ' . $trier . ' ' . $sensTri;
        } else {
            $query .= ' ORDER BY Offres.untrusteddate ASC ';
        }
        if ($limit && $offset) {
            $query .= ' limit ' . $offset . ',' . $limit;
        }
        $params[':consultation_id'] = $consultationId;
        $params[':typeEnv'] = Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE');
        $params[':depotAnnule'] = Atexo_Config::getParameter('DEPOT_NON_ANNULE');
        $params[':organisme'] = $organisme;
        $params[':statutBrouillon'] = Atexo_Config::getParameter('STATUT_ENV_BROUILLON');
        $statement = Atexo_Db::getLinkCommon(true)->prepare($query);
        $statement->execute($params);
        $arraysEnveloppes = [];
        $arrayIds = [];
        $returnOffreWithinEnveloppe = true;
        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            if ($returnOffreWithinEnveloppe) {
                $id = $row['id_enveloppe_electro'];
            } else {
                $id = $row['id'];
            }
            if (!isset($arrayIds[$id])) {
                $enveloppe = self::fillObjectFromArray($row, $returnOffreWithinEnveloppe);
                $arraysEnveloppes[] = $enveloppe;
                $arrayIds[$id] = true;
            }
        }

        return $arraysEnveloppes;
    }

    public function retrieveEnveloppesCandidatureElectroniquesByidOffre($idOffre, $organisme, $modeUnique = false)
    {
        $params = [];
        $query = <<<QUERY
SELECT 
	Enveloppe. * , 
	Offres. *  
FROM
 	Enveloppe, 
 	Offres 
WHERE
 		Offres.ID=:idOffre 
	AND 
		Enveloppe.TYPE_ENV =:typeEnv 
	AND
	 	Offres.DEPOT_ANNULE =:depotAnnule 
	AND
		Enveloppe.OFFRE_ID = Offres.ID  
	AND
		Enveloppe.ORGANISME = Offres.ORGANISME 
	AND
		Enveloppe.ORGANISME =:organisme
	AND 
		Offres.statut_offres != :statutBrouillon
	AND 
		Enveloppe.statut_enveloppe != :statutBrouillon
QUERY;

        if ($modeUnique) {
            $query .= ' AND ( Enveloppe.statut_enveloppe=:ouverte1  OR '
                  . '		Enveloppe.statut_enveloppe=:ouverteEnLigne  OR '
                  . '		Enveloppe.statut_enveloppe=:ouverteHorsLigne  OR '
                  . '		Enveloppe.statut_enveloppe=:ouverteADistance  ) ';

            $params[':ouverte1'] = Atexo_Config::getParameter('STATUT_ENV_OUVERTE');
            $params[':ouverteEnLigne'] = Atexo_Config::getParameter('STATUT_ENV_OUVERTE_EN_LIGNE');
            $params[':ouverteHorsLigne'] = Atexo_Config::getParameter('STATUT_ENV_OUVERTE_HORS_LIGNE');
            $params[':ouverteADistance'] = Atexo_Config::getParameter('STATUT_ENV_OUVERTE_A_DISTANCE');
        }
        $query .= ' ORDER BY Offres.untrusteddate ASC ';
        $params[':idOffre'] = $idOffre;
        $params[':typeEnv'] = Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE');
        $params[':depotAnnule'] = Atexo_Config::getParameter('DEPOT_NON_ANNULE');
        $params[':organisme'] = $organisme;
        $params[':statutBrouillon'] = Atexo_Config::getParameter('STATUT_ENV_BROUILLON');

        $statement = Atexo_Db::getLinkCommon(true)->prepare($query);
        $statement->execute($params);
        $arraysEnveloppes = [];
        $arrayIds = [];
        $returnOffreWithinEnveloppe = true;
        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            if ($returnOffreWithinEnveloppe) {
                $id = $row['id_enveloppe_electro'];
            } else {
                $id = $row['id'];
            }
            if (!isset($arrayIds[$id])) {
                $enveloppe = self::fillObjectFromArray($row, $returnOffreWithinEnveloppe);
                $arraysEnveloppes[] = $enveloppe;
                $arrayIds[$id] = true;
            }
        }

        return $arraysEnveloppes;
    }

    /*
     * Permet d'ajouter statistique des avis CAO à les enveloppes donnée
     */
    private function addStatistiqueAvisCAO($consultationId, $organisme, &$object, $arrayIdsGuests = null)
    {
        if (is_array($object)) {
            foreach ($object as $oneObject) {
                if ($oneObject instanceof CommonEnveloppe) {
                    $static = self::retrieveStatisticCAO($oneObject->getIdEnveloppeElectro(), $oneObject, Atexo_Config::getParameter('DEPOT_ELECTRONIQUE'), $consultationId, $organisme, $arrayIdsGuests);
                    $oneObject->setStatistiqueAvisCAO($static);
                } elseif ($oneObject instanceof CommonEnveloppePapier) {
                    $static = self::retrieveStatisticCAO($oneObject->getIdEnveloppePapier(), $oneObject, Atexo_Config::getParameter('DEPOT_PAPIER'), $consultationId, $organisme, $arrayIdsGuests);
                    $oneObject->setStatistiqueAvisCAO($static);
                }
            }
        }
    }

    public function addAdmissibility(&$enveloppe, $consultationId, $organisme, $typeEnv, $sousPli = null, $admissible = true)
    {
        foreach ($enveloppe as $uneEnveloppe) {
            if ($uneEnveloppe instanceof CommonEnveloppe) {
                if ($uneEnveloppe->getCommonOffres() instanceof CommonOffres) {
                    self::retrieveNumberAdmissibilites($uneEnveloppe, $consultationId, $organisme, $typeEnv, $sousPli, $admissible);
                }
            } elseif ($uneEnveloppe instanceof CommonEnveloppePapier) {
                $offrePapier = self::retrieveOffrePapierById($uneEnveloppe->getOffrePapierId(), $organisme);
                if ($offrePapier instanceof CommonOffrePapier) {
                    $uneEnveloppe->setCommonOffrePapier($offrePapier);
                    self::retrieveNumberAdmissibilites($uneEnveloppe, $consultationId, $organisme, $typeEnv, $sousPli, $admissible);
                }
            }
        }
    }

    private function addLibelleStatut(&$enveloppe)
    {
        if (is_array($enveloppe)) {
            foreach ($enveloppe as $uneEnveloppe) {
                if ($uneEnveloppe instanceof CommonEnveloppe) {
                    if ($uneEnveloppe->getCommonOffres() instanceof CommonOffres) {
                        $uneEnveloppe->setLibelleStatutEnveloppe((new Atexo_Consultation_StatutEnveloppe())->getStatutEnveloppeById($uneEnveloppe->getStatutEnveloppe()));
                    }
                }
            }
        }
    }

    /**
     * Retourne le type d'enveloppe suivante selon l'ordre d'ouverture des plis.
     *
     * @param $typeEnveloppeEnCours char l'enveloppe en cours
     * @param $consultation Consultation
     */
    public function admissibiliteSuivante($typeEnveloppeEnCours, $consultation)
    {
        if ($typeEnveloppeEnCours == Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE')) {
            if ('0' != $consultation->getEnvOffreTechnique()) {
                return Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE');
            } elseif ('0' != $consultation->getEnvOffre()) {
                return Atexo_Config::getParameter('TYPE_ENV_OFFRE');
            }
        } elseif ($typeEnveloppeEnCours == Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE')) {
            if ('0' != $consultation->getEnvOffre()) {
                return Atexo_Config::getParameter('TYPE_ENV_OFFRE');
            }
        }

        return false;
    }

    /**
     * Retourne l'admissibilié des enveloppes d'offres associées à l'enveloppe de candidature passée en paramètre.
     *
     * @param $enveloppe (entrée/sortie) l'objet Enveloppe ou EnveloppePapier de type candidature
     * @param $organisme le nom de l'organisme
     *
     * @return void
     */
    private function retrieveNumberAdmissibilites(&$enveloppe, $consultationId, $organisme, $typeEnveloppeEnCours, $sousPli, $admissible = true)
    {
        $arrayDescriptionLot = [];
        if (!($enveloppe instanceof CommonEnveloppe) && !($enveloppe instanceof CommonEnveloppePapier)) {
            throw new Atexo_Exception('Expected first Parameter to be an instance of Enveloppe or EnveloppePapier et and second one to be instance of consultation ');
        }
        $consultation = (new Atexo_Consultation())->retrieveConsultation($consultationId, $organisme);
        if (!($consultation instanceof CommonConsultation)) {
            throw new Atexo_Exception('No consultation found');
        }
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $nombreLot = is_countable($consultation->getAllLots($connexionCom, $organisme, $consultationId)) ? count($consultation->getAllLots($connexionCom, $organisme, $consultationId)) : 0;
        $arrayCryptage = [];
        $numSousPlis = [];

        $admissibiliteSuivante = self::admissibiliteSuivante($typeEnveloppeEnCours, $consultation);

        if ($enveloppe instanceof CommonEnveloppe) {
            $typeDepot = Atexo_Config::getParameter('DEPOT_ELECTRONIQUE');
        } else {
            $typeDepot = Atexo_Config::getParameter('DEPOT_PAPIER');
        }
        // dans le cas ou on gere l'admissibilite mais on a un env cand ,
        //on doit filtrer les lots postulee on gardant l'enable des element true c'est une cas exceptionnel
        if (!$admissible && $enveloppe->getTypeEnv() == Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE')) {
            $admissible = true;
            $casExceptionnel = true;
        } else {
            $casExceptionnel = false;
        }
        if ($admissibiliteSuivante && $admissible) {
            $params = [];
            if ($typeDepot == Atexo_Config::getParameter('DEPOT_ELECTRONIQUE')) {
                $idEnveloppe = $enveloppe->getIdEnveloppeElectro();
                $offre = $enveloppe->getCommonOffres();

                $query = <<<QUERY
 SELECT  
 	Env.id_enveloppe_electro AS idEnveloppe, 
 	sous_pli, 
 	cryptage, 
 	Env.statut_enveloppe 
FROM 
	Offres AS Of, 
	Enveloppe AS Env 
WHERE 
		Of.id=:idOffre 
	AND 
		Of.id=Env.offre_id 
	AND
	 	type_env=:typeEnv 
	AND 
		Of.organisme=Env.organisme 
	AND 
		Of.organisme=:organisme 
	AND 
		Of.statut_offres != :statutBrouillon
	AND 
		Env.statut_enveloppe != :statutBrouillon 
QUERY;
                $params[':statutBrouillon'] = Atexo_Config::getParameter('STATUT_ENV_BROUILLON');
            } else {
                $idEnveloppe = $enveloppe->getIdEnveloppePapier();
                $offre = $enveloppe->getOffrePapier();

                $query = 'select Env.id_enveloppe_papier as idEnveloppe, sous_pli, enveloppe_postule, cryptage, Env.statut_enveloppe from Offre_papier as Of, Enveloppe_papier as Env where '
                      . ' Of.id=:idOffre AND Of.id=Env.offre_papier_id AND type_env=:typeEnv AND Env.enveloppe_postule=:enveloppePostule AND Of.organisme=Env.organisme AND Of.organisme=:organisme ';

                $params[':enveloppePostule'] = Atexo_Config::getParameter('ENVELOPPE_POSTULE');
            }
            $query .= ' AND depot_annule=:depotNonAnnule ';
            $params[':depotNonAnnule'] = Atexo_Config::getParameter('DEPOT_NON_ANNULE');

            if ($sousPli) {
                $query .= ' AND sous_pli=:sousPli ';
                $params[':sousPli'] = $sousPli;
            }
            $params[':typeEnv'] = $admissibiliteSuivante;
            $params[':idOffre'] = $offre->getId();
            $params[':organisme'] = $organisme;

            $statement = Atexo_Db::getLinkCommon(true)->prepare($query);
            $statement->execute($params);

            while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                $numSousPlis[$row['sous_pli']] = $row['sous_pli'];
                if (
                    !$casExceptionnel && ($row['statut_enveloppe'] == Atexo_Config::getParameter('STATUT_ENV_OUVERTE') ||
                    $row['statut_enveloppe'] == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_EN_LIGNE') ||
                    $row['statut_enveloppe'] == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_HORS_LIGNE') ||
                    $row['statut_enveloppe'] == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_A_DISTANCE'))
                ) {
                    $arrayCryptage[$row['sous_pli']] = '0';
                } else {
                    $arrayCryptage[$row['sous_pli']] = '1';
                }
            }
        } else {
            if ($nombreLot > 0) {
                if ($sousPli) {
                    $numSousPlis[$sousPli] = $sousPli;
                    $arrayCryptage[$sousPli] = '1';
                } else {
                    $lots = $consultation->getAllLots($connexionCom, $organisme, $consultationId);
                    $numSousPlis = [];
                    $arrayCryptage = [];
                    foreach ($lots as $lot) {
                        $numSousPlis[$lot->getLot()] = $lot->getLot();
                        $arrayCryptage[$lot->getLot()] = '1';
                    }
                }
            } else {
                $numSousPlis['0'] = '0';
                $arrayCryptage['0'] = '1';
            }
        }

        if (!$numSousPlis) {
            return; //throw new Atexo_Exception('cas impossible.');
        }
        $nombreOffreDeposer = count($numSousPlis);
        $params = [];
        if ($typeDepot == Atexo_Config::getParameter('TYPE_DEPOT_QUESTION_ELECTRONIQUE')) {
            $query = ' SELECT AdmissibiliteLot.id_Offre as idEnveloppe, AdmissibiliteLot.sous_pli, '
                   . ' AdmissibiliteLot.admissibilite, AdmissibiliteLot.commentaire, AdmissibiliteLot.type_enveloppe  '
                   . ' FROM Admissibilite_Enveloppe_Lot as AdmissibiliteLot '
                   . ' Where  AdmissibiliteLot.id_Offre=:idOffre AND AdmissibiliteLot.sous_pli in (' . implode(',', $numSousPlis) . ') AND AdmissibiliteLot.organisme=:organisme ';

            $params[':idOffre'] = $enveloppe->getCommonOffres()->getId();
        } else {
            $query = ' SELECT AdmissibiliteLot.id_offre_papier as idEnveloppe, AdmissibiliteLot.sous_pli, '
                   . ' AdmissibiliteLot.admissibilite, AdmissibiliteLot.commentaire, AdmissibiliteLot.type_enveloppe '
                   . ' FROM Admissibilite_Enveloppe_papier_Lot as AdmissibiliteLot '
                   . ' Where  AdmissibiliteLot.id_offre_papier=:idOffre AND AdmissibiliteLot.sous_pli in (' . implode(',', $numSousPlis) . ') AND AdmissibiliteLot.organisme=:organisme ';

            $params[':idOffre'] = $enveloppe->getCommonOffrePapier()->getId();
        }
        if ($admissibiliteSuivante) {
            $query .= ' AND type_enveloppe=:typeEnveloppe ';
            $params[':typeEnveloppe'] = $admissibiliteSuivante;
        }
        $params[':organisme'] = $organisme;

        $statement = Atexo_Db::getLinkCommon(true)->prepare($query);
        $statement->execute($params);

        $lotsAdmis = 0;
        $lotsNonAdmis = 0;
        $arrayAdmission = [];
        $indiceArrayAdmission = 0;

        if (0 == $nombreLot) {
            $titreConsultation = $consultation->getIntituleTraduit();
        } else {
            $arrayDescriptionLot = (new Atexo_Consultation_Lots())->retrieveArrayIntituleLot($consultation->getId(), $organisme);
        }
        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            $arrayAdmission[$indiceArrayAdmission]['sousPli'] = $row['sous_pli'];
            $arrayAdmission[$indiceArrayAdmission]['admissibilite'] = $row['admissibilite'];
            $arrayAdmission[$indiceArrayAdmission]['commentaire'] = $row['commentaire'];
            $arrayAdmission[$indiceArrayAdmission]['offreCrypte'] = ('1' == $arrayCryptage[$row['sous_pli']]) ? true : false;
            $arrayAdmission[$indiceArrayAdmission]['typeEnveloppe'] = $row['type_enveloppe'];
            if (0 == $nombreLot) {
                $arrayAdmission[$indiceArrayAdmission]['descriptionLot'] = $titreConsultation;
            } else {
                $arrayAdmission[$indiceArrayAdmission]['descriptionLot'] = $arrayDescriptionLot[$row['sous_pli']];
            }

            unset($numSousPlis[$row['sous_pli']]);

            ++$indiceArrayAdmission;

            if ($row['admissibilite'] == Atexo_Config::getParameter('LOT_ADMISSIBLE')) {
                ++$lotsAdmis;
            } elseif ($row['admissibilite'] == Atexo_Config::getParameter('LOT_NON_ADMISSIBLE')) {
                ++$lotsNonAdmis;
            }
        }
        foreach ($numSousPlis as $sousPli) {
            $arrayAdmission[$indiceArrayAdmission]['sousPli'] = $sousPli;
            $arrayAdmission[$indiceArrayAdmission]['admissibilite'] = Atexo_Config::getParameter('LOT_A_TRAITER');
            $arrayAdmission[$indiceArrayAdmission]['typeEnveloppe'] = $admissibiliteSuivante;
            $arrayAdmission[$indiceArrayAdmission]['commentaire'] = '';
            $arrayAdmission[$indiceArrayAdmission]['offreCrypte'] = ('1' == $arrayCryptage[$sousPli]) ? true : false;
            if (0 == $nombreLot) {
                $arrayAdmission[$indiceArrayAdmission]['descriptionLot'] = $titreConsultation;
            } else {
                $arrayAdmission[$indiceArrayAdmission]['descriptionLot'] = $arrayDescriptionLot[$sousPli];
            }

            ++$indiceArrayAdmission;
        }
        $lotsATraiter = $nombreOffreDeposer - ($lotsAdmis + $lotsNonAdmis);
        // Trier par lot
        $arrayAdmission = Atexo_Util::sortmddata($arrayAdmission, 'sousPli', 'ASC', 'num');

        if ($typeDepot == Atexo_Config::getParameter('TYPE_DEPOT_QUESTION_ELECTRONIQUE')) {
            $enveloppe->getCommonOffres()->setNombreLotsAdmissibles($lotsAdmis);
            $enveloppe->getCommonOffres()->setNombreLotsNonAdmissibles($lotsNonAdmis);
            $enveloppe->getCommonOffres()->setNombreLotsATraiter($lotsATraiter);
            $enveloppe->getCommonOffres()->setMoreThanOneLot(!(0 == $nombreLot));
            foreach ($arrayAdmission as $admissibilite) {
                $commonAdmissibiliteLot = new CommonAdmissibiliteEnveloppeLot();
                $commonAdmissibiliteLot->setIdOffre($enveloppe->getCommonOffres()->getId());
                $commonAdmissibiliteLot->setSousPli($admissibilite['sousPli']);
                $commonAdmissibiliteLot->setAdmissibilite($admissibilite['admissibilite']);
                $commonAdmissibiliteLot->setCommentaire($admissibilite['commentaire']);
                $commonAdmissibiliteLot->setIntiuleLot($admissibilite['descriptionLot']);
                $commonAdmissibiliteLot->setOffreCrypte($admissibilite['offreCrypte']);
                $commonAdmissibiliteLot->setTypeEnveloppe($admissibilite['typeEnveloppe']);
                $commonAdmissibiliteLot->setOrganisme($admissibilite['organisme']);
                $enveloppe->addAdmissibiliteLot($commonAdmissibiliteLot);
            }
        } else {
            $enveloppe->getCommonOffrePapier()->setNombreLotsAdmissibles($lotsAdmis);
            $enveloppe->getCommonOffrePapier()->setNombreLotsNonAdmissibles($lotsNonAdmis);
            $enveloppe->getCommonOffrePapier()->setNombreLotsATraiter($lotsATraiter);
            $enveloppe->getCommonOffrePapier()->setMoreThanOneLot(!(0 == $nombreLot));
            foreach ($arrayAdmission as $admissibilite) {
                $commonAdmissibiliteLot = new CommonAdmissibiliteEnveloppePapierLot();
                $commonAdmissibiliteLot->setIdOffrePapier($enveloppe->getCommonOffrePapier()->getId());
                $commonAdmissibiliteLot->setSousPli($admissibilite['sousPli']);
                $commonAdmissibiliteLot->setAdmissibilite($admissibilite['admissibilite']);
                $commonAdmissibiliteLot->setCommentaire($admissibilite['commentaire']);
                $commonAdmissibiliteLot->setIntiuleLot($admissibilite['descriptionLot']);
                $commonAdmissibiliteLot->setOffreCrypte($admissibilite['offreCrypte']);
                $commonAdmissibiliteLot->setOrganisme($admissibilite['organisme']);
                //ADD TO AdmissibiliteEnveloppePapierLot Class
                $enveloppe->addAdmissibiliteLot($commonAdmissibiliteLot);
            }
        }
    }

    /*
     * permet de calculculer la statistique des avis CAO pour un enveloppe et consultation donnés
     */
    public function retrieveStatisticCAO($idEnveloppe, $oneObject, $typeDepot, $consultationId, $organisme, $arrayIdsGuests = null)
    {
        $resultat = '';
        $listeOffres = $oneObject->getCollAdmissibiliteLot();
        foreach ($listeOffres as $offre) {
            $favorable = 0;
            $defavorable = 0;
            $aTraiter = 0;
            if (is_array($arrayIdsGuests) && count($arrayIdsGuests)) {
                foreach ($arrayIdsGuests as $idGuest) {
                    $avisAgent = (new Atexo_AvisMembresCAO())->retrieveAvisMembresCAOByAgent($idGuest, $idEnveloppe, $offre->getSousPli(), $typeDepot);
                    if ($avisAgent instanceof CommonAvisMembresCAO) {
                        $admis = $avisAgent->getAdmissibilite();
                        if ($admis == Atexo_Config::getParameter('LOT_ADMISSIBLE')) {
                            ++$favorable;
                        } elseif ($admis == Atexo_Config::getParameter('LOT_NON_ADMISSIBLE')) {
                            ++$defavorable;
                        } elseif ($admis == Atexo_Config::getParameter('LOT_A_TRAITER')) {
                            ++$aTraiter;
                        }
                    } else {
                        ++$aTraiter;
                    }
                }
            }
            if (0 != $offre->getSousPli()) {
                $resultat .= Prado::localize('TEXT_LOT') . ' ' . $offre->getSousPli() . ' : ';
            }
            $resultat .= $favorable . ' ' . Prado::localize('TEXT_AVIS_FAVORABLES') . ' / ' . $defavorable . ' ' . Prado::localize('TEXT_AVIS_DEFAVORABLES') . ' / ' .
                            $aTraiter . ' ' . Prado::localize('TEXT_AVIS_NON_RENSEIGNE') . '<br/>';
        }

        return $resultat;
    }

    public function retrieveEnveloppesOffresElectroniques($criteriaResponse)
    {
        if (
            null !== $criteriaResponse->getTypesEnveloppes() && !self::enveloppeCandidatureExists($criteriaResponse->getTypesEnveloppes()) &&
            !self::enveloppeOffreExists($criteriaResponse->getTypesEnveloppes()) &&
            !self::enveloppeAnonymatExists($criteriaResponse->getTypesEnveloppes()) &&
            !self::enveloppeOffreTechniqueExists($criteriaResponse->getTypesEnveloppes())
        ) {
            throw new Atexo_Exception('Unknown typesEnveloppes ');
        }
        $arrayTri = [];
        if ($criteriaResponse->getTrier() && $criteriaResponse->getObjet()) {
            $arrayTri['tri'] = $criteriaResponse->getObjet() . '.' . $criteriaResponse->getTrier();
            $arrayTri['sens'] = $criteriaResponse->getSensTri();
        }
        $enveloppes = self::retrieveEnveloppesOffreElectroniquesAdmissible(
            $criteriaResponse->getReference(),
            $criteriaResponse->getSousPli(),
            $criteriaResponse->getTypesEnveloppes(),
            false,
            $criteriaResponse->getReturnReponses(),
            $criteriaResponse->getOrganisme(),
            $criteriaResponse->getEnveloppeStatutOuverte(),
            $criteriaResponse->getAdmissible(),
            $criteriaResponse->getLimit(),
            $criteriaResponse->getOffset(),
            $arrayTri
        );

        if ($enveloppes) {
            self::addLibelleStatut($enveloppes);
            if (null !== $criteriaResponse->getTypesEnveloppes() || $criteriaResponse->getNomEntrepriseVisible()) {
                self::addCompanyName($enveloppes, $criteriaResponse->getTypesEnveloppes(), $criteriaResponse->getOrganisme(), $criteriaResponse->getNomEntrepriseVisible());
            }
            if ($criteriaResponse->getTrier()) {
                self::sortByElement($enveloppes, $criteriaResponse->getTrier(), $criteriaResponse->getSensTri());
            }

            return $enveloppes;
        } else {
            return [];
        }
    }

    private function retrieveEnveloppesOffreElectroniquesAdmissible($reference, $sousPli = null, $typesEnveloppes = null, $searchByIdOffre = false, $returnOffreWithinEnveloppe = true, $organisme = null, $enveloppeStatutOuverte = false, $admissible = true, $limit = null, $offset = null, $arrayTri = null)
    {
        if (!$organisme) {
            $organisme = Atexo_CurrentUser::getCurrentOrganism();
        }
        $params = [];
        if (null !== $typesEnveloppes && (self::enveloppeCandidatureExists($typesEnveloppes) || self::enveloppeOffreTechniqueExists($typesEnveloppes)) && $admissible) {
            $query = ' SELECT Offres.consultation_id as consultationId , Offres.organisme as org, Offres.* ' . (($returnOffreWithinEnveloppe) ? ', Enveloppe.* ' : ', decisionEnveloppe.* ')
            . ' FROM Offres ' . (($returnOffreWithinEnveloppe) ? '' : ' left join  decisionEnveloppe on (Offres.id=decisionEnveloppe.id_offre AND Offres.organisme=decisionEnveloppe.organisme ' .
            ((!$searchByIdOffre) ? 'AND decisionEnveloppe.consultation_id=:idRefOrOffre' : 'AND decisionEnveloppe.id_offre=:idRefOrOffre') . ' AND lot=' . $sousPli . " AND decisionEnveloppe.type_enveloppe='"
            . Atexo_Config::getParameter('DEPOT_ELECTRONIQUE') . "') AND decisionEnveloppe.organisme=:organisme ") . ',Enveloppe ,Admissibilite_Enveloppe_Lot as Adm '
            . ' WHERE Offres.id = Enveloppe.offre_id AND Offres.organisme=Enveloppe.organisme ' . ($searchByIdOffre ? ' AND Offres.id=:idRefOrOffre ' : ' AND Offres.consultation_id=:idRefOrOffre ')
            . ' AND Offres.id=Adm.id_Offre AND Adm.admissibilite=:admis1 AND Offres.organisme=Adm.organisme AND Enveloppe.type_env =:typeEnvOffre AND Adm.type_enveloppe=:admissibiliteEnv ';
            if (null !== $sousPli) {
                $query .= '    AND Enveloppe.sous_pli=:sousPli AND Adm.sous_pli=:sousPli ';
            } elseif (0 !== $sousPli) {
                $query .= '    AND Enveloppe.sous_pli=Adm.sous_pli ';
            }
            if ($enveloppeStatutOuverte) {
                $query .= ' AND ( Enveloppe.statut_enveloppe=:ouverte1  OR '
                      . '		Enveloppe.statut_enveloppe=:ouverteEnLigne  OR '
                      . '		Enveloppe.statut_enveloppe=:ouverteHorsLigne  OR '
                      . '		Enveloppe.statut_enveloppe=:ouverteADistance  ) ';
                $params[':ouverte1'] = Atexo_Config::getParameter('STATUT_ENV_OUVERTE');
                $params[':ouverteEnLigne'] = Atexo_Config::getParameter('STATUT_ENV_OUVERTE_EN_LIGNE');
                $params[':ouverteHorsLigne'] = Atexo_Config::getParameter('STATUT_ENV_OUVERTE_HORS_LIGNE');
                $params[':ouverteADistance'] = Atexo_Config::getParameter('STATUT_ENV_OUVERTE_A_DISTANCE');
            }
            ///still getting problems
            $query .= ' AND depot_annule=:depotNonAnnule ';
            $query .= ' AND Offres.organisme=:organisme ';
            $query .= ' AND Offres.statut_offres != :statutBrouillon AND Enveloppe.statut_enveloppe != :statutBrouillon';
            //          $query.=" ORDER BY untrusteddate ASC ";
            $params[':idRefOrOffre'] = $reference;
            $params[':admis1'] = Atexo_Config::getParameter('LOT_ADMISSIBLE');
            $params[':typeEnvOffre'] = Atexo_Config::getParameter('TYPE_ENV_OFFRE');
            $params[':admissibiliteEnv'] = Atexo_Config::getParameter('TYPE_ENV_OFFRE');
            $params[':depotNonAnnule'] = Atexo_Config::getParameter('DEPOT_NON_ANNULE');
            $params[':organisme'] = $organisme;
            $params[':statutBrouillon'] = Atexo_Config::getParameter('STATUT_ENV_BROUILLON');
            if (strstr($query, ':sousPli')) {
                $params[':sousPli'] = $sousPli;
            }
        } else {
            $query = ' SELECT Offres.consultation_id as consultationId , Offres.organisme as org , Offres.* ' . (($returnOffreWithinEnveloppe) ? ', Enveloppe.* ' : ', decisionEnveloppe.* ')
                   . ' FROM   Offres ' . (($returnOffreWithinEnveloppe) ? '' : ' left join  decisionEnveloppe on (Offres.id=decisionEnveloppe.id_offre AND Offres.organisme=decisionEnveloppe.organisme ' .
                   ((!$searchByIdOffre) ? 'AND decisionEnveloppe.consultation_id =:idRefOrOffre' : 'AND decisionEnveloppe.id_offre=:idRefOrOffre') . ' AND lot=' . $sousPli .
                   " AND decisionEnveloppe.type_enveloppe='" . Atexo_Config::getParameter('DEPOT_ELECTRONIQUE') . "') ") . ', Enveloppe '
                   . ' WHERE  Offres.id = Enveloppe.offre_id  '
                   . ($searchByIdOffre ? ' AND Offres.id=:idRefOrOffre ' : ' AND Offres.consultation_id=:idRefOrOffre ')
                   . '        AND Enveloppe.type_env =:typeEnvOffre ';
            if (null !== $sousPli) {
                $query .= '    AND Enveloppe.sous_pli=:sousPli ';
            }
            if ($enveloppeStatutOuverte) {
                $query .= ' AND ( Enveloppe.statut_enveloppe=:ouverte1 OR ' . '		Enveloppe.statut_enveloppe=:ouverteEnLigne OR '
                  . '		Enveloppe.statut_enveloppe=:ouverteHorsLigne OR ' . '		Enveloppe.statut_enveloppe=:ouverteADistance ) ';
                $params[':ouverte1'] = Atexo_Config::getParameter('STATUT_ENV_OUVERTE');
                $params[':ouverteEnLigne'] = Atexo_Config::getParameter('STATUT_ENV_OUVERTE_EN_LIGNE');
                $params[':ouverteHorsLigne'] = Atexo_Config::getParameter('STATUT_ENV_OUVERTE_HORS_LIGNE');
                $params[':ouverteADistance'] = Atexo_Config::getParameter('STATUT_ENV_OUVERTE_A_DISTANCE');
            }
            $query .= ' AND Offres.depot_annule=:depotNonAnnule AND Offres.organisme=Enveloppe.organisme AND Offres.organisme=:organisme ';
            $query .= ' AND Offres.statut_offres != :statutBrouillon AND Enveloppe.statut_enveloppe != :statutBrouillon';
            //          $query.=" ORDER BY untrusteddate ASC ";
            $params[':idRefOrOffre'] = $reference;
            $params[':typeEnvOffre'] = Atexo_Config::getParameter('TYPE_ENV_OFFRE');
            $params[':depotNonAnnule'] = Atexo_Config::getParameter('DEPOT_NON_ANNULE');
            $params[':organisme'] = $organisme;
            $params[':statutBrouillon'] = Atexo_Config::getParameter('STATUT_ENV_BROUILLON');
            if (strstr($query, ':sousPli')) {
                $params[':sousPli'] = $sousPli;
            }
        }
        if (is_array($arrayTri) && count($arrayTri)) {
            $trier = $arrayTri['tri'];
            $sensTri = $arrayTri['sens'];
            $query .= ' ORDER BY ' . $trier . ' ' . $sensTri;
        } else {
            $query .= ' ORDER BY untrusteddate ASC ';
        }
        if ($limit && $offset) {
            $query .= ' limit ' . $offset . ',' . $limit;
        }
        $statement = Atexo_Db::getLinkCommon(true)->prepare($query);
        $statement->execute($params);
        $arraysEnveloppes = [];
        $arrayIds = [];
        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            if ($returnOffreWithinEnveloppe) {
                $id = $row['id_enveloppe_electro'];
            } else {
                $id = $row['id'];
            }
            if (!isset($arrayIds[$id])) {
                $enveloppe = self::fillObjectFromArray($row, $returnOffreWithinEnveloppe);
                $arraysEnveloppes[] = $enveloppe;
                $arrayIds[$id] = true;
            }
        }

        return $arraysEnveloppes;
    }

    public function retrieveEnveloppesOffresTechniqueElectroniques($reference, $sousPli, $constitutionDossierReponse, $trier, $sensTri, $organisme, $returnReponses = true, $nomEntrepriseVisible = false, $enveloppeStatutOuverte = false, $admissible = true, $limit = null, $offset = null, $objet = null, $arrayIdsGuests = null)
    {
        if (
            null !== $constitutionDossierReponse && !self::enveloppeCandidatureExists($constitutionDossierReponse) &&
            !self::enveloppeOffreExists($constitutionDossierReponse) &&
            !self::enveloppeAnonymatExists($constitutionDossierReponse) &&
            !self::enveloppeOffreTechniqueExists($constitutionDossierReponse)
        ) {
            throw new Atexo_Exception('Unknown typesEnveloppes ');
        }
        $arrayTri = [];
        if ($trier && $objet) {
            $arrayTri['tri'] = $objet . '.' . $trier;
            $arrayTri['sens'] = $sensTri;
        }
        $enveloppes = self::retrieveEnveloppesOffreTechniqueElectroniquesAdmissible(
            $reference,
            $sousPli,
            $constitutionDossierReponse,
            false,
            $returnReponses,
            $organisme,
            $enveloppeStatutOuverte,
            $admissible,
            $limit,
            $offset,
            $arrayTri
        );
        if ($enveloppes) {
            self::addAdmissibility($enveloppes, $reference, $organisme, Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE'), $sousPli);

            self::addLibelleStatut($enveloppes);
            if (null !== $constitutionDossierReponse || $nomEntrepriseVisible) {
                self::addCompanyName($enveloppes, $constitutionDossierReponse, $organisme, $nomEntrepriseVisible);
            }
            if ($trier) {
                self::sortByElement($enveloppes, $trier, $sensTri);
            }
            if (Atexo_Module::isEnabled('AvisMembresCommision')) {
                self::addStatistiqueAvisCAO($reference, $organisme, $enveloppes, $arrayIdsGuests);
            }

            return $enveloppes;
        } else {
            return [];
        }
    }

    //TODO : appel de la fct des offres financières
    private function retrieveEnveloppesOffreTechniqueElectroniquesAdmissible($reference, $sousPli = null, $constitutionDossierReponse = null, $searchByIdOffre = false, $returnOffreWithinEnveloppe = true, $organisme = null, $enveloppeStatutOuverte = false, $admissible = true, $limit = null, $offset = null, $arrayTri = null)
    {
        if (!$organisme) {
            $organisme = Atexo_CurrentUser::getCurrentOrganism();
        }
        $params = [];
        if (null !== $constitutionDossierReponse && self::enveloppeCandidatureExists($constitutionDossierReponse) && $admissible) {
            $query = ' SELECT Offres.consultation_id as consultationId ,Offres.* ' . (($returnOffreWithinEnveloppe) ? ', Enveloppe.* ' : ', decisionEnveloppe.* ')
                . ' FROM Offres ' . (($returnOffreWithinEnveloppe) ? '' : ' left join  decisionEnveloppe on (Offres.id=decisionEnveloppe.id_offre AND Offres.organisme=decisionEnveloppe.organisme '
                . ((!$searchByIdOffre) ? 'AND decisionEnveloppe.consultation_id =:idRefOrOffre' : 'AND decisionEnveloppe.id_offre=:idRefOrOffre') . ' AND lot=' . $sousPli
                . " AND decisionEnveloppe.type_enveloppe='" . Atexo_Config::getParameter('DEPOT_ELECTRONIQUE') . "') ") . ', Enveloppe , Admissibilite_Enveloppe_Lot as Adm '
                . ' WHERE  Offres.id = Enveloppe.offre_id ' . ($searchByIdOffre ? ' AND Offres.id=:idRefOrOffre ' : ' AND Offres.consultation_id=:idRefOrOffre ')
                . " AND Offres.id=Adm.id_Offre AND Adm.admissibilite=:admis AND Adm.type_enveloppe='" . Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE') . "' AND Enveloppe.type_env =:typeEnvOffre "
                . ' AND Adm.sous_pli=Enveloppe.sous_pli' . (($returnOffreWithinEnveloppe) ? ' ' : " AND decisionEnveloppe.type_enveloppe='" . Atexo_Config::getParameter('DEPOT_ELECTRONIQUE') . "' ");
            if (null !== $sousPli) {
                $query .= '    AND Enveloppe.sous_pli=:sousPli '
                    . ' 	  AND Adm.sous_pli=:sousPli ';
            } elseif ($sousPli) {
                $query .= ' 	  AND Adm.sous_pli=:sousPli ';
            }
            if ($enveloppeStatutOuverte) {
                $query .= ' AND ( Enveloppe.statut_enveloppe=:ouverte OR ' . '		Enveloppe.statut_enveloppe=:ouverteEnLigne OR '
                          . '		Enveloppe.statut_enveloppe=:ouverteHorsLigne OR ' . '		Enveloppe.statut_enveloppe=:ouverteADistance ) ';
                $params[':ouverte'] = Atexo_Config::getParameter('STATUT_ENV_OUVERTE');
                $params[':ouverteEnLigne'] = Atexo_Config::getParameter('STATUT_ENV_OUVERTE_EN_LIGNE');
                $params[':ouverteHorsLigne'] = Atexo_Config::getParameter('STATUT_ENV_OUVERTE_HORS_LIGNE');
                $params[':ouverteADistance'] = Atexo_Config::getParameter('STATUT_ENV_OUVERTE_A_DISTANCE');
            }
            $query .= ' AND depot_annule=:depotNonAnnule AND Offres.organisme = Enveloppe.organisme AND Offres.organisme=Adm.organisme  AND Offres.organisme=:organisme ';
            $query .= ' AND Offres.statut_offres != :statutBrouillon AND Enveloppe.statut_enveloppe != :statutBrouillon';
            $params[':idRefOrOffre'] = $reference;
            $params[':admis'] = Atexo_Config::getParameter('LOT_ADMISSIBLE');
            $params[':typeEnvOffre'] = Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE');
            $params[':depotNonAnnule'] = Atexo_Config::getParameter('DEPOT_NON_ANNULE');
            $params[':organisme'] = $organisme;
            $params[':statutBrouillon'] = Atexo_Config::getParameter('STATUT_ENV_BROUILLON');
            if (strstr($query, ':sousPli')) {
                $params[':sousPli'] = $sousPli;
            }
        } else {
            $query = ' SELECT Offres.consultation_id as consultationId , Offres.* ' . (($returnOffreWithinEnveloppe) ? ', Enveloppe.*  ' : ', decisionEnveloppe.* ')
                . ' FROM   Offres ' . (($returnOffreWithinEnveloppe) ? '' : ' left join  decisionEnveloppe on (Offres.id=decisionEnveloppe.id_offre AND Offres.organisme=decisionEnveloppe.organisme '
                . ((!$searchByIdOffre) ? 'AND decisionEnveloppe.consultation_id =:idRefOrOffre' : 'AND decisionEnveloppe.id_offre=:idRefOrOffre') . ' AND lot=' . $sousPli
                . "  AND decisionEnveloppe.type_enveloppe='" . Atexo_Config::getParameter('DEPOT_ELECTRONIQUE') . "') ") . ', Enveloppe '
                . ' WHERE Offres.id=Enveloppe.offre_id ' . ($searchByIdOffre ? ' AND Offres.id=:idRefOrOffre ' : ' AND Offres.consultation_id=:idRefOrOffre ') . ' AND Enveloppe.type_env=:typeEnvOffre ';

            if (null !== $sousPli) {
                $query .= '    AND Enveloppe.sous_pli=:sousPli';
            }
            if ($enveloppeStatutOuverte) {
                $query .= ' AND ( Enveloppe.statut_enveloppe=:ouverte OR Enveloppe.statut_enveloppe=:ouverteEnLigne OR '
                          . '		Enveloppe.statut_enveloppe=:ouverteHorsLigne OR Enveloppe.statut_enveloppe=:ouverteADistance ) ';

                $params[':ouverte'] = Atexo_Config::getParameter('STATUT_ENV_OUVERTE');
                $params[':ouverteEnLigne'] = Atexo_Config::getParameter('STATUT_ENV_OUVERTE_EN_LIGNE');
                $params[':ouverteHorsLigne'] = Atexo_Config::getParameter('STATUT_ENV_OUVERTE_HORS_LIGNE');
                $params[':ouverteADistance'] = Atexo_Config::getParameter('STATUT_ENV_OUVERTE_A_DISTANCE');
            }
            $query .= ' AND depot_annule=:depotNonAnnule AND Offres.organisme = Enveloppe.organisme AND Offres.organisme=:organisme ';
            $query .= ' AND Offres.statut_offres != :statutBrouillon AND Enveloppe.statut_enveloppe != :statutBrouillon';
            $params[':idRefOrOffre'] = $reference;
            $params[':typeEnvOffre'] = Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE');
            $params[':depotNonAnnule'] = Atexo_Config::getParameter('DEPOT_NON_ANNULE');
            $params[':organisme'] = $organisme;
            $params[':statutBrouillon'] = Atexo_Config::getParameter('STATUT_ENV_BROUILLON');
            if (strstr($query, ':sousPli')) {
                $params[':sousPli'] = $sousPli;
            }
        }
        if (is_array($arrayTri) && count($arrayTri)) {
            $trier = $arrayTri['tri'];
            $sensTri = $arrayTri['sens'];
            $query .= ' ORDER BY ' . $trier . ' ' . $sensTri;
        } else {
            $query .= ' ORDER BY untrusteddate ASC ';
        }
        if ($limit && $offset) {
            $query .= ' limit ' . $offset . ',' . $limit;
        }
        $statement = Atexo_Db::getLinkCommon(true)->prepare($query);
        $statement->execute($params);
        $arraysEnveloppes = [];
        $arrayIds = [];
        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            if ($returnOffreWithinEnveloppe) {
                $id = $row['id_enveloppe_electro'];
            } else {
                $id = $row['id'];
            }
            if (!isset($arrayIds[$id])) {
                $enveloppe = self::fillObjectFromArray($row, $returnOffreWithinEnveloppe);
                $arraysEnveloppes[] = $enveloppe;
                $arrayIds[$id] = true;
            }
        }

        return $arraysEnveloppes;
    }

    private static function fillObjectFromArray($arrayData, $returnEnveloppe = true)
    {
        if (!is_array($arrayData)) {
            throw new Atexo_Exception('Expected Parameter to be an array');
        }
        $commonOffres = new CommonOffres();
        $commonOffres->setId($arrayData['id']);
        $commonOffres->setConsultationId($arrayData['consultationId'] ?? $arrayData['consultation_id']);
        $commonOffres->setEntrepriseId($arrayData['entreprise_id']);
        $commonOffres->setIdEtablissement($arrayData['id_etablissement']);
        $commonOffres->setInscritId($arrayData['inscrit_id']);
        $commonOffres->setSignatureenvxml($arrayData['signatureenvxml']);
        $commonOffres->setHorodatage($arrayData['horodatage']);
        $commonOffres->setMailsignataire($arrayData['mailsignataire']);
        $commonOffres->setUntrusteddate($arrayData['untrusteddate']);
        $commonOffres->setUntrustedserial($arrayData['untrustedserial']);
        $commonOffres->setEnvoiComplet($arrayData['envoi_complet']);
        $commonOffres->setDateDepotDiffere($arrayData['date_depot_differe']);
        $commonOffres->setHorodatageEnvoiDiffere($arrayData['horodatage_envoi_differe']);
        $commonOffres->setSignatureenvxmlEnvoiDiffere($arrayData['signatureenvxml_envoi_differe']);
        $commonOffres->setExternalSerial($arrayData['external_serial']);
        $commonOffres->setInternalSerial($arrayData['internal_serial']);
        $commonOffres->setUidOffre($arrayData['uid_offre']);
        $commonOffres->setOffreSelectionnee($arrayData['offre_selectionnee']);
        $commonOffres->setNomEntrepriseInscrit($arrayData['nom_entreprise_inscrit']);
        $commonOffres->setNomEntreprise($arrayData['nom_entreprise']);
        $commonOffres->setOffreVariante($arrayData['offre_variante']);
        $commonOffres->setSiretEntreprise($arrayData['siret_entreprise']);
        $commonOffres->setSiretInscrit($arrayData['siret_inscrit']);
        $commonOffres->setVilleInscrit($arrayData['ville_inscrit']);
        $commonOffres->setCodePostalInscrit($arrayData['code_postal_inscrit']);
        $commonOffres->setAdresseInscrit($arrayData['adresse_inscrit']);
        $commonOffres->setObservation($arrayData['Observation']);
        $commonOffres->setPaysInscrit($arrayData['pays_inscrit']);
        $commonOffres->setIdentifiantNational($arrayData['identifiant_national']);
        $commonOffres->setOrganisme($arrayData['org'] ?? $arrayData['organisme']);
        $commonOffres->setNumPli($arrayData['numero_reponse']);
        $commonOffres->setCandidatureIdExterne($arrayData['candidature_id_externe']);

        $commonOffres->setNom($arrayData['nom_inscrit']);
        $commonOffres->setPrenom($arrayData['prenom_inscrit']);
        $commonOffres->setEmail($arrayData['email_inscrit']);
        $commonOffres->setTelephone($arrayData['telephone_inscrit']);
        $commonOffres->setFax($arrayData['fax_inscrit']);
        $commonOffres->setSiret($arrayData['siret_inscrit']);
        $commonOffres->setVille($arrayData['ville_inscrit']);
        //$commonOffres->setCodePostal($arrayData['code_postal_inscrit']);
        $commonOffres->setAdresse($arrayData['adresse_inscrit']);
        $commonOffres->setAdresse2($arrayData['adresse2_inscrit']);
        $commonOffres->setPays($arrayData['pays_inscrit']);
        $commonOffres->setHasGroupement((new Atexo_Groupement())->getGroupementByIdOffre($arrayData['id'], false));
        $commonOffres->setHasMembreGroupement((new Atexo_Groupement())->retrieveMemebreGroupement($arrayData['id']));
        $commonOffres->setNombreCotraitant((new Atexo_Groupement())->retrieveMemebreGroupement($arrayData['id'], Atexo_Config::getParameter('ID_ROLE_JURIDIQUE_CO_TRAITANT')));
        $commonOffres->setNombreSoustraitant((new Atexo_Groupement())->getNombreSousMemebreGroupement($arrayData['id']));
        $commonOffres->setStatutOffres($arrayData['statut_offres']);
        $commonOffres->setVerificationSignatureOffre($arrayData['verification_signature_offre']);
        $commonOffres->setResultatVerificationHashAllFiles($arrayData['resultat_verification_hash_all_files']);

        if (!$returnEnveloppe) {
            $commonOffres->setCodePostal($arrayData['code_postal_inscrit']);
            $commonDecisionEnveloppe = new CommonDecisionEnveloppe();
            $commonDecisionEnveloppe->setIdDecisionEnveloppe($arrayData['id_decision_enveloppe']);
            $commonDecisionEnveloppe->setConsultationId($arrayData['consultationId'] ?? $arrayData['consultation_id']);
            $commonDecisionEnveloppe->setIdOffre($arrayData['id_offre']);
            $commonDecisionEnveloppe->setLot($arrayData['lot']);
            $commonDecisionEnveloppe->setDecision($arrayData['decision']);
            $commonDecisionEnveloppe->setDateNotification($arrayData['date_notification']);
            $commonDecisionEnveloppe->setCodePostal($arrayData['code_postal']);
            $commonDecisionEnveloppe->setVille($arrayData['ville']);
            $commonDecisionEnveloppe->setPmePmi($arrayData['pme_pmi']);
            $commonDecisionEnveloppe->setTrancheBudgetaire($arrayData['tranche_budgetaire']);
            $commonDecisionEnveloppe->setMontantMarche($arrayData['montant_marche']);
            $commonDecisionEnveloppe->setCategorie($arrayData['categorie']);
            $commonDecisionEnveloppe->setObjetMarche($arrayData['objet_marche']);
            $commonDecisionEnveloppe->setCommentaire($arrayData['commentaire']);
            $commonDecisionEnveloppe->setFichierJoint($arrayData['fichier_joint']);
            $commonDecisionEnveloppe->setNomFichierJoint($arrayData['nom_fichier_joint']);
            $commonDecisionEnveloppe->setNumeroMarche($arrayData['numero_marche']);
            $commonDecisionEnveloppe->setNote($arrayData['note']);
            $commonDecisionEnveloppe->setClassement($arrayData['classement']);
            $commonDecisionEnveloppe->setOrganisme($arrayData['org'] ?? $arrayData['organisme']);
            $commonDecisionEnveloppe->setSirenAttributaire($arrayData['siren_Attributaire']);
            $commonDecisionEnveloppe->setNicAttributaire($arrayData['nic_Attributaire']);
            $commonDecisionEnveloppe->setRcVilleAttributaire($arrayData['rc_ville_attributaire']);
            $commonDecisionEnveloppe->setRcNumAttributaire($arrayData['rc_num_attributaire']);
            $commonDecisionEnveloppe->setIdentifiantnationalAttributaire($arrayData['identifiantNational_Attributaire']);
            $commonDecisionEnveloppe->setAcronymepaysAttributaire($arrayData['acronymePays_Attributaire']);
            $commonDecisionEnveloppe->setPaysAttributaire($arrayData['pays_Attributaire']);
            $commonDecisionEnveloppe->setDateFinMarchePrevisionnel($arrayData['date_fin_marche_previsionnel']);
            $commonDecisionEnveloppe->setDateFinMarcheReelle($arrayData['date_fin_marche_reelle']);
            $commonDecisionEnveloppe->setDateNotificationReelle($arrayData['date_notification_reelle']);
            $commonOffres->setCommonDecisionEnveloppe($commonDecisionEnveloppe);

            return $commonOffres;
        }
        $commonEnveloppe = new CommonEnveloppe();
        $commonEnveloppe->setIdEnveloppeElectro($arrayData['id_enveloppe_electro']);
        $commonEnveloppe->setNumPli($arrayData['numero_reponse']);
        $commonEnveloppe->setOffreId($arrayData['offre_id']);
        $commonEnveloppe->setChampsOptionnels($arrayData['champs_optionnels']);
        $commonEnveloppe->setFichier($arrayData['fichier']);
        $commonEnveloppe->setSupprime($arrayData['supprime']);
        $commonEnveloppe->setCryptage($arrayData['cryptage']);
        $commonEnveloppe->setNomFichier($arrayData['nom_fichier']);
        $commonEnveloppe->setHash($arrayData['hash']);
        $commonEnveloppe->setTypeEnv($arrayData['type_env']);
        $commonEnveloppe->setSousPli($arrayData['sous_pli']);
        $commonEnveloppe->setAttribue($arrayData['attribue']);
        $commonEnveloppe->setDateheureOuverture($arrayData['dateheure_ouverture']);
        $commonEnveloppe->setAgentIdOuverture($arrayData['agent_id_ouverture']);
        $commonEnveloppe->setAgentIdOuverture2($arrayData['agent_id_ouverture2']);
        $commonEnveloppe->setDateheureOuvertureAgent2($arrayData['dateheure_ouverture_agent2']);
        $commonEnveloppe->setDonneesOuverture($arrayData['donnees_ouverture']);
        $commonEnveloppe->setHorodatageDonneesOuverture($arrayData['horodatage_donnees_ouverture']);
        $commonEnveloppe->setStatutEnveloppe($arrayData['statut_enveloppe']);
        $commonEnveloppe->setAgentTelechargement($arrayData['agent_telechargement']);
        $commonEnveloppe->setDateTelechargement($arrayData['date_telechargement']);
        $commonEnveloppe->setRepertoireTelechargement($arrayData['repertoire_telechargement']);
        $commonEnveloppe->setNomAgentOuverture($arrayData['nom_agent_ouverture']);
        $commonEnveloppe->setOrganisme($arrayData['organisme']);
        $commonEnveloppe->setOrganisme($arrayData['org'] ?? $arrayData['organisme']);
        $commonEnveloppe->setLibelleStatutEnveloppe((new Atexo_Consultation_StatutEnveloppe())->getStatutEnveloppeById($commonEnveloppe->getStatutEnveloppe()));
        $commonEnveloppe->setEnveloppeFictive($arrayData['enveloppe_fictive']);
        $commonEnveloppe->setVerificationSignature($arrayData['verification_signature']);
        $commonEnveloppe->setResultatVerificationHashFiles($arrayData['resultat_verification_hash_files']);
        $commonEnveloppe->setIdDossierVolumineux($arrayData['id_dossier_volumineux']);
        $commonEnveloppe->setCommonOffres($commonOffres);

        return $commonEnveloppe;
    }

    public function retrieveEnveloppeOffreElectroniqueAdmissibleByIdOffre($idOffre, $typesEnveloppes)
    {
        return self::retrieveEnveloppesOffreElectroniquesAdmissible($idOffre, null, $typesEnveloppes, true, true);
    }

    public function addCompanyName(&$object, $typesEnveloppes = null, $organisme = null, $nomEntrepriseVisible = false)
    {
        foreach ($object as $oneObject) {
            if ($oneObject instanceof CommonEnveloppe) {
                if ($oneObject->getCommonOffres() instanceof CommonOffres) {
                    $oneObject->getCommonOffres()->setNomEntreprise(self::retrieveCompanyName($oneObject, $typesEnveloppes, $organisme, $nomEntrepriseVisible));
                }
            } elseif ($oneObject instanceof CommonEnveloppePapier) {
                if ($oneObject->getCommonOffrePapier() instanceof CommonOffrePapier) {
                    $oneObject->getCommonOffrePapier()->setNomEntreprise(self::retrieveCompanyName($oneObject, $typesEnveloppes, $organisme, $nomEntrepriseVisible));
                }
            } elseif ($oneObject  instanceof CommonOffrePapier || $oneObject  instanceof CommonOffres) {
                $oneObject->setNomEntreprise(self::retrieveCompanyName($oneObject, $typesEnveloppes, $organisme, $nomEntrepriseVisible));
            } elseif ($oneObject instanceof CommonDecisionEnveloppe) {
                $oneObject->setNomEntreprise(self::retrieveNomEntrepriseFromDecision($oneObject, $organisme));
            }
        }
    }

    public function retrieveCompanyName($object, $typesEnveloppes, $organisme, $nomEntrepriseVisible = false)
    {
        $nomEntreprise = null;
        $anonymats = null;
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        if ($object instanceof CommonEnveloppe) {
            $nomEntreprise = $object->getOffre($connexion)->getNomEntrepriseInscrit();
        } elseif ($object instanceof CommonEnveloppePapier) {
            if ($object->getCommonOffrePapier() instanceof CommonOffrePapier) {
                $nomEntreprise = $object->getCommonOffrePapier()->getNomEntreprise();
            }
        } elseif ($object instanceof CommonOffres) {
            return $object->getNomEntrepriseInscrit();
        } elseif ($object instanceof CommonOffrePapier) {
            return $object->getNomEntreprise();
        } else {
            throw new Atexo_Exception('Expected object given to be either Enveloppe or EnveloppePapier instance');
        }
        if ($nomEntrepriseVisible) {
            return $nomEntreprise ?: '-';
        }
        $returnNomEntreprise = true;
        if (self::enveloppeAnonymatExists($typesEnveloppes)) {
            if ($object instanceof CommonEnveloppe) {
                $anonymats = self::retrieveEnveloppeAnonymatByIdOffre($object->getCommonOffres()->getId(), $organisme);
            } elseif ($object instanceof CommonEnveloppePapier) {
                $anonymats = self::retrieveEnveloppesAnonymatPapier($object->getCommonOffrePapier()->getId(), null, null, '', '', $organisme, true);
            }
            if (is_array($anonymats)) {
                $anonymatOuvert = false;
                foreach ($anonymats as $uneEnveloppeAnonymat) {
                    if (
                        $uneEnveloppeAnonymat->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_EN_LIGNE') ||
                           $uneEnveloppeAnonymat->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_HORS_LIGNE') ||
                           $uneEnveloppeAnonymat->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_A_DISTANCE') ||
                           $uneEnveloppeAnonymat->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE')
                    ) {
                        $anonymatOuvert = true;
                        break;
                    }
                }
                if ($anonymatOuvert) {
                    if (Atexo_Module::isEnabled('NomEntrepriseToujoursVisible')) {
                        $returnNomEntreprise = true;
                    } else {
                        if ($object instanceof CommonEnveloppe) {
                            $candidature = self::retrieveEnveloppeCandidatureByIdOffre($object->getOffre($connexion)->getId(), $organisme);
                            $offres = self::retrieveEnveloppeOffreElectroniqueAdmissibleByIdOffre($object->getOffre($connexion)->getId(), $typesEnveloppes);
                        } else {
                            $candidature = self::retrieveEnveloppesCandidaturePapier(
                                $object->getOffrePapier($connexion)->getConsultationId(),
                                $object->getOffrePapier($connexion)->getId(),
                                null,
                                '',
                                '',
                                $organisme,
                                true
                            );
                            if (is_array($candidature) & (is_countable($candidature) ? count($candidature) : 0) > 0) {
                                $candidature = array_shift($candidature);
                            }
                            $offres = self::retrieveEnveloppesOffrePapier($object->getOffrePapier($connexion)->getId(), $typesEnveloppes, null, null, null, $organisme, true, true, true);
                        }
                        if ($candidature) {
                            if (
                                $candidature->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_EN_LIGNE') ||
                                       $candidature->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_HORS_LIGNE') ||
                                       $candidature->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_A_DISTANCE') ||
                                       $candidature->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE')
                            ) {
                                $returnNomEntreprise = true;
                            } else {
                                $returnNomEntreprise = false;
                            }
                        } elseif (is_array($offres)) {
                            foreach ($offres as $offre) {
                                if (
                                    $offre->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_EN_LIGNE') ||
                                       $offre->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_HORS_LIGNE') ||
                                       $offre->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_A_DISTANCE') ||
                                       $offre->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE')
                                ) {
                                    $returnNomEntreprise = true;
                                    break;
                                } else {
                                    $returnNomEntreprise = false;
                                }
                            }
                        } else {
                            if (
                                $object->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_EN_LIGNE') ||
                                   $object->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_HORS_LIGNE') ||
                                   $object->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_A_DISTANCE') ||
                                   $object->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE')
                            ) {
                                $returnNomEntreprise = true;
                            } else {
                                $returnNomEntreprise = false;
                            }
                        }
                    }
                } else {
                    $returnNomEntreprise = false;
                }
            } else {
                throw new Atexo_Exception('Expected function to return an array');
            }
        }
        if (self::enveloppeCandidatureExists($typesEnveloppes)) {
            if (Atexo_Module::isEnabled('NomEntrepriseToujoursVisible') || $nomEntrepriseVisible) {
                return $nomEntreprise ?: '-';
            }
            if ($object->getTypeEnv() == Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE')) {
                if (
                    $object->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_EN_LIGNE') ||
                    $object->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_HORS_LIGNE') ||
                    $object->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_A_DISTANCE') ||
                    $object->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE')
                ) {
                    $returnNomEntreprise = true;
                } else {
                    $returnNomEntreprise = false;
                }
            } elseif ($object->getTypeEnv() == Atexo_Config::getParameter('TYPE_ENV_OFFRE')) {
                if ($object instanceof CommonEnveloppe) {
                    $candidature = self::retrieveEnveloppeCandidatureByIdOffre($object->getOffre($connexion)->getId(), $organisme);
                } else {
                    $candidature = self::retrieveEnveloppesCandidaturePapier(
                        $object->getOffrePapier($connexion)->getConsultationId(),
                        $object->getOffrePapier($connexion)->getId(),
                        null,
                        '',
                        '',
                        $organisme,
                        true
                    );
                    if (is_array($candidature)) {
                        $candidature = array_shift($candidature);
                        if ((!$candidature instanceof CommonEnveloppePapier)) {
                            throw new Atexo_Exception('Expected function to return EnveloppePapierObject');
                        }
                    }
                }
                if ($candidature) {
                    if (
                        $candidature->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_EN_LIGNE') ||
                        $candidature->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_HORS_LIGNE') ||
                        $candidature->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_A_DISTANCE') ||
                        $candidature->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE')
                    ) {
                        $returnNomEntreprise = true;
                    } else {
                        $returnNomEntreprise = false;
                    }
                }
            }
        } else {
            if (Atexo_Module::isEnabled('NomEntrepriseToujoursVisible') || $nomEntrepriseVisible) {
                return $nomEntreprise ?: '-';
            }
            if (
                $object->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_EN_LIGNE') ||
                $object->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_HORS_LIGNE') ||
                $object->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_A_DISTANCE') ||
                $object->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE')
            ) {
                $returnNomEntreprise = true;
            } else {
                $returnNomEntreprise = false;
            }
        }
        if ($returnNomEntreprise) {
            return $nomEntreprise ?: '-';
        }

        return '-';
    }

    public function getTypesEnveloppes($consultaionWithCandidaure, $consultaionWithOffre, $consultaionWithAnonymat, $consultationWithOffreTechnique)
    {
        $typeEnveloppe = 0;
        if ('0' != $consultaionWithAnonymat) {
            $typeEnveloppe += Atexo_Config::getParameter('ANONYMAT_SEUL');
        }

        if ('0' != $consultaionWithOffre) {
            $typeEnveloppe += Atexo_Config::getParameter('OFFRE_SEUL');
        }

        if ('0' != $consultaionWithCandidaure) {
            $typeEnveloppe += Atexo_Config::getParameter('CANDIDATURE_SEUL');
        }

        if ('0' != $consultationWithOffreTechnique) {
            $typeEnveloppe += Atexo_Config::getParameter('OFFRE_TECHNIQUE_SEUL');
        }

        return $typeEnveloppe;
    }

    public function enveloppeCandidatureExists($typeEnveloppe)
    {
        if (
            $typeEnveloppe == Atexo_Config::getParameter('CANDIDATURE_SEUL') ||
            $typeEnveloppe == Atexo_Config::getParameter('CANDIDATURE_ET_OFFRE') ||
            $typeEnveloppe == Atexo_Config::getParameter('CANDIDATURE_ET_ANONYMAT') ||
            $typeEnveloppe == Atexo_Config::getParameter('CANDIDATURE_ET_OFFRE_ET_ANONYMAT') ||
            $typeEnveloppe == Atexo_Config::getParameter('CANDIDATURE_ET_OFFRE_TECHNIQUE_ET_OFFRE') ||
            $typeEnveloppe == Atexo_Config::getParameter('CANDIDATURE_ET_OFFRE_TECHNIQUE')
        ) {
            return true;
        } else {
            return false;
        }
    }

    public function enveloppeOffreExists($typeEnveloppe)
    {
        if (
            $typeEnveloppe == Atexo_Config::getParameter('CANDIDATURE_ET_OFFRE') ||
            $typeEnveloppe == Atexo_Config::getParameter('OFFRE_SEUL') ||
            $typeEnveloppe == Atexo_Config::getParameter('OFFRE_ET_ANONYMAT') ||
            $typeEnveloppe == Atexo_Config::getParameter('CANDIDATURE_ET_OFFRE_ET_ANONYMAT') ||
            $typeEnveloppe == Atexo_Config::getParameter('CANDIDATURE_ET_OFFRE_TECHNIQUE_ET_OFFRE') ||
            $typeEnveloppe == Atexo_Config::getParameter('OFFRE_TECHNIQUE_ET_OFFRE')
        ) {
            return true;
        } else {
            return false;
        }
    }

    public function enveloppeAnonymatExists($typeEnveloppe)
    {
        if (
            $typeEnveloppe == Atexo_Config::getParameter('CANDIDATURE_ET_OFFRE_ET_ANONYMAT') ||
            $typeEnveloppe == Atexo_Config::getParameter('CANDIDATURE_ET_ANONYMAT') ||
            $typeEnveloppe == Atexo_Config::getParameter('OFFRE_ET_ANONYMAT') ||
            $typeEnveloppe == Atexo_Config::getParameter('ANONYMAT_SEUL')
        ) {
            return true;
        } else {
            return false;
        }
    }

    public function enveloppeOffreTechniqueExists($typeEnveloppe)
    {
        if (
            $typeEnveloppe == Atexo_Config::getParameter('OFFRE_TECHNIQUE_SEUL') ||
            $typeEnveloppe == Atexo_Config::getParameter('CANDIDATURE_ET_OFFRE_TECHNIQUE_ET_OFFRE') ||
            $typeEnveloppe == Atexo_Config::getParameter('CANDIDATURE_ET_OFFRE_TECHNIQUE') ||
            $typeEnveloppe == Atexo_Config::getParameter('OFFRE_TECHNIQUE_ET_OFFRE')
        ) {
            return true;
        } else {
            return false;
        }
    }

    public function retrieveSignatureFromXml($signatureXml, $typeEnveloppe, $sousPli)
    {
        $tempFile = Atexo_Config::getParameter('COMMON_TMP') . session_id() . time() . '_tempXmlFile';
        Atexo_Util::write_file($tempFile, $signatureXml);

        $xmlContent = simplexml_load_file($tempFile);
        $signature = '';
        foreach ($xmlContent->Enveloppe as $xmlEnveloppe) {
            if ($typeEnveloppe == $xmlEnveloppe->TypeEnveloppe && $sousPli == $xmlEnveloppe->SousPli) {
                $signature = $xmlEnveloppe->SignatureFichier;
            }
        }
        unlink($tempFile);

        return $signature;
    }

    public function retrieveAdmissibiliteByPk($idEnv, $sousPli, $formeEnveloppe, $organisme, $typeEnv)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        if ($formeEnveloppe == Atexo_Config::getParameter('TYPE_DEPOT_QUESTION_ELECTRONIQUE')) {
            $admissibilite = CommonAdmissibiliteEnveloppeLotPeer::retrieveByPK($idEnv, $organisme, $sousPli, $typeEnv, $connexion);
        } else {
            $admissibilite = CommonAdmissibiliteEnveloppePapierLotPeer::retrieveByPK($idEnv, $organisme, $sousPli, $typeEnv, $connexion);
        }

        if ($admissibilite) {
            return $admissibilite;
        } else {
            return false;
        }
    }

    public function retrieveEnveloppeById($idEnveloppe, $formeEnveloppe, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();

        if ($formeEnveloppe == Atexo_Config::getParameter('TYPE_DEPOT_QUESTION_ELECTRONIQUE')) {
            $c->add(CommonEnveloppePeer::ID_ENVELOPPE_ELECTRO, $idEnveloppe);
            $c->add(CommonEnveloppePeer::ORGANISME, $organisme);
            $enveloppe = CommonEnveloppePeer::doSelectOne($c, $connexion);
        } else {
            $c->add(CommonEnveloppePapierPeer::ID_ENVELOPPE_PAPIER, $idEnveloppe);
            $c->add(CommonEnveloppePapierPeer::ORGANISME, $organisme);
            $enveloppe = CommonEnveloppePapierPeer::doSelectOne($c, $connexion);
        }

        if ($enveloppe) {
            return $enveloppe;
        } else {
            return false;
        }
    }

    public function updateStatutEnveloppe($idEnveloppe, $statut, $organisme)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));
        $enveloppe = CommonEnveloppePeer::retrieveByPK($idEnveloppe, $organisme, $connexionCom);

        $enveloppe->setStatutEnveloppe($statut);
        $enveloppe->save($connexionCom);
    }

    public function updateStatutEnveloppePapier($idEnveloppe, $statut)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));
        $enveloppe = CommonEnveloppePapierPeer::retrieveByPK($idEnveloppe, Atexo_CurrentUser::getCurrentOrganism(), $connexionCom);
        $enveloppe->setStatutEnveloppe($statut);
        $enveloppe->save($connexionCom);
    }

    public function retrieveAllEnveloppes($idReference, $numeroLot, $typesEnveloppes, $organisme)
    {
        $constitutionDossierReponse = null;
        $enveloppesCandidature = [];
        $enveloppesOffres = [];
        $enveloppesAnonymat = [];
        $enveloppesOffresTechnique = [];
        $enveloppesOffresPapierTechnique = [];
        $criteriaResponse = new Atexo_Response_CriteriaVo();
        $criteriaResponse->setReference($idReference);
        $criteriaResponse->setOrganisme($organisme);
        if (self::enveloppeCandidatureExists($typesEnveloppes)) {
            $enveloppesCandidature = self::retrieveEnveloppesCandidatureElectroniques($criteriaResponse);
            $enveloppeCandidaturePapier = self::retrieveEnveloppesCandidaturePapier($idReference, null, null, null, null, $organisme, false);
        } else {
            $enveloppesCandidature = [];
            $enveloppeCandidaturePapier = [];
        }
        if (self::enveloppeOffreTechniqueExists($typesEnveloppes)) {
            $enveloppesOffresTechnique = self::retrieveEnveloppesOffresTechniqueElectroniques($idReference, $numeroLot, $typesEnveloppes, null, null, $organisme);
            $enveloppesOffresPapierTechnique = self::retrieveEnveloppesOffreTechniquePapier($idReference, $typesEnveloppes, $numeroLot, null, null, $organisme, false, true);
        } else {
            $enveloppesOffresTechnique = [];
            $enveloppesOffresPapierTechnique = [];
        }
        if (self::enveloppeOffreExists($typesEnveloppes)) {
            $criteriaResponse = new Atexo_Response_CriteriaVo();
            $criteriaResponse->setReference($idReference);
            $criteriaResponse->setSousPli($numeroLot);
            $criteriaResponse->setConstitutionDossierReponse($constitutionDossierReponse);
            $criteriaResponse->setTypesEnveloppes($typesEnveloppes);
            $criteriaResponse->setOrganisme($organisme);
            $enveloppesOffres = self::retrieveEnveloppesOffresElectroniques($criteriaResponse);
            $enveloppesOffresPapier = self::retrieveEnveloppesOffrePapier($idReference, $typesEnveloppes, $numeroLot, null, null, $organisme, false, true);
        } else {
            $enveloppesOffres = [];
            $enveloppesOffresPapier = [];
        }
        if (self::enveloppeAnonymatExists($typesEnveloppes)) {
            $enveloppesAnonymat = self::retrieveEnveloppesAnonymatElectroniques($idReference, null, null, null, null, $organisme);
            $enveloppeAnonymatPapier = self::retrieveEnveloppesAnonymatPapier($idReference, null, null, null, null, $organisme, false);
        } else {
            $enveloppesAnonymat = [];
            $enveloppeAnonymatPapier = [];
        }

        $toutesLesEnveloppes = array_merge(
            $enveloppesCandidature,
            $enveloppeCandidaturePapier,
            $enveloppesOffresTechnique,
            $enveloppesOffresPapierTechnique,
            $enveloppesOffres,
            $enveloppesOffresPapier,
            $enveloppesAnonymat,
            $enveloppeAnonymatPapier
        );

        return $toutesLesEnveloppes;
    }

    public function calculZeroRat($enveloppes, $numeroLot = null, $returnZero = false, $constitutionDossierReponses = null)
    {
        $resteATraiter = 0;
        if ($returnZero) {
            return 0;
        }
        foreach ($enveloppes as $uneEnveloppe) {
            if (self::resteATraiter($uneEnveloppe, $numeroLot, $constitutionDossierReponses)) {
                ++$resteATraiter;
            }
        }

        return $resteATraiter;
    }

    public function resteATraiter($enveloppe, $sousPliAVerifier = null, $constitutionDossierReponses = null)
    {
        if ($enveloppe instanceof CommonEnveloppePapier && '0' == $enveloppe->getEnveloppePostule()) {
            return false;
        }
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $isPremiereEnveloppe = self::isPremiereEnveloppe($enveloppe->getTypeEnv(), $constitutionDossierReponses);
        if ($enveloppe->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_FERME')) {
            // dans le cas d'une enveloppe fermé, il faut déterminer si l'enveloppe précédente est ouverte et admissible.
            // Si l'enveloppe précédente est ouverte et admissible, l'enveloppe actuelle est considérée RAT
            // sinon, l'enveloppe actuelle est considérée 0 RAT
            if ($isPremiereEnveloppe) {
                return true;
            } else {
                if ($enveloppe instanceof CommonEnveloppe) {
                    $admissibiliteParent = CommonAdmissibiliteEnveloppeLotPeer::retrieveByPK(
                        $enveloppe->getOffreId(),
                        Atexo_CurrentUser::getCurrentOrganism(),
                        $enveloppe->getSousPli(),
                        $enveloppe->getTypeEnv(),
                        $connexion
                    );
                } else {
                    $admissibiliteParent = CommonAdmissibiliteEnveloppePapierLotPeer::retrieveByPK(
                        $enveloppe->getOffrePapierId(),
                        Atexo_CurrentUser::getCurrentOrganism(),
                        $enveloppe->getSousPli(),
                        $enveloppe->getTypeEnv(),
                        $connexion
                    );
                }
                if (
                    ($admissibiliteParent instanceof CommonAdmissibiliteEnveloppeLot || $admissibiliteParent instanceof CommonAdmissibiliteEnveloppePapierLot)
                    && $admissibiliteParent->getAdmissibilite() == Atexo_Config::getParameter('LOT_ADMISSIBLE')
                ) {
                    return true;
                }
            }
        }

        if (
            $enveloppe->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_EN_LIGNE') ||
             $enveloppe->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_HORS_LIGNE') ||
             $enveloppe->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE') ||
             $enveloppe->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_A_DISTANCE')
        ) {
            if ($enveloppe->getTypeEnv() == Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE') || $enveloppe->getTypeEnv() == Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE')) {
                $admissibiliteLots = $enveloppe->getCollAdmissibiliteLot();
                if (is_array($admissibiliteLots)) {
                    foreach ($admissibiliteLots as $admissibliteLotEnveloppeCandidature) {
                        if (null === $sousPliAVerifier || $sousPliAVerifier == $admissibliteLotEnveloppeCandidature->getSousPli()) {
                            if ($enveloppe instanceof CommonEnveloppe) {
                                $admissibiliteParent = CommonAdmissibiliteEnveloppeLotPeer::retrieveByPK(
                                    $enveloppe->getOffreId(),
                                    Atexo_CurrentUser::getCurrentOrganism(),
                                    $admissibliteLotEnveloppeCandidature->getSousPli(),
                                    $enveloppe->getTypeEnv(),
                                    $connexion
                                );
                            } else {
                                $admissibiliteParent = CommonAdmissibiliteEnveloppePapierLotPeer::retrieveByPK(
                                    $enveloppe->getOffrePapierId(),
                                    Atexo_CurrentUser::getCurrentOrganism(),
                                    $admissibliteLotEnveloppeCandidature->getSousPli(),
                                    $enveloppe->getTypeEnv(),
                                    $connexion
                                );
                            }

                            if ($admissibliteLotEnveloppeCandidature->getAdmissibilite() == Atexo_Config::getParameter('LOT_A_TRAITER')) {
                                if ($isPremiereEnveloppe) {
                                    return true;
                                } elseif (
                                    ($admissibiliteParent instanceof CommonAdmissibiliteEnveloppeLot || $admissibiliteParent instanceof CommonAdmissibiliteEnveloppePapierLot)
                                                  && $admissibiliteParent->getAdmissibilite() == Atexo_Config::getParameter('LOT_ADMISSIBLE')
                                ) {
                                    return true;
                                }

                                return false;
                            }
                        }
                    }

                    return false;
                }
            }

            return false;
        }

        return false;
    }

    public function retrieveEnveloppesAnonymatElectroniques($reference, $sousPli, $typesEnveloppes, $trier, $sensTri, $organisme, $modeUnique = false, $limit = null, $offset = null, $objet = null)
    {
        if (
            null !== $typesEnveloppes && !self::enveloppeCandidatureExists($typesEnveloppes) &&
            !self::enveloppeOffreExists($typesEnveloppes) &&
            !self::enveloppeAnonymatExists($typesEnveloppes)
        ) {
            throw new Atexo_Exception('unknown types Enveloppes ');
        }
        $arrayTri = [];
        if ($trier && $objet) {
            $arrayTri['tri'] = $objet . '.' . $trier;
            $arrayTri['sens'] = $sensTri;
        }
        $enveloppes = self::retrieveEnveloppesAnonymatElectroniquesByReference($reference, $sousPli, $organisme, $modeUnique, $limit, $offset, $arrayTri);
        if ($enveloppes) {
            self::addLibelleStatut($enveloppes);
            if (null !== $typesEnveloppes) {
                self::addCompanyName($enveloppes, $typesEnveloppes, $organisme);
            }
            self::sortByElement($enveloppes, $trier, $sensTri);

            return $enveloppes;
        } else {
            return [];
        }
    }

    public function retrieveEnveloppesAnonymatElectroniquesByReference($reference, $sousPli, $organisme, $modeUnique, $limit = null, $offset = null, $arrayTri = null)
    {
        $params = [];
        $query = ' SELECT Enveloppe. * , Offres. *  FROM Enveloppe, Offres WHERE Offres.CONSULTATION_ID=:consultation_id '
                . ' AND Enveloppe.OFFRE_ID = Offres.ID  '
                . 'AND Enveloppe.ORGANISME = Offres.ORGANISME AND Enveloppe.ORGANISME =:organisme AND Enveloppe.type_env=:typeEnv ';
        if (null !== $sousPli) {
            $query .= '    AND Enveloppe.sous_pli=:sousPli ';
            $params[':sousPli'] = $sousPli;
        }
        if ($modeUnique) {
            $query .= ' AND ( Enveloppe.statut_enveloppe=:ouverte1  OR '
                  . '		Enveloppe.statut_enveloppe=:ouverteEnLigne  OR '
                  . '		Enveloppe.statut_enveloppe=:ouverteHorsLigne  OR '
                  . '		Enveloppe.statut_enveloppe=:ouverteADistance  ) ';

            $params[':ouverte1'] = Atexo_Config::getParameter('STATUT_ENV_OUVERTE');
            $params[':ouverteEnLigne'] = Atexo_Config::getParameter('STATUT_ENV_OUVERTE_EN_LIGNE');
            $params[':ouverteHorsLigne'] = Atexo_Config::getParameter('STATUT_ENV_OUVERTE_HORS_LIGNE');
            $params[':ouverteADistance'] = Atexo_Config::getParameter('STATUT_ENV_OUVERTE_A_DISTANCE');
        }
        $query .= ' AND Offres.statut_offres != :statutBrouillon AND Enveloppe.statut_enveloppe != :statutBrouillon';
        $params[':consultation_id'] = $reference;
        $params[':organisme'] = $organisme;
        $params[':typeEnv'] = Atexo_Config::getParameter('TYPE_ENV_ANONYMAT');
        $params[':statutBrouillon'] = Atexo_Config::getParameter('STATUT_ENV_BROUILLON');
        if (is_array($arrayTri) && count($arrayTri)) {
            $trier = $arrayTri['tri'];
            $sensTri = $arrayTri['sens'];
            $query .= ' ORDER BY ' . $trier . ' ' . $sensTri;
        } else {
            $query .= ' ORDER BY Offres.untrusteddate ASC ';
        }
        if ($limit && $offset) {
            $query .= ' limit ' . $offset . ',' . $limit;
        }
        $statement = Atexo_Db::getLinkCommon(true)->prepare($query);
        $statement->execute($params);
        $arraysEnveloppes = [];
        $arrayIds = [];
        $returnOffreWithinEnveloppe = true;
        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            if ($returnOffreWithinEnveloppe) {
                $id = $row['id_enveloppe_electro'];
            } else {
                $id = $row['id'];
            }
            if (!isset($arrayIds[$id])) {
                $enveloppe = self::fillObjectFromArray($row, $returnOffreWithinEnveloppe);
                $arraysEnveloppes[] = $enveloppe;
                $arrayIds[$id] = true;
            }
        }

        return $arraysEnveloppes;
    }

    public function retrieveEnveloppeAnonymatByIdOffre($idOffre, $organisme)
    {
        $params = [];
        $query = ' SELECT Enveloppe. * , Offres. *  FROM Enveloppe, Offres WHERE Offres.ID=:idOffre '
                . 'AND Enveloppe.TYPE_ENV =:typeEnv AND Offres.DEPOT_ANNULE =:depotAnnule AND Enveloppe.OFFRE_ID = Offres.ID  '
                . 'AND Enveloppe.ORGANISME = Offres.ORGANISME AND Enveloppe.ORGANISME =:organisme';
        $query .= ' AND Offres.statut_offres != :statutBrouillon AND Enveloppe.statut_enveloppe != :statutBrouillon';
        $params[':idOffre'] = $idOffre;
        $params[':typeEnv'] = Atexo_Config::getParameter('TYPE_ENV_ANONYMAT');
        $params[':depotAnnule'] = Atexo_Config::getParameter('DEPOT_NON_ANNULE');
        $params[':organisme'] = $organisme;
        $params[':statutBrouillon'] = Atexo_Config::getParameter('STATUT_ENV_BROUILLON');

        $statement = Atexo_Db::getLinkCommon(true)->prepare($query);
        $statement->execute($params);
        $arraysEnveloppes = [];
        $arrayIds = [];
        $returnOffreWithinEnveloppe = true;
        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            if ($returnOffreWithinEnveloppe) {
                $id = $row['id_enveloppe_electro'];
            } else {
                $id = $row['id'];
            }
            if (!isset($arrayIds[$id])) {
                $enveloppe = self::fillObjectFromArray($row, $returnOffreWithinEnveloppe);
                $arraysEnveloppes[] = $enveloppe;
                $arrayIds[$id] = true;
            }
        }

        return $arraysEnveloppes;
    }

    public function retrieveEnveloppeCandidatureByIdOffre($idOffre, $organisme)
    {
        $params = [];
        $query = ' SELECT Enveloppe. * FROM Enveloppe, Offres WHERE Offres.ID=:idOffre '
                . 'AND Enveloppe.TYPE_ENV =:typeEnv AND Enveloppe.organisme =:organisme AND Enveloppe.organisme=Offres.organisme AND Enveloppe.offre_id=Offres.id ';
        $query .= ' AND Offres.statut_offres != :statutBrouillon AND Enveloppe.statut_enveloppe != :statutBrouillon';
        $params[':idOffre'] = $idOffre;
        $params[':typeEnv'] = Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE');
        $params[':organisme'] = $organisme;
        $params[':statutBrouillon'] = Atexo_Config::getParameter('STATUT_ENV_BROUILLON');
        $statement = Atexo_Db::getLinkCommon(true)->prepare($query);
        $statement->execute($params);
        $arraysEnveloppes = [];
        $arrayIds = [];
        $returnOffreWithinEnveloppe = true;
        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            if ($returnOffreWithinEnveloppe) {
                $id = $row['id_enveloppe_electro'];
            } else {
                $id = $row['id'];
            }
            if (!isset($arrayIds[$id])) {
                $enveloppe = self::fillObjectFromArray($row, $returnOffreWithinEnveloppe);
                $arraysEnveloppes[] = $enveloppe;
                $arrayIds[$id] = true;
            }
        }

        return array_shift($arraysEnveloppes);
    }

    public function openUnChiffredEnveloppe($idEnveloppe, $offreId, $typeEnv, $sousPli, $formatEnveloppe, $organisme)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));
        $atexoBlob = new Atexo_Blob();
        $enveloppe = self::retrieveEnveloppeByPk($idEnveloppe, $formatEnveloppe, $organisme);
        if ($enveloppe) {
            try {
                $fichiers = $enveloppe->getCommonFichierEnveloppes($connexionCom);
                $fichierOuvert = Atexo_Config::getParameter('COMMON_TMP') . 'bloc_dechiffre_' . self::class . '_' . session_name() . session_id() . time();
                if ($fichiers && is_array($fichiers)) {
                    foreach ($fichiers as $fichier) {
                        if (!$fichier->getIsHash() && !$fichier->getIdBlob()) { // si on n'as pas id blob from edition
                            $c = new Criteria();
                            $c->add(CommonBlocFichierEnveloppePeer::ID_FICHIER, $fichier->getIdFichier());
                            $c->add(CommonBlocFichierEnveloppePeer::ORGANISME, $organisme);
                            $blocs = $fichier->getBlocfichierenveloppes($c, $connexionCom);
                            foreach ($blocs as $bloc) {
                                $fp = fopen($fichierOuvert, 'a');
                                $fileContent = base64_decode($atexoBlob->return_blob_to_client($bloc->getIdBlobChiffre(), $organisme));
                                if (false === fwrite($fp, $fileContent)) {
                                    throw new Exception("impossible d'ecrire dans le fichier");
                                }
                                fclose($fp);
                            }
                            $idFichierBlob = $atexoBlob->insert_blob($fichier->getNomFichier(), $fichierOuvert, $organisme); //TODO
                            $fichier->setIdBlob($idFichierBlob);
                            $fichier->save($connexionCom);
                            if (is_file($fichierOuvert)) {
                                unlink($fichierOuvert);
                            }
                        }
                    }
                }
                $enveloppe->setStatutEnveloppe(Atexo_Config::getParameter('STATUT_ENV_OUVERTE_EN_LIGNE'));
                $enveloppe->setCryptage('0');
                if (Atexo_Module::isEnabled('fourEyes')) {
                    $enveloppe->setDateheureOuvertureAgent2(date('Y-m-d H:i:s'));
                    $enveloppe->setAgentIdOuverture2(Atexo_CurrentUser::getIdAgentConnected());
                } else {
                    $enveloppe->setDateheureOuverture(date('Y-m-d H:i:s'));
                    $enveloppe->setAgentIdOuverture(Atexo_CurrentUser::getIdAgentConnected());
                }
                $enveloppe->save($connexionCom);

                return 1;
            } catch (Exception $e) {
                echo $e;

                return 0;
            }
        }
    }

    public function retrieveEnveloppeByPk($idEnveloppe, $typeEnveloppe, $organisme)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        if ($typeEnveloppe == Atexo_Config::getParameter('TYPE_DEPOT_QUESTION_ELECTRONIQUE')) {
            $enveloppe = CommonEnveloppePeer::retrieveByPK($idEnveloppe, $organisme, $connexionCom);
        } else {
            $enveloppe = CommonEnveloppePapierPeer::retrieveByPK($idEnveloppe, $organisme, $connexionCom);
        }

        if ($enveloppe) {
            return $enveloppe;
        } else {
            return false;
        }
    }

    public function retrieveEnveloppesCandidaturePapier($reference, $idOffre, $typesEnveloppes, $trier, $sensTri, $organisme, $searchByIdOffre = false, $modeUnique = false, $limit = null, $offset = null, $objet = false, $admissible = true, $count = false, $arrayIdsGuests = null)
    {
        if ($searchByIdOffre && !$idOffre) {
            throw new Atexo_Exception('idOffre non fournie');
        }

        if (!$reference) {
            throw new Atexo_Exception('reference non fournie');
        }
        $arrayTri = [];
        if ($trier && $objet) {
            $arrayTri['tri'] = $objet . '.' . $trier;
            $arrayTri['sens'] = $sensTri;
        }
        if ($limit && $offset) {
            $enveloppes = self::retrieveEnveloppesCandidaturePapierByid($reference, $idOffre, $searchByIdOffre, $organisme, $modeUnique, $limit, $offset, $arrayTri);
        } else {
            $enveloppes = self::retrieveEnveloppesCandidaturePapierByid($reference, $idOffre, $searchByIdOffre, $organisme, $modeUnique, null, null, $arrayTri);
            if ($count) {
                return $enveloppes;
            }
        }

        if ($enveloppes) {
            self::addAdmissibility($enveloppes, $reference, $organisme, Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE'), null, $admissible);
            if (null !== $typesEnveloppes) {
                if (
                    !self::enveloppeCandidatureExists($typesEnveloppes) &&
                    !self::enveloppeOffreExists($typesEnveloppes) &&
                    !self::enveloppeAnonymatExists($typesEnveloppes)
                ) {
                    throw new Atexo_Exception('Values not supported for typesEnveloppes ');
                }
                self::addCompanyName($enveloppes, $typesEnveloppes, $organisme);
            }
            if ($trier) {
                self::sortByElement($enveloppes, $trier, $sensTri);
            }
            if (Atexo_Module::isEnabled('AvisMembresCommision')) {
                self::addStatistiqueAvisCAO($reference, $organisme, $enveloppes, $arrayIdsGuests);
            }

            return $enveloppes;
        } else {
            return [];
        }
    }

    private function retrieveEnveloppesCandidaturePapierByid($reference, $idOffre, $searchByIdOffre, $organisme, $modeUnique = false, $limit = null, $offset = null, $arrayTri = null)
    {
        $params = [];
        $query = ' select  Enveloppe_papier.*, Offre_papier.* from Offre_papier  , Enveloppe_papier  '
              . ' where ' . ($searchByIdOffre ? ' Offre_papier.id=:idSearch ' : ' Offre_papier.consultation_id=:idSearch ') . ' AND Offre_papier.id=Enveloppe_papier.offre_papier_id AND type_env=:typeEnv '
              . ' AND Offre_papier.depot_annule=:depotNonAnnule AND Enveloppe_papier.organisme=Offre_papier.organisme AND Offre_papier.organisme=:organisme ';
        if ($modeUnique) {
            $query .= ' AND Enveloppe_papier.statut_enveloppe=:ouverte ';
            $params[':ouverte'] = Atexo_Config::getParameter('STATUT_ENV_OUVERTE');
        }
        $params[':typeEnv'] = Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE');
        $params[':idSearch'] = ($searchByIdOffre ? $idOffre : $reference);
        $params[':depotNonAnnule'] = Atexo_Config::getParameter('DEPOT_NON_ANNULE');
        $params[':organisme'] = $organisme;
        if (is_array($arrayTri) && count($arrayTri)) {
            $trier = $arrayTri['tri'];
            $sensTri = $arrayTri['sens'];
            $query .= ' ORDER BY ' . $trier . ' ' . $sensTri;
        } else {
            $query .= ' ORDER BY Offre_papier.date_depot ASC ';
        }

        if ($limit && $offset) {
            $query .= ' limit ' . $offset . ',' . $limit;
        }
        $statement = Atexo_Db::getLinkCommon(true)->prepare($query);
        $statement->execute($params);

        $arraysEnveloppes = [];
        $arrayIdsEnveloppes = [];
        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            if (!isset($arrayIdsEnveloppes[$row['id_enveloppe_papier']])) {
                $enveloppe = self::fillObjectEnveloppePapier($row);
                $arraysEnveloppes[] = $enveloppe;
                $arrayIdsEnveloppes[$row['id_enveloppe_papier']] = true;
            }
        }

        return $arraysEnveloppes;
    }

    public function retrieveEnveloppesOffrePapier($id, $typesEnveloppes, $sousPli, $trier, $sensTri, $organisme, $searchByIdOffre = false, $returnEnveloppe = true, $nomEntrepriseVisible = false, $enveloppeStatutOuverte = false, $admissible = true, $limit = null, $offset = null, $objet = null)
    {
        $arrayTri = [];
        if ($trier && $objet) {
            $arrayTri['tri'] = $objet . '.' . $trier;
            $arrayTri['sens'] = $sensTri;
        }
        $enveloppes = self::retrieveEnveloppesOffresPapierByid(
            $id,
            $typesEnveloppes,
            $sousPli,
            $searchByIdOffre,
            true,
            $organisme,
            $returnEnveloppe,
            $enveloppeStatutOuverte,
            $admissible,
            $limit,
            $offset,
            $arrayTri
        );
        if ($enveloppes) {
            if (null !== $typesEnveloppes || $nomEntrepriseVisible) {
                if (
                    !self::enveloppeCandidatureExists($typesEnveloppes) &&
                    !self::enveloppeOffreExists($typesEnveloppes) &&
                    !self::enveloppeAnonymatExists($typesEnveloppes) && !$nomEntrepriseVisible
                ) {
                    throw new Atexo_Exception('Values not supported for typesEnveloppes ');
                }
                self::addCompanyName($enveloppes, $typesEnveloppes, $organisme, $nomEntrepriseVisible);
            }
            if ($trier) {
                self::sortByElement($enveloppes, $trier, $sensTri);
            }

            return $enveloppes;
        } else {
            return [];
        }
    }

    private function retrieveEnveloppesOffresPapierByid($id, $typesEnveloppes, $sousPli, $searchByIdOffre, $enveloppePostule, $organisme, $returnEnveloppe = true, $enveloppeStatutOuverte = false, $admissible = true, $limit = null, $offset = null, $arrayTri = null)
    {
        $params = [];
        if ((self::enveloppeCandidatureExists($typesEnveloppes) || self::enveloppeOffreTechniqueExists($typesEnveloppes)) && $admissible) {
            $query = 'SELECT Offre_papier.consultation_id as consultationId, Offre_papier.organisme as org, Offre_papier.*, Offre_papier.code_postal as code_postal_offre,'
                   . 'Offre_papier.ville as ville_offre,' . (($returnEnveloppe) ? ' Enveloppe_papier.* ' : ' decisionEnveloppe.* ')
                   . ' FROM Offre_papier ' . (($returnEnveloppe) ? ' ' : 'left join decisionEnveloppe on (Offre_papier.id=decisionEnveloppe.id_offre AND Offre_papier.organisme=decisionEnveloppe.organisme'
                   . ' ' . ((!$searchByIdOffre) ? 'AND decisionEnveloppe.consultation_id=:idRefOrOffre' : 'AND decisionEnveloppe.id_offre=:idRefOrOffre')
                   . ' AND lot=' . $sousPli . " AND decisionEnveloppe.type_enveloppe='" . Atexo_Config::getParameter('DEPOT_PAPIER') . "') ") . ', Enveloppe_papier, Admissibilite_Enveloppe_papier_Lot as Adm'
                   . ' WHERE  Offre_papier.id = Enveloppe_papier.offre_papier_id AND Enveloppe_papier.offre_papier_id = Enveloppe_papier.offre_papier_id '
                   . ($searchByIdOffre ? ' AND Offre_papier.id=:idRefOrOffre ' : ' AND Offre_papier.consultation_id=:idRefOrOffre ')
                   . ' AND  Offre_papier.id=Adm.id_offre_papier AND Adm.admissibilite=:admis AND Offre_papier.organisme=Adm.organisme'
                   . (($enveloppePostule) ? ' AND Enveloppe_papier.enveloppe_postule=:enveloppePostule ' : '') . ' AND type_env=:typeEnvOffre ';
            if (null !== $sousPli) {
                $query .= '    AND Enveloppe_papier.sous_pli=:sousPli '
                       . ' 	  AND Adm.sous_pli=:sousPli ';
            } elseif (0 !== $sousPli) {
                $query .= ' 	  AND Enveloppe_papier.sous_pli = Adm.sous_pli ';
            }
            if ($enveloppeStatutOuverte) {
                $query .= ' AND Enveloppe_papier.statut_enveloppe=:ouverte ';
                $params[':ouverte'] = Atexo_Config::getParameter('STATUT_ENV_OUVERTE');
            }
            $query .= ' AND Adm.type_enveloppe=:admissibiliteEnv ';
            $query .= ' AND Offre_papier.depot_annule=:depotNonAnnule  AND Offre_papier.organisme=Enveloppe_papier.organisme AND Offre_papier.organisme=:organisme ';

            $params[':idRefOrOffre'] = $id;
            $params[':admis'] = Atexo_Config::getParameter('LOT_ADMISSIBLE');
            $params[':typeEnvOffre'] = Atexo_Config::getParameter('TYPE_ENV_OFFRE');
            $params[':admissibiliteEnv'] = Atexo_Config::getParameter('TYPE_ENV_OFFRE');
            $params[':depotNonAnnule'] = Atexo_Config::getParameter('DEPOT_NON_ANNULE');
            $params[':organisme'] = $organisme;

            if ($enveloppePostule) {
                $params[':enveloppePostule'] = Atexo_Config::getParameter('ENVELOPPE_POSTULE');
            }
            if (strstr($query, ':sousPli')) {
                $params[':sousPli'] = $sousPli;
            }
        } else {
            $query = ' SELECT Offre_papier.consultation_id as consultationId , Offre_papier.organisme as org, Offre_papier.* ,
						Offre_papier.code_postal as code_postal_offre, Offre_papier.ville as ville_offre, '
                   . (($returnEnveloppe) ? ' Enveloppe_papier.* ' : ' decisionEnveloppe.* ')
                   . ' FROM Offre_papier ' . (($returnEnveloppe) ? '' : 'left join decisionEnveloppe on (Offre_papier.id=decisionEnveloppe.id_offre AND Offre_papier.organisme=decisionEnveloppe.organisme '
                   . ((!$searchByIdOffre) ? 'AND decisionEnveloppe.consultation_id =:idRefOrOffre' : 'AND decisionEnveloppe.id_offre=:idRefOrOffre') . ' AND lot=' . $sousPli
                   . " AND decisionEnveloppe.type_enveloppe='" . Atexo_Config::getParameter('DEPOT_PAPIER') . "' ) ") . ', Enveloppe_papier '
                   . ' WHERE Offre_papier.id = Enveloppe_papier.offre_papier_id  AND Offre_papier.organisme = Enveloppe_papier.organisme '
                   . ($searchByIdOffre ? ' AND Offre_papier.id=:idRefOrOffre ' : ' AND Offre_papier.consultation_id=:idRefOrOffre ')
                   . ' AND Enveloppe_papier.type_env =:typeEnvOffre ' . (($enveloppePostule) ? ' AND Enveloppe_papier.enveloppe_postule=:enveloppePostule ' : '');
            if (null !== $sousPli) {
                $query .= '    AND Enveloppe_papier.sous_pli=:sousPli ';
            }
            if ($enveloppeStatutOuverte) {
                $query .= ' AND Enveloppe_papier.statut_enveloppe=:ouverte ';
                $params[':ouverte'] = Atexo_Config::getParameter('STATUT_ENV_OUVERTE');
            }
            $query .= ' AND Offre_papier.depot_annule=:depotNonAnnule AND Offre_papier.organisme=:organisme ';
            $params[':idRefOrOffre'] = $id;
            $params[':typeEnvOffre'] = Atexo_Config::getParameter('TYPE_ENV_OFFRE');
            $params[':depotNonAnnule'] = Atexo_Config::getParameter('DEPOT_NON_ANNULE');
            $params[':organisme'] = $organisme;
            if ($enveloppePostule) {
                $params[':enveloppePostule'] = Atexo_Config::getParameter('ENVELOPPE_POSTULE');
            }
            if (strstr($query, ':sousPli')) {
                $params[':sousPli'] = $sousPli;
            }
        }
        if (is_array($arrayTri) && count($arrayTri)) {
            $trier = $arrayTri['tri'];
            $sensTri = $arrayTri['sens'];
            $query .= ' ORDER BY ' . $trier . ' ' . $sensTri;
        } else {
            $query .= ' ORDER BY Offre_papier.date_depot ASC ';
        }
        if ($limit && $offset) {
            $query .= ' limit ' . $offset . ',' . $limit;
        }
        $statement = Atexo_Db::getLinkCommon(true)->prepare($query);
        $statement->execute($params);

        $arraysEnveloppes = [];
        $arrayIdsEnveloppes = [];
        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            if ($returnEnveloppe) {
                $id = $row['id_enveloppe_papier'];
            } else {
                $id = $row['id'];
            }
            if (!isset($arrayIdsEnveloppes[$id])) {
                $enveloppe = self::fillObjectEnveloppePapier($row, $returnEnveloppe);
                $arraysEnveloppes[] = $enveloppe;
                $arrayIdsEnveloppes[$id] = true;
            }
        }

        return $arraysEnveloppes;
    }

    public function retrieveEnveloppesOffreTechniquePapier($id, $constitutionDossierReponse, $sousPli, $trier, $sensTri, $organisme, $searchByIdOffre = false, $returnEnveloppe = true, $nomEntrepriseVisible = false, $enveloppeStatutOuverte = false, $admissible = true, $limit = null, $offset = null, $objet = null, $arrayIdsGuests = null)
    {
        $arrayTri = [];
        if ($trier && $objet) {
            $arrayTri['tri'] = $objet . '.' . $trier;
            $arrayTri['sens'] = $sensTri;
        }
        $enveloppes = self::retrieveEnveloppesOffresTechniquesPapierByid(
            $id,
            $constitutionDossierReponse,
            $sousPli,
            $searchByIdOffre,
            true,
            $organisme,
            $returnEnveloppe,
            $enveloppeStatutOuverte,
            $admissible,
            $limit,
            $offset,
            $arrayTri
        );

        if ($enveloppes) {
            if (!$searchByIdOffre) {
                self::addAdmissibility($enveloppes, $id, $organisme, Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE'), $sousPli);
            }

            if (null !== $constitutionDossierReponse || $nomEntrepriseVisible) {
                if (
                    !self::enveloppeCandidatureExists($constitutionDossierReponse) &&
                    !self::enveloppeOffreExists($constitutionDossierReponse) &&
                    !self::enveloppeAnonymatExists($constitutionDossierReponse) &&
                    !self::enveloppeOffreTechniqueExists($constitutionDossierReponse) && !$nomEntrepriseVisible
                ) {
                    throw new Atexo_Exception('Values not supported for typesEnveloppes ');
                }
                self::addCompanyName($enveloppes, $constitutionDossierReponse, $organisme, $nomEntrepriseVisible);
            }
            if ($trier) {
                self::sortByElement($enveloppes, $trier, $sensTri);
            }
            if (Atexo_Module::isEnabled('AvisMembresCommision')) {
                self::addStatistiqueAvisCAO($id, $organisme, $enveloppes, $arrayIdsGuests);
            }

            return $enveloppes;
        } else {
            return [];
        }
    }

    private function retrieveEnveloppesOffresTechniquesPapierByid($id, $constitutionDossierReponse, $sousPli, $searchByIdOffre, $enveloppePostule, $organisme, $returnEnveloppe = true, $enveloppeStatutOuverte = false, $admissible = true, $limit = null, $offset = null, $arrayTri = null)
    {
        $params = [];
        if (self::enveloppeCandidatureExists($constitutionDossierReponse) && $admissible) {
            $query = ' SELECT Offre_papier.consultation_id as consultationId, Offre_papier.* , Offre_papier.code_postal as code_postal_offre, '
                   . (($returnEnveloppe) ? ' Enveloppe_papier.* ' : ' decisionEnveloppe.* ')
                   . ' FROM Offre_papier' . (($returnEnveloppe) ? ' ' : ' left join decisionEnveloppe on (Offre_papier.id=decisionEnveloppe.id_offre AND Offre_papier.organisme=decisionEnveloppe.organisme '
                   . ((!$searchByIdOffre) ? 'AND decisionEnveloppe.consultation_id =:idRefOrOffre' : 'AND decisionEnveloppe.id_offre=:idRefOrOffre') . ' AND lot=' . $sousPli
                   . " AND decisionEnveloppe.type_enveloppe='" . Atexo_Config::getParameter('DEPOT_PAPIER') . "') ") . ', Enveloppe_papier, Admissibilite_Enveloppe_papier_Lot as Adm'
                   . ' WHERE Offre_papier.id = Enveloppe_papier.offre_papier_id AND Enveloppe_papier.organisme = Offre_papier.organisme '
                   . ($searchByIdOffre ? ' AND Offre_papier.id=:idRefOrOffre ' : ' AND Offre_papier.consultation_id=:idRefOrOffre ')
                   . ' AND Offre_papier.id=Adm.id_offre_papier AND Adm.admissibilite=:admis AND Offre_papier.organisme=Adm.organisme AND Offre_papier.organisme=:organisme '
                   . (($enveloppePostule) ? ' AND Enveloppe_papier.enveloppe_postule=:enveloppePostule ' : '')
                   . ' AND type_env=:typeEnvOffre ' . (($returnEnveloppe) ? ' ' : " AND  decisionEnveloppe.type_enveloppe='" . Atexo_Config::getParameter('DEPOT_PAPIER') . "' ");
            if (null !== $sousPli) {
                $query .= '    AND Enveloppe_papier.sous_pli=:sousPli '
                       . ' 	  AND Adm.sous_pli=:sousPli ';
            } elseif ($sousPli) {
                $query .= ' 	  AND  Adm.sous_pli=:sousPli ';
            }
            if ($enveloppeStatutOuverte) {
                $query .= ' AND Enveloppe_papier.statut_enveloppe=:ouverte ';
                $params[':ouverte'] = Atexo_Config::getParameter('STATUT_ENV_OUVERTE');
            }
            $query .= ' AND Offre_papier.depot_annule=:depotNonAnnule ';
            $params[':idRefOrOffre'] = $id;
            $params[':admis'] = Atexo_Config::getParameter('LOT_ADMISSIBLE');
            $params[':typeEnvOffre'] = Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE');
            $params[':depotNonAnnule'] = Atexo_Config::getParameter('DEPOT_NON_ANNULE');
            $params[':organisme'] = $organisme;
            if ($enveloppePostule) {
                $params[':enveloppePostule'] = Atexo_Config::getParameter('ENVELOPPE_POSTULE');
            }
            if (strstr($query, ':sousPli')) {
                $params[':sousPli'] = $sousPli;
            }
        } else {
            $query = ' SELECT Offre_papier.consultation_id as consultationId, Offre_papier.* ,Offre_papier.code_postal as code_postal_offre, '
                   . (($returnEnveloppe) ? ' Enveloppe_papier.* ' : ' decisionEnveloppe.* ')
                   . ' FROM Offre_papier ' . (($returnEnveloppe) ? '' : 'left join decisionEnveloppe on (Offre_papier.id=decisionEnveloppe.id_offre AND Offre_papier.organisme=decisionEnveloppe.organisme '
                   . ((!$searchByIdOffre) ? 'AND decisionEnveloppe.consultation_id =:idRefOrOffre' : 'AND decisionEnveloppe.id_offre=:idRefOrOffre') . ' AND lot=' . $sousPli
                   . " AND decisionEnveloppe.type_enveloppe='" . Atexo_Config::getParameter('DEPOT_PAPIER') . "' ) ") . ', Enveloppe_papier'
                   . ' WHERE  Offre_papier.id = Enveloppe_papier.offre_papier_id  AND Offre_papier.organisme = Enveloppe_papier.organisme  AND Offre_papier.organisme=:organisme '
                   . ($searchByIdOffre ? ' AND Offre_papier.id=:idRefOrOffre ' : ' AND Offre_papier.consultation_id=:idRefOrOffre ')
                   . '        AND Enveloppe_papier.type_env =:typeEnvOffre ' . (($enveloppePostule) ? ' AND Enveloppe_papier.enveloppe_postule=:enveloppePostule ' : '');
            if (null !== $sousPli) {
                $query .= '    AND Enveloppe_papier.sous_pli=:sousPli ';
            }
            if ($enveloppeStatutOuverte) {
                $query .= ' AND Enveloppe_papier.statut_enveloppe=:ouverte ';
                $params[':ouverte'] = Atexo_Config::getParameter('STATUT_ENV_OUVERTE');
            }
            $query .= ' AND Offre_papier.depot_annule=:depotNonAnnule ';
            $params[':idRefOrOffre'] = $id;
            $params[':typeEnvOffre'] = Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE');
            $params[':depotNonAnnule'] = Atexo_Config::getParameter('DEPOT_NON_ANNULE');
            $params[':organisme'] = $organisme;
            if ($enveloppePostule) {
                $params[':enveloppePostule'] = Atexo_Config::getParameter('ENVELOPPE_POSTULE');
            }
            if (strstr($query, ':sousPli')) {
                $params[':sousPli'] = $sousPli;
            }
        }
        if (is_array($arrayTri) && count($arrayTri)) {
            $trier = $arrayTri['tri'];
            $sensTri = $arrayTri['sens'];
            $query .= ' ORDER BY ' . $trier . ' ' . $sensTri;
        } else {
            $query .= ' ORDER BY Offre_papier.date_depot ASC ';
        }
        if ($limit && $offset) {
            $query .= ' limit ' . $offset . ',' . $limit;
        }
        $statement = Atexo_Db::getLinkCommon(true)->prepare($query);
        $statement->execute($params);
        $arraysEnveloppes = [];
        $arrayIdsEnveloppes = [];
        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            if ($returnEnveloppe) {
                $id = $row['id_enveloppe_papier'];
            } else {
                $id = $row['id'];
            }
            if (!isset($arrayIdsEnveloppes[$id])) {
                $enveloppe = self::fillObjectEnveloppePapier($row, $returnEnveloppe);
                $arraysEnveloppes[] = $enveloppe;
                $arrayIdsEnveloppes[$id] = true;
            }
        }

        return $arraysEnveloppes;
    }

    public function retrieveEnveloppesAnonymatPapier($id, $typesEnveloppes, $sousPli, $trier, $sensTri, $organisme, $searchByIdOffre = false, $modeUnique = false, $limit = null, $offset = null, $objet = null)
    {
        $arrayTri = [];
        if ($trier && $objet) {
            $arrayTri['tri'] = $objet . '.' . $trier;
            $arrayTri['sens'] = $sensTri;
        }
        $enveloppes = self::retrieveEnveloppesAnonymatByid($id, $sousPli, $searchByIdOffre, $organisme, $modeUnique, $limit, $offset, $arrayTri);

        if ($enveloppes) {
            if (null !== $typesEnveloppes) {
                if (
                    !self::enveloppeCandidatureExists($typesEnveloppes) &&
                    !self::enveloppeOffreExists($typesEnveloppes) &&
                    !self::enveloppeAnonymatExists($typesEnveloppes)
                ) {
                    throw new Atexo_Exception('Values not supported for typesEnveloppes ');
                }
                self::addCompanyName($enveloppes, $typesEnveloppes, $organisme);
            }
            if ($trier) {
                self::sortByElement($enveloppes, $trier, $sensTri);
            }

            return $enveloppes;
        } else {
            return [];
        }
    }

    private function retrieveEnveloppesAnonymatByid($id, $sousPli, $searchByIdOffre, $organisme, $modeUnique = false, $limit = null, $offset = null, $arrayTri = null)
    {
        $params = [];
        $query = ' SELECT Offre_papier.* , Enveloppe_papier.* '
               . ' FROM   Offre_papier, Enveloppe_papier  '
               . ' WHERE  Offre_papier.id = Enveloppe_papier.offre_papier_id  '
               . ($searchByIdOffre ? ' AND Offre_papier.id=:idRefOrOffre ' : ' AND Offre_papier.consultation_id=:idRefOrOffre ')
               . '        AND Enveloppe_papier.type_env =:typeEnvOffre ';
        if (null !== $sousPli) {
            $query .= '    AND Enveloppe_papier.sous_pli=:sousPli ';
        }
        $query .= ' AND Offre_papier.depot_annule=:depotNonAnnule AND Offre_papier.organisme = Enveloppe_papier.organisme AND Offre_papier.organisme = :organisme ';
        if ($modeUnique) {
            $query .= ' AND Enveloppe_papier.statut_enveloppe=:ouverte ';
            $params[':ouverte'] = Atexo_Config::getParameter('STATUT_ENV_OUVERTE');
        }
        $params[':idRefOrOffre'] = $id;
        $params[':typeEnvOffre'] = Atexo_Config::getParameter('TYPE_ENV_ANONYMAT');
        $params[':depotNonAnnule'] = Atexo_Config::getParameter('DEPOT_NON_ANNULE');
        $params[':organisme'] = $organisme;

        if (null !== $sousPli) {
            $params[':sousPli'] = $sousPli;
        }
        if (is_array($arrayTri) && count($arrayTri)) {
            $trier = $arrayTri['tri'];
            $sensTri = $arrayTri['sens'];
            $query .= ' ORDER BY ' . $trier . ' ' . $sensTri;
        } else {
            $query .= ' ORDER BY Offre_papier.date_depot ASC ';
        }
        if ($limit && $offset) {
            $query .= ' limit ' . $offset . ',' . $limit;
        }
        $statement = Atexo_Db::getLinkCommon(true)->prepare($query);
        $statement->execute($params);

        $arraysEnveloppes = [];
        $arrayIdsEnveloppes = [];
        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            if (!isset($arrayIdsEnveloppes[$row['id_enveloppe_papier']])) {
                $enveloppe = self::fillObjectEnveloppePapier($row);
                $arraysEnveloppes[] = $enveloppe;
                $arrayIdsEnveloppes[$row['id_enveloppe_papier']] = true;
            }
        }

        return $arraysEnveloppes;
    }

    private function fillObjectEnveloppePapier($arrayData, $returnEnveloppe = true)
    {
        if (!is_array($arrayData)) {
            throw new Atexo_Exception('Expected Parameter to be an array');
        }
        $offre = new CommonOffrePapier();
        $offre->setId($arrayData['id']);
        if (isset($arrayData['consultationId'])) {
            $offre->setConsultationId($arrayData['consultationId']);
        } else {
            $offre->setConsultationId($arrayData['consultation_id']);
        }
        $offre->setNomEntreprise($arrayData['nom_entreprise']);
        $offre->setEntrepriseId($arrayData['entreprise_id']);
        $offre->setIdEtablissement($arrayData['id_etablissement']);
        $offre->setDateDepot(Atexo_Util::iso2frnDateTime($arrayData['date_depot']));
        $offre->setExternalSerial($arrayData['external_serial']);
        $offre->setInternalSerial($arrayData['internal_serial']);
        $offre->setOffreSelectionnee($arrayData['offre_selectionnee']);
        $offre->setNom($arrayData['nom']);
        $offre->setPrenom($arrayData['prenom']);
        $offre->setAdresse($arrayData['adresse']);
        $offre->setAdresse2($arrayData['adresse2']);
        $offre->setTelephone($arrayData['telephone']);
        $offre->setFax($arrayData['fax']);
        $offre->setCodePostal($arrayData['code_postal_offre']);
        $offre->setVille($arrayData['ville_offre']);
        $offre->setPays($arrayData['pays']);
        $offre->setAcronymePays($arrayData['acronyme_pays']);
        $offre->setSiret($arrayData['siret']);
        $offre->setIdentifiantNational($arrayData['identifiant_national']);
        $offre->setEmail(trim($arrayData['email']));
        $offre->setObservation($arrayData['observation']);
        $offre->setNumPli($arrayData['numero_reponse']);
        if (isset($arrayData['org'])) {
            $offre->setOrganisme($arrayData['org']);
        } else {
            $offre->setOrganisme($arrayData['organisme']);
        }
        if (!$returnEnveloppe) {
            $decisionEnveloppe = new CommonDecisionEnveloppe();
            $decisionEnveloppe->setIdDecisionEnveloppe($arrayData['id_decision_enveloppe']);
            $decisionEnveloppe->setConsultationId($arrayData['consultation_id']);
            if (isset($arrayData['consultationId'])) {
                $decisionEnveloppe->setConsultationId($arrayData['consultationId']);
            } else {
                $decisionEnveloppe->setConsultationId($arrayData['consultationId']);
            }
            $decisionEnveloppe->setIdOffre($arrayData['id_offre']);
            $decisionEnveloppe->setLot($arrayData['lot']);
            $decisionEnveloppe->setDecision($arrayData['decision']);
            $decisionEnveloppe->setDateNotification($arrayData['date_notification']);
            $decisionEnveloppe->setCodePostal($arrayData['code_postal']);
            $decisionEnveloppe->setPmePmi($arrayData['pme_pmi']);
            $decisionEnveloppe->setTrancheBudgetaire($arrayData['tranche_budgetaire']);
            $decisionEnveloppe->setMontantMarche($arrayData['montant_marche']);
            $decisionEnveloppe->setCategorie($arrayData['categorie']);
            $decisionEnveloppe->setObjetMarche($arrayData['objet_marche']);
            $decisionEnveloppe->setCommentaire($arrayData['commentaire']);
            $decisionEnveloppe->setFichierJoint($arrayData['fichier_joint']);
            $decisionEnveloppe->setNomFichierJoint($arrayData['nom_fichier_joint']);
            $decisionEnveloppe->setNote($arrayData['note']);
            $decisionEnveloppe->setVille($arrayData['ville']);
            $decisionEnveloppe->setClassement($arrayData['classement']);
            $decisionEnveloppe->setSirenAttributaire($arrayData['siren_Attributaire']);
            $decisionEnveloppe->setNicAttributaire($arrayData['nic_Attributaire']);
            $decisionEnveloppe->setRcVilleAttributaire($arrayData['rc_ville_attributaire']);
            $decisionEnveloppe->setRcNumAttributaire($arrayData['rc_num_attributaire']);
            $decisionEnveloppe->setIdentifiantnationalAttributaire($arrayData['identifiantNational_Attributaire']);
            $decisionEnveloppe->setAcronymepaysAttributaire($arrayData['acronymePays_Attributaire']);
            $decisionEnveloppe->setPaysAttributaire($arrayData['pays_Attributaire']);
            $decisionEnveloppe->setDateFinMarchePrevisionnel($arrayData['date_fin_marche_previsionnel']);
            $decisionEnveloppe->setDateFinMarcheReelle($arrayData['date_fin_marche_reelle']);
            $decisionEnveloppe->setDateNotificationReelle($arrayData['date_notification_reelle']);
            $offre->setCommonDecisionEnveloppe($decisionEnveloppe);
            $decisionEnveloppe->setNumeroMarche($arrayData['numero_marche']);
            if (isset($arrayData['org'])) {
                $decisionEnveloppe->setOrganisme($arrayData['org']);
            } else {
                $decisionEnveloppe->setOrganisme($arrayData['organisme']);
            }

            return $offre;
        }
        $enveloppe = new CommonEnveloppePapier();
        $enveloppe->setIdEnveloppePapier($arrayData['id_enveloppe_papier']);
        $enveloppe->setNumPli($arrayData['numero_reponse']);
        $enveloppe->setOffrePapierId($arrayData['offre_papier_id']);
        $enveloppe->setStatut($arrayData['statut']);
        $enveloppe->setSupprime($arrayData['supprime']);
        $enveloppe->setCryptage($arrayData['cryptage']);
        $enveloppe->setIsSend($arrayData['is_send']);
        $enveloppe->setTypeEnv($arrayData['type_env']);
        $enveloppe->setSousPli($arrayData['sous_pli']);
        $enveloppe->setChampsOptionnels($arrayData['champs_optionnels']);
        $enveloppe->setAgentIdOuverture($arrayData['agent_id_ouverture']);
        $enveloppe->setDateheureOuverture($arrayData['dateheure_ouverture']);
        $enveloppe->setStatutEnveloppe($arrayData['statut_enveloppe']);
        $enveloppe->setNomAgentOuverture($arrayData['nom_agent_ouverture']);
        $enveloppe->setLibelleStatutEnveloppe((new Atexo_Consultation_StatutEnveloppe())->getStatutEnveloppeById($enveloppe->getStatutEnveloppe()));
        $enveloppe->setCommonOffrePapier($offre);
        if (isset($arrayData['org'])) {
            $enveloppe->setOrganisme($arrayData['org']);
        } else {
            $enveloppe->setOrganisme($arrayData['organisme']);
        }

        return $enveloppe;
    }

    public function sortByElement(&$enveloppes, $trier, $sensTri)
    {
        $enveloppeDeTest = null;
        if (is_array($enveloppes)) {
            $enveloppeDeTest = $enveloppes[0];
            if (!($enveloppeDeTest instanceof CommonEnveloppe) && !($enveloppeDeTest instanceof CommonEnveloppePapier)) {
                throw new Atexo_Exception('unsupported Object');
            }
        }

        $sortByObject = null;
        if (!method_exists($enveloppeDeTest, 'get' . $trier)) {
            if ($enveloppeDeTest instanceof CommonEnveloppe) {
                if (method_exists(new CommonOffres(), 'get' . $trier)) {
                    $sortByObject = 'CommonOffres';
                } else {
                    return;
                }
            } elseif ($enveloppeDeTest instanceof CommonEnveloppePapier) {
                if (method_exists(new CommonOffrePapier(), 'get' . $trier)) {
                    $sortByObject = 'CommonOffrePapier';
                } else {
                    return;
                }
            } else {
                return;
            }
        }

        $enveloppes = Atexo_Util::sortArrayOfObjects($enveloppes, $trier, $sensTri, $sortByObject);
    }

    //A supprimer apres verification de non utilisation
    public function retrieveAllEnveloppeOffrePapier($idOffre, $consultation, $organisme)
    {
        $enveloppesOffres = self::retrieveEnveloppesOffresPapierByid($idOffre, null, null, true, false, $organisme);

        if (!($consultation instanceof CommonConsultation) && !$consultation) {
            throw new Atexo_Exception('Expected parameter to be either consultation or reference');
        }

        if (!$consultation instanceof CommonConsultation) {
            $consultation = (new Atexo_Consultation())->retrieveConsultation($consultation, $organisme);
        }

        if ($consultation instanceof CommonConsultation) {
            $arrayDescriptionLot = [];
            $consultationAlloti = ((is_countable($consultation->getAllLots()) ? count($consultation->getAllLots()) : 0) > 0) ? true : false;
            if ($consultationAlloti) {
                $arrayDescriptionLot = (new Atexo_Consultation_Lots())->retrieveArrayIntituleLot($consultation->getId(), $organisme);
            } else {
                $intituleConsultation = $consultation->getIntituleTraduit();
            }

            if (is_array($enveloppesOffres)) {
                foreach ($enveloppesOffres as $enveloppe) {
                    if ($enveloppe instanceof CommonEnveloppePapier) {
                        if ($consultationAlloti) {
                            $enveloppe->setIntituleLot($arrayDescriptionLot[$enveloppe->getSousPli()]);
                        } else {
                            $enveloppe->setIntituleLot($intituleConsultation);
                        }
                    }
                }

                return $enveloppesOffres;
            }
        }

        return [];
    }

    public function retrieveLotsForDecision($consultationId, $lot, $organisme)
    {
        $arrayReturn = [];
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));

        $consultation = (new Atexo_Consultation())->retrieveConsultation($consultationId, $organisme);

        if (!($consultation instanceof CommonConsultation)) {
            throw new Atexo_Exception('No Consultation  found');
        }

        if ($consultation->isAllotie()) {
            $c = new Criteria();
            $c->add(CommonCategorieLotPeer::CONSULTATION_ID, $consultationId);
            $c->add(CommonCategorieLotPeer::ORGANISME, $organisme);
            $c->add(CommonCategorieLotPeer::DECISION, Atexo_Config::getParameter('LOT_EN_DECISION'));
            if ($lot || '0' == $lot) {
                $c->add(CommonCategorieLotPeer::LOT, $lot);
            }
            $listeLots = CommonCategorieLotPeer::doSelect($c, $connexionCom);
            if (is_array($listeLots)) {
                foreach ($listeLots as $lot) {
                    $c = new Criteria();
                    $c->add(CommonDecisionLotPeer::CONSULTATION_ID, $consultationId);
                    $c->add(CommonDecisionLotPeer::LOT, $lot->getLot());
                    $c->add(CommonDecisionLotPeer::ORGANISME, $organisme);
                    $decisionLot = CommonDecisionLotPeer::doSelectOne($c, $connexionCom);
                    if (!($decisionLot instanceof CommonDecisionLot)) {
                        $decisionLot = new CommonDecisionLot();
                    }

                    $lot->setDecisionLot($decisionLot);
                }

                if ($listeLots) {
                    return $listeLots;
                } else {
                    return [];
                }
            } else {
                return [];
            }
        } else {
            $c = new Criteria();
            $c->add(CommonDecisionLotPeer::CONSULTATION_ID, $consultationId);
            $c->add(CommonDecisionLotPeer::LOT, '0');
            $c->add(CommonDecisionLotPeer::ORGANISME, $organisme);
            $decisionLot = CommonDecisionLotPeer::doSelectOne($c, $connexionCom);
            if (!($decisionLot instanceof CommonDecisionLot)) {
                $decisionLot = new CommonDecisionLot();
            }
            $consultation->setDecisionLot($decisionLot);
            $arrayReturn[] = $consultation;

            return $arrayReturn;
        }
    }
    public function retrieveReponse($consultationId, $sousPli, $organisme)
    {
        $params = [];
        $sql = ' SELECT Offres.consultation_id as consultationId, Offres.organisme as org, Offres.* '
            . ' FROM Offres Left Join Enveloppe on (Offres.id = Enveloppe.offre_id AND Offres.organisme = Enveloppe.organisme)'
            . ' WHERE Offres.consultation_id=:ref AND Offres.organisme=:organisme ';
        if ($sousPli) {
            $sql .= ' AND Enveloppe.sous_pli=:sousPli ';
            $params[':sousPli'] = $sousPli;
        }
        $sql .= ' AND Offres.depot_annule=:depotNonAnnule ';
        $sql .= ' AND Offres.statut_offres != :statutBrouillon ';
        $sql .= ' ORDER BY Offres.untrusteddate ASC ';

        $params[':ref'] = $consultationId;
        $params[':depotNonAnnule'] = Atexo_Config::getParameter('DEPOT_NON_ANNULE');
        $params[':organisme'] = $organisme;
        $params[':statutBrouillon'] = Atexo_Config::getParameter('STATUT_ENV_BROUILLON');
        $statement = Atexo_Db::getLinkCommon(true)->prepare($sql);
        $statement->execute($params);

        $arraysEnveloppes = [];
        $arrayIdsEnveloppes = [];
        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            if (!isset($arrayIdsEnveloppes[$row['id']])) {
                $enveloppe = self::fillObjectFromArray($row, false);
                $arraysEnveloppes[] = $enveloppe;
                $arrayIdsEnveloppes[$row['id']] = true;
            }
        }
        self::addCompanyName($arraysEnveloppes, null, $organisme, true);

        return $arraysEnveloppes;
    }
    public function retrieveReponseAdmissible($consultationId, $sousPli, $organisme)
    {
        $params = [];
        $sql = ' select Offres.consultation_id as consultationId, Offres.organisme as org, Offres.*, decisionEnveloppe.* '
            . ' from Offres left join decisionEnveloppe on (Offres.id=decisionEnveloppe.id_offre AND Offres.organisme=decisionEnveloppe.organisme AND decisionEnveloppe.organisme=:organisme '
            . ' AND lot=' . $sousPli . " AND decisionEnveloppe.type_enveloppe='" . Atexo_Config::getParameter('DEPOT_ELECTRONIQUE') . "') , Admissibilite_Enveloppe_Lot as Adm  "
            . ' Where Adm.id_Offre=Offres.id AND Adm.organisme=Offres.organisme AND  Adm.admissibilite=:Admissible AND Offres.consultation_id=:ref AND Offres.organisme=:organisme ';
        if ($sousPli) {
            $sql .= ' AND Adm.sous_pli=:sousPli ';
            $params[':sousPli'] = $sousPli;
        }
        $sql .= ' AND Offres.depot_annule=:depotNonAnnule ';
        $sql .= ' AND Offres.statut_offres != :statutBrouillon ';
        $sql .= ' ORDER BY Offres.untrusteddate ASC ';

        $params[':Admissible'] = Atexo_Config::getParameter('LOT_ADMISSIBLE');
        $params[':ref'] = $consultationId;
        $params[':depotNonAnnule'] = Atexo_Config::getParameter('DEPOT_NON_ANNULE');
        $params[':organisme'] = $organisme;
        $params[':statutBrouillon'] = Atexo_Config::getParameter('STATUT_ENV_BROUILLON');
        $statement = Atexo_Db::getLinkCommon(true)->prepare($sql);
        $statement->execute($params);

        $arraysEnveloppes = [];
        $arrayIdsEnveloppes = [];
        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            if (!isset($arrayIdsEnveloppes[$row['id']])) {
                $enveloppe = self::fillObjectFromArray($row, false);
                $arraysEnveloppes[] = $enveloppe;
                $arrayIdsEnveloppes[$row['id']] = true;
            }
        }
        self::addCompanyName($arraysEnveloppes, null, $organisme, true);

        return $arraysEnveloppes;
    }

    public function retrieveReponsePapierAdmissible($consultationId, $sousPli, $organisme)
    {
        $params = [];
        $sql = ' select Offre_papier.consultation_id as consultationId, Offre_papier.organisme as org, Offre_papier.*, decisionEnveloppe.* '
            . ' from Offre_papier left join  decisionEnveloppe on (Offre_papier.id=decisionEnveloppe.id_offre AND Offre_papier.organisme=decisionEnveloppe.organisme AND lot=' . $sousPli
            . " AND decisionEnveloppe.type_enveloppe='" . Atexo_Config::getParameter('DEPOT_PAPIER') . "'), Admissibilite_Enveloppe_papier_Lot as Adm"
            . ' Where Adm.id_offre_papier=Offre_papier.id AND  Adm.organisme=Offre_papier.organisme AND Adm.admissibilite=:Admissible AND Offre_papier.consultation_id=:ref ';
        if ($sousPli) {
            $sql .= ' AND Adm.sous_pli=:sousPli ';
            $params[':sousPli'] = $sousPli;
        }
        $sql .= ' AND Offre_papier.depot_annule=:depotNonAnnule AND Offre_papier.organisme=:organisme ';
        $sql .= ' ORDER BY Offre_papier.date_depot ASC ';
        $params[':Admissible'] = Atexo_Config::getParameter('LOT_ADMISSIBLE');
        $params[':ref'] = $consultationId;
        $params[':depotNonAnnule'] = Atexo_Config::getParameter('DEPOT_NON_ANNULE');
        $params[':organisme'] = $organisme;

        $statement = Atexo_Db::getLinkCommon(true)->prepare($sql);
        $statement->execute($params);

        $arraysEnveloppes = [];
        $arrayIdsEnveloppes = [];
        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            if (!isset($arrayIdsEnveloppes[$row['id']])) {
                $enveloppe = self::fillObjectEnveloppePapier($row, false);
                $arraysEnveloppes[] = $enveloppe;
                $arrayIdsEnveloppes[$row['id']] = true;
            }
        }
        self::addCompanyName($arraysEnveloppes, null, $organisme, true);

        return $arraysEnveloppes;
    }

    public function retrievePlisASupprimer($tri = null, $sensTri = 'ASC', $limit = 0, $offset = 0)
    {
        $pliElectronique = self::retrievePlisElectroniqueASupprimer();
        $pliPapier = self::retrievePlisPapierASupprimer();
        if (!is_array($pliElectronique)) {
            $pliElectronique = [];
        }
        if (!is_array($pliPapier)) {
            $pliPapier = [];
        }
        $plis = array_merge($pliElectronique, $pliPapier);
        if (!$tri) {
            $tri = 'datefin';
        }

        $plis = Atexo_Util::sortArrayOfObjects($plis, $tri, $sensTri, null);

        if ($limit) {
            $plis = array_slice($plis, $offset, $limit);
        }

        return $plis;
    }

    public function retrievePlisElectroniqueASupprimer()
    {
        $params = [];
        $query = ' Select Enveloppe.statut_enveloppe, Enveloppe.type_env, Offres.id, Offres.nom_entreprise_inscrit , Enveloppe.sous_pli, Enveloppe.id_enveloppe_electro as idEnveloppe, '
              . ' Offres.entreprise_id, Offres.consultation_id, 0 as typeEnveloppe from Offres, Enveloppe Where  Offres.id=Enveloppe.offre_id '
              . ' AND Offres.organisme=Enveloppe.organisme AND Enveloppe.statut_enveloppe=:statutRefuse AND Offres.organisme=:organisme '
              . ' UNION '
              . ' Select Enveloppe.statut_enveloppe, Enveloppe.type_env,  Offres.id, Offres.nom_entreprise_inscrit, Enveloppe.sous_pli, Enveloppe.id_enveloppe_electro as idEnveloppe, '
              . ' Offres.entreprise_id, Offres.consultation_id, Admissibilite_Enveloppe_Lot.type_enveloppe as typeEnveloppe from Offres, Enveloppe, Admissibilite_Enveloppe_Lot Where '
              . ' Offres.id=Enveloppe.offre_id AND Offres.organisme=Enveloppe.organisme AND Offres.id=Admissibilite_Enveloppe_Lot.id_Offre'
              . ' AND Offres.organisme=Admissibilite_Enveloppe_Lot.organisme AND Enveloppe.sous_pli=Admissibilite_Enveloppe_Lot.sous_pli '
              . ' AND Admissibilite_Enveloppe_Lot.admissibilite=:nonAdmissible AND type_env=:envOffre AND Enveloppe.statut_enveloppe!=:supprimer '
              . ' AND Offres.organisme=:organisme';

        $params[':statutRefuse'] = Atexo_Config::getParameter('STATUT_ENV_REFUSEE');
        $params[':nonAdmissible'] = Atexo_Config::getParameter('LOT_NON_ADMISSIBLE');
        $params[':envOffre'] = Atexo_Config::getParameter('TYPE_ENV_OFFRE');
        $params[':supprimer'] = Atexo_Config::getParameter('STATUT_ENV_SUPPRIMER');
        $params[':organisme'] = Atexo_CurrentUser::getOrganismAcronym();

        $statement = Atexo_Db::getLinkCommon(true)->prepare($query);
        $statement->execute($params);

        $listeEnveloppesASupprimer = [];
        $datas = [];
        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            $datas[] = $row;
        }
        foreach ($datas as $data) {
            $suppressionPli = self::fillSuppressionPliObject($data);
            if ($suppressionPli instanceof Atexo_Consultation_SuppressionPliVo) {
                $listeEnveloppesASupprimer[] = $suppressionPli;
            }
        }

        return $listeEnveloppesASupprimer;
    }

    public function retrievePlisPapierASupprimer()
    {
        $params = [];
        $query = ' Select Enveloppe_papier.statut_enveloppe, Enveloppe_papier.type_env, Enveloppe_papier.sous_pli, Offre_papier.id , Enveloppe_papier.id_enveloppe_papier as idEnveloppe, '
              . ' Offre_papier.nom_entreprise, Offre_papier.consultation_id, 0 as typeEnveloppe from Offre_papier, Enveloppe_papier Where  Offre_papier.id=Enveloppe_papier.offre_papier_id AND '
              . ' Offre_papier.organisme=Enveloppe_papier.organisme AND Enveloppe_papier.statut_enveloppe=:statutRefuse '
              . ' UNION '
              . ' Select Enveloppe_papier.statut_enveloppe, Enveloppe_papier.type_env, Enveloppe_papier.sous_pli, Offre_papier.id , Enveloppe_papier.id_enveloppe_papier as idEnveloppe, '
              . ' Offre_papier.nom_entreprise, Offre_papier.consultation_id, Admissibilite_Enveloppe_papier_Lot.type_enveloppe as typeEnveloppe from Offre_papier, Enveloppe_papier, '
              . ' Admissibilite_Enveloppe_papier_Lot Where Offre_papier.id=Enveloppe_papier.offre_papier_id AND Offre_papier.organisme=Enveloppe_papier.organisme '
              . ' AND Offre_papier.id=Admissibilite_Enveloppe_papier_Lot.id_Offre_papier AND Offre_papier.organisme=Admissibilite_Enveloppe_papier_Lot.organisme '
              . ' AND Enveloppe_papier.sous_pli=Admissibilite_Enveloppe_papier_Lot.sous_pli AND Admissibilite_Enveloppe_papier_Lot.admissibilite=:nonAdmissible AND type_env=:envOffre '
              . ' AND Enveloppe_papier.statut_enveloppe!=:supprimer ';

        $params[':statutRefuse'] = Atexo_Config::getParameter('STATUT_ENV_REFUSEE');
        $params[':nonAdmissible'] = Atexo_Config::getParameter('LOT_NON_ADMISSIBLE');
        $params[':envOffre'] = Atexo_Config::getParameter('TYPE_ENV_OFFRE');
        $params[':supprimer'] = Atexo_Config::getParameter('STATUT_ENV_SUPPRIMER');

        $statement = Atexo_Db::getLinkCommon(true)->prepare($query);
        $statement->execute($params);

        $listeEnveloppesASupprimer = [];
        $datas = [];
        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            $datas[] = $row;
        }
        foreach ($datas as $data) {
            $suppressionPli = self::fillSuppressionPliObject($data);
            if ($suppressionPli instanceof Atexo_Consultation_SuppressionPliVo) {
                $listeEnveloppesASupprimer[] = $suppressionPli;
            }
        }

        return $listeEnveloppesASupprimer;
    }

    public function fillSuppressionPliObject($arrayData)
    {
        $criteria = new Atexo_Consultation_CriteriaVo();
        $criteria->setcalledFromPortail(false);
        $criteria->setIdService(Atexo_CurrentUser::getCurrentServiceId());
        $criteria->setAcronymeOrganisme(Atexo_CurrentUser::getCurrentOrganism());
        $criteria->setIdReference($arrayData['consultation_id']);
        $criteria->setConnectedAgentId(Atexo_CurrentUser::getId());
        $consultation = (new Atexo_Consultation())->search($criteria);
        if (is_array($consultation) && 1 == count($consultation)) {
            $consultation = array_shift($consultation);
            if ($consultation instanceof CommonConsultation) {
                $suppressionPli = new Atexo_Consultation_SuppressionPliVo();
                $suppressionPli->setId($arrayData['consultation_id']);
                $suppressionPli->setReferenceUtilisateur($consultation->getReferenceUtilisateur());
                $suppressionPli->setIdTypeProcedure($consultation->getIdTypeProcedure());
                $suppressionPli->setStatusConsultation($consultation->getStatusConsultation());
                $suppressionPli->setNomCreateur($consultation->getNomCreateur());
                $suppressionPli->setPrenomCreateur($consultation->getPrenomCreateur());
                $suppressionPli->setTitre($consultation->getIntitule());
                $suppressionPli->setResume($consultation->getObjet());
                $suppressionPli->setChampSuppInvisible($consultation->getChampSuppInvisible());
                $suppressionPli->setDateFin($consultation->getDateFin());
                $suppressionPli->setIdOffre($arrayData['id']);
                $suppressionPli->setIdEnveloppe($arrayData['idEnveloppe']);
                if (isset($arrayData['nom_entreprise_inscrit'])) {
                    $suppressionPli->setNatureEnveloppe(Atexo_Config::getParameter('DEPOT_ELECTRONIQUE'));
                    $suppressionPli->setNomEntreprise($arrayData['nom_entreprise_inscrit'] ?: '-');
                } elseif (isset($arrayData['nom_entreprise'])) {
                    $suppressionPli->setNomEntreprise($arrayData['nom_entreprise']);
                    $suppressionPli->setNatureEnveloppe(Atexo_Config::getParameter('DEPOT_PAPIER'));
                }
                $suppressionPli->setTypeEnv($arrayData['type_env']);
                $suppressionPli->setSousPli($arrayData['sous_pli']);
                $suppressionPli->setEnveloppesExistante(self::getTypesEnveloppes(
                    $consultation->getEnvCandidature(),
                    $consultation->getEnvOffre(),
                    $consultation->getEnvAnonymat(),
                    $consultation->getEnvOffreTechnique()
                ));
                $suppressionPli->setStatutEnveloppe($arrayData['statut_enveloppe']);
                $suppressionPli->setTypeEnveloppe($arrayData['typeEnveloppe']);
                $suppressionPli->setOrganisme($arrayData['organisme']);

                return $suppressionPli;
            }
        }

        return null;
    }

    public function allEnveloppeDecisionByReference($consultationId, $organisme, $numLot = null)
    {
        $commonConnexon = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $arrayDecisionEnveloppe = [];
        $c = new Criteria();

        $c->add(CommonDecisionEnveloppePeer::CONSULTATION_ID, $consultationId, Criteria::EQUAL);
        if (null !== $numLot) {
            $c->add(CommonDecisionEnveloppePeer::LOT, $numLot, Criteria::EQUAL);
        }
        $c->add(CommonDecisionEnveloppePeer::ORGANISME, $organisme, Criteria::EQUAL);
        $enveloppeDecisions = CommonDecisionEnveloppePeer::doSelect($c, $commonConnexon);

        if (is_array($enveloppeDecisions) && 0 != count($enveloppeDecisions)) {
            foreach ($enveloppeDecisions as $enveloppeDecision) {
                if ($enveloppeDecision->getTypeEnveloppe() == Atexo_Config::getParameter('TYPE_DEPOT_QUESTION_ELECTRONIQUE')) {
                    $offre = self::retrieveOffreById($enveloppeDecision->getIdOffre(), $organisme);
                    if ($offre) {
                        $enveloppeDecision->setNomEntreprise($offre->getNomEntrepriseInscrit());
                    }
                } elseif ($enveloppeDecision->getTypeEnveloppe() == Atexo_Config::getParameter('TYPE_DEPOT_QUESTION_PAPIER')) {
                    $offrePapier = self::retrieveOffrePapierById($enveloppeDecision->getIdOffre(), $organisme);
                    if ($offrePapier) {
                        $enveloppeDecision->setNomEntreprise($offrePapier->getNomEntreprise());
                    }
                }
                $arrayDecisionEnveloppe[] = $enveloppeDecision;
            }
        }
        if (is_array($arrayDecisionEnveloppe)) {
            return $arrayDecisionEnveloppe;
        } else {
            return false;
        }
    }

    /**
     * permet de retourner l'offre.
     *
     * @param int    $idOffre
     * @param string $Org
     *
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     * @copyright Atexo 2016
     */
    public static function retrieveOffreById($idOffre, $Org): \Application\Propel\Mpe\CommonOffres|false
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $offre = CommonOffresPeer::retrieveByPK($idOffre, $Org, $connexion);
        if ($offre) {
            return $offre;
        } else {
            return false;
        }
    }

    public function retrieveEnrepriseById($idEntreprise, $commonConnexion)
    {
        $entreprise = EntreprisePeer::retrieveByPK($idEntreprise, $commonConnexion);
        if ($entreprise) {
            return $entreprise;
        } else {
            return false;
        }
    }

    public function retrieveOffrePapierById($idOffrePapier, $organisme)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $offrePapier = CommonOffrePapierPeer::retrieveByPK($idOffrePapier, $organisme, $connexionCom);
        if ($offrePapier) {
            return $offrePapier;
        } else {
            return false;
        }
    }

    public function ifOrganismeInvite($orgEmetteur, $orgInvite, $consultationId, $idLot)
    {
        $c = new Criteria();
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));

        $c->add(CommonInvitationConsultationTransversePeer::ORGANISME_EMETTEUR, $orgEmetteur, Criteria::EQUAL);
        $c->add(CommonInvitationConsultationTransversePeer::ORGANISME_INVITE, $orgInvite, Criteria::EQUAL);
        $c->add(CommonInvitationConsultationTransversePeer::CONSULTATION_ID, $consultationId, Criteria::EQUAL);
        $c->add(CommonInvitationConsultationTransversePeer::LOT, $idLot, Criteria::EQUAL);

        return 0 != count(CommonInvitationConsultationTransversePeer::doSelect($c, $connexionCom));
    }

    public function retrieveInscritResponses($idInscrit, $limit = 0, $offset = 0, $trierPar = '', $sensTri = '', $returnNombreOfElement = false)
    {
        $nestedObjects = null;
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));

        $c = new Criteria();
        $c->add(CommonOffresPeer::INSCRIT_ID, $idInscrit);
        $c->add(CommonOffresPeer::STATUT_OFFRES, Atexo_Config::getParameter('STATUT_ENV_BROUILLON'), Criteria::NOT_EQUAL);
        $offres = CommonOffresPeer::doSelect($c, $connexion);

        if (is_array($offres)) {
            foreach ($offres as $offre) {
                if ($offre instanceof CommonOffres) {
                    $commonConsultation = (new Atexo_Consultation())->retrieveCommonConsultation($offre->getConsultationId(), $offre->getOrganisme());
                    if ($commonConsultation instanceof CommonConsultation) {
                        $categorie = Atexo_Consultation_Category::retrieveCategorie($commonConsultation->getCategorie(), true);
                        if ($categorie instanceof CommonCategorieConsultation) {
                            $commonConsultation->setLibelleCategorieConsultation($categorie->getLibelle());
                        }

                        $offre->setConsultation($commonConsultation);
                    }
                }
            }
        } else {
            $offres = [];
        }

        if ($returnNombreOfElement) {
            return count($offres);
        } else {
            if ($trierPar) {
                if (method_exists(new CommonOffres(), 'get' . $trierPar)) {
                    $nestedObjects = null;
                } elseif (method_exists(new CommonConsultation(), 'get' . $trierPar)) {
                    $nestedObjects = 'Consultation';
                }
                $offres = Atexo_Util::sortArrayOfObjects($offres, $trierPar, $sensTri, $nestedObjects);
            }

            if ($limit) {
                $offres = array_slice($offres, $offset, $limit);
            }

            return $offres;
        }
    }

    public function retrieveEnveloppeFiles($idEnveloppe, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonFichierEnveloppePeer::ID_ENVELOPPE, $idEnveloppe);
        $c->add(CommonFichierEnveloppePeer::ORGANISME, $organisme);
        $fichiers = CommonFichierEnveloppePeer::doSelect($c, $connexion);
        if ($fichiers) {
            return $fichiers;
        } else {
            return [];
        }
    }

    public function retrieveEnveloppesOffreByIdOffre($idOffre, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonEnveloppePeer::OFFRE_ID, $idOffre);
        $c->add(CommonEnveloppePeer::TYPE_ENV, Atexo_Config::getParameter('TYPE_ENV_OFFRE'));
        $c->add(CommonEnveloppePeer::ORGANISME, $organisme);
        $enveloppesOffre = CommonEnveloppePeer::doSelect($c, $connexion);

        return $enveloppesOffre;
    }

    public function retrieveNomEntrepriseFromContrat($contrat, $organisme)
    {
        $idDecisionEnveloppe = $contrat->getIdDecision();
        $decisionEnveloppe = (new Atexo_Consultation_Decision())->retrieveDecisionById($idDecisionEnveloppe, $organisme);

        return self::retrieveNomEntrepriseFromDecision($decisionEnveloppe, $organisme);
    }

    public function retrieveNomEntrepriseFromDecision($decisionEnveloppe, $organisme)
    {
        if ($decisionEnveloppe instanceof CommonDecisionEnveloppe) {
            return self::retrieveNomEntrepriseByIdOffre($decisionEnveloppe->getIdOffre(), $decisionEnveloppe->getTypeEnveloppe(), $organisme);
        }

        return '';
    }

    public function retrieveNomEntrepriseByIdOffre($idOffre, $typeEnveloppe, $organisme = null)
    {
        if (null == $organisme) {
            $organisme = Atexo_CurrentUser::getCurrentOrganism();
        }
        //$propelConnection = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        if ($typeEnveloppe == Atexo_Config::getParameter('DEPOT_ELECTRONIQUE')) {
            $offre = self::retrieveOffreById($idOffre, $organisme);
            if ($offre instanceof CommonOffres) {
                return $offre->getNomEntrepriseInscrit();
            }
        } else {
            $offre = self::retrieveOffrePapierById($idOffre, $organisme);
            if ($offre instanceof CommonOffrePapier) {
                return $offre->getNomEntreprise();
            }
        }

        return '';
    }

    public function retrieveEnveloppesByIdOffre($idOffre, $organisme, $typeEnv = null, $sousPli = null)
    {
        $params = [];
        $query = ' SELECT Enveloppe. * , Offres. *  FROM Enveloppe, Offres WHERE Enveloppe.offre_id=:idOffre '
                . 'AND Enveloppe.offre_id = Offres.id  '
                . 'AND Enveloppe.organisme = Offres.organisme AND Enveloppe.organisme =:organisme ';
        if (null !== $sousPli) {
            $params[':sousPlis'] = $sousPli;
            $query .= ' AND Enveloppe.sous_pli=:sousPlis';
        }
        if (null !== $typeEnv) {
            $params[':typeEnv'] = $typeEnv;
            $query .= ' AND Enveloppe.TYPE_ENV =:typeEnv ';
        }
        $query .= ' AND Enveloppe.enveloppe_fictive =:envFictive ';
        $query .= 'ORDER BY Enveloppe.type_env,Enveloppe.sous_pli  ASC';
        $params[':idOffre'] = $idOffre;
        $params[':organisme'] = $organisme;
        $params[':envFictive'] = 0;
        $statement = Atexo_Db::getLinkCommon(true)->prepare($query);
        $statement->execute($params);
        $arraysEnveloppes = [];
        $arrayIds = [];
        $returnOffreWithinEnveloppe = true;
        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            if ($returnOffreWithinEnveloppe) {
                $id = $row['id_enveloppe_electro'];
            } else {
                $id = $row['id'];
            }
            if (!isset($arrayIds[$id])) {
                $enveloppe = self::fillObjectFromArray($row, $returnOffreWithinEnveloppe);
                $arraysEnveloppes[] = $enveloppe;
                $arrayIds[$id] = true;
            }
        }

        return $arraysEnveloppes;
    }

    public function retrieveFichierEnveloppeByIdEnveloppe($idEnveloppe, $organisme, $typeFichier = 'PRI', $xmlFormat = null)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonFichierEnveloppePeer::ID_ENVELOPPE, $idEnveloppe);
        $c->add(CommonFichierEnveloppePeer::TYPE_FICHIER, $typeFichier);
        if ($xmlFormat) {
            $c->add(CommonFichierEnveloppePeer::NOM_FICHIER, '%' . 'xml' . '%', Criteria::LIKE);
        }
        $c->add(CommonFichierEnveloppePeer::ORGANISME, $organisme);
        $fichier = CommonFichierEnveloppePeer::doSelectOne($c, $connexion);

        return $fichier;
    }

    public function retrieveEnveloppesPapiersByIdOffre($idOffre, $organisme, $typeEnv = null, $sousPli = null)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonEnveloppePapierPeer::OFFRE_PAPIER_ID, $idOffre);
        $c->add(CommonEnveloppePapierPeer::ORGANISME, $organisme);
        if (null !== $typeEnv) {
            $c->add(CommonEnveloppePapierPeer::TYPE_ENV, $typeEnv);
        }
        if (null !== $sousPli) {
            $c->add(CommonEnveloppePapierPeer::SOUS_PLI, $sousPli);
        }
        $enveloppesOffre = CommonEnveloppePapierPeer::doSelect($c, $connexion);

        return $enveloppesOffre;
    }

    public function retrieveAllFichierEnveloppeByIdEnveloppe($idEnveloppe, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonFichierEnveloppePeer::ID_ENVELOPPE, $idEnveloppe);
        $c->add(CommonFichierEnveloppePeer::ORGANISME, $organisme);
        $fichier = CommonFichierEnveloppePeer::doSelect($c, $connexion);

        return $fichier;
    }

    /**
     *   on recupère les offres de la consultation.
     */
    public function retrieveOffreByConsRef($consultationId, $organisme)
    {
        $logger = Atexo_LoggerManager::getLogger('app');
        try {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
            $c = new Criteria();
            $c->add(CommonOffresPeer::CONSULTATION_ID, $consultationId);
            $c->add(CommonOffresPeer::ORGANISME, $organisme);
            $c->add(CommonOffresPeer::STATUT_OFFRES, Atexo_Config::getParameter('STATUT_ENV_BROUILLON'), Criteria::NOT_EQUAL);
            $offres = CommonOffresPeer::doSelect($c, $connexion);
            if ($offres) {
                return $offres;
            }

            return false;
        } catch (Exception $e) {
            $logger->error('Erreur lors de la recuperation des offres electroniques de la consultation.' . PHP_EOL . 'Erreur: ' . $e->getMessage() . PHP_EOL . 'Trace: ' . $e->getTraceAsString());
        }
    }

    /**
     *   on recupère les offres papierde la consultation.
     */
    public function retrieveOffrePapierByConsRef($consultationId, $organisme)
    {
        $logger = Atexo_LoggerManager::getLogger('app');
        try {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
            $c = new Criteria();
            $c->add(CommonOffrePapierPeer::CONSULTATION_ID, $consultationId);
            $c->add(CommonOffrePapierPeer::ORGANISME, $organisme);
            $offres = CommonOffrePapierPeer::doSelect($c, $connexion);
            if ($offres) {
                return $offres;
            }

            return [];
        } catch (Exception $e) {
            $logger->error('Erreur lors de la recuperation des offres papiers de la consultation.' . PHP_EOL . 'Erreur: ' . $e->getMessage() . PHP_EOL . 'Trace: ' . $e->getTraceAsString());
        }
    }

    public function enveloppeSuivante($enveloppeEnCours, $withCandidature, $withOffreTechnique, $withOffre, $withAnonymat)
    {
        if ($enveloppeEnCours == Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE')) {
            if ($withOffreTechnique) {
                return Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE');
            } elseif ($withOffre) {
                return Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE');
            } elseif ($withAnonymat) {
                return Atexo_Config::getParameter('TYPE_ENV_ANONYMAT');
            }
        } elseif ($enveloppeEnCours == Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE')) {
            if ($withOffre) {
                return Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE');
            } elseif ($withAnonymat) {
                return Atexo_Config::getParameter('TYPE_ENV_ANONYMAT');
            }
        } elseif ($enveloppeEnCours == Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE')) {
            if ($withAnonymat) {
                return Atexo_Config::getParameter('TYPE_ENV_ANONYMAT');
            }
        }

        return false;
    }

    public function verifierSiUnPliOuvert($idOffre, $organisme)
    {
        $propelConnection = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonEnveloppePeer::OFFRE_ID, $idOffre);
        $c->add(CommonEnveloppePeer::ORGANISME, $organisme);
        $enveloppes = CommonEnveloppePeer::doSelect($c, $propelConnection);

        if (is_array($enveloppes)) {
            foreach ($enveloppes as $enveloppe) {
                if (
                    $enveloppe->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_EN_LIGNE') ||
                    $enveloppe->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_HORS_LIGNE') ||
                    $enveloppe->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_A_DISTANCE') ||
                    $enveloppe->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE')
                ) {
                    return true;
                }
            }
        }

        return false;
    }

    public function enveloppePrecedente($enveloppeEnCours, $withCandidature, $withOffreTechnique, $withOffre, $withAnonymat)
    {
        if ($enveloppeEnCours == Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE')) {
            return false;
        } elseif ($enveloppeEnCours == Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE')) {
            if ($withCandidature) {
                return Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE');
            } elseif ($withAnonymat) {
                return false;
            }
        } elseif ($enveloppeEnCours == Atexo_Config::getParameter('TYPE_ENV_OFFRE')) {
            if ($withOffreTechnique) {
                return Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE');
            } elseif ($withCandidature) {
                return Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE');
            } else {
                return false;
            }
        } elseif ($enveloppeEnCours == Atexo_Config::getParameter('TYPE_ENV_ANONYMAT')) {
            if ($withOffre) {
                return Atexo_Config::getParameter('TYPE_ENV_OFFRE');
            } elseif ($withOffreTechnique) {
                return Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE');
            } elseif ($withCandidature) {
                return Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE');
            } else {
                return false;
            }
        }

        return false;
    }

    public function isPremiereEnveloppe($enveloppeEnCours, $constitutionDossierReponse)
    {
        if ($enveloppeEnCours == Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE')) {
            return true;
        } elseif ($enveloppeEnCours == Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE')) {
            if (self::enveloppeCandidatureExists($constitutionDossierReponse)) {
                return false;
            }
        } elseif ($enveloppeEnCours == Atexo_Config::getParameter('TYPE_ENV_OFFRE')) {
            if (self::enveloppeCandidatureExists($constitutionDossierReponse) || self::enveloppeOffreTechniqueExists($constitutionDossierReponse)) {
                return false;
            }
        } elseif ($enveloppeEnCours == Atexo_Config::getParameter('TYPE_ENV_ANONYMAT')) {
            if (
                self::enveloppeCandidatureExists($constitutionDossierReponse)
                || self::enveloppeOffreTechniqueExists($constitutionDossierReponse)
                || self::enveloppeOffreExists($constitutionDossierReponse)
            ) {
                return false;
            }
        }

        return true;
    }

    public function addResultatAnalyse(array &$reponses)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        foreach ($reponses as $reponse) {
            $c = new Criteria();
            $c->add(CommonResultatAnalysePeer::CONSULTATION_ID, $reponse->getConsultationId());
            $c->add(CommonResultatAnalysePeer::ID_OFFRE, $reponse->getId());
            $c->add(CommonResultatAnalysePeer::LOT, Atexo_Util::atexoHtmlEntities($_GET['lot']));
            $c->add(CommonResultatAnalysePeer::TYPE_ENVELOPPE, $reponse->getTypeEnveloppe());
            $c->add(CommonResultatAnalysePeer::ORGANISME, $reponse->getOrganisme());
            $proposition = CommonResultatAnalysePeer::doSelectOne($c, $connexion);
            if (!$proposition instanceof CommonResultatAnalyse) {
                $proposition = new CommonResultatAnalyse();
            }
            $reponse->setResultatAnalyse($proposition);
        }
    }

    /**
     * Retourne les offres déposées par l'entreprise.
     */
    public function retrieveReponsesEntreprise($consultationId, $organisme, $idInscrit)
    {
        if (!$idInscrit) {
            return [];
        }

        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));

        $c = new Criteria();
        $c->add(CommonOffresPeer::INSCRIT_ID, $idInscrit);
        $c->add(CommonOffresPeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonOffresPeer::ORGANISME, $organisme);
        $c->add(CommonOffresPeer::DEPOT_ANNULE, '0');
        $c->add(CommonOffresPeer::STATUT_OFFRES, Atexo_Config::getParameter('STATUT_ENV_BROUILLON'), Criteria::NOT_EQUAL);
        $offres = CommonOffresPeer::doSelect($c, $connexion);

        return $offres;
    }

    /**
     * Retourne les offres autres ques les offres variantes déposées par l'entreprise.
     */
    public function retrieveReponsesDeBaseEntreprise($consultationId, $organisme, $idInscrit)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));

        $c = new Criteria();
        $c->add(CommonOffresPeer::INSCRIT_ID, $idInscrit);
        $c->add(CommonOffresPeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonOffresPeer::ORGANISME, $organisme);
        $c->add(CommonOffresPeer::DEPOT_ANNULE, '0');
        $c->add(CommonOffresPeer::OFFRE_VARIANTE, '0');
        $c->add(CommonOffresPeer::STATUT_OFFRES, Atexo_Config::getParameter('STATUT_ENV_BROUILLON'), Criteria::NOT_EQUAL);

        $offres = CommonOffresPeer::doSelect($c, $connexion);

        return $offres;
    }

    public function retrieveEnvOff($consultation, $organisme)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $sql = 'SELECT * FROM Offres, Enveloppe WHERE Offres.consultation_id =' . $consultation->getId() .
            " AND Offres.id = Enveloppe.offre_id AND Offres.organisme = Enveloppe.organisme AND Offres.organisme= '" . $organisme . "' ";
        $sql .= " AND Offres.statut_offres != '" . Atexo_Config::getParameter('STATUT_ENV_BROUILLON') . "' AND Enveloppe.statut_enveloppe != '" . Atexo_Config::getParameter('STATUT_ENV_BROUILLON') . "'";
        $sql .= ' ORDER BY untrusteddate ASC';
        $tab = [];
        $statement = Atexo_Db::getLinkCommon(true)->query($sql);
        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            $index = $row['id'];
            $c = new Criteria();
            $c->add(CommonEnveloppePeer::ID_ENVELOPPE_ELECTRO, $row['id_enveloppe_electro']);
            $c->add(CommonEnveloppePeer::ORGANISME, $row['organisme']);
            $enveloppe = CommonEnveloppePeer::doSelectOne($c, $connexionCom);

            if ('' == $tab[$index]['Entreprise']) {
                $tab[$index]['Entreprise'] = self::retrieveCompanyName($enveloppe, $row['type_env'], $organisme);
            }
            $libelle = 'Type_' . $row['type_env'];
            if ($consultation->getAlloti() && $row['sous_pli']) {
                $tab[$index][$libelle][] = $row['sous_pli'];
            } else {
                $tab[$index][$libelle] = 1;
            }

            if ('' == $tab[$index]['NumPli']) {
                $tab[$index]['NumPli'] = Prado::localize('EL_POUR_ELECTRONIQUE') . $row['numero_reponse'];
            }
        }

        return $tab;
    }

    public function getFileSize($idEnveloppe, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonFichierEnveloppePeer::ID_ENVELOPPE, $idEnveloppe);
        $c->add(CommonFichierEnveloppePeer::ORGANISME, $organisme);
        $fichiers = CommonFichierEnveloppePeer::doSelect($c, $connexion);
        $tailleFichier = 0;
        if ($fichiers && is_array($fichiers)) {
            foreach ($fichiers as $fichier) {
                $tailleFichier += $fichier->getTailleFichier() / 1024;
            }
        }

        return '(' . Atexo_Util::arrondirSizeFile($tailleFichier) . ')';
    }

    public function getEnveloppesByIdOffre($idOffre, $org)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonEnveloppePeer::OFFRE_ID, $idOffre);
        $c->add(CommonEnveloppePeer::ORGANISME, $org);
        $enveloppe = CommonEnveloppePeer::doSelect($c, $connexion);
        if ($enveloppe) {
            return $enveloppe;
        } else {
            return [];
        }
    }

    public function getBlocsFichiersEnveloppesByIdEnveloppe($idFichier, $org = '')
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonBlocFichierEnveloppePeer::ID_FICHIER, $idFichier);
        if ($org) {
            $c->add(CommonBlocFichierEnveloppePeer::ORGANISME, $org);
        }
        $blocsfichierEnveloppes = CommonBlocFichierEnveloppePeer::doSelect($c, $connexion);
        if ($blocsfichierEnveloppes) {
            return $blocsfichierEnveloppes;
        } else {
            return [];
        }
    }

    public function retrieveFichierEnveloppeByIdEnveloppeNonAE($idEnveloppe, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonFichierEnveloppePeer::ID_ENVELOPPE, $idEnveloppe);
        $c->add(CommonFichierEnveloppePeer::TYPE_FICHIER, 'ACE', Criteria::NOT_EQUAL);
        $c->add(CommonFichierEnveloppePeer::ORGANISME, $organisme);
        $fichiers = CommonFichierEnveloppePeer::doSelect($c, $connexion);

        return $fichiers;
    }

    public function updateEnveloppeAutoriserOuverture($idEnveloppe, $organisme)
    {
        if (Atexo_Module::isEnabled('fourEyes')) {
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));
            $enveloppe = CommonEnveloppePeer::retrieveByPK($idEnveloppe, $organisme, $connexionCom);
            $enveloppe->setDateheureOuverture(date('Y-m-d H:i:s'));
            $enveloppe->setAgentIdOuverture(Atexo_CurrentUser::getIdAgentConnected());
            $enveloppe->save($connexionCom);
        }
    }

    public function GetDetailsPlis($idPli, $typeEnv, $consultationId, $Organism, $consultation = null)
    {
        $enveloppe = null;
        $infoRapportPDF = [];
        if (!($consultation instanceof CommonConsultation)) {
            $Organism = Atexo_CurrentUser::getCurrentOrganism();
            if ($idPli && $consultationId) {
                $criteria = new Atexo_Consultation_CriteriaVo();
                $criteria->setcalledFromPortail(false);
                $criteria->setIdService(Atexo_CurrentUser::getCurrentServiceId());
                $criteria->setAcronymeOrganisme($Organism);
                $criteria->setConnectedAgentId(Atexo_CurrentUser::getId());
                $criteria->setIdReference($consultationId);
                $consultation = (new Atexo_Consultation())->search($criteria);
                if ($consultation && is_array($consultation) && 1 == count($consultation)) {
                    $consultation = array_shift($consultation);
                }
            }
        }
        if ($consultation instanceof CommonConsultation) {
            $Organism = $consultation->getOrganisme();
            $consultationId = $consultation->getId();
            $typeEnveloppe = self::getTypesEnveloppes($consultation->getEnvCandidature(), $consultation->getEnvOffre(), $consultation->getEnvAnonymat(), $consultation->getEnvOffreTechnique());
            $criteriaResponse = new Atexo_Response_CriteriaVo();
            $criteriaResponse->setReference($consultationId);
            $criteriaResponse->setTypesEnveloppes($typeEnveloppe);
            $criteriaResponse->setOrganisme($Organism);
            switch ($typeEnv) {
                case Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE'):
                    $enveloppes = self::retrieveEnveloppesCandidatureElectroniques($criteriaResponse);
                                                                            $nomTypeEnveloppe = Prado::localize('CANDIDATURE');
                    break;
                case Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE'):
                    $enveloppes = self::retrieveEnveloppesOffresTechniqueElectroniques($consultationId, null, $typeEnveloppe, '', '', $Organism);
                                                                            $nomTypeEnveloppe = Prado::localize('OFFRE');
                    break;
                case Atexo_Config::getParameter('TYPE_ENV_OFFRE'):
                    $enveloppes = self::retrieveEnveloppesOffresElectroniques($criteriaResponse);
                                                                            $nomTypeEnveloppe = Prado::localize('OFFRE');
                    break;
                case Atexo_Config::getParameter('TYPE_ENV_ANONYMAT'):
                    $enveloppes = self::retrieveEnveloppesAnonymatElectroniques($consultationId, null, $typeEnveloppe, '', '', $Organism);
                                                                            $nomTypeEnveloppe = Prado::localize('ANONYMAT');
                    break;
                default:
                    $enveloppes = null;
                    break;
            }
            if ($enveloppes && is_array($enveloppes)) {
                foreach ($enveloppes as $oneEnveloppe) {
                    if ($oneEnveloppe instanceof CommonEnveloppe && $oneEnveloppe->getIdEnveloppeElectro() == $idPli) {
                        $enveloppe = $oneEnveloppe;
                    }
                }
                if ($enveloppe instanceof CommonEnveloppe && $enveloppe->getOffre() instanceof CommonOffres) {
                    $infoRapportPDF['numeroPli'] = $enveloppe->getNumPli();
                    $infoRapportPDF['nomSte'] = $enveloppe->getOffre()->getNomEntrepriseInscrit();
                    $infoRapportPDF['typeEnveloppe'] = $nomTypeEnveloppe;
                    $infoRapportPDF['statutSignature'] = $enveloppe->getResultatValiditeSignature($Organism);
                    $infoRapportPDF['horodatageEnveloppe'] = $enveloppe->getOffre()->getDateRemisePlie();
                    $statutEnveloppe = $enveloppe->getLibelleStatutEnveloppe();
                    if (
                        $enveloppe->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_EN_LIGNE')
                        || $enveloppe->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_HORS_LIGNE')
                        || $enveloppe->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE')
                        || $enveloppe->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_A_DISTANCE')
                    ) {
                        $statutEnveloppe .= ' ' . Prado::localize('DEFINE_TEXT_LE') . ' ' . Atexo_Util::iso2frnDateTime($enveloppe->getDateheureOuverture());
                        if (0 != $enveloppe->getAgentIdOuverture()) {
                            $agent = (new Atexo_Agent())->retrieveAgent($enveloppe->getAgentIdOuverture());
                            if ($agent) {
                                $statutEnveloppe .= ' ' . Prado::localize('TEXT_PAR') . ' ' . Atexo_Util::escapeXmlChars($agent->getPrenom()) . ' ' . Atexo_Util::escapeXmlChars($agent->getNom());
                            }
                            $statutEnveloppe2 = '';
                            if (Atexo_Module::isEnabled('fourEyes')) {
                                if ($enveloppe->getDateheureOuvertureAgent2() && '0000-00-00 00:00:00' != $enveloppe->getDateheureOuvertureAgent2() && $enveloppe->getAgentIdOuverture2()) {
                                    $statutEnveloppe2 = Prado::localize('DEFINE_ET') . ' ' . Prado::localize('DEFINE_TEXT_LE') . ' ' . Atexo_Util::iso2frnDateTime($enveloppe->getDateheureOuvertureAgent2());
                                    $agent = (new Atexo_Agent())->retrieveAgent($enveloppe->getAgentIdOuverture2());
                                    if ($agent) {
                                        $statutEnveloppe2 .= ' ' . Prado::localize('TEXT_PAR') . ' ' . Atexo_Util::escapeXmlChars($agent->getPrenom()) . ' ' . Atexo_Util::escapeXmlChars($agent->getNom());
                                    }
                                }
                            }
                        }
                    }
                    $infoRapportPDF['statutPli'] = $statutEnveloppe;
                }
            }
        }

        return $infoRapportPDF;
    }

    public function getInfosCertif($fichiers, $avecSignature, $typeSignature, $enveloppeCrypte, $callFrom = 'entreprise')
    {
        $arrayInfos = [];
        if ($fichiers) {
            foreach ($fichiers as $unFichier) {
                if ($unFichier instanceof CommonFichierEnveloppe) {
                    if (empty($unFichier->getSignatureInfos()) && Atexo_Config::getParameter('ACTIVATION_SERVEUR_CRYPTOGRAPHIQUE')) {
                        self::getInfoSignatureViaServCrypto($arrayInfos, $unFichier, $avecSignature, $enveloppeCrypte, $callFrom);
                    }
                }
            }
        }

        return $arrayInfos;
    }

    public function getInfosCertifForRapportVerifSignature($fichiers, $avecSignature, $enveloppeCrypte, $callFrom = 'entreprise')
    {
        $arrayInfos = [];
        if ($fichiers) {
            foreach ($fichiers as $unFichier) {
                if (
                    $unFichier instanceof CommonFichierEnveloppe
                    && !empty($unFichier->getSignatureInfos())
                    && Atexo_Config::getParameter('ACTIVATION_SERVEUR_CRYPTOGRAPHIQUE')
                ) {
                    self::getInfoSignatureViaServCrypto($arrayInfos, $unFichier, $avecSignature, $enveloppeCrypte, $callFrom);
                }
            }
        }

        return $arrayInfos;
    }

    public static function getInfoSignatureViaOpenSSL(&$arrayInfos, $unFichier, $avecSignature, $typeSignature, $enveloppeCrypte, $callFrom)
    {
        if ($unFichier instanceof CommonFichierEnveloppe) {
            $arrayInfos[$unFichier->getIdFichier()]['idFichier'] = $unFichier->getIdFichier();
            $arrayInfos[$unFichier->getIdFichier()]['nomfichier'] = $unFichier->getNomFichier();
            $arrayInfos[$unFichier->getIdFichier()]['EmailSubject'] = $unFichier->getEmailSubject($avecSignature, $typeSignature);
            $arrayInfos[$unFichier->getIdFichier()]['CnSubject'] = $unFichier->getCnSubject($avecSignature, $typeSignature);
            $arrayInfos[$unFichier->getIdFichier()]['OuSubject'] = $unFichier->getOuSubject($avecSignature, $typeSignature);
            $arrayInfos[$unFichier->getIdFichier()]['OSubject'] = $unFichier->getOSubject($avecSignature, $typeSignature);
            $arrayInfos[$unFichier->getIdFichier()]['CSubject'] = $unFichier->getCObject($avecSignature, $typeSignature);
            $arrayInfos[$unFichier->getIdFichier()]['CnEmmeteur'] = $unFichier->getCnEmmeteur($avecSignature, $typeSignature);
            $arrayInfos[$unFichier->getIdFichier()]['OuEmmeteur'] = $unFichier->getOuEmetteur($avecSignature, $typeSignature);
            $arrayInfos[$unFichier->getIdFichier()]['OEmmeteur'] = $unFichier->getOEmetteur($avecSignature, $typeSignature);
            $arrayInfos[$unFichier->getIdFichier()]['CEmmeteur'] = $unFichier->getCEmetteur($avecSignature, $typeSignature);

            $periodeValidite = $unFichier->getStatutCertificatPeriodeValidite($avecSignature);
            $cnValidite = $unFichier->getStatutValiditeChaineCertification($avecSignature);
            $arrayInfos[$unFichier->getIdFichier()]['CertifPeriodValidite'] = Prado::localize($periodeValidite);
            $arrayInfos[$unFichier->getIdFichier()]['CertifPeriodValiditeIcone'] = $periodeValidite;
            $arrayInfos[$unFichier->getIdFichier()]['CertifChaineCertif'] = Prado::localize($cnValidite);
            $arrayInfos[$unFichier->getIdFichier()]['CertifChaineCertifIcone'] = $cnValidite;

            if (Atexo_Module::isEnabled('SurchargeReferentiels')) {
                $arrayInfos[$unFichier->getIdFichier()]['ReferentielChaineCertif'] = Prado::localize('DEFINE_REFERENTIEL_DERTIFICAT') .
                    ' : ' . $unFichier->getNameReferentielCertificat();
            } else {
                $arrayInfos[$unFichier->getIdFichier()]['ReferentielChaineCertif'] = '';
            }

            $crl = $unFichier->getStatutValiditeCrlCertificat($avecSignature);
            $arrayInfos[$unFichier->getIdFichier()]['CertifCRL'] = Prado::localize($crl);
            $arrayInfos[$unFichier->getIdFichier()]['CertifCRLIcone'] = $crl;

            $signatureValidite = $unFichier->getSignatureValidite($avecSignature, $enveloppeCrypte, $callFrom);
            $arrayInfos[$unFichier->getIdFichier()]['SignatireValidite'] = Prado::localize($signatureValidite);
            $arrayInfos[$unFichier->getIdFichier()]['SignatireValiditeIcone'] = $signatureValidite;

            $arrayInfos[$unFichier->getIdFichier()]['dateDu'] = $unFichier->getDateValidFrom($avecSignature, $typeSignature);
            $arrayInfos[$unFichier->getIdFichier()]['dateA'] = $unFichier->getDateValidTo($avecSignature, $typeSignature);
            $arrayInfos[$unFichier->getIdFichier()]['resultValiditeSign'] = $unFichier->getResultatValiditeSignatureWithMessage(null, true, $callFrom);
            $arrayInfos[$unFichier->getIdFichier()]['jetonSignature'] = $unFichier->getJetonFileName();
        }
    }

    public static function getInfoSignatureViaServCrypto(&$arrayInfos, $unFichier, $avecSignature, $enveloppeCrypte, $callFrom)
    {
        if ($unFichier instanceof CommonFichierEnveloppe) {
            $arrayInfoSignature = json_decode($unFichier->getSignatureInfos(), true, 512, JSON_THROW_ON_ERROR);
            $arrayInfos[$unFichier->getIdFichier()]['idFichier'] = $unFichier->getIdFichier();
            $arrayInfos[$unFichier->getIdFichier()]['nomfichier'] = $unFichier->getNomFichier();
            $arrayInfos[$unFichier->getIdFichier()]['EmailSubject'] = (new Atexo_Config())->toPfEncoding($arrayInfoSignature['signataireDetail']['alternativeName']);
            $arrayInfos[$unFichier->getIdFichier()]['CnSubject'] = (new Atexo_Config())->toPfEncoding($arrayInfoSignature['signataireDetail']['cn']);
            $arrayInfos[$unFichier->getIdFichier()]['OuSubject'] = (new Atexo_Config())->toPfEncoding($arrayInfoSignature['signataireDetail']['ou']);
            $arrayInfos[$unFichier->getIdFichier()]['OSubject'] = (new Atexo_Config())->toPfEncoding($arrayInfoSignature['signataireDetail']['o']);
            $arrayInfos[$unFichier->getIdFichier()]['CSubject'] = (new Atexo_Config())->toPfEncoding($arrayInfoSignature['signataireDetail']['c']);
            $arrayInfos[$unFichier->getIdFichier()]['CnEmmeteur'] = (new Atexo_Config())->toPfEncoding($arrayInfoSignature['emetteurDetails']['cn']);
            $arrayInfos[$unFichier->getIdFichier()]['OuEmmeteur'] = (new Atexo_Config())->toPfEncoding($arrayInfoSignature['emetteurDetails']['ou']);
            $arrayInfos[$unFichier->getIdFichier()]['OEmmeteur'] = (new Atexo_Config())->toPfEncoding($arrayInfoSignature['emetteurDetails']['o']);
            $arrayInfos[$unFichier->getIdFichier()]['CEmmeteur'] = (new Atexo_Config())->toPfEncoding($arrayInfoSignature['emetteurDetails']['c']);

            $periodeValidite = $unFichier->getStatutCertificatPeriodeValidite($avecSignature);
            $cnValidite = $unFichier->getStatutValiditeChaineCertification($avecSignature);
            $arrayInfos[$unFichier->getIdFichier()]['CertifPeriodValidite'] = Prado::localize($periodeValidite);
            $arrayInfos[$unFichier->getIdFichier()]['CertifPeriodValiditeIcone'] = $periodeValidite;
            $arrayInfos[$unFichier->getIdFichier()]['CertifChaineCertif'] = Prado::localize($cnValidite);
            $arrayInfos[$unFichier->getIdFichier()]['CertifChaineCertifIcone'] = $cnValidite;
            if (Atexo_Module::isEnabled('SurchargeReferentiels')) {
                $arrayInfos[$unFichier->getIdFichier()]['ReferentielChaineCertif'] = Prado::localize('DEFINE_REFERENTIEL_DERTIFICAT') .
                    ' : ' . $unFichier->getNameReferentielCertificat();
            } else {
                $arrayInfos[$unFichier->getIdFichier()]['ReferentielChaineCertif'] = '';
            }

            $crl = $unFichier->getStatutValiditeCrlCertificat($avecSignature);
            $arrayInfos[$unFichier->getIdFichier()]['CertifCRL'] = Prado::localize($crl);
            $arrayInfos[$unFichier->getIdFichier()]['CertifCRLIcone'] = $crl;
            $signatureValidite = $unFichier->getSignatureValidite($avecSignature, $enveloppeCrypte, $callFrom);
            $arrayInfos[$unFichier->getIdFichier()]['SignatireValidite'] = Prado::localize($signatureValidite);
            $arrayInfos[$unFichier->getIdFichier()]['SignatireValiditeIcone'] = $signatureValidite;

            $arrayInfos[$unFichier->getIdFichier()]['dateDu'] = Atexo_Util::getDateTimeByFormat((new Atexo_Config())->toPfEncoding($arrayInfoSignature['dateValiditeDu']));
            $arrayInfos[$unFichier->getIdFichier()]['dateA'] = Atexo_Util::getDateTimeByFormat((new Atexo_Config())->toPfEncoding($arrayInfoSignature['dateValiditeAu']));
            $arrayInfos[$unFichier->getIdFichier()]['resultValiditeSign'] = $unFichier->getResultatValiditeSignatureWithMessage(null, true, $callFrom);
            $arrayInfos[$unFichier->getIdFichier()]['jetonSignature'] = $unFichier->getNomFichierSignature();

            $infoCertificatSignature = '';
            $infoBulleCertificatSignature = '';
            if (isset($arrayInfoSignature['qualifieEIDAS'])) {
                if (1 == $arrayInfoSignature['qualifieEIDAS']) {
                    $infoCertificatSignature = Prado::localize('CERTIFICAT_SIGNATURE_QUALIFIE');
                    $infoBulleCertificatSignature = Prado::localize('CERTIFICAT_SIGNATURE_QUALIFIE_INFO_BULLE');
                } else {
                    $infoCertificatSignature = Prado::localize('CERTIFICAT_SIGNATURE_NON_QUALIFIE');
                    $infoBulleCertificatSignature = Prado::localize('CERTIFICAT_SIGNATURE_NON_QUALIFIE_INFO_BULLE');
                }
            }
            $arrayInfos[$unFichier->getIdFichier()]['certificatSignature'] = $infoCertificatSignature;
            $arrayInfos[$unFichier->getIdFichier()]['certificatSignatureInfoBulle'] = $infoBulleCertificatSignature;
            $formatSignature = ($arrayInfoSignature['formatSignature'] ?? '');
            $arrayInfos[$unFichier->getIdFichier()]['formatSignature'] = $formatSignature;

            $dateIndicative = ($arrayInfoSignature['dateIndicative'] ?? '');
            $arrayInfos[$unFichier->getIdFichier()]['dateIndicative'] = Atexo_Util::getDateTimeByFormat($dateIndicative, 'd/m/Y H:i:s');
            $infoHorodatage = '';
            if (isset($arrayInfoSignature['jetonHorodatage'])) {
                if (1 == $arrayInfoSignature['jetonHorodatage']) {
                    $infoHorodatage = Prado::localize('JETON_SIGNATURE_HORODATE_OUI') . (isset($arrayInfoSignature['dateHorodatage']) ? ' (' . Atexo_Util::getDateTimeByFormat($arrayInfoSignature['dateHorodatage'], 'd/m/Y H:i:s') . ')' : '');
                } else {
                    $infoHorodatage = Prado::localize('JETON_SIGNATURE_HORODATE_NON');
                }
            }
            $arrayInfos[$unFichier->getIdFichier()]['jetonSignatureHorodate'] = $infoHorodatage;
            $arrayInfos[$unFichier->getIdFichier()]['hasInfoComp'] = (!empty($infoCertificatSignature) || !empty($formatSignature) || !empty($dateIndicative) || !empty($infoHorodatage));
        }
    }

    public function printRapportOdt($arrayInfosCons, $arrayInfosPli, $arrayInfosFichierSoumis)
    {
        try {
            $ok = $nOk = $unknown = $unsigned = $unverified = 0;
            $tableKeys = array_merge($arrayInfosPli, $arrayInfosCons);
            $fileModel = Atexo_Config::getParameter('CHEMIN_MODELE_RAPPORT_VERIF_SIGNATURE');
            $config = ['ZIP_PROXY' => \PhpZipProxy::class,
                            'DELIMITER_LEFT' => '{',
                            'DELIMITER_RIGHT' => '}',
                            'PATH_TO_TMP' => Atexo_Config::getParameter('COMMON_TMP'), ];
            $odf = new Atexo_Odf($fileModel, $config);
            $odf->setVars('entitePublique', $tableKeys['entitePublique'], true, Atexo_Config::getParameter('HTTP_ENCODING'));
            $odf->setVars('entiteAchat', $tableKeys['entiteAchat'], true, Atexo_Config::getParameter('HTTP_ENCODING'));
            $odf->setVars('RefConsUser', $tableKeys['RefConsUser'], true, Atexo_Config::getParameter('HTTP_ENCODING'));
            $odf->setVars('intituleConsultation', $tableKeys['intituleConsultation'], true, Atexo_Config::getParameter('HTTP_ENCODING'));
            $odf->setVars('objetConsultation', $tableKeys['objetConsultation'], true, Atexo_Config::getParameter('HTTP_ENCODING'));
            $odf->setVars('typeAnnonce', $tableKeys['typeAnnonce'], true, Atexo_Config::getParameter('HTTP_ENCODING'));
            $odf->setVars('typeProcedure', $tableKeys['typeProcedure'], true, Atexo_Config::getParameter('HTTP_ENCODING'));
            $odf->setVars('dateRemisePlis', $tableKeys['dateRemisePlis'], true, Atexo_Config::getParameter('HTTP_ENCODING'));
            $odf->setVars('numeroPli', $tableKeys['numeroPli'], true, Atexo_Config::getParameter('HTTP_ENCODING'));
            $odf->setVars('nomSte', $tableKeys['nomSte'], true, Atexo_Config::getParameter('HTTP_ENCODING'));
            $odf->setVars('typeEnveloppe', $tableKeys['typeEnveloppe'], true, Atexo_Config::getParameter('HTTP_ENCODING'));
            $odf->setVars('horodatageEnveloppe', $tableKeys['horodatageEnveloppe'], true, Atexo_Config::getParameter('HTTP_ENCODING'));
            $odf->setVars('statutPli', $tableKeys['statutPli'], true, Atexo_Config::getParameter('HTTP_ENCODING'));
            $odf->setVars('signatureElectronique', 'Oui', true, Atexo_Config::getParameter('HTTP_ENCODING'));
            $entetTab = $odf->setSegment('ENTETE_TABLEAU_VERIF_SIGN');
            if (is_array($arrayInfosFichierSoumis) && count($arrayInfosFichierSoumis) > 0) {
                foreach ($arrayInfosFichierSoumis as $infosFile) {
                    $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));
                    $fichierEnveloppe = CommonFichierEnveloppePeer::retrieveByPK($infosFile['idFichier'], Atexo_CurrentUser::getOrganismAcronym(), $connexion);
                    if ($fichierEnveloppe instanceof CommonFichierEnveloppe) {
                        $dateControlvalidite = $fichierEnveloppe->getCommonOffres()->getDateRemisePlie();
                        $typeFichier = $fichierEnveloppe->getTypeFichier();
                        if ('SIG' != $typeFichier) {
                            $entetTab->ligneFichier->setVars('nomFichier', Atexo_Util::toUtf8($infosFile['nomfichier']), true, Atexo_Config::getParameter('HTTP_ENCODING'));
                            if ($infosFile['resultValiditeSign']['statut'] != Atexo_Config::getParameter('SANS')) {
                                $jetonSingFile = Atexo_Util::toUtf8($infosFile['jetonSignature']);
                            } else {
                                $jetonSingFile = Prado::localize('DEFINE_AUCUN_JETON_SIGNATURE');
                            }
                            $resultatSign = Atexo_Util::toUtf8(self::getStatutSignatureFichier($infosFile['resultValiditeSign']['statut']));
                            $descResultatSign = Atexo_Util::toUtf8($infosFile['resultValiditeSign']['message']);

                            $entetTab->ligneFichier->setVars('resultatSign', $resultatSign, true, Atexo_Config::getParameter('HTTP_ENCODING'));
                            $entetTab->ligneFichier->setVars('descResultatSign', $descResultatSign, true, Atexo_Config::getParameter('HTTP_ENCODING'));
                            $entetTab->ligneFichier->setVars('dateControlvalidite', $dateControlvalidite, true, Atexo_Config::getParameter('HTTP_ENCODING'));
                            $entetTab->ligneFichier->setVars('dateControlIntegrite', $infosFile['dateControleIntegrite'], true, Atexo_Config::getParameter('HTTP_ENCODING'));
                            $certificatSignatureInfoBulle = $infosFile['certificatSignatureInfoBulle'] ? ' ( ' . $infosFile['certificatSignatureInfoBulle'] . ')' : '';
                            $entetTab->ligneFichier->setVars('certificatSignature', $infosFile['certificatSignature'] . $certificatSignatureInfoBulle, true, Atexo_Config::getParameter('HTTP_ENCODING'));
                            $entetTab->ligneFichier->setVars('formatSignature', $infosFile['formatSignature'], true, Atexo_Config::getParameter('HTTP_ENCODING'));
                            $entetTab->ligneFichier->setVars('dateIndicative', $infosFile['dateIndicative'], true, Atexo_Config::getParameter('HTTP_ENCODING'));
                            $entetTab->ligneFichier->setVars('jetonHoradatageSignature', $infosFile['jetonSignatureHorodate'], true, Atexo_Config::getParameter('HTTP_ENCODING'));
                            if ($infosFile['resultValiditeSign']['statut'] != Atexo_Config::getParameter('SANS') && $infosFile['resultValiditeSign']['statut'] != Atexo_Config::getParameter('UNVERIFIED')) {
                                $entetTab->ligneFichier->setVars('EmailSubject',$infosFile['EmailSubject'], true, Atexo_Config::getParameter('HTTP_ENCODING'));
                                $entetTab->ligneFichier->setVars('CnSubject', $infosFile['CnSubject'], true, Atexo_Config::getParameter('HTTP_ENCODING'));
                                $entetTab->ligneFichier->setVars('OuSubject', $infosFile['OuSubject'], true, Atexo_Config::getParameter('HTTP_ENCODING'));
                                $entetTab->ligneFichier->setVars('OSubject', $infosFile['OSubject'], true, Atexo_Config::getParameter('HTTP_ENCODING'));
                                $entetTab->ligneFichier->setVars('CSubject', $infosFile['CSubject'], true, Atexo_Config::getParameter('HTTP_ENCODING'));
                                $entetTab->ligneFichier->setVars('CnEmmeteur', $infosFile['CnEmmeteur'], true, Atexo_Config::getParameter('HTTP_ENCODING'));
                                $entetTab->ligneFichier->setVars('OuEmmeteur', $infosFile['OuEmmeteur'], true, Atexo_Config::getParameter('HTTP_ENCODING'));
                                $entetTab->ligneFichier->setVars('OEmmeteur', $infosFile['OEmmeteur'], true, Atexo_Config::getParameter('HTTP_ENCODING'));
                                $entetTab->ligneFichier->setVars('CEmmeteur', $infosFile['CEmmeteur'], true, Atexo_Config::getParameter('HTTP_ENCODING'));
                                $entetTab->ligneFichier->setVars('dateDu', Atexo_Util::iso2frnDateTime($infosFile['dateDu']), true, Atexo_Config::getParameter('HTTP_ENCODING'));
                                $entetTab->ligneFichier->setVars('dateA', Atexo_Util::iso2frnDateTime($infosFile['dateA']), true, Atexo_Config::getParameter('HTTP_ENCODING'));
                                $entetTab->ligneFichier->setVars('jetonSignFile', $jetonSingFile, true, Atexo_Config::getParameter('HTTP_ENCODING'));
                                $entetTab->ligneFichier->setVars('ReferentielCertif', Atexo_Util::toUtf8($infosFile['ReferentielChaineCertif']), true, Atexo_Config::getParameter('HTTP_ENCODING'));
                            } else {
                                $entetTab->ligneFichier->setVars('EmailSubject', '', true, Atexo_Config::getParameter('HTTP_ENCODING'));
                                $entetTab->ligneFichier->setVars('CnSubject', '', true, Atexo_Config::getParameter('HTTP_ENCODING'));
                                $entetTab->ligneFichier->setVars('OuSubject', '', true, Atexo_Config::getParameter('HTTP_ENCODING'));
                                $entetTab->ligneFichier->setVars('OSubject', '', true, Atexo_Config::getParameter('HTTP_ENCODING'));
                                $entetTab->ligneFichier->setVars('CSubject', '', true, Atexo_Config::getParameter('HTTP_ENCODING'));
                                $entetTab->ligneFichier->setVars('CnEmmeteur', '', true, Atexo_Config::getParameter('HTTP_ENCODING'));
                                $entetTab->ligneFichier->setVars('OuEmmeteur', '', true, Atexo_Config::getParameter('HTTP_ENCODING'));
                                $entetTab->ligneFichier->setVars('OEmmeteur', '', true, Atexo_Config::getParameter('HTTP_ENCODING'));
                                $entetTab->ligneFichier->setVars('CEmmeteur', '', true, Atexo_Config::getParameter('HTTP_ENCODING'));
                                $entetTab->ligneFichier->setVars('dateDu', '', true, Atexo_Config::getParameter('HTTP_ENCODING'));
                                $entetTab->ligneFichier->setVars('dateA', '', true, Atexo_Config::getParameter('HTTP_ENCODING'));
                                $entetTab->ligneFichier->setVars('jetonSignFile', $jetonSingFile, true, Atexo_Config::getParameter('HTTP_ENCODING'));
                                $entetTab->ligneFichier->setVars('ReferentielCertif', Atexo_Util::toUtf8(Prado::localize('DEFINE_NON_REFERENCE')), true, Atexo_Config::getParameter('HTTP_ENCODING'));
                            }
                            switch ($infosFile['resultValiditeSign']['statut']) {
                                case Atexo_Config::getParameter('OK'):
                                    $ok++;
                                    break;
                                case Atexo_Config::getParameter('NOK'):
                                    $nOk++;
                                    break;
                                case Atexo_Config::getParameter('UNKNOWN'):
                                    $unknown++;
                                    break;
                                case Atexo_Config::getParameter('SANS'):
                                    $unsigned++;
                                    break;
                                case Atexo_Config::getParameter('UNVERIFIED'):
                                    $unverified++;
                                    break;
                            }
                            $entetTab->ligneFichier->merge();
                        }
                    }
                }
            }
            $odf->setVars('nbrSignOk', $ok, true, Atexo_Config::getParameter('HTTP_ENCODING'));
            $odf->setVars('nbrSignNok', $nOk, true, Atexo_Config::getParameter('HTTP_ENCODING'));
            $odf->setVars('nbrSignUknown', $unknown, true, Atexo_Config::getParameter('HTTP_ENCODING'));
            $odf->setVars('nbrFichierNonSign', $unsigned, true, Atexo_Config::getParameter('HTTP_ENCODING'));
            $odf->setVars('nbrSignNonVerifie', $unverified, true, Atexo_Config::getParameter('HTTP_ENCODING'));
            $dateGeneration = date('d/m/y') . ' à ' . date('H:i');
            $odf->setVars('dateGeneration', $dateGeneration, true, Atexo_Config::getParameter('HTTP_ENCODING'));
            $entetTab->merge();
            $odf->mergeSegment($entetTab);
            $pathFile = Atexo_Config::getParameter('COMMON_TMP') . 'Rapport_' . uniqid() . '.odt';
            $odf->saveToDisk($pathFile);

            return $pathFile;
        } catch (Exception $e) {
            Prado::log($e->getMessage() . '-', TLogger::ERROR, 'ERROR');
        }
    }

    public function printRapportSignaturePdf($arrayInfosCons, $arrayInfosPli, $arrayInfosFichierSoumis, $toDisk = false, $path = false)
    {
        try {
            $odtFilePath = self::printRapportOdt($arrayInfosCons, $arrayInfosPli, $arrayInfosFichierSoumis);
            if ($odtFilePath) {
                if ($toDisk) {
                    $pdfContent = (new PdfGeneratorClient())->genererPdf($odtFilePath);
                    Atexo_Util::write_file($path, $pdfContent);

                    return $path;
                } else {
                    $file = Atexo_Config::getParameter('NOM_FICHIER_RAPPORT_VERIF_SIGNATURE') . '.pdf';
                    $pdfContent = (new PdfGeneratorClient())->genererPdf($odtFilePath);
                    @unlink($odtFilePath);
                    DownloadFile::downloadFileContent($file, $pdfContent);
                }
            }
        } catch (Exception $e) {
            Prado::log($e->getMessage() . '-', TLogger::ERROR, 'ERROR');
        }
    }

    /*
     * Permet de retourner l'admissibilité enveloppe lot
     * @param $sousPli : sous pli,$idOffre = id de l'offre ,$formeEnv = si l'enveloppe papier ou electronique
     * @param $typeEnv = type d'enveloppe ,$consultation = la consultation
     */
    public function retrieveAdmissibilite($sousPli, $idOffre, $formeEnv, $typeEnv, $consultation)
    {
        $addmissibleLot = null;
        $admissibiliteFor = self::admissibiliteSuivante($typeEnv, $consultation);
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        if ($formeEnv == Atexo_Config::getParameter('TYPE_DEPOT_QUESTION_ELECTRONIQUE')) {
            $c->add(CommonAdmissibiliteEnveloppeLotPeer::ID_OFFRE, $idOffre);
            $c->add(CommonAdmissibiliteEnveloppeLotPeer::SOUS_PLI, $sousPli);
            $c->add(CommonAdmissibiliteEnveloppeLotPeer::TYPE_ENVELOPPE, $admissibiliteFor);
            $c->add(CommonAdmissibiliteEnveloppeLotPeer::ADMISSIBILITE, Atexo_Config::getParameter('LOT_A_TRAITER'), Criteria::NOT_EQUAL);
            $addmissibleLot = CommonAdmissibiliteEnveloppeLotPeer::doSelectOne($c, $connexion);
        } elseif ($formeEnv == Atexo_Config::getParameter('TYPE_DEPOT_QUESTION_PAPIER')) {
            $c->add(CommonAdmissibiliteEnveloppePapierLotPeer::ID_OFFRE_PAPIER, $idOffre);
            $c->add(CommonAdmissibiliteEnveloppePapierLotPeer::SOUS_PLI, $sousPli);
            $c->add(CommonAdmissibiliteEnveloppePapierLotPeer::TYPE_ENVELOPPE, $admissibiliteFor);
            $c->add(CommonAdmissibiliteEnveloppePapierLotPeer::ADMISSIBILITE, Atexo_Config::getParameter('LOT_A_TRAITER'), Criteria::NOT_EQUAL);
            $addmissibleLot = CommonAdmissibiliteEnveloppePapierLotPeer::doSelectOne($c, $connexion);
        }

        return $addmissibleLot;
    }

    /*
     * Peremet de retourner les offres non annulée
     */
    public function retrieveOffreNonAnnuleeByConsRef($consultationId, $organisme = null, $limit = null, $offset = null, $trier = null, $sensTri = null)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonOffresPeer::CONSULTATION_ID, $consultationId);
        if ($organisme) {
            $c->add(CommonOffresPeer::ORGANISME, $organisme);
        }
        $c->add(CommonOffresPeer::DEPOT_ANNULE, '0');
        $c->add(CommonOffresPeer::STATUT_OFFRES, Atexo_Config::getParameter('STATUT_ENV_BROUILLON'), Criteria::NOT_EQUAL);
        $column = strtoupper($trier);
        $peer = new ReflectionClass('CommonOffresPeer');
        if ($trier && 'ASC' == $sensTri) {
            $c->addAscendingOrderByColumn($peer->getConstant($column));
        } elseif ($trier && 'DESC' == $sensTri) {
            $c->addDescendingOrderByColumn($peer->getConstant($column));
        } else {
            $c->addAscendingOrderByColumn('untrusteddate');
        }
        if ($limit) {
            $c->setLimit($limit);
        }
        if ($offset) {
            $c->setOffset($offset);
        }
        $offres = CommonOffresPeer::doSelect($c, $connexion);
        if ($offres) {
            return $offres;
        }

        return [];
    }

    /*
     * Peremet de retourner les offres papiers non annulée
     */
    public function retrieveOffrePapierNonAnnuleeByConsRef($consultationId, $organisme, $limit = null, $offset = null, $trier = null, $sensTri = null)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonOffrePapierPeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonOffrePapierPeer::ORGANISME, $organisme);
        $c->add(CommonOffrePapierPeer::DEPOT_ANNULE, '0');
        $column = strtoupper($trier);
        $peer = new ReflectionClass('CommonOffrePapierPeer');
        if ($trier && 'ASC' == $sensTri) {
            $c->addAscendingOrderByColumn($peer->getConstant($column));
        } elseif ($trier && 'DESC' == $sensTri) {
            $c->addDescendingOrderByColumn($peer->getConstant($column));
        }
        if ($limit) {
            $c->setLimit($limit);
        }
        if ($offset) {
            $c->setOffset($offset);
        }
        $offres = CommonOffrePapierPeer::doSelect($c, $connexion);
        if ($offres) {
            return $offres;
        }

        return [];
    }

    public function getPictoValidite($validite)
    {
        return match ($validite) {
            'OK' => 'valide.png',
            'KO' => 'invalide.png',
            'INCONNUE' => 'incertaine.png',
            'UNKNOWN' => 'incertaine.png',
            'SANS' => 'nonSigne.png',
            default => 'invalide.png',
        };
        //$entetTab->ligneFichier->setImage('JetonSignFile', Atexo_Controller_Front::t().'/images/picto-check-ok-small.gif');
    }

    public function getStatutSignatureFichier($statut)
    {
        return match (strtoupper($statut)) {
            Atexo_Config::getParameter('OK') => Prado::localize('DEFINE_FICHIER_SIGNE_SIGNATURE_VALIDE'),
            Atexo_Config::getParameter('NOK') => Prado::localize('DEFINE_FICHIER_SIGNATURE_INVALIDE'),
            Atexo_Config::getParameter('UNKNOWN') => Prado::localize('DEFINE_FICHIER_SIGNATURE_INCERTAINE'),
            Atexo_Config::getParameter('SANS') => Prado::localize('DEFINE_FICHIER_NON_SIGNE_OU_NON_TROUVABLE'),
            Atexo_Config::getParameter('UNVERIFIED') => Prado::localize('DEFINE_MESSAGE_VALIDITE_SIGNATURE_NON_VERIFIABLE'),
            default => Prado::localize('DEFINE_FICHIER_NON_SIGNE_OU_NON_TROUVABLE'),
        };
    }

    /*
     * permet de retourner la liste des rapports signature sous format "idEnveloppe1-checked1-tailleFichier1#idEnveloppe2-checked2-tailleFichier2#..."
     */
    public function getListeRapportSignatures($enveloppes, $consultation, $typeSignature, $notChecked = false, $callFrom = 'entreprise')
    {
        $listeRapports = null;
        foreach ($enveloppes as $enveloppe) {
            if ($enveloppe instanceof CommonEnveloppe) {
                $sizeFile = self::getRapportSignatureEnveloppe($enveloppe, $consultation, $typeSignature, true, $callFrom);
                $sizeFileName = Atexo_Util::GetSizeName($sizeFile);
                $listeRapports .= $enveloppe->getIdEnveloppeElectro();
                if (!$notChecked && ($sizeFile < Atexo_Config::getParameter('CHORUS_MAX_UPLOAD_FILE_SIZE'))) {
                    $listeRapports .= '-1';
                } else {
                    $listeRapports .= '-0';
                }
                $listeRapports .= '-' . $sizeFileName;
                $listeRapports .= '#';
            }
        }

        return $listeRapports;
    }

    /*
     * Permet de retourner la taille de rapport de signature d un enveloppe
     */
    public function getRapportSignatureEnveloppe($enveloppe, $consultation, $typeSignature, $size = false, $callFrom = 'entreprise')
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $arrayInfosCons = (new Atexo_Consultation())->getInfosConsPrRapportSignature(null, $consultation);
        $arrayInfosPli = self::GetDetailsPlis($enveloppe->getIdEnveloppeElectro(), $enveloppe->getTypeEnv(), null, null, $consultation);
        $avecSignature = $consultation->getSignatureOffre();
        $enveloppeCrypte = $enveloppe->getCryptage();
        $fichiers = $enveloppe->getCommonFichierEnveloppes($connexion);
        $arrayInfosFichierSoumis = self::getInfosCertif($fichiers, $avecSignature, $typeSignature, $enveloppeCrypte, $callFrom);
        $pathFile = Atexo_Config::getParameter('COMMON_TMP') . '/' . Atexo_Config::getParameter('NOM_FICHIER_RAPPORT_VERIF_SIGNATURE') . '_' . uniqid() . '.pdf';
        $path = self::printRapportSignaturePdf($arrayInfosCons, $arrayInfosPli, $arrayInfosFichierSoumis, true, $pathFile);
        if ($size) {
            $sizeFile = filesize($path);
            unlink($path);

            return $sizeFile;
        }

        return $path;
    }

    /*
     * Permet de retourner les ids des offres
     *
     * @param integer $consultationId la reference de la consultation
     * @return array des ids des offres de la consultation concerne
     * @author LEZ <loubna.ezziani@atexo.com>
     * @version 1.0
     * @since 4.6.0
     * @copyright Atexo 2014
     */
    public function getIdsOffresByIdConsultation($consultationId)
    {
        $offreQuery = new CommonOffresQuery();
        $offres = $offreQuery->getOffresNonBrouillonByIdConsultation($consultationId);
        $arrayIds = [];
        if (is_array($offres) && count($offres)) {
            foreach ($offres as $offre) {
                $arrayIds[] = $offre->getId();
            }
        }

        return $arrayIds;
    }

    /*
     * Permet de retourner le registre de depot
     *
     * @param integer $idDepot id du depot
     * @return Atexo_Registres_RegistreDepot $registre
     * @author LEZ <loubna.ezziani@atexo.com>
     * @version 1.0
     * @since 4.6.0
     * @copyright Atexo 2014
     */
    public static function getRegistreDepot($idDepot, $logger)
    {
        /*
        $c = new Criteria();
        $c->add(CommonOffresPeer::ID,$idDepot);

        $c->add(CommonOffresPeer::STATUT_OFFRES,  $listeStatutAutoriser, Criteria::IN);
        $offre = CommonOffresPeer::doSelectOne($c,$connexion);
        */
        $logger->info("La recuperation de l'offre dont l'id => " . $idDepot . ' et son statut non brouillon ');
        $offreQuery = new CommonOffresQuery();
        $offre = $offreQuery->getOffreNonBrouillonById($idDepot);

        if ($offre instanceof CommonOffres) {
            $logger->info('Offre existe');
            $consultation = $offre->getCommonConsultation();
            if ($consultation instanceof CommonConsultation) {
                $chiffrement = Atexo_Util::getBooleanStringFromValInt($consultation->getChiffrementOffre());
                $consultationId = $consultation->getId();
                $organisme = $consultation->getOrganisme();
            }
            $nombrePli = 0;
            $idOffre = $offre->getId();
            $registre = new Atexo_Registres_RegistreDepot();
            $registre->setId($offre->getId());
            $registre->setIdEntreprise($offre->getEntrepriseId());
            $registre->setIdEtablissement($offre->getIdEtablissement());
            $registre->setDate(Atexo_Util::getDateXmlFromDateTimeVal($offre->getUntrusteddate()));
            $registre->setNumero($offre->getNumeroReponse());
            $typeDepot = self::getTypeDepot(Atexo_Config::getParameter('REGISTRE_DEPOT_ELECTRONIQUE'));
            $registre->setType($typeDepot);
            $registre->setNomInscrit($offre->getNomInscrit());
            $registre->setPrenomInscrit($offre->getPrenomInscrit());
            $registre->setRueInscrit(($offre->getAdresseInscrit() . ' ' . $offre->getAdresse2Inscrit()));
            $registre->setCodePostalInscrit($offre->getCodePostalInscrit());
            $registre->setVilleInscrit($offre->getVilleInscrit());
            $registre->setPaysInscrit($offre->getPaysInscrit());
            $registre->setEmailInscrit(trim($offre->getEmailInscrit()));
            $registre->setTelephoneInscrit($offre->getTelephoneInscrit());
            $registre->setFaxInscrit($offre->getFaxInscrit());
            $registre->setRaisonSocialeInscrit($offre->getNomEntrepriseInscrit());
            $registre->setSirenInscrit(substr($offre->getSiretInscrit(), 0, 9));
            $registre->setCodeEtablissementInscrit(substr($offre->getSiretInscrit(), 9));
            $registre->setSiretInscrit(substr($offre->getSiretInscrit(), 9));
            $registre->setTypeIdentifiantEntrepriseInscrit(empty($offre->getSiretInscrit()) ? 'Hors UE' : 'SIRET');
            $registre->setOrganisme($offre->getOrganisme());
            $registre->setReponseAnnonceBase64(base64_encode($offre->getXmlString()));
            $registre->setStatutOffres($offre->getStatutOffres());
            $registre->setIdInscrit($offre->getInscritId());
            if (null !== $offre->getInscritId())
            {
                $inscrit = (new Atexo_Entreprise_Inscrit())->retrieveInscritById($offre->getInscritId());

                $registre->setLoginInscrit($inscrit->getLogin());
                $registre->setActifInscrit(!$inscrit->getBloque());

                $registre->setAnnuaireDefenseInscrit($inscrit->getInscritAnnuaireDefense());
                $registre->setDateModificationRgpdInscrit($inscrit->getDateValidationRgpd());

                $rgpd = '';
                if ($inscrit->getRgpdCommunicationPlace()) {
                    $rgpd .= 'COMMUNICATION_PLACE';
                }
                if ($inscrit->getRgpdEnquete()) {
                    $rgpd .= empty($rgpd) ? '' : ', ';
                    $rgpd .= 'COMMUNICATION_ENQUETE';
                }
                if ($inscrit->getRgpdCommunication()) {
                    $rgpd .= empty($rgpd) ? '' : ', ';
                    $rgpd .= 'COMMUNICATION_SIA';
                }
            }
            $registre->setRgpdInscrit($rgpd);
            $mps = 'false';
            if (Atexo_Module::isEnabled('MarchePublicSimplifie', $organisme)) {
                $candidatureMpsExiste = (new Atexo_MarchePublicSimplifie())->retrieveCandidatureMps($idOffre, $consultationId);
                if ($candidatureMpsExiste instanceof CommonTCandidatureMps) {
                    $mps = 'true';
                }
            }
            $registre->setMarchePublicSimplifie($mps);

            $idEntreprise = $offre->getEntrepriseId();
            $entreprise = (new Atexo_Entreprise())->retrieveCompanyBdeById($idEntreprise);
            if ($entreprise) {
                $registre->setRaisonSocialeEntreprise($offre->getNomEntrepriseInscrit());
                $registre->setSirenEntreprise(substr($offre->getSiretInscrit(), 0, 9));
                $registre->setEmailEntreprise(trim($entreprise->getEmail()));
                $registre->setFormeJuridiqueEntreprise($entreprise->getFormejuridique());
                $registre->setCodeAPEEntreprise($entreprise->getCodeape());
                $registre->setRueEntreprise($entreprise->getAdresse() . ' ' . $entreprise->getAdresse2());
                $registre->setCodePostalEntreprise($entreprise->getCodepostal());
                $registre->setVilleEntreprise($entreprise->getVilleadresse());
                $registre->setPaysEntreprise($entreprise->getPaysadresse());
                $registre->setTelephoneEntreprise($entreprise->getTelephone());
                $registre->setFaxEntreprise($entreprise->getFax());
            }
            $enveloppes = (new self())->retrieveEnveloppesByIdOffre($offre->getId(), $offre->getOrganisme());
            self::fillRegistreByEnveloppes($registre, $enveloppes, $nombrePli, $chiffrement);
            $registre->setNombrePlis($nombrePli);

            return $registre;
        }

        return false;
    }

    /*
     * Permet d'ajouter les enveloppes au registre
     *
     * @param Atexo_Registres_RegistreDepot $registre c'est le registre de depot ,array CommonEnveloppe $enveloppes
     * @return void
     * @author LEZ <loubna.ezziani@atexo.com>
     * @version 1.0
     * @since 4.6.0
     * @copyright Atexo 2014
     */
    public static function fillRegistreByEnveloppes(&$registre, $enveloppes, &$nombrePli, $chiffrement)
    {
        foreach ($enveloppes as $enveloppe) {
            $enveloppeField = new Atexo_Rest_Action_EnveloppeField();
            $typeEnv = self::getTypeEnveloppe($enveloppe->getTypeEnv());
            $enveloppeField->setTypeEnveloppe($typeEnv);
            $enveloppeField->setNumeroLot($enveloppe->getSousPli());
            $nombrePli += is_countable($enveloppe->getCommonFichierEnveloppes()) ? count($enveloppe->getCommonFichierEnveloppes()) : 0;
            $enveloppeField->setPlis($enveloppe->getCommonFichierEnveloppes(null, $chiffrement));
            $enveloppeField->setId($enveloppe->getIdEnveloppeElectro());
            $registre->addEnveloppes($enveloppeField);
        }
    }

    /*
     * Permet de retourner le type d'enveloppe
     *
     * @param integer $type d'enveloppe
     * @return String le type d'enveloppe en lettre
     * @author LEZ <loubna.ezziani@atexo.com>
     * @version 1.0
     * @since 4.6.0
     * @copyright Atexo 2014
     */
    public static function getTypeEnveloppe($type)
    {
        if ($type == Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE')) {
            return 'CANDIDATURE';
        } elseif ($type == Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE')) {
            return 'OFFRE TECHNIQUE';
        } elseif ($type == Atexo_Config::getParameter('TYPE_ENV_OFFRE')) {
            return 'OFFRE';
        } elseif ($type == Atexo_Config::getParameter('TYPE_ENV_ANONYMAT')) {
            return 'ANONYMAT';
        }
    }

    /*
     * Permet de retourner le type de depot
     *
     * @param integer $idType id type depot
     * @return String $type le type de depot
     * @author LEZ <loubna.ezziani@atexo.com>
     * @version 1.0
     * @since 4.6.0
     * @copyright Atexo 2014
     */
    public static function getTypeDepot($idType)
    {
        if ($idType == Atexo_Config::getParameter('REGISTRE_DEPOT_ELECTRONIQUE')) {
            return 'ELECTRONIQUE';
        } else {
            return '';
        }
    }

    /**
     * Permet de retourner  chiffrement de la consultation.
     *
     * @param int    $id_enveloppe_electro id de l'enveloppe
     * @param string $organisme            organisme de l'enveloppe
     *
     * @return string $chiffrement chiffrement de la consultation
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function getChiffrementConsultationByIdEnveloppe($id_enveloppe_electro, $organisme)
    {
        $query = 'SELECT consultation.chiffrement_offre as chiffrement FROM Offres, Enveloppe, consultation WHERE Enveloppe.`id_enveloppe_electro` = ' . $id_enveloppe_electro .
            " AND Offres.id = Enveloppe.offre_id AND Offres.organisme = Enveloppe.organisme AND Offres.organisme= '" . $organisme .
            "' AND consultation.id=Offres.consultation_id And Offres.organisme=consultation.organisme";

        $statement = Atexo_Db::getLinkCommon(true)->prepare($query);
        $statement->execute();

        $row = $statement->fetch(PDO::FETCH_ASSOC);

        return $row['chiffrement'];
    }

    /**
     * Permet de recuperer l'enveloppe d'une offre.
     *
     * @param int    $idOffre   l'id de l'offre
     * @param string $organisme l'organisme
     * @param string $type      le type d'enveloppe
     *
     * @return CommonEnveloppe l'objet enveloppe
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.10.0
     *
     * @copyright Atexo 2015
     */
    public function retrieveEnveloppeByIdOffreAndType($idOffre, $organisme, $type)
    {
        $enveloppeQuery = new CommonEnveloppeQuery();

        return $enveloppeQuery->getEnveloppeByIdOffreAndType($idOffre, $organisme, $type);
    }

    /*
     * Permet de retourner l'admissibilité enveloppe lot
     * @param $sousPli : sous pli,$idOffre = id de l'offre ,$formeEnv = si l'enveloppe papier ou electronique
     * @param $typeEnv = type d'enveloppe ,$consultation = la consultation
     */
    public function retrieveAdmissibiliteBy($organisme, $idOffre, $typeDepot, $typeEnv, $consultation, $connexion = null)
    {
        $addmissibleLotQuery = null;
        $addmissibleLot = [];
        if (!$connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
        if ($typeDepot == Atexo_Config::getParameter('TYPE_DEPOT_QUESTION_ELECTRONIQUE') || $typeDepot == Atexo_Config::getParameter('TYPE_DEPOT_QUESTION_PAPIER')) {
            if ($typeDepot == Atexo_Config::getParameter('DEPOT_ELECTRONIQUE')) {
                $addmissibleLotQuery = new CommonAdmissibiliteEnveloppeLotQuery();
            } elseif ($typeDepot == Atexo_Config::getParameter('DEPOT_PAPIER')) {
                $addmissibleLotQuery = new CommonAdmissibiliteEnveloppePapierLotQuery();
            }
            if ($consultation instanceof CommonConsultation) {
                $admissibiliteFor = self::admissibiliteSuivante($typeEnv, $consultation);
                $addmissibleLot = $addmissibleLotQuery->getAdmissibiliteLot($organisme, $idOffre, $admissibiliteFor, $connexion);
            }
        }

        return $addmissibleLot;
    }

    /**
     * Permet de setter les information d'admisibilite pour le WS.
     *
     * @param $organisme
     * @param $id
     * @param $typeDepotOffre
     * @param $typeEnv
     * @param $xmlResponse
     * @param $logger
     * @param $consultation
     * return string
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-megalis
     *
     * @copyright Atexo 2016
     */
    public function setInfoAdmissibiliteForWs($organisme, $id, $typeDepotOffre, $typeEnv, &$xmlResponse, $logger, $consultation)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $messageErreur = '';
        if ($consultation instanceof CommonConsultation) {
            $reference = $consultation->getId();
            $admissibilitesLot = self::retrieveAdmissibiliteBy($organisme, $id, $typeDepotOffre, $typeEnv, $consultation, $connexion);
            $logger->info("La récupération de l'xml contenant les informations de la candidature");
            if (is_array($admissibilitesLot) && count($admissibilitesLot)) {
                $admissibliteField = new Atexo_Rest_ObjectField_AdmissibiliteField();
                $admissibliteField->setId($id);
                $admissibliteField->setOrganisme($organisme);
                $admissibliteField->setReference($reference);
                $admissibliteField->setType(Atexo_Util::getLibelleTypeDepot($typeDepotOffre));
                foreach ($admissibilitesLot as $admissibiliteLot) {
                    if ($admissibiliteLot instanceof CommonAdmissibiliteEnveloppeLot || $admissibiliteLot instanceof CommonAdmissibiliteEnveloppePapierLot) {
                        $admLot = new Atexo_Rest_ObjectField_AdmissibiliteLotField();
                        $admLot->setNumeroLot($admissibiliteLot->getSousPli());
                        $admLot->setAdmissibilite(Atexo_Util::getLibelleAdmissibilite($admissibiliteLot->getAdmissibilite()));
                        $admissibliteField->addAdmisibiliteLot($admLot);
                    }
                }
                $serializer = new Atexo_Serializer_Serializer();
                $mappingFile = Atexo_Config::getParameter('PATH_FILE_MAPPING_ADMISSIBILITE');
                $xmlResponse = $serializer->SerializeClass($admissibliteField, $admissibliteField::class, $mappingFile);
            } else {
                $messageErreur = "Ce dépôt ne dispose pas d'information d'admissibilité";
            }
        } else {
            $messageErreur = $this->getMessageErrorForAccessDenied();
        }

        return $messageErreur;
    }

    /**
     * Permet de recuperer l'enveloppe d'une offre.
     *
     * @param int    $idEnveloppe l'id de l'envelope
     * @param string $organisme   l'organisme
     * @param int    $idOffre     l'id de l'offre
     * @param PDO    $connexion
     *
     * @return CommonEnveloppe|null l'objet enveloppe
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public static function retrieveEnveloppe($idEnveloppe, $organisme, $idOffre, $connexion = null)
    {
        if (empty($connexion)) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
        $enveloppeQuery = new CommonEnveloppeQuery();

        return $enveloppeQuery->filterByIdEnveloppeElectro($idEnveloppe)
                                    ->filterByOrganisme($organisme)
                                    ->filterByOffreId($idOffre)
                                    ->filterByStatutEnveloppe(Atexo_Config::getParameter('STATUT_ENV_BROUILLON'), Criteria::NOT_EQUAL)
                                    ->findOne($connexion);
    }

    public function getDecisionConsultationNonAlloti($consultation, $lot = '0')
    {
        $decisionLot = null;
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        if ($consultation instanceof CommonConsultation) {
            $c = new Criteria();
            $c->add(CommonDecisionLotPeer::CONSULTATION_ID, $consultation->getId());
            $c->add(CommonDecisionLotPeer::LOT, $lot);
            $c->add(CommonDecisionLotPeer::ORGANISME, $consultation->getOrganisme());
            $decisionLot = CommonDecisionLotPeer::doSelectOne($c, $connexionCom);
        }
        if (!($decisionLot instanceof CommonDecisionLot)) {
            $decisionLot = new CommonDecisionLot();
        }

        return $decisionLot->getIdTypeDecision();
    }

    public function getDecisionConsultationAlloti($consultation)
    {
        $arrayReturn = [];
        if ($consultation instanceof CommonConsultation) {
            $lots = $consultation->getAllLots();
            if (is_array($lots)) {
                foreach ($lots as $lot) {
                    if ($lot instanceof CommonCategorieLot) {
                        $idDecision = self::getDecisionConsultationNonAlloti($consultation, $lot->getLot());
                        $arrayReturn[$idDecision][] = $lot->getLot();
                    }
                }
            }
        }

        return $arrayReturn;
    }

    /**
     * Permet de recuperer nombre enveloppe ferme d'une offre.
     *
     * @param int    $idOffre   l'id de l'offre
     * @param string $organisme l'organisme
     *
     * @return int nombre enveloppe ferme
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public static function nombreEnveloppeFerme($idOffre, $organisme, $connexion)
    {
        $query = <<<QUERY
SELECT
	count(*) as nbEnveloppes
FROM
	Enveloppe
WHERE
		Enveloppe.offre_id=:offreId
	AND
		Enveloppe.organisme=:organisme
	AND
		(
			Enveloppe.statut_enveloppe=:statutFerme
			OR
			Enveloppe.statut_enveloppe=:statutEncoursDechiffrement
		)
QUERY;
        $params = [];
        $params[':offreId'] = $idOffre;
        $params[':organisme'] = $organisme;
        $params[':statutFerme'] = Atexo_Config::getParameter('STATUT_ENV_FERME');
        $params[':statutEncoursDechiffrement'] = Atexo_Config::getParameter('STATUT_ENV_DECHIFFREMENT_EN_COURS');

        $statement = $connexion->prepare($query);
        $statement->execute($params);
        $results = $statement->fetchAll(PDO::FETCH_ASSOC);

        return $results[0]['nbEnveloppes'];
    }

    /**
     * Permet d'ouvrir une offre.
     *
     * @param CommonOffres $Offre        l'offre
     * @param string       $organisme    l'organisme
     * @param string       $statut
     * @param int          $idAgent
     * @param PDO          $connexionCom
     *
     * @return void
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public static function ouvrirOffres(CommonOffres $offre, $statut, $idAgent, $connexionCom, $consultation = null, $logger = null)
    {
        if (($consultation instanceof CommonConsultation) && $consultation->getModeOuvertureReponse()) {
            $referenceUtilisateurCons = $consultation->getReferenceUtilisateur();
            $consultationId = $consultation->getId();
            $organisme = $consultation->getOrganisme();
            $description = '@@DEFINE_CONSULTATION@@';
            $url = '?page=Agent.ouvertureEtAnalyse&id=' . $consultationId;
            $notificationLibelle = '@@LABEL_NOTIFICATION_PLIS_DECHIFFRES@@';
            (new Atexo_Agent_Notifications())->ajouterNotification($idAgent, $organisme, Atexo_Config::getParameter('ID_TYPE_NOTIFICATION_CONSULTATION'), $notificationLibelle, $description, $referenceUtilisateurCons, $connexionCom, $url);
        }
        if ($statut == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_EN_LIGNE')) {
            if ($logger) {
                $logger->info(" verification du hash des fichiers de l'offre " . $offre->getId());
            }
            $resultatVerifiHash = $offre->isHashFichiersOk();
            if ($logger) {
                $logger->info(" resultat  du hash des fichiers de l'offre " . $offre->getId() . ' => ' . $resultatVerifiHash);
            }
            $offre->setResultatVerificationHashAllFiles($resultatVerifiHash);
        }
        $offre->setStatutOffres($statut);
        $offre->setCryptageReponse('0');
        if (Atexo_Module::isEnabled('fourEyes')) {
            $offre->setDateHeureOuvertureAgent2(date('Y-m-d H:i:s'));
            $offre->setAgentidOuverture2($idAgent);
        } else {
            $offre->setDateHeureOuverture(date('Y-m-d H:i:s'));
            $offre->setAgentidOuverture($idAgent);
        }
        $offre->save($connexionCom);
    }

    public static function updatEnveloppeOffre(
        $idOffre,
        $organisme,
        $statut,
        $cryptage,
        $fictif = false
    ) {
        $link = Atexo_Db::getLinkCommon();
        $sqlUpdate = <<<QUERY
UPDATE
	Enveloppe
SET
	statut_enveloppe = :statut,
	cryptage = :cryptage
WHERE
		Enveloppe.offre_id = :idOffre
	AND
		Enveloppe.organisme = :organisme
QUERY;

        $params = [':idOffre' => $idOffre,
            ':organisme' => $organisme,
            ':statut' => $statut,
            ':cryptage' => $cryptage,
        ];

        if ($fictif) {
            $sqlUpdate .= ' AND	Enveloppe.enveloppe_fictive = :enveloppeFictive';
            $params[':enveloppeFictive'] = 1;
        }

        $statement = $link->prepare($sqlUpdate);
        $exec = $statement->execute($params);

        return $exec;
    }

    /**
     * Permet de mettre a jour le champ unstresteddate apres le calcule.
     *
     * @param int $idOffre l'id de l'offre
     *
     * @return string la date de remise
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2017-roadmap
     *
     * @copyright Atexo 2017
     */
    public function saveDateRemisePlis($idOffre)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));
        $offresQuery = new CommonOffresQuery();
        $offre = $offresQuery->getOffreById($idOffre, $connexion);
        if ($offre instanceof CommonOffres && $offre->getHorodatage()) {
            $dateRemise = (new Atexo_Crypto())->retrieveDateFromHorodatage($offre->getHorodatage(), 'fr');
            if ($dateRemise) {
                $offre->setUntrusteddate(Atexo_Util::frnDateTime2iso($dateRemise));
                $offre->save($connexion);

                return $dateRemise;
            }
        }

        return '';
    }

    public static function prepereJsonForDownloadPlis($array, $type, $org)
    {
        $res = [];
        if (is_array($array)) {
            $res = ['PLI' => []];
            $cnx = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
            if ('reponse' == $type) {
                foreach ($array as $idOffre) {
                    $offre = CommonOffresPeer::retrieveByPK($idOffre, $org, $cnx);
                    if ($offre instanceof CommonOffres) {
                        $infoEntreprise = 'EL ' . $offre->getNumeroReponse() . ' - ' . $offre->getNomEntrepriseInscrit();
                        if (!array_key_exists($infoEntreprise, $res['PLI'])) {
                            $res['PLI'][$infoEntreprise] = [];
                        }
                        self::getArrayInfoEnveloppe($offre->getCommonEnveloppes(), $res['PLI'][$infoEntreprise]);
                    }
                }
            } else {
                foreach ($array as $idEnveloppe) {
                    $enveloppe = CommonEnveloppePeer::retrieveByPK($idEnveloppe, $org, $cnx);
                    if ($enveloppe instanceof CommonEnveloppe) {
                        $offre = $enveloppe->getOffre($cnx);
                        if ($offre instanceof CommonOffres) {
                            $infoEntreprise = 'EL ' . $offre->getNumeroReponse() . ' - ' . $offre->getNomEntrepriseInscrit();
                            if (!array_key_exists($infoEntreprise, $res['PLI'])) {
                                $res['PLI'][$infoEntreprise] = [];
                            }
                            self::getArrayInfoEnveloppe([$enveloppe], $res['PLI'][$infoEntreprise]);
                        }
                    }
                }
            }
        }

        return $res;
    }

    public static function getArrayInfoEnveloppe($enveloppes, &$res)
    {
        if (is_array($enveloppes)) {
            foreach ($enveloppes as $enveloppe) {
                if ($enveloppe instanceof CommonEnveloppe) {
                    $type = self::getLabelTypeEnveloppe($enveloppe->getTypeEnv());
                    if ($enveloppe->getSousPli()) {
                        $lot = 'Lot ' . $enveloppe->getSousPli();
                        $element = &$res[$type][$lot];
                    } else {
                        $element = &$res[$type];
                    }
                    $idsBlob = Atexo_FichierEnveloppe::getIdBlobFichiersByIdEnveloppe($enveloppe->getIdEnveloppeElectro(), $enveloppe->getOrganisme());
                    $func = function ($value) {
                        $encrypt = Atexo_Util::getSfService(Encryption::class);

                        return $encrypt->cryptId($value);
                    };
                    $element = array_map($func, $idsBlob);
                }
            }
        }
    }

    public static function getLabelTypeEnveloppe(string $typeEnveloppe)
    {
        return match ($typeEnveloppe) {
            Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE') => 'ENVELOPPE_CANDIDATURE',
            Atexo_Config::getParameter('TYPE_ENV_OFFRE') => 'ENVELOPPE_OFFRE',
            Atexo_Config::getParameter('TYPE_ENV_ANONYMAT') => 'ENVELOPPE_ANONYMA',
            default => '',
        };
    }

    public static function getFilesSizeEnveloppes($idsEnveloppes, $org)
    {
        $taille = 0;
        if (is_array($idsEnveloppes) && count($idsEnveloppes)) {
            $paramIdsEnveloppes = implode("','", $idsEnveloppes);
            $query = <<<SQL
SELECT 
    SUM(`taille_fichier`) 
FROM 
    `fichierEnveloppe` 
WHERE 
        id_enveloppe IN('$paramIdsEnveloppes') 
    AND 
        organisme LIKE '$org'
SQL;
            echo $query;
            $statement = Atexo_Db::getLinkCommon(true)->prepare($query);
            $statement->execute();
            $taille = $statement->fetch(PDO::FETCH_COLUMN);
        }

        return $taille;
    }

    public static function getFilesSizeOffres($idsOffres, $org)
    {
        $taille = 0;
        if (is_array($idsOffres) && count($idsOffres)) {
            $paramIdsOffre = implode("','", $idsOffres);
            $query = <<<SQL
SELECT 
    SUM(`taille_fichier`) 
FROM 
    `fichierEnveloppe`,
    `Enveloppe`,
    `Offres`
WHERE 
        Offres.id IN('$paramIdsOffre') 
    AND 
         Offres.organisme = '$org'
	AND
	    Enveloppe.offre_id = Offres.id
	AND
		Enveloppe.organisme = Offres.organisme
	AND
		fichierEnveloppe.id_enveloppe = Enveloppe.id_enveloppe_electro
	AND
	fichierEnveloppe.organisme = Enveloppe.organisme
SQL;
            echo $query;
            $statement = Atexo_Db::getLinkCommon(true)->prepare($query);
            $statement->execute();
            $taille = $statement->fetch(PDO::FETCH_COLUMN);
        }

        return $taille;
    }
}
