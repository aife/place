<?php

namespace Application\Service\Atexo\Consultation;

use Application\Propel\Mpe\CommonAnnonceJAL;
use Application\Propel\Mpe\CommonReferentielFormXml;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Publicite\Atexo_Publicite_DestinataireJAL;
use DomDocument;
use Prado\Prado;

/**
 * Permet de traiter le dossier d'archivage publicite.
 *
 * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Consultation_ArchivePublicite
{
    /**
     * Permet de retourner le libelle de statut destinataire.
     *
     * @param int $statut le statut destinataire
     *
     * @return string le libelle du statut destinataire
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public static function displayLibelleStatutDestinataire($statut)
    {
        if ($statut == Atexo_Config::getParameter('DESTINATAIRE_EN_ATTENTE')) {
            return Prado::localize('DESTINATAIRE_EN_ATTENTE');
        } elseif ($statut == Atexo_Config::getParameter('DESTINATAIRE_PUBLIE')) {
            return Prado::localize('DESTINATAIRE_PUBLIE');
        } elseif ($statut == Atexo_Config::getParameter('DESTINATAIRE_REFUSE')) {
            return Prado::localize('DESTINATAIRE_REFUSE');
        } elseif ($statut == Atexo_Config::getParameter('DESTINATAIRE_ENVOYE')) {
            return Prado::localize('TEXT_ENVOYE');
        } elseif ($statut == Atexo_Config::getParameter('DESTINATAIRE_STATUT_AV')) {
            return Prado::localize('DESTINATAIRE_EN_ATTENTE').' de publication';
        } elseif ($statut == Atexo_Config::getParameter('DESTINATAIRE_ENVOI_ECHOUE')) {
            return 'En anomalie - Envoi échoué';
        }
        //////////////////////////////////////
        elseif (($statut == Atexo_Config::getParameter('DESTINATAIRE_STATUT_RG'))
        || ($statut == Atexo_Config::getParameter('DESTINATAIRE_STATUT_RX'))
        || ($statut == Atexo_Config::getParameter('DESTINATAIRE_STATUT_RE'))) {
            return Prado::localize('DESTINATAIRE_REFUSE');
        }
        //////////////////////////////////////
        elseif (($statut == Atexo_Config::getParameter('DESTINATAIRE_STATUT_CP'))
        || ($statut == Atexo_Config::getParameter('DESTINATAIRE_STATUT_PU'))) {
            return Prado::localize('DESTINATAIRE_PUBLIE');
        }
    }

    /**
     * Permet de retourner le formulaire xml.
     *
     * @param CommonReferentielFormXml $statut       le statut destinataire
     * @param PropelPDO                $connexionOrg
     *
     * @return string le nom du fichier s'il est créé
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function getFormulaireXml(CommonReferentielFormXml $formulaireXml, $connexionOrg)
    {
        $filename = Atexo_Config::getParameter('COMMON_TMP').'xmlPub_'.session_id().microtime();
        if (!Atexo_Util::write_file($filename, $formulaireXml->getXml())) {
            return null;
        }

        return $filename;
    }

    /**
     * Permet de retourner le format libre annonce xml.
     *
     * @param CommonAnnonceJAL $annonceJal annonce JAL
     * @param string           $org        l'organisme
     *
     * @return string le chemin du fichier
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public static function getFormatLibreAnnonceXml(CommonAnnonceJAL $annonceJal, $org)
    {
        $domDocument = new DomDocument('1.0', 'ISO-8859-1');
        $domDocument->formatOutput = true;
        $domDocument->preserveWhiteSpace = false;

        $formulaire = $domDocument->createElement('Formulaire');

        $nom = $domDocument->createElement('Nom');
        $nomText = $domDocument->createTextNode(utf8_encode($annonceJal->getLibelleType()));
        $nom->appendChild($nomText);
        $formulaire->appendChild($nom);

        $dateCreation = $domDocument->createElement('DateCreation');
        $dateCreationText = $domDocument->createTextNode(utf8_encode(Atexo_Util::iso2frnDateTime($annonceJal->getDateCreation(), false)));
        $dateCreation->appendChild($dateCreationText);
        $formulaire->appendChild($dateCreation);

        $listDestinataires = $domDocument->createElement('Destinataires');
        $listDest = (new Atexo_Publicite_DestinataireJAL())->retreiveListDestinataire($annonceJal->getId(), $org);
        foreach ($listDest as $destinataireAnnonce) {
            //$destinataireAnnonce = new Destinataireannoncejal();

            $destinataire = $domDocument->createElement('Destinataire');
            $destinataire->setAttribute('nom', utf8_encode($destinataireAnnonce->getCommonJAL()->getNom()));
            $destinataire->setAttribute('email', utf8_encode($destinataireAnnonce->getCommonJAL()->getEmail()));
            $statut = $domDocument->createElement('Statut');
            $statutText = $domDocument->createTextNode(utf8_encode(self::displayLibelleStatutDestinataire($destinataireAnnonce->getStatut())));
            $statut->appendChild($statutText);
            $destinataire->appendChild($statut);

            $dateEnvoi = $domDocument->createElement('DateEnvoi');
            $dateEnvoiText = $domDocument->createTextNode(utf8_encode($destinataireAnnonce->retreiveDateEnvoi()));
            $dateEnvoi->appendChild($dateEnvoiText);
            $destinataire->appendChild($dateEnvoi);

            $datePublication = $domDocument->createElement('DatePuplication');
            $datePublicationText = $domDocument->createTextNode(utf8_encode(Atexo_Util::iso2frnDate($destinataireAnnonce->retreiveDatePublication())));
            $datePublication->appendChild($datePublicationText);
            $destinataire->appendChild($datePublication);

            $ar = $domDocument->createElement('AccuseReception');
            $arText = $domDocument->createTextNode(utf8_encode('NA'));
            $ar->appendChild($arText);
            $destinataire->appendChild($ar);
            $listDestinataires->appendChild($destinataire);
        }
        $formulaire->appendChild($listDestinataires);

        $domDocument->appendChild($formulaire);

        $filename = Atexo_Config::getParameter('COMMON_TMP').'xmlAnnonceMeta_'.session_id().microtime();
        $filename = str_replace(' ', '', $filename);

        $domDocument->save($filename);

        return $filename;
    }

    /**
     * Permet de retourner le fichier xml moniteur.
     *
     * @param string    $xml          le contenu de l'xml
     * @param PropelPDO $connexionOrg
     *
     * @return string le chemin du fichier
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function getFileXmlMoniteur($xml, $connexionOrg)
    {
        $filename = Atexo_Config::getParameter('COMMON_TMP').'xmlPub_'.session_id().microtime();
        if (!Atexo_Util::write_file($filename, $xml)) {
            return null;
        }

        return $filename;
    }
}
