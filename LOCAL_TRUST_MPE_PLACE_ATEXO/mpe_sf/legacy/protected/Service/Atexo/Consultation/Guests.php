<?php

namespace Application\Service\Atexo\Consultation;

use App\Enum\Habilitation\HabilitationSlug;
use App\Service\Agent\HabilitationTypeProcedureService;
use App\Service\AtexoUtil;
use App\Utils\Utils;
use App\Service\Consultation\FavorisService;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonAgent;
use Application\Propel\Mpe\CommonAgentPeer;
use Application\Propel\Mpe\CommonAgentQuery;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationFavoris;
use Application\Propel\Mpe\CommonConsultationFavorisPeer;
use Application\Propel\Mpe\CommonConsultationFavorisQuery;
use Application\Propel\Mpe\CommonConsultationPeer;
use Application\Propel\Mpe\CommonHabilitationAgentPeer;
use Application\Propel\Mpe\CommonInterneConsultationPeer;
use Application\Propel\Mpe\CommonInterneConsultationSuiviSeulPeer;
use Application\Propel\Mpe\CommonInvitePermanentTransverseQuery;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Config\Atexo_Config_Exception;
use Application\Service\Atexo\Consultation\Guests\Atexo_Consultation_Guests_CriteriaVo;
use Application\Service\Atexo\Consultation\Guests\Atexo_Consultation_Guests_Exception;

/**
 * Classe de manipulation des invités d'une consultation.
 *
 * @author ZAKI Anas <anas.zaki@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Consultation_Guests
{
    /**
     * Recherche les invités d'une consultation.
     *
     * @param Atexo_Consultation_Guests_CriteriaVo $guestCriteriaVo
     *
     * @return Atexo_Consultation_GuestVo[]
     */
    public static function searchNonPermanentGuests($guestCriteriaVo)
    {
        // connexion
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));

        // Récupération des paramètres
        $poleAgent = $guestCriteriaVo->getCurAgentServiceId();
        $acrOrg = $guestCriteriaVo->getOrgAcronym();
        $name = $guestCriteriaVo->getName();
        $excludedIdAgents = ($guestCriteriaVo->getExcludedAgents());

        if (0 === $poleAgent || '' === $acrOrg) {
            throw new Atexo_Consultation_Guests_Exception('searchNonPermanentGuests: Wrong parameters');
        }

        $c = new Criteria();

        // Ajout du critère organisme
        $c->add(CommonAgentPeer::ORGANISME, $acrOrg, Criteria::EQUAL);
        $c->addAnd(CommonAgentPeer::DELETED_AT, null, Criteria::EQUAL);

        // Ajout du critère nom, tri par ordre alphabétique
        if (isset($name) && '' != $name) {
            $c->add(CommonAgentPeer::NOM, '%'.$name.'%', Criteria::LIKE);
            $c->addAscendingOrderByColumn(CommonAgentPeer::NOM);
        }

        // Exclure d'éventuels agents
        if (count($excludedIdAgents) > 0) {
            $c->add(CommonAgentPeer::ID, $excludedIdAgents, Criteria::NOT_IN);
        }
        if ('' !== $poleAgent) {
            $c->add(CommonAgentPeer::SERVICE_ID, $poleAgent, Criteria::EQUAL);
        }

        if ($guestCriteriaVo->getLimit()) {
            $c->setLimit($guestCriteriaVo->getLimit());
        }
        if ($guestCriteriaVo->getOffset()) {
            $c->setOffset($guestCriteriaVo->getOffset());
        }

        $nonPermanentGuests = CommonHabilitationAgentPeer::doSelectJoinCommonAgent($c, $connexionCom);

        // Construction des objects guestsVo
        $guestsArrayVo = [];

        if (is_array($nonPermanentGuests)) {
            foreach ($nonPermanentGuests as $guest) {
                $guestVo = new Atexo_Consultation_GuestVo();
                $guestVo->populate($guest->getCommonAgent());
                $guestVo->setPermanentGuest(0);
                $guestVo->setTypeInvitation(0);
                $guestsArrayVo[] = $guestVo;
            }
        }

        return $guestsArrayVo;
    }

    /**
     * Transforme en objets les invités récupérés de la session.
     *
     * @param array $guestsArray
     *
     * @return guestsVoObjects
     */
    public static function toArrayGuestsVo($guestsArray)
    {
        $i = 0;
        $guestsArrayVo = [];
        if (is_array($guestsArray)) {
            foreach ($guestsArray as $key => $guest) {
                $guestVo[$i] = new Atexo_Consultation_GuestVo();
                if ('1' == $guest['InvitePermanent']) {
                    $guestVo[$i]->setPermanentGuest(1);
                } else {
                    $guestVo[$i]->setPermanentGuest(0);
                }
                $guestVo[$i]->setId($key);
                $guestVo[$i]->setFirstName($guest['Prenom']);
                $guestVo[$i]->setLastName($guest['Nom']);

                if ($guest['EntiteCom']) {
                    $guestVo[$i]->setEntityPurchase($guest['EntiteCom']);
                }
                if ('1' == $guest['SuiviSeul']) {
                    $guestVo[$i]->setTypeInvitation(0);
                } elseif ('1' == $guest['SuiviHabilitation']) {
                    $guestVo[$i]->setTypeInvitation(1);
                }
                $guestsArrayVo[] = $guestVo[$i];
                ++$i;
            }
        }

        return $guestsArrayVo;
    }

    /**
     * Transforme les objets guestsVo en tableau.
     *
     * @param array $arrayGuestsVo
     *
     * @return array
     */
    public static function guestVoToArrayGuests($guestsArrayVo)
    {
        $guestsArray = [];
        if (is_array($guestsArrayVo)) {
            foreach ($guestsArrayVo as $guestVo) {
                if ('0' == $guestVo->getTypeInvitation()) {
                    $suiviSeul = '1';
                    $suiviHabilitation = '0';
                } else {
                    $suiviSeul = '0';
                    $suiviHabilitation = '1';
                }
                $guestsArray[$guestVo->getId()] = ['Nom' => $guestVo->getLastName(),
                    'Prenom' => $guestVo->getFirstName(),
                    'InvitePermanent' => $guestVo->getPermanentGuest(),
                    'SuiviSeul' => $suiviSeul,
                    'SuiviHabilitation' => $suiviHabilitation,
                    'EntiteCom' => $guestVo->getEntityPurchase(),
                    'CheminEntite' => '',
                ];
            }
        }

        return $guestsArray;
    }

    /**
     * @param $reference
     *
     * @return array
     */
    public static function listAgentConsultationFavoris($reference, $params = [])
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $favoris = new CommonConsultationFavorisQuery();
        $listFavoris = $favoris->findByIdConsultation((int) $reference);
        $listAgents = [];

        if (!empty($listFavoris)) {
            $reponseReceived = false;
            if (array_key_exists('reponseReceived', $params)) {
                $reponseReceived = true;
            }

            $consultationCloture = false;
            if (array_key_exists('consultationCloture', $params)) {
                $consultationCloture = true;
            }

            $formulairePoserQuestion = false;
            if (array_key_exists('formulairePoserQuestion', $params)) {
                $formulairePoserQuestion = true;
            }

            $templateEnvoieCourrierElectronique = false;
            if (array_key_exists('templateEnvoieCourrierElectronique', $params)) {
                $templateEnvoieCourrierElectronique = true;
            }

            $sendAlertePublicationBoamp = false;
            if (array_key_exists('sendAlertePublicationBoamp', $params)) {
                $sendAlertePublicationBoamp = true;
            }

            $sendAlerteEchecPublicationBoamp = false;
            if (array_key_exists('sendAlerteEchecPublicationBoamp', $params)) {
                $sendAlerteEchecPublicationBoamp = true;
            }

            foreach ($listFavoris as $consultationFavoris) {
                $addSendAgent = false;
                $agentQuery = new CommonAgentQuery();
                $agent = $agentQuery->findOneById((int) $consultationFavoris->getIdAgent(), $connexion);
                if ($agent instanceof CommonAgent) {
                    if (true === $reponseReceived) {
                        if ('1' === $agent->getAlerteReponseElectronique()) {
                            $addSendAgent = true;
                        }
                    }

                    if (true === $consultationCloture) {
                        if ('1' === $agent->getAlerteClotureConsultation()) {
                            $addSendAgent = true;
                        }
                    }

                    if (true === $formulairePoserQuestion) {
                        if ('1' === $agent->getAlerteQuestionEntreprise()) {
                            $addSendAgent = true;
                        }
                    }

                    if (true === $templateEnvoieCourrierElectronique) {
                        if ('1' === $agent->getAlerteReceptionMessage()) {
                            $addSendAgent = true;
                        }
                    }

                    if (true === $sendAlertePublicationBoamp) {
                        if ('1' === $agent->getAlertePublicationBoamp()) {
                            $addSendAgent = true;
                        }
                    }

                    if (true === $sendAlerteEchecPublicationBoamp) {
                        if ('1' === $agent->getAlerteEchecPublicationBoamp()) {
                            $addSendAgent = true;
                        }
                    }

                    if (true === $addSendAgent) {
                        $listAgents[] = $agent->getId();
                    }
                }
            }
        }

        return $listAgents;
    }

    public function deleteGuests($reference, $org)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $c = new Criteria();
        $c->add(CommonInterneConsultationPeer::CONSULTATION_ID, $reference, Criteria::EQUAL);
        $c->add(CommonInterneConsultationPeer::ORGANISME, $org, Criteria::EQUAL);
        CommonInterneConsultationPeer::doDelete($c, $connexion);
        $c = new Criteria();
        $c->add(CommonInterneConsultationSuiviSeulPeer::CONSULTATION_ID, $reference, Criteria::EQUAL);
        $c->add(CommonInterneConsultationSuiviSeulPeer::ORGANISME, $org, Criteria::EQUAL);
        CommonInterneConsultationSuiviSeulPeer::doDelete($c, $connexion);
    }

    public function getAllInvitedAgents($reference, $organisme)
    {
        $AllGuests = [];

        $criteriaVo = new Atexo_Consultation_Guests_CriteriaVo();
        $criteriaVo->setReadOnlyGuests(false);
        $criteriaVo->setIdReference($reference);
        $criteriaVo->setOrgAcronym($organisme);
        $AllGuests = self::retrieveInvitedGuests($criteriaVo);

        $criteriaVo->setReadOnlyGuests(true);
        $AllGuests = array_merge(self::retrieveInvitedGuests($criteriaVo), $AllGuests);

        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $commonConsultation = CommonConsultationPeer::retrieveByPK($reference, $connexion);
        if ($commonConsultation instanceof CommonConsultation) {
            $criteriaVo = new Atexo_Consultation_Guests_CriteriaVo();
            $criteriaVo->setCurAgentServiceId($commonConsultation->getServiceId());
            $criteriaVo->setOrgAcronym($organisme);
            $criteriaVo->setAssociatedServiceId($commonConsultation->getServiceAssocieId());
            $criteriaVo->setCentralizedOrganization(Atexo_Module::isEnabled('OrganisationCentralisee', $organisme));

            $permanentsGuestsMonEntitee = self::retrievePermanentGuestsMonEntitee($criteriaVo);
            $permanentsGuestsEntiteeDependante = self::retrievePermanentGuestsEntiteeDependante($criteriaVo);
            $permanentsGuestsEntiteeTransverse = self::retrievePermanentGuestsEntiteeTransverse($criteriaVo);

            $AllGuests = array_unique(
                array_merge(
                    $permanentsGuestsMonEntitee,
                    $permanentsGuestsEntiteeDependante,
                    $permanentsGuestsEntiteeTransverse,
                    $AllGuests
                ),
                SORT_REGULAR
            );
        }

        return $AllGuests;
    }

    public function retrieveInvitedGuests(Atexo_Consultation_Guests_CriteriaVo $criteriaVo)
    {
        $criteria = new Criteria();
        if ($criteriaVo->getReadOnlyGuests()) {
            $criteria->add(CommonInterneConsultationSuiviSeulPeer::CONSULTATION_ID, $criteriaVo->getIdReference());
            $criteria->add(CommonInterneConsultationSuiviSeulPeer::ORGANISME, $criteriaVo->getOrgAcronym());
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            $guests = CommonInterneConsultationSuiviSeulPeer::doSelect($criteria, $connexion);
            $typeInvitation = 0;
        } else {
            $criteria->add(CommonInterneConsultationPeer::CONSULTATION_ID, $criteriaVo->getIdReference());
            $criteria->add(CommonInterneConsultationPeer::ORGANISME, $criteriaVo->getOrgAcronym());
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            $guests = CommonInterneConsultationPeer::doSelect($criteria, $connexion);
            $typeInvitation = 1;
        }
        $guestsArrayVo = [];
        if ($guests) {
            foreach ($guests as $Oneguest) {
                $agentInfos = (new Atexo_Agent())->retrieveAgent($Oneguest->getInterneId(), true);
                if (0 === strcasecmp($agentInfos->getOrganisme(), $criteriaVo->getOrgAcronym())) {
                    $guestVo = new Atexo_Consultation_GuestVo();
                    $guestVo->populate($agentInfos);
                    $guestVo->setPermanentGuest(0);
                    $guestVo->setTypeInvitation($typeInvitation);
                    $guestsArrayVo[] = $guestVo;
                }
            }
        }

        return $guestsArrayVo;
    }

    /**
     * retourne la liste des agents rattachés à l'entité d'achat passée en paramètre et qui ont l'habilitation "Invité permanent sur mon entité".
     *
     * @param Atexo_Consultation_Guests_CriteriaVo $guestCriteriaVo
     *
     * @return Atexo_Consultation_GuestVo[]
     */
    public static function retrievePermanentGuestsMonEntitee($guestCriteriaVo)
    {
        // connexion
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        // Récupération des paramètres
        $curServiceId = $guestCriteriaVo->getCurAgentServiceId();
        $acrOrg = $guestCriteriaVo->getOrgAcronym();

        if ('' === $curServiceId || '' === $acrOrg) {
            throw new Atexo_Consultation_Guests_Exception('retrievePermanentGuestsMonEntitee: Wrong parameters');
        }
        // Tableau de retour
        $guestsArrayVo = [];
        $permanentGuestMonEntiteeCriteria = new Criteria();

        // Ajout du critère organisme
        $permanentGuestMonEntiteeCriteria->add(CommonAgentPeer::ORGANISME, $acrOrg, Criteria::EQUAL);

        if ('' !== $curServiceId) {
            // Ajout du critère Pole de l'agent
            $criterion = $permanentGuestMonEntiteeCriteria->getNewCriterion(CommonAgentPeer::SERVICE_ID, $curServiceId, Criteria::EQUAL);
            $permanentGuestMonEntiteeCriteria->addAnd($criterion);

            $criterion = $permanentGuestMonEntiteeCriteria->getNewCriterion(CommonAgentPeer::DELETED_AT, null, Criteria::EQUAL);
            $permanentGuestMonEntiteeCriteria->addAnd($criterion);

            $permanentGuestsMonEntitee = CommonAgentPeer::doSelect($permanentGuestMonEntiteeCriteria, $connexionCom);

            if (is_array($permanentGuestsMonEntitee)) {
                foreach ($permanentGuestsMonEntitee as $guest) {
                    if (
                        Atexo_Util::getSfService(HabilitationTypeProcedureService::class)
                            ->checkHabilitationForPrado(
                                $guest->getId(),
                                HabilitationSlug::INVITE_PERMANENT_MON_ENTITE->value
                            )
                    ) {
                        $guestVo = new Atexo_Consultation_GuestVo();
                        $guestVo->populate($guest);
                        // Ajout du critère Pole de l'agent
                        $guestVo->setPermanentGuest(1);
                        $guestVo->setTypeInvitation(1);
                        $guestsArrayVo[] = $guestVo;
                    }
                }
            }
        }

        return $guestsArrayVo;
    }

    /**
     * retourne la liste des agents rattachés aux mères de l'entité d'achat passée en paramètre et qui ont l'habilitation "Invité permanent sur les entités qui dépendent de mon entité".
     *
     * @param Atexo_Consultation_Guests_CriteriaVo $guestCriteriaVo
     *
     * @return Atexo_Consultation_GuestVo[]
     */
    public static function retrievePermanentGuestsEntiteeDependante($guestCriteriaVo)
    {
        // connexion
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        // Récupération des paramètres
        $curServiceId = $guestCriteriaVo->getCurAgentServiceId();
        $acrOrg = $guestCriteriaVo->getOrgAcronym();

        if ('' === $curServiceId || '' === $acrOrg) {
            throw new Atexo_Consultation_Guests_Exception('retrievePermanentGuestsEntiteeDependante: Wrong parameters');
        }
        // Tableau de retour
        $guestsArrayVo = [];
        $permanentGuestEntiteeDependanteCriteria = new Criteria();

        // Ajout du critère organisme
        $permanentGuestEntiteeDependanteCriteria->add(CommonAgentPeer::ORGANISME, $acrOrg, Criteria::EQUAL);

        if ('' !== $curServiceId) {
            // Récupération de la liste des entités parents de l'entité de rattachement de la consultation
            $poleAgentParents = Atexo_EntityPurchase::getArParentsId($curServiceId, $acrOrg, false);

            $criterion = $permanentGuestEntiteeDependanteCriteria->getNewCriterion(CommonAgentPeer::SERVICE_ID, $poleAgentParents, Criteria::IN);
            $criterion2 = $permanentGuestEntiteeDependanteCriteria->getNewCriterion(CommonAgentPeer::SERVICE_ID, null, Criteria::ISNULL);
            $criterion->addOr($criterion2);
            $permanentGuestEntiteeDependanteCriteria->addAnd($criterion);

            $criterion = $permanentGuestEntiteeDependanteCriteria->getNewCriterion(CommonAgentPeer::DELETED_AT, null, Criteria::EQUAL);
            $permanentGuestEntiteeDependanteCriteria->addAnd($criterion);

            $permanentGuestsEntiteeDependante = CommonAgentPeer::doSelect($permanentGuestEntiteeDependanteCriteria, $connexionCom);

            if (is_array($permanentGuestsEntiteeDependante)) {
                foreach ($permanentGuestsEntiteeDependante as $guest) {
                    if (
                        Atexo_Util::getSfService(HabilitationTypeProcedureService::class)
                            ->checkHabilitationForPrado(
                                $guest->getId(),
                                HabilitationSlug::INVITE_PERMANENT_ENTITE_DEPENDANTE->value
                            )
                    ) {
                        $guestVo = new Atexo_Consultation_GuestVo();
                        $guestVo->populate($guest);
                        // Ajout du critère Pole de l'agent
                        $guestVo->setPermanentGuest(1);
                        $guestVo->setTypeInvitation(1);
                        $guestsArrayVo[] = $guestVo;
                    }
                }
            }
        }

        return $guestsArrayVo;
    }

    /**
     * retourne la liste des agents qui ont l'habilitation "Invité permanent transverse" et qui sont associés comme invité permanent tranverse de l'entité d'achat passée en paramètre.
     *
     * @param Atexo_Consultation_Guests_CriteriaVo $guestCriteriaVo
     *
     * @return Atexo_Consultation_GuestVo[]
     */
    public static function retrievePermanentGuestsEntiteeTransverse($guestCriteriaVo)
    {
        // connexion
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        // Récupération des paramètres
        $curServiceId = $guestCriteriaVo->getCurAgentServiceId();
        $acrOrg = $guestCriteriaVo->getOrgAcronym();
        if ('' === $curServiceId || '' === $acrOrg) {
            throw new Atexo_Consultation_Guests_Exception('retrievePermanentGuestsEntiteeTransverse: Wrong parameters');
        }
        // Tableau de retour
        $guestsArrayVo = [];
        $permanentGuestEntiteeTransverse = new Criteria();

        // Ajout du critère organisme
        $permanentGuestEntiteeTransverse->add(CommonAgentPeer::ORGANISME, $acrOrg, Criteria::EQUAL);
        $permanentGuestEntiteeTransverse->addAnd(CommonAgentPeer::DELETED_AT, null, Criteria::ISNULL);

        if ('' !== $curServiceId) {
            $listesInvitesPermanentsTransverse = [];
            // Récupération de la liste des agents qui sont associés comme invité permanent tranverse de l'entité d'achat passée en paramètre
            if (0 === $curServiceId) {
                $curServiceId = null;
            }
            $listesTInvitesPermanentsTransverse = (new CommonInvitePermanentTransverseQuery())->findByArray([
                'serviceId' => $curServiceId,
                'acronyme' => $acrOrg,
            ]);

            foreach ($listesTInvitesPermanentsTransverse as $item) {
                $listesInvitesPermanentsTransverse[] = $item->getAgentId();
            }

            $criterion = $permanentGuestEntiteeTransverse->getNewCriterion(CommonAgentPeer::ID, $listesInvitesPermanentsTransverse, Criteria::IN);
            $permanentGuestEntiteeTransverse->addAnd($criterion);

            $permanentGuestsEntiteeTransverse = CommonAgentPeer::doSelect($permanentGuestEntiteeTransverse, $connexionCom);

            if (is_array($permanentGuestsEntiteeTransverse)) {
                foreach ($permanentGuestsEntiteeTransverse as $guest) {
                    if (
                        Atexo_Util::getSfService(HabilitationTypeProcedureService::class)
                            ->checkHabilitationForPrado(
                                $guest->getId(),
                                HabilitationSlug::INVITE_PERMANENT_TRANSVERSE->value
                            )
                    ) {
                        $guestVo = new Atexo_Consultation_GuestVo();
                        $guestVo->populate($guest);
                        // Ajout du critère Pole de l'agent
                        $guestVo->setPermanentGuest(1);
                        $guestVo->setTypeInvitation(1);
                        $guestsArrayVo[] = $guestVo;
                    }
                }
            }
        }

        return $guestsArrayVo;
    }

    public function getIdsAgentFromArrayGuestsVo(array $guests)
    {
        $arrayIdsAgents = [];
        if (is_array($guests)) {
            foreach ($guests as $guest) {
                if ($guest instanceof Atexo_Consultation_GuestVo) {
                    $arrayIdsAgents[] = $guest->getId();
                }
            }
        }

        return $arrayIdsAgents;
    }

    /**
     * @param : $stringValues = idAgent-TypeInvitation, idAgent-TypeInvitation , ...
     */
    public function createArrayGuestFromValueString($stringValues)
    {
        $arrayGuests = [];
        $invites = explode(',', $stringValues);
        if (is_array($invites)) {
            foreach ($invites as $invite) {
                if (trim($invite)) {
                    [$idAgent, $typeInvitation] = explode('-', $invite);
                    $agent = (new CommonAgentQuery())->findOneById($idAgent);
                    if ($agent instanceof CommonAgent) {
                        $guest['Prenom'] = $agent->getPrenom();
                        $guest['Nom'] = $agent->getNom();
                        $guest['EntiteCom'] = Atexo_EntityPurchase::getServiceById($agent->getServiceId(), $agent->getOrganisme());
                        switch ($typeInvitation) {
                            case '1':
                                $guest['InvitePermanent'] = '1';
                                $guest['SuiviSeul'] = '0';
                                $guest['SuiviHabilitation'] = '1';
                                break;
                            case '2':
                                $guest['InvitePermanent'] = '0';
                                $guest['SuiviSeul'] = '0';
                                $guest['SuiviHabilitation'] = '1';
                                break;
                            case '3':
                                $guest['InvitePermanent'] = '0';
                                $guest['SuiviSeul'] = '1';
                                $guest['SuiviHabilitation'] = '0';
                                break;
                        }

                        $arrayGuests[$agent->getId()] = $guest;
                    }
                }
            }
        }

        return $arrayGuests;
    }

    /**
     * @param $reference
     * @param $org
     *
     * @throws Atexo_Config_Exception
     * @throws PropelException
     */
    public function deleteGuestConsultationFavoris($reference, $org, $tabGuests = [], $connexion = null)
    {
        if (null === $connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        }
        $c = new Criteria();
        $c->add(CommonInterneConsultationPeer::CONSULTATION_ID, $reference, Criteria::EQUAL);
        $c->add(CommonInterneConsultationPeer::ORGANISME, $org, Criteria::EQUAL);
        $listInterneConsultation = CommonInterneConsultationPeer::doSelect($c, $connexion);

        foreach ($listInterneConsultation as $interneConsultation) {
            $c = new Criteria();
            $c->add(
                CommonConsultationFavorisPeer::ID_CONSULTATION,
                $reference,
                Criteria::EQUAL
            );
            $c->add(
                CommonConsultationFavorisPeer::ID_AGENT,
                $interneConsultation->getInterneId(),
                Criteria::EQUAL
            );

            if (array_key_exists($interneConsultation->getInterneId(), $tabGuests)) {
                $consultationFavoris = CommonConsultationFavorisPeer::doSelectOne($c, $connexion);
                if ($consultationFavoris instanceof CommonConsultationFavoris) {
                    $tabGuests[$interneConsultation->getInterneId()] = true;
                }
            }

            CommonConsultationFavorisPeer::doDelete($c, $connexion);
        }

        $c = new Criteria();
        $c->add(CommonInterneConsultationSuiviSeulPeer::CONSULTATION_ID, $reference, Criteria::EQUAL);
        $c->add(CommonInterneConsultationSuiviSeulPeer::ORGANISME, $org, Criteria::EQUAL);
        $listInterneConsultationSeul = CommonInterneConsultationSuiviSeulPeer::doSelect($c, $connexion);

        foreach ($listInterneConsultationSeul as $interneConsultationSeul) {
            $c = new Criteria();
            $c->add(
                CommonConsultationFavorisPeer::ID_CONSULTATION,
                $reference,
                Criteria::EQUAL
            );
            $c->add(
                CommonConsultationFavorisPeer::ID_AGENT,
                $interneConsultationSeul->getInterneId(),
                Criteria::EQUAL
            );

            if (array_key_exists($interneConsultationSeul->getInterneId(), $tabGuests)) {
                $consultationFavoris = CommonConsultationFavorisPeer::doSelectOne($c, $connexion);
                if ($consultationFavoris instanceof CommonConsultationFavoris) {
                    $tabGuests[$interneConsultationSeul->getInterneId()] = true;
                }
            }

            CommonConsultationFavorisPeer::doDelete($c, $connexion);
            /** @var FavorisService $favorisService */
            $favorisService = Atexo_Util::getSfService(FavorisService::class);
            $favorisService->removeInvitePonctuelFromFavoris($reference, $interneConsultationSeul->getInterneId());
        }

        return $tabGuests;
    }

    public static function guestConsultation($reference, $org, $connexion = null)
    {
        $tabGuests = [];
        if (null === $connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        }
        $c = new Criteria();
        $c->add(CommonInterneConsultationPeer::CONSULTATION_ID, $reference, Criteria::EQUAL);
        $c->add(CommonInterneConsultationPeer::ORGANISME, $org, Criteria::EQUAL);
        $interneConsultations = CommonInterneConsultationPeer::doSelect($c, $connexion);

        foreach ($interneConsultations as $interneConsultation) {
            $tabGuests[$interneConsultation->getInterneId()] = false;
        }

        $c = new Criteria();
        $c->add(CommonInterneConsultationSuiviSeulPeer::CONSULTATION_ID, $reference, Criteria::EQUAL);
        $c->add(CommonInterneConsultationSuiviSeulPeer::ORGANISME, $org, Criteria::EQUAL);
        $interneConsultationSuiviSeuls = CommonInterneConsultationSuiviSeulPeer::doSelect($c, $connexion);

        foreach ($interneConsultationSuiviSeuls as $interneConsultationSuiviSeul) {
            $tabGuests[$interneConsultationSuiviSeul->getInterneId()] = false;
        }

        return $tabGuests;
    }
}
