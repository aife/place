<?php

namespace Application\Service\Atexo\Consultation;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonConsultationPeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Exception;

/**
 * Classe de.
 *
 * @author Khalid BENAMAR <khalid.benamar@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Consultation_Validation
{
    private string $_reference = '';
    private string $_organisme = '';
    private string $_conOrg = '';
    private string $_conCom = '';

    public function __construct($consultationId, $organisme)
    {
        $this->_reference = $consultationId;
        $this->_organisme = $organisme;
        $this->_conCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $this->_conOrg = Propel::getConnection($organisme.Atexo_Config::getParameter('CONST_READ_WRITE'));
    }

    public static function validateConsultation($consultationId)
    {
        $atexoConsltation = new Atexo_Consultation();
        $consltation = $atexoConsltation->retrieveConsultation($consultationId);
        if ($consltation && '1' == $consltation->getChiffrementOffre()) {
            return 'index.php?page=Agent.ValiderUneConsultation&id='.$consultationId.'&consRefEnv=1';
        } else {
            return '#';
        }
    }

    public function validateWithoutChiffrement($idValidateur)
    {
        $this->_conCom->beginTransaction();
        try {
            $commonCons = CommonConsultationPeer::retrieveByPK($this->_reference, $this->_conCom);
            $commonCons->setDatevalidation(date('Y-m-d H:i:s'));
            $commonCons->setEtatValidation(Atexo_Config::getParameter('CONS_VALIDEE'));
            $commonCons->setIdValideur($idValidateur);
            $commonCons->save($this->_conCom);
            $this->_conCom->commit();
        } catch (Exception) {
            $this->_conCom->rollback();
        }
    }

    public function approve($idAprobateur)
    {
        $this->_conCom->beginTransaction();
        try {
            $commonCons = CommonConsultationPeer::retrieveByPK($this->_reference, $this->_conCom);
            $commonCons->setEtatValidation(Atexo_Config::getParameter('CONS_APPROUVEE'));
            $commonCons->setEtatApprobation(Atexo_Config::getParameter('CONS_APPROUVEE'));
            $commonCons->setIdApprobateur($idAprobateur);

            $commonCons->save($this->_conCom);
            $this->_conCom->commit();
        } catch (Exception) {
            $this->_conCom->rollback();
        }
    }
}
