<?php

namespace Application\Service\Atexo\Consultation;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonStatutEnveloppePeer;
use Application\Service\Atexo\Atexo_Config;
use Prado\Prado;

class Atexo_Consultation_StatutEnveloppe
{
    public function getStatutEnveloppeById($idStatut)
    {
        $statutsEnveloppes = self::getCachedStatutEnveloppe();
        if (is_array($statutsEnveloppes) && isset($statutsEnveloppes[$idStatut])) {
            return Prado::localize($statutsEnveloppes[$idStatut]);
        } else {
            return '-';
        }
    }

    public function getCachedStatutEnveloppe()
    {
        $cachedStatutEnveloppe = Prado::getApplication()->Cache->get('cachedStatutEnveloppe');

        if (!$cachedStatutEnveloppe) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            $c = new Criteria();
            $statusEnveloppe = CommonStatutEnveloppePeer::doSelect($c, $connexion);
            $allStatuts = [];
            foreach ($statusEnveloppe as $unStatut) {
                $allStatuts[$unStatut->getIdStatut()] = $unStatut->getDescription();
            }

            $cachedStatutEnveloppe = $allStatuts;
            Prado::getApplication()->Cache->set('cachedStatutEnveloppe', $cachedStatutEnveloppe, Atexo_Config::getParameter('TTL_PRADO_CACHE'));
        }

        return $cachedStatutEnveloppe;
    }
}
