<?php

namespace Application\Service\Atexo\Consultation;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonDecisionLotPeer;
use Application\Propel\Mpe\CommonOffres;
use Application\Propel\Mpe\CommonOrganisme;
use Application\Propel\Mpe\CommonRPA;
use Application\Propel\Mpe\CommonService;
use Application\Propel\Mpe\CommonServicePeer;
use Application\Propel\Mpe\CommonTContratTitulaireQuery;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Tree\Atexo_Consultation_Tree_Arborescence;
use Application\Service\Atexo\Consultation\Tree\Atexo_Consultation_Tree_Directory;
use Application\Service\Atexo\Consultation\Tree\Atexo_Consultation_Tree_File;
use Application\Service\Atexo\Consultation\Tree\Atexo_Consultation_Tree_Group;
use Application\Service\Atexo\Consultation\Tree\Atexo_Consultation_Tree_Item;
use Application\Service\Atexo\Consultation\Tree\Atexo_Consultation_Tree_Signature;
use Application\Service\Atexo\Consultation\Tree\Atexo_Consultation_Tree_String;
use Application\Service\Atexo\Consultation\Tree\Atexo_Consultation_Tree_TimeStamp;
use Application\Service\Atexo\Controller\Atexo_Controller_Front;
use Application\Service\Atexo\EntityPurchase\Atexo_EntityPurchase_RPA;
use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2;
use Application\Service\Atexo\Interfaces\Atexo_Interfaces_Offre;
use Application\Service\Atexo\Rest\Action\Atexo_Rest_Action_Actions;
use Application\Service\Atexo\Rest\Action\Atexo_Rest_Action_Consultation;
use DateTime;
use DOMDocument;
use DomNode;
use Exception;
use Prado\Prado;

/**
 * Classe de gestion d'archive XML.
 *
 * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Consultation_ArchiveXml
{
    private \Application\Service\Atexo\Consultation\Tree\Atexo_Consultation_Tree_Arborescence $docTree;
    private \Application\Propel\Mpe\CommonConsultation $consultation;
    private $organismeAcronyme;
    private ?\DOMDocument $domDocument = null;
    private $connexion;
    private $sizeArchive;
    private $lastestDate;
    private ?string $dateDepot = null;
    private bool $defineAccesRestriction = false; //Permet de préciser si la balise "AccesRestriction" est definie par le client
    private ?\Application\Propel\Mpe\CommonService $service = null;
    private $codeAccesRestriction;

    /**
     * Permet de setter les attribut du Atexo_Consultation_ArchiveXml.
     *
     * @param Atexo_Consultation_Tree_Arborescence $docTree l'arborescence, CommonConsultation $consultation la consultation, String $organisme l'organsime, Integer $sizeArchive la taille du fichier
     *
     * @return void
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function __construct(Atexo_Consultation_Tree_Arborescence $docTree, CommonConsultation $consultation, $organisme, $sizeArchive = 0)
    {
        $this->docTree = $docTree;
        $this->organismeAcronyme = $organisme;
        $this->consultation = $consultation;
        $this->sizeArchive = $sizeArchive;
    }

    /**
     * Get the value of [consultation] column.
     *
     * @return CommonConsultation la consultation
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    private function getConsultation()
    {
        return $this->consultation;
    }

    /**
     * Get the value of [docTree] column.
     *
     * @return Atexo_Consultation_Tree_Arborescence la consultation
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    private function getDocTree()
    {
        return $this->docTree;
    }

    /**
     * Get the value of [domDocument] column.
     *
     * @return DOMDocument domDocument
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    private function getDomDocument()
    {
        return $this->domDocument;
    }

    /**
     * Get the value of [connexion] column.
     *
     * @return PropelPDO connexion
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    private function getConnexion()
    {
        if (null == $this->connexion) {
            $this->connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));
        }

        return $this->connexion;
    }

    /**
     * Permet de retourner le noeaud d'un element.
     *
     * @param string $element l'element à creer,String $text le contenu du noeud de l'element
     *
     * @return DOMNode noeaud
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    private function textNode($element, $text)
    {
        $node = $this->getDomDocument()->createElement($element);
        $nodeText = $this->getDomDocument()->createTextNode($text);
        $node->appendChild($nodeText);

        return $node;
    }

    /**
     * permet de recuperer l'url de telechargement du DIC selon le parametrage.
     *
     * @param string $consultationId la reference de la Consultation
     *
     * @return string url de telechargement du DIC selon le parametrage
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2008
     */
    public function generateXml($useLangageCatalogue = true, $numLot = false)
    {
        if ($useLangageCatalogue) {
            Atexo_Languages::setLanguageCatalogue('agent');
        }

        $domDoc = new DOMDocument('1.0', 'UTF-8');

        $domDoc->formatOutput = true;
        $domDoc->preserveWhiteSpace = false;
        $xmlstylesheet = new \DOMProcessingInstruction('xml-stylesheet', ' type="text/xsl" href="archive.xsl"');

        $domDoc->appendChild($xmlstylesheet);
        $this->domDocument = $domDoc;
        $this->addArchiveTransfer(self::getArchiveSpecificConfig(), $numLot);
        $tmpPathXml = Atexo_Config::getParameter('COMMON_TMP') . 'xml_' . session_id() . time();

        // TODO : Test
        try {
            $this->domDocument->save($tmpPathXml);
        } catch (\Exception $e) {
            echo $e->getMessage() . $e->getTraceAsString();
            //throw $e;
        }

        return $tmpPathXml;
    }

    /**
     * Ajouter les donnees de "TransferIdentifier" dans archiveXml.
     *
     * @param array $data   donne de config
     * @param int   $numLot numero du lot
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function addArchiveTransfer($data, $numLot = false)
    {
        $archiveTransfert = $this->getDomDocument()->createElement('ArchiveTransfer');
        $archiveTransfert->setAttribute('xmlns', 'fr:gouv:ae:archive:draft:standard_echange_v0.2');

        // Comment
        //  <!-- La valeur comment aura toujours une valeur fixe : comme ci-dessous-->
        $url = explode('//', Atexo_Config::getParameter('PF_URL'));

        $url_absolute = explode('/', $url[1]);

        if (1 == $data['DefineAccessRestriction']['Value']) {
            $this->defineAccesRestriction = true;
        } else {
            $this->defineAccesRestriction = false;
        }

        if ($data['Comment']) {
            self::creerBalise('Comment', $data['Comment'], null, $this->getDomDocument(), $archiveTransfert);
        } else {
            $archiveTransfert->appendChild($this->textNode('Comment', Atexo_Util::toUtf8(Prado::localize('TEXT_TRANFERT_PIECES_MARCHE_PUBLIC')) . $url_absolute[0]));
        }

        // Date
        $annee = strftime('%Y');
        $mois = strftime('%m');
        $jour = strftime('%d');
        $dateExport = $annee . '-' . $mois . '-' . $jour;
        $date = $this->getDomDocument()->createElement('Date');
        $dateText = $this->getDomDocument()->createTextNode($dateExport . (($dateExport) ? 'T00:00:00Z' : ''));
        $date->appendChild($dateText);
        $archiveTransfert->appendChild($date);

        // TransferIdentifier :
        //identifiant de l'entité d'achat + identifiant interne (utilisateur) de la consultation
        $id = '';
        if (is_array($data['TransferIdentifier'])) {
            $id = 'EMEG_ACCORD_MP_0001-' . date('Ymd') . '-' . $this->getConsultation()->getOrganisme() . '-' . $this->getConsultation()->getServiceId() . '-';
            $id .= $this->getConsultation()->getReferenceUtilisateur() . '_' . $this->getConsultation()->getId();
            if ($numLot) {
                $id .= '_' . str_pad($numLot, 3, '0', STR_PAD_LEFT);
            }
            if (strlen($id) >= 256) {
                $id = substr($id, 0, 256);
            }
            $arrayTransferIdentifier = $data['TransferIdentifier'];
            $arrayTransferIdentifier['value'] = $id;
            self::creerBalise('TransferIdentifier', $arrayTransferIdentifier['value'], $arrayTransferIdentifier['attributes'], $this->getDomDocument(), $archiveTransfert);
        } else {
            $id = $this->organismeAcronyme . ' - ' . $this->getConsultation()->getServiceId() . ' - ' . $this->getConsultation()->getReferenceUtilisateur() . '_' . $this->getConsultation()->getId();
            if ($numLot) {
                $id .= '_' . str_pad($numLot, 3, '0', STR_PAD_LEFT);
            }
            $archiveTransfert->appendChild($this->textNode('TransferIdentifier', Atexo_Util::toUtf8($id)));
        }

        //calcul de latestDate et stockage de l'information
        $this->lastestDate = Atexo_Util::frnDate2iso(self::calculLatestDate());

        // -> Composant TransferingAgency
        $this->addTransferringAgency($archiveTransfert, self::getArchiveSpecificConfig('TransferringAgency'));

        // -> Composant ArchivalAgency
        $this->addArchivalAgency($archiveTransfert, self::getArchiveSpecificConfig('ArchivalAgency'));

        // -> Composants Contains
        $this->addContains($archiveTransfert, self::getArchiveSpecificConfig('Contains'), $numLot);

        // Ajout au document
        $this->getDomDocument()->appendChild($archiveTransfert);
    }

    /**
     * Permet d'ajouter les elements de transferring agency dans l'xml.
     *
     * @param DomNode $archiveTransfert,String $data les donnees
     *
     * @return void
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function addTransferringAgency(DomNode $archiveTransfert, $data)
    {
        $identificationEntity = null;
        $serviceId = $this->getConsultation()->getServiceId();
        $ministere = Atexo_Organismes::retrieveOrganismeByAcronyme($this->organismeAcronyme);
        $denominationOrg = Atexo_Util::toUtf8($ministere->getDenominationorg());
        $entiteLibelle = Atexo_Util::toUtf8((new Atexo_EntityPurchase())->getEntityLibelle($serviceId, $this->organismeAcronyme));

        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));
        $c = new Criteria();
        $c->add(CommonServicePeer::ID, $serviceId);
        $c->add(CommonServicePeer::ORGANISME, $this->organismeAcronyme);
        $service = CommonServicePeer::doSelectOne($c, $connexion);

        $transferringAgency = $this->getDomDocument()->createElement('TransferringAgency');

        if ($data['BusinessType']) {
            self::creerBalise('BusinessType', $data['BusinessType']['value'], $data['BusinessType']['attributes'], $this->getDomDocument(), $transferringAgency);
        }
        // Description :
        //<!-- mettre l'entité d'achat qui gère la consultation -->
        //<!-- concaténation des libellés du ministère, suivi de la hierarchie des pôles/direction/sous-direction -->
        if ($data['Description']) {
            self::creerBalise('Description', $data['Description'], null, $this->getDomDocument(), $transferringAgency);
        } else {
            $description = $this->getDomDocument()->createElement('Description');

            $desc = $denominationOrg;
            $allParent = Atexo_EntityPurchase::getAllParents($serviceId, $this->organismeAcronyme);
            if (count($allParent) > 0) {
                foreach ($allParent as $parent) {
                    $desc .= ', ' . $parent['libelle'];
                }
            }

            if ('' != $entiteLibelle) {
                $desc .= $entiteLibelle;
            }
            //nous sommes niveau organisme le plus haut, enlever la virgule et l'espace qui sont en trop
            else {
                $entiteLibelle = substr($desc, 0, (is_countable($desc) ? count($desc) : 0) - 2);
            }

            $descriptionText = $this->getDomDocument()->createTextNode(Atexo_Util::toUtf8($desc));
            $description->appendChild($descriptionText);
            $transferringAgency->appendChild($description);
        }
        $rpa = (new Atexo_EntityPurchase_RPA())->getResponsabeArchiveByIdService($serviceId, $this->organismeAcronyme);
        // Identification
        if ($data['Identification']) {
            self::creerBalise('Identification', self::formaterIdentifiant($data['Identification']['value']), $data['Identification']['attributes'], $this->getDomDocument(), $transferringAgency);
        } else {
            if (Atexo_Module::isEnabled('InterfaceArchiveArcadePmi')) {
                if ($rpa instanceof CommonRPA) {
                    $identificationEntity = Atexo_Util::toUtf8($rpa->getOrganisme());
                }
            } else {
                $identificationEntity = Atexo_Util::toUtf8($serviceId);
            }

            // pour les plateformes utilisant un socle externe
            if (Atexo_Module::isEnabled('socleExternePpp')) {
                if ($serviceId) {
                    $service = Atexo_EntityPurchase::retrieveEntityById($serviceId, $this->organismeAcronyme);
                    if ($service instanceof CommonService) {
                        $identificationEntity = Atexo_Util::toUtf8($service->getIdExterne());
                    } else {
                        $identificationEntity = $this->organismeAcronyme;
                    }
                } else {
                    $identificationEntity = $this->organismeAcronyme;
                }
            }
            $transferringAgency->appendChild($this->textNode('Identification', $identificationEntity));
        }

        //LegalClassification
        if ($data['LegalClassification']) {
            self::creerBalise('LegalClassification', $data['LegalClassification'], null, $this->getDomDocument(), $transferringAgency);
        }

        // Nom de l'entité d'achat :
        //      <!-- le nom de l'entité d'achat : rien n'apparaît lorsqu'il s'agit d'un niveau organisme-->
        if ($data['Name']) {
            self::creerBalise('Name', $data['Name'], null, $this->getDomDocument(), $transferringAgency);
        } else {
            $transferringAgency->appendChild($this->textNode('Name', Atexo_Util::toUtf8($entiteLibelle)));
        }

        //Contact

        if ($data['Contact']) {
            self::createContact($data['Contact'], 'Contact', $transferringAgency);
        } else {
            // Contact Responsable Archivage
            if (Atexo_Module::isEnabled('InterfaceArchiveArcadePmi')) {
                $personNameRespArchive = Atexo_Util::toUtf8(Prado::localize('TEXT_NON_RENSEIGNE'));
                if ($rpa instanceof CommonRPA) {
                    if ($rpa->getNom() && $rpa->getPrenom()) {
                        $personNameRespArchive = $rpa->getPrenom() . ' ' . $rpa->getNom();
                    }
                }

                $contactRespArchive = $this->getDomDocument()->createElement('Contact');
                $departementNameRespArchive = $entiteLibelle;
                $responsibilityRespArchive = 'responsable';

                $contactRespArchive->appendChild($this->textNode('DepartmentName', Atexo_Util::toUtf8($departementNameRespArchive)));
                $contactRespArchive->appendChild($this->textNode('PersonName', Atexo_Util::toUtf8($personNameRespArchive)));
                $contactRespArchive->appendChild($this->textNode('Responsibility', Atexo_Util::toUtf8($responsibilityRespArchive)));

                $transferringAgency->appendChild($contactRespArchive);
            }

            // Contact de l'entité d'achat:

            $contact = $this->getDomDocument()->createElement('Contact');

            $departementName = $entiteLibelle;
            $personName = $this->getConsultation()->getPrenomCreateur() . ' ' . $this->getConsultation()->getNomCreateur();
            $responsibility = 'redacteur';
            $createur = (new Atexo_Agent())->retrieveAgentById($this->getConsultation()->getIdCreateur());
            if ($createur) {
                $Uri = $createur->getEmail();
            } else {
                $Uri = 'nonrenseigne@nonrenseigne.fr';
            }

            $channel = 'mail';

            $idArchiveur = $this->getConsultation()->getIdArchiveur();
            if ($idArchiveur) {
                $agent = (new Atexo_Agent())->retrieveAgent($idArchiveur);
                if ($agent) {
                    $personName = $agent->getPrenom() . ' ' . $agent->getNom();
                }
            }
            $contact->appendChild($this->textNode('DepartmentName', Atexo_Util::toUtf8($departementName)));
            $contact->appendChild($this->textNode('PersonName', Atexo_Util::toUtf8($personName)));
            $contact->appendChild($this->textNode('Responsibility', Atexo_Util::toUtf8($responsibility)));

            // Communication : contact créateur de la consultation
            $communication = $this->getDomDocument()->createElement('Communication');
            $communication->appendChild($this->textNode('Channel', Atexo_Util::toUtf8($channel)));
            $communication->appendChild($this->textNode('URI', Atexo_Util::toUtf8($Uri)));
            $contact->appendChild($communication);
            $transferringAgency->appendChild($contact);
        }

        //Address
        if ($data['Address']) {
            self::CreateAdress($data['Address'], 'Address', $transferringAgency, $this->getDomDocument());
        } else {
            // Address de l'entité d'achat :
            $address = $this->getDomDocument()->createElement('Address');

            // Si idService = 0 on est dans le cas d'une consultation du ministere
            if (!$service) {
                $address->appendChild($this->textNode('BuildingNumber', ''));
                $address->appendChild($this->textNode('CityName', Atexo_Util::toUtf8($ministere->getVille())));

                $Country = $this->getDomDocument()->createElement('Country');
                $Country->setAttribute('listVersionID', 'second edition 2006');
                $CountryText = $this->getDomDocument()->createTextNode('FR');
                $Country->appendChild($CountryText);
                $address->appendChild($Country);

                $address->appendChild($this->textNode('Postcode', Atexo_Util::toUtf8($ministere->getCp())));
                $address->appendChild($this->textNode('StreetName', Atexo_Util::toUtf8($ministere->getAdresse())));
            } else {
                $address->appendChild($this->textNode('BuildingNumber', ''));
                $address->appendChild($this->textNode('CityName', Atexo_Util::toUtf8($service->getVille())));

                $Country = $this->getDomDocument()->createElement('Country');
                $Country->setAttribute('listVersionID', 'second edition 2006');
                $CountryText = $this->getDomDocument()->createTextNode('FR');
                $Country->appendChild($CountryText);
                $address->appendChild($Country);

                $address->appendChild($this->textNode('Postcode', Atexo_Util::toUtf8($service->getCp())));
                $address->appendChild($this->textNode('StreetName', Atexo_Util::toUtf8($service->getAdresse())));
            }
            $transferringAgency->appendChild($address);
        }
        // Ajout à Active Transfert
        $archiveTransfert->appendChild($transferringAgency);
    }

    /**
     * Permet d'ajouter les elements d'ArchivalAgency.
     *
     * @param DomNode $archiveTransfert,String $data les donnees
     *
     * @return void
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function addArchivalAgency(DomNode $archiveTransfert, $data)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));
        $serviceId = $this->getConsultation()->getServiceId();
        $c = new Criteria();
        $c->add(CommonServicePeer::ID, $serviceId);
        $c->add(CommonServicePeer::ORGANISME, $this->organismeAcronyme);
        $service = CommonServicePeer::doSelectOne($c, $connexion);
        if ($service && $service->getNomServiceArchiveur() && $service->getIdentifiantServiceArchiveur()) {
            $description = Atexo_Util::toUtf8($service->getNomServiceArchiveur());
            $identifiant = Atexo_Util::toUtf8($service->getIdentifiantServiceArchiveur());
        } else {
            $ministere = Atexo_Organismes::retrieveOrganismeByAcronyme($this->organismeAcronyme);
            $description = Atexo_Util::toUtf8(Prado::localize('TEXT_SERVICE_ARCHIVE')) . Atexo_Util::toUtf8($ministere->getDenominationorg());
            $identifiant = Atexo_Util::toUtf8($this->organismeAcronyme);
        }
        $archivalAgency = $this->getDomDocument()->createElement('ArchivalAgency');

        //BusinessType
        if ($data['BusinessType']) {
            self::creerBalise('BusinessType', $data['BusinessType']['value'], $data['BusinessType']['attributes'], $this->getDomDocument(), $archivalAgency);
        }

        // Description :
        if ($data['Description']) {
            self::creerBalise('Description', $data['Description'], null, $this->getDomDocument(), $archivalAgency);
        } else {
            $archivalAgency->appendChild($this->textNode('Description', $description));
        }

        // Identification :
        if ($data['Identification']) {
            self::creerBalise('Identification', self::formaterIdentifiant($data['Identification']['value']), $data['Identification']['attributes'], $this->getDomDocument(), $archivalAgency);
        } else {
            $archivalAgency->appendChild($this->textNode('Identification', $identifiant));
        }

        //LegalClassification
        if ($data['LegalClassification']) {
            self::creerBalise('LegalClassification', $data['LegalClassification'], null, $this->getDomDocument(), $archivalAgency);
        }

        //Name
        if ($data['Name']) {
            self::creerBalise('Name', $data['Name'], null, $this->getDomDocument(), $archivalAgency);
        }

        //Contact
        if ($data['Contact']) {
            self::createContact($data['Contact'], 'Contact', $archivalAgency);
        }

        //Address
        if ($data['Address']) {
            self::CreateAdress($data['Address'], 'Address', $archivalAgency, $this->getDomDocument());
        }

        // Ajout à Active Transfert
        $archiveTransfert->appendChild($archivalAgency);
    }

    /**
     * Permet d'ajouter les elements de Contains à l'archive XML.
     *
     * @param DomNode $archiveTransfert,String $data les donnees
     *
     * @return void
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function addContains(DomNode $archiveTransfert, $data, $lot = false)
    {
        $contains = $this->getDomDocument()->createElement('Contains');

        //ArchivalAgreement
        if ($data['ArchivalAgreement']) {
            self::creerBalise('ArchivalAgreement', $data['ArchivalAgreement']['Value'], $data['ArchivalAgreement']['Attributes'], $this->getDomDocument(), $contains);
        } else {
            $contains->appendChild($this->textNode('ArchivalAgreement', Atexo_Util::toUtf8(Prado::localize('ATEXO_LOCAL_TRUST_MPE') . ' - ' . Atexo_Config::getParameter('PF_SHORT_NAME') . '  ' . Prado::localize('ARCHIVES_INTERMEDIAIRES'))));
        }
        // ArchivalProfile :
        if ($data['ArchivalProfile']) {
            self::creerBalise('ArchivalProfile', $data['ArchivalProfile']['Value'], $data['ArchivalProfile']['Attributes'], $this->getDomDocument(), $contains);
        } else {
            $contains->appendChild($this->textNode('ArchivalProfile', Atexo_Util::toUtf8(Prado::localize('TEXT_MARCHES_PUBLICS') . Atexo_Config::getParameter('PF_SHORT_NAME'))));
        }

        // DescriptionLanguage :
        if ($data['DescriptionLanguage']) {
            self::creerBalise('DescriptionLanguage', $data['DescriptionLanguage']['Value'], $data['DescriptionLanguage']['Attributes'], $this->getDomDocument(), $contains);
        } else {
            $descripLang = $this->getDomDocument()->createElement('DescriptionLanguage');
            $descripLang->setAttribute('listVersionID', 'edition 2009');
            $descripLangText = $this->getDomDocument()->createTextNode('fr');
            $descripLang->appendChild($descripLangText);
            $contains->appendChild($descripLang);
        }

        // DescriptionLevel :
        if ($data['DescriptionLevel']) {
            self::creerBalise('DescriptionLevel', $data['DescriptionLevel']['Value'], $data['DescriptionLevel']['Attributes'], $this->getDomDocument(), $contains);
        } else {
            $descipLevel = $this->getDomDocument()->createElement('DescriptionLevel');
            $descipLevel->setAttribute('listVersionID', 'edition 2009');
            $descipLevelText = $this->getDomDocument()->createTextNode('recordgrp');
            $descipLevel->appendChild($descipLevelText);
            $contains->appendChild($descipLevel);
        }

        if ($data['Name']) {
            self::creerBalise('Name', $data['Name']['Value'], $data['Name']['Attributes'], $this->getDomDocument(), $contains);
        } else {
            if ('' != $this->getConsultation()->getIntitule()) {
                $name = Atexo_Util::toUtf8($this->getConsultation()->getIntitule());
            } else {
                $name = Atexo_Util::toUtf8($this->getConsultation()->getObjet());
            }

            if ('MEGALIS' == strtoupper(Atexo_Config::getParameter('PROFIL_ARCHIVE')) && $lot) {
                $name .= ' - ' . Prado::Localize('LOT') . ' ' . $lot;
            }
            // Name (description courte de la consulattion)
            $contains->appendChild($this->textNode('Name', $name));
        }

        // Description du contenu de l'archive
        $this->addContentDescription($contains, self::getArchiveSpecificConfig('ContentDescription'), $lot);

        //Ajout règle de sort final au niveau de l'archive
        $this->addSortFinalArchive($contains);

        //Ajout règle de communicabilité au niveau de l'archive
        $this->addCommunicabiliteArchive($contains);

        // Ajout de tous les fichiers contenus dans l'archive
        $arrayContains = self::getArchiveSpecificConfig('Contains');
        $this->addAllFiles($contains, $arrayContains['Contains']);

        $archiveTransfert->appendChild($contains);
    }

    /**
     * Permet d'ajouter les elements de ContentDescription à l'archive XML.
     *
     * @param DomNode $archiveTransfert,String $data les donnees
     *
     * @return void
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function addContentDescription(DomNode $contains, $data, $lot = false)
    {
        $contentDescription = $this->getDomDocument()->createElement('ContentDescription');

        // CustodialHistory :
        if ($data['CustodialHistory']) {
            self::creerBalise('CustodialHistory', $data['CustodialHistory'], null, $this->getDomDocument(), $contentDescription);
        } else {
            $contentDescription->appendChild($this->textNode('CustodialHistory', Atexo_Util::toUtf8(Prado::localize('TEXT_PIECES_MARCHES_ENREGISTREES_ATEXO')) . Atexo_Config::getParameter('PF_SHORT_NAME')));
        }

        // Description (Objet / Resume):
        if ($data['Description']) {
            self::creerBalise('Description', $data['Description']['Value'], $data['Description']['Attributes'], $this->getDomDocument(), $contentDescription);
        } else {
            $contentDescription->appendChild($this->textNode('Description', Atexo_Util::toUtf8($this->getConsultation()->getObjet())));
        }

        if ($data['Language']) {
            self::creerBalise('Language', $data['Language']['Value'], $data['Language']['Attributes'], $this->getDomDocument(), $contentDescription);
        } else {
            $language = $this->getDomDocument()->createElement('Language');
            $language->setAttribute('listVersionID', 'edition 2009');
            $languageText = $this->getDomDocument()->createTextNode('fr');
            $language->appendChild($languageText);
            $contentDescription->appendChild($language);
        }

        // LatestDate
        $LatestDate = $this->getDomDocument()->createElement('LatestDate');
        $LatestDateText = $this->getDomDocument()->createTextNode($this->lastestDate);
        $LatestDate->appendChild($LatestDateText);
        $contentDescription->appendChild($LatestDate);

        $OldestDateValue = $this->lastestDate;

        // OldestDate (date de mise en ligne calculée) :
        // TODO : DATE DE publication ?
        $dateMELC = Atexo_Util::toUtf8($this->getConsultation()->getDateMiseEnLigneCalcule());
        $date = explode(' ', $dateMELC);
        $dateMELC = $date[0];

        if ('' != $dateMELC && '0000-00-00' != $dateMELC) {
            $OldestDateValue = $dateMELC;
        } else {
            $dateValidation = Atexo_Util::toUtf8($this->getConsultation()->getDatevalidation());
            $date = explode(' ', $dateValidation);
            $dateValidation = $date[0];
            if ('' != $dateValidation && '0000-00-00' != $dateValidation) {
                $OldestDateValue = $dateValidation;
            } else {
                $dateDebut = Atexo_Util::toUtf8($this->getConsultation()->getDatedebut());
                if ('' != $dateDebut && '0000-00-00' != $dateDebut) {
                    $OldestDateValue = $dateDebut;
                } else {
                    $dateMEL = Atexo_Util::toUtf8($this->getConsultation()->getDatemiseenligne());
                    $date = explode(' ', $dateMEL);
                    $dateMEL = $date[0];
                    if ('' != $dateMEL && '0000-00-00' != $dateMEL) {
                        $OldestDateValue = $dateMEL;
                    }
                }
            }
        }

        $OldestDate = $this->getDomDocument()->createElement('OldestDate');
        $OldestDateText = $this->getDomDocument()->createTextNode(Atexo_Util::toUtf8(Atexo_Util::frnDate2iso($OldestDateValue)));
        $OldestDate->appendChild($OldestDateText);
        $contentDescription->appendChild($OldestDate);

        // OtherDescriptiveData (enrichissement de l'archive ) :
        $contentDescription->appendChild($this->textNode('OtherDescriptiveData', Atexo_Util::toUtf8($this->getConsultation()->getArchivemetadescription())));

        // Size (taille de l'archive non zippée sans cet Xml / rq : AD # octect) :
        $size = $this->getDomDocument()->createElement('Size');
        $size->setAttribute('unitCode', 'AD');
        $sizeText = $this->getDomDocument()->createTextNode(Atexo_Util::toUtf8($this->sizeArchive));
        $size->appendChild($sizeText);
        $contentDescription->appendChild($size);

        //OriginatingAgency
        if ($data['OriginatingAgency']) {
            $this->addOriginatingAgency($contentDescription, $this->getDomDocument(), $data['OriginatingAgency']);
        }

        //Content descriptive (collectivité locale, marché public, Numéro du marché saisi au moment de la notification)
        if ($data['ContentDescriptive']) {
            $this->addContentDescriptive($contentDescription, Atexo_Util::toUtf8(Prado::localize('DEFINE_COLLECTIVITE_LOCALE')), 'subject', '', Atexo_Util::toUtf8(Prado::localize('DEFINE_COLLECTIVITE_LOCALE')));
            $this->addContentDescriptive($contentDescription, Atexo_Util::toUtf8(Prado::localize('DEFINE_MARCHE_PUBLIC')), 'subject', '', Atexo_Util::toUtf8(Prado::localize('DEFINE_MARCHE_PUBLIC')));
            /*
            $numeroMarche = '';
            $c = new Criteria();
            $c->add(CommonDecisionEnveloppePeer::CONSULTATION_REF, $this->getConsultation()->getId());
            $c->add(CommonDecisionEnveloppePeer::ORGANISME, $this->getConsultation()->getOrganisme());
            $decisionsEnvs = CommonDecisionEnveloppePeer::doSelect($c, $this->getConnexion());
            if ($decisionsEnvs) {
                foreach($decisionsEnvs as $decisionsEnv) {
                    if ($decisionsEnv->getNumeroMarche()) {
                        $libelleNumeroMarche = Atexo_Util::toUtf8(Prado::localize('NUMERO_MARCHE'));
                        $numeroMarche = $decisionsEnv->getNumeroMarche();
                        if ($decisionsEnv->getLot()){
                            $libelleNumeroMarche .= " lot num ".$decisionsEnv->getLot();
                        }
                        $this->addContentDescriptive($contentDescription, Atexo_Util::toUtf8($numeroMarche), "subject",'',$libelleNumeroMarche);
                    }
                }
            }
            */
            $numeroMarche = '';
            $contratQuery = new CommonTContratTitulaireQuery();
            $contrats = $contratQuery->getListeContratsByRefCons($this->getConsultation()->getId(), $this->getConsultation()->getOrganisme());
            if ($contrats) {
                foreach ($contrats as $contrat) {
                    if ($contrat->getNumeroContrat()) {
                        $libelleNumeroMarche = Atexo_Util::toUtf8(Prado::localize('NUMERO_MARCHE'));
                        $numeroMarche = $contrat->getNumeroContrat();
                        if (is_array($contrat->getLots())) {
                            $lots = $contrat->getLots();
                            foreach ($lots as $oneLot) {
                                if ($oneLot) {
                                    $libelleNumeroMarche .= ' lot num ' . $oneLot;
                                }
                            }
                        }
                        $this->addContentDescriptive($contentDescription, Atexo_Util::toUtf8($numeroMarche), 'subject', '', $libelleNumeroMarche);
                    }
                }
            }
        }

        //info Consultation
        $referenceUtilisateur = $this->getConsultation()->getReferenceUtilisateur();
        if ('MEGALIS' == strtoupper(Atexo_Config::getParameter('PROFIL_ARCHIVE')) && $lot) {
            $referenceUtilisateur .= '-' . Prado::Localize('LOT') . $lot;
        }
        $this->addContentDescriptive($contentDescription, Atexo_Util::toUtf8($referenceUtilisateur), 'subject', Atexo_Util::toUtf8(Prado::localize('REFERENCE_CONSULTATION')), Atexo_Util::toUtf8(Prado::localize('REFERENCE_CONSULTATION')));

        // ----> Mots clefs (externes et internes) :
        // Lieux géolocalisation :
        $lieuxStr = $this->getConsultation()->getLieuExecution();
        $arrayIdsLieux = explode(',', $lieuxStr);
        $atexoLieux = Atexo_Geolocalisation_GeolocalisationN2::retrieveGeolocalisationN2byIds($arrayIdsLieux);
        if ($atexoLieux) {
            foreach ($atexoLieux as $lieu) {
                $res = [];
                mb_eregi("\((.*)\) (.*)$", Atexo_Util::toUtf8($lieu->getDenomination1()), $res);
                $this->addContentDescriptive($contentDescription, Atexo_Util::toUtf8($res[2]), 'geogname', 'Geolocalisation', Atexo_Util::toUtf8($res[1]), $data['ContentDescriptive']);
            }
        }

        // Type de procédures :
        $this->addContentDescriptive($contentDescription, Atexo_Util::toUtf8($this->getConsultation()->getLibelleTypeProcedureOrg()), 'function', '', Atexo_Util::toUtf8(Prado::localize('TEXT_TYPE_PROCEDURE')));

        // Categorie principale
        $this->addContentDescriptive($contentDescription, Atexo_Util::toUtf8($this->getConsultation()->getLibelleCategorieConsultation()), 'subject', '', Atexo_Util::toUtf8(Prado::localize('DEFINE_TYPE_MARCHE')));

        // ref aux marchés publics
        // TODO ref dans priam ?

        // Mots clef de l'utilisateur :
        $motsClef = Atexo_Util::toUtf8($this->getConsultation()->getArchivemetamotsclef());
        if ($motsClef) {
            $arrayMotsClef = explode(',', $motsClef);
            foreach ($arrayMotsClef as $motClef) {
                $this->addContentDescriptive($contentDescription, trim(Atexo_Util::toUtf8($motClef)), 'subject', '', Atexo_Util::toUtf8(Prado::localize('TEXT_ENRICHISSEMENT_UTILISATEUR')));
            }
        }

        // Mot-matière issu de la liste "Vocabulaire commun pour les marchés publics" (Common Procurement Vocabulary, CPV)
        $cpv1 = $this->getConsultation()->getCodeCpv1();
        if ($cpv1) {
            $this->addContentDescriptive($contentDescription, trim(Atexo_Util::toUtf8($cpv1)), 'subject', '', Atexo_Util::toUtf8(Prado::localize('DEFINE_CODES_CPV_CONSULTATION')));
        }
        $arrayCpv2 = explode('#', $this->getConsultation()->getCodeCpv2());
        if (is_array($arrayCpv2) && count($arrayCpv2)) {
            foreach ($arrayCpv2 as $cpv) {
                if ($cpv) {
                    $this->addContentDescriptive($contentDescription, trim(Atexo_Util::toUtf8($cpv)), 'subject', '', Atexo_Util::toUtf8(Prado::localize('DEFINE_CODES_CPV_CONSULTATION')));
                }
            }
        }

        // Mot-matière issu de la liste "Vocabulaire commun pour les marchés publics" (Common Procurement Vocabulary, CPV)
        $lots = $this->getConsultation()->getAllLots();
        foreach ($lots as $lot) {
            $libelleCPV = Atexo_Util::toUtf8(Prado::localize('TEXT_CODE_CPV')) . '  lot num ' . $lot->getLot();

            $cpv1 = $lot->getCodeCpv1();
            if ($cpv1) {
                $this->addContentDescriptive($contentDescription, trim(Atexo_Util::toUtf8($cpv1)), 'subject', '', $libelleCPV);
            }
            $arrayCpv2 = explode('#', $lot->getCodeCpv2());
            if (is_array($arrayCpv2) && count($arrayCpv2)) {
                foreach ($arrayCpv2 as $cpv) {
                    if ($cpv) {
                        $this->addContentDescriptive($contentDescription, trim(Atexo_Util::toUtf8($cpv)), 'subject', '', $libelleCPV);
                    }
                }
            }
        }

        $contains->appendChild($contentDescription);
    }

    /**
     * Permet d'ajouter les elements de ContentDescriptive à l'archive XML.
     *
     * @param DomNode $archiveTransfert
     * @param string  $content             les donnees
     * @param string  $typeText            le type de text
     * @param string  $referenceSchemeName le nom du shema de la reference
     * @param string  $referenceText       la reference du text
     * @param string  $data                les donnees
     *
     * @return void
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function addContentDescriptive(DomNode $contentDescription, $content, $typeText, $referenceSchemeName, $referenceText, $data = null)
    {
        $ContentDescriptive = $this->getDomDocument()->createElement('ContentDescriptive');

        $KeywordContent = $this->getDomDocument()->createElement('KeywordContent');
        $KeywordContentText = $this->getDomDocument()->createTextNode($content);
        $KeywordContent->appendChild($KeywordContentText);
        $ContentDescriptive->appendChild($KeywordContent);

        $KeywordReference = $this->getDomDocument()->createElement('KeywordReference');
        if ($data) {
            $KeywordReference->setAttribute('schemeName', Atexo_Util::toUtf8('Geolocalisation'));
            $KeywordReference->setAttribute('schemeAgencyName', Atexo_Util::toUtf8('INSEE'));
        } else {
            $KeywordReference->setAttribute('schemeName', Atexo_Util::toUtf8('ATEXO'));
        }
        $KeywordReferenceText = $this->getDomDocument()->createTextNode(Atexo_Util::toUtf8($referenceText));
        $KeywordReference->appendChild($KeywordReferenceText);
        $ContentDescriptive->appendChild($KeywordReference);

        $KeywordType = $this->getDomDocument()->createElement('KeywordType');
        $KeywordType->setAttribute('listVersionID', 'edition 2009');
        $KeywordTypeText = $this->getDomDocument()->createTextNode(Atexo_Util::toUtf8($typeText));
        $KeywordType->appendChild($KeywordTypeText);
        $ContentDescriptive->appendChild($KeywordType);

        $contentDescription->appendChild($ContentDescriptive);
    }

    /**
     * Permet d'ajouter des donnees mainContains.
     *
     * @param string $data les donnees
     *
     * @return void
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function addAllFiles(DomNode $mainContains, $data = null)
    {
        $increment = 0;
        foreach ($this->getDocTree()->getFils() as $directory) {
            $this->codeAccesRestriction = Atexo_Util::toUtf8($directory->getAccess());
            $this->generateContains($mainContains, $directory, $data, $increment);
            ++$increment;
        }
    }

    /**
     * Permet d'ajouter des donnees mainContains.
     *
     * @param DomNode $parentNode
     * @param string  $data       les donnees
     * @param int     $increment  l'increment
     * @param int     $duration
     *
     * @return void
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function generateContains($parentNode, Atexo_Consultation_Tree_Item $item, $data = null, $increment = null, $duration = null)
    {
        $offres = (new Atexo_Interfaces_Offre())->getOffreByRefConsultation($this->getConsultation()->getId(), $this->getConsultation()->getOrganisme());
        $dateDepot = '';
        if ($offres[0] instanceof CommonOffres) {
            $dateDepot = $offres[0]->getUntrusteddate();
        }
        //si pas d'offres rattachées à la consultation alors la date de depôt est celle de la date de fin
        else {
            $dateDepot = Atexo_Util::toUtf8($this->getConsultation()->getDatefin());
        }
        $this->dateDepot = substr($dateDepot, 0, 10);
        //Tableau des autres propriétés $item
        $arrayAutresProprietes = $item->getAutresProprietes();

        // Cas d'un dossier
        if ($item instanceof Atexo_Consultation_Tree_Directory) {
            $contains = $this->getDomDocument()->createElement('Contains');

            // DescriptionLevel
            $arrayDescriptionLevel = $arrayAutresProprietes['Groups'][(new Atexo_Consultation_Archive())->getType($item->getNom())]['DescriptionLevel'];
            if (is_array($arrayDescriptionLevel)) {
                self::creerBalise('DescriptionLevel', $arrayDescriptionLevel['Value'], $arrayDescriptionLevel['Attributes'], $this->getDomDocument(), $contains);
            } else {
                $DescriptionLevel = $this->getDomDocument()->createElement('DescriptionLevel');
                $DescriptionLevel->setAttribute('listVersionID', 'edition 2009');
                $DescriptionLevelText = $this->getDomDocument()->createTextNode('file');
                $DescriptionLevel->appendChild($DescriptionLevelText);
                $contains->appendChild($DescriptionLevel);
            }

            // Nom du dossier
            if (Atexo_Util::toUtf8($arrayAutresProprietes['Name'][(new Atexo_Consultation_Archive())->getType($item->getNom())])) {
                $textName = Atexo_Util::toUtf8($arrayAutresProprietes['Name'][(new Atexo_Consultation_Archive())->getType($item->getNom())]);
                $contains->appendChild($this->textNode('Name', Atexo_Util::toUtf8($textName)));
            } else {
                $contains->appendChild($this->textNode('Name', Atexo_Util::toUtf8($item->getNom())));
            }

            //$ContentDescription
            $ContentDescription = $this->getDomDocument()->createElement('ContentDescription');
            // Description
            $textDescription = Atexo_Util::toUtf8($item->getDescription());
            $ContentDescription->appendChild($this->textNode('Description', $textDescription));

            // Language
            $Language = $this->getDomDocument()->createElement('Language');
            $Language->setAttribute('listVersionID', 'edition 2009');
            $LanguageText = $this->getDomDocument()->createTextNode('fr');
            $Language->appendChild($LanguageText);
            $ContentDescription->appendChild($Language);

            // Ajout du ContentDescription
            $contains->appendChild($ContentDescription);

            // Appraisal
            $Appraisal = $this->getDomDocument()->createElement('Appraisal');
            $Code = $this->getDomDocument()->createElement('Code');
            $Code->setAttribute('listVersionID', 'edition 2009');
            $CodeText = $this->getDomDocument()->createTextNode($item->getAppraisal());
            $Code->appendChild($CodeText);
            $Appraisal->appendChild($Code);

            $contains->appendChild($Appraisal);

            // AccessRestriction :
            $AccessRestriction = $this->getDomDocument()->createElement('AccessRestriction');
            $Code = $this->getDomDocument()->createElement('Code');
            $Code->setAttribute('listVersionID', 'edition 2009');
            if ($this->defineAccesRestriction) {
                $CodeText = $this->getDomDocument()->createTextNode($this->codeAccesRestriction);
            } else {
                $CodeText = $this->getDomDocument()->createTextNode(Atexo_Util::toUtf8($item->getAccess()));
            }
            $Code->appendChild($CodeText);
            $AccessRestriction->appendChild($Code);
            if ($this->lastestDate) {
                $StartDate = $this->getDomDocument()->createElement('StartDate');
                $StartDateText = $this->getDomDocument()->createTextNode($this->lastestDate);
                $StartDate->appendChild($StartDateText);
                $AccessRestriction->appendChild($StartDate);
            }
            if ($this->defineAccesRestriction) {
                $contains->appendChild($AccessRestriction);
            } else {
                $ContentDescription->appendChild($AccessRestriction);
            }

            //duration
            $textDuration = '';
            $aca = (new Atexo_Consultation_Archive())->getType($item->getNom());
            if (
                $duration || (
                    is_array($arrayAutresProprietes)
                    && array_key_exists('Duration', $arrayAutresProprietes)
                    && array_key_exists($aca, $arrayAutresProprietes['Duration'])
                )
            ) {
                $textDuration = $duration ?: $arrayAutresProprietes['Duration'][(new Atexo_Consultation_Archive())->getType($item->getNom())];
            } else {
                $textDuration = 'P6Y';
            }
            $duration = $this->getDomDocument()->createElement('Duration');
            $durationText = $this->getDomDocument()->createTextNode($textDuration); // TODO bon ?
            $duration->appendChild($durationText);
            $Appraisal->appendChild($duration);

            if ($this->lastestDate) {
                $StartDate = $this->getDomDocument()->createElement('StartDate');
                $StartDateText = $this->getDomDocument()->createTextNode($this->lastestDate);
                $StartDate->appendChild($StartDateText);
                $Appraisal->appendChild($StartDate);
            }

            //Document
            if (0 == $increment && $data[$increment]['Document']) {
                $Document = $this->getDomDocument()->createElement('Document');
                $Attachment = $this->getDomDocument()->createElement('Attachment');
                $Attachment->setAttribute('filename', self::formaterPath(Atexo_Util::toUtf8($item->getPath() . '/' . $item->getNom())));
                //Creation
                $Creation = $this->getDomDocument()->createElement('Creation');
                $CreationText = $this->getDomDocument()->createTextNode($this->dateDepot . 'T00:00:00Z');
                $Creation->appendChild($CreationText);
                //Description
                $Description = $this->getDomDocument()->createElement('Description');
                $DescriptionText = $this->getDomDocument()->createTextNode($data[$increment]['Document']['Description']);
                $Description->appendChild($DescriptionText);
                //Copy
                /*$Copy = $this->getDomDocument()->createElement("Copy");
                $CopyText = $this->getDomDocument()->createTextNode('');
                $Copy->appendChild($CopyText);*/
                //Type
                $Type = $this->getDomDocument()->createElement('Type');
                $Type->setAttribute('listVersionID', 'edition 2009');
                $TypeText = $this->getDomDocument()->createTextNode('CDO');
                $Type->appendChild($TypeText);

                $Document->appendChild($Attachment);
                $Document->appendChild($Description);
                $Document->appendChild($Creation);
                //$Document->appendChild($Copy);
                $Document->appendChild($Type);

                $contains->appendChild($Document);
            }

            foreach ($item->getFils() as $itemfils) {
                $this->generateContains($contains, $itemfils, null, null, $textDuration);
            }
            $parentNode->appendChild($contains);
        } elseif ($item instanceof Atexo_Consultation_Tree_Group) {
            $contains = $this->getDomDocument()->createElement('Contains');

            //Les données autres propriétés
            $autresProprietes = $item->getAutresProprietes();
            // DescriptionLevel
            if (
                is_array($autresProprietes)
                && array_key_exists('DescriptionLevel', $autresProprietes)
                && is_array($autresProprietes['DescriptionLevel'])
                && array_key_exists('Value', $autresProprietes['DescriptionLevel'])
                && array_key_exists('Attributes', $autresProprietes['DescriptionLevel'])
            ) {
                self::creerBalise(
                    'DescriptionLevel',
                    $autresProprietes['DescriptionLevel']['Value'],
                    $autresProprietes['DescriptionLevel']['Attributes'],
                    $this->getDomDocument(),
                    $contains
                );
            } else {
                $DescriptionLevel = $this->getDomDocument()->createElement('DescriptionLevel');
                $DescriptionLevel->setAttribute('listVersionID', 'edition 2009');
                $DescriptionLevelText = $this->getDomDocument()->createTextNode('item');
                $DescriptionLevel->appendChild($DescriptionLevelText);
                $contains->appendChild($DescriptionLevel);
            }

            // Nom de l'item
            $nameValue = '';
            if (self::getValue($autresProprietes, 'Name')) {
                $nameValue = self::getValue($autresProprietes, 'Name');
            } else {
                $nameValue = $item->getNom();
            }
            $contains->appendChild($this->textNode('Name', Atexo_Util::toUtf8($nameValue)));

            //$ContentDescription
            $ContentDescription = $this->getDomDocument()->createElement('ContentDescription');
            // Description
            $valueDesc = '';
            if (is_array($autresProprietes)
                && array_key_exists('ContentDescription', $autresProprietes)
                && self::getValue($autresProprietes['ContentDescription'], 'Description')) {
                $valueDesc = self::getValue($autresProprietes['ContentDescription'], 'Description');
            } else {
                $valueDesc = $item->getDescription();
            }
            $ContentDescription->appendChild($this->textNode('Description', Atexo_Util::toUtf8($valueDesc)));

            // Language
            $Language = $this->getDomDocument()->createElement('Language');
            $Language->setAttribute('listVersionID', 'edition 2009');
            $LanguageText = $this->getDomDocument()->createTextNode('fr');
            $Language->appendChild($LanguageText);
            $ContentDescription->appendChild($Language);

            $contains->appendChild($ContentDescription);

            // AccessRestriction :
            $AccessRestriction = $this->getDomDocument()->createElement('AccessRestriction');
            $Code = $this->getDomDocument()->createElement('Code');
            $Code->setAttribute('listVersionID', 'edition 2009');
            if ($this->defineAccesRestriction) {
                $CodeText = $this->getDomDocument()->createTextNode($this->codeAccesRestriction);
            } else {
                $CodeText = $this->getDomDocument()->createTextNode(Atexo_Util::toUtf8($item->getAccess()));
            }
            $Code->appendChild($CodeText);
            $AccessRestriction->appendChild($Code);
            if ($this->lastestDate) {
                $StartDate = $this->getDomDocument()->createElement('StartDate');
                $StartDateText = $this->getDomDocument()->createTextNode($this->lastestDate);
                $StartDate->appendChild($StartDateText);
                $AccessRestriction->appendChild($StartDate);
            }
            if ($this->defineAccesRestriction) {
                $contains->appendChild($AccessRestriction);
            } else {
                $ContentDescription->appendChild($AccessRestriction);
            }

            foreach ($item->getFils() as $itemfils) {
                $this->generateContains($contains, $itemfils);
            }
            $parentNode->appendChild($contains);
        } else {
            // Si c'est un item d'un group :
            // Document
            if ($item instanceof Atexo_Consultation_Tree_File && !$item->getIdBlob()) {
                return;
            }
            //Les données autres propriétés
            $autresProprietes = $item->getAutresProprietes();

            $Document = $this->getDomDocument()->createElement('Document');
            $Attachment = $this->getDomDocument()->createElement('Attachment');
            $Attachment->setAttribute('filename', self::formaterPath(Atexo_Util::toUtf8($item->getPath() . '/' . $item->getNom())));
            $Attachment->setAttribute('mimeCode', Atexo_Util::mime_content_type($item->getNom()));

            $Description = $this->getDomDocument()->createElement('Description');
            $valDesc = '';
            if (self::getValue($autresProprietes, 'Description')) {
                $valDesc = self::getValue($autresProprietes, 'Description');
            } else {
                $valDesc = $item->getDescription();
            }
            $DescriptionText = $this->getDomDocument()->createTextNode(Atexo_Util::toUtf8($valDesc));
            $Description->appendChild($DescriptionText);

            $Type = $this->getDomDocument()->createElement('Type');
            $Type->setAttribute('listVersionID', 'edition 2009');
            $TypeText = $this->getDomDocument()->createTextNode('CDO');
            $Type->appendChild($TypeText);

            //Creation
            $Creation = $this->getDomDocument()->createElement('Creation');
            $CreationText = $this->getDomDocument()->createTextNode($this->dateDepot . 'T00:00:00Z');
            $Creation->appendChild($CreationText);

            $Document->appendChild($Attachment);
            $Document->appendChild($Creation);
            $Document->appendChild($Description);
            $Document->appendChild($Type);

            $parentNode->appendChild($Document);
        }
    }

    /**
     * Permet de retourner la valeur d'un élément dans le fichier archive.xml.
     *
     * @param DomNode $parentNode
     * @param string  $key        la clé de la recherche
     *
     * @return Mixted Boolean si on fait l'appel par clé, si non le tableau d'archiev xml
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public static function getArchiveSpecificConfig($key = null)
    {
        if (is_file(Atexo_Controller_Front::getRootPath() . Atexo_Config::getParameter('FILE_PATH_ARCHIVE_CONFIG'))) {
            $arrayConfig = Atexo_Util::xmlFileToArray(Atexo_Controller_Front::getRootPath() . Atexo_Config::getParameter('FILE_PATH_ARCHIVE_CONFIG'));
            if (!$key) {
                return $arrayConfig;
            }
            if (is_array($arrayConfig) && count($arrayConfig)) {
                return Atexo_Util::returnArrayKeyExistsRecursive($key, $arrayConfig);
            }

            return false;
        } else {
            return false;
        }
    }

    /**
     * Permet de créer une balise.
     *
     * @param string $balise          le nom de la balise que l'on veut créer
     * @param string $values          la valeur de la balise
     * @param array  $arrayAttributes le tableau des attributs
     * @param string $document
     * @param string $bloc
     *
     * @return void
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function creerBalise($balise, $values, $arrayAttributes, $document, $bloc)
    {
        $ligneBalise = $document->createElement($balise);
        $values = $values ?: '';
        if (count($arrayAttributes)) {
            $ligneValueAttr = $document->createTextNode($values);
            foreach ($arrayAttributes as $keyAttribute => $valueAttribute) {
                $ligneBalise->setAttribute($keyAttribute, $valueAttribute);
                $ligneBalise->appendChild($ligneValueAttr);
            }
        } else {
            $values = $document->createTextNode(Atexo_Util::toUtf8($values));
            $ligneBalise->appendChild($values);
        }
        $bloc->appendChild($ligneBalise);
    }

    public function appendChild($balise, $value, $element)
    {
        $element->appendChild($this->textNode($balise, Atexo_Util::toUtf8($value)));
    }

    /**
     * Permet de créer le bloc "Contact".
     *
     * @param array  $data
     * @param string $nameBloc
     * @param string $ligneBloc
     *
     * @return void
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function createContact($data, $nameBloc, $ligneBloc)
    {
        $ligneBalise = $this->getDomDocument()->createElement($nameBloc);
        //DepartmentName
        if ($data['DepartmentName']) {
            self::appendChild('DepartmentName', Atexo_Util::toUtf8($data['DepartmentName']), $ligneBalise);
        }
        //PersonName
        if ($data['PersonName']) {
            self::appendChild('PersonName', Atexo_Util::toUtf8($data['PersonName']), $ligneBalise);
        }
        //Responsibility
        if ($data['Responsibility']) {
            self::appendChild('Responsibility', Atexo_Util::toUtf8($data['Responsibility']), $ligneBalise);
        }
        // Communication
        if (is_countable($data['Communication']) ? count($data['Communication']) : 0) {
            $valCom = null;
            foreach ($data['Communication'] as $keyCom => $valCom) {
                if (is_array($data['Communication'][$keyCom]) && count($data['Communication'][$keyCom])) {
                    $communication = $this->getDomDocument()->createElement('Communication');
                    foreach ($data['Communication'][$keyCom] as $key => $val) {//echo '<br/>'.$key.'#'.$val.'<br/>';
                        self::appendChild($key, Atexo_Util::toUtf8($val), $communication);
                    }
                    $ligneBalise->appendChild($communication);
                }
            }
        }
        $ligneBloc->appendChild($ligneBalise);
    }

    /**
     * Permet de construire le bloc "Adress".
     *
     * @param array  $data
     * @param string $nameBloc
     * @param string $ligneBloc
     * @param string $document
     *
     * @return void
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function CreateAdress($data, $nameBloc, $ligneBloc, $document)
    {
        $ligneBalise = $document->createElement($nameBloc);
        //BuildingNumber
        if ($data['BuildingNumber']) {
            self::appendChild('BuildingNumber', Atexo_Util::toUtf8($data['BuildingNumber']), $ligneBalise);
        }
        //CityName
        if ($data['CityName']) {
            self::appendChild('CityName', Atexo_Util::toUtf8($data['CityName']), $ligneBalise);
        }
        //Country
        if ($data['Country']) {
            self::creerBalise('Country', $data['Country']['Value'], $data['Country']['Attributes'], $this->getDomDocument(), $ligneBalise);
        }
        //Postcode
        if ($data['Postcode']) {
            self::appendChild('Postcode', Atexo_Util::toUtf8($data['Postcode']), $ligneBalise);
        }
        //StreetName
        if ($data['StreetName']) {
            self::appendChild('StreetName', Atexo_Util::toUtf8($data['StreetName']), $ligneBalise);
        }
        $ligneBloc->appendChild($ligneBalise);
    }

    /**
     * Permet de construire le bloc "OriginatingAgency".
     *
     * @param string $document
     * @param array  $data
     *
     * @return void
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function addOriginatingAgency(DomNode $contentDescription, $document, $data)
    {
        $OriginatingAgency = $document->createElement('OriginatingAgency');
        //BusinessType
        if ($data['BusinessType']) {
            self::creerBalise('BusinessType', $data['BusinessType']['Value'], $data['BusinessType']['Attributes'], $this->getDomDocument(), $OriginatingAgency);
        }
        //Description
        if ($data['Description']) {
            self::creerBalise('Description', $data['Description'], null, $this->getDomDocument(), $OriginatingAgency);
        }
        //Identification
        if ($data['Identification']) {
            $organisme = Atexo_Organismes::retrieveOrganismeByAcronyme($this->organismeAcronyme);
            $sirenTrig = '';
            if ($organisme instanceof CommonOrganisme) {
                $sirenTrig = $organisme->getSiren() ?: $organisme->getAcronyme();
            }
            $identification = $sirenTrig . ($this->getConsultation()->getServiceId() ? ('_' . $this->getConsultation()->getServiceId()) : '');
            if ('MEGALIS' != strtoupper(Atexo_Config::getParameter('PROFIL_ARCHIVE'))) {
                $identification .= '_MP';
            }
            self::creerBalise('Identification', $identification, $data['Identification']['Attributes'], $this->getDomDocument(), $OriginatingAgency);
        }
        //LegalClassification
        if ($data['LegalClassification']) {
            self::creerBalise('LegalClassification', $data['LegalClassification'], null, $this->getDomDocument(), $OriginatingAgency);
        }
        $listeServiceWithPath = Atexo_EntityPurchase::retrieveEntityPurchaseByOrganisme($this->organismeAcronyme, true);
        if (is_array($listeServiceWithPath) && $listeServiceWithPath[$this->getConsultation()->getServiceId()]) {
            self::creerBalise('Name', $listeServiceWithPath[$this->getConsultation()->getServiceId()], null, $this->getDomDocument(), $OriginatingAgency);
        }
        $contentDescription->appendChild($OriginatingAgency);
        //Contact et Communication
        if ($data['Contact']) {
            $nomService = Atexo_EntityPurchase::getPathEntityById($this->getConsultation()->getServiceId(), $this->organismeAcronyme);
            $agentConnecte = (new Atexo_Agent())->retrieveAgent($this->getConsultation()->getIdCreateur());
            $nomPrenomAgent = '';
            $statutAgent = '';
            $telAgent = '';
            $courrielAgent = '';
            if ($agentConnecte) {
                $nomPrenomAgent = $agentConnecte->getNom() . ' ' . $agentConnecte->getPrenom();
                $statutAgent = ('1' == $agentConnecte->getElu()) ? 'Élu' : 'Agent';
                $telAgent = $agentConnecte->getNumTel();
                $courrielAgent = $agentConnecte->getEmail();
            }
            //Contact
            $contact = $this->getDomDocument()->createElement('Contact');
            $contact->appendChild($this->textNode('DepartmentName', Atexo_Util::toUtf8($nomService)));
            $contact->appendChild($this->textNode('PersonName', Atexo_Util::toUtf8($nomPrenomAgent)));
            $contact->appendChild($this->textNode('Responsibility', Atexo_Util::toUtf8($statutAgent)));
            // Communication
            $communication = $this->getDomDocument()->createElement('Communication');
            $communication->appendChild($this->textNode('Channel', Atexo_Util::toUtf8('téléphone')));
            $communication->appendChild($this->textNode('CompleteNumber', Atexo_Util::toUtf8($telAgent)));
            $contact->appendChild($communication);
            $communication = $this->getDomDocument()->createElement('Communication');
            $communication->appendChild($this->textNode('Channel', Atexo_Util::toUtf8('courriel')));
            $communication->appendChild($this->textNode('URI', Atexo_Util::toUtf8($courrielAgent)));
            $contact->appendChild($communication);
            $OriginatingAgency->appendChild($contact);
        }
        //Address
        $service = Atexo_EntityPurchase::retrieveEntityById($this->getConsultation()->getServiceId(), $this->getConsultation()->getOrganisme());
        if ($service instanceof CommonService) {
            $this->setService($service);
        }
        $address = $this->getDomDocument()->createElement('Address');
        if (self::getValue($data['Address'], 'BuildingNumber')) {
            self::creerBalise('BuildingNumber', $data['Address']['BuildingNumber'], null, $this->getDomDocument(), $address);
        } else {
            if ($this->getService() instanceof CommonService) {
                self::creerBalise('BuildingNumber', '', null, $this->getDomDocument(), $address);
            }
        }
        if (self::getValue($data['Address'], 'CityName')) {
            self::creerBalise('CityName', $data['Address']['CityName'], null, $this->getDomDocument(), $address);
        } else {
            if ($this->getService() instanceof CommonService) {
                self::creerBalise('CityName', $this->getService()->getVille(), null, $this->getDomDocument(), $address);
            }
        }
        if (is_array($data['Address']['Country'])) {
            self::creerBalise('Country', $data['Address']['Country']['Value'], $data['Address']['Country']['Attributes'], $this->getDomDocument(), $address);
        } else {
            $Country = $this->getDomDocument()->createElement('Country');
            $Country->setAttribute('listVersionID', 'second edition 2006');
            $CountryText = $this->getDomDocument()->createTextNode('FR');
            $Country->appendChild($CountryText);
            $address->appendChild($Country);
        }
        if (self::getValue($data['Address'], 'Postcode')) {
            self::creerBalise('Postcode', $data['Address']['Postcode'], null, $this->getDomDocument(), $address);
        } elseif ($this->getService() instanceof CommonService) {
            self::creerBalise('Postcode', $this->getService()->getCp(), null, $this->getDomDocument(), $address);
        }
        if (self::getValue($data['Address'], 'StreetName')) {
            self::creerBalise('StreetName', $data['Address']['StreetName'], null, $this->getDomDocument(), $address);
        } elseif ($this->getService() instanceof CommonService) {
            self::creerBalise('StreetName', $this->getService()->getAdresse(), null, $this->getDomDocument(), $address);
        }
        $OriginatingAgency->appendChild($address);
    }

    /**
     * Permet de retourner la valeur "$value" contenue dans le "$arrayValues".
     *
     * @param array   $arrayValues
     * @param varchar $value
     *
     * @return Mixted String si la valeur existe si non boolean false
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function getValue($arrayValues, $value)
    {
        if (
            is_array($arrayValues)
            && !empty($arrayValues)
            && !is_array($arrayValues[$value])
            && $arrayValues[$value]
        ) {
            return $arrayValues[$value];
        }

        return false;
    }

    /**
     * Permet d'attribuer l'objet service.
     *
     * @return void
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function setService(CommonService $service)
    {
        $this->service = $service;
    }

    /**
     * Permet de recupérer l'objet service.
     *
     * @return CommonService le service
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function getService()
    {
        return $this->service;
    }

    /**
     * permet de calculer la date latestDate qui sera utilisée dans toute l'application
     *  si consultation est non allotie :
     *      - il s'agit de la date de notification la plus récente parmi tous les attributaires
     *      - si la date de notification n'est pas renseignée, il s'agit de la date décision
     *      - si la date de décision n'est pas renseignée : il s'agit de la date de fin de la consultation + 6 mois
     *  si consultation est allotie :
     *      - Il y a une boucle sur la date de notification des lots. La date est égale à celle du lot ayant une date de notification renseignée ou si présence de plusieurs dates, égale à la date la plus récente.
     *      - si la date de notification n'est pas renseignée. La date est égale à la date de décision du lot ayant une date de décision renseignée ou si présence de plusieurs dates, égale à la date la plus récente.
     *      - si la date de décision n'est renseignée pour aucun des lots alors il s'agit de la date de fin de la consultation + 6 mois.
     *
     * @return Date la date calculee
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function calculLatestDate()
    {
        //calcul de la date de fin de la consultation + 6 mois
        //date de fin au format suivant '2012-10-25 17:30'
        //premier explode retourne '2012-10-25' et '17:30'
        //second explode retoure '2012', '10' et '25'
        $dateSansHeure = explode(' ', Atexo_Util::toUtf8($this->getConsultation()->getDatefin()));
        $date = explode('-', $dateSansHeure[0]);
        $dateFinConsultationPlus6mois = date('Y-m-d', mktime(00, 00, 00, $date[1] + 6, $date[2], $date[0]));

        //calcul de la date de décision la plus récente (si plusieurs lots)
        //date du debut informatique
        $c = new Criteria();
        $c->add(CommonDecisionLotPeer::CONSULTATION_ID, $this->getConsultation()->getId());
        $c->add(CommonDecisionLotPeer::ORGANISME, $this->getConsultation()->getOrganisme());
        $decisionsLots = CommonDecisionLotPeer::doSelect($c, $this->getConnexion());
        $dateDecision = '0000-00-00';
        $premier = true;
        if ($decisionsLots) {
            foreach ($decisionsLots as $decisionLot) {
                //boucle sur toutes les décisions, et on prend la plus récente dans le cas de plusieurs attributaires
                if ($decisionLot->getDateDecision('Y-m-d')) {
                    if (!$premier) {
                        if (new DateTime($decisionLot->getDateDecision('Y-m-d')) > new DateTime($dateDecision)) {
                            $dateDecision = $decisionLot->getDateDecision('Y-m-d');
                        }
                    } else {
                        $premier = false;
                        $dateDecision = $decisionLot->getDateDecision('Y-m-d');
                    }
                }
            }
        }

        //calcul de la date de notification la plus récente (si plusieurs attributaires et plusieurs lots)
        //les informations sont à récupérer dans la table decisionEnveloppe
        /*
        $c = new Criteria();
        $c->add(CommonDecisionEnveloppePeer::CONSULTATION_REF, $this->getConsultation()->getId());
        $c->add(CommonDecisionEnveloppePeer::ORGANISME, $this->getConsultation()->getOrganisme());
        $decisionsEnveloppes = CommonDecisionEnveloppePeer::doSelect($c, $this->getConnexion());
        $dateNotification = "0000-00-00";
        $premier = true;
        if ($decisionsEnveloppes) {
            foreach($decisionsEnveloppes as $decisionEnveloppe) {
                //boucle sur toutes les attributions, et on prend la date de notification la plus récente dans le cas de plusieurs attributaires et plusieurs lots
                if ($decisionEnveloppe->getDateNotification()) {
                    if(!$premier) {
                        if(new DateTime($decisionEnveloppe->getDateNotification()) > new DateTime($dateNotification)) {
                            $dateNotification = $decisionEnveloppe->getDateNotification();
                        }
                    }
                    else {
                        $premier = false;
                        $dateNotification = $decisionEnveloppe->getDateNotification();
                    }
                }
            }
        }
        */
        $contratQuery = new CommonTContratTitulaireQuery();
        $contrats = $contratQuery->getListeContratsByRefCons($this->getConsultation()->getId(), $this->getConsultation()->getOrganisme());
        $dateNotification = '0000-00-00';
        $premier = true;
        if ($contrats) {
            foreach ($contrats as $contrat) {
                //boucle sur toutes les attributions, et on prend la date de notification la plus récente dans le cas de plusieurs attributaires et plusieurs lots
                if ($contrat->getDateNotification()) {
                    if (!$premier) {
                        if (new DateTime($contrat->getDateNotification('Y-m-d')) > new DateTime($dateNotification)) {
                            $dateNotification = $contrat->getDateNotification('Y-m-d');
                        }
                    } else {
                        $premier = false;
                        $dateNotification = $contrat->getDateNotification('Y-m-d');
                    }
                }
            }
        }

        if ('' != $dateNotification && '0000-00-00' != $dateNotification) {
            return Atexo_Util::toUtf8($dateNotification);
        } elseif ('' != $dateDecision && '0000-00-00' != $dateDecision) {
            return Atexo_Util::toUtf8($dateDecision);
        } else {
            return Atexo_Util::toUtf8($dateFinConsultationPlus6mois);
        }
    }

    /**
     * permet de construire le bloc XML relatif à la communicabilité globale de l'archive.
     *
     * @return void
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function addCommunicabiliteArchive(DomNode $contains)
    {
        $AccessRestriction = $this->getDomDocument()->createElement('AccessRestriction');
        $Code = $this->getDomDocument()->createElement('Code');
        $Code->setAttribute('listVersionID', 'edition 2009');
        $CodeText = $this->getDomDocument()->createTextNode(Atexo_Config::getParameter('ACCESS_RESTRICTION_ARCHIVE_GLOBALE'));
        $Code->appendChild($CodeText);
        $AccessRestriction->appendChild($Code);

        if ($this->lastestDate) {
            $StartDate = $this->getDomDocument()->createElement('StartDate');
            $StartDateText = $this->getDomDocument()->createTextNode($this->lastestDate);
            $StartDate->appendChild($StartDateText);
            $AccessRestriction->appendChild($StartDate);
        }

        $contains->appendChild($AccessRestriction);
    }

    /**
     * permet de construire le bloc XML relatif au sort final de l'archive.
     *
     * <Appraisal>
     * Les règles sont les suivantes :
     *  - si offres retenues électronique : sort final = conserver, DUA = 15 ans, P15Y
     *  - sans offres retenues électroniques (réponse retenue = papier) : sort final = detruire, DUA = 10 ans, P10Y
     *  - procédures infructueuse ou sans suite : sort final = detruire, DUA = 10 ans, P10Y
     *
     * @return void
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function addSortFinalArchive(DomNode $contains)
    {
        $valeurCode = 'detruire';
        $valeurDuration = 'P10Y';
        if (self::hasOffreElectroniqueRetenue()) {
            $valeurCode = 'conserver';
            $valeurDuration = 'P15Y';
        }
        $Appraisal = $this->getDomDocument()->createElement('Appraisal');

        $Code = $this->getDomDocument()->createElement('Code');
        $Code->setAttribute('listVersionID', 'edition 2009');
        $CodeText = $this->getDomDocument()->createTextNode($valeurCode);
        $Code->appendChild($CodeText);
        $Appraisal->appendChild($Code);

        $duration = $this->getDomDocument()->createElement('Duration');
        $durationText = $this->getDomDocument()->createTextNode($valeurDuration);
        $duration->appendChild($durationText);
        $Appraisal->appendChild($duration);

        if ($this->lastestDate) {
            $StartDate = $this->getDomDocument()->createElement('StartDate');
            $StartDateText = $this->getDomDocument()->createTextNode($this->lastestDate);
            $StartDate->appendChild($StartDateText);
            $Appraisal->appendChild($StartDate);
        }

        $contains->appendChild($Appraisal);
    }

    /**
     * cette méthode permet de calculer s'il existe une offre électronique retenue pour la consultation
     * On parcours la liste de toutes les décisions / attributions réalisées et pour chacune d'elles on regarde si l'offre_id
     * figure dans la table des Offres électroniques pour cette consultation. Si oui, alors on arrête, sinon on continue.
     *
     * @return bool true si c'est le cas, false sinon
     *
     * @author Thibaut Decaudain <thibaut.decaudain@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function hasOffreElectroniqueRetenue()
    {
        /*
        $hasOffreElectroniqueRetenue = false;

        $c = new Criteria();
        $c->add(CommonDecisionEnveloppePeer::CONSULTATION_ID, $this->getConsultation()->getId());
        $c->add(CommonDecisionEnveloppePeer::ORGANISME, $this->getConsultation()->getOrganisme());
        $decisionsEnveloppes = CommonDecisionEnveloppePeer::doSelect($c, $this->getConnexion());

        if ($decisionsEnveloppes) {
            foreach($decisionsEnveloppes as $decisionEnveloppe) {
                //boucle sur toutes les attributions, et on prend la date de notification la plus récente dans le cas de plusieurs attributaires et plusieurs lots
                if ($decisionEnveloppe->getIdOffre()) {
                    $c1 = new Criteria();
                    $c1->add(CommonOffresPeer::CONSULTATION_REF, $this->getConsultation()->getId());
                    $c1->add(CommonOffresPeer::ORGANISME, $this->getConsultation()->getOrganisme());
                    $c1->add(CommonOffresPeer::ID, $decisionEnveloppe->getIdOffre());

                    $offreElectronique = CommonDecisionEnveloppePeer::doSelectOne($c, $this->getConnexion());

                    if($offreElectronique) {
                        $hasOffreElectroniqueRetenue = true;
                        break;
                    }
                }
            }
        }

        return $hasOffreElectroniqueRetenue;
*/
        $contratQuery = new CommonTContratTitulaireQuery();
        $contrats = $contratQuery->getListeContratsByRefCons($this->getConsultation()->getId(), $this->getConsultation()->getOrganisme());
        if ($contrats) {
            foreach ($contrats as $contrat) {
                //boucle sur toutes les attributions, et on prend la date de notification la plus récente dans le cas de plusieurs attributaires et plusieurs lots
                if ($contrat->getIdOffre() && $contrat->getTypeDepotReponse()) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Permet de supprimer le / dans le debut d'un chemin.
     *
     * @param string $path le chemin
     *
     * @return string le chemin aprés le formatage
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2015
     */
    public function formaterPath($path)
    {
        if ('MEGALIS' == strtoupper(Atexo_Config::getParameter('PROFIL_ARCHIVE'))) {
            $path = preg_replace('#(^/)#', '', $path);
        }

        return $path;
    }

    /**
     * Permet de supprimer les espace d'un idientifiant.
     *
     * @param string $identifiant l'identifiant
     *
     * @return string l'identifiant aprés le formatage
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2015
     */
    public function formaterIdentifiant($identifiant)
    {
        if ('MEGALIS' == strtoupper(Atexo_Config::getParameter('PROFIL_ARCHIVE'))) {
            $identifiant = str_replace(' ', '', $identifiant);
        }

        return $identifiant;
    }

    /**
     * Permet de generer la structure du bordereaud d'archive.
     *
     * @param Atexo_Consultation_Tree_Arborescence $tree
     * @param string                               $consultationId : reference de la consultation
     * @param string                               $numLot         : numero du lot
     *
     * @return string
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-megalis
     *
     * @copyright Atexo 2016
     */
    public function generateXmlStructureArchive($consultationId, $numLot)
    {
        $logger = Atexo_LoggerManager::getLogger('archivage');
        try {
            $tree = $this->getDocTree();
            $this->domDocument = new DOMDocument('1.0', 'utf-8');

            $bordereauArchive = $this->domDocument->createElement('bordereauArchive', '');
            $this->domDocument->appendChild($bordereauArchive);
            $metaDonnees = $this->domDocument->createElement('metaDonnees', '');
            $bordereauArchive->appendChild($metaDonnees);
            $consultation = $this->getConsultation();
            if ($consultation instanceof CommonConsultation) {
                $organisme = $consultation->getOrganisme();
                $serviceId = $consultation->getServiceId();
                $donneesCons = self::getMetaDonneesConsultation($consultation, $logger);
                if ($donneesCons) {
                    $metaDonneesCons = $this->domDocument->importNode($donneesCons, true);
                    $metaDonnees->appendChild($metaDonneesCons);
                }
                $donneesOrg = self::getMetaDonneesOrganisme($organisme, $logger);
                if ($donneesOrg) {
                    $metaDonneesOrg = $this->domDocument->importNode($donneesOrg, true);
                    $metaDonnees->appendChild($metaDonneesOrg);
                }
                if ($serviceId) {
                    $donneesService = self::getMetaDonneesService($serviceId, $organisme, $logger);
                    if ($donneesService) {
                        $metaDonneesService = $this->domDocument->importNode($donneesService, true);
                        $metaDonnees->appendChild($metaDonneesService);
                    }
                }
            }
            if ($tree instanceof Atexo_Consultation_Tree_Arborescence) {
                foreach ($tree->getFils() as $item) {
                    self::creerStructureXmlArborescence($bordereauArchive, $tree->getNom(), $item, $consultationId);
                }
            }

            $tmpPathXml = Atexo_Config::getParameter('COMMON_TMP') . 'xml_' . session_id() . uniqid();
            $this->domDocument->save($tmpPathXml);

            return $tmpPathXml;
        } catch (Exception $e) {
            $logger->error("Erreur lors de la generation du bordereau d'archives " . PHP_EOL . 'Erreur = ' . $e->getMessage() . PHP_EOL . 'Trace = ' . $e->getTraceAsString());
        }
    }

    /**
     * permet de retourner les meta donnees consultation.
     *
     * @param CommonConsultation  $consultation
     * @param Atexo_LoggerManager $logger       tracer les etapes
     *
     * @return string metaDonnees consultation
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-megalis
     *
     * @copyright Atexo 2016
     */
    public function getMetaDonneesConsultation($consultation, $logger)
    {
        $metaDonneesCons = false;
        if ($consultation instanceof CommonConsultation) {
            $xmlInfoCons = (new Atexo_Rest_Action_Consultation())->getXmlConsultation($consultation, $logger);
            $dom = new DOMDocument('1.0', 'utf-8');
            $dom->loadXML($xmlInfoCons);
            foreach ($dom->getElementsByTagName('consultation') as $consI) {
                $metaDonneesCons = $consI;
            }
        }

        return $metaDonneesCons;
    }

    /**
     * permet de retourner les meta donnees organisme.
     *
     * @param string              $acronymeOrg
     * @param Atexo_LoggerManager $logger      tracer les etapes
     *
     * @return string metaDonnees organisme
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-megalis
     *
     * @copyright Atexo 2016
     */
    public function getMetaDonneesOrganisme($acronymeOrg, $logger)
    {
        $logger->info("Début de la recuperation des meta donnees de l'organisme ");
        $metaDonneesOrg = false;
        $logger->info("La recuperation de l'organisme " . $acronymeOrg);
        $organisme = Atexo_Organismes::retrieveOrganismeByAcronyme($acronymeOrg, true);
        if ($organisme instanceof CommonOrganisme) {
            $logger->info('Organisme existe');
            $arrayLogos = [];
            $xmlInfoOrg = (new Atexo_Rest_Action_Actions())->getXmlResultOrganisme($organisme, $arrayLogos, $logger);
            $dom = new DOMDocument('1.0', 'utf-8');
            //echo "\n";
            //print_r($xmlInfoOrg);echo "\n";
            $dom->loadXML($xmlInfoOrg);
            foreach ($dom->getElementsByTagName('organisme') as $orgInfo) {
                $metaDonneesOrg = $orgInfo;
            }
        } else {
            $logger->info("Organisme n'existe pas");
        }
        $logger->info("Fin de la recuperation des meta donnees de l'organisme ");

        return $metaDonneesOrg;
    }

    /**
     * permet de retourner les meta donnees service.
     *
     * @param string              $serviceId
     * @param string              $acronymeOrg
     * @param Atexo_LoggerManager $logger      tracer les etapes
     *
     * @return string metaDonnees service
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-megalis
     *
     * @copyright Atexo 2016
     */
    public function getMetaDonneesService($serviceId, $acronymeOrg, $logger)
    {
        $logger->info('Début de la recuperation des meta donnees du service ');
        $metaDonneesService = false;
        $logger->info("La recuperation du service dont l'id=" . $serviceId . " et l'organisme " . $acronymeOrg);
        $service = (new Atexo_EntityPurchase())->retrieveServiceBYId($serviceId, $acronymeOrg);
        if ($service instanceof CommonService) {
            $logger->info('Service existe');
            $xmlInfoService = (new Atexo_Rest_Action_Actions())->getXmlResultService($service, $logger);
            $dom = new DOMDocument('1.0', 'utf-8');
            $dom->loadXML($xmlInfoService);
            //print_r($xmlInfoService);echo "\n";
            //print_r($dom->getElementsByTagName("service"));exit;
            foreach ($dom->getElementsByTagName('service') as $serviceInfo) {
                $metaDonneesService = $serviceInfo;
            }
        } else {
            $logger->info("Service n'existe pas");
        }
        $logger->info('Fin de la recuperation des meta donnees du service ');

        return $metaDonneesService;
    }

    /**
     * Permet de generer la structure de l'arborescence du bordereau d'archive.
     *
     * @param DOMDocument                  $element        : element domDocument
     * @param string                       $pathParent     : chemin du repertoire parent
     * @param Atexo_Consultation_Tree_Item $item           : item de l'arborescence
     * @param string                       $consultationId : reference de la consultation
     * @param string                       $numLot         : numero du lot
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-megalis
     *
     * @copyright Atexo 2016
     */
    public function creerStructureXmlArborescence($element, $pathParent, Atexo_Consultation_Tree_Item $item, $consultationId)
    {
        try {
            if ($item instanceof Atexo_Consultation_Tree_Directory) {
                $currentDir = $pathParent . DIRECTORY_SEPARATOR . $item->getNom();

                $itemName = self::getCorrespondanceNomRepertoire($item);
                $repElement = $this->domDocument->createElement($itemName, '');
                $element->appendChild($repElement);

                $arrayException = ['echange', 'entrepriseNonRetenue', 'entrepriseRetenue', 'entreprise', 'refConsultationLot'];
                if (in_array($itemName, $arrayException)) {
                    if ('refConsultationLot' == $itemName) {
                        self::addAttribute($this->domDocument, $repElement, 'reference', $consultationId);
                        if (!empty($item->getNumLot())) {
                            self::addAttribute($this->domDocument, $repElement, 'lot', $item->getNumLot());
                        }
                    } else {
                        self::addAttribute($this->domDocument, $repElement, 'nom', $item->getNom());
                    }
                }
                foreach ($item->getFils() as $fils) {
                    self::creerStructureXmlArborescence($repElement, $currentDir, $fils, $consultationId);
                }
            } elseif ($item instanceof Atexo_Consultation_Tree_Group) {
                foreach ($item->getFils() as $fils) {
                    self::creerStructureXmlArborescence($element, $pathParent, $fils, $consultationId);
                }
            } elseif ($item instanceof Atexo_Consultation_Tree_File || $item instanceof Atexo_Consultation_Tree_TimeStamp || $item instanceof Atexo_Consultation_Tree_Signature) {
                $itemName = self::getCorrespondanceNomFichiers($item);
                if ($itemName) {
                    $repElement = $this->domDocument->createElement($itemName, '');
                    $element->appendChild($repElement);
                    self::addAttribute($this->domDocument, $repElement, 'nom', $item->getNom());
                    self::addAttribute($this->domDocument, $repElement, 'empreinte', $item->getEmprunte());
                    self::addAttribute($this->domDocument, $repElement, 'typeEmpreinte', $item->getTypeEmprunte());
                    if ($item->getDateCreation()) {
                        self::addAttribute($this->domDocument, $repElement, 'dateCreation', $item->getDateCreation());
                    }
                }
            } elseif ($item instanceof Atexo_Consultation_Tree_String) {
                $itemName = self::getCorrespondanceNomFichiers($item);
                if ($itemName) {
                    $repElement = $this->domDocument->createElement($itemName, '');
                    $element->appendChild($repElement);
                    self::addAttribute($this->domDocument, $repElement, 'nom', $item->getNom());
                    self::addAttribute($this->domDocument, $repElement, 'empreinte', $item->getEmprunte());
                    self::addAttribute($this->domDocument, $repElement, 'typeEmpreinte', $item->getTypeEmprunte());
                }
            }
        } catch (Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('archivage');
            $logger->error("Erreur lors de la generation de la structure de l'arborescence du bordereau d'archive " . PHP_EOL . 'Erreur = ' . $e->getMessage() . PHP_EOL . 'Trace = ' . $e->getTraceAsString());
        }
    }

    /**
     * Permet de faire la correspondance entre les noms des repertoires de l'arborescence et ceux du bordereau d'archives.
     *
     * @param string $nomRepertoire : nom du repertoire
     *
     * @return mixed
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-megalis
     *
     * @copyright Atexo 2016
     */
    public function getCorrespondanceNomRepertoire($item)
    {
        if ($item instanceof Atexo_Consultation_Tree_Directory) {
            $correspondance = null;
            switch ($item->getNom()) {
                case Atexo_Consultation_Tree_Arborescence::DIR_DOCUMENTS_EXTERNES:
                    $correspondance = 'documentsExternes';
                    break;
                case Atexo_Consultation_Tree_Arborescence::DIR_DEMANDES_COMPLEMENTS_CANDIDATURES:
                    $correspondance = 'demandesComplements';
                    break;
                case Atexo_Consultation_Tree_Arborescence::DIR_REJET_CANDIDATURES:
                    $correspondance = 'rejet';
                    break;
                case Atexo_Consultation_Tree_Arborescence::DIR_DEMANDES_COMPLEMENTS_OFFRES:
                    $correspondance = 'demandesComplements';
                    break;
                case Atexo_Consultation_Tree_Arborescence::DIR_REJET_OFFRES:
                    $correspondance = 'rejet';
                    break;
                case Atexo_Consultation_Tree_Arborescence::DIR_NOTIFICATION_OFFRES:
                    $correspondance = 'notificationActeEngagementEntreprise';
                    break;
                case Atexo_Consultation_Tree_Arborescence::DIR_ATTRIBUTION_OFFRES:
                    $correspondance = 'attributionEntreprise';
                    break;
                case 1 == preg_match('#^echange#', $item->getNom()):
                    $correspondance = 'echange';
                    break;
                case 1 == preg_match('#^offre_non_retenue_entreprise#', $item->getNom()):
                    $correspondance = 'entrepriseNonRetenue';
                    break;
                case 1 == preg_match('#^offre_retenue_entreprise#', $item->getNom()):
                    $correspondance = 'entrepriseRetenue';
                    break;
            }
            if (empty($correspondance)) {
                switch ($item->getNomDirParent()) {
                    case Atexo_Consultation_Tree_Arborescence::DIR_CANDIDATURES_NON_RETENUES:
                        $correspondance = 'entreprise';
                        break;
                    case Atexo_Consultation_Tree_Arborescence::DIR_CANDIDATURES_RETENUES:
                        $correspondance = 'entreprise';
                        break;
                    case Atexo_Consultation_Tree_Arborescence::DIR_CANDIDATURES_OFFRES_DECISION:
                        $correspondance = 'refConsultationLot';
                        break;
                }
            }
            if (empty($correspondance)) {
                $arrayRep = explode('_', $item->getNom());
                if (is_array($arrayRep)) {
                    $correspondance = array_shift($arrayRep);
                    foreach ($arrayRep as $oneName) {
                        $correspondance .= ucfirst(strtolower($oneName));
                    }
                }
            }

            return $correspondance;
        }
    }

    /**
     * Permet de faire la correspondance entre les noms des fichiers des repertoires de l'arborescence et ceux du bordereau d'archives.
     *
     * @param Atexo_Consultation_Tree_File|Atexo_Consultation_Tree_TimeStamp|Atexo_Consultation_Tree_Signature $item : item
     *
     * @return string|null
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-megalis
     *
     * @copyright Atexo 2016
     */
    public function getCorrespondanceNomFichiers($item)
    {
        if (
            $item instanceof Atexo_Consultation_Tree_File || $item instanceof Atexo_Consultation_Tree_TimeStamp || $item instanceof Atexo_Consultation_Tree_Signature
                || $item instanceof Atexo_Consultation_Tree_String
        ) {
            $correspondance = null;
            $nomDirParent = $item->getNomDirParent();
            if (1 == preg_match('#^echange#', $item->getNomDirParent())) {
                $nomDirParent = 'messages_echanges';
            }
            switch ($nomDirParent) {
                case Atexo_Consultation_Tree_Arborescence::DIR_DOSSIER_CONSULTATION:
                    switch ($item->getGroupeName()) {
                        case 'LAST_DCE':
                            if ($item instanceof Atexo_Consultation_Tree_TimeStamp) {
                                $correspondance = 'dceJeton';
                            } elseif ($item instanceof Atexo_Consultation_Tree_File) {
                                $correspondance = 'dce';
                            }
                            break;
                        case 'LAST_RC':
                            if ($item instanceof Atexo_Consultation_Tree_TimeStamp) {
                                $correspondance = 'rcJeton';
                            } elseif ($item instanceof Atexo_Consultation_Tree_File) {
                                $correspondance = 'rc';
                            }
                            break;
                        case 'LAST_COMPLEMENT':
                            if ($item instanceof Atexo_Consultation_Tree_TimeStamp) {
                                $correspondance = 'complementsJeton';
                            } elseif ($item instanceof Atexo_Consultation_Tree_File) {
                                $correspondance = 'complements';
                            }
                            break;
                    }
                    break;

                case Atexo_Consultation_Tree_Arborescence::DIR_REGISTRES:
                    switch ($item->getGroupeName()) {
                        case 'registreQuestions':
                            $correspondance = 'registreQuestions';
                            break;
                        case 'registreRetraits':
                            $correspondance = 'registreRetraits';
                            break;
                        case 'registreDepots':
                            $correspondance = 'registreDepots';
                            break;
                        case 'registreSuiviMessage':
                            $correspondance = 'registreSuiviMessage';
                            break;
                    }
                    break;

                case Atexo_Consultation_Tree_Arborescence::DIR_MESSAGES_ECHANGES:
                    switch ($item->getGroupeName()) {
                        case 'contenusMessagesEchanges':
                            if ($item instanceof Atexo_Consultation_Tree_TimeStamp) {
                                $correspondance = 'messageJeton';
                            } elseif ($item instanceof Atexo_Consultation_Tree_File) {
                                $correspondance = 'message';
                            }
                            break;
                    }
                    break;
                case Atexo_Consultation_Tree_Arborescence::DIR_ACTE_ENGAGEMENT:
                    switch ($item->getGroupeName()) {
                        case 'ACTE_ENGAGEMENT':
                            if ($item instanceof Atexo_Consultation_Tree_File) {
                                $correspondance = 'acteEngagementFichier';
                            }
                            break;
                        case 'SIGNATURE_ACTE_ENGAGEMENT':
                            if ($item instanceof Atexo_Consultation_Tree_Signature) {
                                $correspondance = 'acteEngagementSignature';
                            }
                            break;
                        case 'FICHIER_ENV_OFFRE':
                            if ($item instanceof Atexo_Consultation_Tree_File || $item instanceof Atexo_Consultation_Tree_Signature) {
                                $correspondance = 'fichier';
                            }
                            break;
                    }
                    break;
                case Atexo_Consultation_Tree_Arborescence::DIR_ENVELOPPE_OFFRE:
                    switch ($item->getGroupeName()) {
                        case 'FICHIER_ENV_XML_OFFRE':
                            if ($item instanceof Atexo_Consultation_Tree_String) {
                                $correspondance = 'enveloppeXmlOffre';
                            }
                            if ($item instanceof Atexo_Consultation_Tree_TimeStamp) {
                                $correspondance = 'jetonHorodatage';
                            }
                            break;
                        case 'FICHIER_RAPPORT_SIGNATURE':
                            if ($item instanceof Atexo_Consultation_Tree_File) {
                                $correspondance = 'rapportVerificationSignature';
                            }
                            break;
                    }
                    break;
                case Atexo_Consultation_Tree_Arborescence::DIR_DOCUMENTS_DECISION:
                    switch ($item->getGroupeName()) {
                        case 'REGISTRE_NOTIFICATION':
                            $correspondance = 'registreNotifications';
                            break;
                    }
                    break;

                default:
                    $correspondance = 'fichier';
                    break;
            }

            return $correspondance;
        }
    }

    /**
     * Permet d'ajouter un attribut a une balise DomDocument.
     *
     * @param DOMDocument $domDoc : DomDocument
     * @param $element : balise dans laquelle l'attribut sera ajoutee
     * @param string $name  : nom de l'attribut
     * @param string $value : valeur de l'attribut
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-megalis
     *
     * @copyright Atexo 2016
     */
    public function addAttribute($domDoc, $element, $name, $value)
    {
        try {
            $value = Atexo_Util::toUtf8($value);
            $attribute = $domDoc->createAttribute($name);
            $element->appendChild($attribute);
            $attributeValue = $domDoc->createTextNode($value);
            $attribute->appendChild($attributeValue);
        } catch (Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('archivage');
            $logger->error("Erreur lors de la creation de l'attribut: {nom_attribut = $name}, {valeur_attribut = $value} " . PHP_EOL . 'Erreur = ' . $e->getMessage() . PHP_EOL . 'Trace = ' . $e->getTraceAsString());
        }
    }
}
