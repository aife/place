<?php

namespace Application\Service\Atexo\Consultation;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonTypeAvis;
use Application\Propel\Mpe\CommonTypeAvisPeer;
use Application\Propel\Mpe\CommonTypeAvisQuery;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Languages;
use AtexoCrypto\Dto\Consultation;

/**
 * Classe de TypeAvis.
 *
 * @author Khalid BENAMAR <khalid.benamar@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Consultation_TypeAvis
{
    /**
     * retourne la liste des type d'avis à partir de la base de donnees commune et les charge dans le dropdownlist.
     *
     * @param $returnPropelObject : par defaut à true si l'objet retourné doir être un tableau d'objet propel, à false
     * si l'objet retourné doit être un tableau de type array [id type avis] = libelle
     * $indexedPropelObjectArray : à true si le tableau d'objets propel doit être indexé par id de type d'avis
     *
     * @return la liste des types d'avis
     */
    public static function retrieveAvisTypes($returnPropelObjectsArray = true, $langue = null)
    {
        $typeAvis = self::getCachedTypeAvis();
        if ($typeAvis) {
            if ($returnPropelObjectsArray) {
                return $typeAvis;
            } else {
                $arrayTypeAvis = [];
                foreach ($typeAvis as $unType) {
                    if (!$langue) {
                        $arrayTypeAvis[$unType->getId()] = $unType->getIntituleAvis();
                    } else {
                        $getIntituleAvis = 'getIntituleAvis'.Atexo_Languages::getLanguageAbbreviation($langue);
                        if (0 == strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE'))
                        || !$unType->$getIntituleAvis()) {
                            $arrayTypeAvis[$unType->getId()] = $unType->getIntituleAvis();
                        } else {
                            $arrayTypeAvis[$unType->getId()] = $unType->$getIntituleAvis();
                        }
                    }
                }

                return $arrayTypeAvis;
            }
        } else {
            return [];
        }
    }

    /**
     * Retourne le type d'annonce de la consultation.
     *
     * @param $idTypeAvis identifiant de type d'annonce
     */
    public function retrieveTypeAnnonce($idTypeAvis)
    {
        $typeAvis = self::getCachedTypeAvis();
        if ($typeAvis[$idTypeAvis] instanceof CommonTypeAvis) {
            return $typeAvis[$idTypeAvis];
        } else {
            return false;
        }
    }

    public static function getCachedTypeAvis()
    {
        $typeAvisQuery = new CommonTypeAvisQuery();

        $typesAvis = $typeAvisQuery->find();
        $allAvis = [];
        foreach ($typesAvis as $unAvis) {
            $allAvis[$unAvis->getId()] = $unAvis;
        }

        return $allAvis;
    }

    /**
     * retourne le type d'avis par rapport a son abreviation.
     */
    public function retreiveTypeAvisByAbreviation($abbreviation)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonTypeAvisPeer::ABBREVIATION, $abbreviation);
        $typesAvis = CommonTypeAvisPeer::doSelect($c, $connexionCom);
        if ($typesAvis) {
            return $typesAvis;
        } else {
            return false;
        }
    }
}
