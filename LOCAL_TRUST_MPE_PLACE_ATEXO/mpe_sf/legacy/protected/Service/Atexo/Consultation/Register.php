<?php

namespace Application\Service\Atexo\Consultation;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonEnveloppe;
use Application\Propel\Mpe\CommonEnveloppePapier;
use Application\Propel\Mpe\CommonOffrePapier;
use Application\Propel\Mpe\CommonOffrePapierPeer;
use Application\Propel\Mpe\CommonOffres;
use Application\Propel\Mpe\CommonOffresPeer;
use Application\Propel\Mpe\CommonQuestionsDce;
use Application\Propel\Mpe\CommonQuestionsDcePeer;
use Application\Propel\Mpe\CommonRetraitPapier;
use Application\Propel\Mpe\CommonRetraitPapierPeer;
use Application\Propel\Mpe\CommonTelechargement;
use Application\Propel\Mpe\CommonTelechargementPeer;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_Entreprise;
use Application\Service\Atexo\Atexo_Exception;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Exception;

/**
 * Cette class contient les fonctions permettant d'obtenir différentes informations sur les registres
 * de retraits , registre de dépots et questions pour une consultation (nombre , etc...).
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Consultation_Register
{
    /**
     * calcul le nombre de depot electronique par consultation.
     *
     * @param $idReference : reference de la consultation
     *
     * @return int
     */
    public function retrieveNumbreOfElectronicDepot($consultationId)
    {
        $sql = 'select count(id) as nbDepotElectro FROM Offres WHERE consultation_id=:consultation_id ';
        $statement = Atexo_Db::getLinkCommon(true)->prepare($sql);
        $params = [':consultation_id' => $consultationId];
        $results = $statement->execute($params);
        $nbDepotElectro = 0;
        foreach ($results as $row) {
            $nbDepotElectro = $row['nbDepotElectro'];
        }

        return $nbDepotElectro;
    }

    /** calcul le nombre de depot papier par consultation.
     *
     * @param $whereCondition : string contenat des conditions suplémentaires
     *
     * @return array
     */
    public function retrieveNumbreOfPaperDepot($consultationId)
    {
        $sql = 'select count(id) as nbDepotPapier  FROM Offre_papier WHERE consultation_id=:consultation_id ';
        $statement = Atexo_Db::getLinkCommon(true)->prepare($sql);
        $params = [':consultation_id' => $consultationId];
        $results = $statement->execute($params);
        $nbDepotPapier = 0;
        foreach ($results as $row) {
            $nbDepotPapier = $row['nbDepotPapier'];
        }

        return $nbDepotPapier;
    }

    /** calcul le nombre de questions electroniques par consultation.
     *
     * @param $whereCondition : string contenat des conditions suplémentaires
     *
     * @return array
     */
    public function retrieveNumbreOfElectrnoicQuestion($consultationId)
    {
        $sql = "select count(id) as nbQuestElectro FROM QuestionDCE WHERE type_depot='".Atexo_Config::getParameter('TYPE_DEPOT_QUESTION_ELECTRONIQUE')."' AND consultation_id=:consultation_id ";
        $statement = Atexo_Db::getLinkCommon(true)->prepare($sql);
        $params = [':consultation_id' => $consultationId];
        $results = $statement->execute($params);
        $nbQuestElectro = 0;
        foreach ($results as $row) {
            $nbQuestElectro = $row['nbQuestElectro'];
        }

        return $nbQuestElectro;
    }

    /** calcul le nombre de questions papier par consultation.
     *
     * @param $whereCondition : string contenat des conditions suplémentaires
     *
     * @return array
     */
    public function retrieveNumbreOfPaperQuestion($consultationId)
    {
        $sql = "select count(id) as nbQuestPapier FROM QuestionDCE WHERE type_depot='".Atexo_Config::getParameter('TYPE_DEPOT_QUESTION_PAPIER').' AND consultation_id=:consultation_id ';
        $statement = Atexo_Db::getLinkCommon(true)->prepare($sql);
        $params = [':consultation_id' => $consultationId];
        $results = $statement->execute($params);
        $nbQuestPapier = 0;
        foreach ($results as $row) {
            $nbQuestPapier = $row['nbQuestPapier'];
        }

        return $nbQuestPapier;
    }

    /** calcul le nombre de retraits electroniques par consultation.
     *
     * @param $whereCondition : string contenat des conditions suplémentaires
     *
     * @return array
     */
    public function retrieveNumbreOfElectrnicRetrait($consultationId)
    {
        $sql = 'select count(id) as nbRetraitElectro  FROM Telechargement WHERE consultation_id=:consultation_id ';
        $statement = Atexo_Db::getLinkCommon(true)->prepare($sql);
        $params = [':consultation_id' => $consultationId];
        $results = $statement->execute($params);
        $nbRetraitElectror = 0;
        foreach ($results as $row) {
            $nbRetraitElectror = $row['nbRetraitElectro'];
        }

        return $nbRetraitElectror;
    }

    /** calcul le nombre de retriats papiers par consultation.
     *
     * @param $whereCondition : string contenat des conditions suplémentaires
     *
     * @return array
     */
    public function retrieveNumbreOfPaperRetrait($consultationId)
    {
        $sql = 'select count(id) as nbRetraitPaper  FROM Retrait_Papier WHERE  consultation_id=:consultation_id  ';
        $statement = Atexo_Db::getLinkCommon(true)->prepare($sql);
        $params = [':consultation_id' => $consultationId];
        $results = $statement->execute($params);
        $nbRetraitPaper = 0;

        foreach ($results as $row) {
            $nbRetraitPaper = $row['nbRetraitPaper'];
        }

        return $nbRetraitPaper;
    }

    public function retrieveRetraitsElectroniques($consultationId, $arrayTri, $organisme, $returnCount = false, $limit = 0, $offset = 0)
    {
        $sensTri = $arrayTri['sens'];
        $sensTriParDefaut = $arrayTri['sensTriDefaut'];

        if (!$sensTriParDefaut) {
            $sensTriParDefaut = 'ASC';
        }

        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $c = new Criteria();
        $c->add(CommonTelechargementPeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonTelechargementPeer::ORGANISME, $organisme);
        if ('ASC' == $sensTriParDefaut) {
            $c->addAscendingOrderByColumn(CommonTelechargementPeer::DATETELECHARGEMENT);
        } else {
            $c->addDescendingOrderByColumn(CommonTelechargementPeer::DATETELECHARGEMENT);
        }
        $listeTelecharegement = CommonTelechargementPeer::doSelect($c, $connexionCom);
        if ($returnCount) {
            return count($listeTelecharegement);
        }
        if ('ASC' == $sensTriParDefaut) {
            $numeroTelechargement = 1;
        } else {
            $numeroTelechargement = count($listeTelecharegement);
        }
        if ($listeTelecharegement) {
            if (is_array($listeTelecharegement)) {
                $arrayRegistreVo = [];
                foreach ($listeTelecharegement as $id => $telechargement) {
                    if ($telechargement instanceof CommonTelechargement) {
                        $registreVo = new Atexo_Consultation_RegistreVo();

                        $registreVo->setNumRegistre($numeroTelechargement);

                        $registreVo->setIdRegistre($telechargement->getId());

                        if ('0000' != substr($telechargement->getDatetelechargement(), 0, 4)) {
                            $horodatage = Atexo_Util::iso2frnDateTime($telechargement->getDatetelechargement());
                            $registreVo->setHorodatage(($horodatage));
                        }

                        $registreVo->setNomEntreprise(($telechargement->getEntreprise()));

                        if ($telechargement->getSiret()) {
                            $registreVo->setSirenEntreprise(substr($telechargement->getSiret(), 0, 9));
                            $registreVo->setNicEntreprise(substr($telechargement->getSiret(), 9, 5));
                        } else {
                            if ($telechargement->getSirenEtranger()) {
                                $registreVo->setSirenEntreprise($telechargement->getSirenEtranger());
                            }
                        }

                        $registreVo->setNomContact(($telechargement->getNom()));
                        $registreVo->setPrenomContact(($telechargement->getPrenom()));
                        $registreVo->setMailContact((trim($telechargement->getEmail())));
                        $registreVo->setTelephoneContact(($telechargement->getTelephone()));
                        $registreVo->setFaxContact(($telechargement->getFax()));

                        $registreVo->setAdresseContact($telechargement->getAdresse());
                        $registreVo->setAdresseContactSuite($telechargement->getAdresse2());
                        $registreVo->setCodePostalContact($telechargement->getCodepostal());
                        $registreVo->setVilleContact($telechargement->getVille());
                        $registreVo->setPaysEntreprise($telechargement->getPays());

                        $registreVo->setAccronymePaysEntreprise($telechargement->getAcronymePays());

                        $registreVo->setObservation(($telechargement->getObservation()));

                        $listeFichier = explode('#', $telechargement->getNomsFichiersDce());
                        foreach ($listeFichier as $id => $fichier) {
                            if (!$fichier) {
                                unset($listeFichier[$id]);
                            }
                        }
                        $registreVo->setFichiers($listeFichier);
                        $registreVo->setTiragePlan($telechargement->getTiragePlan());
                        $registreVo->setSupport($telechargement->getSupport());
                        $registreVo->setTypeRegistre(Atexo_Config::getParameter('REGISTRE_RETRAIT_ELECTRONIQUE'));

                        $arrayRegistreVo[] = $registreVo;

                        ('ASC' == $sensTriParDefaut) ? $numeroTelechargement++ : $numeroTelechargement--;
                    }
                }
                if ($sensTri) {
                    $arrayRegistreVo = Atexo_Util::sortArrayOfObjects($arrayRegistreVo, 'NomEntreprise', $sensTri);
                }
                if ($limit) {
                    $arrayRegistreVo = array_slice($arrayRegistreVo, $offset, $limit);
                }

                return $arrayRegistreVo;
            } else {
                return [];
            }
        } else {
            return [];
        }
    }

    public function retrieveRetraitsPapier($id, $arrayTri, $organisme, $searchByReference = true, $returnCount = false, $limit = 0, $offset = 0)
    {
        $sensTri = $arrayTri['sens'];
        $sensTriParDefaut = $arrayTri['sensTriDefaut'];

        if (!$sensTriParDefaut) {
            $sensTriParDefaut = 'ASC';
        }
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $c = new Criteria();
        $c->add(CommonRetraitPapierPeer::ORGANISME, $organisme);
        if ($searchByReference) {
            $c->add(CommonRetraitPapierPeer::CONSULTATION_ID, $id);
        } else {
            $c->add(CommonRetraitPapierPeer::ID, $id);
        }

        if ('ASC' == $sensTriParDefaut) {
            $c->addAscendingOrderByColumn(CommonRetraitPapierPeer::DATETELECHARGEMENT);
        } else {
            $c->addDescendingOrderByColumn(CommonRetraitPapierPeer::DATETELECHARGEMENT);
        }

        $listeRetraits = CommonRetraitPapierPeer::doSelect($c, $connexionCom);
        if ($returnCount) {
            return count($listeRetraits);
        }
        if ('ASC' == $sensTriParDefaut) {
            $numeroRetrait = 1;
        } else {
            $numeroRetrait = count($listeRetraits);
        }

        if ($listeRetraits) {
            if (is_array($listeRetraits)) {
                $arrayRegistreVo = [];
                foreach ($listeRetraits as $id => $retrait) {
                    if ($retrait instanceof CommonRetraitPapier) {
                        $registreVo = new Atexo_Consultation_RegistreVo();

                        $registreVo->setNumRegistre($numeroRetrait);

                        $registreVo->setIdRegistre($retrait->getId());

                        if ('0000' != substr($retrait->getDatetelechargement(), 0, 4)) {
                            $horodatage = Atexo_Util::iso2frnDateTime($retrait->getDatetelechargement());
                            $registreVo->setHorodatage(($horodatage));
                        }

                        $registreVo->setNomEntreprise(($retrait->getEntreprise()));

                        $registreVo->setAccronymePaysEntreprise($retrait->getAcronymePays());

                        if ($retrait->getSiret()) {
                            $registreVo->setSirenEntreprise(substr($retrait->getSiret(), 0, 9));
                            $registreVo->setNicEntreprise(substr($retrait->getSiret(), 9, 5));
                        } elseif ($retrait->getIdentifiantNational()) {
                            $registreVo->setSirenEntreprise($retrait->getIdentifiantNational());
                        }

                        $registreVo->setNomContact(($retrait->getNom()));
                        $registreVo->setPrenomContact(($retrait->getPrenom()));
                        $registreVo->setMailContact((trim($retrait->getEmail())));
                        $registreVo->setTelephoneContact(($retrait->getTelephone()));
                        $registreVo->setFaxContact(($retrait->getFax()));
                        $registreVo->setTiragePlan($retrait->getTiragePlan());
                        $registreVo->setSupport($retrait->getSupport());
                        $registreVo->setObservation(($retrait->getObservation()));

                        $registreVo->setAdresseContact($retrait->getAdresse());
                        $registreVo->setAdresseContactSuite($retrait->getAdresse2());
                        $registreVo->setCodePostalContact($retrait->getCodepostal());
                        $registreVo->setVilleContact($retrait->getVille());
                        $registreVo->setPaysEntreprise($retrait->getPays());

                        $registreVo->setFichiers(['-']);

                        $registreVo->setTypeRegistre(Atexo_Config::getParameter('REGISTRE_RETRAIT_PAPIER'));

                        $arrayRegistreVo[] = $registreVo;

                        ('ASC' == $sensTriParDefaut) ? $numeroRetrait++ : $numeroRetrait--;
                    }
                }
                if ($sensTri) {
                    $arrayRegistreVo = Atexo_Util::sortArrayOfObjects($arrayRegistreVo, 'NomEntreprise', $sensTri);
                }
                if ($limit) {
                    $arrayRegistreVo = array_slice($arrayRegistreVo, $offset, $limit);
                }

                return $arrayRegistreVo;
            } else {
                return [];
            }
        } else {
            return [];
        }
    }

    public function retrieveQuestions($id, $arrayTri, $typeQuestion, $organisme, $searchByReference = true, $returnCount = false, $limit = 0, $offset = 0)
    {
        $sensTri = $arrayTri['sens'];
        $sensTriParDefaut = $arrayTri['sensTriDefaut'];

        if (!$sensTriParDefaut) {
            $sensTriParDefaut = 'ASC';
        }
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $c = new Criteria();
        if ($searchByReference) {
            $c->add(CommonQuestionsDcePeer::CONSULTATION_ID, $id);
        } else {
            $c->add(CommonQuestionsDcePeer::ID, $id);
        }

        $c->add(CommonQuestionsDcePeer::TYPE_DEPOT, $typeQuestion);
        $c->add(CommonQuestionsDcePeer::ORGANISME, $organisme);

        if ('ASC' == $sensTriParDefaut) {
            $c->addAscendingOrderByColumn(CommonQuestionsDcePeer::DATE_DEPOT);
        } else {
            $c->addDescendingOrderByColumn(CommonQuestionsDcePeer::DATE_DEPOT);
        }

        $listeQuestions = CommonQuestionsDcePeer::doSelect($c, $connexionCom);
        if ($returnCount) {
            return count($listeQuestions);
        }
        if ('ASC' == $sensTriParDefaut) {
            $numeroQuestion = 1;
        } else {
            $numeroQuestion = count($listeQuestions);
        }

        if ($listeQuestions) {
            if (is_array($listeQuestions)) {
                $arrayRegistreVo = [];
                foreach ($listeQuestions as $id => $question) {
                    if ($question instanceof CommonQuestionsDce) {
                        $registreVo = new Atexo_Consultation_RegistreVo();

                        $registreVo->setNumRegistre($numeroQuestion);

                        $registreVo->setIdRegistre($question->getId());
                        $registreVo->setReference($question->getConsultationId());

                        if ('0000' != substr($question->getDateDepot(), 0, 4)) {
                            $horodatage = Atexo_Util::iso2frnDateTime($question->getDateDepot());
                            $registreVo->setHorodatage(($horodatage));
                        }

                        if ($question->getSiret()) {
                            $registreVo->setSirenEntreprise(substr($question->getSiret(), 0, 9));
                            $registreVo->setNicEntreprise(substr($question->getSiret(), 9, 5));
                        } elseif ($question->getIdentifiantNational()) {
                            $registreVo->setSirenEntreprise($question->getIdentifiantNational());
                        }

                        $registreVo->setNomEntreprise(($question->getEntreprise()));
                        $registreVo->setAccronymePaysEntreprise($question->getAcronymePays());

                        $registreVo->setNomContact(($question->getNom()));
                        $registreVo->setPrenomContact(($question->getPrenom()));
                        $registreVo->setMailContact((trim($question->getEmail())));
                        $registreVo->setTelephoneContact(($question->getTel()));
                        $registreVo->setFaxContact(($question->getFax()));

                        $registreVo->setAdresseContact($question->getAdresse());
                        $registreVo->setAdresseContactSuite($question->getAdresse2());
                        $registreVo->setCodePostalContact($question->getCp());
                        $registreVo->setVilleContact($question->getVille());
                        $registreVo->setPaysEntreprise($question->getPays());

                        $registreVo->setObservation(($question->getObservation()));

                        $registreVo->setQuestion($question->getQuestion());
                        if ('0000' != substr($question->getDateReponse(), 0, 4)) {
                            $dateReponse = Atexo_Util::iso2frnDateTime($question->getDateReponse());
                            $registreVo->setDateReponse($dateReponse);
                        }

                        if ($question->getPersonneRepondu()) {
                            $registreVo->setResponder($question->getPersonneRepondu());
                        }

                        if ($typeQuestion == Atexo_Config::getParameter('TYPE_DEPOT_QUESTION_ELECTRONIQUE')) {
                            $registreVo->setTypeRegistre(Atexo_Config::getParameter('REGISTRE_QUESTION_ELECTRONIQUE'));
                        } else {
                            $registreVo->setTypeRegistre(Atexo_Config::getParameter('REGISTRE_QUESTION_PAPIER'));
                        }

                        if ($question->getIdFichier()) {
                            $fichierQuesion = [];
                            $fichier['idQuestion'] = $question->getId();
                            $fichier['reference'] = $question->getConsultationId();
                            $fichier['typeQuestion'] = $typeQuestion;
                            $fichier['idFichier'] = $question->getIdFichier();
                            $blob = (new Atexo_Blob())->getBlobById($question->getIdFichier(), $organisme, $question->getNomFichier(), false);
                            $fichier['nomFichier'] = $question->getNomFichier();
                            $tailleFichier = (new Atexo_Blob())->getTailFile($question->getIdFichier(), $organisme);
                            if (!$tailleFichier) {
                                $tailleFichier = 0;
                            }
                            $fichier['tailleFichier'] = '('.Atexo_Util::arrondirSizeFile($tailleFichier / 1024).')';
                            $fichierQuesion[] = $fichier;
                            $registreVo->setFichiers($fichierQuesion);
                        }

                        ('ASC' == $sensTriParDefaut) ? $numeroQuestion++ : $numeroQuestion--;

                        $arrayRegistreVo[] = $registreVo;
                    }
                }
                if ($sensTri) {
                    $arrayRegistreVo = Atexo_Util::sortArrayOfObjects($arrayRegistreVo, 'NomEntreprise', $sensTri);
                }
                if ($limit) {
                    $arrayRegistreVo = array_slice($arrayRegistreVo, $offset, $limit);
                }

                return $arrayRegistreVo;
            } else {
                return [];
            }
        } else {
            return [];
        }
    }

    public function retrieveOffreElectronique($consultationId, $arrayTri, $organisme, $returnCount = false, $limit = 0, $offset = 0)
    {
        $sensTri = $arrayTri['sens'];
        $sensTriParDefaut = $arrayTri['sensTriDefaut'];

        if (!$sensTriParDefaut) {
            $sensTriParDefaut = 'ASC';
        }
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $c = new Criteria();
        $c->add(CommonOffresPeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonOffresPeer::ORGANISME, $organisme);
        $c->add(CommonOffresPeer::STATUT_OFFRES, Atexo_Config::getParameter('STATUT_ENV_BROUILLON'), Criteria::NOT_EQUAL);
        if ('ASC' == $sensTriParDefaut) {
            $c->addAscendingOrderByColumn(CommonOffresPeer::UNTRUSTEDDATE);
        } else {
            $c->addDescendingOrderByColumn(CommonOffresPeer::UNTRUSTEDDATE);
        }

        $listeOffres = CommonOffresPeer::doSelect($c, $connexionCom);
        if ($returnCount) {
            return count($listeOffres);
        }
        if ($listeOffres) {
            if (is_array($listeOffres)) {
                $arrayRegistreVo = [];
                $id = '';
                foreach ($listeOffres as $id => $offre) {
                    if ($offre instanceof CommonOffres) {
                        $registreVo = new Atexo_Consultation_RegistreVo();

                        $registreVo->setNumRegistre($offre->getNumeroReponse());

                        $registreVo->setIdRegistre($offre->getId());

                        if ('0000' != substr($offre->getUntrusteddate(), 0, 4)) {
                            $horodatage = Atexo_Util::iso2frnDateTime($offre->getUntrusteddate(), false, true);
                            $registreVo->setHorodatage(($horodatage));
                        }

                        $paysEnregistrement = '';
                        $nomEn = self::getNomEntrepriseByOffre($offre);
                        $registreVo->setNomEntreprise($nomEn);
                        if ('-' == $nomEn) {
                            $registreVo->setNomContact('-');
                            $registreVo->setMailContact('-');
                            $registreVo->setTelephoneContact('-');
                            $registreVo->setFaxContact('-');
                            $registreVo->setAdresseContact('-');
                            $registreVo->setCodePostalContact('-');
                        } else {
                            $registreVo->setAccronymePaysEntreprise(($offre->getAcronymePays()));
                            $paysEnregistrement = $offre->getAcronymePays();
                            if ('fr' == strtolower($paysEnregistrement)) {
                                $registreVo->setSirenEntreprise(substr($offre->getSiretEntreprise(), 0, 9));
                                $registreVo->setNicEntreprise(substr($offre->getSiretInscrit(), 9, 5));
                            } else {
                                $registreVo->setSirenEntreprise($offre->getIdentifiantNational());
                            }

                            $registreVo->setNomContact(($offre->getNomInscrit()));
                            $registreVo->setPrenomContact(($offre->getPrenomInscrit()));
                            $registreVo->setMailContact((trim($offre->getEmailInscrit())));
                            $registreVo->setTelephoneContact(($offre->getTelephoneInscrit()));
                            $registreVo->setFaxContact(($offre->getFaxInscrit()));

                            $registreVo->setAdresseContact($offre->getAdresseInscrit());
                            $registreVo->setAdresseContactSuite($offre->getAdresse2Inscrit());
                            $registreVo->setCodePostalContact($offre->getCodePostalInscrit());
                            $registreVo->setVilleContact($offre->getVilleInscrit());
                            $registreVo->setPaysEntreprise($offre->getPaysInscrit());
                            $registreVo->setIdEntreprise($offre->getEntreprise()->getId());
                        }
                        $registreVo->setObservation(($offre->getObservation()));

                        $registreVo->setTypeRegistre(Atexo_Config::getParameter('REGISTRE_DEPOT_ELECTRONIQUE'));

                        $registreVo->setRegistreAnnule(('1' == $offre->getDepotAnnule()));
                        $registreVo->setDateAnnulation(Atexo_Util::iso2frnDateTime(substr($offre->getDateAnnulation(), 0, 16)));
                        $registreVo->setOffreVariante($offre->getOffreVariante());

                        $arrayRegistreVo[] = $registreVo;
                    }
                }
                if ($sensTri) {
                    $arrayRegistreVo = Atexo_Util::sortArrayOfObjects($arrayRegistreVo, 'NomEntreprise', $sensTri);
                }
                if ($limit) {
                    $arrayRegistreVo = array_slice($arrayRegistreVo, $offset, $limit);
                }

                return $arrayRegistreVo;
            } else {
                return [];
            }
        } else {
            return [];
        }
    }

    public function retrieveOffrePapier($id, $arrayTri, $organisme, $searchByReference = true, $returnCount = false, $limit = 0, $offset = 0)
    {
        $sensTri = $arrayTri['sens'];
        $sensTriParDefaut = $arrayTri['sensTriDefaut'];

        if (!$sensTriParDefaut) {
            $sensTriParDefaut = 'ASC';
        }

        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $c = new Criteria();
        $c->add(CommonOffrePapierPeer::ORGANISME, $organisme);
        if ($searchByReference) {
            $c->add(CommonOffrePapierPeer::CONSULTATION_ID, $id);
        } else {
            $c->add(CommonOffrePapierPeer::ID, $id);
        }

        if ('ASC' == $sensTriParDefaut) {
            $c->addAscendingOrderByColumn(CommonOffrePapierPeer::DATE_DEPOT);
        } else {
            $c->addDescendingOrderByColumn(CommonOffrePapierPeer::DATE_DEPOT);
        }

        $listeOffres = CommonOffrePapierPeer::doSelect($c, $connexionCom);
        if ($returnCount) {
            return count($listeOffres);
        }
        if ($listeOffres) {
            if (is_array($listeOffres)) {
                $arrayRegistreVo = [];
                foreach ($listeOffres as $id => $offre) {
                    if ($offre instanceof CommonOffrePapier) {
                        $registreVo = new Atexo_Consultation_RegistreVo();

                        $registreVo->setNumRegistre($offre->getNumeroReponse());

                        $registreVo->setReference($offre->getConsultationId());

                        $registreVo->setIdRegistre($offre->getId());

                        if ('0000' != substr($offre->getDateDepot(), 0, 4)) {
                            $horodatage = Atexo_Util::iso2frnDateTime($offre->getDateDepot());
                            $registreVo->setHorodatage($horodatage ?: '-');
                        } else {
                            $registreVo->setHorodatage('-');
                        }

                        $nomEn = self::getNomEntrepriseByOffre($offre);
                        $registreVo->setNomEntreprise($nomEn);
                        if ('-' == $nomEn) {
                            $registreVo->setNomContact('-');
                            $registreVo->setMailContact('-');
                            $registreVo->setTelephoneContact('-');
                            $registreVo->setFaxContact('-');
                            $registreVo->setAdresseContact('-');
                            $registreVo->setCodePostalContact('-');
                        } else {
                            $registreVo->setAccronymePaysEntreprise(($offre->getAcronymePays()));
                            if ($offre->getSiret()) {
                                $registreVo->setSirenEntreprise(substr($offre->getSiret(), 0, 9));
                                $registreVo->setNicEntreprise(substr($offre->getSiret(), 9, 5));
                            } elseif ($offre->getIdentifiantNational()) {
                                $registreVo->setSirenEntreprise($offre->getIdentifiantNational());
                            }

                            $registreVo->setNomContact(($offre->getNom()));
                            $registreVo->setPrenomContact(($offre->getPrenom()));
                            $registreVo->setMailContact((trim($offre->getEmail())));
                            $registreVo->setTelephoneContact(($offre->getTelephone()));
                            $registreVo->setFaxContact(($offre->getFax()));

                            $registreVo->setAdresseContact($offre->getAdresse());
                            $registreVo->setAdresseContactSuite($offre->getAdresse2());
                            $registreVo->setCodePostalContact($offre->getCodepostal());
                            $registreVo->setVilleContact($offre->getVille());
                            $registreVo->setPaysEntreprise($offre->getPays());
                        }
                        $registreVo->setObservation($offre->getObservation() ?: '-');

                        $registreVo->setTypeRegistre(Atexo_Config::getParameter('REGISTRE_DEPOT_PAPIER'));

                        $registreVo->setRegistreAnnule(('1' == $offre->getDepotAnnule()));
                        $registreVo->setDateAnnulation(Atexo_Util::iso2frnDateTime(substr($offre->getDateAnnulation(), 0, 16)));
                        $registreVo->setOffreVariante($offre->getOffreVariante());

                        $arrayRegistreVo[] = $registreVo;
                    }
                }
                if ($sensTri) {
                    $arrayRegistreVo = Atexo_Util::sortArrayOfObjects($arrayRegistreVo, 'NomEntreprise', $sensTri);
                }
                if ($limit) {
                    $arrayRegistreVo = array_slice($arrayRegistreVo, $offset, $limit);
                }

                return $arrayRegistreVo;
            } else {
                return [];
            }
        } else {
            return [];
        }
    }

    public function saveRegistreVo(Atexo_Consultation_RegistreVo $registre, $typeRegistre, $organisme)
    {
        if ($_GET['typeRegistre'] == Atexo_Config::getParameter('REGISTRE_QUESTION_PAPIER')) {
            self::saveQuestion($registre, Atexo_Config::getParameter('TYPE_DEPOT_QUESTION_PAPIER'), $organisme);
        } elseif ($_GET['typeRegistre'] == Atexo_Config::getParameter('REGISTRE_RETRAIT_PAPIER')) {
            self::saveRetrait($registre, $organisme);
        } elseif ($_GET['typeRegistre'] == Atexo_Config::getParameter('REGISTRE_DEPOT_PAPIER')) {
            self::saveDepot($registre, $organisme);
        }
    }

    private function saveQuestion(Atexo_Consultation_RegistreVo $registre, $typeDepot, $organisme)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        if ($registre->getIdRegistre()) {
            $question = CommonQuestionsDcePeer::retrieveByPK($registre->getIdRegistre(), $organisme, $connexionCom);
        } else {
            $question = new CommonQuestionsDce();
            $question->setOrganisme($organisme);
        }

        $question->setConsultationId($registre->getReference());

        if ($registre->getHorodatage()) {
            $question->setDateDepot($registre->getHorodatage());
        } else {
            throw new Atexo_Exception('Date depo cannot be nul');
        }

        if ($registre->getPaysEntreprise() == Atexo_Config::getParameter('DENOMINATION1_GEON2_FRANCE')) {
            $question->setSiret($registre->getSirenEntreprise().$registre->getNicEntreprise());
        } else {
            $question->setIdentifiantNational($registre->getSirenEntreprise());
        }
        $question->setEntreprise($registre->getNomEntreprise());
        $question->setAcronymePays($registre->getAccronymePaysEntreprise());
        $question->setNom($registre->getNomContact());
        $question->setPrenom($registre->getPrenomContact());
        $question->setEmail($registre->getMailContact());
        $question->setTel($registre->getTelephoneContact());
        $question->setFax($registre->getFaxContact());
        $question->setAdresse($registre->getAdresseContact());
        $question->setAdresse2($registre->getAdresseContactSuite());
        $question->setCp($registre->getCodePostalContact());
        $question->setVille($registre->getVilleContact());
        $question->setPays($registre->getPaysEntreprise());
        $question->setQuestion($registre->getQuestion());
        $question->setTypeDepot($typeDepot);

        if ($registre->getFichiers()) {
            $fichier = $registre->getFichiers();
            if ($fichier['idFichier'] && $fichier['nomFichier']) {
                $question->setNomFichier($fichier['nomFichier']);
                $question->setIdFichier($fichier['idFichier']);
            }
        }

        $question->save($connexionCom);
    }

    private function saveRetrait(Atexo_Consultation_RegistreVo $registre, $organisme)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        if ($registre->getIdRegistre()) {
            $retrait = CommonRetraitPapierPeer::retrieveByPK($registre->getIdRegistre(), $organisme, $connexionCom);
        } else {
            $retrait = new CommonRetraitPapier();
            $retrait->setOrganisme($organisme);
        }

        $retrait->setConsultationId($registre->getReference());

        if ($registre->getHorodatage()) {
            $retrait->setDatetelechargement($registre->getHorodatage());
        } else {
            throw new Atexo_Exception('Date depot cannot be null');
        }

        if ($registre->getPaysEntreprise() == Atexo_Config::getParameter('DENOMINATION1_GEON2_FRANCE')) {
            $retrait->setSiret($registre->getSirenEntreprise().$registre->getNicEntreprise());
        } else {
            $retrait->setIdentifiantNational($registre->getSirenEntreprise());
        }
        $retrait->setEntreprise($registre->getNomEntreprise());
        $retrait->setAcronymePays($registre->getAccronymePaysEntreprise());
        $retrait->setNom($registre->getNomContact());
        $retrait->setPrenom($registre->getPrenomContact());
        $retrait->setEmail(trim($registre->getMailContact()));
        $retrait->setTelephone($registre->getTelephoneContact());
        $retrait->setFax($registre->getFaxContact());
        $retrait->setAdresse($registre->getAdresseContact());
        $retrait->setAdresse2($registre->getAdresseContactSuite());
        $retrait->setCodepostal($registre->getCodePostalContact());
        $retrait->setVille($registre->getVilleContact());
        $retrait->setPays($registre->getPaysEntreprise());
        $retrait->setTiragePlan($registre->getTiragePlan());
        $retrait->setSupport($registre->getSupport());
        //echo $retrait->getPays()." ".$retrait->getAcronymePays();exit;
        $retrait->save($connexionCom);
    }

    private function saveDepot(Atexo_Consultation_RegistreVo $registre, $organisme)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $connexionCom->beginTransaction();
        try {
            $consultation = (new Atexo_Consultation())->retrieveConsultation($registre->getReference(), $organisme);
            if ($registre->getIdRegistre()) {
                $depot = CommonOffrePapierPeer::retrieveByPK($registre->getIdRegistre(), $organisme, $connexionCom);
            } else {
                $depot = new CommonOffrePapier();
                $depot->setOrganisme($organisme);
                if ($consultation instanceof CommonConsultation) {
                    $depot->setNumeroReponse($consultation->getNombreOffrePapier() + 1);
                }
            }

            $depot->setConsultationId($registre->getReference());

            if ($registre->getHorodatage()) {
                $depot->setDateDepot($registre->getHorodatage());
            } else {
                throw new Atexo_Exception('Date depo cannot be nul');
            }

            if ($registre->getPaysEntreprise() == Atexo_Config::getParameter('DENOMINATION1_GEON2_FRANCE')) {
                $depot->setSiret($registre->getSirenEntreprise().$registre->getNicEntreprise());
            } else {
                $depot->setIdentifiantNational($registre->getSirenEntreprise());
            }
            $depot->setNomEntreprise($registre->getNomEntreprise());
            $depot->setAcronymePays($registre->getAccronymePaysEntreprise());
            $depot->setNom($registre->getNomContact());
            $depot->setPrenom($registre->getPrenomContact());
            $depot->setEmail(trim($registre->getMailContact()));
            $depot->setTelephone($registre->getTelephoneContact());
            $depot->setFax($registre->getFaxContact());
            $depot->setAdresse($registre->getAdresseContact());
            $depot->setAdresse2($registre->getAdresseContactSuite());
            $depot->setCodepostal($registre->getCodePostalContact());
            $depot->setVille($registre->getVilleContact());
            $depot->setPays($registre->getPaysEntreprise());
            if (null !== $registre->getOffreVariante()) {
                $depot->setOffreVariante($registre->getOffreVariante());
            }

            $depot->setStatutOffrePapier(Atexo_Config::getParameter('STATUT_ENV_FERME'));
            $depot->save($connexionCom);

            if (!$registre->getIdRegistre()) {
                if ($consultation instanceof CommonConsultation) {
                    $nombreLots = is_countable($consultation->getAllLots()) ? count($consultation->getAllLots()) : 0;
                    $lots = $consultation->getAllLots();
                    if ('0' != $consultation->getEnvCandidature()) {
                        $enveloppePapier = new CommonEnveloppePapier();
                        $enveloppePapier->setOrganisme($organisme);
                        $enveloppePapier->setOffrePapierId($depot->getId());
                        $enveloppePapier->setTypeEnv(Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE'));
                        $enveloppePapier->setSousPli('0');
                        $enveloppePapier->setStatutEnveloppe(Atexo_Config::getParameter('STATUT_ENV_FERME'));
                        $enveloppePapier->save($connexionCom);
                    }
                    if ('0' != $consultation->getEnvOffreTechnique()) {
                        if ($nombreLots < 1) {
                            $enveloppePapier = new CommonEnveloppePapier();
                            $enveloppePapier->setOrganisme($organisme);
                            $enveloppePapier->setOffrePapierId($depot->getId());
                            $enveloppePapier->setTypeEnv(Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE'));
                            $enveloppePapier->setSousPli('0');
                            $enveloppePapier->setStatutEnveloppe(Atexo_Config::getParameter('STATUT_ENV_FERME'));
                            $enveloppePapier->save($connexionCom);
                        } else {
                            if (is_array($lots)) {
                                foreach ($lots as $lot) {
                                    $enveloppePapier = new CommonEnveloppePapier();
                                    $enveloppePapier->setOrganisme($organisme);
                                    $enveloppePapier->setOffrePapierId($depot->getId());
                                    $enveloppePapier->setTypeEnv(Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE'));
                                    $enveloppePapier->setSousPli($lot->getLot());
                                    $enveloppePapier->setStatutEnveloppe(Atexo_Config::getParameter('STATUT_ENV_FERME'));
                                    $enveloppePapier->save($connexionCom);
                                }
                            }
                        }
                    }
                    if ('0' != $consultation->getEnvOffre()) {
                        if ($nombreLots < 1) {
                            $enveloppePapier = new CommonEnveloppePapier();
                            $enveloppePapier->setOrganisme($organisme);
                            $enveloppePapier->setOffrePapierId($depot->getId());
                            $enveloppePapier->setTypeEnv(Atexo_Config::getParameter('TYPE_ENV_OFFRE'));
                            $enveloppePapier->setSousPli('0');
                            $enveloppePapier->setStatutEnveloppe(Atexo_Config::getParameter('STATUT_ENV_FERME'));
                            $enveloppePapier->save($connexionCom);
                        } else {
                            if (is_array($lots)) {
                                foreach ($lots as $lot) {
                                    $enveloppePapier = new CommonEnveloppePapier();
                                    $enveloppePapier->setOrganisme($organisme);
                                    $enveloppePapier->setOffrePapierId($depot->getId());
                                    $enveloppePapier->setTypeEnv(Atexo_Config::getParameter('TYPE_ENV_OFFRE'));
                                    $enveloppePapier->setSousPli($lot->getLot());
                                    $enveloppePapier->setStatutEnveloppe(Atexo_Config::getParameter('STATUT_ENV_FERME'));
                                    $enveloppePapier->save($connexionCom);
                                }
                            }
                        }
                    }
                    if ('0' != $consultation->getEnvAnonymat()) {
                        if ($nombreLots < 1) {
                            $enveloppePapier = new CommonEnveloppePapier();
                            $enveloppePapier->setOrganisme($organisme);
                            $enveloppePapier->setOffrePapierId($depot->getId());
                            $enveloppePapier->setTypeEnv(Atexo_Config::getParameter('TYPE_ENV_ANONYMAT'));
                            $enveloppePapier->setSousPli('0');
                            $enveloppePapier->setStatutEnveloppe(Atexo_Config::getParameter('STATUT_ENV_FERME'));
                            $enveloppePapier->save($connexionCom);
                        } else {
                            if (is_array($lots)) {
                                foreach ($lots as $lot) {
                                    $enveloppePapier = new CommonEnveloppePapier();
                                    $enveloppePapier->setOrganisme($organisme);
                                    $enveloppePapier->setOffrePapierId($depot->getId());
                                    $enveloppePapier->setTypeEnv(Atexo_Config::getParameter('TYPE_ENV_ANONYMAT'));
                                    $enveloppePapier->setSousPli($lot->getLot());
                                    $enveloppePapier->setStatutEnveloppe(Atexo_Config::getParameter('STATUT_ENV_FERME'));
                                    $enveloppePapier->save($connexionCom);
                                }
                            }
                        }
                    }
                } else {
                    throw new Exception('error');
                }
            }

            $connexionCom->commit();
        } catch (Exception) {
            $connexionCom->rollBack();
        }
    }

    public function deleteRegistrePapierById($idRegistre, $typeRegistre, $organisme)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        switch ($typeRegistre) {
            case Atexo_Config::getParameter('REGISTRE_RETRAIT_PAPIER'):
                 $registrePapier = CommonRetraitPapierPeer::retrieveByPK($idRegistre, $organisme, $connexionCom);
                                                                            $registrePapier->delete($connexionCom);
                break;
            case Atexo_Config::getParameter('REGISTRE_QUESTION_PAPIER'):
                 $questionDce = CommonQuestionsDcePeer::retrieveByPK($idRegistre, $organisme, $connexionCom);
                if ($questionDce->getTypeDepot() == Atexo_Config::getParameter('TYPE_DEPOT_QUESTION_PAPIER')) {
                    $questionDce->delete($connexionCom);
                }
                break;
            case Atexo_Config::getParameter('REGISTRE_DEPOT_PAPIER'):
                 $offrePapier = CommonOffrePapierPeer::retrieveByPK($idRegistre, $organisme, $connexionCom);
                                                                            $offrePapier->delete($connexionCom);
                break;
            default:
                return false;
        }

        return true;
    }

    public function annulerRegistrePapierById($idRegistre, $typeRegistre, $organisme, $dateAnnulation)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));

        if ($typeRegistre == Atexo_Config::getParameter('REGISTRE_DEPOT_PAPIER')) {
            $offrePapier = CommonOffrePapierPeer::retrieveByPK($idRegistre, $organisme, $connexionCom);
            $offrePapier->setDateAnnulation($dateAnnulation);
            $offrePapier->setDepotAnnule('1');
            $offrePapier->save($connexionCom);
        } else {
            return false;
        }

        return true;
    }

    public function restaurerRegistrePapierById($idRegistre, $typeRegistre, $organisme)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));

        if ($typeRegistre == Atexo_Config::getParameter('REGISTRE_DEPOT_PAPIER')) {
            $offrePapier = CommonOffrePapierPeer::retrieveByPK($idRegistre, $organisme, $connexionCom);
            $offrePapier->setDateAnnulation(null);
            $offrePapier->setDepotAnnule('0');
            $offrePapier->save($connexionCom);
        } else {
            return false;
        }

        return true;
    }

    public static function retrieveTelechargementByIdInscritAndRefCons($consultationId, $organisme, $idEntreprise, $idInscrit, $returnCount = false, $limit = 0, $offset = 0, $arrayTri = [])
    {
        if (!$idInscrit) {
            if ($returnCount) {
                return 0;
            }

            return [];
        }
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonTelechargementPeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonTelechargementPeer::ORGANISME, $organisme);
        $c->add(CommonTelechargementPeer::ID_ENTREPRISE, $idEntreprise);
        $c->add(CommonTelechargementPeer::ID_INSCRIT, $idInscrit);
        if ('fichier_telech' != $arrayTri['commandeName'] && (!$arrayTri['sensTri'] || 'DESC' == $arrayTri['sensTri'])) {
            $c->addDescendingOrderByColumn(CommonTelechargementPeer::DATETELECHARGEMENT);
        } elseif ('fichier_telech' != $arrayTri['commandeName'] && 'ASC' == $arrayTri['sensTri']) {
            $c->addAscendingOrderByColumn(CommonTelechargementPeer::DATETELECHARGEMENT);
        }
        if ('fichier_telech' == $arrayTri['commandeName'] && $arrayTri['sensTri']) {
            if ('DESC' == $arrayTri['sensTri']) {
                $c->addDescendingOrderByColumn(CommonTelechargementPeer::NOMS_FICHIERS_DCE);
            } else {
                $c->addAscendingOrderByColumn(CommonTelechargementPeer::NOMS_FICHIERS_DCE);
            }
        }
        if ($limit) {
            $c->setLimit($limit);
        }
        if ($offset) {
            $c->setOffset($offset);
        }
        $listeFichiersTelecharges = CommonTelechargementPeer::doSelect($c, $connexion);
        if ($returnCount) {
            return count($listeFichiersTelecharges);
        }

        return $listeFichiersTelecharges;
    }

    public static function retrieveQuestionByIdInscritAndRefCons($consultationId, $organisme, $idEntreprise, $idInscrit, $returnCount = false, $limit = 0, $offset = 0, $arrayTri = [], $idquestion = null)
    {
        if (!$idInscrit) {
            if ($returnCount) {
                return 0;
            }

            return [];
        }
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonQuestionsDcePeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonQuestionsDcePeer::ORGANISME, $organisme);
        $c->add(CommonQuestionsDcePeer::ID_ENTREPRISE, $idEntreprise);
        $c->add(CommonQuestionsDcePeer::ID_INSCRIT, $idInscrit);
        if ($idquestion) {
            $c->add(CommonQuestionsDcePeer::ID, $idquestion);
        }
        if ('question' != $arrayTri['commandeName'] && (!$arrayTri['sensTri'] || 'DESC' == $arrayTri['sensTri'])) {
            $c->addDescendingOrderByColumn(CommonQuestionsDcePeer::DATE_DEPOT);
        } elseif ('question' != $arrayTri['commandeName'] && 'ASC' == $arrayTri['sensTri']) {
            $c->addAscendingOrderByColumn(CommonQuestionsDcePeer::DATE_DEPOT);
        }
        if ('date_reponse_question' != $arrayTri['commandeName'] && (!$arrayTri['sensTri'] || 'DESC' == $arrayTri['sensTri'])) {
            $c->addDescendingOrderByColumn(CommonQuestionsDcePeer::DATE_REPONSE);
        } elseif ('date_reponse_question' != $arrayTri['commandeName'] && 'ASC' == $arrayTri['sensTri']) {
            $c->addAscendingOrderByColumn(CommonQuestionsDcePeer::DATE_REPONSE);
        }
        if ('question' == $arrayTri['commandeName'] && $arrayTri['sensTri']) {
            if ('DESC' == $arrayTri['sensTri']) {
                $c->addDescendingOrderByColumn(CommonQuestionsDcePeer::QUESTION);
            } else {
                $c->addAscendingOrderByColumn(CommonQuestionsDcePeer::QUESTION);
            }
        }
        if ($limit) {
            $c->setLimit($limit);
        }
        if ($offset) {
            $c->setOffset($offset);
        }
        $listeQuestions = CommonQuestionsDcePeer::doSelect($c, $connexion);
        if ($returnCount) {
            return count($listeQuestions);
        }

        return $listeQuestions;
    }

    public function getNomEntrepriseByOffre($offre)
    {
        $enveloppes = [];
        if (!Atexo_Module::isEnabled('NomEntrepriseToujoursVisible')) {
            $consultation = (new Atexo_Consultation())->retrieveConsultation($offre->getConsultationId(), $offre->getOrganisme());
            $typeEnveloppe = (new Atexo_Consultation_Responses())->getTypesEnveloppes($consultation->getEnvCandidature(), $consultation->getEnvOffre(), $consultation->getEnvAnonymat(), $consultation->getEnvOffreTechnique());
            if ($offre instanceof CommonOffres) {
                $enveloppes = (new Atexo_Consultation_Responses())->retrieveEnveloppesOffreByIdOffre($offre->getId(), $offre->getOrganisme());
            } elseif ($offre instanceof CommonOffrePapier) {
                $enveloppes = (new Atexo_Consultation_Responses())->retrieveEnveloppesPapiersByIdOffre($offre->getId(), $offre->getOrganisme());
            }
            if (!is_array($enveloppes) || empty($enveloppes)) {
                $enveloppes[] = $offre;
            }
            foreach ($enveloppes as $enveloppe) {
                if ($enveloppe instanceof CommonEnveloppe) {
                    $enveloppe->setCommonOffres($offre);
                } elseif ($enveloppe instanceof CommonEnveloppePapier) {
                    $enveloppe->setCommonOffrePapier($offre);
                }

                $companyName = (new Atexo_Consultation_Responses())->retrieveCompanyName($enveloppe, $typeEnveloppe, $offre->getOrganisme());
                if ('-' != $companyName || ' ' != $companyName) {
                    return $companyName;
                }
            }

            return '-';
        } else {
            if ($offre instanceof CommonOffres) {
                return $offre->getNomEntrepriseInscrit();
            } elseif ($offre instanceof CommonOffrePapier) {
                return $offre->getNomEntreprise();
            }
        }
    }
}
