<?php

namespace Application\Service\Atexo\Consultation;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonEchangePieceJointePeer;
use Application\Service\Atexo\Atexo_Config;
use AtexoCrypto\Dto\Consultation;

class Atexo_Consultation_EchangePieceJointe
{
    /*
     * Récupérer les fichiers documents externes d'une consultation
     */
    public function getEchangePiecesJointes($idEchange, $org)
    {
        $connection = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonEchangePieceJointePeer::ID_MESSAGE, $idEchange);
        $c->add(CommonEchangePieceJointePeer::ORGANISME, $org);
        $results = CommonEchangePieceJointePeer::doSelect($c, $connection);
        if ($results) {
            return $results;
        } else {
            return [];
        }
    }
}
