<?php

namespace Application\Service\Atexo\Consultation;

use App\Utils\Encryption;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonDCE;
use Application\Propel\Mpe\CommonDCEPeer;
use Application\Propel\Mpe\CommonDCEQuery;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_Crypto;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Document;
use Application\Service\Atexo\Atexo_Exception;
use Application\Service\Atexo\Atexo_Files;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_Zip;
use Application\Service\Atexo\Rest\Action\Atexo_Rest_Action_Actions;
use AtexoCrypto\Dto\Consultation;
use AtexoCrypto\Dto\Fichier;
use Exception;
use ZipArchive;

/**
 * Classe de manipulation des consultations.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Consultation_Dce
{
    /**
     * Retourne la dernière version du DCE d'une consulation.
     *
     * @param int    $consultationId
     * @param object $connection
     *
     * @return object DCE
     */
    public static function getDce($consultationId, $org)
    {
        if (null !== $consultationId && null !== $org) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            $c = new Criteria();
            $c->add(CommonDCEPeer::CONSULTATION_ID, $consultationId);
            $c->add(CommonDCEPeer::ORGANISME, $org);
            $c->add(CommonDCEPeer::DCE, '0', Criteria::NOT_EQUAL);
            $c->addDescendingOrderByColumn(CommonDCEPeer::ID);
            $result = CommonDCEPeer::doSelectOne($c, $connexion);
            if ($result) {
                return $result;
            }
        }
    }

    /**
     * Rerourne la liste des fichiers du zip DCE.
     */
    public function retrieveListFileDce($consultationId, $org)
    {
        $dce = (new Atexo_Consultation_Dce())->getDce($consultationId, $org);
        if (!$dce) {
            return false;
        }
        $blob_resource = Atexo_Blob::acquire_lock_on_blob($dce->getDce(), $org);
        $blob_file = $blob_resource['blob']['pointer'];
        //$dce_tmp_file = Atexo_Files::rebuild_zip($dce_tmp_file, $consultationId);
        //FIXME utiliser une copie du DCE
        $listFileDce = Atexo_Zip::getOrderedFilesForZip($blob_file, false);
        if ($listFileDce) {
            return $listFileDce;
        } else {
            return false;
        }
    }

    /**
     * Permet d'ajouter une pièce dans le dossier zip DCE.
     *
     * @param $dce le fichier zip du DCE
     * @param $newFile le nouveau fichier à ajouter dans le zip
     */
    //TODO usesed fct (supprimer apres confirmation)
    /*
          public function addFileInDce($dce,$newFile,$org) {
          $sess_id = session_id();
          $zip_name = "zip-" . $sess_id;
          $zip_filename = Atexo_Config::getParameter('COMMON_TMP') . $zip_name . ".tmp";
          $zip_dir = Atexo_Config::getParameter('COMMON_TMP') . $zip_name . "_dce/";
          @mkdir($zip_dir);
          $dce_user_file = Atexo_Config::getParameter('COMMON_TMP') . session_id() . "_dce_user.tmp";
          @unlink($dce_user_file);
          $zip = "";
          if(!write_file($zip_filename,$zip)) {
              PrintError("Une erreur s'est produite lors de la récupération du dossier de consultation.");
              include 'file_log.php';
              $logger_interne_error->log("modify_cons.php (fct:)Erreur lors de la recuperation du dossier de".
              " consultation $zip de la consultation $reference ",PEAR_LOG_CRIT);
              return;
          }
          // tester l'intégrité du fichier zip
          // -T revient au zip precedent en cas d'echec
          // -D ne considere pas
          $sys_answer=shell_exec("nice zip -T ".escapeshellarg($zip_filename));
          if(strlen($sys_answer)==0) {
              PrintError("Le dossier de consultation ".$zip_filename." est corrompu.");
              return;
          } // END if
          //création du répertoire reprenant l'architecture et les pièces du zip
          $dce_items = array();
          $zip = zip_open($zip_filename);
          if ($zip) {
              $index = 0;
              while ($zip_entry = zip_read($zip)) {
              $index++;
              if (substr(zip_entry_name($zip_entry), strlen(zip_entry_name($zip_entry)) - 1, 1) != "/") {
                $dce_items[$index] = zip_entry_name($zip_entry);
              }
              if (zip_entry_open($zip, $zip_entry, "r")) {
                  $buf = zip_entry_read($zip_entry, zip_entry_filesize($zip_entry));
                  $dce_user_item_file = $zip_dir . zip_entry_name($zip_entry);
                  $depth = 0;
                  $path = dirname(zip_entry_name($zip_entry));
                  while ($path != ".") {
                      $path = dirname($path);
                      $depth++;
                  }
                  for ($i = 1; $i <= $depth; $i++) {
                      $path = dirname($zip_dir . zip_entry_name($zip_entry));
                      for ($j = $i; $j < $depth; $j++) {
                        $path = dirname($path);
                      }
                      @mkdir($path);
                  }
                  if (substr(zip_entry_name($zip_entry), strlen(zip_entry_name($zip_entry)) - 1, 1) != "/") {
                    write_file($dce_user_item_file, $buf);
                  }
                  zip_entry_close($zip_entry);
                }
            }
              zip_close($zip);
          }
          asort($dce_items);
          $path='';
          if ($_POST['file']!=-1) {
                //Suppression du fichier à remplacer
                $val=$dce_items[$_POST['file']];
                $cmd = "rm -f ".$zip_dir.escapeshellarg($val);
                system("$cmd", $sys_answer);
                if($sys_answer!=0) {
                    PrintError("Erreur lors de la suppression du fichier à remplacer dans le dossier de consultation.");
                    include 'file_log.php';
                    $logger_interne_error->log("modify_cons.php (fct:)Erreur lors de la suppression d'un fichier à remplacer ".
                    "dans le dossier de consultation de la consultation $reference ",PEAR_LOG_CRIT);
                    return;
                }
                if (strrpos($val,'/')) {
                    $path=substr($val,0,(strrpos($val,'/')+1));
                }
            } // fin if
          $newfile_filename=$zip_dir.$path.stripslashes($_FILES["attachment"]['name']);
          $exe = "cp -f ".$_FILES["attachment"]['tmp_name']." ".escapeshellarg($newfile_filename)." ;";
          $sys_answer=shell_exec($exe);

          if(strlen($sys_answer)!=0) {
              PrintError("Erreur lors de l'ajout du fichier dans le dossier de consultation (sans doute une partition pleine).");
              include 'file_log.php';
              $logger_interne_error->log("modify_cons.php (fct:)Erreur lors de l'ajout du fichier dans le dossier".
              " de consultation de la consultation $reference ",PEAR_LOG_CRIT);
              return;
          }
          $cmd="cd ".$zip_dir." ; nice zip -r ".escapeshellarg($dce_user_file)." *";
          system("$cmd  > ".Atexo_Config::getParameter('COMMON_TMP')."zip.log 2>&1", $sys_answer);
          $zip = read_file($dce_user_file);
          @unlink($dce_user_file);
          @unlink($zip_filename);
          $cmd = "cd " . Atexo_Config::getParameter('COMMON_TMP') . " ; rm -rf ".$zip_name."_dce/";
          system($cmd, $sys_answer);
        }
    */

    /**
     * Verifie l'intégrité du ZIP via la lib PHP.
     *
     * @param string $zip_filename Fichier zip à tester
     *
     * @return bool true (si ZIP valide)
     *
     * @throws Exception
     */
    public function verifyZip($zip_filename)
    {
        $zip = new ZipArchive();
        $statut_zip = $zip->open($zip_filename, ZipArchive::CHECKCONS);
        $zip->close();
        if (true === $statut_zip) {
            return true;
        }
        throw new Exception("Le fichier original du DCE n'est pas un ZIP valide (error code : $statut_zip).");
    }

    /**
     * Permet d'ajouter ou de remplacer un fichier dans un dossier zip du DCE.
     *
     * @param Dce         $dce                Le DCE original
     * @param int ou bool $indexFileToChange: Index du fichier à remplacer (dans le cas d'un remplacement); ou "false" dans le cas l'ajout d'un fichier
     * @param string      $newFileToAdd       Nom du nouveau fichier
     * @param string      $tmpFilePath        Chemin du nouveau fichier à uploader
     * @param Connection  $org                Connexion db organisme
     *
     * @return string Fichier ZIP modifié (chemin)
     */
    public function updateSelectedFile(CommonDCE $dce, $indexFileToChange, $newFileToAdd, $tmpFilePath, $org)
    {
        $zip_filename = Atexo_Config::getParameter('COMMON_TMP').'zip_tmp_update_'.session_id().'_'.md5(uniqid(random_int(0, mt_getrandmax()), true)).'.zip';
        Atexo_Blob::copyBlob($dce->getDce(), $zip_filename, $org); // On copie l'ancien DCE dans un fichier temp

        $zip = new ZipArchive();
        $zip->open($zip_filename);
        if (false !== $indexFileToChange) {//Remplacement d'un fichier
            $old_dir = dirname($zip->getNameIndex(--$indexFileToChange)); // le -- sert à décrémenter l'index qui est décalé d'un cran.
            ('.' != $old_dir) ? $old_dir .= '/' : $old_dir = ''; // Au cas où on est à la racine du ZIP
            $zip->deleteIndex($indexFileToChange); // on enlève l'ancien fichier
        } else {//Ajout d'un nouveau fichier
            $old_dir = '';
        }
        $zip->addFile($tmpFilePath, $old_dir.Atexo_Util::encodeToOsVisiteur($newFileToAdd)); // on y met le nouveau au même endroit
        $zip->close();
        unlink($tmpFilePath);

        return $zip_filename;
    }

    /*
     * Récupérer les DCES d'une consultation
     */
    public function getDces($consultationId, $org, $c = null)
    {
        $connection = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        if (!$c) {
            $c = new Criteria();
        }
        $c->add(CommonDCEPeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonDCEPeer::ORGANISME, $org);
        $c->add(CommonDCEPeer::DCE, '0', Criteria::NOT_EQUAL);
        $results = CommonDCEPeer::doSelect($c, $connection);
        if ($results) {
            return $results;
        } else {
            return [];
        }
    }

    /*
    * Récupérer les fichiers du DCE
    */
    public function getDataSourecFileDCE($consultationId, $organisme)
    {
        $listFileDce = self::retrieveListFileDce($consultationId, $organisme);

        $dataSource = [];
        if ($listFileDce) {
            foreach ($listFileDce as $directory_files) {
                foreach ($directory_files as $directory_one_file) {
                    $cmp = strcmp(substr($directory_one_file['base'], -1), '/');
                    if (0 == $cmp || '__R__' == $directory_one_file['base']) {
                        if ('__R__' == $directory_one_file['base']) {
                            $directoryOneFileName = $directory_one_file['name'];
                        } else {
                            $directoryOneFileName = $directory_one_file['base'].$directory_one_file['name'];
                        }
                        $dataSource[$directory_one_file['index'].'-'.$directory_one_file['base'].'-'.$directory_one_file['taille']] = $directoryOneFileName;
                    }
                }
            }

            return $dataSource;
        }
    }

    /**
     * Fabrique l'arboresconce des pièces du zip.
     *
     * @param $dceItem: tableau des index de DCE
     * @param $maxIndexPieces:
     */
    public function getArborescence($reference, $organisme, $dceItem = [], $maxIndexPieces = false, $calledFromPortail = false, $justblob = false)
    {
        $dce = (new Atexo_Consultation_Dce())->getDce($reference, $organisme);
        if ($dce instanceof CommonDCE) {
            $blobResource = Atexo_Blob::acquire_lock_on_blob($dce->getDce(), $organisme);
            $blob_file = $blobResource['blob']['pointer'];
            if ($blob_file) {
                $ordered_res = Atexo_Zip::getOrderedFilesForZip($blob_file, false);
                $maxIndex = (new Atexo_Zip())->getMaxIndexZip($blob_file, false);
                if ($justblob) {
                    $result = [];
                    $result[0] = $blobResource['blob']['id'];
                    $result[1] = $ordered_res;

                    return $result;
                }

                return (new Atexo_Zip())->getArborescence($blobResource, $dce->getDce(), $dceItem, null, 'dce_item', false, true);
            }
        }
    }

    public function deleteFile(CommonDCE $dce, $indexFileTodelete, $tmpFilePath, $org)
    {
        $zip_filename = Atexo_Config::getParameter('COMMON_TMP').'zip_tmp_update_'.session_id().'_'.md5(uniqid(random_int(0, mt_getrandmax()), true)).'.zip';
        Atexo_Blob::copyBlob($dce->getDce(), $zip_filename, $org); // On copie l'ancien DCE dans un fichier temp
        $zip = new ZipArchive();
        $zip->open($zip_filename);
        $old_dir = dirname($zip->getNameIndex(--$indexFileTodelete)); // le -- sert à décrémenter l'index qui est décalé d'un cran.
        ('.' != $old_dir) ? $old_dir .= '/' : $old_dir = ''; // Au cas où on est à la racine du ZIP
        $zip->deleteIndex($indexFileTodelete); // on enlève l'ancien fichier
        $zip->close();

        return $zip_filename;
    }

    /*
     * Permet de retourner le chemin du DCE
     */
    public function getPathBlobDCE($reference, $organisme)
    {
        $dce = self::getDce($reference, $organisme);
        if ($dce instanceof CommonDCE) {
            $blobResource = Atexo_Blob::acquire_lock_on_blob($dce->getDce(), $organisme);
            $blob_file = $blobResource['blob']['pointer'];

            return $blob_file;
        } else {
            return false;
        }
    }

    /**
     * Permet de retourner la taille de DCE.
     *
     * @param CommonDCE $dce
     * @param string    $organisme l'organisme
     *
     * @return string la taille de DCE
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since ESR-2015
     *
     * @copyright Atexo 2015
     */
    public function getTailleDCE($dce, $organisme)
    {
        if ($dce instanceof CommonDCE) {
            return $dce->getTailleDce() ? Atexo_Util::GetSizeName($dce->getTailleDce()) : Atexo_Util::arrondirSizeFile((new Atexo_Blob())->getTailFile($dce->getDce(), $organisme) / 1024);
        }

        return '';
    }

    /**
     * Permet de reourner le dce by id.
     *
     * @param int    $id        l'id de dce
     * @param string $organisme acronyme de l'organisme
     *
     * @return CommonDCE
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function retrieveDceById($id, $organisme = null)
    {
        $dceQuery = new CommonDCEQuery();

        return $dceQuery->getDceById($id, $organisme);
    }

    /**
     * Permet d'ajouter les informations du DCE pour le ws.
     *
     * @param $id
     * @param $xmlResponse
     * @param $arrayBlob
     * @param $logger
     * @param $contentType
     * @param string $organisme Acronyme de l'organisme connecte
     *
     * @return bool|string un message dans le cas d'erreur
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function setInfoDCEForWs($id, &$xmlResponse, &$arrayBlob, &$logger, &$contentType, $organisme = null, $typeChecksum = 'md5'): bool|string
    {
        $logger->info("La récupération du DCE dont l'id = ".$id);
        $dce = self::retrieveDceById($id, $organisme);
        if ($dce instanceof CommonDCE) {
            $logger->info('Le DCE existe');
            $blobInfo = Atexo_Blob::getBlobById($dce->getDce(), $dce->getOrganisme(), $dce->getNomDce());
            $arrayBlob = [$blobInfo];
            $logger->info("Mettre les informations du DCE dans l'objet Atexo_Rest_ObjectField_DocumentField");
            $tailleDce = self::getTailleDCESansFormatage($dce, $dce->getOrganisme());
            $documentField = (new Atexo_Document())->getDocumentFieldByInfo($dce->getId(), $dce->getNomDce(), $dce->getOrganisme(), $dce->getDce(), $blobInfo['content'], 'DCE', $tailleDce, null, null, null, null, null, $typeChecksum);
            $contentType = Atexo_Util::getContentTypeByExtension(Atexo_Util::getExtension($dce->getNomDce()));
            $logger->info("La récupération de l'xml contenant les informations du DCE");
            $xmlResponse = (new Atexo_Rest_Action_Actions())->getXmlInfosObjectField($documentField);

            return false;
        } else {
            return "Le DCE dont l'id = ".$id." n'existe pas";
        }
    }

    /**
     * Permet de retourner la taille de DCE sans fomatage.
     *
     * @param CommonDCE $dce
     * @param string    $organisme l'organisme
     *
     * @return string la taille de DCE
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function getTailleDCESansFormatage($dce, $organisme)
    {
        if ($dce instanceof CommonDCE) {
            return $dce->getTailleDce() ?: (new Atexo_Blob())->getTailFile($dce->getDce(), $organisme);
        }

        return 0;
    }

    /**
     * Peremet de modifier le DCE d'une consultation.
     *
     * @param $id
     * @param $organisme
     * @param $tmpFile
     * @param $nomFichier
     * @param $xmlResponse
     * @param $logger
     *
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     * @copyright Atexo 2016
     */
    public function updateDCEFromWs($id, $organisme, $tmpFile, $nomFichier, &$xmlResponse, &$logger): bool|string
    {
        $connexion = null;
        $logger->info('La modification du DCE de la consultation dont la reference = '.$id." et l'organisme ".$organisme);
        $messageErreur = '';
        try {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $connexion->beginTransaction();
            $logger->info('La récupération de la consultation dont la reference = '.$id." et l'organisme ".$organisme);
            $consultation = (new Atexo_Consultation())->getConsultationByReference($id, $organisme, $connexion);
            if ($consultation instanceof CommonConsultation) {
                $ref = $consultation->getReference() ?: $consultation->getId();
                if ($tmpFile) {
                    if (filesize($tmpFile) <= Atexo_Config::getParameter('MAX_SIZE_FILE_WS_REST')) {
                        $logger->info('La récupération du fichier DCE  = '.$nomFichier);
                        $infileDce = Atexo_Config::getParameter('COMMON_TMP').'dce'.session_id().time();
                        if (move_uploaded_file($tmpFile, $infileDce)) {
                            $logger->info('DCE existe');
                            if ($infileDce) {
                                $messageErreur = self::setDceToConsultation($consultation, $organisme, $logger, $nomFichier, $infileDce);
                                if (!$messageErreur) {
                                    $logger->info('Début de la mise à jour de la consultation');
                                    $consultation->saveConsultation($connexion);
                                    $connexion->commit();
                                    $logger->info('Fin de la mise à jour de la consultation');
                                    $xmlResponse = Atexo_Rest_Action_Actions::generateXmlSuccess("Mise à jour du DCE : $organisme - $ref réussi");
                                } else {
                                    $connexion->rollBack();

                                    return $messageErreur;
                                }
                            } else {
                                $connexion->rollBack();

                                return 'Erreur lors de la Mise à jour du DCE : id_consultation = '.$ref.' organisme = '.$organisme;
                            }
                        } else {
                            $connexion->rollBack();
                            $logger->error('move_uploaded_file probleme reception  DCE _FILE : '.$tmpFile.' Destination : '.$infileDce);

                            return 'Erreur lors de la Mise à jour du DCE : id_consultation = '.$ref.' organisme = '.$organisme;
                        }
                    } else {
                        $connexion->rollBack();

                        return 'La taille du fichier DCE depasse la taille maximum';
                    }
                } else {
                    $connexion->rollBack();

                    return 'Merci de joindre le DCE';
                }
            } else {
                $connexion->rollBack();

                return 'La consultation dont la référence est  '.$id." et l'organisme ".$organisme." n'existe pas";
            }
        } catch (Exception $e) {
            $connexion->rollBack();
            throw new Atexo_Exception($e->getMessage().' '.$e->getTraceAsString());
        }

        return false;
    }

    /**
     * Peremet de setter le DCE dans la consultation.
     *
     * @param $consultation
     * @param $organisme
     * @param $logger
     * @param $nomFichier
     * @param $path
     *
     * @return bool|string dans le cas d'une erreur on retourne le message d'erreur si non false
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function setDceToConsultation(&$consultation, $organisme, &$logger, $nomFichier, $path): bool|string
    {
        $dce = (new Atexo_Consultation_Dce())->getDce($consultation->getId(), $organisme);
        $statut = ($dce instanceof CommonDCE) ? Atexo_Config::getParameter('MODIFICATION_FILE') : Atexo_Config::getParameter('AJOUT_FILE');
        $logger->info('Debut traitement DCE');
        if ('ZIP' == strtoupper(Atexo_Util::getExtension($nomFichier))) {
            if (Atexo_Util::verifyZipIntegrity($path)) {
                $logger->info("verifier l'integrite du DCE .... OK");
                $atexoBlob = new Atexo_Blob();
                $atexoCrypto = new Atexo_Crypto();
                $logger->info('Debut horodatage DCE');
                $arrayTimeStampDCE = $atexoCrypto->timeStampFile($path);
                if (is_array($arrayTimeStampDCE)) {
                    $logger->info('horodatage DCE .... OK');
                    $logger->info('Debut instance CommonDCE');
                    $dceO = new CommonDCE();
                    $dceO->setHorodatage($arrayTimeStampDCE['horodatage']);
                    $dceO->setUntrusteddate($arrayTimeStampDCE['untrustedDate']);
                    $dceO->setOrganisme($organisme);
                    $logger->info('Debut sauvegarde blob DCE');
                    $dceIdBlob = $atexoBlob->insert_blob($nomFichier, $path, $organisme, null, Atexo_Config::getParameter('EXTENSION_DCE'));
                    if ('0' != $dceIdBlob) {
                        $logger->info('Sauvegarde blob DCE .... OK');
                        $dceO->setDce($dceIdBlob);
                        $dceO->setNomDce(Atexo_Util::atexoHtmlEntities((new Atexo_Config())->toPfEncoding($nomFichier)));
                        $dceO->setNomFichier(Atexo_Util::atexoHtmlEntities((new Atexo_Config())->toPfEncoding($nomFichier)));
                        $dceO->setStatut($statut);
                        $logger->info("L'enregistrement du DCE dans la consultation");
                        $consultation->addCommonDCE($dceO);
                    } else {
                        return "Probleme d'enregistrement du fichier DCE";
                    }
                } else {
                    return "Probleme d'horodatage du fichier DCE";
                }
            } else {
                return 'Probleme le DCE doit etre un ZIP valide';
            }
        } else {
            return 'Probleme le DCE doit etre un ZIP';
        }

        return false;
    }

    /**
     * @return string[]
     */
    public static function getFileDceOrRgToDownloadForMessec(string $type, int $agentId, int $consultationId, int $id, string $nameFile): array
    {
        $encrypte = Atexo_Util::getSfService(Encryption::class);

        $idHash = $encrypte->cryptId($id);
        $agentIdHash = $encrypte->cryptId($agentId);
        $nameFileHash = $encrypte->cryptId($nameFile);

        $urlDownloadRg = '/agent/direct-download/' . $agentIdHash . '/' . $consultationId . '/' . $type . '-' . $idHash;
        if ($type == 'DCE') {
            $urlDownloadRg .= '-' . $nameFileHash;
        }

        return [
            'nom' => $nameFile,
            'url_telechargement' => $urlDownloadRg
        ];
    }
}
