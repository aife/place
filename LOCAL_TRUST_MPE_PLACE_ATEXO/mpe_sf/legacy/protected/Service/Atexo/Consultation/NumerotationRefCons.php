<?php

namespace Application\Service\Atexo\Consultation;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonNumerotationRefConsAuto;
use Application\Propel\Mpe\CommonNumerotationRefConsAutoPeer;
use Application\Propel\Mpe\CommonServicePeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_Organismes;
use PDO;

/**
 * Génération de la référence.
 *
 * @author Othmane ABISOUROUR <othmane.abisourour@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Consultation_NumerotationRefCons
{
    /*
     * Fonction qui génére la référence utilisateur
     */
    public static function generateUserRef($connexion = null)
    {
        if (!$connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        }
        $value = Atexo_Config::getParameter('CONSULTATION_NUMERATION_AUTOMATIQUE');
        $date = substr(date('Y'), 2, 4);
        $referenceName = $date.$value;
        $serviceId = Atexo_CurrentUser::getCurrentServiceId();
        $acronyme = Atexo_CurrentUser::getCurrentOrganism();
        $org = Atexo_Organismes::retrieveOrganismeByAcronyme(Atexo_CurrentUser::getCurrentOrganism());
        //Numerotaion avec SIGLE et ID service ou organisme.
        if ('1' == Atexo_Config::getParameter('NUMEROTATION_AUTOMATIQUE_AJOUT_SERVICE_SIGLE')) {
            $increment = self::getNewIncrementConsultation($serviceId, $acronyme, $connexion);
            if (!empty($serviceId)) {
                $service = CommonServicePeer::retrieveByPK($serviceId, $connexion);
                $sigle = $service->getSigle();
            } else {
                $sigle = $org->getSigle();
            }
            $position = '';
        } elseif ('1' == Atexo_Config::getParameter('NUMEROTATION_AUTOMATIQUE_AJOUT_SIGLE')) {
            $increment = self::getNewIncrementConsultation(null, $acronyme, $connexion);
            $sigle = $org->getSigle();
            $position = '0';
        } else {
            $increment = self::getNewIncrementConsultation(null, $acronyme, $connexion);
            $sigle = '';
            $position = '0';
        }

        if ($increment < '10') {
            $increment = $position.'00'.$increment;
        } elseif ($increment < '100') {
            $increment = $position.'0'.$increment;
        } elseif ($increment < '999') {
            $increment = $position.$increment;
        }

        return $sigle.$referenceName.$increment;
    }

    public static function retrieveNumerotation($annee, $serviceId)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $org = Atexo_CurrentUser::getCurrentOrganism();
        $c = new Criteria();
        $c->add(CommonNumerotationRefConsAutoPeer::ANNEE, $annee, Criteria::EQUAL);
        $c->add(CommonNumerotationRefConsAutoPeer::ORGANISME, $org, Criteria::EQUAL);
        $comparison = (null != $serviceId) ? Criteria::EQUAL : Criteria::ISNULL;
        $c->add(CommonNumerotationRefConsAutoPeer::SERVICE_ID, $serviceId, $comparison);

        $numRefCons = CommonNumerotationRefConsAutoPeer::doSelectOne($c, $connexion);

        if ($numRefCons) {
            return $numRefCons;
        } else {
            return false;
        }
    }

    public static function getNewIncrementConsultation($serviceId, $org, $connexion = null)
    {
        if (!$connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        }
        $numerotationRefConsAuto = self::retrieveNumerotation(date('Y'), $serviceId);
        if ($numerotationRefConsAuto instanceof CommonNumerotationRefConsAuto) {
            $numeroRefCons = $numerotationRefConsAuto->getIdConsAuto() + 1;
            $sql = 'UPDATE Numerotation_ref_cons_auto set `id_cons_auto`='.$numeroRefCons." where `organisme`='".$numerotationRefConsAuto->getOrganisme()."' ".
                  ' and `annee`='.$numerotationRefConsAuto->getAnnee().' and ';
            if (null != $serviceId) {
                $sql .= '`service_id`='.$serviceId;
            } else {
                $sql .= '`service_id` IS NULL';
            }
            Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_NUM);
        } else {
            $numerotationRefConsAuto = new CommonNumerotationRefConsAuto();
            $numerotationRefConsAuto->setAnnee(date('Y'));
            $numerotationRefConsAuto->setOrganisme($org);
            $numerotationRefConsAuto->setIdConsAuto(1);
            if (null != $serviceId) {
                $numerotationRefConsAuto->setServiceId($serviceId);
            }
            $numerotationRefConsAuto->save($connexion);
            $numeroRefCons = $numerotationRefConsAuto->getIdConsAuto();
        }

        return $numeroRefCons;
    }

    public function resetIncrementConsForService($connexionOrg)
    {
        $numerotationRefConsAuto = self::retrieveNumerotation(date('Y'));
        if ($numerotationRefConsAuto instanceof CommonNumerotationRefConsAuto) {
            return false;
        } else {
            return true;
        }
    }
}
