<?php

namespace Application\Service\Atexo\Consultation;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonDCE;
use Application\Propel\Mpe\CommonRG;
use Application\Propel\Mpe\CommonRGPeer;
use Application\Propel\Mpe\CommonRGQuery;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_Crypto;
use Application\Service\Atexo\Atexo_Document;
use Application\Service\Atexo\Atexo_Exception;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Rest\Action\Atexo_Rest_Action_Actions;
use AtexoCrypto\Dto\Consultation;
use Exception;

/**
 * Classe de manipulation des consultations.
 *
 * @author ZAKI Anas <anas.zaki@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Consultation_Rg
{
    /**
     * Retourne la dernière version du Reglement d'une consulation.
     *
     * @param int    $consultationId
     * @param object $connection
     *
     * @return object DCE
     */
    public static function getReglement($consultationId, $org)
    {
        if (null !== $consultationId && null !== $org) {
            $connection = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
            $c = new Criteria();
            $c->add(CommonRGPeer::CONSULTATION_ID, $consultationId);
            $c->add(CommonRGPeer::ORGANISME, $org);
            $c->add(CommonRGPeer::RG, '0', Criteria::NOT_EQUAL);
            $c->addDescendingOrderByColumn(CommonRGPeer::ID);
            $result = CommonRGPeer::doSelectOne($c, $connection);
            if ($result) {
                return $result;
            }
        }
    }

    /**
     * Recupérer les RGs pour une consultation.
     */
    public function getRGs($consultationId, $org, $c = null)
    {
        $connection = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        if (!$c) {
            $c = new Criteria();
        }
        $c->add(CommonRGPeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonRGPeer::ORGANISME, $org);
        $c->add(CommonRGPeer::RG, '0', Criteria::NOT_EQUAL);
        $results = CommonRGPeer::doSelect($c, $connection);

        if ($results) {
            return $results;
        } else {
            return [];
        }
    }

    /**
     * Permet de reourner le rgelement by id.
     *
     * @param int    $id        l'id de regelemnt
     * @param string $organisme Acronyme de l'organisme connecte
     *
     * @return CommonDCE
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function retrieveRgById($id, $organisme = null)
    {
        $rgQuery = new CommonRGQuery();

        return $rgQuery->getRgById($id, $organisme);
    }

    /**
     * Permet d'ajouter les informations du reglement de la consultation pour le ws.
     *
     * @param $id
     * @param $xmlResponse
     * @param $arrayBlob
     * @param $logger
     * @param $contentType
     * @param string $organisme Acronyme de l'organisme connecte
     *
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function setInfoRGForWs($id, &$xmlResponse, &$arrayBlob, &$logger, &$contentType, $organisme = null, $typeChecksum = 'md5'): bool|string
    {
        $logger->info("La récupération du règlement de consultation  dont l'id = ".$id);
        $rg = self::retrieveRgById($id, $organisme);
        if ($rg instanceof CommonRG) {
            $logger->info('Le réglement  existe');
            $blobInfo = Atexo_Blob::getBlobById($rg->getRg(), $rg->getOrganisme(), $rg->getNomFichier());
            $arrayBlob = [$blobInfo];
            $logger->info("Mettre les informations du Réglement dans l'objet Atexo_Rest_ObjectField_DocumentField");
            $documentField = (new Atexo_Document())->getDocumentFieldByInfo($rg->getId(), $rg->getNomFichier(), $rg->getOrganisme(), $rg->getRg(), $blobInfo['content'], 'REGLEMENT_CONSULTATION', null, null, null, null, null, null, $typeChecksum, $rg->getHorodatage());
            $contentType = Atexo_Util::getContentTypeByExtension(Atexo_Util::getExtension($rg->getNomFichier()));
            $logger->info("La récupération de l'xml contenant les informations du Réglement");
            $xmlResponse = (new Atexo_Rest_Action_Actions())->getXmlInfosObjectField($documentField);

            return false;
        } else {
            return "Le règlement de consultation  dont l'id = ".$id." n'existe pas";
        }
    }

    /**
     * Peremet de modifier le reglement d'une consultation.
     *
     * @param $id
     * @param $organisme
     * @param $tmpFile
     * @param $nomFichier
     * @param $xmlResponse
     * @param $logger
     *
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     * @copyright Atexo 2016
     */
    public function updateRGFromWs($id, $organisme, $tmpFile, $nomFichier, &$xmlResponse, &$logger): bool|string
    {
        $connexion = null;
        $logger->info('La modification du réglement de la consultation dont la reference = '.$id." et l'organisme ".$organisme);
        $messageErreur = '';
        try {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $connexion->beginTransaction();
            $logger->info('La récupération de la consultation dont la reference = '.$id." et l'organisme ".$organisme);
            $consultation = (new Atexo_Consultation())->getConsultationByReference($id, $organisme, $connexion);
            if ($consultation instanceof CommonConsultation) {
                $ref = $consultation->getReference() ?: $consultation->getId();
                if ($tmpFile) {
                    if (filesize($tmpFile) <= Atexo_Config::getParameter('MAX_SIZE_FILE_WS_REST')) {
                        $logger->info('La récupération du fichier réglement  = '.$nomFichier);
                        $infileRc = Atexo_Config::getParameter('COMMON_TMP').'rc'.session_id().time();
                        if (move_uploaded_file($tmpFile, $infileRc)) {
                            $logger->info('Règlement de consultation  existe');
                            if ($infileRc) {
                                $messageErreur = self::setRgToConsultation($consultation, $organisme, $logger, $nomFichier, $infileRc);
                                if (!$messageErreur) {
                                    $logger->info('Début de la mise à jour de la consultation');
                                    $consultation->saveConsultation($connexion);
                                    $connexion->commit();
                                    $logger->info('Fin de la mise à jour de la consultation');
                                    $xmlResponse = Atexo_Rest_Action_Actions::generateXmlSuccess("Mise à jour du règlement de consultation  : $organisme - $ref réussi");
                                } else {
                                    return $messageErreur;
                                }
                            } else {
                                return 'Erreur lors de la Mise à jour du règlement de consultation  : id_consultation = '.$ref.' organisme = '.$organisme;
                            }
                        } else {
                            $logger->error('move_uploaded_file probleme reception  RC _FILE : '.$tmpFile.' Destination : '.$infileRc);

                            return 'Erreur lors de la Mise à jour du  règlement de consultation  : id_consultation = '.$ref.' organisme = '.$organisme;
                        }
                    } else {
                        return 'La taille du fichier  règlement de consultation  depasse la taille maximum';
                    }
                } else {
                    return 'Merci de joindre le règlement de consultation ';
                }
            } else {
                return 'La consultation dont la référence est  '.$id." et l'organisme ".$organisme." n'existe pas";
            }
        } catch (Exception $e) {
            $connexion->rollBack();
            throw new Atexo_Exception($e->getMessage().' '.$e->getTraceAsString());
        }

        return false;
    }

    /**
     * Peremet de mettre le reglement de consultation dans la consultation.
     *
     * @param $consultation
     * @param $organisme
     * @param $logger
     * @param $nomFichier
     * @param $path
     *
     * @return bool|string dans le cas d'une erreur on retourne le message d'erreur si non false
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function setRgToConsultation(&$consultation, $organisme, &$logger, $fileName, $path): bool|string
    {
        $rgConsultation = (new Atexo_Consultation_Rg())->getReglement($consultation->getId(), $organisme);
        $statut = ($rgConsultation instanceof CommonRG) ? Atexo_Config::getParameter('REMPLACEMENT_FILE') : Atexo_Config::getParameter('AJOUT_FILE');
        $logger->info('Début traitement du fichier règlement de consultation ');
        $atexoBlob = new Atexo_Blob();
        $atexoCrypto = new Atexo_Crypto();
        $arrayTimeStampRg = $atexoCrypto->timeStampFile($path);
        if (is_array($arrayTimeStampRg)) {
            $rgIdBlob = $atexoBlob->insert_blob($fileName, $path, $organisme, null, Atexo_Config::getParameter('EXTENSION_RC'));
            if ('0' != $rgIdBlob) {
                $rgO = new CommonRG();
                $rgO->setHorodatage($arrayTimeStampRg['horodatage']);
                $rgO->setUntrusteddate($arrayTimeStampRg['untrustedDate']);
                $rgO->setOrganisme($organisme);
                $rgO->setRg($rgIdBlob);
                $rgO->setNomFichier(Atexo_Util::atexoHtmlEntities($fileName));
                $rgO->setStatut($statut);
                $consultation->addCommonRG($rgO);
                $logger->info("L'enregistrement du fichier règlement de consultation ");
            } else {
                return "Problème d'enregistrement du fichier : règlement de consultation ";
            }
        } else {
            return "Problème d'horodatage du fichier : règlement de consultation ";
        }

        return false;
    }
}
