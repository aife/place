<?php

namespace Application\Service\Atexo\Consultation;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonTireurPlan;
use Application\Propel\Mpe\CommonTireurPlanPeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;

/**
 * Classe de manipulation des fournisseurs de documents.
 *
 * @author ZAKI Anas <anas.zaki@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Consultation_SuppliersOfDocuments
{
    /**
     * Recherche les fournisseurs de documents.
     */
    public static function search($idService = null)
    {
        $c = new Criteria();
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c->add(CommonTireurPlanPeer::SERVICE_ID, $idService, Criteria::EQUAL);
        $c->add(CommonTireurPlanPeer::ORGANISME, Atexo_CurrentUser::getCurrentOrganism());
        $c->addAscendingOrderByColumn(CommonTireurPlanPeer::NOM);
        $suppliersOfDocuments = CommonTireurPlanPeer::doSelect($c, $connexionCom);

        return $suppliersOfDocuments;
    }

    /**
     * retourne les données relatives au fournisseur.
     *
     * @param int $idSupplier l'id du fournisseur
     *
     * @return object fournisseur
     */
    public static function getDataSupplier($idSupplier, $organisme = '')
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        if ('' == $organisme) {
            $organisme = Atexo_CurrentUser::getCurrentOrganism();
        }
        $supplier = CommonTireurPlanPeer::retrieveByPk($idSupplier, $organisme, $connexionCom);

        return $supplier;
    }

    /**
     * Supprime un fournisseur de document.
     *
     * @param int $idSupplier l'id du fournisseur de documents
     */
    public static function delete($idSupplier)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $supplier = CommonTireurPlanPeer::retrieveByPk($idSupplier, Atexo_CurrentUser::getCurrentOrganism(), $connexionCom);
        CommonTireurPlanPeer::doDelete($supplier, $connexionCom);
    }

    /**
     * Ajoute un fournisseur de documents.
     *
     * @param array $arraySupplier le tableau des données du fournisseur
     */
    public static function add($arraySupplier)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $supplierObject = new CommonTireurPlan();
        //$supplierObject->setId('');
        $supplierObject->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
        $supplierObject->setNom($arraySupplier['Nom']);
        $supplierObject->setEmail(trim($arraySupplier['Email']));
        $supplierObject->setServiceId($arraySupplier['IdService']);
        $supplierObject->save($connexionCom);
    }

    /**
     * Modifie un fournisseur de documents.
     *
     * @param int $idSupplier l'id du fournisseur
     */
    public static function modify($arraySupplier)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $supplier = CommonTireurPlanPeer::retrieveByPk($arraySupplier['Id'], Atexo_CurrentUser::getCurrentOrganism(), $connexionCom);
        $supplier->setNom($arraySupplier['Nom']);
        $supplier->setEmail(trim($arraySupplier['Email']));
        $supplier->setServiceId($arraySupplier['IdService']);
        $supplier->save($connexionCom);
    }

    /**
     * Retourne le mail du fournisseur en fonction de son id.
     *
     * @param int    $idSupplier l'id du fournisseur
     * @param string $organisme  l'organisme
     */
    public function getMailSupplierById($idSupplier, $organisme)
    {
        $supplier = self::getDataSupplier($idSupplier, $organisme);
        if ($supplier) {
            return $supplier->getEmail();
        }
    }
}
