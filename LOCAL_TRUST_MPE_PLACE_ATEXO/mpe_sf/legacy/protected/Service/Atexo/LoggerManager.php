<?php

namespace Application\Service\Atexo;

/**
 * Classe de log.
 *
 * @category Atexo
 */
class Atexo_LoggerManager
{
    public static function getLogger($channel = 'app')
    {
        $loggerService = $channel === 'app' ? 'mpe.logger' : 'monolog.logger.' . $channel;

        return Atexo_Util::getSfService($loggerService);
    }
}
