<?php

namespace Application\Service\Atexo\InterfaceWs;

/**
 * Classe de.
 *
 * @author Khalid BENAMAR <khalid.benamar@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_InterfaceWs_UserVo
{
    private $_idToken;
    private $_validityDate;
    private $_login;
    private $_email;
    private $_firstname;
    private $_lastname;
    private $_type;

    public function getIdToken()
    {
        return $this->_idToken;
    }

    public function setIdToken($value)
    {
        $this->_idToken = $value;
    }

    public function getValidityDate()
    {
        return $this->_validityDate;
    }

    public function setValidityDate($value)
    {
        $this->_validityDate = $value;
    }

    public function getLogin()
    {
        return $this->_login;
    }

    public function setLogin($value)
    {
        $this->_login = $value;
    }

    public function getEmail()
    {
        return $this->_email;
    }

    public function setEmail($value)
    {
        $this->_email = $value;
    }

    public function getFirstname()
    {
        return $this->_firstname;
    }

    public function setFirstname($value)
    {
        $this->_firstname = $value;
    }

    public function getLastname()
    {
        return $this->_lastname;
    }

    public function setLastname($value)
    {
        $this->_lastname = $value;
    }

    public function getType()
    {
        return $this->_type;
    }

    public function setType($value)
    {
        $this->_type = $value;
    }
}
