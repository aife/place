<?php

namespace Application\Service\Atexo\InterfaceWs;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonEntrepriseInfoExercice;
use Application\Propel\Mpe\CommonTEtablissement;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Entreprise;
use Application\Service\Atexo\Atexo_InterfaceWs;
use Application\Service\Atexo\Atexo_Xml;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Etablissement;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Inscrit;

/**
 * Classe de.
 *
 * @author Mouslim MITALI <mouslim.mital@atexo.com>
 * @copyright Atexo 2009
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_InterfaceWs_Company extends Atexo_InterfaceWs
{
    public function getXmlResponseCompany($defensePortalRegistered, $fromDate = null)
    {
        return $this->generateXmlReponseCompany([], [], $fromDate, false, $defensePortalRegistered);
    }

    public function generateXmlReponseCompany($entreprises, $inscrits, $fromDate = null, $soclePPP = false, $defensePortalRegistered = null)
    {
        $domDocument = self::getXmlTemplate($soclePPP, true);
        $transaction = $domDocument->documentElement;
        $response = $transaction->getElementsByTagName('Response')->item(0);
        $companies = $domDocument->createElement('Companies');
        $response->appendChild($companies);
        if ($fromDate) {
            (new Atexo_Xml())->addAttribute($domDocument, $companies, 'fromDate', ($fromDate));
        }

        if ($defensePortalRegistered) {
            $entreprises = (new Atexo_Entreprise())->retreiveEntrepriseAnnuaireDefense($fromDate);
        }
        // Companies
        if (is_array($entreprises)) {
            foreach ($entreprises as $oneEntreprise) {
                $paysEnregistrement = '';
                //            $oneEntreprise = array_map("",$oneEntreprise);
                // Companie
                $companie = $domDocument->createElement('Company');
                $companies->appendChild($companie);
                // id_initial
                if ($soclePPP) { //pour le socle PPP
                    $id = $oneEntreprise['ID_INITIAL'];
                    //$id=$oneEntreprise['ID'];
                    if (($oneEntreprise['PAYSENREGISTREMENT']) == Atexo_Config::getParameter('DENOMINATION2_GEON2_FRANCE') || ($oneEntreprise['PAYSENREGISTREMENT']) == Atexo_Config::getParameter('DENOMINATION1_GEON2_FRANCE')) {
                        $paysEnregistrement = '1';
                        if ('' != $oneEntreprise['SIREN']) {
                            (new Atexo_Xml())->addAttribute($domDocument, $companie, 'siren', ($oneEntreprise['SIREN'])); // Pour le socle PPP
                        }
                        if ('' != $oneEntreprise['NICSIEGE']) {
                            (new Atexo_Xml())->addAttribute($domDocument, $companie, 'nicCode', ($oneEntreprise['NICSIEGE'])); // Pour le socle PPP
                        }
                        if ('' != $oneEntreprise['CODEAPE']) {
                            (new Atexo_Xml())->addAttribute($domDocument, $companie, 'apeNafNaceCode', ($oneEntreprise['CODEAPE']));
                        }
                    } else {
                        $paysEnregistrement = '0';
                        (new Atexo_Xml())->addAttribute($domDocument, $companie, 'nationalId', ($oneEntreprise['SIRENETRANGER'])); // Pour le socle PPP
                    }
                    (new Atexo_Xml())->addAttribute($domDocument, $companie, 'paysEnregistrementFrance', ($paysEnregistrement)); // test sur les valeurs a saisir(boolean)
                } else {
                    $id = $oneEntreprise['ID'];
                }
                (new Atexo_Xml())->addAttribute($domDocument, $companie, 'id', ($id));
                (new Atexo_Xml())->addAttribute($domDocument, $companie, 'name', ($oneEntreprise['NOM']));

                $siret = $oneEntreprise['SIREN'].$oneEntreprise['NICSIEGE'];
                if (!$soclePPP) {
                    (new Atexo_Xml())->addAttribute($domDocument, $companie, 'siret', ($siret));
                }

                $etablissementSiege = (new Atexo_Entreprise_Etablissement())->retreiveEtablissementSiegeByIdEntreprise($id);
                if ($etablissementSiege instanceof CommonTEtablissement) {
                    (new Atexo_Xml())->addAttribute($domDocument, $companie, 'address', ($etablissementSiege->getAdresse().' '.$etablissementSiege->getAdresse2()));
                    (new Atexo_Xml())->addAttribute($domDocument, $companie, 'postalCode', ($etablissementSiege->getCodePostal()));
                    (new Atexo_Xml())->addAttribute($domDocument, $companie, 'city', ($etablissementSiege->getVille()));
                }
                unset($etablissementSiege);
                (new Atexo_Xml())->addAttribute($domDocument, $companie, 'country', ($oneEntreprise['PAYSADRESSE']));
                (new Atexo_Xml())->addAttribute($domDocument, $companie, 'phone', ($oneEntreprise['TELEPHONE']));

                (new Atexo_Xml())->addAttribute($domDocument, $companie, 'legalStatus', ($oneEntreprise['FORMEJURIDIQUE']));
                if (!$soclePPP) {
                    (new Atexo_Xml())->addAttribute($domDocument, $companie, 'apeNafNaceCode', ($oneEntreprise['CODEAPE']));
                }

                // Managers
                // Manager
                $dirigents = (new Atexo_Entreprise())->retreiveDirigentsCompany($oneEntreprise['id']); //$oneEntreprise['id']
                if ($dirigents) {
                    $managers = $domDocument->createElement('Managers');
                    $companie->appendChild($managers);
                    foreach ($dirigents as $oneDirigent) {
                        //return $oneDirigent->getNom();
                        $manager = $domDocument->createElement('Manager');
                        $managers->appendChild($manager);
                        (new Atexo_Xml())->addAttribute($domDocument, $manager, 'firstname', ($oneDirigent->getPrenom()));
                        (new Atexo_Xml())->addAttribute($domDocument, $manager, 'lastname', ($oneDirigent->getNom()));
                        (new Atexo_Xml())->addAttribute($domDocument, $manager, 'jobFunction', ($oneDirigent->getQualite()));
                    }
                }
                unset($dirigents);
                // return $domDocument->saveXml();

                if ($oneEntreprise['SITE_INTERNET']) {
                    $webSite = $domDocument->createElement('WebSite');
                    $companie->appendChild($webSite);
                    $site = $domDocument->createCDATASection(($oneEntreprise['SITE_INTERNET']));
                    $webSite->appendChild($site);
                }
                if ($oneEntreprise['description_activite']) {
                    $activityDescription = $domDocument->createElement('ActivityDescription');
                    $companie->appendChild($activityDescription);
                    $activity = $domDocument->createCDATASection(($oneEntreprise['description_activite']));
                    $activityDescription->appendChild($activity);
                }
                if (!$soclePPP) {
                    // ActivityDefenseDomainDescription
                    if ($oneEntreprise['ACTIVITE_DOMAINE_DEFENSE']) {
                        $activityDefenseDomainDescription = $domDocument->createElement('ActivityDefenseDomainDescription');
                        $companie->appendChild($activityDefenseDomainDescription);
                        $activity = $domDocument->createCDATASection(($oneEntreprise['ACTIVITE_DOMAINE_DEFENSE']));
                        $activityDefenseDomainDescription->appendChild($activity);
                    }
                }

                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
                $etpInfosExercices = CommonEntrepriseInfoExercice::retrieveEntrepriseInfoExerciceByIdEtp($oneEntreprise['id'], $connexion);
                if (is_array($etpInfosExercices) && count($etpInfosExercices)) {
                    $fiscalYears = $domDocument->createElement('FiscalYears');
                    $companie->appendChild($fiscalYears);
                    foreach ($etpInfosExercices as $infoExercice) {
                        // Fiscal years
                        if ('' != $infoExercice->getAnneeClotureExercice()) {
                            if ('' != $infoExercice->getAnneeClotureExercice() || null != $infoExercice->getAnneeClotureExercice()) {
                                $fiscalYear = $domDocument->createElement('FiscalYear');
                                $fiscalYears->appendChild($fiscalYear);
                                (new Atexo_Xml())->addAttribute($domDocument, $fiscalYear, 'year', (($infoExercice->getAnneeClotureExercice())));
                                $debutExerciceGlob1 = '1970-01-01';
                                if ('' != $infoExercice->getDebutExerciceGlob() || null != $infoExercice->getDebutExerciceGlob()) {
                                    $debutExerciceGlob1 = $infoExercice->getDebutExerciceGlob();
                                }
                                $finExerciceGlob1 = '1970-01-01';
                                if ($infoExercice->getFinExerciceGlob() || null != $infoExercice->getFinExerciceGlob()) {
                                    $finExerciceGlob1 = $infoExercice->getFinExerciceGlob();
                                }
                                (new Atexo_Xml())->addAttribute($domDocument, $fiscalYear, 'startDate', ($debutExerciceGlob1));
                                (new Atexo_Xml())->addAttribute($domDocument, $fiscalYear, 'endDate', ($finExerciceGlob1));

                                $pme = '';
                                if ('0' == $infoExercice->getPme()) {
                                    $pme = 'NON';
                                } elseif ('1' == $infoExercice->getPme()) {
                                    $pme = 'OUI';
                                } elseif ('' == $infoExercice->getPme()) {
                                    $pme = 'NON_RENSEIGNE';
                                }
                                (new Atexo_Xml())->addAttribute($domDocument, $fiscalYear, 'isSME', ($pme));

                                // Turnover
                                $turnover = $domDocument->createElement('Turnover');
                                $fiscalYear->appendChild($turnover);
                                (new Atexo_Xml())->addAttribute($domDocument, $turnover, 'domesticGoods', ($infoExercice->getVentesGlob()));
                                (new Atexo_Xml())->addAttribute($domDocument, $turnover, 'soldProductionGoods', ($infoExercice->getBiensGlob()));
                                (new Atexo_Xml())->addAttribute($domDocument, $turnover, 'soldProductionServices', ($infoExercice->getServicesGlob()));
                                (new Atexo_Xml())->addAttribute($domDocument, $turnover, 'total', ($infoExercice->getTotalGlob()));

                                //Manpower
                                $manpower = $domDocument->createElement('Manpower');
                                $fiscalYear->appendChild($manpower);
                                (new Atexo_Xml())->addAttribute($domDocument, $manpower, 'average', ($infoExercice->getEffectifMoyen()));
                                (new Atexo_Xml())->addAttribute($domDocument, $manpower, 'includingManagement', ($infoExercice->getEffectifEncadrement()));
                            }
                        }
                    }
                    unset($etpInfosExercices);
                }
                unset($oneEntreprise);
            }
            unset($entreprises);
        }

        if ($defensePortalRegistered) {
            $inscrits = (new Atexo_Entreprise_Inscrit())->retreiveInscritAnnuaireDefense($fromDate);
        }

        // Employee
        if (is_array($inscrits)) {
            foreach ($inscrits as $oneInscrit) {
                $idInscrit = '';
                $idEntreprise = '';

                $employee = $domDocument->createElement('Employee');
                $companies->appendChild($employee);
                if ($soclePPP) { //pour le socle PPP
                    $idInscrit = $oneInscrit->getIdInitial();
                    $entrepriseObject = (new Atexo_Entreprise())->retrieveCompanyBdeById($oneInscrit->getEntrepriseId());
                    if ($entrepriseObject) {
                        $idEntreprise = $entrepriseObject->getIdInitial();
                    }
                    $compteActif = '1';
                    if ($oneInscrit->getBloque()) {
                        $compteActif = '0';
                    }
                    (new Atexo_Xml())->addAttribute($domDocument, $employee, 'title', 'NA'); /// ($oneInscrit->getCivilite()) cette valeur vaut tjr '0'
                    (new Atexo_Xml())->addAttribute($domDocument, $employee, 'compteActif', ($compteActif));
                    (new Atexo_Xml())->addAttribute($domDocument, $employee, 'profilSDM', ($oneInscrit->getProfil())); // (1 et 2)voir la correspondance a un string
                    (new Atexo_Xml())->addAttribute($domDocument, $employee, 'password', (sha1($oneInscrit->getMdp()))); /// Pour le socle PPP

                //Verification du format de l'email
                    $email = '';
                    if (preg_match('/[_\-a-zA-Z0-9\.\+]+@[a-zA-Z0-9](\.?[\-a-zA-Z0-9]*[a-zA-Z0-9])*/', $oneInscrit->getEmail())) {
                        $email = $oneInscrit->getEmail();
                    }
                    (new Atexo_Xml())->addAttribute($domDocument, $employee, 'email', ($email));
                } else {
                    $idInscrit = $oneInscrit->getId();
                    $idEntreprise = $oneInscrit->getEntrepriseId();

                    (new Atexo_Xml())->addAttribute($domDocument, $employee, 'email', ($oneInscrit->getEmail()));
                }
                (new Atexo_Xml())->addAttribute($domDocument, $employee, 'id', ($idInscrit));
                (new Atexo_Xml())->addAttribute($domDocument, $employee, 'companyId', ($idEntreprise));
                (new Atexo_Xml())->addAttribute($domDocument, $employee, 'login', ($oneInscrit->getLogin()));

                if ($soclePPP) { // Pour le scole PPP
                    if ('' != $oneInscrit->getSiret()) {
                        (new Atexo_Xml())->addAttribute($domDocument, $employee, 'nicCode', ($oneInscrit->getSiret()));
                    }
                } else {
                    (new Atexo_Xml())->addAttribute($domDocument, $employee, 'nicCode', ($oneInscrit->getSiret()));
                }
                (new Atexo_Xml())->addAttribute($domDocument, $employee, 'firstname', ($oneInscrit->getPrenom()));
                (new Atexo_Xml())->addAttribute($domDocument, $employee, 'lastname', ($oneInscrit->getNom()));
                (new Atexo_Xml())->addAttribute($domDocument, $employee, 'phone', ($oneInscrit->getTelephone()));
                (new Atexo_Xml())->addAttribute($domDocument, $employee, 'fax', ($oneInscrit->getFax()));
                (new Atexo_Xml())->addAttribute($domDocument, $employee, 'address', ($oneInscrit->getAdresse().' '.$oneInscrit->getAdresse2()));
                (new Atexo_Xml())->addAttribute($domDocument, $employee, 'postalCode', ($oneInscrit->getCodepostal()));
                (new Atexo_Xml())->addAttribute($domDocument, $employee, 'city', ($oneInscrit->getVille()));
                (new Atexo_Xml())->addAttribute($domDocument, $employee, 'country', ($oneInscrit->getPays()));

                unset($oneInscrit);
            }
            unset($inscrits);
        }
        //return $domDocument->schemaValidate(Atexo_Config::getParameter('XSD_INTERFACES_DEFENSE'));
        return $domDocument->saveXml();
    }

    public function getXmlCompanies($companies)
    {
        if ($companies) {
            $defensePortalRegistered = (string) $companies->attributes()->defensePortalRegistered;
            $fromDate = (string) $companies->attributes()->fromDate;
            $xml = $this->getXmlResponseCompany($defensePortalRegistered, $fromDate);
            if ($xml) {
                return $xml;
            }
        }
    }
}
