<?php

namespace Application\Service\Atexo\InterfaceWs;

use Application\Service\Atexo\Atexo_InterfaceWs;
use Application\Service\Atexo\Atexo_Xml;

/**
 * Classe de génération d'un xml qui contient le type et la description de l'erreur.
 *
 * @author Mounir LAMANE <mounir.lamane@atexo.com>
 * @copyright Atexo 2009
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_InterfaceWs_Error extends Atexo_InterfaceWs
{
    public function getXmlError($code, $description)
    {
        $domDocument = self::getXmlTemplate();
        $transaction = $domDocument->documentElement;
        $response = $transaction->getElementsByTagName('Response')->item(0);
        $errors = $domDocument->createElement('Errors');
        $error = $domDocument->createElement('Error');
        $response->appendChild($errors);
        $errors->appendChild($error);
        (new Atexo_Xml())->addAttribute($domDocument, $error, 'code', $code);
        (new Atexo_Xml())->addAttribute($domDocument, $error, 'description', $description);

        return $domDocument->saveXml();
    }
}
