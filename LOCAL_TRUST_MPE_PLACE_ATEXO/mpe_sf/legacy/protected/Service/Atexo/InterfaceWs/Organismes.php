<?php

namespace Application\Service\Atexo\InterfaceWs;

use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_InterfaceWs;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Xml;

/*
 * Created on 30 dec. 2009
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */

class Atexo_InterfaceWs_Organismes extends Atexo_InterfaceWs
{
    public function getXmlResponseOrganismes($organismes, $soclePPP = false)
    {
        $adherentGIP = 'false';
        $siren = '';

        $resulatCSV = (new Atexo_Organismes())->getInfoOrganismFromCSV();
//      $domDocument = self::getXmlTemplate($soclePPP);
        $domDocument = self::getXmlTemplate($soclePPP, true);
        $transaction = $domDocument->documentElement;
        $response = $transaction->getElementsByTagName('Response')->item(0);
        $organisms = $domDocument->createElement('Organisms');
        $response->appendChild($organisms);

        foreach ($organismes as $oneOrg) {
            if ($resulatCSV[$oneOrg->getAcronyme()]) {
                $adherentGIP = $resulatCSV[$oneOrg->getAcronyme()]['adherentGIP'];
                $siren = $resulatCSV[$oneOrg->getAcronyme()]['siren'];
            }

            $organisme = $domDocument->createElement('Organism');
            $organisms->appendChild($organisme);
            (new Atexo_Xml())->addAttribute($domDocument, $organisme, 'organismId', $oneOrg->getIdInitial()); //$organismId

            $organismeTest = '1';
            if ($oneOrg->getActive()) {
                $organismeTest = '0';
            }

            (new Atexo_Xml())->addAttribute($domDocument, $organisme, 'article', $oneOrg->getTypeArticleOrg());
            (new Atexo_Xml())->addAttribute($domDocument, $organisme, 'acronym', $oneOrg->getAcronyme());
            (new Atexo_Xml())->addAttribute($domDocument, $organisme, 'denomination', ($oneOrg->getDenominationOrg()));
            (new Atexo_Xml())->addAttribute($domDocument, $organisme, 'categorieJuridique', $oneOrg->getCategorieInsee());
            (new Atexo_Xml())->addAttribute($domDocument, $organisme, 'organismeTest', $organismeTest);
            //Atexo_Xml::addAttribute($domDocument, $organisme, "adherentGIP",$adherentGIP); // lecture de la valeur 'adherentGIP' appartir du fichier csv
            (new Atexo_Xml())->addAttribute($domDocument, $organisme, 'description', ($oneOrg->getDescriptionOrg()));
            if ('' != $siren) {
                (new Atexo_Xml())->addAttribute($domDocument, $organisme, 'siren', $siren); // lecture de la valeur 'siren' appartir du fichier csv
            } else {
                (new Atexo_Xml())->addAttribute($domDocument, $organisme, 'siren', $oneOrg->getSiren());
            }

            (new Atexo_Xml())->addAttribute($domDocument, $organisme, 'address', ($oneOrg->getAdresse().' '.$oneOrg->getAdresse2()));
            (new Atexo_Xml())->addAttribute($domDocument, $organisme, 'postalCode', $oneOrg->getCp());
            (new Atexo_Xml())->addAttribute($domDocument, $organisme, 'city', ($oneOrg->getVille()));
            //Atexo_Xml::addAttribute($domDocument, $organisme, "canton",($oneOrg->getCanton())); //$cauton

            //Reccuperation de l'email
            $email = $oneOrg->getEmail();
            if ($soclePPP && !preg_match('/[_\-a-zA-Z0-9\.\+]+@[a-zA-Z0-9](\.?[\-a-zA-Z0-9]*[a-zA-Z0-9])*/', $oneOrg->getEmail())) {
                //Si l'adresse email ne satisfait pas l'expression reguliere, l'attribut "email" sera vide
                $email = '';
            }
            (new Atexo_Xml())->addAttribute($domDocument, $organisme, 'email', ($email));
            (new Atexo_Xml())->addAttribute($domDocument, $organisme, 'urlSite', (base64_encode($oneOrg->getUrl()))); //urlSite

            $dirPath = Atexo_Config::getParameter('BASE_ROOT_DIR').'/'.$oneOrg->getAcronyme().Atexo_Config::getParameter('PATH_ORGANISME_IMAGE');

            if (is_dir($dirPath)) {
                $bigLogo = $dirPath.'logo-organisme-grand.jpg';
                $smallLogo = $dirPath.'logo-organisme-petit.jpg';
                if (is_file($smallLogo)) {
                    (new Atexo_Xml())->addAttribute($domDocument, $organisme, 'petitLogo', base64_encode(file_get_contents($smallLogo)));
                }
                if (is_file($bigLogo)) {
                    (new Atexo_Xml())->addAttribute($domDocument, $organisme, 'grandLogo', base64_encode(file_get_contents($bigLogo)));
                }
            }
        }

        return $domDocument->saveXml();
    }
}
