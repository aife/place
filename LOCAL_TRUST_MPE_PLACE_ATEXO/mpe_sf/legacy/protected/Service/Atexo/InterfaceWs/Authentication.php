<?php

namespace Application\Service\Atexo\InterfaceWs;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonAgent;
use Application\Propel\Mpe\CommonInscrit;
use Application\Propel\Mpe\CommonSsoAgent;
use Application\Propel\Mpe\CommonSsoAgentPeer;
use Application\Propel\Mpe\CommonSsoEntreprise;
use Application\Propel\Mpe\CommonSsoEntreprisePeer;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_InterfaceWs;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_Xml;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Inscrit;

/**
 * Classe de.
 *
 * @author Khalid BENAMAR <khalid.benamar@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_InterfaceWs_Authentication extends Atexo_InterfaceWs
{
    public function getXmlByPassword($authenticationPassword, $typeUser)
    {
        $userVo = null;
        if ($authenticationPassword) {
            $login = Atexo_Util::atexoHtmlEntities(utf8_decode(base64_decode((string) ($authenticationPassword->attributes()->login))));
            $mpd = Atexo_Util::atexoHtmlEntities(utf8_decode(base64_decode((string) ($authenticationPassword->attributes()->password))));

            if ('ENTITY' == $typeUser) {
                $agentPhp = new Atexo_Agent();
                $pwd = $agentPhp->getPswAgentAfterVerification($login, $mpd);
                $agentObject = (new Atexo_Agent())->retrieveAgentByLoginAndMdp($login, $pwd);
                if ($agentObject instanceof CommonAgent) {
                    $userVo = new Atexo_InterfaceWs_UserVo();
                    $ssoAgent = $this->generateSsoForAgent($agentObject);
                    $userVo->setIdToken($ssoAgent->getIdSso());
                    $userVo->setValidityDate(Atexo_Util::formateIsoDate($ssoAgent->getDateLastRequest(), 'c'));
                    $userVo->setLogin($agentObject->getLogin());
                    $userVo->setEmail(trim($agentObject->getEmail()));
                    $userVo->setFirstname($agentObject->getPrenom());
                    $userVo->setLastname($agentObject->getNom());
                    $userVo->setType('ENTITY');
                }
            }
            if ('COMPANY' == $typeUser) {
                $inscrit = null;
                $inscritPhp = new Atexo_Entreprise_Inscrit();
                $pwd = $inscritPhp->getPswInscritAfterVerification($inscrit, $login, $mpd);
                $inscritObject = (new Atexo_Entreprise_Inscrit())->retrieveInscritByLoginMdp($login, $pwd);
                if ($inscritObject instanceof CommonInscrit) {
                    $userVo = new Atexo_InterfaceWs_UserVo();
                    $ssoInscrit = $this->generateSsoForInscrit($inscritObject);
                    $userVo->setIdToken($ssoInscrit->getIdSso());
                    $userVo->setValidityDate(Atexo_Util::formateIsoDate($ssoInscrit->getDateLastRequest(), 'c'));
                    $userVo->setLogin($inscritObject->getLogin());
                    $userVo->setEmail(trim($inscritObject->getEmail()));
                    $userVo->setFirstname($inscritObject->getPrenom());
                    $userVo->setLastname($inscritObject->getNom());
                    $userVo->setType('COMPANY');
                }
            }
            $xmlResponse = $this->generateResponseAuthentication($userVo);
            $res = Atexo_Util::validateXml($xmlResponse, Atexo_Config::getParameter('XSD_INTERFACES_DEFENSE'), true);
            if (true == $res) {
                return $xmlResponse;
            } else {
                return (new Atexo_InterfaceWs_Error())->getXmlError('TECHNICAL_ERROR', $res);
            }
        }
    }

    public function generateResponseAuthentication($userVo)
    {
        $domDocument = self::getXmlTemplate();
        if ($userVo instanceof Atexo_InterfaceWs_UserVo) {
            $transaction = $domDocument->documentElement;
            $response = $transaction->getElementsByTagName('Response')->item(0);
            $authentication = $domDocument->createElement('Authentication');
            $token = $domDocument->createElement('Token');
            (new Atexo_Xml())->addAttribute($domDocument, $token, 'id', $userVo->getIdToken());
            (new Atexo_Xml())->addAttribute($domDocument, $token, 'validityDate', $userVo->getValidityDate());
            $authentication->appendChild($token);
            $response->appendChild($authentication);

            if ($userVo->getLogin() && $userVo->getEmail() && $userVo->getFirstname() && $userVo->getLastname() && $userVo->getType()) {
                $user = $domDocument->createElement('User');
                $authentication->appendChild($user);
                (new Atexo_Xml())->addAttribute($domDocument, $user, 'login', utf8_encode($userVo->getLogin()));
                (new Atexo_Xml())->addAttribute($domDocument, $user, 'email', utf8_encode($userVo->getEmail()));
                (new Atexo_Xml())->addAttribute($domDocument, $user, 'firstname', utf8_encode($userVo->getFirstname()));
                (new Atexo_Xml())->addAttribute($domDocument, $user, 'lastname', utf8_encode($userVo->getLastname()));
                (new Atexo_Xml())->addAttribute($domDocument, $user, 'type', $userVo->getType());
            }
        }

        return $domDocument->saveXml();
    }

    /***
    * générer un sso pour l'agent connecté
    * @param CommonAgent $agentObject
    * @return  l'objet CommonSsoAgent
    */
    public function generateSsoForAgent($agentObject)
    {
        if ($agentObject) {
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $ssoAgent = new CommonSsoAgent();
            $sso = uniqid();
            $ssoAgent->setIdSso($sso);
            $ssoAgent->setIdAgent($agentObject->getId());
            $ssoAgent->setOrganisme($agentObject->getOrganisme());
            $ssoAgent->setServiceId($agentObject->getServiceId());
            $ssoAgent->setDateConnexion(date('Y-m-d H:i:s'));
            $ssoAgent->setDateLastRequest(Atexo_Util::dateDansFutur(date('Y-m-d H:i:s'), Atexo_Config::getParameter('NBR_HEURE_MAINTENANCE_CNX')));
            $ssoAgent->save($connexionCom);

            return $ssoAgent;
        }
    }

    /***
    * générer un sso pour l'inscrit connecté
    * @param CommonInscrit $inscritObject
    * @return  l'objet CommonSsoEntreprise
    */
    public function generateSsoForInscrit($inscritObject)
    {
        if ($inscritObject) {
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $ssoInscrit = new CommonSsoEntreprise();
            $sso = uniqid();
            $ssoInscrit->setIdSso($sso);
            $ssoInscrit->setIdInscrit($inscritObject->getId());
            $ssoInscrit->setDateConnexion(date('Y-m-d H:i:s'));
            $ssoInscrit->setDateLastRequest(Atexo_Util::dateDansFutur(date('Y-m-d H:i:s'), Atexo_Config::getParameter('NBR_HEURE_MAINTENANCE_CNX')));
            $ssoInscrit->save($connexionCom);

            return $ssoInscrit;
        }
    }

    public function getXmlByToken($authenticationToken, $typeUser)
    {
        $userVo = null;
        if ($authenticationToken) {
            $sso = utf8_decode((string) ($authenticationToken->attributes()->id));

            if ('ENTITY' == $typeUser) {
                $userVo = new Atexo_InterfaceWs_UserVo();
                $ssoAgentObject = (new Atexo_Agent())->retrieveSsoAgentBySso($sso);
                if ($ssoAgentObject instanceof CommonSsoAgent) {
                    //test de la date validité de la requette
                    $userVo->setIdToken($ssoAgentObject->getIdSso());
                    $userVo->setValidityDate(Atexo_Util::formateIsoDate($ssoAgentObject->getDateLastRequest(), 'c'));
                    $agentObject = (new Atexo_Agent())->retrieveAgentById($ssoAgentObject->getIdAgent());
                    if ($agentObject instanceof CommonAgent) {
                        $userVo->setLogin($agentObject->getLogin());
                        $userVo->setEmail(trim($agentObject->getEmail()));
                        $userVo->setFirstname($agentObject->getPrenom());
                        $userVo->setLastname($agentObject->getNom());
                        $userVo->setType('ENTITY');
                    }
                } else { // remplir le UserVo au cas ou les paramètres fournis sont invalides pour retourner juste l'element 'Token' dans la reponse
                    self::setUserVoIfParametersInvalid($authenticationToken, $userVo);
                }
            }
            if ('COMPANY' == $typeUser) {
                $userVo = new Atexo_InterfaceWs_UserVo();
                $ssoEntrepriseObject = (new Atexo_Entreprise_Inscrit())->retrieveSsoEntrepriseBySso($sso);
                if ($ssoEntrepriseObject instanceof CommonSsoEntreprise) {
                    //test de la date validité de la requette
                    $userVo->setIdToken($ssoEntrepriseObject->getIdSso());
                    $userVo->setValidityDate(Atexo_Util::formateIsoDate($ssoEntrepriseObject->getDateLastRequest(), 'c'));
                    $inscritObject = (new Atexo_Entreprise_Inscrit())->retrieveInscritById($ssoEntrepriseObject->getIdInscrit());
                    if ($inscritObject instanceof CommonInscrit) {
                        $userVo->setLogin($inscritObject->getLogin());
                        $userVo->setEmail(trim($inscritObject->getEmail()));
                        $userVo->setFirstname($inscritObject->getPrenom());
                        $userVo->setLastname($inscritObject->getNom());
                        $userVo->setType('COMPANY');
                    }
                } else { // remplir le UserVo au cas ou les paramètres fournis sont invalides pour retourner juste l'element 'Token' dans la reponse
                    self::setUserVoIfParametersInvalid($authenticationToken, $userVo);
                }
            }
            $xmlResponse = $this->generateResponseAuthentication($userVo);
            $res = Atexo_Util::validateXml($xmlResponse, Atexo_Config::getParameter('XSD_INTERFACES_DEFENSE'), true);
            if (true == $res) {
                return $xmlResponse;
            } else {
                return (new Atexo_InterfaceWs_Error())->getXmlError('TECHNICAL_ERROR', $res);
            }
        }
    }

    public function setUserVoIfParametersInvalid($authenticationToken, $userVo)
    {
        $userVo->setIdToken($authenticationToken->attributes()->id);
        $userVo->setValidityDate($authenticationToken->attributes()->validityDate);

        return $userVo;
    }

    public function getSsoInscritByIdSso($sso)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonSsoEntreprisePeer::ID_SSO, $sso);
        $c->add(CommonSsoEntreprisePeer::DATE_LAST_REQUEST, date('Y-m-d H:i'), Criteria::GREATER_THAN);
        $ssoInscrit = CommonSsoEntreprisePeer::doSelectOne($c, $connexionCom);

        return $ssoInscrit;
    }

    public function getSsoAgentByIdSso($sso)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonSsoAgentPeer::ID_SSO, $sso);
        $c->add(CommonSsoAgentPeer::DATE_LAST_REQUEST, date('Y-m-d H:i'), Criteria::GREATER_THAN);
        $ssoAgent = CommonSsoAgentPeer::doSelectOne($c, $connexionCom);

        return $ssoAgent;
    }
}
