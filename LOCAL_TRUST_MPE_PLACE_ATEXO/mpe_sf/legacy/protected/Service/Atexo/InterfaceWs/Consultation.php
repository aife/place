<?php

namespace Application\Service\Atexo\InterfaceWs;

use Application\Propel\Mpe\CommonCategorieConsultation;
use Application\Propel\Mpe\CommonCategorieLot;
use Application\Propel\Mpe\CommonTypeAvis;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_InterfaceWs;
use Application\Service\Atexo\Atexo_MarchePublicSimplifie;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_Xml;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Category;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureType;
use Application\Service\Atexo\Consultation\Atexo_Consultation_TypeAvis;
use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2;
use Application\Service\Atexo\Referentiel\Atexo_Referentiel_Referentiel;
use Application\Service\Atexo\Referentiel\Atexo_Referentiel_ReferentielVo;
use AtexoDume\Dto\Lot;
use Exception;

/**
 * Classe de.
 *
 * @author Khalid BENAMAR <khalid.benamar@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_InterfaceWs_Consultation extends Atexo_InterfaceWs
{
    public function generateResponseTender($consultations, $annoncesMarches = false, $calledFromAgent = false, $calledForMPS = false)
    {
        try {
            $domDocument = self::getXmlTemplate();
            $transaction = $domDocument->documentElement;
            $response = $transaction->getElementsByTagName('Response')->item(0);
            // return count($consultations);
            if (is_array($consultations) && count($consultations) > 0) {
                $index = 1;
                foreach ($consultations as $oneConsultation) {
                    if ($index > Atexo_Config::getParameter('NOMBRE_CONSULTATION_IN_RESPONSE') && !$annoncesMarches) {
                        continue;
                    }
                    $oneTender = $domDocument->createElement('Tender');
                    $response->appendChild($oneTender);
                    $categories = Atexo_Consultation_Category::retrieveCategories(true, false);
                    $organisme = '';
                    if ($calledFromAgent) { // Mettre l'organisme en session pour eviter de planter les connexion vers la db org
                        $organisme = Atexo_CurrentUser::getCurrentOrganism();
                    } else {
                        $organisme = $oneConsultation->getOrganisme();
                    }
                    (new Atexo_Xml())->addAttribute($domDocument, $oneTender, 'organism', Atexo_Util::toUtf8((($organisme))));

                    $entiteAchat = '';
                    $organismeObjet = Atexo_Organismes::retrieveOrganismeByAcronyme($organisme);

                    //si jamais l'information est présente en base de données alors on ne la calcule pas
                    if ($oneConsultation->getOrgDenomination()) {
                        $entitePublique = $oneConsultation->getOrgDenomination();
                    } else {
                        //cette variable $entitePublique sert à remplir le champ org_denomination dans ANM
                        //par défaut on prend les informations du pouvoir adjudicateur à savoir l'organisme
                        if ($organismeObjet) {
                            $entitePublique = $organismeObjet->getDenominationOrg();
                        }
                        //cas organisation dé-centralisée  : on prend les informations sans le service d'achat si existantes sinon celle de l'organisme
                        if (!Atexo_Module::isEnabled('OrganisationCentralisee', $organisme)) {
                            $service = (new Atexo_EntityPurchase())->getEntityById($oneConsultation->getServiceId(), $organisme);
                            if ($service && $organismeObjet) {
                                $entitePublique .= ' - '.(new Atexo_EntityPurchase())->getEntityLibelle($oneConsultation->getServiceId(), $organisme);
                                $cpService = $service->getCp();
                                $villeService = $service->getVille();
                                if ((0 !== $cpService || '0' !== $cpService) && ('' != $villeService)) {
                                    $entitePublique .= ' ('.$cpService.' - '.$villeService.')';
                                } else {
                                    $entitePublique .= ' ('.$organismeObjet->getCp().' - '.$organismeObjet->getVille().')';
                                }
                            }
                        } else {
                            $entitePublique .= ' ('.$organismeObjet->getCp().' - '.$organismeObjet->getVille().')';
                        }
                    }

                    if ($organismeObjet) {
                        $entiteAchat = $organismeObjet->getSigle();
                    }

                    if (0 == $oneConsultation->getServiceId()) {
                        $entiteAchat .= ' - '.$organismeObjet->getDenominationOrg();
                    } else {
                        if ((new Atexo_EntityPurchase())->getEntityPath($oneConsultation->getId(), $organisme)) {
                            $entiteAchat .= ' / '.(new Atexo_EntityPurchase())->getEntityPath($oneConsultation->getId(), $organisme);
                        }
                        if ((new Atexo_EntityPurchase())->getEntityLibelle($oneConsultation->getServiceId(), $organisme)) {
                            $entiteAchat .= ' - '.(new Atexo_EntityPurchase())->getEntityLibelle($oneConsultation->getServiceId(), $organisme);
                        }
                    }

                    if (Atexo_Module::isEnabled('AnnoncesMarches')) {
                        $entitePublique = $oneConsultation->getOrgDenomination();
                        $entiteAchat = $oneConsultation->getOrgDenomination();
                    }
                    (new Atexo_Xml())->addAttribute($domDocument, $oneTender, 'service', Atexo_Util::toUtf8((($entiteAchat))));
                    (new Atexo_Xml())->addAttribute($domDocument, $oneTender, 'entitePublique', Atexo_Util::toUtf8((($entitePublique))));
                    if ($annoncesMarches) {
                        $orgObject = Atexo_Organismes::retrieveOrganismeByAcronyme(Atexo_Util::toUtf8(($organisme)));
                        (new Atexo_Xml())->addAttribute($domDocument, $oneTender, 'organismeName', Atexo_Util::toUtf8(($orgObject->getDenominationOrg())));
                        (new Atexo_Xml())->addAttribute($domDocument, $oneTender, 'referenceConsultation', Atexo_Util::toUtf8(($oneConsultation->getId())));
                        (new Atexo_Xml())->addAttribute($domDocument, $oneTender, 'dateMiseOnLigneCalcule', Atexo_Util::toUtf8($oneConsultation->getDateMiseEnLigneCalcule()));
                        (new Atexo_Xml())->addAttribute($domDocument, $oneTender, 'electronicResponse', Atexo_Util::toUtf8($oneConsultation->getAutoriserReponseElectronique()));
                        (new Atexo_Xml())->addAttribute($domDocument, $oneTender, 'digitalSignature', Atexo_Util::toUtf8($oneConsultation->getSignatureOffre()));
                        (new Atexo_Xml())->addAttribute($domDocument, $oneTender, 'electronicResponseObligatoire', Atexo_Util::toUtf8($oneConsultation->getReponseObligatoire()));

                        $urlLogo = Atexo_Config::getParameter('BASE_ROOT_DIR').$organisme.'/images/logo-organisme-petit.jpg';
                        if (file_exists($urlLogo)) {
                            $urlLogo = Atexo_Config::getParameter('PF_URL_REFERENCE').'themes/'.$organisme.'/images/logo-organisme-petit.jpg';
                        } else {
                            $urlLogo = '';
                        }
                        (new Atexo_Xml())->addAttribute($domDocument, $oneTender, 'urlLogo', Atexo_Util::toUtf8($urlLogo));
                    }
                    if ($calledForMPS) {
                        (new Atexo_Xml())->addAttribute($domDocument, $oneTender, 'buyerEmail', Atexo_Util::strToUtf8Xml(
                            (new Atexo_Agent())->getMailAgentById($oneConsultation->getIdCreateur())
                        ));
                        (new Atexo_Xml())->addAttribute($domDocument, $oneTender, 'appelOffreId', Atexo_Util::toUtf8(
                            (new Atexo_MarchePublicSimplifie())->getAppelOffreId($oneConsultation->getReferenceUtilisateur(), $oneConsultation->getId(), $oneConsultation->getOrganisme())
                        ));
                    }
                    (new Atexo_Xml())->addAttribute($domDocument, $oneTender, 'reference', Atexo_Util::toUtf8(($oneConsultation->getReferenceUtilisateur())));
                    (new Atexo_Xml())->addAttribute($domDocument, $oneTender, 'referenceId', Atexo_Util::toUtf8($oneConsultation->getId()));
                    $typeAnnonce = (new Atexo_Consultation_TypeAvis())->retrieveTypeAnnonce(Atexo_Util::toUtf8(($oneConsultation->getIdTypeAvis())));
                    if ($typeAnnonce instanceof CommonTypeAvis) {
                        (new Atexo_Xml())->addAttribute($domDocument, $oneTender, 'type', Atexo_Util::toUtf8($typeAnnonce->getAbbreviation()));
                    }
                    (new Atexo_Xml())->addAttribute($domDocument, $oneTender, 'procedureType', Atexo_Util::toUtf8(((new Atexo_Consultation_ProcedureType())->getAbbreviationAnmTypeProcedureById($oneConsultation->getIdTypeProcedure()))));
                    // TODO cette partie est pour traiter des consultations migrées dans ebourgogne
                    $libelleCategory = $oneConsultation->getLibelleCategorieConsultation();
                    if ('' == $libelleCategory) {
                        $explodeCategorie = explode('.', $oneConsultation->getCategorie());
                        $idCategorieConsultation = $explodeCategorie[0];
                        $libelleCategory = match ($idCategorieConsultation) {
                            '1' => 'Travaux',
                            '2' => 'Fournitures',
                            '3' => 'Services',
                            default => '',
                        };
                    }
                    // TODO cette partie est pour traiter des consultations migrées dans ebourgogne
                    (new Atexo_Xml())->addAttribute($domDocument, $oneTender, 'category', Atexo_Util::toUtf8(($libelleCategory)));
                    $cpv = '';

                    if ('' != $oneConsultation->getCodeCpv1()) {
                        $cpv .= $oneConsultation->getCodeCpv1().',';
                    }
                    $arrayCodeCpvSec = explode('#', $oneConsultation->getCodeCpv2());
                    if (is_array($arrayCodeCpvSec) && $arrayCodeCpvSec[1]) {
                        foreach ($arrayCodeCpvSec as $codeCpvSec) {
                            if ($codeCpvSec != $arrayCodeCpvSec[0]) {
                                $cpv .= $codeCpvSec.',';
                            }
                        }
                    }

                    (new Atexo_Xml())->addAttribute($domDocument, $oneTender, 'cpv', Atexo_Util::toUtf8(substr($cpv, 0, -1)));
                    // la gestion des LtReferentiel
                    $ltReferentiels = (new Atexo_Referentiel_Referentiel())->getAllLtReferentielByLotouConsulation($oneConsultation->getOrganisme(), $oneConsultation->getId(), null, 1);
                    $prefix = Atexo_Config::getParameter('PREFIX_LTREFERENTIEL_WS');
                    if (isset($ltReferentiels)) {
                        foreach ($ltReferentiels as $ref) {
                            if ($ref['id']) {
                                $ltRefeAttribut = $ref['code_libelle'];
                                $valeurAttribut = $ref['valeur_Principale_Lt_Referentiel'].$ref['valeur_Secondaire_Lt_Referentiel'];
                                $valeurAttribut = str_replace('#', ',', $valeurAttribut);
                                $valeurAttribut = ltrim($valeurAttribut, ',');
                                $valeurAttribut = rtrim($valeurAttribut, ',');
                                (new Atexo_Xml())->addAttribute($domDocument, $oneTender, $prefix.(new Atexo_Referentiel_Referentiel())->getNomAttribue($ltRefeAttribut), Atexo_Util::toUtf8($valeurAttribut));
                            } else {
                                (new Atexo_Xml())->addAttribute($domDocument, $oneTender, $prefix.(new Atexo_Referentiel_Referentiel())->getNomAttribue($ref['code_libelle']), '');
                            }
                        }
                    }
                    (new Atexo_Xml())->addAttribute($domDocument, $oneTender, 'closureDate', Atexo_Util::toUtf8(Atexo_Util::formateIsoDate($oneConsultation->getDatefin(), 'c')));
                    if (! is_null($oneConsultation->getDateMiseEnLigneCalcule())) {
                        (new Atexo_Xml())->addAttribute($domDocument, $oneTender, 'dateOnLineCalculated', Atexo_Util::toUtf8(Atexo_Util::formateIsoDate($oneConsultation->getDateMiseEnLigneCalcule(), 'c')));
                    }
                    if ('' != $oneConsultation->getLieuExecution()) {
                        $arrayId = explode(',', $oneConsultation->getLieuExecution());
                        $geoN2 = Atexo_Geolocalisation_GeolocalisationN2::retrieveGeolocalisationN2byIds($arrayId);
                        $codeSACD = '';
                        if ($geoN2) {
                            foreach ($geoN2 as $oneN2) {
                                $codeSACD .= $oneN2->getDenomination2().',';
                            }
                            (new Atexo_Xml())->addAttribute($domDocument, $oneTender, 'deliveryPlace', Atexo_Util::toUtf8(substr($codeSACD, 0, -1)));
                        }
                    }
                    $oneTitle = $domDocument->createElement('Title');
                    if ($oneConsultation->getIntitule()) {
                        $title = $domDocument->createTextNode(Atexo_Util::toUtf8((($oneConsultation->getIntitule()))));
                        $oneTitle->appendChild($title);
                    }
                    $oneTender->appendChild($oneTitle);
                    $oneSubject = $domDocument->createElement('Subject');
                    if ($oneConsultation->getObjet()) {
                        $subject = $domDocument->createTextNode(Atexo_Util::toUtf8((($oneConsultation->getObjet()))));
                        $oneSubject->appendChild($subject);
                    }
                    $oneTender->appendChild($oneSubject);

                    if (Atexo_Module::isEnabled('AnnoncesMarches') && $oneConsultation->getConsultationExterne()) {
                        if ($oneConsultation->getUrlConsultationExterne()) {
                            $url = $oneConsultation->getUrlConsultationExterne();
                            $urlAvisPub = '';
                        } elseif ($oneConsultation->getUrlConsultationAvisPub()) {
                            $url = '';
                            $urlAvisPub = $oneConsultation->getUrlConsultationAvisPub();
                        }
                    } else {
                        if ($oneConsultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION')) {
                            $url = (new Atexo_Consultation())->getUrlAccesDirectProcPubliciteOuverte($oneConsultation->getId(), $organisme);
                        } elseif ($oneConsultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_ATTRIBUTION')) {
                            $url = (new Atexo_Consultation())->getUrlAccesDirectAnnonce('AvisAttribution', $oneConsultation->getId(), $organisme);
                        } elseif ($oneConsultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_INFORMATION')) {
                            $url = (new Atexo_Consultation())->getUrlAccesDirectAnnonce('AvisInformation', $oneConsultation->getId(), $organisme);
                        }
                    }
                    $oneUrl = $domDocument->createElement('Url');
                    $urlCDATA = $domDocument->createCDATASection(Atexo_Util::toUtf8($url));
                    $oneUrl->appendChild($urlCDATA);
                    $oneTender->appendChild($oneUrl);

                    $oneUrlAvisPub = $domDocument->createElement('UrlAvisPub');
                    $urlCDATAAvisPub = $domDocument->createCDATASection(Atexo_Util::toUtf8($urlAvisPub));
                    $oneUrlAvisPub->appendChild($urlCDATAAvisPub);
                    $oneTender->appendChild($oneUrlAvisPub);
                    //clauses envi /sociales
                    self::insertClausesEnviSociales($domDocument, $oneTender, $oneConsultation);
                    if (Atexo_Module::isEnabled('gestionOperations', $oneConsultation->getOrganisme())) {
                        (new Atexo_Xml())->addAttribute($domDocument, $oneTender, 'codeOperation', $oneConsultation->getCodeOperation());
                    }
                    $lots = $oneConsultation->getAllLots();
                    if (is_array($lots) && count($lots) > 0) {
                        $oneLots = $domDocument->createElement('Lots');
                        $oneTender->appendChild($oneLots);
                        foreach ($lots as $oneLot) {
                            $lotElement = $domDocument->createElement('Lot');
                            $oneLots->appendChild($lotElement);
                            //$oneLot = new CommonCategorieLot();
                            $oneTitleLot = $domDocument->createElement('Title');
                            $lotElement->appendChild($oneTitleLot);
                            $titleLot = $domDocument->createTextNode(Atexo_Util::toUtf8((($oneLot->getDescription()))));
                            $oneTitleLot->appendChild($titleLot);

                            //clauses envi /sociales
                            self::insertClausesEnviSociales($domDocument, $lotElement, $oneLot);
                            $oneSubjectLot = $domDocument->createElement('Subject');
                            $lotElement->appendChild($oneSubjectLot);
                            $subjectLot = $domDocument->createTextNode(Atexo_Util::toUtf8((($oneLot->getDescriptionDetail()))));
                            $oneSubjectLot->appendChild($subjectLot);

                            (new Atexo_Xml())->addAttribute($domDocument, $lotElement, 'number', Atexo_Util::toUtf8($oneLot->getLot()));
                            // TODO cette partie est pour traiter des consultations migrées dans ebourgogne
                            if (!in_array($idCategoryLot, ['1', '2', '3'])) {
                                $explodeidCategorieLot = explode('.', $idCategoryLot);
                                $idCategorieLotConsultation = $explodeidCategorieLot[0];
                                $idCategoryLot = match ($idCategorieLotConsultation) {
                                    '1' => '1',
                                    '2' => '2',
                                    '3' => '3',
                                    default => '',
                                };
                            }
                            (new Atexo_Xml())->addAttribute($domDocument, $lotElement, 'category', Atexo_Util::toUtf8(($categories[$idCategoryLot])));
                            $cpv = '';
                            if ('' != $oneLot->getCodeCpv1()) {
                                $cpv .= $oneLot->getCodeCpv1().',';
                            }
                            $arrayCodeCpvSec = explode('#', $oneLot->getCodeCpv2());
                            if (is_array($arrayCodeCpvSec) && $arrayCodeCpvSec[1]) {
                                foreach ($arrayCodeCpvSec as $codeCpvSec) {
                                    if ($codeCpvSec != $arrayCodeCpvSec[0]) {
                                        $cpv .= $codeCpvSec.',';
                                    }
                                }
                            }
                            (new Atexo_Xml())->addAttribute($domDocument, $lotElement, 'cpv', Atexo_Util::toUtf8(substr($cpv, 0, -1)));
                            // la gestion des LtReferentiel pour les Lot
                            $ltReferentielsLot = (new Atexo_Referentiel_Referentiel())->getAllLtReferentielByLotouConsulation($oneConsultation->getOrganisme(), $oneConsultation->getId(), 1, null, $oneLot->getLot());
                            if (isset($ltReferentielsLot)) {
                                foreach ($ltReferentielsLot as $ref) {
                                    if ($ref['id']) {
                                        $ltRefeAttribut = $ref['code_libelle'];
                                        $valeurAttribut = $ref['valeur_Principale_Lt_Referentiel'].$ref['valeur_Secondaire_Lt_Referentiel'];
                                        $valeurAttribut = str_replace('#', ',', $valeurAttribut);
                                        $valeurAttribut = ltrim($valeurAttribut, ',');
                                        $valeurAttribut = rtrim($valeurAttribut, ',');
                                        (new Atexo_Xml())->addAttribute($domDocument, $lotElement, $prefix.(new Atexo_Referentiel_Referentiel())->getNomAttribue($ltRefeAttribut), Atexo_Util::toUtf8($valeurAttribut));
                                    } else {
                                        (new Atexo_Xml())->addAttribute($domDocument, $lotElement, $prefix.(new Atexo_Referentiel_Referentiel())->getNomAttribue($ref['code_libelle']), '');
                                    }
                                }
                            }
                        }
                    }
                    ++$index;
                }

                return $domDocument->saveXml();
            } else {
                return false;
            }
        } catch (Exception $e) {
            $errorsWs = new Atexo_InterfaceWs_Error();

            return $errorsWs->getXmlError('TECHNICAL_ERROR', $e->getMessage());
        }
    }

    public function getXmlTender($tender, $annoncesMarches = false)
    {
        try {
            $calledFromAgent = false;
            $calledForMPS = false;

            $criteriaVo = new Atexo_Consultation_CriteriaVo();
            $criteriaVo->setEtatConsultation(Atexo_Config::getParameter('STATUS_CONSULTATION'));
            $criteriaVo->setcalledFromPortail(true);
            //$criteriaVo->setSansCritereActive(true);
            $criteriaVo->setTypeAcces(Atexo_Config::getParameter('TYPE_PROCEDURE_PUBLICITE'));
            if ((string) $tender->attributes()->type) {
                $typeAvis = (new Atexo_Consultation_TypeAvis())->retreiveTypeAvisByAbreviation((string) $tender->attributes()->type);
                if ($typeAvis[0] instanceof CommonTypeAvis) {
                    $criteriaVo->setIdTypeAvis($typeAvis[0]->getId());
                }
            }
            if ($tender->attributes()->organism) {
                $criteriaVo->setAcronymeOrganisme((string) $tender->attributes()->organism);
            }
            if ($tender->attributes()->reference) {
                $criteriaVo->setReferenceConsultation((string) $tender->attributes()->reference);
            }
            if ($tender->attributes()->procedureType) {
                $criteriaVo->setIdTypeProcedure((new Atexo_Consultation_ProcedureType())->retrieveTypeProcedureObjectByAbreviationAnm((string) $tender->attributes()->procedureType, true));
            }
            if ($tender->attributes()->category) {
                $criteriaVo->setCategorieConsultation($this->retrieveIdCategoryByLibelle((string) $tender->attributes()->category));
            }
            if ($tender->attributes()->minClosureDate) {
                $date = Atexo_Util::iso860TimeToIso((string) $tender->attributes()->minClosureDate);
                $criteriaVo->setDateFinStart($date);
            }
            if ($tender->attributes()->maxClosureDate) {
                $date = Atexo_Util::iso860TimeToIso((string) $tender->attributes()->maxClosureDate);
                $criteriaVo->setDateFinEnd($date);
            }
            if ($tender->attributes()->enCours) {
                $criteriaVo->setDateFinStart(date('d/m/Y'));
            }
            if ($tender->attributes()->deliveryPlace) {
                $arrayCode = explode(',', (string) $tender->attributes()->deliveryPlace);
                $str = '';
                foreach ($arrayCode as $oneCode) {
                    $oneGeo = (new Atexo_Geolocalisation_GeolocalisationN2())->retrieveGeoN2ByDenomination2($oneCode);
                    if ($oneGeo) {
                        $str .= $oneGeo->getId().',';
                    }
                }
                $criteriaVo->setLieuxexecution(substr($str, 0, -1));
            }
            if ($tender->attributes()->cpv) {
                $arrayCpv = explode(',', (string) $tender->attributes()->cpv);
                $criteriaVo->setIdCodeCpv1($arrayCpv[0]);
                $criteriaVo->setIdCodeCpv2($arrayCpv[1]);
                $criteriaVo->setIdCodeCpv3($arrayCpv[2]);
                $criteriaVo->setIdCodeCpv4($arrayCpv[3]);
            }
            if ($tender->attributes()->keywords) {
                $criteriaVo->setKeyWordAdvancedSearch((string) $tender->attributes()->keywords);
            }
            //Les clauses achat responsable
            if ($tender->attributes()->clauseSociale) {
                $criteriaVo->setClauseSociale('1');
            }
            if ($tender->attributes()->atelierProtege) {
                $criteriaVo->setAtelierProtege('1');
            }
            if ($tender->attributes()->clauseEnv) {
                $criteriaVo->setClauseEnv('1');
            }

            //l'attribut Marche Public Simplifie
            if ($tender->attributes()->mps) {
                $criteriaVo->setMps('1');
                $calledForMPS = true;
            }

            // les LtRefrentiel
            $allLtRef = (new Atexo_Referentiel_Referentiel())->getAllLtReferentiel();
            $tabRefVo = null;
            $prefix = Atexo_Config::getParameter('PREFIX_LTREFERENTIEL_WS');
            foreach ($allLtRef as $ltRef) {
                $balise = $prefix.(new Atexo_Referentiel_Referentiel())->getNomAttribue($ltRef->getCodeLibelle());
                $value = utf8_decode((string) $tender->attributes()->$balise);
                if ($value && '' != $value && 'NULL' != $value) {
                    $val = str_replace(',', '#', $value);
                    $value = '#'.$val.'#';
                    $refVo = new Atexo_Referentiel_ReferentielVo();
                    $refVo->setValueForReferentielVo($ltRef->getId(), '', $value, $ltRef->getTypeSearch());
                    $tabRefVo[$ltRef->getId()] = $refVo;
                }
            }

            $criteriaVo->setReferentielVo($tabRefVo);
            if (1 == Atexo_Config::getParameter('EXCLURE_CONSULTATION_EXTERNE')) {
                $criteriaVo->setExclureConsultationExterne('1');
            }
            //return $criteriaVo;
            $consultationArray = (new Atexo_Consultation())->search($criteriaVo);
            // print_r($consultationArray);exit;
            $xml = self::generateResponseTender($consultationArray, $annoncesMarches, $calledFromAgent, $calledForMPS);
            if ($xml) {
                return $xml;
            } else {
                return false;
            }
        } catch (Exception) {
            return 'TECHNICAL_ERROR';
        }
    }

    public function getCorrespondanceCategorieConsultation($codeSACD, $response = false)
    {
        $arrayCode = ['Travaux' => '1', 'Fournitures' => '2', 'Services' => '3'];
        foreach ($arrayCode as $oneKey => $oneCode) {
            if ($response) {
                if ($oneCode == $codeSACD) {
                    return $oneKey;
                }
            } else {
                if ($oneKey == $codeSACD) {
                    //echo $oneCode;exit;
                    return $oneCode;
                }
            }
        }
    }

    public function retrieveIdCategoryByLibelle($libelle)
    {
        $category = (new Atexo_Consultation_Category())->retrieveCategoryObjectByLibelle($libelle);
        if ($category instanceof CommonCategorieConsultation) {
            return $category->getId();
        } else {
            return false;
        }
    }

    /*
     * Permet l'insertion des clauses envirementales et sociales
     */
    public function insertClausesEnviSociales(&$domDocument, &$oneTender, $objet)
    {
        if (Atexo_Module::isEnabled('ConsultationClause')) {
            $clauseSocialeEnv = $domDocument->createElement('Clauses_Sociales_Env');
            $oneTender->appendChild($clauseSocialeEnv);
            (new Atexo_Xml())->addAttribute($domDocument, $clauseSocialeEnv, 'socialeConditionExecution', $objet->getClauseSocialeConditionExecution());
            (new Atexo_Xml())->addAttribute($domDocument, $clauseSocialeEnv, 'socialeInsertion', $objet->getClauseSocialeInsertion());
            (new Atexo_Xml())->addAttribute($domDocument, $clauseSocialeEnv, 'socialeAteliersProteges', $objet->getClauseSocialeAteliersProteges());
            (new Atexo_Xml())->addAttribute($domDocument, $clauseSocialeEnv, 'socialeSiae', $objet->getClauseSocialeSiae());
            (new Atexo_Xml())->addAttribute($domDocument, $clauseSocialeEnv, 'socialeEess', $objet->getClauseSocialeEss());
            (new Atexo_Xml())->addAttribute($domDocument, $clauseSocialeEnv, 'envSpecsTechniques', $objet->getClauseEnvSpecsTechniques());
            (new Atexo_Xml())->addAttribute($domDocument, $clauseSocialeEnv, 'envCondExecution', $objet->getClauseEnvCondExecution());
            (new Atexo_Xml())->addAttribute($domDocument, $clauseSocialeEnv, 'envCriteresSelect', $objet->getClauseEnvCriteresSelect());
        }
    }
}
