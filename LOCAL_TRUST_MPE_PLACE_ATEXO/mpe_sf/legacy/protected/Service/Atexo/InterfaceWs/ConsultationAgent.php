<?php

namespace Application\Service\Atexo\InterfaceWs;

use Application\Propel\Mpe\CommonTypeAvis;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_UserVo;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureType;
use Application\Service\Atexo\Consultation\Atexo_Consultation_TypeAvis;
use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2;
use Exception;
use Prado\Prado;

/**
 * Classe de.
 *
 * @author Khalid BENAMAR <khalid.benamar@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_InterfaceWs_ConsultationAgent extends Atexo_InterfaceWs_Consultation
{
    public function getXmlTenderAgent($tender)
    {
        try {
            //  Authentification de l'agent par le jeton sso pour mettre les habilitation en session

            $sso = (string) $tender->attributes()->sso;
            if ($sso) {
                $agent = (new Atexo_Agent())->retrieveAgentBySso($sso);
                $userVo = new Atexo_UserVo();
                $userVo->setLogin($agent['login']);
                $userVo->setPassword($agent['mdp']);
                $userVo->setType('agent');
                /*if ($agent['login'] && $agent['mdp']){
                    if (Prado::getApplication()->getModule("auth")->login($userVo, null)) {
                    }
                }*/
                $criteriaVo = new Atexo_Consultation_CriteriaVo();
                $criteriaVo->setTypeAcces(Atexo_Config::getParameter('TYPE_PROCEDURE_PUBLICITE'));
                $criteriaVo->setcalledFromPortail(false);
                $criteriaVo->setIdService(Atexo_CurrentUser::getCurrentServiceId());
                $criteriaVo->setAcronymeOrganisme(Atexo_CurrentUser::getCurrentOrganism());
                $criteriaVo->setConnectedAgentId(Atexo_CurrentUser::getId());
                $criteriaVo->setEtatConsultation(Atexo_Config::getParameter('STATUS_APRES_DECISION'));

                if ((string) $tender->attributes()->type) {
                    $typeAvis = (new Atexo_Consultation_TypeAvis())->retreiveTypeAvisByAbreviation((string) $tender->attributes()->type);
                    if ($typeAvis[0] instanceof CommonTypeAvis) {
                        $criteriaVo->setIdTypeAvis($typeAvis[0]->getId());
                    }
                }

                if ($tender->attributes()->reference) {
                    $criteriaVo->setReferenceConsultation((string) $tender->attributes()->reference);
                }

                if ($tender->attributes()->procedureType) {
                    $criteriaVo->setIdTypeProcedure((new Atexo_Consultation_ProcedureType())->getIdTypeProcedureByAbbreviation((string) $tender->attributes()->procedureType));
                }
                if ($tender->attributes()->category) {
                    $criteriaVo->setCategorieConsultation($this->retrieveIdCategoryByLibelle((string) $tender->attributes()->category));
                }
                if ($tender->attributes()->minClosureDate) {
                    $date = Atexo_Util::iso860TimeToIso((string) $tender->attributes()->minClosureDate);
                    $criteriaVo->setDateFinStart($date);
                }
                if ($tender->attributes()->maxClosureDate) {
                    $date = Atexo_Util::iso860TimeToIso((string) $tender->attributes()->maxClosureDate);
                    $criteriaVo->setDateFinEnd($date);
                }
                if ($tender->attributes()->deliveryPlace) {
                    $arrayCode = explode(',', (string) $tender->attributes()->deliveryPlace);
                    $str = '';
                    foreach ($arrayCode as $oneCode) {
                        $oneGeo = (new Atexo_Geolocalisation_GeolocalisationN2())->retrieveGeoN2ByDenomination2($oneCode);
                        if ($oneGeo) {
                            $str .= $oneGeo->getId().',';
                        }
                    }
                    $criteriaVo->setLieuxexecution(substr($str, 0, -1));
                }
                if ($tender->attributes()->cpv) {
                    $arrayCpv = explode(',', (string) $tender->attributes()->cpv);
                    $criteriaVo->setIdCodeCpv1($arrayCpv[0]);
                    $criteriaVo->setIdCodeCpv2($arrayCpv[1]);
                    $criteriaVo->setIdCodeCpv3($arrayCpv[2]);
                    $criteriaVo->setIdCodeCpv4($arrayCpv[3]);
                }
                if ($tender->attributes()->keywords) {
                    $criteriaVo->setKeyWordAdvancedSearch((string) $tender->attributes()->keywords);
                }
                $consultationArray = (new Atexo_Consultation())->search($criteriaVo);
                $xml = self::generateResponseTender($consultationArray, false, true);
                // print_r($xml);exit;
                return $xml;
            }
        } catch (Exception) {
            return 'TECHNICAL_ERROR';
        }
    }
}
