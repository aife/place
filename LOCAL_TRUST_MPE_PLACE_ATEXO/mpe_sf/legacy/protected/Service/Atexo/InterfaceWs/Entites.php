<?php

namespace Application\Service\Atexo\InterfaceWs;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonAgent;
use Application\Propel\Mpe\CommonAgentServiceMetierPeer;
use Application\Propel\Mpe\CommonHabilitationAgent;
use Application\Propel\Mpe\CommonHabilitationAgentPeer;
use Application\Propel\Mpe\CommonRPA;
use Application\Propel\Mpe\CommonServiceMertierProfilsQuery;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_InterfaceWs;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_Xml;
use Application\Service\Atexo\EntityPurchase\Atexo_EntityPurchase_RPA;

/**
 * Classe de.
 *
 * @author Nabil BELLAHCEN <nabil.bellahcen@atexo.com>
 * @copyright Atexo 2009
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_InterfaceWs_Entites extends Atexo_InterfaceWs
{
    public function getXmlEntities($entites, $extractionDonneesSocle = true)
    {
        if ($entites) {
            $organisme = (string) $entites->attributes()->organism;
            $fromDate = (string) $entites->attributes()->fromDate;
            if ($fromDate) {
                $fromDate = Atexo_Util::iso860TimeToIso($fromDate);
            }
            if ('' != $organisme || null != $organisme) {
                $services = Atexo_EntityPurchase::getAllServicesAndPoles($organisme, $fromDate);
                $agents = Atexo_Agent::retriveAgentsByOrganismeAndDateModif($organisme, $fromDate);
                $rpas = Atexo_EntityPurchase_RPA::retriveRpasByOrganismeAndDateModif($organisme, $fromDate);
            } else {
                $services = (new Atexo_EntityPurchase())->getAllServicesPlateForme($fromDate);
                $agents = Atexo_Agent::retriveAgentsByOrganismeAndDateModif(null, $fromDate);
                $rpas = (new Atexo_EntityPurchase_RPA())->getAllRpaslateForme($organisme);
            }

            return $this->generateResponseEntites($services, $agents, $rpas, $organisme, $fromDate, $extractionDonneesSocle);
        }
    }

    public function generateResponseEntites($services, $agents, $rpas, $organisme, $fromDate, $extractionDonneesSocle = true, $modeFonctionnementSocleExterne = false)
    {
        $domDocument = self::getXmlTemplate($extractionDonneesSocle, $modeFonctionnementSocleExterne);

        if ($services || $agents || $rpas) {
            $transaction = $domDocument->documentElement;
            $response = $transaction->getElementsByTagName('Response')->item(0);
            $entities = $domDocument->createElement('Entities');
            if ($organisme) {
                (new Atexo_Xml())->addAttribute($domDocument, $entities, 'organism', Atexo_Util::toUtf8($organisme));
            }
            if ($fromDate) {
                (new Atexo_Xml())->addAttribute($domDocument, $entities, 'fromDate', Atexo_Util::toUtf8($fromDate));
            }
            $response->appendChild($entities);

            if ($services) {
                foreach ($services as $oneServ) {
                    $entity = $domDocument->createElement('Entity');
                    $entities->appendChild($entity);
                    $oneServ = array_map([Atexo_Util::class, 'toUtf8'], $oneServ);
                    //donnees initialisees et ecrasees si modeFonctionnementSocleExterne
                    $idEntite = $oneServ['id'];
                    if ($oneServ['parentId']) {
                        $idParent = $oneServ['parentId'];
                    } else {
                        $idParent = '0';
                    }

                    if ($extractionDonneesSocle) {
                        if ($modeFonctionnementSocleExterne) {//pour la migration vers le socle PPP
                            $idEntite = $oneServ['id_initial'];
                            if ($oneServ['parentId']) {
                                $serviceParent = $services[$oneServ['acronymOrganism'].'#'.$oneServ['parentId']];
                                $idParent = $serviceParent['id_initial'];
                            } else {
                                $idParent = '0';
                            }
                            (new Atexo_Xml())->addAttribute($domDocument, $entity, 'organismId', $oneServ['id_initial_organism']); /// $organismId Pour le socle PPP
                        } else {
                            (new Atexo_Xml())->addAttribute($domDocument, $entity, 'organismId', $oneServ['id_organism']); /// $organismId Pour le socle
                        }
                        if ('' != $oneServ['siren']) { // on ajoute l'attribut siren juste s'il est renseigne
                            (new Atexo_Xml())->addAttribute($domDocument, $entity, 'siren', $oneServ['siren']); /// Pour le socle
                        }
                        (new Atexo_Xml())->addAttribute($domDocument, $entity, 'acronymOrganism', $oneServ['acronymOrganism']); /// Pour le socle
                    }

                    (new Atexo_Xml())->addAttribute($domDocument, $entity, 'id', $idEntite);

                    (new Atexo_Xml())->addAttribute($domDocument, $entity, 'parentId', $idParent);
                    (new Atexo_Xml())->addAttribute($domDocument, $entity, 'title', $oneServ['title']);
                    if ($extractionDonneesSocle && $modeFonctionnementSocleExterne) {
                        (new Atexo_Xml())->addAttribute($domDocument, $entity, 'sigle', $oneServ['acronym']); // Pour le socle PPP
                    } else {
                        (new Atexo_Xml())->addAttribute($domDocument, $entity, 'acronym', $oneServ['acronym']);
                    }

                    (new Atexo_Xml())->addAttribute($domDocument, $entity, 'address', $oneServ['address']);
                    (new Atexo_Xml())->addAttribute($domDocument, $entity, 'postalCode', $oneServ['postalCode']);
                    (new Atexo_Xml())->addAttribute($domDocument, $entity, 'city', $oneServ['city']);
                    (new Atexo_Xml())->addAttribute($domDocument, $entity, 'phone', $oneServ['phone']);
                    (new Atexo_Xml())->addAttribute($domDocument, $entity, 'fax', $oneServ['fax']);
                    (new Atexo_Xml())->addAttribute($domDocument, $entity, 'country', $oneServ['country']);

                    //Verification du format de l'email
                    $email = '';
                    if (preg_match('/[_\-a-zA-Z0-9\.\+]+@[a-zA-Z0-9](\.?[\-a-zA-Z0-9]*[a-zA-Z0-9])*/', $oneServ['email'])) {
                        $email = $oneServ['email'];
                    }
                    (new Atexo_Xml())->addAttribute($domDocument, $entity, 'email', $email);
                }
            }
            if ($rpas) {
                /** @var CommonRPA $oneRpas */
                foreach ($rpas as $oneRpas) {
                    $rpa = $domDocument->createElement('RPA');
                    $entities->appendChild($rpa);
                    (new Atexo_Xml())->addAttribute($domDocument, $rpa, 'id', Atexo_Util::toUtf8($oneRpas->getId()));
                    (new Atexo_Xml())->addAttribute($domDocument, $rpa, 'entityId', Atexo_Util::toUtf8($oneRpas->getServiceId()));
                    (new Atexo_Xml())->addAttribute($domDocument, $rpa, 'firstname', Atexo_Util::toUtf8($oneRpas->getNom()));
                    (new Atexo_Xml())->addAttribute($domDocument, $rpa, 'lastname', Atexo_Util::toUtf8($oneRpas->getPrenom()));
                    (new Atexo_Xml())->addAttribute($domDocument, $rpa, 'jobFunction', Atexo_Util::toUtf8($oneRpas->getFonction()));
                    (new Atexo_Xml())->addAttribute($domDocument, $rpa, 'address', Atexo_Util::toUtf8($oneRpas->getAdresse1().' '.$oneRpas->getAdresse2()));
                    (new Atexo_Xml())->addAttribute($domDocument, $rpa, 'postalCode', Atexo_Util::toUtf8($oneRpas->getCodepostal()));
                    (new Atexo_Xml())->addAttribute($domDocument, $rpa, 'city', Atexo_Util::toUtf8($oneRpas->getVille()));
                    (new Atexo_Xml())->addAttribute($domDocument, $rpa, 'country', Atexo_Util::toUtf8($oneRpas->getPays()));
                }
            }
            if ($agents) {
                /** @var CommonAgent $oneAgent */
                foreach ($agents as $oneAgent) {
                    $idAgent = '';
                    $idService = '';
                    $tentative = '';
                    $organismeIdInitial = '';

                    $agent = $domDocument->createElement('Agent');

                    $entities->appendChild($agent);
                    //il faut differencier le mode socle interne ou externe pour la gestion du rattachement
                    if ($extractionDonneesSocle && $modeFonctionnementSocleExterne) {
                        $idAgent = $oneAgent->getIdExterne();
                        if ($oneAgent->getServiceId()) {
                            $serviceObject = (new Atexo_EntityPurchase())->getEntityById($oneAgent->getServiceId(), $oneAgent->getOrganisme());
                            if ($serviceObject) {
                                $idService = $serviceObject->getIdExterne();
                            }
                        } else {
                            $idService = 0;
                        }
                    } else {
                        $idAgent = $oneAgent->getId();
                        $idService = $oneAgent->getServiceId();
                    }
                    // Partie : mutualisation de l'extraction donnees socle interne ou externe
                    if ($extractionDonneesSocle) {
                        $objOrganisme = Atexo_Organismes::retrieveOrganismeByAcronyme($oneAgent->getOrganisme());
                        if ($objOrganisme) {
                            $organismeIdInitial = $objOrganisme->getId();
                        }

                        if ($modeFonctionnementSocleExterne) {
                            if ($objOrganisme) {
                                $organismeIdInitial = $objOrganisme->getIdInitial();
                            }
                            (new Atexo_Xml())->addAttribute($domDocument, $agent, 'elu', Atexo_Util::toUtf8($oneAgent->getElu())); /// Pour le socle PPP
                            if (2 == $oneAgent->getTypeComm()) { // TYPE COMMUNICATION EMAIL
                                $typeCommEmail = '1';
                            } else {
                                $typeCommEmail = '0';
                            }
                            (new Atexo_Xml())->addAttribute($domDocument, $agent, 'typeCommunicationCAOPrefEmail', Atexo_Util::toUtf8($typeCommEmail)); /// Pour le socle PPP
                            (new Atexo_Xml())->addAttribute($domDocument, $agent, 'jobFunction', Atexo_Util::toUtf8($oneAgent->getNomFonction())); /// Pour le socle PPP
                        }

                        (new Atexo_Xml())->addAttribute($domDocument, $agent, 'organismId', Atexo_Util::toUtf8($organismeIdInitial)); ///Pour le socle
                        /* @author : JAP @date : 18/08/2014
                         * Les deux attributs suivants sont superflus d'apres Worldline; ils sont d'ailleurs absents de la derniere version du XSD
                        * */
                        //                      Atexo_Xml::addAttribute($domDocument, $agent, "denominationOrganism",utf8_encode($denominationOrganism)); /// Pour le socle
                        //                      Atexo_Xml::addAttribute($domDocument, $agent, "sigleOrganism",utf8_encode($sigleOrganism)); /// Pour le socle

                        if ($oneAgent->getTentativesMdp() >= '3') {
                            $tentative = 0;
                        } else {
                            $tentative = 1;
                        }
                        (new Atexo_Xml())->addAttribute($domDocument, $agent, 'acronymOrganism', Atexo_Util::toUtf8($oneAgent->getOrganisme())); ///  Pour le socle
                        (new Atexo_Xml())->addAttribute($domDocument, $agent, 'compteActif', Atexo_Util::toUtf8($tentative)); /// Pour le socle
                        (new Atexo_Xml())->addAttribute($domDocument, $agent, 'password', Atexo_Util::toUtf8($oneAgent->getPassword())); /// Pour le socle

                        //l'ajout de la balise 'Applications' d'acces aux applications pour l'agent  (socle PPP)
                        $applications = $domDocument->createElement('Applications');
                        $agent->appendChild($applications);
                        $cr = new Criteria();
                        if ($modeFonctionnementSocleExterne) {
                            $cr->add(CommonAgentServiceMetierPeer::ID_AGENT, $oneAgent->getIdExterne());
                        } else {
                            $cr->add(CommonAgentServiceMetierPeer::ID_AGENT, $oneAgent->getId());
                        }

                        $lesApplications = CommonAgentServiceMetierPeer::doSelect($cr, Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY')));

                        if ($lesApplications) {
                            foreach ($lesApplications as $oneApplication) {
                                $applicationId = $oneApplication->getIdServiceMetier();
                                $profilId = $oneApplication->getIdProfilService();

                                $profilIdExterne = CommonServiceMertierProfilsQuery::create()
                                     ->filterByIdServiceMetier($applicationId)
                                     ->filterByIdInterne($profilId)
                                     ->select(['id_externe'])
                                     ->findOne();

                                $application = $domDocument->createElement('Application');
                                $applications->appendChild($application);

                                (new Atexo_Xml())->addAttribute($domDocument, $application, 'applicationId', Atexo_Util::toUtf8($applicationId));
                                (new Atexo_Xml())->addAttribute($domDocument, $application, 'profilId', Atexo_Util::toUtf8($profilId));
                                //Determiner quel id doit etre exporte pour la migration du socle
                                if (!$modeFonctionnementSocleExterne) {
                                    (new Atexo_Xml())->addAttribute($domDocument, $application, 'profilIdExterne', Atexo_Util::toUtf8($profilIdExterne));
                                }
                            }
                        }
                    }

                    (new Atexo_Xml())->addAttribute($domDocument, $agent, 'id', Atexo_Util::toUtf8($idAgent));
                    (new Atexo_Xml())->addAttribute($domDocument, $agent, 'entityId', Atexo_Util::toUtf8($idService));
                    (new Atexo_Xml())->addAttribute($domDocument, $agent, 'login', Atexo_Util::toUtf8($oneAgent->getLogin()));

                    //Verification du format de l'email
                    $email = '';
                    if (preg_match('/[_\-a-zA-Z0-9\.\+]+@[a-zA-Z0-9](\.?[\-a-zA-Z0-9]*[a-zA-Z0-9])*/', $oneAgent->getEmail())) {
                        $email = $oneAgent->getEmail();
                    }
                    (new Atexo_Xml())->addAttribute($domDocument, $agent, 'email', Atexo_Util::toUtf8($email));
                    $firstname = $oneAgent->getPrenom();
                    $lastname = $oneAgent->getNom();
                    (new Atexo_Xml())->addAttribute($domDocument, $agent, 'firstname', Atexo_Util::toUtf8($firstname));
                    (new Atexo_Xml())->addAttribute($domDocument, $agent, 'lastname', Atexo_Util::toUtf8($lastname));
                    (new Atexo_Xml())->addAttribute($domDocument, $agent, 'phone', Atexo_Util::toUtf8($oneAgent->getNumTel()));
                    (new Atexo_Xml())->addAttribute($domDocument, $agent, 'fax', Atexo_Util::toUtf8($oneAgent->getNumFax()));

                    if (!$extractionDonneesSocle) {
                        //rajout de ces informations specifiquement pour la defense (PMI)
                        $c = new Criteria();
                        $c->add(CommonHabilitationAgentPeer::ID_AGENT, $oneAgent->getId(), Criteria::EQUAL);
                        $habilitation = CommonHabilitationAgentPeer::doSelectOne($c, Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY')));
                        if ($habilitation instanceof CommonHabilitationAgent && ($habilitation->getTelechargementGroupeAnticipePlisChiffres()
                            || $habilitation->getTelechargementUnitairePlisChiffres()
                            || $habilitation->getOuvrirCandidatureEnLigne()
                            || $habilitation->getOuvrirOffreADistance()
                            || $habilitation->getOuvrirOffreEnLigne()
                            || $habilitation->getOuvrirOffreADistance()
                            || $habilitation->getOuvrirAnonymatEnLigne()
                            || $habilitation->getOuvrirAnonymatADistance())) {
                            $droitCertificat = true;
                        } else {
                            $droitCertificat = false;
                        }
                        (new Atexo_Xml())->addAttribute($domDocument, $agent, 'droitCertificat', Atexo_Util::toUtf8($droitCertificat));
                    }
                }
            }
        }

        return $domDocument->saveXml();
    }
}
