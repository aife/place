<?php

namespace Application\Service\Atexo\DomainesActivites;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonCG76Domaine;
use Application\Propel\Mpe\CommonCG76DomainePeer;
use Application\Propel\Mpe\CommonCG76DonneeComplementaireDomainePeer;
use Application\Service\Atexo\Atexo_Config;

/**
 * Classe de manipulation des domaines.
 *
 * @author ABISOUROUR Othmane <othmane.abisourour@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_DomainesActivites_Domaines
{
    public function retreiveDomaines()
    {
        $arrayParents = [];
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonCG76DomainePeer::PARENT, Atexo_Config::getParameter('DOMAINE_ACTIVITE_AUCUN_PARENT'));
        $domaineParentList = CommonCG76DomainePeer::doSelect($c, $connexionCom);
        if (is_array($domaineParentList)) {
            foreach ($domaineParentList as $parent) {
                if ($parent) {
                    $arrayParents[$parent->getId()]['libelle'] = $parent->getLibelle();
                    $arrayParents[$parent->getId()]['id'] = $parent->getId();
                    $arrayParents[$parent->getId()]['fils'] = [];
                    $c = new Criteria();
                    $c->add(CommonCG76DomainePeer::PARENT, $parent->getId());
                    $fils = CommonCG76DomainePeer::doSelect($c, $connexionCom);
                    if ($fils) {
                        foreach ($fils as $f) {
                            $arrayParents[$parent->getId()]['fils'][$f->getId()]['libelle'] = $f->getLibelle();
                            $arrayParents[$parent->getId()]['fils'][$f->getId()]['id'] = $f->getId();
                        }
                    }
                }
            }
        }

        return $arrayParents;
    }

    public function retrieveDomaineById($idDomaine)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonCG76DomainePeer::ID, $idDomaine);
        $domaine = CommonCG76DomainePeer::doSelectOne($c, $connexionCom);

        return $domaine;
    }

    public function isDomaineParent($idDomaine)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonCG76DomainePeer::ID, $idDomaine);
        $domaine = CommonCG76DomainePeer::doSelectOne($c, $connexionCom);
        if ($domaine instanceof CommonCG76Domaine) {
            if ($domaine->getParent() == Atexo_Config::getParameter('DOMAINE_ACTIVITE_AUCUN_PARENT')) {
                return true;
            }
        }

        return false;
    }

    public function retrieveDomaineParent(CommonCG76Domaine $domaine)
    {
        if ($domaine->getParent() != Atexo_Config::getParameter('DOMAINE_ACTIVITE_AUCUN_PARENT')) {
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            $c = new Criteria();
            $c->add(CommonCG76DomainePeer::ID, $domaine->getParent());
            $domaineParent = CommonCG76DomainePeer::doSelectOne($c, $connexionCom);

            return $domaineParent;
        }

        return false;
    }

    public function retrieveDomaineByIdDonneeCompelementaire($idDonneeComplementaire)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonCG76DonneeComplementaireDomainePeer::IDDONNEECOMLEMENTAIRE, $idDonneeComplementaire);
        $domaines = CommonCG76DonneeComplementaireDomainePeer::doSelect($c, $connexionCom);

        return $domaines;
    }

    public function deleteDomainesByIdDonneeComplementaire($idDonneeComplementaire)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $c = new Criteria();
        $c->add(CommonCG76DonneeComplementaireDomainePeer::IDDONNEECOMLEMENTAIRE, $idDonneeComplementaire);
        CommonCG76DonneeComplementaireDomainePeer::doDelete($c, $connexionCom);
    }
}
