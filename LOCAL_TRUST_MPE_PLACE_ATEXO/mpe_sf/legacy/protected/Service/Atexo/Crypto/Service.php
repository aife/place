<?php

namespace Application\Service\Atexo\Crypto;

use AtexoCrypto\Dto\Signature;
use AtexoCrypto\Dto\Consultation;

use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Config\Atexo_Config_Exception;
use AtexoCrypto\Client\Client;
use Psr\Log\LoggerInterface;


/**
 * Classe contenant les fonctions crypto.
 *
 * @author Anas RHORBAL <anas.rhorbal@atexo.com>
 * @copyright Atexo 2016
 *
 * @version 0
 *
 * @since   0
 */
class Atexo_Crypto_Service
{
    private $url;
    private $logger;
    private $proxy;
    private $platform;
    private $enableSignatureServerV2;
    private $webServicesCrypto = null;

    /**
     * @return mixed
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param mixed $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

    /**
     * @return mixed
     */
    public function getLogger()
    {
        return $this->logger;
    }

    /**
     * @param mixed $logger
     */
    public function setLogger($logger)
    {
        $this->logger = $logger;
    }

    /**
     * @return mixed
     */
    public function getProxy()
    {
        return $this->proxy;
    }

    /**
     * @param mixed $proxy
     */
    public function setProxy($proxy)
    {
        $this->proxy = $proxy;
    }

    /**
     * @return mixed
     */
    public function getPlatform()
    {
        return $this->platform;
    }

    /**
     * @param mixed $platform
     */
    public function setPlatform($platform)
    {
        $this->platform = $platform;
    }

    /**
     * @return mixed
     */
    public function getEnableSignatureServerV2()
    {
        return $this->enableSignatureServerV2;
    }

    /**
     * @param mixed $enableSignatureServerV2
     */
    public function setEnableSignatureServerV2($enableSignatureServerV2)
    {
        $this->enableSignatureServerV2 = $enableSignatureServerV2;
    }

    /**
     * Atexo_Crypto_Service constructor.
     *
     * @param $url
     * @param LoggerInterface $logger
     * @param $enableSignatureServerV2
     * @param null $platform
     *
     * @throws Atexo_Config_Exception
     */
    public function __construct($url, $logger, $enableSignatureServerV2, $platform = null, $webServicesCrypto = null)
    {
        $this->setUrl($url);
        $this->setLogger($logger);
        $this->setEnableSignatureServerV2($enableSignatureServerV2);

        if ($proxyUrl = Atexo_Config::getParameter('URL_PROXY')) {
            $this->setProxy($proxyUrl.':'.Atexo_Config::getParameter('PORT_PROXY'));
        }

        if (null != $platform) {
            $this->setPlatform($platform);
        }

        $this->webServicesCrypto = $webServicesCrypto;
    }

    /**
     * Demande de chiffrement.
     *
     * @param App\Entity\Consultation $consultation
     * @param App\Entity\offre        $offre
     *
     * @return string $horodatage
     *
     * @throws \AtexoCrypto\Exception\ServiceException
     */
    public function demandeChiffrement($consultation, $offre, array $params)
    {
        $c = new Client(
            $this->url,
            null,
            null,
            $this->logger,
            $this->proxy,
            null,
            $this->webServicesCrypto
        );

        $mappingDto = new Atexo_Crypto_MappingDto();
        $chiffrement = $mappingDto->getChiffrement($consultation, $offre, $params);

        return $c->chiffrementService()->demandeChiffrement($chiffrement);
    }

    /**
     * Demande de dechiffrement.
     *
     * @param int $idAgent
     *
     * @return string $jnlp
     *
     * @throws \AtexoCrypto\Exception\ServiceException
     */
    public function demandeDechiffrement(array $offres, $idAgent, $consultation = null, array $params = [])
    {
        $c = new Client(
            $this->url,
            null,
            null,
            $this->logger,
            $this->proxy,
            null,
            $this->webServicesCrypto
        );

        $mappingDto = new Atexo_Crypto_MappingDto();
        $dechiffrement = $mappingDto->getDechiffrement($offres, $idAgent, $consultation, $params);

        return $c->dechiffrementService()->demandeDechiffrement($dechiffrement);
    }

    /**
     * Verification de signature.
     *
     * @param $hash
     * @param $signatureXml
     * @param string $type
     * @param string $hash256
     *
     * @return array
     *
     * @throws \AtexoCrypto\Exception\ServiceException
     */
    public function verifierSignature($hash, $signatureXml, $type = 'XADES', $hash256 = '')
    {
        $c = new Client(
            $this->url,
            null,
            null,
            $this->logger,
            $this->proxy,
            null,
            $this->webServicesCrypto
        );

        $mappingDto = new Atexo_Crypto_MappingDto();
        $signature = $mappingDto->getVerifHash($hash, $signatureXml, $type, $hash256);
        $eSSV2 = 0;

        if ($this->enableSignatureServerV2) {
            $eSSV2 = 1;
        }

        return $c->validationService()->verifierSignature($signature, $eSSV2);
    }

    /**
     * Verification de signature PADES.
     *
     * @param $pdfPath
     *
     * @return array
     *
     * @throws \AtexoCrypto\Exception\ServiceException
     */
    public function verifierSignaturePades($pdfPath)
    {
        $c = new Client(
            $this->url,
            null,
            null,
            $this->logger,
            $this->proxy,
            $this->platform,
            $this->webServicesCrypto
        );

        $mappingDto = new Atexo_Crypto_MappingDto();

        $signatureFichier = $mappingDto->getSignatureFichier(
            $pdfPath
        );

        $signature = $mappingDto->getVerifPdf($pdfPath, 'PADES');
        $eSSV2 = 0;

        if ($this->enableSignatureServerV2) {
            $eSSV2 = 1;
        }

        return $c->validationService()->verifierSignature($signature, $eSSV2, $signatureFichier);
    }

    /**
     * Verification de signature.
     *
     * @param $signatureXml
     * @param $path
     * @param $idFile
     * @param $type
     *
     * @return array
     *
     * @throws \AtexoCrypto\Exception\ServiceException
     */
    public function verifierSignatureFichier($signatureXml, $path, $idFile, $type)
    {
        $c = new Client(
            $this->url,
            null,
            null,
            $this->logger,
            $this->proxy,
            null,
            $this->webServicesCrypto
        );

        $mappingDto = new Atexo_Crypto_MappingDto();
        $signature = $mappingDto->getVerifFichier($signatureXml, $path, $idFile, $type);
        $eSSV2 = 0;

        if ($this->enableSignatureServerV2) {
            $eSSV2 = 1;
        }

        return $c->validationService()->verifierSignature($signature, $eSSV2);
    }

    /**
     * Signature Pades.
     *
     * @param string $urlServeurCrypto
     *
     * @return string jnlp
     *
     * @throws \AtexoCrypto\Exception\ServiceException
     */
    public function jnlpSignature($urlServeurCrypto)
    {
        $c = new Client(
            $this->url,
            null,
            null,
            $this->logger,
            $this->proxy,
            null,
            $this->webServicesCrypto
        );

        return $c->signatureService()->jnlpSignature($urlServeurCrypto);
    }

    /**
     * Validation horodatage.
     *
     * @param string $jetonTSR
     * @param string $hashSha256
     *
     * @return string json
     *
     * @throws \AtexoCrypto\Exception\ServiceException
     */
    public function verifierHorodatage($jetonTSR, $hashSha256)
    {
        $c = new Client(
            $this->url,
            null,
            null,
            $this->logger,
            $this->proxy,
            null,
            $this->webServicesCrypto
        );

        return $c->validationService()->verifierHorodatage($jetonTSR, $hashSha256);
    }
}
