<?php

namespace Application\Service\Atexo\Crypto;

use Application\Propel\Mpe\CommonOffres;
/*
 * Created by PhpStorm.
 * User: ARH
 * Date: 29/09/2016
 * Time: 12:21
 */

use AtexoCrypto\Dto\Certificat;
use AtexoCrypto\Dto\Chiffrement;
use AtexoCrypto\Dto\Consultation;
use AtexoCrypto\Dto\ConsultationAlloti;
use AtexoCrypto\Dto\Dechiffrement;
use AtexoCrypto\Dto\Enveloppe;
use AtexoCrypto\Dto\EnveloppeAlloti;
use AtexoCrypto\Dto\Fichier;
use AtexoCrypto\Dto\Offre;
use AtexoCrypto\Dto\VerifFichier;
use AtexoCrypto\Dto\VerifHash;
use AtexoCrypto\Dto\VerifPdf;

class Atexo_Crypto_MappingDto
{
    /**
     * Map consultation to chiffrement.
     *
     * @param App\Entity\Consultation $consultationObject
     * @param App\Entity\offre        $offreObject
     *
     * @return Chiffrement
     */
    public function getChiffrement($consultationObject, $offreObject, array $params)
    {
        $chiffrement = new Chiffrement();
        $chiffrement->setServeurCryptoURLpublique($params['ServeurCryptoURLpublique']);
        $chiffrement->setMpeURLPrivee($params['MpeURLPrivee']);
        $chiffrement->setMpeFilePath($params['MpeFilePath']);
        $chiffrement->setMpeLogin($params['MpeLogin']);
        $chiffrement->setMpePassword($params['MpePassword']);
        $chiffrement->setFonction($params['Fonction']);

        $chiffrement->setRaisonSocialEntreprise($offreObject->getNomEntreprise());

        if ($consultationObject->getAlloti()) {
            $consultation = new ConsultationAlloti();
            $consultation->setNombreLots(is_countable($consultationObject->getLots()) ? count($consultationObject->getLots()) : 0);
        } else {
            $consultation = new Consultation();
        }
        $consultation->setReference($consultationObject->getReferenceUtilisateur());
        $consultation->setId($consultationObject->getId());
        $consultation->setOrganisme($consultationObject->getAcronymeOrg());
        $consultation->setChiffrement(true);
        $consultation->setAlloti(('1' == $consultationObject->getAlloti()));
        $consultation->setIntitule($consultationObject->getIntitule());
        $consultation->setObjet($consultationObject->getObjet());

        $consultation->setCmsActif($consultationObject->getCmsActif());

        $chiffrement->setConsultation($consultation);

        $offre = self::getOffre($offreObject, $consultationObject, true);

        $chiffrement->setOffre($offre);

        return $chiffrement;
    }

    /**
     * Map Offres to dechiffrement.
     *
     * @param int $idAgent
     *
     * @return Dehiffrement
     */
    public function getDechiffrement(array $offres, $idAgent, $consultationObject = null, array $params = [])
    {
        $dechiffrement = new Dechiffrement();

        $dechiffrement->setServeurCryptoURLpublic($params['ServeurCryptoURLPublic']);
        $dechiffrement->setMpeURLPrivee($params['MpeURLPrivee']);
        $dechiffrement->setMpeFilePath($params['MpeFilePath']);
        $dechiffrement->setMpeLogin($params['MpeLogin']);
        $dechiffrement->setMpePassword($params['MpePassword']);
        $dechiffrement->setFormat($params['Format']);
        $dechiffrement->setEnLigne($params['EnLigne']);

        if ($consultationObject) {
            $dechiffrement->setReference($consultationObject->getReferenceUtilisateur());
            $dechiffrement->setOrganisme($consultationObject->getOrganisme());
            $dechiffrement->setDateLimiteRemisePlis($consultationObject->getDateFin());
        }

        $dechiffrement->setIdAgent($idAgent);

        foreach ($offres as $offre) {
            $dechiffrement->addOffre(self::getOffre($offre));
        }

        return $dechiffrement;
    }

    /**
     * Map fichier to signature.
     *
     * @param App\Entity\EnveloppeFichier $fichier
     * @param string                      $path
     *
     * @return Dehiffrement
     */
    public function getVerifHash($hash, $signatureXml, $type, $hash256 = '')
    {
        $signature = new VerifHash();

        $signature->addHash($hash);
        $signature->addHash($hash256);
        $signature->setSignatureBase64($signatureXml);
        $signature->setSignatureType($type);

        return $signature;
    }

    /**
     * Map fichier to signature.
     *
     * @param App\Entity\EnveloppeFichier $fichier
     * @param string                      $path
     *
     * @return Dehiffrement
     */
    public function getVerifPdf($pdfBase64, $type)
    {
        $signature = new VerifPdf();

        $signature->setPdfPath($pdfBase64);
        $signature->setSignatureType($type);

        return $signature;
    }

    /**
     * Map fichier to signature.
     *
     * @param App\Entity\EnveloppeFichier $fichier
     * @param string                      $path
     *
     * @return Dehiffrement
     */
    public function getVerifFichier($signatureXml, $path, $idFile, $type)
    {
        $signature = new VerifFichier();

        $signature->setSignatureBase64($signatureXml);
        $signature->setMpeFilePath($path);
        $signature->setIdFileSystem($idFile);
        $signature->setSignatureType($type);

        return $signature;
    }

    /**
     * Map Offre to chiffrement.
     *
     * @param CommonOffres $offreObject
     * @param bool         $chiffrement
     *
     * @return Offre
     */
    public function getOffre($offreObject, $consultationObject = null, $chiffrement = false)
    {
        $offre = new Offre();
        $offre->setIdOffre($offreObject->getId());

        if (!$chiffrement) {
            $offre->setReponseAnnonceBase64(base64_encode($offreObject->getXmlString()));
            if (0 == $offreObject->getVerificationHotodatage()) {
                $offre->setJetonHorodatage($offreObject->getHorodatage());
                $offre->setHashFichiersOffre($offreObject->getHorodatageHashFichiers());
            }
        }
        if (is_array($offreObject->getEnveloppesToDechiffre()) && count($offreObject->getEnveloppesToDechiffre())) {
            $enveloppes = $offreObject->getEnveloppesToDechiffre();
        } else {
            $enveloppes = $offreObject->getCommonEnveloppes();
        }
        foreach ($enveloppes as $enveloppe) {
            $offre->addEnveloppe(self::getEnveloppe($enveloppe, $chiffrement));
        }

        if ($chiffrement) {
            if ($consultationObject) {
                foreach ($consultationObject->getCertificatChiffrementsNonDouble() as $certificat) {
                    $offre->addCertificate(self::getCertificat($certificat));
                }
            }
        }

        return $offre;
    }

    /**
     * Map Enveloppe to chiffrement.
     *
     * @param App\Entity\Enveloppe $enveloppeObject
     * @param bool                 $chiffrement
     *
     * @return Enveloppe
     */
    public function getEnveloppe($enveloppeObject, $chiffrement = false)
    {
        if ($enveloppeObject->getSousPli()) {
            $enveloppe = new EnveloppeAlloti();
            $enveloppe->setNumeroLot($enveloppeObject->getSousPli());
        } else {
            $enveloppe = new Enveloppe();
        }
        $enveloppe->setIdEnveloppe($enveloppeObject->getIdEnveloppeElectro());
        $enveloppe->setType($enveloppeObject->getTypeEnv());

        if ($chiffrement) {
            $i = 1;
            foreach ($enveloppeObject->getFichierEnveloppes() as $fichierObject) {
                $fichier = self::getFichier($fichierObject);
                $fichier->setNumeroOrdre($i);
                $enveloppe->addFichier($fichier);
                ++$i;
            }
        }

        return $enveloppe;
    }

    /**
     * Map fichier to chiffrement.
     *
     * @param App\Entity\EnveloppeFichier $fichierObject
     *
     * @return Fichier
     */
    public function getFichier($fichierObject)
    {
        $fichier = new Fichier();
        $fichier->setIdFichier($fichierObject->getIdFichier());
        $fichier->setTypeFichier($fichierObject->getTypeFichier());
        $fichier->setName($fichierObject->getNomFichier());
        $fichier->setIdFileSystem($fichierObject->getIdBlob().'-0');
        $fichier->setHashFichier ($fichierObject->getHash256());
        $fichier->setTailleFichier($fichierObject->getTailleFichier());
        return $fichier;
    }

    /**
     * Map certificat to chiffrement.
     *
     * @param App\Entity\CertificatChiffrement $certificatObject
     *
     * @return Certificat
     */
    public function getCertificat($certificatObject)
    {
        $certificat = new Certificat();
        $certificat->setTypeEnveloppe($certificatObject->getTypeEnv());

        $certificat->setCertificatBase64($certificatObject->getCertificat());

        return $certificat;
    }

    /**
     * @param $pathFichier
     * @param $pathCertificat
     * @param $password
     *
     * @return \AtexoCrypto\Dto\SignatureFichier
     */
    public function getSignatureFichier($pathFichier, $pathCertificat = null, $password = null)
    {
        $signatureFichier = new \AtexoCrypto\Dto\SignatureFichier();
        $signatureFichier->setCertificat($pathCertificat);
        $signatureFichier->setPassword($password);
        $signatureFichier->setContenu($pathFichier);

        return $signatureFichier;
    }
}
