<?php

namespace Application\Service\Atexo\Crypto;

use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Crypto;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Util;
use AtexoCrypto\Dto\Certificat;
use AtexoCrypto\Dto\Fichier;
use Prado\Prado;

/**
 * Classe de gestion des certificats x509.
 *
 * @author ?
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Crypto_Certificat
{
    /**
     * Génération de certificat.
     */
    public function generateCertificat($pkcs10Req, $organisme)
    {
        $arrayReturn = [];
        $token = md5(uniqid(random_int(0, mt_getrandmax()), true));
        Atexo_CurrentUser::writeToSession('pkcs10_req', $pkcs10Req);
        $msgErreur = '';
        $sessId = session_id();
        $tempDir = Atexo_Config::getParameter('COMMON_TMP');
        // creation de la requete OPENSSL
        $req = "-----BEGIN CERTIFICATE REQUEST-----\n" . $pkcs10Req . '-----END CERTIFICATE REQUEST-----';
        $tmpReqFilename = $tempDir . $sessId . '_pkcs10.pem';
        $reqFp = fopen($tmpReqFilename, 'w');
        if (!fwrite($reqFp, $req)) {
            $msgErreur = Prado::localize('ERREUR_INTERNE_IMPOSS_ECRIRE_REQUETE');

            return $msgErreur;
        }
        fclose($reqFp);
        $tmpCertFilename = $tempDir . $sessId . '_cert.pem';
        if (0 == strcasecmp(Atexo_Config::getParameter('ONLINE_SET_LD_PATH'), 'on')) {
            putenv('LD_LIBRARY_PATH=' . Atexo_Config::getParameter('NCIPHER_LD_PATH'));
        }

        $crypto = new Atexo_Crypto();
        $crypto->generateCaSerialFile();

        if (Atexo_Config::getParameter('ARCH_AVEC_PORTAIL')) {
            $marcheCaCert = Atexo_Config::getParameter('BASE_ROOT_DIR') . Atexo_Config::getParameter('COMMON_DIRECTORY') . Atexo_Config::getParameter('MARCHES_CA_CERT');
            $marchesCaKey = Atexo_Config::getParameter('BASE_ROOT_DIR') . Atexo_Config::getParameter('COMMON_DIRECTORY') . 'ca/keys/' . 'marches_ca_pkey.pem';
            $opensslConfig = Atexo_Config::getParameter('BASE_ROOT_DIR') . Atexo_Config::getParameter('COMMON_DIRECTORY') . Atexo_Config::getParameter('OPENSSL_CONFIG');
            $caSerial = Atexo_Config::getParameter('BASE_ROOT_DIR') . Atexo_Config::getParameter('COMMON_DIRECTORY') . Atexo_Config::getParameter('CA_SERIAL');
        } else {
            $marcheCaCert = Atexo_Config::getParameter('BASE_ROOT_DIR') . strtolower($organisme) . '/' . Atexo_Config::getParameter('MARCHES_CA_CERT');
            $marchesCaKey = Atexo_Config::getParameter('BASE_ROOT_DIR') . strtolower($organisme) . '/' . 'ca/keys/' . 'marches_ca_pkey.pem';
            $opensslConfig = Atexo_Config::getParameter('BASE_ROOT_DIR') . strtolower($organisme) . '/' . Atexo_Config::getParameter('OPENSSL_CONFIG');
            $caSerial = Atexo_Config::getParameter('BASE_ROOT_DIR') . strtolower($organisme) . '/' . Atexo_Config::getParameter('CA_SERIAL');
        }

        $createCert = escapeshellarg(Atexo_Config::getParameter('OPENSSL'))
                    . " x509 -req -in $tmpReqFilename -out $tmpCertFilename " .
                    ' -days ' . Atexo_Config::getParameter('CERTIFICATE_LENGTH') .
                    ' -CA ' . $marcheCaCert .
                    ' -CAkey ' . $marchesCaKey .
                    ' -CAserial ' . $caSerial .
                    ' -extfile ' . escapeshellarg($opensslConfig) .
                    ' -extensions s_mime_cert ' .
                    Atexo_Config::getParameter('OPENSSL_ENGINE');
        $var = '';
        exec($createCert . '  > ' . $tempDir . 'create_cert_' . $token . ' 2>&1', $var, $res);

        if (0 != $res) {
            $msgErreur = Prado::localize('ERREUR_CREATION_CERTIFICAT');
            $certFp = fopen($tmpCertFilename, 'r');
            $reqFp = fread($tmpCertFilename, filesize($tmpCertFilename));
            fclose($certFp);

            return $msgErreur;
        }

        $certFp = fopen($tmpCertFilename, 'r');
        $pemCert = fread($certFp, filesize($tmpCertFilename));
        fclose($certFp);

        @unlink($tmpReqFilename);
        @unlink($tmpCertFilename);
        @unlink($tempDir . "create_cert_$token");

        //La méthode php ereg() est obsolete depuis le php5.3, on utilise le preg_match à sa place
        $temp = str_replace("\n", '', $pemCert);
        preg_match("/^-----BEGIN CERTIFICATE-----\s*(.*)\s*-----END CERTIFICATE-----$/", $temp, $certText);

        if (!$certText) {
            $msgErreur = Prado::localize('CERTIFICAT_GENERE_INVALIDE');

            return $msgErreur;
        }

        $arrayReturn['pemCert'] = $pemCert;
        $arrayReturn['certText'] = $certText;

        return $arrayReturn;
    }

    public function UseExistingCertificat($userCertificat, $org)
    {
        $token = md5(uniqid(random_int(0, mt_getrandmax()), true));
        $certificate = "-----BEGIN CERTIFICATE-----\n" . $userCertificat . '-----END CERTIFICATE-----';
        $temp_cert_file = Atexo_Config::getParameter('COMMON_TMP') . 'user_certificate_' . $token . '.pem';
        $temp_cert_fp = fopen($temp_cert_file, 'w');
        fwrite($temp_cert_fp, $certificate);
        fclose($temp_cert_fp);
        $check_cert = Atexo_Config::getParameter('OPENSSL') . ' x509 -in ' . $temp_cert_file;
        system($check_cert . ' >/dev/null 2>&1', $res);
        @unlink($temp_cert_file);
        if (0 != $res) {
            return false;
        } else {
            return $certificate;
        }
    }

    public function checkCertificatFileValidity($certifFilePath, $org)
    {
        $checkCert = Atexo_Config::getParameter('OPENSSL') . ' x509 -in ' . escapeshellarg($certifFilePath);
        system($checkCert . ' >/dev/null 2>&1', $res);
        if (0 != $res) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * retourne le CN de l'utilisateur inscrit par certificat.
     *
     * @param string $certif Le certificat en string
     *
     * @return string La valeur du CN du certificat
     */
    public function getCnCertificat($certif, $org = null)
    {
        return (new Atexo_Crypto_Certificat())->parseCertificat($certif, 'subject', 'CN');
    }

    /**
     * retourne le CN de l'utilisateur inscrit par certificat.
     *
     * @param string $certif Le certificat en string
     *
     * @return string La valeur du CN de l'émetteur du certificat
     */
    public function getACCertificat($certif)
    {
        return (new Atexo_Crypto_Certificat())->parseCertificat($certif, 'issuer', 'CN');
    }

    /**
     * Retourne la date d'expiration d'un certificat au format Y-m-d H:i.
     *
     * @param string $certif Le certificat en string
     *
     * @return string date d'expiration au format Y-m-d H:i
     */
    public function getDateExpiration($certif)
    {
        $dateExpiration = (new Atexo_Crypto_Certificat())->parseCertificat($certif, 'validTo_time_t');
        if (is_long(intval($dateExpiration)) && intval($dateExpiration) > 0) {
            return date('Y-m-d H:i', $dateExpiration);
        } else {
            return false;
        }
    }

    /**
     * Parse un certificat x509 depuis un fichier et retourne la valeur d'un champ précisé en param.
     *
     * @param string $certificatFile Fichier sur disque du certificat x509
     * @param string $categorie      Indique la catégorie. Choix possible : subject, issuer, validTo_time_t
     * @param string $param          (optionnel) Indique le paramètre à afficher dans la catégorie
     *                               ou la catégorie si pas renseigné. Choix possible : C, L, OU, O, emailAddress, CN
     *
     * @return string La valeur du paramètre (retourne N/A si pas de valeur)
     */
    protected function parseCertificatFromFile($certificatFile, $categorie, $param = '')
    {
        if (is_file($certificatFile)) {
            return (new Atexo_Crypto_Certificat())
                ->parseCertificat(file_get_contents($certificatFile), $categorie, $param)
                ;
        } else {
            return 'N/A';
        }
    }

    /**
     * Parse un certificat x509 et retourne la valeur d'un champ précisé en param.
     *
     * @param string $certificat Certificat x509 (contenu dans une string)
     * @param string $categorie  Indique la catégorie. Choix possible : subject, issuer, validTo_time_t
     * @param string $param      (optionnel) Indique le paramètre à afficher dans la catégorie
     *                           ou la catégorie si pas renseigné. Choix possible : C, L, OU, O, emailAddress, CN
     *
     * @return string La valeur du paramètre (retourne N/A si pas de valeur)
     */
    public function parseCertificat($certificat, $categorie, $param = '')
    {
        $x509 = openssl_x509_parse($certificat);
        if (is_array($x509)) {
            if ('' != $param) {
                $paramValue = $x509[$categorie][$param];
            } else {
                $paramValue = $x509[$categorie];
            }
            if (is_array($paramValue)) {
                $paramValue = str_replace("\x00", '', implode(', ', $paramValue));
            }

            $res = $paramValue;
            if (!Atexo_Util::isUTF8($paramValue)) {
                $res = utf8_decode($paramValue);
            }
            return $res;
        } else {
            return 'N/A';
        }
    }

    public function getEmailCertificatFromFile($certifFile)
    {
        return self::parseCertificatFromFile($certifFile, 'subject', 'emailAddress');
    }

    public function getCnCertificatFromFile($certifFile)
    {
        return self::parseCertificatFromFile($certifFile, 'subject', 'CN');
    }

    public function getOuCertificatFromFile($certifFile)
    {
        return self::parseCertificatFromFile($certifFile, 'subject', 'OU');
    }

    public function getOCertificatFromFile($certifFile)
    {
        return self::parseCertificatFromFile($certifFile, 'subject', 'O');
    }

    public function getCCertificatFromFile($certifFile)
    {
        return self::parseCertificatFromFile($certifFile, 'subject', 'C');
    }

    public function getCnEmetteurCertificatFromFile($certifFile)
    {
        return self::parseCertificatFromFile($certifFile, 'issuer', 'CN');
    }

    public function getOuEmetteurCertificatFromFile($certifFile)
    {
        return self::parseCertificatFromFile($certifFile, 'issuer', 'OU');
    }

    public function getOEmetteurCertificatFromFile($certifFile)
    {
        return self::parseCertificatFromFile($certifFile, 'issuer', 'O');
    }

    public function getCEmetteurCertificatFromFile($certifFile)
    {
        return self::parseCertificatFromFile($certifFile, 'issuer', 'C');
    }

    public function CertificatInsertNewLineEvery64Char($certificat)
    {
        if (
            ('-----BEGIN CERTIFICATE-----' == substr($certificat, 0, 27) && '-----END CERTIFICATE-----' == substr($certificat, -25)) ||
            ('_____BEGIN CERTIFICATE_____' == substr($certificat, 0, 27) && '_____END CERTIFICATE_____' == substr($certificat, -25))
        ) {
            $certificat = str_replace('-----BEGIN CERTIFICATE-----', '', $certificat);
            $certificat = str_replace('-----END CERTIFICATE-----', '', $certificat);
            $certificat = str_replace('_____BEGIN CERTIFICATE_____', '', $certificat);
            $certificat = str_replace('_____END CERTIFICATE_____', '', $certificat);
            $certificat = wordwrap($certificat, 64, "\n", true);
            $certificat = "-----BEGIN CERTIFICATE-----\n" . $certificat . "\n-----END CERTIFICATE-----";
        }

        return $certificat;
    }

    public function getCnSubject($signatureOffre)
    {
        $cnCeritifcat = '';
        if ($signatureOffre) {
            $typeSignature = 'XML';
            $signatureOffre = base64_decode($signatureOffre);
            $pos = strpos($signatureOffre, 'SignedInfo');
            if (!$pos) {
                $signatureOffre = "-----BEGIN PKCS7-----\n" . trim($signatureOffre) . "\n-----END PKCS7-----";
                $typeSignature = '';
            }
            $fileCertificat = (new Atexo_Crypto())->getCertificatFromSignature($signatureOffre, false, true, $typeSignature);
            $cnCeritifcat = self::getCnCertificatFromFile($fileCertificat);
        }

        return $cnCeritifcat;
    }

    public function getSerialFromCertif($certificatFilePath)
    {
        $command = Atexo_Config::getParameter('OPENSSL') . ' x509 -in ' . $certificatFilePath . ' -noout -serial';
        $verificationResultatFile = Atexo_Config::getParameter('COMMON_TMP') . '/' . 'chaineVerif_' . session_name() . session_id() . time();
        system($command . ' > ' . $verificationResultatFile . ' 2>&1', $res);
        if (!$res) {
            $serialChaine = file_get_contents($verificationResultatFile);
            $serials = explode('=', $serialChaine);
            $serial = trim($serials[1]);
            unlink($verificationResultatFile);

            return $serial;
        }

        return false;
    }

    public function getDateCertficateValidTo($certifPath)
    {
        if (is_file($certifPath)) {
            $dateTo = (new Atexo_Crypto_Certificat())
                ->parseCertificat(file_get_contents($certifPath), 'validTo_time_t')
            ;

            return date('Y-m-d H:i', $dateTo);
        }

        return '';
    }

    public function getDateCertficateValidFrom($certifPath)
    {
        if (is_file($certifPath)) {
            $dateFrom = (new Atexo_Crypto_Certificat())
                ->parseCertificat(file_get_contents($certifPath), 'validFrom_time_t')
            ;

            return date('Y-m-d H:i', $dateFrom);
        }

        return '';
    }

    public function getSubjectCertificatFromFile($certifFile)
    {
        return self::parseCertificatFromFile($certifFile, 'subject');
    }

    public function getIssuerCertificatFromFile($certifFile)
    {
        return self::parseCertificatFromFile($certifFile, 'issuer');
    }
}
