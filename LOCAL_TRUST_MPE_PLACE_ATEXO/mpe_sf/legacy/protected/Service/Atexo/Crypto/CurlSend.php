<?php

namespace Application\Service\Atexo\Crypto;

use Application\Service\Atexo\Atexo_Config;
use Exception;

/**
 * Classe de.
 *
 * @author Khalid BENAMAR <khalid.benamar@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Crypto_CurlSend
{
    /**
     * permet d envoyer au serveur d horodatage la requete time stamping et de recuperer la reponse
     * retourne un tableau contenant le statut de la requete OPENSSL (0 si OK) et le chemin du fichier contenant le jeton d horodatage si OK ou l'erreur si échec.
     *
     * @param unknown_type $req_file chemin du fichier contenant la requete OPENSSL
     *
     * @return array (statut de la requete OPENSSL, chemin du fichier contenant le jeton d horodatage ou erreur)
     */
    public function sendToTsServer($req_file)
    {
        $formvars = [];
        $submit_url = Atexo_Config::getParameter('TS_SERVER_ADDR');
        $formvars['platform_id'] = Atexo_Config::getParameter('PLATFORM_ID');

        $fp = fopen($req_file, 'r');
        $request = base64_encode(fread($fp, filesize($req_file)));
        $formvars['request'] = $request;
        fclose($fp);

        // initialisation de la session curl
        $ch = curl_init($submit_url);

        // options de transfert curl
        curl_setopt($ch, CURLOPT_SSLVERSION, 3);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_CAPATH, TRUST_DIR);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $formvars);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, Atexo_Config::getParameter('CURLOPT_CONNECTTIMEOUT'));

        // envoi des parametres et recuperation de la reponse
        $pnp_result_page = curl_exec($ch);

        // erreur curl
        $errorNumber = curl_errno($ch);
        if (0 != $errorNumber) {
            $errorMsg = '';
            $errorMsg = curl_error($ch);
            $fp = fopen(Atexo_Config::getParameter('COMMON_TMP').'tsr_error', 'w');
            fwrite($fp, $errorMsg);
            fclose($fp);
            $error = CONNEXION_SERVEUR_HORODATAGE_ECHEC." \n $errorMsg";

            return [1, $error];
        }

        // fermeture de la session curl
        curl_close($ch);

        $tab = explode('.tsq', $req_file);
        $response_file = $tab[0].'.tsr';

        if (preg_match('/##DEBUT JETON HORODATAGE##/', $pnp_result_page)) {
            $tab1 = explode('##DEBUT JETON HORODATAGE##', $pnp_result_page);
            $tab2 = explode('##FIN JETON HORODATAGE##', $tab1[1]);
            $response = $tab2[0];
            $fp = fopen($response_file, 'w+');
            fwrite($fp, base64_decode($response));
            fclose($fp);

            return [0, $response_file];
        } else {
            $tab1 = explode('##DEBUT ERREUR##', $pnp_result_page);
            $tab2 = explode('##FIN ERREUR##', $tab1[1]);
            $error = $tab2[0];

            return [2, $error];
        }
    }

    public function sendToServerLuxTrust($requestfile_path)
    {
        if (!file_exists($requestfile_path)) {
            throw new \HttpException('The Requestfile was not found');
        }

        $submit_url = Atexo_Config::getParameter('TS_SERVER_ADDR');
        $ch = curl_init($submit_url);
        curl_setopt($ch, CURLOPT_SSLVERSION, 3);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, Atexo_Config::getParameter('CURLOPT_TIMEOUT'));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, file_get_contents($requestfile_path));
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/timestamp-query']);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, true);
        curl_setopt($ch, CURLOPT_SSLCERT, Atexo_Config::getParameter('PATH_FILE_CERTIFICAT'));
        curl_setopt($ch, CURLOPT_SSLCERTPASSWD, Atexo_Config::getParameter('PASSWORD_CERTIFICATE'));
        curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, true);
        //definition du proxy si existe
        $urlProxy = Atexo_Config::getParameter('URL_PROXY');
        $portProxy = Atexo_Config::getParameter('PORT_PROXY');
        $curlOptProxy = $urlProxy.':'.$portProxy;
        if ('' != $urlProxy) {
            curl_setopt($ch, CURLOPT_PROXY, $curlOptProxy);
        }

        $binary_response_string = curl_exec($ch);
        $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $tab = explode('.tsq', $requestfile_path);
        $response_file = $tab[0].'.tsr';
        $fp = fopen($response_file, 'w+');
        fwrite($fp, $binary_response_string);
        fclose($fp);
        // erreur curl
        $errorNumber = curl_errno($ch);
        $error = '';
        if (0 != $errorNumber) {
            $errorMsg = '';
            $errorMsg = curl_error($ch);
            $error = "CONNEXION_SERVEUR_HORODATAGE_ECHEC \n $errorMsg";

            return [1, $error];
        }

        curl_close($ch);

        if (200 != $status || !strlen($binary_response_string)) {
            return [2, $error];
        } else {
            return [0, $response_file];
        }
    }

    public function sendDataToModuleValidation($postData)
    {
        $submit_url = Atexo_Config::getParameter('URL_MODULE_VALIDATION_SIGNATURE_LOCAL');
        $ch = curl_init($submit_url);
        curl_setopt($ch, CURLOPT_SSLVERSION, 3);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, Atexo_Config::getParameter('CURLOPT_TIMEOUT'));
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, true);
        curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, Atexo_Config::getParameter('TUNNEL_PROXY'));

        //definition du proxy si existe
        $urlProxy = Atexo_Config::getParameter('URL_PROXY');
        $portProxy = Atexo_Config::getParameter('PORT_PROXY');
        $curlOptProxy = $urlProxy.':'.$portProxy;

        if ('' != $urlProxy) {
            curl_setopt($ch, CURLOPT_PROXY, $curlOptProxy);
        }

        $postResult = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Erreur Curl : '.curl_error($ch);
        }

        return $postResult;
    }
}
