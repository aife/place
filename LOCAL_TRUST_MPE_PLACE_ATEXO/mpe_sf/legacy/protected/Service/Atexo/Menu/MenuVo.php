<?php

namespace Application\Service\Atexo\Menu;

/**
 * Contient le détail des paramètres d'un menu 1.8.
 *
 * @author TANTAOUI Omar <omar.tantaoui@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Menu_MenuVo
{
    /**
     * @var string
     */
    private $_cmd = '';
    /**
     * @var int
     */
    private $_org = '';
    /**
     * @var string
     */
    private $_reference = '';

    private string $_refParamName = '';

    public function setCmd($value)
    {
        $this->_cmd = $value;
    }

    public function getCmd()
    {
        return $this->_cmd;
    }

    public function setOrg($value)
    {
        $this->_org = $value;
    }

    public function getOrg()
    {
        return $this->_org;
    }

    public function setReference($value)
    {
        $this->_reference = $value;
    }

    public function getReference()
    {
        return $this->_reference;
    }

    public function setRefParamName($value)
    {
        $this->_refParamName = $value;
    }

    public function getRefParamName()
    {
        return $this->_refParamName;
    }

    public function display()
    {
        echo $this->getId().' : '.$this->getFirstName().' '.$this->getLastName().' ('.$this->getEntityPurchase().') : Permanent = '.
        $this->getPermanentGuest().'; TypeInvitation = '.$this->getTypeInvitation().'<br/>';
    }
}
