<?php

namespace Application\Service\Atexo;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonEnveloppe;
use Application\Propel\Mpe\CommonFichierEnveloppe;
use Application\Propel\Mpe\CommonFichierEnveloppePeer;
use Application\Propel\Mpe\CommonOffres;
use Application\Propel\Mpe\CommonResultatAnalyse;
use Application\Propel\Mpe\CommonResultatAnalysePeer;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Responses;
use Application\Service\Atexo\Controller\Atexo_Controller_Front;
use Prado\Prado;

class Atexo_ActeDEngagement
{
    public function preRemplirXmlAe($xml, $consultationObject, $inscritObject, $entrepriseObject, $typeProcedure, $lotObject = null)
    {
        $xmlString = null;
        if ($xml) {
            $xmlContent = simplexml_load_file($xml);
            if ($xmlContent->onglet) {
                $arrayXml = $xmlContent->onglet->bloc;
            } else {
                $arrayXml = $xmlContent->bloc;
            }
            $titleDoc = $xmlContent->titre;
            foreach ($titleDoc as $title) {
                $lesAttrbts = $title->attributes();
                $lesAttrbts['libelle'] = Atexo_Util::httpEncodingToUtf8(Prado::localize($lesAttrbts['libelle']));
            }
            foreach ($arrayXml as $unBloc) {
                $lesAttributs = $unBloc->attributes();
                $lesAttributs['libelle'] = Atexo_Util::httpEncodingToUtf8(Prado::localize($lesAttributs['libelle']));
                $lesAttributs['h2'] = Atexo_Util::httpEncodingToUtf8(Prado::localize($lesAttributs['h2']));
                $lesAttributs['h3'] = Atexo_Util::httpEncodingToUtf8(Prado::localize($lesAttributs['h3']));
                foreach ($unBloc as $unElt) {
                    $lesAttributs = $unElt->attributes();
                    $id = explode('_', $lesAttributs['id']);
                    $object = $id['0'];
                    $methode1 = 'get'.ucwords($id['1']);
                    $lesAttributs['libelle'] = Atexo_Util::httpEncodingToUtf8(Prado::localize($lesAttributs['libelle']));
                    if ('consultation' == $object) {
                        if ('' != strripos($lesAttributs['id'], 'date')) {
                            $lesAttributs['valeur'] = Atexo_Util::httpEncodingToUtf8(Atexo_Util::iso2frnDateTime($consultationObject->$methode1()));
                        } else {
                            $lesAttributs['valeur'] = ('' != $consultationObject->$methode1()) ? Atexo_Util::httpEncodingToUtf8($consultationObject->$methode1()) : '--';
                        }
                    } elseif ('inscrit' == $object) {
                        $lesAttributs['valeur'] = Atexo_Util::httpEncodingToUtf8($inscritObject->$methode1());
                    } elseif ('entreprise' == $object) {
                        $lesAttributs['valeur'] = Atexo_Util::atexoHtmlEntities($entrepriseObject->$methode1());
                    } elseif ('categorieLot' == $object) {
                        if ($lotObject) {
                            $lesAttributs['valeur'] = Atexo_Util::httpEncodingToUtf8($lotObject->$methode1());
                        } else {
                            $lesAttributs['valeur'] = '--';
                        }
                    } elseif ('typeProcedure' == $object) {
                        $lesAttributs['valeur'] = Atexo_Util::httpEncodingToUtf8($typeProcedure->$methode1());
                    }
                    foreach ($unElt as $unSSElt) {
                        $lesAttribs = $unSSElt->attributes();
                        $lesAttribs['libelle'] = Atexo_Util::httpEncodingToUtf8(Prado::localize($lesAttribs['libelle']));
                    }
                }
            }
            $xmlString = $xmlContent->asXML();
        }//echo $xmlString;exit;

        return $xmlString;
    }

    public function getUrlDownloadLogoAe()
    {
        return Atexo_Util::getAbsoluteUrl().Atexo_Controller_Front::t().'/images/logo_acte_engagement.jpg';
    }

    public function getMontantOffreFromXmlActeDEngagement($fichier, $enveloppeObject)
    {
        $montantOffre = [];
        $atexoBlob = new Atexo_Blob();
        $content = '';
        if ($enveloppeObject->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_EN_LIGNE')
            || $enveloppeObject->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_HORS_LIGNE')
            || $enveloppeObject->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_A_DISTANCE')) {
            $organisme = Atexo_CurrentUser::getCurrentOrganism();
            if ($fichier instanceof CommonFichierEnveloppe && $fichier->getIdBlob()) {
                $content = $atexoBlob->return_blob_to_client($fichier->getIdBlob(), $organisme);
            }
        }
        if ('' != $content) {
            $xmlContent = simplexml_load_string(utf8_encode($content));
            //print_r($xmlContent);exit;
            if ($xmlContent->onglet) {
                $arrayXml = $xmlContent->onglet->bloc;
            } else {
                $arrayXml = $xmlContent->bloc;
            }
            foreach ($arrayXml as $unBloc) {
                foreach ($unBloc as $unElt) {
                    $lesAttributs = $unElt->attributes();
                    if ('montantOffre' == $lesAttributs['id']) {
                        $montantOffre['0'] = (string) $lesAttributs['valeur'];
                    }
                    for ($i = 0; $i < 5; ++$i) {
                        if ($lesAttributs['id'] == 'montantVariante'.($i + 1)) {
                            $montantOffre[($i + 1)] = (string) $lesAttributs['valeur'];
                        }
                    }
                }
            }
        }

        return $montantOffre;
    }

    /*
         * Cette fonction enregistre le montant recupéré depuis le fichier xml AE dans resultatAnalyse.
         * $typeEnv : electronique , papier
         */
    public function saveMontantOffre($idEnveloppeElectro, $consultationId, $idLot, $typeEnv, $withVariante)
    {
        $fichiers = null;
        $montantOffre = [];
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $organisme = Atexo_CurrentUser::getCurrentOrganism();
        $enveloppe = (new Atexo_Consultation_Responses())->retrieveEnveloppeById($idEnveloppeElectro, Atexo_Config::getParameter('TYPE_ENVELOPPE_ELECTRONIQUE'), $organisme);

        if ($enveloppe instanceof CommonEnveloppe && !($enveloppe->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_FERME') ||
                                                $enveloppe->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_REFUSEE') ||
                                                $enveloppe->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_SUPPRIMER'))) {
            $offre = $enveloppe->getOffre($connexion);
            if ($offre instanceof CommonOffres && $offre->getConsultationId() == $consultationId) {
                if ($enveloppe instanceof CommonEnveloppe) {
                    $c = new Criteria();
                    $c->add(CommonFichierEnveloppePeer::ID_ENVELOPPE, $idEnveloppeElectro);
                    $c->add(CommonFichierEnveloppePeer::ORGANISME, $organisme);
                    $fichiers = CommonFichierEnveloppePeer::doSelect($c, $connexion);
                }
            }
            if ($fichiers && is_array($fichiers)) {
                foreach ($fichiers as $file) {
                    if ($file->getTypeFichier() == Atexo_Config::getParameter('REPONSE_FICHIER_ACTE_ENGAGEMENT') && Atexo_Util::getExtension($file->getNomFichier()) == Atexo_Config::getParameter('XML_EXTENTION')) {
                        $montantOffre = (new Atexo_ActeDEngagement())->getMontantOffreFromXmlActeDEngagement($file, $enveloppe);
                    }
                }
            }
            $c = new Criteria();
            $c->add(CommonResultatAnalysePeer::CONSULTATION_ID, $consultationId);
            $c->add(CommonResultatAnalysePeer::ID_OFFRE, $offre->getId());
            $c->add(CommonResultatAnalysePeer::LOT, $idLot);
            $c->add(CommonResultatAnalysePeer::TYPE_ENVELOPPE, $typeEnv);
            $c->add(CommonResultatAnalysePeer::ORGANISME, $organisme);
            $propositions = CommonResultatAnalysePeer::doSelect($c, $connexion);

            foreach ($propositions as $proposition) {
                if ($proposition instanceof CommonResultatAnalyse) {
                    $proposition->delete($connexion);
                }
            }
            $index = 0;
            if (is_array($montantOffre)) {
                if ($withVariante) {
                    foreach ($montantOffre as $one) {
                        if ('0.00' != $one && '' != $one) {
                            $proposition = new CommonResultatAnalyse();
                            $proposition->setConsultationId($consultationId);
                            $proposition->setIdOffre($offre->getId());
                            $proposition->setLot($idLot);
                            $proposition->setTypeEnveloppe($typeEnv); //electronique ou papier
                            $proposition->setMontantOffre($one);
                            $proposition->setOrganisme($organisme);
                            if (0 == $index) {
                                $proposition->setOffreVariante('0');
                            } elseif ($index > 0) {
                                $proposition->setOffreVariante('1');
                            }
                            $proposition->save($connexion);
                        }
                        ++$index;
                    }
                } else {
                    if ($montantOffre['0']) {
                        $proposition = new CommonResultatAnalyse();
                        $proposition->setConsultationId($consultationId);
                        $proposition->setIdOffre($offre->getId());
                        $proposition->setLot($idLot);
                        $proposition->setTypeEnveloppe($typeEnv); //electronique ou papier
                        $proposition->setMontantOffre($montantOffre['0']);
                        $proposition->setOrganisme($organisme);
                        $proposition->save($connexion);
                    }
                }
            }
        }

        return $montantOffre;
    }
}
