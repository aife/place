<?php

namespace Application\Service\Atexo;

use App\Entity\Agent;
use App\Service\PradoPasswordEncoderInterface;
use Application\Service\Atexo\Atexo_CurrentUser;
use Exception;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Agent\Atexo_Agent_EnablingsAgents;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Library\Propel\Query\Criterion;
use Application\Propel\Mpe\CommonAgent;
use Application\Propel\Mpe\CommonAgentPeer;
use Application\Propel\Mpe\CommonAgentQuery;
use Application\Propel\Mpe\CommonCertificatAgentPeer;
use Application\Propel\Mpe\CommonHabilitationAgent;
use Application\Propel\Mpe\CommonHistoriqueSuppressionAgent;
use Application\Propel\Mpe\CommonOrganisme;
use Application\Propel\Mpe\CommonOrganismePeer;
use Application\Propel\Mpe\CommonServicePeer;
use Application\Propel\Mpe\CommonSsoAgent;
use Application\Propel\Mpe\CommonSsoAgentPeer;
use Application\Propel\Mpe\CommonTContratTitulaire;
use Application\Service\Atexo\Agent\Atexo_Agent_CriteriaVo;
//use Application\Service\Atexo\Agent\Atexo_Agent_EnablingsAgents;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Guests;
use Application\Service\Atexo\Consultation\Atexo_Consultation_GuestVo;
use Application\Service\Atexo\Referentiel\Atexo_Referentiel_Referentiel;
use Prado\Prado;

/**
 * $Id: Agent.php,v 1.81 2014-11-24 17:55:25 lezziani Exp $.
 */

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Agent
{
    /**
     * retourne l'objet  Agent (nom, prénom, login ...) équivalent à l'id de l'agent passé en paramètre.
     *
     * @param : $idAgent : l'id de l'agent
     *
     * @return Agent si l'agent est trouvé, false sinon
     */
    public static function retrieveAgent($idAgent, bool $forceRefresh = false)
    {
        $useCache = false;
        if (!$forceRefresh && (Atexo_CurrentUser::isAgent() || Atexo_CurrentUser::isAgentSocle()) && Atexo_CurrentUser::isConnected() && Atexo_CurrentUser::getId() == $idAgent) {
            $useCache = true;
        }

        if ($useCache) {
            $agent = Atexo_CurrentUser::readFromSession('cachedAgent');
            if (!$agent || $idAgent != $agent->getId()) {
                $c = new Criteria();
                $c->add(CommonAgentPeer::ID, $idAgent, Criteria::EQUAL);
                $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
                $agent = CommonAgentPeer::doSelectOne($c, $connexionCom);
                Atexo_CurrentUser::writeToSession('cachedAgent', $agent);
            }
        } else {
            $c = new Criteria();
            $c->add(CommonAgentPeer::ID, $idAgent, Criteria::EQUAL);
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
            $agent = CommonAgentPeer::doSelectOne($c, $connexionCom);
        }

        return $agent ?: false;
    }

    /**
     * retourne l'objet  Agent (nom, prénom, ...) équivalent à l'login  de l'agent passé en paramètre.
     *
     * @param $idAgent l'id de l'agent
     *
     * @return Agent si l'agent est trouvé, false sinon
     */
    public function retrieveAgentByLogin($login)
    {
        $c = new Criteria();
        $c->add(CommonAgentPeer::LOGIN, $login, Criteria::EQUAL);
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $agentObject = CommonAgentPeer::doSelectOne($c, $connexionCom);

        return $agentObject ?: false;
    }

    /**
     * Vérifier que les deux agents.
     *
     * @param object agent connecté
     * @param object agent à traiter
     *
     * @return bool
     */
    public function verifyCapacityToModify($agentConnectedO, $agentToUpO)
    {
        if ($agentConnectedO && $agentToUpO) {
            // Est-ce que les deux agents appariennent au même organisme ?
            if ($agentConnectedO->getOrganisme() == $agentToUpO->getOrganisme()) {
                // Est-ce que le service de l'agent connecté est parent du service de l'agent à traiter ou les deux agent sont du même service ?
                if (
                    (Atexo_EntityPurchase::isParent($agentConnectedO->getServiceId(), $agentToUpO->getServiceId(), $agentToUpO->getOrganisme()))
                    || ($agentConnectedO->getServiceId() == $agentToUpO->getServiceId())
                ) {
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Retourne l'objet propel Agent (nom, prénom, ...) équivalent à l'email de l'agent passé en paramètre.
     *
     * @param $idAgent l'id de l'agent
     *
     * @return Agent Objet Agent si l'agent est trouvé, false sinon
     */
    public function retrieveAgentByMail($email)
    {
        $c = new Criteria();
        $c->add(CommonAgentPeer::EMAIL, $email, Criteria::EQUAL);
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $agentObject = CommonAgentPeer::doSelectOne($c, $connexionCom);

        return $agentObject ?: false;
    }

    /**
     * retourne tous les objets Agents ayant l'option d'alerte pour les consultations clôurées (nom, prénom, login ...).
     *
     * @return les objets Agents
     */
    public function retrieveAgentsAyantAlertePourConsultationCloture()
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonAgentPeer::ALERTE_CLOTURE_CONSULTATION, '1', Criteria::EQUAL);
        $agentsObjects = CommonAgentPeer::doSelect($c, $connexionCom);

        return $agentsObjects;
    }

    /**
     * retourne l'objet  Agent (nom, prénom, ...) équivalent à l'login  de l'agent passé en paramètre.
     *
     * @param $idAgent l'id de l'agent
     *
     * @return CommonAgent si l'agent est trouvé, false sinon
     */
    public function retrieveAgentByLoginAndMdp($login, $password)
    {
        $c = new Criteria();
        $c->add(CommonAgentPeer::LOGIN, $login, Criteria::EQUAL);
        $c->add(CommonAgentPeer::PASSWORD, $password, Criteria::EQUAL);
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $agentObject = CommonAgentPeer::doSelectOne($c, $connexionCom);

        return $agentObject ?: false;
    }

    public function retrieveAgentsAvecAlerteReponseElectronique($arrayIdsAgent)
    {
        $c = new Criteria();
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonAgentPeer::ALERTE_REPONSE_ELECTRONIQUE, '1', Criteria::EQUAL);
        $c->add(CommonAgentPeer::ACTIF, '0', Criteria::NOT_EQUAL);
        $c->add(CommonAgentPeer::ID, $arrayIdsAgent, Criteria::IN);
        $agentsObjects = CommonAgentPeer::doSelect($c, $connexionCom);

        return $agentsObjects;
    }

    public function retrieveAgentsAvecAlertePublicationBOAMP($arrayIdsAgent)
    {
        $c = new Criteria();
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonAgentPeer::ALERTE_PUBLICATION_BOAMP, '1', Criteria::EQUAL);
        $c->add(CommonAgentPeer::ACTIF, '0', Criteria::NOT_EQUAL);
        $c->add(CommonAgentPeer::ID, $arrayIdsAgent, Criteria::IN);
        $agentsObjects = CommonAgentPeer::doSelect($c, $connexionCom);

        return $agentsObjects;
    }

    public function retrieveAgentsAvecAlerteEchecPublicationBOAMP($arrayIdsAgent)
    {
        $c = new Criteria();
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonAgentPeer::ALERTE_ECHEC_PUBLICATION_BOAMP, '1', Criteria::EQUAL);
        $c->add(CommonAgentPeer::ACTIF, '0', Criteria::NOT_EQUAL);
        $c->add(CommonAgentPeer::ID, $arrayIdsAgent, Criteria::IN);
        $agentsObjects = CommonAgentPeer::doSelect($c, $connexionCom);

        return $agentsObjects;
    }

    public static function retriveServiceAgents($organisme, $serviceId, $inclureDescendant = false)
    {
        $c = new Criteria();
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonAgentPeer::ORGANISME, $organisme, Criteria::EQUAL);
        $c->addAnd(CommonAgentPeer::DELETED_AT, null, Criteria::EQUAL);

        if ($inclureDescendant) {
            $listeServices = Atexo_EntityPurchase::getSubServices($serviceId, $organisme, true, false);
            $c->add(CommonAgentPeer::SERVICE_ID, $listeServices, Criteria::IN);
        } else {
            $c->add(CommonAgentPeer::SERVICE_ID, $serviceId, Criteria::EQUAL);
        }
        $agents = CommonAgentPeer::doSelect($c, $connexionCom);

        return $agents;
    }

    public function deleteAgent($idAgentSupprime, $idAgentSupprimeur)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));
        $agentSupprime = CommonAgentPeer::retrieveByPK($idAgentSupprime, $connexionCom);
        $agentSupprimeur = CommonAgentPeer::retrieveByPK($idAgentSupprimeur, $connexionCom);
        //$habilitationAgentSupprime = $agentSupprime->getCommonHabilitationAgents(new Criteria(), $connexionCom);
        try {
            $connexionCom->beginTransaction();
            $historique = new CommonHistoriqueSuppressionAgent();
            $historique->setIdAgentSupprime($idAgentSupprime);
            $historique->setDateSuppression(date('Y-m-d H:i:s'));
            $historique->setEmail(trim($agentSupprimeur->getEmail()));
            $historique->setIdAgentSuppresseur($idAgentSupprimeur);
            $historique->setServiceId($agentSupprimeur->getServiceId());
            $historique->setNom($agentSupprimeur->getNom());
            $historique->setOrganisme($agentSupprimeur->getOrganisme());
            $historique->setPrenom($agentSupprimeur->getPrenom());
            $historique->save($connexionCom);
            $email = $agentSupprime->getEmail();
            $login = $agentSupprime->getLogin();
            $agentSupprime->setEmail($historique->getId() . '_' . $email);
            $agentSupprime->setLogin($historique->getId() . '_' . $login);
            $agentSupprime->setServiceId(null);
            $agentSupprime->setDeletedAt(new \DateTime());
            $agentSupprime->setTentativesMdp(3);
            $agentSupprime->setDateModification(date('Y-m-d H:i:s'));
            //$habilitationAgentSupprime = Atexo_Agent_EnablingsAgents::addOrModifyEnablingsOfTheAgents(0, $id);
            //$habilitationAgentSupprime->save($connexionCom);

            $agentSupprime->save($connexionCom);
            $connexionCom->commit();
        } catch (\Exception $e) {
            $connexionCom->rollback();
            echo $e->__toString();
        }
    }

    /**
     * Permet de recuperer la liste des agents avec ou sans leurs habilitations.
     *
     * @param string $organisme        : organisme
     * @param bool   $returnObject     : precise s'il faut retourner un objet
     * @param bool   $prenomFirst
     * @param bool   $withHabilitation : precise si on doit recuperer les agents avec leurs habilitations ou non
     *
     *
     * @author     Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version    1.0
     *
     * @since      4.11.0
     * @copyright  Atexo 2015
     */
    public static function retriveAgentsByOrganisme($organisme, $returnObject = false, $prenomFirst = false, $withHabilitation = false): array|bool
    {
        $c = new Criteria();
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonAgentPeer::ORGANISME, $organisme, Criteria::EQUAL);
        $c->add(CommonAgentPeer::DELETED_AT, null, Criteria::ISNULL);
        $c->addAscendingOrderByColumn(CommonAgentPeer::NOM);
        $c->addAscendingOrderByColumn(CommonAgentPeer::PRENOM);
        if (true === $withHabilitation) {
            $agents = CommonAgentPeer::doSelectJoinCommonHabilitationAgent($c, $connexionCom);
        } else {
            $agents = CommonAgentPeer::doSelect($c, $connexionCom);
        }
        if ($returnObject) {
            return $agents;
        } elseif (is_array($agents)) {
            $arrayElements = [];
            foreach ($agents as $elment) {
                if ($prenomFirst) {
                    $arrayElements[$elment->getId()] = $elment->getPrenom() . ' ' . $elment->getNom();
                } else {
                    $arrayElements[$elment->getId()] = $elment->getNom() . ' ' . $elment->getPrenom();
                }
            }

            return $arrayElements;
        } else {
            return false;
        }
    }

    /**
     * Mettre les infos récupèrer depuis le web service dans les champs de la table agent du projet associé.
     *
     * @param array $agent, array $wsAgent, array $entity
     */
    public static function remplirAgent($agent, $wsAgent, $entity)
    {
        $email = $wsAgent['email'];

        $agent->setIdExterne($wsAgent['id']);
        $agent->setLogin(trim($email));
        $agent->setEmail(trim($email));
        Atexo_Agent::updateNewAgentPasswordByArgon2($agent, $wsAgent['mdp']);
        $agent->setNom(utf8_decode($wsAgent['nom']));
        $agent->setPrenom(utf8_decode($wsAgent['prenom']));
        $agent->setTentativesMdp($wsAgent['tentatives_mdp']);
        $agent->setOrganisme($wsAgent['organisme']);
        $agent->setServiceId($entity->getId());
        $agent->setElu($wsAgent['elu']);
        $agent->setNomFonction(utf8_decode($wsAgent['nom_fonction']));
        $agent->setNumTel($wsAgent['num_tel']);
        $agent->setNumFax($wsAgent['num_fax']);
        $agent->setTypeComm($wsAgent['type_comm']);
        $agent->setAdrPostale(utf8_decode($wsAgent['adr_postale']));
        $agent->setCivilite($wsAgent['civilite']);
        $agent->setDateCreation($wsAgent['date_creation']);
        $agent->setDateModification($wsAgent['date_modification']);
    }

    /**
     * Définir pour chaque profil récupérer du web service un profil du projet associé.
     *
     * @param array $wsAgent
     */
    public static function recoverProfileAgent($wsAgent)
    {
        if (0 == $wsAgent['admin']) {
            $profilSocle = Atexo_Config::getParameter('UTILISATEUR_INTERNE');
        } elseif ((1 == $wsAgent['admin']) && (0 == $wsAgent['admin_type'])) {
            $profilSocle = Atexo_Config::getParameter('ADMINISTRATEUR_GLOBAL');
        } elseif ((1 == $wsAgent['admin']) && (1 == $wsAgent['admin_type'])) {
            $profilSocle = Atexo_Config::getParameter('ADMINISTRATEUR_TECHNIQUE');
        } elseif ((1 == $wsAgent['admin']) && (2 == $wsAgent['admin_type'])) {
            $profilSocle = Atexo_Config::getParameter('ADMINISTRATEUR_FONCTIONNEL');
        } else {
            $profilSocle = 0;
        }

        return $profilSocle;
    }

    public function retrieveAgentByIdExterne($idInitial)
    {
        $c = new Criteria();
        $c->add(CommonAgentPeer::ID_EXTERNE, $idInitial, Criteria::EQUAL);
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $agentObject = CommonAgentPeer::doSelectOne($c, $connexionCom);

        return $agentObject ?: false;
    }

    public function retrieveAgentById($idAgent)
    {
        $c = new Criteria();
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonAgentPeer::ID, $idAgent, Criteria::EQUAL);
        $agent = CommonAgentPeer::doSelectOne($c, $connexionCom);

        return $agent;
    }

    public function retrieveSsoAgentBySso($sso)
    {
        $c = new Criteria();
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonSsoAgentPeer::ID_SSO, $sso, Criteria::EQUAL);
        $c->add(CommonSsoAgentPeer::DATE_LAST_REQUEST, date('Y-m-d H:i:s'), Criteria::GREATER_THAN);
        $ssoAgent = CommonSsoAgentPeer::doSelectOne($c, $connexionCom);

        return $ssoAgent;
    }

    public static function retriveAgentsByOrganismeAndDateModif($organisme, $dateModif = null)
    {
        $c = new Criteria();
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        if ($organisme) {
            $c->add(CommonAgentPeer::ORGANISME, $organisme, Criteria::EQUAL);
        }
        $c->addAscendingOrderByColumn(CommonAgentPeer::NOM);
        $c->addAscendingOrderByColumn(CommonAgentPeer::PRENOM);
        if ($dateModif) {
            $c->add(CommonAgentPeer::DATE_MODIFICATION, $dateModif, Criteria::GREATER_EQUAL);
        }
        $agents = CommonAgentPeer::doSelect($c, $connexionCom);

        if ($agents) {
            return $agents;
        }

        return null;
    }

    public function retrieveAgentsAvecAlerteQuestionEntreprise($arrayIdsAgent)
    {
        $c = new Criteria();
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonAgentPeer::ALERTE_QUESTION_ENTREPRISE, '1', Criteria::EQUAL);
        $c->add(CommonAgentPeer::ACTIF, '0', Criteria::NOT_EQUAL);
        $c->add(CommonAgentPeer::ID, $arrayIdsAgent, Criteria::IN);
        $agentsObjects = CommonAgentPeer::doSelect($c, $connexionCom);

        return $agentsObjects;
    }

    public function retrieveAgentsAvecAlerteReceptionMessage($arrayIdsAgent)
    {
        $c = new Criteria();
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonAgentPeer::ALERTE_RECEPTION_MESSAGE, '1', Criteria::EQUAL);
        $c->add(CommonAgentPeer::ID, $arrayIdsAgent, Criteria::IN);
        $agentsObjects = CommonAgentPeer::doSelect($c, $connexionCom);

        return $agentsObjects;
    }

    /***
     * tous les agents
     * @param $organisme :l'organisme de l'agent
     */
    public function getAllAgent($organisme = null)
    {
        $c = new Criteria();
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonAgentPeer::TENTATIVES_MDP, Atexo_Config::getParameter('MAX_TENTATIVES_MDP'), Criteria::NOT_EQUAL);
        $c->add(CommonAgentPeer::DELETED_AT, null, Criteria::ISNULL);
        if ($organisme) {
            $c->add(CommonAgentPeer::ORGANISME, $organisme, Criteria::EQUAL);
        }
        $agentsObjects = CommonAgentPeer::doSelect($c, $connexionCom);

        return $agentsObjects;
    }

    /***
     * calcule le nombre des agents pour chaque organisme donné
     * @retuen : le nombre d'agent(s)
     */
    public function getCountAgent($organisme = null, $actif = null)
    {
        $sql = 'Select count(*) as nombreAgent FROM Agent WHERE deleted_at is null AND TENTATIVES_MDP!=' . Atexo_Config::getParameter('MAX_TENTATIVES_MDP');
        if ($organisme) {
            $sql .= " AND ORGANISME = '" . (new Atexo_Db())->quote($organisme) . "'";
        }

        if (null !== $actif) {
            $sql .= " AND ACTIF = '" . (new Atexo_Db())->quote($actif) . "'";
        }
        $statement = Atexo_Db::getLinkCommon()->query($sql);
        foreach ($statement as $row) {
            return $row['nombreAgent'];
        }
    }

    /*
     * Recuperer les agents avec tentatives de mot de passe = 3.
     */
    public function getAllAgentTentativesMdpMax($organisme = null)
    {
        $c = new Criteria();
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonAgentPeer::TENTATIVES_MDP, Atexo_Config::getParameter('MAX_TENTATIVES_MDP'), Criteria::EQUAL);
        if ($organisme) {
            $c->add(CommonAgentPeer::ORGANISME, $organisme, Criteria::EQUAL);
        }
        $agentsObjects = CommonAgentPeer::doSelect($c, $connexionCom);

        return $agentsObjects;
    }

    public function retrieveAgentBySso($sso)
    {
        $sql = 'Select id,login,mdp FROM Agent,sso_agent WHERE sso_agent.id_agent =Agent.id ' .
                //" AND sso_agent.DATE_LAST_REQUEST < '".date('Y-m-d H:i:s')."' " .
                        " AND id_sso ='" . (new Atexo_Db())->quote($sso) . "'";
        $statement = Atexo_Db::getLinkCommon()->query($sql);
        $agent = [];
        foreach ($statement as $row) {
            $agent['login'] = $row['login'];
            $agent['mdp'] = $row['mdp'];
            $agent['id_agent'] = $row['id'];
        }

        return $agent;
    }

    public function retrieveCertificatAgentById($idAgent)
    {
        $c = new Criteria();
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonCertificatAgentPeer::ID_AGENT, $idAgent);
        $certifObject = CommonCertificatAgentPeer::doSelectOne($c, $connexionCom);
        if ($certifObject) {
            return $certifObject;
        } else {
            return false;
        }
    }

    public function retrieveAgentByCertificatAndSerial($cert, $serial)
    {
        $c = new Criteria();
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonAgentPeer::NUM_CERTIFICAT, $serial);
        $agents = CommonAgentPeer::doSelect($c, $connexionCom);
        if ($agents) {
            $searchCert = base64_decode($cert);
            foreach ($agents as $oneAgent) {
                $userCert = base64_decode($oneAgent->getCertificat());
                $cmp = strcmp($searchCert, $userCert);
                if (0 == $cmp) {
                    return $oneAgent;
                }
            }
        }

        return false;
    }

    public function retrieveAgentByAlertServiceAndHabilitation($service, $alert, $habilitation, $organisme)
    {
        $sql = "Select A.email from Agent A Left Join HabilitationAgent HA on A.id=HA.id_agent where A.service_id='" . (new Atexo_Db())->quote($service) . "' And A." . (new Atexo_Db())->quote($alert) . "='1' And HA." . (new Atexo_Db())->quote($habilitation) . "='1' and A.organisme='" . $organisme . "';";
        $statement = Atexo_Db::getLinkCommon()->query($sql);

        $email = [];
        foreach ($statement as $row) {
            $email[] = $row['email'];
        }

        return $email;
    }

    /**
     * Recupere :
     *     => les agents invités sur la consultation et ayant l'alerte chorus activé, pour les contrat issus de la passation
     *     => les agents createurs des contrats pour les contrats hors passation.
     *
     * @param CommonTContratTitulaire $contrat           : contrat
     * @param string                  $organismeAcronyme : organisme
     * @param PropelPDO               $connexion         : connexion
     * @param Logger                  $logger
     *
     * @return array
     */
    public function retrieveGuestSubscribersChorusAlerts($contrat, $organismeAcronyme, $connexion = null, $logger = null)
    {
        $refConsultation = null;
        if (empty($logger)) {
            $logger = Atexo_LoggerManager::getLogger('chorus');
        }
        if (empty($connexion)) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
        if (is_array($contrat)) {
            try {
                $listeAgent = [];
                if (false == $contrat['horsPassation']) {//Contrat issus de la passation
                    $refConsultation = $contrat['idConsultation'];
                    //recupération des agents invités à
                    $guests = (new Atexo_Consultation_Guests())->getAllInvitedAgents($refConsultation, $organismeAcronyme);
                    if (is_array($guests)) {
                        foreach ($guests as $guest) {
                            //On retient juste les agents ayant les "alertes Chorus" activés
                            if ($guest->getActif() && $guest->getAlerteChorus()) {
                                $listeAgent[] = $guest;
                            }
                        }
                    }
                } else {//Contrats hors passation
                    $idAgent = explode('_', $contrat['createur']['idExterne']);
                    $idAgent = end($idAgent);
                    $agent = self::retrieveAgent($idAgent);
                    if ($agent instanceof CommonAgent) {
                        $guestVo = new Atexo_Consultation_GuestVo();
                        $guestVo->populate($agent);
                        if ($guestVo->getActif() && $guestVo->getAlerteChorus()) {
                            $listeAgent[] = $guestVo;
                        }
                    }
                }

                return $listeAgent;
            } catch (\Exception $e) {
                $logger->error("Erreur lors de la recuperation des agents pour l'envoi de mail retours CHORUS : id_contrat = " . $contrat->getIdContratTitulaire() . " , organisme = $organismeAcronyme , reference = $refConsultation " . PHP_EOL . PHP_EOL . 'Erreur : ' . $e->getMessage());
            }
        } else {
            $logger->error("Le contrat n'est pas une instance de CommonTContratTitulaire : " . print_r($contrat, true));
        }
    }

    public function retrieveAgentsParService($idService, $organisme)
    {
        $connection = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonAgentPeer::SERVICE_ID, $idService);
        $c->add(CommonAgentPeer::ORGANISME, $organisme);
        $c->addAnd(CommonAgentPeer::DELETED_AT, null, Criteria::EQUAL);
        $Agents = CommonAgentPeer::doSelect($c, $connection);

        return $Agents;
    }

    public function retreiveNomPrenomAgent($idAgent)
    {
        $agent = self::retrieveAgent($idAgent);
        if ($agent instanceof CommonAgent) {
            return $agent->getPrenom() . ' ' . $agent->getNom();
        }

        return '';
    }

    public function getMailAgentById($idAgent)
    {
        $agent = self::retrieveAgent($idAgent);
        if ($agent instanceof CommonAgent) {
            return $agent->getEmail();
        }
    }

    /**
     * retourne un tableau d'identifiants d'elements de la table "Agent".
     *
     * @param $names: chaine de caracteres contenant "nom" et "prenoms" separés par des espaces claviers
     *
     * @return : tableau d'identifiants d'elements de la table "Agent"
     */
    public function getIdsAgentByName($names)
    {
        $connection = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $arrayIdsAgent = [];
        //$c->add(CommonAgentPeer::ORGANISME, $organisme);
        $arrayName = explode(' ', $names);
        if (is_array($arrayName) && count($arrayName)) {
            foreach ($arrayName as $name) {
                $cton1 = $c->getNewCriterion(CommonAgentPeer::NOM, '%' . $name . '%', Criteria::LIKE);
                $cton2 = $c->getNewCriterion(CommonAgentPeer::PRENOM, '%' . $name . '%', Criteria::LIKE);
                // combine them
                $cton1->addOr($cton2);
                // add to Criteria
                $c->add($cton1);
                $Agents = CommonAgentPeer::doSelect($c, $connection);
                if (is_array($Agents) && count($Agents)) {
                    foreach ($Agents as $agent) {
                        if (!in_array($agent->getId(), $arrayIdsAgent)) {
                            $arrayIdsAgent[] = $agent->getId();
                        }
                    }
                }
            }
        }

        return $arrayIdsAgent;
    }

    /**
     * Permet de recuperer la liste des agents avec ou sans leurs habilitations.
     *
     * @param string $serviceId         : l'identifiant technique du service
     * @param string $organisme         : organisme
     * @param bool   $inclureDescendant : precise s'il faut inclure ou non la descendance
     * @param bool   $withHabilitations : precise s'il faut recuperer ou non les habilitations
     *
     * @return array : liste des agents
     *
     * @author     Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version    1.0
     *
     * @since      4.11.0
     *
     * @copyright  Atexo 2015
     */
    public function retriveAgentsByServiceAndOrg($serviceId, $organisme, $inclureDescendant = false, $withHabilitations = false)
    {
        $c = new Criteria();
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c->add(CommonAgentPeer::ORGANISME, $organisme);
        $c->addAnd(CommonAgentPeer::DELETED_AT, null, Criteria::EQUAL);

        if ($serviceId) {
            if ($inclureDescendant) {
                $listeServices = Atexo_EntityPurchase::getSubServices($serviceId, $organisme, true);
                $listeServices[] = $serviceId;
                $c->add(CommonAgentPeer::SERVICE_ID, $listeServices, Criteria::IN);
            } else {
                $c->add(CommonAgentPeer::SERVICE_ID, $serviceId);
            }
        } else {
            $listeServices = Atexo_EntityPurchase::getEntityPurchaseById($organisme);
            $c->add(CommonAgentPeer::SERVICE_ID, $listeServices, Criteria::IN);
            $c->addOr(CommonAgentPeer::SERVICE_ID, null, Criteria::EQUAL);
        }

        if (true === $withHabilitations) {
            $agents = CommonAgentPeer::doSelectJoinCommonHabilitationAgent($c, $connexionCom);
        } else {
            $agents = CommonAgentPeer::doSelect($c, $connexionCom);
        }

        return $agents;
    }

    public function getAgentsForCommission($c, $nbreElement = false)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));
        if (!$c) {
            $c = new Criteria();
        }
        if ($nbreElement) {
            $nbre = CommonAgentPeer::doCount($c, false, $connexion);

            return $nbre;
        } else {
            $agents = CommonAgentPeer::doSelect($c, $connexion);

            if (is_array($agents)) {
                foreach ($agents as $agent) {
                    $entityPath = Atexo_EntityPurchase::getSigleLibelleEntityById($agent->getServiceId(), $agent->getOrganisme());
                    $agent->setEntityPath($entityPath);
                }
            }

            return $agents;
        }
    }

    /**
     * Effectue une recherche paramétrée en fonction du critère proposé.
     *
     * @param Atexo_Agent_CriteriaVo $criteria, $returnJustNumberOfElement : à true pour avoir uniquement le nombre d'éléments
     *
     * @return array of consultation object
     */
    public function search(Atexo_Agent_CriteriaVo $criteriaVo, $returnJustNumberOfElement = false, $withRef = false)
    {
        $ctonOrg = null;
        $Services = null;
        $IdsServices = [];
        $Organismes = null;
        $orgAcronyme = [];
        $ctonAcheteur = null;
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        if ($criteriaVo->getNom()) {
            $c->add(CommonAgentPeer::NOM, '%' . $criteriaVo->getNom() . '%', Criteria::LIKE);
        }
        if ($criteriaVo->getPrenom()) {
            $c->add(CommonAgentPeer::PRENOM, '%' . $criteriaVo->getPrenom() . '%', Criteria::LIKE);
        }
        if ($criteriaVo->getEmail()) {
            $c->add(CommonAgentPeer::EMAIL, '%' . $criteriaVo->getEmail() . '%', Criteria::LIKE);
        }
        if ($criteriaVo->getIdentifiant()) {
            $c->add(CommonAgentPeer::LOGIN, '%' . $criteriaVo->getIdentifiant() . '%', Criteria::LIKE);
        }
        $c->add(CommonAgentPeer::DELETED_AT, null, Criteria::ISNULL);

        if (0 === $criteriaVo->getOrganisme() || '0' === $criteriaVo->getOrganisme()) {
            $c->addJoin(CommonAgentPeer::ORGANISME, CommonOrganismePeer::ACRONYME);
            $c->add(CommonOrganismePeer::ACTIVE, '0', Criteria::NOT_EQUAL);
        }

        if ($criteriaVo->getOrganisme()) {
            $c->add(CommonAgentPeer::ORGANISME, $criteriaVo->getOrganisme(), Criteria::EQUAL);
            if ($criteriaVo->getInclureDesc()) {
                $listeServices = [];
                if ($criteriaVo->getService()) {
                    $listeServices = Atexo_EntityPurchase::getSubServices($criteriaVo->getService(), $criteriaVo->getOrganisme(), true, false);
                    $c->add(CommonAgentPeer::SERVICE_ID, $listeServices, Criteria::IN);
                } else {
                    $listeServices = Atexo_EntityPurchase::getEntityPurchaseById($criteriaVo->getOrganisme());
                    if (is_array($listeServices) && count($listeServices)) {
                        $c->add(CommonAgentPeer::SERVICE_ID, $listeServices, Criteria::IN);
                        $c->addOr(CommonAgentPeer::SERVICE_ID, null, Criteria::EQUAL);
                    }
                }
            } elseif ($criteriaVo->getService() > 0) {
                $c->add(CommonAgentPeer::SERVICE_ID, $criteriaVo->getService(), Criteria::EQUAL);
            } else {
                $c->add(CommonAgentPeer::SERVICE_ID, null, Criteria::ISNULL);
            }
        }
        if ($criteriaVo->getLimit()) {
            $c->setLimit($criteriaVo->getLimit());
        }
        if ($criteriaVo->getOffset()) {
            $c->setOffset($criteriaVo->getOffset());
        }
        if ($criteriaVo->getSortByElement()) {
            $peer = new \ReflectionClass('CommonAgentPeer');
            $column = strtoupper($criteriaVo->getSortByElement());
            if ('DESC' == $criteriaVo->getSensOrderBy()) {
                $c->addDescendingOrderByColumn($peer->getConstant($column));
            } else {
                $c->addAscendingOrderByColumn($peer->getConstant($column));
            }
        }
        if ($criteriaVo->getReferentielVo()) {
            $IdAgentByReferentiel = (new Atexo_Referentiel_Referentiel())->searchAgent($criteriaVo->getReferentielVo(), $criteriaVo->getOrganisme(), $withRef);
            if ($IdAgentByReferentiel) {
                $c->add(CommonAgentPeer::ID, $IdAgentByReferentiel, Criteria::IN);
            }
        }
        if ($criteriaVo->getListeIdsAgents()) {
            $c->add(CommonAgentPeer::ID, $criteriaVo->getListeIdsAgents(), Criteria::IN);
        }
        if (true === $criteriaVo->getActif()) {
            $c->add(CommonAgentPeer::ACTIF, '1');
        } elseif (false === $criteriaVo->getActif()) {
            $c->add(CommonAgentPeer::ACTIF, '0');
        }

        if (Atexo_Module::isEnabled('AfficherCodeService')) {
            $criteriaService = new Criteria(); //service
            $criteriaOrganisme = new Criteria(); //organisme
            $cSer = false;
            if (0 != $criteriaVo->getCodeAcheteur()) {
                $cSer = true;
                $criteriaService->add(CommonServicePeer::SIREN, $criteriaVo->getCodeAcheteur(), Criteria::LIKE);
                $criteriaOrganisme->add(CommonOrganismePeer::SIREN, $criteriaVo->getCodeAcheteur(), Criteria::LIKE);
            }
            if ('' != $criteriaVo->getLibelleAcheteur()) {
                $cSer = true;
                $criteriaService->add(CommonServicePeer::LIBELLE, '%' . $criteriaVo->getLibelleAcheteur() . '%', Criteria::LIKE);
                $criteriaOrganisme->add(CommonOrganismePeer::DENOMINATION_ORG, '%' . $criteriaVo->getLibelleAcheteur() . '%', Criteria::LIKE);
            }
            if ($cSer) {
                if ($criteriaVo->getOrganisme()) {
                    $criteriaService->add(CommonServicePeer::ORGANISME, $criteriaVo->getOrganisme(), Criteria::EQUAL);
                    $criteriaOrganisme->add(CommonOrganismePeer::ACRONYME, $criteriaVo->getOrganisme(), Criteria::EQUAL);
                }
                $Services = CommonServicePeer::doSelect($criteriaService, $connexionCom);
                $Organismes = CommonOrganismePeer::doSelect($criteriaOrganisme, $connexionCom);
            }
            if (count((array) $Services)) {
                foreach ($Services as $oneService) {
                    $IdsServices[] = ['id' => $oneService->getId(), 'org' => $oneService->getOrganisme()];
                }
            }
            $index = 0;
            if (is_countable($IdsServices) ? count($IdsServices) : 0) {
                foreach ($IdsServices as $oneService) {
                    $cton1 = $c->getNewCriterion(CommonAgentPeer::ORGANISME, $oneService['org'], Criteria::LIKE);
                    $cton2 = $c->getNewCriterion(CommonAgentPeer::SERVICE_ID, $oneService['id']);
                    $cton1->addAnd($cton2);
                    if (!$index) {
                        $ctonAcheteur = $cton1;
                    } else {
                        $ctonAcheteur->addOr($cton1);
                    }
                    ++$index;
                }
            }
            if (count((array) $Organismes)) {
                foreach ($Organismes as $oneOrg) {
                    $orgAcronyme[] = $oneOrg->getAcronyme();
                }
            }
            if (is_countable($orgAcronyme) ? count($orgAcronyme) : 0) {
                $ctonOrg = $c->getNewCriterion(CommonAgentPeer::ORGANISME, $orgAcronyme, Criteria::IN);
                if ($ctonAcheteur instanceof Criterion) {
                    $ctonAcheteur->addOr($ctonOrg);
                }
            }
            if ($ctonAcheteur) {
                $c->add($ctonAcheteur);
            } elseif ($ctonOrg) {
                $c->add($ctonOrg);
            } elseif ($cSer) {
                $c->add(CommonAgentPeer::ORGANISME, null);
            }
        }
        if (!$returnJustNumberOfElement) {
            $ListeAgent = CommonAgentPeer::doSelect($c, $connexionCom);
            if ($ListeAgent) {
                $refsAgent = (new Atexo_Referentiel_Referentiel())->getAllLtReferentielForAgent();
                foreach ($ListeAgent as $agent) {
                    $entityPath = Atexo_EntityPurchase::getSigleLibelleEntityById($agent->getServiceId(), $agent->getOrganisme());
                    $agent->setEntityPath($entityPath);
                    $tabReferentielAgent = [];
                    foreach ($refsAgent as $ref) {
                        $agentRef = (new Atexo_Referentiel_Referentiel())->getCommonLtReferentielByIdAgent($agent->getId(), $ref->getId());
                        if ($agentRef) {
                            $value = $agentRef->getValeurPrincipaleLtReferentiel() . $agentRef->getValeurSecondaireLtReferentiel();
                        } else {
                            $value = '';
                        }
                        $tabReferentielAgent[Prado::localize($ref->getCodeLibelle())] = $value;
                    }
                    $agent->setReferentiel($tabReferentielAgent);
                    $org = Atexo_Organismes::retrieveOrganismeByAcronyme($agent->getOrganisme());
                    if ($org instanceof CommonOrganisme) {
                        $orgDenomination = $org->getDenominationOrg();
                        $agent->setDenomonationOrg($orgDenomination);
                    }
                }

                return $ListeAgent;
            } else {
                return [];
            }
        } else {
            return CommonAgentPeer::doCount($c, true, $connexionCom);
        }
    }

    public function getSso($idAgent, $idServiceMetier)
    {
        $agent = self::retrieveAgent($idAgent);
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonSsoAgentPeer::ID_AGENT, $agent->getId());
        $c->add(CommonSsoAgentPeer::ORGANISME, $agent->getOrganisme());
        $c->add(CommonSsoAgentPeer::SERVICE_ID, $idServiceMetier);
        $c->add(CommonSsoAgentPeer::DATE_LAST_REQUEST, date('Y-m-d H:i:s'), Criteria::GREATER_THAN);
        $c->addDescendingOrderByColumn(CommonSsoAgentPeer::DATE_LAST_REQUEST);
        $ssoAgent = CommonSsoAgentPeer::doSelectOne($c, $connexionCom);
        if ($ssoAgent instanceof CommonSsoAgent) {
            return $ssoAgent->getIdSso();
        }

        return false;
    }

    public function getLoginById($idAgent)
    {
        $agent = self::retrieveAgent($idAgent);
        if ($agent instanceof CommonAgent) {
            return $agent->getLogin();
        }

        return '';
    }

    public function getSsoValide($idAgent, $organisme, $idServiceMetier)
    {
        $sso = self::getSso($idAgent, $idServiceMetier);
        if (!$sso) {
            $ssoAgent = new CommonSsoAgent();
            $sso = uniqid();
            $ssoAgent->setIdSso($sso);
            $ssoAgent->setIdAgent($idAgent);
            $ssoAgent->setDateConnexion(date('Y-m-d H:i:s'));
            $ssoAgent->setOrganisme($organisme);
            $ssoAgent->setDateLastRequest(Atexo_Util::dateDansFutur(date('Y-m-d H:i:s'), Atexo_Config::getParameter('NBR_HEURE_MAINTENANCE_CNX')));
            $ssoAgent->setServiceId($idServiceMetier);
            $ssoAgent->save(Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE')));
        }

        return $sso;
    }

    /*
     * notifier l'agent de son inscription et envoi du pwd
     *
     */
    public function notifierAgent($agent, $pwd)
    {
        $from = Atexo_Config::getParameter('PF_MAIL_FROM');
        $to = $agent->getEmail();
        $subject = Prado::localize('DEFINE_TEXT_OBJET_CREATION_COMPTE');
        $message = Atexo_Message::getMessageModificationCompte($agent, $pwd);
        Atexo_Message::simpleMail($from, $to, $subject, $message, '', '', false, true);
    }

    /**
     * retourne la liste des agent redacteurs depuis une liste des agents.
     *
     * @param array $agents liste des agents
     *
     * @return array $redacteurs liste de agents redacteurs
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.10
     *
     * @copyright Atexo 2015
     */
    public function getRedacteurs($agents)
    {
        $redacteurs = [];
        foreach ($agents as $agent) {
            $habilitationAgent = $agent->getCommonHabilitationAgents();
            $habilitationAgentObject = $habilitationAgent[0];
            if ($habilitationAgentObject instanceof CommonHabilitationAgent) {
                if ('1' == $habilitationAgentObject->getRedactionDocumentsRedac()) {
                    $redacteurs[] = $agent;
                }
            }
        }

        return $redacteurs;
    }

    /**
     * Permet de retourner le mot de passe d'un agent sous sha1 ou sha256.
     *
     * @param $login
     * @param $passowrd
     *
     * @return string pwd hasher
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2018-esr
     *
     * @copyright Atexo 2018
     */
    public function getPswAgentAfterVerification($login, $passowrd)
    {
        $pwd = sha1($passowrd);
        if (Atexo_Config::getParameter('ENABLE_SHA256_PASSWORDS')) {
            $agentPhp = new Atexo_Agent();
            $user = $agentPhp->retrieveAgentByLogin($login);
            if (($user instanceof CommonAgent) && 'SHA256' == $user->getTypeHash()) {
                $pwd = hash('sha256', Atexo_Config::getParameter('SALT_POUR_MOT_DE_PASSE') . $passowrd);
            }
        }

        return $pwd;
    }

    /**
     * Permet de mettre à jour l'agent avec psw en sha256 si le parametre ENABLE_SHA256_PASSWORDS est activé
     * et l'agent avais sha1.
     *
     * @param $agent
     * @param $passowrd
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2018-esr
     *
     * @copyright Atexo 2018
     */
    public static function updatePswAgentBySHA256(&$agent, $passowrd)
    {
        if (Atexo_Config::getParameter('ENABLE_SHA256_PASSWORDS') && ($agent instanceof CommonAgent) && 'SHA1' == $agent->getTypeHash()) {
            $agent->setTypeHash('SHA256');
            $agent->setPassword(hash('sha256', Atexo_Config::getParameter('SALT_POUR_MOT_DE_PASSE') . $passowrd));
        }
    }

    /**
     * Permet de mettre à jour l'agent avec psw en sha256 si le parametre ENABLE_SHA256_PASSWORDS est activé.
     *
     * @param $agent
     * @param $passowrd
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2018-esr
     *
     * @copyright Atexo 2018
     */
    public static function updateNewAgentPswBySHA256(&$agent, $passowrd)
    {
        if (Atexo_Config::getParameter('ENABLE_SHA256_PASSWORDS')) {
            $agent->setTypeHash('SHA256');
            $agent->setPassword(hash('sha256', Atexo_Config::getParameter('SALT_POUR_MOT_DE_PASSE') . $passowrd));
        } else {
            $agent->setTypeHash('SHA1');
            $agent->setPassword(sha1($passowrd));
        }
    }

    /**
     * retourne l'objet  Agent (nom, prénom, ...) équivalent à l'id externe et service id de l'agent passé en paramètre.
     *
     * @param $idAgent l'id de l'agent
     * @param $idService l'id de service
     *
     * @return Agent si l'agent est trouvé, false sinon
     */
    public function retrieveAgentByExternalIdAndService($idExterne, $idService)
    {
        $agentQuery = new CommonAgentQuery();

        return $agentQuery->getAgentByExternalIdAndService($idExterne, $idService);
    }

    /**
     * @param $idExterne
     * @param $acronyme
     *
     * @return mixed
     */
    public function retrieveAgentsByExternalIdAndOrganisme($idExterne, $acronyme, $idAgent = null, $connexion = null)
    {
        if (!$connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB')
                .
                Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
        $criteria = new Criteria();
        if (!is_null($idAgent)) {
            $criteria->add(CommonAgentPeer::ID, $idAgent, Criteria::NOT_EQUAL);
        }
        $criteria->add(CommonAgentPeer::ORGANISME, $acronyme);
        $criteria->add(CommonAgentPeer::ID_EXTERNE, $idExterne);

        return CommonAgentPeer::doSelect($criteria, $connexion);
    }

    public function retrieveAgentsAvecAlerteValidation($arrayIdsAgent)
    {
        $c = new Criteria();
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonAgentPeer::ALERTE_VALIDATION_CONSULTATION, '1', Criteria::EQUAL);
        $c->add(CommonAgentPeer::ACTIF, '0', Criteria::NOT_EQUAL);
        $c->add(CommonAgentPeer::ID, $arrayIdsAgent, Criteria::IN);
        $agentsObjects = CommonAgentPeer::doSelect($c, $connexionCom);

        return $agentsObjects;
    }

    public function getSsoForSocle($idAgent, $organisme, $idServiceMetier)
    {
        $sso = self::getSso($idAgent, $idServiceMetier);
        if (empty($sso)) {
            $ssoAgent = new CommonSsoAgent();
            $sso = Atexo_CurrentUser::dumpSession();
            $ssoAgent->setIdSso($sso);
            $ssoAgent->setIdAgent($idAgent);
            $ssoAgent->setDateConnexion(date('Y-m-d H:i:s'));
            $ssoAgent->setOrganisme($organisme);
            $ssoAgent->setDateLastRequest(Atexo_Util::dateDansFutur(date('Y-m-d H:i:s'), Atexo_Config::getParameter('NBR_HEURE_MAINTENANCE_CNX')));
            $ssoAgent->setServiceId($idServiceMetier);
            $ssoAgent->save(Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE')));
        }
        return $sso;
    }

    public static function updateNewAgentPasswordByArgon2(CommonAgent &$agent, string $password): void
    {
        $password = htmlspecialchars_decode($password);
        $agent->setTypeHash(Agent::ARGON_TYPE);
        $encodedPassword = Atexo_Util::getSfService(PradoPasswordEncoderInterface::class)
            ->encodePassword(Agent::class, $password);
        $agent->setPassword($encodedPassword);
    }

    public function loginUser(string $login, string $password): ?CommonAgent
    {
        $user = $this->retrieveAgentByLogin($login);
        if (!($user instanceof CommonAgent)) {
            return null;
        }

        if ($user->getTypeHash() == 'SHA1') {
            return null;
        }

        if ($user->getTypeHash() == 'SHA256') {
            $encodedPassword = hash('sha256', Atexo_Config::getParameter('SALT_POUR_MOT_DE_PASSE') . $password);
            $validUser = $this->retrieveAgentByLoginAndMdp($login, $encodedPassword);
            if (!($validUser instanceof CommonAgent)) {
                $this->incrementTentativesMdp($user);
                return null;
            }

            return $validUser;
        }

        if ($user->getTypeHash() == 'ARGON2') {
            $isValidPassword = Atexo_Util::getSfService(PradoPasswordEncoderInterface::class)
                ->isPasswordValid(Agent::class, $user->getPassword(), $password);
            if (false === $isValidPassword) {
                $this->incrementTentativesMdp($user);
                return null;
            }

            return $user;
        }

        return null;
    }

    public static function updatePswAgentByARGON2(CommonAgent &$agent, string $password): void
    {
        if ('SHA256' === $agent->getTypeHash()) {
            $agent->setTypeHash('ARGON2');
            $pass = Atexo_Util::getSfService(PradoPasswordEncoderInterface::class)
                ->encodePassword(Agent::class, $password);
            $agent->setPassword($pass);
        }
    }

    private function incrementTentativesMdp(CommonAgent $agent): void
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));
        $tentative = $agent->getTentativesMdp();
        $agent->setTentativesMdp(++$tentative);
        $agent->save($connexionCom);
    }
}
