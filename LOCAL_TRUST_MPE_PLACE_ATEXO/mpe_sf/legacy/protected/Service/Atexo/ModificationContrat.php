<?php

namespace Application\Service\Atexo;

use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonModificationContratQuery;
use Application\Propel\Mpe\CommonTContratTitulaire;
use Application\Propel\Mpe\CommonTContratTitulaireQuery;

/**
 * Class Atexo_ModiciationContrat.
 */
class Atexo_ModificationContrat
{
    /**
     * @param $idContrat
     *
     * @return CommonTContratTitulaire
     *
     * @throws PropelException
     */
    public function retrieveContratOneByIdContratTitulaire($idContrat)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $commonTContratTitulaireQuery = new CommonTContratTitulaireQuery(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $contrat = $commonTContratTitulaireQuery->findOneByIdContratTitulaire($idContrat, $connexion);

        return $contrat;
    }

    public function retrieveContratOneById($idModifcationContrat)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $commonModificationContratQuery = new CommonModificationContratQuery(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $modificationContrat = $commonModificationContratQuery->findOneById($idModifcationContrat, $connexion);

        return $modificationContrat;
    }
}
