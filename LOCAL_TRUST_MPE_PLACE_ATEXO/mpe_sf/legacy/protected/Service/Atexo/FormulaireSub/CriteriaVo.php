<?php

namespace Application\Service\Atexo\FormulaireSub;

/**
 * Vo pour définir les critères du formulaire SUB.
 *
 * @author KONATE Oumar (oumar.konate@atexo.com)
 * @copyright Atexo 2013
 *
 * @since MPE-4
 */
class Atexo_FormulaireSub_CriteriaVo
{
    private $_refCons;
    private $_organisme;
    private $_dateRemetre;
    private int $_offset = 0;
    private int $_limit = 0;
    private $_statutDemande;
    private $_typeReponseDossier;
    private $_idInscrit;
    //-
    private $_dateReference;
    private bool $_controlWithDateRemettre = false; //permet d'activer/desactiver le control sur le champ "date_a_remettre"
    //-
    private string $_orderByColumn = ''; //cet attribut s'utilise paralèllement avec l'attribut: $_sensTri
    private $_sensTri;
    //-
    private $_nomEntreprise;
    private $_typeDossier;
    private $_lot;
    private $_typeDemande;
    private $_arrayIds; //tableau des ids des demandes de compléments
    private $_motif;
    private $_etatMessageInformation = null;
    private $_etatReouverture = null;

    public function getRefCons()
    {
        return $this->_refCons;
    }

    public function setRefCons($value)
    {
        $this->_refCons = $value;
    }

    public function getDateRemetre()
    {
        return $this->_dateRemetre;
    }

    public function setDateRemetre($value)
    {
        $this->_dateRemetre = $value;
    }

    public function getOffset()
    {
        return $this->_offset;
    }

    public function setOffset($offset)
    {
        $this->_offset = $offset;
    }

    public function getLimit()
    {
        return $this->_limit;
    }

    public function setLimit($limit)
    {
        $this->_limit = $limit;
    }

    public function getStatutDemande()
    {
        return $this->_statutDemande;
    }

    public function setStatutDemande($value)
    {
        $this->_statutDemande = $value;
    }

    public function getTypeReponseDossier()
    {
        return $this->_typeReponseDossier;
    }

    public function setTypeReponseDossier($value)
    {
        $this->_typeReponseDossier = $value;
    }

    public function getIdInscrit()
    {
        return $this->_idInscrit;
    }

    public function setIdInscrit($value)
    {
        $this->_idInscrit = $value;
    }

    public function getDateReference()
    {
        return $this->_dateReference;
    }

    public function setDateReference($value)
    {
        $this->_dateReference = $value;
    }

    public function getControlWithDateRemettre()
    {
        return $this->_controlWithDateRemettre;
    }

    public function setControlWithDateRemettre($value)
    {
        $this->_controlWithDateRemettre = $value;
    }

    public function getOrderByColumn()
    {
        return $this->_orderByColumn;
    }

    public function setOrderByColumn($value)
    {
        $this->_orderByColumn = $value;
    }

    public function getSensTri()
    {
        return $this->_sensTri;
    }

    public function setSensTri($value)
    {
        $this->_sensTri = $value;
    }

    public function getNomEntreprise()
    {
        return $this->_nomEntreprise;
    }

    public function setNomEntreprise($value)
    {
        $this->_nomEntreprise = $value;
    }

    public function getTypeDossier()
    {
        return $this->_typeDossier;
    }

    public function setTypeDossier($value)
    {
        $this->_typeDossier = $value;
    }

    public function getLot()
    {
        return $this->_lot;
    }

    public function setLot($value)
    {
        $this->_lot = $value;
    }

    public function getTypeDemande()
    {
        return $this->_typeDemande;
    }

    public function setTypeDemande($value)
    {
        $this->_typeDemande = $value;
    }

    public function getOrganisme()
    {
        return $this->_organisme;
    }

    public function setOrganisme($value)
    {
        $this->_organisme = $value;
    }

    public function getArrayIds()
    {
        return $this->_arrayIds;
    }

    public function setArrayIds($value)
    {
        $this->_arrayIds = $value;
    }

    public function getMotif()
    {
        return $this->_motif;
    }

    public function setMotif($value)
    {
        $this->_motif = $value;
    }

    public function setEtatMessageInformation($value)
    {
        $this->_etatMessageInformation = $value;
    }

    public function getEtatMessageInformation()
    {
        return $this->_etatMessageInformation;
    }

    public function setEtatReouverture($value)
    {
        $this->_etatReouverture = $value;
    }

    public function getEtatReouverture()
    {
        return $this->_etatReouverture;
    }
}
