<?php

namespace Application\Service\Atexo\FormulaireSub;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonInscrit;
use Application\Propel\Mpe\Entreprise;
use Application\Propel\Mpe\EntreprisePeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_Xml;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Etablissement;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Inscrit;
use DOMDocument;
use DOMNodeList;
use Exception;
use GuzzleHttp\Client;

/**
 * Classe de.
 *
 * @author Khalid BENAMAR <khalid.benamar@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_FormulaireSub_Echange
{
    private string $encoding = 'UTF-8';
    public $codeErreur;
    public $messageErreur;

    const CONTENT_TYPE_XML = 'application/xml';

    public function generateXmlEntreprise($idEntreprise)
    {
        $xml = null;
        //$inscritAtexo_Entreprise_Inscrit::retrieveInscritById($idInscrit);
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $entrepriseO = EntreprisePeer::retrieveByPK($idEntreprise, $connexion);
        if ($entrepriseO instanceof Entreprise && $entrepriseO->getSiren()) {
            $domDocument = new DOMDocument('1.0', $this->encoding);
            $entreprise = (new Atexo_Xml())->addElement($domDocument, $domDocument, 'entreprise');
            (new Atexo_Xml())->addAttribute($domDocument, $entreprise, 'resultatNet', '0.0');
            (new Atexo_Xml())->addAttribute($domDocument, $entreprise, 'chiffreDAffaire', '0.0');
            (new Atexo_Xml())->addAttribute($domDocument, $entreprise, 'liquidation', 'false');
            (new Atexo_Xml())->addAttribute($domDocument, $entreprise, 'redressementJuridique', 'false');
            (new Atexo_Xml())->addAttribute($domDocument, $entreprise, 'nombreApprentis', '0.0');
            (new Atexo_Xml())->addAttribute($domDocument, $entreprise, 'nombreInterimaire', '0.0');
            (new Atexo_Xml())->addAttribute($domDocument, $entreprise, 'nombreCDI', '0.0');
            (new Atexo_Xml())->addAttribute($domDocument, $entreprise, 'nombreCDD', '0.0');
            (new Atexo_Xml())->addAttribute($domDocument, $entreprise, 'appartenanceGroupe', 'false');
            (new Atexo_Xml())->addAttribute($domDocument, $entreprise, 'creationEncours', 'false');
            (new Atexo_Xml())->addAttribute($domDocument, $entreprise, 'siteInternet', Atexo_Util::toUtf8($entrepriseO->getSiteInternet())); /////////////////
            (new Atexo_Xml())->addAttribute($domDocument, $entreprise, 'telecopie', Atexo_Util::toUtf8($entrepriseO->getFax())); ///////////////////
            (new Atexo_Xml())->addAttribute($domDocument, $entreprise, 'idExterne', Atexo_Util::toUtf8($entrepriseO->getId())); //////////////////
            $siret = $entrepriseO->getSiren();
            $siret .= (new Atexo_Entreprise_Etablissement())->getCodeEtablissemntByIdEtab(Atexo_CurrentUser::getIdEtablissement());
            (new Atexo_Xml())->addAttribute($domDocument, $entreprise, 'siret', Atexo_Util::toUtf8($siret)); ////////////////////////
            (new Atexo_Xml())->addAttribute($domDocument, $entreprise, 'adressePostale', 'adressePostale');
            (new Atexo_Xml())->addAttribute($domDocument, $entreprise, 'tiersEtat', 'actif');
            (new Atexo_Xml())->addAttribute($domDocument, $entreprise, 'tiersStatut', 'tiersAdministratif');
            (new Atexo_Xml())->addAttribute($domDocument, $entreprise, 'tiersType', 'entrepriseCommerciale');
            (new Atexo_Xml())->addElement($domDocument, $entreprise, 'nomComplet', Atexo_Util::toUtf8($entrepriseO->getNom()));
            (new Atexo_Xml())->addElement($domDocument, $entreprise, 'ribs');
            $contacts = (new Atexo_Xml())->addElement($domDocument, $entreprise, 'contacts');
            $inscrits = (new Atexo_Entreprise_Inscrit())->retrieveInscritByIdCompnay(Atexo_Util::toUtf8($entrepriseO->getId()));
            if ($inscrits && is_array($inscrits)) {
                foreach ($inscrits as $oneInscrit) {
                    $oneContact = null;
                    $oneContact = (new Atexo_Xml())->addElement($domDocument, $contacts, 'contact');
                    (new Atexo_Xml())->addAttribute($domDocument, $oneContact, 'id', Atexo_Util::toUtf8($oneInscrit->getId()));
                    (new Atexo_Xml())->addAttribute($domDocument, $oneContact, 'login', Atexo_Util::toUtf8($oneInscrit->getLogin())); ////////////////////
                    (new Atexo_Xml())->addAttribute($domDocument, $oneContact, 'representantLegal', 'false');
                    if ($oneInscrit->getProfil() == Atexo_Config::getParameter('PROFIL_INSCRIT_ATES')) {
                        $principal = 'true';
                    } else {
                        $principal = 'false';
                    }
                    (new Atexo_Xml())->addAttribute($domDocument, $oneContact, 'principal', $principal);
                    (new Atexo_Xml())->addAttribute($domDocument, $oneContact, 'email', Atexo_Util::toUtf8($oneInscrit->getEmail())); ////////////////////////
                    (new Atexo_Xml())->addAttribute($domDocument, $oneContact, 'prenom', Atexo_Util::toUtf8($oneInscrit->getPrenom())); //////////////////////
                    (new Atexo_Xml())->addAttribute($domDocument, $oneContact, 'nom', Atexo_Util::toUtf8($oneInscrit->getNom())); //////////////////////
                }
            }

            //$enseigne = Atexo_Xml::addElement ($domDocument, $entreprise, "enseigne");

            $xml = $domDocument->saveXml();
        }

        return $xml;
    }

    public function sendEntrepriseToSUB($idEntreprise)
    {
        $res = null;
        try {
            $xml = $this->generateXmlEntreprise($idEntreprise); //echo $xml;exit;
            $messageLog = '';
            $client = new Client();
            $uri = Atexo_Config::getParameter('URL_SUB_REST_CREATION_INSCRIT');

            if ($xml) {
                $options = [
                    'headers' => [
                        'Content-Type' => self::CONTENT_TYPE_XML
                    ],
                    'body' => $xml,
                ];
            }
            $options['auth'] = self::getLoginAndPassForSub();
            $res = $client->Post($uri, $options)->getBody();
            //print_r($res); exit;
            $domDoc = new DOMDocument();
            $domDoc->loadXML($res);
            $reponse = $domDoc->getElementsByTagName('reponse');
            if ($reponse->length) {
                foreach ($reponse as $oneReponse) {
                    if ('OK' != (string) $oneReponse->getAttribute('statutReponse')) {
                        $messages = $oneReponse->getElementsByTagName('messages');
                        foreach ($messages as $onemessages) {
                            $retourSUB = (string) $onemessages->nodeValue;
                        }
                        $messageLog = date('Y-m-d H:i:s').' : '.'Atexo_FormulaireSub_Echange CREATION/MAJ non effectue ('.Atexo_Util::fromUtf8ToHttpEncoding($retourSUB).") \n";
                    }
                }
            } else {
                $messageLog = date('Y-m-d H:i:s').' : '."Atexo_FormulaireSub_Echange Balise reponse non presente sur le retour de SUB \n";
            }
            if ($messageLog) {
                Atexo_Util::writeLogFile(
                    Atexo_Config::getParameter('LOG_FILE_PATH_GESTION_FORMULAIRE_SUB'),
                    Atexo_Config::getParameter('LOG_FILE_PATH_GESTION_FORMULAIRE_SUB').'gestionFormulaireSub.log',
                    $messageLog,
                    'a'
                );
                echo $messageLog;
                exit;
            }
        } catch (Exception $e) {
            $messageException = date('Y-m-d H:i:s').' : '.'Atexo_FormulaireSub_Echange Erreur création tier'.$e->getMessage()." \n";
            Atexo_Util::writeLogFile(
                Atexo_Config::getParameter('LOG_FILE_PATH_GESTION_FORMULAIRE_SUB'),
                Atexo_Config::getParameter('LOG_FILE_PATH_GESTION_FORMULAIRE_SUB').'gestionFormulaireSub.log',
                $messageException,
                'a'
            );
            echo 'Erreur création tier'.$this->messageErreur.' '.print_r($res, true);
            exit;
        }
        //print_r($res); exit;
    }

    /**
     * Permet de recuperer le flux xml des éditions dépuis SUB.
     */
    public function recupererXmlEditions($cleExterneDossier)
    {
        $xml = null;
        try {
            $client = new Client();
            $uri = Atexo_Config::getParameter('URL_SUB_REST_EDITION_SERVICE').'&iddossier='.$cleExterneDossier;
            $options['auth'] = self::getLoginAndPassForSub();
            $xml = $client->Get($uri, $options)->getBody();
            $domDoc = new DOMDocument();
            $domDoc->loadXML($xml);

            return $domDoc->saveXml();
        } catch (Exception $e) {
            $messageException = date('Y-m-d H:i:s').' : '.'Atexo_FormulaireSub_Echange Erreur recuperer Xml Editions'.$e->getMessage().'Retour SUB '.print_r($xml, true)." \n";
            Atexo_Util::writeLogFile(Atexo_Config::getParameter('LOG_FILE_PATH_GESTION_FORMULAIRE_SUB'), Atexo_Config::getParameter('LOG_FILE_PATH_GESTION_FORMULAIRE_SUB').'gestionFormulaireSub.log', $messageException, 'a');
            echo 'Erreur création tier'.$this->messageErreur.' '.print_r($xml, true);
        }
    }

    /**
     * Permet de recuperer le flux xml de la demande génération des éditions depuis SUB.
     */
    public function recupererXmlDemandeGenerationDocuments($idDocument, $cleExterneDossier)
    {
        $xml = null;
        try {
            $clientConfig['timeout'] = 300;
            $client = new Client($clientConfig);
            $uri = Atexo_Config::getParameter('URL_SUB_REST_DEMANDE_GENERATION_DOCUMENT').'&idedition='.$idDocument.'&iddossier='.$cleExterneDossier;
            $options['auth'] = self::getLoginAndPassForSub();

            $xml = $client->Get($uri, $options)->getBody();
            //print_r($xml);
            $domDoc = new DOMDocument();
            $domDoc->loadXML($xml);

            return $domDoc->saveXml();
        } catch (Exception $e) {
            $messageException = date('Y-m-d H:i:s').' : '.'Atexo_FormulaireSub_Echange Erreur recuperer Xml Demande Generation Documents '.$e->getMessage().'Retour SUB '.print_r($xml, true)." \n";
            Atexo_Util::writeLogFile(Atexo_Config::getParameter('LOG_FILE_PATH_GESTION_FORMULAIRE_SUB'), Atexo_Config::getParameter('LOG_FILE_PATH_GESTION_FORMULAIRE_SUB').'gestionFormulaireSub.log', $messageException, 'a');
            echo 'Erreur generation edition '.$messageException.' '.print_r($xml, true);
        }
    }

    /**
     * Permet de recuperer le flux xml des demandes de compléments dépuis SUB.
     */
    public function recupererXmlDemandesComplement($idDispositif = null)
    {
        $xml = null;
        try {
            $client = new client();
            $uri = Atexo_Config::getParameter('URL_SUB_REST_DEMANDE_COMPLEMENTS').'iddispositif='.$idDispositif;
            $options['auth'] = self::getLoginAndPassForSub();
            $xml = $client->Get($uri, $options)->getBody();
            $domDoc = new DOMDocument();
            $domDoc->loadXML($xml);

            return $domDoc->saveXml();
        } catch (Exception $e) {
            $messageException = date('Y-m-d H:i:s').' : '.'Atexo_FormulaireSub_Echange Erreur recuperer Xml demande compléments '.$e->getMessage().' Retour SUB '." \n";
            $messageException .= print_r($xml, true);
            $logger = Atexo_LoggerManager::getLogger('publicite');
            $logger->error($messageException);
        }
    }

    /**
     * Permet d'envoyer la liste des id dossiers pour ouvrire au ligne au SUB.
     */
    public function sendListeIdDossiersToSUB($listeIdDocument)
    {
        $messageLog = null;
        $res = null;
        try {
            $arrayReponse = [];
            $xml = self::generateXmlReponse($listeIdDocument);
            $client = new Client();
            $uri = Atexo_Config::getParameter('URL_SUB_DEMANDE_VALIDATION_DOSSIERS');
            if ($xml) {
                $options = [
                    'headers' => [
                        'Content-Type' => self::CONTENT_TYPE_XML
                    ],
                    'body' => $xml,
                ];
            }
            $options['auth'] = self::getLoginAndPassForSub();
            $res = $client->Post($uri, $options)->getBody();
            $domDoc = new DOMDocument();
            $domDoc->loadXML($res);
            $reponse = $domDoc->getElementsByTagName('reponseDossier');
            if ($reponse->length) {
                foreach ($reponse as $oneReponse) {
                    if ('OK' != (string) $oneReponse->getAttribute('statutReponse')) {
                        $messages = $oneReponse->getElementsByTagName('messages');
                        foreach ($messages as $onemessages) {
                            $retourSUB = (string) $onemessages->nodeValue;
                        }
                        $messageLog .= date('Y-m-d H:i:s').' : '.'Atexo_FormulaireSub_Echange Dossier '.
                                      $oneReponse->getAttribute('identifiant').'STATUT reponse KO ('.Atexo_Util::fromUtf8ToHttpEncoding($retourSUB).") \n ";
                    } elseif ('OK' == (string) $oneReponse->getAttribute('statutReponse')) {
                        $idDossier = $oneReponse->getAttribute('identifiant');
                        $arrayReponse[] = $idDossier;
                    }
                }
            } else {
                $messageLog = date('Y-m-d H:i:s').' : '."Atexo_FormulaireSub_Echange Balise reponse non presente sur le retour de SUB \n ";
            }
            if ($messageLog) {
                Atexo_Util::writeLogFile(Atexo_Config::getParameter('LOG_FILE_PATH_GESTION_FORMULAIRE_SUB'), Atexo_Config::getParameter('LOG_FILE_PATH_GESTION_FORMULAIRE_SUB').'gestionFormulaireSub.log', $messageLog, 'a+');
                echo $messageLog;
            }

            return $arrayReponse;
        } catch (Exception $e) {
            $messageException = date('Y-m-d H:i:s').' : '.'Atexo_FormulaireSub_Echange Erreur Envoi de la liste des IDS document pour demande validation dossier pour ouverture de reponse '.$e->getMessage()."  \n ";
            Atexo_Util::writeLogFile(Atexo_Config::getParameter('LOG_FILE_PATH_GESTION_FORMULAIRE_SUB'), Atexo_Config::getParameter('LOG_FILE_PATH_GESTION_FORMULAIRE_SUB').'gestionFormulaireSub.log', $messageException, 'a+');
            echo "Erreur d'envoi de la liste des IDS documents ".$this->messageErreur.' '.print_r($res, true);
            echo $e->getMessage();
        }
    }

    /**
     * Permet de génerer le flux xml de demande de validation des dossier cote SUB.
     */
    public function generateXmlReponse($listeIdDocument)
    {
        $domDocument = new DOMDocument('1.0', $this->encoding);
        $dossiers = (new Atexo_Xml())->addElement($domDocument, $domDocument, 'dossiers');
        foreach ($listeIdDocument as $id) {
            $dossier = (new Atexo_Xml())->addElement($domDocument, $dossiers, 'dossier');
            (new Atexo_Xml())->addAttribute($domDocument, $dossier, 'idDossier', $id);
        }
        $xml = $domDocument->saveXml();

        return $xml;
    }

    /**
     * Permet demander le passage en instruction d'un dossier AU MOMENT DU DEPOT DE LA DEMANDE DE COMPLEMENT
     * (SUB).
     */
    public function demanderValidationIdDossiersDemandeComplementToSUB($idDossier)
    {
        $res = null;
        try {
            $messageLog = '';

            if ($idDossier) {
                $ref = Atexo_Util::atexoHtmlEntities($_GET['id']);
                //deverouiller le dossier chez SUB - tous les verrous du dossier sont supprimes (loginCompte ignore)
                (new Atexo_FormulaireSub_Echange())->unlockDossier($idDossier, 'mpe', $ref);
            }

            $client = new Client();
            $uri = Atexo_Config::getParameter('URL_SUB_DEMANDE_VALIDATION_DOSSIER_DEMANDE_COMPLEMENT').$idDossier;
            $options['auth'] = self::getLoginAndPassForSub();
            $res = $client->Get($uri, $options)->getBody();
            $domDoc = new DOMDocument();
            $domDoc->loadXML($res);
            $reponse = $domDoc->getElementsByTagName('reponse');
            if ($reponse->length) {
                foreach ($reponse as $oneReponse) {
                    if ('OK' == (string) $oneReponse->getAttribute('statutReponse')) {
                        return true;
                    }
                }
            } else {
                $messageLog .= date('Y-m-d H:i:s').' : '."Atexo_FormulaireSub_Echange Balise reponse non presente sur le retour de SUB \n ";
            }
            if ($messageLog) {
                Atexo_Util::writeLogFile(Atexo_Config::getParameter('LOG_FILE_PATH_GESTION_FORMULAIRE_SUB'), Atexo_Config::getParameter('LOG_FILE_PATH_GESTION_FORMULAIRE_SUB').'gestionFormulaireSub.log', $messageLog, 'a+');
                echo $messageLog;
            }

            return false;
        } catch (Exception $e) {
            $messageLog .= date('Y-m-d H:i:s').' : '."Atexo_FormulaireSub_Echange Erreur Envoi de l'ID du dossier au moment du dépot de la demande de complement ".$e->getMessage()."  \n ";
            Atexo_Util::writeLogFile(Atexo_Config::getParameter('LOG_FILE_PATH_GESTION_FORMULAIRE_SUB'), Atexo_Config::getParameter('LOG_FILE_PATH_GESTION_FORMULAIRE_SUB').'gestionFormulaireSub.log', $messageLog, 'a+');
            echo "Erreur d'envoi de l'id dossier au moment du dépot de la demande de complement ".print_r($res, true);
            echo $e->getMessage();
        }
    }

    /**
     * Permet de créer le dossier d'offre dans SUB avec les méta-données contenues dans l'xml.
     *
     * @param $xml : xml à poster à SUB
     */
    public function CreerDossierOffresSub($xml)
    {
        $messageLog = null;
        $res = null;
        try {
            $client = new Client();
            $uri = Atexo_Config::getParameter('URL_SUB_REST_CREATION_DOSSIER_OFFRE');
            if ($xml) {
                $options = [
                    'headers' => [
                        'Content-Type' => self::CONTENT_TYPE_XML
                    ],
                    'body' => $xml,
                ];
            }
            $options['auth'] = self::getLoginAndPassForSub();
            $res = $client->Post($uri, $options)->getBody();
            //print_r($res); exit;
            $domDoc = new DOMDocument();
            $domDoc->loadXML($res);
            $reponsegets = $domDoc->getElementsByTagName('reponseget');
            $idDossier = '';
            if ($reponsegets instanceof DOMNodeList) {
                foreach ($reponsegets as $reponseget) {
                    if ('OK' == (string) $reponseget->getAttribute('statut')) {
                        $dossiersubs = $reponseget->getElementsByTagName('dossiersub');
                        foreach ($dossiersubs as $dossiersub) {
                            $idDossier = $dossiersub->getAttribute('idDossier');
                        }
                    } else {
                        $messageLog = date('Y-m-d H:i:s').' : '."Atexo_FormulaireSub_Echange : [reponse xml :<<KO>>] \n";

                        return false;
                    }
                }
            } else {
                $messageLog = date('Y-m-d H:i:s').' : '."Atexo_FormulaireSub_Echange : [l'xml ne contient pas la balise : <reponseget/>] \n";

                return false;
            }
            if ($messageLog) {
                Atexo_Util::writeLogFile(
                    Atexo_Config::getParameter('LOG_FILE_PATH_GESTION_FORMULAIRE_SUB'),
                    Atexo_Config::getParameter('LOG_FILE_PATH_GESTION_FORMULAIRE_SUB').'gestionFormulaireSub.log',
                    $messageLog,
                    'a'
                );
                echo $messageLog;
                exit;
            }

            return $idDossier;
        } catch (Exception $e) {
            $messageException = date('Y-m-d H:i:s').' : '.'Atexo_FormulaireSub_Echange Erreur création tier'.$e->getMessage()." \n";
            Atexo_Util::writeLogFile(
                Atexo_Config::getParameter('LOG_FILE_PATH_GESTION_FORMULAIRE_SUB'),
                Atexo_Config::getParameter('LOG_FILE_PATH_GESTION_FORMULAIRE_SUB').'gestionFormulaireSub.log',
                $messageException,
                'a'
            );
            echo $messageException.' : '.print_r($res, true);
            exit;
        }
    }

    /**
     * permet d'invoquer un WS pour liberer un dossier chez SUB.
     *
     * @param $idDossier : id du dossier à liberer
     * @param $loginUser : login de l'iscrit qui declenche l'action
     * @param $ConsRefUser : reference utilisateur de la consultation
     *
     * @return bool
     */
    public function unlockDossier($idDossier, $loginUser, $ConsRefUser)
    {
        $messageLog = null;
        try {
            $client = new Client();
            $uri = Atexo_Config::getParameter('URL_SUB_REST_UNLOCK_DOSSIER')
                . "?iddossier=$idDossier&logincompte=$loginUser";
            $options['auth'] = self::getLoginAndPassForSub();
            $res = $client->Get($uri, $options)->getBody();
            $domDoc = new DOMDocument();
            $domDoc->loadXML($res);
            $reponse = $domDoc->getElementsByTagName('reponseget');
            if ($reponse->length) {
                foreach ($reponse as $oneReponse) {
                    if ('OK' != (string) $oneReponse->getAttribute('statut')) {
                        $messageLog = date('Y-m-d H:i:s').' : '.'impossible de deverouiller le dossier numero : '.$idDossier.' de la consultation : '.$ConsRefUser."\n retourSub : ".$domDoc->saveXML()."\n";
                    }
                }
            }
            if ($messageLog) {
                Atexo_Util::writeLogFile(
                    Atexo_Config::getParameter('LOG_FILE_PATH_GESTION_FORMULAIRE_SUB'),
                    Atexo_Config::getParameter('LOG_FILE_PATH_GESTION_FORMULAIRE_SUB').'gestionFormulaireSub.log',
                    $messageLog,
                    'a'
                );
            }
        } catch (Exception $e) {
            Atexo_Util::writeLogFile(
                Atexo_Config::getParameter('LOG_FILE_PATH_GESTION_FORMULAIRE_SUB'),
                Atexo_Config::getParameter('LOG_FILE_PATH_GESTION_FORMULAIRE_SUB').'gestionFormulaireSub.log',
                $messageLog."\n".$e->getMessage(),
                'a'
            );
        }
    }

    /**
     * Permet d'envoyer la requete de mise a jour des dossiers a SUB.
     *
     * @param string $idDossier     : identifiant du dossier
     * @param array  $infosDossiers :  tableau contenant les infos de mise a jour
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function updateDossierInterfaceSub($idDossier, array $infosDossiers)
    {
        $xml = null;
        try {
            $xml = self::getXmlUpdateDossierInterfaceSub($idDossier, $infosDossiers);
            $client = new Client();
            $uri = Atexo_Config::getParameter('URL_SUB_REST_UPDATE_DOSSIER_OFFRE');
            if ($xml) {
                $options = [
                    'headers' => [
                        'Content-Type' => self::CONTENT_TYPE_XML
                    ],
                    'body' => $xml,
                ];
            }
            $options['auth'] = self::getLoginAndPassForSub();
            $res = $client->Post($uri, $options)->getBody();
            $logger = Atexo_LoggerManager::getLogger('publicite');
            $logger->info('Retour du ws URL_SUB_REST_UPDATE_DOSSIER_OFFRE id dossier : '.$idDossier.' Message : '.print_r($res, true).". XML = [$xml]");
        } catch (Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('publicite');
            $logger->error("Erreur lors de l'envoi de la requete a l'interface SUB : id dossier : ".$idDossier.' Erreur : '.$e->getMessage().". XML = [$xml]");
        }
    }

    /**
     * Permet de construire l'xml de la requete de mise a jour pour les dossiers SUB.
     *
     * @param string $idDossier     : identifiant du dossier
     * @param array  $infosDossiers :  tableau contenant les infos de mise a jour
     *
     * @return string : xml de la requete
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getXmlUpdateDossierInterfaceSub($idDossier, array $infosDossiers)
    {
        $xml = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>';
        $xml .= '<dossiersub idDossier="'.$idDossier.'">';
        $xml .= '<dossier_datas>';
        foreach ($infosDossiers as $infosDossier) {
            foreach ($infosDossier as $info) {
                $xml .= '<data type="'.$info['type'].'" nom="'.$info['nom'].'" valeur="'.$info['valeur'].'" />';
            }
        }
        $xml .= '</dossier_datas>';
        $xml .= '</dossiersub>';

        return $xml;
    }

    /**
     * Permet d'envoyer la requete de mise a jour de la date limite de reponse.
     *
     * @param string $idDossier    : identifiant du dossier
     * @param string $datelimite:  date limite de reponse
     * @param string $heurelimite: heure limite de reponse
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function updateDateLimiteComplement($idDossier, $datelimite, $heurelimite)
    {
        $res = null;
        try {
            $uri = Atexo_Config::getParameter('URL_SUB_REST_UPDATE_DATE_LIMITE_COMPLEMENTS')."?iddossier=$idDossier&datelimite=$datelimite&heurelimite=$heurelimite";
            $client = new Client();
            $options['auth'] = self::getLoginAndPassForSub();
            $res = $client->Get($uri, $options)->getBody();
            $logger = Atexo_LoggerManager::getLogger('publicite');
            $logger->info('Retour du ws URL_SUB_REST_UPDATE_DATE_LIMITE_COMPLEMENTS id dossier : '.$idDossier.' date limite : '.$datelimite.' heure limite : '.$heurelimite.' Retour ws : '.print_r($res, true));
        } catch (Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('publicite');
            $logger->error('Retour du ws URL_SUB_REST_UPDATE_DATE_LIMITE_COMPLEMENTS id dossier : '.$idDossier.' date limite : '.$datelimite.' heure limite : '.$heurelimite.' Retour ws : '.print_r($res, true).' Erreur : '.$e->getMessage());
        }
    }

    /**
     * Permet d'envoyer la requete de mise a jour des infos de contact de l'OF a SUB.
     *
     * @param string        $oldLogin : ancien login de l'inscrit
     * @param CommonInscrit $inscrit  : objet contenant les informations de l'inscrit (cet objet contient les informations recentes)
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function updateUserInfos($oldLogin, CommonInscrit $inscrit)
    {
        $xml = null;
        try {
            $xml = self::getXmlInfosUserForWsUpdateContactInscrit($inscrit);
            $client = new Client();
            $uri = str_replace('{login}', rawurlencode($oldLogin), Atexo_Config::getParameter('URL_SUB_REST_UPDATE_CONTACT_INSCRIT'));
            if ($xml) {
                $options = [
                    'headers' => [
                        'Content-Type' => self::CONTENT_TYPE_XML
                    ],
                    'body' => $xml,
                ];
            }
            $options['auth'] = self::getLoginAndPassForSub();
            $res = $client->Post($uri, $options)->getBody();
            $logger = Atexo_LoggerManager::getLogger('publicite');
            $logger->info("Retour du ws URL_SUB_REST_UPDATE_CONTACT_INSCRIT \nAncien_login = ".$oldLogin." \nxml request = ".$xml." \nNouveau_login = ".$inscrit->getLogin()."  \nRetour ws : ".print_r($res, true).'');
        } catch (Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('publicite');
            $logger->info("Retour du ws URL_SUB_REST_UPDATE_CONTACT_INSCRIT \nAncien_login = ".$oldLogin." \nxml request = ".$xml." \nNouveau_login = ".$inscrit->getLogin()."  \nRetour ws : ".$e->getMessage().'');
        }
    }

    /**
     * Permet de construire l'xml des infos de contact de l'OF pour la mise a jour au niveau de SUB.
     *
     * @param CommonInscrit $inscrit : objet inscrit
     *
     * @return string : xml
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function getXmlInfosUserForWsUpdateContactInscrit(CommonInscrit $inscrit)
    {
        $xml = '<?xml version="1.0" encoding="UTF-8"?>';
        $xml .= '<contact nom="'.Atexo_Util::toUtf8($inscrit->getNom()).'" prenom="'.Atexo_Util::toUtf8($inscrit->getPrenom()).'" email="'.Atexo_Util::toUtf8($inscrit->getEmail()).'" telephone="'.Atexo_Util::toUtf8($inscrit->getTelephone()).'" login="'.Atexo_Util::toUtf8($inscrit->getLogin()).'"/>';

        return $xml;
    }

    private static function getLoginAndPassForSub(): array
    {
        return [
            Atexo_Config::getParameter('LOGIN_APPLI_ACCES_SUB'),
            Atexo_Config::getParameter('PASSWORD_APPLI_ACCES_SUB')
        ];
    }
}
