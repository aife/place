<?php

namespace Application\Service\Atexo\FormulaireSub;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonTDossierFormulaire;
use Application\Propel\Mpe\CommonTDossierFormulairePeer;
use Application\Propel\Mpe\CommonTEditionFormulaire;
use Application\Propel\Mpe\CommonTEditionFormulairePeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_Zip;
use DOMDocument;
use DOMNodeList;
use Exception;

/**
 * Classe de gestion des éditions.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 1.0
 *
 * @since MPE-4.0
 */
class Atexo_FormulaireSub_Edition
{
    /**
     * Permet de recuperer la liste des éditions via WS.
     */
    public function recupererListeEditionsDepuisSub($idDossierFormulaire, $idDossierFormulaireExterne)
    {
        //Appel WS des éditions pour recuperer l'xml des éditions
        $xml = (new Atexo_FormulaireSub_Echange())->recupererXmlEditions($idDossierFormulaireExterne);
        //print_r($xml);exit;
        $listeEditions = self::parseXmlEdition($xml, $idDossierFormulaire);
        self::parseXmlEditionUpdateDossier($xml, $idDossierFormulaire);

        return $listeEditions;
    }

    /**
     * Permet de parser l'xml et charge des objets de la classe CommonTEditionFormulaire.
     */
    public function parseXmlEdition($xml, $idDossierFormulaire)
    {
        try {
            $dom = new DOMDocument();
            $dom->loadXML($xml);
        } catch (\Exception $e) {
            self::writeToLog('Récuperation des éditions: xml non valide', $e->getMessage());

            return false;
        }
        $reponsegets = $dom->getElementsByTagName('reponseget');
        $idDispositif = '';
        $idDossier = '';
        $listeEditions = [];
        if ($reponsegets instanceof DOMNodeList) {
            foreach ($reponsegets as $reponseget) {
                if ('OK' == (string) $reponseget->getAttribute('statut')) {
                    $dossiersFormulaire = $reponseget->getElementsByTagName('dossier');
                    foreach ($dossiersFormulaire as $dossierFormulaire) {
                        $idDispositif = $dossierFormulaire->getAttribute('id_dispositif');
                        $idDossier = $dossierFormulaire->getAttribute('id_dossier');
                        $documents = $dossierFormulaire->getElementsByTagName('document');
                        if ($documents->length) {
                            foreach ($documents as $document) {
                                //Recupération des informations des éditions
                                $libelleDocument = '';
                                $typeDocument = '';
                                $pathDocument = '';
                                $hashDocument = '';
                                $cleExterneEdition = $document->getAttribute('id_document');
                                $libelles = $document->getElementsByTagName('libelle');
                                foreach ($libelles as $libelle) {
                                    $libelleDocument = $libelle->nodeValue;
                                }
                                $types = $document->getElementsByTagName('type');
                                foreach ($types as $type) {
                                    $typeDocument = $type->nodeValue;
                                }
                                $paths = $document->getElementsByTagName('path');
                                foreach ($paths as $path) {
                                    $pathDocument = $path->nodeValue;
                                }
                                $hashs = $document->getElementsByTagName('hash');
                                foreach ($hashs as $hash) {
                                    $hashDocument = $hash->nodeValue;
                                }
                                //Sauvegarde des éditions
                                $ignoreEdition = false;
                                if (Atexo_Config::getParameter('ACTIVER_FILTRE_EDITION_SUB')
                                && 'edition' == $typeDocument
                                && str_starts_with($libelleDocument, Atexo_Config::getParameter('FILTRE_EDITION_SUB'))) {
                                    $ignoreEdition = true;
                                }
                                if (false == $ignoreEdition) {
                                    $edition = false;
                                    $edition = self::retrieveEditionByCleExterneIdDossier((new Atexo_Util())->toHttpEncoding($cleExterneEdition), $idDossierFormulaire);
                                    if (!$edition) {
                                        $edition = new CommonTEditionFormulaire();
                                    }
                                    $edition->setIdDossierFormulaire((new Atexo_Util())->toHttpEncoding($idDossierFormulaire));
                                    $edition->setCleExterneEdition((new Atexo_Util())->toHttpEncoding($cleExterneEdition));
                                    $edition->setLibelle((new Atexo_Util())->toHttpEncoding($libelleDocument));
                                    if ((new Atexo_Util())->toHttpEncoding($pathDocument)) {
                                        $edition->setPath((new Atexo_Util())->toHttpEncoding($pathDocument));
                                    }
                                    if ((new Atexo_Util())->toHttpEncoding($hashDocument)) {
                                        $edition->setHash((new Atexo_Util())->toHttpEncoding($hashDocument));
                                    }
                                    switch ($typeDocument) {
                                        case 'edition':
                                            $edition->setType(Atexo_Config::getParameter('TYPE_DOCUMENT_EDITION'));
                                            break;
                                        case 'pj':
                                            $edition->setType(Atexo_Config::getParameter('TYPE_DOCUMENT_PJ'));
                                            $edition->setNomFichier((new Atexo_Util())->toHttpEncoding($libelleDocument));
                                            break;
                                    }
                                    $listeEditions[] = $edition;
                                }
                            }
                        }
                    }
                } else {
                    self::writeToLog('Récuperation des éditions: Statut Non OK');

                    return false;
                }
            }
        } else {
            self::writeToLog('Récuperation des éditions: Liste des documents vide');

            return false;
        }

        return $listeEditions;
    }

    public function writeToLog($messageErreur, $messageException = '')
    {
        $log = "\n\t".date('Y-m-d H:i:s').$messageErreur.' '.$messageException;
        $filePath = Atexo_Config::getParameter('LOG_FILE_PATH_GESTION_FORMULAIRE_SUB').'gestionFormulaireSub.log';
        Atexo_Util::writeLogFile(Atexo_Config::getParameter('LOG_FILE_PATH_GESTION_FORMULAIRE_SUB'), $filePath, $log, 'a');
    }

    /**
     * Permet de recuperer la liste des éditions d'un dossier formulaire.
     */
    public function recupererEditionsByIdDossier($idDossier)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonTEditionFormulairePeer::ID_DOSSIER_FORMULAIRE, $idDossier, Criteria::EQUAL);
        $listeEditions = CommonTEditionFormulairePeer::doSelect($c, $connexion);

        return $listeEditions;
    }

    /**
     * Permet de recuperer la liste des éditions en attente de génération (statut_generation : STATUT_EDITION_A_GENERER) dans l'ordre des validations.
     */
    public function recupererEditionsAGenerer($limit = 5)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonTEditionFormulairePeer::STATUT_GENERATION, Atexo_Config::getParameter('STATUT_EDITION_A_GENERER'), Criteria::EQUAL);
        $c->addJoin(CommonTEditionFormulairePeer::ID_DOSSIER_FORMULAIRE, CommonTDossierFormulairePeer::ID_DOSSIER_FORMULAIRE);
        $c->addAscendingOrderByColumn(CommonTDossierFormulairePeer::DATE_VALIDATION);
        if ($limit > 0) {
            $c->setLimit($limit);
        }
        $listeEditions = CommonTEditionFormulairePeer::doSelect($c, $connexion);

        return $listeEditions;
    }

    /**
     * Permet de supprimer une édition.
     */
    public function deleteEditionByIdDossier($idDossier, $connexion = null)
    {
        if (!$connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        }
        $c = new Criteria();
        $c->add(CommonTEditionFormulairePeer::ID_DOSSIER_FORMULAIRE, $idDossier, Criteria::EQUAL);
        CommonTEditionFormulairePeer::doDelete($c, $connexion);
    }

    /**
     * Permet de parser l'xml de demande de génération de document.
     */
    public function parseXmlDemandeGenerationDocument($xml)
    {
        try {
            $dom = new DOMDocument();
            $dom->loadXML($xml);
        } catch (\Exception $e) {
            self::writeToLog('--> ERROR : Lors de la récupération xml de la demande de génération des documents', $e->getMessage());

            return false;
        }
        $reponsegets = $dom->getElementsByTagName('reponseget');
        $infosParse = [];
        if ($reponsegets instanceof DOMNodeList) {
            foreach ($reponsegets as $reponseget) {
                if ('OK' == (string) $reponseget->getAttribute('statut')) {
                    $dossiersFormulaire = $reponseget->getElementsByTagName('dossier');
                    foreach ($dossiersFormulaire as $dossierFormulaire) {
                        $documents = $dossierFormulaire->getElementsByTagName('document');
                        if ($documents->length) {
                            foreach ($documents as $document) {
                                //Recupération des informations des éditions
                                $libelleDocument = '';
                                $typeDocument = '';
                                $pathDocument = '';
                                $hashDocument = '';
                                $dateGenaration = '';
                                $libelles = $document->getElementsByTagName('libelle');
                                foreach ($libelles as $libelle) {
                                    $libelleDocument = $libelle->nodeValue;
                                }
                                $types = $document->getElementsByTagName('type');
                                foreach ($types as $type) {
                                    $typeDocument = $type->nodeValue;
                                }
                                $paths = $document->getElementsByTagName('path');
                                foreach ($paths as $path) {
                                    $pathDocument = $path->nodeValue;
                                }
                                $hashs = $document->getElementsByTagName('hash');
                                foreach ($hashs as $hash) {
                                    $hashDocument = $hash->nodeValue;
                                }
                                $dateGenarations = $document->getElementsByTagName('date_generation');
                                foreach ($dateGenarations as $oneDateGeneration) {
                                    $dateGenaration = $oneDateGeneration->nodeValue;
                                }
                                $infosParse['path'] = $pathDocument;
                                $infosParse['libelle'] = $libelleDocument;
                                $infosParse['type'] = $typeDocument;
                                $infosParse['hash'] = $hashDocument;
                                $infosParse['nomFichier'] = $libelleDocument;
                                $infosParse['dateGeneration'] = Atexo_Util::frnDateTime2iso($dateGenaration);
                            }
                        }
                    }
                } else {
                    self::writeToLog('Récupération xml de la demande de génération des documents :Non OK');

                    return false;
                }
            }
        } else {
            self::writeToLog('Récupération xml de la demande de génération des documents: Liste des documents vide');

            return false;
        }
        //print_r($infosParse);exit;
        return $infosParse;
    }

    /**
     * Permet de générer une édition.
     *
     * @param $edition: objet de la classe CommonTEditionFormulaire
     */
    public function genererEdition($edition)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));

        if ($edition instanceof CommonTEditionFormulaire) {
            try {
                self::writeToLog('--> INFO : Récuperation du dossier formulaire: xml non valide', $edition->getIdDossierFormulaire());
                $dossierFormulaire = (new Atexo_FormulaireSub_Dossier())->getDossierFormulaireById($edition->getIdDossierFormulaire());
                if ($dossierFormulaire instanceof CommonTDossierFormulaire) {
                    self::writeToLog('--> INFO : Démarrage appel WS génération des éditions avec paramètres : cle_externe_edition '.$edition->getCleExterneEdition().
                            ' et cle_externe_dossier '.$dossierFormulaire->getCleExterneDossier());

                    //Appel du WS génération des éditions
                    $xml = (new Atexo_FormulaireSub_Echange())->recupererXmlDemandeGenerationDocuments($edition->getCleExterneEdition(), $dossierFormulaire->getCleExterneDossier());

                    self::writeToLog('--> INFO : Fin appel WS génération des éditions avec paramètres : cle_externe_edition '.$edition->getCleExterneEdition().
                        ' et cle_externe_dossier '.$dossierFormulaire->getCleExterneDossier());

                    $infosParse = (new Atexo_FormulaireSub_Edition())->parseXmlDemandeGenerationDocument($xml);
                    if (is_array($infosParse)) {
                        self::writeToLog("--> INFO : Le XML a bien été parsé, l'édition id_dossier_formulaire ".$edition->getIdDossierFormulaire().' va être mise à jour ');
                        //Mise à jour des informations en base (path, hash, date de génération)
                        $edition->setPath((new Atexo_Util())->toHttpEncoding($infosParse['path']));
                        $edition->setHash((new Atexo_Util())->toHttpEncoding($infosParse['hash']));
                        $edition->setNomFichier((new Atexo_Util())->toHttpEncoding($infosParse['nomFichier']));
                        $edition->setDateGeneration(date('Y-m-d H:i:s'));
                        $edition->setStatutGeneration(Atexo_Config::getParameter('STATUT_EDITION_GENERER'));
                        $edition->save($connexion);
                        self::writeToLog("--> INFO : l'édition id_dossier_formulaire ".$edition->getIdDossierFormulaire().' est passée au STATUT_EDITION_GENERER');
                    } else {
                        self::writeToLog("--> ERROR : Le XML n'a pu être parsé, l'édition id_dossier_formulaire ".$edition->getIdDossierFormulaire().' va repasser au statut EDITION A GENERER');
                        $edition->setStatutGeneration(Atexo_Config::getParameter('STATUT_EDITION_A_GENERER'));
                        $edition->save($connexion);
                    }
                }
            } catch (Exception $e) {
                self::writeToLog("--> ERROR : Une exception est survenu lors de la génération de l'édition message= ".$e->getMessage().", l'édition id_dossier_formulaire ".$edition->getIdDossierFormulaire().
                    ' va repasser au statut EDITION A GENERER');
                $edition->setStatutGeneration(Atexo_Config::getParameter('STATUT_EDITION_A_GENERER'));
                $edition->save($connexion);
            }
        }
    }

    public function gestionActionEdition($edition, $dossier)
    {
        if (($edition instanceof CommonTEditionFormulaire) && ($dossier instanceof CommonTDossierFormulaire)) {
            if ($edition->getType() == Atexo_Config::getParameter('TYPE_DOCUMENT_PJ')) {
                $res = 'cas3'; // bonne verion de l Edition qui est sauvegardé 3
            } else {
                if (!$edition->getPath()) {
                    $res = 'cas1'; // aucune edition n est généré 1
                } elseif (strcmp($dossier->getDateModif(), $edition->getDateGeneration()) <= 0) {
                    $res = 'cas3'; // bonne verion de l Edition qui est sauvegardé 3
                } else {
                    $res = 'cas2'; // mauvaise verion de l Edition qui est sauvegardé 2
                }
            }
        } else {
            $res = 'cas1'; // aucune edition n est généré  1
        }

        return $res;
    }

    public function retrieveEditionByCleExterneIdDossier($cleExterneEdition, $idDossierFormulaire)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $c = new Criteria();
        $c->add(CommonTEditionFormulairePeer::CLE_EXTERNE_EDITION, $cleExterneEdition, Criteria::EQUAL);
        $c->add(CommonTEditionFormulairePeer::ID_DOSSIER_FORMULAIRE, $idDossierFormulaire, Criteria::EQUAL);
        $edition = CommonTEditionFormulairePeer::doSelectOne($c, $connexion);
        if ($edition instanceof CommonTEditionFormulaire) {
            return $edition;
        } else {
            return false;
        }
    }

    /**
     * Permet de mettre à jour le dossier après le callBack.
     */
    public function updateDossier($idDossier, $idFormulaire)
    {
        try {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $dossier = CommonTDossierFormulairePeer::retrieveByPK($idDossier, $connexion);
            if ($dossier instanceof CommonTDossierFormulaire) {
                $dossier->setCleExterneFormulaire($idFormulaire);
                $dossier->save($connexion);
            }
        } catch (\Exception $e) {
            echo $e->getMessage();

            return false;
        }
    }

    /**
     * Permet de parser l'xml des éditions pour finalité de mettre à jour le dossier.
     */
    public function parseXmlEditionUpdateDossier($xml, $idDossierFormulaire)
    {
        try {
            $dom = new DOMDocument();
            $dom->loadXML($xml);
        } catch (\Exception $e) {
            self::writeToLog('Récuperation des éditions: xml non valide', $e->getMessage());

            return false;
        }
        $reponsegets = $dom->getElementsByTagName('reponseget');
        $idFormulaire = '';
        if ($reponsegets instanceof DOMNodeList) {
            foreach ($reponsegets as $reponseget) {
                if ('OK' == (string) $reponseget->getAttribute('statut')) {
                    $dossiersFormulaire = $reponseget->getElementsByTagName('dossier');
                    foreach ($dossiersFormulaire as $dossierFormulaire) {
                        $idFormulaire = $dossierFormulaire->getAttribute('id_formulaire');
                        if ($idFormulaire) {
                            self::updateDossier($idDossierFormulaire, $idFormulaire);
                        }
                    }
                } else {
                    self::writeToLog('Récuperation des éditions: Statut Non OK');

                    return false;
                }
            }
        } else {
            self::writeToLog('Récuperation des éditions: Liste des documents vide');

            return false;
        }
    }

    /**
     * Permet de verifier si toutes les éditions sont générées une réponse.
     *
     * @param $editionsGenerees: passés en référence, cette variable sera remplie à true si toutes éditions deja générées, false sinon
     */
    public function verifierSiToutesEditionsGenereesParDossier($idDossier, &$dossierValidees, &$editionsGenerees)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        //Recuperer les dossiers de cette reponse formulaire
        $dossier = CommonTDossierFormulairePeer::retrieveByPK($idDossier, $connexion);

        if ($dossier instanceof CommonTDossierFormulaire) {
            if ($dossier->getStatutValidation() != Atexo_Config::getParameter('STATUT_DOSSIER_VALIDE')) {
                $dossierValidees = false;

                return;
            }
            //Recuperer les éditions de ce dossier
            $editions = (new Atexo_FormulaireSub_Dossier())->recupererEditionsFormulaire($dossier->getIdDossierFormulaire());
            if (is_array($editions) && count($editions)) {
                foreach ($editions as $uneEdition) {
                    //print_r($uneEdition);
                    $cas = self::gestionActionEdition($uneEdition, $dossier);
                    if ('cas3' != $cas) {
                        $editionsGenerees = false;

                        return;
                    }
                }
            }
        } else {
            $dossierValidees = false;

            return;
        }
    }

    /**
     * Permet de télécharger l'ensemble des éditions pour un dossier dans un fichier zip.
     */
    public function genererIntegraliteEditionsParDossier($idDossier)
    {
        $tmpFileDir = Atexo_Config::getParameter('COMMON_TMP').'/dir_ensemble_editions_'.session_id().uniqid();
        $tmpZipFile = $tmpFileDir.'.zip';
        $editions = self::recupererEditionsByIdDossier($idDossier);
        if (is_array($editions) && count($editions)) {
            if (is_file($tmpZipFile)) {
                system('rm -f '.$tmpZipFile, $sys_answer);
            }
            $arrayFile = [];
            foreach ($editions as $edition) {
                $pathFile = $edition->getPath();
                if (is_file($pathFile)) {
                    $fileName = Atexo_Util::verifierNameFile($tmpZipFile, $edition->getNomFichier(), $arrayFile);
                    $fileName = trim(preg_replace('#[ ]{2,}#', ' ', $fileName));
                    (new Atexo_Zip())->addFile2Zip($tmpZipFile, $pathFile, $fileName, 'CREATE');
                } else {
                    $logger = Atexo_LoggerManager::getLogger('app');
                    $logger->error("Le chemin du fichier $pathFile n'est pas valide.");
                }
            }
        }

        return $tmpZipFile;
    }
}
