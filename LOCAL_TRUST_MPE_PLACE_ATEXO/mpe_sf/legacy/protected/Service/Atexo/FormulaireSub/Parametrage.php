<?php

namespace Application\Service\Atexo\FormulaireSub;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonTParamDossierFormulairePeer;
use Application\Service\Atexo\Atexo_Config;

/**
 * Classe de.
 *
 * @author Khalid BENAMAR <khalid.benamar@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_FormulaireSub_Parametrage
{
    /**
     * Recupèration du paramétrage du dossier sur la consultation pour le type d'enveloppe.
     */
    public function retrieveParametrageEnveloppe($consultationId, $organisme, $typeEnveloppe)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));

        $c = new Criteria();
        $c->add(CommonTParamDossierFormulairePeer::CONSULTATION_ID, $consultationId, Criteria::EQUAL);
        $c->add(CommonTParamDossierFormulairePeer::ORGANISME, $organisme, Criteria::EQUAL);
        $c->add(CommonTParamDossierFormulairePeer::TYPE_ENVELOPPE, $typeEnveloppe, Criteria::EQUAL);

        $paramFormObject = CommonTParamDossierFormulairePeer::doSelectOne($c, $connexionCom);

        return $paramFormObject ?: false;
    }
}
