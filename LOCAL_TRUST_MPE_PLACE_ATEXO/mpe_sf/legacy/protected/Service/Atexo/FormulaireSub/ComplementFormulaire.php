<?php

namespace Application\Service\Atexo\FormulaireSub;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonInscritPeer;
use Application\Propel\Mpe\CommonTComplementFormulaire;
use Application\Propel\Mpe\CommonTComplementFormulairePeer;
use Application\Propel\Mpe\CommonTDossierFormulairePeer;
use Application\Propel\Mpe\CommonTReponseElecFormulairePeer;
use Application\Propel\Mpe\EntreprisePeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_LoggerManager;
use PDO;

/**
 * Classe de gestion des compléments formulaires SUB.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 1.0
 *
 * @since MPE-4
 */
class Atexo_FormulaireSub_ComplementFormulaire
{
    /**
     * Permet de recuperer l'objet CommonTComplementFormulaire.
     *
     * @param $idDossier : l'identifiant technique du dossier
     *
     * @return : objet de la classe CommonTComplementFormulaire
     */
    public function getComplementFormulaireByIdDossier($idDossier)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonTComplementFormulairePeer::ID_DOSSIER_FORMULAIRE, $idDossier, Criteria::EQUAL);
        $complementFormulaire = CommonTComplementFormulairePeer::doSelectOne($c, $connexion);
        if ($complementFormulaire instanceof CommonTComplementFormulaire) {
            return $complementFormulaire;
        }

        return false;
    }

    /**
     * Permet de recuperer tous les demandes de complements.
     */
    public function recupererDemandesComplement(Atexo_FormulaireSub_CriteriaVo $criteriaVo, $forNbrResult = false)
    {
        $complementsFormulaire = null;
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->addJoin(CommonTComplementFormulairePeer::ID_DOSSIER_FORMULAIRE, CommonTDossierFormulairePeer::ID_DOSSIER_FORMULAIRE);
        $c->addJoin(CommonTDossierFormulairePeer::ID_REPONSE_ELEC_FORMULAIRE, CommonTReponseElecFormulairePeer::ID_REPONSE_ELEC_FORMULAIRE);
        if ($criteriaVo->getTypeReponseDossier()) {
            $c->add(CommonTDossierFormulairePeer::TYPE_REPONSE, $criteriaVo->getTypeReponseDossier(), Criteria::EQUAL);
        }
        if ($criteriaVo->getIdInscrit()) {
            $c->add(CommonTReponseElecFormulairePeer::ID_INSCRIT, $criteriaVo->getIdInscrit(), Criteria::EQUAL);
        }
        if ($criteriaVo->getOrganisme()) {
            $c->add(CommonTReponseElecFormulairePeer::ORGANISME, $criteriaVo->getOrganisme(), Criteria::EQUAL);
        }
        $c->addJoin(CommonTReponseElecFormulairePeer::ID_INSCRIT, CommonInscritPeer::ID, Criteria::LEFT_JOIN);
        $c->addJoin(CommonInscritPeer::ENTREPRISE_ID, EntreprisePeer::ID, Criteria::LEFT_JOIN);
        if ($criteriaVo->getNomEntreprise()) {//TODO: cela ramene toutes les demandes de compléments lié à cette consultation
            $c->add(EntreprisePeer::NOM, '%'.$criteriaVo->getNomEntreprise().'%', Criteria::LIKE);
        }
        if ($criteriaVo->getTypeDossier()) {
            $c->add(CommonTDossierFormulairePeer::TYPE_ENVELOPPE, $criteriaVo->getTypeDossier(), Criteria::EQUAL);
        }
        if ($criteriaVo->getLot()) {
            $c->add(CommonTDossierFormulairePeer::ID_LOT, $criteriaVo->getLot(), Criteria::EQUAL);
        }
        if ('0' != $criteriaVo->getEtatMessageInformation()) {
            if ('1' == $criteriaVo->getEtatMessageInformation()) {
                $c->add(CommonTComplementFormulairePeer::DATE_ENVOI_1ER_MAIL, null, Criteria::EQUAL);
                $c->addOr(CommonTComplementFormulairePeer::DATE_ENVOI_1ER_MAIL, '', Criteria::EQUAL);
            } elseif ('2' == $criteriaVo->getEtatMessageInformation()) {
                $c->add(CommonTComplementFormulairePeer::DATE_ENVOI_1ER_MAIL, null, Criteria::NOT_EQUAL);
                $c->addOr(CommonTComplementFormulairePeer::DATE_ENVOI_1ER_MAIL, '', Criteria::NOT_EQUAL);
                $c->add(CommonTComplementFormulairePeer::DATE_1ER_AR, null, Criteria::EQUAL);
            } elseif ('3' == $criteriaVo->getEtatMessageInformation()) {
                $c->add(CommonTComplementFormulairePeer::DATE_ENVOI_1ER_MAIL, null, Criteria::NOT_EQUAL);
                $c->add(CommonTComplementFormulairePeer::DATE_1ER_AR, null, Criteria::NOT_EQUAL);
                $c->addOr(CommonTComplementFormulairePeer::DATE_1ER_AR, '', Criteria::NOT_EQUAL);
            }
        }
        if ('0' != $criteriaVo->getEtatReouverture()) {
            if ('1' == $criteriaVo->getEtatReouverture()) {
                $c->add(CommonTComplementFormulairePeer::DATE_REMIS_LE, '', Criteria::EQUAL);
                $c->addOr(CommonTComplementFormulairePeer::DATE_REMIS_LE, null, Criteria::EQUAL);
                $c->add(CommonTComplementFormulairePeer::DATE_A_REMETTRE, date('Y-m-d H:i:s'), Criteria::GREATER_THAN);
            } elseif ('2' == $criteriaVo->getEtatReouverture()) {
                $c->add(CommonTComplementFormulairePeer::DATE_REMIS_LE, '', Criteria::EQUAL);
                $c->addOr(CommonTComplementFormulairePeer::DATE_REMIS_LE, null, Criteria::EQUAL);
                $c->add(CommonTComplementFormulairePeer::DATE_A_REMETTRE, date('Y-m-d H:i:s'), Criteria::LESS_EQUAL);
            } elseif ('3' == $criteriaVo->getEtatReouverture()) {
                $c->add(CommonTComplementFormulairePeer::DATE_REMIS_LE, '', Criteria::NOT_EQUAL);
            }
        }
        if ($criteriaVo->getControlWithDateRemettre()) {
            if ($criteriaVo->getStatutDemande() == Atexo_Config::getParameter('STATUT_DEMANDE_COMPLEMENT_EN_COURS')) {
                $c->add(CommonTComplementFormulairePeer::DATE_A_REMETTRE, $criteriaVo->getDateReference(), Criteria::GREATER_EQUAL);
            } elseif ($criteriaVo->getStatutDemande() == Atexo_Config::getParameter('STATUT_DEMANDE_COMPLEMENT_ENVOYEE')) {
                $c->add(CommonTComplementFormulairePeer::DATE_A_REMETTRE, $criteriaVo->getDateReference(), Criteria::LESS_EQUAL);
            }
        }
        if ($criteriaVo->getRefCons()) {
            $c->add(CommonTReponseElecFormulairePeer::CONSULTATION_ID, $criteriaVo->getRefCons(), Criteria::EQUAL);
        }
        if ($criteriaVo->getStatutDemande()) {
            $c->add(CommonTComplementFormulairePeer::STATUT_DEMANDE, $criteriaVo->getStatutDemande(), Criteria::EQUAL);
        }
        if ($criteriaVo->getMotif()) {
            $c->add(CommonTComplementFormulairePeer::MOTIF, $criteriaVo->getMotif(), Criteria::LIKE);
        }
        if ('' != $criteriaVo->getOrderByColumn()) {
            if ('DESC' == $criteriaVo->getSensTri()) {
                $c->addDescendingOrderByColumn($criteriaVo->getOrderByColumn());
            } elseif ('ASC' == $criteriaVo->getSensTri()) {
                $c->addAscendingOrderByColumn($criteriaVo->getOrderByColumn());
            }
        }
        if ($criteriaVo->getLimit()) {
            $c->setLimit($criteriaVo->getLimit());
        }
        if ($criteriaVo->getOffset()) {
            $c->setOffset($criteriaVo->getOffset());
        }
        $c->setDistinct();
        try {
            $complementsFormulaire = CommonTComplementFormulairePeer::doSelect($c, $connexion);
        } catch (\Exception $e) {
            echo $e->getMessage();
        }
        if (!is_array($complementsFormulaire)) {
            $complementsFormulaire = [];
        }
        if ($forNbrResult && is_array($complementsFormulaire)) {
            return count($complementsFormulaire);
        }

        return $complementsFormulaire;
    }

    /*
     * Permet de recuperer la liste des demandes de compléments avec messages non envoyés
     * Retourne un tableau d'id
     */
    public function getComplementsWithMessagesNonEnvoyes($criteriaVo)
    {
        $typeRelation = Atexo_Config::getParameter('TYPE_RELATION_ECHANGE_COMPLEMENT_FORMULAIRE');
        $sql = ' SELECT id_complement_formulaire ';
        $sql .= ' FROM t_complement_formulaire, t_dossier_formulaire, t_reponse_elec_formulaire ';
        $sql .= " WHERE id_complement_formulaire not in 
            	( 
            	  SELECT RE.id_externe 
            	  FROM Relation_Echange RE, Echange E, EchangeDestinataire ED
            	  WHERE RE.id_echange = E.id AND E.id = ED.id_echange AND RE.type_relation = $typeRelation
            	 ) ";
        $sql .= ' AND t_complement_formulaire.id_dossier_formulaire = t_dossier_formulaire.id_dossier_formulaire ';
        $sql .= ' AND t_reponse_elec_formulaire.id_reponse_elec_formulaire = t_dossier_formulaire.id_reponse_elec_formulaire ';
        $sql .= " AND t_reponse_elec_formulaire.consultation_id = '".$criteriaVo->getRefCons()."'";
        $sql .= " AND t_reponse_elec_formulaire.organisme= '".$criteriaVo->getOrganisme()."'";
        $statement = Atexo_Db::getLinkCommon(true)->query($sql);
        $arrayIds = [];
        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            $arrayIds[] = $row['id_complement_formulaire'];
        }

        return $arrayIds;
    }

    /**
     * Permet d'enregistrer la date d'envoi du premier mail de demande de reouverture a l'OF.
     *
     * @param CommonTComplementFormulaire $complementFormulaire : objet demande de complements
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function enregistrerDatePremierEnvoiMailDemandeComplements($complementFormulaire)
    {
        try {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
            if ($complementFormulaire instanceof CommonTComplementFormulaire) {
                $datePermierEnvoiMail = $complementFormulaire->getDateEnvoi1erMail();
                if (null == $datePermierEnvoiMail || empty($datePermierEnvoiMail)) {
                    $complementFormulaire->setDateEnvoi1erMail(date('Y-m-d H:i:s'));
                    $complementFormulaire->save($connexion);
                }
            }
        } catch (\Exception) {
            $logger = Atexo_LoggerManager::getLogger('app');
            $logger->error("Erreur lors de l'enregistrement de la date d'envoi du premier mail de demande de reouverture a l'OF");
        }
    }

    /**
     * Permet d'enregistrer la date de premier accuse de reception du mail par l'OF.
     *
     * @param CommonTComplementFormulaire $complementFormulaire : objet demande de complements
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function enregistrerDatePremierARDemandeComplements($complementFormulaire)
    {
        try {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
            if ($complementFormulaire instanceof CommonTComplementFormulaire) {
                $datePremierAr = $complementFormulaire->getDate1erAr();
                if (null == $datePremierAr || empty($datePremierAr)) {
                    $complementFormulaire->setDate1erAr(date('Y-m-d H:i:s'));
                    $complementFormulaire->save($connexion);
                }
            }
        } catch (\Exception) {
            $logger = Atexo_LoggerManager::getLogger('app');
            $logger->error("Erreur lors de l'enregistrement de la date d'envoi du premier mail de demande de reouverture a l'OF");
        }
    }

    /**
     * Recupere l'organisme correspondant pour une demande de complement.
     *
     * @param string $idComplement : identifiant technique de la demande de complement
     *
     * @return string : organisme
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getOrganismeByIdComplement($idComplement)
    {
        try {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
            $query = "
    				 SELECT 
    				      TR.organisme
    				 FROM 
    				      `t_reponse_elec_formulaire` TR
    				 	  ,`t_dossier_formulaire` TD
    				 	  ,`t_complement_formulaire` TC
    				 WHERE 
    				      TR.id_reponse_elec_formulaire = TD.id_reponse_elec_formulaire
    				      AND TD.id_dossier_formulaire = TC.id_dossier_formulaire
    				      AND TC.id_complement_formulaire = '$idComplement'
    				 ";
            $statement = Atexo_Db::getLinkCommon(true)->query($query);
            $organisme = null;
            if ($statement) {
                foreach ($statement as $row) {
                    $organisme = $row['organisme'];
                }
            }

            return $organisme;
        } catch (\Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('app');
            $logger->error('Erreur lors de la recuperation de la demande de complements : '.$e->getMessage());
        }
    }
}
