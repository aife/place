<?php

namespace Application\Service\Atexo\FormulaireSub;

use Application\Library\Propel\Collection\PropelObjectCollection;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonTComplementFormulaire;
use Application\Propel\Mpe\CommonTComplementFormulairePeer;
use Application\Propel\Mpe\CommonTComplementFormulaireQuery;
use Application\Propel\Mpe\CommonTDossierFormulaire;
use Application\Propel\Mpe\CommonTDossierFormulairePeer;
use Application\Propel\Mpe\CommonTEditionFormulairePeer;
use Application\Propel\Mpe\CommonTEnveloppeDossierFormulaire;
use Application\Propel\Mpe\CommonTEnveloppeDossierFormulairePeer;
use Application\Propel\Mpe\CommonTParamDossierFormulairePeer;
use Application\Propel\Mpe\CommonTReponseElecFormulaire;
use Application\Propel\Mpe\CommonTReponseElecFormulairePeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Lots;
use Application\Service\Atexo\Message\Atexo_Message_RelationEchange;
use DOMDocument;
use DOMNodeList;
use PDO;

/**
 * Classe de.
 *
 * @author Khalid BENAMAR <khalid.benamar@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 1.0
 *
 * @since MPE-4.0
 */
class Atexo_FormulaireSub_Dossier
{
    /**
     * Permet de recuperer un enregistrement de la table CommonTParamDossierFormulaire.
     */
    public function retrieveParametrageEnveloppe($consultationId, $organisme, $typeEnveloppe)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));

        $c = new Criteria();
        $c->add(CommonTParamDossierFormulairePeer::CONSULTATION_ID, $consultationId, Criteria::EQUAL);
        $c->add(CommonTParamDossierFormulairePeer::ORGANISME, $organisme, Criteria::EQUAL);
        $c->add(CommonTParamDossierFormulairePeer::TYPE_ENVELOPPE, $typeEnveloppe, Criteria::EQUAL);

        $paramFormObject = CommonTParamDossierFormulairePeer::doSelectOne($c, $connexionCom);

        return $paramFormObject ?: false;
    }

    /**
     * Permet de supprimer un dossier.
     *
     * @param $idDossier: identifiant de la table CommonTDossierFormulaire
     */
    public function supprimerDossier($idDossier)
    {
        //Supprimer les éditions
        (new Atexo_FormulaireSub_Edition())->deleteEditionByIdDossier($idDossier);
        //Suppression du dossier
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonTDossierFormulairePeer::ID_DOSSIER_FORMULAIRE, $idDossier, Criteria::EQUAL);
        CommonTDossierFormulairePeer::doDelete($c, $connexion);
    }

    /**
     * Permet de charger la dossiers.
     *
     * @param $consultationId: Référence de la consultation
     * @param $organisme: organisme
     * @param $idInscrit: identifiant de l'inscrit
     * @param $typeEnv: type de l'enveloppe
     */
    public function getChargerListeFormulairesLots($consultationId, $organisme, $idInscrit, $typeEnv)
    {
        // reccuperer t_reponse_elec_formulaire
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $reponseElectFormulaire = self::recupererReponseElectFormulaire($consultationId, $organisme, $idInscrit);
        $resultat = [];
        if ($reponseElectFormulaire instanceof CommonTReponseElecFormulaire) {
            $sql = ' SELECT distinct(id_lot) as lot ';
            $sql .= ' FROM t_dossier_formulaire ';
            $sql .= " WHERE type_enveloppe = '".$typeEnv."'";
            $sql .= " AND id_reponse_elec_formulaire = '".$reponseElectFormulaire->getIdReponseElecFormulaire()."'";
            $sql .= 'ORDER BY lot';

            $statement = Atexo_Db::getLinkCommon(true)->query($sql);
            while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                $c = new Criteria();
                $c->add(CommonTDossierFormulairePeer::ID_LOT, $row['lot'], Criteria::EQUAL);
                $c->add(CommonTDossierFormulairePeer::ID_REPONSE_ELEC_FORMULAIRE, $reponseElectFormulaire->getIdReponseElecFormulaire(), Criteria::EQUAL);
                $c->add(CommonTDossierFormulairePeer::TYPE_ENVELOPPE, $typeEnv, Criteria::EQUAL);
                $c->add(CommonTDossierFormulairePeer::TYPE_REPONSE, Atexo_Config::getParameter('TYPE_DOSSIER_REPONSE_PRINCIPALE'), Criteria::EQUAL);
                $c->add(CommonTDossierFormulairePeer::FORMULAIRE_DEPOSE, Atexo_Config::getParameter('FORMULAIRE_DEPOSE_DOSSIER_PREPARATION'), Criteria::EQUAL);
                $listeDossiers = CommonTDossierFormulairePeer::doSelect($c, $connexion);
                if (is_array($listeDossiers) && count($listeDossiers)) {
                    //Recuperation des lots
                    $lot = (new Atexo_Consultation_Lots())->retrieveCategorieLot($consultationId, $row['lot'], $organisme);
                    if ($lot) {
                        $intituleLot = $lot->getDescriptionTraduite();
                    }
                    $resultat[$row['lot']] = ['lot' => $row['lot'], 'intitule' => $intituleLot, 'dossiers' => $listeDossiers];
                }
            }
        }//print_r($resultat);exit;

        return $resultat;
    }

    /**
     * Permet de recuperer un objet de la classe CommonTReponseElecFormulaire.
     */
    public function recupererReponseElectFormulaire($consultationId, $organisme, $idInscrit, $idEntreprise = null)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonTReponseElecFormulairePeer::CONSULTATION_ID, $consultationId, Criteria::EQUAL);
        $c->add(CommonTReponseElecFormulairePeer::ORGANISME, $organisme, Criteria::EQUAL);
        $c->add(CommonTReponseElecFormulairePeer::ID_INSCRIT, $idInscrit, Criteria::EQUAL);
        if ($idEntreprise) {
            $c->add(CommonTReponseElecFormulairePeer::ID_ENTREPRISE, $idEntreprise, Criteria::EQUAL);
        }
        $reponseElectFormulaire = CommonTReponseElecFormulairePeer::doSelectOne($c, $connexion);
        if ($reponseElectFormulaire instanceof CommonTReponseElecFormulaire) {
            return $reponseElectFormulaire;
        }

        return false;
    }

    /**
     * @todo La fonction n'est utilisé nulle part
     */
    public function recupererDossierParentByReponseElect($consultationId, $organisme, $idInscrit, $idEntreprise = null)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonTReponseElecFormulairePeer::CONSULTATION_ID, $consultationId, Criteria::EQUAL);
        $c->add(CommonTReponseElecFormulairePeer::ORGANISME, $organisme, Criteria::EQUAL);
        $c->add(CommonTReponseElecFormulairePeer::ID_INSCRIT, $idInscrit, Criteria::EQUAL);
        if ($idEntreprise) {
            $c->add(CommonTReponseElecFormulairePeer::ID_ENTREPRISE, $idEntreprise, Criteria::EQUAL);
        }
        $c->addJoin(CommonTReponseElecFormulairePeer::ID_REPONSE_ELEC_FORMULAIRE, CommonTDossierFormulairePeer::ID_REPONSE_ELEC_FORMULAIRE);
        $c->add(CommonTDossierFormulairePeer::TYPE_ENVELOPPE, Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE'), Criteria::EQUAL);
        $dossierFormulaire = CommonTDossierFormulairePeer::doSelectOne($c, $connexion);
        if ($dossierFormulaire instanceof CommonTDossierFormulaire) {
            return $dossierFormulaire;
        }

        return false;
    }

    /**
     *  @todo : la fonction ne semble pas être utilisé
     */
    public function insererReponseFormulaire($consultationId, $organisme, $idInscrit, $idEntreprise)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonTReponseElecFormulairePeer::CONSULTATION_ID, $consultationId, Criteria::EQUAL);
        $c->add(CommonTReponseElecFormulairePeer::ORGANISME, $organisme, Criteria::EQUAL);
        $c->add(CommonTReponseElecFormulairePeer::ID_INSCRIT, $idInscrit, Criteria::EQUAL);
        $c->add(CommonTReponseElecFormulairePeer::ID_ENTREPRISE, $idEntreprise, Criteria::EQUAL);
        $idReponseFormulaire = CommonTReponseElecFormulairePeer::doInsert($c, $connexion);

        return $idReponseFormulaire;
    }

    /**
     * Permet de recuperer des enregistrements de la classe CommonTEditionFormulaire.
     */
    public function recupererEditionsFormulaire($idDossierFormulaire)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonTEditionFormulairePeer::ID_DOSSIER_FORMULAIRE, $idDossierFormulaire, Criteria::EQUAL);
        $c->addDescendingOrderByColumn(CommonTEditionFormulairePeer::TYPE);
        $editionsFormulaires = CommonTEditionFormulairePeer::doSelect($c, $connexion); //print_r($connexion);exit;
        if (is_array($editionsFormulaires) && count($editionsFormulaires)) {
            return $editionsFormulaires;
        }

        return [];
    }

    /**
     * Permet de recuperer un dossier formulaire par id_dossier_formulaire.
     */
    public function getDossierFormulaireById($idDossier)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonTDossierFormulairePeer::ID_DOSSIER_FORMULAIRE, $idDossier, Criteria::EQUAL);
        $dossierFormulaire = CommonTDossierFormulairePeer::doSelectOne($c, $connexion);
        if ($dossierFormulaire instanceof CommonTDossierFormulaire) {
            return $dossierFormulaire;
        }

        return false;
    }

    /**
     * Permet de recuperer un dossier données par ses parametres.
     *
     * @param $idReponseElectFormulaire: identifiant technique de la table TReponseElecFormulaire
     * @param $typeEnv: le type de l'enveloppe (candidature, offres techniques, offres, anonymat)
     * @param $numLot: le numéro du lot
     */
    public function getDossierByIdReponseElectFormulaireAndTypeEnveloppeAndNumLot($idReponseElectFormulaire, $typeEnv, $numLot)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonTDossierFormulairePeer::ID_LOT, $numLot, Criteria::EQUAL);
        $c->add(CommonTDossierFormulairePeer::ID_REPONSE_ELEC_FORMULAIRE, $idReponseElectFormulaire, Criteria::EQUAL);
        $c->add(CommonTDossierFormulairePeer::TYPE_ENVELOPPE, $typeEnv, Criteria::EQUAL);
        $c->add(CommonTDossierFormulairePeer::TYPE_REPONSE, Atexo_Config::getParameter('TYPE_DOSSIER_REPONSE_PRINCIPALE'), Criteria::EQUAL);
        $c->add(CommonTDossierFormulairePeer::FORMULAIRE_DEPOSE, Atexo_Config::getParameter('FORMULAIRE_DEPOSE_DOSSIER_PREPARATION'), Criteria::EQUAL);
        $dossier = CommonTDossierFormulairePeer::doSelectOne($c, $connexion);
        if ($dossier instanceof CommonTDossierFormulaire) {
            return $dossier;
        }

        return false;
    }

    /**
     * Permet de recuperer les demandes de complements qui ne sont pas encore envoyés par l'entreprise.
     */
    public function recupererDemandesComplementsOF($consultationId, $org, $idInscrit = null)
    {
        $complementsFormulairesQuery = new CommonTComplementFormulaireQuery(Atexo_Config::getParameter('COMMON_DB'));
        $complementsFormulaires = $complementsFormulairesQuery->recupererDemandesComplementsOF($consultationId, $idInscrit);
        $listeDemandeComplementsAvecMailsEnvoyes = [];
        if ($complementsFormulaires instanceof PropelObjectCollection) {
            //Ne recuperer que les demandes de compléments dont les mails ont été envoyés par l'agent
            if (is_array($complementsFormulaires->getData()) && count($complementsFormulaires->getData())) {
                foreach ($complementsFormulaires->getData() as $complement) {
                    if ($complement instanceof CommonTComplementFormulaire) {
                        $idExterne = $complement->getIdComplementFormulaire();
                        $typeRelation = Atexo_Config::getParameter('TYPE_RELATION_ECHANGE_COMPLEMENT_FORMULAIRE');
                        $relationsEchange = (new Atexo_Message_RelationEchange())->getRelationEchangeByIdExterneAndTypeRelation($idExterne, $typeRelation, $org);
                        if (is_array($relationsEchange) && count($relationsEchange)) {
                            $listeDemandeComplementsAvecMailsEnvoyes[] = $complement;
                        }
                    }
                }
            }
        }

        return $listeDemandeComplementsAvecMailsEnvoyes;
    }

    /**
     * Permet de recuperer un objet de la classe CommonTReponseElecFormulaire.
     */
    public function getDossierFormulaireByIdEnveloppeAndOrg($idEnveloppe, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonTEnveloppeDossierFormulairePeer::ID_ENVELOPPE, $idEnveloppe, Criteria::EQUAL);
        $c->add(CommonTEnveloppeDossierFormulairePeer::ORGANISME, $organisme, Criteria::EQUAL);
        $dossier = CommonTEnveloppeDossierFormulairePeer::doSelectOne($c, $connexion);
        if ($dossier instanceof CommonTEnveloppeDossierFormulaire) {
            return $dossier;
        }

        return false;
    }

    /**
     * Permet de recuperer un objet de la classe CommonTReponseElecFormulaire par id_dossier.
     */
    public function getDossierFormulaireByIdDossierAndOrg($idDossier, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonTEnveloppeDossierFormulairePeer::ID_DOSSIER_FORMULAIRE, $idDossier, Criteria::EQUAL);
        $c->add(CommonTEnveloppeDossierFormulairePeer::ORGANISME, $organisme, Criteria::EQUAL);
        $dossier = CommonTEnveloppeDossierFormulairePeer::doSelectOne($c, $connexion);
        if ($dossier instanceof CommonTEnveloppeDossierFormulaire) {
            return $dossier;
        }

        return false;
    }

    /**
     * Permet de parser l'xml et retourner un tableau contenant les dossiers et demandes de complements.
     *
     * @param string $xml: xml
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function parseXmlDemandeComplementsReturnDossiers($xml)
    {
        try {
            $dom = new DOMDocument();
            $dom->loadXML($xml);
        } catch (\Exception $e) {
            self::writeToLog('Récuperation des dossiers en demandes de complément: xml non valide', $e->getMessage());

            return false;
        }
        $reponsegets = $dom->getElementsByTagName('reponseget');
        $tableauDemandesComplement = [];
        if ($reponsegets instanceof DOMNodeList) {
            foreach ($reponsegets as $reponseget) {
                if ('OK' == (string) $reponseget->getAttribute('statut')) {
                    $dossiersFormulaires = $reponseget->getElementsByTagName('dossiers');
                    if ($dossiersFormulaires->length) {
                        foreach ($dossiersFormulaires as $dossiersFormulaire) {
                            $dossierFormulaires = $dossiersFormulaire->getElementsByTagName('dossier');
                            foreach ($dossierFormulaires as $dossierFormulaire) {
                                $idDossier = $dossierFormulaire->getAttribute('id_dossier');
                                $idDispositif = $dossierFormulaire->getAttribute('id_dispositif');
                                $idFormulaire = $dossierFormulaire->getAttribute('id_formulaire');
                                $numDemandeComplement = $dossierFormulaire->getAttribute('num_demande_complement');
                                //echo $idDossier.' - '.$idDispositif.' - '.$idFormulaire.' - '.$numDemandeComplement;exit;
                                $motifDemandeComplement = '';
                                $commentaireDemandeComplement = '';
                                $dateARemettreDemandeComplement = '';
                                $numeroDossierSub = '';
                                $motifs = $dossierFormulaire->getElementsByTagName('motif');
                                foreach ($motifs as $motif) {
                                    $motifDemandeComplement = $motif->nodeValue;
                                }
                                $commentaires = $dossierFormulaire->getElementsByTagName('commentaire');
                                foreach ($commentaires as $commentaire) {
                                    $commentaireDemandeComplement = $commentaire->nodeValue;
                                }
                                $dateARemettres = $dossierFormulaire->getElementsByTagName('date_a_remettre');
                                foreach ($dateARemettres as $dateARemettre) {
                                    $dateARemettreDemandeComplement = $dateARemettre->nodeValue;
                                }
                                $numsActions = $dossierFormulaire->getElementsByTagName('numero_action');
                                foreach ($numsActions as $numsAction) {
                                    $numeroDossierSub = $numsAction->nodeValue;
                                }

                                //Remplissage objet dossier
                                $dossier = new CommonTDossierFormulaire();
                                $dossier->setCleExterneDossier((new Atexo_Util())->toHttpEncoding($idDossier));
                                $dossier->setCleExterneDispositif((new Atexo_Util())->toHttpEncoding($idDispositif));
                                $dossier->setCleExterneFormulaire((new Atexo_Util())->toHttpEncoding($idFormulaire));
                                $dossier->setReferenceDossierSub((new Atexo_Util())->toHttpEncoding($numeroDossierSub));
                                //Remplissage objet demande complement
                                $complementFormulaire = new CommonTComplementFormulaire();
                                $complementFormulaire->setNumeroComplement((new Atexo_Util())->toHttpEncoding($numDemandeComplement));
                                $complementFormulaire->setDateARemettre((new Atexo_Util())->toHttpEncoding($dateARemettreDemandeComplement));
                                $complementFormulaire->setMotif((new Atexo_Util())->toHttpEncoding($motifDemandeComplement));
                                $complementFormulaire->setCommentaire((new Atexo_Util())->toHttpEncoding($commentaireDemandeComplement));
                                //Remplissage liste contenant les objets dossier et demande de complement
                                $tableauDemandesComplement[$idDossier.'_'.$numDemandeComplement]['dossier'] = $dossier;
                                $tableauDemandesComplement[$idDossier.'_'.$numDemandeComplement]['complementFormulaire'] = $complementFormulaire;
                            }
                        }
                    }
                } else {
                    self::writeToLog('Récuperation des Dossiers en demandes de complement: Statut Non OK');

                    return false;
                }
            }
        } else {
            self::writeToLog('Récuperation des Dossiers en demandes de complement: Liste des documents vide');

            return false;
        }

        return $tableauDemandesComplement;
    }

    /**
     * Permet de recuperer un dossier par sa clé externe SUB et par type de reponse.
     *
     * @param $cleExterneDossier: clé externe dossier SUB
     * @param $typeReponse: type de reponse initial ou en demande de complement
     * @param $numeroComplement: numéro de la demande de complément, ce paramètre est optionnel
     */
    public function getDossierByCleExterneAndTypeReponse($cleExterneDossier, $typeReponse, $numeroComplement = null)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonTDossierFormulairePeer::CLE_EXTERNE_DOSSIER, $cleExterneDossier, Criteria::EQUAL);
        $c->add(CommonTDossierFormulairePeer::TYPE_REPONSE, $typeReponse, Criteria::EQUAL);
        if ($numeroComplement) {
            $c->addJoin(CommonTComplementFormulairePeer::ID_DOSSIER_FORMULAIRE, CommonTDossierFormulairePeer::ID_DOSSIER_FORMULAIRE);
            $c->add(CommonTComplementFormulairePeer::NUMERO_COMPLEMENT, $numeroComplement, Criteria::EQUAL);
        }
        $dossier = CommonTDossierFormulairePeer::doSelectOne($c, $connexion);
        if ($dossier instanceof CommonTDossierFormulaire) {
            return $dossier;
        }

        return false;
    }

    /**
     * Permet de recuperer les dossiers en fonctions des parametres spécifiés.
     *
     * @param string $idReponseElectFormulaire: identifiant technique de la table TReponseElecFormulaire
     * @param string $typeReponse:              type de reponse (principale ou demande de complements)
     * @param string $typeEnv:                  type d'enveloppe
     * @param bool   $formulaireDepose:         precise si le formulaire est depose
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getDossierByIdReponseElectFormulaire($idReponseElectFormulaire, $typeReponse = null, $typeEnv = '', $formulaireDepose = '')
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonTDossierFormulairePeer::ID_REPONSE_ELEC_FORMULAIRE, $idReponseElectFormulaire, Criteria::EQUAL);
        if ($typeReponse) {
            $c->add(CommonTDossierFormulairePeer::TYPE_REPONSE, $typeReponse, Criteria::EQUAL);
        }
        if ('' !== $typeEnv) {
            $c->add(CommonTDossierFormulairePeer::TYPE_ENVELOPPE, $typeEnv, Criteria::EQUAL);
        }
        if ('' !== $formulaireDepose) {
            $c->add(CommonTDossierFormulairePeer::FORMULAIRE_DEPOSE, $formulaireDepose, Criteria::EQUAL);
        }
        $dossier = CommonTDossierFormulairePeer::doSelect($c, $connexion);

        return $dossier;
    }

    /**
     * Permet de recuperer un dossier formulaire par identifiant de la demande de complément.
     */
    public function getDossierFormulaireByIdComplement($idComplement)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonTComplementFormulairePeer::ID_COMPLEMENT_FORMULAIRE, $idComplement, Criteria::EQUAL);
        $c->addJoin(CommonTComplementFormulairePeer::ID_DOSSIER_FORMULAIRE, CommonTDossierFormulairePeer::ID_DOSSIER_FORMULAIRE);
        $dossierFormulaire = CommonTDossierFormulairePeer::doSelectOne($c, $connexion);
        if ($dossierFormulaire instanceof CommonTDossierFormulaire) {
            return $dossierFormulaire;
        }

        return false;
    }

    public function writeToLog($messageErreur, $messageException = '')
    {
        $log = "\n\n\n\t_______________________".date('Y-m-d H:i:s')."_______________________\n";
        $log .= "\n\n ".$messageErreur;
        $log .= "\n\n\n\t___________________________________________________________\n";
        $log .= $messageException;
        $filePath = Atexo_Config::getParameter('LOG_FILE_PATH_GESTION_FORMULAIRE_SUB').'gestionFormulaireSub.log';
        Atexo_Util::writeLogFile(Atexo_Config::getParameter('LOG_FILE_PATH_GESTION_FORMULAIRE_SUB'), $filePath, $log, 'a');
    }
}
