<?php

namespace Application\Service\Atexo\FormulaireSub;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonAcheteurPublic;
use Application\Propel\Mpe\CommonCategorieLot;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationPeer;
use Application\Propel\Mpe\CommonDestinatairePub;
use Application\Propel\Mpe\CommonGeolocalisationN2;
use Application\Propel\Mpe\CommonOrganisme;
use Application\Propel\Mpe\CommonRenseignementsBoamp;
use Application\Propel\Mpe\CommonTAnnonceConsultation;
use Application\Propel\Mpe\CommonTConsultationComptePub;
use Application\Propel\Mpe\CommonTCritereAttribution;
use Application\Propel\Mpe\CommonTDispositifAnnonce;
use Application\Propel\Mpe\CommonTDonneeComplementaire;
use Application\Propel\Mpe\CommonTOffreSupportPublicite;
use Application\Propel\Mpe\CommonTProfilJoue;
use Application\Propel\Mpe\CommonTReferentielMotsClesQuery;
use Application\Propel\Mpe\CommonTSousCritereAttribution;
use Application\Propel\Mpe\CommonTSupportAnnonceConsultation;
use Application\Propel\Mpe\CommonTSupportPublication;
use Application\Propel\Mpe\CommonTypeProcedure;
use Application\Propel\Mpe\CommonValeurReferentiel;
use Application\Propel\Mpe\CommonValeurReferentielOrg;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_CommonReferentiel;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_DonnesComplementaires;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_MultiDomaine;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Referentiel;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2;
use Application\Service\Atexo\Publicite\Atexo_Publicite_AnnonceSub;
use Application\Service\Atexo\Publicite\Atexo_Publicite_AvisPub;
use Application\Service\Atexo\Publicite\Atexo_Publicite_RenseignementsBoamp;
use Application\Service\Atexo\Referentiel\Atexo_Referentiel_Referentiel;
use DomDocument;
use Exception;
use Prado\Prado;

use App\Traits\ConfigProxyTrait;
use GuzzleHttp\Client;

/**
 * permet de gérer l'echange entre MPE et SUB.
 *
 * @author AME <amal.elbekkaoui@atexo.com>
 *
 * @version 1.0
 *
 * @since roadmap-2015
 *
 * @copyright Atexo 2015
 */
class Atexo_FormulaireSub_AnnonceEchange
{
    use ConfigProxyTrait;

    /**
     * permet d'avoir le jeton d'authentification a SUB_ANNONCE.
     *
     * @param Atexo_LoggerManager $logger log de trace
     *
     * @return string Jeton d'authentification retourner par SUB
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2015
     */
    public static function authentification($logger = null)
    {
        if (!$logger) {
            $logger = Atexo_LoggerManager::getLogger('publicite');
        }
        $logger->info('Début appelle WS authyentification URI : '.Atexo_Config::getParameter('URL_SUB_ANNONCE_REST_AUTHENTIFICATION'));
        $xmlResponse = self::sendFluxASUB(
            Atexo_Config::getParameter('LOGIN_APPLI_ACCES_SUB_ANNONCE'),
            Atexo_Config::getParameter('PASSWORD_APPLI_ACCES_SUB_ANNONCE'),
            Atexo_Config::getParameter('URL_SUB_ANNONCE_REST_AUTHENTIFICATION'),
            $logger,
            'GET'
        );
        $logger->info('traitement xml response: '.print_r($xmlResponse, true));
        $jeton = trim(self::traiterReponseSub($xmlResponse, $logger, 'authorization', 'jeton', true));
        $logger->info('Fin appelle WS authyentification, valeur retourne par la fct  Atexo_FormulaireSub_AnnonceEchange::authentification '.print_r($jeton, true));

        return $jeton;
    }

    /**
     * permet de créer un dossier sur SUB_ANNONCE.
     *
     * @param string              $xml    xml d'initialisation du dossier annonce
     * @param Atexo_LoggerManager $logger log de trace
     *
     * @return interger identifiant du dossier créé sur SUB_ANNONCE
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2015
     */
    public static function creerDossier($xml, $logger = null)
    {
        if (!$logger) {
            $logger = Atexo_LoggerManager::getLogger('publicite');
        }
        $logger->info('Début appelle WS création du dossier URI : '.Atexo_Config::getParameter('URL_SUB_ANNONCE_REST_CREATION_DOSSIER_ANNONCE'));
        $logger->info('xml request: '.$xml);
        $xmlResponse = self::sendFluxASUB(
            Atexo_Config::getParameter('LOGIN_APPLI_ACCES_SUB_ANNONCE'),
            Atexo_Config::getParameter('PASSWORD_APPLI_ACCES_SUB_ANNONCE'),
            Atexo_Config::getParameter('URL_SUB_ANNONCE_REST_CREATION_DOSSIER_ANNONCE'),
            $logger,
            'POST',
            $xml
        );
        $logger->info('traitement xml response: '.print_r($xmlResponse, true));
        $idDossierSub = self::traiterReponseSub($xmlResponse, $logger, 'dossiersub', 'idDossier');
        $logger->info('Fin appelle WS création du dossier, valeur retourne par la fct  Atexo_FormulaireSub_AnnonceEchange::creerDossier '.print_r($idDossierSub, true));

        return $idDossierSub;
    }

    /**
     * Permet de dupliquer un dossier sur SUB_ANNONCE.
     *
     * @param CommonTAnnonceConsultation $avisInitial : avis de publicite initial
     * @param Atexo_LoggerManager        $logger      : logger
     * @param string                     $typeAvis    : type d'avis (valeurs possibles = AVIS_RECTIFICATIF, AVIS_ANNULATION)
     *
     * @return interger : identifiant du nouveau dossier resultant de la duplication
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public static function dupliquerDossier($avisInitial, $typeAvis, $logger = null)
    {
        try {
            if (!$logger) {
                $logger = Atexo_LoggerManager::getLogger('publicite');
            }
            $uri = Atexo_Config::getParameter('URL_SUB_ANNONCE_REST_DUPLICATION_DOSSIER_ANNONCE');
            $logger->info('Début appel WS de duplication du dossier URI : '.$uri);
            $xml = self::getXmlRequestDuplicationDossier($avisInitial, $typeAvis);
            $logger->info('xml request: '.$xml);
            $xmlResponse = self::sendFluxASUB(Atexo_Config::getParameter('LOGIN_APPLI_ACCES_SUB_ANNONCE'), Atexo_Config::getParameter('PASSWORD_APPLI_ACCES_SUB_ANNONCE'), $uri, $logger, 'POST', $xml);
            $logger->info('traitement xml response: '.print_r($xmlResponse, true));
            $idDossier = self::traiterReponseSub($xmlResponse, $logger, 'dossiersub', 'idDossier');
            $logger->info('Fin appel WS de duplication du dossier, valeur retourne par la fct  Atexo_FormulaireSub_AnnonceEchange::dupliquerDossier '.print_r($idDossier, true));

            return $idDossier;
        } catch (Exception $e) {
            $logger->error("Erreur appel WS de duplication de dossier : \n\nAvis initial = ".print_r($avisInitial, true)." \n\nErreur = ".$e->getMessage()." \n\nTrace = ".$e->getTraceAsString()."\n\n Methode = Atexo_FormulaireSub_AnnonceEchange::dupliquerDossier");
        }
    }

    /**
     * Permet de construire l'xml de la requete de duplication de dossier.
     *
     * @param CommonTAnnonceConsultation $avisInitial : objet annonce de publicite
     * @param string                     $typeAvis    : type d'avis (valeurs possibles = AVIS_RECTIFICATIF, AVIS_ANNULATION)
     *
     * @return string : xml request
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public static function getXmlRequestDuplicationDossier($avisInitial, $typeAvis)
    {
        $logger = Atexo_LoggerManager::getLogger('publicite');
        try {
            $logger->info('Construction xml request pour la duplication du dossier');
            if ($avisInitial instanceof CommonTAnnonceConsultation) {
                $dispositif = $avisInitial->getDispositif();
                if ($dispositif instanceof CommonTDispositifAnnonce) {
                    //Dispositif cible
                    $dispositifCible = null;
                    if ('AVIS_RECTIFICATIF' == $typeAvis) {
                        $dispositifCible = $dispositif->getDispositifRectificatif();
                    } elseif ('AVIS_ANNULATION' == $typeAvis) {
                        $dispositifCible = $dispositif->getDispositifAnnulation();
                    }

                    if ($dispositifCible instanceof CommonTDispositifAnnonce) {
                        $xml = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>';
                        $xml .= '<dossierduplication loginTiers="'.Atexo_Config::getParameter('IDENTIFIANT_ACCES_SUB_ANNONCE').'" sigleDispositifCible="'.$dispositifCible->getSigle().'" idDossierSource="'.$avisInitial->getIdDossierSub().'"></dossierduplication>';
                        $logger->info("Xml request = \n".print_r($xml, true));

                        return $xml;
                    }
                } else {
                    $logger->info('Dispositif non trouvé : ID dispositif = '.$avisInitial->getIdDispositif()." \n\nAvis initial = ".print_r($avisInitial, true));
                }
            } else {
                $logger->info("Avis initial non trouve (ou n'est pas une instance de CommonTAnnonceConsultation) : ".print_r($avisInitial, true));
            }
        } catch (Exception $e) {
            $logger->error("Erreur Construction XML REQUEST de duplication de dossier : \n\nAvis de publicite initial = ".print_r($avisInitial, true)." \n\nErreur = ".$e->getMessage()." \n\nTrace = ".$e->getTraceAsString()."\n\n Methode = Atexo_FormulaireSub_AnnonceEchange::getXmlRequestDuplicationDossier");
        }
    }

    /**
     * permet de mettre à jour un dossier sur SUB_ANNONCE.
     *
     * @param string              $xml    xml d'initialisation du dossier annonce
     * @param Atexo_LoggerManager $logger log de trace
     *
     * @return interger identifiant du dossier créé sur SUB_ANNONCE
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2015
     */
    public static function updateAnnonce($xml, $logger = null)
    {
        if (!$logger) {
            $logger = Atexo_LoggerManager::getLogger('publicite');
        }
        $logger->info('Début appelle WS update  du dossier du dossier URI : '.Atexo_Config::getParameter('URL_SUB_ANNONCE_REST_UPDATE_DOSSIER_ANNONCE'));
        $logger->info('xml request: '.$xml);
        $xmlResponse = self::sendFluxASUB(
            Atexo_Config::getParameter('LOGIN_APPLI_ACCES_SUB_ANNONCE'),
            Atexo_Config::getParameter('PASSWORD_APPLI_ACCES_SUB_ANNONCE'),
            Atexo_Config::getParameter('URL_SUB_ANNONCE_REST_UPDATE_DOSSIER_ANNONCE'),
            $logger,
            'POST',
            $xml
        );
        $logger->info('traitement xml response: '.print_r($xmlResponse, true));
        $res = self::traiterReponseSub($xmlResponse, $logger, '', '', false, 'reponse', 'statutReponse');
        $logger->info('Fin appelle WS update du dossier, valeur retourne par la fct  Atexo_FormulaireSub_AnnonceEchange::updateAnnonce '.print_r($res, true));

        return $res;
    }

    /**
     * permet d'envoyer un flux à SUB_ANNONCE.
     *
     * @param string              $identifiant identifiant du WS
     * @param string              $pwd         mot de passe du WS
     * @param string              $uri         uri WS appelle
     * @param Atexo_LoggerManager $logger      log de trace
     * @param string              $xml         xml à envoyer
     * @param string              $typeData    type de data envoye
     * @param string              $fct         fonction HTTP utilise
     *
     * @return interger identifiant du dossier créé sur SUB_ANNONCE
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2015
     */
    public static function sendFluxASUB($identifiant, $pwd, $uri, $logger, $fct = 'POST', $xml = '', $typeData = 'application/xml')
    {
        if (!$logger) {
            $logger = Atexo_LoggerManager::getLogger('publicite');
        }
        try {
            $configProxy = self::getConfigProxyForGuzzle(Atexo_Util::getSfContainer()) ?: [];
            $configProxy['timeout'] = 120;
            $client = new Client($configProxy);

            $options = [];
            if ($xml) {
                $options = [
                    'headers' => [
                        'Content-Type' => $typeData
                    ],
                    'body' => $xml,
                ];
            }
            $options['auth'] = [
                $identifiant,
                $pwd
            ];
            $fct = ucfirst(strtolower($fct));
            $res = $client->$fct($uri, $options)->getBody();

            return $res;
        } catch (Exception $e) {
            $logger->error('Erreur Atexo_FormulaireSub_AnnonceEchange::sendFluxASUB : '.$e->getMessage().' '.$e->getTraceAsString());
        }
    }

    /**
     * permet recuperer la valeur d'une balise de xml retourne par SUB_ANNONCE.
     *
     * @param string              $res      xml reponse SUB_ANNONCE
     * @param string              $element  element xml reponse SUB_ANNONCE
     * @param string              $attribue attribue de l'element xml reponse SUB_ANNONCE
     * @param Atexo_LoggerManager $logger   log de trace
     *
     * @return interger valeur de l'attribue precise
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2015
     */
    public static function traiterReponseSub($res, $logger, $element = '', $attribue = '', $node = false, $reponse = 'reponseget', $statut = 'statut')
    {
        $value = null;
        if (!$logger) {
            $logger = Atexo_LoggerManager::getLogger('publicite');
        }
        try {
            if(!empty($res)) {
                $domDoc = new DomDocument();
                $domDoc->loadXML($res);
                $reponsegets = $domDoc->getElementsByTagName($reponse);
                $value = false;
                if ($reponsegets instanceof \DOMNodeList) {
                    foreach ($reponsegets as $reponseget) {
                        if ('OK' == (string)$reponseget->getAttribute($statut)) {
                            if ($element && $attribue) {
                                $results = $reponseget->getElementsByTagName($element);
                                foreach ($results as $result) {
                                    if ($node) {
                                        $value = (string)$result->nodeValue;
                                    } else {
                                        $value = $result->getAttribute($attribue);
                                    }
                                    $logger->info($attribue.' : '.$value);
                                }
                            } else {
                                $value = true;
                            }
                        } else {
                            $error = $reponseget->textContent;
                            $logger->error('Erreur retourné par SUB  : '.print_r($error, true));
                        }
                    }
                } else {
                    $logger->error(
                        'Erreur Atexo_FormulaireSub_AnnonceEchange::traiterReponseSub : on trouve pas la balise reponseget au niveau du xml'
                    );
                }
            }
        } catch (Exception $e) {
            $logger->error('Erreur Atexo_FormulaireSub_AnnonceEchange::traiterReponseSub '.$element.'.'.$attribue.' : '.$e->getMessage().' '.$e->getTraceAsString());
        }

        return $value;
    }

    /**
     * permet de changer l'etat du dossier SUB_ANNONCE à A_PUBLIER.
     *
     * @param int                 $idDossier identifiant du dossier SUB_ANNONCE
     * @param Atexo_LoggerManager $logger    log de trace
     *
     * @return bool etat change ou pas
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2015
     */
    public static function publierDossier($idDossier, $logger = null)
    {
        if (!$logger) {
            $logger = Atexo_LoggerManager::getLogger('publicite');
        }
        if ($idDossier) {
            $logger->info('Début appel suppression verrou dossier : '.$idDossier);
            //deverouiller le dossier chez SUB - tous les verrous du dossier sont supprimes (loginCompte ignore)
            (new Atexo_FormulaireSub_Echange())->unlockDossier($idDossier, 'mpe', $idDossier);
        }
        $logger->info('Début appelle WS publication URI : '.Atexo_Config::getParameter('URL_SUB_ANNONCE_REST_PUBLICATION_DOSSIER_ANNONCE').$idDossier.' identifiant dossier : '.$idDossier);
        $xmlResponse = self::sendFluxASUB(
            Atexo_Config::getParameter('LOGIN_APPLI_ACCES_SUB_ANNONCE'),
            Atexo_Config::getParameter('PASSWORD_APPLI_ACCES_SUB_ANNONCE'),
            Atexo_Config::getParameter('URL_SUB_ANNONCE_REST_PUBLICATION_DOSSIER_ANNONCE').$idDossier,
            $logger,
            'POST'
        );
        $logger->info('traitement xml response: '.print_r($xmlResponse, true));
        $res = self::traiterReponseSub($xmlResponse, $logger, '', '', false, 'reponse', 'statutReponse');
        $logger->info('Fin appelle WS publication, valeur retourne par la fct  Atexo_FormulaireSub_AnnonceEchange::publierDossier  '.print_r($res, true));

        return $res;
    }

    /**
     * permet d'avoir le dossier SUB_ANNONCE.
     *
     * @param int                 $idDossier identifiant du dossier SUB_ANNONCE
     * @param Atexo_LoggerManager $logger    log de trace
     *
     * @return string xml SUB_ANNONCE
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2015
     */
    public static function getDossier($idDossier, $logger = null)
    {
        if (!$logger) {
            $logger = Atexo_LoggerManager::getLogger('publicite');
        }
        $logger->info('Début appelle WS get dossier URI : '.Atexo_Config::getParameter('URL_SUB_ANNONCE_REST_GET_DOSSIER_ANNONCE').$idDossier.' identifiant dossier : '.$idDossier);
        $xmlResponse = self::sendFluxASUB(
            Atexo_Config::getParameter('LOGIN_APPLI_ACCES_SUB_ANNONCE'),
            Atexo_Config::getParameter('PASSWORD_APPLI_ACCES_SUB_ANNONCE'),
            Atexo_Config::getParameter('URL_SUB_ANNONCE_REST_GET_DOSSIER_ANNONCE').$idDossier,
            $logger,
            'GET'
        );
        $logger->info('Fin appelle WS get dossier, Atexo_FormulaireSub_AnnonceEchange::getDossier : traitement xml response: '.print_r($xmlResponse, true));

        return $xmlResponse;
    }

    /**
     * Permet de supprimer un dossier correspondant a une annonce au niveau de SUB.
     *
     * @param int                 $idDossier identifiant du dossier SUB_ANNONCE
     * @param Atexo_LoggerManager $logger    log de trace
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public static function supprimerDossier($idDossier, $logger = null)
    {
        if (!$logger) {
            $logger = Atexo_LoggerManager::getLogger('publicite');
        }
        $uri = Atexo_Config::getParameter('URL_SUB_ANNONCE_REST_SUPPRESSION_DOSSIER_ANNONCE').'?iddossier='.$idDossier;
        $logger->info("Début appelle WS suppression dossier SUB : URI = $uri , identifiant dossier sub = ".$idDossier);
        $xmlResponse = self::sendFluxASUB(Atexo_Config::getParameter('LOGIN_APPLI_ACCES_SUB_ANNONCE'), Atexo_Config::getParameter('PASSWORD_APPLI_ACCES_SUB_ANNONCE'), $uri, $logger, 'GET');
        $logger->info('traitement xml response: '.print_r($xmlResponse, true));
        $res = self::traiterReponseSub($xmlResponse, $logger);
        $logger->info("Fin appelle WS suppression dossier SUB : URI = $uri , identifiant dossier sub = ".$idDossier.' , retour : '.print_r($res, true));

        return $res;
    }

    /**
     * permet de creer le xml a envoye à SUB_ANNONCE.
     *
     * @param CommonConsultation                                  $consultation consultation depuis on va creer le dossier SUB_ANNONCE
     * @param CommonTAnnonceConsultation                          $annonce      annonce consultation
     * @param CommonAcheteurPublic | CommonTConsultationComptePub $compteBoamp  compte pub
     * @param array                                               $listeSupport liste supports
     * @param Atexo_LoggerManager                                 $logger       log de trace
     * @param int                                                 $idDossier    identifiant du dossier
     *
     * @return string xml a envoye à SUB_ANNONCE
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2015
     */
    public static function getXmlInitialisationDossier($consultation, $annonce, $compte, $listeSupport, $pubSimplifier = false, $isBrouillon = 'true', $offresCodes = [])
    {
        $logger = null;
        try {
            $logger = Atexo_LoggerManager::getLogger('publicite');
            if ($annonce instanceof CommonTAnnonceConsultation) {
                $idDispositif = $annonce->getIdDispositif();
                $idDossier = $annonce->getIdDossierSub();
                $xml = '';
                $xml .= '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>';
                $xml .= '<dossiercreation filtreBinding="true" loginTiers="'.Atexo_Config::getParameter('IDENTIFIANT_ACCES_SUB_ANNONCE').'" idDispositif="'.$idDispositif.'" '.
                    ($idDossier ? ' idDossier="'.$idDossier.'"' : ' etatDemande="demande_pre_valide" ').' >';
                $xml .= '<dossier_datas>';
                $xml .= self::baliseBinding('etat_brouillon', $isBrouillon);
                if (!empty($listeSupport) && is_array($listeSupport) && count($listeSupport)) {
                    $xml .= self::baliseBinding('canal_type_concentrateur', implode('|', $listeSupport));
                }
                $xml .= self::getXmlInitialisationDepuisCompteBOAMP($compte, $pubSimplifier);
                $xml .= self::getXmlInitialisationDepuisComptePub($compte);

                $xml .= self::getXmlInitialisationDepuisAnnonce($annonce, $pubSimplifier, $offresCodes, $listeSupport ?? []);

                if ($consultation instanceof CommonConsultation) {
                    $xml .= self::getXmlInitialisationDepuisConsultation($consultation);
                } else {
                    $logger->error("Erreur Atexo_FormulaireSub_AnnonceEchange::getXmlInitialisationDossier l'objet envoyé n'est pas une consultation ".print_r($consultation, true));
                }

                $xml .= '</dossier_datas>';
                $xml .= '</dossiercreation>';

                return $xml;
            }
        } catch (Exception $e) {
            $logger->error('Erreur Atexo_FormulaireSub_AnnonceEchange::getXmlInitialisationDossier : '.$e->getMessage().' '.$e->getTraceAsString());
        }
    }

    /**
     * permet de creer le xml a envoye à SUB_ANNONCE.
     *
     * @param CommonTAnnonceConsultation $annonce consultation depuis on va creer le dossier SUB_ANNONCE
     * @param Atexo_LoggerManager        $logger  log de trace
     *
     * @return string xml a envoye à SUB_ANNONCE
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2015
     */
    public static function getXmlInitialisationDepuisAnnonce($annonce, $pubSimplifier = false, $offresCodes = [], $listeSupport = [])
    {
        $logger = null;
        try {
            $logger = Atexo_LoggerManager::getLogger('publicite');
            $xml = '';
            if ($annonce instanceof CommonTAnnonceConsultation) {
                if (!$pubSimplifier && $annonce->getNumeroAvisBoampParent()) {
                    $xml .= self::baliseBinding('avisInitial_idWeb', $annonce->getNumeroAvisBoampParent());
                }
                $offres = [];
                if (is_array($offresCodes) && count($offresCodes)) {
                    $offres = $offresCodes;
                }
                foreach ($listeSupport as $supportCode) {
                    $supportPub = Atexo_Publicite_AnnonceSub::getSupportPublicationByCode($supportCode);
                    if ($supportPub instanceof CommonTSupportPublication) {
                        $support = $annonce->getSupportAnnonce($supportPub->getId());
                        if ($support instanceof CommonTSupportAnnonceConsultation) {
                            if (!empty($support->getDepartementsParutionAnnonce())) {
                                $liste = explode(',', $support->getDepartementsParutionAnnonce());
                                $codesDepartements = Atexo_Geolocalisation_GeolocalisationN2::retrieveDenomination2ByIdsGeoN2($liste, '|');
                                $xml .= self::baliseBinding('CodesDepartements', $codesDepartements, 'stringType');
                            }
                            if (!empty($support->getMessageAcheteur())) {
                                $xml .= self::baliseBinding('messageAcheteur', $support->getMessageAcheteur(), 'textType');
                            }
                            if (empty($offresCodes)) {
                                $offre = $support->getOffre();
                                if ($offre instanceof CommonTOffreSupportPublicite) {
                                    $offres[] = $offre->getCode();
                                }
                            }
                        }
                    }
                }
                if (is_array($offres) && count($offres)) {
                    $xml .= self::baliseBinding('offre_type_concentrateur', implode('|', $offres));
                }
            }

            return $xml;
        } catch (Exception $e) {
            $logger->error('Erreur Atexo_FormulaireSub_AnnonceEchange::getXmlInitialisationDepuisAnnonce : '.$e->getMessage().' '.$e->getTraceAsString());
        }
    }

    /**
     * permet de creer le xml a envoye à SUB_ANNONCE.
     *
     * @param CommonConsultation  $consultation consultation depuis on va creer le dossier SUB_ANNONCE
     * @param Atexo_LoggerManager $logger       log de trace
     *
     * @return string xml a envoye à SUB_ANNONCE
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2015
     */
    public static function getXmlInitialisationDepuisConsultation($consultation)
    {
        $nombreCandidatAdmis = null;
        $logger = null;
        try {
            $logger = Atexo_LoggerManager::getLogger('publicite');
            $xml = '';
            if ($consultation instanceof CommonConsultation) {
                $donnesComplaimentaireCons = $consultation->getDonneComplementaire();
                $xml .= self::baliseBinding('descriptionMarche_numeroReference', $consultation->getReferenceUtilisateur());
                $xml .= self::baliseBinding('organisme_acronyme', $consultation->getOrganisme());
                $xml .= self::baliseBinding('email_agent', Atexo_CurrentUser::getEmail());
                $xml .= self::baliseBinding('service_id', $consultation->getServiceId());

                $codeNatureMarche = '';
                switch ($consultation->getCategorie()) {
                    case Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX'):
                        $codeNatureMarche = '01_travaux';
                        break;
                    case Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES'):
                        $codeNatureMarche = '02_fournitures';
                        break;
                    case Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES'):
                        $codeNatureMarche = '03_services';
                        break;
                }
                if ($codeNatureMarche) {
                    $xml .= self::baliseBindingReferentiel('descriptionMarche_natureMarche', $codeNatureMarche, 'NatureMarché');
                }
                if ($donnesComplaimentaireCons instanceof CommonTDonneeComplementaire) {
                    if ($donnesComplaimentaireCons->getPublicationMontantEstimation()) {
                        $xml .= self::baliseBinding('descriptionMarche_estimationValeurTotal', $donnesComplaimentaireCons->getValeurMontantEstimationPubliee(), 'doubleType');
                        $xml .= self::baliseBindingReferentiel('descriptionMarche_estimationValeurTotal_devise', 'EUR', 'devise');
                    }
                    if ($donnesComplaimentaireCons->getMotsCles()) {
                        $motsCles = $donnesComplaimentaireCons->getValeurSubMotsCles();
                        if (is_array($motsCles)) {
                            $xml .= self::baliseBindingItemReferentiel('descriptionMarche_motsClef', $motsCles, 'motsClef');
                        }
                    }
                    $procedureOmc = '2';
                    if ($donnesComplaimentaireCons->getProcedureAccordMarchesPublicsOmc()) {
                        $procedureOmc = '1';
                    }
                    $xml .= self::baliseBindingReferentiel('description_AMP', $procedureOmc, 'Réponse_oui_non');

                    if ($donnesComplaimentaireCons->getDelaiValiditeOffres()) {
                        $xml .= self::baliseBinding('renseignementsAdm_validiteOffreDuree', $donnesComplaimentaireCons->getDelaiValiditeOffres(), 'longType');
                    }

                    $ref = (new Atexo_CommonReferentiel())->retrieveValeurReferentielById(Atexo_Config::getParameter('REFERENTIEL_FORME_GROUPEMENT'), $donnesComplaimentaireCons->getIdGroupementAttributaire());
                    $formeGroupement = ($ref instanceof CommonValeurReferentiel) ? $ref->getLibelleValeurReferentiel() : '';
                    $xml .= self::baliseBinding('formeGroupement', $formeGroupement, 'textType');

                    if ($donnesComplaimentaireCons->getIdNbCandidatsAdmis() == Atexo_Config::getParameter('ID_NB_CANDIDATS_ADMIS_FIXE')) {
                        $nombreCandidatAdmis = sprintf(Prado::localize('NBRE_CANDIDATS_INVITES_A_PARTICIPER'), $donnesComplaimentaireCons->getNombreCandidatsFixe());
                    } elseif ($donnesComplaimentaireCons->getIdNbCandidatsAdmis() == Atexo_Config::getParameter('ID_NB_CANDIDATS_ADMIS_FOURCHETTE')) {
                        $min = $donnesComplaimentaireCons->getNombreCandidatsMin();
                        $max = $donnesComplaimentaireCons->getNombreCandidatsMax();
                        $nombreCandidatAdmis = sprintf(Prado::localize('NBRE_CANDIDATS_INVITES_A_PARTICIPER_ENTRE'), $min, $max);
                    }
                    $xml .= self::baliseBinding('nombreCandidatAdmis', $nombreCandidatAdmis, 'textType');
                }

                $typeProcedure = $consultation->getTypeProcedure();
                if ($typeProcedure instanceof CommonTypeProcedure && $typeProcedure->getValueBindingSub()) {
                    $xml .= self::baliseBinding('procedureLibelleAlphanumerique', $typeProcedure->getLibelleTypeProcedure());
                    $xml .= self::baliseBindingReferentiel('typeProcedureMPE', $typeProcedure->getValueBindingSub(), 'typeProcedureMPE');
                }
                $mps = '2';
                if ($consultation->getMarchePublicSimplifie()) {
                    $mps = '1';
                }
                $xml .= self::baliseBindingReferentiel('EligibleMPS', $mps, 'Réponse_oui_non');
                $xml .= self::baliseBinding('descriptionMarche_titreMarche', Atexo_Util::atexoHtmlEntitiesDecode($consultation->getIntituleTraduit()));
                $xml .= self::baliseBinding('descriptionMarche_description', Atexo_Util::atexoHtmlEntitiesDecode($consultation->getObjetTraduit()), 'textType');
                if ($consultation->getCodeCpv1() || $consultation->getCodeCpv2()) {
                    if (Atexo_Module::isEnabled('Publicite')) {
                        $xml .= self::baliseBinding('descriptionMarche_CPV_objetPrincipal_classPrincipale_alphanumerique', Atexo_Util::atexoHtmlEntitiesDecode($consultation->getCodeCpv1()), 'stringType');
                        $cpvs = explode('#', $consultation->getCodeCpv2());
                        $i = 1;
                        foreach ($cpvs as $cpv) {
                            if ($i > 3) {
                                break;
                            }
                            if (!empty($cpv)) {
                                $xml .= self::baliseBinding('descriptionMarche_CPV_objetPrincipal_classSupplementaire_alphanumerique_'.$i++, $cpv, 'stringType');
                            }
                        }
                    } else {
                        $xml .= self::baliseBindingReferentiel('descriptionMarche_CPV_objetPrincipal_classPrincipale', $consultation->getCodeCpv1(), 'CPV');
                        $cpvs = explode('#', $consultation->getCodeCpv2());
                        $i = 1;
                        foreach ($cpvs as $cpv) {
                            if ($i > 4) {
                                break;
                            }
                            if (!empty($cpv)) {
                                $xml .= self::baliseBindingReferentiel('descriptionMarche_CPV_objetPrincipal_classSupplementaire'.$i++, $cpv, 'CPV');
                            }
                        }
                    }
                }
                if ($consultation->getAlloti()) {
                    $valeurAlloti = '02_lots';
                    $clauseSociele = '';
                    $clauseEnv = '';
                } else {
                    $valeurAlloti = '01_marcheUnique';
                    $clauseSociele = $consultation->getClauseSociale();
                    $clauseEnv = $consultation->getClauseEnvironnementale();
                }
                $xml .= self::baliseBindingReferentiel('descriptionMarche_marcheUnique_OU_lots', $valeurAlloti, 'TypeMarché');
                $xml .= self::baliseBindingReferentiel('criteresSE_criteresSociaux', $clauseSociele, 'Réponse_oui_non');
                $xml .= self::baliseBindingReferentiel('criteresSE_criteresEnvironnementaux', $clauseEnv, 'Réponse_oui_non');

                if ($consultation->getDatefin() && '0000-00-00 00:00:00' != $consultation->getDatefin()) {
                    $xml .= self::baliseBinding('renseignementsAdm_receptionInteret', Atexo_Util::iso2frnDateTime($consultation->getDatefin()).':00', 'dateTimeType');
                    $xml .= self::baliseBinding('renseignementsAdm_dateOuverture', Atexo_Util::iso2frnDateTime($consultation->getDatefin()).':00', 'dateTimeType');
                    $dateFin = explode(' ', $consultation->getDatefin());
                    $xml .= self::baliseBinding('indexation_dateFinDiffusion', Atexo_Util::iso2frnDate($dateFin[0]), 'dateType');
                }

                // Organisme
                $organisme = Atexo_Organismes::retrieveOrganismeByAcronyme($consultation->getOrganisme());
                if ($organisme instanceof CommonOrganisme) {
                    $xml .= self::baliseBinding('facturation_libelle_siret', $organisme->getSiren().$organisme->getComplement(), 'longType');
                }
                $xml .= self::baliseBinding('renseignementsAdm_lieuOuverture', Atexo_Util::atexoHtmlEntitiesDecode($consultation->getLieuOuverturePlisTraduit()));
                $xml .= self::baliseBindingReferentiel('languages', 'FR', 'Langues2');
                $afficherLibelle = Atexo_Config::getParameter('AFFICHER_LIBELLE_INFOS_COMP_PUB');
                $infoComp = '';
                if ($donnesComplaimentaireCons instanceof CommonTDonneeComplementaire) {
                    if ($donnesComplaimentaireCons->getCautionnementRegimeFinancier()) {
                        $infoComp .= '
'.($afficherLibelle ? Prado::localize('DEFINE_CAUTIONNEMENT_ET_GARANTIES_EXIGEES_CAS_ECHEANT').' : ' : '')
                        .$donnesComplaimentaireCons->getCautionnementRegimeFinancier();
                    }
                    if ($donnesComplaimentaireCons->getModalitesFinancementRegimeFinancier()) {
                        $infoComp .= '

'.($afficherLibelle ? Prado::localize('DEFINE_MODALITES_ESSENTIELLES_DE_FINANCEMENT_ET_DE_PAIEMENT_REFERENCE_AUX_TEXTES_QUI_LES_REGLEMENTENT').' : ' : '')
                        .$donnesComplaimentaireCons->getModalitesFinancementRegimeFinancier();
                    }
                }
                $conditionParticipation = (new Atexo_Referentiel_Referentiel())->getInfoFromReferentielConsultation(Atexo_Config::getParameter('CONDITION_PARTICIPATION'), $consultation->getId(), 0, $consultation->getOrganisme());
                if ($conditionParticipation) {
                    $infoComp .= '

'.($afficherLibelle ? Prado::localize(Atexo_Config::getParameter('CONDITION_PARTICIPATION')).' : ' : '')
                    .$conditionParticipation;
                }

                if ($consultation->getAdresseRetraisDossiersTraduit()) {
                    $infoComp .= '
'.($afficherLibelle ? Prado::localize('ADRESSE_RETRAIT_DOSSIERS').' : ' : '')
                    .$consultation->getAdresseRetraisDossiersTraduit();
                }
                if ($consultation->getLieuOuverturePlisTraduit()) {
                    $infoComp .= '
'.($afficherLibelle ? Prado::localize('LIEU_OUVERTURE_PLIS').' : ' : '').$consultation->getLieuOuverturePlisTraduit();
                }
                $modalitesVisites = (new Atexo_Referentiel_Referentiel())->getInfoFromReferentielConsultation(Atexo_Config::getParameter('MODALITES_VISITES_LIEUX_REUNIONS'), $consultation->getId(), 0, $consultation->getOrganisme());
                if ($modalitesVisites) {
                    $infoComp .= '
'.($afficherLibelle ? Prado::localize(Atexo_Config::getParameter('MODALITES_VISITES_LIEUX_REUNIONS')).' : ' : '').$modalitesVisites;
                }
                if ($consultation->getAdresseDepotOffresTraduit()) {
                    $infoComp .= '
'.($afficherLibelle ? Prado::localize('ADRESSE_DEPOT_OFFRE').' : ' : '').$consultation->getAdresseDepotOffresTraduit();
                }
                $info = (new Atexo_Referentiel_Referentiel())->getInfoFromReferentielConsultation(Atexo_Config::getParameter('AUTRE_INFOS'), $consultation->getId(), 0, $consultation->getOrganisme());
                if ($info) {
                    $infoComp .= '
'.($afficherLibelle ? Prado::localize(Atexo_Config::getParameter('AUTRE_INFOS')).' : ' : '').$info;
                }

                $xml .= self::baliseBinding('infoCompl', Atexo_Util::atexoHtmlEntitiesDecode($infoComp), 'textType');

                $option['multi_domaine'] = (bool) Atexo_Config::getParameter('PF_ACTIVE_MULTI_DOMAINE');
                $urlPf = Atexo_MultiDomaine::getDomaine(Atexo_Config::getParameter('PF_URL_REFERENCE'), $option);
                $url = $urlPf.'?page=Entreprise.EntrepriseDetailsConsultation&id='.$consultation->getId().'&orgAcronyme='.$consultation->getOrganisme();
                $xml .= self::baliseBinding('communication_documentMarche_urlDocument', $url);
                $xml .= self::baliseBinding('communication_urlOutilLogiciel', $url);
                $xml .= self::baliseBinding('communication_participation_urlParticipationAdresse', $url);
                $codeNuts = explode('#', ltrim($consultation->getCodesNuts(), '#'));
                $xml .= (is_array($codeNuts) && count($codeNuts)) ? self::baliseBindingReferentiel('organisme_codeNUTS', $codeNuts[0], 'NUTS') : '';
                $xml .= self::getFluxDepuisLots($consultation);
            } else {
                $logger->error("Erreur Atexo_FormulaireSub_AnnonceEchange::getXmlInitialisationDepuisConsultation l'objet envoyé n'est pas une consultation ".$consultation->getId());
            }

            return $xml;
        } catch (\Exception $e) {
            $logger->error('Erreur Atexo_FormulaireSub_AnnonceEchange::getXmlInitialisationDepuisConsultation : '.$e->getMessage().' '.$e->getTraceAsString());
        }
    }

    /**
     * permet de creer le xml a envoye à SUB_ANNONCE.
     *
     * @param CommonAcheteurPublic $compteBoamp compte BOAMP
     * @param bool                 $logger      log de trace
     *
     * @return string xml a envoye à SUB_ANNONCE
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2015
     */
    public static function getXmlInitialisationDepuisCompteBOAMP($compteBoamp, $pubSimplifier = false)
    {
        $bindindTypeActivite = null;
        $codeReferentiel = null;
        $logger = null;
        try {
            $logger = Atexo_LoggerManager::getLogger('publicite');
            $xml = '';
            if ($compteBoamp instanceof CommonAcheteurPublic) {
                $xml .= self::baliseBinding('boamp_identifiant', Atexo_Util::atexoHtmlEntitiesDecode($compteBoamp->getBoampLogin()));
                $xml .= self::baliseBinding('boamp_mel', Atexo_Util::atexoHtmlEntitiesDecode($compteBoamp->getBoampMail()));
                $xml .= self::baliseBinding('organisme_PersonnePhysique_nom', Atexo_Util::atexoHtmlEntitiesDecode($compteBoamp->getPrm()));
                $xml .= self::baliseBinding('organisme_PersonnePhysique_pren', Atexo_Util::atexoHtmlEntitiesDecode($compteBoamp->getPrm()));
                $xml .= self::baliseBindingReferentiel('organisme_PersonnePhysique_civilite', 'M', 'Civilité');
                $xml .= self::baliseBinding('organisme_acheteurPublic', Atexo_Util::atexoHtmlEntitiesDecode($compteBoamp->getDenomination()), 'textType');
                $adresseOrg = false;
                $adresseOrgFacture = false;
                if ($compteBoamp->getAdresse() || $compteBoamp->getCp() || $compteBoamp->getVille()) {
                    $adresseOrg = true;
                }
                if ($compteBoamp->getFactureAdresse() || $compteBoamp->getFactureCp() || $compteBoamp->getFactureVille()) {
                    $adresseOrgFacture = true;
                }

                if ($adresseOrg) {
                    $xml .= self::baliseBindingAdresse('organisme_adr', $compteBoamp->getAdresse(), $compteBoamp->getCp(), $compteBoamp->getVille());
                } elseif ($adresseOrgFacture) {
                    $xml .= self::baliseBindingAdresse('organisme_adr', $compteBoamp->getFactureAdresse(), $compteBoamp->getFactureCp(), $compteBoamp->getFactureVille());
                }
                if ($adresseOrgFacture) {
                    $xml .= self::baliseBindingAdresse('facturation_libelle_adr', $compteBoamp->getFactureAdresse(), $compteBoamp->getFactureCp(), $compteBoamp->getFactureVille());
                } elseif ($adresseOrg) {
                    $xml .= self::baliseBindingAdresse('facturation_libelle_adr', $compteBoamp->getAdresse(), $compteBoamp->getCp(), $compteBoamp->getVille());
                }
                if ($compteBoamp->getTypeOrg()) {
                    $typeOrganisme = (new Atexo_Referentiel())->retrieveValeurReferentielById(Atexo_Config::getParameter('REFERENCE_TYPE_ORGANISME'), $compteBoamp->getOrganisme(), $compteBoamp->getTypeOrg());
                    if ($typeOrganisme instanceof CommonValeurReferentielOrg && $typeOrganisme->getValeurSub()) {
                        if (!$pubSimplifier) {
                            $xml .= self::baliseBindingReferentiel('organisme_classificationOrganisme', $typeOrganisme->getValeurSub(), 'TypeOrganisme');
                        }
                        $xml .= self::baliseBindingReferentiel('facturation_libelle_classeProfit', $typeOrganisme->getValeurSub(), 'TypeOrganisme');
                    }
                }

                $xml .= self::baliseBinding('facturation_libelle_denomination', Atexo_Util::atexoHtmlEntitiesDecode($compteBoamp->getFactureDenomination()));
                if ($compteBoamp->getDepartementMiseEnLigne()) {
                    $depts = explode(',', $compteBoamp->getDepartementMiseEnLigne());
                    $subDepts = [];
                    foreach ($depts as $dept) {
                        if (!empty($dept)) {
                            $geoN2 = (new Atexo_Geolocalisation_GeolocalisationN2())->retrieveGeolocalisationN2ById($dept);
                            if (is_array($geoN2) && isset($geoN2[0]) && $geoN2[0] instanceof CommonGeolocalisationN2) {
                                $subDepts[] = $geoN2[0]->getValeurSub();
                            }
                        }
                    }
                    $xml .= self::baliseBindingItemReferentiel('departementsMiseEnLigne', $subDepts, 'Départements');
                }

                $xml .= self::baliseBinding('facturation_numBonCommande', Atexo_Util::atexoHtmlEntitiesDecode($compteBoamp->getReferenceCommande()));

                if (!$pubSimplifier) {
                    $xml .= self::baliseBinding('organisme_coord_tel', $compteBoamp->getTelephone());
                    $xml .= self::baliseBinding('organisme_coord_fax', $compteBoamp->getFax());
                    $xml .= self::baliseBinding('organisme_coord_url', $compteBoamp->getUrl());
                    $xml .= self::baliseBinding('organisme_urlProfilAcheteur', $compteBoamp->getUrlAcheteur());

                    $renseignements = (new Atexo_Publicite_RenseignementsBoamp())->retrieveRenseignementsBoampByIdType($compteBoamp->getId());
                    if (is_array($renseignements)) {
                        foreach ($renseignements as $renseignement) {
                            if ($renseignement instanceof CommonRenseignementsBoamp) {
                                $prefixLibelleBinding = '';
                                $correspondant = false;
                                $organeChargeProcedure = false;
                                $bindingCaseCoche = false;
                                $typeStringBindingCaseCoche = true;
                                switch ($renseignement->getIdType()) {
                                    case Atexo_Config::getParameter('ADRESSE_COMPLEMENTAIRES_1'):
                                        $correspondant = true;
                                        $prefixLibelleBinding = 'adressesComplt_admOuTech_adm';
                                        $bindingCaseCoche = 'adressesComplt_admOuTech_adm';
                                        break;
                                    case Atexo_Config::getParameter('ADRESSE_COMPLEMENTAIRES_2'):
                                        $correspondant = true;
                                        $prefixLibelleBinding = 'adressesComplt_admOuTech_tech';
                                        $bindingCaseCoche = 'adressesComplt_admOuTech_tech';
                                        break;
                                    case Atexo_Config::getParameter('ADRESSE_COMPLEMENTAIRES_3'):
                                        $correspondant = true;
                                        $prefixLibelleBinding = 'adressesComplt_document';
                                        $bindingCaseCoche = 'adressesComplt_document';
                                        break;
                                    case Atexo_Config::getParameter('ADRESSE_COMPLEMENTAIRES_4'):
                                        $correspondant = true;
                                        $prefixLibelleBinding = 'adressesComplt_envoi';
                                        $bindingCaseCoche = 'adressesComplt_envoi';
                                        break;
                                    case Atexo_Config::getParameter('ADRESSE_COMPLEMENTAIRES_5'):
                                        $correspondant = true;
                                        $prefixLibelleBinding = 'adressesComplt_rensComplementaires';
                                        $bindingCaseCoche = 'adressesComplt_rensComplementaires';
                                        break;
                                    case Atexo_Config::getParameter('PROCEDURE_RECOURS_1'):
                                        $prefixLibelleBinding = 'procedureRecours_instanceChargeeRecours';
                                        break;
                                    case Atexo_Config::getParameter('PROCEDURE_RECOURS_2'):
                                        $prefixLibelleBinding = 'procedureRecours_instanceChargeemediation';
                                        break;
                                    case Atexo_Config::getParameter('PROCEDURE_RECOURS_3'):
                                        $organeChargeProcedure = 'procedureRecours_introductionRecoursPrecisions';
                                        $prefixLibelleBinding = '';
                                        break;
                                    case Atexo_Config::getParameter('PROCEDURE_RECOURS_4'):
                                        $prefixLibelleBinding = 'procedureRecours_introductionRecoursRenseignements';
                                        $bindingCaseCoche = 'procedureRecours_introductionRecoursPrecisions_OU_introductionRecoursRenseignements';
                                        $typeStringBindingCaseCoche = false;
                                        break;
                                }
                                if ($organeChargeProcedure && !empty($renseignement->getOrganeChargeProcedure())) {
                                    $xml .= self::baliseBinding($organeChargeProcedure, $renseignement->getOrganeChargeProcedure(), 'textType');
                                }
                                $hasValue = 'false';
                                if ($prefixLibelleBinding) {
                                    if ($renseignement->getCorrespondant() && $correspondant) {
                                        $hasValue = 'true';
                                        $xml .= self::baliseBinding($prefixLibelleBinding.'_PersonnePhysique_nom', $renseignement->getCorrespondant());
                                        $xml .= self::baliseBinding($prefixLibelleBinding.'_PersonnePhysique_pren', $renseignement->getCorrespondant());
                                        $xml .= self::baliseBindingReferentiel($prefixLibelleBinding.'_PersonnePhysique_civilite', 'M', 'Civilité');
                                    }
                                    if ($renseignement->getOrganisme()) {
                                        $hasValue = 'true';
                                        $xml .= self::baliseBinding($prefixLibelleBinding.'_PersonneMorale', Atexo_Util::atexoHtmlEntitiesDecode($renseignement->getOrganisme()), 'textType');
                                    }
                                    if ($renseignement->getTelephone()) {
                                        $hasValue = 'true';
                                        $xml .= self::baliseBinding($prefixLibelleBinding.'_coord_tel', $renseignement->getTelephone());
                                    }
                                    if ($renseignement->getFax()) {
                                        $hasValue = 'true';
                                        $xml .= self::baliseBinding($prefixLibelleBinding.'_coord_fax', $renseignement->getFax());
                                    }
                                    if ($renseignement->getMail()) {
                                        $hasValue = 'true';
                                        $xml .= self::baliseBinding($prefixLibelleBinding.'_coord_mel', $renseignement->getMail());
                                    }
                                    if ($renseignement->getUrl()) {
                                        $hasValue = 'true';
                                        $xml .= self::baliseBinding($prefixLibelleBinding.'_coord_url', $renseignement->getUrl());
                                    }
                                }
                                if ($bindingCaseCoche) {
                                    if ($typeStringBindingCaseCoche) {
                                        $xml .= self::baliseBinding($bindingCaseCoche, $hasValue, 'booleanType');
                                    } else {
                                        $xml .= self::baliseBindingReferentiel($bindingCaseCoche, ('true' == $hasValue ? '1' : '2'), 'Réponse_oui_non');
                                    }
                                }
                            }
                        }
                    }

                    $xml .= self::baliseBinding('conditions_modFinancement', $compteBoamp->getModalitesFinancement(), 'textType');
                    $codesNute = explode('#', $compteBoamp->getCodeNuts());
                    if (is_array($codesNute)) {
                        $i = 1;
                        foreach ($codesNute as $code) {
                            if ($i > 4) {
                                break;
                            }
                            if (!empty($code)) {
                                $xml .= self::baliseBindingReferentiel('description_lieuCodeNUTS'.$i++, $code, 'NUTS');
                            }
                        }
                    }

                    if ($compteBoamp->getTypePouvoirActivite()) {
                        $valeurBindingsActivite = '';
                        if (strstr($compteBoamp->getTypePouvoirActivite(), Atexo_Config::getParameter('REFERENTIEL_PRINCIPAL_ACTIVITES_POUVOIR').'_')) {
                            $valeurBindingsActivite = 'pouvoirAdjudicateur';
                            $bindindTypeActivite = 'organisme_activitesOrganisme_activites';
                            $codeReferentiel = 'activites';
                        } elseif (strstr($compteBoamp->getTypePouvoirActivite(), Atexo_Config::getParameter('REFERENTIEL_PRINCIPAL_ACTIVITES_ENTITE').'_')) {
                            $valeurBindingsActivite = 'entiteAdjudicatrice';
                            $bindindTypeActivite = 'organisme_activitesOrganisme_activitesSecteursSpeciaux';
                            $codeReferentiel = 'Activités2';
                        }
                        $logger->info('organisme_activitesOrganisme_activiteDefiniePour  '.$valeurBindingsActivite);
                        if ($valeurBindingsActivite) {
                            $xml .= self::baliseBindingReferentiel('organisme_activitesOrganisme_activiteDefiniePour', $valeurBindingsActivite, 'activitePrincipaleDefiniePour');
                        }
                        $logger->info('bindindTypeActivite  '.$bindindTypeActivite);
                        if ($bindindTypeActivite) {
                            $codesActivite = [];
                            $typeActivites = explode('#', $compteBoamp->getTypePouvoirActivite());
                            foreach ($typeActivites as $type) {
                                if (!empty($type)) {
                                    $typeActivite = explode('_', $type);
                                    $ref = (new Atexo_CommonReferentiel())->retrieveValeurReferentielById($typeActivite[0], $typeActivite[1]);
                                    if ($ref instanceof CommonValeurReferentiel) {
                                        $codesActivite[] = $ref->getValeurSub();
                                    }
                                }
                            }
                            $xml .= self::baliseBindingItemReferentiel($bindindTypeActivite, $codesActivite, $codeReferentiel);
                        }
                    }
                }
            }

            return $xml;
        } catch (Exception $e) {
            $logger->error('Erreur Atexo_FormulaireSub_AnnonceEchange::getXmlInitialisationDepuisCompteBOAMP : '.$e->getMessage().' '.$e->getTraceAsString());
        }
    }

    /**
     * permet de creer le xml a envoye à SUB_ANNONCE.
     *
     * @param CommonTConsultationComptePub $comptePub compte PUB
     *
     * @return string xml a envoye à SUB_ANNONCE
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public static function getXmlInitialisationDepuisComptePub($comptePub)
    {
        $logger = null;
        try {
            $logger = Atexo_LoggerManager::getLogger('publicite');
            $xml = '';
            if ($comptePub instanceof CommonTConsultationComptePub) {
                $xml .= self::baliseBinding('boamp_identifiant', Atexo_Util::atexoHtmlEntitiesDecode($comptePub->getBoampLogin()));
                $xml .= self::baliseBinding('boamp_mel', Atexo_Util::atexoHtmlEntitiesDecode($comptePub->getBoampMail()));
                $xml .= self::baliseBinding('organisme_pointDeContact', Atexo_Util::atexoHtmlEntitiesDecode($comptePub->getPrm()));
                $xml .= self::baliseBinding('organisme_acheteurPublic', Atexo_Util::atexoHtmlEntitiesDecode($comptePub->getDenomination()), 'textType');
                $xml .= self::baliseBindingAdresse('organisme_adr', $comptePub->getAdresse(), $comptePub->getCp(), $comptePub->getVille());
                $xml .= self::baliseBinding('organisme_url', $comptePub->getUrl());

                $xml .= self::baliseBinding('procedureRecours_instanceChargeeRecours_PersonneMorale', Atexo_Util::atexoHtmlEntitiesDecode($comptePub->getInstanceRecoursOrganisme()), 'textType');
                $xml .= self::baliseBindingAdresse('procedureRecours_instanceChargeeRecours_adr', $comptePub->getInstanceRecoursAdresse(), $comptePub->getInstanceRecoursCp(), $comptePub->getInstanceRecoursVille());
                $xml .= self::baliseBinding('procedureRecours_instanceChargeeRecours_coord_url', $comptePub->getInstanceRecoursUrl());

                $xml .= self::baliseBinding('facturation_libelle_denomination', Atexo_Util::atexoHtmlEntitiesDecode($comptePub->getFactureDenomination()));
                $xml .= self::baliseBindingAdresse('facturation_libelle_adr', $comptePub->getFactureAdresse(), $comptePub->getFactureCp(), $comptePub->getFactureVille());
            }

            return $xml;
        } catch (Exception $e) {
            $logger->error('Erreur Atexo_FormulaireSub_AnnonceEchange::getXmlInitialisationDepuisComptePub : '.$e->getMessage().' '.$e->getTraceAsString());
        }
    }

    /**
     * Permet de construire l'xml des criteres d'attribution.
     *
     * @param CommonTDonneeComplementaire $objet
     *
     * @return string : xml criteres d'attribution
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public static function getXmlCriteresAttribution($objet)
    {
        $logger = Atexo_LoggerManager::getLogger('publicite');
        try {
            $xml = '';
            if ($objet instanceof CommonTDonneeComplementaire && '0' != $objet->getIdCritereAttribution()) {
                $oneRefCritere = (new Atexo_CommonReferentiel())->retrieveValeurReferentielById(Atexo_Config::getParameter('REFERENTIEL_CRITERE_ATTRIBUTION'), $objet->getIdCritereAttribution());
                if ($oneRefCritere instanceof CommonValeurReferentiel) {
                    $j = 1;
                    if ('layerDefinitionCriteres_1' == $oneRefCritere->getLibelle2() || 'layerDefinitionCriteres_2' == $oneRefCritere->getLibelle2()) {
                        $criteresAttribution = $oneRefCritere->getLibelleValeurReferentiel().' : '.PHP_EOL;
                        //Critères énoncés par ordre de priorité décroissante ou Critères énoncés avec leur pondération
                        $xml .= self::baliseBindingReferentiel('criteresOUcriteresDoc', 'criterecidessous', 'criteresAttribution2');
                        $xml .= self::baliseBinding('criteresQualite', 'true', 'booleanType');
                        $bindigEnonce = 'criteresQcritere';
                        $bindigPonderation = 'criteresQcriterePoids';
                        $criteresAttr = $objet->getAllCriteresAttributions();
                        if (is_array($criteresAttr) && count($criteresAttr)) {
                            foreach ($criteresAttr as $unCritere) {
                                if ($unCritere instanceof CommonTCritereAttribution) {
                                    if ($j > 20) {
                                        break;
                                    }
                                    $increment = ($j < 10) ? '0'.$j : $j;
                                    $xml .= self::baliseBinding($bindigEnonce.$increment, $unCritere->getEnonce(), 'textType');
                                    $xml .= self::baliseBinding($bindigPonderation.$increment, round($unCritere->getPonderation()), 'longType');
                                    $criteresAttribution .= '* '.$unCritere->getEnonce();
                                    if ('layerDefinitionCriteres_2' == $oneRefCritere->getLibelle2() && round($unCritere->getPonderation())) {
                                        $criteresAttribution .= '. '.Prado::localize('TEXT_PONDERATION').' : '.$unCritere->getPonderation().'%';
                                    }
                                    $criteresAttribution .= PHP_EOL;
                                    $sousCriteres = $unCritere->getSousCriteres();
                                    if (is_array($sousCriteres) && count($sousCriteres)) {
                                        foreach ($sousCriteres as $sousCritere) {
                                            if ($sousCritere instanceof CommonTSousCritereAttribution) {
                                                $criteresAttribution .= ' - '.$sousCritere->getEnonce();
                                                if ('layerDefinitionCriteres_2' == $oneRefCritere->getLibelle2() && round($sousCritere->getPonderation())) {
                                                    $criteresAttribution .= '. '.Prado::localize('TEXT_PONDERATION').' : '.$sousCritere->getPonderation().'%';
                                                }
                                                $criteresAttribution .= PHP_EOL;
                                            }
                                        }
                                    }
                                    ++$j;
                                }
                            }
                        }
                        $xml .= self::baliseBinding('criteresAttribution', $criteresAttribution, 'textType');
                    } else {
                        $xml .= self::baliseBinding('criteresAttribution', $oneRefCritere->getLibelleValeurReferentiel(), 'textType');
                        if ('layerDefinitionVide' == $oneRefCritere->getLibelle2()) {//Critère unique du prix le plus bas
                            $xml .= self::baliseBindingReferentiel('criteresOUcriteresDoc', 'criterecidessous', 'criteresAttribution2');
                            $xml .= self::baliseBindingReferentiel('critereCoutPrix', '02_prix', 'CriteresEconomique');
                        } elseif ('layerDefinitionVide_2' == $oneRefCritere->getLibelle2()) {//Critères énoncés dans le cahier des charges
                            $xml .= self::baliseBindingReferentiel('criteresOUcriteresDoc', 'critereCDC', 'criteresAttribution2');
                        }
                    }
                }
            }
            $logger->info("Construction de l'XML d'attribution. XML : \n ".print_r($xml, true));

            return $xml;
        } catch (\Exception $e) {
            $logger->error("Erreur lors de la construction de l'xml des criteres d'attribution : Erreur = ".$e->getMessage()." \nTrace: ".$e->getTraceAsString());
        }
    }

    /**
     * permet de creer le xml update support a envoye à SUB_ANNONCE.
     *
     * @param int                 $idDossier    identifiant du dossier SUB_ANNONCE
     * @param int                 $idDispositif identifiant du dispositif SUB_ANNONCE
     * @param array               $listeSupport liste support
     * @param Atexo_LoggerManager $logger       log de trace
     *
     * @return string xml a envoye à SUB_ANNONCE
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2015
     */
    public static function getXmlUpdatSupport($idDossier, $idDispositif, $listeSupport, $logger = null)
    {
        if (!$logger) {
            $logger = Atexo_LoggerManager::getLogger('publicite');
        }
        try {
            $xml = '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>';
            $xml .= '<dossiercreation loginTiers="'.Atexo_Config::getParameter('IDENTIFIANT_ACCES_SUB_ANNONCE').'" idDispositif="'.$idDispositif.'" idDossier="'.$idDossier.'">';
            $xml .= '<dossier_datas>';

            if (is_array($listeSupport) && count($listeSupport)) {
                $xml .= self::baliseBinding('canal_type_concentrateur', implode('|', $listeSupport));
            }

            $xml .= '</dossier_datas>';
            $xml .= '</dossiercreation>';

            return $xml;
        } catch (Exception $e) {
            $logger->error('Erreur Atexo_FormulaireSub_AnnonceEchange::getXmlUpdatSupport : '.$e->getMessage().' '.$e->getTraceAsString());
        }
    }

    /**
     * Permet de recuperer les categories INSEE.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function recupererCategorieInsee()
    {
        $logger = Atexo_LoggerManager::getLogger('publicite');
        try {
            $organisme = Atexo_Organismes::retrieveOrganismeByAcronyme(Atexo_CurrentUser::getCurrentOrganism());
            if ($organisme instanceof CommonOrganisme) {
                $categoriesInseeQuery = new CommonCategorieINSEEQuery();

                return $categoriesInseeQuery->recupererCategorieInsee($organisme->getCategorieInsee());
            }
        } catch (\Exception $e) {
            $logger->error('Erreur recuperation referentiel Categories INSEE : Erreur = '.$e->getMessage());
        }
    }

    /**
     * permet de creer la balise binding du xml a envoye à SUB_ANNONCE.
     *
     * @param string $nom   nom du binding
     * @param string $value value du binding
     * @param string $type  type de la valeur du bindings
     *
     * @return string la balise binding du xml a envoye à SUB_ANNONCE
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2015
     */
    public static function baliseBinding($nom, $value, $type = 'stringType')
    {
        return '<data nom="'.Atexo_Util::toUtf8($nom).'" type="'.Atexo_Util::toUtf8($type).'" ><valeur><![CDATA['.Atexo_Util::toUtf8($value).']]></valeur></data>';
    }

    /**
     * permet de creer la balise binding referentiel du xml a envoye à SUB_ANNONCE.
     *
     * @param string $nom             nom du binding
     * @param string $code            code  du binding
     * @param string $codeReferentiel code Referentiel de la valeur du bindings
     *
     * @return string la balise binding type referentiel du xml a envoye à SUB_ANNONCE
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2015
     */
    public static function baliseBindingReferentiel($nom, $code, $codeReferentiel)
    {
        if ('' != $code) {
            $return = '<data nom="'.Atexo_Util::toUtf8($nom).'" type="referentielType">
                <referentiel >
                    <code>'.Atexo_Util::toUtf8($code).'</code>
                    <codeReferentiel>'.Atexo_Util::toUtf8($codeReferentiel).'</codeReferentiel>
                </referentiel>
            </data>';
        } else {
            $return = '<data nom="'.Atexo_Util::toUtf8($nom).'" type="referentielType"/>';
        }

        return $return;
    }

    /**
     * permet de creer la balise binding type adresse du xml a envoye à SUB_ANNONCE.
     *
     * @param string $nom        nom du binding
     * @param string $nomVoix    nom voix
     * @param string $codePostal code postal
     * @param string $nomCommune nom Commune
     *
     * @return string la balise binding type adresse du xml a envoye à SUB_ANNONCE
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2016
     */
    public static function baliseBindingAdresse($nom, $nomVoix, $codePostal, $nomCommune, $pays = false)
    {
        $return = '<data nom="'.Atexo_Util::toUtf8($nom).'" type="adresseType">
        <adresse  typeAdresse="etrangere" numeroVoie="" complementAdresse="" complementDestinataire="" cedex="" nomVoie="'.Atexo_Util::toUtf8($nomVoix).'" nomCommune="'.Atexo_Util::toUtf8($nomCommune).'" codePostal="'.Atexo_Util::toUtf8($codePostal).'"
        >';

        if (!$pays) {
            $pays = Atexo_Config::getParameter('DENOMINATION1_GEON2_FRANCE');
        }
        $return .= '<pays>  <libelle>'.$pays.'</libelle> <code>'.$pays.'</code>  <codeReferentiel>Pays_Adresse</codeReferentiel> </pays>';
        $return .= '</adresse></data>';

        return $return;
    }

    /**
     * permet de creer la balise binding referentiel item du xml a envoye à SUB_ANNONCE.
     *
     * @param string $nom   nom du binding
     * @param string $codes ensemble de code  du binding
     *
     * @return string la balise binding type referentiel item du xml a envoye à SUB_ANNONCE
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2015
     */
    public static function baliseBindingItemReferentiel($nom, $codes, $codeReferentiel)
    {
        $balise = '';
        if (is_array($codes)) {
            foreach ($codes as $code) {
                if (!empty($code)) {
                    $balise .= '
                 <item>
                    <referentielValeur>
                        <code>'.trim(Atexo_Util::toUtf8($code)).'</code>
                        <codeReferentiel>'.Atexo_Util::toUtf8($codeReferentiel).'</codeReferentiel>
                    </referentielValeur>
                </item>';
                }
            }
            if ($balise) {
                $balise = '<data nom="'.Atexo_Util::toUtf8($nom).'" type="referentielItemType">'.$balise.' </data>';
            }
        }

        return $balise;
    }

    /**
     * Appelle le ws de mise a jour des annonces via concentrateur
     * Retourne les donnees de mise a jour en json.
     *
     * @param string $listeIds : liste des id des annonces
     *
     * @return false|string : contenu du retour du ws en json si valide, false sinon
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function recupererDonneesMajConcentrateur($listeIds): false|string
    {
        $url = null;
        try {
            $url = Atexo_Config::getParameter('URL_CONCENTRATEUR_MAJ_LISTE_ANNONCES')."?listIdAnnonce=$listeIds";
            (new Atexo_Publicite_AnnonceSub())->loggerInfosSuiviAnnonces("Url ws = $url");

            $configProxy = self::getConfigProxyForGuzzle(Atexo_Util::getSfContainer()) ?: [];
            $configProxy['timeout'] = 120;
            $client = new Client($configProxy);

            $reponseAPI = $client->Get($url);
            $contentAPI = $reponseAPI->getBody();
            $statut = $reponseAPI->getStatusCode();
            (new Atexo_Publicite_AnnonceSub())->loggerInfosSuiviAnnonces("Statut retour ws = $statut");
            (new Atexo_Publicite_AnnonceSub())->loggerInfosSuiviAnnonces("Contenu retour ws = $contentAPI");

            if (true === self::validerRetourWs($statut)) {
                return $contentAPI;
            }

            return false;
        } catch (Exception $e) {
            (new Atexo_Publicite_AnnonceSub())->loggerErreursSuiviAnnonces("Erreur survenue lors de la recuperation des donnees de mise a jour des annonces via concentrateur : Liste des Ids = '$listeIds' , Url = $url , Erreur = ".$e->getMessage()." \nMethode : Atexo_FormulaireSub_AnnonceEchange::recupererDonneesMajConcentrateur");
        }
    }

    /**
     * Permet de valider le retour du ws de mise a jour des annonces via le concentrateur.
     *
     * @param string $statut : code statut du retour du ws
     *
     * @return bool : true si resultat ws valide, false sinon
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function validerRetourWs($statut)
    {
        try {
            $resultatValide = false;
            switch ($statut) {
                case 200:
                    $resultatValide = true;
                    (new Atexo_Publicite_AnnonceSub())->loggerErreursSuiviAnnonces("Retour ws VALIDE , Code statut = $statut");
                    break;
                case 401:
                    (new Atexo_Publicite_AnnonceSub())->loggerErreursSuiviAnnonces("Retour ws NON VALIDE , Code statut = $statut , Erreur statut = Unauthorized");
                    break;
                case 403:
                    (new Atexo_Publicite_AnnonceSub())->loggerErreursSuiviAnnonces("Retour ws NON VALIDE , Code statut = $statut , Erreur statut = Forbidden");
                    break;
                case 404:
                    (new Atexo_Publicite_AnnonceSub())->loggerErreursSuiviAnnonces("Retour ws NON VALIDE , Code statut = $statut , Erreur statut = Not Found");
                    break;
                default:
                    (new Atexo_Publicite_AnnonceSub())->loggerErreursSuiviAnnonces("Retour ws NON VALIDE , Code statut = $statut , Erreur statut = INCONNUE");
            }

            return $resultatValide;
        } catch (Exception $e) {
            (new Atexo_Publicite_AnnonceSub())->loggerErreursSuiviAnnonces('Erreur validation du retour du ws de mise a jour des annonces via concentrateur : '.$e->getMessage()." \nMethode : Atexo_FormulaireSub_AnnonceEchange::validerRetourWs");
        }
    }

    /**
     * permet de recupere les valeur du xml  SUB_ANNONCE.
     *
     * @param CommonConsultation   $consultation consultation depuis on va creer le dossier SUB_ANNONCE
     * @param CommonAcheteurPublic $compteBoamp  compte BOAMP
     * @param string xml SUB_ANNONCE
     * @param PropelConnexion     $connexion
     * @param Atexo_LoggerManager $logger    log de trace
     *
     * @return void
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2015
     */
    public static function updateFromXmlSUB(&$consultation, &$compteBoamp, $xml, $connexion, $logger = null)
    {
        try {
            if (!$logger) {
                $logger = Atexo_LoggerManager::getLogger('publicite');
            }
            $logger->info('Début de pacours des bidings fct getArrayBaliseBinding');
            $bindings = self::getArrayBaliseBinding($xml);
            $logger->info('Fin de pacours des bidings fct getArrayBaliseBinding');
            self::updateConsultationFromXmlSUB($consultation, $bindings, $connexion, $logger);
            // self::updateCompteBoampFromXmlSUB($compteBoamp, $bindings, $connexion, $logger);
        } catch (Exception $e) {
            $logger->error('Erreur Atexo_FormulaireSub_AnnonceEchange::updateFromXmlSUB : '.$e->getMessage().' '.$e->getTraceAsString());
        }
    }

    /**
     * permet de recupere les valeur du xml  SUB_ANNONCE.
     *
     * @param CommonAcheteurPublic $compteBoamp compte BOAMP
     * @param array                $bindings    balise des bindings
     * @param PropelConnexion      $connexion
     * @param Atexo_LoggerManager  $logger      log de trace
     *
     * @return void
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2015
     */
    public static function updateCompteBoampFromXmlSUB(&$compteBoamp, $bindings, $connexion, $logger = null)
    {
        $idRef = null;
        $urlA = null;
        try {
            if (!$logger) {
                $logger = Atexo_LoggerManager::getLogger('publicite');
            }
            if ($compteBoamp instanceof CommonAcheteurPublic) {
                $logger->info('Début traitement bidings compte BOAMP');
                if (is_array($bindings) && count($bindings)) {
                    $synchroCompte = null;
                    if (isset($bindings['synchro_boamp'])) {
                        $synchroCompte = self::getValueFromBaliseBindingReferentiel($bindings['synchro_boamp']);
                    }
                    if ('01' == $synchroCompte) {
                        if (isset($bindings['organisme_classificationOrganisme'])) {
                            $typeOrgSub = self::getValueFromBaliseBindingReferentiel($bindings['organisme_classificationOrganisme']);
                            $typeOrganisme = (new Atexo_Referentiel())->retrieveValeurReferentielByValeurSub(Atexo_Config::getParameter('REFERENCE_TYPE_ORGANISME'), $compteBoamp->getOrganisme(), $typeOrgSub);
                            if ($typeOrganisme instanceof CommonValeurReferentielOrg) {
                                $compteBoamp->getTypeOrg();
                                $logger->info('traitement bidings compte BOAMP '.$compteBoamp->getId().' (type Organisme): organisme_classificationOrganisme = '.$typeOrganisme->getId());
                            }
                        }
                        if (isset($bindings['boamp_mel'])) {
                            $mail = self::getValueFromBaliseBinding($bindings['boamp_mel']);
                            $compteBoamp->setBoampMail($mail);
                            $logger->info('traitement bidings compte BOAMP '.$compteBoamp->getId().' : boamp_mel = '.$mail);
                        }
                        $prenom = '';
                        if (isset($bindings['organisme_PersonnePhysique_pren'])) {
                            $prenom = self::getValueFromBaliseBinding($bindings['organisme_PersonnePhysique_pren']);
                            $logger->info('traitement bidings compte BOAMP '.$compteBoamp->getId().' : organisme_PersonnePhysique_pren = '.$prenom);
                        }
                        $nom = '';
                        if (isset($bindings['organisme_PersonnePhysique_nom'])) {
                            $nom = self::getValueFromBaliseBinding($bindings['organisme_PersonnePhysique_nom']);
                            $logger->info('traitement bidings compte BOAMP '.$compteBoamp->getId().' : organisme_PersonnePhysique_nom = '.$nom);
                        }
                        $compteBoamp->setPrm(trim($prenom.' '.$nom));

                        if (isset($bindings['organisme_acheteurPublic'])) {
                            $organisme = self::getValueFromBaliseBinding($bindings['organisme_acheteurPublic']);
                            $compteBoamp->setDenomination($organisme);
                            $logger->info('traitement bidings compte BOAMP '.$compteBoamp->getId().' : organisme_acheteurPublic = '.$organisme);
                        }
                        if (isset($bindings['organisme_coord_tel'])) {
                            $tel = self::getValueFromBaliseBinding($bindings['organisme_coord_tel']);
                            $compteBoamp->setTelephone($tel);
                            $logger->info('traitement bidings compte BOAMP '.$compteBoamp->getId().' : organisme_coord_tel = '.$tel);
                        }
                        if (isset($bindings['organisme_coord_fax'])) {
                            $fax = self::getValueFromBaliseBinding($bindings['organisme_coord_fax']);
                            $compteBoamp->setFax($fax);
                            $logger->info('traitement bidings compte BOAMP '.$compteBoamp->getId().' : organisme_coord_fax = '.$fax);
                        }
                        if (isset($bindings['organisme_coord_url'])) {
                            $url = self::getValueFromBaliseBinding($bindings['organisme_coord_url']);
                            $compteBoamp->setUrl($url);
                            $logger->info('traitement bidings compte BOAMP '.$compteBoamp->getId().' : organisme_coord_url = '.$url);
                        }
                        if (isset($bindings['organisme_urlProfilAcheteur'])) {
                            $urlA = self::getValueFromBaliseBinding($bindings['organisme_urlProfilAcheteur']);
                            $compteBoamp->setUrlAcheteur($urlA);
                            $logger->info('traitement bidings compte BOAMP '.$compteBoamp->getId().' : organisme_urlProfilAcheteur = '.$urlA);
                        }
                        if (isset($bindings['organisme_activitesOrganisme_activiteDefiniePour'])) {
                            $activitesSub = self::getValueFromBaliseBindingReferentiel($bindings['organisme_activitesOrganisme_activiteDefiniePour']);
                            $bindindTypeActivite = null;
                            if ('pouvoirAdjudicateur' == $activitesSub) {
                                $idRef = Atexo_Config::getParameter('REFERENTIEL_PRINCIPAL_ACTIVITES_POUVOIR');
                                $bindindTypeActivite = 'organisme_activitesOrganisme_activites';
                            } elseif ('entiteAdjudicatrice' == $activitesSub) {
                                $idRef = Atexo_Config::getParameter('REFERENTIEL_PRINCIPAL_ACTIVITES_ENTITE');
                                $bindindTypeActivite = 'organisme_activitesOrganisme_activitesSecteursSpeciaux';
                            }
                            $codesActivite = [];
                            if (isset($bindings[$bindindTypeActivite])) {
                                $listActivitesSub = self::getValueFromBaliseBindingItemReferentiel($bindings[$bindindTypeActivite]);
                                $liste = (new Atexo_CommonReferentiel())->retrieveValeurReferentielByValeurSub($idRef, $listActivitesSub);
                                if (is_array($liste)) {
                                    foreach ($liste as $ref) {
                                        if ($ref instanceof CommonValeurReferentiel) {
                                            $codesActivite[] = $idRef.'_'.$ref->getId();
                                        }
                                    }
                                }
                            }
                            $compteBoamp->setTypePouvoirActivite(implode('#', $codesActivite));
                            $logger->info('traitement bidings compte BOAMP '.$compteBoamp->getId().' : organisme_urlProfilAcheteur = '.$urlA);
                        }
                        if (isset($bindings['adressesComplt_admOuTech_adm'])) {
                            $cas1 = self::getValueFromBaliseBinding($bindings['adressesComplt_admOuTech_adm']);
                            if ('true' == $cas1) {
                                self::updateRenseignementBoamp($compteBoamp->getId(), Atexo_Config::getParameter('ADRESSE_COMPLEMENTAIRES_1'), $bindings, 'adressesComplt_admOuTech_adm', $connexion);
                                $logger->info('traitement bidings compte BOAMP '.$compteBoamp->getId().' : adressesComplt_admOuTech_adm ');
                            }
                        }

                        if (isset($bindings['adressesComplt_admOuTech_tech'])) {
                            $cas1 = self::getValueFromBaliseBinding($bindings['adressesComplt_admOuTech_tech']);
                            if ('true' == $cas1) {
                                self::updateRenseignementBoamp($compteBoamp->getId(), Atexo_Config::getParameter('ADRESSE_COMPLEMENTAIRES_2'), $bindings, 'adressesComplt_admOuTech_tech', $connexion);
                                $logger->info('traitement bidings compte BOAMP '.$compteBoamp->getId().' : adressesComplt_admOuTech_tech ');
                            }
                        }

                        if (isset($bindings['adressesComplt_document'])) {
                            $cas1 = self::getValueFromBaliseBinding($bindings['adressesComplt_document']);
                            if ('true' == $cas1) {
                                self::updateRenseignementBoamp($compteBoamp->getId(), Atexo_Config::getParameter('ADRESSE_COMPLEMENTAIRES_3'), $bindings, 'adressesComplt_document', $connexion);
                                $logger->info('traitement bidings compte BOAMP '.$compteBoamp->getId().' : adressesComplt_document ');
                            }
                        }
                        if (isset($bindings['adressesComplt_envoi'])) {
                            $cas1 = self::getValueFromBaliseBinding($bindings['adressesComplt_envoi']);
                            if ('true' == $cas1) {
                                self::updateRenseignementBoamp($compteBoamp->getId(), Atexo_Config::getParameter('ADRESSE_COMPLEMENTAIRES_4'), $bindings, 'adressesComplt_envoi', $connexion);
                                $logger->info('traitement bidings compte BOAMP '.$compteBoamp->getId().' : adressesComplt_envoi ');
                            }
                        }
                        if (isset($bindings['adressesComplt_rensComplementaires'])) {
                            $cas1 = self::getValueFromBaliseBinding($bindings['adressesComplt_rensComplementaires']);
                            if ('true' == $cas1) {
                                self::updateRenseignementBoamp($compteBoamp->getId(), Atexo_Config::getParameter('ADRESSE_COMPLEMENTAIRES_5'), $bindings, 'adressesComplt_rensComplementaires', $connexion);
                                $logger->info('traitement bidings compte BOAMP '.$compteBoamp->getId().' : adressesComplt_rensComplementaires ');
                            }
                        }
                        self::updateRenseignementBoamp($compteBoamp->getId(), Atexo_Config::getParameter('PROCEDURE_RECOURS_1'), $bindings, 'procedureRecours_instanceChargeeRecours', $connexion);
                        $logger->info('traitement bidings compte BOAMP '.$compteBoamp->getId().' : procedureRecours_instanceChargeeRecours ');

                        self::updateRenseignementBoamp($compteBoamp->getId(), Atexo_Config::getParameter('PROCEDURE_RECOURS_2'), $bindings, 'procedureRecours_instanceChargeemediation', $connexion);
                        $logger->info('traitement bidings compte BOAMP '.$compteBoamp->getId().' : procedureRecours_instanceChargeemediation ');

                        if (isset($bindings['procedureRecours_introductionRecoursPrecisions'])) {
                            $value = self::getValueFromBaliseBinding($bindings['procedureRecours_introductionRecoursPrecisions']);
                            $renseignement = (new Atexo_Publicite_RenseignementsBoamp())->retrieveOneRenseignementBoamp($compteBoamp->getId(), Atexo_Config::getParameter('PROCEDURE_RECOURS_3'));
                            if (!$renseignement instanceof CommonRenseignementsBoamp) {
                                $renseignement = new CommonRenseignementsBoamp();
                                $renseignement->setIdCompte($compteBoamp->getId());
                                $renseignement->setIdType(Atexo_Config::getParameter('PROCEDURE_RECOURS_3'));
                            }
                            $renseignement->setOrganeChargeProcedure($value);
                            $renseignement->save($connexion);
                            $logger->info('traitement bidings compte BOAMP '.$compteBoamp->getId().' : procedureRecours_introductionRecoursPrecisions = '.$value);
                        }

                        if (isset($bindings['procedureRecours_introductionRecoursPrecisions_OU_introductionRecoursRenseignements'])) {
                            $cas1 = self::getValueFromBaliseBindingReferentiel($bindings['procedureRecours_introductionRecoursPrecisions_OU_introductionRecoursRenseignements']);
                            if ('1' == $cas1) {
                                self::updateRenseignementBoamp($compteBoamp->getId(), Atexo_Config::getParameter('PROCEDURE_RECOURS_4'), $bindings, 'procedureRecours_introductionRecoursRenseignements', $connexion);
                                $logger->info('traitement bidings compte BOAMP '.$compteBoamp->getId().' : procedureRecours_introductionRecoursRenseignements ');
                            }
                        }
                    }
                    $synchroFacture = null;
                    if (isset($bindings['synchroBOAMP2'])) {
                        $synchroFacture = self::getValueFromBaliseBindingReferentiel($bindings['synchroBOAMP2']);
                    }
                    if ('01' == $synchroFacture) {
                        if (isset($bindings['facturation_numBonCommande'])) {
                            $numCommande = self::getValueFromBaliseBinding($bindings['facturation_numBonCommande']);
                            $compteBoamp->setFactureNumero($numCommande);
                            $logger->info('traitement bidings compte BOAMP '.$compteBoamp->getId().' : facturation_numBonCommande = '.$numCommande);
                        }
                        if (isset($bindings['facturation_libelle_denomination'])) {
                            $libelleFacture = self::getValueFromBaliseBinding($bindings['facturation_libelle_denomination']);
                            $compteBoamp->setFactureDenomination($libelleFacture);
                            $logger->info('traitement bidings compte BOAMP '.$compteBoamp->getId().' : facturation_libelle_denomination = '.$libelleFacture);
                        }
                    }
                }
                $logger->info('Fin traitement bidings compte BOAMP');
            }
        } catch (Exception $e) {
            $logger->error('Erreur Atexo_FormulaireSub_AnnonceEchange::updateCompteBoampFromXmlSUB : '.$e->getMessage().' '.$e->getTraceAsString());
        }
    }

    public static function updateRenseignementBoamp($idCompte, $idType, $bindings, $prefixLibelleBinding, $connexion)
    {
        $update = false;
        $renseignement = (new Atexo_Publicite_RenseignementsBoamp())->retrieveOneRenseignementBoamp($idCompte, $idType);
        if (!$renseignement instanceof CommonRenseignementsBoamp) {
            $renseignement = new CommonRenseignementsBoamp();
            $renseignement->setIdCompte($idCompte);
            $renseignement->setIdType($idType);
        }
        if (is_array($bindings) && $prefixLibelleBinding) {
            $correspondant = '';
            if (isset($bindings[$prefixLibelleBinding.'_PersonnePhysique_pren'])) {
                $correspondant .= self::getValueFromBaliseBinding($bindings[$prefixLibelleBinding.'_PersonnePhysique_pren']).' ';
            }
            if (isset($bindings[$prefixLibelleBinding.'_PersonnePhysique_nom'])) {
                $correspondant .= self::getValueFromBaliseBinding($bindings[$prefixLibelleBinding.'_PersonnePhysique_nom']);
            }
            if (trim($correspondant)) {
                $renseignement->setCorrespondant(trim($correspondant));
                $update = true;
            }

            if (isset($bindings[$prefixLibelleBinding.'_PersonneMorale'])) {
                $value = self::getValueFromBaliseBinding($bindings[$prefixLibelleBinding.'_PersonneMorale']);
                if ($value) {
                    $renseignement->setOrganisme($value);
                    $update = true;
                }
            }
            if (isset($bindings[$prefixLibelleBinding.'_coord_tel'])) {
                $value = self::getValueFromBaliseBinding($bindings[$prefixLibelleBinding.'_coord_tel']);
                if ($value) {
                    $renseignement->setTelephone($value);
                    $update = true;
                }
            }
            if (isset($bindings[$prefixLibelleBinding.'_coord_fax'])) {
                $value = self::getValueFromBaliseBinding($bindings[$prefixLibelleBinding.'_coord_fax']);
                if ($value) {
                    $renseignement->setFax($value);
                    $update = true;
                }
            }
            if (isset($bindings[$prefixLibelleBinding.'_coord_mel'])) {
                $value = self::getValueFromBaliseBinding($bindings[$prefixLibelleBinding.'_coord_mel']);
                if ($value) {
                    $renseignement->setMail($value);
                    $update = true;
                }
            }
            if (isset($bindings[$prefixLibelleBinding.'_coord_url'])) {
                $value = self::getValueFromBaliseBinding($bindings[$prefixLibelleBinding.'_coord_url']);
                if ($value) {
                    $renseignement->setUrl(self::getValueFromBaliseBinding($value));
                    $update = true;
                }
            }
        }
        if ($update) {
            $renseignement->save($connexion);
        }
    }

    /**
     * permet de recupere les valeur consultation du xml  SUB_ANNONCE.
     *
     * @param CommonConsultation  $consultation consultation depuis on va creer le dossier SUB_ANNONCE
     * @param array               $bindings     balise des bindings
     * @param PropelConnexion     $connexion
     * @param Atexo_LoggerManager $logger       log de trace
     *
     * @return void
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2015
     */
    public static function updateConsultationFromXmlSUB(&$consultation, $bindings, $connexion, $logger = null)
    {
        $valeurAlloti = null;
        try {
            if (!$logger) {
                $logger = Atexo_LoggerManager::getLogger('publicite');
            }
            if ($consultation instanceof CommonConsultation) {
                $donnesComplaimentaireCons = $consultation->getDonneComplementaire();
                if (!$donnesComplaimentaireCons instanceof CommonTDonneeComplementaire) {
                    $donnesComplaimentaireCons = new CommonTDonneeComplementaire();
                }
                $logger->info('Début traitement bidings consultation');
                if (is_array($bindings) && count($bindings)) {
                    if (isset($bindings['natureMarche'])) {
                        $codeCategorie = self::getValueFromBaliseBindingReferentiel($bindings['natureMarche']);
                        $codeNatureMarche = null;
                        $natureMarche = false;
                        switch ($codeCategorie) {
                            case '01_travaux':
                                $natureMarche = Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX');
                                if (isset($bindings['natureMarche_travaux'])) {
                                    $codeNatureMarche = self::getValueFromBaliseBindingReferentiel($bindings['natureMarche_travaux']);
                                }
                                break;
                            case '02_fournitures':
                                $natureMarche = Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES');
                                if (isset($bindings['natureMarche_fournitures'])) {
                                    $codeNatureMarche = self::getValueFromBaliseBindingReferentiel($bindings['natureMarche_fournitures']);
                                }
                                break;
                            case '03_services':
                                $natureMarche = Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES');
                                if (isset($bindings['natureMarche_serviceCategorie'])) {
                                    $codeNatureMarche = self::getValueFromBaliseBindingReferentiel($bindings['natureMarche_serviceCategorie']);
                                }
                                break;
                        }
                        if ($natureMarche) {
                            $consultation->setCategorie($natureMarche);
                            $logger->info('traitement bidings consultation '.$consultation->getId().' : categorie  = '.$natureMarche);
                        }
                    }
                    if (isset($bindings['description_titreMarche'])) {
                        $intitule = self::getValueFromBaliseBinding($bindings['description_titreMarche']);
                        $consultation->setIntituleTraduit(null, $intitule);
                        $logger->info('traitement bidings consultation '.$consultation->getId().' : description_titreMarche = '.$intitule);
                    }
                    if (isset($bindings['description_objet'])) {
                        $objet = self::getValueFromBaliseBinding($bindings['description_objet']);
                        $consultation->setObjetTraduit(null, $objet);
                        $logger->info('traitement bidings consultation '.$consultation->getId().' : description_objet = '.$objet);
                    }

                    if (isset($bindings['description_CPV_objetPrincipal_classPrincipale'])) {
                        $cpvP = self::getValueFromBaliseBindingReferentiel($bindings['description_CPV_objetPrincipal_classPrincipale']);
                        $consultation->setCodeCpv1($cpvP);
                        $logger->info('traitement bidings consultation '.$consultation->getId().' : cpv1 = '.$cpvP);
                    }

                    $cpvs = [];
                    for ($i = 1; $i <= 4; ++$i) {
                        if (isset($bindings['description_CPV_objetComplementaire'.$i.'_classPrincipale'])) {
                            $cpvs[$i] = self::getValueFromBaliseBindingReferentiel($bindings['description_CPV_objetComplementaire'.$i.'_classPrincipale']);
                            $logger->info('traitement bidings consultation '.$consultation->getId().': description_CPV_objetComplementaire'.$i.'classPrincipale = '.$cpvs[$i]);
                        }
                    }
                    $consultation->setCodeCpv2(implode('#', $cpvs));
                    $logger->info('traitement bidings consultation '.$consultation->getId().': cpv2 = '.$consultation->getCodeCpv2());

                    if (isset($bindings['motClef'])) {
                        $motsClesSub = self::getValueFromBaliseBindingItemReferentiel($bindings['motClef']);
                        if (is_array($motsClesSub)) {
                            $motsClesQuery = new CommonTReferentielMotsClesQuery();
                            $motsClesQuery->filterByValeurSub($motsClesSub, Criteria::IN);
                            $motsClesO = (array) $motsClesQuery->find($connexion);
                            $motsCles = [];
                            foreach ($motsClesO as $mc) {
                                $motsCles[] = $mc->getId();
                            }
                            $donnesComplaimentaireCons->setMotsCles(implode(',', $motsCles));
                            $logger->info('traitement bidings consultation '.$consultation->getId().' : motClef = '.$donnesComplaimentaireCons->getMotsCles());
                        }
                    }
                    if (isset($bindings['caracteristiques_estimationValeur_valeur'])) {
                        $montant = self::getValueFromBaliseBinding($bindings['caracteristiques_estimationValeur_valeur']);
                        if ($montant) {
                            $donnesComplaimentaireCons->setMontantMarche($montant);
                            $logger->info('traitement bidings consultation '.$consultation->getId().' : caracteristiques_estimationValeur_valeur = '.$montant);
                        }
                    }

                    if (isset($bindings['marcheUnique_OU_lots'])) {
                        $allotissementSub = self::getValueFromBaliseBindingReferentiel($bindings['marcheUnique_OU_lots']);
                        if ('02_lots' == $allotissementSub) {
                            $valeurAlloti = 1;
                        } elseif ('01_marcheUnique' == $allotissementSub) {
                            $valeurAlloti = 0;
                        }
                        if (!is_null($valeurAlloti)) {
                            $consultation->setAlloti($valeurAlloti);
                            $logger->info('traitement bidings consultation '.$consultation->getId().' : marcheUnique_OU_lots = '.$valeurAlloti);
                        }
                    }

                    if (isset($bindings['criteresSE_criteresSociaux'])) {
                        $clauseSocialSub = self::getValueFromBaliseBindingReferentiel($bindings['criteresSE_criteresSociaux']);
                        if ($clauseSocialSub) {
                            $consultation->setClauseSociale($clauseSocialSub);
                            $logger->info('traitement bidings consultation '.$consultation->getId().' : criteresSE_criteresSociaux = '.$clauseSocialSub);
                        }
                    }
                    if (isset($bindings['criteresSE_criteresEnvironnementaux'])) {
                        $clauseEnvSub = self::getValueFromBaliseBindingReferentiel($bindings['criteresSE_criteresEnvironnementaux']);
                        if ($clauseEnvSub) {
                            $consultation->setClauseSociale($clauseEnvSub);
                            $logger->info('traitement bidings consultation '.$consultation->getId().' : criteresSE_criteresEnvironnementaux = '.$clauseEnvSub);
                        }
                    }
                    /* $dateFin = false;
                     if(isset($bindings["delais_receptCandidatures"])){
                         $valeurDateFin = self::getValueFromBaliseBinding($bindings["delais_receptCandidatures"]);
                         if($valeurDateFin){
                             $dateFin = Atexo_Util::frnDateTime2iso($valeurDateFin);
                             $logger->info("traitement bidings consultation " . $consultation->getId() . "(date fin) : delais_receptCandidatures = " . $dateFin);
                         }
                     }
                     if(!$dateFin && isset($bindings["delais_receptionsOffres"])){
                         $valeurDateFin = self::getValueFromBaliseBinding($bindings["delais_receptionsOffres"]);
                         if($valeurDateFin){
                             $dateFin = Atexo_Util::frnDateTime2iso($valeurDateFin);
                             $logger->info("traitement bidings consultation " . $consultation->getId() . "(date fin) : delais_receptionsOffres = " . $dateFin);
                         }
                     }
                     if($dateFin) {
                         $consultation->setDatefin($dateFin);
                     }*/
                    $logger->info('debut traitement bidings consultation '.$consultation->getId().' criteres attributions');
                    self::updateCriteresAttribution($consultation, $bindings, $connexion);
                    $logger->info('fin traitement bidings consultation '.$consultation->getId().' criteres attributions');

                    $donnesComplaimentaireCons->save($connexion);
                    $consultation->setIdDonneeComplementaire($donnesComplaimentaireCons->getIdDonneeComplementaire());
                    $logger->info('Fin traitement bidings consultation');
                }
            } else {
                $logger->error("Erreur Atexo_FormulaireSub_AnnonceEchange::updateConsultationFromXmlSUB l'objet envoyé n'est pas une consultation ".print_r($consultation, true));
            }
        } catch (Exception $e) {
            $logger->error('Erreur Atexo_FormulaireSub_AnnonceEchange::updateConsultationFromXmlSUB : '.$e->getMessage().' '.$e->getTraceAsString());
        }
    }

    /**
     * permet de recupperer la balise binding du xml SUB_ANNONCE.
     *
     * @param DOMNodeList $balise balise du binding
     *
     * @return mixed valeur de la balise binding du xml SUB_ANNONCE
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2015
     */
    public static function getValueFromBaliseBinding($balise)
    {
        try {
            $values = $balise->getElementsByTagName('valeur');
            if (!empty($values)) {
                foreach ($values as $value) {
                    return (new Atexo_Util())->toHttpEncoding(trim($value->nodeValue));
                }
            }

            return '';
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * permet de recupperer la balise binding referentiel du xml SUB_ANNONCE.
     *
     * @param DOMNodeList $balise balise du binding
     *
     * @return mixed valeur de la balise du binding du xml SUB_ANNONCE
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2015
     */
    public static function getValueFromBaliseBindingReferentiel($balise)
    {
        try {
            $refs = $balise->getElementsByTagName('referentiel');
            if (!empty($refs)) {
                foreach ($refs as $ref) {
                    $codeRef = $ref->getElementsByTagName('code');
                    if (!empty($codeRef)) {
                        foreach ($codeRef as $code) {
                            return (new Atexo_Util())->toHttpEncoding(trim($code->nodeValue));
                        }
                    }
                }
            }

            return false;
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * permet de recupperer la balise binding item referentiel du xml SUB_ANNONCE.
     *
     * @param DOMNodeList $balise balise du binding
     *
     * @return mixed valeur de la balise du binding du xml SUB_ANNONCE
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2015
     */
    public static function getValueFromBaliseBindingItemReferentiel($balise)
    {
        try {
            $return = [];
            $itemsRef = $balise->getElementsByTagName('item');
            if (!empty($itemsRef)) {
                foreach ($itemsRef as $item) {
                    $refs = $item->getElementsByTagName('referentielValeur');
                    if (!empty($refs)) {
                        foreach ($refs as $ref) {
                            $codeRef = $ref->getElementsByTagName('code');
                            if (!empty($codeRef)) {
                                foreach ($codeRef as $code) {
                                    $return[] = (new Atexo_Util())->toHttpEncoding(trim($code->nodeValue));
                                }
                            }
                        }
                    }
                }
            }

            return $return;
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * permet de parcourir des balises binding du xml SUB_ANNONCE.
     *
     * @param string $xml xml
     *
     * @return array des balises binding du xml SUB_ANNONCE
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2015
     */
    public static function getArrayBaliseBinding($xml)
    {
        try {
            $domDoc = new DomDocument();
            $domDoc->loadXML($xml);
            $reponsegets = $domDoc->getElementsByTagName('reponseget');
            $dataBindings = [];
            if ($reponsegets instanceof \DOMNodeList) {
                foreach ($reponsegets as $reponseget) {
                    if ('OK' == (string) $reponseget->getAttribute('statut')) {
                        $dossierDatas = $reponseget->getElementsByTagName('dossier_datas');
                        foreach ($dossierDatas as $dossierData) {
                            $bindings = $dossierData->getElementsByTagName('data');
                            foreach ($bindings as $binding) {
                                $dataBindings[$binding->getAttribute('nom')] = $binding;
                            }
                        }
                    }
                }
            }

            return $dataBindings;
        } catch (Exception $e) {
            throw $e;
        }
    }

    /**
     * Permet de mettre à jour des criteres d'attribution.
     *
     * @param CommonConsultation $objet:    Objet consultation
     * @param array              $bindings  liste balise bindings
     * @param PropelConnexion    $connexion
     *
     * @return void
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function updateCriteresAttribution(&$objet, $bindings, $connexion)
    {
        $libelle2 = null;
        $logger = Atexo_LoggerManager::getLogger('publicite');
        try {
            if ($objet instanceof CommonConsultation) {
                $donneeComplementaire = $objet->getDonneComplementaire();
                if (!($donneeComplementaire instanceof CommonTDonneeComplementaire)) {
                    $donneeComplementaire = new CommonTDonneeComplementaire();
                }
                if ($donneeComplementaire instanceof CommonTDonneeComplementaire) {
                    (new Atexo_DonnesComplementaires())->deleteCritereAttribution($donneeComplementaire, $connexion);
                    if (is_array($bindings) && count($bindings)) {
                        if (isset($bindings['criteres'])) {
                            $valeurCritere = self::getValueFromBaliseBindingReferentiel($bindings['criteres']);
                            $arrayDonnesCriteres = [];
                            switch ($valeurCritere) {
                                case '01_critereCDC':
                                    $libelle2 = 'layerDefinitionVide_2';
                                    break;
                                case '02_critereLibre':
                                    $libelle2 = 'layerDefinitionCriteres_1';
                                    $i = 21;
                                    while ($i < 40) {
                                        if (isset($bindings['criteres_critere'.$i])) {
                                            $enonceCritere = self::getValueFromBaliseBinding($bindings['criteres_critere'.$i]);
                                            $arrayDonnesCriteres[$i]['enonce'] = $enonceCritere;
                                        }
                                        ++$i;
                                    }
                                    break;
                                case '03_criterePondere':
                                    $libelle2 = 'layerDefinitionCriteres_2';
                                    $i = 1;
                                    while ($i < 20) {
                                        $increment = ($i < 10) ? '0'.$i : $i;
                                        if (isset($bindings['criteres_critere'.$increment])) {
                                            $enonceCritere = self::getValueFromBaliseBinding($bindings['criteres_critere'.$increment]);
                                            $arrayDonnesCriteres[$increment]['enonce'] = $enonceCritere;
                                        }
                                        if (isset($bindings['criteres_criterePoids'.$increment])) {
                                            $ponderationCritere = self::getValueFromBaliseBinding($bindings['criteres_criterePoids'.$increment]);
                                            $arrayDonnesCriteres[$increment]['ponderation'] = $ponderationCritere;
                                        }
                                    }
                                    break;
                                case '04_criterePrix':
                                    $libelle2 = 'layerDefinitionVide';
                                    break;
                            }

                            $ref = (new Atexo_CommonReferentiel())->retrieveValeurReferentielByLibelle2(Atexo_Config::getParameter('REFERENTIEL_CRITERE_ATTRIBUTION'), $libelle2);
                            if ($ref instanceof CommonValeurReferentiel) {
                                $donneeComplementaire->setIdCritereAttribution($ref->getId());
                                $donneeComplementaire->save($connexion);
                                $objet->setIdDonneeComplementaire($donneeComplementaire->getIdDonneeComplementaire());
                                $logger->info('traitement bidings consultation '.$objet->getId().' criteres attributions'.print_r($arrayDonnesCriteres, true));
                                if (is_array($arrayDonnesCriteres) && count($arrayDonnesCriteres)) {
                                    foreach ($arrayDonnesCriteres as $donneCritere) {
                                        $critere = new CommonTCritereAttribution();
                                        if (isset($donneCritere['ponderation'])) {
                                            $critere->setPonderation($donneCritere['ponderation']);
                                        }
                                        $critere->setEnonce($donneCritere['enonce']);
                                        $critere->setIdDonneeComplementaire($donneeComplementaire->getIdDonneeComplementaire());
                                        $critere->save($connexion);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        } catch (\Exception $e) {
            $logger->error("Erreur lors de l'enregistrement des criteres d'attribution : Consultation_ID = ".$objet->getId().' , Organisme = '.$objet->getOrganisme()." \nErreur = ".$e->getMessage()." \nTrace: ".$e->getTraceAsString());
            throw $e;
        }
    }

    /**
     * permet de télécharger le pdf de l'annonce.
     *
     * @param int $idDossier id dossier SUB
     *
     * @return string Jeton d'authentification retourner par SUB
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2015
     */
    public static function getPdfAnnonce($idDossier)
    {
        $logger = Atexo_LoggerManager::getLogger('publicite');
        $uri = Atexo_Config::getParameter('URL_SUB_ANNONCE_REST_DOWNLOAD_PDF_ANNONCE').'?nomdocumentmodele='.Atexo_Config::getParameter('NOM_PDF_ANNONCE').'&iddossier='.$idDossier;
        $logger->info('Début appelle WS download pdf URI : '.$uri);
        $xmlResponse = self::sendFluxASUB(Atexo_Config::getParameter('LOGIN_APPLI_ACCES_SUB_ANNONCE'), Atexo_Config::getParameter('PASSWORD_APPLI_ACCES_SUB_ANNONCE'), $uri, $logger, 'GET');
        $logger->info('response: '.print_r($xmlResponse, true));
        $logger->info('Fin appelle WS download pdf');

        return $xmlResponse;
    }

    /**
     * permet de creer le xml a envoye à SUB_ANNONCE.
     *
     * @param CommonConsultation    $consultation    consultation depuis on va creer le dossier SUB_ANNONCE
     * @param CommonDestinatairePub $destinatairePub
     * @param CommonTProfilJoue     $compteJoue      profil JOUE
     * @param array                 $listeSupport    liste supports
     *
     * @return string xml a envoye à SUB_ANNONCE
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public static function getXmlInitialisationDossierSIMAP($consultation, $destinatairePub, $compteJoue, $listeSupport)
    {
        $logger = null;
        try {
            $logger = Atexo_LoggerManager::getLogger('publicite');
            if ($destinatairePub instanceof CommonDestinatairePub) {
                $idDispositif = $destinatairePub->getIdDispositif();
                $idDossier = $destinatairePub->getIdDossier();
                $xml = '';
                $xml .= '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>';
                $xml .= '<dossiercreation filtreBinding="true" loginTiers="'.Atexo_Config::getParameter('IDENTIFIANT_ACCES_SUB_ANNONCE').'" idDispositif="'.$idDispositif.'" '.
                    ($idDossier ? ' idDossier="'.$idDossier.'"' : '').' >';
                $xml .= '<dossier_datas>';

                if (is_array($listeSupport) && count($listeSupport)) {
                    $xml .= self::baliseBinding('canal_type_concentrateur', implode(',', $listeSupport));
                }

                $xml .= self::getXmlInitialisationDepuisCompteJoue($compteJoue);
                $xml .= self::getXmlInitialisationDepuisConsultation($consultation);
                $xml .= self::getFluxSIMAPAvisInitial($consultation, $destinatairePub);

                $xml .= '</dossier_datas>';
                $xml .= '</dossiercreation>';

                return $xml;
            }
        } catch (Exception $e) {
            $logger->error('Erreur Atexo_FormulaireSub_AnnonceEchange::getXmlInitialisationDossier : '.$e->getMessage().' '.$e->getTraceAsString());
        }
    }

    /**
     * permet de creer le xml a envoye à SUB_ANNONCE.
     *
     * @param CommonTProfilJoue $compteJoue compte Joue
     *
     * @return string xml a envoye à SUB_ANNONCE
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public static function getXmlInitialisationDepuisCompteJoue($compteJoue)
    {
        $logger = null;
        try {
            $logger = Atexo_LoggerManager::getLogger('publicite');
            $xml = '';
            if ($compteJoue instanceof CommonTProfilJoue) {
                $xml .= self::baliseBinding('organisme_codeIdentificationNational', $compteJoue->getNumeroNationalIdentification());
                $xml .= self::baliseBinding('organisme_acheteurPublic', Atexo_Util::atexoHtmlEntitiesDecode($compteJoue->getNomOfficiel()), 'textType');
                $xml .= self::baliseBindingAdresse('organisme_adr', $compteJoue->getAdresse(), $compteJoue->getCodePostal(), $compteJoue->getVille(), $compteJoue->getLibelleSubPays());
                $xml .= self::baliseBinding('organisme_pointDeContact', Atexo_Util::atexoHtmlEntitiesDecode($compteJoue->getPointContact()));
                $xml .= self::baliseBinding('organisme_tel', $compteJoue->getTelephone());
                $xml .= self::baliseBinding('organisme_mel', $compteJoue->getEmail());
                $xml .= self::baliseBinding('organisme_fax', $compteJoue->getFax());
                $xml .= self::baliseBinding('organisme_url', $compteJoue->getAdressePouvoirAdjudicateur());
                $xml .= self::baliseBinding('organisme_urlProfilAcheteur', $compteJoue->getAdresseProfilAcheteur());
                $xml .= self::addBindingActivites($compteJoue);
                $xml .= self::addBindingTypePouvoirAdjuducateur($compteJoue);
            } else {
                $logger->error("Erreur Atexo_FormulaireSub_AnnonceEchange::getXmlInitialisationDepuisCompteJoue l'objet envoyé n'est pas un profil joue ".print_r($compteJoue, true));
            }

            return $xml;
        } catch (Exception $e) {
            $logger->error('Erreur Atexo_FormulaireSub_AnnonceEchange::getXmlInitialisationDepuisCompteBOAMP : '.$e->getMessage().' '.$e->getTraceAsString());
        }
    }

    /**
     * permet de creer la balise xml des activites du profil Joue.
     *
     * @param CommonTProfilJoue $compteJoue compte Joue
     *
     * @return string xml a envoye à SUB_ANNONCE
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public static function addBindingActivites($compteJoue)
    {
        $xml = '';
        $libelleAutre = '';
        if ($compteJoue instanceof CommonTProfilJoue) {
            $codesActivite = [];

            if ('1' == $compteJoue->getServicesGeneraux()) {
                $codesActivite = '01_servicesGenerauxAdmPub';
            }
            if ('1' == $compteJoue->getDefense()) {
                $codesActivite = '06_defense';
            }
            if ('1' == $compteJoue->getSecuritePublic()) {
                $codesActivite = '02_ordreSecuritePub';
            }
            if ('1' == $compteJoue->getEnvironnement()) {
                $codesActivite = '07_environnement';
            }
            if ('1' == $compteJoue->getAffairesEconomiques()) {
                $codesActivite = '03_affairesEcoFi';
            }
            if ('1' == $compteJoue->getSante()) {
                $codesActivite = '08_sante';
            }
            if ('1' == $compteJoue->getDeveloppementCollectif()) {
                $codesActivite = '04_logementDevCol';
            }
            if ('1' == $compteJoue->getProtectionSociale()) {
                $codesActivite = '09_protectionSociale';
            }
            if ('1' == $compteJoue->getLoisirs()) {
                $codesActivite = '05_loisirsCultureReligion';
            }
            if ('1' == $compteJoue->getEduction()) {
                $codesActivite = '10_education';
            }
            if ('1' == $compteJoue->getAutreActivitesPrincipales()) {
                $codesActivite = '20';
                $libelleAutre = $compteJoue->getAutreLibelleActivitesPrincipales();
            }

            $xml .= self::baliseBindingReferentiel('organisme_activitesOrganisme_activites', $codesActivite, 'activites');
            if ($libelleAutre) {
                $xml .= self::baliseBinding('organisme_activitesOrganisme_activites_autre', $libelleAutre);
            }
        }

        return $xml;
    }

    /**
     * permet de creer la balise xml des activites du profil Joue.
     *
     * @param CommonTProfilJoue $compteJoue compte Joue
     *
     * @return string xml a envoye à SUB_ANNONCE
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public static function addBindingTypePouvoirAdjuducateur($compteJoue)
    {
        $xml = '';
        $libelleAutre = '';
        if ($compteJoue instanceof CommonTProfilJoue) {
            $typePouvoir = '';
            if ('1' == $compteJoue->getAutoriteNationale()) {
                $typePouvoir = '001_ministereoutouteautreautorite';
            }
            if ('1' == $compteJoue->getOfficeNationale()) {
                $typePouvoir = '002_agenceofficenational';
            }
            if ('1' == $compteJoue->getOfficeRegionale()) {
                $typePouvoir = '004_agenceofficeregionaleoulocale';
            }
            if ('1' == $compteJoue->getOrganismePublic()) {
                $typePouvoir = '005_organismededroitspublics';
            }
            if ('1' == $compteJoue->getOrganisationEuropenne()) {
                $typePouvoir = '006_institutionagenceeuropéenne';
            }
            if ('1' == $compteJoue->getAutreTypePouvoirAdjudicateur()) {
                $typePouvoir = '020_autre';
                $libelleAutre = $compteJoue->getAutreLibelleTypePouvoirAdjudicateur();
            }
            $xml .= self::baliseBindingReferentiel('organisme_classificationOrganismeJOUE', $typePouvoir, 'ClassificationOrganisme');
            if ($libelleAutre) {
                $xml .= self::baliseBinding('organisme_classificationOrganismeJOUE_autre', $libelleAutre);
            }
        }

        return $xml;
    }

    /**
     * permet de creer le xml.
     *
     * @param CommonConsultation       $consultation
     * @param array CommonCategorieLot $lots
     *
     * @return string xml a envoye à SUB_ANNONCE
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public static function getFluxDepuisLots($consultation)
    {
        $logger = null;
        try {
            $logger = Atexo_LoggerManager::getLogger('publicite');
            $xml = '';
            if ($consultation instanceof CommonConsultation) {
                $xml .= ' <data nom="'.Atexo_Config::getParameter('SUB_DATA_BINDING_DETAILS_LOT').'" type="listeObjetType"><liste_objet>';
                if ($consultation->getAlloti()) {
                    $lots = $consultation->getAllLots();
                    if (is_array($lots) && !empty($lots)) {
                        foreach ($lots as $lot) {
                            if ($lot instanceof CommonCategorieLot) {
                                $xml .= self::getFluxDepuisUnLot($lot);
                            }
                        }
                    } else {
                        $logger->error("Erreur Atexo_FormulaireSub_AnnonceEchange::getFluxDepuisLots l'objet envoyé n'est pas un tableau ".print_r($lots, true));
                    }
                } else {
                    $xml .= self::getFluxDepuisUnLot($consultation);
                }
                $xml .= '</liste_objet> </data>';
            }

            return $xml;
        } catch (Exception $e) {
            $logger->error('Erreur Atexo_FormulaireSub_AnnonceEchange::getFluxDepuisLots : '.$e->getMessage().' '.$e->getTraceAsString());
        }
    }

    /**
     * permet de creer le xml.
     *
     *
     * @return string xml a envoye à SUB_ANNONCE
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     * @copyright Atexo 2016
     */
    public static function getFluxDepuisUnLot(\Application\Propel\Mpe\CommonConsultation|\Application\Propel\Mpe\CommonCategorieLot $data)
    {
        $xml = '';
        if ($data instanceof CommonConsultation || $data instanceof CommonCategorieLot) {
            $xml .= '<objet>';
            if ($data instanceof CommonCategorieLot) {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
                $consultation = CommonConsultationPeer::retrieveByPK($data->getConsultationId(), $connexion);
                $xml .= self::baliseBinding('numLot', $data->getLot(), 'longType');
                $infosComplementaireLot = (new Atexo_Referentiel_Referentiel())->getInfoFromReferentielConsultation(Atexo_Config::getParameter('INFOS_CMPLT_LOT'), $data->getConsultationId(), $data->getLot(), $data->getOrganisme());
                $xml .= self::baliseBinding('description', Atexo_Util::atexoHtmlEntitiesDecode($data->getDescriptionDetail()), 'textType');
                $xml .= self::baliseBinding('intituleLot', Atexo_Util::atexoHtmlEntitiesDecode($data->getDescription()), 'stringType');

                if (Atexo_Module::isEnabled('LieuxExecution')) {
                    $lieuExecution = (new Atexo_Consultation())->getLibelleLieuxExecution(null, $consultation);
                    $xml .= self::baliseBinding('lieuExecutionLivraison', Atexo_Util::atexoHtmlEntitiesDecode($lieuExecution), 'stringType');
                }

                $xml .= self::baliseBindingReferentiel('criteresSociaux', $data->getClauseSociale(), 'Réponse_oui_non');
                $xml .= self::baliseBindingReferentiel('criteresEnvironnementaux', $data->getClauseEnvironnementale(), 'Réponse_oui_non');
            } else {
                $consultation = $data;
                $infosComplementaireLot = (new Atexo_Referentiel_Referentiel())->getInfoFromReferentielConsultation(Atexo_Config::getParameter('INFOS_CMPLT_LOT'), $data->getId(), 0, $data->getOrganisme());
                $xml .= self::baliseBinding('intituleLot', Atexo_Util::atexoHtmlEntitiesDecode($data->getIntituleTraduit()), 'stringType');
                $xml .= self::baliseBinding('description', Atexo_Util::atexoHtmlEntitiesDecode($data->getObjetTraduit()), 'textType');
                if (Atexo_Module::isEnabled('LieuxExecution')) {
                    $lieuExecution = (new Atexo_Consultation())->getLibelleLieuxExecution(null, $consultation);
                    $xml .= self::baliseBinding('lieuExecutionLivraison', Atexo_Util::atexoHtmlEntitiesDecode($lieuExecution), 'stringType');
                }
            }
            $xml .= self::baliseBinding('infosComplementaires', Atexo_Util::atexoHtmlEntitiesDecode($infosComplementaireLot), 'textType');

            $agent = (new Atexo_Agent())->retrieveAgent(Atexo_CurrentUser::getIdAgentConnected());

            $xml .= self::baliseBinding('correspondantNom', $agent->getNom(), 'stringType');
            $xml .= self::baliseBinding('correspondantPrenom', $agent->getPrenom(), 'stringType');
            $xml .= self::baliseBinding('correspondantMail', $agent->getEmail(), 'stringType');
            $xml .= self::baliseBinding('correspondantTelephone', $agent->getNumTel(), 'stringType');
            $xml .= self::baliseBinding('correspondantFax', $agent->getNumFax(), 'stringType');

            if ($data->getCodeCpv1() || $data->getCodeCpv2()) {
                if (Atexo_Module::isEnabled('Publicite')) {
                    $xml .= self::baliseBinding('CPVObjetPrincipalClassPrincipale', Atexo_Util::atexoHtmlEntitiesDecode($data->getCodeCpv1()), 'stringType');
                    $cpvs = explode('#', $data->getCodeCpv2());
                    $i = 1;
                    foreach ($cpvs as $cpv) {
                        if ($i > 3) {
                            break;
                        }
                        if (!empty($cpv)) {
                            $xml .= self::baliseBinding('CPVObjetPrincipalClassSupplementaire'.$i++, $cpv, 'stringType');
                        }
                    }
                } else {
                    $xml .= self::baliseBindingReferentiel('CPVObjetPrincipalClassPrincipale', $data->getCodeCpv1(), 'cpv');
                    $cpvs = explode('#', $data->getCodeCpv2());
                    $i = 1;
                    foreach ($cpvs as $cpv) {
                        if ($i > 4) {
                            break;
                        }
                        if (!empty($cpv)) {
                            $xml .= self::baliseBindingReferentiel('CPVObjetPrincipalClassSupplementaire'.$i++, $cpv, 'cpv');
                        }
                    }
                }
            }
            $variante = '2';
            if (Atexo_Module::isEnabled('ConsultationVariantesAutorisees')) {
                if ($data->getVariantes()) {
                    $variante = '1';
                }
            }

            $donnesComplementaire = $data->getDonneComplementaire();
            if ($donnesComplementaire instanceof CommonTDonneeComplementaire) {
                if (Atexo_Module::isEnabled('DonneesRedac')) {
                    if ($donnesComplementaire->getVarianteExigee() || $donnesComplementaire->getVariantesAutorisees()) {
                        $variante = '1';
                    }
                }
                $marcheReconductible = '2';
                $reconduction = Prado::localize('DEFINE_NON');
                if ($donnesComplementaire->getReconductible()) {
                    $marcheReconductible = '1';
                    $reconduction = Prado::localize('DEFINE_OUI').'.';
                    if ($donnesComplementaire->getNombreReconductions()) {
                        $reconduction .= ' '.Prado::localize('DEFINE_MARCHE_RECONDUCTIBLE').' '.$donnesComplementaire->getNombreReconductions().' '.Prado::localize('TEXT_FOIS').'.';
                    }
                    if ($donnesComplementaire->getModalitesReconduction()) {
                        $reconduction .= ' '.Prado::localize('DEFINE_MODALITE_RECONDUCTIONS').' : '.$donnesComplementaire->getModalitesReconduction().'.';
                    }
                }
                $xml .= self::baliseBindingReferentiel('infosSupReconductionOuiOUReconductionNon', $marcheReconductible, 'Réponse_oui_non');
                $xml .= self::baliseBinding('Reconduction', $reconduction, 'textType');
                if ($donnesComplementaire->getPublicationMontantEstimation()) {
                    $xml .= self::baliseBindingReferentiel('estimationValeurDevise', 'EUR', 'devise');
                    $xml .= self::baliseBinding('estimationValeurValeur', $donnesComplementaire->getValeurMontantEstimationPubliee(), 'doubleType');
                }
                if ($donnesComplementaire->getIdDureeDelaiDescription()) {
                    $dureeDelaiDescription = (new Atexo_CommonReferentiel())->retrieveValeurReferentielById(Atexo_Config::getParameter('REFERENTIEL_DUREE_DELAI'), $donnesComplementaire->getIdDureeDelaiDescription());
                    $dureeDelaiMarche = false;
                    $dureeNbMoisOUNbJours = false;
                    $dureeMarche = false;
                    $delaiMarche = false;

                    if ($dureeDelaiDescription instanceof CommonValeurReferentiel) {
                        switch ($dureeDelaiDescription->getLibelle2()) {
                            case 'DUREE_MARCHEE':
                                $dureeMarche = true;
                                $dureeDelaiMarche = 'delaiduree';
                                $refUniteDureeMarche = (new Atexo_CommonReferentiel())->retrieveValeurReferentielById(Atexo_Config::getParameter('REFERENTIEL_UNITE_DUREE_MARCHE'), $donnesComplementaire->getIdChoixMoisJour());
                                if ($refUniteDureeMarche instanceof CommonValeurReferentiel) {
                                    switch ($refUniteDureeMarche->getLibelle2()) {
                                        case 'dureeMois':
                                            $dureeNbMoisOUNbJours = 'unitedureemois';
                                            break;
                                        case 'dureeJours':
                                            $dureeNbMoisOUNbJours = 'unitedureejours';
                                            break;
                                    }
                                }
                                break;
                            case 'DELAI_EXECUTION':
                                $delaiMarche = true;
                                $dureeDelaiMarche = 'delaidate';
                                break;
                            case 'DESCRIPTION_LIBRE':
                                //$xml .= self::baliseBinding("infoCompl", Atexo_Util::atexoHtmlEntitiesDecode($donnesComplementaire->getDureeDescription()), "textType");
                                break;
                        }
                    }
                    if ($dureeDelaiMarche) {
                        $xml .= self::baliseBindingReferentiel('dureeNbOUdate', $dureeDelaiMarche, 'TypeDelai');
                    }
                    if ($dureeMarche) {
                        if ($dureeNbMoisOUNbJours) {
                            $xml .= self::baliseBindingReferentiel('dureeNbMoisOUNbJours', $dureeNbMoisOUNbJours, 'untiéduree');
                        }
                        $xml .= self::baliseBinding('dureeNb', $donnesComplementaire->getDureeMarche(), 'longType');
                    }
                    if ($delaiMarche) {
                        $xml .= self::baliseBinding('dureeDateAcompterDu', Atexo_Util::iso2frnDate($donnesComplementaire->getDureeDateDebut()), 'dateType');
                        $xml .= self::baliseBinding('dureeDateJusquAu', Atexo_Util::iso2frnDate($donnesComplementaire->getDureeDateFin()), 'dateType');
                    }
                }

                if ($data instanceof CommonCategorieLot && ((($donnesCompCons = $data->getDonneComplementaireCons()) instanceof CommonTDonneeComplementaire) && $donnesCompCons->getCriteresIdentiques())) {
                    $donnesComplementaireCritereAttribution = $donnesCompCons;
                } else {
                    $donnesComplementaireCritereAttribution = $donnesComplementaire;
                }
                $xml .= self::getXmlCriteresAttribution($donnesComplementaireCritereAttribution);

                if ($donnesComplementaire->getMotsCles()) {
                    $motsCles = $donnesComplementaire->getValeurSubMotsCles();
                    if (is_array($motsCles)) {
                        $xml .= self::baliseBindingItemReferentiel('motsClef', $motsCles, 'motsClef');
                    }
                }
                if ($consultation instanceof CommonConsultation) {
                    $codeNuts = explode('#', ltrim($consultation->getCodesNuts(), '#'));
                    if (is_array($codeNuts) && count($codeNuts)) {
                        for ($i = 0; $i < 4; ++$i) {
                            if (isset($codeNuts[$i])) {
                                $xml .= self::baliseBindingReferentiel('lieuCodeNUTS'.($i + 1), $codeNuts[$i], 'NUTS');
                            }
                        }
                    }
                }
            }

            $xml .= self::baliseBindingReferentiel('caracteristiquesVariante', $variante, 'Réponse_oui_non');
            $infoFondsUE = '';
            if ($donnesComplementaire instanceof CommonTDonneeComplementaire) {
                if (1 == $donnesComplementaire->getProjetFinanceFondsUnionEuropeenne()) {
                    $infoFondsUE = Prado::localize('DEFINE_OUI').'.';
                    if ($donnesComplementaire->getIdentificationProjet()) {
                        $infoFondsUE .= PHP_EOL.Prado::localize('IDENTIFICATION_PROJET_PROGRAMME').' : '.$donnesComplementaire->getIdentificationProjet().'.';
                    }
                } elseif (0 == $donnesComplementaire->getProjetFinanceFondsUnionEuropeenne()) {
                    $infoFondsUE = Prado::localize('DEFINE_NON');
                }
            }
            $xml .= self::baliseBinding('informationFondsEuropeens', $infoFondsUE, 'textType');
            $xml .= '</objet>';
        }

        return $xml;
    }

    /**
     * permet de creer le xml a envoye à SUB_ANNONCE.
     *
     * @param CommonConsultation    $consultation
     * @param CommonDestinatairePub $destinatairePub
     *
     * @return string xml
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public static function getFluxSIMAPAvisInitial($consultation, $destinataire)
    {
        $logger = null;
        try {
            $logger = Atexo_LoggerManager::getLogger('publicite');
            $xml = '';
            $infoJoue = (new Atexo_Publicite_AvisPub())->recupererInfosJoue($consultation, 0, $destinataire);
            if (isset($infoJoue['id_joue']) && !empty($infoJoue['id_joue'])) {
                $xml .= self::baliseBinding('avisInitial_idWeb', $infoJoue['id_joue']);
                $xml .= self::baliseBinding('TED_avisInitial_dateEnvoi', Atexo_Util::iso2frnDate($infoJoue['date_pub_joue']), 'dateType');
            }

            return $xml;
        } catch (Exception $e) {
            $logger->error('Erreur Atexo_FormulaireSub_AnnonceEchange::getFluxSIMAPAvisInitial : '.$e->getMessage().' '.$e->getTraceAsString());
        }
    }

    /**
     * Retourne pdf simap.
     *
     * @param string $id : id dossier
     *
     * @return false|string : contenu du retour du ws en json si valide, false sinon
     *
     * @author Amal EL BEKKAOUI <elbekkaoui.amal@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function recupererPdfSimap($id): false|string
    {
        $url = null;
        try {
            $url = Atexo_Config::getParameter('URL_CONCENTRATEUR_PDF_SIMAP').'?id='.$id;
            (new Atexo_Publicite_AnnonceSub())->loggerInfosSuiviAnnonces("Url ws = $url");

            $configProxy = self::getConfigProxyForGuzzle(Atexo_Util::getSfContainer()) ?: [];
            $configProxy['timeout'] = 120;
            $client = new Client($configProxy);

            $reponseAPI = $client->Get($url);
            $contentAPI = $reponseAPI->getBody();
            $statut = $reponseAPI->getStatusCode();
            (new Atexo_Publicite_AnnonceSub())->loggerInfosSuiviAnnonces("Statut retour ws = $statut");
            (new Atexo_Publicite_AnnonceSub())->loggerInfosSuiviAnnonces("Contenu retour ws = $contentAPI");

            if (true === self::validerRetourWs($statut)) {
                $response = json_decode($contentAPI, null, 512, JSON_THROW_ON_ERROR);
                if (isset($response->pdfEncodedBase64)) {
                    return base64_decode($response->pdfEncodedBase64);
                }
            }

            return false;
        } catch (Exception $e) {
            (new Atexo_Publicite_AnnonceSub())->loggerErreursSuiviAnnonces("Erreur survenue lors de la recuperation des donnees de mise a jour des annonces via concentrateur : Id = '$id' , Url = $url , Erreur = ".$e->getMessage()." \nMethode : Atexo_FormulaireSub_AnnonceEchange::recupererPdfSimap");
        }
    }
}
