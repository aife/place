<?php

namespace Application\Service\Atexo\Soap;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonAgent;
use Application\Propel\Mpe\CommonCategorieLot;
use Application\Propel\Mpe\CommonEnveloppe;
use Application\Propel\Mpe\CommonOffrePapier;
use Application\Propel\Mpe\CommonOffres;
use Application\Propel\Mpe\CommonTypeProcedureOrganisme;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Lots;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureType;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Responses;
use PDO;
use SoapFault;

/**
 * Classe de gestion de l'interface CNFPT-AX.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2014
 *
 * @since MPE-4
 */
class Atexo_Soap_InterfaceCnfptAx
{
    public function envoyerDataMarches($dataRequest)
    {
        try {
            ini_set('default_socket_timeout', 300);
            ini_set('soap.wsdl_cache_enabled', 0);
            $url = Atexo_Config::getParameter('URL_CNFPT_SOAP_CREATION_MARCHE');
            $arrayOption = ['trace' => 1];
            $client = new Atexo_Soap_NtlmSoapClient($url, $arrayOption);
            $resultat = $client->create($dataRequest);

            //Résultat du web service
            return $resultat;
        } catch (SoapFault $e) {
            //Atexo_Soap_NtlmSoapClient::soapDebug($client); exit;
            throw $e;
            //$logger->error($client->__getLastRequest());
        }
    }

    /**
     * Permet de recuperer les données du marché et construire un tableau pour organiser les données.
     */
    public function recupererDataMarches()
    {
        $statement = null;
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $dataMarche = [];

        $sql = '	SELECT distinct (CT.id_contrat_titulaire), CT.date_notification, CT.organisme, CT.id_offre, C.datefin, C.date_mise_en_ligne_calcule, ';
        $sql .= ' C.id_type_procedure, C.id_createur, C.reference_utilisateur, C.intitule, C.objet, C.alloti, C.id, DL.date_decision, ';
        $sql .= ' C.organisme as OrgConsultation, CT.type_depot_reponse as type_env ';
        $sql .= '  FROM t_contrat_titulaire CT, consultation C, DecisionLot DL , t_cons_lot_contrat CLC';
        $sql .= '  WHERE  C.id = DL.consultation_id AND DL.consultation_id = CLC.consultation_id ';
        $sql .= "  AND t_contrat_titulaire.contrat_class_key = '".Atexo_Config::getParameter('TYPE_OBJET_CONTRAT_TITULAIRE')."'";
        $sql .= '  AND  CLC.id_contrat_titulaire = CT.id_contrat_titulaire ';
        $sql .= '  AND CT.organisme = C.organisme AND CT.organisme = DL.organisme ';
        $sql .= '  AND CT.date_notification IS NOT NULL ';
        $sql .= "  AND DL.id_type_decision IN ('".Atexo_Config::getParameter('DECISION_ATTRIBUTION_MARCHE')."') ";
        $sql .= "  AND CT.envoi_interface = '0'";

        try {
            $statement = Atexo_Db::getLinkCommon()->query($sql);
        } catch (\Exception $e) {
            $msgErreur = $e->getMessage()."\n Erreur lors de l'exécution de la requete: [$sql] \n";
            $logger = Atexo_LoggerManager::getLogger();
            $logger->error($msgErreur);
        }
        try {
            if ($statement) {
                while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                    $consultationId = $row['id'];
                    $organisme = $row['organisme'];
                    if ($row['alloti']) {//Consultation allotie
                        $lots = (new Atexo_Consultation_Lots())->retrieveAllLots($organisme, $consultationId, $connexion);
                        if (is_array($lots) && count($lots)) {
                            foreach ($lots as $lot) {
                                self::remplirDataLots($dataMarche, $row, $lot);
                            }
                        }
                    } else {//Consultation non allotie
                        self::remplirDataLots($dataMarche, $row);
                    }
                }
            }

            return $dataMarche;
        } catch (\Exception $e) {
            $logger = Atexo_LoggerManager::getLogger();
            $logger->error($e->getMessage());
        }
    }

    /**
     * Permet d'organiser les données des marchés dans un tableau.
     *
     * @param $dataMarche: tableau des marchés à remplir
     * @param $row: contient les données de la ligne de la requete
     * @param $lot: objet CommonCategorieLot
     */
    public function remplirDataLots(&$dataMarche, $row, $lot = null)
    {
        try {
            $numLot = '0';
            $consultationId = $row['id'];
            $idOffre = $row['id_offre'];
            $organisme = $row['organisme'];
            if ($lot instanceof CommonCategorieLot) {
                $numLot = $lot->getLot();
            }
            $dataMarche[$consultationId][$numLot]['siret'] = '';
            $dataMarche[$consultationId][$numLot]['DateReceptOffr'] = substr($row['datefin'], 0, 10);
            $dataMarche[$consultationId][$numLot]['DateLigne'] = substr($row['date_mise_en_ligne_calcule'], 0, 10);
            if (!$dataMarche[$consultationId][$numLot]['DateNotif']) {
                $dataMarche[$consultationId][$numLot]['DateNotif'] = substr($row['date_notification'], 0, 10);
            }
            if (!$dataMarche[$consultationId][$numLot]['DateAttrib']) {
                $dataMarche[$consultationId][$numLot]['DateAttrib'] = substr($row['date_decision'], 0, 10);
            }
            $typeProc = Atexo_Consultation_ProcedureType::retrieveProcedureById($row['id_type_procedure'], $organisme);
            if ($typeProc instanceof CommonTypeProcedureOrganisme) {
                $dataMarche[$consultationId][$numLot]['ModePass'] = substr($typeProc->getAbbreviation(), 0, 10);
            }
            $dataMarche[$consultationId][$numLot]['idContratTitulaire'] = $row['id_contrat_titulaire'];
            $dataMarche[$consultationId][$numLot]['organisme'] = $row['OrgConsultation'];
            $agent = (new Atexo_Agent())->retrieveAgent($row['id_createur']);
            if ($agent instanceof CommonAgent) {
                $dataMarche[$consultationId][$numLot]['NomResp'] = substr($agent->getPrenom().' '.$agent->getNom(), 0, 80);
            }
            $dataMarche[$consultationId][$numLot]['NumCons'] = substr($row['reference_utilisateur'], 0, 20);
            $dataMarche[$consultationId][$numLot]['ObjCourt'] = substr($row['intitule'], 0, 40);
            $dataMarche[$consultationId][$numLot]['ObjLong'] = substr($row['objet'], 0, 3999);
            if ($numLot && '0' != $numLot) {
                $dataMarche[$consultationId][$numLot]['NumLot'] = substr($numLot, 0, 20);
                $dataMarche[$consultationId][$numLot]['ObjCourtLot'] = substr($lot->getDescription(), 0, 40);
                $dataMarche[$consultationId][$numLot]['ObjLongLot'] = substr($lot->getDescriptionDetail(), 0, 3999);
            }
            if ($row['type_env'] == Atexo_Config::getParameter('DEPOT_ELECTRONIQUE')) {
                $offre = (new Atexo_Consultation_Responses())->retrieveOffreById($idOffre, $organisme);
                if ($offre instanceof CommonOffres && !$dataMarche[$consultationId][$numLot]['siret']) {
                    $dataMarche[$consultationId][$numLot]['siret'] = substr($offre->getSiretEntreprise(), 0, 14);
                }
            } else {
                $offre = (new Atexo_Consultation_Responses())->retrieveOffrePapierById($idOffre, $organisme);
                if ($offre instanceof CommonOffrePapier && !$dataMarche[$consultationId][$numLot]['siret']) {
                    $dataMarche[$consultationId][$numLot]['siret'] = substr($offre->getSiret(), 0, 14);
                }
            }
            $enveloppes = (new Atexo_Consultation_Responses())->retrieveEnveloppesByIdOffre($idOffre, $organisme);
            if (is_array($enveloppes) && count($enveloppes)) {
                foreach ($enveloppes as $enveloppe) {
                    if ($enveloppe instanceof CommonEnveloppe) {
                        if ($enveloppe->getTypeEnv() == Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE') && !$dataMarche[$consultationId][$numLot]['DateCandid']) {
                            $dataMarche[$consultationId][$numLot]['DateCandid'] = substr($enveloppe->getDateheureOuverture(), 0, 10);
                        } elseif ($enveloppe->getTypeEnv() == Atexo_Config::getParameter('TYPE_ENV_OFFRE') && !$dataMarche[$consultationId][$numLot]['DateOuvPlis']) {
                            $dataMarche[$consultationId][$numLot]['DateOuvPlis'] = substr($enveloppe->getDateheureOuverture(), 0, 10);
                        }
                    }
                }
            }
        } catch (\Exception $e) {
            $logger = Atexo_LoggerManager::getLogger();
            $logger->error($e->getMessage());
        }
    }

    /**
     * Permet de construire le tableau des données à transmettre via soap.
     *
     * @param array $data: les données concernant une consultation
     */
    public function createDataRequest($dataCons)
    {
        $dataRequest = [];
        if (is_array($dataCons) && count($dataCons)) {
            $dataRequest['GFIAtexoConsultation']['SenderId'] = '0';
            $dataRequest['GFIAtexoConsultation']['ClearNilFieldsOnUpdate'] = 'false';
            $dataRequest['GFIAtexoConsultation']['GFIAtexoConsultation']['NumCons'] = Atexo_Util::toUtf8($dataCons[0]['NumCons']);
            $dataRequest['GFIAtexoConsultation']['GFIAtexoConsultation']['ObjLong'] = Atexo_Util::toUtf8($dataCons[0]['ObjLong']);
            $dataRequest['GFIAtexoConsultation']['GFIAtexoConsultation']['ObjCourt'] = Atexo_Util::toUtf8($dataCons[0]['ObjCourt']);
            $dataRequest['GFIAtexoConsultation']['GFIAtexoConsultation']['NomResp'] = Atexo_Util::toUtf8($dataCons[0]['NomResp']);
            $dataRequest['GFIAtexoConsultation']['GFIAtexoConsultation']['ModePassation'] = ''; //Atexo_Util::toUtf8($dataCons[0]['ModePass']);
            $dataRequest['GFIAtexoConsultation']['GFIAtexoConsultation']['class'] = 'entity';
            foreach ($dataCons as $numLot => $dataLot) {
                $dataRequest['GFIAtexoConsultation']['GFIAtexoConsultation'][$numLot]['GFIAtexoLot']['class'] = 'entity';
                if ($numLot && '0' != $numLot) {
                    $dataRequest['GFIAtexoConsultation']['GFIAtexoConsultation']['NumCons'] = Atexo_Util::toUtf8($dataCons[$numLot]['NumCons']);
                    $dataRequest['GFIAtexoConsultation']['GFIAtexoConsultation']['ObjLong'] = Atexo_Util::toUtf8($dataCons[$numLot]['ObjLong']);
                    $dataRequest['GFIAtexoConsultation']['GFIAtexoConsultation']['ObjCourt'] = Atexo_Util::toUtf8($dataCons[$numLot]['ObjCourt']);
                    $dataRequest['GFIAtexoConsultation']['GFIAtexoConsultation']['NomResp'] = Atexo_Util::toUtf8($dataCons[$numLot]['NomResp']);
                    $dataRequest['GFIAtexoConsultation']['GFIAtexoConsultation']['ModePassation'] = ''; //Atexo_Util::toUtf8($dataCons[$numLot]['ModePass']);
                    $dataRequest['GFIAtexoConsultation']['GFIAtexoConsultation'][$numLot]['GFIAtexoLot']['NumLot'] = Atexo_Util::toUtf8($dataLot['NumLot']);
                    $dataRequest['GFIAtexoConsultation']['GFIAtexoConsultation'][$numLot]['GFIAtexoLot']['ObjLong'] = Atexo_Util::toUtf8($dataLot['ObjLongLot']);
                    $dataRequest['GFIAtexoConsultation']['GFIAtexoConsultation'][$numLot]['GFIAtexoLot']['ObjCourt'] = Atexo_Util::toUtf8($dataLot['ObjCourtLot']);
                } else {
                    $dataRequest['GFIAtexoConsultation']['GFIAtexoConsultation'][$numLot]['GFIAtexoLot']['NumLot'] = 0;
                    //$dataRequest["GFIAtexoConsultation"]["GFIAtexoConsultation"]["GFIAtexoLot"]["ObjLong"]=$dataLot[$numLot]['ObjLongLot'];
                    //$dataRequest["GFIAtexoConsultation"]["GFIAtexoConsultation"]["GFIAtexoLot"]["ObjCourt"]=$dataLot[$numLot]['ObjCourtLot'];
                }
                $dataRequest['GFIAtexoConsultation']['GFIAtexoConsultation'][$numLot]['GFIAtexoLot']['Siret'] = Atexo_Util::toUtf8($dataLot['siret']);
                if ($dataLot['DateLigne'] && '0000-00-00' != $dataLot['DateLigne']) {
                    $dataRequest['GFIAtexoConsultation']['GFIAtexoConsultation'][$numLot]['GFIAtexoLot']['DateLigne'] = Atexo_Util::toUtf8($dataLot['DateLigne']);
                }
                if ($dataLot['DateCandid'] && '0000-00-00' != $dataLot['DateCandid']) {
                    $dataRequest['GFIAtexoConsultation']['GFIAtexoConsultation'][$numLot]['GFIAtexoLot']['DateCandid'] = Atexo_Util::toUtf8($dataLot['DateCandid']);
                }
                if ($dataLot['DateReceptOffr'] && '0000-00-00' != $dataLot['DateReceptOffr']) {
                    $dataRequest['GFIAtexoConsultation']['GFIAtexoConsultation'][$numLot]['GFIAtexoLot']['DateReceptOffr'] = Atexo_Util::toUtf8($dataLot['DateReceptOffr']);
                }
                if ($dataLot['DateOuvPlis'] && '0000-00-00' != $dataLot['DateOuvPlis']) {
                    $dataRequest['GFIAtexoConsultation']['GFIAtexoConsultation'][$numLot]['GFIAtexoLot']['DateOuvPlis'] = Atexo_Util::toUtf8($dataLot['DateOuvPlis']);
                }
                if ($dataLot['DateNotif'] && '0000-00-00' != $dataLot['DateNotif']) {
                    $dataRequest['GFIAtexoConsultation']['GFIAtexoConsultation'][$numLot]['GFIAtexoLot']['DateNotif'] = Atexo_Util::toUtf8($dataLot['DateNotif']);
                }
                if ($dataLot['DateAttrib'] && '0000-00-00' != $dataLot['DateAttrib']) {
                    $dataRequest['GFIAtexoConsultation']['GFIAtexoConsultation'][$numLot]['GFIAtexoLot']['DateAttrib'] = Atexo_Util::toUtf8($dataLot['DateAttrib']);
                }
                $dataRequest['GFIAtexoConsultation']['GFIAtexoConsultation'][$numLot]['GFIAtexoLot']['idDecisionEnveloppe'] = Atexo_Util::toUtf8($dataLot['idDecisionEnveloppe']);
                $dataRequest['GFIAtexoConsultation']['GFIAtexoConsultation'][$numLot]['GFIAtexoLot']['organisme'] = Atexo_Util::toUtf8($dataLot['organisme']);
            }
        }

        return $dataRequest;
    }

    /**
     * Précise si l'envoi s'est deroulé avec succès.
     *
     * @param object $resultat
     * @return: true si succès, false sinon
     */
    public function isSendWsSucces($resultat)
    {
        try {
            if (is_object($resultat) && is_object($resultat->EntityKeyList) && is_object($resultat->EntityKeyList->EntityKey)
                && is_object($resultat->EntityKeyList->EntityKey->KeyData) && is_object($resultat->EntityKeyList->EntityKey->KeyData->KeyField)) {
                if ($resultat->EntityKeyList->EntityKey->KeyData->KeyField->Field && $resultat->EntityKeyList->EntityKey->KeyData->KeyField->Value) {
                    return $resultat->EntityKeyList->EntityKey->KeyData->KeyField->Value;
                }
            }

            return false;
        } catch (\Exception $e) {
            $logger = Atexo_LoggerManager::getLogger();
            $logger->error($e->getMessage());
        }
    }

    /**
     * Permet de recuperer l'identifiant de la decisionEnveloppe à partir du tableau input.
     *
     * @param array $cons
     * @return: identifiant de la decisionEnveloppe
     */
    public function getIdDecisionFromDataCons($datas)
    {
        if (is_array($datas) && count($datas)) {
            foreach ($datas as $data) {
                if ($data['idContratTitulaire']) {
                    return $data['idContratTitulaire'];
                }
            }
        }

        return false;
    }

    /**
     * Permet de recuperer l'organisme à partir du tableau input.
     *
     * @param array $cons
     * @return: organisme
     */
    public function getOrganismeFromDataCons($datas)
    {
        if (is_array($datas) && count($datas)) {
            foreach ($datas as $data) {
                if ($data['organisme']) {
                    return $data['organisme'];
                }
            }
        }

        return false;
    }
}
