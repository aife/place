<?php

namespace Application\Service\Atexo\Soap;

use Application\Service\Atexo\Atexo_Config;
use SoapClient;
use stdClass;

class Atexo_Soap_InterfaceWsTED
{
    public function soapClient()
    {
        $wsdlUrl = Atexo_Config::getParameter('WSDL_TED');
        $soapClient = new SoapClient($wsdlUrl, [
            'trace' => 1,
            'exceptions' => 0,
            'proxy_host' => Atexo_Config::getParameter('URL_PROXY'),
            'proxy_port' => Atexo_Config::getParameter('PORT_PROXY'),
        ]);
        $headers = Atexo_Soap_WSSoapClient::wssecurity_header();
        $soapClient->__setSoapHeaders($headers);

        return $soapClient;
    }

    public function parseNotice($notice)
    {
        if (Atexo_Config::getParameter('UTILISER_CONCENTRATEUR_TED')) {
            return self::sendCurl(['service' => 'parseNotice', 'notice' => $notice]);
        } else {
            $soapClient = self::soapClient();
            $parse = new stdClass();
            $parse->notice = $notice;
            $response = $soapClient->parseNotice($parse);
            $result = ['erreur' => 'null', 'code_erreur' => 'null', 'libelle_erreur' => 'null', 'resultat' => 'null'];
            if (is_soap_fault($response)) {
                $result['erreur'] = 'true';
                $result['code_erreur'] = $response->detail->applicationErrorFault->errorCode;
                $result['libelle_erreur'] = $response->detail->applicationErrorFault->errorDescription;
            } else {
                $result['erreur'] = 'false';
                $result['libelle_erreur'] = 'null';
                $result['resultat'] = $response->parsingResult;
            }

            return $result;
        }
    }

    public function submitNotice($notice)
    {
        if (Atexo_Config::getParameter('UTILISER_CONCENTRATEUR_TED')) {
            return self::sendCurl(['service' => 'submitNotice', 'notice' => $notice]);
        } else {
            $soapClient = self::soapClient();
            $parse = new stdClass();
            $parse->notice = $notice;
            $response = $soapClient->submitNotice($parse);
            $result = ['erreur' => 'null', 'code_erreur' => 'null', 'libelle_erreur' => 'null', 'resultat' => 'null'];
            if (is_soap_fault($response)) {
                $result['erreur'] = 'true';
                $result['code_erreur'] = $response->detail->applicationErrorFault->errorCode;
                $result['libelle_erreur'] = $response->detail->applicationErrorFault->errorDescription;
            } else {
                $result['erreur'] = 'false';
                $result['libelle_erreur'] = 'null';
                $result['resultat'] = $response->submissionId;
            }

            return $result;
        }
    }

    public function getNoticeStatus($submissionId)
    {
        if (Atexo_Config::getParameter('UTILISER_CONCENTRATEUR_TED')) {
            return self::sendCurl(['service' => 'getNoticeStatus', 'submissionId' => $submissionId]);
        } else {
            $soapClient = self::soapClient();
            $parse = new stdClass();
            $parse->submissionId = $submissionId;
            $response = $soapClient->getNoticeStatus($parse);
            $result = ['erreur' => 'null', 'code_erreur' => 'null', 'libelle_erreur' => 'null', 'resultat' => 'null'];
            if (is_soap_fault($response)) {
                $result['erreur'] = 'true';
                $result['code_erreur'] = $response->detail->applicationErrorFault->errorCode;
                $result['libelle_erreur'] = $response->detail->applicationErrorFault->errorDescription;
            } else {
                $result['erreur'] = 'false';
                $result['libelle_erreur'] = 'null';
                $result['resultat'] = $response->noticeStatus->statusCode;
            }

            return $result;
        }
    }

    public function getNoticeReport($submissionId)
    {
        if (Atexo_Config::getParameter('UTILISER_CONCENTRATEUR_TED')) {
            return self::sendCurl(['service' => 'getNoticeReport', 'submissionId' => $submissionId]);
        } else {
            $soapClient = self::soapClient();
            $parse = new stdClass();
            $parse->submissionId = $submissionId;
            $response = $soapClient->getNoticeReport($parse);
            $result = ['erreur' => 'null', 'code_erreur' => 'null', 'libelle_erreur' => 'null', 'resultat' => 'null'];
            if (is_soap_fault($response)) {
                $result['erreur'] = 'true';
                $result['code_erreur'] = $response->detail->applicationErrorFault->errorCode;
                $result['libelle_erreur'] = $response->detail->applicationErrorFault->errorDescription;
            } else {
                $result['erreur'] = 'false';
                $result['libelle_erreur'] = 'null';
                $result['resultat'] = $response->noticeReport;
            }

            return $result;
        }
    }

    public function getNoticesReport($submissionDateInterval, $publicationDateInterval, $statusCode = false)
    {
        if (Atexo_Config::getParameter('UTILISER_CONCENTRATEUR_TED')) {
            return self::sendCurl(['service' => 'getNoticeReport', 'submissionDateInterval' => $submissionDateInterval, 'publicationDateInterval' => $publicationDateInterval, 'statusCode' => $statusCode]);
        } else {
            $soapClient = self::soapClient();
            $parse = new stdClass();
            $parse->submissionDateInterval = $submissionDateInterval;
            $parse->publicationDateInterval = $publicationDateInterval;
            $parse->statusCode = $statusCode;
            $response = $soapClient->getNoticesReport($parse);
            $result = ['erreur' => 'null', 'code_erreur' => 'null', 'libelle_erreur' => 'null', 'resultat' => 'null'];
            if (is_soap_fault($response)) {
                $result['erreur'] = 'true';
                $result['code_erreur'] = $response->detail->applicationErrorFault->errorCode;
                $result['libelle_erreur'] = $response->detail->applicationErrorFault->errorDescription;
            } else {
                $result['erreur'] = 'false';
                $result['libelle_erreur'] = 'null';
                $result['resultat'] = $response;
            }

            return $result;
        }
    }

    public function generatePDFRegulation($notice, $submissionId)
    {
        if (Atexo_Config::getParameter('UTILISER_CONCENTRATEUR_TED')) {
            return self::sendCurl(['service' => 'generatePDFRegulation', 'notice' => $notice, 'submissionId' => $submissionId]);
        } else {
            $soapClient = self::soapClient();
            $parse = new stdClass();
            $parse->notice = $notice;
            $parse->submissionId = $submissionId;
            $response = $soapClient->generatePdfRegulation($parse);
            $result = ['erreur' => 'null', 'code_erreur' => 'null', 'libelle_erreur' => 'null', 'resultat' => 'null'];
            if (is_soap_fault($response)) {
                $result['erreur'] = 'true';
                $result['code_erreur'] = $response->detail->applicationErrorFault->errorCode;
                $result['libelle_erreur'] = $response->detail->applicationErrorFault->errorDescription;
            } else {
                $result['erreur'] = 'false';
                $result['libelle_erreur'] = 'null';
                $result['resultat'] = $response->pdf;
            }

            return $result;
        }
    }

    public function generatePDFTED($notice, $submissionId, $lang = null)
    {
        if (Atexo_Config::getParameter('UTILISER_CONCENTRATEUR_TED')) {
            return self::sendCurl(['service' => 'generatePDFTED', 'notice' => $notice, 'submissionId' => $submissionId, 'coverPage' => 'no', 'language' => $lang]);
        } else {
            $soapClient = self::soapClient();
            $parse = new stdClass();
            $parse->notice = $notice;
            $parse->submissionId = $submissionId;
            $parse->language = $lang;
            $parse->coverPage = 'no';
            $response = $soapClient->generatePdfTED($parse);
            $result = ['erreur' => 'null', 'code_erreur' => 'null', 'libelle_erreur' => 'null', 'resultat' => 'null'];
            if (is_soap_fault($response)) {
                $result['erreur'] = 'true';
                $result['code_erreur'] = $response->detail->applicationErrorFault->errorCode;
                $result['libelle_erreur'] = $response->detail->applicationErrorFault->errorDescription;
            } else {
                $result['erreur'] = 'false';
                $result['libelle_erreur'] = 'null';
                $result['resultat'] = $response->pdf;
            }

            return $result;
        }
    }

    public function getLastResponseNoticeReport($submissionId)
    {
        if (Atexo_Config::getParameter('UTILISER_CONCENTRATEUR_TED')) {
            return self::sendCurl(['service' => 'getLastResponseNoticeReport', 'submissionId' => $submissionId]);
        } else {
            $response = '';
            $soapClient = self::soapClient();
            $parse = new stdClass();
            $parse->submissionId = $submissionId;
            $response = $soapClient->getNoticeReport($parse);

            return $soapClient->__getLastResponse() ?: '';
        }
    }

    public function sendCurl($data)
    {
        $submit_url = Atexo_Config::getParameter('URL_CONCENTRATEUR_TED');
        $ch = curl_init($submit_url);
        curl_setopt($ch, CURLOPT_SSLVERSION, 3);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/4.0 (compatible; MSIE 5.01; Windows NT 5.0)');
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, true);
        curl_setopt($ch, CURLOPT_HTTPPROXYTUNNEL, true);
        //definition du proxy si existe
        $urlProxy = Atexo_Config::getParameter('URL_PROXY');
        $portProxy = Atexo_Config::getParameter('PORT_PROXY');
        $curlOptProxy = $urlProxy.':'.$portProxy;
        if ('' != $urlProxy) {
            curl_setopt($ch, CURLOPT_PROXY, $curlOptProxy);
        }

        $response = curl_exec($ch);
        $arrayReponse = unserialize($response);
        //$status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        // erreur curl
        $errorNumber = curl_errno($ch);
        if (0 != $errorNumber) {
            $errorMsg = '';
            $errorMsg = curl_error($ch);
            $error = "CONNEXION_SERVEUR_HORODATAGE_ECHEC \n $errorMsg";

            return [1, $error];
        }

        curl_close($ch);

        return $arrayReponse;
    }
}
