<?php

namespace Application\Service\Atexo\Soap;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonAffiliationService;
use Application\Propel\Mpe\CommonAffiliationServicePeer;
use Application\Propel\Mpe\CommonAgent;
use Application\Propel\Mpe\CommonAgentPeer;
use Application\Propel\Mpe\CommonAgentServiceMetier;
use Application\Propel\Mpe\CommonAgentServiceMetierPeer;
use Application\Propel\Mpe\CommonDateLancementCron;
use Application\Propel\Mpe\CommonDateLancementCronPeer;
use Application\Propel\Mpe\CommonInscrit;
use Application\Propel\Mpe\CommonInscritPeer;
use Application\Propel\Mpe\CommonOrganismePeer;
use Application\Propel\Mpe\CommonResponsableengagement;
use Application\Propel\Mpe\CommonResponsableengagementPeer;
use Application\Propel\Mpe\CommonService;
use Application\Propel\Mpe\CommonServicePeer;
use Application\Propel\Mpe\Entreprise;
use Application\Propel\Mpe\EntreprisePeer;
use Application\Service\Atexo\Agent\Atexo_Agent_EnablingsAgents;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Entreprise;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Inscrit;
use Exception;
use Prado\Prado;
use Prado\Util\TLogger;
use SoapClient;

/*
 * Created on 31 oct. 2008
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */

class Atexo_Soap_SocleSynchro
{
    const ACCES_BLOQUE = '3';
    const VERSION_DB_2 = '2'; //pme 2.0
    const VERSION_DB_3 = '3'; //pmi3.0
    const VERSION_DB_1 = '1'; //e-bourgogne
    const PREFIXE_SUPPRIME = 'supprime_';
    const POLE = '2';

    /**
     * fonction de mise à jour des organismes.
     *
     * @param $clientSOAP
     */
    public static function updateOrganismes($clientSOAP)
    {
        $arrayWsOrgs = $clientSOAP->getAllOrganismes();
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        if (is_array($arrayWsOrgs)) {
            foreach ($arrayWsOrgs as $wsOrg) {
                $c = new Criteria();
                $c->add(CommonOrganismePeer::ACRONYME, $wsOrg['acronyme']);
                $org = CommonOrganismePeer::doSelectOne($c, $connexionCom);
                if ($org) {
                    $org = CommonOrganismePeer::retrieveByPK($org->getId(), $connexionCom);
                    (new Atexo_Organismes())->remplirOrganisme($org, $wsOrg);
                    $org->save($connexionCom);
                } else {
                    /*$org = new CommonOrganisme();
                    Atexo_Organismes::remplirOrganisme($org, $wsOrg);
                    $moduleOrganisme = new CommonModuleOrganisme();
                    $moduleOrganisme->setOrganisme($wsOrg['acronyme']);
                    $moduleOrganisme->setPresenceElu('1');
                    $org->save($connexionCom);
                    $moduleOrganisme->save($connexionCom);*/
                    echo 'ERROR : Organisme  '.$wsOrg['acronyme']." mis a jour \r\n";
                }
                echo 'SUCCES : Organisme  '.$wsOrg['acronyme']." mis a jour \r\n";
            }
        }
    }

    /**
     * fonction de mise à jour des entreprises.
     *
     * @param $clientSOAP, date $dateLancementCron
     */
    public static function updateEntreprises($clientSOAP, $dateLancementCron)
    {
        $arrayWsEntreprise = $clientSOAP->getAllEntreprises($dateLancementCron);
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));

        if (is_array($arrayWsEntreprise)) {
            foreach ($arrayWsEntreprise as $wsEntreprise) {
                $c = new Criteria();
                $c->add(EntreprisePeer::ID_INITIAL, $wsEntreprise['id']);
                $entreprise = EntreprisePeer::doSelectOne($c, $connexionCom);
                if ($entreprise) {
                    $entreprise = EntreprisePeer::retrieveByPK($entreprise->getId(), $connexionCom);
                } else {
                    $entreprise = new Entreprise();
                }
                Atexo_Entreprise::remplirEntreprise($entreprise, $wsEntreprise);
                $entreprise->save($connexionCom);
                echo 'SUCCES : Entreprise  id initial '.$wsEntreprise['id']." inseree /mis a jour \r\n";
            }
        }
    }

    /**
     * fonction de mise à jour des Agents.
     *
     * @param $clientSOAP, date $dateLancementCron
     */
    public static function updateAgents($clientSOAP, $dateLancementCron)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $listeIdServiceMetier = explode('#', Atexo_Config::getParameter('ID_SERVICE_METIER'));

        foreach ($listeIdServiceMetier as $idServiceMetier) {
            $arrayWsAgent = $clientSOAP->getAllAgents($dateLancementCron, $idServiceMetier);
            if ($arrayWsAgent) {
                foreach ($arrayWsAgent as $wsAgent) {
                    //if(in_array($wsAgent['organisme'], array  ('u9m', 'o3f', 'd4q', 'q9t', 'e9m', 'o3z', 'q3m', 'n1k', 'w7v', 't5s', 'd5r', 't7p', 'e6v', 'a7w', 'z7x', 'g8e', 'g0u', 'p8x', 'd2q', 'l5y'))) {
                    if ($wsAgent['service_id']) {
                        $entity = (new Atexo_EntityPurchase())->retrieveEntityByIdInitial($wsAgent['service_id'], $wsAgent['organisme'], $connexionCom);
                    } else {
                        $entity = new CommonService();
                        $entity->setId('0');
                    }

                    if ($entity) {
                        $c = new Criteria();
                        $c->add(CommonAgentPeer::ID_INITIAL, $wsAgent['id']);
                        $agent = CommonAgentPeer::doSelectOne($c, $connexionCom);
                        if ($agent) {
                            $agent = CommonAgentPeer::retrieveByPK($agent->getId(), $connexionCom);
                        } else {
                            $agent = new CommonAgent();
                        }
                        Atexo_Agent::remplirAgent($agent, $wsAgent, $entity);
                        $profilSocle = Atexo_Agent::recoverProfileAgent($wsAgent);
                        if ($profilSocle != $agent->getIdProfilSocleExterne()) {
                            $agent->setIdProfilSocleExterne($profilSocle);
                            $agent->save($connexionCom);
                            $newHablitation = Atexo_Agent_EnablingsAgents::addOrModifyEnablingsOfTheAgents($agent->getIdProfilSocleExterne(), $agent->getId());
                            $newHablitation->setIdAgent($agent->getId());
                            $newHablitation->save($connexionCom);
                            echo 'SUCCES : Agent  id initial '.$wsAgent['id']." inseree /mis a jour avec habilitation \r\n";
                        } else {
                            $agent->save($connexionCom);
                            echo 'SUCCES : Agent  id initial '.$wsAgent['id']." inseree /mis a jour sans habilitation \r\n";
                        }
                    } else {
                        echo 'ERROR : le service .'.$wsAgent['service_id']." de l'agent ".$wsAgent['email'].' org '.$wsAgent['organisme']." n'a pas été inseré \r\n";
                        Prado::log('Erreur : le service .'.$wsAgent['service_id']." de l'agent ".$wsAgent['email'].' org '.$wsAgent['organisme']." n'a pas été inseré", TLogger::ERROR, 'Atexo.Cli.Cron.SynchroSocleExterne');
                    }
                    //}
                }
            }
        }
    }

    /**
     * fonction de mise à jour des inscrits.
     *
     * @param $clientSOAP, date $dateLancementCron
     */
    public static function updateInscrits($clientSOAP, $dateLancementCron)
    {
        $arrayWsInscrit = $clientSOAP->getAllInscrits($dateLancementCron);
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        if ($arrayWsInscrit) {
            foreach ($arrayWsInscrit as $wsInscrit) {
                $c1 = new Criteria();
                $c1->add(CommonInscritPeer::ID_INITIAL, $wsInscrit['id']);
                $inscrit = CommonInscritPeer::doSelectOne($c1, $connexionCom);

                $c2 = new Criteria();
                $c2->add(EntreprisePeer::ID_INITIAL, $wsInscrit['entreprise_id']);
                $entreprise = EntreprisePeer::doSelectOne($c2, $connexionCom);

                if ($entreprise) {
                    if ($inscrit && $entreprise) {
                        $inscrit = CommonInscritPeer::retrieveByPK($inscrit->getId(), $connexionCom);
                    } else {
                        $inscrit = new CommonInscrit();
                    }
                    Atexo_Entreprise_Inscrit::remplirInscrit($inscrit, $entreprise, $wsInscrit);
                    try {
                        $inscrit->save($connexionCom);
                        echo 'SUCCES : Inscrit id initial .'.$wsInscrit['id']." l'entreprise  ".$wsInscrit['entreprise_id']." a été inseré \r\n";
                    } catch (Exception $e) {
                        $messageErreur = $e->getMessage();
                        echo $messageErreur;
                    }
                } else {
                    echo 'ERROR : Inscrit id initial .'.$wsInscrit['id']." l'entreprise  ".$wsInscrit['entreprise_id']." n'a pas été inseré \r\n";
                }
            }
        }
    }

    /**
     * fonction de suppression des inscrits.
     *
     * @param $clientSOAP
     */
    public static function deleteInscrits($clientSOAP, $dateLancementCron = null)
    {
        $arrayWsInscrit = null;
        try {
            $arrayWsInscrit = $clientSOAP->getInscritsSupprimes($dateLancementCron);
        } catch (Exception) {
            if (is_soap_fault($arrayWsInscrit)) {
                // Debug
                Prado::log('Erreur :'.$arrayWsInscrit->faultcode.' '.$arrayWsInscrit->faultstring.'', TLogger::ERROR, 'Atexo.Cli.Cron.SynchroSocleExterne');
            }
        }

        if (is_array($arrayWsInscrit) && count($arrayWsInscrit)) {
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            foreach ($arrayWsInscrit as $inscritHistorique) {
                $idInscritIntial = $inscritHistorique['id_inscrit_supprime'];
                $c = new Criteria();
                $c->add(CommonInscritPeer::ID_INITIAL, $idInscritIntial);
                $nbrSupprime = CommonInscritPeer::doDelete($c, $connexionCom);
                Prado::log('Suppression de '.$nbrSupprime." element dont id_intial est $idInscritIntial", TLogger::INFO, 'Atexo.Cli.Cron.SynchroSocleExterne');
            }
        }
    }

    /**
     * fonction de mise à jour des services.
     *
     * @param $clientSOAP, date $dateLancementCron
     */
    public static function updateServices($clientSOAP, $dateLancementCron)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $c = new Criteria();
        $arrayLocalOrgs = CommonOrganismePeer::doSelect($c, $connexionCom);
        if ($arrayLocalOrgs) {
            foreach ($arrayLocalOrgs as $localOrg) {
                $arrayWsServices = $clientSOAP->getServicesByAcronyme($localOrg->getAcronyme(), $dateLancementCron);

                if ($arrayWsServices) {
                    foreach ($arrayWsServices as $wsService) {
                        $service = (new Atexo_EntityPurchase())->retrieveEntityByIdInitial($wsService->id, $localOrg->getAcronyme(), $connexionCom);
                        if ($service) {
                            $service = CommonServicePeer::retrieveByPk($service->getId(), $connexionCom);
                        } else {
                            $service = new CommonService();
                            $service->setOrganisme($localOrg->getAcronyme());
                        }
                        (new Atexo_EntityPurchase())->remplirService($service, $wsService);
                        $service->save($connexionCom);
                    }
                }
            }
        }
    }

    /**
     * fonction de suppression des services.
     *
     * @param $clientSOAP
     */
    public function supprimerService($clientSOAP)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $arrayLocalOrgs = CommonOrganismePeer::doSelect($c, $connexionCom);
        if ($arrayLocalOrgs) {
            foreach ($arrayLocalOrgs as $localOrg) {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                $c = new Criteria();
                $c->add(CommonServicePeer::ORGANISME, $localOrg->getAcronyme());
                $arrayLocalService = CommonServicePeer::doSelect($c, $connexion);
                if ($arrayLocalService) {
                    $arrayWsServices = $clientSOAP->getServicesByAcronyme($localOrg->getAcronyme());
                    foreach ($arrayLocalService as $localService) {
                        $trouve = false;
                        if ($arrayWsServices) {
                            foreach ($arrayWsServices as $wsService) {
                                if (($wsService->id == $localService->getIdExterne())) {
                                    $trouve = true;
                                    break;
                                }
                            }
                        }
                        if (false == $trouve) {
                            (new Atexo_EntityPurchase())->deleteEntity($localService->getId(), $localOrg->getAcronyme());
                        }
                    }
                }
            }
        }
    }

    /**
     * fonction de mise à jour des affiliations services.
     *
     * @param $clientSOAP
     */
    public static function updateAffiliationService($clientSOAP)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $c = new Criteria();
        $arrayLocalOrgs = CommonOrganismePeer::doSelect($c, $connexionCom);
        if ($arrayLocalOrgs) {
            foreach ($arrayLocalOrgs as $localOrg) {
                $arrayWsAffiliationServices = $clientSOAP->getAffiliationServiceByAcronyme($localOrg->getAcronyme());

                if ($arrayWsAffiliationServices) {
                    foreach ($arrayWsAffiliationServices as $wsAffiliationService) {
                        $cr1 = new Criteria();
                        $cr1->add(CommonServicePeer::ID_INITIAL, $wsAffiliationService['id_service']);
                        $cr1->add(CommonServicePeer::ORGANISME, $localOrg->getAcronyme());
                        $service = CommonServicePeer::doSelectOne($cr1, $connexionCom);
                        $cr2 = new Criteria();
                        $cr2->add(CommonServicePeer::ID_INITIAL, $wsAffiliationService['id_pole']);
                        $cr2->add(CommonServicePeer::ORGANISME, $localOrg->getAcronyme());
                        $pole = CommonServicePeer::doSelectOne($cr2, $connexionCom);

                        if ($service && $pole) {
                            $affiliationService = CommonAffiliationServicePeer::retrieveByPk($localOrg->getAcronyme(), $pole->getId(), $service->getId(), $connexionCom);
                            if (!($affiliationService instanceof CommonAffiliationService)) {
                                $affiliationService = new CommonAffiliationService();
                                $affiliationService->setOrganisme($localOrg->getAcronyme());
                                $affiliationService->setServiceParentId($pole->getId());
                                $affiliationService->setServiceId($service->getId());
                                $affiliationService->save($connexionCom);
                            }
                        } else {
                            Prado::log(Atexo_Config::getParameter('SERVICE_POLE_UNKNOWN'), TLogger::ERROR, 'Atexo.Cli.Cron.SynchroSocleExterne');
                        }
                    }
                }
            }
        }
    }

    /**
     * fonction de suppression des affiliations services.
     *
     * @param $clientSOAP
     */
    public static function supprimerAffiliationService($clientSOAP)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $c = new Criteria();
        $arrayLocalOrgs = CommonOrganismePeer::doSelect($c, $connexionCom);

        if ($arrayLocalOrgs) {
            foreach ($arrayLocalOrgs as $localOrg) {
                $allServices = '';
                $cr = new Criteria();
                $cr->add(CommonAffiliationServicePeer::ORGANISME, $localOrg->getAcronyme());
                $arrayLocalAffiliationService = CommonAffiliationServicePeer::doSelect($cr, $connexionCom);
                if (is_array($arrayLocalAffiliationService)) {
                    $allAffiLocal = [];
                    foreach ($arrayLocalAffiliationService as $oneLocalAffiliationServices) {
                        $allAffiLocal[] = [$oneLocalAffiliationServices->getIdPole(), $oneLocalAffiliationServices->getIdService()];
                    }
                    $c = new Criteria();
                    $c->add(CommonServicePeer::ORGANISME, $localOrg->getAcronyme());
                    $entityPurchase = CommonServicePeer::doSelect($c, $connexionCom);
                    if (is_array($entityPurchase)) {
                        foreach ($entityPurchase as $oneEntity) {
                            $allServices[$oneEntity->getId()] = $oneEntity;
                        }
                    }
                    $arrayWsAffiliationServices = $clientSOAP->getAffiliationServiceByAcronyme($localOrg->getAcronyme());
                    $allAffiWS = [];
                    if (is_array($arrayLocalAffiliationService)) {
                        foreach ($arrayWsAffiliationServices as $oneWsAffiliationServices) {
                            $allAffiWS[] = [$oneWsAffiliationServices['id_pole'], $oneWsAffiliationServices['id_service']];
                        }
                    }

                    if (is_array($allAffiLocal)) {
                        foreach ($allAffiLocal as $localAffiliationService) {
                            $service = $allServices[$localAffiliationService[1]];
                            $pole = $allServices[$localAffiliationService[0]];
                            if ($allAffiWS && $service && $pole) {
                                if (!in_array([$pole->getIdExterne(), $service->getIdExterne()], $allAffiWS)) {
                                    $AffServAsupprime = CommonAffiliationServicePeer::retrieveByPk($localOrg->getAcronyme(), $pole->getId(), $service->getId(), $connexionCom);
                                    CommonAffiliationServicePeer::doDelete($AffServAsupprime, $connexionCom);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * fonction qui récupère la dernière date du lancement du cron avant son nouveau lancement.
     */
    public static function getLastDateSaved()
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $c = new Criteria();
        $dateLancement = CommonDateLancementCronPeer::doSelectOne($c, $connexionCom);

        if ($dateLancement) {
            $dateLancementCron = $dateLancement->getMiseAJourSocle();
        } else {
            $dateLancementCron = new CommonDateLancementCron();
            $dateLancementCron->setMiseAJourSocle(date('Y-m-d H:i:s', strtotime('-1 day')));
            $dateLancementCron->save($connexionCom);
        }

        return $dateLancementCron;
    }

    /**
     * fonction d'enregistrement de la date du lancement du cron.
     */
    public static function saveDateLancementCron()
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $c = new Criteria();
        $dateLancement = CommonDateLancementCronPeer::doSelectOne($c, $connexionCom);
        if ($dateLancement) {
            $dateLancement = CommonDateLancementCronPeer::retrieveByPK($dateLancement->getId(), $connexionCom);
        } else {
            $dateLancement = new CommonDateLancementCron();
        }
        $dateLancement->setMiseAJourSocle(date('Y-m-d H:i:s'));
        $dateLancement->save($connexionCom);
    }

    public function updateAgentById($idagent)
    {
        $agent = null;
        $listeIdServiceMetier = explode('#', Atexo_Config::getParameter('ID_SERVICE_METIER'));
        foreach ($listeIdServiceMetier as $idServiceMetier) {
            $soapClient = new SoapClient(null, ['location' => Atexo_Config::getParameter('URL_INDEX_SSO_WS'), 'uri' => '', 'trace' => 1, 'exceptions' => 0]);
            // vérification du SSO

            $wsAgent = $soapClient->getAgentById($idagent, $idServiceMetier);

            //print_r($agent_sso_service["id_service_ebrgn"]);exit;
            if (is_soap_fault($wsAgent)) {
                // Debug
                Prado::log('Erreur :'.$wsAgent->faultcode.' '.$wsAgent->faultstring, TLogger::ERROR, 'Atexo.Cli.Cron.SynchroSocleExterne');

                return;
            }
            if (is_array($wsAgent)) {
                $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                $wsAgent = $wsAgent[0];
                if ($wsAgent['service_id']) {
                    $entity = (new Atexo_EntityPurchase())->retrieveEntityByIdInitial($wsAgent['service_id'], $wsAgent['organisme'], $connexionCom);
                } else {
                    $entity = new CommonService();
                    $entity->setId('0');
                }

                if ($entity) {
                    $c = new Criteria();
                    $c->add(CommonAgentPeer::ID_INITIAL, $wsAgent['id']);
                    $agent = CommonAgentPeer::doSelectOne($c, $connexionCom);
                    if ($agent) {
                        $agent = CommonAgentPeer::retrieveByPK($agent->getId(), $connexionCom);
                    } else {
                        $agent = new CommonAgent();
                    }
                    Atexo_Agent::remplirAgent($agent, $wsAgent, $entity);
                    $profilSocle = Atexo_Agent::recoverProfileAgent($wsAgent);
                    if ($profilSocle != $agent->getIdProfilSocleExterne()) {
                        $agent->setIdProfilSocleExterne($profilSocle);
                        $agent->save($connexionCom);
                        $newHablitation = Atexo_Agent_EnablingsAgents::addOrModifyEnablingsOfTheAgents($agent->getIdProfilSocleExterne(), $agent->getId());
                        $newHablitation->setIdAgent($agent->getId());
                        $newHablitation->save($connexionCom);
                    } else {
                        $agent->save($connexionCom);
                    }
                }
            }
        }

        return $agent;
    }

    public function updateInscritById($idInscrit)
    {
        $wsInscrit = [];
        $inscrit = null;
        Prado::log('updateInscritById '.$idInscrit.'', TLogger::INFO, 'Atexo.Soap.SocleSynchro');
        $soapClient = new SoapClient(null, ['location' => Atexo_Config::getParameter('URL_INDEX_SSO_WS'), 'uri' => '', 'trace' => 1, 'exceptions' => 0]);
        // vérification du SSO
        try {
            $wsInscrit = $soapClient->getInscritById($idInscrit);
            if (is_soap_fault($wsInscrit)) {
                // Debug
                echo 'Erreur :'.$wsInscrit->faultcode.' '.$wsInscrit->faultstring;
                Prado::log('Erreur :'.$wsInscrit->faultcode.' '.$wsInscrit->faultstring, TLogger::ERROR, 'Atexo.Soap.SocleSynchro');

                return;
            }
        } catch (Exception $e) {
            if (is_soap_fault($wsInscrit)) {
                // Debug
                Prado::log('Erreur :'.$wsInscrit->faultcode.' '.$wsInscrit->faultstring, TLogger::ERROR, 'Atexo.Soap.SocleSynchro');

                return;
            }
        }
        $wsInscrit = $wsInscrit[0];

        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        if ($wsInscrit) {
            try {
                $wsInscrit['id'] = $idInscrit;
                $c1 = new Criteria();
                $c1->add(CommonInscritPeer::ID_INITIAL, $wsInscrit['id']);
                $inscrit = CommonInscritPeer::doSelectOne($c1, $connexionCom);

                $c2 = new Criteria();
                $c2->add(EntreprisePeer::ID_INITIAL, $wsInscrit['entreprise_id']);
                $entreprise = EntreprisePeer::doSelectOne($c2, $connexionCom);
                if (!($entreprise instanceof Entreprise) && !($entreprise instanceof Entreprise)) {
                    $entreprise = new Entreprise();
                    Atexo_Entreprise::remplirEntreprise($entreprise, $wsInscrit);
                    $entreprise->save($connexionCom);
                }
                if ($entreprise) {
                    if ($inscrit && $entreprise) {
                        $inscrit = CommonInscritPeer::retrieveByPK($inscrit->getId(), $connexionCom);
                    } else {
                        $inscrit = new CommonInscrit();
                    }
                    Atexo_Entreprise_Inscrit::remplirInscrit($inscrit, $entreprise, $wsInscrit);
                    $inscrit->save($connexionCom);
                }
            } catch (\Exception $e) {
                echo 'error '.$e->__toString();

                return;
            }
        }

        return $inscrit;
    }

    public function updateAgentAccesApplication($clientSOAP)
    {
        $dateDebut = date('Y-m-d H:i:s');
        echo "DEBUT : agentServiceMetier \r\n";
        $arrayWsAcces = $clientSOAP->getAgentAccesApplication();
        //print_r($arrayWsAcces); exit;
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        if (is_array($arrayWsAcces)) {
            foreach ($arrayWsAcces as $wsAcces) {
                $c = new Criteria();
                $c->add(CommonAgentServiceMetierPeer::ID_AGENT, $wsAcces['id_agent']);
                $c->add(CommonAgentServiceMetierPeer::ID_SERVICE_METIER, $wsAcces['id_service']);
                $agentServiceMetier = CommonAgentServiceMetierPeer::doSelectOne($c, $connexionCom);
                if (!($agentServiceMetier instanceof CommonAgentServiceMetier)) {
                    $agentServiceMetier = new CommonAgentServiceMetier();
                }
                $agentServiceMetier->setIdAgent($wsAcces['id_agent']);
                $agentServiceMetier->setIdServiceMetier($wsAcces['id_service']);
                $agentServiceMetier->setIdProfilService((new Atexo_Agent())->recoverProfileAgentByAppli($wsAcces));
                $agentServiceMetier->setDateModification(date('Y-m-d H:i:s'));
                $agentServiceMetier->save($connexionCom);
                echo 'SUCCES : agentServiceMetier  id initial '.$wsAcces['id_agent']." inseree /mis a jour \r\n";
            }
        }
        echo 'FIN : agentServiceMetier  '.$dateDebut.' => '.date('Y-m-d H:i:s')."\r\n";
    }

    public function updateEntrepriseManagers($clientSOAP)
    {
        $dateDebut = date('Y-m-d H:i:s');
        echo "DEBUT : updateEntrepriseManagers \r\n";
        $managers = $clientSOAP->getEntrepriseManagers();
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $arrayIdExterneId = Atexo_Entreprise::retrieveIdExterneIdEntrprise();
        $nbrSucces = 0;
        $nbrError = 0;
        if (is_array($managers)) {
            foreach ($managers as $wsManger) {
                if ($arrayIdExterneId[$wsManger['entreprise_id']]) {
                    $c = new Criteria();
                    $c->add(CommonResponsableengagementPeer::ID_INITIAL, $wsManger['id']);
                    $responsablEengagement = CommonResponsableengagementPeer::doSelectOne($c, $connexionCom);
                    if (!($responsablEengagement instanceof CommonResponsableengagement)) {
                        $responsablEengagement = new CommonResponsableengagement();
                    }
                    $responsablEengagement->setIdInitial($wsManger['id']);
                    $responsablEengagement->setEntrepriseId($arrayIdExterneId[$wsManger['entreprise_id']]);
                    $responsablEengagement->setNom(utf8_decode($wsManger['nom']));
                    $responsablEengagement->setPrenom(utf8_decode($wsManger['prenom']));
                    $responsablEengagement->setQualite(utf8_decode($wsManger['qualite']));
                    $responsablEengagement->save($connexionCom);
                    ++$nbrSucces;
                    echo 'SUCCES : responsablEengagement  id initial '.$wsManger['id']." inseree /mis a jour \r\n";
                } else {
                    ++$nbrError;
                    echo 'ERROR : responsablEengagement  id initial '.$wsManger['id'].'  entreprise non trouvee '.$wsManger['entreprise_id']." \r\n";
                }
            }
        }
        echo 'FIN : updateEntrepriseManagers '.$dateDebut.' => '.date('Y-m-d H:i:s').' succes : '.$nbrSucces.'/'.(is_countable($managers) ? count($managers) : 0).' error : '.$nbrError.'/'.(is_countable($managers) ? count($managers) : 0)." \r\n";
    }
}
