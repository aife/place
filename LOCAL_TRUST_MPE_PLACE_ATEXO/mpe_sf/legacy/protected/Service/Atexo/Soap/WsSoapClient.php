<?php

namespace Application\Service\Atexo\Soap;

use Application\Service\Atexo\Atexo_Config;
use SoapClient;
use SoapHeader;
use SoapVar;

class Atexo_Soap_WSSoapClient extends SoapClient
{
    private $username;
    private $password;

    /*Generation du WSSecurity header*/
    public function wssecurity_header()
    {
        $auth =
        '<wsse:Security SOAP-ENV:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
         <wsse:UsernameToken wsu:Id="UsernameToken-1" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
            <wsse:Username>'.Atexo_Config::getParameter('USERNAME_TED').'</wsse:Username>
            <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">'.Atexo_Config::getParameter('PASS_TED').'</wsse:Password>
         </wsse:UsernameToken>
         </wsse:Security>';

        $authvalues = new SoapVar($auth, XSD_ANYXML);
        $header = new SoapHeader(Atexo_Config::getParameter('XSD_NS_SECURITY_TED'), 'Security', $authvalues);

        return $header;
    }

    /* nécessaire si on veut changer d'utilisateur et de mot de passe*/
    public function __setUsernameToken($username, $password)
    {
        $this->username = $username;
        $this->password = $password;
    }

    /* modifie la méthode originale en ajoutant un security header, nécessaire quand on veut ajouter d'autres headers */
    public function __soapCall(
        $function_name,
        $arguments,
        $options = null,
        $input_headers = null,
        &$output_headers = null
    ) {
        $result = parent::__soapCall(
            $function_name,
            $arguments,
            $options,
            $this->wssecurity_header()
        );

        return $result;
    }
}
