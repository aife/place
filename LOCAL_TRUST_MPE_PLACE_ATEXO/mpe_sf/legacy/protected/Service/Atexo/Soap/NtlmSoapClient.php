<?php

namespace Application\Service\Atexo\Soap;

use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_LoggerManager;
use SoapClient;
use SoapFault;

/**
 * Gestion des web services soap avec l'authentification NTLM.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 */
class Atexo_Soap_NtlmSoapClient extends SoapClient
{
    /**
     * Call a url using curl with ntlm auth.
     *
     * @param string $url
     * @param string $data
     *
     * @return string
     *
     * @throws SoapFault on curl connection error
     */
    public function callCurl($url, $data, $action)
    {
        $headers = [
                'PHP SOAP-NTLM Client',
                'Connection: Keep-Alive',
                'User-Agent: PHP-SOAP-CURL',
                'Content-Type: text/xml;charset=UTF-8',
                'SOAPAction: "'.$action.'"',
                'Content-Length: '.strlen($data),
            ];

        $handle = curl_init();
        curl_setopt($handle, CURLOPT_POST, true);
        curl_setopt($handle, CURLOPT_HEADER, false);
        curl_setopt($handle, CURLOPT_URL, $url);
        curl_setopt($handle, CURLOPT_FAILONERROR, false);
        curl_setopt($handle, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($handle, CURLOPT_POSTFIELDS, $data);
        curl_setopt($handle, CURLOPT_USERPWD, Atexo_Config::getParameter('LOGIN_PASSWORD_INTERFACE_CNFPT_AX'));
        curl_setopt($handle, CURLOPT_HTTPAUTH, CURLAUTH_NTLM); //Utilisation de l'authentification NTLM
        curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, true);
        curl_setopt($handle, CURLOPT_SSL_VERIFYHOST, true);
        curl_setopt($handle, CURLINFO_HEADER_OUT, true);

        $response = curl_exec($handle);
        if (empty($response)) {
            throw new SoapFault('CURL error: '.curl_error($handle), curl_errno($handle));
            $logger = Atexo_LoggerManager::getLogger();
            $logger->error('CURL error: '.curl_error($handle));
            $logger->error('CURL num error: '.curl_errno($handle));
        }
        curl_close($handle);

        return $response;
    }

    /*
    * Redefinition de parent::__doRequest
    */
    public function __doRequest($request, $location, $action, $version, $one_way = 0)
    {
        return $this->callCurl($location, $request, $action);
    }

    /*
    * Debugage
    */
    public function soapDebug($client)
    {
        $requestHeaders = $client->__getLastRequestHeaders();
        $request = ($client->__getLastRequest());
        $responseHeaders = $client->__getLastResponseHeaders();
        $response = ($client->__getLastResponse());

        echo '<code>'.nl2br(htmlspecialchars($requestHeaders, true)).'</code>';
        echo highlight_string($request, true)."<br/>\n";

        echo '<code>'.nl2br(htmlspecialchars($responseHeaders, true)).'</code>'."<br/>\n";
        echo highlight_string($response, true)."<br/>\n";
    }
}
