<?php

namespace Application\Service\Atexo\Socle;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonAgentPeer;
use Application\Propel\Mpe\CommonAgentServiceMetier;
use Application\Propel\Mpe\CommonAgentServiceMetierPeer;
use Application\Propel\Mpe\CommonServiceMertierPeer;
use Application\Propel\Mpe\CommonServiceMertierProfilsQuery;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Db;
use PDO;
use Prado\Prado;

/**
 * Page.
 *
 * @author mouslim.mitali@atexo.com
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Socle_AgentServicesMetiers
{

    /**
     * retourne la liste des services disponible à l'agen t authentifié.
     *
     * @param unknown_type $idAgent identifiant de l'agent
     *
     * @return CommonAgentServiceMetierPeer
     */
    public function retreiveListeServiceAgent($idAgent, $withoutSocle = false)
    {
        $connexionCom = Propel::getConnection(
            Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY')
        );
        $c = new Criteria();
        $c->add(CommonAgentServiceMetierPeer::ID_AGENT, $idAgent);
        if ($withoutSocle) {
            $c->add(
                CommonAgentServiceMetierPeer::ID_SERVICE_METIER,
                Atexo_Config::getParameter('SERVICE_METIER_SOCLE'),
                Criteria::NOT_EQUAL
            );
        }
        $c->addJoin(CommonServiceMertierPeer::ID, CommonAgentServiceMetierPeer::ID_SERVICE_METIER);
        $services = CommonServiceMertierPeer::doSelect($c, $connexionCom);
        if ($services) {
            return $services;
        }

        return false;
    }

    public function retrieveServicesMeiersAndProfils($idAgent = null, $withoutSocle = false, $calledFromSocle = true, $org = null
    ){
        $servicesMetiers = [];
        $profils = [];
        $langue = Atexo_CurrentUser::readFromSession('lang');
        if (!$langue || 0 == strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE'))) {
            $libelle = 'libelle';
        } else {
            $libelle = "libelle_$langue";
        }
        $sql = 'Select Service_Mertier.id as idServiceMetier, Service_Mertier.sigle, Service_Mertier.denomination , '
            . " Service_Mertier_Profils.id_interne as idProfil, Service_Mertier_Profils.$libelle,Service_Mertier_Profils.id_externe as idExterne, "
        .' Organisme_Service_Metier.id_service_metier as ServiceActive';
        if ($idAgent) {
            $sql .= ',id_agent as checkedServices';
        }
        $sql .= ' FROM  ((Service_Mertier left join Service_Mertier_Profils on Service_Mertier.id=Service_Mertier_Profils.id_service_metier)'
        ." left join Organisme_Service_Metier on Service_Mertier.id=Organisme_Service_Metier.id_service_metier  and Organisme_Service_Metier.organisme='".$org."')";

        if ($idAgent) {
            $sql .= " left join Agent_Service_Metier on Service_Mertier.id=Agent_Service_Metier.id_service_metier AND id_agent='" . (new Atexo_Db())->quote($idAgent) . "'";
        }
        if (!$calledFromSocle) {
            $sql .= " WHERE Service_Mertier.id='".Atexo_Config::getParameter('SERVICE_METIER_MPE')."'";
            if (!Atexo_CurrentUser::hasHabilitation('HyperAdmin')) {
                // si on a pas l'habilitation hyper-admin, on n'a pas le droit au profil administrateur entité publique
                $sql .= " AND Service_Mertier_Profils.show_profile_for_hyperadmin_only <> '1' ";
            }
        }
        if ($withoutSocle) {
            $sql .= " WHERE Service_Mertier.id !='".Atexo_Config::getParameter('SERVICE_METIER_SOCLE')."'";
        }
        $statement = Atexo_Db::getLinkCommon(true)->query($sql);

        $arrayServicesMetiers = [];
        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            if (!$langue || 0 == strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) || !$row["libelle_$langue"]) {
                $libelleProfil = $row['libelle'];
            } else {
                $libelleProfil = $row["libelle_$langue"];
            }
            //if($idAgent === null) $servicesMetiers['checked'] = true;
            if (!isset($arrayServicesMetiers[$row['idServiceMetier']])) {
                if ($row['checkedServices']) {
                    $servicesMetiers['checked'] = true;
                } else {
                    $servicesMetiers['checked'] = false;
                }
                if ($row['ServiceActive']) {
                    $servicesMetiers['activate'] = true;
                } else {
                    $servicesMetiers['activate'] = false;
                }

                $servicesMetiers['idServiceMetier'] = $row['idServiceMetier'];
                $servicesMetiers['sigle'] = $row['sigle'];
                $servicesMetiers['denomination'] = $row['denomination'];
                $profils = [];
                $profils[0] = Prado::localize('TEXT_SELECTIONNER').'...';
                $profils[$row['idProfil'].'-'.$row['idExterne']] = $libelleProfil;
                $servicesMetiers['profils'] = $profils;
                $arrayServicesMetiers[$row['idServiceMetier']] = $servicesMetiers;
            } else {
                $servicesMetiers['idServiceMetier'] = $row['idServiceMetier'];
                $profils[$row['idProfil'].'-'.$row['idExterne']] = $libelleProfil;
                $servicesMetiers['profils'] = $profils;
                $arrayServicesMetiers[$row['idServiceMetier']] = $servicesMetiers;
            }
        }

        return $arrayServicesMetiers;
    }

    /**
     * retourne la liste des services de l'agent.
     *
     * @param int $idAgent
     * @param int $idService
     *
     * @return CommonAgentServiceMetier
     */
    public static function retrieveServiceAgentByIdAgentByService($idAgent, $idService)
    {
        $connexionCom = Propel::getConnection(
            Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY')
        );
        $c = new Criteria();
        $c->add(CommonAgentServiceMetierPeer::ID_AGENT, $idAgent);
        $c->add(CommonAgentServiceMetierPeer::ID_SERVICE_METIER, $idService);
        $serviceAgent = CommonAgentServiceMetierPeer::doSelect($c, $connexionCom);
        if ($serviceAgent) {
            return $serviceAgent;
        }

        return false;
    }

    public function retrieveServicesOrganisme($idAgent, $organisme, $withoutSocle = false)
    {
        $servicesMetiers = [];
        $arrayServicesMetiers = Atexo_CurrentUser::readFromSession('servicesMetiersAgent');
        //si les données ne sont pas en session alors on les récupère
        if (!$arrayServicesMetiers) {
            $sql = 'SELECT Service_Mertier.id as idServiceMetier, Service_Mertier.sigle, Service_Mertier.denomination , Service_Mertier.url_acces as  url_acces, Service_Mertier.logo as logo';
            if ($idAgent) {
                $sql .= ',id_agent as checkedServices,id_profil_service ';
            }
            $sql .= ' FROM (Service_Mertier ';
            if ($idAgent) {
                $sql .= " left join Agent_Service_Metier on Service_Mertier.id=Agent_Service_Metier.id_service_metier AND id_agent='" .(new Atexo_Db())->quote($idAgent). "'), Organisme_Service_Metier";
                $sql .= ' WHERE  Service_Mertier.id=Organisme_Service_Metier.id_service_metier ';
            }
            if ($withoutSocle) {
                $sql .= " AND Service_Mertier.id !='".Atexo_Config::getParameter('SERVICE_METIER_SOCLE')."'";
            }
            $sql .= ' order By ordre ASC ';
            $statement = Atexo_Db::getLinkCommon(true)->query($sql);
            $arrayServicesMetiers = [];
            $index = 0;
            while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                if (!isset($arrayServicesMetiers[$row['idServiceMetier']])) {
                    if ($row['checkedServices']) {
                        $servicesMetiers['activate'] = true;
                        $servicesMetiers['url_acces'] = $row['url_acces'];
                    } else {
                        $servicesMetiers['activate'] = false;
                        $servicesMetiers['url_acces'] = '';
                    }
                    $servicesMetiers['idServiceMetier'] = $row['idServiceMetier'];
                    $servicesMetiers['sigle'] = $row['sigle'];
                    $servicesMetiers['index'] = $index++;
                    $servicesMetiers['denomination'] = $row['denomination'];
                    $servicesMetiers['logo'] = $row['logo'];
                    $servicesMetiers['idProfilService'] = $row['id_profil_service'];
                    $arrayServicesMetiers[$row['idServiceMetier']] = $servicesMetiers;
                }
            }
            Atexo_CurrentUser::writeToSession('servicesMetiersAgent', $arrayServicesMetiers);
        }

        return $arrayServicesMetiers;
    }

    /*
     * Retourne la liste des services metiers
     */
    public function retrieveServicesByOrganisme($org, $calledFromSocle = true)
    {
        $servicesMetiers = [];
        $sql = 'Select Service_Mertier.id as idServiceMetier, Service_Mertier.sigle, Service_Mertier.denomination , '
        .' Organisme_Service_Metier.id_service_metier as ServiceActive';

        $sql .= ' FROM  (Service_Mertier'
        ." left join Organisme_Service_Metier on Service_Mertier.id=Organisme_Service_Metier.id_service_metier  and Organisme_Service_Metier.organisme='".$org."')";

        if (!$calledFromSocle) {
            $sql .= " WHERE Service_Mertier.id='".Atexo_Config::getParameter('SERVICE_METIER_MPE')."'";
        }

        $statement = Atexo_Db::getLinkCommon(true)->query($sql);

        $arrayServicesMetiers = [];
        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            if (!isset($arrayServicesMetiers[$row['idServiceMetier']])) {
                if ($row['ServiceActive']) {
                    $servicesMetiers['checked'] = true;
                } else {
                    $servicesMetiers['checked'] = false;
                }
                if (!$calledFromSocle) {
                    if ($row['idServiceMetier'] == Atexo_Config::getParameter('SERVICE_METIER_MPE')) {
                        $servicesMetiers['activate'] = false;
                        $servicesMetiers['checked'] = true;
                    } else {
                        $servicesMetiers['activate'] = true;
                    }
                } else {
                    if ($row['idServiceMetier'] == Atexo_Config::getParameter('SERVICE_METIER_SOCLE')) {
                        $servicesMetiers['activate'] = false;
                        $servicesMetiers['checked'] = true;
                    } else {
                        $servicesMetiers['activate'] = true;
                    }
                }
                $servicesMetiers['idServiceMetier'] = $row['idServiceMetier'];
                $servicesMetiers['sigle'] = $row['sigle'];
                $servicesMetiers['denomination'] = $row['denomination'];
                $arrayServicesMetiers[$row['idServiceMetier']] = $servicesMetiers;
            } else {
                $servicesMetiers['idServiceMetier'] = $row['idServiceMetier'];
                $arrayServicesMetiers[$row['idServiceMetier']] = $servicesMetiers;
            }
        }

        return $arrayServicesMetiers;
    }

    /*
     * Retourne la liste des services metiers pour un agent
     */
    public function retreiveListeServicePourUnAgent($idAgent, $withoutSocle = false)
    {
        $connexionCom = Propel::getConnection(
            Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY')
        );
        $c = new Criteria();
        $c->add(CommonAgentServiceMetierPeer::ID_AGENT, $idAgent);
        if ($withoutSocle) {
            $c->add(
                CommonAgentServiceMetierPeer::ID_SERVICE_METIER,
                Atexo_Config::getParameter('SERVICE_METIER_SOCLE'),
                Criteria::NOT_EQUAL
            );
        }
        $servicesMetiers = CommonAgentServiceMetierPeer::doSelect($c, $connexionCom);
        if ($servicesMetiers) {
            return $servicesMetiers;
        }
    }

    /**
     * retourne la liste des agents.
     *
     * @param unknown_type $org       l'organisme
     * @param unknown_type $idService id service metier
     *
     * @return CommonAgentServiceMetier
     */
    public function retriveListeAgentsByOrgServiceMetier($org, $idService, $connexion = null)
    {
        if (!$connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        }
        $c = new Criteria();
        $c->addJoin(CommonAgentServiceMetierPeer::ID_AGENT, CommonAgentPeer::ID);
        $c->add(CommonAgentPeer::ORGANISME, $org);
        $c->add(CommonAgentServiceMetierPeer::ID_SERVICE_METIER, $idService);
        $agentServicesMetiers = CommonAgentServiceMetierPeer::doSelect($c, $connexion);

        return $agentServicesMetiers;
    }

    /**
     * Permet de retourner le service metier profil.
     *
     * @param int $idServiceMetier l'id service metier
     * @param int $idProfilService l'id profil service
     *
     * @return CommonSeriviceMertier l'objet s'il existe si non false
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function retrieveServiceMetierProfil($idServiceMetier, $idProfilService)
    {
        $connexion = Propel::getConnection(
            Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY')
        );
        $profilIdExterne = CommonServiceMertierProfilsQuery::create()
            ->filterByIdServiceMetier($idServiceMetier)
            ->filterByIdInterne($idProfilService)
            ->findOne($connexion);
        if ($profilIdExterne) {
            return $profilIdExterne;
        }

        return false;
    }
}
