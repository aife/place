<?php

namespace Application\Service\Atexo\Socle;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonServiceMertier;
use Application\Propel\Mpe\CommonServiceMertierPeer;
use Application\Propel\Mpe\CommonSsoAgentPeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Db;
use PDO;

/**
 * Page d'accueil non authentifié des agents.
 *
 * @author
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Socle_Authentification
{
    /**
     * retourne un objet agent identifié par le sso.
     *
     * @param  $ssoAgent
     *
     * @return CommonServiceMertier
     */
    public function retrieveSsoAgentBySso($ssoAgent, $idAgent = null)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        if ($idAgent) {
            $c->add(CommonSsoAgentPeer::ID_AGENT, $idAgent);
        } else {
            $c->add(CommonSsoAgentPeer::ID_SSO, $ssoAgent);
        }
        $c->addJoin(CommonServiceMertierPeer::ID, CommonSsoAgentPeer::SERVICE_ID);
        $services = CommonServiceMertierPeer::doSelectOne($c, $connexionCom);
        if ($services) {
            return $services;
        } else {
            return false;
        }
    }

    /**
     * retourne un tableau qui contient le login et l'organisme de l'agent identifié par le sso.
     *
     * @param  $ssoAgent
     *
     * @return $array
     */
    public function retrieveAgentBySso($ssoAgent)
    {
        $sql = "Select Agent.login,Agent.organisme from Agent, sso_agent where Agent.id = sso_agent.id_agent and sso_agent.id_sso = '".(new Atexo_Db())->quote($ssoAgent)."'";
        $statement = Atexo_Db::getLinkCommon(true)->query($sql);
        $array = [];
        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            $array['login'] = $row['login'];
            $array['organisme'] = $row['organisme'];
        }

        return $array;
    }

    public function deleteSsoByIdAgent($idAgent)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $c = new Criteria();
        $c->add(CommonSsoAgentPeer::ID_AGENT, $idAgent);
        CommonSsoAgentPeer::doDelete($c, $connexionCom);
    }
}
