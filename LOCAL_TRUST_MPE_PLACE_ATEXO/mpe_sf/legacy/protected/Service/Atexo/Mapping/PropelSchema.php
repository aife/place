<?php

namespace Application\Service\Atexo\Mapping;

/*
 * Created on 31 mars 2008
 * @author:adil el kanabi
 * @package:bin

 */
class Atexo_Mapping_PropelSchema
{
    /**
     * cette fonction ajoute, pour chaque balise "table", dans le fichier xml transmis en paramètre l'attribut "phpName"
     * tel que attribut.
     *
     * @author : adil el kanabi
     *
     * @param : $shemaFilePath : chemin du fichier shema.xml
     * @param : $prefix        : valeur à ajouter en prefix au nom de la table
     */
    public function addPhpName($shemaFilePath, $prefix)
    {
        //si le fichier schema.xml transmis en paramètre existe
        if (file_exists($shemaFilePath)) {
            //le fichiers xml est chargé
            $xml = simplexml_load_file($shemaFilePath);
            $indiceTabXml = 0;
            //pour chaque balise "<table>"
            $table = '';
            foreach ($xml->table as $table) {
                //on ajoute un attribut "phpName" composé du mot "Common" concatener avec la valeur de l'attribut de name après avoir
                //supprimer les caractères "_" et transformer en majuscule le caractère suivant l'underscore
                $phpName = $prefix;
                $valueOfPhpNameAttributes = explode('_', $xml->table[$indiceTabXml]['name']);
                foreach ($valueOfPhpNameAttributes as $elementOfPhpNameAttributes) {
                    $phpName .= strtoupper(substr($elementOfPhpNameAttributes, 0, 1)).substr($elementOfPhpNameAttributes, 1);
                }
                $xml->table[$indiceTabXml]['phpName'] = $phpName;
                ++$indiceTabXml;
            }
            $contentXml = $xml->asXML();
            $contentXml = str_replace('defaultIdMethod="native"', 'defaultIdMethod="native" namespace="Application\Propel\Mpe" ', $contentXml);
            $contentXml = str_replace('<table name="EnchereReference" ', '<table name="EnchereReference" allowPkInsert="true" ', $contentXml);
            $contentXml = str_replace('<table name="InterneConsultationSuiviSeul" ', '<table name="InterneConsultationSuiviSeul" allowPkInsert="true" ', $contentXml);
            $contentXml = str_replace('<table name="InterneConsultation" ', '<table name="InterneConsultation" allowPkInsert="true" ', $contentXml);
            $contentXml = str_replace('<table name="Annonce" ', '<table name="Annonce" allowPkInsert="true" ', $contentXml);
            $contentXml = str_replace('<table name="Entreprise" phpName="CommonEntreprise"', '<table name="Entreprise" phpName="Entreprise"', $contentXml);
            $contentXml = str_replace('<foreign-key foreignTable="CommonEntreprise"', '<foreign-key foreignTable="Entreprise"', $contentXml);

            $heritageContrat = '<column name="contrat_class_key" phpName="ContratClassKey" type="INTEGER" size="1" required="true" defaultValue="1" inheritance="single">
									<inheritance key="1" class="CommonTContratTitulaire"/>
									<inheritance key="2" class="CommonTContratMulti" extends="CommonTContratTitulaire"/>
						 		 </column>';
            $contentXml = str_replace('<column name="contrat_class_key" phpName="ContratClassKey" type="INTEGER" size="1" required="true" defaultValue="1"/>', $heritageContrat, $contentXml);

            //on ecrase le contenu de l'ancien schema.xm par le nouveau
            if (!$handle = fopen($shemaFilePath, 'w')) {
                echo "Impossible d'ouvrir le fichier (".$shemaFilePath.')';

                return;
            }
            if (false === fwrite($handle, $contentXml)) {
                echo "Impossible d'écrire dans le fichier  (".$shemaFilePath.')';

                return;
            }
        } else {
            echo "Echec lors de l\'ouverture du fichier".$shemaFilePath;
        }
    }

    /**
     * Remplace les attributs SET NULL, CASCADE et RESTRICT par set null, cascade et restrict.
     *
     * @param $shemaFilePath : chemin du fichier shema.xml
     *
     * @author Omar TANTAOUI (omar.tantaoui@atexo.com)
     */
    public function replaceCapsChars($shemaFilePath)
    {
        //si le fichier schema.xml transmis en paramètre existe
        if (!file_exists($shemaFilePath)) {
            echo "Echec lors de l\'ouverture du fichier".$shemaFilePath;

            return;
        }

        //le fichiers xml est chargé
        $xml = simplexml_load_file($shemaFilePath);
        $tagFk = 'foreign-key';
        foreach ($xml->table as $table) {
            if ($table->$tagFk) {
                foreach ($table->$tagFk as $fk) {
                    $fkAttributes = $fk->attributes();
                    if ($fkAttributes) {
                        if ('' != $fkAttributes['onDelete']) {
                            $onDeleteAttr = $fkAttributes['onDelete'];
                            $fkAttributes['onDelete'] = strtolower($onDeleteAttr);
                        }
                        if ('' != $fkAttributes['onUpdate']) {
                            $onUpdateAttr = $fkAttributes['onUpdate'];
                            $fkAttributes['onUpdate'] = strtolower($onUpdateAttr);
                        }
                    }
                }
            }

            // Patch pour les types DATE et TIMESTAMP, remplacés par BU_TIMESTAMP et BU_DATE
            $tagColumn = 'column';
            if ($table->$tagColumn) {
                foreach ($table->$tagColumn as $Column) {
                    $colAttributes = $Column->attributes();
                    if ($colAttributes) {
                        if ('' != $colAttributes['type']) {
                            $typeAttr = $colAttributes['type'];
                            if (0 == strcasecmp($typeAttr, 'TIMESTAMP')) {
                                $colAttributes['type'] = 'BU_TIMESTAMP';
                            }
                            if (0 == strcasecmp($typeAttr, 'DATE')) {
                                $colAttributes['type'] = 'BU_DATE';
                                unset($colAttributes['default']);
                            }
                        }
                        if ('' != $colAttributes['defaultValue']) {
                            $typeAttr = $colAttributes['defaultValue'];
                            if (0 == strcasecmp($typeAttr, 'current_timestamp()')) {
                                $colAttributes['defaultValue'] = 'now';
                            }
                        }
                    }
                }
            }
        }
        //on ecrase le contenu de l'ancien schema.xm par le nouveau
        if (!$handle = fopen($shemaFilePath, 'w')) {
            echo "Impossible d'ouvrir le fichier (".$shemaFilePath.')';

            return;
        }
        if (false === fwrite($handle, $xml->asXML())) {
            echo "Impossible d'écrire dans le fichier  (".$shemaFilePath.')';

            return;
        }
    }
}
