<?php

namespace Application\Service\Atexo;

use PDO;

class Atexo_Multizone
{
    /*
     * fonction de convertion de la date serveur en date zone
     * @parameter : $date_server : date du serveur
     * @return : $date_zone : date de la zone
     */
    public function server2zone($date_server, $format = 'Y-m-d H:i:s', $organisme = '')
    {
        if (strstr($date_server, '/')) {
            $date_server = Atexo_Util::frnDateTime2iso($date_server);
        }
        $offset = self::getOffsetServer2Zone($date_server, $organisme);
        $timestamp_server = strtotime($date_server);
        $timestamp_zone = $timestamp_server + ($offset * 3600);
        $date_zone = date($format, $timestamp_zone);

        return $date_zone;
    }

    /*
     * fonction de convertion de la date zone en date serveur
     * @return : $date_server : date du serveur
     * @parameter : $date_zone : date de la zone
     */
    public function zone2server($date_zone, $format = 'Y-m-d H:i:s', $organisme = '')
    {
        if (strstr($date_zone, '/')) {
            $date_zone = Atexo_Util::frnDateTime2iso($date_zone);
        }
        $offset = self::getOffsetZone2Server($date_zone, $organisme);
        $timestamp_zone = strtotime($date_zone);
        $timestamp_server = $timestamp_zone + ($offset * 3600);

        return date($format, $timestamp_server);
    }

    /*
     * fonction permettant de calculer de décalage entre l'heure serveur et l'heure zone
     * @parameter : $date_server : date du serveur
     * @return : $date_zone : $offset : décalage entre la date serveur et la date
     */
    public function getOffsetServer2Zone($date_server, $organisme)
    {
        $module = new Atexo_Module();

        if ($module->isEnabled('MultiZone')) {
            $decalage = 0;
            $decalage = self::sql_get_decalage_horaire($date_server, $organisme);

            return $decalage;
        } else {
            return 0;
        }
    }

    /*
     * fonction permettant de calculer de décalage entre l'heure zone et l'heure serveur
     * @parameter : $date_server : date du serveur
     * @return : $offset : décalage entre la date zone et la date serveur
     */
    public function getOffsetZone2Server($date_zone, $organisme)
    {
        return -self::getOffsetServer2Zone($date_zone, $organisme);
    }

    /** convertit une date française en date ISO.
     * @param $_frnDate date au format dd/mm/yyyy
     *
     * @return date au format yyyy-mm-dd
     */
    public function sql_get_decalage_horaire($date, $organisme)
    {
        $org = '';
        if ('' == $organisme) {
            $org = Atexo_CurrentUser::getOrganismAcronym();
        } else {
            $org = $organisme;
        }

        $sql = ' select C.decalage, O.offset from changement_heure as C,Organisme as O';
        $sql .= ' where C.organisme = O.acronyme ';
        $sql .= "and C.date_debut_zone <= '".(new Atexo_Db())->quote($date)."' ";
        $sql .= " and C.date_fin_zone > '".(new Atexo_Db())->quote($date)."' ";
        $sql .= "and C.organisme ='".(new Atexo_Db())->quote($org)."' ";
        //$stmt2 = $connexionCOM->createStatement();
        //$zones = $stmt2->executeQuery($sql, ResultSet::FETCHMODE_NUM);
        $stmt2 = Atexo_Db::getLinkCommon(true)->prepare($sql);
        $stmt2->execute();
        $zones = $stmt2->fetchAll(PDO::FETCH_NUM);

        $decalage_horaire = [];
        $i = 0;

        while ($zones->next()) {
            $decalage_horaire[$i]['decalage'] = $zones->getString(1);
            $decalage_horaire[$i]['offset'] = $zones->getString(2);
            ++$i;
        }
        $decalage = $decalage_horaire[0]['decalage'] + ($decalage_horaire[0]['offset']);

        return $decalage;
    }

    public function server2zone_get_all_consultation($array_cons)
    {
        // conversion des dates
        for ($index_cons = 0; $index_cons < (is_countable($array_cons) ? count($array_cons) : 0); ++$index_cons ) {
            if (0 != strcmp($array_cons[$index_cons]['datemiseenligne'], '0000-00-00 00:00:00')) {
                $array_cons[$index_cons]['datemiseenligne'] =
                strtotime(self::server2zone($array_cons[$index_cons]['datemiseenligne']));
            }

            if (0 != strcmp($array_cons[$index_cons]['datevalidation'], '0000-00-00 00:00:00')) {
                $array_cons[$index_cons]['datevalidation'] =
                strtotime(self::server2zone($array_cons[$index_cons]['datevalidation']));
            }

            $array_cons[$index_cons]['unix_datefin'] =
            strtotime(self::server2zone($array_cons[$index_cons]['unix_datefin']));

            $array_cons[$index_cons]['unix_datefin_sad'] =
            strtotime(self::server2zone($array_cons[$index_cons]['unix_datefin_sad']));

            $array_cons[$index_cons]['datefin'] =
            Atexo_Util::iso2frnDateTime(self::server2zone($array_cons[$index_cons]['datefin']));

            $array_cons[$index_cons]['datefin_sad'] =
            Atexo_Util::iso2frnDateTime(self::server2zone($array_cons[$index_cons]['datefin_sad']));

            $array_cons[$index_cons]['cmpdatefin'] =
            Atexo_Util::iso2frnDateTime(self::server2zone($array_cons[$index_cons]['cmpdatefin']));
        }

        return $array_cons;
    }
}
