<?php

namespace Application\Service\Atexo;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonAutresPiecesMiseDispositionPeer;

/**
 * Classe de manipulation des consultations.
 *
 * @author Khadija Chouika <khadija.chouika@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 2.1
 *
 * @since MPE-4.0
 */
class Atexo_AutresPiecesMiseDisposition
{
    public function getAllPieces($attributaire, $destinataire, $org)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonAutresPiecesMiseDispositionPeer::ID_DESTINATAIRE, $destinataire);
        $c->add(CommonAutresPiecesMiseDispositionPeer::ORG, $org);
        $c->add(CommonAutresPiecesMiseDispositionPeer::ID_DECISION_ENVELOPPE, $attributaire);
        $pieces = CommonAutresPiecesMiseDispositionPeer::doSelect($c, $connexion);
        if ($pieces) {
            return $pieces;
        } else {
            return false;
        }
    }

    public function getAutrePieceById($id)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonAutresPiecesMiseDispositionPeer::ID, $id);
        $piece = CommonAutresPiecesMiseDispositionPeer::doSelectone($c, $connexion);
        if ($piece) {
            return $piece;
        } else {
            return false;
        }
    }
}
