<?php

namespace Application\Service\Atexo;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonTrancheArticle133;
use Application\Propel\Mpe\CommonTrancheArticle133Peer;
use Application\Propel\Mpe\CommonTrancheArticle133Query;

/**
 * commentaires.
 *
 * @author Khadija CHOUIKA <khadija.chouika@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 3.0
 *
 * @since MPE-4.0
 */
class Atexo_TrancheBudgetaire
{
    public function retrieveTrancheBudgetaireByOrgAnne($annee, $organisme, $defautValue = null, $encog = null)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonTrancheArticle133Peer::ACRONYME_ORG, $organisme);
        $c->add(CommonTrancheArticle133Peer::MILLESIME, $annee);
        $arrayTranches = CommonTrancheArticle133Peer::doSelect($c, $connexion);
        $arrayReturn = [];
        if ($defautValue) {
            if ($encog) {
                $defautValue = Atexo_Util::toUtf8($defautValue);
            }
            $arrayReturn[0] = $defautValue;
        }
        if ($encog) {
            foreach ($arrayTranches as $uneValeur) {
                $arrayReturn[$uneValeur->getId()] = Atexo_Util::toUtf8($uneValeur->getLibelleTrancheBudgetaire());
            }
        } else {
            foreach ($arrayTranches as $uneValeur) {
                $arrayReturn[$uneValeur->getId()] = $uneValeur->getLibelleTrancheBudgetaire();
            }
        }

        return $arrayReturn;
    }

    public function retrieveTrancheBudgetaireById($id, $org, $annee = null, $objet = null)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonTrancheArticle133Peer::ACRONYME_ORG, $org); // pour s'assure comme la misgration peut donner des erreurs
        $c->add(CommonTrancheArticle133Peer::ID, $id);
        if ($annee) {
            $c->add(CommonTrancheArticle133Peer::MILLESIME, $annee);
        }
        $objetTranche = CommonTrancheArticle133Peer::doSelectOne($c, $connexion);
        $objes = [];
        if (!$objet) {
            if ($objetTranche) {
                $objes['BorneInf'] = $objetTranche->getBorneInf();
                if ($objetTranche->getBorneSup()) {
                    $objes['BorneSup'] = $objetTranche->getBorneSup();
                } else {
                    $objes['BorneSup'] = 0;
                }

                return $objes;
            }
        }

        return $objetTranche;
    }

    public function retrieveTrancheBudgetaireByOrgLibelleAnnee($org, $libelle, $annee)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonTrancheArticle133Peer::ACRONYME_ORG, $org);
        $c->add(CommonTrancheArticle133Peer::MILLESIME, $annee);
        $c->add(CommonTrancheArticle133Peer::LIBELLE_TRANCHE_BUDGETAIRE, $libelle);
        $objetTranche = CommonTrancheArticle133Peer::doSelectOne($c, $connexion);
        if ($objetTranche) {
            return $objetTranche;
        }

        return false;
    }

    /**
     * test  si un montant est entre ces deux bornes superieur
     * est inferieur
     * a revoir avec  pour id tranche
     * l'ajout de l'annee et $org c'est pour s'assurer que la tranche est correct.
     */
    public function isMontantBetweenBorne($montant, $idTranche, $annee, $org)
    {
        $result = false;
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $montant = Atexo_Util::formatterMontant($montant, 2, true);
        $c = new Criteria();
        $c->add(CommonTrancheArticle133Peer::ID, $idTranche);
        $c->add(CommonTrancheArticle133Peer::ACRONYME_ORG, $org);
        $c->add(CommonTrancheArticle133Peer::MILLESIME, $annee);
        $objetTranche = CommonTrancheArticle133Peer::doSelectOne($c, $connexion);
        if ($objetTranche && $montant) {
            $borneInf = Atexo_Util::formatterMontant($objetTranche->getBorneInf(), 2, true);
            $borneSup = Atexo_Util::formatterMontant($objetTranche->getBorneSup(), 2, true);
            if (!$borneSup) {
                if ($montant >= $borneInf) {
                    $result = true;
                }
            } elseif (($montant >= $borneInf) && ($montant <= $borneSup)) {
                $result = true;
            }
        }

        return $result;
    }

    /**
     * Permet de retourner le libe.
     *
     * @param $id
     * @param $org
     *
     * @return string
     */
    public function getLibelleTrancheById($id, $org)
    {
        $tranche = self::retrieveTrancheBudgetaireById($id, $org, null, true);
        if ($tranche instanceof CommonTrancheArticle133) {
            return (new Atexo_Config())->toPfEncoding($tranche->getLibelleTrancheBudgetaire());
        }

        return '';
    }

    public function getIdTrancheByMontantAndAnnee($montant, $annee, $org, $conx = null)
    {
        $trancheQuery = new CommonTrancheArticle133Query();
        $tranches = $trancheQuery->getTrancheByOrganisme($annee, $org, $conx);
        if ($tranches) {
            foreach ($tranches as $objetTranche) {
                $borneInf = Atexo_Util::formatterMontant($objetTranche->getBorneInf(), 2, true);
                $borneSup = Atexo_Util::formatterMontant($objetTranche->getBorneSup(), 2, true);
                if (!$borneSup) {
                    if ($montant >= $borneInf) {
                        return $objetTranche->getId();
                    }
                } elseif (($montant >= $borneInf) && ($montant <= $borneSup)) {
                    return $objetTranche->getId();
                }
            }
        }

        return '';
    }

    public function getIdTrancheByMontant($montant, $org, $conx = null)
    {
        $trancheQuery = new CommonTrancheArticle133Query();
        $tranches = $trancheQuery->getLastTrancheByOrganisme($org);
        if ($tranches) {
            foreach ($tranches as $objetTranche) {
                $borneInf = Atexo_Util::formatterMontant($objetTranche->getBorneInf(), 2, true);
                $borneSup = Atexo_Util::formatterMontant($objetTranche->getBorneSup(), 2, true);
                if (!$borneSup) {
                    if ($montant >= $borneInf) {
                        return $objetTranche->getId();
                    }
                } elseif (($montant >= $borneInf) && ($montant <= $borneSup)) {
                    return $objetTranche->getId();
                }
            }
        }
    }
}
