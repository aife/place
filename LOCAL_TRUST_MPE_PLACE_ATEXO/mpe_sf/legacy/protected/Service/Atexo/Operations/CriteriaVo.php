<?php

namespace Application\Service\Atexo\Operations;

/**
 * Vo pour définir les critères de Atexo_Operations_Operations::search().
 *
 * @author khadija CHOUIKA <khadija.chouika@atexo.com>
 * @copyright Atexo 2014
 *
 * @version 6.0
 *
 * @since MPE-4.0
 */
class Atexo_Operations_CriteriaVo
{
    private string $idOperation = '';
    private string $acronyme = '';
    private ?string $id_service = '';
    private string $id_agent = '';
    private string $type = '';
    private string $categorie = '';
    private string $code = '';
    private string $description = '';
    private string $annee_debut = '';
    private string $annee_fin = '';
    private string $budget = '';
    private string $supprime = '';
    private bool $searchModeExact = false;
    private string $keywordSearch = '';
    private string $statut = '';
    private string $sensTri = '';
    private string $sortByElement = '';
    private string $refOperation = '';
    private string $dateCreation = '';
    private string $dateModification = '';
    private bool $organisationCentralisee = false;

    public function getAcronyme()
    {
        return $this->acronyme;
    }

    public function setAcronyme($value)
    {
        $this->acronyme = $value;
    }

    public function getIdService()
    {
        return $this->id_service;
    }

    public function setIdService($value)
    {
        $this->id_service = $value;
    }

    public function getIdAgent()
    {
        return $this->id_agent;
    }

    public function setIdAgent($value)
    {
        $this->id_agent = $value;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setType($value)
    {
        $this->type = $value;
    }

    public function getCategorie()
    {
        return $this->categorie;
    }

    public function setCategorie($value)
    {
        $this->categorie = $value;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function setCode($value)
    {
        $this->code = $value;
    }

    public function getAnneeDebut()
    {
        return $this->annee_debut;
    }

    public function setAnneeDebut($value)
    {
        $this->annee_debut = $value;
    }

    public function getAnneeFin()
    {
        return $this->annee_fin;
    }

    public function setAnneeFin($value)
    {
        $this->annee_fin = $value;
    }

    public function getBudget()
    {
        return $this->budget;
    }

    public function setBudget($value)
    {
        $this->budget = $value;
    }

    public function getSupprime()
    {
        return $this->supprime;
    }

    public function setSupprime($value)
    {
        $this->supprime = $value;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($value)
    {
        $this->description = $value;
    }

    public function getSearchModeExact()
    {
        return $this->searchModeExact;
    }

    public function setSearchModeExact($boolean)
    {
        $this->searchModeExact = $boolean;
    }

    public function getkeywordSearch()
    {
        return $this->keywordSearch;
    }

    public function setkeywordSearch($value)
    {
        $this->keywordSearch = $value;
    }

    public function getStatut()
    {
        return $this->statut;
    }

    public function setStatut($value)
    {
        $this->statut = $value;
    }

    public function getSensTri()
    {
        return $this->sensTri;
    }

    public function setSensTri($value)
    {
        $this->sensTri = $value;
    }

    public function getOffset()
    {
        return $this->offset;
    }

    public function setOffset($value)
    {
        $this->offset = $value;
    }

    public function getLimit()
    {
        return $this->limit;
    }

    public function setLimit($value)
    {
        $this->limit = $value;
    }

    public function getSensOrderBy()
    {
        return $this->sensOrderBy;
    }

    public function setSensOrderBy($value)
    {
        $this->sensOrderBy = $value;
    }

    public function getSortByElement()
    {
        return $this->sortByElement;
    }

    public function setSortByElement($value)
    {
        $this->sortByElement = $value;
    }

    public function getRefOperation()
    {
        return $this->refOperation;
    }

    public function setRefOperation($value)
    {
        $this->refOperation = $value;
    }

    public function getDateCreation()
    {
        return $this->dateCreation;
    }

    public function setDateCreation($value)
    {
        $this->dateCreation = $value;
    }

    public function getDateModification()
    {
        return $this->dateModification;
    }

    public function setDateModification($value)
    {
        $this->dateModification = $value;
    }

    public function getOrganisationCentralisee()
    {
        return $this->organisationCentralisee;
    }

    public function setOrganisationCentralisee($value)
    {
        $this->organisationCentralisee = $value;
    }

    public function getIdOperation()
    {
        return $this->idOperation;
    }

    public function setIdOperation($value)
    {
        $this->idOperation = $value;
    }
}
