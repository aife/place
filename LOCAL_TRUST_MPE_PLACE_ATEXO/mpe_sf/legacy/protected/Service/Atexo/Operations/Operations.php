<?php

namespace Application\Service\Atexo\Operations;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonOperations;
use Application\Propel\Mpe\CommonOperationsPeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Util;
use Exception;
use PDO;

/**
 * Classe de manipulation des operations.
 *
 * @author Khadija CHOUIKA  <khadija.chouika@atexo.com>
 * @copyright Atexo 2014
 *
 * @version 6.0
 *
 * @since MPE-4.0
 */
class Atexo_Operations_Operations
{
    /*
     * Methode de recherche
     * $Param un objet criteria repmlit par les critères
     */
    public function search(Atexo_Operations_CriteriaVo $criteriaVo, $nombreElement = false)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $c = new Criteria();
        if ($criteriaVo->getRefOperation()) {
            $c->add(CommonOperationsPeer::ID_OPERATION, $criteriaVo->getRefOperation(), Criteria::EQUAL);
        }
        if ($criteriaVo->getAcronyme()) {
            $c->add(CommonOperationsPeer::ACRONYME, $criteriaVo->getAcronyme(), Criteria::EQUAL);
        }
        if ($criteriaVo->getIdAgent()) {
            $c->add(CommonOperationsPeer::ID_AGENT, $criteriaVo->getIdAgent(), Criteria::EQUAL);
        }
        if ($criteriaVo->getType()) {
            $c->add(CommonOperationsPeer::TYPE, $criteriaVo->getType(), Criteria::EQUAL);
        }
        if ($criteriaVo->getCategorie()) {
            $c->add(CommonOperationsPeer::CATEGORIE, $criteriaVo->getCategorie(), Criteria::EQUAL);
        }
        if ($criteriaVo->getAnneeDebut()) {
            $c->add(CommonOperationsPeer::ANNEE_DEBUT, $criteriaVo->getAnneeDebut(), Criteria::EQUAL);
        }
        if ($criteriaVo->getAnneeFin()) {
            $c->add(CommonOperationsPeer::ANNEE_FIN, $criteriaVo->getAnneeFin(), Criteria::EQUAL);
        }
        if ($criteriaVo->getBudget()) {
            $c->add(CommonOperationsPeer::BUDGET, $criteriaVo->getBudget(), Criteria::EQUAL);
        }
        if ($criteriaVo->getSupprime()) {
            $c->add(CommonOperationsPeer::SUPPRIME, $criteriaVo->getSupprime(), Criteria::EQUAL);
        }
        //service and Sous services
        self::setService($criteriaVo, $c);
        //By KeyWord
        self::setKeyWordCritere($criteriaVo, $c);
        // set statut
        self::setStatutOperation($criteriaVo, $c);

        if ($criteriaVo->getLimit()) {
            $c->setLimit($criteriaVo->getLimit());
        }
        if ($criteriaVo->getOffset()) {
            $c->setOffset($criteriaVo->getOffset());
        }
        //sort By
        self::setSortAndOrderBy($criteriaVo, $c);
        $operations = CommonOperationsPeer::doSelect($c, $connexion); //print_r($connexion);
        if (!$operations) {
            $operations = [];
        }
        if ($nombreElement) {
            return count($operations);
        }

        return $operations;
    }

    /*
     * Permet set Service et sous service
     */
    public function setService($criteriaVo, &$c)
    {
        if (null != $criteriaVo->getIdService()) {
            if ($criteriaVo->getOrganisationCentralisee()) {
                $arrayIds = (new Atexo_EntityPurchase())->getArrayIdServiceWithChild($criteriaVo->getIdService(), $criteriaVo->getAcronyme());
                $c->add(CommonOperationsPeer::SERVICE_ID, $arrayIds, Criteria::IN);
            } else {
                $c->add(CommonOperationsPeer::SERVICE_ID, $criteriaVo->getIdService());
            }
        }
    }

    /*
     * Permet set KeyWord
     */
    public function setKeyWordCritere($criteriaVo, &$c)
    {
        $arrayMots = [];
        //fonction non terminer
        $chaine = '';
        if ($criteriaVo->getkeywordSearch() && $criteriaVo->getSearchModeExact()) {
            $chaine = str_replace('%', '', $criteriaVo->getkeywordSearch());
            $crit0 = $c->getNewCriterion(CommonOperationsPeer::CODE, $chaine, Criteria::LIKE);
            $crit1 = $c->getNewCriterion(CommonOperationsPeer::DESCRIPTION, $chaine, Criteria::LIKE);
            $crit0->addOr($crit1);
            $c->add($crit0);
        } elseif ($criteriaVo->getkeywordSearch()) {
            $arrayMotsCle = Atexo_Util::deleteSpecialCharacteresFromChaine($criteriaVo->getkeywordSearch());
            $arrayMotInterdit = ' and or et ou le la les un une du de des a à au aux son sa ses ne ni non sauf ce ces cet je tu il elle on nous vous ils elles etc mon ma ton ta vos se y en sur ';
            foreach ($arrayMotsCle as $unMotCle) {
                if ($unMotCle && !strstr($arrayMotInterdit, (string) $unMotCle) && strlen($unMotCle) >= 2) {
                    $arrayMots[] = $unMotCle;
                }
            }
            if ($arrayMots) {
                $chaine = implode('%', $arrayMots);
            }
            $crit0 = $c->getNewCriterion(CommonOperationsPeer::CODE, '%'.$chaine.'%', Criteria::LIKE);
            $crit1 = $c->getNewCriterion(CommonOperationsPeer::DESCRIPTION, '%'.$chaine.'%', Criteria::LIKE);
            $crit0->addOr($crit1);
            $c->add($crit0);
        }
    }

    /*
     * Permet de Set Statut (en cours / achevee)
     */
    public function setStatutOperation($criteriaVo, &$c)
    {
        if ($criteriaVo->getStatut()) {
            if ($criteriaVo->getStatut() == Atexo_Config::getParameter('STATUT_OPERATION_EN_COURS')) {
                if ($criteriaVo->getAnneeFin()) {
                    $c->addAnd(CommonOperationsPeer::ANNEE_FIN, date('Y'), Criteria::GREATER_EQUAL);
                } else {
                    $c->add(CommonOperationsPeer::ANNEE_FIN, date('Y'), Criteria::GREATER_EQUAL);
                }
            } elseif ($criteriaVo->getStatut() == Atexo_Config::getParameter('STATUT_OPERATION_ACHEVEE')) {
                if ($criteriaVo->getAnneeFin()) {
                    $c->addAnd(CommonOperationsPeer::ANNEE_FIN, date('Y'), Criteria::LESS_THAN);
                } else {
                    $c->add(CommonOperationsPeer::ANNEE_FIN, date('Y'), Criteria::LESS_THAN);
                }
            }
        }
    }

    /*
     * Permet de Set Sort et Order By
     */
    public function setSortAndOrderBy($criteriaVo, &$c)
    {
        if ($criteriaVo->getSortByElement()) {
            $clause = '';
            if ('budget' == $criteriaVo->getSortByElement()) {
                $clause = CommonOperationsPeer::BUDGET;
            } elseif ('code' == $criteriaVo->getSortByElement()) {
                $clause = CommonOperationsPeer::CODE;
            } elseif ('type' == $criteriaVo->getSortByElement()) {
                $clause = CommonOperationsPeer::TYPE;
            } elseif ('categorie' == $criteriaVo->getSortByElement()) {
                $clause = CommonOperationsPeer::CATEGORIE;
            } elseif ('annee_debut' == $criteriaVo->getSortByElement()) {
                $clause = CommonOperationsPeer::ANNEE_DEBUT;
            } elseif ('annee_fin' == $criteriaVo->getSortByElement()) {
                $clause = CommonOperationsPeer::ANNEE_FIN;
            }
            if ('ASC' == $criteriaVo->getSensOrderBy()) {
                $c->addAscendingOrderByColumn($clause);
            } elseif ('DESC' == $criteriaVo->getSensOrderBy()) {
                $c->addDescendingOrderByColumn($clause);
            }
        }
    }

    /*
     * Permet de retourner une operation par son Pk (id operation)
     * ou plusieurs ids
     */
    public function retrieveByRefOperation($idOperation, $ids = null)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $c = new Criteria();
        if ($ids) {
            $c->add(CommonOperationsPeer::ID_OPERATION, $ids, Criteria::IN);
            $operations = CommonOperationsPeer::doSelect($c, $connexion);
        } else {
            $c->add(CommonOperationsPeer::ID_OPERATION, $idOperation);
            $operations = CommonOperationsPeer::doSelectOne($c, $connexion);
        }
        if ($operations) {
            return $operations;
        } else {
            return false;
        }
    }

    /*
     * Permet de supprimer un Objet operation
     * Param  id Operation
     */
    public function deleteOperation($idOperation)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));

        return CommonOperationsPeer::doDelete($idOperation, $connexion);
    }

    /*
     * Permet de recuperer une liste d'operations (auto completion)
     * param Mot clé
     * param Id service
     * retun un array
     */
    public function retreiveListeOperations($keyword, $idservice, $org, $childService)
    {
        try {
            $word = trim(utf8_decode($keyword));
            $arrayMotInterdit = ' and or et ou le la les un une du de des a à au aux son sa ses ne ni non sauf ce ces cet je tu il elle on nous vous ils elles etc mon ma ton ta vos se y en sur ';
            if (!strstr($arrayMotInterdit, $word)) {
                $word = '%'.Atexo_Util::replaceArrayCharactersByPourcentagage($word).'%';
                $results = self::retreiveOperations($word, $idservice, $org, $childService);
                if ((is_countable($results) ? count($results) : 0) > 0) {
                    return $results;
                }
            }

            return false;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    /*
     * Permet de recuperer une operations
     * param Mot clé
     * param Id service
     * return array Operations
     */
    public function retreiveOperations($keyWord, $idservice, $org, $sousService)
    {
        $arrayIds = [];
        $operations = [];
        if ('' != $keyWord) {
            if ($sousService) {
                $arrayIds = (new Atexo_EntityPurchase())->getArrayIdServiceWithChild($idservice, $org);
            } else {
                $arrayIds[] = $idservice;
            }
            $sql = "SELECT `annee_debut`,`annee_fin`,code, `description`, concat(id_operation,concat('#',code))  as id
					FROM `Operations` WHERE `id_service` in ('".(new Atexo_Db())->quote(implode("','", $arrayIds))."') AND `acronyme`='".(new Atexo_Db())->quote($org)
                    ."' AND (`code` like '".(new Atexo_Db())->quote($keyWord)."' OR `description` like '".(new Atexo_Db())->quote($keyWord)."')
					ORDER BY `code`";
            $statement = Atexo_Db::getLinkCommon(true)->prepare($sql);
            $statement->execute();
            $operations = $statement->fetchAll(PDO::FETCH_ASSOC);
        }

        return $operations;
    }

    /*
     * Permet de Create et mette a jour
     *
     */
    public function createUpdateOperarion(Atexo_Operations_CriteriaVo $criteriaVo, $operation = false)
    {
        try {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            if (!$operation instanceof CommonOperations) {
                $operation = new CommonOperations();
            }
            if ($criteriaVo->getAcronyme()) {
                $operation->setAcronyme($criteriaVo->getAcronyme());
            }
            if ($criteriaVo->getIdService()) {
                $operation->setServiceId($criteriaVo->getIdService());
            }
            if ($criteriaVo->getIdAgent()) {
                $operation->setIdAgent($criteriaVo->getIdAgent());
            }
            if ($criteriaVo->getDateModification()) {
                $operation->setDateModification($criteriaVo->getDateModification());
            }
            if ($criteriaVo->getDateCreation()) {
                $operation->setDateCreation($criteriaVo->getDateCreation());
            }
            if ($criteriaVo->getType()) {
                $operation->setType($criteriaVo->getType());
            }
            if ($criteriaVo->getCategorie()) {
                $operation->setCategorie($criteriaVo->getCategorie());
            }
            if ($criteriaVo->getCode()) {
                $operation->setCode($criteriaVo->getCode());
            }
            if ($criteriaVo->getDescription()) {
                $operation->setDescription($criteriaVo->getDescription());
            }
            if ($criteriaVo->getAnneeDebut()) {
                $operation->setAnneeDebut($criteriaVo->getAnneeDebut());
            }
            if ($criteriaVo->getAnneeFin()) {
                $operation->setAnneeFin($criteriaVo->getAnneeFin());
            }
            if ($criteriaVo->getBudget()) {
                $budget = Atexo_Util::formatterMontant(Atexo_Util::atexoHtmlEntities($criteriaVo->getBudget()));
                $operation->setBudget($budget);
            }
            $operation->save($connexion);

            return $operation->getIdOperation();
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
}
