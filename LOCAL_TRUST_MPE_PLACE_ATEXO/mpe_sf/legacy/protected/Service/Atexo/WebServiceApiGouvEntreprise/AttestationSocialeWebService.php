<?php

namespace Application\Service\Atexo\WebServiceApiGouvEntreprise;

use Application\Service\Atexo\Atexo_Util;

/**
 * Class AttestationSocialeWebService.
 *
 * @see https://api.apientreprise.fr/docs/1.0/v1-attestations_sociales.html
 *
 * @author David Delobel <david.delobel@atexo.com>
 */
class AttestationSocialeWebService extends WebService
{
    /**
     * AttestationSocialeWebService constructor.
     *
     * @param string     $identifiant   : siren ou siret
     * @param Parameters $host          : domaine
     * @param string     $token         : token
     * @param array      $paramsTypeDoc : parametres du type de document
     * @param Logger     $logger        : logger
     *
     * @throws \Exception si le type d'attestation n'est pas bon
     *
     * @author David Delobel <david.delobel@atexo.com>
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    final public function __construct($identifiant, $host, $token, $paramsTypeDoc, $logger)
    {
        if (!in_array($paramsTypeDoc['listeParamsUri']['type_attestation'], ['AMP_UR', 'AVG_UR'])) {
            throw new \Exception("Le type d'attestation sociale demandée n'a pas été correctement défini : ce paramètre est obligatoire");
        }
        $parameters = new Parameters();
        if (is_array($paramsTypeDoc['listeParamsUri']) && !empty($paramsTypeDoc['listeParamsUri'])) {
            foreach ($paramsTypeDoc['listeParamsUri'] as $key => $value) {
                $parameters->add($key, true, null, Atexo_Util::toUtf8($value));
            }
        }

        $response_status_codes = new HttpStatusCodes();
        $response_status_codes
            ->add(200, 'Succès', true)
            ->add(401, "Non autorisé : Le jeton d'authentification est manquant")
            ->add(422, "Une erreur s'est produite lors de la résolution de l'attestation ou les paramètres ne sont pas valides")
            ->add(503, "La plateforme ACOSS n'est pas disponible actuellement")
            ->add(400, 'Bad request')
            ->add(404, "L'attestation sociale n'est pas disponible")
            ->add(500, 'Erreur interne au serveur')
            ->add(502, 'Erreur : Bad Gateway')
        ;

        parent::__construct($host.$paramsTypeDoc['uri'].$identifiant, $parameters, $response_status_codes);
        $this->logger = $logger;
        $this->paramsTypeDoc = $paramsTypeDoc;
    }
}
