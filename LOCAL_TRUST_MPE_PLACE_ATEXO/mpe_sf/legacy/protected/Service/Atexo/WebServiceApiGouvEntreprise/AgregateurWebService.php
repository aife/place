<?php

namespace Application\Service\Atexo\WebServiceApiGouvEntreprise;

/**
 * Class DocumentWebService.
 *
 * @see https://api.apientreprise.fr/docs/1.0/v1-opqibi-certificat.html
 */
class AgregateurWebService extends WebService
{
    /**
     * AgregateurWebService constructor.
     *
     * @param string     $identifiant   : siren ou siret
     * @param Parameters $host          : domaine
     * @param string     $token         : token
     * @param array      $paramsTypeDoc : parametres du type de document
     * @param Logger     $logger        : logger
     *
     * @throws \Exception si le type de document n'est pas bon
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    final public function __construct($identifiant, $host, $token, $paramsTypeDoc, $logger)
    {
        $parameters = new Parameters();
        if (is_array($paramsTypeDoc['listeParamsUri']) && !empty($paramsTypeDoc['listeParamsUri'])) {
            foreach ($paramsTypeDoc['listeParamsUri'] as $key => $value) {
                $parameters->add($key, true, null, \Atexo_Util::toUtf8($value));
            }
        }

        $response_status_codes = new HttpStatusCodes();
        $response_status_codes
            ->add(200, 'Succès', true)
            ->add(401, 'Non autorisé : le jeton est manquant ou invalide')
            ->add(400, "L'identifiant n'est pas valide")
            ->add(404, "L'entreprise ou l'etablissement n'est pas connu(e)")
            ->add(500, 'Erreur interne au serveur')
            ->add(503, 'Service indisponible');

        parent::__construct($host.$paramsTypeDoc['uri'].$identifiant, $parameters, $response_status_codes);
        $this->logger = $logger;
        $this->paramsTypeDoc = $paramsTypeDoc;
        $this->typeWebService = 'wsAgregateur';
    }
}
