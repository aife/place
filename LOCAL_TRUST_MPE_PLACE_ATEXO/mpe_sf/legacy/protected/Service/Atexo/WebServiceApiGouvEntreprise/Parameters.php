<?php

namespace Application\Service\Atexo\WebServiceApiGouvEntreprise;

/**
 * Class Parameters.
 *
 * @author David Delobel <david.delobel@atexo.com>
 */
class Parameters
{
    private array $params = [];

    /**
     * @param $field
     * @param bool $is_required
     * @param null $default
     * @param null $current
     *
     * @return $this
     *
     * @throws \Exception
     */
    final public function add($field, $is_required = false, $default = null, $current = null)
    {
        if (key_exists($field, $this->params)) {
            throw new \Exception("Le champ d'URL '$field' existe déjà");
        }
        $this->params[$field] = [
            'is_required' => $is_required,
            'default' => $default,
            'current' => $current, ];

        return $this;
    }

    /**
     * @param bool $include_optional_parameters
     *
     * @return array
     *
     * @throws \Exception
     */
    private function getQueryData($include_optional_parameters = false)
    {
        $query_data = [];
        foreach ($this->params as $field => $value) {
            $is_undefined = (null === $value['current'] and null === $value['default']);
            // Vérifier les champs obligatoires
            if ($value['is_required'] and $is_undefined) {
                throw new \Exception("Le champ obligatoire '$field' n'a pas été défini");
            }
            // Si non inclus, ignorer les paramètres optionnels
            if (!$include_optional_parameters and !$value['is_required']) {
                continue;
            }
            // Ignorer les paramètres optionnels non définis
            if (!$value['is_required'] and $is_undefined) {
                continue;
            }
            // Créer une paire champ-valeur
            $query_data[$field] = is_null($value['current']) ? $value['default'] : $value['current'];
        }

        return $query_data;
    }

    /**
     * @param bool $include_optional_parameters
     *
     * @return string
     *
     * @throws \Exception
     */
    final public function getQueryString($include_optional_parameters = false)
    {
        return http_build_query($this->getQueryData($include_optional_parameters));
    }
}
