<?php

namespace Application\Service\Atexo\WebServiceApiGouvEntreprise;

use App\Traits\ConfigProxyTrait;
use Application\Service\Atexo\Atexo_Config;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use GuzzleHttp\Psr7\Response;

abstract class WebService
{
    use ConfigProxyTrait;

    protected array $paramsTypeDoc = [];
    protected $logger = null;
    /**
     * Les valeurs possibles sont:
     *     => wsAgregateur : pour le ws agregateur document
     *     => wsDocument : pour les autres web service
     *     => wsEntreprise : pour le ws entreprise
     *     => wsEtablissement : pour le ws etablissement.
     */
    protected ?string $typeWebService = 'wsDocument';
    private ?\Application\Service\Atexo\WebServiceApiGouvEntreprise\Parameters $parameters = null;
    private ?\GuzzleHttp\Client $client = null;
    /**
     * @var Response
     */
    private $response = null;
    private ?\Application\Service\Atexo\WebServiceApiGouvEntreprise\HttpStatusCodes $responseStatusCodes = null;

    /**
     * WebService constructor.
     *
     * @param string          $base_uri            base d'URL de l'API
     * @param Parameters      $parameters          paramètres à envoyer lors de la requête
     * @param HttpStatusCodes $responseStatusCodes liste des status codes renvoyés par l'API
     *
     * @author David Delobel <david.delobel@atexo.com>
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function __construct($base_uri, Parameters $parameters, HttpStatusCodes $responseStatusCodes)
    {
        $this->parameters = $parameters;
        $config = ['base_uri' => $base_uri];
        $params = [];
        if ('' != Atexo_Config::getParameter('URL_PROXY')) {
            $params['URL_PROXY'] = Atexo_Config::getParameter('URL_PROXY');
            $params['PORT_PROXY'] = Atexo_Config::getParameter('PORT_PROXY');
        }

        if ($configProxy = self::getConfigProxyForGuzzle(null, $params)) {
            $config = array_merge($config, $configProxy);
        }
        $this->client = new Client($config);
        $this->responseStatusCodes = $responseStatusCodes;
    }

    /**
     * Ajouter un paramètre pour l'envoi de la requête.
     *
     * @param string $field       Nom du champ
     * @param bool   $is_required true si le champ est obligatoire pour l'appel à l'API
     * @param mixed  $default     valeur par défaut envoyée si la valeur current n'est pas définie
     * @param mixed  $current     valeur à envoyer, la valeur par défaut est utilisée si celle-ci n'est pas définie
     *
     * @throws \Exception si le champ est déjà défini dans les paramètres
     *
     * @author David Delobel <david.delobel@atexo.com>
     */
    final public function addParameter($field, $is_required = false, $default = null, $current = null)
    {
        $this->parameters->add($field, $is_required, $default, $current);
    }

    /**
     * Envoyer la requête après sa préparation éventuelle.
     *
     * @param string $relative_url
     * @param string $method       POST, GET, PUT, DELETE...
     * @param null   $headers      Headers à envoyer lors de la requête
     * @param null   $body         Corps du message à envoyer lors de la requête
     * @param array  $options      Options supplémentaires pour Guzzle
     *
     * @return WebService $this
     *
     * @author David Delobel <david.delobel@atexo.com>
     */
    final public function request($relative_url, $method = 'GET', $headers = null, $body = null, $options = [])
    {
        $relative_url_with_query_string = $relative_url . '?' . $this->parameters->getQueryString();

        try {
            $this->logger->info(
                "Appel du ws avec la methode '$method' et avec URL relative = '$relative_url_with_query_string'"
            );
            if (
                Atexo_Config::getParameter('SEARCH_SF_ENTREPRISE')
                || Atexo_Config::getParameter('SEARCH_SF_AGENT')
            ) {
                if (null === $headers) {
                    $headers = [];
                }
                if (null === $body) {
                    $body = [];
                }

                $this->response = $this->client->request(
                    $method,
                    $relative_url_with_query_string,
                    [
                        'headers' => $headers,
                    ]
                );
            } else {
                $this->response = $this->client->$method(
                    $relative_url_with_query_string,
                    [$method, $headers, $body, $options]
                );
            }
        } catch (BadResponseException $e) {
            $this->logger->Error(
                "ERREUR lors de l'appel du WS (BadResponseException) : Reponse: " . print_r($e->getResponse(), true)
            );
            $this->response = $e->getResponse();
        } catch (\Exception $e) {
            $this->logger->Error("ERREUR TECHNIQUE lors de l'appel du WS. Exception: " . $e->getMessage());
            $this->response = new Response(500);
        } finally {
            return $this;
        }
    }

    /**
     * Renvoie le corps de la réponse suite à la request.
     *
     * @param bool $as_string true si le corps de la requête doit être sous forme de string et non d'objet
     *
     * @return \Guzzle\Http\EntityBodyInterface|string
     *
     * @author David Delobel <david.delobel@atexo.com>
     */
    final public function getBody($as_string = false)
    {
        return $this->response->getBody();
    }

    /**
     * Permet de recuperer le contenu de la reponse.
     *
     * @return string : contenu de la reponse
     *
     * @author Oumar KONATE <oumar.konate@atexo.com
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016>
     */
    final public function getContentResponse()
    {
        $stream = $this->response->getBody();

        return $stream->getContents();
    }

    /**
     * Permet de gerer le retour du web service
     * Dans les retours, en cas d'erreur:
     *    ===> le 2e element du tableau contient le code d'erreur
     *    ===> le 1e element du tableau contient les details de l'erreur.
     *
     * @param array $reponse : la reponse du web service
     *
     * @return array|int : un tableau contenant la reponse ou les erreurs eventuelles
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     * @author Gregory CHEVRET <gregory.chevret@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function traiterRetourWs($statut, $reponse, $jeton): array|int
    {
        if (200 == (int) $statut) {
            if (isset($reponse['reponse']['gateway_error']) && true === $reponse['reponse']['gateway_error']) {
                $this->logger->error('Gateway error');
                $return = [$reponse, $statut, $jeton]; //Gateway error
            } else {
                $this->logger->info($this->paramsTypeDoc['code'] . ' trouvee');
                $url = $reponse['data']['document_url'] ?? null;
                $return = [$url, $statut, $jeton];
            }
        } else {
            $this->logger->error('Erreur : ' . $this->getErrorReason());
            $return = [$reponse, $statut, $jeton];
        }

        return $return;
    }

    /**
     * Obtenir le message lié à l'erreur dans le cas d'un non-succès.
     *
     * @return string la description de l'erreur
     *
     * @throws \Exception si la réponse a été un succès
     *
     * @author David Delobel <david.delobel@atexo.com>
     */
    public function getErrorReason()
    {
        if ($this->isSuccessful()) {
            throw new \Exception("Impossible d'obtenir l'erreur car la requête s'est effectuée avec succès.");
        }

        return $this->responseStatusCodes->getDescription($this->getStatusCode());
    }

    /**
     * Savoir si la réponse est succès.
     *
     * @return bool true si la réponse est un succès
     *
     * @throws \Exception si le code HTTP de la réponse n'a pas été indiqué dans $responseStatusCodes
     *
     * @author David Delobel <david.delobel@atexo.com>
     */
    public function isSuccessful()
    {
        return $this->responseStatusCodes->isSuccess($this->getStatusCode());
    }

    /**
     * Obtenir le status code HTTP de la réponse.
     *
     * @return int le status code de la réponse
     *
     * @author David Delobel <david.delobel@atexo.com>
     */
    final public function getStatusCode()
    {
        return $this->response->getStatusCode();
    }

    /**
     * Permet de traiter le retour du WS Agregateur.
     *
     * @param string $reponseWsJson : la reponse du WS encode en json
     *
     * @return mixed
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     * @author Gregory CHEVRET <gregory.chevret@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function traiterRetourWsAgregateur($reponseWsJson)
    {
        return json_decode($reponseWsJson, true, 512, JSON_THROW_ON_ERROR);
    }
}
