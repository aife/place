<?php

namespace Application\Service\Atexo\WebServiceApiGouvEntreprise;

use Application\Service\Atexo\Atexo_Util;

/**
 * Class ExtraitsRcsWebService.
 *
 * @see https://api.apientreprise.fr/docs/1.0/v1-infogreffe-extraits_rcs.html
 */
class ExtraitsRcsWebService extends WebService
{
    /**
     * ExtraitsRcsWebService constructor.
     *
     * @param string     $identifiant   : siren ou siret
     * @param Parameters $host          : domaine
     * @param string     $token         : token
     * @param array      $paramsTypeDoc : parametres du type de document
     * @param Logger     $logger        : logger
     *
     * @throws \Exception si le type d'attestation n'est pas bon
     *
     * @author David Delobel <david.delobel@atexo.com>
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    final public function __construct($identifiant, $host, $token, $paramsTypeDoc, $logger)
    {
        $parameters = new Parameters();
        if (is_array($paramsTypeDoc['listeParamsUri']) && !empty($paramsTypeDoc['listeParamsUri'])) {
            foreach ($paramsTypeDoc['listeParamsUri'] as $key => $value) {
                $parameters->add($key, true, null, Atexo_Util::toUtf8($value));
            }
        }

        $response_status_codes = new HttpStatusCodes();
        $response_status_codes
            ->add(200, 'Succès', true)
            ->add(400, 'Requête incorrecte')
            ->add(401, 'Non autorisé : le jeton est manquant ou invalide')
            ->add(404, 'Attestation non trouvée')
            ->add(422, 'Erreur lors de la résolution du WS, ou paramètres manquants')
            ->add(500, 'Erreur interne au serveur')
            ->add(503, 'Service temporairement indisponible ou en maintenance');

        parent::__construct($host.$paramsTypeDoc['uri'].$identifiant, $parameters, $response_status_codes);
        $this->logger = $logger;
        $this->paramsTypeDoc = $paramsTypeDoc;
    }
}
