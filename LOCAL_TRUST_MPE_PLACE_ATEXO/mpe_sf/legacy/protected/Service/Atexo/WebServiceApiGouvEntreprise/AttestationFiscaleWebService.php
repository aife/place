<?php

namespace Application\Service\Atexo\WebServiceApiGouvEntreprise;

use Application\Service\Atexo\Atexo_Util;

/**
 * Class AttestationFiscaleWebService.
 *
 * @see https://api.apientreprise.fr/docs/1.0/v1-entreprises.html
 *
 * @author David Delobel <david.delobel@atexo.com>
 */
class AttestationFiscaleWebService extends WebService
{
    /**
     * AttestationFiscaleWebService constructor.
     *
     * @param string     $identifiant   : siren ou siret
     * @param Parameters $host          : domaine
     * @param string     $token         : token
     * @param array      $paramsTypeDoc : parametres du type de document
     * @param Logger     $logger        : logger
     *
     * @throws \Exception si le type d'attestation n'est pas bon
     *
     * @author David Delobel <david.delobel@atexo.com>
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    final public function __construct($identifiant, $host, $token, $paramsTypeDoc, $logger)
    {
        $parameters = new Parameters();
        if (is_array($paramsTypeDoc['listeParamsUri']) && !empty($paramsTypeDoc['listeParamsUri'])) {
            foreach ($paramsTypeDoc['listeParamsUri'] as $key => $value) {
                $parameters->add($key, true, null, Atexo_Util::toUtf8($value));
            }
        }

        $response_status_codes = new HttpStatusCodes();
        $response_status_codes
            ->add(200, 'Succès', true)
            ->add(401, "Le couple email/password est invalide ou bien le jeton d'authentification est manquant")
            ->add(422, "Une erreur s'est produite lors de la résolution de l'attestation, ou des paramètres sont manquants")
            ->add(400, "Le SIREN n'est pas valide")
            ->add(404, "L'attestation fiscale n'existe pas")
            ->add(500, 'Erreur interne au serveur')
            ->add(503, 'Service indisponible')
        ;

        parent::__construct($host.$paramsTypeDoc['uri'].$identifiant, $parameters, $response_status_codes);
        $this->logger = $logger;
        $this->paramsTypeDoc = $paramsTypeDoc;
    }
}
