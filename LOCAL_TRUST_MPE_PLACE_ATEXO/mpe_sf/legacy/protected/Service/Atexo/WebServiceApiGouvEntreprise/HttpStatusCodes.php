<?php

namespace Application\Service\Atexo\WebServiceApiGouvEntreprise;

/**
 * Class HttpStatusCodes.
 *
 * Représente une collection de status codes HTTP ainsi que leur description.
 *
 * @author David Delobel <david.delobel@atexo.com>
 */
class HttpStatusCodes
{
    /**
     * @var array La collection de status codes
     */
    private array $http_status_codes = [];

    /**
     * Ajouter un status code à la collection.
     *
     * @param int    $code        numéro de status code HTTP (1xx - 5xx)
     * @param string $description description associée au code (ex. : le token est manquant)
     * @param bool   $is_success  si le status représente un succès
     *
     * @return HttpStatusCodes $this
     *
     * @throws \Exception si le code est déjà défini dans la collection
     *
     * @author David Delobel <david.delobel@atexo.com>
     */
    final public function add($code, $description, $is_success = false)
    {
        if (key_exists($code, $this->http_status_codes)) {
            throw new \Exception("Le code $code a déjà été défini");
        }
        $this->http_status_codes[$code] = [
            'description' => $description,
            'is_success' => $is_success,
        ];
        //ksort($this->http_status_codes, SORT_NUMERIC); // Trier les codes dans l'ordre à chaque ajout (facultatif)
        return $this;
    }

    /**
     * Obtenir la description d'un status code de la collection.
     *
     * @param int $code le status code HTTP
     *
     * @return string la description du code
     *
     * @throws \Exception si le code n'est pas défini
     *
     * @author David Delobel <david.delobel@atexo.com>
     */
    final public function getDescription($code)
    {
        if (!key_exists($code, $this->http_status_codes)) {
            throw new \Exception("Le code $code n'est pas défini");
        }

        return $this->http_status_codes[$code]['description'];
    }

    /**
     * Savoir si le code est un code de type success.
     *
     * @param int $code le status code HTTP
     *
     * @return bool
     *
     * @throws \Exception si le code n'est pas défini
     *
     * @author David Delobel <david.delobel@atexo.com>
     */
    final public function isSuccess($code)
    {
        if (!key_exists($code, $this->http_status_codes)) {
            throw new \Exception("Le code $code n'est pas défini");
        }

        return $this->http_status_codes[$code]['is_success'];
    }
}
