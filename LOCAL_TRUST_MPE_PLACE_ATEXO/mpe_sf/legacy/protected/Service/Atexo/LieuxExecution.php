<?php

namespace Application\Service\Atexo;

/**
 * Classe de generation de l'URL de la page des lieux d'execution.
 *
 * @author Khadija CHOUIKA <khalid.benamar@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 4.0
 *
 * @since MPE-3.0
 */
class Atexo_LieuxExecution
{
    /*
     * fct return l'url de la popup des lieux d'execution
     */
    public static function getLieuExecutionUrl($param = '', $selectedLoc = false)
    {
        $param = Atexo_Util::atexoHtmlEntities($param);
        $selectedLoc = Atexo_Util::atexoHtmlEntities($selectedLoc);

        $fileLocation = 'Commun';
        if (Atexo_CurrentUser::isAgent()) {
            $fileLocation = 'Agent';
        }
        $sessionLang = Atexo_CurrentUser::readFromSession('lang');

        if (Atexo_Module::isEnabled('UtiliserPageHtmlLieuxExecution')) {
            if (Atexo_Module::isEnabled('LieuxExecutionCarte')) {
                return "javascript:popUpSetSize('/pages/{$fileLocation}.LieuxExecutionCarte."
                    . $sessionLang . ".htm','850px','850','yes');";
            }

            return "javascript:popUpSetSize('/pages/{$fileLocation}.LieuxExecution."
                . $sessionLang . ".htm','1050px','450px','yes');";
        } else {
            if (Atexo_Module::isEnabled('LieuxExecutionCarte')) {
                $url = 'index.php?page=' . $fileLocation .
                    '.LieuxExecutionCarte&lieux=' . $param . '&selected=' . $selectedLoc;

                return "javascript:popUpSetSize('" . $url . "','850px','850px','yes');";
            }

            return "javascript:popUpSetSize('index.php?page={$fileLocation}.LieuxExecution&selected="
                . $selectedLoc . "','1050px','450px','yes');";
        }
    }
}
