<?php

namespace Application\Service\Atexo;

use Application\Service\Atexo\Agent\Atexo_Agent_EnablingsAgents;
use Exception;
use Laminas\Http\Client;
use Laminas\Http\Request;

/*
 * Created on 20 mai 2013
 *
 * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
 * @package
 */
class Atexo_EspaceCollaboratif
{
    /*
     * permet de creer un site (espace collaboratif)
     */
    public function createConsultationSite($consultation)
    {
        try {
            $infoJson = self::getInfosAlfresco($consultation, Atexo_CurrentUser::getIdAgentConnected());
            self::send($infoJson);
            //ajouter gestion des erreurs selon code retourné
        } catch (Exception $e) {
            //Add Logs
            Atexo_Util::write_file(Atexo_Config::getParameter('LOG_ERRORS'), date('Y-m-d H:i:s').' -- >  \n'.$e->getMessage().'\n', 'a+');
        }
    }

    /*
     * retourne un fichier Json contenant les infos à envoyer au WS Alfresco
     */
    public function getInfosAlfresco($consultation, $idAgent)
    {
        $infoMember = self::getInfosAgentForEspaceCollaboratif($idAgent); //print_r($infoMember);exit;
        $info = [
                'visibility' => 'PRIVATE',
                'title' => $consultation->getReferenceUtilisateur().' - '.utf8_encode($consultation->getIntituleTraduit()),
                'shortName' => $consultation->getOrganisme().''.$consultation->getId(),
                'description' => $consultation->getObjetTraduit(),
                'sitePreset' => 'site-collaboratif-consultation',
                'members' => $infoMember,
                 ];

        return json_encode($info, JSON_THROW_ON_ERROR);
    }

    /*
     * return (id,role) dont le role est de l habilitation espaceCollaboratif la plus forte
     */
    public function getInfosAgentForEspaceCollaboratif($idAgent)
    {
        $infoMember = [];
        if ((new Atexo_Agent_EnablingsAgents())->isHabilitationAgentExiste(Atexo_CurrentUser::getIdAgentConnected(), 'EspaceCollaboratifGestionnaire')) {
            $role = 'SiteManager';
        } elseif ((new Atexo_Agent_EnablingsAgents())->isHabilitationAgentExiste(Atexo_CurrentUser::getIdAgentConnected(), 'EspaceCollaboratifContributeur')) {
            $role = 'SiteContributor';
        }
        $login = (new Atexo_Agent())->getLoginById($idAgent);
        $infoMember[0]['userName'] = $login;
        $infoMember[0]['role'] = $role;

        return $infoMember;
    }

    /*
     * permet d invoquer le WS Alfresco
     */
    public function send($json)
    {
        try {
            $client = new Client();
            $client->setUri(Atexo_Config::getParameter('URL_WS_ALFRESCO'));
            $client->setMethod(Request::METHOD_POST);
            $client->setEncType('application/json');
            $client->setRawBody($json);
            $res = $client->send();

            return $res;
        } catch (Exception $e) {
            Atexo_Util::write_file(Atexo_Config::getParameter('LOG_ERRORS'), date('Y-m-d H:i:s').' -- >  \n'.$e->getMessage().'\n', 'a+');
        }
    }
}
