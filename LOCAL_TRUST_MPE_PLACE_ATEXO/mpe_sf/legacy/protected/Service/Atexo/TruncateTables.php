<?php

namespace Application\Service\Atexo;

use Exception;
use Prado\Prado;
use Prado\Util\TLogger;

/*
 * commentaires
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 * @version 1.0
 * @since MPE-3.0
 * @package
 * @subpackage
 */

if (!defined('HOST_SPEC')) {
    define('HOST_SPEC', 'localhost');
}
if (!defined('USERNAME')) {
    define('USERNAME', 'root');
}
if (!defined('PASSWORD')) {
    define('PASSWORD', '');
}

class Atexo_TruncateTables
{
    public function run()
    {
        $baseCommune = $_SERVER['argv'][1];
        self::connectToMysql($baseCommune, HOST_SPEC, USERNAME, PASSWORD);
        $sql = 'Truncate table consultation';
        mysqli_query($sql);
        $sql = 'Truncate table Offre';
        mysqli_query($sql);
        $sql = 'SELECT acronyme FROM Organisme';
        $result = mysqli_query($sql);
        self::closeMysqlConnection();
        while ($ligne = mysqli_fetch_assoc($result)) {
            self::connectToMysql($ligne['acronyme'], HOST_SPEC, USERNAME, PASSWORD);
            $sql = 'Truncate table consultation';
            mysqli_query($sql);
            $sql = 'Truncate table Offre';
            mysqli_query($sql);
            $sql = 'Truncate table Enveloppe';
            mysqli_query($sql);
            $sql = 'Truncate table fichierEnveloppe';
            mysqli_query($sql);
            $sql = 'Truncate table blocFichierEnveloppe';
            mysqli_query($sql);
            $sql = 'Truncate table blocFichierEnveloppeTemporaire';
            mysqli_query($sql);
            $sql = 'Truncate table Admissibilite_Enveloppe_Lot';
            mysqli_query($sql);
            $sql = 'Truncate table Admissibilite_Enveloppe_papier_Lot';
            mysqli_query($sql);
            $sql = 'Truncate table Offre_papier';
            mysqli_query($sql);
            $sql = 'Truncate table Enveloppe_papier';
            mysqli_query($sql);
            $sql = 'Truncate table decisionEnveloppe';
            mysqli_query($sql);
            $sql = 'Truncate table DecisionLot';
            mysqli_query($sql);
            self::closeMysqlConnection();
        }
    }

    public function connectToMysql($db, $host, $userName, $passWord)
    {
        try {
            $connection = mysqli_connect($host, $userName, $passWord);
            mysqli_select_db($db, $connection);
        } catch (Exception $e) {
            Prado::log('Erreur TruncateTables.php'.$e->getMessage().$e->getTraceAsString(), TLogger::ERROR, 'TruncateTables.php');
        }
    }

    public function closeMysqlConnection()
    {
        mysqli_close();
    }
}
