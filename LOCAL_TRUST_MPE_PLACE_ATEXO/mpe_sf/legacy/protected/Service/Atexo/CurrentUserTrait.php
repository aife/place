<?php

namespace Application\Service\Atexo;

use Application\Propel\Mpe\CommonConfigurationOrganisme;
use Application\Propel\Mpe\CommonConfigurationOrganismeQuery;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationQuery;
use Application\Propel\Mpe\Om\BaseCommonConfigurationOrganismeQuery;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Agent;
use Application\Pages\Commun\Classes\MPEUser;
use Application\Propel\Mpe\CommonServicePeer;

/**
 * Classe de CurrentUser.
 *
 * @author Khalid BENAMAR <khalid.benamar@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
trait CurrentUserTrait
{


    /**
     * Retourne la dénomination de l'organisme courant. S'il n'y a pas d'organisme courant, alors retourne false.
     *
     * @return string|false
     */
    public function getOrganismDesignation()
    {
        return Atexo_CurrentUser::getOrganismDesignation();
    }

    /**
     * Retourne l'acronyme de l'organisme courant. S'il n'y a pas d'organisme courant, alors retourne false.
     * @return string|false
     */
    public function getOrganismAcronym()
    {
        return Atexo_CurrentUser::getOrganismAcronym();
    }

    /**
     * Retourne le nom de l'organisme courant. S'il n'y a pas d'organisme courant, alors retourne false.
     * @return string|false
     * @deprecated use getOrganismAcronym() instead
     */
    public function getCurrentOrganism()
    {
        return Atexo_CurrentUser::getOrganismAcronym();
    }


    public function getIdEntreprise()
    {
        return Atexo_CurrentUser::getIdEntreprise();
    }

    public function getIdEtablissement()
    {
        return Atexo_CurrentUser::getIdEtablissement();
    }

    public function getIdInscrit()
    {
        return Atexo_CurrentUser::getIdInscrit();
    }

    public function isATES()
    {
        return Atexo_CurrentUser::isATES();
    }

    /**
     * Retourne le nom du service de l'agent connecté. S'il n'y a pas, alors retourne false.
     */
    public function getIdServiceAgentConnected(): string|false
    {
        return Atexo_CurrentUser::getIdServiceAgentConnected();
    }

    /**
     * Retourne le prenom de l'agent connecté. S'il n'y a pas d'agent connecté, alors retourne false.
     */
    public function getFirstName(): string|false
    {
        return Atexo_CurrentUser::getFirstName();
    }

    /**
     * Retourne le prenom de l'agent connecté. S'il n'y a pas d'agent connecté, alors retourne false.
     *
     *
     * @deprecated use getFirstName() instead
     */
    public function getFirstNameAgentConnected(): string|false
    {
        return Atexo_CurrentUser::getFirstNameAgentConnected();
    }

    /**
     * Retourne le nom de l'agent connecté. S'il n'y a pas d'agent connecté, alors retourne false.
     */
    public function getLastName(): string|false
    {
        return Atexo_CurrentUser::getLastName();
    }

    /**
     * Retourne le nom de l'agent connecté. S'il n'y a pas d'agent connecté, alors retourne false.
     *
     *
     * @deprecated use getLastName() instead
     */
    public function getLastNameAgentConnected(): string|false
    {
        return Atexo_CurrentUser::getLastNameAgentConnected();
    }

    /**
     * Retourne les module de l'agent connecté. S'il n'y a pas d'agent connecté, alors retourne false.
     */
    public function getModules(): array|false
    {
        return Atexo_CurrentUser::getModules();
    }

    /**
     * verifie si l'agent connecté a l'habilitation passée en paramètre.
     *
     * @param string $habilitation : habilitation à tester
     *
     * @return bool true si l'agent a l'habilitation false sinon
     */
    public function hasHabilitation($habilitation)
    {
        return Atexo_CurrentUser::hasHabilitation($habilitation);
    }

    /**
     * Ajoute la valeur $data à la session, identifiée par la clé $key.
     *
     * @param string $key
     * @param mixed  $data
     */
    public function writeToSession($key, $data, $idElement = null)
    {
        Atexo_CurrentUser::writeToSession($key, $data, $idElement);
    }

    /**
     * is key in session.
     *
     * @param string $key
     *
     * @return
     */
    public function isSessionSet($key)
    {
        return Atexo_CurrentUser::isSessionSet($key);
    }

    /**
     * Supprime l'element ayant la clé $key de la session.
     *
     * @param int $key       la clé de l'element
     * @param int $idElement la clé du sous-element
     */
    public function deleteFromSession($key, $idElement = null)
    {
        Atexo_CurrentUser::deleteFromSession($key, $idElement);
    }

    public function isAgent()
    {
        return Atexo_CurrentUser::isAgent();
    }

    public function isAdmin()
    {
        return Atexo_CurrentUser::isAdmin();
    }

    /**
     * retourne le prenom de l'inscrit authentifié
     *
     */
    public function getFirstNameInscrit()
    {
        return Atexo_CurrentUser::getFirstNameInscrit();
    }

    /**
     * retourne le nom de l'inscrit authentifié
     *
     */
    public function getLastNameInscrit()
    {
        return Atexo_CurrentUser::getLastNameInscrit();
    }

    public function isConnected()
    {
        return Atexo_CurrentUser::isConnected();
    }

    /**
     * écrit le contenu sérialisé de la session au format attendu par
     * mpe-1.8 dans un fichier.
     */
    public function dumpSession()
    {
        return Atexo_CurrentUser::dumpSession();
    }

    public function getBloque()
    {
        return Atexo_CurrentUser::getBloque();
    }

    public function getEmailInscrit()
    {
        return Atexo_CurrentUser::getEmailInscrit();
    }

    public function getRole()
    {
        return Atexo_CurrentUser::getRole();
    }


    public function isAgentSocle()
    {
        return Atexo_CurrentUser::isAgentSocle();
    }

    /**
     * Recupere l'email de l'agent ou l'inscrit de l'entreprise connecte.
     */
    public function getEmail(): false|string
    {
        return Atexo_CurrentUser::getEmail();
    }

    /**
     * Recupere le login de l'agent ou l'inscrit connecte.
     */
    public function getLoginUser(): false|string
    {
        return (new Atexo_CurrentUser())->getLoginUser();
    }

    /**
     * @return mixed
     */
    public function isSiretAlert()
    {
        return (new Atexo_CurrentUser())->isSiretAlert();
    }

    public function isMAMPEnabled(): bool
    {
        $configurationOrgQuery = new CommonConfigurationOrganismeQuery();
        $configurationOrganisme = $configurationOrgQuery->findOneByOrganisme($this->getOrganismAcronym());

        if($configurationOrganisme instanceof CommonConfigurationOrganisme){
            return $configurationOrganisme->getActiverMonAssistantMarchesPublics();
        }

        return false;
    }
}
