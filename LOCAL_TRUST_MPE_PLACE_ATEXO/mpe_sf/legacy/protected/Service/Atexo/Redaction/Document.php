<?php

namespace Application\Service\Atexo\Redaction;

use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Util;

/**
 * Classe de manipulation des consultations.
 *
 * @author Loubna Ezziani <loubna.ezziani@atexo.com>
 * @copyright Atexo 2012
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Redaction_Document
{
    protected $id;
    protected $type;
    protected $titre;
    protected $nom;
    protected $auteur;
    protected $statut;
    protected $date;
    protected $heure;
    protected $blob;
    protected $organisme;
    protected $consultation_id;
    public $_redaction = null;

    //Le getters
    public function getId()
    {
        return $this->id;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getTitre()
    {
        return $this->titre;
    }

    public function getNom()
    {
        return $this->nom;
    }

    public function getAuteur()
    {
        return $this->auteur;
    }

    public function getStatut()
    {
        return $this->statut;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function getHeure()
    {
        return $this->heure;
    }

    public function getBlob()
    {
        return $this->blob;
    }

    public function getRef_Consultation()
    {
        return $this->consultation_id;
    }

    public function getOrganisme()
    {
        return $this->organisme;
    }

    // les setters
    public function setId($v)
    {
        $this->id = $v;
    }

    public function setType($v)
    {
        $this->type = $v;
    }

    public function setTitre($v)
    {
        $this->titre = $v;
    }

    public function setNom($v)
    {
        $this->nom = $v;
    }

    public function setAuteur($v)
    {
        $this->auteur = $v;
    }

    public function setStatut($v)
    {
        $this->statut = $v;
    }

    public function setDate($v)
    {
        $this->date = $v;
    }

    public function setHeure($v)
    {
        $this->heure = $v;
    }

    public function setBlob($v)
    {
        $this->blob = $v;
    }

    public function setRef_Consultation($v)
    {
        $this->consultation_id = $v;
    }

    public function setOrganisme($v)
    {
        $this->organisme = $v;
    }

    public function createListeRedacFromXml($xml, $isRC= false)
    {
        $redact = null;
        $xml = Atexo_Util::toUtf8($xml);
        if (null === $redact) {
            //set_magic_quotes_runtime(0);
            ini_set('magic_quotes_runtime', 0);
            $domDocument = new \DOMDocument();
            $domDocument->loadXML($xml);
            $redact = $domDocument->documentElement;
        }
        $docRedacRC = [];
        $docRedacNotRC = [];
        $documents = $redact->getElementsByTagName('document');
        foreach ($documents as $document) {
            $doc = new Atexo_Redaction_Document();
            $tag = $document->getElementsByTagName('statut')->item(0);
            $doc->setStatut($tag->nodeValue);
            $tag = $document->getElementsByTagName('nom')->item(0);
            $doc->setNom(Atexo_Util::atexoHtmlEntities((new Atexo_Config())->toPfEncoding($tag->nodeValue)));
            $tag = $document->getElementsByTagName('titre')->item(0);
            $doc->setTitre(Atexo_Util::atexoHtmlEntities((new Atexo_Config())->toPfEncoding($tag->nodeValue)));
            $tag = $document->getElementsByTagName('identifiant')->item(0);
            $doc->setId($tag->nodeValue);

            $tag = $document->getElementsByTagName('type')->item(0);
            $doc->setType($tag->nodeValue);

            if ('RC' == $tag->nodeValue) {
                $docRedacRC[] = $doc;
            } else {
                $docRedacNotRC[] = $doc;
            }

        }
        return $isRC ? $docRedacRC : array_merge($docRedacRC, $docRedacNotRC);
    }
}
