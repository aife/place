<?php

namespace Application\Service\Atexo\Blob;

use Application\Service\Atexo\Atexo_Config;
use Exception;
use PDO;
use PDOException;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * Classe de.
 *
 * @author Khalid BENAMAR <khalid.benamar@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Blob_DbManagement
{
    /* ---------------- *
     * GLOBAL VARIABLES *
     * ---------------- */

    private array $_db_call_stack = [];
    private $_db_error_code = 0;
    private $_db_error_message = '';
    private array $_db_remote_servers = [];
    /* ------------------------------ *
     * ERROR/LOG MANAGEMENT FUNCTIONS *
     * ------------------------------ */

    public function __construct()
    {
        $chaine = Atexo_Config::getParameter('DB_REMOTE_SERVERS');
        if ('' == $chaine) {
            $this->_db_remote_servers = [];
        } else {
            $this->_db_remote_servers = explode(';', $chaine);
        }
    }

    public function removePrefix($database)
    {
        $db_prefix = Atexo_Config::getParameter('DB_PREFIX');
        if (isset($db_prefix) && '' != $db_prefix && $db_prefix == substr($database, 0, strlen($db_prefix))) {
            $database = substr($database, (strlen($db_prefix) + 1));
        }

        return $database;
    }

    public function db_set_error($code, $message)
    {
        $this->_db_error_code = $code;
        $this->_db_error_message = $message;
    }

    public function db_reset_error()
    {
        $this->_db_error_code = 0;
        $this->_db_error_message = '';
    }

    public function db_error_code()
    {
        return $this->_db_error_code;
    }

    public function db_error_message()
    {
        return $this->_db_error_message;
    }

    public function db_save_error()
    {
        $result = [];
        $result['code'] = self::db_error_code();
        $result['message'] = self::db_error_message();

        return $result;
    }

    public function db_restore_error($error)
    {
        if (0 != $error['code']) {
            self::db_set_error($error['code'], $error['message']);
        }
    }

    public function db_call_stack_push($function)
    {
        array_push($this->_db_call_stack, $function);
    }

    public function db_call_stack_pop()
    {
        return array_pop($this->_db_call_stack);
    }

    public function db_logger($code, $message, $level, $set_error = false)
    {
        if ($set_error) {
            Prado::log("$level : blob :$code : $message : ", TLogger::FATAL, 'Fatal');
        } else {
            //Prado::log("$level : blob :$code : $message : ",TLogger::INFO, "Info");
        }
        /* if ($set_error)
         self::db_set_error($code, $message);
         if ($level <= Atexo_Config::getParameter('DB_LOG_LEVEL'])
         {
         $function_list = "";
         foreach ($this->_db_call_stack as $function_id => $function)
         $function_list .= $function . ": ";
         // $this->_db_logger->log("[" . getmypid() . "] (" . $code . ") " . $function_list . $message, $level);
         }*/
    }

    /* --------------------------- *
     * SYSTEM MANAGEMENT FUNCTIONS *
     * --------------------------- */

    public function db_insert_blob(
        $db_host,
        $db_login,
        $db_password,
        $database,
        $file_name,
        $file_path,
        $subfolder = null,
        $extension = ''
    ) {
        self::db_call_stack_push('db_insert_blob');
        self::db_reset_error();
        $result = false;
        self::db_logger(0, 'Inserting new blob '.$file_path.' ('.$file_name.') in database '.$database.'@'.$db_host, Atexo_Config::getParameter('PEAR_LOG_INFO'));
        $db_resource = self::db_open_connection($db_host, $db_login, $db_password);
        $commonDB = Atexo_Config::getParameter('DB_PREFIX').'_'.Atexo_Config::getParameter('COMMON_DB');
        if (false !== $db_resource) {
            if (self::db_select_database($db_resource, $commonDB)) {
                if (self::db_start_transaction($db_resource)) {
                    $params = [];
                    $repertoire = date('Y').'/'.date('m').'/'.date('d')."/$database/".Atexo_Config::getParameter('FILES_DIR');
                    $dir = $repertoire;
                    if ($subfolder) {
                        $dir .= $subfolder.'/';
                    }
                    $params[':chemin'] = $dir;
                    $params[':dossier'] = $subfolder;
                    $params[':extension'] = $extension;
                    if ($database == Atexo_Config::getParameter('COMMON_BLOB')) {
                        $query = 'INSERT INTO `blob_file` (`id`, `name`, `deletion_datetime`, `statut_synchro`, `hash`, `dossier`, `chemin`, `extension`) VALUES (:id, :name, NULL, 0,  :hash, :dossier, :chemin, :extension)';
                        $params[':id'] = null;
                        $params[':name'] = $file_name;
                        $params[':hash'] = sha1_file($file_path);
                    } else {
                        $query = 'INSERT INTO `blobOrganisme_file` (`id`, `organisme`, `name`, `deletion_datetime`, `statut_synchro`, `hash`, `dossier`, `chemin`, `extension`)  VALUES (:id, :organisme, :name, NULL, 0, :hash, :dossier, :chemin, :extension)';
                        $params[':id'] = null;
                        $params[':name'] = $file_name;
                        $params[':organisme'] = $database;
                        $params[':hash'] = sha1_file($file_path);
                    }
                    $stm = $db_resource->prepare($query);
                    $stm->execute($params);
                    if (false !== $stm) {
                        self::db_logger(0, 'New blob ID created ', Atexo_Config::getParameter('PEAR_LOG_DEBUG'));
                        //$query = "SELECT last_insert_id()";
                        //$query_result = @mysql_query($query, $db_resource);
                        $blob_id = $db_resource->lastInsertId();
                        if (false !== $blob_id) {
                            //$query_result_parsed = self::db_parse_query_result($db_resource, $query_result);
                            if ($blob_id > 0) {
                                $repertoire = date('Y').'/'.date('m').'/'.date('d')
                                    ."/$database/".Atexo_Config::getParameter('FILES_DIR');
                                //$blob_id = $query_result_parsed[0]['last_insert_id()'];
                                //$blob_id = $query_result_parsed[0]['last_insert_id()'];
                                self::db_logger(
                                    0,
                                    'Blob ID retrieved '.' ('.$blob_id.')',
                                    Atexo_Config::getParameter('PEAR_LOG_DEBUG')
                                );
                                $blob_file = self::db_get_blob_storage_file_path(
                                    $db_host,
                                    $database,
                                    $blob_id,
                                    0,
                                    $repertoire,
                                    true,
                                    $subfolder,
                                    $extension
                                );

                                $dest_dir = dirname($blob_file);
                                $baseRootDir = str_replace('//', '/', Atexo_Config::getParameter('BASE_ROOT_DIR'));
                                if ('' != Atexo_Config::getParameter('DOCUMENT_ROOT_DIR_MULTIPLE')) {
                                    $mnts = explode(';', Atexo_Config::getParameter('DOCUMENT_ROOT_DIR_MULTIPLE'));
                                    $baseRootDir = $mnts[0];
                                }
                                if (!is_dir($dest_dir)
                                    && substr($dest_dir, 0, strlen($baseRootDir)) == $baseRootDir
                                ) {
                                    mkdir($dest_dir, 0700, true);
                                }

                                if (false !== $stm) {
                                    self::db_logger(0, 'File information created for blob ID '.$blob_id.' / revision 0 ', Atexo_Config::getParameter('PEAR_LOG_DEBUG'));
                                    if (0 === self::db_system_command('mv '.escapeshellarg($file_path).' '.escapeshellarg($blob_file))) {
                                        self::db_logger(0, 'Blob ID '.$blob_id.' stored in '.$blob_file, Atexo_Config::getParameter('PEAR_LOG_DEBUG'));
                                        $continue = false;
                                        if (1 == Atexo_Config::getParameter('DB_REMOTE_SYNCHRO_ENABLED')) {
                                            if (self::db_synchronize_remote_blob_file($db_host, $database, $blob_id, 0)) {
                                                $continue = true;
                                            } elseif (1 != Atexo_Config::getParameter('DB_REMOTE_SYNCHRO_CRITICAL')) {
                                                $continue = true;
                                                self::db_logger(-1, '(db_synchronize_remote_blob_file) Blob file synchronization failed ('.$blob_file.')', Atexo_Config::getParameter('PEAR_LOG_WARNING'), true);
                                            }
                                        } else {
                                            $continue = true;
                                        }
                                        if ($continue) {
                                            if (self::db_commit_transaction($db_resource)) {
                                                $result = $blob_id;
                                                self::db_logger(
                                                    0,
                                                    'Blob stored at '.$blob_file.' with blob ID '.$blob_id.' / revision 0 in database '.$database.'@'.$db_host,
                                                    Atexo_Config::getParameter('PEAR_LOG_INFO')
                                                );
                                            } else {
                                                $error = self::db_save_error();
                                                self::db_system_command('rm '.escapeshellarg($blob_file));
                                                if (1 == Atexo_Config::getParameter('DB_REMOTE_SYNCHRO_ENABLED')) {
                                                    self::db_synchronize_remote_blob_file($db_host, $database, $blob_id, 0);
                                                }
                                                self::db_rollback_transaction($db_resource);
                                                self::db_restore_error($error);
                                            }
                                        } else {
                                            $error = self::db_save_error();
                                            self::db_system_command('rm '.escapeshellarg($blob_file));
                                            if (1 == Atexo_Config::getParameter('DB_REMOTE_SYNCHRO_ENABLED')) {
                                                self::db_synchronize_remote_blob_file($db_host, $database, $blob_id, 0);
                                            }
                                            self::db_rollback_transaction($db_resource);
                                            self::db_restore_error($error);
                                        }
                                    } else {
                                        $error = self::db_save_error();
                                        self::db_rollback_transaction($db_resource);
                                        self::db_restore_error($error);
                                    }
                                } else {
                                    self::db_logger(1, '(mysql_query) Cannot create file information for blob ID '.$blob_id.' / revision 0 / extension '.$extension, Atexo_Config::getParameter('PEAR_LOG_ERR'), true);
                                    $error = self::db_save_error();
                                    self::db_rollback_transaction($db_resource);
                                    self::db_restore_error($error);
                                }
                            } else {
                                self::db_logger(1, '(mysql_query) Last insert ID not found ', Atexo_Config::getParameter('PEAR_LOG_ERR'), true);
                                $error = self::db_save_error();
                                self::db_rollback_transaction($db_resource);
                                self::db_restore_error($error);
                            }
                        } else {
                            self::db_logger(1, '(mysql_query) Cannot retrieve last insert ID ', Atexo_Config::getParameter('PEAR_LOG_ERR'), true);
                            $error = self::db_save_error();
                            self::db_rollback_transaction($db_resource);
                            self::db_restore_error($error);
                        }
                    } else {
                        self::db_logger(1, '(mysql_query) Cannot create new blob ID ', Atexo_Config::getParameter('PEAR_LOG_ERR'), true);
                        $error = self::db_save_error();
                        self::db_rollback_transaction($db_resource);
                        self::db_restore_error($error);
                    }
                }
            }
            $error = self::db_save_error();
            // self::db_close_connection($db_resource);
            self::db_restore_error($error);
        }
        self::db_call_stack_pop();

        return $result;
    }

    public function db_acquire_lock_on_blob($db_host, $db_login, $db_password, $database, $blob_id, $exclusive_lock = false, $subfolder = null)
    {
        self::db_call_stack_push('db_acquire_lock_on_blob');
        self::db_reset_error();
        $result = false;
        if ($exclusive_lock) {
            self::db_logger(0, 'Acquiring write lock on blob ID '.$blob_id.' in database '.$database.'@'.$db_host, Atexo_Config::getParameter('PEAR_LOG_INFO'));
        } else {
            self::db_logger(0, 'Acquiring read lock on blob ID '.$blob_id.' in database '.$database.'@'.$db_host, Atexo_Config::getParameter('PEAR_LOG_INFO'));
        }
        $db_resource = self::db_open_connection($db_host, $db_login, $db_password);
        $commonDB = Atexo_Config::getParameter('DB_PREFIX').'_'.Atexo_Config::getParameter('COMMON_DB');
        if (false !== $db_resource) {
            if (self::db_select_database($db_resource, $commonDB)) {
                $blob_revision = 0;
                self::db_logger(0, 'Revision retrieved for blob ID '.$blob_id.'AND DESTINATION '.$database.' ('.$blob_revision.')', Atexo_Config::getParameter('PEAR_LOG_DEBUG'));
                $params = [];
                $params[':id'] = $blob_id;

                if ($database == Atexo_Config::getParameter('COMMON_BLOB')) {//si un blob d'entreprise
                    $query = 'SELECT `id`, `name`, `chemin`, `extension` , `old_id` FROM `blob_file` WHERE `id` = :id';
                } else {
                    $query = 'SELECT `id`, `organisme`, `name`, `chemin`, `extension` , `old_id` FROM `blobOrganisme_file` WHERE `id`= :id AND `organisme`=:organisme';
                    $params[':organisme'] = $database;
                }
                $stm = $db_resource->prepare($query);
                $stm->execute($params);
                $extension = '';
                if (false !== $stm) {
                    $query_result_parsed = self::db_parse_query_result($db_resource, $stm);
                    if (is_countable($query_result_parsed) ? count($query_result_parsed) : 0) {
                        self::db_logger(0, 'File information for blob ID '.$blob_id.' / revision '.$blob_revision.' found ', Atexo_Config::getParameter('PEAR_LOG_DEBUG'));
                        $result = ['db_resource' => $db_resource, 'blob' => $query_result_parsed[0]];

                        $chemin = null;
                        if (null !== $result['blob']['chemin']) {
                            $chemin = $result['blob']['chemin'];
                        }
                        if (null !== $result['blob']['extension']) {
                            $extension = $result['blob']['extension'];
                        }

                        // récupérer id blob
                        $blob_id = self::getBlobId($result['blob']['id'], $result['blob']['old_id']);

                        $result['blob']['pointer'] = self::db_get_blob_storage_file_path($db_host, $database, $blob_id, $blob_revision, $chemin, false, $subfolder, $extension);
                        $result['blob']['db_host'] = $db_host;
                        $result['blob']['database'] = $database;

                        if ($exclusive_lock) {
                            $result['lock_type'] = 'write';
                        } else {
                            $result['lock_type'] = 'read';
                        }
                        if ($exclusive_lock) {
                            self::db_logger(0, 'Write lock acquired on blob ID '.$blob_id.' in database '.$database.'@'.$db_host, Atexo_Config::getParameter('PEAR_LOG_INFO'));
                        } else {
                            self::db_logger(0, 'Read lock acquired on blob ID '.$blob_id.' in database '.$database.'@'.$db_host, Atexo_Config::getParameter('PEAR_LOG_INFO'));
                        }
                    } else {
                        self::db_logger(
                            1,
                            '(mysql_query) File information for blob ID '.$blob_id.' / revision '.$blob_revision.'/ extension '.$extension.' not found ',
                            Atexo_Config::getParameter('PEAR_LOG_ERR'),
                            true
                        );
                        $error = self::db_save_error();
                        self::db_restore_error($error);
                    }
                } else {
                    self::db_logger(
                        1,
                        '(mysql_query) Cannot find file information for blob ID '.$blob_id.' / revision '.$blob_revision.'/ extension '.$extension,
                        Atexo_Config::getParameter('PEAR_LOG_ERR'),
                        true
                    );
                    $error = self::db_save_error();
                    self::db_restore_error($error);
                }
            }
            if (false === $result) {
                $error = self::db_save_error();
                self::db_close_connection($db_resource);
                self::db_restore_error($error);
            }
        }
        self::db_call_stack_pop();

        return $result;
    }

    public function db_release_lock_on_blob($blob_resource)
    {
        self::db_call_stack_push('db_release_lock_on_blob');
        self::db_reset_error();
        $result = false;
        if ('write' == $blob_resource['lock_type']) {
            self::db_logger(
                0,
                'Releasing write lock on blob ID '.$blob_resource['blob']['id'].' in database '.$blob_resource['blob']['database'].'@'.$blob_resource['blob']['db_host'],
                Atexo_Config::getParameter('PEAR_LOG_INFO')
            );
        } else {
            self::db_logger(
                0,
                'Releasing read lock on blob ID '.$blob_resource['blob']['id'].' in database '.$blob_resource['blob']['database'].'@'.$blob_resource['blob']['db_host'],
                Atexo_Config::getParameter('PEAR_LOG_INFO')
            );
        }
        self::db_rollback_transaction($blob_resource['db_resource']);
        $result = self::db_close_connection($blob_resource['db_resource']);
        if ($result) {
            if ('write' == $blob_resource['lock_type']) {
                self::db_logger(
                    0,
                    'Write lock released on blob ID '.$blob_resource['blob']['id'].' in database '.$blob_resource['blob']['database'].'@'.$blob_resource['blob']['db_host'],
                    Atexo_Config::getParameter('PEAR_LOG_INFO')
                );
            } else {
                self::db_logger(
                    0,
                    'Read lock released on blob ID '.$blob_resource['blob']['id'].' in database '.$blob_resource['blob']['database'].'@'.$blob_resource['blob']['db_host'],
                    Atexo_Config::getParameter('PEAR_LOG_INFO')
                );
            }
        }
        self::db_call_stack_pop();

        return $result;
    }

    /**
     * @param $id
     * @param $oldId
     *
     * @return mixed
     */
    public function getBlobId($id, $oldId)
    {
        $blobId = $id;
        if (!empty($oldId)) {
            $blobId = $oldId;
        }

        return $blobId;
    }

    /* ---------------------------------- *
     * LOW-LEVEL SQL MANAGEMENT FUNCTIONS *
     * ---------------------------------- */

    public function db_open_connection($db_host, $db_login, $db_password)
    {
        $db_resource = null;
        self::db_call_stack_push('db_open_connection');
        self::db_reset_error();
        $dsn_a = \Application\Propel\Config\Database\construct_array();
        $db_host = $dsn_a['propel']['datasources']['MPE_DB']['connection']['dsn'];
        try {
            $db_resource = new PDO($db_host, $db_login, $db_password);
            $db_resource->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            self::db_logger(0, 'Connected to '.$db_login.'@'.$db_host, Atexo_Config::getParameter('PEAR_LOG_DEBUG'));
        } catch (PDOException $e) {
            self::db_logger($e->getCode(), '(mysql_connect) Cannot connect to '.$db_login.'@'.$db_host.' - '.$e->getCode().' '.$e->getMessage(), Atexo_Config::getParameter('PEAR_LOG_ERR'), true);
        }
        self::db_call_stack_pop();

        return $db_resource;
    }

    public function db_close_connection($db_resource)
    {
        self::db_call_stack_push('db_close_connection');
        self::db_reset_error();
        self::db_logger(0, 'db closed', Atexo_Config::getParameter('PEAR_LOG_DEBUG'));
        self::db_call_stack_pop();

        return true;
    }

    public function db_select_database($db_resource, $database)
    {
        self::db_call_stack_push('db_select_database');
        self::db_reset_error();
        self::db_logger(0, 'Database '.$database.' selected', Atexo_Config::getParameter('PEAR_LOG_DEBUG'));
        self::db_call_stack_pop();

        return true;
    }

    public function db_start_transaction($db_resource)
    {
        self::db_call_stack_push('db_start_transaction');
        self::db_reset_error();
        $result = false;
        try {
            $db_resource->beginTransaction();
            $result = true;
            self::db_logger(0, 'Transaction started ', Atexo_Config::getParameter('PEAR_LOG_DEBUG'));
        } catch (PDOException $e) {
            self::db_logger($e->getCode(), '(beginTransaction) Cannot start transaction '.' - '.$e->getCode().' '.$e->getMessage(), Atexo_Config::getParameter('PEAR_LOG_ERR'), true);
        }
        self::db_call_stack_pop();

        return $result;
    }

    public function db_commit_transaction($db_resource)
    {
        self::db_call_stack_push('db_commit_transaction');
        self::db_reset_error();
        $result = false;
        try {
            $db_resource->commit();
            $result = true;
            self::db_logger(0, 'Transaction committed ', Atexo_Config::getParameter('PEAR_LOG_DEBUG'));
        } catch (PDOException $e) {
            self::db_logger(
                $e->getCode(),
                '(mysql_query) Cannot commit transaction ' . ' - ' . $e->getCode()
                . ' ' . $e->getMessage() . ' ' . $e->getTraceAsString()
                , Atexo_Config::getParameter('PEAR_LOG_ERR'),
                true
            );
        }
        self::db_call_stack_pop();

        return $result;
    }

    private function db_rollback_transaction($db_resource)
    {
        self::db_call_stack_push('db_rollback_transaction');
        self::db_reset_error();
        $result = false;
        try {
            $db_resource->rollBack();
            $result = true;
            self::db_logger(0, 'Transaction rollbacked ', Atexo_Config::getParameter('PEAR_LOG_DEBUG'));
        } catch (PDOException $e) {
            self::db_logger(
                $e->getCode(),
                '(mysql_query) Cannot rollback transaction '.' - '.$e->getCode().
                ' '.$e->getMessage() . ' ' . $e->getTraceAsString(),
                Atexo_Config::getParameter('PEAR_LOG_ERR'),
                true
            );
        }
        self::db_call_stack_pop();

        return $result;
    }

    private function db_parse_query_result($db_resource, $stmt)
    {
        self::db_call_stack_push('db_parse_query_result');
        self::db_reset_error();
        $result = [];
        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
            array_push($result, $row);
        }
        if ($stmt->closeCursor()) {
            self::db_logger(0, ' statement freed', Atexo_Config::getParameter('PEAR_LOG_DEBUG'));
        } else {
            self::db_logger(
                @mysqli_errno(),
                '(mysql_free_result) Cannot free statement - '.@mysqli_errno().' '.@mysqli_error(),
                Atexo_Config::getParameter('PEAR_LOG_WARNING'),
                true
            );
        }
        self::db_call_stack_pop();

        return $result;
    }

    public function db_get_blob_storage_file_path(
        $db_host,
        $database,
        $id,
        $revision,
        $repertoire = null,
        $insert = false,
        $subfolder = null,
        $extension = ''
    ) {
        $subfolder = $subfolder ? '/'.$subfolder.'/' : '/';
        $pathFile = false;
        $blobStorageDirectoryPath = self::db_get_blob_storage_directory_path($db_host, $database, $repertoire, $insert);
        if (is_array($blobStorageDirectoryPath)) {
            foreach ($blobStorageDirectoryPath as $directory) {
                $file = rtrim($directory, '/').$subfolder.
                    self::db_get_blob_storage_file_name($id, $revision, $extension);
                if (file_exists($file)) {
                    $pathFile = $file;
                    break;
                }
            }
        } else {
            $pathFile = rtrim($blobStorageDirectoryPath, '/').$subfolder.
                self::db_get_blob_storage_file_name($id, $revision, $extension);
        }

        return $pathFile;
    }

    public function db_system_command($command, $normal_result = 0)
    {
        $token_command = md5(uniqid(random_int(0, mt_getrandmax()), true));
        self::db_call_stack_push('db_system_command');
        self::db_reset_error();
        $result = false;
        $command_output_file = Atexo_Config::getParameter('COMMON_TMP').getmypid()."_$token_command";
        if (false !== system($command.' > '.$command_output_file.' 2>&1 ', $command_result)) {
            $result = $command_result;
            if ($command_result == $normal_result) {
                self::db_logger(0, $command.' succeeded', Atexo_Config::getParameter('PEAR_LOG_DEBUG'));
            } else {
                self::db_logger($command_result, '(system) '.$command.' failed ('.$command_result.') - '.self::db_read_file($command_output_file), Atexo_Config::getParameter('PEAR_LOG_ERR'), true);
            }
        } else {
            self::db_logger(-1, '(system) Cannot execute command '.$command, Atexo_Config::getParameter('PEAR_LOG_ERR'), true);
        }
        @unlink($command_output_file);
        self::db_call_stack_pop();

        return $result;
    }

    private function db_synchronize_remote_blob_file($db_host, $database, $id, $revision)
    {
        self::db_call_stack_push('db_synchronize_remote_blob_file');
        self::db_reset_error();
        $result = false;
        $blob_directory_path = self::db_get_blob_storage_directory_path($db_host, $database);
        $blob_file_name = self::db_get_blob_storage_file_name($id, $revision);
        $blob_file_path = self::db_get_blob_storage_file_path($db_host, $database, $id, $revision);
        $failure_counter = 0;

        foreach ($this->_db_remote_servers as $remote_server) {
            $system_command = 'sudo /usr/bin/rsync --rsh=ssh -a --delete --include "'.$blob_file_name.'" --exclude "/*" --exclude "/*/" '.escapeshellarg($blob_directory_path).'/ '
            .Atexo_Config::getParameter('DB_REMOTE_SYNCHRO_USER').'@'.$remote_server.':'.$blob_directory_path;
            if (0 === self::db_system_command($system_command)) {
                self::db_logger(
                    0,
                    'Blob ID '.$id.' / revision '.$revision.' in database '.$database.'@'.$db_host.' ('.$blob_file_path.') synchronized on '.$remote_server,
                    Atexo_Config::getParameter('PEAR_LOG_DEBUG')
                );
            } else {
                ++$failure_counter;
            }
        }
        if (!$failure_counter) {
            $result = true;
        }
        self::db_call_stack_pop();

        return $result;
    }

    public function db_get_blob_storage_directory_path($db_host, $database, $repertoire = null, $insert = false)
    {
        $mnts = [];
        try {
            if ('' != Atexo_Config::getParameter('DOCUMENT_ROOT_DIR_MULTIPLE')) {
                $mnts = explode(';', Atexo_Config::getParameter('DOCUMENT_ROOT_DIR_MULTIPLE'));
                $baseRootDir = $mnts[0];
            } else {
                $baseRootDir = Atexo_Config::getParameter('BASE_ROOT_DIR');
                $mnts[0] = $baseRootDir;
            }

            if (false === $insert) {
                if (null === $repertoire) {
                    $repertoire = $database.'/'.Atexo_Config::getParameter('FILES_DIR');
                }
                $documentRootDirMultiple = [];
                foreach ($mnts as $mnt) {
                    $documentRootDirMultiple[] = $mnt.$repertoire;
                }

                return $documentRootDirMultiple;
            } else {
                if (null !== $repertoire) {
                    $dir = $baseRootDir.$repertoire.'/';
                } else {
                    $dir = self::db_get_blob_storage_directory_path_init($db_host, $database, $baseRootDir);
                }
                $dir = str_replace('//', '/', $dir);

                return $dir;
            }
        } catch (\Exception $exception) {
            self::db_logger(1, 'Test', Atexo_Config::getParameter('PEAR_LOG_ERR'), true);
            throw new Exception($exception->getMessage());
        }
    }

    public static function db_get_blob_storage_directory_path_init($db_host, $database, $baseRootDir)
    {
        try {
            $dbManagement = new Atexo_Blob_DbManagement();
            if (Atexo_Config::getParameter('DB_BLOB_DIR') && is_dir(Atexo_Config::getParameter('DB_BLOB_DIR'))) {
                $dir = Atexo_Config::getParameter('DB_BLOB_DIR').'/'.self::db_get_blob_storage_directory_name($db_host, $database);
            } else {

                $database = $dbManagement->removePrefix($database);
                if ($database == Atexo_Config::getParameter('COMMON_BLOB')) {
                    $database = Atexo_Config::getParameter('COMMON_BLOB');
                }
                $dir = $baseRootDir."/$database/".Atexo_Config::getParameter('FILES_DIR');
            }
        } catch (\Exception) {
            $database = $dbManagement->removePrefix($database);
            if ($database == Atexo_Config::getParameter('COMMON_BLOB')) {
                $database = Atexo_Config::getParameter('COMMON_BLOB');
            }
            $dir = Atexo_Config::getParameter('BASE_ROOT_DIR')."/$database/".Atexo_Config::getParameter('FILES_DIR');

            return $dir;
        }

        return $dir;
    }

    private function db_get_blob_storage_file_name($id, $revision, $extension = '')
    {
        return $id.'-'.$revision.$extension;
    }

    private function db_bind_param($stmt, $params)
    {
        foreach ($params as $key => $value) {
            $stmt->bindParam($key, $value);
        }

        return $stmt;
    }

    private function db_get_blob_storage_directory_name($db_host, $database)
    {
        $dbManagement = new Atexo_Blob_DbManagement();
        $database = $dbManagement->removePrefix($database);
        $db_host = Atexo_Config::getParameter('HOSTSPEC');
        if ('127.0.0.1' == $db_host) {
            $db_host = 'localhost';
        }
        if (0 == strcasecmp($database, Atexo_Config::getParameter('COMMON_BLOB'))) {
            $res = Atexo_Config::getParameter('COMMON_BLOB');
        } else {
            $res = sha1('localhost'.'/'.$database);
        }

        return $res;
    }

    public function db_read_file($file_name)
    {
        self::db_call_stack_push('db_read_file');
        self::db_reset_error();
        $result = false;
        $file_size = @filesize($file_name);
        if (false !== $file_size) {
            $file_resource = @fopen($file_name, 'r');
            if (false !== $file_resource) {
                $file_data = @fread($file_resource, $file_size);
                if (false !== $file_data) {
                    $result = $file_data;
                    self::db_logger(0, 'File '.$file_name.' successfully read', Atexo_Config::getParameter('PEAR_LOG_DEBUG'));
                } else {
                    self::db_logger(-1, '(fread) Cannot read file '.$file_name, Atexo_Config::getParameter('PEAR_LOG_ERR'), true);
                }
                if (false === @fclose($file_resource)) {
                    self::db_logger(-1, '(fclose) Cannot close file '.$file_name, Atexo_Config::getParameter('PEAR_LOG_WARNING'), true);
                }
            } else {
                self::db_logger(-1, '(fopen) Cannot open file '.$file_name, Atexo_Config::getParameter('PEAR_LOG_ERR'), true);
            }
        } else {
            self::db_logger(-1, '(filesize) Cannot retrieve size of file '.$file_name, Atexo_Config::getParameter('PEAR_LOG_ERR'), true);
        }
        self::db_call_stack_pop();

        return $result;
    }

    public function db_delete_blob($db_host, $db_login, $db_password, $database, $blob_id)
    {
        self::db_call_stack_push('db_delete_blob');
        self::db_reset_error();
        $result = false;
        self::db_logger(0, 'Deleting blob ID '.$blob_id.' from database '.$database.'@'.$db_host, Atexo_Config::getParameter('PEAR_LOG_INFO'));
        $blob_resource = self::db_acquire_lock_on_blob($db_host, $db_login, $db_password, $database, $blob_id, true);
        if (false !== $blob_resource) {
            $blob_revision = 0;
            $chemin = $blob_resource['blob']['chemin'];
            $extension = $blob_resource['blob']['extension'];

            $blob_file = self::db_get_blob_storage_file_path($db_host, $database, $blob_id, $blob_revision, $chemin, false, null, $extension);
            self::db_logger(0, 'Blob '.$blob_id.' deleted ', Atexo_Config::getParameter('PEAR_LOG_DEBUG'));
            $params = [];
            if ($database == Atexo_Config::getParameter('COMMON_BLOB')) {//si un blob d'entreprise
                $query = 'DELETE FROM `blob_file` WHERE `id` = :id';
                $params[':id'] = $blob_id;
            } else {
                $query = 'DELETE FROM `blobOrganisme_file` WHERE `id` = :id AND `organisme`=:organisme';
                $params[':id'] = $blob_id;
                $params[':organisme'] = $database;
            }
            $stm = $blob_resource['db_resource']->prepare($query);
            $stm->execute($params);
            if (false !== $stm) {
                if ($stm->rowCount()) {
                    self::db_logger(
                        0,
                        'File information deleted for blob ID '.$blob_id.' / revision '.($blob_revision),
                        Atexo_Config::getParameter('PEAR_LOG_DEBUG')
                    );
                    if (0 === self::db_system_command('unlink '.escapeshellarg($blob_file))) {
                        self::db_logger(0, 'File deleted ', Atexo_Config::getParameter('PEAR_LOG_DEBUG'));
                        $continue = false;
                        if (1 == Atexo_Config::getParameter('DB_REMOTE_SYNCHRO_ENABLED')) {
                            if (self::db_synchronize_remote_blob_file($db_host, $database, $blob_id, $blob_revision)) {
                                $continue = true;
                            } elseif (1 != Atexo_Config::getParameter('DB_REMOTE_SYNCHRO_CRITICAL')) {
                                $continue = true;
                                self::db_logger(-1, '(db_synchronize_remote_blob_file) Blob file synchronization failed ('.$blob_file.')', Atexo_Config::getParameter('PEAR_LOG_WARNING'), true);
                            }
                        } else {
                            $continue = true;
                        }
                        if ($continue) {
                            if (self::db_commit_transaction($blob_resource['db_resource'])) {
                                $result = true;
                                if (1 != Atexo_Config::getParameter('DB_GARBAGE_COLLECTOR_ENABLED')) {
                                    self::db_system_command('rm '.escapeshellarg($blob_resource['blob']['pointer']));
                                    if (1 == Atexo_Config::getParameter('DB_REMOTE_SYNCHRO_ENABLED')) {
                                        self::db_synchronize_remote_blob_file($db_host, $database, $blob_id, $blob_revision - 1);
                                    }
                                }
                            } else {
                                $error = self::db_save_error();
                                self::db_system_command('rm '.$blob_file);
                                if (1 == Atexo_Config::getParameter('DB_REMOTE_SYNCHRO_ENABLED')) {
                                    self::db_synchronize_remote_blob_file($db_host, $database, $blob_id, $blob_revision);
                                }
                                self::db_rollback_transaction($blob_resource['db_resource']);
                                self::db_restore_error($error);
                            }
                        } else {
                            $error = self::db_save_error();
                            self::db_system_command('rm '.escapeshellarg($blob_file));
                            if (1 == Atexo_Config::getParameter('DB_REMOTE_SYNCHRO_ENABLED')) {
                                self::db_synchronize_remote_blob_file($db_host, $database, $blob_id, $blob_revision);
                            }
                            self::db_rollback_transaction($blob_resource['db_resource']);
                            self::db_restore_error($error);
                        }
                    } else {
                        $error = self::db_save_error();
                        self::db_rollback_transaction($blob_resource['db_resource']);
                        self::db_restore_error($error);
                    }
                } else {
                    self::db_logger(
                        1,
                        '(mysql_query) Cannot find file information for blob ID '.$blob_id.' / revision '.($blob_revision - 1),
                        Atexo_Config::getParameter('PEAR_LOG_ERR'),
                        true
                    );
                    $error = self::db_save_error();
                    self::db_rollback_transaction($blob_resource['db_resource']);
                    self::db_restore_error($error);
                }
            } else {
                self::db_logger(
                    1,
                    '(mysql_query) Cannot update file information for blob ID '.$blob_id.' / revision '.($blob_revision - 1),
                    Atexo_Config::getParameter('PEAR_LOG_ERR'),
                    true
                );
                $error = self::db_save_error();
                self::db_rollback_transaction($blob_resource['db_resource']);
                self::db_restore_error($error);
            }
            $error = self::db_save_error();
            self::db_close_connection($blob_resource['db_resource']);
            self::db_restore_error($error);
        }
        self::db_call_stack_pop();

        return $result;
    }

    public function db_mv_extension_blob($db_host, $db_login, $db_password, $database, $blob_id, $subfolder, $extension = '')
    {
        $stmExt = null;
        self::db_call_stack_push('db_mv_extension_blob');
        self::db_reset_error();
        $result = false;
        $blob_revision = 0;
        self::db_logger(0, 'change extension blob '.$blob_id.' ('.$extension.') in database '.$database.'@'.$db_host, Atexo_Config::getParameter('PEAR_LOG_INFO'));
        $db_resource = self::db_open_connection($db_host, $db_login, $db_password);
        $commonDB = Atexo_Config::getParameter('DB_PREFIX').'_'.Atexo_Config::getParameter('COMMON_DB');
        if (false !== $db_resource) {
            if (self::db_select_database($db_resource, $commonDB)) {
                if (self::db_start_transaction($db_resource)) {
                    $params = [];
                    $params[':id'] = $blob_id;
                    $params[':revision'] = $blob_revision;
                    if ($database == Atexo_Config::getParameter('COMMON_BLOB')) {//si un blob d'entreprise
                        $query = 'SELECT `id`, `revision`, `name`, `chemin`, `extension` , `old_id` FROM `blob_file` WHERE `id` = :id AND `revision` = :revision';
                    } else {
                        $query = 'SELECT `id`, `organisme`, `revision`, `name`, `chemin`, `extension` , `old_id` FROM `blobOrganisme_file` WHERE `id`= :id AND `organisme`=:organisme AND `revision` = :revision';
                        $params[':organisme'] = $database;
                    }
                    $stm = $db_resource->prepare($query);
                    $stm->execute($params);
                    $extensionCurrent = '';
                    if (false !== $stm) {
                        $query_result_parsed = self::db_parse_query_result($db_resource, $stm);

                        if (is_countable($query_result_parsed) ? count($query_result_parsed) : 0) {
                            self::db_logger(0, 'File information for blob ID '.$blob_id.' / revision '.$blob_revision.' found ', Atexo_Config::getParameter('PEAR_LOG_DEBUG'));
                            $result = ['db_resource' => $db_resource, 'blob' => $query_result_parsed[0]];
                            $blob_id = self::getBlobId($result['blob']['id'], $result['blob']['old_id']);
                            $chemin = null;
                            if (null !== $result['blob']['chemin']) {
                                $chemin = $result['blob']['chemin'];
                            }
                            if (null !== $result['blob']['extension']) {
                                $extensionCurrent = $result['blob']['extension'];
                            }
                            if ($extensionCurrent != $extension) {
                                $pathBlob = self::db_get_blob_storage_file_path($db_host, $database, $blob_id, $blob_revision, $chemin, false, $subfolder, $extensionCurrent);
                                $newPathBlob = str_replace($extensionCurrent, $extension, $pathBlob);
                                try {
                                    $params = [];
                                    $params[':extension'] = $extension;
                                    $params[':id'] = $blob_id;
                                    if ($database == Atexo_Config::getParameter('COMMON_BLOB')) {
                                        $queryUpdateHash = 'Update `blob_file` set extension = :extension where id = :id';
                                    } else {
                                        $queryUpdateHash = 'Update `blobOrganisme_file` set extension = :extension where id = :id';
                                    }
                                    $stmExt = $db_resource->prepare($queryUpdateHash);
                                    $stmExt->execute($params);
                                } catch (Exception $exception) {
                                    self::db_logger(
                                        -1,
                                        "Impossible de mette a jour l'extension".$exception->getMessage()
                                        .' '.$exception->getTraceAsString(),
                                        Atexo_Config::getParameter('PEAR_LOG_WARNING'),
                                        true
                                    );
                                }
                                if (false !== $stmExt) {
                                    if (0 === self::db_system_command('mv '.escapeshellarg($pathBlob).' '.escapeshellarg($newPathBlob))) {
                                        if (!self::db_commit_transaction($db_resource)) {
                                            $error = self::db_save_error();
                                            self::db_system_command('mv '.escapeshellarg($newPathBlob).' '.escapeshellarg($pathBlob));
                                            self::db_rollback_transaction($db_resource);
                                            self::db_restore_error($error);
                                        }
                                    } else {
                                        $error = self::db_save_error();
                                        self::db_rollback_transaction($db_resource);
                                        self::db_restore_error($error);
                                    }
                                } else {
                                    self::db_logger(1, '(mysql_query) Cannot mv file information for blob ID '.$blob_id.' / revision 0 / new extension '.$extension, Atexo_Config::getParameter('PEAR_LOG_ERR'), true);
                                    $error = self::db_save_error();
                                    self::db_rollback_transaction($db_resource);
                                    self::db_restore_error($error);
                                }
                            } else {
                                self::db_logger(2, '(new extension is the same as current extension) Cannot mv file information for blob ID '.$blob_id.' / revision 0 / extension '.$extension, Atexo_Config::getParameter('PEAR_LOG_ERR'), true);
                            }
                        } else {
                            self::db_logger(
                                1,
                                '(mysql_query) File information for blob ID '.$blob_id.' / revision '.$blob_revision.'/ new extension '.$extension.' not found ',
                                Atexo_Config::getParameter('PEAR_LOG_ERR'),
                                true
                            );
                            $error = self::db_save_error();
                            self::db_rollback_transaction($db_resource);
                            self::db_restore_error($error);
                        }
                    } else {
                        self::db_logger(
                            1,
                            '(mysql_query) Cannot find file information for blob ID '.$blob_id.' / revision '.$blob_revision,
                            Atexo_Config::getParameter('PEAR_LOG_ERR'),
                            true
                        );
                        $error = self::db_save_error();
                        self::db_rollback_transaction($db_resource);
                        self::db_restore_error($error);
                    }
                }
            }
            $error = self::db_save_error();
            self::db_restore_error($error);
        }
        self::db_call_stack_pop();
    }
}
