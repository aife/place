<?php

namespace Application\Service\Atexo;

use Exception;
use Prado\Prado;
use Prado\Util\TLogger;

/*
 * commentaires
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 * @version 1.0
 * @since MPE-3.0
 * @package
 * @subpackage
 */

if (!defined('HOST_SPEC')) {
    define('HOST_SPEC', '127.0.0.1:3307');
}
if (!defined('USERNAME')) {
    define('USERNAME', 'root');
}
if (!defined('PASSWORD')) {
    define('PASSWORD', '');
}

class Atexo_UpdateDateMiseEnLigneCalculeOrganisme
{
    public function run()
    {
        $baseCommune = $_SERVER['argv'][1];
        $sql = "select organisme, id, date_mise_en_ligne_calcule from consultation where date_mise_en_ligne_calcule IS NOT NULL AND date_mise_en_ligne_calcule NOT LIKE '0000-00-00%' ";
        echo $sql."\r\n\r\n";
        self::connectToMysql($baseCommune, HOST_SPEC, USERNAME, PASSWORD);
        $rs = mysqli_query($sql);
        self::closeMysqlConnection();
        while ($row = mysqli_fetch_assoc($rs)) {
            self::connectToMysql($row['organisme'], HOST_SPEC, USERNAME, PASSWORD);
            $sql = "UPDATE consultation set date_mise_en_ligne_calcule='".$row['date_mise_en_ligne_calcule']."' Where id ='".$row['id']."'";
            echo $row['organisme'].' => '.$sql."\r\n";
            self::closeMysqlConnection();
        }
    }

    public function connectToMysql($db, $host, $userName, $passWord)
    {
        try {
            $connection = mysqli_connect($host, $userName, $passWord);
            mysqli_select_db($db, $connection);
        } catch (Exception $e) {
            Prado::log('Erreur UpdateDateMiseEnLigneCalculeOrganisme.php'.$e->getMessage().$e->getTraceAsString(), TLogger::ERROR, 'UpdateDateMiseEnLigneCalculeOrganisme.php');
        }
    }

    public function closeMysqlConnection()
    {
        mysqli_close();
    }
}
