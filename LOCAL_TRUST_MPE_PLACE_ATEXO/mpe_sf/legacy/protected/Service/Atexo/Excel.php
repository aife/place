<?php

namespace Application\Service\Atexo;

use PhpOffice\PhpSpreadsheet\Spreadsheet as PHPExcel;
use PhpOffice\PhpSpreadsheet\IOFactory as PHPExcel_IOFactory;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate as PHPExcel_Cell;
use PhpOffice\PhpSpreadsheet\Style\Border as PHPExcel_Style_Border;
use PhpOffice\PhpSpreadsheet\Style\Fill as PHPExcel_Style_Fill;
use PhpOffice\PhpSpreadsheet\Worksheet\PageSetup as PHPExcel_Worksheet_PageSetup;
use PhpOffice\PhpSpreadsheet\Cell\DataType as PHPExcel_Cell_DataType;

/**
 * Cette classe herite d'une librairie externe pour la gestion des fichiers Excel.
 * en date du 2014-09-08 elle utilise PHPExcel 1.8.0.
 *
 * @copyright Atexo 2014
 * @author Gregory CHEVRET <gregory.chevret@atexo.com>
 *
 * @version 1.0
 *
 * @since 4.7.0
 */
class Atexo_Excel extends PHPExcel
{

    /**
     * Le type du Writer (= 'Excel5' pour xls, 'Excel2007' pour xlsx).
     *
     * @var string
     */
    public $typeWriter = 'Xls';

    /**
     * Le nom du fichier.
     *
     * @var string
     */
    public $nameFile = null;

    /**
     * Le nom du fichier construit sous forme de "$this->directory/$this->nameFile" et utilise pour le telechargement.
     *
     * @var string
     */
    public $filename = '/tmp/document_excel.xls';

    /**
     * Le nom du fichier par defaut.
     *
     * @var string
     */
    public $nameFileDefault = 'document_excel.xls';

    /**
     * Le type de fichier à générer (xls, xlsx, pdf, zip).
     *
     * @var string
     */
    public $typeFile = 'xls';

    /**
     * Le repertoire de telechargement.
     *
     * @var string
     */
    public $directoryFile = '/tmp/';

    /**
     * Document excel.
     *
     * @var PHPExcel
     */
    public $excelDoc;

    /**
     * Le chemin du modele de document a generer.
     *
     * @var string
     */
    public $cheminModelDoc = '';

    /**
     * Gestion du message d'erreur.
     *
     * @var string
     */
    public $errorMsg = 'error ATX_XLS';

    /**
     * Affectes le le nom du fichier (nameFile).
     *
     * @param string $value
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function setNameFile($value)
    {
        $this->nameFile = $value;
        $this->setFilename();
    }

    /**
     * Affectes le chemin du modele de document.
     *
     * @param string $value
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function setCheminModelDoc($value)
    {
        if (null !== $value) {
            $this->cheminModelDoc = $value;
        }
    }

    /**
     * Permet de sauvegarder le document en fichier.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function writeExcelDocument()
    {
        $objWriter = PHPExcel_IOFactory::createWriter($this->excelDoc, $this->typeWriter);
        $this->setFilename();
        $objWriter->save($this->filename);
    }

    /**
     * Permet d'envoyer le fichier vers le navigateur pour telechargement.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function telechargerExcelDocument()
    {
        if ($this->excelDoc instanceof PHPExcel) {
            ob_clean();
            if ('xls' == $this->typeFile) {
                header('Content-Type: application/vnd.ms-excel');
                header('Content-Disposition: attachment;filename="'.$this->nameFile.'"');
                header('Cache-Control: max-age=0');
                $objPHPExcel = PHPExcel_IOFactory::load($this->filename);
                $objWriter = PHPExcel_IOFactory::createWriter($this->excelDoc, $this->typeWriter);
                $objWriter->save('php://output');
            }
        } elseif ('xlsx' == $this->typeFile) {
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename="'.$this->nameFile.'"');
            header('Cache-Control: max-age=0');
            $objPHPExcel = PHPExcel_IOFactory::load($this->filename);
            $objWriter = PHPExcel_IOFactory::createWriter($this->excelDoc, $this->typeWriter);
            $objWriter->save('php://output');
        } elseif ('zip' == $this->typeFile) {
            header('Content-Type: application/zip');
            header('Content-Length: '.filesize($this->filename));
            header('Content-Disposition: attachment;filename="'.$this->nameFile.'"');
            header('Cache-Control: max-age=0');
            echo file_get_contents($this->filename);
        } elseif ('pdf' == $this->typeFile) {
            header('Content-Type: application/pdf');
            header('Content-Disposition: attachment;filename="'.$this->nameFile.'"');
            header('Cache-Control: max-age=0');
            $objPHPExcel = PHPExcel_IOFactory::load($this->filename);
            $objPHPExcel->getActiveSheet()->setShowGridLines(false);
            $objPHPExcel->getActiveSheet()->getPageSetup()->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);

            $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'PDF');
            $objWriter->save('php://output');
        }
        exit;
    }

    /**
     * Permet d'affecter le repertoire de telechargement.
     *
     * @param string $directory: le chemin du repertoire de telechargement
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function setDirectoryFile($directory = null)
    {
        if (!$directory) {
            $directory = Atexo_Config::getParameter('EXCEL_DIRECTORY');
        }
        if (!is_dir($directory)) {
            mkdir($directory, 0755, true);
        }
        $this->directoryFile = $directory;
        $this->setFilename();
    }

    /**
     * Permet d'affecter le nom du fichier
     * A definir apres setDirectoryFile().
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function setFilename()
    {
        $this->filename = $this->directoryFile.(!($this->nameFile) ? $this->nameFileDefault : $this->nameFile);
    }

    /**
     * Permet de charger à partir d'un module de document.
     *
     * @param string $modele: le modele de document
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function chargerDepuisModele($modele)
    {
        if (file_exists($modele)) {
            $this->setCheminModelDoc($modele);
            $objReader = PHPExcel_IOFactory::createReader($this->typeWriter);
            $this->excelDoc = $objReader->load($this->cheminModelDoc);
        } else {
            new \Exception($this->errorMsg." : le modele $modele n'est pas accessible");
        }
    }

    /**
     * Affectes le createur  a l'excel.
     *
     * @param string $creator
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function setCreator($creator)
    {
        if ($this->excelDoc instanceof PHPExcel) {
            $this->excelDoc->getProperties()->setCreator($creator);
        }
    }

    /**
     * Affectes l'auteur de la derniere modification a l'excel.
     *
     * @param string $value
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function setLastModifiedBy($value)
    {
        if ($this->excelDoc instanceof PHPExcel) {
            $this->excelDoc->getProperties()->setLastModifiedBy($value);
        }
    }

    /**
     * Affectes le titre a l'excel.
     *
     * @param string $value
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function setTitle($value)
    {
        if ($this->excelDoc instanceof PHPExcel) {
            $this->excelDoc->getProperties()->setTitle($value);
        }
    }

    /**
     * Affectes le sujet a l'excel.
     *
     * @param string $value
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function setSubject($value)
    {
        if ($this->excelDoc instanceof PHPExcel) {
            $this->excelDoc->getProperties()->setSubject($value);
        }
    }

    /**
     * Affectes la description a l'excel.
     *
     * @param string $value
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function setDescription($value)
    {
        if ($this->excelDoc instanceof PHPExcel) {
            $this->excelDoc->getProperties()->setDescription($value);
        }
    }

    /**
     * Affectes les mots cles a l'excel.
     *
     * @param string $value
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function setKeywords($value)
    {
        if ($this->excelDoc instanceof PHPExcel) {
            $this->excelDoc->getProperties()->setKeywords($value);
        }
    }

    /**
     * Affectes la valeur a une cellule de l'excel.
     *
     * @param int    $activeSheetIndex: index de activeSheet
     * @param string $cellule:          la cellule dont on souhaite affecter une valeur
     * @param mixed  $value:            la valeur de la cellule
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function setCellValue($activeSheetIndex, $cellule, $value)
    {
        if ($this->excelDoc instanceof PHPExcel) {
            $this->excelDoc->setActiveSheetIndex($activeSheetIndex)
                   ->setCellValueExplicit($cellule, Atexo_Util::toUtf8($value), PHPExcel_Cell_DataType::TYPE_STRING);
        }
    }

    /**
     * Permet d'ecrire dans une cellule en utilisant son index.
     *
     * @param int  $pColumn
     * @param int  $pRow
     * @param null $pValue
     * @param bool $returnCell
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since 2017-place
     *
     * @copyright Atexo 2017
     */
    public function setCellValueByColumnAndRow($pColumn = 0, $pRow = 1, $pValue = null, $returnCell = false)
    {
        if ($this->excelDoc instanceof PHPExcel) {
            $this->excelDoc->getActiveSheet()->setCellValueByColumnAndRow($pColumn, $pRow, Atexo_Util::toUtf8($pValue), $returnCell);
        }
    }

    /**
     * Recupere la ligne max.
     *
     * @return int
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since 2017-place
     *
     * @copyright Atexo 2017
     */
    public function getHighestRow()
    {
        if ($this->excelDoc instanceof PHPExcel) {
            return $this->excelDoc->setActiveSheetIndex(0)->getHighestRow();
        }
    }

    /**
     * Recupere la colonne max.
     *
     * @return string
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since 2017-place
     *
     * @copyright Atexo 2017
     */
    public function getHighestColumn()
    {
        if ($this->excelDoc instanceof PHPExcel) {
            return $this->excelDoc->setActiveSheetIndex(0)->getHighestColumn();
        }
    }

    /**
     * @param $column
     * @param $row
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since 2017-place
     *
     * @copyright Atexo 2017
     */
    public function getCellByColumnAndRow($column, $row)
    {
        if ($this->excelDoc instanceof PHPExcel) {
            $this->excelDoc->getActiveSheet()->getCellByColumnAndRow($column, $row)->getValue();
        }
    }

    /**
     * Permet de recuperer l'index de la colonne a partir du nom de la cellule.
     *
     * @param $cell
     *
     * @return int
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since 2017-place
     *
     * @copyright Atexo 2017
     */
    public function getColumnIndexFromString($cell)
    {
        return PHPExcel_Cell::columnIndexFromString($cell);
    }

    /**
     * Permet de recuperer le nom de la cellule a partir de l'index.
     *
     * @param $columnIndex
     *
     * @return string
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since 2017-place
     *
     * @copyright Atexo 2017
     */
    public function getStringFromColumnIndex($columnIndex)
    {
        return PHPExcel_Cell::stringFromColumnIndex($columnIndex);
    }

    /**
     * Permet de modifier la couleur d'arriere plan d'une cellule.
     *
     * @param string|array $cell            : cellule au format string (exemple A1) ou en index colonne et ligne array($colonneIndex, $ligneIndex)
     * @param string       $backGroundColor : couleur du background au format (exemple: FF0000)
     *
     * @throws \PHPExcel_Exception
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since 2017-place
     *
     * @copyright Atexo 2017
     */
    public function setBackGroundColor($cell, $backGroundColor)
    {
        if ($this->excelDoc instanceof PHPExcel) {
            if (is_array($cell) && !empty($cell)) {
                $cell = $this->convertCellIndexToStringFormat($cell);
            }
            $this->excelDoc->getActiveSheet()
                ->getStyle($cell)
                ->applyFromArray(
                    ['fill' => ['type' => PHPExcel_Style_Fill::FILL_SOLID, 'color' => ['rgb' => $backGroundColor],
                        ],
                    ]
                );
        }
    }

    /**
     * Permet de convertir les index d'une cellule en format string.
     *
     * @param array $arrayIndex : array($colonneIndex, $ligneIndex)
     *
     * @return string
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since 2017-place
     *
     * @copyright Atexo 2017
     */
    public function convertCellIndexToStringFormat($arrayIndex)
    {
        $cell = '';
        if (is_array($arrayIndex) && !empty($arrayIndex)) {
            $ligne = $arrayIndex[1];
            $colonneIndex = $arrayIndex[0];
            $cell = $this->getStringFromColumnIndex($colonneIndex).$ligne;
        }

        return $cell;
    }

    /**
     * Permet de modifier le format d'une cellule.
     *
     * @param string|array $cell : cellule au format string (exemple A1) ou en index colonne et ligne array($colonneIndex, $ligneIndex)
     * @param $arrayStyle
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since 2017-place
     *
     * @copyright Atexo 2017
     */
    public function setFormatCellule($cell, $arrayStyle)
    {
        if ($this->excelDoc instanceof PHPExcel) {
            $sheet = $this->excelDoc->getActiveSheet();
            if (is_array($cell) && !empty($cell)) {
                $cell = $this->convertCellIndexToStringFormat($cell);
            }
            $styleCell = $sheet->getStyle($cell);
            $styleFont = $styleCell->getFont();
            if (!empty($arrayStyle['bold'])) {
                $styleFont->setBold($arrayStyle['bold']);
            }
            if (!empty($arrayStyle['size'])) {
                $styleFont->setSize($arrayStyle['size']);
            }
            if (!empty($arrayStyle['name'])) {
                $styleFont->setName($arrayStyle['name']);
            }
            if (!empty($arrayStyle['style_color'])) {
                $styleFont->getColor()->setARGB($arrayStyle['style_color']);
            }
        }
    }

    /**
     * Permet de recuperer la valeur d'une cellule.
     *
     * @param string|array $cell : cellule au format string (exemple A1) ou en index colonne et ligne array($colonneIndex, $ligneIndex)
     *
     * @return mixed
     *
     * @throws \PHPExcel_Exception
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since 2017-place
     *
     * @copyright Atexo 2017
     */
    public function getCellValue($cell)
    {
        $cellValue = '';
        if ($this->excelDoc instanceof PHPExcel) {
            if (is_array($cell) && !empty($cell)) {
                $cell = $this->convertCellIndexToStringFormat($cell);
            }
            $cellValue = $this->excelDoc->getActiveSheet()->getCell($cell)->getValue();
        }

        return $cellValue;
    }

    /**
     * Permet d'ajouter les borders aux cellules.
     *
     * @param string|array $cell            : cellule au format string (exemple A1) ou en index colonne et ligne array($colonneIndex, $ligneIndex)
     * @param string       $backGroundColor : couleur du background au format (exemple: FF0000)
     *
     * @throws \PHPExcel_Exception
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since 2017-place
     *
     * @copyright Atexo 2017
     */
    public function setAllBorders($cell)
    {
        if ($this->excelDoc instanceof PHPExcel) {
            $styleArray = [
                'borders' => [
                    'allborders' => [
                        'style' => PHPExcel_Style_Border::BORDER_THIN,
                    ],
                ],
            ];
            $this->excelDoc->getActiveSheet()
                ->getStyle($cell)
                ->applyFromArray($styleArray);
        }
    }
}
