<?php

namespace Application\Service\Atexo;

use DomDocument;
use Prado\Prado;
use Prado\Util\TLogger;
use ScanFile;

/**
 * Description de la classe.
 *
 * @author Khalid Atlassi<khalid.atlassi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_ScanAntivirus
{
    /*
     * fonction de traitement du resultat de scan antivirus sous forme de xml
     * @parameter : $xml_String : Résultat du scan antivirus en xml
     * @return : $resultat : Tableau de tableau contenant le corps du message et status
     */
    public static function parseXml($xmlString)
    {
        if (empty($xmlString)) {
            return false;
        }
        try {
            $dom = new DomDocument();
            $dom->loadXML($xmlString);

            $elts = $dom->getElementsByTagName('reply');
            foreach ($elts as $elt) {
                if ($elt->hasAttribute('status')) {
                    return $elt->getAttribute('status');
                }
            }
        } catch (\Exception) {
            return false;
        }
    }

    public static function startScan($fileToScan)
    {
        $resScan = null;
        $errorMessage = 0;
        if (Atexo_Config::getParameter('ACTIVER_ATEXO_ANTIVIRUS')) {
            //Commencer le scan
            //Executer le scan Antivirus
            try {
                $resScan = (new ScanFile())->scan($fileToScan);
            } catch (\Exception $e) {
                //TODO
                Prado::log('Erreur ScanAntivirus.php'.$e->getMessage().$e->getTraceAsString(), TLogger::ERROR, 'ScanAntivirus.php');
            }
            //Analyse du resultat du scan Antivirus
            $status = self::parseXml($resScan);
            //Cas ou le scan trouve un virus
            //Affichage de l'erreur (Erreur bloquante)
            if ($status == Atexo_Config::getParameter('ANTIVIRUS_SCAN_ETAT_VIRUS')) {
                return prado::localize('MESSAGE_INFECTED_FILE');
            } elseif ($status == Atexo_Config::getParameter('ANTIVIRUS_SCAN_ETAT_ERROR')) {
                return prado::localize('ANTIVIRUS_DOESNT_EXIST');
            }
            //Fin code scan antivirus
        }

        return $errorMessage;
    }
}
