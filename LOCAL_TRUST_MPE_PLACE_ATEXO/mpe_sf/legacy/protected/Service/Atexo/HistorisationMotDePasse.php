<?php

namespace Application\Service\Atexo;

use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonHistorisationMotDePasse;
use Application\Propel\Mpe\CommonTInformationModificationPassword;
use Application\Service\Atexo\Config\Atexo_Config_Exception;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Inscrit;
use Application\Service\Atexo\HistorisationMotDePasse\Atexo_HistorisationMotDePasse_Responses;
use DateTime;

class Atexo_HistorisationMotDePasse
{
    /**
     * Permet de vérifié si le mot de passe courant n'a pas déjà été utilisé depuis les X derniers changement.
     *
     * @param $email
     * @param $nouveauMotDePasse
     *
     * @return bool
     *
     * @throws Atexo_Config_Exception
     * @throws PropelException
     */
    public function checkRecentPassword($email, $nouveauMotDePasse)
    {
        $passwordAlreadyUsed = false;
        $inscrit = (new Atexo_Entreprise_Inscrit())->retrieveInscritByMail($email);

        if (empty($inscrit)) {
            return $passwordAlreadyUsed;
        }

        $hashNouveauMotDePasse = Atexo_Config::getParameter('ENABLE_SHA256_PASSWORDS') ?
            hash('sha256', Atexo_Config::getParameter('SALT_POUR_MOT_DE_PASSE').$nouveauMotDePasse) :
            sha1($nouveauMotDePasse)
        ;

        $historiquesMotDePasse = (new Atexo_HistorisationMotDePasse_Responses())->retrieveInscritLastPasswordUpdate($inscrit->getId(), Atexo_Module::retrieveModules()->getEntrepriseMotsDePasseHistorises());

        if (is_array($historiquesMotDePasse)) {
            foreach ($historiquesMotDePasse as $historique) {
                if ($historique->getAncienMotDePasse() == $hashNouveauMotDePasse) {
                    $passwordAlreadyUsed = true;
                    break;
                }
            }
        }

        if (!$passwordAlreadyUsed) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB')
                .Atexo_Config::getParameter('CONST_READ_WRITE'));
            $historisationMDP = new CommonHistorisationMotDePasse();
            $historisationMDP->setAncienMotDePasse($inscrit->getMdp());
            $historisationMDP->setDateModification(new DateTime());
            $historisationMDP->setIdInscrit($inscrit->getId());
            $historisationMDP->save($connexion);
        }

        return $passwordAlreadyUsed;
    }

    /**
     * On vérifie si le mot de passe n'a pas été changé depuis n jours. (n correspond à getEntrepriseDureeVieMotDePasse).
     *
     * @return bool
     *
     * @throws Exception
     */
    public function hasExpiredPassword()
    {
        $lastHistorisation = (new Atexo_HistorisationMotDePasse_Responses())->retrieveInscritLastPasswordUpdate(Atexo_CurrentUser::getId());
        if (empty($lastHistorisation)) {
            return true;
        }

        $dateDerniereModificationPassword = new DateTime($lastHistorisation[0]->getDateModification());
        $dureeDeVie = Atexo_Module::retrieveModules()->getEntrepriseDureeVieMotDePasse();
        $dateDerniereModificationPassword->add(new \DateInterval('P'.$dureeDeVie.'D'));

        $exp = $dateDerniereModificationPassword->format('U');
        $now = new DateTime();
        $now = $now->format('U');
        if ($exp < $now) {
            return true;
        }

        return false;
    }

    public function getEntrepriseJeton()
    {
        $jeton = Atexo_Util::getJetonModificationPsw(Atexo_CurrentUser::getIdInscrit(), 'entreprise');
        self::saveInformationModification(Atexo_CurrentUser::getIdInscrit(), 'entreprise', Atexo_CurrentUser::getEmailInscrit(), $jeton);

        return $jeton;
    }

    public function redirectToChangePassword($contexte)
    {
        $jeton = self::getEntrepriseJeton();
        $contexte->getApplication()->getModule('auth')->logout();
        $contexte->response->redirect('index.php?page=Entreprise.ModifierPassword&jeton='.$jeton.'&typeUser=entreprise&expire=1');
    }

    private function saveInformationModification($idUser, $typeUser, $email, $jeton)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));

        $informationPsw = new CommonTInformationModificationPassword();
        $informationPsw->setIdUser($idUser);
        $informationPsw->setTypeUser($typeUser);
        $informationPsw->setEmail($email);
        $informationPsw->setJeton($jeton);
        $dateDebut = date('Y-m-d H:i:s');
        $informationPsw->setDateDemandeModification($dateDebut);
        $dateFin = Atexo_Util::dateDansFutur($dateDebut, Atexo_Config::getParameter('DUREE_VALIDITE_JETON_MODIFICATION_PASSWORD'));
        $informationPsw->setDateFinValidite($dateFin);
        $informationPsw->save($connexion);
    }
}
