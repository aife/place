<?php

namespace Application\Service\Atexo;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonCertificatChiffrement;
use Application\Propel\Mpe\CommonCertificatChiffrementPeer;
use Application\Propel\Mpe\CommonCertificatPermanent;
use Application\Propel\Mpe\CommonCertificatPermanentPeer;

/**
 * Classe de CurrentUser.
 *
 * @author Adil El KANABI <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Certificat
{
    /**
     *  Retourne un tableau de tous les certificats (personnel et de secours) du service passé en paramètre.
     *
     * @param : $idService : l'id du service , $org  : m'accronyme de l'organisme
     *
     * @return : array un tableau d'objet d'objet Certificatpermanent
     */
    public function retrievePermanentCertificatByService($idService, $org)
    {
        $arrayChilds = Atexo_EntityPurchase::getSubServices($idService, $org, true);
        $c = new Criteria();
        $c->add(CommonCertificatPermanentPeer::SERVICE_ID, $arrayChilds, Criteria::IN);
        $c->add(CommonCertificatPermanentPeer::ORGANISME, $org);
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $ceritificatPersonal = CommonCertificatPermanentPeer::doSelect($c, $connexionCom);

        return $ceritificatPersonal;
    }

    /**
     *  Retourne un tableau de tous les certificats permanent du service passé en paramètre.
     *
     * @param : $idService : l'id du service , $org  : m'accronyme de l'organisme
     *
     * @return : array un tableau d'objet d'objet Certificatpermanent
     */
    public function retrievePersonalPermanentCertificatByService($idService, $org)
    {
        $c = new Criteria();
        $c->add(CommonCertificatPermanentPeer::SERVICE_ID, $idService);
        $c->add(CommonCertificatPermanentPeer::ORGANISME, $org);
        $c->add(CommonCertificatPermanentPeer::MASTER_KEY, '0');
        $c->add(CommonCertificatPermanentPeer::CERTIFICAT_UNIVERSELLE, '0');
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $ceritificatPersonal = CommonCertificatPermanentPeer::doSelect($c, $connexionCom);

        return $ceritificatPersonal;
    }

    /**
     *  Retourne un tableau de tous les certificats de secours du service passé en paramètre.
     *
     * @param : $idService : l'id du service , $org  : m'accronyme de l'organisme
     *
     * @return : array un tableau d'objet d'objet Certificatpermanent
     */
    public function retrieveReliefPermanentCertificatByService($idService, $org)
    {
        $c = new Criteria();
        $c->add(CommonCertificatPermanentPeer::SERVICE_ID, $idService);
        $c->add(CommonCertificatPermanentPeer::ORGANISME, $org);
        $c->add(CommonCertificatPermanentPeer::MASTER_KEY, '1');
        $c->add(CommonCertificatPermanentPeer::CERTIFICAT_UNIVERSELLE, '0');
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $ceritificatPersonal = CommonCertificatPermanentPeer::doSelect($c, $connexionCom);

        return $ceritificatPersonal;
    }

    public function saveCertificat(CommonCertificatPermanent $certificat, $org)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $certificat->setOrganisme($org);
        $certificat->save($connexionCom);

        return $certificat;
    }

    public function retrieveCertificatByPk($idCertificat, $org)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $certificat = CommonCertificatPermanentPeer::retrieveByPK($idCertificat, $org, $connexionCom);

        return $certificat;
    }

    public function retrieveCertificatById($idCertificat, $org)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonCertificatPermanentPeer::ID, $idCertificat);
        $c->add(CommonCertificatPermanentPeer::ORGANISME, $org);
        $certificat = CommonCertificatPermanentPeer::doSelectOne($c, $connexionCom);
        if ($certificat) {
            return $certificat;
        } else {
            return false;
        }
    }

    public function deleteCertificat($idCertif, $org)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonCertificatPermanentPeer::ID, $idCertif);
        $c->add(CommonCertificatPermanentPeer::ORGANISME, $org);
        CommonCertificatPermanentPeer::doDelete($c, $connexionCom);
    }

    public function retrieveArrayBiCle($idService, $typeBiCle, $organisme, $returnArrayObject = false)
    {
        $biCles = null;
        if ($typeBiCle == Atexo_Config::getParameter('BI_CLE_PERMANENT')) {
            $biCles = self::retrievePersonalPermanentCertificatByService($idService, $organisme);
        } elseif ($typeBiCle == Atexo_Config::getParameter('BI_CLE_SECOURS')) {
            $biCles = self::retrieveReliefPermanentCertificatByService($idService, $organisme);
        }

        if ($returnArrayObject) {
            return $biCles;
        } else {
            $biCles = self::getArrayNomBiCleFromArrayObject($biCles);

            return $biCles;
        }
    }

    public function retrieveCertificatChiffrement($consultationId, $typeEnv, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonCertificatChiffrementPeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonCertificatChiffrementPeer::ORGANISME, $organisme);
        if ($typeEnv) {
            $c->add(CommonCertificatChiffrementPeer::TYPE_ENV, $typeEnv);
        }

        $certificats = CommonCertificatChiffrementPeer::doSelect($c, $connexion);
        if ($certificats) {
            return $certificats;
        } else {
            return [];
        }
    }

    /**
     * Supprime les certificats en doubles dans le tableau.
     *
     * @param array $certificats tableau d'objet CertificatPermanent
     *
     * @return array $certificats sans les doublons
     */
    public function removeDoublons(array $certificats)
    {
        $certificatsSansDoublons = $certificats;
        foreach ($certificats as $idCert => $certificat) {
            for ($indice = $idCert + 1; isset($certificats[$indice]); ++$indice) {
                $certificatTmp = $certificats[$indice];
                if (trim($certificatTmp->getCertificat()) == trim($certificat->getCertificat())) {
                    unset($certificatsSansDoublons[$indice]);
                }
            }
        }

        return $certificatsSansDoublons;
    }

    /**
     * Supprime les certificats en doubles par type de certificat (type d'enveloppe) dans le tableau.
     *
     * @param array $certificats: liste d'objets CertificatPermanent
     *
     * @return array: liste d'objets certificats permanenents sans doublons
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function removeDoublonsParTypeCertif(array $certificats)
    {
        $listeCertifSansDoublons = [];
        foreach ($certificats as $certificat) {
            if ($certificat instanceof CommonCertificatChiffrement) {
                $key = $certificat->getTypeEnv();
                if (!in_array($key, $listeCertifSansDoublons)) {
                    $listeCertifSansDoublons[$key] = $certificat;
                }
            }
        }

        return $listeCertifSansDoublons;
    }

    public function getArrayNomBiCleFromArrayObject($biCles)
    {
        if (is_array($biCles)) {
            $listeBiCle = [];
            foreach ($biCles as $unBiCle) {
                if ($unBiCle instanceof CommonCertificatPermanent) {
                    $listeBiCle[$unBiCle->getId()] = $unBiCle->getNom();
                }
            }

            return $listeBiCle;
        } else {
            return [];
        }
    }

    /**
     *  Retourne un tableau de tous les certificats universelles (personnel et de secours)e.
     */
    public function retrievePermanentCertificatUniverselle($acronyme = null)
    {
        $c = new Criteria();
        $c->add(CommonCertificatPermanentPeer::CERTIFICAT_UNIVERSELLE, '1');
        $crit0 = $c->getNewCriterion(CommonCertificatPermanentPeer::ORGANISME, null, Criteria::ISNULL);
        $crit1 = $c->getNewCriterion(CommonCertificatPermanentPeer::ORGANISME, '', Criteria::LIKE);
        $crit0->addOr($crit1);
        if ($acronyme) {
            $crit2 = $c->getNewCriterion(CommonCertificatPermanentPeer::ORGANISME, $acronyme, Criteria::LIKE);
            $crit1->addOr($crit2);
        }
        $c->add($crit0);
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $ceritificatPersonal = CommonCertificatPermanentPeer::doSelect($c, $connexionCom);

        return $ceritificatPersonal;
    }

    public function deleteCommonCertificats($organisme, $consultationId, $connexion = null)
    {
        if (!$connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        }
        $c = new Criteria();
        $c->add(CommonCertificatChiffrementPeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonCertificatChiffrementPeer::ORGANISME, $organisme);
        CommonCertificatChiffrementPeer::doDelete($c, $connexion);
    }
}
