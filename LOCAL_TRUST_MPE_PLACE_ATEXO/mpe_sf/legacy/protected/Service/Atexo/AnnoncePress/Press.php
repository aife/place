<?php

namespace Application\Service\Atexo\AnnoncePress;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonAnnoncePress;
use Application\Propel\Mpe\CommonAnnoncePressPeer;
use Application\Propel\Mpe\CommonAnnoncePressPieceJointe;
use Application\Propel\Mpe\CommonAnnoncePressPieceJointePeer;
use Application\Propel\Mpe\CommonDestinataireAnnoncePress;
use Application\Propel\Mpe\CommonDestinataireAnnoncePressPeer;
use Application\Propel\Mpe\CommonHistoriqueAvisPub;
use Application\Propel\Mpe\CommonHistoriqueAvisPubPeer;
use Application\Propel\Mpe\CommonJALPeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Prado\Prado;

/**
 * permet de gerer les annonce press.
 *
 * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
 *
 * @version 1.0
 *
 * @since 4.0.0
 *
 * @copyright Atexo 2011
 */
class Press
{
    /**
     * permet de recuperer une annonce press par son id.
     *
     * @param int    $id  de l'annoncePress
     * @param string $org l'accronyme de l'organisme
     *
     * @return CommonAnnoncePress $listForm
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.0.0
     *
     * @copyright Atexo 2014
     */
    public function retreiveListFormulairePressById($id, $org = null)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        if (!$org) {
            $org = Atexo_CurrentUser::getCurrentOrganism();
        }
        $c = new Criteria();
        $c->add(CommonAnnoncePressPeer::ID, $id);
        if ($org) {
            $c->add(CommonAnnoncePressPeer::ORGANISME, $org);
        }
        $listForm = CommonAnnoncePressPeer::doSelectOne($c, $connexionCom);

        if ($listForm) {
            return $listForm;
        } else {
            return [];
        }
    }

    /**
     * permet de recuperer une annonce press par idDestPress.
     *
     * @param int    $id  (champ id de la table Destinataire_Annonce_Press)
     * @param string $org l'accronyme de l'organisme
     *
     * @return CommonAnnoncePress $listForm
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.0.0
     *
     * @copyright Atexo 2014
     */
    public function retreiveListAnnoncPressByRefIdDestPub($id, $org = null)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        if (!$org) {
            $org = Atexo_CurrentUser::getCurrentOrganism();
        }
        $c = new Criteria();
        $c->add(CommonAnnoncePressPeer::ID_DEST_PRESS, $id);
        if ($org) {
            $c->add(CommonAnnoncePressPeer::ORGANISME, $org);
        }
        $listForm = CommonAnnoncePressPeer::doSelectOne($c, $connexionCom);

        if ($listForm) {
            return $listForm;
        } else {
            return [];
        }
    }

    /**
     * permet de supprimer une piece jointe d'une annonce presse.
     *
     * @param int    $idpj id de la PJ
     * @param string $org  l'accronyme de l'organisme
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.0.0
     *
     * @copyright Atexo 2014
     */
    public static function DeletePjAnnoncePress($idpj, $org = null)
    {
        if (!$org) {
            $org = Atexo_CurrentUser::getCurrentOrganism();
        }
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $PJ = CommonAnnoncePressPieceJointePeer::retrieveByPk($idpj, $org, $connexionCom);
        if (is_object($PJ)) {
            $PJ->delete($connexionCom);
        }
    }

    /**
     * permet de recuperer une piece jointe d'une annonce presse par son id.
     *
     * @param int    $idpj id de la PJ
     * @param string $org  l'accronyme de l'organisme
     *
     * @return CommonAnnoncePressPieceJointe $CommonAnnoncePressPieceJointe l'objet
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.0.0
     *
     * @copyright Atexo 2014
     */
    public function retreiveAnnoncePressPJByIdPiece($id, $org = null)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonAnnoncePressPieceJointePeer::PIECE, $id);
        if ($org) {
            $c->add(CommonAnnoncePressPieceJointePeer::ORGANISME, $org);
        }

        return CommonAnnoncePressPieceJointePeer::doSelectOne($c, $connexionCom);
    }

    /**
     * permet de recuperer le statut d'une annonce presse selon $idStatut.
     *
     * @param int $idStatut id du statut
     *
     * @return string le statut de l'annonce presse
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.0.0
     *
     * @copyright Atexo 2014
     */
    public function getStatutPress($idStatut)
    {
        if ($idStatut == Atexo_Config::getParameter('STATUT_AVIS_A_TRANSMETTRE')) {
            return Prado::localize('TEXT_STATUT_AVIS_A_TRANSMETTRE');
        } elseif ($idStatut == Atexo_Config::getParameter('STATUT_AVIS_ENVOYE')) {
            return Prado::localize('TEXT_STATUT_AVIS_ENVOYE');
        } elseif ($idStatut == Atexo_Config::getParameter('STATUT_AVIS_VALIDE')) {
            return Prado::localize('TEXT_STATUT_AVIS_VALIDE');
        } elseif ($idStatut == Atexo_Config::getParameter('STATUT_AVIS_REJETE')) {
            return Prado::localize('TEXT_SSTATUT_AVIS_REJETE');
        }
    }

    /**
     * permet de recuperer un destinataire d'une annonce presse.
     *
     * @param int    $idPress id de l'annonce presse
     * @param int    $idJal   id du journal
     * @param string $org     l'accronyme de l'organisme
     *
     * @return $listDest CommonDestinataireAnnoncePress le destinataire de l'annonce de presse
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.0.0
     *
     * @copyright Atexo 2014
     */
    public static function getDestinataireAnnoncePress($idPress, $idJal, $org = null)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        if (!$org) {
            $org = Atexo_CurrentUser::getCurrentOrganism();
        }
        $c = new Criteria();
        $c->add(CommonDestinataireAnnoncePressPeer::ID_ANNONCE_PRESS, $idPress);
        $c->add(CommonDestinataireAnnoncePressPeer::ID_JAL, $idJal);
        if ($org) {
            $c->add(CommonDestinataireAnnoncePressPeer::ORGANISME, $org);
        }
        $listDest = CommonDestinataireAnnoncePressPeer::doSelectOne($c, $connexionCom);

        if ($listDest) {
            return $listDest;
        } else {
            return [];
        }
    }

    /**
     * permet de supprimer un destinataire d'une annonce presse.
     *
     * @param int    $idDestinataire id du destinataire (champ id de la table Destinataire_Annonce_Press)
     * @param int    $idJal          id du journal
     * @param string $org            l'accronyme de l'organisme
     *
     * @return array $listDest liste de destinataires affecte
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.0.0
     *
     * @copyright Atexo 2014
     */
    public static function DeleteDestNonCoche($idDestinataire, $idJal, $org = null)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        if (!$org) {
            $org = Atexo_CurrentUser::getCurrentOrganism();
        }
        $c = new Criteria();
        $c->add(CommonDestinataireAnnoncePressPeer::ID_JAL, $idJal);
        $c->add(CommonDestinataireAnnoncePressPeer::ID, $idDestinataire);
        if ($org) {
            $c->add(CommonDestinataireAnnoncePressPeer::ORGANISME, $org);
        }
        $listDest = CommonDestinataireAnnoncePressPeer::doDelete($c, $connexionCom);

        if ($listDest) {
            return $listDest;
        } else {
            return [];
        }
    }

    /**
     * permet de recuperer l'historique d'une annonce presse.
     *
     * @param int    $idAvis id de l'avis
     * @param string $org    l'accronyme de l'organisme
     *
     * @return CommonHistoriqueAvisPub $listDest
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.0.0
     *
     * @copyright Atexo 2014
     */
    public static function getHistoriqueAnnoncePress($idAvis, $org = null)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        if (!$org) {
            $org = Atexo_CurrentUser::getCurrentOrganism();
        }
        $c = new Criteria();
        $c->add(CommonHistoriqueAvisPubPeer::ID_AVIS, $idAvis);
        if ($org) {
            $c->add(CommonHistoriqueAvisPubPeer::ORGANISME, $org);
        }
        $listDest = CommonHistoriqueAvisPubPeer::doSelectOne($c, $connexionCom);

        if ($listDest) {
            return $listDest;
        } else {
            return [];
        }
    }

    /**
     * permet de recuperer la liste des destinataire d'une annonce presse.
     *
     * @param int    $idAnnoncePress id de l'annonce presse
     * @param string $org            l'accronyme de l'organisme
     * @param bool   $joinJal        true pour fauire la jointure avec CommonJALPeer,sinon false
     *
     * @return CommonHistoriqueAvisPub $listDest
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.0.0
     *
     * @copyright Atexo 2014
     */
    public function getListeDestAnnoncePress($idAnnoncePress, $org, $joinJal = false)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->setDistinct();
        $c->add(CommonDestinataireAnnoncePressPeer::ID_ANNONCE_PRESS, $idAnnoncePress);
        $c->add(CommonDestinataireAnnoncePressPeer::ORGANISME, $org);
        if (!$joinJal) {
            $listeDestinataireAnnoncePress = CommonDestinataireAnnoncePressPeer::doSelect($c, $connexion);

            return $listeDestinataireAnnoncePress;
        } else {
            $c->addJoin(CommonDestinataireAnnoncePressPeer::ID_JAL, CommonJALPeer::ID);
            $c->addAlias('Jal', CommonJALPeer::TABLE_NAME);
            $c->addJoin(CommonDestinataireAnnoncePressPeer::ORGANISME, CommonJALPeer::alias('Jal', CommonJALPeer::ORGANISME));
            $listeDestinataireAnnoncePress = CommonJALPeer::doSelect($c, $connexion);

            return $listeDestinataireAnnoncePress;
        }
    }

    /**
     * permet de recuperer une annonce press par l'id desinataire.
     *
     * @param int    $id  l'id su destinataire presse
     * @param string $org l'accronyme de l'organisme
     *
     * @return CommonAnnoncePress $listForm
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.0.0
     *
     * @copyright Atexo 2014
     */
    public function getAnnoncePressByIdDest($id, $org = null)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        if (!$org) {
            $org = Atexo_CurrentUser::getCurrentOrganism();
        }
        $c = new Criteria();
        $c->add(CommonAnnoncePressPeer::ID_DEST_PRESS, $id);
        if ($org) {
            $c->add(CommonAnnoncePressPeer::ORGANISME, $org);
        }
        $listForm = CommonAnnoncePressPeer::doSelectOne($c, $connexionCom);

        return $listForm;
    }
}
