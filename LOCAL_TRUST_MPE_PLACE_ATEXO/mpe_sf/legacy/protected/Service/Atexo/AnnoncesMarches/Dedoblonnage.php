<?php

namespace Application\Service\Atexo\AnnoncesMarches;

use Application\Propel\Mpe\CommonConsultation;
use Application\Service\Atexo\Atexo_Consultation;

/**
 * Classe de regle de dedoblonnage.
 *
 * @author Khadija CHOUIKA <khadija.chouika@atexo.com>
 * @copyright Atexo 2012
 *
 * @version
 *
 * @since MPE-4.0
 */
class Atexo_AnnoncesMarches_Dedoblonnage
{
    private string $regle = '';

    /**
     * modifie la valeur de [regle].
     *
     * @param string $value la valeur a mettre
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setRegle($value)
    {
        $this->regle = $value;
    }

    /**
     * La régle de dedoblonnage selon Url figurant dans la balise <DOCUMENTS> <URl> du fichier xml boamp
     * on cherche si existe une consultation (url_consultation_externe) ayant le même url (ei : provenance d'une autre PF ).
     *
     * @param string $url     url_migration
     * @param string $orgnism l'organisme a exclure dans la recherche
     *
     * @return CommonConsultation $consultation la consultation si elle  existe,sinon false
     *
     * @author Khadija CHOUIKA <khadija.chouika@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.0.0
     *
     * @copyright Atexo 2012
     */
    public function regleUrl($url, $orgnism)
    {
        $consultation = (new Atexo_Consultation())->hasDoublon($url, $orgnism);
        if ($consultation instanceof CommonConsultation) {
            return $consultation;
        } else {
            return false;
        }
    }

    /**
     * Permet d'executer la regle de dedoublonnage.
     *
     * @param object $param
     * @param string $orgnism l'accronyme de l'organisme
     *
     * @return void
     *
     * @author Khadija CHOUIKA <khadija.chouika@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.0.0
     *
     * @copyright Atexo 2012
     */
    public function executeRegle($param, $orgnism)
    {
        return match ($this->regle) {
            1 => self::regleUrl($param, $orgnism),
            default => null,
        };
    }
}
