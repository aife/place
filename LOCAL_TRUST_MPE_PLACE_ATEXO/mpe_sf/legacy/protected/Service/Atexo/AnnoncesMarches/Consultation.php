<?php

namespace Application\Service\Atexo\AnnoncesMarches;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonCategorieLot;
use Application\Propel\Mpe\CommonCategorieLotPeer;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationPeer;
use Application\Propel\Mpe\CommonOrganisme;
use Application\Propel\Mpe\CommonTypeAvis;
use Application\Service\Atexo\Atexo_CategorieLot;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_ReferentielOrgDenomination;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Category;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureType;
use Application\Service\Atexo\Consultation\Atexo_Consultation_TypeAvis;
use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2;
use Application\Service\Atexo\Publicite\Atexo_Publicite_Boamp;
use AtexoDume\Dto\Lot;
use Exception;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * commentaires.
 *
 * @author mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_AnnoncesMarches_Consultation
{
    const ATEXO_CLI_ANNONCES_M_ARCHES = 'Atexo.Cli.AnnoncesMArches';
    const ORGANISME = 'Organisme ';

    /**
     * appel du web service et recuperation du flux de réponse.
     *
     * @param string $url l'url du webService
     * @param string $xml la chaine xml
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2008
     */
    public function webGetXmlCompanyTender($url, $xml)
    {
        $postData = [];
        $postData['xml_request'] = base64_encode($xml);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, Atexo_Config::getParameter('CURLOPT_CONNECTTIMEOUT'));
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);

        //definition du proxy si existe
        $urlProxy = Atexo_Config::getParameter('URL_PROXY');
        $portProxy = Atexo_Config::getParameter('PORT_PROXY');
        $curlOptProxy = $urlProxy.':'.$portProxy;
        if ('' != $urlProxy) {
            curl_setopt($ch, CURLOPT_PROXY, $curlOptProxy);
        }
        $postResult = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Erreur Curl : '.curl_error($ch);
        }

        return $postResult;
    }

    /**
     * Permet d'inserer des objets consultations et catégorie lot dans la base commune.
     *
     * @param CommonOrganisme $oneEntity
     * @param string          $xmlRequest la chaine xml
     *
     * @return string $postResult  le retour du ws
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 3.0.0
     *
     * @copyright Atexo 2014
     */
    public function insertTenders($oneEntity, $xmlRequest)
    {
        $xmlTender = null;
        $organismeName = null;
        Prado::log('Début Appel WS de la plate forme ', TLogger::INFO, self::ATEXO_CLI_ANNONCES_M_ARCHES);
        $url = $oneEntity->getUrlInterfaceAnm().'/?page=Entreprise.InterfaceAnnonceMarchesHTTPS';
        $url = str_replace('//?', '/?', $url); // l'appel HTTPS n'aime pas les doubles slashs
        $acronymeOrg = $oneEntity->getAcronyme();
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        try {
            $fileName = 'WebServiceAnnoncesMarches';
            echo 'Début Appel WS '.$url."\n";
            $zipContent = base64_decode($this->webGetXmlCompanyTender($url, $xmlRequest));
            if ($zipContent) {
                if (!is_dir(Atexo_Config::getParameter('COMMON_TMP').'/testAnnonceMarche/')) {
                    mkdir(Atexo_Config::getParameter('COMMON_TMP').'/testAnnonceMarche/');
                }
                $dirSave = Atexo_Config::getParameter('COMMON_TMP').'/testAnnonceMarche/tmpWSAnnoncesMarches'.date('Y_d_m_H_i_s').'_'.uniqid();
                mkdir($dirSave);
                Atexo_Util::write_file($dirSave.'/'.$fileName.'.zip', $zipContent);
                system('cd '.$dirSave.';unzip  '.$dirSave.'/'.$fileName.'.zip');
                if (is_file($dirSave.'/'.$fileName.'.xml')) {
                    $xmlTender = file_get_contents($dirSave.'/'.$fileName.'.xml');
                }
            } else {
                echo "Réponse vide \n";
                Prado::log($url.' : Réponse vide : ', TLogger::ERROR, self::ATEXO_CLI_ANNONCES_M_ARCHES);
            }
        } catch (Exception $e) {
            Prado::log('Erreur chargement XML Reponse : '.$e, TLogger::ERROR, self::ATEXO_CLI_ANNONCES_M_ARCHES);
        }
        try {
            if ($xmlTender) {
                $domXml = (new Atexo_Publicite_Boamp())->getXmlDocument($xmlTender);
                $nodeTender = $domXml->getElementsByTagName('Tender');
                $nombreTender = is_countable($nodeTender) ? count($nodeTender) : 0;
                echo "Nombre de consultation $nombreTender \n";
                $arrayNbreParOrg = [];

                for ($i = 0; $i < $nombreTender; ++$i) {
                    $organisme = (string) $nodeTender->item($i)->getAttribute('organism');
                    $organismeName = (string) $nodeTender->item($i)->getAttribute('organismeName');
                    $entitePublique = (string) $nodeTender->item($i)->getAttribute('entitePublique');
                    // Calcul de la nouvelle reference
                    $consultationId = (string) $nodeTender->item($i)->getAttribute('referenceConsultation');
                    $refUtilisateur = (string) $nodeTender->item($i)->getAttribute('reference');
                    $newRef = sha1($organisme.$consultationId);
                    $updateConsultation = true;
                    //Pour ferivier si la consultation éxiste déjà

                    if (!isset($arrayNbreParOrg[self::ORGANISME.$organisme])) {
                        $arrayNbreParOrg[self::ORGANISME.$organisme]['Consultations créés'] = 0;
                        $arrayNbreParOrg[self::ORGANISME.$organisme]['Consultations mises à jour'] = 0;
                    }
                    $c = new Criteria();
                    $c->add(CommonConsultationPeer::REF_ORG_PARTENAIRE, $newRef);
                    $c->add(CommonConsultationPeer::ORGANISME, $acronymeOrg);
                    $commonConsultation = CommonConsultationPeer::doSelectOne($c, $connexionCom);

                    if (!($commonConsultation instanceof CommonConsultation)) {
                        $commonConsultation = new CommonConsultation();
                        $commonConsultation->setOrganisme($acronymeOrg);
                        $commonConsultation->setRefOrgPartenaire($newRef);
                        $updateConsultation = false;
                        ++$arrayNbreParOrg[self::ORGANISME.$organisme]['Consultations créés'];
                    } else {
                        ++$arrayNbreParOrg[self::ORGANISME.$organisme]['Consultations mises à jour'];
                    }
                    // informations modalités de réponse
                    $electronicResponse = $nodeTender->item($i)->getAttribute('electronicResponse');
                    $digitalSignature = $nodeTender->item($i)->getAttribute('digitalSignature');
                    $electronicResponseObligatoire = $nodeTender->item($i)->getAttribute('electronicResponseObligatoire');

                    $dateMiseOnLigneCalcule = $nodeTender->item($i)->getAttribute('dateMiseOnLigneCalcule');
                    $type = (string) $nodeTender->item($i)->getAttribute('type');
                    $procedureType = $nodeTender->item($i)->getAttribute('procedureType');
                    $category = (string) $nodeTender->item($i)->getAttribute('category');
                    $cpv = $nodeTender->item($i)->getAttribute('cpv');
                    $closureDate = $nodeTender->item($i)->getAttribute('closureDate');
                    $deliveryPlace = $nodeTender->item($i)->getAttribute('deliveryPlace');
                    $nodeTitle = $nodeTender->item($i)->getElementsByTagName('Title');
                    $title = (string) $nodeTitle->item(0)->nodeValue;
                    $nodeSubject = $nodeTender->item($i)->getElementsByTagName('Subject');
                    $description = (string) $nodeSubject->item(0)->nodeValue;
                    $nodeUrl = $nodeTender->item($i)->getElementsByTagName('Url');
                    $url = $nodeUrl->item(0)->nodeValue;
                    $nodeUrlAvisPub = $nodeTender->item($i)->getElementsByTagName('UrlAvisPub');
                    $urlAvisPub = '';
                    if ($nodeUrlAvisPub) {
                        $urlAvisPub = $nodeUrlAvisPub->item(0)->nodeValue;
                    }

                    // Inserssion en base de l'objet consultation
                    $commonConsultation->setOrgDenomination($entitePublique);
                    $commonConsultation->setDenominationAdapte(strtoupper(Atexo_Util::formatterChaine($entitePublique)));
                    $commonConsultation->setReferenceUtilisateur($refUtilisateur);
                    $commonConsultation->setIdTypeProcedure((new Atexo_Consultation_ProcedureType())->retrieveTypeProcedureObjectByAbreviationAnm($procedureType, true));
                    $commonConsultation->setIdTypeProcedureOrg((new Atexo_Consultation_ProcedureType())->retrieveTypeProcedureObjectByAbreviationAnm($procedureType, true));
                    $arrayTypeAvisObject = (new Atexo_Consultation_TypeAvis())->retreiveTypeAvisByAbreviation($type);
                    $typeAvisObject = $arrayTypeAvisObject[0];
                    if ($typeAvisObject instanceof CommonTypeAvis) {
                        $commonConsultation->setIdTypeAvis($typeAvisObject->getId());
                    } else {
                        throw new Exception('Erreur : type avis manquant, Organisme : '.$organisme.' ReferenceUtilisateur '.$refUtilisateur.' typeAvis : '.$type.' .');
                    }
                    $commonConsultation->setDateMiseEnLigneCalcule($dateMiseOnLigneCalcule);
                    $commonConsultation->setDatedebut(date('Y-m-d H:i:s'));
                    $commonConsultation->setDatemiseenligne($dateMiseOnLigneCalcule);
                    $commonConsultation->setDatevalidation($dateMiseOnLigneCalcule);
                    $commonConsultation->setTypeAcces(Atexo_Config::getParameter('TYPE_PROCEDURE_PUBLICITE'));
                    $commonConsultation->setIdRegleValidation(Atexo_Config::getParameter('REGLE_VALIDATION_TYPE_1'));
                    $commonConsultation->setServiceId('0');
                    $commonConsultation->setServiceAssocieId('0');
                    $commonConsultation->setConsultationExterne('1');
                    if ('' != $url) {
                        $commonConsultation->setUrlConsultationExterne($url);
                    }
                    if ('' != $urlAvisPub) {
                        $commonConsultation->setUrlConsultationAvisPub($urlAvisPub);
                    }
                    $categoryObject = (new Atexo_Consultation_Category())->retrieveCategoryObjectByLibelle($category);
                    if ($categoryObject) {
                        $commonConsultation->setCategorie($categoryObject->getId());
                    }
                    $arCpv = explode(',', $cpv, 2);
                    $commonConsultation->setCodeCpv1($arCpv[0]);
                    $commonConsultation->setCodeCpv2('#'.str_replace(',', '#', $arCpv[1]));
                    $commonConsultation->setDatefin(Atexo_Util::iso860TimeToIso($closureDate));
                    $scriptGeo = '';
                    $lieuxExec = (new Atexo_Geolocalisation_GeolocalisationN2())->retrieveGeoN2ByArrayDenomination2(explode(',', $deliveryPlace));
                    if ($lieuxExec) {
                        // tout les ids doivent être entouré par une virgule
                        $scriptGeo = ',';

                        foreach ($lieuxExec as $oneGeo) {
                            $scriptGeo .= $oneGeo->getId().',';
                        }
                    }
                    $commonConsultation->setLieuExecution($scriptGeo);
                    $commonConsultation->setIntitule($title);
                    $commonConsultation->setObjet($description);
                    $commonConsultation->setAutoriserReponseElectronique($electronicResponse);
                    $commonConsultation->setSignatureOffre($digitalSignature);
                    $commonConsultation->setReponseObligatoire($electronicResponseObligatoire);

                    $logo = (string) $nodeTender->item($i)->getAttribute('urlLogo');
                    if ($logo) {
                        $commonConsultation->setUrl($logo);
                    }
                    //clauses
                    if ($nodeTender->item($i)->getElementsByTagName('Clauses_Sociales_Env')) {
                        $nodeClauses = $nodeTender->item($i)->getElementsByTagName('Clauses_Sociales_Env');
                        $socialeConditionExecution = $nodeClauses->item(0)->getAttribute('socialeConditionExecution');
                        $socialeInsertion = $nodeClauses->item(0)->getAttribute('socialeInsertion');
                        $socialeAteliersProteges = $nodeClauses->item(0)->getAttribute('socialeAteliersProteges');
                        $socialeSiae = $nodeClauses->item(0)->getAttribute('socialeSiae');
                        $socialeEess = $nodeClauses->item(0)->getAttribute('socialeEess');
                        $envSpecsTechniques = $nodeClauses->item(0)->getAttribute('envSpecsTechniques');
                        $envCondExecution = $nodeClauses->item(0)->getAttribute('envCondExecution');
                        $envCriteresSelect = $nodeClauses->item(0)->getAttribute('envCriteresSelect');

                        $commonConsultation->setClauseSocialeConditionExecution($socialeConditionExecution);
                        $commonConsultation->setClauseSocialeInsertion($socialeInsertion);
                        $commonConsultation->setClauseSocialeAteliersProteges($socialeAteliersProteges);
                        $commonConsultation->setClauseSocialeSiae($socialeSiae);
                        $commonConsultation->setClauseSocialeEss($socialeEess);
                        $commonConsultation->setClauseEnvSpecsTechniques($envSpecsTechniques);
                        $commonConsultation->setClauseEnvCondExecution($envCondExecution);
                        $commonConsultation->setClauseEnvCriteresSelect($envCriteresSelect);

                        if (('1' == $socialeConditionExecution) || ('1' == $socialeInsertion) || ('1' == $socialeAteliersProteges) || ('1' == $socialeSiae) || ('1' == $socialeEess)) {
                            $commonConsultation->setClauseSociale(Atexo_Config::getParameter('CLAUSES_SOCIALES_ACTIVER'));
                        } else {
                            $commonConsultation->setClauseSociale(Atexo_Config::getParameter('CLAUSES_SOCIALES_DESACTIVER'));
                        }
                        if (('1' == $envSpecsTechniques) || ('1' == $envCondExecution) || ('1' == $envCriteresSelect)) {
                            $commonConsultation->setClauseEnvironnementale(Atexo_Config::getParameter('CLAUSES_ENVIRONNEMENTALES_ACTIVER'));
                        } else {
                            $commonConsultation->setClauseEnvironnementale(Atexo_Config::getParameter('CLAUSES_ENVIRONNEMENTALES_DESACTIVER'));
                        }
                    }
                    // Récuperation des Lots
                    $allotisement = $nodeTender->item($i)->getElementsByTagName('Lot');
                    if ((is_countable($allotisement) ? count($allotisement) : 0) > 0) {
                        $commonConsultation->setAlloti('1');
                    } else {
                        $commonConsultation->setAlloti('0');
                    }
                    $commonConsultation->setDateFinUnix(strtotime($commonConsultation->getDatefin().' + '.$commonConsultation->getPoursuivreAffichage().' '.$commonConsultation->getPoursuivreAffichageUnite()));
                    $commonConsultation->save($connexionCom);
                    // Referntiel org denomonation debut
                    if ($commonConsultation->getDenominationAdapte()) {
                        (new Atexo_ReferentielOrgDenomination())->insertReferentielOrgDenomonation($commonConsultation);
                    }
                    // Fin
                    $nodeLots = $nodeTender->item($i)->getElementsByTagName('Lot');
                    $nombreLot = is_countable($nodeLots) ? count($nodeLots) : 0;
                    for ($j = 0; $j < $nombreLot; ++$j) {
                        $number = $nodeLots->item($j)->getAttribute('number');
                        $category = (string) $nodeLots->item($j)->getAttribute('category');
                        $cpv = $nodeLots->item($j)->getAttribute('cpv');
                        $nodeTitle = $nodeLots->item($j)->getElementsByTagName('Title');
                        $title = (string) $nodeTitle->item(0)->nodeValue;
                        $nodeSubject = $nodeLots->item($j)->getElementsByTagName('Subject');
                        $description = (string) $nodeSubject->item(0)->nodeValue;
                        if ($updateConsultation) {
                            $categoryLot = (new Atexo_CategorieLot())->retrieveByOrganismeConsultationIdNumero($acronymeOrg, $commonConsultation->getId(), $number);
                            if (!($categoryLot instanceof CommonCategorieLot)) {
                                $categoryLot = new CommonCategorieLot();
                            }
                        } else {
                            $categoryLot = new CommonCategorieLot();
                        }
                        $categoryLot->setOrganisme($acronymeOrg);
                        $categoryLot->setConsultationId($commonConsultation->getId());
                        $categoryLot->setLot($number);
                        $categoryLot->setDescription($description);
                        $categoryObject = (new Atexo_Consultation_Category())->retrieveCategoryObjectByLibelle($category);
                        if ($categoryObject) {
                            $categoryLot->setCategorie($categoryObject->getId());
                        }
                        $categoryLot->setDescription($title);
                        $categoryLot->setDescriptionDetail($description);
                        $arCpv = explode(',', $cpv);
                        $categoryLot->setCodeCpv1($arCpv[0]);
                        $categoryLot->setCodeCpv2(str_replace(',', '#', $arCpv[1]));
                        //clauses
                        if ($nodeLots->item($j)->getElementsByTagName('Clauses_Sociales_Env')) {
                            $nodeClauses = $nodeLots->item($j)->getElementsByTagName('Clauses_Sociales_Env');
                            $socialeConditionExecution = $nodeClauses->item(0)->getAttribute('socialeConditionExecution');
                            $socialeInsertion = $nodeClauses->item(0)->getAttribute('socialeInsertion');
                            $socialeAteliersProteges = $nodeClauses->item(0)->getAttribute('socialeAteliersProteges');
                            $socialeSiae = $nodeClauses->item(0)->getAttribute('socialeSiae');
                            $socialeEess = $nodeClauses->item(0)->getAttribute('socialeEess');
                            $envSpecsTechniques = $nodeClauses->item(0)->getAttribute('envSpecsTechniques');
                            $envCondExecution = $nodeClauses->item(0)->getAttribute('envCondExecution');
                            $envCriteresSelect = $nodeClauses->item(0)->getAttribute('envCriteresSelect');

                            $categoryLot->setClauseSocialeConditionExecution($socialeConditionExecution);
                            $categoryLot->setClauseSocialeInsertion($socialeInsertion);
                            $categoryLot->setClauseSocialeAteliersProteges($socialeAteliersProteges);
                            $categoryLot->setClauseSocialeSiae($socialeSiae);
                            $categoryLot->setClauseSocialeEss($socialeEess);
                            $categoryLot->setClauseEnvSpecsTechniques($envSpecsTechniques);
                            $categoryLot->setClauseEnvCondExecution($envCondExecution);
                            $categoryLot->setClauseEnvCriteresSelect($envCriteresSelect);

                            if (('1' == $socialeConditionExecution) || ('1' == $socialeInsertion) || ('1' == $socialeAteliersProteges) || ('1' == $socialeSiae) || ('1' == $socialeEess)) {
                                $categoryLot->setClauseSociale(Atexo_Config::getParameter('CLAUSES_SOCIALES_ACTIVER'));
                            } else {
                                $categoryLot->setClauseSociale(Atexo_Config::getParameter('CLAUSES_SOCIALES_DESACTIVER'));
                            }
                            if (('1' == $envSpecsTechniques) || ('1' == $envCondExecution) || ('1' == $envCriteresSelect)) {
                                $categoryLot->setClauseEnvironnementale(Atexo_Config::getParameter('CLAUSES_ENVIRONNEMENTALES_ACTIVER'));
                            } else {
                                $categoryLot->setClauseEnvironnementale(Atexo_Config::getParameter('CLAUSES_ENVIRONNEMENTALES_DESACTIVER'));
                            }
                        }
                        $categoryLot->save($connexionCom);
                        unset($categoryLot);
                        ///////
                    }
                    unset($commonConsultation);
                }

                if (Atexo_Config::getParameter('PF_MAIL_CLI_ANM')) {
                    $obj = '[Annonces marché] - Intégration des annonces - Rapport';
                    $msg = str_replace(')', '', str_replace('(', '', str_replace('Array', '', print_r($arrayNbreParOrg, true))));
                    $message = Atexo_Message::getGenericEmailFormat($msg);
                    Atexo_Message::simpleMail(Atexo_Config::getParameter('PF_MAIL_FROM'), Atexo_Config::getParameter('PF_MAIL_CLI_ANM'), $obj, $message, '', '', false, true);
                }
                Prado::log('Cool ! Migration de :'.$nombreTender.' consultations réussite pour '.$organismeName.'! !', TLogger::INFO,
                    self::ATEXO_CLI_ANNONCES_M_ARCHES
                );
            }
        } catch (\Exception $e) {
            echo 'exception '.$e->getMessage();
            Prado::log('Erreur lors de la recuperation OU insersion  des consultations : '.$e->getMessage(), TLogger::ERROR,
                self::ATEXO_CLI_ANNONCES_M_ARCHES
            );
        }
    }
}
