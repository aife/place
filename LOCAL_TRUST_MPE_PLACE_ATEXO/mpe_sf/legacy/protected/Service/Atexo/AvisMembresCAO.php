<?php

namespace Application\Service\Atexo;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonAvisMembresCAOPeer;

/**
 * commentaires.
 *
 * @author loubna Ezziani <loubna.ezziani@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 1.0
 *
 * @since MPE-4.0
 */
class Atexo_AvisMembresCAO
{
    /**
     * retourne l'objet  AvisMembresCAO (nom, prénom, admissiblité ...).
     *
     * @param : $idAgent     : l'id de l'agent
     * @param : $idEnveloppe : l'id de l'enveloppe,$sousPli :  sous pli,$formeEnv : forme d'enveloppe
     *
     * @return AvisMembresCAO si l'AvisMembresCAO est trouvé, false sinon
     */
    public function retrieveAvisMembresCAOByAgent($idAgent, $idEnveloppe, $sousPli, $formeEnv)
    {
        $c = new Criteria();
        $c->add(CommonAvisMembresCAOPeer::ID_AGENT, $idAgent, Criteria::EQUAL);
        $c->add(CommonAvisMembresCAOPeer::ID_ENVELOPPE, $idEnveloppe, Criteria::EQUAL);
        $c->add(CommonAvisMembresCAOPeer::SOUS_PLI, $sousPli, Criteria::EQUAL);
        $c->add(CommonAvisMembresCAOPeer::TYPE_DEPOT_REPONSE, $formeEnv, Criteria::EQUAL);
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $avisMembresCAOObject = CommonAvisMembresCAOPeer::doSelectOne($c, $connexionCom);

        return $avisMembresCAOObject ?: false;
    }
}
