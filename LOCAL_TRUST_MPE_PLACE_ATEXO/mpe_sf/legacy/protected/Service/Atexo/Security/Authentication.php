<?php

namespace Application\Service\Atexo\Security;

use App\Service\User;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\MpeTAuthManager;
use Prado;

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */
class Atexo_Security_Authentication
{
    public static function loginBySF()
    {
        /** @var MpeTAuthManager $pradoAuth */
        $pradoAuth = Prado::getApplication()->getModule("auth");
        $pradoUser = $pradoAuth->getUser();

        if (!$pradoUser) {
            $userService = Atexo_Util::getSfService(User::class);
            $sfUser = $userService->getUser();

            if (!is_null($sfUser)) {
                $pradoAuth->login(null, null);
            }
        }
    }
}
