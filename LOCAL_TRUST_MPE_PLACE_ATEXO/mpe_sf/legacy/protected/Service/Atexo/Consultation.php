<?php

namespace Application\Service\Atexo;

use App\Entity\Consultation;
use App\Entity\Lot;
use App\Repository\ConsultationRepository;
use App\Repository\ContratTitulaireRepository;
use App\Repository\LotRepository;
use App\Repository\Referentiel\Consultation\ClausesN1Repository;
use App\Repository\Referentiel\Consultation\ClausesN2Repository;
use App\Repository\Referentiel\Consultation\ClausesN3Repository;
use App\Service\ClausesService;
use App\Service\Consultation\FavorisService;
use App\Service\DataTransformer\ClausesTransformer;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Pages\Agent\FormulaireConsultation;
use Application\Pages\Agent\ouvertureEtAnalyse;
use Application\Propel\Mpe\CommonAgent;
use Application\Propel\Mpe\CommonAVIS;
use Application\Propel\Mpe\CommonCategorieLot;
use Application\Propel\Mpe\CommonCategorieLotPeer;
use Application\Propel\Mpe\CommonCertificatChiffrement;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationFavoris;
use Application\Propel\Mpe\CommonConsultationFavorisQuery;
use Application\Propel\Mpe\CommonConsultationPeer;
use Application\Propel\Mpe\CommonConsultationQuery;
use Application\Propel\Mpe\CommonGeolocalisationN2;
use Application\Propel\Mpe\CommonGestionAdressesPeer;
use Application\Propel\Mpe\CommonHabilitationAgent;
use Application\Propel\Mpe\CommonHistoriquesConsultation;
use Application\Propel\Mpe\CommonInterneConsultation;
use Application\Propel\Mpe\CommonInterneConsultationSuiviSeul;
use Application\Propel\Mpe\CommonOrganisme;
use Application\Propel\Mpe\CommonOrganismePeer;
use Application\Propel\Mpe\CommonPanierEntreprise;
use Application\Propel\Mpe\CommonProcedureEquivalence;
use Application\Propel\Mpe\CommonService;
use Application\Propel\Mpe\CommonStatutEnveloppePeer;
use Application\Propel\Mpe\CommonTContratTitulaire;
use Application\Propel\Mpe\CommonTCritereAttribution;
use Application\Propel\Mpe\CommonTDonneeComplementaire;
use Application\Propel\Mpe\CommonTFormePrix;
use Application\Propel\Mpe\CommonTLotTechnique;
use Application\Propel\Mpe\CommonTTypeContrat;
use Application\Propel\Mpe\CommonTypeProcedureOrganisme;
use Application\Propel\Mpe\CommonVisiteLieuxPeer;
use Application\Propel\Mpe\Om\BaseCommonCategorieLotPeer;
use Application\Propel\Mpe\Om\BaseCommonConsultationPeer;
use Application\Propel\Mpe\Om\BaseCommonDocumentExternePeer;
use Application\Propel\Mpe\Om\BaseCommonEtatConsultationPeer;
use Application\Service\Atexo\Config\Atexo_Config_Exception;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Contrat;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Dce;
use Application\Service\Atexo\Consultation\Atexo_Consultation_DocumentExterneVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Exception;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Guests;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Lots;
use Application\Service\Atexo\Consultation\Atexo_Consultation_NumerotationRefCons;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureEquivalence;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureType;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Rg;
use Application\Service\Atexo\Consultation\Atexo_Consultation_TypeAvis;
use Application\Service\Atexo\Crypto\Atexo_Crypto_Certificat;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_PaniersEntreprises;
use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2;
use Application\Service\Atexo\Publicite\Atexo_Publicite_Avis;
use Application\Service\Atexo\Recherche\Atexo_Recherche_Search;
use Application\Service\Atexo\Rest\Action\Atexo_Rest_Action_CertificatChiffrementField;
use Application\Service\Atexo\Rest\Action\Atexo_Rest_Action_ConsultationField;
use Application\Service\Atexo\Rest\Action\Atexo_Rest_Action_LotField;
use Application\Service\Atexo\Rest\Action\Atexo_Rest_Action_SynchroSubField;
use AtexoPdf\PdfGeneratorClient;
use Exception;
use PDO;
use Prado\Prado;
use Application\Service\Atexo\GenerationFichier\Atexo_GenerationFichier_ImpressionODT;

/**
 * Classe de manipulation des consultations.
 *
 * @author Guillaume Poncon <guillaume.poncon@openstates.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Consultation
{
    /**
     * retourne la reference utilisateur,
     * sans les suffixe type de procedure et type d'annonce.
     *
     * @param string $reference : reference de la consultation
     *
     * @return string
     */
    public static function explodeUserReference($reference)
    {
        $tab_ref = explode(' - ', $reference);
        $result = $tab_ref[0];
        for ($i = 1; $i < count($tab_ref) - 2; ++$i) {
            $result .= ' - ' . $tab_ref[$i];
        }

        return $result;
    }

    /**
     * Permet de calculer la date de mise en ligne de la consultation.
     *
     * @param CommonConsultation $consultation : objet consultation
     *
     * @return string : date de mise en ligne
     */
    public static function getDateMiseEnLigne($consultation)
    {
        try {
            $dateMiseEnligne = '';
            $connexion = Propel::getConnection(
                Atexo_Config::getParameter('COMMON_DB')
                . Atexo_Config::getParameter('CONST_READ_ONLY')
            );

            if ($consultation instanceof CommonConsultation) {
                $typeMiseEnLigne = $consultation->getRegleMiseEnLigne();
                $dateMiseEnLigne = $consultation->getDatemiseenligne();
                $dateValidation = $consultation->getDatevalidation();
                $annoncePubliee = $consultation->getFirstAnnoncePubliee($connexion);
                $annonceEnvoyee = $consultation->getFirstAnnonceEnvoyee($connexion);
                $dateEnvoi = ($annonceEnvoyee->getDateEnvoi() == "0000-00-00 00:00:00")
                    ? "0000-00-00" : $annonceEnvoyee->getDateEnvoi();
                $datePub = $annoncePubliee->getDatePub();
            } else {
                throw new Atexo_Consultation_Exception(
                    'Parametre pour la fonction getStatus errone : attendu Objet Consultation ou CommonConsultation '
                );
            }

            if ($consultation->getEtatValidation() === '0') {
                return '';
            }

            if ($typeMiseEnLigne == Atexo_Config::getParameter('MISE_EN_LIGNE_APRES_VALIDATION_ET_DATE')) {
                if ($dateMiseEnLigne > "0000-00-00" && $dateMiseEnLigne <= $dateValidation) {
                    return $dateValidation;
                } elseif ($dateValidation > "0000-00-00") {
                    return $dateMiseEnLigne;
                } else {
                    return '';
                }
            } elseif ($typeMiseEnLigne == Atexo_Config::getParameter('MISE_EN_LIGNE_APRES_VALIDATION_MANUELLE')) {
                // Valdation manuelle
                return $dateValidation;
            } elseif ($typeMiseEnLigne == Atexo_Config::getParameter('MISE_EN_LIGNE_APRES_VALIDATION_ET_ENVOI_BOAMP')) {
                if ($dateEnvoi > "0000-00-00" && $dateEnvoi <= $dateValidation) {
                    return $dateValidation;
                } elseif ($dateValidation > "0000-00-00") {
                    return $dateEnvoi;
                } else {
                    return '';
                }
            } elseif ($typeMiseEnLigne == Atexo_Config::getParameter('MISE_EN_LIGNE_APRES_VALIDATION_ET_PUBLICATION_BOAMP')) {
                if ($datePub > "0000-00-00" && $datePub <= $dateValidation) {
                    return $dateValidation;
                } elseif ($dateValidation > "0000-00-00") {
                    return $datePub;
                } else {
                    return '';
                }
            }

            return $dateMiseEnligne;
        } catch (\Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('app');
            $logger->error("Erreur calcul de la date de mise en ligne de la consultation : \n" . $e->getMessage() . " \n\n Trace : " . $e->getTraceAsString());
        }
    }

    public static function generateRegistreCommission(CommonConsultation $consultation, $organisme = null)
    {

        $filePath = Atexo_Config::getParameter('COMMON_TMP') . 'com_' . session_id() . microtime();
        $filePath = str_replace(' ', '', $filePath);

        $impr = new Atexo_GenerationFichier_ImpressionODT();
        $tableKeys = [];
        if ('0' != $consultation->getEnvCandidature()) {
            $tableKeys['<!--lineContentCandidature-->'] = $impr->imprimerTableauCandidature($consultation, $organisme);
        }
        if ('0' != $consultation->getEnvOffre()) {
            $tableKeys['<!--lineContentOffres-->'] = $impr->imprimerTableauxOffres($consultation, $organisme);
        }
        if ('0' != $consultation->getEnvAnonymat()) {
            $tableKeys['<!--lineContentAnonymat-->'] = $impr->imprimerTableauxAnonymat($consultation, $organisme);
        }
        $consSammury = self::arrayConsultationSummary($consultation, $organisme);
        $tableKeys = array_merge($consSammury, $tableKeys);
        $modelePath = Atexo_Config::getParameter('CHEMIN_MODEL_ODT_DEPOUILLEMENT_CONSULTATION');
        $odtFilePath = $impr->generateOdtFile($tableKeys, $modelePath);

        if ($odtFilePath) {
            $content = (new PdfGeneratorClient())->genererPdf($odtFilePath);
            $fp = fopen($filePath, 'w');
            if (!fwrite($fp, $content)) {
                return null;
            }
            fclose($fp);

            return $filePath;
        }

        return null;
    }

    public function isEntityAbleToValidate($idServiceValidateur, $idService, $organisme)
    {
        return (new Atexo_EntityPurchase())->isEntityAbleToValidate($idServiceValidateur, $idService, $organisme);
    }

    //TODO : a supprimer, aide pour les tests
    public function consultationServices($org)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonConsultationPeer::ORGANISME, $org);
        $consultation = BaseCommonConsultationPeer::doSelect($c, $connexion);
        $chaine = '';
        if ($consultation) {
            foreach ($consultation as $uneCons) {
                $chaine .= ' Reference ' . $uneCons->getId() . ' => Service ' . $uneCons->getServiceId() . '<br>';
            }
        }

        return $chaine;
    }

    /**
     * Met a jour le nombre de telechargement du DCE.
     *
     * @param int $reference la reference de la consultation
     */
    public function updateNbrDownloadDce($consultationId, $organisme)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));
        $consultation = CommonConsultationPeer::retrieveByPK($consultationId, $connexionCom);
        $currentNbrDownloadDce = 0;
        if ($consultation) {
            $currentNbrDownloadDce = (int) $consultation->getNbrTelechargementDce();
        }
        $newNbrDownloadDce = $currentNbrDownloadDce + 1;
        $consultation->setNbrTelechargementDce($newNbrDownloadDce);
        $consultation->save($connexionCom);
    }

    /**
     * Retourne le code d'acces de la consultation.
     */
    public function getCodeAccesConsultationRestreinte($consultationId, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonConsultationPeer::ID, $consultationId);
        $c->add(CommonConsultationPeer::ORGANISME, $organisme);
        $c->add(CommonConsultationPeer::TYPE_ACCES, 2);
        $consultation = CommonConsultationPeer::doSelectOne($c, $connexion);
        if ($consultation) {
            return $consultation->getCodeProcedure();
        } else {
            return false;
        }
    }

    public static function retrieveConsultation($id, $org = null)
    {
        try {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));
            if (!$org) {
                $org = Atexo_CurrentUser::getCurrentOrganism();
            }

            $c = new Criteria();
            $c->add(CommonConsultationPeer::ID, $id);
            $c->add(CommonConsultationPeer::ORGANISME, $org);
            $consultation = CommonConsultationPeer::doSelectOne($c, $connexion);

            if ($consultation) {
                return $consultation;
            } else {
                return false;
            }
        } catch (\Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('app');
            $logger->error("Erreur : organisme = $org , reference = $id \n\nErreur : " . $e->getMessage());
        }
    }

    /**
     * Effectue une recherche parametree en fonction du critere propose.
     *
     * @param Atexo_Consultation_CriteriaVo $criteria , $returnJustNumberOfElement : a true pour avoir uniquement le nombre d'elements
     *
     * @return array of consultation object
     */
    public function search(
        Atexo_Consultation_CriteriaVo $criteriaVo,
        $returnJustNumberOfElement = false,
        $returnReferences = false,
        $forAlertesEntreprises = false
    ) {
        if (('1' == Atexo_Config::getParameter('SEARCH_SF_ENTREPRISE') && $criteriaVo->getcalledFromPortail()) || 'true' === $_GET['visuel']) {
            $search = self::searchSf(
                $criteriaVo,
                $returnJustNumberOfElement,
                $returnReferences,
                $forAlertesEntreprises
            );
        } elseif ('1' == Atexo_Config::getParameter('SEARCH_SF_AGENT') && !$criteriaVo->getcalledFromPortail()) {
            $search = self::searchSf(
                $criteriaVo,
                $returnJustNumberOfElement,
                $returnReferences,
                $forAlertesEntreprises,
                true
            );
        } else {
            $search = self::searchPrado(
                $criteriaVo,
                $returnJustNumberOfElement,
                $returnReferences,
                $forAlertesEntreprises
            );
        }

        return $search;
    }

    /**
     * @param bool $returnJustNumberOfElement
     * @param bool $returnReferences
     * @param bool $forAlertesEntreprises
     * @param bool $searchAgent
     *
     * @return array
     *
     * @throws Atexo_Config_Exception
     * @throws PropelException
     */
    private static function searchSf(
        Atexo_Consultation_CriteriaVo $criteriaVo,
        $returnJustNumberOfElement = false,
        $returnReferences = false,
        $forAlertesEntreprises = false,
        $searchAgent = false
    ) {
        $keys = [];

        if (true === $searchAgent) {
            $statement = Atexo_Util::getSfService(\App\Service\AtexoSearchAgent::class)
                ->getConsultationsByQuery(
                    $criteriaVo,
                    $returnJustNumberOfElement,
                    $returnReferences,
                    $forAlertesEntreprises
                );
        } else {
            $statement = Atexo_Util::getSfService(\App\Service\AtexoSearchEntreprise::class)
                ->getConsultationsByQuery(
                    $criteriaVo,
                    $returnJustNumberOfElement,
                    $returnReferences,
                    $forAlertesEntreprises
                );
        }

        $arrayReferences = [];
        if ($returnJustNumberOfElement) {
            return is_countable($statement) ? count($statement) : 0;
        } elseif ($returnReferences) {
            foreach ($statement as $row) {
                if (!in_array($row['id'], $arrayReferences)) {
                    $arrayReferences[] = $row['id'];
                }
            }

            return $arrayReferences;
        } else {
            //ajouter la clause order by pour le tri : elle peut etre passe en parametre dans le criteriaVo
            $atLeastOneConsultationFound = false;
            $readOnlyGuest = [];
            $validationOnlyGuest = [];
            $c = new Criteria();
            //on parcours le PDO record set pour recuperer l'objet Consultation en utilisant propel
            foreach ($statement as $row) {
                $atLeastOneConsultationFound = true;
                $c->addOr(CommonConsultationPeer::ID, $row['id']);
                if (!$criteriaVo->getcalledFromPortail()) {
                    //Si l'agent connecte est inivite par defaut ou avec ces habilitations, $readOnlyGuest prend la valeur false
                    //Sinon s'il est invite en lecture seul, $readOnlyGuest prend la valeur true.
                    //(la cle du tableau est la reference)
                    if ('0' == $row['invitedValidationOnly']) {
                        if ('1' == $row['invitedWithHisHabilitation']) {
                            $readOnlyGuest[$row['id']] = false;
                        } else {
                            $readOnlyGuest[$row['id']] = true;
                        }
                    } else {
                        $validationOnlyGuest[$row['id']] = true;
                        if (!isset($readOnlyGuest[$row['id']])) {
                            $readOnlyGuest[$row['id']] = true;
                        }
                    }
                }
            }
            $arrayConsultation = [];
            if ($atLeastOneConsultationFound) {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
                if ($criteriaVo->getcalledFromPortail()) {
                    $c->addJoin(CommonConsultationPeer::ORGANISME, CommonOrganismePeer::ACRONYME, Criteria::INNER_JOIN);
                    if ($criteriaVo->getSortByElement()) {
                        if ('ASC' == $criteriaVo->getSensOrderBy()) {
                            $c->addAscendingOrderByColumn('consultation.' . $criteriaVo->getSortByElement());
                        } else {
                            $c->addDescendingOrderByColumn('consultation.' . $criteriaVo->getSortByElement());
                        }
                        $c->addAscendingOrderByColumn('Organisme.ordre');
                        if ('datefin' != strtolower($criteriaVo->getSortByElement())) {
                            $c->addDescendingOrderByColumn(CommonConsultationPeer::DATEFIN);
                        }
                    } else {
                        $c->addAscendingOrderByColumn('Organisme.ordre');
                        $c->addDescendingOrderByColumn(CommonConsultationPeer::DATEFIN);
                    }
                    $arrayConsultation = CommonConsultationPeer::doSelect($c, $connexion);
                } else {
                    if ($criteriaVo->getSortByElement()) {
                        if ('ASC' == $criteriaVo->getSensOrderBy()) {
                            $c->addAscendingOrderByColumn('consultation.' . $criteriaVo->getSortByElement());
                        } else {
                            $c->addDescendingOrderByColumn('consultation.' . $criteriaVo->getSortByElement());
                        }
                        if ('datefin' != strtolower($criteriaVo->getSortByElement())) {
                            $c->addDescendingOrderByColumn(CommonConsultationPeer::DATEFIN);
                        }
                    } else {
                        $c->addDescendingOrderByColumn(CommonConsultationPeer::DATEFIN);
                    }
                    $c->add(CommonConsultationPeer::ORGANISME, $criteriaVo->getAcronymeOrganisme());
                    if ($criteriaVo->getLimit()) {
                        $c->setLimit($criteriaVo->getLimit());
                        $c->setOffset($criteriaVo->getOffset());
                    }

                    $arrayConsultation = CommonConsultationPeer::doSelect($c, $connexion);
                }
            }
            $arrayReturn = [];
            foreach ($arrayConsultation as $consultation) {
                if ($consultation instanceof CommonConsultation && !$criteriaVo->getcalledFromPortail()) {
                    $consultation->setCurrentUserReadOnly($readOnlyGuest[$consultation->getId()]);
                    $consultation->setCurrentUserValidationOnly($validationOnlyGuest[$consultation->getId()]);
                    $idEtatConsultation = self::getStatus($consultation, $criteriaVo->getcalledFromPortail());
                    $consultation->setStatusConsultation($idEtatConsultation);
                    $consultation->setPublicationHabilitation(Atexo_CurrentUser::hasHabilitation('PublierConsultation'));
                    $consultation->setRedactionDocumentsRedac(Atexo_CurrentUser::hasHabilitation('RedactionDocumentsRedac'));
                    if (
                        (
                        (
                            $consultation->hasHabilitationModifierApresValidation()
                            &&
                            (
                                $consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_CONSULTATION')
                                || $consultation->getDepouillablePhaseConsultation() == Atexo_Config::getParameter('CONSULTATION_DEPOUIABLE_PHASE_1')
                            )
                            &&
                            $consultation->getDatefin() > date('Y-m-d H:i:s')
                        )
                        ||
                        (
                            Atexo_CurrentUser::hasHabilitation('ModifierConsultationAvantValidation')
                            &&
                            (
                                $consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_PREPARATION')
                                || $consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_ELABORATION')
                            )
                            &&
                            (
                                $consultation->getDatefin() > date('Y-m-d H:i:s')
                                ||
                                strstr($consultation->getDatefin(), '0000-00-00 00:00:00')
                            )
                        )
                        )
                    ) {
                        $consultation->setModifHabilitation(true);
                    } else {
                        $consultation->setModifHabilitation(false);
                    }

                    if (
                        $consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_PREPARATION')
                        || $consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_ELABORATION')
                    ) {
                        $chiffrement = $consultation->getChiffrementOffre();

                        if (1 == $chiffrement) {
                            $consultation->setValidateWithChiffrement(true);
                        } else {
                            $consultation->setValidateWithoutChiffrement(true);
                        }
                    }

                    // Attribution possible si status decision, ou (status != de preparation et consultation depouillable phase consultation)
                    if (
                        $consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_DECISION')
                        || ($consultation->getStatusConsultation() !== Atexo_Config::getParameter('STATUS_ELABORATION')
                            && $consultation->getStatusConsultation() !== Atexo_Config::getParameter('STATUS_PREPARATION')
                            && $consultation->getDepouillablePhaseConsultation() == Atexo_Config::getParameter('CONSULTATION_DEPOUIABLE_PHASE_1'))
                        || ($consultation->getDecisionPartielle() == Atexo_Config::getParameter('DECISION_PARTIELLE_OUI')
                            && $consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE'))
                    ) {
                        $consultation->setAttributionHabilitation(Atexo_CurrentUser::hasHabilitation('AttributionMarche'));
                        $consultation->setDecisionSuiviSeulHabilitation(Atexo_CurrentUser::hasHabilitation('DecisionSuiviSeul'));
                    }

                    $consultation->setDepotHabilitation(Atexo_CurrentUser::hasHabilitation('AccesRegistreDepotsPapier'));
                    $consultation->setRetraiteHabilitation(Atexo_CurrentUser::hasHabilitation('AccesRegistreRetraitsPapier'));
                    $consultation->setQuestionHabilitation(Atexo_CurrentUser::hasHabilitation('AccesRegistreQuestionsPapier'));

                    // Depouillement possible si status decision, ou (status != de preparation et consultation depouillable phase consultation)
                    if (
                        $consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE')
                        || ($consultation->getStatusConsultation() !== Atexo_Config::getParameter('STATUS_PREPARATION')
                            && $consultation->getStatusConsultation() !== Atexo_Config::getParameter('STATUS_ELABORATION')
                            && $consultation->getDepouillablePhaseConsultation() == Atexo_Config::getParameter('CONSULTATION_DEPOUIABLE_PHASE_1'))
                    ) {
                        $consultation->setDepouillerHabilitation(Atexo_CurrentUser::hasHabilitation('AccesReponses'));
                        $consultation->setResultatAnalyseHabilitation(Atexo_CurrentUser::hasHabilitation('ResultatAnalyse'));
                    }

                    if (
                        $consultation->getDecisionPartielle() == Atexo_Config::getParameter('DECISION_PARTIELLE_OUI')
                        && $consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE')
                    ) {
                        $consultation->setStatusConsultation(Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE') . Atexo_Config::getParameter('STATUS_DECISION'));
                    }

                    if (
                        $consultation->getStatusConsultation() !== Atexo_Config::getParameter('STATUS_PREPARATION')
                        && $consultation->getStatusConsultation() !== Atexo_Config::getParameter('STATUS_ELABORATION')
                        && $consultation->getDepouillablePhaseConsultation() == Atexo_Config::getParameter('CONSULTATION_DEPOUIABLE_PHASE_1')
                    ) {
                        if ($consultation->getDatefin() > date('Y-m-d H:i:s')) {
                            $consultation->setStatusConsultation(Atexo_Config::getParameter('STATUS_CONSULTATION') .
                                Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE') .
                                Atexo_Config::getParameter('STATUS_DECISION'));
                        } else {
                            $consultation->setStatusConsultation(Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE') .
                                Atexo_Config::getParameter('STATUS_DECISION'));
                        }
                    }
                }
                $key = $consultation->getId() . $consultation->getOrganisme();
                if (!in_array($key, $keys)) {
                    $keys[] = $key;
                    $arrayReturn[] = $consultation;
                }
            }

            return $arrayReturn;
        }
    }

    private static function searchPrado(Atexo_Consultation_CriteriaVo $criteriaVo, $returnJustNumberOfElement = false, $returnReferences = false, $forAlertesEntreprises = false)
    {
        $keys = [];

        $statement = (new Atexo_Recherche_Search())->getConsultationsByQuery($criteriaVo, $returnJustNumberOfElement, $returnReferences, $forAlertesEntreprises);

        $arrayReferences = [];
        if ($returnJustNumberOfElement) {
            $nombreElement = $statement->rowCount();

            return $nombreElement;
        } elseif ($returnReferences) {
            while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                if (!in_array($row['reference'], $arrayReferences)) {
                    $arrayReferences[] = $row['reference'];
                }
            }

            return $arrayReferences;
        } else {
            //ajouter la clause order by pour le tri : elle peut etre passe en parametre dans le criteriaVo
            $atLeastOneConsultationFound = false;
            $readOnlyGuest = [];
            $validationOnlyGuest = [];
            $c = new Criteria();
            //on parcours le PDO record set pour recuperer l'objet Consultation en utilisant propel
            while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                $atLeastOneConsultationFound = true;
                $c->addOr(CommonConsultationPeer::ID, $row['id']);
                if (!$criteriaVo->getcalledFromPortail()) {
                    //Si l'agent connecte est inivite par defaut ou avec ces habilitations, $readOnlyGuest prend la valeur false
                    //Sinon s'il est invite en lecture seul, $readOnlyGuest prend la valeur true.
                    //(la cle du tableau est la reference)
                    if ('0' == $row['invitedValidationOnly']) {
                        if ('1' == $row['invitedWithHisHabilitation']) {
                            $readOnlyGuest[$row['id']] = false;
                        } else {
                            $readOnlyGuest[$row['id']] = true;
                        }
                    } else {
                        $validationOnlyGuest[$row['id']] = true;
                        if (!isset($readOnlyGuest[$row['id']])) {
                            $readOnlyGuest[$row['id']] = true;
                        }
                    }
                }
            }
            $arrayConsultation = [];
            if ($atLeastOneConsultationFound) {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
                if ($criteriaVo->getcalledFromPortail()) {
                    $c->addJoin(CommonConsultationPeer::ORGANISME, CommonOrganismePeer::ACRONYME, Criteria::INNER_JOIN);
                    if ($criteriaVo->getSortByElement()) {
                        if ('ASC' == $criteriaVo->getSensOrderBy()) {
                            $c->addAscendingOrderByColumn('consultation.' . $criteriaVo->getSortByElement());
                        } else {
                            $c->addDescendingOrderByColumn('consultation.' . $criteriaVo->getSortByElement());
                        }
                        $c->addAscendingOrderByColumn('Organisme.ordre');
                        if ('datefin' != strtolower($criteriaVo->getSortByElement())) {
                            $c->addDescendingOrderByColumn(CommonConsultationPeer::DATEFIN);
                        }
                    } else {
                        $c->addAscendingOrderByColumn('Organisme.ordre');
                        $c->addDescendingOrderByColumn(CommonConsultationPeer::DATEFIN);
                    }
                    $arrayConsultation = CommonConsultationPeer::doSelect($c, $connexion);
                } else {
                    if ($criteriaVo->getSortByElement()) {
                        if ('ASC' == $criteriaVo->getSensOrderBy()) {
                            $c->addAscendingOrderByColumn('consultation.' . $criteriaVo->getSortByElement());
                        } else {
                            $c->addDescendingOrderByColumn('consultation.' . $criteriaVo->getSortByElement());
                        }
                        if ('datefin' != strtolower($criteriaVo->getSortByElement())) {
                            $c->addDescendingOrderByColumn(CommonConsultationPeer::DATEFIN);
                        }
                    } else {
                        $c->addDescendingOrderByColumn(CommonConsultationPeer::DATEFIN);
                    }
                    $c->add(CommonConsultationPeer::ORGANISME, $criteriaVo->getAcronymeOrganisme());
                    if ($criteriaVo->getLimit()) {
                        $c->setLimit($criteriaVo->getLimit());
                        $c->setOffset($criteriaVo->getOffset());
                    }

                    $arrayConsultation = CommonConsultationPeer::doSelect($c, $connexion);
                }
            }
            $arrayReturn = [];
            foreach ($arrayConsultation as $consultation) {
                if ($consultation instanceof CommonConsultation && !$criteriaVo->getcalledFromPortail()) {
                    $consultation->setCurrentUserReadOnly($readOnlyGuest[$consultation->getId()]);
                    $consultation->setCurrentUserValidationOnly($validationOnlyGuest[$consultation->getId()]);
                    $idEtatConsultation = self::getStatus($consultation, $criteriaVo->getcalledFromPortail());
                    $consultation->setStatusConsultation($idEtatConsultation);
                    $consultation->setPublicationHabilitation(Atexo_CurrentUser::hasHabilitation('PublierConsultation'));
                    $consultation->setRedactionDocumentsRedac(Atexo_CurrentUser::hasHabilitation('RedactionDocumentsRedac'));
                    if (
                        (
                        (
                            $consultation->hasHabilitationModifierApresValidation()
                            &&
                            (
                                $consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_CONSULTATION')
                                || $consultation->getDepouillablePhaseConsultation() == Atexo_Config::getParameter('CONSULTATION_DEPOUIABLE_PHASE_1')
                            )
                            &&
                            $consultation->getDatefin() > date('Y-m-d H:i:s')
                        )
                        ||
                        (
                            Atexo_CurrentUser::hasHabilitation('ModifierConsultationAvantValidation')
                            &&
                            (
                                $consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_PREPARATION')
                                || $consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_ELABORATION')
                            )
                            &&
                            (
                                $consultation->getDatefin() > date('Y-m-d H:i:s')
                                ||
                                strstr($consultation->getDatefin(), '0000-00-00 00:00:00')
                            )
                        )
                        )
                    ) {
                        $consultation->setModifHabilitation(true);
                    } else {
                        $consultation->setModifHabilitation(false);
                    }

                    if (
                        $consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_PREPARATION')
                        || $consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_ELABORATION')
                    ) {
                        $chiffrement = $consultation->getChiffrementOffre();

                        if (1 == $chiffrement) {
                            $consultation->setValidateWithChiffrement(true);
                        } else {
                            $consultation->setValidateWithoutChiffrement(true);
                        }
                    }

                    // Attribution possible si status decision, ou (status != de preparation et consultation depouillable phase consultation)
                    if (
                        $consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_DECISION')
                        || ($consultation->getStatusConsultation() !== Atexo_Config::getParameter('STATUS_ELABORATION')
                            && $consultation->getStatusConsultation() !== Atexo_Config::getParameter('STATUS_PREPARATION')
                            && $consultation->getDepouillablePhaseConsultation() == Atexo_Config::getParameter('CONSULTATION_DEPOUIABLE_PHASE_1'))
                        || ($consultation->getDecisionPartielle() == Atexo_Config::getParameter('DECISION_PARTIELLE_OUI')
                            && $consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE'))
                    ) {
                        $consultation->setAttributionHabilitation(Atexo_CurrentUser::hasHabilitation('AttributionMarche'));
                        $consultation->setDecisionSuiviSeulHabilitation(Atexo_CurrentUser::hasHabilitation('DecisionSuiviSeul'));
                    }

                    $consultation->setDepotHabilitation(Atexo_CurrentUser::hasHabilitation('AccesRegistreDepotsPapier'));
                    $consultation->setRetraiteHabilitation(Atexo_CurrentUser::hasHabilitation('AccesRegistreRetraitsPapier'));
                    $consultation->setQuestionHabilitation(Atexo_CurrentUser::hasHabilitation('AccesRegistreQuestionsPapier'));

                    // Depouillement possible si status decision, ou (status != de preparation et consultation depouillable phase consultation)
                    if (
                        $consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE')
                        || ($consultation->getStatusConsultation() !== Atexo_Config::getParameter('STATUS_PREPARATION')
                            && $consultation->getStatusConsultation() !== Atexo_Config::getParameter('STATUS_ELABORATION')
                            && $consultation->getDepouillablePhaseConsultation() == Atexo_Config::getParameter('CONSULTATION_DEPOUIABLE_PHASE_1'))
                    ) {
                        $consultation->setDepouillerHabilitation(Atexo_CurrentUser::hasHabilitation('AccesReponses'));
                        $consultation->setResultatAnalyseHabilitation(Atexo_CurrentUser::hasHabilitation('ResultatAnalyse'));
                    }

                    if (
                        $consultation->getDecisionPartielle() == Atexo_Config::getParameter('DECISION_PARTIELLE_OUI')
                        && $consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE')
                    ) {
                        $consultation->setStatusConsultation(Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE') . Atexo_Config::getParameter('STATUS_DECISION'));
                    }

                    if (
                        $consultation->getStatusConsultation() !== Atexo_Config::getParameter('STATUS_PREPARATION')
                        && $consultation->getStatusConsultation() !== Atexo_Config::getParameter('STATUS_ELABORATION')
                        && $consultation->getDepouillablePhaseConsultation() == Atexo_Config::getParameter('CONSULTATION_DEPOUIABLE_PHASE_1')
                    ) {
                        if ($consultation->getDatefin() > date('Y-m-d H:i:s')) {
                            $consultation->setStatusConsultation(Atexo_Config::getParameter('STATUS_CONSULTATION') .
                                Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE') .
                                Atexo_Config::getParameter('STATUS_DECISION'));
                        } else {
                            $consultation->setStatusConsultation(Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE') .
                                Atexo_Config::getParameter('STATUS_DECISION'));
                        }
                    }
                }
                $key = $consultation->getId() . $consultation->getOrganisme();
                if (!in_array($key, $keys)) {
                    $keys[] = $key;
                    $arrayReturn[] = $consultation;
                }
            }

            return $arrayReturn;
        }
    }

    /**
     * fonction qui retourne le status actuel de la consultation en fonction des informations passe en parametre.
     *
     * @param $consultation : l'objet consultation ou un tableau contenant la consultation
     *
     * @return (int) status de la consultation
     */
    public static function getStatus($consultation, $calledFromPortail)
    {
        $statut = null;
        if ($consultation instanceof CommonConsultation) {
            if (!$calledFromPortail) {
                $idEtatConsultation = $consultation->getIdEtatConsultation();
            } else {
                $idEtatConsultation = 0;
            }
        } else {
            throw new Atexo_Consultation_Exception('Parametre pour la fonction getStatus errone : attendu Objet Consultation ou CommonConsultation ');
        }
        $dateMiseEnligneCalcule = $consultation->getDateMiseEnLigneCalcule();
        $dateFin = $consultation->getDatefin();
        if ($idEtatConsultation > 0) {
            $statut = $idEtatConsultation;
        } elseif ((!$dateMiseEnligneCalcule || strstr($dateMiseEnligneCalcule, '0000-00-00')
                || $dateMiseEnligneCalcule > date('Y-m-d H:i:s'))
        ) {
            if ('1' == $consultation->getEtatEnAttenteValidation()) {
                $statut = Atexo_Config::getParameter('STATUS_PREPARATION');
            } else {
                $statut = Atexo_Config::getParameter('STATUS_ELABORATION');
            }
        } elseif ($dateFin > date('Y-m-d H:i:s') && $dateMiseEnligneCalcule && !strstr($dateMiseEnligneCalcule, '0000-00-00') && $dateMiseEnligneCalcule <= date('Y-m-d H:i:s')) {
            $statut = Atexo_Config::getParameter('STATUS_CONSULTATION');
        } elseif ($dateFin <= date('Y-m-d H:i:s') && !$idEtatConsultation) {
            $statut = Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE');
        }

        return $statut;
    }

    public static function saveConsultationGuests(&$consultationO, $invitesChoisis, $updateFavoris = true)
    {
        $atexoConsultationGuests = new Atexo_Consultation_Guests();

        if (is_array($invitesChoisis)) {
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));
            $tabGuests = Atexo_Consultation_Guests::guestConsultation(
                $consultationO->getId(),
                Atexo_CurrentUser::getCurrentOrganism(),
                $connexionCom
            );
            $i = 0;
            if ($updateFavoris === true) {
                $tabGuests = $atexoConsultationGuests->deleteGuestConsultationFavoris(
                    $consultationO->getId(),
                    Atexo_CurrentUser::getCurrentOrganism(),
                    $tabGuests,
                    $connexionCom
                );
            }

            $atexoConsultationGuests->deleteGuests($consultationO->getId(), Atexo_CurrentUser::getCurrentOrganism());
            foreach ($invitesChoisis as $invite) {
                if ('0' == $invite->getTypeInvitation()) {
                    $interneSuiviSeul[$i] = new CommonInterneConsultationSuiviSeul();
                    $interneSuiviSeul[$i]->setId('');
                    $interneSuiviSeul[$i]->setInterneId($invite->getId());
                    $interneSuiviSeul[$i]->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
                    $consultationO->addCommonInterneConsultationSuiviSeul($interneSuiviSeul[$i]);
                } elseif ('1' == $invite->getTypeInvitation() && 0 == $invite->getPermanentGuest()) {
                    $interneConsultation[$i] = new CommonInterneConsultation();
                    $interneConsultation[$i]->setId('');
                    $interneConsultation[$i]->setInterneId($invite->getId());
                    $interneConsultation[$i]->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
                    $consultationO->addCommonInterneConsultation($interneConsultation[$i]);
                }

                if ($updateFavoris === true) {
                    self::addFavorisPonctuelle($invite, $consultationO, $tabGuests, $connexionCom);
                }

                ++$i;
            }
        }

        return $consultationO;
    }

    /**
     * @param $invite
     * @param $consultationO
     * @param $tabGuests
     * @param $connexionCom
     *
     * @throws PropelException
     */
    public static function addFavorisPonctuelle($invite, $consultationO, $tabGuests, $connexionCom)
    {
        if (array_key_exists($invite->getId(), $tabGuests)) {
            if (true === $tabGuests[$invite->getId()]) {
                $commonConsultationFavorisQuery = new CommonConsultationFavorisQuery();
                $checkedCommonConsultationFavoris = $commonConsultationFavorisQuery->findOneByArray([
                    'idConsultation' => $consultationO->getId(),
                    'idAgent' => $invite->getId(),
                ], $connexionCom);

                $commonConsultationQuery = new CommonConsultationQuery();
                $checkedCommonConsultation = $commonConsultationQuery->findOneByArray([
                    'reference' => $consultationO->getId(),
                ], $connexionCom);
                if (null === $checkedCommonConsultation) {
                    $commonConsultationQuery = new CommonConsultationQuery();
                    $checkedCommonConsultation = $commonConsultationQuery->findOneByArray([
                        'id' => $consultationO->getId(),
                    ], $connexionCom);
                }

                if (
                    $checkedCommonConsultationFavoris === null
                    && $checkedCommonConsultation !== null
                ) {
                    $addConsultationFavorisInvitePonctuel = new CommonConsultationFavoris();
                    $addConsultationFavorisInvitePonctuel->setIdAgent($invite->getId());
                    $addConsultationFavorisInvitePonctuel->setIdConsultation($consultationO->getId());
                    $addConsultationFavorisInvitePonctuel->save($connexionCom);
                }
            }
        }
    }

    public function retrieveTypeValidation($consultation)
    {
        $typeValidation = null;
        if ($consultation->getIdRegleValidation() == Atexo_Config::getParameter('REGLE_VALIDATION_TYPE_1')) {
            $typeValidation = Atexo_Config::getParameter('VALIDATION_SIMPLE');
        } elseif (
            $consultation->getIdRegleValidation() == Atexo_Config::getParameter('REGLE_VALIDATION_TYPE_2') ||
            $consultation->getIdRegleValidation() == Atexo_Config::getParameter('REGLE_VALIDATION_TYPE_3')
        ) {
            $typeValidation = Atexo_Config::getParameter('VALIDATION_FINALE');
        } elseif (
            $consultation->getIdRegleValidation() == Atexo_Config::getParameter('REGLE_VALIDATION_TYPE_4') ||
            $consultation->getIdRegleValidation() == Atexo_Config::getParameter('REGLE_VALIDATION_TYPE_5')
        ) {
            if ('0' == $consultation->getEtatApprobation()) {
                $typeValidation = Atexo_Config::getParameter('VALIDATION_INTERMEDIAIRE_ETAPE1');
            } else {
                $typeValidation = Atexo_Config::getParameter('VALIDATION_FINALE_ETAPE2');
            }
        }

        return $typeValidation;
    }

    /**
     * Retourne un tableau d'objet CategorieLot de la consultation appellante.
     */
    public function getAllLots($connexionCom = null, $consultationId = null, $organisme = null)
    {
        if (!$connexionCom) {
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        }
        if ($consultationId && $organisme) {
            $c = new Criteria();
            $c->add(CommonCategorieLotPeer::CONSULTATION_ID, $consultationId, Criteria::EQUAL);
            $c->add(CommonCategorieLotPeer::ORGANISME, $organisme, Criteria::EQUAL);
            $categorieLots = CommonCategorieLotPeer::doSelect($c, $connexionCom);
            if ($categorieLots) {
                return $categorieLots;
            }
        }

        return [];
    }

    /**
     * Retourne un objet Consultation.
     *
     * @param $consultationId
     * @param $organisme
     * @param bool $statutEnLigneSeulement
     * @param null $critere
     */
    public function retrieveConsultationForCompagny(
        $consultationId,
        $organisme,
        $statutEnLigneSeulement = true,
        $critere = null
    ): ?CommonConsultation {
        $referenceUtilisateur = null;
        if (!$critere) {
            $critere = new Atexo_Consultation_CriteriaVo();
        }
        $critere->setIdReference($consultationId);
        $critere->setCalledFromPortail(true);
        $critere->setSansCritereActive(true);
        if ($statutEnLigneSeulement) {
            $critere->setEtatConsultation(Atexo_Config::getParameter('STATUS_CONSULTATION'));
        }
        $accessCode = self::getCodeAccesConsultationRestreinte($consultationId, $organisme);
        $consultation = (new Atexo_Consultation())->retrieveConsultation($consultationId, $organisme);
        if ($consultation) {
            $referenceUtilisateur = $consultation->getReferenceUtilisateur();
        }

        $sessionConsultation = Atexo_CurrentUser::readFromSessionSf("contexte_authentification");
        if (
            !empty($sessionConsultation['criteriaCons'])
            && $sessionConsultation['criteriaCons'] instanceof Atexo_Consultation_CriteriaVo
        ) {
            $arrayDataToCompareWith = [
                $sessionConsultation['criteriaCons']->getAcronymeOrganismeProcRest(),
                $sessionConsultation['criteriaCons']->getReferenceConsultationRestreinte(),
                $sessionConsultation['criteriaCons']->getCodeAccesProcedureRestreinte()
            ];
        } elseif (!empty(Atexo_CurrentUser::readFromSessionSf("contexte_authentification_{$consultationId}"))) {
            $sessionConsultation = Atexo_CurrentUser::readFromSessionSf("contexte_authentification_{$consultationId}");
            $arrayDataToCompareWith = [
                $sessionConsultation['organisme_consultation'],
                $sessionConsultation['reference_utilisateur_consultation'],
                $sessionConsultation['codeAcces']
            ];
        } else {
            $organismeSession = Atexo_CurrentUser::readFromSession('organisme');
            $referenceSession = Atexo_CurrentUser::readFromSession('reference');
            $codeAcces = Atexo_CurrentUser::readFromSession('codeAcces');

            $arrayDataToCompareWith = [$organismeSession, $referenceSession, $codeAcces];
        }

        if (
            $arrayDataToCompareWith[1]
            && $organisme == $arrayDataToCompareWith[0]
            && strstr(strtolower($referenceUtilisateur), strtolower($arrayDataToCompareWith[1]))
            && $accessCode && in_array($accessCode, $arrayDataToCompareWith)
        ) {
            $critere->setCodeAccesProcedureRestreinte($accessCode);
            $critere->setAcronymeOrganismeProcRest($organisme);
            $critere->setTypeAcces(2);
        }
        if ($organisme) {
            $critere->setAcronymeOrganisme($organisme);
        }
        $consultationArray = self::search($critere);
        $consultation = array_shift($consultationArray);

        return $consultation;
    }

    public function searchDocument(Atexo_Consultation_DocumentExterneVo $criteriaVo)
    {
        $org = Atexo_CurrentUser::getCurrentOrganism();
        $query = 'SELECT * FROM DocumentExterne ';
        $join = '';
        $otherQueries = '';
        $orderBy = '';

        $otherQueries = " AND organisme = '" . $org . "'";
        if ($criteriaVo->getRefConsultation()) {
            $consultationId = (new Atexo_Db())->quote($criteriaVo->getRefConsultation());
            $otherQueries .= " AND DocumentExterne.consultation_id = '" . $consultationId . "' ";
        }
        if ($criteriaVo->getIdBlob()) {
            $idBlob = (new Atexo_Db())->quote($criteriaVo->getIdBlob());
            $otherQueries .= " AND DocumentExterne.idBlob = '" . $idBlob . "' ";
        }
        if ($criteriaVo->getIdEntreprise()) {
            $idEntreprise = (new Atexo_Db())->quote($criteriaVo->getIdEntreprise());
            $otherQueries .= " AND DocumentExterne.idEntreprise = '" . $idEntreprise . "' ";
        }
        if ($criteriaVo->getType()) {
            $type = (new Atexo_Db())->quote($criteriaVo->getType());
            $otherQueries .= " AND DocumentExterne.type = '" . $type . "' ";
        }
        if ($criteriaVo->getNom()) {
            $nom = (new Atexo_Db())->quote($criteriaVo->getNom());
            $otherQueries .= " AND DocumentExterne.nom = '" . $nom . "' ";
        }
        if ($criteriaVo->getDescription()) {
            $description = (new Atexo_Db())->quote($criteriaVo->getDescription());
            $otherQueries .= " AND DocumentExterne.description = '" . $description . "' ";
        }
        if ($criteriaVo->getDate()) {
            $date = (new Atexo_Db())->quote($criteriaVo->getDate());
            $otherQueries .= " AND DocumentExterne.date = '" . $date . "' ";
        }

        if ($criteriaVo->getSensOrderBy()) {
            if ('default' == $criteriaVo->getSortByElement()) {
                $orderBy .= ' ORDER BY DocumentExterne.nom ASC ';
            } else {
                $orderBy .= ' ORDER BY DocumentExterne.' . (new Atexo_Db())->quote($criteriaVo->getSortByElement()) . ' ' . (new Atexo_Db())->quote($criteriaVo->getSensOrderBy());
            }
        }

        $finalQuery = $query . $join . ' WHERE 1 ' . $otherQueries . $orderBy;

        $stmt = Atexo_Db::getLinkCommon(true)->prepare($finalQuery);
        $stmt->execute();
        //      $documentExterne = $stmt->fetchAll(PDO::FETCH_NUM);
//      $stmt = $connexion->createStatement();
//      $rs = $stmt->executeQuery($finalQuery, ResultSet::FETCHMODE_NUM);
        $documentExterne = BaseCommonDocumentExternePeer::populateObjects($stmt);

        return $documentExterne;
    }

    public function retrieveConsultationsByService($idService, $org)
    {
        if (!$org) {
            $org = Atexo_CurrentUser::getCurrentOrganism();
        }
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonConsultationPeer::ORGANISME, $org);
        $cton1 = $c->getNewCriterion(CommonConsultationPeer::SERVICE_ID, $idService);
        $cton2 = $c->getNewCriterion(CommonConsultationPeer::SERVICE_ASSOCIE_ID, $idService);

        $cton1->addOr($cton2);
        $c->add($cton1);

        $consultations = CommonConsultationPeer::doSelect($c, $connexion);
        if (is_array($consultations) && count($consultations)) {
            return $consultations;
        } else {
            return false;
        }
    }

    /**
     * Fonction qui retourne l'url d'acces direct a la consultation.
     */
    public function getUrlAcceeDirect($consultation, $org)
    {
        if ($consultation->getTypePublicite() == Atexo_Config::getParameter('TYPE_PROCEDURE_PUBLICITE')) {
            return self::getUrlAccesDirectProcPubliciteOuverte($consultation->getId(), $org);
        } elseif ($consultation->getTypePublicite() == Atexo_Config::getParameter('TYPE_PROCEDURE_RESTREINTE')) {
            return self::getUrlAccesDirectProcPubliciteRestreinte($consultation->getId(), $org);
        }

        return false;
    }

    /**
     * retourne l'url d'acces direct a la consultation depuis le portail Entreprise.
     */
    public function getUrlAccesDirectProcPubliciteOuverte($consultationId, $org = null)
    {
        $pfURL = Atexo_MultiDomaine::getPfUrl($org, true);

        return $pfURL . '?page=Entreprise.EntrepriseAdvancedSearch&AllCons&id=' . $consultationId . '&orgAcronyme=' . $org;
    }

    /**
     * retourne l'url d'acces direct a la consultation (restreinte) depuis le portail Entreprise.
     */
    public function getUrlAccesDirectProcPubliciteRestreinte($consultationId, $org = null)
    {
        $pfURL = Atexo_MultiDomaine::getPfUrl($org, true);
        $url = $pfURL . '?page=Entreprise.EntrepriseAdvancedSearch&id=' . $consultationId . '&orgAcronyme=' . $org;

        return $url;
    }

    /**
     * MAPA.
     */
    public function retrieveConsultationByProcedureType($idProcedure, $organisme)
    {
        $c = new Criteria();
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonConsultationPeer::ID_TYPE_PROCEDURE_ORG, $idProcedure);
        $c->add(CommonConsultationPeer::ORGANISME, $organisme);
        $consultations = CommonConsultationPeer::doSelect($c, $connexion);

        return $consultations;
    }

    public function retrieveMaxNumeroPhaseByReferenceUtilisateur($referenceUtilisateur, $organisme)
    {
        $sql = "Select max(numero_phase) from consultation where reference_utilisateur='" . (new Atexo_Db())->quote($referenceUtilisateur) . "' AND organisme='" . (new Atexo_Db())->quote($organisme) . "' ";
        $statement = Atexo_Db::getLinkCommon(true)->query($sql);
        $max = 0;
        if ($statement) {
            foreach ($statement as $row) {
                $max = $row[0];
            }
        }

        return $max;
    }

    public function retrieveFirstPhaseConsultation($referenceUtilisitaeur, $organisme)
    {
        $params = [];
        $reference = null;
        $sql = 'Select id, min(numero_phase) from consultation where consultation.id_type_avis=:idTypeAvis AND '
            . 'reference_utilisateur=:refUtilisateur AND organisme=:organisme group by id';

        $params[':idTypeAvis'] = Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION');
        $params[':refUtilisateur'] = $referenceUtilisitaeur;
        $params[':organisme'] = $organisme;

        $statement = Atexo_Db::getLinkCommon(true)->prepare($sql);
        $statement->execute($params);

        if ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            $reference = $row['id'];
        }

        return $reference;
    }

    public function getConsultationByReferenceConnecteur($referenceConnecteur, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonConsultationPeer::REFERENCE_CONNECTEUR, $referenceConnecteur);
        $c->add(CommonConsultationPeer::ORGANISME, $organisme);
        $consultation = CommonConsultationPeer::doSelectOne($c, $connexion);
        if ($consultation instanceof CommonConsultation) {
            return $consultation;
        }

        return false;
    }

    public function getLibelleLieuExecution($idLieu)
    {
        $geoN2 = (new Atexo_Geolocalisation_GeolocalisationN2())->retrieveGeolocalisationN2ById($idLieu);
        if (is_array($geoN2)) {
            $geoN2 = array_shift($geoN2);
            if ($geoN2 instanceof CommonGeolocalisationN2) {
                return $geoN2->getDenomination1();
            }
        }

        return '';
    }

    public function getNombreLot($consultationId, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonCategorieLotPeer::CONSULTATION_ID, $consultationId);
        $c->add(CommonCategorieLotPeer::ORGANISME, $organisme);
        $lots = CommonCategorieLotPeer::doSelect($c, $connexion);
        if (is_array($lots)) {
            return count($lots);
        }

        return 0;
    }

    /***
     * supprime une consultation donnee
     * @param $refConsultation : la reference de la consultation ,$organisme: l'organisme en question
     */

    public function deleteConsultation($id, $organisme)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));

        $c = new Criteria();
        $c->add(CommonConsultationPeer::ORGANISME, $organisme);
        $c->add(CommonConsultationPeer::ID, $id);
        CommonConsultationPeer::doDelete($c, $connexionCom);
    }

    /**
     * retourne l'url d'acces direct a l'annonce dont le type est donne en paramettre depuis le portail Entreprise.
     */
    public function getUrlAccesDirectAnnonce($typeAnnonce, $refCons, $org)
    {
        $url = self::getUrlAccesDirectProcPubliciteOuverte($refCons, $org);
        if ($typeAnnonce) {
            $url = str_replace('AllCons', $typeAnnonce, $url);
        }

        return $url;
    }

    /**
     * retourne un tableau dont les cles sont les acronymes et la valeur du max des references.
     */
    public function retreiveLastConsRefForAllOrg()
    {
        $querySelect = ' Select  max(consultation.id) as maxRef,consultation.organisme';
        $querySelect .= ' FROM consultation group by consultation.organisme';
        $statement = Atexo_Db::getLinkCommon(true)->prepare($querySelect);
        $results = $statement->execute();
        $data = [];
        if ($results) {
            while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                $data[$row['organisme']] = $row['maxRef'];
            }
        }

        return $data;
    }

    /**
     * @param string $reference
     * @param string $acronyme
     * @return CommonConsultation|false
     * @throws PropelException
     */
    public function retreiveConsultationByReferenceAndAcronyme(string $reference, string $acronyme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonConsultationPeer::REFERENCE, $reference);
        $c->add(CommonConsultationPeer::ORGANISME, $acronyme);
        $consultation =  CommonConsultationPeer::doSelect($c, $connexion);
        if (is_array($consultation) && count($consultation) == 1 && $consultation[0] instanceof CommonConsultation) {
            return $consultation[0];
        } else {
            return false;
        }
    }

    /**
     * @param string $reference
     * @param string $acronyme
     * @return CommonConsultation|false
     * @throws PropelException
     */
    public function retrieveConsultationById(string $id)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB')
            . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonConsultationPeer::ID, $id);
        $consultation =  CommonConsultationPeer::doSelect($c, $connexion);
        if (is_array($consultation) && count($consultation) == 1 && $consultation[0] instanceof CommonConsultation) {
            return $consultation[0];
        } else {
            return false;
        }
    }

    /**
     * Met a jour le statut de la consultation.
     */
    public function updateStatutConsultation($idCons, $org, $idEtatConsultation)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));
        $consultation = CommonConsultationPeer::retrieveByPK($idCons, $connexionCom);
        $consultation->setIdEtatConsultation($idEtatConsultation);
        $consultation->save($connexionCom);
    }

    public function referenceUtilisateurExiste($organisme, $referenceU, $idService = null, $affaireExclu = null)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));
        $c = new Criteria();
        $c->add(CommonConsultationPeer::REFERENCE_UTILISATEUR, $referenceU);
        $c->add(CommonConsultationPeer::ORGANISME, $organisme);

        if (null !== $affaireExclu) {
            $c->add(CommonConsultationPeer::ID_AFFAIRE, $affaireExclu, Criteria::NOT_EQUAL);
        }
        if (null !== $idService) {
            $c->add(CommonConsultationPeer::SERVICE_ID, $idService);
        }
        $cons = CommonConsultationPeer::doSelect($c, $connexion);
        if (is_array($cons) && count($cons) > 1) {
            return true;
        }

        return false;
    }

    /**
     * retourne la liste des visites des lieux d'une consultation'.
     */
    public function retreiveVisitesLieuxByRef($consultationId, $lot, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));
        $c = new Criteria();
        $c->add(CommonVisiteLieuxPeer::ID, $consultationId);
        $c->add(CommonVisiteLieuxPeer::ORGANISME, $organisme);
        if ($lot) {
            $c->add(CommonVisiteLieuxPeer::LOT, $lot);
        }
        $result = CommonVisiteLieuxPeer::doSelect($c, $connexion);
        if ($result) {
            return $result;
        } else {
            return [];
        }
    }

    public function deleteVisiteLieuById($idVisite, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));

        return CommonVisiteLieuxPeer::doDelete([$idVisite, $organisme], $connexion);
    }

    /**
     * retourne les adresses lie au service.
     */
    public function retreiveAdressesServices($IdService, $organisme)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonGestionAdressesPeer::SERVICE_ID, $IdService);
        $c->add(CommonGestionAdressesPeer::ORGANISME, $organisme);
        $result = CommonGestionAdressesPeer::doSelectOne($c, $connexionCom);
        if ($result) {
            return $result;
        } else {
            return false;
        }
    }

    public function getLibelleLieuxExecution($langue, $consultation)
    {
        $arIds = explode(',', $consultation->getLieuExecution());

        return Atexo_Geolocalisation_GeolocalisationN2::getLibelleLieuExecution($arIds, $langue);
    }

    /*
     * Retourne les lieux d'execution de niveau geolocalisation N2
     */

    public function retreiveConsultationAgent($idServiceAgentConnected, $currentOrg, $idAgent, $idRef)
    {
        $criteria = new Atexo_Consultation_CriteriaVo();
        $criteria->setcalledFromPortail(false);
        $criteria->setIdService($idServiceAgentConnected);
        $criteria->setAcronymeOrganisme($currentOrg);
        $criteria->setIdReference($idRef);
        $criteria->setConnectedAgentId($idAgent);
        $consultation = self::search($criteria);

        if (is_array($consultation) && 1 == count($consultation) && $consultation[0] instanceof CommonConsultation) {
            return true;
        } else {
            return false;
        }
    }

    /** retourne les adresses lie au service.
     *
     */
    public function retreiveConsultationByRefPartenaire($refCons, $connexion, $organisme = null)
    {
        $c = new Criteria();
        $c->add(CommonConsultationPeer::REF_ORG_PARTENAIRE, $refCons);
        if ($organisme) {
            $c->add(CommonConsultationPeer::ORGANISME, $organisme);
        }
        $consultation = CommonConsultationPeer::doSelectOne($c, $connexion);
        if ($consultation) {
            return $consultation;
        } else {
            return false;
        }
    }

    public function annulerConsultation($id, $organisme)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));
        $commonConsultation = CommonConsultationPeer::retrieveByPK($id, $connexionCom);
        $commonConsultation->setConsultationAnnulee('1');
        $commonConsultation->setIdEtatConsultation(Atexo_Config::getParameter('STATUS_DECISION'));
        $commonConsultation->setDateDecision(date('Y-m-d'));
        $commonConsultation->save($connexionCom);
    }

    /***
     * Retourne les consultations avec le status archive realisee.
     */
    public function retrieveConsultationArchivee($organisme)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonConsultationPeer::ID_ETAT_CONSULTATION, Atexo_Config::getParameter('STATUS_ARCHIVE_REALISEE'));
        $c->add(CommonConsultationPeer::ORGANISME, $organisme);
        $c->add(CommonConsultationPeer::DATE_ARCHIVAGE, Atexo_Util::dateDansPasse(date('Y-m-d'), 0, Atexo_Config::getParameter('DELAI_ACCESSIBILITE_DIC_APRES_ARCHIVAGE'), 0, 0), Criteria::LESS_THAN);
        $c->addAnd(CommonConsultationPeer::DATE_ARCHIVAGE, null, Criteria::ISNOTNULL);
        $c->addAnd(CommonConsultationPeer::DATE_ARCHIVAGE, '', Criteria::NOT_EQUAL);

        $consultation = CommonConsultationPeer::doSelect($c, $connexionCom);
        if ($consultation) {
            return $consultation;
        } else {
            return [];
        }
    }

    public function isDicNotDownloadable($consultation)
    {
        $dateArchivage = Atexo_Util::iso2frnDate(substr($consultation->getDateArchivage(), 0, 10));

        if (
            $consultation->getIdEtatConsultation() == Atexo_Config::getParameter('STATUS_ARCHIVE_REALISEE')
            && $dateArchivage && Atexo_Util::diffJours(Atexo_Config::getParameter('DELAI_ACCESSIBILITE_DIC_APRES_ARCHIVAGE'), $dateArchivage) < date('d/m/Y')
        ) {
            return true; // is Not Downloadable anymore.
        }

        return false; // still Downloadable.
    }

    public function getConsultationByReferenceUser($referenceUser, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));

        $c = new Criteria();
        $c->add(CommonConsultationPeer::REFERENCE_UTILISATEUR, $referenceUser);
        $c->add(CommonConsultationPeer::ORGANISME, $organisme);
        $consultation = CommonConsultationPeer::doSelectOne($c, $connexion);
        if ($consultation instanceof CommonConsultation) {
            return $consultation;
        }

        return false;
    }

    /**
     * Retourne un objet Consultation.
     *
     * @param $consultationId reference  de la consultation
     */
    public function retrieveConsultationByRefConsultation($consultationId)
    {
        $critere = new Atexo_Consultation_CriteriaVo();
        $critere->setIdReference($consultationId);
        $critere->setAcronymeOrganisme(Atexo_CurrentUser::getOrganismAcronym());
        $critere->setIdService(Atexo_CurrentUser::getCurrentServiceId());
        $critere->setConnectedAgentId(Atexo_CurrentUser::getId());
        $consultationArray = self::search($critere);
        if (is_array($consultationArray) && 1 == count($consultationArray)) {
            $consultation = array_shift($consultationArray);

            return $consultation;
        } else {
            return false;
        }
    }

    /**
     * Retourne un objet Consultation partiel.
     *
     * @param $consultationId reference  de la consultation
     */
    public function retrieveConsultationByRefForCAO($consultationId)
    {
        $consultation = CommonConsultationQuery::create()
            ->filterByOrganisme(Atexo_CurrentUser::getOrganismAcronym())
            ->filterById($consultationId)
            ->select(['reference_utilisateur', 'objet', 'datefin', 'id_type_procedure_org', 'categorie', 'service_id', 'service_associe_id', 'date_mise_en_ligne_calcule'])
            ->findOne();

        return $consultation;
    }

    public function getEtatConsultation($db)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $etatConsultation = BaseCommonEtatConsultationPeer::doSelect($c, $connexion);
        $allEtatConsultation = [];
        foreach ($etatConsultation as $unStatut) {
            $allEtatConsultation[] = $unStatut->getCodeEtat();
        }

        return $allEtatConsultation;
    }

    public function getStatutEnveloppe($organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $statusEnveloppe = CommonStatutEnveloppePeer::doSelect($c, $connexion);
        $allStatuts = [];
        foreach ($statusEnveloppe as $unStatut) {
            $allStatuts[] = $unStatut->getDescription();
        }

        return $allStatuts;
    }

    public function retreiveVisitesLieuxWhereDateIs($id, $date, $organisme, $crittere = 0)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));
        $c = new Criteria();
        $c->add(CommonVisiteLieuxPeer::ID, $id);
        $c->add(CommonVisiteLieuxPeer::ORGANISME, $organisme, Criteria::EQUAL);
        if ('-1' == $crittere) {
            $c->add(CommonVisiteLieuxPeer::DATE, $date, Criteria::LESS_THAN);
        } elseif ('0' == $crittere) {
            $c->add(CommonVisiteLieuxPeer::DATE, $date);
        } elseif ('1' == $crittere) {
            $c->add(CommonVisiteLieuxPeer::DATE, $date, Criteria::GREATER_THAN);
        }

        $result = CommonVisiteLieuxPeer::doSelect($c, $connexion);
        if ($result) {
            return $result;
        } else {
            return false;
        }
    }

    public function getReunionLotsWhereDateIs($consultationId, $date, $organisme, $crittere = 0)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonCategorieLotPeer::CONSULTATION_ID, $consultationId, Criteria::EQUAL);
        $c->add(CommonCategorieLotPeer::ORGANISME, $organisme, Criteria::EQUAL);
        $c->add(CommonCategorieLotPeer::REUNION, '1');

        if ('-1' == $crittere) {
            $c->add(CommonCategorieLotPeer::DATE_REUNION, $date, Criteria::LESS_THAN);
        } elseif ('0' == $crittere) {
            $c->add(CommonCategorieLotPeer::DATE_REUNION, $date);
        } elseif ('1' == $crittere) {
            $c->add(CommonCategorieLotPeer::DATE_REUNION, $date, Criteria::GREATER_THAN);
        }

        $categorieLots = CommonCategorieLotPeer::doSelect($c, $connexionCom);
        if ($categorieLots) {
            return $categorieLots;
        } else {
            return false;
        }
    }

    public function getEchantillionLotsWhereDateIs($consultationId, $date, $organisme, $crittere = 0)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonCategorieLotPeer::CONSULTATION_ID, $consultationId, Criteria::EQUAL);
        $c->add(CommonCategorieLotPeer::ORGANISME, $organisme, Criteria::EQUAL);
        $c->add(CommonCategorieLotPeer::ECHANTILLON, 1);

        if ('-1' == $crittere) {
            $c->add(CommonCategorieLotPeer::DATE_LIMITE_ECHANTILLION, $date, Criteria::LESS_THAN);
        } elseif ('0' == $crittere) {
            $c->add(CommonCategorieLotPeer::DATE_LIMITE_ECHANTILLION, $date);
        } elseif ('1' == $crittere) {
            $c->add(CommonCategorieLotPeer::DATE_LIMITE_ECHANTILLION, $date, Criteria::GREATER_THAN);
        }

        $categorieLots = CommonCategorieLotPeer::doSelect($c, $connexionCom);
        if ($categorieLots) {
            return $categorieLots;
        } else {
            return false;
        }
    }

    public function retrieveConsultationByReferenceUtilisateur($refUtilisateur, $organisme = false)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonConsultationPeer::REFERENCE_UTILISATEUR, $refUtilisateur . '%', Criteria::LIKE);
        if ($organisme) {
            $c->add(CommonConsultationPeer::ORGANISME, $organisme);
        }
        $consultations = CommonConsultationPeer::doSelect($c, $connexionCom);
        if ($consultations) {
            return $consultations;
        }

        return false;
    }

    public function hasHabilitationModifCons($consultation)
    {
        $etatConsultation = $consultation->getStatusConsultation();
        $dateAjourdhui = date('Y-m-d H:i:s');
        $depouiablePhaseCons = $consultation->getDepouillablePhaseConsultation();

        if (
            $etatConsultation == Atexo_Config::getParameter('STATUS_ELABORATION')
            || $etatConsultation == Atexo_Config::getParameter('ETAT_EN_ATTENTE')
            || ($depouiablePhaseCons && $consultation->getDatefin() > $dateAjourdhui) || ($etatConsultation == Atexo_Config::getParameter('ETAT_EN_LIGNE'))
        ) {
            if ($consultation->getModifHabilitation() && !$consultation->getCurrentUserReadOnly()) {
                return true;
            } else {
                return false;
            }
        } elseif (
            Atexo_Module::isEnabled('AutoriserModificationApresPhaseConsultation', Atexo_CurrentUser::getCurrentOrganism())
            && (
                $depouiablePhaseCons
                || ($etatConsultation == Atexo_Config::getParameter('ETAT_DEPOUILLE')
                    || ($etatConsultation == (Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE') . Atexo_Config::getParameter('STATUS_DECISION'))))
                || $etatConsultation == Atexo_Config::getParameter('ETAT_ARCHIVE')
                || ($etatConsultation == Atexo_Config::getParameter('ETAT_CLOTURE'))
            )
        ) {
            if ($consultation->hasHabilitationModifierApresValidation() && !$consultation->getCurrentUserReadOnly()) {
                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * Permet de verifier si une consultation existe dans le panier de l'entreprise.
     *
     * @param  $consultationRef : la reference de la consultation
     * @param $organisme : l'acronyme de l'organisme
     * @return: true si consultation existe dans le panier, false sinon
     */
    public function verifierConsultationDansPanierEntreprise($consultationId, $organisme)
    {
        $panier = Atexo_Entreprise_PaniersEntreprises::getPanierEntreprise($organisme, $consultationId, Atexo_CurrentUser::getIdEntreprise(), Atexo_CurrentUser::getIdInscrit());
        if ($panier instanceof CommonPanierEntreprise) {
            return true;
        }

        return false;
    }

    /**
     * Permet de calculer la date et heure limite de remise des plis heure locale.
     *
     * @param $dateHeureLimiteRemisePlis : date et heure limite de remise des plis
     * @param  $decalageHoraireService : decalage horaire du service de l'agent connecte
     */
    public function calculDateLimiteRemisePlisLocale($dateHeureLimiteRemisePlis, $decalageHoraireService)
    {
        if (!str_contains($dateHeureLimiteRemisePlis, '/')) {
            $dateHeureLimiteRemisePlis = Atexo_Util::iso2frnDateTime($dateHeureLimiteRemisePlis);
        }
        //Decalage horaire en heure
        $decalageHoraire = Atexo_Util::getDecalageHoraireParRapportGmt($dateHeureLimiteRemisePlis);
        //Calcul de la date et heure limite de remise des plis locale
        $dateLimiteRemisePlisLocale = Atexo_Util::dateDansFutur(Atexo_Util::frnDateTime2iso($dateHeureLimiteRemisePlis), (int) ($decalageHoraireService) - (int) $decalageHoraire);

        return $dateLimiteRemisePlisLocale;
    }

    public function hasDoublon($urlP, $org)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonConsultationPeer::URL_CONSULTATION_EXTERNE, $urlP);
        $c->add(CommonConsultationPeer::ORGANISME, $org, Criteria::NOT_EQUAL);
        $commonConsultation = CommonConsultationPeer::doSelectOne($c, $connexion);
        if ($commonConsultation) {
            return $commonConsultation;
        } else {
            return false;
        }
    }

    public function getConsultationByOrganismeAndDoublon($organisme, $date = false, $doublon = 0)
    {
        if (false !== $date) {
            $requete = "SELECT id FROM consultation WHERE organisme='" . $organisme . "' and  datedebut >='" . $date . "' and doublon='" . $doublon . "'";
        } else {
            $requete = "SELECT id FROM consultation WHERE organisme='" . $organisme . "' and doublon ='" . $doublon . "'";
        }
        $statement = Atexo_Db::getLinkCommon(true)->prepare($requete);
        $results = $statement->execute();
        $data = [];
        if ($results) {
            while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                $data[] = $row['id'];
            }

            return $data;
        } else {
            return false;
        }
    }

    public function getConsultationByIdDomaineActivite($idDomaine)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));
        $c = new Criteria();
        $crit0 = $c->getNewCriterion(CommonConsultationPeer::DOMAINES_ACTIVITES, '%#' . $idDomaine . '#%', Criteria::LIKE);
        $crit1 = $c->getNewCriterion(CommonConsultationPeer::DOMAINES_ACTIVITES, $idDomaine . '#%', Criteria::LIKE);
        $crit2 = $c->getNewCriterion(CommonConsultationPeer::DOMAINES_ACTIVITES, '%#' . $idDomaine, Criteria::LIKE);

        $crit0->addOr($crit1);
        $crit0->addOr($crit2);
        $c->add($crit0);
        $arrayConsultation = CommonConsultationPeer::doSelect($c, $connexion);
        if ($arrayConsultation) {
            return $arrayConsultation;
        } else {
            return false;
        }
    }

    public function getInfosConsPrRapportSignature($reference, $consultation = null)
    {
        $infoRapportPDF = [];
        if (!($consultation instanceof CommonConsultation)) {
            $org = Atexo_CurrentUser::getCurrentOrganism();
            if ($reference) {
                $criteria = new Atexo_Consultation_CriteriaVo();
                $criteria->setcalledFromPortail(false);
                $criteria->setIdService(Atexo_CurrentUser::getCurrentServiceId());
                $criteria->setAcronymeOrganisme($org);
                $criteria->setIdReference($reference);
                $criteria->setConnectedAgentId(Atexo_CurrentUser::getId());
                $consultation = self::search($criteria);
                if (1 == count($consultation) && $consultation[0] instanceof CommonConsultation) {
                    $consultation = array_shift($consultation);
                }
            }
        }

        if ($consultation instanceof CommonConsultation) {
            $org = $consultation->getOrganisme();
            $langue = Atexo_CurrentUser::readFromSession('lang');
            $abbreviationLangue = Atexo_Languages::getLanguageAbbreviation($langue);
            $getIntituleAvis = 'getIntituleAvis' . $abbreviationLangue;
            $getDenominationOrg = 'getDenominationOrg' . $abbreviationLangue;
            $infoRapportPDF['RefConsUser'] = $consultation->getReferenceUtilisateur();
            $infoRapportPDF['entiteAchat'] = Atexo_EntityPurchase::getPathEntityById($consultation->getServiceId(), $org, $langue);
            $organismeO = Atexo_Organismes::retrieveOrganismeByAcronyme($org);
            if (0 == strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) || !$organismeO->$getDenominationOrg()) {
                $infoRapportPDF['entitePublique'] = $organismeO->getSigle() . ' - ' . $organismeO->getDenominationOrg();
            } else {
                $infoRapportPDF['entitePublique'] = $organismeO->getSigle() . ' - ' . $organismeO->$getDenominationOrg();
            }
            $typeAvis = (new Atexo_Consultation_TypeAvis())->retrieveTypeAnnonce($consultation->getIdTypeAvis());
            if ($typeAvis) {
                if (0 == strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) || !$typeAvis->$getIntituleAvis()) {
                    $infoRapportPDF['typeAnnonce'] = $typeAvis->getIntituleAvis();
                } else {
                    $infoRapportPDF['typeAnnonce'] = $typeAvis->$getIntituleAvis();
                }
            }
            $infoRapportPDF['typeProcedure'] = (new Atexo_Consultation_ProcedureType())->retrieveTypeProcedure($consultation->getIdTypeProcedureOrg(), false, null, $langue);
            $infoRapportPDF['intituleConsultation'] = $consultation->getIntituleTraduit();
            $infoRapportPDF['objetConsultation'] = $consultation->getObjetTraduit();
            $infoRapportPDF['dateRemisePlis'] = Atexo_Util::iso2frnDateTime($consultation->getDatefin());
        }

        return $infoRapportPDF;
    }

    public function getUrlAccesDirectParTypeAvis($consultationId, $org, $idTypeAvis)
    {
        $typeAvis = self::getTypeAvis($idTypeAvis);

        return Atexo_Config::getParameter('PF_URL', $org) . '?page=Entreprise.EntrepriseAdvancedSearch&' . $typeAvis . '&id=' . $consultationId . '&orgAcronyme=' . $org;
    }

    /*
 * retourne l'url d'acces direct a un avis
 */

    public function getTypeAvis($idTypeAvis)
    {
        return match ($idTypeAvis) {
            Atexo_Config::getParameter('TYPE_AVIS_INFORMATION') => 'AvisInformation',
            Atexo_Config::getParameter('TYPE_AVIS_EXTRAIT_PV') => 'AvisExtraitPV',
            Atexo_Config::getParameter('TYPE_AVIS_ATTRIBUTION') => 'AvisAttribution',
            Atexo_Config::getParameter('TYPE_AVIS_RAPPORT_ACHEVEMENT') => 'AvisRapportAchevement',
            default => '',
        };
    }

    /**
     * Permet de charger les valeurs parametrees des procedures equivalences dans l'objet consultation
     * passe en parametres.
     *
     * @param CommonConsultation $consultation : objet passe par reference
     * @param $idTypeProcedure : l'identifiant du type de procedure
     */
    public function loadConsultationWithParametersProceduresEquivalence(&$consultation, $idTypeProcedure)
    {
        $procedureEquivalence = Atexo_Consultation_ProcedureEquivalence::retrieveProcedureEquivalenceById($idTypeProcedure);
        if ($procedureEquivalence instanceof CommonProcedureEquivalence) {
            //Enregistrement des types de decision pour cette consultation
            self::loadInfosDecisionWithParametersProceduresEquivalence($consultation, $procedureEquivalence);

            // Onglet modalites reponse
            self::loadResponseModalitiesWithParametersProceduresEquivalence($consultation, $procedureEquivalence);

            // Onglet Calendrier
            self::loadCalendarWithParametersProceduresEquivalence($consultation, $procedureEquivalence);

            // Onglet documents joints
            self::loadAttachmentsWithParametersProceduresEquivalence($consultation, $procedureEquivalence);
        }
    }

    /**
     * Permet de charger les valeurs parametrees des procedures equivalences au niveau de la decision.
     *
     * @param CommonConsultation         $consultation         : objet passe par reference
     * @param CommonProcedureEquivalence $procedureEquivalence
     */
    public function loadInfosDecisionWithParametersProceduresEquivalence(&$consultation, $procedureEquivalence)
    {
        $consultation->setTypeDecisionARenseigner((('+0' == $procedureEquivalence->getTypeDecisionARenseigner()) ? '1' : '0'));
        $consultation->setTypeDecisionAttributionMarche((('+0' == $procedureEquivalence->getTypeDecisionAttributionMarche()) ? '1' : '0'));
        $consultation->setTypeDecisionDeclarationSansSuite((('+0' == $procedureEquivalence->getTypeDecisionDeclarationSansSuite()) ? '1' : '0'));
        $consultation->setTypeDecisionDeclarationInfructueux((('+0' == $procedureEquivalence->getTypeDecisionDeclarationInfructueux()) ? '1' : '0'));
        $consultation->setTypeDecisionSelectionEntreprise((('+0' == $procedureEquivalence->getTypeDecisionSelectionEntreprise()) ? '1' : '0'));
        $consultation->setTypeDecisionAttributionAccordCadre((('+0' == $procedureEquivalence->getTypeDecisionAttributionAccordCadre()) ? '1' : '0'));
        $consultation->setTypeDecisionAdmissionSad((('+0' == $procedureEquivalence->getTypeDecisionAdmissionSad()) ? '1' : '0'));
        $consultation->setTypeDecisionAutre((('+0' == $procedureEquivalence->getTypeDecisionAutre()) ? '1' : '0'));
    }

    /**
     * Permet de charger les valeurs parametrees des procedures equivalences dans l'etape modalites de reponse.
     *
     * @param CommonConsultation         $consultation         : objet passe par reference
     * @param CommonProcedureEquivalence $procedureEquivalence
     */
    public function loadResponseModalitiesWithParametersProceduresEquivalence(&$consultation, $procedureEquivalence)
    {
        //Reponse electronique
        if ('+1' == $procedureEquivalence->getElecResp() || '1' == $procedureEquivalence->getElecResp()) {
            $consultation->setAutoriserReponseElectronique('1');
        } else {
            $consultation->setAutoriserReponseElectronique('0');
        }
        if ('+1' == $procedureEquivalence->getRepObligatoire() || '1' == $procedureEquivalence->getRepObligatoire()) {
            $consultation->setReponseObligatoire('1');
        } else {
            $consultation->setReponseObligatoire('0');
        }

        //Acces a la consultation pour les entreprises
        if ('+1' == $procedureEquivalence->getPoursuiteDateLimiteRemisePli() || '1' == $procedureEquivalence->getPoursuiteDateLimiteRemisePli()) {
            $consultation->setPoursuivreAffichage('0');
        } elseif ($procedureEquivalence->getDelaiPoursuiteAffichage()) {
            $consultation->setPoursuivreAffichage($procedureEquivalence->getDelaiPoursuiteAffichage());
            $consultation->setPoursuivreAffichageUnite($procedureEquivalence->getDelaiPoursuivreAffichageUnite());
        } else {
            $consultation->setPoursuivreAffichage('1');
        }

        //Signature electronique requise
        if ('+1' == $procedureEquivalence->getSignatureEnabled() || '1' == $procedureEquivalence->getSignatureEnabled()) {
            $consultation->setSignatureOffre('1');
        } else {
            $consultation->setSignatureOffre('0');
        }
        //Chiffrement de la reponse
        if ('+1' == $procedureEquivalence->getCipherEnabled() || '1' == $procedureEquivalence->getCipherEnabled()) {
            $consultation->setChiffrementOffre('1');
        } else {
            $consultation->setChiffrementOffre('0');
        }
        //Enveloppe et questionnaire
        //if (Atexo_Module::isEnabled("GestionModelesFormulaire")) {
        if ('1' == $procedureEquivalence->getEnvCandidature() || '+1' == $procedureEquivalence->getEnvCandidature()) {
            $consultation->setEnvCandidature(1);
        } else {
            $consultation->setEnvCandidature(0);
        }
        if ('1' != $procedureEquivalence->getEnvOffre() && '+1' != $procedureEquivalence->getEnvOffre()) {
            $consultation->setEnvOffre(0);
        } else {
            $consultation->setEnvOffre(1);
        }
        if ('1' == $procedureEquivalence->getEnvOffreTechnique() || '+1' == $procedureEquivalence->getEnvOffreTechnique()) {
            $consultation->setEnvOffreTechnique(1);
        } else {
            $consultation->setEnvOffreTechnique(0);
        }
        if ('1' == $procedureEquivalence->getEnvAnonymat() || '+1' == $procedureEquivalence->getEnvAnonymat()) {
            $consultation->setEnvAnonymat(1);
        } else {
            $consultation->setEnvAnonymat(0);
        }
        //}
        //Signature acte d'engagement
        if (
            ('1' == $procedureEquivalence->getSignaturePropre() || '+1' == $procedureEquivalence->getSignaturePropre())
            && ('1' == $procedureEquivalence->getSignatureEnabled() || '+1' == $procedureEquivalence->getSignatureEnabled())
        ) {
            $consultation->setSignatureActeEngagement(1);
        } else {
            $consultation->setSignatureActeEngagement(0);
        }
        //mode ouverture
        if ('1' == $procedureEquivalence->getModeOuvertureReponse() || '+1' == $procedureEquivalence->getModeOuvertureReponse()) {
            $consultation->setModeOuvertureReponse(1);
        } else {
            $consultation->setModeOuvertureReponse(0);
        }
    }

    /**
     * Permet de charger les valeurs parametrees des procedures equivalences dans l'etape calendrier.
     *
     * @param CommonConsultation         $consultation         : objet passe par reference
     * @param CommonProcedureEquivalence $procedureEquivalence
     */
    public function loadCalendarWithParametersProceduresEquivalence(&$consultation, $procedureEquivalence)
    {
        if ('+1' == $procedureEquivalence->getMiseEnLigne1() || '1' == $procedureEquivalence->getMiseEnLigne1()) {
            $consultation->setRegleMiseEnLigne(Atexo_Config::getParameter('MISE_EN_LIGNE_APRES_VALIDATION_ET_DATE'));
        } elseif ('+1' == $procedureEquivalence->getMiseEnLigne2() || '1' == $procedureEquivalence->getMiseEnLigne2()) {
            $consultation->setRegleMiseEnLigne(Atexo_Config::getParameter('MISE_EN_LIGNE_APRES_VALIDATION_MANUELLE'));
        } elseif ('+1' == $procedureEquivalence->getMiseEnLigne3() || '1' == $procedureEquivalence->getMiseEnLigne3()) {
            $consultation->setRegleMiseEnLigne(Atexo_Config::getParameter('MISE_EN_LIGNE_APRES_VALIDATION_ET_ENVOI_BOAMP'));
        } elseif ('+1' == $procedureEquivalence->getMiseEnLigne4() || '1' == $procedureEquivalence->getMiseEnLigne4()) {
            $consultation->setRegleMiseEnLigne(Atexo_Config::getParameter('MISE_EN_LIGNE_APRES_VALIDATION_ET_PUBLICATION_BOAMP'));
        } elseif (
            Atexo_Module::isEnabled('RegleMiseEnLigneParEntiteCoordinatrice')
            && ('+1' == $procedureEquivalence->getMiseEnLigneEntiteCoordinatrice()
                || '1' == $procedureEquivalence->getMiseEnLigneEntiteCoordinatrice())
        ) {
            $consultation->setRegleMiseEnLigne(Atexo_Config::getParameter('MISE_EN_LIGNE_APRES_VALIDATION_ENTITE_COORDINATRICE'));
        }
        //autorisation publicite
        if (Atexo_Module::isEnabled('ParametragePubliciteParTypeProcedure')) {
            $consultation->setAutoriserPublicite($procedureEquivalence->getAutoriserPublicite());
        }
    }

    /**
     * Permet de charger les valeurs parametrees des procedures equivalences dans l'etape "documents joints".
     *
     * @param CommonConsultation         $consultation         : objet passe par reference
     * @param CommonProcedureEquivalence $procedureEquivalence
     */
    public function loadAttachmentsWithParametersProceduresEquivalence(&$consultation, $procedureEquivalence)
    {
        $consultation->setPartialDceDownload('1');
        if (
            ('+1' != $procedureEquivalence->getProcedurePublicite() && '1' != $procedureEquivalence->getProcedurePublicite())
            || ('+1' == $procedureEquivalence->getProcedureRestreinte() || '1' == $procedureEquivalence->getProcedureRestreinte())
        ) {
            $consultation->setTypeAcces(Atexo_Config::getParameter('TYPE_PROCEDURE_RESTREINTE'));
        } else {
            $consultation->setTypeAcces(Atexo_Config::getParameter('TYPE_PROCEDURE_PUBLICITE'));
        }

        if (
            ('+1' == $procedureEquivalence->getGestionEnvoisPostaux() || '1' == $procedureEquivalence->getGestionEnvoisPostaux())
            || ('+1' != $procedureEquivalence->getConstitutionDossierReponse() && '1' != $procedureEquivalence->getConstitutionDossierReponse())
        ) {
            $consultation->setTiragePlan('0');
        } else {
            if ('+1' == $procedureEquivalence->getTireurPlanPapier() || '1' == $procedureEquivalence->getTireurPlanPapier()) {
                $consultation->setTiragePlan('1');
            }
            if ('+1' == $procedureEquivalence->getTireurPlanCdrom() || '1' == $procedureEquivalence->getTireurPlanCdrom()) {
                $consultation->setTiragePlan('3');
            }
        }
    }

    /**
     * Permet de copier les donnees complementaires de la consultation ou des lots.
     *
     * @param $objet : instance de CommonConsultation ou de CommonCategorieLot
     * @param $connexion : objet connexion
     */
    public function copierDonneesComplementaires($objet, $connexion)
    {
        if (($objet instanceof CommonConsultation) || ($objet instanceof CommonCategorieLot)) {
            $oldDonneesComplementaires = $objet->getDonneComplementaire();
            if ($oldDonneesComplementaires instanceof CommonTDonneeComplementaire) {
                $donneesComplementaires = $oldDonneesComplementaires->copy();
                $donneesComplementaires->setNew(true);

                //Sauvegarde donnees complementaires
                $donneesComplementaires->save($connexion);

                //Criteres d'attribution
                self::copierCriteresAttribution($oldDonneesComplementaires, $donneesComplementaires, $connexion);
                $donneesComplementaires->save($connexion);

                //Forme du marche et forme(s) de prix
                self::copierFormeMarcheFormePrix($oldDonneesComplementaires, $donneesComplementaires, $connexion);

                //Sauvegarde de la consultation ou du lot
                $objet->setIdDonneeComplementaire($donneesComplementaires->getIdDonneeComplementaire());
                $objet->save($connexion);
            }
        }
    }

    /**
     * Permet de copier les criteres d'attribution des donnees complementaires.
     *
     * @param $oldDonneesComplementaires
     * @param $donneesComplementaires
     * @param $connexion
     */
    public function copierCriteresAttribution($oldDonneesComplementaires, &$donneesComplementaires, $connexion)
    {
        if (is_array($oldDonneesComplementaires->getAllCriteresAttributions()) && count($oldDonneesComplementaires->getAllCriteresAttributions())) {
            $oldCriteresAttribution = $oldDonneesComplementaires->getAllCriteresAttributions();
            foreach ($oldCriteresAttribution as $uneOldCriteresAttribution) {
                if ($uneOldCriteresAttribution instanceof CommonTCritereAttribution) {
                    $newCriteresAttribution = $uneOldCriteresAttribution->copy();
                    $newCriteresAttribution->setNew(true);
                    //Sauvegarde
                    $newCriteresAttribution->setIdDonneeComplementaire($donneesComplementaires->getIdDonneeComplementaire());
                    $newCriteresAttribution->save($connexion);

                    $sousCriteres = $uneOldCriteresAttribution->getSousCriteres($connexion);
                    foreach ($sousCriteres as $sousCritere) {
                        $newSousCritere = $sousCritere->copy();
                        $newSousCritere->setNew(true);
                        //Sauvegarde
                        $newSousCritere->setIdCritereAttribution($newCriteresAttribution->getIdCritereAttribution());
                        $newSousCritere->save($connexion);
                    }
                }
            }
            //Affectation id critere attribution
            $donneesComplementaires->setIdCritereAttribution($oldDonneesComplementaires->getIdCritereAttribution());
        }
    }

    /**
     * Copie des formes de prix et formes de marche.
     *
     * @param $oldDonneesComplementaires
     * @param $donneesComplementaires
     * @param $connexion
     */
    public function copierFormeMarcheFormePrix($oldDonneesComplementaires, &$donneesComplementaires, $connexion)
    {
        //Existence de forme de prix
        if ($oldDonneesComplementaires->getFormePrix() instanceof CommonTFormePrix) {
            self::copierFormePrix($oldDonneesComplementaires, $donneesComplementaires, $connexion);
            //Sauvegarde donnees complementaires de nouveau
            $donneesComplementaires->save($connexion);
        } // Existence de tranches
        else {
            if (is_array($oldDonneesComplementaires->getAllTranches()) && count($oldDonneesComplementaires->getAllTranches())) {
                self::copierTranches($oldDonneesComplementaires, $donneesComplementaires, $connexion);
            }
        }
        //Lots techniques
        self::copierLotsTechniques($oldDonneesComplementaires, $donneesComplementaires, $connexion);
    }

    /**
     * Copie des formes de prix.
     *
     * @param $objet : instance de CommonTTranche ou de CommonTDonneeComplementaire
     * @param $newObjet : instance de CommonTTranche ou de CommonTDonneeComplementaire
     * @param $connexion
     */
    public function copierFormePrix($objet, &$newObjet, $connexion)
    {
        $oldFormePrix = $objet->getFormePrix();
        if ($oldFormePrix) {
            $formePrix = $oldFormePrix->copy();
            $formePrix->setNew(true);

            //Sauvegarde forme de prix
            $formePrix->save($connexion);
            //Copie types prix pour une forme prix
            self::copierTypesPrix($oldFormePrix, $formePrix, $connexion);
            //Copie variations prix unitaires pour une forme prix
            self::copierVariationsPrixUnitaire($oldFormePrix, $formePrix, $connexion);
            //Copie variations prix forfaitaires pour une forme prix
            self::copierVariationsPrixForfaitaires($oldFormePrix, $formePrix, $connexion);
            //Modification objet donnees complementaires
            $newObjet->setIdFormePrix($formePrix->getIdFormePrix());
        }
    }

    /**
     * Copie des types de prix.
     *
     * @param $oldFormePrix
     * @param $formePrix
     * @param $connexion
     */
    public function copierTypesPrix($oldFormePrix, $formePrix, $connexion)
    {
        if ($oldFormePrix->getAllTypesPrix()) {
            $oldTypesPrix = $oldFormePrix->getAllTypesPrix();
            if (is_array($oldTypesPrix) && count($oldTypesPrix)) {
                foreach ($oldTypesPrix as $uneOldTypesPrix) {
                    $oldIdTypePrix = $uneOldTypesPrix->getIdTypePrix();
                    $typesPrix = $uneOldTypesPrix->copy();
                    $typesPrix->setNew(true);
                    //Sauvegarde type de prix
                    $typesPrix->setIdFormePrix($formePrix->getIdFormePrix());
                    $typesPrix->setIdTypePrix($oldIdTypePrix);
                    $typesPrix->save($connexion);
                }
            }
        }
    }

    /**
     * Copie des variations prix unitaires.
     *
     * @param $oldFormePrix
     * @param $formePrix
     * @param $connexion
     */
    public function copierVariationsPrixUnitaire($oldFormePrix, $formePrix, $connexion)
    {
        if ($oldFormePrix->getAllVariationsPrixUnitaire()) {
            $oldVariationsPrixUnitaire = $oldFormePrix->getAllVariationsPrixUnitaire();
            if (is_array($oldVariationsPrixUnitaire) && count($oldVariationsPrixUnitaire)) {
                foreach ($oldVariationsPrixUnitaire as $uneOldVariationsPrixUnitaire) {
                    $oldIdVariation = $uneOldVariationsPrixUnitaire->getIdVariation();
                    $variationsPrixUnitaire = $uneOldVariationsPrixUnitaire->copy();
                    $variationsPrixUnitaire->setNew(true);
                    //Sauvegarde
                    $variationsPrixUnitaire->setIdFormePrix($formePrix->getIdFormePrix());
                    $variationsPrixUnitaire->setIdVariation($oldIdVariation);
                    $variationsPrixUnitaire->save($connexion);
                }
            }
        }
    }

    /**
     * Copie des variations prix forfaitaires.
     *
     * @param $oldFormePrix
     * @param $formePrix
     * @param $connexion
     */
    public function copierVariationsPrixForfaitaires($oldFormePrix, $formePrix, $connexion)
    {
        if ($oldFormePrix->getAllVariationsPrixForfaitaire()) {
            $oldVariationsPrixForfaitaire = $oldFormePrix->getAllVariationsPrixForfaitaire();
            if (is_array($oldVariationsPrixForfaitaire) && count($oldVariationsPrixForfaitaire)) {
                foreach ($oldVariationsPrixForfaitaire as $uneOldVariationsPrixForfaitaire) {
                    $oldIdVariation = $uneOldVariationsPrixForfaitaire->getIdVariation();
                    $variationsPrixForfaitaire = $uneOldVariationsPrixForfaitaire->copy();
                    $variationsPrixForfaitaire->setNew(true);
                    //Sauvegarde
                    $variationsPrixForfaitaire->setIdFormePrix($formePrix->getIdFormePrix());
                    $variationsPrixForfaitaire->setIdVariation($oldIdVariation);
                    $variationsPrixForfaitaire->save($connexion);
                }
            }
        }
    }

    /**
     * Copie des tranches.
     *
     * @param $oldDonneesComplementaires
     * @param $donneesComplementaires
     * @param $connexion
     */
    public function copierTranches($oldDonneesComplementaires, &$donneesComplementaires, $connexion)
    {
        $oldTranche = $oldDonneesComplementaires->getAllTranches();
        if (is_array($oldTranche) && count($oldTranche)) {
            foreach ($oldTranche as $uneOldTranche) {
                $tranche = $uneOldTranche->copy();
                $tranche->setNew(true);
                //Sauvegarde forme de prix
                $tranche->setIdDonneeComplementaire($donneesComplementaires->getIdDonneeComplementaire());
                //Copie des formes de prix des tranches
                self::copierFormePrix($uneOldTranche, $tranche, $connexion);
                $tranche->save($connexion);
            }
        }
    }

    /**
     * Permet de copier les lots techniques.
     *
     * @param $objet : anciennes donnees complementaires
     * @param $donneesComplementaires : nouvelles donnees complementaires
     * @param $connexion : objet connexion
     */
    public function copierLotsTechniques($objet, $donneesComplementaires, $connexion)
    {
        if ($objet instanceof CommonTDonneeComplementaire) {
            $oldLotsTechniques = $objet->getAllLotTechnique();
            if (is_array($oldLotsTechniques) && count($oldLotsTechniques)) {
                foreach ($oldLotsTechniques as $uneOldLotsTechniques) {
                    if ($uneOldLotsTechniques instanceof CommonTLotTechnique) {
                        $lotTechnique = $uneOldLotsTechniques->copy();
                        $lotTechnique->setNew(true);
                        //Sauvegarde
                        $lotTechnique->setIdDonneeComplementaire($donneesComplementaires->getIdDonneeComplementaire());
                        $lotTechnique->save($connexion);
                        //Tranches des lots techniques
                        self::copierTranchesLotsTechniques($uneOldLotsTechniques->getAllLotTechniqueTranche(), $lotTechnique, $connexion);
                    }
                }
            }
        }
    }

    /**
     * Copie des tranches des lots techniques.
     *
     * @param $oldLotsTechniquesTranches
     * @param $lotTechniques
     * @param $connexion
     */
    public function copierTranchesLotsTechniques($oldLotsTechniquesTranches, $lotTechnique, $connexion)
    {
        if (is_array($oldLotsTechniquesTranches) && count($oldLotsTechniquesTranches)) {
            foreach ($oldLotsTechniquesTranches as $uneOldLotTechniqueTranche) {
                $lotTechniqueTranche = $uneOldLotTechniqueTranche->copy();
                $lotTechniqueTranche->setNew(true);
                //Sauvegarde
                $lotTechniqueTranche->setIdTranche($uneOldLotTechniqueTranche->getIdTranche());
                $lotTechniqueTranche->setIdLotTechnique($lotTechnique->getIdLotTechnique());
                $lotTechniqueTranche->save($connexion);
            }
        }
    }

    /**
     * Permet de savoir si l'objet e une ClauseSociale.
     *
     * NB: deux conditions supplémentaires étaient indiqué pour afficher ou non l'icône des clausesSociales
     * qui sont situé dans les méthodes hasClauseQueSocialeExecutionAndInsertion et hasClauseSocialeMarcheReserve mais
     * nous avons simplifié la régle d'affichage de l'icône
     *
     * @param $objet
     */
    public static function hasClauseSociale($objet)
    {
        /** @var ClausesService $service */
        $service = Atexo_Util::getSfService(ClausesService::class);
        if ($objet instanceof CommonConsultation) {
            $type = 'consultation';
            $id = $objet->getId();
        } elseif ($objet instanceof CommonCategorieLot) {
            $type = 'lot';
            $id = $objet->getId();
        } elseif ($objet instanceof CommonTContratTitulaire) {
            $type = 'contrat';
            $id = $objet->getIdContratTitulaire();
        }

        return $objet && $service->hasClauseSocialeByIdAndType($id, $type);
    }

    public static function hasClauseQueSocialeExecutionAndInsertion($objet)
    {
        return ($objet && method_exists($objet, 'hasThisClause')) && (($objet->hasThisClause('ClauseSocialeConditionExecution') || $objet->hasThisClause('ClauseSocialeInsertion') || $objet->hasThisClause('MarcheInsertion')) && !self::hasClauseSocialeMarcheReserve($objet));
    }

    /**
     * Permet de savoir si l'objet e une ClauseSocialeConditionExecution.
     *
     * @param $objet :doit etre instance of  CommonCategorieLot ou CommonConsultation
     */
    public static function hasClauseSocialeMarcheReserve($objet)
    {
        return $objet && method_exists($objet, 'hasThisClause') && ($objet->hasThisClause('ClauseSocialeAteliersProteges') || $objet->hasThisClause('ClauseSocialeSiae') || $objet->hasThisClause('ClauseSocialeEss'));
    }

    /**
     * Permet de savoir si l'objet a une ClauseEnvironnementales.
     *
     * @param $objet
     */
    public static function hasClauseEnvironnementales($objet)
    {
        /** @var ClausesService $service */
        $service = Atexo_Util::getSfService(ClausesService::class);
        if ($objet instanceof CommonConsultation) {
            $type = 'consultation';
            $id = $objet->getId();
        } elseif ($objet instanceof CommonCategorieLot) {
            $type = 'lot';
            $id = $objet->getId();
        } elseif ($objet instanceof CommonTContratTitulaire) {
            $type = 'contrat';
            $id = $objet->getIdContratTitulaire();
        }

        return $objet && $service->hasClauseEnvironnementaleByIdAndType($id, $type);
    }

    /**
     * Permet de savoir si l'objet a une ClauseSocialeAteliersProteges.
     *
     * @param $objet :doit etre instance of  CommonCategorieLot ou CommonConsultation
     */
    public function hasClauseSocialeAteliersProteges($objet)
    {
        return ($objet && method_exists($objet, 'hasThisClause')) && $objet->hasThisClause('ClauseSocialeAteliersProteges');
    }

    /**
     * Creation de l'historique de la consultation.
     *
     * @param $arrayParameters : tableau des parametres
     * @param $connexion : objet connexion
     */
    public function saveHistoriqueConsultation($arrayParameters, $connexion)
    {
        //Debut recuperation des parametres
        $consultationId = $arrayParameters[0];
        $nomElement = $arrayParameters[1];
        $valeur = $arrayParameters[2];
        $detail1 = $arrayParameters[3];
        $detail2 = $arrayParameters[4];
        $lot = $arrayParameters[5];
        $org = $arrayParameters[6];
        $statut = $arrayParameters[7];
        //Fin recuperation des parametres
        $newHistorique = new CommonHistoriquesConsultation();
        $newHistorique->setConsultationId($consultationId);
        $newHistorique->setOrganisme($org);
        $newHistorique->setIdAgent(Atexo_CurrentUser::getIdAgentConnected());
        $newHistorique->setNomAgent(Atexo_CurrentUser::getFirstNameAgentConnected());
        $newHistorique->setPrenomAgent(Atexo_CurrentUser::getLastNameAgentConnected());
        $newHistorique->setNomElement($nomElement);
        $newHistorique->setValeur($valeur);
        $newHistorique->setValeurDetail1($detail1);
        $newHistorique->setValeurDetail2($detail2);
        $newHistorique->setNumeroLot($lot);
        $newHistorique->setStatut($statut);
        //      $atexoCrypto = new Atexo_Crypto();
        //      $arrayTimeStamp = $atexoCrypto->timeStampData($valeur . "-" . $detail1 . "-" . $detail2);
        //      $newHistorique->setHorodatage($arrayTimeStamp['horodatage']);
        $newHistorique->setUntrusteddate(date('Y-m-d H:i:s'));
        try {
            $newHistorique->save($connexion);
        } catch (Exception $exception) {
            echo $exception->getMessage();
        }
    }

    /**
     * Permet de preciser si la modification est permise.
     *
     * @param $consultation : objet consultation
     * @param bool $isContinuation : cas de creation d'une suite
     * @param bool $isRefCons      : cas de creation de la modification de la consultation
     *                             Si Oui retourne true, sinon false
     */
    public function isModificationAllowed($consultation, $isContinuation, $isRefCons)
    {
        if (
            $isRefCons
            && (Atexo_CurrentUser::hasHabilitation('ModifierConsultationAvantValidation') || $consultation->hasHabilitationModifierApresValidation())
            && !$isContinuation
        ) {
            return true;
        } else {
            return false;
        }
    }

    public function ajouterPj($consultationId, $organisme)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));
        $infile = Atexo_Config::getParameter('COMMON_TMP') . 'pieceJointe' . session_id() . time();
        if (move_uploaded_file($this->ajoutFichier->LocalName, $infile)) {
            $atexoBlob = new Atexo_Blob();
            $atexoCrypto = new Atexo_Crypto();
            $arrayTimeStampAvis = $atexoCrypto->timeStampFile($infile);

            $avis = new CommonAVIS();
            $avis->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
            $avis->setConsultationId($consultationId);
            $avis->setStatut(Atexo_Config::getParameter('DESTINATAIRE_VIDE'));

            if (is_array($arrayTimeStampAvis)) {
                $avis->setHorodatage($arrayTimeStampAvis['horodatage']);
                $avis->setUntrusteddate($arrayTimeStampAvis['untrustedDate']);
            }
            $avisIdBlob = $atexoBlob->insert_blob($this->ajoutFichier->FileName, $infile, $organisme);
            $avis->setAgentId(Atexo_CurrentUser::getIdAgentConnected());
            $avis->setAvis($avisIdBlob);
            $avis->setNomFichier($this->ajoutFichier->FileName);
            $avis->setStatut(Atexo_Config::getParameter('DESTINATAIRE_PUBLIE'));
            $avis->setDatePub(date('Y-m-d'));
            $avis->setType(Atexo_Config::getParameter('TYPE_FORMAT_LIBRE_FICHIER_JOINT'));
            $avis->save($connexionCom);

            return $avisIdBlob;
        }
    }

    /*
     * permet d ajouter un piece a une consultation (ex: fichier joint de motif de la decision)
     */

    /**
     * Permet de créer une consultation.
     *
     * @param Atexo_Rest_Action_ConsultationField $consultation , string $typeAvis le type d'avis
     *
     * @return mixed Atexo_Rest_Action_ConsultationField la consultation si c'est creee sinon string un message d'erreur
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function createConsultation($consultation, $oldServiceId = null)
    {
        $connexion = null;
        if ($consultation instanceof Atexo_Rest_Action_ConsultationField) {
            $logger = Atexo_LoggerManager::getLogger('consultation');
            $erreur = null;
            $consultationId = $consultation->getId();

            if ($consultationId) {
                $logger->info("La phase modification de l'objet consultation dont la reference : " . $consultationId);
            } else {
                $logger->info('La phase creation consultation');
            }

            $consultation->setIdTypeAvis(Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION'));
            $organisme = $consultation->getOrganisme();
            $serviceId = $consultation->getServiceId();

            if ('0' == $serviceId) {
                $consultation->setServiceId(null);
            }

            if (Atexo_Module::isEnabled('NumerotationRefCons') && !$consultationId) {
                $logger->info('La numerotation de la reference utilisateur est automatique');
                $UserRef = Atexo_Consultation_NumerotationRefCons::generateUserRef();
                $logger->info('La reference utilisateur aprés la numerotation automatique est ' . $UserRef);
                $consultation->setReferenceUtilisateur($UserRef);
            }

            $accesCode = $consultation->getCodeProcedure();

            if ($accesCode) {
                $logger->info('La consultation est en phase acces restreint ');
                $consultation->setTypeAcces(Atexo_Config::getParameter('TYPE_PROCEDURE_RESTREINTE'));
                $consultation->setNumProcedure(Atexo_Config::getParameter('TYPE_PROCEDURE_RESTREINTE'));
            }

            if (!Atexo_Module::isEnabled('ConsultationClause')) {
                $logger->info("Le module des clause est desactive : pas d'enregistrement des clauses");
                $consultation->setClauseSocialeConditionExecution('0');
                $consultation->setClauseSocialeInsertion('0');
                $consultation->setClauseSocialeAteliersProteges('0');
                $consultation->setClauseSocialeSiae('0');
                $consultation->setClauseSocialeEss('0');
                $consultation->setClauseSociale(Atexo_Config::getParameter('CLAUSES_SOCIALES_DESACTIVER'));
                $consultation->setClauseEnvSpecsTechniques('0');
                $consultation->setClauseEnvCondExecution('0');
                $consultation->setClauseEnvCriteresSelect('0');
                $consultation->setClauseEnvironnementale(
                    Atexo_Config::getParameter('CLAUSES_ENVIRONNEMENTALES_DESACTIVER')
                );
            }

            $codeTypeContrat = $consultation->getTypeContrat();

            if ($codeTypeContrat) {
                $logger->info('Le libelle de type de contrat est ' . $codeTypeContrat);
                $logger->info('La récupération de type de contrat dont le code de libelle ' . $codeTypeContrat);
                $typeContrat = (new Atexo_Consultation_Contrat())->getTypeContratByCodeLibelle($codeTypeContrat);

                if ($typeContrat instanceof CommonTTypeContrat) {
                    $logger->info('Le type de contrat existe');
                    $consultation->setTypeMarche($typeContrat->getIdTypeContrat());
                    $logger->info("L'id type contrat est " . $typeContrat->getIdTypeContrat());
                } else {
                    $logger->info('Le type de contrat dont le code de libelle ' . $codeTypeContrat . " n'existe pas");
                }
            }

            $consultation->setTypeProcedure('');
            $logger->info('Recuperation type de procedure ' . $consultation->getAbbreviationTypeProcedure());

            //Type de procedure
            $typeProcedure = (new Atexo_Consultation_ProcedureType())->retrieveTypeProcedureObjectByAbreviationInterface($consultation->getAbbreviationTypeProcedure(), $organisme);

            if ($typeProcedure) {
                $idTypeProcedureOrg = $typeProcedure->getIdTypeProcedure();
                $idTypeProcedurePortail = $typeProcedure->getIdTypeProcedurePortail();
                $consultation->setIdTypeProcedure($idTypeProcedurePortail);
                $consultation->setIdTypeProcedureOrg($idTypeProcedureOrg);

                //sdr MPE-129
                $logger->info('Recuperation  objet CommonProcedureEquivalence : ' . $idTypeProcedureOrg);
                $procedureEquivalence = Atexo_Consultation_ProcedureEquivalence::retrieveProcedureEquivalenceById(
                    $idTypeProcedureOrg,
                    $organisme
                );

                if ($procedureEquivalence instanceof CommonProcedureEquivalence) {
                    $logger->info('mettre le champs PartialDceDownload  ');
                    $consultation->setPartialDceDownload(substr($procedureEquivalence->getPartialDceDownload(), -1, 1));

                    if (substr($procedureEquivalence->getProcedureRestreinte(), -1, 1)) {
                        $logger->info('La procedure est restreint ');

                        if (!$accesCode) {
                            $logger->info('La génération du code restreint ');
                            $accesCode = Atexo_Util::generateCode(8);
                            $logger->info('Le code restreint genere est ' . $accesCode);
                            $consultation->setCodeProcedure($accesCode);
                            $consultation->setTypeAcces(Atexo_Config::getParameter('TYPE_PROCEDURE_RESTREINTE'));
                            $consultation->setNumProcedure(Atexo_Config::getParameter('TYPE_PROCEDURE_RESTREINTE'));
                        }
                    } elseif (!$accesCode) {
                        $logger->info('La consultation est en phase acces public ');
                        $consultation->setTypeAcces(Atexo_Config::getParameter('TYPE_PROCEDURE_PUBLICITE'));
                        $consultation->setNumProcedure(Atexo_Config::getParameter('TYPE_PROCEDURE_PUBLICITE'));
                    }

                    // l'heritage depuis type de procedure est true
                    if ($consultation->getHeritageModalitesReponseProcedure()) {
                        $logger->info("L'heritage des modalites de reponse depuis le type procedure");

                        self::loadResponseModalitiesWithParametersProceduresEquivalence(
                            $consultation,
                            $procedureEquivalence
                        );
                    }

                    //MPE-6151 (Activer le DUME si la plateforme et l'organisme le permettent)
                    if (Atexo_Module::isEnabled('InterfaceDume')) {
                        $logger->info('La plateforme a le DUME activé');
                        if (self::procedureActive($procedureEquivalence->getDumeDemande())) {
                            $logger->info(sprintf(
                                "La procédure %s pour l'organisme %s : DUME activé : %s",
                                $consultation->getAbbreviationTypeProcedure(),
                                $organisme,
                                $procedureEquivalence->getDumeDemande()
                            ));

                            $idProcedureEquivalenceDume = Atexo_Consultation_ProcedureEquivalence::getDefaultSelectedTypeProcedureDume(
                                $procedureEquivalence->getIdTypeProcedure(),
                                $organisme
                            );

                            if (0 != $idProcedureEquivalenceDume) {
                                $consultation->setDumeDemande('1');
                                $consultation->setTypeProcedureDume($idProcedureEquivalenceDume);
                                $logger->info("La consultation a le DUME d'activé");
                            }

                            if (self::procedureActive($procedureEquivalence->getTypeFormulaireDumeStandard())) {
                                $logger->info('Le type de formuliare DUME est :  DUME Standard');
                                $consultation->setTypeFormulaireDume('0');
                            } elseif (self::procedureActive($procedureEquivalence->getTypeFormulaireDumeSimplifie())) {
                                $logger->info('Le type de formuliare DUME est :  DUME Simplifié');
                                $consultation->setTypeFormulaireDume('1');
                            } else {
                                $logger->error("Le type de formuliare DUME n'est pas définit pour le type de procédure dont l'id = " . $procedureEquivalence->getIdTypeProcedure());
                                $erreur = 'Les prérequis de paramétrage du type de procédure ne sont pas respectés.';
                            }
                        }
                    }
                }
            }

            if (!$erreur) {
                $erreur = self::validerConsultationData($consultation, $logger);
            }
            if ($erreur) {
                return $erreur;
            } else {
                try {
                    $idEtatConsultation = $consultation->getConsultationStatut();
                    $consultation->setDatedebut(date('Y-m-d'));
                    if ($idEtatConsultation == Atexo_Config::getParameter('STATUS_ELABORATION')) {
                        $logger->info('Etat de la consultation =  ELABORATION');
                        $consultation->setIdEtatConsultation('0');
                        $consultation->setEtatEnAttenteValidation('0');
                        $consultation->setEtatValidation(Atexo_Config::getParameter('CONS_NON_VALIDEE'));
                        $consultation->setDateMiseEnLigneCalcule(null);
                    } elseif ($idEtatConsultation == Atexo_Config::getParameter('STATUS_PREPARATION')) {
                        $logger->info('Etat de la consultation =  PREPARATION');
                        $consultation->setIdEtatConsultation('0');
                        $consultation->setEtatEnAttenteValidation('1');
                        $consultation->setEtatValidation(Atexo_Config::getParameter('CONS_NON_VALIDEE'));
                        $consultation->setRegleMiseEnLigne(
                            Atexo_Config::getParameter('MISE_EN_LIGNE_APRES_VALIDATION_ET_DATE')
                        );
                        $consultation->setDateMiseEnLigneCalcule(null);
                    } elseif (
                        $idEtatConsultation == Atexo_Config::getParameter('STATUS_CONSULTATION') ||
                        $idEtatConsultation == Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE')
                    ) {
                        $logger->info('Etat de la consultation =  CONSULTATION ou OUVERTURE_ANALYSE');
                        $consultation->setIdEtatConsultation('0');
                        $consultation->setEtatEnAttenteValidation('1');
                        $consultation->setEtatValidation(Atexo_Config::getParameter('CONS_VALIDEE'));
                        $consultation->setDateValidation($consultation->getDateMiseEnLigneCalcule());
                    } elseif ($idEtatConsultation == Atexo_Config::getParameter('STATUS_DECISION')) {
                        $logger->info('Etat de la consultation =  DECISION');
                        $consultation->setIdEtatConsultation(Atexo_Config::getParameter('STATUS_DECISION'));
                        $consultation->setEtatEnAttenteValidation('1');
                        $consultation->setEtatValidation(Atexo_Config::getParameter('CONS_VALIDEE'));
                        $consultation->setDateValidation($consultation->getDateMiseEnLigneCalcule());
                    }

                    $logger->info("Debut de l'enregistrement de la consultation ");

                    $connexion = Propel::getConnection(
                        Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE')
                    );

                    $connexion->beginTransaction();

                    if (Atexo_Module::isEnabled('LieuxExecution')) {
                        $lieuExecutions = $consultation->getIdsLieuExecutions();

                        if (is_array($lieuExecutions) && count($lieuExecutions)) {
                            $idsLieuExecution = ',' . implode(',', $lieuExecutions) . ',';
                            $logger->info("L'enregistrement des lieux d'executions " . $idsLieuExecution);
                            $consultation->setLieuExecution($idsLieuExecution);
                        }
                    }

                    if ($consultationId) {
                        $oldConsultation = (new Atexo_Consultation())->retrieveCommonConsultation($consultationId, $organisme);

                        if ($oldConsultation instanceof CommonConsultation) {
                            $consultation->setIdDonneeComplementaire($oldConsultation->getIdDonneeComplementaire());
                        }

                        $consultation->setNew(false);
                        $logger->info('La suppression des ancienne  certificats ');
                        (new Atexo_Certificat())->deleteCommonCertificats($organisme, $consultationId, $connexion);
                        $logger->info('La suppression des ancien Lots ');
                        (new Atexo_Consultation_Lots())->deleteCommonLots($organisme, $consultationId, $connexion);
                    } else {
                        $consultation->setNew(true);
                        $consultation->setId(null);
                    }

                    //bugfix/MWA-MPE-4936
                    $logger->info("La recuperation de la denomination de l'organiseme ");
                    $denominationOrg = $consultation->getOrgDenomination();
                    if ($denominationOrg) {
                        $logger->info("La denomination de l'organiseme est " . $denominationOrg);
                    } else {
                        $denominationOrg = self::getOrganismeDenominationByService($organisme, $consultation->getServiceId());
                        $logger->info("Pour l'organiseme " . $organisme . " et l'idService = " . $consultation->getServiceId() . " La denomination de l'organiseme est '" . $denominationOrg . "'");
                        $consultation->setOrgDenomination($denominationOrg);
                    }
                    $consultation->setDenominationAdapte(strtoupper(Atexo_Util::formatterChaine($denominationOrg)));
                    $logger->info('Parametrage invite de la consultation');
                    $agentObject = (new Atexo_Agent())->retrieveAgent($consultation->getIdCreateur());

                    if ($agentObject instanceof CommonAgent) {
                        $logger->info('Agent dont id createur associe a la consultation existe : ' . $consultation->getIdCreateur());
                        $habilitationAgentObject = $agentObject->getCommonHabilitationAgent();

                        if ($habilitationAgentObject instanceof CommonHabilitationAgent) {
                            $logger->info('Recuperation des habilitations Agent ok : ' . $consultation->getIdCreateur());

                            if ('1' != $habilitationAgentObject->getInvitePermanentMonEntite()) {
                                $logger->info("L'agent n'a pas l'habilitation invite permanent de mon entité, on le rajoute en tant qu'invite avec habilitation: " . $consultation->getIdCreateur());
                                $interneConsultation = new CommonInterneConsultation();
                                $interneConsultation->setId('');
                                $interneConsultation->setInterneId($consultation->getIdCreateur());
                                $interneConsultation->setOrganisme($consultation->getOrganisme());
                                $consultation->addCommonInterneConsultation($interneConsultation);
                            } else {
                                $logger->info("L'agent a l'habilitation invite permanent de mon entité, il n'est pas rajoute en tant qu'invite : " . $consultation->getIdCreateur());
                            }
                        } else {
                            $logger->error('Impossible de recuperer les habilitations Agent  : ' . $consultation->getIdCreateur());
                        }
                    } else {
                        $logger->error("Impossible de recuperer l'Agent  : " . $consultation->getIdCreateur());
                    }

                    if ($consultation->getDceContent()) {
                        $logger->info('Debut traitement DCE');
                        $nomFichier = $consultation->getNomFichierDCE();

                        $messageErreur = (new Atexo_Consultation_Dce())->setDceToConsultation($consultation, $organisme, $logger, $nomFichier, $consultation->getDceContent());

                        if ($messageErreur) {
                            return $messageErreur;
                        }
                    }
                    if ($consultation->getRcContent()) {
                        $messageErreur = (new Atexo_Consultation_Rg())->setRgToConsultation($consultation, $organisme, $logger, $consultation->getNomFichierRc(), $consultation->getRcContent());

                        if ($messageErreur) {
                            return $messageErreur;
                        }
                    }

                    $synchroSub = $consultation->getSynchroSub();

                    if ($synchroSub instanceof Atexo_Rest_Action_SynchroSubField) {
                        $envCand = Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE');
                        $logger->info("Debut de l'enregistrement des parametres du dossier candidature avec cle externe = " . $synchroSub->getIdCleExterneDispositifCandidature());

                        $consultation->addParametrageFormulaireSub(
                            $envCand,
                            $synchroSub->getIdCleExterneDispositifCandidature()
                        );

                        $logger->info("Fin de l'enregistrement des parametre du dossier candidature");
                        $logger->info("Debut de l'enregistrement des parametres du dossier offre avec cle externe = " . $synchroSub->getIdCleExterneDispositifOffre());
                        $envOffre = Atexo_Config::getParameter('TYPE_ENV_OFFRE');

                        $consultation->addParametrageFormulaireSub(
                            $envOffre,
                            $synchroSub->getIdCleExterneDispositifOffre()
                        );

                        $logger->info("Fin de l'enregistrement des parametre du dossier offre");
                    }

                    $logger->info("L'enregistrement de tous les elements de la consultation");
                    $consultation->saveConsultation($connexion);
                    $consultationId = $consultation->getId();

                    $urlExterne = $consultation->getUrlConsultationExterne();
                    $logger->info("La recuperation de l'url externe de la consultation URL = " . $urlExterne);
                    if (!trim($urlExterne)) {
                        $logger->info("La generation de l'urlExterne   de la consultation ");
                        $urlExterne = Atexo_MpeSf::getUrlDetailsConsultation(
                            $consultationId,
                            '&orgAcronyme=' . $consultation->getOrganisme()
                        ); //Debut sauvegarde de l'url externe
                        //Debut sauvegarde de l'url externe
                        $logger->info("L'enregistrement de l'url externe de la consultation URL = " . $urlExterne);
                        $consultation->setUrlConsultationExterne($urlExterne);
                        //Fin sauvegarde de l'url externe
                    }

                    $annonceUrlsExternes = $consultation->getCollAnnonceUrlExternes();

                    if (is_array($annonceUrlsExternes) && count($annonceUrlsExternes)) {
                        $logger->info("L'enregistrement des urls externes des annonces");

                        foreach ($annonceUrlsExternes as $annonceUrlExterne) {
                            $avis = (new Atexo_Publicite_Avis())->retreiveAvisFormatLibreUrlByUrl($consultationId, $organisme, $annonceUrlExterne);

                            if (!$avis) {
                                $logger->info("L'ajout de l'url '" . $annonceUrlExterne . "'");
                                $newAvis = new CommonAVIS();
                                $newAvis->setOrganisme($organisme);
                                $newAvis->setConsultationId($consultationId);
                                $newAvis->setStatut(Atexo_Config::getParameter('DESTINATAIRE_PUBLIE'));
                                $newAvis->setDateCreation(date('Y-m-d H:i'));
                                $newAvis->setType(Atexo_Config::getParameter('TYPE_FORMAT_LIBRE_URL'));
                                $newAvis->setUrl($annonceUrlExterne);
                                $newAvis->save($connexion);
                            } else {
                                $logger->info("L'url '" . $annonceUrlExterne . "' existe déjà");
                            }
                        }

                        $logger->info("Fin de l'enregistrement des urls externes des annonces");
                    }

                    if ($consultation->getFichierPubliciteContent()) {
                        $logger->info('Debut traitement fichier Publicite');
                        $logger->info('Suspension des fichier Publicite');

                        if (
                            (new Atexo_Publicite_Avis())->suspendreAvisByConsultation($consultationId, $organisme, Atexo_Config::getParameter('TYPE_FORMAT_LIBRE_FICHIER_JOINT'), $connexion)
                        ) {
                            $fileName = $consultation->getNomFichierPublicite();
                            $atexoBlob = new Atexo_Blob();
                            $atexoCrypto = new Atexo_Crypto();
                            $path = $consultation->getFichierPubliciteContent();
                            $arrayTimeStampRg = $atexoCrypto->timeStampFile($path);

                            if (is_array($arrayTimeStampRg)) {
                                $avisIdBlob = $atexoBlob->insert_blob($fileName, $path, $organisme);

                                if ('0' != $avisIdBlob) {
                                    $avisPub = new CommonAVIS();
                                    $dateAvis = date('Y-m-d H:i');
                                    $avisPub->setOrganisme($organisme);
                                    $avisPub->setConsultationId($consultationId);
                                    $avisPub->setStatut(Atexo_Config::getParameter('DESTINATAIRE_PUBLIE'));
                                    $avisPub->setDateCreation($dateAvis);
                                    $avisPub->setDatePub($dateAvis);
                                    $avisPub->setAvis($avisIdBlob);
                                    $avisPub->setNomFichier(Atexo_Util::atexoHtmlEntities($fileName));
                                    $avisPub->setType(Atexo_Config::getParameter('TYPE_FORMAT_LIBRE_FICHIER_JOINT'));
                                    $avisPub->setUrl('');
                                    $avisPub->setHorodatage($arrayTimeStampRg['horodatage']);
                                    $avisPub->save($connexion);
                                    $logger->info("L'enregistrement du fichier publicite");
                                } else {
                                    return "Probleme d'enregistrement du fichier publicite de la consultation";
                                }
                            } else {
                                return "Probleme d'horodatage du fichier publicite de la consultation";
                            }
                        } else {
                            return 'Probleme suspension des fichiers publicite de la consultation';
                        }
                    }

                    //Debut sauvegarde des lots
                    $alloti = self::enregistrerLots($consultation, $connexion, $logger);
                    //Debut gestion des certificats de chiffrement
                    $logger->info('Debut gestion des certificats de chiffrement');

                    if ('true' == $consultation->getChiffrementOffre()) {
                        $listeAllCertificatsChiffrement = [];

                        if ('false' === $consultation->getModeOuvertureReponse()) {
                            self::insererCertificatsParEnveloppe(
                                $consultation,
                                $connexion,
                                $logger,
                                $listeAllCertificatsChiffrement
                            );
                        } else {
                            self::insererCertificatsParReponse(
                                $consultation,
                                $connexion,
                                $logger,
                                $listeAllCertificatsChiffrement
                            );
                        }
                        self::SaveCertificate($listeAllCertificatsChiffrement, $connexion, $logger);
                    }

                    $logger->info('Fin gestion des certificats de chiffrement');
                    //Fin gestion des certificats de chiffrement
                    $consultation->setAlloti($alloti);

                    if (Atexo_Module::isEnabled('DonneesRedac', $organisme)) {
                        //La gestion de la justification de non allotissement
                        $logger->info('Debut de la gestion de la justification de non allotissement');
                        $justificationNonnAllotissement = $consultation->getJustificationNonAllotissement();
                        $logger->info('La valeur du JustificationNonAllotissement = ' . $justificationNonnAllotissement);

                        if (!$alloti) {
                            $logger->info('Le cas non alloti');
                            $donneeComplementaire = $consultation->getDonneComplementaire();

                            if (!$donneeComplementaire instanceof CommonTDonneeComplementaire) {
                                $donneeComplementaire = new CommonTDonneeComplementaire();
                            }

                            $logger->info("L'enregistrement de la valeur JustificationNonAllotissement");
                            $donneeComplementaire->setJustificationNonAlloti($justificationNonnAllotissement);
                            $donneeComplementaire->save($connexion);
                            $consultation->setIdDonneeComplementaire(
                                $donneeComplementaire->getIdDonneeComplementaire()
                            );
                        } else {
                            $logger->info('Le cas alloti');
                            $donneeComplementaire = $consultation->getDonneComplementaire();

                            if ($donneeComplementaire instanceof CommonTDonneeComplementaire) {
                                $logger->info('Vider la valeur JustificationNonAllotissement');
                                $donneeComplementaire->setJustificationNonAlloti('');
                                $donneeComplementaire->save($connexion);
                            }
                        }
                        $logger->info('Fin de la gestion de la justification de non allotissement');
                    }

                    /**
                     * Gestion de la MESSEC V2
                     */
                    $consultation->setVersionMessagerie('1');
                    if (Atexo_Module::isEnabled('messagerieV2')) {
                        $consultation->setVersionMessagerie('2');
                    }

                    $consultation->save($connexion);
                    $connexion->commit();
                    $logger->info("Fin de l'enregistrement de la consultation l'id = " . $consultationId);

                    BaseCommonConsultationPeer::removeInstanceFromPool($consultation);
                    $consultation = (new Atexo_Consultation())->retrieveConsultation($consultationId, $organisme);

                    if ($consultation instanceof CommonConsultation) {
                        self::createOrUpdateAchatsResponsables($consultation);

                        BaseCommonCategorieLotPeer::clearInstancePool(true);
                        $lots = $this->getAllLots(null, $consultation->getId(), $consultation->getOrganisme());

                        foreach ($lots as $lot) {
                            self::createOrUpdateAchatsResponsables($lot);
                        }

                        if ($oldServiceId) {
                            $consultation->setServiceId($oldServiceId);
                        }

                        if (Atexo_Config::getParameter('SPECIFIQUE_PLACE')) {
                            // Ajout en favoris
                            (new FormulaireConsultation())->addConsultationFavorisByWebService(
                                $consultation->getIdCreateur(),
                                $consultation->getId(),
                                $connexion
                            );
                        }

                        return new Atexo_Rest_Action_ConsultationField($consultation);
                    } else {
                        $logger->error("Probleme lors de l'enregistrement de la conultation dans la base de donnée");

                        return "Probleme lors de l'enregistrement de la conultation dans la base de donnée";
                    }
                } catch (\Exception $e) {
                    $connexion->rollBack();
                    $logger->error("Probleme lors de l'enregistrement de la conultation= " . $e->getMessage() . $e->getTraceAsString());

                    return "Probleme lors de l'enregistrement ";
                }
            }
        } else {
            return "input n'est pas une instance de Atexo_Rest_Action_ConsultationField";
        }
    }

    /**
     * Regarde si la procédure est cochée ou non.
     *
     * @param $value
     *
     * @return bool
     */
    public function procedureActive($value)
    {
        return (!strcmp($value, '1') || !strcmp($value, '+1') || !strcmp($value, '-1')) ? true : false;
    }

    /**
     * Permet de valider les donnees de la consultation;.
     *
     * @param Atexo_Rest_Action_ConsultationField $consultation
     *
     * @return string message  dans le cas d'erreur
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function validerConsultationData($consultation, &$logger)
    {
        $logger->info('Debut de la validation de la consultation ');
        $erreur = self::validerData($consultation);
        if ($erreur) {
            return $erreur;
        }

        if (Atexo_Module::isEnabled('LieuxExecution')) {
            $logger->info("La validation des lieux d'executions ");
            $lieuExecutions = $consultation->getIdsLieuExecutions();
            if (!is_array($lieuExecutions) || !count($lieuExecutions)) {
                return "Les lieux d'exécutions sont obligatoires";
            }
        }
        if ($consultation->getCodeProcedure() && strlen($consultation->getCodeProcedure()) > 9) {
            return "La longueur du code d'acces " . $consultation->getCodeProcedure() . ' doit etre inferieur ou egale a 9 caracteres';
        }
        $lots = $consultation->getCommonCategorieLots();
        if (is_array($lots) && count($lots)) {
            $logger->info('Debut de la validation des lots ');
            $messageErreur = '';
            foreach ($lots as $lot) {
                if (!$lot->getLot()) {
                    return 'Le numero du lot est obligatoire';
                }
                if (!is_numeric($lot->getLot())) {
                    $messageErreur .= ' Lot ' . $lot->getLot() . ' : ' . Prado::localize('TEXT_LOT_ALPHANUMERIQUE');
                }
                if (!$lot->getDescription()) {
                    $messageErreur .= ' Lot ' . $lot->getLot() . ' : La description est obligatoire';
                }
            }
            $logger->info('Fin de la validation des lots ');
            if ($messageErreur) {
                return $messageErreur;
            }
        }

        if (!Atexo_Module::isEnabled('MarchePublicSimplifie', $consultation->getOrganisme()) && 'true' == strtolower($consultation->getMarchePublicSimplifie())) {
            return 'Le module marché public simplifie est désactivé la valeur de marchePublicSimplifie doit être à false';
        }
        $idEtatConsultation = $consultation->getConsultationStatut();
        if (
            $idEtatConsultation == Atexo_Config::getParameter('STATUS_PREPARATION') || $idEtatConsultation == Atexo_Config::getParameter('STATUS_CONSULTATION')
            || $idEtatConsultation == Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE')
        ) {
            //Les validations commun pour les statuts : STATUS_PREPARATION, STATUS_CONSULTATION et STATUS_OUVERTURE_ANALYSE
            $logger->info('La verfication des enveloppes ');
            $logger->info('Enveloppe candidature =  ' . $consultation->getEnvCandidature());
            $logger->info('Enveloppe offre =  ' . $consultation->getEnvOffre());
            $logger->info('Enveloppe anonymat =  ' . $consultation->getEnvAnonymat());
            $logger->info('Enveloppe offre technique =  ' . $consultation->getEnvOffreTechnique());
            if (
                (!$consultation->getEnvCandidature() || 'false' == strtolower($consultation->getEnvCandidature()))
                && (!$consultation->getEnvOffre() || 'false' == strtolower($consultation->getEnvOffre()))
                && (!$consultation->getEnvAnonymat() || 'false' == strtolower($consultation->getEnvAnonymat()))
                && (!$consultation->getEnvOffreTechnique() || 'false' == strtolower($consultation->getEnvOffreTechnique()))
            ) {
                return "aucune enveloppe n'est sélectionnée";
            }

            $logger->info("La verfication de l'autorisation de la reponse electronique ");
            if (
                (!$consultation->getAutoriserReponseElectronique() || 'false' == strtolower($consultation->getAutoriserReponseElectronique()))
                && (!$consultation->getReponseObligatoire() || 'false' == strtolower($consultation->getReponseObligatoire()))
                && ('0' != $consultation->getSignatureOffre() || 'true' == $consultation->getChiffrementOffre())
            ) {
                return 'Reponse electronique Refusee avec chiffrement  ou signature oui ';
            }
            $logger->info('La validation de la date limite de remise plis = ' . $consultation->getDatefin());
            if (!$consultation->getDatefin() || strstr($consultation->getDatefin(), '0000-00-00')) {
                return 'Date Limite Remise Plis obligatoire ';
            }
            //Les validations commun pour les statuts : STATUS_CONSULTATION et STATUS_OUVERTURE_ANALYSE
            if ($idEtatConsultation == Atexo_Config::getParameter('STATUS_CONSULTATION') || $idEtatConsultation == Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE')) {
                $logger->info('La verfication de la date mise en ligne calcule ' . $consultation->getDateMiseEnLigneCalcule() .
                    ' dans le cas statut consultation est etat consultation ou ouverture et analyse ');
                if (!$consultation->getDateMiseEnLigneCalcule() || strstr($consultation->getDateMiseEnLigneCalcule(), '0000-00-00')) {
                    return 'Date mise en ligne est obligatoire';
                }

                //Validation certificats de chiffrement
                $erreur = self::validerCertificatsChiffrement($consultation, $logger);
                if ($erreur) {
                    return $erreur;
                }
                //Les validations pour le statut : STATUS_CONSULTATION
                if ($idEtatConsultation == Atexo_Config::getParameter('STATUS_CONSULTATION')) {
                    $logger->info("La verfication de la date mise en ligne calcule et la date fin d'une telle façon que dateMiseEnLigneCalcule <= date Now <= dateFin ");
                    if ($consultation->getDateMiseEnLigneCalcule() > date('Y-m-d H:i:s')) {
                        return "La date de mise en ligne doit être inférieur ou égale à la date actuelle. Le statut 'consultation' n'est pas correct.";
                    }
                    if ($consultation->getDatefin() < date('Y-m-d H:i:s')) {
                        return "La date limite de remise des plis doit être supérieur ou égale la date actuelle. Le statut 'consultation' n'est pas correct.";
                    }
                } else {
                    //Les validations pour le statut :  STATUS_OUVERTURE_ANALYSE
                    $logger->info("La verfication de la date fin d'une telle façon que date Now > dateFin ");
                    if ($consultation->getDatefin() > date('Y-m-d H:i:s')) {
                        return 'la date limite de remise des plis doit être inférieur la date actuelle';
                    }
                }
            }
        }

        $logger->info(' Fin de la validation de la consultation ');
    }

    /**
     * Permet de valider les données de l'annonce;.
     *
     * @param Atexo_Rest_Action_ConsultationField $consultation
     *
     * @return string message  dans le cas d'erreur
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function validerData($consultation)
    {
        if (!$consultation->getOrganisme()) {
            return "L'organisme est obligatoire";
        } else {
            $organisme = Atexo_Organismes::retrieveOrganismeByAcronyme($consultation->getOrganisme());
            if (!$organisme) {
                return "L'organisme " . $consultation->getOrganisme() . " n'existe pas ";
            }
        }

        $id = $consultation->getId();
        if ($id) {
            $org = $consultation->getOrganisme();
            $consultationExiste = (new Atexo_Consultation())->retrieveConsultation($id, $org);
            if (!$consultationExiste) {
                return "La consultation dont l'id = " . $id . "  n'existe pas";
            }
        }
        $referenceUtilisateur = $consultation->getReferenceUtilisateur();
        $res = preg_match('([0-9a-zA-Z_.-' . (Atexo_Module::isEnabled('AutoriserCaracteresSpeciauxDansReference') ? '/' : '') . ']{5,32})', $referenceUtilisateur);
        if (!$res) {
            return 'Format de la reference utilisateur ' . $referenceUtilisateur . ' incorrect';
        }

        if (!$consultation->getObjet()) {
            return "L'objet est obligatoire";
        }

        if (!$consultation->getIdTypeProcedure()) {
            return 'Le type procedure ' . $consultation->getAbbreviationTypeProcedure() . " n'est pas valide";
        }
    }

    /**
     * Permet de valider les certificats de chiffrement (validation de la chaine de certificat, regles de validation).
     *
     * @param CommonConsultation $consultation : objet consultation
     * @param Logger             $logger       : Logger
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function validerCertificatsChiffrement($consultation, &$logger)
    {
        if ($consultation->getChiffrementOffre() && 'true' == strtolower($consultation->getChiffrementOffre())) {
            $logger->info('La verfication des certificats dans le cas de chiffrement ');
            $certificats = $consultation->getCollCertificats();
            if ((!is_array($certificats) || !count($certificats))) {
                return 'Certificat obligatoire ';
            }
            $certificats = self::validateCertificate($certificats, $logger);
            if (!is_array($certificats)) {
                if (str_contains($certificats, 'CERTIFICAT_INVALIDE')) {
                    return str_replace('CERTIFICAT_INVALIDE', 'Certificat invalide : ', $certificats);

                    return 'Certificat invalide';
                }
                if (str_contains($certificats, 'ECHEC_VERIFICATION')) {
                    $logger->info('La verfication des certifcats a echoue : write_file');

                    return 'La verfication des certifcats a echoue ';
                }
            } else {
                if ('false' === $consultation->getModeOuvertureReponse()) {//Mode d'ouverture par enveloppe
                    if (0 !== $consultation->getEnvOffre() && !$certificats[Atexo_Config::getParameter('TYPE_ENV_OFFRE')]) {
                        return "La consultation autorise l'enveloppe d'offre. Le certificat de type 'OFFRE' est obligatoire.";
                    }
                    if (0 !== $consultation->getEnvCandidature() && !$certificats[Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE')]) {
                        return "La consultation autorise l'enveloppe de candidature. Le certificat de type 'CANDIDATURE' est obligatoire.";
                    }
                    if (0 !== $consultation->getEnvOffreTechnique() && !$certificats[Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE')]) {
                        return "La consultation autorise l'enveloppe d'offres techniques. Le certificat de type 'OFFRE_TECHNIQUE' est obligatoire.";
                    }
                    if (0 !== $consultation->getEnvAnonymat() && !$certificats[Atexo_Config::getParameter('TYPE_ENV_ANONYMAT')]) {
                        return "La consultation autorise l'enveloppe d'anonymat. Le certificat de type 'ANONYMAT' est obligatoire.";
                    }
                } else {//Mode d'ouverture par réponse
                    if (!$certificats[Atexo_Config::getParameter('TYPE_CERTIFICAT_REPONSE')]) {
                        return "Le mode d'ouverture de la consultation est par 'REPONSE'. Le certificat de type 'REPONSE' est obligatoire.";
                    }
                }
            }
        }

        return false;
    }

    /**
     * Permet de valider les certificats envoyés par le flux
     * Construit également un tableau de la liste des certificats pour la validation de la consultation.
     *
     * @param array  $listeCertificats : liste des certificats
     * @param Logger $logger
     *
     * @return array $arrayCertificateForValidation: liste des certificats si la validation se passe bien, string sinon
     *
     * @author OKO <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function validateCertificate(array $listeCertificats, &$logger)
    {
        $arrayCertificateForValidation = [];
        if (count($listeCertificats)) {
            $logger->info('La verfication des certificats');
            foreach ($listeCertificats as $certificat) {
                if ($certificat instanceof Atexo_Rest_Action_CertificatChiffrementField) {
                    $infileCert = Atexo_Config::getParameter('COMMON_TMP') . 'certificat' . session_id() . time();
                    $certificatContenu = $certificat->getCertificat();
                    if (Atexo_Util::write_file($infileCert, $certificatContenu)) {
                        $isValid = (new Atexo_Crypto_Certificat())->checkCertificatFileValidity($infileCert, null);
                        if (!$isValid) {
                            unlink($infileCert);

                            return 'CERTIFICAT_INVALIDE' . base64_encode($certificatContenu);
                        }
                        unlink($infileCert);
                    } else {
                        return 'ECHEC_VERIFICATION';
                    }
                    $arrayCertificateForValidation[$certificat->getTypeEnv()] = $certificat;
                }
            }
        }

        return $arrayCertificateForValidation;
    }

    public function retrieveCommonConsultation($consultationId, $organisme)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));

        $c = new Criteria();
        $c->add(CommonConsultationPeer::ID, $consultationId);
        $c->add(CommonConsultationPeer::ORGANISME, $organisme);
        $commonConsultation = CommonConsultationPeer::doSelectOne($c, $connexion);

        return $commonConsultation;
    }

    /**
     * Permet de retourner la denomination de l'organisme.
     *
     * @param string $organisme l'acronyme de l'organisme
     * @param int    $idService l'id de service
     * @param $sigle
     *
     * @return string la denomination de l'organisme
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getOrganismeDenominationByService($organisme, $idService, $sigle = false)
    {
        $entitePublique = '';
        $infoCpVille = '';

        if (Atexo_Module::isEnabled('AffichageNomServicePere', $organisme)) {
            if ($idService) {
                $servicesParents = Atexo_EntityPurchase::getArParentsId($idService, $organisme, true);
                $idServiceParent = $servicesParents[(count($servicesParents) - 2)];
                $serviceParent = (new Atexo_EntityPurchase())->getEntityById($idServiceParent, $organisme);
                if ($serviceParent instanceof CommonService) {
                    if ($sigle) {
                        $entitePublique = $serviceParent->getPathServiceTraduit();
                    } else {
                        $entitePublique = $serviceParent->getLibelleTraduit();
                    }

                    if (!empty($serviceParent->getCp())) {
                        $infoCpVille = $serviceParent->getCp() ;
                    }
                    if (!empty($serviceParent->getVille())) {
                        $infoCpVille .= $infoCpVille ? '-' : '';
                        $infoCpVille .= $serviceParent->getVille();
                    }
                }
            } else {
                $organismeObjet = Atexo_Organismes::retrieveOrganismeByAcronyme($organisme);

                if ($organismeObjet instanceof CommonOrganisme) {
                    if ($sigle) {
                        $entitePublique = $organismeObjet->getSigle() . ' - ' . $organismeObjet->getDenominationOrg();
                    } else {
                        $entitePublique = $organismeObjet->getDenominationOrg();
                    }
                    if (!empty($organismeObjet->getCp())) {
                        $infoCpVille = $organismeObjet->getCp() ;
                    }
                    if (!empty($organismeObjet->getVille())) {
                        $infoCpVille .= $infoCpVille ? '-' : '';
                        $infoCpVille .= $organismeObjet->getVille();
                    }
                }
            }
        } else {
            $organismeObjet = Atexo_Organismes::retrieveOrganismeByAcronyme($organisme);
            //cette variable $entitePublique sert à remplir le champ org_denomination dans ANM
            //par défaut on prend les informations du pouvoir adjudicateur à savoir l'organisme
            if ($organismeObjet) {
                if ($sigle) {
                    $entitePublique = $organismeObjet->getSigle() . ' - ' . $organismeObjet->getDenominationOrg();
                } else {
                    $entitePublique = $organismeObjet->getDenominationOrg();
                }
            }

            if (!Atexo_Module::isEnabled('OrganisationCentralisee', $organisme)) {
                $service = (new Atexo_EntityPurchase())->getEntityById($idService, $organisme);

                if ($service && $organismeObjet) {
                    if ($sigle) {
                        $entitePublique = Atexo_EntityPurchase::getPathEntityById($idService, $organisme);
                    } else {
                        $entitePublique .= ' - ' . (new Atexo_EntityPurchase())->getEntityLibelle($idService, $organisme);
                    }
                    if (!empty($service->getCp())) {
                        $infoCpVille = $service->getCp() ;
                    }
                    if (!empty($service->getVille())) {
                        $infoCpVille .= $infoCpVille ? '-' : '';
                        $infoCpVille .= $service->getVille();
                    }
                    if (empty($infoCpVille)) {
                        if (!empty($organismeObjet->getCp())) {
                            $infoCpVille = $organismeObjet->getCp() ;
                        }
                        if (!empty($organismeObjet->getVille())) {
                            $infoCpVille .= $infoCpVille ? '-' : '';
                            $infoCpVille .= $organismeObjet->getVille();
                        }
                    }
                }
            } else {
                if (!empty($organismeObjet->getCp())) {
                    $infoCpVille = $organismeObjet->getCp() ;
                }
                if (!empty($organismeObjet->getVille())) {
                    $infoCpVille .= $infoCpVille ? '-' : '';
                    $infoCpVille .= $organismeObjet->getVille();
                }
            }
        }
        $entitePublique .=  $infoCpVille ? ' (' . $infoCpVille . ')' : '';

        return $entitePublique;
    }

    /**
     * Permet d'enregistrer les lots de la consultation.
     *
     * @param CommonConsultation $consultation : objet consultation
     * @param PropelPDO          $connexion    : objet connexion
     * @param Logger             $logger       : objet logger
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function enregistrerLots($consultation, $connexion, $logger)
    {
        $alloti = 0;
        if ($consultation instanceof CommonConsultation) {
            $lots = $consultation->getCommonCategorieLots();
            if (is_array($lots) && count($lots)) {
                $alloti = 1;
                $logger->info('Enregistrement des lots');
                foreach ($lots as $lot) {
                    if ($lot instanceof Atexo_Rest_Action_LotField) {
                        $lot->setOrganisme($consultation->getOrganisme());
                        $lot->setConsultationId($consultation->getId());
                        $lot->save($connexion);
                    }
                }
            }
        }

        return $alloti;
    }

    /**
     * Permet d'enregistrer les certificats pour les enveloppes lorsque le mode d'ouverture de la consultation est par "enveloppe".
     *
     * @param CommonConsultation $consultation : objet consultation
     * @param PropelPDO          $connexion    : objet connexion
     * @param Logger             $logger       : objet logger
     * @param $alloti : parametre envoyé et qui précise si la consultation est allotie
     *
     * @author OKO <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function insererCertificatsParEnveloppe($consultation, $connexion, &$logger, &$listeAllCertificatsChiffrement)
    {
        if ($consultation instanceof CommonConsultation) {
            $lots = $consultation->getCommonCategorieLots();
            $certificats = $consultation->getCollCertificats();
            $consultationId = $consultation->getId();
            $organisme = $consultation->getOrganisme();
            $index = 1;
            foreach ($certificats as $certificat) {
                if ($certificat instanceof Atexo_Rest_Action_CertificatChiffrementField) {
                    self::insererCertificatEnveloppeCandidature($listeAllCertificatsChiffrement, $consultation, $certificat, $organisme, $consultationId, $index);
                    if (is_array($lots) && count($lots)) {
                        $logger->info('Enregistrement des certificats pour les lots');
                        foreach ($lots as $lot) {
                            if ($lot instanceof Atexo_Rest_Action_LotField) {
                                self::insererCertificatEnveloppeOffre($listeAllCertificatsChiffrement, $consultation, $certificat, $organisme, $consultationId, $index, $lot->getLot());
                                self::insererCertificatEnveloppeOffreTechnique($listeAllCertificatsChiffrement, $consultation, $certificat, $organisme, $consultationId, $index, $lot->getLot());
                                self::insererCertificatEnveloppeAnonymat($listeAllCertificatsChiffrement, $consultation, $certificat, $organisme, $consultationId, $index, $lot->getLot());
                            }
                        }
                    } else {
                        self::insererCertificatEnveloppeOffre($listeAllCertificatsChiffrement, $consultation, $certificat, $organisme, $consultationId, $index);
                        self::insererCertificatEnveloppeOffreTechnique($listeAllCertificatsChiffrement, $consultation, $certificat, $organisme, $consultationId, $index);
                        self::insererCertificatEnveloppeAnonymat($listeAllCertificatsChiffrement, $consultation, $certificat, $organisme, $consultationId, $index);
                    }
                }
            }
        }
    }

    /**
     * Permet d'enregistrer le certificat pour le type d'enveloppe candidature.
     *
     * @param array              $listeAllCertificatsChiffrement : liste des certificats de chiffrements pour la consultation
     * @param CommonConsultation $consultation                   : objet consultation
     * @param string             $certificat                     : le certificat
     * @param string             $organisme                      : organisme
     * @param string             $consultationId                 : reference de la consultation
     * @param string             $index                          : index du certificat
     * @param string             $lot                            : numéro de lot
     *
     * @author OKO <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function insererCertificatEnveloppeCandidature(&$listeAllCertificatsChiffrement, $consultation, $certificat, $organisme, $consultationId, &$index, $lot = '0')
    {
        if ('CANDIDATURE' == $certificat->getType()) {
            $listeCertificatsChiffrementOneEnveloppe = self::insererCertificatUneEnveloppe(
                $organisme,
                $consultationId,
                Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE'),
                $certificat,
                $lot,
                $index
            );
            $listeAllCertificatsChiffrement = array_merge($listeAllCertificatsChiffrement, $listeCertificatsChiffrementOneEnveloppe);
            ++$index;
        }
    }

    /**
     * Permet de retourner la liste des certificats pour un type d'enveloppe donnés.
     *
     * @param string $organisme , integer $consultationId, integer $typeEnv,array of Atexo_Rest_Action_CertificatChiffrementField $certificats ,integer $lot
     *
     * @return array of Atexo_Rest_Action_CertificatChiffrementField $listeCertificatsChiffrementEnveloppes
     *
     * @author OKO <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function insererCertificatUneEnveloppe($organisme, $consultationId, $typeEnv, $certificat, $lot = '0', $index = '0')
    {
        $listeCertificatsChiffrement = [];
        if ($certificat instanceof Atexo_Rest_Action_CertificatChiffrementField) {
            $newCertificat = new CommonCertificatChiffrement();
            $certificat->copyInto($newCertificat);
            $newCertificat->setOrganisme($organisme);
            $newCertificat->setConsultationId($consultationId);
            $newCertificat->setTypeEnv($typeEnv);
            $newCertificat->setSousPli($lot);
            $newCertificat->setIndexCertificat($index);
            $listeCertificatsChiffrement[] = $newCertificat;
        }

        return $listeCertificatsChiffrement;
    }

    /**
     * Permet d'enregistrer le certificat pour le type d'enveloppe d'offre.
     *
     * @param array              $listeAllCertificatsChiffrement : liste des certificats de chiffrements pour la consultation
     * @param CommonConsultation $consultation                   : objet consultation
     * @param string             $certificat                     : le certificat
     * @param string             $organisme                      : organisme
     * @param string             $consultationId                 : reference de la consultation
     * @param string             $index                          : index du certificat
     * @param string             $lot                            : numéro de lot
     *
     * @author OKO <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function insererCertificatEnveloppeOffre(&$listeAllCertificatsChiffrement, $consultation, $certificat, $organisme, $consultationId, &$index, $lot = '0')
    {
        if ('OFFRE' == $certificat->getType()) {
            $listeCertificatsChiffrementOneEnveloppe = self::insererCertificatUneEnveloppe(
                $organisme,
                $consultationId,
                Atexo_Config::getParameter('TYPE_ENV_OFFRE'),
                $certificat,
                $lot,
                $index
            );
            $listeAllCertificatsChiffrement = array_merge($listeAllCertificatsChiffrement, $listeCertificatsChiffrementOneEnveloppe);
            ++$index;
        }
    }

    /**
     * Permet d'enregistrer le certificat pour le type d'enveloppe d'offre technique.
     *
     * @param array              $listeAllCertificatsChiffrement : liste des certificats de chiffrements pour la consultation
     * @param CommonConsultation $consultation                   : objet consultation
     * @param string             $certificat                     : le certificat
     * @param string             $organisme                      : organisme
     * @param string             $consultationId                 : reference de la consultation
     * @param string             $index                          : index du certificat
     * @param string             $lot                            : numéro de lot
     *
     * @author OKO <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function insererCertificatEnveloppeOffreTechnique(&$listeAllCertificatsChiffrement, $consultation, $certificat, $organisme, $consultationId, &$index, $lot = '0')
    {
        if ('OFFRE_TECHNIQUE' == $certificat->getType()) {
            $listeCertificatsChiffrementOneEnveloppe = self::insererCertificatUneEnveloppe(
                $organisme,
                $consultationId,
                Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE'),
                $certificat,
                $lot,
                $index
            );
            $listeAllCertificatsChiffrement = array_merge($listeAllCertificatsChiffrement, $listeCertificatsChiffrementOneEnveloppe);
            ++$index;
        }
    }

    /**
     * Permet d'enregistrer le certificat pour le type d'enveloppe anonymat.
     *
     * @param array              $listeAllCertificatsChiffrement : liste des certificats de chiffrements pour la consultation
     * @param CommonConsultation $consultation                   : objet consultation
     * @param string             $certificat                     : le certificat
     * @param string             $organisme                      : organisme
     * @param string             $consultationId                 : reference de la consultation
     * @param string             $index                          : index du certificat
     * @param string             $lot                            : numéro de lot
     *
     * @author OKO <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function insererCertificatEnveloppeAnonymat(&$listeAllCertificatsChiffrement, $consultation, $certificat, $organisme, $consultationId, &$index, $lot = '0')
    {
        if ('ANONYMAT' == $certificat->getType()) {
            $listeCertificatsChiffrementOneEnveloppe = self::insererCertificatUneEnveloppe(
                $organisme,
                $consultationId,
                Atexo_Config::getParameter('TYPE_ENV_ANONYMAT'),
                $certificat,
                $lot,
                $index
            );
            $listeAllCertificatsChiffrement = array_merge($listeAllCertificatsChiffrement, $listeCertificatsChiffrementOneEnveloppe);
            ++$index;
        }
    }

    /**
     * Permet d'enregistrer les certificats pour toutes les enveloppes lorsque le mode d'ouverture de la consultation est par "réponse".
     *
     * @param CommonConsultation $consultation : objet consultation
     * @param PropelPDO          $connexion    : objet connexion
     * @param Logger             $logger       : objet logger
     * @param $alloti : parametre envoyé et qui précise si la consultation est allotie
     *
     * @author OKO <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function insererCertificatsParReponse($consultation, $connexion, &$logger, &$listeAllCertificatsChiffrement)
    {
        if ($consultation instanceof CommonConsultation) {
            $lots = $consultation->getCommonCategorieLots();
            $certificats = $consultation->getCollCertificats();
            $consultationId = $consultation->getId();
            $organisme = $consultation->getOrganisme();
            $index = 1;
            foreach ($certificats as $certificat) {
                if ($certificat instanceof Atexo_Rest_Action_CertificatChiffrementField) {
                    self::insererCertificatReponse($listeAllCertificatsChiffrement, $consultation, $certificat, $organisme, $consultationId, $index, Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE'));
                    if (is_array($lots) && count($lots)) {
                        $logger->info('Enregistrement des certificats pour les lots');
                        foreach ($lots as $lot) {
                            if ($lot instanceof Atexo_Rest_Action_LotField) {
                                self::insererCertificatReponse(
                                    $listeAllCertificatsChiffrement,
                                    $consultation,
                                    $certificat,
                                    $organisme,
                                    $consultationId,
                                    $index,
                                    Atexo_Config::getParameter('TYPE_ENV_OFFRE'),
                                    $lot->getLot()
                                );
                                self::insererCertificatReponse(
                                    $listeAllCertificatsChiffrement,
                                    $consultation,
                                    $certificat,
                                    $organisme,
                                    $consultationId,
                                    $index,
                                    Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE'),
                                    $lot->getLot()
                                );
                                self::insererCertificatReponse(
                                    $listeAllCertificatsChiffrement,
                                    $consultation,
                                    $certificat,
                                    $organisme,
                                    $consultationId,
                                    $index,
                                    Atexo_Config::getParameter('TYPE_ENV_ANONYMAT'),
                                    $lot->getLot()
                                );
                            }
                        }
                    } else {
                        self::insererCertificatReponse($listeAllCertificatsChiffrement, $consultation, $certificat, $organisme, $consultationId, $index, Atexo_Config::getParameter('TYPE_ENV_OFFRE'));
                        self::insererCertificatReponse($listeAllCertificatsChiffrement, $consultation, $certificat, $organisme, $consultationId, $index, Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE'));
                        self::insererCertificatReponse($listeAllCertificatsChiffrement, $consultation, $certificat, $organisme, $consultationId, $index, Atexo_Config::getParameter('TYPE_ENV_ANONYMAT'));
                    }
                }
            }
        }
    }

    /**
     * Permet d'enregistrer le certificat pour un type d'enveloppe donné et pour le mode d'ouverture par "Réponse".
     *
     * @param array              $listeAllCertificatsChiffrement : liste des certificats de chiffrements pour la consultation
     * @param CommonConsultation $consultation                   : objet consultation
     * @param string             $certificat                     : le certificat
     * @param string             $organisme                      : organisme
     * @param string             $consultationId                 : reference de la consultation
     * @param unknown_type       $index
     * @param string             $index                          : index du certificat
     * @param string             $lot                            : numéro de lot
     *
     * @author OKO <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function insererCertificatReponse(&$listeAllCertificatsChiffrement, $consultation, $certificat, $organisme, $consultationId, &$index, $typeEnv, $lot = '0')
    {
        if ('REPONSE' == $certificat->getType()) {
            $listeCertificatsChiffrementReponse = self::insererCertificatUneEnveloppe($organisme, $consultationId, $typeEnv, $certificat, $lot, $index);
            $listeAllCertificatsChiffrement = array_merge($listeAllCertificatsChiffrement, $listeCertificatsChiffrementReponse);
            ++$index;
        }
    }

    /**
     * Permet d'enregistrer les certificats.
     *
     * @param array     $listeAllCertificatsChiffrement : liste des certificats de chiffrement
     * @param PropelPDO $connexion                      : objet connexion
     * @param Logger    $logger                         : objet logger
     */
    public function SaveCertificate($listeAllCertificatsChiffrement, $connexion, &$logger)
    {
        if (is_array($listeAllCertificatsChiffrement)) {
            $logger->info('Enregistrement des certificats');
            foreach ($listeAllCertificatsChiffrement as $certificat) {
                if ($certificat instanceof CommonCertificatChiffrement) {
                    $certificat->save($connexion);
                }
            }
        }
    }

    /**
     * Permet de creer une annonce.
     *
     * @param Atexo_Rest_Action_ConsultationField $consultation , string $typeAvis  le type d'avis
     *
     * @return mixed Atexo_Rest_Action_ConsultationFieldla annonce si c'est creee sinon un message d'erreur
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function createAnnonce($consultation, $oldServiceId = null)
    {
        $connexion = null;
        if ($consultation instanceof Atexo_Rest_Action_ConsultationField) {
            $logger = Atexo_LoggerManager::getLogger('consultation');
            $organisme = $consultation->getOrganisme();
            $serviceId = $consultation->getServiceId();
            if ('0' == $serviceId) {
                $consultation->setServiceId(null);
            }
            $consultationId = $consultation->getId();
            if ($consultationId) {
                $logger->info("La phase modification de l'objet annonce dont la reference : " . $consultationId);
            } else {
                $logger->info('La phase creation annonce ');
            }
            if (Atexo_Module::isEnabled('NumerotationRefCons') && !$consultationId) {
                $logger->info('La numerotation de la reference utilisateur est automatique');
                $UserRef = Atexo_Consultation_NumerotationRefCons::generateUserRef();
                $consultation->setReferenceUtilisateur($UserRef);
            }
            $logger->info('Reccuperation type de procedure');
            //Type de procedure
            $typeProcedure = (new Atexo_Consultation_ProcedureType())->retrieveTypeProcedureObjectByAbreviationInterface($consultation->getAbbreviationTypeProcedure(), $organisme);
            if ($typeProcedure) {
                $idTypeProcedureOrg = $typeProcedure->getIdTypeProcedure();
                $idTypeProcedurePortail = $typeProcedure->getIdTypeProcedurePortail();
                $consultation->setIdTypeProcedure($idTypeProcedurePortail);
                $consultation->setIdTypeProcedureOrg($idTypeProcedureOrg);
            }
            $consultation->setTypeAcces(Atexo_Config::getParameter('TYPE_PROCEDURE_PUBLICITE'));
            $erreur = self::validerAnnonceData($consultation, $logger);
            if ($erreur) {
                return $erreur;
            } else {
                try {
                    $idEtatAnnonce = $consultation->getFullAnnonceStatut();
                    if ($idEtatAnnonce == Atexo_Config::getParameter('STATUS_PREPARATION')) {
                        $logger->info("Etat de l'annonce =  ATTENTE_VALIDATION");
                        $consultation->setIdEtatConsultation('0');
                        $consultation->setEtatEnAttenteValidation('1');
                        $consultation->setEtatValidation(Atexo_Config::getParameter('CONS_NON_VALIDEE'));
                        $consultation->setDateMiseEnLigneCalcule('0000-00-00 00:00:00');
                    } elseif ($idEtatAnnonce == Atexo_Config::getParameter('STATUS_CONSULTATION') || $idEtatAnnonce == Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE')) {
                        $logger->info("Etat de l'annonce =  VALIDER ou HORS_LIGNE");
                        $consultation->setIdEtatConsultation('0');
                        $consultation->setEtatEnAttenteValidation('1');
                        $consultation->setEtatValidation(Atexo_Config::getParameter('CONS_VALIDEE'));
                        $consultation->setDateMiseEnLigneCalcule(date('Y-m-d H:i:s'));
                        $consultation->setDateValidation(date('Y-m-d H:i:s'));
                    }
                    $logger->info("Debut de l'enregistrement de l'annonce ");
                    $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));
                    $connexion->beginTransaction();
                    if (Atexo_Module::isEnabled('LieuxExecution')) {
                        $lieuExecutions = $consultation->getIdsLieuExecutions();
                        if (is_array($lieuExecutions) && count($lieuExecutions)) {
                            $idsLieuExecution = ',' . implode(',', $lieuExecutions) . ',';
                            $logger->info("L'enregistrement des lieux d'executions " . $idsLieuExecution);
                            $consultation->setLieuExecution($idsLieuExecution);
                        }
                    }
                    if ($consultationId) {
                        $consultation->setNew(false);
                    } else {
                        $consultation->setNew(true);
                        $consultation->setId(null);
                    }
                    $logger->info("L'enregistrement de tous les elements de l'annonce");
                    $logger->info("La recuperation de la denomination de l'organiseme ");
                    $denominationOrg = self::getOrganismeDenominationByService($organisme, $consultation->getServiceId());
                    $logger->info("Pour l'organiseme " . $organisme . " et l'idService = " . $consultation->getServiceId() . " La denomination de l'organiseme est '" . $denominationOrg . "'");
                    $consultation->setOrgDenomination($denominationOrg);
                    $consultation->setDenominationAdapte(strtoupper(Atexo_Util::formatterChaine($denominationOrg)));
                    $consultation->saveConsultation($connexion);
                    $consultationId = $consultation->getId();
                    $urlExterne = Atexo_MpeSf::getUrlDetailsConsultation($consultationId, 'orgAcronyme=' . $organisme);
                    $logger->info("L'enregistrement de l'url externe de l'annonce URL = " . $urlExterne);
                    $consultation->setUrlConsultationExterne($urlExterne);
                    $consultation->save($connexion);
                    if (is_array($consultation->getNomFichiers()) && count($consultation->getNomFichiers())) {
                        $logger->info("Debut de traitement des fichiers de l'annonce");
                        $files = $consultation->getNomFichiers();
                        foreach ($files as $file) {
                            $infile = $file['filePath'];
                            if ($infile) {
                                $fileName = $file['fileName'];
                                $logger->info("Debut de l'enregistrement du fichier : " . $fileName);
                                $atexoBlob = new Atexo_Blob();
                                $atexoCrypto = new Atexo_Crypto();
                                $arrayTimeStampAvis = $atexoCrypto->timeStampFile($infile);
                                if (is_array($arrayTimeStampAvis)) {
                                    $avisIdBlob = $atexoBlob->insert_blob($fileName, $infile, $consultation->getOrganisme());
                                    if ('0' != $avisIdBlob) {
                                        $avis = new CommonAVIS();
                                        $avis->setOrganisme($consultation->getOrganisme());
                                        $avis->setConsultationId($consultation->getId());
                                        $avis->setHorodatage($arrayTimeStampAvis['horodatage']);
                                        $avis->setUntrusteddate($arrayTimeStampAvis['untrustedDate']);
                                        if ($consultation->getIdCreateur()) {
                                            $avis->setAgentId($consultation->getIdCreateur());
                                        }
                                        $avis->setAvis($avisIdBlob);
                                        $avis->setNomFichier($fileName);
                                        $avis->setStatut(Atexo_Config::getParameter('DESTINATAIRE_PUBLIE'));
                                        $avis->setDatePub(date('Y-m-d'));
                                        $avis->setType(Atexo_Config::getParameter('TYPE_FORMAT_LIBRE_FICHIER_JOINT'));
                                        $avis->save($connexion);
                                        $logger->info("Fin de l'enregistrement du fichier : " . $fileName);
                                    }
                                } else {
                                    return "Probleme d'horodatage du fichier: " . $fileName;
                                }
                            }
                        }
                        $logger->info("Fin de traitement des fichiers de l'annonce");
                    }
                    $connexion->commit();
                    $logger->info("Fin de l'enregistrement de l'annonce l'id = " . $consultationId);
                    BaseCommonConsultationPeer::removeInstanceFromPool($consultation);
                    $consultation = (new Atexo_Consultation())->retrieveConsultation($consultationId, $organisme);
                    if ($consultation instanceof CommonConsultation) {
                        if ($oldServiceId) {
                            $consultation->setServiceId($oldServiceId);
                        }

                        $consultationFiled = new Atexo_Rest_Action_ConsultationField($consultation);

                        return $consultationFiled;
                    } else {
                        $logger->error("Probleme lors de l'enregistrement de l'annonce dans la base de donnée");

                        return "Probleme lors de l'enregistrement de l'annonce dans la base de donnée";
                    }
                } catch (\Exception $e) {
                    $connexion->rollBack();

                    $logger->error("Probleme lors de l'enregistrement= " . $e->getMessage() . $e->getTraceAsString());

                    return "Probleme lors de l'enregistrement ";
                }
            }
        } else {
            $msg = "input n'est pas une instance de Atexo_Rest_Action_ConsultationField";

            return $msg;
        }
    }

    /**
     * Permet de valider les donnees de l'annonce;.
     *
     * @param Atexo_Rest_Action_ConsultationField $consultation
     *
     * @return string message dans le cas d'erreur
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function validerAnnonceData($consultation, &$logger)
    {
        $logger->info("Debut de la validation de l'annonce ");
        $erreur = self::validerData($consultation);
        if ($erreur) {
            return $erreur;
        }
        $idEtatAnnonce = $consultation->getFullAnnonceStatut();
        if (
            $idEtatAnnonce == Atexo_Config::getParameter('STATUS_PREPARATION') || $idEtatAnnonce == Atexo_Config::getParameter('STATUS_CONSULTATION')
            || $idEtatAnnonce == Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE')
        ) {
            if (!$consultation->getDatefin() || strstr($consultation->getDatefin(), '0000-00-00')) {
                return "La date fin d'affichage est obligatoire ";
            }
            if ($idEtatAnnonce == Atexo_Config::getParameter('STATUS_CONSULTATION')) {
                $logger->info("La verfication de la date fin d'une telle façon que date Now <= dateFin ");
                if ($consultation->getDatefin() < date('Y-m-d H:i:s')) {
                    return 'La date fin affichage doit être superieur à la date actuelle';
                }
            } elseif ($idEtatAnnonce == Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE')) {
                $logger->info("La verfication de la date fin d'une telle façon que date Now > dateFin ");
                if ($consultation->getDatefin() > date('Y-m-d H:i:s')) {
                    return 'La date fin affichage doit être inférieur à la date actuelle';
                }
            }
        }
        $logger->info(" Fin de la validation de l'annonce ");
    }

    /**
     * Permet de retourner l'id etat de la consultation selon le statut.
     *
     * @param string $statut
     *
     * @return int $idEtat
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getIdEtatConsByStatut($statut)
    {
        $idEtat = 0;
        if ('ELABORATION' === $statut) {
            $idEtat = Atexo_Config::getParameter('STATUS_ELABORATION');
        } elseif ('EN_ATTENTE_VALIDATION' === $statut || 'ATTENTE_VALIDATION' === $statut) {
            $idEtat = Atexo_Config::getParameter('STATUS_PREPARATION');
        } elseif ('CONSULTATION' === $statut || 'VALIDER' === $statut) {
            $idEtat = Atexo_Config::getParameter('STATUS_CONSULTATION');
        } elseif ('OUVERTURE_ANALYSE' === $statut || 'HORS_LIGNE' === $statut) {
            $idEtat = Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE');
        } elseif ('DECISION' === $statut) {
            $idEtat = Atexo_Config::getParameter('STATUS_DECISION');
        }

        return $idEtat;
    }

    /**
     * Permet de retourner le statutu de la consultation.
     *
     * @param int $idStatut id etat de la consultation
     *
     * @return string $statut le statut
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getStatutByIdEtatCons($idStatut)
    {
        $statut = '';
        if ($idStatut == Atexo_Config::getParameter('STATUS_ELABORATION')) {
            $statut = 'ELABORATION';
        } elseif ($idStatut == Atexo_Config::getParameter('STATUS_PREPARATION')) {
            $statut = 'EN_ATTENTE_VALIDATION';
        } elseif ($idStatut == Atexo_Config::getParameter('STATUS_CONSULTATION')) {
            $statut = 'CONSULTATION';
        } elseif ($idStatut == Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE')) {
            $statut = 'OUVERTURE_ANALYSE';
        } elseif ($idStatut == Atexo_Config::getParameter('STATUS_DECISION')) {
            $statut = 'DECISION';
        }

        return $statut;
    }

    /**
     * Permet de retourner le statutu de l'annonce.
     *
     * @param int $idStatut id etat de l'annonce
     *
     * @return string $statut le statutt
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getStatutByIdEtatAnnonce($idStatut)
    {
        $statut = '';
        if ($idStatut == Atexo_Config::getParameter('STATUS_PREPARATION')) {
            $statut = 'ATTENTE_VALIDATION';
        } elseif ($idStatut == Atexo_Config::getParameter('STATUS_CONSULTATION')) {
            $statut = 'VALIDER';
        } elseif ($idStatut == Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE')) {
            $statut = 'HORS_LIGNE';
        }

        return $statut;
    }

    /**
     * Permet de retourner 1 si la reponse est autorisée sinon 0.
     *
     * @param string $reponseElectronique type de reponse electronique
     *
     * @return int
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getValAutoriseReponse($reponseElectronique)
    {
        if ('autorisee' === $reponseElectronique || 'obligatoire' === $reponseElectronique) {
            return 1;
        } else {
            return 0;
        }
    }

    /**
     * Permet de retourner 1 si la reponse est obligatoire sinon 0.
     *
     * @param string $reponseElectronique type de reponse electronique
     *
     * @return int
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getValReponseObligatoire($reponseElectronique)
    {
        if ('obligatoire' === $reponseElectronique) {
            return 1;
        } else {
            return 0;
        }
    }

    /**
     * Permet de retourner le type de reponse de l'annonce.
     *
     * @param int $autoriseReponse ,integer $reponseObligatoire
     *
     * @return string type de la reponse
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getTypeReponse($autoriseReponse, $reponseObligatoire)
    {
        if ($reponseObligatoire) {
            return 'OBLIGATOIRE';
        } elseif ($autoriseReponse) {
            return 'AUTORISEE';
        } else {
            return 'REFUSEE';
        }
    }

    /**
     * Permet de retourner la valeur de type d'avis dans le cas Annnonce.
     *
     * @param string $type le type d'avis
     *
     * @return int l'id type avis
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getValTypeAvis($type)
    {
        if ('INFORMATION' === $type) {
            return Atexo_Config::getParameter('TYPE_AVIS_INFORMATION');
        } elseif ('ATTRIBUTION' === $type) {
            return Atexo_Config::getParameter('TYPE_AVIS_ATTRIBUTION');
        }
    }

    /**
     *  Permet de retourner la valeur de type d'avis dans le cas Annnonce.
     *
     * @param int $idTypeAvis l'id de type d'avis
     *
     * @return $string type avis
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getValTypeAvisById($idTypeAvis)
    {
        if ($idTypeAvis == Atexo_Config::getParameter('TYPE_AVIS_INFORMATION')) {
            return 'INFORMATION';
        } elseif ($idTypeAvis == Atexo_Config::getParameter('TYPE_AVIS_ATTRIBUTION')) {
            return 'ATTRIBUTION';
        }
    }

    /**
     * Permet de recuperer les consultations archives dans la journee d'il y a un certain delai.
     *
     * @param string $date        : date de generation des archives au format timestamp
     * @param array  $acronymeOrg : l'acronyme de l'organisme
     * @param Logger $logger      : logger
     *
     * @return array: tableau des consultations
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function retrieveConsultationArchiveeByDate($date, $acronymeOrgs = [], $logger = null)
    {
        try {
            $idEtat = Atexo_Config::getParameter('STATUS_ARCHIVE_REALISEE');
            $sql = <<<SQL
					 SELECT
     						id,
     						organisme,
     						date_archivage
     				 FROM
     				 		consultation
     				 WHERE
     				 		SUBSTR(date_archivage, 1, 10) LIKE '$date'
     				 		AND date_archivage IS NOT NULL
     				 		AND date_archivage != ''
     				 		AND id_etat_consultation = '$idEtat'
SQL;
            if (is_array($acronymeOrgs) && !empty($acronymeOrgs)) {
                $sql .= '	AND ( 0 ';
                foreach ($acronymeOrgs as $key => $services) {
                    $sql .= " OR ( organisme = '$key'";
                    if (!in_array(0, $services)) {
                        $sql .= " AND service_id in ('" . implode("','", $services) . "')";
                    }
                    $sql .= ')';
                }
                $sql .= ')';
            }

            $listeCons = [];
            $statement = Atexo_Db::getLinkCommon(true)->prepare($sql);
            $results = $statement->execute();
            if ($results) {
                while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                    $listeCons[$row['id']]['id'] = $row['id'];
                    $listeCons[$row['id']]['organisme'] = $row['organisme'];
                    $listeCons[$row['id']]['dateArchivage'] = $row['date_archivage'];
                }
            }

            return $listeCons;
        } catch (\Exception $e) {
            if (null == $logger) {
                $logger = Atexo_LoggerManager::getLogger('app');
            }
            $logger->error('Erreur technique = ' . $e->getMessage() . ' ' . $e->getTraceAsString());
        }
    }

    /**
     * sauvgarder le service de validation de la consultation.
     *
     * @param CommonConsultation $consultation          objet du CommonConsultation
     * @param int                $idServiceRattachement objet du CommonConsultation
     * @param bool               $isContinuation        est ce que c'est la création d'une suite
     * @param bool               $isRefCons             est ce que la création d'une consultation
     *
     * @return void
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function saveIdServiceValidation(&$consultation, $idServiceRattachement, $isContinuation, $isRefCons)
    {
        $proceduresTypes = Atexo_Consultation_ProcedureType::retrieveProcedureType();
        $typeProcedure = $proceduresTypes[$consultation->getIdTypeProcedureOrg()];
        //Sauvegarde service validation, cas gestion mandataire
        if ($typeProcedure instanceof CommonTypeProcedureOrganisme) {
            $consultation->setIdRegleValidation($typeProcedure->getIdTypeValidation());
            if ($typeProcedure->getIdTypeValidation() == Atexo_Config::getParameter('REGLE_VALIDATION_TYPE_3') || $typeProcedure->getIdTypeValidation() == Atexo_Config::getParameter('REGLE_VALIDATION_TYPE_4')) {
                $serviceValidation = ($typeProcedure->getServiceValidation() == 0) ? null : $typeProcedure->getServiceValidation();
                $consultation->setServiceValidation($serviceValidation);
                if ($typeProcedure->getIdTypeValidation() == Atexo_Config::getParameter('REGLE_VALIDATION_TYPE_4')) {
                    if (Atexo_Module::isEnabled('GestionMandataire')) {
                        $idServiceValidation = $idServiceRattachement;
                    } else {
                        $idServiceValidation = Atexo_CurrentUser::getCurrentServiceId();
                    }
                    $idServiceValidation = $idServiceValidation > 0 ? $idServiceValidation : null;
                    $consultation->setServiceValidationIntermediaire($idServiceValidation);
                }
            } else {
                if (Atexo_Module::isEnabled('GestionMandataire')) {
                    $idServiceValidation = $idServiceRattachement;
                } else {
                    $idServiceValidation = Atexo_CurrentUser::getCurrentServiceId();
                }
                $idServiceValidation = $idServiceValidation > 0 ? $idServiceValidation : null;
                $consultation->setServiceValidation($idServiceValidation);
                if ($typeProcedure->getIdTypeValidation() == Atexo_Config::getParameter('REGLE_VALIDATION_TYPE_5')) {
                    $consultation->setServiceValidationIntermediaire($idServiceValidation);
                }
            }
        }
    }

    /**
     * Retour le siret du service ou de l'organisme.
     *
     * @param $organisme
     * @param $idService
     *
     * @return string
     */
    public function getSiretByService($organisme, $idService)
    {
        $entitePublique = '';
        if (Atexo_Module::isEnabled('AffichageNomServicePere', $organisme)) {
            if ($idService) {
                $servicesParents = Atexo_EntityPurchase::getArParentsId($idService, $organisme, true);
                $idServiceParent = $servicesParents[(count($servicesParents) - 2)];
                $serviceParent = (new Atexo_EntityPurchase())->getEntityById($idServiceParent, $organisme);
                if ($serviceParent instanceof CommonService) {
                    $entitePublique = $serviceParent->getSiren() . ' - ' . $serviceParent->getComplement();
                }
            } else {
                $organismeObjet = Atexo_Organismes::retrieveOrganismeByAcronyme($organisme);
                if ($organismeObjet instanceof CommonOrganisme) {
                    $entitePublique = $organismeObjet->getSiren() . ' - ' . $organismeObjet->getComplement();
                }
            }
        } else {
            $organismeObjet = Atexo_Organismes::retrieveOrganismeByAcronyme($organisme);
            //cette variable $entitePublique sert à remplir le champ org_denomination dans ANM
            //par défaut on prend les informations du pouvoir adjudicateur à savoir l'organisme
            if ($organismeObjet) {
                $entitePublique = $organismeObjet->getSiren() . ' - ' . $organismeObjet->getComplement();
            }
            if (!Atexo_Module::isEnabled('OrganisationCentralisee', $organisme)) {
                $service = (new Atexo_EntityPurchase())->getEntityById($idService, $organisme);
                if ($service && $organismeObjet) {
                    $entitePublique = $service->getSiren() . ' - ' . $service->getComplement();
                }
            } else {
                $entitePublique = $organismeObjet->getSiren() . ' - ' . $organismeObjet->getComplement();
            }
        }

        return $entitePublique;
    }

    /**
     * Fonction qui pointe maintenant vers id de consultation (et plus référence)
     * on garde le nom de la fonction car elle est utilisée dans plusieurs endroit.
     *
     * @param $consultationId
     * @param $organisme
     * @param null $connexion
     *
     * @return CommonConsultation
     *
     * @throws Atexo_Config_Exception
     */
    public function getConsultationByReference($consultationId, $organisme, $connexion = null)
    {
        $consultationQuery = new CommonConsultationQuery();

        return $consultationQuery->getConsultationByTypeAvis($consultationId, $organisme, Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION'), $connexion);
    }

    /**
     * Retourne le siret de l'organisme ou du service.
     *
     * @param $organisme
     * @param $idService
     *
     * @return string
     */
    public function getSiretOrganismeByService($organisme, $idService)
    {
        $entitePublique = '';
        if (Atexo_Module::isEnabled('AffichageNomServicePere', $organisme)) {
            if ($idService) {
                $servicesParents = Atexo_EntityPurchase::getArParentsId($idService, $organisme, true);
                $idServiceParent = $servicesParents[(count($servicesParents) - 2)];
                $serviceParent = (new Atexo_EntityPurchase())->getEntityById($idServiceParent, $organisme);

                if ($serviceParent instanceof CommonService) {
                    $entitePublique = $serviceParent->getSiren() . ' - ' . $serviceParent->getComplement();
                }
            } else {
                $organismeObjet = Atexo_Organismes::retrieveOrganismeByAcronyme($organisme);
                if ($organismeObjet instanceof CommonOrganisme) {
                    $entitePublique = $organismeObjet->getSiren() . ' - ' . $organismeObjet->getComplement();
                }
            }
        } else {
            $organismeObjet = Atexo_Organismes::retrieveOrganismeByAcronyme($organisme);
            //cette variable $entitePublique sert à remplir le champ org_denomination dans ANM
            //par défaut on prend les informations du pouvoir adjudicateur à savoir l'organisme
            if ($organismeObjet) {
                $entitePublique = $organismeObjet->getSiren() . ' - ' . $organismeObjet->getComplement();
            }
            if (!Atexo_Module::isEnabled('OrganisationCentralisee', $organisme)) {
                $service = (new Atexo_EntityPurchase())->getEntityById($idService, $organisme);
                if ($service && $organismeObjet) {
                    $entitePublique = $service->getSiren() . ' - ' . $service->getComplement();
                }
            }
        }

        return $entitePublique;
    }

    /**
     * @param $idPli
     * @param $organisme
     *
     * @return int|null
     */
    public function getReferenceConsultationByIdFichierEnveloppe($idPli, $organisme = null)
    {
        $sql = <<<QUERY
SELECT
	consultation.id, consultation.organisme From fichierEnveloppe , Enveloppe, Offres, consultation
WHERE
        consultation.id = Offres.consultation_id
    AND
        consultation.organisme = Offres.organisme
    AND
		Enveloppe.offre_id = Offres.id
	AND
		Enveloppe.organisme = Offres.organisme
	AND
		fichierEnveloppe.id_enveloppe = Enveloppe.id_enveloppe_electro
	AND
		fichierEnveloppe.organisme = Enveloppe.organisme
	AND
	    fichierEnveloppe.id_fichier = :idPli
QUERY;
        $params = [
            ':idPli' => $idPli,
        ];
        if ($organisme) {
            $sql .= 'AND
		fichierEnveloppe.organisme = :organisme';
            $params[':organisme'] = $organisme;
        }
        $link = Atexo_Db::getLinkCommon();
        $statement = $link->prepare($sql);
        $statement->execute($params);

        return $statement->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * @param CommonConsultation|CommonCategorieLot $object
     * @return void
     */
    public static function createOrUpdateAchatsResponsables(CommonConsultation|CommonCategorieLot $object): void
    {
        if (!Atexo_Module::isEnabled('ConsultationClause')) {
            return;
        }

        $clausesN1Ref = Atexo_Util::getSfService(ClausesN1Repository::class)
            ->getAllClausesN1IndexedBySlug();
        $clausesN2Ref = Atexo_Util::getSfService(ClausesN2Repository::class)
            ->getAllClausesN2IndexedBySlug();
        $clausesN3Ref = Atexo_Util::getSfService(ClausesN3Repository::class)
            ->getAllClausesN3IndexedBySlug();

        /** @var ClausesService $clausesService */
        $clausesService = Atexo_Util::getSfService(ClausesService::class);

        $clausesN1 = [];
        if ('1' == $object->getClauseSociale() || $object->getMarcheInsertion()) {
            $clausesN2 = [];
            if ($object->getMarcheInsertion()) {
                $clausesN2[] = [
                    'referentielClauseN2' => $clausesService->getIriFromItem($clausesN2Ref['insertion'])
                ];
            }
            if ('1' == $object->getClauseSocialeConditionExecution()) {
                $clausesN2[] = [
                    'referentielClauseN2' => $clausesService
                        ->getIriFromItem($clausesN2Ref['conditionExecution'])
                ];
            }
            if ('1' == $object->getClauseSocialeInsertion()) {
                $clausesN2[] = [
                    'referentielClauseN2' => $clausesService->getIriFromItem($clausesN2Ref['critereAttributionMarche'])
                ];
            }

            $clausesN3 = [];
            if ('1' == $object->getClauseSocialeAteliersProteges()) {
                $clausesN3[] = [
                    'referentielClauseN3' => $clausesService
                        ->getIriFromItem($clausesN3Ref['clauseSocialeReserveAtelierProtege'])
                ];
            }
            if ('1' == $object->getClauseSocialeSiae()) {
                $clausesN3[] = [
                    'referentielClauseN3' => $clausesService
                        ->getIriFromItem($clausesN3Ref['clauseSocialeSIAE'])
                ];
            }
            if ('1' == $object->getClauseSocialeEss()) {
                $clausesN3[] = [
                    'referentielClauseN3' => $clausesService
                        ->getIriFromItem($clausesN3Ref['clauseSocialeEESS'])
                ];
            }

            if (!empty($clausesN3)) {
                $clausesN2[] = [
                    'referentielClauseN2' => $clausesService
                        ->getIriFromItem($clausesN2Ref['marcheReserve']),
                    'clausesN3' => $clausesN3
                ];
            }

            $clausesN1[] = [
                'referentielClauseN1' => $clausesService
                    ->getIriFromItem($clausesN1Ref['clausesSociales']),
                'clausesN2' => $clausesN2
            ];
        }

        if ('1' == $object->getClauseEnvironnementale()) {
            $clausesN2 = [];
            if ('1' == $object->getClauseEnvSpecsTechniques()) {
                $clausesN2[] = [
                    'referentielClauseN2' => $clausesService
                        ->getIriFromItem($clausesN2Ref['specificationsTechniques'])
                ];
            }
            if ('1' == $object->getClauseEnvCondExecution()) {
                $clausesN2[] = [
                    'referentielClauseN2' => $clausesService
                        ->getIriFromItem($clausesN2Ref['conditionsExecutions'])
                ];
            }
            if ('1' == $object->getClauseEnvCriteresSelect()) {
                $clausesN2[] = [
                    'referentielClauseN2' => $clausesService
                        ->getIriFromItem($clausesN2Ref['criteresSelections'])
                ];
            }

            $clausesN1[] = [
                'referentielClauseN1' => $clausesService
                    ->getIriFromItem($clausesN1Ref['clauseEnvironnementale']),
                'clausesN2' => $clausesN2
            ];
        }

        if ($object instanceof CommonConsultation) {
            /** @var ConsultationRepository $consultationRepository */
            $consultationRepository = Atexo_Util::getSfService(ConsultationRepository::class);
            /** @var Consultation $consultationSf */
            $consultationSf = $consultationRepository->find($object->getId());

            $clausesService->updateClauses($consultationSf, $clausesN1);

            $consultationRepository->persistConsultation($consultationSf);
        } else {
            /** @var LotRepository $lotRepository */
            $lotRepository = Atexo_Util::getSfService(LotRepository::class);
            /** @var Lot $consultationSf */
            $lotSf = $lotRepository->find($object->getId());

            $clausesService->updateClauses($lotSf, $clausesN1);

            $lotRepository->persistLot($lotSf);
        }
    }

    public static function duplicateLotAchatsResponsablesFromConsultation(CommonConsultation|CommonCategorieLot $object, ?int $lotId): void
    {
        if (empty($lotId)) {
            throw new Atexo_Consultation_Exception('Id lot vide');
        }
        if (!Atexo_Module::isEnabled('ConsultationClause')) {
            return;
        }

        if ($object instanceof CommonConsultation) {
            /** @var ConsultationRepository $objectRepository */
            $objectRepository = Atexo_Util::getSfService(ConsultationRepository::class);
        } else {
            /** @var LotRepository $objectRepository */
            $objectRepository = Atexo_Util::getSfService(LotRepository::class);
        }

        /** @var Consultation|Lot $sfObject */
        $sfObject = $objectRepository->find($object->getId());

        /** @var ClausesService $clausesService */
        $clausesService = Atexo_Util::getSfService(ClausesService::class);


        $clausesN1 = [];
        foreach ($sfObject->getClausesN1() as $clauseN1) {
            $clausesN2 = [];
            foreach ($clauseN1->getClausesN2() as $clauseN2) {
                $clausesN3 = [];
                foreach ($clauseN2->getClausesN3() as $clauseN3) {
                    $clausesN3[] = [
                        'referentielClauseN3' => $clausesService
                            ->getIriFromItem($clauseN3->getReferentielClauseN3())
                    ];
                }
                $clausesN2[] = [
                    'referentielClauseN2' => $clausesService
                        ->getIriFromItem($clauseN2->getReferentielClauseN2()),
                    'clausesN3' => $clausesN3
                ];
            }
            $clausesN1[] = [
                'referentielClauseN1' => $clausesService
                    ->getIriFromItem($clauseN1->getReferentielClauseN1()),
                'clausesN2' => $clausesN2
            ];
        }

        /** @var LotRepository $lotRepository */
        $lotRepository = Atexo_Util::getSfService(LotRepository::class);

        $lotSF = $lotRepository->find($lotId);
        if (!$lotSF) {
            return;
        }

        $clausesService->updateClauses($lotSF, $clausesN1);
        $lotRepository->persistLot($lotSF);
    }

    public static function duplicateAchatsResponsablesFromConsultation(CommonConsultation|CommonCategorieLot $object, ?int $contratId): void
    {
        if (empty($contratId)) {
            throw new Atexo_Consultation_Exception('Id contrat vide');
        }
        if (!Atexo_Module::isEnabled('ConsultationClause')) {
            return;
        }

        if ($object instanceof CommonConsultation) {
            /** @var ConsultationRepository $objectRepository */
            $objectRepository = Atexo_Util::getSfService(ConsultationRepository::class);
        } else {
            /** @var LotRepository $objectRepository */
            $objectRepository = Atexo_Util::getSfService(LotRepository::class);
        }

        /** @var Consultation|Lot $sfObject */
        $sfObject = $objectRepository->find($object->getId());

        /** @var ClausesService $clausesService */
        $clausesService = Atexo_Util::getSfService(ClausesService::class);


        $clausesN1 = [];
        foreach ($sfObject->getClausesN1() as $clauseN1) {
            $clausesN2 = [];
            foreach ($clauseN1->getClausesN2() as $clauseN2) {
                $clausesN3 = [];
                foreach ($clauseN2->getClausesN3() as $clauseN3) {
                    $clausesN3[] = [
                        'referentielClauseN3' => $clausesService
                            ->getIriFromItem($clauseN3->getReferentielClauseN3())
                    ];
                }
                $clausesN2[] = [
                    'referentielClauseN2' => $clausesService
                        ->getIriFromItem($clauseN2->getReferentielClauseN2()),
                    'clausesN3' => $clausesN3
                ];
            }
            $clausesN1[] = [
                'referentielClauseN1' => $clausesService
                    ->getIriFromItem($clauseN1->getReferentielClauseN1()),
                'clausesN2' => $clausesN2
            ];
        }

        /** @var ContratTitulaireRepository $contratRepository */
        $contratRepository = Atexo_Util::getSfService(ContratTitulaireRepository::class);

        $contratSF = $contratRepository->find($contratId);
        if (!$contratSF) {
            return;
        }

        $clausesService->updateClauses($contratSF, $clausesN1);
        $contratRepository->persistContrat($contratSF);
    }

    public function getClausesN1(string $consultationId): array
    {
        return Atexo_Util::getSfService(ClausesService::class)->getClausesN1LabelByConsultationId($consultationId);
    }

    public static function hasClauseSocialeN1(?CommonConsultation $objet): bool
    {
        $clauseSociale = (new self())->getClausesN1($objet->getId());

        return array_key_exists(Consultation\ClausesN1::CLAUSE_SOCIALES, $clauseSociale);
    }

    public static function hasClauseEnvironnementalesN1(?CommonConsultation $objet)
    {
        $clauseSociale = (new self())->getClausesN1($objet->getId());

        return array_key_exists(Consultation\ClausesN1::CLAUSES_ENVIRONNEMENTALES, $clauseSociale);
    }

    /**
     * retourne un tableau d'Informations complementaires de la consultation.
     */
    public static function arrayConsultationSummary($consultation, $org = null)
    {
        $tableKeys = [];
        if (!$org) {
            $org = Atexo_CurrentUser::getOrganismAcronym();
        }
        $organismeO = Atexo_Organismes::retrieveOrganismeByAcronyme($org);
        $tableKeys['entitePublique'] = Atexo_Util::replaceSpecialCharacters(Atexo_Util::escapeXmlChars($organismeO->getSigle() . ' - ' . $organismeO->getDenominationOrg()));
        $tableKeys['entiteAchat'] = Atexo_Util::replaceSpecialCharacters(Atexo_Util::escapeXmlChars(Atexo_EntityPurchase::getPathEntityById($consultation->getServiceId(), $org)));

        if (Atexo_Module::isEnabled('NumeroProjetAchat')) {
            $numeroProjetAchat = Atexo_Util::replaceSpecialCharacters(Atexo_Util::escapeXmlChars($consultation->getCodeExterne()));
            $labelNumeroProjetAchat = prado::localize('DEFINE_FULL_NUMERO_PROJET_ACHAT') . ' : ';
            $tableKeys['BALISE_TO_REPLACE_FOR_NUMERO_PROJET_ACHAT'] = '<text:p text:style-name="P5">' . $labelNumeroProjetAchat . $numeroProjetAchat . '</text:p>';
        } else {
            $tableKeys['BALISE_TO_REPLACE_FOR_NUMERO_PROJET_ACHAT'] = '';
        }

        $tableKeys['refConsultation'] = Atexo_Util::replaceSpecialCharacters(Atexo_Util::escapeXmlChars($consultation->getReferenceUtilisateur()));
        $tableKeys['intituleConsultation'] = Atexo_Util::replaceSpecialCharacters(Atexo_Util::escapeXmlChars(Atexo_Util::atexoHtmlEntitiesDecode($consultation->getIntitule())));
        $tableKeys['objetConsultation'] = Atexo_Util::replaceSpecialCharacters(Atexo_Util::escapeXmlChars(Atexo_Util::atexoHtmlEntitiesDecode($consultation->getObjet())));
        $typeAvis = (new Atexo_Consultation_TypeAvis())->retrieveTypeAnnonce($consultation->getIdTypeAvis());
        if ($typeAvis) {
            $tableKeys['typeAnnonce'] = Atexo_Util::replaceSpecialCharacters(Atexo_Util::escapeXmlChars($typeAvis->getIntituleAvis()));
        }
        $libelleProcedure = str_replace(
            '&#8804;',
            Prado::localize('MAPA_INF_OU_EGALE'),
            (new Atexo_Consultation_ProcedureType())->retrieveTypeProcedure($consultation->getIdTypeProcedureOrg(), false, $org)
        );
        $libelleProcedure = str_replace('&#8805;', Prado::localize('MAPA_SUPP_OU_EGALE'), $libelleProcedure);
        $tableKeys['typeProcedure'] = Atexo_Util::replaceSpecialCharacters(Atexo_Util::escapeXmlChars($libelleProcedure));
        $tableKeys['dateRemisePlis'] = Atexo_Util::iso2frnDateTime($consultation->getDatefin());
        $dateMiseEnLigne = Atexo_Util::iso2frnDateTime(Atexo_Consultation::getDateMiseEnLigne($consultation));
        if ($dateMiseEnLigne) {
            $tableKeys['dateMiseLigne'] = Atexo_Util::replaceSpecialCharacters(Atexo_Util::escapeXmlChars($dateMiseEnLigne));
        } else {
            $tableKeys['dateMiseLigne'] = '-';
        }
        if (
            Atexo_Module::isEnabled('FuseauHoraire', Atexo_CurrentUser::getOrganismAcronym())
            && (new Atexo_EntityPurchase())->getEtatActivationFuseauHoraire(Atexo_CurrentUser::getCurrentServiceId(), Atexo_CurrentUser::getOrganismAcronym())
        ) {
            $scriptXmlFuseauHoraire = '';
            //Date heure limite remise des plis heure locale
            if ('0000-00-00 00:00:00' != $consultation->getDatefinLocale() && '' != $consultation->getDatefinLocale()) {
                $scriptXmlFuseauHoraire .= '<text:p text:style-name="P5">'
                    . Prado::localize('TEXT_DATE_HEURE_LIMITE_REMISE_PLIS_LOCALE') . ' : '
                    . Atexo_Util::iso2frnDateTime(substr($consultation->getDatefinLocale(), 0, 16)) . '</text:p>';
            }
            //Lieu de residence
            if ('' != $consultation->getLieuResidence()) {
                $scriptXmlFuseauHoraire .= '<text:p text:style-name="P5">' . Prado::localize('DEFINE_LIEU_RESIDENCE') . ' : '
                    . $consultation->getLieuResidence() . '</text:p>';
            }
            $tableKeys['<!--dateHeureLocaleLieuResidence-->'] = $scriptXmlFuseauHoraire;
        }

        $lots = $consultation->getAllLots();
        $scriptXmlLots = '';
        if ($consultation->getAlloti()) {
            $tableKeys['allotissement'] = Prado::localize('DEFINE_OUI');
            foreach ($lots as $oneLot) {
                //<text:p text:style-name="P7">lot</text:p>
                $scriptXmlLots .= '<text:p text:style-name="P7">Lot :' . $oneLot->getLot() . ' '
                    . Atexo_Util::replaceSpecialCharacters(
                        Atexo_Util::escapeXmlChars($oneLot->getDescription())
                    ) . '</text:p>';
            }
            $tableKeys['<!--lots-->'] = $scriptXmlLots;
        } else {
            $tableKeys['allotissement'] = Prado::localize('DEFINE_NON');
            $tableKeys['<!--lots-->'] = $scriptXmlLots;
        }

        return $tableKeys;
    }
}
