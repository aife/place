<?php

namespace Application\Service\Atexo;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonSsoAgentPeer;
use Application\Propel\Mpe\CommonTraceOperationsInscrit;
use Application\Propel\Mpe\CommonTraceOperationsInscritPeer;
use Application\Propel\Mpe\CommonTTraceOperationsInscritDetails;
use PDO;
use Prado\Prado;

class Atexo_InscritOperationsTracker
{
    public static function trackingOperations($idInscrit, $idEntreprise, $ip, $date, $dateDebAction, $action, $detail, $dateFinAction, $arrayDonnees = null)
    {
        $description = null;
        $logApplet = null;
        $lienDownload = null;
        $consultationId = '';
        $org = '';
        $idDescription = '';
        $afficher = '';
        if (is_array($arrayDonnees)) {
            $consultationId = $arrayDonnees['ref'];
            $org = $arrayDonnees['org'];
            $idDescription = $arrayDonnees['IdDescritpion'];
            $afficher = $arrayDonnees['afficher'];
            $logApplet = $arrayDonnees['logApplet'];
            $description = $arrayDonnees['description'];
            $lienDownload = $arrayDonnees['lienDownload'];
        } else {
            $consultationId = $arrayDonnees;
        }
        if ('' != $idInscrit) {
            $operationInDb = '';
            $listeIp = isset($_SERVER['HTTP_X_FORWARDED_FOR']) ?
                $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
            $ips = explode(',', $listeIp);
            $ip = trim($ips[0]);
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $traceOprInscritObject = (new Atexo_InscritOperationsTracker())
                ->retrieveTraceOperationsInscrit($idInscrit, $idEntreprise, $ip, $date, $consultationId, $org);
            if (!($traceOprInscritObject instanceof CommonTraceOperationsInscrit)) {
                $enteteOperation = 'date_debut_action;nom_action;details;date_fin_action';
                $traceOprInscritObject = new CommonTraceOperationsInscrit();
                $traceOprInscritObject->setIdInscrit($idInscrit);
                $traceOprInscritObject->setIdEntreprise($idEntreprise);
                $traceOprInscritObject->setAddrIp($ip);
                $traceOprInscritObject->setDate($date);
                $traceOprInscritObject->setOperations($enteteOperation);
                $traceOprInscritObject->setAfficher('1');
                if ($consultationId && $org) {
                    $traceOprInscritObject->setOrganisme($org);
                    $traceOprInscritObject->setConsultationId($consultationId);
                }
                $traceOprInscritObject->setOperations('@todo: Champ plus utiliser');
                $traceOprInscritObject->save($connexionCom);
            }

            $idTrace = $traceOprInscritObject->getIdTrace();
            if ($idTrace) {
                $detailsTrace = new CommonTTraceOperationsInscritDetails();
                $detailsTrace->setIdTrace($idTrace);
                $detailsTrace->setDateDebutAction($dateFinAction);
                $detailsTrace->setNomAction($action);
                $detailsTrace->setDetails($detail);
                $detailsTrace->setdateFinAction($dateDebAction);
                $detailsTrace->setIdDescription($idDescription);
                $detailsTrace->setDescripton($description);
                $detailsTrace->setLogApplet($logApplet);
                if ($lienDownload) {
                    $detailsTrace->setLienDownload($lienDownload);
                }
                if ($afficher) {
                    $detailsTrace->setAfficher('1');
                }
                $detailsTrace->setDebutActionMillisecond(1000 * microtime(true));
                $detailsTrace->save($connexionCom);
            }
        }
    }

    public function retrieveTraceOperationsInscrit($idInscrit, $idEntreprise, $ip, $date, $consultationId = '', $org = '')
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonTraceOperationsInscritPeer::ID_INSCRIT, $idInscrit, Criteria::EQUAL);
        $c->add(CommonTraceOperationsInscritPeer::ID_ENTREPRISE, $idEntreprise, Criteria::EQUAL);
        $c->add(CommonTraceOperationsInscritPeer::ADDR_IP, $ip, Criteria::EQUAL);
        $c->add(CommonTraceOperationsInscritPeer::DATE, $date, Criteria::EQUAL);
        if ($consultationId) {
            $c->add(CommonTraceOperationsInscritPeer::CONSULTATION_ID, $consultationId, Criteria::EQUAL);
        }
        if ($org) {
            $c->add(CommonTraceOperationsInscritPeer::ORGANISME, $org, Criteria::EQUAL);
        }
        $traceOprInscritObject = CommonTraceOperationsInscritPeer::doSelectOne($c, $connexionCom);

        return $traceOprInscritObject ?: false;
    }

    public function newOperationInscrit($idInscrit, $idEntreprise, $ip, $date, $operations, $connexionCom)
    {
        $traceOprInscritObject = new CommonTraceOperationsInscrit();
        $traceOprInscritObject->setIdInscrit($idInscrit);
        $traceOprInscritObject->setIdEntreprise($idEntreprise);
        $traceOprInscritObject->setAddrIp($ip);
        $traceOprInscritObject->setDate($date);
        $traceOprInscritObject->setOperations($operations);
        $traceOprInscritObject->save($connexionCom);
    }

    public function updateOperationInscrit($idTrace, $operations, $connexionCom)
    {
        $traceOprInscritObject = CommonTraceOperationsInscritPeer::retrieveByPK($idTrace, $connexionCom);
        $operationInDb = $traceOprInscritObject->getOperations();
        $traceOprInscritObject->setOperations($operationInDb.$operations);
        $traceOprInscritObject->save($connexionCom);
    }

    public function retrieveTraceOperationsInscritByRefConsultation($consultationId, $org, $idEntreprise = '', $date = '', $orderBy = '', $asc = '', $limit = '', $offset = '')
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonTraceOperationsInscritPeer::CONSULTATION_ID, $consultationId, Criteria::EQUAL);
        $c->add(CommonTraceOperationsInscritPeer::ORGANISME, $org, Criteria::EQUAL);
        if ($idEntreprise) {
            $c->add(CommonTraceOperationsInscritPeer::ID_ENTREPRISE, $idEntreprise, Criteria::EQUAL);
        }
        if ($date) {
            $c->add(CommonTraceOperationsInscritPeer::DATE, $date, Criteria::EQUAL);
        }
//      if($limit) {
        //          $c->setLimit($limit);
        //      }
        //      if($offset) {
        //          $c->setOffset($offset);
        //      }
        //      if($asc && $orderBy){
        //          $column= strtoupper($orderBy);
//
        //      }elseif($orderBy)){
//
        //      }
        //      $c->addDescendingOrderByColumn(CommonSsoAgentPeer::DATE_LAST_REQUEST);
//
        //      $c->addAscendingOrderByColumn($peer->getConstant($column));
//

        $traceOprInscritObject = CommonTraceOperationsInscritPeer::doSelect($c, $connexionCom);

        return $traceOprInscritObject;
    }

    public function retrieveEntrepriseTraceOperationsInscritByRefConsultation($consultationId, $org)
    {
        $requete = "SELECT * FROM trace_operations_inscrit WHERE organisme='".$org."' and consultation_id ='".$consultationId."' GROUP BY id_entreprise";
        $statement = Atexo_Db::getLinkCommon(true)->prepare($requete);
        $results = $statement->execute();
        $data = [];
        $data['0'] = Prado::localize('DEFINE_TOUTES_LES_ENTREPRISES');
        if ($results) {
            while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                $entreprise = (new Atexo_Entreprise())->retrieveCompanyBdeById($row['id_entreprise']);

                if ($entreprise) {
                    $data[$entreprise->getId()] = $entreprise->getNom();
                }
            }

            return $data;
        } else {
            return false;
        }
    }

    public function retrieveDateTraceOperationsInscritByRefConsultation($consultationId, $org)
    {
        $requete = "SELECT * FROM trace_operations_inscrit WHERE organisme='".$org."' and consultation_id ='".$consultationId."' and afficher = '1' GROUP BY date";
        $statement = Atexo_Db::getLinkCommon(true)->prepare($requete);
        $results = $statement->execute();
        $data = [];
        $data['0'] = Prado::localize('DEFINE_TOUTES_LES_DATES');
        if ($results) {
            while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                $data[$row['id_trace']] = Atexo_Util::iso2frnDate($row['date']);
            }

            return $data;
        } else {
            return false;
        }
    }

    public function retrieveTraceByRefConsultation($consultationId, $org, $idEntreprise = '', $date = '', $orderBy = '', $asc = '', $limit = '', $offset = '')
    {
        $traceOprInscritObject = [];
        $requete = "SELECT t.date as date ,t.addr_ip as ip,t.id_trace, e.nom as nomEntreprise , i.nom  as nom_inscrit ,i.prenom as prenom_inscrit FROM `trace_operations_inscrit`t INNER JOIN 
					Entreprise e ON(t.id_entreprise = e.id) INNER JOIN Inscrit i ON(t.id_inscrit = i.id) and afficher = '1'
					where t.consultation_id = '".$consultationId."' 
					AND t.organisme = '".$org."' ";
        if ($idEntreprise) {
            $requete .= " AND t.id_entreprise ='".$idEntreprise."'";
        }
        if ($date) {
            $requete .= "AND t.date =  '".$date."' ";
        }
        if ($orderBy) {
            $requete .= 'ORDER BY '.$orderBy;
        }
        if ($asc) {
            $requete .= ' '.$asc;
        }
        if ($limit) {
            $requete .= ' limit '.$offset.','.$limit;
        }
        $statement = Atexo_Db::getLinkCommon(true)->prepare($requete);
        $results = $statement->execute();
        if ($results) {
            while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                $traceOprInscritObject[$row['id_trace']]['id'] = $row['id_trace'];
                $traceOprInscritObject[$row['id_trace']]['nomEntreprise'] = $row['nomEntreprise'];
                $traceOprInscritObject[$row['id_trace']]['nom_inscrit'] = $row['nom_inscrit'];
                $traceOprInscritObject[$row['id_trace']]['prenom_inscrit'] = $row['prenom_inscrit'];
                $traceOprInscritObject[$row['id_trace']]['addr_ip'] = $row['ip'];
                $traceOprInscritObject[$row['id_trace']]['date'] = $row['date'];
            }
        }

        return $traceOprInscritObject;
    }

    public function retrieveTraceById($idTrace)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonTraceOperationsInscritPeer::ID_TRACE, $idTrace, Criteria::EQUAL);

        $traceOprInscritObject = CommonTraceOperationsInscritPeer::doSelectOne($c, $connexionCom);

        return $traceOprInscritObject ?: false;
    }

    public function retrieveDetailsTraceByRefConsultation($idTrace, $count = false, $orderBy = '', $asc = '', $limit = '', $offset = '')
    {
        $traceOprInscritObject = [];
        $requete = 'SELECT id,id_description,date_debut_action,descripton,lien_download,date_debut_action_client';
        $requete .= " from T_trace_operations_inscrit_details 
					  where id_trace = '".$idTrace."' and afficher = '1' ";
        //jointure avec les traces qui n'ont pas de ref consultation comme authenfitication
        $traceObjet = (new self())->retrieveTraceById($idTrace);
        $requete .= " UNION SELECT  dt.id as id,dt.id_description as IdDescription, dt.date_debut_action as dateAction ,dt.descripton as description, dt.date_debut_action_client,consultation_id
					  FROM trace_operations_inscrit tr, T_trace_operations_inscrit_details dt
					  WHERE tr.date = '".$traceObjet->getDate()."'
					  AND tr.id_Inscrit = '".$traceObjet->getIdInscrit()."'
					  AND tr.id_Entreprise = '".$traceObjet->getIdEntreprise()."'
					  AND tr.addr_Ip = '".$traceObjet->getAddrIp()."'
					  AND tr.organisme IS NULL
					  AND tr.consultation_id IS NULL
					  AND tr.id_Trace = dt.id_Trace
					  AND dt.afficher = '1'
					  GROUP BY dt.id ";
        if (!$count) {
            if (!$orderBy) {
                $orderBy = 'date_debut_action';
            }
            $requete .= 'ORDER BY '.$orderBy;
            if (!$asc) {
                $asc = 'ASC';
            }
            $requete .= ' '.$asc;
            if ($limit) {
                $requete .= ' limit '.$offset.','.$limit;
            }
        }
        $statement = Atexo_Db::getLinkCommon(true)->prepare($requete);
        $results = $statement->execute();
        if ($results) {
            while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                $traceOprInscritObject[$row['id']]['dateAction'] = $row['date_debut_action'];
                $traceOprInscritObject[$row['id']]['description'] = $row['descripton'];
                $traceOprInscritObject[$row['id']]['lien_download'] = $row['lien_download'];
                $traceOprInscritObject[$row['id']]['dateActionPosteClient'] = $row['date_debut_action_client'];
            }
        }
        if ($count) {
            return count($traceOprInscritObject);
        }

        return $traceOprInscritObject;
    }
}
