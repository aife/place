<?php

namespace Application\Service\Atexo;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonMarche;
use Application\Propel\Mpe\CommonMarchePeer;
use Application\Propel\Mpe\CommonMarchePublie;
use Application\Propel\Mpe\CommonMarchePubliePeer;
use Application\Propel\Mpe\CommonMarchePublieQuery;
use Application\Propel\Mpe\CommonTContratTitulaireQuery;
use Application\Propel\Mpe\Om\BaseCommonMarchePeer;
use Application\Service\Atexo\Marche\Atexo_Marche_CriteriaVo;
use PDO;

/**
 * Classe de manipulation des encheres.
 *
 * @author Thibaut DECAUDAIN <thibaut.decaudain@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Marche
{
    public function search(Atexo_Marche_CriteriaVo $criteriaVo)
    {
        $select = 'SELECT * FROM Marche ';
        $join = '';
        $otherQueries = '';
        $orderBy = '';
        //si l'appel est fait du cote agent, on fait une recherche uniquement sur le service qui
        // est concerne par l'agent connecte
        if (0 == $criteriaVo->getCallFromEntreprise()) {
            if (null != $criteriaVo->getIdService() && 0 != $criteriaVo->getIdService()) {
                $id = $criteriaVo->getIdService();
                $otherQueries .= " AND Marche.idService = '$id' ";
            } else {
                $id = $criteriaVo->getIdService();
                $otherQueries .= " AND (Marche.idService IS NULL OR Marche.idService='0')";
            }
        }
        //si l'appel est fait du cote entreprise, on a déjà "normalement" listé les services
        // pour lesquels on recherche les marchés publiés
        else {
            $arrayIdsServices = $criteriaVo->getIdsService();
            if (!empty($arrayIdsServices)) {
                $ids = $criteriaVo->getIdsService();
                $otherQueries .= ' AND (0 ';
                foreach ($ids as $id) {
                    if (0 != $id) {
                        $otherQueries .= " OR Marche.idService = '$id' ";
                    } else {
                        $otherQueries .= " OR (Marche.idService IS NULL OR Marche.idService='0')";
                    }
                }
                $otherQueries .= ')';
            }
        }
        if ($criteriaVo->getAcronymeOrganisme()) {
            $isOrg = $criteriaVo->getAcronymeOrganisme();
            $otherQueries .= " AND Marche.organisme = '".$isOrg."' ";
        }
        if ($criteriaVo->getValide()) {
            $isValide = $criteriaVo->getValide();
            $otherQueries .= " AND Marche.valide = '".$isValide."' ";
        }
        if ($criteriaVo->getNatureMarche()) {
            $idNature = $criteriaVo->getNatureMarche();
            $otherQueries .= " AND Marche.natureMarche = '".$idNature."' ";
        }
        if ($criteriaVo->getNumeroMarcheAnnee()) {
            $annee = $criteriaVo->getNumeroMarcheAnnee();
            $otherQueries .= " AND Marche.numeroMarcheAnnee = '".$annee."' ";
        }
        if ($criteriaVo->getIdMarcheTrancheBudgetaire()) {
            $idMarcheTrancheBudgetaire = $criteriaVo->getIdMarcheTrancheBudgetaire();
            $otherQueries .= " AND Marche.idMarcheTrancheBudgetaire = '".$idMarcheTrancheBudgetaire."' ";
        }
        if ($criteriaVo->getNomAttributaire()) {
            $nomAttributaire = '%'.$criteriaVo->getNomAttributaire().'%';
            $otherQueries .= " AND Marche.nomAttributaire LIKE '".$nomAttributaire."' ";
        }
        if ($criteriaVo->getMotclef()) {
            $otherQueries .= ' AND Marche.objetMarche LIKE ? ';
        }
        if ($criteriaVo->getDate1()) {
            $date1 = $criteriaVo->getDate1();
            $otherQueries .= " AND Marche.dateNotification >= '".$date1."' ";
        }
        if ($criteriaVo->getDate2()) {
            $date2 = $criteriaVo->getDate2();
            $otherQueries .= " AND Marche.dateNotification <= '".$date2."' ";
        }
        if ($criteriaVo->getSensOrderBy()) {
            if ('default' == $criteriaVo->getSortByElement()) {
                $orderBy .= ' ORDER BY Marche.natureMarche, Marche.idMarcheTrancheBudgetaire ASC ';
            } else {
                $orderBy .= ' ORDER BY Marche.'.$criteriaVo->getSortByElement().' '.$criteriaVo->getSensOrderBy();
            }
        }
        $finalQuery = $select.$join.' WHERE 1 '.$otherQueries.$orderBy;

        if ($criteriaVo->getLimit()) {
            $finalQuery .= ' LIMIT '.$criteriaVo->getOffset().','.$criteriaVo->getLimit();
        }

        //$connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        //$stmt = $connexionCom->prepareStatement($finalQuery);

        $stmt = Atexo_Db::getLinkCommon(true)->prepare($finalQuery);
        $stmt->execute();

        if ($criteriaVo->getMotclef()) {
            $stmt->setString(1, '%'.$criteriaVo->getMotclef().'%');
        }
        //$rs = $stmt->executeQuery(ResultSet::FETCHMODE_NUM);

        $arrayMarches = BaseCommonMarchePeer::populateObjects($stmt);

        return $arrayMarches;
    }

    /**
     * Cette fonction permet de supprimer les marchés qui ne sont rattachés à aucune décision
     * En effet, il est possible de supprimer une décision sans que cela n'affecte la liste des marchés.
     */
    private static function removeMarcheImportedWithoutDecision($idServiceAgent, $annee, $organisme)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonMarchePeer::ORGANISME, $organisme);
        $c->add(CommonMarchePeer::NUMEROMARCHEANNEE, $annee);
        $c->add(CommonMarchePeer::IDSERVICE, 0 == $idServiceAgent ? null : $idServiceAgent);
        $c->add(CommonMarchePeer::ISMANUEL, 0);
        $marches = CommonMarchePeer::doSelect($c, $connexionCom);
        foreach ($marches as $marche) {
            $sql = '	SELECT count(*) as nombreDecision'
            .'  FROM t_contrat_titulaire'
            ."  WHERE  t_contrat_titulaire.contrat_class_key = '".Atexo_Config::getParameter('TYPE_OBJET_CONTRAT_TITULAIRE')."'"
                ." AND t_contrat_titulaire.id_contrat_titulaire = '".(new Atexo_Db())->quote($marche->getIdContratTitulaire())."' AND t_contrat_titulaire.organisme = '".$organisme."'"
                ." AND ((t_contrat_titulaire.date_notification IS NOT NULL AND t_contrat_titulaire.date_notification  LIKE '".$annee."%')
				OR (t_contrat_titulaire.date_notification IS NULL AND t_contrat_titulaire.date_prevue_notification  LIKE '".$annee."%' ) )";

            $nbreDecision = '1';

            $statement = Atexo_Db::getLinkCommon()->query($sql);
            while ($row = $statement->fetch(PDO::FETCH_NUM)) {
                $nbreDecision = $row[0];
            }
            if ($nbreDecision < 1) {
                $marche->delete($connexionCom);
            }
        }
    }

    /***
     * Recupérer tous les marchés déja publié par un service à une date donnée
     */
    public function saveSelectedYear($idService, $annee, $organisme)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $c = new Criteria();
        $c->add(CommonMarchePubliePeer::SERVICE_ID, $idService);
        $c->add(CommonMarchePubliePeer::ORGANISME, $organisme);
        $c->add(CommonMarchePubliePeer::NUMEROMARCHEANNEE, $annee);
        $marchePublie = CommonMarchePubliePeer::doSelectOne($c, $connexionCom);
        if (!$marchePublie) {
            $marchePublie = new CommonMarchePublie();
            $marchePublie->setOrganisme($organisme);
            $marchePublie->setServiceId($idService);
            $marchePublie->setNumeromarcheannee($annee);
            $marchePublie->setNewversion('1');
            $marchePublie->setIsimportee('1');
            $marchePublie->save($connexionCom);
        }

        return $marchePublie;
    }

    /***
     * Migrer les données de décision enveloppe vers la table Marche
     */
    public function saveMarche($idService, $annee, $organisme)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $idServiceQuery = $idService ?? '0';
        $sql = 'SELECT distinct t_contrat_titulaire.* , t_contact_contrat.* , t_etablissement.* , Entreprise.* '
            .'  FROM t_contrat_titulaire , t_contact_contrat , t_etablissement , Entreprise'
            ."  WHERE t_contrat_titulaire.contrat_class_key = '".Atexo_Config::getParameter('TYPE_OBJET_CONTRAT_TITULAIRE')."'"
            .'  AND t_contrat_titulaire.id_titulaire_etab = t_etablissement.id_etablissement '
            .'  AND t_contact_contrat.id_etablissement = t_etablissement.id_etablissement '
            .'  AND Entreprise.id = t_etablissement.id_entreprise '
            ."  AND t_contrat_titulaire.service_id='".(new Atexo_Db())->quote($idServiceQuery)."'"
            ."  AND t_contrat_titulaire.organisme = '".$organisme."'"
            ." AND ((t_contrat_titulaire.date_notification IS NOT NULL AND t_contrat_titulaire.date_notification  LIKE '".$annee."%')
				OR (t_contrat_titulaire.date_notification IS NULL AND t_contrat_titulaire.date_prevue_notification  LIKE '".$annee."%' ) ) "
        ;

        $statement = Atexo_Db::getLinkCommon()->query($sql);
        if ($statement) {
            while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
                //$typeEnveloppe = $row['type_depot-reponse'];
                //$idOffre = $row['id_offre'];
                //$nomEntreprise = "";

                $contratQuery = new CommonTContratTitulaireQuery(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                $contrat = $contratQuery->getTContratTitulaireById($row['id_contrat_titulaire']);

                $nomEntreprise = $contrat->getNomTitulaireContrat();

                //$connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
                $crit = new Criteria();
                $crit->add(CommonMarchePeer::ID_CONTRAT_TITULAIRE, $row['id_contrat_titulaire']);
                $crit->add(CommonMarchePeer::ORGANISME, $organisme);
                $marche = CommonMarchePeer::doSelectOne($crit, $connexionCom);

                if (!$marche) {
                    $marche = new CommonMarche();
                    $marche->setOrganisme($organisme);
                    $marche->setValide('0'); //on met à jour la valeur de validation uniquement si le marché n'existe pas, sinon on reprend la valeur précédente
                }
                $marche->setIdmarchetranchebudgetaire($row['tranche_budgetaire']);
                $marche->setNumeromarcheannee($annee);
                $marche->setIdservice($idService);
                $marche->setNomattributaire($nomEntreprise);
                $marche->setDatenotification($row['date_notification'] ?: $row['date_prevue_notification']);

                $marche->setCodepostal($row['code_postal']);
                $marche->setNaturemarche($row['categorie']);

                $marche->setObjetmarche($row['objet_contrat']);

                $marche->setIsmanuel('0');
                $marche->setPmepmi(null);
                $marche->setMontantmarche($row['montant_contrat']);
                //$marche->setIdDecisionEnveloppe($row['id_decision_enveloppe']);
                $marche->setIdContratTitulaire($row['id_contrat_titulaire']);

                $marche->setVille($row['ville']);
                $marche->setSirenAttributaire($row['siren']);
                $marche->setNicAttributaire($row['nicSiege']);
                $marche->setIdentifiantNationalAttributaire($row['sirenEtranger']);
                $marche->setRcVilleAttributaire(null);
                $marche->setRcNumAttributaire(null);
                $marche->setAcronymepaysAttributaire($row['paysenregistrement']);
                $marche->setPaysAttributaire($row['pays']);
                $marche->save($connexionCom);
            }
        }

        self::removeMarcheImportedWithoutDecision($idServiceQuery, $annee, $organisme);
    }

    /*
     * Recherche la liste des marchés notifiés en fonction des critères de recherche
     * @param: critères de recherche
     * @return: tableau contenant des informations sur les marchés notifiés
     */
    public function searchMarchesNotifies($criteriaVo, $returnJustNumberOfElement = false)
    {
        $queryParams = [];
        $keyWord = null;
        $otherCondition = '';
        if ($criteriaVo->getIdsService()) {
            $otherCondition .= ('0' != $criteriaVo->getIdsService()) ? ' AND C.service_id IN  '.$criteriaVo->getIdsService() : ' AND C.service_id = '.$criteriaVo->getIdsService();
        } else {
            $otherCondition .= ' AND C.service_id = :serviceId ';
            $queryParams[':serviceId'] = $criteriaVo->getIdService();
        }
        if ($criteriaVo->getNatureMarche()) {
            $otherCondition .= ' AND C.categorie = :categorieConsultation ';
            $queryParams[':categorieConsultation'] = $criteriaVo->getNatureMarche();
        }
        if ($criteriaVo->getDate1()) {
            $otherCondition .= ' AND D.date_notification_reelle >= :dateNotificationStart ';
            $queryParams[':dateNotificationStart'] = Atexo_Util::frnDate2iso($criteriaVo->getDate1());
        }
        if ($criteriaVo->getDate2()) {
            $otherCondition .= ' AND D.date_notification_reelle <= :dateNotificationEnd ';
            $queryParams[':dateNotificationEnd'] = Atexo_Util::frnDate2iso($criteriaVo->getDate2());
        }
        if ($criteriaVo->getMotclef()) {
            if (!str_contains($criteriaVo->getMotclef(), '"')) {
                $motCle = strtr($criteriaVo->getMotclef(), ",;.&!?'\"()[]{}°+*/\\|:%", '                      ');
                $arrayMotsCle = explode(' ', $motCle);
                $arrayMotInterdit = ' and or et ou le la les un une du de des a à au aux son sa ses ne ni non sauf ce ces cet je tu il elle on nous vous ils elles etc mon ma ton ta vos se y en sur ';
                $otherCondition .= ' AND ( ';
                foreach ($arrayMotsCle as $unMotCle) {
                    $unMotCle = trim((new Atexo_Db())->quote($unMotCle));
                    if ($unMotCle && !strstr($arrayMotInterdit, $unMotCle) && strlen($unMotCle) >= 2) {
                        if ($unMotCle == $arrayMotsCle[0]) {
                            $otherCondition .= " D.objet_marche LIKE '%".$unMotCle."%' ";
                        } else {
                            $otherCondition .= " OR D.objet_marche LIKE '%".$unMotCle."%' ";
                        }
                    }
                }
                $otherCondition .= ' ) ';
            } else {
                $arrayWorrd = explode('"', $criteriaVo->getMotclef());
                if (3 == count($arrayWorrd)) {
                    $keyWord = $arrayWorrd[1];
                }
                $otherCondition .= " AND D.objet_marche = '".$keyWord."'";
            }
        }
        $sql1 = ' SELECT D.siren_Attributaire, D.nic_Attributaire, D.identifiantNational_Attributaire, D.acronymePays_Attributaire, D.pays_Attributaire,D.rc_ville_attributaire,
 				   D.rc_num_attributaire,C.service_id, D.objet_marche, D.date_notification, D.date_notification_reelle, O.nom_entreprise_inscrit as nom_attributaire, D.code_postal as code_postal,
 				   D.ville  as ville,  ';
        $sql1 .= ' D.montant_marche, C.categorie, C.id_type_procedure_org , C.type_acces, C.code_cpv_1, C.code_cpv_2, D.type_enveloppe ';
        $sql1 .= ' FROM Offres O, decisionEnveloppe D, consultation C ';
        $sql1 .= ' WHERE O.id = D.id_offre AND D.consultation_id = C.id AND O.organisme = D.organisme AND D.organisme = C.organisme ';
        $sql1 .= " AND D.date_notification_reelle IS NOT NULL AND D.type_enveloppe = '1'";
        $sql1 .= " AND C.organisme='".$criteriaVo->getAcronymeOrganisme()."' ";
        if ($criteriaVo->getNomAttributaire()) {
            $sql1 .= " AND O.nom_entreprise_inscrit LIKE '%".(new Atexo_Db())->quote($criteriaVo->getNomAttributaire())."%' ";
        }
        $sql1 .= "  $otherCondition ";
        $sql2 = ' SELECT D.siren_Attributaire, D.nic_Attributaire, D.identifiantNational_Attributaire, D.acronymePays_Attributaire, D.pays_Attributaire, D.rc_ville_attributaire,
 				   D.rc_num_attributaire,C.service_id, D.objet_marche, D.date_notification, D.date_notification_reelle, P.nom_entreprise as nom_attributaire, D.code_postal as code_postal,
 				   D.ville  as ville, ';
        $sql2 .= ' D.montant_marche, C.categorie,C.id_type_procedure_org , C.type_acces, C.code_cpv_1, C.code_cpv_2, D.type_enveloppe ';
        $sql2 .= ' FROM Offre_papier P, decisionEnveloppe D, consultation C ';
        $sql2 .= ' WHERE P.id = D.id_offre AND D.consultation_id = C.id AND P.organisme = D.organisme AND D.organisme = C.organisme';
        $sql2 .= " AND D.date_notification_reelle IS NOT NULL AND D.type_enveloppe = '2' AND C.organisme='".$criteriaVo->getAcronymeOrganisme()."' ";
        if ($criteriaVo->getNomAttributaire()) {
            $sql2 .= " AND P.nom_entreprise LIKE '%".(new Atexo_Db())->quote($criteriaVo->getNomAttributaire())."%' ";
        }
        $sql2 .= "  $otherCondition ";
        $sql = " SELECT * FROM ( $sql1 UNION $sql2 )  res WHERE 1 = 1 ";
        if (!$returnJustNumberOfElement) {
            if ($criteriaVo->getSortByElement()) {
                $sql .= ' ORDER BY  '.$criteriaVo->getSortByElement().' '.$criteriaVo->getSensOrderBy();
            } else {
                $sql .= ' ORDER BY service_id ASC, date_notification_reelle ASC ';
            }
            if ($criteriaVo->getLimit() && !$criteriaVo->getExportXls()) {
                $sql .= ' limit '.$criteriaVo->getOffset().','.$criteriaVo->getLimit();
            }
        }
        $statement = Atexo_Db::getLinkCommon(true)->prepare($sql);
        $statement->execute($queryParams);
        $numberOfElement = 0;
        $i = 0;
        $arrayMarchesNotifies = [];
        while ($row = $statement->fetch(PDO::FETCH_ASSOC)) {
            if ($returnJustNumberOfElement) {
                ++$numberOfElement;
            } else {
                $arrayMarchesNotifies[$i]['service'] = $row['service_id'];
                $arrayMarchesNotifies[$i]['objet_marche'] = $row['objet_marche'];
                $arrayMarchesNotifies[$i]['date_notification'] = $row['date_notification_reelle'];
                $arrayMarchesNotifies[$i]['attributaire'] = $row['nom_attributaire'];
                $arrayMarchesNotifies[$i]['code_postal'] = $row['code_postal'];
                $arrayMarchesNotifies[$i]['ville'] = $row['ville'];
                $arrayMarchesNotifies[$i]['montant_ht'] = $row['montant_marche'];
                $arrayMarchesNotifies[$i]['nature_prestation'] = $row['categorie'];
                $arrayMarchesNotifies[$i]['id_type_procedure'] = $row['id_type_procedure_org'];
                $arrayMarchesNotifies[$i]['type_procedure'] = $row['type_acces'];
                $arrayMarchesNotifies[$i]['siren_Attributaire'] = $row['siren_Attributaire'];
                $arrayMarchesNotifies[$i]['nic_Attributaire'] = $row['nic_Attributaire'];
                $arrayMarchesNotifies[$i]['identifiantNational_Attributaire'] = $row['identifiantNational_Attributaire'];
                $arrayMarchesNotifies[$i]['acronymePays_Attributaire'] = $row['acronymePays_Attributaire'];
                $arrayMarchesNotifies[$i]['pays_Attributaire'] = $row['pays_Attributaire'];
                $arrayMarchesNotifies[$i]['rc_ville_attributaire'] = $row['rc_ville_attributaire'];
                $arrayMarchesNotifies[$i]['rc_num_attributaire'] = $row['rc_num_attributaire'];
                $arrayMarchesNotifies[$i]['organisme'] = $criteriaVo->getAcronymeOrganisme();
                $arrayCodesCpvSecondaires = '';
                if ($row['code_cpv_2']) {
                    $arrayCodesCpvSecondaires = explode('#', $row['code_cpv_2']);
                }
                $codesCpvSec = '';
                if ($arrayCodesCpvSecondaires && count($arrayCodesCpvSecondaires)) {
                    foreach ($arrayCodesCpvSecondaires as $cpv) {
                        if ($cpv) {
                            $codesCpvSec .= '<br/>'.$cpv;
                        }
                    }
                }
                $arrayMarchesNotifies[$i]['code_cpv'] = $row['code_cpv_1'].$codesCpvSec;
            }
            ++$i;
        }
        if ($returnJustNumberOfElement) {
            return $numberOfElement;
        }

        return $arrayMarchesNotifies;
    }

    /**
     * Permet de recuperer l'objet marche publie.
     *
     * @param string $organisme l'organisme
     * @param int    $idService l'id service
     * @param string $year      l'annee
     * @param null   $connexion
     *
     * @return CommonMarchePublie
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2016
     */
    public function getMarchePublieByYearAndOrganisme($organisme, $idService, $year, $connexion = null)
    {
        $marchePublieQuery = new CommonMarchePublieQuery();

        return $marchePublieQuery->getMarchePublieByYear($organisme, $idService, $year, $connexion);
    }

    /**
     * Permet de recuperer l'objet marche publie si il existe si nn le creer.
     *
     * @param $annee
     * @param $organisme
     * @param int  $idService
     * @param null $connexion
     *
     * @return CommonMarchePublie
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2018-esr
     *
     * @copyright Atexo 2018
     */
    public function creerMarchePublieIfNotExiste($annee, $organisme, $idService = 0, $connexion = null)
    {
        if (!$connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        }
        $marchePublieQuery = self::getMarchePublieByYearAndOrganisme($organisme, $idService, $annee, $connexion);
        if (!$marchePublieQuery) {
            $marchePublie = new CommonMarchePublie();
            $marchePublie->setOrganisme($organisme);
            $marchePublie->setServiceId($idService);
            $marchePublie->setNumeromarcheannee($annee);
            $marchePublie->setIspubliee('1');
            $marchePublie->save($connexion);
        }

        return $marchePublieQuery;
    }
}
