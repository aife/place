<?php

namespace Application\Service\Atexo\Serializer;

use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Rest\ObjectField\Atexo_Rest_ObjectField_DceField;
use Application\Service\Atexo\Rest\ObjectField\Atexo_Rest_ObjectField_PubliciteField;
use Application\Service\Atexo\Rest\ObjectField\Atexo_Rest_ObjectField_RcField;
use DOMDocument;
use DOMElement;
use Exception;
use Prado\Prado;
use Prado\Util\TLogger;

class Atexo_Serializer_Serializer
{
    private array $transformList = [];
    private array $escapeList = [];
    private array $optionnelList = [];
    private $instance = null;
    private $nameInstance = null;

    public const CLASSIGNORE = [
                Atexo_Rest_ObjectField_DceField::class,
                Atexo_Rest_ObjectField_RcField::class,
                Atexo_Rest_ObjectField_PubliciteField::class
    ];

	public function Serialize($ObjectInstance, $ClassName)
    {
        return $this->SerializeClass($ObjectInstance, $ClassName);
    }

    public function __construct($instance = null, $nameInstance = null)
    {
        $this->setInstance($instance);
        $this->setNameInstance($nameInstance);
    }

    public function SerializeClass($ObjectInstance, $ClassName, $paramList = '', $hideField = false, $optionnelList = false)
    {
        if ($paramList) {
            $this->transformList = self::getParam($paramList);
        }
        if ($hideField) {
            $this->escapeList = self::getParam($hideField);
        }
        if ($optionnelList) {
            $this->optionnelList = self::getParam($optionnelList);
        }
        if (isset($this->transformList[$ClassName])) {
            $xmlName = $this->transformList[$ClassName];
        } else {
            $xmlName = $ClassName;
        }
        $xml = new DOMDocument('1.0', 'UTF-8');
        $xml->formatOutput = true;
        $xml_elem = new DOMElement($xmlName);
        $xml->appendChild($xml_elem);
        self::SerializeClassToXml($ObjectInstance, $ClassName, $this->escapeList, $xml_elem, $ClassName);

        return $xml->saveXML();
    }

    private function SerializeClassToXml($ObjectInstance, $ClassName, $hideField, $xml, $rootXmlName)
    {
        $i = 0;
        $listeDom = [];
        $ClassArray = ((array) $ObjectInstance);
        foreach ($ClassArray as $prpName => $ClassMember) {
            $prpName = str_replace("\000", '', $prpName);
            $prpName = trim($prpName, ' *');
            if (isset($this->transformList[$prpName])) {
                $xmlName = $this->transformList[$prpName];
            } else {
                $xmlName = trim(str_replace(['*', $ClassName], '', $prpName));
            }
            $prpName = trim(str_replace(['*', $ClassName], '', $prpName));

			if(in_array($prpName,$hideField)
                && !in_array($ClassName, self::CLASSIGNORE) ) {
				continue;
			}
			$valeur = '';
            $prpType = gettype($ClassMember);
            $f = self::getFunctionGet($prpName);
            $valeur = $ObjectInstance->$f();
            $list = explode('.', $xmlName);
            $indexName = $list[0];
            $n = count($list);
            $xml_parent = $xml;
            if ($n > 1) {
                for ($j = 0; $j < $n - 1; ++$j) {
                    if (!isset($listeDom[$indexName][$list[$j]])) {
                        $listeDom[$indexName][$list[$j]] = new DOMElement($list[$j]);
                        $xml_parent->appendChild($listeDom[$indexName][$list[$j]]);
                    }
                    $xml_parent = $listeDom[$indexName][$list[$j]];
                }
                $xmlName = $list[$n - 1];
            }
            $className = '';
            if (is_object($valeur)) {
                $className = $valeur::class;
            }
            if (\stdClass::class == $className) {
                $stdArray = ((array) $valeur);
                $xml_child = new DOMElement($xmlName);
                $xml_parent->appendChild($xml_child);
                foreach ($stdArray as $key => $value) {
                    $xml_child->setAttribute($key, $value);
                }
                continue;
            }
            if ('object' == $prpType) {
                if (isset($this->transformList[$className])) {
                    $xmlClassName = $this->transformList[$className];
                } else {
                    $xmlClassName = $className;
                }
                $xml_elem = new DOMElement($xmlClassName);
                $xml_parent->appendChild($xml_elem);
                self::SerializeClassToXml($valeur, $className, $hideField, $xml_elem, $rootXmlName.'.'.$className);
                continue;
            }
            if ('array' == $prpType && is_array($valeur)) {
                $first = reset($valeur);
                $prpType = gettype($first);
                $xml_elem = new DOMElement($xmlName);
                $xml_parent->appendChild($xml_elem);
                if ('object' != $prpType) {
                    $xmlNameList = substr($xmlName, 0, strlen($xmlName) - 1);
                    foreach ($valeur as $oneValue) {
                        if (!in_array($rootXmlName.'.'.$prpName, $this->optionnelList) || ($oneValue && 'false' !== $oneValue)) {
                            if (self::hasCaracCDATA($oneValue)) {
                                $xml_child = $xml_parent->ownerDocument->createElement($xmlNameList);
                                $ct = $xml_parent->ownerDocument->createCDATASection($oneValue);
                                $xml_child->appendChild($ct);
                            } else {
                                $xml_child = new DOMElement($xmlNameList, $oneValue);
                            }
                            $xml_elem->appendChild($xml_child);
                        }
                    }
                    continue;
                }
                $className = get_class($first);

                foreach ($valeur as $object) {
                    if (isset($this->transformList[$className])) {
                        $classNameXml = $this->transformList[$className];
                    } else {
                        $classNameXml = $className;
                    }
                    $xml_elem_elem = new DOMElement($classNameXml);
                    $xml_elem->appendChild($xml_elem_elem);
                    try {
                        if (isset($this->transformList[$classNameXml])) {
                            $classNameXml = $this->transformList[$classNameXml];
                        }
                        $fGet = self::getFunctionGet($classNameXml);
                        $value = $object->$fGet();
                        $classNameObject = $value::class;
                        if (\stdClass::class == $classNameObject) {
                            $stdArrayObject = ((array) $value);
                            foreach ($stdArrayObject as $keyObject => $valueObject) {
                                $xml_elem_elem->setAttribute($keyObject, $valueObject);
                            }
                        }
                    } catch (Exception $e) {
                        Prado::log('Erreur --'.date('Y-m-d H:i:s').'--'.$e->getMessage().'-', TLogger::ERROR, 'ERROR');
                    }
                    self::SerializeClassToXml($object, $className, $hideField, $xml_elem_elem, $rootXmlName.'.'.$className);
                }
                continue;
            } elseif(
                !in_array($rootXmlName.".".$prpName,$this->optionnelList)
                || ($valeur && $valeur !== "false" && $valeur !== "" && !is_null($valeur) && !is_array($valeur))
            ) {
                try {
                    if (0 != strcmp(mb_detect_encoding($valeur, 'UTF-8'), 'UTF-8')) {
                        mb_convert_encoding($valeur, 'UTF-8');
                    }
                    //Pouvoir gerer des caractères spéciaux dans l'xml
                    if (self::hasCaracCDATA($valeur)) {
                        $xml_child = $xml_parent->ownerDocument->createElement($xmlName);
                        $ct = $xml_parent->ownerDocument->createCDATASection($valeur);
                        $xml_child->appendChild($ct);
                    } else {
                        $xml_child = new DOMElement($xmlName, $valeur);
                    }
                    $xml_parent->appendChild($xml_child);
                } catch (Exception $e) {
                    Prado::log('Erreur --'.date('Y-m-d H:i:s').'--'.$e->getMessage().'-', TLogger::ERROR, 'ERROR');
                }
            }
            ++$i;
        }
    }

    public function hasCaracCDATA($valeur)
    {
        return str_contains($valeur, '&') || str_contains($valeur, '<') || str_contains($valeur, '>') || str_contains($valeur, "'");
    }

    public static function getFunctionGet($prop)
    {
        $func = 'get';
        $names = explode('_', $prop);

        foreach ($names as $name) {
            $func .= ucfirst($name);
        }

        return $func;
    }

    public function WriteXmlFile($XmlData, $FilePath)
    {
        $Xml = simplexml_load_string($XmlData);
        $Doc = new DOMDocument();
        $Doc->loadXML($Xml->asXML());
        $Doc->save($FilePath);
    }

    public function DeserializeClass($content, $paramList = '')
    {
        $xml = simplexml_load_string($content);
        if ($paramList) {
            $this->transformList = self::getParam($paramList);
        }

        $Dom = new DomDocument();
        $Dom->load(Atexo_Config::getParameter('PATH_FILE_XSD_WS'));

        $root = $Dom->getElementsByTagName('schema');
        $dataXsd = self::shownode($root->item(0));

        return $this->Deserialize($xml, $dataXsd);
    }

    public function Deserialize($Root, $dataXsd, $paramList = '')
    {
        $nameInstance = $Root->getName();
        if (isset($this->transformList[$nameInstance])) {
            $nameInstance = $this->transformList[$nameInstance];
        }
        $instance = new \ReflectionClass($nameInstance);
        if ($this->getInstance() && $this->getNameInstance() == $nameInstance) {
            $ins = $this->getInstance();
        } else {
            $ins = $instance->newInstance();
        }
        //var_dump($ins);echo "\n\n";var_dump($nameInstance);echo "\n\n";var_dump($instance);echo "\n\n";exit;
        // A voir aprés
        $nameParent = $Root->getName();
        //      if($nameParent == "lot"){
        //          var_dump($instance);exit;
        //      }

        foreach ($Root->attributes() as $name => $value) {
            if (isset($this->transformList[$nameParent.$name])) {
                $name = $this->transformList[$nameParent.$name];
            }
            $rp = $instance->getMethod('set'.$name);
            $rp->invoke($ins, (new Atexo_Config())->toPfEncoding((string) $value));
        }
        foreach ($Root as $member) {
            $nameMember = $member->getName();
            foreach ($member->attributes() as $name => $value) {
                if (isset($this->transformList[$nameParent.$nameMember.$name])) {
                    $name = $this->transformList[$nameParent.$nameMember.$name];
                }
                $rp = $instance->getMethod('set'.$name);
                $rp->invoke($ins, (new Atexo_Config())->toPfEncoding((string) $value));
            }

            $name = $member->getName();
            $nameOriginal = $member->getName();
            if (isset($this->transformList[$nameParent.$name])) {
                $name = $this->transformList[$nameParent.$name];
            }
            if (0 == (is_countable($member->children()) ? count($member->children()) : 0)) {
                $elmentListe = (int) self::isList($dataXsd, $nameOriginal, $dataXsd);
                if (!$elmentListe) {
                    $rp = $instance->getMethod('set'.$name);
                    $rp->invoke($ins, (new Atexo_Config())->toPfEncoding((string) $member));
                }
                continue;
            }

            try {
                if (isset($this->transformList[$name])) {
                    $nameClass = $this->transformList[$name];
                } else {
                    $nameClass = $name;
                }
                //$newInstance = new ReflectionClass($nameClass);
                $rp = $instance->getMethod('set'.$name);
                $rp->invoke($ins, $this->Deserialize($member, $paramList));
                continue;
            } catch (Exception) {
                foreach ($member as $child) {
                    $method = 'add' . $name;
                    if( $method === 'addclausesSociales' || $method === 'addclausesEnvironnementales') {
                        continue;
                    }
                    $rp = $instance->getMethod($method);
                    if (
                        0 == (is_countable($child->children()) ? count($child->children()) : 0)
                        && 0 == (is_countable($child->attributes()) ? count($child->attributes()) : 0)
                    ) {
                        $rp->invoke($ins, (string) $child);
                    } else {
                        $rp->invoke($ins, $this->Deserialize($child, $paramList));
                    }
                }
            }
        }

        return $ins;
    }

    public function getParam($nameFile)
    {
        $list = [];
        $contenu_array = file($nameFile);
        foreach ($contenu_array as $ligne) {
            $tab = preg_split('#=#', $ligne);
            $list[str_replace('.', '', $tab[0])] = trim($tab[1]);
        }

        return $list;
    }

    public function shownode($root)
    {
        $data = [];
        foreach ($root->childNodes as $element) {
            if (self::hasChild($element) || XML_ELEMENT_NODE == $element->nodeType) {
                if ('' != $element->getAttribute('name')) {
                    $data[$element->getAttribute('name')]['elements'] = self::shownode($element);
                } else {
                    $data = self::shownode($element);
                }
                if ('' != $element->getAttribute('type')) {
                    $data[$element->getAttribute('name')]['type'] = $element->getAttribute('type');
                }
                if ('' != $element->getAttribute('minOccurs')) {
                    $data[$element->getAttribute('name')]['minOccurs'] = $element->getAttribute('minOccurs');
                }
                if ('' != $element->getAttribute('maxOccurs')) {
                    $data[$element->getAttribute('name')]['maxOccurs'] = $element->getAttribute('maxOccurs');
                }
            }
        }

        return $data;
    }

    public function hasChild($p)
    {
        if ($p->hasChildNodes()) {
            foreach ($p->childNodes as $c) {
                if (XML_ELEMENT_NODE == $c->nodeType) {
                    return true;
                }
            }
        }

        return false;
    }

    public function isList($data, $name, $allData)
    {
        if (is_array($data)) {
            foreach ($data as $key => $value) {
                if ($key == $name) {
                    if (str_contains($value['type'], 'c:')) {
                        $allDataElements = $allData[str_replace('c:', '', $value['type'])]['elements'];
                        if (is_array($allDataElements)) {
                            foreach ($allDataElements as $element) {
                                if ('unbounded' == $element['maxOccurs']) {
                                    return true;
                                }
                            }
                        }
                    }

                    return false;
                } else {
                    $elements = $value['elements'];
                    if (self::isList($elements, $name, $allData)) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    /**
     * @return null
     */
    public function getNameInstance()
    {
        return $this->nameInstance;
    }

    /**
     * @param null $nameInstance
     */
    public function setNameInstance($nameInstance)
    {
        $this->nameInstance = $nameInstance;
    }

    /**
     * @return null
     */
    public function getInstance()
    {
        return $this->instance;
    }

    /**
     * @param null $instance
     */
    public function setInstance($instance)
    {
        $this->instance = $instance;
    }
}
