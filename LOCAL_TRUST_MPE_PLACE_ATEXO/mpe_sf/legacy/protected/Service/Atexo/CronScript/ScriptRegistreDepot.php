<?php

namespace Application\Service\Atexo\CronScript;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonInscrit;
use Application\Propel\Mpe\CommonOffresPeer;
use Application\Propel\Mpe\CommonOrganismePeer;
use Application\Propel\Mpe\Entreprise;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_Entreprise;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Inscrit;

/*
 * Created on 21 mai 2009
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
class Atexo_CronScript_ScriptRegistreDepot
{
    public static function runScriptRegistreDepot()
    {
        $commonConnexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        if (isset($_GET['organisme'])) {
            $c->add(CommonOrganismePeer::ACRONYME, Atexo_Util::atexoHtmlEntities($_GET['organisme']));
        }
        $organismes = CommonOrganismePeer::doSelect($c, $commonConnexion);
        $texte = '';
        if (is_array($organismes)) {
            foreach ($organismes as $organisme) {
                $totalErreur = 0;
                $totalSucces = 0;

                $texte .= "\n\n=> Organisme : ".$organisme->getAcronyme()."\n";

                $c = new Criteria();
                $c->add(CommonOffresPeer::ORGANISME, $organisme->getAcronyme());
                $offres = CommonOffresPeer::doSelect($c, $commonConnexion);
                if (is_array($offres)) {
                    foreach ($offres as $offre) {
                        $entreprise = (new Atexo_Entreprise())->retrieveCompanyBdeById($offre->getEntrepriseId());
                        $inscrit = (new Atexo_Entreprise_Inscrit())->retrieveInscritById($offre->getInscritId());

                        if (!$entreprise || !$inscrit instanceof CommonInscrit) {
                            if (!($entreprise)) {
                                $entreprise = new Entreprise();
                                $entreprise->setNom('Inconnu');
                                $entreprise->setNom('Inconnu');
                                $entreprise->setSiren('000000000');
                                $entreprise->setNicsiege('00000');
                                $entreprise->setSirenetranger('00000000000000');

                                $texte .= '-> Offre numéro '.$offre->getId().' : Entreprise '.$offre->getEntrepriseId()." non trouvé. Valeurs par défaut inséré. \n";
                            }
                            if (!$inscrit instanceof CommonInscrit) {
                                $inscrit = new CommonInscrit();
                                $inscrit->setNom('Inconnu');
                                $inscrit->setPrenom('Inconnu');
                                $inscrit->setAdresse('Inconnu');
                                $inscrit->setTelephone('000000000');
                                $inscrit->setFax('000000000');
                                $inscrit->setCodepostal('00000');
                                $inscrit->setVille('Ville');
                                $inscrit->setPays('Pays');
                                $inscrit->setEmail('inconnu@mail');
                                $inscrit->setSiret('00000');

                                $texte .= '-> Offre numéro '.$offre->getId().' : inscrit '.$offre->getEntrepriseId()." non trouvé. Valeurs par défaut inséré. \n";
                            }
                            //  $totalErreur++;
                        }

                        $sql = "UPDATE `Offres` SET `nom_entreprise_inscrit` = '".addslashes($entreprise->getNom())."', `nom_inscrit` = '".addslashes($inscrit->getNom())."', "
                            ." `prenom_inscrit` = '".addslashes($inscrit->getPrenom())."', `adresse_inscrit` = '".addslashes($inscrit->getAdresse())."', "
                            ." `adresse2_inscrit` = '".addslashes($inscrit->getAdresse2())."', `telephone_inscrit` = '".addslashes($inscrit->getTelephone())."', "
                            ." `fax_inscrit` = '".addslashes($inscrit->getFax())."', `code_postal_inscrit` = '".addslashes($inscrit->getCodepostal())."', "
                            ." `ville_inscrit` = '".addslashes($inscrit->getVille())."', `pays_inscrit` = '".addslashes($inscrit->getPays())."', "
                            ." `acronyme_pays` = '".addslashes($entreprise->getAcronymePays())."', `siret_entreprise` = '".addslashes($entreprise->getSiren().$entreprise->getNicsiege())."', "
                            ." `identifiant_national` = '".addslashes($entreprise->getSirenEtranger())."', `email_inscrit` = '".addslashes($inscrit->getEmail())."', "
                            ." `siret_inscrit` = '".addslashes($entreprise->getSiren().$inscrit->getSiret())."' WHERE  `Offres`.id='".$offre->getId()."' AND `Offres`.organisme LIKE '".$organisme->getAcronyme()."' ";

                        Atexo_Db::getLinkCommon()->query($sql);
                        ++$totalSucces;
                    }
                }

                $texte .= '=>Bilan : '.$totalErreur.' erreur rencontrée et '.$totalSucces." offres màj\n";
            }
        }

        return $texte;
    }
}
