<?php

namespace Application\Service\Atexo;

use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonAdministrateurPeer;
use Application\Propel\Mpe\CommonAgent;
use Application\Propel\Mpe\CommonAgentServiceMetier;
use Application\Propel\Mpe\CommonCategorieLotPeer;
use Application\Propel\Mpe\CommonConfigurationOrganisme;
use Application\Propel\Mpe\CommonOrganisme;
use Application\Propel\Mpe\CommonOrganismePeer;
use Application\Propel\Mpe\CommonOrganismeServiceMetier;
use Application\Propel\Mpe\CommonOrganismeServiceMetierPeer;
use Application\Propel\Mpe\CommonSocleHabilitationAgent;
use Application\Propel\Mpe\Om\BaseCommonOrganismePeer;
use Application\Service\Atexo\Agent\Atexo_Agent_EnablingsAgents;
use Application\Service\Atexo\Blob\Atexo_Blob_DbManagement;
use Application\Service\Atexo\Organisme\Atexo_Organisme_CriteriaVo;
use Application\Service\Atexo\Rest\Action\Atexo_Rest_Action_Actions;
use Application\Service\Atexo\Rest\Action\Atexo_Rest_Action_LogoField;
use Application\Service\Atexo\Socle\Atexo_Socle_AgentServicesMetiers;
use PDO;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Atexo_Organismes
{
    const IS_ACTIVE = 1;
    const IS_INACTIVE = 0;
    /**
     * retourne la liste des organismes.
     *
     * @param $returnPropelObject : par defaut à true si l'objet retourné doir être un tableau d'objet propel, à false
     * si l'objet retourné doit être un tableau de type array [id catégorie] = libelle
     *
     * @return les categories
     */
    public static function retrieveOrganismes($returnPropelObject = true, $langue = null, $organismesActifsSeulement = true, $codeInsee = null, $justAccronyme = false, $withCPVille = false)
    {
        $arrayAcronyme = [];
        $allOrganismes = CommonOrganismePeer::retrieveAllOrganismes();

        $organismesTemp = [];
        if ($organismesActifsSeulement) {
            foreach ($allOrganismes as $org) {
                if ('1' == $org->getActive()) {
                    $organismesTemp[] = $org;
                }
            }
            $allOrganismes = $organismesTemp;
        }

        $organismesTemp = [];
        if ($codeInsee) {
            foreach ($allOrganismes as $org) {
                if ($org->getCategorieInsee() == $codeInsee) {
                    $organismesTemp[] = $org;
                }
            }
            $allOrganismes = $organismesTemp;
        }

        $organismes = $allOrganismes;

        if ($organismes) {
            if ($justAccronyme) {
                foreach ($organismes as $oneOrg) {
                    $arrayAcronyme[] = $oneOrg->getAcronyme();
                }

                return $arrayAcronyme;
            }
            if ($returnPropelObject) {
                return $organismes;
            } else {
                $organismesArray = [];
                foreach ($organismes as $oneOrg) {
                    if (!$langue) {
                        if ($withCPVille) {
                            $organismesArray[$oneOrg->getAcronyme()] = $oneOrg->getDenominationOrg().' ('.$oneOrg->getCp().' - '.$oneOrg->getVille().')';
                        } else {
                            $organismesArray[$oneOrg->getAcronyme()] = $oneOrg->getDenominationOrg();
                        }
                    } else {
                        $denominationOrganisme = '';
                        $villeOrganisme = '';
                        $cpOrganisme = '';
                        $result = '';

                        $getDenominationOrg = 'getDenominationOrg'.Atexo_Languages::getLanguageAbbreviation($langue);
                        if ($langue == Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')
                               || !method_exists($oneOrg, $getDenominationOrg) || !$oneOrg->$getDenominationOrg()) {
                            $denominationOrganisme = $oneOrg->getDenominationOrg();
                        } else {
                            $denominationOrganisme = $oneOrg->$getDenominationOrg();
                        }

                        if ($withCPVille) {
                            $getVilleOrg = 'getVille'.Atexo_Languages::getLanguageAbbreviation($langue);

                            if ($langue == Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')
                               || !method_exists($oneOrg, $getVilleOrg) || !$oneOrg->$getVilleOrg()) {
                                $villeOrganisme = $oneOrg->getVille();
                            } else {
                                $villeOrganisme = $oneOrg->$getVilleOrg();
                            }

                            $result = $denominationOrganisme.' ('.$oneOrg->getCp().' - '.$villeOrganisme.')';
                        } else {
                            $result = $denominationOrganisme;
                        }

                        $organismesArray[$oneOrg->getAcronyme()] = $result;
                    }
                }

                return $organismesArray;
            }
        } else {
            return [];
        }
    }

    /**
     * retourne l'acronyme de l'organisme donnée en paramètre.
     *
     * @param $idOrganisme : id de l'organisme
     *
     * @return string : accronyme
     */
    public function retrieveAccronymeOrganisme($idOrganisme)
    {
        try {
            $connexionCommon = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            $c = new Criteria();
            $c->add(CommonOrganismePeer::ID, $idOrganisme, Criteria::EQUAL);
            $organisme = CommonOrganismePeer::doSelectOne($c, $connexionCommon);
            if ($organisme) {
                return $organisme->getAcronyme();
            } else {
                return false;
            }
        } catch (\Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('app');
            $logger->error("Erreur : idOrganisme = $idOrganisme \n\nErreur : ".$e->getMessage());
        }
    }

    /**
     * retourne la denimination de l'organisme sinon false.
     *
     * @param $acronyme l'acronyme de l'organisme
     *
     * @return CommonOrganisme : $organisme
     */
    public static function retrieveOrganismeByAcronyme($acronyme, $activeSeulement = false)
    {
        try {
            return CommonOrganismePeer::retrieveOrganismeByAcronyme($acronyme);
        } catch (\Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('app');
            $logger->error("Erreur : organisme = $acronyme , activeSeulement = $activeSeulement \n\nErreur : ".$e->getMessage());
        }
    }

    /**
     *  exemple : array('id1','id2');.
     *
     * @param array<ids> : $arrayAcronymes
     *
     * @return array(CommonOrganisme)
     *
     * @deprecated il faut changer le nom de la fonction car elle prend en parametre les ids au lieu des acronymes organisme
     */
    public static function retrieveArrayOrganismesByAcronyme($arrayAcronymes)
    {
        $connexionCommon = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonOrganismePeer::ID, $arrayAcronymes, Criteria::IN);
        $organismes = CommonOrganismePeer::doSelect($c, $connexionCommon);
        if ($organismes) {
            return $organismes;
        } else {
            return [];
        }
    }

    /**
     * retourne la liste des organismes actives.
     *
     * @param $returnPropelObject : par defaut à true si l'objet retourné doir être un tableau d'objet propel, à false
     * si l'objet retourné doit être un tableau de type array [id catégorie] = libelle
     *
     * @return les categories
     */
    public function retrieveActiveOrganismes($returnPropelObject = false, $active = false, $affichageEntite = false, $codeInsee = null)
    {
        $allOrganismes = CommonOrganismePeer::retrieveAllOrganismes();

        $organismesTemp = [];
        if ($active) {
            foreach ($allOrganismes as $org) {
                if ('1' == $org->getActive()) {
                    $organismesTemp[] = $org;
                }
            }
            $allOrganismes = $organismesTemp;
        }

        $organismesTemp = [];
        if ($affichageEntite) {
            foreach ($allOrganismes as $org) {
                if ('1' == $org->getAffichageEntite()) {
                    $organismesTemp[] = $org;
                }
            }
            $allOrganismes = $organismesTemp;
        }

        $organismesTemp = [];
        if ($codeInsee) {
            foreach ($allOrganismes as $org) {
                if ($org->getCategorieInsee() == $codeInsee) {
                    $organismesTemp[] = $org;
                }
            }
            $allOrganismes = $organismesTemp;
        }

        $organismes = $allOrganismes;
        if ($organismes) {
            if ($returnPropelObject) {
                return $organismes;
            } else {
                $organismesArray = [];
                foreach ($organismes as $oneOrg) {
                    $organismesArray[$oneOrg->getAcronyme()] = $oneOrg->getDenominationOrg();
                }

                return $organismesArray;
            }
        } else {
            return [];
        }
    }

    /**
     * retourne tout les organismes.
     */
    public function retrieveAllEntity()
    {
        return CommonOrganismePeer::retrieveAllOrganismes();
    }

    public function update($acronyme, $sigle, $denomination, $article, $insee, array $infos, $listesServices = false)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        try {
            $organisme = self::retrieveOrganismeByAcronyme($acronyme);
            $organisme->setSigle($sigle);
            $organisme->setDenominationOrg($denomination);
            if ('' != $article && $article > 0) {
                $organisme->setTypeArticleOrg($article);
            }
            if ('' != $insee && $insee > 0) {
                $organisme->setCategorieInsee($insee);
            }
            if (isset($infos) && count($infos) > 0) {
                foreach ($infos as $key => $value) {
                    switch ($key) {
                        case 'description':
                            $organisme->setDescriptionOrg($value);
                            break;
                        case 'email':
                            $organisme->setEmail(trim($value));
                            break;
                        case 'adresse':
                            $organisme->setAdresse($value);
                            break;
                        case 'adresse2':
                            $organisme->setAdresse2($value);
                            break;
                        case 'cp':
                            $organisme->setCp($value);
                            break;
                        case 'ville':
                            $organisme->setVille($value);
                            break;
                        case 'pays':
                            $organisme->setPays($value);
                            break;
                        case 'tel':
                            $organisme->setTel($value);
                            break;
                        case 'telecopie':
                            $organisme->setTelecopie($value);
                            break;
                        case 'pays_ar':
                            $organisme->setPaysAr($value);
                            break;
                        case 'ville_ar':
                            $organisme->setVilleAr($value);
                            break;
                        case 'adresse_ar':
                            $organisme->setAdresseAr($value);
                            break;
                        case 'adresse2_ar':
                            $organisme->setAdresse2Ar($value);
                            break;
                        case 'denomination_ar':
                            $organisme->setDenominationOrgAr($value);
                            break;
                        case 'description_ar':
                            $organisme->setDescriptionOrgAr($value);
                            break;
                        case 'url':
                            $organisme->setUrl($value);
                            break;
                        case 'siren':
                            $organisme->setSiren($value);
                            break;
                        case 'siret':
                            $organisme->setComplement($value);
                            break;
                        case 'pf_url':
                            $organisme->setPfUrl($value);
                            break;
                        case 'sousTypeOrg':
                            $organisme->setSousTypeOrganisme($value);
                            break;
                        case 'idEntite':
                            $organisme->setIdEntite($value);
                            break;
                    }
                }
            }
            $organisme->save($connexionCom);
            if ($listesServices && is_array($listesServices)) {
                $idsServisOrganisme = self::getIdServiceMetierByOrganisme($acronyme, $connexionCom);
                foreach ($listesServices as $idService => $isChecked) {
                    if ($isChecked && !in_array($idService, $idsServisOrganisme)) {
                        // Ajouter le services metier pour l'organisme
                        $organismeService = new CommonOrganismeServiceMetier();
                        $organismeService->setOrganisme($acronyme);
                        $organismeService->setIdServiceMetier($idService);
                        $organismeService->save($connexionCom);
                    } elseif (!$isChecked && in_array($idService, $idsServisOrganisme)) {
                        // Supprimer le services metier pour les agents
                        $listeAgentsServiceMetiers = (new Atexo_Socle_AgentServicesMetiers())->retriveListeAgentsByOrgServiceMetier($acronyme, $idService, $connexionCom);
                        if (is_array($listeAgentsServiceMetiers)) {
                            foreach ($listeAgentsServiceMetiers as $agentServiceMetier) {
                                $agentServiceMetier->delete($connexionCom);
                            }
                        }
                        // Supprimer le services metier pour l'organisme
                        $c = new Criteria();
                        $c->add(CommonOrganismeServiceMetierPeer::ID_SERVICE_METIER, $idService);
                        $c->add(CommonOrganismeServiceMetierPeer::ORGANISME, $acronyme);
                        CommonCategorieLotPeer::doDelete($c, $connexionCom);
                    }
                }
            }
        } catch (\Exception $e) {
            $connexionCom->rollback();
            echo $e->__toString();
            throw new Atexo_Exception($e->getMessage());
        }
    }

    /**
     * Création d'un organisme (BDD + dossier de stockage).
     *
     * @author LCH
     *
     * @param string $acronyme     Acronyme de l'organisme préfixé
     * @param string $sigle        Sigle de l'organisme
     * @param string $denomination Description de l'organisme
     * @param int    $article      ID de l'article (0 => rien, 1 => le, 2 => la) ?
     * @param string $insee        Code INSEE
     * @param array  $infos        Tableau associatif des autres champs
     */
    public function create($acronyme, $sigle, $denomination, $article, $insee, array $infos, $fromMigration = false, $idAdmin = null, $active = true, $listeIdServices = false)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        try {
            $connexionCom->beginTransaction();

            $organisme = new CommonOrganisme();
            $organisme->setAcronyme($acronyme);
            $organisme->setSigle($sigle);
            $organisme->setDenominationOrg($denomination);
            $organisme->setDateCreation(date('Y-m-d H:i:s', time()));
            $organisme->setTypeArticleOrg($article);
            $organisme->setCategorieInsee($insee);
            $marchePublie = (new Atexo_Marche())->creerMarchePublieIfNotExiste(date('Y'), $acronyme, 0, $connexionCom);
            if (false === $active) {
                $organisme->setActive('0');
            }
            if (isset($infos) && count($infos) > 0) {
                foreach ($infos as $key => $value) {
                    switch ($key) {
                        case 'description':
                            $organisme->setDescriptionOrg($value);
                            break;
                        case 'email':
                            $organisme->setEmail(trim($value));
                            break;
                        case 'adresse':
                            $organisme->setAdresse($value);
                            break;
                        case 'adresse2':
                            $organisme->setAdresse2($value);
                            break;
                        case 'cp':
                            $organisme->setCp($value);
                            break;
                        case 'ville':
                            $organisme->setVille($value);
                            break;
                        case 'pays':
                            $organisme->setPays($value);
                            break;
                        case 'tel':
                            $organisme->setTel($value);
                            break;
                        case 'telecopie':
                            $organisme->setTelecopie($value);
                            break;
                        case 'description_ar':
                            $organisme->setDescriptionOrgAr($value);
                            break;
                        case 'adresse_ar':
                            $organisme->setAdresseAr($value);
                            break;
                        case 'adresse2_ar':
                            $organisme->setAdresse2Ar($value);
                            break;
                        case 'ville_ar':
                            $organisme->setVilleAr($value);
                            break;
                        case 'pays_ar':
                            $organisme->setPaysAr($value);
                            break;
                        case 'denomination_ar':
                            $organisme->setDenominationOrgAr($value);
                            break;
                        case 'url':
                            $organisme->setUrl($value);
                            break;
                        case 'siren':
                            $organisme->setSiren($value);
                            break;
                        case 'siret':
                            $organisme->setComplement($value);
                            break;
                        case 'sousTypeOrg':
                            $organisme->setSousTypeOrganisme($value);
                            break;
                        case 'idEntite':
                            $organisme->setIdEntite($value);
                            break;
                    }
                }
            }
            $organisme->save($connexionCom);
            self::copierConfigurationOrganisme(Atexo_Config::getParameter('ORGANISME_REFERENCE'), $acronyme, $connexionCom);
			//initialisation des tables referentielles
			$sql_script_orga = dirname(__DIR__) . '/../scripts-sql/init/scriptOrg.sql';
            preg_match_all("/[\n]{1}((INSERT|UPDATE).*;)[ ]?[\n]{0,1}/Uxs", file_get_contents($sql_script_orga), $sql_ORGA);
            foreach ($sql_ORGA[1] as $query) {
                try {
                    $query = str_replace('##ACCRONYME_NEW_ORG##', $acronyme, $query);
                    if ('utf8' == Atexo_Config::getParameter('DB_ENCODING')) {
                        $query = Atexo_Util::toUtf8($query);
                    }
                    Atexo_Db::getLinkCommon()->query($query);
                } catch (\Exception $e) {
                    //echo $e->getMessage();
                    Prado::log('Erreur Organismes.php'.$e->getMessage().$e->getTraceAsString(), TLogger::ERROR, 'Organismes.php');
                }
            }

            //Si a1a prendre ses valeurs comme valeurs par defaut
            $is_a1a = Atexo_Db::getLinkCommon()->query("SELECT acronyme FROM Organisme WHERE acronyme = '".Atexo_Config::getParameter('ORGANISME_REFERENCE')."'")->fetch(PDO::FETCH_ASSOC);
            if ($acronyme != Atexo_Config::getParameter('ORGANISME_REFERENCE') && $is_a1a['acronyme'] == Atexo_Config::getParameter('ORGANISME_REFERENCE')) {
                Atexo_Db::getLinkCommon()->query("DELETE FROM `t_calendrier_etape_referentiel` WHERE ORGANISME = '".$acronyme."';");
                Atexo_Db::getLinkCommon()->query("DELETE FROM `t_calendrier_transition_referentiel` WHERE ORGANISME = '".$acronyme."';");
                Atexo_Db::getLinkCommon()->query("DELETE FROM `ProcedureEquivalence` WHERE organisme = '".$acronyme."';");
                Atexo_Db::getLinkCommon()->query("DELETE FROM `Type_Procedure_Organisme` WHERE organisme = '".$acronyme."' ;");
                Atexo_Db::getLinkCommon()->query("DELETE FROM `Tranche_Article_133` WHERE acronyme_org = '".$acronyme."' ;");
                Atexo_Db::getLinkCommon()->query("DELETE FROM `t_type_contrat_et_procedure` WHERE organisme = '".$acronyme."' ;");
                $sql = "SELECT * FROM Type_Procedure_Organisme WHERE organisme = '".Atexo_Config::getParameter('ORGANISME_REFERENCE')."' ";
                $statement = Atexo_Db::getLinkCommon()->query($sql, PDO::FETCH_ASSOC);
                foreach ($statement as $res) {
                    $res['organisme'] = $acronyme;
                    $champs = '';
                    $valeurs = '';
                    $i = 0;
                    foreach ($res as $key => $value) {
                        if ($i > 0) {
                            $champs .= ',';
                            $valeurs .= ',';
                        }
                        $champs .= '`'.$key.'`';

                        if ($value !== null) {
                            $valeurs .= "'" . addslashes($value) . "'";
                        } else {
                            $valeurs .= "null";
                        }

                        ++$i;
                    }
                    $req = 'insert into Type_Procedure_Organisme ('.$champs.') VALUES ('.$valeurs.')';
                    Atexo_Db::getLinkCommon()->query($req);
                }
                //prendre valeur par defaut "Tranche_Article_133"
                $sql = "SELECT * FROM Tranche_Article_133 WHERE acronyme_org = '".Atexo_Config::getParameter('ORGANISME_REFERENCE')."' ";
                $statement = Atexo_Db::getLinkCommon()->query($sql, PDO::FETCH_ASSOC);
                foreach ($statement as $res) {
                    $res['acronyme_org'] = $acronyme;
                    $res['id'] = null;
                    $champs = '';
                    $valeurs = '';
                    $i = 0;
                    foreach ($res as $key => $value) {
                        if ($i > 0) {
                            $champs .= ',';
                            $valeurs .= ',';
                        }
                        $champs .= '`'.$key.'`';
                        $valeurs .= "'".addslashes($value)."'";
                        ++$i;
                    }
                    $req = 'insert into Tranche_Article_133 ('.$champs.') VALUES ('.$valeurs.')';
                    Atexo_Db::getLinkCommon()->query($req);
                }

                // creation des referentiels calendrier etapes et transitions
                $requete = "SELECT * FROM t_calendrier_etape_referentiel WHERE ORGANISME = '".Atexo_Config::getParameter('ORGANISME_REFERENCE')."' ";
                $statement = Atexo_Db::getLinkCommon()->query($requete, PDO::FETCH_ASSOC);
                foreach ($statement as $res) {
                    $res['ORGANISME'] = $acronyme;
                    $champs = '';
                    $valeurs = '';
                    $i = 0;
                    foreach ($res as $key => $value) {
                        if ($i > 0) {
                            $champs .= ',';
                            $valeurs .= ',';
                        }
                        $champs .= '`'.$key.'`';
                        $valeurs .= "'".addslashes($value)."'";
                        ++$i;
                    }
                    $reqEtape = 'insert into t_calendrier_etape_referentiel ('.$champs.') VALUES ('.$valeurs.')';
                    Atexo_Db::getLinkCommon()->query($reqEtape);
                }
                // insertions des transitions
                $requeteTransition = "SELECT * FROM t_calendrier_transition_referentiel WHERE ORGANISME = '".Atexo_Config::getParameter('ORGANISME_REFERENCE')."' ";
                $statementTransition = Atexo_Db::getLinkCommon()->query($requeteTransition, PDO::FETCH_ASSOC);
                foreach ($statementTransition as $res) {
                    $res['ORGANISME'] = $acronyme;
                    $champs = '';
                    $valeurs = '';
                    $i = 0;
                    foreach ($res as $key => $value) {
                        if ($i > 0) {
                            $champs .= ',';
                            $valeurs .= ',';
                        }
                        $champs .= '`'.$key.'`';
                        $valeurs .= "'".addslashes($value)."'";
                        ++$i;
                    }
                    $reqTransition = 'insert into t_calendrier_transition_referentiel ('.$champs.') VALUES ('.$valeurs.')';
                    Atexo_Db::getLinkCommon()->query($reqTransition);
                }

                // insertions des t_type_contrat_et_procedure
                $requeteTypeContrat = "SELECT `id_type_contrat`,`id_type_procedure`,`organisme` FROM t_type_contrat_et_procedure WHERE organisme = '".Atexo_Config::getParameter('ORGANISME_REFERENCE')."' ";
                $statementTypeContrat = Atexo_Db::getLinkCommon()->query($requeteTypeContrat, PDO::FETCH_ASSOC);
                foreach ($statementTypeContrat as $res) {
                    $res['organisme'] = $acronyme;
                    $champs = '';
                    $valeurs = '';
                    $i = 0;
                    foreach ($res as $key => $value) {
                        if ($i > 0) {
                            $champs .= ',';
                            $valeurs .= ',';
                        }
                        $champs .= '`'.$key.'`';
                        $valeurs .= "'".addslashes($value)."'";
                        ++$i;
                    }
                    $reqTypeContrat = 'insert into t_type_contrat_et_procedure ('.$champs.') VALUES ('.$valeurs.')';
                    Atexo_Db::getLinkCommon()->query($reqTypeContrat);
                }
            }
            Atexo_Db::closeCommon();
            // On test si le service metier "SOCLE" est en base pour l'affecter aux admins si c'est le cas
            $is_socle = false;
            $socle = Atexo_Db::getLinkCommon()->query('SELECT id, sigle FROM Service_Mertier WHERE id = 2')->fetch(PDO::FETCH_ASSOC);
            if ('SOCLE' == $socle['sigle']) {
                $is_socle = true;
            }
            // Si l'organisme A1A existe, on ajoute les memes services au nouvel organisme
            $is_a1a = Atexo_Db::getLinkCommon()->query("SELECT acronyme FROM Organisme WHERE acronyme = '".Atexo_Config::getParameter('ORGANISME_REFERENCE')."'")->fetch(PDO::FETCH_ASSOC);
            if ($acronyme != Atexo_Config::getParameter('ORGANISME_REFERENCE') && $is_a1a['acronyme'] == Atexo_Config::getParameter('ORGANISME_REFERENCE')) {
                $c = new Criteria();
                $c->add(CommonOrganismeServiceMetierPeer::ORGANISME, Atexo_Config::getParameter('ORGANISME_REFERENCE'), Criteria::EQUAL);
                $commonOrganismeServiceMetierPeer = CommonOrganismeServiceMetierPeer::doSelect($c, $connexionCom);
                for ($i = 0; $i < count($commonOrganismeServiceMetierPeer); ++$i) {
                    $commonOrganismeServiceMetier = new CommonOrganismeServiceMetier();
                    $commonOrganismeServiceMetier->setIdServiceMetier($commonOrganismeServiceMetierPeer[$i]->getIdServiceMetier());
                    $commonOrganismeServiceMetier->setOrganisme($acronyme);
                    $commonOrganismeServiceMetier->save($connexionCom);
                }
            } else {
                if (true === $is_socle) {
                    $organismeServiceMetier = new CommonOrganismeServiceMetier();
                    $organismeServiceMetier->setOrganisme($acronyme);
                    $organismeServiceMetier->setIdServiceMetier(Atexo_Config::getParameter('SERVICE_METIER_SOCLE'));
                    $organismeServiceMetier->save($connexionCom);
                    $idServiceExclure = Atexo_Config::getParameter('SERVICE_METIER_SOCLE');
                } else {
                    // Si il n'y a pas a1a, on ajoute au moins MPE (voir le socle s'il y a)
                    $organismeServiceMetier = new CommonOrganismeServiceMetier();
                    $organismeServiceMetier->setOrganisme($acronyme);
                    $organismeServiceMetier->setIdServiceMetier(Atexo_Config::getParameter('SERVICE_METIER_MPE'));
                    $organismeServiceMetier->save($connexionCom);
                    $idServiceExclure = Atexo_Config::getParameter('SERVICE_METIER_MPE');
                }
            }
            // Ajouter les services metier pour l'organisme
            if ($listeIdServices && is_array($listeIdServices)) {
                foreach ($listeIdServices as $idService) {
                    if ($idService != $idServiceExclure) {
                        $organismeService = new CommonOrganismeServiceMetier();
                        $organismeService->setOrganisme($acronyme);
                        $organismeService->setIdServiceMetier($idService);
                        $organismeService->save($connexionCom);
                    }
                }
            }

            if (1 == $organisme->getPrimaryKey()) {
                $agent = new CommonAgent();
                $agent->setLogin('hyperadmin');
                $agent->setPassword('b1ec4e52da9742d5098dfffe89a1f17647030d8f');
                $agent->setEmail('suivi.mpe@atexo.com');
                $agent->setNom('Admin');
                $agent->setPrenom('Hyper');
                $agent->setOrganisme($acronyme);
                $agent->setDateModification(date('Y-m-d H:i:s'));
                $agent->setDateCreation(date('Y-m-d H:i:s'));
                $agent->save($connexionCom);

                $habilitationAgent = Atexo_Agent_EnablingsAgents::addOrModifyEnablingsOfTheAgents(1);
                $habilitationAgent->setHyperAdmin('1');
                $habilitationAgent->setAdministrerCompte('1');
                $habilitationAgent->setGestionAgentPole('1');
                $habilitationAgent->setGestionAgents('1');
                $habilitationAgent->setGestionBiCles('1');
                $habilitationAgent->setGestionHabilitations('1');
                $habilitationAgent->setGestionTypeValidation('1');
                $habilitationAgent->setDroitGestionServices('1');
                $habilitationAgent->setGestionCompteJal('1');
                $habilitationAgent->setGestionCompteBoamp('1');
                $habilitationAgent->setAdministrerProcedure('1');
                $habilitationAgent->setAdministrerProceduresFormalisees('1');
                $habilitationAgent->setGestionMapa('1');
                $habilitationAgent->setIdAgent($agent->getId());
                $habilitationAgent->save($connexionCom);

                $socleHabilitationAgent = new CommonSocleHabilitationAgent();
                $socleHabilitationAgent->setIdAgent($agent->getId());
                $socleHabilitationAgent->setGestionAgentPoleSocle(1);
                $socleHabilitationAgent->setGestionAgentsSocle(1);
                $socleHabilitationAgent->setDroitGestionServicesSocle(1);
                $socleHabilitationAgent->save($connexionCom);

                $agentServiceMetier = new CommonAgentServiceMetier();
                $agentServiceMetier->setIdAgent($agent->getId());
                $agentServiceMetier->setIdServiceMetier(1);
                $agentServiceMetier->setIdProfilService(1);
                $agentServiceMetier->save($connexionCom);

                if (true === $is_socle) {
                    $agentServiceMetier = new CommonAgentServiceMetier();
                    $agentServiceMetier->setIdAgent($agent->getId());
                    $agentServiceMetier->setIdServiceMetier(2);
                    $agentServiceMetier->setIdProfilService(12);
                    $agentServiceMetier->save($connexionCom);
                }
            }

            $agent = new CommonAgent();
            $agent->setLogin('admin_'.$acronyme);
            Atexo_Agent::updateNewAgentPasswordByArgon2($agent, 'admin_'.$acronyme);
            $agent->setEmail('suivi.mpe@atexo.com');
            $agent->setNom(strtoupper($acronyme));
            $agent->setPrenom('Admin');
            $agent->setOrganisme($acronyme);
            $agent->setDateModification(date('Y-m-d H:i:s'));
            $agent->setDateCreation(date('Y-m-d H:i:s'));
            $agent->save($connexionCom);

            $newHablitation = Atexo_Agent_EnablingsAgents::addOrModifyEnablingsOfTheAgents(1);
            $newHablitation->setIdAgent($agent->getId());
            $newHablitation->save($connexionCom);

            $socleHabilitationAgent = new CommonSocleHabilitationAgent();
            $socleHabilitationAgent->setIdAgent($agent->getId());
            $socleHabilitationAgent->setGestionAgentPoleSocle(1);
            $socleHabilitationAgent->setGestionAgentsSocle(1);
            $socleHabilitationAgent->setDroitGestionServicesSocle(1);
            $socleHabilitationAgent->save($connexionCom);

            $agentServiceMetier = new CommonAgentServiceMetier();
            $agentServiceMetier->setIdAgent($agent->getId());
            $agentServiceMetier->setIdServiceMetier(1);
            $agentServiceMetier->setIdProfilService(1);
            $agentServiceMetier->save($connexionCom);

            if (true === $is_socle) {
                $agentServiceMetier = new CommonAgentServiceMetier();
                $agentServiceMetier->setIdAgent($agent->getId());
                $agentServiceMetier->setIdServiceMetier(2);
                $agentServiceMetier->setIdProfilService(12);
                $agentServiceMetier->save($connexionCom);
            }

            if (null != $idAdmin) {
                $id = $idAdmin;
            } else {
                $id = Atexo_CurrentUser::getId();
            }

            if (false != $id && '' != $id && $id > 0) {
                $admin = CommonAdministrateurPeer::retrieveByPK(intval($id), $connexionCom);
                $admin->setOrganisme($acronyme);
                $admin->save($connexionCom);

                //on change le mot de passe de l'agent admin_xxx pour lui donner celui de l'administrateur qui est plus securisé
                $agent->setPassword($admin->getMdp());
                $agent->save($connexionCom);
            }

            // création dossier de l'organisme
            if (!is_dir(Atexo_Config::getParameter('BASE_ROOT_DIR').'/'.$acronyme)) {
                Atexo_Util::createDirectory(Atexo_Config::getParameter('BASE_ROOT_DIR').'/'.$acronyme);
            }
            // création dossier de stockage (blob en fichiers)
            if (!is_dir((new Atexo_Blob_DbManagement())->db_get_blob_storage_directory_path('localhost', $acronyme, null, true))) {
                Atexo_Util::createDirectory((new Atexo_Blob_DbManagement())->db_get_blob_storage_directory_path('localhost', $acronyme, null, true));
            }

            // Organisme de reference A1A pour les types de procedure (et equivalence)
            $is_a1a = Atexo_Db::getLinkCommon()->query("SELECT acronyme FROM Organisme WHERE acronyme = '".Atexo_Config::getParameter('ORGANISME_REFERENCE')."'")->fetch(PDO::FETCH_ASSOC);
            if ($acronyme != Atexo_Config::getParameter('ORGANISME_REFERENCE') && $is_a1a['acronyme'] == Atexo_Config::getParameter('ORGANISME_REFERENCE')) {
                (new Atexo_Organismes())->migrateData('Type_Procedure_Organisme', Atexo_Config::getParameter('ORGANISME_REFERENCE'), $acronyme);
                (new Atexo_Organismes())->migrateData('ProcedureEquivalence', Atexo_Config::getParameter('ORGANISME_REFERENCE'), $acronyme);
            }

            // création base organisme
            Atexo_Db::closeCommon();
            $connexionCom->commit();
            BaseCommonOrganismePeer::removeInstanceFromPool($organisme);

            return $agent;
        } catch (\Exception $e) {
            $connexionCom->rollback();
            //echo $e->__toString();
            throw $e;
        }
    }

    /**
     * permet de copier la configuration organisme d'un organisme source vers l'organisme cible, si pas de conf pour
     * l'organisme source, on cree une nouvelle configuration avec les valeurs par defaut.
     *
     * @param string       $organismeSource acronyme de l'organisme source
     * @param string       $organismeCible  acronyme de l'organisme cible
     * @param PdoConnexion $connexion       connexion base de donnees
     *
     * @return void
     *
     * @author AME <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function copierConfigurationOrganisme($organismeSource, $organismeCible, $connexion)
    {
        $configSource = (new Atexo_Module())->getModuleOrganismeByAcronyme($organismeSource);
        if ($configSource instanceof CommonConfigurationOrganisme) {
            $configCible = $configSource->copy();
        } else {
            $configCible = new CommonConfigurationOrganisme();
        }
        if ($configCible instanceof CommonConfigurationOrganisme) {
            $configCible->setOrganisme($organismeCible);
            $configCible->setPresenceElu('1');
            $configCible->save($connexion);
        }
    }

    /**
     * Mettre les infos récupèrer depuis le web service dans les champs de la table organisme du projet associé.
     *
     * @param string $org, array $wsOrg
     */
    public function remplirOrganisme($org, $wsOrg)
    {
        $org->setIdInitial($wsOrg['id']);
        $org->setAcronyme(utf8_decode($wsOrg['acronyme']));
        $org->setTypeArticleOrg($wsOrg['type_article_org']);
        $org->setDenominationOrg(utf8_decode($wsOrg['denomination_org']));
        $org->setCategorieInsee($wsOrg['categorie_insee']);
        $org->setDescriptionOrg(utf8_decode($wsOrg['description_org']));
        $org->setAdresse(utf8_decode($wsOrg['adresse']));
        $org->setCp($wsOrg['cp']);
        $org->setVille(utf8_decode($wsOrg['ville']));
        $org->setCanton($wsOrg['canton']);
        $org->setLatitudeDegre($wsOrg['latitude_degre']);
        $org->setLatitudeMinute($wsOrg['latitude_minute']);
        $org->setLatitudeSeconde($wsOrg['latitude_seconde']);
        $org->setLongitudeDegre($wsOrg['longitude_degre']);
        $org->setLongitudeMinute($wsOrg['longitude_minute']);
        $org->setLongitudeSeconde($wsOrg['longitude_seconde']);
        $org->setEmail(trim($wsOrg['email']));
        $org->setUrl($wsOrg['url']);
        $org->setIdAttribFile($wsOrg['attribution_file']);
        $org->setDateCreation($wsOrg['date_creation']);
        $org->setActive($wsOrg['active']);
        $org->setSignataireCao($wsOrg['signataire_cao']);
        $org->setPays('France');
        $org->setAffichageEntite('0');
    }

    /***
    *   extraire les informations d'un organisme appartir de fichier csv
    *   $ligne[0]=>'Acronym organism',$ligne[1]=>'siren',$ligne[2]=>'adherentGIP',$ligne[3]=>'id organism',$ligne[4]=>'cauton',$ligne[5]=>'url site'
    */
    public function getInfoOrganismFromCSV()
    {
        $arrayGlob = [];
        $adherentGIP = 'false';
        $siren = '';

        $fichier = Atexo_Config::getParameter('CSV_ORGANISME_E-BOURGOGNE');
        $fic = fopen($fichier, 'rb');
        while ($ligne = fgetcsv($fic, 1024, ';')) {
            if ('' != $ligne[2]) {
                $adherentGIP = $ligne[2];
            }
            $siren = $ligne[1];
            $arrayGlob[$ligne[0]] = ['siren' => $siren, 'adherentGIP' => $adherentGIP];
        }

        return $arrayGlob;
    }

    /**
     * retourne tous les organismes.
     *
     * @return string : objets organisme
     */
    public function retrieveAllOrganismes()
    {
        $connexionCommon = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $organisme = CommonOrganismePeer::doSelect($c, $connexionCommon);
        if ($organisme) {
            return $organisme;
        } else {
            return false;
        }
    }

    /**
     * Récupère les dénominations d'organismes pour une autocompletion
     *
     * @param string $keyword Mot-clé à rechercher
     */
    public static function retrieveOrganismesByDenominationForAutocompletion(string $keyword): array
    {
        $data = [];

        $word = trim($keyword);
        $forbiddenWords = ' and or et ou le la les un une du de des a à au aux son sa ses ne ni non sauf ce ces cet je tu il elle on nous vous ils elles etc mon ma ton ta vos se y en sur ';
        if (!strstr($forbiddenWords, $word)) {
            $word = '%'.Atexo_Util::replaceArrayCharactersByPourcentagage($word).'%';

            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $c = new Criteria();
            $c->add(CommonOrganismePeer::DENOMINATION_ORG, $word, Criteria::LIKE);
            $c->add(CommonOrganismePeer::ACTIVE, self::IS_ACTIVE, Criteria::EQUAL);
            $c->addAscendingOrderByColumn(CommonOrganismePeer::DENOMINATION_ORG);
            $organismes = CommonOrganismePeer::doSelect($c, $connexion);

            if (!empty($organismes) && is_array($organismes)) {
                foreach ($organismes as $organisme) {
                    $data[] = [
                        'id' => $organisme->getAcronyme(),
                        'denomination' => $organisme->getDenominationOrg()
                    ];
                }
            }
        }

        return $data;
    }

    public function migrateData($table, $organismeSource, $organismeDestination)
    {
        $fromField = '';
        $intoField = '';
        $arrayField = [];
        $nameFieldOrganisme = 'organisme';
        try {
            $arrayField = (new Atexo_Db())->getNomColumns($table, $nameFieldOrganisme);
            $implodField = implode(', ', $arrayField);
            $intoField = $nameFieldOrganisme.', '.$implodField;
            $fromField = "'".$organismeDestination."', ".$implodField;
            $sql = "INSERT INTO `$table` ($intoField) SELECT $fromField FROM `$table` WHERE `$table`.`$nameFieldOrganisme` = '$organismeSource'";
            Atexo_Db::getLinkCommon()->query($sql);
        } catch (\Exception $ex) {
            Propel::log($ex->getMessage(), Propel::LOG_ERR);
        }
    }

    public function getSigleOrganisme($acronyme)
    {
        $organisme = self::retrieveOrganismeByAcronyme($acronyme);
        $sigle = '';
        if ($organisme instanceof CommonOrganisme) {
            $sigle = $organisme->getSigle();
        }

        return $sigle;
    }

    public function getIdServiceMetierByOrganisme($acronyme, $connexionCom)
    {
        $listeIds = [];
        $c = new Criteria();
        $c->add(CommonOrganismeServiceMetierPeer::ORGANISME, $acronyme);
        $commonOrganismeServiceMetierPeer = CommonOrganismeServiceMetierPeer::doSelect($c, $connexionCom);
        for ($i = 0; $i < count($commonOrganismeServiceMetierPeer); ++$i) {
            $listeIds[$i] = $commonOrganismeServiceMetierPeer[$i]->getIdServiceMetier();
        }

        return $listeIds;
    }

    public function getDenominationOrganisme($acronyme)
    {
        $organisme = self::retrieveOrganismeByAcronyme($acronyme);
        $denomination = '';
        if ($organisme instanceof CommonOrganisme) {
            $denomination = $organisme->getDenominationOrgTraduit();
        }

        return $denomination;
    }

    /*
     * Permet de faire la recherche sur les organisme à partir du criteriaVo
     */
    public function search(Atexo_Organisme_CriteriaVo $criteriaVo)
    {
        $c = new Criteria();
        if ($criteriaVo->getAcronyme()) {
            $c->add(CommonOrganismePeer::ACRONYME, $criteriaVo->getAcronyme());
        } else {
            if ($criteriaVo->getDenomination()) {
                $denomination = $criteriaVo->getDenomination();
                $crit0 = $c->getNewCriterion(CommonOrganismePeer::DENOMINATION_ORG, '%'.$denomination.'%', Criteria::LIKE);
                $crit1 = $c->getNewCriterion(CommonOrganismePeer::DENOMINATION_ORG_AR, '%'.$denomination.'%', Criteria::LIKE);
                $crit2 = $c->getNewCriterion(CommonOrganismePeer::DENOMINATION_ORG_FR, '%'.$denomination.'%', Criteria::LIKE);
                $crit3 = $c->getNewCriterion(CommonOrganismePeer::DENOMINATION_ORG_ES, '%'.$denomination.'%', Criteria::LIKE);
                $crit4 = $c->getNewCriterion(CommonOrganismePeer::DENOMINATION_ORG_EN, '%'.$denomination.'%', Criteria::LIKE);
                $crit5 = $c->getNewCriterion(CommonOrganismePeer::DENOMINATION_ORG_SU, '%'.$denomination.'%', Criteria::LIKE);
                $crit6 = $c->getNewCriterion(CommonOrganismePeer::DENOMINATION_ORG_DU, '%'.$denomination.'%', Criteria::LIKE);
                $crit7 = $c->getNewCriterion(CommonOrganismePeer::DENOMINATION_ORG_CZ, '%'.$denomination.'%', Criteria::LIKE);
                $crit8 = $c->getNewCriterion(CommonOrganismePeer::DENOMINATION_ORG_IT, '%'.$denomination.'%', Criteria::LIKE);

                $crit0->addOr($crit1);
                $crit0->addOr($crit2);
                $crit0->addOr($crit3);
                $crit0->addOr($crit4);
                $crit0->addOr($crit5);
                $crit0->addOr($crit6);
                $crit0->addOr($crit7);
                $crit0->addOr($crit8);
                $c->add($crit0);
            }
        }
        if ($criteriaVo->getActive()) {
            $c->add(CommonOrganismePeer::ACTIVE, $criteriaVo->getActive());
        }
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $listeOrgs = CommonOrganismePeer::doSelect($c, $connexion);

        return $listeOrgs;
    }

    /**
     * Permet de retourner des info du logo d'un organisme.
     *
     * @param string $acronyme l'acronyme de l'organisme
     * @param $logger le logger
     *
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     * @copyright Atexo 2015
     */
    public function getLogoOrganismeInfo($acronyme, $logger, $logoGrand = true): \Application\Service\Atexo\Rest\Action\Atexo_Rest_Action_LogoField|bool
    {
        $logo = false;
        $logger->info("La recuperation des info du logo de l'organisme ".$acronyme);
        if (Atexo_Module::isEnabled('AfficherImageOrganisme')) {
            $cheminLogo = Atexo_Config::getParameter('BASE_ROOT_DIR').'/'.$acronyme.Atexo_Config::getParameter('PATH_ORGANISME_IMAGE');
            if ($logoGrand) {
                $logger->info("La recuperation du grand logo de l'organisme ".$acronyme);
                $cheminLogo .= 'logo-organisme-grand.jpg';
                $fileName = 'logo_grand_'.$acronyme.'.jpg';
            } else {
                $logger->info("La recuperation du petit logo de l'organisme ".$acronyme);
                $cheminLogo .= 'logo-organisme-petit.jpg';
                $fileName = 'logo_petit_'.$acronyme.'.jpg';
            }
            $logger->info('Le chemin du logo est '.$cheminLogo);
            if (is_file($cheminLogo)) {
                $logo = new Atexo_Rest_Action_LogoField();
                $logger->info("Le logo de l'organisme ".$acronyme.' existe');
                $logo->setFilename($fileName);
                $sizeFile = filesize($cheminLogo);
                $logo->setSize($sizeFile);
                $logo->setPathFile($cheminLogo);
                $logo->setMd5Checksum(md5_file($cheminLogo));
            } else {
                $logger->info("Le logo de l'organisme ".$acronyme." n'existe pas");
            }
        } else {
            $logger->info('Le module AfficherImageOrganisme est désactivé');
        }

        return $logo;
    }

    /**
     * Permet d'ajouter les informations d'une question pour le ws.
     *
     * @param $id
     * @param $xmlResponse
     * @param $arrayBlob
     * @param $logger
     * @param $contentType
     *
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function setInfoLogoOrganismeForWs($id, &$xmlResponse, &$arrayBlob, &$logger, &$contentType, $logoGrand = true, $typeChecksum = 'md5'): bool|string
    {
        $logger->info("La récupération de l'organisme = ".$id);
        $organisme = Atexo_Organismes::retrieveOrganismeByAcronyme($id, true);
        if ($organisme instanceof CommonOrganisme) {
            $logger->info("L'organisme existe");
            $logo = self::getLogoOrganismeInfo($organisme->getAcronyme(), $logger, $logoGrand);
            if ($logo instanceof Atexo_Rest_Action_LogoField) {
                $content = file_get_contents($logo->getPathFile());
                $file = 'file_'.$organisme->getAcronyme();
                $arrayBlob[] = ['name' => $logo->getFilename(), 'file' => $file, 'content' => $content];
                $logger->info("Mettre les informations du logo dans l'objet Atexo_Rest_ObjectField_DocumentField");
                $documentField = (new Atexo_Document())->getDocumentFieldByInfo($id, $logo->getFilename(), $id, '', $content, 'ORG_LOGO_GRAND', $logo->getSize(), null, null, null, null, null, $typeChecksum);
                $contentType = Atexo_Util::getContentTypeByExtension(Atexo_Util::getExtension($logo->getFilename()));
                $logger->info("La récupération de l'xml contenant les informations du logo de l'organisme ");
                $xmlResponse = (new Atexo_Rest_Action_Actions())->getXmlInfosObjectField($documentField);

                return false;
            } else {
                return "Le logo de l'organisme ".$id." n'existe pas";
            }
        } else {
            return "L'organisme ".$id." n'existe pas";
        }
    }

    /**
     * Permet de recuperer le code postal d'un organisme.
     *
     * @param $acronyme
     *
     * @return string
     */
    public static function getCodePostalOrganisme($acronyme)
    {
        $organisme = self::retrieveOrganismeByAcronyme($acronyme);
        $cp = '';
        if ($organisme instanceof CommonOrganisme) {
            $cp = $organisme->getCp();
        }

        return $cp;
    }
}
