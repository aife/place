<?php

namespace Application\Controls;

use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonOrganisme;
use Application\Propel\Mpe\CommonTAnnonceConsultation;
use Application\Propel\Mpe\CommonTConsultationComptePub;
use Application\Propel\Mpe\CommonTConsultationComptePubQuery;
use Application\Propel\Mpe\CommonTDonneeComplementaire;
use Application\Propel\Mpe\CommonTOffreSupportPublicite;
use Application\Propel\Mpe\CommonTSupportAnnonceConsultation;
use Application\Propel\Mpe\CommonTSupportPublication;
use Application\Propel\Mpe\CommonTypeProcedureOrganisme;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_PublicPurchaser;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\FormulaireSub\Atexo_FormulaireSub_AnnonceEchange;
use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2;
use Application\Service\Atexo\Publicite\Atexo_Publicite_AnnonceSub;
use Exception;
use Prado\Prado;

/**
 * Classe d'onglet de publicite pour le consultation.
 *
 * @author Amal EL BEKKAOUI <amal.elbekkaoui@atexo.com>
 *
 * @version 1.0
 *
 * @since esr-2015
 *
 * @copyright Atexo 2016
 */
class AtexoPubliciteConsultation extends MpeTTemplateControl
{
    private $consultation;
    private int $mode = 1; // 1 : mode modification 0: affichage
    private ?string $mapa = null;
    private $_calledFrom = '';
    private $langue;
    private $codeDenomAdapte;

    /**
     * @return mixed
     */
    public function getConsultation()
    {
        return $this->Page->getViewState('consultation');
    }

    /**
     * @param mixed $consultation
     */
    public function setConsultation($consultation)
    {
        $this->consultation = $consultation;
    }

    /**
     * @return int
     */
    public function getMode()
    {
        return $this->mode;
    }

    /**
     * @param int $mode
     */
    public function setMode($mode)
    {
        $this->mode = $mode;
    }

    /**
     * @param $value
     */
    public function setCalledFrom($value)
    {
        $this->_calledFrom = $value;
    }

    /**
     * @return string
     */
    public function getCalledFrom()
    {
        return $this->_calledFrom;
    }

    public function charger($consultation, $donneesComplementaires, $param = null)
    {
        $this->scriptJs->setText('');
        $pbVal = 100; //$this->chargerDonnees($consultation, $donneesComplementaires, $idDispositif);
        $this->supportsSelectionnes->setVisible(false);
        $consultation = $this->getConsultation();
        if ($consultation instanceof CommonConsultation) {
            $logger = Atexo_LoggerManager::getLogger('publicite');
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $annonce = Atexo_Publicite_AnnonceSub::getAnnonceSimplifie($consultation->getId(), $consultation->getOrganisme(), $connexion);
            if (!$this->getMode()) {
                if ($annonce instanceof CommonTAnnonceConsultation) {
                    $supports = $annonce->getSupportsAnnonce();
                    if (is_array($supports) && count($supports)) {
                        $this->listeSupportLibelles->setDataSource($supports);
                        $this->listeSupportLibelles->DataBind();
                        $this->supportsSelectionnes->setVisible(true);
                        $this->rappelOngletPublicite->setVisible(false);
                    } else {
                        $this->listeSupportLibelles->setDataSource([]);
                        $this->listeSupportLibelles->DataBind();
                        $this->supportsSelectionnes->setVisible(false);
                        $this->rappelOngletPublicite->setVisible(true);
                    }
                } else {
                    $pbVal = 0;
                    $this->bloc_PubliciteConsultation->setVisible(false);
                }
            } else {
                if ($consultation instanceof CommonConsultation) {
                    $lieuxExecution = explode(',', $consultation->getLieuExecution());
                }
                $supports = Atexo_Publicite_AnnonceSub::getSupportsAgent(Atexo_CurrentUser::getId(), Atexo_CurrentUser::getOrganismAcronym(), $connexion, $annonce, $lieuxExecution);
                if (is_array($supports)) {
                    $this->listeSupport->setDataSource($supports);
                    $this->listeSupport->dataBind();
                } else {
                    $this->listeSupport->setDataSource([]);
                    $this->listeSupport->dataBind();
                }
                $this->gererAffichageSupport();
            }
        }
        if ($param) {
            if (Atexo_Config::getParameter('CONCENTRATEUR_V2')) {
                $this->bloc_PubliciteConsultationConcentrateurV2->render($param->getNewWriter());
            } else {
                $this->bloc_PubliciteConsultation->render($param->getNewWriter());
            }
        }

        return $pbVal;
    }

    public function chargerDonnees($consultation, $donneesComplementaire, $idDispositif)
    {
        try {
            $logger = Atexo_LoggerManager::getLogger('publicite');
            $this->panelSousTypeOrganisme->setVisible(false);
            $this->panelArticle->setVisible(false);
            $this->panelFormulairePropose->setVisible(false);
            if ($consultation instanceof CommonConsultation && $consultation->getId()) {
                $this->typeProcedure->setText($consultation->getLibelleTypeProcedureOrg());
                if ($consultation->getEntiteAdjudicatrice()) {
                    $this->typeEntite->setText(Prado::localize('ENTITE_ADJUDICATRICE'));
                } else {
                    $this->typeEntite->setText(Prado::localize('POUVOIR_ADJIDUCATEUR_CONSULTATION'));
                    $organisme = $consultation->getOrganismeObject();
                    if ($organisme instanceof CommonOrganisme) {
                        $sousTypeOrg = '';
                        if (1 == $organisme->getSousTypeOrganisme()) {
                            $sousTypeOrg = Prado::localize('ELIGIBLES_SEUIL_ETAT');
                        } elseif (2 == $organisme->getSousTypeOrganisme()) {
                            $sousTypeOrg = Prado::localize('ELIGIBLES_SEUIL_COLLECTIVITES');
                        }
                        if ($sousTypeOrg) {
                            $this->libelleSousTypeOrganisme->setText($sousTypeOrg);
                            $this->panelSousTypeOrganisme->setVisible(true);
                        }
                    }
                }
                $this->libelleCategorie->setText($consultation->getLibelleCategorieConsultationTraduit());
                if ($donneesComplementaire instanceof CommonTDonneeComplementaire) {
                    $this->montantEstime->setText(Atexo_Util::getMontantArronditEspace($donneesComplementaire->getMontantMarche(), 0));
                }

                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                if ($idDispositif) {
                    /*$dispositifObjet = CommonTDispositifAnnonceQuery::create()->filterByIdExterne($idDispositif)->findOne($connexion);
                    if ($dispositifObjet instanceof CommonTDispositifAnnonce && $dispositifObjet->getLibelleFormulairePropose()) {
                        $this->panelFormulairePropose->setVisible(true);
                        $this->formulairePropose->setText(Prado::localize($dispositifObjet->getLibelleFormulairePropose()));
                    }*/
                    $annonce = Atexo_Publicite_AnnonceSub::getAnnonceSimplifieEncours($consultation->getId(), $consultation->getOrganisme(), $connexion);
                    if (!$annonce instanceof CommonTAnnonceConsultation) {
                        $annonce = new CommonTAnnonceConsultation();
                        $annonce->setOrganisme($consultation->getOrganisme());
                        $annonce->setConsultationId($consultation->getId());
                        $annonce->setPubliciteSimplifie(1);
                        $annonce->setIdTypeAnnonce(Atexo_Publicite_AnnonceSub::getTypeAvisDispositif($idDispositif));
                    }
                    $pbVal = 100; // TODO $this->calculChampsManquants($consultation, $relation->getIdCompteBoamp());
                    if ($annonce instanceof CommonTAnnonceConsultation) {
                        $statut = Atexo_Config::getParameter('ANNONCE_STATUT_A_COMPLETER');
                        if ($this->getMode()) {
                            if ($idDispositif && $idDispositif != $annonce->getIdDispositif()) {
                                $annonce->setIdDossierSub(0);
                                $annonce->setIdDispositif($idDispositif);
                            }
                        }
                        if (100 == $pbVal) {
                            $statut = Atexo_Config::getParameter('ANNONCE_STATUT_COMPLET');
                        }

                        if ($annonce->getStatut() != $statut) {
                            $annonce->setStatut($statut);
                            $annonce->setDateStatut(date('Y-m-d H:i:s'));
                        }
                        $annonce->save($connexion);
                        Atexo_Publicite_AnnonceSub::ajouterHistorique($annonce, $connexion);
                    }
                }
            }
        } catch (Exception $e) {
            // echo $e->getMessage() . $e->getTraceAsString();exit;
            $logger->error('Consultation '.$consultation->getId()."Erreur s'est produite lors du chargement du bloc synthese : ".$e->getMessage().' '.$e->getTraceAsString());
        }
    }

    public function calculChampsManquants($consultation, $idCompteBoamp)
    {
        $compteBoamp = (new Atexo_PublicPurchaser())->retrievePublicPurchasersById($idCompteBoamp);
        $infosConsultation = Atexo_Publicite_AnnonceSub::getChampsManquantsConsultation($consultation);
        $infosBoamp = Atexo_Publicite_AnnonceSub::getChampsManquantsCompteBoamp($compteBoamp);

        $prTotal = 0;
        $totalChampsManquantBoamp = 0;
        $totalChampsManquantCons = 0;
        if (is_array($infosConsultation)) {
            if (isset($infosConsultation['nbreChampsManquants'])) {
                $this->nbreChampsCons->setText($infosConsultation['nbreChampsManquants']);
                $totalChampsManquantCons = $infosConsultation['nbreChampsManquants'];
            }
            if (isset($infosConsultation['pourcentage'])) {
                $prTotal += $infosConsultation['pourcentage'];
            }
            if (isset($infosConsultation['libellesChampsManquants']) && is_array($infosConsultation['libellesChampsManquants'])) {
                $this->listeChampsManquantsCons->dataSource = $infosConsultation['libellesChampsManquants'];
                $this->listeChampsManquantsCons->dataBind();
            }
        }
        $this->pourcentageBoamp->setValue(0);
        if (is_array($infosBoamp)) {
            if (isset($infosBoamp['nbreChampsManquants'])) {
                $this->nbreChampsBoamp->setText($infosBoamp['nbreChampsManquants']);
                $totalChampsManquantBoamp = $infosBoamp['nbreChampsManquants'];
            }
            if (isset($infosBoamp['pourcentage'])) {
                $this->pourcentageBoamp->setValue($infosBoamp['pourcentage']);
                $prTotal += $infosBoamp['pourcentage'];
            }
            if (isset($infosBoamp['libellesChampsManquants']) && is_array($infosBoamp['libellesChampsManquants'])) {
                $this->listeChampsManquantsBoamp->setDataSource($infosBoamp['libellesChampsManquants']);
                $this->listeChampsManquantsBoamp->dataBind();
            }
        }
        $this->pourcentageTotal1->setText($prTotal);
        $this->pourcentageTotal2->setText($prTotal);
        $this->nbreChampsManquant->setText($totalChampsManquantCons + $totalChampsManquantBoamp);
        $this->panelBlocManquantCons->setVisible(($totalChampsManquantCons ? true : false));
        $this->panelBlocManquantBoamp->setVisible(($totalChampsManquantBoamp ? true : false));
        if ($prTotal >= 100) {
            $this->panelInfoPub->setVisible(false);
        }
        $script = '<script> J(".dial").val('.$prTotal.'+\'%\'); J(".dial").trigger(\'change\');</script>';
        $this->scriptJs->setText($script);

        return $prTotal;
    }

    public function getToOnglet($sender, $param)
    {
        $script = '';
        switch ($param->CallbackParameter) {
            case 'IDENTIFICATION':
                $script = "<script>document.getElementById('ctl0_CONTENU_PAGE_etapesConsultation_identification').click();</script>";
                break;
            case 'CALENDRIER':
                $script = "<script>document.getElementById('ctl0_CONTENU_PAGE_etapesConsultation_calendrier').click();</script>";
                break;
            case 'DONNEES_REDAC':
                $script = "<script>document.getElementById('ctl0_CONTENU_PAGE_etapesConsultation_donneesComplementaires').click();</script>";
                break;
        }
        if ($script) {
            $this->scriptJs->setText($script);
        }
    }

    public function downloadPdfAnnonce($sende, $param)
    {
        $consultation = $this->Page->getConsultation();
        $annonce = $this->creerAnnonce($consultation);

        $this->setViewState('annonce', $annonce);
        if ($annonce instanceof CommonTAnnonceConsultation && $annonce->getIdDossierSub()) {
            $this->scriptJs->setText("<script>document.getElementById('".$this->downloadPdfSub->getClientId()."').click();</script>");
        }
    }

    public function downloadPdf($sende, $param)
    {
        $this->scriptJs->setText('');
        $annonce = $this->getViewState('annonce');
        if ($annonce instanceof CommonTAnnonceConsultation) {
            Atexo_Publicite_AnnonceSub::downLoadPdfAnnonce($annonce->getIdDossierSub());
        }
    }

    public function recupererSupportsSelectionnes()
    {
        $return = ['listeCode' => [], 'listeId' => [], 'listeIdASupprimer' => []];
        foreach ($this->listeSupport->Items as $item) {
            if ($item->choixSupport->checked) {
                if ($item->CodeSupport->getValue()) {
                    $return['listeCode'][$item->CodeSupport->getValue()] = $item->CodeSupport->getValue();
                    $offre = Atexo_Publicite_AnnonceSub::getOffreSupportPublicationById($item->idOffre->getValue());
                    if ($offre instanceof CommonTOffreSupportPublicite) {
                        $return['listeCodeOffre'][$item->CodeSupport->getValue()] = $offre->getCode();
                    }
                }
                $return['listeId'][$item->idSupport->getValue()] = $item->idOffre->getValue();
                $return['listeSupport'][$item->idSupport->getValue()]['idOffre'] = $item->idOffre->getValue();
                $return['listeSupport'][$item->idSupport->getValue()]['code'] = $item->CodeSupport->getValue();
                $return['listeSupport'][$item->idSupport->getValue()]['messageSupport'] = $item->textMessageSupport->getText();
                $return['listeSupport'][$item->idSupport->getValue()]['departementsParution'] = $item->hiddenDepartementsParution->getValue();
            } else {
                $return['listeIdASupprimer'][$item->idSupport->getValue()] = $item->idSupport->getValue();
            }
        }

        return $return;
    }

    public function creerAnnonce($consultation, $statut = null)
    {
        $connexion = null;
        try {
            $logger = Atexo_LoggerManager::getLogger('publicite');
            $logger->info('debut creation annonce ');
            $annonce = null;
            $idDispositif = Atexo_Config::getParameter('ID_DISPOSITIF_PUBLICITE_SIMPLIFIE');
            if (empty($statut)) {
                $statut = Atexo_Config::getParameter('ANNONCE_STATUT_A_COMPLETER');
            }
            // echo $idDispositif, $statut;exit;
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            if ($consultation instanceof CommonConsultation) {
                //Recuperation de la liste des supports selectionnes
                $supports = $this->recupererSupportsSelectionnes();
                $supportsCodes = $supports['listeCode'] ?? [];
                $offresCodes = $supports['listeCodeOffre'] ?? [];
                $listeSupports = $supports['listeSupport'] ?? [];
                $supportsIdsSupprime = $supports['listeIdASupprimer'] ?? [];
                $tConsultationComptePubQuery = new CommonTConsultationComptePubQuery();
                $compteBoamp = $tConsultationComptePubQuery->recupererComptePubByIdDonneesCompAndOrg($consultation->getIdDonneeComplementaire(), $consultation->getOrganisme(), $logger);
                $annonce = Atexo_Publicite_AnnonceSub::getAnnonceSimplifieEncours($consultation->getId(), $consultation->getOrganisme(), $connexion);
                if ((!$annonce instanceof CommonTAnnonceConsultation)) {
                    $annonce = new CommonTAnnonceConsultation();
                    $annonce->setOrganisme($consultation->getOrganisme());
                    $annonce->setConsultationId($consultation->getId());
                    $annonce->setIdDispositif($idDispositif);
                    $xml = Atexo_FormulaireSub_AnnonceEchange::getXmlInitialisationDossier($consultation, $annonce, $compteBoamp, $supportsCodes, true, 'true', $offresCodes);
                    $idDossierSub = Atexo_FormulaireSub_AnnonceEchange::creerDossier($xml);
                    if (false !== $idDossierSub && is_string($idDossierSub)) {
                        $annonce->setIdDossierSub($idDossierSub);
                    } else {
                        $logger->error("Methode = AtexoPubliciteConsultation::creerAnnonce Le dossier n'a pas pu etre cree : idDossier = ".$idDossierSub.' , RefCons = '.$consultation->getId().' , Organisme = '.$consultation->getOrganisme());
                    }
                } else {
                    $xml = Atexo_FormulaireSub_AnnonceEchange::getXmlInitialisationDossier($consultation, $annonce, $compteBoamp, $supportsCodes, true, 'true', $offresCodes);
                    $update = Atexo_FormulaireSub_AnnonceEchange::updateAnnonce($xml);
                    if ($update) {
                        $logger->info('Methode = AtexoPubliciteConsultation::creerAnnonce Consultation '.$consultation->getId().' Annonce '.$annonce->getId().' est mise à jour avec succes');
                    } else {
                        $logger->info('Methode = AtexoPubliciteConsultation::creerAnnonce Consultation '.$consultation->getId().' Annonce '.$annonce->getId()." n'est pas mise à jour");
                    }
                }
                if ($compteBoamp instanceof CommonTConsultationComptePub) {
                    $annonce->setIdCompteBoamp($compteBoamp->getId());
                }
                if ($annonce->getIdDossierSub()) {
                    $annonce->setIdAgent(Atexo_CurrentUser::getIdAgentConnected());
                    $annonce->setPrenomNomAgent(Atexo_CurrentUser::getFirstName().' '.Atexo_CurrentUser::getLastName());
                    $annonce->setPubliciteSimplifie(1);
                    $oldStatut = $annonce->getStatut();
                    $annonce->setStatut($statut);
                    $annonce->setDateStatut(date('Y-m-d H:i:s'));
                    $connexion->beginTransaction();
                    if (is_array($listeSupports)) {
                        foreach ($listeSupports as $idSupport => $array) {
                            $logger->info('Methode = AtexoPubliciteConsultation::creerAnnonce : Ajout/update support annonce : id support pub = '.$idSupport.'idAnnonce  = '.$annonce->getId().' , refCons = '.$annonce->getRefConsultation().' , org = '.$annonce->getOrganisme());
                            (new Atexo_Publicite_AnnonceSub())->addSupportsToAnnonce($annonce, $idSupport, null, $array['idOffre'], $statut, $array['messageSupport'], $array['departementsParution']);
                        }
                    }
                    if (is_array($supportsIdsSupprime)) {
                        foreach ($supportsIdsSupprime as $idSupportSupprime) {
                            $support = Atexo_Publicite_AnnonceSub::getSupportAnnonceBySupportPub($annonce->getId(), $idSupportSupprime, $connexion);
                            if ($support instanceof CommonTSupportAnnonceConsultation) {
                                $support->delete($connexion);
                                $logger->info('Methode = AtexoPubliciteConsultation::creerAnnonce : supprimer un support annonce : id support pub = '.$idSupportSupprime.'idAnnonce  = '.$annonce->getId().' , refCons = '.$annonce->getRefConsultation().' , org = '.$annonce->getOrganisme());
                            }
                        }
                    }
                    $annonce->save($connexion);
                    $connexion->commit();
                    $logger->info('Methode = AtexoPubliciteConsultation::creerAnnonce : annonce est créé ou mise à jour avec succes id '.$annonce->getId());
                    if ($oldStatut != $statut) {
                        Atexo_Publicite_AnnonceSub::ajouterHistorique($annonce, $connexion);
                    }
                } else {
                    $logger->error("Methode = AtexoPubliciteConsultation::creerAnnonce L'annonce n'a pas pu etre cree : pas de dossier créé id_Dispositif = ".$idDispositif.' , RefCons = '.$consultation->getId().' , Organisme = '.$consultation->getOrganisme());
                }
            } else {
                $logger->error("Methode = AtexoPubliciteConsultation::creerAnnonce L'objet consultation est introuvable ou n'est pas une instance de CommonConsultation");
            }

            return $annonce;
        } catch (Exception $e) {
            $connexion->rollBack();
            $logger->error("Methode = AtexoPubliciteConsultation::creerAnnonce Erreur lors de l'ajout d'un formulaire de publicite : RefCons = ".$consultation->getId().' Organisme = '.$consultation->getOrganisme().', Erreur : '.$e->getMessage().' '.$e->getTraceAsString());
        }
    }

    public function envoyerAnnonce($consultation)
    {
        $annonce = null;
        try {
            $res = true;
            if ($this->publier->checked) {
                $logger = Atexo_LoggerManager::getLogger('publicite');
                if ($consultation instanceof CommonConsultation) {
                    $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                    $annonce = Atexo_Publicite_AnnonceSub::getAnnonceSimplifieEncours($consultation->getId(), $consultation->getOrganisme(), $connexion);
                    if ($annonce instanceof CommonTAnnonceConsultation && $annonce->getIdDispositif()) {
                        $lisSupportPub = $annonce->getListeSupportPub();
                        if (!empty($lisSupportPub)) {
                            $res = Atexo_Publicite_AnnonceSub::envoyerAnnonce($annonce);
                        }
                    }
                }
            }

            return $res;
        } catch (Exception $e) {
            $logger->error("Erreur envoi de l'annonce ".($annonce instanceof CommonTAnnonceConsultation) ? $annonce->getId() : ''.' de la consultation '.print_r($consultation, true).' '.$e->getMessage().' '.$e->getTraceAsString());
            throw $e;
        }
    }

    public function isOffreSelectionne($support, $idOffre)
    {
        $return = '';
        if ($support instanceof CommonTSupportPublication) {
            if (!$support->getSelectedOffre()) {
                $consultation = $this->getConsultation();
                if ($consultation instanceof CommonConsultation) {
                    $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
                    $annonce = Atexo_Publicite_AnnonceSub::getAnnonceSimplifieEncours($consultation->getId(), $consultation->getOrganisme(), $connexion);
                    if ($annonce instanceof CommonTAnnonceConsultation) {
                        $supportAnnonce = Atexo_Publicite_AnnonceSub::getSupportAnnonceBySupportPub($annonce->getId(), $support->getId(), $connexion);
                        if ($supportAnnonce instanceof CommonTSupportAnnonceConsultation) {
                            $support->setSelectedOffre($supportAnnonce->getIdOffre());
                        }
                    }
                    if (!$support->getSelectedOffre()) {
                        $dataSource = $this->getListeOffres($support);
                        foreach ($dataSource as $data) {
                            if ($data instanceof CommonTOffreSupportPublicite) {
                                $support->setSelectedOffre($data->getId());
                                break;
                            }
                        }
                    }
                }
            }
            $return = ($idOffre == $support->getSelectedOffre()) ? 'selected="selected"' : '';
        }

        return $return;
    }

    public function getSupportChecked($support)
    {
        $checked = false;
        $consultation = $this->getConsultation();
        if ($consultation instanceof CommonConsultation && $support instanceof CommonTSupportPublication) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
            $annonce = Atexo_Publicite_AnnonceSub::getAnnonceSimplifieEncours($consultation->getId(), $consultation->getOrganisme(), $connexion);
            if ($annonce instanceof CommonTAnnonceConsultation) {
                $supportAnnonce = Atexo_Publicite_AnnonceSub::getSupportAnnonceBySupportPub($annonce->getId(), $support->getId(), $connexion);
                if ($supportAnnonce instanceof CommonTSupportAnnonceConsultation) {
                    $checked = true;
                }
            }
        }

        return $checked;
    }

    public function supportSelected($consultation, $etape)
    {
        $supportsWithCode = [];
        $supportsWithOutCode = [];
        if ('PubliciteConsultation' == $etape) {
            $supports = $this->recupererSupportsSelectionnes();
            if (isset($supports['listeSupport']) && is_array($supports['listeSupport'])) {
                foreach ($supports['listeSupport'] as $idSupport => $support) {
                    $supportAnnonce = new CommonTSupportAnnonceConsultation();
                    $supportAnnonce->setIdSupport($idSupport);
                    $supportAnnonce->setIdOffre($support['idOffre']);
                    if ($support['code']) {
                        $supportsWithCode[] = $supportAnnonce;
                    } else {
                        $supportsWithOutCode[] = $supportAnnonce;
                    }
                    unset($supportAnnonce);
                }
            }
        } else {
            if ($consultation instanceof CommonConsultation) {
                $annonce = Atexo_Publicite_AnnonceSub::getAnnonceSimplifie($consultation->getId(), $consultation->getOrganisme());
                if ($annonce instanceof CommonTAnnonceConsultation) {
                    $supports = $annonce->getSupportsAnnonce();
                    foreach ($supports as $support) {
                        if ($support->getCodeSupport()) {
                            $supportsWithCode[] = $support;
                        } else {
                            $supportsWithOutCode[] = $support;
                        }
                    }
                }
            }
        }
        $supportsSelectionnee = [...$supportsWithOutCode, ...$supportsWithCode];

        return $supportsSelectionnee;
    }

    public function deleteAnnonce($consultation)
    {
        $supports = null;
        try {
            $logger = Atexo_LoggerManager::getLogger('publicite');
            if ($consultation instanceof CommonConsultation) {
                $delete = false;
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
                $annonce = Atexo_Publicite_AnnonceSub::getAnnonceSimplifie($consultation->getId(), $consultation->getOrganisme(), $connexion);
                if ($annonce instanceof CommonTAnnonceConsultation) {
                    if ('1' != $consultation->getDonneePubliciteObligatoire()) {
                        $delete = true;
                        $logger->info('AtexoPubliciteConsultation::deleteAnnonce Consultation '.$consultation->getId()." la pub n'est pas obligatoire : ".$consultation->getDonneePubliciteObligatoire());
                    } else {
                        $supports = $annonce->getSupportsAnnonce();
                        if (empty($supports)) {
                            $delete = true;
                            $logger->info('AtexoPubliciteConsultation::deleteAnnonce Consultation '.$consultation->getId()." l'annonce ".$annonce->getId()." n'a aucun support ".print_r($supports, true));
                        }
                    }
                    if ($delete) {
                        if (is_array($supports)) {
                            foreach ($supports as $s) {
                                if ($s instanceof CommonTSupportAnnonceConsultation) {
                                    $s->delete($connexion);
                                }
                            }
                        }
                        $annonce->delete($connexion);
                        $logger->info('AtexoPubliciteConsultation::deleteAnnonce Consultation '.$consultation->getId()." l'annonce est supprime id : ".$annonce->getId());
                    }
                }
            }
        } catch (Exception $e) {
            $logger->error('Consultation '.$consultation->getId()."Erreur s'est produite lors suppression de l'annonce  simplifié : ".$e->getMessage().' '.$e->getTraceAsString());
        }
    }

    public function getListeOffres($support)
    {
        if ($support instanceof CommonTSupportPublication) {
            $consultation = $this->getConsultation();
            $idAnnonce = 0;
            $montant = 0;
            if ($consultation instanceof CommonConsultation) {
                if (null === $this->mapa) {
                    $typeProcedure = $consultation->getTypeProcedureOrg();
                    if ($typeProcedure instanceof CommonTypeProcedureOrganisme) {
                        $this->mapa = $typeProcedure->getMapa();
                    }
                }
                $donneesComplementaire = $consultation->getDonneComplementaire();
                if ($donneesComplementaire instanceof CommonTDonneeComplementaire) {
                    $montant = $donneesComplementaire->getMontantMarche();
                }
                $annonce = Atexo_Publicite_AnnonceSub::getAnnonceSimplifie($consultation->getId(), $consultation->getOrganisme());
                if ($annonce instanceof CommonTAnnonceConsultation) {
                    $idAnnonce = $annonce->getId();
                }
            }

            return $support->getListeOffres($this->mapa, $montant, Atexo_CurrentUser::getId(), Atexo_CurrentUser::getOrganismAcronym(), $idAnnonce);
        }
    }

    public function updateStatutAnnonce($consultation, $statut)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        if ($consultation instanceof CommonConsultation) {
            $annonce = Atexo_Publicite_AnnonceSub::getAnnonceSimplifieEncours($consultation->getId(), $consultation->getOrganisme(), $connexion);
            if ($annonce instanceof CommonTAnnonceConsultation && $annonce->getStatut() != $statut) {
                $connexion->beginTransaction();
                $dateStatut = date('Y-m-d H:i:s');
                $annonce->setStatut($statut);
                $annonce->setDateStatut($dateStatut);
                $annonce->save($connexion);
                $save = Atexo_Publicite_AnnonceSub::updateAllStatutSupport($annonce->getId(), $statut, $dateStatut, $connexion);
                if ($save) {
                    $connexion->commit();
                    //historique apres le commite car il ya des info qui doivent s'enregistrer en base avant de creer l'historique
                    Atexo_Publicite_AnnonceSub::ajouterHistorique($annonce, $connexion);
                }
            }
        }
    }

    public function chargerDepartements($item, $listeDepartements)
    {
        $departements = Atexo_Geolocalisation_GeolocalisationN2::retrieveGeolocalisationN2byIds($listeDepartements);

        $result = [];

        foreach ($departements as $departement) {
            $result[$departement->getId()] = $departement->getDenomination1();
        }

        if (is_array($result) && count($result)) {
            $item->departementParution->DataSource = $result;
            $item->departementParution->DataBind();
        }
    }

    /**
     * @param $support
     *
     * @return string
     *
     * @throws PropelException
     */
    public function getMessageSupportValue($support)
    {
        $message = '';

        $consultation = $this->getConsultation();

        if ($consultation instanceof CommonConsultation && $support instanceof CommonTSupportPublication) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
            $annonce = Atexo_Publicite_AnnonceSub::getAnnonceSimplifieEncours($consultation->getId(), $consultation->getOrganisme(), $connexion);

            if ($annonce instanceof CommonTAnnonceConsultation) {
                $supportAnnonce = Atexo_Publicite_AnnonceSub::getSupportAnnonceBySupportPub($annonce->getId(), $support->getId(), $connexion);

                if ($supportAnnonce instanceof CommonTSupportAnnonceConsultation &&
                    null != $supportAnnonce->getMessageAcheteur() &&
                    '' != $supportAnnonce->getMessageAcheteur()
                ) {
                    $message = $supportAnnonce->getMessageAcheteur();
                }
            }
        }

        return $message;
    }

    /**
     * @param $support
     * @param bool $values
     *
     *
     * @throws PropelException
     */
    public function getDepartementsParutionSelected($support, $values = true): array|string
    {
        $departements = '';

        $consultation = $this->getConsultation();

        if ($consultation instanceof CommonConsultation &&
            $support instanceof CommonTSupportPublication &&
            $support->getSelectionDepartementsParution()
        ) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
            $annonce = Atexo_Publicite_AnnonceSub::getAnnonceSimplifieEncours($consultation->getId(), $consultation->getOrganisme(), $connexion);

            if ($annonce instanceof CommonTAnnonceConsultation) {
                $supportAnnonce = Atexo_Publicite_AnnonceSub::getSupportAnnonceBySupportPub($annonce->getId(), $support->getId(), $connexion);

                if ($supportAnnonce instanceof CommonTSupportAnnonceConsultation &&
                    null != $supportAnnonce->getDepartementsParutionAnnonce()
                ) {
                    $departements = explode(',', $supportAnnonce->getDepartementsParutionAnnonce());

                    if (!$values) {
                        $departements = $supportAnnonce->getDepartementsParutionAnnonce();
                    }
                }
            }
        }

        return $departements;
    }

    public function gererAffichageSupport()
    {
        foreach ($this->listeSupport->Items as $item) {
            if ($item->elementActif->value) {
                if ($item->choixSupport->checked) {
                    $item->panelSuppport->setDisplay('Dynamic');
                }
                if ($item->selectionDepartementsParution->value) {
                    $listeDepartements = explode(',', $item->departementsParution->value);
                    $this->chargerDepartements($item, $listeDepartements);
                }
            }
        }
    }
}
