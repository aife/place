<!--Debut Ligne Achat responsable-->
	<div class="line" id="panel_achatResponsable">
		<div class="intitule col-150">
			<com:TTranslate>DEFINE_ACHAT_RESPONSABLE</com:TTranslate><span style="display:<%=$this->enabledElement()?'':'none'%>" class="champ-oblig">*</span> :</div>
		<div id="achat-responsable" class="content-bloc bloc-580">
			<clauses-n1 v-for="(value, index) in values"
							   :data="value"
							   :token="token"
							   :values-save="valuesSave"
							   :key="index"
						       :uri="'<%=$this->getUrl()%>'"
						       :yes-label="'<%=$this->getLabelTrans('DEFINE_OUI')%>'"
						       :no-label="'<%=$this->getLabelTrans('DEFINE_NON')%>'"
						       :required-field="'<%=$this->getLabelTrans('CHAMP_OBLIGATOIRE')%>'"
						       :search-criteria="'<%=$this->getLabelTrans('RECHERCHER_UN_CRITERE')%>'"
						       :tiny-emitter="tinyEmitter"
							   @send-add-value-clause1="add"
							   @send-remove-value-clause1="remove"
			></clauses-n1>
		</div>
		<com:TTextBox Display="None" id="achatResponsableFormValue"/>
		<com:TCustomValidator
				Id="validatorClauseSocialesIdentification"
				ValidationGroup="<%=($this->getValidationGroup())%>"
				ControlToValidate="achatResponsableFormValue"
				ClientValidationFunction="validateClauseSocialeIdentification"
				Enabled="<%=($this->getSourcePage() == 'etapeIdentification')%>"
				Display="Dynamic"
				EnableClientScript="true"
				ErrorMessage="<%=Prado::localize('DEFINE_CLAUSE_SOCIALE_INSERTION_HANDICAP_ERREUR')%>"
				Text="<span title='<%=Prado::localize('CHAMP_OBLIGATOIRE')%>' class='check-invalide TestMessage'></span>">
			<prop:ClientSide.OnValidationError>
				blockErreur = '<%=($this->getValidationSummary())%>';
				if(document.getElementById(blockErreur))
				document.getElementById(blockErreur).style.display='';
				if(document.getElementById('ctl0_CONTENU_PAGE_blocBoutons'))
				{
					document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
				}
			</prop:ClientSide.OnValidationError>
		</com:TCustomValidator>
		<com:TCustomValidator
				Id="validatorClauseEnvironnementalesIdentification"
				ValidationGroup="<%=($this->getValidationGroup())%>"
				ControlToValidate="achatResponsableFormValue"
				ClientValidationFunction="validateClauseEnvironnementaleIdentification"
				Enabled="<%=($this->getSourcePage() == 'etapeIdentification')%>"
				Display="Dynamic"
				EnableClientScript="true"
				ErrorMessage="<%=Prado::localize('CLAUSES_ENVIRONNEMENTALES_MESSAGE')%>"
				Text="<span title='Champ obligatoire' class='check-invalide TestMessage'></span>">
			<prop:ClientSide.OnValidationError>
				blockErreur = '<%=($this->getValidationSummary())%>';
				if(document.getElementById(blockErreur))
				document.getElementById(blockErreur).style.display='';
				if(document.getElementById('ctl0_CONTENU_PAGE_blocBoutons'))
				{
				document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
				}
			</prop:ClientSide.OnValidationError>
		</com:TCustomValidator>
		<com:TCustomValidator
				Id="validatorClauseSocialesLot"
				ValidationGroup="<%=($this->getValidationGroup())%>"
				ControlToValidate="achatResponsableFormValue"
				ClientValidationFunction="validateClauseSocialeLots"
				Enabled="<%=($this->getSourcePage() == 'detailLots')%>"
				Display="Dynamic"
				EnableClientScript="true"
				ErrorMessage="<%=Prado::localize('DEFINE_CLAUSE_SOCIALE_INSERTION_HANDICAP_ERREUR')%>"
				Text="<span title='Champ obligatoire' class='check-invalide TestMessage'></span>">
			<prop:ClientSide.OnValidationError>
				blockErreur = '<%=($this->getValidationSummary())%>';
				if(document.getElementById(blockErreur))
				document.getElementById(blockErreur).style.display='';
				if(document.getElementById('ctl0_CONTENU_PAGE_blocBoutons'))
				{
				document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
				}
			</prop:ClientSide.OnValidationError>
		</com:TCustomValidator>
		<com:TCustomValidator
				Id="validateClauseEnvironnementalesLot"
				ValidationGroup="<%=($this->getValidationGroup())%>"
				ControlToValidate="achatResponsableFormValue"
				ClientValidationFunction="validateClauseEnvironnementaleLots"
				Enabled="<%=($this->getSourcePage() == 'detailLots')%>"
				Display="Dynamic"
				EnableClientScript="true"
				ErrorMessage="<%=Prado::localize('CLAUSES_ENVIRONNEMENTALES_MESSAGE')%>"
				Text="<span title='Champ obligatoire' class='check-invalide'></span>">
			<prop:ClientSide.OnValidationError>
				blockErreur = '<%=($this->getValidationSummary())%>';
				if(document.getElementById(blockErreur))
				document.getElementById(blockErreur).style.display='';
			</prop:ClientSide.OnValidationError>
		</com:TCustomValidator>


		<!-- -->
		<a href="#"
		   onmouseover="afficheBulle('infoJustificationNonAllotie2')"
		   onmouseout="cacheBulle('infoJustificationNonAllotie2')"
		   onfocus="afficheBulle('infoJustificationNonAllotie2')"
		   onblur="cacheBulle('infoJustificationNonAllotie2')" class="picto-info-intitule" id="considerationNonAllotieInfo">
			<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" />
		</a>
		<div onmouseout="mouseOutBulle();"
			 onmouseover="mouseOverBulle();"
			 class="info-bulle" id="infoJustificationNonAllotie2">
			<div><com:TTranslate>DEFINE_INFO_BULLE_CONSIDERATION_SOCIALE</com:TTranslate></div>
		</div>
		<!-- -->
	</div>
	<com:TLabel id="script" />
<!--Fin Ligne Achat responsable-->

