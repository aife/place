<!--Debut Bloc barre etape action-->
<com:TConditional condition="Application\Service\Atexo\Atexo_Config::getParameter('ACTIVE_ELABORATION_V2') == '1'">
    <prop:FalseTemplate>
        <com:TPanel cssclass="etape first current" id="etapeIdentification">
            <div>
                <com:TActiveLinkButton id="goToEtapeIdentification" ValidationGroup="<%=$this->getValidationGroup()%>" OnCallBack="goToEtapeIdentification" Attributes.onClick="return actionJsAvantSauvegarde('<%=$this->Page->consultationExiste->getClientId()%>','<%=$this->etapeToGo->getClientId()%>','goToEtapeIdentification');">
                    <com:TTranslate>DEFINE_IDENTIFICATION</com:TTranslate>
                    <prop:ClientSide.OnLoading>showLoader();</prop:ClientSide.OnLoading>
                    <prop:ClientSide.OnFailure>showLoader();</prop:ClientSide.OnFailure>
                    <prop:ClientSide.OnSuccess>showLoader();remonter();</prop:ClientSide.OnSuccess>
                    <prop:ClientSide.RequestTimeOut>60000</prop:ClientSide.RequestTimeOut>
                </com:TActiveLinkButton>
            </div>
        </com:TPanel>
        <com:TActivePanel cssclass="etape" id="etapeDonneesComplementaires" Visible="<%=Application\Service\Atexo\Atexo_Module::isEnabled('DonneesComplementaires')%>">
            <div>
                <com:TActiveLinkButton id="goToEtapeDonneesComplementaires" ValidationGroup="<%=$this->getValidationGroup()%>" OnCallBack ="goToEtapeDonneesComplementaires"  Attributes.onClick="return actionJsAvantSauvegarde('<%=$this->Page->consultationExiste->getClientId()%>','<%=$this->etapeToGo->getClientId()%>','goToEtapeDonneesComplementaires');">
                    <prop:ClientSide.OnLoading>showLoader();</prop:ClientSide.OnLoading>
                    <prop:ClientSide.OnFailure>showLoader();</prop:ClientSide.OnFailure>
                    <prop:ClientSide.OnSuccess>showLoader();remonter();</prop:ClientSide.OnSuccess>
                    <prop:ClientSide.RequestTimeOut>60000</prop:ClientSide.RequestTimeOut>
                    <com:TTranslate>DEFINE_DONNEES</com:TTranslate><br /> <%=(Application\Service\Atexo\Atexo_Module::isEnabled('Publicite') && !Application\Service\Atexo\Atexo_Module::isEnabled('InterfaceModuleRsem'))? Prado::localize('DEFINE_COMPLEMENTAIRES_PUB') : Prado::localize('DEFINE_COMPLEMENTAIRES')%>
                </com:TActiveLinkButton>
            </div>
        </com:TActivePanel>
        <com:TActivePanel cssclass="etape etape-off" id="etapeDonneesComplementairesInactif" Visible="<%=Application\Service\Atexo\Atexo_Module::isEnabled('DonneesComplementaires') && Application\Service\Atexo\Atexo_Module::isEnabled('Publicite')%>">
            <div>
                <com:TTranslate>DEFINE_DONNEES</com:TTranslate><br /> <%=(Application\Service\Atexo\Atexo_Module::isEnabled('Publicite') && !Application\Service\Atexo\Atexo_Module::isEnabled('InterfaceModuleRsem'))? Prado::localize('DEFINE_COMPLEMENTAIRES_PUB') : Prado::localize('DEFINE_COMPLEMENTAIRES')%>
            </div>
        </com:TActivePanel>
        <com:TActivePanel cssclass="etape" id="etapeDetailsLots" >
            <div>
                <com:TActiveLinkButton id="goToEtapeDetailsLots" ValidationGroup="<%=$this->getValidationGroup()%>" OnCallBack="goToEtapeDetailsLots" Attributes.onClick="return actionJsAvantSauvegarde('<%=$this->Page->consultationExiste->getClientId()%>','<%=$this->etapeToGo->getClientId()%>','goToEtapeDetailsLots');">
                    <com:TTranslate>DEFINE_LOTS</com:TTranslate>
                    <prop:ClientSide.OnLoading>showLoader();</prop:ClientSide.OnLoading>
                    <prop:ClientSide.OnFailure>showLoader();</prop:ClientSide.OnFailure>
                    <prop:ClientSide.OnSuccess>showLoader();remonter();</prop:ClientSide.OnSuccess>
                    <prop:ClientSide.RequestTimeOut>60000</prop:ClientSide.RequestTimeOut>
                </com:TActiveLinkButton>
            </div>
        </com:TActivePanel>
        <com:TActivePanel cssclass="etape etape-off" id="etapeDetailsLotsInactif">
            <div>
                <com:TLabel><com:TTranslate>DEFINE_LOTS</com:TTranslate></com:TLabel>
            </div>
        </com:TActivePanel>
        <com:TActivePanel cssclass="etape" id="etapeDumeAcheteur" Visible="<%=Application\Service\Atexo\Atexo_Module::isEnabled('InterfaceDume')%>" >
            <div>
                <com:TActiveLinkButton id="goToEtapeDumeAcheteur" ValidationGroup="<%=$this->getValidationGroup()%>" OnCallBack="goToEtapeDumeAcheteurs" Attributes.onClick="return actionJsAvantSauvegarde('<%=$this->Page->consultationExiste->getClientId()%>','<%=$this->etapeToGo->getClientId()%>','goToEtapeDumeAcheteurs');">
                    <com:TTranslate>DUME_ACHETEUR</com:TTranslate>
                    <prop:ClientSide.OnLoading>showLoader();</prop:ClientSide.OnLoading>
                    <prop:ClientSide.OnFailure>showLoader();</prop:ClientSide.OnFailure>
                    <prop:ClientSide.OnSuccess>showLoader();remonter();</prop:ClientSide.OnSuccess>
                    <prop:ClientSide.RequestTimeOut>60000</prop:ClientSide.RequestTimeOut>
                </com:TActiveLinkButton>
            </div>
        </com:TActivePanel>
        <com:TActivePanel cssclass="etape etape-off" id="etapeDumeAcheteurInactif" Visible="<%=Application\Service\Atexo\Atexo_Module::isEnabled('InterfaceDume')%>">
            <div>
                <com:TLabel><com:TTranslate>DUME_ACHETEUR</com:TTranslate></com:TLabel>
            </div>
        </com:TActivePanel>
        <com:TPanel cssclass="etape" id="etapeCalendrier">
            <div>
                <com:TActiveLinkButton id="goToEtapeCalendrier" ValidationGroup="<%=$this->getValidationGroup()%>" OnCallBack="goToEtapeCalendrier" Attributes.onClick="return actionJsAvantSauvegarde('<%=$this->Page->consultationExiste->getClientId()%>','<%=$this->etapeToGo->getClientId()%>','goToEtapeCalendrier');">
                    <com:TTranslate>DEFINE_CALENDRIER</com:TTranslate>
                    <prop:ClientSide.OnLoading>showLoader();</prop:ClientSide.OnLoading>
                    <prop:ClientSide.OnFailure>showLoader();</prop:ClientSide.OnFailure>
                    <prop:ClientSide.OnSuccess>showLoader();remonter();</prop:ClientSide.OnSuccess>
                    <prop:ClientSide.RequestTimeOut>60000</prop:ClientSide.RequestTimeOut>
                </com:TActiveLinkButton>
            </div>
        </com:TPanel>
        <com:TPanel cssclass="etape " id="etapeDocumentsJoints">
            <div>
                <com:TActiveLinkButton id="goToEtapeDocumentsJoints"  ValidationGroup="<%=$this->getValidationGroup()%>" OnCallBack="goToEtapeDocumentsJoints" Attributes.onClick="return actionJsAvantSauvegarde('<%=$this->Page->consultationExiste->getClientId()%>','<%=$this->etapeToGo->getClientId()%>','goToEtapeDocumentsJoints');">
                    <com:TTranslate>DEFINE_PIECES</com:TTranslate>
                    <prop:ClientSide.OnLoading>showLoader();</prop:ClientSide.OnLoading>
                    <prop:ClientSide.OnFailure>showLoader();</prop:ClientSide.OnFailure>
                    <prop:ClientSide.OnSuccess>showLoader();remonter();</prop:ClientSide.OnSuccess>
                    <prop:ClientSide.RequestTimeOut>60000</prop:ClientSide.RequestTimeOut>
                </com:TActiveLinkButton></div>
        </com:TPanel>
        <com:TPanel cssclass="etape " id="etapeModalitesReponse">
            <div>
                <com:TActiveLinkButton id="goToEtapeModalitesReponse"  ValidationGroup="<%=$this->getValidationGroup()%>" OnCallBack="goToEtapeModalitesReponse" Attributes.onClick="return actionJsAvantSauvegarde('<%=$this->Page->consultationExiste->getClientId()%>','<%=$this->etapeToGo->getClientId()%>','goToEtapeModalitesReponse');">
                    <com:TTranslate>DEFINE_TEXT_MODALITES</com:TTranslate>
                    <prop:ClientSide.OnLoading>showLoader();</prop:ClientSide.OnLoading>
                    <prop:ClientSide.OnFailure>showLoader();</prop:ClientSide.OnFailure>
                    <prop:ClientSide.OnSuccess>showLoader();remonter();</prop:ClientSide.OnSuccess>
                    <prop:ClientSide.RequestTimeOut>60000</prop:ClientSide.RequestTimeOut>
                </com:TActiveLinkButton>
            </div>
        </com:TPanel>
        <com:TPanel cssclass="etape last" id="etapeDroitsAcces">
            <div>
                <com:TActiveLinkButton id="goToEtapeDroitsAcces"  ValidationGroup="<%=$this->getValidationGroup()%>" OnCallBack="goToEtapeDroitsAcces" Attributes.onClick="return actionJsAvantSauvegarde('<%=$this->Page->consultationExiste->getClientId()%>','<%=$this->etapeToGo->getClientId()%>','goToEtapeDroitsAcces');">
                    <com:TTranslate>TEXT_ACCES</com:TTranslate>
                    <prop:ClientSide.OnLoading>showLoader();</prop:ClientSide.OnLoading>
                    <prop:ClientSide.OnFailure>showLoader();</prop:ClientSide.OnFailure>
                    <prop:ClientSide.OnSuccess>showLoader();remonter();</prop:ClientSide.OnSuccess>
                    <prop:ClientSide.RequestTimeOut>60000</prop:ClientSide.RequestTimeOut>
                </com:TActiveLinkButton>
            </div>
        </com:TPanel>
        <com:TActivePanel cssclass="etape last" id="PubliciteConsultation"  Visible="<%=Application\Service\Atexo\Atexo_Module::isEnabled('Publicite')%>">
            <div>
                <!--
                <div class="progress-circle" data-toggle="popover" data-trigger="hover" data-placement="top" data-popover-content=".popover">
                    <input type="text" readonly="readonly" id="pubProgress" class="dial" />
                </div>
                -->
                <com:TActiveLinkButton id="publicite" ValidationGroup="<%=$this->getValidationGroup()%>" OnCallBack="goToEtapePublicite" Attributes.onClick="return actionJsAvantSauvegarde('<%=$this->Page->consultationExiste->getClientId()%>','<%=$this->etapeToGo->getClientId()%>','goToEtapePublicite');">
                    <com:TTranslate>PUBLICITE_CONSULTATION</com:TTranslate>
                    <prop:ClientSide.OnLoading>showLoader();</prop:ClientSide.OnLoading>
                    <prop:ClientSide.OnFailure>hideLoader();</prop:ClientSide.OnFailure>
                    <prop:ClientSide.OnSuccess>hideLoader();remonter();</prop:ClientSide.OnSuccess>
                    <prop:ClientSide.RequestTimeOut>60000</prop:ClientSide.RequestTimeOut>
                </com:TActiveLinkButton>
            </div>
        </com:TActivePanel>
        <com:TActivePanel cssclass="etape etape-off" id="PubliciteConsultationInactif" Visible="<%=Application\Service\Atexo\Atexo_Module::isEnabled('Publicite')%>">
            <div>
                <com:TLabel><com:TTranslate>PUBLICITE_CONSULTATION</com:TTranslate></com:TLabel>
            </div>
        </com:TActivePanel>
    </prop:FalseTemplate>
    <prop:TrueTemplate>
        <com:TPanel cssclass="step active" id="etapeIdentification">
            <div>
                <com:TActiveLinkButton cssclass="step-trigger" id="goToEtapeIdentification" ValidationGroup="<%=$this->getValidationGroup()%>" OnCallBack="goToEtapeIdentification" Attributes.onClick="return actionJsAvantSauvegarde('<%=$this->Page->consultationExiste->getClientId()%>','<%=$this->etapeToGo->getClientId()%>','goToEtapeIdentification');">
                    <prop:ClientSide.OnLoading>showLoader();</prop:ClientSide.OnLoading>
                    <prop:ClientSide.OnFailure>showLoader();</prop:ClientSide.OnFailure>
                    <prop:ClientSide.OnSuccess>showLoader();remonter();</prop:ClientSide.OnSuccess>
                    <prop:ClientSide.RequestTimeOut>60000</prop:ClientSide.RequestTimeOut>
                    <span class="bs-stepper-circle step-size-small">1</span>
                    <span class="bs-stepper-label step-text-size">Identification</span>
                </com:TActiveLinkButton>
            </div>
        </com:TPanel>
        <div class="bs-stepper-line"></div>
        <com:TActivePanel cssclass="step" id="etapeDonneesComplementaires"
                          Visible="<%=($this->Page->getConsultation()->getDonneeComplementaireObligatoire() || $this->Page->getConsultation()->getDonneePubliciteObligatoire())%>">
            <div>
                <com:TActiveLinkButton cssclass="step-trigger" id="goToEtapeDonneesComplementaires" ValidationGroup="<%=$this->getValidationGroup()%>" OnCallBack ="goToEtapeDonneesComplementaires"  Attributes.onClick="return actionJsAvantSauvegarde('<%=$this->Page->consultationExiste->getClientId()%>','<%=$this->etapeToGo->getClientId()%>','goToEtapeDonneesComplementaires');">
                    <prop:ClientSide.OnLoading>showLoader();</prop:ClientSide.OnLoading>
                    <prop:ClientSide.OnFailure>showLoader();</prop:ClientSide.OnFailure>
                    <prop:ClientSide.OnSuccess>showLoader();remonter();</prop:ClientSide.OnSuccess>
                    <prop:ClientSide.RequestTimeOut>60000</prop:ClientSide.RequestTimeOut>
                    <span class="bs-stepper-circle step-size-small">2</span>
                    <span class="bs-stepper-label step-text-size"><com:TTranslate>DEFINE_DONNEES</com:TTranslate> <com:TTranslate>DEFINE_COMPLEMENTAIRES</com:TTranslate></span>
                </com:TActiveLinkButton>
            </div>
        </com:TActivePanel>
        <com:TActivePanel cssclass="step step-off" id="etapeDonneesComplementairesInactif"
                          Visible="<%=(!$this->Page->getConsultation()->getDonneeComplementaireObligatoire() && !$this->Page->getConsultation()->getDonneePubliciteObligatoire())%>">
            <div class="step-trigger default-cursor">
                <span class="bs-stepper-circle step-size-small">2</span>
                <span class="bs-stepper-label step-text-size"><com:TTranslate>DEFINE_DONNEES</com:TTranslate> <com:TTranslate>DEFINE_COMPLEMENTAIRES</com:TTranslate></span>
            </div>
        </com:TActivePanel>
        <div class="bs-stepper-line"></div>
        <com:TActivePanel cssclass="step" id="etapeDetailsLots" >
            <div>
                <com:TActiveLinkButton cssclass="step-trigger" id="goToEtapeDetailsLots" ValidationGroup="<%=$this->getValidationGroup()%>" OnCallBack="goToEtapeDetailsLots" Attributes.onClick="return actionJsAvantSauvegarde('<%=$this->Page->consultationExiste->getClientId()%>','<%=$this->etapeToGo->getClientId()%>','goToEtapeDetailsLots');">
                    <prop:ClientSide.OnLoading>showLoader();</prop:ClientSide.OnLoading>
                    <prop:ClientSide.OnFailure>showLoader();</prop:ClientSide.OnFailure>
                    <prop:ClientSide.OnSuccess>showLoader();remonter();</prop:ClientSide.OnSuccess>
                    <prop:ClientSide.RequestTimeOut>60000</prop:ClientSide.RequestTimeOut>
                    <span class="bs-stepper-circle step-size-small">3</span>
                    <span class="bs-stepper-label step-text-size"><com:TTranslate>DEFINE_LOTS</com:TTranslate></span>
                </com:TActiveLinkButton>
            </div>
        </com:TActivePanel>
        <com:TActivePanel cssclass="step step-off" id="etapeDetailsLotsInactif">
            <div class="step-trigger default-cursor">
                <span class="bs-stepper-circle step-size-small">3</span>
                <span class="bs-stepper-label step-text-size"><com:TTranslate>DEFINE_LOTS</com:TTranslate></span>
            </div>
        </com:TActivePanel>
        <div class="bs-stepper-line"></div>
        <com:TActivePanel cssclass="step " id="etapeDumeAcheteur" Visible="<%=Application\Service\Atexo\Atexo_Module::isEnabled('InterfaceDume')%>">
            <div>
                <com:TActiveLinkButton cssclass="step-trigger" id="goToEtapeDumeAcheteur" ValidationGroup="<%=$this->getValidationGroup()%>" OnCallBack="goToEtapeDumeAcheteurs" Attributes.onClick="return actionJsAvantSauvegarde('<%=$this->Page->consultationExiste->getClientId()%>','<%=$this->etapeToGo->getClientId()%>','goToEtapeDumeAcheteurs');">
                    <prop:ClientSide.OnLoading>showLoader();</prop:ClientSide.OnLoading>
                    <prop:ClientSide.OnFailure>showLoader();</prop:ClientSide.OnFailure>
                    <prop:ClientSide.OnSuccess>showLoader();remonter();</prop:ClientSide.OnSuccess>
                    <prop:ClientSide.RequestTimeOut>60000</prop:ClientSide.RequestTimeOut>
                    <span class="bs-stepper-circle step-size-small">4</span>
                    <span class="bs-stepper-label step-text-size"><com:TTranslate>DUME_ACHETEUR</com:TTranslate></span>
                </com:TActiveLinkButton>
            </div>
        </com:TActivePanel>
        <com:TActivePanel cssclass="step step-off" id="etapeDumeAcheteurInactif" Visible="<%=Application\Service\Atexo\Atexo_Module::isEnabled('InterfaceDume')%>">
            <div class="step-trigger default-cursor">
                <span class="bs-stepper-circle step-size-small">4</span>
                <span class="bs-stepper-label step-text-size"><com:TTranslate>DUME_ACHETEUR</com:TTranslate></span>
            </div>
        </com:TActivePanel>
        <div class="bs-stepper-line"></div>
        <com:TPanel cssclass="step" id="etapeCalendrier">
            <div>
                <com:TActiveLinkButton cssclass="step-trigger" id="goToEtapeCalendrier" ValidationGroup="<%=$this->getValidationGroup()%>" OnCallBack="goToEtapeCalendrier" Attributes.onClick="return actionJsAvantSauvegarde('<%=$this->Page->consultationExiste->getClientId()%>','<%=$this->etapeToGo->getClientId()%>','goToEtapeCalendrier');">
                    <prop:ClientSide.OnLoading>showLoader();</prop:ClientSide.OnLoading>
                    <prop:ClientSide.OnFailure>showLoader();</prop:ClientSide.OnFailure>
                    <prop:ClientSide.OnSuccess>showLoader();remonter();</prop:ClientSide.OnSuccess>
                    <prop:ClientSide.RequestTimeOut>60000</prop:ClientSide.RequestTimeOut>
                    <span class="bs-stepper-circle step-size-small">5</span>
                    <span class="bs-stepper-label step-text-size"><com:TTranslate>DEFINE_CALENDRIER</com:TTranslate></span>
                </com:TActiveLinkButton>
            </div>
        </com:TPanel>
        <div class="bs-stepper-line"></div>
        <com:TPanel cssclass="step " id="etapeDocumentsJoints">
            <div>
                <com:TActiveLinkButton cssclass="step-trigger" id="goToEtapeDocumentsJoints"  ValidationGroup="<%=$this->getValidationGroup()%>" OnCallBack="goToEtapeDocumentsJoints" Attributes.onClick="return actionJsAvantSauvegarde('<%=$this->Page->consultationExiste->getClientId()%>','<%=$this->etapeToGo->getClientId()%>','goToEtapeDocumentsJoints');">
                    <prop:ClientSide.OnLoading>showLoader();</prop:ClientSide.OnLoading>
                    <prop:ClientSide.OnFailure>showLoader();</prop:ClientSide.OnFailure>
                    <prop:ClientSide.OnSuccess>showLoader();remonter();</prop:ClientSide.OnSuccess>
                    <prop:ClientSide.RequestTimeOut>60000</prop:ClientSide.RequestTimeOut>
                    <span class="bs-stepper-circle step-size-small">6</span>
                    <span class="bs-stepper-label step-text-size"><com:TTranslate>DEFINE_PIECES</com:TTranslate></span>
                </com:TActiveLinkButton></div>
        </com:TPanel>
        <div class="bs-stepper-line"></div>
        <com:TPanel cssclass="step " id="etapeModalitesReponse">
            <div>
                <com:TActiveLinkButton cssclass="step-trigger" id="goToEtapeModalitesReponse"  ValidationGroup="<%=$this->getValidationGroup()%>" OnCallBack="goToEtapeModalitesReponse" Attributes.onClick="return actionJsAvantSauvegarde('<%=$this->Page->consultationExiste->getClientId()%>','<%=$this->etapeToGo->getClientId()%>','goToEtapeModalitesReponse');">
                    <prop:ClientSide.OnLoading>showLoader();</prop:ClientSide.OnLoading>
                    <prop:ClientSide.OnFailure>showLoader();</prop:ClientSide.OnFailure>
                    <prop:ClientSide.OnSuccess>showLoader();remonter();</prop:ClientSide.OnSuccess>
                    <prop:ClientSide.RequestTimeOut>60000</prop:ClientSide.RequestTimeOut>
                    <span class="bs-stepper-circle step-size-small">7</span>
                    <span class="bs-stepper-label step-text-size"><com:TTranslate>DEFINE_TEXT_MODALITES</com:TTranslate></span>
                </com:TActiveLinkButton>
            </div>
        </com:TPanel>
        <div class="bs-stepper-line"></div>
        <com:TPanel cssclass="step last" id="etapeDroitsAcces">
            <div>
                <com:TActiveLinkButton cssclass="step-trigger" id="goToEtapeDroitsAcces"  ValidationGroup="<%=$this->getValidationGroup()%>" OnCallBack="goToEtapeDroitsAcces" Attributes.onClick="return actionJsAvantSauvegarde('<%=$this->Page->consultationExiste->getClientId()%>','<%=$this->etapeToGo->getClientId()%>','goToEtapeDroitsAcces');">
                    <prop:ClientSide.OnLoading>showLoader();</prop:ClientSide.OnLoading>
                    <prop:ClientSide.OnFailure>showLoader();</prop:ClientSide.OnFailure>
                    <prop:ClientSide.OnSuccess>showLoader();remonter();</prop:ClientSide.OnSuccess>
                    <prop:ClientSide.RequestTimeOut>60000</prop:ClientSide.RequestTimeOut>
                    <span class="bs-stepper-circle step-size-small">8</span>
                    <span class="bs-stepper-label step-text-size"><com:TTranslate>TEXT_ACCES</com:TTranslate></span>
                </com:TActiveLinkButton>
            </div>
        </com:TPanel>
        <div class="bs-stepper-line" style="display:<%=Application\Service\Atexo\Atexo_Module::isEnabled('Publicite') ? '':'none';%>"></div>
        <com:TActivePanel cssclass="step last" id="PubliciteConsultation"  Visible="<%=Application\Service\Atexo\Atexo_Module::isEnabled('Publicite')%>">
            <div>
                <com:TActiveLinkButton cssclass="step-trigger" id="goToEtapePublicite" ValidationGroup="<%=$this->getValidationGroup()%>" OnCallBack="goToEtapePublicite" Attributes.onClick="return actionJsAvantSauvegarde('<%=$this->Page->consultationExiste->getClientId()%>','<%=$this->etapeToGo->getClientId()%>','goToEtapePublicite');">
                    <prop:ClientSide.OnLoading>showLoader();</prop:ClientSide.OnLoading>
                    <prop:ClientSide.OnFailure>hideLoader();</prop:ClientSide.OnFailure>
                    <prop:ClientSide.OnSuccess>hideLoader();remonter();</prop:ClientSide.OnSuccess>
                    <prop:ClientSide.RequestTimeOut>60000</prop:ClientSide.RequestTimeOut>
                    <span class="bs-stepper-circle step-size-small">9</span>
                    <span class="bs-stepper-label step-text-size"><com:TTranslate>PUBLICITE_CONSULTATION</com:TTranslate></span>
                </com:TActiveLinkButton>
            </div>
        </com:TActivePanel>
        <com:TActivePanel cssclass="step step-off" id="PubliciteConsultationInactif" Visible="<%=Application\Service\Atexo\Atexo_Module::isEnabled('Publicite')%>">
            <div class="step-trigger default-cursor">
                <span class="bs-stepper-circle step-size-small">9</span>
                <span class="bs-stepper-label step-text-size"><com:TTranslate>PUBLICITE_CONSULTATION</com:TTranslate></span>
            </div>
        </com:TActivePanel>
    </prop:TrueTemplate>
</com:TConditional>
<com:TActiveLabel id="scriptJs"  />
<com:TActiveLabel id="scriptJsPub"  />
<com:THiddenField id="valeurEtapeCourante" />
<com:THiddenField id="valeurEtapeSuivante" />
<com:TConditional condition=" Application\Service\Atexo\Atexo_Config::getParameter('ACTIVER_WEKA')">
    <prop:TrueTemplate>
        <script language="JavaScript" type="text/JavaScript">
            addListnerPage(false);
        </script>
    </prop:TrueTemplate>
</com:TConditional>

<!--Fin Bloc barre etape action-->
<com:TActiveLabel id="scriptJsEtapes" />

<!--Debut Modal-->
<div class="demander-enregistrement" style="display:none;">
    <!--Debut Formulaire-->
    <div class="form-bloc">
        <div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
        <div class="content">
            <com:TTranslate>CONFIRMATION_ENREGISTRMENT_CONSULTATION</com:TTranslate>
        </div>
        <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
    </div>
    <!--Fin Formulaire-->
    <com:TActiveHiddenField ID="etapeToGo" />
    <!--Debut line boutons-->
    <div class="boutons-line">
        <input type="button"
               class="bouton-moyen float-left"
               value="<%=Prado::localize('DEFINE_ANNULER')%>"
               title="<%=Prado::localize('DEFINE_ANNULER')%>"
               onclick="J('.demander-enregistrement').dialog('destroy');return false;"
        />
        <com:TActiveButton
                Text = "<%=Prado::localize('ICONE_OK')%>"
                Attributes.OnClick="getCalendrierJson();J('.demander-enregistrement').dialog('destroy');"
                id="enregistrementEtapeIdentification"
                CssClass="bouton-moyen float-right"
                OnCallBack="goToEtapeFromPopin"
                ValidationGroup="<%=$this->getValidationGroup()%>"
        >
        </com:TActiveButton >
    </div>
    <!--Fin line boutons-->
</div>
<com:TActiveButton
        id="enregistrementEtapeDume"
        OnCallBack="goToEtapeFromPopin"
        ValidationGroup="<%=$this->getValidationGroup()%>"
        style="display:none"
        visible="<%=Application\Service\Atexo\Atexo_Module::isEnabled('InterfaceDume')%>"
/>
<!--Fin Modal-->
<link rel="stylesheet" href="<%=$this->themesPath()%>/css/bs-stepper.min.css?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>" type="text/css" />