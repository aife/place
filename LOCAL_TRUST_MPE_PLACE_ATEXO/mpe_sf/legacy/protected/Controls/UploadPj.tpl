<!--Debut Bloc Echange-->
<div class="form-field">
	<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
	<div class="content">
		<!--Debut Bloc Infos echange-->
		<div class="form-bloc">
			<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
			<div class="content">
				<div class="line">
					<div class="intitule-100 bold">
						<com:TTranslate>DEFINE_TEXT_PJ</com:TTranslate> :
					</div>

					<com:TActivePanel CssClass="content-bloc bloc-660" ID="PanelPJ">
						<com:TRepeater ID="RepeaterPiecesJointes" EnableViewState="true" DataKeyField="Id">
							<prop:ItemTemplate>
								<div class="picto-link inline">
									<a style="display:<%#(!isset($_GET['idCom']))? '':'none'%>" href="<%#$this->Page->IdUploadPj->getLienTelechargementPj($this->Data)%>"><%#$this->Page->IdUploadPj->formaterNomFichierPj($this->Data)%><span class="info-aide"> (<%# Application\Service\Atexo\Atexo_Util::arrondirSizeFile($this->Data->getTaille()/1024)%>)</span></a>
									<a style="display:<%#(isset($_GET['idCom']))? '':'none'%>" href="<%#$this->Page->IdUploadPj->getLienTelechargementPj($this->Data)%>"><%#$this->Page->IdUploadPj->formaterNomFichierPj($this->Data)%><span class="info-aide"> (<%# Application\Service\Atexo\Atexo_Util::arrondirSizeFile($this->Data->getTaille()/1024)%>)</span></a>
									<com:TActiveLinkButton
											visible="<%#(new UploadPj())->getVisibiliteBoutonSupprimer($this->Data)%>"
											CssClass="indent-5"
											OnCallBack="Page.IdUploadPj.refreshRepeaterPJ"
											OnCommand="Page.IdUploadPj.onDeleteClick"
											CommandParameter="<%#$this->Data->getId()%>"
									>
										<com:TImage
												ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-supprimer.gif"
												AlternateText="<%=Prado::localize('DEFINE_SUPPRIMER')%>"
												Attributes.title="<%=Prado::localize('DEFINE_SUPPRIMER')%>"
										/>
									</com:TActiveLinkButton>
								</div>
								<br/><br/>
							</prop:ItemTemplate>
						</com:TRepeater>
					</com:TActivePanel>

				</div>

				<div class="breaker"></div>
			</div>
			<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
		</div>
		<!--Fin Bloc Infos echange-->
		<com:PanelMessageErreur visible="false" ID="panelMessageErreur"/>
		<!--Debut Bloc Ajout piece jointe-->
		<div class="form-bloc">
			<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
			<com:TPanel ID="mainPart" cssClass="content">
				<h2><com:TTranslate>DEFINE_TEXT_AJOUT_PJ</com:TTranslate></h2>
				<div class="spacer-small"></div>
				<div class="line">
					<div class="intitule-100 bold"><label for="<%=$this->ajoutFichier->getClientId()%>"><com:TTranslate>DEFINE_TEXT_FICHIER</com:TTranslate></label> :</div>
					<com:AtexoFileUpload
							ID="ajoutFichier"
							Attributes.CssClass="file-580 float-left"
							Attributes.size="98"
							Attributes.OnChange="if(this.value) document.getElementById('<%=$this->hasFile->getClientId()%>').value='1'; else document.getElementById('<%=$this->hasFile->getClientId()%>').value='0';"
							Attributes.title="<%=Prado::localize('OBJET')%>"
					/>
					<com:THiddenField ID="hasFile" />
					<com:TImageButton
							OnClick="onAjoutClick"
							ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-ajout-element.gif"
							AlternateText="<%=Prado::localize('DEFINE_TEXT_AJOUT_COMME_PJ')%>"
							Attributes.title="<%=Prado::localize('DEFINE_TEXT_AJOUT_COMME_PJ')%>"
					/>
					<span style="display:none;">
					<com:TActiveButton
							ID="refreshRepeater"
							Text="buttonRefreshPieceExternes"
							OnCallBack="Page.IdUploadPj.refreshRepeaterPJ"/>
                </span>
				</div>
			</com:TPanel>
			<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
		</div>
		<!--Fin Bloc Ajout piece jointe-->
		<com:TLabel ID="labelServerSide"/>
		<div class="breaker"></div>
	</div>
	<div class="breaker"></div>
	<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
</div>
<!--Fin Bloc Echange-->