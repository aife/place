<!--Debut Bloc DUME ACHETEUR -->
<com:TActivePanel cssclass="form-field" id="bloc_etapeDumeAcheteur" Attributes.style="<%=$this->getVisible()%>">


    <div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
    <div class="content">

        <span class="DUMEsimplifieeMessage">
            <com:TActivePanel id="panelInfoMsgDumeAcheteurSimplifie" Display="None">
                <com:ActivePanelMessageAvertissement ID="activeInfoMsgDumeAcheteurSimplifie" Message="<%=Prado::localize('MESSAGE_TYPE_FORMULAIRE_DUME_SIMPLIFIE')%>"  DisplayStyle="Dynamic" />
            </com:TActivePanel>
        </span>

        <com:TActivePanel id="panelInfoMsgDumeAcheteur" Display="None">
            <com:ActivePanelMessageAvertissement ID="activeInfoMsgDumeAcheteur" Message="<%=Prado::localize('MESSAGE_TYPE_FORMULAIRE_DUME_STANDARD')%>"  DisplayStyle="Dynamic" />
        </com:TActivePanel>

        <com:TActivePanel id="panelBlocErreur" Display="None" cssclass="bloc-message form-bloc-conf msg-erreur">
            <com:PanelMessageErreur ID="messageErreur" Message="<%=Prado::localize('DUME_ACHETEUR_SERVICE_INDISPONIBLE')%>"/>
        </com:TActivePanel>
        <com:TActivePanel id="panelBlocErreurModifiedDume" Display="None">
            <com:PanelMessageErreur ID="messageErreurModifiedDume" Message="<%=Prado::localize('DUME_ACHETEUR_EN_COURS_DE_PUBLICATION')%>"/>
        </com:TActivePanel>
        <com:TActivePanel id="lt_dume" Display="None">
            <dume-request class="bootstrap-iso" uselot="true"></dume-request>
            <com:TActiveLabel id="scriptJs" style="display:none"/>
            <com:TConditional condition="Application\Service\Atexo\Atexo_Config::getParameter('ACTIVE_ELABORATION_V2') == '1'">
                <prop:TrueTemplate>
                    <script>
                        document.addEventListener('DOMContentLoaded', function() {
                            // Code JavaScript à exécuter après le chargement du DOM
                            J('#ctl0_CONTENU_PAGE_panelBlocErreurDume').hide();
                            J('#ctl0_CONTENU_PAGE_bloc_etapeDumeAcheteur_saved').val(0);
                            chargerLtDume('<%=$this->getToken()%>' , '<%=$this->getIdContexteDume()%>', '/dume/');
                        });
                    </script>
                </prop:TrueTemplate>
            </com:TConditional>
        </com:TActivePanel>
    </div>
    <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
    <com:THiddenField id="saveDumefrom" />
    <com:THiddenField id="saved" value="1" />
</com:TActivePanel>
<!--Fin Bloc DUME ACHETEUR -->
