<!--com:TContent ID="CONTENU_PAGE"-->
<!--Debut colonne droite-->
<div class="main-part" id="main-part">
	<div class="breadcrumbs">&gt; <com:TTranslate>TEXT_ACCUEIL</com:TTranslate></div>
	<com:PanelMessage id="panelMessage" visible="false"  />
	<com:TPanel id="panelEnvoiLogin" >
		<div class="form-field">
			<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
			<div class="content">
				<com:TPanel CssClass="top-title" id="panelMotPasseOublie" >
					<h2><com:TTranslate>TEXT_MOT_PASSE_OUBLIE</com:TTranslate></h2>
					<p><com:TTranslate>TEXT_INFOS_MOT_PASSE_OUBLIE</com:TTranslate></p>
				</com:TPanel>
				<!--Debut Bloc Login Identifiant Mot de passe-->
				<div class="form-bloc">
					<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
					<div class="content">
						<div class="line">
							<div class="intitule-150"><label for="emailUser"><com:TTranslate>TEXT_ADRESSE_ELECTRONIQUE</com:TTranslate></label> :</div>
							<com:TTextBox id="emailUser" Attributes.title="<%=Prado::localize('TEXT_ADRESSE_ELECTRONIQUE')%>" CssClass="login float-left" />
							<com:TLinkButton CssClass="bouton-small" onClick="getPassword" ><com:TTranslate>DEFINE_TEXT_ENVOYER</com:TTranslate></com:TLinkButton>
						</div>
						<div class="breaker"></div>
					</div>
					<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
				</div>
				<!--Fin Bloc Login Identifiant Mot de passe-->
				<div class="breaker"></div>
			</div>
			<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
		</div>
	</com:TPanel>
	<div class="spacer"></div>
	<div class="link-line">
		<com:THyperLink id="lienRetour" CssClass="bouton-retour" Attributes.title="<%=Prado::localize('DEFINE_TEXT_RETOUR_ACCUEIL')%>"><com:TTranslate>DEFINE_TEXT_RETOUR_ACCUEIL</com:TTranslate></com:THyperLink>
	</div>
	<div class="breaker"></div>
	<div class="breaker"></div>
</div>
<!--/com:TContent-->