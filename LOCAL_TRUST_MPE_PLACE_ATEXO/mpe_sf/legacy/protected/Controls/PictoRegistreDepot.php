<?php

namespace Application\Controls;

use Application\Service\Atexo\Controller\Atexo_Controller_Front;
use Prado\Prado;
use Prado\Web\UI\WebControls\TImageButton;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PictoRegistreDepot extends TImageButton
{
    public string $_activate = 'false';
    public string $_clickable = 'true';

    public function onLoad($param)
    {
        if ('false' == $this->_activate) {
            $this->setImageUrl(Atexo_Controller_Front::t().'/images/picto-depot-inactive.gif');
        } else {
            $this->setImageUrl(Atexo_Controller_Front::t().'/images/picto-depot.gif');
        }

        if ('false' == $this->_clickable) {
            $this->Attributes->onclick = 'return false;';
        }

        $this->setAlternateText(Prado::localize('ICONE_REGISTRE_DES_DEPOTS'));
        $this->setToolTip(Prado::localize('ICONE_REGISTRE_DES_DEPOTS'));
    }

    public function getActivate()
    {
        return $this->_activate;
    }

    public function setActivate($bool)
    {
        $this->_activate = $bool;
        if ('false' == $this->_activate) {
            $this->setImageUrl(Atexo_Controller_Front::t().'/images/picto-depot-inactive.gif');
        } else {
            $this->setImageUrl(Atexo_Controller_Front::t().'/images/picto-depot.gif');
        }
    }

    public function getClickable()
    {
        return $this->_clickable;
    }

    public function setClickable($bool)
    {
        $this->_clickable = $bool;
        if ('false' == $this->_clickable) {
            $this->Attributes->onclick = 'return false;';
        }
    }
}
