<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Prado\Prado;

class ArchiveRappelResultSearch extends ArchiveResultSearch
{
    public function fillRepeaterWithDataForArchiveSearchResult($criteriaVo)
    {
        $offset = null;
        $limit = null;
        $consultation = new Atexo_Consultation();

        $criteriaVo->setOffset($offset);
        $criteriaVo->setLimit($limit);
        $arrayConsultation = $consultation->search($criteriaVo);
        $nombreElement = count($arrayConsultation);
        if ($nombreElement >= 1) {
            $this->panelMoreThanOneElementFound->setVisible(true);
            $this->panelNoElementFound->setVisible(false);
            $this->nombreElement->Text = $nombreElement;
            $this->PagerBottom->setVisible(true);
            $this->PagerTop->setVisible(true);
            $this->setViewState('nombreElement', $nombreElement);
            $this->setViewState('CriteriaVo', $criteriaVo);
            $this->nombrePageTop->Text = ceil($nombreElement / $this->ArchiveTableauBordRepeater->PageSize);
            $this->nombrePageBottom->Text = ceil($nombreElement / $this->ArchiveTableauBordRepeater->PageSize);
            $this->ArchiveTableauBordRepeater->setVirtualItemCount($nombreElement);
            $this->ArchiveTableauBordRepeater->setCurrentPageIndex(0);
            $offset = $this->ArchiveTableauBordRepeater->CurrentPageIndex * $this->ArchiveTableauBordRepeater->PageSize;
            $limit = $this->ArchiveTableauBordRepeater->PageSize;
            if ($offset + $limit > $this->ArchiveTableauBordRepeater->getVirtualItemCount()) {
                $limit = $this->ArchiveTableauBordRepeater->getVirtualItemCount() - $offset;
            }
            $this->ArchiveTableauBordRepeater->DataSource = $arrayConsultation;
            $this->ArchiveTableauBordRepeater->DataBind();
        } else {
            $this->panelMoreThanOneElementFound->setVisible(false);
            $this->panelNoElementFound->setVisible(true);
            $this->PagerBottom->setVisible(false);
            $this->PagerTop->setVisible(false);
        }
    }

    public function infoBulleEtapeConnsultation($statusConsultation)
    {
        if ($statusConsultation == Atexo_Config::getParameter('STATUS_ELABORATION')) {
            return Prado::localize('ETAPE_ELABORATION');
        }
        if ($statusConsultation == Atexo_Config::getParameter('STATUS_DECISION')) {
            return Prado::localize('ETAPE_DECISION');
        }
        if ($statusConsultation == Atexo_Config::getParameter('STATUS_PREPARATION')) {
            return Prado::localize('ETAPE_PREPARATION');
        }
        if ($statusConsultation == Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE')) {
            return Prado::localize('ETAPE_OUVERTURE_ET_ANALYSE');
        }
    }

    public function getDiffMonths($date)
    {
        if (null == $date || '' == $date) {
            return 0;
        }
        $today = strtotime(date('Y-m-d'));

        return ($today - strtotime($date)) / 60 / 60 / 24 / 30;
    }
}
