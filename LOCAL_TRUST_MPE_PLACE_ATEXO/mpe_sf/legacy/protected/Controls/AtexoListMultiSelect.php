<?php

namespace Application\Controls;

/**
 * controleur de liste multi-choix.
 *
 * @author AME <amal.elbekkaoui@atexo.com>
 *
 * @version 1.0
 *
 * @since roadmap-2015
 *
 * @copyright Atexo 2015
 */
class AtexoListMultiSelect extends MpeTTemplateControl
{
    private $libelleListe;
    private $titleListe;
    private $libelleSelectionez;
    private bool $afficherInfoBulle = false;
    private $libelleInfoBulle;
    private $validationGroup;
    private $validationSummary;
    private $fonctionValidation;
    private $nombreMaxSelection;

    /**
     * @return mixed
     */
    public function getNombreMaxSelection()
    {
        return $this->nombreMaxSelection;
    }

    /**
     * @param mixed $nombreMaxSelection
     */
    public function setNombreMaxSelection($nombreMaxSelection)
    {
        $this->nombreMaxSelection = $nombreMaxSelection;
    }

    /**
     * @return mixed
     */
    public function getValidationGroup()
    {
        return $this->validationGroup;
    }

    /**
     * @param mixed $validationGroup
     */
    public function setValidationGroup($validationGroup)
    {
        $this->validationGroup = $validationGroup;
    }

    /**
     * @return mixed
     */
    public function getValidationSummary()
    {
        return $this->validationSummary;
    }

    /**
     * @param mixed $validationSummary
     */
    public function setValidationSummary($validationSummary)
    {
        $this->validationSummary = $validationSummary;
    }

    /**
     * @return mixed
     */
    public function getLibelleInfoBulle()
    {
        return $this->libelleInfoBulle;
    }

    /**
     * @param mixed $libelleInfoBulle
     */
    public function setLibelleInfoBulle($libelleInfoBulle)
    {
        $this->libelleInfoBulle = $libelleInfoBulle;
    }

    /**
     * @return mixed
     */
    public function getAfficherInfoBulle()
    {
        return $this->afficherInfoBulle;
    }

    /**
     * @param mixed $afficherInfoBulle
     */
    public function setAfficherInfoBulle($afficherInfoBulle)
    {
        $this->afficherInfoBulle = $afficherInfoBulle;
    }

    /**
     * @return mixed
     */
    public function getLibelleListe()
    {
        return $this->libelleListe;
    }

    /**
     * @param mixed $libelleListe
     */
    public function setLibelleListe($libelleListe)
    {
        $this->libelleListe = $libelleListe;
    }

    /**
     * @return mixed
     */
    public function getTitleListe()
    {
        return $this->titleListe;
    }

    /**
     * @param mixed $titleListe
     */
    public function setTitleListe($titleListe)
    {
        $this->titleListe = $titleListe;
    }

    /**
     * @return mixed
     */
    public function getLibelleSelectionez()
    {
        return $this->libelleSelectionez;
    }

    /**
     * @param mixed $libelleSelectionez
     */
    public function setLibelleSelectionez($libelleSelectionez)
    {
        $this->libelleSelectionez = $libelleSelectionez;
    }

    public function setDataSource($data)
    {
        if (is_array($data)) {
            $this->listMultiSelect->setDataSource($data);
            $this->listMultiSelect->dataBind();
        }
    }

    public function getSelectedValues()
    {
        return $this->hiddenSelectedValue->getValue();
    }

    public function setSelectedValues($ids)
    {
        if (is_array($ids)) {
            $this->listMultiSelect->setSelectedValues($ids);
            $this->hiddenSelectedValue->setValue(implode(',', $ids));
        }
    }

    /**
     * @return mixed
     */
    public function getFonctionValidation()
    {
        return $this->fonctionValidation;
    }

    /**
     * @param mixed $fonctionValidation
     */
    public function setFonctionValidation($fonctionValidation)
    {
        $this->fonctionValidation = $fonctionValidation;
    }
}
