<div class="toggle-panel form-toggle donnees-redac">
	<div class="content">
		<div class="cover"></div>
		<a class="title-toggle-open" onclick="togglePanel(this,'panelProcédure');" href="javascript:void(0);"><com:TTranslate>DEFINE_PROCEDURE</com:TTranslate></a>
		<div class="panel no-indent panel-donnees-complementaires" style="display:block;margin-bottom:0px;" id="panelProcédure">
			<!--Debut Ligne Nombre candidat-->
			<div class="line">
				<div class="intitule col-300"><com:TTranslate>DEFINE_CANDIDAT_ADMET_A_PRESENTER_OFFRE</com:TTranslate> :</div>
				<div class="content-bloc check-list">
					<div class="line-input">
						<div class="intitule col-100">
							<com:TRadioButton 
								cssClass="radio float-left pos-t-2-child-label"
								GroupName="offreFixeFourchette" 
								ID="offreFixe" 
								Text="<%=Prado::localize('DEFINE_FIXE')%>"
								Attributes.onclick="isChecked2(this,'nbCandidatsFixe','nbCandidatsFourchette');"
								Attributes.title="<%=Prado::localize('DEFINE_FIXE')%>"
							/>
						</div>
						<div class="col-160 float-left indent-30" id="nbCandidatsFixe" style="display:block;">
							<com:TTextBox 
								ID="nbCandidatsFixeInput"  
								Attributes.title="<%=Prado::localize('DEFINE_NOMBRE_CANDIDAT_FIXE')%>"
								cssClass="col-30 float-left align-right" 
							/>
						</div>
					</div>
					<div class="line-input">
						<div class="intitule col-100 clear-both pos-t-2-child-label">
							<com:TRadioButton 
								cssClass="radio" 
								GroupName="offreFixeFourchette" 
								ID="offreFourchette" 
								Text="<%=Prado::localize('DEFINE_FOURCHETTE')%>"
								Attributes.onclick="isChecked2(this,'nbCandidatsFourchette','nbCandidatsFixe');"
								Attributes.title="<%=Prado::localize('DEFINE_FOURCHETTE')%>"
							/>
						</div>
						<div class="float-left" style="display: none;" id="nbCandidatsFourchette">
							<div class="column-auto">
								<div class="intitule pos-t-2"><com:TTranslate>FCSP_MIN</com:TTranslate>.</div>
								<com:TTextBox 
									ID="fourchetteMini"  
									Attributes.title="<%=Prado::localize('DEFINE_MINIMUM')%>"
									cssClass="col-30 float-left align-right" 
								/>
							</div>
							<div class="column">
								<div class="intitule pos-t-2"><com:TTranslate>FCSP_MAX</com:TTranslate>.</div>
								<com:TTextBox 
									ID="fourchetteMax"  
									Attributes.title="<%=Prado::localize('DEFINE_MAXIMUM')%>"
									cssClass="col-30 float-left align-right" 
								/>
							</div>
						</div>
					</div>
					<div class="breaker"></div>
				</div>
			</div>
			<!--Fin Ligne Nombre candidat-->
			<div class="spacer-small"></div>
			<!--Debut Ligne Délai de validité des offres-->
			<div class="line">
				<div class="intitule col-300"><label for="dureeValidite"><com:TTranslate>FCSP_DELAIS_VALIDITE_OFFRES</com:TTranslate></label><span class="champ-oblig">*</span> :</div>
				<com:TTextBox 
					ID="dureeValidite"  
					Attributes.title="<%=Prado::localize('FCSP_DELAIS_VALIDITE_OFFRES')%>"
					cssClass="col-30 float-left align-righ" 
				/>
				<com:TCustomValidator 
						ControlToValidate="dureeValidite" 
						ValidationGroup="validateInfosConsultation" 
						ClientValidationFunction="validationDureeValidite" 
						Display="Dynamic" 
						Enabled="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('DonneesComplementaires') && Application\Service\Atexo\Atexo_Module::isEnabled('DonneesRedac'))%>"
						ErrorMessage="<%=Prado::localize('FCSP_DELAIS_VALIDITE_OFFRES')%>"
						Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
						EnableClientScript="true" 
					> 						 					 
						<prop:ClientSide.OnValidationError>
							document.getElementById('divValidationSummary').style.display='';
			                document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
			                document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
			                document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
			                document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
						</prop:ClientSide.OnValidationError>
				</com:TCustomValidator>
				<com:TLabel text="<%=Prado::localize('MONTHS')%>" />
			</div>
			<!--Fin Ligne Délai de validité des offres-->
			<com:TPanel Visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('Publicite') && !Application\Service\Atexo\Atexo_Config::getParameter('SPECIFIQUE_PLACE'))%>">
				<div class="spacer-small panel-donnees-complementaires-compte-publicite"></div>
				<div class="line panel-donnees-complementaires-compte-publicite">
					<div class="intitule col-300"><com:TTranslate>POSSIBILTY_ATTRIBUTION_WITHOUT_NEGOCIATION</com:TTranslate> <span class="champ-oblig">*</span> : </div>
					<div class="content-bloc-auto">
						<com:TActiveRadioButtonList ID="attributionWhitoutNegociationChoices" Enabled="<%=$this->enabledElement()%>" OnCallback="Page.bloc_etapeDonneesComplementaires.donneeProcedure.attributionWhitoutNegociationSelected" cssClass="radioButtonPrado">
							<com:TListItem Value="1" Id="attributionWhitoutNegociationYes" />
							<com:TListItem Value="0" Id="attributionWhitoutNegociationNo" />
						</com:TActiveRadioButtonList>
						<com:TRequiredFieldValidator
								ValidationGroup="validateInfosConsultation"
								ControlToValidate="attributionWhitoutNegociationChoices"
								Text="<span title='<%=Prado::localize('CHAMP_OBLIGATOIRE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMP_OBLIGATOIRE')%>' /></span>"
								ErrorMessage="<%=Prado::localize('POSSIBILTY_ATTRIBUTION_WITHOUT_NEGOCIATION')%>"
								Enabled="<%=($this->TemplateControl->getConsultation() instanceof CommonConsultation && $this->TemplateControl->getConsultation()->getDonneePubliciteObligatoire())%>"
						>
							<prop:ClientSide.OnValidate>
								if(document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeIdentification_intitulePubliciteOui")
									&& document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeIdentification_intitulePubliciteOui").checked) {
								sender.enabled = true;
								} else{
								sender.enabled = false;
								}
							</prop:ClientSide.OnValidate>
                            <prop:ClientSide.OnValidationError>
                                document.getElementById('divValidationSummary').style.display='';
                                document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
                                document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
                                document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
                                document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
                            </prop:ClientSide.OnValidationError>
						</com:TRequiredFieldValidator>
					</div>
				</div>
			</com:TPanel>
			<com:TPanel Visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('Publicite') && !Application\Service\Atexo\Atexo_Config::getParameter('SPECIFIQUE_PLACE'))%>">
				<div class="spacer-small panel-donnees-complementaires-compte-publicite"></div>
				<div class="line panel-donnees-complementaires-compte-publicite">
					<div class="intitule col-300"><label for="technique_achat"><com:TTranslate>TECHNIQUE_ACHAT</com:TTranslate></label><span class="champ-oblig"> *</span> :</div>
					<div class="content-bloc">
						<com:TActiveTextBox
								id="technique_achat"
								CssClass="moyen float-left"
								Attributes.title="<%=Prado::localize('TECHNIQUE_ACHAT')%>"
								Attributes.MaxLength="<%= Application\Service\Atexo\Atexo_Config::getParameter('NOMBRE_CARACTERES_MAX_AUTORISES_TECHNIQUE_ACHAT')%>"
						/>
						<div class="col-info-bulle">
							<img title="Info-bulle" alt="Info-bulle" class="picto-info" onmouseout="cacheBulle('infosTechniqueAchat')" onmouseover="afficheBulle('infosTechniqueAchat', this)" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" />
							<div id="infosTechniqueAchat" class="info-bulle" onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();"><div><com:TTranslate>TECHNIQUE_ACHAT_INFOBULLE</com:TTranslate></div></div>
						</div>
						<com:TRequiredFieldValidator
								ControlToValidate="technique_achat"
								ValidationGroup="validateInfosConsultation"
								Text="<span title='<%=Prado::localize('CHAMP_OBLIGATOIRE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMP_OBLIGATOIRE')%>' /></span>"
								ErrorMessage="<%=Prado::localize('TECHNIQUE_ACHAT')%>"
								Enabled="<%=($this->TemplateControl->getConsultation() instanceof CommonConsultation && $this->TemplateControl->getConsultation()->getDonneePubliciteObligatoire())%>"
						>
							<prop:ClientSide.OnValidate>
								if(document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeIdentification_intitulePubliciteOui")
								&& document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeIdentification_intitulePubliciteOui").checked) {
								sender.enabled = true;
								} else{
								sender.enabled = false;
								}
							</prop:ClientSide.OnValidate>
                            <prop:ClientSide.OnValidationError>
                                document.getElementById('divValidationSummary').style.display='';
                                document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
                                document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
                                document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
                                document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
                            </prop:ClientSide.OnValidationError>
						</com:TRequiredFieldValidator>
					</div>
				</div>
			</com:TPanel>

			<com:TPanel Visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('Publicite') && !Application\Service\Atexo\Atexo_Config::getParameter('SPECIFIQUE_PLACE'))%>">
				<div class="spacer-small panel-donnees-complementaires-compte-publicite"></div>
				<div class="line panel-donnees-complementaires-compte-publicite">
					<div class="intitule col-300"><label for="electronicCatalog"><com:TTranslate>ELECTRONIC_CATALOG</com:TTranslate></label><span class="champ-oblig"> *</span> :</div>
					<div class="content-bloc">
						<com:TActiveDropDownList
								id="electronicCatalog"
								CssClass="moyen"
								Attributes.title="<%=Prado::localize('ELECTRONIC_CATALOG')%>"
						/>
						<com:TRequiredFieldValidator
								ValidationGroup="validateInfosConsultation"
								ControlToValidate="electronicCatalog"
								InitialValue="0"
								Text="<span title='<%=Prado::localize('CHAMP_OBLIGATOIRE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMP_OBLIGATOIRE')%>' /></span>"
								ErrorMessage="<%=Prado::localize('ELECTRONIC_CATALOG')%>"
								Enabled="<%=($this->TemplateControl->getConsultation() instanceof CommonConsultation && $this->TemplateControl->getConsultation()->getDonneePubliciteObligatoire())%>"
						>
							<prop:ClientSide.OnValidate>
								if(document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeIdentification_intitulePubliciteOui")
								&& document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeIdentification_intitulePubliciteOui").checked) {
								sender.enabled = true;
								} else{
								sender.enabled = false;
								}
							</prop:ClientSide.OnValidate>
                            <prop:ClientSide.OnValidationError>
                                document.getElementById('divValidationSummary').style.display='';
                                document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
                                document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
                                document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
                                document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
                            </prop:ClientSide.OnValidationError>
						</com:TRequiredFieldValidator>
					</div>
				</div>
			</com:TPanel>

			<!--Debut Ligne Procedure phases successives-->
			<com:TPanel>
				<div class="spacer-small"></div>
				<div class="line">
					<div class="intitule-auto float-left">
						<com:TActiveCheckBox
								cssClass="check"
								Attributes.title="<%=Prado::localize('DEFINE_PROCEDURE_PHASE_SUCCESSIVE_AFINE_REDUIRE_NOMBRE_OFFRE_A_NEGOCIER')%>"
								id="procedurePhaseSuccessive">
						</com:TActiveCheckBox>
						<label for="procedurePhaseSuccessive"><com:TTranslate>DEFINE_PROCEDURE_PHASE_SUCCESSIVE_AFINE_REDUIRE_NOMBRE_OFFRE_A_NEGOCIER</com:TTranslate></label>
					</div>
					<div class="col-info-bulle">
						<img alt="Info-bulle" class="picto-info-intitule" onmouseout="cacheBulle('infosProcedurePhaseSuccessive')" onmouseover="afficheBulle('infosProcedurePhaseSuccessive', this)" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif">
						<div style="display: none; left: 1081px; top: 589px;" id="infosProcedurePhaseSuccessive" class="info-bulle" onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();"><div><com:TTranslate>TEXT_INFO_BULLE_PROCEDURE</com:TTranslate></div></div>
					</div>
				</div>
			</com:TPanel>
			<!--Fin Ligne Procedure phases successives-->
			<!--Fin Ligne Accord OMC-->
			<com:TPanel cssClass="line" visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('Publicite') && !Application\Service\Atexo\Atexo_Config::getParameter('SPECIFIQUE_PLACE'))%>">
				<div class="intitule-auto">
					<com:TActiveCheckBox
							cssClass="check"
							Attributes.title="<%=Prado::localize('DEFINE_MESSAGE_PROCEDURE_ACHAT_COUVERTE_PAR_ACCORD_MARCHES_PUBLICS_OMC')%>"
							id="accordMarchePublicOmc">
					</com:TActiveCheckBox>
					<label for="accordMarchePublicOmc"><com:TTranslate>DEFINE_MESSAGE_PROCEDURE_ACHAT_COUVERTE_PAR_ACCORD_MARCHES_PUBLICS_OMC</com:TTranslate></label>
				</div>
				<div class="spacer"></div>
			</com:TPanel>
			<!--Fin Ligne Accord OMC-->

			<com:AtexoListMultiSelect
				id="motsCles"
				visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('Publicite') && !Application\Service\Atexo\Atexo_Config::getParameter('SPECIFIQUE_PLACE'))%>"
				libelleListe="<%=Prado::localize('MOTS_CLES_DESCRIPTEURS_CONSULTATION')%>"
				titleListe="<%=Prado::localize('MOTS_CLES_DESCRIPTEURS_CONSULTATION')%>"
				libelleSelectionez="<%=Prado::localize('SELECTIONNEZ_MOTS_CLES')%>"
				afficherInfoBulle="true"
				libelleInfoBulle="<%=Prado::localize('INFO_BULLE_MOTS_CLES_DESCRIPTEURS_CONSULTATION')%>"
				validationGroup="validateInfosConsultation"
				validationSummary="divValidationSummary"
				fonctionValidation ="validateListMotsDescriptifs"
				nombreMaxSelection="5"
				Enabled="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('Publicite') && !Application\Service\Atexo\Atexo_Config::getParameter('SPECIFIQUE_PLACE'))%>"

			/>

			<com:TLabel ID="scriptLabel" />

			<com:TPanel Visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('Publicite') && !Application\Service\Atexo\Atexo_Config::getParameter('SPECIFIQUE_PLACE'))%>">
				<div class="line line--mandatoryVisit panel-donnees-complementaires-compte-publicite">
					<div class="intitule col-300">
						<com:TTranslate>MANDATORY_VISIT</com:TTranslate> <span class="champ-oblig">*</span> :
					</div>
					<div class="content-bloc content-bloc-auto">
						<com:TActiveRadioButtonList ID="mandatoryVisitChoices" Enabled="<%=$this->enabledElement()%>" OnCallback="Page.bloc_etapeDonneesComplementaires.donneeProcedure.mandatoryVisitSelected" cssClass="radioButtonPrado">
							<com:TListItem Value="1" Id="mandatoryVisitYes" />
							<com:TListItem Value="0" Id="mandatoryVisitNo" />
						</com:TActiveRadioButtonList>
						<com:TRequiredFieldValidator
								ValidationGroup="validateInfosConsultation"
								ControlToValidate="mandatoryVisitChoices"
								Text="<span title='<%=Prado::localize('CHAMP_OBLIGATOIRE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMP_OBLIGATOIRE')%>' /></span>"
								ErrorMessage="<%=Prado::localize('MANDATORY_VISIT')%>"
								Enabled="<%=($this->TemplateControl->getConsultation() instanceof CommonConsultation && $this->TemplateControl->getConsultation()->getDonneePubliciteObligatoire())%>"
						>
							<prop:ClientSide.OnValidate>
								if(document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeIdentification_intitulePubliciteOui")
								&& document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeIdentification_intitulePubliciteOui").checked) {
								sender.enabled = true;
								} else{
								sender.enabled = false;
								}
							</prop:ClientSide.OnValidate>
                            <prop:ClientSide.OnValidationError>
                                document.getElementById('divValidationSummary').style.display='';
                                document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
                                document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
                                document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
                                document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
                            </prop:ClientSide.OnValidationError>
						</com:TRequiredFieldValidator>
						<com:TPanel  cssClass="content-bloc-auto descriptionMandatoryVisit" id="descriptionMandatoryVisit" style="display:none;">
							<com:TTextBox
									id="descMandatoryVisit"
									TextMode="MultiLine"
									Attributes.MaxLength="<%= Application\Service\Atexo\Atexo_Config::getParameter('NOMBRE_CARACTERES_MAX_AUTORISES_DESCRIPTION_VISITE')%>"
									CssClass="moyen"
							/>
						</com:TPanel>
					</div>
				</div>
			</com:TPanel>
		</div>
		<div class="breaker"></div>
	</div>
</div>

<script>
	const description = document.getElementById('<%= $this->descriptionMandatoryVisit->ClientId %>');
	const radiosMandatoryVisitBlock = document.getElementById('<%= $this->mandatoryVisitChoices->ClientId %>');
	const radioAlreadyChecked = radiosMandatoryVisitBlock.querySelector('input[type=radio]:checked');

	if (radioAlreadyChecked) {
		description.style.display = (radioAlreadyChecked.value == 1) ? 'block' : 'none';
	}

	radiosMandatoryVisitBlock.addEventListener('click', event => {
		description.style.display = (event.target.value == 1) ? 'block' : 'none';
	})
</script>