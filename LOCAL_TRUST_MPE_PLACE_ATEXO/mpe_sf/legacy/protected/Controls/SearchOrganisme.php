<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Organisme\Atexo_Organisme_CriteriaVo;
use Prado\Prado;

/*
 * Created on 29 mai 2013
 *
 * @author loubna EZZIANI <loubna.ezziani@atexo.com>
 * @package
 */
class SearchOrganisme extends MpeTPage
{
    public $_organisme;
    public $langue;

    public function setOrganisme($organisme)
    {
        $this->_organisme = $organisme;
    }

    public function getOrganisme()
    {
        return $this->_organisme;
    }

    public function onLoad($param)
    {
    }

    public function fillData()
    {
        $this->langue = Atexo_CurrentUser::readFromSession('lang');
        if (!$this->Page->IsPostBack) {
            self::displayOrganismes();
        } else {
            $this->_organisme = $this->entitePubique->SelectedValue;
        }
    }

    public function displayOrganismes()
    {
        $ArrayOrganismes = Atexo_Organismes::retrieveOrganismes(false, $this->langue, true);
        $arrayOrga = [];
        foreach ($ArrayOrganismes as $key => $value) {
            $arrayOrga[$key] = $value;
        }
        $ArrayOrganismes = $arrayOrga;
        array_unshift($ArrayOrganismes, '--- '.Prado::localize('TEXT__TOUS_MINISTERES').' ---');

        $this->entitePubique->dataSource = $ArrayOrganismes;
        $this->entitePubique->DataBind();
    }

    /*
     * Method qui remplie le criteriaVo Organisme
     */
    public function fillCriteriaVoOrganisme($limit = 0, $offset = 0)
    {
        $criteriaVo = new Atexo_Organisme_CriteriaVo();
        $this->_organisme = $this->entitePubique->SelectedValue;
        $criteriaVo->setAcronyme($this->_organisme);

        if ('' != $this->denominationOrganisme->Text) {
            $criteriaVo->setDenomination(Atexo_Util::encodeToHttp($this->denominationOrganisme->Text));
        }
        $criteriaVo->setLimit($limit);
        $criteriaVo->setOffset($offset);
        $criteriaVo->setActive(0);

        return $criteriaVo;
    }
}
