<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN1;
use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2;
use Prado\Prado;

/**
 * composant pour afficher les regions et les provinces.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class AtexoRegionProvince extends MpeTPage
{
    private string $_postBack = '';
    private string $_callback = '';
    private bool $_display = false;

    public function setPostBack($value)
    {
        $this->_postBack = $value;
    }

    public function getPostBack()
    {
        return $this->_postBack;
    }

    public function setCallBack($value)
    {
        $this->_callback = $value;
    }

    public function getCallBack()
    {
        return $this->_callback;
    }

    public function setDisplay($value)
    {
        $this->_display = $value;
    }

    public function getDisplay()
    {
        return $this->_display;
    }

    public function onLoad($param)
    {
        $this->script->Text = '';
        $this->script2->Text = '';

        if ('Agent.SearchCompany' == $_GET['page'] || 'Agent.ChoixDestinataireBdFournisseur' == $_GET['page']) {
            $this->regionObligatoire->setDisplay('None');
            $this->provinceObligatoire->setDisplay('None');
            if (Atexo_Module::isEnabled('CompteEntrepriseRegion')) {
                //$this->activePanelRegionProvince->setDisplay("Dynamic");
                $this->panelRegion->setVisible(true);
                $this->validatorRegion->SetEnabled(false);
                if (!$this->getPostBack()) {
                    $this->displayRegion();
                }
            } else {
                $this->panelRegion->setVisible(false);
                //$this->activePanelRegionProvince->setDisplay("None");
                $this->validatorRegion->SetEnabled(true);
            }
            if (Atexo_Module::isEnabled('CompteEntrepriseProvince')) {
                $this->validatorProvince->SetEnabled(false);
                $this->panelProvince->setVisible(true);
            } else {
                $this->validatorProvince->SetEnabled(true);
                $this->panelProvince->setVisible(false);
            }
        }
        if ('Entreprise.EntrepriseInscriptionUsers' == $_GET['page']) {
            if (isset($_GET['siren']) || true == $this->getDisplay()) {
                if (Atexo_Module::isEnabled('CompteEntrepriseRegion')) {
                    //$this->activePanelRegionProvince->setDisplay("Dynamic");
                    $this->panelRegion->setVisible(true);
                    if (!$this->getPostBack()) {
                        $this->displayRegion();
                    }
                    $this->validatorRegion->SetEnabled(true);
                } else {
                    //$this->activePanelRegionProvince->setDisplay("None");
                    $this->panelRegion->setVisible(false);
                    $this->validatorRegion->SetEnabled(false);
                }
                if (Atexo_Module::isEnabled('CompteEntrepriseProvince')) {
                    //$this->activePanelRegionProvince->setDisplay("Dynamic");
                    $this->panelProvince->setVisible(true);
                    if (!$this->getPostBack()) {
                        $this->displayProvince();
                    }
                    $this->validatorProvince->SetEnabled(true);
                } else {
                    //$this->activePanelRegionProvince->setDisplay("None");
                    $this->panelProvince->setVisible(false);
                    $this->validatorProvince->SetEnabled(false);
                }
            } else {
                $this->panelProvince->setVisible(false);
                $this->validatorProvince->SetEnabled(false);
                $this->panelRegion->setVisible(false);
                $this->validatorRegion->SetEnabled(false);
            }
        }
    }

    /**
     * Charger la liste des région.
     */
    public function displayRegion()
    {
        $region = (new Atexo_Geolocalisation_GeolocalisationN1())->retrieveGeoN1ByIdGeoN0(Atexo_Config::getParameter('IDENTIFIANT_GEOLOCALISATION_N0_MAROC'));
        $data = [];
        $data['-1'] = Prado::localize('TEXT_SELECTIONNER');
        if ($region) {
            foreach ($region as $oneRegion) {
                $data[$oneRegion->getId()] = $oneRegion->getDenomination1();
            }
        }
        $this->regionSiegeSocial->DataSource = $data;
        $this->regionSiegeSocial->dataBind();
    }

    /**
     * affichage de toutes les provinces.
     */
    public function displayProvince()
    {
        $listProvince = (new Atexo_Geolocalisation_GeolocalisationN2())->retrieveListGeolocalisationN2($this->regionSiegeSocial->SelectedValue);
        $data = [];
        $data['-1'] = Prado::localize('TEXT_SELECTIONNER');
        if ($listProvince) {
            foreach ($listProvince as $oneProvince) {
                $data[$oneProvince->getId()] = $oneProvince->getDenomination1();
            }
        }
        $this->provinceSiegeSocial->dataSource = $data;
        $this->provinceSiegeSocial->DataBind();
    }

    /**
     * pour ré-actualiser la partie des regions.
     */
    public function onCallBackRegion($sender, $param)
    {
        $this->script->Text = "<script>document.getElementById('ctl0_CONTENU_PAGE_buttonRefresh').click();</script>";
        $this->displayProvince();
        $this->activePanelRegionProvince->render($param->NewWriter);
    }

    /**
     * pour ré-actualiser la partie des provinces.
     */
    public function onCallBackProvince($sender, $param)
    {
        $this->script2->Text = "<script>document.getElementById('ctl0_CONTENU_PAGE_buttonRefresh').click();</script>";
        $this->panelProvince->render($param->NewWriter);
    }
}
