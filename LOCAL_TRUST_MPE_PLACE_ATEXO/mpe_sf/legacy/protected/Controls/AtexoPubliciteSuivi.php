<?php

namespace Application\Controls;

use App\Entity\ConfigurationOrganisme;
use App\Exception\ConnexionWsException;
use App\Service\AgentService;
use App\Service\ConcentrateurManager;
use App\Service\ConfigurationOrganismeService;
use App\Service\Publicite\EchangeConcentrateur;
use Application\Propel\Mpe\CommonAdresseFacturationJalQuery;
use Application\Propel\Mpe\CommonAgentQuery;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonTypeAvisPubProcedureQuery;
use Application\Propel\Mpe\CommonTypeAvisPubQuery;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Jal;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_MultiDomaine;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Config\Atexo_Config_Exception;
use Exception;
use Prado\Prado;

/**
 * Class AtexoPubliciteSuivi.
 */
class AtexoPubliciteSuivi extends MpeTTemplateControl
{
    public const CONCENTRATEUR_PUB_TNCP = 'TNCP';
    public const CONCENTRATEUR_PUB_MOL = 'MOL';
    public const CONCENTRATEUR_PUB_JAL_FR = 'JAL_FR';
    public const CONCENTRATEUR_PUB_JAL_LUX = 'JAL_LUX';
    public const CONCENTRATEUR_PUB_JOUE = 'JOUE';


    /**
     * @param $cons
     *
     * @throws Atexo_Config_Exception
     */
    public function charger($cons)
    {
        $logger = Atexo_LoggerManager::getLogger('publicite');
        try {
            $concentrateurService = Atexo_Util::getSfService(ConcentrateurManager::class);
            $accesToken = $concentrateurService->addAgent($cons);
            $data = [];
            if ($cons instanceof CommonConsultation) {
                $data['europeen'] = (bool) $this->getEuropeanPub($cons);
                $data['blocList'] = $this->getConfigPub($cons);
                $data['facturationList'] = $this->getFacturations($cons);
            }

            $concentrateurService->configAnnonces($cons, $accesToken, $data);
            $urlPf = $this->getUrlConcentrateur();
            $traductions = $this->getTraduction();
            $uidPf = Atexo_Config::getParameter('UID_PF_MPE');
            $uidConsultation = $cons->getId();
            $this->scriptJs->text = <<<SCRIPT
<script>
J(document).ready(function(){
var paramsConcentrateur = [];
paramsConcentrateur["plateforme"] = "$uidPf";
paramsConcentrateur["id-consultation"] = "$uidConsultation";
paramsConcentrateur["access-token"] = "$accesToken";
paramsConcentrateur["traductions "] = '$traductions';
var scriptsList = [
        'concentrateur-annonces/annonce-suivi/annonceSuivi.umd.js',
        'concentrateur-annonces/annonceSuiviVue.js'
    ];
chargerAnnoncesConcentrateur('$urlPf','annonce-suivi', paramsConcentrateur, scriptsList);
});
</script>
SCRIPT;
            $this->panelConcentrateur->setDisplay('Dynamic');
            $this->panelBlocErreur->setDisplay('None');
        } catch (\Exception $exception) {
            $logger->error(
                'Probleme lors recuperation du token Concentrateur'
                . $exception->getMessage()
                . ' ' . $exception->getTraceAsString()
            );
            $this->panelConcentrateur->setDisplay('None');
            $this->panelBlocErreur->setDisplay('Dynamic');
        }
    }

    public function getTraduction(): false|string
    {
        $trad = [
            'STATUT_ANNONCE_A_COMPLETER' => base64_encode(Prado::localize('STATUT_ANNONCE_BROUILLON')),
            'DATE_ANNONCE_A_COMPLETER' => base64_encode(Prado::localize('DATE_ANNONCE_A_COMPLETER')),
            'STATUT_ANNONCE_COMPLET' => base64_encode(Prado::localize('STATUT_ANNONCE_COMPLET')),
            'DATE_ANNONCE_COMPLET' => base64_encode(Prado::localize('DATE_ANNONCE_COMPLET')),
            'STATUT_ANNONCE_ENVOYER' => base64_encode(Prado::localize('STATUT_ANNONCE_ENVOYER')),
            'DATE_ANNONCE_ENVOYER' => base64_encode(Prado::localize('DATE_ANNONCE_ENVOYER')),
            'STATUT_ANNONCE_EN_ATTENTE_PUBLICATION' => base64_encode(Prado::localize('STATUT_ANNONCE_EN_ATTENTE_PUBLICATION')),
            'DATE_ANNONCE_EN_ATTENTE_PUBLICATION' => base64_encode(Prado::localize('DATE_ANNONCE_EN_ATTENTE_PUBLICATION')),
            'STATUT_ANNONCE_PUBLIE' => base64_encode(Prado::localize('STATUT_ANNONCE_PUBLIE')),
            'DATE_ANNONCE_PUBLIE' => base64_encode(Prado::localize('DATE_ANNONCE_PUBLIE')),
            'STATUT_ANNONCE_REJETE' => base64_encode(Prado::localize('STATUT_ANNONCE_REJETE')),
            'DATE_ANNONCE_REJETE' => base64_encode(Prado::localize('DATE_ANNONCE_REJETE')),
            'TEXT_SELECTION_OFFRE_ECHOS' => base64_encode(Prado::localize('TEXT_SELECTION_OFFRE_ECHOS')),
            'TEXT_FORMULAIRE_PUBLICITE_LES_ECHOS' => base64_encode(Prado::localize('TEXT_FORMULAIRE_PUBLICITE_LES_ECHOS')),
            'DATES' => base64_encode(Prado::localize('DATES')),
            'ACTIONS_PUBLICATION' => base64_encode(Prado::localize('ACTIONS_PUBLICATION')),
            'STATUT_PUBLICATION' => base64_encode(Prado::localize('STATUT_PUBLICATION')),
        ];

        return json_encode($trad, JSON_THROW_ON_ERROR);
    }

    /**
     * @return string|string[]
     */
    public function getUrlConcentrateur(): string|array
    {
        return Atexo_MultiDomaine::replaceDomain(Atexo_Config::getParameter('PF_URL_REFERENCE'));
    }

    protected function getFacturations(CommonConsultation $cons): array
    {
        if (!$cons->getId()) {
            return [];
        }

        $addresses = (new CommonAdresseFacturationJalQuery())->getAddressesByOrganismAndService(
            $cons->getOrganisme(),
            $cons->getServiceId()
        );
        $addressList = [];

        foreach ($addresses as $address) {
            $addressList[] = [
                'address' => $address->getInformationFacturation(),
                'sip' => $address->getFacturationSip() ? 'true' : 'false',
                'email' => $address->getEmailAr()
            ];
        }

        return $addressList;
    }
    public function getConfigPub(CommonConsultation $cons): array
    {
        $configPub = [];

        if ($idAgent = Atexo_CurrentUser::getId()) {
            $agentCreator = (new CommonAgentQuery())->findOneById($idAgent);

            /** @var ConfigurationOrganismeService $configOrgService */
            $configOrgService = Atexo_Util::getSfService(ConfigurationOrganismeService::class);
            /** @var ConfigurationOrganisme $configOrganisme */
            $configOrganisme = $configOrgService->getConfigurationOrganismeByAcronyme($agentCreator->getOrganisme());

            $allConfigPub = [
                'isPubTncp'     => self::CONCENTRATEUR_PUB_TNCP,
                'isPubMol'      => self::CONCENTRATEUR_PUB_MOL,
                'isPubJalFr'    => self::CONCENTRATEUR_PUB_JAL_FR,
                'isPubJalLux'   => self::CONCENTRATEUR_PUB_JAL_LUX,
                'isPubJoue'     => self::CONCENTRATEUR_PUB_JOUE
            ];
            foreach ($allConfigPub as $key => $value) {
                if ($configOrganisme->$key()) {
                    $configPub[] = $value;
                }
            }
        }

        return $configPub;
    }

    public function getConfigEnvoi(CommonConsultation $cons): string
    {
        $configEnvoi = [];
        if ($idAgent = Atexo_CurrentUser::getId()) {
            /** @var AgentService $agentService */
            $agentService = Atexo_Util::getSfService(AgentService::class);
            $agent = $agentService->getAgent($idAgent);

            /** @var EchangeConcentrateur $echangeConcentrateur */
            $echangeConcentrateur = Atexo_Util::getSfService(EchangeConcentrateur::class);

            $configEnvoi = $echangeConcentrateur->getDataEnvoi($agent);
        }

        return json_encode($configEnvoi);
    }

    protected function getEuropeanPub(CommonConsultation $cons): string
    {
        if ($cons->getIdTypeProcedure() && $cons->getOrganisme()) {
            $idTypeAvis = (new CommonTypeAvisPubProcedureQuery())->getTypeAvisByProcedureAndOrganisme(
                $cons->getIdTypeProcedure(),
                $cons->getOrganisme()
            )?->getIdTypeAvis();
            $typeAvis = (new CommonTypeAvisPubQuery())->findOneById($idTypeAvis);
            if (1 === $typeAvis?->getTypePub()) {
                return '1';
            }

            return '0';
        }

        return '0';
    }
}
