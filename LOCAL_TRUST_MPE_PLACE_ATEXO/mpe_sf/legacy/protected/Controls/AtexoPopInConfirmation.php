<?php

namespace Application\Controls;

/**
 * Page de gestion de la pop-in de confirmation.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 1.0
 *
 * @since MPE-4.0
 */
class AtexoPopInConfirmation extends MpeTTemplateControl
{
    private $_modal; //le nom du modal
    private $_message; //le message de confirmation à afficher
    private $_callFrom; //La page où est appelée le composant
    //Les deux parametres suivants sont liés au bouton "Suivant" de la pop-in de génération de l'édition
    private $_functionCallBack; //la fonction appelée après la confirmation (OK)
    private string $_placeFunctionCallBack = 'parent'; //l'endroit où est definie la fonction à appeler après le callBack
                                    //valeur="page" ou "parent" (page si emplacement dans la page mère, parent si c'est la page ou template parent)

    /*
     * Attribut la valeur
     */
    public function setModal($value)
    {
        $this->_modal = $value;
    }

    /*
     * Recupère la valeur
     */
    public function getModal()
    {
        //print_r($this->_modal);exit;
        return $this->_modal;
    }

    /*
     * Attribut la valeur
     */
    public function setMessage($value)
    {
        $this->_message = $value;
    }

    /*
     * Recupère la valeur
     */
    public function getMessage()
    {
        return $this->_message;
    }

    /*
     * Attribut la valeur
     */
    public function setFunctionCallBack($value)
    {
        $this->_functionCallBack = $value;
    }

    /*
     * Recupère la valeur
     */
    public function getFunctionCallBack()
    {
        return $this->_functionCallBack;
    }

    /*
     * Attribut la valeur
     */
    public function setPlaceFunctionCallBack($value)
    {
        $this->_placeFunctionCallBack = $value;
    }

    /*
     * Recupère la valeur
     */
    public function getPlaceFunctionCallBack()
    {
        return $this->_placeFunctionCallBack;
    }

    /*
     * Attribut la valeur
     */
    public function setCallFrom($value)
    {
        $this->_callFrom = $value;
    }

    /*
     * Recupère la valeur
     */
    public function getCallFrom()
    {
        return $this->_callFrom;
    }

    /**
     * Chargement de la page.
     */
    public function onLoad($param)
    {
    }

    /**
     * Appelle la fonction à executer après la confirmation
     * La fonction appelée se trouve dans la page (ou controller) qui appelle ce controller.
     */
    public function fonctionAppeleeApresConfirmation($sender, $param)
    {
        $functionCallBack = $this->getFunctionCallBack();
        if ('parent' == $this->getPlaceFunctionCallBack()) {
            $this->TemplateControl->$functionCallBack($sender, $param);
        } elseif ('page' == $this->getPlaceFunctionCallBack()) {
            $this->Page->$functionCallBack($sender, $param);
        }
    }
}
