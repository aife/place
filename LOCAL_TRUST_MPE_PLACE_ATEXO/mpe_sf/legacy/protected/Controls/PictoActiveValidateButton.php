<?php

namespace Application\Controls;

use Prado\Prado;
use Prado\Web\UI\ActiveControls\TActiveButton;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PictoActiveValidateButton extends TActiveButton
{
    private ?string $_cssClasse = null;

    public function onLoad($param)
    {
        if ($this->getCssClasse()) {
            $this->setCssClass($this->getCssClasse());
        } else {
            $this->setCssClass('bouton-moyen float-right');
        }
        $this->Text = Prado::localize('TEXT_VALIDER_FORMULAIRE');
        $this->Attributes->value = Prado::localize('ICONE_VALIDER');
        $this->setToolTip(Prado::localize('ICONE_VALIDER'));
    }

    /**
     * Affecte la valeur.
     *
     * @param string $value
     */
    public function setCssClasse($value)
    {
        $this->_cssClasse = $value;
    }

    // setCssClass()

    /**
     * recupère la valeur.
     */
    public function getCssClasse()
    {
        return $this->_cssClasse;
    }

    // getCssClass()
}
