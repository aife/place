<?php

namespace Application\Controls;

use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Leaders;

/**
 * Composant Dirigeants de l'Entreprise.
 *
 * @author Anas ZAKI <anas.zaki@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class DirigeantsEntreprise extends MpeTPage
{
    public $idEntreprise;
    /*
     * 0 => mpe normal
     * 1 => mpe responsive
     */
    public int|bool $responsive = 0;

    public function onLoad($param)
    {
        $this->remplirTableauDirigeants($this->getIdEntreprise());
    }

    public function setIdEntreprise($idEntreprise)
    {
        $this->idEntreprise = $idEntreprise;
    }

    public function getIdEntreprise()
    {
        return $this->idEntreprise;
    }

    public function remplirTableauDirigeants($idEntreprise)
    {
        $leaders = Atexo_Entreprise_Leaders::retrieveLeaders($idEntreprise);

        if (is_array($leaders)) {
            $this->repeaterDirigeants->dataSource = $leaders;
            $this->repeaterDirigeants->dataBind();
        }
    }

    /**
     * @return bool
     */
    public function getResponsive()
    {
        return $this->responsive;
    }

    /**
     * @param bool $responsive
     */
    public function setResponsive($responsive)
    {
        $this->responsive = $responsive;
    }
}
