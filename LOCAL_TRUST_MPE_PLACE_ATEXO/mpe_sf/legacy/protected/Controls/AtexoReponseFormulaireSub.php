<?php

namespace Application\Controls;

use Application\Propel\Mpe\CommonTDossierFormulaire;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\FormulaireSub\Atexo_FormulaireSub_Dossier;
use Application\Service\Atexo\Signature\Atexo_Signature_FichierVo;
use Prado\Prado;

/**
 * Page de gestion des formulaires de réponse herités de SUB.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 1.0
 *
 * @since MPE-4.0
 */
class AtexoReponseFormulaireSub extends MpeTTemplateControl
{
    public $_idHtlmToggleFormulaire;
    public $_refConsultation;
    public $_orgConsultation;
    public $_numLot;
    public $_typeEnv;

    /**
     * Recupère la valeur.
     */
    public function getIdHtlmToggleFormulaire()
    {
        return $this->_idHtlmToggleFormulaire;
    }

    /**
     * Attribut la valeur.
     */
    public function setIdHtlmToggleFormulaire($value)
    {
        $this->_idHtlmToggleFormulaire = $value;
    }

    /**
     * Recupère la valeur.
     */
    public function getNumLot()
    {
        $numLot = $this->_numLot;
        if (!$numLot) {
            $numLot = $this->sourceTemplateControl->numLot->Value;
        }

        return $numLot;
    }

    /**
     * Affectes la valeur.
     */
    public function setNumLot($value)
    {
        $this->_numLot = $value;
    }

    /**
     * Recupère la valeur.
     */
    public function getRefConsultation()
    {
        return $this->_refConsultation;
    }

    /**
     * Affectes la valeur.
     */
    public function setRefConsultation($value)
    {
        $this->_refConsultation = $value;
    }

    /**
     * Recupère la valeur.
     */
    public function getOrgConsultation()
    {
        return $this->_orgConsultation;
    }

    /**
     * Affectes la valeur.
     */
    public function setOrgConsultation($value)
    {
        $this->_orgConsultation = $value;
    }

    /**
     * Permet de recuperer la consultation.
     */
    public function getConsultation()
    {
        return $this->Page->getViewState('consultation');
    }

    /**
     * Recupère la valeur.
     */
    public function getTypeEnv()
    {
        return $this->sourceTemplateControl->typeEnveloppe->Value;
    }

    /**
     * Affectes la valeur.
     */
    public function setTypeEnv($value)
    {
        $this->_typeEnv = $value;
    }

    /**
     * Chargement de la page.
     */
    public function onLoad($param)
    {
        //      echo "OnLoad : AtexoReponseFormulaireSub " . $this->getTypeEnv() . " - " . date('Y-m-d H:i:s') . "\n";
        if (Atexo_Module::isEnabled('InterfaceModuleSub') && !$this->Page->IsPostBack && !$this->Page->IsCallBack) {
            $this->initDataSource();
            $this->remplirEditionsFormulaire();
        }
    }

    /**
     * Permet de recuperer le dossier formulaire SUB.
     */
    public function getDossier()
    {
        $dossier = false;
        $reponseElectFormulaire = (new Atexo_FormulaireSub_Dossier())->recupererReponseElectFormulaire($this->getConsultation()->getId(), $this->getConsultation()->getOrganisme(), Atexo_CurrentUser::getIdInscrit());
        if ($reponseElectFormulaire) {
            $dossier = (new Atexo_FormulaireSub_Dossier())->getDossierByIdReponseElectFormulaireAndTypeEnveloppeAndNumLot($reponseElectFormulaire->getIdReponseElecFormulaire(), $this->getTypeEnv(), $this->getNumLot());
        }

        return $dossier;
    }

    /**
     * Recupere le nom du formulaire.
     */
    public function getNomFormulaire()
    {
        if ($this->getTypeEnv() == Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE')) {
            $nomFormulaire = Prado::localize('DEFINE_FORMULAIRE_CANDIDATURE');
        } else {
            $nomFormulaire = Prado::localize('DEFINE_FORMULAIRE_OFFRE');
        }

        return $nomFormulaire;
    }

    /**
     * Permet de remplir les éditions des formulaires.
     */
    public function remplirEditionsFormulaire()
    {
        $this->listeEditionsSub->DataSource = $this->getDataSource();
        $this->listeEditionsSub->DataBind();
    }

    public function initDataSource()
    {
        if (!$this->getDataSource()) {
            if ($this->getDossier() instanceof CommonTDossierFormulaire) {
                $idDossier = $this->getDossier()->getIdDossierFormulaire();
                $listeEditions = (new Atexo_FormulaireSub_Dossier())->recupererEditionsFormulaire($idDossier);
                $arrayEditions = [];
                if (is_countable($listeEditions) ? count($listeEditions) : 0) {
                    foreach ($listeEditions as $edition) {
                        $fichierVo = new Atexo_Signature_FichierVo();
                        $fichierVo->setIdentifiant($this->Page->getLastIdFichier());
                        $fichierVo->setIdFichier($edition->getIdEditionFormulaire());
                        if ($edition->getNomFichier()) {
                            $fichierVo->setNomFichier($edition->getNomFichier());
                        } elseif ($edition->getLibelle()) {
                            $fichierVo->setNomFichier($edition->getLibelle());
                        }
                        $fichierVo->setCheminFichier($edition->getPath());
                        $fichierVo->setTypeFichier(Atexo_Config::getParameter('TYPE_DOCUMENT_HASH'));
                        $fichierVo->setHashFichier($edition->getHash());
                        if ($this->getDossier() instanceof CommonTDossierFormulaire) {
                            $fichierVo->setTypeEnveloppe($this->getDossier()->getTypeEnveloppe());
                            $fichierVo->setNumeroLot($this->getDossier()->getIdLot());
                        }
                        if ($edition->getType() == Atexo_Config::getParameter('TYPE_DOCUMENT_EDITION')) {
                            $fichierVo->setOrgineFichier(Atexo_Config::getParameter('TYPE_DOCUMENT_EDITION'));
                        } elseif ($edition->getType() == Atexo_Config::getParameter('TYPE_DOCUMENT_PJ')) {
                            $fichierVo->setOrgineFichier(Atexo_Config::getParameter('TYPE_DOCUMENT_PJ'));
                        }
                        $arrayEditions[$fichierVo->getIdentifiant()] = $fichierVo;
                    }
                }
                $this->setDataSource($arrayEditions);
                // print_r($this->getDataSource());exit;
            }
        }
    }

    public function getDataSource()
    {
        $arrayPiece = $this->Page->getViewState('dataSourcePieces');

        return $arrayPiece[$this->getTypeEnv().'_'.$this->getNumLot()]['pieceEdition'];
    }

    public function setDataSource($arrayPiece)
    {
        $dataSource = $this->Page->getViewState('dataSourcePieces');
        $dataSource[$this->getTypeEnv().'_'.$this->getNumLot()]['pieceEdition'] = $arrayPiece;
        $this->Page->setViewState('dataSourcePieces', $dataSource);
    }

    /**
     * Permet de recuperer toutes les pieces éditions cochées.
     */
    public function recupererPiecesCochees(&$listPiecesCoches)
    {
        $dataKeys = $this->listeEditionsSub->getDataKeys();
        foreach ($this->listeEditionsSub->getItems() as $itemEdition) {
            if (true == $itemEdition->idCheckBoxSelectionPieceEdition->Checked) {
                $listPiecesCoches[] = $this->getFichierFromDataSource($dataKeys[$itemEdition->getItemIndex()]);
            }
        }
    }

    /**
     * Permet de recuperer les pièces cochées depuis la source des données.
     */
    public function getFichierFromDataSource($identifiant)
    {
        $arrayPiece = $this->getDataSource();

        return $arrayPiece[$identifiant];
    }

    /**
     * Recupère l'image du picto si signature existe ou pas.
     */
    public function getPictoSignatureExiste($fichier)
    {
        return $this->SourceTemplateControl->getPictoSignatureExiste($fichier);
    }

    /**
     * Recupère le message alt du picto si signature existe ou pas.
     */
    public function getAltSignatureExiste($fichier)
    {
        return $this->SourceTemplateControl->getAltSignatureExiste($fichier);
    }

    /**
     * Recupère l'image du picto de la verification de la signature.
     */
    public function getPictoVerifSignature($fichier)
    {
        return $this->SourceTemplateControl->getPictoVerifSignature($fichier);
    }

    /**
     * Recupère le message alt du picto de la verification de la signature.
     */
    public function getAltVerifSignature($fichier)
    {
        return $this->SourceTemplateControl->getAltVerifSignature($fichier);
    }

    /**
     * Recupère l'image du picto si signature existe ou pas.
     */
    public function afficherLienSignature($fichier)
    {
        return $this->SourceTemplateControl->afficherLienSignature($fichier);
    }

    /**
     * Recupère l'emetteur et le signataire (CN, AC).
     */
    public function getInfosSignataire($fichier)
    {
        return $this->SourceTemplateControl->getInfosSignataire($fichier);
    }

    /**
     * Recupère l'image du statut du dossier.
     */
    public function getStatutPictoDossier($dossier)
    {
        return $this->SourceTemplateControl->getStatutPictoDossier($dossier);
    }

    /**
     * Recupère le statut du dossier.
     */
    public function getStatutDossier($dossier)
    {
        return $this->SourceTemplateControl->getStatutDossier($dossier);
    }

    /**
     * Permet de gerer la visibilité de la case à cocher pour la signature des fichiers de réponse.
     */
    public function getVisibiliteCocherSignature($fichier)
    {
        return $this->SourceTemplateControl->getVisibiliteCocherSignature($fichier);
    }

    /**
     * Permet d'afficher les détails de la signature.
     */
    public function afficherDetailsSignature($sender, $param)
    {
        if ($param->CallbackParameter || 0 === $param->CallbackParameter) {
            $dataKeys = $this->listeEditionsSub->getDataKeys();
            $fichier = $this->getFichierFromDataSource($dataKeys[$param->CallbackParameter]);
            if ($fichier instanceof Atexo_Signature_FichierVo) {
                //L'information fichierVo sera exploitée lors de l'appel du composant <com:AtexoDetailsSignatureFormulaireReponse/>
                //dans le fichier AtexoReponseEnveloppe.tpl
                $this->Page->setViewState('fichierVo', $fichier);
                $this->Page->setViewState('identifiantFichier', $fichier->getIdentifiant());
                $script = "<script>
            				  openModal('modal-ajout-offre_".$fichier->getIdentifiant()."','popup-800',document.getElementById('".$this->Page->scriptJsPopInDetailsSignature->getClientId()."'));
            			  </script>";
            }
            //print_r($this->Page->getViewState("signatureFichier"));exit;
            $this->Page->raffraichirListeFormulaires($sender, $param);
            $this->Page->panelDetailsSignatureFormulaireReponse->render($param->getNewWriter());
            $this->Page->scriptJsPopInDetailsSignature->Text = $script;
        }
    }
}
