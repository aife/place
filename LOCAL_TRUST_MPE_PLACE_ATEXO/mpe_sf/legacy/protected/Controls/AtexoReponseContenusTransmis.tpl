	<com:TConditional condition=" Application\Service\Atexo\Atexo_Config::getParameter('ACTIVATION_SERVEUR_CRYPTOGRAPHIQUE')">
		<prop:trueTemplate>
			<%include Application.templates.crypto.AtexoReponseContenusTransmis %>
		</prop:trueTemplate>
		<prop:falseTemplate>
			<%include Application.templates.mpe.AtexoReponseContenusTransmis %>
		</prop:falseTemplate>
	</com:TConditional>