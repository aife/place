<?php

namespace Application\Controls;

use App\Service\WebServices\WebServicesExec;
use Application\Propel\Mpe\CommonTContratMulti;
use Application\Propel\Mpe\CommonTContratTitulaire;
use Application\Propel\Mpe\CommonTContratTitulaireQuery;
use Application\Propel\Mpe\CommonTEtablissement;
use Application\Propel\Mpe\CommonTTypeContrat;
use Application\Propel\Mpe\Entreprise;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Entreprise;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Contrat;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Lots;
use DateTime;
use Prado\Prado;

/**
 * Le bloc de recap de consultation.
 *
 * @author LEZ <loubna.ezziani@atexo.com>
 *
 * @version 1.0
 *
 * @since 4.11.0
 *
 * @copyright Atexo 2015
 */
class AtexoBlocRecapConsultationChorus extends MpeTTemplateControl
{
    public function setIdContrat($value)
    {
        $this->setViewState('idContrat', $value);
    }

    public function getIdContrat()
    {
        return $this->getViewState('idContrat');
    }

    public function initialiserComposant($lots, $idContrat = null)
    {
        $this->setIdContrat($idContrat);
        if ($idContrat || isset($_GET['uuid'])) {
            $this->fillInfoAttribution($idContrat, $lots);
        } else {
            $this->setVisible(false);
        }
    }

    public function fillInfoLots($contrat, $lots)
    {
        if ($contrat instanceof CommonTContratTitulaire) {
            $listeLots = explode('#', $lots);
            if (is_array($listeLots)) {
                $categorieLots = (new Atexo_Consultation_Lots())->retrieveListeLotsByArray($contrat->getRefConsDonneesConsultation(), $contrat->getOrganisme(), $listeLots);
                $this->repeaterLots->DataSource = $categorieLots;
                $this->repeaterLots->DataBind();
            }
        }
    }

    /**
     * Permet de replire des information sur l'attribution.
     *
     * @param int $idContrat l'id de contrat
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function fillInfoAttribution($idContrat, $lots)
    {
        $contratQuery = new CommonTContratTitulaireQuery(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $contrat = $contratQuery->getTContratTitulaireById($idContrat);

        if (Atexo_Config::getParameter('ACTIVE_EXEC_V2') && isset($_GET['uuid'])) {
            $uuidContratExec = $_GET['uuid'];

            /** @var $execWS WebServicesExec */
            $execWS = Atexo_Util::getSfService(WebServicesExec::class);
            $contratExec = $execWS->getContent('getContratByUuid', $uuidContratExec);

            if ($contratExec) {
                $this->labelReferenceContrat->Text = $contratExec['referenceLibre'];
                $this->labelObjetContrat->Text = $contratExec['objet'];
                $this->labelTitulaireContrat->Text = $this->getComputedTitulaireLabelInfos($contratExec);

                if ($contratExec['dateNotification'] ?? '') {
                    $dateNotification = (new DateTime($contratExec['dateNotification']))->format('d/m/Y');
                } else {
                    $date = $contratExec['datePrevisionnelleNotification'] ?
                        (new DateTime($contratExec['datePrevisionnelleNotification']))->format('d/m/Y')
                        : ''
                    ;
                    $dateNotification = sprintf(
                        '%s <i>%s</i>',
                        $date,
                        Prado::localize('DATE_PREVISIONNELLE')
                    );
                }

                $this->labelDateNotification->Text = $dateNotification;
                $this->labelMontant->Text = $contratExec['montant'] ?
                    Atexo_Util::getMontantArronditEspace($contratExec['montant'])
                    : '-'
                ;
            }
        } elseif ($contrat instanceof CommonTContratTitulaire) {
            $this->labelReferenceContrat->Text = $contrat->getReferenceLibre();
            $this->labelObjetContrat->Text = $contrat->getObjetContrat();
            $nomEntreprise = $contrat->getNomTitulaireContrat();
            $labelTitulaire = $nomEntreprise.' ';

            $cp = $contrat->getCodePostalEtablissementContrat();
            $ville = $contrat->getVilleEtablissementContrat();
            if ($cp || $ville) {
                $labelTitulaire .= '(';
                if ($cp) {
                    $labelTitulaire .= $cp;
                    if ($ville) {
                        $labelTitulaire .= ' - '.$ville;
                    }
                } else {
                    $labelTitulaire .= $ville;
                }

                $labelTitulaire .= ')';
            }
            $titulaire = $contrat->getTitulaireContrat();
            if ($titulaire instanceof Entreprise) {
                $siren = (new Atexo_Entreprise())->getIndentifiantLocalOuEtranger($titulaire);
            }
            $etablissement = $contrat->getTitulaireEtablissementContrat();
            if ($etablissement instanceof CommonTEtablissement) {
                $codeEtab = $etablissement->getCodeEtablissement();
            }
            $this->labelTitulaireContrat->Text = "$labelTitulaire - $siren $codeEtab";
            if ($contrat->getDateNotification()) {
                $dateNotification = $contrat->getDateNotification();
            } else {
                $dateNotification = $contrat->getDatePrevueNotification().' <i>'.Prado::localize('DATE_PREVISIONNELLE').'</i>';
            }
            $this->labelDateNotification->Text = $dateNotification;
            //Montant
            $montant = $this->recupererMontantContratMulti($contrat);
            if (empty($montant)) {
                $montant = $contrat->getMontantMaxEstime();
            }
            if (!$montant) {
                $montant = $contrat->getMontantContrat();
            }
            $this->labelMontant->Text = (($montant) ? Atexo_Util::getMontantArronditEspace($montant) : '-');
            //$this->typeContrat->Text = Atexo_Consultation_Contrat::getLibelleTypeContratById($contrat->getIdTypeContrat());
            // $this->dateAttribution->Text = $contrat->getDateAttribution('d/m/Y');
            if ($lots) {
                self::fillInfoLots($contrat, $lots);
            }
        }
    }

    public function getComputedTitulaireLabelInfos(?array $contratExec): string
    {
        $labelTitulaire = $contratExec['etablissementTitulaire']['fournisseur']['raisonSociale'] ?? '';
        $siret = $contratExec['etablissementTitulaire']['siret'] ?? '';
        $cp = $contratExec['etablissementTitulaire']['adresse']['codePostal'] ?? null;
        $ville = $contratExec['etablissementTitulaire']['adresse']['ville'] ?? null;

        if ($cp || $ville) {
            $labelTitulaire .= '(';
            if ($cp) {
                $labelTitulaire .= $cp;
                if ($ville) {
                    $labelTitulaire .= ' - '.$ville;
                }
            } else {
                $labelTitulaire .= $ville;
            }

            $labelTitulaire .= ')';
        }

        return $labelTitulaire . ' - ' . $siret;
    }

    /**
     * Permet de recuperer le montant du chapeau dans le cas d'un contrat multi attributaire.
     *
     * @param $contrat
     *
     * @return string
     */
    public function recupererMontantContratMulti($contrat)
    {
        if ($contrat && ($contrat->getCommonTContratMulti() instanceof CommonTContratMulti)) {
            return $contrat->getCommonTContratMulti()->getMontantMaxEstime();
        }

        return '';
    }

    public function getLibelleMontant()
    {
        $idContrat = $this->getIdContrat();
        $contratQuery = new CommonTContratTitulaireQuery(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $contrat = $contratQuery->getTContratTitulaireById($idContrat);
        if ($contrat && ($contrat->getCommonTContratMulti() instanceof CommonTContratMulti)) {
            return Prado::localize('MONTANT_MAX_HT');
        }

        return Prado::localize('MONTANT');
    }

    /**
     * Permet de calculer le libelle a afficher pour le type de marche.
     *
     * @param string $idTypeContrat : identifiant technique du type de contrat
     *
     * @return mixed|string
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function getLibelleTypeMarche()
    {
        $libelle = '';
        if (Atexo_Config::getParameter('ACTIVE_EXEC_V2') && isset($_GET['uuid'])) {
            $uuidContratExec = $_GET['uuid'];

            /** @var $execWS WebServicesExec */
            $execWS = Atexo_Util::getSfService(WebServicesExec::class);
            $contratExec = $execWS->getContent('getContratByUuid', $uuidContratExec);

            if ($contratExec) {
                return $contratExec['type']['abreviation'] ?? '';
            }
        } else {
            $idContrat = $this->getIdContrat();
            $contratQuery = new CommonTContratTitulaireQuery(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            $contrat = $contratQuery->getTContratTitulaireById($idContrat);
            if ($contrat instanceof CommonTContratTitulaire) {
                if ((new Atexo_Consultation_Contrat())->isRetourChorusModeIntegre($idContrat)) {
                    $typeContrat = $contrat->getTypeContrat();
                    if ($typeContrat instanceof CommonTTypeContrat) {
                        if ($typeContrat->getAccordCadreSad() == Atexo_Config::getParameter('TYPE_CONTRAT_ACCORD_CADRE')) {
                            $libelle = Prado::localize('DEFINE_ACCORD_CADRES');
                        } elseif ($typeContrat->getAccordCadreSad() == Atexo_Config::getParameter('TYPE_CONTRAT_SAD')) {
                            $libelle = Prado::localize('DEFINE_SYSTEME_ACQUISITION_DYNAMIQUE');
                        }
                    }
                } else {
                    $libelle = Prado::localize('DEFINE_MARCHE_SIMPLE');
                }
            }
        }

        return $libelle;
    }
}
