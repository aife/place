<com:TActivePanel id="panelDossierVolumineux" CssClass="form-field" visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('ModuleEnvol') && Application\Service\Atexo\Atexo_CurrentUser::hasHabilitation('GestionEnvol')) ? true : false %>">
    <div class="top">
        <span class="left">&nbsp;</span>
        <span class="right">&nbsp;</span>
        <span class="title">
             <com:TPanel ID="textUpDV" visible="<%=$this->getUpdatemode()%>" >
                <com:TTranslate>DEFINE_MODIFIER_DCE_ANNEXES_TECHNIQUE</com:TTranslate>
            </com:TPanel>
            <com:TPanel ID="textAddDV" visible="<%=!$this->getUpdatemode()%>" >
                <com:TTranslate>DEFINE_DCE_ANNEXES_TECHNIQUE</com:TTranslate>
            </com:TPanel>
        </span>
    </div>
    <div class="content">
        <div class="line">
            <div class="intitule-auto"><com:TTranslate>DECRIPTION_ANNEXE_TECHNIQUE_DCE</com:TTranslate></div>
        </div>
        <com:TActivePanel id="panelAutorisationDossiersVolumineuxEnvol" >
            <com:TActivePanel id="panelAutorisationDossiersVolumineuxEnvolNo" CssClass="line">
                <div class="intitule-auto">
                    <com:TActiveRadioButton
                            id="autorisationDossiersVolumineuxEnvolNo"
                            GroupName="autorisationDossiersVolumineuxEnvol"
                            Attributes.title=""
                            CssClass="radio"
                            OnCallback="toggleListeDossiersVolumineux"
                    />
                    <label for="autorisationDossiersVolumineuxEnvolNo">
                        <com:TTranslate>TEXT_NON</com:TTranslate>
                    </label>
                </div>
            </com:TActivePanel>

            <com:TActivePanel id="panelAutorisationDossiersVolumineuxEnvolYes" CssClass="line">
                <div class="intitule-auto">
                    <com:TActiveRadioButton
                            id="autorisationDossiersVolumineuxEnvolYes"
                            GroupName="autorisationDossiersVolumineuxEnvol"
                            Attributes.title=""
                            CssClass="radio"
                            OnCallback="toggleListeDossiersVolumineux"
                    />
                    <label for="autorisationDossiersVolumineuxEnvolYes">
                        <com:TTranslate>TEXT_OUI</com:TTranslate>
                    </label>
                </div>
            </com:TActivePanel>
        </com:TActivePanel>

        <div class="spacer-small"></div>

        <com:TActivePanel id="panelListeDossierVolumineux">
            <div class="intitule-130 line">
                <label for="listeDossierVolumineux"><com:TTranslate>DOSSIER_VOLUMINEUX</com:TTranslate></label> :
                <img title="Info-bulle" alt="Info-bulle" class="picto-info" onmouseout="cacheBulle('infosDV')" onmouseover="afficheBulle('infosDV', this)" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" />
                <div id="infosDV" class="info-bulle" onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();">
                    <div>
                        <com:TTranslate>INFO_BULLE_DOSSIER_VOLUMINEUX_ANNEXE_DCE</com:TTranslate>
                    </div>
                </div>
            </div>
            <div class="content-bloc bloc-630">
                <com:TActiveDropDownList ID="listeDossierVolumineux" cssclass="bloc-630"/>
            </div>
        </com:TActivePanel>

        <div class="breaker"></div>
    </div>
    <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
</com:TActivePanel>