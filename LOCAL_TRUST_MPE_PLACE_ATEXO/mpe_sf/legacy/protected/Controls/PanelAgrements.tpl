		<com:TActivePanel ID="panelAgrement" >	
		<div class="content-bloc">
		    <com:TActiveHiddenField ID="idsSelectedAgrements" />
			<div><com:TActiveLabel ID="labelleAgrement" />
			<span
				onmouseover="afficheBulle('infosAgrement', this)" 
				onmouseout="cacheBulle('infosAgrement')" 
				class="info-suite"> <com:TActiveLabel Display="None" ID="toisPoints" Text ="..."/>
			</span>
			</div>
				<div class="spacer-mini"></div>
			<com:TActiveHyperLink NavigateUrl="javascript:new_window('index.php?page=<%= Application\Service\Atexo\Atexo_Util::getTypeUserCalledForPradoPages() %>.PopupAgrements&ids=<%=base64_encode($this->idsSelectedAgrements->Value)%>&clientId=<%=$this->getClientId()%>&callFrom=<%= $this->getCallFrom() ? $this->getCallFrom() : $this->Page->Master->getCalledFrom()%>','yes');" cssClass="bouton-small"><com:TTranslate>DEFINIR</com:TTranslate></com:TActiveHyperLink>
		</div>
		<div ID="infosAgrement" class="info-bulle" >
				<div><com:TActiveLabel ID="libellesInfoBulleAgrement"/></div>
		</div>
		<span style="display:none; ">
			 <com:TActiveButton ID="selectedAgrements"  OnCallBack="refresh" OnCommand="displaySelectedAgrements" />
		</span>
		</com:TActivePanel>
