<?php

namespace Application\Controls;

use App\Service\ContratService;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonTReferentielMotsCles;
use Application\Propel\Mpe\CommonTReferentielMotsClesQuery;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Exception;
use Prado;

class DonneesComplementairesProcedure extends MpeTTemplateControl
{
    private $mode;
    private $_donneeComplementaire;

    public const ELECTRONIC_CATALOG_CHOICES_FORM = [
        'REQUIRED' => 'ELECTRONIC_CATALOG_REQUIRED',
        'AUTHORIZED' => 'ELECTRONIC_CATALOG_AUTHORIZED',
        'FORBIDDEN' => 'ELECTRONIC_CATALOG_FORBIDDEN',
    ];

    public function getDonneeComplementaire()
    {
        return $this->_donneeComplementaire;
    }

    public function setDonneeComplementaire($donneeComplementaire)
    {
        $this->_donneeComplementaire = $donneeComplementaire;
    }

    public function onLoad($param)
    {
        if (Atexo_Module::isEnabled('DonneesComplementaires') && Atexo_Module::isEnabled('DonneesRedac')) {
            if (!$this->Page->IsPostBack) {
                $this->populateDatas();
                $this->rafraichissementJs();
                if (Atexo_Module::isEnabled('Publicite')) {
                    $this->chargerMotsClesDescripteurs();
                }
            }
        }
        $this->traductionSpecificText();
    }

    public function saveProcedure($donneComplementaire)
    {
        $this->_donneeComplementaire = $donneComplementaire;
        if ($this->offreFixe->Checked) {
            $this->_donneeComplementaire->setIdNbCandidatsAdmis(Atexo_Config::getParameter('ID_NB_CANDIDATS_ADMIS_FIXE'));
            $this->_donneeComplementaire->setNombreCandidatsFixe($this->nbCandidatsFixeInput->Text);
            self::viderFourchette();
        } elseif ($this->offreFourchette->Checked) {
            $this->_donneeComplementaire->setIdNbCandidatsAdmis(Atexo_Config::getParameter('ID_NB_CANDIDATS_ADMIS_FOURCHETTE'));
            $this->_donneeComplementaire->setNombreCandidatsMin($this->fourchetteMini->Text);
            $this->_donneeComplementaire->setNombreCandidatsMax($this->fourchetteMax->Text);
            self::viderFixe();
        }
        if ($this->procedurePhaseSuccessive->Checked) {
            $this->_donneeComplementaire->setPhaseSuccessive(1);
        } else {
            $this->_donneeComplementaire->setPhaseSuccessive(0);
        }
        $this->_donneeComplementaire->setDelaiValiditeOffres($this->dureeValidite->Text);

        if (Atexo_Module::isEnabled('Publicite')) {
            if ($this->accordMarchePublicOmc->Checked) {
                $this->_donneeComplementaire->setProcedureAccordMarchesPublicsOmc(1);
            } else {
                $this->_donneeComplementaire->setProcedureAccordMarchesPublicsOmc(0);
            }

            $this->_donneeComplementaire->setMotsCles($this->motsCles->getSelectedValues());
        }

        if ($this->getViewState('mandatoryVisit') !== null) {
            $this->_donneeComplementaire->setVisiteObligatoire($this->getViewState('mandatoryVisit'));
        }

        if (true === $this->_donneeComplementaire->getVisiteObligatoire()) {
            $description = (!empty($this->descMandatoryVisit->Text))
                ? (new Atexo_Config())->toPfEncoding($this->descMandatoryVisit->Text)
                : null;
            $this->_donneeComplementaire->setVisiteDescription($description);
        } elseif (false === $this->_donneeComplementaire->getVisiteObligatoire()) {
            $this->_donneeComplementaire->setVisiteDescription(null);
        }

        if ($this->getViewState('attributionWhitoutNegociation') !== null) {
            $this->_donneeComplementaire->setAttributionSansNegociation($this->getViewState('attributionWhitoutNegociation'));
        }

        if ($this->electronicCatalog->getSelectedValue() > 0) {
            $this->_donneeComplementaire->setCatalogueElectronique($this->electronicCatalog->getSelectedValue());
        }

        $consultation = $this->Page->getConsultation();
        $contratService = Atexo_Util::getSfService(ContratService::class);
        $techniqueAchat = $contratService->getTechniqueAchatLibelle($consultation);

        if ($techniqueAchat !== null) {
            if ($this->technique_achat->Text != $techniqueAchat) {
                $consultation->setAutreTechniqueAchat($this->technique_achat->Text);
            }
        }
    }

    public function populateDatas()
    {
        // Nombre de candidats admis à présenter une offre
        if ($this->_donneeComplementaire->getIdNbCandidatsAdmis() == Atexo_Config::getParameter('ID_NB_CANDIDATS_ADMIS_FIXE')) {
            $this->offreFixe->Checked = true;
            $this->nbCandidatsFixeInput->Text = $this->_donneeComplementaire->getNombreCandidatsFixe();
        } elseif ($this->_donneeComplementaire->getIdNbCandidatsAdmis() == Atexo_Config::getParameter('ID_NB_CANDIDATS_ADMIS_FOURCHETTE')) {
            $this->offreFourchette->Checked = true;
            $this->fourchetteMini->Text = $this->_donneeComplementaire->getNombreCandidatsMin();
            $this->fourchetteMax->Text = $this->_donneeComplementaire->getNombreCandidatsMax();
        }
        if ($this->_donneeComplementaire->getPhaseSuccessive()) {
            $this->procedurePhaseSuccessive->Checked = true;
        } else {
            $this->procedurePhaseSuccessive->Checked = false;
        }
        $this->dureeValidite->Text = $this->_donneeComplementaire->getDelaiValiditeOffres();

        if (Atexo_Module::isEnabled('Publicite')) {
            if ($this->_donneeComplementaire->getProcedureAccordMarchesPublicsOmc()) {
                $this->accordMarchePublicOmc->Checked = true;
            } else {
                $this->accordMarchePublicOmc->Checked = false;
            }

            if ($this->_donneeComplementaire->getMotsCles()) {
                $this->motsCles->setSelectedValues(explode(',', $this->_donneeComplementaire->getMotsCles()));
            }
        }

        if (true === $this->_donneeComplementaire->getVisiteObligatoire()) {
            $this->mandatoryVisitYes->Selected = true;
            $description = $this->_donneeComplementaire->getVisiteDescription() ?: '';
            $this->descMandatoryVisit->Text = $description;
        } elseif (false === $this->_donneeComplementaire->getVisiteObligatoire()) {
            $this->mandatoryVisitNo->Selected = true;
        }

        if (true === $this->_donneeComplementaire->getAttributionSansNegociation()) {
            $this->attributionWhitoutNegociationYes->Selected = true;
        } elseif (false === $this->_donneeComplementaire->getAttributionSansNegociation()) {
            $this->attributionWhitoutNegociationNo->Selected = true;
        }

        $contratService = Atexo_Util::getSfService(ContratService::class);
        $techniqueAchat = $contratService->getTechniqueAchatLibelle($this->Page->getConsultation());
        if ($techniqueAchat !== null) {
            $this->technique_achat->Text = $techniqueAchat;
        }
    }

    public function rafraichissementJs()
    {
        $this->scriptLabel->Text = '<script>'.
                                    "isChecked2(document.getElementById('".$this->offreFixe->ClientId."'),'nbCandidatsFixe','nbCandidatsFourchette');".
                                    "isChecked2(document.getElementById('".$this->offreFourchette->ClientId."'),'nbCandidatsFourchette','nbCandidatsFixe');".
                                    '</script>';
    }

    public function viderFixe()
    {
        $this->_donneeComplementaire->setNombreCandidatsFixe(null);
    }

    public function viderFourchette()
    {
        $this->_donneeComplementaire->setNombreCandidatsMin(null);
        $this->_donneeComplementaire->setNombreCandidatsMax(null);
    }

    /**
     * Permet de charger les mots cles descripteurs.
     */
    public function chargerMotsClesDescripteurs()
    {
        try {
            $listesMotsCles = [];
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            $allMotsCles = (array) CommonTReferentielMotsClesQuery::create()->find($connexion);
            foreach ($allMotsCles as $motCle) {
                if ($motCle instanceof CommonTReferentielMotsCles) {
                    $listesMotsCles[$motCle->getId()] = $motCle->getId().' - '.$motCle->getLibelle();
                }
            }
            $this->motsCles->setDatasource($listesMotsCles);
        } catch (Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('app');
            $logger->error("Erreur recuperation 'Mots cles' : \n\nErreur = ".$e->getMessage()." \n\nTrace:\n ".$e->getTraceAsString()." \n\n Methode = DonneesComplementairesProcedure::chargerMotsClesDescripteurs");
        }
    }

    public function enabledElement(): bool
    {
        return match ($this->mode) {
            0 => false,
            default => true,
        };
    }

    /**
     * @param object $inputRadio
     * @return void
     */
    protected function mandatoryVisitSelected(object $inputRadio): void
    {
        $valueInput = $this->radioButtonSelected($inputRadio);
        $this->setViewState('mandatoryVisit', $valueInput);
    }

    /**
     * @param object $inputRadio
     * @return void
     */
    protected function attributionWhitoutNegociationSelected(object $inputRadio): void
    {
        $valueInput = $this->radioButtonSelected($inputRadio);
        $this->setViewState('attributionWhitoutNegociation', $valueInput);
    }

    /**
     * Récupère la valeur du Radio Button selectionné
     *
     * @param object $inputRadio
     * @return string|null
     */
    private function radioButtonSelected(object $inputRadio): ?string
    {
        $indices = $inputRadio->SelectedIndices;
        $item = $inputRadio->Items[$indices[0]];

        return $item ? $item->Value : null;
    }

    /**
     * Rempli le champ (Select-Option) du catalogue électronique
     *
     * @return void
     */
    public function fillElectronicCatalog(): void
    {
        $dataSource = [
            (new Atexo_Config())->toPfEncoding(Prado::localize('TEXT_SELECTIONNER').'...'),
            Prado::localize(self::ELECTRONIC_CATALOG_CHOICES_FORM['REQUIRED']),
            Prado::localize(self::ELECTRONIC_CATALOG_CHOICES_FORM['AUTHORIZED']),
            Prado::localize(self::ELECTRONIC_CATALOG_CHOICES_FORM['FORBIDDEN'])
        ];

        $this->electronicCatalog->DataSource = $dataSource;
        $this->electronicCatalog->DataBind();

        if ($this->_donneeComplementaire->getCatalogueElectronique()) {
            $this->electronicCatalog->SelectedValue = $this->_donneeComplementaire->getCatalogueElectronique();
        }
    }

    /**
     * Traduit du texte qui pour une raison inconnu n'est pas en FR dans le .tpl
     *
     * @return void
     */
    private function traductionSpecificText(): void
    {
        $idTextToTranslate = [
            'attributionWhitoutNegociationYes',
            'attributionWhitoutNegociationNo',
            'mandatoryVisitYes',
            'mandatoryVisitNo',
        ];

        foreach ($idTextToTranslate as $id) {
            $this->$id->Text = (str_contains($id, 'Yes'))
                ? Prado::localize('DEFINE_OUI')
                : Prado::localize('DEFINE_NON')
            ;
        }
    }
}
