<!--Debut Bloc formulaire-->
			<com:TActivePanel cssclass="form-field" id="bloc_etapeDonneesComplementaires">
				<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
				<div class="content">
					<h3 class="float-left"><com:TTranslate>DEFINE_DONNES_COMPLEMENTAIRES_CONSULTATION</com:TTranslate></h3>
					<div class="float-right margin-fix"><com:TTranslate>DEFINE_SYMBOLE</com:TTranslate> <span class="champ-oblig">*</span> <com:TTranslate>TEXT_INDIQUE_CHAMPS_OBLIGATOIRES</com:TTranslate></div>
					<div class="breaker"></div>
						<!--Debut Bloc Delai Adresse depot / retrait / lieu d'ouverture de Pli -->
						<com:TActivePanel ID="panelAdresseRetraitDossiers" cssClass="line" visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('ConsultationAdresseRetraisDossiers') &&  Application\Service\Atexo\Atexo_Config::getParameter('AFFICHAGE_DONNEES_COMPLEMENTAIRES_PERSONNALISEE')) ? 'true' : 'false' %>">
							<div class="intitule-bloc intitule-150"><label for="add_retraitDossiers"><com:TTranslate>ADRESSE_RETRAIT_DOSSIERS</com:TTranslate></label><span class="champ-oblig">*</span> :</div>
							<com:TActiveTextBox TextMode="MultiLine" ID="retraitDossiers"  Attributes.OnChange="replaceCaracSpecial(this);" Attributes.title="<%=Prado::localize('ADRESSE_RETRAIT_DOSSIERS')%>" cssClass="long" />
					     		<com:TRequiredFieldValidator
											ID="validatorRetraitDossiersInfosCons"
											ControlToValidate="retraitDossiers"
											ValidationGroup="validateInfosConsultation"
											Display="Dynamic"
											Enabled="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('ConsultationAdresseRetraisDossiers') &&  Application\Service\Atexo\Atexo_Config::getParameter('AFFICHAGE_DONNEES_COMPLEMENTAIRES_PERSONNALISEE'))%>"
											ErrorMessage="<%=Prado::localize('ADRESSE_RETRAIT_DOSSIERS')%>"
											EnableClientScript="true"
								 			Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
											<prop:ClientSide.OnValidationError>
												document.getElementById('divValidationSummary').style.display='';
							                	document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
							                	document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
							                	document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
							                	document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
											</prop:ClientSide.OnValidationError>
					     		</com:TRequiredFieldValidator>
						</com:TActivePanel>
						<com:TActivePanel ID="panelAdresseDepot" cssClass="line" visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('ConsultationAdresseDepotOffres') &&  Application\Service\Atexo\Atexo_Config::getParameter('AFFICHAGE_DONNEES_COMPLEMENTAIRES_PERSONNALISEE')) ? 'true' : 'false' %>">
							<div class="intitule-bloc intitule-150"><label for="addDepotDossiers"><com:TTranslate>ADRESSE_DEPOT_BR_OFFRE</com:TTranslate></label><span class="champ-oblig">*</span> :</div>
							<com:TActiveTextBox TextMode="MultiLine" ID="addDepotDossiers"  Attributes.OnChange="replaceCaracSpecial(this);" Attributes.title="<%=Prado::localize('ADRESSE_DEPOT_OFFRE')%>" cssClass="long" />
					     		<com:TRequiredFieldValidator
											ID="validatorAddDepotDossiersInfosCons"
											ControlToValidate="addDepotDossiers"
											ValidationGroup="validateInfosConsultation"
											Display="Dynamic"
											Enabled="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('ConsultationAdresseDepotOffres') &&  Application\Service\Atexo\Atexo_Config::getParameter('AFFICHAGE_DONNEES_COMPLEMENTAIRES_PERSONNALISEE'))%>"
											ErrorMessage="<%=Prado::localize('ADRESSE_DEPOT_OFFRE')%>"
											EnableClientScript="true"
								 			Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
											<prop:ClientSide.OnValidationError>
												document.getElementById('divValidationSummary').style.display='';
								 				document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
							                	document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
							                	document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
							                	document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
											</prop:ClientSide.OnValidationError>
					     		</com:TRequiredFieldValidator>
					 	</com:TActivePanel>
					     <com:TActivePanel ID="panelOuverturePlis" cssClass="line" visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('ConsultationLieuOuverturePlis') &&  Application\Service\Atexo\Atexo_Config::getParameter('AFFICHAGE_DONNEES_COMPLEMENTAIRES_PERSONNALISEE')) ? 'true' : 'false' %>">
					    	<div class="intitule-bloc intitule-150"><label for="lieuOuverturePlis"><com:TTranslate>LIEU_OUVERTURE_PLIS</com:TTranslate></label><span <%=(Application\Service\Atexo\Atexo_Module::isEnabled('LieuOuverturePlisObligatoire')) ? "class='champ-oblig'" : "style='display:none'" %> >*</span> :</div>
					    	<com:TActiveTextBox TextMode="MultiLine" ID="lieuOuverturePlis"  Attributes.OnChange="replaceCaracSpecial(this);" Attributes.title="<%=Prado::localize('LIEU_OUVERTURE_PLIS')%>" cssClass="long" />
					    		<com:TRequiredFieldValidator
					    					ID="validatorLieuOuverturePlis"
					    					ControlToValidate="lieuOuverturePlis"
					    					ValidationGroup="validateInfosConsultation"
					    					Display="Dynamic"
					    					Enabled="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('ConsultationLieuOuverturePlis') &&  Application\Service\Atexo\Atexo_Config::getParameter('AFFICHAGE_DONNEES_COMPLEMENTAIRES_PERSONNALISEE') && Application\Service\Atexo\Atexo_Module::isEnabled('LieuOuverturePlisObligatoire'))%>"
					    					ErrorMessage="<%=Prado::localize('LIEU_OUVERTURE_PLIS')%>"
					    					EnableClientScript="true"
					    		 			Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
					    					<prop:ClientSide.OnValidationError>
					    							document.getElementById('divValidationSummary').style.display='';
					    		 					document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
					    		                	document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
					    		                	document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
					    		                	document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
					    					</prop:ClientSide.OnValidationError>
					     		</com:TRequiredFieldValidator>
					    </com:TActivePanel>
					    <com:TActivePanel id="panelLtrefText" >
						    <!-- Debut Referentiels zone de texte -->
								<com:AtexoReferentielZoneText id="idReferentielZoneText" consultation="1" obligatoire="1" validationGroup="validateInfosConsultation" validationSummary="divValidationSummary" idPage="InformationsDonneesComplementairesConsultation" cssClass="intitule-150" mode="1" Type="1"/>
							<!-- Fin Referentiels zone de texte -->
						</com:TActivePanel>
						<!--Debut Bloc Type prestation -->
					   <com:TActivePanel cssClass="line" ID="panelTypePrestation" visible="<%=Application\Service\Atexo\Atexo_Module::isEnabled('TypePrestation') && Application\Service\Atexo\Atexo_Module::isEnabled('DonneesComplementaires')%>">
								<div class="intitule-150"><label for="marcheType"><com:TTranslate>TEXT_TYPE_DE_PRESTATION</com:TTranslate></label> :</div>
								<div class="content-bloc bloc-600">
                                        <div class="line">
                                        	<com:TActiveRadioButton
												cssClass="radio"
												GroupName="typeprestation"
												ID="prestationCourante"
												Text="<%=Prado::localize('DEFINE_COURANTE')%>"
												Attributes.title="<%=Prado::localize('DEFINE_COURANTE')%>"/>
                                    	</div>
										<div class="line">
                                       		 <com:TActiveRadioButton
												cssClass="radio"
												GroupName="typeprestation"
												ID="prestationComplexe"
												Text="<%=Prado::localize('DEFINE_COMPLEXE')%>"
												Attributes.title="<%=Prado::localize('DEFINE_COMPLEXE')%>"/>
                                		</div>
                                	</div>
						</com:TActivePanel>
						<!--Fin Bloc Type prestation -->
						<!--Debut Bloc Delai Partiel -->
						<com:TActivePanel cssClass="line" ID="panelDelaiPartiel" visible="<%=Application\Service\Atexo\Atexo_Module::isEnabled('TypePrestation') && Application\Service\Atexo\Atexo_Module::isEnabled('DonneesComplementaires')%>">
							<div class="intitule-150"><label for="delaiPartiel"><com:TTranslate>TEXT_DELAI_PARTIEL</com:TTranslate></label> :</div>
							<div class="content-bloc bloc-600">
	                             <div class="line">
	                                      	<com:TActiveRadioButton
											cssClass="radio"
											GroupName="delaiPartiel"
											ID="delaiPartielOui"
											Text="<%=Prado::localize('DEFINE_OUI')%>"
											Attributes.title="<%=Prado::localize('DEFINE_OUI')%>"/>
	                       		</div>
						   		<div class="line">
	                        		<com:TActiveRadioButton
											cssClass="radio"
											GroupName="delaiPartiel"
											ID="delaiPartielNon"
											Text="<%=Prado::localize('DEFINE_NON')%>"
											Attributes.title="<%=Prado::localize('DEFINE_NON')%>"/>
	                       		</div>
	                        </div>
						</com:TActivePanel>
						<!--Fin Bloc Delai Partiel -->

					<com:TActivePanel ID="panelCautionProvisoire" visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('ConsultationCautionProvisoire')) ? 'true' : 'false' %>">
					<div Class="line">
						<div class="intitule-150"><label for="cautionProvisoire"><com:TTranslate>CAUTION_PROVISOIRE</com:TTranslate></label> :</div>
						<div class="content-bloc-auto">
							<com:TActiveTextBox ID="cautionProvisoire" cssClass="montant float-left" Attributes.title="<%=Prado::localize('CAUTION_PROVISOIRE')%>" />
							<div class="intitule-bloc"><com:TTranslate>DEFINE_UNITE</com:TTranslate></div>
				       				<com:TRegularExpressionValidator
				       					 ID="validatorRegularCautionProvisoire"
					                	 ValidationGroup="validateInfosConsultation"
				                    	 ControlToValidate="cautionProvisoire"
				                     	 RegularExpression="\d*[\,|\.]?\d*"
				                    	 ErrorMessage="<%=Prado::localize('CAUTION_PROVISOIRE')%>"
				                  		 Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
				                 		 <prop:ClientSide.OnValidationError>
					                            document.getElementById('divValidationSummary').style.display='';
							        			document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
							        			document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
							       	    		document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
							       	    		document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
					                    </prop:ClientSide.OnValidationError>
					            </com:TRegularExpressionValidator>
						</div>
					</div>
					<div class="spacer-small"></div>
				   </com:TActivePanel>
				   <com:TActivePanel ID="panelPrixAquisition" cssClass="line" visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('ConsultationPrixAcquisition')) ? 'true' : 'false' %>">
						<div class="intitule-150"><label for="prixAquisitaionPlans"><com:TTranslate>PRIX_ACQUISITION_DES_PLANS</com:TTranslate></label> :</div>
						<div class="content-bloc-auto">
							<com:TActiveTextBox ID="prixAquisitaionPlans" cssClass="montant float-left-fix" Attributes.title="<%=Prado::localize('PRIX_ACQUISITION_DES_PLANS')%>" />
							<div class="intitule-bloc"><com:TTranslate>DEFINE_UNITE</com:TTranslate></div>
				        		<com:TRegularExpressionValidator
				        				 ID="validatorRegularPrixAquisitaionPlans"
						                 ValidationGroup="validateInfosConsultation"
					                     ControlToValidate="prixAquisitaionPlans"
					                     RegularExpression="\d*[\,|\.]?\d*"
					                     ErrorMessage="<%=Prado::localize('PRIX_ACQUISITION_DES_PLANS')%>"
					                     Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
					                  <prop:ClientSide.OnValidationError>
					                            document.getElementById('divValidationSummary').style.display='';
								             	document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
								             	document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
								             	document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
								             	document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
						             </prop:ClientSide.OnValidationError>
						      </com:TRegularExpressionValidator>
						</div>
					</com:TActivePanel>

				<!-- Debut Domaines activites -->
                <com:TActivePanel ID="panelDomaineActiviteLtRef" visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('ConsultationDomainesActivitesLtReferentiel')) ? 'true' : 'false' %>">
					<div class="line">
              				<div class="intitule-150"><com:TTranslate>TEXT_DOMAINES_ACTIVITE</com:TTranslate>
              				<span class="champ-oblig">*</span> :</div>
						<com:AtexoReferentiel ID="idAtexoRefDomaineActivites" cas="cas5"/>
					</div>
                     <com:TRequiredFieldValidator
						Id="validatorDomaineActiviteRef"
						ControlToValidate="idAtexoRefDomaineActivites.codesRefSec"
						ValidationGroup="validateInfosConsultation"
						Display="Dynamic"
						ErrorMessage="<%=Prado::localize('TEXT_DOMAINES_ACTIVITE')%>"
						EnableClientScript="true"
			 			Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
						<prop:ClientSide.OnValidationError>
								document.getElementById('divValidationSummary').style.display='';
     				 			document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
			                	document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
			                	document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
			                	document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
   						</prop:ClientSide.OnValidationError>
      				</com:TRequiredFieldValidator>
				<div class="spacer-small"></div>
				</com:TActivePanel>

				<!-- Fin Domaine activite -->


				<!-- Debut Qualification -->

				<com:TActivePanel ID="panelQualification" cssClass="line">
					<div class="intitule-150"><com:TTranslate>TEXT_QUALIFICATIONS</com:TTranslate> :</div>
					<div class="content-bloc bloc-580">
						<com:AtexoQualification ID="qualification" />
					</div>
				</com:TActivePanel>

				<!--Fin Qualification -->

				<!--Debut Agrement -->

				<com:TActivePanel ID="panelAgrement">
					<div Class="line">
						<div class="intitule-150"><com:TTranslate>TEXT_AGREMENT</com:TTranslate> :</div>
						<div class="content-bloc bloc-580">
					    <com:PanelAgrements ID="agrements" />
						</div>
					</div>
					<div class="spacer-small"></div>
				</com:TActivePanel>

				<!--Fin Agrement -->
				<com:TActivePanel id="panelLtrefRadio">
					<!-- Debut Referentiels Radio -->
						<com:AtexoLtRefRadio id="idAtexoLtRefRadio" consultation="1" obligatoire="1" validationGroup="validateInfosConsultation" validationSummary="divValidationSummary" idPage="InformationsDonneesComplementairesConsultation" cssClass="intitule-155" mode="1" Type="2"/>
					<!-- Fin Referentiels Radio -->
					<div class='spacer-small'></div>
				</com:TActivePanel>

				<com:TActivePanel visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('ConsultationEchantillonsDemandes')) ? 'true' : 'false' %>">
				<!-- Début bloc échantillons demandés -->

					<com:EchantillonsDemandesConsultation id="echantillonsDemandes"/>
					<!-- Fin bloc échantillons demandés -->

					<div class="spacer-small"></div>
				</com:TActivePanel>

				<com:TActivePanel visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('ConsultationReunion')) ? 'true' : 'false' %>">
					<!-- Début bloc réunions -->
					<com:ReunionsConsultation id="reunions"/>
					<!-- Fin bloc réunions -->

					<div class="spacer-small"></div>
				</com:TActivePanel>
				<com:TActivePanel visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('ConsultationVisiteDesLieux')) ? 'true' : 'false' %>">
				<!-- Début bloc visites des lieux -->
				<com:VisitesLieux id="visitesLieux"/>
				<!-- Fin bloc visites des lieux -->

				<div class="spacer"></div>
				</com:TActivePanel>
				<com:TActivePanel ID="panelVariantesAutorisees" visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('ConsultationVariantesAutorisees') &&  Application\Service\Atexo\Atexo_Config::getParameter('AFFICHAGE_DONNEES_COMPLEMENTAIRES_PERSONNALISEE')) ? 'true' : 'false' %>">
					<div Class="line">
						<div class="intitule col-150"><label for="variantesAutorisees"><com:TTranslate>VARIANTES_AUTORISEES</com:TTranslate></label><span class="champ-oblig">*</span> :</div>
						<div class="content-bloc bloc-400">
							<div class="line">
								<com:TActiveRadioButton GroupName="variantes" ID="variantesNon" Attributes.title="<%=Prado::localize('TEXT_NON')%>" checked="true"/>
								<label for="variantesNon"><com:TTranslate>TEXT_NON</com:TTranslate></label>
							</div>
							<div class="line">
								<com:TActiveRadioButton GroupName="variantes"  ID="variantesOui"  Attributes.title="<%=Prado::localize('FCSP_OUI')%>"/>
								<label for="variantesOui"><com:TTranslate>FCSP_OUI</com:TTranslate></label>
							</div>
						</div>
						</div>
						<div class="spacer-small"></div>
				</com:TActivePanel>

				<com:TActivePanel visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('ConsultationPiecesDossiers')) ? 'true' : 'false' %>">
					<!--Debut Référentiel pièces des dossiers réponses-->
						<com:ReferentielPiecesDossierReponse ID="PiecesDossierReponse" ValidationGroup="validateInfosConsultation" validationSummary="divValidationSummary" />
					<!--Fin Référentiel pièces des dossiers réponses-->
				</com:TActivePanel>


					<!--Fin Bloc Delai Adresse depot / retrait / lieu d'ouverture de Pli -->
					<com:TActivePanel ID="panelDonneesRedac"
									  CssClass="panel-donnees-redac"
									  Visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('DonneesComplementaires'))%>">

							<div class="form-bloc margin-0"
								 style="display: none">
								<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
								<div class="content">
								<com:TActivePanel id="panelCheckDonneesRedac" cssclass="line">
									<div class="intitule-auto"><com:TTranslate>DEFINE_JE_SOUHAITE_ENRICHIR_DONNEES_CONSULTATION_AVEC_DONNES_REDAC</com:TTranslate> : </div>
									<div class="intitule-60 indent-20">
										<com:TActivePanel ID="paneldonneeOrmeOui">
										<com:TActiveRadioButton
											cssClass="radio"
											Attributes.onclick="enableTogglePanels(this,'donnees-redac');activerBlocsDonneesComplementaires();"
											GroupName="ChoixDonneeOrme"
											ID="donneeOrmeOui"
											Text="<%=Prado::localize('DEFINE_OUI')%>"
											Attributes.title="<%=Prado::localize('DEFINE_OUI')%>"
											onCallBack="enabledDonneesRedac"
										/>
										</com:TActivePanel>
									</div>
									<div class="intitule-auto">
										<com:TActivePanel ID="paneldonneeOrmeNon">
										<com:TActiveRadioButton
											cssClass="radio"
											Attributes.onclick="disableTogglePanels(this,'donnees-redac');desactiverBlocsDonneesComplementaires();"
											GroupName="ChoixDonneeOrme"
											ID="donneeOrmeNon"
											Text="<%=Prado::localize('DEFINE_NON')%>"
											Attributes.title="<%=Prado::localize('DEFINE_NON')%>"
											onCallBack="enabledDonneesRedac"
										/>
										</com:TActivePanel>
									</div>
								</com:TActivePanel>
								<div class="breaker"></div>
								<p><strong><com:TTranslate>DEFINE_REMARQUES</com:TTranslate> :</strong> <com:TTranslate>DEFINE_DONNES_OBLIGATOIRE_POUR_UTILISATION_MODULAE_REDAC</com:TTranslate></p>
							</div>
								<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
							</div>


						<com:TActivePanel id="panelDonneesRedacEnabled" ></com:TActivePanel>
							<!--Debut Bloc Forme juridique du groupement attributaire-->
							<com:TConditional condition="Application\Service\Atexo\Atexo_Module::isEnabled('DonneesRedac')">
								<prop:trueTemplate>
									<com:DonneesComplementairesFormeJuridiqueGroupementAttributaire id="donneeFormeGroupement" />
								</prop:trueTemplate>
							</com:TConditional>
							<!--Fin Bloc Forme juridique du groupement attributaire-->
							<!--Debut Bloc Duree-->
							<com:TConditional condition="Application\Service\Atexo\Atexo_Module::isEnabled('DonneesRedac')">
								<prop:trueTemplate>
									<com:DonneesComplementairesDureeMarche id="donneeDureeMarche" sourcePage="etapeIdentification" mode="1"/>
								</prop:trueTemplate>
							</com:TConditional>
							<!--Fin Bloc Duree-->
							<!--Debut Bloc Procedure-->
							<com:TConditional condition="Application\Service\Atexo\Atexo_Module::isEnabled('DonneesRedac')">
								<prop:trueTemplate>
									<com:DonneesComplementairesProcedure id="donneeProcedure" />
								</prop:trueTemplate>
							</com:TConditional>
							<!--Fin Bloc Procedure-->
							<!--Debut Bloc conditions participations-->
							<com:TActivePanel ID="panel_active_conditions_participation" cssclass="toggle-panel form-toggle donnees-redac" visible="<%= $this->getConsultation()->getDonneePubliciteObligatoire() && !Application\Service\Atexo\Atexo_Config::getParameter('SPECIFIQUE_PLACE') && Application\Service\Atexo\Atexo_Module::isEnabled('ConfPubliciteFrancaise') %>">
							    <div class="content">
							        <div class="cover"></div>
							        <com:THyperLink ID="linkTogglePanel" NavigateUrl="javascript:void(0);" Attributes.onClick="togglePanel(this,'panel-conditions-participation');" CssClass="title-toggle" >
                                        <com:TTranslate>CONDITIONS_PARTICIPATION</com:TTranslate>
                                    </com:THyperLink>
                                    <div class="panel no-indent panel-donnees-complementaires-conditions-participation" id="panel-conditions-participation" style="display:block;">
                                        <div class="line">
                                            <div class="intitule col-300"><label for="activiteProfessionel"><com:TTranslate>DEFINE_ACTIVITE_PROFESSIONEL</com:TTranslate></label><span class="champ-oblig">*</span> :</div>
                                            <com:TTextBox id="activiteProfessionel"
                                                 Attributes.title="<%=Prado::Localize('DEFINE_ACTIVITE_PROFESSIONEL')%>"
                                                 attributes.minlength="5"
                                        	     TextMode="MultiLine"
                                        		 cssClass="long"
                                        	>
                                        	</com:TTextBox>
                                        	<com:TCustomValidator
                                            	ValidationGroup="validateInfosConsultation"
                                            	ControlToValidate="activiteProfessionel"
                                            	Enabled="<%= $this->getConsultation()->getDonneePubliciteObligatoire() %>"
                                            	ClientValidationFunction="validerConditionsParticipation"
                                            	Display="Dynamic"
                                            	Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span> "
                                            	ErrorMessage="<%=Prado::localize('TEXT_ERREUR_CONDITIONS_PARTICIPATION')%>"
                                            	EnableClientScript="true">
                                            	<prop:ClientSide.OnValidationError>
                                            			document.getElementById('divValidationSummary').style.display='';
                                             			document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
                                                          	document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
                                                          	document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
                                                          	document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
                                               	</prop:ClientSide.OnValidationError>
                                            </com:TCustomValidator>
                                        </div>
                                        <div class="line">
                                            <div class="intitule col-300">
												<label for="economiqueFinanciere">
													<com:TTranslate>DEFINE_ECONOMIQUE_FINANCIERE</com:TTranslate>
												</label>
												<span class="champ-oblig">*</span> :
											</div>
                                            <com:TTextBox id="economiqueFinanciere"
                                                 Attributes.title="<%=Prado::Localize('DEFINE_ECONOMIQUE_FINANCIERE')%>"
                                                 attributes.minlength="5"
                                                 TextMode="MultiLine"
                                                 cssClass="long"
                                            >

                                            </com:TTextBox>
                                        </div>
                                        <div class="line">
                                            <div class="intitule col-300">
												<label for="techniquesProfessionels">
													<com:TTranslate>DEFINE_TECHNIQUES_PROFESSIONELS</com:TTranslate>
												</label>
												<span class="champ-oblig">*</span> :
											</div>
                                            <com:TTextBox id="techniquesProfessionels"
                                                Attributes.title="<%=Prado::Localize('DEFINE_TECHNIQUES_PROFESSIONELS')%>"
                                                attributes.minlength="5"
                                                TextMode="MultiLine"
                                                cssClass="long"
                                            >
                                            </com:TTextBox>
                                        </div>
                                    </div>
							    </div>
							</com:TActivePanel>
							<!--Fin Bloc conditions participations-->
							<!--Debut Bloc Criteres attribution-->
							<com:TConditional condition="Application\Service\Atexo\Atexo_Module::isEnabled('DonneesRedac')">
								<prop:trueTemplate>
									<com:DonneesComplementaireCriteresAttributions id="donneeCriteresAttribution" mode="1"  Composant="donneesComplementaires" />
								</prop:trueTemplate>
							</com:TConditional>
							<!--Fin Bloc Criteres attribution-->
							<!--Debut Bloc CCAG - Variantes-->
							<com:TConditional condition="Application\Service\Atexo\Atexo_Module::isEnabled('DonneesRedac')">
								<prop:trueTemplate>
									<com:DonneesComplementaireVariantes id="donneeVariante" mode="1" />
								</prop:trueTemplate>
							</com:TConditional>
							<!--Fin Bloc CCAG - Variantes-->

						<com:TPanel visible="<%=Application\Service\Atexo\Atexo_Module::isEnabled('Publicite') && Application\Service\Atexo\Atexo_Module::isEnabled('AfficherValeurEstimee')%>">
								<com:DonneesComplementairesMontantMarche id="donneeMontantMarche" sourcePage="etapeIdentification" mode="1"/>
						</com:TPanel>
							<!--Debut Bloc Forme du marche et forme(s) de prix-->
							<com:TConditional condition="Application\Service\Atexo\Atexo_Module::isEnabled('DonneesRedac')">
								<prop:trueTemplate>
									<com:DonneesComplementaireFormeMarcheFormePrix ID="donneeFormePrix" mode="1" sourcePage="etapeIdentification" ValidationGroup="validateInfosConsultation" validationSummary="divValidationSummary" />
								</prop:trueTemplate>
							</com:TConditional>
					</com:TActivePanel>
					<com:TActivePanel cssclass="form-field" id="informationRegimePub" visible="<%=Application\Service\Atexo\Atexo_Module::isEnabled('Publicite') && Application\Service\Atexo\Atexo_Module::isEnabled('ConfPubliciteFrancaise') && $this->checkIfPubActiveNiveauConsultation() && !Application\Service\Atexo\Atexo_Config::getParameter('SPECIFIQUE_PLACE')%>">
						<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
						<div class="content">
							<h3 class="float-left"><com:TTranslate>INFORMATIONS_FACTORISEES_DANS_FAVORIS</com:TTranslate></h3>
							<div class="line">
								<div class="intitule col-50"><label for="favoris"><com:TTranslate>FAVORIS</com:TTranslate></label> :</div>
								<div class="content-bloc">

									<com:TActiveDropDownList id="favorisPublicite" AutoPostBack="true" onCallBack="preRemplirInfosDepuisFavoris" cssClass="moyen" Attributes.title="<%=Prado::localize('FAVORIS')%>" >
										<prop:ClientSide.OnLoading>showLoader();</prop:ClientSide.OnLoading>
										<prop:ClientSide.OnFailure>showLoader();</prop:ClientSide.OnFailure>
										<prop:ClientSide.OnSuccess>showLoader();</prop:ClientSide.OnSuccess>
										<prop:ClientSide.RequestTimeOut>60000</prop:ClientSide.RequestTimeOut>
									</com:TActiveDropDownList>
									<com:TActiveTextBox id="nomFavorisPublicite" cssClass="moyen" />

								</div>
								<div class="content-bloc bloc-320 float-right">
									<com:TActiveCheckBox cssClass="check" Attributes.title="<%=Prado::localize('TEXT_ENREGISTRER_INFFORMATIONS_SAISIES_SUR_LE_COMPTE_PUBLICITE_SELECTIONNE')%>" id="enregistrementFavoris" />
									<label for="enregistrementInfosCompteAdresse1">Mettre à jour en favoris les informations saisies ci-dessous</label>
									<div class="info-aide">(Régime financier, Adresses et correspondants)</div>
								</div>
							</div>
							<div class="spacer-small"></div>
							<!--Fin Bloc Forme du marche et forme(s) de prix-->
							<com:TConditional condition="Application\Service\Atexo\Atexo_Module::isEnabled('DonneesRedac')">
								<prop:trueTemplate>
									<!--Debut Bloc Régime financier -->
									<com:DonneesComplementairesRegimeFinancier id="regimeFinancier"/>
									<!--Fin Bloc Régime financier -->
								</prop:trueTemplate>
							</com:TConditional>
							<!--Debut Bloc Donnees du compte publicite-->
							<com:TConditional condition="Application\Service\Atexo\Atexo_Module::isEnabled('Publicite') && !(Application\Service\Atexo\Atexo_Config::getParameter('SPECIFIQUE_PLACE'))">
								<prop:trueTemplate>
									<com:DonneesComplementairesComptePublicite ID="donneesComplementairesComptePublicite" sourcePage="etapeDonneesComplementaires" validationGroupBtnValidate="validateInfosConsultation" ValidationSummaryBtnValidate="divValidationSummary"/>
								</prop:trueTemplate>
							</com:TConditional>
							<!--Fin Bloc Donnees du compte publicite-->
						</div>
						<!--Fin Ligne Date-->
						<div class="breaker"></div>
						<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
					</com:TActivePanel>
					<div class="breaker"></div>

				</div>
				<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
			</com:TActivePanel>
			<com:TActiveHiddenField id="formPrix" />
			<com:TCustomValidator
				ValidationGroup="validateInfosConsultation"
				ControlToValidate="formPrix"
				Enabled="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('DonneesComplementaires') && Application\Service\Atexo\Atexo_Module::isEnabled('DonneesRedac'))%>"
				ClientValidationFunction="validerFormPrix"
				Display="Dynamic"
				Text=" "
				ErrorMessage="<%=Prado::localize('TEXT_FORME_DE_PRIX')%>"
				EnableClientScript="true">
				<prop:ClientSide.OnValidationError>
						document.getElementById('divValidationSummary').style.display='';
		 				document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
		               	document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
		               	document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
		               	document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
		   		</prop:ClientSide.OnValidationError>
			</com:TCustomValidator>
			<!--Fin Bloc formulaire-->