<com:TPanel ID="panelStatutCompteEntreprise">
    <div class="section-heading page-header">
        <h3 class="h4 panel-title">
            <com:TTranslate>DEFINE_PRECISION_STATUT_ENTREPRISE</com:TTranslate>
        </h3>
    </div>
        <div class="checkbox">
            <label for="ctl0_CONTENU_PAGE_panelStatutEntreprise_entrepriseEA">
                <com:TCheckBox id="entrepriseEA"
                               Attributes.title="><%=Prado::localize('DEFINE_MON_ENTREPRISE_EA')%>" cssclass="check"/>
                <com:TTranslate>DEFINE_MON_ENTREPRISE_EA_LABEL</com:TTranslate>
            </label>
        </div>
        <div class="checkbox">
            <label for="ctl0_CONTENU_PAGE_panelStatutEntreprise_entrepriseSIAE">
                <com:TCheckBox id="entrepriseSIAE"
                               Attributes.title="><%=Prado::localize('DEFINE_MON_ENTREPRISE_SIAE')%>" cssclass="check"/>
                <com:TTranslate>DEFINE_MON_ENTREPRISE_SIAE_LABEL</com:TTranslate>
            </label>
        </div>
</com:TPanel>