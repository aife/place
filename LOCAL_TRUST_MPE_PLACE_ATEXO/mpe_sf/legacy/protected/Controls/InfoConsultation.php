<?php

namespace Application\Controls;

use Application\Propel\Mpe\CommonConsultation;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Category;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureType;
use Application\Service\Atexo\Consultation\Atexo_Consultation_TypeAvis;

class InfoConsultation extends MpeTPage
{
    private $reference;

    public function getReference()
    {
        return $this->reference;
    }

    public function setReference($reference)
    {
        return $this->reference = $reference;
    }

    public function getConsultation()
    {
        return $this->consultation;
    }

    public function setConsultation($consultation)
    {
        return $this->consultation = $consultation;
    }

    public function onLoad($param)
    {
        $org = Atexo_CurrentUser::getCurrentOrganism();
        if (!($this->getConsultation() instanceof CommonConsultation)) {
            $reference = 0;

            if (isset($_GET['id'])) {
                $reference = Atexo_Util::atexoHtmlEntities($_GET['id']);
            }
            $criteria = new Atexo_Consultation_CriteriaVo();
            $criteria->setcalledFromPortail(false);
            $criteria->setIdService(Atexo_CurrentUser::getCurrentServiceId());
            $criteria->setAcronymeOrganisme($org);
            $criteria->setIdReference($reference);
            $criteria->setConnectedAgentId(Atexo_CurrentUser::getId());
            $consultation = (new Atexo_Consultation())->search($criteria);

            if (1 == count($consultation) && $consultation[0] instanceof CommonConsultation) {
                $consultation = array_shift($consultation);
            }
        } else {
            $consultation = $this->getConsultation();
        }
        if ($consultation instanceof CommonConsultation) {
            $organismeO = Atexo_Organismes::retrieveOrganismeByAcronyme($org);
            $this->refConsultation->Text = $consultation->getReferenceUtilisateur();
            $this->objetConsultation->Text = $consultation->getObjet();
            $this->entitePublique->Text = $organismeO->getSigle().' - '.$organismeO->getDenominationOrg();
            $this->entiteAchat->Text = $organismeO->getSigle().' - '.$organismeO->getDenominationOrg();
            $typeAvis = (new Atexo_Consultation_TypeAvis())->retrieveTypeAnnonce($consultation->getIdTypeAvis());
            if ($typeAvis) {
                $this->typeAnnonce->Text = $typeAvis->getIntituleAvis();
            }
            $this->typeProcedure->Text = (new Atexo_Consultation_ProcedureType())->retrieveTypeProcedure($consultation->getIdTypeProcedure(), false);
            $this->intituleConsultation->Text = $consultation->getIntitule();
            $this->objetPrincipal->Text = $consultation->getObjet();
            $this->objetConsultation->Text = $consultation->getObjet();
            $this->categorieConsultation->Text = Atexo_Consultation_Category::retrieveLibelleCategorie($consultation->getCategorie(), false);
            $this->dateHeureLimiteRemisePlis->Text = Atexo_Util::iso2frnDateTime($consultation->getDatefin());
        }
    }
}
