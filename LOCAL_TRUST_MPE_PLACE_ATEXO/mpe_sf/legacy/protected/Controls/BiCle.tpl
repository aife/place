<com:TPanel ID="panelBiCleSecours" Visible="true">
<div class="line">
	<div class="intitule-auto bold"><com:TLabel ID="texteSecours"/></div>
</div>
 
		<div class="table-bloc bloc-760">
		<com:TRepeater ID="biCleSecours" EnableViewState="true">
   			<prop:HeaderTemplate>
				<table class="table-results" summary="<%=Prado::localize('LISTE_BI_CLE_PERSONNELS')%>">
					<caption><com:TTranslate>LISTE_BI_CLE_PERSONNELS</com:TTranslate></caption>
					<thead>
						<tr>
							<th class="top" colspan="4"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></th>
						</tr>
						<tr>
							<th class="col-300" id="nomBiCle"><com:TTranslate>NOM_BI_CLE</com:TTranslate></th>
							<th class="col-250" id="champCN"><com:TTranslate>CHAMP_CN</com:TTranslate></th>
                            <th class="col-120" id="dateEnd"><com:TTranslate>DATE_EXPIRATION</com:TTranslate></th>
						</tr>
					</thead>
			</prop:HeaderTemplate>
			<prop:ItemTemplate>
				<tr>
					<td class="col-300" headers="nomBiCle"><%#$this->Data->getNom()%></td>
					<td class="col-250" headers="champCN"><%#$this->Data->getTitre()%></td>
                    <td class="col-120" headers="dateEnd"><%#$this->TemplateControl->getDateExpiration($this->Data->getCertificat())%></td>
				</tr>
			</prop:ItemTemplate>
			<prop:AlternatingItemTemplate>
				<tr class="on">
					<td class="col-300" headers="nomBiCle"><%#$this->Data->getNom()%></td>
					<td class="col-250" headers="champCN"><%#$this->Data->getTitre()%></td>
                    <td class="col-120" headers="dateEnd"><%#$this->TemplateControl->getDateExpiration($this->Data->getCertificat())%></td>
				</tr>
			</prop:AlternatingItemTemplate>
			<prop:FooterTemplate>
				</table>
			</prop:FooterTemplate>
		</com:TRepeater>
		</div>
<!--Fin Bloc Gestion des bi-cles de secours-->
</com:TPanel>