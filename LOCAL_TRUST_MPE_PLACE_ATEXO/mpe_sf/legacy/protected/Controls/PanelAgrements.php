<?php

namespace Application\Controls;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonCategorieLot;
use Application\Propel\Mpe\CommonConsultation;
use Application\Service\Atexo\Atexo_Agrement;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;

/**
 * Panel Agréments.
 *
 * @author Mounir LAMANE <mounir.lamane@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PanelAgrements extends MpeTPage
{
    private string $_postBack = '';
    private bool|string $_afficher = true;
    private $_objet;
    private $_connexion;
    private $_callFrom;

    public function setIdsSelectedAgrements($value)
    {
        $this->idsSelectedAgrements->Value = $value;
    }

    public function setPostBack($value)
    {
        $this->_postBack = $value;
    }

    public function getPostBack()
    {
        return $this->_postBack;
    }

    /**
     * recupère la consultation.
     */
    public function getObjet()
    {
        return $this->_objet;
    }

    // getObjet()

    /**
     * Affecte consultation.
     *
     * @param objet
     */
    public function setObjet($objet)
    {
        if ($this->_objet !== $objet) {
            $this->_objet = $objet;
        }
    }

    // setObjet)

    /**
     * recupère la valeur de afficher.
     */
    public function getAfficher()
    {
        return $this->_afficher;
    }

    // getAfficher()

    /**
     * Affecte la valeur de afficher.
     *
     * @param string $afficher
     */
    public function setAfficher($afficher)
    {
        if ($this->_afficher !== $afficher) {
            $this->_afficher = $afficher;
        }
    }

    // setAfficher()

    /**
     * recupère la valeur de connexion.
     */
    public function getConnexion()
    {
        return $this->_connexion;
    }

    // getConnexion()

    /**
     * Affecte la connexion.
     *
     * @param string connexion
     */
    public function setConnexion($connexion)
    {
        $this->_connexion = $connexion;
    }

    // setConnexion()

    /**
     * recupère CallFrom.
     */
    public function getCallFrom()
    {
        return $this->_callFrom;
    }

    // getConnexion()

    /**
     * Affecte la CallFrom.
     *
     * @param string CallFrom
     */
    public function setCallFrom($callFrom)
    {
        $this->_callFrom = $callFrom;
    }

    // setConnexion()

    public function onLoad($param)
    {
        $this->chargerComposant();
        if (!$this->getPostBack()) {
            $this->displaySelectedAgrements();
        }
    }

    /**
     * affiche les dénominations des Agrements.
     */
    public function displaySelectedAgrements()
    {
        $agrement = null;
        $ids = $this->idsSelectedAgrements->Value;
        $arIds = explode(',', $ids);
        $list = Atexo_Agrement::retrieveAgrementById($arIds);
        $langue = Atexo_CurrentUser::readFromSession('lang');
        $libelles = '';
        if ($list) {
            foreach ($list as $oneAgrement) {
                $getLibelleTraduit = 'getLibelle'.Atexo_Languages::getLanguageAbbreviation($langue);
                if (0 == strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE'))
                     || !$oneAgrement->$getLibelleTraduit()) {
                    $agrement .= $oneAgrement->getLibelle().', ';
                } else {
                    $agrement .= $oneAgrement->$getLibelleTraduit().', ';
                }
            }
            $libelles = substr(trim($agrement), 0, -1);
        }

        if (strlen($libelles) > 100) {
            $this->toisPoints->setDisplay('Dynamic');
            $this->labelleAgrement->Text = Atexo_Util::truncateTexte(wordwrap(($libelles), 400, '<br />', true), 100);
            $this->libellesInfoBulleAgrement->Text = wordwrap(($libelles), 400, '<br />', true);
        } else {
            $this->toisPoints->setDisplay('None');
            $this->labelleAgrement->Text = $libelles;
            $this->libellesInfoBulleAgrement->Text = $libelles;
        }
    }

    /**
     * Permet de sauvegarder les données complémentaires.
     *
     * @param Objet lot ou consultation
     */
    public function save(&$objet, $saveBb = false, $modification = true)
    {
        if (Atexo_Module::isEnabled('ConsultationAgrement')) {
            $agrement = '';
            $agrementOld = $objet->getAgrements();
            if ($this->getAfficher()) {
                $agrement = Atexo_Util::replaceSpecialCharacters($this->idsSelectedAgrements->Value);
            }
            // historiques
            if (!$this->_connexion) {
                $this->_connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            }
            $objet->setAgrements($agrement);
            $this->saveHistoriqueAgrements($agrementOld, $objet, $this->_connexion, Atexo_CurrentUser::getCurrentOrganism(), $modification);
            if ($saveBb) {
                $objet->save($this->_connexion);
            }
        }
    }

    /**
     * Permet de sauvegarder l'historique d'agrement.
     *
     * @param Objet $consultation: Objet consultation
     * @param $connexion: Objet connexion
     * @param string $org l'acronyme de l'organisme
     */
    public function saveHistoriqueAgrements($agrementOld, $objet, $connexion, $org, $modification = true)
    {
        $arrayParameters = [];
        if (Atexo_Module::isEnabled('ConsultationAgrement') &&
            (trim($agrementOld) != trim($objet->getAgrements()) || !$modification)
            ) {
            $nomElement = Atexo_Config::getParameter('ID_HISTORIQUE_AGREMENT');
            $valeur = '0';
            $detail2 = '';
            $detail1 = '';
            $consultationId = '';
            $lot = '0';
            if ($objet instanceof CommonCategorieLot) {
                $lot = $objet->getLot();
            }
            if ($objet->getAgrements()) {
                $detail1 = $objet->getAgrements();
                $valeur = '1';
            }
            if ($objet instanceof CommonConsultation) {
                $consultationId = $objet->getConsultationId();
                $consultation = $objet;
            } elseif ($objet instanceof CommonCategorieLot) {
                $consultation = (new Atexo_Consultation())->retrieveConsultation($objet->getConsultationId(), $org);
                $consultationId = $objet->getConsultationId();
            }
            if ($modification) {
                $statut = Atexo_Config::getParameter('MODIFICATION_FILE');
            } else {
                $statut = Atexo_Config::getParameter('CREATION_FILE');
            }

            $arrayParameters[0] = $consultationId;
            $arrayParameters[1] = $nomElement;
            $arrayParameters[2] = $valeur;
            $arrayParameters[3] = $detail1;
            $arrayParameters[4] = $detail2;
            $arrayParameters[5] = $lot;
            $arrayParameters[6] = $org;
            $arrayParameters[7] = $statut;
            (new Atexo_Consultation())->saveHistoriqueConsultation($arrayParameters, $connexion);
        }
    }

    /**
     * Permet de préciser si la modification est permise.
     *
     * @param $consultation: objet consultation
     * Si Oui retourne true, sinon false
     */
    public function isModificationAllowed($consultation)
    {
        return (new Atexo_Consultation())->isModificationAllowed($consultation, isset($_GET['continuation']), $consultation->getId());
    }

    public function chargerComposant($recharger = false)
    {
        $this->customize();
        if (!$this->Page->isPostBack) {
            if ($this->getObjet()) {
                if ($this->getObjet()->getAgrements()) {
                    $this->idsSelectedAgrements->Value = $this->getObjet()->getAgrements();
                    //base64_encode($this->getObjet()->getAgrements());
                }
            }
        }
        if (Atexo_Module::isEnabled('ConsultationAgrement') && $this->getAfficher()) {
            $this->afficherComposant();
        } else {
            $this->masquerComposant();
        }
    }

    /**
     * Permet de gerer la visibilité.
     */
    public function customize()
    {
        if (Atexo_Module::isEnabled('ConsultationAgrement')) {
            $this->panelAgrement->visible = true;
            $this->setPostBack($this->IsPostBack);
        } else {
            $this->panelAgrement->visible = false;
        }
    }

    /**
     * Permet d'afficher les échantillons demandés.
     */
    public function afficherComposant()
    {
        $this->panelAgrement->setDisplay('Dynamic');
        $this->setPostBack($this->IsPostBack);
    }

    /**
     * Permet de masquer les échantillons demandés.
     */
    public function masquerComposant()
    {
        $this->panelAgrement->setDisplay('None');
    }

    public function refresh($sender, $param)
    {
        $this->panelAgrement->render($param->NewWriter);
    }

    /*
        /* public function onCallBackAgrement($sender,$param)
        {
            $this->panelAgrements->render($param->NewWriter);
        }*/
}
