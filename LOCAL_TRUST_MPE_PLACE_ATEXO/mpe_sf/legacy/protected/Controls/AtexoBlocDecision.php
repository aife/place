<?php

namespace Application\Controls;

use App\Service\AgentService;
use App\Service\CurrentUser;
use App\Service\WebServices\WebServicesExec;
use App\Utils\Encryption;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Pages\Commun\Classes\Util;
use Application\Pages\Commun\DownloadFile;
use Application\Propel\Mpe\CommonCategorieLot;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonDecisionLot;
use Application\Propel\Mpe\CommonTContratMulti;
use Application\Propel\Mpe\CommonTContratTitulaire;
use Application\Propel\Mpe\CommonTContratTitulaireQuery;
use Application\Propel\Mpe\CommonTDecisionSelectionEntrepriseQuery;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_MultiDomaine;
use Application\Service\Atexo\Atexo_TypeContrat;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Contrat;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Decision;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Lots;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureType;
use Application\Service\Atexo\Contrat\Atexo_Contrat_IndicateurEnum;
use Application\Service\Atexo\GenerationFichier\Atexo_GenerationFichier_ImpressionODT;
use AtexoPdf\DocGeneratorClient;
use PDO;
use Prado\Prado;
use Prado\Web\UI\ActiveControls\TActiveDropDownList;
use Prado\Web\UI\ActiveControls\TCallbackEventParameter;

/**
 * Le bloc de décision.
 *
 * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
 *
 * @version 1.0
 *
 * @since 4.11.0
 *
 * @copyright Atexo 2015
 */
class AtexoBlocDecision extends MpeTTemplateControl
{
    public const TYPE_PROCEDURE_SAD = "SAD-A";

    protected $consultation;
    public const TYPE_PAGE = 1;
    public const TYPE_POPUP = 2;
    public const TYPE_POPIN = 3;
    protected $_lotsExclusAndContratGroupees;

    /**
     * Get the value of [consultation] column.
     *
     * @return CommonConsultation consultation
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function getConsultation()
    {
        if (!$this->consultation instanceof CommonConsultation) {
            return $this->getViewState('consultation');
        }

        return $this->consultation;
    }

    /**
     * Set the value of [consultation] column.
     *
     * @param string $v new value
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function setConsultation($consultation)
    {
        $this->consultation = $consultation;
        $this->setViewState('consultation', $consultation);
    }

    /**
     * Get the value of dataSource.
     *
     * @return array datasource
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function getDataSource()
    {
        return $this->getViewState('dataSourceDecisionInfo');
    }

    /**
     * Set the value of dataSource.
     *
     * @param array $data new value
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function setDataSource($data)
    {
        $this->setViewState('dataSourceDecisionInfo', $data);
    }

    /**
     * Permet d'initialiser le composant.
     *
     * @param void
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function initaliserComposant()
    {
        $this->javascriptLabel->Text = '';
        $consultation = $this->getConsultation();
        if ($consultation instanceof CommonConsultation) {
            if ($this->isTypeProcedureSad()) {
                $this->fillListeLotsSad();
            } else {
                $this->lotsSad->visible = false;
            }

            $this->fillActionsGroupees();
            $this->fillRepeaterDecision($consultation);
        }
    }

    /**
     * Pemet de remplir le repeater de decision.
     *
     * @param CommonConsultation $consultation la consultation
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function fillRepeaterDecision($consultation)
    {
        $dataSource = [];
        $nombreElement = 0;
        if ($consultation instanceof CommonConsultation) {
            $consultationId = $consultation->getId();
            $organisme = $consultation->getOrganisme();

            if ($consultation->isAllotie()) {
                $this->_lotsExclusAndContratGroupees = $this->getLotExclusAndContratsGroupees();
                $this->setViewState('lotsExclusAndContratGroupees', $this->_lotsExclusAndContratGroupees);
                $lots = (new Atexo_Consultation_Lots())->retrieveListeLotsByArray($consultation->getId(), $consultation->getOrganisme(), $this->_lotsExclusAndContratGroupees['lotExclus'], Criteria::NOT_IN);
                if (is_array($lots)) {
                    $nombreElement = count($lots);
                }
            } else {
                $nombreElement = 1;
            }
        }
        $this->nombreResultat->Text = $nombreElement;
        $this->nombreResultatRepeater->Value = $nombreElement;
        $this->setViewState('nbrElements', $nombreElement);
        if ($nombreElement) {
            $this->nombrePageTop->Text = ceil($nombreElement / $this->repraterDecision->PageSize);
            $this->nombrePageBottom->Text = ceil($nombreElement / $this->repraterDecision->PageSize);
            $this->repraterDecision->setVirtualItemCount($nombreElement);
            $this->repraterDecision->setCurrentPageIndex(0);
            self::showComponent();
            self::populateData();
        } else {
            $this->repraterDecision->DataSource = [];
            $this->repraterDecision->DataBind();
            self::hideComponent();
        }
    }

    /**
     * Pemet de charger le repeater de decision.
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function populateData()
    {
        $offset = $this->repraterDecision->CurrentPageIndex * $this->repraterDecision->PageSize;
        $limit = $this->repraterDecision->PageSize;
        if ($offset + $limit > $this->repraterDecision->getVirtualItemCount()) {
            $limit = $this->repraterDecision->getVirtualItemCount() - $offset;
        }
        $dataSource = self::getDataSourceRepeater($limit, $offset);
        if (is_countable($dataSource) ? count($dataSource) : 0) {
            $this->showComponent();
            $this->repraterDecision->DataSource = $dataSource;
            $this->repraterDecision->DataBind();
        } else {
            $this->hideComponent();
        }
    }

    public function getDataSourceRepeater($limit, $offset)
    {
        $dataSource = [];
        $consultation = $this->getConsultation();
        if ($consultation instanceof CommonConsultation) {
            $consultationId = $consultation->getId();
            $organisme = $consultation->getOrganisme();
            if ($consultation->isAllotie()) {
                $this->_lotsExclusAndContratGroupees = $this->getViewState('lotsExclusAndContratGroupees', []);

                if ($this->isTypeProcedureSad()) {
                    $lots = (new Atexo_Consultation_Lots())->retrieveListeLotsByArray(
                        $consultationId,
                        $organisme,
                        [$this->lotsSad->getSelectedValue()]
                    );
                } else {
                    $lots = (new Atexo_Consultation_Lots())->retrieveListeLotsByArray(
                        $consultationId,
                        $organisme,
                        $this->_lotsExclusAndContratGroupees['lotExclus'],
                        Criteria::NOT_IN,
                        $limit,
                        $offset
                    );
                }

                if (is_array($lots)) {
                    if (!empty(Atexo_Config::getParameter('ACTIVE_EXEC_V2'))) {
                        $execWS = Atexo_Util::getSfService(WebServicesExec::class);
                        $query = [
                            "page"                      => 0,
                            "size"                      => 5000,
                            "idExternesConsultation"    => $consultationId
                        ];

                        $response = $execWS->getContent('getContrats', $query);
                        $contratsFromExec = $response['content'] ?? [];
                    }
                    foreach ($lots as $lot) {
                        if ($lot instanceof CommonCategorieLot) {
                            $listeContrats = (new Atexo_Consultation_Contrat())->getListeContrats($consultationId, $organisme, $lot->getLot());
                            if (!empty(Atexo_Config::getParameter('ACTIVE_EXEC_V2'))) {
                                $contratFromExecFound = false;
                                foreach ($contratsFromExec as $contratFromExec) {
                                    if (!empty($contratFromExec['numeroLot']) && $contratFromExec['numeroLot'] == $lot->getLot()) {
                                        $contratFromExecFound = true;
                                        if (!empty($contratFromExec['type']['codeExterne'])) {
                                            $firstTypeContrat = (new Atexo_TypeContrat())->retrieveTypeContratByLibelle('TYPE_CONTRAT_' . $contratFromExec['type']['codeExterne']);
                                            $isContratExecMulti = $firstTypeContrat?->getMulti() == '1';
                                        }
                                    }
                                }

                                if (!$contratFromExecFound) {
                                    $decisionLot = (new Atexo_Consultation_Decision())
                                        ->retrieveDecisionLotByReferenceAndLot($consultationId, $lot->getLot(), $organisme);
                                    $listeAllowedTypeDecision = [
                                        Atexo_Config::getParameter('DECISION_ATTRIBUTION_MARCHE'),
                                        Atexo_Config::getParameter('DECISION_ADMISSION_SAD'),
                                        Atexo_Config::getParameter('DECISION_ATTRIBUTION_MEME_FOURNISSEUR')
                                    ];
                                    if (
                                        !empty($decisionLot)
                                        &&
                                        in_array($decisionLot->getIdTypeDecision(), $listeAllowedTypeDecision)
                                    ) {
                                        $decisionLot->delete();
                                    }
                                }
                                $listeContrats = [];
                            }

                            $oneContrat = is_array($listeContrats) && count($listeContrats) ? $listeContrats[0] : '';
                            $contratMulti = '';
                            if ($oneContrat instanceof CommonTContratTitulaire) {
                                $contratMulti = $oneContrat->getCommonTContratMulti();
                            }
                            $decisionLot = (new Atexo_Consultation_Decision())->retrieveDecisionLotByReferenceAndLot($consultationId, $lot->getLot(), $organisme);
                            $decisionEntrepriseQuery = new CommonTDecisionSelectionEntrepriseQuery();
                            $listeSelectionEntreprise = (array) $decisionEntrepriseQuery->retrieveDecisionSelectionEntreprise($consultationId, $lot->getLot(), $organisme);
                            if (isset($this->_lotsExclusAndContratGroupees['lotsGroupes'][$lot->getLot()])) {
                                $intitule = '';
                                $lotsGroupes = (new Atexo_Consultation_Lots())->retrieveListeLotsByArray($consultation->getId(), $consultation->getOrganisme(), $this->_lotsExclusAndContratGroupees['lotsGroupes'][$lot->getLot()], Criteria::IN);
                                if (is_array($lotsGroupes)) {
                                    $intitule .= '<div class="groupement-lot">';
                                    $i = 0;
                                    foreach ($lotsGroupes as $oneLotGroupe) {
                                        $classeCss = '';
                                        if (0 == $i) {
                                            $classeCss = 'class="first"';
                                        } elseif ($i == count($lotsGroupes) - 1) {
                                            $classeCss = 'class="last"';
                                        }
                                        $intitule .= '<div ' . $classeCss . '>';
                                        $intitule .= Prado::Localize('LOT') . ' ' . $oneLotGroupe->getLot() . ' - ' . $oneLotGroupe->getDescription();
                                        $intitule .= '</div>';

                                        ++$i;
                                    }
                                    $intitule .= '</div>';
                                }
                                $idLots = implode('#', $this->_lotsExclusAndContratGroupees['lotsGroupes'][$lot->getLot()]);
                            } else {
                                $intitule = Prado::Localize('LOT') . ' ' . $lot->getLot() . ' - ' . $lot->getDescription();
                                $idLots = $lot->getLot();
                            }
                            // TODO retour en arrière sur cette écolution, je garde le code pour la suite
                            // if (null === $decisionLot) {
                            $dataSource[$lot->getLot()] = [
                                'lots' => $idLots,
                                'referenceConsultation' => $consultationId,
                                'organisme' => $organisme,
                                'intitule' => $intitule,
                                'listeContrats' => $listeContrats,
                                'contratMulti' => $contratMulti,
                                'decisionLot' => $decisionLot,
                                "listeSelectionEntreprise"  => $listeSelectionEntreprise,
                                'contratsFromExecFound'     => !empty($contratFromExecFound),
                                'isContratExecMulti'        => !empty($isContratExecMulti)
                            ];
                        }
                    }
                }
            } else {
                $listeContrats = (new Atexo_Consultation_Contrat())->getListeContrats($consultationId, $organisme, 0);
                if (!empty(Atexo_Config::getParameter('ACTIVE_EXEC_V2'))) {
                    $execWS = Atexo_Util::getSfService(WebServicesExec::class);
                    $query = [
                        "page"                      => 0,
                        "size"                      => 5000,
                        "idExternesConsultation"    => $consultationId
                    ];

                    $response = $execWS->getContent('getContrats', $query);
                    $contratsFromExec = $response['content'] ?? [];

                    if (!empty($contratsFromExec[0]['type']['codeExterne'])) {
                        $firstTypeContrat = (new Atexo_TypeContrat())->retrieveTypeContratByLibelle('TYPE_CONTRAT_' . $contratsFromExec[0]['type']['codeExterne']);
                        $isContratExecMulti = $firstTypeContrat?->getMulti() == '1';
                    }

                    if (empty($contratsFromExec)) {
                        $decisionLot = (new Atexo_Consultation_Decision())->retrieveDecisionLotByReferenceAndLot($consultationId, 0, $organisme);
                        if (!empty($decisionLot) && (
                                $decisionLot->getIdTypeDecision() == Atexo_Config::getParameter('DECISION_ATTRIBUTION_MARCHE')
                                ||
                                $decisionLot->getIdTypeDecision() == Atexo_Config::getParameter('DECISION_ADMISSION_SAD')
                            )) {
                            $decisionLot->delete();
                        }
                    }

                    $listeContrats = [];
                }
                $oneContrat = is_array($listeContrats) && count($listeContrats) ? $listeContrats[0] : '';
                $contratMulti = '';
                if ($oneContrat instanceof CommonTContratTitulaire) {
                    $contratMulti = $oneContrat->getCommonTContratMulti();
                }
                $decisionLot = (new Atexo_Consultation_Decision())->retrieveDecisionLotByReferenceAndLot($consultationId, 0, $organisme);
                $decisionEntrepriseQuery = new CommonTDecisionSelectionEntrepriseQuery();
                $listeSelectionEntreprise = (array) $decisionEntrepriseQuery->retrieveDecisionSelectionEntreprise($consultationId, 0, $organisme);
                $intitule = $consultation->getReferenceUtilisateur() . ' - ' . $consultation->getObjetTraduit();
                // if (null === $decisionLot) {
                $dataSource[0] = [
                    'lots' => 0,
                    'referenceConsultation' => $consultationId,
                    'organisme' => $organisme,
                    'intitule' => $intitule,
                    'listeContrats' => $listeContrats,
                    'contratMulti' => $contratMulti,
                    'decisionLot' => $decisionLot,
                    "listeSelectionEntreprise"  => $listeSelectionEntreprise,
                    'contratsFromExecFound'     => !empty($contratsFromExec),
                    'isContratExecMulti'        => !empty($isContratExecMulti)
                ];
                /* }else {
                    $this->nombreResultat->Text--;
                }*/
            }
        }

        //@todo : [TAG] : Diff
        /* if($this->nombreResultat->Text == 0) {
            $this->panelDecisionConsultation->setVisible(false);
        }*/

        return $dataSource;
    }

    /**
     * Permet de verfifier si la consultation est allotie.
     *
     * @param void
     *
     * @return bool true si la consultation est allotie,false si non
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function isConsultationAllotie()
    {
        return ($this->getConsultation() instanceof CommonConsultation) && $this->getConsultation()->getAlloti() ? true : false;
    }

    /**
     * Permet d'avoir les actions possible.
     *
     * @param string $statutContrat statut de contrat
     *
     * @return array tableau des actions possible
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function possibleActionsUnique($statutContrat, $contrat = null)
    {
        $actions = [];
        $actions = match ($statutContrat) {
            intval(Atexo_Config::getParameter('STATUT_DECISION_CONTRAT')) => self::getActionDecisionContrat(),
            intval(Atexo_Config::getParameter('STATUT_DONNEES_CONTRAT_A_SAISIR')) => self::getActionConstructionContrat($contrat),
            intval(Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT')) => self::getActionNotificationContrat($contrat),
            intval(Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT_EFFECTUEE')) => self::getActionContratNotifie($contrat),
            default => $actions,
        };

        return $actions;
    }

    /**
     * Permet de retourner les action dans l'etape construction.
     *
     * @param CommonTContratTitulaire $contrat : contrat
     *
     * @return array tableau des actions possible
     *
     * @author AMAL EL BEKKAOUI <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function getActionConstructionContrat($contrat)
    {
        $actions = [];
        if ($contrat instanceof CommonTContratTitulaire) {
            if (Atexo_CurrentUser::hasHabilitation('ModifierContrat') && !Atexo_Config::getParameter('ACTIVE_EXEC_V2')) {
                $actions[Atexo_Config::getParameter('DECLARER_LE_CONTRAT')] = Prado::Localize('DECLARER_LE_CONTRAT');
            }
            if (Atexo_CurrentUser::hasHabilitation('SupprimerContrat')) {
                $actions[Atexo_Config::getParameter('SUPPRIMER_CONTRAT')] = Prado::Localize('SUPPRIMER_LE_CONTRAT');
            }
            if (Atexo_CurrentUser::hasHabilitation('ConsulterContrat') && !Atexo_Config::getParameter('ACTIVE_EXEC_V2')) {
                $actions[Atexo_Config::getParameter('CONSULTER_CONTRAT')] = Prado::Localize('TEXT_CONSULTER_LE_CONTRAT');
            }
        }

        return $actions;
    }

    /**
     * Permet de retourner les action dans l'etape notification.
     *
     * @param CommonTContratTitulaire $contrat : contrat
     *
     * @return array tableau des actions possible
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function getActionNotificationContrat($contrat)
    {
        $actions = [];
        if ($contrat instanceof CommonTContratTitulaire) {
            if ((new Atexo_Module())->isChorusAccessActivate(Atexo_CurrentUser::getCurrentServiceId()) && $contrat->getModeEchangeChorus()) {
                $actions[Atexo_Config::getParameter('ECHANGE_CHORUS')] = Prado::Localize('TEXT_ACCEDER_ECHANGE_CHORUS');
                if (Atexo_CurrentUser::hasHabilitation('ModifierContrat')) {
                    if ((new Atexo_Consultation_Contrat())->isRetourChorus($contrat)) {
                        $actions[Atexo_Config::getParameter('NOTIFICATION')] = Prado::Localize('NOTIFIER_LE_CONTRAT');
                        $actions[Atexo_Config::getParameter('NOTIFICATION_MESSAGERIE')] = Prado::Localize('NOTIFIER_PAR_MESSAGERIE');
                        $actions[Atexo_Config::getParameter('EDITER_CONTRAT')] = Prado::Localize('TEXT_EDITER_LE_CONTRAT');
                    } else {
                        $actions[Atexo_Config::getParameter('EDITER_CONTRAT')] = Prado::Localize('TEXT_EDITER_LE_CONTRAT');
                    }
                }
                if (Atexo_CurrentUser::hasHabilitation('SupprimerContrat')) {
                    $actions[Atexo_Config::getParameter('SUPPRIMER_CONTRAT')] = Prado::Localize('SUPPRIMER_LE_CONTRAT');
                }
            } else {
                if (Atexo_CurrentUser::hasHabilitation('ModifierContrat')) {
                    $actions[Atexo_Config::getParameter('NOTIFICATION')] = Prado::Localize('NOTIFIER_LE_CONTRAT');
                    $actions[Atexo_Config::getParameter('NOTIFICATION_MESSAGERIE')] = Prado::Localize('NOTIFIER_PAR_MESSAGERIE');
                    $actions[Atexo_Config::getParameter('EDITER_CONTRAT')] = Prado::Localize('TEXT_EDITER_LE_CONTRAT');
                }
                if (Atexo_CurrentUser::hasHabilitation('SupprimerContrat')) {
                    $actions[Atexo_Config::getParameter('SUPPRIMER_CONTRAT')] = Prado::Localize('SUPPRIMER_LE_CONTRAT');
                }
            }
            if (Atexo_CurrentUser::hasHabilitation('ConsulterContrat')) {
                $actions[Atexo_Config::getParameter('CONSULTER_CONTRAT')] = Prado::Localize('TEXT_CONSULTER_LE_CONTRAT');
            }
        }

        return $actions;
    }

    /**
     * Permet d'avoir les actions possible sur une consultation.
     *
     * @param void
     *
     * @return array tableau des actions possible
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function getActionDecisionContrat($fromActionGroupee = false)
    {
        $actions = [];
        $consultation = $this->getConsultation();
        if (Atexo_CurrentUser::hasHabilitation('AttributionMarche')) {
            if ($consultation instanceof CommonConsultation) {
                if ('1' == $consultation->getTypeDecisionAttributionMarche() || '1' == $consultation->getTypeDecisionAttributionAccordCadre()) {
                    $actions[Atexo_Config::getParameter('DECISION_ATTRIBUTION_MARCHE')] = Prado::Localize('ATTRIBUER');
                }
                if ($fromActionGroupee && (new Atexo_Consultation_Contrat())->isTypeContratMarche($consultation->getTypeMarche())) {
                    $actions[Atexo_Config::getParameter('DECISION_ATTRIBUTION_MEME_FOURNISSEUR')] = Prado::Localize('ATTRIBUER_A_UN_MEME_FOURNISSEUR');
                }
                if ('1' == $consultation->getTypeDecisionAdmissionSad()) {
                    $actions[Atexo_Config::getParameter('DECISION_ADMISSION_SAD')] = Prado::Localize('ADMETTRE_SAD');
                }
                if ('1' == $consultation->getTypeDecisionSelectionEntreprise()) {
                    $actions[Atexo_Config::getParameter('DECISION_SELECTION_ENTREPRISE')] = Prado::Localize('SELECTIONNER_ENTREPRISES_EN_VUE_PHASE_SUIVANTE');
                }
                if ('1' == $consultation->getTypeDecisionDeclarationSansSuite()) {
                    $actions[Atexo_Config::getParameter('DECISION_DECLARATION_SANS_SUITE')] = Prado::Localize('DEFINE_DECLARER_SANS_SUITE');
                }
                if ('1' == $consultation->getTypeDecisionDeclarationInfructueux()) {
                    $actions[Atexo_Config::getParameter('DECISION_DECLARATION_INFRUCTUEUX')] = Prado::Localize('DEFINE_DECLARER_INFRUCTUEUX');
                }
                if ('1' == $consultation->getTypeDecisionAutre()) {
                    $actions[Atexo_Config::getParameter('DECISION_AUTRE')] = Prado::Localize('AUTRE');
                }
            }
        }

        return $actions;
    }

    /**
     * Permet d'avoir la liste des actions groupee.
     *
     * @param void
     *
     * @return array tableau des actions possible
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function possibleActionsGroupee()
    {
        return self::getActionDecisionContrat(true);
    }

    /**
     * Permet de remplir la liste des actions groupee.
     *
     * @param void
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function fillActionsGroupees()
    {
        $this->listeActionsGroupees->DataSource = $this->possibleActionsGroupee();
        $this->listeActionsGroupees->DataBind();
    }

    /**
     * Permet d'executer l'action choisi.
     *
     * @param $sender
     * @param $param
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function actionDecision($sender, $param)
    {
        $idLot = $param->CommandParameter;
        $action = $this->getActionSelectedFromElement($idLot);
        $arrayUrlAction = $this->getUrlRedirectByAction($action);
        $urlAction = $arrayUrlAction['url'];
        if ($urlAction && $this->getConsultation() instanceof CommonConsultation) {
            $consultationId = $this->getConsultation()->getId();
            $urlAction .= '&id=' . base64_encode($consultationId) . '&lots=' . base64_encode($idLot);
            $urlAction .= '&time=' . base64_encode(date('H:i:s'));
            $this->javascriptLabel->Text = "<script>javascript:popUp('" . $urlAction . "', 'yes')</script>";
        }
    }

    /**
     * Permet d'executer l'action groupee choisi.
     *
     * @param $sender
     * @param $param
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function actionGroupeeDecision($sender, $param)
    {
        $idsLot = '';
        foreach ($this->repraterDecision->Items as $item) {
            if ($item->checkListLots->checked) {
                $idsLot .= '#' . $item->idLot->value;
            }
        }
        $idsLot = ltrim($idsLot, '#');
        if ($idsLot) {
            $action = $this->listeActionsGroupees->getSelectedValue();
            $arrayUrlAction = $this->getUrlRedirectByAction($action);
            $urlAction = $arrayUrlAction['url'];
            if ($urlAction && $this->getConsultation() instanceof CommonConsultation) {
                $consultationId = $this->getConsultation()->getId();
                $urlAction .= '&id=' . base64_encode($consultationId) . '&lots=' . base64_encode($idsLot);
                $urlAction .= '&time=' . base64_encode(date('H:i:s'));
                $this->javascriptLabel->Text = "<script>javascript:popUp('" . $urlAction . "', 'yes');</script>";
            }
        }
    }

    /**
     * permet de retourner l'action choisi.
     *
     * @param int $idLot
     *
     * @return string l'id de l'action
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function getActionSelectedFromElement($idLot)
    {
        foreach ($this->repraterDecision->Items as $item) {
            if ($item->idLot->value == $idLot) {
                return $item->listeActions->getSelectedValue();
            }
        }

        return false;
    }

    /**
     * Permet de retourner l'url de redirection selon l'action.
     *
     * @param string $action l'action
     *
     * @return string l'url de redirection
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function getUrlRedirectByAction($action)
    {
        $url = '';
        $typePage = self::TYPE_POPUP;
        switch ($action) {
            case Atexo_Config::getParameter('DECISION_ATTRIBUTION_MARCHE'):
                $url = 'index.php?page=Agent.PopupDecisionAttribution&action=decision&typeDecision=' .
                base64_encode(Atexo_Config::getParameter('DECISION_ATTRIBUTION_MARCHE'));
                break;
            case Atexo_Config::getParameter('DECISION_ATTRIBUTION_MEME_FOURNISSEUR'):
                $url = 'index.php?page=Agent.PopupDecisionAttribution&action=decision&typeDecision=' .
                base64_encode(Atexo_Config::getParameter('DECISION_ATTRIBUTION_MEME_FOURNISSEUR'));
                break;
            case Atexo_Config::getParameter('DECISION_ADMISSION_SAD'):
                $url = 'index.php?page=Agent.PopupDecisionAttribution&action=decision&typeDecision=' .
                base64_encode(Atexo_Config::getParameter('DECISION_ADMISSION_SAD'));
                break;
            case Atexo_Config::getParameter('DECISION_DECLARATION_SANS_SUITE'):
                $url = 'index.php?page=Agent.PopupDecisionNonAttribution&action=decision&typeDecision=' . Atexo_Config::getParameter('DECISION_DECLARATION_SANS_SUITE');
                break;

            case Atexo_Config::getParameter('DECISION_DECLARATION_INFRUCTUEUX'):
                $url = 'index.php?page=Agent.PopupDecisionNonAttribution&action=decision&typeDecision=' . Atexo_Config::getParameter('DECISION_DECLARATION_INFRUCTUEUX');
                break;

            case Atexo_Config::getParameter('DECISION_SELECTION_ENTREPRISE'):
                $url = 'index.php?page=Agent.PopupDecisionAttribution&action=decision&typeDecision=' .
                base64_encode(Atexo_Config::getParameter('DECISION_SELECTION_ENTREPRISE'));
                break;

            case Atexo_Config::getParameter('DECISION_AUTRE'):
                $url = 'index.php?page=Agent.PopupDecisionNonAttribution&action=decision&typeDecision=' . Atexo_Config::getParameter('DECISION_AUTRE');
                break;
            case Atexo_Config::getParameter('DECLARER_LE_CONTRAT'):
                $url = 'index.php?page=Agent.PopupDecisionContrat&action=decision';
                break;
            case Atexo_Config::getParameter('NUMEROTATION_AUTONOME'):
                $url = 'index.php?page=Agent.PopupNumerotationContrat&action=decision';
                break;
            case Atexo_Config::getParameter('ECHANGE_CHORUS'):
                $url = 'index.php?page=Agent.ChorusEchanges&fromCons&action=decision';
                $typePage = self::TYPE_PAGE;
                break;
            case Atexo_Config::getParameter('NOTIFICATION'):
                $url = 'index.php?page=Agent.PopupNotificationContrat&action=decision';
                break;
            case Atexo_Config::getParameter('NOTIFICATION_MESSAGERIE'):
                $url = 'index.php?page=Agent.EnvoiCourrierElectroniqueNotification&action=decision';
                $typePage = self::TYPE_PAGE;
                break;
            case Atexo_Config::getParameter('SUPPRIMER_CONTRAT'):
                $url = '';
                $typePage = self::TYPE_POPIN;
                break;
            case Atexo_Config::getParameter('EDITER_CONTRAT'):
                $url = 'index.php?page=Agent.PopupDecisionContrat&action=decision';
                break;
            case Atexo_Config::getParameter('CONSULTER_CONTRAT'):
                $url = 'index.php?page=Agent.PopupConsulterContrat&action=decision';
                break;
            default:
                $url = '';
        }

        return ['typePage' => $typePage, 'url' => $url, 'action' => $action];
    }

    /**
     * Permet de retourner le titre de l'etape.
     *
     * @param string $statut le statut du contrat
     *
     * @return string le titre de l'etape
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function getTitreEtape($statut)
    {
        return match ($statut) {
            (int) Atexo_Config::getParameter('STATUT_DONNEES_CONTRAT_A_SAISIR') => Prado::Localize('DONNEES_DU_CONTRAT_A_SAISIR'),
            (int) Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT') => Prado::Localize('ETAPE_NOTIFICATION'),
            (int) Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT_EFFECTUEE') => Prado::Localize('ETAPE_NOTIFICATION_EFFECTUEE'),
            default => '',
        };
    }

    /**
     * Permet de retourner le titre du calendrier du date previsionnel.
     *
     * @param string $statut le statut du contrat
     *
     * @return string le titre du calendrier du date previsionnel
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function getTitreDataPrev($statut)
    {
        return match ($statut) {
            Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT_EFFECTUEE') => Prado::Localize('DATE_NOTIFICATION'),
            default => Prado::Localize('DATE_PREVISIONNELLE_NOTIFICATION'),
        };
    }

    /**
     * Permet de retourner l'etape de decision.
     *
     * @param string $statut le statut du contrat
     *
     * @return string l'etape
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function getEtapeDecision($statut)
    {
        return match (strval($statut)) {
            Atexo_Config::getParameter('STATUT_DONNEES_CONTRAT_A_SAISIR') => 'contrat-etape2sur3',
            Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT') => 'contrat-etape3asur3',
            Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT_EFFECTUEE') => 'contrat-etape3bsur3',
            default => '',
        };
    }

    /**
     * Permet de refresher le repeater.
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function refreshRepeater()
    {
        $this->javascriptLabel->Text = '';
        $lots = $this->listeLotToUp->value;
        $listeIdsLot = explode('#', $lots);
        if (is_array($listeIdsLot) && count($listeIdsLot)) {
            $dataSourceUp = $this->getDataSource();
            $consultation = $this->getConsultation();
            if ($consultation instanceof CommonConsultation) {
                $consultationId = $consultation->getId();
                $organisme = $consultation->getOrganisme();
                foreach ($listeIdsLot as $idLot) {
                    $listeContrats = (new Atexo_Consultation_Contrat())->getListeContrats($consultationId, $organisme, $idLot);
                    if ($dataSourceUp[$idLot]) {
                        $dataSourceUp[$idLot]['listeContrats'] = $listeContrats;
                    }
                }
                $this->setDataSource($dataSourceUp);
                self::updatePagination($this->nbResultsTop->getSelectedValue(), $this->pageNumberTop->Text);
                $this->populateData();
            }
        }
    }

    /**
     * Permet d'executer une action sur le contrat choisi.
     *
     * @param $sender
     * @param $param
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function actionSurContrat($sender, $param)
    {
        $arrayParams = $param->CommandParameter;

        if (is_array($arrayParams)) {
            $idContrat = $arrayParams['idContrat'];
            $lots = $arrayParams['lots'];
            $action = $this->getActionSelectedFromElementContrat($idContrat);
            $urlAction = $this->getUrlRedirectByAction($action);
            if ($urlAction['url'] && $this->getConsultation() instanceof CommonConsultation) {
                $consultationId = $this->getConsultation()->getId();
                $url = $urlAction['url'];
                $url .= '&id=' . base64_encode($consultationId) . '&lots=' . base64_encode($lots) . '&idContrat=' . base64_encode($idContrat);
                $url .= '&time=' . base64_encode(date('H:i:s'));
                $script = '';
                if (self::TYPE_POPUP == $urlAction['typePage']) {
                    $script = "<script>javascript:popUp('" . $url . "', 'yes')</script>";
                } else {
                    $messecVersion = $this->getConsultation()->getVersionMessagerie();
                    if (2 === $messecVersion && $urlAction['action'] == Atexo_Config::getParameter('NOTIFICATION_MESSAGERIE')) {
                        $contratTitulaireQuery = new CommonTContratTitulaireQuery();
                        $contratTitulaire = $contratTitulaireQuery->findOneByIdContratTitulaire($idContrat);
                        $offreId = $contratTitulaire->getIdOffre();



                        $encrypte = Atexo_Util::getSfService(Encryption::class);
                        $idCrypte = $encrypte->cryptId($consultationId);
                        $offreIdCrypte = $encrypte->cryptId($offreId);
                        $urlMessec = Atexo_MultiDomaine::replaceDomain(
                            Atexo_Config::getParameter('PF_URL_MESSAGERIE_COURRIER_NOTIFICATION_CONSULTATION')
                        )
                            .'/'
                            .$idCrypte.'/'.$offreIdCrypte;
                        if ($contratTitulaire->getTypeDepotReponse() === '2') {
                            $urlMessec = Atexo_MultiDomaine::replaceDomain(
                                Atexo_Config::getParameter('PF_URL_MESSAGERIE_COURRIER_NOTIFICATION_CONSULTATION_PAPIER')
                            )
                                .'/'
                                .$idCrypte.'/'.$offreIdCrypte;
                        }

                        $script .= "<script> window.open('" . $urlMessec . "') ;</script>";
                    } else {
                        $this->redirectBouton->NavigateUrl = $url;
                        $script = "<script>document.getElementById('" .
                            $this->redirectBouton->getClientID() .
                            "').click();</script>";
                    }
                }
                $this->javascriptLabel->Text = $script;
            }
            if (self::TYPE_POPIN == $urlAction['typePage'] && $this->getConsultation() instanceof CommonConsultation) {
                $this->idContratToDelete->value = $idContrat;
                $this->organismeContrat->value = $this->getConsultation()->getOrganisme();
                $this->lotContratToDelete->value = $lots;
                $cas = Atexo_Consultation_Contrat::getCasSuppressionContrat($idContrat, $this->getConsultation()->getOrganisme());
                $this->messageConfirmationSuppressionContrat->Text = Atexo_Consultation_Contrat::getMessageConfirmationSuppressionContrat($cas, $idContrat, $this->getConsultation()->getOrganisme());
                $this->gererAffichageBoutonValidationSuppressionContrat($cas);
                $script = "<script>openModal('demander-suppression','popup-small2',document.getElementById('" . $this->titrePopin->getClientId() . "'));</script>";
                $this->javascriptLabel->Text = '';
                $this->javascriptLabel->Text = $script;
            }
        }
    }

    /**
     * Permet d'avoir l'action selectionnee.
     *
     * @param int $idContrat l'id de contrat concerne
     *
     * @return Intger id de l'action selectionnee
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function getActionSelectedFromElementContrat($idContrat)
    {
        foreach ($this->repraterDecision->Items as $item) {
            foreach ($item->repeaterContrat->Items as $itemContrat) {
                if ($itemContrat->contratId->value == $idContrat) {
                    return $itemContrat->listeActions->getSelectedValue();
                }
            }
        }

        return false;
    }

    /**
     * TODO.
     */
    public function showComponent()
    {
        $this->PagerTop->visible = true;
        $this->PagerBottom->visible = true;
        $this->pageNumberBottom->visible = true;
        $this->pageNumberTop->visible = true;
        $this->labelSlashBottom->visible = true;
        $this->labelSlashTop->visible = true;
        $this->nombrePageTop->visible = true;
        $this->nombrePageBottom->visible = true;
        $this->labelAfficherTop->visible = true;
        $this->labelAfficherDown->visible = true;
        $this->resultParPageDown->visible = true;
        $this->resultParPageTop->visible = true;
        $this->panelBouttonGotoPageBottom->visible = true;
        $this->panelBouttonGotoPageTop->visible = true;

        if ($this->isTypeProcedureSad()) {
            $this->labelAfficherSad->visible = true;
            // On n'affiche plus la pagination en mode type de prorcédure Sad (selection lot)
            $this->labelDefineNombreResultat->visible = false;
            $this->labelAfficherTop->visible = false;
            $this->nbResultsTop->visible = false;
            $this->nbResultsBottom->visible = false;
            $this->labelAfficherDown->visible = false;
            $this->resultParPageTop->visible = false;
            $this->resultParPageDown->visible = false;
            $this->panelBouttonGotoPageBottom->visible = false;
            $this->panelBouttonGotoPageTop->visible = false;
        } else {
            $this->nbResultsTop->visible = true;
            $this->nbResultsBottom->visible = true;
            $this->lotsSad->visible = false;
            $this->labelAfficherSad->visible = false;
        }
    }

    /**
     * TODO.
     */
    public function hideComponent()
    {
        $this->PagerTop->visible = false;
        $this->PagerBottom->visible = false;
        $this->pageNumberBottom->visible = false;
        $this->pageNumberTop->visible = false;
        $this->labelSlashBottom->visible = false;
        $this->labelSlashTop->visible = false;
        $this->nombrePageTop->visible = false;
        $this->nombrePageBottom->visible = false;
        $this->nbResultsTop->visible = false;
        $this->labelAfficherTop->visible = false;
        $this->labelAfficherDown->visible = false;
        $this->nbResultsBottom->visible = false;
        $this->resultParPageTop->visible = false;
        $this->resultParPageDown->visible = false;
        $this->panelBouttonGotoPageBottom->visible = false;
        $this->panelBouttonGotoPageTop->visible = false;
        $this->lotsSad->visible = false;
        $this->labelAfficherSad->visible = false;
        $this->labelDefineNombreResultat->visible = false;
        $this->nbrElementRepeater = 0;
    }

    /**
     * @param $sender
     * @param $param
     * TODO
     */
    public function changePagerLenght($sender, $param)
    {
        $pageSize = null;
        switch ($sender->ID) {
            case 'nbResultsBottom':
                $pageSize = $this->nbResultsBottom->getSelectedValue();
                $this->nbResultsTop->setSelectedValue($pageSize);
                break;
            case 'nbResultsTop':
                $pageSize = $this->nbResultsTop->getSelectedValue();
                $this->nbResultsBottom->setSelectedValue($pageSize);
                break;
        }

        self::updatePagination($pageSize, $this->pageNumberTop->Text);
        $this->initaliserComposant();
        $this->javascriptLabel->Text = '';
        $this->panelDecisionConsultation->render($param->getNewWriter());
    }

    /*
     * Permet de mettre ? jour la pagination
     */
    public function updatePagination($pageSize, $numPage)
    {
        $nombreElement = $this->getViewState('nbrElements');
        $this->repraterDecision->PageSize = $pageSize;
        $this->nombrePageTop->Text = ceil($nombreElement / $this->repraterDecision->PageSize);
        $this->nombrePageBottom->Text = ceil($nombreElement / $this->repraterDecision->PageSize);
        if ($numPage > $this->nombrePageTop->Text) {
            $numPage = $this->nombrePageTop->Text;
        }
        $this->repraterDecision->setCurrentPageIndex($numPage - 1);
        $this->pageNumberBottom->Text = $numPage;
        $this->pageNumberTop->Text = $numPage;
        $this->setNbrElementRepeater($nombreElement);
    }

    /**
     * @param $nbrElement
     *
     * @return mixed
     */
    public function setNbrElementRepeater($nbrElement)
    {
        return $this->nbrElementRepeater = $nbrElement;
    }

    /**
     * Permet d'avoir le montant max du contrat.
     *
     * @param CommonTContratMulti $contratMulti
     *
     * @return string le montant max du contrat
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function getMontantMaxFromContrats($contratMulti)
    {
        if ($contratMulti instanceof CommonTContratMulti) {
            return $contratMulti->getMontantMaxEstime() ? Atexo_Util::getMontantArronditEspace($contratMulti->getMontantMaxEstime()) : '-';
        }

        return '';
    }

    /**
     * Permet d'avoir le libelle de type de contrat.
     *
     * @param CommonTContratMulti $contratMulti
     *
     * @return string libelle de type de contrat
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function getLabelleTypeContratMulti($contratMulti)
    {
        if ($contratMulti instanceof CommonTContratMulti) {
            return $contratMulti->getLibelleTypeContrat();
        }

        return '-';
    }

    /**
     * Permet d'avoir lstatut d'action from decision.
     *
     * @param CommonDecisionLot $decision decision
     *
     * @return string le statut d'action
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function getStatutActionFromDecision($decision)
    {
        if ($decision instanceof CommonDecisionLot) {
            if (
                $decision->getIdTypeDecision() == Atexo_Config::getParameter('DECISION_DECLARATION_SANS_SUITE')
                || $decision->getIdTypeDecision() == Atexo_Config::getParameter('DECISION_DECLARATION_INFRUCTUEUX')
                || $decision->getIdTypeDecision() == Atexo_Config::getParameter('DECISION_AUTRE')
                || $decision->getIdTypeDecision() == Atexo_Config::getParameter('DECISION_SELECTION_ENTREPRISE')
            ) {
                return '';
            }
        }

        return 0;
    }

    /**
     * Permet d'avoir intitulé from decision.
     *
     * @param CommonDecisionLot $decision decision
     *
     * @return string le type de décision
     *
     * @author Fatima Ezzahra IZGUA <fatima.izgua@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function getIntituleDecision($decision)
    {
        $libelleDecision = Prado::Localize('DECISION') . ' : ';
        if ($decision instanceof CommonDecisionLot) {
            if ($decision->getIdTypeDecision() == Atexo_Config::getParameter('DECISION_DECLARATION_SANS_SUITE')) {
                return $libelleDecision . Prado::Localize('DEFINE_DECLAREE_SANS_SUITE');
            } elseif ($decision->getIdTypeDecision() == Atexo_Config::getParameter('DECISION_DECLARATION_INFRUCTUEUX')) {
                return $libelleDecision . Prado::Localize('DEFINE_DECLAREE_INFRUCTUEUX');
            } elseif ($decision->getIdTypeDecision() == Atexo_Config::getParameter('DECISION_AUTRE')) {
                return $libelleDecision . Prado::Localize('DECISION_AUTRE');
            } elseif ($decision->getIdTypeDecision() == Atexo_Config::getParameter('DECISION_SELECTION_ENTREPRISE')) {
                return $libelleDecision . Prado::Localize('DEFINE_ENTREPRISE_SELECTIONNEES_EN_VUE_PHASE_SUIVANTE');
            }
        }
    }

    public function pageChanged($sender, $param)
    {
        $this->repraterDecision->CurrentPageIndex = $param->NewPageIndex;
        $this->setNbrElementRepeater($this->getViewState('nbrElements'));

        self::updatePagination($this->nbResultsTop->getSelectedValue(), $param->NewPageIndex + 1);

        $this->populateData();
    }

    public function getLotExclusAndContratsGroupees()
    {
        $contratGroupeParams = [];
        $sqlContratGroupe = ' SELECT id_contrat_titulaire, count(lot) AS NBR FROM t_cons_lot_contrat
                              WHERE consultation_id = :ref AND organisme = :organisme
                              GROUP BY id_contrat_titulaire
                              HAVING NBR > 1';
        $contratGroupeParams['ref'] = $this->getConsultation()->getId();
        $contratGroupeParams['organisme'] = $this->getConsultation()->getOrganisme();
        $statement = Atexo_Db::getLinkCommon(true)->prepare($sqlContratGroupe);
        $statement->execute($contratGroupeParams);
        $arrayContrat = $statement->fetchAll(PDO::FETCH_ASSOC);
        $arrayLotExclus = [];
        $arrayLotsGroupes = [];
        if (is_countable($arrayContrat) ? count($arrayContrat) : 0) {
            foreach ($arrayContrat as $oneElement) {
                $sqlLotExclus = "SELECT lot FROM t_cons_lot_contrat
                                WHERE id_contrat_titulaire = '" . $oneElement['id_contrat_titulaire'] . "'
                                ORDER BY lot "; // LIMIT 1, ". $oneElement['NBR'] . " ";

                $statement = Atexo_Db::getLinkCommon(true)->query($sqlLotExclus);
                $arrayLotContrat = $statement->fetchAll(PDO::FETCH_COLUMN, 0);
                if (is_countable($arrayLotContrat) ? count($arrayLotContrat) : 0) {
                    $arrayLotsGroupes[$arrayLotContrat[0]] = $arrayLotContrat;
                    array_shift($arrayLotContrat);
                    $arrayLotExclus = array_merge($arrayLotExclus, $arrayLotContrat);
                }
            }
        }

        return ['lotExclus' => $arrayLotExclus, 'lotsGroupes' => $arrayLotsGroupes];
    }

    /**
     * Permet d'afficher ou cacher le bloc des actions groupees.
     *
     * @param void
     *
     * @return bool
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function afficherActionGroupees()
    {
        if (!self::isConsultationAllotie() || !$this->getViewState('nbrElements')) {
            return false;
        }
        if (!empty(Atexo_Config::getParameter('ACTIVE_EXEC_V2'))) {
            $execWS = Atexo_Util::getSfService(WebServicesExec::class);
            $query = [
                "page"                      => 0,
                "size"                      => 50,
                "idExternesConsultation"    => $this->getConsultation()->getId(),
                "chapeau"                   => false
            ];

            $response = $execWS->getContent('getContrats', $query);
            $contratsFromExec = $response['content'] ?? [];

            if (!empty($contratsFromExec[0]['type']['codeExterne'])) {
                $firstTypeContrat = (new Atexo_TypeContrat())->retrieveTypeContratByLibelle('TYPE_CONTRAT_' . $contratsFromExec[0]['type']['codeExterne']);
                if ($firstTypeContrat?->getMulti() == '1') {
                    return true;
                }
            }

            if (!empty($contratsFromExec) && count($contratsFromExec) == $this->getViewState('nbrElements')) {
                return false;
            }
        }
        return true;
    }

    /**
     * Permet de telecharger la fiche de recensement.
     *
     * @param void
     *
     * @return bool
     *
     * @author Amal EL BEKKAOUI <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015-objet-contrat
     *
     * @copyright Atexo 2016
     */
    public function genarateFicheRecensement($sender, $param)
    {
        $idContrat = $param->CommandParameter;
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $commonTContratTitulaireQuery = new CommonTContratTitulaireQuery(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));
        $contrat = $commonTContratTitulaireQuery->findOneByIdContratTitulaire($idContrat, $connexion);
        $consultation = $this->getConsultation();
        $odtFilePath = (new Atexo_GenerationFichier_ImpressionODT())->generateFicheRessencementOdt($contrat);
        if ($odtFilePath) {
            $content = (new DocGeneratorClient())->genererDoc($odtFilePath);
            @unlink($odtFilePath);
            DownloadFile::downloadFileContent('Fiche_recensement.doc', $content);
        }
    }

    /**
     * Permet de vérifier si on peut ajouter un attributaire.
     *
     * @pram arary of CommonTcontratTiulaire contrats
     *
     * @return bool true si le contrat est multi et on a déjà une décision si nn false
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2015
     */
    public function canAddAttribute($data)
    {
        $contrats = $data['listeContrats'];
        $decisionLot = $data['decisionLot'];
        $bonType = false;
        if ($decisionLot instanceof CommonDecisionLot) {
            if ($decisionLot->getIdTypeDecision() == Atexo_Config::getParameter('DECISION_ATTRIBUTION_MARCHE') || $decisionLot->getIdTypeDecision() == Atexo_Config::getParameter('DECISION_ATTRIBUTION_MEME_FOURNISSEUR')) {
                $bonType = true;
            }
        }

        if (!empty(Atexo_Config::getParameter('ACTIVE_EXEC_V2'))) {
            return $data['contratsFromExecFound'] && !empty($data['isContratExecMulti']) || ($bonType && empty($data['contratsFromExecFound']));
        } else {
            return (is_array($contrats)) && count($contrats) && (new Atexo_Consultation_Contrat())->isOneContratMulti($contrats) || ($bonType && !(is_countable($contrats) ? count($contrats) : 0));
        }
    }

    /**
     * Permet de retourner l'url d'ajout d'un attributaire.
     *
     * @pram arary of data
     *
     * @return string l'url d'ajout d'un attributaire
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2015
     */
    public function getUrlAddAttribute($data)
    {
        $url = 'index.php?page=Agent.PopupDecisionAttribution';
        $url .= '&lots=' . base64_encode($data['lots']);
        $url .= '&id=' . base64_encode($data['referenceConsultation']);
        $decisionLot = $data['decisionLot'];
        if ($decisionLot instanceof CommonDecisionLot) {
            $url .= '&typeDecision=' . base64_encode($decisionLot->getIdTypeDecision());
        }

        return $url;
    }

    /**
     * Permet de supprimer un contrat.
     *
     * @param $sender
     * @param $param
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2016
     */
    public function supprimerAttributaire($sender, $param)
    {
        $idContrat = $this->idContratToDelete->value;
        //$lots = $this->lotContratToDelete->value;
        (new Atexo_Consultation_Contrat())->deleteContrat($idContrat);
        $this->javascriptLabel->Text = "<script>window.location.replace(window.location.href);J('.demander-suppression').dialog('destroy');</script>";
    }

    /**
     * Permet de retourner les action dans l'etape notification realisée.
     *
     * @param CommonTContratTitulaire $contrat : contrat
     *
     * @return array tableau des actions possible
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function getActionContratNotifie($contrat)
    {
        $actions = [];
        if ($contrat instanceof CommonTContratTitulaire) {
            if ((new Atexo_Module())->isChorusAccessActivate(Atexo_CurrentUser::getIdServiceAgentConnected()) && '' != $contrat->getModeEchangeChorus()) {
                $actions[Atexo_Config::getParameter('ECHANGE_CHORUS')] = Prado::Localize('TEXT_ACCEDER_ECHANGE_CHORUS');
            }
            if (Atexo_CurrentUser::hasHabilitation('ModifierContrat') && !Atexo_Module::isEnabled('DonneesEssentiellesSuiviSn')) {
                $actions[Atexo_Config::getParameter('EDITER_CONTRAT')] = Prado::Localize('TEXT_EDITER_LE_CONTRAT');
                $actions[Atexo_Config::getParameter('SUPPRIMER_CONTRAT')] = Prado::Localize('SUPPRIMER_LE_CONTRAT');
            }
            if (Atexo_CurrentUser::hasHabilitation('ConsulterContrat')) {
                $actions[Atexo_Config::getParameter('CONSULTER_CONTRAT')] = Prado::Localize('TEXT_CONSULTER_LE_CONTRAT');
            }
        }

        return $actions;
    }

    /**
     * Permet de gerer l'affichage des boutons de suppression du contrat et d'envoi de mail de suppression de l'EJ.
     *
     * @param $cas
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since 2017-roadmap
     *
     * @copyright Atexo 2017
     */
    public function gererAffichageBoutonValidationSuppressionContrat($cas)
    {
        switch ($cas) {
            case Atexo_Contrat_IndicateurEnum::CONTRAT_SUPPRESSION_AUTORISEE:
                $this->supprimerAttributaire->visible = true;
                $this->envoiMailAgent->visible = false;
                break;
            case Atexo_Contrat_IndicateurEnum::CONTRAT_SUPPRESSION_NON_AUTORISEE_ENVOI_MAIL:
                $this->supprimerAttributaire->visible = false;
                $this->envoiMailAgent->visible = true;
                break;
            default:
                $this->supprimerAttributaire->visible = false;
                $this->envoiMailAgent->visible = false;
                break;
        }
    }

    /**
     * Permet d'envoyer le mail de suppression de l'EJ (suite a la demande de suppression du contrat) a l'agent.
     *
     * @param $sender
     * @param $param
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @since 2017-roadmap
     *
     * @copyright Atexo 2017
     */
    public function envoiMailSuppressionEj($sender, $param)
    {
        Atexo_Consultation_Contrat::envoiMailSuppressionEj($this->idContratToDelete->value, $this->organismeContrat->value);
        $this->javascriptLabel->Text = "<script>J('.demander-suppression').dialog('destroy');</script>";
    }

    /**
     * @param $sender
     * @param $param
     */
    public function goToPage($sender, $param)
    {
        $numPage = null;
        switch ($sender->ID) {
            case 'DefaultButtonTop':
                $numPage = $this->pageNumberTop->Text;
                break;
            case 'DefaultButtonBottom':
                $numPage = $this->pageNumberBottom->Text;
                break;
        }
        if ((new Util())->isEntier($numPage)) {
            if ($numPage >= $this->nombrePageTop->Text) {
                $numPage = $this->nombrePageTop->Text;
            } elseif ($numPage <= 0) {
                $numPage = 1;
            }

            self::updatePagination($this->nbResultsTop->getSelectedValue(), $numPage);

            $this->populateData();
        } else {
            $this->pageNumberTop->Text = $this->repraterDecision->CurrentPageIndex + 1;
            $this->pageNumberBottom->Text = $this->repraterDecision->CurrentPageIndex + 1;
        }
    }

    public function isResetable(?CommonDecisionLot $decision): bool
    {
        $idTypeDecision = (null !== $decision) ? $decision->getIdTypeDecision() : null;
        $resetableDecision = [
            CommonDecisionLot::DECISION_SANS_SUITE,
            CommonDecisionLot::DECISION_INFRUCTUEUX,
            CommonDecisionLot::DECISION_ENTREPRISE_SELECTIONNEES,
            CommonDecisionLot::DECISION_AUTRE,
        ];

        return in_array($idTypeDecision, $resetableDecision);
    }

    public function isTypeProcedureSad(): bool
    {
        $consultationSad = $this->getConsultation();

        if (null === $consultationSad) {
            return false;
        }

        $acronymeTypeProcedure = (new Atexo_Consultation_ProcedureType())->retrieveAcronymeTypeProcedure(
            $consultationSad->getIdTypeProcedureOrg()
        );

        return $acronymeTypeProcedure === self::TYPE_PROCEDURE_SAD && $consultationSad->isAllotie();
    }

    public function fillListeLotsSad(): void
    {
        $consultationSad = $this->getConsultation();

        $lots = (new Atexo_Consultation_Lots())->retrieveListeLotsByArray(
            $consultationSad->getId(),
            $consultationSad->getOrganisme(),
            $this->getLotExclusAndContratsGroupees()['lotExclus'],
            Criteria::NOT_IN
        );

        $listLotSad = [];
        foreach ($lots as $key => $lot) {
            $listLotSad[$lot->getLot()] = sprintf(
                "%s %s - %s",
                Prado::Localize('TEXT_LOT'),
                $lot->getLot(),
                $lot->getDescription(),
            );

            if ($key === 0) {
                $this->lotsSad->setSelectedValue($lot->getLot());
            }
        }

        $this->lotsSad->DataSource = $listLotSad;
        $this->lotsSad->DataBind();
    }

    public function selectLotSad(?TActiveDropDownList $sender, TCallbackEventParameter $param): void
    {
        $pageSize = $this->lotsSad->getSelectedValue();
        $this->lotsSad->setSelectedValue($pageSize);

        $this->populateData();
        $this->javascriptLabel->Text = '';
        $this->panelDecisionConsultation->render($param->getNewWriter());
    }

    public function getAgentSso(): string
    {
        /** @var $agentService AgentService */
        $agentService = Atexo_Util::getSfService(AgentService::class);
        /** @var $currentUser CurrentUser */
        $currentUser = Atexo_Util::getSfService(CurrentUser::class);

        return $agentService->getSsoForSocle($currentUser);
    }

    public function getFiltresForWebComponent(): string
    {
        $filtres = [
            'idExternesConsultation'    => [(string)$this->getConsultation()->getId()]
        ];

        return json_encode($filtres);
    }
}
