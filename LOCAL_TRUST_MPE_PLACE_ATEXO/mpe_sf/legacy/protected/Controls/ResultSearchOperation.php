<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Operations\Atexo_Operations_CriteriaVo;
use Application\Service\Atexo\Operations\Atexo_Operations_Operations;

/**
 * commentaires.
 *
 * @author khadija Chouika <khadija.chouika@atexo.com>
 * @copyright Atexo 2014
 *
 * @version 6.0
 *
 * @since MPE-4.0
 */
class ResultSearchOperation extends MpeTTemplateControl
{
    public $nbrElementRepeater;

    public function getNbrElementRepeater()
    {
        return $this->nbrElementRepeater;
    }

    public function setNbrElementRepeater($nbrElement)
    {
        return $this->nbrElementRepeater = $nbrElement;
    }

    /*
     * L'evenement qui se lance au changement de la nombre de ligne souhaite par page
     */
    public function pageChanged($sender, $param)
    {
        $this->RepeaterOperations->CurrentPageIndex = $param->NewPageIndex;
        $this->pageNumberBottom->Text = $param->NewPageIndex + 1;
        $this->pageNumberTop->Text = $param->NewPageIndex + 1;
        $criteriaVo = $this->Page->getViewState('CriteriaVo');
        $this->populateData($criteriaVo);
    }

    /*
     * Permet changer l'affiche selon le nombre de page selectionne
     */
    public function changePagerLenght($sender, $param)
    {
        $pageSize = null;
        switch ($sender->ID) {
            case 'nbResultsBottom':
                 $pageSize = $this->nbResultsBottom->getSelectedValue();
                                                  $this->nbResultsBottom->setSelectedValue($pageSize);
                                                  $this->nbResultsTop->setSelectedValue($pageSize);
                break;
            case 'nbResultsTop':
                 $pageSize = $this->nbResultsTop->getSelectedValue();
                                               $this->nbResultsBottom->setSelectedValue($pageSize);
                                               $this->nbResultsTop->setSelectedValue($pageSize);
                break;
        }
        self::updatePagination($pageSize, $this->pageNumberTop->Text);
        $criteriaVo = $this->Page->getViewState('CriteriaVo');
        $this->populateData($criteriaVo);
    }

    /*
     * Permet de mettre a jour la pagination
     */
    public function updatePagination($pageSize, $numPage)
    {
        $nombreElement = $this->Page->getViewState('nombreElement');
        $this->RepeaterOperations->PageSize = $pageSize;
        $this->nombrePageBottom->Text = ceil($nombreElement / $this->RepeaterOperations->PageSize);
        $this->nombrePageTop->Text = ceil($nombreElement / $this->RepeaterOperations->PageSize);

        if ($numPage > $this->nombrePageTop->Text) {
            $numPage = $this->nombrePageTop->Text;
        }
        $this->RepeaterOperations->setCurrentPageIndex($numPage - 1);
        $this->pageNumberTop->Text = $numPage;
        $this->pageNumberBottom->Text = $numPage;
    }

    /*
     * Permet de remplir le repeater par le resultat selon le criteria
     * passer en parameter
     */
    public function populateData(Atexo_Operations_CriteriaVo $criteriaVo)
    {
        $offset = $this->RepeaterOperations->CurrentPageIndex * $this->RepeaterOperations->PageSize;
        $limit = $this->RepeaterOperations->PageSize;
        if ($offset + $limit > $this->RepeaterOperations->getVirtualItemCount()) {
            $limit = $this->RepeaterOperations->getVirtualItemCount() - $offset;
        }
        $criteriaVo->setOffset($offset);
        $criteriaVo->setLimit($limit);
        $this->Page->setViewState('CriteriaVo', $criteriaVo);
        $operations = (new Atexo_Operations_Operations())->search($criteriaVo);
        $this->RepeaterOperations->DataSource = $operations;
        $this->RepeaterOperations->DataBind();
    }

    /**
     * Pagination.
     */
    public function goToPage($sender)
    {
        $numPage = null;
        switch ($sender->ID) {
            case 'DefaultButtonTop':
                 $numPage = $this->pageNumberTop->Text;
                break;
            case 'DefaultButtonBottom':
                 $numPage = $this->numPageBottom->Text;
                break;
        }
        if (Atexo_Util::isEntier($numPage)) {
            if ($numPage >= $this->nombrePageTop->Text) {
                $numPage = $this->nombrePageTop->Text;
            } elseif ($numPage <= 0) {
                $numPage = 1;
            }
            self::updatePagination($this->nombreResultatAfficherTop->getSelectedValue(), $numPage);
            $criteriaVo = $this->Page->getViewState('CriteriaVo');
            $this->populateData($criteriaVo);
        } else {
            $this->numPageTop->Text = $this->RepeaterOperations->CurrentPageIndex + 1;
            $this->numPageBottom->Text = $this->RepeaterOperations->CurrentPageIndex + 1;
        }
    }

    /*
    * Permet de faire le fill de repeater des operations
    */
    public function fillRepeaterWithDataForSearchResult(Atexo_Operations_CriteriaVo $criteriaVo)
    {
        $nombreElement = (new Atexo_Operations_Operations())->search($criteriaVo, true);
        if ($nombreElement >= 1) {
            $this->panelMoreThanOneElementFound->setVisible(true);
            $this->panelNoElementFound->setVisible(false);
            $this->nombreElement->Text = $nombreElement;
            $this->PagerBottom->setVisible(true);
            $this->PagerTop->setVisible(true);
            $this->Page->setViewState('nombreElement', $nombreElement);
            $this->Page->setViewState('CriteriaVo', $criteriaVo);
            $this->nombrePageTop->Text = ceil($nombreElement / $this->RepeaterOperations->PageSize);
            $this->nombrePageBottom->Text = ceil($nombreElement / $this->RepeaterOperations->PageSize);
            $this->RepeaterOperations->setVirtualItemCount($nombreElement);
            $this->RepeaterOperations->setCurrentPageIndex(0);
            $this->populateData($criteriaVo);
        } else {
            $this->panelMoreThanOneElementFound->setVisible(false);
            $this->panelNoElementFound->setVisible(true);
            $this->panelPartiner->setVisible(false);
        }
    }

    /*
    * Permet de faire le tri
    */
    public function Trier($sender, $param)
    {
        $colunm = $sender->CommandName;
        $this->Page->setViewState('sortByElement', $colunm);
        $criteriaVo = $this->Page->getViewState('CriteriaVo');
        $criteriaVo->setSortByElement($colunm);
        $arraySensTri = $this->Page->getViewState('sensTriArray', []);
        $arraySensTri[$colunm] = ('ASC' == $criteriaVo->getSensOrderBy()) ? 'DESC' : 'ASC';
        $criteriaVo->setSensOrderBy($arraySensTri[$colunm]);
        $this->Page->setViewState('sensTriArray', $arraySensTri);
        $this->Page->setViewState('CriteriaVo', $criteriaVo);
        $this->populateData($criteriaVo);
    }

    /*
    * Permet de mettre la valeur de l'id de l'agent selectionner
    */
    public function getIdAgent($sender, $param)
    {
        $this->idSelecteOperation->value = $param->CallbackParameter;
    }

    /*
     * Permet gerer l'affichae selon le nombre de caractere de la chaine
     * passer en parametre
     */
    public function isTextTruncated($texte, $nbrCaractere = 100)
    {
        return (strlen($texte) > 230) ? '' : 'display:none';
    }

    /*
    * Permet de supprimer une operation
    */
    public function supprimerOperation($sender, $param)
    {
        $idOperation = $this->idSelecteOperation->value;
        (new Atexo_Operations_Operations())->deleteOperation($idOperation);
        $this->scriptT->Text = "<script>J('.".$this->getClientId()."_suppression-operation').dialog('close');</script>;";
        $criteriaVo = $this->Page->getViewState('CriteriaVo');
        $this->fillRepeaterWithDataForSearchResult($criteriaVo);
        $this->PanelResult->render($param->getNewWriter());
    }

    /*
    * Permet de rediriger vers la page de creation d'une operation
    */
    public function createOperation($sender, $param)
    {
        $criteria = $this->Page->getViewState('CriteriaVo');
        Atexo_CurrentUser::writeToSession('CriteriaVo', $criteria);
        if ($sender->CommandName) {
            $this->response->redirect(Atexo_Config::getParameter('PF_URL_AGENT').'index.php?page=Agent.FormulaireOperation&idOperation='.$sender->CommandName);
        } else {
            $this->response->redirect(Atexo_Config::getParameter('PF_URL_AGENT').'index.php?page=Agent.FormulaireOperation&idOperation');
        }
    }
}
