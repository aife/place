<com:TPanel ID="panelMessage" cssClass="alert bloc-message form-bloc-conf msg-erreur">
    <div class="top hide"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
    <div class="<%=$this->getCssClass()%> message-certificat">
        <i class="fa fa-exclamation-triangle m-r-1 hide-agent"></i>
        <com:TLabel ID="labelMessage"/>
        <div class="spacer-mini"></div>
    </div>
    <div class="bottom hide"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
</com:TPanel>


