<?php

namespace Application\Controls;

use Application\Propel\Mpe\CommonEntreprise;
use Application\Propel\Mpe\CommonTEtablissement;
use Application\Propel\Mpe\Entreprise;

/**
 * permet de gérer les infos du candidat.
 *
 * @author ASO <ayoub.souidahmed@atexo.com>
 *
 * @version 1.0
 *
 * @since 4.10.0
 *
 * @copyright Atexo 2015
 */
class AtexoInfoAttributaire extends MpeTTemplateControl
{
    private ?int $entreprise = null;

    public function onLoad($param)
    {
        $this->fillData();
    }

    /**
     * recupere la valeur du [entreprise].
     *
     * @return int la valeur courante [entreprise]
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.10.0
     *
     * @copyright Atexo 2015
     */
    public function getEntreprise()
    {
        return $this->entreprise;
    }

    /**
     * modifie la valeur de [entreprise].
     *
     * @param int $entreprise la valeur a mettre
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.10.0
     *
     * @copyright Atexo 2015
     */
    public function setEntreprise($entreprise)
    {
        return $this->entreprise = $entreprise;
    }

    /**
     * recupere la valeur du [etablissement].
     *
     * @return int la valeur courante [etablissement]
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function getEtablissement()
    {
        return $this->etablissement;
    }

    /**
     * modifie la valeur de [etablissement].
     *
     * @param CommonTEtablissement $etablissement la valeur a mettre
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function setEtablissement($etablissement)
    {
        return $this->etablissement = $etablissement;
    }

    public function getVisibiliteSiren()
    {
        if ($this->entreprise instanceof CommonEntreprise) {
            if ($this->entreprise->getSiren()) {
                return true;
            }
        }

        return false;
    }

    public function getAdresseEtab()
    {
        $codeEtab = null;
        $adresse = null;
        $codePostal = null;
        $ville = null;
        if ($this->getEtablissement() instanceof CommonTEtablissement) {
            $etab = $this->getEtablissement();
            if ($etab->getCodeEtablissement()) {
                $codeEtab = $etab->getCodeEtablissement().' - ';
            }
            if ($etab->getAdresse() || $etab->getAdresse2()) {
                $adresse = $etab->getAdresse().' '.$etab->getAdresse2().', ';
            }

            if ($etab->getCodePostal()) {
                $codePostal = $etab->getCodePostal().', ';
            }
            if ($etab->getVille()) {
                $ville = $etab->getVille();
            }

            return $codeEtab.$adresse.$codePostal.$ville;
        }

        return '-';
    }

    public function fillData()
    {
        if ($this->getEntreprise() instanceof Entreprise) {
            $this->raisonSocial->Text = $this->getEntreprise()->getNom();
            $etablissementSiege = $this->getEntreprise()->getEtablissementSiege();
            $infoSiege = '';
            if ($etablissementSiege instanceof CommonTEtablissement) {
                if ($etablissementSiege->getAdresse() || $etablissementSiege->getAdresse2()) {
                    $infoSiege .= $etablissementSiege->getAdresse().' '.$etablissementSiege->getAdresse2();
                }
                if ($etablissementSiege->getCodePostal()) {
                    $infoSiege .= ' '.$etablissementSiege->getCodePostal();
                }
                if ($etablissementSiege->getVille()) {
                    $infoSiege .= ' '.$etablissementSiege->getVille();
                }

                if ($etablissementSiege->getPays()) {
                    $infoSiege .= ' - '.$etablissementSiege->getPays();
                }
            }
            if (!$infoSiege) {
                $infoSiege = '-';
            }
            $this->siegeSocial->Text = $infoSiege;
            $this->codeAPE->Text = $this->getEntreprise()->getCodeape().' - '.$this->getEntreprise()->getLibelleApe();
            $this->siren->Text = $this->getEntreprise()->getSiren().' ('.$this->getEntreprise()->getNicSiege().')';
            $this->identifiantNational->Text = $this->getEntreprise()->getSirenetranger().' ('.$this->getEntreprise()->getNicSiege().')';
            $this->formeJuridique->Text = $this->getEntreprise()->getFormejuridique();
            $this->capital->Text = $this->getEntreprise()->getCapitalSocial() ?: '-';
        }
    }
}
