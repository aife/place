<?php

namespace Application\Controls;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonCategorieINSEEPeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Organismes;
use Exception;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * Classe DetailOrganisme.
 *
 * @author Yassine SBAI <yassine.sbai@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class DetailOrganisme extends MpeTPage
{
    public string $_org = '';
    protected string $_calledFrom = '';
    public bool $entiteValide = true;
    public string $_codeAcheteur = '';

    public function onLoad($param)
    {
        if (Atexo_Module::isEnabled('SiretDetailEntiteAchat')) {
            $this->sirenPanel->visible = true;
        } else {
            $this->sirenPanel->visible = false;
        }
        if ($this->getCodeAchteur()) {
            $this->codeAchteurPanel->visible = true;
        }

        if (Atexo_Module::isEnabled('SocleExternePpp')) {
            $this->buttoncreation->setVisible(false);
        }

        if (0 == strcmp($this->getCalledFrom(), 'entreprise')) {
            $org = $this->_org;
            $this->buttoncreation->setVisible(false);
        } else {
            if (Atexo_CurrentUser::hasHabilitation('HyperAdmin')) {
                $org = $this->_org;
            } else {
                $org = Atexo_CurrentUser::getOrganismAcronym();
            }
        }
        if (0 != Atexo_CurrentUser::getCurrentServiceId()) {
            $this->entiteValide = false;
        }

        self::detailOrg($org);
    }

    public function setCalledFrom($value)
    {
        $this->_calledFrom = $value;
    }

    public function getCalledFrom()
    {
        return $this->_calledFrom;
    }

    public function setOrg($value)
    {
        $this->_org = $value;
    }

    public function getOrg()
    {
        return $this->_org;
    }

    /**
     * setter de l'attribut codeAcheteur.
     *
     * @param $value
     *
     * @author Oussama SALMANI <oussama.salmani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function setCodeAchteur($value)
    {
        $this->_codeAcheteur = $value;
    }

    /**
     * getter de l'attribut codeAcheteur.
     *
     * @return la valeur de l'attribut codeAcheteur
     *
     * @author Oussama SALMANI <oussama.salmani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function getCodeAchteur()
    {
        return $this->_codeAcheteur;
    }

    /**
     * displayEA remplie la page de detail de l'organisme'.
     **/
    public function detailOrg($AcrconnexionOrg)
    {
        try {
            $EntitePurchaseBean = Atexo_Organismes::retrieveOrganismeByAcronyme($AcrconnexionOrg);
            if ($EntitePurchaseBean) {
                $this->denomination->Text = $EntitePurchaseBean->getDenominationOrg();
                $this->description->Text = $EntitePurchaseBean->getDescriptionOrg();
                $this->adresseprincipal->Text = $EntitePurchaseBean->getAdresse();
                $this->codepostal->Text = $EntitePurchaseBean->getCp();
                $this->ville->Text = $EntitePurchaseBean->getVille();
                $this->pays->Text = $EntitePurchaseBean->getPays();
                $this->mail->Text = $EntitePurchaseBean->getEmail();
                //$this->url->Text = $EntitePurchaseBean->getUrl();
                $this->siret->Text = $EntitePurchaseBean->getSiren().' '.$EntitePurchaseBean->getComplement();
                $this->idEntiteLabel->Text = $EntitePurchaseBean->getIdEntite();
                $this->sigle->Text = $EntitePurchaseBean->getSigle();
                $this->adressesecondaire->Text = $EntitePurchaseBean->getAdresse2();
                $this->tel->Text = $EntitePurchaseBean->getTel();
                $this->telecopie->Text = $EntitePurchaseBean->getTelecopie();
                $this->pays_ar->Text = $EntitePurchaseBean->getPaysAr();
                $this->ville_ar->Text = $EntitePurchaseBean->getVilleAr();
                $this->denomination_ar->Text = $EntitePurchaseBean->getDenominationOrgAr();
                $this->description_ar->Text = $EntitePurchaseBean->getDescriptionOrgAr();

                $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
                $typeAvis = CommonCategorieINSEEPeer::retrieveByPK($EntitePurchaseBean->getCategorieInsee(), $connexionCom);
                $this->categorie->Text = $typeAvis->getLibelle();

                $this->adresseprincipal_ar->Text = $EntitePurchaseBean->getAdresseAr();
                $this->adressesecondaire_ar->Text = $EntitePurchaseBean->getAdresse2Ar();
                $this->typeOrg->Text = ('1' == $EntitePurchaseBean->getActive()) ? Prado::localize('MODE_ACTIVE') : Prado::localize('MODE_NON_ACTIVE');
            }
        } catch (Exception $e) {
            Prado::log("Impossible de récuperer le detail de l'organisme''-".$e->__toString().'-', TLogger::FATAL, 'Fatal');
        }
    }

    public function RedirectUrlCreation()
    {
        $this->response->redirect('?page=Agent.CreationEntityPurchase&idParent=0'.'&org='.$this->_org);
    }

    public function RedirectUrlUpdate()
    {
        $this->response->redirect('?page=Agent.GestionOrganisme&org='.$this->_org);
    }
}
