<?php

namespace Application\Controls;

/*
 * Created on 11 juil. 2013
 *
 * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
 * @package
 */

class PanelMessageConfirmation extends MpeTPage
{
    public string $cssClass = 'message bloc-700';

    public function getCssClass()
    {
        return $this->cssClass;
    }

    public function setCssClass($value)
    {
        $this->cssClass = $value;
    }

    public function setMessage($message)
    {
        $this->labelMessage->Text = $message;
    }
}
