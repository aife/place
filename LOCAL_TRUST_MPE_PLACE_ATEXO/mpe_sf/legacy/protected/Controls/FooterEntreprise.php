<?php

namespace Application\Controls;

use App\Service\Footer\Footer;
use Application\Service\Atexo\Atexo_Util;

/**
 * Footer des pages coté Entreprise
 */
class FooterEntreprise extends MpeTPage
{
    public function getFooter()
    {
        $footerService = Atexo_Util::getSfService(Footer::class);

        return $footerService->getTemplate('mpe');
    }
}
