<!--Debut Side menu-->
<com:TPanel visible="<%=!(Application\Service\Atexo\Atexo_Config::getParameter('UTILISER_UTAH') && ($this->isConnected()|| Application\Service\Atexo\Atexo_Config::getParameter('UTILISER_UTAH_MODE_DECONNECTER')))%>">
	<div id="acces-tel" class="help-bar">
		<ul>
			<li style="display:<%=(Prado::localize('TEXT_FAQ') != 'TEXT_FAQ' && !Application\Service\Atexo\Atexo_Config::getParameter('UTILISER_UTAH') && ($this->getCalledFrom() == "entreprise" || $this->isConnected())) ? 'block':'none'%>;">
				<a href="<%=$this->getUrlFaq()%>" target="_blank" class="faq">
					<com:TTranslate>TEXT_FAQ</com:TTranslate>
				</a>
			</li>
			<li>
				<com:THyperLink
						NavigateUrl="<%=$this->getUrlAssistanceTelephonique()%>"
						Attributes.target="_blank"
						Attributes.title="<%=Prado::localize('ASSISTANCE_NOUVELLE_FENETRE')%>"
						cssClass="hotline">
					<span class="text">
						<com:TTranslate>DEFINE_TEXT_ASSISTANCE_TELEPHONIQUE</com:TTranslate> <span><%=$this->getNumeroAssistanceTelephonique()%></span>
					</span>
				</com:THyperLink>
			</li>
		</ul>
	</div>
</com:TPanel>
<!--Fin Side menu-->