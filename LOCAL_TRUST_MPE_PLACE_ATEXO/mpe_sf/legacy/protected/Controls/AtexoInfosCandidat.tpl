        <!--Debut Bloc Entreprise et etablissement-->
        <div class="form-field">
            <div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><span class="title"><com:TTranslate>TEXT_ENTREPRISE</com:TTranslate></span></div>
            <div class="content">
                <div class="spacer-mini"></div>
                <div class="line">
                    <div class="intitule-150 bold"><com:TTranslate>TEXT_RAISON_SOCIAL</com:TTranslate> :</div>
                    <div class="content-bloc content-bloc bloc-570"><com:TLabel text="<%=$this->getCandidat()->getNom()%>" /></div>
                </div>
                <div class="line">
                    <div class="intitule-150 bold"><com:TTranslate>TEXT_SIEGE_SOCIAL</com:TTranslate> :</div>
                    <div class="content-bloc content-bloc bloc-570"><com:TLabel text='<%=$this->getCandidat()->getAdresse() ." ".$this->getCandidat()->getAdresse2() .", ". $this->getCandidat()->getCodepostal() ." ". $this->getCandidat()->getVilleadresse()." - ".$this->getCandidat()->getPaysadresse() %>' /></div>
                </div>
                <div class="line">
                    <div class="intitule-150 bold"><com:TTranslate>CODE_APE_NAF_NACE</com:TTranslate> :</div>
                    <div class="content-bloc content-bloc bloc-570">
                        <com:TLabel text='<%=$this->getCandidat()->getCodeape() ." - " .$this->getCandidat()->getLibelleApe() %>' /> 
                    </div>
                </div>
                <div class="line">
                    <div class="intitule-150 bold"><com:TTranslate>TEXT_SIREN</com:TTranslate> :</div>
                    <div class="content-bloc"> <com:TLabel text='<%=$this->getCandidat()->getSiren() ." (" .$this->getCandidat()->getNicSiege().")" %>' />  </div>
                </div>
                <div class="line">
                    <div class="intitule-150 bold"><com:TTranslate>TEXT_FJ</com:TTranslate> :</div>
                    <div class="content-bloc"><com:TLabel text='<%=$this->getCandidat()->getFormejuridique () %>' /></div>
                </div>    
                <div class="line">
                    <div class="intitule-150 bold"><com:TTranslate>TEXT_CAPITAL_SICIAL</com:TTranslate> :</div>
                    <div class="content-bloc bloc-570"><com:TLabel text='<%=$this->getCandidat()->getCapitalSocial()%>' /> <com:TTranslate>TEXT_CURRENCY</com:TTranslate></div>
                </div>
                <div class="line">
                    <div class="intitule-150 bold line-height-normal"><com:TTranslate>TEXT_ETBLISSEMENT_AYANT_REPONDU</com:TTranslate> :</div>
                    <div class="content-bloc bloc-570"><com:TLabel text='<%=current($this->getCandidat()->getEtablissements())->getCodeEtablissement() ." - " . current($this->getCandidat()->getEtablissements())->getAdresse() ." " . current($this->getCandidat()->getEtablissements())->getAdresse2() .", " . current($this->getCandidat()->getEtablissements())->getCodePostal() .", " . current($this->getCandidat()->getEtablissements())->getVille() %>' /></div>
                </div>
                <div class="line">
	                <com:TRepeater ID="repeaterMandataire" >
						<prop:HeaderTemplate>
							<div class="intitule-150 bold"><com:TTranslate>TEXT_MANDATAIRES_SOCIAUX</com:TTranslate> :</div>
							<div class="content-bloc bloc-570">
						</prop:HeaderTemplate>
						<prop:ItemTemplate>
							<com:TLabel text='<%# Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($this->Data->getNom()) ." ".  Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($this->Data->getPrenom()) ." - ". $this->Data->getQualite() %>' />
							<br/>
						</prop:ItemTemplate>
						<prop:FooterTemplate>
							</div>
					    </prop:FooterTemplate>
					</com:TRepeater>
                </div>
                <div class="breaker"></div>
            </div>
            <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
        </div>
        <!--Fin Bloc Entreprise et etablissement-->