<div class="line">
	<div class="intitule-150 line-height-normal"><label for="certificat"><com:TTranslate>FICHIER_CERTIFICAT</com:TTranslate>
	<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" onmouseover="afficheBulle('infosCertificat', this)" onmouseout="cacheBulle('infosCertificat')" class="picto-info" alt="Info-bulle" title="Info-bulle" /></label></div>
	<com:AtexoFileUpload id="certificat" CssClass="file-580" Attributes.size="96" />
	<com:TCustomValidator
	    ValidationGroup="validateInfoAccount"
	    Enabled="<%=($this->getSelectionCertObligatoire() == true)? true:false%>"
		ControlToValidate="certificat"
	    ClientValidationFunction="verifierAjoutCertificat"
	    ErrorMessage="<%=Prado::localize('REGLE_VALIDATION_CERTIFICAT')%>"
	    Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
		 >
	 	<prop:ClientSide.OnValidationError>
    		 document.getElementById('divValidationSummary').style.display='';
    	</prop:ClientSide.OnValidationError>
    </com:TCustomValidator>
	<com:TCustomValidator
  		ControlToValidate="certificat" 
  		ValidationGroup="validateInfoAccount" 
  		Display="Dynamic" 
  		EnableClientScript="true"
  		ID="certificatCreationValidator"
  		onServerValidate="Page.authentificationByCer.checkCertificatFileValidity"
  		Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
	</com:TCustomValidator>
	<div id="infosCertificat" class="info-bulle" onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();"><div><com:TTranslate>FICHIER_CERTIFICAT_FORMAT_CER_ENCODE_64</com:TTranslate></div></div>
</div>