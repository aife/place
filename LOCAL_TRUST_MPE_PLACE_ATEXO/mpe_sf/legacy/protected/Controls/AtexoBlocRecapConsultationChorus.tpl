<!--Debut Bloc Recap Consultation -->
<div class="form-bloc margin-0">
    <div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
    <div class="content">
        <div class="recap-bloc">

            <div class="line" >
                <com:TRepeater ID="repeaterLots">
                    <prop:HeaderTemplate>
                        <div class="intitule-250"><strong><com:TTranslate>LOTS_CONCERNES</com:TTranslate> :</strong></div>
                        <div class="content-bloc bloc-500">
                    </prop:HeaderTemplate>
                    <prop:ItemTemplate>
                        <div><com:TTranslate>LOT</com:TTranslate> <com:TLabel ID="numLot" Text="<%#$this->Data->getLot()%>" /> - <com:TLabel ID="intituleLot" Text="<%#$this->Data->getDescription()%>" /></div>
                    </prop:ItemTemplate>
                    <prop:FooterTemplate>
            </div>
            </prop:FooterTemplate>
            </com:TRepeater>
        </div>
        <div class="line">
            <div class="intitule-250 bold"><com:TLabel Text="<%=$this->getLibelleTypeMarche()%>"/> :</div>
            <div class="content-bloc bloc-500"><com:TActiveLabel ID="labelReferenceContrat" /> | <com:TActiveLabel ID="labelObjetContrat" /></div>
        </div>
        <div class="line">
            <div class="intitule-250 bold"><com:TTranslate>TEXT_TITULAIRE_MARCHE</com:TTranslate></div>
            <div class="content-bloc bloc-500"><com:TActiveLabel ID="labelTitulaireContrat" /></div>
        </div>
        <div class="line">
            <div class="intitule-250 bold"><com:TTranslate>DATE_NOTIFICATION</com:TTranslate> : </div>
            <div class="content-bloc bloc-500"><com:TActiveLabel ID="labelDateNotification" /></div>
        </div>
        <div class="line">
            <div class="intitule-250 bold"><com:TLabel Text="<%=$this->getLibelleMontant()%>"/> : </div>
            <div class="content-bloc bloc-500"><com:TActiveLabel ID="labelMontant" /> <com:TTranslate>DEFINE_UNITE_HT</com:TTranslate></div>
        </div>
    </div>
    <div class="breaker"></div>
</div>
<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
</div>
<div class="spacer-small"></div>

<!--Fin Bloc Recap Consultation-->