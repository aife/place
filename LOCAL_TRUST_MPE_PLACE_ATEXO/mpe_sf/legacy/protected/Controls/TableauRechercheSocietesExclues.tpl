<!--Debut Bloc Recherche avancee--> 
<div class="form-field" style="display:block;">
	<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><span class="title"><com:TTranslate>TEXT_RECHERCHE_MULTI_CRITERES</com:TTranslate></span></div> 
	<div class="content"> 
		<div class="spacer-small"></div> 
		<!--Debut Ligne Libelle fournisseur Fr-->
		<div class="line"> 
			<div class="intitule-150"><label for="libelleFournisseurFr"><com:TTranslate>TEXT_RAISON_SOCIAL</com:TTranslate></label> :</div> 
			<com:TTextBox id="libelleFournisseurFr" Cssclass="long" Attributes.title="<%=Prado::localize('TEXT_LIBELLE_FR')%>" />
		</div> 
		<!--Fin Ligne Libelle fournisseur Fr--> 
		
		<div class="spacer-small"></div> 
		<!--Debut Ligne Recherche par date--> 
		<h4><com:TTranslate>DEFINE_RECHERCHE_PAR_DATE</com:TTranslate> </h4> 
		<!--Debut Bloc duree Exclusion--> 
		<div class="line"> 
			<div class="intitule-150"><com:TTranslate>TEXT_DUREE_EXCLUSION</com:TTranslate> :</div> 
			<div class="float-left"> 
				<!--Debut Bloc duree Exclusion Indifférent--> 
				<div class="clear-both"> 
					<span class="radio-bloc"> 
						<span class="radio">
    						<com:TRadioButton 
    							id="exclusion_indifferent" 
    							Attributes.title="<%=Prado::localize('TEXT_EXCLUSION_INDIFFERENTE')%>"
    							checked="true" 
    							GroupName="duree_exclusion" 
    						/>
						</span> 
					</span> 
					<div class="float-left"> 
						<div class="intitule-150"><label for="exclusion_indifferent"><com:TTranslate>TEXT_INDIFFERENT</com:TTranslate></label> </div> 
						<div class="intitule-auto"><label for="exclusion_indifferent_dateStart"><com:TTranslate>DATE_DEBUT</com:TTranslate> :</label></div> 
						<div class="calendar"> 
							<com:TTextBox 
    							Attributes.title="Entre le" 
    							id="exclusion_indifferent_dateStart" 
    							Cssclass="date" 
							/>
							<com:TImage 
    							ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-calendar.gif"
    							Attributes.alt="<%=Prado::localize('DEFINE_CALENDRIER')%>"
    							Attributes.title="<%=Prado::localize('DEFINE_CALENDRIER')%>"
    							Attributes.onclick="popUpCalendar(this,document.getElementById('ctl0_CONTENU_PAGE_tableauRechercheSocietesExclues_exclusion_indifferent_dateStart'),'dd/mm/yyyy','');"
							/>								
						</div> 
						<div class="intitule-auto indent-10"><label for="exclusion_indifferent_dateEnd"><com:TTranslate>DATE_FIN</com:TTranslate> : </label></div> 
						<div class="calendar"> 
							<com:TTextBox 
    							Attributes.title="Et le" 
    							id="exclusion_indifferent_dateEnd" 
    							Cssclass="date" 
							/>
							<com:TImage 
    							ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-calendar.gif"
    							Attributes.alt="<%=Prado::localize('DEFINE_CALENDRIER')%>"
    							Attributes.title="<%=Prado::localize('DEFINE_CALENDRIER')%>"
    							Attributes.onclick="popUpCalendar(this,document.getElementById('ctl0_CONTENU_PAGE_tableauRechercheSocietesExclues_exclusion_indifferent_dateEnd'),'dd/mm/yyyy','');"
							/>									
						</div> 
						<div class="info-aide-right"><com:TTranslate>DEFINE_FORMAT_DATE_SIMPLE</com:TTranslate></div> 
					</div> 
				</div> 
				<!--Fin Bloc duree Exclusion Indifférent--> 
				<!--Debut Bloc duree Exclusion definitive--> 
				<div class="clear-both"> 
					<span class="radio-bloc"> 
						<span class="radio">
						<com:TRadioButton 
							id="exclusion_definitive" 
							Attributes.title="<%=Prado::localize('TEXT_EXCLUSION_DEFINITIVE')%>"
							GroupName="duree_exclusion" 
						/>
						</span> 
					</span> 
					<div class="float-left"> 
						<div class="intitule-150"><label for="exclusion_definitive"><com:TTranslate>TEXT_EXCLUSION_DEFINITIVE</com:TTranslate></label> </div> 
						<div class="intitule-auto"><label for="critereFiltre_1_dateStart"><com:TTranslate>DATE_DEBUT</com:TTranslate> :</label></div> 
						<div class="calendar"> 
							<com:TTextBox 
    							Attributes.title="Entre le" 
    							id="exclusion_definitive_dateStart" 
    							Attributes.class="date" 
							/>
							<com:TImage 
    							ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-calendar.gif"
    							Attributes.alt="<%=Prado::localize('DEFINE_CALENDRIER')%>"
    							Attributes.title="<%=Prado::localize('DEFINE_CALENDRIER')%>"
    							Attributes.onclick="popUpCalendar(this,document.getElementById('ctl0_CONTENU_PAGE_tableauRechercheSocietesExclues_exclusion_definitive_dateStart'),'dd/mm/yyyy','');"
							/>									
						</div> 
						<div class="info-aide-right"><com:TTranslate>DEFINE_FORMAT_DATE_SIMPLE</com:TTranslate></div> 
					</div> 
				</div> 
				<!--Fin Bloc duree Exclusion definitive--> 
				<!--Debut Bloc duree Exclusion temporaire--> 
				<div class="clear-both"> 
					<span class="radio-bloc"> 
						<span class="radio">
						<com:TRadioButton 
							id="exclusion_temporaire" 
							Attributes.title="Exclusion temporaire" 
							GroupName="duree_exclusion" 
						/>
					</span> 
					</span> 
					<div class="float-left"> 
						<div class="intitule-150"><label for="exclusion_temporaire"><com:TTranslate>TEXT_EXCLUSION_TEMPORAIRE</com:TTranslate></label> </div> 
						<div class="intitule-auto"><label for="critereFiltre_1_dateStart"><com:TTranslate>DATE_DEBUT</com:TTranslate> :</label></div> 
						<div class="calendar"> 
							<com:TTextBox 
    							Attributes.title="Entre le" 
    							id="exclusion_temporaire_dateStart" 
    							Cssclass="date" 
							/>
							<com:TImage 
    							ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-calendar.gif"
    							Attributes.alt="<%=Prado::localize('DEFINE_CALENDRIER')%>"
    							Attributes.title="<%=Prado::localize('DEFINE_CALENDRIER')%>"
    							Attributes.onclick="popUpCalendar(this,document.getElementById('ctl0_CONTENU_PAGE_tableauRechercheSocietesExclues_exclusion_temporaire_dateStart'),'dd/mm/yyyy','');"
							/>								
						</div> 
						<div class="intitule-auto indent-10"><label for="critereFiltre_1_dateEnd"><com:TTranslate>DATE_FIN</com:TTranslate> : </label></div> 
						<div class="calendar"> 
							<com:TTextBox 
    							Attributes.title="Et le" 
    							id="exclusion_temporaire_dateEnd" 
    							Cssclass="date" 
							/>
							<com:TImage 
    							ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-calendar.gif"
    							Attributes.alt="<%=Prado::localize('DEFINE_CALENDRIER')%>"
    							Attributes.title="<%=Prado::localize('DEFINE_CALENDRIER')%>"
    							Attributes.onclick="popUpCalendar(this,document.getElementById('ctl0_CONTENU_PAGE_tableauRechercheSocietesExclues_exclusion_temporaire_dateEnd'),'dd/mm/yyyy','');"
							/>									
						</div> 
						<div class="info-aide-right"><com:TTranslate>DEFINE_FORMAT_DATE_SIMPLE</com:TTranslate></div> 
					</div> 
				</div> 
				<!--Fin Bloc duree Exclusion temporaire--> 
			</div> 
		</div> 
		<!--Fin Bloc duree Exclusion--> 
		<div class="spacer-small"></div> 
		
		<!--Debut Bloc Portee Exclusion--> 
				<div class="line"> 
					<div class="intitule-150"><com:TTranslate>TEXT_PORTEE_EXCLUSION</com:TTranslate> <com:TImage  ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" Attributes.onmouseover="afficheBulle('infosPorteeExclusion', this)" Attributes.onmouseout="cacheBulle('infosPorteeExclusion')" CssClass="picto-info-intitule" Attributes.alt="Info-bulle" Attributes.title="Info-bulle" /><div id="infosPorteeExclusion" class="info-bulle" onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();"><div><com:TTranslate>TEXT_INFO_BULLE_PORTEE_EXCLUSION</com:TTranslate></div></div>:</div>
					<div class="float-left"> 
						<!--Debut Bloc portee Indifférent--> 
						<div class="clear-both"> 
							<span class="radio-bloc"> 
								<span class="radio">
    								<com:TRadioButton 
        								GroupName="portee_exclusion" 
        								id="portee_indifferent"         								 
        								Attributes.title="Indifférent" 
        								Attributes.onclick="desactiver_entite();" 
        								checked="true" 
    								/>
								</span> 
							</span> 
							<div class="float-left"> 
								<div class="intitule-150"><label for="portee_indifferent"><com:TTranslate>TEXT_INDIFFERENT</com:TTranslate></label> </div> 
							</div> 
						</div> 
						<!--Fin Bloc portee Indifférent--> 
						<!--Debut Bloc portee totale--> 
						<div class="clear-both"> 
							<span class="radio-bloc"> 
								<span class="radio">
    								<com:TRadioButton 
        								GroupName="portee_exclusion" 
        								id="portee_totale" 
        								Attributes.title="<%=Prado::localize('TEXT_PORTEE_TOTALE')%>"
        								Attributes.onclick="desactiver_entite();" 
    								/>
								</span> 
							</span> 
							<div class="float-left"> 
								<div class="intitule-150"><label for="portee_totale"><com:TTranslate>TEXT_PORTEE_TOTALE</com:TTranslate></label> </div> 
							</div> 
						</div> 
						<!--Fin Bloc portee totale--> 
						<!--Debut Bloc portee partielle--> 
						<div class="clear-both"> 
							<span class="radio-bloc"> 
								<span class="radio">
    								<com:TRadioButton 
    									GroupName="portee_exclusion" 
        								id="portee_partielle" 
        								Attributes.type="radio" 
        								Attributes.title="<%=Prado::localize('TEXT_PORTEE_PARTIELLE')%>"
        								Attributes.onclick="activer_entite();" 
    								/>
								</span> 
							</span> 
							<div class="float-left"> 
								<div class="intitule-150"><label for="portee_partielle"><com:TTranslate>TEXT_PORTEE_PARTIELLE</com:TTranslate></label></div> 
								<com:TDropDownList  
    								id="entitePublique" 
    								Cssclass="bloc-400 disabled" 
    								Attributes.title="Entité publique" 
    								Attributes.disabled="disabled"> 
								</com:TDropDownList > 
							</div> 
						</div> 
						<!--Fin Bloc portee partielle--> 
					</div> 
				</div> 
				<div class="spacer-small"></div> 
				<!--Fin Bloc Portee Exclusion--> 
				
		<!--Debut Line bouton recherche multi-criteres--> 
		<div class="spacer"></div>
		<div class="boutons-line">
			<com:TButton
					OnClick="getUrlTableauRecherche"
					Cssclass="bouton-long-190 float-left"
					Attributes.title="<%=Prado::localize('DEFINE_EFFACER_CRITERE_RECHERCHE')%>"
					Text="<%=Prado::localize('DEFINE_EFFACER_CRITERE_RECHERCHE')%>"
			/>
			<com:TButton
				Cssclass="bouton-moyen-120 float-right"
				Attributes.title="<%=Prado::localize('DEFINE_LANCER_RECHERCHE')%>"
				Text="<%=Prado::localize('DEFINE_LANCER_RECHERCHE')%>"
				OnClick="lancerRecherche"
			/>
		</div> 
		<!--Fin Line bouton recherche multi-criteres--> 
		<div class="breaker"></div> 
	</div> 
	<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div> 
</div> 
<!--Fin Bloc Recherche avancee-->
