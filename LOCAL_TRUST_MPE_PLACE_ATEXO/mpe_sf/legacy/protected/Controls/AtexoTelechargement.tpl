		<com:PanelMessageInformation  visible = "true" id="panelInfo" cssClass="message bloc-700"/>
		<com:PanelMessageAvertissement visible = "false" id="panelAvertissement" cssClass="message bloc-700"/>
		<com:TActivePanel id="listeTelechargement" cssClass="form-field">
					<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
					<div class="content">
						<h3>Liste de téléchargements</h3>
						<div class="table-bloc">
							<com:TActiveRepeater ID="RepeaterArchiveTelechargement" >
						    <prop:HeaderTemplate>
								<table summary="Liste des téléchargements" class="table-results">
									<thead>
										<tr>
											<th colspan="9" class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></th>
										</tr>
										<tr>
											<th id="nomAgent" class="col-150">
												<%#$this->SourceTemplateControl->elementSortedWith('nomAgent','open')%>
													<com:TTranslate>TEXT_AGENT</com:TTranslate>
												<%#$this->SourceTemplateControl->elementSortedWith('nomAgent','close')%>
												<com:PictoSort CommandName="nomAgent" OnCommand="Page.atexoTelechargement.Trier" />
											</th>
											<th id="dateArchive" class="col-100"> 
												<%#$this->SourceTemplateControl->elementSortedWith('dateArchive','open')%>
													<com:TTranslate>DEFINE_TEXT_DATE_HEURE</com:TTranslate>
												<%#$this->SourceTemplateControl->elementSortedWith('dateArchive','close')%>
												<com:PictoSort CommandName="dateArchive" OnCommand="Page.atexoTelechargement.Trier" />
											</th>
											<th id="nomArchive" class="col-300">
												<%#$this->SourceTemplateControl->elementSortedWith('nomArchive','open')%>
													<com:TTranslate>DEFINE_NOM_FICHIER</com:TTranslate>
												<%#$this->SourceTemplateControl->elementSortedWith('nomArchive','close')%>
												<com:PictoSort CommandName="nomArchive" OnCommand="Page.atexoTelechargement.Trier" />
											</th>
											<th id="tailleArchive" class="col-60">
												<%#$this->SourceTemplateControl->elementSortedWith('tailleArchive','open')%>
													<com:TTranslate>TEXT_TAILLE</com:TTranslate>
												<%#$this->SourceTemplateControl->elementSortedWith('tailleArchive','close')%>
												<com:PictoSort CommandName="tailleArchive" OnCommand="Page.atexoTelechargement.Trier" />
											</th>
											<th id="telechargerArchive" class="actions"><com:TTranslate>DEFINE_TELECHARGER</com:TTranslate></th>
											<th id="suppArchive" class="actions"><com:TTranslate>DEFINE_SUPPRIMER</com:TTranslate></th>
										</tr>
									</thead>
							</prop:HeaderTemplate>
							<prop:ItemTemplate>
								<tr class="<%#(($this->ItemIndex%2==0)? '':'on')%>" >
									<td headers="nomAgent" class="col-150"><%#$this->Data->getNomPrenomAgent()%></td>
									<td headers="dateArchive" class="col-100"><%#$this->Data->getDateGeneration('d/m/Y H:i')%></td>
									<td headers="nomArchive" class="col-300"><%#$this->Data->getNomFichierTelechargement()%></td>
									<td headers="tailleArchive" class="col-60"><%#$this->Data->getTailleFichier()%> Ko</td>
									<td headers="telechargerArchive" class="actions">
										<com:THyperLink 
											id="linkDownloadArchive" 
											NavigateUrl="<%#$this->SourceTemplateControl->getUrlDownloadarchive($this->Data)%>"
										>
											<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-dl-dossier.gif"
												alt="<%=Prado::localize('DEFINE_TELECHARGER_ARCHIVE')%>" 
												title="<%=Prado::localize('DEFINE_TELECHARGER_ARCHIVE')%>" 
										 	/>
										</com:THyperLink>
									</td>
									<td headers="suppArchive" class="actions">
										<com:TActiveImageButton 
											id="supprimerTelechargement" 
											ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-supprimer-big.gif"
											Attributes.title="<%=Prado::localize('TEXT_SUPPRIME_ARCHIVE') . ' ' . $this->Data->getNomFichierTelechargement()%>"
											Attributes.alt="<%=Prado::localize('TEXT_SUPPRIME_ARCHIVE')%>"  
											Attributes.OnClick = "document.getElementById('<%#$this->SourceTemplateControl->idToDetele->getClientId()%>').value = '<%#$this->Data->getId()%>';openModal('modal-confirmation-suppression','popup-small2',this);"
										>
										</com:TActiveImageButton>
										
									</td>
								</tr>
							</prop:ItemTemplate>
							<prop:FooterTemplate>
								</table>					
							</prop:FooterTemplate>								 
						</com:TActiveRepeater>
					</div>
					<div class="breaker"></div>
				</div>
			<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
		</com:TActivePanel>
		<!--Fin Bloc Telechargements-->
		<div class="breaker"></div>
			</div>
		<div class="breaker"></div>
		<!--Debut Modal Confirmation suppression-->
		<com:TActiveHiddenField id="idToDetele"/>

		<div class="modal-confirmation-suppression" style="display:none;">
			<!--Debut Formulaire-->
			<div class="form-bloc">
				<div class="top"><span class="left"></span><span class="right"></span></div>
				<div class="content">
					<p class="center margin-0"><strong><com:TTranslate>TEXT_ALERTE_SUPPRESSION_TELECHARGEMENT</com:TTranslate>.</strong></p>
				</div>
				<div class="bottom"><span class="left"></span><span class="right"></span></div>
			</div>
			<!--Fin Formulaire-->
			<!--Debut line boutons-->
			<div class="boutons-line">
				<input type="button" 
			    	class="bouton-moyen float-left" 
			    	value="<%=Prado::localize('DEFINE_ANNULER')%>" 
			    	title="<%=Prado::localize('DEFINE_ANNULER')%>" 
			    	onclick="J('.modal-confirmation-suppression').dialog('close');return false;" 
		    	/>

				<com:TActiveButton 
					id="cofirmSupression" 
					Text = "<%=Prado::localize('TEXT_POURSUIVRE')%>"
			    	Attributes.title="<%=Prado::localize('TEXT_POURSUIVRE')%>" 
					Attributes.OnClick="J('.modal-confirmation-suppression').dialog('close');" 
					CssClass="bouton-moyen float-right msg-confirmation-envoi" 
					OnCommand="supprimerTelechargement" 
					CommandParameter="<%=$this->idToDetele->value%>"
					onCallBack="refreshListe" 
				 >
			   </com:TActiveButton >
				
			
			</div>
			<!--Fin line boutons-->
		</div>
		<!--Fin Modal Confirmation suppression-->
		</div>
