<?php

namespace Application\Controls;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonEchange;
use Application\Propel\Mpe\CommonEchangeDestinataire;
use Application\Propel\Mpe\CommonEchangePeer;
use Application\Propel\Mpe\CommonEchangePieceJointe;
use Application\Propel\Mpe\CommonEchangePieceJointePeer;
use Application\Propel\Mpe\CommonEchangeTypeMessage;
use Application\Propel\Mpe\CommonInscrit;
use Application\Propel\Mpe\CommonOrganisme;
use Application\Propel\Mpe\CommonPlateformeVirtuelle;
use Application\Propel\Mpe\CommonTComplementFormulaire;
use Application\Propel\Mpe\CommonTComplementFormulairePeer;
use Application\Propel\Mpe\CommonTDossierFormulaire;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_MpeSf;
use Application\Service\Atexo\Atexo_MultiDomaine;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Inscrit;
use Application\Service\Atexo\FormulaireSub\Atexo_FormulaireSub_Dossier;
use Application\Service\Atexo\Message\Atexo_Message_RelationEchange;
use Exception;
use Prado\Prado;

/**
 * Classe EnvoiCourrierElectronique pour la gestion des envois de mail pour les demandes de compléments.
 *
 * @author KONATE Oumar <oumar.konate@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 1.0
 *
 * @since MPE-4
 */
class TemplateEnvoiCourrierElectroniqueComplementsFormulaires extends MpeTPage
{
    public bool $_calledFrom = false;
    public $_refConsultation;
    public bool $idMsgModifCons = false;
    public $_org;
    public bool $_postBack = false;
    public $_forInvitationConcourir;
    public $_tailleMaxPj;
    public bool $_redirect = false;
    public $_mailOffre;
    public $_mailQuestion;
    public array $_idsComplement = [];

    public function setCalledFrom($value)
    {
        $this->_calledFrom = $value;
    }

    public function getCalledFrom()
    {
        return $this->_calledFrom;
    }

    public function setPostBack($value)
    {
        $this->_postBack = $value;
    }

    public function getPostBack()
    {
        return $this->_postBack;
    }

    public function setTailleMaxPj($value)
    {
        $this->_tailleMaxPj = $value;
    }

    public function getTailleMaxPj()
    {
        return $this->_tailleMaxPj;
    }

    public function setMailOffre($value)
    {
        $this->_mailOffre = $value;
    }

    public function getMailOffre()
    {
        return $this->_mailOffre;
    }

    public function setRedirect($value)
    {
        $this->_redirect = $value;
    }

    public function setMailQuestion($value)
    {
        $this->_mailQuestion = $value;
    }

    public function getMailQuestion()
    {
        return $this->_mailQuestion;
    }

    public function getRedirect()
    {
        return $this->_redirect;
    }

    public function setIdsComplement($value)
    {
        $this->_idsComplement = $value;
    }

    public function getIdsComplement()
    {
        return $this->_idsComplement;
    }

    public function onLoad($param)
    {
        $agentCon = null;
        $serviceCon = null;
        $this->_refConsultation = Atexo_Util::atexoHtmlEntities($_GET['id']);
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        if (0 == strcmp($this->getCalledFrom(), 'agent')) {//agent
            $this->_org = Atexo_CurrentUser::getCurrentOrganism();
            $serviceCon = Atexo_CurrentUser::getIdServiceAgentConnected();
            $agentCon = Atexo_CurrentUser::getIdAgentConnected();
        }
        //Chargement les ids demandes de complement
        if (isset($_GET['idsCompl'])) {
            $this->setIdsComplement(base64_decode($_GET['idsCompl']));
        }
        //ajouter un warning pour les consultations archivees
        $ConsultationO = (new Atexo_Consultation())->retrieveConsultation($this->_refConsultation, $this->_org);
        if ($ConsultationO->getIdEtatConsultation() == Atexo_Config::getParameter('ETAT_EN_ATTENTE_ARCHIVAGE')) {
            $msgAvertissement = '<span>'.Prado::localize('MSG_AVERTISSEMENT_MODIFS_NON_INTEGRES_CAR_CONS_ARCHIVEE').'</span><br/>';
            $this->panelMessageWarning->setVisible(true);
            $this->panelMessageWarning->setMessage($msgAvertissement);
        }
        $clotures = '';
        if ($ConsultationO->getDateFin() <= date('Y-m-d H:i:s')) {
            $clotures = '&clotures';
        }
        if (!$this->getPostBack()) {
            $this->remplirListeMessage();
            $organismeObj = Atexo_Organismes::retrieveOrganismeByAcronyme($this->_org);
            if ($organismeObj instanceof CommonOrganisme) {
                $expediteur = $organismeObj->getSigle().' - '.$organismeObj->getDenominationOrg();
            } else {
                $expediteur = '-';
            }
            $message = new CommonEchange();
            if (0 == strcmp($this->getCalledFrom(), 'agent')) {//agent
                if (isset($_GET['IdEchange'])) {
                    $message = Atexo_Message::retrieveMessageById(Atexo_Util::atexoHtmlEntities($_GET['IdEchange']), $this->_org);
                    $this->destinataires->Text = $message->getDestinataires();
                    $this->objet->Text = $message->getObjet();
                    $this->textMail->text = $message->getCorps();
                    $this->messageType->setSelectedValue($message->getIdTypeMessage());
                    if ($message->getOptionEnvoi() == Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_SIMPLE')) {
                        $this->courrierElectroniqueSimple->Checked = 1;
                    }
                    if ($message->getOptionEnvoi() == Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_AVEC_AR')) {
                        $this->courrierElectroniqueContenuIntegralAR->Checked = 1;
                    }
                    if ($message->getOptionEnvoi() == Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_AVEC_LIEN')) {
                        $this->courrierElectroniqueUniquementLienTelechargementObligatoire->Checked = 1;
                    }
                    if ($message->getOptionEnvoi() == Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_REPONSE_AUTORISEE')) {
                        $this->courrierElectroniqueDeDemandeDeComplement->Checked = 1;
                    }
                    $this->setViewState('idMessage', Atexo_Util::atexoHtmlEntities($_GET['IdEchange']));
                } elseif (isset($_GET['IdInscrit'])) {
                    if (strstr($this->getIdsComplement(), '#')) {
                        $arrayIdsComplementsInscrits = $this->getArrayIdsComplementsInscrits();
                        if (is_array($arrayIdsComplementsInscrits) && count($arrayIdsComplementsInscrits)) {
                            $email = '';
                            $idComplement = '';
                            foreach ($arrayIdsComplementsInscrits as $idComplement => $idInscrit) {
                                $inscrit = (new Atexo_Entreprise_Inscrit())->retrieveInscritById($idInscrit);
                                if ($inscrit instanceof CommonInscrit) {
                                    $email .= trim($inscrit->getEmail()).',';
                                }
                            }
                            $this->destinataires->Text = $email;
                        }
                    } else {
                        $objectInscrit = (new Atexo_Entreprise_Inscrit())->retrieveInscritById(Atexo_Util::atexoHtmlEntities($_GET['IdInscrit']));
                        if ($objectInscrit) {
                            $this->destinataires->Text = trim($objectInscrit->getEmail());
                        }
                    }
                }
                if ($this->idMsgModifCons && !$_GET['IdEchange']) {
                    $this->remplirObjetEtCorpsMessage($this->_org, isset($_GET['invitation']), $clotures);
                }
                $message = $this->setInfoMessage($message, $agentCon, $expediteur, Atexo_Config::getParameter('PF_MAIL_FROM'), $serviceCon, $this->_org);
                if ($message->save($connexionCom)) {
                    $this->setViewState('idMessage', $message->getId());
                }
            }
            $message->setPageSource(Atexo_Util::atexoHtmlEntities($_GET['page']));
            $message->save($connexionCom);
        }
        $PJ = [];
        $c = new Criteria();
        $objetMessage = Atexo_Message::retrieveMessageById($this->getViewState('idMessage'), $this->_org);
        if ($objetMessage) {
            $c->add(CommonEchangePieceJointePeer::ORGANISME, $this->_org);
            $PJ = $objetMessage->getCommonEchangePieceJointes($c, $connexionCom);
        }
        $this->remplirTableauPJ($PJ);
    }

    public function getCorpsAR($echangeDest, $objetMessage)
    {
        $string = '';
        if ($objetMessage) {
            $string = '-----'.Prado::localize('DEFINE_TEXT_MSG_ORIGINE').'-----';
            $string .= '
';
            $string .= Prado::localize('TEXT_DE').trim($objetMessage->getEmailExpediteur());
            $string .= '
';
            $string .= Prado::localize('DEFINE_TEXT_ENVOYE').Atexo_Util::iso2frnDateTime($objetMessage->getDateMessage());
            $string .= '
';
            $string .= Prado::localize('TEXT_A').trim($echangeDest->getMailDestinataire());
            $string .= '
';
            $string .= Prado::localize('DEFINE_TEXT_OBJET').$objetMessage->getObjet();
            $string .= '
';
            $string .= Prado::localize('TEXT_CORPS').$objetMessage->getCorps();
        }

        return $string;
    }

    public function refreshText($sender, $param)
    {
        $pfUrl = Atexo_MultiDomaine::replaceDomain(
            Atexo_Config::getParameter('PF_URL_MESSAGERIE'),
            Atexo_Config::getParameter('PF_ACTIVE_MULTI_DOMAINE')
        );
        // Rafraichissement du ActiveTextBox
        $string = '';
        $IdTypeMessage = $sender->getSelectedValue();
        $objetMessage = Atexo_Message::retrieveTypeMessageById($IdTypeMessage);

        if (0 != $IdTypeMessage) {
            $this->objet->Text = Atexo_Util::toUtf8($objetMessage->getLibelle());
            if (isset($_GET['id'])) {
                $string = (new Atexo_Message())->getInfoConsultation(Atexo_Util::atexoHtmlEntities($_GET['id']), $this->_org);
            }
            $consultation = (new Atexo_Consultation())->retrieveConsultation(Atexo_Util::atexoHtmlEntities($_GET['id']), $this->_org);
            $string .= '
';
            if ($consultation->getTypeAcces() == Atexo_Config::getParameter('TYPE_PROCEDURE_PUBLICITE')) {
                $string .= Prado::localize('DEFINE_ACCES_DIRECT').' : '.
                     $pfUrl.'?page=Entreprise.EntrepriseDetailConsultation&id='.
                     Atexo_Util::atexoHtmlEntities($_GET['id']).'&orgAcronyme='.$this->_org;
                $string .= '
';
            } elseif ($consultation->getTypeAcces() == Atexo_Config::getParameter('TYPE_PROCEDURE_RESTREINTE')) {
                $string .= Prado::localize('DEFINE_ACCES_DIRECT').' : '.$pfUrl.
                     '?page=Entreprise.EntrepriseDetailConsultation&id='.
                     Atexo_Util::atexoHtmlEntities($_GET['id']).'&orgAcronyme='.$this->_org.'&code='.$consultation->getCodeProcedure();
                $string .= '
';
            }
            if ($objetMessage instanceof CommonEchangeTypeMessage) {
                $string .= '
';
                $string .= $objetMessage->getCorps();
            }

            $placeMarchePublicInterministere = Prado::localize('PLACE_MARCHE_PUBLIC_INTERMINISTERE');
            if ($consultation && $consultation->getCommonPlateformeVirtuelle() instanceof CommonPlateformeVirtuelle) {
                $plateformeVirtuelle = $consultation->getCommonPlateformeVirtuelle();
                $placeMarchePublicInterministere = $plateformeVirtuelle->getFooterMail();
            }

            $string .= (new Atexo_Message())->getBasPage($placeMarchePublicInterministere);
            $this->textMail->Text = Atexo_Util::toUtf8($string);
        } else {
            $this->objet->Text = '';
            $this->textMail->Text = '';
        }
    }

    public function onDeleteClick($sender, $param)
    {
        $idpj = $param->CommandParameter;
        Atexo_Message::DeletePj($idpj, $this->_org);
    }

    public function refreshRepeaterPJ($sender, $param)
    {
        //Raffraichissement du tableau PJ:
        $objetMessage = Atexo_Message::retrieveMessageById($this->getViewState('idMessage'), $this->_org);
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $PJ = [];
        if ($objetMessage) {
            $c->add(CommonEchangePieceJointePeer::ORGANISME, $this->_org);
            $PJ = $objetMessage->getCommonEchangePieceJointes($c, $connexionCom);
        }
        $this->remplirTableauPJ($PJ);
        $this->PanelPJ->render($param->NewWriter);
    }

    public function remplirTableauPJ($data)
    {
        $this->RepeaterPiecesJointes->dataSource = $data;
        $this->RepeaterPiecesJointes->dataBind();
    }

    public function remplirListeMessage()
    {
        $dataMessageType = [];
        $resultCriteria = Atexo_Message::getAllTypeMessages();
        $dataMessageType[0] = Prado::localize('TEXT_SELECTIONNER').'...';
        if (is_array($resultCriteria)) {
            foreach ($resultCriteria as $result) {
                $dataMessageType[$result->getId()] = $result->getLibelle();
            }
        }
        $this->messageType->dataSource = $dataMessageType;
        if ($this->idMsgModifCons && !$_GET['IdEchange']) {
            $this->messageType->SelectedValue = $this->idMsgModifCons;
        }
        $this->messageType->dataBind();
    }

    public function setInfoMessage($message, $agentCon, $expediteur, $emailExpediteur, $idService, $org)
    {
        $message->setIdCreateur($agentCon);
        $message->setStatus(Atexo_Config::getParameter('STATUS_ECHANGE_BROUILLON'));
        if (isset($_GET['id'])) {
            $message->setConsultationId(Atexo_Util::atexoHtmlEntities($_GET['id']));
        }
        $message->setExpediteur($expediteur);
        $message->setEmailExpediteur(trim($emailExpediteur));
        $message->setFormat(Atexo_Config::getParameter('ECHANGE_PLATE_FORME'));
        $message->setServiceId($idService);
        $message->setOrganisme($org);

        return $message;
    }

    /**
     * Permet de remplir l'objet et le corps du mail.
     *
     * @param string $org:       organisme
     * @param string $invitation
     * @param string $clotures
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function remplirObjetEtCorpsMessage($org, $invitation = null, $clotures = '')
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $res = (new Atexo_Message())->remplirObjetEtCorpsMessage($org, Atexo_Util::atexoHtmlEntities($_GET['id']), $this->idMsgModifCons, $invitation);
        $objetMail = Prado::localize('DEFINE_OBJET_COURRIER_DEMANDE_COMPLEMENT');
        $corpsMail = $res['corps'];
        //Début lien d'accès à la consultation cloturées
        $urlAccesDossier = Atexo_MpeSf::getUrlDetailsConsultation($this->_refConsultation, 'orgAcronyme='.$this->_org.'&depots'.$clotures);
        $corpsMail = str_replace('[__LIEN_ACCES_PAGE_REOUVERTURE_]', $urlAccesDossier, $corpsMail);
        //Fin lien d'accès à la consultation cloturées
        if ($this->getIdsComplement() && !strstr($this->getIdsComplement(), '#')) {
            if (strstr($this->getIdsComplement(), '_')) {
                $arrayIdsComplementsInscrits = explode('_', $this->getIdsComplement());
                if (is_array($arrayIdsComplementsInscrits) && count($arrayIdsComplementsInscrits)) {
                    $idComplement = $arrayIdsComplementsInscrits[0];
                    $complement = CommonTComplementFormulairePeer::retrieveByPK($idComplement, $connexionCom);
                    if ($complement instanceof CommonTComplementFormulaire) {
                        $corpsMail = str_replace('[__DATE_REMISE_DOSSIER_REOUVERTURE_]', Atexo_Util::iso2frnDateTime($complement->getDateARemettre(), true), $corpsMail);
                        $corpsMail = str_replace('[__PRECISIONS_COMPLEMENTS_SUB_SUB__]', $complement->getCommentaire(), $corpsMail);
                        $corpsMail = str_replace('[__MOTIF_REOUVERTURE__]', $complement->getMotif(), $corpsMail);
                        $objetMail = str_replace('[__MOTIF_REOUVERTURE__]', $complement->getMotif(), $objetMail);
                        $objetMail = str_replace('[MAIL_PREFIXE_OBJET]', Atexo_Config::getParameter('PF_SHORT_NAME'), $objetMail);
                        $dossier = (new Atexo_FormulaireSub_Dossier())->getDossierFormulaireById($complement->getIdDossierFormulaire());
                        if ($dossier instanceof CommonTDossierFormulaire) {
                            $corpsMail = str_replace('[__NUM_DOSSIER_SUB__]', $dossier->getReferenceDossierSub(), $corpsMail);
                        }
                    }
                }
            }
        }
        $this->objet->Text = $objetMail;
        $this->textMail->Text = $corpsMail;
    }

    /**
     * Fonction permettant d'envoyer le mail.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function envoyer()
    {
        try {
            if (!$this->validerTaillePj()) {
                $this->messageErreur->setVisible(true);
                $this->messageErreur->setMessage(Prado::localize('ERREUR_TAILLE_FICHIER_PJ'));
            } else {
                $echangeParDefaut = Atexo_Message::retrieveMessageById($this->getViewState('idMessage'), $this->_org);
                if ($echangeParDefaut instanceof CommonEchange) {
                    $maildestinataires = explode(',', $this->destinataires->Text);
                    if (is_array($maildestinataires) && 0 != count($maildestinataires)) {
                        $arrayIdsComplementsInscrits = $this->getArrayIdsComplementsInscrits();
                        $objetMessage = null;
                        $echangedestinataire = null;
                        foreach ($maildestinataires as $emailDest) {
                            $objetMessage = new CommonEchange();
                            $echangedestinataire = new CommonEchangeDestinataire();
                            if ($this->getOptionsMessage() == Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_SIMPLE')) {
                                $echangedestinataire->setTypeAr(Atexo_Config::getParameter('ID_TYPE_AR_SANS_OBJET'));
                            } else {
                                $echangedestinataire->setTypeAr(Atexo_Config::getParameter('ID_TYPE_AR_NON_RETIRE'));
                            }
                            $echangedestinataire->setOrganisme($this->_org);
                            $echangedestinataire->setUid((new Atexo_Message())->getUniqueId());
                            $echangedestinataire->setMailDestinataire(trim($emailDest));
                            $objetMessage->addCommonEchangeDestinataire($echangedestinataire);
                            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                            $Pjs = (new Atexo_Message())->recupererPjEchange($echangeParDefaut);
                            if (is_array($Pjs) && count($Pjs)) {
                                foreach ($Pjs as $pj) {
                                    $copiePj = null;
                                    if ($pj instanceof CommonEchangePieceJointe) {
                                        $copiePj = $pj->copy(true);
                                        $objetMessage->addCommonEchangePieceJointe($copiePj);
                                    }
                                }
                            }
                            $this->remplirInfosMessage($objetMessage, $echangeParDefaut);
                            if ($echangedestinataire->getMailDestinataire()) {
                                $objetMessage->save($connexion);
                                if ($this->messageType->getSelectedValue() == Atexo_Config::getParameter('ID_MESSAGE_COURRIER_DEMANDE_COMPLEMENT') && isset($_GET['idsCompl'])) {
                                    $inscrit = (new Atexo_Entreprise_Inscrit())->retrieveInscritByMail($emailDest);
                                    if ($inscrit instanceof CommonInscrit) {
                                        $idComplement = array_search($inscrit->getId(), $arrayIdsComplementsInscrits);
                                        $typeRelation = Atexo_Config::getParameter('TYPE_RELATION_ECHANGE_COMPLEMENT_FORMULAIRE');
                                        (new Atexo_Message_RelationEchange())->save($objetMessage->getId(), Atexo_Util::atexoHtmlEntities($idComplement), $typeRelation);
                                        unset($arrayIdsComplementsInscrits[$idComplement]);
                                    }
                                }
                                $idEchange = Atexo_Message::EnvoyerEmail($objetMessage, $this->_org, $this->getCalledFrom(), true);
                                if (!$idEchange) {
                                    $logger = Atexo_LoggerManager::getLogger('app');
                                    $logger->error("Erreur lors de l'envoi du mail de la demande de compléments (AOF) aux OF: \nObjet Echange= ".print_r($objetMessage, true)." \nOrganisme = ".$this->_org."\n");
                                }
                            }
                        }
                    }
                }
                $this->response->redirect('?page=Agent.TableauDeBordDemandesComplements&id='.Atexo_Util::atexoHtmlEntities($_GET['id']));
            }
        } catch (Exception $ex) {
            $logger = Atexo_LoggerManager::getLogger('app');
            $logger->error("Erreur lors de l'envoi du mail de la demande de compléments (AOF) aux OF. \nMessage Erreur: ".print_r($ex->getMessage(), true));
        }
    }

    public function RedirectReturn()
    {
        if (self::getRedirect()) {
            $this->response->redirect(self::getRedirect());
        } else {
            $this->response->redirect('?page=Agent.TableauDeBordDemandesComplements&id='.Atexo_Util::atexoHtmlEntities($_GET['id']));
        }
    }

    public function setForInvitationConcourir($valeur)
    {
        $this->_forInvitationConcourir = $valeur;
    }

    public function getForInvitationConcourir()
    {
        return $this->_forInvitationConcourir;
    }

    public function validerTaillePj()
    {
        if ($this->_tailleMaxPj) {
            $tailleMaxPj = $this->_tailleMaxPj;
        } else {
            $tailleMaxPj = Atexo_Config::getParameter('MAX_FILE_SIZE_IN_MAIL');
        }
        $limitationTailleFichier = false;
        if ($this->courrierElectroniqueSimple->Checked) {
            $limitationTailleFichier = true;
        }
        if ($this->courrierElectroniqueContenuIntegralAR->Checked) {
            $limitationTailleFichier = true;
        }
        if ($limitationTailleFichier) {
            $pjs = Atexo_Message::getPjByIdEchange($this->getViewState('idMessage'), $this->_org);
            if ($pjs && (is_countable($pjs) ? count($pjs) : 0)) {
                $taille = 0;
                foreach ($pjs as $pj) {
                    if ($pj->getTaille() > $tailleMaxPj) {
                        return false;
                    }
                    $taille += $pj->getTaille();
                    if ($taille > $tailleMaxPj) {
                        return false;
                    }
                }

                return true;
            }
        }

        return true;
    }

    public function editerDestinatairesMail($sender, $param)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $echange = CommonEchangePeer::retrieveByPK($this->getViewState('idMessage'), $connexionCom);
        $echange->setObjet($this->objet->Text);
        $echange->setCorps($this->textMail->text);
        $echange->setIdTypeMessage($this->messageType->getSelectedValue());
        $echange->setDestinataires($this->destinataires->Text);

        if ($this->courrierElectroniqueSimple->Checked) {
            $echange->setOptionEnvoi(Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_SIMPLE'));
        }
        if ($this->courrierElectroniqueContenuIntegralAR->Checked) {
            $echange->setOptionEnvoi(Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_AVEC_AR'));
        }
        if ($this->courrierElectroniqueUniquementLienTelechargementObligatoire->Checked) {
            $echange->setOptionEnvoi(Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_AVEC_LIEN'));
        }
        if ($this->courrierElectroniqueDeDemandeDeComplement->Checked) {
            $echange->setOptionEnvoi(Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_REPONSE_AUTORISEE'));
        }
        if (('1' == $this->getForInvitationConcourir() || '1' == $this->getMailOffre() || '1' == $this->getMailQuestion()) && !$_GET['IdEchange']) {
            $echange->setDestinatairesLibres($this->destinataires->Text);
        }
        //Sauvegarde de l'url de la page
        $urlPage = strstr($_SERVER['REQUEST_URI'], '?');
        $echange->setPageSource($urlPage);
        $echange->save($connexionCom);
        $this->response->redirect('index.php?page=Agent.EnvoiCourrierElectroniqueChoixDestinataire&id='.$this->_refConsultation.'&forInvitationConcourir='
        .$this->getForInvitationConcourir().'&IdEchange='.$this->getViewState('idMessage'));
    }

    /**
     * Permet de construire un tableau pour faire la correspondance entre id_complement et id_inscrit
     * Clé du tableau = id_complément, valeur = id_inscrit.
     *
     * @return: le tableau de correspondance
     */
    public function getArrayIdsComplementsInscrits()
    {
        //Chargement du tableau de correspondance id_complement=>id_inscrit
        $arrayCorrespondance = [];
        if ($this->getIdsComplement() && strstr($this->getIdsComplement(), '#')) {
            $arrayCompl1 = explode('#', $this->getIdsComplement());
            if (is_array($arrayCompl1) && count($arrayCompl1)) {
                foreach ($arrayCompl1 as $oneCompl1) {
                    if ($oneCompl1) {
                        $arrayCompl2 = explode('_', $oneCompl1);
                        $arrayCorrespondance[$arrayCompl2[0]] = $arrayCompl2[1];
                    }
                }
            }
        } elseif (strstr($this->getIdsComplement(), '_')) {
            $arrayCompl2 = explode('_', $this->getIdsComplement());
            $arrayCorrespondance[$arrayCompl2[0]] = $arrayCompl2[1];
        }
        //print_r( $arrayCorrespondance);exit;
        return $arrayCorrespondance;
    }

    /**
     * Permet de copier les informations de l'échange par défaut dans l'echange spécifié.
     *
     * @param CommonEchange $echange:          echange spécifié
     * @param CommonEchange $echangeParDefaut: echange par defaut à copier
     */
    public function copierInfosFromEchangeParDefaut(&$echange, $echangeParDefaut)
    {
        if ($echange instanceof CommonEchange && $echangeParDefaut instanceof CommonEchange) {
            $echange->setIdCreateur($echangeParDefaut->getIdCreateur());
            $echange->setExpediteur($echangeParDefaut->getExpediteur());
            $echange->setEmailExpediteur(trim($echangeParDefaut->getEmailExpediteur()));
            $echange->setFormat($echangeParDefaut->getFormat());
            $echange->setServiceId($echangeParDefaut->getServiceId());
            $echange->setOrganisme($echangeParDefaut->getOrganisme());
            $echange->setStatus($echangeParDefaut->getStatus());
            $echange->setConsultationId($echangeParDefaut->getConsultationId());
        }
    }

    /**
     * Permet de recuperer le corps du message se trouvant dans l'interface.
     *
     * @return: le corps du message.
     */
    public function getCorpsMessage()
    {
        $corps = $this->textMail->text;
        if ($this->corpsAR->text) {
            $corps .= '
			 '.$this->corpsAR->text;
        }

        return $corps;
    }

    /**
     * Permet de determiner l'option du message selectionné par l'utilisateur.
     *
     * @return: option du message selectionné
     */
    public function getOptionsMessage()
    {
        if ($this->courrierElectroniqueSimple->Checked) {
            return Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_SIMPLE');
        }
        if ($this->courrierElectroniqueContenuIntegralAR->Checked) {
            return Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_AVEC_AR');
        }
        if ($this->courrierElectroniqueUniquementLienTelechargementObligatoire->Checked) {
            return Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_AVEC_LIEN');
        }
        if ($this->courrierElectroniqueDeDemandeDeComplement->Checked) {
            return Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_REPONSE_AUTORISEE');
        }
    }

    /**
     * Permet de remplir les infos du message.
     *
     * @param CommonEchange $echange:          message à remplir
     * @param CommonEchange $echangeParDefaut: message par défaut
     */
    public function remplirInfosMessage(&$echange, $echangeParDefaut)
    {
        if ($echange instanceof CommonEchange) {
            $echange->setOrganisme($this->_org);
            $echange->setCorps($this->getCorpsMessage());
            $echange->setObjet($this->objet->Text);
            $echange->setIdTypeMessage($this->messageType->getSelectedValue());
            $echange->setOptionEnvoi($this->getOptionsMessage());
            $this->copierInfosFromEchangeParDefaut($echange, $echangeParDefaut);
        }
    }

    /**
     * Permet de formatter le nom du fichier.
     *
     * @param CommonEchangePieceJointe $file : objet fichier
     *
     * @return string : nom du fichier
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function formaterNomFichierPj($file)
    {
        if ($file instanceof CommonEchangePieceJointe) {
            return $file->getNomFichierFormate();
        }
    }
}
