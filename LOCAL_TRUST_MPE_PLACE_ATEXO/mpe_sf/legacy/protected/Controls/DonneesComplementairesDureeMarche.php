<?php

namespace Application\Controls;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonTDonneeComplementaire;
use Application\Propel\Mpe\CommonValeurReferentiel;
use Application\Propel\Mpe\CommonValeurReferentielPeer;
use Application\Service\Atexo\Atexo_CommonReferentiel;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;

class DonneesComplementairesDureeMarche extends MpeTTemplateControl
{
    private ?CommonConsultation $consultation = null;
    private $mode;
    private $_donneeComplementaire;
    private $_connexion;
    // $sourcePage = ('etapeIdentification' si on est sur l'étape "identification")
    // $sourcePage = ('detailLots' si on est sur l'étape "detailLots")
    private ?string $sourcePage = null;
    private bool $afficherMarcheReconductible = true;
    private ?string $idElement = null;

    /**
     * recupère la valeur.
     */
    public function getIdElement()
    {
        return $this->idElement;
    }

    /**
     * Affecte la valeur.
     *
     * @param string $value
     */
    public function setIdElement($value)
    {
        $this->idElement = $value;
    }

    public function setAfficherMarcheReconductible($afficher)
    {
        $this->afficherMarcheReconductible = $afficher;
    }

    public function getAfficherMarcheReconductible()
    {
        return $this->afficherMarcheReconductible;
    }

    /**
     * Affecte la valeur.
     *
     * @param string $value
     */
    public function setMode($mode)
    {
        $this->mode = $mode;
    }

    /**
     * recupère la valeur.
     */
    public function getMode()
    {
        return $this->mode;
    }

    /**
     * recupère la valeur.
     */
    public function getSourcePage()
    {
        return $this->sourcePage;
    }

    // getSourcePage()

    /**
     * Affecte la valeur.
     *
     * @param string $value
     */
    public function setSourcePage($value)
    {
        if ($this->sourcePage !== $value) {
            $this->sourcePage = $value;
        }
    }

    // setSourcePage()

    public function getDonneeComplementaire()
    {
        return $this->_donneeComplementaire;
    }

    public function setDonneeComplementaire($donneeComplementaire)
    {
        $this->_donneeComplementaire = $donneeComplementaire;
    }

    public function getConnexion()
    {
        return $this->_connexion;
    }

    public function setConnexion($connexion)
    {
        $this->_connexion = $connexion;
    }

    public function setConsultation(?CommonConsultation $consultation)
    {
        $this->consultation = $consultation;
    }

    public function getConsultation(): ?CommonConsultation
    {
        if (!($this->consultation instanceof CommonConsultation)) {
            $this->consultation = $this->Page->getConsultation();
        }

        return $this->consultation;
    }

    public function onLoad($param)
    {
        if (Atexo_Module::isEnabled('DonneesComplementaires') && Atexo_Module::isEnabled('DonneesRedac')) {
            self::chargerComposant();
        }
    }

    /*
     * remplir liste choix duree delai marche
     */
    public function fillDureeDelai()
    {
        $dataSource = [];
        $dataSource['divSelectionner'] = (new Atexo_Config())->toPfEncoding(Prado::localize('TEXT_SELECTIONNER').'...');
        $dataDureeDelai = (new Atexo_CommonReferentiel())->retrieveValeurReferentiel(Atexo_Config::getParameter('REFERENTIEL_DUREE_DELAI'));
        if ($dataDureeDelai) {
            foreach ($dataDureeDelai as $oneVal) {
                $dataSource[$oneVal->getLibelle2().$this->getIdPanel()] = $oneVal->getLibelleValeurReferentiel();
            }
        }
        $this->dureeDelai->DataSource = $dataSource;
        $this->dureeDelai->DataBind();
    }

    /*
     * remplir liste unite duree marche
     */
    public function fillUniteDuree()
    {
        $dataSource = [];
        $dataUniteDuree = (new Atexo_CommonReferentiel())->retrieveValeurReferentiel(Atexo_Config::getParameter('REFERENTIEL_UNITE_DUREE_MARCHE'));
        if ($dataUniteDuree) {
            foreach ($dataUniteDuree as $oneVal) {
                $dataSource[$oneVal->getLibelle2()] = $oneVal->getLibelleValeurReferentiel();
            }
        }
        $this->dureeJourMois->DataSource = $dataSource;
        $this->dureeJourMois->DataBind();
    }

    public function saveDureeMarche(&$donneComplementaire)
    {
        $this->_donneeComplementaire = $donneComplementaire;
        $idDureeDelaiMarche = self::deleteIdPanelFromeValue($this->dureeDelai->getSelectedValue());
        $valeurRef = (new Atexo_CommonReferentiel())->retrieveValeurReferentielByLibelle2(Atexo_Config::getParameter('REFERENTIEL_DUREE_DELAI'), $idDureeDelaiMarche);
        if ($valeurRef instanceof CommonValeurReferentiel) {
            $this->_donneeComplementaire->setIdDureeDelaiDescription($valeurRef->getId());
        } else {
            $this->_donneeComplementaire->setIdDureeDelaiDescription(0);
        }
        switch ($idDureeDelaiMarche) {
            case 'DUREE_MARCHEE':
                self::saveDuree();
                break;
            case 'DELAI_EXECUTION':
                self::saveDelai();
                break;
            case 'DESCRIPTION_LIBRE':
                self::saveDescLibre();
                break;
            default:
                self::viderDureeDelai();
                break;
        }
        if ($this->marcheReconductible->Checked) {
            $this->_donneeComplementaire->setReconductible(1);
            $this->_donneeComplementaire->setNombreReconductions($this->nbReconduction->Text);
            $this->_donneeComplementaire->setModalitesReconduction((new Atexo_Config())->toPfEncoding($this->modalitesReconduction->Text));
        } else {
            $this->_donneeComplementaire->setReconductible(0);
            $this->_donneeComplementaire->setNombreReconductions(null);
            $this->_donneeComplementaire->setModalitesReconduction(null);
        }
        //      print_r($this->_donneeComplementaire);
    }

    public function viderDureeDelai()
    {
        self::viderDuree();
        self::viderDelai();
        self::viderDescLibre();
    }

    public function saveDuree()
    {
        $valeurRef = (new Atexo_CommonReferentiel())->retrieveValeurReferentielByLibelle2(Atexo_Config::getParameter('REFERENTIEL_UNITE_DUREE_MARCHE'), $this->dureeJourMois->getSelectedValue());
        $this->_donneeComplementaire->setIdChoixMoisJour($valeurRef->getId());
        $this->_donneeComplementaire->setDureeMarche($this->duree->Text);
    }

    public function saveDelai()
    {
        $this->_donneeComplementaire->setDureeDateDebut(Atexo_Util::frnDate2iso($this->dateNotif->Text));
        $this->_donneeComplementaire->setDureeDateFin(Atexo_Util::frnDate2iso($this->dateNotifJusquau->Text));
    }

    public function saveDescLibre()
    {
        $this->_donneeComplementaire->setDureeDescription((new Atexo_Config())->toPfEncoding($this->delaiLibre->Text));
    }

    public function populateDuree()
    {
        $valeurRef = CommonValeurReferentielPeer::retrieveByPK($this->_donneeComplementaire->getIdChoixMoisJour(), Atexo_Config::getParameter('REFERENTIEL_UNITE_DUREE_MARCHE'), $this->_connexion);
        $this->dureeJourMois->setSelectedValue($valeurRef->getLibelle2());
        $this->duree->Text = $this->_donneeComplementaire->getDureeMarche();
        self::viderDelai();
        self::viderDescLibre();
    }

    public function populateDelai()
    {
        $this->dateNotif->Text = Atexo_Util::iso2frnDate($this->_donneeComplementaire->getDureeDateDebut('Y-m-d'));
        $this->dateNotifJusquau->Text = Atexo_Util::iso2frnDate($this->_donneeComplementaire->getDureeDateFin('Y-m-d'));
        self::viderDuree();
        self::viderDescLibre();
    }

    public function populateDescLibre()
    {
        $this->delaiLibre->Text = $this->_donneeComplementaire->getDureeDescription();
        self::viderDuree();
        self::viderDelai();
    }

    public function viderDuree()
    {
        $this->_donneeComplementaire->setIdChoixMoisJour(null);
    }

    public function viderDelai()
    {
        $this->_donneeComplementaire->setDureeDateDebut(null);
        $this->_donneeComplementaire->setDureeDateFin(null);
    }

    public function viderDescLibre()
    {
        $this->_donneeComplementaire->setDureeDescription(null);
    }

    public function populateDatas()
    {
        // initialisation des composants
        self::fillDureeDelai();
        self::fillUniteDuree();

        // Duree Delai du marché
        if (($this->_donneeComplementaire instanceof CommonTDonneeComplementaire) && $this->_donneeComplementaire->getIdDureeDelaiDescription()) {
            $valeurRef = CommonValeurReferentielPeer::retrieveByPK($this->_donneeComplementaire->getIdDureeDelaiDescription(), Atexo_Config::getParameter('REFERENTIEL_DUREE_DELAI'), $this->_connexion);
            $this->dureeDelai->setSelectedValue($valeurRef->getLibelle2().$this->getIdPanel());
            if ($valeurRef instanceof CommonValeurReferentiel) {
                switch ($valeurRef->getLibelle2()) {
                    case 'DUREE_MARCHEE':
                        self::populateDuree();
                        break;
                    case 'DELAI_EXECUTION':
                        self::populateDelai();
                        break;
                    case 'DESCRIPTION_LIBRE':
                        self::populateDescLibre();
                        break;
                }
            }
        }

        // Marché reconductible
        if (($this->_donneeComplementaire instanceof CommonTDonneeComplementaire) && $this->_donneeComplementaire->getReconductible()) {
            $this->marcheReconductible->Checked = true;
            $this->nbReconduction->Text = $this->_donneeComplementaire->getNombreReconductions();
            $this->modalitesReconduction->Text = $this->_donneeComplementaire->getModalitesReconduction();
        }
    }

    public function rafraichissementJs()
    {
        $this->scriptLabel->Text = '<script>'.
                                    "isCheckedShowDiv(document.getElementById('".$this->marcheReconductible->ClientId."'),'".$this->infos_reconductible->getClientId()."');".
                                    "displayOptionChoice(document.getElementById('".$this->dureeDelai->ClientId."'));".
                                    "displayOptionChoice(document.getElementById('".$this->dureeJourMois->ClientId."'));";

        if ('Agent.PopupDetailLot' === $_GET['page']) {
            //Bloc durée maximal du marché
            $this->scriptLabel->Text .='disactivateSelect("'.$this->dureeDelai->ClientId.'");';
            $this->scriptLabel->Text .='disactivateSelect("'.$this->dureeJourMois->ClientId.'");';
            $this->scriptLabel->Text .='disactivateSelect("'.$this->duree->ClientId.'");';
            $this->scriptLabel->Text .='disactivateSelect("'.$this->marcheReconductible->ClientId.'");';
        }
        $this->scriptLabel->Text .='</script>';

    }

    public function chargerComposant()
    {
        $this->_connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        if (!$this->Page->IsPostBack) {
            self::populateDatas();
            self::rafraichissementJs();
        }
    }

    /*
     * Permet d'afficher les elements du composant selon le mode choisi
     */
    public function enabledElement()
    {
        return match ($this->mode) {
            0 => false,
            1 => true,
            default => true,
        };
    }

    /*
     * on l'utilise si on veux afficher plusieurs instance de ce composant dans une même page
     */
    public function getIdPanel()
    {
        if ($this->idElement) {
            return '_'.$this->idElement;
        } else {
            return '';
        }
    }

    /*
     * Permet de supprimer l'id panel dans une valeur
     */
    public function deleteIdPanelFromeValue($value)
    {
        if ($this->idElement) {
            $elements = explode($this->getIdPanel(), $value);
            $value = $elements[0];
        }

        return $value;
    }
}
