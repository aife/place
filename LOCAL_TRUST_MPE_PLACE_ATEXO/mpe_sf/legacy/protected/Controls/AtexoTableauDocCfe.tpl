<com:TConditional condition="$this->isResponsive()">
	<prop:trueTemplate>
		<%include Application.templates.responsive.AtexoTableauDocCfe %>
	</prop:trueTemplate>
	<prop:falseTemplate>
		<%include Application.templates.mpe.AtexoTableauDocCfe %>
	</prop:falseTemplate>
</com:TConditional>