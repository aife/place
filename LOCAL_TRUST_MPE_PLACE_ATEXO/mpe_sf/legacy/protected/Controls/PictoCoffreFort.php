<?php

namespace Application\Controls;

use Application\Service\Atexo\Controller\Atexo_Controller_Front;
use Prado\Prado;
use Prado\Web\UI\WebControls\TImage;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PictoCoffreFort extends TImage
{
    public string $_activate = 'true';

    public function getActivate()
    {
        return $this->_activate;
    }

    public function setActivate($value)
    {
        $this->_activate = $value;
    }

    public function onLoad($param)
    {
        if ('true' == $this->_activate) {
            $this->setImageUrl(Atexo_Controller_Front::t().'/images/picto-coffre-fort.gif');
        } else {
            $this->setImageUrl(Atexo_Controller_Front::t().'/images/picto-coffre-fort-inactive.gif');
        }

        $this->setAlternateText(Prado::localize('COFFRE_FORT_ELECTRONIQUE'));
    }
}
