<com:TActivePanel ID="panelListePiecesDCE"  >
	<com:TRepeater ID="repeaterPiecesDCE" >
		<prop:HeaderTemplate>
			<table class="table-results" summary="Pièces incluses dans le DCE">
		</prop:HeaderTemplate>
		<prop:ItemTemplate>
		<tr class="<%#(($this->ItemIndex%2==0)? 'on':'')%>">
			<td class="check-col">
				<com:THiddenField ID="idDocReplace" Value="<%#$this->Data['index']%>" />
				<com:TRadioButton 
					id="idPieceRedac" 
					UniqueGroupName="pieceDCEReplace" 
					Attributes.title="<%=Prado::localize('DOC_A_REMPLACER')%>"
					Attributes.onchange="document.getElementById('ctl0_CONTENU_PAGE_remplacerPieceDce').checked  = true;"
					CssClass="radio" />
				
			</td>
			<td class="col-500"><com:TLabel id="nomFile" text="<%#$this->Data['nom']%>" /></td>
			<td class="actions">
				<com:TActiveImageButton 
					id="supprimerPiece" 
					ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t().'/images/picto-supprimer-big.gif'%>"
					onCallBack="SourceTemplateControl.deletePieceDce" 
					Attributes.title="<%=Prado::localize('DEFINE_SUPPRIMER')%>" 
					ActiveControl.CallbackParameter="<%#$this->Data['index']%>"
					Attributes.onclick="if(!confirm('<%=Prado::localize('TEXT_ALERTE_SUPPRESSION_PIECE_DCE')%>')) return false;"  >
				</com:TActiveImageButton>
			</td>
		</tr>
	</prop:ItemTemplate>
	<prop:FooterTemplate>
		</table>
	</prop:FooterTemplate>
</com:TRepeater>
</com:TActivePanel>	
<com:TActivePanel ID="panelNoElementFound"  >
		<h2><center><com:TTranslate>DEFINE_AUCUN_RESULTAT</com:TTranslate>&nbsp;</center></h2>
</com:TActivePanel>		
<com:THiddenField ID="filesToDelete"/>
