	<com:TRepeater ID="repeaterDestinataire" >
		
	<prop:HeaderTemplate>
		<table summary="<%=Prado::localize('DEFINE_TEXT_LISTE_DESTINATAIRE')%>">
			<caption><com:TTranslate>DEFINE_TEXT_LISTE_DESTINATAIRE</com:TTranslate></caption>
			<thead>
				<tr>
					<th class="col-desti" id="destinataires_form_xml_1"><com:TTranslate>TEXT_DESTINATAIRE</com:TTranslate></th>
					<th class="col-90" id="statutEnvoi_form_xml_1"><com:TTranslate>TEXT_STATUT_ENVOI</com:TTranslate></th>
					<th class="col-90" id="dateEnvoi_form_xml_1"><com:TTranslate>TEXT_DATE_ENVOI</com:TTranslate></th>
					<th class="col-110" id="datePub_form_xml_1" ><com:TTranslate>TEXT_DATE_PUBLICATION</com:TTranslate></th>
					<th class="col-110" id="ar_form_xml_1"><com:TTranslate>TEXT_ACCUSE_RECEPTION</com:TTranslate></th>
					<th class="actions" id="actionsEnvoi_form_xml_1"><com:TTranslate>DEFINE_ACTIONS</com:TTranslate></th>
				</tr>
				
			</thead>
		</prop:HeaderTemplate>
		<prop:ItemTemplate>
		<tr class="on">
			<td class="col-desti" headers="destinataires_form_xml_1"><%#$this->Data->getDestinataire()%></td>
			<td class="col-90" headers="statutEnvoi_form_xml_1"><%#$this->Data->getStatut()%></td>
			<td class="col-90" headers="dateEnvoi_form_xml_1"><%#$this->Data->getDateEnvoi()%></td>
			<td class="col-110" headers="datePub_form_xml_1">
				<%#$this->Data->getDatePublication()%>
				<br />
				<a href="#">Voir l'avis PDF</a>
			</td>
			<td class="col-110" headers="ar_form_xml_1"><%#$this->Data->getAccuseReception()%></td>
			<td class="actions" headers="actionsEnvoi_form_xml_1">-</td>
		</tr>
		</prop:ItemTemplate>
		<prop:AlternatingItemTemplate>
			<tr>
				<td class="col-desti" headers="destinataires_form_xml_1"><%#$this->Data->getDestinataire()%></td>
				<td class="col-90" headers="statutEnvoi_form_xml_1"><%#$this->Data->getStatut()%></td>
				<td class="col-90" headers="dateEnvoi_form_xml_1"><%#$this->Data->getDateEnvoi()%></td>
				<td class="col-110" headers="datePub_form_xml_1">
					<%#$this->Data->getDatePublication()%>
					<br />
					<a href="#">Lien</a>
				</td>
				<td class="col-110" headers="ar_form_xml_1"><%#$this->Data->getAccuseReception()%></td>
				<td class="actions" headers="actionsEnvoi_form_xml_1">-</td>
			</tr>
	</prop:AlternatingItemTemplate>
	<prop:FooterTemplate>
		</table>
	</prop:FooterTemplate>
</com:TRepeater>
