	<com:TConditional condition="$this->getResponsive() == 1">
		<prop:trueTemplate>
			<%include Application.templates.responsive.IdentificationEntreprise %>
		</prop:trueTemplate>
		<prop:falseTemplate>
			<com:TConditional condition="$this->getResponsive() == 2">
				<prop:trueTemplate>
					<%include Application.templates.mpe.IdentificationEntrepriseContrat %>
				</prop:trueTemplate>
				<prop:falseTemplate>
					<%include Application.templates.mpe.IdentificationEntreprise %>
				</prop:falseTemplate>
			</com:TConditional>
		</prop:falseTemplate>
	</com:TConditional>