<?php

namespace Application\Controls;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonSsoAgent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Socle\Atexo_Socle_AgentServicesMetiers;

/**
 * Page de gestion des enveloppes de reponse electronique.
 *
 * @author khalid BENAMAR <khalid.benamar@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 1.0
 *
 * @since MPE-4.0
 */
class AtexoListeServicesMetier extends MpeTTemplateControl
{
    private $_idAgentConnecte;
    private $_organisme;

    /**
     * Recupère la valeur.
     */
    public function getIdAgentConnecte()
    {
        return $this->_idAgentConnecte;
    }

    /**
     * Affectes la valeur.
     */
    public function setIdAgentConnecte($value)
    {
        $this->_idAgentConnecte = $value;
    }

    /**
     * Recupère la valeur.
     */
    public function getOrganisme()
    {
        return $this->_organisme;
    }

    /**
     * Affectes la valeur.
     */
    public function setOrganisme($value)
    {
        $this->_organisme = $value;
    }

    /**
     * Chargement de la page.
     */
    public function onLoad($param)
    {
        if (!$this->getIdAgentConnecte()) {
            $this->setIdAgentConnecte(Atexo_CurrentUser::getIdAgentConnected());
            $this->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
        }
        $this->displayListeService();
    }

    public function displayListeService()
    {
        if ($this->getIdAgentConnecte() && $this->getOrganisme()) {
            $services = (new Atexo_Socle_AgentServicesMetiers())->retrieveServicesOrganisme($this->getIdAgentConnecte(), $this->getOrganisme(), true);
            $arrayService = [];
            foreach ($services as $service) {
                if ($service['logo']) {
                    $arrayService[] = $service;
                }
            }
            if (count($arrayService)) {
                $this->repeaterListeServices->DataSource = $arrayService;
                $this->repeaterListeServices->DataBind();
            } else {
                $this->setVisible(false);
            }
        }
    }

    public function generateUrlAccesWithSso($sender, $param)
    {
        $params = explode('#__#', $param->CommandParameter);
        $urlAcces = $params[0];
        $idServiceMetier = $params[1];
        $ssoAgent = new CommonSsoAgent();
        $sso = Atexo_CurrentUser::dumpSession();
        $ssoAgent->setIdSso($sso);
        $ssoAgent->setIdAgent(Atexo_CurrentUser::getIdAgentConnected());
        $ssoAgent->setDateConnexion(date('Y-m-d H:i:s'));
        $ssoAgent->setOrganisme(Atexo_CurrentUser::getOrganismAcronym());
        $ssoAgent->setDateLastRequest(Atexo_Util::dateDansFutur(date('Y-m-d H:i:s'), Atexo_Config::getParameter('NBR_HEURE_MAINTENANCE_CNX')));
        $ssoAgent->setServiceId($idServiceMetier);
        $ssoAgent->save(Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE')));
        if (strstr($urlAcces, '?')) {
            $separateur = '&';
        } else {
            $separateur = '?';
        }
        $this->response->redirect($urlAcces.$separateur.'sso='.$sso);
        //$this->response->redirect($urlAcces.$separateur.'sso='.$sso);
    }

    public function getCssClassServiceMetier($idServiceMetier)
    {
        if (('socle' == Atexo_CurrentUser::readFromSession('ServiceMetier') && $idServiceMetier == Atexo_Config::getParameter('SERVICE_METIER_SOCLE'))
            || ('mpe' == Atexo_CurrentUser::readFromSession('ServiceMetier') && $idServiceMetier == Atexo_Config::getParameter('SERVICE_METIER_MPE'))
        ) {
            return 'on';
        } else {
            return 'off';
        }
    }
}
