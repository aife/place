<?php

namespace Application\Controls;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonAcheteurPublic;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonTConsultationComptePub;
use Application\Propel\Mpe\CommonTConsultationComptePubQuery;
use Application\Propel\Mpe\CommonTDonneeComplementaire;
use Application\Propel\Mpe\CommonTPubliciteFavoris;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_PublicPurchaser;
use Exception;

/**
 * Class DonneesComplementairesComptePublicite
 * Permet de gerer l'affichage du bloc de compte de publicite.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 *
 * @version 1.0
 *
 * @since 2016-les-echos
 *
 * @copyright Atexo 2016
 */
class DonneesComplementairesComptePublicite extends MpeTTemplateControl
{
    private $_refConsultation;
    private $_organisme;
    private $_validationGroupBtnValidate;
    private $_validationSummaryBtnValidate;
    private ?string $_sourcePage = null;
    private $_validationGroup;
    private $_validationSummary;

    /**
     * Recupere la valeur de [_refConsultation].
     *
     * @return mixed
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-les-echos
     *
     * @copyright Atexo 2016
     */
    public function getRefConsultation()
    {
        return $this->_refConsultation;
    }

    /**
     * Modifie la valeur de [_refConsultation].
     *
     * @param mixed $consultationId : reference de la consultation
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-les-echos
     *
     * @copyright Atexo 2016
     */
    public function setRefConsultation($consultationId)
    {
        $this->_refConsultation = $consultationId;
    }

    /**
     * Recupere la valeur de [_organisme].
     *
     * @return mixed
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-les-echos
     *
     * @copyright Atexo 2016
     */
    public function getOrganisme()
    {
        return $this->_organisme;
    }

    /**
     * Modifie la valeur de [_organisme].
     *
     * @param mixed $organisme
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-les-echos
     *
     * @copyright Atexo 2016
     */
    public function setOrganisme($organisme)
    {
        $this->_organisme = $organisme;
    }

    /**
     * Modifie la valeur de [_validationGroupBtnValidate].
     *
     * @param $validationGroup
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-les-echos
     *
     * @copyright Atexo 2016
     */
    public function setValidationGroupBtnValidate($validationGroup)
    {
        $this->_validationGroupBtnValidate = $validationGroup;
    }

    /**
     * Recupere la valeur de [_validationGroupBtnValidate].
     *
     * @return mixed
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-les-echos
     *
     * @copyright Atexo 2016
     */
    public function getValidationGroupBtnValidate()
    {
        return $this->_validationGroupBtnValidate;
    }

    /**
     * Modifie la valeur de [_validationSummaryBtnValidate].
     *
     * @param $value
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-les-echos
     *
     * @copyright Atexo 2016
     */
    public function setValidationSummaryBtnValidate($value)
    {
        $this->_validationSummaryBtnValidate = $value;
    }

    /**
     * Recupere la valeur de [_validationSummaryBtnValidate].
     *
     * @return mixed
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-les-echos
     *
     * @copyright Atexo 2016
     */
    public function getValidationSummaryBtnValidate()
    {
        return $this->_validationSummaryBtnValidate;
    }

    /**
     * Modifie la valeur de [_validationGroup].
     *
     * @param $validationGroup
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-les-echos
     *
     * @copyright Atexo 2016
     */
    public function setValidationGroup($validationGroup)
    {
        $this->_validationGroup = $validationGroup;
    }

    /**
     * Recupere la valeur de [_validationGroup].
     *
     * @return mixed
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-les-echos
     *
     * @copyright Atexo 2016
     */
    public function getValidationGroup()
    {
        return $this->_validationGroup;
    }

    /**
     * Modifie la valeur de [_validationSummary].
     *
     * @param $validationSummary
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-les-echos
     *
     * @copyright Atexo 2016
     */
    public function setValidationSummary($validationSummary)
    {
        $this->_validationSummary = $validationSummary;
    }

    /**
     * Recupere la valeur de [_validationSummary].
     *
     * @return mixed
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-les-echos
     *
     * @copyright Atexo 2016
     */
    public function getValidationSummary()
    {
        return $this->_validationSummary;
    }

    /**
     * Recupere l'objet consultation.
     *
     * @return CommonConsultation|null : consultation
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-les-echos
     *
     * @copyright Atexo 2016
     */
    private function getConsultation()
    {
        return $this->getViewState('consultation');
    }

    /**
     * recupère la valeur de [_sourcePage].
     *
     * @return mixed
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-les-echos
     *
     * @copyright Atexo 2016
     */
    public function getSourcePage()
    {
        return $this->_sourcePage;
    }

    /**
     * Affecte la valeur de [_sourcePage].
     *
     * @param string $value
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-les-echos
     *
     * @copyright Atexo 2016
     */
    public function setSourcePage($value)
    {
        $this->_sourcePage = $value;
    }

    /**
     * Sauvegarde l'objet consultation.
     *
     * @param CommonConsultation $consultation : consultation
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-les-echos
     *
     * @copyright Atexo 2016
     */
    private function setConsultation($consultation)
    {
        $this->setViewState('consultation', $consultation);
    }

    /**
     * Permet d'initialiser le composant.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-les-echos
     *
     * @copyright Atexo 2016
     */
    public function initialiserComposant($consultation)
    {
        $this->setConsultation($consultation);
        //$this->initComposant();
        $this->preRemplirInfosComptePublicite(null, null);
    }

    /**
     * Permet de recuperer les comptes de publicite.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-les-echos
     *
     * @copyright Atexo 2016
     */
    public function initComposant()
    {
        $logger = Atexo_LoggerManager::getLogger('publicite');
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        try {
            $dataSource = [];
            $comptesPubs = [];

            $consultation = $this->getConsultation();
            if ($consultation instanceof CommonConsultation) {
                $donneesComplementaires = $consultation->getDonneComplementaire();
                if ($donneesComplementaires instanceof CommonTDonneeComplementaire) {
                    //Recuperer le compte pub lie a la consultation
                    $comptePubliciteCons = CommonTConsultationComptePubQuery::create()->filterByIdDonneesComplementaires($donneesComplementaires->getIdDonneeComplementaire())->findOne($connexion);
                    if ($comptePubliciteCons instanceof CommonTConsultationComptePub) {
                        $this->comptePublicite->selectedValue = $comptePubliciteCons->getIdComptePub();
                        $comptesPubs = [$comptePubliciteCons->getIdComptePub() => $comptePubliciteCons];
                    }

                    $this->setViewState('idDonneesComplementaires', $donneesComplementaires->getIdDonneeComplementaire());
                } else {
                    $logger->error('Les donnees complementaires ne sont pas une instance de CommonTDonneeComplementaire. Methode = DonneesComplementairesComptePublicite::initComposant');
                }
            } else {
                $logger->error("La consultation n'est pas une instance de CommonConsultation. Methode = DonneesComplementairesComptePublicite::initComposant");
            }
            $arrayKeys = array_keys($comptesPubs);

            $this->setComptePubliciteSelectionne($comptesPubs[array_shift($arrayKeys)]);
            $this->comptePublicite->dataSource = $dataSource;
            $this->comptePublicite->dataBind();
        } catch (Exception $e) {
            $logger->error('Erreur lors du remplissage de la liste des comptes de publicite. Erreur: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString());
        }
    }

    /**
     * Permet de pre-Remplir les infos du compte de publicite.
     *
     * @param $sender
     * @param $param
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-les-echos
     *
     * @copyright Atexo 2016
     */
    public function preRemplirInfosComptePublicite($sender, $param)
    {
        try {
            /*$compteBoamp = Atexo_PublicPurchaser::retrievePublicPurchasersById($this->comptePublicite->selectedValue);
            if($compteBoamp instanceof CommonAcheteurPublic) {
                $this->setCompteBoampSelectionne($compteBoamp);
            }
            if ($sender != null && $param != null) {
                $this->setComptePubliciteSelectionne($compteBoamp);
            }*/

            $comptePublicite = $this->getConsultationComptePublicite();
            if ($comptePublicite instanceof CommonTConsultationComptePub) {
                //Bloc "Adresse de l'organisme qui passe le marche"
                $this->correspondant->Text = $comptePublicite->getPrm();
                $this->nomOrganisme1->Text = $comptePublicite->getDenomination();
                $this->adresseVoie1->Text = $comptePublicite->getAdresse();
                $this->adresseCP1->Text = $comptePublicite->getCp();
                $this->telephone1->Text = $comptePublicite->getTelephone();
                $this->email1->Text = $comptePublicite->getEmail();
                $this->adresseVille1->Text = $comptePublicite->getVille();
                $this->adresseURL1->Text = $comptePublicite->getUrl();

                //Bloc "Adresse de facturation de la publicité "
                $this->nomOrganisme3->Text = $comptePublicite->getFactureDenomination();
                $this->adresseVoie3->Text = $comptePublicite->getFactureAdresse();
                $this->adresseCP3->Text = $comptePublicite->getFactureCp();
                $this->adresseVille3->Text = $comptePublicite->getFactureVille();

                //Bloc "Adresse de l'instance chargée des procédures de recours"
                $this->nomOrganisme2->Text = $comptePublicite->getInstanceRecoursOrganisme();
                $this->adresseVoie2->Text = $comptePublicite->getInstanceRecoursAdresse();
                $this->adresseCP2->Text = $comptePublicite->getInstanceRecoursCp();
                $this->adresseVille2->Text = $comptePublicite->getInstanceRecoursVille();
                $this->adresseURL2->Text = $comptePublicite->getInstanceRecoursUrl();
            }

            //Raffraichissement du bloc
            if (null != $sender && null != $param) {
                $this->raffraichirBlocDonneesComptePublicite($sender, $param);
            }
        } catch (Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('publicite');
            $logger->error('Erreur lors du pre-remplissage des champs du formulaire de la liste des comptes de publicite. Erreur: '.$e->getMessage().PHP_EOL.'Trace : '.$e->getTraceAsString());
        }
    }

    /**
     * Permet de sauvegarder le compte publicite selectionne dans la session de la page.
     *
     * @param CommonAcheteurPublic $comptePublicite : compte publicite
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-les-echos
     *
     * @copyright Atexo 2016
     */
    public function setComptePubliciteSelectionne($comptePublicite)
    {
        if ($comptePublicite instanceof CommonAcheteurPublic || $comptePublicite instanceof CommonTConsultationComptePub) {
            $this->setViewState('comptePubliciteSelectionne', $comptePublicite);
        }
    }

    /**
     * Permet de recuperer le compte publicite selectionne dans la session de la page.
     *
     * @return mixed
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-les-echos
     *
     * @copyright Atexo 2016
     */
    public function getComptePubliciteSelectionne()
    {
        return $this->getViewState('comptePubliciteSelectionne');
    }

    /**
     * Permet de sauvegarder le compte boamp selectionne dans la session de la page.
     *
     * @param CommonAcheteurPublic $comptePublicite : compte boamp
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-les-echos
     *
     * @copyright Atexo 2016
     */
    public function setCompteBoampSelectionne($compteBoamp)
    {
        if ($compteBoamp instanceof CommonAcheteurPublic) {
            $this->setViewState('compteBoampSelectionne', $compteBoamp);
        }
    }

    /**
     * Permet de recuperer le compte boamp selectionne depuis la session de la page.
     *
     * @return mixed
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-les-echos
     *
     * @copyright Atexo 2016
     */
    public function getCompteBoampSelectionne()
    {
        return $this->getViewState('compteBoampSelectionne');
    }

    public function setConsultationComptePublicite($compteConsComptePub)
    {
        if ($compteConsComptePub instanceof CommonTConsultationComptePub) {
            $this->setViewState('ConsultationComptePub', $compteConsComptePub);
        }
    }

    public function getConsultationComptePublicite()
    {
        return $this->getViewState('ConsultationComptePub');
    }

    /**
     * Permet de raffraichir le bloc des donnees des comptes de publicite.
     *
     * @param $sender
     * @param $param
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-les-echos
     *
     * @copyright Atexo 2016
     */
    public function raffraichirBlocDonneesComptePublicite($sender, $param)
    {
        try {
            $this->blocDonneesComptePublicite->render($param->getNewWriter());
        } catch (Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('publicite');
            $logger->error('Erreur lors du raffraichissement des champs du formulaire lies aux comptes de publicite. Erreur: '.$e->getMessage().PHP_EOL.'Trace : '.$e->getTraceAsString());
        }
    }

    /**
     * Permet de sauvegarder les donnees du compte publicite.
     *
     * @param CommonTDonneeComplementaire $donneesComplementaires
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-les-echos
     *
     * @copyright Atexo 2016
     */
    public function saveDonneesComptesPublicite(&$donneesComplementaires, &$favorisPub, $connexion)
    {
        if ($donneesComplementaires instanceof CommonTDonneeComplementaire) {
            //Save infos 't_consultation_compte_pub'
            $this->saveTConsultationComptePub($donneesComplementaires, $connexion);

            //Remplacement des infos du compte BOAMP
            $this->enregistrerInfosFavoris($favorisPub, $connexion);
        }
    }

    /**
     * Permet de :
     *     1. creer un compte BOAMP avec les informations saisies par l'agent, si le compte n'existe pas
     *     2. remplacer les infos du compte BOAMP existant pour les informations saisies par l'agent.
     *
     * @param PropelPDO $connexion : connexion
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-les-echos
     *
     * @copyright Atexo 2016
     */
    public function enregistrerInfosFavoris(&$favorisPub, $connexion)
    {
        try {
            if ($favorisPub instanceof CommonTPubliciteFavoris) {
                //Bloc "Adresse de l'organisme qui passe le marche"
                $favorisPub->setAcheteurCorrespondant($this->correspondant->SafeText);
                $favorisPub->setAcheteurNomOrganisme($this->nomOrganisme1->SafeText);
                $favorisPub->setAcheteurAdresse($this->adresseVoie1->SafeText);
                $favorisPub->setAcheteurCp($this->adresseCP1->SafeText);
                $favorisPub->setAcheteurVille($this->adresseVille1->SafeText);
                $favorisPub->setAcheteurEmail($this->email1->SafeText);
                $favorisPub->setAcheteurTelephone($this->telephone1->SafeText);
                $favorisPub->setAcheteurUrl($this->adresseURL1->SafeText);

                $favorisPub->setFactureDenomination($this->nomOrganisme3->SafeText);
                $favorisPub->setFactureAdresse($this->adresseVoie3->SafeText);
                $favorisPub->setFactureCp($this->adresseCP3->SafeText);
                $favorisPub->setFactureVille($this->adresseVille3->SafeText);

                $favorisPub->setInstanceRecoursOrganisme($this->nomOrganisme2->SafeText);
                $favorisPub->setInstanceRecoursAdresse($this->adresseVoie2->SafeText);
                $favorisPub->setInstanceRecoursCp($this->adresseCP2->SafeText);
                $favorisPub->setInstanceRecoursVille($this->adresseVille2->SafeText);
                $favorisPub->setInstanceRecoursUrl($this->adresseURL2->SafeText);

                $favorisPub->save($connexion);
            }
        } catch (Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('publicite');
            $logger->error("Erreur lors de la mise a jour des infos du favoris publicite  ('CommonTPubliciteFavoris'). Erreur: ".$e->getMessage().PHP_EOL.'Trace : '.$e->getTraceAsString());
        }
    }

    /**
     * Permet d'enregistrer les infos du compte de publicite dans la table "t_consultation_compte_pub".
     *
     * @param CommonTDonneeComplementaire $donneesComplementaires : donnees complementaires
     * @param PropelPDO                   $connexion              : connexion
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-les-echos
     *
     * @copyright Atexo 2016
     */
    public function saveTConsultationComptePub(&$donneesComplementaires, $connexion)
    {
        $logger = Atexo_LoggerManager::getLogger('publicite');
        try {
            $tConsultationComptePubQuery = new CommonTConsultationComptePubQuery();
            $comptePublicite = $tConsultationComptePubQuery->recupererComptePubByIdDonneesCompAndOrg($donneesComplementaires->getIdDonneeComplementaire(), Atexo_CurrentUser::getCurrentOrganism(), $logger);
            if (!$comptePublicite instanceof CommonTConsultationComptePub) {
                $comptePublicite = new CommonTConsultationComptePub();
            }

            $comptePublicite->setIdDonneesComplementaires($donneesComplementaires->getIdDonneeComplementaire());
            $comptePublicite->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
            /*$comptePublicite->setIdComptePub($this->comptePublicite->selectedValue);
            $comptePublicite->setBoampLogin($this->identifiantComptePublicite->SafeText);
            $comptePublicite->setBoampPassword($this->passwordComptePublicite->SafeText);
            $comptePublicite->setBoampMail($this->emailComptePublicite->SafeText);
            if($this->siteBoampDemo->Checked) {
                $comptePublicite->setBoampTarget('0');
            } else if($this->siteBoampProd->Checked) {
                $comptePublicite->setBoampTarget('1');
            }*/

            //Bloc "Adresse de l'organisme qui passe le marche"
            $comptePublicite->setPrm($this->correspondant->SafeText);
            $comptePublicite->setDenomination($this->nomOrganisme1->SafeText);
            $comptePublicite->setAdresse($this->adresseVoie1->SafeText);
            $comptePublicite->setCp($this->adresseCP1->SafeText);
            $comptePublicite->setTelephone($this->telephone1->SafeText);
            $comptePublicite->setEmail($this->email1->SafeText);
            $comptePublicite->setVille($this->adresseVille1->SafeText);
            $comptePublicite->setUrl($this->adresseURL1->SafeText);

            //Bloc "Adresse de facturation de la publicité"
            $comptePublicite->setFactureDenomination($this->nomOrganisme3->SafeText);
            $comptePublicite->setFactureAdresse($this->adresseVoie3->SafeText);
            $comptePublicite->setFactureCp($this->adresseCP3->SafeText);
            $comptePublicite->setFactureVille($this->adresseVille3->SafeText);

            //Bloc "Adresse de l'instance chargée des procédures de recours"
            $comptePublicite->setInstanceRecoursOrganisme($this->nomOrganisme2->SafeText);
            $comptePublicite->setInstanceRecoursAdresse($this->adresseVoie2->SafeText);
            $comptePublicite->setInstanceRecoursCp($this->adresseCP2->SafeText);
            $comptePublicite->setInstanceRecoursVille($this->adresseVille2->SafeText);
            $comptePublicite->setInstanceRecoursUrl($this->adresseURL2->SafeText);
            $comptePublicite->save($connexion);

            //$this->setComptePubliciteSelectionne($comptePublicite);
            $this->setViewState('idDonneesComplementaires', $comptePublicite->getIdDonneesComplementaires());
        } catch (Exception $e) {
            $logger->error("Erreur lors de la mise a jour des infos du compte de publicite (dans la table 't_consultation_compte_pub'). Erreur: ".$e->getMessage().PHP_EOL.'Trace : '.$e->getTraceAsString());
        }
    }
}
