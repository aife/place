<com:TActivePanel id="publiciteRecap" >
    <com:TActivePanel id="panelBlocErreur" Display="None">
        <com:PanelMessageErreur ID="messageErreur" Message="<%=Prado::localize('CONCENTRATEUR_SERVICE_INDISPONIBLE')%>"/>
    </com:TActivePanel>
    <com:TActivePanel id="panelConcentrateur" Display="None" >
        <link rel="stylesheet" href="<%=$this->getUrlConcentrateur()%>/concentrateur-annonces/annonce-recap/annonceRecap.css">
        <script src="<%=$this->getUrlConcentrateur()%>/concentrateur-annonces/annonce-recap/annonceRecap.umd.js"></script>
        <div id="annonce-recap">
            <%=$this->getHtml()%>
        </div>
        <script src="<%=$this->getUrlConcentrateur()%>/concentrateur-annonces/annonceRecapVue.js"></script>
    </com:TActivePanel>

    <com:TActiveHiddenField id="doNotRemove"/> <!-- Champ caché car impossible de récupérer la valeur du premier champ via Prado -->
    <com:TActiveHiddenField id="isEnvoiChecked"/>
    <com:TActiveTextbox id="hasMol"  Display="None" />
</com:TActivePanel>
<com:TActiveLabel id="scriptJs"/>

