<div class="searchCotraitant form-field form-horizontal" style="display:block;">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h2 class="h4 panel-title">
                <com:TTranslate>DEFINE_RECHERCHE_MULTICRITAIRE</com:TTranslate>
            </h2>
        </div>
        <div class="panel-body">
            <div class="form-grou^p form-group-sm clearfix">
                <label class="control-label col-md-3" for="ctl0_CONTENU_PAGE_AtexoSearchCotraitant_keywordSearch">
                    <com:TTranslate>DEFINE_MOTS_CLES</com:TTranslate> :
                    <span class="m-0 p-0 m-l-1">
                        <a data-target="#" data-content="<%=Prado::localize('MSG_EXPLICATION_RECHERCHE_SIMPLE')%>" data-toggle="tooltip" title="<com:TTranslate>DEFINE_INFOS_MOTS_CLES_BC</com:TTranslate>"><i class="fa fa-question-circle text-info"></i></a>
                    </span>
                </label>
                <div class="col-md-9">
                    <com:TActiveTextBox  id="keywordSearch"
                                         Attributes.title="<%=Prado::localize('DEFINE_RECHERCHE_REF_TITRE_OBJET')%>"
                                         CssClass="form-control"
                    />
                    <small class="info">
                        <com:TTranslate>TEXT_RECHERCHE_MOTS_CLES_BOURSE_COTRAITANCE</com:TTranslate>
                    </small>
                </div>
            </div>
            <div class="form-grou^p form-group-sm clearfix">
                <hr>
                <label class="control-label col-md-3">
                    <com:TTranslate>DEFINE_TYPE_GROUPEMENT</com:TTranslate>
                    <span class="m-0 p-0 m-l-1">
                        <a data-target="#" data-content="<%=Prado::localize('MSG_EXPLICATION_RECHERCHE_SIMPLE')%>" data-toggle="tooltip" title="<com:TTranslate>TEXT_ENTREPRISE_POSTULE_EN_TANT_QUE</com:TTranslate>"><i class="fa fa-question-circle text-info"></i></a>
                    </span>
                </label>
                <div class="col-md-9">
                    <div class="col-md-4">
                        <div class="checkbox">
                            <div class="intitule-auto clear-both">
                                <label for="ctl0_CONTENU_PAGE_AtexoSearchCotraitant_collaborateurMandataire">
                                    <com:TCheckBox id="collaborateurMandataire"
                                                   Attributes.title="<%=Prado::localize('DEFINE_MANDATAIRE_GROUPEMENT')%>"
                                                   cssclass="check"
                                                   checked="true"/>
                                    <com:TTranslate>DEFINE_MANDATAIRE_GROUPEMENT</com:TTranslate>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="checkbox">
                            <div class="intitule-auto clear-both">
                                <label for="ctl0_CONTENU_PAGE_AtexoSearchCotraitant_collaborateurCotraitantSolidaire">
                                    <com:TCheckBox id="collaborateurCotraitantSolidaire"
                                                   Attributes.title="<%=Prado::localize('DEFINE_COTRAITANT_SOLIDAIRE')%>"
                                                   cssclass="check"
                                                   checked="true"/>
                                    <com:TTranslate>DEFINE_COTRAITANT_SOLIDAIRE</com:TTranslate>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="checkbox">
                            <div class="intitule-auto clear-both">
                                <label for="ctl0_CONTENU_PAGE_AtexoSearchCotraitant_collaborateurCotraitantConjoint">
                                    <com:TCheckBox id="collaborateurCotraitantConjoint"
                                                   Attributes.title="<%=Prado::localize('DEFINE_COTRAITANT_CONJOINT')%>"
                                                   cssclass="check"
                                                   checked="true"/>
                                    <com:TTranslate>DEFINE_COTRAITANT_CONJOINT</com:TTranslate>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="checkbox">
                            <div class="intitule-auto clear-both">
                                <label for="ctl0_CONTENU_PAGE_AtexoSearchCotraitant_collaborateurSoustraitant">
                                    <com:TCheckBox id="collaborateurSoustraitant"
                                                   Attributes.title="<%=Prado::localize('TEXT_SOUS_TRAITANT')%>"
                                                   cssclass="check"
                                                   checked="true"/>
                                    <com:TTranslate>TEXT_SOUS_TRAITANT</com:TTranslate>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-grou^p form-group-sm clearfix">
                <hr>
                <label class="control-label col-md-3" for="ctl0_CONTENU_PAGE_AtexoSearchCotraitant_clauseSociales">
                    <com:TTranslate>CLAUSES_SOCIALES</com:TTranslate> :
                    <span class="m-0 p-0 m-l-1">
                        <a data-target="#" data-content="<%=Prado::localize('MSG_EXPLICATION_RECHERCHE_SIMPLE')%>" data-toggle="tooltip" title="<com:TTranslate>DEFINE_CLAUSE_SOCIALE_INSERTION_HANDICAP</com:TTranslate>"><i class="fa fa-question-circle text-info"></i></a>
                    </span>
                </label>
                <div class="col-md-3">
                    <com:TDropDownList ID="clauseSociales"
                                       CssClass="form-control"
                                       Attributes.title="<%=Prado::localize('CLAUSES_SOCIALES')%>" >
                    </com:TDropDownList>
                </div>
                <label class="control-label col-md-3" for="ctl0_CONTENU_PAGE_AtexoSearchCotraitant_EseAdapte">
                    <com:TTranslate>TEXT_ESAT_EA</com:TTranslate> :
                    <span class="m-0 p-0 m-l-1">
                        <a data-target="#" data-content="<%=Prado::localize('MSG_EXPLICATION_RECHERCHE_SIMPLE')%>" data-toggle="tooltip" title="<com:TTranslate>DEFINE_ESAT_EA</com:TTranslate>"><i class="fa fa-question-circle text-info"></i></a>
                    </span>
                </label>
                <div class="col-md-3">
                    <com:TDropDownList ID="EseAdapte" CssClass="form-control"
                                       Attributes.title="<%=Prado::localize('CLAUSES_SOCIALES')%>" ></com:TDropDownList>
                </div>
            </div>
        </div>
        <div class="panel-footer">
            <div class="clearfix">
                <div class="pull-left">
                    <com:THyperLink ID="linkRetour"
                                    NavigateUrl="<%# $this->getUrlConsultation($_GET['id'], 'orgAcronyme='. $_GET['orgAcronyme'])%>"
                                    cssClass="btn btn-default btn-sm"
                                    Text="<%=Prado::localize('TEXT_RETOUR')%>"
                                    Attributes.title="<%=Prado::localize('TEXT_RETOUR')%>"
                    />
                </div>
                <div class="pull-right">
                    <com:TActiveButton id="boutonClear"
                                       text="<%=Prado::Localize('DEFINE_EFFACER_CRITERES_RECHERCHE')%>"
                                       Attributes.title="<%=Prado::Localize('DEFINE_EFFACER_CRITERES_RECHERCHE')%>"
                                       cssClass="btn btn-primary btn-sm"
                                       OnCommand="clear"
                                       OnCallBack="Page.AtexoSearchCotraitant.refreshBlocSearch"
                    >
                        <prop:ClientSide.OnLoading>showLoader();</prop:ClientSide.OnLoading>
                        <prop:ClientSide.OnFailure>showLoader();</prop:ClientSide.OnFailure>
                        <prop:ClientSide.OnSuccess>showLoader();</prop:ClientSide.OnSuccess>
                    </com:TActiveButton>

                    <com:TActiveButton id="boutonSearch"
                                       text="<%=Prado::Localize('DEFINE_LANCER_RECHERCHE')%>"
                                       Attributes.title="<%=Prado::Localize('DEFINE_LANCER_RECHERCHE')%>"
                                       cssClass="btn btn-primary btn-sm"
                                       onCallBack="Page.search"
                    >
                        <prop:ClientSide.OnLoading>showLoader();</prop:ClientSide.OnLoading>
                        <prop:ClientSide.OnFailure>showLoader();</prop:ClientSide.OnFailure>
                        <prop:ClientSide.OnSuccess>showLoader();</prop:ClientSide.OnSuccess>
                    </com:TActiveButton>
                </div>
            </div>
        </div>
    </div>
</div>