<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Prado\Prado;

class AtexoTableauDocCfe extends MpeTTemplateControl
{
    private $_titreTableau;
    private $_postBack;
    private bool $responsive = false;

    public function setDataSource($value)
    {
        if (!$this->getPostBack()) {
            if (is_countable($value) ? count($value) : 0) {
                $this->piecesCoffreFort->dataSource = $value;
                $this->piecesCoffreFort->dataBind();
            } else {
                $this->aucunePiece->Text = Prado::localize('AUCUN_DOCUMENT_TROUVE');
            }
        }
    }

    public function setTitreTableau($titre)
    {
        $this->_titreTableau = $titre;
    }

    public function getTitreTableau()
    {
        return $this->_titreTableau;
    }

    public function setPostBack($value)
    {
        $this->_postBack = $value;
    }

    public function getPostBack()
    {
        return $this->_postBack;
    }

    public function downloadPiece($sender, $param)
    {
        $nomFichier = $param->CommandParameter;
        $idFichier = $param->CommandName;
        $atexoBlob = new Atexo_Blob();
        $tailleFichier = $atexoBlob->getTailFile($idFichier, Atexo_Config::getParameter('COMMON_BLOB'));
        header('Pragma: public');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Cache-Control: private', false);
        if ('.zip' == strrchr($nomFichier, '.')) {
            header('Content-Type: application/zip');
        } else {
            header('Content-Type: application/octet-stream');
        }
        $nomFichier = str_replace('\\', '', $nomFichier);
        header('Content-Disposition: attachment; filename="'.$nomFichier.'";');
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: '.$tailleFichier);

        $atexoBlob->send_blob_to_client($idFichier, Atexo_Config::getParameter('COMMON_BLOB'));
        exit;
    }

    /**
     * @return bool
     */
    public function isResponsive()
    {
        return $this->responsive;
    }

    /**
     * @param bool $responsive
     */
    public function setResponsive($responsive)
    {
        $this->responsive = $responsive;
    }
}
