<?php

namespace Application\Controls;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonAcheteurPublic;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonTAnnonceConsultation;
use Application\Propel\Mpe\CommonTPreferenceSupportPublication;
use Application\Propel\Mpe\CommonTTypeAnnonceConsultation;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_PublicPurchaser;
use Application\Service\Atexo\FormulaireSub\Atexo_FormulaireSub_AnnonceEchange;
use Application\Service\Atexo\Publicite\Atexo_Publicite_AnnonceSub;
use Application\Service\Atexo\Publicite\Atexo_Publicite_Boamp;
use Exception;
use Prado\Prado;

/**
 * Classe d'ajout des publicites pour les consultations.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 *
 * @version 1.0
 *
 * @since esr-2015
 *
 * @copyright Atexo 2015
 */
class AtexoFormulaireAjoutPublicite extends MpeTTemplateControl
{
    private $_refConsultation;
    private $_organisme;
    private $_fonctionAAppelerApresAjoutAnnonce;

    /**
     * Recupere la valeur de [_fonctionAAppelerApresAjoutAnnonce].
     *
     * @return mixed
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function getFonctionAAppelerApresAjoutAnnonce()
    {
        $fonctionAAppelerApresAjoutAnnonce = $this->getViewState('fonctionAAppelerApresAjoutAnnonce');
        if (empty($fonctionAAppelerApresAjoutAnnonce)) {
            $fonctionAAppelerApresAjoutAnnonce = $this->_fonctionAAppelerApresAjoutAnnonce;
        }

        return $fonctionAAppelerApresAjoutAnnonce;
    }

    /**
     *  Modifie la valeur de [_fonctionAAppelerApresAjoutAnnonce].
     *
     * @param mixed $fonctionAAppelerApresAjoutAnnonce
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function setFonctionAAppelerApresAjoutAnnonce($fonctionAAppelerApresAjoutAnnonce)
    {
        $this->_fonctionAAppelerApresAjoutAnnonce = $fonctionAAppelerApresAjoutAnnonce;
        $this->setViewState('fonctionAAppelerApresAjoutAnnonce', $this->getFonctionAAppelerApresAjoutAnnonce());
    }

    /**
     * Recupere la valeur de [_refConsultation].
     *
     * @return mixed
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function getRefConsultation()
    {
        return $this->_refConsultation;
    }

    /**
     * Modifie la valeur de [_refConsultation].
     *
     * @param mixed $consultationId : reference de la consultation
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function setRefConsultation($consultationId)
    {
        $this->_refConsultation = $consultationId;
    }

    /**
     * Recupere la valeur de [_organisme].
     *
     * @return mixed
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function getOrganisme()
    {
        return $this->_organisme;
    }

    /**
     * Modifie la valeur de [_organisme].
     *
     * @param mixed $organisme
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function setOrganisme($organisme)
    {
        $this->_organisme = $organisme;
    }

    /**
     * Recupere l'objet consultation.
     *
     * @return CommonConsultation|null : consultation
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    private function getConsultation()
    {
        return $this->getViewState('consultation');
    }

    /**
     * Sauvegarde l'objet consultation.
     *
     * @param CommonConsultation $consultation : consultation
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    private function setConsultation($consultation)
    {
        $this->setViewState('consultation', $consultation);
    }

    /**
     * Permet d'initialiser le formulaire d'ajout de publicite.
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function initialiserFormulaire()
    {
        try {
            $consultation = (new Atexo_Consultation())->retrieveConsultation($this->getRefConsultation(), $this->getOrganisme());
            $this->setConsultation($consultation);
            $this->saveIdsSupportsPreferesAgent();
            $this->remplirListeComptesBoamp();
            $this->remplirListeTypesAnnonces();
            $this->remplirListeSupports();
            $this->remplirListeFormulaires();
        } catch (Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('publicite');
            $logger->error("Erreur lors de l'initialisation du formulaire d'ajout d'une publicite : RefCons = ".$this->getRefConsultation().' , Org = '.$this->getOrganisme().' , Erreur = '.$e->getMessage());
        }
    }

    /**
     * Permet de remplir la liste des types d'avis.
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function remplirListeTypesAnnonces()
    {
        try {
            $dataSource = ['0' => prado::localize('TEXT_SELECTIONNER').' ...'];
            $listeTypesAnnonces = Atexo_Publicite_AnnonceSub::getTypesAnnonces();
            if (!empty($listeTypesAnnonces)) {
                foreach ($listeTypesAnnonces as $typeAnnonce) {
                    if ($typeAnnonce instanceof CommonTTypeAnnonceConsultation) {
                        $dataSource[$typeAnnonce->getId()] = $typeAnnonce->getLibelle();
                    }
                }
            }
            $this->typeAnnonce->dataSource = $dataSource;
            $this->typeAnnonce->dataBind();
        } catch (Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('publicite');
            $logger->error("Erreur lors du remplissage de la liste des types d'annonces : ".$e->getMessage());
        }
    }

    /**
     * Permet de remplir la liste des comptes BOAMP.
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function remplirListeComptesBoamp()
    {
        try {
            $consultation = $this->getConsultation();
            if ($consultation instanceof CommonConsultation) {
                $dataSource = ['0' => prado::localize('TEXT_SELECTIONNER').' ...'];
                $listeAcheteurs = (new Atexo_PublicPurchaser())->retrievePublicPurchasersCons(Atexo_CurrentUser::getIdServiceAgentConnected(), $consultation->getServiceAssocieId());
                if (!empty($listeAcheteurs)) {
                    foreach ($listeAcheteurs as $acheteur) {
                        if ($acheteur instanceof CommonAcheteurPublic) {
                            $mailAgent = $acheteur->getBoampMail();
                            if (!empty($mailAgent)) {
                                $dataSource[$acheteur->getId()] = $acheteur->getBoampLogin().' - '.$mailAgent;
                            }
                        }
                    }
                }
                $this->compteBoamp->dataSource = $dataSource;
                $this->compteBoamp->dataBind();
            } else {
                $logger = Atexo_LoggerManager::getLogger('publicite');
                $logger->error("La consultation n'est pas une instance de CommonConsultation. Methode = AtexoAjoutSupportPublicite::remplirListeComptesBoamp");
            }
        } catch (Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('publicite');
            $logger->error('Erreur lors du remplissage de la liste des comptes BOAMP : '.$e->getMessage());
        }
    }

    /**
     * Permet de recuperer la liste des supports selectionnes par l'agent.
     *
     * @return array|null : liste des supports
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function getListeSupportsSelectionnes()
    {
        $listeSupports = $this->Page->getViewState('listeSupportsSelectionnes');
        if (empty(!$listeSupports)) {
            return $listeSupports;
        }

        return [];
    }

    /**
     * Permet d'ajouter un support donne a la liste des supports selectionnes par l'utilisateur.
     * Cette liste des supports est sauvegardee dans la session de la page.
     *
     * @param string $idSupport : identifiant du support a ajouter a la liste
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function setListeSupportsSelectionnes($idSupport)
    {
        try {
            $listeSupportsSelectionnes = $this->getListeSupportsSelectionnes();
            if (empty($listeSupportsSelectionnes)) {
                $listeSupportsSelectionnes = [];
            }
            $listeSupportsSelectionnes[$idSupport] = $idSupport;
            $this->Page->setViewState('listeSupportsSelectionnes', $listeSupportsSelectionnes);
        } catch (Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('publicite');
            $logger->error("Erreur lors de l'ajout d'un support a la liste des supports selectionnes par l'utilisateur : ".$e->getMessage()."\n Methode = AtexoFormulaireAjoutPublicite::setListeSupportsSelectionnes");
        }
    }

    /**
     * Permet d'ajouter un code de support a la liste des codes des supports selectionnes par l'utilisateur.
     * Cette liste des codes des supports est sauvegardee dans la session de la page.
     *
     * @param string $codeSupport : code du support de publication
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function setListeCodesSupportsSelectionnes($codeSupport)
    {
        try {
            $listeCodesSupportsSelectionnes = $this->getListeCodesSupportsSelectionnes();
            if (empty($listeCodesSupportsSelectionnes)) {
                $listeCodesSupportsSelectionnes = [];
            }
            $listeCodesSupportsSelectionnes[$codeSupport] = $codeSupport;
            $this->Page->setViewState('listeCodesSupportsSelectionnes', $listeCodesSupportsSelectionnes);
        } catch (Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('publicite');
            $logger->error("Erreur lors de l'ajout d'un code de support a la liste des codes des supports selectionnes par l'utilisateur : ".$e->getMessage()."\n Methode = AtexoFormulaireAjoutPublicite::setListeCodesSupportsSelectionnes");
        }
    }

    /**
     * Permet de recuperer la liste des codes des supports selectionnes par l'agent.
     *
     * @return array|null : liste des codes des supports
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function getListeCodesSupportsSelectionnes()
    {
        $listeCodesSupports = $this->Page->getViewState('listeCodesSupportsSelectionnes');
        if (empty(!$listeCodesSupports)) {
            return $listeCodesSupports;
        }

        return [];
    }

    /**
     * Permet de raffraichir la liste des choix de formulaire
     * Cette liste est impactee par les choix des supports et des types d'avis selectionnes par l'utilisateur.
     *
     * @param $sender
     * @param $param
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function raffraichirListeChoixFormulaire($sender, $param)
    {
        try {
            $listeSupportsSelectionnes = $this->getListeSupportsSelectionnes();
            if (!empty($listeSupportsSelectionnes)) {
                //TODO: faire le remplissage dynamique des choix de formulaires de publicite, a faire apres l'adaptation de la maquette finale
            }
        } catch (Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('publicite');
            $logger->error('Erreur lors du raffraichissement des choix des formulaires de publicite : '.$e->getMessage()."\n Methode = AtexoFormulaireAjoutPublicite::raffraichirListeChoixFormulaire");
        }
    }

    /**
     * Permet de creer une annonce de publicite
     * Redirige par la suite vers le tableau de suivi avec l'annonce nouvellement creee
     * Les dates de creation et de de modification de l'annonce sont automatisees dans le mapping.
     *
     * @param $sender
     * @param $param
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function creerAnnoncePublicite($sender, $param)
    {
        $compteBoamp = null;
        $connexion = null;
        $this->masquerMessageErreur($param);
        $this->initialiserJs();
        $logger = Atexo_LoggerManager::getLogger('publicite');
        $consultation = $this->getConsultation();
        if ($consultation instanceof CommonConsultation) {
            try {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
                $logger->info('Debut transaction : connexion = '.print_r($connexion, true));
                $connexion->beginTransaction();

                //Recuperation de la liste des supports selectionnes
                $this->recupererSupportsSelectionnes();

                //Recuperation du compte BOAMP
                $idCompteBoamp = $this->compteBoamp->selectedValue;
                if (!empty($idCompteBoamp)) {
                    $compteBoamp = (new Atexo_Publicite_Boamp())->retreiveAcheteurPublic($this->compteBoamp->selectedValue, Atexo_CurrentUser::getCurrentOrganism());
                } else {
                    $logger->error('Identifiant du compte BOAMP introuvable: refCons = '.$consultation->getId().' Organisme = '.$consultation->getOrganisme().", \n Methode = AtexoFormulaireAjoutPublicite::creerAnnoncePublicite");
                }

                $idDispositif = $this->formulaire->selectedValue;
                //Ajout de l'annonce de publicite
                $annonce = new CommonTAnnonceConsultation();
                $annonce->setIdDispositif($idDispositif);

                $xml = Atexo_FormulaireSub_AnnonceEchange::getXmlInitialisationDossier($consultation, $annonce, $compteBoamp, $this->getListeCodesSupportsSelectionnes());

                $idDossierSub = Atexo_FormulaireSub_AnnonceEchange::creerDossier($xml);
                if (false !== $idDossierSub && is_string($idDossierSub)) {
                    $annonce->setIdDossierSub($idDossierSub);
                    $annonce->setOrganisme($consultation->getOrganisme());
                    $annonce->setConsultationId($consultation->getId());
                    $annonce->setStatut(Atexo_Config::getParameter('ANNONCE_STATUT_A_COMPLETER'));
                    $annonce->setDateStatut(date('Y-m-d H:i:s'));
                    $annonce->setIdTypeAnnonce($this->getIdTypeAnnonce());
                    $annonce->setIdAgent(Atexo_CurrentUser::getIdAgentConnected());
                    $annonce->setPrenomNomAgent(Atexo_CurrentUser::getFirstName().' '.Atexo_CurrentUser::getLastName());
                    $annonce->setIdCompteBoamp($idCompteBoamp);

                    //Ajout des supports selectionnes a l'annonce de publicite
                    $supports = $this->getListeSupportsSelectionnes();
                    if (!empty($supports)) {
                        foreach ($supports as $idSupport) {
                            (new Atexo_Publicite_AnnonceSub())->addSupportsToAnnonce($annonce, $idSupport);
                        }
                    }

                    //Enregistrement de l'annonce
                    $annonce->save($connexion);
                    $connexion->commit();
                    //historique apres le commite car il ya des info qui doivent s'enregistrer en base avant de creer l'historique
                    Atexo_Publicite_AnnonceSub::ajouterHistorique($annonce, $connexion);
                    $logger->info('Transaction commitee : connexion = '.print_r($connexion, true));

                    //Recharger la page de suivi
                    $this->rechargerPageSuiviPublicite();
                } else {
                    $connexion->rollBack();
                    $logger->info('Transaction annulee : connexion = '.print_r($connexion, true));
                    $this->afficherErreur(Prado::localize('MSG_ERREUR_CREATION_ANNONCE_PUBLICITE'), $param);
                    $logger->error("Le dossier n'a pas pu etre cree : idDossier = ".$idDossierSub.' , RefCons = '.$consultation->getId().' , Organisme = '.$consultation->getOrganisme()." \n Xml Request = ".$xml."  \n Methode = AtexoFormulaireAjoutPublicite::creerAnnoncePublicite");

                    return;
                }
            } catch (Exception $e) {
                $connexion->rollBack();
                $logger->info('Transaction annulee : connexion = '.print_r($connexion, true));
                $logger->error("Erreur lors de l'ajout d'un formulaire de publicite : RefCons = ".$consultation->getId().' Organisme = '.$consultation->getOrganisme().', Erreur : '.$e->getMessage()."\n Methode = AtexoFormulaireAjoutPublicite::creerAnnoncePublicite");
            }
        } else {
            $logger->error("L'objet consultation est introuvable ou n'est pas une instance de CommonConsultation, \n Methode = AtexoFormulaireAjoutPublicite::creerAnnoncePublicite");
        }
    }

    /**
     * Recupere la valeur du type d'annonce de publicite sauvegardee dans la session de la page.
     *
     * @return string : type d'annonce de publicite
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function getIdTypeAnnonce()
    {
        return $this->getViewState('idTypeAnnoncePub');
    }

    /**
     * Sauvegarde la valeur du type d'annonce dans la session de la page.
     *
     * @param string $idTypeAnnonce : type d'annonce
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function setIdTypeAnnonce($idTypeAnnonce)
    {
        $this->setViewState('idTypeAnnoncePub', $idTypeAnnonce);
    }

    /**
     * Recupere la valeur de l'identifiant du type de formulaire dans la session de la page.
     *
     * @return string
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function getidTypeFormulaire()
    {
        return $this->getViewState('idTypeFormulaire');
    }

    /**
     * Sauvegarde l'identifiant du type de formulaire dans la session de la page.
     *
     * @param string $idTypeFormulaire : identifiant du type de formulaire
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function setidTypeFormulaire($idTypeFormulaire)
    {
        $this->setViewState('idTypeFormulaire', $idTypeFormulaire);
    }

    /**
     * Permet de recuperer la liste des identifiants et des codes des supports selectionnes
     * Sauvegarde cette liste dans la session de la page.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function recupererSupportsSelectionnes()
    {
        $logger = Atexo_LoggerManager::getLogger('publicite');
        try {
            foreach ($this->listeSupportsPub->getItems() as $item) {
                if (true === $item->support->checked) {
                    $this->setListeSupportsSelectionnes(base64_decode($item->idSupport->value));
                    $this->setListeCodesSupportsSelectionnes(base64_decode($item->codeSupport->value));
                } else {
                    $this->supprimerSupportDeLaListe(base64_decode($item->idSupport->value));
                }
            }
            $logger->info('Support(s) selectionne(s) : '.print_r($this->getListeSupportsSelectionnes(), true));
        } catch (Exception $e) {
            $logger->error('Erreur selection liste des supports : '.$e->getMessage()."\n Methode = AtexoFormulaireAjoutPublicite::recupererSupportsSelectionnes");
        }
    }

    /**
     * Permet de supprimer un support de la liste des supports selectionnes.
     *
     * @param string $idSupport : identifiant du support
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function supprimerSupportDeLaListe($idSupport)
    {
        $logger = Atexo_LoggerManager::getLogger('publicite');
        try {
            $listeSupports = $this->getListeSupportsSelectionnes();
            if (!empty($listeSupports[$idSupport])) {
                unset($listeSupports[$idSupport]);
                $this->Page->setViewState('listeSupportsSelectionnes', $listeSupports);
                $logger->error('Support(s) selectionne(s) '.print_r($this->getListeSupportsSelectionnes(), true));
            }
        } catch (Exception $e) {
            $logger->error("Erreur suppression d'un support de la liste : id_support = $idSupport \n\nErreur = ".$e->getMessage()."\n\n Methode = AtexoFormulaireAjoutPublicite::supprimerSupportDeLaListe");
        }
    }

    /**
     * Permet d'afficher le message d'erreur.
     *
     * @param string $message : message d'erreur
     * @param null   $param
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function afficherErreur($message, $param = null)
    {
        try {
            $this->labelMessage->Text = $message;
            $this->panelMessage->style = 'display:block';
            $this->panelMessage->render($param->getNewWriter());
        } catch (Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('publicite');
            $logger->error("Erreur affichage du message d'erreur : message = ".$message.' , Erreur = '.$e->getMessage()."\n Methode = AtexoFormulaireAjoutPublicite::afficherErreur");
        }
    }

    /**
     * Permet de masquer le message d'erreur.
     *
     * @param null $param
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function masquerMessageErreur($param = null)
    {
        try {
            $this->panelMessage->style = 'display:none;';
            $this->panelMessage->render($param->getNewWriter());
        } catch (Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('publicite');
            $logger->error("Erreur dissimulation du message d'erreur : ".$e->getMessage()."\n Methode = AtexoFormulaireAjoutPublicite::masquerMessageErreur");
        }
    }

    /**
     * Permet d'annuler l'action d'ajout de l'annonce de publicite.
     *
     * @param $sender
     * @param $param
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function annuler($sender, $param)
    {
        try {
            $this->masquerMessageErreur($param);
            $this->fermerFormulaireAjoutPublicite();
        } catch (Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('publicite');
            $logger->error("Erreur annulation de l'ajout de l'annonce de publicite : ".$e->getMessage()."\n Methode = AtexoFormulaireAjoutPublicite::annuler");
        }
    }

    /**
     * Permet de recharger la page de suivi de la publicite.
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function rechargerPageSuiviPublicite()
    {
        try {
            $this->rechargerTableauSuiviPublicite();
            $this->fermerFormulaireAjoutPublicite();
        } catch (Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('publicite');
            $logger->error('Erreur rechargement page de suivi : '.$e->getMessage()."\n Methode = AtexoFormulaireAjoutPublicite::rechargerPageSuiviPublicite");
        }
    }

    /**
     * Permet de fermer le formulaire d'ajout de la publicite.
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function fermerFormulaireAjoutPublicite()
    {
        $this->labelJavascript->Text .= "<script>J('.modal-choix-formulaire').dialog('close');</script>";
    }

    /**
     * Permet de recharger le tableau de suivi de la publicite.
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function rechargerTableauSuiviPublicite()
    {
        $this->labelJavascript->Text .= '<script>window.location.reload();</script>';
    }

    /**
     * Permet d'initialiser les scripts js.
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function initialiserJs()
    {
        $this->labelJavascript->Text = '';
    }

    /**
     * Permet de remplir le tableau de la liste des supports de publication.
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function remplirListeSupports()
    {
        try {
            $listeSupports = (new Atexo_Publicite_AnnonceSub())->getSupportsPublication();
            $this->setNbrSupports(is_countable($listeSupports) ? count($listeSupports) : 0);
            $this->listeSupportsPub->dataSource = $listeSupports;
            $this->listeSupportsPub->dataBind();
        } catch (Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('publicite');
            $logger->error('Erreur lors du remplissage de la liste des supports : '.$e->getMessage()."\n Methode = AtexoFormulaireAjoutPublicite::remplirListeSupports");
        }
    }

    /**
     * Permet de recuperer le nombre de supports a afficher dans la session de la page.
     *
     * @return string : nombre de supports
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function getNbrSupports()
    {
        return $this->getViewState('nbrSupports');
    }

    /**
     * Permet de sauvegarder le nombre de supports a afficher dans la session de la page.
     *
     * @param string $nbrSupports : nombre de supports
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function setNbrSupports($nbrSupports)
    {
        return $this->setViewState('nbrSupports', $nbrSupports);
    }

    /**
     * Effectue l'action de selection du support et du type d'annonce.
     *
     * @param $sender
     * @param $param
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function actionSelectionTypeAnnonceSupport($sender, $param)
    {
        $logger = Atexo_LoggerManager::getLogger('publicite');
        try {
            $this->recupererSupportsSelectionnes();
            $this->setIdTypeAnnonce($this->typeAnnonce->selectedValue);
            $logger->info('Type avis selectionne = '.$this->getIdTypeAnnonce());
            $this->preRemplirListeFormulaires();
        } catch (Exception $e) {
            $logger->error("Erreur lors de la selection d'un support ou d'un type d'avis : id type d'avis = ".$this->getIdTypeAnnonce().' , Supports selctionnes = '.print_r($this->getListeSupportsSelectionnes(), true)." \nErreur = ".$e->getMessage()."\n Methode = AtexoFormulaireAjoutPublicite::actionSelectionTypeAnnonceSupport");
        }
    }

    /**
     * Permet de pre-remplir le referentiel des formulaires en fonction des supports et du type d'annonce choisis.
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function preRemplirListeFormulaires()
    {
        $logger = Atexo_LoggerManager::getLogger('publicite');
        try {
            $idsSupportsSelectionnes = $this->getListeSupportsSelectionnes();
            $idTypeAnnonceSelectionne = $this->getIdTypeAnnonce();
            $choixIdsFormulaires = (new Atexo_Publicite_AnnonceSub())->getChoixIdsFormulairesByTypesAnnoncesAndSupports($idsSupportsSelectionnes, $idTypeAnnonceSelectionne);
            $dataSource = (new Atexo_Publicite_AnnonceSub())->getListeFormulaires($choixIdsFormulaires);
            $logger->info("Formulaires proposes a l'agent = ".print_r($dataSource, true));
            $this->remplirListeFormulaires($dataSource);
        } catch (Exception $e) {
            $logger->error('Erreur lors du pre-remplissage de la liste des formulaires : '.$e->getMessage()."\n Methode = AtexoFormulaireAjoutPublicite::preRemplirListeFormulaires");
        }
    }

    /**
     * Permet de remplir le referentiel des formulaires de publicite.
     *
     * @param array $dataSource
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function remplirListeFormulaires($dataSource = null)
    {
        try {
            if (empty($dataSource)) {
                $dataSource = (new Atexo_Publicite_AnnonceSub())->getListeFormulaires();
            }
            $this->formulaire->dataSource = $dataSource;
            $this->formulaire->dataBind();
        } catch (Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('publicite');
            $logger->error('Erreur lors du remplissage de la liste des formulaires : '.$e->getMessage()."\n Methode = AtexoFormulaireAjoutPublicite::remplirListeFormulaires");
        }
    }

    /**
     * Precise si le support passe en parametre doit etre coche ou non.
     *
     * @param string $idSupport : identifiant du support
     *
     * @return bool : true si support a cocher, false sinon
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function cocherSupportPublication($idSupport)
    {
        try {
            $supportsPreferes = $this->getIdsSupportsPreferesAgent();
            if (!empty($supportsPreferes) && !empty($supportsPreferes[$idSupport])) {
                return true;
            }

            return false;
        } catch (Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('publicite');
            $logger->error("Erreur lors de la coche par defaut des supports preferes de l'agent : Erreur = ".$e->getMessage()."\n Methode = AtexoFormulaireAjoutPublicite::cocherSupportPublication");
        }
    }

    /**
     * Permet de sauvegarder les ids des supports preferes de l'agent dans la session de la page.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function saveIdsSupportsPreferesAgent()
    {
        $dataSource = null;
        try {
            $idsSupports = [];
            if (empty($dataSource)) {
                $supports = (new Atexo_Publicite_AnnonceSub())->getPreferencesSupportsAgent(Atexo_CurrentUser::getIdAgentConnected(), Atexo_CurrentUser::getCurrentOrganism());
                if (!empty($supports)) {
                    foreach ($supports as $support) {
                        if ($support instanceof CommonTPreferenceSupportPublication) {
                            $idsSupports[$support->getIdSupport()] = $support->getIdSupport();
                        }
                    }
                }
            }
            $this->setViewState('idsSupportsPreferesAgent', $idsSupports);
        } catch (Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('publicite');
            $logger->error("Erreur lors de la sauvegarde des ids des supports preferes de l'agent dans la session de la page : Id_Agent = ".Atexo_CurrentUser::getIdAgentConnected().' , Organisme = '.Atexo_CurrentUser::getCurrentOrganism().' , Erreur = '.$e->getMessage()."\n Methode = AtexoFormulaireAjoutPublicite::saveIdsSupportsPreferesAgent");
        }
    }

    /**
     * Permet de recuperer les ids des supports preferes de l'agent depuis la session de la page.
     *
     * @return array
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function getIdsSupportsPreferesAgent()
    {
        return $this->getViewState('idsSupportsPreferesAgent');
    }
}
