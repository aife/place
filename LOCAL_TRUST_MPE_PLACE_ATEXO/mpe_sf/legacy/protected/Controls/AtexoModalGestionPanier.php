<?php

namespace Application\Controls;

use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonPanierEntreprise;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_PaniersEntreprises;
use Exception;
use Prado\Prado;

/**
 * Page de creation compte entreprise et compte utilisateur.
 *
 * @author Mouslim MITALI <anis.abid@atexo.com>
 * @copyright Atexo 2017
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class AtexoModalGestionPanier extends MpeTTemplateControl
{
    public bool $visible = false;
    public string $message = '';

    /**
     * @return mixed
     */
    public function getVisible($checkParents = true)
    {
        return $this->visible;
    }

    /**
     * @param mixed $visible
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    public function deleteAction($confirmation = false)
    {
        if (!$confirmation) {
            $consultation = (new Atexo_Consultation())->retrieveConsultation(Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id'])), Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']));
            if ($consultation instanceof CommonConsultation) {
                $panierEntreprise = Atexo_Entreprise_PaniersEntreprises::getPanierEntreprise(Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']), Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id'])), Atexo_CurrentUser::getIdEntreprise(), Atexo_CurrentUser::getIdInscrit());
                if ($panierEntreprise instanceof CommonPanierEntreprise) {
                    $this->setMessage(str_replace('[__REF_UTILISATEUR_]', '<strong>'.$consultation->getReferenceUtilisateur().'</strong>', Prado::localize('MESSAGE_SUPPRESSION_CONSULTATION_PANIER')));
                }
            }
        } else {
            $consultation = (new Atexo_Consultation())->retrieveConsultation(Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id'])), Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']));
            if ($consultation instanceof CommonConsultation) {
                $panierEntreprise = Atexo_Entreprise_PaniersEntreprises::getPanierEntreprise(Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']), Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id'])), Atexo_CurrentUser::getIdEntreprise(), Atexo_CurrentUser::getIdInscrit());
                if ($panierEntreprise instanceof CommonPanierEntreprise) {
                    try {
                        if (Atexo_Entreprise_PaniersEntreprises::deletePanierEntreprise(Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']), Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id'])), Atexo_CurrentUser::getIdEntreprise(), Atexo_CurrentUser::getIdInscrit())) {
                            $return = ['status' => true, 'id' => $_GET['id'], 'idRow' => $_GET['idRow']];
                        } else {
                            $return = ['status' => false, 'id' => $_GET['id'], 'idRow' => $_GET['idRow']];
                        }
//                        /echo json_encode($return);
                    } catch (Exception $e) {
                        throw $e;
                    }
                }
            }
        }
    }
}
