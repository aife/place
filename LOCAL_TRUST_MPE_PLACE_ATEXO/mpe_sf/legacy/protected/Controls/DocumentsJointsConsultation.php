<?php

namespace Application\Controls;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonComplement;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonDCE;
use Application\Propel\Mpe\CommonProcedureEquivalence;
use Application\Propel\Mpe\CommonRG;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_Crypto;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Document;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_ScanAntivirus;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Complements;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Dce;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureEquivalence;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Rg;
use Application\Service\Atexo\Consultation\Atexo_Consultation_SuppliersOfDocuments;
use Application\Service\Atexo\DossierVolumineux\Atexo_DossierVolumineux_Responses;
use Application\Service\Atexo\Dume\Atexo_Dume_AtexoDume;
use Application\Service\Atexo\Exception\ExceptionDceLimit;
use Exception;
use Prado\Prado;

/**
 * Page de gestion des pieces jointes de la consultation.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 4
 */
class DocumentsJointsConsultation extends MpeTTemplateControl
{
    public $consultation;
    public $_mode;
    public ?string $_affiche = null; // (=1 pour afficher, 0 sinon)
    public ?object $_dce = null;
    public ?object $_rg = null;
    public $_complement;
    public $_composantDCE;
    public ?string $errorMessageDce = null;

    /**
     * recupere la consultation.
     */
    public function getConsultation()
    {
        if (!($this->consultation instanceof CommonConsultation)) {
            $this->consultation = $this->Page->getConsultation();
        }

        return $this->consultation;
    }

    /**
     * Affecte l'objet de la consultation.
     *
     * @param objet $consultation
     */
    public function setConsultation($consultation)
    {
        if ($this->consultation !== $consultation) {
            $this->consultation = $consultation;
        }
    }

    // setConsultation()

    /**
     * recupere le mode.
     */
    public function getMode()
    {
        if (isset($_GET['id'])) {
            $this->setMode(Atexo_Config::getParameter('MODE_MODIFICATION_CONSULTATION'));
        } else {
            $this->setMode(Atexo_Config::getParameter('MODE_CREATION_CONSULTATION'));
        }

        return $this->mode;
    }

    // getMode()

    /**
     * Affecte le mode.
     *
     * @param string $mode
     */
    public function setMode($mode)
    {
        if ($this->mode !== $mode) {
            $this->mode = $mode;
        }
    }

    // setMode()

    /**
     * recupere la valeur.
     */
    public function getAffiche()
    {
        return $this->_affiche;
    }

    // getAffiche()

    /**
     * Affecte la valeur.
     *
     * @param string $value
     */
    public function setAffiche($value)
    {
        if ($this->_affiche !== $value) {
            $this->_affiche = $value;
        }
    }

    // setAffiche()

    public function setDownlodDceNavigateUrl($consultationId)
    {
        $this->DownloadDce->NavigateUrl = 'index.php?page=Agent.DownloadDce&id='.base64_encode($consultationId);
    }

    public function setDownlodRcNavigateUrl($consultationId)
    {
        $this->DownloadRc->NavigateUrl = 'index.php?page=Agent.DownloadReglement&id='.base64_encode($consultationId);
    }

    public function setDownlodAutrePieceNavigateUrl($consultationId)
    {
        $this->DownloadAutrePiece->NavigateUrl = 'index.php?page=Agent.AgentDownloadComplement&id='.base64_encode($consultationId);
    }

    /**
     * Chargement du composant.
     */
    public function onLoad($param)
    {
        if (!$this->Page->isCallBack) {
            $this->chargerComposants();
        }
    }

    public function saveDce(&$consultation, $modification = true)
    {
        $zip_filename = null;
        // Debut DCE
        try {
            $atexoBlob = new Atexo_Blob();
            $atexoCrypto = new Atexo_Crypto();
            if (!$modification) {
                if ($this->uploadBarDce->hasFile()) {
                    $arrayTimeStamp = $atexoCrypto->timeStampData('DCE NON RENSEIGNEE');
                    if (is_array($arrayTimeStamp)) {
                        $organisme = $consultation->getOrganisme();
                        $dceO = new CommonDCE();
                        $dceO->setDce('0');
                        $dceO->setHorodatage($arrayTimeStamp['horodatage']);
                        $dceO->setUntrusteddate($arrayTimeStamp['untrustedDate']);
                        $dceO->setOrganisme(Atexo_CurrentUser::getOrganismAcronym());
                        $dceO->setAgentId(Atexo_CurrentUser::getIdAgentConnected());
                        $dceO->setStatut(Atexo_Config::getParameter('CREATION_FILE'));
                        $consultation->addCommonDCE($dceO);
                    } else {
                        $this->afficherPanelMessageErreur("Probleme d'horodatage du fichier DCE");

                        return false;
                    }
                } elseif ('1' == $this->hasDce->Value) {
                    $this->afficherPanelMessageErreur('Probleme de taille du fichier DCE');

                    return false;
                }
            } else {
                if ($consultation instanceof CommonConsultation) {
                    $organisme = $consultation->getOrganisme();
                    $consultationId = $consultation->getId();
                    $this->_dce = (new Atexo_Consultation_Dce())->getDce($consultationId, $organisme);
                    // ceation DCE
                    if ($this->getMode() == Atexo_Config::getParameter('MODE_CREATION_CONSULTATION')) {
                        if ($this->uploadBarDce->hasFile()) {
                            if ('ZIP' == strtoupper(Atexo_Util::getExtension($this->uploadBarDce->getFileName()))) {
                                $infileDce = $this->uploadBarDce->getLocalName();
                                $arrayTimeStampDCE = $atexoCrypto->timeStampFile($infileDce);
                                if (is_array($arrayTimeStampDCE)) {
                                    $dceO = new CommonDCE();
                                    $dceO->setHorodatage($arrayTimeStampDCE['horodatage']);
                                    $dceO->setUntrusteddate($arrayTimeStampDCE['untrustedDate']);
                                    $dceO->setOrganisme($organisme);
                                    $dceO->setAgentId(Atexo_CurrentUser::getIdAgentConnected());
                                    $dceIdBlob = $atexoBlob->insert_blob($this->uploadBarDce->getFileName(), $infileDce, $organisme, null, Atexo_Config::getParameter('EXTENSION_DCE'));
                                    if ('0' != $dceIdBlob) {
                                        $dceO->setDce($dceIdBlob);
                                        $dceO->setNomDce(Atexo_Util::atexoHtmlEntities((new Atexo_Config())->toPfEncoding($this->uploadBarDce->getFileName())));
                                        $dceO->setNomFichier(Atexo_Util::atexoHtmlEntities((new Atexo_Config())->toPfEncoding($this->uploadBarDce->getFileName())));
                                        $dceO->setStatut(Atexo_Config::getParameter('AJOUT_FILE'));
                                        $consultation->addCommonDCE($dceO);
                                    } else {
                                        $this->afficherPanelMessageErreur("Probleme d'enregistrement du fichier DCE");

                                        return false;
                                    }
                                } else {
                                    $this->afficherPanelMessageErreur("Probleme d'horodatage du fichier DCE");

                                    return false;
                                }
                            } else {
                                $this->afficherPanelMessageErreur('Le dossier de la consultation (DCE) doit etre compresse au format ZIP');

                                return false;
                            }
                        } else {
                            if ('1' == $this->hasDce->Value) {
                                $this->afficherPanelMessageErreur('Probleme de taille du fichier DCE');

                                return false;
                            }
                        }

                        //Telechargement Partiel
                        if ($this->telechargementPartiel->Checked) {
                            $consultation->setPartialDceDownload('1');
                        } else {
                            $consultation->setPartialDceDownload('0');
                        }

                        //Modification DCE
                    } elseif ($this->getMode() == Atexo_Config::getParameter('MODE_MODIFICATION_CONSULTATION')
                        //&& (Atexo_CurrentUser::hasHabilitation('ModifierConsultationAvantValidation') || $consultation->hasHabilitationModifierApresValidation())
                    ) {
                        if (Atexo_Module::isEnabled('InterfaceModuleRsem') && ('1' == Atexo_Config::getParameter('CONFIG_REDAC'))) {
                            if (empty(Atexo_Config::getParameter('ACTIVE_ELABORATION_V2'))) {
                                $this->_composantDCE = $this->dceComposant;
                                $composantDCE = $this->dceComposant;
                            }
                        } else {
                            $this->_composantDCE = $this->blocDce;
                            $composantDCE = $this->blocDce;
                        }
                        //Pour modifier le DCE
                        $dce = $this->_dce;
                        $createZip = false;
                        $zip_filename = Atexo_Config::getParameter('COMMON_TMP').'zip_tmp_update_'.session_id().'_'.md5(uniqid(random_int(0, mt_getrandmax()), true)).'.zip';
                        if ($dce instanceof CommonDCE && '0' != $dce->getDce()) {
                            // On copie l'ancien DCE dans un fichier temp
                            Atexo_Blob::copyBlob($dce->getDce(), $zip_filename, $organisme);
                            $composantDCE->_nomFileDce = $dce->getNomDce();
                        } else {
                            $createZip = true;
                            $composantDCE->_nomFileDce = preg_replace('/[\/\\\\:\*\?"<>|]/', '', $consultationId).'.zip';
                        }

                        $finalDce = '';
                        $finalDceName = '';

                        if (Atexo_Module::isEnabled('InterfaceModuleRsem') && ('1' == Atexo_Config::getParameter('CONFIG_REDAC'))) {
                            $composantDCE->createZip = $createZip;
                            $finalDce = $composantDCE->getNewZipDCE($zip_filename);
                        } else {
                            $finalDce = $composantDCE->getNewZipDCE();
                        }
                        $finalDceName = $composantDCE->_nomFileDce;

                        //--------- COMMENTAIRES IMPORTANTS A NE PAS SUPPRIMER !!! ---------
                        //$hasFile specifie si une piece est selectionnee dans la "barre de selection" par l'utilisateur
                        //$composantDCE->uploadBar->hasFile(): si "Remplacer tout le DCE" est selectionnee
                        //$composantDCE->uploadBarNewPieceDce->hasFile(): si "Ajouter une piece au DCE" est selectionnee, seulement dans le cas: "Module Redac" desactive
                        //$this->remplacerPieceDce->value: si "Remplacer une piece du DCE" est selectionnee, dans le cas "module REDAC" desactive
                        //$this->remplacerPieceDce->value est rempli dans le fichier atexoScript.js.php, fonction: getFileChange()
                        if (Atexo_Module::isEnabled('InterfaceModuleRsem') && ('1' == Atexo_Config::getParameter('CONFIG_REDAC'))) {
                            //                  if($composantDCE->hasFile()){
                            $hasFile = true;
                            //                  } else {
                            //                      $hasFile = false;
                            //                  }
                        } else {
                            $hasFile = $composantDCE->uploadBar->hasFile() ||
                                ($composantDCE->uploadBarNewPieceDce->hasFile() || $this->remplacerPieceDansDce->value);
                            //$composantDCE->hasFile() TODO
                        }
                        if ($hasFile && '' !== $finalDce) {
                            $atexoCrypto = new Atexo_Crypto();
                            $atexoBlob = new Atexo_Blob();
                            $arrayTimeStampDCE = $atexoCrypto->timeStampFile($finalDce);
                            if (is_array($arrayTimeStampDCE)) {
                                $dceIdBlob = $atexoBlob->insert_blob($finalDceName, $finalDce, $organisme, null, Atexo_Config::getParameter('EXTENSION_DCE'));
                                if ($dceIdBlob) {
                                    $dceO = new CommonDCE();
                                    $dceO->setOrganisme($organisme);
                                    $dceO->setHorodatage($arrayTimeStampDCE['horodatage']);
                                    $dceO->setUntrusteddate(($arrayTimeStampDCE['untrustedDate']));
                                    $dceO->setAgentId(Atexo_CurrentUser::getId());
                                    $dceO->setDce($dceIdBlob);
                                    $dceO->setNomDce(Atexo_Util::atexoHtmlEntities((new Atexo_Config())->toPfEncoding($finalDceName)));
                                    $dceExiste = false;
                                    if ($this->_dce instanceof CommonDCE && '0' != $this->_dce->getDce()) {
                                        $dceExiste = true;
                                    }
                                    if ($composantDCE->statut == Atexo_Config::getParameter('REMPLACEMENT_FILE')) {
                                        $dceO->setNomFichier(Atexo_Util::atexoHtmlEntities((new Atexo_Config())->toPfEncoding($composantDCE->_fileAdddedName)));
                                        $dceO->setAncienFichier(Atexo_Util::atexoHtmlEntities((new Atexo_Config())->toPfEncoding($composantDCE->_oldfileName)));
                                    } elseif ($composantDCE->statut == Atexo_Config::getParameter('AJOUT_FILE') && $dceExiste) {
                                        $dceO->setNomFichier(Atexo_Util::atexoHtmlEntities((new Atexo_Config())->toPfEncoding($composantDCE->_fileAdddedName)));
                                    } else {
                                        $dceO->setNomFichier(Atexo_Util::atexoHtmlEntities((new Atexo_Config())->toPfEncoding($finalDceName)));
                                    }
                                    $dceO->setStatut($composantDCE->statut);
                                    $consultation->addCommonDCE($dceO);
                                }
                            }
                        }

                        //Telechargement Partiel
                        if ($composantDCE->telechargementPartiel->Checked) {
                            $consultation->setPartialDceDownload('1');
                        } else {
                            $consultation->setPartialDceDownload('0');
                        }
                        //Reinitialiser le composant lors de la modification
                        $composantDCE->uploadBar->refrechCompsant();
                    }
                }
            }
            //Reinitialiser le composant lors de l'ajout
            $this->uploadBarDce->refrechCompsant();
            if (isset($zip_filename) && is_file($zip_filename)) {
                unlink($zip_filename);
            }

            return true;
        } catch (ExceptionDceLimit $e) {
            throw $e;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    // enregistrement RG
    public function saveRg(&$consultation, $modification = true)
    {
        // Debut RG
        try {
            if ($consultation instanceof CommonConsultation) {
                if (!$modification) {
                    if ($this->uploadBarRg->hasFile()) {
                        $atexoCrypto = new Atexo_Crypto();
                        $arrayTimeStamp = $atexoCrypto->timeStampData('RG NON RENSEIGNEE');
                        if (is_array($arrayTimeStamp)) {
                            $rgO = new CommonRG();
                            $rgO->setHorodatage($arrayTimeStamp['horodatage']);
                            $rgO->setUntrusteddate($arrayTimeStamp['untrustedDate']);
                            $rgO->setOrganisme($consultation->getOrganisme());
                            $rgO->setAgentId(Atexo_CurrentUser::getIdAgentConnected());
                            $rgO->setRg('0');
                            $rgO->setStatut(Atexo_Config::getParameter('CREATION_FILE'));
                            $consultation->addCommonRG($rgO);
                        } else {
                            $this->afficherPanelMessageErreur("Probleme d'horodatage du fichier RC");

                            return false;
                        }
                    } elseif ('1' == $this->hasRg->Value) {
                        $this->afficherPanelMessageErreur('Probleme de taille du fichier reglement de la consultation');

                        return false;
                    }
                } else {
                    $organisme = $consultation->getOrganisme();
                    $consultationId = $consultation->getId();
                    $this->_rg = (new Atexo_Consultation_Rg())->getReglement($consultationId, $organisme);
                    // ceation RG
                    if ($this->getMode() == Atexo_Config::getParameter('MODE_CREATION_CONSULTATION')) {
                        //$this->_statutHistorique = Atexo_Config::getParameter('CREATION_FILE');
                        if (Atexo_Module::isEnabled('ReglementConsultation')) {
                            if ($this->uploadBarRg->hasFile()) {
                                $infileRg = Atexo_Config::getParameter('COMMON_TMP').'rg'.session_id().time();
                                if (copy($this->uploadBarRg->getLocalName(), $infileRg)) {
                                    if ($this->_rg instanceof CommonRG && '0' != $this->_rg->getRg()) {
                                        $statut = Atexo_Config::getParameter('REMPLACEMENT_FILE');
                                    } else {
                                        $statut = Atexo_Config::getParameter('AJOUT_FILE');
                                    }
                                    $return = $this->insertDocReglement($consultation, $statut, $infileRg, $this->uploadBarRg->getFileName());
                                    if (!$return) {
                                        return false;
                                    }
                                } else {
                                    $this->afficherPanelMessageErreur('Probleme de taille du fichier reglement de la consultation');

                                    return false;
                                }
                            } else {
                                if ('1' == $this->hasRg->Value) {
                                    $this->afficherPanelMessageErreur('Probleme de taille du fichier reglement de la consultation');

                                    return false;
                                }
                            }
                        }
                        //Modification RG
                    } elseif ($this->getMode() == Atexo_Config::getParameter('MODE_MODIFICATION_CONSULTATION' && empty(Atexo_Config::getParameter('ACTIVE_ELABORATION_V2')))
                        //&& (Atexo_CurrentUser::hasHabilitation('ModifierConsultationAvantValidation') || $consultation->hasHabilitationModifierApresValidation())
                    ) {
                        if (Atexo_Module::isEnabled('InterfaceModuleRsem') && ('1' == Atexo_Config::getParameter('CONFIG_REDAC'))) {
                            $composantDCE = $this->dceComposant;
                        } else {
                            $composantDCE = $this->blocDce;
                        }
                        if (Atexo_Module::isEnabled('ReglementConsultation')) {
                            $infoFileRG = [];
                            $infoFileRG = $composantDCE->getInfoFileRG();
                            if ($this->blocDce->uploadBarNewReglement->hasFile()) {
                                $msg = Atexo_ScanAntivirus::startScan($infoFileRG['infile']);
                                if ($msg) {
                                    $this->afficherPanelMessageErreur($msg.' "'.$infoFileRG['fileName'].'"');

                                    return false;
                                }
                            }
                            if (is_countable($infoFileRG) ? count($infoFileRG) : 0) {
                                $inFileRG = $infoFileRG['infile'];
                                if ($this->_rg instanceof CommonRG && '0' != $this->_rg->getRg()) {
                                    $statut = Atexo_Config::getParameter('REMPLACEMENT_FILE');
                                } else {
                                    $statut = Atexo_Config::getParameter('AJOUT_FILE');
                                }

                                $return = $this->insertDocReglement($consultation, $statut, $inFileRG, $infoFileRG['fileName']);
                                if (!$return) {
                                    return false;
                                }
                            }
                        }
                        //Reinitialiser le composant lors de la modification
                        if (!Atexo_Module::isEnabled('InterfaceModuleRsem') || ('1' != Atexo_Config::getParameter('CONFIG_REDAC'))) {
                            $composantDCE->uploadBarNewReglement->refrechCompsant();
                        }
                    }
                }
            }
            //Reinitialisation du composant lors de la creation
            $this->uploadBarRg->refrechCompsant();

            return true;
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        // FIN RG
    }

    public function saveComplement(&$consultation)
    {
        // Debut Complement
        try {
            if ($consultation instanceof CommonConsultation) {
                $organisme = $consultation->getOrganisme();
                $consultationId = $consultation->getId();
                $this->_complement = (new Atexo_Consultation_Complements())->getComplement($consultationId, $organisme);
                if ($this->uploadBarAutrePiece->hasFile()) {
                    $atexoBlob = new Atexo_Blob();
                    $atexoCrypto = new Atexo_Crypto();
                    $infileAutrePiece = Atexo_Config::getParameter('COMMON_TMP').'cp'.session_id().time();
                    if (copy($this->uploadBarAutrePiece->getLocalName(), $infileAutrePiece)) {
                        $arrayTimeStampComplement = $atexoCrypto->timeStampFile($infileAutrePiece);
                        if (is_array($arrayTimeStampComplement)) {
                            $autrePieceIdBlob = $atexoBlob->insert_blob($this->uploadBarAutrePiece->getFileName(), $infileAutrePiece, $organisme);
                            if ('0' != $autrePieceIdBlob) {
                                $complementO = new CommonComplement();
                                $complementO->setHorodatage($arrayTimeStampComplement['horodatage']);
                                $complementO->setUntrusteddate($arrayTimeStampComplement['untrustedDate']);
                                $complementO->setOrganisme($organisme);
                                $complementO->setAgentId(Atexo_CurrentUser::getIdAgentConnected());
                                $complementO->setComplement($autrePieceIdBlob);
                                $complementO->setNomFichier($this->uploadBarAutrePiece->getFileName());
                                if ($this->_complement instanceof CommonComplement) {
                                    $complementO->setStatut(Atexo_Config::getParameter('REMPLACEMENT_FILE'));
                                } else {
                                    $complementO->setStatut(Atexo_Config::getParameter('CREATION_FILE'));
                                }
                                $consultation->addCommonComplement($complementO);
                            } else {
                                $this->afficherPanelMessageErreur("Problème d'enregistrement du fichier Autre pi?ce");

                                return false;
                            }
                        } else {
                            $this->afficherPanelMessageErreur("Problème d'horodatage du fichier Autre pi?ce");

                            return false;
                        }
                    } else {
                        $this->afficherPanelMessageErreur('Problème de taille du fichier Autre pi?ce');

                        return false;
                    }
                } else {
                    if ('1' == $this->hasAutrePiece->Value) {
                        $this->afficherPanelMessageErreur('Problème de taille du fichier Autre pi?ce');

                        return false;
                    }
                }
            }
            //Reinitialisation du composant lors de la creation
            $this->uploadBarAutrePiece->refrechCompsant();
            $this->uploadBarAutrePiece->removeTmpFile();

            return true;
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        // Fin Complement
    }

    /**
     * sauvegarde de composant.
     */
    public function save(&$consultation, $modification = true)
    {
        try {
            // Enregistrement téléchargement partiel
            $this->savePartialDownload($consultation);

            if ($this->scanFiles()) {
                if (empty(Atexo_Config::getParameter('ACTIVE_ELABORATION_V2'))) {
                    //Enregistrement "DCE"
                    $return = $this->saveDce($consultation, $modification);
                    if (!$return) {
                        return false;
                    }
                    //Enregistrement "RG"
                    if (Atexo_Module::isEnabled('ReglementConsultation')) {
                        $return = $this->saveRg($consultation, $modification);
                        if (!$return) {
                            return false;
                        }
                    }
                }
                //Enregistrement "AUTRES PIECES TELECHARGEABLES (COMPLEMENTS)"
                if (empty(Atexo_Config::getParameter('ACTIVE_ELABORATION_V2')) && Atexo_Module::isEnabled('ConsultationPjAutresPiecesTelechargeables')) {
                    $return = $this->saveComplement($consultation);
                    if (!$return) {
                        return false;
                    }
                }
                if (empty(Atexo_Config::getParameter('ACTIVE_ELABORATION_V2'))) {
                    // enregistrement des info : Acces aux informations / DCE par les entreprises
                    if ($this->accesPublic->Checked) {
                        $typeProcedure = 1;
                        $accesCode = '';
                    } elseif ($this->accesRestreint->Checked) {
                        $typeProcedure = 2;
                        $accesCode = trim((new Atexo_Config())->toPfEncoding($this->accessCode->Text));
                    } else {
                        $typeProcedure = 1;
                        $accesCode = '';
                    }
                    $typeAccesOld = $consultation->getTypeAcces();
                    $consultation->setTypeAcces($typeProcedure);

                    $consultation->setNumProcedure($typeProcedure);
                    $consultation->setCodeProcedure(trim($accesCode));
                }

                $consultation->setCodeDceRestreint(null);
                if ($this->accesRestreintDCE->Checked) {
                    $consultation->setCodeDceRestreint(trim((new Atexo_Config())->toPfEncoding($this->accessCodeDCE->Text)));
                }


                // enregistrement des info : Gestion des envois postaux complementaires
                $consultation->setTiragePlan('0');
                if (!$this->noEnvoisPostaux->Checked && $this->envoiType->Checked) {
                    $tiragePlan = 0;
                    if ($this->papier->Checked) {
                        ++$tiragePlan;
                    }
                    if ($this->cdRom->Checked) {
                        $tiragePlan += 2;
                    }

                    $consultation->setTiragePlan($tiragePlan);
                    $consultation->setTirageDescriptif((new Atexo_Config())->toPfEncoding(Atexo_Util::atexoHtmlEntities($this->descriptifDoscComp->Text)));
                    $consultation->setTireurPlan($this->fournisseurDoc->getSelectedValue());
                }

                //Debut sauvegarde historique
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));

                if (empty(Atexo_Config::getParameter('ACTIVE_ELABORATION_V2'))) {
                    // debut : Historique de l'acces aux informations / DCE par les entreprises
                    $this->saveHistoriqueTypeAcces($typeAccesOld, $consultation, $connexion, Atexo_CurrentUser::getCurrentOrganism(), $modification);
                    // fin : Historique de l'acces aux informations / DCE par les entreprises
                }

                //Fin sauvegarde historique

                if (Atexo_Module::isEnabled('ModuleEnvol') && Atexo_CurrentUser::hasHabilitation('GestionEnvol')) {
                    $envol = ($this->annexeTechniqueDCE->autorisationDossiersVolumineuxEnvolNo->Checked) ? 'false' : 'true';
                    $consultation->setEnvolActivation($envol);
                    $dvOld = $consultation->getIdDossierVolumineux();
                    $consultation->setIdDossierVolumineux($this->annexeTechniqueDCE->getSelectedValue());
                    Atexo_DossierVolumineux_Responses::saveHistoriqueDossierVolumineux($dvOld, $consultation, $connexion, Atexo_CurrentUser::getCurrentOrganism(), $modification);
                }

                return true;
            } else {
                return false;
            }
        } catch (ExceptionDceLimit $e) {
            throw $e;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function scanFiles()
    {
        if ($this->uploadBarDce->hasFile()) {
            $msg = Atexo_ScanAntivirus::startScan($this->uploadBarDce->getLocalName());
            if ($msg) {
                $this->afficherPanelMessageErreur($msg.' "'.$this->uploadBarDce->getFileName().'"');

                return false;
            }
        }
        //Fin scan antivirus
        //Debut scan antivirus
        if (Atexo_Module::isEnabled('ReglementConsultation') && $this->uploadBarRg->hasFile()) {
            $msg = Atexo_ScanAntivirus::startScan($this->uploadBarRg->getLocalName());
            if ($msg) {
                $this->afficherPanelMessageErreur($msg.' "'.$this->uploadBarRg->getFileName().'"');

                return false;
            }
        }
        //Fin scan antivirus
        //Debut scan antivirus
        if (empty(Atexo_Config::getParameter('ACTIVE_ELABORATION_V2')) && Atexo_Module::isEnabled('ConsultationPjAutresPiecesTelechargeables') && $this->uploadBarAutrePiece->hasFile()) {
            $msg = Atexo_ScanAntivirus::startScan($this->uploadBarAutrePiece->getLocalName());
            if ($msg) {
                $this->afficherPanelMessageErreur($msg.' "'.$this->uploadBarAutrePiece->getFileName().'"');

                return false;
            }
        }

        return true;
    }

    // Methode pour inserer le reglement
    public function insertDocReglement(&$consultation, $statut, $infileRg, $fileName)
    {
        $atexoBlob = new Atexo_Blob();
        $atexoCrypto = new Atexo_Crypto();
        if ('' != $infileRg && '' != $fileName) {
            $arrayTimeStampRg = $atexoCrypto->timeStampFile($infileRg);
            if (is_array($arrayTimeStampRg)) {
                $rgIdBlob = $atexoBlob->insert_blob($fileName, $infileRg, $consultation->getOrganisme(), null, Atexo_Config::getParameter('EXTENSION_RC'));
                if ('0' != $rgIdBlob) {
                    $rgO = new CommonRG();
                    $rgO->setHorodatage($arrayTimeStampRg['horodatage']);
                    $rgO->setUntrusteddate($arrayTimeStampRg['untrustedDate']);
                    $rgO->setOrganisme($consultation->getOrganisme());
                    $rgO->setAgentId(Atexo_CurrentUser::getIdAgentConnected());
                    $rgO->setRg($rgIdBlob);
                    $rgO->setNomFichier($fileName);
                    $rgO->setStatut($statut);
                    $consultation->addCommonRG($rgO);
                } else {
                    $this->afficherPanelMessageErreur("Probleme d'enregistrement du fichier reglement de la consultation");

                    return false;
                }
            } else {
                $this->afficherPanelMessageErreur("Probleme d'horodatage du fichier reglement de la consultation");

                return false;
            }
        }

        return true;
    }

    /**
     * Permet d'afficher/masquer des blocs en fonction des valeurs parametrees dans les procedures equivalences.
     *
     * @param CommonConsultation $consultation
     */
    public function afficherMasquerBlocsEnFonctionParametrageProcedures($consultation = false, $procEquivalence = false)
    {
        if (!$procEquivalence) {
            $procEquivalence = Atexo_Consultation_ProcedureEquivalence::retrieveProcedureEquivalenceById($consultation->getIdTypeProcedureOrg());
            $fromProcedure = false;
        } else {
            $fromProcedure = true;
        }
        if ($procEquivalence instanceof CommonProcedureEquivalence) {
            if ($this->getMode() == Atexo_Config::getParameter('MODE_CREATION_CONSULTATION')) {
                //DCE
                $this->displayElement('uploadBarDce', $procEquivalence->getDossierDce(), false, $fromProcedure);
                $this->displayElement('telechargementPartiel', $procEquivalence->getPartialDceDownload(), true, $fromProcedure);
                //RC
                if (Atexo_Module::isEnabled('ReglementConsultation')) {
                    $this->displayElement('uploadBarRg', $procEquivalence->getReglementCons(), false, $fromProcedure);
                }
            } else {
                //RC
                if (Atexo_Module::isEnabled('ReglementConsultation') && '-0' === $procEquivalence->getReglementCons() && empty(Atexo_Config::getParameter('ACTIVE_ELABORATION_V2'))) {
                    $this->dceComposant->panelRc->style = 'display:none';
                }
            }
            if (Atexo_Module::isEnabled('ReglementConsultation')) {
                if ('+1' === $procEquivalence->getReglementCons()
                    || '+0' === $procEquivalence->getReglementCons()
                    || '0' === $procEquivalence->getReglementCons()
                    || '1' === $procEquivalence->getReglementCons()) {
                    $this->blocDce->panelRg->style = 'display:block';
                } else {
                    $this->blocDce->panelRg->Attributes->Style = 'display:none';
                }
                if ('0' === $procEquivalence->getReglementCons()) {
                    $this->blocDce->uploadBarNewReglement->SetEnabled(false);
                }
            }
            //COMPLEMENTS
            if (empty(Atexo_Config::getParameter('ACTIVE_ELABORATION_V2')) && Atexo_Module::isEnabled('ConsultationPjAutresPiecesTelechargeables')) {
                $this->displayElement('uploadBarAutrePiece', $procEquivalence->getAutrePieceCons(), false, $fromProcedure);
                if ('-0' === $procEquivalence->getAutrePieceCons()) {
                    $this->panelmoduleAutrePiece->setDisplay('None');
                }
            }

            //AUTRES BLOCS DE LA PAGE
            if ('-0' === $procEquivalence->getConstitutionDossierReponse() || '-1' === $procEquivalence->getConstitutionDossierReponse()) {
                $this->blocTypeEnvoi->setDisplay('None');
            }
            if (('-0' === $procEquivalence->getGestionEnvoisPostaux() && '-0' === $procEquivalence->getConstitutionDossierReponse()) || ('-1' === $procEquivalence->getGestionEnvoisPostaux() && '-1' === $procEquivalence->getConstitutionDossierReponse())) {
                $this->blocEnvoiPostaux->setStyle('display:none');
            }

            if (empty(Atexo_Config::getParameter('ACTIVE_ELABORATION_V2'))) {
                $this->panelAccesPublicRestreint->setStyle('NULL');
                $this->displayElement('accesRestreint', $procEquivalence->getProcedureRestreinte(), true, $fromProcedure, false, 'panelAccesPublicRestreint');
                $this->displayElement('accesPublic', $procEquivalence->getProcedurePublicite(), true, $fromProcedure, false, 'panelAccesPublicRestreint');
            }

            $this->blocEnvoiPostaux->setStyle('NULL');
            $this->displayElement('noEnvoisPostaux', $procEquivalence->getGestionEnvoisPostaux(), true, $fromProcedure, false, 'blocEnvoiPostaux');
            $this->displayElement('envoiType', $procEquivalence->getConstitutionDossierReponse(), true, $fromProcedure, false, 'blocEnvoiPostaux');
            $this->displayElement('par', $procEquivalence->getConstitutionDossierReponse(), false, $fromProcedure, false, 'blocEnvoiPostaux');
            $this->displayElement('descriptif', $procEquivalence->getConstitutionDossierReponse(), false, $fromProcedure, false, 'blocEnvoiPostaux');
            $this->displayElement('fournisseurDoc', $procEquivalence->getConstitutionDossierReponse(), false, $fromProcedure, false, 'blocEnvoiPostaux');
            $this->displayElement('descriptifDoscComp', $procEquivalence->getConstitutionDossierReponse(), false, $fromProcedure, false, 'blocEnvoiPostaux');
            $this->displayElement('papier', $procEquivalence->getTireurPlanPapier(), true, $fromProcedure, false, 'blocEnvoiPostaux');
            $this->displayElement('cdRom', $procEquivalence->getTireurPlanCdrom(), true, $fromProcedure, false, 'blocEnvoiPostaux');
        }
    }

    /*
     * permet de redessiner le panel du composant
     */
    public function refreshPanel($param)
    {
        $this->bloc_etapeDocumentsJoints->render($param->getNewWriter());
    }

    /**
     * Permet d'afficher/masquer des composants.
     *
     * @param $element: l'element a afficher/masquer
     * @param $values: valeur (+1= afficher+cocher ...)
     * @param $isChecked
     */
    public function displayElement($element, $values, $isChecked = true, $fromProcedure = false, $ligneAllElements = false, $panelGlobale = false)
    {
        $panel = 'panel'.$element;

        if (!strcmp($values, '0')) {
            $this->$panel->setStyle('display:');
            $this->$element->SetEnabled(false);
            if ($isChecked && $fromProcedure) {
                $this->$element->SetChecked(false);
            }
            if ($ligneAllElements && ('display:none' == $this->$ligneAllElements->getStyle()->getCustomStyle() || 'NULL' == $this->$ligneAllElements->getStyle()->getCustomStyle())) {
                $this->$ligneAllElements->setStyle('display:');
            }
            if ($panelGlobale && ('display:none' == $this->$panelGlobale->getStyle()->getCustomStyle() || 'NULL' == $this->$panelGlobale->getStyle()->getCustomStyle())) {
                $this->$panelGlobale->setStyle('display:');
            }
        } elseif (!strcmp($values, '1')) {
            $this->$panel->setStyle('display:');
            $this->$element->SetEnabled(false);
            if ($isChecked && $fromProcedure) {
                $this->$element->SetChecked(true);
            }
            if ($ligneAllElements && ('display:none' == $this->$ligneAllElements->getStyle()->getCustomStyle() || 'NULL' == $this->$ligneAllElements->getStyle()->getCustomStyle())) {
                $this->$ligneAllElements->setStyle('display:');
            }
            if ($panelGlobale && ('display:none' == $this->$panelGlobale->getStyle()->getCustomStyle() || 'NULL' == $this->$panelGlobale->getStyle()->getCustomStyle())) {
                $this->$panelGlobale->setStyle('display:');
            }
        } elseif (!strcmp($values, '+1')) {
            $this->$panel->setStyle('display:');
            if ($isChecked) {
                $this->$element->SetEnabled(true);
                if ($fromProcedure) {
                    $this->$element->SetChecked(true);
                }
            }
            if ($ligneAllElements && ('display:none' == $this->$ligneAllElements->getStyle()->getCustomStyle() || 'NULL' == $this->$ligneAllElements->getStyle()->getCustomStyle())) {
                $this->$ligneAllElements->setStyle('display:');
            }
            if ($panelGlobale && ('display:none' == $this->$panelGlobale->getStyle()->getCustomStyle() || 'NULL' == $this->$panelGlobale->getStyle()->getCustomStyle())) {
                $this->$panelGlobale->setStyle('display:');
            }
        } elseif (!strcmp($values, '+0')) {
            $this->$panel->setStyle('display:');
            if ($isChecked) {
                $this->$element->SetEnabled(true);
                if ($fromProcedure) {
                    $this->$element->SetChecked(false);
                }
            }
            if ($ligneAllElements && ('display:none' == $this->$ligneAllElements->getStyle()->getCustomStyle() || 'NULL' == $this->$ligneAllElements->getStyle()->getCustomStyle())) {
                $this->$ligneAllElements->setStyle('display:');
            }
            if ($panelGlobale && ('display:none' == $this->$panelGlobale->getStyle()->getCustomStyle() || 'NULL' == $this->$panelGlobale->getStyle()->getCustomStyle())) {
                $this->$panelGlobale->setStyle('display:');
            }
        } elseif (!strcmp($values, '-1')) {
            $this->$panel->setStyle('display:none');
            if ($isChecked && $fromProcedure) {
                $this->$element->SetChecked(true);
            }
            if ($ligneAllElements && ('NULL' == $this->$ligneAllElements->getStyle()->getCustomStyle())) {
                $this->$ligneAllElements->setStyle('display:none');
            }
            if ($panelGlobale && ('NULL' == $this->$panelGlobale->getStyle()->getCustomStyle())) {
                $this->$panelGlobale->setStyle('display:none');
            }
        } elseif (!strcmp($values, '-0')) {
            $this->$panel->setStyle('display:none');
            if ($isChecked && $fromProcedure) {
                $this->$element->SetChecked(false);
            }
            if ($ligneAllElements && ('NULL' == $this->$ligneAllElements->getStyle()->getCustomStyle())) {
                $this->$ligneAllElements->setStyle('display:none');
            }
            if ($panelGlobale && ('NULL' == $this->$panelGlobale->getStyle()->getCustomStyle())) {
                $this->$panelGlobale->setStyle('display:none');
            }
        }
    }

    public function afficherPanelMessageErreur($msg)
    {
        $this->Page->afficherErreur($msg);
    }

    public function chargerComposants()
    {
        $consultation = $this->getConsultation();

        if (empty(Atexo_Config::getParameter('ACTIVE_ELABORATION_V2'))) {
            $this->propositionAccesCode->Text = Atexo_Util::generateCode(8);
        }

        if (empty(Atexo_Config::getParameter('ACTIVE_ELABORATION_V2')) && Atexo_Module::isEnabled('ConsultationPjAutresPiecesTelechargeables')) {
            $this->uploadBarAutrePiece->refrechCompsant();
        }
        if (Atexo_Module::isEnabled('ReglementConsultation')) {
            $this->uploadBarRg->refrechCompsant();
        }
        $this->uploadBarDce->refrechCompsant();
        if ($this->getMode() == Atexo_Config::getParameter('MODE_CREATION_CONSULTATION')) {
            $this->blocDocJointsDCE->setVisible(true);
            $this->blocDceModif->setVisible(false);
            if (empty(Atexo_Config::getParameter('ACTIVE_ELABORATION_V2'))) {
                $this->blocDceRedac->setVisible(false);
            }
        } elseif ($consultation instanceof CommonConsultation) {
            $consultationId = $consultation->getId();
            $organisme = $consultation->getOrganisme();
            $this->blocDocJointsDCE->setVisible(false);
            $this->blocDceModif->setVisible(true);
            if (empty(Atexo_Config::getParameter('ACTIVE_ELABORATION_V2'))) {
                $this->blocDceRedac->setVisible(false);
            }
            $this->_dce = (new Atexo_Consultation_Dce())->getDce($consultationId, $organisme);
            if (Atexo_Module::isEnabled('ReglementConsultation')) {
                $this->_rg = (new Atexo_Consultation_Rg())->getReglement($consultationId, $organisme);
            }
            $this->_complement = (new Atexo_Consultation_Complements())->getComplement($consultationId, $organisme);

            if (Atexo_Module::isEnabled('InterfaceModuleRsem') && ('1' == Atexo_Config::getParameter('CONFIG_REDAC'))) {
                $this->blocDceModif->setVisible(false);
                if (empty(Atexo_Config::getParameter('ACTIVE_ELABORATION_V2'))) {
                    $this->blocDceRedac->setVisible(true);
                    $this->dceComposant->refrechCompsant();
                    $this->dceComposant->_consultation = $consultation;
                }
            } else {
                if (empty(Atexo_Config::getParameter('ACTIVE_ELABORATION_V2'))) {
                    $this->blocDceRedac->setVisible(false);
                    $this->blocDceModif->setVisible(true);
                } else {
                    $this->blocDceModif->setVisible(false);
                }
                $this->blocDce->setConsultation($this->getConsultation());
            }
            $this->remplirLienTelechargementDocsJoints($consultationId);
        }

        //Gerer l'affichage du bloc "panelmoduleAutrePiece"
        if (empty(Atexo_Config::getParameter('ACTIVE_ELABORATION_V2'))) {
            if (Atexo_Module::isEnabled('ConsultationPjAutresPiecesTelechargeables')) {
                $this->panelmoduleAutrePiece->visible = true;
            } else {
                $this->panelmoduleAutrePiece->visible = false;
            }
        }

        if ($consultation instanceof CommonConsultation) {
            //Afficher/masquer en fonction du parametrage des types de procedures
            //Merci de garder l'appel de la fonction afficherMasquerBlocsEnFonctionParametrageProcedures suivante en premier
            $this->afficherMasquerBlocsEnFonctionParametrageProcedures($consultation);
            if (empty(Atexo_Config::getParameter('ACTIVE_ELABORATION_V2'))) {
                if ('2' == $consultation->getTypeAcces()) {
                    $this->accesRestreint->Checked = true;
                    $this->accessCode->Text = $consultation->getCodeProcedure();
                    $this->accesPublic->Checked = false;
                } else {
                    $this->accesRestreint->Checked = false;
                    $this->accesPublic->Checked = true;
                }
            }

            if ('0' != $consultation->getTiragePlan()) {
                $this->envoiType->Checked = true;
                if ('1' == $consultation->getTiragePlan() || '3' == $consultation->getTiragePlan()) {
                    $this->papier->Checked = true;
                }
                if ($consultation->getTiragePlan() >= '2') {
                    $this->cdRom->Checked = true;
                }
            } else {
                $this->noEnvoisPostaux->Checked = true;
            }
            $this->displayListeFournisseurDoc($consultation->getServiceId());
            $this->annexeTechniqueDCE->displayListeDossierVolumineux($consultation->getIdDossierVolumineux(), $consultation);
            $this->descriptifDoscComp->Text = $consultation->getTirageDescriptif();
            $this->fournisseurDoc->SelectedValue = $consultation->getTireurPlan();

            if (!isset($_GET['id'])) {
                $procEquivalence = Atexo_Consultation_ProcedureEquivalence::retrieveProcedureEquivalenceById($consultation->getIdTypeProcedureOrg());
                if ($procEquivalence &&
                    (str_contains($procEquivalence->getAutorisationDossiersVolumineuxEnvolNon(), '-') ||
                        str_contains($procEquivalence->getAutorisationDossiersVolumineuxEnvolOui(), '-'))
                ) {
                    $this->annexeTechniqueDCE->autorisationDossiersVolumineuxEnvolNo->Enabled = false;
                    $this->annexeTechniqueDCE->autorisationDossiersVolumineuxEnvolYes->Enabled = false;
                }
            }

            $this->gestionAffichageReglementConsultationInfoMsg($consultation);

            if (Atexo_Module::isEnabled('InterfaceDume') && $consultation->getDumeDemande()) {
                $tDumecontext = Atexo_Dume_AtexoDume::getTDumeContexteStandard($consultation);
                if (!$tDumecontext) {
                    $this->panelDumeAcheteur->setDisplay('Dynamic');
                }
            }

            if (Atexo_Module::isEnabled('DceRestreint')) {
                $this->accesDCE->Checked = !(bool)$consultation->getCodeDceRestreint();
                $this->accesRestreintDCE->Checked = !$this->accesDCE->Checked;
                $this->blocAccesDCE->setDisplay('Dynamic');
            }
        }
    }

    public function verifyDceIntegrity($sender, $param)
    {
        $scriptJs = null;
        if ($this->verifiyDce() !== true ) {
            $errorDce = $this->verifiyDce();
            $errorMessageDce = ((is_countable($errorDce) ? count($errorDce) : 0) == 2) ? Prado::localize('MSG_ERREUR_ZIP_AND_SIZE_DCE_INVALIDE') : Prado::localize($errorDce[0]);
            $this->errorMessageDce = $errorMessageDce;
            $param->IsValid = false;
            $this->dceVerifyIntegrity->ErrorMessage = $errorMessageDce;

            if ('dceVerifyIntegrity' == $sender->ID) {
                $scriptJs = "document.getElementById('divValidationSummarySave').style.display='';";
                $this->Page->divValidationSummarySave->addServerError($errorMessageDce);
            } elseif ('dceVerifyIntegrityValidate' == $sender->ID) {
                $scriptJs = "document.getElementById('divValidationSummary').style.display='';";
                $this->Page->divValidationSummary->addServerError($errorMessageDce);
            }
            $scriptJs .= "document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';";
            $this->labelServerSide->Text = "<script>$scriptJs</script>";

            return;
        }
    }

    public function displayListeFournisseurDoc($idService)
    {
        $dataFournisseurs = [];
        // Debut remplissage de la liste des fournisseurs de docs
        $fournisseurs = Atexo_Consultation_SuppliersOfDocuments::search($idService);
        $dataFournisseurs[0] = '----- '.Prado::localize('DEFINE_SELECTIONNER_FOURNISSEUR_DOC').' -----';
        if ($fournisseurs) {
            foreach ($fournisseurs as $fournisseur) {
                $dataFournisseurs[$fournisseur->getId()] = $fournisseur->getNom().' ('.$fournisseur->getEmail().')';
            }
        }
        $this->fournisseurDoc->Datasource = $dataFournisseurs;
        $this->fournisseurDoc->dataBind();
        // Fin remplissage de la liste des fournisseurs de docs
    }

    /**
     * Permet de sauvegarder l'historique des types d'acces a la consultation cote operateurs economiques.
     *
     * @param $typeAccesOld: ancienne valeur du type d'acces a la consultation
     * @param $consultation: Objet consultation
     * @param $connexion: Objet connexion
     * @param $org: l'acronyme de l'organisme
     */
    public function saveHistoriqueTypeAcces($typeAccesOld, $consultation, $connexion, $org, $modification = true)
    {
        $arrayParameters = [];
        $typeAccesNew = $consultation->getTypeAcces();
        if ($typeAccesOld != $typeAccesNew || !$modification) {
            $nomElement = Atexo_Config::getParameter('ID_HISTORIQUE_ACCES_INFORMATIONS_DCE_ENTREPRISES');
            $valeur = '1';

            if ('2' == $typeAccesNew) {
                $detail1 = '0';
            } else {
                $detail1 = '1';
            }

            $detail2 = '';
            $lot = '0';
            $statut = '';
            if ($modification) {
                $statut = Atexo_Config::getParameter('MODIFICATION_FILE');
            } else {
                $statut = Atexo_Config::getParameter('CREATION_FILE');
            }
            //Sauvegarde
            $arrayParameters[0] = $consultation->getId();
            $arrayParameters[1] = $nomElement;
            $arrayParameters[2] = $valeur;
            $arrayParameters[3] = $detail1;
            $arrayParameters[4] = $detail2;
            $arrayParameters[5] = $lot;
            $arrayParameters[6] = $org;
            $arrayParameters[7] = $statut;
            (new Atexo_Consultation())->saveHistoriqueConsultation($arrayParameters, $connexion);
        }
    }

    /**
     * Ajoute le message d'erreur apres verification du DCE dans le array passe en params
     * utlise cote serveur ( Durant la validation  du formulaire de la consultation).
     */
    public function validerDocumentsJoint(&$isValid, &$arrayMsgErreur)
    {
        if ($this->verifiyDce() !== true) {
            $errorDce = $this->verifiyDce();
            $errorMessage = ((is_countable($errorDce) ? count($errorDce) : 0) == 2) ? Prado::localize('MSG_ERREUR_ZIP_AND_SIZE_DCE_INVALIDE') : Prado::localize($errorDce[0]);
            $isValid = false;
            $arrayMsgErreur[] = $errorMessage;
        }
    }

    /**
     * permet de verifier l'integrite du DCE
     * return false en cas d'erreur.
     */
    public function verifiyDce()
    {
        $sys_answer = 0;
        $infileDce = '';
        if ($this->blocDce->uploadBar->hasFile()) {
            $infileDce = $this->blocDce->uploadBar->getLocalName();
        }
        if ($this->uploadBarDce->hasFile()) {
            $infileDce = $this->uploadBarDce->getLocalName();
        }
        if (empty(Atexo_Config::getParameter('ACTIVE_ELABORATION_V2')) && $this->dceComposant->uploadBar->hasFile()) {
            $infileDce = $this->dceComposant->uploadBar->getLocalName();
        }

        // tester l'integrite du fichier zip

        // -T revient au zip precedent en cas d'echec
        $cmdIsItZipFile = 'nice zip -T '.$infileDce;
        exec($cmdIsItZipFile, $output, $sys_answer_zip_cmd);

        // Check file size : Limit 1Go ('post_max_size')
        $limitSizeFile = Atexo_Document::return_bytes(ini_get('post_max_size'));
        $sizeFile = filesize($infileDce);

        $errorDce = [];
        if (($sizeFile > $limitSizeFile) || (0 != $sys_answer_zip_cmd)) {
            if (($sizeFile > $limitSizeFile)) {
                $errorDce[] = 'MSG_ERREUR_SIZE_DCE_INVALIDE';
            }
            if ((0 != $sys_answer_zip_cmd)) {
                $errorDce[] = 'MSG_ERREUR_ZIP_DCE_INVALIDE';
            }

            return $errorDce;
        }

        return true;
    }

    /*
     * Permet de remplir les liens de telechargement des documents
     */
    public function remplirLienTelechargementDocsJoints($consultationId)
    {
        if ($this->_dce instanceof CommonDCE && '0' != $this->_dce->getDce()) {
            if (Atexo_Module::isEnabled('InterfaceModuleRsem') && ('1' == Atexo_Config::getParameter('CONFIG_REDAC')) && empty(Atexo_Config::getParameter('ACTIVE_ELABORATION_V2'))) {
                $this->dceComposant->PanelDownloadDce->style = "display:''";
            }
            $this->setDownlodDceNavigateUrl($consultationId);
        }
        if (Atexo_Module::isEnabled('ReglementConsultation') && $this->_rg instanceof CommonRG && '0' != $this->_rg->getRg()) {
            $this->panelDownloadRc->setVisible(true);
            $this->setDownlodRcNavigateUrl($consultationId);
        }
        if (empty(Atexo_Config::getParameter('ACTIVE_ELABORATION_V2')) && Atexo_Module::isEnabled('ConsultationPjAutresPiecesTelechargeables') && $this->_complement instanceof CommonComplement) {
            $this->panelDownloadAutrePiece->style = "display:''"; //->setVisible(true);
            $this->setDownlodAutrePieceNavigateUrl($consultationId);
        }
    }

    /**
     * Permet de gerer l'affichage du message informatif qui rappelle à l'agent qu'il est en proc?dure r?pondant au service MPS.
     *
     * @param $consultation: Objet consultation
     *
     * @return void
     *
     * @author YEL <youssef.elalaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    private function gestionAffichageReglementConsultationInfoMsg($consultation)
    {
        $this->activeInfoMsg->displayStyle = 'None';
        $this->blocDce->activeInfoMsg->displayStyle = 'None';
        if (empty(Atexo_Config::getParameter('ACTIVE_ELABORATION_V2'))) {
            $this->dceComposant->activeInfoMsg->displayStyle = 'None';
        }

        if ($consultation instanceof CommonConsultation
            && Atexo_Module::isEnabled('MarchePublicSimplifie', Atexo_CurrentUser::getCurrentOrganism())
            && 1 == $consultation->getMarchePublicSimplifie()
        ) {
            $this->activeInfoMsg->displayStyle = 'Dynamic';
            $this->blocDce->activeInfoMsg->displayStyle = 'Dynamic';
            if (empty(Atexo_Config::getParameter('ACTIVE_ELABORATION_V2'))) {
                $this->dceComposant->activeInfoMsg->displayStyle = 'Dynamic';
            }
        }
    }

    public function savePartialDownload(&$consultation, $modification = true)
    {
        if ($consultation instanceof CommonConsultation) {
            if ($this->dceComposant->telechargementPartiel->Checked) {
                $consultation->setPartialDceDownload('1');
            } else {
                $consultation->setPartialDceDownload('0');
            }
        }
    }
}