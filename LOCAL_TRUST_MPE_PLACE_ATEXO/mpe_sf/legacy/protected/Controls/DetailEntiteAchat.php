<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Certificat;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Module;
use AtexoCrypto\Dto\Consultation;
use Exception;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * Classe DetailOrganisme.
 *
 * @author Yassine SBAI <yassine.sbai@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class DetailEntiteAchat extends MpeTPage
{
    protected $_calledFrom = '';
    public $_org = '';
    public $_IdEntite = false;
    public $entiteValide = true;
    public $_codeAcheteur = false;
    public $_idService;
    public $_libelleService;

    public function onLoad($param)
    {
        if (Atexo_Module::isEnabled('SocleExternePpp')) {
            $this->btnRattachementEntiteAchat->setVisible(false);
            $this->buttonFusionService->setVisible(false);
            $this->buttonSuppression->setVisible(false);
        }

        $this->_codeAcheteur = Atexo_Module::isEnabled('AfficherCodeService');
        if (Atexo_Module::isEnabled('SiretDetailEntiteAchat')) {
            $this->panelSiret->setDisplay('Dynamic');
        } else {
            $this->panelSiret->setDisplay('None');
        }

        if ($this->_codeAcheteur) {
            $this->panelCodeAcheteur->setDisplay('Dynamic');
        } else {
            $this->panelCodeAcheteur->setDisplay('None');
        }

        if (0 == strcmp($this->getCalledFrom(), 'entreprise')) {
            $this->panelChorus->setVisible(false);
            $org = $this->_org;
            $this->buttonCreationModification->setVisible(false);
            $this->buttonSuppression->setVisible(false);
            $this->buttonFusionService->setVisible(false);
            $this->blocIdentificationArchive->setVisible(false);
            $this->idHorloge->setVisible(false);
        } else {
            if (Atexo_CurrentUser::hasHabilitation('HyperAdmin') && Atexo_Module::isEnabled('GestionOrganismeParAgent') && isset($_GET['org']) && $_GET['org']) {
                $org = $this->_org;
            } else {
                $org = Atexo_CurrentUser::getCurrentOrganism();
            }
            if (Atexo_Module::isEnabled('GererMonService')) {
                if ('socle' != Atexo_CurrentUser::readFromSession('ServiceMetier')) {
                    $this->buttonCreationModification->setVisible(false);
                    $this->buttonSuppression->setVisible(false);
                }
            }
            if (!Atexo_CurrentUser::hasHabilitation('DeplacerService')) {
                $this->buttonFusionService->setVisible(false);
            }
        }
        if ($org) {
            if (null !== Atexo_CurrentUser::getCurrentServiceId()) {
                $arrayServicesVaide = Atexo_EntityPurchase::getSubServices(Atexo_CurrentUser::getCurrentServiceId(), $org, true);
                if (!in_array($this->getIdEntity(), $arrayServicesVaide)) {
                    $this->entiteValide = false;
                }
            }
            self::detailEA($this->getIdEntity(), $org);
        }
    }

    public function setCalledFrom($value)
    {
        $this->_calledFrom = $value;
    }

    public function getCalledFrom()
    {
        return $this->_calledFrom;
    }

    /**
     * displayEA remplie la page de detail d'une entite d'achat.
     **/
    public function detailEA($IdEntite, $AcrconnexionOrg)
    {
        try {
            $EntitePurchaseBean = Atexo_EntityPurchase::retrieveEntityById($IdEntite, $AcrconnexionOrg);
            if ($EntitePurchaseBean) {
                $this->_idService = $EntitePurchaseBean->getId();
                $this->_libelleService = $EntitePurchaseBean->getLibelle();
                $this->sigle->Text = $EntitePurchaseBean->getSigle();
                $this->libelle->Text = $EntitePurchaseBean->getLibelle();
                $this->entiterattachement->Text = Atexo_EntityPurchase::getPathEntityById(Atexo_EntityPurchase::getParent($IdEntite, $AcrconnexionOrg), $AcrconnexionOrg);
                $this->formejuridique->Text = $EntitePurchaseBean->getFormeJuridique();
                $this->adresseprincipal->Text = $EntitePurchaseBean->getAdresse();
                $this->adressesuite->Text = $EntitePurchaseBean->getAdresseSuite();
                $this->codepostal->Text = $EntitePurchaseBean->getCp();
                $this->tel->Text = $EntitePurchaseBean->getTelephone();
                $this->ville->Text = $EntitePurchaseBean->getVille();
                $this->pays->Text = $EntitePurchaseBean->getPays();
                $this->fax->Text = $EntitePurchaseBean->getFax();
                $this->mail->Text = $EntitePurchaseBean->getMail();
                $this->siret->Text = $EntitePurchaseBean->getSiren().' '.$EntitePurchaseBean->getComplement();

                if ((new Atexo_Module())->isSaisieManuelleActive($AcrconnexionOrg)) {
                    $this->idExterneSSO->Text = $EntitePurchaseBean->getIdExterne();
                }
                $this->idEntiteL->Text = $EntitePurchaseBean->getIdEntite();
                if ($this->_codeAcheteur) {
                    $this->codeAcheteur->Text = $EntitePurchaseBean->getSiren();
                }
                if (Atexo_Module::isEnabled('GestionMandataire')) {
                    if ('0' == $EntitePurchaseBean->getAffichageService()) {
                        $this->afficherService->Text = Prado::localize('DEFINE_NON');
                    } else {
                        $this->afficherService->Text = Prado::localize('DEFINE_OUI');
                    }
                }
                $this->libelle_ar->Text = $EntitePurchaseBean->getLibelleAr();
                $this->adresseprincipal_ar->Text = $EntitePurchaseBean->getAdresseAr();
                $this->adressesuite_ar->Text = $EntitePurchaseBean->getAdresseSuiteAr();
                $this->ville_ar->Text = $EntitePurchaseBean->getVilleAr();

                $this->nomServiceArchive->Text = $EntitePurchaseBean->getNomServiceArchiveur();
                $this->idServiceArchive->Text = $EntitePurchaseBean->getIdentifiantServiceArchiveur();
                if (Atexo_Module::isEnabled('FuseauHoraire', Atexo_CurrentUser::getCurrentOrganism())) {
                    $this->utiliserFuseauHoraire->Text = ('1' == $EntitePurchaseBean->getActivationFuseauHoraire()) ? Prado::localize('DEFINE_OUI') : Prado::localize('DEFINE_NON');
                    $decalageHoraire = '';
                    if ($EntitePurchaseBean->getDecalageHoraire() && 'Selec' != $EntitePurchaseBean->getDecalageHoraire()) {
                        $decalageHoraire = $EntitePurchaseBean->getDecalageHoraire();
                        if ('-' != $decalageHoraire[0]) {
                            $decalageHoraire = '+'.$decalageHoraire;
                        }
                    } else {
                        $decalageHoraire = '-';
                    }
                    $this->decalageHoraire->Text = $decalageHoraire;
                    $this->lieuResidence->Text = $EntitePurchaseBean->getLieuResidence() ?:'-';
				}
                if (Atexo_CurrentUser:: isConnected()  && Atexo_CurrentUser::isAgent()) {
                    $this->panelChorus->Visible = Atexo_Module::isEnabled('InterfaceChorusPmi', $EntitePurchaseBean->getOrganisme());
                }

				$this->utiliserChorus->Text = ($EntitePurchaseBean->getAccesChorus() === '1') ? Prado::localize('DEFINE_OUI') : Prado::localize('DEFINE_NON');
			}
        }catch(Exception $e){
			Prado::log("Impossible de recuperer cette entite d'achat'-".$e->__toString().'-', TLogger::FATAL, 'Fatal');
        }
    }

    public function RedirectUrlCreation()
    {
        $this->response->redirect('?page=Agent.CreationEntityPurchase&idParent='.$this->getIdEntity().'&org='.$this->_org);
    }

    public function RedirectUrlUpdate()
    {
        $this->response->redirect('?page=Agent.CreationEntityPurchase&idEntite='.$this->getIdEntity().'&org='.$this->_org);
    }

    public function onClickDelete()
    {
        if ((new Atexo_EntityPurchase())->deleteEntity($this->getIdEntity(), $this->_org)) {
            $this->response->redirect('index.php?page=Agent.GestionEntitesAchat&org='.$this->_org);
        } else {
            $this->Page->panelMessageErreur->setVisible(true);
            $this->Page->panelMessageErreur->setMessage(Prado::localize('TEXT_ERREUR_SUPPRESSION_ENTITE').'<br />'.self::detailErreurSuppression());
            //echo "Cette suppression n'est pas possible car il est necessaire qu'aucun RPA / Agent / Consultation / Entite d'achat ne soit rattache a cette Entite d'achat";
        }
    }

    public function detailErreurSuppression()
    {
        $consultations = (new Atexo_Consultation())->retrieveConsultationsByService($this->getIdEntity(), $this->_org);
        $etats = (new Atexo_Consultation())->getEtatConsultation(Atexo_Config::getParameter('COMMON_DB'));
        $res = '';
        if (is_array($consultations)) {
            foreach ($consultations as $c) {
                $statutConsultation = Atexo_Consultation::getStatus($c, '') - 1;
                $etatConsultation = in_array($statutConsultation, array_keys($etats))
                    ? Prado::localize($etats[$statutConsultation])
                    : Prado::localize('STATUS_LABEL_ELABORATION')
                ;
                $res1 = str_replace('[__REF_CONSULTATION__]', $c->getReferenceUtilisateur(), Prado::localize('TEXT_CONSULTATION_ASSOCIE_SERVICE'));
                $res .= ' - '.str_replace('[__ETAT_CONSULTATION__]', $etatConsultation, $res1).'<br />';
            }
        }
        $agents = (new Atexo_Agent())->retrieveAgentsParService($this->_idService, $this->_org);
        if (is_array($agents)) {
            foreach ($agents as $unAgent) {
                $res .= ' - '.str_replace('[__LOGIN_AGENT__]', $unAgent->getLogin(), Prado::localize('TEXT_AGENT_ASSOCIE_SERVICE')).'<br />';
            }
        }
        $certificats = (new Atexo_Certificat())->retrievePermanentCertificatByService($this->_idService, $this->_org);
        if (is_array($certificats)) {
            foreach ($certificats as $unCert) {
                $res .= ' - '.str_replace('[__BICLE__]', $unCert->getNom(), Prado::localize('TEXT_BICLE_ASSOCIE_SERVICE')).'<br />';
            }
        }

        return $res;
    }

    public function RedirectUrlGestionOAEtGA()
    {
        $url = "javascript:popUp('index.php?page=Agent.PopupGestionOAGA&organisme=".base64_encode($this->_org).'&idService='.base64_encode($this->getIdEntity())."','yes');";
        $this->scriptJS->text = '<script>'.$url.'</script>';
    }

    public function redirectToFusionService()
    {
        $this->response->redirect('index.php?page=Agent.FusionService&organisme='.base64_encode($this->_org).'&idService='.base64_encode($this->getIdEntity()));
    }

    /**
     * @return mixed
     */
    public function getIdEntity(): mixed
    {
        return $_GET['idEntite'] ?? $this->_IdEntite;
    }
}
