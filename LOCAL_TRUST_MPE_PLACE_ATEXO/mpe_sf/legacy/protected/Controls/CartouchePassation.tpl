	<!--Debut Bloc Information Passation-->
		<div class="form-bloc" id="recap-consultation">
			<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
			<div class="content">
				<a class="title-toggle" onclick="togglePanel(this,'infosPassation');" href="javascript:void(0);">&nbsp;</a>
				<div class="recap-bloc">
					<div class="line">
						<div class="intitule-auto bloc-700"><strong><com:TTranslate>TEXT_INFORMATIONS_PASSATION</com:TTranslate></strong></div>
					</div>
					<div class="line">
						<div class="intitule-180"><strong><com:TTranslate>REFERENCE_CONSULTATION</com:TTranslate> :</strong></div>
						<div class="content-bloc bloc-550"><com:TLabel ID="referenceUtilisateur"/> </div>
					</div>
					<div id="infosPassation" style="display:none;" class="panel-toggle">
						<div class="line">
							<div class="intitule-180 bold"> <com:TTranslate>DEFINE_OBJET_MARCHE</com:TTranslate> : </div>
							<div class="content-bloc bloc-550"><com:TLabel ID="objetMarche"/></div>
						</div>
						<div class="line">
							<div class="intitule-180 bold"><com:TTranslate>TEXT_TITULAIRE_MARCHE</com:TTranslate></div>
							<div class="content-bloc bloc-550"><com:TLabel ID="titulaireMarche"/></div>
						</div>
						<div class="line">
							<div class="intitule-180 bold"><com:TTranslate>DATE_NOTIFICATION </com:TTranslate>: </div><com:TLabel ID="dateNotification"/>
						</div>
						<div class="breaker"></div>
					</div>
				</div>
				<div class="spacer-small"></div>
				<div class="breaker"></div>
			</div>
			<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
		</div>
		<!--Fin Bloc Information Passation-->