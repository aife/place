<div class="toggle-panel form-toggle donnees-redac" id="donneeCritereAttribution">
	<div class="content">
		<div class="cover"></div>
		<com:THyperLink ID="linkTogglePanel" NavigateUrl="javascript:void(0);" Attributes.onClick="togglePanel(this,'<%=$this->panelCriteresAttribution->getClientId()%>');" CssClass="title-toggle" >
				  <com:TTranslate>DEFINE_CRITERES_ATTRIBUTION</com:TTranslate>
		</com:THyperLink>
		<com:TPanel cssClass="panel no-indent panel-donnees-complementaires" style="display:none;" id="panelCriteresAttribution">
			<!--Debut Ligne Criteres-->
			<div class="line">
				<div class="intitule col-150"><label for="dureeDelai"><com:TTranslate>DEFINE_CRITERE_ATTRIBUTION</com:TTranslate></label><span class="champ-oblig">*</span> :</div>
				<com:TActivePanel ID="critereIdentiqueCons"   display="<%=$this->isCritereIdentiqueCons()?'Dynamic':'None'%>">
					<div class="content-bloc">
						<com:TTranslate>DEFINI_AU_NIVEAU_CONSULTATION</com:TTranslate>
					</div>
				</com:TActivePanel>
				<com:TActivePanel ID="critereNonIdentiqueCons"  display="<%=!$this->isCritereIdentiqueCons()?'Dynamic':'None'%>">
					<div class="content-bloc">
						<com:TActiveHiddenField id="alloti" value="<%=$this->getIsAlloti()? '1':'0'%>" />
						<com:TActiveDropDownList 
							id="criteresAttribution" 
							CssClass="moyen float-left champ-pub"
							Attributes.title="<%=Prado::localize('DEFINE_CRITERE_ATTRIBUTION')%>" 
							Attributes.onchange="displayOptionByClass(this,'<%=$this->alloti->getClientId()%>');"
							Enabled="<%=$this->isObjetConsultation()%>"
						/>
						<com:TCustomValidator 
								ControlToValidate="criteresAttribution" 
								ValidationGroup="validateInfosConsultation" 
								ClientValidationFunction="validationCriteresAttribution" 
								Display="Dynamic" 
								Enabled="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('DonneesComplementaires') && Application\Service\Atexo\Atexo_Module::isEnabled('DonneesRedac'))%>"
								ErrorMessage="<%=Prado::localize('DEFINE_CRITERE_ATTRIBUTION')%>"
								Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
								EnableClientScript="true" 
							> 						 					 
								<prop:ClientSide.OnValidationError>
									document.getElementById('divValidationSummary').style.display='';
					                document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
					                document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
					                document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
					                document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
								</prop:ClientSide.OnValidationError>
						</com:TCustomValidator>
						
						
						<div class="float-left indent-10">
							<div style="display: none;" class="layerDefinitionCriteres_1" id="layerDefinitionCriteres_1">
								<a href="javascript:void(0);" class="bouton-small definir-criteres"  onclick="J('#panel_attrib_ponderation').hide();J('#panel_attrib_ordreDecroissant').show();" title="<%=Prado::localize('DEFINE_DEFINITION_CRITERES_ATTRIBUTION').'('.Prado::localize('DEFINE_PAR_ORDRE_PRIORITE_DECROISSANTE').')'%>" ><com:TTranslate>DEFINIR</com:TTranslate></a>
							</div>
							<div style="display: none;" class="layerDefinitionCriteres_2" id="layerDefinitionCriteres_2">
								<a href="javascript:void(0);" class="bouton-small definir-criteres"  onclick="J('#panel_attrib_ordreDecroissant').hide();J('#panel_attrib_ponderation').show();" title="<%=Prado::localize('DEFINE_DEFINITION_CRITERES_ATTRIBUTION')%>" ><com:TTranslate>DEFINIR</com:TTranslate></a>
							</div>	
							<div style="display: none;" id="layerDefinitionVide"></div>
							<div style="display: none;" id="layerDefinitionVide_2"></div>
							<div style="display: none;" id="layerDefinitionVide1"></div>
						</div>
						<com:TActivePanel ID="panelCopyCritere" cssclass="line layerDefinitionCriteres_1 layerDefinitionCriteres_2"  style="display:none;">
							<div class="intitule-auto" id="panel_copy_attrib">
								<label for="<%=$this->copy_attrib->getClientId()%>">
									<com:TActiveCheckBox 
										id="copy_attrib" 
										CssClass="check" 
										Attributes.onclick="toggleDisplay(this,'definir-criteres');"
									/>
									<com:TTranslate>CRITERES_IDENTIQUES_POUR_LOTS</com:TTranslate>
								</label>
							</div>
						</com:TActivePanel>
					</div>
				</com:TActivePanel>
			</div>
			<!--Fin Ligne Criteres-->
		</com:TPanel>
		<div class="breaker"></div>
	</div>
</div>
<script>
J(document).ready(function(){
		J( ".definir-criteres" ).click(function() {
			J(".modal-criteres").dialog({
				modal: true,
				resizable: false,
				closeText:'Fermer',
				title:'<h2>'+this.title+'</h2>',
				dialogClass: 'modal-form popup-small2',
				open: function(){
					jQuery('.ui-widget-overlay').bind('click',function(){
						J(".modal-criteres").dialog('close');
					})
				}
			});	
		});
	});
</script>
<com:TActiveLabel id="scriptJs" />

<!--Debut Modal Criteres-->
<div class="modal-criteres" style="display:none;">
	<!--Debut Formulaire-->
	<com:AtexoValidationSummary  id="validSummaryPonderation" ValidationGroup="validatePonderation"  />
	<com:AtexoValidationSummary  id="validSummarySousPonderation" ValidationGroup="validateSousPonderation"  />
	<com:AtexoValidationSummary  id="validSummaryPonderationSave" ValidationGroup="validatePonderationSave"  />
	<div class="form-bloc">
		<div class="top"><span class="left"></span><span class="right"></span></div>
		<div class="content">
			<com:TActivePanel ID="panelCriteresPonderation">
			<div class="criteres-attrib" id="panel_attrib_ponderation">
				<table id="critereAttribPonderationTable_ponderation" summary="Liste des critères">
					<caption><com:TTranslate>DEFINE_LISTE_CRITERES</com:TTranslate></caption>
					<thead>
						<tr>
							<th class="critere" colspan="2"><com:TTranslate>TEXT_CRITERES</com:TTranslate></th>
							<th class="actions"><com:TTranslate>DEFINE_SOUS_CRITERE</com:TTranslate></th>
							<th class="actions"><com:TTranslate>DEFINE_SUPPRIMER</com:TTranslate></th>
						</tr>
						<tr>
							<th class="separator" colspan="4"></th>
						</tr>
					</thead>
					<tbody id="critereAttribPonderationBody_ponderation">
						<com:TRepeater ID="repeaterCritereAttribPonderation">
							<prop:ItemTemplate>
								<tr>
									<td class="ponderation">
										<com:TLabel ID="criterePonderation" Text="<%#$this->Data['critereVal']->getPonderation()%>"/>
										<com:THiddenField ID="criterePonderationHidden" Value="<%#$this->Data['critereVal']->getPonderation()%>"/>
										<span class="intitule bloc-20">%</span>
									</td>
									<td class="critere">
										<com:TLabel ID="enonceCriterePonderation" Text="<%#$this->Data['critereVal']->getEnonce()%>"/>
									</td>
									<com:THiddenField ID="critereIndexItem" Value="<%#$this->Data['critereVal']->index%>" />
									<com:THiddenField ID="IndexItem" Value="<%#$this->ItemIndex%>" />
									<com:THiddenField ID="totalPonderationSousCritere" Value="<%#$this->Data['critereVal']->getTotalPonderationSousC()%>" />
									<td class="actions">
										<com:TActiveLinkButton 
												Enabled="<%=$this->TemplateControl->enabledElement()%>"
												cssClass="btn-action btn-action-small"
												ID="linkAjouterSousCritere"
		           								Attributes.title="<%=Prado::localize('AJOUTER_SOUS_CRITERE')%>" 
		           								OnCallBack="SourceTemplateControl.definirSousCritere"
		           								ActiveControl.CallbackParameter="<%#$this->Data['critereVal']->index.'#'.$this->TemplateControl->getTypePonderation().'#'.$this->ItemIndex%>"
											>
											<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-ajout-sous-element.gif" alt="<%=Prado::localize('AJOUTER_SOUS_CRITERE')%>" />
										</com:TActiveLinkButton>
									</td>
									<td class="actions">
										<com:TActiveLinkButton ID="supprimerCriterePonderation"
											Enabled="<%=$this->TemplateControl->enabledElement()%>"
											OnCallback="SourceTemplateControl.suppressionCriteresPonderation"
											ActiveControl.CallbackParameter="<%#$this->Data['critereVal']->index%>"
											Attributes.title="<%=Prado::localize('DEFINE_SUPPRIMER_CRITERE')%>"
											><img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-supprimer.gif" /></com:TActiveLinkButton>
									</td>
								</tr>
								<com:TRepeater ID="repeaterSousCritereAttribPonderation"  DataSource="<%#$this->Data['sousCirteres']%>" >
									<prop:ItemTemplate>
										<tr  class="on">
											<td class="ponderation"><div class="indent-arrow"><%#$this->Data->getPonderation()%>%</div></td>
											<td class="critere" colspan="2">
												<div><%#$this->Data->getEnonce()%></div>
											</td>
											<td class="actions">
												<com:TActiveLinkButton ID="supprimerSousCriterePonderation"
													Enabled="<%=$this->TemplateControl->TemplateControl->enabledElement()%>"
													OnCallback="SourceTemplateControl.suppressionSousCriteres"
													ActiveControl.CallbackParameter="<%#$this->Data->indexPere.'#'.$this->ItemIndex.'#'.$this->TemplateControl->TemplateControl->getTypePonderation()%>"
													Attributes.title="<%=Prado::localize('DEFINE_SUPPRIMER')%>"
													><img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-supprimer.gif" /></com:TActiveLinkButton>
											</td>
										</tr>
									</prop:ItemTemplate>
								</com:TRepeater>
							</prop:ItemTemplate>
						</com:TRepeater>
					</tbody>
				</table>
				<a class="ajout-el" id="lien_ajoutCritere_<%=$this->getTypePonderation()%>" style="display:none;" onclick="J('#panel_ajoutCritereAttribution_<%=$this->getTypePonderation()%>').show();J('#panel_ajoutSousCritereAttribution_<%=$this->getTypePonderation()%>').hide();J('#lien_ajoutCritere_<%=$this->getTypePonderation()%>').hide();"><com:TTranslate>DEFINE_AJOUTER_CRITERE</com:TTranslate></a>
				<div class="spacer"></div>
				<!--Debut tableau Ajout Sous-Critère-->
				<div id="panel_ajoutSousCritereAttribution_<%=$this->getTypePonderation()%>" style="display:none;">
					<table summary="Ajouter un sous-critère d'attribution">
						<thead>
							<tr>
								<th class="critere" colspan="2"><com:TTranslate>AJOUTER_SOUS_CRITERE</com:TTranslate></th>
								<th class="actions"></th>
								<th class="actions"><com:TTranslate>AJOUTER</com:TTranslate></th>
							</tr>
							<tr>
								<th class="separator" colspan="4"></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="ponderation">
									<com:TActiveTextBox 
										Enabled="<%=$this->enabledElement()%>"
										ID="ponderationSousCritere"  
										Attributes.title="<%=Prado::localize('TEXT_PONDERATION')%>"
										cssClass="champ-ponderation" 
									/>
									<span class="intitule bloc-20">%</span>
								</td>
								<com:TActiveHiddenfield ID="criterePereIndexElementPonderation"  />
								<com:TActiveHiddenfield ID="IndexItemPere"  />
								<td class="critere">
									<com:TActiveTextBox 
										Enabled="<%=$this->enabledElement()%>"
										id="enonceSousCriterePonderation" 
										TextMode="MultiLine"
										CssClass="champ-critere-moyen"  
										Attributes.rows="0"
										Attributes.cols="0"
										Attributes.OnChange="replaceCaracSpecial(this);"
										Attributes.maxlength="800"
										Attributes.title="<%=Prado::localize('DEFINE_ENNONCE_CRITERE')%>"
									/>
								</td>
								<td class="actions"></td>
								<td class="actions">
										<com:TActiveLinkButton ID="ajouterSousCriterePonderation"
											Enabled="<%=$this->enabledElement()%>"
										OnCallback="ajoutSousCriteres" 
										ActiveControl.CallbackParameter="<%=$this->getTypePonderation()%>"
										Attributes.title="<%=Prado::localize('AJOUTER_SOUS_CRITERE')%>"
										ValidationGroup="validateSousPonderation" 
										><img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-ajout-element.gif" /></com:TActiveLinkButton>
								</td>
							</tr>
						</tbody>
					</table>
					<div class="spacer-small"></div>
				</div>
				<!--Fin tableau Ajout Sous-Critère-->
				<div id="panel_ajoutCritereAttribution_<%=$this->getTypePonderation()%>" style="display:block;">
					<table summary="<%=Prado::localize('DEFINE_AJOUTER_CRITERE_ATTRIBUTION')%>">
						<thead>
							<tr>
								<th class="critere" colspan="2"><com:TTranslate>DEFINE_AJOUTER_CRITERE</com:TTranslate></th>
								<th class="actions"><com:TTranslate>AJOUTER</com:TTranslate></th>
							</tr>
							<tr>
								<th class="separator" colspan="3"></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="ponderation">
									<com:TActiveTextBox 
										Enabled="<%=$this->enabledElement()%>"
										ID="ponderationCritere"  
										Attributes.title="<%=Prado::localize('TEXT_PONDERATION')%>"
										cssClass="champ-ponderation" 
									/>
									<span class="intitule bloc-20">%</span>
								</td>
								<td class="critere">
									<com:TActiveTextBox 
										Enabled="<%=$this->enabledElement()%>"
										id="enonceCritere_ponderation" 
										TextMode="MultiLine"
										CssClass="champ-critere-moyen"  
										Attributes.rows="0"
										Attributes.cols="0"
										Attributes.OnChange="replaceCaracSpecial(this);"
										Attributes.maxlength="800"
										Attributes.title="<%=Prado::localize('DEFINE_ENNONCE_CRITERE')%>"
									/>
									
								</td>
								<td class="actions">
									<com:TActiveLinkButton ID="ajouterCriterePonderation"
										Enabled="<%=$this->enabledElement()%>"
										OnCallback="ajoutCriteresPonderation" 
										Attributes.title="<%=Prado::localize('DEFINE_AJOUTER_CRITERE')%>"
										ValidationGroup="validatePonderation" 
										><img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-ajout-element.gif" /></com:TActiveLinkButton>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			</com:TActivePanel>
			<com:TActivePanel ID="panelCriteresOrdreDecroissant">
			<div class="criteres-attrib" id="panel_attrib_ordreDecroissant">
				<table id="critereAttribPonderationTable_ordreDecroissant" summary="Liste des critères">
					<caption><com:TTranslate>DEFINE_LISTE_CRITERES</com:TTranslate></caption>
					<thead>
						<tr>
							<th class="critere"><com:TTranslate>TEXT_CRITERES</com:TTranslate></th>
							<th class="actions"><com:TTranslate>DEFINE_SOUS_CRITERE</com:TTranslate></th>
							<th class="actions"><com:TTranslate>DEFINE_SUPPRIMER</com:TTranslate></th>
						</tr>
						<tr>
							<th class="separator" colspan="3"></th>
						</tr>
					</thead>
					<tbody id="critereAttribPonderationBody_ordreDecroissant">
						<com:TRepeater ID="repeaterCritereAttribOrdreDecroissant">
							<prop:ItemTemplate>
								<tr>
									<td class="critere">
										<com:TLabel ID="enonceCritereDecroissant" Text="<%#$this->Data['critereVal']->getEnonce()%>"/>
									</td>
									<com:THiddenField ID="critereIndexItemOrdreDecroissant" Value="<%#$this->Data['critereVal']->index%>" />
									<td class="actions">
									
										<com:TActiveLinkButton 
												Enabled="<%=$this->TemplateControl->enabledElement()%>"
												cssClass="btn-action btn-action-small"
												ID="linkAjouterSousCritere"
		           								Attributes.title="Ajouter un sous-critère" 
		           								OnCallBack="SourceTemplateControl.definirSousCritere"
		           								ActiveControl.CallbackParameter="<%#$this->Data['critereVal']->index.'#'.$this->TemplateControl->getTypeOrdreDecroissant()%>"
											>
											<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-ajout-sous-element.gif" alt="Ajouter un sous-critère" />
										</com:TActiveLinkButton>
									</td>
									<td class="actions">
										<com:TActiveLinkButton ID="supprimerCritereDecroissant"
											Enabled="<%=$this->TemplateControl->enabledElement()%>"
											OnCallback="SourceTemplateControl.suppressionCriteresDecroissant"
											ActiveControl.CallbackParameter="<%#$this->Data['critereVal']->index%>"
											Attributes.title="<%=Prado::localize('DEFINE_SUPPRIMER_CRITERE')%>"
											><img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-supprimer.gif" /></com:TActiveLinkButton>
									</td>
								</tr>
								<com:TRepeater ID="repeaterSousCritereAttribOrdreDecroissant"  DataSource="<%#$this->Data['sousCirteres']%>" >
									<prop:ItemTemplate>
										<tr id="sous_critereAttribPonderationBody_ordreDecroissant_ordre1"  class="on">
											<td class="critere" colspan="2">
												<div class="indent-arrow"><%#$this->Data->getEnonce()%></div>
											</td>
											<td class="actions">
												<com:TActiveLinkButton ID="supprimerSousCritereDecroissant"
													Enabled="<%=$this->TemplateControl->TemplateControl->enabledElement()%>"
													OnCallback="SourceTemplateControl.suppressionSousCriteres"
													ActiveControl.CallbackParameter="<%#$this->Data->indexPere.'#'.$this->ItemIndex.'#'.$this->TemplateControl->TemplateControl->getTypeOrdreDecroissant()%>"
													Attributes.title="<%=Prado::localize('DEFINE_SUPPRIMER')%>"
													><img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-supprimer.gif" /></com:TActiveLinkButton>
											</td>
										</tr>
									</prop:ItemTemplate>
								</com:TRepeater>
							</prop:ItemTemplate>
						</com:TRepeater>
					</tbody>
				</table>
				<a class="ajout-el" id="lien_ajoutCritere_<%=$this->getTypeOrdreDecroissant()%>" style="display:none;" onclick="J('#panel_ajoutCritereAttribution_<%=$this->getTypeOrdreDecroissant()%>').show();J('#panel_ajoutSousCritereAttribution_<%=$this->getTypeOrdreDecroissant()%>').hide();J('#lien_ajoutCritere_<%=$this->getTypeOrdreDecroissant()%>').hide();"><com:TTranslate>DEFINE_AJOUTER_CRITERE</com:TTranslate></a>
				<div class="spacer"></div>
				
				<!--Debut tableau Ajout Sous-Critère-->
				<div id="panel_ajoutSousCritereAttribution_<%=$this->getTypeOrdreDecroissant()%>" style="display:none;">
					<table summary="Ajouter un sous-critère d'attribution">
						<thead>
							<tr>
								<th class="critere" colspan="2"><com:TTranslate>AJOUTER_SOUS_CRITERE</com:TTranslate></th>
								<th class="actions"><com:TTranslate>AJOUTER</com:TTranslate></th>
							</tr>
							<tr>
								<th class="separator" colspan="3"></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="critere">
									<com:TActiveTextBox 
										Enabled="<%=$this->enabledElement()%>"
										id="enonceSousCritereOrdreDecroissant" 
										TextMode="MultiLine"
										CssClass="champ-critere-moyen"  
										Attributes.rows="0"
										Attributes.cols="0"
										Attributes.OnChange="replaceCaracSpecial(this);"
										Attributes.maxlength="800"
										Attributes.title="<%=Prado::localize('DEFINE_ENNONCE_CRITERE')%>"
									/>
								</td>
								<com:TActiveHiddenfield ID="criterePereIndexElementOrdreDecroissant"  />
								<td class="actions"></td>
								<td class="actions">
									<com:TActiveLinkButton ID="ajouterSousCritereDecroissant"
										Enabled="<%=$this->enabledElement()%>"
										OnCallback="ajoutSousCriteres" 
										ActiveControl.CallbackParameter="<%=$this->getTypeOrdreDecroissant()%>"
										Attributes.title="<%=Prado::localize('AJOUTER_SOUS_CRITERE')%>"
										><img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-ajout-element.gif" /></com:TActiveLinkButton>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<!--Fin tableau Ajout Sous-Critère-->
				<div id="panel_ajoutCritereAttribution_<%=$this->getTypeOrdreDecroissant()%>" style="display:block;">
					<table summary="<%=Prado::localize('DEFINE_AJOUTER_CRITERE_ATTRIBUTION')%>" >
						<thead>
							<tr>
								<th class="critere"><com:TTranslate>DEFINE_AJOUTER_CRITERE</com:TTranslate></th>
								<th class="actions"><com:TTranslate>AJOUTER</com:TTranslate></th>
							</tr>
							<tr>
								<th class="separator" colspan="2"></th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td class="critere">
									<com:TActiveTextBox 
										Enabled="<%=$this->enabledElement()%>"
										id="enonceCritere_ordreDecroissant" 
										TextMode="MultiLine"
										CssClass="champ-critere-moyen"  
										Attributes.rows="0"
										Attributes.cols="0"
										Attributes.OnChange="replaceCaracSpecial(this);"
										Attributes.maxlength="800"
										Attributes.title="<%=Prado::localize('DEFINE_ENNONCE_CRITERE')%>"
									/>
								</td>
								<td class="actions">
									<com:TActiveLinkButton ID="ajouterCritereDecroissant"
										Enabled="<%=$this->enabledElement()%>"
										OnCallback="ajoutCriteresDecroissant" 
										Attributes.title="<%=Prado::localize('DEFINE_AJOUTER_CRITERE')%>"
										><img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-ajout-element.gif" /></com:TActiveLinkButton>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			</com:TActivePanel>			
		</div>
		<div class="bottom"><span class="left"></span><span class="right"></span></div>
	</div>
	<!--Fin Formulaire-->
	<!--Debut line boutons-->
	<div class="boutons-line">
		<com:TActiveButton 
			ID="EnregistrerCrit" 
			cssclass="bouton-moyen float-right" 
			Attributes.title="<%=Prado::localize('DEFINE_ENREGISTRER')%>"
			Attributes.value="<%=Prado::localize('DEFINE_ENREGISTRER')%>"
			ValidationGroup="validatePonderationSave" 
			OnCallback="enregistrerCritere"
		/>
		<com:TActiveButton 
			ID="annulerCrit" 
			cssclass="bouton-moyen float-left" 
			Attributes.title="<%=Prado::localize('DEFINE_ANNULER')%>"
			Attributes.value="<%=Prado::localize('DEFINE_ANNULER')%>"
			OnCallback="annulerCritere"
		/>
	</div>
	<!--Fin line boutons-->
	
</div>
<!--Fin Modal Criteres-->
<com:TActiveHiddenField id="totalPonderation" />
<com:TActiveLabel ID="scriptLabel" />
<com:TLabel id="script" />
<com:TCustomValidator 
		ControlToValidate="ponderationCritere" 
		ValidationGroup="validatePonderation" 
		ClientValidationFunction="validationPonderation" 
		Display="Dynamic" 
		Enabled="true"
		Text=" " 
		ErrorMessage="<%=Prado::localize('DEFINE_MSG_ERREUR_PONDERATION')%>"
		EnableClientScript="true" 
> 						 					 
		<prop:ClientSide.OnValidationError>
			document.getElementById('validSummaryPonderation').style.display='';
		</prop:ClientSide.OnValidationError>
</com:TCustomValidator>
<com:TCustomValidator 
		ControlToValidate="ponderationSousCritere" 
		ValidationGroup="validateSousPonderation" 
		ClientValidationFunction="validationValeurPonderationSousCritee" 
		Display="Dynamic" 
		Enabled="true"
		Text=" " 
		ErrorMessage="<%=Prado::localize('VALEUR_PONDERATION_INCORRECTE')%>"
		EnableClientScript="true" 
> 						 					 
		<prop:ClientSide.OnValidationError>
			document.getElementById('validSummarySousPonderation').style.display='';
		</prop:ClientSide.OnValidationError>
</com:TCustomValidator>
<com:TCustomValidator 
		ControlToValidate="ponderationSousCritere" 
		ValidationGroup="validateSousPonderation" 
		ClientValidationFunction="validationPonderationSousCritee" 
		Display="Dynamic" 
		Enabled="true"
		Text=" " 
		ErrorMessage="<%=Prado::localize('SOMME_PONDERATION_INCORRECTE')%>"
		EnableClientScript="true" 
> 						 					 
		<prop:ClientSide.OnValidationError>
			document.getElementById('validSummarySousPonderation').style.display='';
		</prop:ClientSide.OnValidationError>
</com:TCustomValidator>
<com:TCustomValidator 
		ControlToValidate="ponderationCritere" 
		ValidationGroup="validatePonderationSave" 
		ClientValidationFunction="validationPonderationSave" 
		Display="Dynamic" 
		Enabled="true"
		Text=" " 
		ErrorMessage="<%=Prado::localize('DEFINE_MSG_ERREUR_PONDERATION')%>"
		EnableClientScript="true" 
> 						 					 
		<prop:ClientSide.OnValidationError>
			document.getElementById('validSummaryPonderationSave').style.display='';
		</prop:ClientSide.OnValidationError>
</com:TCustomValidator>
<com:TCustomValidator 
		ControlToValidate="ponderationSousCritere" 
		ValidationGroup="validatePonderationSave" 
		ClientValidationFunction="validationPonderationSousCriteeSave" 
		Display="Dynamic" 
		Enabled="true"
		Text=" " 
		ErrorMessage="<%=Prado::localize('SOMME_PONDERATION_INCORRECTE')%>"
		EnableClientScript="true" 
> 						 					 
		<prop:ClientSide.OnValidationError>
			document.getElementById('validSummaryPonderationSave').style.display='';
		</prop:ClientSide.OnValidationError>
</com:TCustomValidator>
