<?php

namespace Application\Controls;

use Application\Propel\Mpe\CommonTCandidatureMps;
use Application\Propel\Mpe\Entreprise;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_CandidatureMPS;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_EntrepriseVo;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Leaders;
use Prado\Prado;

/**
 * Le bloc declaration de l'entreprise.
 *
 * @author LEZ <loubna.ezziani@atexo.com>
 *
 * @version 1.0
 *
 * @since 4.7.0
 *
 * @copyright Atexo 2014
 */
class AtexoBlocDeclarationEntreprise extends MpeTTemplateControl
{
    protected ?int $_idEntreprise = null;
    public bool $_modeAffichage = false;

    /**
     * Set the value of [_modeAffichage] .
     *
     * @param bool $modeAffichage new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function setModeAffichage($modeAffichage)
    {
        $this->_modeAffichage = $modeAffichage;
    }

    /**
     * Get the value of [_modeAffichage] column.
     *
     * @return Booleean
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function getModeAffichage()
    {
        return $this->_modeAffichage;
    }

    /**
     * Set the value of [_idEntreprise] .
     *
     * @param int $idEntreprise new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function setIdEntreprise($idEntreprise)
    {
        $this->_idEntreprise = $idEntreprise;
    }

    /**
     * Get the value of [_idEntreprise] column.
     *
     * @return int
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function getIdEntreprise()
    {
        $idEntreprise = $this->_idEntreprise;
        if (!$idEntreprise) {
            if ($this->Page->getViewState('entreprise') instanceof Atexo_Entreprise_EntrepriseVo) {
                $idEntreprise = $this->Page->getViewState('entreprise')->getId();
            }
        }

        return $idEntreprise;
    }

    /**
     * Get the value of [_candidatureMPS] column.
     *
     * @return CommonTCandidatureMps
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function getCandidatureMPS()
    {
        return $this->Page->getViewState('candidatureMPS');
    }

    /**
     * Permet de charger les donnees.
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function onLoad($param)
    {
        if (!$this->Page->isPostBack) {
            $this->fillListRepresentantLegal();
        }
    }

    /**
     * Remplire la liste des representant legal.
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function fillListRepresentantLegal()
    {
        $data = [];
        $data['0'] = Prado::localize('TEXT_SELECTIONNER');
        $data['autre'] = Prado::localize('AUTRE_REPRESENTANT');
        $leaders = Atexo_Entreprise_Leaders::retrieveLeaders($this->getIdEntreprise());
        if (is_array($leaders)) {
            foreach ($leaders as $leader) {
                $data[$leader->getId()] = $leader->getNom().' '.$leader->getPrenom().', '.Prado::localize('EN_QUALITE_DE').' : '.$leader->getQualite();
            }
        }
        $this->representantLegal->DataSource = $data;
        $this->representantLegal->dataBind();
    }

    /**
     * Remplire la candidature MPS par les donnees de la declaration.
     *
     * @param CommonTCandidatureMps $candidatureMPS la candidature MPS
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function fillCandidatureByDataDeclaration(&$candidatureMPS)
    {
        if ($this->pouvoirEngage->checked) {
            $candidatureMPS->setPouvoirEngage(true);
        }
        if ($this->representantLegal->getSelectedValue()) {
            $candidatureMPS->setRepresentantLegal($this->representantLegal->getSelectedValue());
            if ('autre' == $this->representantLegal->getSelectedValue()) {
                $candidatureMPS->setNomRepresentantLegal($this->representantNom->Text);
                $candidatureMPS->setPrenomRepresentantLegal($this->representantPrenom->Text);
                $candidatureMPS->setQualiteRepresentantLegal($this->representantFonction->Text);
            }
        }

        if ($this->autorisationVerification->checked) {
            $candidatureMPS->setAutorisationVerification(true);
        }
    }

    /**
     * Permet de charger les informations des declarations de l'entreprise.
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function chargerInfoDeclarationsRecap()
    {
        $candidatureMPS = $this->getCandidatureMPS();
        if ($candidatureMPS instanceof Atexo_Entreprise_CandidatureMPS) {
            $entreprise = $candidatureMPS->getEntreprise();
            if ($entreprise instanceof Entreprise) {
                $this->setIdEntreprise($entreprise->getId());
            }
            $this->fillListRepresentantLegal();
            if ($candidatureMPS->getRepresentantLegal()) {
                $this->representantLegal->setSelectedValue($candidatureMPS->getRepresentantLegal());
                $this->representantLegalLabel->Text = $this->representantLegal->getSelectedItem()->getText();
                if ('autre' == $candidatureMPS->getRepresentantLegal()) {
                    $this->representantNomLabel->Text = $candidatureMPS->getNomRepresentantLegal();
                    $this->representantPrenomLabel->Text = $candidatureMPS->getPrenomRepresentantLegal();
                    $this->representantFonctionLabel->Text = $candidatureMPS->getQualiteRepresentantLegal();
                    $this->scriptJs->Text = "<script>displayOneOptionPanel(document.getElementById('ctl0_CONTENU_PAGE_".$this->getId().
                                                "_representantLegal'),'1','ctl0_CONTENU_PAGE_".$this->getId()."_panel_representantAutre');</script>";
                    if ($candidatureMPS->getNomRepresentantLegal() && $candidatureMPS->getPrenomRepresentantLegal() && $candidatureMPS->getQualiteRepresentantLegal()) {
                        Atexo_Util::setCheckImageByEtat('imgRepresentantLegal', $this, true);
                    } else {
                        Atexo_Util::setCheckImageByEtat('imgRepresentantLegal', $this, false);
                    }
                } else {
                    $this->representantNomLabel->Text = '';
                    $this->representantPrenomLabel->Text = '';
                    $this->representantFonctionLabel->Text = '';
                    Atexo_Util::setCheckImageByEtat('imgRepresentantLegal', $this, true);
                }
            } else {
                $this->representantLegalLabel->Text = '';
                Atexo_Util::setCheckImageByEtat('imgRepresentantLegal', $this, false);
            }
            Atexo_Util::setCheckImageByEtat('imgPouvoirEngage', $this, $candidatureMPS->getPouvoirEngage());
            Atexo_Util::setCheckImageByEtat(
                'imgAutorisationVerification',
                $this,
                $candidatureMPS->getAutorisationVerification()
            );
        }
    }
}
