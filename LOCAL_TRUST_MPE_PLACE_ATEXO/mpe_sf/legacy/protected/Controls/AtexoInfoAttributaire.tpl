<!--Debut Bloc Formulaire Infos sur les attributaires-->
<h3><com:TTranslate>INFORMATIONS_SUR_ATTRIBUTAIRE</com:TTranslate></h3>
<div class="line">
    <div class="intitule-150 bold"><com:TTranslate>TEXT_RAISON_SOCIAL</com:TTranslate> :</div>
    <div class="content-bloc content-bloc bloc-570"><com:TLabel id="raisonSocial" /></div>
</div>
<div class="line">
    <div class="intitule-150 bold"><com:TTranslate>TEXT_SIEGE_SOCIAL</com:TTranslate> :</div>
    <div class="content-bloc content-bloc bloc-570"><com:TLabel id="siegeSocial" /></div>
</div>
<div class="line">
    <div class="intitule-150 bold"><com:TTranslate>CODE_APE_NAF_NACE</com:TTranslate> :</div>
    <div class="content-bloc content-bloc bloc-570">
        <com:TLabel id="codeAPE"  />
    </div>
</div>
<com:TPanel cssClass="line" Visible = "<%=$this->getVisibiliteSiren()%>">
    <div class="intitule-150 bold"><com:TTranslate>TEXT_SIREN</com:TTranslate> :</div>
    <div class="content-bloc"> <com:TLabel  id="siren" />  </div>
</com:TPanel>
<com:TPanel cssClass="line" Visible = "<%= ! $this->getVisibiliteSiren()%>">
    <div class="intitule-150 bold"><com:TTranslate>TEXT_IDENTIFIANT_NATIONNAL</com:TTranslate> :</div>
    <div class="content-bloc"> <com:TLabel id="identifiantNational" />  </div>
</com:TPanel>
<div class="line">
    <div class="intitule-150 bold"><com:TTranslate>TEXT_FJ</com:TTranslate> :</div>
    <div class="content-bloc"><com:TLabel id="formeJuridique" /></div>
</div>
<div class="line">
    <div class="intitule-150 bold"><com:TTranslate>TEXT_CAPITAL_SICIAL</com:TTranslate> :</div>
    <div class="content-bloc bloc-570"><com:TLabel id="capital"  /> <com:TTranslate>TEXT_CURRENCY</com:TTranslate></div>
</div>
<div class="line">
    <div class="intitule-150 bold line-height-normal"><com:TTranslate>TEXT_ETBLISSEMENT_AYANT_REPONDU</com:TTranslate> :</div>
    <div class="content-bloc bloc-570"><com:TLabel text='<%=$this->getAdresseEtab() %>' /></div>
</div>
<!--Fin Bloc Formulaire Infos sur les attributaires-->