<!-- Début de l'entête du sommateur de messages d'erreurs -->
<div id="<%=($this->getId())? $this->getId():'divValidationSummary'%>" style="display:none" class="bloc-message form-bloc-conf msg-erreur erreur-danger">

    <div role="alert">
        <div class="h4-alerte">
            <i class="fa fa-exclamation-triangle m-r-1 hidden"></i>
            <com:TTranslate>DEFINE_ERREUR_SAISIE</com:TTranslate>
        </div>
        <div class="spacer-small"></div>
        <div>
            <com:TTranslate>DEFINE_VERIFIER_CHAMPS_OBLIGATOIRES</com:TTranslate>
            <com:TValidationSummary
                    Id="validationSummary"
                    Display="None"
                    ShowMessageBox="false"
                    ShowSummary="true"
                    AutoUpdate="true"
                    EnableClientScript="true"
                    DisplayMode="BulletList"
                    forecolor="#d9534f"

            >
                <prop:ClientSide.OnHideSummary>
                    document.getElementById('<%=$this->validationSummary->ClientId%>').innerHTML='';
                    document.getElementById('<%=($this->getId())? $this->getId():"divValidationSummary"%>').style.display='none';
                </prop:ClientSide.OnHideSummary>
            </com:TValidationSummary>
            <com:TActiveLabel id="serverError"/>
        </div>
    </div>
</div>
<!-- Fin du footer du sommateur de messages d'erreurs -->