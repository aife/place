<?php

namespace Application\Controls;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonBlobFile;
use Application\Propel\Mpe\CommonBlobFilePeer;
use Application\Propel\Mpe\CommonCG76DonneeComplementaireDomainePeer;
use Application\Propel\Mpe\CommonCG76DonneeComplementaireEntreprise;
use Application\Propel\Mpe\CommonCG76PieceJointe;
use Application\Propel\Mpe\CommonCG76PieceJointePeer;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\DomainesActivites\Atexo_DomainesActivites_Domaines;
use Application\Service\Atexo\DonneesComplementairesEntreprise\Atexo_DonneesComplementairesEntreprise_DonneesComplementairesEntreprise;
use Prado\Prado;

class InfoDonneesComplementaires extends MpeTPage
{
    private $idEntreprise;

    public function getIdEntreprise()
    {
        return $this->idEntreprise;
    }

    public function setIdEntreprise($value)
    {
        $this->idEntreprise = $value;
    }

    public function displayDonneeComplementaire()
    {
        $donneVide = false;
        $cg76DonneeComplementaire = (new Atexo_DonneesComplementairesEntreprise_DonneesComplementairesEntreprise())->retrieveDonneComplementaireByIdEntreprise($this->idEntreprise);
        if (!$cg76DonneeComplementaire instanceof CommonCG76DonneeComplementaireEntreprise) {
            $cg76DonneeComplementaire = new CommonCG76DonneeComplementaireEntreprise();
            $donneVide = true;
        }
        if ('' != $cg76DonneeComplementaire->getEmail()) {
            $this->emailDC->Text = $cg76DonneeComplementaire->getEmail();
        } else {
            $this->emailDC->Text = '-';
        }
        if ('' != $cg76DonneeComplementaire->getTypeFormation()) {
            $this->typeDC->Text = $cg76DonneeComplementaire->getTypeFormation();
        } else {
            $this->typeDC->Text = '-';
        }
        if ('' != $cg76DonneeComplementaire->getCoutMoyenJournee()) {
            $this->coutMoyenDC->Text = $cg76DonneeComplementaire->getCoutMoyenJournee();
        } else {
            $this->coutMoyenDC->Text = '-';
        }
        if ($donneVide) {
            $this->fptDC->Text = '-';
        } else {
            if ('1' == $cg76DonneeComplementaire->getCollaborationFpt()) {
                $this->fptDC->Text = Prado::localize('DEFINE_OUI');
            } else {
                $this->fptDC->Text = Prado::localize('DEFINE_NON');
            }
        }

        if ($donneVide) {
            $this->fpeDC->Text = '-';
        } else {
            if ('1' == $cg76DonneeComplementaire->getCollaborationFpe()) {
                $this->fpeDC->Text = Prado::localize('DEFINE_OUI');
            } else {
                $this->fpeDC->Text = Prado::localize('DEFINE_NON');
            }
        }

        if ($donneVide) {
            $this->centreDocDC->Text = '-';
        } else {
            if ('1' == $cg76DonneeComplementaire->getCentreDocumentation()) {
                $this->centreDocDC->Text = Prado::localize('DEFINE_OUI');
            } else {
                $this->centreDocDC->Text = Prado::localize('DEFINE_NON');
            }
        }

        if ($donneVide) {
            $this->serviceRepDC->Text = '-';
        } else {
            if ('1' == $cg76DonneeComplementaire->getServiceReprographie()) {
                $this->serviceRepDC->Text = Prado::localize('DEFINE_OUI');
            } else {
                $this->serviceRepDC->Text = Prado::localize('DEFINE_NON');
            }
        }

        if ($donneVide) {
            $this->salleInfoDC->Text = '-';
        } else {
            if ('1' == $cg76DonneeComplementaire->getSalleInfo()) {
                $this->salleInfoDC->Text = Prado::localize('DEFINE_OUI');
            } else {
                $this->salleInfoDC->Text = Prado::localize('DEFINE_NON');
            }
        }

        if ($donneVide) {
            $this->salleCoursDC->Text = '-';
        } else {
            if ('1' == $cg76DonneeComplementaire->getSalleCours()) {
                $this->salleCoursDC->Text = Prado::localize('DEFINE_OUI');
            } else {
                $this->salleCoursDC->Text = Prado::localize('DEFINE_NON');
            }
        }

        if ($donneVide) {
            $this->enregistrerCom->setEnabled(false);
        }

        if (!$this->Page->IsPostBack) {
            $this->commentaires->Text = $cg76DonneeComplementaire->getCommentaire();
        }

        if ('' != $cg76DonneeComplementaire->getAireGeoInter()) {
            $this->aireGeoInterDC->Text = $cg76DonneeComplementaire->getAireGeoInter();
        } else {
            $this->aireGeoInterDC->Text = '-';
        }
        $this->infoPieceJointe();
        $this->infoDomaines($cg76DonneeComplementaire->getRef());
    }

    public function infoPieceJointe()
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonCG76PieceJointePeer::IDENTREPRISE, $this->idEntreprise);
        $infos = CommonCG76PieceJointePeer::doSelect($c, $connexionCom);
        $files = [];
        if ($infos) {
            foreach ($infos as $info) {
                if ($info instanceof CommonCG76PieceJointe) {
                    $blobFile = null;
                    $c = new Criteria();
                    $c->add(CommonBlobFilePeer::ID, $info->getIdpj());
                    $blobFile = CommonBlobFilePeer::doSelectOne($c, $connexionCom);
                    if ($blobFile instanceof CommonBlobFile) {
                        $infos['name'] = $blobFile->getName();
                        $infos['id'] = $blobFile->getId();
                        $files[] = $infos;
                    }
                }
            }
        }
        $this->RepeaterNomsFiles->DataSource = $files;
        $this->RepeaterNomsFiles->DataBind();
    }

    public function infoDomaines($idDonnee)
    {
        $arrayDomaines = [];
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $c->add(CommonCG76DonneeComplementaireDomainePeer::IDDONNEECOMLEMENTAIRE, $idDonnee);
        $domainesChoisis = CommonCG76DonneeComplementaireDomainePeer::doSelect($c, $connexionCom);
        if (is_array($domainesChoisis)) {
            foreach ($domainesChoisis as $domaineChoisi) {
                $domaine = (new Atexo_DomainesActivites_Domaines())->retrieveDomaineById($domaineChoisi->getIddomaine());
                if ((new Atexo_DomainesActivites_Domaines())->isDomaineParent($domaine->getId())) {
                    $arrayDomaines[$domaine->getId()]['libelle'] = $domaine->getLibelle();
                    if (!isset($arrayDomaines[$domaine->getId()]['fils'])) {
                        $arrayDomaines[$domaine->getId()]['fils'] = [];
                    }
                } else {
                    $domaineParent = (new Atexo_DomainesActivites_Domaines())->retrieveDomaineParent($domaine);
                    if ($domaineParent) {
                        $arrayDomaines[$domaineParent->getId()]['libelle'] = $domaineParent->getLibelle();
                        $fils['libelle'] = $domaine->getLibelle();
                        $arrayDomaines[$domaineParent->getId()]['fils'][] = $fils;
                    }
                }
            }
        }
        $this->RepeaterNomsDomaines->DataSource = $arrayDomaines;
        $this->RepeaterNomsDomaines->DataBind();
    }

    public function downloadBlob($sender, $param)
    {
        $idBlob = $param->CommandName;
        $nomBlob = $param->CommandParameter;
        $atexoBlob = new Atexo_Blob();
        $atexoBlob->sendBlobToClient($idBlob, $nomBlob, Atexo_Config::getParameter('COMMON_BLOB'));
        exit;
    }

    public function OnValidClick()
    {
        $cg76DonneeComplementaire = (new Atexo_DonneesComplementairesEntreprise_DonneesComplementairesEntreprise())->retrieveDonneComplementaireByIdEntreprise($this->idEntreprise);
        if ($cg76DonneeComplementaire instanceof CommonCG76DonneeComplementaireEntreprise) {
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $cg76DonneeComplementaire->setCommentaire($this->commentaires->Text);
            $cg76DonneeComplementaire->save($connexionCom);
        }
    }
}
