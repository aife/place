<?php

namespace Application\Controls;

use Application\Propel\Mpe\CommonIntervenantOrdreDuJour;
use Application\Propel\Mpe\CommonTCAOCommissionIntervenantExterne;
use Application\Propel\Mpe\CommonTCAOSeanceIntervenantExterne;
use Application\Propel\Mpe\CommonTCAOSeanceInvite;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_ValeursReferentielles;
use Application\Service\Atexo\Commission\Atexo_Commission_IntervenantExterne;

/**
 * composant d'affichage des intervenents externes.
 *
 * @author Amal EL BEKKAOUI <amal.elbekkaoui@atexo.com>
 * @copyright Atexo 2012
 *
 * @since MPE4
 */
class CAOListeIntervenant extends MpeTTemplateControl
{
    private bool $organisation = false;
    private bool $serviceIntervenant = false;
    private bool $fonction = false;
    private bool $typeCompte = false;
    private bool $isIntervenant = true;
    private bool $actionModifier = false;
    private bool $actionSupprimer = false;
    private bool $actions = true;
    private bool $checkbox = false;
    private bool $listeTypeVoix = false;
    private bool $typeVoix = false;
    private bool $typeVoixCommission = false;
    private bool $typeVoixOrdreJour = false;
    /* typeRecherche :
     * 1:liste intervenants externes;
     * 2:liste Agents
     * 3:liste agents commission
     */
    private string $typeRecherche = '1';

    private string $typeIntervenant = '1';

    public function getOrganisation()
    {
        return $this->organisation;
    }

    public function getServiceIntervenant()
    {
        return $this->serviceIntervenant;
    }

    public function getFonction()
    {
        return $this->fonction;
    }

    public function getTypeCompte()
    {
        return $this->typeCompte;
    }

    public function getIsIntervenant()
    {
        return $this->isIntervenant;
    }

    public function getActionModifier()
    {
        return $this->actionModifier;
    }

    public function getActionSupprimer()
    {
        return $this->actionSupprimer;
    }

    public function getActions()
    {
        return $this->actions;
    }

    public function getCheckbox()
    {
        return $this->checkbox;
    }

    public function getTypeRecherche()
    {
        return $this->typeRecherche;
    }

    public function getListeTypeVoix()
    {
        return $this->listeTypeVoix;
    }

    public function getTypeVoix()
    {
        return $this->typeVoix;
    }

    public function getTypeVoixCommission()
    {
        return $this->typeVoixCommission;
    }

    public function getTypeVoixOrdreJour()
    {
        return $this->typeVoixOrdreJour;
    }

    public function getTypeIntervenant()
    {
        return $this->typeIntervenant;
    }

    public function setTypeRecherche($x)
    {
        $this->typeRecherche = $x;
    }

    public function setOrganisation($x)
    {
        $this->organisation = $x;
    }

    public function setServiceIntervenant($x)
    {
        $this->serviceIntervenant = $x;
    }

    public function setFonction($x)
    {
        $this->fonction = $x;
    }

    public function setTypeCompte($x)
    {
        $this->typeCompte = $x;
    }

    public function setIsIntervenant($x)
    {
        $this->isIntervenant = $x;
    }

    public function setActionModifier($x)
    {
        $this->actionModifier = $x;
    }

    public function setActionSupprimer($x)
    {
        $this->actionSupprimer = $x;
    }

    public function setActions($x)
    {
        $this->actions = $x;
    }

    public function setCheckbox($x)
    {
        $this->checkbox = $x;
    }

    public function setListeTypeVoix($x)
    {
        $this->listeTypeVoix = $x;
    }

    public function setTypeVoix($x)
    {
        $this->typeVoix = $x;
    }

    public function setTypeVoixCommission($x)
    {
        $this->typeVoixCommission = $x;
    }

    public function setTypeVoixOrdreJour($x)
    {
        $this->typeVoixOrdreJour = $x;
    }

    public function setTypeIntervenant($x)
    {
        $this->typeIntervenant = $x;
    }

    public function fillRepeaterWithDataForSearchResult($criteriaVo)
    {
        $nombreElement = self::search($criteriaVo, true);
        if ($nombreElement >= 1) {
            $this->panelMoreThanOneElementFound->setVisible(true);
            $this->panelNoElementFound->setVisible(false);
            $this->nombreElement->Text = $nombreElement;
            $this->PagerBottom->setVisible(true);
            $this->PagerTop->setVisible(true);
            $this->setViewState('nombreElement', $nombreElement);
            $this->nombrePageTop->Text = ceil($nombreElement / $this->tableauIntervenant->PageSize);
            $this->nombrePageBottom->Text = ceil($nombreElement / $this->tableauIntervenant->PageSize);
            $this->tableauIntervenant->setVirtualItemCount($nombreElement);
            $this->tableauIntervenant->setCurrentPageIndex(0);
            $this->populateData($criteriaVo);
        } else {
            $this->panelMoreThanOneElementFound->setVisible(false);
            $this->panelNoElementFound->setVisible(true);
            $this->PagerBottom->setVisible(false);
            $this->PagerTop->setVisible(false);
        }
    }

    public function populateData($criteriaVo)
    {
        $offset = $this->tableauIntervenant->CurrentPageIndex * $this->tableauIntervenant->PageSize;
        $limit = $this->tableauIntervenant->PageSize;
        if ($offset + $limit > $this->tableauIntervenant->getVirtualItemCount()) {
            $limit = $this->tableauIntervenant->getVirtualItemCount() - $offset;
        }
        $criteriaVo->setOffset($offset);
        $criteriaVo->setLimit($limit);
        $arrayIntervenant = self::search($criteriaVo);
        $this->setViewState('elements', $arrayIntervenant);
        $this->tableauIntervenant->DataSource = $arrayIntervenant;
        $this->tableauIntervenant->DataBind();
    }

    public function pageChanged($sender, $param)
    {
        $this->tableauIntervenant->CurrentPageIndex = $param->NewPageIndex;
        $this->numPageBottom->Text = $param->NewPageIndex + 1;
        $this->numPageTop->Text = $param->NewPageIndex + 1;
        $criteriaVo = $this->getViewState('CriteriaVo');
        $this->populateData($criteriaVo);
    }

    public function elementSortedWith($panelName, $openCloseTag)
    {
        if ($this->getViewState('sortByElement') == $panelName) {
            if ('open' == $openCloseTag) {
                return '<strong>';
            } else {
                return '</strong>';
            }
        } else {
            return '';
        }
    }

    public function goToPage($sender, $param)
    {
        $numPage = null;
        switch ($sender->ID) {
            case 'DefaultButtonTop':
                 $numPage = $this->numPageTop->Text;
                break;
            case 'DefaultButtonBottom':
                 $numPage = $this->numPageBottom->Text;
                break;
        }
        if (Atexo_Util::isEntier($numPage)) {
            if ($numPage >= $this->nombrePageTop->Text) {
                $numPage = $this->nombrePageTop->Text;
            } elseif ($numPage <= 0) {
                $numPage = 1;
            }
            $this->tableauIntervenant->CurrentPageIndex = $numPage - 1;
            $this->numPageBottom->Text = $numPage;
            $this->numPageTop->Text = $numPage;
            $criteriaVo = $this->getViewState('CriteriaVo');
            $this->populateData($criteriaVo);
        } else {
            $this->numPageTop->Text = $this->tableauIntervenant->CurrentPageIndex + 1;
            $this->numPageBottom->Text = $this->tableauIntervenant->CurrentPageIndex + 1;
        }
    }

    public function changePagerLenght($sender, $param)
    {
        switch ($sender->ID) {
            case 'nombreResultatAfficherBottom':
                 $pageSize = $this->nombreResultatAfficherBottom->getSelectedValue();
                                                  $this->nombreResultatAfficherTop->setSelectedValue($pageSize);
                break;
            case 'nombreResultatAfficherTop':
                 $pageSize = $this->nombreResultatAfficherTop->getSelectedValue();
                                               $this->nombreResultatAfficherBottom->setSelectedValue($pageSize);
                break;
        }

        $this->tableauIntervenant->PageSize = $pageSize;
        $nombreElement = $this->getViewState('nombreElement');
        $this->nombrePageTop->Text = ceil($nombreElement / $this->tableauIntervenant->PageSize);
        $this->nombrePageBottom->Text = ceil($nombreElement / $this->tableauIntervenant->PageSize);
        $criteriaVo = $this->getViewState('CriteriaVo');
        $this->tableauIntervenant->setCurrentPageIndex(0);
        $this->populateData($criteriaVo);
    }

    public function search($c, $nbreElement = false)
    {
        $this->setViewState('CriteriaVo', $c);
        if (1 == $this->typeRecherche) {
            return (new Atexo_Commission_IntervenantExterne())->getIntervenants($c, $nbreElement);
        }

        return [];
    }

    public function getLibelleTypeVoix($id)
    {
        $typeVoix = $this->getViewState('typeVoix', []);
        if (!(is_array($typeVoix) && count($typeVoix) > 0)) {
            $typeVoix = (new Atexo_ValeursReferentielles())->getValeurRefByIdRef(Atexo_Config::getParameter('ID_REFERENTIEL_TYPE_VOIX_INTERVENANTS'));
            $this->setViewState('typeVoix', $typeVoix);
        }

        return $typeVoix[$id];
    }

    public function getTypeVoixIntervenant($id, $returnIdTypeVoix = false)
    {
        $idTypeVoix = null;
        if (1 == $this->getTypeIntervenant()) {
            $idTypeVoix = $this->getTypeVoixCommissionIntervenant($id, $returnIdTypeVoix);
        } elseif (2 == $this->getTypeIntervenant()) {
            $idTypeVoix = $this->getTypeVoixSeanceIntervenant($id, $returnIdTypeVoix);
        } elseif (3 == $this->getTypeIntervenant()) {
            $idTypeVoix = $this->getTypeVoixOrdreDePassageIntervenant($id, $returnIdTypeVoix);
        }

        $_t_typeVoix = $this->getViewState('typeVoix', []);
        if (!(is_array($_t_typeVoix) && count($_t_typeVoix) > 0)) {
            $_t_typeVoix = (new Atexo_ValeursReferentielles())->getValeurRefByIdRef(Atexo_Config::getParameter('ID_REFERENTIEL_TYPE_VOIX_INTERVENANTS'));
            $this->setViewState('typeVoix', $_t_typeVoix);
        }

        if ($idTypeVoix) {
            if ($returnIdTypeVoix) {
                return $idTypeVoix;
            } else {
                return $_t_typeVoix[$idTypeVoix];
            }
        }
        //Type de voix par defaut
        $last_item = array_slice($_t_typeVoix, -1, 1, true);
        $idTypeVoix = key($last_item);

        return $idTypeVoix;
    }

    public function getTypeVoixCommissionIntervenant($id, $returnIdTypeVoix = false)
    {
        $idCommission = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id']));
        $org = Atexo_CurrentUser::getCurrentOrganism();
        $intervenant = (new Atexo_Commission_IntervenantExterne())->getCommissionIntervenantExterne($id, $idCommission, $org);

        if ($intervenant instanceof CommonTCAOCommissionIntervenantExterne) {
            return $intervenant->getIdRefValTypeVoixDefaut();
        }
    }

    public function getTypeVoixSeanceIntervenant($id, $returnIdTypeVoix = false)
    {
        $idSeance = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id']));
        $org = Atexo_CurrentUser::getCurrentOrganism();
        $intervenant = (new Atexo_Commission_IntervenantExterne())->getSeanceIntervenantExterne($id, $idSeance, $org);

        if ($intervenant instanceof CommonTCAOSeanceIntervenantExterne) {
            return $intervenant->getIdRefValTypeVoix();
        }
    }

    public function getTypeVoixOrdreDePassageIntervenant($id, $returnIdTypeVoix)
    {
        //Recuperation des parametres d'interet
        $org = Atexo_CurrentUser::getCurrentOrganism();
        $idOrdreDePassage = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id']));

        //Recuperation de l'invite
        $invite = (new Atexo_Commission_IntervenantExterne())->getInviteOrdreDePassage($id, $idOrdreDePassage, $org);

        if ($invite instanceof CommonTCAOSeanceInvite) {
            return $invite->getIdRefValTypeVoix();
        }
    }

    public function getSelectedValueTypeVoix($idTypeVoix)
    {
        if ($idTypeVoix) {
            return $idTypeVoix;
        } else {
            $typeVoix = $this->getViewState('typeVoix', []);
            if (!(is_array($typeVoix) && count($typeVoix) > 0)) {
                $typeVoix = (new Atexo_ValeursReferentielles())->getValeurRefByIdRef(Atexo_Config::getParameter('ID_REFERENTIEL_TYPE_VOIX_INTERVENANTS'));
                $this->setViewState('typeVoix', $typeVoix);
            }
            $value = '';
            foreach ($typeVoix as $key => $value) {
                return $key;
            }
        }
    }

    public function getLibelleTypeVoixCommission($id, $returnIdTypeVoix = false)
    {
        return self::getTypeVoixIntervenant($id, $returnIdTypeVoix);
    }

    public function getLibelleTypeVoixOrdreJour($id, $returnIdTypeVoix = false)
    {
        return self::getTypeVoixIntervenantOrdreJour($id, $returnIdTypeVoix);
    }

    public function getTypeVoixIntervenantOrdreJour($id, $returnIdTypeVoix = false)
    {
        $idOrdreJour = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id']));
        $org = Atexo_CurrentUser::getCurrentOrganism();
        $intervenant = (new Atexo_Commission_IntervenantExterne())->getIntervenantOrdreJour($id, $idOrdreJour, $org);
        $typeVoix = $this->getViewState('typeVoix', []);
        if (!(is_array($typeVoix) && count($typeVoix) > 0)) {
            $typeVoix = (new Atexo_ValeursReferentielles())->getValeurRefByIdRef(Atexo_Config::getParameter('ID_REFERENTIEL_TYPE_VOIX_INTERVENANTS'));
            $this->setViewState('typeVoix', $typeVoix);
        }
        if ($intervenant instanceof CommonIntervenantOrdreDuJour) {
            $idTypeVoix = $intervenant->getTypeVoix();
            if ($idTypeVoix) {
                if ($returnIdTypeVoix) {
                    return $idTypeVoix;
                } else {
                    return $typeVoix[$idTypeVoix];
                }
            } else {
                foreach ($typeVoix as $key => $value) {
                    return $key;
                }
            }
        } else {
            foreach ($typeVoix as $key => $value) {
                return $key;
            }
        }
    }

    public function actionsRepeater($sender, $param)
    {
        if ('delete' == $param->CommandName) {
            $id = $param->CommandParameter;
            $this->Page->removeIntervenant($id, $this->getIsIntervenant());
            $criteriaVo = $this->getViewState('CriteriaVo');
            $this->fillRepeaterWithDataForSearchResult($criteriaVo);
        }
    }

    public function getListeAllTypeVoix()
    {
        $typeVoix = $this->getViewState('typeVoix', []);
        if (!(is_array($typeVoix) && count($typeVoix) > 0)) {
            $typeVoix = (new Atexo_ValeursReferentielles())->getValeurRefByIdRef(Atexo_Config::getParameter('ID_REFERENTIEL_TYPE_VOIX_INTERVENANTS'));
            $this->setViewState('typeVoix', $typeVoix);
        }

        return $typeVoix;
    }

    public function getIntervenantsExternes()
    {
        $data = [];
        foreach ($this->tableauIntervenant->Items as $item) {
            $id = $item->selectedIdIntervenant->value;
            $idTypeVoix = $item->selectTypeVoixIntervenant->SelectedValue;
            $data[$id]['ID'] = $id;
            $data[$id]['ID_REF_VAL_TYPE_VOIX_DEFAUT'] = $idTypeVoix;
        }

        return $data;
    }
}
