<?php

namespace Application\Controls;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonAutresPiecesMiseDisposition;
use Application\Propel\Mpe\CommonPiecesMiseDisposition;
use Application\Service\Atexo\Atexo_AutresPiecesMiseDisposition;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_Crypto;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Files;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_ValeursReferentielles;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Dce;
use Application\Service\Atexo\Consultation\Atexo_Consultation_DocumentExterneVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Responses;
use Application\Service\Atexo\Interfaces\Atexo_Interfaces_Offre;
use Application\Service\Atexo\MiseDispositionPieces\Atexo_MiseDispositionPieces_MiseDisposition;
use Application\Service\Atexo\MiseDispositionPieces\Atexo_MiseDispositionPieces_PieceMiseDispisitionVo;
use Application\Service\Atexo\Publicite\Atexo_Publicite_Avis;
use Exception;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * commentaires.
 *
 * @author khadija Chouika <khadija.chouika@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 2.1
 *
 * @since MPE-4.0
 */
class PiecesConsultationMiseDisposition extends MpeTTemplateControl
{
    private $_typePiece;
    private $_reference;
    private $_titre;

    public function setTitre($value)
    {
        $this->_titre = $value;
    }

    public function getTitre()
    {
        return $this->_titre;
    }

    public function setTypePiece($value)
    {
        $this->_typePiece = $value;
    }

    public function getTypePiece()
    {
        return $this->_typePiece;
    }

    public function setReference($value)
    {
        $this->_reference = $value;
    }

    public function getReference()
    {
        return $this->_reference;
    }

    public function onLoad($param)
    {
        $this->labelServerSide->Text = "<script>document.getElementById('divValidationSummary').style.display='none';</script>";
    }

    public function fillRepeaterPiecesMiseDisposition()
    {
        $this->_reference = $this->Page->_refConsultation;
        $this->setViewState('refConsultation', $this->_reference);
        $arraypieces = [];
        switch ($this->_typePiece) {
            case Atexo_Config::getParameter('TYPE_PIECES_DEPOT_ATTRIBUTAIRE'):
                $arraypieces = $this->fillTableauDepotAttributaire();

            break;
            case Atexo_Config::getParameter('TYPE_PIECES_DCE'):
                $arraypieces = $this->fillTableauDce();

            break;
            case Atexo_Config::getParameter('TYPE_PIECES_PUBLICITE'):
                $arraypieces = $this->fillTableauPublicite();

            break;
            case Atexo_Config::getParameter('TYPE_AUTRE_PIECES'):
                $arraypieces = $this->fillTableauAutresPieces();

            break;
            case Atexo_Config::getParameter('TYPE_DOCUMENT_EXTERNE'):
                $arraypieces = $this->fillTableauDocumentExterne();

            break;
        }
        $this->tablePiecesMarche->datasource = $arraypieces;
        $this->tablePiecesMarche->DataBind();
        if ($arraypieces) {
            $this->panelAucuneAction->setStyle('display:none');
        } else {
            $this->panelAucuneAction->setStyle('display:block');
        }
    }

    public function mettreAdisponible($sender, $param)
    {
        if ($this->Page->IsValid) {
            try {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                $pieces = $sender->CommandParameter;
                $arrayParam = explode('#', $pieces);
                $objetPiece = new CommonPiecesMiseDisposition();
                $objetPiece->setIdTypeDoc($sender->Parent->dce_piece_type_doc->SelectedValue);
                $arrayAttr = $this->Page->getAttributaire();
                $objetPiece->setIdDecisionEnveloppe($arrayAttr[0]);
                $objetPiece->setlot($arrayAttr[1]);
                $objetPiece->setOrg(Atexo_CurrentUser::getCurrentOrganism());
                $objetPiece->setIdExterne($arrayParam[0]);
                $objetPiece->setConsultationId($this->Page->_refConsultation);
                $objetPiece->setDateMiseDisposition(date('Y-m-d H:i:s'));
                $objetPiece->setStatutDisposition(Atexo_Config::getParameter('STATUT_MISE_DISPOSITION'));
                $objetPiece->setIdBlob($arrayParam[2]);
                $objetPiece->setIdDestinataire($this->Page->destination->SelectedValue);
                $objetPiece->setType($arrayParam[1]);
                $objetPiece->save($connexion);
                $this->fillRepeaterPiecesMiseDisposition();
                $this->panelPiece->render($param->getNewWriter());
            } catch (Exception $e) {
                echo $e->getMessage();
            }
        }
    }

    public function annulerMettreAdisponible($sender, $param)
    {
        try {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $pieces = $sender->CommandParameter;
            $arrayParam = explode('#', $pieces);
            $arrayAttr = $this->Page->getAttributaire();
            $attributaire = $arrayAttr[0];
            $detinataire = $this->Page->destination->SelectedValue;
            $objetpiece = (new Atexo_MiseDispositionPieces_MiseDisposition())->retrievePiece($arrayParam[0], Atexo_CurrentUser::getCurrentOrganism(), $attributaire, $detinataire, $this->Page->_refConsultation, false, $arrayParam[1]);
            if ($objetpiece instanceof CommonPiecesMiseDisposition) {
                $objetpiece->delete($connexion);
            }
            $this->fillRepeaterPiecesMiseDisposition();
            $this->panelPiece->render($param->getNewWriter());
        } catch (Exception $e) {
            //echo $e->getMessage();exit;
            Prado::log('Erreur PiecesConsultationMiseDisposition.php'.$e->getMessage().$e->getTraceAsString(), TLogger::ERROR, 'PiecesConsultationMiseDisposition.php');
        }
    }

    /**
     * retourne le libelle du type de document.
     * */
    public function getTypeDocName($id)
    {
        $arrayTypesDocuments = (new Atexo_ValeursReferentielles())->getValeurRefByIdRef(Atexo_Config::getParameter('REF_TYPE_DOCUMENTS'));
        foreach ($arrayTypesDocuments as $key => $type) {
            if ($key == $id) {
                return $type;
            }
        }
    }

    public function ajoutAutrePiece($sender, $param)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $atexoBlob = new Atexo_Blob();
        $atexoCrypto = new Atexo_Crypto();
        $arrtibuLot = explode('-', $this->Page->selectionAttributaire->SelectedValue);
        $attributaire = $arrtibuLot[0];
        $detinataire = $this->Page->destination->SelectedValue;

        $infilepiece = Atexo_Config::getParameter('COMMON_TMP').$this->uploadBar->reponseServeur->value;
        // $infilepiece = $this->uploadBar->reponseServeur->value ;
        if ($this->uploadBar->reponseServeur->value) {
            $arrayTimeStampAutrePieces = $atexoCrypto->timeStampFile($infilepiece);
            if (is_array($arrayTimeStampAutrePieces)) {
                $objetAP = new CommonAutresPiecesMiseDisposition();
                $objetAP->setHorodatage($arrayTimeStampAutrePieces['horodatage']);
                $objetAP->setUntrusteddate($arrayTimeStampAutrePieces['untrustedDate']);
                $objetAP->setOrg(Atexo_CurrentUser::getCurrentOrganism());
                $objetAP->setAgentId(Atexo_CurrentUser::getIdAgentConnected());

                $objetAPIdBlob = $atexoBlob->insert_blob($this->uploadBar->NomFichier->value, $infilepiece, Atexo_CurrentUser::getCurrentOrganism());

                $objetAP->setIdBlob($objetAPIdBlob);
                $objetAP->setNomFichier($this->uploadBar->NomFichier->value);
                $taille = (new Atexo_Blob())->getTailFile($objetAPIdBlob, Atexo_CurrentUser::getCurrentOrganism());
                $objetAP->setTailleFichier($taille);
                $objetAP->setIdDestinataire($this->Page->destination->SelectedValue);
                $objetAP->setDateCreation(date('Y-m-d H:i:s'));
                $arrayAttr = $this->Page->getAttributaire();
                $attributaire = $arrayAttr[0];
                $objetAP->setIdDecisionEnveloppe($attributaire);
                $objetAP->save($connexion);
                if ('0' != $objetAPIdBlob) {
                    // objet mise à disponibilite
                    $objetPiece = new CommonPiecesMiseDisposition();
                    $objetPiece->setIdTypeDoc($this->idType->value);
                    $objetPiece->setIdDecisionEnveloppe($attributaire[0]);
                    $objetPiece->setOrg(Atexo_CurrentUser::getCurrentOrganism());
                    $objetPiece->setIdExterne($objetAP->getId());
                    $objetPiece->setConsultationId($this->Page->_refConsultation);
                    $objetPiece->setIdBlob($objetAPIdBlob);
                    $objetPiece->setIdDestinataire($detinataire);
                    $objetPiece->setType(Atexo_Config::getParameter('TYPE_AUTRE_PIECES'));
                    $objetPiece->save($connexion);
                    $this->fillRepeaterPiecesMiseDisposition();
                    $this->panelPiece->render($param->getNewWriter());
                } else {
                    $this->Parent->panelMessageErreur->setVisible(true);
                    $this->Parent->panelMessageErreur->setMessage("Probleme d'enregistrement du fichier reglement de la consultation");

                    return;
                }
            } else {
                $this->Parent->panelMessageErreur->setVisible(true);
                $this->Parent->panelMessageErreur->setMessage("Probleme d'horodatage du fichier reglement de la consultation");

                return;
            }
        } else {
            $this->Parent->panelMessageErreur->setVisible(true);
            $this->Parent->panelMessageErreur->setMessage('Probleme de taille du fichier reglement de la consultation');

            return;
        }
    }

    public function getUrlNewDownload($sender, $param)
    {
        $index = $sender->CommandParameter;
        if ($index) {
            return (new Atexo_Files())->downloadPartialConsFile($this->getViewState('refConsultation'), Atexo_CurrentUser::getCurrentOrganism(), $index);
        }
    }

    public function supprimerAutrePiece($sender, $param)
    {
        try {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $indexPiece = $sender->CommandParameter;
            if ($indexPiece) {
                $objetAutrepiece = (new Atexo_AutresPiecesMiseDisposition())->getAutrePieceById($indexPiece);
                if ($objetAutrepiece instanceof CommonAutresPiecesMiseDisposition) {
                    $idBlob = $objetAutrepiece->getIdBlob();
                    $blob = new Atexo_Blob();
                    $organisme = Atexo_CurrentUser::getCurrentOrganism();
                    $blob->deleteBlobFile($idBlob, $organisme);
                    $objetAutrepiece->delete($connexion);
                }
                $this->fillRepeaterPiecesMiseDisposition();
                $this->panelPiece->render($param->getNewWriter());
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function getFileSize($idEnveloppe)
    {
        return (new Atexo_Consultation_Responses())->getFileSize($idEnveloppe, Atexo_CurrentUser::getCurrentOrganism());
    }

    public function getPiecesSelect($justIds = false)
    {
        try {
            $arrayAttr = explode('-', $this->Page->selectionAttributaire->SelectedValue);
            $detinataire = $this->Page->destination->SelectedValue;
            $data = [];
            foreach ($this->tablePiecesMarche->Items as $item) {
                if ($item->piece_item->checked) {
                    $idType = $item->dce_piece_type_doc->SelectedValue;
                    if (!$justIds) {
                        if (0 != $idType && '' == $item->type->Text) {
                            $objetPiece = new CommonPiecesMiseDisposition();
                            $objetPiece->setIdTypeDoc($idType);
                            $objetPiece->setIdDecisionEnveloppe($arrayAttr[0]);
                            $objetPiece->setlot($arrayAttr[1]);
                            $objetPiece->setOrg(Atexo_CurrentUser::getCurrentOrganism());
                            $objetPiece->setIdExterne($item->id->Value);
                            $objetPiece->setConsultationId($this->Page->_refConsultation);
                            $objetPiece->setDateMiseDisposition(date('Y-m-d H:i:s'));
                            $objetPiece->setStatutDisposition(Atexo_Config::getParameter('STATUT_MISE_DISPOSITION'));
                            $objetPiece->setIdBlob($item->idsBlob->Value);
                            $objetPiece->setIdDestinataire($detinataire);
                            $objetPiece->setType($item->id_type->Value);
                        }
                    } elseif ('' != $item->type->Text) {
                        $objetPiece = (new Atexo_MiseDispositionPieces_MiseDisposition())->retrievePiece($item->id->Value, Atexo_CurrentUser::getCurrentOrganism(), $arrayAttr[0], $detinataire, $this->Page->_refConsultation, $arrayAttr[1], $item->id_type->Value);
                    }
                    if ($objetPiece) {
                        $data[] = $objetPiece;
                    }
                }
            }

            return $data;
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    public function verifyTypeDoc($sender, $param)
    {
        $imgError = null;
        if (0 == $param->value) {
            $param->IsValid = false;
            $imgError = $this->idClientErreur->value;
            $this->labelServerSide->Text = "<script>document.getElementById('divValidationSummary').style.display='';
			document.getElementById('".$imgError."').style.display='';</script>";
            $this->idValidationTypeDoc->ErrorMessage = Prado::localize('TEXT_ERREUR_SELECTION_TYPE_DOC_MISE_DISPOSITION');
            $this->Page->divValidationSummary->addServerError(Prado::localize('TEXT_ERREUR_SELECTION_TYPE_DOC_MISE_DISPOSITION'));

            return;
        } else {
            $param->IsValid = true;
            $this->labelServerSide->Text = "<script>document.getElementById('divValidationSummary').style.display='none';
			document.getElementById('".$imgError."').style.display='none';</script>";
        }
    }

    public function chargerTypeDocument()
    {
        $valeursReferentiels = [];
        $valeursReferentiels[0] = Prado::localize('TEXT_RENSEIGNER_TYPE_DOCUMENT');
        $arrayTypesDocuments = (new Atexo_ValeursReferentielles())->getValeurRefByIdRef(Atexo_Config::getParameter('REF_TYPE_DOCUMENTS'), $valeursReferentiels);

        return $arrayTypesDocuments;
    }

    public function fillTableauDce()
    {
        $arraypieces = [];
        $result = (new Atexo_Consultation_Dce())->getArborescence($this->_reference, Atexo_CurrentUser::getCurrentOrganism(), null, false, false, true);
        if ($result) {
            $idBlob = $result[0];
            $dce = $result[1];
            $ordre = '';
            $cle = '';
            foreach ($dce as $ordre => $directory_files) {
                foreach ($directory_files as $cle => $value) {
                    if ('file' == $value['type']) {
                        $piece = new Atexo_MiseDispositionPieces_PieceMiseDispisitionVo();
                        $arrtibuLot = explode('-', $this->Page->selectionAttributaire->SelectedValue);
                        $attributaire = $arrtibuLot[0];
                        $detinataire = $this->Page->destination->SelectedValue;
                        $objetpiece = (new Atexo_MiseDispositionPieces_MiseDisposition())->retrievePiece($value['index'], Atexo_CurrentUser::getCurrentOrganism(), $attributaire, $detinataire, $this->_reference, false, Atexo_Config::getParameter('TYPE_PIECES_DCE'));
                        if ($objetpiece instanceof CommonPiecesMiseDisposition) {
                            $piece->setTypeDocument($objetpiece->getIdTypeDoc());

                            if ($objetpiece->getStatutDisposition()) {
                                $date = Atexo_Util::formatterDate($objetpiece->getDateMiseDisposition());
                                $piece->setDateMiseDisposition($date);
                                $piece->setStatutMiseDisposition(Atexo_Config::getParameter('STATUT_MISE_DISPOSITION'));
                            }
                            if ($objetpiece->getStatutRecuperation()) {
                                if ($objetpiece->getStatutRecuperation() == Atexo_Config::getParameter('STATUT_ERREUR_RECUPERATION')) {
                                    $piece->setStatutRecuperation(Atexo_Config::getParameter('STATUT_ERREUR_RECUPERATION'));
                                    $piece->setMessage($objetpiece->getMessage());
                                    $date = Atexo_Util::formatterDate($objetpiece->getDateRecuperation());
                                    $piece->setDateRecuperation($date);
                                } elseif ($objetpiece->getStatutRecuperation() == Atexo_Config::getParameter('STATUT_SUCCES_RECUPERATION')) {
                                    $piece->setStatutRecuperation(Atexo_Config::getParameter('STATUT_SUCCES_RECUPERATION'));
                                    $date = Atexo_Util::formatterDate($objetpiece->getDateRecuperation());
                                    $piece->setDateRecuperation($date);
                                }
                            }
                        }
                        $name = $value['name'];
                        if ($value['taille']) {
                            $name .= ' ('.$value['taille'].')';
                        }

                        $piece->setName($name);
                        $piece->setId($value['index']);
                        $piece->setIdBlob($idBlob);
                        $piece->setType(Atexo_Config::getParameter('TYPE_PIECES_DCE'));
                        $arraypieces[] = $piece;
                    }
                }
            }
        }

        return $arraypieces;
    }

    public function fillTableauPublicite()
    {
        $arraypieces = [];
        $arrtibuLot = explode('-', $this->Page->selectionAttributaire->SelectedValue);
        $attributaire = $arrtibuLot[0];
        $lot = $arrtibuLot[1];
        $detinataire = $this->Page->destination->SelectedValue;
        $objetPublicite = (new Atexo_Publicite_Avis())->retreiveListAvisEnvoye($this->_reference, Atexo_CurrentUser::getCurrentOrganism());
        foreach ($objetPublicite as $publicite) {
            $piece = new Atexo_MiseDispositionPieces_PieceMiseDispisitionVo();
            $objetpiece = (new Atexo_MiseDispositionPieces_MiseDisposition())->retrievePiece($publicite->getId(), Atexo_CurrentUser::getCurrentOrganism(), $attributaire, $detinataire, $this->_reference, $lot, Atexo_Config::getParameter('TYPE_PIECES_PUBLICITE'));
            if ($objetpiece instanceof CommonPiecesMiseDisposition) {
                $piece->setTypeDocument($objetpiece->getIdTypeDoc());
                if ($objetpiece->getStatutDisposition()) {
                    $date = Atexo_Util::formatterDate($objetpiece->getDateMiseDisposition());
                    $piece->setDateMiseDisposition($date);
                    $piece->setStatutMiseDisposition(Atexo_Config::getParameter('STATUT_MISE_DISPOSITION'));
                }
                if ($objetpiece->getStatutRecuperation()) {
                    if ($objetpiece->getStatutRecuperation() == Atexo_Config::getParameter('STATUT_ERREUR_RECUPERATION')) {
                        $piece->setStatutRecuperation(Atexo_Config::getParameter('STATUT_ERREUR_RECUPERATION'));
                        $piece->setMessage($objetpiece->getMessage());
                        $date = Atexo_Util::formatterDate($objetpiece->getDateRecuperation());
                        $piece->setDateRecuperation($date);
                    } elseif ($objetpiece->getStatutRecuperation() == Atexo_Config::getParameter('STATUT_SUCCES_RECUPERATION')) {
                        $piece->setStatutRecuperation(Atexo_Config::getParameter('STATUT_SUCCES_RECUPERATION'));
                        $date = Atexo_Util::formatterDate($objetpiece->getDateRecuperation());
                        $piece->setDateRecuperation($date);
                    }
                }
            }
            $name = $publicite->getNomFichier();
            $tailleFichier = (new Atexo_Blob())->getTailFile($publicite->getAvis(), Atexo_CurrentUser::getCurrentOrganism());
            $tailleFichier = ($tailleFichier / 1024);
            $tailleFichier = Atexo_Util::arrondirSizeFile($tailleFichier);
            if ($tailleFichier) {
                $name .= ' ('.$tailleFichier.')';
            }
            $piece->setName($name);
            $piece->setId($publicite->getId());
            $piece->setIdBlob($publicite->getAvis());
            $piece->setType(Atexo_Config::getParameter('TYPE_PIECES_PUBLICITE'));
            $arraypieces[] = $piece;
        }

        return $arraypieces;
    }

    public function fillTableauDepotAttributaire()
    {
        $arraypieces = [];
        $arrtibuLot = explode('-', $this->Page->selectionAttributaire->SelectedValue);
        $attributaire = $arrtibuLot[0];
        $detinataire = $this->Page->destination->SelectedValue;
        $lot = $arrtibuLot[1];
        if ($attributaire) {
            $offre = (new Atexo_Interfaces_Offre())->getOffreByIdOffre($arrtibuLot[2]);
            $resultPieces = (new Atexo_MiseDispositionPieces_MiseDisposition())->getAllFichiersOffreByLot($offre, Atexo_CurrentUser::getCurrentOrganism(), $this->_reference, $lot);
            foreach ($resultPieces as $piece) {
                $objetpiece = (new Atexo_MiseDispositionPieces_MiseDisposition())->retrievePiece($piece->getId(), Atexo_CurrentUser::getCurrentOrganism(), $attributaire, $detinataire, $this->_reference, false, Atexo_Config::getParameter('TYPE_PIECES_DEPOT_ATTRIBUTAIRE'));
                if ($objetpiece instanceof CommonPiecesMiseDisposition) {
                    $piece->setTypeDocument($objetpiece->getIdTypeDoc());

                    if ($objetpiece->getStatutDisposition()) {
                        $date = Atexo_Util::formatterDate($objetpiece->getDateMiseDisposition());
                        $piece->setDateMiseDisposition($date);
                        $piece->setStatutMiseDisposition(Atexo_Config::getParameter('STATUT_MISE_DISPOSITION'));
                    }
                    if ($objetpiece->getStatutRecuperation()) {
                        if ($objetpiece->getStatutRecuperation() == Atexo_Config::getParameter('STATUT_ERREUR_RECUPERATION')) {
                            $piece->setStatutRecuperation(Atexo_Config::getParameter('STATUT_ERREUR_RECUPERATION'));
                            $piece->setMessage($objetpiece->getMessage());
                            $date = Atexo_Util::formatterDate($objetpiece->getDateRecuperation());
                            $piece->setDateRecuperation($date);
                        } elseif ($objetpiece->getStatutRecuperation() == Atexo_Config::getParameter('STATUT_SUCCES_RECUPERATION')) {
                            $piece->setStatutRecuperation(Atexo_Config::getParameter('STATUT_SUCCES_RECUPERATION'));
                            $date = Atexo_Util::formatterDate($objetpiece->getDateRecuperation());
                            $piece->setDateRecuperation($date);
                        }
                    }
                }
                $piece->setType(Atexo_Config::getParameter('TYPE_PIECES_DEPOT_ATTRIBUTAIRE'));
                $arraypieces[] = $piece;
            }
        }

        return $arraypieces;
    }

    public function fillTableauAutresPieces()
    {
        $arraypieces = [];
        $attributaire = explode('-', $this->Page->selectionAttributaire->SelectedValue);
        if ($attributaire) {
            $detinataire = $this->Page->destination->SelectedValue;
            $autresPieces = (new Atexo_AutresPiecesMiseDisposition())->getAllPieces($attributaire[0], $detinataire, Atexo_CurrentUser::getCurrentOrganism());
            if ($autresPieces) {
                foreach ($autresPieces as $autrePiece) {
                    $piece = new Atexo_MiseDispositionPieces_PieceMiseDispisitionVo();
                    $name = $autrePiece->getNomFichier();
                    if ($autrePiece->getTailleFichier()) {
                        $tailleFichier = ($autrePiece->getTailleFichier() / 1024); // a tester
                        $tailleFichier = Atexo_Util::arrondirSizeFile($tailleFichier);
                        $name .= '( '.$tailleFichier.' )';
                    }
                    $piece->setName(Atexo_Util::utf8ToIso($name));
                    $piece->setId($autrePiece->getId());
                    $piece->setIdBlob($autrePiece->getIdBlob());
                    $piece->setType(Atexo_Config::getParameter('TYPE_AUTRE_PIECES'));
                    $objetpiece = (new Atexo_MiseDispositionPieces_MiseDisposition())->retrievePiece($autrePiece->getId(), Atexo_CurrentUser::getCurrentOrganism(), $attributaire[0], $detinataire, $this->_reference, false, Atexo_Config::getParameter('TYPE_AUTRE_PIECES'));
                    if ($objetpiece instanceof CommonPiecesMiseDisposition) {
                        $piece->setTypeDocument($objetpiece->getIdTypeDoc());
                        if ($objetpiece->getStatutDisposition()) {
                            $piece->setStatutMiseDisposition(Atexo_Config::getParameter('STATUT_MISE_DISPOSITION'));
                            $date = Atexo_Util::formatterDate($objetpiece->getDateMiseDisposition());
                            $piece->setDateMiseDisposition($date);
                        }
                        if ($objetpiece->getStatutRecuperation()) {
                            if ($objetpiece->getStatutRecuperation() == Atexo_Config::getParameter('STATUT_ERREUR_RECUPERATION')) {
                                $piece->setStatutRecuperation(Atexo_Config::getParameter('STATUT_ERREUR_RECUPERATION'));
                                $piece->setMessage($objetpiece->getMessage());
                                $date = Atexo_Util::formatterDate($objetpiece->getDateRecuperation());
                                $piece->setDateRecuperation($date);
                            } elseif ($objetpiece->getStatutRecuperation() == Atexo_Config::getParameter('STATUT_SUCCES_RECUPERATION')) {
                                $piece->setStatutRecuperation(Atexo_Config::getParameter('STATUT_SUCCES_RECUPERATION'));
                                $date = Atexo_Util::formatterDate($objetpiece->getDateRecuperation());
                                $piece->setDateRecuperation($date);
                            }
                        }
                    }
                    $arraypieces[] = $piece;
                }
            }
        }

        return $arraypieces;
    }

    public function fillTableauDocumentExterne()
    {
        $arraypieces = [];
        if (isset($_GET['id'])) {
            $criteriaVo = new Atexo_Consultation_DocumentExterneVo();
            $criteriaVo->setRefConsultation(Atexo_Util::atexoHtmlEntities($_GET['id']));
            $consultation = new Atexo_Consultation();
            $documentExterne = $consultation->searchDocument($criteriaVo);
            if ($documentExterne) {
                foreach ($documentExterne as $document) {
                    $piece = new Atexo_MiseDispositionPieces_PieceMiseDispisitionVo();
                    $arrtibuLot = explode('-', $this->Page->selectionAttributaire->SelectedValue);
                    $attributaire = $arrtibuLot[0];
                    $detinataire = $this->Page->destination->SelectedValue;
                    $objetpiece = (new Atexo_MiseDispositionPieces_MiseDisposition())->retrievePiece($document->getId(), Atexo_CurrentUser::getCurrentOrganism(), $attributaire, $detinataire, $_GET['id'], false, Atexo_Config::getParameter('TYPE_DOCUMENT_EXTERNE'));
                    if ($objetpiece instanceof CommonPiecesMiseDisposition) {
                        $piece->setTypeDocument($objetpiece->getIdTypeDoc());
                        if ($objetpiece->getStatutDisposition()) {
                            $date = Atexo_Util::formatterDate($objetpiece->getDateMiseDisposition());
                            $piece->setDateMiseDisposition($date);
                            $piece->setStatutMiseDisposition(Atexo_Config::getParameter('STATUT_MISE_DISPOSITION'));
                        }
                        if ($objetpiece->getStatutRecuperation()) {
                            if ($objetpiece->getStatutRecuperation() == Atexo_Config::getParameter('STATUT_ERREUR_RECUPERATION')) {
                                $piece->setStatutRecuperation(Atexo_Config::getParameter('STATUT_ERREUR_RECUPERATION'));
                                $piece->setMessage($objetpiece->getMessage());
                                $date = Atexo_Util::formatterDate($objetpiece->getDateRecuperation());
                                $piece->setDateRecuperation($date);
                            } elseif ($objetpiece->getStatutRecuperation() == Atexo_Config::getParameter('STATUT_SUCCES_RECUPERATION')) {
                                $piece->setStatutRecuperation(Atexo_Config::getParameter('STATUT_SUCCES_RECUPERATION'));
                                $date = Atexo_Util::formatterDate($objetpiece->getDateRecuperation());
                                $piece->setDateRecuperation($date);
                            }
                        }
                    }
                    $name = $document->getNom();
                    $taille = (new Atexo_Blob())->getTailFile($document->getIdBlob(), Atexo_CurrentUser::getCurrentOrganism());
                    $tailleFichier = ($taille / 1024);
                    $tailleFichier = Atexo_Util::arrondirSizeFile($tailleFichier);
                    if ($tailleFichier) {
                        $name .= ' ('.$tailleFichier.')';
                    }
                    $piece->setName($name);
                    $piece->setId($document->getId());
                    $piece->setIdBlob($document->getIdBlob());
                    $piece->setType(Atexo_Config::getParameter('TYPE_DOCUMENT_EXTERNE'));
                    $arraypieces[] = $piece;
                }
            }
        }

        return $arraypieces;
    }

    public function downloadDocExterneFile($sender, $param)
    {
        $org = Atexo_CurrentUser::getCurrentOrganism();
        $blob_id = $param->CommandParameter;
        $fileName = $param->CommandName;
        $blob = new Atexo_Blob();
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Pragma: public');
        header('Content-Type: application/octet-stream');
        header('Content-Tranfert-Encoding: binary');
        $blob->sendBlobToClient($blob_id, $fileName, $org);
        exit;
    }
}
