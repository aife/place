<!--Debut Bloc Recherche avancee-->
                <div class="breadcrumbs"><span class="normal"><com:TTranslate>TEXT_ADMINISTRATION_METIER</com:TTranslate></span>
                &gt; <com:TTranslate>TEXT_VISUALIER_ENTREPRISE</com:TTranslate></div>
                <div class="form-field" style="display: block;">
                <div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><span
                	class="title"><com:TTranslate>TEXT_RECHERCHE_MULTI_CRITERES</com:TTranslate></span></div>
                <div class="content"><!--Debut Ligne Reference-->
                <div class="line">
                <div class="intitule-150"><label for="raisonSociale"><com:TTranslate>TEXT_RAISON_SOCIALE</com:TTranslate></label>
                :</div>
                <com:TTextBox id="raisonSociale" cssClass="moyen" Attributes.title="<%=Prado::localize('TEXT_RAISON_SOCIALE')%>" />
                				</div>
                <div class="spacer-small"></div>
                <!--Debut Line Entreprise etablie en France-->
                <div class="line">
                <div class="intitule-auto bloc-700">
                <com:TRadioButton Checked="true"
                	GroupName="RadioGroup" id="entrepriseLocale" Attributes.title="<%=Prado::localize('TEXT_ENTREPRISE_ETABLIE_EN_FRANCE')%>" cssClass="radio" Attributes.onclick="isCheckedShowDiv(this,'etablieFrance');isCheckedHideDiv(this,'nonEtablieFrance');" /><label
                		for="france"><com:TTranslate>TEXT_ENTREPRISE_ETABLIE_EN_FRANCE</com:TTranslate>
                	</label></div>
                <div id="etablieFrance" style="display: block;">
                <com:TPanel ID="panelSiren">
                    <div class="intitule-120 indent-30">
                        <div class="float-left">
                        	<label for="ifu">
                        		<com:TTranslate>TEXT_SIREN</com:TTranslate> :
                       		</label>
                        </div>
                	</div>
                <com:TTextBox id="ifu" Attributes.title="SIREN"
                	cssClass="siren" />
                </com:TPanel>	
                <com:TPanel ID="panelRc" >
	        		<div class="intitule-120 indent-30">
	        			<label for="<%=$this->getClientId()%>villeRc"><com:TTranslate>DEFINE_VILLE_RC</com:TTranslate> :</label>  
	        		</div>
	        		<com:TDropDownList ID="villeRc" Attributes.title="<%=Prado::localize('DEFINE_VILLE_RC')%>"  CssClass="select-185"/>
	    		  	<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" onmouseover="afficheBulle('infosSiret', this)" onmouseout="cacheBulle('infosSiret')" class="picto-info-intitule" alt="Info-bulle" title="Info-bulle" />
			  		<div class="breaker"></div>
		      		<div class="intitule-120 indent-30">
		      			<label for="<%=$this->getClientId()%>numeroRc"><com:TTranslate>DEFINE_NUMERO</com:TTranslate></label> :
		      		</div>
		      		<com:TTextBox  id="numeroRc" CssClass="input-185" Attributes.title="<%=Prado::localize('DEFINE_NUMERO')%>" />
        		 </com:TPanel>	
        		 <com:TPanel ID="panelIdentifiantUnique"> 
                    <div class="intitule-120 indent-30">
                        <div class="float-left">
                        	<label for="ifu">
                        		<com:TTranslate>TEXT_SIREN</com:TTranslate> :
                       		</label>
                        </div>
                	</div>
                <com:TTextBox id="identifiantUnique" Attributes.title="SIREN"
                	cssClass="siren" />
                </com:TPanel>	
                </div>
                </div>
                <!--Fin Line Entreprise etablie en France--> <!--Debut Line Entreprise non etablie en France-->
                <div class="line">
                <div class="intitule-auto bloc-700"><com:TRadioButton
                	GroupName="RadioGroup" id="etranger" Attributes.title="<%=Prado::localize('TEXT_ENTREPRISE_NON_ETABLIE_EN_FRANCE')%>" cssClass="radio" Attributes.onclick="isCheckedShowDiv(this,'nonEtablieFrance');isCheckedHideDiv(this,'etablieFrance');" /><label
                		for="etranger"><com:TTranslate>TEXT_ENTREPRISE_NON_ETABLIE_EN_FRANCE</com:TTranslate></label></div>
                <div id="nonEtablieFrance" style="display: none;">
                <div class="intitule-120 indent-30"><label for="pays"><com:TTranslate>TEXT_PAYS</com:TTranslate></label>
                :</div>
                <com:DropDownListCountries TypeAffichage="withCode" ID="listPays"
                	cssClass="select-185" />
                <div class="breaker"></div>
                <div class="intitule-120 indent-30"><label for="idNational"><com:TTranslate>TEXT_IDENTIFIANT_NATIONNAL</com:TTranslate></label>
                :</div>
                <com:TTextBox id="idNational" Attributes.title="<%=Prado::localize('TEXT_IDENTIFIANT_NATIONNAL')%>" cssClass="input-185" /><span
                		class="info-aide-right"><com:TTranslate>TEXT_NUM_ENREGISTREMENT_NATIONAL</com:TTranslate></span></div>
                <div class="spacer-mini"></div>
                </div>
                <div class="spacer"></div>
                <!--Fin Line Entreprise non etablie en France-->
            <div class="spacer"></div>
          <!--Debut Ligne Recherche par date-->
				<div class="line">
					<div class="intitule-150"><com:TTranslate>TEXT_DATE_CREACTION_COMPTE</com:TTranslate> :</div>
					<div class="intitule-auto"><label for="dateStart"><com:TTranslate>TEXT_RECHERCHE_AVANCEE_AVANT_LE</com:TTranslate> </label></div>
					<div class="calendar">
						<com:TTextBox Attributes.title="<%=Prado::localize('TEXT_RECHERCHE_AVANCEE_AVANT_LE')%>"  id="dateStart" cssClass="heure" />
						<com:TImage ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-calendar.gif" Attributes.alt="<%=Prado::localize('DEFINE_CALENDRIER')%>" Attributes.title="<%=Prado::localize('DEFINE_CALENDRIER')%>" Attributes.onclick="popUpCalendar(this,document.getElementById('ctl0_CONTENU_PAGE_panelSearch_dateStart'),'dd/mm/yyyy','');" />
						<com:TDataTypeValidator	ID="controlDateStart" ValidationGroup="datesValidation" ControlToValidate="dateStart" DataType="Date"	DateFormat="dd/M/yyyy" Text="<%=Prado::localize('MSG_ERROR_FORMAT_DATE_INVALIDE')%>" Display="Dynamic"/>
					</div>
					<div class="intitule-auto indent-10"><label for="dateEnd"><com:TTranslate>ET_LE</com:TTranslate> </label></div>
					<div class="calendar">
						<com:TTextBox Attributes.title="<%=Prado::localize('ET_LE')%>" id="dateEnd" cssClass="heure" />
						<com:TImage ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-calendar.gif" Attributes.alt="<%=Prado::localize('DEFINE_CALENDRIER')%>" Attributes.title="<%=Prado::localize('DEFINE_CALENDRIER')%>" Attributes.onclick="popUpCalendar(this,document.getElementById('ctl0_CONTENU_PAGE_panelSearch_dateEnd'),'dd/mm/yyyy','');" />
						<com:TDataTypeValidator	ID="controlDateEnd" ValidationGroup="datesValidation" ControlToValidate="dateEnd" DataType="Date"	DateFormat="dd/M/yyyy" Text="<%=Prado::localize('MSG_ERROR_FORMAT_DATE_INVALIDE')%>" Display="Dynamic"/>
					</div>
					<div class="info-aide-right"><com:TTranslate>DEFINE_FORMAT_DATE_SIMPLE</com:TTranslate></div>
				</div>
				<!--Fin Ligne Recherche par date-->
            	<!--Debut Line bouton recherche multi-criteres-->
                	<div class="spacer"></div>
                	<div class="boutons-line">
                		<input type="reset"
                			class="bouton-long-190 float-left"
                			 value="<%=Prado::localize('DEFINE_EFFACER_CRITERE_RECHERCHE')%>"
                			title="<%=Prado::localize('DEFINE_EFFACER_CRITERE_RECHERCHE')%>" /> <com:TButton
                		ValidationGroup="sirenValidationGroup"
                		CssClass="bouton-moyen-120 float-right" Text="<%=Prado::localize('DEFINE_LANCER_RECHERCHE')%>"
                    					Attributes.title="<%=Prado::localize('DEFINE_LANCER_RECHERCHE')%>"
                    					OnClick="onSearchClick"/>
                				</div>
            	<!--Fin Line bouton recherche multi-criteres-->
            	<div class="breaker"></div></div>
            <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
            </div>
            <!--Fin Bloc Recherche avancee-->
