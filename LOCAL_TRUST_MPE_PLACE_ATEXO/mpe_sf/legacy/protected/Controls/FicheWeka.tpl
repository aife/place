<com:TPanel visible="<%=Application\Service\Atexo\Weka\Atexo_Weka_FicheWeka::recupererFicheWekaByPage( Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($_GET['page']))%>">
<div id="ficheConseil" class="expanded">
	<div class="fiche-header">
		<h2><com:TTranslate>DEFINE_FICHE_CONSEIL</com:TTranslate></h2>
		<a id="headerFicheConseil" href="javascript:" onclick="writeToCookies();" class="btn-toggle close" title="<%#Prado::localize('DEFINE_FERMER_FICHE_CONSEIL')%>"><com:TTranslate>FERMER</com:TTranslate></a>
	</div>
	<div class="fiche-content" id="contentFicheConseil">
		<div class="content">
			<com:TRepeater ID="listeFichesWeka" >
    			<prop:ItemTemplate>
					<div id="weka_<%#$this->Data->getIdFiche()%>" style="display:none;">
						<p><%#$this->SourceTemplateControl->getTitre($this->Data)%></p>
						<a href="javascript:popUpSetSize('<%#$this->Data->getUrl()%>','950px','600px','yes');" class="acces-fiche"><span><com:TTranslate>DEFINE_VOIR_FICHE</com:TTranslate></span></a>
					</div>
				</prop:ItemTemplate>
			</com:TRepeater>
		</div>
	</div>
</div>
</com:TPanel>
<script language="JavaScript" type="text/JavaScript">
addListnerPage(true);
</script>

<script type="text/JavaScript">
	//bootbox.alert(getCook('etatFicheWeka'));
    if(getCook('etatFicheWeka') == 'ferme') {
  	  	J("#ficheConseil").removeClass("expanded").addClass("collapsed");
  	  	if(document.getElementById("contentFicheConseil")) {
  	    	document.getElementById("contentFicheConseil").style.display="none";
  	  	}
    } else {
    	if(document.getElementById("contentFicheConseil")) {
    		document.getElementById("contentFicheConseil").style.display="";
    	}
    } 
</script>
<script type="text/JavaScript">
    function writeToCookies ()
    {
        if(getCook('etatFicheWeka') == 'ferme') {
            setCook('etatFicheWeka','ouvert');   
        } else {
            setCook('etatFicheWeka','ferme');   
        }   
    }
</script>