<?php

namespace Application\Controls;

use Application\Propel\Mpe\Entreprise;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Etablissement;

/**
 * Le bloc identite de l'entreprise.
 *
 * @author LEZ <loubna.ezziani@atexo.com>
 *
 * @version 1.0
 *
 * @since 4.7.0
 *
 * @copyright Atexo 2014
 */
class AtexoBlocIdentiteEntreprise extends MpeTTemplateControl
{
    public ?\Application\Propel\Mpe\Entreprise $_company = null;
    protected $siretMonEtablissement;

    /**
     * Set the value of [_company] .
     *
     * @param Entreprise $company new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function setCompany($company)
    {
        $this->_company = $company;
    }

    /**
     * Get the value of [_company] column.
     *
     * @return Entreprise
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function getCompany()
    {
        return $this->_company;
    }

    /**
     * mettre la Siret de mon etablissement ds le VS.
     *
     * @param int $siretEtablissement Siret de mon etablissement
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function setSiretMonEtablissement($siretMonEtablissement)
    {
        $this->siretMonEtablissement = $siretMonEtablissement;
    }

    /**
     * recupere la siret de mon etablissement depuis le VS.
     *
     * @return int $siretEtablissement Siret de mon etablissement
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getSiretMonEtablissement()
    {
        return $this->siretMonEtablissement;
    }

    /**
     * Permet de charger les informations d'etablissements.
     *
     * @param EntrepriseVo $entreprise l'entreprise
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function chargerInfoEtablissements($entreprise)
    {
        $etablissements = (new Atexo_Entreprise_Etablissement())->retreiveEtablissementsByIdEntreprise($entreprise->getId());
        $etablissementsVo = (new Atexo_Entreprise_Etablissement())->fillEtablissementsVoArray($etablissements);

        $this->listeEtablissments->setMode('1');
        $this->listeEtablissments->setSiretSiegeSocial($entreprise->getSiretSiegeSocial());
        $this->listeEtablissments->setIsEntrepriseLocale($entreprise->isEntrepriseLocale());
        $this->listeEtablissments->setEtablissementsList($etablissementsVo);
        $this->listeEtablissments->initComposant();
    }

    /**
     * Permet de charger les donnees de l'entreprise.
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function chargerInfoEntreprise()
    {
        $entreprise = $this->getCompany();
        if ($entreprise) {
            $this->infoEntreprise->setEntreprise($entreprise);
            $this->infoEntreprise->setIsEntrepriselocale($entreprise->isEntrepriseLocale());
            $this->infoEntreprise->chargerInfosEntreprise();
            $this->chargerInfoEtablissements($entreprise);
        }
    }
}
