
<com:TRepeater ID="repeaterLot" >
	<prop:ItemTemplate>
				<!--Debut Ligne -->
				<div class="line">
					<div class="intitule-auto indent-20">
						<com:TPanel cssClass="intitule-auto indent-20"><strong><com:TTranslate>DEFINE_LOT_NUMERO</com:TTranslate> <com:TLabel ID="numLot" Text="<%#$this->Data['lot']%>" />:</strong></com:TPanel>
					</div>
				</div>
				<!--Fin Ligne -->
				<div class="content-bloc bloc-400">
				 	<div class="line">
						<com:TActiveRadioButton 
							ID="OptionOui" 
							visible="<%#(!$this->SourceTemplateControl->getLabel())%>
							cssClass="radio" 
							GroupName="radioRef"
							checked="<%#($this->Data['defaultValue'] === '1' ) ? 'true' : 'false' %>"
							Text="<%=Prado::localize('TEXT_OUI')%>"
							Attributes.title="<%=Prado::localize('TEXT_OUI')%>"
						/>
		            </div>
		            <div class="line">
						<com:TActiveRadioButton 
							ID="OptionNon" 
							visible="<%#(!$this->SourceTemplateControl->getLabel())%>
							cssClass="radio" 
							GroupName="radioRef"
							Checked="<%#($this->Data['defaultValue'] === '0' ) ? 'true' : 'false' %>"
							Text="<%=Prado::localize('TEXT_NON')%>"
							Attributes.title="<%=Prado::localize('TEXT_NON')%>"
						/>
					</div>
				</div>
				<com:TTextBox style="display:none" ID="ClientIdsRadio" text="<%=$this->OptionOui->getClientId().'#'.$this->OptionNon->getClientId()%>"/>
				<com:TCustomValidator 
					ID="validatorOptionSelected"
					visible="<%#($this->Data['obligatoire']) ? 'true' :'false'%>"
					ControlToValidate="ClientIdsRadio"
					ValidationGroup="<%#($this->SourceTemplateControl->getValidationGroup())%>"
					ClientValidationFunction="validateOptionSelected"
					ErrorMessage="<%=$this->codeLibelle->Text%> ,<%=Prado::localize('DEFINE_EST_OBLIGATOIRE') %>"
					Text=" ">   
					<prop:ClientSide.OnValidationError>
						blockErreur = '<%#($this->SourceTemplateControl->getValidationSummary())%>';
						if(document.getElementById(blockErreur))
		 				document.getElementById(blockErreur).style.display='';
		 				if(document.getElementById('ctl0_CONTENU_PAGE_saveBack'))
						document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
						if(document.getElementById('ctl0_CONTENU_PAGE_save'))
						document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
						if(document.getElementById('ctl0_CONTENU_PAGE_buttonValidate'))
						document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
						if(document.getElementById('ctl0_CONTENU_PAGE_blocBoutons'))
						document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
					</prop:ClientSide.OnValidationError>
				</com:TCustomValidator>
				<com:TLabel id="iDLibelle" visible="false" text="<%#$this->Data['id']%>" />
	         	<com:TLabel id="codeLibelle" visible="false" text="<%#$this->Data['codeLibelle']%>" />
	         	<com:TLabel id="labelReferentielRadio"  text="" visible="<%# $this->SourceTemplateControl->getLabel()%>"/>
	         	<com:THiddenField id="oldValue" />
	</prop:ItemTemplate>
</com:TRepeater>