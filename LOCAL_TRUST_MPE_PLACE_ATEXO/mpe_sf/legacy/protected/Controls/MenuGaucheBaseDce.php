<?php

namespace Application\Controls;

class MenuGaucheBaseDce extends MpeTPage
{
    public function onLoad($param)
    {
    }

    public function quickSearchDCE()
    {
        $keyWord = $this->quickSearchDCE->Text;
        $this->response->redirect('index.php?page=Agent.SearchDCE&keyWord='.$keyWord);
    }
}
