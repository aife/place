<?php

namespace Application\Controls;

use Application\Propel\Mpe\CommonTDonneeComplementaire;
use Application\Propel\Mpe\CommonTPubliciteFavoris;

/**
 * Class DonneesComplementairesRegimeFinancier : classe de gestion du regime financier dans les donnees complementaires.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 *
 * @version 1.0
 *
 * @since 2016-les-echos
 *
 * @copyright Atexo 2016
 */
class DonneesComplementairesRegimeFinancier extends MpeTTemplateControl
{
    private $_donneeComplementaire;

    /**
     * @return mixed
     */
    public function getDonneeComplementaire()
    {
        return $this->_donneeComplementaire;
    }

    /**
     * @param mixed $donneeComplementaire
     */
    public function setDonneeComplementaire($donneeComplementaire)
    {
        $this->_donneeComplementaire = $donneeComplementaire;
    }

    /**
     * Permet d'initialiser le composant.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-les-echos
     *
     * @copyright Atexo 2016
     */
    public function initialiserComposant()
    {
        $this->chargerElements();
    }

    /**
     * Permet de charger les elements du composant.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-les-echos
     *
     * @copyright Atexo 2016
     */
    public function chargerElements($cautionnementGaranties = null, $modalitesFinancement = null)
    {
        if (null == $cautionnementGaranties && null == $modalitesFinancement) {
            $donneesComplementaires = $this->getDonneeComplementaire();
            if ($donneesComplementaires instanceof CommonTDonneeComplementaire) {
                $cautionnementGaranties = $donneesComplementaires->getCautionnementRegimeFinancier();
                $modalitesFinancement = $donneesComplementaires->getModalitesFinancementRegimeFinancier();
            }
        }
        if ($cautionnementGaranties || $modalitesFinancement) {
            $this->cautionnementGaranties->Text = $cautionnementGaranties;
            $this->modalitesFinancement->Text = $modalitesFinancement;
            $this->radioNonRegimeFinancier->Checked = false;
            $this->radioOuiRegimeFinancier->Checked = true;
            $this->blocRegimeFinancier->setDisplay('Dynamic');
        } else {
            $this->radioNonRegimeFinancier->Checked = true;
            $this->radioOuiRegimeFinancier->Checked = false;
            $this->blocRegimeFinancier->setDisplay('None');
        }
    }

    /**
     * Permet d'enregistrer les informations du composant dans la table "t_donnee_complementaire".
     *
     * @param $donneesComplementaires
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-les-echos
     *
     * @copyright Atexo 2016
     */
    public function saveRegimeFinancier(&$donneesComplementaires, &$favorisPub)
    {
        if ($this->radioOuiRegimeFinancier->Checked) {
            $cautionnementGaranties = $this->cautionnementGaranties->SafeText;
            $modalitesFinancement = $this->modalitesFinancement->SafeText;
        } else {
            $cautionnementGaranties = '';
            $modalitesFinancement = '';
        }
        if ($donneesComplementaires instanceof CommonTDonneeComplementaire) {
            $donneesComplementaires->setCautionnementRegimeFinancier($cautionnementGaranties);
            $donneesComplementaires->setModalitesFinancementRegimeFinancier($modalitesFinancement);
        }
        if ($favorisPub instanceof CommonTPubliciteFavoris) {
            $favorisPub->setRegimeFinancierCautionnement($cautionnementGaranties);
            $favorisPub->setRegimeFinancierModalitesFinancement($modalitesFinancement);
        }
    }
}
