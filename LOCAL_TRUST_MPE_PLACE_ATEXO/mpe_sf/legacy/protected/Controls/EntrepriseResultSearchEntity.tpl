<div class="main-part" id="main-part">

	<div class="clearfix">
		<ul class="breadcrumb">
			<li>
				<a href="/entreprise">
						<com:TTranslate>TEXT_ACCUEIL</com:TTranslate>
                    </a>
			</li>
			<li>
				<com:TTranslate>DEFINE_TEXT_VISUALISER_EA</com:TTranslate>
			</li>
		</ul>
	</div>

	<!--BEGIN BTN NOUVEL RECHERCHER / MODIFIER MA RECHERCHE TOP-->
	<div class="link-line">
		<ul class="list-unstyled clearfix m-t-2">
			<li class="pull-left">
				<com:TLinkButton cssClass="btn btn-sm btn-default"
								 Text ="<%=Prado::localize('TEXT_MODIFIER_MA_RECHERCHE')%>"
								 Attributes.title="<%=Prado::localize('TEXT_MODIFIER_MA_RECHERCHE')%>"
								 onClick ="Page.remplissageChamps"
				/>
			</li>
			<li class="pull-right">
				<com:TLinkButton cssClass="btn btn-sm btn-default"
								 Text ="<%=Prado::localize('TEXT_NOUVELLE_RECHERCHE')%>"
								 Attributes.title="<%=Prado::localize('TEXT_NOUVELLE_RECHERCHE')%>"
								 onClick ="Page.retour"
				/>
			</li>
		</ul>
	</div>
	<!--END BTN NOUVEL RECHERCHER / MODIFIER MA RECHERCHE TOP-->

	<com:TPanel ID="panelMessageResultat" Visible="false">
		<com:PanelMessageErreur ID="PanelMessage"/>
	</com:TPanel>

	<!--BEGIN RESULT SEARCH -->
	<com:TPanel ID="panelResultat" Visible="true">
		<div class="form-field">
			<div class="content">
				<div class="table table-striped list-group m-t-2">
					<com:TRepeater
							ID="repeaterResultatEntite"
							EnableViewState="true"
							DataKeyField="Id"
					>
						<prop:HeaderTemplate>
							<div class="list-group-item active small">
								<div class="clearfix">
									<ul class="list-unstyled list-inline m-b-0">
										<li id="entite" class="col-md-8">
											<com:TTranslate>DEFINE_TEXT_ENTITE_PUBLIC</com:TTranslate> / <com:TTranslate>TEXT_LIBELLE_ENTITE_ACHAT</com:TTranslate>
										</li>
										<li id="ville" class="col-md-3">
											<com:TTranslate>TEXT_VILLE</com:TTranslate> / <com:TTranslate>TEXT_PAYS</com:TTranslate>
										</li>
										<li id="detail" class="col-md-1 text-right">
											<com:TTranslate>TEXT_DETAIL</com:TTranslate>
										</li>
									</ul>
								</div>
							</div>
						</prop:HeaderTemplate>
						<prop:ItemTemplate>
							<div class="item_consultation list-group-item kt-callout left kt-vertical-align <%#($this->ItemIndex%2)?'':'on'%>">
								<div class="col-md-8 p-0">
									<div>
										<abbr data-toggle="tooltip"
											  data-placement="top"
											  title="<%#$this->Data['denominationorg']%>">
											<strong><com:TLabel text="<%#$this->Data['sigleorg']%>"></com:TLabel></strong>
										</abbr>
										<span class="small m-l-1">
												<%#$this->Data['denominationorg']%>
											</span>
									</div>
									<div>
										<abbr data-toggle="tooltip"
											  data-placement="top"
											  title="<%#$this->Data['libelle']%>">
											<strong><com:TLabel text="<%#$this->Data['sigle']%>"></com:TLabel></strong>
										</abbr>
										<span class="small m-l-1">
												<%#$this->Data['libelle']%>
											</span>
									</div>
								</div>

								<div class="col-md-3 p-0">
										<span>
											<i class="fa fa-map-marker text-danger m-r-1"></i>
											<span class="small"><%#$this->Data['ville']%>, </span>
											<span class="small"><%#$this->Data['pays']%></span>
										</span>
								</div>

								<div class="col-md-1 text-right p-0">
									<com:TLinkButton
											OnCommand="Page.EntrepriseResultSearchEntity.onDetailClick"
											CommandParameter="<%#$this->Data['id']%>"
											cssClass="btn btn-sm btn-primary"
									>
										<i class="fa fa-search-plus"></i>
									</com:TLinkButton>
								</div>
							</div>
						</prop:ItemTemplate>
					</com:TRepeater>
				</div>
			</div>
		</div>
	</com:TPanel>
	<!--END RESULT SEARCH-->

	<!--BEGIN BTN NOUVEL RECHERCHER / MODIFIER MA RECHERCHE BOTTOM-->
	<div class="link-line">
		<ul class="list-unstyled m-t-2">
			<li class="pull-left">
				<com:TLinkButton cssClass="btn btn-sm btn-default"
								 Text ="<%=Prado::localize('TEXT_MODIFIER_MA_RECHERCHE')%>"
								 Attributes.title="<%=Prado::localize('TEXT_MODIFIER_MA_RECHERCHE')%>"
								 onClick ="Page.remplissageChamps"
				/>
			</li>
			<li class="pull-right">
				<com:TLinkButton cssClass="btn btn-sm btn-default"
								 Text ="<%=Prado::localize('TEXT_NOUVELLE_RECHERCHE')%>"
								 Attributes.title="<%=Prado::localize('TEXT_NOUVELLE_RECHERCHE')%>"
								 onClick ="Page.retour"
				/>
			</li>
		</ul>
	</div>
	<!--END BTN NOUVEL RECHERCHER / MODIFIER MA RECHERCHE BOTTOM-->
</div>