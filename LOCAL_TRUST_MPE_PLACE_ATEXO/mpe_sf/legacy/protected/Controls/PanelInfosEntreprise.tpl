<com:TPanel cssclass="form-field" id="blocSaisieManuelInfosEntreprise">
    <div class="section">
        <div class="section-heading page-header m-t-0">
            <h3 class="h4 m-0">
                <com:TTranslate>TEXT_ENTREPRISE</com:TTranslate>
            </h3>
        </div>
        <div class="section-body">
            <div class="content">
                <com:TPanel cssClass="form-bloc margin-0" Visible="<%=$this->getIsEntrepriselocale()%>">
                    <div class="alert alert-info" role="alert">
                        <span class="fa fa-info-circle m-r-1" aria-hidden="true"></span>
                        <com:TTranslate>MESSAGE_INFO_MODIFICATION_SIRET</com:TTranslate>
                    </div>
                </com:TPanel>
                <div class="form-group form-group-sm clearfix">
                    <label class="control-label col-md-3" for="raisonSocial">
                        <com:TTranslate>TEXT_RAISON_SOCIAL</com:TTranslate>
                        :
                        <span class="text-danger">* </span>
                    </label>
                    <div class="col-md-9">
                        <com:TTextBox id="raisonSociale" cssClass="form-control"/>
                    </div>
                    <com:TRequiredFieldValidator
                            ControlToValidate="raisonSociale"
                            ValidationGroup="validateCreateCompte"
                            Display="Dynamic"
                            ErrorMessage="<%=Prado::localize('TEXT_RAISON_SOCIAL').' '.Prado::localize('TEXT_DU_SIEGE_SOCIAL')%>"
                            EnableClientScript="true"
                            Text="<span title='Champ obligatoire' class='check-invalide'></span>">
                        <prop:ClientSide.OnValidationError>
                            document.getElementById('divValidationSummary').style.display='';
                            var btn = J('#ctl0_CONTENU_PAGE_buttonEnregistrer');
                                if(btn) { 
                                    J('#ctl0_CONTENU_PAGE_buttonEnregistrer').show();
                                }
                        </prop:ClientSide.OnValidationError>
                    </com:TRequiredFieldValidator>
                </div>
                <com:TPanel ID="panelCodeApe">
                    <div class="form-group form-group-sm clearfix">
                        <label class="control-label col-md-3">
                            Code APE / NAF / NACE
                            <span class="text-danger" style="<%=($this->getIsEntrepriselocale())?'':'display:none'%>">* </span> :
                        </label>
                        <div class="col-md-9">
                            <div class="row">
                                <div class="col-md-4 p-r-0" data-toggle="tooltip" Attributes.title="<%=Prado::localize('TEXT_NAF')%>">
                                    <com:TTextBox MaxLength="4" id="apeNafNace1" cssClass="form-control"/>
                                </div>
                                <div class="col-md-4 p-l-0" data-toggle="tooltip" Attributes.title="<%=Prado::localize('TEXT_NAF')%>">
                                    <com:TTextBox MaxLength="1" id="apeNafNace2" cssClass="form-control"/>
                                </div>
                                <a href="javascript:popUpSetSize('?page=Entreprise.EntreprisePopupCodeNaf','1000px', '800x;');" class="infos-plus col-md-4 p-t-1">
                                    <%=Prado::localize('TEXT_SAVOIR_PLUS')%>
                                </a>
                            </div>
                        </div>
                        <com:TRequiredFieldValidator
                                Id="validatorApe"
                                ControlToValidate="apeNafNace1"
                                ValidationGroup="validateCreateCompte"
                                Display="Dynamic"
                                ErrorMessage="<%=Prado::localize('TEXT_NAF')%>"
                                EnableClientScript="true"
                                visible=<%=($this->getIsEntrepriselocale())?true:false%>
                            Text="<span title='Champ obligatoire' class='check-invalide'></span>">
                            <prop:ClientSide.OnValidationError>
                                document.getElementById('divValidationSummary').style.display='';
                                var btn = J('#ctl0_CONTENU_PAGE_buttonEnregistrer');
                                if(btn) { 
                                    J('#ctl0_CONTENU_PAGE_buttonEnregistrer').show();
                                }
                            </prop:ClientSide.OnValidationError>
                        </com:TRequiredFieldValidator>
                    </div>
                </com:TPanel>
                <com:TPanel cssclass="form-group form-group-sm clearfix" ID="blocSirenModeSaisieManuelle">
                    <label class="col-md-3 control-label">
                        <com:TLabel Attributes.for="siren" ID="labelSiren">
                            <com:TTranslate>TEXT_SIREN</com:TTranslate>
                        </com:TLabel>
                        <span class="text-danger">* </span> :
                    </label>
                    <div class="col-md-9">
                        <com:TTextBox id="siren" MaxLength="9" Enabled="false" cssClass="form-control"/>
                    </div>
                    <com:TRequiredFieldValidator
                            ID="validateurSirenModeSaisieManuelle"
                            ControlToValidate="siren"
                            ValidationGroup="validateCreateCompte"
                            Display="Dynamic"
                            ErrorMessage="<%=Prado::localize('TEXT_SIREN')%>"
                            EnableClientScript="true"
                            Text="<span title='Champ obligatoire' class='check-invalide'></span>">
                        <prop:ClientSide.OnValidationError>
                            document.getElementById('divValidationSummary').style.display='';
                            var btn = J('#ctl0_CONTENU_PAGE_buttonEnregistrer');
                                if(btn) { 
                                    J('#ctl0_CONTENU_PAGE_buttonEnregistrer').show();
                                }
                        </prop:ClientSide.OnValidationError>
                    </com:TRequiredFieldValidator>
                </com:TPanel>
                <com:TPanel cssclass="form-group form-group-sm clearfix" ID="blocIdentifiantUniqueModeSaisieManuelle">
                    <label class="col-md-3 control-label">
                        <com:TLabel Attributes.for="identifiantUnique" ID="labelIdentifiantUnique">
                            <com:TTranslate>TEXT_IDENTIFIANT_NATIONNAL</com:TTranslate>
                        </com:TLabel>
                        <span class="text-danger">* </span> :
                    </label>
                    <div class="col-md-9">
                        <com:TTextBox id="identifiantUnique" MaxLength="<%=Application\Service\Atexo\Atexo_Config::getParameter('NOMBRE_MAX_CARACTERES_ID_ENTREPRISE_ETRANGERE')%>" cssClass="form-control"/>
                    </div>
                    <com:TRequiredFieldValidator
                            ID="validateurIdentifiantUniqueModeSaisieManuelle"
                            ControlToValidate="identifiantUnique"
                            ValidationGroup="validateCreateCompte"
                            Display="Dynamic"
                            ErrorMessage="<%=Prado::localize('TEXT_IDENTIFIANT_NATIONNAL')%>"
                            EnableClientScript="true"
                            Text="<span title='Champ obligatoire' class='check-invalide'></span>">
                        <prop:ClientSide.OnValidationError>
                            document.getElementById('divValidationSummary').style.display='';
                            var btn = J('#ctl0_CONTENU_PAGE_buttonEnregistrer');
                                if(btn) { 
                                    J('#ctl0_CONTENU_PAGE_buttonEnregistrer').show();
                                }
                        </prop:ClientSide.OnValidationError>
                    </com:TRequiredFieldValidator>
                </com:TPanel>
                <div class="form-group form-group-sm clearfix">
                    <label for="formeJuridique" class="col-md-3 control-label">
                        <com:TTranslate>TEXT_FJ</com:TTranslate>
                        <span class="text-danger">* </span> :
                    </label>
                    <div class="col-md-9">
                        <com:TDropDownList ID="formeJuridique" cssClass="form-control"></com:TDropDownList>
                    </div>
                    <com:TCompareValidator
                            ControlToValidate="formeJuridique"
                            ValueToCompare="0"
                            DataType="String"
                            Operator="NotEqual"
                            ValidationGroup="validateCreateCompte"
                            Display="Dynamic"
                            ErrorMessage="<%=Prado::localize('TEXT_FJ')%>"
                            EnableClientScript="true"
                            Text="<span title='Champ obligatoire' class='check-invalide'></span>">
                        <prop:ClientSide.OnValidationError>
                            document.getElementById('divValidationSummary').style.display='';
                            var btn = J('#ctl0_CONTENU_PAGE_buttonEnregistrer');
                                if(btn) { 
                                    J('#ctl0_CONTENU_PAGE_buttonEnregistrer').show();
                                }
                        </prop:ClientSide.OnValidationError>
                    </com:TCompareValidator>
                </div>
                <div class="form-group form-group-sm clearfix">
                    <label class="col-md-3 control-label">
                        <com:TTranslate>TEXT_LIEU_ETABLISSEMENT</com:TTranslate>
                    </label>
                    <div class="col-md-9 p-t-1">
                        <com:TLabel ID="lieuEtablissement"/>
                    </div>
                </div>
                <div class="form-group form-group-sm clearfix">
                    <label class="col-md-3 control-label">
                        <com:TTranslate>DEFINE_CATEGORIE_ENTREPRISE_PME</com:TTranslate>
                        :
                    </label>
                    <div class="col-md-9">
                        <com:TDropDownList ID="categorieEntrepriseListe" cssClass="form-control"></com:TDropDownList>
                    </div>
                </div>
            </div>
        </div>
    </div>
</com:TPanel>

<com:TPanel cssclass="form-field clearfix" id="blocInfosEntrepriseChargementAuto">
    <div class="section">
        <div class="section-heading page-header m-t-0">
            <h2 class="h4 m-0">
                <com:TTranslate>TEXT_ENTREPRISE</com:TTranslate>
            </h2>
        </div>
        <div class="section-body">
            <com:TPanel cssClass="form-bloc margin-0" Visible="<%=$this->getIsEntrepriselocale()%>">
                <div class="alert alert-info" role="alert">
                    <span class="fa fa-info-circle m-r-1" aria-hidden="true"></span>
                    <com:TTranslate>MESSAGE_INFO_MODIFICATION_SIRET</com:TTranslate>
                </div>
            </com:TPanel>
            <div class="col-md-6">
                <div class="form-group form-group-sm clearfix">
                    <label class="col-md-5">
                        <com:TTranslate>TEXT_RAISON_SOCIAL</com:TTranslate>
                        :
                    </label>
                    <div class="col-md-7">
                        <com:TLabel ID="texteRaisonSociale"/>
                    </div>
                </div>
                <com:TPanel cssClass="form-group form-group-sm clearfix" Visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseCodeApe'))? true:false %>">
                    <label class="col-md-5">
                        <com:TTranslate>TEXT_NAF</com:TTranslate>
                        :
                    </label>
                    <div class="col-md-7">
                        <com:TLabel ID="code_APE_NACE"/>
                        <com:TLabel ID="libelleApeNafNace"/>
                    </div>
                </com:TPanel>
                <com:TPanel cssClass="form-group form-group-sm clearfix" ID="panelIdUniqueModeNormal">
                    <label class="col-md-5">
                        <com:TTranslate>TEXT_IDENTIFIANT_NATIONNAL</com:TTranslate>
                    </label>
                    <div class="col-md-7">
                        <com:TLabel ID="IdUnique_chargement_auto"/>
                    </div>
                </com:TPanel>
                <div class="form-group form-group-sm clearfix">
                    <label class="col-md-5">
                        <com:TTranslate>TEXT_FJ</com:TTranslate> :
                    </label>
                    <div class="col-md-7">
                        <com:TLabel ID="texteFormeJuridique"/>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <com:TPanel cssClass="form-group form-group-sm clearfix" ID="panelSirenModeNormal">
                    <label class="col-md-5">
                        <com:TTranslate>TEXT_SIREN</com:TTranslate>
                        :
                    </label>
                    <div class="col-md-7">
                        <com:TLabel ID="siren_chargement_auto"/>
                    </div>
                </com:TPanel>
                <div class="form-group form-group-sm clearfix">
                    <label class="col-md-5">
                        <com:TTranslate>TEXT_LIEU_ETABLISSEMENT</com:TTranslate>
                    </label>
                    <div class="col-md-7">
                        <com:TLabel ID="lieuEtablissement_chargement_auto"/>
                    </div>
                </div>
                <div class="form-group form-group-sm clearfix">
                    <label class="col-md-5">
                        <com:TTranslate>DEFINE_CATEGORIE_ENTREPRISE_PME</com:TTranslate>
                        :
                    </label>
                    <div class="col-md-7">
                        <com:TLabel ID="categorieEntreprise"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
</com:TPanel>