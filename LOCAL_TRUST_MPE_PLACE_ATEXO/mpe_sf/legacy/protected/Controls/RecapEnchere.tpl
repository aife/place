<div class="form-bloc text-left" id="recap-consultation">
	<div class="recap-bloc clearfix">
		<blockquote class="blockquote small clearfix">
			<div class="">
				<strong class="col-md-2 text-right"><com:TTranslate>TEXT_REFERENCE</com:TTranslate> :</strong>
				<div class="col-md-10"><com:TLabel ID="reference" Text="<%=$this->getReference()%>" /></div>
			</div>
			<div class="">
				<strong class="col-md-2 text-right"><com:TTranslate>OBJET</com:TTranslate> :</strong>
				<div class="col-md-10"><com:TLabel ID="objet" Text="<%=$this->getObjet()%>" /></div>
			</div>
			<div class="">
				<strong class="col-md-2 text-right"><com:TTranslate>DEFINE_ENTITE_ACHAT</com:TTranslate> :</strong>
				<div class="col-md-10"><com:TLabel ID="entiteAchat" Text="<%=$this->getEntiteAchat()%>" /></div>
			</div>
		</blockquote>
	</div>
</div>