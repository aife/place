<?php

namespace Application\Controls;

/**
 * resultat de contenu de la reponse entreprise.
 *
 * @author Mounir LAMANE <mounir.lamane@atexo.com>
 * @copyright Atexo 2010
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class ConfirmationReponse extends MpeTTemplateControl
{
    private $_dataSource;
    private bool $_messageConfirmation = true;
    private $_dateDepotOffre;

    public function onLoad($param)
    {
        if ($this->getDataSource()) {
            $this->repeaterConfirmationDepotPli->dataSource = $this->getDataSource();
            $this->repeaterConfirmationDepotPli->dataBind();
        }
    }

    public function setDataSource($dataSource)
    {
        $this->_dataSource = $dataSource;
    }

    public function getDataSource()
    {
        return $this->_dataSource;
    }

    public function setMessageConfirmation($messageConfirmation)
    {
        $this->_messageConfirmation = $messageConfirmation;
    }

    public function getMessageConfirmation()
    {
        return $this->_messageConfirmation;
    }

    public function setDateDepotOffre($dateDepot)
    {
        $this->_dateDepotOffre = $dateDepot;
    }

    public function getDateDepotOffre()
    {
        return $this->_dateDepotOffre;
    }
}
