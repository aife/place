<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2;
use Prado\Prado;
use Prado\Web\UI\WebControls\TDropDownList;

/**
 * Classe du composant pays.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class DropDownListCountries extends TDropDownList
{
    private $_typeAffichage;
    private bool $_withaoutFrance = true;
    public bool $_postBack = false;

    public function onLoad($param)
    {
        if (!$this->getPostBack()) {
            $this->fillDataSource();
        }
    }

    public function fillDataSource()
    {
        $this->setDataSource($this->displayCountries($this->_typeAffichage));
        $this->DataBind();
    }

    public function setPostBack($value)
    {
        $this->_postBack = $value;
    }

    public function getPostBack()
    {
        return $this->_postBack;
    }

    public function getTypeAffichage()
    {
        return $this->_typeAffichage;
    }

    public function setWithaoutFrance($withaoutFrance)
    {
        $this->_withaoutFrance = $withaoutFrance;
    }

    public function setTypeAffichage($_typeAffichage)
    {
        $this->_typeAffichage = $_typeAffichage;
    }

    public function displayCountries($typeAffichage)
    {
        $listCountries = (new Atexo_Geolocalisation_GeolocalisationN2())->retrieveListCountries($this->_withaoutFrance);
        $dataSource = [];
        $dataSource['0'] = Prado::localize('TEXT_SELECTIONNER');
        if ($listCountries) {
            foreach ($listCountries as $oneCountrie) {
                if ('withCode' == $typeAffichage) {
                    $dataSource[$oneCountrie->getId()] = $oneCountrie->getDenomination1().
                                                         ' - ('.$oneCountrie->getDenomination2().')';
                } elseif ('withoutCode' == $typeAffichage) {
                    $dataSource[$oneCountrie->getId()] = $oneCountrie->getDenomination1();
                } elseif ('justCode' == $typeAffichage) {
                    $dataSource[$oneCountrie->getId()] = $oneCountrie->getDenomination2();
                }
            }

            return $dataSource;
        } else {
            return false;
        }
    }

    /**
     * Initialise la valeur à null uniquement
     * dans le cas ou la FRANCE est selectionné
     * et n'est pas presente dans la liste.
     *
     * @param string the value of the item to be selected
     *
     * @return void
     *
     * @author Gregory CHEVRET <gregory.chevret@atexom.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function setSelectedValue($value)
    {
        if ($this->_withaoutFrance && Atexo_Config::getParameter('ID_GEON2_FRANCE') == $value) {
            $value = null;
        }
        parent::setSelectedValue($value);
    }
}
