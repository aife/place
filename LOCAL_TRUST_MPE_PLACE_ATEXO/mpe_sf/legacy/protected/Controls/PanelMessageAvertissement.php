<?php

namespace Application\Controls;

/**
 * Panel des messages d'avertissement.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2009
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PanelMessageAvertissement extends MpeTTemplateControl
{
    public string $cssClass = 'message bloc-700';

    public function getCssClass()
    {
        return $this->cssClass;
    }

    public function setCssClass($value)
    {
        $this->cssClass = $value;
    }

    public function setMessage($message)
    {
        $this->labelMessage->Text = $message;
    }
}
