<?php

namespace Application\Controls;

use Application\Propel\Mpe\CommonTBourseCotraitance;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_Util;
use PDO;

/**
 * Created by PhpStorm.
 * User: AsoAtexo
 * Date: 11/08/2015
 * Time: 14:48.
 */
class AtexoResultCotraitant extends MpeTTemplateControl
{
    protected $currentPage = 1;
    protected $pageSize = 10;
    protected $totalPages;
    protected $criteresSearch;

    public function getlisteCotraitant($criteres, $count = false)
    {
        $reqClause = null;
        $reqEA = null;
        $reqLocalite = null;
        $listeBourseCotraitance = [];
        $reqEtab = $fromEtab = $groupBy = '';
        $countStart = $countEnd = '';
        if ($count) {
            $countStart = 'SELECT count(*) As count FROM (';
            $countEnd = ') a';
            $req = 'SELECT count(BC.id_auto_BC) As count';
        } else {
            $req = 'SELECT BC.*,E.nom as raisonSocial, Etb.id_etablissement As idEtablissementSiege';
        }
        $reqEtab = ' AND Etb.id_entreprise = E.id ';
        // AND Etb.est_siege = '1' ";
        $fromEtab = ',t_etablissement AS Etb';
        $groupBy = ' GROUP BY BC.id_entreprise ';

        $reqGroupement = '';
        $typeGroupement = [];
        if (!empty($criteres['mandataire'])) {
            $typeGroupement[] = " BC.mandataire_groupement = '".$criteres['mandataire']."'";
        }
        if (!empty($criteres['solidaire'])) {
            $typeGroupement[] = " BC.cotraitant_solidaire = '".$criteres['solidaire']."'";
        }
        if (!empty($criteres['conjoint'])) {
            $typeGroupement[] = " BC.cotraitant_conjoint = '".$criteres['conjoint']."'";
        }
        if (!empty($criteres['sousTraitant'])) {
            $typeGroupement[] = " BC.sous_traitant = '".$criteres['sousTraitant']."'";
        }
        if (is_array($typeGroupement) && (is_countable($typeGroupement) ? count($typeGroupement) : 0)) {
            $reqGroupement = ' AND ('.implode(' OR ', $typeGroupement).' ) ';
        }
        if ('2' != $criteres['clauseSocial'] && '' != $criteres['clauseSocial']) {
            $reqClause = "AND BC.clause_social = '{$criteres['clauseSocial']}'";
        }
        if ('2' != $criteres['eseAdaptee'] && '' != $criteres['eseAdaptee']) {
            $reqEA = "AND BC.entreprise_adapte = '{$criteres['eseAdaptee']}'";
        }
        if ('' != $criteres['localite']) {
            $reqLocalite = " AND ( BC.ville_inscrit LIKE '%".$criteres['localite']."%'
                                OR BC.cp_inscrit  LIKE '%".$criteres['localite']."%' ) ";
        }
        $reqKeyWord = '';

        if (is_array($criteres['keyWord']) && count($criteres['keyWord'])) {
            $reqKeyWord .= 'AND ( ';
            $reqKeyWord .= " E.description_activite LIKE '%".implode('%\' OR E.description_activite LIKE \'%', $criteres['keyWord'])."%'";
            $reqKeyWord .= " OR E.activite_domaine_defense LIKE '%".implode('%\' OR E.activite_domaine_defense LIKE \'%', $criteres['keyWord'])."%'";
            $reqKeyWord .= " OR E.nom LIKE '%".implode('%\' OR E.nom LIKE \'%', $criteres['keyWord'])."%'";
            $reqKeyWord .= ')';
        } elseif ('' != $criteres['keyWord']) {
            $reqKeyWord = "AND ( E.description_activite LIKE '%{$criteres['keyWord']}%'
            OR E.activite_domaine_defense LIKE '%{$criteres['keyWord']}%'
            OR E.nom  LIKE '%{$criteres['keyWord']}%' )";
        }
        $sql = <<<SQL
        $countStart
        $req
        FROM Entreprise AS E, t_bourse_cotraitance AS BC $fromEtab
        WHERE BC.id_entreprise = E.id
        $reqEtab
        AND BC.reference_consultation = {$criteres['refCons']}
        $reqGroupement
        $reqClause
        $reqEA
        $reqLocalite
        $reqKeyWord
        $groupBy
        LIMIT  {$criteres['offset']}, {$criteres['limit']}
        $countEnd
        ;
SQL;
        //echo $sql;//exit;
        $statement = Atexo_Db::getLinkCommon(true)->prepare($sql);
        $results = $statement->execute();
        if ($results) {
            if ($count) {
                while ($data = $statement->fetch(PDO::FETCH_ASSOC)) {
                    return $data['count'];
                }
            } else {
                while ($data = $statement->fetch(PDO::FETCH_NUM)) {
                    $bourseCotraitance = new CommonTBourseCotraitance();
                    $bourseCotraitance->hydrate2($data);
                    $listeBourseCotraitance[] = $bourseCotraitance;
                }
            }
        }

        return $listeBourseCotraitance;
    }

    public function displayCotraitant($count = false)
    {
        $arrayCotraitants = self::getlisteCotraitant($this->criteresSearch, $count);

        $this->totalPages = ceil($this->resultCount / $this->pageSize);
        $this->nombreElement->Text = $this->resultCount;

        $this->numPageTop->Text = $this->currentPage;
        $this->numPageBottom->Text = $this->currentPage;

        if ($this->resultCount) {
            $this->labelScript->Text = '<script type="text/JavaScript">initMap();</script>';
            $this->labelScript2->Text = '<script type="text/JavaScript">addToggle();</script>';

            $this->nombrePageTop->Text = $this->totalPages;
            $this->nombrePageBottom->Text = $this->totalPages;

            $this->repeaterCotraitants->PageSize = $this->pageSize;
            $this->repeaterCotraitants->CurrentPageIndex = $this->currentPage - 1;
            $this->repeaterCotraitants->VirtualItemCount = $this->resultCount;

            $this->panelMessage->style = 'display:none;';
            //self::showComponent();
            $this->repeaterCotraitants->DataSource = $arrayCotraitants;
            $this->repeaterCotraitants->DataBind();
        }
        $this->_setPagerVariables();
    }

    public function goToPage($sender, $param)
    {
        $numPage = null;
        $this->_getPagerVariables();
        switch ($sender->ID) {
            case 'DefaultButtonTop':
                $numPage = $this->numPageTop->Text;
                break;
            case 'DefaultButtonBottom':
                $numPage = $this->numPageBottom->Text;
                break;
        }
        if (Atexo_Util::isEntier($numPage)) {
            if ($numPage >= $this->totalPages) {
                $numPage = $this->totalPages;
            } elseif ($numPage <= 0) {
                $numPage = 1;
            }
            //self::updatePagination($this->nombreResultatAfficherTop->getSelectedValue(),$numPage);
            $this->criteresSearch['offset'] = ($numPage - 1) * $this->pageSize;
            $this->criteresSearch['limit'] = $this->pageSize;
            $this->currentPage = $numPage;
        }
        $this->displayCotraitant();
    }

    public function changePageSize($sender, $param)
    {
        $this->_getPagerVariables();
        switch ($sender->ID) {
            case 'nombreResultatAfficherBottom':
                $this->pageSize = $this->nombreResultatAfficherBottom->getSelectedValue();
                                                    $this->nombreResultatAfficherTop->setSelectedValue($this->pageSize);
                break;
            case 'nombreResultatAfficherTop':
                $this->pageSize = $this->nombreResultatAfficherTop->getSelectedValue();
                                                    $this->nombreResultatAfficherBottom->setSelectedValue($this->pageSize);
                break;
        }
        $this->criteresSearch['offset'] = 0;
        $this->criteresSearch['limit'] = $this->pageSize;
        $this->currentPage = 1;
        $this->displayCotraitant();
    }

    /**
     * navigation sur les pages de repeater.
     *
     * @param unknown_type $sender
     * @param unknown_type $param
     */
    protected function pagerGoToPage($sender, $param)
    {
        $this->_getPagerVariables();
        $offSet = 0;
        // next page click
        if ($param->getCommandSource()->Text == $sender->getNextPageText()) {
            ++$this->currentPage;
            $offSet = ($this->currentPage - 1) * $this->pageSize;
        }
        // previews page click
        if ($param->getCommandSource()->Text == $sender->getPrevPageText()) {
            --$this->currentPage;
            $offSet = ($this->currentPage - 1) * $this->pageSize;
        }
        // first page click
        if (($param->getCommandSource()->Text == $sender->getFirstPageText())) {
            $this->currentPage = 1;
            $offSet = ($this->currentPage - 1) * $this->pageSize;
        }
        // last page click
        if ($param->getCommandSource()->Text == $sender->getLastPageText()) {
            $this->currentPage = $this->totalPages;
            $offSet = ($this->currentPage - 1) * $this->pageSize;
        }

        $this->criteresSearch['offset'] = $offSet;
        $this->criteresSearch['limit'] = $this->pageSize;
        $this->repeaterCotraitants->setCurrentPageIndex($this->currentPage - 1);
        self::displayCotraitant();
    }

    public function searchWithFiltre($sender, $param)
    {
        $criteresFiltre = self::getCriteresFiltre();
        $this->setCriteresSearch($criteresFiltre);
        $countResult = self::getlisteCotraitant($criteresFiltre, true);
        $this->setResultCount($countResult);
        if ($countResult) {
            self::displayCotraitant();
            self::showComponent($sender, $param);
        } else {
            self::hideComponent($sender, $param);
        }
        $this->repeaterCotraitants->render($param->getNewWriter());
        $this->panelMap->render($param->getNewWriter());
    }

    public function hideComponent($sender, $param)
    {
        $this->pagerTop->style = 'display:none;';
        $this->pagerButtom->style = 'display:none;';
        $this->panelMap->style = 'display:none;';
        $this->repeaterCotraitants->DataSource = [];
        $this->repeaterCotraitants->DataBind();
        $this->panelMessage->style = 'display:block;';
        $this->pagerTop->render($param->getNewWriter());
        $this->pagerButtom->render($param->getNewWriter());
    }

    public function showComponent($sender, $param)
    {
        $this->pagerTop->style = 'display:block;';
        $this->pagerButtom->style = 'display:block;';
        $this->panelMap->style = 'display:block;';
        $this->panelMessage->style = 'display:none;';
        $this->pagerTop->render($param->getNewWriter());
        $this->pagerButtom->render($param->getNewWriter());
    }

    public function getCriteresFiltre()
    {
        $criteres = [
            \REFCONS => $_GET['id'],
            \MANDATAIRE => '',
            \SOLIDAIRE => '',
            \CONJOINT => '',
            \SOUSTRAITANT => '',
            \CLAUSESOCIAL => '',
            \ESEADAPTEE => '',
            \LOCALITE => '',
            \LIMIT => '10',
            \OFFSET => '0',
        ];
        if ('' != $this->filtreMotsCles->Text) {
            $criteres['keyWord'] = explode(',', $this->filtreMotsCles->Text);
        }
        if (true == $this->mandataire->checked) {
            $criteres['mandataire'] = '1';
        }
        if (true == $this->solidaire->checked) {
            $criteres['solidaire'] = '1';
        }
        if (true == $this->conjoint->checked) {
            $criteres['conjoint'] = '1';
        }
        if (true == $this->sousTraitant->checked) {
            $criteres['sousTraitant'] = '1';
        }
        if ('' != $this->localite->Text) {
            $criteres['localite'] = $this->localite->Text;
        }
        //maintenir les criteres initial depuis le moteur de recherche
        $critereFromSearch = $this->page->getViewState('criteresRecherche');
        if (is_array($critereFromSearch)) {
            $criteres['clauseSocial'] = $critereFromSearch['clauseSocial'];
            $criteres['eseAdaptee'] = $critereFromSearch['eseAdaptee'];
        }

        return $criteres;
    }

    /**
     * gere l'affichage du bloc description type cotraitance recherche.
     *
     * @param $BourseObjt CommonTBourseCotraitance
     *
     * @return bool
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function ShowDescTypeCotraitanceRecherche($BourseObjt)
    {
        if ($BourseObjt instanceof CommonTBourseCotraitance && $BourseObjt->getDescTypeCotraitanceRecherche()) {
            return true;
        }

        return false;
    }

    /**
     * gere l'affichage du bloc description appart marche.
     *
     * @param $BourseObjt CommonTBourseCotraitance
     *
     * @return bool
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function ShowDescMonApportMarche($BourseObjt)
    {
        if ($BourseObjt instanceof CommonTBourseCotraitance && $BourseObjt->getDescMonApportMarche()) {
            return true;
        }

        return false;
    }

    /**
     * Recuperer les valeurs de pagination du viewState.
     */
    private function _getPagerVariables()
    {
        $this->currentPage = $this->Page->getViewState('currentPage');
        $this->pageSize = $this->Page->getViewState('pageSize');
        $this->totalPages = $this->Page->getViewState('totalPages');
        $this->resultCount = $this->Page->getViewState('resultCount');
        $this->criteresSearch = $this->Page->getViewState('CriteresSearch');
    }

    /**
     * Enregistrer les valeurs de pagination dans le viewState.
     */
    private function _setPagerVariables()
    {
        $this->Page->setViewState('currentPage', $this->currentPage);
        $this->Page->setViewState('pageSize', $this->pageSize);
        $this->Page->setViewState('totalPages', $this->totalPages);
        $this->Page->setViewState('resultCount', $this->resultCount);
        $this->Page->setViewState('CriteresSearch', $this->criteresSearch);
    }

    /**
     * @return int
     */
    public function getPageSize()
    {
        return $this->pageSize;
    }

    /**
     * @param int $pageSize
     */
    public function setPageSize($pageSize)
    {
        $this->pageSize = $pageSize;
    }

    private $resultCount;

    /**
     * @return mixed
     */
    public function getResultCount()
    {
        return $this->resultCount;
    }

    /**
     * @param mixed $resultCount
     */
    public function setResultCount($resultCount)
    {
        $this->resultCount = $resultCount;
    }

    /**
     * @return mixed
     */
    public function getCriteresSearch()
    {
        return $this->criteresSearch;
    }

    /**
     * @param mixed $criteresSearch
     */
    public function setCriteresSearch($criteresSearch)
    {
        $this->criteresSearch = $criteresSearch;
    }
}
