    <com:TActivePanel ID="panelQualification" cssClass="content-bloc" >	
    	<div><com:TActiveLabel ID="trunLibelleQualif" />
    	<span
    		onmouseover="afficheBulle('<%=$this->infoQualif->getClientId()%>', this)" 
    		onmouseout="cacheBulle('<%=$this->infoQualif->getClientId()%>')" 
    		class="info-suite"> <com:TActiveLabel Display="None" ID="toisPointsInfoBulle" Text ="<%=Prado::localize('DEFINE_SUITE')%>"/>
    	</span>
    	</div>
    	<div class="spacer-mini"></div>
    	<span style="display:none;">
    		<com:TActiveTextBox ID="idsQualification" />
    		<com:TActiveTextBox TextMode="MultiLine" Rows="10" ID="libelleQualif" />
    		<com:TActiveButton 
	    		  ID="displayQualif" 
	    		  onCommand="displayQualification" 
	              OnCallBack="onCallBackQualification" />
    	</span>
    	<com:THyperLink 
    	NavigateUrl="javascript:popUpSetSize('index.php?page=<%= Application\Service\Atexo\Atexo_Util::getTypeUserCalledForPradoPages() %>.PopupQualification&ids=<%=($this->idsQualification->Text)%>&clientId=<%=$this->getClientId()%>','850px','400px','yes');"
    	cssClass="bouton-small"
    	Attributes.title="<%=Prado::localize('DEFINIR')%> <%=Prado::localize('TEXT_QUALIFICATIONS')%> <%=Prado::localize('NOUVELLE_FENETRE')%>"
    	>
    		<com:TTranslate>DEFINIR</com:TTranslate>
    	</com:THyperLink>
    </com:TActivePanel>
    	<com:TActivePanel ID="infoQualif" cssClass="info-bulle" >
    		<div><com:TActiveLabel ID="libellesQualif"/></div>
    	</com:TActivePanel>
	<com:TCustomValidator
    		ID="validatorQualification"
    		Enabled="false"
    		ValidationGroup="validateCreateCompte"
    		ControlToValidate="idsQualification"
        	Display="Dynamic"
    		ErrorMessage="<%=Prado::localize('TEXT_QUALIFICATIONS')%>"
            Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
    		EnableClientScript="true" > 						 					 
		<prop:ClientSide.OnValidationError>
			document.getElementById('divValidationSummary').style.display='';
		</prop:ClientSide.OnValidationError>
   </com:TCustomValidator>
