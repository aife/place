<?php

namespace Application\Controls;

use Prado\Web\UI\WebControls\TRegularExpressionValidator;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class TDateTimeValidator extends TRegularExpressionValidator
{
    public function onLoad($param)
    {
        $this->setRegularExpression("^(([0-2]\d|[3][0-1])/([0]\d|[1][0-2])/([1][9]\d{2}|[2][0]\d{2}) (([0-1][0-9]|[2][0-3])\:[0-5][0-9]))$");
    }
}
