<!--Debut Bloc Gestion des agents-->
<com:TActivePanel id="panelRefresh" >
		<div class="form-field">
			<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
			<div class="content">
				<div class="file-link float-right margin-right-10"> 
					<com:TLinkButton OnClick="genererExcel" visible="<%=($this->getNbrElementRepeater() == '0')? false:true%>" >
						<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-xls.gif" alt="<%=Prado::localize('EXPORT_XLS')%>" title="<%=Prado::localize('EXPORT_XLS')%>" /> <com:TTranslate>EXPORT_XLS</com:TTranslate>
					</com:TLinkButton>
				</div>
				<h3><com:TTranslate>TEXT_LISTE_AGENTS</com:TTranslate></h3>
				<div class="table-bloc">
				<!--Debut partitionneur-->
					<div class="line-partitioner">
						<h2><com:TTranslate>DEFINE_NOMBRE_RESULTATS</com:TTranslate> : <com:TLabel Text="<%=$this->getNbrElementRepeater()%>"/></h2>
						<div class="partitioner">
							<div class="intitule"><strong><label for="allAnnonces_nbResultsTop"><com:TLabel id="labelAfficherTop"><com:TTranslate>DEFINE_AFFICHER</com:TTranslate></com:TLabel></label></strong></div> 
							<com:TDropDownList Attributes.title="<%=Prado::localize('NOMBRE_DE_RESULTAT_PAR_PAGE')%>" id="nbResultsTop" AutoPostBack="true" onSelectedIndexChanged="changePagerLenght" >
	        					<com:TListItem Text="10" Selected="10"/>
	        					<com:TListItem Text="20" Value="20"/>
	        				</com:TDropDownList>
							<div class="intitule"><com:TLabel id="resultParPageTop"><com:TTranslate>DEFINE_RESULTATS_PAGE</com:TTranslate></com:TLabel></div> 
							<label style="display:none;" for="allAnnonces_pageNumberTop"><com:TTranslate>DEFINE_ALLER_PAGE</com:TTranslate></label>
							<com:TPanel ID="panelBouttonGotoPageTop" DefaultButton="DefaultButtonTop" CssClass="float-left">
								<com:TTextBox Attributes.title="<%=Prado::localize('N_DE_LA_PAGE')%>" Text="1" id="pageNumberTop" />
								<div class="nb-total ">
									<com:TLabel ID="labelSlashTop" Text="/" visible="true"/> 
									<com:TLabel ID="nombrePageTop"/>
									<com:TButton ID="DefaultButtonTop"  OnClick="goToPage" Attributes.style="display:none"/>
								</div>
							</com:TPanel>
							<div class="liens">
								<com:TPager
	        						ID="PagerTop"
	            					ControlToPaginate="RepeaterAgent"
	            					FirstPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-premier.gif' alt='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>' title='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>' />"
	            					PrevPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-precedent.gif' alt='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>' title='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>' />"
	            					Mode="NextPrev"
	            					NextPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-suivant.gif' alt='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>' title='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>' />"
	            					LastPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-dernier.gif' alt='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>' title='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>' />"
	            					OnPageIndexChanged="pageChanged"
	            				/>
							</div>
						</div>
					</div>
				<!--Fin partitionneur-->
					 <com:TRepeater ID="RepeaterAgent" 
						 DataKeyField="Id" 
						 EnableViewState="true" 
						 AllowPaging="true"
			    	     PageSize="<%= Application\Service\Atexo\Atexo_Config::getParameter('NOMBRE_AFFICHAGE_PAR_DEFAUT')%>"
			    	     AllowCustomPaging="true"
					 >
				    <prop:HeaderTemplate>
					<table class="table-results" summary="Liste des agents">
						<caption>TEXT_LISTE_AGENTS</caption>
						<thead>
							<tr>
								<th class="top" colspan="6"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></th>
							</tr>
							<tr>
								<th class="col-100" id="agentName">
			                        <com:TActiveLinkButton 
										OnCallback="Page.sortAgents" 
										ActiveControl.CallbackParameter="Nom"><com:TTranslate>DEFINE_NOM_MAJ</com:TTranslate></com:TActiveLinkButton>
									<com:TActiveLinkButton 
										OnCallback="Page.sortAgents" 
										ActiveControl.CallbackParameter="Prenom"><com:TTranslate>DEFINE_PRENOM</com:TTranslate></com:TActiveLinkButton>
									<com:TActiveImageButton 
										ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/arrow-tri-off.gif"
										OnCallback="Page.sortAgents" 
										ActiveControl.CallbackParameter="Nom" 
										AlternateText="<%=Prado::localize('DEFINE_TRIER')%>" 
										Attributes.title="<%=Prado::localize('DEFINE_TRIER')%>" /> /<br> <com:TTranslate>TEXT_ADRESSE_ELECTRONIQUE</com:TTranslate>
										<br/><com:TTranslate>TEXT_TEL</com:TTranslate>
										<br/><com:TTranslate>TEXT_FAX</com:TTranslate>
								</th>											
								<th class="col-150" id="entiteAchat"><com:TTranslate>TEXT_MINISTERE</com:TTranslate>/<br/><com:TTranslate>DEFINE_ENTITE_ACHAT</com:TTranslate></th>
								<th class="col-250" id="referentiels">
									<com:TLabel Text="<%#$this->SourceTemplateControl->getLibelleReferentielsAgent()%>"/>
								</th>
							</tr>
						</thead>
					</prop:HeaderTemplate>
					<prop:ItemTemplate>
						<tr class="<%#(($this->ItemIndex%2==0)? '':'on')%>" >
							<td class="col-100" headers="agentName">
                                <%#$this->Data->getNom()%> <%#$this->Data->getPrenom()%><br/><%#$this->Data->getEmail()%>
                                <br/><%#$this->Data->getNumTel()%><br/><%#$this->Data->getNumFax()%>
                            </td>
							<td class="col-150" headers="entiteAchat"><%#Application\Service\Atexo\Atexo_Organismes::getDenominationOrganisme($this->Data->getOrganisme())%><br/><%#$this->Data->getEntityPath()%></td>
						    <td class="col-250" headers="referentiels">
						    	<com:AtexoLtReferentiel  agent="1" mode="0" organisme="<%=$this->Data->getOrganisme()%>" loadData="1" idObject="<%=$this->Data->getId()%>" cssclass="intitule-140" afficherInfoBulle="0" />
						    </td>
						</tr>
					</prop:ItemTemplate>
					<prop:FooterTemplate>
				</table>					
					</prop:FooterTemplate>								 
				</com:TRepeater>
					<!--Debut partitionneur-->
					<div class="line-partitioner">
						<div class="partitioner">
							<div class="intitule"><strong><label for="horsLigne_nbResultsBottom"><com:TLabel id="labelAfficherDown"><com:TTranslate>DEFINE_AFFICHER</com:TTranslate></com:TLabel></label></strong></div> 
								<com:TDropDownList Attributes.title="<%=Prado::localize('NOMBRE_DE_RESULTAT_PAR_PAGE')%>" id="nbResultsBottom" AutoPostBack="true" onSelectedIndexChanged="changePagerLenght" >
	        					<com:TListItem Text="10" Selected="10"/>
	                  			<com:TListItem Text="20" Value="20"/>
	        				</com:TDropDownList>
							<div class="intitule"><com:TLabel id="resultParPageDown"><com:TTranslate>DEFINE_RESULTATS_PAGE</com:TTranslate></com:TLabel></div> 
							<label style="display:none;" for="horsLigne_pageNumberBottom"><com:TTranslate>DEFINE_ALLER_PAGE</com:TTranslate></label>
							<com:TPanel ID="panelBouttonGotoPageBottom" DefaultButton="DefaultButtonBottom" CssClass="float-left">
								<com:TTextBox Attributes.title="<%=Prado::localize('N_DE_LA_PAGE')%>" Text="1" id="pageNumberBottom" />
								<div class="nb-total ">
									<com:TLabel ID="labelSlashBottom" Text="/" visible="true"/> 
									<com:TLabel ID="nombrePageBottom"/>
									<com:TButton ID="DefaultButtonBottom"  OnClick="goToPage" Attributes.style="display:none"/>
								</div>
							</com:TPanel>
							<div class="liens">
								<com:TPager
	        						ID="PagerBottom"
	            					ControlToPaginate="RepeaterAgent"
	            					FirstPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-premier.gif' alt='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>' title='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>' />"
	            					PrevPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-precedent.gif' alt='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>' title='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>' />"
	            					Mode="NextPrev"
	            					NextPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-suivant.gif' alt='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>' title='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>' />"
	            					LastPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-dernier.gif' alt='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>' title='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>' />"
	            					OnPageIndexChanged="pageChanged"
	            				/>
							</div>
						</div>
					</div>
                    <!--Fin partitionneur-->
				</div>
				<span style="display:none"><com:TActiveButton id="refreshRepeater" onCallBack="refreshRepeater" /></span>
				<div class="breaker"></div>
			</div>
			<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
		</div>
</com:TActivePanel>
<!--Fin Bloc Gestion des agents-->