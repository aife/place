<?php

namespace Application\Controls;

use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonTDonneeComplementairePeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Exception;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Ref;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Category;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureType;
use Application\Service\Atexo\Consultation\Atexo_Consultation_TypeAvis;
use Application\Service\Atexo\Controller\Atexo_Controller_Front;
use Prado\Prado;


/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class ConsultationSummary extends MpeTPage
{
    private $_consultation = false;
    private $_reference = false;
    private $_withException = true;
    private $_langue;
    public $_activationFuseauHoraire = false;

    public function getReference()
    {
        return $this->_reference;
    }

    public function setReference($reference)
    {
        $this->_reference = $reference;
    }

    public function getConsultation()
    {
        return $this->_consultation;
    }

    public function setConsultation($consultation)
    {
        $this->_consultation = $consultation;
    }

    public function getWithException()
    {
        return $this->_withException;
    }

    public function setWithException($withException)
    {
        $this->_withException = $withException;
    }

    public function getLangue()
    {
        return $this->_langue;
    }

    public function setLangue($langue)
    {
        $this->_langue = $langue;
    }

    public function onLoad($param)
    {
        $this->_langue = Atexo_CurrentUser::readFromSession('lang');
        if (!($this->getConsultation() instanceof CommonConsultation) && !$this->getReference()) {
            if ($this->getWithException()) {
                throw new Atexo_Exception('ConsultationSummary Error : Attendu id de la référence ou objet consultation');
            } else {
                $this->summaryVisible->Visible = false;

                return;
            }
        }
        $org = Atexo_CurrentUser::getCurrentOrganism();
        if (!($this->getConsultation() instanceof CommonConsultation)) {
            $consultation = $this->recupererConsultation($this->_reference, $org, true);
            if (1 == (is_countable($consultation) ? count($consultation) : 0) && $consultation[0] instanceof CommonConsultation) {
                $consultation = array_shift($consultation);
                $this->_consultation = $consultation;
            }
        } else {
            $consultation = $this->getConsultation();
        }  //Début vérification de l'activation du fuseau horaire
        $this->_activationFuseauHoraire = (new Atexo_EntityPurchase())->getEtatActivationFuseauHoraire(Atexo_CurrentUser::getIdServiceAgentConnected(), Atexo_CurrentUser::getCurrentOrganism());
        if (Atexo_Module::isEnabled('FuseauHoraire', Atexo_CurrentUser::getCurrentOrganism()) && $this->getActivationFuseauHoraire()) {
            $this->lieuResidence->Text = $consultation->getLieuResidence();
            $this->dateFinLocale->Text = Atexo_Util::iso2frnDateTime(substr($consultation->getDatefinLocale(), 0, 16));
        }
        $langue = Atexo_CurrentUser::readFromSession('lang');
        $abbreviationLangue = Atexo_Languages::getLanguageAbbreviation($langue);
        $getIntituleAvis = 'getIntituleAvis'.$abbreviationLangue;
        $getDenominationOrg = 'getDenominationOrg'.$abbreviationLangue;
        $organismeO = Atexo_Organismes::retrieveOrganismeByAcronyme($org);
        $this->reference->Text = $consultation->getReferenceUtilisateur();
        $this->objet->Text = (new Atexo_Config())->toPfEncoding($consultation->getObjetTraduit());
        if (Atexo_Module::isEnabled('NumeroProjetAchat')) {
            $this->numProjetAchat->Text = $consultation->getCodeExterne();
        }
        $this->service->Text = Atexo_EntityPurchase::getPathEntityById($consultation->getServiceId(), $org, $langue);
        if (0 == strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) || !$organismeO->$getDenominationOrg()) {
            $this->organisme->Text = $organismeO->getSigle().' - '.$organismeO->getDenominationOrg();
        } else {
            $this->organisme->Text = $organismeO->getSigle().' - '.$organismeO->$getDenominationOrg();
        }
        if (Atexo_Module::isEnabled('LieuxExecution')) {
            $this->lieuxExecutionSuite->Text = trim((new Atexo_Consultation())->getLibelleLieuxExecution($this->_langue, $consultation), ',');
            $this->lieuxExecutions->Text = trim(Atexo_Util::truncateTexte((new Atexo_Consultation())->getLibelleLieuxExecution($this->_langue, $consultation), 100, true), ',');
            $this->spanLieux->visible = Atexo_Util::isTextTruncated((new Atexo_Consultation())->getLibelleLieuxExecution($this->_langue, $consultation));
        }
        if (Atexo_Module::isEnabled('CodeNutLtReferentiel')) {//Début codes nuts
            $atexoCodesNutsRef = new Atexo_Ref();
            $atexoCodesNutsRef->setCheminFichierConfigXML(Atexo_Config::getParameter('PATH_FILE_NUTS_CONFIG'));
            $atexoCodesNutsRef->setSecondaires($consultation->getCodesNuts());
            $this->idAtexoRefNuts->afficherReferentiel($atexoCodesNutsRef);
        }
        $typeAvis = (new Atexo_Consultation_TypeAvis())->retrieveTypeAnnonce($consultation->getIdTypeAvis());
        if ($typeAvis) {
            if (0 == strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) || !$typeAvis->$getIntituleAvis()) {
                $this->typeAnnonce->Text = $typeAvis->getIntituleAvis();
            } else {
                $this->typeAnnonce->Text = $typeAvis->$getIntituleAvis();
            }
        }
        $this->typeProcedure->Text = (new Atexo_Consultation_ProcedureType())->retrieveTypeProcedure($consultation->getIdTypeProcedureOrg(), false, null, $langue);
        $this->typeContrat->Text = $consultation->getLibelleTypeContrat();
        if ($consultation->getModePassation() == Atexo_Config::getParameter('MODE_PASSATION_AU_RABAIS')) {
            $this->modePassation->Text = ' | '.Prado::localize('AU_RABAIS');
        } elseif ($consultation->getModePassation() == Atexo_Config::getParameter('MODE_PASSATION_OFFRE_PRIX')) {
            $this->modePassation->Text = ' | '.Prado::localize('SUR_OFFRE_DE_PRIX');
        }
        $this->titre->Text = (new Atexo_Config())->toPfEncoding($consultation->getIntituleTraduit());
        $this->objetRecap->Text = (new Atexo_Config())->toPfEncoding($consultation->getObjetTraduit());
        $this->categorie->Text = Atexo_Consultation_Category::retrieveLibelleCategorie($consultation->getCategorie(), false, null, $langue);
        if (Atexo_Module::isEnabled('AffichageCodeCpv')) {
            $atexoRef = new Atexo_Ref();
            $atexoRef->setCheminFichierConfigXML(Atexo_Config::getParameter('PATH_FILE_CPV_CONFIG'));
            $atexoRef->setPrincipal($consultation->getCodeCpv1());
            $atexoRef->setSecondaires($consultation->getCodeCpv2());
            $this->idAtexoRef->afficherReferentiel($atexoRef);
        }
        $this->dateFin->Text = Atexo_Util::iso2frnDateTime($consultation->getDatefin()) ?? '-';
        if (Atexo_Module::isEnabled('RegleMiseEnLigneParEntiteCoordinatrice')) {
            $dateMiseEnligne = Atexo_Util::iso2frnDateTime($consultation->getDateMiseEnLigneSouhaitee());
            if ($dateMiseEnligne) {
                $this->dateMiseEnLigneParEntiteCoordinatrice->Text = $dateMiseEnligne;
            } else {
                $this->dateMiseEnLigneParEntiteCoordinatrice->Text = '-';
            }
        }
        if ($consultation->getIdTypeAvis() != Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION')) {
            $this->panelDetailsAnnonce->setVisible(true);
            $this->panelDateLimiteRemisePlis->setVisible(false);
            $this->panelDateFinAffichage->setVisible(true);
            $this->detailAnnonce->Text = $consultation->getDetailConsultation();
            $this->dateFinAffichage->Text = Atexo_Util::iso2frnDateTime($consultation->getDatefin());
            $this->pictMpsLogo->setVisible(false);
            $this->logoDume->setVisible(false);
        } else {
            self::displayMPS($consultation);
            $this->logoDume->setVisible(Atexo_Module::isEnabled('InterfaceDume') && $consultation->getDumeDemande());
            $this->logoDume->setText($consultation->getInfoDumeAcheteur());
        }
        $this->idRefZoneText->setDependanceLot($this->_consultation->getAlloti());
        $this->idRefRadio->setDependanceLot($this->_consultation->getAlloti());
        $this->ltrefCons->afficherReferentielConsultation($consultation->getId(), 0, $org);
        $this->idRefZoneText->afficherTextConsultation($consultation->getId(), 0, $org);
        $this->idRefRadio->afficherReferentielRadio($consultation->getId(), 0, $org);
        self::setInfoRapportPdf($consultation);
        if (Atexo_Module::isEnabled('AnnulerConsultation')) {
            if ($consultation->getConsultationAnnulee()) {
                $this->panelDateAnnulation->setVisible(true);
                $this->dateAnnulation->Text = Atexo_Util::iso2frnDate($consultation->getDateDecisionAnnulation());
                $this->pictConsultationAnnulee->setVisible(true);
                $this->pictConsultationAnnulee->ImageUrl = Atexo_Controller_Front::t().'/images/picto-cons-annulee.gif';
                $this->pictConsultationAnnulee->Attributes->title = Prado::Localize('CONSULTATION_ANNULEE');
            } else {
                $this->panelDateAnnulation->setVisible(false);
                $this->pictConsultationAnnulee->setVisible(false);
            }
        } else {
            $this->panelDateAnnulation->setVisible(false);
            $this->pictConsultationAnnulee->setVisible(false);
        }

        $this->panelValeurEstimer->setVisible(false);
        if (!Atexo_Config::getParameter('SPECIFIQUE_PLACE')) {
            $this->montant->Text = null;
            if(Atexo_Module::isEnabled('DonneesComplementaires')){
                $this->panelValeurEstimer->setVisible(true);
                if($consultation->getIdDonneeComplementaire()){
                    $donneeComplementaire = CommonTDonneeComplementairePeer::retrieveByPK(
                        $consultation->getIdDonneeComplementaire());
                    $this->montant->Text = $donneeComplementaire->getMontantMarche();
                }
            }
        }
    }

    public function setActivationFuseauHoraire($value)
    {
        $this->_activationFuseauHoraire = $value;
    }

    public function getActivationFuseauHoraire()
    {
        return $this->_activationFuseauHoraire;
    }

    public function setInfoRapportPdf($consultation)
    {
        $infoRapportPDF = [];
        $infoRapportPDF['RefConsUser'] = $consultation->getReferenceUtilisateur();
        $infoRapportPDF['entiteAchat'] = Atexo_EntityPurchase::getPathEntityById($consultation->getServiceId(), Atexo_CurrentUser::getCurrentOrganism(), Atexo_CurrentUser::readFromSession('lang'));
        $infoRapportPDF['entitePublique'] = $this->organisme->Text;
        $infoRapportPDF['typeAnnonce'] = $this->typeAnnonce->Text;
        $infoRapportPDF['typeProcedure'] = $this->typeProcedure->Text;
        $infoRapportPDF['intituleConsultation'] = $this->titre->Text;
        $infoRapportPDF['objetConsultation'] = $this->objetRecap->Text;
        $infoRapportPDF['dateRemisePlis'] = $this->dateFin->Text;
        $this->Page->setViewState('infoRapportPdfCons', $infoRapportPDF);
    }

    /**
     * @author Oumar KONATE <oumar.konate@atexo.com>
     * Permet de recuperer l'objet consultation
     *
     * @param string $reference: la référence de la consultation
     * @param string $organisme: l'organisme
     *
     * @return CommonConsultation: objet consultation
     */
    public function recupererConsultation($reference, $organisme, $toUseServiceIdConsultation = false)
    {
        $serviceId = Atexo_CurrentUser::getCurrentServiceId();
        if (true === $toUseServiceIdConsultation) {
            $consultation = (new Atexo_Consultation())->retrieveConsultation($reference);
            if ($consultation instanceof CommonConsultation) {
                $serviceId = $consultation->getServiceId();
            }
        }

        $criteria = new Atexo_Consultation_CriteriaVo();
        $criteria->setcalledFromPortail(false);
        $criteria->setIdService($serviceId);
        $criteria->setAcronymeOrganisme($organisme);
        $criteria->setIdReference($reference);
        $criteria->setConnectedAgentId(Atexo_CurrentUser::getId());

        return (new Atexo_Consultation())->search($criteria);
    }

    /**
     * Permet d'afficher le logo MPS Si on marche public simplifie.
     *
     * @param CommonConsultation $consultation la consultation
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function displayMPS($consultation)
    {
        if (Atexo_Module::isEnabled('MarchePublicSimplifie', $consultation->getOrganisme()) && $consultation->getMarchePublicSimplifie()) {
            $this->pictMpsLogo->setVisible(true);
            $this->pictMpsLogo->ImageUrl = Atexo_Controller_Front::t().'/images/logo-mps-small.png';
            $this->pictMpsLogo->Attributes->title = Prado::Localize('DEFINE_MARCHE_PUBLIC_SIMPLIFIE');
        } else {
            $this->pictMpsLogo->setVisible(false);
        }
    }
}
