<div class="toggle-panel form-toggle donnees-redac">
	<div class="content">
		<div class="cover"></div>
		<a class="title-toggle-open" onclick="togglePanel(this,'panelConditionsMarche');" href="javascript:void(0);"><com:TTranslate>DEFINE_FORME_JURIDIQUE_GROUPEMENT_ATTRIBUTAIRE</com:TTranslate></a>
		<div class="panel no-indent panel-donnees-complementaires" style="display:block;" id="panelConditionsMarche">
			<!--Debut Ligne Forme groupement-->
			<div class="line">
				<div class="intitule col-300"><label for="formeGroupement"><com:TTranslate>DEFINE_FORME_REQUISE_SI_GROUPEMENT</com:TTranslate></label> :</div>
				<div class="content-bloc">
					<com:TActiveDropDownList 
						id="formeGroupement" 
						CssClass="moyen" 
						Attributes.title="<%=Prado::localize('DEFINE_FORME_ATTENDU_GROUPEMENT')%>" 
					/>
				</div>
			</div>
			<!--Fin Ligne Forme groupement-->
			<div class="spacer-small"></div>
		</div>
		<div class="breaker"></div>
	</div>
</div>
