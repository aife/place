<?php

namespace Application\Controls;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonTNotificationAgent;
use Application\Service\Atexo\Agent\Atexo_Agent_Notifications;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Util;
use Exception;

/**
 * @author LCH
 * Template referentiel
 */
class AtexoNotificationsAgent extends MpeTTemplateControl
{
    public function onLoad($param)
    {
        if (!$this->Page->getIsCallback()) {
            $this->javascriptLabel->Text = '<script>refreshNotifications();</script>';
        }
    }

    /**
     * Permet de retourner le nombre des notifications non lue.
     *
     * @return int
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function getNombreNotificationsActif()
    {
        return (new Atexo_Agent_Notifications())->getNombreNotificationActif(Atexo_CurrentUser::getId());
    }

    /**
     * Permet de retourner le nombre des notifications non lue.
     *
     * @return int
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function fillRepeaterNotification()
    {
        $dataSource = (new Atexo_Agent_Notifications())->getNotificationActif(Atexo_CurrentUser::getId());
        if (is_array($dataSource)) {
            $this->repeaterNotifications->DataSource = $dataSource;
            $this->repeaterNotifications->DataBind();
        }
    }

    /**
     * Permet de retourner le nombre des notifications non lue.
     *
     * @return int
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function viderRepeaterNotification()
    {
        $this->repeaterNotifications->DataSource = [];
        $this->repeaterNotifications->DataBind();
    }

    /**
     * Permet de retourner le nombre des notifications non lue.
     *
     * @return int
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function refreshNotification()
    {
        $this->fillRepeaterNotification();
        // $this->panelNotification->setDisplay('Dynamic');
        $this->javascriptLabel->Text = "<script>refreshNotifications();J('.notificationsLoader').hide();</script>";
    }

    /**
     * Permet de retourner le nombre des notifications non lue.
     *
     * @return int
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function getNotifications()
    {
        $dataSource = (new Atexo_Agent_Notifications())->getNotificationActif(Atexo_CurrentUser::getId());
        if (is_array($dataSource)) {
            return $dataSource;
        }

        return [];
    }

    /**
     * Permet de retourner le nombre des notifications non lue.
     *
     * @return int
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function lireNotification($sender, $param)
    {
        $notification = null;
        try {
            $idNotif = $param->CommandParameter;
            if ($idNotif) {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                $notification = (new Atexo_Agent_Notifications())->getNotificationById($idNotif, $connexion);
                if ($notification instanceof CommonTNotificationAgent) {
                    $notification->setNotificationLue('1');
                    $notification->setDateLecture(date('Y-m-d H:i:s'));
                    $notification->save($connexion);
                    $url = $notification->getUrlNotification();
                    if ($url) {
                        $this->response->redirect(Atexo_Config::getParameter('PF_URL_AGENT').$url);
                    } else {
                        //TODO PB de isPostBack dans les composant TabealDeBord
                        $this->response->redirect(Atexo_Util::getCurrentUrl());
                        //$this->refreshNotification();
                    }
                }
            }
        } catch (Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('app');
            $logger->error('Erreur lors de la mise à jour de la notification = '.$notification->getIdNotification().' => '.$e->getMessage().' '.$e->getTraceAsString());
        }
    }

    /**
     * Permet de retourner le nombre des notifications non lue.
     *
     * @return int
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function desactiverNotification($sender, $param)
    {
        $notification = null;
        try {
            $idNotif = $param->CallbackParameter;
            if ($idNotif) {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                $notification = (new Atexo_Agent_Notifications())->getNotificationById($idNotif, $connexion);
                if ($notification instanceof CommonTNotificationAgent) {
                    $notification->setNotificationActif('0');
                    $notification->save($connexion);
                    $this->refreshNotification();
                }
            }
        } catch (Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('app');
            $logger->error('Erreur lors de la mise à jour de la notification = '.$notification->getIdNotification().' => '.$e->getMessage().' '.$e->getTraceAsString());
        }
    }
}
