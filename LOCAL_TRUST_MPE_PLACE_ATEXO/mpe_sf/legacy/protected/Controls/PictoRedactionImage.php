<?php

namespace Application\Controls;

use Application\Service\Atexo\Controller\Atexo_Controller_Front;
use Prado\Prado;
use Prado\Web\UI\WebControls\TImage;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PictoRedactionImage extends TImage
{
    public string $_activate = 'false';
    public string $_clickable = 'true';

    public function onPrerender($param)
    {
        if ('false' == $this->_activate) {
            $this->setImageUrl(Atexo_Controller_Front::t().'/images/picto-acces-redaction-inactive.gif');
        } else {
            $this->setImageUrl(Atexo_Controller_Front::t().'/images/picto-acces-redaction.gif');
        }
        if ('false' == $this->_clickable) {
            $this->Attributes->onclick = 'return false;';
        }
        $this->setAlternateText(Prado::localize('TEXTE_GERER_DOCUMENTS'));
        $this->setToolTip(Prado::localize('TEXTE_GERER_DOCUMENTS'));
    }

    public function getActivate()
    {
        return $this->_activate;
    }

    public function setActivate($bool)
    {
        $this->_activate = $bool;
    }

    public function getClickable()
    {
        return $this->_clickable;
    }

    public function setClickable($bool)
    {
        $this->_clickable = $bool;
    }
}
