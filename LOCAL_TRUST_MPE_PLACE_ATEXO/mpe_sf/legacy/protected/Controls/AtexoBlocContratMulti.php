<?php

namespace Application\Controls;

use Application\Library\Propel\Exception\PropelException;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonGeolocalisationN2;
use Application\Propel\Mpe\CommonTContratMulti;
use Application\Propel\Mpe\CommonTContratTitulaire;
use Application\Service\Atexo\Atexo_CommonReferentiel;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Departement;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Category;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Contrat;
use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2;
use Prado\Prado;

/**
 * Le bloc identite de contrat multi.
 *
 * @author LEZ <loubna.ezziani@atexo.com>
 *
 * @version 1.0
 *
 * @since 4.11.0
 *
 * @copyright Atexo 2015
 */
class AtexoBlocContratMulti extends MpeTTemplateControl
{
    public $codeDenomAdapte;
    
    /**
     * Permet de recuperer l'objet contrat multi.
     *
     * @return CommonTContratMulti
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function getContratMultiO()
    {
        return $this->getViewState('contratMultiO');
    }

    /**
     * Permet d'initialiser l'objet contrat multi.
     *
     * @param CommonTContratMulti $v
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function setContratMultiO($v)
    {
        $this->setViewState('contratMultiO', $v);
    }

    /**
     * Permet de recuperer le numero de contrat multi.
     *
     * @return string numero de contrat
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function getNumeroContrat()
    {
        return $this->getViewState('numeroContra');
    }

    /**
     * Permet d'initialiser le numero de contrat multi.
     *
     * @param string $v
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function setNumeroContrat($v)
    {
        $this->setViewState('numeroContra', $v);
    }

    /**
     * Permet de recuperer le numero de contrat multi.
     *
     * @return string numero de contrat
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function getNumeroContratMultiCourt()
    {
        return $this->getViewState('numeroContraMultiCourt');
    }

    /**
     * Permet d'initialiser le numero de contrat multi.
     *
     * @param string $v
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function setNumeroContratMultiCourt($v)
    {
        $this->setViewState('numeroContraMultiCourt', $v);
    }

    /**
     * Permet de recuperer le numero de contrat multi.
     *
     * @return string numero de contrat
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function getNumeroContratMultiLong()
    {
        return $this->getViewState('numeroContraMultiLong');
    }

    /**
     * Permet d'initialiser le numero de contrat multi.
     *
     * @param string $v
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function setNumeroContratMultiLong($v)
    {
        $this->setViewState('numeroContraMultiLong', $v);
    }

    /**
     * Permet d'initialiser le composant.
     *
     * @param CommonTContratMulti $contratMulti
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function initialiserComposant($contratMulti, $idTypeContra, $contrat = null)
    {
        $lieuExecution = [];
        $this->setContratMultiO($contratMulti);
        $this->chargerCategories($contratMulti);
        if (!empty($contrat) && !empty($contrat->getFormePrix())) {
            $this->chargerFormePrix($contrat);
        } else {
            $this->chargerFormePrix($contratMulti);
        }

        $this->fillCcagReference();
        if (!empty($contrat->getNomLieuPrincipalExecution())) {
            $this->lieuPrincipalExecution->setData($contrat->getNomLieuPrincipalExecution());
        } else {
            $lieuxExecutionArray = explode(',', $contrat->getLieuExecution());
            $lieuxExecution = array_values(array_filter($lieuxExecutionArray, fn($value) => '' !== $value));
            if (!empty($lieuExecution)) {
                $this->lieuPrincipalExecution->setData($lieuExecution[0]->getDenomination1());
            }
        }
        $this->chargerComposantBydTypeContrat($idTypeContra);
    }

    /**
     * Permet de charger le composant selon le type de contrat.
     *
     * @param int $idType
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function chargerComposantBydTypeContrat($idType)
    {
        if ((new Atexo_Consultation_Contrat())->isTypeContratAvecChapeauById($idType)) {
            $isContratSAD = (new Atexo_Consultation_Contrat())->isTypeContratSad($idType);
            $isContratAccordCadre = (new Atexo_Consultation_Contrat())->isTypeContratAccordCadre($idType);
            $this->chargerDonneeObjetContratMulti();
            $this->panelContratMulti->setDisplay('Dynamic');
            if ((new Atexo_Consultation_Contrat())->isTypeContratAvecMontantMaxById($idType)) {
                $this->panelMontantAC->setVisible(true);
            } else {
                $this->panelMontantAC->setVisible(false);
            }
            if ($isContratAccordCadre || $isContratSAD) {
                $this->panelNumero->setVisible(true);
            } else {
                $this->panelNumero->setVisible(false);
            }
        } else {
            $this->panelContratMulti->setDisplay('None');
        }
    }

    /**
     * @param $sender
     * @param $param
     *
     * @throws PropelException
     */
    protected function suggestLieux($sender, $param)
    {
        // Get the token
        $token = $param->getToken();

        if ($listeEntite = (new Atexo_Geolocalisation_GeolocalisationN2())->retrieveDepartements($token)) {
            $sender->DataSource = $listeEntite;
            $sender->dataBind();
        } else {
            $sender->DataSource = [];
            $sender->dataBind();
            $this->codeDenomAdapte = '';
            $this->Page->setViewState('denominationAdapte', '');
        }
    }

    /**
     * @param $sender
     * @param $param
     */
    public function suggestionSelected($sender, $param)
    {
        $this->codeDenomAdapte = $sender->Suggestions->DataKeys[$param->selectedIndex];
        $this->Page->setViewState('denominationAdapte', '');

        if ('' != $this->codeDenomAdapte) {
            $this->Page->setViewState('denominationAdapte', $this->codeDenomAdapte);
        }
    }

    /**
     * @param $sender
     * @param $param
     */
    public function chargerLieuxExecution($sender, $param)
    {
        // Get the token
        $token = $param->getToken();

        // Sender is the Suggestions repeater
        $sender->DataSource = $this->chargerDepartementsListe($token);
        $sender->lieuPrincipalExecution->dataBind();
    }

    /**
     * @param $name
     *
     * @return array
     */
    public function chargerDepartementsListe($name)
    {
        $departements = (new Atexo_Departement())->getListDepartements(true);

        if ($name) {
            return array_filter($departements, fn($el) => false !== stripos($el['name'], (string) $name));
        } else {
            return $departements;
        }
    }

    /**
     * Permet de charger la natures de prestation.
     *
     * @param CommonConsultation $consultation la consultation
     * @param array              $listeLots    la liste des lots
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function chargerCategories($contratMulti)
    {
        if ($contratMulti instanceof CommonTContratMulti) {
            $categories = Atexo_Consultation_Category::retrieveCategories();
            $dataCategories = Atexo_Consultation_Category::retrieveDataCategories($categories);
            $this->naturePrestations->Datasource = $dataCategories;
            $this->naturePrestations->dataBind();
            $natureDePrestation = $contratMulti->getCategorie();
            $this->naturePrestations->setSelectedValue($natureDePrestation);
        }
    }

    /**
     * Permet de récupérer la forme du prix.
     *
     * @param $contrat
     */
    public function chargerFormePrix($contrat)
    {
        if (!empty($contrat->getFormePrix())) {
            if (1 == $contrat->getFormePrix()) {
                $this->formePrix_ferme->setChecked(true);
            } elseif (2 == $contrat->getFormePrix()) {
                $this->formePrix_ferme_actualisable->setChecked(true);
            } elseif (3 == $contrat->getFormePrix()) {
                $this->formePrix_revisable->setChecked(true);
            }
        }

        $this->formePrix_ferme->dataBind();
        $this->formePrix_ferme_actualisable->dataBind();
        $this->formePrix_revisable->dataBind();
    }

	/**
	 * remplir liste CCAG
	 *
	 * @return void
	 * @author loubna EZZIANI <loubna.ezziani@atexo.com>
	 * @version 1.0
	 * @since 4.11.0
	 * @copyright Atexo 2015
	 */
	public function fillCcagReference(){
        $dataCcagRef = (new Atexo_CommonReferentiel())->retrieveValeurReferentielCCAG(Atexo_Config::getParameter('REFERENTIEL_CCAG_REFERENCE'), false, true);
        $dataSource = array();
		if($dataCcagRef)
		{
			foreach($dataCcagRef as $oneVal)
			{
				$dataSource[$oneVal->getId()] = $oneVal->getLibelleValeurReferentiel();
			}
		}
		$this->ccag->DataSource = $dataSource;
		$this->ccag->DataBind();
	}

    /**
     * Permet de charger le les donnee de contrat multi.
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function chargerDonneeObjetContratMulti()
    {
        $contratMulti = $this->getContratMultiO();
        if ($contratMulti instanceof CommonTContratMulti) {
            $this->article133->setVisible(false);
            $this->titreTypeContrat->Text = Prado::Localize('INFORMATIONS').' '.$contratMulti->getLibelleTypeContrat();
            $idTypeContrat = $contratMulti->getIdTypeContrat();
            if ((new Atexo_Consultation_Contrat())->isTypeContratSad($idTypeContrat)) {
                $this->labelObjetMarche->setText(Prado::Localize('OBJET_SAD'));
            } elseif ((new Atexo_Consultation_Contrat())->isTypeContratAccordCadre($idTypeContrat)) {
                $this->labelObjetMarche->setText(Prado::Localize('OBJET_ACCORD_CADRE'));
            } else {
                $this->labelObjetMarche->setText(Prado::Localize('DEFINE_OBJET_MARCHE'));
            }
            $this->intitule->setText($contratMulti->getIntitule());
            if ($contratMulti->getObjetContrat()) {
                $this->objetMarche->setText(mb_strimwidth($contratMulti->getObjetContrat(), 0, 255));
            }
            if (Atexo_Module::isEnabled('AffichageCodeCpv')) {
                //charger referentiel CPV
                $this->referentielCPV->onInitComposant();
                $this->referentielCPV->setCodeCpvObligatoire(true);
                $this->referentielCPV->chargerReferentielCpv($contratMulti->getCodeCpv1(), $contratMulti->getCodeCpv2());
            }

            if ($contratMulti->getNumeroContrat()) {
                $this->numero->setText($contratMulti->getNumeroContrat());
            } elseif (!$contratMulti->getIdContratTitulaire()) {
                $numeroContratMulti = $this->getNumeroContrat();
                if (!$numeroContratMulti) {
                    /*$contrat = new CommonTContratTitulaire();
                    $contrat->setServiceId($contratMulti->getServiceId());
                    $contrat->setOrganisme($contratMulti->getOrganisme());
                    $numeroContratCourt = '';
                    $numeroContratLong = '';
                    Atexo_Consultation_Contrat::genererNumerotationContrat($numeroContratCourt, $numeroContratLong, $contrat, $contratMulti);
                    $numeroContratMulti = $numeroContratLong . Atexo_Config::getParameter('SEPARATEUR_NUMEROTATION_CONTRAT') . "00";
                    $this->setNumeroContratMultiCourt($numeroContratCourt);
                    $this->setNumeroContratMultiLong($numeroContratLong);
                    $this->setNumeroContrat($numeroContratMulti);*/
                    //TODO : A revoir suite au deplacement du bloc des contrats multi de l'attribution du contrat à la declaration du contrat
                }
                $this->numero->Text = $numeroContratMulti;
            }

            if ((new Atexo_Consultation_Contrat())->isTypeContratAvecMontantMaxById($contratMulti->getIdTypeContrat()) && $contratMulti->getMontantMaxEstime()) {
                $this->montantMax->setText(Atexo_Util::getMontantArronditEspace($contratMulti->getMontantMaxEstime()));
            }

            if ($contratMulti->getCategorie()) {
                $this->naturePrestations->setSelectedValue($contratMulti->getCategorie());
            }

            if ($contratMulti->getCcagApplicable()) {
                $this->ccag->setSelectedValue($contratMulti->getCcagApplicable());
            }
            if ($this->getContratAvecArticle133($contratMulti)) {
                if ($contratMulti->getPublicationMontant() && $contratMulti->getPublicationContrat()) {
                    $this->art133Publie->setChecked(false);
                    $this->art133NonPublie->setChecked(true);

                } elseif (0 === $contratMulti->getPublicationMontant() && 0 === $contratMulti->getPublicationContrat()) {
                    $this->art133Publie->setChecked(true);
                    $this->art133NonPublie->setChecked(false);
                }
            }
            $this->achatResponsableConsultation->afficherClausesConsultation($contratMulti);
        }
    }

    /**
     * Vérifie si un contrat est avec article 133.
     *
     * @return \Application\Service\Atexo\Consultation\Bollean
     */
    protected function getContratAvecArticle133(CommonTContratTitulaire $contrat)
    {
        return (new Atexo_Consultation_Contrat())->isTypeContratArticle133ById($contrat->getIdTypeContrat());
    }

    /**
     * Permet de sauvegarder le contrat multi.
     *
     * @param ObjetConnexion $connexion
     * @param string         $organisme
     * @param int            $serviceId
     * @param int            $idTypeContrat
     *
     * @return CommonTContratMulti
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function saveContratMulti($connexion, $organisme, $serviceId, $idTypeContrat)
    {
        $contratMulti = $this->getContratMultiO();
        if ($contratMulti instanceof CommonTContratMulti) {
            if ((new Atexo_Consultation_Contrat())->isTypeContratAvecChapeauById($idTypeContrat)) {
                if (!$contratMulti->getIdContratMulti()) {
                    $contratMulti->setDateCreation(date('Y-m-d H:i:s'));
                }
                $contratMulti->setDateModification(date('Y-m-d H:i:s'));

                $contratMulti->setIntitule($this->intitule->getSafeText());
                $contratMulti->setObjetContrat($this->objetMarche->getSafeText());
                $contratMulti->setNomLieuPrincipalExecution($this->lieuPrincipalExecution->Text);

                $lieuExecutionId = (new Atexo_Geolocalisation_GeolocalisationN2())->retrieveIdsByDenomination1($this->lieuPrincipalExecution->Text);
                $lieuExecution = (new Atexo_Geolocalisation_GeolocalisationN2())->retrieveGeolocalisationN2ById($lieuExecutionId);
                if ($lieuExecution[0] instanceof CommonGeolocalisationN2) {
                    $contratMulti->setCodeLieuPrincipalExecution($lieuExecution[0]->getDenomination2());
                    $contratMulti->setTypeCodeLieuPrincipalExecution(1); // todo récupérer valeur dynamiquement (DB)
                }
                $contratMulti->setNumeroContrat($this->numero->getSafeText());

                if ((new Atexo_Consultation_Contrat())->isTypeContratAvecMontantMaxById($idTypeContrat)) {
                    $contratMulti->setMontantMaxEstime(Atexo_Util::getMontantArronditSansEspace($this->montantMax->getSafeText()));
                }

                $formePrix = '';
                if ($this->formePrix_ferme->Checked) {
                    $formePrix = 1;
                } elseif ($this->formePrix_ferme_actualisable->Checked) {
                    $formePrix = 2;
                } elseif ($this->formePrix_revisable->Checked) {
                    $formePrix = 3;
                }

                $contratMulti->setFormePrix($formePrix);
                $contratMulti->setCategorie($this->naturePrestations->getSelectedValue());
                $contratMulti->setCcagApplicable($this->ccag->getSelectedValue());
                $contratMulti->setOrganisme($organisme);
                $contratMulti->setServiceId($serviceId);
                $contratMulti->setIdTypeContrat($idTypeContrat);
                $this->achatResponsableConsultation->save($contratMulti);
                $codePrincipal = $this->referentielCPV->getCpvPrincipale();
                if ('' != $codePrincipal) {
                    $contratMulti->setCodeCpv1($codePrincipal);
                } else {
                    $contratMulti->setCodeCpv1(null);
                }
                $codesSec = $this->referentielCPV->getCpvSecondaires();
                if ('' != $codesSec) {
                    $contratMulti->setCodeCpv2($codesSec);
                } else {
                    $contratMulti->setCodeCpv2(null);
                }

                if ($this->getContratAvecArticle133($contratMulti)) {
                    if ($this->art133NonPublie->getChecked()) {
                        $contratMulti->setPublicationMontant('1');
                        $contratMulti->setPublicationContrat('1');
                    } elseif ($this->art133Publie->getChecked()) {
                        $contratMulti->setPublicationMontant('0');
                        $contratMulti->setPublicationContrat('0');
                    }
                }
                $contratMulti->save($connexion);

                return $contratMulti;
            }
        }

        return false;
    }

    /**
     * Permet de valider les donnees contrat multi.
     *
     * @param bool   $isValid
     * @param array  $arrayMsgErreur tableau des message d'erreurs
     * @param string $scriptJs       le script js
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function validerDonneeContratMulti(&$isValid, &$arrayMsgErreur, &$scriptJs)
    {
        $contratMulti = $this->getContratMultiO();

        //validation de l'intitulé du contrat
        if ($this->intitule->getSafeText()) {
            $scriptJs .= "document.getElementById('".$this->ImgErrorIntituleContrat->getClientId()."').style.display = 'none';";
        } else {
            $scriptJs .= "document.getElementById('".$this->ImgErrorIntituleContrat->getClientId()."').style.display = '';";
            $isValid = false;
            $scriptJs .= "document.getElementById('divValidationSummary').style.display='';";
            $arrayMsgErreur[] = Prado::Localize('INTITULE_CONTRAT_OBLIGATOIRE');
        }

        //validation d'objet de marche
        if ($this->objetMarche->getSafeText()) {
            $scriptJs .= "document.getElementById('".$this->ImgErrorObjetMarche->getClientId()."').style.display = 'none';";
        } else {
            $scriptJs .= "document.getElementById('".$this->ImgErrorObjetMarche->getClientId()."').style.display = '';";
            $isValid = false;
            $scriptJs .= "document.getElementById('divValidationSummary').style.display='';";

            if ($contratMulti->isTypeContratACSad()) {
                $arrayMsgErreur[] = Prado::Localize('OBJET_ACCORD_CADRE');
            } else {
                $arrayMsgErreur[] = Prado::Localize('DEFINE_OBJET_MARCHE');
            }
        }

        if ($contratMulti instanceof CommonTContratMulti && (new Atexo_Consultation_Contrat())->isTypeContratAvecMontantMaxById($contratMulti->getIdTypeContrat())) {
            //validation de montant max estime
            if ((float) $this->montantMax->getSafeText() > 0) {
                $scriptJs .= "document.getElementById('".$this->imgErrorMontantMax->getClientId()."').style.display = 'none';";
            } else {
                $scriptJs .= "document.getElementById('".$this->imgErrorMontantMax->getClientId()."').style.display = '';";
                $isValid = false;
                $scriptJs .= "document.getElementById('divValidationSummary').style.display='';";
                $arrayMsgErreur[] = Prado::Localize('MONTANT_MAX_ESTIME');
            }
        }

        //validation de nature de prestation
        if ($this->naturePrestations->getSelectedValue()) {
            $scriptJs .= "document.getElementById('".$this->imgErrorNaturePrestations->getClientId()."').style.display = 'none';";
        } else {
            $scriptJs .= "document.getElementById('".$this->imgErrorNaturePrestations->getClientId()."').style.display = '';";
            $isValid = false;
            $scriptJs .= "document.getElementById('divValidationSummary').style.display='';";
            $arrayMsgErreur[] = Prado::Localize('NATURE_DES_PRESTATIONS');
        }
        //validation de CCAG
        if ((float) $this->ccag->getSelectedValue()) {
            $scriptJs .= "document.getElementById('".$this->imgErrorCcag->getClientId()."').style.display = 'none';";
        } else {
            $scriptJs .= "document.getElementById('".$this->imgErrorCcag->getClientId()."').style.display = '';";
            $isValid = false;
            $scriptJs .= "document.getElementById('divValidationSummary').style.display='';";
            $arrayMsgErreur[] = Prado::Localize('FCSP_CCAG_APPLICABLE');
        }
        if (Atexo_Module::isEnabled('ConsultationClause')) {
            $this->achatResponsableConsultation->validerAchatResponsable($isValid, $arrayMsgErreur, $scriptJs);
        }
        if (Atexo_Module::isEnabled('AffichageCodeCpv')
            && empty($this->referentielCPV->getCpvPrincipale())) {
            $scriptJs .= "document.getElementById('".$this->referentielCPV->getIdClientImgErrorCpv()."').style.display = '';";
            $isValid = false;
            $scriptJs .= "document.getElementById('divValidationSummary').style.display='';";
            $arrayMsgErreur[] = Prado::localize('TEXT_CODE_CPV');
        } else {
            $scriptJs .= "document.getElementById('".$this->referentielCPV->getIdClientImgErrorCpv()."').style.display = 'none';";
        }
    }
}
