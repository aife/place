

<!--Debut Bloc Informations complementaires-->
<com:TPanel ID="summaryVisible" CssClass="form-bloc recap-infos-consultation">
	<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
	<div class="content">
		<div class="title-toggle" onclick="togglePanel(this,'infosComp');">&nbsp;</div>
		<div class="recap-bloc"><h2><com:TTranslate>DEFINE_INFORMATIONS_COMPLEMENTAIRES</com:TTranslate></h2>
		<div class="panel-toggle" style="display:none;" id="infosComp">
				<div class="breaker"></div>
				<com:TPanel id="panelDateMiseEnLigne" cssClass="line" visible="false">
					<div class="intitule-250 bold"><com:TTranslate>DEFINE_DATE_MISE_EN_LIGNE</com:TTranslate></div>
					<com:TLabel ID="dateMiseEnLigne"/>
				</com:TPanel>
				<div class="line">
					<div class="intitule-250 bold"><com:TTranslate>DEFINE_DATE_HEURE_LIMITE_DE_REMISE</com:TTranslate></div>
					<div class="content-bloc bloc-450"><com:TLabel ID="dateHeureLimiteRemise"/>
						<div class="picto-link inline indent-20">
							<com:PictoHistorique/>
							<a href="index.php?page=Agent.HistoriqueConsultation&id=<%=$this->getReference()%>" class="link">
								<com:TTranslate>DEFINE_TEXT_HISTORIQUE_ENCHERE</com:TTranslate>
							</a>
						</div>
					</div>
				</div>
				<com:TPanel cssclass="line" visible="<%=($this->getConsultation()->getDatefinLocale() != '0000-00-00 00:00:00' && ($this->getConsultation() instanceOf Application\Propel\Mpe\CommonConsultation)) ? true:false%>">
					<com:TPanel id="panelDateLimiteRemisePlisLocale" visible="true" CssClass="float-left" >
						<div class="intitule-250 bold"><com:TTranslate>TEXT_DATE_HEURE_LIMITE_REMISE_PLIS_LOCALE</com:TTranslate> : </div>
					</com:TPanel>
					<div class="content-bloc bloc-450"><com:TLabel id="dateFinLocale" /></div>
					<div class="breaker"></div>
				</com:TPanel>
				<com:TPanel cssclass="line" visible="<%=($this->getConsultation()->getLieuResidence()) ? true:false%>">
					<com:TPanel visible="true" CssClass="float-left" >
						<div class="intitule-250 bold"><com:TTranslate>DEFINE_LIEU_RESIDENCE</com:TTranslate> : </div>
					</com:TPanel>
					<div class="content-bloc bloc-450"><com:TLabel id="lieuResidence" /></div>
					<div class="breaker"></div>
				</com:TPanel>
				<div class="line">
					<div class="intitule-250 bold"><com:TTranslate>TEXT_ALLOTISSEMENT</com:TTranslate> : </div>
					<div class="content-bloc bloc-450"><com:TLabel ID="allotissement"/>
					&nbsp;&nbsp;&nbsp;&nbsp;
            		<com:THyperLink id="idLienLot" NavigateUrl="javascript:popUp('index.php?page=<%= Application\Service\Atexo\Atexo_Util::getTypeUserCalledForPradoPages() %>.PopUpDetailLots&orgAccronyme=<%=Application\Service\Atexo\Atexo_CurrentUser::getCurrentOrganism()%>&id=<%=$this->getReference()%>&lang=<%=$this->getLange()%>','yes')"><img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-details.gif"  alt="<%=Prado::localize('ICONE_VOIR_LE_DETAIL')%>" title="<%=Prado::localize('ICONE_VOIR_LE_DETAIL')%>" /></com:THyperLink>
						 <table>
	       	                 <com:TRepeater ID="lots" >
								<prop:ItemTemplate>
									<tr>
										<td><com:TLabel   Text=<%#Prado::localize('TEXT_LOT').' '.($this->Data->getLot())." : " %> /> </td>
										<td><com:TLabel   Text=<%#$this->Data->getDescriptionTraduite() %> /> </td>
									</tr>
								</prop:ItemTemplate>
							 </com:TRepeater>
	                        </table>
					</div>
				</div>
     			<com:TPanel ID="panelDomaineActivites" cssClass="line" >
					<div class="intitule-250 bold"><com:TTranslate>TEXT_DOMAINES_ACTIVITE</com:TTranslate> : </div>
					<div class="content-bloc bloc-450">
						<!-- ul class="default-list" -->
							<com:TLabel ID="domainesActivite" />
						<!-- /ul -->
					</div>
			   </com:TPanel>
			   <span class="spacer-small"></span>
			   <com:TPanel ID="adresseRetraitDossier" cssClass="line" >
					<div class="intitule-250 bold"><com:TTranslate>ADRESSE_RETRAIT_DOSSIERS</com:TTranslate> : </div>
					<div class="content-bloc bloc-450"><com:TLabel ID="adresseRetraitDossiers" />
						<div class="picto-link inline indent-20">
							<com:PictoHistorique/>
							<a href="index.php?page=Agent.HistoriqueConsultation&id=<%=$this->getReference()%>" class="link">
								<com:TTranslate>ICONE_HISTORIQUE</com:TTranslate>
							</a>
						</div>
					</div>
				</com:TPanel>
				<com:TPanel ID="panelAdresseDepotOffre" cssClass="line" Display="None">
					<div class="intitule-250 bold"><com:TTranslate>TEXT_ADRESSE_DEPOT_OFFRES</com:TTranslate> : </div>
					<div class="content-bloc bloc-450"><com:TLabel ID="adresseDepotOffres" />
						<div class="picto-link inline indent-20">
							<com:PictoHistorique/>
							<a href="index.php?page=Agent.HistoriqueConsultation&id=<%=$this->getReference()%>" class="link">
								<com:TTranslate>ICONE_HISTORIQUE</com:TTranslate>
							</a>
						</div>
					</div>
				</com:TPanel>
				<com:TPanel ID="panelLieuOuverturePlis" cssClass="line" >
					<div class="intitule-250 bold"><com:TTranslate>LIEU_OUVERTURE_PLIS</com:TTranslate> : </div>
					<div class="content-bloc bloc-450"><com:TLabel ID="lieuOuverturePlis" />
						<div class="picto-link inline indent-20">
							<com:PictoHistorique/>
							<a href="index.php?page=Agent.HistoriqueConsultation&id=<%=$this->getReference()%>" class="link">
								<com:TTranslate>ICONE_HISTORIQUE</com:TTranslate>
							</a>
						</div>
					</div>
				</com:TPanel>
				<span class="spacer-small" ></span>
				<com:TPanel ID="prixAcquisitionPlans" cssClass="line" >
					<div class="intitule-250 bold"><com:TTranslate>PRIX_ACQUISITION_DES_PLANS</com:TTranslate> : </div>
					<div class="content-bloc bloc-450"><com:TLabel ID="prixAcquisitionPlan" /></div>
				</com:TPanel>
				<com:TPanel ID="panelConsultationNonAlotti">
					<com:TPanel ID="panelCautionProvisoire" cssClass="line" >
							<div class="intitule-250 bold"><com:TTranslate>CAUTION_PROVISOIRE</com:TTranslate> : </div>
							<div class="content-bloc bloc-450"><com:TLabel ID="cautionProvisoire" />

							<div class="picto-link inline indent-20">
								<com:PictoHistorique/>
								<a href="index.php?page=Agent.HistoriqueConsultation&id=<%=$this->getReference()%>" class="link">
									<com:TTranslate>ICONE_HISTORIQUE</com:TTranslate>
								</a>
							</div>
						</div>
					</com:TPanel>

					<com:TPanel ID="panelQualification" cssClass="line" >
						<div class="intitule-250 bold"><com:TTranslate>TEXT_QUALIFICATIONS</com:TTranslate> : </div>
						<div class="content-bloc bloc-450">
							<com:TLabel ID="qualification" />
							<com:TPanel cssClass="picto-link" visible="false">
								<com:PictoHistorique/>
								<a href="index.php?page=Agent.HistoriqueConsultation&id=<%=$this->getReference()%>" class="link">
									<com:TTranslate>ICONE_HISTORIQUE</com:TTranslate>
								</a>
							</com:TPanel>
						</div>
					</com:TPanel>
					<com:TPanel ID="panelAgrements" cssClass="line" >
						<div class="intitule-250 bold"><com:TTranslate>TEXT_AGREMENT</com:TTranslate> : </div>
						<div class="content-bloc bloc-450">
							<com:TLabel ID="agrements" />
							<com:TPanel cssClass="picto-link" visible="false">
								<com:PictoHistorique/>
								<a href="index.php?page=Agent.HistoriqueConsultation&id=<%=$this->getReference()%>" class="link">
									<com:TTranslate>ICONE_HISTORIQUE</com:TTranslate>
								</a>
							</com:TPanel>
						</div>
					</com:TPanel>
					<!-- span class="spacer-small"></span -->
					<com:TPanel ID="panelEchantillons" cssClass="line" >
						<div class="intitule-250 bold"><com:TTranslate>ECHANTILLONS_DEMANDES</com:TTranslate> : </div>
						<div class="content-bloc bloc-450"><com:TLabel ID="labelDateHeureLimiteEchantillon" Text="<%=Prado::localize('DEFINE_DATE_HEURE_LIMITE')%> :"/><com:TLabel ID="dateEchantillons" />
							<br/><com:TLabel ID="adresseEchantillons" />
							<div class="picto-link">
								<com:PictoHistorique/>
								<a href="index.php?page=Agent.HistoriqueConsultation&id=<%=$this->getReference()%>" class="link">
									<com:TTranslate>ICONE_HISTORIQUE</com:TTranslate>
								</a>
							</div>
						</div>
					</com:TPanel>
					<com:TPanel ID="panelReunion" cssClass="line" >
						<div class="intitule-250 bold"><com:TTranslate>REUNION</com:TTranslate> : </div>
						<div class="content-bloc bloc-450"><com:TLabel ID="labelDateHeureReunion" Text="<%=Prado::localize('DEFINE_DATE_HEURE')%> :"/><com:TLabel ID="dateReunion" />
							 <br/><com:TLabel ID="adresseReunion" />
							 <div class="picto-link">
									<com:PictoHistorique/>
									<a href="index.php?page=Agent.HistoriqueConsultation&id=<%=$this->getReference()%>" class="link">
										<com:TTranslate>ICONE_HISTORIQUE</com:TTranslate>
									</a>
							 </div>
						</div>
					</com:TPanel>
					<com:TPanel ID="panelVisitesLieux" cssClass="line" >
						<div class="intitule-250 bold"><com:TTranslate>VISITE_DES_LIEUX</com:TTranslate> : </div>
						<com:TPanel ID="panelRepeaterVisitesLieux" cssClass="content-bloc bloc-450">
							 <com:TRepeater id="repeaterVisitesLieux">
							 	<prop:HeaderTemplate>
									<ul class="default-list">
								</prop:HeaderTemplate>
								<prop:ItemTemplate>
									<li><com:TLabel ID="dateVisites" Text="<%# Application\Service\Atexo\Atexo_Util::iso2frnDateTime($this->Data->getDate())%>"/>,&nbsp;<com:TLabel ID="adresseVisites" Text="<%#$this->Data->getAdresseTraduit()%>"/></li>
								</prop:ItemTemplate>
								<prop:FooterTemplate>
									</ul>
								</prop:FooterTemplate>
							 </com:TRepeater>
							  <div class="picto-link">
								<com:PictoHistorique/>
								<a href="index.php?page=Agent.HistoriqueConsultation&id=<%=$this->getReference()%>" class="link">
									<com:TTranslate>ICONE_HISTORIQUE</com:TTranslate>
								</a>
						 	</div>
						</com:TPanel>
						<com:TPanel ID="panelPasVisitelieu" cssClass="content-bloc bloc-450" visible="false">
							-
							 <div class="picto-link">
									<com:PictoHistorique/>
									<a href="index.php?page=Agent.HistoriqueConsultation&id=<%=$this->getReference()%>" class="link">
										<com:TTranslate>ICONE_HISTORIQUE</com:TTranslate>
									</a>
						     </div>
						</com:TPanel>
					</com:TPanel>

					<com:TPanel ID="panelVariante" cssClass="line">
						<div class="intitule-250 bold"><com:TTranslate>TEXT_VARIANTE</com:TTranslate> : </div>
						<div class="content-bloc bloc-450"><com:TLabel ID="varianteValeur" />
						<com:TPanel cssClass="picto-link inline indent-20" visible="false">
								<com:PictoHistorique/>
								<a href="index.php?page=Agent.HistoriqueConsultation&id=<%=$this->getReference()%>" class="link">
									<com:TTranslate>ICONE_HISTORIQUE</com:TTranslate>
								</a>
						</com:TPanel>
					  </div>
					</com:TPanel>


				</com:TPanel>
				<com:TPanel ID="panelDCE" CssClass="line">
					<div class="intitule-250 bold"><com:TTranslate>DEFINE_DCE</com:TTranslate> :</div>
					<div class="content-bloc bloc-450">
						<div class="picto-link inline">
							<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-compresse.gif" alt="<%=Prado::localize('TELECHARGEMENT_COMPLET')%>" title="<%=Prado::localize('DEFINE_TELECHARGER')%>" />
							<com:THyperLink ID="linkDownloadDce"></com:THyperLink>
							<com:TLabel ID="sizeDce"/>
						</div>
						<div class="picto-link inline indent-20" style="display:">
							<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-details-grey.gif" alt="<%=Prado::localize('DETAIL_PIECE')%>" title="<%=Prado::localize('DETAIL_PIECE')%>" />
							<com:THyperLink ID="linkDetailPiece" ><com:TTranslate>DEFINE_DETAIL_PIECES</com:TTranslate></com:THyperLink>
						</div>
						<div class="picto-link inline indent-20"><img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-historique.gif" alt="<%=Prado::localize('HISTORIQUE_MODIFICATION_DCE')%>" title="<%=Prado::localize('ICONE_HISTORIQUE')%>" />
							<com:THyperLink ID="linkHistorique" ><com:TTranslate>DEFINE_TEXT_HISTORIQUE_DCE</com:TTranslate></com:THyperLink>
						</div>
					</div>
				</com:TPanel>

				<com:TPanel ID="panelDCERestreint" CssClass="line" Visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('DceRestreint'))? true:false %>">
					<div class="intitule-250 bold"><com:TTranslate>ACCES_AU_DCE_ENTREPRISE</com:TTranslate> : </div>
					<div class="content-bloc bloc-450"><com:TLabel ID="accessCodeDCE"/></div>
				</com:TPanel>

				<com:TPanel ID="panelRC" CssClass="line" Visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('ReglementConsultation'))? true:false %>">
					<div class="intitule-250 bold"><com:TTranslate>REGELEMENT_CONS</com:TTranslate> :</div>
					<div class="content-bloc bloc-450">
						<div class="picto-link inline">
							<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-compresse.gif" alt="<%=Prado::localize('TELECHARGEMENT_COMPLET')%>" title="<%=Prado::localize('DEFINE_TELECHARGER')%>" />
							<com:THyperLink ID="linkDownloadReglement" >
							</com:THyperLink>
							<com:TLabel ID="sizeReglementConsultation"/>
						</div>

						<div class="picto-link inline indent-20">
							<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-historique.gif" alt="<%=Prado::localize('HISTORIQUE_MODIFICATION_DCE')%>" title="<%=Prado::localize('ICONE_HISTORIQUE')%>" />
							<a href="index.php?page=Agent.HistoriqueConsultation&id=<%=$this->getReference()%>"><com:TTranslate>DEFINE_TEXT_HISTORIQUE_ENCHERE</com:TTranslate></a>
						</div>

					</div>
				</com:TPanel>
				<com:TPanel ID="panelDumeDemmande" CssClass="line" Visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('InterfaceDume'))? true:false %>">
					<com:TRepeater id="listeDumeDemande">
						<prop:ItemTemplate>
					<div class="intitule-250 bold"><%#($this->itemIndex == 0)? Prado::localize('DUME_DEMANDE_ASSOCIES') : '&nbsp;' %></div>
					<div class="content-bloc bloc-450">
						<div class="picto-link inline">
							<a>- <com:TLabel id="numDume" text="<%#$this->Data->getNumeroDumeNational()%>" /></a> &nbsp;&nbsp;
							<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-compresse.gif" alt="<%=Prado::localize('TELECHARGEMENT_COMPLET')%>" title="<%=Prado::localize('DEFINE_TELECHARGER')%>" style="<%#($this->Data->getBlobId())?'':'display:none'%>"/>
							<com:TLinkButton ID="linkDownloadDumeDemande" text="<%=Prado::localize('DEFINE_TELECHARGER')%>" onclick="SourceTemplateControl.downloadDumeDemande" visible="<%# ($this->Data->getBlobId()) ? true : false %>" />
							<com:THiddenField id="idPdf" value="<%#$this->Data->getBlobId()%>"/>
							<com:THiddenField id="idDumeNumero" value="<%#$this->Data->getId()%>"/>
						</div>
					</div>
						</prop:ItemTemplate>
					</com:TRepeater>
				</com:TPanel>
				<com:TPanel CssClass="bloc-740"
							Visible="<%=($this->getConsultation()->getDumeDemande() == 1 && $this->getConsultation()->getAlloti() == 1) ? true : false %>"
				>
					<div class="bloc-message form-bloc-conf msg-info">
						<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
						<div class="content">
							<com:TTranslate>MSG_INFORMATIONS_PDF_NE_CONTIENT_PAS_LES_CRITERES_DE_LOTS</com:TTranslate>
							<div class="breaker"></div>
						</div>
						<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
					</div>
				</com:TPanel>
				<com:TPanel ID="panelAutresPieces" CssClass="line">
					<div class="intitule-250 bold"><com:TTranslate>AUTRE_PIECE</com:TTranslate>  :</div>
					<div class="content-bloc bloc-450">
						<div class="picto-link inline">
							<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-compresse.gif" alt="<%=Prado::localize('DEFINE_TELECHARGER')%>" title="<%=Prado::localize('DEFINE_TELECHARGER')%>" />
							<com:THyperLink ID="linkDownloadOtherPJ" />
							<com:TLabel ID="sizeAutresPiecesConsultation"/>
						</div>
					</div>
				</com:TPanel>
				<com:TPanel ID="panelDic" CssClass="line">
					<div class="intitule-250 bold"><com:TTranslate>DOSSIER_INTEGRALE_CONSULTATION</com:TTranslate> :</div>
					<div class="content-bloc bloc-500">
						<com:TConditional condition="Application\Service\Atexo\Atexo_Config::getParameter('SPECIFIQUE_PLACE')">
							<prop:trueTemplate>
								<div class="picto-link inline">
									<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-compresse.gif" alt="<%[TEXT_TELECHARGEMENT_COMPLET]%>" title="<%=Prado::localize('TELECHARGER_DOSSIER_INTEGRAL_CONSULTATION')%>" />
									<com:THyperLink
											ID="linkDownloadDossierIntegrale"
											Text = "<%=Prado::localize('DEFINE_TELECHARGER')%>"
									/>
									<com:TLabel ID="sizeDossierIntegrale"/>
								</div>
							</prop:trueTemplate>
							<prop:falseTemplate>
								<div class="picto-link inline">
									<com:TActiveLinkButton
											cssClass="bouton-moyen  msg-confirmation-envoi"
											Attributes.title="<%=Prado::localize('DEFINE_ACCEDER_ARCHIVE_GENEREES')%>"
											onCallBack="TemplateControl.preparerTelechargementAsynchrone"
									>
										<prop:ClientSide
												OnLoading="console.log('loading')"
												OnComplete="confirmationArchive();" />
										<%=Prado::localize('DEFINE_GENERE_DIC')%>
									</com:TActiveLinkButton>
									&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-compresse.gif"
										 alt="<%[TEXT_TELECHARGEMENT_COMPLET]%>"
										 title="<%=Prado::localize('TELECHARGER_DOSSIER_INTEGRAL_CONSULTATION')%>" />
									<com:THyperLink
										ID="linkDownloadDossierIntegraleDIC"
										Text = "<%=Prado::localize('DEFINE_ACCEDER_ARCHIVE_GENEREES')%>"
										 />
									<com:TLabel ID="sizeDossierIntegraleDIC"/>
								</div>
							</prop:falseTemplate>
						</com:TConditional>
					</div>
				</com:TPanel>
								<com:TPanel ID="panelDac" CssClass="line">
					<div class="intitule-250 bold"><com:TTranslate>DOSSIER_ARCHIVE_CONSULTATION</com:TTranslate> :</div>
					<div class="content-bloc bloc-500">
						<div class="picto-link inline">
							<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-compresse.gif" alt="<%[TEXT_TELECHARGEMENT_COMPLET]%>" title="<%=Prado::localize('DOSSIER_ARCHIVE_CONSULTATION')%>" />
							<com:THyperLink
								ID="linkDownloadDossierArchive"
								Text = "<%=Prado::localize('DEFINE_TELECHARGER')%>"
								 />
						</div>
					</div>
				</com:TPanel>
			<com:TPanel ID="panelTncp" cssClass="line" visible="false">
				<div class="title-toggle" onclick="togglePanel(this,'infosTiers');" style="margin-top: 5px;">&nbsp;</div>
				<div class="intitule-400 bold"><com:TTranslate>DEFINE_ACHETEUR_TIERS_STATUS</com:TTranslate></div>
				<div class="panel-toggle" style="display:none;" id="infosTiers">
					<div class="line">
						<div class="intitule-230 bold"><com:TTranslate>DEFINE_DATE_PUBLICATION</com:TTranslate></div> <com:TLabel ID="statusPublication"/>
					</div>
					<div class="line">
						<div class="intitule-230 bold"><com:TTranslate>DEFINE_DATE_DLRO</com:TTranslate></div> <com:TLabel ID="statusDlro"/>
					</div>
					<div class="line">
						<div class="intitule-230 bold"><com:TTranslate>DEFINE_DATE_DCE</com:TTranslate></div> <com:TLabel ID="statusDce"/>
					</div>
				</div>
			</com:TPanel>
				<div class="line">
					<div class="intitule-250 bold"><com:TTranslate>DEFINE_ENTITE_ACHAT_RATTACHEMENT</com:TTranslate></div> <com:TLabel ID="entiteAchatRattachement"/>
				</div>

				<div class="line">
					<div class="intitule-250 bold"><com:TTranslate>DEFINE_ENTITE_ACHAT_ASSOCIEE</com:TTranslate></div> <com:TLabel ID="entiteAchatAssociee"/>
				</div>

				<div class="line">
					<div class="intitule-250 bold"><com:TTranslate>TEXT_COMENTAIRE_INTERNE</com:TTranslate> :</div>
					<div class="content-bloc bloc-450"><com:TLabel ID="commantaireInterne"/></div>
				</div>

				<div class="line">
					<div class="intitule-250 bold"><com:TTranslate>TEXT_ACCES_PUBLIC_RESTREINT</com:TTranslate> :</div>
					<div class="content-bloc bloc-450 ow-anywhere"><com:TLabel ID="restreintPublique"/><br /><com:TLabel ID="urlDirecteConsultation"/></div>
				</div>

				<com:TPanel ID="panelDonneeComplementaire1" CssClass="spacer-small" Visible="false"></com:TPanel>
				<com:TPanel ID="panelDonneeComplementaire2" Cssclass="picto-link inline indent-10" Visible="false">
					<com:TImage ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-acces-arrow-blue.gif" Attributes.alt="" /><a href="index.php?page=Agent.DonneesComplementairesConsultation&id=<%=$this->getReference()%>"><com:TTranslate>FCSP_ACCES_DONNEES_COMPLEMENTAIRES_CONSULTATIONS</com:TTranslate></a>
				</com:TPanel>

				<div class="breaker"></div>
		</div>
		</div>
	</div>
	<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>

<script>
	J('#modal_demande_telechargement_confirmation').modal('hide');
 function confirmationArchive() {
	 J('#modal_demande_telechargement_confirmation').css({ 'z-index': 100000 });
	 J('#modal_demande_telechargement_confirmation').modal('show');
	 J(".modal-backdrop").css({ opacity: 0.5 });
 }
</script>



</com:TPanel>
				<!--Fin Bloc Informations complementaires-->






