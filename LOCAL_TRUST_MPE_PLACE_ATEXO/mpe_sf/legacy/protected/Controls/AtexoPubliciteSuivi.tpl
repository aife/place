<com:TActivePanel id="publiciteSuivi" >
    <com:TActivePanel id="panelBlocErreur" Display="None">
        <com:PanelMessageErreur ID="messageErreur" Message="<%=Prado::localize('CONCENTRATEUR_SERVICE_INDISPONIBLE')%>"/>
    </com:TActivePanel>
    <com:TActivePanel id="panelConcentrateur" Display="None">
        <link rel="stylesheet" href="<%=$this->getUrlConcentrateur()%>concentrateur-annonces/annonce-suivi/annonceSuivi.css">
        <div id="annonce-suivi">
        </div>
    </com:TActivePanel>
    <com:TActiveLabel id="scriptJs"/>
</com:TActivePanel>
