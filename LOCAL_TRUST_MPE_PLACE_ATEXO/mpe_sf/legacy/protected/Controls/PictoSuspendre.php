<?php

namespace Application\Controls;

use Application\Service\Atexo\Controller\Atexo_Controller_Front;
use Prado\Prado;
use Prado\Web\UI\ActiveControls\TActiveImageButton;

/**
 * commentaires.
 *
 * @author Mouslim MITALI<mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PictoSuspendre extends TActiveImageButton
{
    public bool $_activate = false;

    public function onLoad($param)
    {
        if (false == $this->_activate) {
            $this->setImageUrl(Atexo_Controller_Front::t().'/images/picto-suspendre.gif');
            $this->Attributes->onclick = 'return false';
        } else {
            $this->setImageUrl(Atexo_Controller_Front::t().'/images/picto-suspendre.gif');
            $this->setAlternateText(Prado::localize('SUSPENDRE'));
            $this->setToolTip(Prado::localize('SUSPENDRE'));
        }
    }

    public function getActivate()
    {
        return $this->_activate;
    }

    public function setActivate($bool)
    {
        $this->_activate = $bool;
    }
}
