<?php

namespace Application\Controls;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonTDumeContextePeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_MpeSf;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Lots;
use Application\Service\Atexo\Dume\Atexo_Dume_AtexoDume;
use Prado\Prado;

/**
 * Page de gestion des lots de la consultation.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 4
 */
class DetailsLotsConsultation extends MpeTTemplateControl
{
    protected $_refConsultation;
    protected $_consultation;
    protected $mode = null;
    protected $lots = null;

    public function setConsultation($value)
    {
        $this->_consultation = $value;
    }

    public function getConsultation()
    {
        if (!($this->_consultation instanceof CommonConsultation)) {
            $this->_consultation = $this->Page->getConsultation();
        }

        return $this->_consultation;
    }

    /**
     * recupere le mode.
     */
    public function getMode()
    {
        return $this->mode;
    }

    // getMode()

    /**
     * Affecte le mode.
     *
     * @param string $mode
     */
    public function setMode($mode)
    {
        if ($this->mode !== $mode) {
            $this->mode = $mode;
        }
    }

    // setMode()

    /**
     * recupere le lot.
     */
    public function getLots()
    {
        return $this->lots;
    }

    // getLots()

    /**
     * Affecte les lots.
     *
     * @param array $lots
     */
    public function setLots($lots)
    {
        if ($this->lots !== $lots) {
            $this->lots = $lots;
        }
    }

    // setLots()

    public function onLoad($param)
    {
        if (!$this->Page->IsCallback) {
            if ($this->getConsultation()) {
                $this->fillRepeaterListeLots();
                $this->chargerComposant();
            }
        }

        $this->_consultation = $this->getConsultation();
    }

    /*
     * permet de remplir le repeater des lot
     */
    public function fillRepeaterListeLots()
    {
        $consultation = $this->getConsultation();

        if (($consultation instanceof CommonConsultation) && $consultation->getAlloti()) {
            $lots = [];
            //$this->PanelListeLots->setVisible(true);

            if (is_array($consultation->getAllLots()) && count($consultation->getAllLots())) {
                $lots = $consultation->getAllLots();
            } elseif (is_array($this->getLots()) && count($this->getLots())) {
                $lots = $this->getLots();
            }

            $this->tableauDesLots->DataSource = $lots;
            $this->tableauDesLots->DataBind();
            $this->setViewState('nombreDesLots', count($lots));

            foreach ($this->tableauDesLots->Items as $item) {
                //DUME
                if (Atexo_Module::isEnabled('InterfaceDume') && 1 == $this->getConsultation()->getDumeDemande()) {
                    $item->delteLotButton->Visible = false;
                } else {
                    $item->newDeleteLotButton->Visible = false;
                }
            }

            if (Atexo_Module::isEnabled('InterfaceDume') && 1 == $this->getConsultation()->getDumeDemande()) {
                $this->delteAllLotsButton->Visible = false;
            } else {
                $this->newDeleteAllLotsButton->Visible = false;
            }
        }
    }

    /*
     * permet de supprimer un lot
     */
    public function deleteLot($sender, $param)
    {
        $consultation = $this->getConsultation();

        if (Atexo_Module::isEnabled('InterfaceDume') && 1 == $consultation->getDumeDemande()) {
            $idLot = $param->CallbackParameter;

            if (null == $idLot) {
                $idLot = $this->deleteLotDume->getValue();
            }
        } else {
            $idLot = $param->CallbackParameter;
        }

        if ($consultation instanceof CommonConsultation) {
            $categorieLot = (new Atexo_Consultation_Lots())
                ->retrieveCategorieLot($consultation->getId(), (int) $idLot, $consultation->getOrganisme());

            if ($categorieLot) {
                /**
                 * Suppression des clause pour la suppression d'un lot
                 */
                (new Atexo_Consultation_Lots())->deleteClausesForLot(
                    $consultation->getId(),
                    $categorieLot->getId()
                );

                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB')
                    . Atexo_Config::getParameter('CONST_READ_WRITE'));
                $categorieLot->delete($connexion);

                //DUME
                if (Atexo_Module::isEnabled('InterfaceDume') && 1 == $consultation->getDumeDemande()) {
                    $this->checkPurge($consultation, $connexion);

                    Atexo_Dume_AtexoDume::createOrUpdateDumeContexte($consultation, $idLot);

                    $this->getPage()->scriptJs->text = "<script>J('.demander-purge').dialog('close');</script>";
                }
            }
        }

        $this->fillRepeaterListeLots();
        $this->PanelListeLots->render($param->getNewWriter());
    }

    /*
     * Permet de faire refresh
     */
    public function refreshRepeater($sender, $param)
    {
        $this->fillRepeaterListeLots();
        $this->chargerComposant();
        $this->PanelListeLots->render($param->getNewWriter());
    }

    public function getNombreDesLots()
    {
        return $this->getViewState('nombreDesLots');
    }

    public function chargerComposant()
    {
        if ($this->getConsultation()) {
            $this->fillRepeaterListeLots();
            (new Atexo_MpeSf())->consultationsAutorisees($this->getConsultation()->getId());
            $this->lienAjouterLot->NavigateUrl = "javascript:popUp('index.php?page=Agent.PopupAjoutLot&orgAccronyme=" . $this->getConsultation()->getOrganisme() . '&id=' . $this->getConsultation()->getId() . '&idButtonRefresh=' . $this->refreshPanel->getClientId() . " ','yes');";

            if (!$this->getConsultation()->getDonneeComplementaireObligatoire()) {
                $this->lienImportLot->NavigateUrl = "javascript:popUpOpen('agent/consultation/lot/import_lot?acronyme=" . $this->getConsultation()->getOrganisme() . '&consultation=' . $this->getConsultation()->getId() . '&idButtonRefresh=' . $this->refreshPanel->getClientId() . " ', 'newWin', 'toolbar=no,menubar=no,location=no,width=900,height=480,scrollbars=yes');";
            }

            $this->lienDetailLots->NavigateUrl = "javascript:popUp('index.php?page=Agent.PopupDetailLot&orgAccronyme=" . $this->getConsultation()->getOrganisme() . '&id=' . $this->getConsultation()->getId() . "','yes');";
        }
    }

    public function save(&$consultation)
    {
    }

    /**
     * permet de valider les donnees de l'etape detailLots.
     *
     * @params : $consultation : la consultation,$isValid,$arrayMsgErreur
     */
    public function validerDonneesDetailLotConsultation($consultation, &$isValid, &$arrayMsgErreur)
    {
        if (($consultation instanceof CommonConsultation) && $consultation->getAlloti()) {
            if (is_array($consultation->getAllLots()) && count($consultation->getAllLots())) {
                $lots = $consultation->getAllLots();
                $valide = true;
                $messages = [];

                foreach ($lots as $lot) {
                    if (Atexo_Module::isEnabled('ConsultationClause')) {
                        if (!$lot->getClauseSociale()) {
                            $valide = false;
                            $messages[0] = Prado::localize('DETAIL_LOT_CLAUSES_SOCIALES_NON_RENSEIGNER');
                        }

                        if (!$lot->getClauseEnvironnementale()) {
                            $valide = false;
                            $messages[1] = Prado::localize('DETAIL_LOT_CLAUSES_ENVIRONNEMENTALES_NON_RENSEIGNER');
                        }
                    }
                }

                if (!$valide) {
                    $isValid = false;

                    foreach ($messages as $message) {
                        $arrayMsgErreur[] = $message;
                    }
                }
            }
        }
    }

    /**
     * Permet de supprrimer tous les lots.
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2017-develop
     *
     * @copyright Atexo 2018
     */
    public function deleteAllLots()
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB')
            . Atexo_Config::getParameter('CONST_READ_WRITE'));
        $consultation = $this->getConsultation();

        if ($consultation instanceof CommonConsultation) {
            // Ici on vide tout les critère (social et environ)
            (new Atexo_Consultation_Lots())->deleteCommonLots(
                $consultation->getOrganisme(),
                $consultation->getId(),
                $connexion
            );

            (new Atexo_Consultation_Lots())->deleteLots($consultation->getId(), $consultation->getOrganisme());

            //DUME
            if (Atexo_Module::isEnabled('InterfaceDume') && 1 == $consultation->getDumeDemande()) {
                $this->checkPurge($consultation, $connexion, 'deleteAll');

                Atexo_Dume_AtexoDume::createOrUpdateDumeContexte($consultation);

                if (
                    !(Atexo_Module::isEnabled('InterfaceDume')
                    && 1 == $this->_consultation->getDumeDemande())
                ) {
                    $this->getPage()->scriptJs->text = "<script>J('.demander-purge-all').dialog('close');</script>";
                }
            }
        }
    }

    /**
     * @param $consultation
     * @param $lot
     * @param $connexion
     *
     * @return mixed
     */
    public function checkPurge($consultation, $connexion, $lot = null)
    {
        $c = new Criteria();
        $c->add(CommonTDumeContextePeer::ORGANISME, $consultation->getOrganisme());
        $c->add(CommonTDumeContextePeer::CONSULTATION_ID, $consultation->getId());
        $c->add(CommonTDumeContextePeer::TYPE_DUME, Atexo_Config::getParameter('TYPE_DUME_ACHETEUR'));
        $tDumeContexte = CommonTDumeContextePeer::doSelectOne($c, $connexion);

        if (null != $tDumeContexte) {
            return Atexo_Dume_AtexoDume::checkPurge($consultation, $tDumeContexte->getContexteLtDumeId(), $lot);
        }
    }

    /**
     * @param $sender
     * @param $param
     */
    public function openModalDume($sender, $param)
    {
        //DUME
        if (Atexo_Module::isEnabled('InterfaceDume') && 1 == $this->_consultation->getDumeDemande()) {
            $connexion = Propel::getConnection(
                Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE')
            );

            $checkPurge = $this->checkPurge($this->_consultation, $connexion, $param->CallbackParameter);

            if ($checkPurge['statut'] == Atexo_Config::getParameter('CHECK_PURGE_STATUS_PURGE')) {
                $this->deleteLotDume->value = $param->CallbackParameter;
                $this->deleteLotDume->dataBind();

                $this->getPage()->scriptJs->text = "<script>openModal('demander-purge', 'popup-small2', document.getElementById('panelValidationCheckPurgeDelete'), 'Demande de validation');</script>";
            } else {
                $this->deleteLot($sender, $param);
            }
        }
    }

    /**
     * @param $sender
     * @param $param
     */
    public function openModalDumeAll($sender, $param)
    {
        //DUME
        if (Atexo_Module::isEnabled('InterfaceDume') && 1 == $this->_consultation->getDumeDemande()) {
            $connexion = Propel::getConnection(
                Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE')
            );

            $checkPurge = $this->checkPurge($this->_consultation, $connexion, 'deleteAll');

            if ($checkPurge['statut'] == Atexo_Config::getParameter('CHECK_PURGE_STATUS_PURGE')) {
                $this->deleteLotDume->value = $param->CallbackParameter;
                $this->deleteLotDume->dataBind();

                $this->getPage()->scriptJs->text = "<script>openModal('demander-purge-all', 'popup-small2', document.getElementById('panelValidationCheckPurgeDeleteAll'), 'Demande de validation');</script>";
            } else {
                $this->deleteAllLots();
                $this->refreshRepeater($sender, $param);
            }
        }
    }
}
