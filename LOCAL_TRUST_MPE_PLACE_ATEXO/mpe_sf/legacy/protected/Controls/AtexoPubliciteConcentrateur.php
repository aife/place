<?php

namespace Application\Controls;

use App\Exception\ConnexionWsException;
use App\Service\ConcentrateurManager;
use App\Service\Publicite\EchangeConcentrateur;
use Application\Propel\Mpe\CommonAdresseFacturationJalQuery;
use Application\Propel\Mpe\CommonAgentQuery;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonTDonneeComplementaire;
use Application\Propel\Mpe\CommonTypeAvisPubProcedureQuery;
use Application\Propel\Mpe\CommonTypeAvisPubQuery;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Jal;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_MultiDomaine;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Config\Atexo_Config_Exception;
use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2;
use Exception;
use Prado\Prado;

/**
 * Class AtexoPubliciteConcentrateur.
 */
class AtexoPubliciteConcentrateur extends MpeTTemplateControl
{
    /**
     * @var CommonConsultation
     */
    private $consultation;

    /**
     * @var string
     */
    private $token;

    /**
     * @var string
     */
    private $xml;

    /**
     * @return CommonConsultation
     */
    public function getConsultation()
    {
        return $this->Page->getViewState('consultation');
    }

    /**
     * @param mixed $consultation
     */
    public function setConsultation($consultation)
    {
        $this->consultation = $consultation;
    }

    public function getDepartements(CommonConsultation $cons): false|string
    {
        $lieuExecution = $cons->getLieuExecution();
        $arIds = explode(',', $lieuExecution);
        $geolocalisations = Atexo_Geolocalisation_GeolocalisationN2::retrieveGeolocalisationN2byIds($arIds);
        $departements = [];
        foreach ($geolocalisations as $geolocalisation) {
            $departements[] = $geolocalisation->getDenomination2();
        }

        return json_encode($departements, JSON_THROW_ON_ERROR);
    }

    /**
     * @param null $param
     *
     * @throws Atexo_Config_Exception
     */
    public function charger($param = null)
    {
        $logger = Atexo_LoggerManager::getLogger('publicite');
        $cons = $this->getConsultation();
        if ($cons instanceof CommonConsultation) {
            try {
                $urlPf = $this->getUrlConcentrateur();
                $this->Page->scriptJs->text = <<<SCRIPT
                    <script>
                    var paramsConcentrateur = [];
                    var scriptsList = [
                        'concentrateur-annonces/consultation-formulaire/consultationFormulaire.umd.js',
                        'concentrateur-annonces/consultationFormulaireVue.js'
                    ];
                     chargerAnnoncesConcentrateurConsultationFormulaire('$urlPf', scriptsList);
                    </script>
                    SCRIPT;
                $this->panelConcentrateur->setDisplay('Dynamic');
                $this->panelBlocErreur->setDisplay('None');
            } catch (\Exception $exception) {
                $logger->error(
                    'Probleme lors recuperation du token Concentrateur'
                    . $exception->getMessage()
                    . ' ' . $exception->getTraceAsString()
                );
                $this->panelConcentrateur->setDisplay('None');
                $this->panelBlocErreur->setDisplay('Dynamic');
            }
        }
        if ($param) {
            $this->bloc_PubliciteConsultationConcentrateurV2->render($param->getNewWriter());
        }
    }

    /**
     * @return string
     */
    public function getHtml()
    {
        $res = '';
        $cons = $this->getConsultation();
        $logger = Atexo_LoggerManager::getLogger('publicite');
        $serviceSF = Atexo_Util::getSfService(EchangeConcentrateur::class);
        if ($cons instanceof CommonConsultation) {
            try {
                $concentrateurService = Atexo_Util::getSfService(ConcentrateurManager::class);
                $accesToken = $concentrateurService->addAgent($cons);
                $data = [];
                if ($cons instanceof CommonConsultation) {
                    $data['europeen'] = (bool) $this->getEuropeanPub($cons);
                    $data['blocList'] = (new AtexoPubliciteSuivi())->getConfigPub($cons);
                    $data['facturationList'] = $this->getFacturations($cons);
                }

                $concentrateurService->configAnnonces($cons, $accesToken, $data);
                $traductions = $this->getTraductions();
                $uidPf = Atexo_Config::getParameter('UID_PF_MPE');
                $uidConsultation = $cons->getId();
                $res = <<<EOD
                <consultation-formulaire
                    plateforme='$uidPf'
                    id-consultation='$uidConsultation'
                    access-token='$accesToken'
                    traductions='$traductions'
                     />
            EOD;
                $this->panelConcentrateur->setDisplay('Dynamic');
                $this->panelBlocErreur->setDisplay('None');
            } catch (\Exception $exception) {
                $logger->error(
                    'Probleme lors recuperation du token Concentrateur'
                    . $exception->getMessage()
                    . ' ' . $exception->getTraceAsString()
                );
                $this->panelConcentrateur->setDisplay('None');
                $this->panelBlocErreur->setDisplay('Dynamic');
            }
        } else {
            $this->panelConcentrateur->setDisplay('None');
            $this->panelBlocErreur->setDisplay('Dynamic');
        }

        return $res;
    }

    /**
     * @return string|string[]
     */
    public function getUrlConcentrateur(): string|array
    {
        return Atexo_MultiDomaine::replaceDomain(Atexo_Config::getParameter('PF_URL_REFERENCE'));
    }

    public function getTraductions(): false|string
    {
        return json_encode([
            'SELECTIONNER_SUPPORT_PUBLICITE_CONSULTATION' => base64_encode(
                Prado::localize('SELECTIONNER_SUPPORT_PUBLICITE_CONSULTATION')
            ),
            'SUPPORT_PUBLICATION' => base64_encode(Prado::localize('SUPPORT_PUBLICATION')),
            'TYPE_AVIS_OFFRE' => base64_encode(Prado::localize('TYPE_AVIS_OFFRE')),
            'DEFINE_ACTIONS' => base64_encode(Prado::localize('DEFINE_ACTIONS')),
            'SELECTIONNEZ_TYPE_AVIS' => base64_encode(Prado::localize('SELECTIONNEZ_TYPE_AVIS')),
        ], JSON_THROW_ON_ERROR);
    }

    private function getFacturations(CommonConsultation $cons): array
    {
        if (!$cons->getId()) {
            return [];
        }

        $addresses = (new CommonAdresseFacturationJalQuery())->getAddressesByOrganismAndService(
            $cons->getOrganisme(),
            $cons->getServiceId()
        );
        $addressList = [];

        foreach ($addresses as $address) {
            $addressList[] = [
                'address' => $address->getInformationFacturation(),
                'sip' => $address->getFacturationSip() ? 'true' : 'false',
                'email' => $address->getEmailAr()
            ];
        }

        return $addressList;
    }

    private function getEuropeanPub(CommonConsultation $cons): string
    {
        if ($cons->getIdTypeProcedure() && $cons->getOrganisme()) {
            $idTypeAvis = (new CommonTypeAvisPubProcedureQuery())->getTypeAvisByProcedureAndOrganisme(
                $cons->getIdTypeProcedure(),
                $cons->getOrganisme()
            )?->getIdTypeAvis();
            $typeAvis = (new CommonTypeAvisPubQuery())->findOneById($idTypeAvis);
            if (1 === $typeAvis?->getTypePub()) {
                return '1';
            }

            return '0';
        }

        return '0';
    }
}
