<div class="spacer-small"></div>
<com:TActivePanel ID="panelVisiteLieux" cssClass="line" Enabled="<%=($this->getActif())? true:false%>">
	<div class="intitule-150"><label for="visite_cons"><com:TTranslate>VISITE_DES_LIEUX</com:TTranslate></label><span class="champ-oblig">*</span> :</div>
	<div class="content-bloc bloc-600">
			<div class="line">
				<com:TActiveRadioButton 
					GroupName="visiteLot" 
					Checked="true" 
					ID="visiteLotNon" 
					Text="<%=Prado::localize('TEXT_NON')%>"
					Attributes.onclick="hideDiv('<%=$this->visiteLayerLot->getClientId()%>');" Attributes.title="<%=Prado::localize('TEXT_NON')%>"/>
			</div>
			<div class="line">
				<com:TActiveRadioButton 
					 GroupName="visiteLot" 
					 ID="visiteLotOui"
					 Text="<%=Prado::localize('FCSP_OUI')%>"
					 Attributes.onclick="showDiv('<%=$this->visiteLayerLot->getClientId()%>');" 
					 Attributes.title="<%=Prado::localize('FCSP_OUI')%>"/>
					 <span id="spanErrorVisite" style="display:none" title='Champ obligatoire' ><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>
				 <com:TCustomValidator
					ClientValidationFunction="hasVisiteLieuxCons"
					ID="validatorVisiteDesLieux" 
					ControlToValidate="visiteLotOui"
					ValidationGroup="<%=($this->getValidationGroup())%>" 
					Display="Dynamic" 
					ErrorMessage="<%=Prado::localize('VISITE_DES_LIEUX')%>"
					EnableClientScript="true"  						 					 
		 			Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
					<prop:ClientSide.OnValidationError>
		 				document.getElementById('divValidationSummary').style.display='';
		 				if(document.getElementById('ctl0_CONTENU_PAGE_blocBoutons'))
	 						document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
					</prop:ClientSide.OnValidationError>
 				</com:TCustomValidator>
 				 <com:TCustomValidator
					ClientValidationFunction="dateVisiteLieuxCompare"
					ID="validatorVisiteDesLieuxDateCompare" 
					ControlToValidate="visiteLotOui"
					ValidationGroup="<%=($this->getValidationGroup())%>" 
					Display="Dynamic" 
					ErrorMessage="<%=Prado::localize('DATE_VISITE_LIEUX_POSTERIEUR_DATE_FIN')%>"
					EnableClientScript="true"  						 					 
		 			Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
					<prop:ClientSide.OnValidationError>
		 				document.getElementById('divValidationSummary').style.display='';
		 				if(document.getElementById('ctl0_CONTENU_PAGE_blocBoutons'))
	 				    	document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
					</prop:ClientSide.OnValidationError>
 				</com:TCustomValidator>
 				<com:TActiveHiddenField id="hiddenVisitesLieux" value="<%=$this->getClientId()%>"/>
 				<com:TCustomValidator
					ClientValidationFunction="validateDateVisites"
					ID="validatorVisiteDesLieuxDateCompareDate" 
					ControlToValidate="hiddenVisitesLieux"
					ValidationGroup="<%=($this->getValidationGroup())%>"
					Display="Dynamic" 
					ErrorMessage="<%=Prado::localize('DATE_VISITE_LIEUX_POSTERIEUR_AUJOURDHUI')%>"
					EnableClientScript="true"  	
					visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('validationFormatChampsStricte'))?'true' : 'false'%>"
		 			Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
					<prop:ClientSide.OnValidationError>
		 				document.getElementById('divValidationSummary').style.display='';
		 				if(document.getElementById('ctl0_CONTENU_PAGE_blocBoutons'))
	 						document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
					</prop:ClientSide.OnValidationError>
 				</com:TCustomValidator>
			</div>
		<com:TActivePanel ID="visiteLayerLot" Style="display:none;">
			<com:TActiveTextBox Display="None" ID="infoVisite" />
			<com:TActiveTextBox Display="None" ID="addVisite" />
			
			<com:TActiveButton 
				  Display="None"
				  ID="infoVisiteButton"
	              OnCallBack="onCallBackLieuxVisiteCons" />
			<div class="table-bloc">
			    <table class="table-results" summary="Visites des lieux">
				 	<com:TRepeater ID="visiteRepeater">
                     	<prop:HeaderTemplate>
							<caption><com:TTranslate>VISITE_DES_LIEUX</com:TTranslate></caption>
							<thead>
							<tr>
								<th class="top" colspan="3"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></th>
							</tr>
							<tr>
								<th class="col-350" id="nomDirigeant"><com:TTranslate>TEXT_ADRESSE</com:TTranslate></th>
								<th class="col-150" id="prenomDirigeant"><com:TTranslate>TEXT_DATE_HEURE</com:TTranslate></th>
								<th class="actions" id="suppDirigeant"><com:TTranslate>TEXT_ACTIONS</com:TTranslate></th>
							</tr>
						</thead>
						</prop:HeaderTemplate>
                        <prop:ItemTemplate>
							<tr class="<%#(($this->ItemIndex%2==0)? '':'on')%>"> 
								<td class="col-350" headers="nomDirigeant"><%#$this->Data['adresse']%>
									<com:THiddenField ID="adresseValue" Value="<%#$this->Data['adresse']%>"/>
								 </td>
								<td class="col-150" headers="prenomDirigeant"><%#Application\Service\Atexo\Atexo_Util::iso2frnDateTime($this->Data['date'])%>
									<com:THiddenField ID="dateValue" Value="<%#$this->Data['date']%>"/>
								</td>
								<com:AtexoHiddenLangue id="visiteLieuxLang"    />
								<td class="actions" headers="suppDirigeant">
								<com:PictoEdit  
        							Clickable="true" 
        							Attributes.onClick="javascript:new_window('index.php?page=Agent.PopupAjoutLieuxVisites&clientId=<%=$this->getClientId()%>&data=<%#base64_encode($this->Data['adresse'].'#_#'.$this->Data['date'].'#_#'.$this->Data['id'])%>','yes');return false;"
        							Activate="true" 
        							AlternateText="<%=Prado::localize('DEFINE_TEXT_MODIFIER')%>"
        							Attributes.title="<%=Prado::localize('DEFINE_TEXT_MODIFIER')%>" />
								
								<com:PictoDeleteActive
        							OnCommand="SourceTemplateControl.deleteVisiteLieuxCons" 
        							onCallBack="SourceTemplateControl.onCallBackDeleteVisiteCons" 
        							CommandName = "<%#$this->Data['id']%>"
        							AlternateText="<%=Prado::localize('TEXT_SUPPRIMER')%>"
        							Attributes.onclick='javascript:if(!confirm("<%=Prado::localize('TEXT_ALERTE_SUPPRESSION_VISITE_LIEUX')%>"))return false;'
                					/>
								</td>
							</tr>
						</prop:ItemTemplate>
					</com:TRepeater>
				</table>
				<com:TActiveHyperLink 
					Visible="<%=($this->getActif())? true:false%>"
					Text="<%=Prado::localize('AJOUTER_UNE_VISITE')%>"
					Attributes.href="javascript:new_window('index.php?page=Agent.PopupAjoutLieuxVisites&clientId=<%=$this->getClientId()%>','yes');"
					cssClass="ajout-el float-left" />
			</div>
		</com:TActivePanel>
	</div>
</com:TActivePanel>
