<com:TConditional condition="$this->getAffichage() == 1">
    <prop:trueTemplate>
        <%include Application.templates.responsive.AtexoSoumissionnaire %>
    </prop:trueTemplate>
    <prop:falseTemplate>
        <%include Application.templates.mpe.AtexoSoumissionnaire %>
    </prop:falseTemplate>
</com:TConditional>