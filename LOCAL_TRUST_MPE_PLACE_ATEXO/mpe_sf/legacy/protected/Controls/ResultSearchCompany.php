<?php

namespace Application\Controls;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonEchangePeer;
use Application\Propel\Mpe\EntreprisePeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_Entreprise;
use Application\Service\Atexo\Atexo_GenerationExcel;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_ValeursReferentielles;
use Application\Service\Atexo\DonneesComplementairesEntreprise\Atexo_DonneesComplementairesEntreprise_DonneesComplementairesEntreprise;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_CriteriaVo;
use PDO;
use Prado\Prado;

/**
 * commentaires.
 *
 * @author mouslim MITALI <mouslim.MITALI@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class ResultSearchCompany extends MpeTPage
{
    public string $_calledFrom = '';

    public function setCalledFrom($value)
    {
        $this->_calledFrom = $value;
    }

    public function getCalledFrom()
    {
        return $this->_calledFrom;
    }

    public function fillRepeaterWithDataForSearchResult($criteriaVo)
    {
        $this->Page->panelSearch->setVisible(false);
        $this->Page->panelResultSerch->setVisible(true);
        $this->panelMessageAvertTraduction->setVisible(false);
        $entreprise = new Atexo_Entreprise();
        $nombreElement = $entreprise->searchCompany($criteriaVo);
        //print_r($result);exit;
        $nombreElement = count($nombreElement);
        $this->setViewState('virtualItemCount', $nombreElement);
        if ($nombreElement >= 1) {
            if (('entreprise' == $this->_calledFrom) && Atexo_Module::isEnabled('BourseALaSousTraitance') && ($nombreElement >= Atexo_Config::getParameter('NOMBRE_LIMIT_ENTREPRISE_SEARCH'))) {
                $msgAvertissement = '<span>'.Prado::localize('MSG_AVERTISSEMENT_NOMBRE_RESULTAT').'</span><br/>';
                $msgAvertissement .= '<span>'.Prado::localize('MSG_AVERTISSEMENT_AFFINER_RECHERCHE').'</span>';
                $this->panelMessageAvertTraduction->setMessage($msgAvertissement);
                $this->panelMessageAvertTraduction->setVisible(true);
            }
            $this->panelElementsFound->setVisible(true);
            $this->panelNoElementFound->setVisible(false);
            $this->nombreElement->Text = $nombreElement;
            $this->PagerBottom->setVisible(true);
            $this->PagerTop->setVisible(true);
            $this->setViewState('nombreElement', $nombreElement);
            $this->nombrePageTop->Text = ceil($nombreElement / $this->tableauResultSearch->PageSize);
            $this->nombrePageBottom->Text = ceil($nombreElement / $this->tableauResultSearch->PageSize);
            $this->tableauResultSearch->setVirtualItemCount($nombreElement);
            $this->tableauResultSearch->setCurrentPageIndex(0);
            $this->populateData($criteriaVo);
        } else {
            $this->panelElementsFound->setVisible(false);
            $this->panelNoElementFound->setVisible(true);
            $this->PagerBottom->setVisible(false);
            $this->PagerTop->setVisible(false);
        }
    }

    public function populateData(Atexo_Entreprise_CriteriaVo $criteriaVo)
    {
        $offset = $this->tableauResultSearch->CurrentPageIndex * $this->tableauResultSearch->PageSize;
        $limit = $this->tableauResultSearch->PageSize;
        if ($offset + $limit > $this->tableauResultSearch->getVirtualItemCount()) {
            $limit = $this->tableauResultSearch->getVirtualItemCount() - $offset;
        }
        $entreprise = new Atexo_Entreprise();
        $criteriaVo->setOffset($offset);
        $criteriaVo->setLimit($limit);
        $arrayConsultation = $entreprise->searchCompany($criteriaVo);
        $this->tableauResultSearch->DataSource = $arrayConsultation;
        $this->tableauResultSearch->DataBind();
    }

    public function goToPage($sender, $param)
    {
        $numPage = null;
        switch ($sender->ID) {
            case 'DefaultButtonTop':
                $numPage = $this->numPageTop->Text;
                break;
            case 'DefaultButtonBottom':
                $numPage = $this->numPageBottom->Text;
                break;
        }
        self::saveEtat();
        if (Atexo_Util::isEntier($numPage)) {
            if ($numPage >= $this->nombrePageTop->Text) {
                $numPage = $this->nombrePageTop->Text;
            } elseif ($numPage <= 0) {
                $numPage = 1;
            }
            $this->tableauResultSearch->CurrentPageIndex = $numPage - 1;
            $this->numPageBottom->Text = $numPage;
            $this->numPageTop->Text = $numPage;
            $criteriaVo = $this->Page->getViewState('CriteriaVo');
            $this->populateData($criteriaVo);
        } else {
            $this->numPageTop->Text = $this->tableauResultSearch->CurrentPageIndex + 1;
            $this->numPageBottom->Text = $this->tableauResultSearch->CurrentPageIndex + 1;
        }
    }

    public function changePagerLenght($sender, $param)
    {
        switch ($sender->ID) {
            case 'listePageSizeBottom':
                $pageSize = $this->listePageSizeBottom->getSelectedValue();
                $this->listePageSizeTop->setSelectedValue($pageSize);
                break;

            case 'listePageSizeTop':
                $pageSize = $this->listePageSizeTop->getSelectedValue();
                $this->listePageSizeBottom->setSelectedValue($pageSize);
                break;
        }

        $this->tableauResultSearch->PageSize = $pageSize;
        $nombreElement = $this->getViewState('nombreElement');
        $this->nombrePageTop->Text = ceil($nombreElement / $this->tableauResultSearch->PageSize);
        $this->nombrePageBottom->Text = ceil($nombreElement / $this->tableauResultSearch->PageSize);
        $criteriaVo = $this->Page->getViewState('CriteriaVo');
        $this->tableauResultSearch->setCurrentPageIndex(0);
        $this->populateData($criteriaVo);
    }

    public function pageChanged($sender, $param)
    {
        self::saveEtat();
        $this->tableauResultSearch->CurrentPageIndex = $param->NewPageIndex;
        $this->numPageBottom->Text = $param->NewPageIndex + 1;
        $this->numPageTop->Text = $param->NewPageIndex + 1;
        $criteriaVo = $this->Page->getViewState('CriteriaVo');
        $this->populateData($criteriaVo);
    }

    public function Trier($champsOrderBy)
    {
        $this->setViewState('sortByElement', $champsOrderBy);
        $criteriaVo = $this->Page->getViewState('CriteriaVo');
        $criteriaVo->setSortByElement($champsOrderBy);
        $arraySensTri = $this->Page->getViewState('sensTriArray', []);
        $arraySensTri[$champsOrderBy] = ('ASC' == $criteriaVo->getSensOrderBy()) ? 'DESC' : 'ASC';
        $criteriaVo->setSensOrderBy($arraySensTri[$champsOrderBy]);
        $this->Page->setViewState('sensTriArray', $arraySensTri);
        $this->Page->setViewState('CriteriaVo', $criteriaVo);
        $this->tableauResultSearch->setCurrentPageIndex(0);
        $this->populateData($criteriaVo);
    }

    public function elementSortedWith($panelName, $openCloseTag)
    {
        if ($this->getViewState('sortByElement') == $panelName) {
            if ('open' == $openCloseTag) {
                return '<strong>';
            } else {
                return '</strong>';
            }
        } else {
            return '';
        }
    }

    public function truncate($texte)
    {
        $truncatedText = null;
        $maximumNumberOfCaracterToReturn = 100;
        $arrayText = explode(' ', $texte);
        $indexArrayText = 0;
        $textlenght = strlen($arrayText[$indexArrayText]) + 1;
        $truncatedText .= $arrayText[$indexArrayText];
        while (isset($arrayText[$indexArrayText]) && $textlenght <= $maximumNumberOfCaracterToReturn) {
            ++$indexArrayText;
            if ($textlenght + (strlen($arrayText[$indexArrayText]) + 1) > $maximumNumberOfCaracterToReturn) {
                break;
            }
            $textlenght += strlen($arrayText[$indexArrayText]) + 1;
            $truncatedText .= ' '.$arrayText[$indexArrayText];
        }

        return $truncatedText;
    }

    public function isTextTruncated($texte, $nbrCaractere = 100)
    {
        return (strlen($texte) > 100) ? '' : 'display:none';
    }

    public function genererExcelEntreprise()
    {
        $criteriaVo = $this->Page->getViewState('CriteriaVo');
        $criteriaVo->setOffset('');
        $criteriaVo->setLimit('');
        $entreprise = new Atexo_Entreprise();
        $donneeComplementaire = new Atexo_DonneesComplementairesEntreprise_DonneesComplementairesEntreprise();
        //si le checkbox est coche
        $listEntreprises = $entreprise->searchCompany($criteriaVo);

        if ($criteriaVo->getRechercheDomaines()) {
            $listDomaines = $this->Page->panelSearch->getListeDomaines();
            $listeIdEntreprise = '';
            if (is_array($listEntreprises)) {
                foreach ($listEntreprises as $entreprise) {
                    $listeIdEntreprise .= $entreprise->getId().',';
                }
            }
            if (!$listeIdEntreprise) {
                $listeIdEntreprise = '0';
            }
            $listDonneeComplementaireEntreprises = $donneeComplementaire->searchDonneeComplementaire(trim($listeIdEntreprise, ','), $listDomaines);
            (new Atexo_GenerationExcel())->genererListeEntrepriseExcelDonneeComplementaire($listDonneeComplementaireEntreprises);
        }
        //Sinon
        else {
            (new Atexo_GenerationExcel())->genererListeEntrepriseExcel($listEntreprises);
        }
        exit;
    }

    public function actionCompany($sender, $param)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $company = EntreprisePeer::retrieveByPK($param->CommandParameter, $connexion);
        if ('Lock' === $param->CommandName) { // DESACTIVER
            $company->setCompteActif('0');
        } elseif ('UnLock' === $param->CommandName) { // ACTIVER
            $company->setCompteActif('1');
        }
        $company->save($connexion);
    }

    /**
     * Actualiser le tableau de resultat dans le cas d'une nouvelle action.
     */
    public function refreshRepeater($sender, $param)
    {
        $criteriaVo = $this->Page->getViewState('CriteriaVo');
        $this->fillRepeaterWithDataForSearchResult($criteriaVo);
        $this->Page->activePanelResultSearch->render($param->NewWriter);
    }

    public function saveEtat()
    {
        $listEtat = $this->getViewState('saveEtat', []);
        foreach ($this->tableauResultSearch->getItems() as $item) {
            if ($item->entrepriseSelection->Checked) {
                $listEtat[$item->IdEse->value] = $item->IdEse->value;
            } else {
                unset($listEtat[$item->IdEse->value]);
            }
        }
        $this->setViewState('saveEtat', $listEtat);
    }

    public function getEtat($idEse)
    {
        $listEtat = $this->getViewState('saveEtat', []);

        return isset($listEtat[$idEse]);
    }

    public function validerChoix($sender, $param)
    {
        self::saveEtat();
        $sql = 'select adresses_electroniques from Entreprise '.
                "where id in ('".implode("','", $this->getViewState('saveEtat', []))."')";

        $listeMail = '';
        $statement = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_ASSOC);
        foreach ($statement as $ligne) {
            if ($ligne['adresses_electroniques']) {
                $listeMail .= $ligne['adresses_electroniques'];
                $listeMail .= ',';
            }
        }
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $echange = CommonEchangePeer::retrieveByPK(Atexo_Util::atexoHtmlEntities($_GET['IdEchange']), $connexionCom);
        $echange->setDestinatairesBdFournisseurs(trim($listeMail, ','));
        $echange->save($connexionCom);

        $this->response->redirect('index.php?page=Agent.EnvoiCourrierElectroniqueChoixDestinataire&id='.Atexo_Util::atexoHtmlEntities($_GET['id']).'&forInvitationConcourir='.Atexo_Util::atexoHtmlEntities($_GET['forInvitationConcourir']).'&IdEchange='.Atexo_Util::atexoHtmlEntities($_GET['IdEchange']));
    }

    public function getArrayTypeColloboration($typeCollaborationEntreprise)
    {
        $dataTypeCollaboration = $this->Page->getViewState('arrayTypeCollaboration');
        $dataSource = [];
        if (!$dataTypeCollaboration) {
            $dataTypeCollaboration = $this->remplirValTypeCollaboration();
        }

        if ($typeCollaborationEntreprise) {
            $arrayIds = explode('#', $typeCollaborationEntreprise);
            foreach ($arrayIds as $id) {
                if ($id) {
                    $dataSource[$id] = $dataTypeCollaboration[$id];
                }
            }
        }

        return $dataSource;
    }

    public function remplirValTypeCollaboration()
    {
        $dataSource = [];
        $arrayType = (new Atexo_ValeursReferentielles())->getValeurRefByIdRef(Atexo_Config::getParameter('REFERENTIEL_TYPE_COLLABORATION'));
        foreach ($arrayType as $key => $value) {
            $dataSource[$key]['id'] = $key;
            $dataSource[$key]['libelle'] = $value;
        }
        $this->Page->setViewState('arrayTypeCollaboration', $dataSource);
    }

    /**
     * permet de retourner le texte de detail.
     *
     * @return chaine de caractère
     *
     * @param $dataItem
     * param $all un boolean s'il est à true la fonction retourne toute la chaine si non la fonction retourne les 110 premier caractères
     *
     * @author Oussama SALMANI <oussama.salmani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function doGetLibelleLtRefDomaineActivite($dataItem, $all = false)
    {
        if (Atexo_Module::isEnabled('compteEntrepriseDomaineActiviteLtReferentiel')) {
            $chaine = $dataItem->getLibelleLtRef($dataItem->getDomainesActivites(), Atexo_Languages::readLanguageFromSession(), Atexo_Config::getParameter('PATH_FILE_DOMAINES_ACTIVITES_CONFIG'));

            return getChaine($chaine, $all);
        }

        return '';
    }

    /**
     * permet de retourner le texte de detail.
     *
     * @return chaine de caractère
     *
     * @param $dataItem
     * param $all un boolean s'il est à true la fonction retourne toute la chaine si non la fonction retourne les 110 premier caractères
     *
     * @author Oussama SALMANI <oussama.salmani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function doGetLibelleLtRefAgrement($dataItem, $all = false)
    {
        if (Atexo_Module::isEnabled('CompteEntrepriseAgrementLtReferentiel')) {
            $chaine = $dataItem->getLibelleLtRef($dataItem->getAgrement(), Atexo_Languages::readLanguageFromSession(), Atexo_Config::getParameter('PATH_FILE_CERTIFICAT_COMPTE_ENTREPRISE_CONFIG'));

            return getChaine($chaine, $all);
        }

        return '';
    }

    /**
     * permet de retourner le texte de detail.
     *
     * @return chaine de caractère
     *
     * @param $dataItem
     * param $all un boolean s'il est à true la fonction retourne toute la chaine si non la fonction retourne les 110 premier caractères
     *
     * @author Oussama SALMANI <oussama.salmani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function doGetLibelleLtRefQualification($dataItem, $all = false)
    {
        if (Atexo_Module::isEnabled('CompteEntrepriseQualificationLtReferentiel')) {
            $chaine = $dataItem->getLibelleLtRef($dataItem->getQualification(), Atexo_Languages::readLanguageFromSession(), Atexo_Config::getParameter('PATH_FILE_QUALIFICATION_CONFIG'));

            return getChaine($chaine, $all);
        }

        return '';
    }

    /**
     * permet de retourner les 110 premiers caractères suivant un boolean.
     *
     * @return chaine de caractère
     *
     * @param $chaine la chaine à traiter
     * param $all un boolean s'il est à true la fonction retourne toute la chaine si non la fonction retourne les 110 premier caractères
     *
     * @author Oussama SALMANI <oussama.salmani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function getChaine($chaine, $all)
    {
        if (false == $all && (strlen($chaine) > 110)) {
            return substr($chaine, 0, 110);
        } else {
            return $chaine;
        }
    }
}
