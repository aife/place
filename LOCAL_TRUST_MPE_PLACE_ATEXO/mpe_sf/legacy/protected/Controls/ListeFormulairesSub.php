<?php

namespace Application\Controls;

use Application\Propel\Mpe\CommonCategorieLot;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonTDossierFormulaire;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\FormulaireSub\Atexo_FormulaireSub_Dossier;
use Prado\Prado;

/**
 * Page de gestion des formulaires SUB.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 1.0
 *
 * @since MPE-4.0
 */
class ListeFormulairesSub extends MpeTTemplateControl
{
    protected $_consultation;

    /*
     * Attribut la valeur
     */
    public function setConsultation($value)
    {
        $this->_consultation = $value;
    }

    /*
     * Recupère la valeur
     */
    public function getConsultation()
    {
        return $this->_consultation;
    }

    public function onLoad($param)
    {
        $this->scriptOpenFormulaireEnveloppe->Text = '';
        if (Atexo_Module::isEnabled('InterfaceModuleSub') && Atexo_CurrentUser::isConnected()) {
            if (!$this->Page->IsCallback) {
                if ($this->getConsultation() instanceof CommonConsultation) {
                    //Chargement de la liste des lots dans le pop-in
                    $dossierCandidature = $this->getDossierCandidature();
                    $this->idCandidatureFormSUB->setDossier($dossierCandidature);
                    $this->idCandidatureFormSUB->setRefCons($this->getConsultation()->getId());
                    $this->idCandidatureFormSUB->setOrganisme($this->getConsultation()->getOrganisme());
                    $this->idDossierPere->value = $dossierCandidature->getIdDossierFormulaire();
                    $this->getChargerListeLots();
                    //Chargement des dossiers
                    $this->chargerListeDossiers();
                    $this->idCandidatureFormSUB->setListeEditions((new Atexo_FormulaireSub_Dossier())->recupererEditionsFormulaire($dossierCandidature->getIdDossierFormulaire()));
                }
            }
        }
    }

    /**
     * Permet de determiner si la variante est autorisée.
     *
     * @param $objet: instance de CommonConsultation ou CommonCategorieLot
     */
    public function getVarianteAutorisee($consultation, $lot)
    {
        if ($this->Page->getConsultation()->getAlloti()) {
            $objet = $lot;
        } else {
            $objet = $this->getConsultation();
        }
        $hasVariantes = false;
        if ((new Atexo_Util())->objetHasVariante($objet)) {
            $hasVariantes = true;
        }

        return $hasVariantes;
    }

    /*
     * Permet de gérer la visibilité du bouton ajouter
     */
    public function afficherBoutonAjouter()
    {
        if (is_array($this->getViewState('listeFormulairesOffres')) && count($this->getViewState('listeFormulairesOffres'))) {
            return 'Enabled';
        } elseif (is_array($this->getViewState('listeFormulairesOffres')) && !count($this->getViewState('listeFormulairesOffres')) && /* candidature valide, ici on met true */ 1 == 1) {
            return 'Enabled';
        } else {
            return 'disabled';
        }
    }

    /*
     * Retourne la liste des lots
     */
    public function getChargerListeLots()
    {
        $dataSource = [];
        $listeLots = [];
        if ($this->getConsultation()->getAlloti()) {
            $listeObjetsLots = $this->getConsultation()->getAllLots();
            if (is_array($listeObjetsLots) && count($listeObjetsLots)) {
                foreach ($listeObjetsLots as $lot) {
                    $dataSource[$lot->getLot()] = Prado::localize('TEXT_LOT').' '.$lot->getLot().' - '.$lot->getDescription();
                    $listeLots[$lot->getLot()] = $lot;
                }
            }
        }
        $this->setViewState('listeLots', $listeLots);
        $this->listeLots->DataSource = $dataSource;
        $this->listeLots->DataBind();
    }

    /*
     * Permet de supprimer un dossier
     */
    public function supprimerDossier($sender, $param)
    {
        (new Atexo_FormulaireSub_Dossier())->supprimerDossier($this->Page->getViewState('idDossierFormulaire'));

        //Chargement des dossiers
        $this->chargerListeDossiers();
        $this->raffraichirBlocDossiersFormulaire($sender, $param);
    }

    /*
     * Raffraichissement du bloc des dossiers
     */
    public function raffraichirBlocDossiersFormulaire($sender, $param)
    {
        $this->scriptOpenFormulaireEnveloppe->Text = '';
        $this->idCandidatureFormSUB->setDossier($this->getDossierCandidature());
        $this->idDossierPere->value = $this->getDossierCandidature()->getIdDossierFormulaire();
        $this->idCandidatureFormSUB->setRefCons($this->Page->getConsultation()->getId());
        $this->idCandidatureFormSUB->setOrganisme($this->Page->getConsultation()->getOrganisme());
        //Chargement des dossiers
        $listEdition = (new Atexo_FormulaireSub_Dossier())->recupererEditionsFormulaire($this->getDossierCandidature()->getIdDossierFormulaire());
        $this->idCandidatureFormSUB->setListeEditions($listEdition);
        $this->idCandidatureFormSUB->chargerEditions();
        //Chargement des dossiers
        $this->chargerListeDossiers();

        $this->BlocdossiersFormulaires->render($param->getNewWriter());
    }

    /*
     * Permet de charger la liste des dossiers
     */
    public function chargerListeDossiers()
    {
        $dataSource = (new Atexo_FormulaireSub_Dossier())->getChargerListeFormulairesLots($this->getConsultation()->getId(), $this->getConsultation()->getOrganisme(), Atexo_CurrentUser::getIdInscrit(), Atexo_Config::getParameter('TYPE_ENV_OFFRE'));
        $this->consultationOuListeDesLots->DataSource = $dataSource;
        $this->consultationOuListeDesLots->DataBind();
    }

    /**
     * Construit l'url d'édition du document.
     *
     * @param $lot: numéro du lot
     */
    public function getUrlFormulaireEnveloppe($lot)
    {
        $url = '?page=Entreprise.PopUpFormulaireEnveloppe';
        $url .= '&idDossier=&id='.$this->getConsultation()->getId().
                    '&orgAcronyme='.$this->getConsultation()->getOrganisme().'&typeEnv='.Atexo_Config::getParameter(TYPE_ENV_OFFRE).'&lot='.$lot;
        if ($this->idDossierPere->value) {
            $url .= '&idDossierPere='.$this->idDossierPere->value;
        }

        return $url;
    }

    public function openFormulaireEnveloppe($sender, $param)
    {
        $this->scriptOpenFormulaireEnveloppe->Text = '';
        $lot = $this->listeLots->selectedValue;
        $url = $this->getUrlFormulaireEnveloppe($lot);
        $reponseElect = (new Atexo_FormulaireSub_Dossier())->recupererReponseElectFormulaire($this->getConsultation()->getId(), $this->getConsultation()->getOrganisme(), Atexo_CurrentUser::getIdInscrit());
        if ($reponseElect) {
            $dataSource = (new Atexo_FormulaireSub_Dossier())->getDossierByIdReponseElectFormulaireAndTypeEnveloppeAndNumLot($reponseElect->getIdReponseElecFormulaire(), Atexo_Config::getParameter('TYPE_ENV_OFFRE'), $lot);
            if ($dataSource) {
                if ($lot) {
                    $msg = Prado::localize('DEFINE_IMPOSSIBLE_SOUMETTRE_DEUX_OFFRES_POUR_UN_LOT');
                } else {
                    $msg = Prado::localize('DEFINE_IMPOSSIBLE_SOUMETTRE_DEUX_OFFRES');
                }
                $this->panelMsgErreur->setMessage($msg);
                $this->blocAjoutFormulaireOffre->render($param->getNewWriter());
                $this->scriptOpenFormulaireEnveloppe->Text = "<script language='javascript'>showDiv('panelMsgErreur');</script>";
            } else {
                $this->blocAjoutFormulaireOffre->render($param->getNewWriter());
                $this->scriptOpenFormulaireEnveloppe->Text = "<script language='javascript'>document.cookie = 'JSESSIONID=; expires=Thu, 01 Jan 1970 00:00:00 GMT; path=/sub';
																 hideDiv('panelMsgErreur');J('.modal-ajout-offre').dialog('close'); popUpFormulaireSub('".$url."','yes');</script>";
            }
        }
    }

    public function getDossierCandidature()
    {
        $candidature = null;
        if ($this->getConsultation() instanceof CommonConsultation) {
            $dataSource = (new Atexo_FormulaireSub_Dossier())->getChargerListeFormulairesLots($this->getConsultation()->getId(), $this->getConsultation()->getOrganisme(), Atexo_CurrentUser::getIdInscrit(), Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE'));
            $candidature = $dataSource[0]['dossiers'][0];
            if (!($candidature instanceof CommonTDossierFormulaire)) {
                $candidature = new CommonTDossierFormulaire();
                $candidature->setIdLot('0');
                $candidature->setStatutValidation('1');
                $candidature->setTypeEnveloppe('1');
            }
        }

        return $candidature;
    }

    /**
     * Permet d'ouvrir la pop in de suppréssion d'un dossier.
     */
    public function ouvrirPopInSuppressionDossier($sender, $param)
    {
        $arrayParam = explode('_', $param->CallbackParameter);
        $this->Page->setViewState('idDossierFormulaire', $arrayParam[0]);
        $this->Page->setViewState('itemIndexDossierFormulaire', $arrayParam[1]);
        $index = $arrayParam[1];
        $script = "<script>
					  document.getElementById('".$this->scriptJsPopInSuppressionDossier->getClientId()."').title='".Atexo_Util::toUtf8(Prado::localize('DEFINE_SUPPRIMER_DOSSIER'))."';
    				  openModal('modal-suppression-Dossier-".$index."','popup-small2',document.getElementById('".$this->scriptJsPopInSuppressionDossier->getClientId()."'));
    			  </script>";
        $this->panelRaffraichissementPopInConfirmation->render($param->getNewWriter());
        $this->scriptJsPopInSuppressionDossier->Text = $script;
    }
}
