<?php

namespace Application\Controls;

use Application\Service\Atexo\Signature\Atexo_Signature_FichierVo;

/**
 * Page d'affichage des détails de la signature du formulaire de reponse.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 1.0
 *
 * @since MPE-4.0
 */
class AtexoDetailsSignatureFormulaireReponse extends MpeTTemplateControl
{
    private $_fichierVo;
    public $_idModal;
    public $_identifiantFichier;

    /**
     * Recupère la valeur.
     */
    public function getFichierVo()
    {
        return $this->_fichierVo;
    }

    /**
     * Affectes la valeur.
     */
    public function setFichierVo($value)
    {
        $this->_fichierVo = $value;
        if ($this->getFichierVo() instanceof Atexo_Signature_FichierVo) {
            $this->setIdModal($this->_fichierVo->getIdentifiant());
        }
    }

    /**
     * Recupère la valeur.
     */
    public function getIdModal()
    {
        return $this->_idModal;
    }

    /**
     * Affectes la valeur.
     */
    public function setIdModal($value)
    {
        $this->_idModal = $value;
    }

    /**
     * Recupère la valeur.
     */
    public function getIdentifiantFichier()
    {
        return $this->_identifiantFichier;
    }

    /**
     * Attribut la valeur.
     */
    public function setIdentifiantFichier($value)
    {
        $this->_identifiantFichier = $value;
    }

    /**
     * Chargement de la page.
     */
    public function onLoad($param)
    {
    }
}
