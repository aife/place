<?php

namespace Application\Controls;

use Application\Service\Atexo\Controller\Atexo_Controller_Front;
use Prado\Prado;
use Prado\Web\UI\WebControls\TImageButton;

/**
 * commentaires.
 *
 * @author Khadija CHOUIKA <khadija.chouika@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 2.1
 *
 * @since MPE-4.0
 */
class PictoMiseDispositionPiecesMarche extends TImageButton
{
    public string $_clickable = 'false';
    public string $_activate = 'false';

    public function onLoad($param)
    {
        if ('false' == $this->_activate) {
            $this->setImageUrl(Atexo_Controller_Front::t().'/images/picto-formulaire-inactive.gif');
        } else {
            $this->setImageUrl(Atexo_Controller_Front::t().'/images/picto-formulaire.gif');
        }
        if ('false' == $this->getClickable()) {
            //le "return false" empeche le contrôle de faire un callback, cela peut être considéré comme un extends TImage (pas de rafraichissement)
            $this->Attributes->onclick = 'return false';
        }

        $this->setAlternateText(Prado::localize('TEXT_PIECES_DU_MARCHE'));
        $this->setToolTip(Prado::localize('TEXT_PIECES_DU_MARCHE'));
    }

    public function getClickable()
    {
        return $this->_clickable;
    }

    public function setClickable($value)
    {
        $this->_clickable = $value;
    }

    public function getActivate()
    {
        return $this->_activate;
    }

    public function setActivate($bool)
    {
        $this->_activate = $bool;
    }
}
