<!--Debut Bloc Groupement-->
<div class="form-field bloc-formulaires-reponse">
	<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
	<div class="content">
		<h3><com:TTranslate>DEFINE_GROUPEMENT</com:TTranslate></h3>


		<p class="infos-line margin-0" style="display:<%=$this->isModeAgent()?'none':''%>"><com:TTranslate>INFO_DECLARATION_GROUPEMENT</com:TTranslate></p>
		<div class="spacer-small"></div>
		<com:AtexoValidationSummary id="divValidationSummaryCoTraitant"  ValidationGroup="validateAjoutCoTraitant" />
		<com:TActivePanel id="panelBlocMessages" ">
			<com:PanelMessageErreur ID="messageErreur" />
		</com:TActivePanel>
		<!--Bloc message-->
		<div class="form-bloc margin-0" style="display:<%=$this->isModeAgent()?'none':''%>">
			<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
			<div class="content">

				<div class="line margin-0">
					<div class="intitule-270"><strong><com:TTranslate>REPONSE_EN_GROUPEMENT</com:TTranslate><span class="champ-oblig">*</span></strong></div>
					<div class="content-bloc bloc-460">
						<div class="intitule-80">
							<com:TRadioButton GroupName="groupement" id="groupementOui" cssclass="radio" Attributes.onclick="showDiv('<%=$this->layerGroupement->getClientId()%>');showDiv('<%=$this->layerRepeaterGrouepement->getClientId()%>');"  /><label for="groupementOui"><com:TTranslate>DEFINE_OUI</com:TTranslate></label>
						</div>
						<div class="intitule-auto">
							<com:TRadioButton GroupName="groupement" id="groupementNon" cssclass="radio" Attributes.onclick="hideDiv('<%=$this->layerGroupement->getClientId()%>');hideDiv('<%=$this->layerRepeaterGrouepement->getClientId()%>');"  checked="true"/><label for="groupementNon"><com:TTranslate>DEFINE_NON</com:TTranslate></label>
						</div>
					</div>
				</div>

				<com:TPanel id="layerGroupement" style="display:none;">
					<div class="line margin-0">
						<div class="intitule-250 indent-hyphen indent-20"><com:TTranslate>DEFINE_TYPE_GROUPEMENT</com:TTranslate></div>
						<div class="content-bloc bloc-460">
							<div class="intitule-80">
								<com:TActiveRadioButton GroupName="typeGroupement" id="groupementSolidaire" onCheckedChanged="typeGroupementChanged" cssclass="radio"  checked="true" /><label for="groupementSolidaire"><com:TTranslate>TYPE_GROUPEMENT_SOLIDAIRE</com:TTranslate></label>
							</div>
							<div class="intitule-auto">
								<com:TActiveRadioButton GroupName="typeGroupement" id="groupementConjointNonSolidaire" onCheckedChanged="typeGroupementChanged" cssclass="radio"   /><label for="groupementConjointNonSolidaire"><com:TTranslate>TYPE_GROUPEMENT_CONJOINT_NON_SOLIDAIRE</com:TTranslate></label>
							</div>
							<div class="intitule-auto">
								<com:TActiveRadioButton GroupName="typeGroupement" id="groupementConjointSolidaire" onCheckedChanged="typeGroupementChanged" cssclass="radio"   /><label for="groupementConjointSolidaire"><com:TTranslate>TYPE_GROUPEMENT_CONJOINT_SOLIDAIRE</com:TTranslate></label>
							</div>
						</div>
					</div>
					<div class="line margin-0">
						<div class="intitule-250 indent-hyphen indent-20"><com:TTranslate>DECLARER_GROUPEMENT</com:TTranslate>:</div>
						<div class="content-bloc bloc-460">
							<div class="intitule-80">
								<com:TActiveRadioButton GroupName="declarerGroupement" id="declarerGroupementOui"  cssclass="radio" Attributes.onclick="showDiv('<%=$this->layerGroupementCoTraitant->getClientId()%>');showDiv('<%=$this->layerRepeaterGrouepement->getClientId()%>');"  /><label for="declarerGroupementOui"><com:TTranslate>DEFINE_OUI</com:TTranslate></label>
							</div>
							<div class="intitule-auto">
								<com:TActiveRadioButton GroupName="declarerGroupement" id="declarerGroupementNon" cssclass="radio" Attributes.onclick="hideDiv('<%=$this->layerGroupementCoTraitant->getClientId()%>');hideDiv('<%=$this->layerRepeaterGrouepement->getClientId()%>');"  checked="true" /><label for="declarerGroupementNon"><com:TTranslate>DEFINE_NON</com:TTranslate></label>
							</div>
						</div>
					</div>
					<com:TPanel id="layerGroupementCoTraitant" cssClass="line margin-0" style="display:none;">
						<div class="intitule-220 indent-hyphen indent-50"><com:TTranslate>AJOUTER_CO_TRAITANT</com:TTranslate> : </div>
						<div class="content-bloc bloc-siret bloc-460">
							<com:TActiveTextBox id="siren" Attributes.title="<%=Prado::localize('TEXT_SIREN')%>"  Attributes.placeholder="<%=Prado::localize('TEXT_SIREN')%>" MaxLength="9" CssClass="siren placehold" />
							<com:TActiveTextBox id="siret" Attributes.title="<%=Prado::localize('TEXT_SIRET')%>" Attributes.placeholder="<%=Prado::localize('TEXT_SIRET')%>" MaxLength="5" CssClass="siret placehold" />
							<com:THiddenField  id="siretSiren" Value="<%=$this->siren->getClientId().'#'.$this->siret->getClientId()%>" />
							<com:TCustomValidator
									ValidationGroup="validateAjoutCoTraitant"
									ControlToValidate="siretSiren"
									ErrorMessage="<%=Prado::localize('SIREN_SIRET')%>"
									Display="Dynamic"
									EnableClientScript="true"
									ClientValidationFunction="validationSiretCoTraitantNationale"
									Text="<span title='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' /></span>" >
								<prop:ClientSide.OnValidationError>
									document.getElementById('divValidationSummaryCoTraitant').style.display='';
								</prop:ClientSide.OnValidationError>
							</com:TCustomValidator>
							<com:TActiveLinkButton
									id="ajouterCoTraitant"
									cssClass="btn btn-primary btn-xs btn-action"
									ValidationGroup="validateAjoutCoTraitant"
									onCallBack="ajouterCoTraitant"
									Attributes.title="<%=Prado::localize('AJOUTER_CO_TRAITANT')%>"
							>
								<img alt="<%=Prado::localize('AJOUTER_CO_TRAITANT')%>" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-ajout-element.gif" />
								<prop:ClientSide.OnLoading>showLoader();</prop:ClientSide.OnLoading>
								<prop:ClientSide.OnFailure>showLoader();</prop:ClientSide.OnFailure>
								<prop:ClientSide.OnSuccess>showLoader();</prop:ClientSide.OnSuccess>
							</com:TActiveLinkButton>
						</div>
					</com:TPanel>
				</com:TPanel>

			</div>
			<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
		</div>
		<div class="spacer-small"></div>
		<!--Fin Bloc message-->

		<com:TPanel id="layerRepeaterGrouepement"  style="display:none;">
			<com:TActivePanel id="infoGroupementPanel" style="display:none;" >
			<com:PanelMessageInformation id="infoGroupement" message="<%=Prado::Localize('DEFINE_MESSAGE_SOUS_TRAITANCE')%>" />
			</com:TActivePanel>
		<!--Debut Tableau Attributaires-->
		<com:TActiveRepeater ID="repeaterGroupement">
			<prop:ItemTemplate>
				<div class="panel panel-default panel-attributaires">
					<table class="table table-hover table-striped treeview table-suivi table-attributaires groupement-solidaire" style="border-collapse:collapse;" role="presentation">
						<caption class="sr-only"><com:TTranslate>ATTRIBUTAIRES_ET_CONTRACTANTS</com:TTranslate></caption>
						<tbody>
							<tr class="row-attributaire">
								<th><span class="icon icon-titulaire"><strong><com:TLabel id="labelTypeGroupement" Text="<%#Prado::Localize('DEFINE_GROUPEMENT') . ' : ' .$this->Data->getLibelleTypeGroupement()%>"  /></strong></span></th>
								<th class="actions col-md-2">
									<strong>
										<com:TLabel ID="labelActions" Text="<%=Prado::Localize('DEFINE_ACTIONS')%>" visible="<%#(!$this->SourceTemplateControl->isModeAgent())%>" />
										<com:TLabel ID="labelAttestations" Text="<%=Prado::Localize('TEXT_ATTESTATIONS')%>" visible="<%#($this->SourceTemplateControl->isModeAgent())%>"/>
									</strong>
								</th>
							</tr>

							<com:TRepeater ID="repeaterMembres" DataSource="<%#$this->Data->getMembres()%>">
								<prop:ItemTemplate>

									<tr class="indent-n1 treegrid-<%#($this->ItemIndex+1)%> <%#($this->ItemIndex == ($this->SourceTemplateControl->getNombreMembreGroupement()-1)) ? 'last':''%> <%#($this->Data->isHasChild())?' treegrid-expanded':''%>">
										<td>
											<span class="icon icon-mandataire <%#(($this->TemplateControl->Data->getIdTypeGroupement() ==  Application\Service\Atexo\Atexo_Config::getParameter('ID_TYPE_GROUPEMENT_CONJOINT_SOLIDAIRE')) && ($this->Data->getIdRoleJuridique() ==  Application\Service\Atexo\Atexo_Config::getParameter('ID_ROLE_JURIDIQUE_MANDATAIRE'))) ?'solidaire':''%>">
												<strong><com:TLabel ID="labelTypeMembre" Text="<%#$this->Data->getLibelleRoleJuridique()%>" /> :
													<com:TActiveHyperLink
															id="lienFicheFournisseur"
															NavigateUrl="<%#$this->SourceTemplateControl->getLienFicheFournisseur($this->TemplateControl->Data->getIdOffre(),$this->Data->getIdEntreprise())%>"
													>
														<com:TLabel ID="labelNomEntreprise" Text="<%#$this->Data->getNomEntreprise()%>" /> -
														<com:TLabel ID="labelCodePostal" Text="<%#$this->Data->getCodePostalEtablissement()%>" /> <com:TLabel ID="labelVille2" Text="<%#$this->Data->getVilleEtablissement()%>" />
													</com:TActiveHyperLink>
													<abbr tabindex="0" title="<%=Prado::Localize('VERIFIE')%>" data-toggle="tooltip" class="btn-link" data-original-title="<%=Prado::Localize('VERIFIE')%>">
														<i class="fa fa-check text-success">
															<span style="display:<%#($this->Data->getSaisiManuel())?'none':'dynamic'%>" class="sr-only"><com:TTranslate>VERIFIE</com:TTranslate></span>
														</i>
													</abbr>
												</strong>
												<com:THiddenField id="idEntreprise" value="<%#$this->Data->getIdEntreprise()%>" />
											</span>
										</td>
										<td class="actions col-md-1">
											<com:TPanel Visible="<%#($this->SourceTemplateControl->isModeAgent() && Application\Service\Atexo\Atexo_Module::isEnabled('DonneesCandidat'))%>">
												<com:AtexoDisponibiliteAttestation id="dispoAttestationsReponse" idEntreprise="<%#$this->Data->getIdEntreprise()%>" idOffre="<%#$this->TemplateControl->Data->getIdOffre()%>" />
											</com:TPanel>
											<com:TActiveLinkButton
													id="ajouterLigneSousTraitant"
													cssClass="btn btn-primary btn-xs btn-action"
													CommandParameter="<%#($this->Data->getIdEntreprise())%>"
													OnCommand="SourceTemplateControl.ajouterLigneSousTraitant"
													Attributes.title="<%=Prado::localize('AJOUTER_SOUS_TRAITANT')%>"
													Visible="<%#(!$this->SourceTemplateControl->isModeAgent())%>"
											>
												<img alt="<%=Prado::localize('AJOUTER_SOUS_TRAITANT')%>" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/icone-ajout-user.png" />
												<prop:ClientSide.OnLoading>showLoader();</prop:ClientSide.OnLoading>
												<prop:ClientSide.OnFailure>showLoader();</prop:ClientSide.OnFailure>
												<prop:ClientSide.OnSuccess>showLoader();J('#ctl0_CONTENU_PAGE_blocGroupementEntreprise_infoGroupementPanel').show();</prop:ClientSide.OnSuccess>
											</com:TActiveLinkButton>
											<com:TActiveLinkButton
													Attributes.title="<%=Prado::Localize('DEFINE_SUPPRIMER')%>"
													cssClass="btn btn-action"
													Visible="<%#(!$this->SourceTemplateControl->isModeAgent()) && ($this->Data->getIdRoleJuridique() !=  Application\Service\Atexo\Atexo_Config::getParameter('ID_ROLE_JURIDIQUE_MANDATAIRE'))%>"
													CommandParameter="<%#($this->Data->getIdEntreprise())%>"
													OnCommand="SourceTemplateControl.SupprimerCoTraitant"

											>
												<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-supprimer-big.gif" alt="" />
												<prop:ClientSide.OnLoading>showLoader();</prop:ClientSide.OnLoading>
												<prop:ClientSide.OnFailure>showLoader();</prop:ClientSide.OnFailure>
												<prop:ClientSide.OnSuccess>showLoader();</prop:ClientSide.OnSuccess>
											</com:TActiveLinkButton>

										</td>
									</tr>
									<com:TRepeater ID="repeaterSousMembres" DataSource="<%#$this->Data->getSousMembres()%>">
										<prop:ItemTemplate>
											<tr class="child treegrid-parent-1 <%#($this->ItemIndex == ($this->SourceTemplateControl->getNombreSousMembreGroupement($this->Data->getIdEntrepriseParent())-1)) ? 'last':''%> ">
												<td>
													<span class="icon icon-sous-traitant">
														<strong class="intitule-auto"><com:TLabel ID="labelTypeSousMembre" Text="<%#$this->Data->getLibelleRoleJuridique()%>" /> :</strong>
														<com:TPanel cssClass="content-bloc bloc-siret" id="panelInfoSousTraitant" style="display:<%#$this->Data->getIdEntreprise()?'':'none'%>">
															<com:TActiveHyperLink
																	id="lienFicheFournisseur"
																	NavigateUrl="<%#$this->SourceTemplateControl->getLienFicheFournisseur($this->TemplateControl->TemplateControl->Data->getIdOffre(),$this->Data->getIdEntreprise())%>"
															>
																<com:TLabel ID="labelNomEntreprise" Text="<%#$this->Data->getNomEntreprise()%>" /> -
																<com:TLabel ID="labelCodePostal" Text="<%#$this->Data->getCodePostalEtablissement()%>" /> <com:TLabel ID="labelVille2" Text="<%#$this->Data->getVilleEtablissement()%>" />
															</com:TActiveHyperLink>
															<abbr tabindex="0" title="<%=Prado::Localize('VERIFIE')%>" data-toggle="tooltip" class="btn-link" data-original-title="<%=Prado::Localize('VERIFIE')%>">
																<i class="fa fa-check text-success">
																	<span style="display:<%#($this->Data->getSaisiManuel())?'none':'dynamic'%>" class="sr-only"><com:TTranslate>VERIFIE</com:TTranslate></span>
																</i>
															</abbr>
														</com:TPanel>
														<com:TPanel cssClass="content-bloc bloc-siret" id="panelAjoutSousTraitant" style="display:<%#$this->Data->getIdEntreprise()?'none':''%>" >
															<com:TActiveTextBox id="siren" Attributes.title="<%=Prado::localize('TEXT_SIREN')%>" Attributes.placeholder="<%=Prado::localize('TEXT_SIREN')%>" MaxLength="9" CssClass="siren placehold" />
															<com:TActiveTextBox id="siret" Attributes.title="<%=Prado::localize('TEXT_SIRET')%>" Attributes.placeholder="<%=Prado::localize('TEXT_SIRET')%>" MaxLength="5" CssClass="siret placehold" />
															<com:THiddenField id="idEntreprise" value="<%#$this->Data->getIdEntreprise()%>" />
															<com:TActiveLinkButton
																	id="ajouterSousTraitant"
																	cssClass="btn btn-primary btn-xs btn-action"
																	ActiveControl.CallbackParameter="<%#array('idEntrepriseParent'=>$this->Data->getIdEntrepriseParent(),'indexElement'=>$this->Data->getIdEntreprise())%>"
																	onCallBack="SourceTemplateControl.ajouterSousTraitant"
																	Attributes.title="<%=Prado::localize('AJOUTER_SOUS_TRAITANT')%>"
																	ValidationGroup="validateAjoutSousTraitant<%#($this->ItemIndex)%>"
																	attributes.onClick="if(!validationSiretSousTraitantNationale('<%#$this->siren->getClientId()%>','<%#$this->siret->getClientId()%>','<%#$this->imgError->getClientId()%>','<%#$this->labelSiretInvalide->getClientId()%>')) return false;"
															>
																<img alt="<%=Prado::localize('AJOUTER_SOUS_TRAITANT')%>" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/icone-ajout-user.png" />
																<prop:ClientSide.OnLoading>showLoader();</prop:ClientSide.OnLoading>
																<prop:ClientSide.OnFailure>showLoader();</prop:ClientSide.OnFailure>
																<prop:ClientSide.OnSuccess>showLoader();</prop:ClientSide.OnSuccess>
															</com:TActiveLinkButton>
															<com:TActiveImage id="imgError"  ImageUrl='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif'  Attributes.alt="<%=Prado::localize('FORMAT_INCORRECTE')%>" Style="display:none" />
															<com:TActiveLabel Id="labelSiretInvalide"  cssClass="statut-erreur"/>
														</com:TPanel>
													</span>
												</td>
												<td class="actions col-md-1">
													<com:TPanel Visible="<%#($this->SourceTemplateControl->isModeAgent() && Application\Service\Atexo\Atexo_Module::isEnabled('DonneesCandidat'))%>">
														<com:AtexoDisponibiliteAttestation id="dispoAttestationsReponse" idEntreprise="<%#$this->Data->getIdEntreprise()%>" idOffre="<%#$this->TemplateControl->TemplateControl->Data->getIdOffre()%>" />
													</com:TPanel>
													<com:TActiveLinkButton
															Attributes.title="<%=Prado::Localize('DEFINE_SUPPRIMER')%>"
															cssClass="btn btn-action"
															CommandParameter="<%#array('idEntreprise'=>$this->Data->getIdEntreprise(),'idEntrepriseParent'=>$this->Data->getIdEntrepriseParent())%>"
															OnCommand="SourceTemplateControl.deleteSousTraitant"
															Visible="<%#(!$this->SourceTemplateControl->isModeAgent())%>"

													>
														<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-supprimer-big.gif" alt="" />
														<prop:ClientSide.OnLoading>showLoader();</prop:ClientSide.OnLoading>
														<prop:ClientSide.OnFailure>showLoader();</prop:ClientSide.OnFailure>
														<prop:ClientSide.OnSuccess>showLoader();if(!J('.icon-sous-traitant')[0])J('#ctl0_CONTENU_PAGE_blocGroupementEntreprise_infoGroupementPanel').hide();</prop:ClientSide.OnSuccess>
													</com:TActiveLinkButton>
												</td>
											</tr>
										</prop:ItemTemplate>
									</com:TRepeater>

								</prop:ItemTemplate>
							</com:TRepeater>

						</tbody>
					</table>
					<div class="breaker"></div>
				</div>
			</prop:ItemTemplate>
		</com:TActiveRepeater>
	</com:TPanel>

	</div>
	<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
</div>
<com:TActiveLabel id="scriptJs" style="display:none"/>
<!--Fin Bloc
Groupement-->