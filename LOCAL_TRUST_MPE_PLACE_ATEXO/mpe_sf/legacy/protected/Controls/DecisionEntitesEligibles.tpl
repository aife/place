<!--Debut Bloc Identification de l'enchere-->
		<com:TActivePanel id="panelEntitesEligibles" CssClass="form-field" Style="display:none;" >
			<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
			<div class="content">
				<h3><com:TTranslate>TEXT_ENTITE_ELIGIBLES_AUX_MARCHES</com:TTranslate></h3>
				<com:THiddenField ID="isMyEntity" />
				<span id="spanMyEntity">
					<div class="line">
					<div class="intitule-220">
						<com:TTranslate>TEXT_AGENT_INVITE_RATTACHE_CONTRAT</com:TTranslate>
					</div>
					<div class="content-bloc bloc-500">
						<com:TLabel ID="myEntityPurchese"/>
						<div class="spacer-mini"></div>
						<com:TActiveHyperLink
								id="urlChoixEntitesNew"
								NavigateUrl="javascript:popUpSetSize('index.php?page=Agent.PopupDecisionEntiteEligible','850px','750px','yes','popupDecisionEntiteEligibleNew');"
								cssclass="bouton-edit clear-both">
							 <com:TTranslate>DEFINE_TEXT_EDITER</com:TTranslate>
						</com:TActiveHyperLink>
					</div>
				</div>
				</span>

				<span id="spanInvitePermanent" style="display:none">
					<com:TActivePanel ID="panelInvitePermanent" cssClass="line">
						<div class="intitule">
							<com:TTranslate>TEXT_AGENT_INVITE_PERMANENT_CONTRAT</com:TTranslate>
						</div>
						<div class="content-bloc bloc-550">
							<com:TLabel ID="listInvite"/>
							<com:TRepeater ID="repeaterSelectedServices" >
								<prop:ItemTemplate>
									<div class="intitule-auto clear-both">
											<%#$this->Data->getLibelle()%> <br />
											<com:THiddenField
													ID="acronymeService"
													Value="<%#$this->Data->getId()%>"/>
									</div>
									<div class="spacer-mini"></div>
								</prop:ItemTemplate>
    						</com:TRepeater>


							<com:TActiveHyperLink
									id="urlChoixInvite"
									NavigateUrl="javascript:popUpSetSize('index.php?page=Agent.PopupDecisionEntiteEligible&idEntite=<%=$this->idInvite->getText()%>&isMyEntity=<%=$this->isMyEntity->getValue()%>','850px','750px', 'yes','popupDecisionEntiteEligible');"
									cssclass="bouton-edit clear-both">
								<com:TTranslate>DEFINE_TEXT_EDITER</com:TTranslate>
							</com:TActiveHyperLink>
						</div>
						<span style="display:none">
							<com:TTextBox ID="idInvite" />
							 <com:TActiveButton
									 ID="inviteEligible"
									 OnCommand="Page.decisionEntitesEligibles.displaySelectedInviteEligibles"
									 OnCallBack="Page.decisionEntitesEligibles.refreshSelectedInviteEligibles"
							 />
						</span>
					</com:TActivePanel>
				</span>

				<span id="spanEntityPurchese" style="display:none">
    				<com:TActivePanel ID="panelEntityPurchese" cssClass="line">
    					<div class="intitule-220"><com:TTranslate>TEXT_AGENT_RATTACHE_ORGANISME</com:TTranslate></div>
    					<div class="content-bloc bloc-550">
    						<com:TRepeater ID="repeaterSelectedEntitys" >
								<prop:ItemTemplate>
									<div class="intitule-auto clear-both">
											<%#$this->Data->getDenominationOrg()%> <br />
											<com:THiddenField ID="acronymeEntite" Value="<%#$this->Data->getAcronyme()%>"/>
									</div>
									<div class="spacer-mini"></div>
								</prop:ItemTemplate>
    						</com:TRepeater>
    				
    						<div class="spacer-mini"></div>

							<com:TActiveHyperLink
									id="urlChoixEntites"
									NavigateUrl="javascript:popUpSetSize('index.php?page=Agent.PopupDecisionEntiteEligible&idEntite=<%=$this->idEntite->getText()%>&isMyEntity=<%=$this->isMyEntity->getValue()%>','850px','750px', 'yes','popupDecisionEntiteEligible');"
									cssclass="bouton-edit clear-both">
								<com:TTranslate>DEFINE_TEXT_EDITER</com:TTranslate>
							</com:TActiveHyperLink>
    					</div>
						<span style="display:none">
							<com:TTextBox ID="idEntite" />
							 <com:TActiveButton
									 ID="entiteEligible"
									 OnCommand="Page.decisionEntitesEligibles.displaySelectedEntitesEligibles"
									 OnCallBack="Page.decisionEntitesEligibles.refreshSelectedEntitesEligibles"
							 />
						</span>
               		</com:TActivePanel>
           		</span>
			</div>
			<com:THiddenField ID="labelUpdate"/>
			<com:TLabel ID="script"/>
			<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
		</com:TActivePanel>

<!--Fin Bloc Identification de l'enchere-->