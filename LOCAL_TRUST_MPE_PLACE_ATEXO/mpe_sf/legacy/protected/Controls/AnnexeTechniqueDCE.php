<?php

namespace Application\Controls;

use Application\Propel\Mpe\CommonConsultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureEquivalence;
use Application\Service\Atexo\DossierVolumineux\Atexo_DossierVolumineux_Responses;

/**
 * Created by PhpStorm.
 * User: atexo
 * Date: 05/03/2019
 * Time: 12:47.
 */
class AnnexeTechniqueDCE extends MpeTTemplateControl
{
    public bool $updatemode = false;

    /**
     * Permet charger la lite des dossiers volumineux.
     *
     * @param $idDossier: ancienne valeur du type d'acces a la consultation
     * @param CommonConsultation|null $consultation
     */
    public function displayListeDossierVolumineux($idDossier, CommonConsultation $consultation = null): void
    {
        if (Atexo_Module::isEnabled('ModuleEnvol') && Atexo_CurrentUser::hasHabilitation('GestionEnvol')) {
            if ($consultation !== NULL) {
                if ($consultation->getId() === NULL && !isset($_GET['id'])) {
                    $procEquivalence = Atexo_Consultation_ProcedureEquivalence::retrieveProcedureEquivalenceById($consultation->getIdTypeProcedureOrg());
                    if (!$procEquivalence) {
                        $this->visibilityAndCheckAnnexeDce(false, true, false);
                    } else {
                        if ($procEquivalence->getAutorisationDossiersVolumineuxEnvolNon()) {
                            $this->visibilityAndCheckAnnexeDce(false, true, false);
                        } else {
                            $this->visibilityAndCheckAnnexeDce(true, false, true);
                        }
                    }
                }

                if ($consultation->getId() > 0 && isset($_GET['id']) && $_GET['id'] != '') {
                    if ($consultation->getEnvolActivation()) {
                        $this->visibilityAndCheckAnnexeDce(true, false, true);
                    } else {
                        $this->visibilityAndCheckAnnexeDce(false, true, false);
                    }
                }
            }

            $dossiersVolumineux = Atexo_DossierVolumineux_Responses::getListeDossiersVolumineuxActifs(Atexo_CurrentUser::getId(), Atexo_CurrentUser::isAgent());
            $this->listeDossierVolumineux->Datasource = $dossiersVolumineux;
            $this->listeDossierVolumineux->dataBind();
            $idDossier = (isset($dossiersVolumineux[$idDossier])) ? $idDossier : 0;
            $this->listeDossierVolumineux->setSelectedValue($idDossier);
            $this->panelDossierVolumineux->setVisible(true);
        } else {
            $this->panelDossierVolumineux->setVisible(false);
        }
    }

    public function getSelectedValue()
    {
        return $this->listeDossierVolumineux->getSelectedValue() ?: null;
    }

    public function setUpdatemode($isUpdateMode)
    {
        $this->updatemode = $isUpdateMode;
    }

    public function getUpdatemode()
    {
        return $this->updatemode;
    }

    public function toggleListeDossiersVolumineux(): void
    {
        if ($this->autorisationDossiersVolumineuxEnvolNo->Checked) {
            $this->panelListeDossierVolumineux->setVisible(false);
        } else {
            $this->panelListeDossierVolumineux->setVisible(true);
        }
    }

    public function visibilityAndCheckAnnexeDce(bool $visibilityListeDossierVolumineux, bool $envolNoCheck, bool $envolYesCheck): void
    {
        $this->panelListeDossierVolumineux->setVisible($visibilityListeDossierVolumineux);
        $this->autorisationDossiersVolumineuxEnvolNo->Checked = $envolNoCheck;
        $this->autorisationDossiersVolumineuxEnvolYes->Checked = $envolYesCheck;
    }
}
