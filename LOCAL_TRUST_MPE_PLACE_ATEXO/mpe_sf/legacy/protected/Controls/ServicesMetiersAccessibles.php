<?php

namespace Application\Controls;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonAgentPeer;
use Application\Propel\Mpe\CommonAgentServiceMetier;
use Application\Propel\Mpe\CommonAgentServiceMetierPeer;
use Application\Propel\Mpe\CommonHabilitationAgentPeer;
use Application\Propel\Mpe\CommonSocleHabilitationAgentPeer;
use Application\Service\Atexo\Agent\Atexo_Agent_EnablingsAgents;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Socle\Atexo_Socle_AgentServicesMetiers;
use Exception;
use Prado\Prado;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class ServicesMetiersAccessibles extends MpeTPage
{
    protected $_idAgent;
    protected $_org;
    protected $_idProfileInterne;

    public bool $_afficherProfil = true;

    public function onInit($param)
    {
        Atexo_Languages::setLanguageCatalogue('fr');
    }

    public function setAfficherProfil($value)
    {
        $this->_afficherProfil = $value;
    }

    public function getAfficherProfil()
    {
        return $this->_afficherProfil;
    }

    public function setIdAgent($value)
    {
        $this->_idAgent = $value;
    }

    public function setOrg($value)
    {
        $this->_org = $value;
    }

    public function getOrg()
    {
        return $this->_org;
    }

    public function onLoad($param)
    {
        if ('servicesMetiersAccessibles' != $this->Id) {
            throw new Exception('Compenant name must be servicesMetiersAccessibles');
        }
    }

    public function showServicesMetiersAccessiblesCompenant()
    {
        return Atexo_Module::isEnabled('SocleInterne', Atexo_CurrentUser::getCurrentOrganism());
    }

    public function displayCompenenant()
    {
        $serviceMetier = self::remplirDataSourceServicesMetiersAgent();
        $this->serviceMetier->DataSource = $serviceMetier;
        $this->serviceMetier->DataBind();
        //if($this->_idAgent!='') {
        $this->getJavaScriptProfil(is_countable($serviceMetier) ? count($serviceMetier) : 0, false);
        //}
    }

    public function displayOrganismeCompenenant()
    {
        $serviceMetier = self::remplirDataSourceServicesMetiersOrganisme();
        $this->serviceMetier->DataSource = $serviceMetier;
        $this->serviceMetier->DataBind();
    }

    public function onEnregistrerClick($idAgent)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $agentModified = false;
        foreach ($this->serviceMetier->getItems() as $item) {
            $idServiceMetier = $item->idService->Value;
            if (true == $item->service->Checked) {
                $paramsProfile = explode('-', $item->profils->getSelectedValue());
                $idProfileInterne = $paramsProfile[0];
                $this->setProfilId($idProfileInterne);
                if ('' != $idProfileInterne && '0' != $idProfileInterne) {
                    $serviceAgent = '';
                    if ($this->_idAgent) {
                        $serviceAgent = Atexo_Socle_AgentServicesMetiers::retrieveServiceAgentByIdAgentByService($this->_idAgent, $idServiceMetier);
                    }

                    $serviceMetier = CommonAgentServiceMetierPeer::retrieveByPK($this->_idAgent, $idServiceMetier, $connexionCom);
                    if (0 != (is_countable($serviceAgent) ? count($serviceAgent) : 0) && $serviceMetier instanceof CommonAgentServiceMetier) {
                        $serviceMetier->setDateModification(date('Y-m-d H:i:s'));
                    } else {
                        $serviceMetier = new CommonAgentServiceMetier();
                        $serviceMetier->setDateCreation(date('Y-m-d H:i:s'));
                        $serviceMetier->setDateModification(date('Y-m-d H:i:s'));
                    }

                    if ($serviceMetier instanceof CommonAgentServiceMetier) {
                        $serviceMetier->setIdAgent($idAgent);
                        $serviceMetier->setIdServiceMetier($idServiceMetier);
                        $serviceMetier->setIdProfilService($idProfileInterne);
                        // print_r($serviceMetier);exit;
                        $serviceMetier->save($connexionCom);
                    }
                    //traitement unique pour le service MPE
                    if ($idServiceMetier == Atexo_Config::getParameter('SERVICE_METIER_MPE')) {
                        $newHablitation = Atexo_Agent_EnablingsAgents::addOrModifyEnablingsOfTheAgents($idProfileInterne, $idAgent);
                        $newHablitation->setIdAgent($idAgent);
                        $newHablitation->save($connexionCom);
                    }
                    if ($idServiceMetier == Atexo_Config::getParameter('SERVICE_METIER_SOCLE')) {
                        $newHablitationSocle = (new Atexo_Agent_EnablingsAgents())->addOrModifyEnablingsSocleOfAgent($idAgent);
                        $newHablitationSocle->setIdAgent($idAgent);
                        $newHablitationSocle->save($connexionCom);
                    }
                    $agentModified = true;
                }
            } else {
                $AgentServiceMetier = Atexo_Socle_AgentServicesMetiers::retrieveServiceAgentByIdAgentByService($idAgent, $idServiceMetier);
                if ($AgentServiceMetier) {
                    CommonAgentServiceMetierPeer::doDelete([$this->_idAgent, $idServiceMetier], $connexionCom);
                    if ($idServiceMetier == Atexo_Config::getParameter('SERVICE_METIER_MPE')) {
                        CommonHabilitationAgentPeer::doDelete($this->_idAgent, $connexionCom);
                    }
                    if ($idServiceMetier == Atexo_Config::getParameter('SERVICE_METIER_SOCLE')) {
                        CommonSocleHabilitationAgentPeer::doDelete($this->_idAgent, $connexionCom);
                    }
                    $agentModified = true;
                }
            }
        }
        if ($agentModified) {
            $agent = CommonAgentPeer::retrieveByPK($idAgent, $connexionCom);
            $agent->setDateModification(date('Y-m-d H:i:s'));
            $agent->save($connexionCom);
        }
    }

    /*
     * créer javascript pour controle du profil
     *
     */

    public function getJavaScriptProfil($count, $enabled)
    {
        $this->Script->Text = '<script language="javascript" type="text/javascript">';
        for ($i = 0; $i <= $count; ++$i) {
            $this->Script->Text .= 'function controleProfil'.$i.'(){';
            if (!$enabled) {
                $this->Script->Text .= 'return true;}';
            } else {
                $this->Script->Text .= "profil = document.getElementById('ctl0_CONTENU_PAGE_servicesMetiersAccessibles_serviceMetier_ctl".($i)."_profils');".
                 "service = document.getElementById('ctl0_CONTENU_PAGE_servicesMetiersAccessibles_serviceMetier_ctl".($i)."_service');".
                'valueProfil = profil.options[profil.selectedIndex].value;if((service.checked==true)&&(valueProfil==0)) return false;return true;}';
            }
        }
        $this->Script->Text .= '</script>';
    }

    /**
     * validateur qui verifi que le service selectionné est déjà attribué à l'agent avec un profil.
     *
     * @param unknown_type $sender
     * @param unknown_type $param
     */
    public function verifyServicesMetiers($sender, $param)
    {
        $itemIndex = $sender->parent->parent->getItemIndex();
        foreach ($this->serviceMetier->getItems() as $item) {
            if ($item->getItemIndex() == $itemIndex) {
                if (true == $item->service->Checked) {
                    $idServiceMetier = $item->idService->Value;
                    $paramsProfile = explode('-', $item->profils->getSelectedValue());
                    $idProfileInterne = $paramsProfile[0];
                    $serviceAgent = Atexo_Socle_AgentServicesMetiers::retrieveServiceAgentByIdAgentByService($this->_idAgent, $idServiceMetier);
                    if (0 == (is_countable($serviceAgent) ? count($serviceAgent) : 0)
                    && 0 == $idProfileInterne) {
                        $this->Page->labelServerSide->Text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";
                        $param->IsValid = false;
                        $item->profilsValidator->ErrorMessage = Prado::localize('TEXT_PROFIL');

                        return;
                    }
                }
            }
        }
    }

    public function remplirDataSourceServicesMetiersAgent()
    {
        $serviceMetier = (new Atexo_Socle_AgentServicesMetiers())->retrieveServicesMeiersAndProfils($this->_idAgent, false, (Atexo_Module::isEnabled('SocleInterne') && 'socle' == Atexo_CurrentUser::readFromSession('ServiceMetier')), $this->_org);

        return $serviceMetier;
    }

    public function remplirDataSourceServicesMetiersOrganisme()
    {
        $serviceMetier = (new Atexo_Socle_AgentServicesMetiers())->retrieveServicesByOrganisme($this->_org, Atexo_Module::isEnabled('SocleInterne'));

        return $serviceMetier;
    }

    public function setProfilId($idProfileInterne)
    {
        $this->_idProfileInterne = $idProfileInterne;
    }

    public function getProfilId()
    {
        return $this->_idProfileInterne;
    }
}
