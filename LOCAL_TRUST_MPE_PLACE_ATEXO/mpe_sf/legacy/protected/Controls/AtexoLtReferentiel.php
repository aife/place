<?php

namespace Application\Controls;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonReferentielAgent;
use Application\Propel\Mpe\CommonReferentielConsultation;
use Application\Propel\Mpe\CommonReferentielEntreprise;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Ref;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Controller\Atexo_Controller_Front;
use Application\Service\Atexo\Referentiel\Atexo_Referentiel_Referentiel;
use Application\Service\Atexo\Referentiel\Atexo_Referentiel_ReferentielVo;

/**
 * commentaires.
 *
 * @author amal EL BEKKAOUI <amal.elbekkaoui@atexo.com>
 * @copyright Atexo 2011
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class AtexoLtReferentiel extends MpeTTemplateControl
{
    private $organisme;
    private bool $consultation = false;
    private bool $lot = false;
    private bool $obligatoire = false;
    private bool $typeSearch = false;
    private $validationGroup;
    private $validationSummary;
    private $cssClass;
    private bool $loadData = false;
    private $mode;
    private bool $entreprise = false;
    private $idPage;
    private $type;
    private bool $agent = false;
    private $idObject;
    private string $afficherInfoBulle = '1';

    public function setAfficherInfoBulle($afficherInfoBulle)
    {
        $this->afficherInfoBulle = $afficherInfoBulle;
    }

    public function getAfficherInfoBulle()
    {
        return $this->afficherInfoBulle;
    }

    public function setConsultation($consultation)
    {
        $this->consultation = $consultation;
    }

    public function getConsultation()
    {
        return $this->consultation;
    }

    public function setAgent($agent)
    {
        $this->agent = $agent;
    }

    public function getAgent()
    {
        return $this->agent;
    }

    public function setLot($lot)
    {
        $this->lot = $lot;
    }

    public function getLot()
    {
        return $this->lot;
    }

    public function setObligatoire($obligatoire)
    {
        $this->obligatoire = $obligatoire;
    }

    public function getObligatoire()
    {
        return $this->obligatoire;
    }

    public function setTypeSearch($typeSearch)
    {
        $this->typeSearch = $typeSearch;
    }

    public function getTypeSearch()
    {
        return $this->typeSearch;
    }

    public function setValidationGroup($validationGroup)
    {
        $this->validationGroup = $validationGroup;
    }

    public function getValidationGroup()
    {
        return $this->validationGroup;
    }

    public function setValidationSummary($validationSummary)
    {
        $this->validationSummary = $validationSummary;
    }

    public function getValidationSummary()
    {
        return $this->validationSummary;
    }

    public function setCssClass($cssClass)
    {
        $this->cssClass = $cssClass;
    }

    public function getCssClass()
    {
        return $this->cssClass;
    }

    public function setMode($mode)
    {
        $this->mode = $mode;
    }

    public function getMode()
    {
        return $this->mode;
    }

    public function setEntreprise($entreprise)
    {
        $this->entreprise = $entreprise;
    }

    public function getEntreprise()
    {
        return $this->entreprise;
    }

    public function setIdPage($idPage)
    {
        $this->idPage = $idPage;
    }

    public function getIdPage()
    {
        return $this->idPage;
    }

    public function setType($type)
    {
        $this->type = $type;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setOrganisme($organisme)
    {
        $this->organisme = $organisme;
    }

    public function getOrganisme()
    {
        if (Atexo_CurrentUser::isAgent()) {
            $this->organisme = Atexo_CurrentUser::getCurrentOrganism();
        }

        return $this->organisme;
    }

    public function setIdObject($idObject)
    {
        $this->idObject = $idObject;
        if ($this->loadData) {
            if ($this->agent) {
                self::afficherReferentielAgent($this->idObject, $this->organisme, true);
            }
        }
    }

    public function getIdObject()
    {
        return $this->idObject;
    }

    public function setloadData($loadData)
    {
        $this->loadData = $loadData;
    }

    public function getloadData()
    {
        return $this->loadData;
    }

    public function getLtReferentiels($page, $organisme = null)
    {
        $type = 0;
        $ltRefs = Atexo_Referentiel_Referentiel::getLtReferentiels($this->entreprise, $this->consultation, $this->lot, $type, $page, $organisme);

        return $ltRefs;
    }

    public function saveReferentielConsultationAndLot($consultationId, $lot, &$consultation = false)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));

        $organisme = Atexo_CurrentUser::getCurrentOrganism();

        foreach ($this->RepeaterReferentiel->Items as $Item) {
            $ltReferentiel = $Item->idRef->value;
            $valeurPricLtRef = $Item->idAtexoRef->codeRefPrinc->value;
            $valeurSecLtRef = $Item->idAtexoRef->codesRefSec->value;
            if ($consultationId) {
                $CommonRefCons = (new Atexo_Referentiel_Referentiel())->getCommonLtReferentielByRefConsultationAndLot($consultationId, $lot, $ltReferentiel, $organisme);
            }
            if (!$CommonRefCons instanceof CommonReferentielConsultation) {
                $CommonRefCons = new CommonReferentielConsultation();
                $CommonRefCons->setLot($lot);
                $CommonRefCons->setIdLtReferentiel($ltReferentiel);
            }

            $CommonRefCons->setValeurPrincipaleLtReferentiel($valeurPricLtRef);
            $CommonRefCons->setValeurSecondaireLtReferentiel($valeurSecLtRef);
            if ($consultation instanceof CommonConsultation) {
                $consultation->addCommonReferentielConsultation($CommonRefCons);
            } else {
                $CommonRefCons->setReference($reference);
                $CommonRefCons->setOrganisme($organisme);
                $CommonRefCons->save($connexionCom);
            }
            unset($CommonRefCons);
        }
    }

    public function saveReferentielEntreprise($entreprise)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));

        foreach ($this->RepeaterReferentiel->Items as $Item) {
            $ltReferentiel = $Item->idRef->value;
            $valeurPricLtRef = $Item->idAtexoRef->codeRefPrinc->value;
            $valeurSecLtRef = $Item->idAtexoRef->codesRefSec->value;

            $refEntr = (new Atexo_Referentiel_Referentiel())->getLtReferentielByEntreprise($entreprise, $ltReferentiel);
            if (!$refEntr) {
                $refEntr = new CommonReferentielEntreprise();
                $refEntr->setIdEntreprise($entreprise);
                $refEntr->setIdLtReferentiel($ltReferentiel);
            }

            $refEntr->setValeurPrincipaleLtReferentiel($valeurPricLtRef);
            $refEntr->setValeurSecondaireLtReferentiel($valeurSecLtRef);
            $refEntr->save($connexionCom);
        }
    }

    public function getValueReferentielVo()
    {
        $value = [];
        foreach ($this->RepeaterReferentiel->Items as $Item) {
            $refVo = new Atexo_Referentiel_ReferentielVo();
            $refVo->setId($Item->idRef->value);
            $refVo->setValuePrinc($Item->idAtexoRef->codeRefPrinc->value);
            $refVo->setValueSecon($Item->idAtexoRef->codesRefSec->value);
            $refVo->setValue($Item->idAtexoRef->codeRefPrinc->value.$Item->idAtexoRef->codesRefSec->value);
            $refVo->setCodeLibelle($Item->codeLibelle->value);
            $refVo->setTypeSearch($Item->typeSearch->value);
            $refVo->setType($Item->typeRef->value);
            $value[$Item->idRef->value] = $refVo;
        }

        return $value;
    }

    public function afficherReferentielConsultation($consultationId = null, $lot = null, $organisme = null, $defineValue = null, $postback = false)
    {
        if (!$this->Page->isPostBack || $postback) {
            $nbrElement = self::remplirRepeaterReferentiel();
            if ($nbrElement) {
                foreach ($this->RepeaterReferentiel->Items as $Item) {
                    $ltReferentiel = $Item->idRef->value;
                    $codeRefPrinc = null;
                    $codesRefSec = null;
                    if (!$defineValue) {
                        if ($organisme) {
                            $refCons = (new Atexo_Referentiel_Referentiel())->getCommonLtReferentielByRefConsultationAndLot($consultationId, $lot, $ltReferentiel, $organisme);
                            if ($refCons) {
                                $codeRefPrinc = $refCons->getValeurPrincipaleLtReferentiel();
                                $codesRefSec = $refCons->getValeurSecondaireLtReferentiel();
                            }
                        }
                    } else {
                        if ($defineValue[$ltReferentiel]) {
                            $codeRefPrinc = $defineValue[$ltReferentiel]->getValuePrinc();
                            $codesRefSec = $defineValue[$ltReferentiel]->getValueSecon();
                        }
                    }

                    $atexo_ref = new Atexo_Ref();
                    $atexo_ref->setCheminFichierConfigXML($Item->chemin->value);
                    $atexo_ref->setPrincipal($codeRefPrinc);
                    $atexo_ref->setSecondaires($codesRefSec);
                    if ($Item->logoRef->value) {
                        $atexo_ref->setUrlLogo(Atexo_Controller_Front::t().'/'.$this->getNomLogoReferentiel($Item->logoRef->value));
                    }
                    $Item->idAtexoRef->afficherReferentiel($atexo_ref);
                }
            }
        }
    }

    public function afficherReferentielEntreprise($entreprise = null, $defineValue = null, $postback = false)
    {
        if (!$this->Page->isPostBack || $postback) {
            self::remplirRepeaterReferentiel();
            foreach ($this->RepeaterReferentiel->Items as $Item) {
                $ltReferentiel = $Item->idRef->value;
                $codeRefPrinc = null;
                $codesRefSec = null;
                if (!$defineValue) {
                    $refEntr = (new Atexo_Referentiel_Referentiel())->getLtReferentielByEntreprise($entreprise, $ltReferentiel);
                    if ($refEntr) {
                        $codeRefPrinc = $refEntr->getValeurPrincipaleLtReferentiel();
                        $codesRefSec = $refEntr->getValeurSecondaireLtReferentiel();
                    }
                } else {
                    $codeRefPrinc = $defineValue[$ltReferentiel]->getValuePrinc();
                    $codesRefSec = $defineValue[$ltReferentiel]->getValueSecon();
                }

                $atexo_ref = new Atexo_Ref();
                $atexo_ref->setCheminFichierConfigXML($Item->chemin->value);
                $atexo_ref->setPrincipal($codeRefPrinc);
                $atexo_ref->setSecondaires($codesRefSec);
                if ($Item->logoRef->value) {
                    $atexo_ref->setUrlLogo(Atexo_Controller_Front::t().'/'.$this->getNomLogoReferentiel($Item->logoRef->value));
                }
                $Item->idAtexoRef->afficherReferentiel($atexo_ref);
            }
        }
    }

    public function refreshReferentielConsultation()
    {
        foreach ($this->RepeaterReferentiel->getItems() as $Item) {
            $refPrinc = $Item->idAtexoRef->codeRefPrinc->value;
            $refSec = $Item->idAtexoRef->codesRefSec->value;
            if ('' != $refPrinc) {
                $atexo_ref = new Atexo_Ref();
                $atexo_ref->setCheminFichierConfigXML($Item->chemin->value);
                $atexo_ref->setPrincipal($refPrinc);
                $atexo_ref->setSecondaires($refSec);
                $Item->idAtexoRef->afficherReferentiel($atexo_ref);
                if ($Item->logoRef->value) {
                    $atexo_ref->setUrlLogo(Atexo_Controller_Front::t().'/'.$this->getNomLogoReferentiel($Item->logoRef->value));
                }
            }
        }
    }

    public function refreshReferentielAgent()
    {
        foreach ($this->RepeaterReferentiel->getItems() as $Item) {
            $refPrinc = $Item->idAtexoRef->codeRefPrinc->value;
            $refSec = $Item->idAtexoRef->codesRefSec->value;
            $cas = $Item->idAtexoRef->casRef->value;
            if ('' != $refPrinc || '' != $refSec) {
                $atexo_ref = new Atexo_Ref();
                $atexo_ref->setCheminFichierConfigXML($Item->chemin->value);
                $atexo_ref->setPrincipal($refPrinc);
                $atexo_ref->setSecondaires($refSec);
                $Item->idAtexoRef->setCas($cas);
                $Item->idAtexoRef->afficherReferentiel($atexo_ref);
                if ($Item->logoRef->value) {
                    $atexo_ref->setUrlLogo(Atexo_Controller_Front::t().'/'.$this->getNomLogoReferentiel($Item->logoRef->value));
                }
            } else {
                $atexo_ref = new Atexo_Ref();
                $atexo_ref->setCheminFichierConfigXML($Item->chemin->value);
                $Item->idAtexoRef->setCas($cas);
                $Item->idAtexoRef->afficherReferentiel($atexo_ref);
            }
        }
    }

    public function remplirRepeaterReferentiel()
    {
        if ('' == $this->idPage) {
            $this->idPage = Atexo_Util::atexoHtmlEntities($_GET['page']);
        }
        $organisme = self::getOrganisme();
        if ($organisme) {
            $dataSource = self::getLtReferentiels($this->idPage, $organisme);
        } else {
            $dataSource = self::getLtReferentiels($this->idPage);
        }
        if ($this->idObject) {
            //      print_r($dataSource);exit;
        }
        $this->RepeaterReferentiel->DataSource = $dataSource;
        $this->RepeaterReferentiel->DataBind();

        return is_countable($dataSource) ? count($dataSource) : 0;
    }

    public function getCas($item)
    {
        return match (intval($this->mode)) {
            0 => $item->getModeAffichage(),
            1 => $item->getModeModification(),
            2 => $item->getModeRecherche(),
            default => '',
        };
    }

    public function getValeurForXml()
    {
        $valueForXml = '';
        foreach ($this->RepeaterReferentiel->getItems() as $Item) {
            $baliseName = (new Atexo_Referentiel_Referentiel())->getNomAttribue($Item->codeLibelle->value);
            $refPrinc = $Item->idAtexoRef->codeRefPrinc->value;
            $refSec = $Item->idAtexoRef->codesRefSec->value;
            $valeurBalise = $refPrinc.$refSec;
            if ('' != $valeurBalise) {
                $valueForXml[$baliseName] = $valeurBalise;
            } else {
                $valueForXml[$baliseName] = 'NULL';
            }
        }

        return $valueForXml;
    }

    public function isVisible($item)
    {
        if ('cas2' == self::getCas($item)) {
            return true;
        }

        return false;
    }

    public function afficherReferentielAgent($idAgent = null, $organisme = null, $postback = false)
    {
        if (!$this->Page->isPostBack || $postback) {
            $nomreRefAgent = self::remplirRepeaterReferentiel();
            foreach ($this->RepeaterReferentiel->Items as $Item) {
                $ltReferentiel = $Item->idRef->value;
                $codeRefPrinc = null;
                $codesRefSec = null;
                $refAgent = (new Atexo_Referentiel_Referentiel())->getCommonLtReferentielByIdAgent($idAgent, $ltReferentiel, $organisme);
                if ($refAgent) {
                    $codeRefPrinc = $refAgent->getValeurPrincipaleLtReferentiel();
                    $codesRefSec = $refAgent->getValeurSecondaireLtReferentiel();
                }
                $atexo_ref = new Atexo_Ref();
                $atexo_ref->setCheminFichierConfigXML($Item->chemin->value);
                $atexo_ref->setPrincipal($codeRefPrinc);
                $atexo_ref->setSecondaires($codesRefSec);
                if ($Item->logoRef->value) {
                    $atexo_ref->setUrlLogo(Atexo_Controller_Front::t().'/'.$this->getNomLogoReferentiel($Item->logoRef->value));
                }
                $Item->idAtexoRef->afficherReferentiel($atexo_ref);
            }

            return $nomreRefAgent;
        }
    }

    public function saveReferentielAgent($idAgent, $organisme = false)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        if (!$organisme) {
            $organisme = Atexo_CurrentUser::getCurrentOrganism();
        }
        foreach ($this->RepeaterReferentiel->Items as $Item) {
            $ltReferentiel = $Item->idRef->value;
            $valeurPricLtRef = $Item->idAtexoRef->codeRefPrinc->value;
            $valeurSecLtRef = $Item->idAtexoRef->codesRefSec->value;
            $CommonRefAgent = (new Atexo_Referentiel_Referentiel())->getCommonLtReferentielByIdAgent($idAgent, $ltReferentiel, $organisme);
            if (!$CommonRefAgent) {
                $CommonRefAgent = new CommonReferentielAgent();
                $CommonRefAgent->setIdAgent($idAgent);
                $CommonRefAgent->setIdLtReferentiel($ltReferentiel);
                $CommonRefAgent->setOrganisme($organisme);
            }
            if ($valeurPricLtRef || $valeurSecLtRef) {
                $CommonRefAgent->setValeurPrincipaleLtReferentiel($valeurPricLtRef);
                $CommonRefAgent->setValeurSecondaireLtReferentiel($valeurSecLtRef);
                $CommonRefAgent->save($connexionCom);
            }
        }
    }

    // retourne le nom du grand logo du Lt_referentiel
    public function getNomLogoReferentiel($nomPetitLogo)
    {
        if ($nomPetitLogo) {
            $nomPetitLogo = substr($nomPetitLogo, 0, -4);
            $nomPetitLogo = $nomPetitLogo.'-referentiel.gif';

            return $nomPetitLogo;
        }
    }
}
