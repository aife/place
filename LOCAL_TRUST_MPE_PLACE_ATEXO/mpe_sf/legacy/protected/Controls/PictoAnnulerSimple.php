<?php

namespace Application\Controls;

use Prado\Prado;
use Prado\Web\UI\WebControls\TButton;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PictoAnnulerSimple extends TButton
{
    public function onLoad($param)
    {
        $this->setCssClass('btn btn-default btn-sm pull-left');
        $this->Text = 'Annuler';
        $this->Attributes->value = Prado::localize('ICONE_ANNULER');
        $this->setToolTip(Prado::localize('ICONE_ANNULER'));
    }
}
