<tr class="<%=$this->getClassFormulaire()%>">
	<td class="col-piece" headers="formOffre_col_01">
		<a href="javascript:void(0);" 
			onclick="togglePanelClass(this,'<%=$this->getIdHtlmToggleFormulaire()%>');" 
			class="title-toggle-open" style="<%=(is_array($this->getListeEditions()) && count($this->getListeEditions()))? 'display:;':'display:none;'%>"
		>
			<%=$this->getLibelleFormulaire()%>
			<span class="nb-docs">
				<com:TTranslate>DEFINE_NOMBRE_DOCUMENTS</com:TTranslate> : <%=$this->getNombreDocuments($this->getDossier())%>
			</span>
		</a>
		<div style="<%=(!is_array($this->getListeEditions()) || !count($this->getListeEditions()))? 'display:;':'display:none;'%>">
			<%=$this->getLibelleFormulaire()%>
			<span class="nb-docs">
				<com:TTranslate>DEFINE_NOMBRE_DOCUMENTS</com:TTranslate> : <%=$this->getNombreDocuments($this->getDossier())%>
			</span>
		</div>
	</td>
	<td headers="formCand_col_02" class="col-50 center">
		<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/<%=$this->getStatutPictoDossier($this->getDossier())%>" alt="<%=$this->getStatutDossier($this->getDossier())%>" title="<%=$this->getStatutDossier($this->getDossier())%>" class="statut"/>
	</td>
	<td class="col-100" style="display:<%#($this->getCallDepuisDemandeComplementTransmis() === '0')?'':'none'%>">
		<abbr title="Date de validation"><%=$this->getDateValidation($this->getDossier())%></abbr>
	</td>
	<td headers="formCand_col_03" class="actions">
		<!-- Début Editer/Visualiser -->
		<a
			href="#" title="<%=Prado::localize('TEXT_EDITER_FORMULAIRE')%>"
			onclick="javascript:document.cookie = 'JSESSIONID=; expires=Thu, 01 Jan 1970 00:00:00 GMT; path=/sub';popUpFormulaireSub('<%=$this->getUrl()%>','yes');"
			style="display:<%#($this->getCallDepuisDemandeComplementTransmis() === '0')?'':'none'%>"
		>
			<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-edit.gif" alt="<%=Prado::localize('TEXT_ACCEDER')%>"/>
		</a> 
		<a 
			href="#" title="<%=Prado::localize('DEFINE_VISUALISER')%>"
			onclick="javascript:document.cookie = 'JSESSIONID=; expires=Thu, 01 Jan 1970 00:00:00 GMT; path=/sub';javascript:popUpFormulaireSub('<%=$this->getUrl()%>','yes');"
			style="display:<%=($this->getCallDepuisDemandeComplementTransmis() === '1')?'':'none'%>" 
		>
			<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-details.gif" alt="<%=Prado::localize('DEFINE_VISUALISER')%>"/>
		</a> 
		<!-- Fin Editer/Visualiser -->
		<com:TActiveLinkButton 
			ID="boutonEnvoiDemandeComplement"
			attributes.alt="<%=Prado::localize('TRANSMETTRE_OFFRE_MODIFIEE')%>"
			attributes.title=<%=Prado::localize('TRANSMETTRE_OFFRE_MODIFIEE')%>"
			onCallBack="SourceTemplateControl.ouvrirPopInSuppressionDossier"
			ActiveControl.CallbackParameter="<%=$this->getIdComplementFormulaire()%>_<%=$this->getRefCons()%>_<%=$this->getOrganisme()%>" 			
			style="display:<%=($this->getCallDepuisDemandeComplementTransmis() === '0' && $this->getDossier() instanceof CommonTDossierFormulaire && $this->getDossier()->getTypeReponse() ==  Application\Service\Atexo\Atexo_Config::getParameter('TYPE_DOSSIER_DEMANDE_COMPLEMENT'))? '':'none'%>"
		>
			<img alt="<%=Prado::localize('TRANSMETTRE_OFFRE_MODIFIEE')%>"  title="<%=Prado::localize('TRANSMETTRE_OFFRE_MODIFIEE')%>" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-envoyer.gif"/>
		</com:TActiveLinkButton>
	</td>
</tr>

<com:TRepeater ID="repeaterListeEditions" DataSource="<%#$this->getListeEditions()%>">
      <prop:ItemTemplate>
      	   <tr style="display: table-row;" class="<%#(($this->ItemIndex%2==0))? 'row-piece-heritee ' . $this->SourceTemplateControl->getIdHtmlFormulaire():'on row-piece-heritee '.$this->SourceTemplateControl->getIdHtmlFormulaire()%>">
				<td class="<%#($this->ItemIndex != count($this->SourceTemplateControl->getListeEditions()) - 1)?'col-piece':'col-piece piece-last'%>">
					<div class="blue piece-type">
						<com:TImage
						 ImageUrl="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-type-form.png"
						 Visible="<%#($this->Data->getType() == Application\Service\Atexo\Atexo_Config::getParameter('TYPE_DOCUMENT_EDITION')) ? 'true' : 'false' %>"
						 />
						<com:TImage
						 ImageUrl="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-type-libre.png"
						 Visible="<%#($this->Data->getType() == Application\Service\Atexo\Atexo_Config::getParameter('TYPE_DOCUMENT_PJ')) ? true : false%>"
						 />
						<abbr title="<%#Prado::localize('TEXT_NOM_DU_DOCUMENT')%> : <%#($this->Data->getNomFichier())? $this->Data->getNomFichier():$this->Data->getLibelle()%>"> <%# $this->cutString($this->Data->getLibelle(),89)%></abbr>
					</div>
				</td>
				<td class="col-50 center">
					<img src="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-details-inactive.gif" alt="<%#Prado::localize('DEFINE_VISUALISER_INACTIF')%>" style="display:<%#(Application\Service\Atexo\FormulaireSub\Atexo_FormulaireSub_Edition::gestionActionEdition($this->Data, $this->SourceTemplateControl->getDossier()) == 'cas2')?'':'none'%>"/>
					<img 
						src="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-attente.gif" alt="<%#Prado::localize('TEXT_PROCESSUS_ASYNCHRONE_GENERATION_PLANIFIEE')%>"
						style="display:<%#($this->SourceTemplateControl->getCallDepuisDemandeComplementTransmis() === '0' && Application\Service\Atexo\FormulaireSub\Atexo_FormulaireSub_Edition::gestionActionEdition($this->Data, $this->SourceTemplateControl->getDossier()) == 'cas1')?'':'none'%>"
					/>
					<com:TLinkButton
						id="boutonTelechargerEdition" 
						Attributes.title="<%#Prado::localize('DEFINE_VISUALISER')%>"
						onCommand="SourceTemplateControl.telechargerEdition"
						CommandParameter="<%#$this->Data->getIdEditionFormulaire()%>"
						style="display:<%#($this->SourceTemplateControl->getCallDepuisDemandeComplementTransmis() === '0' && Application\Service\Atexo\FormulaireSub\Atexo_FormulaireSub_Edition::gestionActionEdition($this->Data, $this->SourceTemplateControl->getDossier()) == 'cas3')?'':'none'%>" >
							<img src="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-details.gif" alt="<%#Prado::localize('DEFINE_VISUALISER')%>"/>
					</com:TLinkButton>
				</td>
				<td class="col-100" style="display:<%#($this->SourceTemplateControl->getCallDepuisDemandeComplementTransmis() === '0')?'':'none'%>">
					<abbr title="<%#Prado::localize('DEFINE_DATE_GENERATION')%>"><%# Application\Service\Atexo\Atexo_Util::iso2frnDateTime($this->Data->getDateGeneration(),true)%></abbr>
				</td>
				<td class="actions">
					<com:TActiveLinkButton  
						style="display:<%#($this->SourceTemplateControl->getCallDepuisDemandeComplementTransmis() === '0' && Application\Service\Atexo\FormulaireSub\Atexo_FormulaireSub_Edition::gestionActionEdition($this->Data, $this->SourceTemplateControl->getDossier()) != 'cas3')?'':'none'%>"
						Attributes.title="<%#Prado::localize('DEFINE_GENERER_DOCUMENT')%>"
						onCallBack="SourceTemplateControl.ouvrirPopInGeneration"
     					ActiveControl.CallbackParameter="<%#$this->Data->getIdEditionFormulaire()%>_<%#$this->ItemIndex%>"
     					ID="boutonGenerationEdition"
					>
						<img src="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-ajout-doc.gif" alt="<%#Prado::localize('DEFINE_GENERER')%>"/>
					</com:TActiveLinkButton>
					<com:TLinkButton
						id="boutonTelechargerEditionComplement" 
						onCommand="SourceTemplateControl.telechargerEdition"
						CommandParameter="<%#$this->Data->getIdEditionFormulaire()%>"
						style="display:<%#($this->SourceTemplateControl->getCallDepuisDemandeComplementTransmis() === '1' /*&& Application\Service\Atexo\FormulaireSub\Atexo_FormulaireSub_Edition::gestionActionEdition($this->Data, $this->SourceTemplateControl->getDossier()) == 'cas3'*/)?'':'none'%>"
					>
						<img src="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-dl-dossier.gif" alt="<%#Prado::localize('DEFINE_TELECHARGER_EDITION')%>"/>
					</com:TLinkButton>
				</td>
			</tr>
      </prop:ItemTemplate>
</com:TRepeater>
<com:TActiveLabel ID="scriptJsPopInEnvoiDossier" />
<!-- Debut pop-in Envoi dossier -->	
<com:TActivePanel id="panelRaffraichissementPopInConfirmation">						
	<com:AtexoPopInConfirmation
		Message="<%=Prado::localize('TEXT_ENVOYER_MODIFICATIONS_DOSSIER')%>"
		Modal="modal-Envoi-Dossier<%=($this->getIdComplementFormulaire() != '' )? '-'.$this->getIdComplementFormulaire():''%>"
		FunctionCallBack="envoyerDemandeComplement"
	/>
</com:TActivePanel>	
