		<div class="form-field">
			<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
			<div class="content">
				<h3><com:TTranslate>DEFINE_HEADER_LIST_REPRESENTANT</com:TTranslate></h3>
					<com:TActivePanel CssClass="table-bloc" ID="PanelListRepresentant">
					<com:TRepeater ID="RepeaterListRepresentant" EnableViewState="true" DataKeyField="Id">
					<prop:HeaderTemplate>
					<table class="table-results" summary="<%=Prado::localize('DEFINE_SUMMURY_REPRESENTANT')%>">
					<caption><com:TTranslate>DEFINE_SUMMURY_REPRESENTANT</com:TTranslate></caption>
					<thead>
					<tr>
						<th class="top" colspan="8"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></th>
					</tr>
					<tr>
						<th class="col-100" id="nomRpa">
							<a href="#" class="on">
								<com:TActiveLinkButton
										OnCallback="Page.IDTRepeaterRPA.sortRpa"
										Text="<%=Prado::localize('DEFINE_NOM_MAJ')%>"
										ActiveControl.CallbackParameter="FirstName"/>
								<com:TActiveImageButton
										ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/arrow-tri.gif"
										OnCallback="Page.IDTRepeaterRPA.sortRpa"
										ActiveControl.CallbackParameter="FirstName"
										AlternateText="<%=Prado::localize('DEFINE_TRIER')%>"
										Attributes.title="<%=Prado::localize('DEFINE_TRIER')%>" />
							</a>
						</th>
						<th class="col-90" id="prenomRpa">
							    <com:TActiveLinkButton
										OnCallback="Page.sortRpa"
										Text="<%=Prado::localize('DEFINE_PRENOM')%>"
										ActiveControl.CallbackParameter="SecondName"/>
						</th>
						<th class="col-90" id="fonctionmRpa">
							    <com:TActiveLinkButton
										OnCallback="Page.sortRpa"
										Text="<%=Prado::localize('DEFINE_FONCTION_MAJ')%>"
										ActiveControl.CallbackParameter="Fonction"/>
						</th>
						<th class="col-200" id="coodonneesRpa">
								<com:TActiveLinkButton
										OnCallback="Page.sortRpa"
										Text="<%=Prado::localize('DEFINE_CORDONNEE_MAJ')%>"
										ActiveControl.CallbackParameter="Coordonnee"/>
						</th>
						<com:TLabel Visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('AjoutRpaChampFax') || Application\Service\Atexo\Atexo_Module::isEnabled('AjoutRpaChampTelephone') || Application\Service\Atexo\Atexo_Module::isEnabled('AjoutRpaChampEmail'))%>">
							<th class="'col-150" id="contact" ><com:TTranslate>CONTACT</com:TTranslate></th>
						</com:TLabel>
						<th class="actions" id="modifRpa">
								<com:TTranslate>DEFINE_TEXT_MODIFIER</com:TTranslate>
						</th>
						<th class="actions" id="suppRpa">
								<com:TTranslate>DEFINE_TEXT_SUPPRIMER</com:TTranslate>
						</th>
					</tr>
					</thead>
					</prop:HeaderTemplate>

					<prop:ItemTemplate>
						<tr class="<%#($this->ItemIndex%2)?'on':''%>">
							<td class="col-100" headers="nomRpa"><%#$this->Data->getNom()%></td>
							<td class="col-90" headers="prenomRpa"><%#$this->Data->getPrenom()%></td>
							<td class="col-100" headers="fonctionmRpa"><%#$this->Data->getFonction()%></td>
							<td class="col-150" headers="coodonneesRpa"><%#$this->Data->getCoordonne()%></td>
							<com:TLabel Visible="<%#(Application\Service\Atexo\Atexo_Module::isEnabled('AjoutRpaChampFax') || Application\Service\Atexo\Atexo_Module::isEnabled('AjoutRpaChampTelephone') || Application\Service\Atexo\Atexo_Module::isEnabled('AjoutRpaChampEmail'))%>">
								<td class="col-200" headers="retrait_el_contact">
									<com:TLabel Visible="<%#Application\Service\Atexo\Atexo_Module::isEnabled('AjoutRpaChampEmail')%>">
										<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-email.gif" alt="<%=Prado::localize('ADRESSE_ELECTRONIQUE')%>" title="<%=Prado::localize('ADRESSE_ELECTRONIQUE')%>" /><span class="ltr"><%#($this->Data->getEmail())? $this->Data->getEmail():'-'%></span><br />
									</com:TLabel>
									<com:TLabel Visible="<%#Application\Service\Atexo\Atexo_Module::isEnabled('AjoutRpaChampTelephone')%>">
										<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-tel.gif" alt="<%=Prado::localize('TEXT_TELEPHONE')%>" title="<%=Prado::localize('TEXT_TELEPHONE')%>" /> <span class="ltr"><%#($this->Data->getTelephone())? $this->Data->getTelephone():'-'%></span><br />
									</com:TLabel>
									<com:TLabel Visible="<%#Application\Service\Atexo\Atexo_Module::isEnabled('AjoutRpaChampFax')%>">
										<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-fax.gif" alt="<%=Prado::localize('TEXT_TELECOPIEUR')%>" title="<%=Prado::localize('TEXT_TELECOPIEUR')%>" /> <span class="ltr"><%#($this->Data->getFax())? $this->Data->getFax():'-'%></span>
									</com:TLabel>
								</td>
							</com:TLabel>
							<td class="actions" headers="modifRpa">
							<com:TPanel visible="<%#$this->Page->getViewState('entiteValide')%>">
								<a href='javascript:popUp("?page=Agent.popUpAjoutRPA&IdRpa=<%#$this->Data->getId()%>&org=<%#$this->Page->getViewState("organismeRpa")%>","yes");'><img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-edit.gif" alt="<%=Prado::localize('ICONE_DETAIL_MODIFIER')%>" title="<%=Prado::localize('ICONE_DETAIL_MODIFIER')%>" /></a></td>
							</com:TPanel>
							<td class="actions" headers="suppRpa">
								<com:TPanel visible="<%#$this->Page->getViewState('entiteValide')%>">
									<com:TConditional condition="is_countable($arrayServicesVaide) && !in_array($IdEntite,$arrayServicesVaide)">
										<prop:trueTemplate>
											<a href="#">
												<com:TActiveImageButton
														ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-supprimer-big.gif"
														Enabled="<%#$this->Page->getViewState('entiteValide')%>"
														Attributes.onclick="if(!confirm('<%=Prado::localize('DEFINE_TEXT_QUESTION_SUPPRIMER_AGENT')%>')) return false;"
														onCallBack="Page.IDTRepeaterRPA.refreshRepeaterRpa"
														OnCommand="Page.IDTRepeaterRPA.onDeleteClick"
														CommandParameter="<%#$this->Data->getId()%>"
														AlternateText="<%=Prado::localize('DEFINE_SUPPRIMER')%>"
														Attributes.title="<%=Prado::localize('DEFINE_SUPPRIMER')%>"
														cssClass="disabled-input"
												/>
											</a>
										</prop:trueTemplate>
										<prop:falseTemplate>
											<a href="#">
												<com:TActiveImageButton
														ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-supprimer-big.gif"
														Attributes.onclick="if(!confirm('<%=Prado::localize('DEFINE_TEXT_QUESTION_SUPPRIMER_AGENT')%>')) return false;"
														onCallBack="Page.IDTRepeaterRPA.refreshRepeaterRpa"
														OnCommand="Page.IDTRepeaterRPA.onDeleteClick"
														CommandParameter="<%#$this->Data->getId()%>"
														AlternateText="<%=Prado::localize('DEFINE_SUPPRIMER')%>"
														Attributes.title="<%=Prado::localize('DEFINE_SUPPRIMER')%>"
												/>
											</a>
										</prop:falseTemplate>
									</com:TConditional>
								</com:TPanel>
							</td>
						</tr>
					</prop:ItemTemplate>
						<prop:FooterTemplate>
						</table>
						</prop:FooterTemplate>
						</com:TRepeater>
							<com:TPanel visible="<%=$this->entiteValide%>">
								<a href='javascript:popUp("?page=Agent.popUpAjoutRPA&IdEntite=<%=$this->Page->getViewState("idEntiteRpa")%>&org=<%=$this->Page->getViewState("organismeRpa")%>","yes");' class="ajout-el float-left"><com:TTranslate>AJOUTER_UN_RPA</com:TTranslate></a>
							</com:TPanel>
								<span style ="display:none;">
									<com:TActiveButton
										Id="validateRpa"
										Enabled="<%=$this->entiteValide%>"
										onCallBack = "Page.IDTRepeaterRPA.refreshRepeaterRpa"
										ActiveControl.CallbackParameter=<%=$this->_ID%>
										/>
								</span>
					</com:TActivePanel>
				<div class="breaker"></div>
			</div>
			<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
		</div>
<!--Fin Bloc Gestion des RPA-->