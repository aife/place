<?php

namespace Application\Controls;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonOffrePapier;
use Application\Propel\Mpe\CommonOffrePapierQuery;
use Application\Propel\Mpe\CommonOffres;
use Application\Propel\Mpe\CommonOffresQuery;
use Application\Propel\Mpe\CommonTDocumentEntreprise;
use Application\Propel\Mpe\CommonTDocumentType;
use Application\Propel\Mpe\CommonTDocumentTypeQuery;
use Application\Propel\Mpe\CommonTEntrepriseDocumentVersion;
use Application\Propel\Mpe\CommonTEtablissement;
use Application\Propel\Mpe\CommonTEtablissementQuery;
use Application\Propel\Mpe\Entreprise;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Entreprise;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Attestation;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Etablissement;
use Prado\Prado;

/**
 * commentaires.
 *
 * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
 * @copyright Atexo 2015
 *
 * @version 1.0
 *
 * @since 4.10.0
 */
class AtexoDisponibiliteAttestation extends MpeTTemplateControl
{
    protected ?int $idEntreprise = null;
    protected ?int $typeDoc = null;
    protected bool $responsive = false;
    protected array $lastVersion = [];
    protected $idOffre;
    protected $typeEnv;
    protected $consultationId;
    protected $_organisme;
    protected $callFromOA; /*Precise si le composant est appele depuis la page ouverture et analyse*/

    public const INPI_URL = 'https://data.inpi.fr/export/companies?format=pdf&ids=[__SIRET__]';
    public const API_RNM_URL = 'https://api-rnm.artisanat.fr/v2/entreprises/__SIRET__';
    public const FORMAT_FILE_PDF = '?format=pdf';

    /**
     * recupere la valeur du [typeDoc].
     *
     * @return int la valeur courante [typeDoc]
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.10.0
     *
     * @copyright Atexo 2015
     */
    public function getTypeDoc()
    {
        return $this->typeDoc;
    }

    /**
     * modifie la valeur de [typeDoc].
     *
     * @param int $typeDoc la valeur a mettre
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.10.0
     *
     * @copyright Atexo 2015
     */
    public function setTypeDoc($typeDoc)
    {
        return $this->typeDoc = $typeDoc;
    }

    /**
     * recupere la valeur du [idEntreprise].
     *
     * @return int la valeur courante [idEntreprise]
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.10.0
     *
     * @copyright Atexo 2015
     */
    public function getIdEntreprise()
    {
        $idEntreprise = $this->getViewState('idEntreprise');
        if (empty($idEntreprise)) {
            $idEntreprise = $this->IdEntreprise;
        }

        return $idEntreprise;
    }

    /**
     * modifie la valeur de [idEntreprise].
     *
     * @param int $idEntreprise la valeur a mettre
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.10.0
     *
     * @copyright Atexo 2015
     */
    public function setIdEntreprise($idEntreprise)
    {
        $this->idEntreprise = $idEntreprise;
        $this->setViewState('idEntreprise', $idEntreprise);
        if (!$this->responsive) {
            $this->getDatasource();
        }
    }

    /**
     * Recupere la valeur de [idOffre].
     *
     * @return mixed
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function getIdOffre()
    {
        $idOffre = $this->getViewState('idOffre');
        if (empty($idOffre)) {
            $idOffre = $this->idOffre;
        }

        return $idOffre;
    }

    /**
     * Affecte la valeur de [idOffre].
     *
     * @param mixed $idOffre
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function setIdOffre($idOffre)
    {
        $this->setViewState('idOffre', $idOffre);
        $this->idOffre = $this->getViewState('idOffre');
    }

    /**
     * Recupere la valeur de [typeEnv].
     *
     * @return mixed
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function getTypeEnv()
    {
        $typeEnv = $this->getViewState('typeEnv');
        if (empty($typeEnv)) {
            $typeEnv = $this->typeEnv;
        }

        return $typeEnv;
    }

    /**
     * Affecte la valeur de [typeEnv].
     *
     * @param mixed $typeEnv
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function setTypeEnv($typeEnv)
    {
        $this->setViewState('typeEnv', $typeEnv);
        $this->typeEnv = $this->getViewState('typeEnv');
    }

    /**
     * @return mixed
     */
    public function getRefCons()
    {
        $consultationId = $this->getViewState('refCons');
        if (empty($consultationId)) {
            $consultationId = $this->refCons;
        }

        return $consultationId;
    }

    /**
     * @param mixed $consultationId
     */
    public function setRefCons($consultationId)
    {
        $this->setViewState('refCons', $consultationId);
        $this->refCons = $this->getViewState('refCons');
    }

    /**
     * @return mixed
     */
    public function getOrganisme()
    {
        $organisme = $this->getViewState('organisme');
        if (empty($organisme)) {
            $organisme = $this->_organisme;
        }

        return $organisme;
    }

    /**
     * @param mixed $organisme
     */
    public function setOrganisme($organisme)
    {
        $this->setViewState('organisme', $organisme);
        $this->_organisme = $this->getViewState('organisme');
    }

    /**
     * @return string
     */
    public function getCallFromOA()
    {
        $callFromOA = $this->getViewState('callFromOA');
        if (empty($callFromOA)) {
            $callFromOA = $this->callFromOA;
        }

        return $callFromOA;
    }

    /**
     * @param string $callFromOA
     */
    public function setCallFromOA($callFromOA)
    {
        $this->setViewState('callFromOA', $callFromOA);
        $this->callFromOA = $this->getViewState('callFromOA');
    }

    /**
     * recuperer la liste des dooc a affichier (datasource du repeater).
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.10.0
     *
     * @copyright Atexo 2015
     */
    public function getDatasource()
    {
        if (!$this->responsive) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            //on donne la liste des attestaions à afficher
            $docTypesQuery = new CommonTDocumentTypeQuery();
            $docTypes = $docTypesQuery->getListeTypesDocumentsByNatureDoc(Atexo_Config::getParameter('SGMAP_NATURE_DOCUMENT_PRINCIPAL'), $connexion);
            $this->repeaterDispoAttestations->DataSource = $docTypes;
            $this->repeaterDispoAttestations->DataBind();
        }
    }

    /**
     * recuperer la derniere version d un type de document.
     *
     * @param int    $identifiant   l'id de l entreprise/etablissement
     * @param int    $typeDoc       le type de document
     * @param string $sourceTypeDoc : precise si le document provient de l'entreprise ou etablissement
     *                              => $sourceTypeDoc = Atexo_Config::getParameter('TYPE_DOCUMENT_SGMAP_ENTREPRISE')    : document entreprise
     *                              => $sourceTypeDoc = Atexo_Config::getParameter('TYPE_DOCUMENT_SGMAP_ETABLISSEMENT') : document etablissement
     *
     * @return CommonTEntrepriseDocumentVersion $lastVersionDocument
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.10.0
     *
     * @copyright Atexo 2015
     */
    public function getLastVersionByTypeDoc($identifiant, $typeDoc, $sourceTypeDoc = '1')
    {
        $document = null;
        if (!($this->lastVersion[$typeDoc.'-'.$identifiant] instanceof CommonTDocumentEntreprise)) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            if ($sourceTypeDoc == Atexo_Config::getParameter('TYPE_DOCUMENT_SGMAP_ENTREPRISE')) {
                $document = (new Atexo_Entreprise_Attestation())->retrieveDocumentEntreprise($identifiant, $typeDoc);
            } elseif ($sourceTypeDoc == Atexo_Config::getParameter('TYPE_DOCUMENT_SGMAP_ETABLISSEMENT')) {
                $document = (new Atexo_Entreprise_Attestation())->retrieveDocumentEtablissement($identifiant, $typeDoc);
            }
            if ($document instanceof CommonTDocumentEntreprise) {
                $this->lastVersion[$typeDoc.'-'.$identifiant] = (new Atexo_Entreprise_Attestation())->retrieveLastVersionDocumentEntreprise($document->getIdDerniereVersion());
            }
        }

        return $this->lastVersion[$typeDoc.'-'.$identifiant];
    }

    /**
     * retourne l'url de telechargement de la derniere version d'un typepe de document.
     *
     * @param int    $typeDoc       le type de document
     * @param int    $identifiant   l'id de l entreprise/etablissement
     * @param string $sourceTypeDoc : precise si le document provient de l'entreprise ou etablissement
     *                              => $sourceTypeDoc = Atexo_Config::getParameter('TYPE_DOCUMENT_SGMAP_ENTREPRISE')    : document entreprise
     *                              => $sourceTypeDoc = Atexo_Config::getParameter('TYPE_DOCUMENT_SGMAP_ETABLISSEMENT') : document etablissement
     *
     * @return string $url url de telechargement
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.10.0
     *
     * @copyright Atexo 2015
     */
    public function getDlAttestationUrl($typeDoc, $identifiant, $sourceTypeDoc = '1')
    {
        $lastversion = $this->getLastVersionByTypeDoc($identifiant, $typeDoc, $sourceTypeDoc);
        if ($lastversion instanceof CommonTEntrepriseDocumentVersion) {
            $document = $lastversion->getCommonTDocumentEntreprise();
            if ($document instanceof CommonTDocumentEntreprise) {
                $fileName = $document->getNomDocument().'_'.date('Y_m_d_H_i_s').'.'.$lastversion->getExtensionDocument();

                return '?page=Agent.FileDownLoad&idFichier='.$lastversion->getIdBlob().'&org='.Atexo_Config::getParameter('COMMON_BLOB').'&nomFichier='.$fileName;
            }
        }
    }

    /**
     * retourne le titre du picto de l'attestation.
     *
     * @param CommonTDocumentType $objetTypeDocument : objet type de document
     *
     * @return string $alt alt du picto
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.10.0
     *
     * @copyright Atexo 2015
     */
    public function getAltAttestation($objetTypeDocument)
    {
        $titre = $this->getTitreAttestation($objetTypeDocument);
        $lastversion = $this->getLastVersionByTypeDoc($this->idEntreprise, ($objetTypeDocument instanceof CommonTDocumentType) ? $objetTypeDocument->getIdTypeDocument() : null);
        if ($lastversion instanceof CommonTEntrepriseDocumentVersion) {
            $infoLastversion = $lastversion->getDateRecuperation('d/m/Y').' - '.$lastversion->getTailleDocument().' - '.$lastversion->getExtensionDocument();
        } else {
            $infoLastversion = Prado::localize('TEXT_NON_DISPONIBLE');
        }

        return $titre.' - '.$infoLastversion;
    }

    /**
     * retourne le taille du picto de l'attestation.
     *
     * @param int    $typeDoc       le type de document
     * @param int    $identifiant   l'id de l entreprise/etablissement
     * @param string $sourceTypeDoc : precise si le document provient de l'entreprise ou etablissement
     *                              => $sourceTypeDoc = Atexo_Config::getParameter('TYPE_DOCUMENT_SGMAP_ENTREPRISE')    : document entreprise
     *                              => $sourceTypeDoc = Atexo_Config::getParameter('TYPE_DOCUMENT_SGMAP_ETABLISSEMENT') : document etablissement
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.10.0
     *
     * @copyright Atexo 2015
     */
    public function getTailleAttestation($typeDoc, $identifiant, $sourceTypeDoc = '1')
    {
        $lastversion = $this->getLastVersionByTypeDoc($identifiant, $typeDoc, $sourceTypeDoc);
        if ($lastversion instanceof CommonTEntrepriseDocumentVersion) {
            return $lastversion->getTailleDocument() ? '('.$lastversion->getTailleDocument().')' : '';
        }
    }

    /**
     * retourne le extention du picto de l'attestation.
     *
     * @param int    $typeDoc       le type de document
     * @param int    $identifiant   l'id de l entreprise/etablissement
     * @param string $sourceTypeDoc : precise si le document provient de l'entreprise ou etablissement
     *                              => $sourceTypeDoc = Atexo_Config::getParameter('TYPE_DOCUMENT_SGMAP_ENTREPRISE')    : document entreprise
     *                              => $sourceTypeDoc = Atexo_Config::getParameter('TYPE_DOCUMENT_SGMAP_ETABLISSEMENT') : document etablissement
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.10.0
     *
     * @copyright Atexo 2015
     */
    public function getExtentionAttestation($typeDoc, $identifiant, $sourceTypeDoc = '1')
    {
        $lastversion = $this->getLastVersionByTypeDoc($identifiant, $typeDoc, $sourceTypeDoc);
        if ($lastversion instanceof CommonTEntrepriseDocumentVersion) {
            return $lastversion->getExtensionDocument();
        }
    }

    /**
     * retourne le titre  de l'attestation.
     *
     * @param CommonTDocumentType $objetTypeDocument : objet type de document
     *
     * @return string $alt alt du picto
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.10.0
     *
     * @copyright Atexo 2015
     */
    public function getTitreAttestation($objetTypeDocument)
    {
        return ($objetTypeDocument instanceof CommonTDocumentType) ? $objetTypeDocument->getNomTypeDocument() : '-';
    }

    /**
     * retourne le picto selon le type de l'attestation.
     *
     * @param int $typeDoc le type de document
     *
     * @author AMAL EL BEKKAOUI <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.10.0
     *
     * @copyright Atexo 2015
     */
    public function getPictoByType($typeDoc)
    {
        switch ($typeDoc) {
            case Atexo_Config::getParameter('TYPE_ATTESTATION_FISCALE'):
                return 'picto-attestation-fiscale.png';
                                                                             break;
            case Atexo_Config::getParameter('TYPE_ATTESTATION_SOCIALE_MARCHE_PUBLIC'):
                return 'picto-attestation-marche-public.png';
                                                                             break;
            case Atexo_Config::getParameter('TYPE_ATTESTATION_SOCIALE_VIGILANCE'):
                return 'picto-attestation-vigilence.png';
                                                                             break;
        }
    }

    /**
     * retourne la classe css du picto selon la disponibilité de l'attestation.
     *
     * @param int $typeDoc le type de document
     *
     * @return string la classe css
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.10.0
     *
     * @copyright Atexo 2015
     */
    public function getClassPicto($typeDoc)
    {
        $class = 'non-disponible';
        $lastversion = $this->getLastVersionByTypeDoc($this->idEntreprise, $typeDoc);
        if ($lastversion instanceof CommonTEntrepriseDocumentVersion) {
            $class = '';
        }

        return $class;
    }

    /**
     * @return bool
     */
    public function isResponsive()
    {
        return $this->responsive;
    }

    /**
     * @param bool $responsive
     */
    public function setResponsive($responsive)
    {
        $this->responsive = $responsive;
    }

    /**
     * @return array
     */
    public function getLastVersion()
    {
        return $this->lastVersion;
    }

    /**
     * @param array $lastVersion
     */
    public function setLastVersion($lastVersion)
    {
        $this->lastVersion = $lastVersion;
    }

    /**
     * @param int    $identifiant   l'id de l entreprise/etablissement
     * @param string $sourceTypeDoc : precise si le document provient de l'entreprise ou etablissement
     *                              => $sourceTypeDoc = Atexo_Config::getParameter('TYPE_DOCUMENT_SGMAP_ENTREPRISE')    : document entreprise
     *                              => $sourceTypeDoc = Atexo_Config::getParameter('TYPE_DOCUMENT_SGMAP_ETABLISSEMENT') : document etablissement
     *
     * @return array
     */
    public function hasVersion($typeDoc, $identifiant, $sourceTypeDoc = '1')
    {
        $lastversion = $this->getLastVersionByTypeDoc($identifiant, $typeDoc, $sourceTypeDoc);
        if ($lastversion instanceof CommonTEntrepriseDocumentVersion) {
            return true;
        }

        return false;
    }

    /**
     * retourne le titre du picto de l'attestation.
     *
     * @param int    $typeDoc       le type de document
     * @param int    $identifiant   l'id de l entreprise/etablissement
     * @param string $sourceTypeDoc : precise si le document provient de l'entreprise ou etablissement
     *                              => $sourceTypeDoc = Atexo_Config::getParameter('TYPE_DOCUMENT_SGMAP_ENTREPRISE')    : document entreprise
     *                              => $sourceTypeDoc = Atexo_Config::getParameter('TYPE_DOCUMENT_SGMAP_ETABLISSEMENT') : document etablissement
     *
     * @return string $alt alt du picto
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.10.0
     *
     * @copyright Atexo 2015
     */
    public function getDateMAJAttestation($typeDoc, $identifiant, $sourceTypeDoc = '1')
    {
        $lastversion = $this->getLastVersionByTypeDoc($identifiant, $typeDoc, $sourceTypeDoc);
        if ($lastversion instanceof CommonTEntrepriseDocumentVersion) {
            return $lastversion->getDateRecuperation('d/m/Y');
        }

        return ' - ';
    }

    /**
     *Permet d'afficher l'historique d'une attestation.
     *
     * @param $sender
     * @param $param
     *
     * @return void
     *
     * @author Loubna Ezziani <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function afficherHistoriqueAttestation($sender, $param)
    {
        $idTypeDoc = null;
        $idObjet = null;
        $documentEntreprise = null;
        $identifiant = null;
        $listeParameters = explode('-', $param->CommandParameter);
        $actif = false;
        if (is_array($listeParameters) && !empty($listeParameters)) {
            $idTypeDoc = $listeParameters[0];
            $identifiant = $listeParameters[1];
            if (!empty($listeParameters[2])) {
                $idObjet = $listeParameters[2];
            }
        }
        if ($idTypeDoc) {
            $this->javascriptLabel->Text = '';
            $idEntreprise = $this->getViewState('idEntreprise');
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            $typeDocumentObjet = CommonTDocumentTypeQuery::create()->filterByIdTypeDocument($idTypeDoc)->findOne($connexion);
            if ($typeDocumentObjet instanceof CommonTDocumentType) {
                if ($typeDocumentObjet->getTypeDocEntrepriseEtablissement() == Atexo_Config::getParameter('TYPE_DOCUMENT_SGMAP_ENTREPRISE')) {
                    $documentEntreprise = (new Atexo_Entreprise_Attestation())->retrieveDocumentEntreprise($idEntreprise, $idTypeDoc);
                } elseif ($typeDocumentObjet->getTypeDocEntrepriseEtablissement() == Atexo_Config::getParameter('TYPE_DOCUMENT_SGMAP_ETABLISSEMENT')) {
                    $documentEntreprise = (new Atexo_Entreprise_Attestation())->retrieveDocumentEtablissement($idObjet, $idTypeDoc);
                }
                $actif = $typeDocumentObjet->getSynchroActif();
            }
            $dataSource = [];
            if ($documentEntreprise instanceof CommonTDocumentEntreprise) {
                $idDocument = $documentEntreprise->getIdDocument();
                $dataSource = (array) (new Atexo_Entreprise_Attestation())->retrieveDocumentVersion($idDocument);
            }
            $this->atexoHistoriquesDocument->setIdTypeDocument($idTypeDoc);
            $this->atexoHistoriquesDocument->setIdEntreprise($idEntreprise);
            $this->atexoHistoriquesDocument->setIdentifiant($identifiant);
            $this->atexoHistoriquesDocument->setOrganisme($this->getOrganisme());
            $this->atexoHistoriquesDocument->setRefConsultation($this->getRefCons());
            $this->atexoHistoriquesDocument->setSynchroActif($actif);
            $this->atexoHistoriquesDocument->initComposant($dataSource);
            $this->javascriptLabel->Text = "<script>openModal('modal-historique','modal-form popup-small popup-800',document.getElementById('".$this->titrePopin->getClientId()."'));</script>";
        }
    }

    /**
     * Permet de recuperer le nombre de dernieres versions de documents secondaires par entreprise.
     *
     * @param string $idEntreprise : identifiant de l'entreprise
     *
     * @return string
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function getNbrDernieresVersionsDocSecondairesParEntreprise($idEntreprise)
    {
        $cle = 'nombre_documents_secondaires_entreprise_id_'.$idEntreprise;

        if (!empty($this->getViewState($cle))) {
            return $this->getViewState($cle);
        }

        $nbrDocumentsSecondaires = (new Atexo_Entreprise_Attestation())->recupererNombreDernieresVersionsDocuments($idEntreprise, Atexo_Config::getParameter('SGMAP_NATURE_DOCUMENT_SUPPLEMENTAIRE'));
        $this->setViewState($cle, $nbrDocumentsSecondaires);

        return $this->getViewState($cle);
    }

    /**
     * Permet de construire le titre des liens des attsetations supplementaires disponibles pour l'entreprise.
     *
     * @param string $idEntreprise
     *
     * @return string
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function getTitreLiensAttestationsSupplementairesEntreprise($idEntreprise)
    {
        $nbrDocSupplementaires = $this->getNbrDernieresVersionsDocSecondairesParEntreprise($idEntreprise);

        return ((int) $nbrDocSupplementaires > 0) ? Prado::localize('DEFINE_VOIR_LES_ATTESTATIONS') : Prado::localize('DEFINE_AUCUNE_ATTESTATION_SUPPLEMENTAIRE_DISPONIBLE');
    }

    /**
     * Permet de construire le lien de la fiche fournisseur.
     *
     * @return string
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function getUrlFicheFournisseur()
    {
        $idEntreprise = $this->getIdEntreprise();
        $idOffre = $this->getIdOffre();
        $typeEnv = $this->getTypeEnv();
        $ats = sha1($idEntreprise.'1');
        $consultationId = $this->getRefCons();
        $org = $this->getOrganisme();
        $callFromOA = $this->getCallFromOA();

        return "javascript:popUpSetSize('index.php?page=Agent.DetailEntreprise&amp;callFrom=agent&amp;idEntreprise=$idEntreprise&amp;idO=$idOffre&amp;type=$typeEnv&amp;ats=$ats&amp;refCons=$consultationId&amp;organisme=$org&amp;callFromOA=$callFromOA#bloc_documents_entreprise','1060px','800px','yes');";
    }

    /**
     * Permet de remplir le tableau des types de documents entreprise SGMAP.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function remplirListeTypesDocumentsEntreprise()
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $docTypesQuery = new CommonTDocumentTypeQuery();
        $this->listeTypesDocsEntreprise->DataSource = $docTypesQuery->getTypesDocumentsByTypesEntrepriseEtablissement(Atexo_Config::getParameter('TYPE_DOCUMENT_SGMAP_ENTREPRISE'), $connexion, 1);
        $this->listeTypesDocsEntreprise->DataBind();
    }

    /**
     * Permet de remplir le tableau des types de documents etablissement SGMAP.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function remplirListeTypesDocumentsEtablissement()
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $docTypesQuery = new CommonTDocumentTypeQuery();

        return $docTypesQuery->getTypesDocumentsByTypesEntrepriseEtablissement(Atexo_Config::getParameter('TYPE_DOCUMENT_SGMAP_ETABLISSEMENT'), $connexion, 1);
    }

    /**
     * Permet de remplir la liste des etablissements d'une entreprise.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function remplirListeEtablissements()
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $listeEtablissement = (array) CommonTEtablissementQuery::create()->filterByIdEntreprise($this->getIdEntreprise())
                ->orderByCodeEtablissement(Criteria::ASC)
                ->orderByDateModification(Criteria::DESC)
                ->groupByCodeEtablissement()
                ->find($connexion);
        $this->listeEtablissements->DataSource = $listeEtablissement;
        $this->listeEtablissements->DataBind();

        return $listeEtablissement;
    }

    /**
     * Permet de recuperer le siren de l'entreprise courante.
     *
     * @return string|null
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function getSirenEntreprise()
    {
        $entreprise = (new Atexo_Entreprise())->retrieveCompanyBdeById($this->getIdEntreprise(), null, false);

        return ($entreprise instanceof Entreprise) ? $entreprise->getSiren() : null;
    }

    /**
     * Permet de determiner les cas pour l'initialisation du treegrid des attestations.
     *
     * @return string cas : cas2 lorsque le composant est appele depuis Ouverture et analyse, cas1 sinon
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function recupererCasInitialisationAttestationTreeGrid()
    {
        $cas = 'cas1';
        if ('true' == $this->getCallFromOA()) {
            $cas = 'cas2';
        }

        return $cas;
    }

    /**
     * Permet de recuperer l'id de l'etablissement ayant depose la reponse.
     *
     * @return int
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function recupererIdEtablissementDepotReponse()
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        if ('E' == $this->getTypeEnv()) {
            $offre = CommonOffresQuery::create()->filterById($this->getIdOffre())->filterByOrganisme($this->getOrganisme())->findOne($connexion);
            if ($offre instanceof CommonOffres) {
                $idEtablissement = $offre->getIdEtablissement();
            }
        } else {
            $offrePapier = CommonOffrePapierQuery::create()->filterById($this->getIdOffre())->filterByOrganisme($this->getOrganisme())->findOne($connexion);
            if ($offrePapier instanceof CommonOffrePapier) {
                $idEtablissement = $offrePapier->getIdEtablissement();
                if (empty($idEtablissement)) {
                    $etablissement = (new Atexo_Entreprise_Etablissement())->getEtablissementBySiret($offrePapier->getSiret());
                    $idEtablissement = ($etablissement instanceof CommonTEtablissement) ? $etablissement->getIdEtablissement() : null;
                }
            }
        }

        return $idEtablissement;
    }

    /**
     * permet de telecharger la dernier version d'un document.
     *
     * @param mixed $sender
     * @param mixed $param
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2017
     */
    public function downloadVersion($sender, $param)
    {
        $paramsDoc = explode('#', $param->CommandParameter);
        $this->javascriptLabel->Text = "";
        if (is_array($paramsDoc)) {
            $identifiant = $paramsDoc[0];
            $typeDoc = $paramsDoc[1];
            $sourceTypeDoc = $paramsDoc[2];
            $lastversion = $this->getLastVersionByTypeDoc($identifiant, $typeDoc, $sourceTypeDoc);
            if ($lastversion instanceof CommonTEntrepriseDocumentVersion) {
                $document = $lastversion->getCommonTDocumentEntreprise();
                if ($document instanceof CommonTDocumentEntreprise) {
                    if (!$lastversion->getIdBlob()) {
                        (new Atexo_Entreprise_Attestation())->genererAttestationFromJson($lastversion, $document);
                    }
                    if ($lastversion->getIdBlob()) {
                        $fileName = $document->getNomDocument().'_'.date('Y_m_d_H_i_s').'.'.$lastversion->getExtensionDocument();
                        $this->setViewState('infoFichier', 'idFichier='.$lastversion->getIdBlob().'&org='.Atexo_Config::getParameter('COMMON_BLOB').'&nomFichier='.urlencode($fileName));
                        $this->response->redirect("?page=Agent.FileDownLoad&idFichier=".$lastversion->getIdBlob()."&org=".Atexo_Config::getParameter('COMMON_BLOB')."&nomFichier=".$fileName);
                    }
                }
            }
        }
    }

    public function downloadFichier()
    {
        $infoFichier = $this->getViewState('infoFichier');
        if ($infoFichier) {
            $this->response->redirect('?page=Agent.FileDownLoad&'.$infoFichier);
        }
    }

    /**
     * Récupère le récapitulatif d’état des inscriptions
     * au Registre national du commerce et des sociétés concernant une entreprise
     *
     * @return string
     */
    public function getNationalRegisterTradeCompanyPdf(): string
    {
        return $this->getFileInfosCompany(self::INPI_URL);
    }

    /**
     * Récupère l'avis de situation d'une entreprise
     * via le Répertoire national des métiers
     *
     * @return string
     */
    public function getAvisSituationPdf(): string
    {
        return $this->getFileInfosCompany(self::API_RNM_URL, self::FORMAT_FILE_PDF);
    }

    /**
     * Retourne l'url d'un fichier qui contient des infos sur l'entreprise
     *
     * @param string $url
     * @param string $format
     * @return string
     */
    private function getFileInfosCompany(string $url, string $format = ''): string
    {
        return
            ($this->getSirenEntreprise())
            ? str_replace('__SIRET__', $this->getSirenEntreprise(), $url) . $format
            : '#'
        ;
    }
}
