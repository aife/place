<?php

namespace Application\Controls;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonCategorieLot;
use Application\Propel\Mpe\CommonConsultation;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Qualifications;
use Application\Service\Atexo\Atexo_Util;

/**
 * Composant pour les qualifications.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class AtexoQualification extends MpeTPage
{
    private string $_postBack = '';
    private string $_callback = '';
    private string $_itemIndex = '';
    private $_donneesComplementaires;
    private bool|string $_afficher = true;
    private $_objet;
    private $_connexion;

    public function setIdsQualification($value)
    {
        $this->idsQualification->Text = $value;
    }

    public function setPostBack($value)
    {
        $this->_postBack = $value;
    }

    public function getPostBack()
    {
        return $this->_postBack;
    }

    public function setCallBack($value)
    {
        $this->_callback = $value;
    }

    public function getCallBack()
    {
        return $this->_callback;
    }

    public function setItemIndex($value)
    {
        $this->_itemIndex = $value;
    }

    public function getItemIndex()
    {
        return $this->_itemIndex;
    }

    /**
     * recupère la consultation.
     */
    public function getObjet()
    {
        return $this->_objet;
    }

    // getObjet()

    /**
     * Affecte consultation.
     *
     * @param objet
     */
    public function setObjet($objet)
    {
        if ($this->_objet !== $objet) {
            $this->_objet = $objet;
        }
    }

    // setObjet)

    /**
     * recupère l'objet données complémentaires.
     */
    public function getDonneesComplementaires()
    {
        return $this->_donneesComplementaires;
    }

    // getDonneesComplementaires()

    /**
     * Affecte l'objet données complémentaires.
     *
     * @param CommonTDonneesComplementaires $donneesComplementaires
     */
    public function setDonneesComplementaires($donneesComplementaires)
    {
        if ($this->_donneesComplementaires !== $donneesComplementaires) {
            $this->_donneesComplementaires = $donneesComplementaires;
        }
    }

    // setDonneesComplementaires()

    /**
     * recupère la valeur de afficher.
     */
    public function getAfficher()
    {
        return $this->_afficher;
    }

    // getAfficher()

    /**
     * Affecte la valeur de afficher.
     *
     * @param string $afficher
     */
    public function setAfficher($afficher)
    {
        if ($this->_afficher !== $afficher) {
            $this->_afficher = $afficher;
        }
    }

    // setAfficher()

    /**
     * recupère la valeur de connexion.
     */
    public function getConnexion()
    {
        return $this->_connexion;
    }

    // getConnexion()

    /**
     * Affecte la connexion.
     *
     * @param string connexion
     */
    public function setConnexion($connexion)
    {
        $this->_connexion = $connexion;
    }

    // setConnexion()

    public function onLoad($param)
    {
        $this->chargerComposant();
        if (!$this->getCallBack()) {
            $this->displayQualification();
        }
    }

    /**
     * affiche les libellés des domaines retournés de la popup.
     */
    public function displayQualification()
    {
        if (Atexo_Module::isEnabled('ConsultationQualification')) {
            $libelles = '';
            if ('' == $this->libelleQualif->Text) {
                $libelles = (new Atexo_Qualifications())->displayListeLibelleDomaineByArrayId(base64_decode($this->idsQualification->Text));
            }
            if ('' == $libelles) {
                $libelles = substr(base64_decode(trim($this->libelleQualif->Text)), 0, -1);
                $this->libellesQualif->Text = '';
                $this->trunLibelleQualif->Text = '';
            }
            if (strlen($libelles) > 100) {
                $this->toisPointsInfoBulle->setDisplay('Dynamic');
            }
            if ($libelles) {
                $this->trunLibelleQualif->Text = Atexo_Util::truncateTexte(wordwrap(($libelles), 400, '<br>', true), 100);
            }
            $this->libellesQualif->Text = wordwrap(($libelles), 400, '<br>', true);
            //  $this->panelQualification->render($param->NewWriter);
        }
    }

    /**
     * rafrechir le panel des qualifications.
     */
    public function onCallBackQualification($sender, $param)
    {
        $this->panelQualification->render($param->NewWriter);
    }

    /**
     * Permet de gerer la visibilité.
     */
    public function customize()
    {
        if (Atexo_Module::isEnabled('ConsultationQualification')) {
            $this->panelQualification->visible = true;
            $this->setCallBack($this->IsCallBack);
            $this->setPostBack($this->IsPostBack);
        } else {
            $this->panelQualification->visible = false;
        }
    }

    /**
     * Permet de sauvegarder les donnees des qualifications.
     *
     * @param CommonConsultation ou CommonCategorieLot $objet ,Boolean $saveBb enregistrer l'objet, Boolean $modification
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function save(&$objet, $saveBb = false, $modification = true)
    {
        if (Atexo_Module::isEnabled('ConsultationQualification')) {
            $qualification = '';
            $qualificationOld = $objet->getQualification();
            if ($this->getAfficher()) {
                $qualification = base64_decode($this->idsQualification->Text);
            }
            // historiques
            if (!$this->_connexion) {
                $this->_connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            }
            $objet->setQualification($qualification);
            $this->saveHistoriqueQualification($qualificationOld, $objet, $this->_connexion, Atexo_CurrentUser::getCurrentOrganism(), $modification);
            if ($saveBb) {
                $objet->save($this->_connexion);
            }
        }
    }

    /**
     * Permet de sauvegarder l'historique des qualifications.
     *
     * @param string $qualificationOld l'ancienne qualification,CommonConsultation ou CommonCategorieLot $objet ,Objet connexion $connexion,String $org l'acronyme de l'organisme, Boolean $modification
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function saveHistoriqueQualification($qualificationOld, $objet, $connexion, $org, $modification = true)
    {
        $arrayParameters = [];
        if (!$connexion) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        }
        if (Atexo_Module::isEnabled('ConsultationQualification') &&
            (trim($qualificationOld) != trim($objet->getQualification()) || !$modification)
            ) {
            $nomElement = Atexo_Config::getParameter('ID_HISTORIQUE_QUALIFICATION');
            $valeur = '0';
            $detail2 = '';
            $detail1 = '';
            $consultationId = '';
            $lot = '0';
            if ($objet instanceof CommonCategorieLot) {
                $lot = $objet->getLot();
            }
            if ($objet->getQualification()) {
                $detail1 = $objet->getQualification();
                $valeur = '1';
            }
            if ($objet instanceof CommonConsultation) {
                $consultationId = $objet->getConsultationId();
                $consultation = $objet;
            } elseif ($objet instanceof CommonCategorieLot) {
                $consultation = (new Atexo_Consultation())->retrieveConsultation($objet->getConsultationId(), $org);
                $consultationId = $objet->getConsultationId();
            }

            if ($modification) {
                $statut = Atexo_Config::getParameter('MODIFICATION_FILE');
            } else {
                $statut = Atexo_Config::getParameter('CREATION_FILE');
            }
            //Sauvegarde
            $arrayParameters[0] = $consultationId;
            $arrayParameters[1] = $nomElement;
            $arrayParameters[2] = $valeur;
            $arrayParameters[3] = $detail1;
            $arrayParameters[4] = $detail2;
            $arrayParameters[5] = $lot;
            $arrayParameters[6] = $org;
            $arrayParameters[7] = $statut;
            (new Atexo_Consultation())->saveHistoriqueConsultation($arrayParameters, $connexion);
        }
    }

    /**
     * Permet de préciser si la modification est permise.
     *
     * @param $consultation: objet consultation
     * Si Oui retourne true, sinon false
     */
    public function isModificationAllowed($consultation)
    {
        return (new Atexo_Consultation())->isModificationAllowed($consultation, isset($_GET['continuation']), $consultation->getId());
    }

    /**
     * Permet d'afficher les échantillons demandés.
     */
    public function afficherComposant()
    {
        $this->panelQualification->setDisplay('Dynamic');
        $this->setCallBack($this->IsCallBack);
        $this->setPostBack($this->IsPostBack);
    }

    /**
     * Permet de masquer les échantillons demandés.
     */
    public function masquerComposant()
    {
        $this->panelQualification->setDisplay('None');
    }

    public function chargerComposant($recharger = false)
    {
        $this->customize();
        if (!$this->Page->isPostBack) {
            if ($this->getObjet()) {
                if ($this->getObjet()->getQualification()) {
                    $this->idsQualification->Text = base64_encode($this->getObjet()->getQualification());
                }
            }
        }
        if (Atexo_Module::isEnabled('ConsultationQualification') && $this->getAfficher()) {
            $this->afficherComposant();
        } else {
            $this->masquerComposant();
        }
    }
}
