<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_Config;
use Prado\Prado;
use Prado\Web\UI\WebControls\TLabel;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PictoStatutSignature extends TLabel
{
    private $_statut;
    private $_size;
    private bool $_nouveauAffichage = false;

    public function getStatut()
    {
        return $this->_statut;
    }

    public function setStatut($statut)
    {
        $this->_statut = $statut;
        self::displayImages();
    }

    public function getSize()
    {
        return $this->_size;
    }

    public function setSize($size)
    {
        $this->_size = $size;
    }

    public function getNouveauAffichage()
    {
        return $this->_nouveauAffichage;
    }

    public function setNouveauAffichage($value)
    {
        $this->_nouveauAffichage = $value;
    }

    public function onLoad($param)
    {
    }

    public function displayImages()
    {
        switch (strtoupper($this->_statut)) {
            case Atexo_Config::getParameter('OK'):
                if ('BIG' == strtoupper($this->_size)) {
                    $this->setCssClass('fa fa-check-circle text-success fa-2x m-l-1 statut-valide');
                } else {
                    $this->setCssClass('fa fa-check-circle text-success m-l-1 fa-lg statut-valide ');
                }
                                                         $this->setToolTip(Prado::localize('TEXT_VERIFICATION_OK'));
                                                         //$this->setText=Prado::localize('TEXT_VERIFICATION_OK');
                break;
            case Atexo_Config::getParameter('NOK'):
                if ('BIG' == strtoupper($this->_size)) {
                    $this->setCssClass('fa fa-times-circle text-danger fa-2x m-l-1 statut-erreur');
                } else {
                    $this->setCssClass('fa fa-times-circle text-danger m-l-1 fa-lg statut-erreur');
                }
                                                         $this->setToolTip(Prado::localize('TEXT_VERIFICATION_NOK'));
                                                         //$this->setText=Prado::localize('TEXT_VERIFICATION_NOK');
                break;
            case Atexo_Config::getParameter('UNKNOWN'):
                if ('BIG' == strtoupper($this->_size)) {
                    $this->setCssClass('fa fa-exclamation-circle text-warning fa-2x m-l-1 statut-avertissement');
                } else {
                    $this->setCssClass('fa fa-exclamation-circle text-warning m-l-1 fa-lg statut-avertissement');
                }
                                                         $this->setToolTip(Prado::localize('TEXT_VERIFICATION_IMCOMPLETE'));
                                                         //$this->setText=Prado::localize('TEXT_VERIFICATION_IMCOMPLETE');
                break;
            case Atexo_Config::getParameter('SANS'):
                 $this->setVisible(false);
                break;
            case Atexo_Config::getParameter('UNVERIFIED'):
                if ('BIG' == strtoupper($this->_size)) {
                    $this->setCssClass('fa fa-ban');
                } else {
                    $this->setCssClass('fa fa-ban');
                }
                                                            $this->setToolTip(Prado::localize('VALIDITE_NON_VERIFIABLE'));
                                                            //$this->setText=Prado::localize('VALIDITE_NON_VERIFIABLE');
                break;
            default:
                 $this->setToolTip(Prado::localize('TEXT_INDEFINI'));
                        //$this->setText=Prado::localize('TEXT_INDEFINI');
        }
    }
}
