<?php

namespace Application\Controls;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonTTelechargementAsynchrone;
use Application\Propel\Mpe\CommonTTelechargementAsynchronePeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Archive;
use Exception;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * permet de lister les differents telechargement des archives effectues.
 *
 * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
 *
 * @version 1.0
 *
 * @since 4.8.0
 *
 * @copyright Atexo 2014
 * @
 */
class AtexoTelechargement extends MpeTTemplateControl
{
    private ?int $idAgent = null;

    /**
     * modifie la valeur de [IdAgent].
     *
     * @param int $idAgent la valeur a mettr
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setIdAgent($idAgent)
    {
        $this->idAgent = $idAgent;
    }

    /**
     * recupere la valeur du [IdAgent].
     *
     * @return int la valeur courante [IdAgent]
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getIdAgent()
    {
        return $this->idAgent;
    }

    /**
     * permet d'initialiser le composant.
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function initComposant()
    {
        $msgDefault = Prado::localize('DEFINE_MESSAGE_DISPONIBILITE_ARCHIVE_SUR_PF');
        $nbrJrsDispo = Atexo_Config::getParameter('NBR_JRS_DISPONIBILITE_ARCHIVE_ASYNCHRONE_PF');
        $msgInfo = str_replace('[NB_JOUR_DISPO]', $nbrJrsDispo, $msgDefault);
        $this->panelInfo->setMessage($msgInfo);
        $this->setViewState('sortByElement', 'dateArchive');
        self::populateData();
    }

    /**
     * permet de lister les differents telechargement des archives effectues.
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function populateData()
    {
        $arrayTelechargement = self::retrieveAllTelechargement();
        self::chargerDonnees($arrayTelechargement);
    }

    /**
     * permet de rafraichir la liste telechargement.
     *
     * @param $sender
     * @param $param
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function refreshListe($sender, $param)
    {
        $arrayTelechargement = self::retrieveAllTelechargement();
        self::chargerDonnees($arrayTelechargement, $param);
    }

    /**
     * permet de recuperer la liste des telechargement.
     *
     * @param string $sortWith le nom de la colonne avec laquel on fait le tri
     *
     * @return array $arrayTelechargement tableau des objets CommonTTelechargementAsynchrone
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function retrieveAllTelechargement($sortWith = 'dateArchive')
    {
        $c = new Criteria();
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $dateDispoZipSurPF = Atexo_Util::dateDansPasse(date('Y-m-d'), 0, Atexo_Config::getParameter('NBR_JRS_DISPONIBILITE_ARCHIVE_ASYNCHRONE_PF'));
        $organisme = Atexo_CurrentUser::getCurrentOrganism();
        $service = Atexo_CurrentUser::getIdServiceAgentConnected();
        if ($this->idAgent) {
            $c->add(CommonTTelechargementAsynchronePeer::ID_AGENT, $this->idAgent);
        } else {
            if (0 != $service) {
                $sousService = Atexo_EntityPurchase::getSubServices($service, $organisme, true);
                $c->add(CommonTTelechargementAsynchronePeer::ID_SERVICE_AGENT, $sousService, Criteria::IN);
            }
        }
        $c->add(CommonTTelechargementAsynchronePeer::ORGANISME_AGENT, $organisme);
        $c->add(CommonTTelechargementAsynchronePeer::DATE_GENERATION, $dateDispoZipSurPF, Criteria::GREATER_EQUAL);
        $c->add(CommonTTelechargementAsynchronePeer::TAG_FICHIER_GENERE, 1);
        $c->add(CommonTTelechargementAsynchronePeer::TAG_FICHIER_SUPPRIME, 0);
        $c->add(CommonTTelechargementAsynchronePeer::TYPE_TELECHARGEMENT, Atexo_Config::getParameter('TYPE_TELECHARGEMENT_ASYNCHRONE_ARCHIVE'));
        switch ($sortWith) {
            case 'dateArchive':
                $c->addDescendingOrderByColumn(CommonTTelechargementAsynchronePeer::DATE_GENERATION);
                break;
            case 'nomArchive':
                $c->addAscendingOrderByColumn(CommonTTelechargementAsynchronePeer::NOM_FICHIER_TELECHARGEMENT);
                break;
            case 'nomAgent':
                $c->addAscendingOrderByColumn(CommonTTelechargementAsynchronePeer::ID_AGENT);
                break;
            case 'tailleArchive':
                $c->addAscendingOrderByColumn(CommonTTelechargementAsynchronePeer::TAILLE_FICHIER);
                break;
        }
        $arrayTelechargement = CommonTTelechargementAsynchronePeer::doSelect($c, $connexion);
        if ($arrayTelechargement) {
            return $arrayTelechargement;
        }

        return [];
    }

    /**
     * permet de recuperer l'url de telechargement de l'archive.
     *
     * @param CommonTTelechargementAsynchrone $telachargementObj l'objet CommonTTelechargementAsynchrone
     *
     * @return string url de telechargement de l'archive
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function getUrlDownloadarchive($telachargementObj)
    {
        $FileArchiveDir = Atexo_Config::getParameter('FICHIERS_ARCHIVE_ASYNCHRONE_DIR').$telachargementObj->getOrganismeAgent();
        $nameDownloadedFile = $telachargementObj->getNomFichierTelechargement();
        $nameFileDisq = $telachargementObj->getId();
        $pathFile = $FileArchiveDir.'/'.$nameFileDisq;

        return 'index.php?page=Agent.DownloadFileArchive&file='.base64_encode($pathFile).'&name='.base64_encode($nameDownloadedFile);
    }

    /**
     * permet de mettre en gras l'entete de la colonne utilisée pour le tri.
     *
     * @param string $panelName    le nom de l'entete
     * @param string $openCloseTag la valeur pour gerer l'affichage
     *
     * @return string url de telechargement de l'archive
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function elementSortedWith($panelName, $openCloseTag)
    {
        if ($this->getViewState('sortByElement') == $panelName) {
            if ('open' == $openCloseTag) {
                return '<strong>';
            } else {
                return '</strong>';
            }
        } else {
            return '';
        }
    }

    /**
     * permet de faire un tri sur le tableau des telechargement.
     *
     * @param $sender
     * @param $param
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function Trier($sender, $param)
    {
        $champsOrderBy = $param->CommandName;
        $this->setViewState('sortByElement', $champsOrderBy);
        $arrayTelechargement = self::retrieveAllTelechargement($champsOrderBy);
        self::chargerDonnees($arrayTelechargement);
    }

    /**
     * permet de remplir le tableau des telechargement.
     *
     * @param array $arrayTelechargement tableau des CommonTTelechargementAsynchrone a afficher
     * @param $param
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function chargerDonnees($arrayTelechargement, $param = false)
    {
        if (count($arrayTelechargement)) {
            $this->listeTelechargement->setVisible(true);
            $this->RepeaterArchiveTelechargement->DataSource = $arrayTelechargement;
            $this->RepeaterArchiveTelechargement->DataBind();
        } else {
            $this->listeTelechargement->setVisible(false);
            if ($param) {
                $this->Page->panelTelechargement->render($param->getNewWriter());
            }
            $this->panelAvertissement->setVisible(true);
            $this->panelAvertissement->setMessage(Prado::localize('DEFINE_LISTE_TELECHARGEMENTS_VIDE'));
        }
    }

    /**
     * permet de supprimer un telechargement (CommonTTelechargementAsynchrone).
     *
     * @param $sender
     * @param $param
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function supprimerTelechargement($sender, $param)
    {
        $connexionCom = null;
        $idArchive = $param->CommandParameter;
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $telechargementSupprime = CommonTTelechargementAsynchronePeer::retrieveByPK($idArchive, $connexionCom);
        try {
            (new Atexo_Consultation_Archive())->deleteArchive($telechargementSupprime, $connexion);
        } catch (Exception $e) {
            Prado::log(' Erreur lors de la suppresion d un telechargement (AtexoTelechargement.php) '.$e->getMessage().$e->getTraceAsString(), TLogger::ERROR, 'AtexoTelechargement.php');
        }
    }
}
