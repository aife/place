<com:TActivePanel id="panelSiren" Cssclass="line no-indent" >
	<div class="intitule-auto">
		<com:TActiveRadioButton 
              GroupName="entreprise" 
              id="attributaire_Nationale" 
              CssClass="radio"  
              Attributes.onclick="showHideBlocEntreprise(this,'<%=$this->attributaire_etablieFrance->ClientId%>',
              '<%=$this->attributaire_nonEtablieFrance->ClientId%>','<%=$this->ClientId%>','0');"
        />
		<label for="attributaire_Nationale"><com:TTranslate>TEXT_ENTREPRISE_ETABLIE_FRANCE</com:TTranslate></label>
	</div>
	<com:TActivePanel Cssclass="clear-both"  id="attributaire_etablieFrance">
		
		<com:TActivePanel Visible="<%=Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseRcville') ? true : false%>" Cssclass="clear-both">
			 <div class="intitule-110 indent-40"><label for="RcVille">
				<com:TTranslate>DEFINE_VILLE_RC</com:TTranslate></label><span class="champ-oblig">*</span>:
			 </div>
			 <com:TActiveDropDownList
				ID="rcVille" 
				Attributes.title="<%=Prado::localize('DEFINE_VILLE_RC')%>"
				AutoPostBack="true" 
				Attributes.style="width:185px;"
				/>
			<com:TCustomValidator 
				ValidationGroup="<%=$this->getValidationGroup()%>" 
				ControlToValidate="rcVille" 
				Enabled="<%=$this->getEnabled()%>"
				Display="Dynamic"
				ErrorMessage="<%=Prado::localize('TEXT_SELECTIONNEZ_VILLE_RC')%>"
				ClientValidationFunction="validateVilleRcAttributaire"
				EnableClientScript="true" 
				Text = "<span title='<%=Prado::localize('TEXT_SELECTIONNEZ_VILLE_RC')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' /></span>"
				> 						 					 
				<prop:ClientSide.OnValidationError>
						document.getElementById('divValidationSummary').style.display='';
				</prop:ClientSide.OnValidationError>
    		</com:TCustomValidator>
			 <com:TActiveImage id="ImgErrorRc"  ImageUrl='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif'  Attributes.alt='Champ obligatoire' Style="display:none" />
			 <div class="breaker"></div>
			 <div class="intitule-110 indent-40">
				<label for="numero_Rc">
				<com:TTranslate>DEFINE_NUMERO</com:TTranslate></label><span class="champ-oblig">*</span> :
			 </div>
			 <div class="content-bloc">
				<com:TTextBox id="numero_Rc" Cssclass="input-185" Attributes.title="<%=Prado::localize('DEFINE_NUMERO')%>"/>
				<com:TCustomValidator 
					ValidationGroup="<%=$this->getValidationGroup()%>" 
					ControlToValidate="numero_Rc" 
					Enabled="<%=$this->getEnabled()%>"
					Display="Dynamic"
					ErrorMessage="<%=Prado::localize('MSG_ERROR_OBLIGATORY_NUM_RC')%>"
					ClientValidationFunction="validateNumRcAttributaire"
					EnableClientScript="true" 
					Text = "<span title='<%=Prado::localize('MSG_ERROR_OBLIGATORY_NUM_RC')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' /></span>"
				> 						 					 
				<prop:ClientSide.OnValidationError>
						document.getElementById('divValidationSummary').style.display='';
				</prop:ClientSide.OnValidationError>
	    		</com:TCustomValidator>
	    		<com:TActiveImage id="ImgErrorNumRc"  ImageUrl='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif'  Attributes.alt='Champ obligatoire' Style="display:none" />
			 </div>
		</com:TActivePanel>
		
		<com:TActivePanel id="attributaire_panelSiren" Visible="<%=( Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseRcville') || Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseIdentifiantUnique')) ? false : true%>">
			<div class="intitule-110 indent-40">
				<div class="float-left">
					<label for="siren"><com:TTranslate>TEXT_SIREN</com:TTranslate> / </label>
					<label for="siret "><com:TTranslate>TEXT_SIRET</com:TTranslate></label><span class="champ-oblig">*</span>:
				</div>
			</div>
			<com:TTextBox id="siren" CssClass="siren" MaxLength="9" />
			<com:TTextBox id="siret" CssClass="siret" MaxLength="5 "/>
			  <com:TCustomValidator 
				ValidationGroup="<%=$this->getValidationGroup()%>" 
				ControlToValidate="siren" 
				Enabled="<%=$this->getEnabled()%>"
				Display="Dynamic"
				ErrorMessage="<%=Prado::localize('MSG_ERROR_SIRET_INVALIDE')%>"
				ClientValidationFunction="validateSirenAttributaire"
				EnableClientScript="true" 
				Text = "<span title='<%=Prado::localize('MSG_ERROR_SIRET_INVALIDE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' /></span>  "
				> 						 					 
				<prop:ClientSide.OnValidationError>
						document.getElementById('divValidationSummary').style.display='';
				</prop:ClientSide.OnValidationError>
    </com:TCustomValidator>
			<com:TActiveImage id="ImgError"  ImageUrl='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif'  Attributes.alt='Champ obligatoire' Style="display:none" />
	   </com:TActivePanel>
        <!--Debut Bloc Identifiant Unique -->
        <com:TActivePanel ID="panelIdentifiantUnique" visible="<%=(!Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseRcville') && Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseIdentifiantUnique'))? true : false%>"  >

            <div class="intitule-110 indent-40">
                <div class="float-left">
                    <label for="siren"><com:TTranslate>TEXT_SIREN</com:TTranslate><span class="champ-oblig">*</span>:</label>
                </div>
            </div>
            <com:TTextBox ID="identifiantUnique" CssClass="id-national"  Attributes.title="<%=Prado::localize('TEXT_SIREN')%>" />

            <com:TRequiredFieldValidator
                    ValidationGroup="<%=$this->getValidationGroup()%>"
                    ControlToValidate="identifiantUnique"
                    Enabled="<%=$this->getEnabled()%>"
                    Display="Dynamic"
                    ErrorMessage="<%=Prado::localize('TEXT_SIREN')%>"
                    EnableClientScript="true"
                    Text = "<span title='<%=Prado::localize('MSG_ERROR_OBLIGATORY_NUM_RC')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' /></span>"
                    >
                <prop:ClientSide.OnValidationError>
                    document.getElementById('divValidationSummary').style.display='';
                </prop:ClientSide.OnValidationError>
            </com:TRequiredFieldValidator>
            <com:TActiveImage id="ImgErrorIdentifiantUnique"  ImageUrl='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif'  Attributes.alt='Champ obligatoire' Style="display:none" />
        </com:TActivePanel>
        <!--Fin Bloc Identifiant Unique -->
	</com:TActivePanel>

</com:TActivePanel>

<com:TActivePanel id="panel_Ese_Etrengere" CssClass="line no-indent"  >
	<div class="intitule-auto">
		<com:TActiveRadioButton 
              GroupName="entreprise" 
              id="attributaire_etranger" 
              CssClass="radio" 
              Attributes.title="<%=Prado::localize('TEXT_ENTREPRISE_ETRANGER')%>"
              Attributes.onclick="showHideBlocEntreprise(this,'<%=$this->attributaire_nonEtablieFrance->ClientId%>',
              '<%=$this->attributaire_etablieFrance->ClientId%>','<%=$this->ClientId%>','1');"
        />
		<label for="attributaire_etranger"><com:TTranslate>TEXT_ENTREPRISE_ETRANGER</com:TTranslate></label>
	</div>
	<com:TActivePanel Cssclass="clear-both" id="attributaire_nonEtablieFrance" >
		<div class="intitule-110 indent-40">
			<label for="listPays"><com:TTranslate>TEXT_PAYS</com:TTranslate></label><span class="champ-oblig">*</span>:
		</div>
			 <com:DropDownListCountries 
				 id="listPays" 
				 TypeAffichage="withCode" 
				 Attributes.title="<%=Prado::localize('TEXT_PAYS')%>"
				 CssClass="select-185"
			 />
			 <com:TCustomValidator 
				ValidationGroup="<%=$this->getValidationGroup()%>" 
				ControlToValidate="listPays" 
				Enabled="<%=$this->getEnabled()%>"
				Display="Dynamic"
				ErrorMessage="<%=Prado::localize('TEXT_SELECTIONNEZ_UN_PAYS')%>"
				ClientValidationFunction="validatePaysAtributaire"
				EnableClientScript="true" 
				Text = "<span title='<%=Prado::localize('TEXT_SELECTIONNEZ_UN_PAYS')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' /></span> "
			> 						 					 
			<prop:ClientSide.OnValidationError>
					document.getElementById('divValidationSummary').style.display='';
			</prop:ClientSide.OnValidationError>
   		 </com:TCustomValidator>
		 <com:TActiveImage id="ImgErrorPays"  ImageUrl='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif'  Attributes.alt='Champ obligatoire' Style="display:none" />
		<div class="breaker"></div>
		<div class="intitule-110 indent-40">
			<label for="identifiant_national">
			<com:TTranslate>TEXT_IDENTIFIANT_NATIONNAL</com:TTranslate></label><span class="champ-oblig">*</span> :
		</div>
		<div class="content-bloc">
			<com:TTextBox id="identifiant_national" Cssclass="input-185" />
			 <com:TCustomValidator 
				ValidationGroup="<%=$this->getValidationGroup()%>" 
				ControlToValidate="identifiant_national" 
				Enabled="<%=$this->getEnabled()%>"
				Display="Dynamic"
				ErrorMessage="<%=Prado::localize('MSG_ERROR_OBLIGATORY_ID_CONTRY')%>"
				ClientValidationFunction="validateIdentifiantnationalAttributaire"
				EnableClientScript="true" 
				Text = "<span title='<%=Prado::localize('MSG_ERROR_OBLIGATORY_ID_CONTRY')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' /></span>"
				> 						 					 
				<prop:ClientSide.OnValidationError>
						document.getElementById('divValidationSummary').style.display='';
				</prop:ClientSide.OnValidationError>
    		</com:TCustomValidator>
			<com:TActiveImage id="ImgErrorIdentifiant"  ImageUrl='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif'  Attributes.alt='Champ obligatoire' Style="display:none" />
			<div class="info-aide-clear">
				<com:TTranslate>DEFINE_NUM_ENREGISTREMENT_NATIONAL</com:TTranslate>
			</div>
		</div>
	</com:TActivePanel>
	</com:TActivePanel>
<com:TActiveHiddenField id="moduleRc" value="0" />
<div class="spacer-mini"></div>