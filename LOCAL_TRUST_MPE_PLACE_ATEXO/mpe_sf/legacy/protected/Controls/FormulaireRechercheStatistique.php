<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class FormulaireRechercheStatistique extends MpeTPage
{
    private $_parentPage;
    private string $_functionName = '';
    private string $_useAjax = 'false';
    private string $_statistiqueTransversales = 'false';

    public function getParentPage()
    {
        return $this->_parentPage;
    }

    public function setFunctionName($functionName)
    {
        $this->_functionName = $functionName;
    }

    public function getFunctionName()
    {
        return $this->_functionName;
    }

    public function setUseAjax($useAjax)
    {
        $this->_useAjax = $useAjax;
    }

    public function getUseAjax()
    {
        return $this->_useAjax;
    }

    public function setParentPage($parentPage)
    {
        $this->_parentPage = $parentPage;
    }

    public function setDateDebut($dateDebut)
    {
        $this->dateDebut->Text = $dateDebut;
    }

    public function setDateFin($dateFin)
    {
        $this->dateFin->Text = $dateFin;
    }

    public function getStatistiqueTransversales()
    {
        return $this->_statistiqueTransversales;
    }

    public function setStatistiqueTransversales($statistiqueTransversales)
    {
        $this->_statistiqueTransversales = $statistiqueTransversales;
    }

    public function getSelectedEntiteAchat()
    {
        return $this->entiteAchat->getSelectedItem()->getText();
    }

    public function displayFormulaireRecherche()
    {
        $this->displayTexts();
        if ('true' == $this->_statistiqueTransversales) {
            $this->panelStatistiquesTranservales->setVisible(false);
        } else {
            $this->panelStatistiquesTranservales->setVisible(true);
            if (!$this->Page->IsPostBack) {
                $entities = Atexo_EntityPurchase::getEntityPurchase(Atexo_CurrentUser::getCurrentOrganism(), true);
                if (!is_array($entities)) {
                    $entities = [];
                }

                //$entities=Atexo_Util::arrayUnshift($entities, Prado::localize('TOUTES_LES_ENTITES'));
                $this->entiteAchat->DataSource = $entities;
                $this->entiteAchat->DataBind();
            }
        }
    }

    public function displayTexts()
    {
        switch ($this->_parentPage) {
            case 'StatistiquesAvisPublies':
                 $this->titreFormulaire->Text = Prado::localize('SUIVI_AVIS_PUBLIES');
                      $this->rechercheDates->Text = Prado::localize('STATISTIQUES_CONCERNANT_PROCEDURE_MISE_EN_LIGNE').' ';
                break;
        }
    }

    public function getDateDebut()
    {
        return $this->dateDebut->Text;
    }

    public function getDateFin()
    {
        return $this->dateFin->Text;
    }

    public function getSelectedService()
    {
        return $this->entiteAchat->getSelectedValue();
    }

    public function getCumulValeurs()
    {
        return $this->cumulValeurs->Checked;
    }

    public function recalculStatistique($sender, $param)
    {
        $functionName = $this->_functionName;
        $this->Page->$functionName($sender, $param);
    }
}
