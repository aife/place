<?php

namespace Application\Controls;

use Application\Service\Atexo\Redaction\Atexo_Redaction_Document;

/**
 * commentaires.
 *
 * @author  Ezziani Loubna <loubna.ezziani@atexo.com>
 * @copyright Atexo 2012
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class DocumentModuleREDAC extends MpeTTemplateControl
{
    public $_consultation = '';
    private $isRadio;

    public function getIsRadio()
    {
        return $this->isRadio;
    }

    public function setIsRadio($value)
    {
        $this->isRadio = $value;
    }

    public function onLoad($param)
    {
    }

    public function isRadio()
    {
        return 'true' == $this->getIsRadio() ? true : false;
    }

    public function displayDocsRedac($file)
    {
        $dataSource = [];
        $xml = strstr($file, '<?xml');
        if ($xml) {
            $dataSource = (new Atexo_Redaction_Document())->createListeRedacFromXml($xml);
        }
        $this->displayDocs($dataSource);
    }

    public function displayDocs($dataSource)
    {
        if (0 == (is_countable($dataSource) ? count($dataSource) : 0)) {
            $this->panelListeDocsRedac->setDisplay('None');
            $this->panelNoElementFound->setDisplay('Dynamic');
        } else {
            $this->repeaterDocsRedac->DataSource = $dataSource;
            $this->repeaterDocsRedac->DataBind();
            $this->panelListeDocsRedac->setDisplay('Dynamic');
            $this->panelNoElementFound->setDisplay('None');
        }
    }
}
