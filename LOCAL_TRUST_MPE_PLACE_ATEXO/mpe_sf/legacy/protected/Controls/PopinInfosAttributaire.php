<?php

namespace Application\Controls;

use Application\Propel\Mpe\CommonEntreprise;
use Application\Propel\Mpe\CommonEntrepriseQuery;
use Application\Propel\Mpe\CommonOffrePapier;
use Application\Propel\Mpe\CommonOffrePapierQuery;
use Application\Propel\Mpe\CommonOffres;
use Application\Propel\Mpe\CommonOffresQuery;
use Application\Propel\Mpe\CommonTContactContrat;
use Application\Propel\Mpe\CommonTEtablissement;
use Application\Propel\Mpe\CommonTEtablissementQuery;
use Application\Propel\Mpe\Entreprise;
use Application\Propel\Mpe\EntrepriseQuery;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Entreprise;
use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;

/**
 * Page de gestion du bloc "Attriburaire" du contrat.
 *
 * @author Khalid BENAMAR <kbe@atexo.com>
 * @copyright Atexo 2014
 *
 * @version 4
 */
class PopinInfosAttributaire extends MpeTTemplateControl
{
    private $idDiv;
    public $etablissement;
    public $contact;
    private $entreprise;
    public $idOffre;
    public $consultationId;
    public $typeReponse;
    private $refreshComposant;
    public $visibiliteAts;
    public string $callFromOA = 'false'; /*Precise si le composant est appele depuis la page ouverture et analyse*/
    public $_organisme;
    public $_siret;
    public $siretAlertMsg;
    /**
     * @var mixed|string
     */
    public $lienNavigateUrlFicheFournisseur;

    /**
     * @return string|null
     */
    public function getSiretAlertDivForPopInfos()
    {
        $entreprise = $this->getEntreprise();
        $etablissement = $this->getEtablissement();
        $siretAlert = Atexo_Util::getSiretAlertDiv($etablissement, $entreprise);
        if (empty($siretAlert)) {
            $res = '<div><strong>';
        } else {
            $this->siretAlertMsg = $siretAlert['siretAlertMsg'];
            $res = '<div class="' . $siretAlert['color'] . '">';
            $res .= '<strong><span class="fa fa-' . $siretAlert['siretAlertFA'] . '-circle tTip_suite disable-siret" title="' . Prado::localize('VERIFICATION_DU_SIRET') . '"></span>';
        }

        return $res;
    }

    /**
     * @return mixed
     */
    public function getSiretAlertFA()
    {
        return $this->siretAlertFA;
    }

    /**
     * @return mixed
     */
    public function getSiretAlertMsg()
    {
        return $this->siretAlertMsg;
    }

    /**
     * @return mixed
     */
    public function getVisibiliteAts()
    {
        return $this->visibiliteAts;
    }

    /**
     * @param mixed $visibiliteAts
     */
    public function setVisibiliteAts($visibiliteAts)
    {
        $this->visibiliteAts = $visibiliteAts;
    }

    public function onLoad($param)
    {
        if (!$this->Page->getIsPostBack()) {
            $this->remplirChamps();
        }
    }

    /**
     * @return mixed
     */
    public function getTypeReponse()
    {
        return $this->typeReponse;
    }

    /**
     * @param mixed $typeReponse
     */
    public function setTypeReponse($typeReponse)
    {
        $this->typeReponse = $typeReponse;
    }

    /**
     * @return mixed
     */
    public function getRefConsultation()
    {
        return $this->refConsultation;
    }

    /**
     * @param mixed $consultationId
     */
    public function setRefConsultation($consultationId)
    {
        $this->refConsultation = $consultationId;
    }

    /**
     * @return mixed
     */
    public function getIdOffre()
    {
        return $this->idOffre;
    }

    /**
     * @param mixed $idOffre
     */
    public function setIdOffre($idOffre)
    {
        $this->idOffre = $idOffre;
    }

    /**
     * @return mixed
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * @param mixed $contact
     */
    public function setContact($contact)
    {
        $this->contact = $contact;
    }

    /**
     * @return mixed
     */
    public function getEntreprise()
    {
        return $this->entreprise;
    }

    /**
     * @param mixed $entreprise
     */
    public function setEntreprise($entreprise)
    {
        $this->entreprise = $entreprise;
    }

    /**
     * @return mixed
     */
    public function getEtablissement()
    {
        return $this->etablissement;
    }

    /**
     * @param mixed $etablissement
     */
    public function setEtablissement($etablissement)
    {
        $this->etablissement = $etablissement;
    }

    /**
     * @return string
     */
    public function getCallFromOA()
    {
        return $this->callFromOA;
    }

    /**
     * @param string $callFromOA
     */
    public function setCallFromOA($callFromOA)
    {
        $this->callFromOA = $callFromOA;
    }

    /**
     * @return mixed
     */
    public function getOrganisme()
    {
        return $this->_organisme;
    }

    /**
     * @param mixed $organisme
     */
    public function setOrganisme($organisme)
    {
        $this->_organisme = $organisme;
    }

    /**
     * @return mixed
     */
    public function getSiret()
    {
        return $this->_siret;
    }

    /**
     * @param mixed $siret
     */
    public function setSiret($siret)
    {
        $this->_siret = $siret;
    }

    public function remplirChamps()
    {
        $this->verifierEtRecupererEntrepriseEtablissement();
        $offre = (new CommonOffresQuery())->findById($this->getIdOffre());
        if ($this->entreprise instanceof Entreprise) {
            $this->nomEntreprise->Text = $this->entreprise->getNom();
            $this->siretEntreprise->Text = (new Atexo_Entreprise())->getIndentifiantLocalOuEtranger($this->entreprise);

            if ($this->etablissement instanceof CommonTEtablissement) {
                $this->siretEntreprise->Text = (new Atexo_Entreprise())
                        ->getIndentifiantLocalOuEtranger($this->entreprise)
                    . ' '
                    . $this->etablissement->getCodeEtablissement();

                $this->adresse->Text = $this->etablissement->getAdresse();
                $this->codePostal->Text = $this->etablissement->getCodePostal();
                $this->ville->Text = $this->etablissement->getVille();
                $this->pays->Text = $this->etablissement->getPays();

                if ($offre[0] instanceof CommonOffres) {
                    $nomEntreprise = $offre[0]->getNomEntrepriseInscrit();
                    $etablissement = (new CommonTEtablissementQuery())->findByIdEtablissement($offre[0]->getIdEtablissement());
                    if($etablissement[0] instanceof CommonTEtablissement) {
                        $this->adresse->Text = $etablissement[0]->getAdresse();
                        $this->codePostal->Text = $etablissement[0]->getCodePostal();
                        $this->ville->Text = $etablissement[0]->getVille();
                        $this->pays->Text = $etablissement[0]->getPays();
                    }

                    if ("" !== $nomEntreprise) {
                       $entreprise = (new EntrepriseQuery())
                            ->getEntrepriseById(
                                $offre[0]->getEntrepriseId()
                            );
                        if( $entreprise instanceof Entreprise) {
                            $this->nomEntreprise->Text = $entreprise->getNom();
                        }
                    }

                    if ("" !== $offre[0]->getAdresseInscrit()) {
                        $this->adresse->Text = $offre[0]->getAdresseInscrit();
                    }

                    if ("" !== $offre[0]->getCodePostalInscrit()) {
                        $this->codePostal->Text = $offre[0]->getCodePostalInscrit();
                    }
                    if ("" !== $offre[0]->getVilleInscrit()) {
                        $this->ville->Text = $offre[0]->getVilleInscrit();
                    }
                    if ("" !== $offre[0]->getPaysInscrit()) {
                        $this->pays->Text = $offre[0]->getPaysInscrit();
                    }
                }
            }
            if ($this->contact instanceof CommonTContactContrat) {
                $this->nomContact->Text = $this->contact->getNom() . ' ' . $this->contact->getPrenom();
                $this->adresseMail->Text = $this->contact->getEmail();
                $this->tel->Text = $this->contact->getTelephone();
            }
            $this->lienNavigateUrlFicheFournisseur = $this->getUrlCoffre();
            $this->lienFicheFournisseur->NavigateUrl = $this->getUrlCoffre();
        }
        if ((!empty($this->entreprise) || !empty($this->getSiret())) && empty($this->siretEntreprise->Text)) {
            $this->siretEntreprise->Text = $this->getIdentifiantFromOffres();
        }
    }

    public function getIdDiv()
    {
        return $this->idDiv;
    }

    public function setIdDiv($idDiv)
    {
        $this->idDiv = $idDiv;
    }

    protected function getUrlCoffre()
    {
        $complementUrl = '';
        if ($this->getContact() instanceof CommonTContactContrat && $this->getContact()->getIdContactContrat()) {
            $complementUrl = '&idC=' . $this->getContact()->getIdContactContrat();
        } elseif ($this->getIdOffre()) {
            $complementUrl = '&idO=' . $this->getIdOffre() . '&type=' . ($this->getTypeReponse() == Atexo_Config::getParameter('DEPOT_ELECTRONIQUE') ? 'E' : 'P');
        }

        if ($this->visibiliteAts) {
            $ats = sha1($this->entreprise->getId() . '1');
            $callFromOA = $this->getCallFromOA();
            $refCons = $this->getRefConsultation();
            $organisme = $this->getOrganisme();

            $complementUrl .= $complementUrl . '&id=' . $refCons . '&organisme=' . $organisme . '&callFromOA=' . $callFromOA;
            $url = 'index.php?page=Agent.DetailEntreprise&callFrom=agent&idEntreprise=' . $this->entreprise->getId() . '&ats=' . $ats . $complementUrl;
        } else {
            $url = 'index.php?page=Agent.DetailEntreprise&callFrom=agent&idEntreprise=' . $this->entreprise->getId() . $complementUrl;
        }

        return $url;
    }

    public function setRefreshComposant($value)
    {
        $this->refreshComposant = $value;
        $this->remplirChamps();
    }

    public function getRefreshComposant()
    {
        return '';
    }

    /**
     * Permet de verifier si l'entreprise et l'etablissement existent et ne sont pas vide
     * Sinon recupere l'entreprise et l'etablissement via le siret.
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function verifierEtRecupererEntrepriseEtablissement()
    {
        //Cas entreprise nationale
        if (empty($this->getEntreprise()) && !empty($this->getSiret())) {
            $entreprise = (new Atexo_Entreprise())->retrieveCompanyBdeBySiren(substr($this->getSiret(), 0, 9));
            if ($entreprise instanceof Entreprise) {
                $this->setEntreprise($entreprise);
            }
        }

        if (empty($this->getEtablissement()) && !empty($this->getSiret()) && $this->getEntreprise() instanceof Entreprise) {
            $etablissementQuery = new CommonTEtablissementQuery();
            $etablissement = $etablissementQuery->getEtabissementbyIdEseAndCodeEtab(substr($this->getSiret(), 9, 14), $this->getEntreprise()->getId());
            if ($etablissement instanceof CommonTEtablissement) {
                $this->setEtablissement($etablissement);
            }
        }

        //Cas entreprise etrangere
        if (empty($this->getSiret())) {
            $offre = $this->recupererOffre();
            if (($offre instanceof CommonOffres || $offre instanceof CommonOffrePapier) && !empty($offre->getIdentifiantNational())) {
                $entreprise = (new Atexo_Entreprise())->getEntrepriseEtrangereByIdentifiantNational($offre->getIdentifiantNational());
                if ($entreprise instanceof Entreprise) {
                    $this->setEntreprise($entreprise);
                }
            }
        }
    }

    /**
     * Permet de verifier si les conditions d'affichage du lien de la fiche fournisseur sont verifiees.
     *
     * @param CommonOffrePapier|CommonOffre $offre : offre de l'entreprise
     *
     * @return bool
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function afficherLienFicheFournisseur()
    {
        $entreprise = null;
        if ($this->getTypeReponse() == Atexo_Config::getParameter('DEPOT_ELECTRONIQUE')) {
            return true;
        }

        $offrePapierQuery = new CommonOffrePapierQuery();
        $offrePapier = $offrePapierQuery->getOffrePapierById($this->getIdOffre());
        if ($offrePapier instanceof CommonOffrePapier) {
            if (!empty($offrePapier->getSiret())) {
                $entreprise = (new Atexo_Entreprise())->retrieveCompanyBdeBySiren(substr($offrePapier->getSiret(), 0, 9));
            } elseif (!empty($offrePapier->getIdentifiantNational())) {
                $entreprise = (new Atexo_Entreprise())->getEntrepriseEtrangereByIdentifiantNational($offrePapier->getIdentifiantNational());
            }
            if ($entreprise instanceof Entreprise && !empty($entreprise->getId())) {
                return true;
            }
        }

        return false;
    }

    /**
     * Permet de recuperer l'identifiant de l'entreprise depuis l'offre.
     *
     * @return string
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function getIdentifiantFromOffres()
    {
        $offre = $this->recupererOffre();
        if ($offre instanceof CommonOffres) {
            return (!empty($offre->getSiretInscrit())) ? $offre->getSiretInscrit() : $offre->getIdentifiantNational();
        }
        if ($offre instanceof CommonOffrePapier) {
            return (!empty($offre->getSiret())) ? $offre->getSiret() : $offre->getIdentifiantNational();
        }
    }

    /**
     * Permet de recuperer l'offre.
     *
     * @return CommonOffre|CommonOffrePapier
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function recupererOffre()
    {
        if ($this->getTypeReponse() == Atexo_Config::getParameter('DEPOT_ELECTRONIQUE')) {
            $offreQuery = new CommonOffresQuery();
            $offre = $offreQuery->getOffreById($this->getIdOffre());
        } else {
            $offrePapierQuery = new CommonOffrePapierQuery();
            $offre = $offrePapierQuery->getOffrePapierById($this->getIdOffre());
        }

        return $offre;
    }
}
