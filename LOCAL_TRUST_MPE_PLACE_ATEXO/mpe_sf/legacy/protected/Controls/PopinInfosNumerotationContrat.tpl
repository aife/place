<!-- Content for Popover Contrat -->
</script>
<div id="detail_contrat_<%=$this->getIdContrat()%>" class="hidden">
    <div class="popover-body">
        <div><span class="intitule-120"><strong><com:TTranslate>REFERENCE_INTERNE_CONTRAT</com:TTranslate> :</strong></span><%=$this->getReferenceInterne()%></div>
        <div><span class="intitule-120"><strong><com:TTranslate>CONTRAT_NUMERO_COURT</com:TTranslate></strong> :</span><%=$this->getNumeroCourt()%></div>
        <div><span class="intitule-120"><strong><com:TTranslate>CONTRAT_NUMERO_LONG</com:TTranslate></strong> :</span><%=$this->getNumeroLong()%></div>
        <div class="boutons">
            <a href="#" onclick="javascript:popUp('<%=$this->getUrlPageNumerotation()%>', 'yes')" class="bouton float-right">
                <span class="left border-left-blue-btn"></span>
                <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/icone-modification.png" />
                <com:TTranslate>DEFINE_TEXT_MODIFIER</com:TTranslate>
            </a>
        </div>
    </div>
</div>
<!-- End Content for Popover Contrat -->