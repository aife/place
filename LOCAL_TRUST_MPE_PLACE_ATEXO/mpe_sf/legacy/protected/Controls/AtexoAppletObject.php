<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_Config;

class AtexoAppletObject extends MpeTTemplateControl
{
    public $_mainClass;
    public $_classIdApplet;
    public $_params;

    public function getMainClass()
    {
        return $this->_mainClass;
    }

    public function setMainClass($mainClass)
    {
        $this->_mainClass = $mainClass;
    }

    public function getClassIdApplet()
    {
        return $this->_classIdApplet;
    }

    public function setClassIdApplet($value)
    {
        $this->_classIdApplet = $value;
    }

    public function getParams()
    {
        $listeParams = '';
        if (is_array($this->_params)) {
            foreach ($this->_params as $k => $v) {
                $listeParams .= "<param name='".$k."' value='".$v."'/> \n ";
            }
        }

        return $listeParams;
    }

    /*
      *  Permet  activer / désactiver l'utilisation du paramettre java_arguments.
      *
      *  @return String param java_arguments
      *  @author : loubna.ezziani@atexom.com
      *  @version : since 4.5.0
      */
    public function getParamJavaArguments()
    {
        if (Atexo_Config::getParameter('USE_APPLET_MEMORY_PARAM')) {
            return "<param name='java_arguments' value='-Xms".Atexo_Config::getParameter('APPLET_MEMORY_SIZE_MIN').' -Xmx'.Atexo_Config::getParameter('APPLET_MEMORY_SIZE_MAX')."'/>".PHP_EOL;
        }

        return '';
    }

    public function setParams($params)
    {
        $this->_params = $params;
    }

    /**
     * Retourne la liste des jars fournis par l'applet.
     *
     *
     * @author Gregory CHEVRET
     */
    public function getListeJarsApplet(): bool|string
    {
        $versionsFichier = Atexo_Config::getParameter('DIR_PATH_APPLET').DIRECTORY_SEPARATOR.'versions.txt';
        $versionsListe = '';
        if (is_file($versionsFichier)) {
            $versionsListe = file_get_contents($versionsFichier);
        }

        return $versionsListe;
    }

    public function getCodeBaseApplet()
    {
        return Atexo_Config::getParameter('DIR_CODEBASE_APPLET');
    }
}
