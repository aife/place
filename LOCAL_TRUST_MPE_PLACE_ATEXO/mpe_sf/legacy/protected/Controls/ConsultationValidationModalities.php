<?php

namespace Application\Controls;

use Application\Propel\Mpe\CommonConsultation;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Exception;
use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class ConsultationValidationModalities extends MpeTTemplateControl
{
    private $_consultation = false;
    private $_reference = false;
    private $_withException = true;
    private $_typeValidation = '';
    private $_organisme = '';

    public function getReference()
    {
        return $this->_reference;
    }

    public function setReference($reference)
    {
        return $this->_reference = $reference;
    }

    public function getConsultation()
    {
        return $this->_consultation;
    }

    public function setConsultation($consultation)
    {
        return $this->_consultation = $consultation;
    }

    public function getWithException()
    {
        return $this->_withException;
    }

    public function setWithException($withException)
    {
        return $this->_withException = $withException;
    }

    public function onLoad($param)
    {
        $this->panelValidationIntermediaire->setVisible(false);
        $this->panelValidationSimple->setVisible(false);
        $this->panelValidationFinale->setVisible(false);
        if (!($this->getConsultation() instanceof CommonConsultation) && !$this->getReference()) {
            if ($this->getWithException()) {
                throw new Atexo_Exception('ConsultationValidationModalities Error : Attendu id de la référence ou objet consultation');
            } else {
                $this->summaryVisible->Visible = false;

                return;
            }
        }
        $this->_organisme = Atexo_CurrentUser::getCurrentOrganism();
        $langue = Atexo_CurrentUser::readFromSession('lang');
        if (($this->getConsultation() instanceof CommonConsultation)) {
            $this->_consultation = $this->getConsultation();

            if ($this->_consultation->getRegleMiseEnLigne() == Atexo_Config::getParameter('MISE_EN_LIGNE_APRES_VALIDATION_ET_DATE')) {
                $this->datemiseEnLigne->Text = Atexo_Util::iso2frnDateTime($this->_consultation->getDatemiseenligne());
            } elseif ($this->_consultation->getRegleMiseEnLigne() == Atexo_Config::getParameter('MISE_EN_LIGNE_APRES_VALIDATION_MANUELLE')) {
                $this->datemiseEnLigne->Text = Prado::localize('DEFINE_DATE_VALIDATION');
            } elseif ($this->_consultation->getRegleMiseEnLigne() == Atexo_Config::getParameter('MISE_EN_LIGNE_APRES_VALIDATION_ET_ENVOI_BOAMP')) {
                $this->datemiseEnLigne->Text = Prado::localize('DATE_ENVOI_BOAMP');
            } elseif ($this->_consultation->getRegleMiseEnLigne() == Atexo_Config::getParameter('MISE_EN_LIGNE_APRES_VALIDATION_ET_PUBLICATION_BOAMP')) {
                $this->datemiseEnLigne->Text = Prado::localize('DATE_PUBLICATION_BOAMP');
            }
            if ('1' == $this->_consultation->getEtatValidation()) {
                $messageError = Prado::localize('CONSULTATION_DEJA_VALIDER');
            }
            if ($this->_consultation->getIdRegleValidation() == Atexo_Config::getParameter('REGLE_VALIDATION_TYPE_1')
            && '1' == $this->_consultation->getChiffrementOffre()) {
                $messageError = Prado::localize('VALIDATION_IMPOSSIBLE');
            } else {
                $this->_typeValidation = (new Atexo_Consultation())->retrieveTypeValidation($this->_consultation);
                if ($this->_typeValidation == Atexo_Config::getParameter('VALIDATION_SIMPLE')) {
                    $pathEnttityPurchase = Atexo_EntityPurchase::getPathEntityById($this->_consultation->getServiceValidation(), $this->_organisme, $langue);
                    $this->panelValidationSimple->setVisible(true);
                    $this->panelValidationSimple->setCssClass('validation-etape-on');
                    $this->panelValidationIntermediaire->setVisible(false);
                    $this->panelValidationFinale->setVisible(false);
                    $this->eniteAchat3->Text = $pathEnttityPurchase;
                } elseif ($this->_typeValidation == Atexo_Config::getParameter('VALIDATION_FINALE')) {
                    $pathEnttityPurchase = Atexo_EntityPurchase::getPathEntityById($this->_consultation->getServiceValidation(), $this->_organisme, $langue);
                    $this->numeroEtape->Text = '1/1';
                    $this->panelValidationIntermediaire->setVisible(false);
                    $this->panelValidationSimple->setVisible(false);
                    $this->panelValidationFinale->setVisible(true);
                    $this->panelValidationFinale->setCssClass('validation-etape-on');
                    $this->eniteAchat2->Text = $pathEnttityPurchase;
                } elseif ($this->_typeValidation == Atexo_Config::getParameter('VALIDATION_INTERMEDIAIRE_ETAPE1')) {
                    $this->numeroEtape->Text = '2/2';
                    $pathEnttityPurchaseFinal = Atexo_EntityPurchase::getPathEntityById($this->_consultation->getServiceValidation(), $this->_organisme, $langue);
                    $pathEnttityPurchaseIntermidiaire = Atexo_EntityPurchase::getPathEntityById($this->_consultation->getServiceValidationIntermediaire(), $this->_organisme, $langue);
                    $this->panelValidationSimple->setVisible(false);

                    $this->panelValidationIntermediaire->setVisible(true);
                    $this->panelValidationIntermediaire->setCssClass('validation-etape');
                    $this->eniteAchat1->Text = $pathEnttityPurchaseIntermidiaire;

                    $this->panelValidationFinale->setVisible(true);
                    $this->panelValidationFinale->setCssClass('validation-etape-on');
                    $this->eniteAchat2->Text = $pathEnttityPurchaseFinal;
                } elseif ($this->_typeValidation == Atexo_Config::getParameter('VALIDATION_FINALE_ETAPE2')) {
                    $pathEnttityPurchaseFinal = Atexo_EntityPurchase::getPathEntityById($this->_consultation->getServiceValidation(), $this->_organisme, $langue);
                    $pathEnttityPurchaseIntermidiaire = Atexo_EntityPurchase::getPathEntityById($this->_consultation->getServiceValidationIntermediaire(), $this->_organisme, $langue);

                    $this->numeroEtape->Text = '2/2';

                    $this->panelValidationSimple->setVisible(false);
                    $this->panelValidationIntermediaire->setVisible(true);
                    $this->panelValidationIntermediaire->setCssClass('validation-etape');
                    $this->eniteAchat1->Text = $pathEnttityPurchaseIntermidiaire;

                    $this->panelValidationFinale->setVisible(true);
                    $this->panelValidationFinale->setCssClass('validation-etape-on');
                    $this->eniteAchat2->Text = $pathEnttityPurchaseFinal;
                }
            }
            if ($this->_consultation->getDateMiseEnLigneCalcule()) {
                $this->dateMiseEnLigneCalcule->Text = Atexo_Util::iso2frnDateTime($this->_consultation->getDateMiseEnLigneCalcule());
            } else {
                $this->dateMiseEnLigneCalcule->Text = '-';
            }
        }
    }
}
