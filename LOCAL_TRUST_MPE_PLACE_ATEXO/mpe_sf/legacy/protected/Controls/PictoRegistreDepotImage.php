<?php

namespace Application\Controls;

use Application\Service\Atexo\Controller\Atexo_Controller_Front;
use Prado\Prado;
use Prado\Web\UI\WebControls\TImage;

/**
 * commentaires.
 *
 * @author anas ZAKI<anas.zaki@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PictoRegistreDepotImage extends TImage
{
    public string $_activate = 'false';

    public function onLoad($param)
    {
        if ('false' == $this->_activate) {
            $this->setImageUrl(Atexo_Controller_Front::t().'/images/picto-depot-inactive.gif');
        } else {
            $this->setImageUrl(Atexo_Controller_Front::t().'/images/picto-depot.gif');
        }

        $this->setAlternateText(Prado::localize('ICONE_REGISTRE_DES_DEPOTS'));
        $this->setToolTip(Prado::localize('ICONE_REGISTRE_DES_DEPOTS'));
    }

    public function getActivate()
    {
        return $this->_activate;
    }

    public function setActivate($bool)
    {
        $this->_activate = $bool;
    }
}
