<div class="content panelSiege">
	<div class="form-bloc">
			<blockquote>
				<!-- Si Entreprise Française, Afficher les 2 lignes SIREN ET NIC du siege-->
				<com:TPanel ID="panelRaisonSociale" cssClass="clearfix small">
					<label class="col-md-2">
						<com:TTranslate>TEXT_RAISON_SOCIAL</com:TTranslate> :
					</label>
					<div class="col-md-10">
						<com:TLabel ID="texteRaisonSociale" />
					</div>
				</com:TPanel>
				<div class="clearfix small">
					<label class="col-md-2">
						<com:TTranslate>TEXT_LIEU_ETABLISSEMENT</com:TTranslate>
					</label>
					<div class="col-md-10">
						<com:TLabel ID="lieuEtablissement" />
					</div>
				</div>
				<com:TPanel ID="panelSiren" cssClass="clearfix small">
					<label class="col-md-2">
						<com:TTranslate>TEXT_SIREN</com:TTranslate> :
					</label>
					<div class="col-md-10">
						<com:TLabel ID="siren" />
					</div>
				</com:TPanel>
				<!-- Si Entreprise Françcaise, Afficher les 2 lignes SIREN ET NIC du siege-->
				<!-- -->
				<!-- End Si Entreprise Françcaise, Afficher les 2 lignes SIREN ET NIC du siege-->
				<!-- Si Entreprise Entrangere, Afficher la ligne Identifiant National-->
				<com:TPanel ID="panelIdNational" cssClass="clearfix small">
					<label class="control-label col-md-2">
						<com:TTranslate>TEXT_IDENTIFIANT_NATIONNAL</com:TTranslate> :
					</label>
					<div class="col-md-10">
						<com:TLabel ID="idNational" />
					</div>
				</com:TPanel>
				<!-- End Si Entreprise Entrangere, Afficher la ligne Identifiant National-->
			</blockquote>
	</div>
</div>
	