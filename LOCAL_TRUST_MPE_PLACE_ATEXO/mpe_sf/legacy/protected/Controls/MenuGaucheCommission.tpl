<!--Debut colonne gauche-->
	<div class="left-part" id="left-part">
		<!--Debut menu -->
		<div id="menu">
			<ul id="menuList">
				<li class="<%=(in_array($_GET['page'],$this->ArrayPages['GESTION_SEANCES']))?'menu-open menu-on':'menu-open'%>"><span><a accesskey="c" onclick="toggleMenu('menuSeance');" href="javascript:void(0);"><com:TTranslate>DEFINE_GESTION_SEANCES</com:TTranslate></a></span>
					<ul style="display: block;" id="menuSeance" class="ss-menu-open">
						<li class="off"> <a href="index.php?page=Commission.FormulaireSeance&creation"><com:TTranslate>DEFINE_TEXT_CREER</com:TTranslate></a> </li>
						<li class="off"> <a href="index.php?page=Commission.SuiviSeance"><com:TTranslate>DEFINE_TEXT_SUIVRE</com:TTranslate></a> </li>
						<li class="off" style="display: none;"> <a href="#"><com:TTranslate>DEFINE_SIGNATAIRE_COURRIERS</com:TTranslate></a> </li>
					</ul>
				</li>
				<li class="<%=(in_array($_GET['page'],$this->ArrayPages['ADMINISTRATION']))?'menu-open menu-on':'menu-open'%>"><span><a accesskey="c" onclick="toggleMenu('menuAdministration');" href="javascript:void(0);"><com:TTranslate>DEFINE_ADMINISTRATION</com:TTranslate></a></span>
					<ul style="display: none;" id="menuAdministration" class="ss-menu-open">
						<li class="off"> <a href="index.php?page=Commission.ListeCommissions"><com:TTranslate>DEFINE_GERER_COMMISSIONS</com:TTranslate></a> </li>
						<li class="off"> <a href="index.php?page=Commission.ListeIntervenantsExternes"><com:TTranslate>GERER_INTERVENANTS_EXTERNES</com:TTranslate></a> </li>
					</ul>
				</li>
			</ul>
		</div>
		<div class="menu-bottom"></div>
		<!--Fin menu -->
		<script type="text/javascript">
			if (document.getElementById('menuAdministration') != null ) document.getElementById('menuAdministration').style.display = '<%=(in_array($_GET['page'],$this->ArrayPages['ADMINISTRATION']))?'block':'none'%>';
			if (document.getElementById('menuSeance') != null ) document.getElementById('menuSeance').style.display = '<%=(in_array($_GET['page'],$this->ArrayPages['GESTION_SEANCES']))?'block':'none'%>';
		</script>
	</div>
	<!--Fin colonne gauche-->