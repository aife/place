<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Inscrit;
use Prado\Prado;

/**
 * composant pour afficher les informations user.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2010
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class AtexoIdentificationUser extends MpeTPage
{
    private string $_inscrit = '';
    private bool $_postBack = false;

    public function setInscrit($value)
    {
        $this->_inscrit = $value;
    }

    public function setPostBack($value)
    {
        $this->_postBack = $value;
    }

    public function getPostBack()
    {
        return $this->_postBack;
    }

    public function setCallBack($value)
    {
        $this->_callback = $value;
    }

    public function getCallBack()
    {
        return $this->_callback;
    }

    public function onLoad($param)
    {
        if (!$this->getPostBack()) {
            $this->displayInscrit();
        }
    }

    public function setNewInscrit($inscrit)
    {
        $inscrit->setNom($this->nomPerso->Text);
        $inscrit->setPrenom($this->prenomPerso->Text);
        $inscrit->setEmail(trim($this->emailPersonnel->Text));
        $inscrit->setLogin(Atexo_Util::atexoHtmlEntities($this->identifiant->Text));
        $inscrit->setDateCreation(date('Y-m-d H:i:s'));
        $inscrit->setDateModification(date('Y-m-d H:i:s'));
        $inscrit->setProfil('2'); //ATES
        if (Atexo_Module::isEnabled('GenerationAutomatiqueMdpInscrit') && true == $this->pwdAutomatique->checked) {
            $pwd = Atexo_Util::randomMdp(8);
            $this->Page->setViewState('PwdInscrit', $pwd);
            Atexo_Entreprise_Inscrit::updateNewInscritPasswordByArgon2($inscrit, $pwd);
        } elseif ('' != $this->password->Text && Atexo_CurrentUser::PASSWORD_INIT_VALUE != $this->password->Text && '' != $this->confPassword->Text && Atexo_CurrentUser::PASSWORD_INIT_VALUE != $this->confPassword->Text) {
            Atexo_Entreprise_Inscrit::updateNewInscritPasswordByArgon2($inscrit, $this->password->Text);
        }
        $this->Page->setViewState('Inscrit', $inscrit);

        return $inscrit;
    }

    /**
     * Verifier que le login est unique.
     */
    public function verifyLoginCreation($sender, $param)
    {
        $login = $this->identifiant->getText();
        if ($login) {
            $inscrit = (new Atexo_Entreprise_Inscrit())->retrieveInscritByLogin($login);
            if ($inscrit) {
                $this->Page->labelServerSide->Text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";
                $param->IsValid = false;
                $this->loginCreationValidator->ErrorMessage = Prado::localize('TEXT_IDENTIFIANT_EXISTE_DEJA', ['identifiant' => $login]);

                return;
            }
        }
    }

    /**
     * Verifier que le e-mail est unique.
     */
    public function verifyMailCreation($sender, $param)
    {
        $mail = $this->emailPersonnel->getText();
        if ($mail) {
            $inscrit = (new Atexo_Entreprise_Inscrit())->retrieveInscritByMail($mail);
            if ($inscrit) {
                $this->Page->labelServerSide->Text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";
                $param->IsValid = false;
                $this->emailCreationValidator->ErrorMessage = Prado::localize('TEXT_MAIL_EXISTE_DEJA', ['mail' => $mail]);

                return;
            }
        }
    }

    /**
     * Verifier que l'identifiant n'existe pas parmis les utilisateurs
     *  autre que l'utilisateur authentifié.
     */
    public function verifyLoginModification($sender, $param)
    {
        $login = $this->identifiant->Text;
        $inscrit = (new Atexo_Entreprise_Inscrit())->retrieveInscritByLoginForModValidation($login, $this->_inscrit->getId());
        if ($inscrit) {
            $this->Page->labelServerSide->Text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";
            $param->IsValid = false;
            $this->loginModificationValidator->ErrorMessage = Prado::localize('TEXT_IDENTIFIANT_EXISTE_DEJA', ['identifiant' => $login]);

            return;
        }
    }

    /**
     * Verifier que l'adresse electronique n'existe pas parmis les utilisateurs
     *  autre que l'utilisateur authentifiés.
     */
    public function verifyMailModification($sender, $param)
    {
        $mail = $this->emailPersonnel->Text;
        $inscrit = (new Atexo_Entreprise_Inscrit())->retrieveInscritByMailForModValidation($mail, $this->_inscrit->getId());
        if ($inscrit) {
            $this->Page->labelServerSide->Text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";
            $param->IsValid = false;
            $this->emailModificationValidator->ErrorMessage = Prado::localize('TEXT_MAIL_EXISTE_DEJA', ['mail' => $mail]);
            //return;
        }
    }

    /**
     * Chargement des champs de l'inscrit(Compte presonnel).
     */
    public function displayInscrit()
    {
        $inscrit = $this->_inscrit;
        if ($inscrit) {
            $this->nomPerso->Text = $inscrit->getNom();
            $this->prenomPerso->Text = $inscrit->getPrenom();
            $this->emailPersonnel->Text = $inscrit->getEmail();
            $this->identifiant->Text = $inscrit->getLogin();
            $this->password->Text = Atexo_CurrentUser::PASSWORD_INIT_VALUE;
            $this->confPassword->Text = Atexo_CurrentUser::PASSWORD_INIT_VALUE;
            if ('1' == $this->_inscrit->getProfil()) {
                $this->inscritSimple->Checked = true;
            } else {
                $this->adminEntreprise->Checked = true;
            }
        }
    }
}
