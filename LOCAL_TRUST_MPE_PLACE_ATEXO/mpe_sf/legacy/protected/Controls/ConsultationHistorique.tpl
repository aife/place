		<!--Debut Bloc Historique date et Heure limite de remise des plis-->
		<div class="form-field">
			<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
			<div class="content">
				<h3><%=$this->getTitleHistorique()%></h3>
				<com:TPanel CssClass="table-bloc" ID="PanelHistorique">
					<com:TRepeater ID="repeaterHistorique" EnableViewState="true">
					<prop:HeaderTemplate>
					<table class="table-results" summary="<%=Prado::localize('ICONE_HISTORIQUE')%>">
					<caption><com:TTranslate>ICONE_HISTORIQUE</com:TTranslate></caption>
					<thead>
						<tr>
							<th class="top" colspan="4"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></th>
						</tr>
						<tr>
							<th class="col-150" id="actionOrigine"><com:TTranslate>DEFINE_TEXT_ACTION_ORIGINE</com:TTranslate></th>
							<th class="col-150" id="dateHeureAction"><com:TTranslate>DEFINE_TEXT_DATE_HEURE_ACTION</com:TTranslate></th>
							<th class="col-150" id="agentRealisateur"><com:TTranslate>DEFINE_TEXT_AGENT_REALISATEUR_ACTION</com:TTranslate></th>
							<th class="col-200" id="valeurAction"><com:TTranslate>DEFINE_TEXT_VALEUR_DEFINIT_ACTION</com:TTranslate> :<br><%=$this->Parent->Parent->Parent->getDetailHistorique()%></th>
						</tr>
					</thead>
					</prop:HeaderTemplate>
					<prop:ItemTemplate>
						<tr class="<%#($this->ItemIndex%2)?'on':''%>">
							<td class="col-150" headers="actionDateRemise"><%#$this->Page->getActionOrigine($this->Data->getStatut())%></td>
							<td class="col-150" headers="dateEventDateRemise"><%# Application\Service\Atexo\Atexo_Util::iso2frnDateTime($this->Data->getUntrusteddate())%></td>
							<td class="col-150" headers="agentDateRemise"><%#$this->Data->getNomAgent()%> <%#$this->Data->getPrenomAgent()%></td>
							<td class="col-200" headers="valeurDateRemise"><%#$this->Data->getRightValue($this->TemplateControl->getMessage())%></td>
						</tr>
					</prop:ItemTemplate>
					<prop:FooterTemplate>
					</table>					
					</prop:FooterTemplate>								 
					</com:TRepeater>
					</com:TPanel>
				<div class="breaker"></div>
			</div>
			<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
		</div>
		<!--Fin Bloc Historique date et Heure limite de remise des plis-->