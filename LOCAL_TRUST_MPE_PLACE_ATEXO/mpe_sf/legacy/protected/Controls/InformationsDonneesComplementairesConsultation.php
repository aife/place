<?php

namespace Application\Controls;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonProcedureEquivalence;
use Application\Propel\Mpe\CommonTConsultationComptePub;
use Application\Propel\Mpe\CommonTConsultationComptePubQuery;
use Application\Propel\Mpe\CommonTDonneeComplementaire;
use Application\Propel\Mpe\CommonTDonneeComplementairePeer;
use Application\Propel\Mpe\CommonTPubliciteFavoris;
use Application\Propel\Mpe\CommonTPubliciteFavorisPeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_DonnesComplementaires;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Ref;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureEquivalence;
use Application\Service\Atexo\Publicite\Atexo_Publicite_Favoris;
use Prado\Prado;

/**
 * Page de gestion des données complémentaires de la consultation.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 4
 */
class InformationsDonneesComplementairesConsultation extends MpeTTemplateControl
{
    private $consultation;
    private $donneeComplementaire;
    private $_connexion;
    public ?string $langue = null;

    public function getConnexion()
    {
        return $this->_connexion;
    }

    public function setConnexion($connexion)
    {
        $this->_connexion = $connexion;
    }

    public function getDonneeComplementaire()
    {
        return $this->donneeComplementaire;
    }

    public function setDonneeComplementaire($donneeComplementaire)
    {
        $this->donneeComplementaire = $donneeComplementaire;
    }

    public function setConsultation($consultation)
    {
        $this->consultation = $consultation;
    }

    public function getConsultation()
    {
        if (!($this->consultation instanceof CommonConsultation)) {
            $this->consultation = $this->Page->getConsultation();
        }

        return $this->consultation;
    }

    /**
     * recupère la langue.
     */
    public function getLangue()
    {
        return $this->langue;
    }

    // getLangue()

    /**
     * Affecte la langue.
     *
     * @param string $langue
     */
    public function setLangue($langue)
    {
        if ($this->langue !== $langue) {
            $this->langue = $langue;
        }
    }

    // setLangue()

    public function onLoad($param)
    {
        $this->setLangue(Atexo_Languages::readLanguageFromSession());
        //self::chargerComposants();
        $this->desactiverBlocsAllotiePourAjax();
    }

    public function saveDonneComplementaire(&$consultation, $modification = true, $param = false)
    {
        $favorisPub = null;
        $this->_connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').
            Atexo_Config::getParameter('CONST_READ_WRITE'));
        if ($consultation instanceof CommonConsultation) {
            if (Atexo_Module::isEnabled('DonneesComplementaires')) {
                self::saveLtReferentiel($consultation->getId(), $consultation);
                if ($consultation->getIdDonneeComplementaire()) {
                    $this->donneeComplementaire = CommonTDonneeComplementairePeer::retrieveByPK($consultation->getIdDonneeComplementaire(), $this->_connexion);
                } else {
                    $this->donneeComplementaire = new CommonTDonneeComplementaire();
                    $this->donneeComplementaire->setIdFormePrix('');
                }
                $typePrest = 0;
                $delaiPartiel = 0;
                if (Atexo_Module::isEnabled('TypePrestation')) {
                    if ($this->prestationCourante->Checked) {
                        $typePrest = Atexo_Config::getParameter('TYPE_PRESTATION_COURANTE');
                    } elseif ($this->prestationComplexe->Checked) {
                        $typePrest = Atexo_Config::getParameter('TYPE_PRESTATION_COMPLEXE');
                    }
                    if ($this->delaiPartielOui->Checked) {
                        $delaiPartiel = Atexo_Config::getParameter('DELAI_PARTIEL_OUI');
                    } elseif ($this->delaiPartielNon->Checked) {
                        $delaiPartiel = Atexo_Config::getParameter('DELAI_PARTIEL_NON');
                    }
                }
                $this->donneeComplementaire->setTypePrestation($typePrest);
                $this->donneeComplementaire->setDelaiPartiel($delaiPartiel);

                // NB : Traduction non encore gerer
                if (Atexo_Module::isEnabled('DonneesRedac')) {
                    if (
                        !Atexo_Module::isEnabled('Publicite')
                        && !Atexo_Config::getParameter('SPECIFIQUE_PLACE')
                    ) {
                        if ($this->donneeOrmeOui->checked) {
                            $consultation->setDonneeComplementaireObligatoire('1');
                        } else {
                            $consultation->setDonneeComplementaireObligatoire('0');
                        }
                    }
                    $this->donneeComplementaire->save($this->_connexion);
                    $this->donneeFormeGroupement->saveFormeGroupement($this->donneeComplementaire);
                    $this->donneeProcedure->saveProcedure($this->donneeComplementaire);
                    $this->donneeDureeMarche->saveDureeMarche($this->donneeComplementaire);
                    $this->donneeMontantMarche->saveMontantMarche($this->donneeComplementaire);
                    if (!$consultation->getAlloti()) {
                        $this->donneeVariante->saveVariante($this->donneeComplementaire);
                    }
                    $oldIdCritere = $this->donneeComplementaire->getIdCritereAttribution();
                    $this->donneeCriteresAttribution->saveCritereAttribution($this->donneeComplementaire, $param);
                    $newIdCritere = $this->donneeComplementaire->getIdCritereAttribution();
                    if ($oldIdCritere && $oldIdCritere != $newIdCritere) {
                        $lots = $consultation->getAllLots();
                        foreach ($lots as $lot) {
                            // supprimer les critéres atribution des lots
                            $donneeComplementaireLot = $lot->getDonneComplementaire();
                            if ($donneeComplementaireLot instanceof CommonTDonneeComplementaire) {
                                (new Atexo_DonnesComplementaires())->deleteCritereAttribution($donneeComplementaireLot, $this->_connexion);
                                $donneeComplementaireLot->setIdCritereAttribution($newIdCritere);
                                $donneeComplementaireLot->save($this->_connexion);
                            }
                        }
                    }
                    //save pour avoir l id de l objet donneeComplementaire
                    $this->donneeComplementaire->save($this->_connexion);
                    if (!$consultation->getAlloti()) {
                        $this->donneeFormePrix->save($this->donneeComplementaire);
                    }
                }
                if (Atexo_Module::isEnabled('ConsultationPiecesDossiers')) {
                    $this->PiecesDossierReponse->savePiecesDossierReponse($this->donneeComplementaire);
                }
                // Caution provisoire
                if (Atexo_Module::isEnabled('ConsultationCautionProvisoire')) {
                    $cautionProvisoire = $this->donneeComplementaire->getCautionProvisoire();
                    if (!$consultation->getAlloti()) {
                        $this->donneeComplementaire->setCautionProvisoire(str_replace(',', '.', $this->cautionProvisoire->Text));
                        $this->saveHistoriqueCaution($cautionProvisoire, $consultation, $this->donneeComplementaire, $this->_connexion, Atexo_CurrentUser::getCurrentOrganism(), $modification);
                    } else {
                        $this->donneeComplementaire->setCautionProvisoire('');
                    }
                }
                //Prix d'acquisition de plans
                if (Atexo_Module::isEnabled('ConsultationPrixAcquisition')) {
                    $this->donneeComplementaire->setPrixAquisitionPlans(str_replace(',', '.', $this->prixAquisitaionPlans->Text));
                }

                //Autres données complémentaires présentes dans les consultations ou les lots
                if (!$consultation->getAlloti()) {
                    $this->saveAutresDonneesComplementairesConsultationLots($this->donneeComplementaire, $consultation, $modification);
                }
                $favorisPub = null;
                if (Atexo_Module::isEnabled('Publicite') && 'etapeDonneesComplementaires' == $this->Page->getViewState('etapeCourante')) {
                    if ($this->enregistrementFavoris->Checked) {
                        if ($this->favorisPublicite->selectedValue) {
                            $favorisPub = CommonTPubliciteFavorisPeer::retrieveByPK($this->favorisPublicite->selectedValue, $this->_connexion);
                        } elseif ($this->nomFavorisPublicite->SafeText) {
                            $favorisPub = new CommonTPubliciteFavoris();
                            $favorisPub->setLibelle($this->nomFavorisPublicite->SafeText);
                            $favorisPub->setOrganisme(Atexo_CurrentUser::getOrganismAcronym());
                            $favorisPub->setServiceId(Atexo_CurrentUser::getCurrentServiceId());
                        }
                    }
                }
                //Sauvegarde des informations du "Regime Financier"
                if (Atexo_Module::isEnabled('DonneesRedac')) {
                    $this->regimeFinancier->saveRegimeFinancier($this->donneeComplementaire, $favorisPub);
                }

                // Sauvegarder conditions de participation
                $this->donneeComplementaire->setActiviteProfessionel(
                    '' !== $this->activiteProfessionel->Text ? $this->activiteProfessionel->Text : null
                );

                $this->donneeComplementaire->setEconomiqueFinanciere(
                    '' !== $this->economiqueFinanciere->Text ? $this->economiqueFinanciere->Text : null
                );

                $this->donneeComplementaire->setTechniquesProfessionels(
                    '' !== $this->techniquesProfessionels->Text ? $this->techniquesProfessionels->Text : null
                );

                //Sauvegarde
                $this->donneeComplementaire->save($this->_connexion);
                if ($this->donneeComplementaire->getIdDonneeComplementaire()) {
                    $consultation->setIdDonneeComplementaire($this->donneeComplementaire->getIdDonneeComplementaire());
                }
                //Domaines d'activites
                $this->saveDomaineActivites($consultation);
            }
            if (Atexo_Config::getParameter('AFFICHAGE_DONNEES_COMPLEMENTAIRES_PERSONNALISEE')) {
                //Addresse de depot / Lieu d'ouverture / Addresse de retrait
                if (Atexo_Module::isEnabled('ConsultationLieuOuverturePlis')) {
                    $oldLieu = $consultation->getLieuOuverturePlis();
                    $consultation->setLieuOuverturePlis((new Atexo_Config())->toPfEncoding($this->lieuOuverturePlis->Text));
                    $lieuOuverture = 'setLieuOuverturePlis'.ucfirst(strtolower(Atexo_CurrentUser::readFromSession('lang')));
                    $consultation->$lieuOuverture((new Atexo_Config())->toPfEncoding($this->lieuOuverturePlis->Text));
                    $this->saveHistoriqueLieuOuverture($oldLieu, $consultation, $this->_connexion, Atexo_CurrentUser::getCurrentOrganism(), $modification);
                }
                if (Atexo_Module::isEnabled('ConsultationAdresseRetraisDossiers')) {
                    $oldAdresseRetratit = $consultation->getAdresseRetraisDossiers();
                    $consultation->setAdresseRetraisDossiers((new Atexo_Config())->toPfEncoding($this->retraitDossiers->Text));
                    $adresseRetraisDossiers = 'setAdresseRetraisDossiers'.ucfirst(strtolower(Atexo_CurrentUser::readFromSession('lang')));
                    $consultation->$adresseRetraisDossiers((new Atexo_Config())->toPfEncoding($this->retraitDossiers->Text));
                    $this->saveHistoriqueAdresseRetrait($oldAdresseRetratit, $consultation, $this->_connexion, Atexo_CurrentUser::getCurrentOrganism(), $modification);
                }
                if (Atexo_Module::isEnabled('ConsultationAdresseDepotOffres')) {
                    $oldAdressedepot = $consultation->getAdresseDepotOffres();
                    $consultation->setAdresseDepotOffres((new Atexo_Config())->toPfEncoding($this->addDepotDossiers->Text));
                    $adresseDepotOffre = 'setAdresseDepotOffres'.ucfirst(strtolower(Atexo_CurrentUser::readFromSession('lang')));
                    $consultation->$adresseDepotOffre((new Atexo_Config())->toPfEncoding($this->addDepotDossiers->Text));
                    $this->saveHistoriqueAdresseDepotOffres($oldAdressedepot, $consultation, $this->_connexion, Atexo_CurrentUser::getCurrentOrganism(), $modification);
                }

                // variantes
                $varianteOld = $consultation->getVariantes();
                $consultation->setVariantes('');
                if (Atexo_Module::isEnabled('ConsultationVariantesAutorisees') && !$consultation->getAlloti()) {
                    $variantes = '0';
                    if ($this->variantesOui->Checked) {
                        $variantes = '1';
                    }
                    $consultation->setVariantes($variantes);
                    $this->saveHistoriqueVariantes($varianteOld, $consultation, $this->_connexion, Atexo_CurrentUser::getCurrentOrganism(), $modification);
                }
            }

            //Sauvegarde des donnees du compte de publicite selectionne
            $etapeCourante = $_GET['etape'] ?? $this->getViewState('etapeCourante');
            if (
                Atexo_Module::isEnabled('Publicite')
                && 'etapeDonneesComplementaires' == $etapeCourante
                && !Atexo_Config::getParameter('SPECIFIQUE_PLACE')
            ) {
                $this->donneesComplementairesComptePublicite->saveDonneesComptesPublicite($this->donneeComplementaire, $favorisPub, $this->_connexion);
            }
        }
    }

    public function chargerComposants($recharger = false)
    {
        $donnesComplementaitre = null;
        if (Atexo_Module::isEnabled('DonneesComplementaires')) {
            $this->_connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            if ($this->getConsultation() instanceof CommonConsultation) {
                if ($this->getConsultation()->getIdDonneeComplementaire()) {
                    $donnesComplementaitre = CommonTDonneeComplementairePeer::retrieveByPK($this->getConsultation()->getIdDonneeComplementaire(), $this->_connexion);
                }
                if ($donnesComplementaitre instanceof CommonTDonneeComplementaire) {
                    $this->donneeComplementaire = $donnesComplementaitre;
                } else {
                    $this->donneeComplementaire = new CommonTDonneeComplementaire();
                }
                $this->customizeForm();
                if ('1' == $this->getConsultation()->getAlloti()) {
                    $this->masquerAutresDonneesComplementairesConsultationLots();
                } else {
                    $this->afficherAutresDonneesComplementairesConsultationLots();
                }

                //Echantillons demandés
                if (Atexo_Module::isEnabled('ConsultationEchantillonsDemandes')) {
                    $this->echantillonsDemandes->setDonneesComplementaires($this->donneeComplementaire);
                    if ('1' == $this->getConsultation()->getAlloti()) {
                        $this->echantillonsDemandes->setAfficher(false);
                    } else {
                        $this->echantillonsDemandes->setAfficher(true);
                    }
                    $this->echantillonsDemandes->setLangue($this->getLangue());
                    $this->echantillonsDemandes->setValidationGroup('validateInfosConsultation');
                    $this->echantillonsDemandes->chargerComposant($recharger);
                }
                //Reunions
                if (Atexo_Module::isEnabled('ConsultationReunion')) {
                    $this->reunions->setDonneesComplementaires($this->donneeComplementaire);
                    if ('1' == $this->getConsultation()->getAlloti()) {
                        $this->reunions->setAfficher(false);
                    } else {
                        $this->reunions->setAfficher(true);
                    }
                    $this->reunions->setLangue($this->getLangue());
                    $this->reunions->setValidationGroup('validateInfosConsultation');
                    $this->reunions->chargerComposant($recharger);
                }
                //Visites des lieux
                if (Atexo_Module::isEnabled('ConsultationVisiteDesLieux')) {
                    $this->visitesLieux->setDonneesComplementaires($this->donneeComplementaire);
                    $this->visitesLieux->setConsultation($this->consultation);
                    $this->visitesLieux->setLot('0');
                    if ('1' == $this->getConsultation()->getAlloti()) {
                        $this->visitesLieux->setAfficher(false);
                    } else {
                        $this->visitesLieux->setAfficher(true);
                    }
                    $this->visitesLieux->setLangue($this->getLangue());
                    $this->visitesLieux->setValidationGroup('validateInfosConsultation');
                    $this->visitesLieux->chargerComposant($recharger);
                }
                //Qualifications
                if (Atexo_Module::isEnabled('ConsultationQualification')) {
                    //$this->qualification->setDonneesComplementaires($this->donneeComplementaire);
                    if ('1' == $this->getConsultation()->getAlloti()) {
                        $this->panelQualification->setDisplay('None');
                        $this->qualification->setAfficher(false);
                    } else {
                        $this->panelQualification->setDisplay('Dynamic');
                        $this->qualification->setAfficher(true);
                    }
                    $this->qualification->setObjet($this->getConsultation());
                    $this->qualification->setConnexion($this->_connexion);
                    $this->qualification->chargerComposant();
                } else {
                    $this->panelQualification->Visible = false;
                }
                //Agrements
                if (Atexo_Module::isEnabled('ConsultationAgrement')) {
                    //$this->qualification->setDonneesComplementaires($this->donneeComplementaire);
                    if ('1' == $this->getConsultation()->getAlloti()) {
                        $this->panelAgrement->setDisplay('None');
                        $this->agrements->setAfficher(false);
                    } else {
                        $this->panelAgrement->setDisplay('Dynamic');
                        $this->agrements->setAfficher(true);
                    }
                    $this->agrements->setObjet($this->getConsultation());
                    $this->agrements->setConnexion($this->_connexion);
                    $this->agrements->chargerComposant();
                } else {
                    $this->panelAgrement->Visible = false;
                }
            }
            if (!$this->Page->isPostBack) {
                $this->formPrix->value = '0';
                //type de prestation et delai partiel
                $this->prestationCourante->Checked = true;
                $this->delaiPartielOui->Checked = true;
                //Domaines d'activites
                $this->chargerDomainesActivites($this->getConsultation());
                if ($donnesComplementaitre instanceof CommonTDonneeComplementaire) {
                    //Type prestation
                    if (Atexo_Module::isEnabled('TypePrestation')) {
                        if ($donnesComplementaitre->getTypePrestation() == Atexo_Config::getParameter('TYPE_PRESTATION_COURANTE')) {
                            $this->prestationCourante->Checked = true;
                        } elseif ($donnesComplementaitre->getTypePrestation() == Atexo_Config::getParameter('TYPE_PRESTATION_COMPLEXE')) {
                            $this->prestationComplexe->Checked = true;
                        }
                        if ($donnesComplementaitre->getDelaiPartiel() == Atexo_Config::getParameter('DELAI_PARTIEL_OUI')) {
                            $this->delaiPartielOui->Checked = true;
                        } elseif ($donnesComplementaitre->getDelaiPartiel() == Atexo_Config::getParameter('DELAI_PARTIEL_NON')) {
                            $this->delaiPartielNon->Checked = true;
                        }
                    }

                    if (Atexo_Module::isEnabled('ConsultationPrixAcquisition')) {
                        $this->prixAquisitaionPlans->Text = $donnesComplementaitre->getPrixAquisitionPlans();
                    }

                    if ('1' != $this->getConsultation()->getAlloti()) {
                        $this->chargerAutresDonneesComplementairesConsultationLots($donnesComplementaitre);
                    }
                }

                //Affichage des données complémentaires
                $this->chargerInformationsComplementaires();

                // Affichage des LT REFERENTIEL
                $this->chargerReferentiels($this->getConsultation(), $this->getConsultation()->getAlloti());

                if (Atexo_Module::isEnabled('Publicite')) {
                    $this->initialiserInfosPublicite($this->getConsultation());
                }
            }

            if (Atexo_Module::isEnabled('DonneesRedac')) {
                //$this->initialiserDonneesComplementairesRegimeFinancier();
                $this->afficherDonneesRedac($recharger, $donnesComplementaitre);
            }
            if (Atexo_Module::isEnabled('ConsultationPiecesDossiers')) {
                $this->PiecesDossierReponse->setDonneeComplementaire($this->donneeComplementaire);
            }

            /**
             * Initialisation valeur Participation
             */
            $this->activiteProfessionel->Text  = $this->donneeComplementaire->getActiviteProfessionel();
            if ($this->activiteProfessionel->Text === '') {
                $this->activiteProfessionel->Text  = Prado::localize('TEXT_CONDITIONS_PARTICIPATION');
            }

            $this->economiqueFinanciere->Text  = $this->donneeComplementaire->getEconomiqueFinanciere();
            if ($this->economiqueFinanciere->Text === '') {
                $this->economiqueFinanciere->Text  = Prado::localize('TEXT_CONDITIONS_PARTICIPATION');
            }

            $this->techniquesProfessionels->Text  = $this->donneeComplementaire->getTechniquesProfessionels();
            if ($this->techniquesProfessionels->Text === '') {
                $this->techniquesProfessionels->Text  = Prado::localize('TEXT_CONDITIONS_PARTICIPATION');
            }
        }
    }

    /*
     * Permet de valider données complémentaire
     */
    public function validerDonneesComplementaires(&$isValid, &$arrayMsgErreur)
    {
        if (Atexo_Module::isEnabled('DonneesRedac') && true == $this->donneeOrmeOui->Checked && ($this->getConsultation() instanceof CommonConsultation)) {
            if (!$this->getConsultation()->getAlloti()) {
                $this->donneeFormePrix->validateDataFormePrix($isValid, $arrayMsgErreur);
                //Validation Ccag Ref
                $this->donneeVariante->validerDonneesComplementairesVariantes($isValid, $arrayMsgErreur);
            }
            $this->donneeCriteresAttribution->validateCritereAttribution($isValid, $arrayMsgErreur, $this->getConsultation()->getAlloti());
        }
    }

    /*
     * retourne si données complèmentaires orme est checked
     */
    public function getDonneeOrmeChecked()
    {
        return $this->donneeOrmeOui->Checked;
    }

    /*
     * afficher les Blocs des donnees REDACS
     */
    public function afficherDonneesRedac($recharger, $donnesComplementaitre)
    {
        if ($this->getConsultation()->getAlloti()) {
            $this->donneeVariante->setAfficher(false);
            $this->donneeFormePrix->setAfficher(false);
            $this->donneeDureeMarche->setAfficherMarcheReconductible(false);
            $this->donneeMontantMarche->setAfficher(false);
        } else {
            $this->donneeVariante->setAfficher(true);
            $this->donneeFormePrix->setAfficher(true);
            $this->donneeDureeMarche->setAfficherMarcheReconductible(true);
            $this->donneeMontantMarche->setAfficher(true);
        }
        if ($donnesComplementaitre instanceof CommonTDonneeComplementaire) {
            $this->donneeComplementaire = $donnesComplementaitre;
            $this->donneeFormePrix->afficherDataFormeMarcheFormePrix($donnesComplementaitre);
        } else {
            $this->donneeComplementaire = new CommonTDonneeComplementaire();
            $this->donneeFormePrix->afficherDataFormeMarcheFormePrix();
        }

        if ($this->Page->isCallBack) {
            if (
                Atexo_Module::isEnabled('DonneesRedac')
                && Atexo_Config::getParameter('SPECIFIQUE_PLACE')
            ) {
                if ('1' == $this->getConsultation()->getDonneeComplementaireObligatoire()) {
                    $this->donneeOrmeOui->Checked = true;
                    $this->donneeOrmeNon->Checked = false;
                    $this->panelDonneesRedacEnabled->enabled = true;
                    // Permet d'afficher tous les blocs si 'OUI' est coché
                    $this->Page->scriptJs->Text .= "
			<script>
				J('.panel-donnees-complementaires').css('display','block');
				J('.panel-donnees-redac .title-toggle').removeClass('title-toggle').addClass('title-toggle-open');
				J('.panel-donnees-redac .title-toggle-open').removeClass('detail-toggle-inactive');
				J('.panel-donnees-redac .donnees-redac').removeClass('panel-off');
			</script>
			";
                } else {
                    $this->donneeOrmeNon->Checked = true;
                    $this->donneeOrmeOui->Checked = false;
                    $this->panelDonneesRedacEnabled->enabled = false;
                    /* Permet de masquer tous les blocs si 'NON' est coché */
                    $this->Page->scriptJs->Text .= "
			<script>
				J('.panel-donnees-complementaires').css('display','none');
				J('.panel-donnees-redac .title-toggle').addClass('detail-toggle-inactive');
				J('.panel-donnees-redac .donnees-redac').addClass('panel-off');
				J('.panel-donnees-redac .title-toggle-open').removeClass('title-toggle-open').addClass('title-toggle');
			</script>
			";
                }
            } else {
                if ($this->getConsultation() instanceof CommonConsultation && null == $this->getConsultation()->getId() && (!$this->getConsultation()->getDonneeComplementaireObligatoire() && !$this->getConsultation()->getDonneePubliciteObligatoire())) {
                    $this->Page->scriptJs->Text .= "<script>activerBlocsDonneesComptePublicite();activerBlocsDonneesComplementaires();cocherDecocherBoutonDonneesOrmeOui('ctl0_CONTENU_PAGE_bloc_etapeIdentification_intituleRedactionOui');</script>";
                } else {
                    if (($this->getConsultation()->getDonneeComplementaireObligatoire() && $this->getConsultation()->getDonneePubliciteObligatoire()) ||
                        (!$this->getConsultation()->getDonneeComplementaireObligatoire() && $this->getConsultation()->getDonneePubliciteObligatoire())
                    ) {
                        $this->Page->scriptJs->Text .= "<script>activerBlocsDonneesComptePublicite();activerBlocsDonneesComplementaires();cocherDecocherBoutonDonneesOrmeOui('ctl0_CONTENU_PAGE_bloc_etapeIdentification_intituleRedactionOui');</script>";
                    } elseif (!$this->getConsultation()->getDonneeComplementaireObligatoire() && !$this->getConsultation()->getDonneePubliciteObligatoire()) {
                        $this->Page->scriptJs->Text .= "<script>desactiverBlocsDonneesComptePublicite();desactiverBlocsDonneesComplementaires();cocherDecocherBoutonDonneesOrmeOui('ctl0_CONTENU_PAGE_bloc_etapeIdentification_intituleRedactionOui');</script>";
                    } elseif ($this->getConsultation()->getDonneeComplementaireObligatoire() && !$this->getConsultation()->getDonneePubliciteObligatoire()) {
                        $this->Page->scriptJs->Text .= "<script>activerBlocsDonneesComplementaires();desactiverBlocsDonneesComptePublicite();cocherDecocherBoutonDonneesOrmeOui('ctl0_CONTENU_PAGE_bloc_etapeIdentification_intituleRedactionOui');</script>";
                    }
                }
            }
        }

        $this->donneeFormeGroupement->setDonneeComplementaire($this->donneeComplementaire);
        $this->donneeProcedure->setDonneeComplementaire($this->donneeComplementaire);
        $this->donneeDureeMarche->setDonneeComplementaire($this->donneeComplementaire);
        $this->donneeVariante->setDonneeComplementaire($this->donneeComplementaire);
        $this->donneeMontantMarche->setDonneeComplementaire($this->donneeComplementaire);
        $this->initialiserDonneesComplementairesRegimeFinancier($recharger);
        $this->regimeFinancier->setDonneeComplementaire($this->donneeComplementaire);
        $this->donneeFormePrix->setDonneeComplementaire($this->donneeComplementaire);
        $this->donneeCriteresAttribution->setDonneeComplementaire($this->donneeComplementaire);
        $this->donneeCriteresAttribution->setObjet($this->getConsultation());
        $this->donneeProcedure->fillElectronicCatalog();
        $this->donneeFormeGroupement->fillFormeGroupement();
        $this->donneeDureeMarche->setConsultation($this->getConsultation());

        if ($recharger) {
            $this->donneeFormePrix->chargerComposant();
            $this->donneeVariante->chargerComposant();
            $this->donneeCriteresAttribution->chargerComposant($recharger);
            $this->donneeMontantMarche->chargerComposant();
        }
    }

    public function customizeForm()
    {
        //caution provisoire
        if (Atexo_Module::isEnabled('ConsultationCautionProvisoire') && Atexo_Module::isEnabled('DonneesComplementaires')) {
            $this->panelCautionProvisoire->setDisplay('Dynamic');
            $this->validatorRegularCautionProvisoire->setEnabled(true);
        } else {
            $this->panelCautionProvisoire->setDisplay('None');
            $this->validatorRegularCautionProvisoire->setEnabled(false);
        }
        //Prix d'acquisition
        if (Atexo_Module::isEnabled('ConsultationPrixAcquisition') && Atexo_Module::isEnabled('DonneesComplementaires')) {
            $this->panelPrixAquisition->setDisplay('Dynamic');
            $this->validatorRegularPrixAquisitaionPlans->setEnabled(true);
        } else {
            $this->panelPrixAquisition->setDisplay('None');
            $this->validatorRegularPrixAquisitaionPlans->setEnabled(false);
        }

        // Domaines d'activités LT-Ref
        if (Atexo_Module::isEnabled('ConsultationDomainesActivitesLtReferentiel')) {
            $this->panelDomaineActiviteLtRef->setDisplay('Dynamic');
            $this->validatorDomaineActiviteRef->setEnabled(true);
        } else {
            $this->panelDomaineActiviteLtRef->setDisplay('None');
            $this->validatorDomaineActiviteRef->setEnabled(false);
        }

        if (Atexo_Module::isEnabled('ConsultationAdresseRetraisDossiers') && Atexo_Config::getParameter('AFFICHAGE_DONNEES_COMPLEMENTAIRES_PERSONNALISEE')) {
            $this->panelAdresseRetraitDossiers->setDisplay('Dynamic');
        } else {
            $this->panelAdresseRetraitDossiers->setDisplay('None');
        }

        //Adresse de depot d'offres
        if (Atexo_Module::isEnabled('ConsultationAdresseDepotOffres') && Atexo_Config::getParameter('AFFICHAGE_DONNEES_COMPLEMENTAIRES_PERSONNALISEE')) {
            $this->panelAdresseDepot->setDisplay('Dynamic');
        } else {
            $this->panelAdresseDepot->setDisplay('None');
        }
        //Lieu ouverture de Pli
        if (Atexo_Module::isEnabled('ConsultationLieuOuverturePlis') && Atexo_Config::getParameter('AFFICHAGE_DONNEES_COMPLEMENTAIRES_PERSONNALISEE')) {
            $this->panelOuverturePlis->setDisplay('Dynamic');
        } else {
            $this->panelOuverturePlis->setDisplay('None');
        }
    }

    /**
     * Permet de charger les donnees Complémentaires appartenant à la consultation et aux lots.
     *
     * @param $donnesComplementaitre: objet données complémentaires
     */
    public function chargerAutresDonneesComplementairesConsultationLots($donnesComplementaitre)
    {
        if (($donnesComplementaitre instanceof CommonTDonneeComplementaire) && Atexo_Module::isEnabled('ConsultationCautionProvisoire')) {
            //Cautions provisoires
            $this->cautionProvisoire->Text = $donnesComplementaitre->getCautionProvisoire();
        }
    }

    /**
     * Permet d'afficher les autres données complémentaires des consultations et des lots.
     */
    public function afficherAutresDonneesComplementairesConsultationLots()
    {
        if (Atexo_Module::isEnabled('ConsultationCautionProvisoire')) {
            $this->panelCautionProvisoire->setDisplay('Dynamic');
        } else {
            $this->panelCautionProvisoire->setDisplay('None');
        }

        if (Atexo_Module::isEnabled('ConsultationVariantesAutorisees') && Atexo_Config::getParameter('AFFICHAGE_DONNEES_COMPLEMENTAIRES_PERSONNALISEE')) {
            $this->panelVariantesAutorisees->setDisplay('Dynamic');
        }
    }

    /**
     * Permet de masquer les autres données complémentaires des consultations et des lots.
     */
    public function masquerAutresDonneesComplementairesConsultationLots()
    {
        if (Atexo_Module::isEnabled('ConsultationCautionProvisoire')) {
            $this->panelCautionProvisoire->setDisplay('None');
        }
        if (Atexo_Module::isEnabled('ConsultationVariantesAutorisees') && Atexo_Config::getParameter('AFFICHAGE_DONNEES_COMPLEMENTAIRES_PERSONNALISEE')) {
            $this->panelVariantesAutorisees->setDisplay('None');
        }
    }

    /**
     * Permet de sauvegarder les autres données complémentaires consultation et lots.
     *
     * @param $donneesComplementaires: objet données complémentaires
     */
    public function saveAutresDonneesComplementairesConsultationLots(&$donneesComplementaires, &$consultation, $modification = true)
    {
        //Echantillons
        if (Atexo_Module::isEnabled('ConsultationEchantillonsDemandes')) {
            $this->echantillonsDemandes->save($donneesComplementaires, $consultation, '0', $this->_connexion, $modification);
        }

        //Reunions
        if (Atexo_Module::isEnabled('ConsultationReunion')) {
            $this->reunions->save($donneesComplementaires, $consultation, '0', $this->_connexion, $modification);
        }
        //Visites des lieux
        if (Atexo_Module::isEnabled('ConsultationVisiteDesLieux')) {
            $this->visitesLieux->save($consultation, '0', $donneesComplementaires, $this->_connexion, $modification);
        }
        //Qualification
        if (Atexo_Module::isEnabled('ConsultationQualification')) {
            $this->qualification->save($consultation, false, $modification);
        }
        //Qualification
        if (Atexo_Module::isEnabled('ConsultationAgrement')) {
            $this->agrements->save($consultation, false, $modification);
        }
    }

    /**
     * Permet de sauvegarder l'historique Caution.
     *
     * @param $consultation: Objet consultation
     * @param $connexion: Objet connexion
     * @param $org: l'acronyme de l'organisme
     */
    public function saveHistoriqueCaution($cautionProvisoireOld, $consultation, $donneComplementaire, $connexion, $org, $modification = true)
    {
        $arrayParameters = [];
        if (Atexo_Module::isEnabled('ConsultationCautionProvisoire') && (trim($cautionProvisoireOld) != trim($donneComplementaire->getCautionProvisoire()) || !$modification)) {
            $nomElement = Atexo_Config::getParameter('ID_HISTORIQUE_CAUTION_PROVISOIRE');
            $lot = '0';
            $valeur = '0';
            $detail1 = '';
            $detail2 = '';
            if ($donneComplementaire->getCautionProvisoire()) {
                $valeur = '1';
                $detail1 = $donneComplementaire->getCautionProvisoire();
            }
            if ($modification) {
                $statut = Atexo_Config::getParameter('MODIFICATION_FILE');
            } else {
                $statut = Atexo_Config::getParameter('CREATION_FILE');
            }
            //Sauvegarde
            $arrayParameters[0] = $consultation->getId();
            $arrayParameters[1] = $nomElement;
            $arrayParameters[2] = $valeur;
            $arrayParameters[3] = $detail1;
            $arrayParameters[4] = $detail2;
            $arrayParameters[5] = $lot;
            $arrayParameters[6] = $org;
            $arrayParameters[7] = $statut;
            (new Atexo_Consultation())->saveHistoriqueConsultation($arrayParameters, $connexion);
        }
    }

    /**
     * Permet de préciser si la modification est permise.
     *
     * @param $consultation: objet consultation
     * Si Oui retourne true, sinon false
     */
    public function isModificationAllowed($consultation)
    {
        return (new Atexo_Consultation())->isModificationAllowed($consultation, isset($_GET['continuation']), isset($_GET['id']));
    }

    /**
     * Permet de sauvgarder les domaines d'activites.
     *
     * @param $consultation: objet consultation
     */
    public function saveDomaineActivites(&$consultation)
    {
        // Domaine d'activités LT-Ref
        if (Atexo_Module::isEnabled('ConsultationDomainesActivitesLtReferentiel')) {
            $domainesActivites = $this->idAtexoRefDomaineActivites->codesRefSec->value;
            if ('' != $domainesActivites) {
                $consultation->setDomainesActivites($domainesActivites);
            } else {
                $consultation->setCodesNuts(null);
            }
        }
    }

    /**
     * Permet de charger les domaines d'actives.
     *
     * @param $consultation: objet consultation
     */
    public function chargerDomainesActivites($consultation)
    {
        // Domaine d'activités LT-Ref
        if (Atexo_Module::isEnabled('ConsultationDomainesActivitesLtReferentiel')) {
            $atexoCodesDomainesActivites = new Atexo_Ref();
            $atexoCodesDomainesActivites->setCheminFichierConfigXML(Atexo_Config::getParameter('PATH_FILE_DOMAINES_ACTIVITES_CONFIG'));
            $atexoCodesDomainesActivites->setSecondaires($consultation->getDomainesActivites());
            $this->idAtexoRefDomaineActivites->afficherReferentiel($atexoCodesDomainesActivites);
        }
    }

    public function enabledDonneesRedac($sender, $param)
    {
        if ($this->donneeOrmeOui->checked) {
            $this->panelDonneesRedacEnabled->enabled = true;
        } else {
            $this->panelDonneesRedacEnabled->enabled = false;
        }
        $this->panelDonneesRedacEnabled->render($param->getNewWriter());
    }

    public function getCritereIdentiqueChecked()
    {
        return $this->donneeCriteresAttribution->isCritereIdentiqueChecked();
    }

    public function saveLtReferentiel($consultationId, $consultation = null)
    {
        $this->idReferentielZoneText->saveConsultationAndLot($consultationId, 0, $consultation);
        $this->idAtexoLtRefRadio->saveConsultationAndLot($consultation->getId(), 0, $consultation);
    }

    /*
     *  Permet de retourner la valeur courante de l'id de critere d'attribution
     *
     *  @return Integer id critere
     *  @author : loubna.ezziani@atexom.com
     *  @version : since 4.5.0
     */
    public function getCurrentIdCritereAttribution()
    {
        return $this->donneeCriteresAttribution->getCurrentIdCritereAttribution();
    }

    /**
     * Permet de sauvegarder l'historique Adresse de lieu ouverture.
     *
     * @param string $lieuOuverture l'ancien Lieu Ouverture Plis,CommonConsultation $consultation la consultation,Objet connexion $connexion,String $org l'acronyme de l'organisme, Boolean $modification
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function saveHistoriqueLieuOuverture($lieuOuverture, $consultation, $connexion, $org, $modification = true)
    {
        $arrayParameters = [];
        if (Atexo_Module::isEnabled('ConsultationLieuOuverturePlis') &&
            (trim($lieuOuverture) != trim($consultation->getLieuOuverturePlisTraduit()) || !$modification)
        ) {
            $valeur = '0';
            $detail2 = '';
            $detail1 = '';
            $lot = '0';
            $nomElement = Atexo_Config::getParameter('ID_LIEU_OUVERTURE_PLIS');

            if ($consultation->getLieuOuverturePlisTraduit()) {
                $valeur = '1';
                $detail1 = $consultation->getLieuOuverturePlisTraduit();
            }
            if ($modification) {
                $statut = Atexo_Config::getParameter('MODIFICATION_FILE');
            } else {
                $statut = Atexo_Config::getParameter('CREATION_FILE');
            }
            //Sauvegarde
            $arrayParameters[0] = $consultation->getId();
            $arrayParameters[1] = $nomElement;
            $arrayParameters[2] = $valeur;
            $arrayParameters[3] = $detail1;
            $arrayParameters[4] = $detail2;
            $arrayParameters[5] = $lot;
            $arrayParameters[6] = $org;
            $arrayParameters[7] = $statut;
            (new Atexo_Consultation())->saveHistoriqueConsultation($arrayParameters, $connexion);
        }
    }

    /**
     * Permet de sauvegarder l'historique Adresse de depot.
     *
     * @param string $adresseOld l'ancienne adresse du depot,CommonConsultation $consultation la consultation,Objet connexion $connexion,String $org l'acronyme de l'organisme, Boolean $modification
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function saveHistoriqueAdresseDepotOffres($adresseOld, $consultation, $connexion, $org, $modification = true)
    {
        $arrayParameters = [];
        if (Atexo_Module::isEnabled('ConsultationAdresseDepotOffres') &&
            (trim($adresseOld) != trim($consultation->getAdresseDepotOffresTraduit()) || !$modification)
        ) {
            $valeur = '0';
            $detail2 = '';
            $detail1 = '';
            $lot = '0';
            $nomElement = Atexo_Config::getParameter('ID_ADRESSE_DEPOT_OFFRES');

            if ($consultation->getAdresseDepotOffresTraduit()) {
                $valeur = '1';
                $detail1 = $consultation->getAdresseDepotOffresTraduit();
            }
            if ($modification) {
                $statut = Atexo_Config::getParameter('MODIFICATION_FILE');
            } else {
                $statut = Atexo_Config::getParameter('CREATION_FILE');
            }
            //Sauvegarde
            $arrayParameters[0] = $consultation->getId();
            $arrayParameters[1] = $nomElement;
            $arrayParameters[2] = $valeur;
            $arrayParameters[3] = $detail1;
            $arrayParameters[4] = $detail2;
            $arrayParameters[5] = $lot;
            $arrayParameters[6] = $org;
            $arrayParameters[7] = $statut;
            (new Atexo_Consultation())->saveHistoriqueConsultation($arrayParameters, $connexion);
        }
    }

    /**
     * Permet de sauvegarder l'historique Adresse de retrait du dossier.
     *
     * @param string $adresseOld l'ancienne adresse de retrait,CommonConsultation $consultation la consultation,Objet connexion $connexion,String $org l'acronyme de l'organisme, Boolean $modification
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function saveHistoriqueAdresseRetrait($adresseOld, $consultation, $connexion, $org, $modification = true)
    {
        $arrayParameters = [];
        if (Atexo_Module::isEnabled('ConsultationAdresseRetraisDossiers')
            && (trim($adresseOld) != trim($consultation->getAdresseRetraisDossiersTraduit()) || !$modification)
        ) {
            $nomElement = Atexo_Config::getParameter('ID_ADRESSE_RETRAIT_DOSSIERS');
            $valeur = '0';
            $detail2 = '';
            $detail1 = '';
            $lot = '0';
            if ($consultation->getAdresseRetraisDossiersTraduit()) {
                $valeur = '1';
                $detail1 = $consultation->getAdresseRetraisDossiersTraduit();
            }
            if ($modification) {
                $statut = Atexo_Config::getParameter('MODIFICATION_FILE');
            } else {
                $statut = Atexo_Config::getParameter('CREATION_FILE');
            }
            //Sauvegarde
            $arrayParameters[0] = $consultation->getId();
            $arrayParameters[1] = $nomElement;
            $arrayParameters[2] = $valeur;
            $arrayParameters[3] = $detail1;
            $arrayParameters[4] = $detail2;
            $arrayParameters[5] = $lot;
            $arrayParameters[6] = $org;
            $arrayParameters[7] = $statut;
            (new Atexo_Consultation())->saveHistoriqueConsultation($arrayParameters, $connexion);
        }
    }

    /**
     * Permet de charger les informations complémentaires de la consultation.
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function chargerInformationsComplementaires()
    {
        if (Atexo_Config::getParameter('AFFICHAGE_DONNEES_COMPLEMENTAIRES_PERSONNALISEE')) {
            //Addresse de retrait des dossiers / Addresse de depots des offres / Lieu d'ouverture de Plis
            if (Atexo_Module::isEnabled('ConsultationLieuOuverturePlis')) {
                $this->lieuOuverturePlis->Text = $this->getConsultation()->getLieuOuverturePlisTraduit();
            }
            if (Atexo_Module::isEnabled('ConsultationAdresseRetraisDossiers')) {
                $this->retraitDossiers->Text = $this->getConsultation()->getAdresseRetraisDossiersTraduit();
            }
            if (Atexo_Module::isEnabled('ConsultationAdresseDepotOffres')) {
                $this->addDepotDossiers->Text = $this->getConsultation()->getAdresseDepotOffresTraduit();
            }
            //Cocher la variante
            if (Atexo_Module::isEnabled('ConsultationVariantesAutorisees') && !$this->getConsultation()->getAlloti()) {
                if ('1' == $this->getConsultation()->getVariantes()) {
                    $this->variantesOui->Checked = true;
                } else {
                    $this->variantesNon->Checked = true;
                }
            }
        }
    }

    /**
     * Permet de recharger les donnees de la consultation selon l'allotissement.
     *
     * @param TCallbackEventParameter $param,Objet CommonConsultation  $consultation la consultation,Integer $alloti
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function refreshDonneesComplementaireConsultation($param, $consultation, $alloti)
    {
        $this->chargerReferentiels($consultation, $alloti);
        if ($alloti) {
            self::masquerChampsConsultation();
        } else {
            self::afficherChampsConsultation();
        }
        $this->panelLtrefText->render($param->getNewWriter());
        $this->panelLtrefRadio->render($param->getNewWriter());
    }

    /**
     * Permet de charger les Referentiels.
     *
     * @param Objet CommonConsultation $consultation la consultation,Integer $alloti
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function chargerReferentiels($consultation = null, $alloti = 0)
    {
        $consultationId = null;
        if ($consultation instanceof CommonConsultation) {
            $consultationId = $consultation->getId();
            if (!$consultationId) {
                $consultationId = $this->Page->getViewState('refConsParent');
            }
        }
        $this->idReferentielZoneText->setDependanceLot($alloti);
        $this->idAtexoLtRefRadio->setDependanceLot($alloti);
        // Referentiel zone texte
        $this->idReferentielZoneText->afficherTextConsultation($consultationId, 0, Atexo_CurrentUser::getCurrentOrganism(), null, true);
        // Referentiel de type radiobouton
        $this->idAtexoLtRefRadio->afficherReferentielRadio($consultationId, 0, Atexo_CurrentUser::getCurrentOrganism(), null, true);
    }

    /**
     * Permet de sauvegarder l'historique Variante.
     *
     * @param int $oldVariante l'ancienne variante,CommonConsultation $consultation la consultation,Objet connexion $connexion,String $org l'acronyme de l'organisme, Boolean $modification
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function saveHistoriqueVariantes($oldVariante, $consultation, $connexion, $org, $modification = true)
    {
        $arrayParameters = [];
        if (Atexo_Module::isEnabled('ConsultationVariantesAutorisees') && ($oldVariante != $consultation->getVariantes() || !$modification)) {
            $nomElement = Atexo_Config::getParameter('ID_HISTORIQUE_VARIANTES');
            $lot = '0';
            $valeur = '0';
            $detail1 = '';
            $detail2 = '';
            if ($consultation->getVariantes()) {
                $valeur = '1';
                $detail1 = $consultation->getVariantes();
            }
            if ($modification) {
                $statut = Atexo_Config::getParameter('MODIFICATION_FILE');
            } else {
                $statut = Atexo_Config::getParameter('CREATION_FILE');
            }
            //Sauvegarde
            $arrayParameters[0] = $consultation->getId();
            $arrayParameters[1] = $nomElement;
            $arrayParameters[2] = $valeur;
            $arrayParameters[3] = $detail1;
            $arrayParameters[4] = $detail2;
            $arrayParameters[5] = $lot;
            $arrayParameters[6] = $org;
            $arrayParameters[7] = $statut;
            (new Atexo_Consultation())->saveHistoriqueConsultation($arrayParameters, $connexion);
        }
    }

    /**
     * Permet de masquer des champs de la consultation.
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function masquerChampsConsultation()
    {
        if (Atexo_Module::isEnabled('ConsultationEchantillonsDemandes')) {
            $this->echantillonsDemandes->masquerComposant();
        }
        if (Atexo_Module::isEnabled('ConsultationReunion')) {
            $this->reunions->masquerComposant();
        }
        if (Atexo_Module::isEnabled('ConsultationVisiteDesLieux')) {
            $this->visitesLieux->masquerComposant();
        }
        if (Atexo_Module::isEnabled('ConsultationQualification')) {
            $this->panelQualification->setDisplay('None');
        }
        if (Atexo_Module::isEnabled('ConsultationAgrement')) {
            $this->panelAgrement->setDisplay('None');
        }
        if (Atexo_Module::isEnabled('ConsultationCautionProvisoire')) {
            $this->panelCautionProvisoire->setDisplay('None');
        }
    }

    /**
     * Permet d'afficher des champs de la consultation.
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function afficherChampsConsultation()
    {
        if (Atexo_Module::isEnabled('ConsultationEchantillonsDemandes')) {
            $this->echantillonsDemandes->afficherComposant();
        }
        if (Atexo_Module::isEnabled('ConsultationReunion')) {
            $this->reunions->afficherComposant();
        }
        if (Atexo_Module::isEnabled('ConsultationVisiteDesLieux')) {
            $this->visitesLieux->afficherComposant();
        }
        if (Atexo_Module::isEnabled('ConsultationQualification')) {
            $this->panelQualification->setDisplay('Dynamic');
        }
        if (Atexo_Module::isEnabled('ConsultationAgrement')) {
            $this->panelAgrement->setDisplay('Dynamic');
        }
        if (Atexo_Module::isEnabled('ConsultationCautionProvisoire')) {
            $this->panelCautionProvisoire->setDisplay('Dynamic');
        }
    }

    /**
     * Permet d'initialiser les donnees du compte de publicite.
     *
     * @param CommonConsultation $consultation : objet consultation
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-les-echos
     *
     * @copyright Atexo 2016
     */
    public function initialiserInfosPublicite($consultation)
    {
        $dataSource = [];
        // Debut remplir liste des favoris
        $listFavoris = [];
        $idService = Atexo_CurrentUser::getCurrentServiceId();
        $organisme = Atexo_CurrentUser::getOrganismAcronym();
        $listFavoris = (new Atexo_Publicite_Favoris())->retreivePublicieFavorisByService($idService, $organisme);
        if (is_array($listFavoris) && !empty($listFavoris)) {
            $dataSource[0] = Prado::localize('SELECTIONNEZ_UN_FAVORI');
            foreach ($listFavoris as $oneFavoris) {
                $dataSource[$oneFavoris->getId()] = $oneFavoris->getLibelle();
            }
            //$this->setCompteBoampSelectionne($comptesPubs[array_shift(array_keys($comptesPubs))]);
            $this->favorisPublicite->setDataSource($dataSource);
            $this->favorisPublicite->dataBind();
            $this->nomFavorisPublicite->Visible = false;
            $this->favorisPublicite->Visible = true;
        } else {
            $this->nomFavorisPublicite->Visible = true;
            $this->favorisPublicite->Visible = false;
        }
        // Fin remplir liste des favoris

        $tConsultationComptePubQuery = new CommonTConsultationComptePubQuery();
        $consComptePub = $tConsultationComptePubQuery->recupererComptePubByIdDonneesCompAndOrg($consultation->getIdDonneeComplementaire(), $consultation->getOrganisme());
        /*if (!($consComptePub instanceof CommonTConsultationComptePub)) {
            $consComptePub = new CommonTConsultationComptePub();
            Atexo_Publicite_Favoris::initConsultationComptePubParFavorisPublicite($consComptePub, $listFavoris[0]);
        }*/
        /*$consComptePub = new CommonTConsultationComptePub();
        Atexo_Publicite_Favoris::initConsultationComptePubParFavorisPublicite($consComptePub, $listFavoris[0]);*/
        if(!Atexo_Config::getParameter('SPECIFIQUE_PLACE')) {
            $this->donneesComplementairesComptePublicite->setConsultationComptePublicite($consComptePub);
            $this->donneesComplementairesComptePublicite->setRefConsultation(($consultation instanceof CommonConsultation) ? $consultation->getId() : '');
            $this->donneesComplementairesComptePublicite->setOrganisme(($consultation instanceof CommonConsultation) ? $consultation->getOrganisme() : '');
            $this->donneesComplementairesComptePublicite->initialiserComposant($consultation);
        }
    }

    /**
     * Permet d'initialiser les donnees complementaires : Regime Financier.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-les-echos
     *
     * @copyright Atexo 2016
     */
    public function initialiserDonneesComplementairesRegimeFinancier($recharger)
    {
        $this->regimeFinancier->setDonneeComplementaire($this->donneeComplementaire);
        if ($recharger) {
            $this->regimeFinancier->initialiserComposant();
        }
    }

    public function preRemplirInfosDepuisFavoris($sender, $param)
    {
        $donnesComplementaitre = null;
        $consComptePub = null;
        $idFavoris = $this->favorisPublicite->selectedValue;
        $favorisPub = CommonTPubliciteFavorisPeer::retrieveByPK($idFavoris, $this->_connexion);
        if ($this->getConsultation() instanceof CommonConsultation) {
            if ($this->getConsultation()->getIdDonneeComplementaire()) {
                $donnesComplementaitre = CommonTDonneeComplementairePeer::retrieveByPK($this->getConsultation()->getIdDonneeComplementaire(), $this->_connexion);
            }
            if ($donnesComplementaitre instanceof CommonTDonneeComplementaire) {
                $this->donneeComplementaire = $donnesComplementaitre;
            } else {
                $this->donneeComplementaire = new CommonTDonneeComplementaire();
            }
            $tConsultationComptePubQuery = new CommonTConsultationComptePubQuery();
            $consComptePub = $tConsultationComptePubQuery->recupererComptePubByIdDonneesCompAndOrg($this->getDonneeComplementaire()->getIdDonneeComplementaire(), Atexo_CurrentUser::getOrganismAcronym());
        }
        if (!($consComptePub instanceof CommonTConsultationComptePub)) {
            $consComptePub = new CommonTConsultationComptePub();
        }
        (new Atexo_Publicite_Favoris())->initConsultationComptePubParFavorisPublicite($consComptePub, $favorisPub);
        if (!Atexo_Config::getParameter('SPECIFIQUE_PLACE')) {
            $this->donneesComplementairesComptePublicite->setConsultationComptePublicite($consComptePub);
            $this->donneesComplementairesComptePublicite->preRemplirInfosComptePublicite($sender, $param);
        }
        if ($favorisPub instanceof CommonTPubliciteFavoris && Atexo_Module::isEnabled('DonneesRedac')) {
            $this->regimeFinancier->chargerElements($favorisPub->getRegimeFinancierCautionnement(), $favorisPub->getRegimeFinancierModalitesFinancement());
        }
    }

    public function checkIfPubActiveNiveauConsultation(): bool
    {
        return (bool) $this->getConsultation()->getDonneePubliciteObligatoire();
    }

    private function desactiverBlocsAllotiePourAjax()
    {
        if ($this->Page->isPostBack && '1' === $this->getConsultation()->getAlloti()) {
            $this->echantillonsDemandes->setAfficher(false);
            $this->reunions->setAfficher(false);
            $this->visitesLieux->setAfficher(false);
            $this->panelQualification->setDisplay('None');
            $this->qualification->setAfficher(false);
            $this->panelAgrement->setDisplay('None');
            $this->agrements->setAfficher(false);
            if (Atexo_Module::isEnabled('DonneesComplementaires')) {
                if (Atexo_Module::isEnabled('DonneesRedac')) {
                    $this->donneeVariante->setAfficher(false);
                    $this->donneeFormePrix->setAfficher(false);
                    $this->donneeDureeMarche->setAfficherMarcheReconductible(false);
                }
                $this->donneeMontantMarche->setAfficher(false);
            }
        }
    }
}
