<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Organismes;
use Prado\Prado;

/**
 * Template de choix d'une entié d'achat.
 */
class ChoixEntiteAchat extends MpeTPage
{
    public $_parentPage;
    public $_service;
    
    public function getParentPage()
    {
        return $this->_parentPage;
    }

    public function setParentPage($value)
    {
        $this->_parentPage = $value;
    }

    public function setVisible($visibility)
    {
        $this->recapConsultation->visible = $visibility;
    }

    public function setCheckedMonEntity()
    {
        $this->_service = Atexo_CurrentUser::getCurrentServiceId();
        $this->js->Text = "<script>document.getElementById('ctl0_CONTENU_PAGE_ChoixEntiteAchat_buttonOk').click()</script>";
    }

    public function getCheckedEntity()
    {
        return $this->_service;
    }

    public function onLoad($param)
    {
        // Service de l'agent
        $this->labelEntite->Text = Atexo_EntityPurchase::getPathEntityById(
            Atexo_CurrentUser::getCurrentServiceId(),
            Atexo_CurrentUser::getOrganismAcronym()
        );
        $this->autreEntite->Text = sprintf(
            "%s %s",
            Prado::localize('DEFINE_AUTRE_ENTITE_ACHAT'),
            Prado::localize('TEXT_2_POINTS')
        );

        // Si le module gestion_fournisseurs_docs_pole est activé on gére les fournisseurs par entité d'achat sinon ceci est fait par organisme
        $this->_service = Atexo_CurrentUser::getCurrentServiceId();

        // remplissage de la liste des sous-service
        if (!$this->Page->isPostBack) {
            // Si l'agent est affecté à un service onb affiche tout les services en doussous de son pôle
            if (Atexo_CurrentUser::getCurrentServiceId()) {
                $listeChilds = Atexo_EntityPurchase::getSubServices(
                    Atexo_CurrentUser::getCurrentServiceId(),
                    Atexo_CurrentUser::getOrganismAcronym(),
                    false,
                    true
                );
            } else {
                // Si l'agent n'est pas affecté à un service on affiche tout les services
                $listeChilds = Atexo_EntityPurchase::getEntityPurchase(
                    Atexo_CurrentUser::getOrganismAcronym(),
                    true,
                    null,
                    true
                );
            }
            $this->listeAutreEntites->DataSource = $listeChilds;
            $this->listeAutreEntites->DataBind();
            if (!is_array($listeChilds) || !count($listeChilds)) {
                $this->autreEntite->visible = false;
                $this->labelAutreEntite->Text = '';
                $this->listeAutreEntites->visible = false;
                $this->buttonOk->visible = false;
            }
        }
    }

    /**
     * retourne le nom du service de l'agent.
     *
     * @return string nom du service de l'agent
     */
    public function getServiceAgent()
    {
        return Atexo_EntityPurchase::getServiceAgentById(Atexo_CurrentUser::getIdServiceAgentConnected(), Atexo_CurrentUser::getOrganismAcronym());
    }

    public function getSelectedEntityPurchase()
    {
        if ($this->monEntite->checked) {
            return Atexo_CurrentUser::getCurrentServiceId();
        } elseif ($this->autreEntite->checked) {
            return empty($this->listeAutreEntites->SelectedValue) ? null : $this->listeAutreEntites->SelectedValue;
        }
    }
}
