<?php

namespace Application\Controls;

use Application\Service\Atexo\Controller\Atexo_Controller_Front;
use Prado\Prado;
use Prado\Web\UI\WebControls\TImage;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PictoEtatMarche extends TImage
{
    private $_valide;

    public function onPreRender($param)
    {
        if ('true' == $this->_valide) {
            $this->setImageUrl(Atexo_Controller_Front::t().'/images/picto-check-ok-small.gif');
            $this->setAlternateText(Prado::localize('DEFINE_VALIDE'));
            $this->setToolTip(Prado::localize('DEFINE_VALIDE'));
        } else {
            $this->setImageUrl(Atexo_Controller_Front::t().'/images/picto-time-grey.gif');
            $this->setAlternateText(Prado::localize('EN_COURS'));
            $this->setToolTip(Prado::localize('EN_COURS'));
        }
    }

    public function setValide($bool)
    {
        $this->_valide = $bool;
    }

    public function getValide()
    {
        return $this->_valide;
    }
}
