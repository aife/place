<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Category;

/**
 * Composant pour les domaines d'activites.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class AtexoDomainesActivites extends MpeTPage
{
    private bool $_postBack = false;
    private bool $_callback = false;
    private bool $_allLevel = false;

    public function setPostBack($value)
    {
        $this->_postBack = $value;
    }

    public function getPostBack()
    {
        return $this->_postBack;
    }

    public function setCallBack($value)
    {
        $this->_callback = $value;
    }

    public function getCallBack()
    {
        return $this->_callback;
    }

    public function setAllLevel($value)
    {
        $this->_allLevel = $value;
    }

    public function getAllLevel()
    {
        return $this->_allLevel;
    }

    public function onLoad($param)
    {
        if (!$this->getPostBack()) {
            $this->displayDomainesActivites();
        }
    }

    /**
     * affiche les libellés des domaines retournés de la popup.
     */
    public function displayDomainesActivites()
    {
        $idsDomaines = $this->idsDomaines->Text;
        if ('' == $idsDomaines) {
            $this->libellesDomaines->Text = '';
            $this->trunLibelleDomaines->Text = '';
        }
        $libelleDomaine = '';
        $libelleDomaine = Atexo_Consultation_Category::displayListeLibelleDomaineByArrayId($idsDomaines);
        if (strlen($libelleDomaine) > 60) {
            $this->toisPointsInfoBulle->setDisplay('Dynamic');
        }
        if ($libelleDomaine) {
            $this->trunLibelleDomaines->Text = Atexo_Util::truncateTexte(wordwrap(($libelleDomaine), 40, '<br/>', true), 100);
            $this->libellesDomaines->Text = (($libelleDomaine));
            $this->panelLibelleDomaine->setDisplay('Dynamic');
        } else {
            $this->trunLibelleDomaines->Text = '';
            $this->libellesDomaines->Text = '';
            $this->panelLibelleDomaine->setDisplay('None');
        }
    }

    /**
     * rafrechir le panel des domaines d'activités.
     */
    public function onCallBackDomaineActivites($sender, $param)
    {
        $this->panelDomaineActivite->render($param->NewWriter);
    }
}
