
<com:TRepeater ID="repeaterLot">
	<prop:ItemTemplate>
								<!--Debut Ligne -->
								<div class="line">
									<com:TPanel cssClass="intitule-auto indent-20"><strong><com:TTranslate>DEFINE_LOT_NUMERO</com:TTranslate> <com:TLabel ID="numLot" Text="<%#$this->Data['lot']%>" />:</strong></com:TPanel>
								</div>
								<!--Fin Ligne -->
								<com:TTextBox id="idReferentielZoneText" TextMode="<%#$this->Data['mode']%>" CssClass="long" /></br></br>
								<com:TLabel id="iDLibelle" visible="false" text="<%#$this->Data['id']%>" />
					         	<com:TLabel id="codeLibelle" visible="false" text="<%#$this->Data['codeLibelle']%>" />
					         	<com:TLabel id="labelReferentielZoneText"  text="" visible="<%# $this->parent->parent->getLabel()%>"/>
								<com:TRequiredFieldValidator 
								Id="validatorAtexoText2"
								ControlToValidate="idReferentielZoneText"
								ValidationGroup="<%#($this->parent->parent->getValidationGroup())%>" 
								Display="Dynamic"
								EnableClientScript="true"
								ErrorMessage="<%#Prado::localize($this->Data['codeLibelle'])%>"
								visible="<%#($this->Data['obligatoire']&& $this->parent->parent->getObligatoire()) ? 'true' :'false'%>"
								Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
									<prop:ClientSide.OnValidationError>
										blockErreur = '<%#($this->parent->parent->getValidationSummary())%>';
										if(document.getElementById(blockErreur))
								 			document.getElementById(blockErreur).style.display='';
								 		if(document.getElementById('ctl0_CONTENU_PAGE_buttonSave'))
								 			document.getElementById('ctl0_CONTENU_PAGE_buttonSave').style.display='';
									</prop:ClientSide.OnValidationError>   
								</com:TRequiredFieldValidator>
								<com:THiddenField id="oldValue" />
	</prop:ItemTemplate>
</com:TRepeater>
	