<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Controller\Atexo_Controller_Front;
use Prado\Prado;
use Prado\Web\UI\WebControls\TImage;

/**
 * commentaires.
 *
 * @author thibaut Decaudain <thibaut.decaudain@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PictoOrigine extends TImage
{
    public $_type;

    public function onPreRender($param)
    {
        if ($this->getType() == Atexo_Config::getParameter('ORIGINE_MARCHE_PLATEFORME')) {
            $this->setImageUrl(Atexo_Controller_Front::t().'/images/picto-ajout-plateforme.gif');
            $this->setAlternateText(Prado::localize('ICONE_PLATEFORME'));
            $this->setToolTip(Prado::localize('AJOUT_PLATEFORME'));
        } elseif ($this->getType() == Atexo_Config::getParameter('ORIGINE_MARCHE_MANUEL')) {
            $this->setImageUrl(Atexo_Controller_Front::t().'/images/picto-ajout-manuel.gif');
            $this->setAlternateText(Prado::localize('ICONE_MANUEL'));
            $this->setToolTip(Prado::localize('AJOUT_MANUEL'));
        }
    }

    public function setType($type)
    {
        $this->_type = $type;
    }

    public function getType()
    {
        return $this->_type;
    }
}
