<?php

namespace Application\Controls;

use App\Service\ConsultationTags;
use App\Service\ContratService;
use App\Service\PlateformeVirtuelle\Context;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonProcedureEquivalence;
use Application\Propel\Mpe\CommonTContratMulti;
use Application\Propel\Mpe\CommonTContratTitulaire;
use Application\Propel\Mpe\CommonTDonneeComplementaire;
use Application\Propel\Mpe\CommonTDonneeComplementairePeer;
use Application\Propel\Mpe\CommonTDumeContexte;
use Application\Propel\Mpe\CommonTypeProcedureOrganisme;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_DonnesComplementaires;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_LieuxExecution;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Ref;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Category;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Contrat;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Lots;
use Application\Service\Atexo\Consultation\Atexo_Consultation_NumerotationRefCons;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureEquivalence;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureType;
use Application\Service\Atexo\Consultation\Atexo_Consultation_TypeAvis;
use Application\Service\Atexo\Dume\Atexo_Dume_AtexoDume;
use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2;
use Application\Service\Atexo\Interfaces\Atexo_Interfaces_Consultation;
use Application\Service\Atexo\Interfaces\Atexo_Interfaces_InterfacesUtilities;
use Application\Service\Atexo\MessageStatusCheckerUtil;
use Application\Service\Atexo\Operations\Atexo_Operations_Operations;
use Exception;
use Prado\Prado;

/**
 * Page de gestion du bloc identification de la consultation.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 4
 */
class IdentificationConsultation extends MpeTTemplateControl
{
    public ?string $langue = null;
    public $organisme;
    public $_consultation;
    public $_mode;
    private $_donneesComplementaires;

    public const UNCHECKED_VALUE = '0';

    public function onInit($param)
    {
        MessageStatusCheckerUtil::initEntryPoints('consultation');
    }

    /**
     * recupère la consultation.
     */
    public function getConsultation()
    {
        if (!($this->_consultation instanceof CommonConsultation)) {
            $this->consultation = $this->Page->getViewState('consultation');
        }

        return $this->_consultation;
    }

    // getConsultation()

    /**
     * Affecte l'objet de la consultation.
     *
     * @param objet $consultation
     */
    public function setConsultation($consultation)
    {
        $this->_consultation = $consultation;
    }

    // setConsultation()

    /**
     * recupère la langue.
     */
    public function getLangue()
    {
        return $this->langue;
    }

    // getLangue()

    /**
     * Affecte la langue.
     *
     * @param string $langue
     */
    public function setLangue($langue)
    {
        if ($this->langue !== $langue) {
            $this->langue = $langue;
        }
    }

    // setLangue()

    /**
     * recupère l'organisme.
     */
    public function getOrganisme()
    {
        return $this->organisme;
    }

    // getOrganisme()

    /**
     * Affecte la valeur de l'organisme.
     *
     * @param string $organisme
     */
    public function setOrganisme($organisme)
    {
        if ($this->organisme !== $organisme) {
            $this->organisme = $organisme;
        }
    }

    // setOrganisme()

    /**
     * recupère le mode.
     */
    public function getMode()
    {
        if (!$this->mode) {
            $this->mode = $this->Page->getModeConsultation();
        }

        return $this->mode;
    }

    // getMode()

    /**
     * Affecte le mode.
     *
     * @param string $mode
     */
    public function setMode($mode)
    {
        if ($this->mode !== $mode) {
            $this->mode = $mode;
        }
    }

    // setMode()

    /**
     * Recupere la valeur de [_donneesComplementaires].
     *
     * @return mixed
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function getDonneesComplementaires()
    {
        return $this->_donneesComplementaires;
    }

    /**
     * Modifie la valeur de [_donneesComplementaires].
     *
     * @param mixed $donneesComplementaires : objet donnees complementaires
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function setDonneesComplementaires($donneesComplementaires)
    {
        $this->_donneesComplementaires = $donneesComplementaires;
    }

    /**
     * Chargement du composant.
     */
    public function onLoad($param)
    {
        $this->setLangue(Atexo_Languages::readLanguageFromSession());
        $this->organisme = Atexo_CurrentUser::getCurrentOrganism();
        if (!$this->Page->isPostBack) {
            $this->loadLists();
        }
        //Gestion affichage achat responsable
        if (!$this->Page->isPostBack) {
            $this->gererAffichageAchatResponsable();
        }
        $consultation = $this->Page->getConsultation();
        if (Atexo_Module::isEnabled('NumeroProjetAchat')) {
            $this->numProjetAchat->Text = $consultation->getCodeExterne();
        }
        if ($consultation instanceof CommonConsultation && null != $consultation->getReference() && (Atexo_Module::isEnabled('NumerotationRefCons'))) {
            $this->reference->Text = $consultation->getReferenceUtilisateur();
        }

        $this->chargerCodeCpv($consultation);

        $this->traductionSpecificText();
    }

    /**
     * Permet de retourner l'url du lieu d'exécution.
     */
    public function getLieuExecutionUrl()
    {
        $param = '';
        if (Atexo_Module::isEnabled('LieuxExecutionCarte')) {
            $param = $this->numSelectedGeoN2->Value;
        }

        $selectedLoc = trim($this->idsSelectedGeoN2->Value, ',');

        return Atexo_LieuxExecution::getLieuExecutionUrl($param, $selectedLoc);
    }

    /**
     * Permet de remplir les listes de selection au chargement de la page:
     * types d'avis, types de procédures, categories.
     */
    public function loadLists()
    {
        //Debut remplissage de la liste Types avis
        $this->chargerTypeAvis();
        //Fin remplissage de la liste Types avis

        //Debut remplissage de la liste Types procedure
        $this->chargerTypeProcedure();
        //Fin remplissage de la liste Types procedure

        //Debut remplissage de la liste Categorie
        $this->chargerCategories();
        //Fin remplissage de la liste Categorie

        // Début chargement de type contrat
        $this->chargerTypeContrat();
        // Fin chargement de type contrat

        //Début affichage des referentiels
        $this->chargerReferentiels();
        //Fin affichage des referentiels

        //Afficher et masquer des elements
        $this->customizeForm();

        //Afficher/masquer les données complémentaires
        if ('1' == $this->getConsultation()->getAlloti()) {
            $this->masquerAutresDonneesComplementairesConsultationLots();
        } else {
            $this->afficherAutresDonneesComplementairesConsultationLots();
        }

        //Chargement du formulaire d'identification
        $this->chargerFormulaireIdentification();
        $this->remplirFormulaireIdentification();

        //Charger préférences lieux d'éxecution
        $this->chargerPreferenceLieuxExecution();

        //Charger url lieux d'exécution
        $this->chargerUrlLieuxExecution();

        //Charger préférences codes nuts
        $this->chargerPreferenceCodesNuts();

        //Initialisation d'élements
        $this->initialisationElements();

        // Mode de passation
        $this->afficherModePassation();

        //Affichage des données complémentaires
        $this->chargerInformationsComplementaires();

        //Domaines d'activites
        $this->chargerDomainesActivites($this->getConsultation());

        //charger referentiel CPV
        $this->referentielCPV->onInitComposant();
        $this->referentielCPV->chargerReferentielCpv($this->getConsultation()->getCodeCpv1(), $this->getConsultation()->getCodeCpv2());

        $this->gererAffichageBlocSelectionPubEtRedactionPieces();
    }

    /**
     * Permet d'afficher et masquer des élèments.
     */
    public function customizeForm()
    {
        //Type de contrat
        if (Atexo_Module::isEnabled('TypeContrat', $this->getOrganisme())) {
            $this->panelTypeContrat->visible = true;
        } else {
            $this->panelTypeContrat->visible = false;
        }
        $this->panelNumAC->setDisplay('None');
        //Entité adjudicatrice
        if (Atexo_Module::isEnabled('EntiteAdjudicatrice', $this->getOrganisme())) {
            $this->panelEntiteAdjudicatrice->visible = true;
        } else {
            $this->panelEntiteAdjudicatrice->visible = false;
        }
        //Code opération
        if (Atexo_Module::isEnabled('gestionOperations', $this->getOrganisme())) {
            $this->panelCodeOperation->visible = true;
        } else {
            $this->panelCodeOperation->visible = false;
        }
        if ((Atexo_Module::isEnabled('NumerotationRefCons'))) {
            //Reference Utilisateur par défault
            if ($this->getMode() == Atexo_Config::getParameter('MODE_CREATION_CONSULTATION')) {
                $this->reference->Text = prado::localize('TEXT_GENEREE_ENREGISTREMENT');
            }
            if (!isset($_GET['continuation']) || (isset($_GET['continuation']) && '1' == Atexo_Config::getParameter('NUMEROTATION_AUTOMATIQUE_SUITE'))) {
                $this->reference->setEnabled(false);
                $this->formatReference->setEnabled(false);
                $this->formatReferenceSave->setEnabled(false);
            }
        }
        if (!Atexo_Config::getParameter('AFFICHAGE_DONNEES_COMPLEMENTAIRES_PERSONNALISEE')) {
            // Adresse de retrait
            if (Atexo_Module::isEnabled('ConsultationAdresseRetraisDossiers')) {
                $this->panelAdresseRetraitDossiers->setDisplay('Dynamic');
                $this->validatorRetraitDossiers->setEnabled(true);
            } else {
                $this->panelAdresseRetraitDossiers->setDisplay('None');
                $this->validatorRetraitDossiers->setEnabled(false);
            }
            //Adresse de depot d'offres
            if (Atexo_Module::isEnabled('ConsultationAdresseDepotOffres')) {
                $this->panelAdresseDepot->setDisplay('Dynamic');
                $this->validatorAddDepotDossiers->setEnabled(true);
            } else {
                $this->panelAdresseDepot->setDisplay('None');
                $this->validatorAddDepotDossiers->setEnabled(false);
            }
            //Lieu ouverture de Pli
            if (Atexo_Module::isEnabled('ConsultationLieuOuverturePlis')) {
                $this->panelOuverturePlis->setDisplay('Dynamic');
                if (Atexo_Module::isEnabled('LieuOuverturePlisObligatoire')) {
                    $this->validatorLieuOuverturePlis->setEnabled(true);
                } else {
                    $this->validatorLieuOuverturePlis->setEnabled(false);
                }
            } else {
                $this->panelOuverturePlis->setDisplay('None');
                $this->validatorLieuOuverturePlis->setEnabled(false);
            }
            // Variantes
            if (Atexo_Module::isEnabled('DonneesComplementaires') && Atexo_Module::isEnabled('ConsultationVariantesAutorisees')) {
                $this->panelVariantesAutorisees->Visible = true;
            } else {
                $this->panelVariantesAutorisees->Visible = false;
            }
        }
        //Domaines Activites
        if (Atexo_Module::isEnabled('ConsultationDomainesActivites')) {
            $this->entrDomaineActivite->Visible = true;
            $this->domaineActivite->validatorDomainesActivites->setValidationGroup('validateInfosConsultation');
            $this->domaineActivite->validatorDomainesActivites->setEnabled(true);
            $this->domaineActivite->setAllLevel(true);
        } else {
            $this->entrDomaineActivite->Visible = false;
        }
    }

    /**
     * Permet de charger les types de procedures.
     */
    public function chargerTypeProcedure()
    {
        if ($this->getMode() == Atexo_Config::getParameter('MODE_MODIFICATION_CONSULTATION')) {
            $dataProcedures = [];
            $dataProcedures[0] = '---' . Prado::localize('DEFAULT_PROCEDURE_TYPE_SELECTED') . '---';
            $proceduresTypes = Atexo_Consultation_ProcedureType::retrieveProcedureType();
            foreach ($proceduresTypes as $procedureType) {
                $dataProcedures[$procedureType->getIdTypeProcedure()] = Atexo_Util::atexoHtmlEntitiesDecode($procedureType->getLibelleTypeProcedure());
            }
            $this->procedureType->Datasource = $dataProcedures;
            $this->procedureType->dataBind();
        } else {
            $consultation = $this->getConsultation();
            if ($consultation instanceof CommonConsultation) {
                self::chargerProcedureByTypeContrat($consultation->getTypeMarche());
            }
        }
    }

    /**
     * Permet de charger les catégories.
     */
    public function chargerCategories()
    {
        $categories = Atexo_Consultation_Category::retrieveCategories();
        $dataCategories = Atexo_Consultation_Category::retrieveDataCategories($categories, $this->langue);
        $this->categorie->Datasource = $dataCategories;
        $this->categorie->dataBind();
    }

    /**
     * Permet de charger les types avis.
     */
    public function chargerTypeAvis()
    {
        $dataAvis = Atexo_Consultation_TypeAvis::retrieveAvisTypes(false, $this->langue);
        $this->intituleAvis->Datasource = $dataAvis;
        $this->intituleAvis->dataBind();
        $this->intituleAvis->setSelectedValue(Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION'));
    }

    /**
     * Permet de charger les types de contrats.
     */
    public function chargerTypeContrat()
    {
        //$listeContrats = Atexo_ValeursReferentielles::getTypeContratByReferentiel(Atexo_Config::getParameter('REFERENTIEL_TYPE_CONTRAT'), $this->organisme);
        $listeContrats = (new Atexo_Consultation_Contrat())->getListeTypeContrat();
        $this->contratType->Datasource = $listeContrats;
        $this->contratType->dataBind();
    }

    /**
     * Permet de charger les Referentiels.
     */
    public function chargerReferentiels($consultation = null, $alloti = 0)
    {
        $consultationId = null;
        if ($consultation instanceof CommonConsultation) {
            $consultationId = $consultation->getId();
            if (!$consultationId) {
                $consultationId = $this->Page->getViewState('refConsParent');
            }
        }
        $this->idReferentielZoneText->setDependanceLot($alloti);
        $this->idAtexoLtRefRadio->setDependanceLot($alloti);
        // Lt Referentiel
        $this->ltrefCons->afficherReferentielConsultation($consultationId, 0, Atexo_CurrentUser::getCurrentOrganism());
        // Referentiel zone texte
        $this->idReferentielZoneText->afficherTextConsultation($consultationId, 0, Atexo_CurrentUser::getCurrentOrganism(), null, true);
        // Referentiel de type radiobouton
        $this->idAtexoLtRefRadio->afficherReferentielRadio($consultationId, 0, Atexo_CurrentUser::getCurrentOrganism(), null, true);
    }

    /**
     * Permet de sauvegarder les informations
     * de la consultation contenues dans l'onglet "identification".
     */
    public function save(&$consultation, $connexion, $modification = false)
    {
        $dataDonneesCompl = [];
        // On ne renseigne l'ID de PFV qu'à la création de la consultation.
        if (!$modification) {
            /** @var Context $pfvContextService */
            $pfvContextService = Atexo_Util::getSfService(Context::class);
            $pfvId = $pfvContextService->getCurrentPlateformeVirtuelleId();
            $consultation->setPlateformeVirtuelleId($pfvId);
        }

        $consultation->setDateDebut(date('Y-m-d h:i:s'));
        //Référence utilisateur
        if ($this->modeCreationConsultationAvecNumerotationAutomatique() || $this->modeCreationSuiteAvecNumerotationAutomatique()) {
            if (!$modification) {
                $UserRef = Atexo_Consultation_NumerotationRefCons::generateUserRef($connexion);
                $consultation->setReferenceUtilisateur($UserRef);
            }
        } else {
            $consultation->setReferenceUtilisateur((new Atexo_Config())->toPfEncoding($this->reference->Text));
        }
        //Référence connecteur
        if ('' != $this->referenceConnecteur->Value) {
            $consultation->setReferenceConnecteur($this->referenceConnecteur->Value);
        }
        //Type d'annonces
        $consultation->setIdTypeAvis($this->intituleAvis->getSelectedValue());
        //Type de contrat
        if (Atexo_Module::isEnabled('TypeContrat', $this->organisme) && '0' != $this->contratType->getSelectedValue()) {
            $consultation->setTypeMarche($this->contratType->getSelectedValue());
        }

        $consultation->setNumeroAC($this->numeroACValue->Value);
        $consultation->setIdContrat($this->idContratSelected->Value);

        //Consultation passée en tant qu'Entité adjudicatrice
        if (Atexo_Module::isEnabled('EntiteAdjudicatrice', $this->organisme)) {
            if ($this->entiteAdjudicatrice->Checked) {
                $entiteAdjudicatrice = '1';
            } else {
                $entiteAdjudicatrice = '0';
            }
            $consultation->setEntiteAdjudicatrice($entiteAdjudicatrice);
        }

        //Consultation passée en tant marche public simplifie
        if (Atexo_Module::isEnabled('MarchePublicSimplifie', $this->organisme)) {
            $marcheMPS = '0';
            if ($this->marcheMPS->Checked) {
                $marcheMPS = '1';
            }
            $consultation->setMarchePublicSimplifie($marcheMPS);
        }

        //Type de procedure
        $proceduresTypes = Atexo_Consultation_ProcedureType::retrieveProcedureType();
        $idTypeProcedurePortail = Atexo_Consultation_ProcedureType::getIdTypeProcedurePortail($this->procedureType->getSelectedValue(), $this->organisme);
        if ($idTypeProcedurePortail) {
            $consultation->setIdTypeProcedure($idTypeProcedurePortail);
        } else {
            $consultation->setIdTypeProcedure($this->procedureType->getSelectedValue());
        }
        $consultation->setIdTypeProcedureOrg($this->procedureType->getSelectedValue());

        // copie de la valeur DepouillablePhaseConsultation dans l'objet consulatation
        $typeProcedure = $proceduresTypes[$this->procedureType->getSelectedValue()];
        if ($typeProcedure instanceof CommonTypeProcedureOrganisme) {
            $consultation->setDepouillablePhaseConsultation($typeProcedure->getDepouillablePhaseConsultation());
            $consultation->setConsultationTransverse($typeProcedure->getConsultationTransverse());
        }

        //Récuperation et enregistrement des valeurs parametrées dans "ProcedureEquivalence"
        if ($this->getMode() == Atexo_Config::getParameter('MODE_CREATION_CONSULTATION') && !$consultation->getId()) {
            (new Atexo_Consultation())->loadConsultationWithParametersProceduresEquivalence($consultation, $this->procedureType->getSelectedValue());
        }

        //Code opération
        if (Atexo_Module::isEnabled('gestionOperations', $this->organisme) && $consultation->getCodeOperation() != $this->codeOperation->Text) {
            if (($this->idCode->value != $consultation->getIdOperation()) && ('' != $this->idCode->value)) {
                $consultation->setIdOperation($this->idCode->value);
            } else {
                $consultation->setIdOperation(null);
            }
            $consultation->setCodeOperation((new Atexo_Config())->toPfEncoding($this->codeOperation->Text));
        }

        //TODO : gérer l'objet, intitulé, commentaire dans la langue de navigation

        //Catégorie consultation
        $consultation->setCategorie($this->categorie->getSelectedValue());

        //Objet de la consultation
        $consultation->setObjetTraduit($this->getLangue(), (new Atexo_Config())->toPfEncoding($this->objet->Text));

        //Intitulé de la consultation
        $consultation->setIntituleTraduit($this->getLangue(), (new Atexo_Config())->toPfEncoding($this->intituleConsultation->Text));

        //Commentaire interne
        $consultation->setChampSuppInvisible((new Atexo_Config())->toPfEncoding($this->commentaire->Text));
        //Lieu d'exécution
        if (Atexo_Module::isEnabled('LieuxExecution')) {
            $consultation->setLieuExecution($this->idsSelectedGeoN2->Value);
        }

        // Codes NUTS
        if (Atexo_Module::isEnabled('CodeNutLtReferentiel')) {
            $codesSec = $this->idAtexoRefNuts->codesRefSec->value;
            if ('' != $codesSec) {
                $consultation->setCodesNuts($codesSec);
            } else {
                $consultation->setCodesNuts(null);
            }
        }

        //Codes CPV

        $consultation->setCodeCpv1(null);
        $consultation->setCodeCpv2(null);
        if (Atexo_Module::isEnabled('AffichageCodeCpv')) {
            $procEquivalence = Atexo_Consultation_ProcedureEquivalence::retrieveProcedureEquivalenceById($consultation->getIdTypeProcedureOrg());
            if ($procEquivalence instanceof CommonProcedureEquivalence) {
                if ($procEquivalence->getAfficherCodeCpv()) {
                    $codePrincipal = $this->referentielCPV->getCpvPrincipale();
                    if ('' != $codePrincipal) {
                        $consultation->setCodeCpv1($codePrincipal);
                    }
                    $codesSec = $this->referentielCPV->getCpvSecondaires();
                    if ('' != $codesSec) {
                        $consultation->setCodeCpv2($codesSec);
                    }
                }
            }
        }
        //LT Referentiel
        $this->ltrefCons->saveReferentielConsultationAndLot($consultation->getId(), 0, $consultation);
        // refrentiel zone texte
        $this->idReferentielZoneText->saveConsultationAndLot($consultation->getId(), 0, $consultation);
        $this->idAtexoLtRefRadio->saveConsultationAndLot($consultation->getId(), 0, $consultation);

        $donneeComplementaire = $consultation->getDonneComplementaire();
        if (!$donneeComplementaire instanceof CommonTDonneeComplementaire) {
            $donneeComplementaire = new CommonTDonneeComplementaire();
        }

        //Allotissement et achat responsable
        if ($this->alloti->checked) {// supprimer les critères atribution de la consultation
            if (Atexo_Module::isEnabled('DonneesRedac')) {
                if (Atexo_Module::isEnabled('DonneesComplementaires') && !$consultation->getAlloti() && $donneeComplementaire instanceof CommonTDonneeComplementaire) {
                    (new Atexo_DonnesComplementaires())->deleteCritereAttribution($donneeComplementaire, $connexion);
                }
                if ($donneeComplementaire instanceof CommonTDonneeComplementaire) {
                    $donneeComplementaire->setJustificationNonAlloti('');
                }
            }
            $consultation->setAlloti('1');
            $this->saveAchatResponsableConsultationAllotie($consultation);
        } else {// supprimer les critères atribution de la consultation
            if (Atexo_Module::isEnabled('DonneesRedac')) {
                if (Atexo_Module::isEnabled('DonneesComplementaires') && $consultation->getAlloti() && $donneeComplementaire instanceof CommonTDonneeComplementaire) {
                    (new Atexo_DonnesComplementaires())->deleteCritereAttribution($donneeComplementaire, $connexion);
                }

                $donneeComplementaire->setJustificationNonAlloti($this->justificationNonAlloti->getText());
            }

            $consultation->setAlloti('0');
            //dans ce cas il n'y a pas de lot et il faut les supprimer s'il en existe
            if (Atexo_Module::isEnabled('DonneesComplementaires') && Atexo_Module::isEnabled('DonneesRedac')) {
                //supprimer les criteres d'attribution pour le lot,
            }
            (new Atexo_Consultation_Lots())->deleteCommonLots(Atexo_CurrentUser::getCurrentOrganism(), $consultation->getId());
        }

        $donneeComplementaire->save($connexion);
        $consultation->setIdDonneeComplementaire($donneeComplementaire->getIdDonneeComplementaire());

        $entitePublique = (new Atexo_Consultation())->getOrganismeDenominationByService(Atexo_CurrentUser::getCurrentOrganism(), Atexo_CurrentUser::getIdServiceAgentConnected());
        $consultation->setOrgDenomination($entitePublique);
        $consultation->setDenominationAdapte(strtoupper(Atexo_Util::formatterChaine($entitePublique)));

        //Si la langue de navigation est une langue obligatoire elle est automatiquement activée
        if ((new Atexo_Languages())->estObligatoire(Atexo_CurrentUser::readFromSession('lang'))) {
            $accessibilite = 'setAccessibilite' . ucfirst(strtolower(Atexo_CurrentUser::readFromSession('lang')));
            $consultation->$accessibilite('1');
        }

        //Recherche de consultations anterieures :
        $maxNumeroPhase = (new Atexo_Consultation())->retrieveMaxNumeroPhaseByReferenceUtilisateur($consultation->getReferenceUtilisateur(), Atexo_CurrentUser::getCurrentOrganism());
        ++$maxNumeroPhase;
        $consultation->setNumeroPhase($maxNumeroPhase);

        // mode de passation
        if (Atexo_Module::isEnabled('ConsultationModePassation') && Atexo_Module::isEnabled('DonneesComplementaires')) {
            $modePassation = '';
            if ($this->surOffrePrix->Checked) {
                $modePassation = Atexo_Config::getParameter('MODE_PASSATION_OFFRE_PRIX');
            } elseif ($this->auRabais->Checked) {
                $modePassation = Atexo_Config::getParameter('MODE_PASSATION_AU_RABAIS');
            }
            $consultation->setModePassation($modePassation);
        }
        if (!Atexo_Config::getParameter('AFFICHAGE_DONNEES_COMPLEMENTAIRES_PERSONNALISEE')) {
            //Addresse de depot / Lieu d'ouverture / Addresse de retrait
            if (Atexo_Module::isEnabled('ConsultationLieuOuverturePlis')) {
                $oldLieu = $consultation->getLieuOuverturePlis();
                $consultation->setLieuOuverturePlis((new Atexo_Config())->toPfEncoding($this->lieuOuverturePlis->Text));
                $lieuOuverture = 'setLieuOuverturePlis' . ucfirst(strtolower(Atexo_CurrentUser::readFromSession('lang')));
                $consultation->$lieuOuverture((new Atexo_Config())->toPfEncoding($this->lieuOuverturePlis->Text));
            }
            if (Atexo_Module::isEnabled('ConsultationAdresseRetraisDossiers')) {
                $oldAdresseRetratit = $consultation->getAdresseRetraisDossiers();
                $consultation->setAdresseRetraisDossiers((new Atexo_Config())->toPfEncoding($this->retraitDossiers->Text));
                $adresseRetraisDossiers = 'setAdresseRetraisDossiers' . ucfirst(strtolower(Atexo_CurrentUser::readFromSession('lang')));
                $consultation->$adresseRetraisDossiers((new Atexo_Config())->toPfEncoding($this->retraitDossiers->Text));
            }
            if (Atexo_Module::isEnabled('ConsultationAdresseDepotOffres')) {
                $oldAdressedepot = $consultation->getAdresseDepotOffres();
                $consultation->setAdresseDepotOffres((new Atexo_Config())->toPfEncoding($this->addDepotDossiers->Text));
                $adresseDepotOffre = 'setAdresseDepotOffres' . ucfirst(strtolower(Atexo_CurrentUser::readFromSession('lang')));
                $consultation->$adresseDepotOffre((new Atexo_Config())->toPfEncoding($this->addDepotDossiers->Text));
            }
            // variantes
            $varianteOld = $consultation->getVariantes();
            $consultation->setVariantes('');
            if (Atexo_Module::isEnabled('ConsultationVariantesAutorisees') && !$consultation->getAlloti()) {
                $variantes = '0';
                if ($this->variantesOui->Checked) {
                    $variantes = '1';
                }
                $consultation->setVariantes($variantes);
            }
            //Début : Historique des données complementaire
            $dataDonneesCompl['varianteOld'] = $varianteOld;
            $dataDonneesCompl['oldLieu'] = $oldLieu;
            $dataDonneesCompl['oldAdressedepot'] = $oldAdressedepot;
            $dataDonneesCompl['oldAdresseRetratit'] = $oldAdresseRetratit;
            $this->sauvegarderHistoriqueDonneesComplementaires($consultation, $connexion, $modification, $dataDonneesCompl);
        }

        //Domaines d'activites
        $this->saveDomaineActivites($consultation);

        //Enregistrer : nature marche, montant marche
        $donneeComplementaire = $consultation->getDonneComplementaire();
        if ($donneeComplementaire instanceof CommonTDonneeComplementaire) {
            $donneeComplementaire->setMontantMarche(Atexo_Util::getMontantArronditSansEspace($this->montantEstimatifMarche->Text));
            if (empty($consultation->getIdDonneeComplementaire())) {
                $this->Page->setViewState('idDonneeComplementaire', $donneeComplementaire->getIdDonneeComplementaire());
            }
            $donneeComplementaire->save($connexion);
        }

        //Enregistrement des infos du bloc "Je souhaite publier un avis depuis cette plate-forme pour cette consultation"
        if (Atexo_Module::isEnabled('Publicite')) {
            if (true == $this->intitulePubliciteOui->getChecked()) {
                $consultation->setDonneePubliciteObligatoire('1');
            } else {
                $consultation->setDonneePubliciteObligatoire('0');
            }

            //Enregistrement des infos du bloc "Je souhaite rédiger les pièces administratives de cette consultation depuis cette plateforme"
            if (Atexo_Module::isEnabled('InterfaceModuleRsem')) {
                if (true == $this->intituleRedactionOui->getChecked()) {
                    $consultation->setDonneeComplementaireObligatoire('1');
                } else {
                    $consultation->setDonneeComplementaireObligatoire('0');
                }
            }
        }

        if (Atexo_Module::isEnabled('InterfaceDume')) {
            $consultation->setDumeDemande($this->dumeDemande->checked);
            $selectedValue = $this->procedureTypeDume->getSelectedValue();
            if (empty($selectedValue)) {
                $selectedValue = Atexo_Consultation_ProcedureEquivalence::getDefaultSelectedTypeProcedureDume($consultation->getIdTypeProcedureOrg(), $consultation->getOrganisme());
            }
            $consultation->setTypeProcedureDume($selectedValue);
            if ($this->dumeDemande->checked) {
                if ($this->typeFormulaireStandard->checked) {
                    $consultation->setTypeFormulaireDume('0');
                } elseif ($this->typeFormulaireSimplifie->checked) {
                    $consultation->setTypeFormulaireDume('1');
                }
            } else {
                if (isset($this)) {
                    if (isset($this->typeProcedureDume)) {
                        $this->typeProcedureDume->setVisible(false);
                    }
                }
                $consultation->setTypeFormulaireDume('0');
            }
        }
        if (Atexo_Module::isEnabled('CaseAttestationConsultation')) {
            $consultation->setAttestationConsultation(($this->caseAttestationConsultation->checked) ? '1' : '0');
        }

        if ($this->intitulePubliciteOui->Checked) {
            if ($this->getViewState('groupBuyers') !== null) {
                $consultation->setGroupement($this->getViewState('groupBuyers'));
            } else {
                $consultation->setGroupement(0);
            }

            if ($this->getViewState('procedureOpenClosed') !== null) {
                $consultation->setProcedureOuverte($this->getViewState('procedureOpenClosed'));
            } else {
                $consultation->setProcedureOuverte(1);
                if ($this->getViewState('procedureTypeOrg')) {
                    $procedureTypeOrg = ($this->getViewState('procedureTypeOrg'));
                    if ($procedureTypeOrg->getMapa() == 0) {
                        $consultation->setProcedureOuverte(null);
                    }
                }
            }
        } elseif ($this->intitulePubliciteNon->Checked) {
            $consultation->setGroupement(null);
            $consultation->setProcedureOuverte(null);
        }
    }

    /**
     * Permet d'initialiser des champs.
     */
    public function initialisationElements()
    {
        if ($this->getConsultation() instanceof CommonConsultation && $this->getConsultation()->getIdAffaire()) {
            $this->idAffaire->value = $this->getConsultation()->getIdAffaire();
        }
        //Validateurs
        $this->formatReference->setRegularExpression('[0-9a-zA-Z_.-' . (Atexo_Module::isEnabled('AutoriserCaracteresSpeciauxDansReference') ? '/' : '') . ']{5,32}');
        $this->formatReferenceSave->setRegularExpression('[0-9a-zA-Z_.-' . (Atexo_Module::isEnabled('AutoriserCaracteresSpeciauxDansReference') ? '/' : '') . ']{5,32}');
        //Achat responsable
        if (Atexo_Module::isEnabled('ConsultationClause')) {
            $this->achatResponsableConsultation->setValidationGroup('validateSave');
        }

        if (true === $this->_consultation->getGroupement()) {
            $this->groupBuyersYes->Selected = true;
            $this->groupBuyersNo->Selected = false;
        } elseif (false === $this->_consultation->getGroupement()) {
            $this->groupBuyersNo->Selected = true;
            $this->groupBuyersYes->Selected = false;
        }

        if (true === $this->_consultation->getProcedureOuverte()) {
            $this->groupProcedureOpen->Selected = true;
            $this->groupProcedureClosed->Selected = false;
        } elseif (false === $this->_consultation->getProcedureOuverte()) {
            $this->groupProcedureClosed->Selected = true;
            $this->groupProcedureOpen->Selected = false;
        }
    }

    /**
     * affiche les denominations des lieux d'executions.
     */
    public function displaySelectedGeoN2($sender, $param)
    {
        $ids = $this->idsSelectedGeoN2->Value;
        $this->denominationGeo2T->Text = $this->getSelectedGeoN2($ids);
    }

    public function getSelectedGeoN2($ids)
    {
        $text = '';
        $arIds = explode(',', $ids);
        $list = Atexo_Geolocalisation_GeolocalisationN2::retrieveGeolocalisationN2byIds($arIds);
        $lieuxExecutionsType1 = '';
        if ($list) {
            foreach ($list as $oneGeoN2) {
                $lieuxExecutionsType1 .= $oneGeoN2->getDenomination1Traduit() . ', ';
            }
            if ($lieuxExecutionsType1) {
                $text = substr($lieuxExecutionsType1, 0, -2);
            }
        }

        return $text;
    }

    /**
     * Raffraichit le bloc des lieux d'executions.
     */
    public function refreshDisplaySelectedGeoN2($sender, $param)
    {
        $ids = $this->idsSelectedGeoN2->Value;
        $this->denominationGeo2T->Text = $this->getSelectedGeoN2($ids);
        $this->Page->bloc_etapeIdentification->lieuExecution->render($param->getNewWriter());

        $this->getLieuExecutionUrl();
    }

    /**
     * Permet de charger les valeurs de procedures equivalence
     * en fonction du changement de type de procedure pour l'onglet "identification".
     *
     * @param $sender
     * @param $param
     */
    public function changeProcedureType($sender, $param)
    {
        $idProcedure = $sender->getSelectedValue();
        $this->arrangeProcedureType($idProcedure, $param);
        $this->displayTypeFormulaireDume($idProcedure);
        if (!Atexo_Config::getParameter('SPECIFIQUE_PLACE')) {
            $this->displayGroupBuyers();
            $this->displayProcedureOpenClosed();
        }
        $this->getProcedureOrganisme();

        $this->activeInfoMsg->render($param->getNewWriter());
        $this->selectPublicationPublicite->render($param->getNewWriter());
    }

    /**
     * Permet de gerer l'affichage d'elements en fonction du type de procedure.
     *
     * @param int $idProcedure
     */
    public function arrangeProcedureType($idProcedure, $param = false, $justIdentification = false)
    {
        $procEquivalence = Atexo_Consultation_ProcedureEquivalence::retrieveProcedureEquivalenceById($idProcedure);
        $this->displayElementsByProcedureType($procEquivalence, $param);
        if ($procEquivalence instanceof CommonProcedureEquivalence && !$justIdentification) {
            $this->Page->bloc_etapeModalitesReponse->afficherMasquerBlocsEnFonctionParametrageProcedures(false, $procEquivalence);
            $this->Page->bloc_etapeDocumentsJoints->afficherMasquerBlocsEnFonctionParametrageProcedures(false, $procEquivalence);
            $this->Page->bloc_etapeCalendrier->afficherMasquerBlocsEnFonctionParametrageProcedures(false, $procEquivalence);
            if (!Atexo_Config::getParameter('SPECIFIQUE_PLACE')) {
                $this->Page->bloc_etapeDonneesComplementaires->afficherMasquerBlocsEnFonctionParametrageProcedures(false, $procEquivalence);
            }
            $this->afficherMasquerBlocsEnFonctionParametrageProcedures($idProcedure);
            if ($param) {
                $this->Page->bloc_etapeModalitesReponse->refreshPanel($param);
                $this->Page->bloc_etapeDocumentsJoints->refreshPanel($param);
            }
        } else {
            $this->activeInfoMsg->displayStyle = 'None';
        }
    }

    /**
     * Enter description here ...
     *
     * @param CommonProcedureEquivalence $procEquivalence
     */
    public function displayElementsByProcedureType($procEquivalence, $param = false)
    {
        if ($procEquivalence instanceof CommonProcedureEquivalence && !isset($_GET['continuation'])) {
            $this->lignePanelalloti->setStyle('NULL');
            $this->displayElement('marcheUnique', $procEquivalence->getEnvOffreTypeUnique(), true, false, 'lignePanelalloti');
            $this->displayElement('alloti', $procEquivalence->getEnvOffreTypeMultiple(), true, false, 'lignePanelalloti');

            if (!isset($_GET['id'])){
                $this->displayElement('intituleRedactionNon', $procEquivalence->getDonneesComplementaireNon(), true, false, 'panelRedac');
                $this->displayElement('intituleRedactionOui', $procEquivalence->getDonneesComplementaireOui(), true, false, 'panelRedac');
            }

            if ($this->alloti->Checked) {
                $script = "<script>traitementConsAlloti(document.getElementById('" . $this->alloti->getClientId() . "'));</script>";
            } else {
                $script = "<script>traitementConsNonAlloti(document.getElementById('" . $this->marcheUnique->getClientId() . "'));</script>";
            }
            $this->labelScript->Text = $script;
            if ($param) {
                $this->panelmarcheUnique->render($param->NewWriter);
                $this->panelalloti->render($param->NewWriter);
            }
        }
    }

    /**
     * Permet de gerer la fonction de gerer les elements passé en parametre.
     *
     * @param $element : element à afficher
     * @param $values : la valeur de l'element dans la table de parametrage des procedures
     * @param $isChecked : précise si l'élement est coché
     */
    public function displayElement($element, $values, $isChecked = true, $fromProcedure = false, $ligneAllElements = false, $panelGlobale = false)
    {
        $panel = 'panel' . $element;
        if (!strcmp($values, '0')) {
            $this->$panel->setStyle('display:');

            if ($isChecked) {
                $this->$element->SetChecked(false);
                $this->$element->SetEnabled(false);
            }
            if ($ligneAllElements && ('display:none' == $this->$ligneAllElements->getStyle()->getCustomStyle() || 'NULL' == $this->$ligneAllElements->getStyle()->getCustomStyle())) {
                $this->$ligneAllElements->setStyle('display:');
            }
        } elseif (!strcmp($values, '1')) {
            $this->$panel->setStyle('display:');
            if ($isChecked) {
                $this->$element->SetChecked(true);
                $this->$element->SetEnabled(false);
            }
            if ($ligneAllElements && ('display:none' == $this->$ligneAllElements->getStyle()->getCustomStyle() || 'NULL' == $this->$ligneAllElements->getStyle()->getCustomStyle())) {
                $this->$ligneAllElements->setStyle('display:');
            }
        } elseif (!strcmp($values, '+1')) {
            $this->$panel->setStyle('display:');
            if ($isChecked) {
                $this->$element->SetChecked(true);
                $this->$element->SetEnabled(true);
            }
            if ($ligneAllElements && ('display:none' == $this->$ligneAllElements->getStyle()->getCustomStyle() || 'NULL' == $this->$ligneAllElements->getStyle()->getCustomStyle())) {
                $this->$ligneAllElements->setStyle('display:');
            }
        } elseif (!strcmp($values, '+0')) {
            $this->$panel->setStyle('display:');
            if ($isChecked) {
                $this->$element->SetChecked(false);
                $this->$element->SetEnabled(true);
            }
            if ($ligneAllElements && ('display:none' == $this->$ligneAllElements->getStyle()->getCustomStyle() || 'NULL' == $this->$ligneAllElements->getStyle()->getCustomStyle())) {
                $this->$ligneAllElements->setStyle('display:');
            }
        } elseif (!strcmp($values, '-1')) {
            $this->$panel->setStyle('display:none');
            if ($isChecked) {
                $this->$element->SetChecked(true);
            }
            if ($ligneAllElements && ('NULL' == $this->$ligneAllElements->getStyle()->getCustomStyle() || null == $this->$ligneAllElements->getStyle()->getCustomStyle())) {
                $this->$ligneAllElements->setStyle('display:none');
            }
        } elseif (!strcmp($values, '-0')) {
            if ($isChecked) {
                $this->$element->SetChecked(false);
            }
            $this->$panel->setStyle('display:none');
            if ($ligneAllElements && ('NULL' == $this->$ligneAllElements->getStyle()->getCustomStyle() || null == $this->$ligneAllElements->getStyle()->getCustomStyle())) {
                $this->$ligneAllElements->setStyle('display:none');
            }
        }
    }

    /**
     * Permet de rafraichir le bloc "identification".
     */
    public function rafraichirBlocIdentification($sender, $param)
    {
        $this->Page->bloc_etapeIdentification->blocEtapeIdentification->render($param->NewWriter);
    }

    public function remplirFormulaireIdentification()
    {
        $libelle = null;
        $consultation = $this->getConsultation();
        if ($consultation instanceof CommonConsultation) {
            //Type d'avis
            if ($consultation->getIdTypeAvis()) {
                $this->intituleAvis->selectedValue = $consultation->getIdTypeAvis();
            }
            //Type de procedure
            if ($consultation->getIdTypeProcedureOrg()) {
                $this->procedureType->selectedValue = $consultation->getIdTypeProcedureOrg();
            }
            
            $this->afficherMasquerBlocsEnFonctionParametrageProcedures($consultation->getIdTypeProcedureOrg());
            //Référence utilisateur
            if ($consultation->getReferenceUtilisateur() && !$this->modeCreationSuiteAvecNumerotationAutomatique()) {
                $this->reference->Text = $consultation->getReferenceUtilisateur();
            }
            //Référence connecteur
            $this->referenceConnecteur->Value = $consultation->getReferenceConnecteur();
            //Type de contrat
            if ($consultation->getTypeMarche()) {
                $this->contratType->selectedValue = $consultation->getTypeMarche();
                if (
                    (new Atexo_Consultation_Contrat())->isTypeContratMarcheSubsequent($consultation->getTypeMarche())
                    || (new Atexo_Consultation_Contrat())->isTypeContratMarcheSpecifique($consultation->getTypeMarche())
                ) {
                    $this->marcheSubsequent->Value = true;
                    $this->panelNumAC->setDisplay('Dynamic');
                    $this->numeroACValue->Value = $consultation->getNumeroAc();
                    $this->idContratSelected->Value = $consultation->getIdContrat();
                    $this->numeroAC->Text = (new Atexo_Consultation_Contrat())->getNumeroACForConsultation($consultation->getIdContrat());

                    if ((new Atexo_Consultation_Contrat())->isTypeContratMarcheSubsequent($this->contratType->getSelectedValue())) {
                        $libelle = Prado::Localize('NUMERO_ACCORD_CADRE_CORRESPONDANT');
                        $this->activeInfoMsgMarcheSubsequent->setDisplayStyle('Dynamic');
                    }
                    if ((new Atexo_Consultation_Contrat())->isTypeContratMarcheSpecifique($this->contratType->getSelectedValue())) {
                        $libelle = Prado::Localize('NUMERO_SAD_CORRESPONDANT');
                        $this->activeInfoMsgMarcheSpecifique->setDisplayStyle('Dynamic');
                    }
                    $this->labelObjetMarche->setText($libelle);
                }
            }
            //Consultation passée en tant qu'entité adjudicatrice
            if ('1' == $consultation->getEntiteAdjudicatrice()) {
                $this->entiteAdjudicatrice->checked = true;
            }

            if (Atexo_Module::isEnabled('MarchePublicSimplifie', $this->organisme)) {
                //Consultation passée en tant marche public simplifie
                if ('1' == $consultation->getMarchePublicSimplifie()) {
                    $this->marcheMPS->checked = true;
                    // Afficher le message informatif rappelant qu'on est en procedure repondant au service MPS
                    $this->activeInfoMsg->displayStyle = 'Dynamic';
                } else {
                    $this->marcheMPS->checked = false;
                    $this->activeInfoMsg->displayStyle = 'None';
                }
            } else {
                $this->marcheMPS->checked = false;
                $this->activeInfoMsg->displayStyle = 'None';
            }

            //Code opération
            if (Atexo_Module::isEnabled('gestionOperations', $this->organisme)) {
                $this->codeOperation->Text = $consultation->getCodeOperation();
                $this->idCode->value = $consultation->getIdOperation();
            }
            //Catégorie de la consultation
            if ($consultation->getCategorie()) {
                $this->categorie->selectedValue = $consultation->getCategorie();
            }
            //Intitulé de la consultation
            $this->intituleConsultation->Text = $consultation->getIntituleTraduit();
            //Objet de la consultation
            $this->objet->Text = $consultation->getObjetTraduit();
            //Commentaire interne
            $this->commentaire->Text = $consultation->getChampSuppInvisible();
            // Affichage codes NUTS
            if (Atexo_Module::isEnabled('CodeNutLtReferentiel')) {
                $this->displayCodesNuts($consultation->getCodesNuts());
            }
            //Affichage codes CPV
            $atexo_ref = new Atexo_Ref();
            $atexo_ref->setCheminFichierConfigXML(Atexo_Config::getParameter('PATH_FILE_CPV_DEMANDE_PRINCIPAL_CONFIG'));
            $atexo_ref->setPrincipal($consultation->getCodeCpv1());
            $atexo_ref->setSecondaires($consultation->getCodeCpv2());
            // Affichage des LT REFERENTIEL
            $this->chargerReferentiels($consultation, $consultation->getAlloti());
            //Lieux d'execution
            if (Atexo_Module::isEnabled('LieuxExecution')) {
                $this->displayGeoN2InModification($consultation->getLieuExecution());
            }

            //Allotissement
            $script = '';
            if ('1' == $consultation->getAlloti()) {
                $this->alloti->checked = true;
                $this->marcheUnique->checked = false;
                $script = "<script>traitementConsAlloti(document.getElementById('" . $this->alloti->getClientId() . "'));</script>";
            } else {
                $script = "<script>traitementConsNonAlloti(document.getElementById('" . $this->marcheUnique->getClientId() . "'));</script>";
                $this->alloti->checked = false;
                $this->marcheUnique->checked = true;
                if (Atexo_Module::isEnabled('DonneesRedac')) {
                    $donneeComp = $consultation->getDonneComplementaire();
                    if ($donneeComp instanceof CommonTDonneeComplementaire) {
                        $this->justificationNonAlloti->setText($donneeComp->getJustificationNonAlloti());
                    }
                }
            }
            $this->Page->scriptJs->Text .= $script;

            if (Atexo_Module::isEnabled('ConsultationClause')) {
                //Bloc achat responsable
                $this->achatResponsableConsultation->afficherClausesConsultation($consultation);
            }
            //mode de passation
            if ($consultation->getModePassation() == Atexo_Config::getParameter('MODE_PASSATION_AU_RABAIS')) {
                $this->auRabais->Checked = true;
            } elseif ($consultation->getModePassation() == Atexo_Config::getParameter('MODE_PASSATION_OFFRE_PRIX')) {
                $this->surOffrePrix->Checked = true;
            }

            if (Atexo_Module::isEnabled('InterfaceDume')) {
                $this->dumeDemande->checked = $consultation->getDumeDemande();
                $this->procedureTypeDume->setSelectedValue($consultation->getTypeProcedureDume());
                if ($consultation instanceof CommonConsultation && $consultation->getId()) {
                    $this->dumeDemande->setEnabled(false);
                    $this->procedureTypeDume->setEnabled(false);
                }
                if ('0' == $consultation->getTypeFormulaireDume()) {
                    $this->typeFormulaireStandard->setChecked(true);
                    $this->typeFormulaireSimplifie->setChecked(false);
                } elseif ('1' == $consultation->getTypeFormulaireDume()) {
                    $this->typeFormulaireStandard->setChecked(false);
                    $this->typeFormulaireSimplifie->setChecked(true);
                }
            }
            if (Atexo_Module::isEnabled('CaseAttestationConsultation')) {
                $this->caseAttestationConsultation->setChecked(('1' == $consultation->getAttestationConsultation()));
            }
        }
    }

    /**
     * Permet de charger le formulaire d'identification
     * Pour les cas suivants: modification, création suite, interface MARCO.
     */
    public function chargerFormulaireIdentification()
    {
        //Récupération de la consultation
        $consultation = $this->getConsultation();

        //Recupération de l'xml
        $_GET['xml'] = Atexo_CurrentUser::readFromSession('xml');
        $_GET['xml'] = html_entity_decode(base64_decode(str_replace(' ', '+', $_GET['xml'])));
        Atexo_CurrentUser::deleteFromSession('xml');

        if (!$this->Page->IsPostBack) {
            // mode de passation
            $this->surOffrePrix->Checked = true;
            if (
                (isset($_GET['xml']) && (Atexo_CurrentUser::hasHabilitation('GererMapaInferieurMontant') || Atexo_CurrentUser::hasHabilitation('GererMapaSuperieurMontant') || Atexo_CurrentUser::hasHabilitation('AdministrerProceduresFormalisees')))
                || (($this->getMode() == Atexo_Config::getParameter('MODE_MODIFICATION_CONSULTATION')) && (Atexo_CurrentUser::hasHabilitation('ModifierConsultationAvantValidation') || $consultation->hasHabilitationModifierApresValidation()))
                    || (Atexo_CurrentUser::hasHabilitation('CreerSuiteConsultation') && isset($_GET['continuation']))
            ) {
                //Interface Marco
                // TODO le contenu de la fonction n'est pas correct chargerDonneesSpecifiquesMarco
                if (isset($_GET['xml']) && '' != $_GET['xml']) {
                    $this->chargerDonneesSpecifiquesMarco($consultation, $_GET['xml']);
                }
                //Création d'une suite
                if (isset($_GET['continuation'])) {
                    $this->chargerDonneesSpecifiquesCreationSuite();
                }
                //Cas modification de la consultation
                else {
                    $this->chargerDonneesSpecifiquesModif($consultation);
                }
            }
            if (isset($_GET['idContrat'])) {
                $this->chargerInfosDepuisContrat($_GET['idContrat'], $consultation);
            }
        }
        $this->gestionAffichageReglementConsultationInfoMsg($consultation);
        if (Atexo_Module::isEnabled('InterfaceDume')) {
            $dumeA = false;
            if ($consultation instanceof CommonConsultation && $consultation->getId()) {
                $this->dumeDemande->setEnabled(false);
                $this->procedureTypeDume->setEnabled(false);
                $dume = Atexo_Dume_AtexoDume::getTDumeContexte($consultation);
                if ($dume instanceof CommonTDumeContexte) {
                    $dumeA = true;
                }
            }
            $this->dumeA->value = $dumeA;
        }
    }

    /**
     * Permet d'afficher les lieux d'exécution.
     */
    public function displayGeoN2InModification($lieuExecution)
    {
        $this->idsSelectedGeoN2->Value = $lieuExecution;
        $ids = $this->idsSelectedGeoN2->Value;
        $this->denominationGeo2T->Text = $this->getSelectedGeoN2($ids);
        $this->numSelectedGeoN2->Value = $this->getNumSelectedGeoN2($ids);
    }

    /**
     * Permet d'afficher les Codes Nuts.
     */
    public function displayCodesNuts($codesNuts)
    {
        $atexoCodesNutsRef = new Atexo_Ref();
        $atexoCodesNutsRef->setCheminFichierConfigXML(Atexo_Config::getParameter('PATH_FILE_NUTS_CONFIG'));
        $atexoCodesNutsRef->setSecondaires($codesNuts);
        $this->idAtexoRefNuts->afficherReferentiel($atexoCodesNutsRef);
    }

    /**
     * Permet de charger les préférences lieux d'exécution.
     */
    public function chargerPreferenceLieuxExecution()
    {
        if (Atexo_Module::isEnabled('LieuxExecution')) {
            $agent = (new Atexo_Agent())->retrieveAgent(Atexo_CurrentUser::getIdAgentConnected());
            if (!isset($_GET['continuation']) && !isset($_GET['id'])) {
                $this->displayGeoN2InModification($agent->getLieuExecution());
            }
        }
    }

    /**
     * Permet de charger les préférences Codes Nuts.
     */
    public function chargerPreferenceCodesNuts()
    {
        if (Atexo_Module::isEnabled('CodeNutLtReferentiel')) {
            $agent = (new Atexo_Agent())->retrieveAgent(Atexo_CurrentUser::getIdAgentConnected());
            if (!isset($_GET['continuation']) && !isset($_GET['id'])) {
                $this->displayCodesNuts($agent->getCodesNuts());
            }
        }
    }

    /**
     * Permet de charger les données spécifiques à MARCO.
     *
     * @param $consultation: objet consultation
     */
    public function chargerDonneesSpecifiquesMarco($consultation, $xml)
    {
        $interfaceConsultation = new Atexo_Interfaces_Consultation();
        $categorie = $interfaceConsultation->getCategorieFromXml($xml);
        $categorie = (new Atexo_Interfaces_InterfacesUtilities())->getCategorie($categorie);
        $objet = $interfaceConsultation->getObjetFromXml($xml);
        $dateFin = $interfaceConsultation->getDateClotureFromXml($xml);
        $dateFin = str_replace('T', ' ', $dateFin);
        $intituleCons = $interfaceConsultation->getIntutileConsultationFromXml($xml);
        $consultation->setIntitule($intituleCons);
        $consultation->setCategorie($categorie);
        $consultation->setDatefin($dateFin);
        $consultation->setObjet($objet);

        //Type de procedure
        $this->procedureType->SetEnabled(true);
    }

    /**
     * Permet de charger les données spécifiques pour la création des suites.
     */
    public function chargerDonneesSpecifiquesCreationSuite()
    {
        //Type de procedure
        $this->procedureType->SetEnabled(true);
        $this->arrangeProcedureType($this->getConsultation()->getIdTypeProcedureOrg());

        //Référence de la consultation
        if ($this->modeCreationSuiteAvecNumerotationAutomatique()) {
            $this->reference->Text = prado::localize('TEXT_GENEREE_ENREGISTREMENT');
            $this->formatReference->setEnabled(false);
            $this->formatReferenceSave->setEnabled(false);
        }
    }

    /**
     * Permet de charger les données spécifiques lors de la modification de la consultation.
     *
     * @param $consultation: objet consultation
     */
    public function chargerDonneesSpecifiquesModif($consultation)
    {
        //Type de procedure
        $this->procedureType->selectedValue = $consultation->getIdTypeProcedureOrg();
        if ($this->getMode() == Atexo_Config::getParameter('MODE_MODIFICATION_CONSULTATION')) {
            $this->procedureType->SetEnabled(false);
            $this->arrangeProcedureType($this->getConsultation()->getIdTypeProcedureOrg(), false, true);

            //Référence de la consultation
            if (Atexo_Module::isEnabled('NumerotationRefCons')) {
                $this->reference->Text = $consultation->getReferenceUtilisateur();
                $this->reference->setEnabled(false);
                $this->formatReference->setEnabled(false);
                $this->formatReferenceSave->setEnabled(false);
            } else {
                $this->reference->Text = $consultation->getReferenceUtilisateur();
                $this->reference->setEnabled(true);
                $this->formatReference->setEnabled(true);
                $this->formatReferenceSave->setEnabled(true);
            }
        }
    }

    /**
     * Permet de sauvegarder l'achat responsable avec les valeurs pour la consultation allotie.
     *
     * @param $consultation: Objet consultation
     */
    public function saveAchatResponsableConsultationAllotie($consultation)
    {
        if (Atexo_Module::isEnabled('ConsultationClause')) {
            //Clause sociale
            $consultation->setClauseSocialeInsertion('0');
            $consultation->setClauseSocialeConditionExecution('0');
            $consultation->setClauseSocialeAteliersProteges('0');
            $consultation->setClauseSocialeEss('0');
            $consultation->setClauseSocialeSiae('0');
            $consultation->setClauseSociale(Atexo_Config::getParameter('CLAUSES_SOCIALES_DESACTIVER'));

            //Clause environnementale
            $consultation->setClauseEnvSpecsTechniques('0');
            $consultation->setClauseEnvCondExecution('0');
            $consultation->setClauseEnvCriteresSelect('0');
            $consultation->setClauseEnvironnementale(Atexo_Config::getParameter('CLAUSES_ENVIRONNEMENTALES_DESACTIVER'));
        }
    }

    /**
     * Permet de gérer l'affichage du composant achat responsable.
     */
    public function gererAffichageAchatResponsable()
    {
        if (Atexo_Module::isEnabled('ConsultationClause')) {
            $consultation = $this->Page->getConsultation();
            if ($consultation instanceof CommonConsultation) {
                $script = '<script>';
                if ($consultation->getClauseSociale() == Atexo_Config::getParameter('CLAUSES_SOCIALES_ACTIVER')) {
                    $script .= "showDiv('ctl0_CONTENU_PAGE_bloc_etapeIdentification_achatResponsableConsultation_panel_clausesSociales'); ";
                } elseif ($consultation->getClauseSociale() == Atexo_Config::getParameter('CLAUSES_SOCIALES_DESACTIVER')) {
                    $script .= "hideDiv('ctl0_CONTENU_PAGE_bloc_etapeIdentification_achatResponsableConsultation_panel_clausesSociales'); ";
                }
                if ($consultation->getClauseEnvironnementale() == Atexo_Config::getParameter('CLAUSES_ENVIRONNEMENTALES_ACTIVER')) {
                    $script .= "showDiv('ctl0_CONTENU_PAGE_bloc_etapeIdentification_achatResponsableConsultation_panel_clausesEnvironnementales'); ";
                } elseif ($consultation->getClauseEnvironnementale() == Atexo_Config::getParameter('CLAUSES_ENVIRONNEMENTALES_DESACTIVER')) {
                    $script .= "hideDiv('ctl0_CONTENU_PAGE_bloc_etapeIdentification_achatResponsableConsultation_panel_clausesEnvironnementales'); ";
                }
                $script .= '</script>';
                $this->Page->scriptJs->Text .= $script;
            }
        }
    }

    /**
     * Permet de charger l'url du lieu d'éxécution.
     */
    public function chargerUrlLieuxExecution()
    {
        $sessionLang = Atexo_CurrentUser::readFromSession('lang');

        if (Atexo_Module::isEnabled('MultiLinguismeEntreprise') && isset($sessionLang)) {
            $ids = $this->idsSelectedGeoN2->Value;
            if (!empty($ids)) {
                $this->numSelectedGeoN2->Value = $this->getNumSelectedGeoN2($ids);
            }

            $this->linkLieuExe1->NavigateUrl = $this->getLieuExecutionUrl();
        }
    }

    /**
     * Permet de charger les lieux d'exécution de la consultation.
     */
    public function getNumSelectedGeoN2($buffer)
    {
        $text = '';
        $arrayId = explode(',', $buffer);
        //Suppression des elements nuls du tableau
        $arrayId = array_filter($arrayId);
        $new_arrayId = [];

        $geoN2 = Atexo_Geolocalisation_GeolocalisationN2::retrieveGeolocalisationN2byIds($arrayId);
        if ($geoN2) {
            foreach ($geoN2 as $oneN2) {
                $new_arrayId[] = $oneN2->getDenomination2();
            }
        }

        $text = implode('_', $new_arrayId);

        return $text;
    }

    /**
     * Afficher le mode de passation.
     */
    public function afficherModePassation()
    {
        if (Atexo_Module::isEnabled('ConsultationModePassation') && Atexo_Module::isEnabled('DonneesComplementaires')) {
            $this->panelModePassation->setDisplay('Dynamic');
        } else {
            $this->panelModePassation->setDisplay('None');
        }
    }

    /**
     * Verifie si on est en mode création suite dans le cas de la numérotation automatique de la référence consultation.
     */
    public function modeCreationSuiteAvecNumerotationAutomatique()
    {
        if (($_GET['continuation']) && '1' == Atexo_Config::getParameter('NUMEROTATION_AUTOMATIQUE_SUITE') && Atexo_Module::isEnabled('NumerotationRefCons')) {
            return true;
        }

        return false;
    }

    /**
     * Verifie si on est en mode création consultation dans le cas de la numérotation automatique de la référence consultation.
     */
    public function modeCreationConsultationAvecNumerotationAutomatique()
    {
        if (Atexo_Module::isEnabled('NumerotationRefCons') && !isset($_GET['id'])) {
            return true;
        }

        return false;
    }

    /**
     * Permet de préciser si la modification est permise.
     *
     * @param $consultation: objet consultation
     * Si Oui retourne true, sinon false
     */
    public function isModificationAllowed($consultation)
    {
        return (new Atexo_Consultation())->isModificationAllowed($consultation, isset($_GET['continuation']), isset($_GET['id']));
    }

    /**
     * permet de valider les données de l'étape consultation.
     *
     * @params : $consultation : la consultation,$isValid,$arrayMsgErreur
     */
    public function validerDonneesIdentificationConsultation($consultation, &$isValid, &$arrayMsgErreur)
    {
        if ($consultation instanceof CommonConsultation) {
            if ((('1' == $consultation->getAlloti() && !$this->marcheUnique->checked) || $this->alloti->checked) && (!is_array($consultation->getAllLots()) || !count($consultation->getAllLots()))) {
                $isValid = false;
                $arrayMsgErreur[] = Prado::localize('DEFINE_VOUS_DEVEZ_AJOUTER_AU_MOINS_UN_LOT');
            }
        }
    }

    /**
     * Permet de recharger les donnees de la consultation selon l'allotissement.
     *
     * @param TCallbackEventParameter $param,Objet CommonConsultation  $consultation la consultation,Integer $alloti
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function refreshDonneesComplementaireConsultation($param, $consultation, $alloti)
    {
        if ($consultation instanceof CommonConsultation) {
            $this->chargerCodeCpv($consultation);
            $this->chargerReferentiels($consultation, $alloti);
            $this->panelLtref->render($param->getNewWriter());
            $this->panelLtrefTextAndRadio->render($param->getNewWriter());
            if (Atexo_Module::isEnabled('DonneesComplementaires')) {
                $donneeComplementaire = $this->recupererDonneesComplementaires();
            }
        }
    }

    /*
     * sugges Names
     */
    protected function suggestNames($sender, $param)
    {
        $token = Atexo_Util::httpEncodingToUtf8($param->getToken());
        $sousService = false;
        if (Atexo_Module::isEnabled('organisationCentralisee', $this->getOrganisme())) {
            $sousService = true;
        }
        $idService = Atexo_CurrentUser::getIdServiceAgentConnected();
        if ($listeOperations = (new Atexo_Operations_Operations())->retreiveListeOperations($token, $idService, Atexo_CurrentUser::getCurrentOrganism(), $sousService)) {
            $sender->DataSource = $listeOperations;
            $sender->dataBind();
        } else {
            $sender->DataSource = [];
            $sender->dataBind();
        }
        $this->idCode->value = '';
    }

    /*
     * Fonction appelé a la selection de la liste de l'auto completion
     */
    protected function suggestionSelected($sender, $param)
    {
        $arrayValue = explode('#', $sender->Suggestions->DataKeys[$param->selectedIndex]);
        $this->idCode->value = $arrayValue['0'];
        $this->codeOperation->Text = $arrayValue['1'];
    }

    /**
     * Permet de charger les informations complémentaires de la consultation.
     */
    public function chargerInformationsComplementaires()
    {
        if (!Atexo_Config::getParameter('AFFICHAGE_DONNEES_COMPLEMENTAIRES_PERSONNALISEE')) {
            if (!$this->getConsultation()->getId() && !isset($_GET['continuation'])) {
                //Adresses de retrait, de depot et lieux d'ouverture recuperées au niveau des adresses parametrées du service
                $organisme = Atexo_CurrentUser::getCurrentOrganism();
                $idService = Atexo_CurrentUser::getIdServiceAgentConnected();
                $adresse = (new Atexo_Consultation())->retreiveAdressesServices($idService, $organisme);
                if ($adresse) {
                    if (Atexo_Module::isEnabled('ConsultationAdresseRetraisDossiers')) {
                        $this->retraitDossiers->Text = $adresse->getAdresseRetraisDossiersTraduit();
                    }
                    if (Atexo_Module::isEnabled('ConsultationAdresseDepotOffres')) {
                        $this->addDepotDossiers->Text = $adresse->getAdresseDepotOffresTraduit();
                    }
                    if (Atexo_Module::isEnabled('ConsultationLieuOuverturePlis')) {
                        $this->lieuOuverturePlis->Text = $adresse->getLieuOuverturePlisTraduit();
                    }
                }
                //Cocher la variante par défaut à non
                if (Atexo_Module::isEnabled('ConsultationVariantesAutorisees') && !$this->getConsultation()->getAlloti()) {
                    $this->variantesNon->Checked = true;
                }
            } else {
                //Addresse de retrait des dossiers / Addresse de depots des offres / Lieu d'ouverture de Plis
                if (Atexo_Module::isEnabled('ConsultationLieuOuverturePlis')) {
                    $this->lieuOuverturePlis->Text = $this->getConsultation()->getLieuOuverturePlisTraduit();
                }
                if (Atexo_Module::isEnabled('ConsultationAdresseRetraisDossiers')) {
                    $this->retraitDossiers->Text = $this->getConsultation()->getAdresseRetraisDossiersTraduit();
                }
                if (Atexo_Module::isEnabled('ConsultationAdresseDepotOffres')) {
                    $this->addDepotDossiers->Text = $this->getConsultation()->getAdresseDepotOffresTraduit();
                }
                // variantes
                if (Atexo_Module::isEnabled('ConsultationVariantesAutorisees') && !$this->getConsultation()->getAlloti()) {
                    if ('1' == $this->getConsultation()->getVariantes()) {
                        $this->variantesOui->Checked = true;
                    } else {
                        $this->variantesNon->Checked = true;
                    }
                }
            }
        }

        $donneeComplementaire = $this->getConsultation()->getDonneComplementaire();
        if ($donneeComplementaire instanceof CommonTDonneeComplementaire) {
            $this->montantEstimatifMarche->Text = Atexo_Util::getMontantArronditEspace($donneeComplementaire->getMontantMarche());
        }
    }

    /**
     * Permet de sauvegarder l'historique Variante.
     *
     * @param $consultation: Objet consultation
     * @param $connexion: Objet connexion
     * @param $org: l'acronyme de l'organisme
     */
    public function saveHistoriqueVariantes($oldVariante, $consultation, $connexion, $org, $modification = true)
    {
        $arrayParameters = [];
        if (Atexo_Module::isEnabled('ConsultationVariantesAutorisees') && ($oldVariante != $consultation->getVariantes() || !$modification)) {
            $nomElement = Atexo_Config::getParameter('ID_HISTORIQUE_VARIANTES');
            $lot = '0';
            $valeur = '0';
            $detail1 = '';
            $detail2 = '';
            if ($consultation->getVariantes()) {
                $valeur = '1';
                $detail1 = $consultation->getVariantes();
            }
            if ($modification) {
                $statut = Atexo_Config::getParameter('MODIFICATION_FILE');
            } else {
                $statut = Atexo_Config::getParameter('CREATION_FILE');
            }
            //Sauvegarde
            $arrayParameters[0] = $consultation->getId();
            $arrayParameters[1] = $nomElement;
            $arrayParameters[2] = $valeur;
            $arrayParameters[3] = $detail1;
            $arrayParameters[4] = $detail2;
            $arrayParameters[5] = $lot;
            $arrayParameters[6] = $org;
            $arrayParameters[7] = $statut;
            (new Atexo_Consultation())->saveHistoriqueConsultation($arrayParameters, $connexion);
        }
    }

    /**
     * Permet de sauvegarder l'historique Adresse de depot et lieu ouverture.
     *
     * @param $consultation: Objet consultation
     * @param $connexion: Objet connexion
     * @param $org: l'acronyme de l'organisme
     */
    public function saveHistoriqueLieuOuverture($lieuOuverture, $consultation, $connexion, $org, $modification = true)
    {
        $arrayParameters = [];
        if (
            Atexo_Module::isEnabled('ConsultationLieuOuverturePlis') &&
            (trim($lieuOuverture) != trim($consultation->getLieuOuverturePlisTraduit()) || !$modification)
        ) {
            $valeur = '0';
            $detail2 = '';
            $detail1 = '';
            $lot = '0';
            $nomElement = Atexo_Config::getParameter('ID_LIEU_OUVERTURE_PLIS');

            if ($consultation->getLieuOuverturePlisTraduit()) {
                $valeur = '1';
                $detail1 = $consultation->getLieuOuverturePlisTraduit();
            }
            if ($modification) {
                $statut = Atexo_Config::getParameter('MODIFICATION_FILE');
            } else {
                $statut = Atexo_Config::getParameter('CREATION_FILE');
            }
            //Sauvegarde
            $arrayParameters[0] = $consultation->getId();
            $arrayParameters[1] = $nomElement;
            $arrayParameters[2] = $valeur;
            $arrayParameters[3] = $detail1;
            $arrayParameters[4] = $detail2;
            $arrayParameters[5] = $lot;
            $arrayParameters[6] = $org;
            $arrayParameters[7] = $statut;
            (new Atexo_Consultation())->saveHistoriqueConsultation($arrayParameters, $connexion);
        }
    }

    /**
     * Permet de sauvegarder l'historique Adresse de depot.
     *
     * @param $consultation: Objet consultation
     * @param $connexion: Objet connexion
     * @param $org: l'acronyme de l'organisme
     */
    public function saveHistoriqueAdresseDepotOffres($adresseOld, $consultation, $connexion, $org, $modification = true)
    {
        $arrayParameters = [];
        if (
            Atexo_Module::isEnabled('ConsultationAdresseDepotOffres') &&
            (trim($adresseOld) != trim($consultation->getAdresseDepotOffresTraduit()) || !$modification)
        ) {
            $valeur = '0';
            $detail2 = '';
            $detail1 = '';
            $lot = '0';
            $nomElement = Atexo_Config::getParameter('ID_ADRESSE_DEPOT_OFFRES');

            if ($consultation->getAdresseDepotOffresTraduit()) {
                $valeur = '1';
                $detail1 = $consultation->getAdresseDepotOffresTraduit();
            }
            if ($modification) {
                $statut = Atexo_Config::getParameter('MODIFICATION_FILE');
            } else {
                $statut = Atexo_Config::getParameter('CREATION_FILE');
            }
            //Sauvegarde
            $arrayParameters[0] = $consultation->getId();
            $arrayParameters[1] = $nomElement;
            $arrayParameters[2] = $valeur;
            $arrayParameters[3] = $detail1;
            $arrayParameters[4] = $detail2;
            $arrayParameters[5] = $lot;
            $arrayParameters[6] = $org;
            $arrayParameters[7] = $statut;
            (new Atexo_Consultation())->saveHistoriqueConsultation($arrayParameters, $connexion);
        }
    }

    /**
     * Permet de sauvegarder l'historique Adresse retrait.
     *
     * @param $consultation: Objet consultation
     * @param $connexion: Objet connexion
     * @param $org: l'acronyme de l'organisme
     */
    public function saveHistoriqueAdresseRetrait($adresseOld, $consultation, $connexion, $org, $modification = true)
    {
        $arrayParameters = [];
        if (
            Atexo_Module::isEnabled('ConsultationAdresseRetraisDossiers')
            && (trim($adresseOld) != trim($consultation->getAdresseRetraisDossiersTraduit()) || !$modification)
        ) {
            $nomElement = Atexo_Config::getParameter('ID_ADRESSE_RETRAIT_DOSSIERS');
            $valeur = '0';
            $detail2 = '';
            $detail1 = '';
            $lot = '0';
            if ($consultation->getAdresseRetraisDossiersTraduit()) {
                $valeur = '1';
                $detail1 = $consultation->getAdresseRetraisDossiersTraduit();
            }
            if ($modification) {
                $statut = Atexo_Config::getParameter('MODIFICATION_FILE');
            } else {
                $statut = Atexo_Config::getParameter('CREATION_FILE');
            }
            //Sauvegarde
            $arrayParameters[0] = $consultation->getId();
            $arrayParameters[1] = $nomElement;
            $arrayParameters[2] = $valeur;
            $arrayParameters[3] = $detail1;
            $arrayParameters[4] = $detail2;
            $arrayParameters[5] = $lot;
            $arrayParameters[6] = $org;
            $arrayParameters[7] = $statut;
            (new Atexo_Consultation())->saveHistoriqueConsultation($arrayParameters, $connexion);
        }
    }

    /**
     * Permet d'afficher les autres données complémentaires des consultations et des lots.
     */
    public function afficherAutresDonneesComplementairesConsultationLots()
    {
        if (Atexo_Module::isEnabled('ConsultationVariantesAutorisees') && !Atexo_Config::getParameter('AFFICHAGE_DONNEES_COMPLEMENTAIRES_PERSONNALISEE')) {
            $this->panelVariantesAutorisees->setDisplay('Dynamic');
        }
    }

    /**
     * Permet de masquer les autres données complémentaires des consultations et des lots.
     */
    public function masquerAutresDonneesComplementairesConsultationLots()
    {
        if (Atexo_Module::isEnabled('ConsultationVariantesAutorisees') && !Atexo_Config::getParameter('AFFICHAGE_DONNEES_COMPLEMENTAIRES_PERSONNALISEE')) {
            $this->panelVariantesAutorisees->setDisplay('None');
        }
    }

    /**
     * Permet de sauvegarder l'historique des données complémentaires.
     *
     * @param CommonConsultation $consultation
     * @param Objet              $connexion
     * @param string             $modification
     * @param array              $dataDonneesCompl
     */
    public function sauvegarderHistoriqueDonneesComplementaires($consultation, $connexion, $modification, $dataDonneesCompl)
    {
        if (!Atexo_Config::getParameter('AFFICHAGE_DONNEES_COMPLEMENTAIRES_PERSONNALISEE')) {
            $varianteOld = $dataDonneesCompl['varianteOld'];
            $oldLieu = $dataDonneesCompl['oldLieu'];
            $oldAdressedepot = $dataDonneesCompl['oldAdressedepot'];
            $oldAdresseRetratit = $dataDonneesCompl['oldAdresseRetratit'];
            if ($consultation->getId()) {
                //Début : Historique des données complementaire
                if (!$consultation->getAlloti()) {
                    $this->saveHistoriqueVariantes($varianteOld, $consultation, $connexion, Atexo_CurrentUser::getCurrentOrganism(), $modification);
                }
                $this->saveHistoriqueLieuOuverture($oldLieu, $consultation, $connexion, Atexo_CurrentUser::getCurrentOrganism(), $modification);
                $this->saveHistoriqueAdresseDepotOffres($oldAdressedepot, $consultation, $connexion, Atexo_CurrentUser::getCurrentOrganism(), $modification);
                $this->saveHistoriqueAdresseRetrait($oldAdresseRetratit, $consultation, $connexion, Atexo_CurrentUser::getCurrentOrganism(), $modification);
            }
        }
    }

    /**
     * Permet d'afficher/masquer des blocs en fonction des valeurs parametrées dans les procédures équivalences.
     *
     * @param int $idProcedure
     *
     * @author LEZ <loubna.ezziani@atexom.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     */
    public function afficherMasquerBlocsEnFonctionParametrageProcedures($idProcedure)
    {
        $this->activeInfoMsg->displayStyle = 'None';
        $consultation = $this->Page->getConsultation();

        $procEquivalence = Atexo_Consultation_ProcedureEquivalence::retrieveProcedureEquivalenceById($idProcedure);
        if ($procEquivalence instanceof CommonProcedureEquivalence) {
            if (Atexo_Module::isEnabled('MarchePublicSimplifie', $this->getOrganisme())) {
                $this->panelmarcheMPS->setStyle('NULL');
                $this->displayElement('marcheMPS', $procEquivalence->getMarchePublicSimplifie(), true, false, 'panelmarcheMPS');
                if (1 == $procEquivalence->getMarchePublicSimplifie()) {
                    $this->activeInfoMsg->displayStyle = 'Dynamic';
                }
            } else {
                $this->panelmarcheMPS->setStyle('display:none');
            }

            if (Atexo_Module::isEnabled('InterfaceDume')) {
                $this->displayElement('dumeDemande', $procEquivalence->getDumeDemande(), true);
                $this->displayElement('typeFormulaireStandard', $procEquivalence->getTypeFormulaireDumeStandard(), true);
                $this->displayElement('typeFormulaireSimplifie', $procEquivalence->getTypeFormulaireDumeSimplifie(), true);
                $this->displayElement('TypeProcedureDume', $procEquivalence->getTypeProcedureDume(), false);
                $this->paramTypeProcedureDume->setValue($procEquivalence->getTypeProcedureDume());

                $typesProcedureDume = (new Atexo_Consultation_ProcedureEquivalence())->getProcedureAndParametrageDume($procEquivalence->getIdTypeProcedure(), $procEquivalence->getOrganisme());
                $selectedId = 0;
                $figer = false;
                $listeTypesProcedureDume = [];
                $listeTypesProcedureDume[0] = '--- ' . Prado::localize('TEXT_RECHERCHE_AVANCEE_TOUS_TYPES_PROCEDURES_DUME') . ' ---';
                if (is_array($typesProcedureDume)) {
                    foreach ($typesProcedureDume as $type) {
                        if ($type['afficher']) {
                            $listeTypesProcedureDume[$type['id']] = $type['libelle'];
                            if ($type['selectionner']) {
                                $selectedId = $type['id'];
                                if ($type['figer']) {
                                    $figer = true;
                                    break;
                                }
                            }
                        }
                    }
                }
                $this->procedureTypeDume->setDataSource($listeTypesProcedureDume);
                $this->procedureTypeDume->dataBind();
                $this->procedureTypeDume->setSelectedValue($selectedId);
                $this->procedureTypeDume->setEnabled(!$figer);

                $this->labelScriptDUME->Text = "<script>traitementDumeDemande(document.getElementById('" . $this->dumeDemande->getClientId() . "'));</script>";
            }

            if (!Atexo_Config::getParameter('SPECIFIQUE_PLACE')) {
                if (Atexo_Module::isEnabled('Publicite')) {
                    if ($consultation->getDonneePubliciteObligatoire() === null){
                        $this->displayElement(
                            'intitulePubliciteOui',
                            $procEquivalence->getAutoriserPubliciteOui(),
                            true,
                            false,
                            "selectPublicationPublicite"
                        );
                        $this->displayElement(
                            'intitulePubliciteNon',
                            $procEquivalence->getAutoriserPubliciteNon(),
                            true,
                            false,
                            "selectPublicationPublicite"
                        );
                    }elseif ($consultation->getDonneePubliciteObligatoire() === "1") {
                               $this->displayElement(
                                'intitulePubliciteOui',
                                $procEquivalence->getAutoriserPubliciteOui(),
                                true,
                                false,
                                "selectPublicationPublicite"
                            );
                        } else {
                            $this->displayElement(
                                'intitulePubliciteNon',
                                $procEquivalence->getAutoriserPubliciteNon(),
                                true,
                                false,
                                "selectPublicationPublicite"
                            );
                        }


                    $clientId = (in_array($procEquivalence->getAutoriserPubliciteOui(), ["1", "+1", "-1"])) ?
                        $this->intitulePubliciteOui->getClientId() : $this->intitulePubliciteNon->getClientId();
                    $this->scriptIntitulePublicite->Text = "<script>
                                                    actionChoixPubliciteRedac('".$clientId."');
                                                    cocherDecocherBoutonDonneesOrmeOui('".$clientId."');
                                                        </script>";
                }
            }
            ///Gérer code CPV
            $this->gererAffichageCodeCpv($procEquivalence);
        }
    }

    /**
     * Permet de sauvgarder les domaines d'activites.
     *
     * @param CommonConsultation $consultation la consultation concernee
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function saveDomaineActivites(&$consultation)
    {
        if (Atexo_Module::isEnabled('ConsultationDomainesActivites')) {
            $idsDomaines = $this->domaineActivite->idsDomaines->Text;
            if ('#' != $idsDomaines[0]) {
                $idsDomaines = '#' . $idsDomaines;
            }
            $consultation->setDomainesActivites($idsDomaines);
        }
    }

    /**
     * Permet de charger les domaines d'actives.
     *
     * @param CommonConsultation $consultation la consultation concernee
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function chargerDomainesActivites($consultation)
    {
        // chercher la condition
        if (Atexo_Module::isEnabled('ConsultationDomainesActivites')) {
            $idsDomaines = $consultation->getDomainesActivites();
            if ('#' == $idsDomaines[0]) {
                $idsDomaines = substr($idsDomaines, 1);
            }
            $this->domaineActivite->idsDomaines->Text = $idsDomaines;
        }
    }

    /**
     * Permet de gerer l'affichage du message informatif qui rappelle à l'agent qu'il est en procédure répondant au service MPS.
     *
     * @param $consultation: Objet consultation
     *
     * @return void
     *
     * @author YEL <youssef.elalaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    private function gestionAffichageReglementConsultationInfoMsg($consultation)
    {
        $this->activeInfoMsg->displayStyle = 'None';

        if (
            $consultation instanceof CommonConsultation
            && Atexo_Module::isEnabled('MarchePublicSimplifie', $this->organisme)
            && 1 == $consultation->getMarchePublicSimplifie()
        ) {
            $this->activeInfoMsg->displayStyle = 'Dynamic';
        }
    }

    /**
     * Permet d'afficher ou non le numero AC selon le type contrat.
     *
     * @param $sender
     * @param $param
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function changeTypeContrat($sender, $param)
    {
        $libelle = null;
        $holder = null;
        $this->numeroACValue->Value = '';
        $this->idContratSelected->Value = '';
        $this->numeroAC->Text = '';
        if (
            (new Atexo_Consultation_Contrat())->isTypeContratMarcheSubsequent($this->contratType->getSelectedValue())
            || (new Atexo_Consultation_Contrat())->isTypeContratMarcheSpecifique($this->contratType->getSelectedValue())
        ) {
            $this->marcheSubsequent->Value = true;
            $this->panelNumAC->setDisplay('Dynamic');
            if ((new Atexo_Consultation_Contrat())->isTypeContratMarcheSubsequent($this->contratType->getSelectedValue())) {
                $libelle = Prado::Localize('NUMERO_ACCORD_CADRE_CORRESPONDANT');
                $holder = Prado::localize('NUM_LONG_OR_OBJET_AC');
                $this->activeInfoMsgMarcheSubsequent->setDisplayStyle('Dynamic');
            }
            if ((new Atexo_Consultation_Contrat())->isTypeContratMarcheSpecifique($this->contratType->getSelectedValue())) {
                $libelle = Prado::Localize('NUMERO_SAD_CORRESPONDANT');
                $holder = Prado::localize('NUM_LONG_OR_OBJET_SAD');
                $this->activeInfoMsgMarcheSpecifique->setDisplayStyle('Dynamic');
            }
            $this->labelObjetMarche->setText($libelle);
            $this->labelScript->Text = "<script>J('#" . $this->numeroAC->ClientId . "').prop('title', '" . addslashes($libelle) . "');
												J('#" . $this->numeroAC->ClientId . "').prop('placeholder', '" . addslashes($holder) . "');
										</script>";
        } else {
            $this->marcheSubsequent->Value = false;
            $this->panelNumAC->setDisplay('None');
        }

        if (Atexo_Module::isEnabled('DonneesRedac')) {
            $contratService = Atexo_Util::getSfService(ContratService::class);
            $techniqueAchat = $contratService->getTechniqueAchatLibelleByTypeContratId($this->contratType->getSelectedValue());
            if (!empty($techniqueAchat)) {
                $this->Page->bloc_etapeDonneesComplementaires->donneeProcedure->technique_achat->Text = $techniqueAchat;
            }
        }

        self::chargerProcedureByTypeContrat();
    }

    /**
     * Permet d'afficher les numero AC suggerer.
     *
     * @param $sender
     * @param $param
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    protected function suggestNumborsAC($sender, $param)
    {
        $token = Atexo_Util::httpEncodingToUtf8($param->getToken());
        $organisme = Atexo_CurrentUser::getCurrentOrganism();
        $idService = Atexo_CurrentUser::getCurrentServiceId();
        $typeACSad = '';
        if ((new Atexo_Consultation_Contrat())->isTypeContratMarcheSubsequent($this->contratType->getSelectedValue())) {
            $typeACSad = Atexo_Config::getParameter('TYPE_CONTRAT_ACCORD_CADRE');
        } elseif ((new Atexo_Consultation_Contrat())->isTypeContratMarcheSpecifique($this->contratType->getSelectedValue())) {
            $typeACSad = Atexo_Config::getParameter('TYPE_CONTRAT_SAD');
        }
        $dataSource = (new Atexo_Consultation_Contrat())->getDataContratAcByNumero($token, $idService, $organisme, $typeACSad);

        $sender->DataSource = $dataSource;
        $sender->dataBind();
        $this->labelScript->Text = '<script>hideLoader();</script>';
    }

    /*
     * Fonction appelé a la selection de la liste de l'auto completion
     *
     * @param $sender
     * @param $param
     * @return void
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     * @version 1.0
     * @since 2015-roadmap
     * @copyright Atexo 2015
     */
    protected function suggestionNumborsACSelected($sender, $param)
    {
        $id = $sender->Suggestions->DataKeys[$param->selectedIndex];
        $contrat = (new Atexo_Consultation_Contrat())->getContratTitulaireById($id);
        $commentaire = '';
        if ($contrat instanceof CommonTContratTitulaire) {
            $this->numeroACValue->Value = $contrat->getNumeroContrat();
            $this->idContratSelected->Value = $id;
            $this->categorie->setSelectedValue($contrat->getCategorie());
            $this->reference->Text = $contrat->getReferenceConsultation();
            $this->intituleConsultation->Text = $contrat->getObjetContrat();
            $this->objet->Text = $contrat->getObjetContrat();
            if (Atexo_Module::isEnabled('ConsultationClause')) {
                $this->achatResponsableConsultation->afficherClausesConsultation($contrat);
            }
            if (Atexo_Module::isEnabled('LieuxExecution') && $contrat->getLieuExecution()) {
                $this->displayGeoN2InModification($contrat->getLieuExecution());
            }
            if (Atexo_Module::isEnabled('AffichageCodeCpv')) {
                $this->referentielCPV->chargerReferentielCpv($contrat->getCodeCpv1(), $contrat->getCodeCpv2());
            }

            if (!Atexo_Config::getParameter('AFFICHAGE_DONNEES_COMPLEMENTAIRES_PERSONNALISEE')) {
                $contratsTitulaire = (new Atexo_Consultation_Contrat())->getContratTitulaireByIdContratMulti($id);
                if (is_array($contratsTitulaire) && count($contratsTitulaire)) {
                    $commentaire = Prado::Localize('CONTRACTANT_S') . "\n";
                    foreach ($contratsTitulaire as $contratTitulaire) {
                        $commentaire .= "\n" . $contratTitulaire->getNomTitulaireContrat();
                        $codePostal = $contratTitulaire->getCodePostalEtablissementContrat();
                        $ville = $contratTitulaire->getVilleEtablissementContrat();
                        if ($codePostal || $ville) {
                            $commentaire .= ' (' . $codePostal .
                                            (($codePostal && $ville) ? ' - ' : '') .
                                $ville . ')';
                        }
                    }
                } else {
                    $commentaire = Prado::Localize('CONTRACTANT_S') . "\n";
                    $commentaire .= "\n" . $contrat->getNomTitulaireContrat();
                    $codePostal = $contrat->getCodePostalEtablissementContrat();
                    $ville = $contrat->getVilleEtablissementContrat();
                    if ($codePostal || $ville) {
                        $commentaire .= ' (' . $codePostal .
                            (($codePostal && $ville) ? ' - ' : '') .
                            $ville . ')';
                    }
                }
                $this->commentaire->Text = $commentaire;
            }
        }
        $this->labelScript->Text = '<script>hideLoader();</script>';
    }

    /*
     * TODO KBE factorisation de la fonction avec suggestionNumborsACSelected
     *
     */
    protected function chargerInfosDepuisContrat($id, &$consultation)
    {
        $contrat = (new Atexo_Consultation_Contrat())->getContratTitulaireById($id);
        $commentaire = '';
        if ($contrat instanceof CommonTContratMulti || $contrat instanceof CommonTContratTitulaire) {
            if ($consultation instanceof CommonConsultation) {
                $typeMarche = (new Atexo_Consultation_Contrat())->getIdTypeContratSuiteACSAD($contrat->getIdTypeContrat());
                $consultation->setTypeMarche($typeMarche);
                self::chargerProcedureByTypeContrat($typeMarche);
                $consultation->setIdContrat($id);
                $consultation->setObjet($contrat->getObjetContrat());
                $consultation->setIntitule($contrat->getObjetContrat());
                //$consultation->setReferenceUtilisateur($contrat->getReferenceLibre());
                $consultation->setReferenceUtilisateur($contrat->getReferenceConsultation());
                $consultation->setOrganisme(Atexo_CurrentUser::getOrganismAcronym());
                $consultation->setServiceId(Atexo_CurrentUser::getCurrentServiceId());
                $consultation->setNumeroAc($contrat->getNumeroContrat());
                $consultation->setCategorie($contrat->getCategorie());
                if (Atexo_Module::isEnabled('ConsultationClause')) {
                    $this->achatResponsableConsultation->afficherClausesConsultation($contrat);
                }
                if (Atexo_Module::isEnabled('LieuxExecution') && $contrat->getLieuExecution()) {
                    $consultation->setLieuExecution($contrat->getLieuExecution());
                }
                if (Atexo_Module::isEnabled('AffichageCodeCpv')) {
                    $consultation->setCodeCpv1($contrat->getCodeCpv1());
                    $consultation->setCodeCpv2($contrat->getCodeCpv2());
                }

                if (!Atexo_Config::getParameter('AFFICHAGE_DONNEES_COMPLEMENTAIRES_PERSONNALISEE')) {
                    $contratsTitulaire = (new Atexo_Consultation_Contrat())->getContratTitulaireByIdContratMulti($id);
                    if (is_array($contratsTitulaire) && count($contratsTitulaire)) {
                        $commentaire = Prado::Localize('CONTRACTANT_S') . "\n";
                        foreach ($contratsTitulaire as $contratTitulaire) {
                            $commentaire .= "\n" . $contratTitulaire->getNomTitulaireContrat();
                            $codePostal = $contratTitulaire->getCodePostalEtablissementContrat();
                            $ville = $contratTitulaire->getVilleEtablissementContrat();
                            if ($codePostal || $ville) {
                                $commentaire .= ' (' . $codePostal .
                                    (($codePostal && $ville) ? ' - ' : '') .
                                    $ville . ')';
                            }
                        }
                    } else {
                        $commentaire = Prado::Localize('CONTRACTANT_S') . "\n";
                        $commentaire .= "\n" . $contrat->getNomTitulaireContrat();
                        $codePostal = $contrat->getCodePostalEtablissementContrat();
                        $ville = $contrat->getVilleEtablissementContrat();
                        if ($codePostal || $ville) {
                            $commentaire .= ' (' . $codePostal .
                                (($codePostal && $ville) ? ' - ' : '') .
                                $ville . ')';
                        }
                    }
                    $consultation->setChampSuppInvisible($commentaire);
                }
            }
        }
    }

    /**
     * Permet de charger les type de procedure by type de contrat.
     *
     * @param int $typeMarche type de marche optionel
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function chargerProcedureByTypeContrat($typeMarche = null)
    {
        $dataProcedures = [];
        $dataProcedures[0] = '---' . Prado::localize('DEFAULT_PROCEDURE_TYPE_SELECTED') . '---';
        if (!$typeMarche) {
            $typeMarche = $this->contratType->getSelectedValue();
        }
        $organisme = Atexo_CurrentUser::getOrganismAcronym();
        $proceduresTypes = Atexo_Consultation_ProcedureType::retrieveProcedureTypeByTypeContrat($typeMarche, $organisme);
        if (is_array($proceduresTypes)) {
            foreach ($proceduresTypes as $procedureType) {
                if (false === $procedureType->getProcedureSimplifie()) {
                    if (
                        Atexo_CurrentUser::hasHabilitation('GererMapaInferieurMontant')
                        && 1 == $procedureType->getMapa()
                        && 1 == $procedureType->getActiverMapa()
                        && $procedureType->getIdMontantMapa() == Atexo_Config::getParameter('MONTANT_MAPA_INF_90')
                    ) {
                        $dataProcedures[$procedureType->getIdTypeProcedure()] = Atexo_Util::atexoHtmlEntitiesDecode($procedureType->getLibeleTypeProcedureTraduit());
                    }
                    if (
                        Atexo_CurrentUser::hasHabilitation('GererMapaSuperieurMontant')
                        && 1 == $procedureType->getMapa()
                        && 1 == $procedureType->getActiverMapa()
                        && $procedureType->getIdMontantMapa() == Atexo_Config::getParameter('MONTANT_MAPA_SUP_90')
                    ) {
                        $dataProcedures[$procedureType->getIdTypeProcedure()] = Atexo_Util::atexoHtmlEntitiesDecode($procedureType->getLibeleTypeProcedureTraduit());
                    }
                    if (Atexo_CurrentUser::hasHabilitation('AdministrerProceduresFormalisees') && 0 == $procedureType->getMapa()) {
                        $dataProcedures[$procedureType->getIdTypeProcedure()] = Atexo_Util::atexoHtmlEntitiesDecode($procedureType->getLibeleTypeProcedureTraduit());
                    }
                }
            }
        }
        $this->procedureType->Datasource = $dataProcedures;
        $this->procedureType->dataBind();
    }

    /**
     * Permet de recuperer les donnees complementaires de la consultation
     * Si pas de donnees complementaires, cree un nouvel enregistrement en base de donnees.
     *
     * @param $connexion : connexion
     *
     * @return CommonTDonneeComplementaire
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function recupererDonneesComplementaires($connexion = null)
    {
        $logger = Atexo_LoggerManager::getLogger('app');
        try {
            if (empty($connexion)) {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
            }
            $consultation = $this->Page->getConsultation();
            if ($consultation instanceof CommonConsultation) {
                $donnesComplementaitres = null;
                if (!empty($consultation->getIdDonneeComplementaire())) {
                    $donnesComplementaitres = CommonTDonneeComplementairePeer::retrieveByPK($consultation->getIdDonneeComplementaire(), $connexion);
                } else {
                    $donnesComplementaitres = $this->getDonneesComplementaires();
                }
                if (!($donnesComplementaitres instanceof CommonTDonneeComplementaire)) {
                    $donnesComplementaitres = new CommonTDonneeComplementaire();
                }
                $this->setDonneesComplementaires($donnesComplementaitres);

                return $this->getDonneesComplementaires();
            } else {
                $logger->info("Consultation non trouvee ou n'est pas une instance de CommonConsultation : " . $consultation);
            }
        } catch (Exception $e) {
            $logger->error("Erreur recuperation donnees complementaires : \n\nErreur = " . $e->getMessage() . " \n\nTrace:\n " . $e->getTraceAsString() . " \n\n Methode = IdentificationConsultation::recupererDonneesComplementaires");
        }
    }

    /**
     * Permet de gerer l'affichage des blocs "Je souhaite publier un avis depuis cette plate-forme pour cette consultation" et "Je souhaite rédiger les pièces administratives de cette consultation depuis cette plateforme".
     *
     * author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-les-echos
     *
     * @copyright Atexo 2016
     */
    public function gererAffichageBlocSelectionPubEtRedactionPieces()
    {
        $consultation = $this->Page->getConsultation();
        if ($consultation instanceof CommonConsultation) {
            if (null == $consultation->getDonneePubliciteObligatoire() || (null != $consultation->getDonneePubliciteObligatoire() && '1' == $consultation->getDonneePubliciteObligatoire())) {
                $this->intitulePubliciteOui->setChecked(true);
                $this->Page->scriptJs->Text .= '<script>activerBlocsDonneesComptePublicite();</script>';
                $clientId = $this->intitulePubliciteOui->getClientId();
            } else {
                $this->intitulePubliciteNon->setChecked(true);
                $this->Page->scriptJs->Text .= '<script>desactiverBlocsDonneesComptePublicite();</script>';
                $clientId = $this->intitulePubliciteNon->getClientId();
            }
            if (Atexo_Module::isEnabled('Publicite')) {
                $script = "actionChoixPubliciteRedac('" .$clientId . "');";
                $script .= " cocherDecocherBoutonDonneesOrmeOui('" . $clientId. "');" ;
                $this->scriptIntitulePublicite->Text = "<script>" . $script . "</script>";
            }
            if (Atexo_Module::isEnabled('InterfaceModuleRsem')) {
                if ($this->getConsultation()->getDonneeComplementaireObligatoire()) {
                    $this->intituleRedactionOui->setChecked(true);
                    $this->Page->scriptJs->Text .= "<script>cocherDecocherBoutonDonneesOrmeOui('" . $this->intituleRedactionOui->getClientId() . "');</script>";
                } else {
                    $this->intituleRedactionNon->setChecked(true);
                    $this->Page->scriptJs->Text .= "<script>cocherDecocherBoutonDonneesOrmeOui('" . $this->intituleRedactionNon->getClientId() . "');</script>";
                }
            }
        }
    }

    public function isPubAutorise()
    {
        return $this->intitulePubliciteOui->getChecked();
    }

    public function chargerCodeCpv($consultation)
    {
        $procEquivalence = null;
        if ($consultation instanceof CommonConsultation && $consultation->getIdTypeProcedureOrg()) {
            $procEquivalence = Atexo_Consultation_ProcedureEquivalence::retrieveProcedureEquivalenceById($consultation->getIdTypeProcedureOrg());
        } elseif ($this->procedureType->getSelectedValue()) {
            $procEquivalence = Atexo_Consultation_ProcedureEquivalence::retrieveProcedureEquivalenceById($this->procedureType->getSelectedValue());
        }
        $this->gererAffichageCodeCpv($procEquivalence);
    }

    public function gererAffichageCodeCpv($procEquivalence)
    {
        $this->referentielCPV->setCodeCpvObligatoire(false);
        $this->enabledCodeCPV->setValue('0');
        if (
            Atexo_Module::isEnabled('CodeCpv')
            && Atexo_Module::isEnabled('AffichageCodeCpv')
        ) {
            $this->referentielCPV->setCodeCpvObligatoire(true);
            $this->enabledCodeCPV->setValue('1');
        }


        if ($procEquivalence instanceof CommonProcedureEquivalence) {
            ///Gérer code CPV
            if (
                Atexo_Module::isEnabled('AffichageCodeCpv')
                    && $procEquivalence->getAfficherCodeCpv()
            ) {
                $this->referentielCPV->setAfficherCodeCpv(true);
            } else {
                $this->referentielCPV->setAfficherCodeCpv(false);
            }

            if (
                Atexo_Module::isEnabled('AffichageCodeCpv')
                    && Atexo_Module::isEnabled('CodeCpv')
                    && $procEquivalence->getAfficherCodeCpv()
                    && $procEquivalence->getCodeCpvObligatoire()
            ) {
                $this->referentielCPV->setCodeCpvObligatoire(true);
                $this->enabledCodeCPV->setValue('1');
            } else {
                $this->referentielCPV->setCodeCpvObligatoire(false);
                $this->enabledCodeCPV->setValue('0');
            }
        }
    }

    public function disableBlocSelectPublicationPublicite(CommonConsultation $consultation): bool
    {
        $procEquivalence = Atexo_Consultation_ProcedureEquivalence::retrieveProcedureEquivalenceById(
            $consultation->getIdTypeProcedureOrg()
        );
        if (
            $procEquivalence instanceof CommonProcedureEquivalence
            && '0' === $procEquivalence->getAutoriserPublicite()
        ) {
            return false;
        }

        return true;
    }

    public function buildPubliciteChoiceBloc(string $idProcedureType): void
    {
        $procEquivalence = Atexo_Consultation_ProcedureEquivalence::retrieveProcedureEquivalenceById(
            $this->procedureType->getSelectedValue(),
            $this->getOrganisme()
        );

        if ($procEquivalence instanceof CommonProcedureEquivalence) {
            $this->renderIntitulePublicite('intitulePubliciteNon', $procEquivalence->getAutoriserPubliciteNon());
            $this->renderIntitulePublicite('intitulePubliciteOui', $procEquivalence->getAutoriserPubliciteOui());
            if ($this->intitulePubliciteNon->getChecked()) {
                $this->Page->scriptJs->Text = '<script>desactiverOngletDonneesComplementaires();desactiverOngletPublicite();</script>';
            } else {
                $this->Page->scriptJs->Text = '<script>activerOngletDonneesComplementaires();activerOngletPublicite();</script>';
            }
        }
    }

    public function renderIntitulePublicite(string $htmlElementId, string $autoriserPubliciteValue): void
    {
        switch (true) {
            case ('+1' === $autoriserPubliciteValue):
                $this->$htmlElementId->Visible = true;
                $this->$htmlElementId->setChecked(true);
                $this->$htmlElementId->enabled = true;
                break;
            case ('1' === $autoriserPubliciteValue):
                $this->$htmlElementId->Visible = true;
                $this->$htmlElementId->setChecked(true);
                $this->$htmlElementId->enabled = false;
                break;
            case ('-1' === $autoriserPubliciteValue):
                $this->$htmlElementId->Visible = false;
                $this->$htmlElementId->setChecked(true);
                $this->$htmlElementId->enabled = true;
                break;
            case ('+0' === $autoriserPubliciteValue):
                $this->$htmlElementId->Visible = true;
                $this->$htmlElementId->setChecked(false);
                $this->$htmlElementId->enabled = true;
                break;
            case ('0' === $autoriserPubliciteValue):
                $this->$htmlElementId->Visible = true;
                $this->$htmlElementId->setChecked(false);
                $this->$htmlElementId->enabled = false;
                break;
            case ('-0' === $autoriserPubliciteValue):
                $this->$htmlElementId->Visible = false;
                $this->$htmlElementId->setChecked(false);
                $this->$htmlElementId->enabled = true;
                break;
        }
    }

    /**
     * @param int $idProcedure
     */
    public function displayTypeFormulaireDume(int $idProcedure): void
    {
        $procEquivalence = Atexo_Consultation_ProcedureEquivalence::retrieveProcedureEquivalenceById($idProcedure);
        $typeProcedure = $procEquivalence->getTypeProcedureDume();

        $this->panelTypeProcedureDume->Visible = true;
        if ($typeProcedure === '-0' || $typeProcedure === '-1') {
            $this->panelTypeProcedureDume->Visible = false;
        }
    }

    public function displayGroupBuyers(): void
    {
        if (Atexo_Module::isEnabled('Publicite'))
        {
            $this->displayBlockElementRelatedPub('buyersBlock');
        }
    }

    public function displayProcedureOpenClosed(): void
    {
        if ($this->getViewState('procedureTypeOrg') && Atexo_Module::isEnabled('Publicite')) {
            $this->displayBlockElementRelatedPub('procedureOpenClosedBlock');
            $procedureTypeOrg = ($this->getViewState('procedureTypeOrg'));
            if ($procedureTypeOrg->getMapa() == 0) {
                $this->procedureOpenClosedBlock->style = 'display:none';
                $this->procedureOpenClosedBlock->setEnabled(false);
                $this->mapaProcedureTypeOrg->Value = 0;
            } else {
                $this->procedureOpenClosedBlock->style = 'display:block';
                $this->procedureOpenClosedBlock->setEnabled(true);
                $this->mapaProcedureTypeOrg->Value = 1;
            }
        }
    }

    public function displayBlockElementRelatedPub(string $blockElement): void
    {
        if (true == $this->intitulePubliciteOui->getChecked()) {
            $this->$blockElement->style = 'display:block';
            $this->$blockElement->setEnabled(true);
        } else {
            $this->$blockElement->style = 'display:none';
            $this->$blockElement->setEnabled(false);
        }
    }

    /**
     * @param object $inputRadio
     * @return void
     */
    protected function groupBuyersSelected(object $inputRadio): void
    {
        $valueInput = $this->radioButtonSelected($inputRadio);
        $this->setViewState('groupBuyers', $valueInput);
    }

    /**
     * @param object $inputRadio
     * @return void
     */
    protected function procedureOpenClosedSelected(object $inputRadio): void
    {
        $valueInput = $this->radioButtonSelected($inputRadio);
        $this->setViewState('procedureOpenClosed', $valueInput);
    }

    /**
     * Récupère la valeur du Radio Button selectionné
     *
     * @param object $inputRadio
     * @return string|null
     */
    private function radioButtonSelected(object $inputRadio): ?string
    {
        $indices = $inputRadio->SelectedIndices;
        $item = $inputRadio->Items[$indices[0]];

        return $item ? $item->Value : null;
    }


    /**
     * Traduit du texte qui pour une raison inconnu n'est pas en FR dans le .tpl
     *
     * @return void
     */
    private function traductionSpecificText(): void
    {
        $idTextToTranslate = [
            'groupBuyersYes' => 'DEFINE_OUI',
            'groupBuyersNo' => 'DEFINE_NON',
            'groupProcedureOpen' => 'PROCEDURE_OUVERTE',
            'groupProcedureClosed' => 'PROCEDURE_RESTREINTE'
        ];

        foreach ($idTextToTranslate as $id => $trans) {
            $this->$id->Text = Prado::localize($trans);
        }
    }

    public function getProcedureOrganisme()
    {
        $proceduresTypes = Atexo_Consultation_ProcedureType::retrieveProcedureType();
        $this->setViewState('procedureTypeOrg', $proceduresTypes[$this->procedureType->getSelectedValue()]);
    }

    /**
     * @param int $idProcedure
     * @return void
     *  méthode qui permet de de setter `OUI` pour les données REDAC (dans l'onglet Données complémentaires)
     * par rapport a la configuration du type de procèdure.
     */
    public function checkedDonneesComplementaireOui(int $idProcedure): void
    {
        $procEquivalence = Atexo_Consultation_ProcedureEquivalence::retrieveProcedureEquivalenceById($idProcedure);
        $donneesComplementaireOui = $procEquivalence->getDonneesComplementaireOui();

        if (
            '+1' === $donneesComplementaireOui
            || '1' === $donneesComplementaireOui
            || '-1' === $donneesComplementaireOui
        ) {
            $this->Page->bloc_etapeDonneesComplementaires->donneeOrmeOui->Checked = true;
        }
    }

    public function setDataTypageJo(): array
    {
        /** @var ConsultationTags $service */
        $service = Atexo_Util::getSfService(ConsultationTags::class);
        $tags = $service->getReferentielTags();

        $data = ['consultationId' => $this->getConsultation()->getId(), 'dataTag' => []];

        foreach ($tags as $tag) {
            if ($tag['code'] == 'typageJo') {
                $data['dataTag'] = $tag;
                break;
            }
        }

        return $data;
    }
}
