<?php

namespace Application\Controls;

use Application\Library\Propel\Propel;
use Application\Pages\Entreprise\EntrepriseInscriptionUsers;
use Application\Propel\Mpe\CommonInscrit;
use Application\Propel\Mpe\CommonInscritPeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Entreprise;
use Application\Service\Atexo\Atexo_InscritOperationsTracker;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Inscrit;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_InscritHistorique;
use Exception;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * Page de creation compte entreprise et compte utilisateur.
 *
 * @author Mouslim MITALI <anis.abid@atexo.com>
 * @copyright Atexo 2017
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class AtexoModalGestionUtilisateurs extends MpeTTemplateControl
{
    public bool $visible = false;
    public string $message = '';
    public string $_calledFrom = '';

    private $_inscrit;

    /**
     * @return mixed
     */
    public function getVisible($checkParents = true)
    {
        return $this->visible;
    }

    /**
     * @param mixed $visible
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;
    }

    public function updateAction()
    {
        if (!is_object($this->_inscrit)) {
            $this->_inscrit = (new Atexo_Entreprise_Inscrit())->retrieveInscritById(Atexo_CurrentUser::getIdInscrit());
        }

        $dateDebAction = date('Y-m-d H:i:s');
        $this->panelMessageErreur->setVisible(false);
        //$this->PanelInscrit->setPostBack($this->IsPostBack);
        $this->PanelInscrit->CaseLogin->Checked = true;
        //$this->PanelInscrit->inscritSimple->Checked=true;
        if ('ChangingUser' == $_GET['action'] || 'update' == $_GET['action']) {
            $isOurInscrit = false;
            $usersCurrentCompany = (new Atexo_Entreprise_Inscrit())->retrieveInscritByIdCompnay(Atexo_CurrentUser::getIdEntreprise());
            if ($usersCurrentCompany) {
                foreach ($usersCurrentCompany as $oneInscrit) {
                    if ($oneInscrit->getId() == $_GET['id']) {
                        $isOurInscrit = true;
                    }
                }
                if (true === $isOurInscrit) {
                    $this->_inscrit = (new Atexo_Entreprise_Inscrit())->retrieveInscritById(Atexo_Util::atexoHtmlEntities($_GET['id']));
                    $this->PanelInscrit->setInscrit($this->_inscrit);
                } else {
                    $this->PanelInscrit->setVisible(false);
                    $this->panelMessageErreur->setVisible(true);
                    $this->panelMessageErreur->setMessage(Prado::localize('DEFINE_ERREUR_ACCEE'));
                }
            }
        } elseif ('creationByATES' == $_GET['action']) {
            $company = (new Atexo_Entreprise())->retrieveCompanyBdeById(Atexo_CurrentUser::getIdEntreprise());
            //if (!$this->IsPostBack) {
            $entrepriseInsUser = new EntrepriseInscriptionUsers();
            $companyVo = $entrepriseInsUser->fillEntrepriseBdeObject($company);
            //}
        }
        Atexo_InscritOperationsTracker::trackingOperations(Atexo_CurrentUser::getIdInscrit(), Atexo_CurrentUser::getIdEntreprise(), $_SERVER['REMOTE_ADDR'], date('Y-m-d'), $dateDebAction, substr($_SERVER['QUERY_STRING'], 0), '-', date('Y-m-d H:i:s'));
    }

    public function doUpdate()
    {
        $nom = null;
        $mail = null;
        if ($this->IsValid) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $inscrit = CommonInscritPeer::retrieveByPK($this->_inscrit->getId(), $connexion);
            $this->setViewState('profilInit', $inscrit->getProfil());
            $action = 0;
            if ($inscrit) {
                $inscritUpdeted = $this->PanelInscrit->updateCompte($inscrit);

                if ($inscritUpdeted) {
                    try {
                        $result = $inscritUpdeted->Save($connexion);

                        if ($result >= 0) {
                            //print_r($inscritUpdeted);exit;
                            $nom = $inscritUpdeted->getPrenom().' '.$inscritUpdeted->getNom();
                            $mail = $inscritUpdeted->getEmail();
                            (new Atexo_Entreprise_InscritHistorique())->setHistorique($nom, $mail, Atexo_Config::getParameter('EVENEMENT_ADMIN_UPDATE'));
                        }
                        $profilInit = $this->getViewState('profilInit');
                        $newProfil = $inscritUpdeted->getProfil();
                        //echo 'Init='.$profilInit.'/new='.$newProfil;exit;
                        if ($profilInit != $newProfil) {
                            if ('2' == $newProfil) {
                                $action = Atexo_Config::getParameter('TEXT_INSCRIPTION_ADMIN');
                            } else {
                                $action = Atexo_Config::getParameter('TEXT_INSCRIPTION_UES');
                            }
                            (new Atexo_Entreprise_InscritHistorique())->setHistorique($nom, $mail, $action);
                        }

                        return $result;
                    } catch (Exception $e) {
                        Prado::log('Erreur Modification Compte Inscrit '.$e->__toString().'-', TLogger::FATAL, 'Fatal');

                        return -3;
                    }
                } else {
                    return -3;
                }
            } else {
                return -3;
            }
        }

        return -1;
    }

    public function doAddInscrit()
    {
        if ($this->IsValid) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $newInscrit = $this->PanelInscrit->setNewInscrit();
            $newInscrit->setEntrepriseId(Atexo_CurrentUser::getIdEntreprise());

            if ($this->PanelInscrit->adminEntreprise->Checked) {
                $newInscrit->setProfil('2');
            } else {
                $newInscrit->setProfil('1');
            }
            $entreprise = (new Atexo_Entreprise())->retrieveCompanyBdeById(Atexo_CurrentUser::getIdEntreprise());

            $msg = Atexo_Message::getMessageConfirmationInscrit($newInscrit, $entreprise);

            $objet = Prado::Localize('DEFINE_TITRE_MAIL_CREATION_COMPTE_UTILISATEUR');
            $msg = '<p>'.str_replace("\n", '</p><p>', $msg).'</p>';
            (new Atexo_Message())->envoiMailsNotificationSuppressionCreationCompteEntreprise($newInscrit, $objet, $msg);

            $result = $newInscrit->save($connexion);

            //Sauvegarde de l'historique
            $this->saveHistorique($newInscrit);

            if ($result >= 0) {
                return $result;
            }

            return -2;
        }

        return -1;
    }

    /**
     * Action Boutton Enregister.
     */
    public function saveChange($sender, $param)
    {
        $result = null;
        if (!$this->_inscrit) {
            $this->_inscrit = (new Atexo_Entreprise_Inscrit())->retrieveInscritById(Atexo_CurrentUser::getIdInscrit());
        }
        $this->PanelInscrit->saveEtablissements();
        if ('ChangingUser' == $_GET['action']) {
            $result = self::doUpdate();
        } elseif ('creationByATES' == $_GET['action']) {
            $result = self::doAddInscrit();
        }

        if ($result >= 0) {
            $this->labelClose->Text = '<script>closePopUpUpdateUser();</script>';
        } else {
            if (-3 == $result) {
                $this->panelMessageErreur->setVisible(true);
                $this->panelMessageErreur->setMessage(Prado::localize('TEXT_ERREUR_MODIFICATION_COMPTE'));
            }
            if (-2 == $result) {
                $this->panelMessageErreur->setVisible(true);
                $this->panelMessageErreur->setMessage(Prado::localize('TEXT_ERREUR_CREATION_COMPTE_INSCRIT'));
            }
        }
    }

    /**
     * Permet de sauvegarder l'historique d'un nouvel utilisateur inscrit.
     *
     * @param CommonInscrit $inscrit: objet inscrit
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function saveHistorique($inscrit, $inscrit1 = null, $mail1 = null, $idInscrit1 = null, $idEnt = null)
    {
        if ($inscrit instanceof CommonInscrit) {
            if ($inscrit->getProfil() == Atexo_Config::getParameter('PROFIL_INSCRIT_ATES')) {
                $action = Atexo_Config::getParameter('TEXT_INSCRIPTION_ADMIN');
            } else {
                $action = Atexo_Config::getParameter('TEXT_INSCRIPTION_UES');
            }
            $inscrit2 = $inscrit->getPrenom().' '.$inscrit->getNom();
            $email2 = $inscrit->getEmail();
            (new Atexo_Entreprise_InscritHistorique())->setHistorique($inscrit2, $email2, $action, $inscrit1, $mail1, $idInscrit1, $idEnt);
        }
    }

    /**
     * Permet de mettre à jour l'historique.
     *
     * @param CommonInscrit $inscrit: objet inscrit
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function updateHistorique($inscrit)
    {
        if ($inscrit instanceof CommonInscrit) {
            $action = Atexo_Config::getParameter('EVENEMENT_ADMIN_UPDATE');
            $inscrit2 = $inscrit->getPrenom().' '.$inscrit->getNom();
            $email2 = $inscrit->getEmail();
            (new Atexo_Entreprise_InscritHistorique())->setHistorique($inscrit2, $email2, $action);
        }
    }

    /**
     * Permet de valider la selection de l'etablissement
     * Verifie si au moins 1 etablissement a ete selectionne
     * Sinon affiche un message d'erreur.
     *
     * @param $sender
     * @param $param
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function validerListeEtablissements($sender, $param)
    {
        $siretMonEtablissement = $this->PanelInscrit->etablissements->getSiretMonEtablissement();
        if (null == $siretMonEtablissement || empty($siretMonEtablissement)) {
            $this->labelServerSide->Text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";
            $param->IsValid = false;
            $this->validateurListeEtablissements->ErrorMessage = Prado::localize('MESSAGE_VOUS_DEVEZ_SELECTIONNER_AU_MOINS_UN_ETABLISSEMENT');

            return;
        }
    }
}
