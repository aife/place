<div class="line"  <%=($this->getStyle())? 'style="' . $this->getStyle() . '"' : '' %>>
	<div class="file-info" <%=($this->getAfficherProgress())?'' : 'style="display:none"' %>>
		<com:TLabel id="fileName" style="display:none"/><com:TLabel id="fileSize" style="display:none" />
		<com:TLabel id="fileErreur" style="display:none" Attributes.style="color:red" />
		<div id="<%=$this->getClientId()%>_progressNumber"></div> 
		<progress id="<%=$this->getClientId()%>_prog" value="0" max="100.0" style="display:none"></progress>
	</div> 
	<div class="bloc-file-upload">
		<a href="javascript:;" class="ajout-file" >
		<com:AtexoFileUpload 
			id="file"
			Cssclass="<%=$this->getCssClass()%>" 
			Attributes.title="<%=$this->getTitre()%>" 
			Attributes.size="<%=$this->getSize()%>" 
			Attributes.OnChange="fileSelected('<%=$this->getClientId()%>');uploadFile('<%=$this->getClientId()%>','<%=$this->getFonctionJs()%>','<%=$this->getUrlServeur()%>');" 
			/>
			<com:TPanel  style="display:<%=($this->getAfficherImageBoutton())? '' : 'none' %>">
			<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-ajout-element.gif" alt="Ajouter" /><%=$this->getTitre()%></com:TPanel>
		</a>
	</div> 
</div>
<com:TActiveHiddenField ID="NomFichier" />
<com:TActiveHiddenField id="reponseServeur" />
<com:TActiveHiddenField ID="hasFile" />
