<?php

namespace Application\Controls;

use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonTDonneeComplementaire;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;

/**
 * Page de gestion des echantillons demandés à la consultation.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 4
 */
class EchantillonsDemandesConsultation extends MpeTTemplateControl
{
    private $_afficher;
    private $_donneesComplementaires;
    private ?string $_langue = null;
    private $_validationGroup;
    private $_validationSummary;
    private ?string $_sourcePage = null;
    private bool|string $_actif = true;

    /**
     * recupère la valeur de afficher.
     */
    public function getAfficher()
    {
        if (null === $this->_afficher) {
            $this->_afficher = true;
        }

        return $this->_afficher;
    }

    // getAfficher()

    /**
     * Affecte la valeur de afficher.
     *
     * @param string $afficher
     */
    public function setAfficher($afficher)
    {
        if ($this->_afficher !== $afficher) {
            $this->_afficher = $afficher;
        }
    }

    // setAfficher()

    /**
     * recupère l'objet données complémentaires.
     */
    public function getDonneesComplementaires()
    {
        return $this->_donneesComplementaires;
    }

    // getDonneesComplementaires()

    /**
     * Affecte l'objet données complémentaires.
     *
     * @param CommonTDonneesComplementaires $donneesComplementaires
     */
    public function setDonneesComplementaires($donneesComplementaires)
    {
        if ($this->_donneesComplementaires !== $donneesComplementaires) {
            $this->_donneesComplementaires = $donneesComplementaires;
        }
    }

    // setDonneesComplementaires()

    /**
     * recupère la langue.
     */
    public function getLangue()
    {
        return $this->_langue;
    }

    // getLangue()

    /**
     * Affecte la langue.
     *
     * @param string $langue
     */
    public function setLangue($langue)
    {
        if ($this->_langue !== $langue) {
            $this->_langue = $langue;
        }
    }

    // setLangue()

    /**
     * Affecte le groupe de validation.
     *
     * @param $validationGroup
     */
    public function setValidationGroup($validationGroup)
    {
        $this->_validationGroup = $validationGroup;
    }

    /**
     * Récupère le groupe de validation.
     */
    public function getValidationGroup()
    {
        return $this->_validationGroup;
    }

    /**
     * Affecte le composant.
     *
     * @param $validationSummary
     */
    public function setValidationSummary($validationSummary)
    {
        $this->_validationSummary = $validationSummary;
    }

    /**
     * Récupère le composant.
     */
    public function getValidationSummary()
    {
        return $this->_validationSummary;
    }

    /**
     * recupère la valeur.
     */
    public function getSourcePage()
    {
        return $this->_sourcePage;
    }

    // getSourcePage()

    /**
     * Affecte la valeur.
     *
     * @param string $value
     */
    public function setSourcePage($value)
    {
        if ($this->_sourcePage !== $value) {
            $this->_sourcePage = $value;
        }
    }

    // setSourcePage()

    /**
     * recupère la valeur.
     */
    public function getActif()
    {
        return $this->_actif;
    }

    // getActif()

    /**
     * Affecte la valeur.
     *
     * @param string $value
     */
    public function setActif($value)
    {
        if ($this->_actif !== $value) {
            $this->_actif = $value;
        }
    }

    // setActif()

    /*
     * Chargement du composant
     */
    public function onLoad($param)
    {
        $this->chargerComposant();
    }

    /**
     * Permet de charger le composant.
     */
    public function chargerComposant($recharger = false)
    {
        $this->customize();
        if (Atexo_Module::isEnabled('ConsultationEchantillonsDemandes') && $this->getAfficher()) {
            $this->afficherComposant();
        } else {
            $this->masquerComposant();
        }

        if (!$this->Page->isPostBack) {
            if (Atexo_Module::isEnabled('ConsultationEchantillonsDemandes')
                && $this->getDonneesComplementaires() instanceof CommonTDonneeComplementaire
                    && $this->getDonneesComplementaires()->getEchantillon()) {
                $this->echantillonOui->Checked = true;
                $this->addEchantillion->Text = (new Atexo_Util())->toHttpEncoding($this->getDonneesComplementaires()->getAddEchantillonTraduit());
                $this->dateLimiteEchantillion->Text = Atexo_Util::iso2frnDateTime($this->getDonneesComplementaires()->getDateLimiteEchantillon());
                if ($this->getAfficher()) {
                    $this->echantillonLayer->setDisplay('Dynamic');
                }
            }
        }
    }

    /**
     * Permet de gerer la visibilité.
     */
    public function customize()
    {
        if (Atexo_Module::isEnabled('ConsultationEchantillonsDemandes')) {
            $this->panelEchantillonsDemandes->visible = true;
            $this->echantillonLayer->setDisplay('None');
            $this->validatorEchantillion->setEnabled(true);
            $this->validatorEchantillionCompare->setEnabled(true);
        } else {
            $this->panelEchantillonsDemandes->visible = false;
            $this->echantillonLayer->setDisplay('None');
            $this->validatorEchantillion->setEnabled(false);
            $this->validatorEchantillionCompare->setEnabled(false);
        }
    }

    /**
     * Permet d'afficher les échantillons demandés.
     */
    public function afficherComposant()
    {
        $this->panelEchantillonsDemandes->setDisplay('Dynamic');
    }

    /**
     * Permet de masquer les échantillons demandés.
     */
    public function masquerComposant()
    {
        $this->panelEchantillonsDemandes->setDisplay('None');
    }

    /**
     * Permet de sauvegarder les données d'echantillon.
     *
     * @param CommonTDonneeComplementaire $donneesComplementaires,CommonConsultation $consultation la consultation
     * @param int                         $lot                                       le numero du lot,Objet connexion $connexion, Boolean $modification
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function save(&$donneesComplementaires, $consultation, $lot, $connexion, $modification = true)
    {
        if (Atexo_Module::isEnabled('ConsultationEchantillonsDemandes')) {
            $oldAddEchantillion = $donneesComplementaires->getAddechantillon();
            $oldDateLimiteEchantillion = $donneesComplementaires->getDatelimiteechantillon();
            $addEchantillion = '';
            $dateLimiteEchantillion = '';
            $echantillion = '0';
            if ($this->echantillonOui->Checked) {
                $echantillion = '1';
                $addEchantillion = $this->addEchantillion->Text;
                $dateLimiteEchantillion = Atexo_Util::frnDateTime2iso($this->dateLimiteEchantillion->Text);
            }
            $donneesComplementaires->setEchantillon($echantillion);
            $donneesComplementaires->setAddechantillon($addEchantillion);
            $donneesComplementaires->setDatelimiteechantillon(($dateLimiteEchantillion));
            $donneesComplementaires->setAddEchantillonTraduit($this->getLangue(), $addEchantillion);
            $this->saveHistoriqueEchantillon(
                $oldAddEchantillion,
                $oldDateLimiteEchantillion,
                $consultation,
                $donneesComplementaires,
                $lot,
                $connexion,
                Atexo_CurrentUser::getCurrentOrganism(),
                $modification
            );
        }
    }

    /**
     * Permet de sauvegarder l'historique echantillon.
     *
     * @param string                      $oldAddEchantillion             l'ancienne adresse,DataeTime $oldDateReunion l'ancienne date,CommonConsultation $consultation la consultation
     * @param CommonTDonneeComplementaire $donneesComplementaires,Integer $lot le numero du lot,Objet connexion $connexion,String $org l'acronyme de l'organisme, Boolean $modification
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function saveHistoriqueEchantillon($oldAddEchantillion, $oldDateLimiteEchantillion, $consultation, $donneesComplementaires, $lot, $connexion, $org, $modification = true)
    {
        $arrayParameters = [];
        if ($oldAddEchantillion != $donneesComplementaires->getAddechantillon() || $oldDateLimiteEchantillion != $donneesComplementaires->getDatelimiteechantillon() || !$modification) {
            $nomElement = Atexo_Config::getParameter('ID_ECHANTILLONS');
            $valeur = '0';
            $detail1 = '';
            $detail2 = '';
            if ($donneesComplementaires->getAddechantillon() && $donneesComplementaires->getDatelimiteechantillon() && $donneesComplementaires->getEchantillon()) {
                $valeur = '1';
                $detail1 = $donneesComplementaires->getAddechantillon();
                $detail2 = $donneesComplementaires->getDatelimiteechantillon();
            }
            if ($modification) {
                $statut = Atexo_Config::getParameter('MODIFICATION_FILE');
            } else {
                $statut = Atexo_Config::getParameter('CREATION_FILE');
            }
            //Sauvegarde
            $arrayParameters[0] = $consultation->getId();
            $arrayParameters[1] = $nomElement;
            $arrayParameters[2] = $valeur;
            $arrayParameters[3] = $detail1;
            $arrayParameters[4] = $detail2;
            $arrayParameters[5] = $lot;
            $arrayParameters[6] = $org;
            $arrayParameters[7] = $statut;
            (new Atexo_Consultation())->saveHistoriqueConsultation($arrayParameters, $connexion);
        }
    }
}
