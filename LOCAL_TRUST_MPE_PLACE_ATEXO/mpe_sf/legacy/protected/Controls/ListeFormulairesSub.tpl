<!-- Début formulaire -->	
<div class="form-field" style="<%=($this->getConsultation()->getEnvOffre() || $this->getConsultation()->getEnvCandidature())? 'display:block;':'display:none;'%>">
<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
<com:TActivePanel cssClass="content" ID="BlocdossiersFormulaires">
	<h3><com:TTranslate>DEFINE_PREPARATION_FORMULAIRES</com:TTranslate></h3>
	<p class="infos-warning"><com:TTranslate>DEFINE_MESSAGE_VALIDATION_SOUMISSION_OFFRES</com:TTranslate></p>
	
	<!--Debut Bloc Dossier de candidature-->
	<div class="form-field bloc-pieces formulaires-aof" style="<%=($this->getConsultation()->getEnvCandidature())? 'display:block;':'display:none;'%>">
		<div class="top"><span class="left">&nbsp;</span><span class="title"><com:TTranslate>DEFINE_DOSSIER_CANDIDATURE</com:TTranslate></span><span class="right">&nbsp;</span></div>
		<div class="content">
			<table class="table-results tableau-reponse" summary="<%=Prado::localize('DEFINE_DOSSIER_CANDIDATURE')%> - <%=Prado::localize('DEFINE_LISTE_FORMULAIRE')%>">
				<!--
				<thead>
					<tr class="row-title">
						<th id="formCand_col_01"><com:TTranslate>TEXT_TYPE_FORMULAIRE</com:TTranslate></th>
						<th id="formCand_col_02" class="col-50 center"><com:TTranslate>DEFINE_STATUS</com:TTranslate></th>
						<th id="formCand_col_03" class="actions"><com:TTranslate>TEXT_ACTIONS</com:TTranslate></th>
					</tr>
				</thead>
				-->
				<tbody>
					<com:AtexoFormulaireSub
						id="idCandidatureFormSUB"
						IdHtmlFormulaire="panelDossiersCandidatureFormulaire"
						IdHtlmToggleFormulaire="panelDossiersCandidatureFormulaire"
						ClassFormulaire="on row-title-2 panelDossiersCandidature"
						VerifyCallBack="true"
						callFrom="formulairePreparation"
					/>
				</tbody>
			</table>
			<div class="breaker"></div>
		</div>
	</div>
	<!--Fin Bloc Dossier de candidature-->
		
		<!--Debut Bloc Dossier d'offre-->
		<div class="form-field bloc-pieces formulaires-aof" style="<%=($this->getConsultation()->getEnvOffre())? 'display:block;':'display:none;'%>">
			<div class="top"><span class="left">&nbsp;</span><span class="title"><com:TTranslate>DEFINE_DOSSIER_OFFRES</com:TTranslate></span><span class="right">&nbsp;</span></div>
			<div class="content">
				<table class="table-results tableau-gestion-doc-prepa tableau-reponse tableau-alloti" summary="<%=Prado::localize('DEFINE_DOSSIER_OFFRES')%> - <%=Prado::localize('DEFINE_LISTE_FORMULAIRE')%>">
					<!-- 
					<thead>
						<tr class="row-title">
							<th id="formOffre_col_01"><com:TTranslate>TEXT_TYPE_FORMULAIRE</com:TTranslate></th>
							<th id="formOffre_col_02" class="col-50 center"><com:TTranslate>DEFINE_STATUS</com:TTranslate></th>
							<th id="formOffre_col_03" class="actions"><com:TTranslate>TEXT_ACTIONS</com:TTranslate></th>
						</tr>
					</thead> 
					 -->
					 <tbody>
					<com:TLabel 
						ID="idLabelMessageDossiersOffreCandidatureNonValidee" 
						style="<%=($this->getDossierCandidature()->getStatutValidation() != Application\Service\Atexo\Atexo_Config::getParameter('STATUT_DOSSIER_VALIDE'))? 'display:;':'display:none;'%>"
						Text="<%=Prado::localize('DEFINE_MESSAGE_DOSSIER_OFFRE_CANDIDATURE_NON_VALIDEE')%>"
					/>
					<div class="breaker"></div>
					<!--Debut lot 2-->
						<com:TRepeater ID="consultationOuListeDesLots">
        					<prop:ItemTemplate>
										
								<tr class="row-lot">
									<td headers="formOffre_col_01" class="title" colspan="3" style="<%=($this->Page->getConsultation()->getAlloti())? 'display:;':'display:none;'%>">
										<a href="javascript:void(0);" onclick="showHidePanelClass(this,'panelDossiersOffre_lot_<%#$this->Data['lot']%>');" class="title-toggle-open">
											<com:TLabel id="infosLots" Text="<%=($this->Page->getConsultation()->getAlloti())? Prado::localize('TEXT_LOT').' '.$this->Data['lot']. ' - ' . $this->Data['intitule']:''%>"/>
										</a>
									</td>
									<td headers="formOffre_col_01" class="title" colspan="3" style="<%=(!$this->Page->getConsultation()->getAlloti())? 'display:;':'display:none;'%>"></td>
									<td headers="formOffre_col_03" class="actions">
										<com:TActiveLinkButton 
											onCallBack="SourceTemplateControl.ouvrirPopInSuppressionDossier"
											ActiveControl.CallbackParameter="<%#$this->Data['dossiers'][0]->getIdDossierFormulaire()%>_<%#$this->ItemIndex%>"
											attributes.title="<%#Prado::localize('MESSAGE_CONFIRMATION_SUPPRESSION_DOSSIER')%>"
										>
											<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-supprimer-big.gif" alt="<%=Prado::localize('DEFINE_SUPPRIMER')%>" title="<%=Prado::localize('DEFINE_SUPPRIMER')%>"/>
										</com:TActiveLinkButton>
									</td>
								</tr>
												
									<com:TRepeater ID="listeFormulaires" DataSource="<%#$this->Data['dossiers']%>">
        								<prop:ItemTemplate>
											<com:AtexoFormulaireSub 
												id="idLotAtexoFormulaireSub"
												IdHtmlFormulaire="panelDossiersOffre_lot_<%#$this->Data->getIdLot() . ' panelFormulaireOffre_lot_'.$this->Data->getIdLot()%>"
												IdHtlmToggleFormulaire="panelFormulaireOffre_lot_<%#$this->Data->getIdLot()%>"
												ClassFormulaire="on row-title-2 panelDossiersOffre_lot_<%#$this->Data->getIdLot()%>"
												RefCons="<%#$this->Page->getConsultation()->getId()%>"
												Organisme="<%#$this->Page->getConsultation()->getOrganisme()%>"
												dossier="<%#$this->Data%>"
												listeEditions="<%#Application\Service\Atexo\FormulaireSub\Atexo_FormulaireSub_Dossier::recupererEditionsFormulaire($this->Data->getIdDossierFormulaire())%>"
												callFrom="formulairePreparation"
												idDosssierPere="<%#$this->Data->getIdDossierPere()%>"
											/>
															
										</prop:ItemTemplate>
	        						</com:TRepeater>
										
							</prop:ItemTemplate>
	        			</com:TRepeater>
						<!--Fin lot 2-->
					
					</tbody>
				</table>
				<com:TActiveLinkButton 
					cssClass="ajout-el ajout-offre" 
					Attributes.title="<%=Prado::localize('DEFINE_AJOUTER_UNE_OFFRE')%>"
					Attributes.onClick="openModal('modal-ajout-offre','popup-800',this);"
					style="<%=($this->getDossierCandidature()->getStatutValidation() == Application\Service\Atexo\Atexo_Config::getParameter('STATUT_DOSSIER_VALIDE'))? 'display:;':'display:none;'%>"
				>
					<com:TTranslate>DEFINE_AJOUTER_UNE_OFFRE</com:TTranslate>
				</com:TActiveLinkButton>
				<div class="breaker"></div>
			</div>
		</div>
		<!--Fin Bloc Dossier d'offre-->
		
		<com:TActiveButton  id="buttonRefreshBlocDossier" style="display:none;" onCallBack="raffraichirBlocDossiersFormulaire" />
		
		<div class="breaker"></div>
		<com:THiddenField id="idDossierPere" />
	</com:TActivePanel>
	<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
</div>				
<!-- Fin formulaire -->		

<com:TActiveLabel ID="scriptJsPopInSuppressionDossier" />
			
<!-- Début pop-in -->						
<div aria-labelledby="ui-id-1" role="dialog" tabindex="-1" style="outline: 0px none; z-index: 1002; position: absolute; height: auto; width: 300px; top: -1228px; left: 540.5px; display: block;" class="ui-dialog ui-widget ui-widget-content ui-corner-all modal-form popup-small2 popup-800 ui-draggable">
<div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"><span class="ui-dialog-title" id="ui-id-1"><h2><com:TTranslate>DEFINE_AJOUTER_UNE_OFFRE</com:TTranslate></h2></span><a role="button" class="ui-dialog-titlebar-close ui-corner-all" href="#"><span class="ui-icon ui-icon-closethick"><com:TTranslate>FERMER</com:TTranslate></span></a></div><div scrollleft="0" scrolltop="0" class="modal-ajout-offre ui-dialog-content ui-widget-content" style="width: auto; min-height: 51px; height: auto;">
    <com:TActivePanel Id ="blocAjoutFormulaireOffre" >
	<!--Debut message de erreur-->
		<com:PanelMessage style="display:none" id ="panelMsgErreur"  typeMessage="msg-erreur"/>
	<!--Fin  message de erreur--> 
	<!--Debut Formulaire-->
	<div class="form-bloc">
		<div class="top"><span class="left"></span><span class="right"></span></div>
		<div class="content">
			<com:TPanel id="panelAjoutOffresConsAllotie" cssClass="line" style="<%=($this->Page->getConsultation()->getAlloti())? 'display:;':'display:none;'%>">
				<div class="intitule intitule-120 bold"><label for="listeLots">Lot :</label></div>
				
				<com:TActiveDropDownList CssClass="select-550"
					ID="listeLots" 
					Attributes.title="<%=Prado::localize('DEFINE_AJOUTER_UNE_OFFRE')%>"
				/>
								
			</com:TPanel>
			<com:TPanel id="panelAjoutOffresConsNonAllotie" cssClass="line" style="<%=($this->Page->getConsultation()->getAlloti())? 'display:none;':'display:;'%>">
				<com:TLabel Text="<%=Prado::localize('DEFINE_MESSAGE_AJOUT_OFFRES_CONSULTATION_NON_ALLOTIE')%>"/>
			</com:TPanel>
			<div id="offresBase_variantes" style="display:none;">
				<div class="line">
					<div class="intitule intitule-120 bold">Type :</div>
					<div class="content-bloc check-list">
						<div class="intitule-auto"><input class="check" title="Offre de base" id="offreType_base" name="offreType" checked="checked" onclick="isCheckedHideDiv(this,'panel_offreVariante');" type="radio"><label for="offreType_base">Offre de base</label></div>
						<div class="intitule-auto clear-both"><input class="check" title="Offre variante" id="offreType_variante" name="offreType" onclick="isCheckedShowDiv(this,'panel_offreVariante');" type="radio"><label for="offreType_variante">Offre variante</label></div>
					</div>
				</div>
				<div class="line" id="panel_offreVariante" style="display:none;">
					<div class="intitule intitule-120 bold"><label for="offreVariante_nom">Nom de la variante :</label></div>
					<input name="offreVariante_nom" id="offreVariante_nom" class="long-550">
				</div>
			</div>
		</div>
		<div class="bottom"><span class="left"></span><span class="right"></span></div>
	</div>
	<!--Fin Formulaire-->
	<!--Debut line boutons-->
	<div class="boutons-line">
		<input class="bouton-moyen float-left" value="<%=Prado::localize('DEFINE_ANNULER')%>" title="<%=Prado::localize('DEFINE_ANNULER')%>" onclick="hideDiv('panelMsgErreur');J('.modal-ajout-offre').dialog('close');" type="button">
		<com:TActiveButton cssClass="bouton-moyen float-right" onCallBack="openFormulaireEnveloppe" Text="<%=Prado::localize('DEFINE_SUIVANT')%>" Attributes.title="<%=Prado::localize('DEFINE_SUIVANT')%>" />
	</div>
	<!--Fin line boutons-->
	</com:TActivePanel>
</div>
</div>
<!-- Fin pop-in -->		
<com:TActiveLabel id="scriptOpenFormulaireEnveloppe"  style="display:none;"/>

<!-- Debut pop-in suppression dossier -->	
<com:TActivePanel id="panelRaffraichissementPopInConfirmation">						
	<com:AtexoPopInConfirmation
		Message="<%=Prado::localize('MESSAGE_CONFIRMATION_SUPPRESSION_DOSSIER')%>"
		Modal="modal-suppression-Dossier<%=($this->Page->getViewState('itemIndexDossierFormulaire') != '')? '-'.$this->Page->getViewState('itemIndexDossierFormulaire'):''%>"
		FunctionCallBack="supprimerDossier"
	/>
</com:TActivePanel>	
<!-- Fin pop-in suppression dossier -->				
					