<?php

namespace Application\Controls;

use Application\Propel\Mpe\CommonTContratMulti;
use Application\Propel\Mpe\CommonTContratTitulaire;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Contrat;

/**
 * Page de gestion du bloc "Attriburaire" du contrat.
 *
 * @author Khalid BENAMAR <kbe@atexo.com>
 * @copyright Atexo 2014
 *
 * @version 4
 */
class CartoucheAttributaire extends MpeTTemplateControl
{
    protected $contrat;

    public function onLoad($param)
    {
        if (!$this->Page->getIsPostBack()) {
            $this->remplirChamps();
            if ($this->contrat instanceof CommonTContratTitulaire) {
                $entreprise = $this->contrat->getTitulaireContrat();
                $etablissement = $this->contrat->getTitulaireEtablissementContrat();
                $contact = $this->contrat->getCommonTContactContrat();
                $this->idPopinInfosAttributaire->setEntreprise($entreprise);
                $this->idPopinInfosAttributaire->setEtablissement($etablissement);
                $this->idPopinInfosAttributaire->setContact($contact);
                $this->idPopinInfosAttributaire->setIdOffre($this->contrat->getIdOffre());
                $this->idPopinInfosAttributaire->setRefConsultation($this->contrat->getRefConsultation());
                $this->idPopinInfosAttributaire->setTypeReponse($this->contrat->getTypeDepotReponse());
                $this->idPopinInfosAttributaire->setIdDiv('detail_lot_4_attributaire_1');
            }
        }
    }

    /**
     * Get the value of [Contrat] column.
     *
     * @return CommonContratTitulaire Contrat
     *
     * @author Loubna EZZIANI <kbe@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function getContrat()
    {
        return $this->contrat;
    }

    /**
     * Set the value of [Contrat] column.
     *
     * @param CommonContratTitulaire $v new value
     *
     * @return void
     *
     * @author Loubna EZZIANI <kbe@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function setContrat($contrat)
    {
        $this->contrat = $contrat;
    }

    public function remplirChamps()
    {
        if ($this->contrat instanceof CommonTContratTitulaire) {
            $this->nomEntreprise->Text = $this->contrat->getNomTitulaireContrat();
            $this->adresse->Text = $this->contrat->getCodePostalEtablissementContrat().' '.$this->contrat->getVilleEtablissementContrat();
            if ((new Atexo_Consultation_Contrat())->isTypeContratAvecMontantById($this->contrat->getIdTypeContrat())) {
                $this->panelMontantAttribue->setVisible(true);
                $this->montant->Text = Atexo_Util::getMontantArronditEspace($this->contrat->getMontantContrat());
            } else {
                $this->panelMontantAttribue->setVisible(false);
            }
            if ((new Atexo_Consultation_Contrat())->isTypeContratAvecMontantMaxById($this->contrat->getIdTypeContrat())) {
                $this->panelMontantMaxAttribue->setVisible(true);
                $montantMax = $this->contrat->getMontantMaxEstime();
                if (!$montantMax) {
                    $contratMulti = $this->contrat->getCommonTContratMulti();
                    if ($contratMulti instanceof CommonTContratMulti) {
                        $montantMax = $contratMulti->getMontantMaxEstime();
                    }
                }
                $this->montantMax->Text = Atexo_Util::getMontantArronditEspace($montantMax);
            } else {
                $this->panelMontantMaxAttribue->setVisible(false);
            }
        }
    }
}
