<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CriteresEvaluation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Util;

/*
 * Created on 5 oct. 2011
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */

class AtexoListeCriteres extends MpeTPage
{
    protected $_previousLot;
    protected $_previousEnv;
    protected $_refConsultation;
    protected $_calledFrom;
    protected $_organisme;

    public function onLoad($param)
    {
        if ('agent' == $this->getCalledFrom()) {
            $this->setRefConsultation(base64_decode($_GET['id']));
            $this->setOrganisme(Atexo_CurrentUser::getOrganismAcronym());
        } elseif ('entreprise' == $this->_calledFrom) {
            $this->setRefConsultation($_GET['id']);
            $this->setOrganisme(Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']));
        }

        $this->remplirRepeaterFormsCand();
        $this->remplirRepeaterForms();
    }

    public function setCalledFrom($value)
    {
        $this->_calledFrom = $value;
    }

    public function getCalledFrom()
    {
        return $this->_calledFrom;
    }

    public function setRefConsultation($val)
    {
        $this->_refConsultation = $val;
    }

    public function getRefConsultation()
    {
        return $this->_refConsultation;
    }

    public function setOrganisme($val)
    {
        $this->_organisme = $val;
    }

    public function getOrganisme()
    {
        return $this->_organisme;
    }

    public function remplirRepeaterFormsCand()
    {
        $data = (new Atexo_CriteresEvaluation())->getListeCriteresByTypeEnveloppe($this->getRefConsultation(), $this->getOrganisme(), Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE'), false);
        $this->repeaterFormsCand->DataSource = $data;
        $this->repeaterFormsCand->dataBind();
    }

    public function remplirRepeaterForms()
    {
        $data = (new Atexo_CriteresEvaluation())->getListeCriteresByTypeEnveloppe($this->getRefConsultation(), $this->getOrganisme(), Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE'), true);
        $this->repeaterForms->DataSource = $data;
        $this->repeaterForms->dataBind();
    }

    public function getLibelleLot($lot)
    {
        if ('agent' == $this->getCalledFrom()) {
            return (new Atexo_CriteresEvaluation())->getDescriptionLot($lot, $this->getOrganisme(), $this->getRefConsultation());
        } elseif ('entreprise' == $this->getCalledFrom()) {
            return (new Atexo_CriteresEvaluation())->getDescriptionLot($lot, $this->getOrganisme(), $this->getRefConsultation());
        }
    }

    public function afficherLigneLot($lot)
    {
        if (!$lot) {
            return false;
        }
        if ('' == $this->_previousLot) {
            $this->_previousLot = $lot;

            return true;
        }
        if ('' != $this->_previousLot && $this->_previousLot == $lot) {
            $this->_previousLot = $lot;

            return false;
        }
        if ('' != $this->_previousLot && $this->_previousLot != $lot) {
            $this->_previousLot = $lot;
            $this->_previousEnv = '-1';

            return true;
        }
    }

    public function afficherLigneEnveloppe($env, $lot)
    {
        if ('' == $this->_previousEnv) {
            $this->_previousEnv = $env;

            return true;
        }
        if ('' != $this->_previousEnv && $this->_previousEnv == $env && $this->_previousLot == $lot) {
            $this->_previousEnv = $env;

            return false;
        }
        if ('' != $this->_previousEnv && $this->_previousEnv != $env) {
            $this->_previousEnv = $env;

            return true;
        }
        if ('' != $this->_previousEnv && $this->_previousEnv == $env && $this->_previousLot != $lot) {
            $this->_previousEnv = $env;

            return true;
        }
    }

    public function getPictoStatut($formCons)
    {
        return (new Atexo_CriteresEvaluation())->getPictoStatutForm($formCons);
    }

    public function getTitleStatut($formCons)
    {
        return (new Atexo_CriteresEvaluation())->getTitleStatutForm($formCons);
    }

    public function refreshRepeater($sender, $param)
    {
        $this->_previousEnv = '';
        $this->_previousLot = '';
        $this->remplirRepeaterFormsCand();
        $this->remplirRepeaterForms();
        $this->PanelListeCriteres->render($param->NewWriter);
    }

    public function getUrlVisualisation($critereEval)
    {
        return "javascript:popUp('index.php?page=Agent.PopUpDetailCriteresEvaluation&id=".$critereEval->getId()."','yes');";
    }
}
