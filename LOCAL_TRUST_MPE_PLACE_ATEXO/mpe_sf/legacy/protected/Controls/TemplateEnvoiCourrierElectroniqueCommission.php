<?php

namespace Application\Controls;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonEchange;
use Application\Propel\Mpe\CommonEchangeDestinataire;
use Application\Propel\Mpe\CommonEchangePieceJointe;
use Application\Propel\Mpe\CommonEchangePieceJointePeer;
use Application\Propel\Mpe\CommonOrganisme;
use Application\Propel\Mpe\CommonService;
use Application\Propel\Mpe\CommonTCAOSeance;
use Application\Propel\Mpe\CommonTCAOSeanceQuery;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_GenerationExcel;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Commission\Atexo_Commission_Seance;
use Application\Service\Atexo\Message\Atexo_Message_RelationEchange;
use Prado\Prado;

/**
 * Classe TemplateEnvoiCourrierElectroniqueCommission.
 *
 * @author KONATE Oumar <oumar.konate@atexo.com>
 * @copyright Atexo 2012
 *
 * @since MPE4
 */
class TemplateEnvoiCourrierElectroniqueCommission extends MpeTPage
{
    public bool $_calledFrom = false;
    public $_org;
    public bool $_postBack = false;
    public $_tailleMaxPj;
    public bool $_redirect = false;

    public function setPostBack($value)
    {
        $this->_postBack = $value;
    }

    public function getPostBack()
    {
        return $this->_postBack;
    }

    public function setTailleMaxPj($value)
    {
        $this->_tailleMaxPj = $value;
    }

    public function getTailleMaxPj()
    {
        return $this->_tailleMaxPj;
    }

    public function setOrg($value)
    {
        $this->_org = $value;
    }

    public function getOrg()
    {
        return $this->_org;
    }

    public function setRedirect($value)
    {
        $this->_redirect = $value;
    }

    public function getRedirect()
    {
        return $this->_redirect;
    }

    public function setCalledFrom($value)
    {
        $this->_calledFrom = $value;
    }

    public function getCalledFrom()
    {
        return $this->_calledFrom;
    }

    public function onLoad($param)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        if (!$this->getPostBack()) {
            $this->remplirListeMessage();

            //Recuperation des informations sur la commission et la seance
            $idSeance = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id']));
            $organisme = $this->_org;
            $seance = CommonTCAOSeanceQuery::create()
                ->findPk([$idSeance, $organisme]);
            if($seance instanceof \CommonTCAOSeance){
                $typeCommission = $seance->getCommonTCAOCommission()->getIntitule();
                $dateSeance = Atexo_Util::iso2frnDateTime($seance->getDate());
            }

            //Recuperation des informations sur l'entite publique
            $entitePublique = '';
            if (0 == (int) Atexo_CurrentUser::getIdServiceAgentConnected()) {
                $objetOrganisme = Atexo_Organismes::retrieveOrganismeByAcronyme(Atexo_CurrentUser::getCurrentOrganism());
                if ($objetOrganisme instanceof CommonOrganisme) {
                    $entitePublique = $objetOrganisme->getDenominationOrgTraduit();
                }
            } else {
                $serviceObjet = Atexo_EntityPurchase::retrieveEntityById(Atexo_CurrentUser::getIdServiceAgentConnected(), Atexo_CurrentUser::getCurrentOrganism());
                if ($serviceObjet instanceof CommonService) {
                    $entitePublique = $serviceObjet->getLibelleTraduit();
                }
            }

            $objetMessage = Prado::localize('OBJET_MAIL_CONVOCATION_COMMISSION');
            $objetMessage = str_replace('[__ENTITE_PUBLIQUE__]', $entitePublique, $objetMessage);
            $objetMessage = str_replace('[__DATE_HEURE_DEBUT_COMMISSION__]', $dateSeance, $objetMessage);

            $corps = Prado::localize('CORPS_MAIL_COMMISSION');
            $corps = str_replace('[__TYPE_COMMISSION__]', $typeCommission, $corps);
            $corps = str_replace('[__DATE_HEURE_DEBUT_COMMISSION__]', $dateSeance, $corps);
            if($seance instanceof \CommonTCAOSeance){
                $corps = str_replace('[__NOM_SALLE__]', $seance->getSalle(), $corps);
                $corps = str_replace('[__NOM_LIEU__]', $seance->getLieu(), $corps);
            }

            $corps .= '
		  		
';
            $corps .= Prado::localize('CORPS_MAIL_COMMISSION_SUITE').'.';
            $corps = Prado::localize('CORPS_DEBUT').',
		  	
'.$corps.'
		  	
'.Prado::localize('CORPS_FIN').'.';
            //$this->objet->Text = $objet;
            $this->textMail->Text = $corps;
            $this->objet->Text = $objetMessage;

            $message = new CommonEchange();
            $message->setOrganisme($organisme);
            $message->save($connexion);

            $_ordre_du_jour = (new Atexo_Commission_Seance())->getOrdreDuJour($idSeance, $organisme);
           if($seance instanceof CommonTCAOSeance){
               $tmpFileOrdreDuJour = (new Atexo_GenerationExcel())->telechargerOrdreDuJour($seance, $_ordre_du_jour, $organisme, true);
               $extensionFile = Atexo_Util::getExtension($tmpFileOrdreDuJour);
           }

            $atexoBlob = new Atexo_Blob();
            $nomFichier = Atexo_Config::getParameter('CAO_FICHIER_ORDRE_DU_JOUR');
            $echangePj = new CommonEchangePieceJointe();
            $echangePj->setOrganisme($organisme);
            $echangePj->setNomFichier($nomFichier.'.'.$extensionFile);
            $echangePj->setIdMessage($message->getId());
            $echangePj->setTaille(filesize($tmpFileOrdreDuJour));
            $idBlob = $atexoBlob->insert_blob($nomFichier, $tmpFileOrdreDuJour, $organisme);
            $echangePj->setPiece($idBlob);
            $echangePj->save($connexion);
            $this->remplirTableauPJ([$echangePj]);

            $this->setViewState('idMessage', $message->getId());
            $this->setViewState('message', $message);
        }
    }

    public function remplirListeMessage()
    {
        $dataMessageType = [];
        $dataMessageType[0] = Prado::localize('TEXT_SELECTIONNER').'...';
        $dataMessageType[Atexo_Config::getParameter('ID_MESSAGE_COURRIER_CONVOCATION_COMMISSION')] = Prado::localize('DEFINE_COURRIER_CONVOCATION_COMMISSION');
        $this->messageType->dataSource = $dataMessageType;
        $this->messageType->dataBind();
    }

    public function RedirectReturn()
    {
        if (self::getRedirect()) {
            $this->response->redirect(self::getRedirect());
        }
    }

    public function remplirTableauPJ($data)
    {
        $this->RepeaterPiecesJointes->dataSource = $data;
        $this->RepeaterPiecesJointes->dataBind();
    }

    public function refreshRepeaterPJ($sender, $param)
    {
        //Raffraichissement du tableau PJ:
        $objetMessage = Atexo_Message::retrieveMessageById($this->getViewState('idMessage'), $this->_org);
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $PJ = [];
        if ($objetMessage) {
            $c->add(CommonEchangePieceJointePeer::ORGANISME, $this->_org);
            $PJ = $objetMessage->getCommonEchangePieceJointes($c, $connexionCom);
        }
        $this->remplirTableauPJ($PJ);
        $this->PanelPJ->render($param->NewWriter);
    }

    public function onDeleteClick($sender, $param)
    {
        $idpj = $param->CommandParameter;
        Atexo_Message::DeletePj($idpj, $this->_org);
    }

    public function validerTaillePj()
    {
        if ($this->_tailleMaxPj) {
            $tailleMaxPj = $this->_tailleMaxPj;
        } else {
            $tailleMaxPj = Atexo_Config::getParameter('MAX_FILE_SIZE_IN_MAIL_COMMISSION');
        }
        $limitationTailleFichier = false;
        if ($this->courrierElectroniqueSimple->Checked) {
            $limitationTailleFichier = true;
        }
        if ($this->courrierElectroniqueContenuIntegralAR->Checked) {
            $limitationTailleFichier = true;
        }
        if ($limitationTailleFichier) {
            $pjs = Atexo_Message::getPjByIdEchange($this->getViewState('idMessage'), $this->_org);
            if ($pjs && (is_countable($pjs) ? count($pjs) : 0)) {
                $taille = 0;
                foreach ($pjs as $pj) {
                    if ($pj->getTaille() > $tailleMaxPj) {
                        return false;
                    }
                    $taille += $pj->getTaille();
                }
                if ($taille > $tailleMaxPj) {
                    return false;
                }

                return true;
            }
        }

        return true;
    }

    /**
     * Fonction d'envoi du mail.
     */
    public function envoyer()
    {
        if (!$this->validerTaillePj()) {
            $this->messageErreur->setVisible(true);
            $this->messageErreur->setMessage(Prado::localize('ERREUR_TAILLE_FICHIER_PJ'));
            $objetMessage = $this->getViewState('message');
            $PJ = [];
            if ($objetMessage instanceof CommonEchange) {
                $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
                $c = new Criteria();
                $c->add(CommonEchangePieceJointePeer::ORGANISME, $this->_org);
                $PJ = $objetMessage->getCommonEchangePieceJointes($c, $connexionCom);
            }
            $this->remplirTableauPJ($PJ);
        } else {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $organismeObj = Atexo_Organismes::retrieveOrganismeByAcronyme($this->_org);
            if ($organismeObj instanceof CommonOrganisme) {
                $expediteur = $organismeObj->getSigle().' - '.$organismeObj->getDenominationOrg();
            } else {
                $expediteur = '-';
            }
            $arraymaildestinataire = [];
            $objetMessage = $this->getViewState('message');
            if ($objetMessage instanceof CommonEchange) {
                if ($this->courrierElectroniqueSimple->Checked) {
                    $objetMessage->setOptionEnvoi(Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_SIMPLE'));
                }
                if ($this->courrierElectroniqueContenuIntegralAR->Checked) {
                    $objetMessage->setOptionEnvoi(Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_AVEC_AR'));
                }
                $objetMessage->setEmailExpediteur(Atexo_Config::getParameter('PF_MAIL_FROM'));
                $maildestinataires = explode(',', $this->destinataires->Text);
                if (is_array($maildestinataires) && 0 != count($maildestinataires)) {
                    foreach ($maildestinataires as $destinataire) {
                        if (!in_array($destinataire, $arraymaildestinataire)) {
                            $echangedestinataire = new CommonEchangeDestinataire();
                            $arraymaildestinataire[] = $destinataire;
                            if ($objetMessage->getOptionEnvoi() == Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_SIMPLE')) {
                                $echangedestinataire->setTypeAr(Atexo_Config::getParameter('ID_TYPE_AR_SANS_OBJET'));
                            } else {
                                $echangedestinataire->setTypeAr(Atexo_Config::getParameter('ID_TYPE_AR_NON_RETIRE'));
                            }
                            $echangedestinataire->setUid((new Atexo_Message())->getUniqueId());
                            $echangedestinataire->setMailDestinataire(trim($destinataire));
                            $echangedestinataire->setOrganisme($this->_org);
                            $objetMessage->addCommonEchangeDestinataire($echangedestinataire);
                        }
                    }
                }

                $corps = $this->textMail->text;
                if ($this->corpsAR->text) {
                    $corps .= '
	';
                    $corps .= $this->corpsAR->text;
                }

                $objetMessage->setCorps($corps);
                $objetMessage->setObjet($this->objet->Text);
                $objetMessage->setIdTypeMessage($this->messageType->getSelectedValue());
                $objetMessage->setExpediteur($expediteur);
                $objetMessage->setDestinataires($this->destinataires->getText());
                $objetMessage->save($connexion);
                $idEchange = $this->Page->EnvoyerEmail($objetMessage, $this->_org, $this->getCalledFrom());
                if ($idEchange) {
                    if ($this->messageType->getSelectedValue() == Atexo_Config::getParameter('ID_MESSAGE_COURRIER_CONVOCATION_COMMISSION') && isset($_GET['id'])) {
                        (new Atexo_Message_RelationEchange())->save($idEchange, base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id'])), Atexo_Config::getParameter('TYPE_RELATION_ECHANGE_COMMISSION_SEANCE'));
                    }
                    self::RedirectReturn();
                } else {
                    echo 'Echec d\'envoie';
                }
            }
        }
    }
}
