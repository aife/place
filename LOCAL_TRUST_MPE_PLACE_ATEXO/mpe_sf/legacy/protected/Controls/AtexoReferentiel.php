<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Ref;

/**
 * @author LCH
 * Template referentiel
 */
class AtexoReferentiel extends MpeTTemplateControl
{
    private $idElement;
    private string $cas = 'cas2';
    public $_atexoRef;
    private string $display = '';

    public function setIdElement($idElement)
    {
        $this->idElement = $idElement;
    }

    public function getIdElement()
    {
        return $this->idElement;
    }

    public function setCas($value)
    {
        $this->cas = $value;
    }

    public function getCas()
    {
        return $this->cas;
    }

    public function setDisplay($value)
    {
        $this->display = $value;
    }

    public function getDisplay()
    {
        return $this->display;
    }

    public function setAtexoRef($value)
    {
        $this->_atexoRef = $value;
    }

    public function getAtexoRef()
    {
        return $this->_atexoRef;
    }

    /*
     * @param Object $atexoRef
     */
    public function afficherReferentiel($atexoRef)
    {
        $dataRefFile = null;
        $urlRef = null;
        $sessionLang = Atexo_CurrentUser::readFromSession('lang');

        if (isset($sessionLang)) {
            $lang = $sessionLang;
        } else {
            $lang = Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE');
        }

        if ($atexoRef instanceof Atexo_Ref) {
            //l'identifiant
            $atexoRef->setClef($this->ClientId);
            // la langue
            if ('su' == $lang) {
                $lang = 'sv';
            }
            $atexoRef->setLocale($lang);
            //fichier de données
            $arrayConfigXml = $atexoRef->getArrayConfigXml();
            if ($arrayConfigXml) {
                $dataRefFile = Atexo_Config::getParameter('APP_BASE_ROOT_DIR').$arrayConfigXml['cheminData'].$arrayConfigXml['fichierDonnee'].$lang.'.csv';
            }

            //code principal
            $consulationPrincipal = [];
            if ($atexoRef->getPrincipal()) {
                $consulationPrincipal[0] = $atexoRef->getPrincipal();
                if (is_file($dataRefFile)) {
                    $consulationPrincipal[1] = $atexoRef->getLibelleRef($atexoRef->getPrincipal(), $dataRefFile);
                }
            }
            //codes secondaires
            $consulationSecondaires = [];
            if ($atexoRef->getSecondaires()) {
                $arrayCodesCPV = explode('#', $atexoRef->getSecondaires());
                if (is_array($arrayCodesCPV) && $arrayCodesCPV) {
                    $i = 0;
                    foreach ($arrayCodesCPV as $cpv) {
                        if (null !== $cpv && false !== $cpv && '' != $cpv) {
                            $consulationSecondaires[$i][0] = $cpv;
                            if (is_file($dataRefFile)) {
                                $consulationSecondaires[$i][1] = $atexoRef->getLibelleRef($cpv, $dataRefFile);
                            }
                            ++$i;
                        }
                    }
                }
            }

            if ('onlyCpvPrincipal' == $this->getDisplay()) {
                $this->scriptRef->Text = $consulationPrincipal[0];
            } else {
                //appel de la fonction retournerReferentiels
                $this->scriptRef->Text = $atexoRef->retournerReferentiels(
                    $atexoRef->getClef(),
                    $consulationPrincipal,
                    $consulationSecondaires,
                    $this->getCas(),
                    $this->getDisplay()
                )
                ;
            }
            //url Referentiel
            $urlRef = $atexoRef->getUrlReferentiel();
        }
        $this->UrlRef->setValue($urlRef);
        $this->casRef->setValue($this->getCas());
    }
}
