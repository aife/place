<?php

namespace Application\Controls;

use Application\Propel\Mpe\CommonTContactContrat;
use Application\Propel\Mpe\CommonTEtablissement;
use Application\Propel\Mpe\CommonTReferentielNace;
use Application\Propel\Mpe\CommonTReferentielNaceQuery;
use Application\Propel\Mpe\Entreprise;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Entreprise;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Ref;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Category;
use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN1;
use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2;
use Prado\Prado;

/**
 * Composant Indentification de l'Entreprise.
 *
 * @author Anas ZAKI <anas.zaki@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class IdentificationEntreprise extends MpeTTemplateControl
{
    public $idEntreprise;
    /*
     * 0 => mpe normal
     * 1 => mpe responsive
     * 2 => popup Decision Contrat
     */
    public int|bool $responsive = 0;
    /**
     * @var Entreprise
     */
    public $entreprise = null;
    public $soumissionnaire = null;

    public function onLoad($param)
    {
        if ($this->getIdEntreprise()) {
            $this->panelInfoEntreprise->setDisplay('Dynamic');
            $this->entreprise = (new Atexo_Entreprise())->retrieveCompanyBdeById($this->getIdEntreprise());
            $this->getIdentificationEntreprise($this->entreprise);
            $this->customizeForm();
            if (Atexo_Module::isEnabled('CompteEntrepriseCodeNaceReferentiel')) {
                $atexoReferentiel = new Atexo_Ref();
                $atexoReferentiel->setCheminFichierConfigXML(Atexo_Config::getParameter('PATH_FILE_NACE_BAS_NIVEAU_CONFIG'));
                if ($this->entreprise->getCodeApe()) {
                    $atexoReferentiel->setSecondaires($this->entreprise->getCodeApe());
                }
                $this->atexoReferentielCodeNace->afficherReferentiel($atexoReferentiel);
            }
        } else {
            $this->panelInfoEntreprise->setDisplay('None');
        }
    }

    public function initialiserComposant($param = null)
    {
        if ($this->getIdEntreprise()) {
            $this->panelInfoEntreprise->setDisplay('Dynamic');
            $this->entreprise = (new Atexo_Entreprise())->retrieveCompanyBdeById($this->getIdEntreprise());
            $this->getIdentificationEntreprise($this->entreprise);
            $this->customizeForm();

            if (Atexo_Module::isEnabled('CompteEntrepriseCodeNaceReferentiel')) {
                $atexoReferentiel = new Atexo_Ref();
                $atexoReferentiel->setCheminFichierConfigXML(Atexo_Config::getParameter('PATH_FILE_NACE_BAS_NIVEAU_CONFIG'));
                if ($this->entreprise->getCodeApe()) {
                    $atexoReferentiel->setSecondaires($this->entreprise->getCodeApe());
                }
                $this->atexoReferentielCodeNace->afficherReferentiel($atexoReferentiel);
            }
        } else {
            $this->panelInfoEntreprise->setDisplay('None');
        }
        if ($param) {
            $this->panelInfoEntreprise->render($param->getNewWriter());
        }
    }

    public function getIdentificationEntreprise($entreprise)
    {
        if ($entreprise) {
            $logger = Atexo_LoggerManager::getLogger('app');
            $this->idRaisonSociale->Text = $entreprise->getNom();
            $this->adresse->Text = $entreprise->getAdresse();
            $this->adresseSuite->Text = $entreprise->getAdresse2();
            $this->codePostal->Text = $entreprise->getCodepostal();
            $this->ville->Text = $entreprise->getVilleadresse();
            $this->pays->Text = $entreprise->getPaysadresse();
            $this->telephone->Text = $entreprise->getTelephone();
            $this->fax->Text = $entreprise->getFax();
            $this->siretSiege->Text = $entreprise->getSiren().' '.$entreprise->getNicSiege();

            $this->idNational->Text = $entreprise->getSirenEtranger();
            $this->formeJuridique->Text = $this->getLibelleFormeJuridique($entreprise->getFormejuridique());
            $this->categorieEntreprise->Text = ($entreprise->getCategorieEntreprise() == Atexo_Config::getParameter('CATEGORIE_ENTREPRISE_PME')) ? Prado::Localize('DEFINE_OUI') : Prado::Localize('DEFINE_NON');
            $libelleNace = $entreprise->getLibelleApe();
            if (!$libelleNace) {
                $refNace = CommonTReferentielNaceQuery::create()->filterByCodeNace5($entreprise->getCodeape())->findOne();
                if ($refNace instanceof CommonTReferentielNace) {
                    $libelleNace = $refNace->getLibelleActiviteDetaillee();
                }
            }
            $this->codeApeNafNace->Text = $entreprise->getCodeape() ? $entreprise->getCodeape().($libelleNace ? ' - '.$libelleNace : '') : ' - ';

            $this->region->Text = (new Atexo_Geolocalisation_GeolocalisationN1())->retrieveDenomination1ByIdGeoN1($entreprise->getRegion());
            $this->province->Text = (new Atexo_Geolocalisation_GeolocalisationN2())->retrieveDenomination1ByIdGeoN3($entreprise->getProvince());
            $this->telephone2->Text = $entreprise->getTelephone2();
            $this->telephone3->Text = $entreprise->getTelephone3();
            $this->idtaxeprof->Text = $entreprise->getNumTax();
            $this->numRc->Text = $entreprise->getRcNum();
            $this->villeRc->Text = $entreprise->getRcVille();
            $this->idsDomaines->Value = $entreprise->getDomainesActivites();
            $this->idcnss->Text = $entreprise->getCnss();
            $this->idcapital->Text = $entreprise->getCapitalSocial();
            $this->ifu->Text = $entreprise->getIfu();

            if ($entreprise->getDescriptionActivite()) {
                $this->description->text = $entreprise->getDescriptionActivite() ? str_replace("\n", '<br/>', Atexo_Util::replaceCharactersMsWordWithRegularCharacters($entreprise->getDescriptionActivite())) : ' -';
                $this->panelDescription->visible = true;
            } else {
                $this->panelDescription->visible = false;
            }

            $siteInternet = $entreprise->getSiteInternet();
            if ($siteInternet) {
                $this->panelSiteInternet->setVisible(true);
                $this->siteInternetLabel->text = $siteInternet;
                if (!strstr($siteInternet, 'http://') && !strstr($siteInternet, 'https://')) {
                    $urlSiteInternet = 'http://'.$siteInternet;
                } else {
                    $urlSiteInternet = $siteInternet;
                }
                $this->siteInternet->NavigateUrl = $urlSiteInternet;
            } else {
                $this->panelSiteInternet->setVisible(false);
            }

            $adress = [];
            $etablissementSiege = $entreprise->getMonEtablissementSiege();
            if ($etablissementSiege instanceof CommonTEtablissement) {
                if ($etablissementSiege->getAdresse() || $etablissementSiege->getAdresse2()) {
                    $adress[] = $etablissementSiege->getAdresse().' '.$etablissementSiege->getAdresse2();
                }

                if ($etablissementSiege->getCodepostal() || $etablissementSiege->getVille()) {
                    $adress[] = $etablissementSiege->getCodepostal().' '.$etablissementSiege->getVille();
                }

                if ($etablissementSiege->getPays()) {
                    $adress[] = $etablissementSiege->getPays();
                }

                $this->siegeSocial->text = implode(' - ', $adress);
            } else {
                $this->siegeSocial->text = '-';
                $logger->error("Il n'y a pas d'etablissement siege pour cette entreprise dont son id = ".$entreprise->getId());
            }

            $paysSiren = $entreprise->getPaysenregistrement().($entreprise->getSiren() ? ' - '.$entreprise->getSiren() : '');
            if ($paysSiren) {
                $this->paysSiren->text = '('.$paysSiren.')';
            }
            $this->idRaisonSociale->Text = $entreprise->getNom();
        }
    }

    public function setIdEntreprise($idEntreprise)
    {
        $this->idEntreprise = $idEntreprise;
    }

    public function getIdEntreprise()
    {
        return $this->idEntreprise;
    }

    /**
     * Adapter le formulaire pour un affichage qui correspond aux modules activés.
     */
    public function customizeForm()
    {
        if (Atexo_Module::isEnabled('CompteEntrepriseTelephone2')) {
            $this->panelTelEntreprise2->setDisplay('Dynamic');
        } else {
            $this->panelTelEntreprise2->setDisplay('None');
        }
        if (Atexo_Module::isEnabled('CompteEntrepriseTelephone3')) {
            $this->panelTelEntreprise3->setDisplay('Dynamic');
        } else {
            $this->panelTelEntreprise3->setDisplay('None');
        }
        if ($this->entreprise
            && ($this->entreprise->getPaysenregistrement() == Atexo_Config::getParameter('DENOMINATION2_GEON2_FRANCE'))
            && Atexo_Module::isEnabled('CompteEntrepriseCnss')
        ) {
            $this->panelCnss->setDisplay('Dynamic');
        } else {
            $this->panelCnss->setDisplay('None');
        }
        if ($this->entreprise
            && ($this->entreprise->getPaysenregistrement() == Atexo_Config::getParameter('DENOMINATION2_GEON2_FRANCE'))
            && Atexo_Module::isEnabled('CompteEntrepriseRcnum')
        ) {
            $this->panelNumRc->setDisplay('Dynamic');
        } else {
            $this->panelNumRc->setDisplay('None');
        }
        if ($this->entreprise
            && ($this->entreprise->getPaysenregistrement() == Atexo_Config::getParameter('DENOMINATION2_GEON2_FRANCE'))
            && Atexo_Module::isEnabled('CompteEntrepriseTaxProf')
        ) {
            $this->panelIdTaxProf->setDisplay('Dynamic');
        } else {
            $this->panelIdTaxProf->setDisplay('None');
        }
        if (!Atexo_Module::isEnabled('CompteEntrepriseCodeApe')) {
            $this->panelCodeApe->setDisplay('None');
        } else {
            $this->panelCodeApe->setDisplay('Dynamic');
        }
        if ($this->entreprise
            && ($this->entreprise->getPaysenregistrement() == Atexo_Config::getParameter('DENOMINATION2_GEON2_FRANCE'))
            && Atexo_Module::isEnabled('CompteEntrepriseRegion')
        ) {
            $this->panelRegion->setDisplay('Dynamic');
        } else {
            $this->panelRegion->setDisplay('None');
        }
        if ($this->entreprise
            && ($this->entreprise->getPaysenregistrement() == Atexo_Config::getParameter('DENOMINATION2_GEON2_FRANCE'))
            && Atexo_Module::isEnabled('CompteEntrepriseProvince')
        ) {
            $this->panelProvince->setDisplay('Dynamic');
        } else {
            $this->panelProvince->setDisplay('None');
        }
        if ($this->entreprise
            && ($this->entreprise->getPaysenregistrement() == Atexo_Config::getParameter('DENOMINATION2_GEON2_FRANCE'))
        ) {
            $this->panelSiren->setDisplay('Dynamic');
            $this->panelIdNational->setDisplay('None');
        } else {
            $this->panelSiren->setDisplay('None');
            $this->panelIdNational->setDisplay('Dynamic');
        }

        if (Atexo_Module::isEnabled('CompteEntrepriseDomaineActivite')) {
            $this->panelDomaineActivite->setDisplay('Dynamic');
        } else {
            $this->panelDomaineActivite->setDisplay('None');
        }
        if (Atexo_Module::isEnabled('CompteEntrepriseCapitalSocial')) {
            $this->panelCapital->setDisplay('Dynamic');
        } else {
            $this->panelCapital->setDisplay('None');
        }
        if ($this->entreprise
            && ($this->entreprise->getPaysenregistrement() == Atexo_Config::getParameter('DENOMINATION2_GEON2_FRANCE'))
            && Atexo_Module::isEnabled('CompteEntrepriseIfu')
        ) {
            $this->panelIfu->setDisplay('Dynamic');
        } else {
            $this->panelIfu->setDisplay('None');
        }
    }

    public function displayDomainesActivites()
    {
        $idsDomaines = $this->idsDomaines->Value;
        $libelleDomaine = '';
        if ($idsDomaines) {
            $libelleDomaine = Atexo_Consultation_Category::displayListeLibelleDomaineByArrayId($idsDomaines);
        }

        return $libelleDomaine;
    }

    public function getLibelleFormeJuridique($FJ)
    {
        $formeJuridique = (new Atexo_Entreprise())->retrieveFormeJuridiqueBySigle($FJ);

        if ($formeJuridique) {
            $langue = Atexo_CurrentUser::readFromSession('lang');
            $getLibelleFormejuridique = 'getLibelleFormejuridique'.ucwords($langue);
            if ($formeJuridique->$getLibelleFormejuridique()) {
                return $formeJuridique->$getLibelleFormejuridique();
            } elseif (!$formeJuridique->getLibelleFormejuridique() && !$formeJuridique->$getLibelleFormejuridique()) {
                return $formeJuridique->getFormejuridique();
            } elseif (0 == strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) && $formeJuridique->getLibelleFormejuridique()) {
                return $formeJuridique->getLibelleFormejuridique();
            }
        } else {
            return $FJ ?: '-';
        }
    }

    /**
     * @return bool
     */
    public function getResponsive()
    {
        return $this->responsive;
    }

    /**
     * @param bool $responsive
     */
    public function setResponsive($responsive)
    {
        $this->responsive = $responsive;
    }

    /**
     * @return null
     */
    public function getSoumissionnaire()
    {
        return $this->soumissionnaire;
    }

    /**
     * @param null $soumissionnaire
     */
    public function setSoumissionnaire($soumissionnaire)
    {
        $this->soumissionnaire = $soumissionnaire;
        if ($this->soumissionnaire instanceof CommonTContactContrat) {
            $this->atexoSoumissionnaire->visible = true;
            $this->atexoSoumissionnaire->setSoumissionnaire($soumissionnaire);
            $this->atexoSoumissionnaire->LoadSoumissionaire();
        } else {
            $this->atexoSoumissionnaire->visible = false;
        }
    }
}
