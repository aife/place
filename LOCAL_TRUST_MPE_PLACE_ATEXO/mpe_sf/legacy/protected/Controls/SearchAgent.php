<?php

namespace Application\Controls;

use Application\Service\Atexo\Agent\Atexo_Agent_CriteriaVo;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;

/*
 * Created on 11 juin 2012
 *
 * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
 * @package
 */
class SearchAgent extends MpeTTemplateControl
{
    public $_organisme;
    public $_service;
    public $langue;

    public function setOrganisme($organisme)
    {
        $this->_organisme = $organisme;
    }

    public function getOrganisme()
    {
        return $this->_organisme;
    }

    public function getServiceId()
    {
        return $this->_service;
    }

    public function setServiceId($value)
    {
        $this->_service = $value;
    }

    public function onLoad($param)
    {
        $this->ltRefAgent->agent = '1';
        $this->ltRefAgent->cssClass = 'intitule-140';
        $this->ltRefAgent->mode = '1';
        if (!$this->Page->isPostBack) {
            $this->ltRefAgent->afficherReferentielAgent();
        }
        $this->ltRefAgent->refreshReferentielAgent();
        $this->displayStatut();
    }

    public function fillData()
    {
        $this->langue = Atexo_CurrentUser::readFromSession('lang');
        if (!$this->Page->IsPostBack) {
            if (('Agent.SearchAchteursPublics' == $_GET['page']) || (Atexo_CurrentUser::hasHabilitation('HyperAdmin') && isset($_GET['all']) && 1 == $_GET['all'])) {
                self::displayOrganismes();
                self::displayEntityPurchase();
            } else {
                $this->_organisme = Atexo_CurrentUser::getCurrentOrganism();
                $this->_service = Atexo_CurrentUser::getIdServiceAgentConnected();
                $this->scriptOrg->Text = "<script>document.getElementById('divOrg').style.display = 'none';</script>";
                self::displayEntityPurchase();
            }
        } else {
            if (('Agent.SearchAchteursPublics' == $_GET['page']) || (Atexo_CurrentUser::hasHabilitation('HyperAdmin') && isset($_GET['all']) && 1 == $_GET['all'])) {
                $this->_organisme = $this->organismesNames->SelectedValue;
                $this->_service = $this->entityPurchaseNames->SelectedValue;
            } else {
                $this->_organisme = Atexo_CurrentUser::getCurrentOrganism();
                $this->_service = $this->entityPurchaseNames->SelectedValue;
            }
        }
    }

    public function displayOrganismes()
    {
        if (isset($_COOKIE['selectedorg'])) {
            $ArrayOrganismes = [];
            $getDenominationOrg = 'getDenominationOrg'.Atexo_Languages::getLanguageAbbreviation($this->langue);
            $ArrayOrganismesO = Atexo_Organismes::retrieveOrganismeByAcronyme($_COOKIE['selectedorg']);
            if (0 == strcmp($this->langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE'))
            || !$ArrayOrganismesO->$getDenominationOrg()) {
                $ArrayOrganismes[$ArrayOrganismesO->getAcronyme()] = $ArrayOrganismesO->getDenominationOrg();
            } else {
                $ArrayOrganismes[$ArrayOrganismesO->getAcronyme()] = $ArrayOrganismesO->$getDenominationOrg();
            }
            //$this->organismesNames->SetEnabled(false);
            //$this->scriptPurchaseNames->Text = "<script>document.getElementById('divEntityPurchase').style.display = '';</script>";
            $ArrayOrganismesRestreint = $ArrayOrganismes;
            $listEntityPurchase = Atexo_EntityPurchase::retrieveEntityPurchaseByOrganisme($ArrayOrganismesO->getAcronyme(), null, $this->langue);
            $this->entityPurchaseNames->dataSource = $listEntityPurchase;
            $this->entityPurchaseNames->DataBind();
        } else {
            $ArrayOrganismes = Atexo_Organismes::retrieveOrganismes(false, $this->langue);
            $ArrayOrganismesRestreint = $ArrayOrganismes;
            //array_unshift($ArrayOrganismes,'--- '.Prado::localize('TEXT__TOUS_MINISTERES').' ---');
        }
        //Recherche multi-critères
        //ajout des informations pour compter le nombre agent et servuces

        $arrayOrga = [];
        foreach ($ArrayOrganismes as $key => $value) {
            $arrayOrga[$key] = $value.' ( '.Prado::localize('DEFINE_SERVICES').' : '.(new Atexo_EntityPurchase())->getCountService($key).'   /   '.Prado::localize('AGENTS_ACTIFS').' : '.(new Atexo_Agent())->getCountAgent($key, '1').'   /   '.Prado::localize('AGENTS_DESACTIVES').' : '.(new Atexo_Agent())->getCountAgent($key, '0').' )';
        }
        $ArrayOrganismes = $arrayOrga;
        array_unshift($ArrayOrganismes, '--- '.Prado::localize('TEXT__TOUS_MINISTERES').' ( '.Prado::localize('TEXT_ENTITES').' : '.count($ArrayOrganismes).'   /   '.Prado::localize('DEFINE_SERVICES').' : '.(new Atexo_EntityPurchase())->getCountAllServicesPortail().'   /   '.Prado::localize('AGENTS_ACTIFS').' : '.(new Atexo_Agent())->getCountAgent(null, '1').'   /   '.Prado::localize('AGENTS_DESACTIVES').' : '.(new Atexo_Agent())->getCountAgent(null, '0').' )'.' ---');

        $this->organismesNames->dataSource = $ArrayOrganismes;
        $this->organismesNames->DataBind();
        if (!Atexo_Module::isEnabled('AfficherCodeService')) {
            $this->organismesNames->SelectedValue = $this->_organisme;
        }
    }

    public function displayEntityPurchase($sender = false, $param = false)
    {
        //script pour afficher liste des services
        if ('' != $this->organismesNames->SelectedValue && ' ' != $this->organismesNames->SelectedValue && '0' != $this->organismesNames->SelectedValue) {
            $this->_organisme = $this->organismesNames->SelectedValue;
        }
        $listEntityPurchase = Atexo_EntityPurchase::getEntityPurchase($this->_organisme, true);
        // pour avoir touts les services
        $entities = [];
        if ($this->Page->IsCallback) {
            if (is_array($listEntityPurchase) && count($listEntityPurchase)) {
                foreach ($listEntityPurchase as $key => $entityPurchase) {
                    $entities[$key] = $entityPurchase;
                }
            }
        } else {
            $entities = $listEntityPurchase;
        }

        $this->entityPurchaseNames->dataSource = $entities;
        $this->entityPurchaseNames->DataBind();

        if (is_array($entities) && count($entities)) {
            if ($this->_organisme != Atexo_CurrentUser::getCurrentOrganism()) {
                $this->entityPurchaseNames->SelectedValue = 0;
            } else {
                $this->entityPurchaseNames->SelectedValue = $this->_service;
            }
            $this->panelPurchaseNames->setVisible(true);
        } else {
            $this->panelPurchaseNames->setVisible(false);
            $this->inclureDescendances->Checked = true;
        }
        if ($param) {
            $this->panelSearchAgent->render($param->getNewWriter());
        }
        $this->setUrlAjoutAgent();
    }

    /*
     * Method qui remplie le criteriaVo Agent
     */
    public function fillCriteriaVoAgent($limit = 0, $offset = 0)
    {
        $criteriaVo = new Atexo_Agent_CriteriaVo();
        if ($this->entityPurchaseNames->SelectedValue) {
            $this->_service = $this->entityPurchaseNames->SelectedValue;
        } else {
            $this->_service = 0;
        }
        $criteriaVo->setService($this->_service);
        if (!$this->_organisme) {
            if ($this->organismesNames->SelectedValue) {
                $this->_organisme = $this->organismesNames->SelectedValue;
            } else {
                $this->_organisme = 0;
            }
        }

        $criteriaVo->setOrganisme($this->_organisme);
        if ('' != $this->nomAgent->Text) {
            $criteriaVo->setNom(Atexo_Util::encodeToHttp($this->nomAgent->Text));
        }
        if ('' != $this->prenomAgent->Text) {
            $criteriaVo->setPrenom(Atexo_Util::encodeToHttp($this->prenomAgent->Text));
        }
        if ('' != $this->emailAgent->Text) {
            $criteriaVo->setEmail(Atexo_Util::encodeToHttp(trim($this->emailAgent->Text)));
        }
        if ('' != $this->IdentifiantAgent->Text) {
            $criteriaVo->setIdentifiant(Atexo_Util::encodeToHttp($this->IdentifiantAgent->Text));
        }

        if (true == $this->inclureDescendances->Checked) {
            $criteriaVo->setInclureDesc(true);
        }
        //remplir le tableau de Referentiel_Agent
        $criteriaVo->setReferentielVo($this->ltRefAgent->getValueReferentielVo());
        if (Atexo_Module::isEnabled('AfficherCodeService')) {
            if ('' != $this->libelleAcheteur->Text) {
                $criteriaVo->setLibelleAcheteur(Atexo_Util::encodeToHttp($this->libelleAcheteur->Text));
            }
            if ('' != $this->codeAcheteur->Text) {
                $criteriaVo->setCodeAcheteur(Atexo_Util::encodeToHttp($this->codeAcheteur->Text));
            }
        }
        $criteriaVo->setLimit($limit);
        $criteriaVo->setOffset($offset);
        if ('1' === $this->statutCompteAgent->SelectedValue) {
            $criteriaVo->setActif(true);
        } elseif ('0' === $this->statutCompteAgent->SelectedValue) {
            $criteriaVo->setActif(false);
        } else {
            $criteriaVo->setActif(null);
        }

        $this->Page->setViewState('CriteriaVoAgent', $criteriaVo);

        return $criteriaVo;
    }

    public function getDataAgents($nombreElement = false, $limit = 0, $offset = 0)
    {
        $criteriaVo = self::fillCriteriaVoAgent($limit, $offset);

        return (new Atexo_Agent())->search($criteriaVo, $nombreElement);
    }

    public function setUrlAjoutAgent()
    {
        $organisme = $this->organismesNames->SelectedValue;
        $service = $this->entityPurchaseNames->SelectedValue;
        $this->Page->getUrlAjoutAgent($organisme, $service);
    }

    public function displayStatut()
    {
        $statuts = ['2' => Prado::localize('TOUS_LES_STATUTS_COMPTE_AGENT'), '1' => Prado::localize('TEXT_ACTIF'), '0' => Prado::localize('DEFINE_DESACTIVE')];
        $this->statutCompteAgent->dataSource = $statuts;
        $this->statutCompteAgent->DataBind();
    }
}
