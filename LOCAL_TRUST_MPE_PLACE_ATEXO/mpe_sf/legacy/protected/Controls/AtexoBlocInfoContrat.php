<?php

namespace Application\Controls;

use Application\Library\Propel\Exception\PropelException;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonTContratMulti;
use Application\Propel\Mpe\CommonTContratTitulaire;
use Application\Propel\Mpe\CommonTypeProcedureConcessionPivot;
use Application\Service\Atexo\Atexo_CommonReferentiel;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Departement;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_TrancheBudgetaire;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Category;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Contrat;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureType;
use Application\Service\Atexo\Contrat\Atexo_Contrat_Gestion;
use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2;
use Prado\Prado;

/**
 * permet d'afficher les information de contrat.
 *
 * @author LEZ <loubna.ezziani@atexo.com>
 *
 * @version 1.0
 *
 * @since 2015-roadmap
 *
 * @copyright Atexo 2015
 */
class AtexoBlocInfoContrat extends MpeTTemplateControl
{
    private $contrat;
    private $codeDenomAdapte;

    /**
     * Permet de initialiser le parametre type contrat.
     *
     * @param int $v le type de contrat
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function setTypeContrat($v)
    {
        $this->setViewState('typeContrat', $v);
    }

    /**
     * Permet de recuperer le parametre type contrat.
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function getTypeContrat()
    {
        return $this->getViewState('typeContrat');
    }

    /**
     * Permet d'initialiser le composant.
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function initialiserComposant()
    {
        $idTypeContrat = $this->getTypeContrat();

        if ($idTypeContrat) {
            $this->panelInfoContrat->setDisplay('Dynamic');
            $this->fillCcagReference();
            $this->chargerProcedures();

            if ((new Atexo_Consultation_Contrat())->isTypeContratArticle133ById($idTypeContrat)) {
                self::initialiserPublicationmarche();
            }

            $this->chargerCategories();

            if (Atexo_Module::isEnabled('AffichageCodeCpv')) {
                //charger referentiel CPV
                $this->referentielCPV->onInitComposant();
                $this->referentielCPV->setCodeCpvObligatoire(true);
            }

            self::chargerComposant($idTypeContrat);
        } else {
            $this->panelInfoContrat->setDisplay('None');
        }
    }

    /**
     * Permet de charger les panel de composant.
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function chargerComposant($idTypeContrat)
    {
        if ((new Atexo_Consultation_Contrat())->isTypeContratAvecMontantMaxById($idTypeContrat)) {
            $this->panelMontantAC->setDisplay('Dynamic');
        } else {
            $this->panelMontantAC->setDisplay('None');
        }

        if ((new Atexo_Consultation_Contrat())->isTypeContratAvecMontantById($idTypeContrat) &&
            !(new Atexo_Consultation_Contrat())->isTypeContratMultiById($idTypeContrat)
        ) {
            $this->panelMontantContrat->setDisplay('Dynamic');
        } else {
            $this->panelMontantContrat->setDisplay('None');
        }

        if ((new Atexo_Consultation_Contrat())->isTypeContratArticle133ById($idTypeContrat)) {
            $this->article133->setDisplay('Dynamic');
        } else {
            $this->article133->setDisplay('None');
        }

        //$libelle = $this->getLibelleObjetMarche($idTypeContrat);
        $libelle = $this->getLibelleObjetContrat($idTypeContrat);
        $this->labelObjetMarche->setText($libelle);
        $this->labelScriptJs->Text = "<script>J('#".$this->objetMarche->ClientId."').prop('title', '".addslashes($libelle)."');</script>";
    }

    /**
     * retourne le libelle d'objet du marche.
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function getLibelleObjetMarche($idTypeContrat)
    {
        if ((new Atexo_Consultation_Contrat())->isTypeContratMarche($idTypeContrat)) {
            return Prado::Localize('DEFINE_OBJET_MARCHE');
        } elseif ((new Atexo_Consultation_Contrat())->isTypeContratSad($idTypeContrat)) {
            return Prado::Localize('OBJET_SAD');
        }

        return Prado::Localize('OBJET_ACCORD_CADRE');
    }

    /**
     * retourne le libelle d'objet du contrat.
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function getLibelleObjetContrat($idTypeContrat)
    {
        if ((new Atexo_Consultation_Contrat())->isTypeContratMarche($idTypeContrat)) {
            return Prado::Localize('DEFINE_OBJET_CONTRAT');
        } elseif ((new Atexo_Consultation_Contrat())->isTypeContratSad($idTypeContrat)) {
            return Prado::Localize('OBJET_SAD');
        }

        return Prado::Localize('OBJET_ACCORD_CADRE');
    }

    /**
     * remplir liste CCAG.
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function fillCcagReference()
    {
        $dataCcagRef = (new Atexo_CommonReferentiel())->retrieveValeurReferentielCCAG(Atexo_Config::getParameter('REFERENTIEL_CCAG_REFERENCE'), false, true);
        $dataSource = [];

        if ($dataCcagRef) {
            foreach ($dataCcagRef as $oneVal) {
                $dataSource[$oneVal->getId()] = $oneVal->getLibelleValeurReferentiel();
            }
        }

        $this->ccag->DataSource = $dataSource;
        $this->ccag->DataBind();
    }

    /**
     * Permet de charger la natures de prestation.
     *
     * @param CommonConsultation $consultation la consultation
     * @param array              $listeLots    la liste des lots
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function chargerCategories()
    {
        $categories = Atexo_Consultation_Category::retrieveCategories();
        $dataCategories = Atexo_Consultation_Category::retrieveDataCategories($categories);
        $this->naturePrestations->Datasource = $dataCategories;
        $this->naturePrestations->dataBind();
    }

    /**
     * Permet de change les tranches budgetaire selon l'annee et le montant.
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function changeTrancheBudgetaire()
    {
        if ((new Atexo_Consultation_Contrat())->isTypeContratAvecMontantById($this->getTypeContrat()) &&
            !(new Atexo_Consultation_Contrat())->isTypeContratMultiById($this->getTypeContrat())
        ) {
            $organisme = Atexo_CurrentUser::getOrganismAcronym();

            $contrat = new CommonTContratTitulaire();
            $contrat->setOrganisme($organisme);
            $contrat->setDatePrevueNotification(Atexo_Util::frnDate2iso($this->dateNotification->Text));
            $contrat->setMontantContrat(Atexo_Util::getMontantArronditSansEspace($this->MontantMarche->Text));

            $contratGestion = new Atexo_Contrat_Gestion();
            $contratGestion->changeTrancheBudgetaire($contrat);

            $this->trancheBudgetaire->Text = (new Atexo_TrancheBudgetaire())->getLibelleTrancheById($contrat->getIdTrancheBudgetaire(), $organisme);
            $this->idTrancheBudgetaire->Value = $contrat->getIdTrancheBudgetaire();
        }
    }

    /**
     * Permet de valider les donnees de contrat selon le type.
     *
     * @param $isValid
     * @param $script
     * @param $arrayMsgErreur
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function validerInfoContrat(&$isValid, &$script, &$arrayMsgErreur)
    {
        $idTypeContrat = $this->getTypeContrat();

        if (!$this->objetMarche->Text) {
            $arrayMsgErreur[] = $this->getLibelleObjetMarche($idTypeContrat);
            $script .= "document.getElementById('".$this->objetMarcheTypeImgError->getClientId()."').style.display = '';";
            $isValid = false;
        } else {
            $script .= "document.getElementById('".$this->objetMarcheTypeImgError->getClientId()."').style.display = 'none';";
        }

        if (!$this->procedurePassation->getSelectedValue()) {
            $arrayMsgErreur[] = Prado::localize('TEXT_PROCEDURE_PASSATION');
            $script .= "document.getElementById('".$this->ImgErrorProcedurePassation->getClientId()."').style.display = '';";
            $isValid = false;
        } else {
            $script .= "document.getElementById('".$this->ImgErrorProcedurePassation->getClientId()."').style.display = 'none';";
        }

        // La deuxième condition permet de validé que le lieu saisi existe dans notre base.
        if (!$this->lieuPrincipalExecution->Text
        || empty((new Atexo_Geolocalisation_GeolocalisationN2())->retrieveListDepartementsByDenomNormalise($this->lieuPrincipalExecution->Text))) {
            $arrayMsgErreur[] = Prado::localize('TEXT_LIEU_PRINCIPAL_EXECUTION');
            $script .= "document.getElementById('".$this->ImgErrorLieuPrincipalExecution->getClientId()."').style.display = '';";
            $isValid = false;
        } else {
            $script .= "document.getElementById('".$this->ImgErrorLieuPrincipalExecution->getClientId()."').style.display = 'none';";
        }

        if (!$this->naturePrestations->getSelectedValue()) {
            $arrayMsgErreur[] = Prado::localize('NATURE_DES_PRESTATIONS');
            $script .= "document.getElementById('".$this->ImgErrorNaturePrestations->getClientId()."').style.display = '';";
            $isValid = false;
        } else {
            $script .= "document.getElementById('".$this->ImgErrorNaturePrestations->getClientId()."').style.display = 'none';";
        }

        if (!$this->ccag->getSelectedValue()) {
            $arrayMsgErreur[] = Prado::localize('ccag');
            $script .= "document.getElementById('".$this->ImgErrorCcag->getClientId()."').style.display = '';";
            $isValid = false;
        } else {
            $script .= "document.getElementById('".$this->ImgErrorCcag->getClientId()."').style.display = 'none';";
        }

        if ((new Atexo_Consultation_Contrat())->isTypeContratAvecMontantMaxById($idTypeContrat) && !$this->montantMax->Text) {
            $arrayMsgErreur[] = Prado::localize('MONTANT_MAX_ESTIME');
            $script .= "document.getElementById('".$this->ImgErrorMontantMax->getClientId()."').style.display = '';";
            $isValid = false;
        } else {
            $script .= "document.getElementById('".$this->ImgErrorMontantMax->getClientId()."').style.display = 'none';";
        }

        if ((new Atexo_Consultation_Contrat())->isTypeContratAvecMontantById($idTypeContrat) &&
            !(new Atexo_Consultation_Contrat())->isTypeContratMultiById($idTypeContrat) &&
            !$this->MontantMarche->Text
        ) {
            $arrayMsgErreur[] = Prado::localize('MONTANT');
            $script .= "document.getElementById('".$this->ImgErrorMontantMarche->getClientId()."').style.display = '';";
            $isValid = false;
        } else {
            $script .= "document.getElementById('".$this->ImgErrorMontantMarche->getClientId()."').style.display = 'none';";
        }

        if (!$this->formePrix_ferme->Checked &&
            !$this->formePrix_ferme_actualisable->Checked &&
            !$this->formePrix_revisable->Checked
        ) {
            $script .= "document.getElementById('".$this->ImgErrorFormePrix->getClientId()."').style.display = '';";
            $arrayMsgErreur[] = Prado::localize('TEXT_FORME_PRIX');
            $isValid = false;
        } else {
            $script .= "document.getElementById('".$this->ImgErrorFormePrix->getClientId()."').style.display = 'none';";
        }

        if (Atexo_Module::isEnabled('MarcheDefense', Atexo_CurrentUser::getOrganismAcronym())) {
            if (!$this->marcheDefense_Oui->Checked && !$this->marcheDefense_Non->Checked) {
                $script .= "document.getElementById('".$this->ImgErrorMarcheDefense->getClientId()."').style.display = '';";
                $arrayMsgErreur[] = Prado::localize('MARCHE_DEFENSE_OU_SECURITE');
                $isValid = false;
            } else {
                $script .= "document.getElementById('".$this->ImgErrorMarcheDefense->getClientId()."').style.display = 'none';";
            }
        }

        if (!$this->dateNotification->Text) {
            $arrayMsgErreur[] = Prado::localize('DATE_PREVISIONNELLE_NOTIFICATION');
            $script .= "document.getElementById('".$this->ImgErrorDateNotification->getClientId()."').style.display = '';";
            $isValid = false;
        } else {
            $script .= "document.getElementById('".$this->ImgErrorDateNotification->getClientId()."').style.display = 'none';";
        }

        if (!$this->datePrevisionnelleFinMarche->Text) {
            $arrayMsgErreur[] = Prado::localize('DATE_PREVISIONNELLE_FIN_MARCHE');
            $script .= "document.getElementById('".$this->ImgErrorDatePrevisionnelleFinMarche->getClientId()."').style.display = '';";
            $isValid = false;
        } else {
            $script .= "document.getElementById('".$this->ImgErrorDatePrevisionnelleFinMarche->getClientId()."').style.display = 'none';";
        }

        if (!$this->datePrevFinMaxMarche->Text) {
            $arrayMsgErreur[] = Prado::localize('DATE_PREVISIONNELLE_FIN_MAX_MARCHE');
            $script .= "document.getElementById('".$this->ImgErrorDatePrevFinMaxMarche->getClientId()."').style.display = '';";
            $isValid = false;
        } else {
            $script .= "document.getElementById('".$this->ImgErrorDatePrevFinMaxMarche->getClientId()."').style.display = 'none';";
        }

        if (Atexo_Module::isEnabled('ConsultationClause')) {
            $this->achatResponsableConsultation->validerAchatResponsable($isValid, $arrayMsgErreur, $script);
        }
        if ($idTypeContrat && (new Atexo_Consultation_Contrat())->isTypeContratConcession($idTypeContrat)) {
            if (!$this->montantSubvention->getText()) {
                $arrayMsgErreur[] = Prado::localize('MONTANT_SUBVENTION_PUBLIQUE');
                $script .= "document.getElementById('".$this->ImgErrorMontantSubvention->getClientId()."').style.display = '';";
                $isValid = false;
            } else {
                $script .= "document.getElementById('".$this->ImgErrorMontantSubvention->getClientId()."').style.display = 'none';";
            }

            if (!$this->dateExecution->getText() || !Atexo_Util::isDate($this->dateExecution->getText())) {
                $arrayMsgErreur[] = Prado::localize('TEXT_DATE_DEBUT_EXECUTION');
                $script .= "document.getElementById('".$this->ImgErrorDateExecution->getClientId()."').style.display = '';";
                $isValid = false;
            } else {
                $script .= "document.getElementById('".$this->ImgErrorDateExecution->getClientId()."').style.display = 'none';";
            }
        } else {
            $script .= "document.getElementById('".$this->ImgErrorMontantSubvention->getClientId()."').style.display = 'none';";
            $script .= "document.getElementById('".$this->ImgErrorDateExecution->getClientId()."').style.display = 'none';";
        }
    }

    /**
     * @param $sender
     * @param $param
     *
     * @throws PropelException
     */
    protected function suggestLieux($sender, $param)
    {
        $this->labelScriptJs->Text = '<script>suggestionSelected();</script>';
        // Get the token
        $token = $param->getToken();

        if ($listeEntite = (new Atexo_Geolocalisation_GeolocalisationN2())->retrieveDepartements($token)) {
            $sender->DataSource = $listeEntite;
            $sender->dataBind();
        } else {
            $sender->DataSource = [];
            $sender->dataBind();
            $this->codeDenomAdapte = '';
            $this->Page->setViewState('denominationAdapte', '');
        }
    }

    /**
     * @param $sender
     * @param $param
     *
     * @throws PropelException
     */
    protected function lieuxExecution($sender, $param)
    {
        $this->labelScriptJs->Text = "";
        $this->labelScriptJs->Text = '<script>suggestionSelected();</script>';
        // Get the token
        $token = $param->getToken();

        if ($listeEntite = (new Atexo_Geolocalisation_GeolocalisationN2())->retrieveDepartements($token)) {
            $sender->DataSource = $listeEntite;
            $sender->dataBind();
        } else {
            $sender->DataSource = [];
            $sender->dataBind();
            $this->lieuPrincipalExecution->Text = '';
            $this->Page->setViewState('lieuPrincipalExecution', '');
        }
    }

    /**
     * @param $sender
     * @param $param
     */
    public function suggestionSelected($sender, $param)
    {
        $this->codeDenomAdapte = $sender->Suggestions->DataKeys[$param->selectedIndex];
        $this->Page->setViewState('denominationAdapte', '');

        if ('' != $this->codeDenomAdapte) {
            $this->Page->setViewState('denominationAdapte', $this->codeDenomAdapte);
        }
    }

    /**
     * Permet de charger les procédures de passation.
     */
    public function chargerProcedures()
    {
        $procedures = Atexo_Consultation_ProcedureType::retrieveProcedureTypesPivot(true, true);
        $proc = [0 => '---'.Prado::localize('TEXT_TOUTES_PROCEDURES').'---'];

        foreach ($procedures as $procedure) {
            $proc[$procedure->getId()] = (new Atexo_Consultation_ProcedureType())->retrieveTypeProcedurePivot($procedure->getId());
        }

        $this->procedurePassation->Datasource = $proc;
        $this->procedurePassation->dataBind();
    }

    /**
     * @param $sender
     * @param $param
     */
    public function chargerLieuxExecution($sender, $param)
    {
        // Get the token
        $token = $param->getToken();

        // Sender is the Suggestions repeater
        $sender->DataSource = $this->chargerDepartementsListe($token);
        $sender->lieuPrincipalExecution->dataBind();
    }

    /**
     * @param $name
     *
     * @return array
     */
    public function chargerDepartementsListe($name)
    {
        $departements = (new Atexo_Departement())->getListDepartements(true);

        if ($name) {
            return array_filter($departements, fn ($el) => false !== stripos($el['name'], (string) $name));
        } else {
            return $departements;
        }
    }

    /**
     * Permet de sauvgarder les contrat.
     *
     * @param CommonTContratTitulaire$contrat
     * @param null ou CommonTContratMulti $contratMulti
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function saveContrat(&$contrat)
    {
        if ($contrat instanceof CommonTContratTitulaire) {
            $idTypeContrat = $contrat->getIdTypeContrat();

            $contrat->setIntitule($this->intitule->getSafeText());
            $contrat->setObjetContrat($this->objetMarche->getSafeText());

            if ((new Atexo_Consultation_Contrat())->isTypeContratConcession($idTypeContrat)) {
                $contrat->setIdTypeProcedureConcessionPivot($this->procedurePassation->getSelectedValue());
                $contrat->setMontantSubventionPublique(Atexo_Util::getMontantArronditSansEspace($this->montantSubvention->getSafeText()));
                $contrat->setDateDebutExecution(Atexo_Util::frnDate2iso($this->dateExecution->Text));
            } else {
                $contrat->setProcedurePassationPivot($this->procedurePassation->Text);
            }
            $contrat->setNomLieuPrincipalExecution($this->lieuPrincipalExecution->Text);
            $contrat->setDateDebutExecution(Atexo_Util::frnDate2iso($this->dateDeDebutExecution->Text));

            $lieuExecutionId = (new Atexo_Geolocalisation_GeolocalisationN2())->retrieveIdsByDenomination1($this->lieuPrincipalExecution->Text);
            $lieuExecution = (new Atexo_Geolocalisation_GeolocalisationN2())->retrieveGeolocalisationN2ById($lieuExecutionId);

            if($this->marcheInnovant_Oui->Checked){
                $contrat->setMarcheInnovant(1);
            }else{
                $contrat->setMarcheInnovant(0);
            }
            $contrat->setCodeLieuPrincipalExecution($lieuExecution[0]->getDenomination2());
            $contrat->setTypeCodeLieuPrincipalExecution(1); // todo récupérer valeur dynamiquement (DB)
            $contrat->setCategorie($this->naturePrestations->getSelectedValue());
            $contrat->setCcagApplicable($this->ccag->getSelectedValue());
            $this->achatResponsableConsultation->save($contrat);

            $dateNotif = Atexo_Util::frnDate2iso($this->dateNotification->Text);
            $contrat->setDatePrevueNotification($dateNotif);
            $contrat->setDatePrevueFinContrat(Atexo_Util::frnDate2iso($this->datePrevisionnelleFinMarche->Text));
            $contrat->setDatePrevueMaxFinContrat(Atexo_Util::frnDate2iso($this->datePrevFinMaxMarche->Text));

            if ((new Atexo_Consultation_Contrat())->isTypeContratAvecMontantById($idTypeContrat) &&
                !(new Atexo_Consultation_Contrat())->isTypeContratMultiById($idTypeContrat)
            ) {
                $montant = Atexo_Util::getMontantArronditSansEspace($this->MontantMarche->Text);
                $contrat->setMontantContrat($montant);

                // Ajout des tranches budgetaire
                $contratGestion = new Atexo_Contrat_Gestion();
                $contratGestion->changeTrancheBudgetaire($contrat);
            }

            $formePrix = null;

            if ($this->formePrix_ferme->Checked) {
                $formePrix = 1;
            } elseif ($this->formePrix_ferme_actualisable->Checked) {
                $formePrix = 2;
            } elseif ($this->formePrix_revisable->Checked) {
                $formePrix = 3;
            }

            $contrat->setFormePrix($formePrix);
            if (Atexo_Module::isEnabled('MarcheDefense', Atexo_CurrentUser::getOrganismAcronym())) {
                if ($this->marcheDefense_Oui->Checked) {
                    $contrat->setMarcheDefense('1');
                } elseif ($this->marcheDefense_Non->Checked) {
                    $contrat->setMarcheDefense('2');
                } else {
                    $contrat->setMarcheDefense('0');
                }
            }

            if ((new Atexo_Consultation_Contrat())->isTypeContratArticle133ById($idTypeContrat)) {
                if ($this->art133NonPublie->getChecked()) {
                    $contrat->setPublicationMontant('1');
                    $contrat->setPublicationContrat('1');
                } elseif ($this->art133Publie->getChecked()) {
                    $contrat->setPublicationMontant('0');
                    $contrat->setPublicationContrat('0');
                }
            }

            if ((new Atexo_Consultation_Contrat())->isTypeContratAvecMontantMaxById($idTypeContrat)) {
                $montantMax = Atexo_Util::getMontantArronditSansEspace($this->montantMax->getSafeText());
                $contrat->setMontantMaxEstime($montantMax);
            }

            //Codes CPV
            $codePrincipal = $this->referentielCPV->getCpvPrincipale();

            if ('' != $codePrincipal) {
                $contrat->setCodeCpv1($codePrincipal);
            } else {
                $contrat->setCodeCpv1(null);
            }

            $codesSec = $this->referentielCPV->getCpvSecondaires();

            if ('' != $codesSec) {
                $contrat->setCodeCpv2($codesSec);
            } else {
                $contrat->setCodeCpv2(null);
            }
            // TODO GESTION DES NUMERO
        }
    }

    /**
     * Permet de sauvgarder les contrat.
     *
     * @param CommonTContratTitulaire$contrat
     * @param null ou CommonTContratMulti $contratMulti
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function saveContratMulti(&$contratMulti)
    {
        if ($contratMulti instanceof CommonTContratMulti) {
            $idTypeContrat = $contratMulti->getIdTypeContrat();

            if ((new Atexo_Consultation_Contrat())->isTypeContratAvecChapeauById($idTypeContrat)) {
                $contratMulti->setIntitule($this->intitule->getSafeText());
                $contratMulti->setObjetContrat($this->objetMarche->getSafeText());
                $contratMulti->setCategorie($this->naturePrestations->getSelectedValue());
                $contratMulti->setCcagApplicable($this->ccag->getSelectedValue());
                $this->achatResponsableConsultation->save($contratMulti);
                //Codes CPV
                $codePrincipal = $this->referentielCPV->getCpvPrincipale();

                if ('' != $codePrincipal) {
                    $contratMulti->setCodeCpv1($codePrincipal);
                } else {
                    $contratMulti->setCodeCpv1(null);
                }

                $codesSec = $this->referentielCPV->getCpvSecondaires();

                if ('' != $codesSec) {
                    $contratMulti->setCodeCpv2($codesSec);
                } else {
                    $contratMulti->setCodeCpv2(null);
                }
            }

            if ((new Atexo_Consultation_Contrat())->isTypeContratAvecMontantMaxById($idTypeContrat)) {
                $montantMax = Atexo_Util::getMontantArronditSansEspace($this->montantMax->getSafeText());
                $contratMulti->setMontantMaxEstime($montantMax);
            }
        }
        // TODO GESTION DES NUMERO
    }

    /**
     * Permet de changer la valeur de la publicationselon l'activation ou nn deu marché de défense.
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2018-esr
     *
     * @copyright Atexo 2018
     */
    public function marcheDefenseChanged()
    {
        if ($this->marcheDefense_Oui->Checked) {
            $this->art133Publie->setChecked(false);
            $this->art133NonPublie->setChecked(true);
            $this->art133Publie->setEnabled(false);
            $this->art133NonPublie->setEnabled(false);
        } elseif ($this->marcheDefense_Non->Checked) {
            $this->art133Publie->setChecked(true);
            $this->art133NonPublie->setChecked(false);
            $this->art133Publie->setEnabled(true);
            $this->art133NonPublie->setEnabled(true);
        }
    }

    /**
     * Permet d'initialiser les valeur de la publication du contrat.
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2018-esr
     *
     * @copyright Atexo 2018
     */
    public function initialiserPublicationmarche()
    {
        $this->art133Publie->setChecked(true);
        $this->art133NonPublie->setChecked(false);
    }

    /**
     * @param $idTypeContrat
     */
    public function loadFieldsContratConcession($idTypeContrat)
    {
        if ((new Atexo_Consultation_Contrat())->isTypeContratConcession($idTypeContrat)) {
            $this->montant_lot_1_attributaire_1->setDisplay('None');
            $this->montant_lot_1_attributaire_1_concession->setDisplay('Dynamic');
            $this->info_bulle_valeur_globale_estimee->setDisplay('Dynamic');
            self::chargerProceduresConcessionPivot();
            $this->panelMontantSubvention->setDisplay('Dynamic');
            $this->panelDateExecution->setDisplay('Dynamic');
        } else {
            $this->montant_lot_1_attributaire_1->setDisplay('Dynamic');
            $this->montant_lot_1_attributaire_1_concession->setDisplay('None');
            $this->info_bulle_valeur_globale_estimee->setDisplay('None');
            self::chargerProcedures();
            $this->panelMontantSubvention->setDisplay('None');
            $this->panelDateExecution->setDisplay('None');
        }
    }

    /**
     * Permet de charger les procédures de passation pour concession.
     */
    public function chargerProceduresConcessionPivot()
    {
        $procedures = Atexo_Consultation_ProcedureType::retrieveProcedureTypesConcessionPivot();
        $proc = [];
        $proc[0] = '--- '.Prado::localize('TEXT_TOUTES_PROCEDURES').' ---';
        foreach ($procedures as $procedure) {
            if ($procedure instanceof CommonTypeProcedureConcessionPivot) {
                $proc[$procedure->getId()] = (new Atexo_Consultation_ProcedureType())->retrieveTypeProcedurePivot($procedure->getId());
            }
        }
        ksort($proc);
        $this->procedurePassation->Datasource = $proc;
        $this->procedurePassation->dataBind();
    }
}
