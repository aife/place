<?php

namespace Application\Controls;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonChorusEchangePeer;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationFavorisQuery;
use Application\Propel\Mpe\CommonConsultationPeer;
use Application\Propel\Mpe\CommonDCE;
use Application\Propel\Mpe\CommonRG;
use Application\Propel\Mpe\CommonTCAOCommissionConsultation;
use Application\Propel\Mpe\CommonTCAOOrdreDePassage;
use Application\Propel\Mpe\CommonTCAOSeanceQuery;
use Application\Propel\Mpe\CommonTEspaceCollaboratif;
use Application\Service\Atexo\Agent\Atexo_Agent_EnablingsAgents;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_ConsultationRma;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_EspaceCollaboratif;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_ValeursReferentielles;
use Application\Service\Atexo\Chorus\Atexo_Chorus_CriteriaVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Dce;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Rg;
use Application\Service\Atexo\Publicite\Atexo_Publicite_Avis;
use Application\Service\Atexo\Publicite\Atexo_Publicite_Destinataire;
use Application\Service\Atexo\Publicite\Atexo_Publicite_FormatLibreVo;
use Exception;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class ResultSearch extends MpeTPage
{
    private $_typeConsultation;
    public $_calledFrom;
    protected $visionRma = false;
    protected $navigateUrl;

    protected $procedureAmont = false;
    protected $procedureAval = false;

    public function getTypeConsultation()
    {
        return $this->_typeConsultation;
    }

    public function setTypeConsultation($typeConsultation)
    {
        $this->_typeConsultation = $typeConsultation;
    }

    public function getCalledFrom()
    {
        return $this->_calledFrom;
    }

    public function setCalledFrom($value)
    {
        $this->_calledFrom = $value;
    }

    public function fillRepeaterWithDataForSearchResult($criteriaVo)
    {
        $this->visionRma = $criteriaVo->getVisionRma();
        if ($this->visionRma) {
            $consultation = new Atexo_ConsultationRma();
        } else {
            $consultation = new Atexo_Consultation();
        }
        $nombreElement = $consultation->search($criteriaVo, true);
        if ($nombreElement >= 1) {
            $this->panelMoreThanOneElementFound->setVisible(true);
            $this->panelNoElementFound->setVisible(false);
            $this->nombreElement->Text = $nombreElement;
            $this->PagerBottom->setVisible(true);
            $this->PagerTop->setVisible(true);
            $this->setViewState('nombreElement', $nombreElement);
            $this->nombrePageTop->Text = ceil($nombreElement / $this->tableauDeBordRepeater->PageSize);
            $this->nombrePageBottom->Text = ceil($nombreElement / $this->tableauDeBordRepeater->PageSize);
            $this->tableauDeBordRepeater->setVirtualItemCount($nombreElement);
            $this->tableauDeBordRepeater->setCurrentPageIndex(0);
            $this->populateData($criteriaVo);
        } else {
            $this->panelMoreThanOneElementFound->setVisible(false);
            $this->panelNoElementFound->setVisible(true);
            $this->PagerBottom->setVisible(false);
            $this->PagerTop->setVisible(false);
        }
    }

    public function populateData(Atexo_Consultation_CriteriaVo $criteriaVo)
    {
        $offset = $this->tableauDeBordRepeater->CurrentPageIndex * $this->tableauDeBordRepeater->PageSize;
        $limit = $this->tableauDeBordRepeater->PageSize;
        if ($offset + $limit > $this->tableauDeBordRepeater->getVirtualItemCount()) {
            $limit = $this->tableauDeBordRepeater->getVirtualItemCount() - $offset;
        }
        $this->visionRma = $criteriaVo->getVisionRma();
        if ($this->visionRma) {
            $consultation = new Atexo_ConsultationRma();
        } else {
            $consultation = new Atexo_Consultation();
        }
        $criteriaVo->setOffset($offset);
        $criteriaVo->setLimit($limit);
        $arrayConsultation = $consultation->search($criteriaVo);
        $this->tableauDeBordRepeater->DataSource = $arrayConsultation;
        $this->tableauDeBordRepeater->DataBind();
    }

    public function Trier($champsOrderBy)
    {
        $this->setViewState('sortByElement', $champsOrderBy);
        $criteriaVo = $this->Page->getViewState('CriteriaVo');
        $criteriaVo->setSortByElement($champsOrderBy);
        $arraySensTri = $this->Page->getViewState('sensTriArray', []);
        $arraySensTri[$champsOrderBy] = ('ASC' == $criteriaVo->getSensOrderBy()) ? 'DESC' : 'ASC';
        $criteriaVo->setSensOrderBy($arraySensTri[$champsOrderBy]);
        $this->Page->setViewState('sensTriArray', $arraySensTri);
        $this->Page->setViewState('CriteriaVo', $criteriaVo);
        $this->tableauDeBordRepeater->setCurrentPageIndex(0);
        $this->populateData($criteriaVo);
    }

    public function pageChanged($sender, $param)
    {
        $this->tableauDeBordRepeater->CurrentPageIndex = $param->NewPageIndex;
        $this->numPageBottom->Text = $param->NewPageIndex + 1;
        $this->numPageTop->Text = $param->NewPageIndex + 1;
        $criteriaVo = $this->Page->getViewState('CriteriaVo');
        $this->populateData($criteriaVo);
    }

    public function elementSortedWith($panelName, $openCloseTag)
    {
        if ($this->getViewState('sortByElement') == $panelName) {
            if ('open' == $openCloseTag) {
                return '<strong>';
            } else {
                return '</strong>';
            }
        } else {
            return '';
        }
    }

    public function goToPage($sender, $param)
    {
        switch ($sender->ID) {
            case 'DefaultButtonTop':
                 $numPage = $this->numPageTop->Text;
                break;
            case 'DefaultButtonBottom':
                 $numPage = $this->numPageBottom->Text;
                break;
        }
        if (Atexo_Util::isEntier($numPage)) {
            if ($numPage >= $this->nombrePageTop->Text) {
                $numPage = $this->nombrePageTop->Text;
            } elseif ($numPage <= 0) {
                $numPage = 1;
            }
            self::updatePagination($this->nombreResultatAfficherTop->getSelectedValue(), $numPage);
            $criteriaVo = $this->Page->getViewState('CriteriaVo');
            $this->populateData($criteriaVo);
        } else {
            $this->numPageTop->Text = $this->tableauDeBordRepeater->CurrentPageIndex + 1;
            $this->numPageBottom->Text = $this->tableauDeBordRepeater->CurrentPageIndex + 1;
        }
    }

    public function changePagerLenght($sender, $param)
    {
        switch ($sender->ID) {
            case 'nombreResultatAfficherBottom':
                 $pageSize = $this->nombreResultatAfficherBottom->getSelectedValue();
                                                  $this->nombreResultatAfficherTop->setSelectedValue($pageSize);
                break;
            case 'nombreResultatAfficherTop':
                 $pageSize = $this->nombreResultatAfficherTop->getSelectedValue();
                                               $this->nombreResultatAfficherBottom->setSelectedValue($pageSize);
                break;
        }
        self::updatePagination($pageSize, $this->numPageTop->Text);
        $criteriaVo = $this->Page->getViewState('CriteriaVo');
        $this->populateData($criteriaVo);
    }

    public function getTrueOrFalse($bool)
    {
        return $bool;
    }

    public function infoBulleEtapeConnsultation($statusConsultation)
    {
        if ($statusConsultation == Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE')) {
            if ($this->isTypeConsultation()) {
                return Prado::localize('ETAPE_OUVERTURE_ET_ANALYSE');
            } else {
                return Prado::localize('HORS_LIGNE');
            }
        }
        if ($statusConsultation == Atexo_Config::getParameter('STATUS_ELABORATION')) {
            if ($this->isTypeConsultation()) {
                return Prado::localize('ETAPE_ELABORATION');
            } else {
                return Prado::localize('DEFINE_PRESENTATION');
            }
        }
        if ($statusConsultation == Atexo_Config::getParameter('STATUS_PREPARATION')) {
            if ($this->isTypeConsultation()) {
                return Prado::localize('DEFINE_ATTENTE_VALIDATION');
            } else {
                return Prado::localize('DEFINE_PRESENTATION');
            }
        }
        if ($statusConsultation == Atexo_Config::getParameter('STATUS_CONSULTATION')) {
            if ($this->isTypeConsultation()) {
                return Prado::localize('ETAPE_CONSULTATION');
            } else {
                return Prado::localize('EN_LIGNE');
            }
        }
        if ($statusConsultation == Atexo_Config::getParameter('STATUS_DECISION')) {
            return Prado::localize('ETAPE_DECISION');
        }

        if ($statusConsultation == Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE').Atexo_Config::getParameter('STATUS_DECISION')) {
            return Prado::localize('TEXT_ETAPE_O_A_D');
        }

        if ($statusConsultation == Atexo_Config::getParameter('STATUS_CONSULTATION').Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE').Atexo_Config::getParameter('STATUS_DECISION')) {
            return Prado::localize('TEXT_ETAPE_C_O_A_D');
        }
    }

    /** Pour inserer un retour a la ligne (<br>), tous les 22 caracteres, dans une chaine.
     * @param : $texte : texte dans lequel sera ajouter les <br>
     *
     * @return : retourn la chaine apres y avoir inserer un retour a la lligne tous les $everyHowManyCaracter caracteres
     */
    public function insertBrWhenTextTooLong($texte, $everyHowManyCaracter)
    {
        return wordwrap($texte, $everyHowManyCaracter, '<br>', true);
    }

    public function truncate($texte)
    {
        $maximumNumberOfCaracterToReturn = 230;
        $arrayText = explode(' ', $texte);
        $indexArrayText = 0;
        $textlenght = strlen($arrayText[$indexArrayText]) + 1;
        $truncatedText = $arrayText[$indexArrayText];
        while (isset($arrayText[$indexArrayText]) && $textlenght <= $maximumNumberOfCaracterToReturn) {
            ++$indexArrayText;
            if ($textlenght + (strlen($arrayText[$indexArrayText]) + 1) > $maximumNumberOfCaracterToReturn) {
                break;
            }
            $textlenght += strlen($arrayText[$indexArrayText]) + 1;
            $truncatedText .= ' '.$arrayText[$indexArrayText];
        }

        return $truncatedText;
    }

    public function isTextTruncated($texte, $nbrCaractere = 100)
    {
        return (strlen($texte) > 230) ? '' : 'display:none';
    }

    public function redirectToRegistreRetrait($sender, $param)
    {
        $reference = $param->CommandName;
        $this->response->redirect('index.php?page=Agent.GestionRegistres&id='.$reference.'&type=1');
    }

    public function redirectToRegistreQuestion($sender, $param)
    {
        $reference = $param->CommandName;
        $this->response->redirect('index.php?page=Agent.GestionRegistres&id='.$reference.'&type=3');
    }

    public function redirectToRegistreDepot($sender, $param)
    {
        $reference = $param->CommandName;
        $this->response->redirect('index.php?page=Agent.GestionRegistres&id='.$reference.'&type=5');
    }

    public function isTypeConsultation()
    {
        return 'consultation' == $this->getTypeConsultation();
    }

    public function isTypeAnnonce()
    {
        return 'Annonce' == $this->getTypeConsultation();
    }

    public function deleteAvis($sender, $param)
    {
        try {
            $refConsultation = $param->CommandParameter;
            $organisme = Atexo_CurrentUser::getCurrentOrganism();

            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $consultationObj = CommonConsultationPeer::retrieveByPK($refConsultation, $connexionCom);
            if ($consultationObj) {
                if ($consultationObj->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_INFORMATION')) {
                    $typeAnnonce = Atexo_Config::getParameter('TYPE_AVIS_INFORMATION');
                } elseif ($consultationObj->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_ATTRIBUTION')) {
                    $typeAnnonce = Atexo_Config::getParameter('TYPE_AVIS_ATTRIBUTION');
                } else {
                    $typeAnnonce = $consultationObj->getIdTypeAvis();
                }
            }
            $consultation = new Atexo_Consultation();
            $consultation->deleteConsultation($refConsultation, $organisme);
        } catch (Exception $e) {
            Prado::log('Erreur ResultSearch.php'.$e->getMessage().$e->getTraceAsString(), TLogger::ERROR, 'ResultSearch.php');
        }

        $this->response->redirect('index.php?page=Agent.TableauDeBordAnnonce&typeAnnonce='.$typeAnnonce);
    }

    public function GoToAvis()
    {
        return 'index.php?page=Agent.PubliciteConsultation&id=';
    }

    public function getNbrColHead($boolean)
    {
        if ('agent' == strtolower(Atexo_CurrentUser::getRole()) && $this->isTypeConsultation()) {
            return true == $boolean ? true : '8';
        } else {
            return true == $boolean ? false : '6';
        }
    }

    public function ajouterOrdreDuJour($sender, $param)
    {
        $arrayCommande = explode('@', $param->CommandParameter);
        $consultationId = $arrayCommande[0];
        $organisme = $arrayCommande[1];
        $idSeance = $arrayCommande[2];

        //Recuperation de la consultation
        $consultation = (new Atexo_Consultation())->retrieveConsultationByRefForCAO($consultationId);

        //Recuperation de la seance
        $seance = CommonTCAOSeanceQuery::create()
              ->findPk([base64_decode(Atexo_Util::atexoHtmlEntities($idSeance)), $organisme]);

        if ($consultation && $seance) {
            //Creation d'une consultation de commission
            $commission_consultation = new CommonTCAOCommissionConsultation();
            $commission_consultation->setIdCommission($seance->getIdCommission());
            $commission_consultation->setConsultationId($consultation['reference_utilisateur']);
            $commission_consultation->setOrganisme($organisme);
            $commission_consultation->setIntitule($consultation['objet']);
            $commission_consultation->setIdTypeProcedure($consultation['id_type_procedure_org']);
            $commission_consultation->setIdCategorie($consultation['categorie']);
            $commission_consultation->setIdServiceGestionnaire($consultation['service_id']);
            $commission_consultation->setIdServiceAssocie($consultation['service_associe_id']);
            $commission_consultation->setDateCloture($consultation['datefin']);

            if ($commission_consultation->save()) {
                //Ajout de la consultation a l'ordre du jour de la seance
                $ordre_de_passage = new CommonTCAOOrdreDePassage();
                $ordre_de_passage->setOrganisme($organisme);
                $ordre_de_passage->setIdSeance($seance->getIdSeance());
                $ordre_de_passage->setIdCommission($seance->getIdCommission());
                $ordre_de_passage->setIdCommissionConsultation($commission_consultation->getIdCommissionConsultation());
                $ordre_de_passage->setDateSeance($seance->getDate());
                $ordre_de_passage->setDatePassage($seance->getDate());
                $dataSource = (new Atexo_ValeursReferentielles())->getValeurRefByIdRef(Atexo_Config::getParameter('ID_REFERENTIEL_ETAPES_ORDRES_DU_JOUR'));
                if (is_array($dataSource) && count($dataSource)) {
                    $data = '';
                    foreach ($dataSource as $key => $data) {
                        if ($key) {
                            $ordre_de_passage->setIdRefOrgValEtape($key);
                            break;
                        }
                    }
                }

                $ordre_de_passage->save();
            }
        }

        //Redirection vers la page d'appel
        $this->response->redirect('?page=Commission.OrdreDuJour&id='.$idSeance);
    }

    /*
     * Permet de mettre a jour la pagination
     */
    public function updatePagination($pageSize, $numPage)
    {
        $nombreElement = $this->getViewState('nombreElement');
        $this->tableauDeBordRepeater->PageSize = $pageSize;
        $this->nombrePageTop->Text = ceil($nombreElement / $this->tableauDeBordRepeater->PageSize);
        $this->nombrePageBottom->Text = ceil($nombreElement / $this->tableauDeBordRepeater->PageSize);
        if ($numPage > $this->nombrePageTop->Text) {
            $numPage = $this->nombrePageTop->Text;
        }
        $this->tableauDeBordRepeater->setCurrentPageIndex($numPage - 1);
        $this->numPageBottom->Text = $numPage;
        $this->numPageTop->Text = $numPage;
    }

    /*
     * Permet de gerer la visibile de l icone de creation d espace collaboratif
     */
    public function DisplayIconeCreerEspaceCollaboratif($consultationId, $Org)
    {
        if (Atexo_Module::isEnabled('EspaceCollaboratif')) {
            $siteExist = (new CommonTEspaceCollaboratif())->espaceCollaboratifExiste($consultationId, $Org);
            if ((new Atexo_Agent_EnablingsAgents())->isHabilitationAgentExiste(
                Atexo_CurrentUser::getIdAgentConnected(),
                'EspaceCollaboratifGestionnaire'
            )
                && !$siteExist
            ) {
                return true;
            }
        }

        return false;
    }

    /*
    * Permet de gerer la visibile de l icone d'acces a l'espace collaboratif
    */
    public function DisplayIconeConsulterEspaceCollaboratif($consultationId, $Org)
    {
        if (Atexo_Module::isEnabled('EspaceCollaboratif')) {
            $siteExist = (new CommonTEspaceCollaboratif())->espaceCollaboratifExiste($consultationId, $Org);
            if ((
                    (new Atexo_Agent_EnablingsAgents())->isHabilitationAgentExiste(
                        Atexo_CurrentUser::getIdAgentConnected(),
                        'EspaceCollaboratifGestionnaire'
                    )
                    || (new Atexo_Agent_EnablingsAgents())->isHabilitationAgentExiste(
                        Atexo_CurrentUser::getIdAgentConnected(),
                        'EspaceCollaboratifContributeur'
                    )
                )
                && $siteExist
            ) {
                return true;
            }
        }

        return false;
    }

    /*
     * redirige vers le site de la consultation cree sur Alfresco
     */
    public function accederEspaceCollaboratif($sender, $param)
    {
        if (Atexo_Module::isEnabled('EspaceCollaboratif')) {
            $arrayCommande = explode('@', $param->CommandParameter);
            $consultationId = $arrayCommande[0];
            //$organisme = $arrayCommande[1];
            $consultation = Atexo_Consultation::retrieveConsultationByRefConsultation($consultationId);
            Atexo_EspaceCollaboratif::createConsultationSite($consultation);
            $a = str_replace('consulter', '', $sender->getClientId());
            $this->scriptJS->Text = "<script language='javascript'>".
                                         "window.document.getElementById('".$a."hiddenConsulter').click();".
                                   '</script>';
            $criteriaVo = $this->Page->getViewState('CriteriaVo');
            self::fillRepeaterWithDataForSearchResult($criteriaVo);
        }
    }

    public function getUrlAccesEspaceCollaboratif($consultationId, $organisme)
    {
        $shortName = $organisme.$consultationId;
        $url = str_replace('#shortName#', $shortName, Atexo_Config::getParameter('URL_ALFRESCO'));

        return $url;
    }

    public function getUrlCreerEspaceCollaboratif($consultationId, $Org, $index)
    {
        return "javascript:popUp('index.php?page=Agent.popUpCreationEspaceCollaboratif".
               '&id='.base64_encode($consultationId).
               "&orgAcronyme=$Org".
               "&response=$index','yes');";
    }

    /**
     * Permet de verifier si l'appel de la page se fait depuis le service metier CAO (Commission).
     *
     * @return bool : true si l'appel se fait depuis Commission, false sinon
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2015
     */
    public function isCallFromCommission()
    {
        $arrayParamRequest = explode('.', $this->getRequest()->getServiceParameter());

        return 'page' == $this->getRequest()->getServiceId() && 'commission' == $arrayParamRequest[0];
    }

    protected function getLibelleEntiteAchat($idService, $organisme)
    {
        return Atexo_EntityPurchase::getPathEntityById($idService, $organisme, Atexo_CurrentUser::readFromSession('lang'));
    }

    protected function getDceText($consultationId, $organisme)
    {
        $dce = Atexo_Consultation_Dce::getDce($consultationId, $organisme);
        if ($dce instanceof CommonDCE && $dce->getDce()) {
            $this->navigateUrl['dce'][$consultationId] = 'index.php?page=Agent.DownloadDceRma&id='.base64_encode($consultationId).'&org='.base64_encode($organisme);
            $tailleDce = Atexo_Util::arrondirSizeFile((new Atexo_Blob())->getTailFile($dce->getDce(), $organisme) / 1024);

            return Prado::localize('DEFINE_DCE').' - '.$tailleDce;
        }
        $this->navigateUrl['dce'][$consultationId] = '';

        return '-';
    }

    protected function getDceNavigateUrl($consultationId)
    {
        return $this->navigateUrl['dce'][$consultationId];
    }

    protected function getRcText($consultationId, $organisme)
    {
        $rg = Atexo_Consultation_Rg::getReglement($consultationId, $organisme);
        if ($rg instanceof CommonRG && $rg->getRg()) {
            $this->navigateUrl['rc'][$consultationId] = 'index.php?page=Agent.DownloadReglementRma&id='.base64_encode($consultationId).'&org='.base64_encode($organisme);
            $tailleRg = Atexo_Util::arrondirSizeFile((new Atexo_Blob())->getTailFile($rg->getRg(), $organisme) / 1024);

            return Prado::localize('DEFINE_RC').' '.'- '.$tailleRg;
        }
        $this->navigateUrl['rc'][$consultationId] = '';

        return '-';
    }

    protected function getRcNavigateUrl($referenceConsult)
    {
        return $this->navigateUrl['rc'][$referenceConsult];
    }

    protected function getDataSourceAvis($consultation)
    {
        return $this->navigateUrl['boamp'][$consultation->getId().'_'.$consultation->getOrganisme()]['dataSource'];
    }

    protected function isUrlAvisBoampVisible($consultation)
    {
        $consultationId = $consultation->getId();
        $organisme = $consultation->getOrganisme();
        $listeFormAvis = Atexo_Publicite_Avis::retreiveListAvisEnvoye($consultationId, $organisme);
        $dataSource = [];
        $index = 0;
        $atexoBlob = new Atexo_Blob();
        foreach ($listeFormAvis as $oneAvis) {
            $newFormLibre = new Atexo_Publicite_FormatLibreVo();
            if ($oneAvis->getType() == Atexo_Config::getParameter('TYPE_FORMAT_LIBRE_FICHIER_JOINT') || $oneAvis->getType() == Atexo_Config::getParameter('TYPE_FORMAT_AVIS_IMPORTES')) {
                if ($oneAvis->getTypeDocGenere() == Atexo_Config::getParameter('TYPE_DOC_GENERE_AVIS')) {
                    if ($oneAvis->getType() == Atexo_Config::getParameter('TYPE_FORMAT_AVIS_IMPORTES')) {
                        $newFormLibre->setLibelleType(Prado::localize('TEXT_FICHIER_JOINT_AVIS_PUB'));
                    } else {
                        $newFormLibre->setLibelleType(Prado::localize('TEXT_FICHIER_JOINT_AVIS_PUBLICITE').' - '.strtoupper($oneAvis->getLangue()));
                    }
                } else {
                    $newFormLibre->setLibelleType(Prado::localize('TEXT_FICHIER_JOINT_AVIS_COMPLEMENTAIRE'));
                }
                $newFormLibre->setTypeFormat(Atexo_Config::getParameter('TYPE_FORMAT_LIBRE_FICHIER_JOINT'));
                $taille = Atexo_Util::arrondirSizeFile($atexoBlob->getTailFile($oneAvis->getAvis(), $organisme) / 1024);
                $newFormLibre->setTaille($taille);
            } else {
                $newFormLibre->setLibelleType(Prado::localize('TEXT_URL_ACCES_DIRECT'));
                $newFormLibre->setTypeFormat(Atexo_Config::getParameter('TYPE_FORMAT_LIBRE_URL'));
                $newFormLibre->setUrl($oneAvis->getUrl());
            }
            $newFormLibre->setId($oneAvis->getId());
            $newFormLibre->setDatePub(Atexo_Util::iso2frnDateTime($oneAvis->getDateCreation(), false));
            $newFormLibre->setStatut($oneAvis->getStatut());
            $dataSource[$index] = $newFormLibre;
            ++$index;
        }
        // Ajouter les liens PDF au BOAMP dans le data source
        $listLienPdfBoamp = Atexo_Publicite_Destinataire::retrieveAnnonceByRefCons($consultationId, $organisme);
        $dataSourceBoamp = [];
        $indexX = 0;
        foreach ($listLienPdfBoamp as $oneAnnonce) {
            if ('' != $oneAnnonce->getLienBoamp()) {
                $newFormxml = new Atexo_Publicite_FormatLibreVo();
                $newFormxml->setLibelleType(Prado::localize('NOM_LIEN_AVIS_BOAMP'));
                $newFormxml->setTypeFormat(Atexo_Config::getParameter('TYPE_FORMAT_URL_BOAMP'));
                $newFormxml->setUrl($oneAnnonce->getLienBoamp());
                $newFormxml->setStatut($oneAnnonce->getStatutDestinataire());
                $dataSourceBoamp[$indexX] = $newFormxml;
                ++$indexX;
            }
        }

        $avis = array_merge($dataSource, $dataSourceBoamp);
        if (isset($avis) && count($avis)) {
            $this->navigateUrl['boamp'][$consultationId.'_'.$organisme]['text'] = count($avis).' '.Prado::localize('MIN_AVIS_DE_PUBLICITE');
            $this->navigateUrl['boamp'][$consultationId.'_'.$organisme]['dataSource'] = $avis;

            return true;
        }
        $this->navigateUrl['boamp'][$consultationId.'_'.$organisme]['dataSource'] = [];

        return false;
    }

    protected function getTextAvisBoamp($consultationId, $organisme)
    {
        return $this->navigateUrl['boamp'][$consultationId.'_'.$organisme]['text'];
    }

    protected function getTextLots($consultationObj)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $consultationObj = CommonConsultationPeer::retrieveByPK($consultationObj->getId(), $connexionCom);
        if ($consultationObj instanceof CommonConsultation) {
            return $consultationObj->getAllLots($connexionCom, null, null, true).' Lot(s)';
        }

        return '-';
    }

    protected function getUrlLienLots($consultationObj)
    {
        $organisme = $consultationObj->getOrganisme();
        $consultationId = $consultationObj->getId();

        return "javascript:popUp('index.php?page=Agent.PopupDetailLot&orgAccronyme=$organisme&id=$consultationId','yes');";
    }

    protected function getNbChorusEchanges($consultation)
    {
        try {
            $criteriaVo = new Atexo_Chorus_CriteriaVo();
            $criteriaVo->setOrganisme($consultation->getOrganisme());
            $criteriaVo->setReferenceConsultation($consultation->getId());
            $criteriaVo->setCount(true);

            $chorusEchangePeer = new CommonChorusEchangePeer();

            return $chorusEchangePeer->getChorusEchangesByCriteres($criteriaVo);
        } catch (Exception $e) {
            return $e->getMessage();
            Prado::log(" Erreur lors de la recuperation du nombre d'echange chorus envoye, ref consultation : "
                       .$consultation->getId().' - Organisme : '.$consultation->getOrganisme().'  ===> message : '.$e->getMessage());

            return '-';
        }
    }

    protected function getVisionRma()
    {
        return $this->visionRma;
    }

    public function isConsultationFavoris($consultation)
    {
        $bool = false;

        try {
            $connexionCom = Propel::getConnection(
                Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY')
            );

            $favoris = new CommonConsultationFavorisQuery();
            $result = $favoris->findOneByArray(['idConsultation' => $consultation->getId(), 'idAgent' => Atexo_CurrentUser::getId()], $connexionCom);

            if (null !== $result) {
                $bool = true;
            }
        } catch (Exception $e) {
            Prado::log(' Erreur lors de la récupèration du statut favoris : '
                .$consultation->getId().' - Organisme : '.$consultation->getOrganisme().'  ===> message : '.$e->getMessage());
        }

        return $bool;
    }

    public function masquerPagination()
    {
        $this->panelMoreThanOneElementFound->setVisible(false);
        $this->panelNoElementFound->setVisible(true);
        $this->PagerBottom->setVisible(false);
        $this->PagerTop->setVisible(false);
    }
}
