<?php

namespace Application\Controls;

class ActivePanelMessageErreur extends MpeTPage
{
    public function setMessage($message)
    {
        $this->labelMessage->Text = $message;
    }

    public function showMessage()
    {
        $this->labelJavascript->Text = "<script>document.getElementById('ctl0_CONTENU_PAGE_".$this->ID."_panelMessage').style.display='block';</script>";
    }

    public function hideMessage()
    {
        $this->labelJavascript->Text = "<script>document.getElementById('ctl0_CONTENU_PAGE_".$this->ID."_panelMessage').style.display='none';</script>";
    }
}
