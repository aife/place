<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Util;

/**
 * permet d'afficher un warning d'interdistion d'utilisation des applet par GoogleChrome.
 *
 * @author ASO <ayoub.souidahmed@atexo.com>
 *
 * @version 1.0
 *
 * @since esr-2015
 *
 * @copyright Atexo 2015
 */
class WarningChrome extends MpeTTemplateControl
{
    /**
     * initialiser le composant.
     *
     * @return int la valeur courante [entrepriseExiste]
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function initiComposant($message)
    {
        if (!Atexo_Config::getParameter('ACTIVATION_SERVEUR_CRYPTOGRAPHIQUE')) {
            $infosBrowser = Atexo_Util::getInfosBrowserVisiteur();
            if ((str_contains($infosBrowser['name'], 'Chrome') || (str_contains($infosBrowser['name'], 'Firefox') && $infosBrowser['version'] >= 52))
                && '1' == Atexo_Config::getParameter('SHOW_WARNING_CHROME')
                && !$_SESSION['showWarningChrome']
            ) {
                $this->scriptJs->text = "<script>
                                        //openModal('message-information-chrome','modal-form popup-small2',document.getElementById('modalWarningChrome'));
                                        jQuery(document).ready(function () {
                                            KTJS.pages_entreprise_common.common.openModal('#modalWarningChrome');
                                        });
                                     </script>";
                $this->popinMessageWarningChrome->setMessage($message);
                $_SESSION['showWarningChrome'] = true;
            } elseif ($_SESSION['showWarningChrome']) {
                $this->scriptJs->text = "<script>
                                        document.getElementById('divWarningChrome').style.display='';
                                     </script>";
            }
        }
    }
}
