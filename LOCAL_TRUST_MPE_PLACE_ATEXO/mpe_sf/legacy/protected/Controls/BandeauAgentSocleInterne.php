<?php

namespace Application\Controls;

use App\Service\Bandeau\BandeauAgentSocleInterne as BandeauAgentSocleInterneService;
use Application\Service\Atexo\Atexo_Util;

class BandeauAgentSocleInterne extends MpeTTemplateControl
{
    public function render($writer)
    {
        /** @var BandeauAgentSocleInterneService $menu */
        $menu = Atexo_Util::getSfService(BandeauAgentSocleInterneService::class);

        echo $menu->render();
    }
}
