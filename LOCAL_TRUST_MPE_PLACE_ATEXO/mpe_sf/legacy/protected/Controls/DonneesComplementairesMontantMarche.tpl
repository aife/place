<com:TActivePanel id="donneeMontantMarche" cssClass="toggle-panel form-toggle donnees-redac" style="display:<%=($this->getAfficher() && Application\Service\Atexo\Atexo_Module::isEnabled('ConfPubliciteFrancaise'))? '':'none'%>">
<div class="content">
	<div class="cover"></div>
	<a class="title-toggle-open" onclick="togglePanel(this,'<%=$this->panelDuree->getClientId()%>');" href="javascript:void(0);"><com:TTranslate>DEFINE_DONNEES_COMP_MONTANT_MARCHE</com:TTranslate></a>
	<com:TPanel cssClass="panel no-indent panel-donnees-complementaires" style="display:block;" id="panelDuree">
		<!--Debut Ligne Dureee/delai-->
		<com:TActivePanel cssClass="line">
			<div class="intitule col-300"><com:TTranslate>TEXT_PUBLICATION_MONTANT</com:TTranslate> :</div>
			<div class="content-bloc margin-top-5">
				<div class="intitule-60">
					<com:TActiveRadioButton
							Enabled="<%=$this->enabledElement()%>"
							GroupName="intitulePubliciteMontant"
							Id="intitulePubliciteMontantOui"
							cssClass="radio"
							Attributes.onclick="isCheckedShowDiv(this, '<%=$this->blocValeurMarchePubliee->getClientId()%>')"/>
					<label for="intitulePubliciteOui"><com:TTranslate>DEFINE_OUI</com:TTranslate></label>
				</div>
				<div class="intitule-120">
					<com:TActiveRadioButton
							Enabled="<%=$this->enabledElement()%>"
							GroupName="intitulePubliciteMontant"
							Id="intitulePubliciteMontantNon"
							cssClass="radio"
							Attributes.onclick="isCheckedHideDiv(this, '<%=$this->blocValeurMarchePubliee->getClientId()%>')"/>
					<label for="intitulePubliciteNon"><com:TTranslate>DEFINE_NON</com:TTranslate></label>
				</div>
				<com:TPanel cssClass="intitule-auto" id="blocValeurMarchePubliee">
					<span class="intitule-auto"><%=($this->getSourcePage() == 'etapeIdentification') ? Prado::localize('DEFINE_VALEUR_DU_MARCHE_PUBLIEE'): Prado::localize('DEFINE_VALEUR_DU_MARCHE_PUBLIEE_LOT') %> :</span>
					<com:TActiveTextBox
							Id="valeurMarchePubliee"
							Attributes.onblur="formatterMontant('<%=$this->valeurMarchePubliee->getClientId()%>');"
							cssClass="long montant"
							Enabled="<%=$this->enabledElement()%>"
							Attributes.title="<%=Prado::localize('DEFINE_MONTANT_ESTIMATIF_CONTRAT')%>"/>
					<com:TTranslate>DEFINE_EURO_HT</com:TTranslate>
				</com:TPanel>
			</div>
		</com:TActivePanel>
	</com:TPanel>
	<div class="breaker"></div>
	<com:TLabel ID="scriptLabel" style="display:none" />
</div>
</com:TActivePanel>