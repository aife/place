<com:TConditional condition="$this->TemplateControl->getVisible()">
    <prop:trueTemplate>
        <div class="modal kt-ajaxmodal fade" id="modalMyCart" tabindex="-1" role="dialog" aria-labelledby="modalMyCart">
            <div class="modal-dialog" role="document">
                <form class="form" action="" method="GET">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel"><%=Prado::localize('DEFINE_AJOUTER_PANIER')%></h4>
                        </div>
                        <div class="modal-body">
                            <div class="text-success"><%=$this->getMessage()%></div>
                        </div>
                        <div class="modal-footer">
                            <a href="/?page=Entreprise.EntrepriseAdvancedSearch&panierEntreprise" class="btn btn-default btn-reset pull-left">
                                <%=Prado::localize('DEFINE_VOIR_MON_PANIER')%>
                            </a>

                            <button type="button" class="btn btn-primary btn-submit" data-dismiss="modal">
                                <%=Prado::localize('TEXT_POURSUIVRE')%>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </prop:trueTemplate>
</com:TConditional>