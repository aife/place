<?php

namespace Application\Controls;

use App\Utils\Encryption;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Pages\Commun\Classes\Util;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\Om\BaseCommonRelationEchangePeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Exception;
use Application\Service\Atexo\Atexo_MultiDomaine;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Register;
use Application\Service\Atexo\Consultation\Atexo_Consultation_RegistreVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Responses;
use Prado\Prado;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class Registres extends MpeTTemplateControl
{
    private $_reference;
    private $_typeRegistre;
    private bool $_visible = true;
    private string $_pictoActive = 'true';
    private $_idUnique;
    private $_consultation;

    public function getReference()
    {
        return $this->_reference;
    }

    public function setReference($refrence)
    {
        $this->_reference = $refrence;
        $this->setViewState('reference'.$this->ID, $refrence);
        $this->setViewState('reference', $refrence);
    }

    public function getTypeRegistre()
    {
        return $this->_typeRegistre;
    }

    public function setTypeRegistre($typeRegistre)
    {
        $this->_typeRegistre = $typeRegistre;
        $this->setViewState('typeRegistre', $typeRegistre);
    }

    public function getVisible($checkParents = true)
    {
        return $this->_visible;
    }

    public function setVisible($visible)
    {
        $this->_visible = $visible;
    }

    public function getPictoActive()
    {
        return $this->_pictoActive;
    }

    public function setPictoActive($pictoActive)
    {
        $this->_pictoActive = $pictoActive;
    }

    public function getIdUnique()
    {
        return $this->_idUnique;
    }

    /**
     * @return mixed
     */
    public function getConsultation()
    {
        $consultation = $this->getViewState('consultation');
        if(!$consultation instanceof CommonConsultation) {
            $critere = new Atexo_Consultation_CriteriaVo();
            $critere->setIdReference($this->getViewState('reference'));
            $critere->setAcronymeOrganisme(Atexo_CurrentUser::getOrganismAcronym());
            $critere->setIdService(Atexo_CurrentUser::getCurrentServiceId());
            $critere->setConnectedAgentId(Atexo_CurrentUser::getId());
            $listeConsultations = (new Atexo_Consultation())->search($critere);
            if (is_array($listeConsultations) && 1 == count($listeConsultations) && $listeConsultations[0] instanceof CommonConsultation) {
                $this->setConsultation($listeConsultations[0]);
            }
        }
        return $this->getViewState('consultation');
    }

    /**
     * @param mixed $consultation
     * @return Registres
     */
    public function setConsultation($consultation)
    {
        $this->setViewState('consultation', $consultation);
    }

    public function onLoad($param)
    {
        $this->Page->scriptJs->Text = '';
        //         $this->_idUnique = $this->getId();
        $this->Page->setViewState('idunique', $this->getId());

        if (!$this->Page->IsPostBack) {
            $this->setViewState('reference', $this->_reference);

            if ($this->_visible) {
                if (!$this->_reference || !is_numeric($this->_reference)) {
                    return; //throw new Atexo_Exception('Expected Parameter reference to be an int');
                }

                if (!$this->_typeRegistre) {
                    return; //throw new Atexo_Exception("You must precise 'typeRetrait'");
                }
                $this->fillDataList($this->_typeRegistre);
            } else {
                $this->_reference = $this->getViewState('reference');
                $this->panelNombreResulat->setVisible(false);
                $this->labelAjouterRetraitPapier->setVisible(false);
            }

        }
        $this->_typeRegistre = $this->getViewState('typeRegistre');
        $this->_reference = $this->getViewState('reference');
    }

    public function fillDataList($typeRegistre, $indexPage = null)
    {
        $this->javaScript->Text = '';
        if (null === $indexPage) {
            $indexPage = $this->numPageTop->text - 1;
        }
        if ($this->_visible) {
            $this->setViewState('typeRegistre', $typeRegistre);
            $nombreResultat = $this->countData();
            if ($nombreResultat >= 1) {
                $this->panelNoElementFound->setVisible(false);
                $this->nombreResultat->Text = $nombreResultat;
                $this->PagerBottom->setVisible(true);
                $this->PagerTop->setVisible(true);
                $this->setViewState('nombreResultat', $nombreResultat);
                $this->nombrePageTop->Text = ceil($nombreResultat / $this->dataListRegistre->PageSize);
                $this->nombrePageBottom->Text = ceil($nombreResultat / $this->dataListRegistre->PageSize);
                $this->dataListRegistre->setVirtualItemCount($nombreResultat);
                $this->dataListRegistre->setCurrentPageIndex($indexPage);
                $this->populateData();
                $this->panelListRegistre->setStyle('display:block');
            } else {
                //$this->javaScript->Text="<script>document.getElementById('ctl0_CONTENU_PAGE_".$this->Id."_panelListRegistre').style.display='none'</script>";
                $this->panelListRegistre->setStyle('display:none');
                $this->dataListRegistre->DataSource = [];
                $this->dataListRegistre->DataBind();
            }
        }
    }

    public function populateData()
    {
        $tri = [];
        $offset = ($this->numPageTop->Text - 1) * $this->dataListRegistre->PageSize;
        $limit = $this->dataListRegistre->PageSize;
        if ($offset + $limit > $this->dataListRegistre->getVirtualItemCount()) {
            $limit = $this->dataListRegistre->getVirtualItemCount() - $offset;
        }
        $typeRegistre = $this->getViewState('typeRegistre');
        $this->_typeRegistre = $typeRegistre;
        $this->setViewState('typeRegistre', $typeRegistre);

        $this->panelNombreResulat->setVisible(true);
        $tri['sens'] = $this->getViewState('sensTri', '');
        if (1 == Atexo_Config::getParameter('ORDRE_AFFICHAGE_DEFAUT_REGISTRES')) {
            $tri['sensTriDefaut'] = $this->getViewState('sensTriDefaut', 'ASC');
        } elseif (0 == Atexo_Config::getParameter('ORDRE_AFFICHAGE_DEFAUT_REGISTRES')) {
            $tri['sensTriDefaut'] = $this->getViewState('sensTriDefaut', 'DESC');
        }
        $tri['trierPar'] = $this->getViewState('tri');
        if ($typeRegistre == Atexo_Config::getParameter('REGISTRE_RETRAIT_ELECTRONIQUE')) {
            $dataSource = (new Atexo_Consultation_Register())->retrieveRetraitsElectroniques($this->getViewState('reference'), $tri, Atexo_CurrentUser::getCurrentOrganism(), false, $limit, $offset);
            $this->textNombreRegistre->Text = Prado::localize('NOMBRE_RETRAIT_ELECRONIQUE');
        } elseif ($typeRegistre == Atexo_Config::getParameter('REGISTRE_RETRAIT_PAPIER')) {
            $dataSource = (new Atexo_Consultation_Register())->retrieveRetraitsPapier($this->getViewState('reference'), $tri, Atexo_CurrentUser::getCurrentOrganism(), true, false, $limit, $offset);
            $this->textNombreRegistre->Text = Prado::localize('NOMBRE_RETRAIT_PAPIER');
        } elseif ($typeRegistre == Atexo_Config::getParameter('REGISTRE_QUESTION_ELECTRONIQUE')) {
            $dataSource = (new Atexo_Consultation_Register())->retrieveQuestions($this->getViewState('reference'), $tri, Atexo_Config::getParameter('TYPE_DEPOT_QUESTION_ELECTRONIQUE'), Atexo_CurrentUser::getCurrentOrganism(), true, false, $limit, $offset);
            $this->textNombreRegistre->Text = Prado::localize('NOMBRE_QUESTION_ELECTRONIQUE');
        } elseif ($typeRegistre == Atexo_Config::getParameter('REGISTRE_QUESTION_PAPIER')) {
            $dataSource = (new Atexo_Consultation_Register())->retrieveQuestions($this->getViewState('reference'), $tri, Atexo_Config::getParameter('TYPE_DEPOT_QUESTION_PAPIER'), Atexo_CurrentUser::getCurrentOrganism(), true, false, $limit, $offset);
            $this->textNombreRegistre->Text = Prado::localize('NOMBRE_QUESTION_PAPIER');
        } elseif ($typeRegistre == Atexo_Config::getParameter('REGISTRE_DEPOT_ELECTRONIQUE')) {
            $dataSource = (new Atexo_Consultation_Register())->retrieveOffreElectronique($this->getViewState('reference'), $tri, Atexo_CurrentUser::getCurrentOrganism(), false, $limit, $offset);
            $this->textNombreRegistre->Text = Prado::localize('NOMBRE_DEPOT_ELECTRONIQUE');
        } elseif ($typeRegistre == Atexo_Config::getParameter('REGISTRE_DEPOT_PAPIER')) {
            $dataSource = (new Atexo_Consultation_Register())->retrieveOffrePapier($this->getViewState('reference'), $tri, Atexo_CurrentUser::getCurrentOrganism(), true, false, $limit, $offset);
            $this->textNombreRegistre->Text = Prado::localize('NOMBRE_DEPOT_PAPIER');
        } else {
            throw new Atexo_Exception('Unkown type registre');
        }

        if (is_array($dataSource)) {
            $this->dataListRegistre->DataSource = $dataSource;
            $this->dataListRegistre->DataBind();
        } else {
            throw new Atexo_Exception('Expected data source to be an array');
        }
    }

    public function countData()
    {
        $tri = [];
        $tri['trierPar'] = $this->getViewState('tri');
        $typeRegistre = $this->getViewState('typeRegistre');
        $this->_typeRegistre = $typeRegistre;

        if ($typeRegistre == Atexo_Config::getParameter('REGISTRE_RETRAIT_ELECTRONIQUE')) {
            $countData = (new Atexo_Consultation_Register())->retrieveRetraitsElectroniques($this->getViewState('reference'), $tri, Atexo_CurrentUser::getCurrentOrganism(), true);
        } elseif ($typeRegistre == Atexo_Config::getParameter('REGISTRE_RETRAIT_PAPIER')) {
            $countData = (new Atexo_Consultation_Register())->retrieveRetraitsPapier($this->getViewState('reference'), $tri, Atexo_CurrentUser::getCurrentOrganism(), true, true);
        } elseif ($typeRegistre == Atexo_Config::getParameter('REGISTRE_QUESTION_ELECTRONIQUE')) {
            $countData = (new Atexo_Consultation_Register())->retrieveQuestions($this->getViewState('reference'), $tri, Atexo_Config::getParameter('TYPE_DEPOT_QUESTION_ELECTRONIQUE'), Atexo_CurrentUser::getCurrentOrganism(), true, true);
        } elseif ($typeRegistre == Atexo_Config::getParameter('REGISTRE_QUESTION_PAPIER')) {
            $countData = (new Atexo_Consultation_Register())->retrieveQuestions($this->getViewState('reference'), $tri, Atexo_Config::getParameter('TYPE_DEPOT_QUESTION_PAPIER'), Atexo_CurrentUser::getCurrentOrganism(), true, true);
        } elseif ($typeRegistre == Atexo_Config::getParameter('REGISTRE_DEPOT_ELECTRONIQUE')) {
            $countData = (new Atexo_Consultation_Register())->retrieveOffreElectronique($this->getViewState('reference'), $tri, Atexo_CurrentUser::getCurrentOrganism(), true);
        } elseif ($typeRegistre == Atexo_Config::getParameter('REGISTRE_DEPOT_PAPIER')) {
            $countData = (new Atexo_Consultation_Register())->retrieveOffrePapier($this->getViewState('reference'), $tri, Atexo_CurrentUser::getCurrentOrganism(), true, true);
        } else {
            $countData = 0;
        }

        return $countData;
    }

    public function isRegistrePapier()
    {
        if ($this->_visible) {
            if ($this->_typeRegistre == Atexo_Config::getParameter('REGISTRE_RETRAIT_PAPIER') ||
               $this->_typeRegistre == Atexo_Config::getParameter('REGISTRE_QUESTION_PAPIER') ||
               $this->_typeRegistre == Atexo_Config::getParameter('REGISTRE_DEPOT_PAPIER')) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function isRegistreElectronique()
    {
        if ($this->_visible) {
            if ($this->_typeRegistre == Atexo_Config::getParameter('REGISTRE_RETRAIT_ELECTRONIQUE') ||
               $this->_typeRegistre == Atexo_Config::getParameter('REGISTRE_QUESTION_ELECTRONIQUE') ||
               $this->_typeRegistre == Atexo_Config::getParameter('REGISTRE_DEPOT_ELECTRONIQUE')) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function getNomColonne()
    {
        if ($this->isRegistreRetrait()) {
            return Prado::localize('FICHIER_TELECHARGE');
        } elseif ($this->isRegistreQuestion()) {
            return Prado::localize('QUESTION');
        }
    }

    public function isRegistreRetrait()
    {
        if ($this->_typeRegistre == Atexo_Config::getParameter('REGISTRE_RETRAIT_ELECTRONIQUE') ||
          $this->_typeRegistre == Atexo_Config::getParameter('REGISTRE_RETRAIT_PAPIER')) {
            return true;
        } else {
            return false;
        }
    }

    public function isRegistreQuestion()
    {
        if ($this->_typeRegistre == Atexo_Config::getParameter('REGISTRE_QUESTION_ELECTRONIQUE') ||
          $this->_typeRegistre == Atexo_Config::getParameter('REGISTRE_QUESTION_PAPIER')) {
            return true;
        } else {
            return false;
        }
    }

    public function isRegistreDepot()
    {
        if ($this->_typeRegistre == Atexo_Config::getParameter('REGISTRE_DEPOT_ELECTRONIQUE') ||
          $this->_typeRegistre == Atexo_Config::getParameter('REGISTRE_DEPOT_PAPIER')) {
            return true;
        } else {
            return false;
        }
    }

    public function getElectronicOrPaper()
    {
        if ($this->isRegistrePapier()) {
            return Prado::localize('PA_POUR_PAPIER');
        } else {
            return Prado::localize('EL_POUR_ELECTRONIQUE');
        }
    }

    public function addBrBeforeNotEmptyText($texte)
    {
        if (trim($texte)) {
            return '<br>'.$texte;
        }
    }

    public function getAjouterTypeRegistre()
    {
        if ($this->isRegistreRetrait()) {
            return Prado::localize('AJOUTER_RETRAIT_PAPIER');
        } elseif ($this->isRegistreQuestion()) {
            return Prado::localize('AJOUTER_QUESTION_PAPIER');
        } elseif ($this->isRegistreDepot()) {
            return Prado::localize('AJOUTER_DEPOT_PAPIER');
        }
    }

    public function critereTri($sender, $param)
    {
        $sensTriDefaut = null;
        $tri = $param->CommandName;
        if (1 == Atexo_Config::getParameter('ORDRE_AFFICHAGE_DEFAUT_REGISTRES')) {
            $sensTriDefaut = $this->getViewState('sensTriDefaut', 'ASC');
        } elseif (0 == Atexo_Config::getParameter('ORDRE_AFFICHAGE_DEFAUT_REGISTRES')) {
            $sensTriDefaut = $this->getViewState('sensTriDefaut', 'DESC');
        }
        $sensTri = $this->getViewState('sensTri', 'ASC');

        if ($tri) {
            if ('ASC' == $sensTri) {
                $sensTri = 'DESC';
            } else {
                $sensTri = 'ASC';
            }
        } else {
            if ('ASC' == $sensTriDefaut) {
                $sensTriDefaut = 'DESC';
            } else {
                $sensTriDefaut = 'ASC';
            }
            $sensTri = '';
        }

        $this->setViewState('sensTri', $sensTri);
        $this->setViewState('sensTriDefaut', $sensTriDefaut);
        $this->setViewState('tri', $tri);
    }

    public function Trier($sender, $param)
    {
        $typeRegistre = $this->getTypeRegistreFromTemplateId();

        if ($typeRegistre) {
            $this->_visible = true;
            $this->_reference = $this->getViewState('reference'.$this->ID);
            $this->fillDataList($typeRegistre);
            $this->panelListRegistre->render($param->NewWriter);
        }
    }

    public function onCallBackRefreshRepeater($sender, $param)
    {
        $typeRegistre = $this->getTypeRegistreFromTemplateId();

        if ($typeRegistre) {
            $this->_visible = true;
            $this->_reference = $this->getViewState('reference'.$this->ID);
            $this->panelListRegistre->render($param->NewWriter);
            $this->fillDataList($typeRegistre);
        }
    }

    public function getTypeRegistreFromTemplateId()
    {
        $typeRegistre = match ($this->ID) {
            'registreRetraisElectronique' => Atexo_Config::getParameter('REGISTRE_RETRAIT_ELECTRONIQUE'),
            'registreRetraisPapier' => Atexo_Config::getParameter('REGISTRE_RETRAIT_PAPIER'),
            'registreQuestionsElectronique' => Atexo_Config::getParameter('REGISTRE_QUESTION_ELECTRONIQUE'),
            'registreQuestionPapier' => Atexo_Config::getParameter('REGISTRE_QUESTION_PAPIER'),
            'registreDepotsElectronique' => Atexo_Config::getParameter('REGISTRE_DEPOT_ELECTRONIQUE'),
            'registreDepotsPapier' => Atexo_Config::getParameter('REGISTRE_DEPOT_PAPIER'),
            default => '',
        };

        return $typeRegistre;
    }

    public function deleteRegistrePapier($sender, $param)
    {
        $typeRegistre = $this->getTypeRegistreFromTemplateId();
        $idRegistre = $param->CommandName;
        $organisme = Atexo_CurrentUser::getCurrentOrganism();

        (new Atexo_Consultation_Register())->deleteRegistrePapierById($idRegistre, $typeRegistre, $organisme);
    }

    public function registreDepotClosed($idRegistre)
    {
        $typeRegistre = $this->getTypeRegistreFromTemplateId();
        if ($typeRegistre == Atexo_Config::getParameter('REGISTRE_DEPOT_PAPIER')) {
            $enveloppes = (new Atexo_Consultation_Responses())->retrieveEnveloppesPapiersByIdOffre($idRegistre, Atexo_CurrentUser::getCurrentOrganism());
            if (is_array($enveloppes)) {
                foreach ($enveloppes as $enveloppe) {
                    if ($enveloppe->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_EN_LIGNE') ||
                      $enveloppe->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_HORS_LIGNE') ||
                      $enveloppe->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_A_DISTANCE') ||
                      $enveloppe->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE')) {
                        return false;
                    }
                }
            }
        }

        return true;
    }

    public function annulerRegistrePapier($sender, $param)
    {
        $typeRegistre = $this->getTypeRegistreFromTemplateId();
        $idRegistre = $param->CommandName;
        $organisme = Atexo_CurrentUser::getCurrentOrganism();

        (new Atexo_Consultation_Register())->annulerRegistrePapierById($idRegistre, $typeRegistre, $organisme);
    }

    public function restaurerRegistrePapier($sender, $param)
    {
        $typeRegistre = $this->getTypeRegistreFromTemplateId();
        $idRegistre = $param->CommandName;
        $organisme = Atexo_CurrentUser::getCurrentOrganism();

        (new Atexo_Consultation_Register())->restaurerRegistrePapierById($idRegistre, $typeRegistre, $organisme);
    }

    public function getColSpan()
    {
        if ($this->isRegistreQuestion() && $this->isRegistrePapier()) {
            return '7';
        } elseif ($this->isRegistreQuestion() && $this->isRegistreElectronique()) {
            return '7';
        } elseif ($this->isRegistreDepot() && $this->isRegistreElectronique()) {
            return '5';
        } elseif ($this->isRegistreDepot() && $this->isRegistrePapier()) {
            return '6';
        } elseif ($this->isRegistreRetrait() && $this->isRegistrePapier()) {
            return '7';
        } elseif ($this->isRegistreRetrait() && $this->isRegistreElectronique()) {
            return '6';
        }
    }

    /***
      * affiche l'icone de l'offre variante
      */
    public function isIconeOffreVarianteDisplayed($objet)
    {
        if ($objet instanceof Atexo_Consultation_RegistreVo) {
            if ('1' == $objet->getOffreVariante()) {
                return true;
            }
        }

        return false;
    }

    /***
     * affiche l'icone de l'offre de base
     */
    public function isIconeOffreBaseDisplayed($objet)
    {
        if ($objet instanceof Atexo_Consultation_RegistreVo) {
            if ('0' == $objet->getOffreVariante()) {
                return true;
            }
        }

        return false;
    }

    public function isQuestionAnswered($idQuestion)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $selectCriteria = new Criteria();
        $selectCriteria->add(BaseCommonRelationEchangePeer::ID_EXTERNE, $idQuestion);
        $selectCriteria->add(BaseCommonRelationEchangePeer::TYPE_RELATION, Atexo_Config::getParameter('TYPE_RELATION_REPONSE_QUESTION'));
        $selectCriteria->add(BaseCommonRelationEchangePeer::ORGANISME, Atexo_CurrentUser::getCurrentOrganism());
        $question = BaseCommonRelationEchangePeer::DoSelectOne($selectCriteria, $connexion);
        if ($question) {
            return $question->getIdEchange();
        }

        return $question;
    }

    public function changePagerLenght($sender, $param)
    {
        switch ($sender->ID) {
            case 'nombreResultatAfficherBottom':
                 $pageSize = $this->nombreResultatAfficherBottom->getSelectedValue();
                                                  $this->nombreResultatAfficherTop->setSelectedValue($pageSize);
                break;
            case 'nombreResultatAfficherTop':
                 $pageSize = $this->nombreResultatAfficherTop->getSelectedValue();
                                               $this->nombreResultatAfficherBottom->setSelectedValue($pageSize);
                break;
        }

        $this->dataListRegistre->PageSize = $pageSize;
        $nombreResultat = $this->getViewState('nombreResultat');
        $this->nombrePageTop->Text = ceil($nombreResultat / $this->dataListRegistre->PageSize);
        $this->nombrePageBottom->Text = ceil($nombreResultat / $this->dataListRegistre->PageSize);
        $this->dataListRegistre->setCurrentPageIndex(0);
        $this->numPageTop->Text = 1;
        $this->numPageBottom->Text = 1;
        $this->_visible = true;
        $this->_reference = $this->getViewState('reference');
        $this->fillDataList($this->getViewState('typeRegistre'));
    }

    public function goToPage($sender, $param)
    {
        $numPage = null;
        switch ($sender->ID) {
            case 'DefaultButtonTop':
                 $numPage = $this->numPageTop->Text;
                break;
            case 'DefaultButtonBottom':
                 $numPage = $this->numPageBottom->Text;
                break;
        }
        if ((new Util())->isEntier($numPage)) {
            if ($numPage >= $this->nombrePageTop->Text) {
                $numPage = $this->nombrePageTop->Text;
            } elseif ($numPage <= 0) {
                $numPage = 1;
            }
            $this->dataListRegistre->CurrentPageIndex = $numPage - 1;
            $this->numPageBottom->Text = $numPage;
            $this->numPageTop->Text = $numPage;
            $this->_visible = true;
            $this->_reference = $this->getViewState('reference');
            $this->fillDataList($this->getViewState('typeRegistre'));
        } else {
            $this->numPageTop->Text = $this->dataListRegistre->CurrentPageIndex + 1;
            $this->numPageBottom->Text = $this->dataListRegistre->CurrentPageIndex + 1;
        }
    }

    public function pageChanged($sender, $param)
    {
        $this->dataListRegistre->CurrentPageIndex = $param->NewPageIndex;
        $this->numPageBottom->Text = $param->NewPageIndex + 1;
        $this->numPageTop->Text = $param->NewPageIndex + 1;
        $this->_visible = true;
        $this->_reference = $this->getViewState('reference');
        $this->fillDataList($this->getViewState('typeRegistre'), $param->NewPageIndex);
    }

    public function setIdItem($sender, $param)
    {
        $this->fillDataList($this->getViewState('typeRegistre'));

        $criteria = new Atexo_Consultation_CriteriaVo();
        $criteria->setcalledFromPortail(false);
        $criteria->setIdService(Atexo_CurrentUser::getCurrentServiceId());
        $criteria->setAcronymeOrganisme(Atexo_CurrentUser::getCurrentOrganism());
        $criteria->setIdReference($this->_reference);
        $criteria->setConnectedAgentId(Atexo_CurrentUser::getId());

        $consultation = (new Atexo_Consultation())->search($criteria);
        $urlMessec = 'index.php?page=Agent.EnvoiCourrierElectroniqueReponseQuestion&id='.Atexo_Util::atexoHtmlEntities($_GET['id']).'&idQuestion='.$param->CallbackParameter;
        if (is_array($consultation) && 1 == count($consultation) && $consultation[0] instanceof CommonConsultation) {
            $consultation = array_shift($consultation);
            $messecVersion = $consultation->getVersionMessagerie();
            if (2 === $messecVersion) {
                $encrypte = Atexo_Util::getSfService(Encryption::class);
                $idCrypte = $encrypte->cryptId($consultation->getId());

                $questionIdCrypte = $encrypte->cryptId($param->CallbackParameter);

                $urlMessec = Atexo_MultiDomaine::replaceDomain(Atexo_Config::getParameter('PF_URL_MESSAGERIE_REPONSE_QUESTION_CONSULTATION'))
                   .'/'
                   .$idCrypte
                   .'/'
                   .$questionIdCrypte;

                $urlDetailConsultation = '/index.php?page=Agent.DetailConsultation&id='.$this->_reference;
                $this->Page->scriptJsReponse->Text = "<script>J(document).ready(function () {
                    J('#ctl0_CONTENU_PAGE_sendElectroniqResponse').css( 'cursor', 'pointer' );
                    J('#ctl0_CONTENU_PAGE_sendElectroniqResponse').click(function () {
                        window.open('".$urlMessec."');
                        window.location.href = '".$urlDetailConsultation."';
                        return false;
                    })
                });</script>";
            } else {
                $this->Page->sendElectroniqResponse->NavigateUrl = "index.php?page=Agent.EnvoiCourrierElectroniqueReponseQuestion&id=".Atexo_Util::atexoHtmlEntities($_GET['id'])."&idQuestion=".$param->CallbackParameter;
            }
        }

        $this->Page->scriptJs->Text = "<script>openModal('modal-msg-reponse-question','popup-small2',document.getElementById('ctl0_CONTENU_PAGE_popinTitle'));</script>";
        $this->Page->scriptJs->render($param->NewWriter);

        if ($this->getViewState('typeRegistre') == Atexo_Config::getParameter('REGISTRE_QUESTION_ELECTRONIQUE')) {
            $this->textNombreRegistre->Text = Atexo_Util::toUtf8(Prado::localize('NOMBRE_QUESTION_ELECTRONIQUE'));
        } elseif ($this->getViewState('typeRegistre') == Atexo_Config::getParameter('REGISTRE_QUESTION_PAPIER')) {
            $this->textNombreRegistre->Text = Atexo_Util::toUtf8(Prado::localize('NOMBRE_QUESTION_PAPIER'));
        }
    }

    public function isReadOnly()
    {
        $res = true;
        $consultation = $this->getConsultation();
        if($consultation instanceof CommonConsultation) {
            $res = $consultation->getCurrentUserReadOnly();
        }
        return $res;
    }
}
