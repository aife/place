<?php

namespace Application\Controls;

use Application\Service\Atexo\DomainesActivites\Atexo_DomainesActivites_Domaines;

class DomainesActivites extends MpeTTemplateControl
{
    private bool $togglePanelVisible = true;

    public function hideToggleMenu()
    {
        $this->panelToggle->setVisible(false);
        $this->setTogglePanelVisible(false);
    }

    public function getTogglePanelVisible()
    {
        return $this->togglePanelVisible;
    }

    public function setTogglePanelVisible($v)
    {
        $this->togglePanelVisible = $v;
    }

    public function displayDomaines()
    {
        $listeDomaines = (new Atexo_DomainesActivites_Domaines())->retreiveDomaines();
        if (!is_array($listeDomaines)) {
            $listeDomaines = [];
        }
        $this->RepeaterNomsDomaines->DataSource = $listeDomaines;
        $this->RepeaterNomsDomaines->DataBind();
    }

    public function getCheckedDomaines()
    {
        $listeDomaines = [];
        foreach ($this->RepeaterNomsDomaines->getItems() as $item) {
            if ($item->el_1->checked) {
                $IdDomaineParent = $item->IdDomaineParent->Value;
                $listeDomaines[$IdDomaineParent] = $IdDomaineParent;
            }
            foreach ($item->RepeaterNomsDomainesFils->getItems() as $item2) {
                if ($item2->el_1_ssElement_1->checked) {
                    $IdDomaineFils = $item2->IdDomaineFils->Value;
                    $listeDomaines[$IdDomaineFils] = $IdDomaineFils;
                }
            }
        }

        return $listeDomaines;
    }

    public function checkDomaines($listeDomaines)
    {
        //print_r($listeDomaines);
        foreach ($this->RepeaterNomsDomaines->getItems() as $item) {
            $IdDomaineParent = $item->IdDomaineParent->Value;
            if (isset($listeDomaines[$IdDomaineParent])) {
                $item->el_1->checked = true;
            }
            foreach ($item->RepeaterNomsDomainesFils->getItems() as $item2) {
                $IdDomaineFils = $item2->IdDomaineFils->Value;
                if (isset($listeDomaines[$IdDomaineFils])) {
                    $item2->el_1_ssElement_1->checked = true;
                }
            }
        }
    }
}
