<!--Debut Bloc Documents du module REDAC-->
	
				<div class="table-bloc">
				<com:TActivePanel ID="panelListeDocsRedac" >
					<com:TRepeater ID="repeaterDocsRedac" >
						<prop:HeaderTemplate>
							<table class="table-results" summary="Documents du module REDAC">
								<caption><com:TTranslate>TEXT_DOCUMENTS_DU_MODULE_REDAC</com:TTranslate></caption>
								<thead>
									<tr>
										<th class="top" colspan="7"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></th>
									</tr>
									<tr>
										<th class="col-60 center" id="type">&nbsp;</th>
										<th class="col-60 center" id="type"><com:TTranslate>TEXT_TYPE</com:TTranslate></th>
										<th class="col-500" id="nomDocument"><com:TTranslate>TEXT_TITRE_DU_DOCUMENT_FICHIER_REDACTEUR</com:TTranslate></th>
										<th class="actions-inline" id="actionDCE"></th>
									</tr>
								</thead>
						</prop:HeaderTemplate>
						<prop:ItemTemplate>
							<tr class="<%#(($this->ItemIndex%2==0)? '':'on')%>">
								<td class="col-60 center" headers="type">
									<com:TPanel id="blocCheck" visible="<%=!($this->page->composantAjouter->docsREDAC->isRadio())%>" >
										<com:TCheckBox id="idPieceRedac1"  Attributes.title="<%=Prado::localize('PIECE_DE_REDAC')%>" cssClass="check" />
									</com:TPanel>
									<com:TPanel id="blocRadio" visible="<%=$this->page->composantAjouter->docsREDAC->isRadio()%>">
										<com:TRadioButton 
											id="idPieceRedac" 
											UniqueGroupName="pieceRedac" 
											Attributes.title="<%=Prado::localize('PIECE_DE_REDAC')%>"
											Attributes.onchange="document.getElementById('ctl0_CONTENU_PAGE_composantAjouter_pieceRedac').checked  = true;"
											CssClass="radio" />
									</com:TPanel>
								</td>
								<td class="col-60 center" headers="type"><%#$this->Data->getType()%></td>
								<td class="col-500" headers="nomDocument"><strong><%#$this->Data->getTitre()%></strong><div><%#$this->Data->getNom()%></div><div><%#$this->Data->getAuteur()%></div></td>
								<td class="actions-inline" headers="actionDCE">
									<a title="<%=Prado::localize('DEFINE_TELECHARGER')%>" href="index.php?page=Agent.DownloadDocRedac&IdBlob=<%#$this->Data->getBlob()%>&nomDoc=<%#$this->Data->getNom()%>"><img src="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-dl-dossier.gif" alt="<%=Prado::localize('DEFINE_TELECHARGER')%>" title="<%=Prado::localize('DEFINE_TELECHARGER')%>" /></a>
								</td>
							</tr>
						</prop:ItemTemplate>
						<prop:FooterTemplate>
							</table>
						</prop:FooterTemplate>
					</com:TRepeater>
				</com:TActivePanel>
				<com:TActivePanel ID="panelNoElementFound"  >
					<h2><center><com:TTranslate>DEFINE_AUCUN_RESULTAT</com:TTranslate>&nbsp;</center></h2>
				</com:TActivePanel>	
				</div>

			
		
		<!--Fin Bloc Documents du module REDAC-->