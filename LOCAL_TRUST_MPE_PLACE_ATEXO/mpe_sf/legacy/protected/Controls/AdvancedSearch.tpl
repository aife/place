<script type="text/JavaScript">
    function returnLieuxExecution(idSelectedGeo, numSelectedGeoN2) {
        document.getElementById('ctl0_CONTENU_PAGE_AdvancedSearch_idsSelectedGeoN2').value = idSelectedGeo;
        document.getElementById('ctl0_CONTENU_PAGE_AdvancedSearch_numSelectedGeoN2').value = numSelectedGeoN2;
        document.getElementById('ctl0_CONTENU_PAGE_AdvancedSearch_selectedGeoN2').click();
    }
</script>
<!--Debut colonne droite-->
<com:TPanel DefaultButton="lancerRecherche" cssClass="main-part" id="panelContent">

	<div class="advancedSearch">
		<div class="clearfix hide-agent">
			<nav role="navigation">
			<ul class="breadcrumb">
				<li>
					<a href="/entreprise">
						<com:TTranslate>TEXT_ACCUEIL</com:TTranslate>
                    </a>
				</li>
				<li>
					<a href="?page=Entreprise.EntrepriseAdvancedSearch&AllCons">
						<com:TTranslate>TEXT_CONSULTATIONS</com:TTranslate>
					</a>
				</li>
				<li>
					<com:TTranslate>TEXT_RECHERCHE_AVANCEE</com:TTranslate>
				</li>
			</ul>
			</nav>
		</div>
		<div class="breadcrumbs hide">
			<com:THyperLink ID="linkDCE" NavigateUrl="#" Text="<%=Prado::localize('TEXT_BASE_DCE')%>"/>
			<com:THyperLink ID="linkConsultation" NavigateUrl="#"/>
			<com:THyperLink ID="linkAnnonces" NavigateUrl="#" Text="<%=Prado::localize('DEFINE_ANNONCES')%>"/>&gt;
			<com:TTranslate>TEXT_RECHERCHE_AVANCEE</com:TTranslate>
		</div>
		<!--BEGIN MESSAGE ALERT-->
		<div class="alert alert-danger" style="display:none">
			<div class="h4">
				<i class="fa fa-exclamation-triangle"></i>
				<com:TTranslate>DEFINE_ERREUR_SAISIE</com:TTranslate>
			</div>
			<div class="m-l-3">
				<com:TTranslate>DEFINE_VERIFIER_CHAMPS_OBLIGATOIRES</com:TTranslate>

				<!-- Debut du sommateur de messages d'erreurs-->
				<div id="validationSummary" class="m-t-1">
					<ul>
						<li id="erreurMinistereLi" style="display:none">
							<com:TTranslate>TEXT_MINISTERE</com:TTranslate>
						</li>
						<li id="erreurReferenceLi" style="display:none">
							<com:TTranslate>TEXT_REFERENCE</com:TTranslate>
						</li>
						<li id="erreurCodeLi" style="display:none">
							<com:TTranslate>TEXT_CODE</com:TTranslate>
						</li>
					</ul>
				</div>

				<!-- Fin du sommateur de messages d'erreurs -->
			</div>
			<!-- Fin de l'entete du sommateur de messages d'erreurs -->
			<!-- Debut du footer du sommateur de messages d'erreurs -->
		</div>
		<!--END MESSAGE ALERT-->
		<!--Debut Bloc Message Recherche -->
		<com:TActivePanel id="panelConfSave" visible="true">
			<com:PanelMessageConfirmation visible="false" id="panelConfirmation" cssClass="message bloc-700"/>
		</com:TActivePanel>
		<!--fin Bloc Message Recherche -->
		<!--BEGIN BLOC ADVANCED SEARCH-->
		<com:AtexoValidationSummary ValidationGroup="validationRestreinte"/>
		<div class="content-advancedSearch form-field form-horizontal">
			<h1 class="hide-agent h2 m-t-0 m-b-2">
				<com:TTranslate>TEXT_RECHERCHE_AVANCEE</com:TTranslate>
			</h1>
			<div class="top "><span class="left">&nbsp;</span><span class="right">&nbsp;</span>
				<div class="title">
					<com:TTranslate>DEFINE_RECHERCHE_MULTICRITAIRE</com:TTranslate>
				</div>
			</div>
			<!--SELECT FOR MOBILE VIEW-->
			<select id="selectRechercheAvance--m" class="m-b-2 hide-agent">
				<option value='0'><com:TTranslate>DEFINE_RECHERCHE_MULTICRITAIRE</com:TTranslate></option>
				<option value='1'><com:TTranslate>TEXT_SEARCH_PROCEDURE_RESTREINTE</com:TTranslate></option>
			</select>
			<ul class="nav nav-tabs hide-agent" id="targetRechercheAvance--m" role="tablist">
				<li class="active limulticriteres" role="presentation">
					<a href="#multicriteres" aria-controls="multicriteres" role="tab" data-toggle="tab">
						<h2 class="h3 panel-title">
							<com:TTranslate>DEFINE_RECHERCHE_MULTICRITAIRE</com:TTranslate>
						</h2>
					</a>
				</li>
				<li role="presentation" class="lirestreinte" >
					<a href="#restreinte" aria-controls="restreinte" role="tab" data-toggle="tab">
						<com:TActivePanel ID="panelSearch" cssClass="form-field">
							<h2 class="h3 panel-title">
								<com:TTranslate>TEXT_SEARCH_PROCEDURE_RESTREINTE</com:TTranslate>
							</h2>
						</com:TActivePanel>
					</a>
				</li>
			</ul>

			<div class="tab-content form-content">
				<div class="panel panel-default tab-pane fade in active" id="multicriteres">
					<div class="panel-body ">
						<!--Debut Bloc Vision RMA-->
						<!--Debut Ligne Vision-->
						<com:TPanel visible="<%=$this->isProfilRmaVisible%>">
							<div class="line">
								<div class="col-lg-12 p-l-0 neg-margin-left-15">
								    <label for="vision">
										<com:TTranslate>DEFINE_RECHERCHE_CONSULTATION</com:TTranslate>
									</label>
								</div>
								<div class="content-bloc bloc-600">
									<div class="line">
									<span title="<%=Prado::localize('DEFINE_VISION_ACHETEUR')%>">
										<com:TRadioButton GroupName="rechercheVision" ID="visionAcheteur" Checked="true"
														  Attributes.onclick="hideDiv('<%=$this->layerVisionRMA->getClientId()%>');hideDiv('<%=$this->statutRma->getClientId()%>');showDiv('<%=$this->statut->getClientId()%>');"
														  Attributes.title="<%=Prado::localize('DEFINE_VISION_ACHETEUR')%>"/>
									</span>
										<com:TLabel ForControl="visionAcheteur">
											<com:TTranslate>DEFINE_VISION_ACHETEUR</com:TTranslate>
										</com:TLabel>
										<span title="<%=Prado::localize('DEFINE_VISION_RMA')%>" class="indent-40">
										<com:TRadioButton GroupName="rechercheVision" ID="visionRma"
														  Attributes.onclick="showDiv('<%=$this->layerVisionRMA->getClientId()%>');showDiv('<%=$this->statutRma->getClientId()%>');hideDiv('<%=$this->statut->getClientId()%>');"
														  Attributes.title="<%=Prado::localize('DEFINE_VISION_RMA')%>"/>
									</span>
										<com:TLabel ForControl="visionRma">
											<com:TTranslate>DEFINE_VISION_RMA</com:TTranslate>
										</com:TLabel>
										<div class="inline">
											<img title="Info-bulle" alt="Info-bulle" class="picto-info-intitule"
												 onmouseout="cacheBulle('infosRMA')"
												 onmouseover="afficheBulle('infosRMA', this)"
												 src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif"/>
											<div onmouseout="mouseOutBulle();" onmouseover="mouseOverBulle();"
												 class="info-bulle" id="infosRMA">
												<div>
													<com:TTranslate>CONSULT_MESSAGE_INFOBULLE_HABILITATION_RMA
													</com:TTranslate>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!--Fin Ligne Vision-->
							<com:TPanel id="layerVisionRMA" Style="display:none">
								<!--Debut Ligne Organisme-->
								<div>
									<div class="intitule-150"><label for="organisme">
											<com:TTranslate>ORGANISME</com:TTranslate>
										</label>
									</div>
									<com:TActiveDropDownList ID="organismesRma" DataValueField="Acronyme"
															 DataTextField="DenominationOrgTraduit"
															 onCallBack="displayEntityPurchaseRma" AutoPostBack="true"
															 cssClass="long"
															 Attributes.title="<%=Prado::localize('ORGANISME')%>"/>
								</div>
								<!--Fin Ligne Organisme-->
								<!--Debut Ligne Entite d'achat-->
								<div>
									<div class="intitule-150"><label for="entiteAchat">
											<com:TTranslate>DEFINE_TEXT_ENTITE_ACHAT</com:TTranslate>
										</label>
									</div>
									<com:TActiveDropDownList ID="entiteAchat" cssClass="long"
															 Attributes.title="<%=Prado::localize('SERVICE_RECHERCHE_AVANCEE')%>"/>
								</div>
								<!--Fin Ligne Entite d'achat-->
							</com:TPanel>
						</com:TPanel>
						<!--Fin Bloc Vision RMA-->

						<!-- BEGIN ROW SEARCH BY KEYWORDS-->
						<section class="section">
							<div class="section-body">
								<div class="form-group form-group-sm hide-agent">
									<div class="h3">
										<com:TTranslate>DEFINE_RECHERCHE_MOT_CLE</com:TTranslate>
										<a href="#keywordSearch" onmouseover="afficheBulle('infosRechercheMotsCles', this)" onmouseout="cacheBulle('infosRechercheMotsCles')"
										   onfocus="afficheBulle('infosRechercheMotsCles', this)" onblur="cacheBulle('infosRechercheMotsCles')" class="picto-info-intitule">
											<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" alt="Info-bulle"/>
										</a>
										<div id="infosRechercheMotsCles" class="info-bulle" onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();">
											<div>
												<com:TTranslate>MSG_EXPLICATION_RECHERCHE_SIMPLE</com:TTranslate>
											</div>
										</div>
									</div>
									<div class="line formulaire-inline">
										<label class="col-md-2 hide-agent" for="ctl0_CONTENU_PAGE_AdvancedSearch_keywordSearch">
											<com:TTranslate>DEFINE_RECHERCHE_MOT_CLE</com:TTranslate>
										</label>
										<div class="intitule-bloc bloc-155 hide">
											<label>
												<com:TTranslate>DEFINE_RECHERCHE_REF_TITRE_OBJET</com:TTranslate>
											</label>
										</div>
									</div>
									<div class="col-md-9 formulaire-inline">
										<com:TTextBox ID="keywordSearch" Attributes.placeholder="<%=Prado::localize('DEFINE_RECHERCHE_REF_TITRE_OBJET')%>" CssClass="long"/>
										<div class="hide">
											<com:TPanel ID="panelMotsCles" cssClass="content-bloc bloc-600">
												<com:TPanel id="panelRechercheFloue" CssClass="line">
													<com:TRadioButton Attributes.title="<%=Prado::localize('TEXT_RECHERCHE_FLOUE')%>" GroupName="rechercheFloue" ID="floue" Checked="true"
																	  CssClass="radio"/>
													<label>
														<com:TTranslate>TEXT_RECHERCHE_FLOUE</com:TTranslate>
													</label>
													&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													<com:TRadioButton Attributes.title="<%=Prado::localize('TEXT_RECHERCHE_EXACTE')%>" GroupName="rechercheFloue" ID="exact" Checked="false"
																	  CssClass="radio"/>
													<label>
														<com:TTranslate>TEXT_RECHERCHE_EXACTE</com:TTranslate>
													</label>
												</com:TPanel>
											</com:TPanel>
										</div>
									</div>
									<div class="col-md-1 p-l-0 hide-agent">
                                        <span data-toggle="tooltip" data-html="true" data-original-title="<%= $this->translateHtmlspecialchars('MSG_EXPLICATION_RECHERCHE_SIMPLE')%>">
                                            <i class="fa p-t-1 fa-question-circle text-info"></i>
                                        </span>
									</div>

								</div>


								<!-- BEGIN BLOCK ENTITE PUBLIQUE TOGGLE-->
								<com:TPanel ID="panelModeRechercheEntite">
									<div class="form-group form-group-sm">
										<label for="ctl0_CONTENU_PAGE_AdvancedSearch_orgNameAM" class="col-md-2">
											<com:TLabel ID="messageModeRecherechEntite" Text="<%=Prado::Localize('MODE_RECHERCHE_ENTITE')%>"/>
										</label>

										<div class="col-md-5 tabs-right tabs-affix">

											<!-- Tab panes -->
											<div class="tab-content col-md-12">
												<div role="tabpanel" class="tab-pane active" id="tabSearchInEntreprise">
													<com:TPanel ID="panelSearchAnnoncesMarches">
														<div class="form-group">
															<label class="hide">
																<com:TLabel ID="labelEntitePubliqueAutoComplition"/>
															</label>
															<div class="clearfix">
																<com:TJuiAutoComplete
																		ID="orgNameAM"
																		OnSuggest="suggestNames"
																		Suggestions.DataKeyField="id"
																		ResultPanel.CssClass="acomplete"
																		cssClass="form-control"
																		MinChars="3"
																		OnSuggestionSelected="suggestionSelected">
																	<prop:Suggestions.ItemTemplate>
																		<li title="<%#$this->Data['denomination']%>"><%# $this->Data['denomination']%></li>
																	</prop:Suggestions.ItemTemplate>
																</com:TJuiAutoComplete>
															</div>
														</div>
													</com:TPanel>
												</div>
												<div role="tabpanel" class="tab-pane" id="tabSearchAnnoncesMarches">
													<div class="form-group">
														<!--Debut Ligne Ministere-->
														<com:TPanel ID="panelOrganismes">
															<label class="hide">
																<com:TLabel ID="labelEntitePublique"/>
															</label>
															<div class="clearfix">
																<com:TActiveDropDownList ID="organismesNames"
																						 cssClass="form-control"
																						 onCallBack="displayEntityPurchase"
																						 attributes.title="<%=Prado::Localize('MODE_RECHERCHE_ENTITE')%>"
																						 AutoPostBack="true"/>
															</div>
														</com:TPanel>
														<!--Fin Ligne Ministere-->
													</div>
													<com:TPanel ID="panelSearchInEntreprise">
													<div class="form-group m-b-0">
														<!--Debut classification-->
														<com:TPanel ID="panelSearchByClassification" Visible="false"
																	cssClass="line">
															<label class="hide"
																   for="ctl0_CONTENU_PAGE_classification">
																<com:TTranslate>ETAT_COLLECTIVITE_LOCAL_ETABLISSEMENT_PUBLIQUE
																</com:TTranslate>
															</label>
															<div class="clearfix">
																<com:TActiveDropDownList ID="classification" CssClass="long"
																						 attributes.title="<%=Prado::Localize('MODE_RECHERCHE_ENTITE')%>"
																						 onCallBack="choixClassification"
																						 AutoPostBack="true" />
															</div>
														</com:TPanel>
														<!--fin classification-->

														<com:TActivePanel ID="panelPurchaseNames">
															<!--Debut Ligne Ministere-->
															<div id="divEntityPurchase" style="display:none" class="clearfix">
																<label class="hide" for="<%=$this->entityPurchaseNames->getClientID()%>">
																	<com:TTranslate>SERVICE_RECHERCHE_AVANCEE</com:TTranslate>
																</label>
																<div class="clearfix">
																	<com:TActiveDropDownList ID="entityPurchaseNames" cssClass="long float-left"/>
																	<!-- Debut bloc inclure la descendance -->
																	<div class="clearfix">
																		<div class="radio radio-inline p-l-0">
																			<label for="<%=$this->entiteSeule->getClientID()%>"
																				   data-toggle="tooltip"
																				   title="<%=Prado::Localize('UNIQUEMENT_CETTE_ENTITE')%>">
																				<com:TRadioButton GroupName="choixInclusionDescendancesServices" id="entiteSeule"/>
																				<com:TTranslate>UNIQUEMENT_CETTE_ENTITE</com:TTranslate>
																			</label>
																		</div>
																		<div class="radio radio-inline pull-right">
																			<label for="ctl0_CONTENU_PAGE_AdvancedSearch_inclureDescendances"
																				   data-toggle="tooltip"
																				   title="<%=Prado::Localize('INCLURE_LES_DESCENDANTS')%>">
																				<com:TRadioButton GroupName="choixInclusionDescendancesServices" id="inclureDescendances"/>
																				<com:TTranslate>INCLURE_LES_DESCENDANTS</com:TTranslate>
																			</label>
																		</div>
																	</div>
																</div>
																<!-- Fin bloc inclure la descendance -->
															</div>
															<!--Fin Ligne Ministere-->
															<com:TLabel ID="scriptPurchaseNames"/>
														</com:TActivePanel>
													</div>
													</com:TPanel>
												</div>
											</div>
										</div>
										<div class="col-md-4">
											<div class="btn-group pull-right" data-toggle="buttons">
												<label id="labelAutoComp" class="btn btn-primary active btn-sm" for="<%=$this->typeRechercheEntiteAutocomplete->getClientID()%>">
													<com:TRadioButton GroupName="type_rechercheEntite"
																	  value="floue" ID="typeRechercheEntiteAutocomplete"
																	  Checked="true"/>
													<a href="#tabSearchInEntreprise" id="dropdown2-tab"
													   data-toggle="tab" onclick="displayInputAutocomplete()"
													   aria-expanded="true">
														<com:TTranslate>RECHERCHE_PAR_AUTOCOMPLETION</com:TTranslate>
													</a>
												</label>
												<label id="labelSelect" class="btn btn-primary btn-sm" for="<%=$this->typeRechercheEntiteListe->getClientID()%>">
													<com:TRadioButton GroupName="type_rechercheEntite"
																	  value="exact"
																	  ID="typeRechercheEntiteListe"
																	  Checked="true"/>
													<a href="#tabSearchAnnoncesMarches" id="dropdown1-tab"
													   data-toggle="tab"
													   aria-expanded="false">
														<com:TTranslate>RECHERCHE_DANS_LISTE</com:TTranslate>
													</a>
												</label>
											</div>
										</div>
									</div>
								</com:TPanel>
								<!-- END BLOCK ENTITE PUBLIQUE TOGGLE-->

								<div>

									<!-- BEGIN ROW REFERENCE-->
									<div class="col-md-12 m-b-1 p-l-0">
										<div class="row line">
											<label class="col-md-2 intitule-150 text-left p-l-0" for="<%=$this->reference->getClientID()%>">
												<com:TTranslate>TEXT_REFERENCE</com:TTranslate>
											</label>
											<div class="col-md-9 p-l-0">
												<com:TTextBox ID="reference" CssClass="long"/>
											</div>

										</div>
									</div>
									<!-- END ROW REFERENCE-->

									<!-- BEGIN ROW TYPE D'ANNONCE-->
									<div class="col-md-12 m-b-1 p-l-0" style="display:<%=($this->getBaseDce()) ? 'none':'' %>;">
										<div class="row line">
											<label class="col-md-2 intitule-150 text-left p-l-0" for="<%=$this->annonceType->getClientID()%>">
												<com:TTranslate>TYPE_ANNONCE</com:TTranslate>
											</label>
											<div class="col-md-9 p-l-0">
												<com:TDropDownList ID="annonceType" CssClass="long"></com:TDropDownList>
											</div>
										</div>
									</div>
									<!-- END ROW TYPE D'ANNONCE-->

									<!-- BEGIN ROW TYPE PROCEDURE-->
									<div class="col-md-12 m-b-1 p-l-0" style="display:<%=($this->getBaseDce()) ? 'none':'' %>;">
										<div class="row line">
											<label class="col-md-2 intitule-150 text-left p-l-0" for="ctl0_CONTENU_PAGE_AdvancedSearch_procedureType">
												<com:TTranslate>TEXT_TYPE_PROCEDURE</com:TTranslate>
											</label>
											<div class="col-md-9 p-l-0">
												<com:TDropDownList ID="procedureType" CssClass="long"></com:TDropDownList>
											</div>
										</div>
									</div>
									<!-- END ROW TYPE PROCEDURE-->

									<!-- BEGIN ROW CATEGORIE PRINCIPALE-->
									<div class="col-md-12 m-b-1 p-l-0">
										<div class="row line">
											<label class="col-md-2 intitule-150 text-left p-l-0" for="ctl0_CONTENU_PAGE_AdvancedSearch_categorie">
												<com:TTranslate>DEFINE_CATEGORIE_PRINCIPAL</com:TTranslate>
											</label>
											<div class="col-md-9 p-l-0">
												<com:TDropDownList ID="categorie" CssClass="long cpvCategoryCheck"/>
											</div>
										</div>
									</div>
									<!-- END ROW CATEGORIE PRINCIPALE-->

								</div>


								<div class="col-md-12 m-b-1 p-l-0">
									<!--Debut Ligne Date remise plis-->
									<com:TPanel ID="panelDateRemisePlis" Visible="<%=$this->getBaseDce()%>">
										<com:TPanel CssClass="intitule-150">
											<com:TTranslate>DEFINE_DATE_LIMITE_AR</com:TTranslate>
											:
										</com:TPanel>
										<div class="intitule-auto"><label for="ctl0_CONTENU_PAGE_dateRemisePlisStart">
											<com:TTranslate>TEXT_RECHERCHE_AVANCEE_AVANT_LE</com:TTranslate>
										</label></div>
										<div class="calendar">
											<com:TTextBox ID="dateRemisePlisStart"
														  CssClass="heure"/>
											<com:TImage ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-calendar.gif"
														Attributes.alt="<%=Prado::localize('DEFINE_CALENDRIER')%>"
														Attributes.title="<%=Prado::localize('DEFINE_CALENDRIER')%>"
														Attributes.onclick="popUpCalendar(this,document.getElementById('ctl0_CONTENU_PAGE_AdvancedSearch_dateRemisePlisStart'),'dd/mm/yyyy','');"/>
											<com:TDataTypeValidator ID="controlDateRemisePlisStart"
																	ValidationGroup="datesValidation"
																	ControlToValidate="dateRemisePlisStart" DataType="Date"
																	DateFormat="dd/M/yyyy"
																	Text="<%=Prado::localize('MSG_ERROR_FORMAT_DATE_INVALIDE')%>"
																	Display="Dynamic"/>
										</div>
										<div class="intitule-auto indent-10"><label for="ctl0_CONTENU_PAGE_dateRemisePlisEnd">
											<com:TTranslate>TEXT_RECHERCHE_AVANCEE_APRES_LE</com:TTranslate>
										</label></div>
										<div class="calendar">
											<com:TTextBox ID="dateRemisePlisEnd"
														  CssClass="heure"/>
											<com:TImage ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-calendar.gif"
														Attributes.alt="<%=Prado::localize('DEFINE_CALENDRIER')%>"
														Attributes.title="<%=Prado::localize('DEFINE_CALENDRIER')%>"
														Attributes.onclick="popUpCalendar(this,document.getElementById('ctl0_CONTENU_PAGE_AdvancedSearch_dateRemisePlisEnd'),'dd/mm/yyyy','');"/>
											<div class="info-aide-right">
												<com:TTranslate>DEFINE_FORMAT_DATE_SIMPLE</com:TTranslate>
											</div>
											<com:TDataTypeValidator ID="controlDateRemisePlisEnd" ValidationGroup="datesValidation"
																	ControlToValidate="dateRemisePlisEnd" DataType="Date"
																	DateFormat="dd/M/yyyy"
																	Text="<%=Prado::localize('MSG_ERROR_FORMAT_DATE_INVALIDE')%>"
																	Display="Dynamic"/>
										</div>
									</com:TPanel>
									<!--Fin Ligne Date remise plis-->
									<!--Debut Ligne Statut-->
									<com:TPanel ID="panelStatutConsultation" cssClass="row line panelStatutConsultation">
										<div class="intitule-150"><label>
												<com:TTranslate>DEFINE_STATUS</com:TTranslate>
											</label>
										</div>
										<com:TDropDownList ID="statut" CssClass="long"
														   Attributes.title="<%=Prado::localize('DEFINE_STATUS')%>"/>
										<com:TDropDownList ID="statutRma" CssClass="long"
														   Attributes.title="<%=Prado::localize('DEFINE_STATUS')%>"
														   Style="display: none;"/>
									</com:TPanel>
									<!--Fin Ligne Statut-->
									<!--debut Ligne Service-->
									<com:TPanel CssClass="line"
												Visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('AnnoncesMarches') && isset($_GET['doublon']))%>">
										<label for="<%=$this->doublon->getClientID()%>">
											<com:TTranslate>INTEGRER_DOUBLONS</com:TTranslate>
										</label> :
										<com:TDropDownList ID="doublon" CssClass="long"></com:TDropDownList>
									</com:TPanel>
									<!--Fin Ligne Service-->
									<com:TPanel ID="refrentielZoneText">
										<com:AtexoReferentielZoneText id="idReferentielZoneText" consultation="1" obligatoire="0"
																	  cssClass="intitule-150" mode="2" Type="1"
																	  idPage="AdvancedSearch"/>
										<com:AtexoLtRefRadio id="idAtexoLtRefRadio" consultation="1" obligatoire="0"
															 cssClass="intitule-150" mode="2" Type="2" idPage="AdvancedSearch"/>
									</com:TPanel>

									<hr class="hide-agent">
									<div class="spacer"></div>
									<com:TPanel ID="panelClauseConsultation">
										<!-- BEGIN ROW CLAUSES SOCIALES -->
										<div class="form-group form-group-sm">
											<div class="col-md-12">
												<div class="row line">
													<div class="intitule-165"><img class="float-left margin-right-5" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-clause-sociale.gif" alt="clause sociale"/>
														<label>
															<com:TTranslate>CLAUSES_SOCIALES</com:TTranslate>
														</label>
													</div>
													<label class="col-md-9 hide-agent" for="<%=$this->clauseSociales->getClientID()%>">
														<i class="s s-social s-18 m-r-1"></i>
														<com:TTranslate>CLAUSES_SOCIALES</com:TTranslate>
													</label>
													<div class="col-md-9 p-l-0">
														<com:TDropDownList ID="clauseSociales" CssClass="small-150"></com:TDropDownList>
														<a href="#clauseSociales" onmouseover="afficheBulle('infosclauseSociales', this)" onmouseout="cacheBulle('infosclauseSociales')"
														   onfocus="afficheBulle('infosclauseSociales', this)" onblur="cacheBulle('infosclauseSociales')" class="picto-info-intitule">
															<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" alt="Info-bulle"/>
														</a>
														<div onmouseout="mouseOutBulle();" onmouseover="mouseOverBulle();" class="info-bulle" id="infosclauseSociales">
															<div>
																<com:TTranslate>DEFINE_INFO_BULLE_CONSIDERATION_SOCIALE</com:TTranslate>
															</div>
														</div>
													</div>
													<div class="col-md-1 p-l-0 hide-agent">
                                                        <span data-target="#" data-html="true" data-toggle="tooltip" data-original-title="<com:TTranslate>DEFINE_INFO_BULLE_CONSIDERATION_SOCIALE</com:TTranslate>">
                                                            <i class="fa fa-question-circle text-info p-t-1"></i>
                                                        </span>
													</div>
												</div>
											</div>
										</div>
										<!-- END ROW CLAUSES SOCIALES -->

										<!-- BEGIN ROW MARCHE RESERVE -->
										<div class="col-md-8">
											<div class="form-group m-b-0">
												<div class="col-md-8 p-b-2 normal spacer">
													<strong>
														<com:TTranslate>DEFINE_TEXT_MARCHE_RESERVE</com:TTranslate> :
													</strong>
													<a href="#infoBulleMarcheReserve" onmouseover="afficheBulle('infoBulleMarcheReserve', this)"
													   onmouseout="cacheBulle('infoBulleMarcheReserve')" onfocus="afficheBulle('infoBulleMarcheReserve', this)"
													   onblur="cacheBulle('infoBulleMarcheReserve')" class="picto-info-intitule">
														<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" alt="Info-bulle"/>
													</a>
													<div onmouseout="mouseOutBulle();" onmouseover="mouseOverBulle();" class="info-bulle" id="infoBulleMarcheReserve">
														<div>
															<com:TTranslate>DEFINE_INFO_BULLE_ATELIERS_PROTEGES</com:TTranslate>
														</div>
													</div>
												</div>
												<div class="col-md-1 p-l-0 hide-agent">
                                                                    <span data-target="#" data-html="true" data-toggle="tooltip" data-original-title="<com:TTranslate>DEFINE_INFORMATION_BULLE_MARCHE_RESERVE</com:TTranslate>">
                                                                        <i class="fa fa-question-circle text-info p-t-1"></i>
                                                                    </span>
												</div>

												<!--Debut Ligne ESAT/EA -->
												<div class="col-md-12 m-b-2">
													<div class="row">
														<label class="col-md-4 multiline line-height-normal"
															   for="ctl0_CONTENU_PAGE_AdvancedSearch_ateliersProteges"
															   data-toggle="tooltip"
															   title="<%=Prado::localize('ETABLISSEMENTS_SERVICES_AIDE_TRAVAIL_ENTREPRISES_ADAPTEES')%>">
															<com:TTranslate>ESAT_EA</com:TTranslate>
														</label>
														<div class="col-md-8 p-l-0">
															<div class="row">
																<div class="col-md-11 p-l-0">
																	<com:TDropDownList ID="ateliersProteges" CssClass="small-150"/>
																</div>
															</div>
														</div>
													</div>
												</div>
												<!--Fin Ligne ESAT/EA -->
												<!--Debut Ligne SIAE -->
												<div class="col-md-12 m-b-2">
													<div class="row">
														<label class="col-md-4 multiline line-height-normal"
															   for="ctl0_CONTENU_PAGE_AdvancedSearch_siae"
															   data-toggle="tooltip"
															   title="<%=Prado::localize('STRUCTURES_INSERTION_ACTIVITE_ECONOMIQUE')%>">
															<com:TTranslate>SIAE</com:TTranslate>
														</label>
														<div class="col-md-8 p-l-0">
															<div class="row">
																<div class="col-md-11 p-l-0">
																	<com:TDropDownList ID="siae" CssClass="small-150" />
																</div>
															</div>
														</div>
													</div>
												</div>
												<!--Fin Ligne SIAE -->

												<!--Debut Ligne EESSs -->
												<div class="col-md-12 m-b-2">
													<div class="row">
														<label class="col-md-4 multiline line-height-normal"
															   for="ctl0_CONTENU_PAGE_AdvancedSearch_ess"
															   data-toggle="tooltip" title="<%=Prado::localize('ENTREPRISE_ECONOMIE_SOCIALE_SOLIDAIRE')%>">
															<com:TTranslate>EESS</com:TTranslate>
														</label>
														<div class="col-md-8 p-l-0">
															<div class="row">
																<div class="col-md-11 p-l-0">
																	<com:TDropDownList ID="ess" CssClass="small-150" />
																</div>
															</div>
														</div>
													</div>
												</div>
												<!--Fin Ligne EESS -->

												<!-- BEGIN ROW CLAUSES SOCIALES SOUS CATEGORIE-->
												<com:TPanel id="SSC" Visible="<%=($this->_calledFrom == 'entreprise')%>" >
												<div class="col-md-12 m-b-2 line indent-30">
													<div class="row">
														<label class="col-md-3 multiline intitule-120 line-height-normal"
															   for="<%=$this->clauseSocialesCommerceEquitable->getClientID()%>">
															<com:TTranslate>CLAUSES_COMMERCE_EQUITABLE</com:TTranslate>
														</label>
														<div class="col-md-9">
															<div class="row">
																<div class="col-md-offset-5 col-md-11">
																	<com:TDropDownList ID="clauseSocialesCommerceEquitable" CssClass="small-150"></com:TDropDownList>
																	<a href="#clauseSocialesCommerceEquitable"
																	   onmouseover="afficheBulle('infosclauseSocialesCommerceEquitable', this)"
																	   onmouseout="cacheBulle('infosclauseSocialesCommerceEquitable')"
																	   onfocus="afficheBulle('infosclauseSocialesCommerceEquitable', this)"
																	   onblur="cacheBulle('infosclauseSocialesCommerceEquitable')"
																	   class="picto-info-intitule">
																		<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" alt="Info-bulle"/>
																	</a>
																	<div onmouseout="mouseOutBulle();"
																	 onmouseover="mouseOverBulle();"
																	 class="info-bulle"
																	 id="infosclauseSocialesCommerceEquitable">
																		<div>
																			<com:TTranslate>
																				DEFINE_INFO_BULLE_COMMERCE_EQUITABLE
																			</com:TTranslate>
																		</div>
																	</div>
																</div>

																<div class="col-md-1 p-l-0 hide-agent">
																	<span data-target="#" data-toggle="tooltip"
																		  data-original-title="<com:TTranslate>DEFINE_INFO_BULLE_COMMERCE_EQUITABLE</com:TTranslate>">
																		<i class="fa fa-question-circle text-info p-t-1"></i>
																	</span>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="col-md-12 m-b-2 line indent-30">
													<div class="row">
														<label class="col-md-3 p-b-2 indent-30 normal spacer"
															   for="<%=$this->clauseSocialesInsertionActiviteEconomique->getClientID()%>">
															<com:TTranslate>CLAUSES_INSERTION_ACTIVITER_ECONOMIQUE</com:TTranslate>
														</label>

														<div class="col-md-9">
															<div class="row">
																<div class="col-md-11">
																	<com:TDropDownList ID="clauseSocialesInsertionActiviteEconomique" CssClass="small-150"></com:TDropDownList>
																	<a href="#clauseSocialesInsertionActiviteEconomique"
																	   onmouseover="afficheBulle('infosclauseSocialesInsertionActiviteEconomique', this)"
																	   onmouseout="cacheBulle('infosclauseSocialesInsertionActiviteEconomique')"
																	   onfocus="afficheBulle('infosclauseSocialesInsertionActiviteEconomique', this)"
																	   onblur="cacheBulle('infosclauseSocialesInsertionActiviteEconomique')"
																	   class="hide picto-info-intitule">
																		<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif"
																			 alt="Info-bulle"/>
																	</a>
																	<div onmouseout="mouseOutBulle();"
																		 onmouseover="mouseOverBulle();"
																		 class="hide info-bulle"
																		 id="infosclauseSocialesInsertionActiviteEconomique">
																		<div>
																			<com:TTranslate>
																				DEFINE_INFO_BULLE_CLAUSES_SOCIALES_INSERTION_ACTIVITER_ECONOMIQUE
																			</com:TTranslate>
																		</div>
																	</div>
																</div>
																<div class="col-md-1 p-l-0 hide-agent">
																<span data-target="#" data-toggle="tooltip"
																	  data-original-title="<com:TTranslate>DEFINE_INFO_BULLE_CLAUSES_SOCIALES_INSERTION_ACTIVITER_ECONOMIQUE</com:TTranslate>">
																	<i class="fa fa-question-circle text-info p-t-1"></i>
																</span>
																</div>
															</div>
														</div>
													</div>
												</div>
												</com:TPanel>
												<!-- END ROW CLAUSES SOCIALES SOUS CATEGORIE-->

											</div>
										</div>
										<!-- END ROW MARCHE RESERVE -->
										<div class="spacer"></div>
										<!--BEGIN CLAUSES ENVIRONNEMENTALES-->
										<div class="form-group form-group-sm">
											<div class="col-md-12">
												<div class="row line">
													<div class="col-lg-4 intitule-150 line-height-normal p-l-0"><img class="float-left margin-right-5" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-clause-environnementale.gif"
																										   alt="clauses environnementale"/><label>
															<com:TTranslate>CLAUSES_ENVIRONNEMENTALES</com:TTranslate>
														</label>
													</div>
													<label class=" col-md-2 multiline hide-agent" for="ctl0_CONTENU_PAGE_AdvancedSearch_clauseEnvironnementale">
														<i class="s s-environnement s-18 m-r-1"></i>
														<com:TTranslate>CLAUSES_ENVIRONNEMENTALES</com:TTranslate>
													</label>
													<div class="col-md-9 p-l-0">
														<com:TDropDownList ID="clauseEnvironnementale" CssClass="small-150"></com:TDropDownList>
														<a href="#clauseEnvironnementale" onmouseover="afficheBulle('infosclauseEnvironnementales', this)"
														   onmouseout="cacheBulle('infosclauseEnvironnementales')" onfocus="afficheBulle('infosclauseEnvironnementales', this)"
														   onblur="cacheBulle('infosclauseEnvironnementales')" class="picto-info-intitule">
															<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" alt="Info-bulle"/>
														</a>
														<div onmouseout="mouseOutBulle();" onmouseover="mouseOverBulle();" class="info-bulle" id="infosclauseEnvironnementales">
															<div>
																<com:TTranslate>DEFINE_INFO_BULLE_CLAUSES_ENVIRONNEMENTALES</com:TTranslate>
															</div>
														</div>
													</div>
													<div class="col-md-1 p-l-0 hide-agent">
                                                        <span data-target="#" data-toggle="tooltip" data-original-title="<com:TTranslate>DEFINE_INFO_BULLE_CLAUSES_ENVIRONNEMENTALES</com:TTranslate>"><i
																	class="fa fa-question-circle text-info p-t-1"></i>
                                                        </span>
													</div>
												</div>
											</div>
										</div>
										<!--END CLAUSES ENVIRONNEMENTALES-->
										<div class="spacer-small"></div>
										<!-- BEGIN CONSULTATION MPS-->
										<div class="form-group form-group-sm" style="display:<%=!Application\Service\Atexo\Atexo_Module::isEnabled('MasquerElementsMps') ?'block':'none'%>;">
											<div class="col-md-12">
												<com:TPanel ID="panelMarchesPublicsSimplifies" visible="<%=!Application\Service\Atexo\Atexo_Module::isEnabled('MasquerElementsMps')%>" cssClass="row line">
													<div class=" intitule-150"><img class="float-left margin-right-5" src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/logo-mps-small.png' alt="logo MPS"/><label>
															<com:TTranslate>MARCHE_PUBLIC_SIMPLIFIE</com:TTranslate>
														</label>
													</div>
													<label class="col-md-2 hide-agent" for="ctl0_CONTENU_PAGE_AdvancedSearch_marchesPublicsSimplifies">
														<i class="s s-mps s-18 m-r-1"></i>
														<com:TTranslate>MARCHE_PUBLIC_SIMPLIFIE</com:TTranslate>
													</label>
													<div class="col-md-2">
														<com:TDropDownList ID="marchesPublicsSimplifies" CssClass="small-150"></com:TDropDownList>
														<a href="#marchesPublicsSimplifies" onmouseover="afficheBulle('infosMarchesPublicsSimplifies', this)"
														   onmouseout="cacheBulle('infosMarchesPublicsSimplifies')" onfocus="afficheBulle('infosMarchesPublicsSimplifies', this)"
														   onblur="cacheBulle('infosMarchesPublicsSimplifies')" class=" picto-info-intitule">
															<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" alt="Info-bulle"/>
														</a>
														<div onmouseout="mouseOutBulle();" onmouseover="mouseOverBulle();" class=" info-bulle" id="infosMarchesPublicsSimplifies">
															<div>
																<com:TLabel text="<%=$this->getMessageMPS()%>"></com:TLabel>
															</div>
														</div>
													</div>
													<div class="col-md-1 p-l-0 hide-agent">
                                                        <span data-target="#" data-toggle="tooltip" data-html="true" data-delay="{ 'show': 500, 'hide': 2000 }"
															  data-original-title="<%= $this->htmlspecialchars($this->getMessageMPS())%>">
                                                            <i class="fa fa-question-circle text-info p-t-1"></i>
                                                        </span>
														<a class="lien-externe small" href="http://www.modernisation.gouv.fr/les-services-publics-se-simplifient-et-innovent/par-des-simplifications-pour-les-entreprises/marche-public-simplifie" target="_blank" title="Lien externe">En savoir plus</a>
													</div>
												</com:TPanel>
											</div>
										</div>
										<!--END CONSULTATION MPS-->
										<div class="spacer-small"></div>
									</com:TPanel>
								</div>
								<!--BEGIN INSCRIT A LA BOURSE A LA CO-TRAITANCE-->
								<com:TPanel ID="panelBourseCotraitance" cssClass="form-group form-group-sm" style="display:none;">
									<label for="ctl0_CONTENU_PAGE_AdvancedSearch_bourseCotraitance" class="col-md-2 multiline">
										<com:TTranslate>DEFINE_INSCRIT_BOURSE_COTRAITANCE</com:TTranslate>
									</label>
									<div class="col-md-2">
										<com:TDropDownList ID="bourseCotraitance" CssClass=""></com:TDropDownList>
									</div>
								</com:TPanel>
								<!--END INSCRIT A LA BOURSE A LA CO-TRAITANCE -->

								<!-- BEGIN ROW LIEU D'EXECUTION -->
								<com:TActivePanel id="lieuExecution" CssClass="form-group form-group-sm line m-b-0" Visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('LieuxExecution'))? true:false %>">
									<label class="col-md-2 hide-agent">
										<com:TTranslate>TEXT_LIEU_EXECUTION</com:TTranslate>
									</label>
									<div class="intitule-150">
										<com:TTranslate>TEXT_LIEU_EXECUTION</com:TTranslate>
										<a href="#lieuExecution" onmouseover="afficheBulle('infosLieuExecution', this)" onmouseout="cacheBulle('infosLieuExecution')"
										   onfocus="afficheBulle('infosLieuExecution', this)" onblur="cacheBulle('infosLieuExecution')" class=" picto-info-intitule">
											<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" alt="Info-bulle"/>
										</a>
									</div>
									<div id="infosLieuExecution" class="info-bulle" onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();">
										<div>
											<com:TTranslate>DEFINE_LIEU_EXECUTION_INFOS_BULLE</com:TTranslate>
										</div>
									</div>
									<div class="col-md-8 p-t-1 content-bloc bloc-500 text-justify p-l-4">
										<div id="lieuExecution" class="pull-left">
											<com:THiddenField ID="idsSelectedGeoN2"/>
											<com:THiddenField ID="numSelectedGeoN2"/>
											<com:TActiveLabel ID="denominationGeoN2"/>
											<a href="<%=$this->getLieuExecutionUrl()%>" class="bouton-small" data-toggle="tooltip"
											   data-original-title="<%=Prado::localize('DEFINE_LIEU_EXECUTION')%> <%=Prado::localize('NOUVELLE_FENETRE')%>">
												<%=Prado::localize('DEFINE_DETAILS')%>
											</a>
											<com:THyperLink ID="linkLieuExe1" cssClass="" Attributes.style="display:none;"/>
											<com:TActiveButton
													Display="None"
													ID="selectedGeoN2"
													OnCommand="Page.AdvancedSearch.displaySelectedGeoN2"
													OnCallBack="Page.AdvancedSearch.refreshDisplaySelectedGeoN2"/>
										</div>
										<span class="pull-left m-l-1" data-target="#" data-toggle="tooltip" data-original-title="<com:TTranslate>DEFINE_LIEU_EXECUTION_INFOS_BULLE</com:TTranslate>">
                                            <i class="fa fa-question-circle text-info hide-agent"></i>
                                        </span>
									</div>
								</com:TActivePanel>
								<!-- END ROW LIEU D'EXECUTION -->

								<!-- debut codes nuts -->
								<com:TActivePanel id="codesNuts" CssClass="form-group form-group-sm" Visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('CodeNutLtReferentiel'))? true:false %>">
									<div class="intitule-150">
										<com:TTranslate>TEXT_LIEU_EXECUTION</com:TTranslate>
									</div>
									<div class="content-bloc bloc-500">
										<com:AtexoReferentiel ID="idAtexoRefNuts" cas="cas5"/>
									</div>
								</com:TActivePanel>
								<!-- fin codes nuts -->
								<com:TActivePanel ID="panelDomainesActivites" cssClass="line" Visible="false">
									<div class="intitule-150">
										<com:TTranslate>TEXT_DOMAINES_ACTIVITE</com:TTranslate>
									</div>
									<com:AtexoDomainesActivites ID="domaineActivite"/>
								</com:TActivePanel>

								<!-- Debut domaine d'activites Lt-Ref -->
								<com:TPanel ID="panelDomainesActivitesLtRef" cssClass="line" Visible="false">
									<div class="intitule-150">
										<com:TTranslate>TEXT_DOMAINES_ACTIVITE</com:TTranslate>
									</div>
									<com:AtexoReferentiel ID="idAtexoRefDomaineActivites" cas="cas5"/>
								</com:TPanel>
								<!-- Fin domaine d'activites Lt-Ref -->

								<com:TActivePanel ID="panelQualifications" cssClass="line" Visible="false">
									<div class="intitule-150">
										<com:TTranslate>TEXT_QUALIFICATIONS</com:TTranslate>
									</div>
									<com:AtexoQualification ID="qualification"/>
								</com:TActivePanel>

								<com:TActivePanel ID="panelAgrements" cssClass="line" Visible="false">
									<div class="intitule-bloc intitule-150"><label for="agrenments">
											<com:TTranslate>TEXT_AGREMENT</com:TTranslate>
										</label>
									</div>
									<com:PanelAgrements ID="agrements"/>
								</com:TActivePanel>

								<!--BEGIN CPV-->
								<div class="form-group form-group-sm">

									<com:AtexoCodesCpv id="referentielCPV" titre="<%=Prado::localize('TEXT_CODE_CPV')%>"/>

									<com:AtexoLtReferentiel id="ltRefCons" consultation="1" cssClass="intitule-150" mode="2" idPage="AdvancedSearch"/>

								</div>
								<!--END CPV-->

								<!--Debut Ligne Recherche alerte-->
								<com:TPanel ID="panelAlerte">
									<div class="spacer"></div>
									<h2>
										<com:TTranslate>TEXT_RECHERCHE_PAR_ALERTE_METIER</com:TTranslate>
									</h2>
									<div class="line">
										<div class="intitule-150"><label for="regleNonObservee">
												<com:TTranslate>TEXT_REGLE_NON_OBSERVEE</com:TTranslate>
											</label> :
										</div>
										<div class="content-bloc bloc-600">
											<com:TDropDownList ID="regleNonObservee" CssClass="long"
															   Attributes.title="<%=Prado::localize('TEXT_REGLE_NON_OBSERVEE')%>"></com:TDropDownList>
											<div class="content-bloc bloc-600">
												<div class="line">
                                        <span class="radio"><com:TCheckBox id="affichageAlerteNonCloturee"
																		   Checked="true"
																		   Attributes.title="<%=Prado::localize('TEXT_AFFICHER_CONSULTATION_ALERTE_NON_CLOTUREE')%>"
																		   CssClass="check"/></span><label
															for="AdvancedSearch_floue">
														<com:TTranslate>TEXT_AFFICHER_CONSULTATION_ALERTE_NON_CLOTUREE</com:TTranslate>
													</label>
												</div>
											</div>

										</div>
									</div>
									<div class="spacer-small"></div>
								</com:TPanel>
								<!--Fin Ligne Recherche alerte-->

								<!-- BEGIN ROW SEARCH BY DATE-->
								<section class="section">
									<div class="m-t-3 m-l-1 form-group">
										<com:TLabel ID="titreDate">
											<h3 class="m-b-0 advancedSearchH2 margin-left-6">
												<com:TTranslate>DEFINE_RECHERCHE_PAR_DATE</com:TTranslate>
											</h3>
										</com:TLabel>
									</div>
									<div class="section-body margin-left-6">
										<fieldset>
										<com:TPanel ID="panelDate" CssClass="form-group form-group-sm line m-b-0">
										<div class="row">
										<com:TPanel CssClass="" Visible="<%=!isset($_GET['annonce'])%>">
											<div class="col-md-3 multiline line-height-normal date_remise_plis" style="margin-right: 8px;">
												<label class=""><com:TTranslate>DEFINE_DATE_LIMITE_AR</com:TTranslate></label>
											</div>
											</com:TPanel>
                                                <com:TPanel CssClass="col-md-2 multiline intitule-150 line-height-normal" Visible="<%=isset($_GET['annonce'])%>">
                                                    <label class="">
                                                      <com:TTranslate>DEFINE_TEXT_DATE_FIN_AFFICHAGE</com:TTranslate>
                                                    </label>
                                                </com:TPanel>

											<div class="form-inline col-md-9 p-l-0">
												<div class="col-md-6 formulaire-inline p-l-0">
													<div class="col-md-4 p-t-1 intitule-auto p-l-0">
														<com:TTranslate>TEXT_RECHERCHE_AVANCEE_AVANT_LE</com:TTranslate>
													</div>
													<div class="datetimepicker col-md-7 calendar" id="dateMiseEnLigneStart">
														<com:TTextBox ID="dateMiseEnLigneStart"
																	  CssClass="text-center"
																	  Attributes.Onchange="supprimerEspace(this);"
																	  Attributes.placeholder="<%=Prado::localize('DEFINE_FORMAT_DATE_SIMPLE')%>"
																	  Attributes.title="<%=Prado::localize('TEXT_RECHERCHE_AVANCEE_AVANT_LE')%> <%=Prado::localize('TEXT_RECHERCHE_AVANCEE_DATE_FORMAT')%>"/>
														<com:TImage cssClass="" ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-calendar.gif" Attributes.alt="<%=Prado::localize('DEFINE_CALENDRIER')%>"
																	Attributes.title="<%=Prado::localize('DEFINE_CALENDRIER')%>"
																	Attributes.onclick="popUpCalendar(this,document.getElementById('ctl0_CONTENU_PAGE_AdvancedSearch_dateMiseEnLigneStart'),'dd/mm/yyyy','');"/>
														<com:TDataTypeValidator ID="controlDateMiseEnLigneStart"
																				ValidationGroup="datesValidation"
																				ControlToValidate="dateMiseEnLigneStart"
																				DataType="Date"
																				DateFormat="dd/M/yyyy"
																				Text="<%=Prado::localize('MSG_ERROR_FORMAT_DATE_INVALIDE')%>"
																				Display="Dynamic"/>
														<span class="input-group-addon hide-agent">
															<i class="fa fa-calendar"></i>
														</span>
													</div>

													<div class="col-md-3 p-t-1 intitule-auto indent-10">
                                                        <div class="text-center">
                                                            <com:TTranslate>TEXT_RECHERCHE_AVANCEE_APRES_LE</com:TTranslate>
                                                        </div>
                                                    </div>
                                                    <div class="datetimepicker col-md-8 calendar">
                                                        <com:TTextBox ID="dateMiseEnLigneEnd"
                                                                      CssClass="text-center"
                                                                      Attributes.Onchange="supprimerEspace(this);"
                                                                      Attributes.placeholder="<%=Prado::localize('DEFINE_FORMAT_DATE_SIMPLE')%>"
                                                                      Attributes.title="<%=Prado::localize('TEXT_RECHERCHE_AVANCEE_APRES_LE')%> <%=Prado::localize('TEXT_RECHERCHE_AVANCEE_DATE_FORMAT')%>"/>
                                                        <com:TImage cssClass="" ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-calendar.gif" Attributes.alt="<%=Prado::localize('DEFINE_CALENDRIER')%>"
                                                                    Attributes.title="<%=Prado::localize('DEFINE_CALENDRIER')%>"
                                                                    Attributes.onclick="popUpCalendar(this,document.getElementById('ctl0_CONTENU_PAGE_AdvancedSearch_dateMiseEnLigneEnd'),'dd/mm/yyyy','');"/>
                                                        <!-- inofrmation sur format de la date JJ/MM/AAAA -->
                                                        <com:TDataTypeValidator ID="controlDateMiseEnLigneEnd"
                                                                                ValidationGroup="datesValidation"
                                                                                ControlToValidate="dateMiseEnLigneEnd"
                                                                                DataType="Date"
                                                                                DateFormat="dd/M/yyyy"
                                                                                Text="<%=Prado::localize('MSG_ERROR_FORMAT_DATE_INVALIDE')%>"
                                                                                Display="Dynamic"/>
                                                        <span class="hide-agent">
                                                                <i class="fa fa-calendar"></i>
                                                            </span>
                                                    </div>
												</div>
											</div>
										</div>

										</com:TPanel>
										</fieldset>
										<fieldset>
										<com:TPanel ID="panelDateMiseEnLigneCalcule" CssClass="form-group form-group-sm line m-b-0"
													Visible="<%=!isset($_GET['annonce']) && !$this->getBaseDce()%>">
											<div class="col-md-2 multiline line-height-normal p-l-0">
												<label class=""><com:TTranslate>DEFINE_DATE_MISE_LIGNE</com:TTranslate></label>
											</div>
											<div class="col-md-9">
												<div class="col-md-9 formulaire-inline date-begin-margin">
													<div class="col-md-4 p-t-1 p-l-0 intitule-auto">
														<com:TTranslate>TEXT_RECHERCHE_AVANCEE_AVANT_LE</com:TTranslate>
													</div>
													<div class="datetimepicker col-md-7 calendar">
														<com:TTextBox ID="dateMiseEnLigneCalculeStart"
																	  CssClass="text-center"
																	  Attributes.Onchange="supprimerEspace(this);"
																	  Attributes.placeholder="<%=Prado::localize('DEFINE_FORMAT_DATE_SIMPLE')%>"
																	  Attributes.title="<%=Prado::localize('TEXT_RECHERCHE_AVANCEE_AVANT_LE')%> <%=Prado::localize('TEXT_RECHERCHE_AVANCEE_DATE_FORMAT')%>"/>
														<com:TImage cssClass="" ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-calendar.gif" Attributes.alt="<%=Prado::localize('DEFINE_CALENDRIER')%>"
																	Attributes.title="<%=Prado::localize('DEFINE_CALENDRIER')%>"
																	Attributes.onclick="popUpCalendar(this,document.getElementById('ctl0_CONTENU_PAGE_AdvancedSearch_dateMiseEnLigneCalculeStart'),'dd/mm/yyyy','');"/>
														<com:TDataTypeValidator ID="controlDateMiseEnLigneCalculeStart"
																				ValidationGroup="datesValidation"
																				ControlToValidate="dateMiseEnLigneCalculeStart"
																				DataType="Date"
																				DateFormat="dd/M/yyyy"
																				Text="<%=Prado::localize('MSG_ERROR_FORMAT_DATE_INVALIDE')%>"
																				Display="Dynamic"/>
														<span class="hide-agent">
                                                                <i class="fa fa-calendar"></i>
                                                            </span>
													</div>
													<div class="col-md-3 p-t-1 intitule-auto indent-10">
                                                        <div class="text-center">
                                                            <com:TTranslate>TEXT_RECHERCHE_AVANCEE_APRES_LE</com:TTranslate>
                                                        </div>
                                                    </div>
                                                    <div class="input-group datetimepicker col-md-8 calendar">
                                                        <com:TTextBox ID="dateMiseEnLigneCalculeEnd"
                                                                      CssClass="text-center"
                                                                      Attributes.Onchange="supprimerEspace(this);"
                                                                      Attributes.placeholder="<%=Prado::localize('DEFINE_FORMAT_DATE_SIMPLE')%>"
                                                                      Attributes.title="<%=Prado::localize('TEXT_RECHERCHE_AVANCEE_APRES_LE')%> <%=Prado::localize('TEXT_RECHERCHE_AVANCEE_DATE_FORMAT')%>"/>
                                                        <com:TImage cssClass="" ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-calendar.gif" Attributes.alt="<%=Prado::localize('DEFINE_CALENDRIER')%>"
                                                                    Attributes.title="<%=Prado::localize('DEFINE_CALENDRIER')%>"
                                                                    Attributes.onclick="popUpCalendar(this,document.getElementById('ctl0_CONTENU_PAGE_AdvancedSearch_dateMiseEnLigneCalculeEnd'),'dd/mm/yyyy','');"/>
                                                        <div class="info-aide-right ">
                                                            <com:TTranslate>DEFINE_FORMAT_DATE_SIMPLE</com:TTranslate>
                                                        </div>
                                                        <com:TDataTypeValidator ID="controlDateMiseEnLigneCalculeEnd"
                                                                                ValidationGroup="datesValidation"
                                                                                ControlToValidate="dateMiseEnLigneCalculeEnd"
                                                                                DataType="Date"
                                                                                DateFormat="dd/M/yyyy"
                                                                                Text="<%=Prado::localize('MSG_ERROR_FORMAT_DATE_INVALIDE')%>"
                                                                                Display="Dynamic"/>
                                                        <span class="input-group-addon hide-agent">
                                                                <i class="fa fa-calendar"></i>
                                                            </span>
                                                    </div>
												</div>
											</div>
										</com:TPanel>
										</fieldset>
									</div>
									<div class="advancedSearchH2 form-group form-field">
										<h3><com:TTranslate>DEFINE_RECHERCHE_MOT_CLE</com:TTranslate></h3>
										<div class="line">
											<div class="intitule-bloc bloc-155"><label for="ctl0_CONTENU_PAGE_keywordSearchBottom"><com:TTranslate>DEFINE_RECHERCHE_REF_TITRE_OBJET</com:TTranslate></label></div>
											<div class="intitule-bloc bloc-155">
                                                <com:TTextBox ID="keywordSearchBottom" Attributes.title="<%=Prado::localize('DEFINE_RECHERCHE_REF_TITRE_OBJET')%>" CssClass="long"/>
                                                <com:TPanel ID="panelMotsClesBottom" cssClass="content-bloc bloc-600">
                                                    <com:TPanel id="panelRechercheFloueBottom" CssClass="line">
                                                        <com:TRadioButton Attributes.title="<%=Prado::localize('TEXT_RECHERCHE_FLOUE')%>" GroupName="rechercheFloue" ID="floueBottom" Checked="true" CssClass="radio"/>
                                                        <label for="floueBottom"><com:TTranslate>TEXT_RECHERCHE_FLOUE</com:TTranslate></label>
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <com:TRadioButton Attributes.title="<%=Prado::localize('TEXT_RECHERCHE_EXACTE')%>"
                                                                          GroupName="rechercheFloue" ID="exactBottom" Checked="false" CssClass="radio" /><label for="exacteBottom">
                                                        <com:TTranslate>TEXT_RECHERCHE_EXACTE</com:TTranslate></label>
                                                    </com:TPanel>
                                                </com:TPanel>
											</div>
										</div>
									</div>
								</section>
								<!-- END ROW SEARCH BY DATE-->
								<div class="spacer"></div>
							</div>

						</section>
						<!-- END ROW SEARCH BY KEYWORDS-->
					</div>
					<div>
						<div class="clearfix fix-footer">
							<com:TActivePanel ID="panelButton" cssClass="">
								<div class="pull-left">
									<input type="reset" style="display:none" id="bouttonReset"
										   class="m-r-1 pull-left"
										   value="<%=Prado::localize('DEFINE_EFFACER_CRITERE_RECHERCHE')%>"
										   title="<%=Prado::localize('DEFINE_EFFACER_CRITERE_RECHERCHE')%>"/>
									<com:TButton id="boutonClear"
												 Attributes.onclick="javascript:document.getElementById('bouttonReset').click();"
												 CssClass="pull-left bouton-long-190 float-left"
												 OnCommand="Page.AdvancedSearch.cleareCritaire"/>
								</div>
								<div class="pull-right">
									<com:TPanel
											visible="<%=( (Application\Service\Atexo\Atexo_CurrentUser::isConnected() && Application\Service\Atexo\Atexo_Module::isEnabled('RecherchesFavoritesAgent', Application\Service\Atexo\Atexo_CurrentUser::getCurrentOrganism()) && $this->_calledFrom == 'agent' && ( isset($_GET['searchAnnCons']) || isset($_GET['annonce']) ) && !isset($_GET['BaseDce']) && !isset($_GET['archive']) )
                   					||(Application\Service\Atexo\Atexo_CurrentUser::isConnected() && Application\Service\Atexo\Atexo_Module::isEnabled('RecherchesFavorites') && $this->_calledFrom == 'entreprise' && !isset($_GET['panierEntreprise']) && (isset($_GET['searchAnnCons']) || isset($_GET['annonce']) || isset($_GET['AllAnn']))&& !isset($_GET['BaseDce'])  && !($this->getBaseDce()))
									) ? true:false%>;"
											CssClass="clearfix pull-left">
										<button type="button" class="pull-left m-r-1 definir-recherche bouton-long-190"
												data-toggle="modal" data-target="#modalSaveSearch"
												onclick="initialiserSauvegardeRecherche(document.getElementById('<%=$this->rechercheName->getClientId()%>'),document.getElementById('<%=$this->sauvegardeRecherche->getClientId()%>'),document.getElementById('<%=$this->sauvegardeAlerte->getClientId()%>'),'<%=$this->infoCompAlerte->getClientId()%>','<%=$_GET['idRecherche']%>');">
											<%=Prado::localize('TEXT_SAUVEGARDER_RECHERCHE')%>
										</button>
									</com:TPanel>
									<com:TButton id="lancerRecherche" CssClass="pull-left bouton-moyen-120 float-right"
												 Text="<%=Prado::localize('DEFINE_LANCER_RECHERCHE')%>" ValidationGroup="datesValidation"
												 OnClick="onSearchClick"/>
								</div>
							</com:TActivePanel>

						</div>
					</div>
				</div>
				<div class="panel panel-default tab-pane fade" id="restreinte">
					<com:TActivePanel ID="panelSearchPrRest" cssClass="form-field">
						<div class="panel-body">
							<div class="content">
								<section class="section">
									<div class="text-right m-b-2 small">
										<com:TTranslate>DEFINE_SYMBOLE</com:TTranslate>
										<span class="text-danger">*</span>
										<com:TTranslate>TEXT_INDIQUE_CHAMPS_OBLIGATOIRES</com:TTranslate>
									</div>
									<div class="section-body">
										<com:TPanel ID="panelSearchByClassificationConsRestreinte" Visible="false">
											<div class="form-group form-group-sm">
												<label class="col-md-2" for="ctl0_CONTENU_PAGE_classification">
													<com:TTranslate>ETAT_COLLECTIVITE_LOCAL_ETABLISSEMENT_PUBLIQUE</com:TTranslate>
												</label>
												<div class="col-md-10">
													<com:TActiveDropDownList ID="classificationConsRestreinte" cssClass="form-control" AutoPostBack="true"
																			 onCallBack="choixClassificationConsRestreinte" Attributes.title="Classifications"/>
												</div>
											</div>
										</com:TPanel>

										<!--BEGIN ENTITE PUBLIQUE-->
										<div class="form-group form-group-sm">
											<label class="col-md-2 control-label" for="ctl0_CONTENU_PAGE_AdvancedSearch_orgNamesRestreinteSearch">
												<com:TTranslate>TEXT_MINISTERE</com:TTranslate>
												<span class="text-danger">*</span>
											</label>
											<div class="col-md-9">
												<com:TActiveDropDownList ID="orgNamesRestreinteSearch" OnCallBack="displayEntityPurchaseConsRestreinte"
																		 cssClass="form-control"></com:TActiveDropDownList>
											</div>
											<span id="erreurMinistere" style="display:none" title='<com:TTranslate>CHAMP_OBLIGATOIRE</com:TTranslate>' class='check-invalide text-danger'><i
														class="fa fa-times-circle-o p-r-1"></i></span>
										</div>
										<!--END ENTITE PUBLIQUE-->

										<com:TActivePanel ID="panelPurchaseNamesConsRestreinte">
											<!--BEGIN SERVICE-->
											<div id="divEntityPurchaseConsRestreinte" style="display:none" class="form-group form-group-sm">
												<label class="col-md-2 control-label" for="ctl0_CONTENU_PAGE_AdvancedSearch_entityPurchaseNamesConsRestreinte">
													<com:TTranslate>SERVICE_RECHERCHE_AVANCEE</com:TTranslate>
												</label>
												<div class="col-md-9">
													<com:TActiveDropDownList ID="entityPurchaseNamesConsRestreinte" cssClass="form-control"/>
												</div>
											</div>
											<!--END SERVICE-->
											<com:TLabel ID="scriptPurchaseNamesConsRestreinte"/>
											<p id="erreurListe" style="" title="<com:TTranslate>CHAMP_OBLIGATOIRE</com:TTranslate>" class="check-invalide text-danger"><i class="fa fa-times-circle-o p-r-1" aria-hidden="true"></i><span class="sr-only"><com:TTranslate>CHAMP_OBLIGATOIRE</com:TTranslate></span></p>
										</com:TActivePanel>
										<!--BEGIN REFERENCE-->
										<div class="form-group form-group-sm">
											<label class="col-md-2 control-label" for="ctl0_CONTENU_PAGE_AdvancedSearch_refRestreinteSearch">
												<com:TTranslate>TEXT_REFERENCE</com:TTranslate>
												<span class="text-danger">*</span>
											</label>
											<div class="col-md-9">
												<com:TTextBox ID="refRestreinteSearch" cssClass="form-control"/>
											</div>
											<p id="erreurReference" style="" title="<com:TTranslate>CHAMP_OBLIGATOIRE</com:TTranslate>" class="check-invalide text-danger"><i class="fa fa-times-circle-o p-r-1" aria-hidden="true"></i><span class="sr-only"><com:TTranslate>CHAMP_OBLIGATOIRE</com:TTranslate></span></p>
										</div>
										<!--BEGIN REFERENCE-->
										<!--BEGIN CODE D"ACCES-->
										<div class="form-group form-group-sm">
											<label class="col-md-2 control-label" for="ctl0_CONTENU_PAGE_AdvancedSearch_accesRestreinteSearch">
												<com:TTranslate>TEXT_CODE</com:TTranslate>
												<span class="text-danger">*</span>
											</label>
											<div class="col-md-9">
												<com:TTextBox ID="accesRestreinteSearch" MaxLength="15" cssClass="form-control"/>
											</div>
											<p id="erreurCode" style="" title="<com:TTranslate>CHAMP_OBLIGATOIRE</com:TTranslate>" class="check-invalide text-danger"><i class="fa fa-times-circle-o p-r-1" aria-hidden="true"></i><span class="sr-only"><com:TTranslate>CHAMP_OBLIGATOIRE</com:TTranslate></span></p>
										</div>
										<!--BEGIN CODE D"ACCES-->
									</div>
								</section>
							</div>
						</div>
						<div class="panel-footer">
							<div class="clearfix">
								<div class="pull-left">
									<button type="reset" class="btn btn-sm btn-default"
											value="<%=Prado::localize('DEFINE_EFFACER_CRITERE_RECHERCHE')%>"
											title="<%=Prado::localize('DEFINE_EFFACER_CRITERE_RECHERCHE')%>">
										<!--i class="fa fa-ban m-r-1"></i-->
										<%=Prado::localize('DEFINE_EFFACER_CRITERE_RECHERCHE')%>
									</button>
								</div>
								<div class="pull-right">
									<com:TButton CssClass="btn btn-sm btn-primary"
												 Text="<%=Prado::localize('DEFINE_LANCER_RECHERCHE')%>"
												 Attributes.title="<%=Prado::localize('DEFINE_LANCER_RECHERCHE')%>"
												 Attributes.onClick="return verifierDonneeRechercheRestreinte();"
												 OnClick="onSearchProcedureRest"/>
								</div>
							</div>
						</div>
					</com:TActivePanel>
				</div>
			</div>

		</div>

		<!--Debut Bloc Recherche Procedure restreinte-->
		<div id="rechercheRes"></div>

		<!--Debut Bloc Gestion des envois postaux compl?mentaires-->
		<com:TPanel ID="panelACCordsCadres" cssClass="form-field">
			<h2 class="title">
				<com:TTranslate>TEXT_ACCORDS_CADRES_ET_SYSTEMES_ACQUISITION</com:TTranslate>
			</h2>
			<div class="content">
				<p>
					<com:TTranslate>TEXT_ACCORDS_CADRES_ET_SYSTEMES_ACQUISITION_PASSER_MARCHE</com:TTranslate>
				</p>
				<div class="boutons-line">
					<a class="bouton-acceder-120 float-right" href="index.php?page=Agent.ListeConsultationsAcSad">
						<com:TTranslate>TEXT_ACCEDER</com:TTranslate>
					</a>
				</div>
			</div>
		</com:TPanel>
		<!--Fin Bloc Gestion des envois postaux complémentaires-->
		<!--Fin Bloc Recherche Procedure restreinte-->

		<!--Debut Lien de retour vers CAO -->
		<com:TPanel ID="panelCAORetour" cssClass="form-field" Display="None">
			<!--Fin Bloc Selection-->
			<div class="link-line">
				<a href="?page=Commission.OrdreDuJour&id=<%= Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($_GET['id'])%>" class="bouton-retour">
					<com:TTranslate>TEXT_RETOUR</com:TTranslate>
				</a>
			</div>
		</com:TPanel>
		<!--Fin Lien de retour vers CAO -->
		<com:TPanel cssClass="link-line" id="panelLienRetourMesRecherchesFavorites" visible="false">
			<com:THyperLink id="lienRetourMesRecherchesFavorites" CssClass="btn btn-sm btn-default" Attributes.title="<%=Prado::localize('TEXT_RETOUR')%>">
				<com:TTranslate>TEXT_RETOUR</com:TTranslate>
			</com:THyperLink>
		</com:TPanel>
	</div>
	<!--Fin Bloc Recherche avancee-->
</com:TPanel>

<!--Debut Modal Recherche-->
<div class="modal fade" id="modalSaveSearch" tabindex="-1" role="dialog" aria-labelledby="modalSaveSearch">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<div class="h4 modal-title" id="modalSaveSearchLabel"><%=Prado::localize('TEXT_SAUVEGARDER_RECHERCHE')%></div>
			</div>
			<div class="modal-body">
				<com:AtexoValidationSummary id="divValidation" ValidationGroup="validateName"/>
				<!--Debut Formulaire-->
				<div class="clearfix">
					<div class="form-horizontal">
						<div class="form-group form-group-sm clearfix line">
							<label for="ctl0_CONTENU_PAGE_AdvancedSearch_rechercheName" class="col-md-2">
								<com:TTranslate>NOM</com:TTranslate>
								<span class="text-danger" style="color:red">*</span>
							</label>
							<div class="col-md-10">
								<com:TActiveTextBox ID="rechercheName" cssClass="moyen"/>
							</div>
						</div>
						<div class="form-group form-group-sm clearfix">
							<div class="col-md-offset-2 col-md-10">
								<div>
									<label for="ctl0_CONTENU_PAGE_AdvancedSearch_sauvegardeRecherche">
										<com:TCheckBox id="sauvegardeRecherche"
													   Attributes.title="<%=Prado::Localize('DEFINE_RECHERCHE_SAUVEGARDEE')%>"/>
										<com:TTranslate>DEFINE_RECHERCHE_SAUVEGARDEE</com:TTranslate>
									</label>
								</div>
							</div>
							<div class="col-md-offset-2 col-md-10 line">
								<com:TPanel CssClass="checkbox" Visible="<%=($this->_calledFrom == 'entreprise')%>">
									<label for="ctl0_CONTENU_PAGE_AdvancedSearch_sauvegardeAlerte">
										<com:TCheckBox
												id="sauvegardeAlerte"
												Attributes.onclick="isCheckedShowDiv(this,'<%=$this->infoCompAlerte->getClientId()%>');"
												Attributes.title="<%=Prado::Localize('TEXT_ALERTE')%>"
												Visible="<%=($this->_calledFrom == 'entreprise')%>"/>
										<com:TTranslate>TEXT_ALERTE</com:TTranslate>
									</label>
								</com:TPanel>
							</div>
						</div>
						<div class="form-group form-group-sm m-b-0">
							<div class="col-md-offset-2 col-md-10">
								<com:TPanel ID="infoCompAlerte" style="display:none" Visible="<%=($this->_calledFrom == 'entreprise')%>">
									<div class="m-b-2">
										<label>
											<com:TTranslate>TEXT_RECEVOIR_ALERTE</com:TTranslate>
										</label>
										<div>
											<label class="radio-inline control-label" for="ctl0_CONTENU_PAGE_AdvancedSearch_tousLesJours">
												<com:TRadioButton GroupName="RadioGroup" id="tousLesJours"
																  Attributes.title="<%=Prado::localize('TEXT_TOUT_LES_JOURS')%>"/>
												<com:TTranslate>TEXT_TOUT_LES_JOURS</com:TTranslate>
											</label>
											<label class="radio-inline control-label" for="ctl0_CONTENU_PAGE_AdvancedSearch_toutesLesSemaines">
												<com:TRadioButton GroupName="RadioGroup" id="toutesLesSemaines"
																  Attributes.title="<%=Prado::localize('TEXT_TOUTES_SEMAINES')%>"/>
												<com:TTranslate>TEXT_TOUTES_SEMAINES</com:TTranslate>
											</label>
											<label class="radio-inline control-label" for="ctl0_CONTENU_PAGE_AdvancedSearch_desactiver">
												<com:TRadioButton GroupName="RadioGroup" id="desactiver"
																  Attributes.title="<%=Prado::localize('TEXT_DESACTIVER_TEMPORAIREMENT')%>"/>
												<com:TTranslate>TEXT_DESACTIVER_TEMPORAIREMENT</com:TTranslate>
											</label>
										</div>
									</div>
									<div class="m-b-2">
										<label>
											<com:TTranslate>RECEVOIR_L_ALERTE_AU_FORMAT</com:TTranslate>
											:
										</label>
										<div>
											<label class="radio-inline control-label" for="ctl0_CONTENU_PAGE_AdvancedSearch_formatHtml">
												<com:TRadioButton GroupName="formatAlerte" id="formatHtml"
																  Attributes.title="<%=Prado::localize('TEXT_FORMAT_HTML')%>"/>
												<%=Prado::localize('HTML_MAJUSCULE')%>
											</label>
											<label class="radio-inline control-label" for="ctl0_CONTENU_PAGE_AdvancedSearch_formatTexte">
												<com:TRadioButton GroupName="formatAlerte" id="formatTexte"
																  Attributes.title="<%=Prado::localize('TEXT_FORMAT_TEXTE')%>"/>
												<%=Prado::localize('DEFINE_TEXT_TEXT')%>
											</label>
										</div>
									</div>
								</com:TPanel>
							</div>
						</div>
						<com:TCustomValidator
								ControlToValidate="sauvegardeRecherche"
								ValidationGroup="validateName"
								Display="Dynamic"
								ID="sauvegardeValidator"
								EnableClientScript="false"
								onServerValidate="validerSauvegardeRecherche"
								Text="<span  title='<com:TTranslate>CHAMP_OBLIGATOIRE</com:TTranslate>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<com:TTranslate>CHAMP_OBLIGATOIRE</com:TTranslate>' /></span>">
						</com:TCustomValidator>
					</div>
				</div>
				<!--Fin Formulaire-->
			</div>
			<div class="modal-footer">
				<!--Debut line boutons-->
				<button type="button" class="pull-left" data-dismiss="modal"><%=Prado::localize('DEFINE_ANNULER')%></button>
				<com:TActiveButton
						id="saveRecherche"
						Text="<%=Prado::localize('TEXT_ENREGISTER')%>"
						CssClass="bouton-validation float-right"
						ValidationGroup="validateName"
						onCallBack="SaveAndSearch">
				</com:TActiveButton>
				<!--Fin line boutons-->
			</div>
		</div>
	</div>
</div>
<com:TActiveLabel id="scriptJs" style="display:none;"/>
<script>
    function displayInputAutocomplete() {
        var idDisplay = document.getElementById("ctl0_CONTENU_PAGE_AdvancedSearch_panelSearchAnnoncesMarches");

        if (idDisplay) {
			if (idDisplay.style.display === "none") {
				idDisplay.style.display = "block";
			}
        }
    }
</script>
<com:TActivePanel id="panelDefaultRestreinte" visible="false">
	<script>
		J('.limulticriteres').removeClass('active').attr("aria-expanded","false");
		J('#multicriteres').removeClass('active').removeClass('in');
		J('.lirestreinte').addClass('active').attr("aria-expanded","true");
		J('#restreinte').addClass('active').addClass('in');
	</script>
</com:TActivePanel>
<!--Fin colonne droite-->
