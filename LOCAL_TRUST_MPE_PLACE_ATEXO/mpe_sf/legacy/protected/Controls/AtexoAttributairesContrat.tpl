<!--Debut Bloc attributaires-->
<div class="form-field">
    <div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
    <div class="content">
        <h2 class="float-left"><com:TTranslate>ATTRIBUTAIRES</com:TTranslate></h2><com:TActiveImage id="ImgErrorAttributaire"  ImageUrl='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif'  Attributes.alt='Champ obligatoire' Style="display:none" />
        <!--Debut bloc Attributaire-->
        <div class="table-bloc">
            <com:TActiveRepeater ID="repeaterAttributaires">
                <prop:HeaderTemplate>
                <table class="table-results" summary="<%=Prado::Localize('LISTE_ATTRIBUTAIRE')%>">
                    <thead>
                    <tr>
                        <th colspan="4" class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></th>
                    </tr>
                    <tr>
                        <th scope="col" class="col-250"><div><com:TTranslate>TEXT_RAISON_SOCIALE</com:TTranslate></div></th>
                        <th scope="col" class="col-200"><com:TTranslate>CONTACT</com:TTranslate></th>
                        <th scope="col" class="col-200"><com:TTranslate>ADRESSE</com:TTranslate></th>
                        <th scope="col" class="actions"><com:TTranslate>DEFINE_ACTIONS</com:TTranslate></th>
                    </tr>
                    </thead>
                    <tbody>
                </prop:HeaderTemplate>
                <prop:ItemTemplate>
                    <tr class="<%#(($this->ItemIndex%2==0)? '':'on ')%>" >
                        <td class="col-250"><span class="blue"><com:TActiveLabel Text="<%#$this->Data->getNomEntreprise()%>" /></span> - <com:TActiveLabel Text="<%#$this->Data->getIdentifiantEntrepriseEtablissement()%>" /><br /></td>
                        <td class="col-200 col-break"><com:TActiveLabel Text="<%#$this->Data->getNomContact().' ' .$this->Data->getPrenomContact()%>" /><br />
                            <img alt="<%=Prado::Localize('ADRESSE_ELECTRONIQUE')%>" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-email.gif" />
                            <span class="ltr"><span><com:TActiveLabel  Text="<%#$this->Data->getEmailContact()%>" /></span></span><br />
                            <img alt="<%=Prado::Localize('DEFINE_TEL')%>" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-tel.gif" />
                            <span class="ltr"><span><com:TActiveLabel Text="<%#$this->Data->getTelephoneContact()%>" /></span></span><br />
                            <img alt="<%=Prado::Localize('DEFINE_FAX')%>" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-fax.gif" />
                            <span class="ltr"><span><com:TActiveLabel Text="<%#$this->Data->getFaxContact()%>" /></span></span>
                        </td>
                        <td class="col-200">
                            <com:TActiveLabel Text="<%#$this->Data->getAdresseEtablissement()%>" /><br /><com:TActiveLabel Text="<%#$this->Data->getCodePostalEtablissement()%>" /> <com:TActiveLabel Text="<%#$this->Data->getVilleEtablissement()%>" /><br /> <com:TActiveLabel Text="<%#$this->Data->getPaysEtablissement()%>" />
                        </td>
                        <td class="actions">
                            <com:TActiveLinkButton
                                    id="updateAttributaire"
                                    Attributes.title="<%=Prado::localize('BOUTON_MODIFIER')%>"
                                    OnCallBack="SourceTemplateControl.updateAttributaire"
                                    ActiveControl.CallbackParameter="<%#$this->Data->getIndex()%>"
                                    >
                                <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-edit.gif" alt="<%=Prado::localize('BOUTON_MODIFIER')%>" title="<%=Prado::localize('BOUTON_MODIFIER')%>"/>
                            </com:TActiveLinkButton>
                            <com:TActiveLinkButton
                                    id="deleteAttributaire"
                                    Attributes.title="<%=Prado::localize('DEFINE_SUPPRIMER')%>"
                                    Attributes.onClick="if(!confirm('<%=addslashes(Prado::localize('CONFIRMATION_SUPPRESSION_ATTRIBUTAIRE'))%>')) return false;"
                                    OnCallBack="SourceTemplateControl.deleteAttributaire"
                                    ActiveControl.CallbackParameter="<%#$this->Data->getIndex()%>"
                                    >
                                <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-supprimer-big.gif" alt="<%=Prado::localize('DEFINE_SUPPRIMER')%>" title="<%=Prado::localize('DEFINE_SUPPRIMER')%>"/>
                            </com:TActiveLinkButton>
                            <com:TActiveHiddenField ID="indexItemAttributaire" value="<%#$this->Data->getIndex()%>"/>
                        </td>
                    </tr>
                </prop:ItemTemplate>
                <prop:FooterTemplate>
                        </tbody>
                    </table>
                </prop:FooterTemplate>
            </com:TActiveRepeater>
        </div>

        <com:TActivePanel id="panelActionAjoutAttributaire" >
            <com:TActiveLinkButton
                    id="ajouterAttributaire"
                    cssClass="ajout-el"
                    Attributes.onclick="openModal('modal-attributaire','popup-moyen1',this);"
                    >
                <com:TTranslate>AJOUTER_UN_ATTRIBUTAIRE</com:TTranslate>
            </com:TActiveLinkButton>
            <!---->
        </com:TActivePanel>

    </div>
    <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
</div>
<com:TActiveLabel id="scriptLable" />
<com:THiddenField id="titrePopin" value="<%=Prado::Localize('AJOUTER_UN_ATTRIBUTAIRE')%>" />
<!--Fin Bloc attributaires-->
<com:TPanel id="panelAjoutNewAttributaire" cssClass="modal-attributaire" style="display:none;">
    <h1><com:TTranslate>AJOUTER_UN_ATTRIBUTAIRE</com:TTranslate></h1>
    <div class="spacer-small"></div>
    <com:AtexoValidationSummary id="divValidationSummaryAttributaire"  ValidationGroup="validateCreateAttributaire" />
    <com:AtexoValidationSummary id="divValidationSummarySGMAP"  ValidationGroup="validateSynchronisationSGMAP" />
    <com:AtexoValidationSummary id="divValidationSummaryAddAttributaire"  ValidationGroup="validateDonneesAttributaire" />
    <div class="form-bloc bloc-contrat margin-0">
        <div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
        <div class="content">
            <com:TActivePanel ID="panelInfoEntrepriseAttributaire">
                <div class="line">
                    <div class="intitule-auto">
                        <com:TActiveRadioButton
                                GroupName="attributaireEntreprise"
                                id="attributaireNationale"
                                Attributes.title="<%=Prado::localize('TEXT_ENTREPRISE_ETABLIE_EN_FRANCE')%>"
                                CssClass="radio"
                                Checked="true"
                                Attributes.onclick="isCheckedShowDiv(this,'etablieFrance');isCheckedHideDiv(this,'nonEtablieFrance');enabledCodePostalAttributaire('');" />
                        <label for="france"><com:TTranslate>TEXT_ENTREPRISE_ETABLIE_FRANCE</com:TTranslate> </label>
                    </div>
                    <div style="display: block;" class="clear-both" id="etablieFrance">
                        <div class="spacer"></div>
                        <div id="blocEntreprise_panelSiren">
                            <div class="intitule-150">
                                <div class="float-left">
                                    <label for="siren_siret"><com:TTranslate>SIREN_SIRET</com:TTranslate></label>
                                    <span class="champ-oblig">*</span>: </div>
                            </div>
                            <com:TActiveTextBox id="siren" Attributes.title="<%=Prado::localize('TEXT_SIREN')%>" MaxLength="9" CssClass="siren" />
                            <com:TActiveTextBox id="siret" Attributes.title="<%=Prado::localize('TEXT_SIRET')%>" MaxLength="5" CssClass="siret" />
                            <com:TCustomValidator
                                    ValidationGroup="validateSynchronisationSGMAP"
                                    ControlToValidate="siren"
                                    ErrorMessage="<%=Prado::localize('SIREN_SIRET')%>"
                                    Display="Dynamic"
                                    EnableClientScript="true"
                                    ClientValidationFunction="validationSiretTitulaire"
                                    enabled="<%= Application\Service\Atexo\Atexo_Config::getParameter('ACTIVER_SYNCHRONISATION_CREATION_ENREPRISE_AGENT') && Application\Service\Atexo\Atexo_Module::isEnabled('SynchronisationSGMAP')%>"
                                    Text="<span title='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' /></span>" >
                                <prop:ClientSide.OnValidationError>
                                    document.getElementById('divValidationSummarySGMAP').style.display='';
                                </prop:ClientSide.OnValidationError>
                            </com:TCustomValidator>
                            <com:TCustomValidator
                                    ValidationGroup="validateCreateAttributaire"
                                    ControlToValidate="siren"
                                    ErrorMessage="<%=Prado::localize('SIREN_SIRET')%>"
                                    Display="Dynamic"
                                    EnableClientScript="true"
                                    ClientValidationFunction="validationSiretAttributaireNationale"
                                    Text="<span title='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' /></span>" >
                                <prop:ClientSide.OnValidationError>
                                    document.getElementById('divValidationSummaryAttributaire').style.display='';
                                </prop:ClientSide.OnValidationError>
                            </com:TCustomValidator>
                            <com:TActiveLinkButton
                                    id="synchroniserAvecApiGouvEntreprise"
                                    visible="<%= Application\Service\Atexo\Atexo_Config::getParameter('ACTIVER_SYNCHRONISATION_CREATION_ENREPRISE_AGENT') && Application\Service\Atexo\Atexo_Module::isEnabled('SynchronisationSGMAP')%>"
                                    cssClass="btn-action dlh"
                                    ValidationGroup="validateSynchronisationSGMAP"
                                    onCallBack="synchroniserAvecApiGouvEntreprise"
                                    >
                                <img class="bottom-5-relative" alt="" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-webservice.png" />
                                <prop:ClientSide.OnLoading>showLoader();</prop:ClientSide.OnLoading>
                                <prop:ClientSide.OnFailure>showLoader();</prop:ClientSide.OnFailure>
                                <prop:ClientSide.OnSuccess>showLoader();</prop:ClientSide.OnSuccess>
                            </com:TActiveLinkButton>
                            <a class="picto-info-intitule" onblur="cacheBulle('infosSGMAP')" onfocus="afficheBulle('infosSGMAP', this)" onmouseout="cacheBulle('infosSGMAP')" onmouseover="afficheBulle('infosSGMAP', this)" href="#marchesPublicsSimplifies">
                                <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" />
                            </a>
                            <div id="infosSGMAP" class="info-bulle" onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();"><div><com:TTranslate>MESSAGE_DISPONIBILITE_INFOS_INSEE</com:TTranslate></div></div>

                        </div>
                    </div>
                    <com:TActiveHiddenField ID="compteEntrepriseIdUnique" Value="<%=Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseIdentifiantUnique') ? 1 : 0 %>" />
                </div>
                <div class="line">
                    <div class="intitule-auto">
                        <com:TActiveRadioButton
                                GroupName="attributaireEntreprise"
                                id="attributaireEtranger"
                                Attributes.title="<%=Prado::localize('TEXT_ENTREPRISE_ETRANGER')%>"
                                CssClass="radio"
                                Attributes.onclick="isCheckedShowDiv(this,'nonEtablieFrance');isCheckedHideDiv(this,'etablieFrance');enabledCodePostalAttributaire('disabled');" />
                        <label for="etranger"><com:TTranslate>TEXT_ENTREPRISE_ETRANGER</com:TTranslate></label>
                    </div>
                    <div style="display: none;" class="clear-both" id="nonEtablieFrance">
                        <div class="spacer"></div>
                        <div class="intitule-110"><label for="pays"><com:TTranslate>TEXT_COUNTRY</com:TTranslate></label><span class="champ-oblig">*</span>: </div>
                        <com:AtexoDropDownListCountries id="pays" TypeAffichage="withoutCode" Attributes.title="<%=Prado::localize('TEXT_PAYS')%>" CssClass="select-320 chosen-select liste-pays"/>
                        <com:TCustomValidator
                                ValidationGroup="validateCreateAttributaire"
                                ControlToValidate="pays"
                                ErrorMessage="<%=Prado::localize('TEXT_COUNTRY')%>"
                                Display="Dynamic"
                                EnableClientScript="true"
                                ClientValidationFunction="validationPaysAttributaireEtranger"
                                Text="<span title='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' /></span>" >
                            <prop:ClientSide.OnValidationError>
                                document.getElementById('divValidationSummaryAttributaire').style.display='';
                            </prop:ClientSide.OnValidationError>
                        </com:TCustomValidator>
                        <div class="breaker"></div>
                        <div class="intitule-110"><label for="idNational"><com:TTranslate>TEXT_IDENTIFIANT_NATIONNAL</com:TTranslate></label><span class="champ-oblig">*</span> : </div>
                        <div class="content-bloc">
                            <com:TActiveTextBox  id="idNational" Attributes.title="<%=Prado::localize('TEXT_IDENTIFIANT_NATIONNAL')%>" CssClass="input-185" />
                            <com:TCustomValidator
                                    ValidationGroup="validateCreateAttributaire"
                                    ControlToValidate="idNational"
                                    ErrorMessage="<%=Prado::localize('TEXT_IDENTIFIANT_NATIONNAL')%>"
                                    Display="Dynamic"
                                    EnableClientScript="true"
                                    ClientValidationFunction="validationIdNationalAttributaireEtranger"
                                    Text="<span title='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' /></span>" >
                                <prop:ClientSide.OnValidationError>
                                    document.getElementById('divValidationSummaryAttributaire').style.display='';
                                </prop:ClientSide.OnValidationError>
                            </com:TCustomValidator>
                            <div class="info-aide-clear"><com:TTranslate>TEXT_NUM_ENREGISTREMENT_NATIONAL</com:TTranslate></div>
                        </div>
                    </div>
                </div>
                <div class="spacer"></div>
                <div class="line">
                    <div class="intitule-150"><label for="nomEntreprise"><com:TTranslate>TEXT_RAISON_SOCIALE</com:TTranslate></label><span class="champ-oblig">* </span> :</div>
                    <div class="content-bloc content-bloc">
                        <com:TActiveTextBox ID="nomEntreprise" Attributes.title="<%=Prado::localize('TEXT_RAISON_SOCIALE')%>" CssClass="long" />
                        <com:TCustomValidator
                                ValidationGroup="validateCreateAttributaire"
                                ControlToValidate="nomEntreprise"
                                ErrorMessage="<%=Prado::localize('TEXT_RAISON_SOCIALE')%>"
                                Display="Dynamic"
                                EnableClientScript="true"
                                ClientValidationFunction="validationChampsAttributaire"
                                Text="<span title='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' /></span>" >
                            <prop:ClientSide.OnValidationError>
                                document.getElementById('divValidationSummaryAttributaire').style.display='';
                            </prop:ClientSide.OnValidationError>
                        </com:TCustomValidator>
                    </div>
                </div>
                <div class="line" id="panelCp">
                    <div class="intitule-150">
                        <label for="cp"><com:TTranslate>TEXT_CP</com:TTranslate></label>
                        <span class="champ-oblig">*</span>/<label for="ville"><com:TTranslate>TEXT_VILLE</com:TTranslate></label><span class="champ-oblig">*</span>:
                    </div>

                    <com:TActiveTextBox ID="cp" Attributes.title="<%=Prado::localize('TEXT_CP')%>" CssClass="cp-long" MaxLength="5" />
                    <com:TCustomValidator
                            ValidationGroup="validateCreateAttributaire"
                            ControlToValidate="cp"
                            ErrorMessage="<%=Prado::localize('TEXT_CP')%>"
                            Display="Dynamic"
                            EnableClientScript="true"
                            ClientValidationFunction="validationCPAttributaire"
                            Text="<span title='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' /></span>" >
                        <prop:ClientSide.OnValidationError>
                            document.getElementById('divValidationSummaryAttributaire').style.display='';
                        </prop:ClientSide.OnValidationError>
                    </com:TCustomValidator>
                    <com:TActiveTextBox ID="ville" Attributes.title="<%=Prado::localize('TEXT_VILLE')%>" CssClass="ville" />
                    <com:TCustomValidator
                            ValidationGroup="validateCreateAttributaire"
                            ControlToValidate="ville"
                            ErrorMessage="<%=Prado::localize('TEXT_VILLE')%>"
                            Display="Dynamic"
                            EnableClientScript="true"
                            ClientValidationFunction="validationChampsAttributaire"
                            Text="<span title='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' /></span>" >
                        <prop:ClientSide.OnValidationError>
                            document.getElementById('divValidationSummaryAttributaire').style.display='';
                        </prop:ClientSide.OnValidationError>
                    </com:TCustomValidator>
                </div>
                <div class="line">
                    <div class="intitule-150"><label for="categorieEntreprise"><com:TTranslate>DEFINE_CATEGORIE_ENTREPRISE_PME</com:TTranslate></label>:</div>
                    <div class="content-bloc content-bloc">
                        <com:TDropDownList ID="categorieEntrepriseListe" cssClass="select-185" />
                    </div>
                </div>

            </com:TActivePanel>

            <com:IdentificationEntreprise id="idIdentificationEntreprise"  responsive="2" />


            <div class="spacer"></div>
            <h2><com:TTranslate>CONTACT</com:TTranslate></h2>
            <div class="spacer-small"></div>
            <div class="bloc-50pourcent">
                <!-- -->
                <div class="line">
                    <div class="intitule-150">
                        <label for="nom"><com:TTranslate>NOM</com:TTranslate></label><span class="champ-oblig">* </span> :
                    </div>
                        <com:TActiveTextBox ID="nom" Attributes.title="<%=Prado::localize('NOM')%>" CssClass="input-185" />
                        <com:TRequiredFieldValidator
                                ControlToValidate="nom"
                                ValidationGroup="validateCreateAttributaire"
                                Display="Dynamic"
                                ErrorMessage="<%=Prado::localize('NOM')%>"
                                EnableClientScript="true"
                                Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
                            <prop:ClientSide.OnValidationError>
                                document.getElementById('divValidationSummaryAttributaire').style.display='';
                            </prop:ClientSide.OnValidationError>
                        </com:TRequiredFieldValidator>
                </div>
                <!-- -->
                <div class="line">
                    <div class="intitule-150">
                        <label for="prenom"><com:TTranslate>PRENOM</com:TTranslate></label><span class="champ-oblig">* </span> :
                    </div>
                    <com:TActiveTextBox ID="prenom" Attributes.title="<%=Prado::localize('PRENOM')%>" CssClass="input-185" />
                    <com:TRequiredFieldValidator
                            ControlToValidate="prenom"
                            ValidationGroup="validateCreateAttributaire"
                            Display="Dynamic"
                            ErrorMessage="<%=Prado::localize('PRENOM')%>"
                            EnableClientScript="true"
                            Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
                        <prop:ClientSide.OnValidationError>
                            document.getElementById('divValidationSummaryAttributaire').style.display='';
                        </prop:ClientSide.OnValidationError>
                    </com:TRequiredFieldValidator>
                </div>
                <!-- -->
                <div class="line">
                    <div class="intitule-150">
                        <label for="email"><com:TTranslate>ADRESSE_ELECTRONIQUE</com:TTranslate></label> <span class="champ-oblig">* </span> :
                    </div>
                    <com:TActiveTextBox ID="email" Attributes.title="<%=Prado::localize('ADRESSE_ELECTRONIQUE')%>" CssClass="input-185" />

                    <com:TCustomValidator
                            ValidationGroup="validateCreateAttributaire"
                            ControlToValidate="email"
                            ErrorMessage="<%=Prado::localize('ADRESSE_ELECTRONIQUE')%>"
                            Display="Dynamic"
                            EnableClientScript="true"
                            ClientValidationFunction="validatorEmail"
                            Text="<span title='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' /></span>" >
                        <prop:ClientSide.OnValidationError>
                            document.getElementById('divValidationSummaryAttributaire').style.display='';
                        </prop:ClientSide.OnValidationError>
                    </com:TCustomValidator>
                </div>
                <!-- -->
            </div>
            <div class="bloc-50pourcent">
                <div class="line">
                    <div class="intitule-150">
                        <label for="tel"><com:TTranslate>DEFINE_TEL</com:TTranslate></label> <span class="champ-oblig">* </span> :
                    </div>
                    <com:TActiveTextBox ID="tel" Attributes.title="<%=Prado::localize('DEFINE_TEL')%>" CssClass="input-185"/>
                    <com:TCustomValidator
                            ValidationGroup="validateCreateAttributaire"
                            ControlToValidate="tel"
                            ErrorMessage="<%=Prado::localize('DEFINE_TEL')%>"
                            Display="Dynamic"
                            EnableClientScript="true"
                            ClientValidationFunction="validerNombreCaracteresNumTelFaxObligatoire"
                            Text="<span title='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' /></span>" >
                        <prop:ClientSide.OnValidationError>
                            document.getElementById('divValidationSummaryAttributaire').style.display='';
                        </prop:ClientSide.OnValidationError>
                    </com:TCustomValidator>
                </div>
                <!-- -->
                <div class="line">
                    <div class="intitule-150">
                        <label for="fax"><com:TTranslate>TEXT_FAX</com:TTranslate></label> :
                    </div>
                    <com:TActiveTextBox ID="fax" Attributes.title="<%=Prado::localize('TEXT_FAX')%> " CssClass="input-185"/>
                    <com:TCustomValidator
                            ValidationGroup="validateCreateAttributaire"
                            ControlToValidate="fax"
                            ErrorMessage="<%=Prado::localize('TEXT_FAX')%>"
                            Display="Dynamic"
                            EnableClientScript="true"
                            ClientValidationFunction="validerNombreCaracteresNumTelFax"
                            Text="<span title='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' /></span>" >
                        <prop:ClientSide.OnValidationError>
                            document.getElementById('divValidationSummaryAttributaire').style.display='';
                        </prop:ClientSide.OnValidationError>
                    </com:TCustomValidator>
                </div>
                <!-- -->
            </div>
        </div>
        <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
    </div>
    <!--Fin bloc Attributaire-->
    <com:TActiveHiddenField ID="indexAttributaire" />
    <com:TActiveHiddenField ID="idEntreprise" />
    <com:TActiveHiddenField ID="idEtablissement" />
    <!--Debut line boutons-->
    <div class="boutons-line">
        <com:TActiveButton
                id="annulerAttributaire"
                Text = "<%=Prado::localize('DEFINE_ANNULER')%>"
                Attributes.title="<%=Prado::localize('DEFINE_ANNULER')%>"
                CssClass="bouton-moyen float-left"
                onCallBack="annulerAttributaire"
                >
            <prop:ClientSide.OnLoading>showLoader();</prop:ClientSide.OnLoading>
            <prop:ClientSide.OnFailure>hideLoader();</prop:ClientSide.OnFailure>
            <prop:ClientSide.OnSuccess>hideLoader();</prop:ClientSide.OnSuccess>
        </com:TActiveButton >

        <com:TActiveButton
                id="saveAttributaire"
                Text = "<%=Prado::localize('TEXT_VALIDER')%>"
                Attributes.title="<%=Prado::localize('TEXT_VALIDER')%>"
                CssClass="bouton-moyen float-right"
                ValidationGroup="validateCreateAttributaire"
                onCallBack="saveAttributaire"
                >
            <prop:ClientSide.OnLoading>showLoader();</prop:ClientSide.OnLoading>
            <prop:ClientSide.OnFailure>showLoader();</prop:ClientSide.OnFailure>
            <prop:ClientSide.OnSuccess>showLoader();</prop:ClientSide.OnSuccess>
        </com:TActiveButton >

        <com:TCustomValidator
                id="validateDonneesAttributaire"
                ValidationGroup="validateCreateAttributaire"
                ErrorMessage="test test test"
                ControlToValidate="idEntreprise"
                onServerValidate="validerDonneesAttributaireDuplicate"
                Display="Dynamic"
                Text=" "
                EnableClientScript="true">
        </com:TCustomValidator>

    </div>
    <!--Fin line boutons-->
</com:TPanel>
