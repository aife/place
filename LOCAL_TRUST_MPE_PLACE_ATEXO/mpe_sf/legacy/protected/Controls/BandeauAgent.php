<?php

namespace Application\Controls;

use Prado\Prado;
use App\Service\Header\Header;
use Application\Service\Atexo\Atexo_Config;
use App\Service\Bandeau\BandeauAgent as BandeauAgentSF;
use Application\Pages\Commun\Classes\MPEUser;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\MessageStatusCheckerUtil;

class BandeauAgent extends MpeTTemplateControl
{
    public const LIMIT_WIDTH_DISPLAY_NAME = 22;

    public function onInit($param)
    {
        MessageStatusCheckerUtil::initEntryPoints('agent-header-statistics');
    }

    public function render($writer)
    {
        /** @var BandeauAgentSF $menu */
        $menu = Atexo_Util::getSfService(BandeauAgentSF::class);

        echo $menu->render();
    }

    public function getNomPrenomUser(bool $bandeau = false)
    {
        $prenom = '';
        $nom = '';

        if ($this->User instanceof MPEUser) {
            $prenom = $this->User->getPrenom();
            $nom = $this->User->getNom();
        }

        $nameToDisplay = Atexo_Util::atexoHtmlEntities($prenom) . " " . Atexo_Util::atexoHtmlEntities($nom);

        return $bandeau
            ? mb_strimwidth($nameToDisplay, 0, self::LIMIT_WIDTH_DISPLAY_NAME, '...')
            : $nameToDisplay;
    }
}
