<?php

namespace Application\Controls;

use Application\Propel\Mpe\CommonCommission;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Commission\Atexo_Commission_Commissions;

/**
 * Affiche le résumé de la commission.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2012
 *
 * @since MPE4
 */
class CommissionSummary extends MpeTPage
{
    private $_dataSource = null;

    public function onLoad($param)
    {
        if ($this->getDataSource()) {
            $dataTypeCommission = Atexo_Commission_Commissions::remplirListeTypeCommission();
            $dataStatutCommission = Atexo_Commission_Commissions::remplirListeStatutCommission();
            $commission = $this->getDataSource();
            if ($commission instanceof CommonCommission) {
                $date = (Atexo_Util::iso2frnDateTime(Atexo_Util::atexoHtmlEntities($commission->getDate()), true)) ? Atexo_Util::iso2frnDateTime(Atexo_Util::atexoHtmlEntities($commission->getDate()), true) : '-';
                $statut = ($dataStatutCommission[$commission->getStatusCao()]) ? ($dataStatutCommission[$commission->getStatusCao()]) : '-';
                $type = ($dataTypeCommission[$commission->getType()]) ? ($dataTypeCommission[$commission->getType()]) : '-';
                $lieu = ($commission->getLieu()) ? Atexo_Util::atexoHtmlEntities($commission->getLieu()) : '-';
                $salle = ($commission->getSalle()) ? Atexo_Util::atexoHtmlEntities($commission->getSalle()) : '-';
                $this->dateHeure->Text = $date;
                $this->statutCommission->Text = $statut;
                $this->typeCommission->Text = $type;
                $this->lieuCommisssion->Text = $lieu;
                $this->salleCommisssion->Text = $salle;
            }
        }
    }

    public function getDataSource()
    {
        return $this->_dataSource;
    }

    public function setDataSource($dataSource)
    {
        $this->_dataSource = $dataSource;
    }
}
