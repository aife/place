<div class="spacer"></div>
				<h4><com:TTranslate>TEXT_PRESTATION_REALISEES_EN_COURS_REALISATION</com:TTranslate></h4>
				<div class="spacer-mini"></div>

				<div class="table-bloc">
					<table class="table-results" summary="<%=Prado::localize('TEXT_PRESTATION_REALISEES_EN_COURS_REALISATION')%>">
					  <com:TRepeater id="repeaterPrestations" DataKeyField="Id" EnableViewState="true">
				      <prop:HeaderTemplate>
						<caption><com:TTranslate>TEXT_PRESTATION_REALISEES_EN_COURS_REALISATION</com:TTranslate></caption>
						<thead>
							<tr>
								<th class="top" colspan="<%= ($this->Page->getPagePath()!='Entreprise.EntrepriseGestionCompteActivite') ?'3': '4' %>"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></th>
							</tr>
							<tr>
								<th class="col-360" id="objetMarche"><com:TTranslate>TEXT_OBJET_LIEU_EXECUTION</com:TTranslate><br/><com:TTranslate>TEXT_MARCHE_NUM</com:TTranslate><br/><com:TTranslate>TEXT_MODE_PASSATION</com:TTranslate></th>
								<th class="col-180" id="montantMarche"><com:TTranslate>TEXT_MAITRE_OUVRAGE</com:TTranslate><br /><com:TTranslate>TEXT_MONTANT_CURRENCY_HT</com:TTranslate></th>
								<th class="col-100" id="dateDebut"><com:TTranslate>TEXT_DATE_EXECUTION</com:TTranslate></th>
								<th class="actions" id="actions" style="display: <%= ($this->Page->getPagePath()!='Entreprise.EntrepriseGestionCompteActivite') ?'none': '' %>;"><com:TTranslate>DEFINE_ACTIONS</com:TTranslate></th>
							</tr>
						</thead>
						</prop:HeaderTemplate>
					
						<prop:ItemTemplate>
						<tr class="<%# ($this->ItemIndex%2==0) ? '' : 'on'%>">
							<td class="col-360" headers="objetMarche">
								<div class="objet-line"><strong><com:TTranslate>OBJET</com:TTranslate> : </strong><%#$this->Data->getObjet()%></div>
								<div class="spacer-mini"></div>
								<div class="objet-line"><strong><com:TTranslate>TEXT_MARCHE_NUM</com:TTranslate> : </strong><span class="ltr"><%#$this->Data->getNumMarche()%></span></div>
								<div class="spacer-mini"></div><div class="spacer-mini"></div><div class="objet-line"><strong><com:TTranslate>TEXT_MODE_PASSATION</com:TTranslate> : </strong><%#Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureType::getLibelleTypeProcedurePortailById($this->Data->getTypeProcedure())%></div>
							</td>
							<td class="col-180" headers="montantMarche"><%#$this->Data->getMaitreOuvrage()%><div class="spacer-mini"></div><span class="ltr"><%#  Application\Service\Atexo\Atexo_Util::getMontantArronditEspace($this->Data->getMontant(),2)%></span></td>
							<td class="col-100" headers="dateDebut"><com:TTranslate>DEFINE_DEBUT</com:TTranslate> : <span class="ltr"><%# Util::iso2frnDate($this->Data->getDateDebutExecution())%></span><br /><com:TTranslate>DEFINE_FIN</com:TTranslate> : <span class="ltr"><%# Util::iso2frnDate($this->Data->getDateFinExecution())%></span></td>
							<td class="actions" headers="modifier" style="display: <%= ($this->Page->getPagePath()!='Entreprise.EntrepriseGestionCompteActivite') ?'none': '' %>;">
									<com:TActiveImageButton visible="<%= ($this->Page->getPagePath()!='Entreprise.EntrepriseGestionCompteActivite') ?'false': 'true' %>" toolTip="<%=Prado::localize('DEFINE_TEXT_MODIFIER')%>"  Attributes.onClick="javascript:popUpSetSize('index.php?page=Entreprise.PopupAjoutPrestation&id=<%=$this->Data->getId()%>','850px','750px','yes');" ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-edit.gif" />
									<com:TActiveImageButton visible="<%= ($this->Page->getPagePath()!='Entreprise.EntrepriseGestionCompteActivite') ?'false': 'true' %>" toolTip="<%=Prado::localize('DEFINE_SUPPRIMER')%>" ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-supprimer-big.gif" Attributes.onclick="if(!confirm('<%=Prado::localize('TEXT_CONFIRMATION_SUPRESSION_PRESTATION')%>'))return false;" CommandName="Delete" onCommand="Page.deletePrestation" onCallBack="Page.refreshEntreprisePrestations" CommandParameter="<%#$this->Data->getId()%>"/>
					       </td>
						</tr>
					 </prop:ItemTemplate>	
				   </com:TRepeater>
				</table>	
					<com:THyperLink  visible="<%= ($this->Page->getPagePath()!='Entreprise.EntrepriseGestionCompteActivite') ?'false': 'true' %>" NavigateUrl="javascript:popUp('index.php?page=Entreprise.PopupAjoutPrestation','yes');" cssClass="ajout-el float-left"><com:TTranslate>TEXT_AJOUTER_PRESTATION</com:TTranslate></com:THyperLink>
				</div>
