<!--Debut message de confirmation-->
<com:TPanel visible="<%=($this->getTypeMessage() == 'msg-erreur')?true:false%>">
    <div class="alert alert-danger <%=$this->getTypeMessage()%>" ID="<%=$this->getId()%>" >
        <div class="content">
            <div class="<%=$this->getCssClass()%>" >
                <!--Debut Message type d'erreur general-->
                <span><com:TLabel ID="labelMessage"/></span>
                <!--Fin Message type d'erreur general-->
            </div>
            <div class="spacer-mini"></div>
        </div>
    </div>
</com:TPanel>
<com:TPanel visible="<%=($this->getTypeMessage() != 'msg-erreur')?true:false%>">
    <div class="alert alert-success <%=$this->getTypeMessage()%>">
        <div class="content">
            <div class="<%=$this->getCssClass()%>" >
                <!--Debut Message type d'erreur general-->
                <span><com:TLabel ID="labelMessageSuccess"/></span>
                <!--Fin Message type d'erreur general-->
            </div>
            <div class="spacer-mini"></div>
        </div>
    </div>
</com:TPanel>
<!--Fin  message de confirmation-->


