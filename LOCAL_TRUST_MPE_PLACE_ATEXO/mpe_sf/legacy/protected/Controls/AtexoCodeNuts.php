<?php

namespace Application\Controls;

class AtexoCodeNuts extends MpeTTemplateControl
{
    public $idComposant;
    public $arrayCodesNuts;

    public function getIdComposant()
    {
        return $this->idComposant;
    }

    public function setIdComposant($idComposant)
    {
        $this->idComposant = $idComposant;
    }

    public function setLibelleCodesNuts($libelleCodeNuts)
    {
        $this->arrayCodesNuts = $libelleCodeNuts;
    }

    public function truncateCodesNuts()
    {
        $truncatedText = '';
        $index = 0;
        foreach ($this->arrayCodesNuts as $oneCodeNut) {
            if (2 == $index) {
                break;
            }
            $truncatedText .= $oneCodeNut.'<br />';
            ++$index;
        }

        return $truncatedText;
    }

    public function isCodesNutsTruncated()
    {
        return ((is_countable($this->arrayCodesNuts) ? count($this->arrayCodesNuts) : 0) > 2) ? true : false;
    }

    public function getLibelleCodesNuts()
    {
        $libellesNuts = null;
        foreach ($this->arrayCodesNuts as $oneCodeNut) {
            $libellesNuts .= $oneCodeNut.'<br />';
        }

        return $libellesNuts;
    }
}
