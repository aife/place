<!--Debut Bloc formulaire-->
<com:TActivePanel id="bloc_etapeModalitesReponse">
	<div class="form-field">
		<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
		<div class="content">
			<h3 class="float-left"><com:TTranslate>DEFINE_MODALITE_DE_REPONSE</com:TTranslate></h3>
			<div class="float-right margin-fix"><com:TTranslate>TEXT_LE_SYMBOLE</com:TTranslate> <span class="champ-oblig">*</span> <com:TTranslate>TEXT_INDIQUE_CHAMPS_OBLIGATOIRES</com:TTranslate></div>
			<div class="breaker"></div>
			<div class="spacer"></div>

			<!--Debut Bloc Modalites de reponse-->
			<com:TActivePanel Cssclass="form-field" id="PanelModaliteReponse">
				<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><span class="title"><com:TTranslate>DEFINE_MODALITE_DE_REPONSE</com:TTranslate></span></div>
				<div class="content">

					<!--Debut Ligne Reponse electronique-->
					<com:TActivePanel Cssclass="line" id="panelLigneReponseElec">
						<div class="intitule-line"><com:TTranslate>DEFINE_REPONSE_ELECTRONIQUE</com:TTranslate> :</div>
						<div class="content-bloc">
							<com:TActivePanel id="panelrepRefusee" CssClass="line">
								<div class="intitule-auto">
									<com:TActiveRadioButton
										id="repRefusee"
										GroupName="repElectronique"
										Attributes.title="<%=Prado::localize('TEXT_REFUSE')%>"
										CssClass="radio"
										Attributes.onclick="hideShowBlocReponse(this,'ctl0_CONTENU_PAGE_bloc_etapeModalitesReponse_panelblocReponse','ctl0_CONTENU_PAGE_bloc_etapeModalitesReponse_panelModeChiffrement','ctl0_CONTENU_PAGE_bloc_etapeModalitesReponse_afficherTjrBlocCaracteristiqueReponse')"
									/>
									<label for="repRefusee">
										<com:TTranslate>TEXT_REFUSE</com:TTranslate>
									</label>
								</div>
							</com:TActivePanel>
							<com:TActivePanel id="panelrepAutorisee" CssClass="line">
								<div class="intitule-auto">
									<com:TActiveRadioButton
										id="repAutorisee"
										GroupName="repElectronique"
										Attributes.title="<%=Prado::localize('DEFINE_AUTORISEE')%>"
										CssClass="radio"
										Attributes.onclick="hideShowBlocReponse('ctl0_CONTENU_PAGE_bloc_etapeModalitesReponse_repRefusee','ctl0_CONTENU_PAGE_bloc_etapeModalitesReponse_panelblocReponse','ctl0_CONTENU_PAGE_bloc_etapeModalitesReponse_panelModeChiffrement','ctl0_CONTENU_PAGE_bloc_etapeModalitesReponse_afficherTjrBlocCaracteristiqueReponse')"
									/>
									<label for="repAutorisee">
										<com:TTranslate>DEFINE_AUTORISEE</com:TTranslate>
									</label>
								</div>
							</com:TActivePanel>
							<com:TActivePanel id="panelobligatoireOui" CssClass="line">
								<div class="intitule-auto">
									<com:TActiveRadioButton
										GroupName="repElectronique"
										id="obligatoireOui"
										Attributes.title="<%=Prado::localize('DEFINE_OBLIGATOIRE')%>"
										CssClass="radio"
										Attributes.onclick="hideShowBlocReponse('ctl0_CONTENU_PAGE_bloc_etapeModalitesReponse_repRefusee','ctl0_CONTENU_PAGE_bloc_etapeModalitesReponse_panelblocReponse','ctl0_CONTENU_PAGE_bloc_etapeModalitesReponse_panelModeChiffrement','ctl0_CONTENU_PAGE_bloc_etapeModalitesReponse_afficherTjrBlocCaracteristiqueReponse')"
									/>
									<label for="obligatoireOui"><com:TTranslate>DEFINE_OBLIGATOIRE</com:TTranslate></label>
								</div>
							</com:TActivePanel>

						</div>
						<div class="spacer-small"></div>
					</com:TActivePanel>
					<!--Fin Ligne Reponse electronique-->
					<!--Debut Ligne Acces a la consultation pour les entreprises-->

					<com:TActivePanel id="panelpoursuivreAffichage" CssClass="line">
						<div class="intitule-line"><com:TTranslate>DEFINE_ACCES_CONSULTATION_POUR_ENTREPRISE</com:TTranslate> :</div>
						<div class="content-bloc">
							<div class="line">

								<com:TActivePanel id="panelaccesConsultationEntrepriseDateLimite" CssClass="line">
									<div class="intitule-auto">
										<com:TActiveRadioButton
											id="accesConsultationEntrepriseDateLimite"
											Attributes.title="<%=Prado::localize('DEFINE_JUSQUA_DATE_LIMITE_REMISE_PLIS')%>"
											cssclass="radio"
											GroupName="accesConsEntreprise"
										/>
										<label for="accesConsultationEntrepriseDateLimite">
											<com:TTranslate>DEFINE_JUSQUA_DATE_LIMITE_REMISE_PLIS</com:TTranslate>
										</label>
									</div>
								</com:TActivePanel>

							</div>
							<div class="line">

								<com:TActivePanel id="panelaccesConsultationEntreprisePoursuivre" CssClass="line">
									<div class="intitule-auto">

										<com:TActiveRadioButton
											id="accesConsultationEntreprisePoursuivre"
											Attributes.title="<%=Prado::localize('DEFINE_POURSUIVRE_AFFICHAGE_ENTREPRISE')%>"
											cssclass="radio"
											GroupName="accesConsEntreprise"
										/>
										<label for="accesConsultationEntreprisePoursuivre">
											<com:TTranslate>DEFINE_POURSUIVRE_AFFICHAGE_ENTREPRISE</com:TTranslate>
										</label>

										<com:TActiveDropDownList
											id="poursuivreAffichageNbJour"
											Attributes.title="<%=Prado::localize('DEFINE_NOMBRE_JOURS')%>"
										/>
										<com:TTranslate>TEXT_APRES_DATE_LIMITE_PLIS</com:TTranslate>
									</div>
								</com:TActivePanel>
							</div>
						</div>
						<div class="spacer-small"></div>
					</com:TActivePanel>
					<com:TActivePanel id="panelblocReponse">
						<!--Debut Ligne Signature electronique requise :-->
						<com:TActivePanel Cssclass="line" id="PanelLigneSinatureElectronique">
									<div class="intitule-line"><com:TTranslate>DEFINE_SIGNATURE_ELECTRONIQUE_REQUISE</com:TTranslate></div>
									<div class="content-bloc">
										<com:TActivePanel id="panelsignatureElectroniqueNon" CssClass="line">
											<div class="intitule-auto">
												<com:TActiveRadioButton
														GroupName="signatureElectronique"
														id="signatureElectroniqueNon"
														Attributes.title="<%=Prado::localize('DEFINE_NON_REQUISE')%>"
														CssClass="radio"
												/>
												<label for="signatureElRequise_non">
													<com:TTranslate>DEFINE_NON_REQUISE</com:TTranslate>
													<i>(<com:TTranslate>DEFINE_AUCUNE_SIGNATURE_REQUISE_REP_SOUMISSIONNAIRE</com:TTranslate>)</i>
												</label>
											</div>
										</com:TActivePanel>
										<com:TActivePanel id="panelsignatureElectroniqueOui" CssClass="line">
											<div class="intitule-auto">
												<com:TActiveRadioButton GroupName="signatureElectronique" id="signatureElectroniqueOui" Attributes.title="<%=Prado::localize('DEFINE_OUI')%>" CssClass="radio" />
												<label for="signatureElRequise_oui">
													<com:TTranslate>DEFINE_REQUISE</com:TTranslate>
													<i>(<com:TTranslate>DEFINE_TEXT_SIGNATURE_REQUISE_REP_SOUMISSIONNAIRE</com:TTranslate>)</i>
												</label>
											</div>
										</com:TActivePanel>

										<com:TActivePanel id="panelsignatureElectroniqueAutoriser" CssClass="line">
											<div class="intitule-auto">
												<com:TActiveRadioButton
														GroupName="signatureElectronique"
														id="signatureElectroniqueAutoriser"
														Attributes.title="<%=Prado::localize('TEXT_AUTORISEE')%>"
														CssClass="radio"
												/>
												<label for="signatureElRequise_autoriser">
													<com:TTranslate>DEFINE_AUTORISEE</com:TTranslate>
													<i>(<com:TTranslate>DEFINE_TEXT_SIGNATURE_AUTORISEE</com:TTranslate>)</i>
												</label>
											</div>
										</com:TActivePanel>

									</div>
							<div class="spacer-small"></div>
						</com:TActivePanel>
					</com:TActivePanel>
					<div class="line">
					</div>
					<!--Fin Ligne Reponse refusee-->
					<div class="breaker"></div>
				</div>

			</com:TActivePanel>
			<!--Fin Ligne Signature electronique requise :-->

			<com:TActivePanel Cssclass="form-field" id="panelModaliteAgents">
					<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><span class="title"><com:TTranslate>DEFINE_MODALITE_OUVERTURE_AGENTS</com:TTranslate></span></div>
					<div class="content">
					<com:TActivePanel Cssclass="line" id="panelModeOuverture">
						<div class="intitule-line"><com:TTranslate>DEFINE_MODE_OUVERTURE</com:TTranslate> :
							<img title="Info-bulle"
								 alt="Info-bulle" class="picto-info"
								 onmouseout="cacheBulle('infosModeOuverture')"
								 onmouseover="afficheBulle('infosModeOuverture', this)"
								 src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" />
							<div id="infosModeOuverture"
								 class="info-bulle"
								 onmouseover="mouseOverBulle();"
								 onmouseout="mouseOutBulle();">
								<div><com:TTranslate>DEFINE_MODALITE_MODE_OUVERTURE_BULLE1</com:TTranslate></br></br>
									<com:TTranslate>DEFINE_MODALITE_MODE_OUVERTURE_BULLE2</com:TTranslate>
								</div>
							</div>
						</div>
						<div class="content-bloc">
							<com:TActivePanel Cssclass="line" id="panelmodeOuvertureParDossier">
								<div class="intitule-auto">
									<com:TActiveRadioButton
									GroupName="modeOuverture"
									id="modeOuvertureParDossier"
									Attributes.title="<%=Prado::localize('DEFINE_PAR_DOSSIER')%>"
									CssClass="radio"
									/>
									<label for="modeOuvertureParDossier">
									<%=Prado::localize('DEFINE_PAR_DOSSIER')%></label>
								</div>
							</com:TActivePanel>
							<com:TActivePanel Cssclass="line" id="panelmodeOuvertureParReponse">
								<div class="intitule-auto">
									<com:TActiveRadioButton
									GroupName="modeOuverture"
									id="modeOuvertureParReponse"
									Attributes.title="<%=Prado::localize('DEFINE_PAR_REPONSE')%>"
									CssClass="radio"
									/>
									<label for="modeOuvertureParReponse"><%=Prado::localize('DEFINE_PAR_REPONSE')%></label>
								</div>
							</com:TActivePanel>
						</div>
						<div class="spacer-small"></div>
					</com:TActivePanel>
					<!--Debut Ligne Chiffrement de la réponse-->
					<com:TActivePanel id="panelModeChiffrement">
						<com:TActivePanel Cssclass="line" id="panelLigneChiffrement">
							<div class="intitule-line"><com:TTranslate>DEFINE_CHIFFREMENT_REPONSE_AVEC_CLE_CRYPTOGRAPHIQUE</com:TTranslate> :</div>
							<div class="content-bloc">
								<com:TActivePanel id="panelchiffrementNon" CssClass="line">
									<div class="intitule-auto">
										<com:TActiveRadioButton
											GroupName="chiffrement"
											id="chiffrementNon"
											Attributes.title="<%=Prado::localize('DEFINE_DETAIL_CHIFFREMENT_NON')%>"
											CssClass="radio"
										/>
										<label for="chiffrementNon">
											<com:TTranslate>DEFINE_DETAIL_CHIFFREMENT_NON</com:TTranslate>
										</label>
									</div>
								</com:TActivePanel>
								<com:TActivePanel id="panelchiffrementOui" CssClass="line">
									<div class="intitule-auto">
										<com:TActiveRadioButton
											GroupName="chiffrement"
											id="chiffrementOui" Attributes.title="<%=Prado::localize('DEFINE_DETAIL_CHIFFREMENT_OUI')%>"
											CssClass="radio"
										/>
										<label for="chiffrementOui">
											<com:TTranslate>DEFINE_DETAIL_CHIFFREMENT_OUI</com:TTranslate></i>
										</label>

									</div>
								</com:TActivePanel>
								<div class="line" style="display:none;" >
									<div class="intitule-auto">
									<com:TActiveRadioButton
										GroupName="chiffrement"
										id="chiffrementOuiEnvUniq" Attributes.title="<%=Prado::localize('DEFINE_DETAIL_CHIFFREMENT_OUI')%>"
										CssClass="radio"
										Enabled="false"
									/>
									<label for="chiffrementOui">
										<com:TTranslate>DEFINE_DETAIL_CHIFFREMENT_OUI</com:TTranslate>
									</label>
									</div>
								</div>
							</div>
						</com:TActivePanel>
					</com:TActivePanel>
					<div class="line"></div>
					<div class="breaker"></div>
				</div>
				<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
			</com:TActivePanel>
			<!--Fin Ligne Chiffrement de la reponse-->
			<!--Fin Bloc Modalites de reponse-->
			<com:TActivePanel id="panelBlocDossierReponse">
			<!--Debut Bloc Constitution des dossiers de reponse-->
			<com:TActivePanel id="panelDossiersReponse" Cssclass="form-field">
						<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><span class="title"> <com:TTranslate>DEFINE_CONSTITUTION_DOSSIERS_REPONSES</com:TTranslate></span></div>
						<div class="content">

							<!-- Debut Legende -->
							<div class="toggle-panel" id="legende">
								<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
								<div class="content">
									<div class="title-toggle" onclick="togglePanel(this,'legendPanel');"><com:TTranslate>LEGENDE</com:TTranslate></div>
									<div class="panel" style="display:none;" id="legendPanel">
										<div class="column-type-piece">
											<h2><com:TTranslate>DEFINE_TYPE_PIECE</com:TTranslate></h2>
											<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-piece-typee.png" alt=""  />
											<div class="intitule-legende"><strong><com:TTranslate>DEFINE_PIECE_TYPEE</com:TTranslate></strong> : <com:TTranslate>DEFINE_NATURE_PIECE_EXPLICITE_FORMULAIRE_REPONSE_SOUMISSIONNAIRE</com:TTranslate></div>
											<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-type-libre.png" alt=""  />
											<div class="intitule-legende"><strong><com:TTranslate>PIECE_LIBRE</com:TTranslate></strong> : <com:TTranslate>DEFINE_SOUMISSIONNAIRE_JOINT_PLUSIEURS_FICHIERS_CHOIX</com:TTranslate></div>
											<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-type-form.png" alt=""  />
											<div class="intitule-legende"><strong><com:TTranslate>DEFINE_TEXT_FORMULAIRE</com:TTranslate></strong> : <com:TTranslate>DEFINE_SOUMISSIONNAIRE_SAISIT_DIRECTEMENT_PF</com:TTranslate></div>
											<div class="spacer-small"></div>
										</div>
									</div>
									<div class="breaker"></div>
								</div>
								<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
							</div>
							<!-- Fin Legende -->

							<!--Debut Pieces Dossier de candidature-->
							<com:TActivePanel id="panelenvelopCandidature" cssclass="form-field bloc-pieces">
								<div class="top"><span class="left">&nbsp;</span>
									<div class="title">
										<com:TActiveCheckBox id="envelopCandidature" Attributes.title="<%=Prado::localize('DEFINE_ENVELOPPE_CONDIDATURE')%>" CssClass="check" Attributes.onclick="toggleMultiPanel('panel_dossier_candidature','panel_grilleEval_candidature');"/>
										<label for="envelopCandidature"><com:TTranslate>DEFINE_ENVELOPPE_CONDIDATURE</com:TTranslate></label>
									</div>
									<span class="right">&nbsp;</span>
								</div>
								<div class="content" id="panel_dossier_candidature">
									<table summary="Liste des pieces - Dossier de candidature" class="table-results">
										<thead>
											<tr>
												<th class="top" colspan="3"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></th>
											</tr>
											<tr>
												<th class="check-col" id="dossierCand_col_01"><com:TTranslate>TEXT_DEFINE_SELECTIONNER</com:TTranslate></th>
												<th class="col-80 center" id="dossierCand_col_02"><com:TTranslate>TEXT_TYPE</com:TTranslate></th>
												<th id="dossierCand_col_03"><com:TTranslate>DEFINE_INTITULE_DOCUMENT</com:TTranslate></th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td class="check-col" headers="dossierCand_col_01"><input checked="true" disabled="disabled" name="dossierCand_piece_03" id="dossierCand_piece_03" title="Sélectionnez cette pièce" type="checkbox" /></td>
												<td class="col-80 center" headers="dossierCand_col_02"><img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-type-libre.png" alt="Libre" title="Libre" /></td>
												<td headers="dossierCand_col_03"><com:TTranslate>DEFINE_AUTANT_PIECES_SOUHAITEE_SOUMISSIONNAIRE</com:TTranslate></td>
											</tr>
										</tbody>
									</table>
									<div class="breaker"></div>
								</div>
							</com:TActivePanel>
							<!--Fin Pieces Dossier de candidature-->

							<com:TActivePanel id="spacerenvelopOffreTechnique" Cssclass="spacer" visible="false"></com:TActivePanel>

							<!--Debut Pieces Dossier d'offre technique-->
							<com:TActivePanel id="panelenvelopOffreTechnique" cssclass="form-field bloc-pieces" visible="false">
								<div class="top">
									<span class="left">&nbsp;</span>
									<div class="title">
										<com:TActiveCheckBox id="envelopOffreTechnique" Attributes.onclick="toggleMultiPanel('panel_dossier_offreTech','panel_grilleEval_offreTech');" Attributes.title="<%=Prado::localize('DEFINE_ENVELOPPE_OFFRE_TECHNIQUE')%>" CssClass="check" />
										<label for="envelopOffreTechnique"><com:TTranslate>DEFINE_ENVELOPPE_OFFRE_TECHNIQUE</com:TTranslate></label>
									</div>
									<span class="right">&nbsp;</span>
								</div>
								<div class="content" id="panel_dossier_offreTech">

									<table summary="Liste des pièces - Dossier d'offre technique" class="table-results">
										<thead>
											<tr>
												<th class="top" colspan="3"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></th>
											</tr>
											<tr>
												<th class="check-col" id="dossierOffreTech_col_01"><com:TTranslate>TEXT_DEFINE_SELECTIONNER</com:TTranslate></th>
												<th class="col-80 center" id="dossierOffreTech_col_02"><com:TTranslate>TEXT_TYPE</com:TTranslate></th>
												<th id="dossierOffreTech_col_03"><com:TTranslate>DEFINE_INTITULE_DOCUMENT</com:TTranslate></th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td class="check-col" headers="dossierOffreTech_col_01"><input checked="true" disabled="disabled" name="dossierOffreTech_piece_01" id="dossierOffreTech_piece_01" title="Sélectionnez cette pièce" type="checkbox" /></td>
												<td class="col-80 center" headers="dossierOffreTech_col_02"><img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-type-libre.png" alt="Libre" title="Libre" /></td>
												<td headers="dossierOffreTech_col_03"><com:TTranslate>DEFINE_AUTANT_PIECES_SOUHAITEE_SOUMISSIONNAIRE</com:TTranslate></td>
											</tr>
										</tbody>
									</table>
									<div class="breaker"></div>
								</div>
							</com:TActivePanel>
							<!--Fin Pieces Dossier d'offre technique-->

							<com:TActivePanel id="spacerenvelopOffre" Cssclass="spacer"></com:TActivePanel>

							<!--Debut Pieces Dossier d'offre-->
							<com:TActivePanel id="panelenvelopOffre" cssclass="form-field bloc-pieces">
								<div class="top">
									<span class="left">&nbsp;</span>
									<div class="title">
										<com:TActiveCheckBox id="envelopOffre" Attributes.title="<%=Prado::localize('DEFINE_ENVELOPPE_OFFRE')%>" CssClass="check" Attributes.onclick="toggleMultiPanel('panel_dossier_offre','panel_grilleEval_offre');"/>
										<label for="envelopOffre"><com:TTranslate>DEFINE_ENVELOPPE_OFFRE</com:TTranslate></label>
									</div>
									<span class="right">&nbsp;</span>
								</div>
								<div class="content" id="panel_dossier_offre">

									<table summary="Liste des piéces - Dossier d'offre" class="table-results">
										<thead>
											<tr>
												<th class="top" colspan="3"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></th>
											</tr>
											<tr>
												<th class="check-col" id="dossierOffre_col_01"><com:TTranslate>TEXT_DEFINE_SELECTIONNER</com:TTranslate></th>
												<th class="col-80 center" id="dossierOffre_col_02"><com:TTranslate>TEXT_TYPE</com:TTranslate></th>
												<th id="dossierOffre_col_03"><com:TTranslate>DEFINE_INTITULE_DOCUMENT</com:TTranslate></th>
											</tr>
										</thead>
										<tbody>
											<tr style="<%=$this->isActeEngagementVisible()%>">
												<td class="check-col" headers="dossierOffre_col_01">
													<com:TActivePanel id="panelsignaturePropre">
														<com:TActiveCheckBox id="signaturePropre" Attributes.title="Sélectionnez cette pièce" CssClass="radio" />
													</com:TActivePanel>
												</td>
												<td class="col-80 center" headers="dossierOffre_col_02"><img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-piece-typee.png" alt="<%=Prado::localize('DEFINE_PIECE_TYPEE')%>" title="<%=Prado::localize('DEFINE_PIECE_TYPEE')%>" /></td>
												<td headers="dossierOffre_col_03"><com:TTranslate>AE</com:TTranslate></td>
											</tr>
											<tr style="<%=$this->isAnnexeFinanciereVisible()%>">
												<td class="check-col" headers="dossierOffre_col_01">
													<com:TActivePanel id="panelannexeFinanciere">
														<com:TActiveCheckBox id="annexeFinanciere" Attributes.title="<com::TTranslate>SELECTIONNER_CETTE_PIECE</com::TTranslate>" CssClass="radio" />
													</com:TActivePanel>
												</td>
												<td class="col-80 center" headers="dossierOffre_col_02"><img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-piece-typee-AF.png" alt="<%=Prado::localize('DEFINE_PIECE_TYPEE')%>" title="<%=Prado::localize('DEFINE_PIECE_TYPEE')%>" /></td>
												<td headers="dossierOffre_col_03"><com:TTranslate>ANNEXE_FINANCIERE</com:TTranslate></td>
											</tr>
											<tr class="on">
												<td class="check-col" headers="dossierOffre_col_01"><input checked="true" disabled="disabled" name="dossierOffre_piece_03" id="dossierOffre_piece_03" title="Sélectionnez cette pièce" type="checkbox" /></td>
												<td class="col-80 center" headers="dossierOffre_col_02"><img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-type-libre.png" alt="Libre" title="Libre" /></td>
												<td headers="dossierOffre_col_03"><com:TTranslate>DEFINE_AUTANT_PIECES_SOUHAITEE_SOUMISSIONNAIRE</com:TTranslate></td>
											</tr>
										</tbody>
									</table>
									<div class="breaker"></div>
								</div>
							</com:TActivePanel>
							<!--Fin Pieces Dossier d'offre-->

							<com:TActivePanel id="spacerenvelopAnonymat" Cssclass="spacer"></com:TActivePanel>
							<!--Debut Pieces Dossier d'anonymat-->
							<com:TActivePanel id="panelanonymat" cssclass="form-field bloc-pieces">
								<div class="top">
									<span class="left">&nbsp;</span>
									<div class="title">
										<com:TActiveCheckBox id="anonymat" Attributes.title="<%=Prado::localize('DEFINE_TROISIEME_ENVELOPPE')%>" CssClass="check" Enabled="true" Attributes.onclick="toggleMultiPanel('panel_dossier_anonymat','panel_grilleEval_anonymat');"/>
										<label for="anonymat"><com:TTranslate>DEFINE_TROISIEME_ENVELOPPE</com:TTranslate></label>
									</div>
									<span class="right">&nbsp;</span>
								</div>
								<div class="content" id="panel_dossier_anonymat">
									<table summary="Liste des pièces - Dossier de d'anonymat" class="table-results">
										<thead>
											<tr>
												<th class="top" colspan="3"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></th>
											</tr>
											<tr>
												<th class="check-col" id="dossierAnonymat_col_01"><com:TTranslate>TEXT_DEFINE_SELECTIONNER</com:TTranslate></th>
												<th class="col-80 center" id="dossierAnonymat_col_02"><com:TTranslate>TEXT_TYPE</com:TTranslate></th>
												<th id="dossierAnonymat_col_03"><com:TTranslate>DEFINE_INTITULE_DOCUMENT</com:TTranslate></th>
											</tr>
										</thead>
										<tbody>
											<tr>
												<td class="check-col" headers="dossierAnonymat_col_01"><input checked="true" disabled="disabled" name="dossierAnonymat_piece_01" id="dossierAnonymat_piece_01" title="Sélectionnez cette pièce" type="checkbox" /></td>
												<td class="col-80 center" headers="dossierAnonymat_col_02"><img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-type-libre.png" alt="Libre" title="Libre" /></td>
												<td headers="dossierAnonymat_col_03"><com:TTranslate>DEFINE_AUTANT_PIECES_SOUHAITEE_SOUMISSIONNAIRE</com:TTranslate></td>
											</tr>
										</tbody>
									</table>
									<div class="breaker"></div>
								</div>
							</com:TActivePanel>
							<!--Fin Pieces Dossier d'anonymat-->

							<div class="breaker"></div>
						</div>
						<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
					</com:TActivePanel>
			<!--Fin Bloc Constitution des dossiers de reponse-->

			<!--Debut Bloc Criteres d'evaluation-->
								<com:TActivePanel cssClass="form-field" id="criteresEval" Visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('GestionModelesFormulaire'))? true:false%>">
									<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><span class="title"><com:TTranslate>TEXT_CRITERES_EVALUATION</com:TTranslate></span></div>
									<div class="content">
										<div class="bloc-non-alloti">
											<div class="spacer-mini"></div>
											<!--Debut Ligne Enveloppe de candidature-->
											<com:TActivePanel cssClass="line" id="panelEnvelopCandCriteres" style="display: none;">
												<div class="intitule-auto"><com:TTranslate>CANDIDATURE</com:TTranslate></div>
												<div class="breaker"></div>
												<div class="content-bloc bloc-600 indent-20">

													<!--Debut Ligne Grille d'evaluation-->
													<div class="intitule-auto">
														<com:TActiveCheckBox id="envelopCandGrilleEval" cssClass="radio" />
														<label for="envelopCandGrilleEval"> <com:TTranslate>TEXT_GRILLE_EVALUATION</com:TTranslate></label>
													</div>
													<!--Fin Ligne Grille d'evaluation-->
													<div class="spacer-mini"></div>
												</div>
											</com:TActivePanel>
											<!--Fin Ligne Enveloppe de candidature-->

											<!--Debut Ligne Enveloppe d'offre technique-->
											<com:TActivePanel cssClass="line" id="panelEnvelopTechCriteres" style="display: none;">
												<div class="intitule-auto"><com:TTranslate>OFFRE_TECHNIQUE</com:TTranslate></div>
												<div class="breaker"></div>
												<div class="content-bloc bloc-600 indent-20">
													<!--Debut Ligne Grille d'evaluation-->
													<div class="intitule-auto">
														<com:TActiveCheckBox id="envelopTechGrilleEval" cssClass="radio"/>
														<label for="envelopTechGrilleEval"> <com:TTranslate>TEXT_GRILLE_EVALUATION</com:TTranslate></label>
													</div>
													<!--Fin Ligne Grille d'evaluation-->
													<div class="spacer-mini"></div>
												</div>
											</com:TActivePanel>
											<!--Fin Ligne Enveloppe d'offre technique-->

											<!--Debut Ligne Enveloppe d'offre financiere-->
											<com:TActivePanel cssClass="line" id="panelEnvelopFinCriteres" style="display: none;">
												<div class="intitule-auto"><com:TTranslate>OFFRE</com:TTranslate></div>
												<div class="breaker"></div>
												<div class="content-bloc bloc-600 indent-20">
													<!--Debut Ligne Grille d'evaluation-->
													<div class="intitule-auto">
														<com:TActiveCheckBox id="envelopFinGrilleEval" cssClass="radio" />
														<label for="envelopFinGrilleEval"> <com:TTranslate>TEXT_GRILLE_EVALUATION</com:TTranslate></label>
													</div>
													<!--Fin Ligne Grille d'evaluation-->
													<div class="breaker"></div>
												</div>
											</com:TActivePanel>
											<!--Fin Ligne Enveloppe d'offre financiere-->

											<!--Debut Ligne Troisieme enveloppe-->
											<com:TActivePanel cssClass="line" id="panelEnv3Criteres" style="display: none;">
												<div class="intitule-auto"><com:TTranslate>DEFINE_TROISIEME_ENVELOPPE</com:TTranslate></div>
												<div class="breaker"></div>
												<div class="content-bloc bloc-600 indent-20">
													<!--Debut Ligne Grille d'evaluation-->
													<div class="intitule-auto">
														<com:TActiveCheckBox id="env3GrilleEval" cssClass="radio"/>
														<label for="env3GrilleEval"> <com:TTranslate>TEXT_GRILLE_EVALUATION</com:TTranslate></label>
													</div>
													<!--Fin Ligne Grille d'evaluation-->
													<div class="breaker"></div>
												</div>
											</com:TActivePanel>
											<!--Fin Ligne Troisieme enveloppe-->

										</div>
										<div class="breaker"></div>
									</div>
									<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
								</com:TActivePanel>
								<!--Fin Bloc Criteres d'evaluation-->
			</com:TActivePanel>
			<div class="breaker"></div>
		</div>
	</div>
	<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
	<com:THiddenField id="afficherTjrBlocCaracteristiqueReponse"/>
    <com:TCustomValidator
            ValidationGroup="validateInfosConsultation"
            ControlToValidate="afficherTjrBlocCaracteristiqueReponse"
            ClientValidationFunction="checkModalitesReponse"
            Display="Dynamic"
            ErrorMessage="<%=Prado::localize('DEFINE_CONSTITUTION_DOSSIERS_REPONSES')%>"
            text=" "
            EnableClientScript="true" >
        <prop:ClientSide.OnValidationError>
            document.getElementById('divValidationSummary').style.display='';
            document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
            document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
            document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
            document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
        </prop:ClientSide.OnValidationError>
    </com:TCustomValidator>
</com:TActivePanel>
<!--Fin Bloc formulaire-->