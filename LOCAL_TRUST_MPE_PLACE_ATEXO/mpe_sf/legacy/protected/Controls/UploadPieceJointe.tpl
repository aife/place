<!--Debut Bloc Echange-->
<div class="form-field">
<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
<div class="content">		

<!--Debut Bloc Infos echange-->
<div class="form-bloc">
	<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
  <div class="content">
		<div class="line">
			<div class="intitule-100 bold">
				<com:TTranslate>DEFINE_TEXT_PJ</com:TTranslate> :
			</div>
				<com:TActivePanel ID="panelListeFichiers" CssClass="content-bloc bloc-600">
					<com:TRepeater ID="RepeaterNomsFichiers" EnableViewState="true">
						<prop:ItemTemplate>
							<div class="picto-link inline">
								<com:TLinkButton ID="download" OnCommand="Page.IdUploadPj.downloadBlob"  CommandName="<%#$this->Data['id']%>"><%#$this->Data['name']%></com:TLinkButton>
								<com:TActiveLinkButton ID="delete" CssClass="indent-5" OnCallBack="Page.IdUploadPj.refreshRepeater" OnCommand="Page.IdUploadPj.onDeleteClick" CommandName="<%#$this->Data['id']%>">
									<com:PictoDeleteImage />
								</com:TActiveLinkButton>
							</div>
							<br />
						</prop:ItemTemplate>
					</com:TRepeater>
				</com:TActivePanel>	
		</div>
		
		<div class="breaker"></div>
	</div>
	<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
</div>
<!--Fin Bloc Infos echange-->
<!--Debut Bloc message erreur-->
<com:PanelMessageErreur id="panelMessageErreur" visible="false"/>
<!--Fin Bloc message erreur-->
<!--Debut Bloc Ajout piece jointe-->
<div class="form-bloc">
	<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
  <div class="content">
		<h2><com:TTranslate>DEFINE_TEXT_AJOUT_PJ</com:TTranslate></h2>
		<div class="spacer-small"></div>
		<div class="line">
			<div class="intitule-100 bold"><label for="<%=$this->ajoutFichier->getClientId()%>"><com:TTranslate>DEFINE_TEXT_FICHIER</com:TTranslate></label> :</div>
			<com:AtexoFileUpload 
				ID="ajoutFichier" 
				Attributes.title="<%=Prado::localize('OBJET')%>"
				Attributes.CssClass="file-580 float-left" 
				Attributes.size="98" 
				/>
				<com:TLinkButton
					OnClick="onAjoutClick"
					><com:TImage ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-ajout-element.gif"
					AlternateText="<%=Prado::localize('DEFINE_TEXT_AJOUT_COMME_PJ')%>" 
					Attributes.title="<%=Prado::localize('DEFINE_TEXT_AJOUT_COMME_PJ')%>" /></com:TLinkButton>
		</div>
	</div>
	<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
</div>
<!--Fin Bloc Ajout piece jointe-->

<div class="breaker"></div>
</div>
<div class="breaker"></div>
<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
</div>
<!--Fin Bloc Echange-->
<!--Debut line boutons-->
	<div class="boutons-line">
		<com:TButton  
				CssClass="bouton-moyen float-left" 
				Attributes.value="<%=Prado::localize('TEXT_ANNULER')%>" 
				Attributes.title="<%=Prado::localize('TEXT_ANNULER')%>" 
				Attributes.onclick = "window.close();"/>
		<com:TLinkButton  
				CssClass="bouton-moyen float-right" 
				Attributes.value="<%=Prado::localize('TEXT_VALIDER')%>" 
				Attributes.title="<%=Prado::localize('DEFINE_VALIDER_SELECTION')%>" 
				Text="Valider"
				onClick="OnValidClick"/>	
	</div>
	<!--Fin line boutons-->