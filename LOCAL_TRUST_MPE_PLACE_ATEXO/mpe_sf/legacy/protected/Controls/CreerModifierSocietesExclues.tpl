<!--Debut Bloc Identification de la societe a exclure--> 
		<div class="form-field" style="display:block;"> 
			<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div> 
			<div class="content"> 
				<h3 class="float-left"><com:TTranslate>TEXT_IDENTIFIANT_SOCIETE</com:TTranslate></h3><div class="float-right margin-fix"><com:TTranslate>DEFINE_SYMBOLE</com:TTranslate> <span class="champ-oblig">*</span> <com:TTranslate>TEXT_INDIQUE_CHAMPS_OBLIGATOIRES</com:TTranslate></div> 
				<div class="spacer-mini"></div> 
				<!--Debut Ligne Document--> 
				<div class="line"> 
					<div class="intitule-150"><label for="document"><com:TTranslate>TEXT_DOCUMENT</com:TTranslate></label><span class="champ-oblig">*</span> :</div> 
					<com:AtexoFileUpload Attributes.type="<%=Prado::localize('TEXT_FILE')%>" id="document_attacher" Attributes.title="<%=Prado::localize('TEXT_DOCUMENT')%>" CssClass="file-580" Attributes.size="96" />
					<com:TCustomValidator
        				ControlToValidate="document_attacher"
        				ValidationGroup="infosSocietesExclues" 
        				Display="Dynamic"
        				ID="validateurDocAttacheSocieteExclues" 
        				ClientValidationFunction="validerDocAttacheSocieteExclues"
        				ErrorMessage="<%=Prado::localize('TEXT_DOCUMENT')%>"
    					EnableClientScript="true"  						 					 
    					Text="<span title='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' /></span>"
    				>   
    					<prop:ClientSide.OnValidationError>
               				 document.getElementById('divValidationSummary').style.display='';
             			</prop:ClientSide.OnValidationError>
             		</com:TCustomValidator> 
				</div> 
				
				<!--Fin Ligne Document--> 
				<div class="spacer-small"></div> 
				<!--Debut Ligne Registre du commerce--> 
				<div class="line"> 
					<div class="intitule-150"><label for="registreCommerce"><com:TTranslate>REGISTRE_DU_COMMERCE</com:TTranslate></label><span class="champ-oblig">*</span> :</div> 
					<com:TDropDownList 
    					id="registreCommerce" 
    					Attributes.title="<%=Prado::localize('DEFINE_VILLE_RC')%>"
    					CssClass="bloc-190"> 
					</com:TDropDownList> 
					/ 
					<label for="registreCommerce_numero" style="display:none;"><com:TTranslate>DEFINE_NUMERO</com:TTranslate></label> 
					<com:TTextBox id="registreCommerce_numero" CssClass="input-50" Attributes.title="<%=Prado::localize('DEFINE_NUMERO')%>" />
					<div class="info-aide-right"><com:TTranslate>TEXT_VILLE_NUMERO</com:TTranslate></div>
					<com:TCustomValidator
        				ControlToValidate="registreCommerce_numero"
        				ValidationGroup="infosSocietesExclues" 
        				Display="Dynamic"
        				ID="validateurIdentifiantEntrepriseSocieteExclues" 
        				ClientValidationFunction="validerIdentifiantEntrepriseSocieteExclues"
        				ErrorMessage="<%=Prado::localize('REGISTRE_DU_COMMERCE')%>"
    					EnableClientScript="true"  						 					 
    					Text="<span title='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' /></span>"
    				>   
    					<prop:ClientSide.OnValidationError>
               				 document.getElementById('divValidationSummary').style.display='';
             			</prop:ClientSide.OnValidationError>
             		</com:TCustomValidator>  
				</div> 
				<!--Fin Ligne Registre du commerce--> 
				<div class="spacer-small"></div> 
				
				<!--Debut bloc sairir en francais--> 
				<div class="form-field" style="display:block;"> 
					<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><span class="title"><com:TImage ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/flag-fr.png" Attributes.alt="" Attributes.title="<%=Prado::localize('TEXT_FR')%>" /><com:TTranslate>TEXT_SAISIR_FRANCAIS</com:TTranslate></span></div>
					<div class="content"> 
						<div class="spacer-small"></div> 
						<fieldset class="no-border padding-0"> 
							<!--Debut Ligne Libelle fournisseur Fr--> 
							<div class="line"> 
								<div class="intitule-150"><label for="libelleFournisseurFr"><com:TTranslate>TEXT_RAISON_SOCIAL</com:TTranslate> </label><span class="champ-oblig">*</span> :</div> 
								<com:TTextBox id="libelleFournisseurFr" Cssclass="long" Attributes.title="<%=Prado::localize('TEXT_LIBELLE_FOURNISSEUR_FRANCAIS')%>" />
								<com:TCustomValidator
                    				ControlToValidate="libelleFournisseurFr"
                    				ValidationGroup="infosSocietesExclues" 
                    				Display="Dynamic"
                    				ID="validateurLibelleFournisseurFrSocieteExclues" 
                    				ClientValidationFunction="validerLibelleFournisseurFrSocieteExclues"
                    				ErrorMessage="<%=Prado::localize('TEXT_RAISON_SOCIAL_FR')%>"
                					EnableClientScript="true"  						 					 
                					Text="<span title='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' /></span>"
                				>   
                					<prop:ClientSide.OnValidationError>
                           				 document.getElementById('divValidationSummary').style.display='';
                         			</prop:ClientSide.OnValidationError>
                         		</com:TCustomValidator>   
							</div> 
							<!--Fin Ligne Libelle fournisseur Fr--> 
							<!--Debut Ligne Motif Fr--> 
							<div class="line"> 
								<div class="intitule-150"><label for="motifFr"><com:TTranslate>TEXT_MOTIF</com:TTranslate></label><span class="champ-oblig">*</span> :</div> 
								<com:TTextBox TextMode="MultiLine" id="motifFr" CssClass="long" Attributes.title="<%=Prado::localize('TEXT_MOTIF_FRANCAIS')%>"></com:TTextBox>
								<com:TCustomValidator
                    				ControlToValidate="motifFr"
                    				ValidationGroup="infosSocietesExclues" 
                    				Display="Dynamic"
                    				ID="validateurMotifFrSocieteExclues" 
                    				ClientValidationFunction="validerMotifFrSocieteExclues"
                    				ErrorMessage="<%=Prado::localize('TEXT_MOTIF_FRANCAIS')%>"
                					EnableClientScript="true"  						 					 
                					Text="<span title='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' /></span>"
                				>   
                					<prop:ClientSide.OnValidationError>
                           				 document.getElementById('divValidationSummary').style.display='';
                         			</prop:ClientSide.OnValidationError>
                         		</com:TCustomValidator>   
							</div> 
							<!--Fin Ligne Motif Fr--> 
						</fieldset> 
						<div class="breaker"></div> 
					</div> 
					<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div> 
				</div> 
				<!--Fin bloc sairir en francais--> 
				<!--Debut bloc sairir en arabe--> 
				<div class="form-field" style="display:block;"> 
					<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><span class="title"><com:TImage ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/flag-ar.png" attributes.alt="" Attributes.title="<%=Prado::localize('DEFINE_TEXT_AR')%>" /><com:TTranslate>TEXT_SAISIR_ARABE</com:TTranslate></span></div>
					<div class="content"> 
						<div class="spacer-small"></div> 
						<fieldset class="no-border padding-0"> 
							<!--Debut Ligne Libelle fournisseur Ar--> 
							<div class="line"> 
								<div class="intitule-150"><label for="libelleFournisseurAr"><com:TTranslate>TEXT_RAISON_SOCIAL</com:TTranslate> </label><span class="champ-oblig">*</span> :</div> 
								<com:TTextBox id="libelleFournisseurAr" CssClass="long rtl" Attributes.title="<%=Prado::localize('TEXT_LIBELLE_FOURNISSEUR_ARABE')%>" />
								<com:TCustomValidator
                    				ControlToValidate="libelleFournisseurAr"
                    				ValidationGroup="infosSocietesExclues" 
                    				Display="Dynamic"
                    				ID="validateurLibelleFournisseurArSocieteExclues" 
                    				ClientValidationFunction="validerLibelleFournisseurArSocieteExclues"
                    				ErrorMessage="<%=Prado::localize('TEXT_RAISON_SOCIAL_AR')%>"
                					EnableClientScript="true"  						 					 
                					Text="<span title='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' /></span>"
                				>   
                					<prop:ClientSide.OnValidationError>
                           				 document.getElementById('divValidationSummary').style.display='';
                         			</prop:ClientSide.OnValidationError>
                         		</com:TCustomValidator>  
							</div> 
							<!--Fin Ligne Libelle fournisseur Ar--> 
							<!--Debut Ligne Motif Ar--> 
							<div class="line"> 
								<div class="intitule-150"><label for="motifAr"><com:TTranslate>TEXT_MOTIF</com:TTranslate></label><span class="champ-oblig">*</span> :</div> 
								<com:TTextBox TextMode="MultiLine" id="motifAr" CssClass="long rtl" Attributes.title="<%=Prado::localize('TEXT_MOTIF_ARABE')%>"></com:TTextBox>
								<com:TCustomValidator
                    				ControlToValidate="motifAr"
                    				ValidationGroup="infosSocietesExclues" 
                    				Display="Dynamic"
                    				ID="validateurMotifArSocieteExclues" 
                    				ClientValidationFunction="validerMotifArSocieteExclues"
                    				ErrorMessage="<%=Prado::localize('TEXT_MOTIF_ARABE')%>"
                					EnableClientScript="true"  						 					 
                					Text="<span title='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' /></span>"
                				>   
                					<prop:ClientSide.OnValidationError>
                           				 document.getElementById('divValidationSummary').style.display='';
                         			</prop:ClientSide.OnValidationError>
                         		</com:TCustomValidator> 
							</div> 
							<!--Fin Ligne Motif Ar--> 
						</fieldset> 
						<div class="breaker"></div> 
					</div> 
					<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div> 
				</div> 
				<!--Fin bloc sairir en arabe--> 
				<!--Debut Bloc duree Exclusion--> 
				<div class="line"> 
					<div class="intitule-150"><com:TTranslate>TEXT_DUREE_EXCLUSION</com:TTranslate><span class="champ-oblig">*</span>:</div>
					<com:TCustomValidator
        				ControlToValidate="exclusion_definitive"
        				ValidationGroup="infosSocietesExclues" 
        				Display="Dynamic"
        				ID="validateurDureeExclusionSocieteExclues" 
        				ClientValidationFunction="validerDureeExclusionSocieteExclues"
        				ErrorMessage="<%=Prado::localize('TEXT_DUREE_EXCLUSION')%>"
    					EnableClientScript="true"  						 					 
    					Text="<span title='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' /></span>"
    				>   
    					<prop:ClientSide.OnValidationError>
               				 document.getElementById('divValidationSummary').style.display='';
             			</prop:ClientSide.OnValidationError>
             		</com:TCustomValidator>   
					<div class="float-left"> 
						<!--Debut Bloc duree Exclusion definitive--> 
						<div class="clear-both"> 
							<span class="radio-bloc indent-10"> 
								<span class="radio">
									<com:TRadioButton 
									GroupName="duree_exclusion" 
									id="exclusion_definitive" 
									Attributes.title="<%=Prado::localize('TEXT_EXCLUSION_DEFINITIVE')%>"
									Text=""
									ValidationGroup=""
									/>
								</span> 
							</span> 
							<div class="float-left"> 
								<div class="intitule-150"><label for="exclusion_definitive"><%=Prado::localize('TEXT_EXCLUSION_DEFINITIVE')%></label> </div>
								<div class="intitule-auto"><label for="exclusion_definitive_dateStart"><com:TTranslate>DATE_DEBUT</com:TTranslate> :</label></div> 
								<div class="calendar"> 
									<com:TTextBox 
    									Attributes.title="<%=Prado::localize('TEXT_RECHERCHE_AVANCEE_AVANT_LE')%>"
    									Text="" 
    									id="exclusion_definitive_dateStart" 
    									Cssclass="date" 
									/> 
									<com:TImage 
    									ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-calendar.gif"
    									Attributes.alt="<%=Prado::localize('DEFINE_CALENDRIER')%>"
    									Attributes.title="<%=Prado::localize('DEFINE_CALENDRIER')%>"
    									Attributes.onclick="popUpCalendar(this,document.getElementById('ctl0_CONTENU_PAGE_formulaireCreationModification_exclusion_definitive_dateStart'),'dd/mm/yyyy','');"
									/>					
								</div> 
								<div class="info-aide-right"><com:TTranslate>DEFINE_FORMAT_DATE_SIMPLE</com:TTranslate></div>
								<com:TCustomValidator
                    				ControlToValidate="exclusion_definitive"
                    				ValidationGroup="infosSocietesExclues" 
                    				Display="Dynamic"
                    				ID="validateurExclusionDefinitive" 
                    				ClientValidationFunction="validerExclusionDefinitive"
                    				ErrorMessage="<%=Prado::localize('DATE_DEBUT')%>"
                					EnableClientScript="true"  						 					 
                					Text="<span title='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' /></span>"
                				>   
                					<prop:ClientSide.OnValidationError>
                           				 document.getElementById('divValidationSummary').style.display='';
                         			</prop:ClientSide.OnValidationError>
             					</com:TCustomValidator> 
             					<com:TRegularExpressionValidator 
             						Enabled="false"
									ValidationGroup="infosSocietesExclues"
								    ControlToValidate="exclusion_definitive_dateStart"
								    RegularExpression="[0,1]?\d{1}\/(([0-2]?\d{1})|([3][0,1]{1}))\/(([1]{1}[9]{1}[9]{1}\d{1})|([2-9]{1}\d{3}))" 	
								    ErrorMessage="<%=Prado::localize('FORMAT_DATE')%>"
						 			Text="<span title='<%=Prado::localize('FORMAT_INCORRECTE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('FORMAT_INCORRECTE')%>' /></span>">
									<prop:ClientSide.OnValidationError>
           				 				document.getElementById('divValidationSummary').style.display='';           				 				
         							</prop:ClientSide.OnValidationError>
             					</com:TRegularExpressionValidator>    
							</div> 
						</div> 
						<!--Fin Bloc duree Exclusion definitive--> 
						<!--Debut Bloc duree Exclusion temporaire--> 
						<div class="clear-both"> 
							<span class="radio-bloc indent-10"> 
								<span class="radio">
									<com:TRadioButton 
									GroupName="duree_exclusion" 
									id="exclusion_temporaire" 
									Attributes.title="<%=Prado::localize('TEXT_EXCLUSION_TEMPORAIRE')%>"
									ValidationGroup="" 
								/>
								</span> 
							</span> 
							<div class="float-left"> 
								<div class="intitule-150"><label for="exclusion_temporaire"><com:TTranslate>TEXT_EXCLUSION_TEMPORAIRE</com:TTranslate></label> </div> 
								<div class="intitule-auto"><label for="exclusion_temporaire_dateStart"><com:TTranslate>DATE_DEBUT</com:TTranslate> :</label></div> 
								<div class="calendar"> 
									<com:TTextBox 
    									Attributes.title="<%=Prado::localize('TEXT_RECHERCHE_AVANCEE_AVANT_LE')%>"
    									Text="" 
    									id="exclusion_temporaire_dateStart" 
    									Cssclass="date" 
									/> 
									<com:TImage 
    									ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-calendar.gif"
    									Attributes.alt="<%=Prado::localize('DEFINE_CALENDRIER')%>"
    									Attributes.title="<%=Prado::localize('DEFINE_CALENDRIER')%>"
    									Attributes.onclick="popUpCalendar(this,document.getElementById('ctl0_CONTENU_PAGE_formulaireCreationModification_exclusion_temporaire_dateStart'),'dd/mm/yyyy','');"
									/>		
									<com:TRegularExpressionValidator 
										Enabled="false"
    									ValidationGroup="infosSocietesExclues"
    								    ControlToValidate="exclusion_temporaire_dateStart"
    								    RegularExpression="[0,1]?\d{1}\/(([0-2]?\d{1})|([3][0,1]{1}))\/(([1]{1}[9]{1}[9]{1}\d{1})|([2-9]{1}\d{3}))" 	
    								    ErrorMessage="<%=Prado::localize('FORMAT_DATE')%>"
    						 			Text="<span title='<%=Prado::localize('FORMAT_INCORRECTE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('FORMAT_INCORRECTE')%>' /></span>">
    									<prop:ClientSide.OnValidationError>
               				 				document.getElementById('divValidationSummary').style.display='';
             							</prop:ClientSide.OnValidationError>
                 					</com:TRegularExpressionValidator>    			
								</div> 
								<div class="intitule-auto indent-10"><label for="exclusion_temporaire_dateEnd"><com:TTranslate>DATE_FIN</com:TTranslate> : </label></div> 
								<div class="calendar"> 
									<com:TTextBox 
    									Attributes.title="<%=Prado::localize('TEXT_ET_LE')%>"
    									Text="" 
    									id="exclusion_temporaire_dateEnd" 
    									Cssclass="date" 
									/> 
									<com:TImage 
    									ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-calendar.gif"
    									Attributes.alt="<%=Prado::localize('DEFINE_CALENDRIER')%>"
    									Attributes.title="<%=Prado::localize('DEFINE_CALENDRIER')%>"
    									Attributes.onclick="popUpCalendar(this,document.getElementById('ctl0_CONTENU_PAGE_formulaireCreationModification_exclusion_temporaire_dateEnd'),'dd/mm/yyyy','');" 
									/>	
									<com:TRegularExpressionValidator 
										Enabled="false"
    									ValidationGroup="infosSocietesExclues"
    								    ControlToValidate="exclusion_temporaire_dateEnd"
    								    RegularExpression="[0,1]?\d{1}\/(([0-2]?\d{1})|([3][0,1]{1}))\/(([1]{1}[9]{1}[9]{1}\d{1})|([2-9]{1}\d{3}))" 	
    								    ErrorMessage="<%=Prado::localize('FORMAT_DATE')%>"
    						 			Text="<span title='<%=Prado::localize('FORMAT_INCORRECTE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('FORMAT_INCORRECTE')%>' /></span>">
    									<prop:ClientSide.OnValidationError>
               				 				document.getElementById('divValidationSummary').style.display='';
             							</prop:ClientSide.OnValidationError>
                 					</com:TRegularExpressionValidator>  				
								</div> 
								<div class="info-aide-right"><com:TTranslate>DEFINE_FORMAT_DATE_SIMPLE</com:TTranslate></div>
								<com:TCustomValidator
                    				ControlToValidate="exclusion_temporaire"
                    				ValidationGroup="infosSocietesExclues" 
                    				Display="Dynamic"
                    				ID="validateurExclusionTemporaire" 
                    				ClientValidationFunction="validerExclusionTemporaire"
                    				ErrorMessage="<%=Prado::localize('TEXT_DATE_EXCLUSION')%>"
                					EnableClientScript="true"  						 					 
                					Text="<span title='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' /></span>"
                				>   
                					<prop:ClientSide.OnValidationError>
                           				 document.getElementById('divValidationSummary').style.display='';
                         			</prop:ClientSide.OnValidationError>
             					</com:TCustomValidator>    
							</div> 
							
						</div> 
						<!--Fin Bloc duree Exclusion temporaire--> 
					</div> 
				</div> 
				<!--Fin Bloc duree Exclusion--> 
				<div class="spacer-small"></div> 
				<!--Debut Bloc portee de l'exclusion--> 
				<div class="line"> 
					<div class="intitule-150"><com:TTranslate>TEXT_PORTEE_EXCLUSION</com:TTranslate><span class="champ-oblig">*</span>:<com:TImage ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" Attributes.onmouseover="afficheBulle('infosPorteeExclusion', this)" Attributes.onmouseout="cacheBulle('infosPorteeExclusion')" Cssclass="picto-info-intitule" Attributes.alt="Info-bulle" Attributes.title="Info-bulle" /><div id="infosPorteeExclusion" class="info-bulle" onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();"><div><com:TTranslate>TEXT_INFO_BULLE_PORTEE_EXCLUSION</com:TTranslate></div></div></div>
					<com:TCustomValidator
        				ControlToValidate="portee_partielle"
        				ValidationGroup="infosSocietesExclues" 
        				Display="Dynamic"
        				ID="validateurPorteeExclusionSocieteExclues" 
        				ClientValidationFunction="validerPorteeExclusionSocieteExclues"
        				ErrorMessage="<%=Prado::localize('TEXT_PORTEE_EXCLUSION')%>"
    					EnableClientScript="true"  						 					 
    					Text="<span title='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' /></span>"
    				>   
    					<prop:ClientSide.OnValidationError>
               				 document.getElementById('divValidationSummary').style.display='';
             			</prop:ClientSide.OnValidationError>
             		</com:TCustomValidator>    
					<div class="float-left"> 
						<!--Debut Bloc portee partielle--> 
						<div class="clear-both"> 
							<span class="radio-bloc indent-10"> 
								<span class="radio">
									<com:TRadioButton 
    									GroupName="portee_exclusion" 
    									id="portee_partielle" 
    									Attributes.title="<%=Prado::localize('TEXT_PORTEE_PARTIELLE')%>"
    									ValidationGroup=""
    								/>
								</span> 
							</span> 
							<div class="float-left"> 
								<div class="intitule-150"><label for="portee_partielle"><com:TTranslate>TEXT_PORTEE_PARTIELLE</com:TTranslate></label> </div> 
							</div> 
						</div> 
						<!--Fin Bloc portee partielle--> 
						<!--Debut Bloc portee totale--> 
						<div class="clear-both"> 
							<span class="radio-bloc indent-10"> 
								<span class="radio">
								<com:TRadioButton 
    								GroupName="portee_exclusion" 
    								id="portee_totale" 
    								Attributes.title="<%=Prado::localize('TEXT_PORTEE_TOTALE')%>"
    							/>
								</span> 
							</span> 
							<div class="float-left"> 
								<div class="intitule-150"><label for="portee_totale"><com:TTranslate>TEXT_PORTEE_TOTALE</com:TTranslate></label> </div> 
							</div> 
						</div> 
						<!--Fin Bloc portee totale--> 
					</div> 
				</div> 
				<!--Fin Bloc portee de l'exclusion--> 
 
				<div class="spacer-small"></div> 
			</div> 
			<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div> 
		</div> 
		<!--Fin Bloc Identification de la societe a exclure--> 