<?php

namespace Application\Controls;

use App\Service\Crypto\Certificat;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Crypto;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Crypto\Atexo_Crypto_Certificat;
use Prado\Prado;

/**
 * composant de gestion de l'authentification par certificat.
 *
 * @author Amal EL BEKKAOUI <amal.elbekkaoui@atexo.com>
 * @copyright Atexo 2010
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class AuthentificationByCert extends MpeTPage
{
    private $_certif;
    private $_serial;
    private $_certFileTmp;
    private string $_message = '';
    private bool $_selectionCertObligatoire = true; //Permet de rendre obligatoire la selection du certificat (true= obligatoire, false sinon)
    private bool $_update = false;

    public function setCertificat($value)
    {
        $this->_certif = $value;
    }

    public function getCertificat()
    {
        return $this->_certif;
    }

    public function setSerial($value)
    {
        $this->_serial = $value;
    }

    public function getSerial()
    {
        return $this->_serial;
    }

    public function setCertFileTmp($value)
    {
        $this->_certFileTmp = $value;
    }

    public function getCertFileTmp()
    {
        return $this->_certFileTmp;
    }

    public function getMessage()
    {
        return $this->_message;
    }

    public function getSelectionCertObligatoire()
    {
        return $this->_selectionCertObligatoire;
    }

    public function setSelectionCertObligatoire($value)
    {
        return $this->_selectionCertObligatoire = $value;
    }

    public function hasFile()
    {
        return $this->certificat->HasFile;
    }

    public function getUpdate()
    {
        return $this->_update;
    }

    public function setUpdate($value)
    {
        return $this->_update = $value;
    }

    /**
     * Verification du certificat.
     */
    public function checkCertificatFileValidity($sender, $param)
    {
        if ('' != $this->certificat->FileName) {
            $certFile = Atexo_Config::getParameter('COMMON_TMP') . $this->certificat->FileName;

            $this->_certFileTmp = $certFile;
            if (move_uploaded_file($this->certificat->LocalName, $certFile)) {
                $certificatService = Atexo_Util::getSfService(Certificat::class);
                $infosCertificat = $certificatService->getCertificatDetails($certFile);

                //tester la validité du certificat
                if (!empty($infosCertificat['chaineDeCertificationValide'])) {
                    $this->Page->script->Text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";
                    $param->IsValid = false;
                    $this->certificatCreationValidator->ErrorMessage = Prado::localize('TEXT_CERTIFICAT_NOT_VALIDE');
                    unlink($certFile);

                    return;
                }

                //tester la révocation du certificat
                if (!empty($infosCertificat['absenceRevocationCRL'])) {
                    $this->Page->script->Text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";
                    $param->IsValid = false;
                    $this->certificatCreationValidator->ErrorMessage = Prado::localize('TEXT_CERTIFICAT_REVOQUE');
                    unlink($certFile);

                    return;
                }

                //tester l'unicité du certificat
                $certif = file_get_contents($certFile);

                $serialFile = (new Atexo_Crypto())->getSerialFromCertif($certFile);
                $serialChaine = file_get_contents($serialFile);
                $serials = explode('=', $serialChaine);
                $serial = trim($serials[1]);
                unlink($serialFile);
                if (!$certif && !$serial) {
                    $this->Page->script->Text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";
                    $param->IsValid = false;
                    $this->certificatCreationValidator->ErrorMessage = Prado::localize('TEXT_CERTIF_NON_SELECTIONNE');
                    unlink($certFile);

                    return;
                }

                if (false == $this->getUpdate()) {
                    $agent = (new Atexo_Agent())->retrieveAgentByCertificatAndSerial($certif, $serial);
                    if ($agent) {
                        $this->Page->script->Text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";
                        $param->IsValid = false;
                        $this->certificatCreationValidator->ErrorMessage = Prado::localize('TEXT_CERTIFICAT_EXISTE_DEJA');
                        unlink($certFile);

                        return;
                    }
                }

                //tester la date d'expiration
                if (empty($infosCertificat['periodiciteValide'])) {
                    $this->_message = "bootbox.alert('" . Prado::localize('TEXT_CERTIFICAT_EXPIRER') . "');";
                }

                self::setCertificat($certif);
                self::setSerial($serial);
            }
        }
    }
}
