<div class="bloc-fileupload">
	<span class="btn btn-success fileinput-button"><%=$this->getTitre()%>
		<input <%=$this->getDisabled()%> id="<%=$this->getClientId()%>_fileupload" type="file"  name="files[]" data-url="index.php?page=<%= Application\Service\Atexo\Atexo_Util::getTypeUserCalledForPradoPages() %>.SaveJqueryUploadFile" multiple >
	</span>
	<div id="<%=$this->getClientId()%>fileName" ></div>
	<div style="display:<%=$this->getAfficherProgress() ? '' : 'none'%>">
		<div id="<%=$this->getClientId()%>erreur" class="red" ></div>
		<div class="progress progress-success progress-striped" id="<%=$this->getClientId()%>_progress">
			  <div class="bar" style="width: 0%;"></div>
		</div>
		<div id="<%=$this->getClientId()%>percent" class="pourcentage-chargement">0%</div>
	</div>
	<com:TActiveHiddenField ID="NomFichier" /> 
	<com:TActiveHiddenField	id="reponseServeur" /> 
	<com:TActiveHiddenField ID="hasFile" />
	<com:TActiveHiddenField ID="filesSize" />
</div>
<script>
	var tmpFilePrefix = '<%=$this->getTmpFileName()%>';
	var reponseServeur = "";
	var fileName = "";
	var sizeFile = "";
	var i = 0;

J(function () {
	 J('#<%=$this->getClientId()%>_fileupload').click(function() {
		 //J('#<%=$this->getClientId()%>_NomFichier')..empty();
		 fileName =  '';
		 reponseServeur =  '';
		 sizeFile = "";
		 i = 0;
		 J('#<%=$this->getClientId()%>_NomFichier').val('');
		 J('#<%=$this->getClientId()%>_filesSize').val('');
		 J('#<%=$this->getClientId()%>fileName').empty();
		 J('#<%=$this->getClientId()%>_hasFile').val('0');
		 J('#<%=$this->getClientId()%>erreur').empty();
		 J('#<%=$this->getClientId()%>_reponseServeur').empty();;
		   J('#<%=$this->getClientId()%>_progress .bar').css('width','0');
		   J('#<%=$this->getClientId()%>percent').empty();
		   J('#<%=$this->getClientId()%>percent').append('0%');
	 });
    J('#<%=$this->getClientId()%>_fileupload').fileupload({
         dataType: 'json',
		 maxChunkSize: 10000000,
         recalculateProgress : true,
         progressInterval : 200,
		submit : function (e, data) {
			data.formData = {tmpFileName: tmpFilePrefix+J.now()+getUid()};
			},
         done: function (e, data) {
            J.each(data.result.files, function (index, file) {
				if(reponseServeur){
					reponseServeur = reponseServeur+ '|'+(file.tmpFile);
				}else{
					reponseServeur = (file.tmpFile);
				}
				if(sizeFile){
					sizeFile = sizeFile + "|" + file.size;
				}else{
					sizeFile = (file.size);
				}
            	J('#<%=$this->getClientId()%>_hasFile').val('1');
            	J('#<%=$this->getClientId()%>erreur').empty();
            	var progress = 0;
                var name = navigator.appName ;
				if(i){
					J('#<%=$this->getClientId()%>fileName').append(' ; ');
				}

				if (name == 'Microsoft Internet Explorer') {
					nomFichier = decodeURIComponent(file.nomFichier);
	            } else {
					nomFichier = utf8_decode(decodeURIComponent(file.nomFichier));
				}
				J('#<%=$this->getClientId()%>fileName').append(nomFichier);

				if(fileName){
					fileName = fileName + '|' + nomFichier;
				}else{
					fileName = nomFichier ;
				}
				i++;
            });
			 if(fileName){
				 J('#<%=$this->getClientId()%>_NomFichier').val(fileName);
			 }
			 if(reponseServeur){
				 J('#<%=$this->getClientId()%>_reponseServeur').val(reponseServeur);
			 }
			 J('#<%=$this->getClientId()%>_filesSize').val(sizeFile);
			 <%= $this->getFonctionJs()? $this->getFonctionJs() : "" %>;
        },
        progressall: function (e, data) {
             var progress = parseInt(data.loaded / data.total * 100, 10);
             J('#<%=$this->getClientId()%>_progress .bar').css('width',progress + '%');
         	 J('#<%=$this->getClientId()%>percent').empty();
        	 J('#<%=$this->getClientId()%>percent').append(progress+"%");

	    },
        fail: function (e, data) {
        	J('#<%=$this->getClientId()%>erreur').empty();
        	J('#<%=$this->getClientId()%>erreur').append("<%=Prado::localize('DEFINE_ERREUR_CHARGMENT_FICHIER')%>");
        	J('#<%=$this->getClientId()%>_progress .bar').css('width','0');
  		    J('#<%=$this->getClientId()%>percent').empty();
  		    J('#<%=$this->getClientId()%>percent').append('0%');
        }
    });

});

</script>