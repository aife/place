		<com:AtexoValidationSummary  ValidationGroup="validateinfomessage" />
		<com:PanelMessageErreur  ID="messageErreur" visible="false"/>
		
		<!--warning : si la consultation a été archivée, les modifications ne sont pas intégrées dans l'archive-->
		<com:PanelMessageAvertissement ID="panelMessageWarning" Visible="false"/>
		<!--Debut Bloc Message à envoyer-->
        <com:TPanel ID="panelTypeMessage" CssClass="form-field">
        	<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
            <com:TPanel CssClass="content">
                <div class="line">
                    <div class="intitule-100"><label for="<%=$this->messageType->getClientId()%>"><com:TTranslate>DEFINE_TEXT_TYPE_MESSAGE</com:TTranslate></label> :</div>
                    <com:TActiveDropDownList 
                    	ID="messageType"
                    	CssClass="moyen"
                    	OnCallBack="refreshText"
                    	Attributes.title = "<%=Prado::localize('DEFINE_TEXT_TYPE_MESSAGE')%>"
                    />
                </div>
            </com:TPanel>
            <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
        </com:TPanel>
		<div class="form-field">
			<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><span class="title"><com:TTranslate>TEXT_MESSAGE</com:TTranslate></span></div>
			<div class="content">
				<com:TPanel ID="panelDestinataire" CssClass="line">
					<div class="intitule-100"><label for="<%=$this->destinataires->getClientId()%>"><com:TTranslate>DEFINE_TEXT_DESTINATAIRE</com:TTranslate></label> :</div>
					<div class="content-bloc bloc-660">
						<com:TLabel ID="adressesLibresHidden" />
						<com:TTextBox
							ID="destinataires"
							CssClass="long-630 disabled"
							TextMode="MultiLine"
							Attributes.readonly = "true"
							Attributes.title = "<%=Prado::localize('DEFINE_TEXT_DESTINATAIRE')%>"
						/>
				       <com:TRequiredFieldValidator 
							ControlToValidate="destinataires"
							ValidationGroup="validateinfomessage"
							Display="Dynamic"
							ErrorMessage="<%=Prado::localize('DEFINE_TEXT_DESTINATAIRE')%>"
							EnableClientScript="true"  						 					 
				 			Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
				 			<prop:ClientSide.OnValidationError>
   				 				document.getElementById('divValidationSummary').style.display='';
 							</prop:ClientSide.OnValidationError>
         		  		</com:TRequiredFieldValidator> 
				       	<com:THiddenField id="adresseRegistreRetrait" value="0"/>
				       	<com:THiddenField id="adresseRegistreQuestion" value="0"/>
				       	<com:THiddenField id="adresseRegistreDepot" value="0"/>
				       	<com:THiddenField id="adresseLibre" value="0"/>
						 
						 <com:TLinkButton
							ID="buttonEditdestinataire"
							CssClass="bouton-edit clear-both" 
							onClick="editerDestinatairesMail"
							visible="false"
							Attributes.title="<%=Prado::localize('DEFINE_TEXT_EDITER_LIST_DESTINATAIRE')%>" 
							Text="<%=Prado::localize('DEFINE_TEXT_EDITER')%>">
						</com:TLinkButton>
					</div>
				</com:TPanel>
                <!--[if lte IE 6]>
                	<div class="spacer-mini"></div>
                <![endif]-->
				<div class="line">
					<div class="intitule-100"><label for="<%=$this->objet->getClientId()%>"><com:TTranslate>OBJET</com:TTranslate></label> :</div>
					<com:TActiveTextBox
							ID="objet"
							CssClass="long-630"
							Attributes.title = "<%=Prado::localize('OBJET')%>"
						/>
				</div>
				<div class="spacer-mini"></div>
				<div class="line">
					<div class="intitule-100"><label for="<%=$this->textMail->getClientId()%>"><com:TTranslate>DEFINE_TEXT_TEXT</com:TTranslate></label> :</div>
						<com:TActiveTextBox
							ID="textMail"
							CssClass="long-630 high-360"
							TextMode="MultiLine"
							Attributes.title = "<%=Prado::localize('DEFINE_TEXT_TEXT')%>"
						/>
				</div>
				<div class="spacer-mini"></div>
				<com:TPanel ID="panelCorpsAR" CssClass="line" Visible="false">
					<div class="intitule-100"><label for="<%=$this->textMail->getClientId()%>"><com:TTranslate>DEFINE_TEXT_MSG_ORIGINE</com:TTranslate></label> :</div>
	  				<com:TTextBox
						ID="corpsAR"
						Attributes.disabled="disabled"
						CssClass="bloc-630 high-360"
						TextMode="MultiLine"
						Attributes.title="<%=Prado::localize('DEFINE_TEXT_LIST_ADRESSE_LIBRE')%>"
					/>
				</com:TPanel>
				<div class="spacer-mini"></div>
				<div class="line">
					<div class="intitule-100"><label for="<%=$this->objet->getClientId()%>"><com:TTranslate>DEFINE_TEXT_PIECE_JOINTE</com:TTranslate></label> :</div>
                        <com:TActivePanel CssClass="content-bloc bloc-650" ID="PanelPJ">
						  <com:TRepeater ID="RepeaterPiecesJointes" EnableViewState="true" DataKeyField="Id">
							<prop:ItemTemplate>
								<div class="picto-link inline">
										<a href="?page=Agent.DownloadPjEchange&idPj=<%#$this->Data->getPiece()%>"><%#$this->SourceTemplateControl->formaterNomFichierPj($this->Data)%><span class="info-aide"> (<%# Application\Service\Atexo\Atexo_Util::arrondirSizeFile($this->Data->getTaille()/1024)%>)</span></a>
										<com:TActiveLinkButton 
											CssClass="indent-5"
											OnCallBack="Page.templateEnvoiCourrierElectroniqueComplementsFormulaires.refreshRepeaterPJ"
											OnCommand="Page.templateEnvoiCourrierElectroniqueComplementsFormulaires.onDeleteClick"
											CommandParameter="<%#$this->Data->getId()%>"
											>
											<com:TImage 
												ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-supprimer.gif"
												AlternateText="<%=Prado::localize('DEFINE_SUPPRIMER')%>"
												Attributes.title="<%=Prado::localize('DEFINE_SUPPRIMER')%>"
												/>
										</com:TActiveLinkButton>
								</div>
								<br/><br/>
							</prop:ItemTemplate>
						</com:TRepeater>
						
						<com:TActiveHyperLink 
							ID="buttonEditPj"
							NavigateUrl="javascript:popUp('?page=<%= Application\Service\Atexo\Atexo_Util::getTypeUserCalledForPradoPages() %>.popUpListePj&Idmsg=<%=$this->getViewState('idMessage')%>&org=<%=$this->_org%>&idBtnValidatePj=<%=$this->validatePJ->getClientId()%>','yes');"
							CssClass="bouton-edit clear-both" 
							Attributes.title="<%=Prado::localize('DEFINE_TEXT_EDITER_LIST_PIECE_JOINTE')%>" 
							Text="<%=Prado::localize('DEFINE_TEXT_EDITER')%>">
						</com:TActiveHyperLink>
		  				
		  				<div class="breaker"></div>
					  </com:TActivePanel>
					  <span style ="display:none;">
						<com:TActiveButton 
							Id="validatePJ"							  
							onCallBack="refreshRepeaterPJ"
							ActiveControl.CallbackParameter=<%=$this->getViewState('idMessage')%>
							/>
					</span>
				</div>
				<div class="breaker"></div>
			</div>
			<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
		</div>
		<!--Fin Bloc Message à envoyer-->
        <!--Debut Bloc Option d'envoi-->
		<com:TPanel ID="panelOptionEnvoi" CssClass="form-field">
			<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><span class="title"><com:TTranslate>DEFINE_TEXT_OPTION_ENVOI</com:TTranslate></span></div>
				<div class="content">
				<div class="spacer-mini"></div>
				<div class="line">
					<div class="intitule-auto bloc-700">
					<com:TRadioButton
						ID = "courrierElectroniqueSimple"
						GroupName="optionEnvoie"
						CssClass = "radio"
						Attributes.title = "<%=Prado::localize('DEFINE_TEXT_COURIER_ELECTRONIQUE_SIMPLE')%>"					
					/>
					<label for="<%=$this->courrierElectroniqueSimple->getClientId()%>"><com:TTranslate>DEFINE_TEXT_COURIER_ELECTRONIQUE_SIMPLE</com:TTranslate></label><span class="info-aide indent-10"><com:TTranslate>DEFINE_TEXT_TAILLE_PIECE_JOINTE</com:TTranslate></span>
				  </div>
					<div class="spacer-mini"></div>
					<div class="intitule-auto bloc-700">
					<com:TRadioButton
						ID = "courrierElectroniqueContenuIntegralAR"
						GroupName="optionEnvoie"
						CssClass = "radio"
						Attributes.title = "<%=Prado::localize('DEFINE_TEXT_COURIER_ELECTRONIQUE_CONTENU_INTEGRAL_COMPLEMENTS_FORMULAIRES')%>"					
					/>
					<label for="<%=$this->courrierElectroniqueContenuIntegralAR->getClientId()%>"><com:TTranslate>DEFINE_TEXT_COURIER_ELECTRONIQUE_CONTENU_INTEGRAL_COMPLEMENTS_FORMULAIRES</com:TTranslate></label><span class="info-aide indent-10"><com:TTranslate>DEFINE_TEXT_TAILLE_PIECE_JOINTE</com:TTranslate></span>
					</div>
                    <div class="spacer-mini"></div>
                    <div class="intitule-auto bloc-700">
                    <com:TRadioButton
						ID = "courrierElectroniqueUniquementLienTelechargementObligatoire"
						GroupName="optionEnvoie"
						CssClass = "radio"
						Attributes.title = "<%=Prado::localize('DEFINE_TEXT_COURIER_ELECTRONIQUE_TELECHARGEMENT_UNIQUE_COMPLEMENTS_FORMULAIRES')%>"					
					/>
					<label for="<%=$this->courrierElectroniqueUniquementLienTelechargementObligatoire->getClientId()%>"><com:TTranslate>DEFINE_TEXT_COURIER_ELECTRONIQUE_TELECHARGEMENT_UNIQUE_COMPLEMENTS_FORMULAIRES</com:TTranslate></label>
				  </div>
				  <div class="spacer-mini"></div>
                   <div class="intitule-auto bloc-700">
                    <com:TRadioButton
						ID = "courrierElectroniqueDeDemandeDeComplement"
						GroupName="optionEnvoie"
						Enabled="true"
						CssClass = "radio"
						Attributes.title = "<%=Prado::localize('DEFINE_TEXT_COURIER_ELECTRONIQUE_DEMANDE_COMPLEMENT_COMPLEMENTS_FORMULAIRES')%>"					
					/>
					<label for="<%=$this->courrierElectroniqueDeDemandeDeComplement->getClientId()%>"><com:TTranslate>DEFINE_TEXT_COURIER_ELECTRONIQUE_DEMANDE_COMPLEMENT_COMPLEMENTS_FORMULAIRES</com:TTranslate></label> 
				  </div>
					<div class="breaker"></div>
				</div>
			</div>
			<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
		</com:TPanel>
		<!--Fin Bloc Option d'envoi-->
		<!--Debut line boutons-->
		<div class="boutons-line">
			<com:TButton  
				CssClass="bouton-annulation float-left" 
				Attributes.value="<%=Prado::localize('TEXT_ANNULER')%>" 
				Attributes.title="<%=Prado::localize('DEFINE_TEXT_MAIL_RETOUR')%>"
				OnClick="RedirectReturn"
				/>
			<com:TButton  
				CssClass="bouton-validation float-right" 
				Attributes.value="<%=Prado::localize('DEFINE_TEXT_ENVOYER')%>" 
				Attributes.title="<%=Prado::localize('DEFINE_TEXT_ENVOIE_IMMEDIAT')%>" 
				onClick="envoyer"
				ValidationGroup="validateinfomessage" 
				/>
		</div>
		<!--Fin line boutons-->
		<div class="breaker"></div>
