<!--Debut Formulaire-->
<%=$this->getTitre()%>
<div class="tableau-detail-pli">
    <!--Debut bloc detail-->
    <div class="bloc-detail">
        <!--Debut Message-->
        <com:PanelMessage Visible="<%=$this->getMessage()?true:false%>" id="panelMessage"
                          message="<%=$this->getMessage()%>" typeMessage="<%=$this->getTypeMessage()%>"
                          cssClass="alert-dismissable"/>
        <!--Fin Message-->
        <div class="panel panel-default" id="panelInfoJeton">
                <div class="bloc">
                  <strong>
                      <com:TTranslate>DEFINE_JETON_SIGNATURE</com:TTranslate>
                      :
                  </strong>
                  <com:TLabel id="pathFileJeton" Text="<%=$this->getPathFileJeton()%>"></com:TLabel>
        </div>
        </div>


        <com:TRepeater ID="repeaterListeSignatures">
            <prop:ItemTemplate>
                <b>
                    <com:TTranslate>DEFINE_TEXT_SIGNATURE</com:TTranslate>&nbsp;
                    <com:TLabel ID="numeroSignature" Text="<%#($this->ItemIndex+1)%>"/>
                </b>
                <!--BEGIN CERTIFICAT DE SIGNATURE-->
                <div class="panel panel-default bloc">
                    <div class="title">
                        <com:TTranslate>DEFINE_CERTIFICAT_SIGNATURE</com:TTranslate>
                        <span class="m-l-1">
                            <i class="fa fa-question-circle" data-toggle="tooltip" title="<%=Prado::localize('DEFINE_INFO_BULLE_CERTIFICAT_SIGNATURE')%>"></i>
                        </span>
                    </div>
                    <div>
                        <div class="col-md-4 column column-xlarge">
                            <div class="page-header m-t-0 m-b-1">
                                <strong>
                                    <com:TTranslate>DEFINE_CERTIFICAT_EMIS_A</com:TTranslate> :
                                </strong>
                            </div>
                            <div class="clearfix">
                                <com:TLabel ID="detailSignataire" Text="<%#($this->Data->getInfosSignature() instanceof Application\Service\Atexo\Signature\Atexo_Signature_InfosVo) ? $this->Data->getInfosSignature()->getSignatairePourVerification() : ''%>"/>
                            </div>
                        </div>
                        <div class="col-md-4 column column-large">
                            <div class="page-header m-t-0 m-b-1">
                                <strong>
                                    <com:TTranslate>CERTIFICAT_EMIS_PAR</com:TTranslate> :
                                </strong>
                            </div>
                            <div class="clearfix">
                                <com:TLabel ID="detailEmisPar" Text="<%#($this->Data->getInfosSignature() instanceof Application\Service\Atexo\Signature\Atexo_Signature_InfosVo) ? $this->Data->getInfosSignature()->getEmetteurFormat() : ''%>"/>
                            </div>
                        </div>
                        <div class="col-md-4 column">
                            <div class="page-header m-t-0 m-b-1">
                                <strong>
                                    <com:TTranslate>TEXT_DATE_DE_VALIDITE</com:TTranslate>
                                    <com:TLabel id="dateControlevaliditeCr"/> :
                                </strong>
                            </div>
                            <div class="row">
                                <div class="col-md-5">
                                    <com:TTranslate>TEXT_DU</com:TTranslate> :
                                </div>
                                <div class="col-md-7 p-l-0">
                                    <com:TLabel id="date_Du" Text="<%#($this->Data->getInfosSignature() instanceof Application\Service\Atexo\Signature\Atexo_Signature_InfosVo) ? $this->Data->getInfosSignature()->getDateValideFrom() : ''%>"/>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5">
                                    <com:TTranslate>TEXT_AU</com:TTranslate> :
                                </div>
                                <div class="col-md-7 p-l-0">
                                    <com:TLabel id="date_Au" Text="<%#($this->Data->getInfosSignature() instanceof Application\Service\Atexo\Signature\Atexo_Signature_InfosVo) ? $this->Data->getInfosSignature()->getDateValideTo() : ''%>"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--END CERTIFICAT DE SIGNATURE-->
                <!--BEGIN CONTROLES DE VALIDITE DU CERTIFICAT-->
                <div class="panel panel-default">
                    <div class="panel-heading hide-agent">
                        <div class="h4 panel-title ">
                            <com:TTranslate>DEFINE_CONTROLE_CONTROLE_TITLE</com:TTranslate>
                    </div>
                    </div>
                    <div class="panel panel-default bloc">
                        <div class="col-md-6 column column-xlarge">
                            <div class="page-header m-t-0 m-b-1 title">
                                <strong>
                                    <com:TTranslate>DEFINE_CONTROLE_VALIDITATION_CERTIF</com:TTranslate>
                                    <i class="fa fa-question-circle fa-lg m-l-1"  data-placement="right" title="<%=Prado::localize('DEFINE_INFO_BULLE_CONTROLE_VALIDITE')%>"></i>
                                </strong>
                            </div>
                            <div class="clearfix">
                                <com:TTranslate>DEFINE_CONTROLE_REALISES_LE</com:TTranslate>
                                <%=Application\Service\Atexo\Atexo_Util::iso2frnDateTime(date("Y-m-d H:i:s"))%>
                            </div>
                            <div class="clearfix">
                                <com:TTranslate>PERIODE_VALIDITE</com:TTranslate> :
                                <com:PictoStatutSignature NouveauAffichage="true" ID="periodeValidite"
                                                          Statut="<%#($this->Data->getInfosSignature() instanceof Application\Service\Atexo\Signature\Atexo_Signature_InfosVo) ? $this->Data->getStatutPeriodeValiditeCert() : ''%>"/>
                            </div>
                            <div class="clearfix">
                                <com:TTranslate>CHAINE_CERTIFICATION</com:TTranslate> :
                                <com:PictoStatutSignature NouveauAffichage="true" ID="chaine"
                                                          Statut="<%#($this->Data->getInfosSignature() instanceof Application\Service\Atexo\Signature\Atexo_Signature_InfosVo) ? $this->Data->getStatutVerifChaineCert() : ''%>"/>
                            </div>
                            <div class="clearfix">
                                <com:TPanel ID="panelReferentielCertificat" Visible="<%=Application\Service\Atexo\Atexo_Module::isEnabled('SurchargeReferentiels')%>" cssClass="">
                                    <div class="indent-arrow">
                                        <com:TTranslate>DEFINE_REFERENTIEL_DERTIFICAT</com:TTranslate>
                                        : <span class="<%#($this->Data->getInfosSignature() instanceof Application\Service\Atexo\Signature\Atexo_Signature_InfosVo) ? $this->Data->getNomReferentielCertificat() : 'UNKNOWN'%>"> <%#($this->Data->getInfosSignature() instanceof Application\Service\Atexo\Signature\Atexo_Signature_InfosVo) ? $this->Data->getNomReferentielCertificat() : ''%></span>
                                    </div>
                                </com:TPanel>
                            </div>
                            <div class="clearfix">
                                <com:TTranslate>DEFINE_NON_REVOCATION</com:TTranslate> :
                                <com:PictoStatutSignature NouveauAffichage="true" ID="CRL" Statut="<%#($this->Data->getInfosSignature() instanceof Application\Service\Atexo\Signature\Atexo_Signature_InfosVo) ? $this->Data->getStatutRevocationCert() : ''%>"/>
                            </div>
                        </div>
                        <div class="col-md-6 column column-xlarge">
                            <div class="page-header m-t-0 m-b-1 title">
                                <strong>
                                    <com:TTranslate>DEFINE_INTERGRITE_FICHIER_SIGNE</com:TTranslate>
                                    <i class="fa fa-question-circle fa-lg m-l-1"  data-placement="right" title="<%=Prado::localize('DEFINE_INFO_BULLE_INTEGRITE_FICHIER')%>"></i>
                                </strong>
                            </div>
                            <div class="clearfix">
                                <com:TTranslate>DEFINE_CONTROLE_REALISES_LE</com:TTranslate>
                                <%=Application\Service\Atexo\Atexo_Util::iso2frnDateTime(date("Y-m-d H:i:s"))%>
                            </div>
                            <div class="clearfi">
                                <com:TTranslate>DEFINE_NON_REPUDIATION</com:TTranslate>
                                <com:PictoStatutSignature NouveauAffichage="true" ID="NR" Statut="<%#($this->Data->getInfosSignature() instanceof Application\Service\Atexo\Signature\Atexo_Signature_InfosVo) ? $this->Data->getStatutRejeuSignature() : ''%>"/>
                            </div>
                        </div>
                    </div>
                </div>
                <!--END CONTROLES DE VALIDITE DU CERTIFICAT-->
                <!-- BEGIN INFO COMPL -->
                <div class="panel panel-default" style="<%#($this->Data->getInfosSignature() instanceof Application\Service\Atexo\Signature\Atexo_Signature_InfosVo && $this->Data->getInfosSignature()->hasInformationsComplementaires()) ? '' : 'display:none' %>">
                    <com:TTranslate>INFOROMATIONS_COMPLEMANTAIRES_SIGNATURE</com:TTranslate>
                    <div class="panel panel-default bloc">
                        <div class="col-md-6 column column-xlarge">
                            <div class="title">
                                <strong> <com:TTranslate>INFOROMATIONS_COMPLEMANTAIRES_SIGNATURE</com:TTranslate></strong>
                            </div>
                            <div class="clearfix">
                                <com:TTranslate>CERTIFICAT_SIGNATURE</com:TTranslate> :
                                <%#($this->Data->getInfosSignature() instanceof Application\Service\Atexo\Signature\Atexo_Signature_InfosVo) ? $this->Data->getInfosSignature()->getInfoCertificatSignature() : ''%>
                                <span  style="display:<%#($this->Data->getInfosSignature() instanceof Application\Service\Atexo\Signature\Atexo_Signature_InfosVo && $this->Data->getInfosSignature()->getInfoBulleCertificatSignature() ) ? '' : 'none'%>" class="m-l-1">
                                    <i class="fa fa-question-circle" data-toggle="tooltip" title="<%#($this->Data->getInfosSignature() instanceof Application\Service\Atexo\Signature\Atexo_Signature_InfosVo) ? $this->Data->getInfosSignature()->getInfoBulleCertificatSignature() : ''%>"></i>
                                </span>
                            </div>
                            <div class="clearfix">
                                <com:TTranslate>FORMAT_SIGNATURE</com:TTranslate> :
                                <%#($this->Data->getInfosSignature() instanceof Application\Service\Atexo\Signature\Atexo_Signature_InfosVo) ? $this->Data->getInfosSignature()->getFormatSignature() : ''%>
                            </div>
                            <div class="clearfix">
                                <com:TTranslate>DATE_INDICATIVE</com:TTranslate> :
                                <%#($this->Data->getInfosSignature() instanceof Application\Service\Atexo\Signature\Atexo_Signature_InfosVo) ? $this->Data->getInfosSignature()->getDateIndicative() : ''%>
                            </div>
                            <div class="clearfix">
                                <com:TTranslate>JETON_SIGNATURE_HORODATE</com:TTranslate> :
                                <%#($this->Data->getInfosSignature() instanceof Application\Service\Atexo\Signature\Atexo_Signature_InfosVo) ? $this->Data->getInfosSignature()->getInfoHorodatageSignature() : ''%>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END INFO COMPL -->
            </prop:ItemTemplate>
        </com:TRepeater>
    </div>
    <!--Fin bloc detail-->
</div>
<!--Fin Formulaire-->
