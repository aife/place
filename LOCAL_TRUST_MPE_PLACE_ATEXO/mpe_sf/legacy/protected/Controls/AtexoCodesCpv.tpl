<script src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/js/vue-multiselect.min.js?k=<%= Application\Service\Atexo\Atexo_Config::getParameter('MEDIA_CACHE_KEY')%>"></script>

<com:TPanel id="panelModeAffichage" visible="false" cssClass="clearfix">
    <div class="line">
        <div class="<%=$this->getCssBlocLibelleCPV()%>">
            <com:TTranslate>TEXT_CODE_CPV</com:TTranslate>
            :
        </div>
        <com:THiddenField id="UrlRef"/>
        <com:THiddenField id="casRef"/>
        <com:THiddenField id="codeRefPrinc"/>
        <com:THiddenField id="codesRefSec"/>
        <com:THiddenField id="defineCodePrincipal" value="<%=Prado::localize('DEFINE_CODE_PRINCIPAL')%>"/>
        <com:TLabel id="scriptRef"/>
    </div>
</com:TPanel>
<com:TPanel id="panelModeMaj" visible="false" cssClass="clearfix col-md-12 m-b-1">
    <com:TActivePanel ID="panelCpv" Visible="<%=$this->getAfficherCodeCpv()%>" cssClass="clearfix row line">
        <div class="intitule-150 col-150">
            <label class="<%=$this->getCssBlocLibelleCPV()%>">
                <com:TLabel Text="<%=$this->titre%>"/>
                <com:TActiveLabel cssClass="champ-oblig" visible="<%=$this->isCodeCpvObligatoire()%>" Text="*"/>
            </label>
        </div>
        <div class="col-md-6 content-bloc">
            <div class="row">
                <div id="multiTreeContainer"
                     class="recherche-referentiel referentiel-cpv<%=$this->getCssBlocRechercheCPV()%> col-md-12">
                    <input id="typeSelectTreeView" type="hidden" value="arborescence">
                    <div id="cpv">
                        <multiselect
                                v-model="selectedCpv"
                                label="name"
                                track-by="code"
                                placeholder="<%=Prado::localize('MOTS_CLES_OU_CODE_CPV')%>"
                                open-direction="bottom"
                                :max="4"
                                :options="listeCpv"
                                :preserveSearch="true"
                                :multiple="true"
                                :searchable="true"
                                :loading="isLoading"
                                :internal-search="false"
                                :clear-on-select="true"
                                :close-on-select="true"
                                :options-limit="300"
                                :max-height="600"
                                :show-no-results="true"
                                :custom-label="customLabel"
                                :hide-selected="false"
                                :show-labels="false"
                                @search-change="asyncFind"
                                @select="onSelectOption"
                                @remove="onRemoveOption"
                                data-tnr="multiselectCPV">
                            <template slot="tag" slot-scope="slotProps"><span class="multiselect__tag"><span>{{ slotProps.option.code }} - {{ slotProps.option.label }}</span><i @click="slotProps.remove(slotProps.option)" aria-hidden="true" tabindex="1" class="multiselect__tag-icon"></i></span></template>
                            <template slot="option" slot-scope="props"><span :style="props.option.style">{{ props.option.code }} - {{ props.option.label }}</span></template>
                            <template slot="clear" slot-scope="props">
                                <div class="multiselect__clear" v-if="selectedCpv.length" @mousedown.prevent.stop="clearAll(props.search)"></div>
                            </template>
                            <span slot="noResult" ><span v-if="!isCategorySelected"><com:TTranslate>ERROR_CATEGORY_NOT_SELECT</com:TTranslate></span><span v-else><com:TTranslate>ERROR_INVALID_REQUEST</com:TTranslate> {{ searchquery }}</span></span>
                        </multiselect>
                    </div>
                    <com:TActiveImage id="ImgErrorCpv"  ImageUrl='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif'  Attributes.alt='Champ obligatoire' Style="display:none" />
                    <div class="spacer-mini"></div>
                    <com:TPanel  id="cpvWarning">
                    <div class="alert alert-warning  bloc-message form-bloc-conf msg-info">
                        <div class="alert-content">
                            <!--Debut Message type d'erreur general-->
                            <com:TTranslate>INFO_CPV_PRINCIPAL</com:TTranslate>
                            <ul>
                                <li><com:TTranslate>INFO_CPV_PRINCIPAL_FOURNITURES</com:TTranslate></li>
                                <li><com:TTranslate>INFO_CPV_PRINCIPAL_TRAVAUX</com:TTranslate></li>
                                <li><com:TTranslate>INFO_CPV_PRINCIPAL_SERVICES</com:TTranslate></li>
                            </ul>
                            <div class="spacer-mini"></div>
                            <!--Fin Message type d'erreur general-->
                        </div>
                    </div>
                    </com:TPanel>

                    <com:TRequiredFieldValidator
                            ValidationGroup="validerNotification"
                            ControlToValidate="cpvPrincipale"
                            Display="Dynamic"
                            Enabled="<%#($this->isCodeCpvObligatoire()) ? true : false %>"
                            ErrorMessage="<%=Prado::localize('TEXT_CODE_CPV')%>"
                            Text="<span title='<%=Prado::localize('TEXT_CODE_CPV')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' /></span>">
                        <prop:ClientSide.OnValidationError>
                            document.getElementById('divValidationSummaryEnregistrement').style.display='';
                        </prop:ClientSide.OnValidationError>
                    </com:TRequiredFieldValidator>

                    <com:TRequiredFieldValidator
                            ValidationGroup="validateInfosContrat"
                            ControlToValidate="cpvPrincipale"
                            Display="Dynamic"
                            Enabled="<%#($this->isCodeCpvObligatoire()) ? true : false %>"
                            ErrorMessage="<%=Prado::localize('TEXT_CODE_CPV')%>"
                            Text="<span title='<%=Prado::localize('TEXT_CODE_CPV')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' /></span>">
                        <prop:ClientSide.OnValidationError>
                            document.getElementById('divValidationSummary').style.display='';
                        </prop:ClientSide.OnValidationError>
                    </com:TRequiredFieldValidator>

                </div>
                <com:THiddenField id="cpvPrincipale" attributes.class="hidenGwtChosenPrincipal"/>
                <com:THiddenField id="cpvSecondaires" attributes.class="hidenGwtChosenSecondaires"/>
            </div>
        </div>
        <div class="col-md-3 switch-toggle switch-candy hide-agent">
            <!-- BEGIN BUTOON SWITCH -->
            <div class="btn-group btn-group-tooltip colors btn-group-justified" data-toggle="buttons">
                <div class="btn btn-primary active" Onclick="toggleTree('multiTreeContainer', 'arborescence')"
                     title="<%=Prado::localize('TEXT_VUE_SIMPLE')%>">
                    <div class="clearfix btn-groupe-item-text text-center" data-toggle="tooltip" data-html="true"
                         title="<div class='text-center'><%=Prado::localize('TEXT_VUE_SIMPLE')%></div>">
                        <com:TRadioButton GroupName="rechercheFloue" ID="cpvArborescence" Checked="true"
                                          attributes.title="<%=Prado::localize('TEXT_VUE_SIMPLE')%>"/>
                        <span class="sr-only"><com:TTranslate>TEXT_VUE_SIMPLE</com:TTranslate></span>
                        <i class="fa fa-list-ul"></i>
                    </div>
                </div>
                <div class="btn btn-primary" onclick="toggleTree('multiTreeContainer', 'simple')"
                     title="<%=Prado::localize('TEXT_VUE_ARBORESCENCE')%>">
                    <div class="clearfix btn-groupe-item-text text-center" data-toggle="tooltip" data-html="true"
                         title="<div class='text-center'><%=Prado::localize('TEXT_VUE_ARBORESCENCE')%></div>">
                        <com:TRadioButton
                                GroupName="rechercheFloue" ID="cpvSimple" Checked="false"
                                attributes.title="<%=Prado::localize('TEXT_VUE_ARBORESCENCE')%>"/>
                        <span class="sr-only"><com:TTranslate>TEXT_VUE_ARBORESCENCE</com:TTranslate></span>
                        <i class="fa fa-align-right"></i>
                    </div>
                </div>
            </div>
            <a href="#"><span class="sr-only">agent</span></a>
            <!-- END BUTOON SWITCH -->
        </div>
        <div class="col-md-3 switch-toggle switch-candy hide-entreprise">
            <input id="simple" name="view" checked="checked" type="radio">
            <label for="simple" onclick="toggleTree('multiTreeContainer', 'arborescence')" title="Vue simple"><i
                        class="icon icon-search"></i><span class="sr-only"><com:TTranslate>TEXT_VUE_SIMPLE</com:TTranslate></span></label>
            <input id="arborescence" name="view" type="radio">
            <label for="arborescence" onclick="toggleTree('multiTreeContainer', 'simple')" title="Vue arborescence"><i
                        class="icon icon-tree"></i><span class="sr-only"><com:TTranslate>TEXT_VUE_ARBORESCENCE</com:TTranslate></span></label>
            <a class="btn btn-default"></a>
        </div>
        <img alt="Info-bulle" class="picto-info-intitule img-question-hide hide-entreprise"
             onmouseout="cacheBulle('infosclauseCPV')"
             onmouseover="afficheBulle('infosclauseCPV', this)" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif">
        <div style="display: none; left: 714px; top: 350px;" onmouseout="mouseOutBulle();"
             onmouseover="mouseOverBulle();" class="info-bulle" id="infosclauseCPV">
            <div>
                <p>
                    <com:TTranslate>TEXT_INFOS_BUL_CPV_P1</com:TTranslate>
                    .
                </p>
                <p>
                    <com:TTranslate>TEXT_INFOS_BUL_CPV_P2</com:TTranslate>
                    :
                </p>
                <ul>
                    <li>-
                        <com:TTranslate>TEXT_TYPE_RECHERCHE_1</com:TTranslate>
                        ,
                    </li>
                    <li>-
                        <com:TTranslate>TEXT_TYPE_RECHERCHE_2</com:TTranslate>
                        ,
                    </li>
                    <li>-
                        <com:TTranslate>TEXT_TYPE_RECHERCHE_3</com:TTranslate>
                        .
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-md-1 p-l-0 hide-agent">
            <span data-target="#" data-toggle="tooltip" data-placement="top" data-html="true"
                  data-original-title="<p><%= $this->translateHtmlspecialchars('TEXT_INFOS_BUL_CPV_P1')%>.</p><p><%= $this->translateHtmlspecialchars('TEXT_INFOS_BUL_CPV_P2')%> :</p><ul style='padding-left: 20px;'><li><%= $this->translateHtmlspecialchars('TEXT_TYPE_RECHERCHE_1')%> ,</li><li><%= $this->translateHtmlspecialchars('TEXT_TYPE_RECHERCHE_2')%> ,</li><li><%= $this->translateHtmlspecialchars('TEXT_TYPE_RECHERCHE_3')%> .</li></ul>">
                <i class="fa fa-question-circle text-info p-t-1"></i>
            </span>
        </div>
    </com:TActivePanel>
    <com:TActiveLabel id="labelScriptCPV" style="display:none"/>
    <com:TActiveLabel id="labelScriptCPVLang" style="display:none"/>
    <com:TActiveLabel id="labelSourceScriptCPV" style="display:none"/>
    <com:TActiveLabel id="labelScriptCategorieParent" style="display:none"/>
    <com:TActiveLabel id="labelScriptCpvsParent" style="display:none"/>
    <div class="spacer-small"></div>
</com:TPanel>

<script src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/js/polyfill.min.js?k=<%= Application\Service\Atexo\Atexo_Config::getParameter('MEDIA_CACHE_KEY')%>"></script>
<script src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/js/axios.min.js?k=<%= Application\Service\Atexo\Atexo_Config::getParameter('MEDIA_CACHE_KEY')%>"></script>
<script src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/js/vue-cpv.js?k=<%= Application\Service\Atexo\Atexo_Config::getParameter('MEDIA_CACHE_KEY')%>"></script>
<script>
    function toggleTree(container, id) {
        typeSelectTreeView=document.getElementById('typeSelectTreeView');
        if(typeSelectTreeView.value!=id) {
            typeSelectTreeView.value= id;
        }
    }
</script>
