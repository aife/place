<com:TRepeater ID="RepeaterReferentielZoneText">
	<prop:ItemTemplate>
			<com:TPanel id="idPanel" cssClass="form-field" > 
			    	<com:TPanel cssClass="top">
				    	<span class="left">&nbsp;</span>
				    	<span class="right">&nbsp;</span>
							<span class="title">
									<com:TActiveCheckBox id="refTexte" Attributes.title="<%#Prado::localize($this->Data->getCodeLibelle())%>" cssClass="check" Checked="false" 	Attributes.onclick="toggleMenu('<%= $this->divRefTexte->getClientId() %>');"/>
									<com:TLabel id="texteCheck"  text="<%#Prado::localize($this->Data->getCodeLibelle())%>" />
							</span>
					</com:TPanel>
			    	<com:TPanel cssClass="content">
						<com:TPanel ID="divRefTexte" CssClass="line" style="display:none;" >
						<com:TPanel visible="<%#$this->parent->parent->getLot()%>">
							<com:AtexoReferentielCheckLots id="idLot" lot="1" ref="<%#$this->Data%>"/>
						</com:TPanel>
							<com:TPanel cssClass="<%#($this->parent->parent->getCssClass())%>" visible="<%#!$this->parent->parent->getLot()%>"><%#Prado::localize($this->Data->getCodeLibelle())%> <span <%#($this->Data->getObligatoire()&& $this->parent->parent->getObligatoire()) ? "class='champ-oblig'" : "style='display:none'" %> >*</span>:</com:TPanel>
								<com:TTextBox id="idReferentielZoneText"
											  Attributes.onblur ="<%#(strtolower($this->Data->getDataType()) == 'montant')?'formatterMontant(\''.$this->idReferentielZoneText->getClientId().'\');':''%>"
											  Attributes.OnChange="replaceCaracSpecial(this);"
											  TextMode="<%#$this->Data->getModeModification()%>"
											  CssClass="long" visible="<%#(!$this->parent->parent->getLabel() && $this->parent->parent->getType() && !$this->parent->parent->getLot())%>"/>
								<com:TLabel id="iDLibelle" visible="false" text="<%#$this->Data->getId()%>" />
					         	<com:TLabel id="codeLibelle" visible="false" text="<%#$this->Data->getCodeLibelle()%>" />
					         	<com:TLabel id="labelReferentielZoneText"  text="" visible="<%# $this->parent->parent->getLabel()%>"/>
					         	<com:TLabel id="numLot"  text="<%#$this->parent->parent->getLot()%>" visible="false"/>
					         	<com:THiddenField id="oldValue" />
								<com:THiddenField id="typeData" value="<%#$this->Data->getDataType()%>" />
								<com:TRequiredFieldValidator 
								Id="validatorAtexoText2"
								ControlToValidate="idReferentielZoneText"
								ValidationGroup="<%#($this->parent->parent->getValidationGroup())%>" 
								Display="Dynamic"
								EnableClientScript="true"
								ErrorMessage="<%#Prado::localize($this->Data->getCodeLibelle())%>"
								visible="<%#($this->Data->getObligatoire()&& $this->parent->parent->getObligatoire()) ? 'true' :'false'%>"
								Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
									<prop:ClientSide.OnValidationError>
										blockErreur = '<%#($this->parent->parent->getValidationSummary())%>';
										if(document.getElementById(blockErreur))
								 			document.getElementById(blockErreur).style.display='';
								 		if(document.getElementById('ctl0_CONTENU_PAGE_buttonSave'))
								 			document.getElementById('ctl0_CONTENU_PAGE_buttonSave').style.display='';
									</prop:ClientSide.OnValidationError>   
								</com:TRequiredFieldValidator>
						</com:TPanel>
						<com:TPanel  cssClass="breaker">
						</com:TPanel >
					</com:TPanel >
					<com:TPanel  cssClass="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span>
					</com:TPanel >
		</com:TPanel >
	</prop:ItemTemplate>
</com:TRepeater>
