<?php

namespace Application\Controls;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonTGroupementEntreprise;
use Application\Propel\Mpe\CommonTMembreGroupementEntreprise;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Entreprise;
use Application\Service\Atexo\Atexo_Groupement;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Etablissement;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_EtablissementVo;
use Exception;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * Le bloc groupement entreprise.
 *
 * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
 *
 * @version 1.0
 *
 * @since 2015-place
 *
 * @copyright Atexo 2016
 */
class AtexoBlocGroupementEntreprise extends MpeTTemplateControl
{
    protected $groupement;
    protected $idEntreprise;
    protected $idEtablissement;
    protected bool $modeReadOnly = false;
    protected bool $modeAgent = false;

    /**
     * @return mixed
     */
    public function getGroupement()
    {
        return $this->getViewState('groupement');
    }

    /**
     * @param mixed $groupeent
     */
    public function setGroupement($groupement)
    {
        $this->setViewState('groupement', $groupement);
    }

    /**
     * @return mixed
     */
    public function getIdEntreprise()
    {
        return $this->getViewState('idEntreprise');
    }

    /**
     * @param mixed $idEntreprise
     */
    public function setIdEntreprise($idEntreprise)
    {
        $this->setViewState('idEntreprise', $idEntreprise);
    }

    /**
     * @return mixed
     */
    public function getIdEtablissement()
    {
        return $this->getViewState('idEtablissement');
    }

    /**
     * @param mixed $idEtablissement
     */
    public function setIdEtablissement($idEtablissement)
    {
        $this->setViewState('idEtablissement', $idEtablissement);
    }

    /**
     * @return bool
     */
    public function isModeReadOnly()
    {
        return $this->modeReadOnly;
    }

    /**
     * @param bool $modeReadOnly
     */
    public function setModeReadOnly($modeReadOnly)
    {
        $this->modeReadOnly = $modeReadOnly;
    }

    /**
     * @return bool
     */
    public function isModeAgent()
    {
        return $this->modeAgent;
    }

    /**
     * @param bool $modeAgent
     */
    public function setModeAgent($modeAgent)
    {
        $this->modeAgent = $modeAgent;
    }

    /**
     * Permet d'initialiser le composoant.
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function initialiserComposant()
    {
        $this->panelBlocMessages->setDisplay('None');
        if ($this->isModeAgent()) {
            $this->layerRepeaterGrouepement->setDisplay('Dynamic');
        }
    }

    /**
     * Permet d'ajouter un co-traitant.
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function ajouterCoTraitant($sender, $param)
    {
        $membreExiste = false;
        $groupement = $this->getGroupement();

        if (!$groupement instanceof CommonTGroupementEntreprise) {
            $groupement = new CommonTGroupementEntreprise();
            $groupement->setIdTypeGroupement(self::getIdTypeGroupement());
            $membreMandataire = new CommonTMembreGroupementEntreprise();
            $membreMandataire->setIdEntreprise($this->getIdEntreprise());
            $membreMandataire->setIdEtablissement($this->getIdEtablissement());
            $membreMandataire->setIdRoleJuridique(Atexo_Config::getParameter('ID_ROLE_JURIDIQUE_MANDATAIRE'));
            $membreMandataire->setHasChild(false);
            $groupement->addOneMembres($membreMandataire);
            $this->setGroupement($groupement);
        }

        if ($groupement instanceof CommonTGroupementEntreprise) {
            $siren = $this->siren->getSafeText();
            $siret = $this->siret->getSafeText();
            self::ajouterMembreToGroupement($membreExiste, Atexo_Config::getParameter('ID_ROLE_JURIDIQUE_CO_TRAITANT'), $param, $siren, $siret);
            if ($membreExiste) {
                $this->panelBlocMessages->setDisplay('Dynamic');
                $this->messageErreur->setMessage(Prado::Localize('SIRET_EXISTE_DEJA'));
                $this->panelBlocMessages->render($param->getNewWriter());
            } else {
                self::viderAjoutCoTraitant();
                $this->panelBlocMessages->setDisplay('None');
                $this->setGroupement($groupement);
                $this->fillRepeaterGroupement();
            }
        }
    }

    /**
     * Permet d'ajouter un sous-traitant.
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function ajouterLigneSousTraitant($sender, $param)
    {
        $groupement = $this->getGroupement();
        if ($groupement instanceof CommonTGroupementEntreprise) {
            $membres = $groupement->getMembres();
            $sousTraitant = new CommonTMembreGroupementEntreprise();
            $idEntrepriseParent = $param->CommandParameter;
            $membreParent = $membres[$idEntrepriseParent];
            if ($membreParent instanceof CommonTMembreGroupementEntreprise) {
                $sousTraitant->setIdEntrepriseParent($idEntrepriseParent);
                $sousTraitant->setIdRoleJuridique(Atexo_Config::getParameter('ID_ROLE_JURIDIQUE_SOUS_TRAITANT'));
                $sousTraitant->setIdEntreprise(0);
                $membreParent->addSousMembres($sousTraitant);
                $membreParent->setHasChild(true);
                $membres[$idEntrepriseParent] = $membreParent;
                $groupement->setMembres($membres);
                $this->setGroupement($groupement);
                $this->fillRepeaterGroupement();
                $this->scriptJs->text = "<script>J('.placehold').placeholder({ customClass: 'my-placeholder' });</script>";
            }
        }
    }

    /**
     * Permet d'ajouter un sous-traitant.
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function ajouterSousTraitant($sender, $param)
    {
        $siren = null;
        $siret = null;
        $parmsSousTraitant = (array) $param->CallbackParameter;
        $membreExiste = false;
        if (is_array($parmsSousTraitant)) {
            // print_r($parmsSousTraitant);exit;
            $idEntrepriseParent = $parmsSousTraitant['idEntrepriseParent'];
            $idEntreprise = $parmsSousTraitant['indexElement'];
            if ($idEntrepriseParent) {
                foreach ($this->repeaterGroupement->Items as $itemGroupement) {
                    foreach ($itemGroupement->repeaterMembres->Items as $itemMembre) {
                        if ($itemMembre->idEntreprise->value == $idEntrepriseParent) {
                            foreach ($itemMembre->repeaterSousMembres->Items as $itemSousMembre) {
                                if ($itemSousMembre->idEntreprise->value == $idEntreprise) {
                                    $siren = $itemSousMembre->siren->getSafeText();
                                    $siret = $itemSousMembre->siret->getSafeText();
                                }
                            }
                        }
                    }
                }

                self::ajouterMembreToGroupement($membreExiste, Atexo_Config::getParameter('ID_ROLE_JURIDIQUE_SOUS_TRAITANT'), $param, $siren, $siret, $idEntrepriseParent);
                if ($membreExiste) {
                    $this->panelBlocMessages->setDisplay('Dynamic');
                    $this->messageErreur->setMessage(Prado::Localize('SIRET_EXISTE_DEJA'));
                    $this->panelBlocMessages->render($param->getNewWriter());
                } else {
                    self::supprimerSousTraitant($idEntrepriseParent, $idEntreprise);
                    $this->fillRepeaterGroupement();
                    $this->panelBlocMessages->setDisplay('None');
                }
            }
        }
    }

    /**
     * Permet de supprimer un sous traitant.
     *
     * @param $idEntrepriseParent
     * @param $idEntrepriseSousTraitant
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function supprimerSousTraitant($idEntrepriseParent, $idEntrepriseSousTraitant)
    {
        $groupement = $this->getGroupement();
        if ($groupement instanceof CommonTGroupementEntreprise) {
            $membres = $groupement->getMembres();
            $membreParent = $membres[$idEntrepriseParent];
            if ($membreParent instanceof CommonTMembreGroupementEntreprise) {
                $sousMembres = $membreParent->getSousMembres();
                if (is_array($sousMembres)) {
                    unset($sousMembres[$idEntrepriseSousTraitant]);
                    $membreParent->setSousMembres($sousMembres);
                    $membres[$idEntrepriseParent] = $membreParent;
                    $groupement->setMembres($membres);
                    $this->setGroupement($groupement);
                }
            }
        }
    }

    /**
     * Permet d'ajouter un membre co/sous-traitant.
     *
     * @param $idRoleJuridique
     * @param $membreExiste
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function ajouterMembreToGroupement(&$membreExiste, $idRoleJuridique, $param, $siren, $siret, $idEntrepriseParent = null)
    {
        $groupement = $this->getGroupement();
        if ($groupement instanceof CommonTGroupementEntreprise) {
            $membres = $groupement->getMembres();

            $coTraitant = self::getCoSousTraitant($idRoleJuridique, $membres, $membreExiste, $siren, $siret);

            if (!$membreExiste && ($coTraitant instanceof CommonTMembreGroupementEntreprise)) {
                if ($idRoleJuridique == Atexo_Config::getParameter('ID_ROLE_JURIDIQUE_CO_TRAITANT')) {
                    $coTraitant->setHasChild(false);
                    $groupement->addOneMembres($coTraitant);
                } elseif ($idRoleJuridique == Atexo_Config::getParameter('ID_ROLE_JURIDIQUE_SOUS_TRAITANT')) {
                    $membreParent = $membres[$idEntrepriseParent];
                    if ($membreParent instanceof CommonTMembreGroupementEntreprise) {
                        $coTraitant->setIdEntrepriseParent($idEntrepriseParent);
                        $membreParent->addSousMembres($coTraitant);
                        $membreParent->setHasChild(true);
                        $membres[$idEntrepriseParent] = $membreParent;
                        $groupement->setMembres($membres);
                    }
                }
            }
        }
    }

    /**
     * Permet de remplir le repeater de groupement.
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function fillRepeaterGroupement()
    {
        $groupement = $this->getGroupement();
        $dataSource = [];
        if ($groupement instanceof CommonTGroupementEntreprise) {
            $dataSource = [$groupement];
        }
        $this->repeaterGroupement->DataSource = $dataSource;
        $this->repeaterGroupement->DataBind();
    }

    /**
     * Permet de retourner l'id de type groupement choisi.
     *
     * @return int l'id de type de groupement
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function getIdTypeGroupement()
    {
        if ($this->groupementSolidaire->checked) {
            return Atexo_Config::getParameter('ID_TYPE_GROUPEMENT_SOLIDAIRE');
        }
        if ($this->groupementConjointNonSolidaire->checked) {
            return Atexo_Config::getParameter('ID_TYPE_GROUPEMENT_CONJOINT_NON_SOLIDAIRE');
        }
        if ($this->groupementConjointSolidaire->checked) {
            return Atexo_Config::getParameter('ID_TYPE_GROUPEMENT_CONJOINT_SOLIDAIRE');
        }
    }

    /**
     * Permet de changer le libelle de type de groupement qand on change le type.
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function typeGroupementChanged()
    {
        $groupement = $this->getGroupement();
        if ($groupement instanceof CommonTGroupementEntreprise) {
            $groupement->setIdTypeGroupement(self::getIdTypeGroupement());
            $this->setGroupement($groupement);
            $this->fillRepeaterGroupement();
        }
    }

    public function getCoSousTraitant($idRoleJuridique, $membres, &$membreExiste, $siren, $siret)
    {
        $company = null;
        $membreGroupement = null;
        if ($siren) {
            $company = (new Atexo_Entreprise())->retrieveCompanyVoBySiren($siren);
            if (!$company) {
                //SynchronisationSGMAP
                if (Atexo_Config::getParameter('ACTIVER_SYNCHRONISATION_SGMAP_LORS_DE_CREATION_ENTREPRISE') && Atexo_Module::isEnabled('SynchronisationSGMAP')) {
                    //$company = Atexo_Entreprise::recupererEntrepriseApiGouvEntreprise($siren, true);
                    $company = Atexo_Entreprise::synchroEntrepriseAvecApiGouvEntreprise($siren, $company);
                    if ($company) {
                        $listeEtablissements = $company->getEtablissements();
                        $newEtablissement = is_array($listeEtablissements) ? array_pop($listeEtablissements) : '';
                        if ($company->getNicsiege() != $siret) {
                            $newEtablissement = (new Atexo_Entreprise_Etablissement())->synchroEtablissementAvecApiGouvEntreprise($siren.$siret, $newEtablissement);
                            if ($newEtablissement) {
                                $company->setEtablissements($newEtablissement);
                            }
                        }
                        $company->setListeEtablissements($company->getEtablissements());
                        $company = (new Atexo_Entreprise())->save($company);
                    }
                }
            }
        }
        if ($company) {
            $etablissementVo = (new Atexo_Entreprise_Etablissement())->getEtablissementVoFromEntreprise($company, $siret);
            $membreExiste = (new Atexo_Groupement())->isMembreGroupementExiste($membres, $company->getId());
            if (!$membreExiste) {
                $membreGroupement = new CommonTMembreGroupementEntreprise();
                $membreGroupement->setIdEntreprise($company->getId());
                if ($etablissementVo instanceof Atexo_Entreprise_EtablissementVo) {
                    $membreGroupement->setIdEtablissement($etablissementVo->getIdEtablissement());
                }
                $membreGroupement->setIdRoleJuridique($idRoleJuridique);
            }
        }

        return $membreGroupement;
    }

    /**
     * Permet de retourner le nombre des membres de groupement.
     *
     * @return int nombre des membre de groupement
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function getNombreMembreGroupement()
    {
        $groupement = $this->getGroupement();
        if ($groupement instanceof CommonTGroupementEntreprise) {
            return is_countable($groupement->getMembres()) ? count($groupement->getMembres()) : 0;
        }

        return 0;
    }

    /**
     * Permet de retourner le nombre des sous membres de groupement.
     *
     * @param int $idEntrepriseParent l'id de l'entrperise parent
     *
     * @return int nombre des sous membre de groupement
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function getNombreSousMembreGroupement($idEntrepriseParent)
    {
        $groupement = $this->getGroupement();
        if ($groupement instanceof CommonTGroupementEntreprise) {
            $membres = $groupement->getMembres();
            if (is_array($membres)) {
                $membre = $membres[$idEntrepriseParent];
                if ($membre instanceof CommonTMembreGroupementEntreprise) {
                    return is_countable($membre->getSousMembres()) ? count($membre->getSousMembres()) : 0;
                }
            }
        }

        return 0;
    }

    /**
     * Permet de supprimer un sous traitant.
     *
     * @param $sender
     * @param $param
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function deleteSousTraitant($sender, $param)
    {
        $idEntreprises = $param->CommandParameter;
        if (is_array($idEntreprises)) {
            $idEntrepriseToDelete = $idEntreprises['idEntreprise'];
            $idEntrepriseParent = $idEntreprises['idEntrepriseParent'];
            self::supprimerSousTraitant($idEntrepriseParent, $idEntrepriseToDelete);
            $this->fillRepeaterGroupement();
        }
    }

    /**
     * Permet de supprimer un co traitant.
     *
     * @param $sender
     * @param $param
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function supprimerCoTraitant($sender, $param)
    {
        $idEntrepriseToDelete = $param->CommandParameter;
        $groupement = $this->getGroupement();
        if ($groupement instanceof CommonTGroupementEntreprise) {
            $membres = $groupement->getMembres();
            if (is_array($membres)) {
                unset($membres[$idEntrepriseToDelete]);
                if (1 == count($membres)) {
                    $groupement = null;
                } else {
                    $groupement->setMembres($membres);
                }
            }
            $this->setGroupement($groupement);
            $this->fillRepeaterGroupement();
        }
    }

    /**
     * Permet de vider les champs.
     *
     * @param $sender
     * @param $param
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function viderAjoutCoTraitant()
    {
        $this->siren->Text = '';
        $this->siret->Text = '';
    }

    /**
     * Permet de sauvgarder le groupement entreprise.
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function saveGroupementEntreprise($idOffre)
    {
        $connexion = null;
        if ($this->groupementOui->checked) {
            try {
                if ($this->declarerGroupementOui->checked) {
                    $groupement = $this->getGroupement();
                } else {
                    $groupement = new CommonTGroupementEntreprise();
                    $groupement->setIdTypeGroupement(self::getIdTypeGroupement());
                }
                if ($groupement instanceof CommonTGroupementEntreprise) {
                    $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                    $connexion->beginTransaction();
                    $membresGroupement = $groupement->getMembres();
                    $groupement->setIdOffre($idOffre);
                    $groupement->setDateCreation(date('Y-m-d H:i:s'));
                    $groupement->save($connexion);
                    if (is_array($membresGroupement) && count($membresGroupement)) {
                        foreach ($membresGroupement as $membre) {
                            if ($membre instanceof CommonTMembreGroupementEntreprise) {
                                $sousMembres = $membre->getSousMembres();
                                if (is_array($membresGroupement) && count($membresGroupement)) {
                                    foreach ($sousMembres as $sousMembre) {
                                        if ($sousMembre instanceof CommonTMembreGroupementEntreprise) {
                                            $sousMembre->setCommonTMembreGroupementEntrepriseRelatedByIdMembreParent($membre);
                                            $sousMembre->save($connexion);
                                        }
                                    }
                                }
                                $membre->setIdGroupementEntreprise($groupement->getIdGroupementEntreprise());
                                $membre->save($connexion);
                            }
                        }
                    }
                    $connexion->commit();
                }
            } catch (Exception $e) {
                $connexion->rollBack();
                Prado::log("Erreur lors d'enregistrement du groupement  :  - Id enteprise: ".Atexo_CurrentUser::getIdEntreprise().
                    ' - Id Inscrit: '.Atexo_CurrentUser::getIdInscrit()."\n".$e, TLogger::ERROR, 'Atexo.controls.AtexoBlocGroupementEntreprise.php');
            }
        }
    }

    protected function getLienFicheFournisseur($idOffre, $idEntreprise)
    {
        $url = '';
        if ($this->isModeAgent()) {
            $url = "javascript:popUpSetSize('index.php?page=Agent.DetailEntreprise&callFrom=agent&idEntreprise=".$idEntreprise.'&idO='.$idOffre.'&type=E'."','1060px','800px','yes');";
        }

        return $url;
    }
}
