<?php

namespace Application\Controls;

use Application\Service\Atexo\Controller\Atexo_Controller_Front;
use Prado\Prado;

/**
 * commentaires.
 *
 * @author Mouslim MITALI<mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PictoEnvoyerInactive extends PictoEnvoyer
{
    public bool $_activate = true;

    public function onLoad($param)
    {
        $this->Attributes->onclick = 'return false';
        if (true == $this->_activate) {
            $this->setImageUrl(Atexo_Controller_Front::t().'/images/picto-envoyer-inactive.gif');
        }
        $this->setAlternateText(Prado::localize('DEFINE_TEXT_ENVOYER'));
        $this->setToolTip(Prado::localize('DEFINE_TEXT_ENVOYER'));
    }

    public function getActivate()
    {
        return $this->_activate;
    }

    public function setActivate($bool)
    {
        $this->_activate = $bool;
    }
}
