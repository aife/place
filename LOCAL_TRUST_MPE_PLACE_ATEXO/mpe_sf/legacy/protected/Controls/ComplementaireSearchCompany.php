<?php

namespace Application\Controls;

use Application\Service\Atexo\DonneesComplementairesEntreprise\Atexo_DonneesComplementairesEntreprise_DonneesComplementairesEntreprise;

/**
 * commentaires.
 *
 * @author mouslim MITALI <mouslim.MITALI@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class ComplementaireSearchCompany extends MpeTPage
{
    public function displayDomaines()
    {
        $this->DomainesActivites->displayDomaines();
        $this->DomainesActivites->hideToggleMenu();
    }

    public function getIdsEntreprises()
    {
        $entreprises = [];
        if ($this->restreindreRecherche->Checked) {
            if ($this->unDomaineAuMoins->Checked) {
                $entreprises = (new Atexo_DonneesComplementairesEntreprise_DonneesComplementairesEntreprise())->entreprisesAyantChoisisDomaine();
            } else {
                $listeDomaines = $this->DomainesActivites->getCheckedDomaines();
                $entreprises = (new Atexo_DonneesComplementairesEntreprise_DonneesComplementairesEntreprise())->entreprisesAyantChoisisDomaine($listeDomaines);
            }
        }

        return $entreprises;
    }

    public function getListeDomainesActivites()
    {
        return $this->DomainesActivites->getCheckedDomaines();
    }
}
