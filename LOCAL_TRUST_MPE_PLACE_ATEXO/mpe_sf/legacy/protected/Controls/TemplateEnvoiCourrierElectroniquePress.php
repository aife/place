<?php

namespace Application\Controls;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonAdresseFacturationJal;
use Application\Propel\Mpe\CommonAdresseFacturationJalPeer;
use Application\Propel\Mpe\CommonAnnoncePress;
use Application\Propel\Mpe\CommonAnnoncePressPieceJointe;
use Application\Propel\Mpe\CommonAnnoncePressPieceJointePeer;
use Application\Propel\Mpe\CommonAvisPub;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonDestinataireAnnoncePress;
use Application\Propel\Mpe\CommonDestinatairePub;
use Application\Propel\Mpe\CommonJAL;
use Application\Service\Atexo\AnnoncePress\Press;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Publicite\Atexo_Publicite_AvisPub;
use Prado\Prado;

/**
 * Classe EnvoiCourrierElectroniquePress.
 *
 * @author HANNAD Hajar <hajar.hannad@atexo.com>
 * @copyright Atexo 2011
 *
 * @version 1.0
 *
 * @since MPE-4.0
 *
 * @category Atexo
 */
class TemplateEnvoiCourrierElectroniquePress extends MpeTPage
{
    public $_annoncePress;
    public $_refConsultation;
    public $_org;
    public $_idPress;
    public $_check;
    public $_mail;
    public bool $_postBack = false;
    public $_calledFrom;
    public ?int $_nbrAdresseFacturation = null;
    public $_avisPub;

    public function setPostBack($value)
    {
        $this->_postBack = $value;
    }

    public function getPostBack()
    {
        return $this->_postBack;
    }

    public function setCalledFrom($value)
    {
        $this->_calledFrom = $value;
    }

    public function getCalledFrom()
    {
        return $this->_calledFrom;
    }

    public function setRefConsultation($value)
    {
        $this->_refConsultation = $value;
    }

    public function getRefConsultation()
    {
        return $this->_refConsultation;
    }

    public function setOrg($value)
    {
        $this->_org = $value;
    }

    public function getOrg()
    {
        return $this->_org;
    }

    public function setAnnoncePress($value)
    {
        $this->_annoncePress = $value;
    }

    public function getAnnoncePress()
    {
        return $this->_annoncePress;
    }

    public function setIdPress($value)
    {
        $this->_idPress = $value;
    }

    public function getIdPress()
    {
        return $this->_idPress;
    }

    public function setCheck($value)
    {
        $this->_check = $value;
    }

    public function getCheck()
    {
        return $this->_check;
    }

    public function setMail($value)
    {
        $this->_mail = $value;
    }

    public function getMail()
    {
        return $this->_mail;
    }

    public function setAvisPub($value)
    {
        $this->_avisPub = $value;
    }

    public function getAvisPub()
    {
        return $this->getViewState('idAvisPub') ?: $this->_avisPub;
    }

    public function onLoad($param)
    {
        if ($_GET['close']) {
            $this->scriptJs->text =  "<script type='text/javascript'>window.close();</script>";
        }
        $dataSource = null;
								if (!$this->Page->IsPostBack) {
            $PJ = [];
            $c = new Criteria();
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));

            $this->setRefConsultation((new Atexo_Publicite_AvisPub())->getRefConsultationByIdDestPub(Atexo_Util::atexoHtmlEntities($_GET['idDest']), $this->_org));
            $consultation = null;
            if ((new Atexo_Publicite_AvisPub())->isAccesGestionPubMpe()) {
                $criteria = new Atexo_Consultation_CriteriaVo();
                $criteria->setcalledFromPortail(false);
                $criteria->setIdService(self::getServiceConsultation(Atexo_Util::atexoHtmlEntities($_GET['id'])));
                $criteria->setAcronymeOrganisme($this->_org);
                $criteria->setIdReference($this->_refConsultation);
                $criteria->setConnectedAgentId(Atexo_CurrentUser::getId());
                $arrayConsultation = (new Atexo_Consultation())->search($criteria);
                $consultation = $arrayConsultation[0];
            } else {
                $consultation = (new Atexo_Consultation())->retrieveConsultation($this->_refConsultation, $this->_org);
            }
            if ($consultation instanceof CommonConsultation) {
                $this->remplirListeMessage($this->_org, $consultation);

                $annonceObject = (new Press())->retreiveListAnnoncPressByRefIdDestPub(Atexo_Util::atexoHtmlEntities($_GET['idDest']), $this->_org);
                if (isset($_GET['consult']) && $annonceObject instanceof CommonAnnoncePress) {
                    $this->remplirRepeaterAdresseFacturation($annonceObject->getIdAdresseFacturation(), $this->_org, $consultation->getServiceId());
                } else {
                    $this->remplirRepeaterAdresseFacturation(null, $this->_org, $consultation->getServiceId());
                }
                // Cas de modification
                if ($annonceObject) {
                    $dest = (new Atexo_Publicite_AvisPub())->getDestinatairePub(Atexo_Util::atexoHtmlEntities($_GET['idDest']));
                    if ($dest) {
                        $avis = (new Atexo_Publicite_AvisPub())->retreiveAvis($dest->getIdAvis(), $this->_org);
                        if ($avis instanceof CommonAvisPub) {
                            $this->_avisPub = $avis->getId();
                            $this->setViewState('idAvisPub', $this->_avisPub);
                            $this->setViewState('avisPub', $avis);
                        }
                    }

                    $this->_annoncePress = $annonceObject;
                    $this->_idPress = $annonceObject->getId();
                    $this->objet->Text = $annonceObject->getObjet();
                    $this->textMail->Text = $annonceObject->getTexte();
                    if ($annonceObject->getOptionEnvoi() == Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_AVEC_AR')) {
                        $this->courrierElectroniqueContenuIntegralAR->setChecked(true);
                    } elseif ($annonceObject->getOptionEnvoi() == Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_SIMPLE')) {
                        $this->courrierElectroniqueSimple->setChecked(true);
                    } elseif ($annonceObject->getOptionEnvoi() == Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_AVEC_LIEN')) {
                        $this->courrierElectroniqueUniquementLienTelechargementObligatoire->setChecked(true);
                    }
                    $avis = $this->getViewState('avisPub');
                    foreach ($this->RepeaterAdresseFacturation->Items as $Item) {
                        //Cocher la case pour l'adresse de facturation
                        if ($Item->idAdresseFact->text == $annonceObject->getIdAdresseFacturation()) {
                            $Item->idRadioBouton->setChecked(true);
                        }
                        //Cocher la case pour le logo lorsque ce dernier est joint au mail
                        if ('true' == $Item->idRadioBouton->getChecked() && $avis instanceof CommonAvisPub) {
                            if ($avis->getIdBlobLogo()) {
                                $Item->ajoutLogo->setChecked(true);
                            }
                        }
                    }

                    $PJ = $annonceObject->getCommonAnnoncePressPieceJointes($c, $connexionCom);
                    $parentService = Atexo_EntityPurchase::getArParentsId(
                        $consultation->getServiceId(),
                        $this->_org,
                        true
                    );

                    //Liste Destinataires JAL
                    if (isset($_GET['consult'])) {
                        $dataSource = (new CommonDestinataireAnnoncePress())
                            ->getEmailDestForTableDest($annonceObject->getId(), $this->_org, $parentService);
                    } else {
                        $dataSource = (new CommonJAL())->getEmailDestForSendPress(
                            $this->_org,
                            $parentService,
                            $consultation->getServiceId()
                        );
                    }
                    $this->remplirTableauPJ($PJ);
                }
                // Cas d'une création d'une nouvelle press
                else {
                    $this->_annoncePress = new CommonAnnoncePress();
                    $this->_annoncePress->setOrganisme($this->_org);
                    $this->_annoncePress->setIdDestPress(Atexo_Util::atexoHtmlEntities($_GET['idDest']));
                    $this->_annoncePress->save($connexionCom);
                    $this->_idPress = $this->_annoncePress->getId();
                    $this->courrierElectroniqueSimple->setChecked(true);
                    $parentService = Atexo_EntityPurchase::getArParentsId(
                        $consultation->getServiceId(),
                        $this->_org,
                        true
                    );

                    $dataSource = (new CommonJAL())
                        ->getEmailDestForSendPress(
                            $this->_org,
                            $parentService,
                            $consultation->getServiceId()
                        );
                    if (isset($_GET['idDest'])) {
                        $dest = (new Atexo_Publicite_AvisPub())->getDestinatairePub(Atexo_Util::atexoHtmlEntities($_GET['idDest']));
                        if ($dest) {
                            $avis = (new Atexo_Publicite_AvisPub())->retreiveAvis($dest->getIdAvis(), $this->_org);
                            if ($avis instanceof CommonAvisPub) {
                                $this->_avisPub = $avis->getId();
                                $this->setViewState('idAvisPub', $this->_avisPub);
                                $this->setViewState('avisPub', $avis);
                                $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
                                $modele = (new Atexo_Publicite_AvisPub())->getModeleDocumentPresse($avis->getTypeAvis());
                                if ($modele) {
                                    //Génération du document à envoyer à la presse
                                    $tableauContenuGeneration = (new Atexo_Publicite_AvisPub())->genererAvisPressePortail($modele, $connexionCom, $avis->getId(), $consultation);
                                    $piece = new CommonAnnoncePressPieceJointe();
                                    $piece->setOrganisme($this->_annoncePress->getOrganisme());
                                    $piece->setIdAnnoncePress($this->_idPress);
                                    $piece->setNomFichier($tableauContenuGeneration[2]);

                                    $atexoBlob = new Atexo_Blob();
                                    $blobContent = $atexoBlob->return_blob_to_client($tableauContenuGeneration[0], $avis->getOrganisme());
                                    $tmpFile = Atexo_Config::getParameter('COMMON_TMP').'/'.session_name().session_id().time().$tableauContenuGeneration[0].'.pdf';
                                    $fp = fopen($tmpFile, 'w');
                                    fwrite($fp, $blobContent);
                                    fclose($fp);

                                    $piece->setTaille(filesize($tmpFile));
                                    $piece->setFichierGenere(1);
                                    $piece->save($connexionCom);
                                    //Début ajout de l'id du fichier presse à l'avis global
                                    $avis->setIdAvisPresse($piece->getId());
                                    $avis->save($connexionCom);
                                    //Fin ajout de l'id du fichier presse à l'avis global

                                    //Affichage des pièces jointes au mail
                                    $PJ = [];
                                    $PJ = $this->_annoncePress->getCommonAnnoncePressPieceJointes($c, $connexionCom);
                                    $this->remplirTableauPJ($PJ);
                                }
                            }
                        }
                    }
                }
                if ($this->_annoncePress instanceof CommonAnnoncePress) {
                    $this->_annoncePress->setObjet($this->objet->Text);
                    $this->_annoncePress->setTexte($this->textMail->Text);
                    $this->_annoncePress->save($connexionCom);
                }
            }
            $this->RepeaterDestinataire->DataSource = $dataSource;
            $this->RepeaterDestinataire->DataBind();
            $this->nbreDest->setText(is_countable($dataSource) ? count($dataSource) : 0);
        }
    }

    public function remplirRepeaterAdresseFacturation($idAdresseFact, $organisme, $idServiceCons)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        if ($idAdresseFact) {
            $c->add(CommonAdresseFacturationJalPeer::ID, $idAdresseFact, Criteria::EQUAL);
        }
        $c->add(CommonAdresseFacturationJalPeer::SERVICE_ID, $idServiceCons, Criteria::EQUAL);
        $c->add(CommonAdresseFacturationJalPeer::ORGANISME, $organisme, Criteria::EQUAL);
        $adressesFacturation = CommonAdresseFacturationJalPeer::doSelect($c, $connexionCom);
        $this->_nbrAdresseFacturation = count($adressesFacturation);
        $this->RepeaterAdresseFacturation->DataSource = $adressesFacturation;
        $this->RepeaterAdresseFacturation->DataBind();
        $this->nbreAdresseFact->setText(count($adressesFacturation));
    }

    public function getChekcdDestJal($idDestJal)
    {
        $listDestChecked = (new Atexo_Publicite_AvisPub())->getDestinataireByIdJalAndIdAnnonce($idDestJal, $this->_annoncePress->getId(), $this->_org);
        if (isset($_GET['consult']) || $listDestChecked) {
            return true;
        }

        return false;
    }

    public function onDeleteClick($sender, $param)
    {
        $idpj = $param->CommandParameter;
        Press::DeletePjAnnoncePress($idpj, $this->_org);
    }

    public function refreshRepeaterPJ($sender, $param)
    {
        //Raffraichissement du tableau PJ:
        $annonceObject = (new Press())->retreiveListAnnoncPressByRefIdDestPub(Atexo_Util::atexoHtmlEntities($_GET['idDest']), $this->_org);
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $PJ = [];
        if ($annonceObject) {
            $c->add(CommonAnnoncePressPieceJointePeer::ORGANISME, $this->_org);
            $PJ = $annonceObject->getCommonAnnoncePressPieceJointes($c, $connexionCom);
        }
        $this->_annoncePress = $annonceObject;
        $this->remplirTableauPJ($PJ);
        $this->PanelPJ->render($param->NewWriter);
    }

    /**
     * permet de remplir le tableau de pieces jointe a l'annonce.
     *
     * @param array $data tableau des objets CommonAnnoncePressPieceJointes
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.0.0
     *
     * @copyright Atexo 2014
     */
    public function remplirTableauPJ($data)
    {
        $dataSource = [];
        if (is_array($data) && count($data) > 0) {
            foreach ($data as $uneData) {
                if ($uneData instanceof CommonAnnoncePressPieceJointe) {
                    $dataSource[] = $uneData;
                }
            }
        }
        $this->countPj->Text = count($dataSource);
        $this->RepeaterPiecesJointes->dataSource = $dataSource;
        $this->RepeaterPiecesJointes->dataBind();
    }

    public function remplirListeMessage($org, $consultation = null)
    {
        $dataMessageType = [];
								$ligne = null;
								$dataMessageType[] = Prado::localize('ID_MESSAGE_ENVOI_PRESS');
        $this->messageType->dataSource = $dataMessageType;
        $this->messageType->dataBind();
        //Remplissage du corps du message
        $message = Prado::localize('CORPS_MESSAGE_PRESSE');
        $ligne .= '
		
		
';

        $message = str_replace('[_ligne_]', $ligne, $message);
        //          //Recherche de la consultation

        if ($consultation instanceof CommonConsultation) {
            $this->objet->Text = str_replace('[__Ref__Utilisateur__Cons__]', $consultation->getReferenceUtilisateur(), Prado::localize('DEFINE_OBJET_MESSAGE_ENVOIE_PRESSE'));
            $texteMessage = $message;
            /*$texteMessage .= Prado::localize('INFOS_MAIL_PUB_PRESS').'
    ';
            $texteMessage .= '
    ';
               $texteMessage .= Atexo_Message::getInfoConsultation($this->getRefConsultation(),$this->_org, true).'
    ';*/
            $texteMessage .= ' 
	';
            $this->textMail->Text = $texteMessage;
        }
    }

    public function enregistrer($sender, $param)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $annonceObject = (new Press())->retreiveListAnnoncPressByRefIdDestPub(Atexo_Util::atexoHtmlEntities($_GET['idDest']), $this->_org);
        if ($annonceObject) {
            $annonceObject->setTexte($this->textMail->Text);
            $annonceObject->setObjet($this->objet->Text);
            $annonceObject->setDateCreation(date('Y-m-d m:s'));
            $annonceObject->setConsultationId(Atexo_Util::atexoHtmlEntities($_GET['id']));
            $annonceObject->setIdDestPress(Atexo_Util::atexoHtmlEntities($_GET['idDest']));
            if ($this->courrierElectroniqueSimple->Checked) {
		 	  		$annonceObject->setOptionEnvoi(Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_SIMPLE'));
            }
            if ($this->courrierElectroniqueContenuIntegralAR->Checked) {
		 	  		$annonceObject->setOptionEnvoi(Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_AVEC_AR'));
            }
            if ($this->courrierElectroniqueUniquementLienTelechargementObligatoire->Checked) {
		 	  		$annonceObject->setOptionEnvoi(Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_AVEC_LIEN'));
            }
            $sipFacturation = '';
            $idBlobLogo = '';
            foreach ($this->RepeaterAdresseFacturation->Items as $Item) {
                if ('true' == $Item->idRadioBouton->getChecked()) {
                    $annonceObject->setIdAdresseFacturation($Item->idAdresseFact->text);
                    //Recupération de la valeur du SIP selectionné et du logo joint
                    if ($annonceObject instanceof CommonAnnoncePress) {
                        $adresseFacturation = CommonAdresseFacturationJalPeer::retrieveByPK($annonceObject->getIdAdresseFacturation(), $annonceObject->getOrganisme(), $connexionCom);
                        if ($adresseFacturation instanceof CommonAdresseFacturationJal) {
                            $sipFacturation = $adresseFacturation->getFacturationSip();
                            //Recuperation du logo joint
                            if ('true' == $Item->idRadioBouton->getChecked() && 'true' == $Item->ajoutLogo->getChecked()) {
                                $idBlobLogo = $adresseFacturation->getIdBlobLogo();
                            } else {
                                $idBlobLogo = null;
                            }
                        }
                    }
                    //Enregistrement de la valeur du sip choisi dans la table Avis_Pub
                    $dest = (new Atexo_Publicite_AvisPub())->getDestinatairePub($annonceObject->getIdDestPress());
                    if ($dest instanceof CommonDestinatairePub) {
                        $avis = (new Atexo_Publicite_AvisPub())->retreiveAvis($dest->getIdAvis(), $dest->getOrganisme());
                        if ($avis instanceof CommonAvisPub) {
                            $avis->setSip($sipFacturation);
                            $avis->setIdBlobLogo($idBlobLogo);
                            $avis->save($connexionCom);
                        }
                    }
                }
            }
            if ($annonceObject->save($connexionCom)) {
                foreach ($this->RepeaterDestinataire->Items as $Item) {
                    $destByIdJal = Press::getDestinataireAnnoncePress($annonceObject->getId(), $Item->idJal->text, $this->_org);
                    if ($destByIdJal) {
                        if ($Item->idCheck->checked) {
                            $destByIdJal->setIdJAl($destByIdJal->getIdJal());
                            $destByIdJal->save($connexionCom);
                        } else {
                            Press::DeleteDestNonCoche($destByIdJal->getId(), $destByIdJal->getIdJal(), $this->_org);
                        }
                    } else {
                        if ($Item->idCheck->checked) {
                            $destinataire = new CommonDestinataireAnnoncePress();
                            $destinataire->setOrganisme($this->_org);
                            $destinataire->setIdAnnoncePress($annonceObject->getId());
                            $destinataire->setIdJAl($Item->idJal->text);
                            $destinataire->save($connexionCom);
                        } else {
                            $logger = Atexo_LoggerManager::getLogger('app');
                            $logger->info('No mail checked');
                        }
                    }
                }
                $this->_idPress = $annonceObject->getId();
                $close = false;
                switch ($sender->ID) {
                    case 'buttonValid':
                        (new Atexo_Publicite_AvisPub())->updateEtatDestPub(Atexo_Util::atexoHtmlEntities($_GET['idDest']), Atexo_Config::getParameter('STATUT_DESTINATAIRE_AVIS_COMPLET'));
                        $close = true;
                        break;
                    case 'buttonSave':
                        (new Atexo_Publicite_AvisPub())->updateEtatDestPub(Atexo_Util::atexoHtmlEntities($_GET['idDest']), Atexo_Config::getParameter('STATUT_DESTINATAIRE_AVIS_BROUILLON'));
                        break;
                }

                $page = $_GET['page'];
                $idDest = $_GET['idDest'];
                $id = $_GET['id'];
                if (!(new Atexo_Publicite_AvisPub())->isAccesGestionPubMpe()) {
                    $organisme = $_GET['organisme'];
                    $url = '?page='.$page.'&idDest='.$idDest
                        .'&id='.$id
                        .'&organisme='.$organisme.'&close='.$close;
                    $this->response->redirect($url);
                } else {
                    if ($page !== 'GestionPub.PopUpConsultationPress' && $close) {
                        $this->response->redirect('?page=Agent.PubliciteAvis&id='.base64_encode(Atexo_Util::atexoHtmlEntities($_GET['id'])));
                    } else {
                        $this->response->redirect('/index.php?page='.$page.'&idDest='.$idDest.'&id='.$id);
                    }
                }
            }
        }
    }

    public function RedirectReturn()
    {
        $this->response->redirect('?page=Agent.PubliciteAvis&id='.base64_encode(Atexo_Util::atexoHtmlEntities($_GET['id'])));
    }

    /*
     * Retourne le nombre d'éléments de la liste des adresses de facturation
     */
    public function getNbrAdresseFacturation()
    {
        return $this->_nbrAdresseFacturation;
    }

    public function getServiceConsultation($reference)
    {
        $consultation = (new Atexo_Consultation())->retrieveConsultation($reference);
        if ($consultation instanceof CommonConsultation) {
            return $consultation->getServiceId();
        }
    }

    public function valider($sender, $param)
    {
        $organePresseChecked = false;
        $adresseFacturationChecked = false;
        foreach ($this->RepeaterDestinataire->Items as $Item) {
            if ($Item->idCheck->getChecked()) {
                $organePresseChecked = true;
            }
        }
        foreach ($this->RepeaterAdresseFacturation->Items as $Item) {
            if ($Item->idRadioBouton->getChecked()) {
                $adresseFacturationChecked = true;
            }
        }
        if ($organePresseChecked && $adresseFacturationChecked) {
            self::enregistrer($sender, $param);
        }
        if (!$organePresseChecked) {
            $this->response->redirect('?page=Agent.EnvoiCourrierElectroniquePress&idDest='.Atexo_Util::atexoHtmlEntities($_GET['idDest']).'&id='.Atexo_Util::atexoHtmlEntities($_GET['id']).'&noSelectDest');
        }
        if (!$adresseFacturationChecked) {
            $this->response->redirect('?page=Agent.EnvoiCourrierElectroniquePress&idDest='.Atexo_Util::atexoHtmlEntities($_GET['idDest']).'&id='.Atexo_Util::atexoHtmlEntities($_GET['id']).'&noSelectFact');
        }
    }

    public function isStatutAvisBrouillonOuRejette()
    {
        $avisPub = $this->getViewState('avisPub');
        if ($avisPub->getStatut() == Atexo_Config::getParameter('STATUT_AVIS_A_TRANSMETTRE')
            || $avisPub->getStatut() == Atexo_Config::getParameter('STATUT_AVIS_REJETE_ECO')
                || $avisPub->getStatut() == Atexo_Config::getParameter('STATUT_AVIS_REJETE_SIP')) {
            return true;
        }

        return false;
    }
}
