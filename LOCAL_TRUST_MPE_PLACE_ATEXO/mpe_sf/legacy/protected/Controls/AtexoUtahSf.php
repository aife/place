<?php

namespace Application\Controls;

use App\Service\Version\VersionService;
use Application\Pages\Agent\ouvertureEtAnalyse;
use Application\Controls\MpeTTemplateControl;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonEtatConsultation;
use Application\Propel\Mpe\CommonTEtablissement;
use Application\Propel\Mpe\CommonTEtablissementQuery;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_ContexteUtah;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Entreprise;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Service;
use Application\Service\Atexo\Atexo_Utah;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Inscrit;
use Exception;
use Prado\Prado;

/**
 * Classe de gestion du bouton d'acces a l'application UTAH.
 *
 * @author Florian MARTIN <florian.martin@atexo.com>
 */
class AtexoUtahSf extends MpeTTemplateControl
{
    protected $_calledFrom = '';
    protected $_refConsultation;
    protected $_organisme;

    /**
     * Get the value of [_calledFrom] column.
     *
     * @return string
     *
     * @author Florian MARTIN <florian.martin@atexo.com>
     */
    public function getCalledFrom()
    {
        return $this->_calledFrom;
    }

    /**
     * Set the value of [_calledFrom] column.
     *
     * @param string $calledFrom
     *
     * @author Florian MARTIN <florian.martin@atexo.com>
     */
    public function setCalledFrom($calledFrom)
    {
        $this->_calledFrom = $calledFrom;
    }

    /**
     * Get the value of [_refConsultation] column.
     *
     * @return mixed
     *
     * @author Florian MARTIN <florian.martin@atexo.com>
     */
    public function getRefConsultation()
    {
        return $this->_refConsultation;
    }

    /**
     * Set the value of [_refConsultation] column.
     *
     * @param mixed $consultationId
     *
     * @author Florian MARTIN <florian.martin@atexo.com>
     */
    public function setRefConsultation($consultationId)
    {
        $this->_refConsultation = $consultationId;
    }

    /**
     * Get the value of [_organisme] column.
     *
     * @return mixed
     *
     * @author Florian MARTIN <florian.martin@atexo.com>
     */
    public function getOrganisme()
    {
        return $this->_organisme;
    }

    /**
     * Set the value of [_organisme] column.
     *
     * @param mixed $organisme
     *
     * @author Florian MARTIN <florian.martin@atexo.com>
     */
    public function setOrganisme($organisme)
    {
        $this->_organisme = $organisme;
    }

    /**
     * Permet de verifier si le composant est appele depuis l'acces "Agent".
     *
     * @return bool
     *
     * @author Florian MARTIN <florian.martin@atexo.com>
     */
    public function isCallFromAgent()
    {
        if (!empty($this->getCalledFrom())) {
            return 'agent' === $this->getCalledFrom();
        }

        return Atexo_CurrentUser::isAgent();
    }

    /**
     * Permet de verifier si le composant est appele depuis l'acces "Entreprise".
     *
     * @return bool
     *
     * @author Florian MARTIN <florian.martin@atexo.com>
     */
    public function isCallFromEntreprise()
    {
        if (!empty($this->getCalledFrom())) {
            return 'entreprise' === $this->getCalledFrom();
        }

        return !Atexo_CurrentUser::isAgent();
    }

    /**
     * Permet de recuperer les contextes depuis la session de la page.
     *
     * @return array|null
     *
     * @author Florian MARTIN <florian.martin@atexo.com>
     */
    public function getContextes()
    {
        return $this->getViewState($this->getIdContexteInSession());
    }

    /**
     * Permet de sauvegarder les contextes dans la session du navigateur.
     *
     * @param $contextes
     *
     * @author Florian MARTIN <florian.martin@atexo.com>
     */
    public function setContextes($contextes)
    {
        $this->setViewState($this->getIdContexteInSession(), $contextes);
    }

    /**
     * Permet de construire la cle des donnees dans la session de la page.
     *
     * @return string
     *
     * @author Florian MARTIN <florian.martin@atexo.com>
     */
    private function getIdContexteInSession()
    {
        return 'contextes_utah_'.implode('_', explode('.', $_GET['page']));
    }

    /**
     * Permet de sauvegarder l'objet consultation dans la session de la page.
     *
     * @param CommonConsultation $consultation
     *
     * @author Florian MARTIN <florian.martin@atexo.com>
     */
    private function saveConsultation($consultation)
    {
        $this->setViewState($this->getIdContexteInSession().'_consultation', $consultation);
    }

    /**
     * Permet de recuperer l'objet consultation depuis la session de la page.
     *
     * @return CommonConsultation|null
     *
     * @author Florian MARTIN <florian.martin@atexo.com>
     */
    private function getConsultationFromSession()
    {
        return $this->getViewState($this->getIdContexteInSession().'_consultation');
    }

    /**
     * Permet de gerer l'acces au formulaire de l'application UTAH.
     *
     * @return string
     *
     * @author Florian MARTIN <florian.martin@atexo.com>
     */
    public function accederFormulaireUtah($javaVersion)
    {
        if (!Atexo_Config::getParameter('UTILISER_FAQ') && !Atexo_Config::getParameter('UTILISER_UTAH')) {
            return;
        }

        if (Atexo_CurrentUser::isConnected() || Atexo_Config::getParameter('UTILISER_UTAH_MODE_DECONNECTER')) {
            //Creation des contextes
            $this->creerContextes($javaVersion);

            //Recuperation du jeton et ouverture du formulaire UTAH
            $jsonContexte = $this->encodeToJson($this->getContextes());
            $token = (new Atexo_Utah())->recupererToken($jsonContexte);
            if (false !== $token && !empty($token)) {
                $urlFormulaireUtah = Atexo_Utah::getUrlAssistance().'?token='.$token;
                $this->response->redirect($urlFormulaireUtah);
            } else {
                $this->messageErreur->visible = true;
                $this->messageErreur->setMessage(Prado::localize('MESSAGE_ERREUR_CONNEXION_APPLICATION_UTAH').Atexo_Config::getParameter('NUMERO_TELEPHONE_INDISPO_UTAH'));
            }
        }
    }

    /**
     * Permet d'initialiser le bouton d'acces a l'application UTAH.
     *
     * @return void
     *
     * @author Florian MARTIN <florian.martin@atexo.com>
     */
    public function initialiser()
    {
        if (Atexo_CurrentUser::isConnected() || Atexo_Config::getParameter('UTILISER_UTAH_MODE_DECONNECTER')) {
            if (!Atexo_Config::getParameter('UTILISER_FAQ') && !Atexo_Config::getParameter('UTILISER_UTAH')) {
                return;
            }

            $this->customize();
            $this->preparerDonneesPourContexteMetier();
        }
    }

    /**
     * Permet de creer les contextes a envoyer a l'application UTAH.
     *
     * @return void
     *
     * @author Florian MARTIN <florian.martin@atexo.com>
     */
    public function creerContextes($javaVersion = false)
    {
        try {
            $contexteApplicative = $this->creerContexteApplicatif();
            $contexteTechnique = $this->creerContexteTechnique($javaVersion);
            $contexteMetier = $this->creerContexteMetier();

            $contextes = [];
            $contextes['applicatif'] = $contexteApplicative->toArray();
            $contextes['metier'] = $contexteMetier->toArray();
            $contextes['technique'] = $contexteTechnique->toArray();

            $this->setContextes($contextes);
        } catch (Exception $e) {
            $logger = Atexo_LoggerManager::getLogger();
            $logger->error('Erreur lors de la creation des contextes UTAH: '.PHP_EOL.'Exception: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString());
        }
    }

    /**
     * Permet de creer un contexte applicatif.
     *
     * @return Atexo_ContexteUtah
     *
     * @author Florian MARTIN <florian.martin@atexo.com>
     */
    private function creerContexteApplicatif()
    {
        try {
            $nomUtilisateur = null;
            $idUtilisateur = null;
            $emailUtilisateur = null;
            $entiteUtilisateur = null;
            if (Atexo_CurrentUser::isConnected()) {
                $nomUtilisateur = (Atexo_CurrentUser::isAgent()) ? Atexo_CurrentUser::getFirstName().' '.Atexo_CurrentUser::getLastName() : Atexo_CurrentUser::getFirstNameInscrit().' '.Atexo_CurrentUser::getLastNameInscrit();
                $userId = Atexo_CurrentUser::getId();
                if (Atexo_CurrentUser::isAgent()) {
                    $agent = Atexo_Agent::retrieveAgent($userId);
                    $createUtilisateur = $agent->getDateCreation();
                    $agentId = $agent->getId();
                    $agentExterneId = $agent->getIdExterne();
                    //entite Id socle
                    $organisme = Atexo_Organismes::retrieveOrganismeByAcronyme($agent->getOrganisme());

                    if (is_object($organisme) && isset($organisme)) {
                        $organismeIdInitial = $organisme->getIdInitial();
                    }

                    $serviceId = $agent->getServiceId();
                    $service = Atexo_Service::retrieveServiceById($serviceId);
                    $nomService = null;
                    if (is_object($service) && isset($service)) {
                        $serviceIdExterne = $service->getIdExterne();
                        $nomService = $service->getLibelle();
                    }
                } else {
                    $inscrit = Atexo_Entreprise_Inscrit::retrieveInscritById($userId);
                    $createUtilisateur = $inscrit->getDateCreation();
                    $inscritId = $inscrit->getId();
                    $inscriIntialId = $inscrit->getIdInitial();
                }

                if ($this->isCallFromAgent()) {
                    $entitePublique = Atexo_EntityPurchase::getPathEntityById(0, Atexo_CurrentUser::getCurrentOrganism());
                    $entiteAchat = Atexo_EntityPurchase::getPathEntityById(Atexo_CurrentUser::getCurrentServiceId(), Atexo_CurrentUser::getCurrentOrganism());
                    $entiteUtilisateur = ['entite_publique' => $entitePublique, 'entite_achat' => $entiteAchat];
                } elseif ($this->isCallFromEntreprise()) {
                    $entreprise = (new Atexo_Entreprise())->retrieveCompanyBdeById(Atexo_CurrentUser::getIdEntreprise(), null, false);
                    $etab = CommonTEtablissementQuery::create()->findOneByIdEtablissement(Atexo_CurrentUser::getIdEtablissement());
                    $cp = '';
                    if ($etab instanceof CommonTEtablissement) {
                        $cp = $etab->getCodePostal();
                    }
                    $entiteUtilisateur = ['nom' => $entreprise->getNom(), 'siren' => $entreprise->getSiren(), 'code_postal' => $cp];
                    $raisonSocialIdSocle = $entreprise->getIdInitial();
                }
                $idUtilisateur = Atexo_CurrentUser::getId();
                $emailUtilisateur = Atexo_CurrentUser::getEmail();
            }

            /** @var VersionService $versionService */
            $versionService = Atexo_Util::getSfService(VersionService::class);

            $contexteApplicative = new Atexo_ContexteUtah('applicatif', 'applicatif', 'Contexte applicatif de la demande', Atexo_Config::getParameter('NOMBRE_ELEMENTS_VISIBLES_CONTEXTE_APPLICATIF'));
            $contexteApplicative->add('application', 'Plate-forme', Atexo_Config::getParameter('PF_SHORT_NAME'), 'string');
            $contexteApplicative->add('version', 'Version', $versionService->getApplicationVersion(), 'string');
            $contexteApplicative->add('type_acteur', "Type d'utilisateur", ($this->isCallFromAgent()) ? 'agent_place' : 'entreprise_place', 'string', false);
            $contexteApplicative->add('utilisateur_nom', "Nom de l'utilisateur", $nomUtilisateur, 'string');
            $contexteApplicative->add('utilisateur_id', "ID de l'utilisateur", $idUtilisateur, 'integer', false);
            $contexteApplicative->add('utilisateur_email', "Courriel de l'utilisateur", $emailUtilisateur, 'mailto');
            if ($createUtilisateur) {
                $contexteApplicative->add('utilisateur_date_creation', 'Date de création', $createUtilisateur, 'datetime');
            }

            if ($this->isCallFromAgent()) {
                $contexteApplicative->add('utilisateur_entite', 'Organisme', $entiteUtilisateur, 'organisme_achat');
                $contexteApplicative->add('entiteIdSocle', 'entiteIdSocle', $organismeIdInitial ?? null, 'string');
                $contexteApplicative->add('serviceIdSocle', 'serviceIdSocle', $serviceIdExterne ?? null, 'string');
                $contexteApplicative->add('service', 'service', $nomService ?? null, 'string');
                $contexteApplicative->add('id', 'Id Agent', $agentId ?? null, 'string');
                $contexteApplicative->add('idSocle', 'Agent id externe', $agentExterneId ?? null, 'string');
            } elseif ($this->isCallFromEntreprise()) {
                $contexteApplicative->add('utilisateur_entite', 'Entreprise', $entiteUtilisateur, 'entreprise');
                $contexteApplicative->add('raisonSocialIdSocle', 'Entreprise id initial', $raisonSocialIdSocle ?? null, 'string');
                $contexteApplicative->add('idEntreprise', 'Id Inscrit', $inscritId ?? null, 'string');
                $contexteApplicative->add('idSocleEntreprise', 'Id Socle', $inscriIntialId ?? null, 'string');
            }

            $contexteApplicative->add(
                'utiliser_utah',
                'utiliser_utah',
                Atexo_Config::getParameter('UTILISER_UTAH'),
                'boolean',
                false
            );
            $contexteApplicative->add(
                'url_utah',
                'url_utah',
                Atexo_Config::getParameter('URL_UTAH'),
                'string',
                false
            );

            return $contexteApplicative;
        } catch (Exception $e) {
            $logger = Atexo_LoggerManager::getLogger();
            $logger->error('Erreur lors de creation du contexte applicatif: '.PHP_EOL.'Exception: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString());
        }
    }

    /**
     * Permet de creer un contexte Technique.
     *
     * @return Atexo_ContexteUtah
     *
     * @author Florian MARTIN <florian.martin@atexo.com>
     */
    public function creerContexteTechnique($javaVersion = false)
    {
        try {
            $javaVersion = ($javaVersion) ? $javaVersion : $this->javaVersion->Value;
            $contexteTechnique = new Atexo_ContexteUtah('technique', 'technique', 'Technique', Atexo_Config::getParameter('NOMBRE_ELEMENTS_VISIBLES_CONTEXTE_TECHNIQUE'));
            $contexteTechnique->add('os', "Système d'exploitation", Atexo_Util::getOsVisiteur(), 'string', true, true, null, $_SERVER['HTTP_USER_AGENT']);
            $infosBrowser = Atexo_Util::getInfosBrowserVisiteur();
            $contexteTechnique->add('navigateur', 'Navigateur', $infosBrowser['name'].' '.$infosBrowser['version'], 'string', true, true, null, $_SERVER['HTTP_USER_AGENT']);
            $contexteTechnique->add('java_ver', 'Version de Java', $javaVersion, 'string');

            return $contexteTechnique;
        } catch (Exception $e) {
            $logger = Atexo_LoggerManager::getLogger();
            $logger->error('Erreur lors de creation du contexte technique: '.PHP_EOL.'Exception: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString());
        }
    }

    /**
     * Permet de creer un contexte metier.
     *
     * @return Atexo_ContexteUtah
     *
     * @author Florian MARTIN <florian.martin@atexo.com>
     */
    public function creerContexteMetier()
    {
        try {
            $contexteMetier = new Atexo_ContexteUtah('metier', 'métier', 'Métier', Atexo_Config::getParameter('NOMBRE_ELEMENTS_VISIBLES_CONTEXTE_METIER'));

            $consultation = $this->getConsultationFromSession();
            if ($consultation instanceof CommonConsultation) {
                $contexteMetier->add('ref_consult', 'Réf. de la consultation', $consultation->getReferenceUtilisateur(), 'string');
                $contexteMetier->add('id_consult', 'ID de la consultation', $consultation->getId(), 'integer', false);

                $objetEtatCons = $consultation->getObjetEtatConsultation($this->isCallFromEntreprise());
                if ($objetEtatCons instanceof CommonEtatConsultation) {
                    $contexteMetier->add('etape', 'Étape', $objetEtatCons->getLibelleEtat(), 'string');
                }
                $contexteMetier->add('allotissement', 'Allotissement', ($consultation->isAllotie()) ? true : false, 'boolean');
                $contexteMetier->add('rdp', 'Date limite de remise des plis', $consultation->getDateFin(), 'datetime');
            }

            return $contexteMetier;
        } catch (Exception $e) {
            $logger = Atexo_LoggerManager::getLogger();
            $logger->error('Erreur lors de creation du contexte metier: '.PHP_EOL.'Exception: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString());
        }
    }

    /**
     * Permet d'encoder en json les donnees a envoyer a l'applicatio UTAH.
     *
     * @param array $data
     *
     * @return string
     *
     * @author Florian MARTIN <florian.martin@atexo.com>
     */
    private function encodeToJson($data)
    {
        try {
            return json_encode(Atexo_Util::utf8_converter($data));
        } catch (Exception $e) {
            switch (json_last_error()) {
                case JSON_ERROR_NONE:
                    $jsonLastError = ' - Aucune erreur';
                    break;
                case JSON_ERROR_DEPTH:
                    $jsonLastError = ' - Profondeur maximale atteinte';
                    break;
                case JSON_ERROR_STATE_MISMATCH:
                    $jsonLastError = ' - Inadéquation des modes ou underflow';
                    break;
                case JSON_ERROR_CTRL_CHAR:
                    $jsonLastError = ' - Erreur lors du contrôle des caractères';
                    break;
                case JSON_ERROR_SYNTAX:
                    $jsonLastError = ' - Erreur de syntaxe ; JSON malformé';
                    break;
                case JSON_ERROR_UTF8:
                    $jsonLastError = ' - Caractères UTF-8 malformés, probablement une erreur d\'encodage';
                    break;
                default:
                    $jsonLastError = ' - Erreur inconnue';
                    break;
            }
            $logger = Atexo_LoggerManager::getLogger();
            $logger->error("json last error = $jsonLastError ".PHP_EOL.'Exception: '.$e->getMessage().PHP_EOL.'Trace: '.$e->getTraceAsString());
        }
    }

    /**
     * Permet de customizer les elements du composant.
     *
     * @return void
     *
     * @author Florian MARTIN <florian.martin@atexo.com>
     */
    public function customize()
    {
        $this->messageErreur->visible = false;
    }

    /**
     * Permet de preparer les donnees necessaires pour la generation des donnees du contexte metier.
     *
     * @return void
     *
     * @author Florian MARTIN <florian.martin@atexo.com>
     */
    public function preparerDonneesPourContexteMetier()
    {
        if ('page' == $this->getRequest()->getServiceId()) {
            $page = $this->getRequest()->getServiceParameter();
            switch ($page) {
                case 'Entreprise.EntrepriseDetailsConsultation':
                    $cas = 'contexte_metier_consultation';
                    $ref = Atexo_Util::atexoHtmlEntities($_GET['id']);
                    $org = Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']);
                    break;
                case 'Entreprise.FormulaireReponseConsultationMPS':
                    $cas = 'contexte_metier_consultation';
                    $ref = Atexo_Util::atexoHtmlEntities($_GET['id']);
                    $org = Atexo_Util::atexoHtmlEntities($_GET['org']);
                    break;
                case 'Entreprise.FormulaireReponseConsultation':
                    $cas = 'contexte_metier_consultation';
                    $ref = Atexo_Util::atexoHtmlEntities($_GET['id']);
                    $org = Atexo_Util::atexoHtmlEntities($_GET['org']);
                    break;
                case 'Entreprise.EntrepriseFormulairePoserQuestion':
                    $cas = 'contexte_metier_consultation';
                    $ref = Atexo_Util::atexoHtmlEntities($_GET['id']);
                    $org = Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']);
                    break;
                case 'Entreprise.EntrepriseDemandeTelechargementDce':
                    $cas = 'contexte_metier_consultation';
                    $ref = Atexo_Util::atexoHtmlEntities($_GET['id']);
                    $org = Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']);
                    break;
                case 'Agent.DetailConsultation':
                    $cas = 'contexte_metier_consultation';
                    $ref = Atexo_Util::atexoHtmlEntities($_GET['id']);
                    $org = Atexo_CurrentUser::getCurrentOrganism();
                    break;
                case 'Agent.ouvertureEtAnalyse':
                    $cas = 'contexte_metier_consultation';
                    $ref = Atexo_Util::atexoHtmlEntities($_GET['id']);
                    $org = Atexo_CurrentUser::getCurrentOrganism();
                    break;
                case 'Agent.GestionRegistres':
                    $cas = 'contexte_metier_consultation';
                    $ref = Atexo_Util::atexoHtmlEntities($_GET['id']);
                    $org = Atexo_CurrentUser::getCurrentOrganism();
                    break;
                case 'Agent.ArchiveArborescence':
                    $cas = 'contexte_metier_consultation';
                    $ref = Atexo_Util::atexoHtmlEntities($_GET['id']);
                    $org = Atexo_CurrentUser::getCurrentOrganism();
                    break;
                case 'Agent.ArchiveMetadonnee':
                    $cas = 'contexte_metier_consultation';
                    $ref = Atexo_Util::atexoHtmlEntities($_GET['id']);
                    $org = Atexo_CurrentUser::getCurrentOrganism();
                    break;
                case 'Agent.ArchiveRappelTableauDeBord':
                    $cas = 'contexte_metier_consultation';
                    $ref = Atexo_Util::atexoHtmlEntities($_GET['id']);
                    $org = Atexo_CurrentUser::getCurrentOrganism();
                    break;
                case 'Agent.ChangingConsultation':
                    $cas = 'contexte_metier_consultation';
                    $ref = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id']));
                    $org = Atexo_CurrentUser::getCurrentOrganism();
                    break;
                case 'Agent.ValiderConsultation':
                    $cas = 'contexte_metier_consultation';
                    $ref = Atexo_Util::atexoHtmlEntities($_GET['id']);
                    $org = Atexo_CurrentUser::getCurrentOrganism();
                    break;
                case 'Agent.TraduireConsultation':
                    $cas = 'contexte_metier_consultation';
                    $ref = Atexo_Util::atexoHtmlEntities($_GET['id']);
                    $org = Atexo_CurrentUser::getOrganismAcronym();
                    break;
                case 'Agent.TableauDecisionChorus':
                    $cas = 'contexte_metier_consultation';
                    $ref = Atexo_Util::atexoHtmlEntities($_GET['id']);
                    $org = Atexo_CurrentUser::getCurrentOrganism();
                    break;
                case 'Agent.TableauDeBord':
                    $cas = 'contexte_metier_consultation';
                    $ref = Atexo_Util::atexoHtmlEntities($_GET['id']);
                    $org = Atexo_CurrentUser::getCurrentOrganism();
                    break;
                case 'Agent.SuiteConsultation':
                    $cas = 'contexte_metier_consultation';
                    $ref = Atexo_Util::atexoHtmlEntities($_GET['id']);
                    $org = Atexo_Util::atexoHtmlEntities($_GET['org']);
                    break;
                case 'Agent.resultatAnalyse':
                    $cas = 'contexte_metier_consultation';
                    $ref = Atexo_Util::atexoHtmlEntities($_GET['id']);
                    $org = Atexo_CurrentUser::getCurrentOrganism();
                    break;
                case 'Agent.PopupUpdateDce':
                    $cas = 'contexte_metier_consultation';
                    $ref = base64_decode($_GET['id']);
                    $org = Atexo_CurrentUser::getCurrentOrganism();
                    break;
                case 'Agent.PubliciteConsultation':
                    $cas = 'contexte_metier_consultation';
                    $ref = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id']));
                    $org = Atexo_CurrentUser::getOrganismAcronym();
                    break;
                case 'Agent.EnvoiCourrierElectroniqueAnnulationConsultation':
                    $cas = 'contexte_metier_consultation';
                    $ref = Atexo_Util::atexoHtmlEntities($_GET['id']);
                    $org = Atexo_CurrentUser::getCurrentOrganism();
                    break;
                case 'Agent.EnvoiCourrierElectroniqueChoixDestinataire':
                    $cas = 'contexte_metier_consultation';
                    $ref = Atexo_Util::atexoHtmlEntities($_GET['id']);
                    $org = Atexo_CurrentUser::getCurrentOrganism();
                    break;
                case 'Agent.EnvoiCourrierElectroniqueComplementsFormulaires':
                    $cas = 'contexte_metier_consultation';
                    $ref = Atexo_Util::atexoHtmlEntities($_GET['id']);
                    $org = Atexo_CurrentUser::getCurrentOrganism();
                    break;
                case 'Agent.EnvoiCourrierElectroniqueDemandeComplement':
                    $cas = 'contexte_metier_consultation';
                    $ref = Atexo_Util::atexoHtmlEntities($_GET['id']);
                    $org = Atexo_CurrentUser::getCurrentOrganism();
                    break;
                case 'Agent.EnvoiCourrierElectroniqueInvitationConcourir':
                    $cas = 'contexte_metier_consultation';
                    $ref = Atexo_Util::atexoHtmlEntities($_GET['id']);
                    $org = Atexo_CurrentUser::getCurrentOrganism();
                    break;
                case 'Agent.EnvoiCourrierElectroniqueNotification':
                    $cas = 'contexte_metier_consultation';
                    $ref = Atexo_Util::atexoHtmlEntities($_GET['id']);
                    $org = Atexo_CurrentUser::getCurrentOrganism();
                    break;
                case 'Agent.EnvoiCourrierElectroniqueModifConsultation':
                    $cas = 'contexte_metier_consultation';
                    $ref = Atexo_Util::atexoHtmlEntities($_GET['id']);
                    $org = Atexo_CurrentUser::getCurrentOrganism();
                    break;
                case 'Agent.EnvoiCourrierElectroniquePress':
                    $cas = 'contexte_metier_consultation';
                    $ref = Atexo_Util::atexoHtmlEntities($_GET['id']);
                    $org = Atexo_CurrentUser::getCurrentOrganism();
                    break;
                case 'Agent.EnvoiCourrierElectroniqueReponseQuestion':
                    $cas = 'contexte_metier_consultation';
                    $ref = Atexo_Util::atexoHtmlEntities($_GET['id']);
                    $org = Atexo_CurrentUser::getCurrentOrganism();
                    break;
                case 'Agent.EnvoiCourrierElectroniqueSimple':
                    $cas = 'contexte_metier_consultation';
                    $ref = Atexo_Util::atexoHtmlEntities($_GET['id']);
                    $org = Atexo_CurrentUser::getCurrentOrganism();
                    break;
                case 'Agent.FormeEnvoiCourrierElectroniqueJAL':
                    $cas = 'contexte_metier_consultation';
                    $ref = Atexo_Util::atexoHtmlEntities($_GET['id']);
                    $org = Atexo_CurrentUser::getCurrentOrganism();
                    break;
                case 'Agent.DonneesComplementairesConsultation':
                    $cas = 'contexte_metier_consultation';
                    $ref = Atexo_Util::atexoHtmlEntities($_GET['id']);
                    $org = Atexo_CurrentUser::getCurrentOrganism();
                    break;
                case 'Agent.DetailDce':
                    $cas = 'contexte_metier_consultation';
                    $ref = Atexo_Util::atexoHtmlEntities($_GET['id']);
                    $org = Atexo_CurrentUser::getCurrentOrganism();
                    break;
                case 'Agent.FormulaireConsultation':
                    $cas = 'contexte_metier_consultation';
                    $ref = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id']));
                    $org = Atexo_CurrentUser::getCurrentOrganism();
                    break;
                case 'Agent.GererEnchere':
                    $cas = 'contexte_metier_consultation';
                    $ref = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id']));
                    $org = Atexo_CurrentUser::getCurrentOrganism();
                    break;
                case 'Agent.PopupAjoutLot':
                    $cas = 'contexte_metier_consultation';
                    $ref = Atexo_Util::atexoHtmlEntities($_GET['id']);
                    $org = Atexo_Util::atexoHtmlEntities($_GET['orgAccronyme']);
                    break;
                case 'Agent.popUpAjoutRegistrePapier':
                    $cas = 'contexte_metier_consultation';
                    $ref = Atexo_Util::atexoHtmlEntities($_GET['id']);
                    $org = Atexo_CurrentUser::getCurrentOrganism();
                    break;
                case 'Agent.popUpAvisMembresCommission':
                    $cas = 'contexte_metier_consultation';
                    $ref = Atexo_Util::atexoHtmlEntities($_GET['id']);
                    $org = Atexo_CurrentUser::getCurrentOrganism();
                    break;
                case 'Agent.PopupChoixTypeAvis':
                    $cas = 'contexte_metier_consultation';
                    $ref = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id']));
                    $org = Atexo_CurrentUser::getCurrentOrganism();
                    break;
                case 'Agent.PopupConcentrateur':
                    $cas = 'contexte_metier_consultation';
                    $ref = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id']));
                    $org = Atexo_CurrentUser::getCurrentOrganism();
                    break;
                case 'Agent.popUpDateAnnulation':
                    $cas = 'contexte_metier_consultation';
                    $ref = Atexo_Util::atexoHtmlEntities($_GET['consultationId']);
                    $org = Atexo_CurrentUser::getCurrentOrganism();
                    break;
                case 'Agent.PopupDetailLot':
                    $cas = 'contexte_metier_consultation';
                    $ref = Atexo_Util::atexoHtmlEntities($_GET['id']);
                    $org = Atexo_Util::atexoHtmlEntities($_GET['orgAccronyme']);
                    break;
                case 'Agent.PopUpEvaluationOffre':
                    $cas = 'contexte_metier_consultation';
                    $ref = Atexo_Util::atexoHtmlEntities($_GET['id']);
                    $org = Atexo_CurrentUser::getCurrentOrganism();
                    break;
                case 'Agent.popUpImporterEnveloppe':
                    $cas = 'contexte_metier_consultation';
                    $ref = Atexo_Util::atexoHtmlEntities($_GET['id']);
                    $org = Atexo_CurrentUser::getCurrentOrganism();
                    break;
                case 'Agent.popUpRenseignerStatut':
                    $cas = 'contexte_metier_consultation';
                    $ref = Atexo_Util::atexoHtmlEntities($_GET['id']);
                    $org = Atexo_CurrentUser::getCurrentOrganism();
                    break;
                case 'Agent.PopUpRenseignerDecision':
                    $cas = 'contexte_metier_consultation';
                    $ref = Atexo_Util::atexoHtmlEntities($_GET['id']);
                    $org = Atexo_CurrentUser::getCurrentOrganism();
                    break;
                case 'Agent.DetailAnalyse':
                    $cas = 'contexte_metier_consultation';
                    $ref = Atexo_Util::atexoHtmlEntities($_GET['id']);
                    $org = Atexo_CurrentUser::getCurrentOrganism();
                    break;
                case 'Agent.ConsultationDetailReponse':
                    $cas = 'contexte_metier_consultation';
                    $ref = Atexo_Util::atexoHtmlEntities($_GET['id']);
                    $org = Atexo_CurrentUser::getCurrentOrganism();
                    break;
                case 'Agent.ConsultationsDetailAccordCadre':
                    $cas = 'contexte_metier_consultation';
                    $ref = Atexo_Util::atexoHtmlEntities($_GET['id']);
                    $org = Atexo_CurrentUser::getCurrentOrganism();
                    break;
                case 'Agent.HistoriqueConsultation':
                    $cas = 'contexte_metier_consultation';
                    $ref = Atexo_Util::atexoHtmlEntities($_GET['id']);
                    $org = Atexo_CurrentUser::getCurrentOrganism();
                    break;
                case 'Agent.PubliciteConsultationMol':
                    $cas = 'contexte_metier_consultation';
                    $ref = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id']));
                    $org = Atexo_CurrentUser::getCurrentOrganism();
                    break;
                case 'Agent.RedactionPiecesConsultation':
                    $cas = 'contexte_metier_consultation';
                    $ref = Atexo_Util::atexoHtmlEntities(base64_decode($_GET['id']));
                    $org = Atexo_CurrentUser::getCurrentOrganism();
                    break;
            }

            switch ($cas) {
                case 'contexte_metier_consultation':
                    if (!empty($ref) && !empty($org)) {
                        $consultation = (new Atexo_Consultation())->retrieveConsultation($ref, $org);
                        $this->saveConsultation($consultation);
                    }
                    break;
            }
        }
    }
}
