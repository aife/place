<?php

namespace Application\Controls;

use Application\Service\Atexo\Controller\Atexo_Controller_Front;
use Prado\Prado;
use Prado\Web\UI\WebControls\TImage;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PictoCheckOk extends TImage
{
    public $_parentPage;

    public function onLoad($param)
    {
        $this->setImageUrl(Atexo_Controller_Front::t().'/images/picto-check-ok.gif');
        if ('ChoixInvites' == $this->getParentPage() || 'FormulaireConsultation' == $this->getParentPage()) {
            $this->setAlternateText(Prado::localize('ICONE_INVITE_PERMANENT'));
            $this->setToolTip(Prado::localize('ICONE_INVITE_PERMANENT'));
        }
    }

    public function getParentPage()
    {
        return $this->_parentPage;
    }

    public function setParentPage($value)
    {
        $this->_parentPage = $value;
    }
}
