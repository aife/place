<?php

namespace Application\Controls;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonGuidesPeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Module;
use Prado\Prado;

class AtexoGuides extends MpeTTemplateControl
{
    public $accesFrom;
    public $typeGuides;
    public $libelle;
    public $idUnicite;

    public function setIdUnicite($idUnicite)
    {
        $this->idUnicite = $idUnicite;
    }

    public function getIdUnicite()
    {
        return $this->idUnicite;
    }

    public function setAccesFrom($accesFrom)
    {
        $this->accesFrom = $accesFrom;
    }

    public function getAccesFrom()
    {
        return $this->accesFrom;
    }

    public function setTypeGuides($typeGuides)
    {
        $this->typeGuides = $typeGuides;
    }

    public function getTypeGuides()
    {
        return $this->typeGuides;
    }

    public function setlibelle($libelle)
    {
        $this->libelle = $libelle;
    }

    public function Getlibelle()
    {
        return $this->libelle;
    }

    public function onLoad($param)
    {
        if (Atexo_Module::isEnabled('PublierGuides')) {
            self::fillRepeaterGuides();
        }
    }

    /*
     * remplir le repeater contenant la liste des guides multimedia
     */
    public function fillRepeaterGuides()
    {
        $arrayGuides = self::retrieveAllGuides($this->getAccesFrom(), $this->getTypeGuides()); //print_r($arrayGuides);exit;
        if (is_countable($arrayGuides) ? count($arrayGuides) : 0) {
            $this->RepeaterGuides->DataSource = $arrayGuides;
            $this->RepeaterGuides->DataBind();
            $this->panelGuides->setVisible(true);
        } else {
            $this->panelGuides->setVisible(false);
        }
    }

    /*
     * reciperer la liste des guides repondant aux criteres
     */
    public function retrieveAllGuides($accesFrom, $type)
    {
        $langue = Atexo_CurrentUser::readFromSession('lang');
        if (!$langue) {
            $langue = Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE');
        }
        $c = new Criteria();
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c->add(CommonGuidesPeer::ACCES_FROM, $accesFrom);
        $c->add(CommonGuidesPeer::TYPE, $type);
        $c->add(CommonGuidesPeer::LANGUE, $langue);
        $arrayGuides = CommonGuidesPeer::doSelect($c, $connexion);

        if ($arrayGuides) {
            return $arrayGuides;
        }

        return false;
    }

    /*
     * return le titre d'un ensemble des guides
     */
    public function getTitle()
    {
        return match ($this->typeGuides) {
            'enchere' => Prado::localize('TEXT_ENCHERES_ELECTRONIQUES_INVERSEES'),
            'soumission' => Prado::localize('TEXT_SOUMISSION_ELECTRONIQUES'),
            'achat' => Prado::localize('TEXT_ACHAT_GROUPES_ELECTRONIQUE'),
            'consultation' => Prado::localize('DEFINE_CONSULTATION'),
            default => '',
        };
    }
}
