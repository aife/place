 <!--Debut active message de avertissement-->
<com:TActivePanel ID="panelMessage" Display="<%=$this->getDisplayStyle()%>" cssClass="bloc-message form-bloc-conf msg-avertissement">
    <div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
    <div class="content">
        <div class="<%=$this->getCssClass()%>"> 
            <!--Debut Message type d'erreur general--> 
            <span><com:TActiveLabel ID="labelMessage" /></span>
            <!--Fin Message type d'erreur general--> 
        </div>
        <div class="breaker"></div>
    </div>
    <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
</com:TActivePanel>
<!--Fin active message de avertissement-->
