<com:TConditional condition="$this->TemplateControl->getVisible()">
    <prop:trueTemplate>
        <div class="modal kt-ajaxmodal fade" id="modalGestionUtilisateurs" tabindex="-1" role="dialog" aria-labelledby="modalGestionUtilisateurs">
            <div class="modal-dialog modal-lg" role="document">
                 <com:TForm Attributes.name="form" Attributes.class="form" Attributes.method="POST">
                    <div class="modal-content form-horizontal">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h2 class="h4 modal-title" id="modalGestionUtilisateursTitle">
                                Modifier les informations
                            </h2>
                        </div>
                        <div class="modal-body">
                            <div class="popup-moyen" id="container">
                                <!--Debut Bloc Informations Utilisateurs-->
                                <div class="clearfix m-b-2">
                                    <div class="small pull-right">
                                        <com:TTranslate>DEFINE_SYMBOLE</com:TTranslate>
                                        <span class="text-danger">*</span>
                                        <com:TTranslate>DEFINE_INDIQUE_CHAMPS_OBLIGATOIRES</com:TTranslate>
                                    </div>
                                </div>
                                <div action="" id="infosUtilisateur">
                                    <com:AtexoValidationSummary  ValidationGroup="validateCreateCompte" />
                                    <com:TLabel id="labelServerSide"/>
                                    <com:PanelMessageErreur ID="panelMessageErreur"/>
                                    <!--Debut Bloc Informations personnelles-->
                                    <com:PanelComptePersonnel ID="PanelInscrit"/>
                                    <com:THiddenField id="validerSelectionEtablissement" />
                                    <com:TCustomValidator
                                            ControlToValidate="validerSelectionEtablissement"
                                            ValidationGroup="validateCreateCompte"
                                            Display="Dynamic"
                                            EnableClientScript="true"
                                            ID="validateurListeEtablissements"
                                            onServerValidate="validerListeEtablissements"
                                            Text=" ">
                                    </com:TCustomValidator>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="clearfix">
                                <div class="pull-left">
                                    <button type="reset" class="btn btn-sm btn-default btn-reset" data-dismiss="modal">
                                        <%=Prado::localize('DEFINE_ANNULER')%>
                                    </button>
                                </div>
                                <div class="pull-right">
                                    <com:TButton cssClass="btn btn-sm btn-primary "
                                                 Text="<%=Prado::localize('TEXT_ENREGISTER')%>"
                                                 ID="buttonEnregistrer"
                                                 ValidationGroup="validateCreateCompte"
                                                 Attributes.title="<%=Prado::localize('TEXT_ENREGISTER')%>"
                                                 onclick="saveChange" />
                                </div>
                            </div>
                            <com:TLabel ID="labelClose"/>
                        </div>
                    </div>
                </com:TForm>
            </div>
        </div>
    </prop:trueTemplate>
</com:TConditional>