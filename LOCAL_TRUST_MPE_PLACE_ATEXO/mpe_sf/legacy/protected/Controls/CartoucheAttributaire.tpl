<div class="form-bloc margin-0">
	<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
	<div class="content">
		<div class="line">
			<div class="intitule-150"><strong><com:TTranslate>DEFINE_ATTRIBUTAIRE</com:TTranslate> :</strong></div>
			<div class="content-bloc bloc-590">
				<div class="attributaire"><a href="#" class="popover-detail-bottom" tabindex="0" data-toggle="popover" data-popover-content="#detail_lot_4_attributaire_1" data-placement="bottom"><com:TLabel id="nomEntreprise"/></a> - <com:TLabel id="codePostalEtVilleEntreprise" /> <com:TActiveLabel id="adresse"/></div>
				<!-- Content for Popover Detail -->
					<com:PopinInfosAttributaire id="idPopinInfosAttributaire"
												IdDiv="detail_lot_4_attributaire_1"
												RefreshComposant="DOIT ETRE DERNIER ELEMENT POUR CHARGER LE COMPOSANT"
							/>
				<!-- End Content for Popover Detail -->
			</div>

		</div>
		<com:TActivePanel cssClass="line" ID="panelMontantAttribue">
			<div class="intitule-150"><strong><com:TTranslate>MONTANT_ATTRIBUE</com:TTranslate> :</strong></div>
			<div class="content-bloc bloc-590">
				<div><com:TLabel id="montant"/> <com:TTranslate>DEFINE_UNITE_HT</com:TTranslate></div>
			</div>
		</com:TActivePanel>
		<com:TActivePanel cssClass="line" ID="panelMontantMaxAttribue">
			<div class="intitule-150"><strong><com:TTranslate>MONTANT_MAX_ESTIME</com:TTranslate> :</strong></div>
			<div class="content-bloc bloc-590">
				<div><com:TLabel id="montantMax"/> <com:TTranslate>DEFINE_UNITE_HT</com:TTranslate></div>
			</div>
		</com:TActivePanel>
		<div class="breaker"></div>
	</div>
	<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
</div>
<div class="spacer-mini"></div>