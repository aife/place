<?php

namespace Application\Controls;

use Application\Propel\Mpe\CommonAgent;
use Application\Propel\Mpe\CommonAgentQuery;
use Application\Propel\Mpe\CommonOrganisme;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Util;

/*
 * commentaires
 * @author khadija Chouika <khadija.chouika@atexo.com>
 * @copyright Atexo 2013
 * @version 0
 * @since MPE-4.4
 * @package Controls
 */
class BlocInformationsCompte extends MpeTTemplateControl
{
    public $_titre;
    public $_sousTitre;

    public function chargerAgent($idAgent, $titre = '', $soustitre = '', $toUtf8 = false)
    {

        $agent = (new CommonAgentQuery())->findOneById($idAgent);
        $this->setViewState('Agent', $agent);
        if ($agent instanceof CommonAgent) {
            $this->setViewState('idAgent', $idAgent);
            if ($toUtf8) {
                $this->nom_prenom->Text = Atexo_Util::toUtf8($agent->getNom()).' '.Atexo_Util::toUtf8($agent->getPrenom());
                $this->identifiant->Text = Atexo_Util::toUtf8($agent->getLogin());
                $this->email->Text = Atexo_Util::toUtf8($agent->getEmail());
                $org = Atexo_Organismes::retrieveOrganismeByAcronyme($agent->getOrganisme());
                if ($org instanceof CommonOrganisme) {
                    $orgDenomination = $org->getDenominationOrg();
                    $this->entite->Text = $orgDenomination;
                }
                $this->service->Text = ' ';
                $entityPath = Atexo_EntityPurchase::getSigleLibelleEntityById($agent->getServiceId(), $agent->getOrganisme());
                if ($entityPath) {
                    $this->service->Text = $entityPath;
                }
                $this->titre->Text = Atexo_Util::toUtf8($titre);
                $this->sousTitre->Text = Atexo_Util::toUtf8($soustitre);
            } else {
                $this->nom_prenom->Text = $agent->getNom().' '.$agent->getPrenom();
                $this->identifiant->Text = $agent->getLogin();
                $this->email->Text = $agent->getEmail();
                $org = Atexo_Organismes::retrieveOrganismeByAcronyme($agent->getOrganisme());
                if ($org instanceof CommonOrganisme) {
                    $orgDenomination = $org->getDenominationOrg();
                    $this->entite->Text = $orgDenomination;
                }
                $this->service->Text = ' ';
                $entityPath = Atexo_EntityPurchase::getSigleLibelleEntityById($agent->getServiceId(), $agent->getOrganisme());
                if ($entityPath) {
                    $this->service->Text = $entityPath;
                }
                $this->titre->Text = $titre;
                $this->sousTitre->Text = $soustitre;
            }
        }
    }

    public function setTitre($value)
    {
        $this->_titre = $value;
    }

    public function getTitre()
    {
        return $this->_titre;
    }

    public function setSousTitre($value)
    {
        $this->_sousTitre = $value;
    }

    public function getSousTitre()
    {
        return $this->_sousTitre;
    }
}
