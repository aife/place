<?php

namespace Application\Controls;

/**
 * permet de visualiser les infos de la signature
 * ce composant se remplit en javaScript.
 *
 * @author Ayoub SOUID AHMED  <ayoub.souidahmed@atexo.com>
 *
 * @version 1.0
 *
 * @since ESR-2015
 *
 * @copyright Atexo 2015
 */
class AtexoInfosSignature extends MpeTTemplateControl
{
}
