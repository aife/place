<div class="form-field form-horizontal" style="display:block;">
	<div class="content">
		<div class="panel panel-primary">
            <h2>
                <com:TTranslate>DEFINE_TEXT_IDENTIFICATION_ENTITE</com:TTranslate>
            </h2>
			<div class="panel-body">
				<!-- BEGIN LIBELLE -->
                <com:TPanel cssClass="gestion--achat-btns">
                    <com:TPanel ID="buttonFusionService" Visible="true">
                        <com:TButton
                                CssClass="depl-entite"
                                Attributes.value="<%=Prado::localize('DEPLACER_DONNEES_ENTITE_ACHAT')%>"
                                Attributes.title="<%=Prado::localize('DEPLACER_DONNEES_ENTITE_ACHAT')%>"
                                OnClick="redirectToFusionService"
                        />
                    </com:TPanel>
                    <com:TPanel ID="buttonSuppression" Visible="true">
                        <com:TButton
                                CssClass="suppr-entite"
                                Enabled="<%=$this->entiteValide%>"
                                Attributes.value="<%=Prado::localize('TEXT_BOUTON_SUPPRESSION_ENTITE')%>"
                                Attributes.title="<%=Prado::localize('TEXT_BOUTON_SUPPRESSION_ENTITE')%>"
                                OnClick="onClickDelete"
                                Attributes.onclick="javascript:if(!confirm('<%=Prado::localize('TEXT_ALERTE_SUPPRESSION_ENTITE')%>'))return false;"
                        />
                    </com:TPanel>
                </com:TPanel>
				<div class="line">
					<label class="control-label">
						<com:TTranslate>DEFINE_LIBELLE_EA_PMI</com:TTranslate> :
					</label>
					<com:TLabel ID='libelle' />
				</div>
				<com:TPanel visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('TraduireEntiteAchatArabe')) ? true:false%>">
					<label class="control-label col-md-2">
						<com:TTranslate>DEFINE_LIBELLE_EA_PMI_AR</com:TTranslate> :
					</label>
					<div class="col-md-10">
						<com:TLabel ID='libelle_ar' />
					</div>
				</com:TPanel>
				<!-- END LIBELLE -->

				<!-- BEGIN SIGLE -->
				<div class="line">
					<label class="control-label">
						<com:TTranslate>DEFINE_TEXT_SIGLE_EA_PMI</com:TTranslate> :
					</label>
					<com:TLabel ID='sigle' />
				</div>
				<!-- END SIGLE -->

				<!-- Début Ligne Entité de rattachement -->
				<div class="line">
					<label class="control-label">
						<com:TTranslate>DEFINE_TEXT_ENTITE_RATTACHEMENT_EA_PMI</com:TTranslate> :
					</label>
					<com:TLabel ID='entiterattachement' />
				</div>
				<!-- Fin Ligne Entité de rattachement -->

				<!-- Debut Ligne Forme juridique -->
				<div class="line">
					<label class="control-label">
						<com:TTranslate>TEXT_CATEGORIE_JURIDIQUE</com:TTranslate> :
					</label>
					<com:TLabel ID='formejuridique' />
				</div>
				<!-- Fin Ligne Forme juridique -->
				<div class="spacer"></div>
				<!-- Debut Ligne Adresse -->
				<div class="line">
					<label class="control-label">
						<com:TTranslate>DEFINE_TEXT_ADRESSE_PRINCIPAL_EA_PMI</com:TTranslate> :
					</label>
					<com:TLabel ID='adresseprincipal' />
				</div>
				<com:TPanel CssClass="line" visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('TraduireEntiteAchatArabe')) ? true:false%>">
					<label class="control-label">
						<com:TTranslate>DEFINE_TEXT_ADRESSE_PRINCIPAL_EA_PMI_AR</com:TTranslate> :
					</label>
					<com:TLabel ID='adresseprincipal_ar' />
				</com:TPanel>
				<div class="line">
					<label class="control-label">
						<com:TTranslate>DEFINE_TEXT_ADRESSE_SECONDAIRE_EA_PMI</com:TTranslate> :
					</label>
					<com:TLabel ID='adressesuite' />
				</div>
				<com:TPanel CssClass="line" visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('TraduireEntiteAchatArabe')) ? true:false%>">
					<label class="control-label">
						<com:TTranslate>DEFINE_TEXT_ADRESSE_SECONDAIRE_EA_PMI_AR</com:TTranslate> :
					</label>
					<com:TLabel ID='adressesuite_ar' />
				</com:TPanel>
				<div class="line">
					<label class="control-label">
						<com:TTranslate>DEFINE_CODE_POSTAL</com:TTranslate> :
					</label><com:TLabel ID='codepostal' />
				</div>
				<div class="line">
					<label class="control-label">
						<com:TTranslate>TEXT_VILLE</com:TTranslate> :
					</label>
					<com:TLabel ID='ville' />
				</div>
				<com:TPanel CssClass="line" visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('TraduireEntiteAchatArabe')) ? true:false%>">
					<label class="control-label">
						<com:TTranslate>TEXT_VILLE_AR</com:TTranslate> :
					</label>
					<com:TLabel ID='ville_ar' />
				</com:TPanel>
				<div class="line">
					<label class="control-label">
						<com:TTranslate>TEXT_PAYS</com:TTranslate> :
					</label>
					<com:TLabel ID='pays' />
				</div>
				<!--Fin Ligne ADRESSE -->

				<!-- BEGIN CONTRAT -->
				<div class="line">
					<label class="control-label">
						<com:TTranslate>DEFINE_TEXT_TEL_EA_PMI</com:TTranslate> :
					</label>
					<com:TLabel ID='tel'/>
				</div>
				<div class="line">
					<label class="control-label">
						<com:TTranslate>DEFINE_TEXT_TELCOPIE_EA_PMI</com:TTranslate> :
					</label>
					<com:TLabel ID='fax' />
				</div>
				<div class="line">
					<label class="control-label">
						<com:TTranslate>DEFINE_EMAIL_EA_PMI</com:TTranslate> :
					</label>
					<com:TLabel ID='mail' />
				</div>
				<com:TPanel ID="panelSiret" cssClass="line">
					<label class="control-label">
						<com:TTRanslate>TEXT_SIRET</com:TTRanslate> :
					</label>
					<com:TLabel ID='siret' />
				</com:TPanel>
				<com:TPanel ID="panelCodeAcheteur" cssClass="line">
					<label class="control-label">
						<com:TTranslate>CODE_ACHETEUR</com:TTranslate>
					</label>
					<com:TLabel ID='codeAcheteur' />
				</com:TPanel>
				<com:TPanel ID="affichageService" cssClass="line" visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('GestionMandataire')) ? true:false%>">
					<div class="intitule-150">
						<label for="affichageService">
							<com:TTranslate>VISIBILITE_SERVICE</com:TTranslate> :
						</label>
					</div>
					<com:TLabel ID='afficherService' />
				</com:TPanel>
				<!-- END CONTRAT -->

				<!-- BEGIN FUSEAU HORAIRE -->
				<com:TPanel ID="idHorloge" visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('FuseauHoraire', Application\Service\Atexo\Atexo_CurrentUser::getCurrentOrganism())) ? true:false%>">
					<div class="line">
						<div class="intitule-bloc"><com:TTranslate>DEFINE_UTILISATION_FUSEAU_HORAIRE</com:TTranslate> : </div> <span class="pos-t-2"><com:TLabel ID='utiliserFuseauHoraire' /></span>
					</div>
					<com:TPanel visible="<%=((new Application\Service\Atexo\Atexo_EntityPurchase())->getEtatActivationFuseauHoraire( Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($_GET['idEntite']), Application\Service\Atexo\Atexo_CurrentUser::getCurrentOrganism())) ? true:false%>">
						<div class="line">
							<div class="intitule-bloc bloc-180"><com:TTranslate>DEFINE_DECALAGE_PAR_RAPPORT_GMT</com:TTranslate> : <com:TLabel ID='decalageHoraire' /></div>
						</div>
						<div class="line">
							<div class="intitule-180"><com:TTranslate>DEFINE_LIEU_RESIDENCE</com:TTranslate> : <com:TLabel ID='lieuResidence' /></div>
						</div>
					</com:TPanel>
				</com:TPanel>
				<!-- END FUSEAU HORAIRE -->
				<!-- BEGIN CHORUS -->
				<com:TPanel ID="panelChorus">
					<div class="line">
						<div class="intitule-bloc"><com:TTranslate>DEFINE_UTILISATION_CHORUS</com:TTranslate> : </div><span class="pos-t-2"><com:TLabel ID='utiliserChorus' /></span>
					</div>
				</com:TPanel>
				<!-- END CHORUS -->
				<!-- BEGIN ID EXTERNE SSO -->
				<com:TPanel cssClass="line" visible="<%=((new Application\Service\Atexo\Atexo_Module())->isSaisieManuelleActive(Application\Service\Atexo\Atexo_CurrentUser::getCurrentOrganism()))? true:false%>">
					<div class="intitule-150">
						<label for="idExterneSSO">
							<com:TTranslate>ID_EXTERNE_SSO</com:TTranslate> :
						</label>
					</div>
					<com:TLabel ID='idExterneSSO' />
				</com:TPanel>
				<!-- FIN ID EXTERNE SSO -->
				<!-- BEGIN ID ENTITE -->
				<com:TPanel id="panelIdEntiteL" cssClass="line" Visible="<%=!Application\Service\Atexo\Atexo_Config::getParameter('SPECIFIQUE_PLACE')%>">
					<div class="intitule-150">
						<label for="idEntiteL">
							<com:TTranslate>TEXT_ID_ENTITE</com:TTranslate> :
						</label>
					</div>
					<com:TLabel ID='idEntiteL' />
				</com:TPanel>
				<!-- FIN ID ENTITE -->
			</div>
			<div class="clearfix">
				<com:TPanel ID="buttonCreationModification" Visible="true">
					<div class="boutons-line">
						<com:TConditional condition="Application\Service\Atexo\Atexo_CurrentUser::getIdServiceAgentConnected()!=0">
							<prop:trueTemplate>
								<com:TButton
										id="btnRattachementEntiteAchat"
										CssClass="btn btn-sm btn-default pull-right bouton-moyen-140 float-right disabled-input"
										Enabled="<%=$this->entiteValide%>"
										Attributes.value="<%=Prado::localize('DEFINE_RATTACHEMENT_ENTITE_ACHAT_VALUE')%>"
										Attributes.title="<%=Prado::localize('DEFINE_RATTACHEMENT_ENTITE_ACHAT')%>"
										OnClick="RedirectUrlCreation"
								/>
								<com:TButton
										CssClass="btn btn-sm btn-default pull-right bouton-moyen-120 float-left disabled-input"
										Enabled="<%=$this->entiteValide%>"
										Attributes.value="<%=Prado::localize('BOUTON_MODIFIER')%>"
										Attributes.title="<%=Prado::localize('BOUTON_MODIFIER')%>"
										OnClick="RedirectUrlUpdate"
								/>
							</prop:trueTemplate>
							<prop:falseTemplate>
								<com:TButton
										id="btnRattachementEntiteAchat"
										CssClass="btn btn-sm btn-default pull-right bouton-moyen-140 float-right"
										Attributes.value="<%=Prado::localize('DEFINE_RATTACHEMENT_ENTITE_ACHAT_VALUE')%>"
										Attributes.title="<%=Prado::localize('DEFINE_RATTACHEMENT_ENTITE_ACHAT')%>"
										OnClick="RedirectUrlCreation"
								/>
								<com:TButton
										CssClass="btn btn-sm btn-default pull-right bouton-moyen-120 float-left"
										Attributes.value="<%=Prado::localize('BOUTON_MODIFIER')%>"
										Attributes.title="<%=Prado::localize('BOUTON_MODIFIER')%>"
										OnClick="RedirectUrlUpdate"
								/>
							</prop:falseTemplate>
						</com:TConditional>

						<com:TButton
								ID="lienGestionGAOA"
								CssClass="bouton-moyen-120 btn-gestion float-right"
								Attributes.value="<%=Prado::localize('GERER_LES_OA_GA')%>"
								Attributes.title="<%=Prado::localize('GERER_LES_OA_GA')%>"
								OnClick="RedirectUrlGestionOAEtGA"
								Visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('InterfaceChorusPmi') && Application\Service\Atexo\Atexo_CurrentUser::hasHabilitation('GererOaGa'))%>"
						/>
					</div>
				</com:TPanel>
			</div>
		</div>
		<com:TPanel cssclass="panel panel-primary" id="blocIdentificationArchive">
			<!-- BEGIN IDENTIFICATION SERVICE ARCHIVES -->
			<h2>
				<com:TTranslate>IDENTIFICATION_SERVICE_ARCHIVES</com:TTranslate>
			</h2>
			<div class="panel-body">
				<!-- BEGIN IDENTIFICATION SERVICE ARCHIVE -->
				<div class="line">
					<div class="intitule-150"><com:TTranslate>TEXT_NOM</com:TTranslate></div>
					<com:TLabel id="nomServiceArchive" cssclass="content-bloc bloc-600"/>
				</div>
				<div class="line">
					<div class="intitule-150"><com:TTranslate>TEXT_IDENTIFIANT</com:TTranslate>	</div>
					<com:TLabel id="idServiceArchive" cssclass="content-bloc bloc-600"/>
				</div>
				<!-- END IDENTIFICATION SERVICE ARCHIVE -->
			</div>
			<!-- END IDENTIFICATION SERVICE ARCHIVES -->
		</com:TPanel>
	</div>
</div>
<com:TLabel ID="scriptJS"/>