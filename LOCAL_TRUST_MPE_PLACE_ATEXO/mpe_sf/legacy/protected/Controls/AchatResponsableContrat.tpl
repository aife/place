<!--Debut Ligne Achat responsable-->
<div class="line" id="panel_achatResponsable">
	<div class="intitule<%=$this->getCssPersonalisee()?'-150':' col-150'%>"><com:TTranslate>DEFINE_ACHAT_RESPONSABLE</com:TTranslate><span class="champ-oblig">*</span> :</div>
	<div class="content-bloc bloc-570">
		<div id="achat-responsable" class="content-bloc bloc-580">
			<clauses-n1 v-for="(value, index) in values"
						:data="value"
						:token="token"
						:values-save="valuesSave"
						:key="index"
						:uri="'<%=$this->getUrl()%>'"
						:yes-label="'<%=$this->getLabelTrans('DEFINE_OUI')%>'"
						:no-label="'<%=$this->getLabelTrans('DEFINE_NON')%>'"
						:required-field="'<%=$this->getLabelTrans('CHAMP_OBLIGATOIRE')%>'"
						:search-criteria="'<%=$this->getLabelTrans('RECHERCHER_UN_CRITERE')%>'"
						:tiny-emitter="tinyEmitter"
						@send-add-value-clause1="add"
						@send-remove-value-clause1="remove"
			></clauses-n1>
		</div>
		<com:TTextBox Display="None" id="achatResponsableFormValue"/>
	</div>

	<com:TCustomValidator
			Id="validatorClauseSocialesContrat"
			ValidationGroup="validateInfosContrat"
			ControlToValidate="achatResponsableFormValue"
			ClientValidationFunction="validateClauseSocialeContrat"
			Enabled="true"
			Display="Dynamic"
			EnableClientScript="true"
			ErrorMessage="<%=Prado::localize('DEFINE_CLAUSE_SOCIALE_INSERTION_HANDICAP_ERREUR')%>"
			Text="<span title='Champ obligatoire' class='check-invalide TestMessage'></span>">
		<prop:ClientSide.OnValidationError>
			console.log('error validatorClauseSocialesContrat');
			blockErreur = '<%=($this->getValidationSummary())%>';
			if(document.getElementById(blockErreur))
			document.getElementById(blockErreur).style.display='';
			if(document.getElementById('ctl0_CONTENU_PAGE_blocBoutons'))
			document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
		</prop:ClientSide.OnValidationError>
	</com:TCustomValidator
	>
	<com:TCustomValidator
			Id="validateClauseEnvironnementalesContrat"
			ValidationGroup="validateInfosContrat"
			ControlToValidate="achatResponsableFormValue"
			ClientValidationFunction="validateClauseEnvironnementaleContrat"
			Enabled="true"
			Display="Dynamic"
			EnableClientScript="true"
			ErrorMessage="<%=Prado::localize('CLAUSES_ENVIRONNEMENTALES_MESSAGE')%>"
			Text="<span title='Champ obligatoire' class='check-invalide'></span>">
		<prop:ClientSide.OnValidationError>
			blockErreur = '<%=($this->getValidationSummary())%>';
			if(document.getElementById(blockErreur))
			document.getElementById(blockErreur).style.display='';
		</prop:ClientSide.OnValidationError>
	</com:TCustomValidator>

	<com:TCustomValidator
			Id="validatorClauseSocialesContratModificationContrat"
			ValidationGroup="Validation"
			ControlToValidate="achatResponsableFormValue"
			ClientValidationFunction="validateClauseSocialeForPagePopupDecisionContrat"
			Enabled="true"
			Display="Dynamic"
			EnableClientScript="true"
			ErrorMessage="<%=Prado::localize('DEFINE_CLAUSE_SOCIALE_INSERTION_HANDICAP_ERREUR')%>"
			Text="<span title='Champ obligatoire' class='check-invalide TestMessage'></span>">
		<prop:ClientSide.OnValidationError>
			console.log('error validatorClauseSocialesContratModificationContrat');
			blockErreur = '<%=($this->getValidationSummary())%>';
			if(document.getElementById(blockErreur))
			document.getElementById(blockErreur).style.display='';
			if(document.getElementById('ctl0_CONTENU_PAGE_blocBoutons'))
			document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
		</prop:ClientSide.OnValidationError>
	</com:TCustomValidator
	>
	<com:TCustomValidator
			Id="validateClauseEnvironnementalesContratModificationContrat"
			ValidationGroup="Validation"
			ControlToValidate="achatResponsableFormValue"
			ClientValidationFunction="validateClauseEnvironnementaleForPagePopupDecisionContrat"
			Enabled="true"
			Display="Dynamic"
			EnableClientScript="true"
			ErrorMessage="<%=Prado::localize('CLAUSES_ENVIRONNEMENTALES_MESSAGE')%>"
			Text="<span title='Champ obligatoire' class='check-invalide'></span>">
		<prop:ClientSide.OnValidationError>
			console.log('error validateClauseEnvironnementalesContratModificationContrat');
			blockErreur = '<%=($this->getValidationSummary())%>';
			if(document.getElementById(blockErreur))
			document.getElementById(blockErreur).style.display='';
		</prop:ClientSide.OnValidationError>
	</com:TCustomValidator>
</div>
<com:TLabel id="script" />
<div class="spacer-small"></div>
<!--Fin Ligne Achat responsable-->
