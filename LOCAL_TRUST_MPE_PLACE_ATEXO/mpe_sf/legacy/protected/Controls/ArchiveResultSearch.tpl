<!--Debut partitionneur-->
<com:TPanel ID="panelNoElementFound">
	<h2><com:TTranslate>DEFINE_AUCUN_RESULTAT</com:TTranslate>&nbsp;</h2>
</com:TPanel>
<com:TPanel ID="panelMoreThanOneElementFound">
<div class="line-partitioner">
	<h2><com:TTranslate>DEFINE_NOMBRE_RESULTAT</com:TTranslate>&nbsp;<com:TLabel ID="nombreElement"/><script>var nbrResultats=<%=(null == $this->ArchiveTableauBordRepeater->DataSource) ? 1 : count($this->ArchiveTableauBordRepeater->DataSource)+1%>;</script></h2>
	<com:THiddenField value="<%=(null == $this->ArchiveTableauBordRepeater->DataSource) ? 1 : count($this->ArchiveTableauBordRepeater->DataSource)+1%>" ID="nombreResultatsRecherche"/>

	<div class="partitioner">
		<div class="intitule"><strong><label for="allCons_nbResultsTop"><com:TTranslate>DEFINE_TEXT_AFFICHER</com:TTranslate></label></strong></div>
		<com:TDropDownList ID="nombreResultatAfficherTop" AutoPostBack="true" onSelectedIndexChanged="changePagerLenght" Attributes.title='<%=Prado::localize("NOMBRE_DE_RESULTAT_PAR_PAGE")%>'>
			<com:TListItem Text="10" Selected="10" />
			<com:TListItem Text="20" Value="20" />
			<com:TListItem Text="50" Value="50" />
			<com:TListItem Text="100" Value="100" />
			<com:TListItem Text="500" Value="500" />
		</com:TDropDownList>
		<div class="intitule"><com:TTranslate>DEFINE_RESULTATS_PAGE</com:TTranslate></div>
			<label style="display:none;" for="allCons_pageNumberTop">Aller à la page</label>
			<com:TPanel ID="panelBouttonGotoPageTop" DefaultButton="DefaultButtonTop" CssClass="float-left">
				<com:TTextBox ID="numPageTop" Attributes.title='<%=Prado::localize("N_DE_LA_PAGE")%>' Text="1"/>
				<div class="nb-total ">/ <com:TLabel ID="nombrePageTop"/></div>
				<com:TButton ID="DefaultButtonTop"  OnClick="goToPage" Attributes.style="display:none"/>
			</com:TPanel>
		<div class="liens">
			<com:TPager ID="PagerTop"
				ControlToPaginate="ArchiveTableauBordRepeater"
				FirstPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-premier.gif' alt='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>' title='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>' />"
		   	 	LastPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-dernier.gif' alt='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>' title='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>' />"
		    	Mode="NextPrev"
				NextPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-suivant.gif' alt='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>' title='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>' />"
				PrevPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-precedent.gif' alt='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>' title='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>' />"
				OnPageIndexChanged="pageChanged"/>
		</div>
	</div>
</div>
<!--Fin partitionneur-->
<!--Debut Tableau consultations-->
<!--<com:TActivePanel id="panelRefresh">-->
<com:TRepeater ID="ArchiveTableauBordRepeater"
			   AllowPaging="true"
			   PageSize="10"
			   EnableViewState="true"
			   AllowCustomPaging="true"
			   >
<prop:HeaderTemplate>
<table summary="<%=Prado::localize('TEXT_TOUTES_CONSULTATION')%>" class="table-results">
	<caption> <com:TTranslate>TEXT_TOUTES_CONSULTATION</com:TTranslate></caption>
	<thead>
		<tr>
			<th class="top" colspan="7"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></th>
		</tr>
		<tr>
			<th class="check-col" abbr="Tous/Aucun" id="selection">
				<com:TActiveCheckBox
					ID="checkAll"
					OnCallBack="SourceTemplateControl.checkAll" />

				<label style="display:none" for="selection"><com:TTranslate>DEFINE_SELECTION</com:TTranslate></label>
			</th>
			<th class="col-100" id="allCons_ref" abbr="Ref">
					<%#$this->Page->ArchiveResultSearch->elementSortedWith('reference_utilisateur','open')%>
						<com:TTranslate>TEXT_REFERENCE</com:TTranslate>
					<%#$this->Page->ArchiveResultSearch->elementSortedWith('reference_utilisateur','close')%>
					<com:PictoSort CommandName="reference_utilisateur" OnCommand="Page.Trier" />
				<br />
				<com:TTranslate>DEFINE_PROCEDURE</com:TTranslate>
				<br />
				<com:TTranslate>DEFINE_STATUS</com:TTranslate>
				<br />
				<%#$this->Page->ArchiveResultSearch->elementSortedWith('nom_createur','open')%>
					<com:TTranslate>DEFINE_AUTEUR</com:TTranslate>
				<%#$this->Page->ArchiveResultSearch->elementSortedWith('nom_createur','close')%>
				<com:PictoSort CommandName="nom_createur" OnCommand="Page.Trier" />
			</th>
			<th class="col-30" id="allCons_details" abbr="Détails">&nbsp;</th>
			<th class="col-350" id="allCons_intitule" abbr="Intitule">
				<%#$this->Page->ArchiveResultSearch->elementSortedWith('intitule','open')%>
					<com:TTranslate>DEFINE_INTITULE</com:TTranslate>
				<%#$this->Page->ArchiveResultSearch->elementSortedWith('intitule','close')%>
				<com:PictoSort CommandName="intitule" OnCommand="Page.Trier" />
				/
				<%#$this->Page->ArchiveResultSearch->elementSortedWith('objet','open')%>
					<com:TTranslate>OBJET</com:TTranslate>
				<%#$this->Page->ArchiveResultSearch->elementSortedWith('objet','close')%>
				<com:PictoSort CommandName="objet" OnCommand="Page.Trier"  />
			</th>
			<th class="<%#($this->TemplateControl->isTypeConsultation()? 'col-110':'col-100')%>" id="allCons_registres">
				<com:TLabel Visible="<%#$this->TemplateControl->isTypeConsultation()%>">
					<com:TTranslate>DEFINE_REGISTRES</com:TTranslate> :	<com:PictoRegistre />
				</com:TLabel>
			</th>
			<th class="col-100" id="allCons_dateEnd" abbr="Date limite">
				<%#$this->Page->ArchiveResultSearch->elementSortedWith('dateFin','open')%>
					<com:TTranslate>DEFINE_DATE_LIMITE_REMIS_PLIS</com:TTranslate>
				<%#$this->Page->ArchiveResultSearch->elementSortedWith('dateFin','close')%>
				<com:PictoSort CommandName="datefin" OnCommand="Page.Trier"/>
			</th>
			<th class="actions" id="allCons_actions"><com:TTranslate>DEFINE_ACTIONS</com:TTranslate></th>
		</tr>
	</thead>
</prop:HeaderTemplate>
<prop:ItemTemplate>
	<tr class="<%#(($this->ItemIndex%2==0)? '':'on')%>" >
		<td class="check-col" headers="selection" >
			<com:THiddenField ID="idArchive" Value="<%#$this->Data->getId()%>" />
			<com:THiddenField ID="statutConsultation" Value="<%#$this->Data->getIdEtatConsultation()%>" />
			<com:TPanel Visible="<%#$this->Page->ArchiveResultSearch->getIsVisibleCheckBox($this->Data->getIdEtatConsultation())%>">
			<com:TActiveCheckBox ID="toUpdate" />
			</com:TPanel>
		</td>
		<td class="col-90" headers="allCons_ref">
			<com:TLabel ID="reference" Attributes.class="ref" Text="<%#$this->Page->ArchiveResultSearch->insertBrWhenTextTooLong($this->Data->getReferenceUtilisateur(),15)%>"/>
			<br />
			<com:TLabel ID="procedure" Text="<%#$this->Data->getAbreviationTypeProcedure()%>"/>
			<com:TPanel Visible="<%#$this->TemplateControl->isTypeConsultation()%>" CssClass="archive-etape<%#$this->Data->getStatusConsultation()%>sur7" Attributes.title="<%#$this->Page->ArchiveResultSearch->infoBulleEtapeConnsultation(($this->Data instanceof CommonConsultation)? $this->Data->getStatusConsultation():null)%>"></com:TPanel>
			<span class="auteur">
				<%#$this->Data->getNomCreateur()." ".$this->Data->getPrenomCreateur()%>
			</span>
		</td>
		<td class="col-30" headers="allCons_details">
			<a href="index.php?page=Agent.DetailConsultation&id=<%#$this->Data->getId()%>&archive=true"><img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-details.gif" alt="<%=Prado::localize('TEXT_VOIR_DETAIL')%>" title="<%=Prado::localize('TEXT_VOIR_DETAIL')%>" /></a>
			<com:THyperLink Visible="<%#Application\Service\Atexo\Atexo_Module::isEnabled('SuiviPassation', Application\Service\Atexo\Atexo_CurrentUser::getCurrentOrganism())%>" NavigateUrl="index.php?page=Agent.DonneesComplementairesConsultation&id=<%#$this->Data->getId()%>" CssClass="detail">
					<com:TImage ImageUrl="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-donnees-comps.gif" Attributes.alt="<%=Prado::localize('VOIR_DONNEES_COMPLEMENTAIRE')%>" ToolTip="<%=Prado::localize('VOIR_DONNEES_COMPLEMENTAIRE')%>" />
			</com:THyperLink>
		</td>
		<td class="col-350" headers="allCons_intitule">
			<div class="objet-line">
				<strong><com:TTranslate>DEFINE_INTITULE</com:TTranslate> :</strong>
				<com:TLabel ID="intitule" Text="<%#$this->Page->ArchiveResultSearch->insertBrWhenTextTooLong($this->Data->getIntituleTraduit(),65)%>"/>
			</div>
			<div class="objet-line">
				<strong><com:TTranslate>OBJET</com:TTranslate> :</strong>
				<%# Application\Service\Atexo\Atexo_Util::truncateTexte($this->Data->getObjetTraduit(),'65')%>
				<span style="<%#$this->Page->ArchiveResultSearch->isTextTruncated($this->Data->getObjetTraduit())%>" onmouseover="afficheBulle('ctl0_CONTENU_PAGE_ArchiveResultSearch_ArchiveTableauBordRepeater_ctl<%#($this->ItemIndex+1)%>_infosConsultation', this)" onmouseout="cacheBulle('ctl0_CONTENU_PAGE_ArchiveResultSearch_ArchiveTableauBordRepeater_ctl<%#($this->ItemIndex+1)%>_infosConsultation')" class="info-suite">...</span>
				<com:TPanel ID="infosConsultation" CssClass="info-bulle" ><div><%#$this->Data->getObjetTraduit()%></div></com:TPanel>
			    </div>
			</div>
			<div class="objet-line">
				<strong><com:TTranslate>DEFINE_COMMENTAIRE</com:TTranslate> :</strong>
				<com:TLabel ID="commentaire" Text="<%#$this->Page->ArchiveResultSearch->insertBrWhenTextTooLong($this->Data->getChampSuppInvisible(),65)%>"/>
			</div>
		</td>
		<td class="col-110 <%#($this->TemplateControl->isTypeAnnonce() ? 'center':'')%>" headers="allCons_registres">
			<com:TLabel Visible="<%#$this->TemplateControl->isTypeConsultation()%>">
				<div class="registre-line">
					<span class="intitule">
					 	<com:PictoRegistreRetraits Activate="true" CommandName="<%#$this->Data->getId()%>" OnCommand="Page.ArchiveResultSearch.redirectToRegistreRetrait"/>&nbsp;&nbsp;:
					 </span>
					<com:THyperLink ID="linkRegistreRetraitsConsultation" visible="true" NavigateUrl="index.php?page=Agent.GestionRegistres&id=<%#$this->Data->getId()%>&type=1">
						<com:TLabel ID="registreRetraits" Text="<%#$this->Data->getNombreTelecharegementElectronique()%> + <%#$this->Data->getNombreTelecharegementPapier()%>"/>
					</com:THyperLink>
				</div>
				<div class="registre-line">
					<span class="intitule">
						<com:PictoRegistreQuestions Activate="true" CommandName="<%#$this->Data->getId()%>" OnCommand="Page.ArchiveResultSearch.redirectToRegistreQuestion"/>&nbsp;&nbsp;:
					</span>
					<com:THyperLink ID="linkQuestionsConsultation" visible="true"  NavigateUrl="index.php?page=Agent.GestionRegistres&id=<%#$this->Data->getId()%>&type=3">
						<com:TLabel ID="questions" Text="<%#$this->Data->getNombreQuestionElectroniqueDces()%> + <%#$this->Data->getNombreQuestionPapierDces()%>"/>
					</com:THyperLink>
				</div>
				<div class="registre-line">
					<span class="intitule">
					    <com:PictoRegistreDepot Activate="true" CommandName="<%#$this->Data->getId()%>" OnCommand="Page.ArchiveResultSearch.redirectToRegistreDepot"/>&nbsp;&nbsp;:
					</span>
				    <com:THyperLink ID="linkRegistreDepot" visible="true" NavigateUrl="index.php?page=Agent.GestionRegistres&id=<%#$this->Data->getId()%>&type=5">
						<com:TLabel ID="registreDepot" Text="<%#$this->Data->getNombreOffreElectronique()%> + <%#$this->Data->getNombreOffrePapier()%>"/>
					</com:THyperLink>
				</div>
			</com:TLabel>
			<com:TLabel Visible="<%#$this->TemplateControl->isTypeAnnonce()%>">
			 	<com:TLabel Visible="<%#($this->Data->getIdTypeAvis()== Application\Service\Atexo\Atexo_Config::getParameter('TYPE_AVIS_ATTRIBUTION'))%>">
			 		<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-avis-attribution.gif" alt="<%=Prado::localize('ANNONCE_ATTRIBUTION')%>" title="<%=Prado::localize('ANNONCE_ATTRIBUTION')%>" />
			 	</com:TLabel>

			 	<com:TLabel Visible="<%#($this->Data->getIdTypeAvis()== Application\Service\Atexo\Atexo_Config::getParameter('TYPE_AVIS_INFORMATION'))%>">
			 		<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-avis-info.gif" alt="<%=Prado::localize('DEFINE_ANNONCE_INFORMATION')%>" title="<%=Prado::localize('DEFINE_ANNONCE_INFORMATION')%>" />
			 	</com:TLabel>
			</com:TLabel>
		</td>
		<td class="col-100" headers="allCons_dateEnd">
			<div class="cloture-line">
				 <com:TPanel Visible="<%#$this->Data instanceof Application\Propel\Mpe\CommonConsultation && $this->Data->getStatusConsultation()>= Application\Service\Atexo\Atexo_Config::getParameter('ETAT_CLOTURE')%>" >
			        <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-time-off.gif" alt="" />
			        <span class="time-red">
						<com:TLabel Text="<%#str_replace(' ','<br />', Application\Service\Atexo\Atexo_Util::iso2frnDateTime($this->Data->getDatefin()))%>"/>
					</span>
				</com:TPanel>
				<com:TPanel Visible="<%#$this->Data instanceof Application\Propel\Mpe\CommonConsultation && $this->Data->getStatusConsultation()< Application\Service\Atexo\Atexo_Config::getParameter('ETAT_CLOTURE')%>" >
			        <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-time-on.gif" alt="" />
			        <span class="time-green">
			        	<com:TLabel Text="<%#str_replace(' ','<br />', Application\Service\Atexo\Atexo_Util::iso2frnDateTime($this->Data->getDatefin()))%>"/>
					</span>
				</com:TPanel>
			</div>
		</td>
		<td class="actions" headers="allCons_actions">
			<com:TPanel Visible="<%#$this->Page->ArchiveResultSearch->getIsAjoutDocumentsExterneVisible($this->Data->getIdEtatConsultation())%>">
			<a href="index.php?page=Agent.ArchiveSuiviDocsExterne&id=<%#$this->Data->getId()%>"><img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-ajout-doc.gif" alt="<%=Prado::localize('TEXT_GESTION_DOCUMENTS_EXTERNES')%>" title="<%=Prado::localize('TEXT_GESTION_DOCUMENTS_EXTERNES')%>" /></a>
			</com:TPanel>
			<com:TPanel Visible="<%#$this->Page->ArchiveResultSearch->getIsAjoutMetadonneesVisible($this->Data->getIdEtatConsultation())%>">
			<a href="index.php?page=Agent.ArchiveMetadonnee&id=<%#$this->Data->getId()%>"><img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-metadonnees.gif" alt="<%=Prado::localize('TEXT_ENRICHIR_META_DONNEES')%>" title="<%=Prado::localize('TEXT_ENRICHIR_META_DONNEES')%>" /></a>
			</com:TPanel>
			<com:TPanel Visible="<%#$this->Page->ArchiveResultSearch->getIsVoirArborescenceVisible($this->Data->getIdEtatConsultation())%>">
			<a href="index.php?page=Agent.ArchiveArborescence&id=<%#$this->Data->getId()%>"><img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-acces-arbo.gif" alt="<%=Prado::localize('TEXT_VISUALISER_ARBORESCENCE')%>" title="<%=Prado::localize('TEXT_VISUALISER_ARBORESCENCE')%>" /></a>
			</com:TPanel>
			<com:TPanel Visible="<%#$this->Data->isDicTelechargeable()%>">
			<com:THyperLink
				id="linkDownloadDIC"
				Attributes.onclick="blockResubmit(this.id);<%#( !Application\Service\Atexo\Atexo_Module::isEnabled('ArchiveParLot') || !$this->Data->getAlloti())? 'showModalLoader();'  : ''%>"
				NavigateUrl="<%#$this->SourceTemplateControl->getUrlDownloadDic($this->Data)%>"

			>
					<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-dl-dossier.gif"
						 alt="<%=Prado::localize('TEXT_TELECHARGER_DIC')%>"
					 	 title="<%=Prado::localize('TEXT_TELECHARGER_DIC')%>" />
			</com:THyperLink>
			</com:TPanel>
			<com:TPanel Visible="<%#$this->Data->isDacTelechargeable()%>">
			<com:THyperLink
				id="linkDownloadDAC"
				Attributes.onclick="blockResubmit(this.id);<%#( !Application\Service\Atexo\Atexo_Module::isEnabled('ArchiveParLot') || !$this->Data->getAlloti())? 'showModalLoader();'  : ''%>"
				NavigateUrl="<%#$this->SourceTemplateControl->getUrlDownloadDac($this->Data)%>"
			>
				<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-dl-dossier.gif"
					 alt="<%=Prado::localize('TELECHARGER_DAC')%>"
					 title="<%=Prado::localize('TELECHARGER_DAC')%>"
				/>
			</com:THyperLink>
			</com:TPanel>
			<com:TPanel Visible="<%#$this->Page->ArchiveResultSearch->getIsArchiverButtonVisible($this->Data->getIdEtatConsultation())%>">
    			<com:TImageButton
    				ID="Valider"
    				ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-archiver.gif"
    				AlternateText="<%=Prado::localize('TEXT_PASSER_STATUT_A_ARCHIVER')%>"
    				Attributes.title="<%=Prado::localize('TEXT_PASSER_STATUT_A_ARCHIVER')%>"
    				Attributes.onClick="if(!confirm('<%#$this->Page->ArchiveResultSearch->getMsgAlerteActionAArchiver($this->Data)%>')) return false;"
    				OnCommand="SourceTemplateControl.changeUniqueStatut"
    				commandName="Valider"
    			/>
			</com:TPanel>

			<com:TPanel Visible="<%#($this->TemplateControl->isDesarchivagePossible($this->Data))? true:false%>">
    			<com:TImageButton
    				ID="desarchivageConsultation"
    				ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-desarchiver.gif"
    				AlternateText="<%=Prado::localize('TEXT_DESARCHIVER_CONSULTATION')%>"
    				Attributes.title="<%=Prado::localize('TEXT_DESARCHIVER_CONSULTATION')%>"
    				OnCommand="SourceTemplateControl.changeStatutDesarchivage"
    			/>
			</com:TPanel>

			<com:TPanel Visible="<%#Application\Service\Atexo\Atexo_CurrentUser::hasHabilitation('ModifierContrat')  && !$this->Data->hasContrat() && ($this->Data->getIdEtatConsultation() ==  Application\Service\Atexo\Atexo_Config::getParameter('STATUS_ARCHIVE_REALISEE') || $this->Data->getIdEtatConsultation() ==  Application\Service\Atexo\Atexo_Config::getParameter('STATUS_A_ARCHIVER'))%>">
				<com:TActiveImageButton
						ID="genererContrat"
						ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-generer-contrat.gif"
						AlternateText="<%=Prado::localize('GENERER_CONTRAT')%>"
						Attributes.title="<%=Prado::localize('GENERER_CONTRAT')%>"
						OnCallback="SourceTemplateControl.genererContrat"
						ActiveControl.CallbackParameter="<%#$this->Data->getId() . '#' . $this->Data->getOrganisme()%>"
				>
					<prop:ClientSide.OnLoading>showLoader();</prop:ClientSide.OnLoading>
					<prop:ClientSide.OnSuccess>
						setTimeout(function() {
							hideLoader();
							openModal('modal_generation_contrats','modal-form popup-small2',document.getElementById('modal_generation_contrats'));
						}, 200);
					</prop:ClientSide.OnSuccess>
				</com:TActiveImageButton>
			</com:TPanel>
			<com:TPanel
					Visible="<%#(Application\Service\Atexo\Atexo_Module::isEnabled('EchangesDocuments', Application\Service\Atexo\Atexo_CurrentUser::getCurrentOrganism())== '1'  && Application\Service\Atexo\Atexo_CurrentUser::hasHabilitation('AccesEchangeDocumentaire') == '1' )? true:false%>">
				<com:THyperLink Target="_blank" cssClass="btn-action"
								NavigateUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::urlPf()%>agent/echange-documentaire/<%# $this->TemplateControl->getIdCrypter($this->Data->getId())%>">
					<com:TImage ImageUrl="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-acces-echange-documentaire.gif"
								Attributes.alt="<%=Prado::localize('MODULE_ECHANGES_DOCUMENTAIRES')%>"
								Attributes.title="<%=Prado::localize('MODULE_ECHANGES_DOCUMENTAIRES')%>"/>

				</com:THyperLink>
			</com:TPanel>
			<com:TPanel visible="<%#$this->SourceTemplateControl->afficherStatusEchangeChorus($this->Data)%>">
				<com:THyperLink visible="<%#Application\Service\Atexo\Atexo_CurrentUser::hasHabilitation('AttributionMarche')%>" NavigateUrl="index.php?page=Agent.TableauDecisionChorus&id=<%#$this->Data->getId()%>&decision">
					<com:TImage ImageUrl="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-statut-CHORUS.gif" Attributes.alt="<%=Prado::localize('STATUT_CHORUS')%>" Attributes.title="<%=Prado::localize('STATUT_CHORUS')%>"/>
				</com:THyperLink>
				<com:THyperLink visible="<%#!Application\Service\Atexo\Atexo_CurrentUser::hasHabilitation('AttributionMarche')%>">
					<com:TImage ImageUrl="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-statut-CHORUS-inactive.gif" Attributes.alt="<%=Prado::localize('STATUT_CHORUS')%>" Attributes.title="<%=Prado::localize('STATUT_CHORUS')%>"/>
				</com:THyperLink>
			</com:TPanel>
		</td>
	</tr>
</prop:ItemTemplate>
<prop:FooterTemplate>
</table>
</prop:FooterTemplate>
</com:TRepeater>
<!--</com:TActivePanel>-->
<!--Fin Tableau consultations-->
<!--Debut partitionneur-->
<div class="line-partitioner">
	<div class="partitioner">
		<div class="intitule"><strong><label for="decision_nbResultsBottom"><com:TTranslate>DEFINE_TEXT_AFFICHER</com:TTranslate></label></strong></div>
		<com:TDropDownList ID="nombreResultatAfficherBottom" AutoPostBack="true" onSelectedIndexChanged="changePagerLenght" Attributes.title='<%=Prado::localize("NOMBRE_DE_RESULTAT_PAR_PAGE")%>'>
			<com:TListItem Text="10" Selected="10"/>
			<com:TListItem Text="20" Value="20"/>
			<com:TListItem Text="50" Value="50" />
			<com:TListItem Text="100" Value="100" />
			<com:TListItem Text="500" Value="500" />
		</com:TDropDownList>
		<div class="intitule"><com:TTranslate>DEFINE_RESULTATS_PAGE</com:TTranslate></div>
		<com:TPanel ID="panelBouttonGotoPageBottom" DefaultButton="DefaultButtonBottom" CssClass="float-left">
			<com:TTextBox ID="numPageBottom" Attributes.title='<%=Prado::localize("N_DE_LA_PAGE")%>' Text="1"/>
			<div class="nb-total ">/ <com:TLabel ID="nombrePageBottom"/></div>
			<com:TButton ID="DefaultButtonBottom"  OnClick="goToPage" Attributes.style="display:none"/>
		</com:TPanel>
		<div class="liens">
			<com:TPager ID="PagerBottom"
				ControlToPaginate="ArchiveTableauBordRepeater"
				FirstPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-premier.gif' alt='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>' title='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>' />"
				LastPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-dernier.gif' alt='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>' title='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>' />"
				Mode="NextPrev"
				NextPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-suivant.gif' alt='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>' title='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>' />"
				PrevPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-precedent.gif' alt='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>' title='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>' />"
				OnPageIndexChanged="pageChanged"/>
		</div>
	</div>
</div>
<!--Fin partitionneur-->
<a
	id="lienDownloadarchives"
	href="#"
	onClick="showModalLoader();">
</a>
<com:TPanel id="panelArchivage">
	<div class="boutons-line">
		<div class="float-right">
            <a
            	id="boutonTelechargementDossiersSelectionnes"
            	href="javascript:void(0);"
            	onClick="verifierCochesArchivesBoutonTelechargementGroupeAsynchrone();"
            	class="bouton-telecharger-long230 telechargement-selection margin-right-10"
            >
            	<com:TTranslate>DEFINE_TELECHARGER_DOSSIERS_SELECTIONNES</com:TTranslate>
            </a>
            <a
            	href="javascript:void(0);"
            	ID="boutonPasserAArchiver"
            	class="bouton-archivage"
            	onClick="verifierCochesArchivesBoutonPasserStatutAArchiver();"
            >
            	<com:TTranslate>TEXT_PASSER_A_ARCHIVER</com:TTranslate>
            </a>
        </div>
    </div>
</com:TPanel>
</com:TPanel>

<!--Debut Modal Confirmation telechargement-->
<div class="modal-confirmation-telechargement" id="modal_confirmation_telechargement_archives" style="display:none;">
	<!--Debut Formulaire-->
	<div class="form-bloc">
		<div class="top"><span class="left"></span><span class="right"></span></div>
		<div class="content">
			<p class="center"><strong><com:TTranslate>MESSAGE_CONFIRMATION_TELECHARGEMENT_ARCHIVES</com:TTranslate></strong></p>
		</div>
		<div class="bottom"><span class="left"></span><span class="right"></span></div>
	</div>
	<!--Fin Formulaire-->
	<!--Debut line boutons-->
	<div class="boutons-line">
		<input type="button" class="bouton-moyen float-left" value="Annuler" title="Annuler" onclick="J('.modal-confirmation-telechargement').dialog('close');" />
		<com:TActiveLinkButton
			cssClass="bouton-moyen float-right msg-confirmation-envoi"
			Attributes.title="Poursuivre"
			Attributes.onclick="J('.modal-confirmation-telechargement').dialog('close');"
			onCallBack="TemplateControl.preparerTelechargementAsynchrone"
		>
		<com:TTranslate>TEXT_POURSUIVRE</com:TTranslate>
		</com:TActiveLinkButton>
	</div>
	<!--Fin line boutons-->
</div>
<!--Debut Modal Confirmation telechargement-->

<!--Debut Modal Confirmation envoi-->
<div class="modal-confirmation-envoi" id="modal_confirmation_envoi_telechargement" style="display:none;">
	<!--Debut Formulaire-->
	<div class="bloc-message form-bloc msg-confirmation">
		<div class="top"><span class="left"></span><span class="right"></span></div>
		<div class="content">
			<div class="message bloc-500">
				<p><com:TTranslate>MESSAGE_CONFIRMATION_DEMANDE_TELECHARGEMENT_PRISE_EN_COMPTE_1</com:TTranslate><br /><com:TTranslate>MESSAGE_CONFIRMATION_DEMANDE_TELECHARGEMENT_PRISE_EN_COMPTE_2</com:TTranslate></p>
			</div>
		</div>
		<div class="bottom"><span class="left"></span><span class="right"></span></div>
	</div>
	<!--Fin Formulaire-->
	<!--Debut line boutons-->
	<div class="boutons-line">
		<input type="button" class="bouton-moyen float-left" value="Fermer" title="Fermer" onclick="J('.modal-confirmation-envoi').dialog('close');" />
	</div>
	<!--Fin line boutons-->
</div>
<!--Fin Modal Confirmation envoi-->

<!--Debut Modal Message demande telechargement-->
<div class="modal-msg-archives-a-telecharger" id="modal_demande_telechargement_statut_decision_found" style="display:none;">
	<!--Debut Formulaire-->
	<div class="form-bloc">
		<div class="top"><span class="left"></span><span class="right"></span></div>
		<div class="content">
			<p class="center"><strong><com:TTranslate>MESSAGE_ACTION_EFFECTIVE_CONSULTATIONS_STATUTS_A_ARCHIVER</com:TTranslate></strong></p>
		</div>
		<div class="bottom"><span class="left"></span><span class="right"></span></div>
	</div>
	<!--Fin Formulaire-->
	<!--Debut line boutons-->
	<div class="boutons-line">
		<input type="button" class="bouton-moyen float-left" value="Annuler" title="Annuler" onclick="J('.modal-msg-archives-a-telecharger').dialog('close');" />
		<input
			type="button"
			class="bouton-moyen float-right msg-confirmation-envoi"
			value="Poursuivre"
			title="Poursuivre"
			onclick="openModal('modal-confirmation-telechargement','modal-form popup-small2',document.getElementById('modal_confirmation_telechargement_archives'));J('.modal-msg-archives-a-telecharger').dialog('close');"
		/>
	</div>
	<!--Fin line boutons-->
</div>
<!--Fin Modal Message demande telechargement-->

<!--Debut Modal Message statut A Archiver-->
<div class="modal-msg-a-archiver" id="modal_message_statut_a_archiver" style="display:none;">
	<!--Debut Formulaire-->
	<div class="form-bloc">
		<div class="top"><span class="left"></span><span class="right"></span></div>
		<div class="content">
			<p class="center"><strong><com:TTranslate>MESSAGE_ACTION_EFFECTIVE_CONSULTATIONS_STATUTS_DECISION</com:TTranslate></strong></p>
		</div>
		<div class="bottom"><span class="left"></span><span class="right"></span></div>
	</div>
	<!--Fin Formulaire-->
	<!--Debut line boutons-->
	<div class="boutons-line">
		<input type="button" class="bouton-moyen float-left" value="Annuler" title="Annuler" onclick="J('.modal-msg-a-archiver').dialog('close');" />
		<input
			type="button"
			class="bouton-moyen float-right msg-confirmation-envoi"
			value="Poursuivre"
			title="Poursuivre"
			onclick="verifierCochesArchivesAfficherConfirmationPassageAArchiver();J('.modal-msg-a-archiver').dialog('close');"
		/>
	</div>
	<!--Fin line boutons-->
</div>
<!--Fin Modal Message statut A Archiver-->

<!--Debut Modal Message statut A Archiver-->
<div class="modal-msg-confirmation-passage-a-archiver" id="modal_message_passage_a_archiver" style="display:none;">
	<!--Debut Formulaire-->
	<div class="form-bloc">
		<div class="top"><span class="left"></span><span class="right"></span></div>
		<div class="content">
			<p class="center"><strong><com:TTranslate>MESSAGE_CONFIRMATION_PASSAGE_STATUT_A_ARCHIVER_1</com:TTranslate> <br/><com:TTranslate>MESSAGE_CONFIRMATION_PASSAGE_STATUT_A_ARCHIVER_2</com:TTranslate></strong></p>
		</div>
		<div class="bottom"><span class="left"></span><span class="right"></span></div>
	</div>
	<!--Fin Formulaire-->
	<!--Debut line boutons-->
	<div class="boutons-line">
		<input type="button" class="bouton-moyen float-left" value="Annuler" title="Annuler" onclick="J('.modal-msg-confirmation-passage-a-archiver').dialog('close');" />
		<com:TLinkButton
        	ID="Valider"
        	CssClass="bouton-moyen float-right msg-confirmation-envoi"
        	attributes.onclick="J('.modal-msg-confirmation-passage-a-archiver').dialog('close');"
        	OnCommand="TemplateControl.changeStatut"
        	commandName="Valider">
        	<com:TTranslate>TEXT_POURSUIVRE</com:TTranslate>
        </com:TLinkButton>
	</div>
	<!--Fin line boutons-->
</div>
<!--Fin Modal Message statut A Archiver-->

<!--Debut Modal Message statut A Archiver cas plusieurs consultation-->
<div class="modal-msg-confirmation-passage-a-archiver-plusieurs-cons" id="modal_message_passage_a_archiver_plusieurs_cons" style="display:none;">
	<!--Debut Formulaire-->
	<div class="form-bloc">
		<div class="top"><span class="left"></span><span class="right"></span></div>
		<div class="content">
			<p class="center">
				<strong><com:TTranslate>MESSAGE_CONFIRMATION_PASSAGE_STATUT_A_ARCHIVER_PLUSIEURS_CONS_1</com:TTranslate>
				<br/>
				<com:TTranslate>MESSAGE_CONFIRMATION_PASSAGE_STATUT_A_ARCHIVER_PLUSIEURS_CONS_2</com:TTranslate>
				</strong>
			</p>
		</div>
		<div class="bottom"><span class="left"></span><span class="right"></span></div>
	</div>
	<!--Fin Formulaire-->
	<!--Debut line boutons-->
	<div class="boutons-line">
		<input type="button" class="bouton-moyen float-left" value="Annuler" title="Annuler" onclick="J('.modal-msg-confirmation-passage-a-archiver-plusieurs-cons').dialog('close');" />
		<com:TLinkButton
        	ID="ValiderPlusieursCons"
        	CssClass="bouton-moyen float-right msg-confirmation-envoi"
        	attributes.onclick="J('.modal-msg-confirmation-passage-a-archiver-plusieurs-cons').dialog('close');"
        	OnCommand="TemplateControl.changeStatut"
        	commandName="Valider">
        	<com:TTranslate>TEXT_POURSUIVRE</com:TTranslate>
        </com:TLinkButton>
	</div>
	<!--Fin line boutons-->
</div>
<!--Fin Modal Message statut A Archiver cas plusieurs consultation-->

<com:TActiveLabel ID="scriptJsStatutsArchives" style="display:none;"/>
<com:TActivePanel id="panelGenerationContrat" >
<div class="modal_generation_contrats" id="modal_generation_contrats" style="display:none;">
	<div class="form-bloc">
		<div class="top"><span class="left"></span><span class="right"></span></div>
		<div class="content">
			<p><strong><com:TActiveLabel id="messageGenertionContrat"/></strong></p>
		</div>
		<div class="bottom"><span class="left"></span><span class="right"></span></div>
	</div>
	<div class="boutons-line">
		<com:TActiveLinkButton
				id="btnAnnulerGenerationContrat"
				cssClass="bouton-moyen float-left"
				Attributes.title="<%=Prado::localize('TEXT_ANNULER')%>"
				Attributes.onclick="J(this).closest('.ui-dialog-content').dialog('destroy');"
				text="<%=Prado::localize('TEXT_ANNULER')%>"
		>
			<prop:ClientSide.OnLoading>showLoader();</prop:ClientSide.OnLoading>
			<prop:ClientSide.OnSuccess>hideLoader();</prop:ClientSide.OnSuccess>
			<prop:ClientSide.OnFailure>hideLoader();</prop:ClientSide.OnFailure>
		</com:TActiveLinkButton>
		<com:TActiveLinkButton
				id="btnFermerGenerationContrat"
				CssClass="bouton-moyen float-right msg-confirmation-envoi"
				Attributes.title="<%=Prado::localize('FERMER')%>"
				Attributes.onclick="J(this).closest('.ui-dialog-content').dialog('destroy');"
				text="<%=Prado::localize('FERMER')%>"
		>
			<prop:ClientSide.OnLoading>showLoader();</prop:ClientSide.OnLoading>
			<prop:ClientSide.OnSuccess>hideLoader();</prop:ClientSide.OnSuccess>
			<prop:ClientSide.OnFailure>hideLoader();</prop:ClientSide.OnFailure>
		</com:TActiveLinkButton>
		<com:TActiveLinkButton
				id="btnValiderGenerationContrat"
				CssClass="bouton-moyen float-right msg-confirmation-envoi"
				Attributes.title="<%=Prado::localize('TEXT_VALIDER')%>"
				onCallBack="doDesarchivage"
				text="<%=Prado::localize('TEXT_VALIDER')%>"
		>
			<prop:ClientSide.OnLoading>J(this).closest('.ui-dialog-content').dialog('destroy'); showLoader();</prop:ClientSide.OnLoading>
		</com:TActiveLinkButton>
	</div>
</div>
	<com:TActiveHiddenField id="infoCons" />
</com:TActivePanel>
<com:TActiveLabel id="scriptJs" />
