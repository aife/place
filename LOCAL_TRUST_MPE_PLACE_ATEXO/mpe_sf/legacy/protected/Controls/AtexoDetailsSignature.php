<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Signature\Atexo_Signature_FichierVo;
use Application\Service\Atexo\Signature\Atexo_Signature_InfosVo;
use Prado\Prado;

/**
 * commentaires.
 *
 * @author khadija Chouika <khadija.chouika@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 2.1
 *
 * @since MPE-4.0
 */
class AtexoDetailsSignature extends MpeTTemplateControl
{
    private $_fichier;
    private $_type;
    private $_titre;
    private $_pathFileJeton;

    /**
     * Recupère la valeur.
     *
     * @return mixed
     */
    public function getFichier()
    {
        return $this->_fichier;
    }

    /**
     * Affectes la valeur.
     *
     * @param $value
     */
    public function setFichier($value)
    {
        $this->_fichier = $value;
    }

    /**
     * Recupère la valeur.
     *
     * @return mixed
     */
    public function getType()
    {
        return $this->_type;
    }

    /**
     * Affectes la valeur.
     *
     * @param $value
     */
    public function setType($value)
    {
        $this->_type = $value;
    }

    /**
     * Recupère la valeur.
     *
     * @return mixed
     */
    public function getTitre()
    {
        return $this->_titre;
    }

    /**
     * Affectes la valeur.
     *
     * @param $value
     */
    public function setTitre($value)
    {
        $this->_titre = $value;
    }

    /**
     * Permet de retourner les informations de la signature.
     *
     * @return mixed
     */
    public function getSignatureVo()
    {
        if ($this->_fichier instanceof Atexo_Signature_FichierVo) {
            return $this->_fichier->getInfosSignature();
        }
    }

    /**
     * Formate une chaine de caractére séparé par ','.
     *
     * @param $chaine
     */
    public function formaterChaine($chaine): bool|string
    {
        $arraySignataire = explode(',', $chaine);

        if (is_array($arraySignataire) && count($arraySignataire)) {
            $chaine = (new Atexo_Config())->toPfEncoding(implode('<br/>', $arraySignataire));
        }

        return $chaine;
    }

    /**
     * Permet de retourner les informations de signataire.
     */
    public function getSignataire(): bool|string
    {
        $signataire = '';

        if ($this->getSignatureVo() instanceof Atexo_Signature_InfosVo) {
            $signataire = $this->getSignatureVo()->getSignataire();

            return self::formaterChaine($signataire);
        }

        return $signataire;
    }

    /**
     * Recupère la valeur de l'emetteur de la signature.
     */
    public function getEmetteur(): bool|string
    {
        $emetteur = '';

        if ($this->getSignatureVo() instanceof Atexo_Signature_InfosVo) {
            $emetteur = $this->getSignatureVo()->getEmetteur();

            return self::formaterChaine($emetteur);
        }

        return $emetteur;
    }

    /**
     * Recupère la valeur de la dateValiditeDu.
     *
     * @return mixed
     */
    public function getDateValideFrom()
    {
        $date = '';

        if ($this->getSignatureVo() instanceof Atexo_Signature_InfosVo) {
            $date = Atexo_Util::iso2frnDate(substr($this->getSignatureVo()->getDateValiditeDu(), 0, 10));
            $date .= ' '.substr($this->getSignatureVo()->getDateValiditeDu(), 11, 8);
        }

        return $date;
    }

    /**
     * Recupère la valeur de la dateValiditeAu.
     *
     * @return mixed
     */
    public function getDateValideTo()
    {
        $date = '';

        if ($this->getSignatureVo() instanceof Atexo_Signature_InfosVo) {
            $date = Atexo_Util::iso2frnDate(substr($this->getSignatureVo()->getDateValiditeAu(), 0, 10));
            $date .= ' '.substr($this->getSignatureVo()->getDateValiditeAu(), 11, 8);
        }

        return $date;
    }

    /**
     * Recupère la statut de la période de validité.
     *
     * @return string
     */
    public function getStatutPeriodicite()
    {
        $statut = Atexo_Config::getParameter('UNKNOWN');

        if ($this->getFichier() instanceof Atexo_Signature_FichierVo) {
            $statut = $this->getFichier()->getStatutPeriodeValiditeCert();
        }

        return $statut;
    }

    /**
     * Recupère la statut de la chaine de certification.
     *
     * @return string
     */
    public function getStatutChaine()
    {
        $statut = Atexo_Config::getParameter('UNKNOWN');

        if ($this->getFichier() instanceof Atexo_Signature_FichierVo) {
            $statut = $this->getFichier()->getStatutVerifChaineCert();
        }

        return $statut;
    }

    /**
     * Recupère la statut de la non révocation.
     *
     * @return string
     */
    public function getStatutCrl()
    {
        $statut = Atexo_Config::getParameter('UNKNOWN');

        if ($this->getFichier() instanceof Atexo_Signature_FichierVo) {
            $statut = $this->getFichier()->getStatutRevocationCert();
        }

        return $statut;
    }

    /**
     * Recupère le statut du signataire.
     *
     * @return string
     */
    public function getStatutSignature()
    {
        $statut = Atexo_Config::getParameter('UNKNOWN');

        if ($this->getFichier() instanceof Atexo_Signature_FichierVo) {
            $statut = $this->getFichier()->getStatutRejeuSignature();
        }

        return $statut;
    }

    /**
     * Retourne le nom du référentiel de certificat.
     *
     * @return mixed|string
     */
    public function getReferentielCertificat()
    {
        $referentielName = Prado::localize('DEFINE_NON_REFERENCE');

        if ($this->getFichier() instanceof Atexo_Signature_FichierVo) {
            $referentielName = $this->getFichier()->getNomReferentielCertificat(true);
        }

        return $referentielName;
    }

    /**
     * Permet de retourner la classe css selon le statut.
     *
     * @param $statut
     *
     * @return string
     */
    public function getClassCss($statut)
    {
        return match (strtoupper($statut)) {
            'OK' => 'statut-resultat statut-valide',
            'NOK' => 'statut-resultat statut-erreur',
            'UNKNOWN' => 'statut-resultat statut-avertissement',
            'SANS' => 'statut-resultat statut-vide',
            default => 'statut-resultat statut-vide',
        };
    }

    /**
     * Retourne le statut de validité du réferentiel.
     *
     * @return string
     */
    public function getStatutValiditeReferentielCertificat()
    {
        $statut = Atexo_Config::getParameter('UNKNOWN');

        if ($this->getFichier() instanceof Atexo_Signature_FichierVo) {
            $statut = $this->getFichier()->getStatutReferentielCertificat();
        }

        return $statut;
    }

    /**
     * Permet de retourner le Message d'erreur,d'avertissement ou de confirmation selon les statuts de la validité.
     *
     * @return mixed|string|null
     */
    public function getMessage()
    {
        $message = null;

        if ($this->getFichier()) {
            $resultatStatut = $this->getFichier()->getResultatValiditeSignatureWithMessage();

            if ('OK' == $resultatStatut['statut']) {
                $message = Prado::localize('DEFINE_TOUS_CONTROLS_SIGNATURE_VALIDE');
            } else {
                $message = $resultatStatut['message'];
            }
        }

        return $message;
    }

    /**
     * Retourne le type de message à afficher selon les statuts de la signature.
     *
     * @return string|null
     */
    public function getTypeMessage()
    {
        $typeMessage = null;

        if ($this->getFichier()) {
            $resultatStatut = $this->getFichier()->getResultatValiditeSignatureWithMessage();

            if ('OK' == $resultatStatut['statut']) {
                $typeMessage = 'alert-success';
            } elseif ('NOK' == $resultatStatut['statut']) {
                $typeMessage = 'alert-danger';
            } elseif ('UNKNOWN' == $resultatStatut['statut']) {
                $typeMessage = 'alert-warning';
            } else {
                $typeMessage = 'alert-warning';
            }
        }

        return $typeMessage;
    }

    /**
     * Retourne le chemin du jeton.
     *
     * @return string
     */
    public function setPathFileJeton($value)
    {
        $this->_pathFileJeton = $value;
    }

    /**
     * Retourne le chemin du jeton.
     *
     * @return string
     */
    public function getPathFileJeton()
    {
        return (new Atexo_Config())->toPfEncoding($this->_pathFileJeton);
    }

    /**
     * @param $data
     */
    public function fillRepeater($data)
    {
        $this->repeaterListeSignatures->DataSource = $data;
        $this->repeaterListeSignatures->DataBind();
    }
}
