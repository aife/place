<!--Debut Bloc Alertes-->
<com:TPanel ID="summaryVisible" CssClass="form-bloc recap-infos-consultation">
	<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
	<div class="content">
		<div onclick="togglePanel(this,'listeAlertes');" class="title-toggle">&nbsp;</div>
		<div class="recap-bloc">
			<h2><com:TTranslate>TEXT_ALERTES</com:TTranslate></h2>
			<div class="breaker"></div>
			<div id="listeAlertes" style="display: none;" class="panel-toggle">
			<com:TPanel id="alerteNotExiste">
				<div>
					<label for="aucuneAlerte"><com:TTranslate>TEXT_AUCUNE_ALERTE_A_AFFICHER</com:TTranslate></label>
				</div>
			</com:TPanel>
				<div class="table-bloc">
						<com:TRepeater ID="repeaterAlertes" >
							<prop:HeaderTemplate>
								<table summary="Tableau des alertes" class="table-results tableau-alertes">
									<caption><com:TTranslate>TEXT_ALERTES</com:TTranslate></caption>
									<thead>
										<tr>
											<th colspan="4" class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></th>
										</tr>
										<tr>
											<th id="detailAlerte" class="col-360"><com:TTranslate>TEXT_ALERTES</com:TTranslate></th>
											<th id="alertesCloturee" class="col-100 center"><com:TTranslate>TEXT_CLOTUREE</com:TTranslate></th>
											<th id="dateCreation" class="col-100"><com:TTranslate>TEXT_DATE_DE_CREATION</com:TTranslate></th>
											<th id="dateCloture" class="col-100"><com:TTranslate>DATE_COTURE</com:TTranslate></th>
										</tr>
									</thead>
							</prop:HeaderTemplate>
							<prop:ItemTemplate>
									<tr class="<%#(($this->ItemIndex%2==0)? '':'on')%>">
										<td headers="detailAlerte" class="col-360"><%#Prado::localize($this->Data->getMessage())%></td>
										<td headers="alertesCloturee" class="col-100 center"><%#$this->Data->getCloturee()=='1'?(Prado::localize('DEFINE_OUI')):(Prado::localize('DEFINE_NON'))%></td>
										<td headers="dateCreation" class="col-100"><%# Application\Service\Atexo\Atexo_Util::iso2frnDateTime($this->Data->getDateCreation())%></td>
										<td headers="dateCloture" class="col-100"><%#$this->Data->getDateCloture()? Application\Service\Atexo\Atexo_Util::iso2frnDateTime($this->Data->getDateCloture()):'-'%></td>
									</tr>
									
							</prop:ItemTemplate>
							<prop:FooterTemplate>
								</table>
							</prop:FooterTemplate>
						</com:TRepeater>
				</div>
				<div class="breaker"></div>
			</div>
		</div>
		<div class="breaker"></div>
	</div>
	<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
</com:TPanel>

<!--Fin Bloc Alertes-->