<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_Certificat;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Exception;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Crypto\Atexo_Crypto_Certificat;
use Prado\Prado;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class BiCle extends MpeTTemplateControl
{
    private $_idService;
    private $_idServiceAssocie;
    private $_idServiceValidation;
    private $_organisme;

    public function getIdService()
    {
        return $this->_idService;
    }

    public function setIdService($idService)
    {
        $this->_idService = $idService;
    }

    public function getIdServiceAssocie()
    {
        return $this->_idServiceAssocie;
    }

    public function setIdServiceAssocie($idServiceAssocie)
    {
        $this->_idServiceAssocie = $idServiceAssocie;
    }

    public function getIdServiceValidation()
    {
        return $this->_idServiceValidation;
    }

    public function setIdServiceValidation($idServiceValidation)
    {
        $this->_idServiceValidation = $idServiceValidation;
    }

    public function getOrganisme()
    {
        return $this->_organisme;
    }

    public function setOrganisme($organisme)
    {
        $this->_organisme = $organisme;
    }

    public function fillRepeater()
    {
        $this->panelBiCleSecours->setVisible(true);
        $idService = $this->_idService;
        $idServiceAssocie = $this->_idServiceAssocie;
        $idServiceValidation = $this->_idServiceValidation;

        if ($this->_organisme) {
            $idOrganisme = $this->_organisme;
        } else {
            $idOrganisme = Atexo_CurrentUser::getCurrentOrganism();
        }

        $biCleSecour = (new Atexo_Certificat())->retrieveReliefPermanentCertificatByService($idService, $idOrganisme);
        if ($idServiceAssocie && $idServiceAssocie != $idService) {
            $biCleSecourServiceAssocie = (new Atexo_Certificat())->retrieveReliefPermanentCertificatByService($idServiceAssocie, $idOrganisme);
            $biCleSecour = array_merge($biCleSecour, $biCleSecourServiceAssocie);
        }
        if ($idServiceValidation && $idServiceValidation != $idService && $idServiceValidation != $idServiceAssocie) {
            $biCleSecourServiceValidation = (new Atexo_Certificat())->retrieveReliefPermanentCertificatByService($idServiceValidation, $idOrganisme);
            $biCleSecour = array_merge($biCleSecour, $biCleSecourServiceValidation);
        }

        if (is_array($biCleSecour)) {
            $biCleSecour = (new Atexo_Certificat())->removeDoublons($biCleSecour);
            if (count($biCleSecour) > 0) {
                $this->texteSecours->Text = Prado::localize('CLE_SECOURS');
            } else {
                $this->texteSecours->Text = Prado::localize('AUCUNE_CLE_SECOURS');
            }
            $this->biCleSecours->DataSource = $biCleSecour;
            $this->biCleSecours->DataBind();
        } else {
            throw new Atexo_Exception('Expeced function to return an array');
        }
    }

    public function getDateExpiration($certificat)
    {
        $dateExpiration = (new Atexo_Crypto_Certificat())->getDateExpiration($certificat);
        if ($dateExpiration) {
            return Atexo_Util::iso2frnDateTime($dateExpiration).' GMT';
        } else {
            return '-';
        }
    }

    public function fillRepeaterCleUniverselle($acronymeOrg = null)
    {
        $dataSource = (new Atexo_Certificat())->retrievePermanentCertificatUniverselle($acronymeOrg);
        if (is_array($dataSource) && count($dataSource)) {
            $this->texteSecours->Text = Prado::localize('CLE_UNIVERSELLES');
            $this->biCleSecours->DataSource = $dataSource;
            $this->biCleSecours->DataBind();
            $this->panelBiCleSecours->setVisible(true);
        } else {
            $this->panelBiCleSecours->setVisible(false);
        }
    }
}
