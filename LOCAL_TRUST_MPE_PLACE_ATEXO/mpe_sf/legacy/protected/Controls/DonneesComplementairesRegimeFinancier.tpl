<div class="toggle-panel form-toggle regime-financier">
    <div class="content">
        <div class="cover"></div>
        <a class="title-toggle-open" onclick="togglePanel(this,'panel_regimeFinancier');" href="javascript:void(0);"><com:TTranslate>DEFINE_REGIME_FINANCIER</com:TTranslate></a>
        <div class="panel no-indent donnees-regime-financier" id="panel_regimeFinancier" style="display:block;">
            <!--Debut Ligne Cautionnement et garanties exigées-->
            <div class="line">
                <div class="intitule-120">
                    <com:TActiveRadioButton GroupName="radioRegimeFinancier" id="radioNonRegimeFinancier" cssClass="radio" Attributes.onclick="hideDiv('<%=$this->blocRegimeFinancier->ClientId %>')"/>
                    <label for="intitulePubliciteOui"><com:TTranslate>TEXT_SANS_OBJET</com:TTranslate></label>
                </div>
                <div class="intitule-auto">
                    <com:TActiveRadioButton GroupName="radioRegimeFinancier" id="radioOuiRegimeFinancier" cssClass="radio" Attributes.onclick="showDiv('<%=$this->blocRegimeFinancier->ClientId %>')" />
                    <label for="intitulePubliciteNon"><com:TTranslate>MES_INFOS_PRE_ENREGISTEES</com:TTranslate></label>
                </div>
            </div>
            <div class="spacer-small"></div>
            <com:TActivePanel id="blocRegimeFinancier">
                <div class="line">
                    <div class="intitule-bloc"><label for="cautionnementGaranties"><com:TTranslate>DEFINE_CAUTIONNEMENT_ET_GARANTIES_EXIGEES_CAS_ECHEANT</com:TTranslate></label> :</div>
                    <div class="breaker"></div>
                    <com:TActiveTextBox
                        id="cautionnementGaranties"
                        Attributes.title="<%=Prado::localize('DEFINE_CAUTIONNEMENT_ET_GARANTIES_EXIGEES_CAS_ECHEANT')%>"
                        TextMode="MultiLine"
                        Cssclass="long"
                        Attributes.maxlength = "2000"
                    />
                </div>
                <!--Fin Ligne Cautionnement et garanties exigées-->
                <div class="spacer-small"></div>
                <!--Debut Ligne Modalités financement-->
                <div class="line">
                    <div class="intitule-bloc"><label for="modalitesFinancement"><com:TTranslate>DEFINE_MODALITES_ESSENTIELLES_DE_FINANCEMENT_ET_DE_PAIEMENT_REFERENCE_AUX_TEXTES_QUI_LES_REGLEMENTENT</com:TTranslate> </label> :</div>
                    <com:TActiveTextBox
                        id="modalitesFinancement"
                        Attributes.title="<%=Prado::localize('DEFINE_MODALITES_ESSENTIELLES_DE_FINANCEMENT_ET_DE_PAIEMENT_REFERENCE_AUX_TEXTES_QUI_LES_REGLEMENTENT')%>"
                        TextMode="MultiLine"
                        Cssclass="long"
                        Attributes.maxlength = "2000"
                    />
                </div>
            </com:TActivePanel>
            <!--Fin Ligne Modalités financement-->
        </div>
        <div class="breaker"></div>
    </div>
</div>
