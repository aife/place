<?php
/*
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 */

namespace Application\Controls;

use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Controller\Atexo_Controller_Front;
use Application\Service\Atexo\CurrentUserTrait;
use Application\Service\Atexo\Security\Atexo_Security_Authentication;
use Application\Service\Atexo\UtilTrait;
use Prado\Web\UI\TPage;

class MpeTPage extends TPage
{
    use UtilTrait;
    use CurrentUserTrait;

    public string $pageTitle = 'TITRE';

    public function onPreInit($param)
    {
        Atexo_Security_Authentication::loginBySF();
        parent::onPreInit($param);
    }

    public function getParameter($parameter)
    {
        return Atexo_Config::getParameter($parameter);
    }

    public function themesPath()
    {
        return Atexo_Controller_Front::t();
    }

    public function urlPf()
    {
        return Atexo_Controller_Front::urlPf();
    }

    public function isEnabledModule($module)
    {
        return Atexo_Module::isEnabled($module);
    }
}
