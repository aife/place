<script type="text/javascript">
  J( document ).ready(function() {
    J('.popover-body p').each(function() {
      var currAbbrContent = J(this).find('abbr').attr('title');

      J(this).find('.popinInjectDiv').append(currAbbrContent);
    });
  });
</script>

<!--Debut partitionneur-->
<com:TPanel ID="panelNoElementFound">
    <h2>
        <com:TTranslate>DEFINE_AUCUN_RESULTAT</com:TTranslate>&nbsp;
    </h2>
</com:TPanel>
<com:TPanel ID="panelMoreThanOneElementFound">
    <div class="line-partitioner">
        <h2>
            <com:TTranslate>DEFINE_NOMBRE_RESULTAT</com:TTranslate>&nbsp;<com:TLabel ID="nombreElement"/>
        </h2>

        <div class="partitioner">
            <div class="intitule"><strong><label for="allCons_nbResultsTop">
                        <com:TTranslate>DEFINE_TEXT_AFFICHER</com:TTranslate>
                    </label></strong></div>
            <com:TDropDownList ID="nombreResultatAfficherTop" AutoPostBack="true"
                               onSelectedIndexChanged="changePagerLenght"
                               Attributes.title='<%=Prado::localize("NOMBRE_DE_RESULTAT_PAR_PAGE")%>'>
                <com:TListItem Text="10" Selected="10"/>
                <com:TListItem Text="20" Value="20"/>
            </com:TDropDownList>
            <div class="intitule">
                <com:TTranslate>DEFINE_RESULTATS_PAGE</com:TTranslate>
            </div>
            <label style="display:none;" for="allCons_pageNumberTop">
                <com:TTranslate>ALLER_A_LA_PAGE</com:TTranslate>
            </label>
            <com:TPanel ID="panelBouttonGotoPageTop" DefaultButton="DefaultButtonTop" CssClass="float-left">
                <com:TTextBox ID="numPageTop" Attributes.title='<%=Prado::localize("N_DE_LA_PAGE")%>' Text="1"/>
                <div class="nb-total ">/
                    <com:TLabel ID="nombrePageTop"/>
                </div>
                <com:TButton ID="DefaultButtonTop" OnClick="goToPage" Attributes.style="display:none"/>
            </com:TPanel>
            <div class="liens">
                <com:TPager ID="PagerTop"
                            ControlToPaginate="tableauDeBordRepeater"
                            FirstPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-premier.gif' alt='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>' title='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>' />"
                            LastPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-dernier.gif' alt='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>' title='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>' />"
                            Mode="NextPrev"
                            NextPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-suivant.gif' alt='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>' title='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>' />"
                            PrevPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-precedent.gif' alt='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>' title='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>' />"
                            OnPageIndexChanged="pageChanged"/>
            </div>
        </div>
    </div>
    <!--Fin partitionneur-->
    <!--Debut Tableau consultations-->
    <com:TRepeater ID="tableauDeBordRepeater"
                   AllowPaging="true"
                   PageSize="10"
                   EnableViewState="true"
                   AllowCustomPaging="true"
                   OnItemCommand="commandConsultation"
    >
        <prop:HeaderTemplate>
            <table summary="Toutes les consultations" class="table-results">
                <caption>
                    <com:TTranslate>TEXT_TOUTES_CONSULTATION</com:TTranslate>
                </caption>
                <thead>
                <tr>
                    <th class="top" colspan="<%=$this->Page->ResultSearch->getNbrColHead(false) + 1 %>"><span class="left">&nbsp;</span><span
                                class="right">&nbsp;</span></th>
                </tr>
                <tr>
                    <com:TPanel Visible="<%# !Application\Service\Atexo\Atexo_Config::getParameter('SPECIFIQUE_PLACE') %>">
                    <th class="col-10"></th>
                    </com:TPanel>
                    <th class="<%#$this->SourceTemplateControl->visionRma ? 'col-150' : 'col-90'%>" id="allCons_ref"
                        abbr="Ref">
                        <%#$this->Page->ResultSearch->elementSortedWith('reference_utilisateur','open')%>
                        <com:TTranslate>TEXT_REFERENCE</com:TTranslate>
                        <%#$this->Page->ResultSearch->elementSortedWith('reference_utilisateur','close')%>
                        <com:PictoSort CommandName="reference_utilisateur" OnCommand="Page.Trier"/>
                        <br/>
                        <com:TTranslate>DEFINE_PROCEDURE</com:TTranslate>
                        <br/>
                        <com:TTranslate>DEFINE_STATUS</com:TTranslate>
                        <br/>
                        <%#$this->Page->ResultSearch->elementSortedWith('nom_createur','open')%>
                        <com:TTranslate>DEFINE_AUTEUR</com:TTranslate>
                        <%#$this->Page->ResultSearch->elementSortedWith('nom_createur','close')%>
                        <com:PictoSort CommandName="nom_createur" OnCommand="Page.Trier"/>
                        <com:TPanel visible="<%=$this->SourceTemplateControl->visionRma%>">
                            <!-- Debut date limite en cas de vision RMA-->
                            <%#$this->Page->ResultSearch->elementSortedWith('dateFin','open')%>
                            <com:TLabel Visible="<%#$this->TemplateControl->isTypeConsultation()%>">
                                <com:TTranslate>DEFINE_DATE_LIMITE_AR</com:TTranslate>
                            </com:TLabel>
                            <%#$this->Page->ResultSearch->elementSortedWith('dateFin','close')%>
                            <com:PictoSort CommandName="datefin" OnCommand="Page.Trier"/>
                            <!-- Fin date limite en cas de vision RMA-->
                        </com:TPanel>
                    </th>
                    <th style="display:<%#$this->SourceTemplateControl->visionRma?'none':'block'%>" class="col-30"
                        id="allCons_details" abbr="Détails">&nbsp;
                    </th>
                    <th class="col-350" id="allCons_intitule" abbr="Intitule">
                        <%#$this->Page->ResultSearch->elementSortedWith('intitule','open')%>
                        <com:TTranslate>DEFINE_INTITULE</com:TTranslate>
                        <%#$this->Page->ResultSearch->elementSortedWith('intitule','close')%>
                        <com:PictoSort CommandName="intitule" OnCommand="Page.Trier"/>
                        /
                        <%#$this->Page->ResultSearch->elementSortedWith('objet','open')%>
                        <com:TTranslate>OBJET</com:TTranslate>
                        <%#$this->Page->ResultSearch->elementSortedWith('objet','close')%>
                        <com:PictoSort CommandName="objet" OnCommand="Page.Trier"/>
                        <!-- Debut Organisme et Entité achat : Vision RMA-->
                        <com:TPanel Visible="<%#$this->SourceTemplateControl->visionRma%>">
                            <!-- Organisme -->
                            <%#$this->Page->ResultSearch->elementSortedWith('organisme','open')%>
                            <com:TTranslate>ORGANISME</com:TTranslate>
                            <%#$this->Page->ResultSearch->elementSortedWith('organisme','close')%>
                            <com:PictoSort CommandName="organisme" OnCommand="Page.Trier"/>
                            <!-- Entité achat -->
                            <%#$this->Page->ResultSearch->elementSortedWith('service_id','open')%>
                            <com:TTranslate>DEFINE_TEXT_ENTITE_ACHAT</com:TTranslate>
                            <%#$this->Page->ResultSearch->elementSortedWith('service_id','close')%>
                            <com:PictoSort CommandName="service_id" OnCommand="Page.Trier"/>
                        </com:TPanel>
                        <!-- Fin Organisme et Entité achat : Vision RMA-->
                        <com:TPanel Visible="<%#!$this->SourceTemplateControl->visionRma%>">
                            <span <%#($this->TemplateControl->isTypeConsultation())? '':'style="display:none"'%> >
                            /
                            <%#$this->Page->ResultSearch->elementSortedWith('alloti','open')%>
                            <com:TTranslate>TEXT_ALLOTISSEMENT</com:TTranslate>
                            <%#$this->Page->ResultSearch->elementSortedWith('alloti','close')%>
                            <com:PictoSort CommandName="alloti" OnCommand="Page.Trier"/>
                            </span>
                            <span <%#($this->TemplateControl->isTypeConsultation() &&
                            Application\Service\Atexo\Atexo_Module::isEnabled('ConsultationClause'))? '':'style="display:none"'%>>
                            <br/>
                            <com:TTranslate>CLAUSES_SOCIALES</com:TTranslate>
                            /
                            <com:TTranslate>DEFINE_ENVIRONNEMENTALES</com:TTranslate>
                            </span>
                            <span <%#($this->TemplateControl->isTypeConsultation() && Application\Service\Atexo\Atexo_Module::
                            isEnabled('MarchePublicSimplifie'))? '':'style="display:none"'%>>
                            <br/>
                            <%=(Application\Service\Atexo\Atexo_Module::isEnabled('MasquerElementsMps'))? Prado::localize("DEFINE_DUME") : Prado::localize("DEFINE_MPS") %>
                            </span>
                        </com:TPanel>
                    </th>
                    <th class="<%#($this->TemplateControl->isTypeConsultation()? 'col-110':'col-100')%>"
                        id="allCons_registres">
                        <com:TLabel Visible="<%#$this->TemplateControl->isTypeConsultation()%>">
                            <com:TTranslate>DEFINE_REGISTRES</com:TTranslate>
                            :
                            <com:PictoRegistre/>
                        </com:TLabel>

                        <com:TLabel Visible="<%#$this->TemplateControl->isTypeAnnonce()%>">
                            <com:TTranslate>TYPE_ANNONCE</com:TTranslate>
                            <com:PictoSort CommandName="id_type_avis" OnCommand="Page.Trier"/>
                        </com:TLabel>

                    </th>
                    <com:TPanel Visible="<%=!$this->SourceTemplateControl->visionRma%>">
                        <th class="col-100" id="allCons_dateEnd" abbr="Date limite">
                            <%#$this->Page->ResultSearch->elementSortedWith('dateFin','open')%>
                            <com:TLabel Visible="<%#$this->TemplateControl->isTypeAnnonce()%>">
                                <com:TTranslate>DATE_FIN_AFFICHAGE</com:TTranslate>
                            </com:TLabel>
                            <com:TLabel Visible="<%#$this->TemplateControl->isTypeConsultation()%>">
                                <com:TTranslate>TEXT_DATE_LIMITE_BR_REMISE_PLIS</com:TTranslate>
                            </com:TLabel>
                            <%#$this->Page->ResultSearch->elementSortedWith('dateFin','close')%>
                            <com:PictoSort CommandName="datefin" OnCommand="Page.Trier"/>
                        </th>
                    </com:TPanel>
                    <com:TPanel ID="colValidationElaborationHead"
                                visible="<%=$this->Page->ResultSearch->getNbrColHead(true) && !$this->SourceTemplateControl->visionRma%>">
                        <th id="allCons_actionsOpt" class="actions"></th>
                    </com:TPanel>
                    <com:TPanel Visible="<%=!$this->SourceTemplateControl->visionRma%>">
                        <th class="actions" id="allCons_actions">
                            <com:TTranslate>DEFINE_ACTIONS</com:TTranslate>
                        </th>
                    </com:TPanel>
                    <!-- debut pieces jointes RMA header -->
                    <com:TPanel Visible="<%=$this->SourceTemplateControl->visionRma%>">
                        <th class="col-120" scope="col">Pièces jointes</th>
                    </com:TPanel>
                    <!-- debut pieces jointes RMA header -->
                </tr>
                </thead>
        </prop:HeaderTemplate>
        <prop:ItemTemplate>

            <tr class="<%#(($this->ItemIndex%2==0)? '':'on')%>">
                <td>
                    <a class="<%#$this->Page->ResultSearch->isConsultationFavoris($this->Data) ? 'deleteFavoris' : 'addFavoris'%>"
                       style="cursor: pointer;"
                       data="<%#base64_encode($this->Data->getId().','.Application\Service\Atexo\Atexo_CurrentUser::getId())%>">
                        <img src="<%#$this->Page->ResultSearch->isConsultationFavoris($this->Data) ? Application\Service\Atexo\Controller\Atexo_Controller_Front::t().'/images/full-star.png' : Application\Service\Atexo\Controller\Atexo_Controller_Front::t().'/images/empty-star.png'%>" alt="" style="border-width:0;">
                    </a>
                </td>

                <td class="<%#$this->SourceTemplateControl->visionRma ? 'col-150' : 'col-90'%>" headers="allCons_ref">
                    <com:TLabel ID="reference" Attributes.class="ref"
                                Text="<%#$this->Page->ResultSearch->insertBrWhenTextTooLong($this->Data->getReferenceUtilisateur(), $this->SourceTemplateControl->visionRma ? '20' : '15' )%>"/>
                    <br/>
                    <div>
                        <com:TLabel ID="procedure" Text="<%#$this->Data->getAbreviationTypeProcedure()%>"/>
                        <span style="" onmouseover="afficheBulle('<%#($this->getClientId())%>_libelleProcedure', this)"
                              onmouseout="cacheBulle('<%#($this->getClientId())%>_libelleProcedure')"
                              class="info-suite">...</span>
                        <com:TPanel ID="libelleProcedure" CssClass="info-bulle"
                                    Attributes.onmouseover="mouseOverBulle();" Attributes.onmouseout="mouseOutBulle();">
                            <div><%#$this->Data->getAcronymeTypeProcedureTraduit()%></div>
                        </com:TPanel>
                    </div>

                    <com:TPanel Visible="<%#$this->TemplateControl->isTypeConsultation()%>"
                                CssClass="consult-etape<%#$this->Data->getStatusConsultation()%>sur5"
                                Attributes.title="<%#$this->Page->ResultSearch->infoBulleEtapeConnsultation(($this->Data instanceof Application\Propel\Mpe\CommonConsultation)? $this->Data->getStatusConsultation():null)%>"></com:TPanel>
                    <com:TPanel Visible="<%#$this->TemplateControl->isTypeAnnonce()%>"
                                CssClass="annonce-etape<%#$this->Data->getStatusConsultation()%>sur3"
                                Attributes.title="<%#$this->Page->ResultSearch->infoBulleEtapeConnsultation(($this->Data instanceof Application\Propel\Mpe\CommonConsultation)? $this->Data->getStatusConsultation():null)%>"></com:TPanel>
                    <span class="auteur">
				<%#$this->SourceTemplateControl->visionRma ? $this->Data->getNomCreateur(). '&nbsp;' .$this->Data->getPrenomCreateur() : $this->Data->getNomCreateur()."<br/>".$this->Data->getPrenomCreateur()%>
			</span>
                    <com:TPanel Visible="<%=$this->SourceTemplateControl->visionRma%>">
                        <div class="spacer-mini"></div>
                        <div class="cloture-line">
                            <com:TPanel
                                    Visible="<%#(strcmp($this->Data->getDatefin(), date('Y-m-d H:i:s')) <= 0 )%>">
                                <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-time-off.gif" alt=""/>
                                <span class="time-red">
							<com:TLabel
                                    Text="<%#str_replace(' ','<br />', Application\Service\Atexo\Atexo_Util::iso2frnDateTime($this->Data->getDatefin()))%>"/>
						</span>
                            </com:TPanel>
                            <com:TPanel Visible="<%#(strcmp($this->Data->getDatefin(), date('Y-m-d H:i:s')) > 0)%>">
                                <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-time-on.gif" alt=""/>
                                <span class="time-green">
							<com:TLabel
                                    Text="<%#str_replace(' ','<br />', Application\Service\Atexo\Atexo_Util::iso2frnDateTime($this->Data->getDatefin()))%>"/>
						</span>
                            </com:TPanel>
                        </div>
                    </com:TPanel>
                </td>
                <td style="display:<%#$this->SourceTemplateControl->visionRma?'none':'block'%>" class="col-31"
                    headers="allCons_details">
                    <com:TLabel
                            Visible="<%#((strtolower(Application\Service\Atexo\Atexo_CurrentUser::getRole())!='agenthelios') && $this->TemplateControl->isTypeConsultation() && ($this->TemplateControl->getCalledFrom() != 'commission') && !$this->SourceTemplateControl->visionRma) ? true:false%>">
                        <a href="index.php?page=Agent.DetailConsultation&id=<%#$this->Data->getId()%>" data-tnr="detailConsultationAgent">
                            <com:PictoDetail ParentPage="ResultSearch"/>
                        </a>
                        <com:THyperLink
                                Visible="<%#Application\Service\Atexo\Atexo_Module::isEnabled('SuiviPassation', Application\Service\Atexo\Atexo_CurrentUser::getCurrentOrganism())%>"
                                NavigateUrl="index.php?page=Agent.DonneesComplementairesConsultation&id=<%#$this->Data->getId()%>"
                                CssClass="detail">
                            <com:TImage ImageUrl="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-donnees-comps.gif"
                                        Attributes.alt="<%=Prado::localize('VOIR_DONNEES_COMPLEMENTAIRE')%>"
                                        ToolTip="<%=Prado::localize('VOIR_DONNEES_COMPLEMENTAIRE')%>"/>
                        </com:THyperLink>
                        <com:TPanel id="panelEspaceCollaboratif"
                                    Visible="<%#Application\Service\Atexo\Atexo_Module::isEnabled('EspaceCollaboratif', Application\Service\Atexo\Atexo_CurrentUser::getCurrentOrganism())%>">
                            <com:TPanel
                                    style="display:<%#($this->TemplateControl->DisplayIconeConsulterEspaceCollaboratif($this->Data->getId(), $this->Data->getOrganisme()))? 'block':'none'%>">
                                <com:TActiveHyperLink
                                        id="hiddenConsulter"
                                        NavigateUrl="<%#$this->TemplateControl->getUrlAccesEspaceCollaboratif($this->Data->getId(), $this->Data->getOrganisme())%>"
                                        target="_blank"
                                >
                                </com:TActiveHyperLink>
                                <com:TImageButton
                                        id="consulter"
                                        cssClass="btn-action acces-espace-collaboratif"
                                        Attributes.alt="<%=Prado::localize('TEXT_ESPACE_COLLABORATIF')%>"
                                        Attributes.title="<%=Prado::localize('TEXT_ACCEDER_ESPACE_COLLABORATIF')%>"
                                        ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-espace-collaboratif.png"
                                        OnCommand="Page.ResultSearch.accederEspaceCollaboratif"
                                        CommandParameter="<%#$this->Data->getId().'@'.$this->Data->getOrganisme()%>"
                                />

                            </com:TPanel>
                            <com:TPanel id="creer"
                                        style="display:<%#($this->TemplateControl->DisplayIconeCreerEspaceCollaboratif($this->Data->getId(), $this->Data->getOrganisme()))? 'block':'none'%>">
                                <com:TActiveHyperLink

                                        NavigateUrl="<%#$this->TemplateControl->getUrlCreerEspaceCollaboratif($this->Data->getId(), $this->Data->getOrganisme(),$this->getClientId())%>"
                                        cssClass="btn-action acces-espace-collaboratif"
                                >
                                    <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-espace-collaboratif.png"
                                         alt="<%=Prado::localize('TEXT_ESPACE_COLLABORATIF')%>"/>
                                </com:TActiveHyperLink>
                            </com:TPanel>
                        </com:TPanel>
                    </com:TLabel>
                    <com:TLabel Visible="<%#$this->TemplateControl->isTypeAnnonce()%>">
                        <a href="index.php?page=Agent.DetailAnnonce&id=<%#$this->Data->getId()%>">
                            <com:PictoDetail ParentPage="ResultSearch"/>
                        </a>
                    </com:TLabel>
                    <com:TPanel ID="alerteConsultation"
                                Visible="<%#$this->Data->getAlerte() && Application\Service\Atexo\Atexo_Module::isEnabled('AlerteMetier')%>">
                        <img class="picto-alerte" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-warning-small.gif"
                             alt="<%=Prado::localize('TEXT_CONSULTATION_HAS_ALERTE')%>"/>
                    </com:TPanel>
                </td>
                <td class="col-350" headers="allCons_intitule">
                    <div class="objet-line">
                        <strong>
                            <com:TTranslate>DEFINE_INTITULE</com:TTranslate>
                            :</strong>
                        <com:TLabel ID="intitule" Text="<%#$this->Data->getIntituleTraduit()%>"/>
                    </div>
                    <div class="objet-line">
                        <strong>
                            <com:TTranslate>OBJET</com:TTranslate>
                            :</strong>
                        <%# Application\Service\Atexo\Atexo_Util::truncateTexte($this->Data->getObjetTraduit(),'230')%>
                        <span style="<%#$this->Page->ResultSearch->isTextTruncated($this->Data->getObjetTraduit())%>"
                              onmouseover="afficheBulle('ctl0_CONTENU_PAGE_ResultSearch_tableauDeBordRepeater_ctl<%#($this->ItemIndex+1)%>_infosConsultation', this)"
                              onmouseout="cacheBulle('ctl0_CONTENU_PAGE_ResultSearch_tableauDeBordRepeater_ctl<%#($this->ItemIndex+1)%>_infosConsultation')"
                              class="info-suite">...</span>
                        <com:TPanel ID="infosConsultation" CssClass="info-bulle">
                            <div><%#$this->Data->getObjetTraduit()%></div>
                        </com:TPanel>
                    </div>
                    <!-- Debut Organisme et Entité achat : Vision RMA-->
                    <com:TPanel Visible="<%=$this->SourceTemplateControl->visionRma%>" CssClass="objet-line"><strong>
                            <com:TTranslate>ORGANISME</com:TTranslate>
                            :</strong> <%#$this->Data->getDenominationOrganisme()%>
                    </com:TPanel>
                    <com:TPanel Visible="<%=$this->SourceTemplateControl->visionRma%>" CssClass="objet-line"><strong>
                            <com:TTranslate>DEFINE_TEXT_ENTITE_ACHAT</com:TTranslate>
                            :</strong>
                        <%#$this->SourceTemplateControl->getLibelleEntiteAchat($this->Data->getServiceId(),
                        $this->Data->getOrganisme())%>
                    </com:TPanel>
                    <!-- Fin Organisme et Entité achat : Vision RMA-->
                    <com:TPanel CssClass="objet-line"
                                Visible="<%=!$this->SourceTemplateControl->visionRma && $this->SourceTemplateControl->isTypeConsultation()%>">
                        <strong>
                            <com:TTranslate>TEXT_ALLOTISSEMENT</com:TTranslate>
                            :</strong> <%#($this->Data->getAlloti())? Prado::localize("TEXT_OUI"):
                        Prado::localize("TEXT_NON")%>
                    </com:TPanel>
                    <!--Debut line Infos-->
                    <com:TPanel ID="panelClauseConsultation"
                                Visible="<%#Application\Service\Atexo\Atexo_Module::isEnabled('ConsultationClause')%>">
                        <com:TPanel cssclass="objet-line"
                                    visible="<%#($this->TemplateControl->isTypeConsultation())? true:false%>">
                            <com:IconeAchatResponsableConsultation objet="<%#$this->Data%>"
                                                                   Visible="<%#Application\Service\Atexo\Atexo_Module::isEnabled('ConsultationClause')%>"/>
                            <com:TLabel visible="<%#($this->Data->getMarchePublicSimplifie() && !Application\Service\Atexo\Atexo_Module::isEnabled('MasquerElementsMps'))? true:false%>">
                                <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/logo-mps-small.png"
                                     alt="<%=Prado::localize('DEFINE_CONSULTATION_MARCHE_PUBLIC_SIMPLIFIE')%>"
                                     title="<%=Prado::localize('DEFINE_CONSULTATION_MARCHE_PUBLIC_SIMPLIFIE')%>"/>
                            </com:TLabel>
                            <span data-tnr="iconeDUME">
                                 <com:TLabel
                                         visible="<%#(Application\Service\Atexo\Atexo_Module::isEnabled('InterfaceDume') && $this->Data->getDumeDemande())? true:false%>"
                                         text="<%#$this->Data->getInfoDumeAcheteur()%>"/>
                                 </com:TPanel>
                            </span>
                    </com:TPanel>
                    <!-- Fin line Infos-->
                    <com:TPanel CssClass="objet-line" Visible="<%=!$this->SourceTemplateControl->visionRma%>">
                        <strong>
                            <com:TTranslate>DEFINE_COMMENTAIRE</com:TTranslate>
                            :</strong>
                        <com:TLabel ID="commentaire" Text="<%#$this->Data->getChampSuppInvisible()%>"/>
                    </com:TPanel>


                    <!-- procédure Amont -->
                    <com:TPanel CssClass="objet-line"
                                Visible="<%#($this->Data->getReferenceConsultationInit() != '' && !empty($this->Data->getConsultationAmont()))%>">
                        <strong>
                            <com:TTranslate>DEFINE_PROCEDURE_AMONT</com:TTranslate>
                            :
                        </strong>
                        <com:THyperLink ID="linkProcedureAmont"
                                        Attributes.title="<%=$this->Data->getConsultationAmont()->getIntitule()%>"
                                        visible="true"
                                        NavigateUrl="/index.php?page=Agent.TableauDeBord&id=<%=$this->Data->getReferenceConsultationInit()%>">
                            <com:TLabel ID="ProcedureAmont"
                                        Text="<%=$this->Data->getConsultationAmont()->getReferenceUtilisateur()%>"/>
                        </com:THyperLink>
                    </com:TPanel>
                    <!-- fin procédure Amont -->

                    <!-- procédure Aval -->
                    <com:TPanel CssClass="objet-line" Visible="<%? $this->Data->isConsultationAval() %>">
                        <strong>
                            <com:TTranslate>DEFINE_PROCEDURE_AVAL</com:TTranslate> :
                        </strong>
                        <com:TRepeater ID="consultationAvale"
                                       DataSource="<%#$this->Data->getConsultationAvals()%>"
                        >
                            <prop:ItemTemplate>
                               <a title="<%=$this->Data->getIntitule()%>"
                                  href="/index.php?page=Agent.TableauDeBord&id=<%#$this->Data->getId()%>">
                                   <%=$this->Data->getReferenceUtilisateur()%>
                               </a>
                            </prop:ItemTemplate>
                            <prop:SeparatorTemplate>
                                |
                            </prop:SeparatorTemplate>
                        </com:TRepeater>
                    </com:TPanel>
                    <!-- Fin procédure Aval -->

                </td>


                <td class="col-110 <%#($this->TemplateControl->isTypeAnnonce() ? 'center':'')%>"
                    headers="allCons_registres">
                    <com:TLabel Enabled="<%#(strtolower(Application\Service\Atexo\Atexo_CurrentUser::getRole())=='agenthelios')? false:true%>"
                                Visible="<%#$this->TemplateControl->isTypeConsultation() && ($this->TemplateControl->getCalledFrom() != 'commission')%>">
                        <div class="registre-line">
					<span class="intitule">
					 	<com:PictoRegistreRetraits
                                Activate='<%=($this->SourceTemplateControl->visionRma)?"false":"true"%>'
                                Clickable='<%=($this->SourceTemplateControl->visionRma)?"false":"true"%>'
                                CommandName="<%#$this->Data->getId()%>"
                                OnCommand="Page.ResultSearch.redirectToRegistreRetrait"/>&nbsp;&nbsp;:
					 </span>
                            <com:THyperLink ID="linkRegistreRetraitsConsultation"
                                            Enabled="<%#!$this->SourceTemplateControl->visionRma%>" visible="true"
                                            NavigateUrl="index.php?page=Agent.GestionRegistres&id=<%#$this->Data->getId()%>&type=1">
                                <com:TLabel ID="registreRetraits"
                                            Text="<%#$this->Data->getNombreTelecharegementElectronique()%> + <%#$this->Data->getNombreTelecharegementPapier()%>"/>
                            </com:THyperLink>
                        </div>
                        <div class="registre-line">
					<span class="intitule">
						<com:PictoRegistreQuestions
                                Activate="<%#($this->SourceTemplateControl->visionRma) ? true:false%>"
                                Clickable="<%#($this->SourceTemplateControl->visionRma) ? true:false%>"
                                CommandName="<%#$this->Data->getId()%>"
                                OnCommand="Page.ResultSearch.redirectToRegistreQuestion"/>&nbsp;&nbsp;:
					</span>
                            <com:THyperLink ID="linkQuestionsConsultation" visible="true"
                                            Enabled="<%#!$this->SourceTemplateControl->visionRma%>"
                                            NavigateUrl="index.php?page=Agent.GestionRegistres&id=<%#$this->Data->getId()%>&type=3">
                                <com:TLabel ID="questions"
                                            Text="<%#$this->Data->getNombreQuestionElectroniqueDces()%> + <%#$this->Data->getNombreQuestionPapierDces()%>"/>
                            </com:THyperLink>
                        </div>
                        <div class="registre-line">
					<span class="intitule">
					    <com:PictoRegistreDepot
                                Activate="<%#($this->SourceTemplateControl->visionRma) ? true:false%>"
                                Clickable="<%#($this->SourceTemplateControl->visionRma) ? true:false%>"
                                CommandName="<%#$this->Data->getId()%>"
                                OnCommand="Page.ResultSearch.redirectToRegistreDepot"/>&nbsp;&nbsp;:
					</span>
                            <com:THyperLink ID="linkRegistreDepot" visible="true"
                                            Enabled="<%#!$this->SourceTemplateControl->visionRma%>"
                                            NavigateUrl="index.php?page=Agent.GestionRegistres&id=<%#$this->Data->getId()%>&type=5">
                                <com:TLabel ID="registreDepot"
                                            Text="<%#$this->Data->getNombreOffreElectronique()%> + <%#$this->Data->getNombreOffrePapier()%>"/>
                            </com:THyperLink>
                        </div>
                        <!-- Debut Echange Chorus : nombre d'echange envoye -->
                        <com:TPanel Visible="<%#$this->SourceTemplateControl->visionRma%>" CssClass="registre-line">
					<span class="intitule">
						<input type="image" title="Echanges CHORUS : envois" alt="Echanges CHORUS : envois"
                               src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-echanges-chorus.png" id="inputEchangesChorus"
                               name="inputEchangesChorus" disabled="disabled"/>
					&nbsp;&nbsp; :
					</span> <%#$this->SourceTemplateControl->getNbChorusEchanges($this->Data)%>
                        </com:TPanel>
                        <!-- Fin Echange Chorus : nombre d'echange envoye -->
                    </com:TLabel>
                    <com:TLabel Visible="<%#$this->TemplateControl->isTypeAnnonce()%>">
                        <com:TLabel
                                Visible="<%#($this->Data->getIdTypeAvis()== Application\Service\Atexo\Atexo_Config::getParameter('TYPE_AVIS_ATTRIBUTION'))%>">
                            <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-avis-attribution.gif"
                                 alt="<%=Prado::localize('ANNONCE_ATTRIBUTION')%>"/>
                        </com:TLabel>

                        <com:TLabel
                                Visible="<%#($this->Data->getIdTypeAvis()== Application\Service\Atexo\Atexo_Config::getParameter('TYPE_AVIS_INFORMATION'))%>">
                            <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-avis-info.gif"
                                 alt="<%=Prado::localize('DEFINE_ANNONCE_INFORMATION')%>"/>
                        </com:TLabel>

                        <com:TLabel
                                Visible="<%#($this->Data->getIdTypeAvis()== Application\Service\Atexo\Atexo_Config::getParameter('TYPE_AVIS_EXTRAIT_PV'))%>">
                            <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-PV.gif" alt="<%=Prado::localize('TEXT_EXTRAIT_PV')%>"/>
                        </com:TLabel>

                        <com:TLabel
                                Visible="<%#($this->Data->getIdTypeAvis()== Application\Service\Atexo\Atexo_Config::getParameter('TYPE_AVIS_RAPPORT_ACHEVEMENT'))%>">
                            <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-rapport-achevement.gif"
                                 alt="<%=Prado::localize('TEXT_RAPPORT_ACHEVEMENT')%>"/>
                        </com:TLabel>
                        <com:TLabel
                                Visible="<%#($this->Data->getIdTypeAvis()== Application\Service\Atexo\Atexo_Config::getParameter('TYPE_AVIS_PROJET_ACHAT'))%>">
                            <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-projet-achat.gif"
                                 alt="<%=Prado::localize('ANNONCE_PROJET_ACHAT')%>"/>
                        </com:TLabel>
                    </com:TLabel>
                </td>
                <com:TPanel Visible="<%=!$this->SourceTemplateControl->visionRma%>">
                    <td class="col-100" headers="allCons_dateEnd">
                        <div class="cloture-line">
                            <com:TPanel
                                    Visible="<%#(strcmp($this->Data->getDatefin(), date('Y-m-d H:i:s')) <= 0 )%>">
                                <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-time-off.gif" alt=""/>
                                <span class="time-red">
							<com:TLabel
                                    Text="<%#str_replace(' ','<br />', Application\Service\Atexo\Atexo_Util::iso2frnDateTime($this->Data->getDatefin()))%>"/>
						</span>
                            </com:TPanel>
                            <com:TPanel Visible="<%#(strcmp($this->Data->getDatefin(), date('Y-m-d H:i:s')) > 0)%>">
                                <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-time-on.gif" alt=""/>
                                <span class="time-green">
							<com:TLabel
                                    Text="<%#str_replace(' ','<br />', Application\Service\Atexo\Atexo_Util::iso2frnDateTime($this->Data->getDatefin()))%>"/>
						</span>
                            </com:TPanel>
                        </div>
                    </td>
                </com:TPanel>
                <com:TPanel ID="colValidationElaboration"
                            visible="<%=$this->Page->ResultSearch->getNbrColHead(true) && !$this->SourceTemplateControl->visionRma%>">
                    <td headers="allCons_actionsOpt" class="actions">
                        <div>
                            <div>
                                <com:TPanel ID="colValidationElaborationNotVide"
                                            visible="<%=($this->Data instanceof Application\Propel\Mpe\CommonConsultation && $this->Data->getStatusConsultation() == Application\Service\Atexo\Atexo_Config::getParameter('STATUS_ELABORATION') && !$this->TemplateControl->isCallFromCommission())%>">

                                    <com:THyperLink
                                            ID="ctl0_CONTENU_PAGE_ResultSearch_tableauDeBordRepeater_ctl1_linkAskValidateWithChiffrement"
                                            CssClass="btn-action action-mpe" visible="true"
                                            NavigateUrl="index.php?page=Agent.FormulaireConsultation&validationElaboration=1&id=<%#base64_encode($this->Data->getId())%>">
                                        <img alt="<%=Prado::localize('DEMANDER_LA_VALIDATION')%>"
                                             src="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-demander-validation.gif"/>
                                    </com:THyperLink>
                                </com:TPanel>
                            </div>
                        </div>
                    </td>
                </com:TPanel>
                <com:TPanel Visible="<%=!$this->SourceTemplateControl->visionRma%>">
                    <td class="actions" headers="allCons_actions"
                        style="display:<%#($this->TemplateControl->getCalledFrom() != 'commission')? '':'none'%>">
                        <com:TLabel visible="<%#(strtolower(Application\Service\Atexo\Atexo_CurrentUser::getRole())=='agent') ? true:false%>">
                            <!-- Debut Liens Type Consultation -->
                            <com:TLabel Visible="<%#$this->TemplateControl->isTypeConsultation()%>">
                                <com:TPanel Visible="true">

                                    <com:TPanel
                                            Visible="<%#($this->Data->getModifHabilitation() && !$this->Data->getCurrentUserReadOnly())%>">
                                        <com:THyperLink ID="linkModifierConsultation" visible="true"
                                                        NavigateUrl="index.php?page=Agent.FormulaireConsultation&id=<%#base64_encode($this->Data->getId())%>">
                                            <com:PictoEditImage Activate="true">
                                            </com:PictoEditImage>
                                        </com:THyperLink>
                                    </com:TPanel>
                                    <com:TPanel
                                            Visible="<%#(Application\Service\Atexo\Atexo_Module::isEnabled('InterfaceModuleRsem') && $this->Data->getRedactionDocumentsRedac() && !$this->Data->getCurrentUserReadOnly())%>">
                                        <com:THyperLink ID="linkRedactionPiecesConsultation" CssClass="action-redac"
                                                        visible="true"
                                                        NavigateUrl="/agent/consultations/redaction-pieces/<%#$this->Data->getId()%>">
                                            <com:PictoRedactionImage Activate="true">
                                            </com:PictoRedactionImage>
                                        </com:THyperLink>
                                    </com:TPanel>
                                    <com:TPanel
                                            Visible="<%#($this->Data->getPublicationHabilitation()&& !$this->Data->getCurrentUserReadOnly())%>">
                                        <com:THyperLink ID="linkPublicite" visible="false" CssClass="action-mpe"
                                                        NavigateUrl="<%= Application\Service\Atexo\Atexo_Util::getBasePopUpUrl()%>index.php?cmd=<%=base64_encode('publicite')%>&cons_ref=<%#$this->Data->getId()%>">
                                            <com:PictoAdvertizeImage Activate="true"/>
                                        </com:THyperLink>
                                    </com:TPanel>

                                    <com:TPanel
                                            Visible="<%#($this->Data->getValidationHabilitation())%>">
                                        <com:THyperLink ID="linkValidateWithChiffrement" visible="true"
                                                        CssClass="action-mpe"
                                                        NavigateUrl="index.php?page=Agent.ValiderConsultation&id=<%#$this->Data->getId()%>">
                                            <com:PictoApproveValidateImage Activate="true"/>
                                        </com:THyperLink>
                                    </com:TPanel>


                                    <com:TPanel
                                            Visible="<%#($this->Data->getDepouillerHabilitation() && !$this->Data->getCurrentUserReadOnly())%>">
                                        <com:THyperLink ID="linkAccesRepOuverture" visible="true" CssClass="action-mpe"
                                                        NavigateUrl="?page=Agent.ouvertureEtAnalyse&id=<%#$this->Data->getId()%>">
                                            <com:PictoDepouillerImage ParentPage="ResultSearch" Activate="true"/>
                                        </com:THyperLink>
                                    </com:TPanel>

                                    <com:TPanel
                                            Visible="<%#($this->Data->getResultatAnalyseHabilitation()&& !$this->Data->getCurrentUserReadOnly()) && Application\Service\Atexo\Atexo_Module::isEnabled('ResultatAnalyseAvantDecision')%>">
                                        <a href="?page=Agent.resultatAnalyse&id=<%#$this->Data->getId()%>"
                                           class="action-mpe"><img src="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-resultat-analyse.gif"
                                                                   alt="<%=Prado::localize('RESULTAT_ANALYSE')%>"/></a>
                                    </com:TPanel>

                                    <com:TPanel
                                            Visible="<%#(($this->Data->getAttributionHabilitation() || $this->Data->getDecisionSuiviSeulHabilitation()) && !$this->Data->getCurrentUserReadOnly())%>">
                                        <com:THyperLink ID="linkAttribuer" CssClass="action-mpe" visible="true"
                                                        NavigateUrl="?page=Agent.ouvertureEtAnalyse&id=<%#$this->Data->getId()%>&decision">
                                            <com:PictoAttributeNotifyImage Activate="true"/>
                                        </com:THyperLink>
                                    </com:TPanel>


                                    <com:TPanel
                                            Visible="<%#($this->Data->getPublicationHabilitation() && !$this->Data->getCurrentUserReadOnly())%>">
                                        <com:THyperLink ID="linkAdvertizeConsultation" CssClass="action-mpe"
                                                        NavigateUrl="<%#$this->Page->ResultSearch->GoToAvis().base64_encode($this->Data->getId())%>">
                                            <com:TImage ImageUrl="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-MOL.gif"
                                                        Attributes.alt="<%=Prado::localize('ICONE_PUBLICICTE')%>"
                                                        Attributes.title="<%=Prado::localize('ICONE_PUBLICICTE')%>"/>
                                        </com:THyperLink>
                                    </com:TPanel>

                                    <com:TPanel
                                            style="display:<%#($this->Data instanceof Application\Propel\Mpe\CommonConsultation && $this->Data->getStatusConsultation() ==  Application\Service\Atexo\Atexo_Config::getParameter('STATUS_PREPARATION'))?'':'none'%>"
                                            Visible="<%#($this->Data->getPublicationHabilitation() && !$this->Data->getCurrentUserReadOnly() && Application\Service\Atexo\Atexo_Module::isEnabled('GestionModelesFormulaire'))%>">
                                        <com:THyperLink ID="linkFormulaireEtCriteres" CssClass="action-mpe"
                                                        visible="true"
                                                        NavigateUrl="index.php?page=Agent.GestionFormulairesEtCriteres&id=<%#base64_encode($this->Data->getId())%>">
                                            <com:TImage ImageUrl="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-formulaire.gif"
                                                        Attributes.alt="<%=Prado::localize('TEXT_FORMULAIRE_ET_CRITERES')%>"
                                                        Attributes.title="<%=Prado::localize('TEXT_FORMULAIRE_ET_CRITERES')%>"/>
                                        </com:THyperLink>
                                    </com:TPanel>

                                    <!-- Début picto statut decision -->
                                    <com:TPanel
                                            visible="<%#($this->Data instanceof Application\Propel\Mpe\CommonConsultation && $this->Data->getStatusConsultation() >= 4 && (new Application\Service\Atexo\Atexo_Module())->isChorusAccessActivate(Application\Service\Atexo\Atexo_CurrentUser::getIdServiceAgentConnected()))%>">
                                        <com:THyperLink CssClass="action-mpe"
                                                        visible="<%#Application\Service\Atexo\Atexo_CurrentUser::hasHabilitation('AttributionMarche')%>"
                                                        NavigateUrl="index.php?page=Agent.TableauDecisionChorus&id=<%#$this->Data->getId()%>&decision">
                                            <com:TImage ImageUrl="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-statut-CHORUS.gif"
                                                        Attributes.alt="<%=Prado::localize('STATUT_CHORUS')%>"
                                                        Attributes.title="<%=Prado::localize('STATUT_CHORUS')%>"/>
                                        </com:THyperLink>
                                        <com:THyperLink CssClass="action-mpe"
                                                        visible="<%#!Application\Service\Atexo\Atexo_CurrentUser::hasHabilitation('AttributionMarche')%>">
                                            <com:TImage ImageUrl="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-statut-CHORUS-inactive.gif"
                                                        Attributes.alt="<%=Prado::localize('STATUT_CHORUS')%>"
                                                        Attributes.title="<%=Prado::localize('STATUT_CHORUS')%>"/>
                                        </com:THyperLink>
                                    </com:TPanel>
                                    <!-- Fin picto statut decision -->
                                    <com:TPanel
                                            Visible="<%#((strtolower(Application\Service\Atexo\Atexo_CurrentUser::getRole())!='agenthelios') && $this->TemplateControl->isTypeConsultation() && !$this->Data->getCurrentUserReadOnly()) ? true:false%>">
                                        <com:THyperLink CssClass="action-mpe" ID="linkConsultationSuiviMessage"
                                                        NavigateUrl="index.php?page=Agent.DetailConsultation&id=<%#$this->Data->getId()%>&suivi=1">
                                            <com:TImage ImageUrl="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-suivi.gif"
                                                        Attributes.alt="<%=Prado::localize('ACCES_SUIVI_MESSAGES_ECHANGES')%>"
                                                        Attributes.title="<%=Prado::localize('ACCES_SUIVI_MESSAGES_ECHANGES')%>"/>
                                        </com:THyperLink>
                                    </com:TPanel>

                                    <!-- Début tableau de suivi des demandes de compléments -->
                                    <com:TPanel
                                            Visible="<%#(Application\Service\Atexo\Atexo_Module::isEnabled('InterfaceModuleSub') && Application\Service\Atexo\Atexo_CurrentUser::hasHabilitation('GererReouverturesModification') && $this->Data instanceof Application\Propel\Mpe\CommonConsultation && $this->Data->getStatusConsultation() ==  Application\Service\Atexo\Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE'))? true:false%>">
                                        <com:THyperLink cssClass="btn-action"
                                                        NavigateUrl="index.php?page=Agent.TableauDeBordDemandesComplements&id=<%#$this->Data->getId()%>"
                                                        Attributes.title="<%=Prado::localize('DEMANDE_COMPLEMENT')%>">
                                            <com:TImage ImageUrl="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-ajout-complement.png"
                                                        Attributes.alt="<%=Prado::localize('DEMANDE_COMPLEMENT')%>"/>
                                        </com:THyperLink>
                                    </com:TPanel>
                                    <!-- Fin tableau de suivi des demandes de compléments -->
                                    <com:TPanel
                                            Visible="<%#(Application\Service\Atexo\Atexo_Module::isEnabled('espaceDocumentaire') == '1' && Application\Service\Atexo\Atexo_CurrentUser::hasHabilitation('EspaceDocumentaireConsultation') == '1' )? true:false%>">
                                        <com:THyperLink cssClass="btn-action"
                                                        NavigateUrl="javascript:popUpSetSize('agent/espace-documentaire-consultation/<%#$this->Data->getId()%>','850px','800px','yes');">
                                            <com:TImage ImageUrl="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-folders.gif"
                                                        Attributes.alt="<%=Prado::localize('DOCUMENT_CONSULTATION')%>"
                                                        Attributes.title="<%=Prado::localize('DOCUMENT_CONSULTATION')%>"/>

                                        </com:THyperLink>
                                    </com:TPanel>
                                    <com:TPanel
                                            Visible="<%#(Application\Service\Atexo\Atexo_Module::isEnabled('EchangesDocuments', Application\Service\Atexo\Atexo_CurrentUser::getCurrentOrganism())== '1'  && Application\Service\Atexo\Atexo_CurrentUser::hasHabilitation('AccesEchangeDocumentaire') == '1' )? true:false%>">
                                        <com:THyperLink Target="_blank" cssClass="btn-action"
                                                        NavigateUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::urlPf()%>agent/echange-documentaire/<%# $this->TemplateControl->getIdCrypter($this->Data->getId())%>">
                                            <com:TImage ImageUrl="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-acces-echange-documentaire.gif"
                                                        Attributes.alt="<%=Prado::localize('MODULE_ECHANGES_DOCUMENTAIRES')%>"
                                                        Attributes.title="<%=Prado::localize('MODULE_ECHANGES_DOCUMENTAIRES')%>"/>

                                        </com:THyperLink>
                                    </com:TPanel>
                                </com:TPanel>
                            </com:TLabel>
                            <!-- Fin Liens Type Consultation -->
                            <!-- Debut Liens Type Annonce -->
                            <com:TLabel Visible="<%#$this->TemplateControl->isTypeAnnonce()%>">
                                <com:TPanel Visible="<%#$this->Data->getModifHabilitation()%>">
                                    <com:THyperLink
                                            NavigateUrl="index.php?page=Agent.FormulaireAnnonce&id=<%#$this->Data->getId()%>">
                                        <com:PictoEditImage Activate="true">
                                        </com:PictoEditImage>
                                    </com:THyperLink>
                                </com:TPanel>

                                <com:TPanel Visible="<%#$this->Data->getPublicationHabilitation()%>">
                                    <com:THyperLink visible="false"
                                                    NavigateUrl="<%= Application\Service\Atexo\Atexo_Util::getBasePopUpUrl()%>index.php?cmd=<%=base64_encode('publicite')%>&cons_ref=<%#$this->Data->getId()%>">
                                        <com:PictoAdvertizeImage Activate="true"/>
                                    </com:THyperLink>
                                </com:TPanel>

                                <com:TPanel Visible="<%#($this->Data->getValidationHabilitation())%>">
                                    <com:THyperLink
                                            NavigateUrl="index.php?page=Agent.ValiderConsultation&id=<%#$this->Data->getId()%>">
                                        <com:PictoApproveValidateImage Activate="true"/>
                                    </com:THyperLink>
                                </com:TPanel>

                                <com:TPanel
                                        Visible="<%#($this->Data->getPublicationHabilitation() && !$this->Data->getCurrentUserReadOnly())%>">
                                    <com:THyperLink ID="linkAdvertizeAnnonce"
                                                    visible="<%#(($this->Data->getAutoriserPublicite() == '1') || !Application\Service\Atexo\Atexo_Module::isEnabled('ParametragePubliciteParTypeProcedure')) ? 'true' : 'false'%>"
                                                    NavigateUrl="<%#$this->Page->ResultSearch->GoToAvis().base64_encode($this->Data->getId())%>">
                                        <com:TImage ImageUrl="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-MOL.gif"
                                                    Attributes.alt="<%=Prado::localize('ICONE_PUBLICICTE')%>"
                                                    Attributes.title="<%=Prado::localize('ICONE_PUBLICICTE')%>"/>
                                    </com:THyperLink>
                                </com:TPanel>
                                <com:TPanel
                                        Visible="<%#$this->Data->hasHabilitationModifierApresValidation() || Application\Service\Atexo\Atexo_CurrentUser::hasHabilitation('ModifierConsultationAvantValidation')%>">
                                    <com:TLinkButton
                                            Attributes.onClick="return confirm('<%#Prado::localize('CONFIRM_DELETE_AVIS')%>');"
                                            CommandParameter="<%#($this->Data->getId())%>"
                                            OnCommand="Page.ResultSearch.deleteAvis">
                                        <img src="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-supprimer-big.gif"
                                             alt="<%=Prado::localize('DEFINE_SUPPRIMER')%>"/>
                                    </com:TLinkButton>
                                </com:TPanel>
                                <com:TPanel
                                        Visible="<%#((strtolower(Application\Service\Atexo\Atexo_CurrentUser::getRole())!='agenthelios') && $this->TemplateControl->isTypeConsultation()) ? true:false%>">
                                    <com:THyperLink ID="linkAnnonceSuiviMessage"
                                                    NavigateUrl="index.php?page=Agent.DetailConsultation&id=<%#$this->Data->getId()%>&suivi=1">
                                        <com:TImage ImageUrl="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-suivi.gif"
                                                    Attributes.alt="<%=Prado::localize('ACCES_SUIVI_MESSAGES_ECHANGES')%>"/>
                                    </com:THyperLink>
                                </com:TPanel>
                            </com:TLabel>
                            <!-- Fin Liens Type Annonce -->
                        </com:TLabel>
                        <!-- Afficher les liens pour Helios-->
                        <com:TLabel
                                visible="<%#(strtolower(Application\Service\Atexo\Atexo_CurrentUser::getRole())=='agenthelios') ? true:false%>">

                            <com:THyperLink ID="linkAcceder" visible="true"
                                            NavigateUrl="index.php?page=helios.HeliosCreateTeletransmission&id=<%#base64_encode($this->Data->getId())%>">
                                <com:TImage ImageUrl="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-acces-consultation.gif"
                                            Attributes.alt="<%=Prado::localize('TEXT_CREER_TELETRANSMISSION')%>"
                                            Attributes.title="<%=Prado::localize('TEXT_CREER_TELETRANSMISSION')%>"/>
                            </com:THyperLink>
                        </com:TLabel>
                    </td>

                    <td class="actions" headers="allCons_actions"
                        style="display:<%#($this->TemplateControl->getCalledFrom() == 'commission')? '':'none'%>">
                        <!-- Afficher les actions Pour commissions -->
                        <com:TPanel
                                Visible="<%#($this->TemplateControl->getCalledFrom() == 'commission') ? true:false%>">
                            <com:TImageButton
                                    Text="<%=Prado::localize('DEFINE_AJOUTER_A_LA_SEANCE')%>"
                                    Attributes.alt="<%=Prado::localize('DEFINE_AJOUTER_A_LA_SEANCE')%>"
                                    Attributes.title="<%=Prado::localize('DEFINE_AJOUTER_A_LA_SEANCE')%>"
                                    ImageUrl="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-ajout-element.gif"
                                    OnCommand="Page.ResultSearch.ajouterOrdreDuJour"
                                    CommandParameter="<%#$this->Data->getId().'@'.$this->Data->getOrganisme().'@'. Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($_GET['id'])%>"
                            />
                        </com:TPanel>
                    </td>
                </com:TPanel>
                <!-- Debut pieces jointes RMA  -->
                <com:TPanel Visible="<%=$this->SourceTemplateControl->visionRma%>">
                    <td>
                        <ul>
                            <li class="picto-link"><img src="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-compresse.gif"
                                                        alt="<%=Prado::localize('TELECHARGEMENT')%>"
                                                        title="<%=Prado::localize('DEFINE_TELECHARGER')%>"/>
                                <com:THyperLink ID="dce"
                                                Text="<%#$this->SourceTemplateControl->getDceText($this->Data->getId(), $this->Data->getOrganisme())%>"
                                                NavigateUrl="<%#$this->SourceTemplateControl->getDceNavigateUrl($this->Data->getId())%>"></com:THyperLink>
                            </li>
                            <li class="picto-link"><img src="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-compresse.gif"
                                                        alt="<%=Prado::localize('TELECHARGEMENT')%>"
                                                        title="<%=Prado::localize('DEFINE_TELECHARGER')%>"/>
                                <com:THyperLink ID="rc"
                                                Text="<%#$this->SourceTemplateControl->getRcText($this->Data->getId(), $this->Data->getOrganisme())%>"
                                                NavigateUrl="<%#$this->SourceTemplateControl->getRcNavigateUrl($this->Data->getId())%>"></com:THyperLink>
                            </li>
                            <com:TPanel
                                    Visible="<%#$this->SourceTemplateControl->isUrlAvisBoampVisible($this->Data)%>">
                                <li class="picto-link">
                                    <com:TLinkButton
                                            CssClass="lien-ext-left acces-avis-publicite"
                                            Attributes.title="<%=Prado::localize('NOM_LIEN_AVIS_BUBLICITE_CONSULTATION')%>"
                                            Text="<%#$this->SourceTemplateControl->getTextAvisBoamp($this->Data->getId(), $this->Data->getOrganisme())%>"
                                            Attributes.onClick="openModal('modal-avis-publicite-<%#$this->ItemIndex%>','',this);return false;"
                                    >
                                    </com:TLinkButton>
                                </li>
                                <!--Debut Modal Avis publicité-->
                                <com:TPanel ID="modalAvisBoamp" CssClass="modal-avis-publicite-<%#$this->ItemIndex%>"
                                            Style="display:none;">
                                    <div class="form-bloc">
                                        <div class="top"><span class="left"></span><span class="right"></span></div>
                                        <div class="content">
                                            <div class="bloc-docs-link">
                                                <div class="blue bold">
                                                    <com:TTranslate>TEXT_LISTES_FORMULAIRES_PUBLICITE_AVIS
                                                    </com:TTranslate>
                                                </div>
                                                <ul>
                                                    <com:TRepeater ID="avisBoamp"
                                                                   DataSource="<%#$this->SourceTemplateControl->getDataSourceAvis($this->Data)%>"
                                                    >
                                                        <prop:ItemTemplate>
                                                            <li style="display:<%#($this->Data->getTypeFormat()== Application\Service\Atexo\Atexo_Config::getParameter('TYPE_FORMAT_LIBRE_FICHIER_JOINT')) ?  'block':'none'%>"
                                                                class="picto-link">
                                                                <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-compresse.gif"
                                                                     alt="<%=Prado::localize('DEFINE_TELECHARGER_AVIS')%>"
                                                                     title="<%=Prado::localize('DEFINE_TELECHARGER_AVIS')%>"/>
                                                                <a title="<%#$this->Data->getDatePub()%> - <%#$this->Data->getTaille()%>"
                                                                   href="index.php?page=Agent.DownloadAvisJAL&id=<%#$this->TemplateControl->Data->getReference()%>&orgAcronyme=<%#$this->TemplateControl->Data->getOrganisme()%>&idAvis=<%#$this->Data->getId()%>"><%#$this->Data->getLibelleType()%></a>
                                                            </li>

                                                            <li style="display:<%#($this->Data->getTypeFormat()== Application\Service\Atexo\Atexo_Config::getParameter('TYPE_FORMAT_LIBRE_URL')) ?  'block':'none'%>"
                                                                class="picto-link">
                                                                <a class="lien-ext-left" target="_blank"
                                                                   title="<%=Prado::localize('TEXT_LIEN_ACCES_DIRECT')%>"
                                                                   href="<%#$this->Data->getUrl()%>">
                                                                    <com:TTranslate>TEXT_LIEN_ACCES_DIRECT
                                                                    </com:TTranslate>
                                                                </a>
                                                            </li>

                                                            <li style="display:<%#($this->Data->getTypeFormat()== Application\Service\Atexo\Atexo_Config::getParameter('TYPE_FORMAT_URL_BOAMP')) ?  'block':'none'%>"
                                                                class="picto-link">
                                                                <a class="lien-ext-left" target="_blank"
                                                                   title="<%#$this->Data->getLibelleType()%>"
                                                                   href="<%#$this->Data->getUrl()%>"><%#$this->Data->getLibelleType()%></a>
                                                            </li>
                                                        </prop:ItemTemplate>
                                                    </com:TRepeater>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="bottom"><span class="left"></span><span class="right"></span></div>
                                    </div>
                                    <div class="boutons-line">
                                        <input type="button" class="bouton-moyen float-left" value="Fermer"
                                               title="Fermer"
                                               onclick="J('.modal-avis-publicite-<%#$this->ItemIndex%>').dialog('close');"/>
                                    </div>
                                </com:TPanel>
                                <!--Fin Modal Avis publicité-->
                            </com:TPanel>
                        </ul>
                    </td>
                </com:TPanel>
                <!-- Fin pieces jointes RMA  -->
            </tr>
        </prop:ItemTemplate>
        <prop:FooterTemplate>
            </table>
        </prop:FooterTemplate>
    </com:TRepeater>
    <!--Debut partitionneur-->
    <div class="line-partitioner">
        <div class="partitioner">
            <div class="intitule"><strong><label for="allCons_nbResultsTop">
                        <com:TTranslate>DEFINE_TEXT_AFFICHER</com:TTranslate>
                    </label></strong></div>
            <com:TDropDownList ID="nombreResultatAfficherBottom" AutoPostBack="true"
                               onSelectedIndexChanged="changePagerLenght"
                               Attributes.title='<%=Prado::localize("NOMBRE_DE_RESULTAT_PAR_PAGE")%>'>
                <com:TListItem Text="10" Selected="10"/>
                <com:TListItem Text="20" Value="20"/>
            </com:TDropDownList>
            <div class="intitule">
                <com:TTranslate>DEFINE_RESULTATS_PAGE</com:TTranslate>
            </div>
            <label style="display:none;" for="allCons_pageNumberBottom">
                <com:TTranslate>ALLER_A_LA_PAGE</com:TTranslate>
            </label>

            <com:TPanel ID="panelBouttonGotoPageBottom" DefaultButton="DefaultButtonBottom" CssClass="float-left">
                <com:TTextBox ID="numPageBottom" Attributes.title='<%=Prado::localize("N_DE_LA_PAGE")%>' Text="1"/>
                <div class="nb-total ">/
                    <com:TLabel ID="nombrePageBottom"/>
                </div>
                <com:TButton ID="DefaultButtonBottom" OnClick="goToPage" Attributes.style="display:none"/>
            </com:TPanel>

            <div class="liens">
                <com:TPager ID="PagerBottom"
                            ControlToPaginate="tableauDeBordRepeater"
                            FirstPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-premier.gif' alt='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>' title='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>' />"
                            LastPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-dernier.gif' alt='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>' title='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>' />"
                            Mode="NextPrev"
                            NextPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-suivant.gif' alt='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>' title='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>' />"
                            PrevPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-precedent.gif' alt='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>' title='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>' />"
                            OnPageIndexChanged="pageChanged"/>

            </div>
        </div>
    </div>
    <!--Fin partitionneur-->
    <!--Fin Tableau consultations-->
</com:TPanel>
<com:TLabel id="scriptJS"/>

<script>
    //J(document).ready(function () {
    function addFavoris() {
        J('.addFavoris').unbind('click').bind('click', function () {
            var reference = J(this).attr('data');
            showLoader();
            var link = J(this);
            J.ajax({
                method: "POST",
                url: "/?page=Agent.addFavoris",
                data: {data: reference }
            }).done(function (resultJson) {
                link.removeClass('addFavoris');
                link.addClass('deleteFavoris');
                link.html('<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t().'/images/full-star.png'%>" alt="" style="border-width:0;">');
                hideLoader();
                addFavoris();
                deleteFavoris();
            }).fail(function () {
                hideLoader();
            });


        });
    }

    function deleteFavoris() {

        J('.deleteFavoris').unbind('click').bind('click', function () {
            var reference = J(this).attr('data');
            showLoader();
            var link = J(this);
            J.ajax({
                method: "POST",
                url: "/?page=Agent.deleteFavoris",
                data: {data: reference }
            }).done(function (resultJson) {
                link.removeClass('deleteFavoris');
                link.addClass('addFavoris');
                link.html('<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t().'/images/empty-star.png'%>" alt="" style="border-width:0;">');
                hideLoader();
                addFavoris();
                deleteFavoris();
            }).fail(function () {
                hideLoader();
            });
        });
    }

    addFavoris();
    deleteFavoris();
   // })
</script>
