<com:TConditional condition="$this->TemplateControl->getVisible()">
    <prop:trueTemplate>
        <div class="modal kt-ajaxmodal fade" id="modalGestionPanier" tabindex="-1" role="dialog" aria-labelledby="modalGestionPanier">
            <div class="modal-dialog" role="document">
                 <com:TForm Attributes.name="form" Attributes.class="form" Attributes.method="GET">
                    <div class="modal-content form-horizontal">
                        <div class="modal-body">
                            <div class="text-danger">
                                <%=$this->getMessage()%>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <div class="clearfix">
                                <div class="pull-left">

                                </div>
                                <div class="pull-right">
                                    <button type="reset" class="btn btn-sm btn-default btn-reset" data-dismiss="modal">
                                        <%=Prado::localize('DEFINE_ANNULER')%>
                                    </button>
                                    <button type="button" class="btn btn-sm btn-danger btn-default btn-submit" title="<%=Prado::localize('DEFINE_SUPPRIMER_DU_PANIER')%>"
                                            data-toggle="ajaxmodal"
                                            data-target="?page=Entreprise.EntrepriseModal&controls=CAtexoModalGestionPanier&action=deleteConfirmation&id=<%=Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($_GET['id'], ENT_QUOTES | ENT_HTML5, 'UTF-8')%>&orgAcronyme=<%=Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme'], ENT_QUOTES | ENT_HTML5, 'UTF-8')%>&idRow=<%=Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($_GET['idRow'], ENT_QUOTES | ENT_HTML5, 'UTF-8')%>"
                                            data-json='{
                                                            "ajaxmodal":{
                                                                "callback":"KTJS.pages_entreprise_advancedsearch.consultation.callback.deleteConfirmation"
                                                            },
                                                            "data":{
                                                                "target":"#<%=Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($_GET["idRow"], ENT_QUOTES | ENT_HTML5, "UTF-8")%>"
                                                            }
                                                        }'>
                                        <%=Prado::localize('TEXT_POURSUIVRE')%>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </com:TForm>
            </div>
        </div>
    </prop:trueTemplate>
</com:TConditional>