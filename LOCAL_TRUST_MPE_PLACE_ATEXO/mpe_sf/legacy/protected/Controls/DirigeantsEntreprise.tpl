<com:TConditional condition="$this->getResponsive()">
	<prop:trueTemplate>
		<%include Application.templates.responsive.DirigeantsEntreprise %>
	</prop:trueTemplate>
	<prop:falseTemplate>
		<%include Application.templates.mpe.DirigeantsEntreprise %>
	</prop:falseTemplate>
</com:TConditional>