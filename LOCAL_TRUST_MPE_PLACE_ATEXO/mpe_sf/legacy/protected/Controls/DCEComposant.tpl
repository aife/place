<!--Debut Bloc Modifier le Réglement de consultation-->
					<com:TPanel id="panelRc" cssClass="form-field" Visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('ReglementConsultation')) && $this->getBlocDCE() %>">
						<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><span class="title"><com:TTranslate>REGLEMENT_DE_CONSULTATION</com:TTranslate></span></div>
						<div class="content">
                            <com:ActivePanelMessageAvertissement ID="activeInfoMsg" Message="<%=Prado::localize('MSG_INFO_PROCEDURE_REPONDANT_SERVICE_MPS')%>"></com:ActivePanelMessageAvertissement>
                            <!--Debut Line Remplacer RC-->
							<div class="line">
								<div class="intitule-auto">
									<com:TTranslate>AJOUTER_MODIFIER_LE_REGLEMENT_DE_CONSULTATION</com:TTranslate>
								</div>
								<div class="breaker"></div>
								<div class="intitule-auto indent-25">
									<com:THyperLink  ID="linkDR" NavigateUrl="javascript:popUp('index.php?page=Agent.PopupRedacDR&id=<%= $this->getConsultation()->getId() %>&response=<%=$this->paramDocReglement->getClientId()%>','yes');" cssClass="arrow-link" Text="<%=Prado::localize('CHOIX_DU_DOCUMENT')%>" />
								</div>

								<div class="intitule-auto indent-30" id="divBlocPiecesRgMoved" style="display:none">
									<com:TLabel cssClass="blue" ID="oldDocRg"/>
									<span class="indent-10"> <com:TTranslate>REPLACE_WITH</com:TTranslate></span>
									<com:TLabel cssClass="blue indent-10"  ID="newDocRg"/>
								</div>

								<com:TActivePanel ID="panelDownloadRg" Visible="false">
									<div class="picto-link">
										<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-compresse.gif" alt="<com:TTranslate>TELECHARGEMENT_COMPLET</com:TTranslate>" title="<com:TTranslate>TELECHARGEMENT_COMPLET</com:TTranslate>" />
										<com:THyperLink ID="linkDownloadRg" Text="<%=Prado::localize('DEFINE_TELECHARGER_VERSION_ACTUELLE')%>">
											<com:TLabel ID="sizeRg"/>
										</com:THyperLink>
									</div>
								</com:TActivePanel>
							</div>
							<!--Fin Line Remplacer RC-->
							<div class="breaker"></div>
						</div>
						<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
					</com:TPanel>
					<!--Fin Bloc Modifier le Réglement de consultation -->

<!--Debut line DCE-->
				<div class="form-field">
					<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><span class="title"><com:TTranslate>DEFINE_DCE</com:TTranslate></span></div>
					<div class="content">
						<div class="spacer-mini"></div>
						<!--Debut Line Remplacer une piece du DCE-->
						<com:TPanel id="panelBlocDCE" Visible="<%=$this->getBlocDCE()%>">
							<div class="line">
								<div class="intitule-auto">
									<com:TRadioButton id="remplacerPieceDce" GroupName="pieceDce" Attributes.title="<%=Prado::localize('REMPLACER_PIECE_DCE')%>" cssClass="radio" />
										<label for="remplacerPieceDce"><com:TTranslate>CONSTITUER_MODIFIER_LE_DCE_PIECE_PAR_PIECE</com:TTranslate></label>
								</div>
								<div class="breaker"></div>
								<div class="intitule-auto indent-25">
									<com:THyperLink  ID="linkDCEUpdate" NavigateUrl="javascript:popUp('index.php?page=Agent.PopupRedacDCE&id=<%=$this->getConsultation()->getId()%>&response=<%=$this->getClientId()%>','yes');" cssClass="arrow-link" Text="<%=Prado::localize('CHOIX_DES_PIECES')%>" />
								</div>

							</div>
							<div class="line">
								<div class="intitule-auto indent-30" id="divBlocPiecesDCEChanged" style="display:none">
									<com:TLabel cssClass="blue" ID="DCEChangedLabel"/>
								</div>
							</div>
							<!--Fin Line Remplacer une piece du DCE-->
							<div class="spacer-mini"></div>
							<!--Debut Line Remplacer tout le DCE-->
							<div class="line">
								<div class="intitule-auto">
									<com:TRadioButton id="remplacerToutDce" GroupName="pieceDce" Attributes.title="<%=$this->getTextDce()%>" cssClass="radio" checked="true" />
										<label for="remplacerToutDce"><%=$this->getTextDce()%></label>
								</div>
								<div class="breaker"></div>
								<div class="intitule-150 bloc-720 indent-30"><label for="uploadBar"><com:TTranslate>CHOIX_DU_DCE</com:TTranslate></label> :</div>
								<div class="content-bloc bloc-570">
										<com:AtexoQueryFileUpload
											id="uploadBar"
											CssClass="file-550"
											style="display:<%=($this->getUseUplodeBar())? '':'none'%>"
											size="90"
											afficherProgress="1"
										/>
									<span class="info-aide-small"><com:TTranslate>TEXT_COMPRESSE_OBLIGATOIRE</com:TTranslate></span>
								</div>
							</div>
							<!--Fin Line Remplacer tout le DCE-->
							<div class="spacer"></div>
						</com:TPanel>

						<!--Debut Line Telechargement partiel/version actuelle-->
						<div class="line">
							<div class="content-bloc bloc-570">
								<com:TActivePanel id="paneltelechargementPartiel" >
									<div class="intitule-auto">
										<com:TActiveCheckBox id="telechargementPartiel" CssClass="check"   />
										<label for="telechargementPartiel"><com:TTranslate>DEFINE_TELECHARGEMENT_PARTIEL_DCE</com:TTranslate></label>
									</div>
								</com:TActivePanel>
								<com:TActivePanel ID="PanelDownloadDce" visible="false">
									<div class="picto-link"> 
										<img title="<%=Prado::localize('DEFINE_TELECHARGER_VERSION_ACTUELLE')%>" alt="<%=Prado::localize('TEXT_TELECHARGEMENT_COMPLET')%>" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-compresse.gif" />
										<a href="index.php?page=Agent.DownloadDce&id=<%=base64_encode($this->getConsultation()->getId())%>" id="ctl0_CONTENU_PAGE_linkDownloadDce"><com:TTranslate>DEFINE_TELECHARGER_VERSION_ACTUELLE</com:TTranslate></a>
									</div>
								</com:TActivePanel>
							</div>
						</div>
						<!--Fin Line Telechargement partiel/version actuelle-->
						<div class="breaker"></div>
					</div>
					<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
				</div>

				<!--Fin Line DCE-->
				<com:THiddenField ID="paramSource" /> 
				<com:THiddenField ID="paramDestination" />
				<com:THiddenField ID="paramDocReglement" />
				<com:THiddenField ID="fileToScan"/> 
				<com:THiddenField ID="filesToDelete"/>
				<com:THiddenField ID="fileToReplace"/>