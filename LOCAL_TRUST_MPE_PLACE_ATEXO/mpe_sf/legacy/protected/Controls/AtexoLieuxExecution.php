<?php

namespace Application\Controls;

class AtexoLieuxExecution extends MpeTTemplateControl
{
    public $libelleLieuxExecution;

    public function setLibelleLieuxExecution($libelleLieuxExecution)
    {
        $this->libelleLieuxExecution = $libelleLieuxExecution;
    }

    public function getLibelleLieuxExecution()
    {
        return $this->libelleLieuxExecution;
    }

    public function truncateLieuExec()
    {
        $truncatedText = '';
        $arrayText = explode(',', $this->libelleLieuxExecution);
        $index = 0;
        foreach ($arrayText as $oneText) {
            if ($oneText) {
                if (2 == $index) {
                    break;
                }
                $truncatedText .= ', '.$oneText;
                ++$index;
            }
        }

        return $truncatedText;
    }

    public function isLieuExecTruncated()
    {
        $arrayText = explode(',', $this->libelleLieuxExecution);

        return (count($arrayText) > 2) ? '' : 'display:none';
    }
}
