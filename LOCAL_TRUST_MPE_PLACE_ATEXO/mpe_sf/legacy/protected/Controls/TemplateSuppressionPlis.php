<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Responses;
use Prado\Prado;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class TemplateSuppressionPlis extends MpeTPage
{
    public function fillRepeaterWithDataForSearchResult($indexPage = 0)
    {
        $this->javascript->Text = '';
        $plis = (new Atexo_Consultation_Responses())->retrievePlisASupprimer();
        $nombreElement = is_countable($plis) ? count($plis) : 0;
        if ($nombreElement >= 1) {
            $this->panelMoreThanOneElementFound->setVisible(true);
            $this->panelNoElementFound->setVisible(false);
            $this->nombreElement->Text = $nombreElement;
            $this->PagerBottom->setVisible(true);
            $this->PagerTop->setVisible(true);
            $this->setViewState('nombreElement', $nombreElement);
            $this->nombrePageTop->Text = ceil($nombreElement / $this->tableauDeBordRepeater->PageSize);
            $this->nombrePageBottom->Text = ceil($nombreElement / $this->tableauDeBordRepeater->PageSize);
            $this->tableauDeBordRepeater->setVirtualItemCount($nombreElement);
            $this->tableauDeBordRepeater->setCurrentPageIndex($indexPage);
            $this->populateData();
        } else {
            $this->javascript->Text = "<script>document.getElementById('ctl0_CONTENU_PAGE_".$this->Id."_panelMoreThanOneElementFound').style.display='none'</script>";
            $this->tableauDeBordRepeater->DataSource = [];
            $this->tableauDeBordRepeater->DataBind();
        }
    }

    public function populateData()
    {
        $offset = $this->tableauDeBordRepeater->CurrentPageIndex * $this->tableauDeBordRepeater->PageSize;
        $limit = $this->tableauDeBordRepeater->PageSize;
        if ($offset + $limit > $this->tableauDeBordRepeater->getVirtualItemCount()) {
            $limit = $this->tableauDeBordRepeater->getVirtualItemCount() - $offset;
        }
        $champsOrderBy = $this->getViewState('sortByElement', '');
        $arraySensTri = $this->Page->getViewState('sensTriArray');
        $sensTri = ($arraySensTri[$champsOrderBy] ?? 'ASC');

        $plis = (new Atexo_Consultation_Responses())->retrievePlisASupprimer($champsOrderBy, $sensTri, $limit, $offset);
        $this->tableauDeBordRepeater->DataSource = $plis;
        $this->tableauDeBordRepeater->DataBind();
    }

    public function Trier($sender, $param)
    {
        $champsOrderBy = $param->CommandName;
        $this->setViewState('sortByElement', $champsOrderBy);
        $arraySensTri = $this->Page->getViewState('sensTriArray', []);
        $arraySensTri[$champsOrderBy] = ('ASC' == $arraySensTri[$champsOrderBy]) ? 'DESC' : 'ASC';
        $this->Page->setViewState('sensTriArray', $arraySensTri);
        $this->populateData();
    }

    public function pageChanged($sender, $param)
    {
        $this->tableauDeBordRepeater->CurrentPageIndex = $param->NewPageIndex;
        $this->numPageBottom->Text = $param->NewPageIndex + 1;
        $this->numPageTop->Text = $param->NewPageIndex + 1;
        $criteriaVo = $this->Page->getViewState('CriteriaVo');
        $this->populateData();
    }

    public function elementSortedWith($panelName, $openCloseTag)
    {
        if ($this->getViewState('sortByElement') == $panelName) {
            if ('open' == $openCloseTag) {
                return '<strong>';
            } else {
                return '</strong>';
            }
        } else {
            return '';
        }
    }

    public function goToPage($sender, $param)
    {
        $numPage = null;
        switch ($sender->ID) {
            case 'DefaultButtonTop':
                 $numPage = $this->numPageTop->Text;
                break;
            case 'DefaultButtonBottom':
                 $numPage = $this->numPageBottom->Text;
                break;
        }
        if (Atexo_Util::isEntier($numPage)) {
            if ($numPage >= $this->nombrePageTop->Text) {
                $numPage = $this->nombrePageTop->Text;
            } elseif ($numPage <= 0) {
                $numPage = 1;
            }
            $this->tableauDeBordRepeater->CurrentPageIndex = $numPage - 1;
            $this->numPageBottom->Text = $numPage;
            $this->numPageTop->Text = $numPage;
            $criteriaVo = $this->Page->getViewState('CriteriaVo');
            $this->populateData();
        } else {
            $this->numPageTop->Text = $this->tableauDeBordRepeater->CurrentPageIndex + 1;
            $this->numPageBottom->Text = $this->tableauDeBordRepeater->CurrentPageIndex + 1;
        }
    }

    public function changePagerLenght($sender, $param)
    {
        switch ($sender->ID) {
            case 'nombreResultatAfficherBottom':
                 $pageSize = $this->nombreResultatAfficherBottom->getSelectedValue();
                                                  $this->nombreResultatAfficherTop->setSelectedValue($pageSize);
                break;
            case 'nombreResultatAfficherTop':
                 $pageSize = $this->nombreResultatAfficherTop->getSelectedValue();
                                               $this->nombreResultatAfficherBottom->setSelectedValue($pageSize);
                break;
        }

        $this->tableauDeBordRepeater->PageSize = $pageSize;
        $nombreElement = $this->getViewState('nombreElement');
        $this->nombrePageTop->Text = ceil($nombreElement / $this->tableauDeBordRepeater->PageSize);
        $this->nombrePageBottom->Text = ceil($nombreElement / $this->tableauDeBordRepeater->PageSize);
        $criteriaVo = $this->Page->getViewState('CriteriaVo');
        $this->tableauDeBordRepeater->setCurrentPageIndex(0);
        $this->populateData();
    }

    public function infoBulleEtapeConnsultation($statusConsultation)
    {
        if ($statusConsultation == Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE')) {
            return Prado::localize('ETAPE_OUVERTURE_ET_ANALYSE');
        }
        if ($statusConsultation == Atexo_Config::getParameter('STATUS_ELABORATION')) {
            return Prado::localize('ETAPE_ELABORATION');
        }
        if ($statusConsultation == Atexo_Config::getParameter('STATUS_PREPARATION')) {
            return Prado::localize('ETAPE_PREPARATION');
        }
        if ($statusConsultation == Atexo_Config::getParameter('STATUS_CONSULTATION')) {
            return Prado::localize('ETAPE_CONSULTATION');
        }
        if ($statusConsultation == Atexo_Config::getParameter('STATUS_DECISION')) {
            return Prado::localize('ETAPE_DECISION');
        }

        if ($statusConsultation == Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE').Atexo_Config::getParameter('STATUS_DECISION')) {
            return Prado::localize('TEXT_ETAPE_O_A_D');
        }

        if ($statusConsultation == Atexo_Config::getParameter('STATUS_CONSULTATION').Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE').Atexo_Config::getParameter('STATUS_DECISION')) {
            return Prado::localize('TEXT_ETAPE_C_O_A_D');
        }
    }

    /** Pour insérer un retour à la ligne (<br>), tous les 22 caractères, dans une chaine.
     * @param : $texte : texte dans lequel sera ajouter les <br>
     *
     * @return : retourn la chaine après y avoir insérer un retour à la lligne tous les $everyHowManyCaracter caractères
     */
    public function insertBrWhenTextTooLong($texte, $everyHowManyCaracter)
    {
        if ($texte) {
            return wordwrap($texte, $everyHowManyCaracter, '<br>', true);
        }
    }

    public function truncate($texte)
    {
        $truncatedText = null;
        $maximumNumberOfCaracterToReturn = 200;
        $arrayText = explode(' ', $texte);
        $indexArrayText = 0;
        $textlenght = strlen($arrayText[$indexArrayText]) + 1;
        $truncatedText .= $arrayText[$indexArrayText];
        while (isset($arrayText[$indexArrayText]) && $textlenght <= $maximumNumberOfCaracterToReturn) {
            ++$indexArrayText;
            if ($textlenght + (strlen($arrayText[$indexArrayText]) + 1) > $maximumNumberOfCaracterToReturn) {
                break;
            }
            $textlenght += strlen($arrayText[$indexArrayText]) + 1;
            $truncatedText .= ' '.$arrayText[$indexArrayText];
        }

        return $truncatedText;
    }

    public function isTextTruncated($texte, $nbrCaractere = 100)
    {
        return (strlen($texte) > 200) ? '' : 'display:none';
    }

    public function pliSupprime($typeEnv, $libelletypeEnveloppe)
    {
        if ($typeEnv == Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE')) {
            return Prado::localize('REPONSE_COMPLETE');
        } else {
            return $libelletypeEnveloppe;
        }
    }
}
