<?php

namespace Application\Controls;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonProcedureEquivalence;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_FormConsultation;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureEquivalence;
use AtexoCrypto\Dto\Chiffrement;
use AtexoCrypto\Dto\Signature;
use Prado\Prado;

/**
 * Page de gestion des modalités de réponse de la consultation.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 4
 */
class ModalitesReponseConsultation extends MpeTTemplateControl
{
    public $consultation;
    public ?string $_affiche = null; // (=1 pour afficher, 0 sinon)
    public $_mode;

    /**
     * recupère la consultation.
     */
    public function getConsultation()
    {
        return $this->consultation;
    }

    // getConsultation()

    /**
     * Affecte l'objet de la consultation.
     *
     * @param objet $consultation
     */
    public function setConsultation($consultation)
    {
        if ($this->consultation !== $consultation) {
            $this->consultation = $consultation;
        }
    }

    // setConsultation()

    /**
     * recupère la valeur.
     */
    public function getAffiche()
    {
        return $this->_affiche;
    }

    // getAffiche()

    /**
     * Affecte la valeur.
     *
     * @param string $value
     */
    public function setAffiche($value)
    {
        if ($this->_affiche !== $value) {
            $this->_affiche = $value;
        }
    }

    // setAffiche()

    /**
     * recupère le mode.
     */
    public function getMode()
    {
        return $this->mode;
    }

    // getMode()

    /**
     * Affecte le mode.
     *
     * @param string $mode
     */
    public function setMode($mode)
    {
        if ($this->mode !== $mode) {
            $this->mode = $mode;
        }
    }

    // setMode()

    /**
     * Chargement de la page.
     */
    public function onLoad($param)
    {
        if (!$this->Page->isPostBack) {
            //Début remplissage nombre jours poursuite affichage
            $this->remplirNbrJoursPoursuivreAffichage();
            //Fin remplissage nombre jours poursuite affichage

            //Chargement du formulaire
            if ($this->getConsultation()) {
                $this->chargerFormulaireModalitesReponses();
            }
            // Module afficherTjrBlocCaracteristiqueReponse
            $this->afficherTjrBlocCaracteristiqueReponse->value = '0';
            if (Atexo_Module::isEnabled('afficherTjrBlocCaracteristiqueReponse')) {
                $this->afficherTjrBlocCaracteristiqueReponse->value = '1';
            }
        }
        //$this->showHideBlocReponse();
    }

    /**
     * Permet de remplir le nombre de jour de poursuite de l'affichage.
     */
    public function remplirNbrJoursPoursuivreAffichage()
    {
        $nombreJour = [];
        $valeursPoursuivreAffichage = json_decode(str_replace("'", '"', Atexo_Config::getParameter('NBR_JOURS_POURSUIVRE_AFFICHAGE')), null, 512, JSON_THROW_ON_ERROR);
        foreach ($valeursPoursuivreAffichage as $valeur) {
            $nombreJour[$valeur[0].$valeur[1]] = $valeur[0].' '.Prado::localize($valeur[1].($valeur[0] > 1 ? 'S' : ''));
        }
        //$nombreJour[Atexo_Config::getParameter('NBR_JOURS_POURSUIVRE_AFFICHAGE_VALEUR2')] = prado::localize('UN_JOUR');
        //$nombreJour[Atexo_Config::getParameter('NBR_JOURS_POURSUIVRE_AFFICHAGE_VALEUR3')] = prado::localize('TRENTE_JOURS');
        $this->poursuivreAffichageNbJour->Datasource = $nombreJour;
        $this->poursuivreAffichageNbJour->dataBind();
    }

    /**
     * Permet de charger le formulaire des modalités de réponses.
     */
    public function chargerFormulaireModalitesReponses()
    {
        //Récupération de la consultation
        $consultation = $this->getConsultation();

        //Afficher/masquer en fonction du parametrage des types de procedures
        $this->afficherMasquerBlocsEnFonctionParametrageProcedures($consultation);

        //Constitution les modalités de réponses
        $this->chargerBlocModalitesReponses($consultation);

        //Constitution des dossiers de réponse
        $this->chargerBlocConstitutionDossiersReponses($consultation);
    }

    /**
     * Permet de charger l'objet consultation avec les données du formulaire modalités de réponse.
     *
     * @param CommonConsultation $consultation
     */
    public function save(&$consultation, $modification = true)
    {
        if ($consultation instanceof CommonConsultation) {
            //Bloc modalités des réponses
            $this->saveModalitesReponses($consultation, $modification);

            //Bloc constitution de la réponse
            $this->saveConstitutionReponses($consultation, $modification);
        }
    }

    /**
     * Permet de charger les dossiers de reponses.
     *
     * @param CommonConsultation $consultation
     */
    public function chargerBlocConstitutionDossiersReponses($consultation)
    {
        $scriptJs = '';
        if ('1' == $consultation->getEnvCandidature()) {
            $this->envelopCandidature->Checked = true;
            /*if($consultation->getDossierAdditif()== '1' ){
                $this->dossierAdditif->Checked = true;
            }else{
                $this->dossierAdditif->Checked = false;
            }*/
            $scriptJs .= "document.getElementById('panel_dossier_candidature').className= 'content';";
        } else {
            $this->envelopCandidature->Checked = false;
            $scriptJs .= "document.getElementById('panel_dossier_candidature').className= 'content hidden';";
        }
        if ('1' == $consultation->getEnvOffre()) {
            $this->envelopOffre->Checked = true;
            //$this->envelopUnique->Checked = true;
            //$this->envelopParLot->Checked = false;
            //$this->acteEngFormeDissociee->Checked = Atexo_FormConsultation::isFormAccesExists($this->getConsultation()->getOrganisme(), $this->getConsultation()->getReference(),
            //Atexo_Config::getParameter('TYPE_ENV_OFFRE'), Atexo_Config::getParameter('ACTE_D_ENGAGEMENT_DISSOCIE'));
            $scriptJs .= "document.getElementById('panel_dossier_offre').className= 'content';";
        } elseif ('0' == $consultation->getEnvOffre()) {
            $this->envelopOffre->Checked = false;
            $scriptJs .= "document.getElementById('panel_dossier_offre').className= 'content hidden';";
        } else {
            $this->envelopOffre->Checked = true;
            //$this->envelopParLot->Checked = true;
            //$this->envelopUnique->Checked = false;
            //$this->acteEngFormeDissociee->Checked = Atexo_FormConsultation::isFormAccesExists($this->getConsultation()->getOrganisme(), $this->getConsultation()->getReference(),
            //Atexo_Config::getParameter('TYPE_ENV_OFFRE'), Atexo_Config::getParameter('ACTE_D_ENGAGEMENT_DISSOCIE'));
            $scriptJs .= "document.getElementById('panel_dossier_offre').className= 'content';";
        }

        if ('1' == $consultation->getEnvOffreTechnique()) {
            $this->envelopOffreTechnique->Checked = true;
            //$this->envelopTechniqueUnique->Checked = true;
            //$this->envelopTechniqueParLot->Checked = false;
            $scriptJs .= "document.getElementById('panel_dossier_offreTech').className= 'content';";
        } elseif ('0' == $consultation->getEnvOffreTechnique()) {
            $this->envelopOffreTechnique->Checked = false;
        } else {
            $this->envelopOffreTechnique->Checked = true;
            //$this->envelopTechniqueParLot->Checked = true;
            //$this->envelopTechniqueUnique->Checked = false;
            $scriptJs .= "document.getElementById('panel_dossier_offreTech').className= 'content';";
        }

        if ('1' == $consultation->getEnvAnonymat()) {
            $this->anonymat->Checked = true;
            $scriptJs .= "document.getElementById('panel_dossier_anonymat').className= 'content';";
        } else {
            $this->anonymat->Checked = false;
            $scriptJs .= "document.getElementById('panel_dossier_anonymat').className= 'content hidden';";
        }
        $this->Page->scriptJs->Text .= "<script>$scriptJs</script>";
    }

    /**
     * Permet de charger les modalités de réponses.
     *
     * @param CommonConsultation $consultation
     */
    public function chargerBlocModalitesReponses($consultation)
    {
        if ($consultation instanceof CommonConsultation) {
            //Réponse électronique
            if ('1' == $consultation->getReponseObligatoire()) {
                $this->obligatoireOui->checked = true;
            } elseif ('1' == $consultation->getAutoriserReponseElectronique()) {
                $this->repAutorisee->checked = true;
            } else {
                $this->repRefusee->checked = true;
            }

            //Accès à la consultation pour les entreprises
            if ('0' == $consultation->getPoursuivreAffichage()) {
                $this->accesConsultationEntrepriseDateLimite->Checked = true;
            } else {
                $this->accesConsultationEntreprisePoursuivre->Checked = true;
                $this->poursuivreAffichageNbJour->SelectedValue = $consultation->getPoursuivreAffichage().$consultation->getPoursuivreAffichageUnite();
            }

            //Signature electronique
            $this->signatureElectroniqueNon->Checked = false;
            $this->signatureElectroniqueOui->Checked = false;
            $this->signatureElectroniqueAutoriser->Checked = false;
            if ('1' == $consultation->getSignatureOffre()) {
                $this->signatureElectroniqueOui->Checked = true;
            } elseif ('0' == $consultation->getSignatureOffre()) {
                $this->signatureElectroniqueNon->Checked = true;
            } elseif ('2' == $consultation->getSignatureOffre()) {
                $this->signatureElectroniqueAutoriser->Checked = true;
            }

            //Chiffrement de la réponse
            //if($consultation->getEnvOffre() != '0') {
            if ('1' == $consultation->getChiffrementOffre()) {
                $this->chiffrementOui->checked = true;
            } else {
                $this->chiffrementNon->checked = true;
            }
            //}

            //Signature acte d'engagement
            if ('1' == $consultation->getSignatureActeEngagement()) {
                $this->signaturePropre->Checked = true;
            } else {
                $this->signaturePropre->Checked = false;
            }

            //annexe financière
            if ('1' == $consultation->getAnnexeFinanciere()) {
                $this->annexeFinanciere->Checked = true;
            } else {
                $this->annexeFinanciere->Checked = false;
            }

            if ('1' == $consultation->getModeOuvertureReponse()) {
                $this->modeOuvertureParReponse->Checked = true;
                $this->modeOuvertureParDossier->Checked = false;
            } else {
                $this->modeOuvertureParReponse->Checked = false;
                $this->modeOuvertureParDossier->Checked = true;
            }

            if (!$consultation->getAutoriserReponseElectronique() && !$consultation->getReponseObligatoire()) {
                $this->panelblocReponse->setStyle('display:none');
                $this->panelModeChiffrement->setStyle('display:none');
            }
        }
        if (Atexo_Module::isEnabled('interfaceModuleSub')) {
            $this->modeOuvertureParDossier->Enabled = false;
            $this->modeOuvertureParReponse->Checked = true;
        }
    }

    /**
     * Permet d'enregistrer le bloc "Constitution des dossiers de réponses".
     *
     * @param CommonConsultation $consultation
     */
    public function saveConstitutionReponses(&$consultation, $modification = true)
    {
        //Candidature
        $envelopCandidatureOld = $consultation->getEnvCandidature();
        if ($this->envelopCandidature->Checked) {
            $consultation->setEnvCandidature(1);
        /*if(Atexo_Module::isEnabled('DossierAdditif')){
            if ($this->dossierAdditif->Checked) {
                $consultation->setDossierAdditif(1);
            } else {
                $consultation->setDossierAdditif(0);
            }
        }*/
        } else {
            $consultation->setEnvCandidature(0);
        }
        //Anonymat
        $envAnonymatOld = $consultation->getEnvAnonymat();
        if ($this->anonymat->Checked) {
            $consultation->setEnvAnonymat(1);
        } else {
            $consultation->setEnvAnonymat(0);
        }
        //Offres
        $envelopOffreOld = $consultation->getEnvOffre();
        $lots = $consultation->getAllLots();
        $nbrLotsRepeater = '1';
        if (is_array($lots) && count($lots)) {
            $nbrLotsRepeater = count($lots);
        }
        if ($this->envelopOffre->Checked) {
            /*if ($this->envelopUnique->Checked) {
                $consultation->setEnvOffre(1);
            }
            elseif ($this->envelopParLot->Checked) {
                $consultation->setEnvOffre($nbrLotsRepeater);
            }*/
            $consultation->setEnvOffre($nbrLotsRepeater);
        } else {
            $consultation->setEnvOffre(0);
        }
        //Offres techniques
        $envelopOffreTechniqueOld = $consultation->getEnvOffreTechnique();
        $consultation->setEnvOffreTechnique(0);

        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        //Début : Historique de la Constitution des dossiers de réponses
        $this->saveHistoriqueConstitutionDossierReponse(
            $envelopOffreOld,
            $envelopOffreTechniqueOld,
            $envelopCandidatureOld,
            $envAnonymatOld,
            $consultation,
            $connexion,
            Atexo_CurrentUser::getCurrentOrganism(),
            $modification
        );
        //Fin : Historique de la Constitution des dossiers de réponses
    }

    /**
     * Permet d'enregistrer le bloc "Modalités des réponses".
     *
     * @param CommonConsultation $consultation
     */
    public function saveModalitesReponses(&$consultation, $modification = true)
    {
        //Réponse électronique
        $reponseElectroniqueOld = $consultation->getAutoriserReponseElectronique();
        if ($this->repAutorisee->Checked) {
            $consultation->setAutoriserReponseElectronique('1');
        } elseif ($this->repRefusee->Checked) {
            $consultation->setAutoriserReponseElectronique('0');
        }
        $reponseObligatoireOld = $consultation->getReponseObligatoire();
        if ($this->obligatoireOui->Checked) {
            $consultation->setReponseObligatoire('1');
            $consultation->setAutoriserReponseElectronique('1');
        } else {
            $consultation->setReponseObligatoire('0');
        }

        //Accès à la consultation pour les entreprises
        if ($this->accesConsultationEntreprisePoursuivre->Checked) {
            $consultation->setPoursuivreAffichage((int) $this->poursuivreAffichageNbJour->getSelectedValue());
            $consultation->setPoursuivreAffichageUnite(preg_replace('#[0-9]#', '', $this->poursuivreAffichageNbJour->getSelectedValue()));
        } elseif ($this->accesConsultationEntrepriseDateLimite->Checked) {
            $consultation->setPoursuivreAffichage('0');
        }

        //Signature electronique
        $signatureElectroniqueOld = $consultation->getSignatureOffre();
        $signatureElectronique = '';
        if ($this->signatureElectroniqueOui->Checked) {
            $signatureElectronique = 1;
        } elseif ($this->signatureElectroniqueNon->Checked) {
            $signatureElectronique = 0;
        } elseif ($this->signatureElectroniqueAutoriser->Checked) {
            $signatureElectronique = 2;
        }
        $consultation->setSignatureOffre($signatureElectronique);

        //Chiffrement de la réponse
        $chiffrementOld = $consultation->getChiffrementOffre();
        if ($this->chiffrementOui->Checked) {
            $consultation->setChiffrementOffre('1');
        } elseif ($this->chiffrementNon->Checked) {
            $consultation->setChiffrementOffre('0');
        }

        //Acte d'engagement
        if ($this->signaturePropre->Checked) {
            $consultation->setSignatureActeEngagement(1);
        } else {
            $consultation->setSignatureActeEngagement(0);
        }

        //Annexe Financiere
        if ($this->annexeFinanciere->Checked) {
            $consultation->setAnnexeFinanciere(1);
        } else {
            $consultation->setAnnexeFinanciere(0);
        }
        //mode d'ouverture
        if ($this->modeOuvertureParReponse->Checked) {
            $consultation->setModeOuvertureReponse('1');
        } else {
            $consultation->setModeOuvertureReponse('0');
        }
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        //Début : Historique des réponses électroniques
        $this->saveHistoriqueReponseElectronique($reponseElectroniqueOld, $consultation, $connexion, Atexo_CurrentUser::getCurrentOrganism(), $modification);
        //Fin : Historique des réponses électroniques

        //Début : Historique des réponses obligatoires
        $this->saveHistoriqueReponseElectroniqueObligatoire($reponseObligatoireOld, $consultation, $connexion, Atexo_CurrentUser::getCurrentOrganism(), $modification);
        //Fin : Historique des réponses obligatoires

        //Début : Historique des Caractéristiques des réponses électroniques
        $this->saveHistoriqueCaracteristiquesReponseElectronique($signatureElectroniqueOld, $chiffrementOld, $consultation, $connexion, Atexo_CurrentUser::getCurrentOrganism(), $modification);
        //Fin : Historique des Caractéristiques des réponses électroniques
    }

    /**
     * Permet d'afficher/masquer des blocs en fonction des valeurs parametrées dans les procédures équivalences.
     *
     * @param CommonConsultation $consultation
     */
    public function afficherMasquerBlocsEnFonctionParametrageProcedures($consultation = false, $procEquivalence = false)
    {
        if (!$procEquivalence) {
            $procEquivalence = Atexo_Consultation_ProcedureEquivalence::retrieveProcedureEquivalenceById($consultation->getIdTypeProcedureOrg());
            $fromProcedure = false;
        } else {
            $fromProcedure = true;
        }
        if ($procEquivalence instanceof CommonProcedureEquivalence) {
            if ('-0' == $procEquivalence->getEnvOffreTechnique() || '-1' == $procEquivalence->getEnvOffreTechnique()) {
                $this->spacerenvelopOffreTechnique->setStyle('display:none');
            } else {
                $this->spacerenvelopOffreTechnique->setStyle('display:block');
            }
            if ('-0' == $procEquivalence->getEnvOffre() || '-1' == $procEquivalence->getEnvOffre()) {
                $this->spacerenvelopOffre->setStyle('display:none');
            } else {
                $this->spacerenvelopOffre->setStyle('display:block');
            }
            if ('-0' == $procEquivalence->getEnvAnonymat() || '-1' == $procEquivalence->getEnvAnonymat()) {
                $this->spacerenvelopAnonymat->setStyle('display:none');
            } else {
                $this->spacerenvelopAnonymat->setStyle('display:block');
            }
            //modalite agents
            $this->panelModaliteAgents->setStyle('NULL');

            $this->panelLigneChiffrement->setStyle('NULL');
            $this->displayElement('chiffrementOui', $procEquivalence->getCipherEnabled(), true, $fromProcedure, 'panelLigneChiffrement', 'panelModaliteAgents');
            $this->displayElement('chiffrementNon', $procEquivalence->getCipherDisabled(), true, $fromProcedure, 'panelLigneChiffrement', 'panelModaliteAgents');

            $this->panelModeOuverture->setStyle('NULL');
            $this->displayElement('modeOuvertureParDossier', $procEquivalence->getModeOuvertureDossier(), true, $fromProcedure, 'panelModeOuverture', 'panelModaliteAgents');
            $this->displayElement('modeOuvertureParReponse', $procEquivalence->getModeOuvertureReponse(), true, $fromProcedure, 'panelModeOuverture', 'panelModaliteAgents');

            $this->panelDossiersReponse->setStyle('NULL');
            $this->displayElement('envelopCandidature', $procEquivalence->getEnvCandidature(), true, $fromProcedure, false, 'panelDossiersReponse');
            $this->displayElement('envelopOffre', $procEquivalence->getEnvOffre(), true, $fromProcedure, false, 'panelDossiersReponse');
            if (!Atexo_Module::isEnabled('EnveloppeAnonymat')) {
                $this->panelanonymat->setStyle('display:none');
                $this->spacerenvelopAnonymat->setStyle('display:none');
            } else {
                $this->displayElement('anonymat', $procEquivalence->getEnvAnonymat(), true, $fromProcedure, false, 'panelDossiersReponse');
            }
            if (!Atexo_Module::isEnabled('EnveloppeOffreTechnique')) {
                $this->panelenvelopOffreTechnique->setStyle('display:none');
                $this->spacerenvelopOffreTechnique->setStyle('display:none');
            } else {
                $this->displayElement('envelopOffreTechnique', $procEquivalence->getEnvOffreTechnique(), true, $fromProcedure, false, 'panelDossiersReponse');
            }

            //Signature electronique
            $this->PanelModaliteReponse->setStyle('NULL');

            $this->PanelLigneSinatureElectronique->setStyle('NULL');
            $this->displayElement('signatureElectroniqueOui', $procEquivalence->getSignatureEnabled(), true, $fromProcedure, 'PanelLigneSinatureElectronique', 'PanelModaliteReponse');
            $this->displayElement('signatureElectroniqueNon', $procEquivalence->getSignatureDisabled(), true, $fromProcedure, 'PanelLigneSinatureElectronique', 'PanelModaliteReponse');
            $this->displayElement(
                'signatureElectroniqueAutoriser',
                $procEquivalence->getSignatureAutoriser(),
                true,
                $fromProcedure,
                'PanelLigneSinatureElectronique',
                'PanelModaliteReponse'
            );

            $this->panelLigneReponseElec->setStyle('NULL');
            $this->displayElement('repAutorisee', $procEquivalence->getElecResp(), true, $fromProcedure, 'panelLigneReponseElec', 'PanelModaliteReponse');
            $this->displayElement('repRefusee', $procEquivalence->getNoElecResp(), true, $fromProcedure, 'panelLigneReponseElec', 'PanelModaliteReponse');
            $this->displayElement('obligatoireOui', $procEquivalence->getRepObligatoire(), true, $fromProcedure, 'panelLigneReponseElec', 'PanelModaliteReponse');

            $this->panelpoursuivreAffichage->setStyle('NULL');
            $this->displayElement('accesConsultationEntreprisePoursuivre', $procEquivalence->getDelaiDateLimiteRemisePli(), true, $fromProcedure, 'panelpoursuivreAffichage', 'PanelModaliteReponse');
            $this->displayElement('signaturePropre', $procEquivalence->getSignaturePropre(), true, $fromProcedure);
            $this->displayElement('annexeFinanciere', $procEquivalence->getAnnexeFinanciere(), true, $fromProcedure);
            $this->displayElement('accesConsultationEntrepriseDateLimite', $procEquivalence->getPoursuiteDateLimiteRemisePli(), true, $fromProcedure, 'panelpoursuivreAffichage', 'PanelModaliteReponse');
            if ($procEquivalence->getDelaiPoursuiteAffichage() && $fromProcedure) {
                $this->poursuivreAffichageNbJour->SelectedValue = $procEquivalence->getDelaiPoursuiteAffichage().$procEquivalence->getDelaiPoursuivreAffichageUnite();
            }
        }
    }

    /*
     *  Permet de redissiner le panel du composant
     */
    public function refreshPanel($param)
    {
        $this->bloc_etapeModalitesReponse->render($param->getNewWriter());
    }

    /**
     * Permet de gerer la visibilité du bloc de l'acte d'engagement.
     */
    public function isActeEngagementVisible()
    {
        $procEquivalence = Atexo_Consultation_ProcedureEquivalence::retrieveProcedureEquivalenceById($this->Page->getConsultation()->getIdTypeProcedureOrg());
        if ($procEquivalence instanceof CommonProcedureEquivalence) {
            if ('-1' === $procEquivalence->getSignaturePropre() || '-0' === $procEquivalence->getSignaturePropre()) {
                return 'display:none;';
            }

            return 'display:;';
        }

        return 'display:;';
    }
    /**
     * Permet de gerer la visibilité du bloc de l'annexe financiére.
     */
    public function isAnnexeFinanciereVisible()
    {
        $procEquivalence = Atexo_Consultation_ProcedureEquivalence::retrieveProcedureEquivalenceById($this->Page->getConsultation()->getIdTypeProcedureOrg());
        if ($procEquivalence instanceof CommonProcedureEquivalence) {
            if ('-1' === $procEquivalence->getAnnexeFinanciere() || '-0' === $procEquivalence->getAnnexeFinanciere()) {
                return 'display:none;';
            }

            return 'display:;';
        }

        return 'display:;';
    }

    /**
     * Permet de sauvegarder l'historique des types d'acces à la consultation coté opérateurs économiques.
     *
     * @param $reponseElectroniqueOld: ancienne valeur de la réponse electronique
     * @param $consultation: Objet consultation
     * @param $connexion: Objet connexion
     * @param $org: l'acronyme de l'organisme
     */
    public function saveHistoriqueReponseElectronique($reponseElectroniqueOld, $consultation, $connexion, $org, $modification = true)
    {
        $arrayParameters = [];
        if ($reponseElectroniqueOld != $consultation->getAutoriserReponseElectronique() || !$modification) {
            $nomElement = Atexo_Config::getParameter('ID_HISTORIQUE_REPONSE_ELECTRONIQUE');
            $valeur = '1';

            if ('0' == $consultation->getAutoriserReponseElectronique()) {
                $detail1 = '0';
            } elseif ('1' == $consultation->getAutoriserReponseElectronique()) {
                $detail1 = '1';
            }/*elseif($commonConsultationO->getAutoriserReponseElectronique() == '2'){

                $detail1 = "2" ;
            }*/

            $detail2 = '';
            $lot = '0';
            if ($modification) {
                $statut = Atexo_Config::getParameter('MODIFICATION_FILE');
            } else {
                $statut = Atexo_Config::getParameter('CREATION_FILE');
            }
            //Sauvegarde
            $arrayParameters[0] = $consultation->getId();
            $arrayParameters[1] = $nomElement;
            $arrayParameters[2] = $valeur;
            $arrayParameters[3] = $detail1;
            $arrayParameters[4] = $detail2;
            $arrayParameters[5] = $lot;
            $arrayParameters[6] = $org;
            $arrayParameters[7] = $statut;
            (new Atexo_Consultation())->saveHistoriqueConsultation($arrayParameters, $connexion);
        }
    }

    /**
     * Permet de sauvegarder l'historique des types d'acces à la consultation coté opérateurs économiques.
     *
     * @param $reponseElectroniqueOld: ancienne valeur de la signature electronique
     * @param $chiffrementOld : ancienne valeur du chiffrement
     * @param $consultation: Objet consultation
     * @param $connexion: Objet connexion
     * @param $org: l'acronyme de l'organisme
     */
    public function saveHistoriqueCaracteristiquesReponseElectronique($signatureElectroniqueOld, $chiffrementOld, $consultation, $connexion, $org, $modification = true)
    {
        $arrayParameters = [];
        if ((Atexo_Module::isEnabled('afficherTjrBlocCaracteristiqueReponse') || $consultation->getAutoriserReponseElectronique())
            && ($signatureElectroniqueOld != $consultation->getSignatureOffre()
                || $chiffrementOld != $consultation->getChiffrementOffre()
                || !$modification)
        ) {
            $nomElement = Atexo_Config::getParameter('ID_HISTORIQUE_CARACTERISTIQUES_REPONSES_ELECTRONIQUES');
            $valeur = '0';
            $detail1 = '';
            if ($consultation->getSignatureOffre()) {
                $detail1 = '1,';
                $valeur = '1';
                if ('2' === $consultation->getSignatureOffre()) {
                    $valeur = '2';
                }
            } else {
                $detail1 = '0,';
            }

            if ($consultation->getChiffrementOffre()) {
                $detail1 .= '1,';
                $valeur = '1';
            } else {
                $detail1 .= '0';
            }

            $detail2 = '';
            $lot = '0';
            if ($modification) {
                $statut = Atexo_Config::getParameter('MODIFICATION_FILE');
            } else {
                $statut = Atexo_Config::getParameter('CREATION_FILE');
            }
            //Sauvegarde
            $arrayParameters[0] = $consultation->getId();
            $arrayParameters[1] = $nomElement;
            $arrayParameters[2] = $valeur;
            $arrayParameters[3] = $detail1;
            $arrayParameters[4] = $detail2;
            $arrayParameters[5] = $lot;
            $arrayParameters[6] = $org;
            $arrayParameters[7] = $statut;
            (new Atexo_Consultation())->saveHistoriqueConsultation($arrayParameters, $connexion);
        }
    }

    /**
     * Permet de sauvegarder l'historique de constitution des dossiers de reponses.
     *
     * @param $envelopOffreOld: ancienne valeur de enveloppe offre
     * @param $envelopOffreTechniqueOld : ancienne valeur de enveloppe offres techniques
     * @param $envelopCandidatureOld: ancienne valeur de enveloppe candidature
     * @param $envAnonymatOld: ancienne valeur de enveloppe anonymat
     * @param $consultation: Objet consultation
     * @param $connexion: Objet connexion
     * @param $org: l'acronyme de l'organisme
     */
    public function saveHistoriqueConstitutionDossierReponse($envelopOffreOld, $envelopOffreTechniqueOld, $envelopCandidatureOld, $envAnonymatOld, $consultation, $connexion, $org, $modification = true)
    {
        $arrayParameters = [];
        if ((Atexo_Module::isEnabled('afficherTjrBlocCaracteristiqueReponse') || $consultation->getAutoriserReponseElectronique())
            && ($envelopOffreOld != $consultation->getEnvOffre()
            || $envelopOffreTechniqueOld != $consultation->getEnvOffreTechnique()
            || $envelopCandidatureOld != $consultation->getEnvCandidature()
            || $envAnonymatOld != $consultation->getEnvAnonymat()
            || !$modification)
        ) {
            $nomElement = Atexo_Config::getParameter('ID_HISTORIQUE_CONSTITUTION_DOSSIERS_REPONSES');
            $valeur = '0';
            $detail1 = '';
            if ($consultation->getEnvCandidature()) {
                $detail1 = '1,';
                $valeur = '1';
            } else {
                $detail1 = '0,';
            }

            if ($consultation->getEnvOffreTechnique()) {
                $detail1 .= '1,';
                $valeur = '1';
            } else {
                $detail1 .= '0,';
            }

            if ($consultation->getEnvOffre()) {
                $detail1 .= '1,';
                $valeur = '1';
            } else {
                $detail1 .= '0,';
            }

            if ($consultation->getEnvAnonymat()) {
                $detail1 .= '1,';
                $valeur = '1';
            } else {
                $detail1 .= '0,';
            }

            $detail2 = '';
            $lot = '0';
            if ($modification) {
                $statut = Atexo_Config::getParameter('MODIFICATION_FILE');
            } else {
                $statut = Atexo_Config::getParameter('CREATION_FILE');
            }
            //Sauvegarde
            $arrayParameters[0] = $consultation->getId();
            $arrayParameters[1] = $nomElement;
            $arrayParameters[2] = $valeur;
            $arrayParameters[3] = $detail1;
            $arrayParameters[4] = $detail2;
            $arrayParameters[5] = $lot;
            $arrayParameters[6] = $org;
            $arrayParameters[7] = $statut;
            (new Atexo_Consultation())->saveHistoriqueConsultation($arrayParameters, $connexion);
        }
    }

    /**
     * Permet de sauvegarder l'historique des réponses électroniques obligatoires.
     *
     * @param $reponseObligatoireOld: ancienne valeur de la réponse electronique obligatoire
     * @param $consultation: Objet consultation
     * @param $connexion: Objet connexion
     * @param $org: l'acronyme de l'organisme
     */
    public function saveHistoriqueReponseElectroniqueObligatoire($reponseObligatoireOld, $consultation, $connexion, $org, $modification = true)
    {
        $arrayParameters = [];
        if ($reponseObligatoireOld != $consultation->getReponseObligatoire() || !$modification) {
            $nomElement = Atexo_Config::getParameter('ID_HISTORIQUE_REPONSE_OBLIGATOIRE');
            $valeur = '1';

            if ('0' == $consultation->getReponseObligatoire()) {
                $detail1 = '0';
            } elseif ('1' == $consultation->getReponseObligatoire()) {
                $detail1 = '1';
            }
            $detail2 = '';
            $lot = '0';
            if ($modification) {
                $statut = Atexo_Config::getParameter('MODIFICATION_FILE');
            } else {
                $statut = Atexo_Config::getParameter('CREATION_FILE');
            }
            //Sauvegarde
            $arrayParameters[0] = $consultation->getId();
            $arrayParameters[1] = $nomElement;
            $arrayParameters[2] = $valeur;
            $arrayParameters[3] = $detail1;
            $arrayParameters[4] = $detail2;
            $arrayParameters[5] = $lot;
            $arrayParameters[6] = $org;
            $arrayParameters[7] = $statut;
            (new Atexo_Consultation())->saveHistoriqueConsultation($arrayParameters, $connexion);
        }
    }

    /**
     * Permet de préciser si la modification est permise.
     *
     * @param $consultation: objet consultation
     * Si Oui retourne true, sinon false
     */
    public function isModificationAllowed($consultation)
    {
        return (new Atexo_Consultation())->isModificationAllowed($consultation, isset($_GET['continuation']), isset($_GET['id']));
    }

    /**
     * Permet d'afficher/masquer des composants.
     *
     * @param $element: l'element à afficher/masquer
     * @param $values: valeur (+1= afficher+cocher ...)
     * @param $isChecked
     */
    public function displayElement($element, $values, $isChecked = true, $fromProcedure = false, $ligneAllElements = false, $panelGlobale = false)
    {
        $panel = 'panel'.$element;
        if (!strcmp($values, '0')) {
            $this->$panel->setStyle('display:');
            $this->$element->SetEnabled(false);
            if ($isChecked && $fromProcedure) {
                $this->$element->SetChecked(false);
            }
            if ($ligneAllElements && ('display:none' == $this->$ligneAllElements->getStyle()->getCustomStyle() || 'NULL' == $this->$ligneAllElements->getStyle()->getCustomStyle())) {
                $this->$ligneAllElements->setStyle('display:');
            }
            if ($panelGlobale && ('display:none' == $this->$panelGlobale->getStyle()->getCustomStyle() || 'NULL' == $this->$panelGlobale->getStyle()->getCustomStyle())) {
                $this->$panelGlobale->setStyle('display:');
            }
        } elseif (!strcmp($values, '1')) {
            $this->$panel->setStyle('display:');
            $this->$element->SetEnabled(false);
            if ($isChecked && $fromProcedure) {
                $this->$element->SetChecked(true);
            }
            if ($ligneAllElements && ('display:none' == $this->$ligneAllElements->getStyle()->getCustomStyle() || 'NULL' == $this->$ligneAllElements->getStyle()->getCustomStyle())) {
                $this->$ligneAllElements->setStyle('display:');
            }
            if ($panelGlobale && ('display:none' == $this->$panelGlobale->getStyle()->getCustomStyle() || 'NULL' == $this->$panelGlobale->getStyle()->getCustomStyle())) {
                $this->$panelGlobale->setStyle('display:');
            }
        } elseif (!strcmp($values, '+1')) {
            $this->$panel->setStyle('display:');
            if ($isChecked) {
                $this->$element->SetEnabled(true);
                if ($fromProcedure) {
                    $this->$element->SetChecked(true);
                }
            }
            if ($ligneAllElements && ('display:none' == $this->$ligneAllElements->getStyle()->getCustomStyle() || 'NULL' == $this->$ligneAllElements->getStyle()->getCustomStyle())) {
                $this->$ligneAllElements->setStyle('display:');
            }
            if ($panelGlobale && ('display:none' == $this->$panelGlobale->getStyle()->getCustomStyle() || 'NULL' == $this->$panelGlobale->getStyle()->getCustomStyle())) {
                $this->$panelGlobale->setStyle('display:');
            }
        } elseif (!strcmp($values, '+0')) {
            $this->$panel->setStyle('display:');
            if ($isChecked) {
                $this->$element->SetEnabled(true);
                if ($fromProcedure) {
                    $this->$element->SetChecked(false);
                }
            }
            if ($ligneAllElements && ('display:none' == $this->$ligneAllElements->getStyle()->getCustomStyle() || 'NULL' == $this->$ligneAllElements->getStyle()->getCustomStyle())) {
                $this->$ligneAllElements->setStyle('display:');
            }
            if ($panelGlobale && ('display:none' == $this->$panelGlobale->getStyle()->getCustomStyle() || 'NULL' == $this->$panelGlobale->getStyle()->getCustomStyle())) {
                $this->$panelGlobale->setStyle('display:');
            }
        } elseif (!strcmp($values, '-1')) {
            $this->$panel->setStyle('display:none');
            if ($isChecked && $fromProcedure) {
                $this->$element->SetChecked(true);
            }
            if ($ligneAllElements && ('NULL' == $this->$ligneAllElements->getStyle()->getCustomStyle())) {
                $this->$ligneAllElements->setStyle('display:none');
            }
            if ($panelGlobale && ('NULL' == $this->$panelGlobale->getStyle()->getCustomStyle())) {
                $this->$panelGlobale->setStyle('display:none');
            }
        } elseif (!strcmp($values, '-0')) {
            $this->$panel->setStyle('display:none');
            if ($isChecked && $fromProcedure) {
                $this->$element->SetChecked(false);
            }
            if ($ligneAllElements && ('NULL' == $this->$ligneAllElements->getStyle()->getCustomStyle())) {
                $this->$ligneAllElements->setStyle('display:none');
            }
            if ($panelGlobale && ('NULL' == $this->$panelGlobale->getStyle()->getCustomStyle())) {
                $this->$panelGlobale->setStyle('display:none');
            }
        }
    }
}
