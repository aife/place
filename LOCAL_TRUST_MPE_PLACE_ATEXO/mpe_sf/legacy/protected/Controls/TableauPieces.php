<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Dce;

/**
 * commentaires.
 *
 * @author Loubna Ezziani <loubna.ezziani@atexo.com>
 * @copyright Atexo 2012
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class TableauPieces extends MpeTTemplateControl
{
    public $_consultation = '';

    public function onLoad($param)
    {
    }

    public function displayPieces($dataSource = false)
    {
        if (false === $dataSource) {
            $dataSourceFiles = [];
            $dataSource = [];
            $dataSourceFiles = (new Atexo_Consultation_Dce())->getDataSourecFileDCE($this->_consultation->getId(), Atexo_CurrentUser::getCurrentOrganism());
            if (is_countable($dataSourceFiles) ? count($dataSourceFiles) : 0) {
                foreach ($dataSourceFiles as $cl => $v) {
                    $indexs = explode('-', $cl);
                    $dataSource[$indexs[0]]['index'] = $indexs[0];
                    $dataSource[$indexs[0]]['nom'] = $v;
                }
            }
        }

        if (0 == (is_countable($dataSource) ? count($dataSource) : 0)) {
            $this->panelListePiecesDCE->setDisplay('None');
            $this->panelNoElementFound->setDisplay('Dynamic');
        } else {
            $this->repeaterPiecesDCE->DataSource = $dataSource;
            $this->repeaterPiecesDCE->DataBind();
            $this->panelListePiecesDCE->setDisplay('Dynamic');
            $this->panelNoElementFound->setDisplay('None');
            if (1 == (is_countable($dataSource) ? count($dataSource) : 0)) {
                foreach ($this->repeaterPiecesDCE->getItems() as $item) {
                    $item->supprimerPiece->Visible = 'false';
                }
            }
        }
    }

    public function deletePieceDce($sender, $param)
    {
        $index = $param->CallbackParameter;
        $dataSource = [];
        foreach ($this->repeaterPiecesDCE->getItems() as $item) {
            if ($item->idDocReplace->Value != $index) {
                $dataSource[$item->idDocReplace->Value]['index'] = Atexo_Util::utf8ToIso($item->idDocReplace->Value);
                $dataSource[$item->idDocReplace->Value]['nom'] = Atexo_Util::utf8ToIso($item->nomFile->Text);
            } else {
                $listPiecesToDelete = $this->Page->getViewState('ListPiecesToDelete');
                $listPiecesToDelete .= '#'.$index;
                $this->filesToDelete->Value = $listPiecesToDelete;
                $this->Page->setViewState('ListPiecesToDelete', $listPiecesToDelete);
            }
        }
        $this->setViewState('ListPieces', $dataSource);
        self::displayPieces($dataSource);
        $this->panelListePiecesDCE->render($param->getNewWriter());
        $this->panelNoElementFound->render($param->getNewWriter());
    }
}
