<?php

namespace Application\Controls;

/**
 * le tableau de Prestations réalisées ou en cours de réalisation pour MPE Maroc.
 *
 * @author Mounir LAMANE <mounir.lamane@atexo.com>
 * @copyright Atexo 2009
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PrestationEntreprise extends MpeTTemplateControl
{
    public function onLoad($param)
    {
    }
}
