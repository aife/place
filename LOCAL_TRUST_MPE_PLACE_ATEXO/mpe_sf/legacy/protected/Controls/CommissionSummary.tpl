 <div class="form-bloc" id="recap-consultation">
    <div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
    <div class="content">
        <a class="title-toggle" onclick="togglePanel(this,'infosPrincipales');" href="javascript:void(0);">&nbsp;</a>
        <div class="recap-bloc">
            <div class="line">
                <div class="intitule-200"><strong><com:TTranslate>DEFINE_IDENTIFICATION_COMMISSION</com:TTranslate></strong></div>
            </div>
            <div class="line">
                <div class="intitule-80 bold"><com:TTranslate>DATE</com:TTranslate> : </div>
                <div class="content-bloc bloc-500"><com:TLabel id="dateHeure" /></div>
            </div>
            <div class="line">
                <div class="intitule-80 bold"><com:TTranslate>TEXT_TYPE</com:TTranslate> :</div>
                <div class="content-bloc bloc-500"><com:TLabel id="typeCommission" /></div>
            </div>
            <div id="infosPrincipales" style="display:none;" class="panel-toggle">
                <div class="line">
                    <div class="intitule-80 bold"><com:TTranslate>DEFINE_STATUS</com:TTranslate> : </div><com:TLabel id="statutCommission" />
                </div>
                <div class="line">
                    <div class="intitule-80 bold"><com:TTranslate>DEFINE_LIEU</com:TTranslate> : </div><com:TLabel id="lieuCommisssion" />  
                </div>
                <div class="line">
                    <div class="intitule-80 bold"><com:TTranslate>DEFINE_SALLE</com:TTranslate> : </div><com:TLabel id="salleCommisssion" />
                </div>
                <div class="breaker"></div>
            </div>
        </div>
        <div class="spacer-small"></div>
        <div class="breaker"></div>
    </div>
    <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
</div>
