	<!--Debut colonne gauche-->
	<div class="left-part" id="left-part">
	<!--Debut menu -->
		<div id="menu">
			<ul id="menuList">
				<li class="menu-open menu-on"><span onclick="toggleMenu('menuTeletransmission');"><com:TTranslate>TEXT_TRANSMISSION</com:TTranslate></span>
					<ul class="ss-menu-open" id="menuTeletransmission" style="display:block">
						<li class="off"><a href="?page=Agent.TableauDeBord&AS=1&searchAnnCons"><com:TTranslate>DEFINE_TEXT_CREER</com:TTranslate></a></li>
						<li class="off"><a href="?page=helios.RechercheTeletransmission"><com:TTranslate>DEFINE_TEXT_SUIVRE_ENCHERE</com:TTranslate></a></li>
						<li class="off"><label for="quickSearch" style="display:none;"><com:TTranslate>TEXT_RECHERCHE_RAPIDE</com:TTranslate></label>
						<com:TPanel DefaultButton="imageOko">
								<label for="quickSearch" style="display:none;"><com:TTranslate>TEXT_RECHERCHE_RAPIDE</com:TTranslate></label>
								<com:TTextBox ID="quickSearch"
										CssClass="rechercher"  
										Text="Recherche rapide" 
										Attributes.title="Recherche rapide" 
										Attributes.onfocus="clearOnFocus(event,this);" 
										Attributes.onmouseover="clearOnFocus(event,this);" />
								<com:TLinkButton ID="imageOko"  OnClick="goToTableauDeBord" CssClass="ok"  >
								 <com:TImage ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/bouton-ok.gif" Attributes.alt="OK" Attributes.title="OK" />
								</com:TLinkButton>
							</com:TPanel>
					</ul>
				</li>
			</ul>
		</div>
		<div class="menu-bottom"></div>
		<!--Fin menu -->
	</div>