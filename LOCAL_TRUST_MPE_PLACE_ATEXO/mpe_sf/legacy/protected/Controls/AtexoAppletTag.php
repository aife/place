<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_Config;

class AtexoAppletTag extends MpeTTemplateControl
{
    public $_mainClass;

    public function getMainClass()
    {
        return $this->_mainClass;
    }

    public function setMainClass($mainClass)
    {
        $this->_mainClass = $mainClass;
    }

    /*
     *  Permet  activer / désactiver l'utilisation du paramettre java_arguments.
    *
    *  @return String param java_arguments
    *  @author : loubna.ezziani@atexom.com
    *  @version : since 4.5.0
    */
    public function getParamJavaArguments()
    {
        if (Atexo_Config::getParameter('USE_APPLET_MEMORY_PARAM')) {
            return "<param name='java_arguments' value='-Xmx256m'/> \n ";
        }

        return '';
    }
}
