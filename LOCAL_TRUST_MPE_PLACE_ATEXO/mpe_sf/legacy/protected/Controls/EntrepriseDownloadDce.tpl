<div class="form-field">
    <com:TPanel id="msgAlerteAnnexeTechnique" cssclass="alert alert-warning">
        <i class="fa fa-exclamation-triangle"></i>
        <com:TTranslate>MESSAGE_ALERTE_TELECHARGEMENT_DCE_TECHNIQUE</com:TTranslate>
    </com:TPanel>
    <!--DEBUT TELECHARGEMENT COMPLET-->
    <com:TPanel id="panelTelechargementComplet">
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="h4 panel-title">
                <com:TTranslate>TEXT_TELECHARGEMENT_COMPLET</com:TTranslate>
            </div>
        </div>
        <div class="panel-body">
            <com:TTranslate>TEXT_CLIQUEZ_LIEN_TELECHARGEMENT</com:TTranslate>:
            <com:TLabel id="nomDce"/>

            <com:TLinkButton
                    CssClass="btn btn-sm btn-primary pull-right"
                    id="completeDownload"
                    onClick="downloadCompleteDce"
                    Attributes.onclick="document.getElementById('ctl0_CONTENU_PAGE_EntrepriseDownloadDce_completeDownload').style.display='none';document.getElementById('ctl0_CONTENU_PAGE_EntrepriseDownloadDce_lienNouveauTelechargement').style.display='';">
                <i class="fa fa-download m-r-1"></i>
                <com:TTranslate>TELECHARGER_DCE</com:TTranslate>
            </com:TLinkButton>
        </div>
    </div>
    </com:TPanel>
    <!--FIN TELECHARGEMENT COMPLET-->

    <!--DEBUT TELECHARGEMENT PARTIEL-->
    <com:TPanel id="panelTelechargementPartiel" CssClass="panel panel-default">
        <div class="panel-heading">
            <div class="h4 panel-title">
                <com:TTranslate>TEXT_TELECHARGEMENT_PARTIEL</com:TTranslate>
            </div>
        </div>
        <div class="panel-body">
            <com:TTranslate>TEXT_CHOISIR_PIECES_DOSSIER_CONSULTATION</com:TTranslate>
            <div class="arbo-dce"><%=$this->getArborescence();%></div>
            <com:TLinkButton
                    ID="telechargementPartiel"
                    CssClass="btn btn-sm btn-primary pull-right"
                    onClick="downloadSelectedFilesForDce"
                    ValidationGroup="ValidateInfoUser"
                    Attributes.onclick="document.getElementById('ctl0_CONTENU_PAGE_EntrepriseDownloadDce_telechargementPartiel').style.display='none';document.getElementById('ctl0_CONTENU_PAGE_EntrepriseDownloadDce_lienNouveauTelechargement').style.display='';"
            >
                <i class="fa fa-download m-r-1"></i>
                <com:TTranslate>TEXT_BOUTON_TELECHARGER_PIECE</com:TTranslate>
            </com:TLinkButton>
            <com:TCustomValidator
                    ValidationGroup="ValidateInfoUser"
                    ControlToValidate="voidControl"
                    ClientValidationFunction="DownloadPartialDCE"
                    ErrorMessage="<%=Prado::localize('MESSAGE_ERREUR_SELECTIONNER_PIECES')%>"
                    Display="Dynamic"
                    Text=" "
            >
                <prop:ClientSide.OnValidationError>
                    document.getElementById('divValidationSummary').style.display='';
                    document.getElementById('ctl0_CONTENU_PAGE_EntrepriseDownloadDce_telechargementPartiel').style.display='';
                    document.getElementById('ctl0_CONTENU_PAGE_EntrepriseDownloadDce_lienNouveauTelechargement').style.display='none';
                </prop:ClientSide.OnValidationError>
            </com:TCustomValidator>
            <span style="display:none">
         		            <com:TTextBox id="voidControl"/>
         		        </span>
            <com:THiddenField id="maxIndexPieces"/>
        </div>
    </com:TPanel>
    <!--FIN TELECHARGEMENT PARTIEL-->

    <com:TPanel id="panelEnvoiPostauComplementaires" CssClass="form-field">
        <div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
        <div class="content">
            <h3>
                <com:TTranslate>TEXT_GESTION_ENVOIES_POSTAUX</com:TTranslate>
            </h3>
            <p>
                <com:TTranslate>TEXT_CONSULTATION_DOCS_AUTRES_FORMATS</com:TTranslate>
                :
            </p>
            <p class="indent-10">
                -
                <com:TTranslate>TEXT_PAPIER</com:TTranslate>
                <br/>
                -
                <com:TTranslate>TEXT_CD_ROM</com:TTranslate>
            </p>
            <p>
                <com:TTranslate>TEXT_DOCS_DECRITS_APRES</com:TTranslate>
                :
            </p>
            <p class="indent-10">
                -
                <com:TLabel id="tirageDescriptif"/>
                <br/>
            </p>
            <div class="spacer-small"></div>
            <!--Debut Bloc choix Format-->
            <div class="line">
                <div class="intitule-bloc intitule-200">
                    <com:TTranslate>TEXT_SOUHAITE_COMPLEMENTS</com:TTranslate>
                    :
                </div>
                <div class="content-bloc-auto bloc-550 float-left">
                    <div class="intitule-auto">
                        <com:TPanel id="papierPanel">
                            <com:TCheckBox id="papier" CssClass="check" Attributes.title="<%=Prado::localize('TEXT_PAPIER')%>"/>
                            <label for="papier">
                                <com:TTranslate>TEXT_PAPIER</com:TTranslate>
                            </label></com:TPanel>
                    </div>
                    <div class="breaker"></div>
                    <div class="intitule-auto">
                        <com:TPanel id="cdRomPanel">
                            <com:TCheckBox id="cdRom" CssClass="check" Attributes.title="<%=Prado::localize('TEXT_CD_ROM')%>"/>
                            <label for="cdRom">
                                <com:TTranslate>TEXT_CD_ROM</com:TTranslate>
                            </label></com:TPanel>
                    </div>
                    <div class="breaker"></div>
                </div>
            </div>
            <div class="spacer-small"></div>
            <!--Fin Bloc choix Format-->
            <!--Debut Bloc choix Format-->
            <div class="line">
                <div class="intitule-bloc intitule-200"><label for="precisionsElements">
                    <com:TTranslate>TEXT_ELEMENTS_SOUHAITE_RECEVOIR</com:TTranslate>
                </label> :
                </div>
                <com:TTextBox id="precisionsElements" TextMode="MultiLine" Attributes.title="<%=Prado::localize('TEXT_ELEMENTS_SOUHAITE_RECEVOIR')%>" CssClass="bloc-550"></com:TTextBox>
            </div>
            <!--Fin Bloc choix Format-->
            <div class="boutons-line">
                <com:TLinkButton CssClass="bouton-arrow-100 float-right" Attributes.title="<%=Prado::localize('TEXT_DEMANDER_COMPLEMENTS')%>" onClick="demander">
                    <com:TTranslate>TEXT_DEMANDER</com:TTranslate>
                </com:TLinkButton>
            </div>
            <div class="breaker"></div>
        </div>
        <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
    </com:TPanel>

    <!--DEBUT TELECHARGEMENT _ANNEXES TECHNIQUES-->
    <com:TPanel id="panelCompleteDownloadAnnexes" CssClass="form-field">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="h4 panel-title">
                    <com:TTranslate>TEXT_TELECHARGEMENT_ANNEXES_TECHNIQUES</com:TTranslate>
                </div>
            </div>
            <div class="panel-body">
                <com:TTranslate>TEXT_CLIQUEZ_LIEN_TELECHARGEMENT_ANNEXES_TECHNIQUES</com:TTranslate>
                :
                <com:TLabel id="nomAnnexes"/>

                <button type="button" class="btn btn-sm btn-primary pull-right" data-toggle="modal" data-target="#exampleModal">
                    <i class="fa fa-download m-r-1"></i>
                    <com:TTranslate>TELECHARGER_ANNEXES_TECHNIQUES</com:TTranslate>
                </button>

                <!-- Modal -->
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <h2 class="h4 modal-title" id="exampleModalLabel"><com:TTranslate>TELECHARGER_ANNEXES_TECHNIQUES_DCE</com:TTranslate></h2>
                            </div>
                            <div class="modal-body clearfix">
                                <com:TTranslate>INFO_TELECHARGER_ANNEXES_TECHNIQUES_DCE</com:TTranslate>
                                <div class="text-center clearfix">
                                    <com:TTranslate>TEXT_TELECHARGEMENT_FICHIER_ATEXO</com:TTranslate>
                                     <com:TActiveLinkButton
                                            CssClass="btn m-l-1 btn-primary"
                                            id="completeDownloadAnnexes"
                                            OnCallBack="saveTraceInscrit"
                                            Attributes.onclick="document.getElementById('DLDVMAMP').click();document.getElementById('ctl0_CONTENU_PAGE_EntrepriseDownloadDce_completeDownloadAnnexes').style.display='none';document.getElementById('ctl0_CONTENU_PAGE_EntrepriseDownloadDce_lienNouveauTelechargement').style.display='';">
                                         <i class="fa fa-download"></i>
                                    </com:TActiveLinkButton>

                                    <button type="button"
                                            class="btn m-l-1 btn-primary object-example-container hide"
                                            id="DLDVMAMP"
                                            data-toggle="modal"
                                            data-body-mamp-objects='<%= $this->getJsonDossierVolumineux(); %>'
                                    ></button>


                                </div>
                            </div>
                            <div class="modal-footer clearfix">
                                <button type="button" class="btn btn-sm btn-default pull-left" data-dismiss="modal"><com:TTranslate>TEXT_RETOUR</com:TTranslate></button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </com:TPanel>
    <!--FIN TELECHARGEMENT _ANNEXES TECHNIQUES-->


    <div class="clearfix">
        <a style="display:<%=($this->getBaseDce()) ? none:'' %>;"
           href="index.php?page=Entreprise.EntrepriseDetailConsultation&id=<%= Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($_GET['id'])%>&orgAcronyme=<%= Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme'])%>"
           class="btn btn-sm btn-default pull-left" title="<%=Prado::localize('TEXT_RETOURNER_FICHE_DETAIL')%>">
            <com:TTranslate>TEXT_RETOUR</com:TTranslate>
        </a>
        <com:THyperlink ID="lienNouveauTelechargement" NavigateUrl="<%=$this->getUrlNewDownload()%>" Cssclass="btn btn-sm btn-primary pull-right" Attributes.title="<%=Prado::localize('TEXT_NOUVEAU_TELECHARGEMENT')%>">
            <i class="fa fa-download m-r-1"></i>
            <com:TTranslate>TEXT_NOUVEAU_TELECHARGEMENT</com:TTranslate>
        </com:THyperlink>
    </div>
</div>
<!--Fin colonne droite-->
<script Text="javascript"> document.getElementById('ctl0_CONTENU_PAGE_EntrepriseDownloadDce_lienNouveauTelechargement').style.display = 'none'; </script>
<com:TLabel id="script"/>