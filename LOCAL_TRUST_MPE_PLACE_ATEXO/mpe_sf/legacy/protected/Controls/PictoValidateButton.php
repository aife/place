<?php

namespace Application\Controls;

use Prado\Prado;
use Prado\Web\UI\WebControls\TButton;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PictoValidateButton extends TButton
{
    public function onLoad($param)
    {
        //$this->setImageUrl(t()."/images/bouton-ok.gif");
        $this->setCssClass('btn btn-primary btn-sm pull-right bouton-moyen float-right');
        $this->Text = 'Valider';
        $this->Attributes->value = Prado::localize('ICONE_VALIDER');
        //$this->setAlternateText(Prado::localize("ICONE_VALIDER"));
        $this->setToolTip(Prado::localize('ICONE_VALIDER'));
    }
}
