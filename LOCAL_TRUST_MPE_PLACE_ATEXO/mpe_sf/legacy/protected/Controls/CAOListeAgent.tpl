<com:TPanel ID="panelNoElementFound">
	<h2><com:TTranslate>DEFINE_AUCUN_AGENT_SELECTIONNE</com:TTranslate>&nbsp;</h2>
</com:TPanel>
<com:TPanel ID="panelMoreThanOneElementFound">
<div class="line-partitioner">
	<h2><com:TTranslate>DEFINE_NOMBRE_RESULTAT</com:TTranslate>&nbsp;<com:TLabel ID="nombreElement"/></h2>

	<div class="partitioner">
		<div class="intitule"><strong><label for="allCons_nbResultsTop"><com:TTranslate>DEFINE_TEXT_AFFICHER</com:TTranslate></label></strong></div>
		<com:TDropDownList ID="nombreResultatAfficherTop" AutoPostBack="true" onSelectedIndexChanged="changePagerLenght" Attributes.title='<%=Prado::localize("NOMBRE_DE_RESULTAT_PAR_PAGE")%>'>
  			<com:TListItem Text="10" Selected="10"/>
  			<com:TListItem Text="20" Value="20"/>
  			<com:TListItem Text="50" Value="50" />
  			<com:TListItem Text="100" Value="100" />
  			<com:TListItem Text="500" Value="500" />
  		</com:TDropDownList>
		<div class="intitule"><com:TTranslate>DEFINE_RESULTATS_PAGE</com:TTranslate></div>
			<label style="display:none;" for="allCons_pageNumberTop">Aller &agrave; la page</label>
			<com:TPanel ID="panelBouttonGotoPageTop" DefaultButton="DefaultButtonTop" CssClass="float-left">
				<com:TTextBox ID="numPageTop" Attributes.title='<%=Prado::localize("N_DE_LA_PAGE")%>' Text="1"/>
				<div class="nb-total ">/ <com:TLabel ID="nombrePageTop"/></div>
				<com:TButton ID="DefaultButtonTop"  OnClick="goToPage" Attributes.style="display:none"/>
			</com:TPanel>
		<div class="liens">
			<com:TPager ID="PagerTop"
           			ControlToPaginate="tableauAgent"
	           		FirstPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-premier.gif' alt='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>' title='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>' />"
		            LastPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-dernier.gif' alt='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>' title='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>' />"
	     	        Mode="NextPrev"
	          		NextPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-suivant.gif' alt='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>' title='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>' />"
		            PrevPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-precedent.gif' alt='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>' title='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>' />"
   	                OnPageIndexChanged="pageChanged"/>
		</div>
	</div>
</div>
<!--Debut Tableau agent-->
  <table class="table-results" summary="Liste des agents">
      <caption><com:TTranslate>DEFINE_LISTE_AGENTS</com:TTranslate></caption>

		<com:TRepeater ID="tableauAgent"
		    AllowPaging="true"
		    PageSize="10"
		    EnableViewState="true"
		    AllowCustomPaging="true"
		    OnItemCommand="actionsRepeater">

			<prop:HeaderTemplate>
		    <thead>
		          <tr>
		              <th colspan="4" class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></th>
		          </tr>
		          <tr>
		          	  <com:TPanel visible="<%#$this->TemplateControl->getCheckbox()%>"><th id="checkAgent" class="check-col"></th></com:TPanel>
		              <th class="col-250"  >
		              	<com:TActiveLinkButton onCommand="Page.critereTriAgent" onCallBack="Page.TrierAgent" CommandName="nom">
		              		<com:TTranslate>DEFINE_NOM</com:TTranslate>
		              		<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/arrow-tri.gif" alt="<%=Prado::localize('ICONE_TRIER')%>" title="<%=Prado::localize('ICONE_TRIER')%>" />
						</com:TActiveLinkButton>&nbsp;
		              	<com:TActiveLinkButton onCommand="Page.critereTriAgent" onCallBack="Page.TrierAgent" CommandName="prenom">
		              		<com:TTranslate>DEFINE_PRENOM</com:TTranslate>
		              		<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/arrow-tri.gif" alt="<%=Prado::localize('ICONE_TRIER')%>" title="<%=Prado::localize('ICONE_TRIER')%>" />
						</com:TActiveLinkButton>
						<br/>
						<com:TActiveLinkButton onCommand="Page.critereTriAgent" onCallBack="Page.TrierAgent" CommandName="email">
		              		<com:TTranslate>TEXT_EMAIL</com:TTranslate>
		              		<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/arrow-tri.gif" alt="<%=Prado::localize('ICONE_TRIER')%>" title="<%=Prado::localize('ICONE_TRIER')%>" />
						</com:TActiveLinkButton>
		              </th>
		              <th class="col-250">
		              	<com:TActiveLinkButton onCommand="Page.critereTriAgent" onCallBack="Page.TrierAgent" CommandName="service">
		              		<com:TTranslate>DEFINE_ENTITE_ACHAT</com:TTranslate>
		              		<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/arrow-tri.gif" alt="<%=Prado::localize('ICONE_TRIER')%>" title="<%=Prado::localize('ICONE_TRIER')%>" />
		              	</com:TActiveLinkButton>
		              	<com:TPanel visible="<%#(!$this->TemplateControl->getPictoTypeCompte())%>">
		        			<br/>
		        			<com:TActiveLinkButton onCommand="Page.critereTriAgent" onCallBack="Page.TrierAgent" CommandName="typeCompte">
			        			<com:TTranslate>TEXT_TYPE_COMPTE</com:TTranslate>
			        			<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/arrow-tri.gif" alt="<%=Prado::localize('ICONE_TRIER')%>" title="<%=Prado::localize('ICONE_TRIER')%>" />
		        			</com:TActiveLinkButton>
		        		</com:TPanel>
					  </th>
		              <th id="typeVoixAgent" class="col-200"  >
		              	<com:TTranslate>DEFINE_TYPE_DE_VOIX</com:TTranslate>
		              </th>
		              <com:TPanel visible="<%#$this->TemplateControl->getActions()%>">
		              	<th id="actionAgent" class="actions">
		              		<com:TTranslate>TEXT_ACTION</com:TTranslate>
		              	</th>
		              </com:TPanel>
		          </tr>
		    </thead>
		    </prop:HeaderTemplate>
	      <tbody>
	      <prop:ItemTemplate>
	          <tr class="<%#(($this->ItemIndex%2==0)? '':'on')%>">
				<com:TPanel visible="<%#$this->TemplateControl->getCheckbox()%>">
				<td headers="checkAgent" class="checkbox">
					<com:TCheckBox id="select"/>
					<com:THiddenField id="selectedIdAgent" value="<%#$this->Data->getId()%>"/>
				</td>
				</com:TPanel>
	         	<td class="col-250">
	         		<com:TPanel visible="<%#($this->TemplateControl->getPictoTypeCompte() == 'true')%>" cssclass="inline">
	         			<img alt="<%#$this->TemplateControl->getTypeCompteAgent($this->Data->getElu())%>" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/<%#$this->TemplateControl->getTypeCompteAgent($this->Data->getElu(),true)%>" />
	         		</com:TPanel>
		          	<%#$this->Data->getNom()%>&nbsp;
		          	<%#$this->Data->getPrenom()%><br/>
		          	<%#$this->Data->getEmail()%>
	         	</td>
		        <td class="col-250">
		        	<%#$this->Data->getEntityPath()%>
		        	<com:TPanel visible="<%#(!$this->TemplateControl->getPictoTypeCompte())%>">
		        		<br/>
		        		<%#$this->TemplateControl->getTypeCompteAgent($this->Data->getElu(),false)%>
		        	</com:TPanel>
		        </td>
		        <com:TPanel visible="<%#(!$this->TemplateControl->getListeTypeVoix())? 'true' : 'false' %>">
	              <com:TPanel visible="<%#$this->TemplateControl->getTypeVoix()%>">
		              <td headers="typeVoixAgent" class="col-100">
		                  <%#$this->TemplateControl->getLibelleTypeVoix($this->TemplateControl->getTypeVoixAgent($this->Data->getId(),true))%>
		              </td>
	          	  </com:TPanel>
             	</com:TPanel>
              	<com:TPanel visible="<%#$this->TemplateControl->getListeTypeVoix()%>">
	              <td headers="typeVoixAgent" class="col-200">
	                  <com:TDropDownList id="selectTypeVoixAgent" DataSource="<%#$this->TemplateControl->getListeAllTypeVoix()%>" SelectedValue="<%#$this->TemplateControl->getTypeVoixAgent($this->Data->getId(), true)%>" />
	         	  </td>
              	</com:TPanel>
		        <com:TPanel visible="<%#$this->TemplateControl->getActions()%>">
		            <td headers="actionAgent" class="actions">
		            	<com:TPanel visible="<%#$this->TemplateControl->getActionSupprimer()%>" >
		              		<com:TLinkButton CommandName="delete" CommandParameter="<%#$this->Data->getId()%>"><img title="<%#Prado::localize('DEFINE_SUPPRIMER')%>"  src="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-supprimer-big.gif" />
		              		</com:TLinkButton>
		              	</com:TPanel>
		        	</td>
		        </com:TPanel>
	          </tr>
	          </prop:ItemTemplate>
	     </tbody>
	     </com:TRepeater>

  </table>
  <!--Fin Tableau agent-->

  <div class="line-partitioner" >
	<div class="partitioner" >
		<div class="intitule"><strong><label for="allCons_nbResultsTop"><com:TTranslate>DEFINE_TEXT_AFFICHER</com:TTranslate></label></strong></div>
		<com:TDropDownList ID="nombreResultatAfficherBottom" AutoPostBack="true" onSelectedIndexChanged="changePagerLenght" Attributes.title='<%=Prado::localize("NOMBRE_DE_RESULTAT_PAR_PAGE")%>'>
  			<com:TListItem Text="10" Selected="10"/>
  			<com:TListItem Text="20" Value="20"/>
  			<com:TListItem Text="50" Value="50" />
  			<com:TListItem Text="100" Value="100" />
  			<com:TListItem Text="500" Value="500" />
  		</com:TDropDownList>
		<div class="intitule"><com:TTranslate>DEFINE_RESULTATS_PAGE</com:TTranslate></div>
		<label style="display:none;" for="allCons_pageNumberBottom">Aller &agrave; la page</label>

		<com:TPanel ID="panelBouttonGotoPageBottom" DefaultButton="DefaultButtonBottom" CssClass="float-left">
			<com:TTextBox ID="numPageBottom" Attributes.title='<%=Prado::localize("N_DE_LA_PAGE")%>' Text="1"/>
			<div class="nb-total ">/ <com:TLabel ID="nombrePageBottom"/></div>
			<com:TButton ID="DefaultButtonBottom"  OnClick="goToPage" Attributes.style="display:none"/>
		</com:TPanel>

		<div class="liens" >
			<com:TPager ID="PagerBottom"
           			ControlToPaginate="tableauAgent"
	           		FirstPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-premier.gif' alt='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>' title='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>' />"
		            LastPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-dernier.gif' alt='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>' title='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>' />"
	     	        Mode="NextPrev"
	          		NextPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-suivant.gif' alt='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>' title='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>' />"
		            PrevPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-precedent.gif' alt='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>' title='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>' />"
   	                OnPageIndexChanged="pageChanged"/>

		</div>
	</div>
</div>
<!--Fin partitionneur-->
</com:TPanel>