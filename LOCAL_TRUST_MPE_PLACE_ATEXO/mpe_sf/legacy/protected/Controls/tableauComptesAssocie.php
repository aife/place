<?php

namespace Application\Controls;

use Application\Service\Atexo\Agent\Atexo_Agent_AssociationComptes;
use Application\Service\Atexo\Agent\Atexo_Agent_CriteriaVo;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Socle\Atexo_Socle_AgentServicesMetiers;

/*
 * commentaires
 * @author khadija Chouika <khadija.chouika@atexo.com>
 * @copyright Atexo 2013
 * @version 0
 * @since MPE-4.4
 * @package Controls
 */

class tableauComptesAssocie extends MpeTTemplateControl
{
    public $_principal;
    //0 le compte principal des comptes secondaires
    public $typeAssociation;

    // 1 le compte secondaire des comptes principaux
    // 2 choix de comptes à utiliser (secondaire / principale)
    public function getTypeAssociation()
    {
        return $this->typeAssociation;
    }

    public function setTypeAssociation($value)
    {
        $this->typeAssociation = $value;
    }

    public function repeaterDataBind($idAgent)
    {
        $comptes = null;
        $_GET['id'] = $idAgent;
        $arrayIds = [];
        if ($this->getTypeAssociation() == Atexo_Config::getParameter('TYPE_ASSOCIATION_COMPTE_PRINCIPALE')) {
            $comptes = (new Atexo_Agent_AssociationComptes())->findComptesAssocies($idAgent, true);
        } elseif ($this->getTypeAssociation() == Atexo_Config::getParameter('TYPE_ASSOCIATION_COMPTE_SECONDAIRE_PRINCIPALE')) {
            $arrayIds[] = $idAgent;
            $comptes = (new Atexo_Agent_AssociationComptes())->getAllActiveComptesAssocies($idAgent);
        } elseif ($this->getTypeAssociation() == Atexo_Config::getParameter('TYPE_ASSOCIATION_COMPTE_SECONDAIRE')) {
            $comptes = (new Atexo_Agent_AssociationComptes())->findComptesAssocies($idAgent, false);
        }
        if ($comptes) {
            $arrayIds = $this->getIdComptes($this->typeAssociation, $comptes, $arrayIds);
            $criteriaVoAgent = new Atexo_Agent_CriteriaVo();
            $criteriaVoAgent->setListeIdsAgents($arrayIds);
            $this->setViewState('CriteriaVoAgent', $criteriaVoAgent);
            if ($criteriaVoAgent instanceof Atexo_Agent_CriteriaVo) {
                $nombreElement = (new Atexo_Agent())->search($criteriaVoAgent, true);
                //              $this->nbrElementRepeater = $nombreElement;
                $this->setViewState('nbrElements', $nombreElement);
                $this->setViewState('listeAgents', (new Atexo_Agent())->search($criteriaVoAgent, false));
                if ($nombreElement) {
                    $this->RepeaterAgent->setVirtualItemCount($nombreElement);
                    $this->RepeaterAgent->setCurrentPageIndex(0);
                    self::populateData();
                } else {
                    $this->setViewState('listeAgents', []);
                    $this->RepeaterAgent->DataSource = [];
                    $this->RepeaterAgent->DataBind();
                    $this->noResult->Visible = true;
                }
            }

            return true;
        } else {
            $this->setViewState('listeAgents', []);
            $this->RepeaterAgent->DataSource = [];
            $this->RepeaterAgent->DataBind();
            $this->noResult->Visible = true;

            return false;
        }
    }

    public function populateData()
    {
        $offset = $this->RepeaterAgent->CurrentPageIndex * $this->RepeaterAgent->PageSize;
        $limit = $this->RepeaterAgent->PageSize;
        if ($offset + $limit > $this->RepeaterAgent->getVirtualItemCount()) {
            $limit = $this->RepeaterAgent->getVirtualItemCount() - $offset;
        }
        $criteriaVoAgent = $this->getViewState('CriteriaVoAgent');
        $criteriaVoAgent->setLimit($limit);
        $criteriaVoAgent->setOffset($offset);
        if ($this->getViewState('listeAgentsTriee')) {
            $dataAllAgent = $this->getViewState('listeAgentsTriee');
            $dataAgent = [];
            $j = 0;
            for ($i = $offset; $i < (is_countable($dataAllAgent) ? count($dataAllAgent) : 0) && $i < ($limit + $offset); ++$i) {
                $dataAgent[$j++] = $dataAllAgent[$i];
            }
        } else {
            $dataAgent = (new Atexo_Agent())->search($criteriaVoAgent, false);
        }
//        $this->setNbrElementRepeater($this->getViewState("nbrElements"));
        if (count($dataAgent)) {
            $this->RepeaterAgent->DataSource = $dataAgent;
            $this->RepeaterAgent->DataBind();
        }
    }

    //  public function getNbrElementRepeater() {
    //      return $this->nbrElementRepeater;
    //  }

    //  public function setNbrElementRepeater($nbrElement) {
    //      return $this->nbrElementRepeater = $nbrElement;
    //  }
//    public function getServiceMetier($idAgent) {
//
    //      $listeService = Atexo_Socle_AgentServicesMetiers::retreiveListeServiceAgent($idAgent);
    //      $sigleServices ='';
    //      if(is_array($listeService)) {
    //          foreach($listeService as $oneService)
    //          {
    //              $sigleServices .= $oneService->getSigle();
    //              $sigleServices .=' / ';
    //          }
//
    //      }
    //       return  substr(trim($sigleServices,' '),0,-1);
    //  }
    public function getUserRoleAdminCompte()
    {
        if (Atexo_CurrentUser::hasHabilitation('GestionAgents') && $this->manageLinks()) {
            return true;
        } else {
            return false;
        }
    }

    //  public function getUserRoleAgentPole()
    //  {
    //      if(Atexo_CurrentUser::hasHabilitation("GestionHabilitations"))
    //          return true;
    //      else
    //          return false;
    //  }
    /***
     * la gestion des icones entre le socle et mpe
     */
    public function manageLinks()
    {
        if (Atexo_Module::isEnabled('SocleExternePpp')) {
            return false;
        } else {
            if (Atexo_Module::isEnabled('SocleInterne') && 'socle' == Atexo_CurrentUser::readFromSession('ServiceMetier')) {
                return true;
            } elseif (Atexo_Module::isEnabled('SocleInterne') && 'mpe' == Atexo_CurrentUser::readFromSession('ServiceMetier')) {
                return false;
            }
        }

        if (!Atexo_Module::isEnabled('SocleInterne') && !Atexo_Module::isEnabled('SocleExternePpp')) {
            return true;
        }
    }

    public function rompreAssociation($sender, $param)
    {
        if (!isset($_GET['id'])) {
            $_GET['id'] = Atexo_CurrentUser::getIdAgentConnected();
        }
        $idAgentParam = Atexo_Util::atexoHtmlEntities($_GET['id']);
        $idAgent = $this->idSelectedAgent->value;
        $arrayComptesAgentsAssocies_key = 'arrayComptesAgentsAssocies';
        if ($this->getTypeAssociation() == Atexo_Config::getParameter('TYPE_ASSOCIATION_COMPTE_PRINCIPALE')) {
            $arrayComptesAgentsAssocies_key = $arrayComptesAgentsAssocies_key.'COMPTE_PRINCIPAL';
            (new Atexo_Agent_AssociationComptes())->rompreAssociation($idAgentParam, $idAgent);
        } else {
            $arrayComptesAgentsAssocies_key = $arrayComptesAgentsAssocies_key.'COMPTE_SECONDAIRE';
            (new Atexo_Agent_AssociationComptes())->rompreAssociation($idAgent, $idAgentParam);
        }
        $this->scriptT->Text = "<script>J('.".$this->getClientId()."_rompre_association').dialog('close');
			  			        </script>;";

        Atexo_CurrentUser::deleteFromSession($arrayComptesAgentsAssocies_key);
        $this->repeaterDataBind($idAgentParam);
        $this->panelRefresh->render($param->getNewWriter());
    }

    public function getIdComptes($typeAssociation, $comptes, &$arrayIds)
    {
        if ($typeAssociation) {
            foreach ($comptes as $compte) {
                $arrayIds[] = $compte->getCompteSecondaire();
            }
        } else {
            foreach ($comptes as $compte) {
                $arrayIds[] = $compte->getComptePrincipal();
            }
        }

        return $arrayIds;
    }

    public function getIdAgent($sender, $param)
    {
        $this->idSelectedAgent->value = $param->CallbackParameter;
    }

    public function choisirCompte($sender, $param)
    {
        $this->Page->connecter($param->CallbackParameter);
    }
}
