	<div class="spacer"></div>
	<h4>Critère de recherche complémentaire </h4>
	<div class="spacer-mini"></div>
	<div class="line">
		<div class="intitule-auto bloc-700">
			<com:TCheckBox ID="restreindreRecherche" Attributes.title="Restreindre la recherche" CssClass="check" Attributes.onclick="isCheckedShowDiv(this,'restreindreLayer');" /><label for="restreindreRecherche">Restreindre la recherche</label>
	   </div>
	   
	    <!--Debut Layer restreindre-->
		<div class="content-bloc bloc-600 indent-20" id="restreindreLayer" style="display:none;">
			<div class="intitule-auto bloc-500">
				<com:TRadioButton ID="unDomaineAuMoins" GroupName="mode" Attributes.title="Inscrit simple" CssClass="radio"  Checked="true" Attributes.onclick="isCheckedHideDiv(this,'listeDomaines');" /><label for="restreindreRecherche_choix_1">aux organismes ayant saisi au moins 1 domaine</label>
		   </div>
		   <div class="breaker"></div>
			<div class="intitule-auto bloc-500">
				<com:TRadioButton ID="domainesSuivants" GroupName="mode" Attributes.title="Administrateur Entreprise" CssClass="radio" Attributes.onclick="isCheckedShowDiv(this,'listeDomaines');"  /><label for="restreindreRecherche_choix_2">aux organismes ayant saisi au moins 1 des domaines ou sous-domaines ci-dessous</label>
		   </div>
			<!--Debut Bloc Liste des domaines-->
			<span id="listeDomaines" style='display:none'>
				<com:DomainesActivites ID="DomainesActivites" />
			</span>

			<!--Fin Bloc Liste des domaines-->
		   
		</div>
	    <!--Fin Layer restreindre-->
	   
	</div>
	<div class="breaker"></div>
