<!--Debut tableau décision-->
<com:TActivePanel ID="panelDecisionConsultation">
	<!--Debut partitionneur-->
	<div class="line-partitioner">
		<com:TLabel id="labelDefineNombreResultat">
			<h2><com:TTranslate>DEFINE_NOMBRE_RESULTAT</com:TTranslate> <com:TLabel id="nombreResultat" /></h2>
		</com:TLabel>
		<com:THiddenField ID="nombreResultatRepeater" />
		<div class="partitioner" style="display: <%= (!$this->isTypeProcedureSad()? 'block':'none')%>">
			<div class="intitule"><strong><label for="allAnnonces_nbResultsTop"><com:TLabel id="labelAfficherTop"><com:TTranslate>DEFINE_AFFICHER</com:TTranslate></com:TLabel></label></strong></div>

			<com:TActiveDropDownList  Attributes.title="<%=Prado::localize('NOMBRE_DE_RESULTAT_PAR_PAGE')%>" id="nbResultsTop" AutoPostBack="true" onCallBack="changePagerLenght" >
				<com:TListItem Text="10" Selected="10"/>
				<com:TListItem Text="20" Value="20"/>
				<com:TListItem Text="50" Value="50" />
				<com:TListItem Text="100" Value="100" />
				<com:TListItem Text="500" Value="500" />
			</com:TActiveDropDownList>

			<div class="intitule"><com:TLabel id="resultParPageTop"><com:TTranslate>DEFINE_RESULTATS_PAGE</com:TTranslate></com:TLabel></div>
			<label style="display:none;" for="allAnnonces_pageNumberTop"><com:TTranslate>DEFINE_ALLER_PAGE</com:TTranslate></label>
			<com:TPanel ID="panelBouttonGotoPageTop" DefaultButton="DefaultButtonTop" CssClass="float-left">
				<com:TTextBox Attributes.title="<%=Prado::localize('N_DE_LA_PAGE')%>" Text="1" id="pageNumberTop" />
				<div class="nb-total ">
					<com:TLabel ID="labelSlashTop" Text="/" visible="true"/>
					<com:TLabel ID="nombrePageTop"/>
					<com:TButton ID="DefaultButtonTop"  OnClick="goToPage" Attributes.style="display:none"/>
				</div>
			</com:TPanel>
			<div class="liens">
				<com:TPager
						ID="PagerTop"
						ControlToPaginate="repraterDecision"
						FirstPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-premier.gif' alt='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>' title='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>' />"
						PrevPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-precedent.gif' alt='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>' title='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>' />"
						Mode="NextPrev"
						NextPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-suivant.gif' alt='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>' title='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>' />"
						LastPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-dernier.gif' alt='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>' title='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>' />"
						OnPageIndexChanged="pageChanged"
						/>
			</div>
		</div>
	</div>
	<!--Fin partitionneur-->

	<div class="blocLotsSad">
		<com:TLabel id="labelAfficherSad"><com:TTranslate>DEFINE_AFFICHER</com:TTranslate></com:TLabel>
		<com:TActiveDropDownList
				ID="lotsSad"
				CssClass="select-500"
				onCallBack="selectLotSad"
				Attributes.title="<%=Prado::localize('TRANCHE_BUDGETAIRE')%>"
				AutoPostBack="true"
		>
			<prop:ClientSide.OnLoading>showLoader();</prop:ClientSide.OnLoading>
			<prop:ClientSide.OnFailure>showLoader();</prop:ClientSide.OnFailure>
			<prop:ClientSide.OnSuccess>showLoader();</prop:ClientSide.OnSuccess>
			<prop:ClientSide.RequestTimeOut>60000</prop:ClientSide.RequestTimeOut>
		</com:TActiveDropDownList>
	</div>

	<com:TRepeater
			ID="repraterDecision"
			EnableViewState="true"
			AllowPaging="true"
			PageSize="<%= Application\Service\Atexo\Atexo_Config::getParameter('NOMBRE_AFFICHAGE_PAR_DEFAUT')%>"
			AllowCustomPaging="true"
			>
		<prop:HeaderTemplate>
			<table class="table-results tableau-actions-groupees table-highlight tableau-decision" summary="<%=Prado::Localize('TABLEAU_DECISION')%>">
				<thead>
					<tr>
						<th colspan="5" class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></th>
					</tr>
					<tr>
						<th scope="col" class="check-col" abbr="<%=Prado::Localize('TEXT_TOUS_AUCUN')%>" id="selection" >
							<com:TCheckBox
									Visible = "<%#$this->TemplateControl->isConsultationAllotie()? 'true' : 'false' %>"
									id="selectAll" Attributes.title="<%=Prado::localize('TEXT_TOUS')%>"
									Attributes.class="check"
									Attributes.onclick="isAllElementCheckedComposant(this,'blocDecision_repraterDecision','_checkListLots','<%#$this->TemplateControl->nombreResultatRepeater->getClientId()%>');"/>

					</th>
						<th colspan="2" class="col-lot" scope="col"><div><com:TTranslate>LOTS_ATTRIBUTAIRES</com:TTranslate></div></th>
						<th scope="col" class="statut-col">
							<a href="#"><com:TTranslate>DEFINE_STATUS</com:TTranslate><img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/arrow-tri.gif" alt="<%=Prado::Localize('TRIER_PAR_STATUT')%>" /></a>
						</th>
						<th scope="col" class="actions-long"><com:TTranslate>DEFINE_ACTIONS</com:TTranslate></th>
					</tr>
				</thead>
		</prop:HeaderTemplate>
		<prop:ItemTemplate>
			<tbody>
				<tr class="<%#(($this->ItemIndex%2==0)? 'on':'')%>">
					<td class="check-col" headers="selection" rowspan="<%#count($this->Data['listeContrats'])?(count($this->Data['listeContrats'])+1):(count($this->Data['listeSelectionEntreprise'])+1)%>">
						<com:THiddenField id="idLot" Value="<%#$this->Data['lots']%>" />
						<com:TActiveCheckBox
								Visible = "<%# ($this->TemplateControl->isConsultationAllotie() && (! $this->Data['decisionLot'] instanceof Application\Propel\Mpe\CommonDecisionLot) )? 'true' : 'false' %>"
								id="checkListLots"
								Attributes.title="<%=Prado::localize('TEXT_DEFINE_SELECTIONNER')%>"
								Attributes.class="check"
						/>
					</td>
					<td <%#($this->Data['decisionLot'] instanceof Application\Propel\Mpe\CommonDecisionLot && !(is_array($this->Data['listeContrats']) && count($this->Data['listeContrats'])))? 'colspan="2"':''%> class="col-lot">
						<div>
							<com:TLabel	Text="<%#$this->Data['intitule']%>" />
                            <span class="normal" style="display: <%#($this->Data['decisionLot'] instanceof Application\Propel\Mpe\CommonDecisionLot)? 'block':'none' %>">
                                <com:TLabel Text="<%#$this->TemplateControl->getIntituleDecision($this->Data['decisionLot'])%>"/>
                            </span>
                            <span style="display: <%#($this->Data['contratMulti'] instanceof Application\Propel\Mpe\CommonTContratMulti)?'block':'none'%>">
								<span class="normal">
									<com:TActiveLabel Text="<%#(($this->Data['contratMulti'] instanceof Application\Propel\Mpe\CommonTContratMulti) && $this->Data['contratMulti']->getNumeroContrat())?$this->Data['contratMulti']->getNumeroContrat():'-'%>" />
								</span>
                                <img class="picto-info-intitule" onmouseout="cacheBulle('infoAC_lot_<%#($this->ItemIndex)%>')" onmouseover="afficheBulle('infoAC_lot_<%#($this->ItemIndex)%>', this)" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info
								.gif" />
                                <div id="infoAC_lot_<%#($this->ItemIndex)%>" class="info-bulle" onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();">
                                    <div><com:TActiveLabel Text="<%#$this->TemplateControl->getLabelleTypeContratMulti($this->Data['contratMulti'])%>" /></div>
                                </div>
                            </span>
                        </div>
					</td>
					<td style="display:<%#($this->Data['decisionLot'] instanceof Application\Propel\Mpe\CommonDecisionLot && !(is_array($this->Data['listeContrats']) && count($this->Data['listeContrats'])))? 'none':''%>">
						<com:TActivePanel cssClass="montant" Visible="<%#(($this->Data['contratMulti'] instanceof Application\Propel\Mpe\CommonTContratMulti) && (new Application\Service\Atexo\Consultation\Atexo_Consultation_Contrat())->isTypeContratAvecMontantMaxById($this->Data['contratMulti']->getIdTypeContrat()))%>">
							<span class="intitule-auto"><com:TTranslate>MONTANT_MAX_ESTIME</com:TTranslate> :</span>
							<com:TActiveLabel Text="<%#$this->TemplateControl->getMontantMaxFromContrats($this->Data['contratMulti'])%>" />
							<com:TTranslate>DEFINE_UNITE_HT</com:TTranslate>
						</com:TActivePanel>
					</td>

					<td >
						<abbr
							style="display: <%#(($this->Data['decisionLot'] instanceof Application\Propel\Mpe\CommonDecisionLot) ? 'none':'')%>"
							title="<%=Prado::Localize('TYPE_DECISION')%>" class="contrat-etape contrat-etape1sur3">
							<span class="sr-only"><com:TTranslate>TYPE_DECISION</com:TTranslate></span>
						</abbr>
					</td>
					<td class="actions-long">

						<com:THyperLink
								cssClass="bouton"
								visible="<%=$this->TemplateControl->canAddAttribute($this->Data)%>"
								NavigateUrl="javascript:popUp('<%=$this->TemplateControl->getUrlAddAttribute($this->Data)%>','yes');"

						>
							<img src="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/icone-inscription.png" alt="" /><com:TTranslate>AJOUTER_DES_ATTRIBUTAIRES</com:TTranslate>
						</com:THyperLink>
						<com:TActiveDropDownList
								CssClass="candidatures-actions"
								ID="listeActions"
								Visible="<%#(($this->Data['decisionLot'] instanceof Application\Propel\Mpe\CommonDecisionLot) ? false:true)%>"
								Attributes.title="<%=Prado::localize('LISTE_DES_ACTIONS')%>"
								DataSource="<%#$this->TemplateControl->possibleActionsUnique($this->TemplateControl->getStatutActionFromDecision($this->Data['decisionLot']))%>"/>
						<span data-tnr="decisionContratPremierLot">
							<com:ActivePictoOk
									visible="<%#(($this->Data['decisionLot'] instanceof Application\Propel\Mpe\CommonDecisionLot) ? false:true)%>"
									CommandParameter="<%#$this->Data['lots']%>"
									OnCommand="SourceTemplateControl.actionDecision"/>
						</span>
						<com:THyperLink cssClass="bouton"
								Visible = "<%# ($this->TemplateControl->isResetable($this->Data['decisionLot'])) %>"
								NavigateUrl="/?page=Agent.ResetAnalyse&idDecision=<%= ($this->Data['decisionLot'] !== null)?$this->Data['decisionLot']->getIdDecisionLot():null %>"
						>
							&nbsp;<img src="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-suspendre.gif" alt="<com:TTranslate>REINITIALISER_DECISION</com:TTranslate>" />
						</com:THyperLink>
					</td>
				</tr>
                    <com:TRepeater ID="repeaterContrat" DataSource="<%#$this->Data['listeContrats']%>">
                        <prop:ItemTemplate>
                            <tr class="line-attributaire <%#(($this->TemplateControl->ItemIndex%2==0)? 'on':'')%>">
                                <com:THiddenField id="contratId" Value="<%#$this->Data->getIdContratTitulaire()%>" />
                                <td class="col-attributaires col-break">
                                    <div class="attributaire">
                                        <a href="#" class="popover-detail" tabindex="0" data-toggle="popover"
                                           data-popover-content="#detail_lot_<%#$this->Data->getIdContratTitulaire()%>_attributaire_<%#($this->ItemIndex+1)%>"
                                           data-placement="top"><com:TLabel Text="<%#$this->Data->getNomTitulaireContrat()%>" /> </a> -
                                        <com:Tlabel Text="<%#$this->Data->getCodePostalEtablissementContrat()%>"/> <com:Tlabel Text="<%#$this->Data->getVilleEtablissementContrat()%>"/>
                                    </div>

                                    <!-- Content for Popover Detail -->
										<com:PopinInfosAttributaire id="idPopinInfosAttributaire"
																	Etablissement="<%#$this->Data->getTitulaireEtablissementContrat()%>"
																	Contact="<%#$this->Data->getCommonTContactContrat()%>"
																	Entreprise="<%#$this->Data->getTitulaireContrat()%>"
																	IdOffre="<%#$this->Data->getIdOffre()%>"
																	RefConsultation="<%#$this->Data->getRefConsultation()%>"
																	organisme="<%#$this->Data->getOrganisme()%>"
																	TypeReponse="<%#$this->Data->getTypeDepotReponse()%>"
																	IdDiv="detail_lot_<%#$this->Data->getIdContratTitulaire()%>_attributaire_<%#($this->ItemIndex+1)%>"
																	VisibiliteAts="true"
																	RefreshComposant="DOIT ETRE DERNIER ELEMENT POUR CHARGER LE COMPOSANT <%#$this->Data->getIdContratTitulaire()%>"
										/>
										<!-- End Content for Popover Detail -->
										<br>
										<com:AtexoDisponibiliteAttestation
												idEntreprise="<%#$this->Data->getTitulaireContrat()->getId()%>"
												idOffre="<%#$this->Data->getIdOffre()%>"
												typeEnv="<%#($this->Data->getTypeDepotReponse() ==  Application\Service\Atexo\Atexo_Config::getParameter('DEPOT_ELECTRONIQUE'))? 'E':'P'%>"
												refCons="<%#$this->Data->getRefConsultation()%>"
												organisme="<%#$this->Data->getOrganisme()%>"
										/>
									</td>
									<td class="contrat-col">
										<div>
									<span class="intitule-auto">
										<com:TActiveLabel Text="<%#Prado::Localize('NUMERO_CONTRAT')%>" /> :
									</span>
											<a title="" data-original-title="" href="#" class="popover-detail" tabindex="0" data-toggle="popover" data-popover-content="#detail_contrat_<%#$this->Data->getIdContratTitulaire()%>" data-placement="top">
												<com:TLabel visible="<%#($this->Data->getReferenceLibre())? true:false%>" Text="<%#$this->Data->getReferenceLibre()%>" />
											</a>
											<com:TLabel visible="<%#($this->Data->getReferenceLibre())? false:true%>" Text="-" />
											<com:PopinInfosNumerotationContrat contrat="<%#$this->Data%>" lots="<%#$this->parent->parent->Data['lots']%>" />
										</div>
										<com:TActivePanel cssClass="montant" Visible="<%# (new Application\Service\Atexo\Consultation\Atexo_Consultation_Contrat())->isTypeContratAvecMontantById($this->Data->getIdTypeContrat())%>" >
											<span class="intitule-auto"><com:TTranslate>MONTANT</com:TTranslate> :</span>
											<com:TActiveLabel Text="<%#($this->Data->getMontantContrat()? Application\Service\Atexo\Atexo_Util::getMontantArronditEspace($this->Data->getMontantContrat()):'-')%>" />
											<com:TTranslate>DEFINE_UNITE_HT</com:TTranslate>
										</com:TActivePanel>
										<com:TActivePanel cssClass="montant" Visible="<%#((new Application\Service\Atexo\Consultation\Atexo_Consultation_Contrat())->isTypeContratAvecMontantMaxById($this->Data->getIdTypeContrat()) && !(new Application\Service\Atexo\Consultation\Atexo_Consultation_Contrat())->isTypeContratAvecChapeauById($this->Data->getIdTypeContrat()))%>" >
											<span class="intitule-auto"><com:TTranslate>MONTANT_MAX_ESTIME</com:TTranslate> :</span>
											<com:TActiveLabel Text="<%#($this->Data->getMontantMaxEstime()? Application\Service\Atexo\Atexo_Util::getMontantArronditEspace($this->Data->getMontantMaxEstime()):'-')%>" />
											<com:TTranslate>DEFINE_UNITE_HT</com:TTranslate>
										</com:TActivePanel>
										<com:TLinkButton visible="<%#Application\Service\Atexo\Atexo_Module::isEnabled('DecisionFicheRecensement') && $this->Data->getStatutContrat()==  Application\Service\Atexo\Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT_EFFECTUEE') %>" cssclass="picto-link" attributes.title="<%=Prado::localize('TELECHARGER_FICHE_RECENSEMENT')%>" CommandParameter="<%#$this->Data->getIdContratTitulaire()%>" onCommand="SourceTemplateControl.genarateFicheRecensement"><img alt="" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-compresse.gif"><com:TTranslate>TEXT_FICHE_RECENSEMENT</com:TTranslate></com:TLinkButton>

									</td>

									<td>
										<div>

											<abbr title="<%#$this->TemplateControl->TemplateControl->getTitreEtape($this->Data->getStatutContrat())%>"
												  class="contrat-etape <%#$this->TemplateControl->TemplateControl->getEtapeDecision($this->Data->getStatutContrat())%>">
												<span class="sr-only"><%#$this->TemplateControl->TemplateControl->getTitreEtape($this->Data->getStatutContrat())%></span>
											</abbr>
										</div>
										<div class="date-icon">
											<abbr title="<%#$this->TemplateControl->TemplateControl->getTitreDataPrev($this->Data->getStatutContrat())%>">
												<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/<%#($this->Data->getStatutContrat() ==  Application\Service\Atexo\Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT_EFFECTUEE')) ?
										'picto-calendar-ok.gif':'picto-date-previsionelle.png'%>" alt="" /></abbr>
											<com:TLabel Text="<%#($this->Data->getStatutContrat() ==  Application\Service\Atexo\Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT_EFFECTUEE')) ?( Application\Service\Atexo\Atexo_Util::iso2frnDateTime($this->Data->getDateNotification(),false)):( Application\Service\Atexo\Atexo_Util::iso2frnDateTime($this->Data->getDatePrevueNotification(),false))%>" />
										</div>
									</td>
									<td class="actions-long">
										<com:TDropDownList
												CssClass="candidatures-actions"
												ID="listeActions"
												Attributes.title="<%=Prado::localize('LISTE_DES_ACTIONS')%>"
												DataSource="<%#$this->TemplateControl->TemplateControl->possibleActionsUnique($this->Data->getStatutContrat(), $this->Data)%>"/>
										<span data-tnr="decisionOkPremierLot">
									<com:ActivePictoOk
											CommandParameter="<%#array('idContrat'=>$this->Data->getIdContratTitulaire(),'lots'=>$this->TemplateControl->Data['lots'])%>"
											OnCommand="SourceTemplateControl.actionSurContrat"/>
								</span>
									</td>
								</tr>
							</prop:ItemTemplate>
						</com:TRepeater>

            <com:TRepeater ID="repeaterSelectionEntreprise" DataSource="<%#$this->Data['listeSelectionEntreprise']%>">
                <prop:ItemTemplate>
                    <tr class="line-attributaire <%#(($this->TemplateControl->ItemIndex%2==0)? 'on':'')%>">
                        <td class="col-attributaires col-break">
                            <div class="attributaire">
                                <a href="#" class="popover-detail" tabindex="0" data-toggle="popover"
                                   data-popover-content="#detail_lot_decision_<%#$this->Data->getId()%>_attributaire_<%#($this->ItemIndex+1)%>"
                                   data-placement="top"><com:TLabel Text="<%#$this->Data->getNomEntrepriseDecision()%>" /> </a> -
                                <com:Tlabel Text="<%#$this->Data->getCodePostalEtablissementDecision()%>"/> <com:Tlabel Text="<%#$this->Data->getVilleEtablissementDecision()%>"/>
                            </div>
                            <!-- Content for Popover Detail -->
                                <com:PopinInfosAttributaire id="idPopinInfosAttributaire"
                                                            Etablissement="<%#$this->Data->getEtablissementDecision()%>"
                                                            Contact="<%#$this->Data->getCommonTContactContrat()%>"
                                                            Entreprise="<%#$this->Data->getEntrepriseDecision()%>"
                                                            IdOffre="<%#$this->Data->getIdOffre()%>"
                                                            RefConsultation="<%#$this->Data->getConsultationId()%>"
															organisme="<%#$this->Data->getOrganisme()%>"
                                                            TypeReponse="<%#$this->Data->getTypeDepotReponse()%>"
                                                            IdDiv="detail_lot_decision_<%#$this->Data->getId()%>_attributaire_<%#($this->ItemIndex+1)%>"
															RefreshComposant="DOIT ETRE DERNIER ELEMENT POUR CHARGER LE COMPOSANT <%#$this->Data->getId()%>"
										/>

                                <!-- End Content for Popover Detail -->
								<br>
								<com:AtexoDisponibiliteAttestation
										idEntreprise="<%#$this->Data->getEntrepriseDecision()->getId()%>"
										idOffre="<%#$this->Data->getIdOffre()%>"
										typeEnv="<%#($this->Data->getTypeDepotReponse() ==  Application\Service\Atexo\Atexo_Config::getParameter('DEPOT_ELECTRONIQUE'))? 'E':'P'%>"
										refCons="<%#$this->Data->getConsultationId()%>"
										organisme="<%#$this->Data->getOrganisme()%>"
								/>

							</td>
							<td class="contrat-col">
							</td>
							<td>
							</td>
							<td class="actions-long">
							</td>
						</tr>
					</prop:ItemTemplate>
				</com:TRepeater>
			</tbody>
		</prop:ItemTemplate>
		<prop:FooterTemplate>
			</table>
		</prop:FooterTemplate>
	</com:TRepeater>
	<div class="form-field actions-groupees actions-groupees-dock" style="display:<%=$this->afficherActionGroupees()?'':'none'%>">
		<div class="content">
			<h2><com:TTranslate>TEXT_ACTIONS_GROUPEES</com:TTranslate></h2>
			<div class="actions">
				<com:TDropDownList
						ID="listeActionsGroupees"
						CssClass="candidatures-actions"
						Attributes.title="<%=Prado::localize('LISTE_DES_ACTIONS')%>"
						/>
				<com:ActivePictoOk

						OnCommand="actionGroupeeDecision"/>

			</div>
			<div class="breaker"></div>
		</div>
		<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
	</div>
	<!--Debut partitionneur-->
	<div class="line-partitioner" >
		<div class="partitioner" >
			<div class="intitule"><strong><label for="horsLigne_nbResultsBottom"><com:TLabel id="labelAfficherDown"><com:TTranslate>DEFINE_AFFICHER</com:TTranslate></com:TLabel></label></strong></div>

			<com:TActiveDropDownList Attributes.title="<%=Prado::localize('NOMBRE_DE_RESULTAT_PAR_PAGE')%>" id="nbResultsBottom" AutoPostBack="true" onCallBack="changePagerLenght" >
				<com:TListItem Text="10" Selected="10"/>
				<com:TListItem Text="20" Value="20"/>
				<com:TListItem Text="50" Value="50" />
				<com:TListItem Text="100" Value="100" />
				<com:TListItem Text="500" Value="500" />
			</com:TActiveDropDownList>

			<div class="intitule"><com:TLabel id="resultParPageDown"><com:TTranslate>DEFINE_RESULTATS_PAGE</com:TTranslate></com:TLabel></div>
			<label style="display:none;" for="horsLigne_pageNumberBottom"><com:TTranslate>DEFINE_ALLER_PAGE</com:TTranslate></label>
			<com:TPanel ID="panelBouttonGotoPageBottom" DefaultButton="DefaultButtonBottom" CssClass="float-left">
				<com:TTextBox Attributes.title="<%=Prado::localize('N_DE_LA_PAGE')%>" Text="1" id="pageNumberBottom" />
				<div class="nb-total ">
					<com:TLabel ID="labelSlashBottom" Text="/" visible="true"/>
					<com:TLabel ID="nombrePageBottom"/>
					<com:TButton ID="DefaultButtonBottom"  OnClick="goToPage" Attributes.style="display:none"/>
				</div>
			</com:TPanel>
			<div class="liens">
				<com:TPager
						ID="PagerBottom"
						ControlToPaginate="repraterDecision"
						FirstPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-premier.gif' alt='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>' title='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>' />"
						PrevPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-precedent.gif' alt='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>' title='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>' />"
						Mode="NextPrev"
						NextPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-suivant.gif' alt='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>' title='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>' />"
						LastPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-dernier.gif' alt='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>' title='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>' />"
						OnPageIndexChanged="pageChanged"
						/>
			</div>
		</div>
	</div>
	<!--Fin partitionneur-->

<div class="spacer-small"></div>
<span style="display:none;">
	<com:TButton id="refreshButton" onclick="refreshRepeater" />
	<com:TActiveHyperLink id="redirectBouton" />
</span>
<com:TActiveHiddenField id="listeLotToUp" />
<com:TActiveLabel ID="javascriptLabel" />
<!--Fin tableau décision-->
</com:TActivePanel>

<!--Debut tableau contrat via exec-->
<com:TConditional condition="Application\Service\Atexo\Atexo_Config::getParameter('ACTIVE_EXEC_V2')">
	<prop:trueTemplate>
		<script src="/execution/exec.js?k=<%= Application\Service\Atexo\Atexo_Config::getParameter('MEDIA_CACHE_KEY')%>" type="application/javascript" defer></script>
		<exec-tableau-contrats
				class="exec"
				afficher-chorus="true"
				sso="<%=$this->getAgentSso()%>"
				rechercheAvanceeOuverte="false"
				filtres='<%=$this->getFiltresForWebComponent()%>'
				pf-uid="<%= Application\Service\Atexo\Atexo_Config::getParameter('UID_PF_MPE')%>"
		>
		</exec-tableau-contrats>
	</prop:trueTemplate>
</com:TConditional>
<!--Fin tableau contrat via exec-->

<com:THiddenField id="titrePopin" Attributes.title="<%=Prado::Localize('SUPPRIMER_LE_CONTRAT')%>" />
<!--Debut Modal-->
<div class="demander-suppression" style="display:none;">
	<!--Debut Formulaire-->
	<div class="form-bloc">
		<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
		<div class="content">
			<p><com:TActiveLabel ID="messageConfirmationSuppressionContrat"/></p>
		</div>
		<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
		<com:TActiveHiddenField id="idContratToDelete" />
		<com:TActiveHiddenField id="lotContratToDelete" />
		<com:TActiveHiddenField id="organismeContrat" />
	</div>
	<!--Fin Formulaire-->
	<!--Debut line boutons-->
	<div class="boutons-line">
		<input type="button" class="bouton-moyen float-left" value="<%=Prado::localize('DEFINE_ANNULER')%>" title="<%=Prado::localize('DEFINE_ANNULER')%>" onclick="J('.demander-suppression').dialog('destroy');" />
		<com:TActiveButton
				id="supprimerAttributaire"
				Text = "<%=Prado::localize('TEXT_VALIDER')%>"
				Attributes.title="<%=Prado::localize('TEXT_VALIDER')%>"
				CssClass="bouton-moyen float-right"
				onCallBack="supprimerAttributaire"
		>
			<prop:ClientSide.OnLoading>showLoader();</prop:ClientSide.OnLoading>
			<prop:ClientSide.OnFailure>showLoader();</prop:ClientSide.OnFailure>
			<prop:ClientSide.OnSuccess>showLoader();</prop:ClientSide.OnSuccess>
		</com:TActiveButton >
		<com:TActiveButton
				id="envoiMailAgent"
				Text = "<%=Prado::localize('TEXT_VALIDER')%>"
				Attributes.title="<%=Prado::localize('TEXT_VALIDER')%>"
				CssClass="bouton-moyen float-right"
				onCallBack="envoiMailSuppressionEj"
		>
			<prop:ClientSide.OnLoading>showLoader();</prop:ClientSide.OnLoading>
			<prop:ClientSide.OnFailure>showLoader();</prop:ClientSide.OnFailure>
			<prop:ClientSide.OnSuccess>showLoader();</prop:ClientSide.OnSuccess>
		</com:TActiveButton >

	</div>
	<!--Fin line boutons-->
</div>
<script>
	if (document.getElementsByTagName('exec-tableau-contrats')) {
		document.getElementsByTagName('exec-tableau-contrats')[0].addEventListener("ondeletecontrat", () => {
			location.reload();
		});
	}
</script>
<!--Fin Modal-->

