		<com:AtexoValidationSummary  ValidationGroup="validateinfomessage" />
	
		<!--Debut Bloc Message à envoyer-->
        <com:TPanel ID="panelTypeMessage" CssClass="form-field">
        	<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
            <com:TPanel CssClass="content">
                <div class="line">
                    <div class="intitule-110"><label for="<%=$this->messageType->getClientId()%>"><com:TTranslate>DEFINE_TEXT_TYPE_MESSAGE</com:TTranslate></label> :</div>
                    <com:TDropDownList 
                    	ID="messageType"
                    	CssClass="moyen"
                    	Enabled="false"
                    	Attributes.title = "<%=Prado::localize('DEFINE_TEXT_TYPE_MESSAGE')%>"
                    />
                </div>
            </com:TPanel>
            <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
        </com:TPanel>
		<div class="form-field">
			<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><span class="title"><com:TTranslate>TEXT_MESSAGE</com:TTranslate></span></div>
			<div class="content">
                <!--[if lte IE 6]>
                	<div class="spacer-mini"></div>
                <![endif]-->
				<div class="line">
					<div class="intitule-110"><label for="<%=$this->objet->getClientId()%>"><com:TTranslate>OBJET</com:TTranslate></label> :</div>
					<com:TTextBox
							ID="objet"
							CssClass="long-630"
							Attributes.title = "<%=Prado::localize('OBJET')%>"
						/>
				</div>
				<div class="spacer-mini"></div>
				<div class="line">
					<div class="intitule-110"><label for="<%=$this->textMail->getClientId()%>"><com:TTranslate>DEFINE_TEXT_TEXT</com:TTranslate></label> :</div>
						<com:TTextBox
							ID="textMail"
							CssClass="long-630 high-360"
							TextMode="MultiLine"
							Attributes.title = "<%=Prado::localize('DEFINE_TEXT_TEXT')%>"
						/>
				</div>
				<div class="spacer-mini"></div>
				<!--Debut Line Partie non modifiable du message-->
				<div class="line">
					<div class="intitule-bloc intitule-110"><com:TTranslate>TEXTE_NON_MODIFIABLE</com:TTranslate> :</div>
					<com:TPanel CssClass="content-bloc bloc-650">
						<div class="border-bloc bloc-630">
							<com:TTranslate>DEFINE_NOM</com:TTranslate> : <com:TTranslate>DEFINE_INFORMATION_PROPRE_JAL</com:TTranslate><br />
							<com:TTranslate>ADRESSE_ELECTRONIQUE</com:TTranslate> : <com:TTranslate>DEFINE_INFORMATION_PROPRE_JAL</com:TTranslate><br />
							<com:TTranslate>TEXT_ADRESSE_ACCUSE</com:TTranslate> : <com:TTranslate>DEFINE_INFORMATION_PROPRE_JAL</com:TTranslate><br />
							<com:TTranslate>TEXT_TELECOPIEUR</com:TTranslate> : <com:TTranslate>DEFINE_INFORMATION_PROPRE_JAL</com:TTranslate><br />
							<com:TTranslate>DEFINE_TEXT_INFORMATION_FACTURATION</com:TTranslate> : <com:TTranslate>DEFINE_INFORMATION_PROPRE_JAL</com:TTranslate><br />
							<span style="display:<%=(Application\Service\Atexo\Atexo_Module::isEnabled('CentralePublication')) ? '':'none'%>"><com:TTranslate>DEFINE_LISTE_JOURNAUX</com:TTranslate> : <com:TTranslate>DEFINE_INFORMATION_PROPRE_CENTRALE_PUBLICATION</com:TTranslate></span>
						</div>
					</com:TPanel>
				</div>
				<div class="spacer-mini"></div>
				<!--Fin Line Partie non modifiable du message-->
				<div class="line">
					<div class="intitule-110"><label for="tableau"><com:TTranslate>DEFINE_TEXT_PIECE_JOINTE</com:TTranslate></label> :</div>
                        <com:TActivePanel CssClass="content-bloc bloc-660" ID="PanelPJ">
						  <com:TRepeater ID="RepeaterPiecesJointes" EnableViewState="true"   AllowPaging="true"
					  			   AllowCustomPaging="true" DataKeyField="Id">
							<prop:ItemTemplate>
								<div class="picto-link inline">
									<a href="?page=Agent.AgentDownloadPjAnnonce&idPj=<%#$this->Data->getPiece()%>"><%#$this->Data->getNomFichier()%><span class="info-aide"> (<%# Application\Service\Atexo\Atexo_Util::arrondirSizeFile($this->Data->getTaille()/1024)%>)</span></a>
										<com:TActiveLinkButton 
											CssClass="indent-5"
											Visible=<%=$this->Page->TemplateEnvoiCourrierElectroniqueJAL->_modeAnnonceJAL%>
											OnCallBack="Page.TemplateEnvoiCourrierElectroniqueJAL.refreshRepeaterPJ" 
											OnCommand="Page.TemplateEnvoiCourrierElectroniqueJAL.onDeleteClick" 
											CommandParameter="<%#$this->Data->getId()%>"
											>
											<com:TImage 
												ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-supprimer.gif"
												AlternateText="<%=Prado::localize('DEFINE_SUPPRIMER')%>"
												Attributes.title="<%=Prado::localize('DEFINE_SUPPRIMER')%>"
												/>
										</com:TActiveLinkButton>
								</div>
								<br/><br/>
							</prop:ItemTemplate>
						</com:TRepeater>
						<com:TActiveHyperLink 
							ID="buttonEditPj"
							Visible="<%=$this->_modeAnnonceJAL%>"
							NavigateUrl="javascript:popUp('?page=Agent.popUpListePjAnnonceJAL&IdAnnonceJAL=<%=$this->_idAnnonceJAL%>&org=<%=$this->_org%>','yes');"
							CssClass="bouton-edit clear-both" 
							Attributes.title="<%=Prado::localize('DEFINE_TEXT_EDITER_LIST_PIECE_JOINTE')%>" 
							Text="<%=Prado::localize('DEFINE_TEXT_EDITER')%>">
						</com:TActiveHyperLink>
		  				<com:TActiveTextBox ID="countPj" Display="None"/>
		  				<div class="breaker"></div>
					  </com:TActivePanel>
					  <span style ="display:none;">
						<com:TActiveButton 
							Id="validatePJ"							  
							onCallBack="refreshRepeaterPJ"
							/>	
					</span>
				</div>
				<div class="breaker"></div>
			</div>
			<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
		</div>
		<!--Fin Bloc Message à envoyer-->
		 <!--Debut Bloc Option d'envoi-->
		<com:TPanel ID="panelOptionEnvoi" CssClass="form-field">
			<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><span class="title"><com:TTranslate>DEFINE_TEXT_OPTION_ENVOI</com:TTranslate></span></div>
				<div class="content">
				<div class="spacer-mini"></div>
				<div class="line">
					<div class="intitule-auto bloc-700">
					<com:TActiveRadioButton
						ID = "courrierElectroniqueSimple"
						GroupName="optionEnvoie"
						CssClass = "radio"
						Attributes.title = "<%=Prado::localize('DEFINE_TEXT_COURIER_ELECTRONIQUE_SIMPLE')%>"					
					/>
					<label for="<%=$this->courrierElectroniqueSimple->getClientId()%>"><com:TTranslate>DEFINE_TEXT_COURIER_ELECTRONIQUE_SIMPLE</com:TTranslate></label><span class="info-aide indent-10"><com:TTranslate>DEFINE_TEXT_TAILLE_PIECE_JOINTE</com:TTranslate></span>
				  </div>
					<div class="spacer-mini"></div>
					<div class="intitule-auto bloc-700">
					<com:TActiveRadioButton
						ID = "courrierElectroniqueContenuIntegralAR"
						GroupName="optionEnvoie"
						CssClass = "radio"
						Attributes.title = "<%=Prado::localize('DEFINE_TEXT_COURIER_ELECTRONIQUE_CONTENU_INTEGRAL')%>"					
					/>
					<label for="<%=$this->courrierElectroniqueContenuIntegralAR->getClientId()%>"><com:TTranslate>DEFINE_TEXT_COURIER_ELECTRONIQUE_CONTENU_INTEGRAL</com:TTranslate></label> (<com:TTranslate>DEFINE_TEXT_Lien_RECEPTION</com:TTranslate>)<span class="info-aide indent-10"><com:TTranslate>DEFINE_TEXT_TAILLE_PIECE_JOINTE</com:TTranslate></span>
					</div>
                    <div class="spacer-mini"></div>
                    <div class="intitule-auto bloc-700">
                    <com:TActiveRadioButton
						ID = "courrierElectroniqueUniquementLienTelechargementObligatoire"
						GroupName="optionEnvoie"
						CssClass = "radio"
						Attributes.title = "<%=Prado::localize('DEFINE_TEXT_COURIER_ELECTRONIQUE_TELECHARGEMENT')%>"					
					/>
					<label for="<%=$this->courrierElectroniqueUniquementLienTelechargementObligatoire->getClientId()%>"><com:TTranslate>DEFINE_TEXT_COURIER_ELECTRONIQUE_TELECHARGEMENT_UNIQUE</com:TTranslate></label> <com:TTranslate>TEXT_PLATE_FORME_AR</com:TTranslate>
				  </div>
					<div class="breaker"></div>
				</div>
			</div>
			<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
		</com:TPanel>
		<!--Fin Bloc Option d'envoi-->
		<!--Debut line boutons-->
		<div class="boutons-line">
			<com:TButton  
				CssClass="bouton-annulation float-left" 
				Attributes.value="<%=Prado::localize('TEXT_ANNULER')%>" 
				Attributes.title="<%=Prado::localize('DEFINE_TEXT_MAIL_RETOUR')%>"
				OnClick="RedirectReturn"
				/>
			<com:TActivePanel ID="panelButtonSave" Visible="<%=$this->_modeAnnonceJAL%>">	
				<com:TActiveHyperLink 
					CssClass="bouton-validation float-right" 
					Text="<%=Prado::localize('DEFINE_ENREGISTRER')%>" 
					NavigateUrl="javascript:displayAvertissementSaveWithPj('<%=$this->countPj->Text%>');"
					Attributes.title="<%=Prado::localize('DEFINE_ENREGISTRER')%>">    
				</com:TActiveHyperLink>
					<com:TButton  
						ID="buttonSave"
						Display="None"
						ValidationGroup="validateinfomessage" 
						onClick="enregistrer" />
			</com:TActivePanel>	
		</div>
		<!--Fin line boutons-->
		<div class="breaker"></div>
