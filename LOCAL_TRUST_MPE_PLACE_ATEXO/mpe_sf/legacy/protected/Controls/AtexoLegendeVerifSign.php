<?php

namespace Application\Controls;

/**
 * Legende de verification de signature.
 *
 * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
 *
 * @version 1.0
 *
 * @since ESR-2015
 *
 * @copyright Atexo 2015
 */
class AtexoLegendeVerifSign extends MpeTTemplateControl
{
}
