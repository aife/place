<?php

namespace Application\Controls;

/**
 * Menu gauche des pages du coté entreprise.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class MenuGaucheEntrepriseMpeSf extends MpeTPage
{
    public function onLoad($param)
    {
    }
}
