<?php

namespace Application\Controls;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonDossierVolumineux;
use Application\Propel\Mpe\CommonEchange;
use Application\Propel\Mpe\CommonEchangePieceJointePeer;
use Application\Propel\Mpe\CommonOrganisme;
use Application\Propel\Mpe\CommonRelationEchange;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_MpeSf;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Config\Atexo_Config_Exception;
use Application\Service\Atexo\Message\Atexo_Message_RelationEchange;

/**
 * Classe DetailEchangeMailAgent.
 *
 * @author SBAI Yassine <yassine.sbai@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class DetailEchangeMailAgent extends MpeTPage
{
    public $_calledFrom = '';
    public $_acronymeOrg = false;
    public $_refConsultation = false;
    public $_numAccuseReception = false;
    public $DossierVolumineux = false;
    public $idMsg = false;

    public function onLoad($param)
    {
        /*if(!$this->Page->IsPostBack) {
         }*/
    }

    public function setCalledFrom($value)
    {
        $this->_calledFrom = $value;
    }

    public function getCalledFrom()
    {
        return $this->_calledFrom;
    }

    public function remplirInfoEchange($objetEchangeDest, $org)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $objetEchange = $objetEchangeDest->getCommonEchange($connexionCom);
        $this->setViewState('echange', $objetEchange);
        if ($objetEchange->getOptionEnvoi() == Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_REPONSE_AUTORISEE')
            && 0 == strcmp($this->getCalledFrom(), 'entreprise')) {
            $this->reponseMessage->setVisible(true);
        }
        $organismeObj = Atexo_Organismes::retrieveOrganismeByAcronyme($org);
        if ($organismeObj instanceof CommonOrganisme) {
            $expediteur = $organismeObj->getSigle().' - '.$organismeObj->getDenominationOrg();
        } else {
            $expediteur = '-';
        }
        $this->expediteur->Text = $objetEchange->getExpediteur();
        $this->destinataire->Text = $objetEchangeDest->getMailDestinataire();
        $this->datenvoi->Text = Atexo_Util::iso2frnDateTime($objetEchange->getDateMessage());
        $this->objet->Text = $objetEchange->getObjet();
        $result = explode('
', $objetEchange->getCorps());

        for ($i = 0; $i < count($result); ++$i) {
            if (strpos($result[$i], 'http')) {
                $result[$i] = $result[$i].'&visuel=true';
                $newResult[$i] = substr($result[$i], 0, strpos($result[$i], 'http'))."<a href='".substr($result[$i], strpos($result[$i], 'http'))."'>".substr($result[$i], strpos($result[$i], 'http')).'</a>';
            } else {
                $newResult[$i] = $result[$i];
            }
        }
        $corps = implode('<br>', $newResult);
        $this->corps->Text = $corps;
        $criteria = new Criteria();
        $criteria->add(CommonEchangePieceJointePeer::ORGANISME, $org);

        $this->remplirRepeaterPJ($objetEchange->getCommonEchangePieceJointes($criteria, $connexionCom));

        $this->textUpDV->setVisible(false);
        if (Atexo_Module::isEnabled('ModuleEnvol')) {
            $dossierVolumineux = $objetEchange->getCommonDossierVolumineux($connexionCom);
            if ($dossierVolumineux instanceof CommonDossierVolumineux) {
                Atexo_MpeSf::addAllowedDossiersVolumineux($dossierVolumineux->getId());
                $url = Atexo_Config::getParameter('URL_LT_MPE_SF').'/entreprise/dossier-volumineux/download/'.$dossierVolumineux->getUuidReference();
                $labelFirst = $dossierVolumineux->getNom().' (Identifiant : '.$dossierVolumineux->getUuidReference().' - Taille : '.Atexo_Util::GetSizeName($dossierVolumineux->getTaille()).')';
                $this->urlDossierVolumineux->Text = '<a href="'.$url.'" class="btn tbn-sm btn-primary"><i class="fa fa-download m-r-1"></i>'.$labelFirst.'</a>';
                $this->textUpDV->setVisible(true);
            }
        }
    }

    public function remplirRepeaterPJ($PJ)
    {
        $this->RepeaterPiecesJointes->dataSource = $PJ;
        $this->RepeaterPiecesJointes->dataBind();
    }

    public function setDossierVolumineux($objDossierVolumineux)
    {
        $this->DossierVolumineux = $objDossierVolumineux;
    }

    public function downloadCompleteAnnexes()
    {
        $this->_acronymeOrg = Atexo_CurrentUser::getCurrentOrganism();
        $this->idMsg = Atexo_Util::atexoHtmlEntities($_GET['idMsgDest']);

        $objetEchangeDest = Atexo_Message::getEchangeDestinataireByid($this->idMsg, $this->_acronymeOrg);

        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $objetEchange = $objetEchangeDest->getCommonEchange($connexionCom);

        $this->setDossierVolumineux($objetEchange->getCommonDossierVolumineux($connexionCom));

        $this->downloadAnnexes($this->DossierVolumineux->getUuidReference());
        exit();
    }

    /**
     * @param $uuidReference
     *
     * @throws Atexo_Config_Exception
     */
    private function downloadAnnexes($uuidReference)
    {
        $downloadDce = new EntrepriseDownloadDce();
        $downloadDce->downloadAnnexes($uuidReference);
    }

    /**
     * Permet de repondre au mail.
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function repondre()
    {
        if (Atexo_Module::isEnabled('InterfaceModuleSub') && $this->isTypeCourrierDemandeComplements()) {
            $url = Atexo_MpeSf::getUrlDetailsConsultation($this->_refConsultation, 'orgAcronyme='.$this->_acronymeOrg.'&depots&clotures');
            $this->getResponse()->redirect($url);
        }
        $this->response->redirect('index.php?page=Entreprise.ReponseEntreprise&id='.$this->_refConsultation.'&acronymeOrg='.$this->_acronymeOrg.'&num_ar='.$this->_numAccuseReception);
    }

    /**
     * Permet de Verifier que le courrier est de type demande de complements.
     *
     * @return bool : true si courrier de type demande de complements, false sinon
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function isTypeCourrierDemandeComplements()
    {
        $courrier = $this->getViewState('echange');
        $return = false;
        if ($courrier instanceof CommonEchange) {
            $relation = (new Atexo_Message_RelationEchange())->getRelationByIdEchangeAndTypeRelation($courrier->getId(), Atexo_Config::getParameter('TYPE_RELATION_ECHANGE_COMPLEMENT_FORMULAIRE'), $courrier->getOrganisme());
            if ($relation instanceof CommonRelationEchange) {
                $return = true;
            }
        }

        return $return;
    }
}
