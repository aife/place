<?php

namespace Application\Controls;

/**
 * Page de gestion des droits d'accès de la consultation.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 4
 */
class DroitsAccesConsultation extends MpeTTemplateControl
{
    public $consultation;

    /**
     * recupère la consultation.
     */
    public function getConsultation()
    {
        return $this->consultation;
    }

    // getConsultation()

    /**
     * Affecte l'objet de la consultation.
     *
     * @param objet $consultation
     */
    public function setConsultation($consultation)
    {
        if ($this->consultation !== $consultation) {
            $this->consultation = $consultation;
        }
    }

    // setConsultation()

    /**
     * Chargement de la page.
     */
    public function onLoad($param)
    {
    }
}
