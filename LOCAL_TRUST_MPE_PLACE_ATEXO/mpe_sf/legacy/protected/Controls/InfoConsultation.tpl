<!--Debut Bloc Recap Consultation-->
<div class="form-bloc" id="recap-consultation">
	<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
	<div class="content">
		<a class="title-toggle" onclick="togglePanel(this,'infosPrincipales');" href="javascript:void(0);">&nbsp;</a>
		<div class="recap-bloc">
			<div class="line">
				<div class="intitule-150"><strong><com:TTranslate>TEXT_REFERENCE</com:TTranslate></strong></div>
				<div class="content-bloc bloc-590"><com:TLabel id="refConsultation" /></div>
			</div>
			<div class="line">
				<div class="intitule-150"><strong><com:TTranslate>DEFINE_OBJET_CONSULTATION</com:TTranslate> :</strong></div>
				<div class="content-bloc bloc-590"><com:TLabel id="objetConsultation" /></div>
			</div>
			<div id="infosPrincipales" style="display:none;" class="panel-toggle">
				<div class="line">
					<div class="intitule-250 bold"><com:TTranslate>DEFINE_ENTITE_PUBLIQUE</com:TTranslate></div>
					<com:TLabel id="entitePublique" />
				</div>
				<div class="line">
					<div class="intitule-250 bold"><com:TTranslate>DEFINE_ENTITE_ACHAT</com:TTranslate> :</div>
					<com:TLabel id="entiteAchat" />
				</div>
				<div class="line">
					<div class="intitule-250 bold"><com:TTranslate>TYPE_ANNONCE</com:TTranslate> :</div>
					<com:TLabel id="typeAnnonce" />
				</div>
				<div class="line">
					<div class="intitule-250 bold"><com:TTranslate>TEXT_TYPE_PROCEDURE</com:TTranslate> :</div>
					<com:TLabel id="typeProcedure" />
				</div>
				<div class="line">
					<div class="intitule-250"><strong><com:TTranslate>DEFINE_INTITULE_CONSULTATION</com:TTranslate> :</strong></div>
					<com:TLabel id="intituleConsultation" />
				</div>
				<div class="line">
					<div class="intitule-250"><strong><com:TTranslate>DEFINE_OBJET_CONSULTATION</com:TTranslate> :</strong></div>
					<com:TLabel id="objetPrincipal" />
				</div>
				<div class="line">
					<div class="intitule-250 bold"><com:TTranslate>DEFINE_CATEGORIE_PRINCIPALE</com:TTranslate> :</div>
					<com:TLabel id="categorieConsultation" />
				</div>
				<div class="line">
					<div class="intitule-250 bold"><com:TTranslate>DEFINE_DATE_LIMITE_REMISE_PLI</com:TTranslate> :</div>
					<com:TLabel id="dateHeureLimiteRemisePlis" />
				</div>
				<div class="breaker"></div>
			</div>

		</div>
		<div class="spacer-small"></div>
		<div class="breaker"></div>
	</div>
	<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
</div>
<!--Fin Bloc Recap Consultation-->