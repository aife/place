<div class="line">
    <div class="intitule col-300"><label for="<%=$this->listMultiSelect->getClientId()%>"><%=$this->getLibelleListe()%><span class="champ-oblig">*</span></label> :</div>
    <div class="content-bloc">
        <com:TActiveListBox
            id="listMultiSelect"
            CssClass="moyen chosen-select champ-pub"
            AutoPostBack="true"
            SelectionMode="Multiple"
            Attributes.multiple ="multiple"
            Attributes.title="<%=$this->getTitleListe()%>"
            attributes.onchange ="updateSelection(this,'<%=$this->hiddenSelectedValue->getClientId()%>');"
        />
    </div>
    <div class="col-info-bulle" style="display: <%=($this->getAfficherInfoBulle() == true)? 'block':'none'%>">
        <img alt="Info-bulle" class="picto-info-intitule" onmouseout="cacheBulle('infosMotsDescripteurs')" onmouseover="afficheBulle('infosMotsDescripteurs', this)" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif">
        <div style="display: none; left: 1166px; top: 653px;" id="infosMotsDescripteurs" class="info-bulle" onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();"><div><%=$this->getLibelleInfoBulle()%></div></div>
    </div>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <com:THiddenField id="hiddenSelectedValue" />
    <com:TCustomValidator
            ControlToValidate="hiddenSelectedValue"
            ValidationGroup="<%=$this->getValidationGroup()%>"
            Enabled="<%=$this->getEnabled()%>"
            ClientValidationFunction="<%=$this->getFonctionValidation()%>"
            Display="Dynamic"
            ErrorMessage="<%=$this->getLibelleListe()%>"
            EnableClientScript="true"
            Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
        <prop:ClientSide.OnValidationError>
            blockErreur = '<%=($this->getValidationSummary())%>';
            if(document.getElementById(blockErreur))
            document.getElementById(blockErreur).style.display='';
            if(document.getElementById('ctl0_CONTENU_PAGE_blocBoutons'))
            document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
        </prop:ClientSide.OnValidationError>
    </com:TCustomValidator>
    <script>
        J("#<%=$this->listMultiSelect->getClientId()%>").chosen({
            disable_search: false,
            no_results_text: "<%=Prado::localize('AUCUN_RESULTAT_MULTISELECT')%>",
            placeholder_text_multiple : "<%=$this->getLibelleSelectionez()%>",
            max_selected_options: "<%=$this->getNombreMaxSelection()%>"
        });

    </script>
</div>