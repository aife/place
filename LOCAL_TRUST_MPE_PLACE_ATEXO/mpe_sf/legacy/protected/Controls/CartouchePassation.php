<?php

namespace Application\Controls;

use Application\Propel\Mpe\CommonDecisionEnveloppe;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Responses;

/**
 * commentaires.
 *
 * @author mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class CartouchePassation extends MpeTPage
{
    private $_decision;
    private $_referenceUtilisateur;

    public function getReferenceUtilisateur()
    {
        return $this->_referenceUtilisateur;
    }

    public function setReferenceUtilisateur($v)
    {
        $this->_referenceUtilisateur = $v;
    }

    public function getDecision()
    {
        return $this->_decision;
    }

    public function setDecision($v)
    {
        $this->_decision = $v;
    }

    public function displayDecision()
    {
        $arrayDecision = [];
        if ($this->getDecision() instanceof CommonDecisionEnveloppe) {
            $arrayDecision[0] = $this->getDecision();
            (new Atexo_Consultation_Responses())->addCompanyName($arrayDecision, null, Atexo_CurrentUser::getCurrentOrganism());
            $this->setDecision($arrayDecision[0]);
            $this->titulaireMarche->Text = $this->getDecision()->getNomEntreprise();
            //$this->numeroMarche->Text=$this->getDecision()->getNumeroMarche();
            $this->dateNotification->Text = Atexo_Util::iso2frnDate($this->getDecision()->getDateNotification());
            $this->objetMarche->Text = $this->getDecision()->getObjetMarche();
            $this->referenceUtilisateur->Text = $this->getReferenceUtilisateur();
        }
    }
}
