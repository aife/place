<div class="toggle-panel form-toggle donnees-compte-publicite">
    <com:TActivePanel cssClass="content" ID="blocDonneesComptePublicite">
        <div class="cover"></div>
        <a class="title-toggle-open" onclick="togglePanel(this,'#panel_donneesComptePub');" href="javascript:void(0);"><com:TTranslate>ADRESSES_CORRESPONDANTS_AVIS</com:TTranslate></a>
        <div class="panel no-indent panel-donnees-complementaires-compte-publicite" id="panel_donneesComptePub" style="display:block;">
            <fieldset class="panel panel-default collapsible with-bg">
                <legend><a role="button" class="collapsed" data-toggle="collapse" href="#panel_adresse_01"><com:TTranslate>DEFINE_ADRESSE_ORGANISME_QUI_PASSE_LE_MARCHE</com:TTranslate></a></legend>
                <div class="panel-body collapse" id="panel_adresse_01">
                    <div class="line">
                        <div class="intitule col-150"><label for="correspondant"><com:TTranslate>DEFINE_CORRESPONDANT</com:TTranslate></label><span class="champ-oblig">*</span> :</div>
                        <com:TActiveTextBox id="correspondant" cssClass="moyen" Attributes.title="<%=Prado::localize('DEFINE_CORRESPONDANT')%>" />
                        <com:TRequiredFieldValidator
                                ValidationGroup="<%=($this->getValidationGroupBtnValidate())%>"
                                Enabled="<%=($this->TemplateControl->getConsultation() instanceof CommonConsultation && $this->TemplateControl->getConsultation()->getDonneePubliciteObligatoire())%>"
                                ControlToValidate="correspondant"
                                Display="Dynamic"
                                ErrorMessage="<%=Prado::localize('DEFINE_MESSAGE_ERREUR_CORRESPONDANT_BLOC_ADRESSE_ORGANISME_QUI_PASSE_LE_MARCHE')%>"
                                EnableClientScript="true"
                                Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
                        >
                            <prop:ClientSide.OnValidate>
                                if(document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeIdentification_intitulePubliciteOui").checked) {
                                sender.enabled = true;
                                }else{
                                sender.enabled = false;
                                }
                            </prop:ClientSide.OnValidate>
                            <prop:ClientSide.OnValidationError>
                                blockErreur = '<%=($this->getValidationSummaryBtnValidate())%>';
                                if(document.getElementById(blockErreur))
                                document.getElementById(blockErreur).style.display='';
                                if(document.getElementById('ctl0_CONTENU_PAGE_blocBoutons'))
                                document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
                            </prop:ClientSide.OnValidationError>
                        </com:TRequiredFieldValidator>
                    </div>
                    <div class="line">
                        <div class="intitule col-150"><label for="nomOrganisme"><com:TTranslate>DEFINE_NOM_ORG</com:TTranslate></label><span class="champ-oblig">*</span> :</div>
                        <com:TActiveTextBox id="nomOrganisme1" cssClass="moyen" Attributes.title="<%=Prado::localize('DEFINE_NOM_ORG')%>" />
                        <com:TRequiredFieldValidator
                                ValidationGroup="<%=($this->getValidationGroupBtnValidate())%>"
                                Enabled="<%=($this->TemplateControl->getConsultation() instanceof CommonConsultation && $this->TemplateControl->getConsultation()->getDonneePubliciteObligatoire())%>"
                                ControlToValidate="nomOrganisme1"
                                Display="Dynamic"
                                ErrorMessage="<%=Prado::localize('DEFINE_MESSAGE_ERREUR_NOM_ORG_BLOC_ADRESSE_ORGANISME_QUI_PASSE_LE_MARCHE')%>"
                                EnableClientScript="true"
                                Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
                        >
                            <prop:ClientSide.OnValidate>
                                if(document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeIdentification_intitulePubliciteOui").checked) {
                                sender.enabled = true;
                                }else{
                                sender.enabled = false;
                                }
                            </prop:ClientSide.OnValidate>
                            <prop:ClientSide.OnValidationError>
                                blockErreur = '<%=($this->getValidationSummaryBtnValidate())%>';
                                if(document.getElementById(blockErreur))
                                document.getElementById(blockErreur).style.display='';
                                if(document.getElementById('ctl0_CONTENU_PAGE_blocBoutons'))
                                document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
                            </prop:ClientSide.OnValidationError>
                        </com:TRequiredFieldValidator>
                    </div>
                    <div class="line">
                        <div class="intitule col-150"><label for="adresseVoie"><com:TTranslate>TEXT_ADRESSE</com:TTranslate></label><span class="champ-oblig">*</span> :</div>
                        <com:TActiveTextBox id="adresseVoie1" cssClass="moyen" Attributes.title="<%=Prado::localize('TEXT_ADRESSE')%>" />
                        <com:TRequiredFieldValidator
                                ValidationGroup="<%=($this->getValidationGroupBtnValidate())%>"
                                Enabled="<%=($this->TemplateControl->getConsultation() instanceof CommonConsultation && $this->TemplateControl->getConsultation()->getDonneePubliciteObligatoire())%>"
                                ControlToValidate="adresseVoie1"
                                Display="Dynamic"
                                ErrorMessage="<%=Prado::localize('DEFINE_MESSAGE_ERREUR_ADRESSE_BLOC_ADRESSE_ORGANISME_QUI_PASSE_LE_MARCHE')%>"
                                EnableClientScript="true"
                                Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
                        >
                            <prop:ClientSide.OnValidate>
                                if(document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeIdentification_intitulePubliciteOui").checked) {
                                sender.enabled = true;
                                }else{
                                sender.enabled = false;
                                }
                            </prop:ClientSide.OnValidate>
                            <prop:ClientSide.OnValidationError>
                                blockErreur = '<%=($this->getValidationSummaryBtnValidate())%>';
                                if(document.getElementById(blockErreur))
                                document.getElementById(blockErreur).style.display='';
                                if(document.getElementById('ctl0_CONTENU_PAGE_blocBoutons'))
                                document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
                            </prop:ClientSide.OnValidationError>
                        </com:TRequiredFieldValidator>
                    </div>

                    <div class="line">
                        <div class="intitule col-150"><label for="adresseCP1"><com:TTranslate>CODE_POSTAL</com:TTranslate> / <com:TTranslate>TEXT_VILLE</com:TTranslate></label><span class="champ-oblig">*</span> :</div>
                        <com:TActiveTextBox id="adresseCP1" cssClass="cp" Attributes.title="<%=Prado::localize('CODE_POSTAL')%>" />
                        <com:TActiveTextBox id="adresseVille1" cssClass="ville" Attributes.title="<%=Prado::localize('TEXT_VILLE')%>" />
                        <com:TRequiredFieldValidator
                                ValidationGroup="<%=($this->getValidationGroupBtnValidate())%>"
                                Enabled="<%=($this->TemplateControl->getConsultation() instanceof CommonConsultation && $this->TemplateControl->getConsultation()->getDonneePubliciteObligatoire())%>"
                                ControlToValidate="adresseCP1"
                                Display="Dynamic"
                                EnableClientScript="true"
                        >
                            <prop:ClientSide.OnValidate>
                                if(document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeIdentification_intitulePubliciteOui").checked) {
                                sender.enabled = true;
                                }else{
                                sender.enabled = false;
                                }
                            </prop:ClientSide.OnValidate>
                            <prop:ClientSide.OnValidationError>
                                blockErreur = '<%=($this->getValidationSummaryBtnValidate())%>';
                                if(document.getElementById(blockErreur))
                                document.getElementById(blockErreur).style.display='';
                                if(document.getElementById('ctl0_CONTENU_PAGE_blocBoutons'))
                                document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
                            </prop:ClientSide.OnValidationError>
                        </com:TRequiredFieldValidator>
                        <com:TRequiredFieldValidator
                                ValidationGroup="<%=($this->getValidationGroupBtnValidate())%>"
                                Enabled="<%=($this->TemplateControl->getConsultation() instanceof CommonConsultation && $this->TemplateControl->getConsultation()->getDonneePubliciteObligatoire())%>"
                                ControlToValidate="adresseVille1"
                                Display="Dynamic"
                                EnableClientScript="true"
                        >
                            <prop:ClientSide.OnValidate>
                                if(document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeIdentification_intitulePubliciteOui").checked) {
                                sender.enabled = true;
                                }else{
                                sender.enabled = false;
                                }
                            </prop:ClientSide.OnValidate>
                            <prop:ClientSide.OnValidationError>
                                blockErreur = '<%=($this->getValidationSummaryBtnValidate())%>';
                                if(document.getElementById(blockErreur))
                                document.getElementById(blockErreur).style.display='';
                                if(document.getElementById('ctl0_CONTENU_PAGE_blocBoutons'))
                                document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
                            </prop:ClientSide.OnValidationError>
                        </com:TRequiredFieldValidator>
                        <com:TCustomValidator
                                ValidationGroup="<%=($this->getValidationGroupBtnValidate())%>"
                                Enabled="true"
                                ControlToValidate="adresseCP1"
                                ClientValidationFunction="zipCodeValidation"
                                ErrorMessage="<%=Prado::localize('DEFINE_MESSAGE_ERREUR_CODE_POSTAL_BLOC_ADRESSE_ORGANISME_QUI_PASSE_LE_MARCHE')%>"
                                Display="Dynamic"
                                Text="<span title='<%=Prado::localize('CHAMP_OBLIGATOIRE_POSTAL_CODE_FORMAT_ERROR')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMP_OBLIGATOIRE_POSTAL_CODE_FORMAT_ERROR')%>' /></span>"
                        >
                            <prop:ClientSide.OnValidate>
                                if(document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeIdentification_intitulePubliciteOui").checked) {
                                sender.enabled = true;
                                }else{
                                sender.enabled = false;
                                }
                            </prop:ClientSide.OnValidate>
                            <prop:ClientSide.OnValidationError>
                                blockErreur = '<%=($this->getValidationSummaryBtnValidate())%>';
                                if(document.getElementById(blockErreur))
                                document.getElementById(blockErreur).style.display='';
                                if(document.getElementById('ctl0_CONTENU_PAGE_blocBoutons'))
                                document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
                            </prop:ClientSide.OnValidationError>
                        </com:TCustomValidator>

                        <com:TCustomValidator
                                ValidationGroup="<%=($this->getValidationGroupBtnValidate())%>"
                                Enabled="true"
                                ControlToValidate="adresseVille1"
                                ClientValidationFunction="cityValidation"
                                ErrorMessage="<%=Prado::localize('DEFINE_MESSAGE_ERREUR_VILLE_BLOC_ADRESSE_ORGANISME_QUI_PASSE_LE_MARCHE')%>"
                                Display="Dynamic"
                                Text="<span title='<%=Prado::localize('CHAMP_OBLIGATOIRE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMP_OBLIGATOIRE')%>' /></span>"
                        >
                            <prop:ClientSide.OnValidate>
                                if(document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeIdentification_intitulePubliciteOui").checked) {
                                sender.enabled = true;
                                }else{
                                sender.enabled = false;
                                }
                            </prop:ClientSide.OnValidate>
                            <prop:ClientSide.OnValidationError>
                                blockErreur = '<%=($this->getValidationSummaryBtnValidate())%>';
                                if(document.getElementById(blockErreur))
                                document.getElementById(blockErreur).style.display='';
                                if(document.getElementById('ctl0_CONTENU_PAGE_blocBoutons'))
                                document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
                            </prop:ClientSide.OnValidationError>
                        </com:TCustomValidator>
                    </div>

                    <div class="line">
                        <div class="intitule col-150"><label for="telephone1"><com:TTranslate>TEXT_TELEPHONE</com:TTranslate></label><span class="champ-oblig">*</span> :</div>
                        <com:TActiveTextBox id="telephone1" cssClass="moyen" Attributes.title="<%=Prado::localize('TEXT_TELEPHONE')%>" />
                        <com:TCustomValidator
                                ControlToValidate="telephone1"
                                ValidationGroup="<%=($this->getValidationGroupBtnValidate())%>"
                                Display="Dynamic"
                                EnableClientScript="true"
                                Enabled="<%=($this->TemplateControl->getConsultation() instanceof CommonConsultation && $this->TemplateControl->getConsultation()->getDonneePubliciteObligatoire())%>"
                                ID="telephoneValidator"
                                ErrorMessage="<%=Prado::localize('DEFINE_MESSAGE_ERREUR_TELEPHONE_BLOC_ADRESSE_ORGANISME_QUI_PASSE_LE_MARCHE')%>"
                                ClientValidationFunction="validatorTelephone"
                                Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span> ">
                            <prop:ClientSide.OnValidate>
                                if(document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeIdentification_intitulePubliciteOui").checked) {
                                sender.enabled = true;
                                }else{
                                sender.enabled = false;
                                }
                            </prop:ClientSide.OnValidate>
                            <prop:ClientSide.OnValidationError>
                                blockErreur = '<%=($this->getValidationSummaryBtnValidate())%>';
                                if(document.getElementById(blockErreur))
                                document.getElementById(blockErreur).style.display='';
                                if(document.getElementById('ctl0_CONTENU_PAGE_blocBoutons'))
                                document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
                            </prop:ClientSide.OnValidationError>
                        </com:TCustomValidator>
                    </div>

                    <div class="line">
                        <div class="intitule col-150"><label for="email1"><com:TTranslate>TEXT_EMAIL</com:TTranslate></label><span class="champ-oblig">*</span> :</div>
                        <com:TActiveTextBox id="email1" cssClass="moyen" Attributes.title="<%=Prado::localize('TEXT_EMAIL')%>" />
                        <com:TCustomValidator
                                ControlToValidate="email1"
                                ValidationGroup="<%=($this->getValidationGroupBtnValidate())%>"
                                Display="Dynamic"
                                EnableClientScript="true"
                                Enabled="<%=($this->TemplateControl->getConsultation() instanceof CommonConsultation && $this->TemplateControl->getConsultation()->getDonneePubliciteObligatoire())%>"
                                ID="emailValidator"
                                ErrorMessage="<%=Prado::localize('DEFINE_MESSAGE_ERREUR_EMAIL_BLOC_ADRESSE_ORGANISME_QUI_PASSE_LE_MARCHE')%>"
                                ClientValidationFunction="validatorEmail"
                                Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span> ">
                            <prop:ClientSide.OnValidate>
                                if(document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeIdentification_intitulePubliciteOui").checked) {
                                sender.enabled = true;
                                }else{
                                sender.enabled = false;
                                }
                            </prop:ClientSide.OnValidate>
                            <prop:ClientSide.OnValidationError>
                                blockErreur = '<%=($this->getValidationSummaryBtnValidate())%>';
                                if(document.getElementById(blockErreur))
                                document.getElementById(blockErreur).style.display='';
                                if(document.getElementById('ctl0_CONTENU_PAGE_blocBoutons'))
                                document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
                            </prop:ClientSide.OnValidationError>
                        </com:TCustomValidator>

                    </div>
                    <div class="line">
                        <div class="intitule col-150"><label for="adresseURL"><com:TTranslate>DEFINE_URL</com:TTranslate></label><span class="champ-oblig">*</span> :</div>
                        <com:TActiveTextBox id="adresseURL1" cssClass="moyen pull-left" Attributes.title="<%=Prado::localize('DEFINE_URL')%>" />
                        <div class="col-info-bulle">
                            <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" onmouseover="afficheBulle('adresseURL', this)" onmouseout="cacheBulle('adresseURL')" class="picto-info" alt="Info-bulle" title="Info-bulle">
                            <div onmouseout="mouseOutBulle();" onmouseover="mouseOverBulle();" class="info-bulle" id="adresseURL"><div><com:TTranslate>INFO_BULLE_URL_FIELD</com:TTranslate></div></div>
                        </div>
                        <com:TRequiredFieldValidator
                                ValidationGroup="<%=($this->getValidationGroupBtnValidate())%>"
                                Enabled="<%=($this->TemplateControl->getConsultation() instanceof CommonConsultation && $this->TemplateControl->getConsultation()->getDonneePubliciteObligatoire())%>"
                                ControlToValidate="adresseURL1"
                                Display="Dynamic"
                                ErrorMessage="<%=Prado::localize('DEFINE_MESSAGE_ERREUR_URL_BLOC_ADRESSE_ORGANISME_QUI_PASSE_LE_MARCHE')%>"
                                EnableClientScript="true"
                                Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
                        >
                            <prop:ClientSide.OnValidate>
                                if(document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeIdentification_intitulePubliciteOui").checked) {
                                sender.enabled = true;
                                }else{
                                sender.enabled = false;
                                }
                            </prop:ClientSide.OnValidate>
                            <prop:ClientSide.OnValidationError>
                                blockErreur = '<%=($this->getValidationSummaryBtnValidate())%>';
                                if(document.getElementById(blockErreur))
                                document.getElementById(blockErreur).style.display='';
                                if(document.getElementById('ctl0_CONTENU_PAGE_blocBoutons'))
                                document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
                            </prop:ClientSide.OnValidationError>
                            <com:TRegularExpressionValidator
                                    ID="adresseURLControl1"
                                    ValidationGroup="<%=($this->getValidationGroupBtnValidate())%>"
                                    ControlToValidate="adresseURL1"
                                    Display="Dynamic"
                                    EnableClientScript="true"
                                    RegularExpression="(https?):\/\/[a-z0-9\/:%_+.,#?!@&=-]+"
                                    ErrorMessage="<%=Prado::localize('DEFINE_URL') .': '.Prado::localize('FORMAT_INCORRECTE')%>"
                                    Text="<span title='<%=Prado::localize('FORMAT_INCORRECTE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('FORMAT_INCORRECTE')%>' /></span>">
                                <prop:ClientSide.OnValidationError>
                                    blockErreur = '<%=($this->getValidationSummaryBtnValidate())%>';
                                    if(document.getElementById(blockErreur))
                                    document.getElementById(blockErreur).style.display='';
                                    if(document.getElementById('ctl0_CONTENU_PAGE_blocBoutons'))
                                    document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
                                </prop:ClientSide.OnValidationError>
                            </com:TRegularExpressionValidator>
                        </com:TRequiredFieldValidator>
                        <com:TCustomValidator
                                ValidationGroup="<%=($this->getValidationGroupBtnValidate())%>"
                                Enabled="true"
                                ControlToValidate="adresseURL1"
                                ClientValidationFunction="urlValidator"
                                ErrorMessage="<%=Prado::localize('DEFINE_MESSAGE_ERREUR_URL_BLOC_ADRESSE_ORGANISME_QUI_PASSE_LE_MARCHE')%>"
                                Display="Dynamic"
                                Text="<span title='<%=Prado::localize('FORMAT_INCORRECTE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('FORMAT_INCORRECTE')%>' /></span>">
                            >
                            <prop:ClientSide.OnValidate>
                                if(document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeIdentification_intitulePubliciteOui").checked) {
                                sender.enabled = true;
                                }else{
                                sender.enabled = false;
                                }
                            </prop:ClientSide.OnValidate>
                            <prop:ClientSide.OnValidationError>
                                blockErreur = '<%=($this->getValidationSummaryBtnValidate())%>';
                                if(document.getElementById(blockErreur))
                                document.getElementById(blockErreur).style.display='';
                                if(document.getElementById('ctl0_CONTENU_PAGE_blocBoutons'))
                                document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
                            </prop:ClientSide.OnValidationError>
                        </com:TCustomValidator>
                    </div>
                </div>
            </fieldset>
            <fieldset class="panel panel-default collapsible with-bg">
                <legend><a role="button" class="collapsed" data-toggle="collapse" href="#panel_adresse_02"><com:TTranslate>TEXT_ADRESSE_INSTANCE_CHARGEE_DES_PROCEDURES_DE_RECOURS</com:TTranslate></a></legend>
                <div class="panel-body collapse" id="panel_adresse_02">
                    <div class="line">
                        <div class="intitule col-150"><label for="nomOrganisme2"><com:TTranslate>DEFINE_NOM_ORG</com:TTranslate></label><span class="champ-oblig">*</span> :</div>
                        <com:TActiveTextBox id="nomOrganisme2" cssClass="moyen" Attributes.title="<%=Prado::localize('DEFINE_NOM_ORG')%>" />
                        <com:TRequiredFieldValidator
                                ValidationGroup="<%=($this->getValidationGroupBtnValidate())%>"
                                Enabled="<%=($this->TemplateControl->getConsultation() instanceof CommonConsultation && $this->TemplateControl->getConsultation()->getDonneePubliciteObligatoire())%>"
                                ControlToValidate="nomOrganisme2"
                                Display="Dynamic"
                                ErrorMessage="<%=Prado::localize('DEFINE_MESSAGE_ERREUR_NOM_ORG_BLOC_ADRESSE_INSTANCE_CHARGEE_PROCEDURES_RECOURS')%>"
                                EnableClientScript="true"
                                Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
                        >
                            <prop:ClientSide.OnValidate>
                                if(document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeIdentification_intitulePubliciteOui").checked) {
                                sender.enabled = true;
                                }else{
                                sender.enabled = false;
                                }
                            </prop:ClientSide.OnValidate>
                            <prop:ClientSide.OnValidationError>
                                blockErreur = '<%=($this->getValidationSummaryBtnValidate())%>';
                                if(document.getElementById(blockErreur))
                                document.getElementById(blockErreur).style.display='';
                                if(document.getElementById('ctl0_CONTENU_PAGE_blocBoutons'))
                                document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
                            </prop:ClientSide.OnValidationError>
                        </com:TRequiredFieldValidator>
                    </div>
                    <div class="line">
                        <div class="intitule col-150"><label for="adresseVoie2"><com:TTranslate>TEXT_ADRESSE</com:TTranslate></label><span class="champ-oblig">*</span> :</div>
                        <com:TActiveTextBox id="adresseVoie2" cssClass="moyen" Attributes.title="<%=Prado::localize('TEXT_ADRESSE')%>" />
                        <com:TRequiredFieldValidator
                                ValidationGroup="<%=($this->getValidationGroupBtnValidate())%>"
                                Enabled="<%=($this->TemplateControl->getConsultation() instanceof CommonConsultation && $this->TemplateControl->getConsultation()->getDonneePubliciteObligatoire())%>"
                                ControlToValidate="adresseVoie2"
                                Display="Dynamic"
                                ErrorMessage="<%=Prado::localize('DEFINE_MESSAGE_ERREUR_ADRESSE_BLOC_ADRESSE_INSTANCE_CHARGEE_PROCEDURES_RECOURS')%>"
                                EnableClientScript="true"
                                Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
                        >
                            <prop:ClientSide.OnValidate>
                                if(document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeIdentification_intitulePubliciteOui").checked) {
                                sender.enabled = true;
                                }else{
                                sender.enabled = false;
                                }
                            </prop:ClientSide.OnValidate>
                            <prop:ClientSide.OnValidationError>
                                blockErreur = '<%=($this->getValidationSummaryBtnValidate())%>';
                                if(document.getElementById(blockErreur))
                                document.getElementById(blockErreur).style.display='';
                                if(document.getElementById('ctl0_CONTENU_PAGE_blocBoutons'))
                                document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
                            </prop:ClientSide.OnValidationError>
                        </com:TRequiredFieldValidator>
                    </div>
                    <div class="line">
                        <div class="intitule col-150"><label for="adresseCP2"><com:TTranslate>CODE_POSTAL</com:TTranslate> / <com:TTranslate>TEXT_VILLE</com:TTranslate></label><span class="champ-oblig">*</span> :</div>
                        <com:TActiveTextBox id="adresseCP2" cssClass="cp" Attributes.title="<%=Prado::localize('CODE_POSTAL')%>" />
                        <com:TActiveTextBox id="adresseVille2" cssClass="ville" Attributes.title="<%=Prado::localize('TEXT_VILLE')%>" />
                        <com:TRequiredFieldValidator
                                ValidationGroup="<%=($this->getValidationGroupBtnValidate())%>"
                                Enabled="<%=($this->TemplateControl->getConsultation() instanceof CommonConsultation && $this->TemplateControl->getConsultation()->getDonneePubliciteObligatoire())%>"
                                ControlToValidate="adresseCP2"
                                Display="Dynamic"
                                EnableClientScript="true"
                        >
                            <prop:ClientSide.OnValidate>
                                if(document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeIdentification_intitulePubliciteOui").checked) {
                                sender.enabled = true;
                                }else{
                                sender.enabled = false;
                                }
                            </prop:ClientSide.OnValidate>
                            <prop:ClientSide.OnValidationError>
                                blockErreur = '<%=($this->getValidationSummaryBtnValidate())%>';
                                if(document.getElementById(blockErreur))
                                document.getElementById(blockErreur).style.display='';
                                if(document.getElementById('ctl0_CONTENU_PAGE_blocBoutons'))
                                document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
                            </prop:ClientSide.OnValidationError>
                        </com:TRequiredFieldValidator>
                        <com:TRequiredFieldValidator
                                ValidationGroup="<%=($this->getValidationGroupBtnValidate())%>"
                                Enabled="<%=($this->TemplateControl->getConsultation() instanceof CommonConsultation && $this->TemplateControl->getConsultation()->getDonneePubliciteObligatoire())%>"
                                ControlToValidate="adresseVille2"
                                Display="Dynamic"
                                EnableClientScript="true"
                        >
                            <prop:ClientSide.OnValidate>
                                if(document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeIdentification_intitulePubliciteOui").checked) {
                                sender.enabled = true;
                                }else{
                                sender.enabled = false;
                                }
                            </prop:ClientSide.OnValidate>
                            <prop:ClientSide.OnValidationError>
                                blockErreur = '<%=($this->getValidationSummaryBtnValidate())%>';
                                if(document.getElementById(blockErreur))
                                document.getElementById(blockErreur).style.display='';
                                if(document.getElementById('ctl0_CONTENU_PAGE_blocBoutons'))
                                document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
                            </prop:ClientSide.OnValidationError>
                        </com:TRequiredFieldValidator>
                        <com:TCustomValidator
                                ValidationGroup="<%=($this->getValidationGroupBtnValidate())%>"
                                Enabled="true"
                                ControlToValidate="adresseCP2"
                                ClientValidationFunction="zipCodeValidation"
                                ErrorMessage="<%=Prado::localize('DEFINE_MESSAGE_ERREUR_CODE_POSTAL_BLOC_ADRESSE_INSTANCE_CHARGEE_PROCEDURES_RECOURS')%>"
                                Display="Dynamic"
                                Text="<span title='<%=Prado::localize('CHAMP_OBLIGATOIRE_POSTAL_CODE_FORMAT_ERROR')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMP_OBLIGATOIRE_POSTAL_CODE_FORMAT_ERROR')%>' /></span>"
                        >
                            <prop:ClientSide.OnValidate>
                                if(document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeIdentification_intitulePubliciteOui").checked) {
                                sender.enabled = true;
                                }else{
                                sender.enabled = false;
                                }
                            </prop:ClientSide.OnValidate>
                            <prop:ClientSide.OnValidationError>
                                blockErreur = '<%=($this->getValidationSummaryBtnValidate())%>';
                                if(document.getElementById(blockErreur))
                                document.getElementById(blockErreur).style.display='';
                                if(document.getElementById('ctl0_CONTENU_PAGE_blocBoutons'))
                                document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
                            </prop:ClientSide.OnValidationError>
                        </com:TCustomValidator>
                        <com:TCustomValidator
                                ValidationGroup="<%=($this->getValidationGroupBtnValidate())%>"
                                Enabled="true"
                                ControlToValidate="adresseVille2"
                                ClientValidationFunction="cityValidation"
                                ErrorMessage="<%=Prado::localize('DEFINE_MESSAGE_ERREUR_VILLE_BLOC_ADRESSE_INSTANCE_CHARGEE_PROCEDURES_RECOURS')%>"
                                Display="Dynamic"
                                Text="<span title='<%=Prado::localize('FORMAT_INCORRECTE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('FORMAT_INCORRECTE')%>' /></span>"
                        >
                            <prop:ClientSide.OnValidate>
                                if(document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeIdentification_intitulePubliciteOui").checked) {
                                sender.enabled = true;
                                }else{
                                sender.enabled = false;
                                }
                            </prop:ClientSide.OnValidate>
                            <prop:ClientSide.OnValidationError>
                                blockErreur = '<%=($this->getValidationSummaryBtnValidate())%>';
                                if(document.getElementById(blockErreur))
                                document.getElementById(blockErreur).style.display='';
                                if(document.getElementById('ctl0_CONTENU_PAGE_blocBoutons'))
                                document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
                            </prop:ClientSide.OnValidationError>
                        </com:TCustomValidator>
                    </div>
                    <div class="line">
                        <div class="intitule col-150"><label for="adresseURL2"><com:TTranslate>DEFINE_URL</com:TTranslate></label><span class="champ-oblig">*</span> :</div>
                        <com:TActiveTextBox id="adresseURL2" cssClass="moyen pull-left" Attributes.title="<%=Prado::localize('DEFINE_URL')%>" />
                        <div class="col-info-bulle">
                            <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" onmouseover="afficheBulle('adresseURL2', this)" onmouseout="cacheBulle('adresseURL2')" class="picto-info" alt="Info-bulle" title="Info-bulle">
                            <div onmouseout="mouseOutBulle();" onmouseover="mouseOverBulle();" class="info-bulle" id="adresseURL2"><div><com:TTranslate>INFO_BULLE_URL_FIELD</com:TTranslate></div></div>
                        </div>
                        <com:TRequiredFieldValidator
                                ValidationGroup="<%=($this->getValidationGroupBtnValidate())%>"
                                Enabled="<%=($this->TemplateControl->getConsultation() instanceof CommonConsultation && $this->TemplateControl->getConsultation()->getDonneePubliciteObligatoire())%>"
                                ControlToValidate="adresseURL2"
                                Display="Dynamic"
                                ErrorMessage="<%=Prado::localize('DEFINE_MESSAGE_ERREUR_URL_BLOC_ADRESSE_INSTANCE_CHARGEE_PROCEDURES_RECOURS')%>"
                                EnableClientScript="true"
                                Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
                        >
                            <prop:ClientSide.OnValidate>
                                if(document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeIdentification_intitulePubliciteOui").checked) {
                                sender.enabled = true;
                                }else{
                                sender.enabled = false;
                                }
                            </prop:ClientSide.OnValidate>
                            <prop:ClientSide.OnValidationError>
                                blockErreur = '<%=($this->getValidationSummaryBtnValidate())%>';
                                if(document.getElementById(blockErreur))
                                document.getElementById(blockErreur).style.display='';
                                if(document.getElementById('ctl0_CONTENU_PAGE_blocBoutons'))
                                document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
                            </prop:ClientSide.OnValidationError>
                            <com:TRegularExpressionValidator
                                    ID="adresseURLControl2"
                                    ValidationGroup="<%=($this->getValidationGroupBtnValidate())%>"
                                    ControlToValidate="adresseURL2"
                                    Display="Dynamic"
                                    EnableClientScript="true"
                                    RegularExpression="(https?):\/\/[a-z0-9\/:%_+.,#?!@&=-]+"
                                    ErrorMessage="<%=Prado::localize('DEFINE_URL') .': '.Prado::localize('FORMAT_INCORRECTE')%>"
                                    Text="<span title='<%=Prado::localize('FORMAT_INCORRECTE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('FORMAT_INCORRECTE')%>' /></span>">
                                <prop:ClientSide.OnValidationError>
                                    blockErreur = '<%=($this->getValidationSummaryBtnValidate())%>';
                                    if(document.getElementById(blockErreur))
                                    document.getElementById(blockErreur).style.display='';
                                    if(document.getElementById('ctl0_CONTENU_PAGE_blocBoutons'))
                                    document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
                                </prop:ClientSide.OnValidationError>
                            </com:TRegularExpressionValidator>
                        </com:TRequiredFieldValidator>

                        <com:TCustomValidator
                                ValidationGroup="<%=($this->getValidationGroupBtnValidate())%>"
                                Enabled="true"
                                ControlToValidate="adresseURL2"
                                ClientValidationFunction="urlValidator"
                                ErrorMessage="<%=Prado::localize('DEFINE_MESSAGE_ERREUR_URL_BLOC_ADRESSE_INSTANCE_CHARGEE_PROCEDURES_RECOURS')%>"
                                Display="Dynamic"
                                Text="<span title='<%=Prado::localize('FORMAT_INCORRECTE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('FORMAT_INCORRECTE')%>' /></span>">
                            >
                            <prop:ClientSide.OnValidate>
                                if(document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeIdentification_intitulePubliciteOui").checked) {
                                sender.enabled = true;
                                }else{
                                sender.enabled = false;
                                }
                            </prop:ClientSide.OnValidate>
                            <prop:ClientSide.OnValidationError>
                                blockErreur = '<%=($this->getValidationSummaryBtnValidate())%>';
                                if(document.getElementById(blockErreur))
                                document.getElementById(blockErreur).style.display='';
                                if(document.getElementById('ctl0_CONTENU_PAGE_blocBoutons'))
                                document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
                            </prop:ClientSide.OnValidationError>
                        </com:TCustomValidator>
                    </div>
                </div>
            </fieldset>
            <fieldset class="panel panel-default collapsible with-bg">
                <legend><a role="button" class="collapsed" data-toggle="collapse" href="#panel_adresse_03"><com:TTranslate>DEFINE_ADRESSE_FACTURATION_PUBLICITE</com:TTranslate> </a></legend>
                <div class="panel-body collapse" id="panel_adresse_03">
                    <div class="line">
                        <div class="intitule col-150"><label for="nomOrganisme3"><com:TTranslate>DEFINE_NOM_ORG</com:TTranslate></label><span class="champ-oblig">*</span> :</div>
                        <com:TActiveTextBox id="nomOrganisme3" cssClass="moyen" Attributes.title="<%=Prado::localize('DEFINE_NOM_ORG')%>" />
                        <com:TRequiredFieldValidator
                                ValidationGroup="<%=($this->getValidationGroupBtnValidate())%>"
                                Enabled="<%=($this->TemplateControl->getConsultation() instanceof CommonConsultation && $this->TemplateControl->getConsultation()->getDonneePubliciteObligatoire())%>"
                                ControlToValidate="nomOrganisme3"
                                Display="Dynamic"
                                ErrorMessage="<%=Prado::localize('DEFINE_MESSAGE_ERREUR_NOM_ORG_BLOC_ADRESSE_DE_FACTURATION_PUBLICITE')%>"
                                EnableClientScript="true"
                                Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
                        >
                            <prop:ClientSide.OnValidate>
                                if(document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeIdentification_intitulePubliciteOui").checked) {
                                sender.enabled = true;
                                }else{
                                sender.enabled = false;
                                }
                            </prop:ClientSide.OnValidate>
                            <prop:ClientSide.OnValidationError>
                                blockErreur = '<%=($this->getValidationSummaryBtnValidate())%>';
                                if(document.getElementById(blockErreur))
                                document.getElementById(blockErreur).style.display='';
                                if(document.getElementById('ctl0_CONTENU_PAGE_blocBoutons'))
                                document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
                            </prop:ClientSide.OnValidationError>
                        </com:TRequiredFieldValidator>
                    </div>
                    <div class="line">
                        <div class="intitule col-150"><label for="adresseVoie3"><com:TTranslate>TEXT_ADRESSE</com:TTranslate></label><span class="champ-oblig">*</span> :</div>
                        <com:TActiveTextBox id="adresseVoie3" cssClass="moyen" Attributes.title="<%=Prado::localize('TEXT_ADRESSE')%>" />
                        <com:TRequiredFieldValidator
                                ValidationGroup="<%=($this->getValidationGroupBtnValidate())%>"
                                Enabled="<%=($this->TemplateControl->getConsultation() instanceof CommonConsultation && $this->TemplateControl->getConsultation()->getDonneePubliciteObligatoire())%>"
                                ControlToValidate="adresseVoie3"
                                Display="Dynamic"
                                ErrorMessage="<%=Prado::localize('DEFINE_MESSAGE_ERREUR_ADRESSE_BLOC_ADRESSE_DE_FACTURATION_PUBLICITE')%>"
                                EnableClientScript="true"
                                Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
                        >
                            <prop:ClientSide.OnValidate>
                                if(document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeIdentification_intitulePubliciteOui").checked) {
                                sender.enabled = true;
                                }else{
                                sender.enabled = false;
                                }
                            </prop:ClientSide.OnValidate>
                            <prop:ClientSide.OnValidationError>
                                blockErreur = '<%=($this->getValidationSummaryBtnValidate())%>';
                                if(document.getElementById(blockErreur))
                                document.getElementById(blockErreur).style.display='';
                                if(document.getElementById('ctl0_CONTENU_PAGE_blocBoutons'))
                                document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
                            </prop:ClientSide.OnValidationError>
                        </com:TRequiredFieldValidator>
                    </div>
                    <div class="line">
                        <div class="intitule col-150"><label for="adresseCP3"><com:TTranslate>CODE_POSTAL</com:TTranslate> / <com:TTranslate>TEXT_VILLE</com:TTranslate></label><span class="champ-oblig">*</span> :</div>
                        <com:TActiveTextBox id="adresseCP3" cssClass="cp" Attributes.title="<%=Prado::localize('CODE_POSTAL')%>" />
                        <com:TActiveTextBox id="adresseVille3" cssClass="ville" Attributes.title="<%=Prado::localize('TEXT_VILLE')%>" />
                        <com:TRequiredFieldValidator
                                ValidationGroup="<%=($this->getValidationGroupBtnValidate())%>"
                                Enabled="<%=($this->TemplateControl->getConsultation() instanceof CommonConsultation && $this->TemplateControl->getConsultation()->getDonneePubliciteObligatoire())%>"
                                ControlToValidate="adresseCP3"
                                Display="Dynamic"
                                EnableClientScript="true"
                        >
                            <prop:ClientSide.OnValidate>
                                if(document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeIdentification_intitulePubliciteOui").checked) {
                                sender.enabled = true;
                                }else{
                                sender.enabled = false;
                                }
                            </prop:ClientSide.OnValidate>
                            <prop:ClientSide.OnValidationError>
                                blockErreur = '<%=($this->getValidationSummaryBtnValidate())%>';
                                if(document.getElementById(blockErreur))
                                document.getElementById(blockErreur).style.display='';
                                if(document.getElementById('ctl0_CONTENU_PAGE_blocBoutons'))
                                document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
                            </prop:ClientSide.OnValidationError>
                        </com:TRequiredFieldValidator>
                        <com:TRequiredFieldValidator
                                ValidationGroup="<%=($this->getValidationGroupBtnValidate())%>"
                                Enabled="<%=($this->TemplateControl->getConsultation() instanceof CommonConsultation && $this->TemplateControl->getConsultation()->getDonneePubliciteObligatoire())%>"
                                ControlToValidate="adresseVille3"
                                Display="Dynamic"
                                EnableClientScript="true"
                        >
                            <prop:ClientSide.OnValidate>
                                if(document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeIdentification_intitulePubliciteOui").checked) {
                                sender.enabled = true;
                                }else{
                                sender.enabled = false;
                                }
                            </prop:ClientSide.OnValidate>
                            <prop:ClientSide.OnValidationError>
                                blockErreur = '<%=($this->getValidationSummaryBtnValidate())%>';
                                if(document.getElementById(blockErreur))
                                document.getElementById(blockErreur).style.display='';
                                if(document.getElementById('ctl0_CONTENU_PAGE_blocBoutons'))
                                document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
                            </prop:ClientSide.OnValidationError>
                        </com:TRequiredFieldValidator>
                        <com:TCustomValidator
                                ValidationGroup="<%=($this->getValidationGroupBtnValidate())%>"
                                Enabled="true"
                                ControlToValidate="adresseCP3"
                                ClientValidationFunction="zipCodeValidation"
                                ErrorMessage="<%=Prado::localize('DEFINE_MESSAGE_ERREUR_CODE_POSTAL_BLOC_ADRESSE_DE_FACTURATION_PUBLICITE')%>"
                                Display="Dynamic"
                                Text="<span title='<%=Prado::localize('CHAMP_OBLIGATOIRE_POSTAL_CODE_FORMAT_ERROR')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMP_OBLIGATOIRE_POSTAL_CODE_FORMAT_ERROR')%>' /></span>"
                        >
                            <prop:ClientSide.OnValidate>
                                if(document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeIdentification_intitulePubliciteOui").checked) {
                                sender.enabled = true;
                                }else{
                                sender.enabled = false;
                                }
                            </prop:ClientSide.OnValidate>
                            <prop:ClientSide.OnValidationError>
                                blockErreur = '<%=($this->getValidationSummaryBtnValidate())%>';
                                if(document.getElementById(blockErreur))
                                document.getElementById(blockErreur).style.display='';
                                if(document.getElementById('ctl0_CONTENU_PAGE_blocBoutons'))
                                document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
                            </prop:ClientSide.OnValidationError>
                        </com:TCustomValidator>
                        <com:TCustomValidator
                                ValidationGroup="<%=($this->getValidationGroupBtnValidate())%>"
                                Enabled="true"
                                ControlToValidate="adresseVille3"
                                ClientValidationFunction="cityValidation"
                                ErrorMessage="<%=Prado::localize('DEFINE_MESSAGE_ERREUR_VILLE_BLOC_ADRESSE_DE_FACTURATION_PUBLICITE')%>"
                                Display="Dynamic"
                                Text="<span title='<%=Prado::localize('FORMAT_INCORRECTE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('FORMAT_INCORRECTE')%>' /></span>"
                        >
                            <prop:ClientSide.OnValidate>
                                if(document.getElementById("ctl0_CONTENU_PAGE_bloc_etapeIdentification_intitulePubliciteOui").checked) {
                                sender.enabled = true;
                                }else{
                                sender.enabled = false;
                                }
                            </prop:ClientSide.OnValidate>
                            <prop:ClientSide.OnValidationError>
                                blockErreur = '<%=($this->getValidationSummaryBtnValidate())%>';
                                if(document.getElementById(blockErreur))
                                document.getElementById(blockErreur).style.display='';
                                if(document.getElementById('ctl0_CONTENU_PAGE_blocBoutons'))
                                document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
                            </prop:ClientSide.OnValidationError>
                        </com:TCustomValidator>
                    </div>
                </div>
            </fieldset>
            <!--Fin Ligne Compte de publicite-->
        </div>
        <div class="breaker"></div>
    </com:TActivePanel>
</div>
<script type="text/javascript">
    function zipCodeValidation(sender, parameter) {
        const regex = /\d{2}[ ]?\d{3}/gm;
        return (parameter.match(regex)) ? true : false;
    }
    function cityValidation(sender, parameter) {
        const regex = /^[\sa-zA-Z0-9\.-]{3,100}$/gm;
        return (parameter.match(regex)) ? true : false;
    }
    function urlValidator(sender, parameter)
    {
        const regex = /\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i;
        return (parameter.match(regex)) ? true : false;
    }

</script>
