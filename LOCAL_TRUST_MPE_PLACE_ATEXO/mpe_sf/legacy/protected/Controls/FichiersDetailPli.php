<?php

namespace Application\Controls;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonEnveloppe;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;

/**
 * commentaires.
 *
 * @author khadija Chouika <khadija.chouika@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 2.1
 *
 * @since MPE-4.0
 */
class FichiersDetailPli extends MpeTTemplateControl
{
    private $titre;
    private $_dataSource;
    public $_enveloppe;
    private $classToggle;

    public function getTitre()
    {
        return $this->titre;
    }

    public function setTitre($value)
    {
        $this->titre = $value;
    }

    public function getEnveloppe()
    {
        return $this->_enveloppe;
    }

    public function setEnveloppe($value)
    {
        $this->_enveloppe = $value;
    }

    public function getClassToggle()
    {
        return $this->classToggle;
    }

    public function setClassToggle($value)
    {
        $this->classToggle = $value;
    }

    /**
     * @return mixed
     */
    public function getSignature()
    {
        // faite comme ça car le TConditionnal se charge avant le replissage des paramettre passes au commposant
        return $this->Page->getViewState('avecSignature');
    }

    /**
     * Recupère le DataSource.
     */
    public function getDataSource()
    {
        return $this->_dataSource;
    }

    /**
     * Affecte le DataSource.
     */
    public function setDataSource($dataSource)
    {
        $this->_dataSource = $dataSource;
    }

    public function getStatutSignatureFichier($statut)
    {
        return match (strtoupper($statut)) {
            Atexo_Config::getParameter('OK') => Prado::localize('DEFINE_FICHIER_SIGNE_SIGNATURE_VALIDE'),
            Atexo_Config::getParameter('NOK') => Prado::localize('DEFINE_FICHIER_SIGNATURE_INVALIDE'),
            Atexo_Config::getParameter('UNKNOWN') => Prado::localize('DEFINE_FICHIER_SIGNATURE_INCERTAINE'),
            Atexo_Config::getParameter('SANS') => Prado::localize('DEFINE_FICHIER_NON_SIGNE_OU_NON_TROUVABLE'),
            Atexo_Config::getParameter('UNVERIFIED') => Prado::localize('DEFINE_MESSAGE_VALIDITE_SIGNATURE_NON_VERIFIABLE'),
            default => Prado::localize('DEFINE_FICHIER_NON_SIGNE_OU_NON_TROUVABLE'),
        };
    }

    public function getClassCss($statut)
    {
        return match (strtoupper($statut)) {
            Atexo_Config::getParameter('OK') => 'statut-resultat statut-valide',
            Atexo_Config::getParameter('NOK') => 'statut-resultat statut-erreur',
            Atexo_Config::getParameter('UNKNOWN') => 'statut-resultat statut-avertissement',
            Atexo_Config::getParameter('UNVERIFIED') => 'statut-resultat statut-vide',
            Atexo_Config::getParameter('SANS') => 'statut-resultat statut-vide',
            default => 'statut-resultat statut-vide',
        };
    }

    /**
     * Chargement de la page.
     */
    public function onLoad($param)
    {
        if (!$this->Page->isPostBack) {
            if ($this->_enveloppe instanceof CommonEnveloppe) {
                // $this->setViewState("enveloppe",$this->_enveloppe);
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
                $this->_dataSource = $this->_enveloppe->getCommonFichierEnveloppes($connexion);
                if (is_array($this->_dataSource)) {
                    $this->getTitreBlocFichier();
                    $this->repeaterFichiers->DataSource = $this->_dataSource;
                    $this->repeaterFichiers->DataBind();
                }
            }
        }
    }

    public function getTitreBlocFichier()
    {
        $titre = ' ';
        if ($this->_enveloppe) {
            if ($this->_enveloppe->getTypeEnv() == Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE')) {
                if ($this->getSignature()) {
                    $titre = Prado::localize('DEFINE_FICHIERS_CANDIDATURE_ET_SIGNATURE');
                } else {
                    $titre = Prado::localize('DEFINE_CANDIDATURE_FICHIERS_CONSTITUANT_DOSSIER');
                }
                $this->classToggle = 'Cand';
            } elseif ($this->_enveloppe->getTypeEnv() == Atexo_Config::getParameter('TYPE_ENV_OFFRE')) {
                $titre = Prado::localize('OFFRES');
                if ('0' != $this->_enveloppe->getSousPli()) {
                    $titre .= ' '.Prado::localize('TEXT_LOT').' '.$this->_enveloppe->getSousPli();
                    $this->classToggle = 'Offre_lot'.$this->_enveloppe->getSousPli();
                } else {
                    $this->classToggle = 'Offre_lot'.$this->_enveloppe->getSousPli();
                }
                $titre .= ' - ';
                if ($this->getSignature()) {
                    $titre .= Prado::localize('DEFINE_OFFRES_FICHIERS_CONSTITUANT_DOSSIER');
                } else {
                    $titre .= Prado::localize('FICHIER_CONSTITUANT_DOSSIER');
                }
            }
            $this->titre = $titre;
        }
    }

    /*
     * Retourne le title du lien fichier a telecharger
     */
    public function getLienTitle($file)
    {
        $taile = null;
        $tailleFichier = (new Atexo_Blob())->getTailFile($file->getIdBlob(), Atexo_CurrentUser::getCurrentOrganism());
        $tailleFichier = ($tailleFichier / 1024);
        $tailleFichier = Atexo_Util::arrondirSizeFile($tailleFichier);
        if ($tailleFichier) {
            $taile = $tailleFichier;
        }
        $title = Prado::localize('TEXT_TELECHARGER_FICHIER').' '.utf8_encode($file->getNomFichier()).' - '.$taile;

        return $title;
    }
}
