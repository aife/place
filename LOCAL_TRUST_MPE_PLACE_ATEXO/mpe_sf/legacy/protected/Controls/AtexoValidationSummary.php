<?php

namespace Application\Controls;

/**
 * Extension du composant du sommateur d'erreurs.
 *
 * @author ZAKI Anas <anas.zaki@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class AtexoValidationSummary extends MpeTTemplateControl
{
    private $_validationGroup;
    private $_id;

    public function setValidationGroup($value)
    {
        $this->_validationGroup = $value;
    }

    public function getValidationGroup()
    {
        return $this->_validationGroup;
    }

    public function setId($value)
    {
        $this->_id = $value;
    }

    public function getId($hideAutoID = true)
    {
        return $this->_id;
    }

    public function onLoad($param)
    {
        $this->validationSummary->setValidationGroup($this->_validationGroup);
    }

    public function addServerError($text, $encode = true)
    {
        $this->addServerErrors([$text], $encode);
    }

    public function addServerErrors($textTab, $encode = true)
    {
        $valeur = '<ul>';
        foreach ($textTab as $text) {
            if ('UTF-8' != mb_detect_encoding($text)) {
                $valeur .= '<li>'.utf8_encode($text).'</li>';
            } else {
                $valeur .= '<li>'.$text.'</li>';
            }
        }
        $valeur .= '</ul>';
        $this->serverError->Text = $valeur;
    }
}
