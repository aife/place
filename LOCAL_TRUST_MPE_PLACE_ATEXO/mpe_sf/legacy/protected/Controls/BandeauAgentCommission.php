<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Menu;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Socle\Atexo_Socle_AgentServicesMetiers;
use Prado\Prado;

class BandeauAgentCommission extends MpeTPage
{
    public function onLoad($param)
    {
        if (Atexo_Module::isEnabled('MultiLinguismeAgent')) {
            $langues = Atexo_Languages::retrieveActiveLangages();
            if (is_array($langues)) {
                $this->langages->DataSource = $langues;
                $this->langages->DataBind();
                /*$nomDesJour = array (
                    "Monday" => Prado::localize('TEXT_DAY1'),
                    "Tuesday" => Prado::localize('TEXT_DAY2'),
                    "Wednesday" => Prado::localize('TEXT_DAY3'),
                    "Thursday" => Prado::localize('TEXT_DAY4'),
                    "Friday" => Prado::localize('TEXT_DAY5'),
                    "Saturday" => Prado::localize('TEXT_DAY6'),
                    "Sunday" => Prado::localize('TEXT_DAY7'),
                );*/

            /*$nomDesMois = array (
                1 => Prado::localize('TEXT_MONTH1'),
                2 => Prado::localize('TEXT_MONTH2'),
                3 => Prado::localize('TEXT_MONTH3'),
                4 => Prado::localize('TEXT_MONTH4'),
                5 => Prado::localize('TEXT_MONTH5'),
                6 => Prado::localize('TEXT_MONTH6'),
                7 => Prado::localize('TEXT_MONTH7'),
                8 => Prado::localize('TEXT_MONTH8'),
                9 => Prado::localize('TEXT_MONTH9'),
                10 => Prado::localize('TEXT_MONTH10'),
                11 => Prado::localize('TEXT_MONTH11'),
                12 => Prado::localize('TEXT_MONTH12'),
            );*/
            //$this->dateAujourdhui->Text = Atexo_Util::getDateAujourdhui($nomDesJour, $nomDesMois);
            }
            $this->panelNaviguationMulilingue->setVisible(true);
        } else {
            $this->panelNaviguationMulilingue->setVisible(false);
            //$this->dateAujourdhui->Text = Atexo_Util::getDateAujourdhui();
        }
        if (!Atexo_CurrentUser::isConnected()) {
            $this->linkAccueilAgent->NavigateUrl = 'index.php?page=Agent.AgentHome';
            $this->panelNonConnecte->visible = true;
            $this->panelConnecte->visible = false;
            $this->seDeconnecter->visible = false;
        } else {
            $this->panelConnecte->visible = true;
            $this->panelNonConnecte->visible = false;
//             $this->listeServiceMetier->setVisible(true);
            if (Atexo_Module::isEnabled('SocleInterne')) {
                $urlMonCompteAgent = 'index.php?page=Agent.AccueilAgentAuthentifieSocleinterne';
            //              $services = Atexo_Socle_AgentServicesMetiers::retrieveServicesOrganisme(Atexo_CurrentUser::getIdAgentConnected(), Atexo_CurrentUser::getCurrentOrganism(),true);
//              $this->repeaterListeServices->DataSource = $services;
//              $this->repeaterListeServices->DataBind();
            } elseif ((Atexo_Module::isEnabled('SocleExternePpp')) && ('agenthelios' == strtolower(Atexo_CurrentUser::getRole()))) {
                $urlMonCompteAgent = 'index.php?page=helios.HeliosAccueilAuthentifie';
            } else {
                $urlMonCompteAgent = 'agent';
            }

            $this->linkMonCompteAgent->NavigateUrl = Atexo_Config::getParameter('PF_URL_AGENT').$urlMonCompteAgent;
            $this->linkAccueilAgent->NavigateUrl = Atexo_Config::getParameter('PF_URL_AGENT').$urlMonCompteAgent;
        }
        if (Atexo_Module::isEnabled('SocleInterne') && Atexo_Config::getParameter('URL_ACCUEIL_SPIP')) {
            $this->linkAccueilPortail->setVisible(true);
            $this->linkAccueilPortail->setNavigateUrl(Atexo_Config::getParameter('URL_ACCUEIL_SPIP_AGENT'));
        } elseif (Atexo_Module::isEnabled('SocleExternePpp')) {
            $this->linkAccueilPortail->setVisible(true);
            $this->linkAccueilPortail->setNavigateUrl(Atexo_Config::getParameter('URL_SOCLE_EXTERNE'));
        }

        if ('URL_LOGO' != Atexo_Config::getParameter('URL_LOGO')) {
            $this->panelLogo->visible = false;
            $this->panelLinklogo->visible = true;
        } else {
            $this->panelLogo->visible = true;
            $this->panelLinklogo->visible = false;
        }
    }

    public function activateMenu($param)
    {
        $activateMenu = new Atexo_Menu();

        return $activateMenu->activateMenu($param, 'Agent');
    }

    public function getNomPrenomUser()
    {
        return ''.Atexo_Util::atexoHtmlEntities(Atexo_CurrentUser::getFirstName()).' '.Atexo_Util::atexoHtmlEntities(Atexo_CurrentUser::getLastName()).'';
    }

    public function Disconnect($sender, $param)
    {
        if (Atexo_Module::isEnabled('SocleExterneAgent')) {
            $this->getApplication()->getModule('auth')->logout();
            $this->response->redirect(Atexo_Config::getParameter('URL_INDEX_AGENT_DISCONECT'));
        } elseif (Atexo_Module::isEnabled('SocleExternePpp')) {
            $this->getApplication()->getModule('auth')->logout();
            $this->response->redirect(Atexo_Config::getParameter('URL_PPP_OPENSSO_LOGOUT'));
        } elseif (Atexo_Module::isEnabled('SocleInterne')) {
            $this->response->redirect(Atexo_Config::getParameter('PF_URL_AGENT').'?page=Agent.Disconnect');
        } elseif (0 == strcmp(substr($_GET['page'], 0, 15), 'administration.')) {
            $this->getApplication()->getModule('auth')->logout();
            $this->response->redirect('?page=Administration.Home');
        } else {
            $this->getApplication()->getModule('auth')->logout();
            $this->response->redirect('?page=Agent.AgentHome');
        }
    }

    public function writeLanguage($sender, $param)
    {
        $langue = $param->CommandName;
        $keys = (array_keys($_GET));
        $values = (array_values($_GET));
        $urlParams = '?';
        for ($i = 0; $i < count($keys); ++$i) {
            if ('lang' != $keys[$i]) {
                $urlParams .= Atexo_Util::atexoHtmlEntities($keys[$i]).'='.Atexo_Util::atexoHtmlEntities($values[$i]).'&';
            }
        }
        $this->response->redirect($_SERVER['PHP_SELF'].$urlParams.'lang='.$langue);
    }

    public function selectFlag($langue)
    {
        $sessionLang = Atexo_CurrentUser::readFromSession('lang');
        if ('atx' == $_GET['lang'] || 'atx' == $sessionLang) {
            return 'off';
        }
        $arrayActiveLangages = Atexo_Languages::retrieveArrayActiveLangages();
        if ((isset($_GET['lang']) && $_GET['lang'] == $langue)
            ||
            (isset($sessionLang) && $sessionLang == $langue)
            ||
            (
                (
                    (!isset($_GET['lang']) && !isset($sessionLang))
                    ||
                    (isset($_GET['lang']) && !in_array($_GET['lang'], $arrayActiveLangages))
                    ||
                    (isset($sessionLang) && !in_array($sessionLang, $arrayActiveLangages))
                )
 	 			&& $this->Data->getLangue()==Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')
            )
            ) {
            return 'on';
        }
    }public function getUrlLogo($org)
    {
        return Atexo_Config::getParameter("PF_URL")."index.php?page=".Atexo_Util::getTypeUserCalledForPradoPages().".LogoOrganisme&grand=true&org=".$org;
    }

}
