<com:TRepeater ID="RepeaterReferentielZoneText">
    <prop:ItemTemplate>
        <com:TActivePanel ID="panelReferentielZoneText" CssClass="<%# $this->SourceTemplateControl->getLine()? 'line':'' %>" visible="<%# !$this->parent->parent->getCheck()%>">
            <div class="form-group form-group-sm">
                <div class="col-md-12 m-b-1<%#($this->parent->parent->getCssClass())%>">
                    <div class="row line">
                        <label for="ctl0_CONTENU_PAGE_AdvancedSearch_idReferentielZoneText_RepeaterReferentielZoneText_ctl0_idReferentielZoneText" class="control-label col-md-2 intitule-150">
                            <com:TLabel id="titre" text="<%#Prado::localize($this->Data->getCodeLibelle())%>" visible="<%# !$this->parent->parent->getCheck()%>"/>
                            <span <%#($this->Data->getObligatoire()&& $this->parent->parent->getObligatoire()) ? "class='champ-oblig'" : "style='display:none'" %>>
                            <com:TLabel id="etoile" text="*" visible="<%# !$this->parent->parent->getCheck()%>"/>
                            </span>
                            <span <%#($this->Data->getLibelleInfoBulle()) ? "class='champ-oblig'" : "style='display:none'" %>>
                            <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" onmouseover="afficheBulle('infosRefrentiel_<%#$this->Data->getId()%>', this)"
                                 onmouseout="cacheBulle('infosRefrentiel_<%#$this->Data->getId()%>')" class="picto-info-intitule" alt="Info-bulle" title="Info-bulle"/>
                            </span>
                        </label>
                        <!--  a voir  -->
                        <com:TLabel id="labelReferentielZoneText" cssClass="content-bloc" text="" visible="<%# $this->parent->parent->getLabel()%>"/>
                        <com:TPanel visible="<%#($this->SourceTemplateControl->getCas($this->Data) === '1') ? false : true%>" cssClass="col-md-9">
                            <com:TTextBox id="idReferentielZoneText"
                                          Attributes.onblur="<%#(strtolower($this->Data->getDataType()) == 'montant')?'formatterMontant(\''.$this->idReferentielZoneText->getClientId().'\');':''%>"
                                          TextMode="<%#$this->Data->getModeModification()%>" CssClass="form-control long"
                                          visible="<%#(!$this->parent->parent->getLabel() && !$this->parent->parent->getCheck() && $this->parent->parent->getType())%>"/>
                            <com:TCustomValidator
                                    Id="validatorAtexoText2"
                                    ControlToValidate="idReferentielZoneText"
                                    ClientValidationFunction="ValidateLtReferentielZoneText"
                                    ValidationGroup="<%#($this->SourceTemplateControl->getValidationGroup())%>"
                                    Display="Dynamic"
                                    EnableClientScript="true"
                                    ErrorMessage="<%#Prado::localize($this->Data->getCodeLibelle())%>"
                                    visible="<%#($this->Data->getObligatoire()&& $this->parent->parent->getObligatoire()) ? 'true' :'false'%>"
                                    Enabled="<%#($this->Data->getObligatoire()&& $this->parent->parent->getObligatoire()) ? 'true' :'false'%>"
                                    Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
                                <prop:ClientSide.OnValidationError>
                                    blockErreur = '<%#($this->parent->parent->getValidationSummary())%>';
                                    if(document.getElementById(blockErreur))
                                    document.getElementById(blockErreur).style.display='';
                                    if(document.getElementById('ctl0_CONTENU_PAGE_buttonSave'))
                                    document.getElementById('ctl0_CONTENU_PAGE_buttonSave').style.display='';
                                    if(document.getElementById('ctl0_CONTENU_PAGE_blocBoutons'))
                                    document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
                                    if(document.getElementById('ctl0_CONTENU_PAGE_buttonSaveNotRsem'))
                                    document.getElementById('ctl0_CONTENU_PAGE_buttonSaveNotRsem').style.display='';
                                </prop:ClientSide.OnValidationError>
                            </com:TCustomValidator>
                            <com:THiddenField id="oldValue"/>
                        </com:TPanel>
                        <com:TPanel visible="<%#($this->SourceTemplateControl->getCas($this->Data) === '1') ? true : false%>">
                            <com:TLabel>
                                <com:TTranslate>DEFINE_ENTRE</com:TTranslate>
                                :
                            </com:TLabel>
                            <com:TTextBox id="idReferentielZoneTextFrom" Attributes.OnChange="replaceCaracSpecial(this);" TextMode="<%#$this->Data->getModeModification()%>" CssClass="small"
                                          visible="<%#(!$this->parent->parent->getLabel() && !$this->parent->parent->getCheck() && $this->parent->parent->getType())%>"/>
                            <com:TLabel>
                                <com:TTranslate>DEFINE_ET</com:TTranslate>
                                :
                            </com:TLabel>
                            <com:TTextBox id="idReferentielZoneTextTo" Attributes.OnChange="replaceCaracSpecial(this);" TextMode="<%#$this->Data->getModeModification()%>" CssClass="small"
                                          visible="<%#(!$this->parent->parent->getLabel() && !$this->parent->parent->getCheck() && $this->parent->parent->getType())%>"/>
                        </com:TPanel>

                        <com:TLabel id="iDLibelle" visible="false" text="<%#$this->Data->getId()%>"/>
                        <com:TLabel id="codeLibelle" visible="false" text="<%#$this->Data->getCodeLibelle()%>"/>
                        <com:TLabel id="typeRef" visible="false" text="<%#$this->Data->getType()%>"/>
                        <com:TLabel id="typeSearch" visible="false" text="<%#$this->Data->getTypeSearch()%>"/>
                        <com:THiddenField id="modeRecherche" value="<%#$this->Data->getModeRecherche()%>"/>
                        <com:THiddenField id="typeData" value="<%#$this->Data->getDataType()%>"/>

                    </div>
                </div>
                <div class="breaker"></div>
            </div>
        </com:TActivePanel>
    </prop:ItemTemplate>
</com:TRepeater>
