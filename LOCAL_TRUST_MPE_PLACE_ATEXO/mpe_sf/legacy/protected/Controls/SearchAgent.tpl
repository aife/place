<div class="form-field margin-0">
			<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><span class="title"><%=Prado::localize('DEFINE_RECHERCHE_MULTICRITAIRE_AGENT')%></span></div>
			<div class="content">
<com:TActivePanel ID="panelSearchAgent">
            			<!--Debut Ligne Ministere-->
            				<div id="divOrg" class="line">
            					<div class="intitule-150"><label for="organismesNames"><com:TTranslate>TEXT_MINISTERE</com:TTranslate></label> :</div>
            					<com:TActiveDropDownList ID="organismesNames" cssClass="long" onCallBack = "displayEntityPurchase" Attributes.title="<%=Prado::localize('TEXT_MINISTERE')%>"/>
            				</div>
            				<!--Fin Ligne Ministere-->
            				<com:TActivePanel ID="panelPurchaseNames">
                				<!--Debut Ligne Ministere-->
                				<div id="divEntityPurchase" style="display:''" class="line">
                					<div class="intitule-150"><label for="entityPurchaseNames"><com:TTranslate>SERVICE_RECHERCHE_AVANCEE</com:TTranslate></label> :</div>                					
                					<div class="content-bloc bloc-600">
	                					<com:TActiveDropDownList ID="entityPurchaseNames" cssClass="long float-left" AutoPostBack="true" OnSelectedIndexChanged="setUrlAjoutAgent" Attributes.title="<%=Prado::localize('SERVICE_RECHERCHE_AVANCEE')%>" />
	                					<!-- Début bloc inclure la descendance -->
	                					<div class="b-ml-15">
											<div class="line"> <span title="<%=Prado::Localize('UNIQUEMENT_CETTE_ENTITE')%>" class="radio">
												<com:TRadioButton GroupName="choixInclusionDescendancesServices"  id="entiteSeule" />
												</span>
												<label for="entiteSeule"><com:TTranslate>UNIQUEMENT_CETTE_ENTITE</com:TTranslate></label>
											</div>
				
											<div class="line"> <span title="<%=Prado::Localize('INCLURE_LES_DESCENDANTS')%>" class="radio">
												<com:TRadioButton GroupName="choixInclusionDescendancesServices" checked="true" id="inclureDescendances" />
												</span>
												<label for="inclureDescendances"><com:TTranslate>INCLURE_LES_DESCENDANTS</com:TTranslate></label>
											</div>
										</div>
									</div>
									<!-- Fin bloc inclure la descendance -->
                				</div>
                				<!--Fin Ligne Ministere-->
                				<com:TLabel ID="scriptPurchaseNames" />
            				</com:TActivePanel>
                			<com:TLabel ID="scriptOrg" />
</com:TActivePanel>
					<com:THiddenField ID="idServiceAgentConnecte"/>
                    <div class="spacer"></div>
					<!--Debut Recherche Agent par mot-cle-->
					<h4><%=Prado::localize('DEFINE_RECHERCHE_MOT_CLE')%><img title="Info-bulle" alt="Info-bulle" class="picto-info-intitule" onmouseout="cacheBulle('infosRechercheAgent')" onmouseover="afficheBulle('infosRechercheAgent', this)" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" /></h4>
					<!-- Debut recherche par code/libelle acheteur -->	
					<com:TPanel visible=<%=Application\Service\Atexo\Atexo_Module::isEnabled('AfficherCodeService')%> >
						<div class="line">
							<div class="intitule-bloc intitule-140">
								<label for="libelleAcheteur"><%=Prado::localize('DEFINE_LIBELLE_ACHETEUR')%> :</label>
							</div>
							<div class="content-bloc  bloc-600">
								<com:TTextBox id="libelleAcheteur"  cssClass="long" Attributes.title="<%=Prado::localize('DEFINE_LIBELLE_ACHETEUR')%>" />
							</div>
						</div>
						<div class="line">
							<div class="intitule-bloc intitule-140">
								<label for="codeAcheteur"><%=Prado::localize('DEFINE_CODE_ACHETEUR')%> :</label>
							</div>
							<div class="content-bloc  bloc-600">
								<com:TTextBox ID="codeAcheteur"  cssClass="input-185" Attributes.title="<%=Prado::localize('DEFINE_CODE_ACHETEUR')%>" />
							</div>
						</div><div class="spacer"></div>
					</com:TPanel>
					<!-- Fin recherche par code/libelle acheteur -->	
					<div class="bloc-50pourcent">	
						<div class="line">
							<div class="intitule-bloc intitule-140">
								<label for="nomAgent"><%=Prado::localize('NOM')%> :</label>
							</div>
							<div class="content-bloc">
								<com:TTextBox ID="nomAgent"  cssClass="input-185" Attributes.title="<%=Prado::localize('NOM')%>" />
							</div>
						</div>
						<div class="line">
							<div class="intitule-bloc intitule-140">
								<label for="prenomAgent"><%=Prado::localize('PRENOM')%> :</label>
							</div>
							<div class="content-bloc">
								<com:TTextBox ID="prenomAgent"  cssClass="input-185" Attributes.title="<%=Prado::localize('PRENOM')%>" />
							</div>
						</div>
						<div class="line">
							<div class="intitule-bloc intitule-140">
								<label for="statutCompteAgent"><%=Prado::localize('STATUT_DU_COMPTE')%> :</label>
							</div>
							<div class="content-bloc">
								<com:TDropDownList id="statutCompteAgent"  cssClass="select-185" Attributes.title="<%=Prado::localize('STATUT_DU_COMPTE')%>" />
							</div>
						</div>
					
					</div>		
					<div class="bloc-50pourcent">	
						<div class="line">
							<div class="intitule-bloc intitule-140">
								<label for="emailAgent"><%=Prado::localize('TEXT_ADRESSE_ELECTRONIQUE')%> :</label>
							</div>
							<div class="content-bloc">
								<com:TTextBox ID="emailAgent"  cssClass="input-185" Attributes.title="<%=Prado::localize('TEXT_ADRESSE_ELECTRONIQUE')%>" />
							</div>
						</div>
						<div class="line">
							<div class="intitule-bloc intitule-140">
								<label for="emailIdentifiant"><%=Prado::localize('TEXT_IDENTIFIANT')%></label>
							</div>
							<div class="content-bloc">
								<com:TTextBox ID="IdentifiantAgent"  cssClass="input-185" Attributes.title="<%=Prado::localize('TEXT_IDENTIFIANT_SANS_POINTS')%>" />
							</div>
						</div>
					</div>		
					<!--Fin Recherche Agent par mot-cle-->
					<div class="spacer-small"></div>
					<!--Debut Bloc Referentiel Agent -->
					<com:TPanel visible="<%=( Application\Service\Atexo\Atexo_Module::isEnabled('AnnuaireAcheteursPublics') || ($this->Page->Master->getCalledFrom() == 'annuaire') ) ? true:false%>" >
						<com:AtexoLtReferentiel ID="ltRefAgent" agent="1" cssClass="intitule-140" mode="1" />
					</com:TPanel>
					<!--Fin Bloc Referentiel Agent -->
                    <div class="spacer-small"></div>
                    <div class="boutons-line">
                        <com:TActiveButton 
                        	CssClass="bouton-moyen-120 float-right" 
                        	Text="<%=Prado::localize('DEFINE_LANCER_RECHERCHE')%>"
                        	Attributes.title="<%=Prado::localize('DEFINE_LANCER_RECHERCHE')%>"
                        	OnCallback="Page.onClickSearch">
                        	<prop:ClientSide.OnLoading>showLoader();</prop:ClientSide.OnLoading>
							<prop:ClientSide.OnFailure>showLoader();</prop:ClientSide.OnFailure>
							<prop:ClientSide.OnSuccess>showLoader();</prop:ClientSide.OnSuccess>
                        </com:TActiveButton>
                    </div>
                    <div class="breaker"></div>
                </div>
                <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
				<div id="infosRechercheAgent" class="info-bulle" onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();"><div><com:TTranslate>TEXT_INFOS_BULLE_SEARCH_AGENT</com:TTranslate>.</div></div>
            </div>
            <div class="spacer-mini"></div>
