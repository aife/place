<div class="notifications-component">
    <button type="button" class="button-default show-notifications js-show-notifications">
        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="30" height="32" viewBox="0 0 30 32">
            <defs>
                <g id="icon-bell">
                    <path class="path1" d="M15.143 30.286q0-0.286-0.286-0.286-1.054 0-1.813-0.759t-0.759-1.813q0-0.286-0.286-0.286t-0.286 0.286q0 1.304 0.92 2.223t2.223 0.92q0.286 0 0.286-0.286zM3.268 25.143h23.179q-2.929-3.232-4.402-7.348t-1.473-8.652q0-4.571-5.714-4.571t-5.714 4.571q0 4.536-1.473 8.652t-4.402 7.348zM29.714 25.143q0 0.929-0.679 1.607t-1.607 0.679h-8q0 1.893-1.339 3.232t-3.232 1.339-3.232-1.339-1.339-3.232h-8q-0.929 0-1.607-0.679t-0.679-1.607q3.393-2.875 5.125-7.098t1.732-8.902q0-2.946 1.714-4.679t4.714-2.089q-0.143-0.321-0.143-0.661 0-0.714 0.5-1.214t1.214-0.5 1.214 0.5 0.5 1.214q0 0.339-0.143 0.661 3 0.357 4.714 2.089t1.714 4.679q0 4.679 1.732 8.902t5.125 7.098z" />
                </g>
            </defs>
            <g>
                <use xlink:href="#icon-bell" transform="translate(0 0)"></use>
            </g>
        </svg>
        <div class="notifications-count js-count"></div>
    </button>
    <com:TActivePanel cssClass="notifications js-notifications" ID="panelNotification" style="display:'none'">
        <h3><com:TTranslate>NOTIFICATIONS</com:TTranslate> </h3>
        <div id="notificationsLoader" class="notificationsLoader">
            <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/ajax-loader-small.gif" />
        </div>
        <ul class="notifications-list">
            <com:TActivePanel Display="<%=$this->getNombreNotificationsActif()?'None':'Dynamic'%>" >
                <li class="item"> <com:TTranslate>AUCUNE_NOTIFICATION</com:TTranslate></li>
            </com:TActivePanel>
            <com:TActiveRepeater
                ID="repeaterNotifications"
            >
                <prop:ItemTemplate>
                    <li class="item js-item <%#$this->Data->getNotificationLue()?'expired':''%>" data-id="<%#$this->Data->getIdNotification('d/m/Y')%>">
                        <div class="details">
                            <span class="title"><com:TLabel id="labelNotification" text="<%# Application\Service\Atexo\Atexo_Util::formaterChaineToMessage($this->Data->getLibelleNotification())%>"/><com:TLabel id="desciptionNotification" text="<%# Application\Service\Atexo\Atexo_Util::formaterChaineToMessage($this->Data->getDescription())%>"/>
                                <com:TLinkButton
                                        id="lireNotification"
                                        OnCommand="SourceTemplateControl.lireNotification"
                                        CommandParameter="<%#$this->Data->getIdNotification()%>"
                                >
                                    <u><com:TLabel id="referenceObjet" text="<%#$this->Data->getReferenceObjet()%>"/></u>
                                </com:TLinkButton>
                            </span>
                            <span class="date"><com:TLabel id="dateCreation" text="<%#$this->Data->getDateCreation('d/m/Y')%>"/> - <com:TLabel id="heureCreation" text="<%#$this->Data->getDateCreation('H:i')%>"/></span></div>
                            <com:TActiveLinkButton
                                    id="desactiverNotification"
                                    cssClass="button-default button-dismiss js-dismiss"
                                    OnCallback="SourceTemplateControl.desactiverNotification"
                                    ActiveControl.CallbackParameter="<%#$this->Data->getIdNotification()%>"
                            >x</com:TActiveLinkButton>

                    </li>
                </prop:ItemTemplate>

            </com:TActiveRepeater>
        </ul>

        <a href="#" class="show-all" style="display: none"><com:TTranslate>AFFICHER_TOUTES_LES_NOTIFICATIONS</com:TTranslate></a></com:TActivePanel>
</div>

<com:TActiveLabel ID="javascriptLabel" display="None"/>

<com:TActiveButton
        style="display:none"
        id="refreshNotification"
        onCallback="refreshNotification"
/>
<com:TActiveButton
        style="display:none"
        id="initilaiserNotification"
        onCallback="viderRepeaterNotification"
/>


<com:TClientScript visible="<%=($this->isConnected() && Application\Service\Atexo\Atexo_Module::isEnabled('NotificationsAgent') && $this->isAgent()) ? true:false%>">
    setInterval(refreshNotifications, <%= Application\Service\Atexo\Atexo_Config::getParameter('TIME_AJAX_REFRESH_NOTIFICATIONS')%> );
</com:TClientScript>