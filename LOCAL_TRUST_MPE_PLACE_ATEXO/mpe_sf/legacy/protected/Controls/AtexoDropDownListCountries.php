<?php

namespace Application\Controls;

use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2;
use Prado\Prado;
use Prado\Web\UI\ActiveControls\TActiveDropDownList;

/**
 * Classe du composant pays.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class AtexoDropDownListCountries extends TActiveDropDownList
{
    private $_typeAffichage;
    private $_withaoutFrance = false;
    private $withDom = false;
    public $_postBack = false;
    
    public function onLoad($param)
    {
        if (!$this->Page->isPostBack) {
            $this->fillDataSource();
        }
    }

    public function initDataSource()
    {
        $dataSource = [];
        $dataSource['0'] = Prado::localize('TEXT_SELECTIONNER');
        $this->setDataSource($dataSource); //$this->displayCountries($this->_typeAffichage));
        $this->DataBind();
    }

    public function fillDataSource()
    {
        $this->setDataSource($this->displayCountries($this->_typeAffichage));
        $this->DataBind();
    }

    public function setPostBack($value)
    {
        $this->_postBack = $value;
    }

    public function getPostBack()
    {
        return $this->_postBack;
    }

    public function setWithDom ($withDom)
    {
        $this->withDom = (bool) $withDom;
    }

    public function getWithDom()
    {
        return $this->withDom;
    }


 public function getTypeAffichage()
    {
        return $this->_typeAffichage;
    }

    public function setWithaoutFrance($withaoutFrance)
    {
        $this->_withaoutFrance = $withaoutFrance;
    }

    public function setTypeAffichage($_typeAffichage)
    {
        $this->_typeAffichage = $_typeAffichage;
    }

    /**
     * Permet de recuperer la liste des pays
     * Organise les donnees dans un tableau.
     *
     * @param string $typeAffichage : type d'affichage a utiliser
     *
     * @return array : liste des pays
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function displayCountries($typeAffichage)
    {
        $listCountries = (new Atexo_Geolocalisation_GeolocalisationN2())->retrieveListCountries($this->_withaoutFrance, $this->withDom);
        $dataSource = array();
        $dataSource['0'] = Prado::localize("TEXT_SELECTIONNER");
        if ($listCountries) {
            foreach ($listCountries as $oneCountrie) {
                if ('withCode' == $typeAffichage) {
                    $dataSource[$oneCountrie->getId()] = $oneCountrie->getDenomination1().
                                                         ' - ('.$oneCountrie->getDenomination2().')';
                } elseif ('withoutCode' == $typeAffichage) {
                    $dataSource[$oneCountrie->getId()] = $oneCountrie->getDenomination1();
                } elseif ('justCode' == $typeAffichage) {
                    $dataSource[$oneCountrie->getId()] = $oneCountrie->getDenomination2();
                }
            }

            return $dataSource;
        } else {
            return false;
        }
    }
}
