<?php

namespace Application\Controls;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonBlobFile;
use Application\Propel\Mpe\CommonBlobFilePeer;
use Application\Propel\Mpe\CommonCG76PieceJointe;
use Application\Propel\Mpe\CommonCG76PieceJointePeer;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_ScanAntivirus;

class UploadPieceJointe extends MpeTTemplateControl
{
    public $_ID;

    public function remplirTableauPJ($data)
    {
        $this->RepeaterPiecesJointes->dataSource = $data;
        $this->RepeaterPiecesJointes->dataBind();
    }

    public function onLoad($param)
    {
    }

    public function displayPiecesJointes($listePjs)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $liste = explode(',', $listePjs);
        $files = [];
        foreach ($liste as $idBlob) {
            if (trim($idBlob)) {
                $c = new Criteria();
                $c->add(CommonBlobFilePeer::ID, $idBlob);
                $blobFile = CommonBlobFilePeer::doSelectOne($c, $connexionCom);
                if ($blobFile instanceof CommonBlobFile) {
                    $infos['id'] = $blobFile->getId();
                    $infos['name'] = $blobFile->getName();
                    $files[$blobFile->getId()] = $infos;
                }
            }
        }
        $this->setViewState('blobs', $files);

        $this->RepeaterNomsFichiers->DataSource = $files;
        $this->RepeaterNomsFichiers->DataBind();
    }

    public function onAjoutClick($sender, $param)
    {
        $blob = [];
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        if ($this->ajoutFichier->HasFile) {
            //commencer le scan Antivirus
            $msg = Atexo_ScanAntivirus::startScan($this->ajoutFichier->LocalName);
            if ($msg) {
                $liste = $this->getViewState('blobs');
                $this->RepeaterNomsFichiers->DataSource = $liste;
                $this->RepeaterNomsFichiers->DataBind();
                $this->afficherErreur($msg.'"'.$this->ajoutFichier->FileName.'"');

                return;
            }
            //Fin du code scan Antivirus
            $infilepj = Atexo_Config::getParameter('COMMON_TMP').'pj'.session_id().time();
            $IdEntreprise = Atexo_CurrentUser::getIdEntreprise();
            if (move_uploaded_file($this->ajoutFichier->LocalName, $infilepj)) {
                $atexoBlob = new Atexo_Blob();
                $idBlob = $atexoBlob->insert_blob($this->ajoutFichier->FileName, $infilepj, Atexo_Config::getParameter('COMMON_BLOB'));

                $blob['id'] = $idBlob;
                $blob['name'] = $this->ajoutFichier->FileName;

                $liste = $this->getViewState('blobs', []);

                $liste[$idBlob] = $blob;
                $this->setViewState('blobs', $liste);

                $pieceJointe = new CommonCG76PieceJointe();
                $pieceJointe->setIdentreprise($IdEntreprise);
                $pieceJointe->setIdpj($idBlob);
                $pieceJointe->save($connexionCom);

                $this->RepeaterNomsFichiers->DataSource = $liste;
                $this->RepeaterNomsFichiers->DataBind();
            }
        } else {
            $liste = $this->getViewState('blobs');
            $this->RepeaterNomsFichiers->DataSource = $liste;
            $this->RepeaterNomsFichiers->DataBind();
        }
    }

    public function onDeleteClick($sender, $param)
    {
        $idBlob = $param->CommandName;

        $liste = $this->getViewState('blobs', []);
        unset($liste[$idBlob]);
        $this->setViewState('blobs', $liste);
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $c = new Criteria();
        $c->add(CommonCG76PieceJointePeer::IDPJ, $idBlob);
        CommonCG76PieceJointePeer::doDelete($c, $connexionCom);
    }

    public function refreshRepeater($sender, $param)
    {
        $liste = $this->getViewState('blobs', []);

        $this->RepeaterNomsFichiers->DataSource = $liste;
        $this->RepeaterNomsFichiers->DataBind();

        $this->panelListeFichiers->render($param->NewWriter);
    }

    public function downloadBlob($sender, $param)
    {
        $idBlob = $param->CommandName;
        $liste = $this->getViewState('blobs', []);
        $atexoBlob = new Atexo_Blob();
        $atexoBlob->sendBlobToClient($idBlob, $liste[$idBlob]['name'], Atexo_Config::getParameter('COMMON_BLOB'));
        exit;
    }

    public function OnValidClick()
    {
        $blob = [];
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        if ($this->ajoutFichier->HasFile) {
            //commencer le scan Antivirus
            $msg = Atexo_ScanAntivirus::startScan($this->ajoutFichier->LocalName);
            if ($msg) {
                $this->afficherErreur($msg.'"'.$this->ajoutFichier->FileName.'"');

                return;
            }
            //Fin du code scan Antivirus
            $infilepj = Atexo_Config::getParameter('COMMON_TMP').'pj'.session_id().time();
            if (move_uploaded_file($this->ajoutFichier->LocalName, $infilepj)) {
                $atexoBlob = new Atexo_Blob();
                $idBlob = $atexoBlob->insert_blob($this->ajoutFichier->FileName, $infilepj, Atexo_Config::getParameter('COMMON_BLOB'));

                $blob['id'] = $idBlob;
                $blob['name'] = $this->ajoutFichier->FileName;

                $liste = $this->getViewState('blobs', []);

                $liste[$idBlob] = $blob;
                $this->setViewState('blobs', $liste);

                $pieceJointe = new CommonCG76PieceJointe();
                $pieceJointe->setIdentreprise(Atexo_CurrentUser::getIdEntreprise());
                $pieceJointe->setIdpj($idBlob);
                $pieceJointe->save($connexionCom);
            }
        }
        $idsBlob = '';
        $liste = $this->getViewState('blobs', []);
        $arrayFile = '';
        foreach ($liste as $idBlob => $arrayFile) {
            $idsBlob .= $idBlob.',';
        }
        echo "<script>opener.document.getElementById('ctl0_CONTENU_PAGE_listFiles').value=',".$idsBlob."';opener.document.getElementById('ctl0_CONTENU_PAGE_refreshPj').click();window.close();</script>";
    }

    public function afficherErreur($msg)
    {
        $this->panelMessageErreur->setMessage($msg);
        $this->panelMessageErreur->setVisible(true);
    }
}
