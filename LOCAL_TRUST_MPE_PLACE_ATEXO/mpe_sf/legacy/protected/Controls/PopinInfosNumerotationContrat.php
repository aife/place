<?php

namespace Application\Controls;

use Application\Propel\Mpe\CommonTContratTitulaire;

/**
 * Page d'affichage des informations de numérotations du contrat.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2015
 *
 * @version 2015-roadmap
 */
class PopinInfosNumerotationContrat extends MpeTTemplateControl
{
    private $contrat;
    private ?string $lots = null;

    /**
     * Modifie la valeur de [contrat].
     *
     * @param CommonTContratTitulaire contrat : la nouvelle valeur
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function setContrat($contrat)
    {
        $this->contrat = $contrat;
    }

    /**
     * Recupere la valeur de [contrat].
     *
     * @return commonTContratTitulaire : la valeur courante [contrat]
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function getContrat()
    {
        return $this->contrat;
    }

    /**
     * Retourne la reference interne du contrat.
     *
     * @return string: la reference interne du contrat
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function getReferenceInterne()
    {
        return ($this->getContrat() instanceof CommonTContratTitulaire && $this->getContrat()->getReferenceLibre()) ? $this->getContrat()->getReferenceLibre() : '-';
    }

    /**
     * Modifie la valeur de [lots].
     *
     * @param string $lots: la nouvelle valeur
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function setLots($lots)
    {
        $this->lots = $lots;
    }

    /**
     * Recupere la valeur de [lots].
     *
     * @return string : la valeur courante [lots]
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function getLots()
    {
        return $this->lots;
    }

    /**
     * Retourne le numero court du contrat.
     *
     * @return string: la reference interne du contrat
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function getNumeroCourt()
    {
        return ($this->getContrat() instanceof CommonTContratTitulaire && $this->getContrat()->getNumeroContrat()) ? $this->getContrat()->getNumeroContrat() : '-';
    }

    /**
     * Retourne le numero long du contrat.
     *
     * @return string: la reference interne du contrat
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function getNumeroLong()
    {
        return ($this->getContrat() instanceof CommonTContratTitulaire && $this->getContrat()->getNumLongOeap()) ? $this->getContrat()->getNumLongOeap() : '-';
    }

    /**
     * Retourne l'identifiant technique du contrat.
     *
     * @return string: identifiant technique du contrat
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function getIdContrat()
    {
        return ($this->getContrat() instanceof CommonTContratTitulaire) ? $this->getContrat()->getIdContratTitulaire() : '';
    }

    /**
     * Construit et retourne l'url de la page de numerotation du contrat.
     *
     * @return string: identifiant technique du contrat
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function getUrlPageNumerotation()
    {
        if ($this->getContrat() instanceof CommonTContratTitulaire) {
            $reference = base64_encode($this->getContrat()->getRefConsultation());
            $lots = base64_encode($this->getLots());
            $idContrat = base64_encode($this->getContrat()->getIdContratTitulaire());

            return "index.php?page=Agent.PopupNumerotationContrat&id=$reference&lots=$lots&idContrat=$idContrat";
        }
    }
}
