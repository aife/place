<?php

namespace Application\Controls;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonConsultationPeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Referentiel\Atexo_Referentiel_Referentiel;
use Prado\Prado;

class AtexoReferentielCheckRadioLots extends AtexoLtRefRadio
{
    private $check = false;
    private $ref;

    public function setRef($ref)
    {
        $this->ref = $ref;
    }

    public function getRef()
    {
        return $this->ref;
    }

    public function setCheck($check)
    {
        $this->check = $check;
    }

    public function getCheck()
    {
        return $this->check;
    }

    /*
     * display composant ref radio Lots
     */
    public function afficherRadioLot($consultationId = null, $organisme = null, $defineValue = null, $postback = false)
    {
        if (!$this->Page->isPostBack || $postback) {
            self::remplirRepeaterLot($this->getRef());
            foreach ($this->repeaterLot->Items as $Item) {
                $IdRef = $Item->iDLibelle->Text;
                $codeRefPrinc = null;
                if (!$defineValue) {
                    if ($organisme) {
						$refCons = (new Atexo_Referentiel_Referentiel())->getCommonLtReferentielByRefConsultationAndLot($consultationId, $Item->Data['lot'], $IdRef, $organisme);
                        if ($refCons) {
                            $codeRefPrinc = $refCons->getValeurPrincipaleLtReferentiel();
                        }
                    }
                } else {
                    if ($defineValue[$IdRef]) {
                        $codeRefPrinc = $defineValue[$IdRef]->getValuePrinc();
                    }
                }
                if ('1' == $codeRefPrinc) {
                    $Item->OptionOui->setChecked(true);
                    $Item->OptionNon->setChecked(false);
                    $Item->labelReferentielRadio->SetText(Prado::localize('TEXT_OUI'));
                } elseif ('0' == $codeRefPrinc) {
                    $Item->OptionOui->setChecked(false);
                    $Item->OptionNon->setChecked(true);
                    $Item->labelReferentielRadio->SetText(Prado::localize('TEXT_NON'));
                }
                $Item->oldValue->SetValue($codeRefPrinc);
            }
        }
    }

    /*
     * remplir repeater ref radio Lots
     */
    public function remplirRepeaterLot($ref)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $CommonConsultation = CommonConsultationPeer::retrieveByPK(base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id'])), $connexionCom);
        $dataLots = $CommonConsultation->getAllLots();
        $arrayDataSource = [];
        $index = 0;
        if (is_array($dataLots) && count($dataLots)) {
            foreach ($dataLots as $oneLot) {
                $arrayDataSource[$index]['lot'] = $oneLot->getLot();
                $arrayDataSource[$index]['codeLibelle'] = $ref->getCodeLibelle();
                $arrayDataSource[$index]['obligatoire'] = $ref->getObligatoire();
                $arrayDataSource[$index]['defaultValue'] = $ref->getDefaultValue();
                $arrayDataSource[$index]['id'] = $ref->getId();
                ++$index;
            }
        }
        $this->repeaterLot->DataSource = $arrayDataSource;
        $this->repeaterLot->DataBind();
    }
}
