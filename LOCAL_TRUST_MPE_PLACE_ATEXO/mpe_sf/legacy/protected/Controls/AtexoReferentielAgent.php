<?php

namespace Application\Controls;

class AtexoReferentielAgent extends MpeTTemplateControl
{
    public function afficherReferentielAgent($idAgent = null, $organisme = null, $postback = false)
    {
        $nbre = $this->ltRefAgent->afficherReferentielAgent($idAgent, $organisme);
        if (!$nbre) {
            $this->expertiseAgent->setVisible(false);
        }
    }

    public function saveReferentielAgent($idAgent, $organisme)
    {
        $this->ltRefAgent->saveReferentielAgent($idAgent, $organisme);
    }
}
