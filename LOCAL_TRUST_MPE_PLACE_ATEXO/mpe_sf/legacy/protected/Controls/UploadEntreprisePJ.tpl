<!--Debut Bloc Echange-->
<div class="form-field">
	<div class="form-horizontal content">
		<div tabindex="-1" role="dialog">
			<div role="document">
				<div>
					<div class="modal-body clearfix">
						<div class="line">

							<com:PanelMessageErreur visible="false" ID="panelMessageErreur"/>

							<!--Debut Bloc Infos echange-->
							<com:TActivePanel CssClass="panel panel-default" ID="PanelPJ">
								<div class="panel-heading">
									<h4 class="panel-title">
										<com:TTranslate>DEFINE_TEXT_PJ</com:TTranslate>
									</h4>
								</div>
								<div class="panel-body">
									<ul class="list-group">
										<com:TRepeater ID="RepeaterPiecesJointes" EnableViewState="true" DataKeyField="Id">
											<prop:ItemTemplate>
												<li class="list-group-item">
													<a style="display:<%#(!isset($_GET['idCom']))? '':'none'%>" href="<%#$this->Page->IdUploadPj->getLienTelechargementPj($this->Data)%>"><%#$this->Page->IdUploadPj->formaterNomFichierPj($this->Data)%><span class="info-aide"> (<%# Application\Service\Atexo\Atexo_Util::arrondirSizeFile($this->Data->getTaille()/1024)%>)</span></a>
													<a style="display:<%#(isset($_GET['idCom']))? '':'none'%>" href="<%#$this->Page->IdUploadPj->getLienTelechargementPj($this->Data)%>"><%#$this->Page->IdUploadPj->formaterNomFichierPj($this->Data)%><span class="info-aide"> (<%# Application\Service\Atexo\Atexo_Util::arrondirSizeFile($this->Data->getTaille()/1024)%>)</span></a>
													<com:TActiveLinkButton
															visible="<%#(new UploadPj())->getVisibiliteBoutonSupprimer($this->Data)%>"
															CssClass="btn btn-sm btn-danger pull-right"
															OnCallBack="Page.IdUploadPj.refreshRepeaterPJ"
															OnCommand="Page.IdUploadPj.onDeleteClick"
															CommandParameter="<%#$this->Data->getId()%>"
													>
														<i class="fa fa-trash"></i>
													</com:TActiveLinkButton>
												</li>
											</prop:ItemTemplate>
										</com:TRepeater>
									</ul>

								</div>
							</com:TActivePanel>
							<!--Fin Bloc Infos echange-->

							<!--Debut Bloc Ajout piece jointe-->
							<com:TPanel ID="mainPart" cssClass="panel panel-default content">
								<div class="panel-heading">
									<h4 class="panel-title">
										<com:TTranslate>DEFINE_TEXT_AJOUT_PJ</com:TTranslate>
									</h4>
								</div>
								<div class="panel-body">
									<div class="clearfix">
										<label class="control-label col-md-2 col-xs-2" for="<%=$this->ajoutFichier->getClientId()%>">
											<com:TTranslate>DEFINE_TEXT_FICHIER</com:TTranslate> :
										</label>
										<div class="col-md-9 col-xs-9">
											<com:AtexoFileUpload
													ID="ajoutFichier"
													Attributes.CssClass="file-580 float-left"
													Attributes.size="98"
													Attributes.OnChange="if(this.value) document.getElementById('<%=$this->hasFile->getClientId()%>').value='1'; else document.getElementById('<%=$this->hasFile->getClientId()%>').value='0';"
													Attributes.title="<%=Prado::localize('OBJET')%>"
											/>
											<com:THiddenField ID="hasFile" />
										</div>
										<div class="col-md-1 col-xs-1">
											<com:TLinkButton
													OnClick="onAjoutClick"
													cssClass="btn btn-sm btn-primary"
													Attributes.title="<%=Prado::localize('DEFINE_TEXT_AJOUT_COMME_PJ')%>">
												<i class="fa fa-plus"></i>
											</com:TLinkButton>
										</div>
										<span style="display:none;">
											<com:TActiveButton
													ID="refreshRepeater"
													Text="buttonRefreshPieceExternes"
													OnCallBack="Page.IdUploadPj.refreshRepeaterPJ"/>
										</span>
									</div>
								</div>
							</com:TPanel>
							<!--Fin Bloc Ajout piece jointe-->

							<com:TLabel ID="labelServerSide"/>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!--Fin Bloc Echange-->