<?php

namespace Application\Controls;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonTDonneeComplementaire;
use Application\Propel\Mpe\CommonTFormePrix;
use Application\Propel\Mpe\CommonTFormePrixHasRefTypePrix;
use Application\Propel\Mpe\CommonTFormePrixPfHasRefVariation;
use Application\Propel\Mpe\CommonTFormePrixPuHasRefVariation;
use Application\Propel\Mpe\CommonTLotTechnique;
use Application\Propel\Mpe\CommonTLotTechniqueHasTranche;
use Application\Propel\Mpe\CommonTLotTechniqueHasTranchePeer;
use Application\Propel\Mpe\CommonTLotTechniquePeer;
use Application\Propel\Mpe\CommonTTranche;
use Application\Propel\Mpe\CommonTTypeContratQuery;
use Application\Service\Atexo\Atexo_CommonReferentiel;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_FormePrix;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;
use Prado\Web\UI\WebControls\TRepeaterItemCollection;

class DonneesComplementaireFormeMarcheFormePrix extends MpeTTemplateControl
{
    private $mode;
    private $validationGroup;
    private $validationSummary;
    private $_donneeComplementaire;
    private bool $afficher = true;
    private ?string $idElement = null;
    // $sourcePage = ('etapeIdentification' si on est sur l'étape "identification")
    // $sourcePage = ('detailLots' si on est sur l'étape "detailLots")
    private ?string $sourcePage = null;

    final public const DISPLAY = 'display:';
    final public const DISPLAY_NONE = 'display:none';

    /**
     * recupère la valeur.
     */
    public function getIdElement()
    {
        return $this->idElement;
    }

    /**
     * Affecte la valeur.
     *
     * @param string $value
     */
    public function setIdElement($value)
    {
        $this->idElement = $value;
    }

    public function setAfficher($afficher)
    {
        $this->afficher = $afficher;
    }

    public function getAfficher()
    {
        return $this->afficher;
    }

    public function getDonneeComplementaire()
    {
        return $this->_donneeComplementaire;
    }

    public function setDonneeComplementaire($donneeComplementaire)
    {
        $this->_donneeComplementaire = $donneeComplementaire;
    }

    public function setValidationGroup($validationGroup)
    {
        $this->validationGroup = $validationGroup;
    }

    public function getValidationGroup()
    {
        return $this->validationGroup;
    }

    public function setValidationSummary($validationSummary)
    {
        $this->validationSummary = $validationSummary;
    }

    public function getValidationSummary()
    {
        return $this->validationSummary;
    }

    /**
     * Affecte la valeur.
     *
     * @param string $value
     */
    public function setMode($mode)
    {
        $this->mode = $mode;
    }

    /**
     * recupère la valeur.
     */
    public function getMode()
    {
        return $this->mode;
    }

    /**
     * recupère la valeur.
     */
    public function getSourcePage()
    {
        return $this->sourcePage;
    }

    // getSourcePage()

    /**
     * Affecte la valeur.
     *
     * @param string $value
     */
    public function setSourcePage($value)
    {
        if ($this->sourcePage !== $value) {
            $this->sourcePage = $value;
        }
    }

    // setSourcePage()

    public function onLoad($param)
    {
        if (Atexo_Module::isEnabled('DonneesComplementaires') && Atexo_Module::isEnabled('DonneesRedac')) {
            if (!$this->Page->IsPostBack) {
                self::chargerComposant();
            }
        }
    }

    /*
     * remplir la liste des formes prix
     */
    public function fillListeFormePrix()
    {
        $selectionnez = Prado::localize('TEXT_SELECTION_LISTE');
        $dataFormePrix = [Atexo_Config::getParameter('KEY_PRIX_SELECTIONNEZ') => $selectionnez, Atexo_Config::getParameter('KEY_PRIX_FORFAITAIRE') => Prado::localize('DEFINE_PRIX_FORFAITE'), Atexo_Config::getParameter('KEY_PRIX_UNITAIRE') => Prado::localize('TEXT_PRIX_UNITAIRE'), Atexo_Config::getParameter('KEY_PRIX_MIXTE') => Prado::localize('DEFINE_PRIX_MIXTE')];
        $this->formePrix->DataSource = $dataFormePrix;
        $this->formePrix->dataBind();
    }

    /*
     * remplir la liste des variation de prix
     */
    public function fillVariationPrix()
    {
        $refVariationPrix = (new Atexo_CommonReferentiel())->retrieveValeurReferentiel(Atexo_Config::getParameter('REFERENTIEL_VARIATION_PRIX'));
        if ($refVariationPrix) {
            $this->repeaterVariationPrixPF->DataSource = $refVariationPrix;
            $this->repeaterVariationPrixPF->DataBind();
            $this->repeaterVariationPrixPU->DataSource = $refVariationPrix;
            $this->repeaterVariationPrixPU->DataBind();

            $this->repeaterVariationPrixPUPM->DataSource = $refVariationPrix;
            $this->repeaterVariationPrixPUPM->DataBind();

            $this->repeaterVariationPrixPFPM->DataSource = $refVariationPrix;
            $this->repeaterVariationPrixPFPM->DataBind();
        }
    }

    /*
     * remplir la liste des types de prix
     */
    public function fillTypePrix()
    {
        $refTypePrix = (new Atexo_CommonReferentiel())->retrieveValeurReferentiel(Atexo_Config::getParameter('REFERENTIEL_TYPE_PRIX'));
        if ($refTypePrix) {
            $this->repeaterTypePrixPU->DataSource = $refTypePrix;
            $this->repeaterTypePrixPU->DataBind();
            $this->repeaterTypePrixPUPM->DataSource = $refTypePrix;
            $this->repeaterTypePrixPUPM->DataBind();
        }
    }

    /*
     * remplir la liste des unités
     */
    public function fillUnitePrixUnitaire()
    {
        $dataSource = [];
        $dataUnitePrixPUPM = [];
        $refsUnitePrix = (new Atexo_CommonReferentiel())->retrieveValeurReferentiel(Atexo_Config::getParameter('REFERENTIEL_UNITE_PRIX'));
        $firstElement = 0;
        if (is_array($refsUnitePrix)) {
            foreach ($refsUnitePrix as $oneVal) {
                $dataSource[$oneVal->getId()] = $oneVal->getLibelleValeurReferentielTraduit();
                if (!$firstElement) {
                    $firstElement = $oneVal->getId();
                }
                $dataUnitePrixPUPM[$oneVal->getId()] = $oneVal->getLibelleValeurReferentielTraduit();
            }
        }
        $this->puSelectUnite->DataSource = $dataSource;
        $this->puSelectUnite->dataBind();

        $this->pmPuSelectUnite->DataSource = $dataUnitePrixPUPM;
        $this->pmPuSelectUnite->dataBind();
        if ($firstElement) {
            $this->puSelectUnite->SelectedValue = $firstElement;
            $this->pmPuSelectUnite->SelectedValue = $firstElement;
            $this->puMaxLabel->Text = $this->puSelectUnite->SelectedItem->getText();
            $this->puMinLabel->Text = $this->puSelectUnite->SelectedItem->getText();
            $this->pmPuMaxLabel->Text = $this->pmPuSelectUnite->SelectedItem->getText();
            $this->pmPuMinLabel->Text = $this->pmPuSelectUnite->SelectedItem->getText();
        }
    }

    /*
     * remplir la liste des unités
     */
    public function fillRepeaterTranchesMarchePrix()
    {
        $dataTranches = [];
        $indexTranche = 0;
        $dataTranches[$indexTranche]['typeTranche'] = Atexo_Config::getParameter('TRANCHE_FIXE');
        $dataTranches[$indexTranche]['idTranche'] = '0';
        $dataTranches[$indexTranche]['idItemTranche'] = $indexTranche;
        $dataTranches[$indexTranche]['intituleTranche'] = prado::localize('DEFINE_INTITULE_TRANCHE');
        $dataTranches[$indexTranche]['formePrix'] = '';
        $dataTranches[$indexTranche]['estimationHt'] = '-';
        $dataTranches[$indexTranche]['supprimerTranche'] = false;
        $dataTranches[$indexTranche]['definir'] = Prado::localize('DEFINIR');
        ++$indexTranche;
        $dataTranches[$indexTranche]['typeTranche'] = Atexo_Config::getParameter('TRANCHE_CONDITIONNELLE');
        $dataTranches[$indexTranche]['idTranche'] = $indexTranche;
        $dataTranches[$indexTranche]['idItemTranche'] = $indexTranche;
        $dataTranches[$indexTranche]['intituleTranche'] = prado::localize('DEFINE_INTITULE_TRANCHE');
        $dataTranches[$indexTranche]['formePrix'] = '';
        $dataTranches[$indexTranche]['estimationHt'] = '-';
        $dataTranches[$indexTranche]['supprimerTranche'] = false;
        $dataTranches[$indexTranche]['definir'] = Prado::localize('DEFINIR');

        $this->listeTranchesMarchePrix->dataSource = $dataTranches;
        $this->listeTranchesMarchePrix->dataBind();

        $this->setViewState('nombresDesTranches', $indexTranche);
    }

    /*
     * Permet d'ajouter une nouvelle tranche
     */
    public function addTrancheMarchePrix($sender, $param)
    {
        $dataTranches = self::getDataTranchesMarchePrix();
        $indexTranche = $this->getIdTranchesMarchePrixNotExist($dataTranches);
        $nbrePiecesArajouter = 1;

        $nbrTranches = $this->getViewState('nombresDesTranches') + 1;
        $this->setViewState('nombresDesTranches', $nbrTranches);
        for ($i = 0; $i < $nbrePiecesArajouter; ++$i) {
            $dataTranches[$indexTranche]['typeTranche'] = Atexo_Config::getParameter('TRANCHE_CONDITIONNELLE');
            $dataTranches[$indexTranche]['idTranche'] = $indexTranche;
            $dataTranches[$indexTranche]['idItemTranche'] = $nbrTranches;
            $dataTranches[$indexTranche]['intituleTranche'] = prado::localize('DEFINE_INTITULE_TRANCHE');
            $dataTranches[$indexTranche]['formePrix'] = '';
            $dataTranches[$indexTranche]['estimationHt'] = '-';
            $dataTranches[$indexTranche]['supprimerTranche'] = true;
            $dataTranches[$indexTranche]['definir'] = Prado::localize('DEFINIR');
            $dataTranches[$indexTranche]['formPrixTC'] = 0;
            ++$indexTranche;
        }

        $idTrTOFirst = 1;
        if (
            isset($dataTranches[$idTrTOFirst])
            && $dataTranches[$idTrTOFirst]['typeTranche'] == Atexo_Config::getParameter('TRANCHE_CONDITIONNELLE')
        ) {
            $dataTranches[$idTrTOFirst]['supprimerTranche'] = true;
        }

        self::remplirTranches($dataTranches);
        $this->panelTranchesMarcheprix->render($param->getNewWriter());
    }

    /*
     * retourne la liste des tranche marche prix
     */
    public function getDataTranchesMarchePrix()
    {
        $indexTranche = 0;
        $dataTranches = [];

        foreach ($this->listeTranchesMarchePrix->getItems() as $item) {
            $dataTranches[$item->getItemIndex()]['typeTranche'] = $item->typeTranche->Text;
            $dataTranches[$item->getItemIndex()]['idTranche'] = $item->idTranche->Text;
            $dataTranches[$item->getItemIndex()]['idItemTranche'] = $item->idItemTranche->Value;
            $dataTranches[$item->getItemIndex()]['intituleTranche'] = $item->intituleTranche->Text;
            $dataTranches[$item->getItemIndex()]['formePrix'] = $item->formePrixTranche->Text;
            $dataTranches[$item->getItemIndex()]['estimationHt'] = $item->estimationHtTranche->Text;
            $dataTranches[$item->getItemIndex()]['supprimerTranche'] = $item->deleteTranche->Value;
            $dataTranches[$item->getItemIndex()]['definir'] = $item->linkDefinirActionFormePrixTranche->Text;
            $dataTranches[$item->getItemIndex()]['formPrixTC'] = $item->formPrixTC->value;
            ++$indexTranche;
        }

        return $dataTranches;
    }

    public function getIdTranchesMarchePrixNotExist(array $dataTranches): int
    {
        $dataIdTranchesExist = [];

        foreach ($this->listeTranchesMarchePrix->getItems() as $item) {
            $dataIdTranchesExist[] = $item->idTranche->Text;
        }

        $incrementedInteger = is_countable($dataTranches) ? count($dataTranches) : 0;

        while (in_array($incrementedInteger, $dataIdTranchesExist)) {
            $incrementedInteger++;
        }

        return $incrementedInteger;
    }

    /*
     * Permet de remplir le repeater des tranches
     */

    public function remplirTranches($dataTranches)
    {
        $this->listeTranchesMarchePrix->dataSource = $dataTranches;
        $this->listeTranchesMarchePrix->dataBind();
    }

    /*
     * Permet d'afficher le type d'unité de prix pour prix unitaire
     */
    public function displayPuAvecMinMax($sender, $param)
    {
        $label = $this->puSelectUnite->SelectedItem->getText();
        $this->puMaxLabel->Text = $label;
        $this->puMinLabel->Text = $label;
        if ($param) {
            $this->panelPuAvecSansMinMax->render($param->getNewWriter());
        }
    }

    /*
     * Permet d'afficher le type d'unité de prix pour prix mixte
     */
    public function displayPmPuAvecMinMax($sender, $param)
    {
        $label = $this->pmPuSelectUnite->SelectedItem->getText();
        $this->pmPuMaxLabel->Text = $label;
        $this->pmPuMinLabel->Text = $label;
        if ($param) {
            $this->panelPmPuAvecSansMinMax->render($param->getNewWriter());
        }
    }

    /*
     * Permet d'enregistrer une forme de prix
     */
    public function saveFormePrix($sender, $param)
    {
        $listeVariationsPrixUnitaire = [];
        $listeVariationsPrixForfaitaire = [];
        $listeTypesPrix = [];
        $formePrix = new CommonTFormePrix();
        $estimation = 0;
        $formePrix->setFormePrix($this->formePrix->SelectedValue);
        $formePrixSelectionne = false;
        if ($this->formePrix->SelectedValue == Atexo_Config::getParameter('KEY_PRIX_FORFAITAIRE')) {
            $formePrixSelectionne = true;
            $index = 0;
            $formePrix->setPfEstimationHt($this->pf_estimationInterneHT->Text);
            if ($this->pf_estimationInterneHT->Text) {
                $estimation = $this->pf_estimationInterneHT->Text;
            }

            $formePrix->setPfEstimationTtc($this->pf_estimationInterneTTC->Text);
            //enregistrement des variations prix forfaitaire
            foreach ($this->repeaterVariationPrixPF->Items as $item) {
                if ($item->pf_variation->checked) {
                    $variation = new CommonTFormePrixPfHasRefVariation();
                    $variation->setIdVariation($item->idPfVariation->value);
                    $listeVariationsPrixForfaitaire[$index] = $variation;
                    ++$index;
                }
            }
        } elseif ($this->formePrix->SelectedValue == Atexo_Config::getParameter('KEY_PRIX_UNITAIRE')) {
            $index = 0;
            $formePrixSelectionne = true;
            $formePrix->setPuEstimationHt($this->pu_estimationInterneHT->Text);
            $formePrix->setPuEstimationTtc($this->pu_estimationInterneTTC->Text);
            if ($this->pu_estimationInterneHT->Text) {
                $estimation = $this->pu_estimationInterneHT->Text;
            }
            if ($this->pu_bc->checked) {
                $modalite = Atexo_Config::getParameter('MODALITE_BON_COMMANDE');
                $formePrix->setModalite($modalite);

                if ($this->pu_bdcAvecMinimaxi->checked) {
                    $formePrix->setIdMinMax($this->puSelectUnite->SelectedValue);
                    $formePrix->setPuMax($this->puMax->Text);
                    $formePrix->setPuMin($this->puMin->Text);
                }
            } elseif ($this->pu_qd->checked) {
                $modalite = Atexo_Config::getParameter('MODALITE_QUANTITE_DEFINIER');
                $formePrix->setModalite($modalite);
            }

            //enregistrement des variations prix unitaire
            foreach ($this->repeaterVariationPrixPU->Items as $item) {
                if ($item->pu_variation->checked) {
                    $variation = new CommonTFormePrixPuHasRefVariation();
                    $variation->setIdVariation($item->idPUVariation->value);
                    $listeVariationsPrixUnitaire[$index] = $variation;
                    ++$index;
                }
            }

            $index2 = 0;
            //enregistrement les types prix unitaire
            foreach ($this->repeaterTypePrixPU->Items as $item) {
                if ($item->pu_prixCatalogue->checked) {
                    $type = new CommonTFormePrixHasRefTypePrix();
                    $type->setIdTypePrix($item->idPUTypePrix->value);
                    $listeTypesPrix[$index2] = $type;
                    ++$index2;
                }
            }
        } elseif ($this->formePrix->SelectedValue == Atexo_Config::getParameter('KEY_PRIX_MIXTE')) {
            $formePrixSelectionne = true;
            $formePrix->setPfEstimationHt($this->pm_pf_estimationInterneHT->Text);
            $formePrix->setPfEstimationTtc($this->pm_pf_estimationInterneTTC->Text);
            if ($this->pm_pf_estimationInterneHT->Text) {
                $estimation = $this->pm_pf_estimationInterneHT->Text;
            }

            $formePrix->setPuEstimationHt($this->pm_pu_estimationInterneHT->Text);
            $formePrix->setPuEstimationTtc($this->pm_pu_estimationInterneTTC->Text);

            if ($this->pm_pu_estimationInterneHT->Text) {
                $estimation = $estimation + $this->pm_pu_estimationInterneHT->Text;
            }

            if ($this->pm_pu_bc->checked) {
                $modalite = Atexo_Config::getParameter('MODALITE_BON_COMMANDE');
                $formePrix->setModalite($modalite);
                if ($this->pm_pu_bdcAvecMinimaxi->checked) {
                    $formePrix->setIdMinMax($this->pmPuSelectUnite->SelectedValue);
                    $formePrix->setPuMin($this->pmPuMin->Text);
                    $formePrix->setPuMax($this->pmPuMax->Text);
                }
            } elseif ($this->pm_pu_qd->checked) {
                $modalite = Atexo_Config::getParameter('MODALITE_QUANTITE_DEFINIER');
                $formePrix->setModalite($modalite);
            }

            // enregistrement de la variation prix forfaitaire
            $index = 0;
            foreach ($this->repeaterVariationPrixPFPM->Items as $item) {
                if ($item->pm_pf_variation->checked) {
                    $variation = new CommonTFormePrixPfHasRefVariation();
                    $variation->setIdVariation($item->idPMPFVariation->value);
                    $listeVariationsPrixForfaitaire[$index] = $variation;
                    ++$index;
                }
            }

            // enregistrement de la variation prix Unitaire
            $index = 0;
            foreach ($this->repeaterVariationPrixPUPM->Items as $item) {
                if ($item->pm_pu_variation->checked) {
                    $variation = new CommonTFormePrixPuHasRefVariation();
                    $variation->setIdVariation($item->idPMPUVariation->value);
                    $listeVariationsPrixUnitaire[$index] = $variation;
                    ++$index;
                }
            }

            $index2 = 0;
            //enregistrement les types prix unitaire
            foreach ($this->repeaterTypePrixPUPM->Items as $item) {
                if ($item->pm_pu_prixCatalogue->checked) {
                    $type = new CommonTFormePrixHasRefTypePrix();
                    $type->setIdTypePrix($item->idPUPMTypePrix->value);
                    $listeTypesPrix[$index2] = $type;
                    ++$index2;
                }
            }
        }

        if ($formePrix->getFormePrix() == Atexo_Config::getParameter('KEY_PRIX_FORFAITAIRE')) {
            $labelFormePrix = Prado::localize('DEFINE_PRIX_FORFAITE');
        } elseif ($formePrix->getFormePrix() == Atexo_Config::getParameter('KEY_PRIX_UNITAIRE')) {
            $labelFormePrix = Prado::localize('TEXT_PRIX_UNITAIRE');
        } elseif ($formePrix->getFormePrix() == Atexo_Config::getParameter('KEY_PRIX_MIXTE')) {
            $labelFormePrix = Prado::localize('DEFINE_PRIX_MIXTE');
        }

        $res = [];
        if (null != $this->idItemToAddFormePrix->value) {
            $res = ['formePrix' => $formePrix, 'typesPrix' => $listeTypesPrix, 'variationsPrixForfaitaire' => $listeVariationsPrixForfaitaire, 'variationsPrixUnitaire' => $listeVariationsPrixUnitaire];
            if (!$this->marcheTranche->Checked) {
                //validation forme prix Js
                if ('etapeIdentification' == self::getSourcePage()) {
                    $this->Page->bloc_etapeDonneesComplementaires->formPrix->value = '1';
                }
                $this->formPrix->value = '1';
                // Marcher sans des tranches
                $this->setViewState('DetailsFormePrix', $res);
                if (!$estimation) {
                    $estimation = '-';
                } else {
                    $estimation = Atexo_Util::getMontantArronditEspace($estimation).' '.Prado::localize('TEXT_CURRENCY');
                }
                $this->estimationHTSansTranche->Text = $estimation;
                $this->formePrixSansTranche->Text = $labelFormePrix;
                $this->formePrixSansTrancheHidden->Value = $labelFormePrix;
                $this->displayFormePrixInfos($formePrix);

                $this->panelFormePrixSansTranche->render($param->getNewWriter());
            } else {
                //Forme prix pour une tranche
                $this->setViewState('DetailsFormePrix_'.$this->idItemToAddFormePrix->value, $res);

                $dataTranches = self::getDataTranchesMarchePrix();

                if (!$estimation) {
                    $estimation = '-';
                } else {
                    $estimation = Atexo_Util::getMontantArronditEspace($estimation).' '.Prado::localize('TEXT_CURRENCY');
                }
                foreach ($this->listeTranchesMarchePrix->getItems() as $item) {
                    if ($this->idItemToAddFormePrix->value === $item->idItemTranche->Value) {
                        $dataTranches[$item->indexItemTranche->value]['estimationHt'] = $estimation;
                        $dataTranches[$item->indexItemTranche->value]['formePrix'] = $labelFormePrix;
                        $dataTranches[$item->indexItemTranche->value]['formPrixTC'] = 1;
                        //                      $item->getItemIndex()
                        $item->formPrixTC->value = '1';
                    }
                }

                self::remplirTranches($dataTranches);
                $this->panelTranchesMarcheprix->render($param->getNewWriter());
            }
        }
        $this->scriptJs->Text = "<script>J('.modal-form-prix').dialog('close');</script>";
    }

    /*
     * Permet de supprimer une tranche
     */
    public function deleteTranche($sender, $param)
    {
        $indexT = $param->CallbackParameter;
        $indexTranche = 0;
        $dataTranchesNew = [];
        $idTrTOFirst = 1;

        foreach ($this->listeTranchesMarchePrix->getItems() as $item) {
            if ($item->indexItemTranche->Value != $indexT) {
                $dataTranchesNew[$indexTranche]['typeTranche'] = $item->typeTranche->Text;
                $dataTranchesNew[$indexTranche]['idTranche'] = $item->idTranche->Text;
                $dataTranchesNew[$indexTranche]['idItemTranche'] = $item->idItemTranche->Value;
                $dataTranchesNew[$indexTranche]['intituleTranche'] = $item->intituleTranche->Text;
                $dataTranchesNew[$indexTranche]['formePrix'] = $item->formePrixTranche->Text;
                $dataTranchesNew[$indexTranche]['estimationHt'] = $item->estimationHtTranche->Text;
                $dataTranchesNew[$indexTranche]['supprimerTranche'] = $item->deleteTranche->Value;
                $dataTranchesNew[$indexTranche]['definir'] = $item->linkDefinirActionFormePrixTranche->Text;
                $dataTranchesNew[$indexTranche]['formPrixTC'] = $item->formPrixTC->value;

                ++$indexTranche;
            } else {
                $this->setViewState('DetailsFormePrix_'.$item->idItemTranche->Value, null);
            }
        }

        if (
            isset($dataTranchesNew[$idTrTOFirst])
            && $dataTranchesNew[$idTrTOFirst]['typeTranche'] == Atexo_Config::getParameter('TRANCHE_CONDITIONNELLE')
            && is_array($dataTranchesNew)
            && count($dataTranchesNew) <= 2
        ) {
            $dataTranchesNew[$idTrTOFirst]['supprimerTranche'] = false;
        }

        self::remplirTranches($dataTranchesNew);
        $this->panelTranchesMarcheprix->render($param->getNewWriter());
    }

    /*
     * Permet d'afficher popupFormePrix on passant le parametre qui y a déclancé l'appelle
     */

    public function definirFormePrixTranche($sender, $param)
    {
        $this->scriptJs->Text = '';
        if ($param->CallbackParameter) {
            $definirElements = $param->CallbackParameter;
            $idsElement = explode('#', $definirElements);
            if (is_array($idsElement) && count($idsElement) > 1) {
                $this->idItemToAddFormePrix->value = $idsElement[1];
                if (!$this->marcheTranche->Checked) {
                    if ($this->getViewState('DetailsFormePrix')) {
                        $script = $this->afficherDonneFormePrixFromViewState($this->getViewState('DetailsFormePrix'));
                    } else {
                        $script = "viderFormePrixConsultation('".$this->getIdComposantFromParent()."');";
                    }
                } else {
                    if ($this->getViewState('DetailsFormePrix_'.$this->idItemToAddFormePrix->value)) {
                        $script = $this->afficherDonneFormePrixFromViewState($this->getViewState('DetailsFormePrix_'.$this->idItemToAddFormePrix->value));
                    } else {
                        $script = "viderFormePrixConsultation('".$this->getIdComposantFromParent()."');";
                    }
                }
                $this->scriptJs->Text = '<script>'.$script." document.getElementById('".$idsElement[0]."').click();</script>";
            }
        }

        // Vérification Accord Cadre
        if ($this->isTypeMarcheAccordCadre()) {
            $this->disableButtonWithoutMinMaxAccordCadre('_donneeFormePrix_pu_bdcSansMinimaxi');
            $this->disableButtonWithoutMinMaxAccordCadre('_donneeFormePrix_pm_pu_bdcSansMinimaxi');
        }
    }

    /*
     * Permet d'afficher les données d'un forme prix déjà saisie
     */
    public function afficherDonneFormePrixFromViewState($formePrixData)
    {
        $script = '';
        if (is_array($formePrixData) && count($formePrixData)) {
            if ($formePrixData['formePrix']) {
                $formePrix = $formePrixData['formePrix'];
                if ($formePrix instanceof CommonTFormePrix) {
                    $typeForme = $formePrix->getFormePrix();
                    if ($typeForme == Atexo_Config::getParameter('KEY_PRIX_FORFAITAIRE')) {
                        $this->formePrix->SelectedValue = Atexo_Config::getParameter('KEY_PRIX_FORFAITAIRE');

                        $this->pf_estimationInterneHT->Text = $formePrix->getPfEstimationHt();
                        $this->pf_estimationInterneTTC->Text = $formePrix->getPfEstimationTtc();
                        //récuperation des variations prix forfaitaire
                        $variationsPF = $formePrixData['variationsPrixForfaitaire'];
                        if (is_array($variationsPF) && 1 === count($variationsPF)) {
                            foreach ($variationsPF as $variationPF) {
                                foreach ($this->repeaterVariationPrixPF->Items as $item) {
                                    if ($item->idPfVariation->value == $variationPF->getIdVariation()) {
                                        $item->pf_variation->checked = true;
                                    }
                                }
                            }
                        }
                    } elseif ($typeForme == Atexo_Config::getParameter('KEY_PRIX_UNITAIRE')) {
                        $this->formePrix->SelectedValue = Atexo_Config::getParameter('KEY_PRIX_UNITAIRE');
                        $this->pu_estimationInterneHT->Text = $formePrix->getPuEstimationHt();
                        $this->pu_estimationInterneTTC->Text = $formePrix->getPuEstimationTtc();

                        if ($formePrix->getModalite() == Atexo_Config::getParameter('MODALITE_BON_COMMANDE')) {
                            $script .= "showDiv('pu_infos-min-max'); ";
                            $this->pu_bc->checked = true;
                            if ($formePrix->getIdMinMax()) {
                                $this->pu_bdcAvecMinimaxi->checked = true;
                                $script .= "isCheckedShowDiv(document.getElementById('".$this->pu_bdcAvecMinimaxi->getClientId()."'),'pu_bdcAvecMinimaxi_selectUnite');isCheckedShowDiv(document.getElementById('".$this->pu_bdcAvecMinimaxi->getClientId()."'),'pu_avecMiniMaxi'); ";
                                $this->puSelectUnite->SelectedValue = $formePrix->getIdMinMax();
                                $this->puMaxLabel->Text = $this->puSelectUnite->SelectedItem->getText();
                                $this->puMinLabel->Text = $this->puSelectUnite->SelectedItem->getText();

                                $this->puMax->Text = $formePrix->getPuMax();
                                $this->puMin->Text = $formePrix->getPuMin();
                            } else {
                                $this->pu_bdcSansMinimaxi->checked = true;
                                $script .= "isCheckedHideDiv(document.getElementById('".$this->pu_bdcSansMinimaxi->getClientId()."'),'pu_bdcAvecMinimaxi_selectUnite');isCheckedHideDiv(document.getElementById('".$this->pu_bdcSansMinimaxi->getClientId()."'),'pu_avecMiniMaxi'); ";
                            }
                            //                          isCheckedShowDiv(document.getElementById('".$this->pm_pu_bdcAvecMinimaxi->getClientId()."'),'pm_pu_bdcAvecMinimaxi_selectUnite');isCheckedShowDiv(document.getElementById('".$this->pm_pu_bdcAvecMinimaxi->getClientId()."'),'pm_pu_avecMiniMaxi');showDiv('pm_pu_infos-min-max');
                        } elseif ($formePrix->getModalite() == Atexo_Config::getParameter('MODALITE_QUANTITE_DEFINIER')) {
                            $this->pu_qd->checked = true;
                            $script .= "hideDiv('pu_infos-min-max'); ";
                        }

                        //récuperer des variations prix unitaire
                        $variationsPU = $formePrixData['variationsPrixUnitaire'];
                        if (is_array($variationsPU) && 1 === count($variationsPU)) {
                            foreach ($variationsPU as $variationPU) {
                                foreach ($this->repeaterVariationPrixPU->Items as $item) {
                                    if ($item->idPUVariation->value == $variationPU->getIdVariation()) {
                                        $item->pu_variation->checked = true;
                                    }
                                }
                            }
                        }

                        //récuperer les types prix unitaire
                        $typesPU = $formePrixData['typesPrix'];
                        if (is_array($typesPU) && count($typesPU)) {
                            foreach ($typesPU as $typePU) {
                                foreach ($this->repeaterTypePrixPU->Items as $item) {
                                    if ($item->idPUTypePrix->value == $typePU->getIdTypePrix()) {
                                        $item->pu_prixCatalogue->checked = true;
                                    }
                                }
                            }
                        }
                    } elseif ($typeForme == Atexo_Config::getParameter('KEY_PRIX_MIXTE')) {
                        $this->formePrix->SelectedValue = Atexo_Config::getParameter('KEY_PRIX_MIXTE');
                        $this->pm_pf_estimationInterneHT->Text = $formePrix->getPfEstimationHt();
                        $this->pm_pf_estimationInterneTTC->Text = $formePrix->getPfEstimationTtc();

                        $this->pm_pu_estimationInterneHT->Text = $formePrix->getPuEstimationHt();
                        $this->pm_pu_estimationInterneTTC->Text = $formePrix->getPuEstimationTtc();

                        if ($formePrix->getModalite() == Atexo_Config::getParameter('MODALITE_BON_COMMANDE')) {
                            $this->pm_pu_bc->checked = true;
                            $script .= "showDiv('pm_pu_infos-min-max'); ";
                            if ($formePrix->getIdMinMax()) {
                                $this->pm_pu_bdcAvecMinimaxi->checked = true;
                                $script .= "isCheckedShowDiv(document.getElementById('".$this->pm_pu_bdcAvecMinimaxi->getClientId()."'),'pm_pu_bdcAvecMinimaxi_selectUnite');isCheckedShowDiv(document.getElementById('".$this->pm_pu_bdcAvecMinimaxi->getClientId()."'),'pm_pu_avecMiniMaxi'); ";
                                $this->pmPuSelectUnite->SelectedValue = $formePrix->getIdMinMax();
                                $this->pmPuMaxLabel->Text = $this->pmPuSelectUnite->SelectedItem->getText();
                                $this->pmPuMinLabel->Text = $this->pmPuSelectUnite->SelectedItem->getText();

                                $this->pmPuMax->Text = $formePrix->getPuMax();
                                $this->pmPuMin->Text = $formePrix->getPuMin();
                            } else {
                                $this->pm_pu_bdcSansMinimaxi->checked = true;
                                $script .= "isCheckedHideDiv(document.getElementById('".$this->pm_pu_bdcSansMinimaxi->getClientId()."'),'pm_pu_bdcAvecMinimaxi_selectUnite');isCheckedHideDiv(document.getElementById('".$this->pm_pu_bdcSansMinimaxi->getClientId()."'),'pm_pu_avecMiniMaxi'); ";
                            }
                        } elseif ($formePrix->getModalite() == Atexo_Config::getParameter('MODALITE_QUANTITE_DEFINIER')) {
                            $this->pm_pu_qd->checked = true;
                            $script .= "hideDiv('pm_pu_infos-min-max'); ";
                        }
                        //récuperation des variations prix forfaitaire
                        $variationsPF = $formePrixData['variationsPrixForfaitaire'];
                        if (is_array($variationsPF) && 1 === count($variationsPF)) {
                            foreach ($variationsPF as $variationPF) {
                                foreach ($this->repeaterVariationPrixPFPM->Items as $item) {
                                    if ($item->idPMPFVariation->value == $variationPF->getIdVariation()) {
                                        $item->pm_pf_variation->checked = true;
                                    }
                                }
                            }
                        }

                        //récuperer des variations prix unitaire
                        $variationsPU = $formePrixData['variationsPrixUnitaire'];
                        if (is_array($variationsPU) && 1 === count($variationsPU)) {
                            foreach ($variationsPU as $variationPU) {
                                foreach ($this->repeaterVariationPrixPUPM->Items as $item) {
                                    if ($item->idPMPUVariation->value == $variationPU->getIdVariation()) {
                                        $item->pm_pu_variation->checked = true;
                                    }
                                }
                            }
                        }

                        //récuperer les types prix unitaire
                        $typesPU = $formePrixData['typesPrix'];
                        if (is_array($typesPU) && count($typesPU)) {
                            foreach ($typesPU as $typePU) {
                                foreach ($this->repeaterTypePrixPUPM->Items as $item) {
                                    if ($item->idPUPMTypePrix->value == $typePU->getIdTypePrix()) {
                                        $item->pm_pu_prixCatalogue->checked = true;
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                $script .= "viderFormePrixConsultation('".$this->getIdComposantFromParent()."');";
            }
            $script .= "displayOptionChoice(document.getElementById('".$this->formePrix->getClientId()."'));";
        }

        return $script;
    }

    /*
     * Permet d'enregistrer les élement de forme prix dans la base de donnée
     * @param : $objet
     */
    public function save(&$donneComplementaire)
    {
        $tranchesaved = [];
        if ($donneComplementaire instanceof CommonTDonneeComplementaire) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            // supprimer tout les tranche et et les formes de rpix lié à ceette donnee complementaire
            $formePrix = $donneComplementaire->getFormePrix();
            if ($formePrix instanceof CommonTFormePrix) {
                $listeTypesPrix = $formePrix->getAllTypesPrix();
                $listeVariationsPrixForfaitaire = $formePrix->getAllVariationsPrixForfaitaire();
                $listeVariationsPrixUnitaire = $formePrix->getAllVariationsPrixUnitaire();
                // supprimer les types prix
                if (is_array($listeTypesPrix) && count($listeTypesPrix)) {
                    foreach ($listeTypesPrix as $typePrix) {
                        if ($typePrix instanceof CommonTFormePrixHasRefTypePrix) {
                            $typePrix->delete($connexion);
                        }
                    }
                }
                // supprimer les variation prix forfotaire
                if (is_array($listeVariationsPrixForfaitaire) && count($listeVariationsPrixForfaitaire)) {
                    foreach ($listeVariationsPrixForfaitaire as $variationPF) {
                        if ($variationPF instanceof CommonTFormePrixPfHasRefVariation) {
                            $variationPF->delete($connexion);
                        }
                    }
                }

                // supprimer les variation prix forfotaire
                if (is_array($listeVariationsPrixUnitaire) && count($listeVariationsPrixUnitaire)) {
                    foreach ($listeVariationsPrixUnitaire as $variationPU) {
                        if ($variationPU instanceof CommonTFormePrixPuHasRefVariation) {
                            $variationPU->delete($connexion);
                        }
                    }
                }

                $formePrix->delete($connexion);
                $donneComplementaire->setIdFormePrix(null);
            } else {
                $listeTranches = $donneComplementaire->getAllTranches();
                if (is_array($listeTranches) && count($listeTranches)) {
                    foreach ($listeTranches as $tranche) {
                        $formePrix = $tranche->getFormePrix();
                        if ($formePrix instanceof CommonTFormePrix) {
                            $listeTypesPrix = $formePrix->getAllTypesPrix();
                            $listeVariationsPrixForfaitaire = $formePrix->getAllVariationsPrixForfaitaire();
                            $listeVariationsPrixUnitaire = $formePrix->getAllVariationsPrixUnitaire();
                            // supprimer les types prix
                            if (is_array($listeTypesPrix) && count($listeTypesPrix)) {
                                foreach ($listeTypesPrix as $typePrix) {
                                    if ($typePrix instanceof CommonTFormePrixHasRefTypePrix) {
                                        $typePrix->delete($connexion);
                                    }
                                }
                            }
                            // supprimer les variation prix forfotaire
                            if (is_array($listeVariationsPrixForfaitaire) && count($listeVariationsPrixForfaitaire)) {
                                foreach ($listeVariationsPrixForfaitaire as $variationPF) {
                                    if ($variationPF instanceof CommonTFormePrixPfHasRefVariation) {
                                        $variationPF->delete($connexion);
                                    }
                                }
                            }

                            // supprimer les variation prix forfotaire
                            if (is_array($listeVariationsPrixUnitaire) && count($listeVariationsPrixUnitaire)) {
                                foreach ($listeVariationsPrixUnitaire as $variationPU) {
                                    if ($variationPU instanceof CommonTFormePrixPuHasRefVariation) {
                                        $variationPU->delete($connexion);
                                    }
                                }
                            }
                            $lotHasTranche = $tranche->getAllLotTechniqueHasTranche();
                            foreach ($lotHasTranche as $oneLotHasTranche) {
                                CommonTLotTechniqueHasTranchePeer::doDelete($oneLotHasTranche, $connexion);
                            }
                            $tranche->delete($connexion);
                            $formePrix->delete($connexion);
                        } else {
                            $lotHasTranche = $tranche->getAllLotTechniqueHasTranche();
                            foreach ($lotHasTranche as $oneLotHasTranche) {
                                CommonTLotTechniqueHasTranchePeer::doDelete($oneLotHasTranche, $connexion);
                            }
                            $tranche->delete($connexion);
                        }
                    }
                }
            }

            if (!$this->marcheTranche->Checked) {
                $dataFormePrix = $this->getViewState('DetailsFormePrix');

                if (is_array($dataFormePrix) && count($dataFormePrix)) {
                    $FormePrix = $dataFormePrix['formePrix'];
                    if (($FormePrix instanceof CommonTFormePrix) && $FormePrix->getFormePrix() != Atexo_Config::getParameter('KEY_PRIX_SELECTIONNEZ')) {
                        $FormePrixNew = new CommonTFormePrix();
                        $FormePrixNew->setFormePrix($FormePrix->getFormePrix());
                        $FormePrixNew->setPfEstimationHt($FormePrix->getPfEstimationHt());
                        $FormePrixNew->setPfEstimationTtc($FormePrix->getPfEstimationTtc());
                        $FormePrixNew->setPuEstimationHt($FormePrix->getPuEstimationHt());
                        $FormePrixNew->setPuEstimationTtc($FormePrix->getPuEstimationTtc());
                        $FormePrixNew->setPuDateValeur($FormePrix->getPuDateValeur());
                        $FormePrixNew->setModalite($FormePrix->getModalite());

                        $FormePrixNew->setIdMinMax($FormePrix->getIdMinMax());
                        $FormePrixNew->setPuMax($FormePrix->getPuMax());
                        $FormePrixNew->setPuMin($FormePrix->getPuMin());
                        $FormePrixNew->save($connexion);

                        $typesPrix = $dataFormePrix['typesPrix'];
                        if (is_array($typesPrix)) {
                            foreach ($typesPrix as $typePrix) {
                                $typePrixNew = new CommonTFormePrixHasRefTypePrix();
                                $typePrixNew->setIdTypePrix($typePrix->getIdTypePrix());
                                $typePrixNew->setIdFormePrix($FormePrixNew->getIdFormePrix());
                                $typePrixNew->save($connexion);
                            }
                        }
                        $variationsPF = $dataFormePrix['variationsPrixForfaitaire'];
                        if (is_array($variationsPF)) {
                            foreach ($variationsPF as $variationPF) {
                                $variationPFNew = new CommonTFormePrixPfHasRefVariation();
                                $variationPFNew->setIdVariation($variationPF->getIdVariation());
                                $variationPFNew->setIdFormePrix($FormePrixNew->getIdFormePrix());
                                $variationPFNew->save($connexion);
                            }
                        }

                        $variationsPU = $dataFormePrix['variationsPrixUnitaire'];
                        if (is_array($variationsPU)) {
                            foreach ($variationsPU as $variationPU) {
                                $variationPUNew = new CommonTFormePrixPuHasRefVariation();
                                $variationPUNew->setIdVariation($variationPU->getIdVariation());
                                $variationPUNew->setIdFormePrix($FormePrixNew->getIdFormePrix());
                                $variationPUNew->save($connexion);
                            }
                        }
                        $donneComplementaire->setIdFormePrix($FormePrixNew->getIdFormePrix());
                    }
                }
            } else {
                foreach ($this->listeTranchesMarchePrix->getItems() as $item) {
                    $tranche = new CommonTTranche();
                    $tranche->setNatureTranche($item->typeTranche->Text);
                    $tranche->setCodeTranche($item->idTranche->Text);
                    $tranche->setIntituleTranche((new Atexo_Config())->toPfEncoding($item->intituleTranche->Text));
                    $tranche->setIdDonneeComplementaire($donneComplementaire->getIdDonneeComplementaire());
                    $dataFormePrix = $this->getViewState('DetailsFormePrix_'.$item->idItemTranche->Value);
                    if (is_array($dataFormePrix) && count($dataFormePrix)) {
                        $FormePrix = $dataFormePrix['formePrix'];
                        if (($FormePrix instanceof CommonTFormePrix) && $FormePrix->getFormePrix() != Atexo_Config::getParameter('KEY_PRIX_SELECTIONNEZ')) {
                            $FormePrixNew = new CommonTFormePrix();
                            $FormePrixNew->setFormePrix($FormePrix->getFormePrix());
                            $FormePrixNew->setPfEstimationHt($FormePrix->getPfEstimationHt());
                            $FormePrixNew->setPfEstimationTtc($FormePrix->getPfEstimationTtc());
                            $FormePrixNew->setPuEstimationHt($FormePrix->getPuEstimationHt());
                            $FormePrixNew->setPuEstimationTtc($FormePrix->getPuEstimationTtc());
                            $FormePrixNew->setPuDateValeur($FormePrix->getPuDateValeur());
                            $FormePrixNew->setModalite($FormePrix->getModalite());

                            $FormePrixNew->setIdMinMax($FormePrix->getIdMinMax());
                            $FormePrixNew->setPuMax($FormePrix->getPuMax());
                            $FormePrixNew->setPuMin($FormePrix->getPuMin());
                            $FormePrixNew->save($connexion);

                            $typesPrix = $dataFormePrix['typesPrix'];
                            if (is_array($typesPrix)) {
                                foreach ($typesPrix as $typePrix) {
                                    $typePrixNew = new CommonTFormePrixHasRefTypePrix();
                                    $typePrixNew->setIdTypePrix($typePrix->getIdTypePrix());
                                    $typePrixNew->setIdFormePrix($FormePrixNew->getIdFormePrix());
                                    $typePrixNew->save($connexion);
                                }
                            }

                            $variationsPF = $dataFormePrix['variationsPrixForfaitaire'];
                            if (is_array($variationsPF)) {
                                foreach ($variationsPF as $variationPF) {
                                    $variationPFNew = new CommonTFormePrixPfHasRefVariation();
                                    $variationPFNew->setIdVariation($variationPF->getIdVariation());
                                    $variationPFNew->setIdFormePrix($FormePrixNew->getIdFormePrix());
                                    $variationPFNew->save($connexion);
                                }
                            }

                            $variationsPU = $dataFormePrix['variationsPrixUnitaire'];
                            if (is_array($variationsPU)) {
                                foreach ($variationsPU as $variationPU) {
                                    $variationPUNew = new CommonTFormePrixPuHasRefVariation();
                                    $variationPUNew->setIdVariation($variationPU->getIdVariation());
                                    $variationPUNew->setIdFormePrix($FormePrixNew->getIdFormePrix());
                                    $variationPUNew->save($connexion);
                                }
                            }

                            $tranche->setIdFormePrix($FormePrixNew->getIdFormePrix());
                        }
                    }
                    $tranche->save($connexion);
                    $tranchesaved[$item->idItemTranche->value] = $tranche->getIdTranche();
                }
                $this->setViewState('tranchesSaved', $tranchesaved);
            }
            self::saveLotTechnique($donneComplementaire);
            self::saveLotTechHastranche();

            if ($this->contratFUEOui->checked) {
                $donneComplementaire->setProjetFinanceFondsUnionEuropeenne(1);
                $donneComplementaire->setIdentificationProjet($this->identificationProjet->getSafeText());
            } elseif ($this->contratFUENon->checked) {
                $donneComplementaire->setProjetFinanceFondsUnionEuropeenne(0);
                $donneComplementaire->setIdentificationProjet('');
            }
        }
    }

    /*
     * Permet d'afficher les données de forme du marché et forme(s) de prix
     * @param : $donneComplementaire
     */
    public function afficherDataFormeMarcheFormePrix($donneeComplementaire = null)
    {
        if (!$this->Page->IsPostBack) {
            $this->formPrix->value = '0';
            if ($donneeComplementaire instanceof CommonTDonneeComplementaire) {
                self::populateLotTechnique($donneeComplementaire);
                $estimation = 0;
                if ($donneeComplementaire->getIdFormePrix()) {
                    self::fillRepeaterTranchesMarchePrix();
                    // marché sans des tranches
                    $formePrix = $donneeComplementaire->getFormePrix();
                    if ($formePrix instanceof CommonTFormePrix) {
                        if ('etapeIdentification' == self::getSourcePage()) {
                            $this->Page->bloc_etapeDonneesComplementaires->formPrix->value = '1';
                        }
                        $this->formPrix->value = '1';
                        $this->marcheTranche->Checked = false;
                        if ($formePrix->getPfEstimationHt()) {
                            $estimation = $formePrix->getPfEstimationHt();
                        }
                        if ($formePrix->getPuEstimationHt()) {
                            $estimation = $estimation + $formePrix->getPuEstimationHt();
                        }
                        if ($estimation) {
                            $this->estimationHTSansTranche->Text = Atexo_Util::getMontantArronditEspace($estimation).' '.Prado::localize('TEXT_CURRENCY');
                        } else {
                            $this->estimationHTSansTranche->Text = '-';
                        }

                        if ($formePrix->getFormePrix() == Atexo_Config::getParameter('KEY_PRIX_FORFAITAIRE')) {
                            $labelFormePrix = Prado::localize('DEFINE_PRIX_FORFAITE');
                        } elseif ($formePrix->getFormePrix() == Atexo_Config::getParameter('KEY_PRIX_UNITAIRE')) {
                            $labelFormePrix = Prado::localize('TEXT_PRIX_UNITAIRE');
                        } elseif ($formePrix->getFormePrix() == Atexo_Config::getParameter('KEY_PRIX_MIXTE')) {
                            $labelFormePrix = Prado::localize('DEFINE_PRIX_MIXTE');
                        }
                        $this->formePrixSansTranche->Text = $labelFormePrix;
                        $this->formePrixSansTrancheHidden->Value = $labelFormePrix;

                        $listeTypesPrix = $formePrix->getAllTypesPrix();
                        $listeVariationsPrixForfaitaire = $formePrix->getAllVariationsPrixForfaitaire();
                        $listeVariationsPrixUnitaire = $formePrix->getAllVariationsPrixUnitaire();

                        $res = ['formePrix' => $formePrix, 'typesPrix' => $listeTypesPrix, 'variationsPrixForfaitaire' => $listeVariationsPrixForfaitaire, 'variationsPrixUnitaire' => $listeVariationsPrixUnitaire];
                        $this->setViewState('DetailsFormePrix', $res);
                        $this->scriptJs->Text = "<script>togglePanel('".$this->linkTogglePanel->getClientId()."','".$this->panelSousFormeMarche->getClientId()."');</script>";

                        $this->loadFormePrixInfos($formePrix);
                    }
                } else {
                    // marché avec des tranches
                    $listeTranches = $donneeComplementaire->getAllTranches();
                    if (is_array($listeTranches) && count($listeTranches)) {
                        $this->marcheTranche->Checked = true;
                        $dataTranches = [];
                        $indexTranche = 0;
                        $idItemTranche = 0;
                        $firstTrancheTCTrouver = false;
                        foreach ($listeTranches as $tranche) {
                            $labelFormePrix = '';
                            $estimation = 0;
                            $listeTypesPrix = [];
                            $listeVariationsPrixForfaitaire = [];
                            $listeVariationsPrixUnitaire = [];
                            $formePrix = $tranche->getFormePrix();
                            if ($formePrix instanceof CommonTFormePrix) {
                                $dataTranches[$indexTranche]['formPrixTC'] = '1';
                                if ($formePrix->getPfEstimationHt()) {
                                    $estimation = $formePrix->getPfEstimationHt();
                                }
                                if ($formePrix->getPuEstimationHt()) {
                                    $estimation = $estimation + $formePrix->getPuEstimationHt();
                                }
                                if ($estimation) {
                                    $dataTranches[$indexTranche]['estimationHt'] = Atexo_Util::getMontantArronditEspace($estimation).' '.Prado::localize('TEXT_CURRENCY');
                                } else {
                                    $dataTranches[$indexTranche]['estimationHt'] = '-';
                                }
                                if ($formePrix->getFormePrix() == Atexo_Config::getParameter('KEY_PRIX_FORFAITAIRE')) {
                                    $labelFormePrix = Prado::localize('DEFINE_PRIX_FORFAITE');
                                } elseif ($formePrix->getFormePrix() == Atexo_Config::getParameter('KEY_PRIX_UNITAIRE')) {
                                    $labelFormePrix = Prado::localize('TEXT_PRIX_UNITAIRE');
                                } elseif ($formePrix->getFormePrix() == Atexo_Config::getParameter('KEY_PRIX_MIXTE')) {
                                    $labelFormePrix = Prado::localize('DEFINE_PRIX_MIXTE');
                                }

                                $listeTypesPrix = $formePrix->getAllTypesPrix();
                                $listeVariationsPrixForfaitaire = $formePrix->getAllVariationsPrixForfaitaire();
                                $listeVariationsPrixUnitaire = $formePrix->getAllVariationsPrixUnitaire();
                            } else {
                                $dataTranches[$indexTranche]['formPrixTC'] = '0';
                            }
                            $dataTranches[$indexTranche]['typeTranche'] = $tranche->getNatureTranche();
                            if ($tranche->getIdTranche() > $idItemTranche) {
                                $idItemTranche = $tranche->getIdTranche();
                            }
                            $dataTranches[$indexTranche]['idItemTranche'] = $tranche->getIdTranche();
                            $dataTranches[$indexTranche]['idTranche'] = $tranche->getCodeTranche();
                            $dataTranches[$indexTranche]['intituleTranche'] = $tranche->getIntituleTranche();
                            $dataTranches[$indexTranche]['formePrix'] = $labelFormePrix;
                            if ($tranche->getNatureTranche() == Atexo_Config::getParameter('TRANCHE_FIXE')) {
                                $dataTranches[$indexTranche]['supprimerTranche'] = false;
                            } else {
                                if (count($listeTranches) > 2) {
                                    $dataTranches[$indexTranche]['supprimerTranche'] = true;
                                } else {
                                    $dataTranches[$indexTranche]['supprimerTranche'] = false;
                                    $firstTrancheTCTrouver = true;
                                }
                            }

                            $dataTranches[$indexTranche]['definir'] = Prado::localize('DEFINIR');
                            $res = ['formePrix' => $formePrix, 'typesPrix' => $listeTypesPrix, 'variationsPrixForfaitaire' => $listeVariationsPrixForfaitaire, 'variationsPrixUnitaire' => $listeVariationsPrixUnitaire];
                            $this->setViewState('DetailsFormePrix_'.$tranche->getIdTranche(), $res);
                            ++$indexTranche;
                        }
                        $this->setViewState('nombresDesTranches', $idItemTranche);
                        self::remplirTranches($dataTranches);
                        $this->scriptJs->Text = "<script>togglePanel('".$this->linkTogglePanel->getClientId()."','".$this->panelSousFormeMarche->getClientId()."');isChecked2(document.getElementById('".$this->marcheTranche->getClientId()."'),'".$this->infos_tranches_lot->getClientId()."','".$this->layer_ss_tranche_lot->getClientId()."');</script>";
                    } else {
                        self::fillRepeaterTranchesMarchePrix();
                    }
                }
                if (1 == $donneeComplementaire->getProjetFinanceFondsUnionEuropeenne()) {
                    $this->contratFUEOui->setChecked(true);
                    $this->contratFUENon->setChecked(false);
                    $this->identificationProjet->setText($donneeComplementaire->getIdentificationProjet());
                    $this->panelIdentificationProjet->setStyle('display:');
                } elseif (0 == $donneeComplementaire->getProjetFinanceFondsUnionEuropeenne()) {
                    $this->contratFUEOui->setChecked(false);
                    $this->contratFUENon->setChecked(true);
                    $this->identificationProjet->setText('');
                    $this->panelIdentificationProjet->setStyle('display:none');
                }

                $this->displayFormePrixRow($donneeComplementaire?->getFormePrix());
            } else {
                $this->displayFormePrixRow();

                self::fillRepeaterTranchesMarchePrix();
            }
        }
    }

    /*
     * Permet d'afficher les elements du composant selon le mode choisi
     */
    public function enabledElement()
    {
        return match ($this->mode) {
            0, '0' => false,
            1, '1' => true,
            default => false,
        };
    }

    /*
     * on l'utilise si on veux afficher plusieurs instance de ce composant dans une même page
     */
    public function getIdPanel()
    {
        if ($this->idElement) {
            return $this->idElement;
        } else {
            return '';
        }
    }

    /****
     * PARTIE LOT TECHNIQUE ASO
     */

    /*
     * initialise le repeater des lots techniques
     */
    public function fillRepeaterLotTechnique()
    {
        $visible = false;
        if ($this->marcheTranche->checked) {
            $visible = true;
        }
        $dataLot = [];
        $indexLot = 1;
        $dataLot[$indexLot]['idLotTechnique'] = $indexLot;
        $dataLot[$indexLot]['intituleLotTechnique'] = prado::localize('DEFINE_INTITULE_LOT_TECHNIQUE');
        $dataLot[$indexLot]['definir'] = Prado::localize('DEFINIR');
        $dataLot[$indexLot]['lotPrincipal'] = true;
        $dataLot[$indexLot]['index'] = 0;
        $dataLot[$indexLot]['visibleAppartenance'] = $visible;
        ++$indexLot;
        $this->repeaterDetailLotsTechniques->dataSource = $dataLot;
        $this->repeaterDetailLotsTechniques->dataBind();

        //$this->setViewState('nombresDesLots',$indexLot);
    }

    /*
     * ajouter une nouvelle ligne lot
     */
    public function addLotTechnique($sender, $param)
    {
        $dataLot = $this->getDataLotsTechnique();
        $indexLot = is_countable($dataLot) ? count($dataLot) : 0;
        $nbrePiecesArajouter = 1;
        $nbrLots = is_countable($dataLot) ? count($dataLot) : 0;
        $visible = false;
        if ($this->marcheTranche->checked) {
            $visible = true;
        }
        for ($i = 0; $i < $nbrePiecesArajouter; ++$i) {
            $dataLot[$indexLot]['idLotTechnique'] = ++$nbrLots;
            $dataLot[$indexLot]['intituleLotTechnique'] = prado::localize('DEFINE_INTITULE_LOT_TECHNIQUE');
            $dataLot[$indexLot]['definir'] = Prado::localize('DEFINIR');
            $dataLot[$indexLot]['lotPrincipal'] = false;
            $dataLot[$indexLot]['index'] = $indexLot;
            $dataLot[$indexLot]['visibleAppartenance'] = $visible;
            //$nbrLots++ ;
            ++$indexLot;
        }
        $this->setViewState('nombresDesLots', $nbrLots);
        self::remplirLotTechnique($dataLot);
        $this->panelListeLotsTechnique->render($param->getNewWriter());
    }

    /*
     * retourne la liste des lots techniques
     */
    public function getDataLotsTechnique()
    {
        $indexLot = 0;
        $dataLot = [];
        $visible = false;
        if ($this->marcheTranche->checked) {
            $visible = true;
        }
        foreach ($this->repeaterDetailLotsTechniques->getItems() as $item) {
            $dataLot[$item->getItemIndex()]['idLotTechnique'] = $item->idLotTechnique->Text;
            $dataLot[$item->getItemIndex()]['intituleLotTechnique'] = $item->intituleLotTechnique->Text;
            $dataLot[$item->getItemIndex()]['lotPrincipal'] = $item->lotPrincipal->Checked;
            $dataLot[$item->getItemIndex()]['index'] = $item->deleteLot->Value;
            $dataLot[$item->getItemIndex()]['visibleAppartenance'] = $visible;
            ++$indexLot;
        }

        return $dataLot;
    }

    public function remplirLotTechnique($dataLot)
    {
        $this->repeaterDetailLotsTechniques->dataSource = $dataLot;
        $this->repeaterDetailLotsTechniques->dataBind();
    }

    public function deleteLot($sender, $param)
    {
        $indexL = $param->CallbackParameter;
        $indexLot = 0;
        $dataLot = [];
        $visible = false;
        if ($this->marcheTranche->checked) {
            $visible = true;
        }
        foreach ($this->repeaterDetailLotsTechniques->getItems() as $item) {
            if ($item->deleteLot->Value != $indexL) {
                $dataLot[$indexLot]['idLotTechnique'] = $item->idLotTechnique->Text;
                $dataLot[$indexLot]['intituleLotTechnique'] = $item->intituleLotTechnique->Text;
                $dataLot[$indexLot]['lotPrincipal'] = $item->lotPrincipal->Checked;
                $dataLot[$indexLot]['index'] = $item->deleteLot->Value;
                $dataLot[$indexLot]['definir'] = Prado::localize('DEFINIR');
                $dataLot[$indexLot]['visibleAppartenance'] = $visible;
                ++$indexLot;
            }
        }
        self::remplirLotTechnique($dataLot);
        $this->panelListeLotsTechnique->render($param->getNewWriter());
    }

    public function definirApprtenanceTranche($sender, $param)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        if ($param->CallbackParameter) {
            $definirElements = $param->CallbackParameter;
            $idsElement = explode('#', $definirElements);
            if (is_array($idsElement) && count($idsElement) > 1) {
                $this->indexLotTechnique->value = $idsElement[1];
                if ($this->marcheTranche->Checked) {
                    $dataTranchesExist = self::getDataTranchesMarchePrix();
                    $this->repeaterTranchesRattachement->dataSource = $dataTranchesExist;
                    $this->repeaterTranchesRattachement->dataBind();
                    $trancheLot = $this->getViewState('tranchesLot_'.$this->indexLotTechnique->value);
                    if (!$trancheLot) {
                        foreach ($this->repeaterDetailLotsTechniques->getItems() as $item) {
                            if ('' != $item->idObjtLotTech->value && $this->indexLotTechnique->value == $item->deleteLot->value) {
                                $oneLot = CommonTLotTechniquePeer::retrieveByPK($item->idObjtLotTech->value, $connexion);
                                if ($oneLot) {
                                    $lotHasTranche = $oneLot->getAllLotTechniqueTranche();
                                    if (is_array($lotHasTranche) && count($lotHasTranche)) {
                                        foreach ($lotHasTranche as $uneAppartenance) {
                                            foreach ($this->repeaterTranchesRattachement->getItems() as $item) {
                                                if ($item->indexTranche->value == $uneAppartenance->getIdTranche()) {
                                                    $listeTranchesLot[] = $uneAppartenance->getIdTranche();
                                                }
                                            }
                                        }
                                        $this->setViewState('tranchesLot_'.$this->indexLotTechnique->value, $listeTranchesLot);
                                        $trancheLot = $this->getViewState('tranchesLot_'.$this->indexLotTechnique->value);
                                    }
                                }
                            }
                        }
                    }
                    if ($trancheLot) {
                        foreach ($this->repeaterTranchesRattachement->getItems() as $item) {
                            if (in_array($item->indexTranche->value, $trancheLot)) {
                                $item->appartenanceTranche->checked = true;
                            }
                        }
                    }
                    $this->panelTrancheRattachement->render($param->getNewWriter());
                    $this->scriptJs->Text = "<script>document.getElementById('".$idsElement[0]."').click();</script>";
                }
            }
        }
    }

    public function SaveLotTracheRattachement($sender, $param)
    {
        $listeTranchesLot = [];
        $atleastOne = false;
        $index = $this->indexLotTechnique->value;
        foreach ($this->repeaterTranchesRattachement->getItems() as $item) {
            if (true == $item->appartenanceTranche->checked) {
                $listeTranchesLot[] = $item->indexTranche->value;
                $atleastOne = true;
            }
        }
        if ($atleastOne) {
            $this->setViewState('tranchesLot_'.$index, $listeTranchesLot);
        }
    }

    public function saveLotTechnique($donneComplementaire)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $lotSaved = [];
        $this->_donneeComplementaire = $donneComplementaire;
        $lotTechnique = (new CommonTLotTechnique())->retrieveLotTechniqueByDonneComplementaire($this->_donneeComplementaire->getIdDonneeComplementaire());

        if (is_array($lotTechnique)) {
            foreach ($lotTechnique as $oneLot) {
                $lotHasTranche = $oneLot->getAllLotTechniqueTranche();
                foreach ($lotHasTranche as $oneLotHasTranche) {
                    CommonTLotTechniqueHasTranchePeer::doDelete($oneLotHasTranche, $connexion);
                }
                CommonTLotTechniquePeer::doDelete($oneLot, $connexion);
            }
        }
        if (true == $this->lotTech->checked) {
            foreach ($this->repeaterDetailLotsTechniques->getItems() as $item) {
                $lotTechnique = new CommonTLotTechnique();
                $lotTechnique->setNumeroLot((new Atexo_Config())->toPfEncoding($item->idLotTechnique->Text));
                $lotTechnique->setIntituleLot((new Atexo_Config())->toPfEncoding($item->intituleLotTechnique->Text));
                if (true == $item->lotPrincipal->checked) {
                    $lotTechnique->setPrincipal(1);
                } else {
                    $lotTechnique->setPrincipal(0);
                }
                $lotTechnique->setIdDonneeComplementaire($this->_donneeComplementaire->getIdDonneeComplementaire());
                $lotTechnique->save($connexion);
                $lotSaved[] = $lotTechnique->getIdLotTechnique();
            }
            $this->setViewState('lotSaved', $lotSaved);
        }
    }

    public function populateLotTechnique($donneComplementaire)
    {
        if (!$this->_donneeComplementaire instanceof CommonTDonneeComplementaire) {
            $this->_donneeComplementaire = $donneComplementaire;
        }
        $lotTechnique = (new CommonTLotTechnique())->retrieveLotTechniqueByDonneComplementaire($this->_donneeComplementaire->getIdDonneeComplementaire());
        $indexLot = 0;
        $dataSource = [];
        $visible = false;
        if ($this->marcheTranche->checked) {
            $visible = true;
        }

        if (is_array($lotTechnique)) {
            $this->lotTech->checked = true;
            foreach ($lotTechnique as $oneLot) {
                $dataSource[$indexLot]['idObjetLotTechnique'] = $oneLot->getIdLotTechnique();
                $dataSource[$indexLot]['idLotTechnique'] = $oneLot->getNumeroLot();
                $dataSource[$indexLot]['intituleLotTechnique'] = $oneLot->getIntituleLot();
                $dataSource[$indexLot]['lotPrincipal'] = $oneLot->getPrincipal();
                $dataSource[$indexLot]['index'] = $indexLot;
                $dataSource[$indexLot]['visibleAppartenance'] = $visible;
                ++$indexLot;
            }

            self::remplirLotTechnique($dataSource);
            $this->scriptJsLot->Text = "<script>isCheckedShowDiv(document.getElementById('".$this->lotTech->getClientId()."'),'".$this->infos_lots_techniques_lot->getClientId()."');</script>";
        } else {
            self::fillRepeaterLotTechnique();
        }
    }

    public function saveLotTechHastranche()
    {
        $lotSaved = $this->getViewState('lotSaved');
        $trancheSaved = $this->getViewState('tranchesSaved');
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        if (is_array($lotSaved)) {
            foreach ($lotSaved as $kLot => $vLot) {
                if (is_array($this->getViewState('tranchesLot_'.$kLot))) {
                    $kLotTranche = '';
                    foreach ($this->getViewState('tranchesLot_'.$kLot) as $kLotTranche => $vLotTranche) {
                        foreach ($trancheSaved as $kTranche => $vTranche) {
                            if ($kTranche == $vLotTranche) {
                                $lotTranche = new CommonTLotTechniqueHasTranche();
                                $lotTranche->setIdLotTechnique($vLot);
                                $lotTranche->setIdTranche($vTranche);
                                $lotTranche->save($connexion);
                            }
                        }
                    }
                }
            }
        }
    }

    public function chargerComposant()
    {
        self::fillListeFormePrix();
        self::fillUnitePrixUnitaire();
        self::fillVariationPrix();
        self::fillTypePrix();
        self::fillRepeaterLotTechnique();
        if ($this->_donneeComplementaire instanceof CommonTDonneeComplementaire) {
            self::populateLotTechnique($this->_donneeComplementaire);
        }
        if (true == $this->getAfficher()) {
            $this->panelFormeMarchePrix->style = 'display:""';
            $this->panelSousFormeMarche->style = 'display:""';
        } else {
            $this->panelFormeMarchePrix->style = 'display:none';
            $this->panelSousFormeMarche->style = 'display:none';
        }

        self::rafraichissementJs();
    }

    /*
     * Permet de valider les données des formes prix
     */
    public function validateDataFormePrix(&$isValid, &$arrayMsgErreur)
    {
        if ($this->marcheTranche->Checked) {
            foreach ($this->listeTranchesMarchePrix->getItems() as $item) {
                if ('0' == $item->idTranche->Text) {
                    $code = Atexo_Config::getParameter('TRANCHE_FIXE');
                } else {
                    $code = Atexo_Config::getParameter('TRANCHE_CONDITIONNELLE').' '.$item->idTranche->Text;
                }
                $trancheMsg = Prado::localize('DEFINE_TEXT_TRANCHE').' - '.$code;
                $dataFormePrix = $this->getViewState('DetailsFormePrix_'.$item->idItemTranche->Value);
                if (is_array($dataFormePrix) && count($dataFormePrix)) {
                    $formePrix = $dataFormePrix['formePrix'];
                    $variationPrixForfaitaire = $dataFormePrix['variationsPrixForfaitaire'];
                    $variationPrixUnitaire = $dataFormePrix['variationsPrixUnitaire'];
                    $typePrix = $dataFormePrix['typesPrix'];
                    (new Atexo_FormePrix())->validerFormePrix($formePrix, $variationPrixForfaitaire, $variationPrixUnitaire, $typePrix, $isValid, $arrayMsgErreur, 0, $trancheMsg);
                } else {
                    $isValid = false;
                    $arrayMsgErreur[] = $trancheMsg.' - '.Prado::localize('TEXT_FORME_DE_PRIX');
                }
            }
        } else {
            $dataFormePrix = $this->getViewState('DetailsFormePrix');
            if (is_array($dataFormePrix) && count($dataFormePrix)) {
                $formePrix = $dataFormePrix['formePrix'];
                $variationPrixForfaitaire = $dataFormePrix['variationsPrixForfaitaire'];
                $variationPrixUnitaire = $dataFormePrix['variationsPrixUnitaire'];
                $typePrix = $dataFormePrix['typesPrix'];
                (new Atexo_FormePrix())->validerFormePrix($formePrix, $variationPrixForfaitaire, $variationPrixUnitaire, $typePrix, $isValid, $arrayMsgErreur);
            } else {
                $isValid = false;
                $arrayMsgErreur[] = Prado::localize('TEXT_FORME_DE_PRIX');
            }
        }
    }

    /**
     * Permet de retourner l'id du composant dans la page père.
     */
    public function getIdComposantFromParent()
    {
        if ('etapeIdentification' == self::getSourcePage()) {
            return '_bloc_etapeDonneesComplementaires';
        }

        return '';
    }

    private function isTypeMarcheAccordCadre(): bool
    {
        $idTypeContrat = $this->Page->consultation->getTypeMarche();
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $typeContrat = (new CommonTTypeContratQuery())->findOneByIdTypeContrat($idTypeContrat, $connexion);

        return $typeContrat && $typeContrat->getAccordCadreSad() === '1';
    }

    private function disableButtonWithoutMinMaxAccordCadre(string $idPartiel): void
    {
        $idCheckBox = sprintf("ctl0_CONTENU_PAGE%s%s", $this->getIdComposantFromParent(), $idPartiel);
        $this->scriptJs->Text = sprintf(
            "<script>disableCheckBoxSelection('%s');</script>",
            $idCheckBox
        );
    }


    public function rafraichissementJs()
    {
        $this->scriptLabel->Text = '<script>';
        if ('Agent.PopupDetailLot' === $_GET['page']) {
            //Bloc Forme du marché public et forme(s) de prix
            $this->scriptLabel->Text .= 'hideLink("'.$this->linkDefinirActionFormePrix->ClientId.'");';
            $this->scriptLabel->Text .= 'disactivateSelect("'.$this->contratFUENon->ClientId.'");';
            $this->scriptLabel->Text .= 'disactivateSelect("'.$this->contratFUEOui->ClientId.'");';
        }
        $this->scriptLabel->Text .= '</script>';
    }

    public function loadFormePrixInfos(CommonTFormePrix $formePrix): void
    {
        if ($formePrix->getFormePrix() == Atexo_Config::getParameter('KEY_PRIX_FORFAITAIRE')) {
            $this->pfVariationLabel->Text = $this->getLabelVariationPrixOnLoad(
                $formePrix->getAllVariationsPrixForfaitaire()
            );

            $this->blocPf->setStyle(self::DISPLAY);
            $this->blocPU->setStyle(self::DISPLAY_NONE);
        } elseif ($formePrix->getFormePrix() == Atexo_Config::getParameter('KEY_PRIX_UNITAIRE')) {
            $this->displayFieldPrixUnitaireOnLoad($formePrix);

            $this->blocPU->setStyle(self::DISPLAY);
            $this->blocPf->setStyle(self::DISPLAY_NONE);
        } elseif ($formePrix->getFormePrix() == Atexo_Config::getParameter('KEY_PRIX_MIXTE')) {
            // Prix forfaire mixte
            $this->pfVariationLabel->Text = $this->getLabelVariationPrixOnLoad(
                $formePrix->getAllVariationsPrixForfaitaire()
            );

            // Prix unitaire mixte
            $this->displayFieldPrixUnitaireOnLoad($formePrix);

            $this->blocPf->setStyle(self::DISPLAY);
            $this->blocPU->setStyle(self::DISPLAY);
        }
    }

    public function displayFormePrixInfos(CommonTFormePrix $formePrix): void
    {
        if ($formePrix->getFormePrix() == Atexo_Config::getParameter('KEY_PRIX_FORFAITAIRE')) {
            $this->pfVariationLabel->Text = $this->getLabelVariationPrix(
                $this->repeaterVariationPrixPF->Items,
                'pf_variation',
                'idPfVariation'
            );
            $this->blocPf->setStyle(self::DISPLAY);
            $this->blocPU->setStyle(self::DISPLAY_NONE);
        } elseif ($formePrix->getFormePrix() == Atexo_Config::getParameter('KEY_PRIX_UNITAIRE')) {
            $this->displayFieldPrixUnitaire(
                'pu_bc',
                'puMin',
                'puMax',
                'puSelectUnite',
                'pu_bdcAvecMinimaxi',
                'pu_qd',
                'repeaterVariationPrixPU',
                'pu_variation',
                'idPUVariation',
                'repeaterTypePrixPU',
                'pu_prixCatalogue',
                'idPUTypePrix',
            );

            $this->blocPU->setStyle(self::DISPLAY);
            $this->blocPf->setStyle(self::DISPLAY_NONE);
        } elseif ($formePrix->getFormePrix() == Atexo_Config::getParameter('KEY_PRIX_MIXTE')) {
            // Prix forfaire mixte
            $this->pfVariationLabel->Text = $this->getLabelVariationPrix(
                $this->repeaterVariationPrixPFPM->Items,
                'pm_pf_variation',
                'idPMPFVariation'
            );

            // Prix unitaire mixte
            $this->displayFieldPrixUnitaire(
                'pm_pu_bc',
                'pmPuMin',
                'pmPuMax',
                'pmPuSelectUnite',
                'pm_pu_bdcAvecMinimaxi',
                'pm_pu_qd',
                'repeaterVariationPrixPUPM',
                'pm_pu_variation',
                'idPMPUVariation',
                'repeaterTypePrixPUPM',
                'pm_pu_prixCatalogue',
                'idPUPMTypePrix',
            );

            $this->blocPf->setStyle(self::DISPLAY);
            $this->blocPU->setStyle(self::DISPLAY);
        }
    }

    public function getLabelVariationPrix(
        ?TRepeaterItemCollection $repeater,
        string $pfVariation,
        string $idVariation
    ): string {
        $codeVariationPrixPf = '';
        foreach ($repeater as $item) {
            if ($item->$pfVariation && $item->$pfVariation->checked) {
                $codeVariationPrixPf = match ($item->$idVariation->value) {
                    Atexo_Config::getParameter('TYPE_PRIX_REVISABLES') => 'PRIX_REVISABLES',
                    Atexo_Config::getParameter('TYPE_PRIX_ACTUALISABLES') => 'PRIX_ACTUALISABLES',
                    Atexo_Config::getParameter('TYPE_PRIX_FERMES') => 'PRIX_FERMES',
                    Atexo_Config::getParameter('TYPE_PRIX_ACTUALISABLES_REVISABLES') => 'PRIX_ACTUALISABLE_REVISABLE',
                    Atexo_Config::getParameter('TYPE_PRIX_FERMES_ACTUALISABLES') => 'PRIX_FERME_ACTUALISABLES',
                    default => '',
                };
            }
        }

        $variationPrixPf = (new Atexo_CommonReferentiel())->retrieveValeurReferentielByLibelle2(
            Atexo_Config::getParameter('REFERENTIEL_VARIATION_PRIX'),
            $codeVariationPrixPf
        );

        $libelleVariationPrixPf = $variationPrixPf ? $variationPrixPf?->getLibelleValeurReferentielTraduit() : null;

        return $libelleVariationPrixPf ?? '';
    }

    public function getListTypePrix(
        ?TRepeaterItemCollection $repeater,
        string $puPrixCatalogue,
        string $idPUTypePrix,
    ): string {
        $listePrix = [];

        foreach ($repeater as $item) {
            if ($item->$puPrixCatalogue && $item->$puPrixCatalogue->checked && $item->$idPUTypePrix) {
                $referentielTypePrix = (new Atexo_CommonReferentiel())->retrieveValeurReferentielById(
                    Atexo_Config::getParameter('REFERENTIEL_TYPE_PRIX'),
                    $item->$idPUTypePrix->value
                );

                if ($referentielTypePrix && $referentielTypePrix?->getLibelleValeurReferentielTraduit()) {
                    $listePrix[] = $referentielTypePrix->getLibelleValeurReferentielTraduit();
                }
            }
        }

        return implode(' - ', $listePrix);
    }

    public function displayFieldPrixUnitaire(
        string $puBc,
        string $puMin,
        string $puMax,
        string $puSelectUnite,
        string $puBdcAvecMinimaxi,
        string $puQd,
        string $repeaterVariationPrixPU,
        string $puVariation,
        string $idPUVariation,
        string $repeaterTypePrixPU,
        string $puPrixCatalogue,
        string $idPUTypePrix,
    ): void {
        if ($this->$puBc->checked) {
            $this->puQuantiteLabel->Text = Prado::localize('DEFINE_BONS_COMMANDE');

            $this->puMinValue->Text = $this->$puMin?->Text;
            $this->puMaxValue->Text = $this->$puMax?->Text;

            $this->puUniteMinLabel->Text = $this->$puSelectUnite?->SelectedItem->getText();
            $this->puUniteMaxLabel->Text = $this->$puSelectUnite?->SelectedItem->getText();

            if ($this->$puBdcAvecMinimaxi->checked) {
                $this->puMinMaxSelectionLabel->Text = Prado::localize('DEFINE_AVEC_MIN_MAX');
                $this->blocPUAvecMinMax->setStyle(self::DISPLAY);
            } else {
                $this->puMinMaxSelectionLabel->Text = Prado::localize('DEFINE_SANS_MIN_MAX');
                $this->blocPUAvecMinMax->setStyle(self::DISPLAY_NONE);
            }

            $this->blocPUMinMax->setStyle(self::DISPLAY);
        } elseif ($this->$puQd->checked) {
            $this->puQuantiteLabel->Text = Prado::localize('FCSP_A_QUANTITE_DEFINIE');
            $this->puMinMaxSelectionLabel->Text = '';

            $this->blocPUMinMax->setStyle(self::DISPLAY_NONE);
            $this->blocPUAvecMinMax->setStyle(self::DISPLAY_NONE);
        }

        $this->puVariationLabel->Text = $this->getLabelVariationPrix(
            $this->$repeaterVariationPrixPU?->Items,
            $puVariation,
            $idPUVariation
        );

        $this->puTypePrixLabel->Text = $this->getListTypePrix(
            $this->$repeaterTypePrixPU->Items,
            $puPrixCatalogue,
            $idPUTypePrix
        );
    }
    public function displayFieldPrixUnitaireOnLoad(CommonTFormePrix $formePrix): void
    {
        if ($formePrix->getModalite() == Atexo_Config::getParameter('MODALITE_BON_COMMANDE')) {
            $this->puQuantiteLabel->Text = Prado::localize('DEFINE_BONS_COMMANDE');
            if ($formePrix->getIdMinMax()) {
                $unitePrix = (new Atexo_CommonReferentiel())->retrieveValeurReferentielById(
                    Atexo_Config::getParameter('REFERENTIEL_UNITE_PRIX'),
                    $formePrix->getIdMinMax()
                );
                $labelUnitePrix = $unitePrix?->getLibelleValeurReferentielTraduit();

                $this->puMinMaxSelectionLabel->Text = Prado::localize('DEFINE_AVEC_MIN_MAX');
                $this->puMinValue->Text = $formePrix->getPuMin();
                $this->puMaxValue->Text = $formePrix->getPuMax();
                $this->puUniteMinLabel->Text = $labelUnitePrix ;
                $this->puUniteMaxLabel->Text = $labelUnitePrix;

                $this->blocPUAvecMinMax->setStyle(self::DISPLAY);
            } else {
                $this->puMinMaxSelectionLabel->Text = Prado::localize('DEFINE_SANS_MIN_MAX');
                $this->blocPUAvecMinMax->setStyle(self::DISPLAY_NONE);
            }
        } elseif ($formePrix->getModalite() == Atexo_Config::getParameter('MODALITE_QUANTITE_DEFINIER')) {
            $this->puQuantiteLabel->Text = Prado::localize('FCSP_A_QUANTITE_DEFINIE');
            $this->puMinMaxSelectionLabel->Text = '';

            $this->blocPUMinMax->setStyle(self::DISPLAY_NONE);
            $this->blocPUAvecMinMax->setStyle(self::DISPLAY_NONE);
        }

        // Variations prix unitaire
        $this->puVariationLabel->Text = $this->getLabelVariationPrixOnLoad(
            $formePrix->getAllVariationsPrixUnitaire()
        );

        // Types prix unitaire
        $this->puTypePrixLabel->Text = $this->getListTypePrixOnLoad(
            $formePrix->getAllTypesPrix()
        );
    }

    public function getLabelVariationPrixOnLoad(?array $listVariations): string
    {
        $codeVariationPrixPf = '';
        foreach ($listVariations as $item) {
            $codeVariationPrixPf = match ((string)$item->getIdVariation()) {
                Atexo_Config::getParameter('TYPE_PRIX_REVISABLES') => 'PRIX_REVISABLES',
                Atexo_Config::getParameter('TYPE_PRIX_ACTUALISABLES') => 'PRIX_ACTUALISABLES',
                Atexo_Config::getParameter('TYPE_PRIX_FERMES') => 'PRIX_FERMES',
                Atexo_Config::getParameter('TYPE_PRIX_ACTUALISABLES_REVISABLES') => 'PRIX_ACTUALISABLE_REVISABLE',
                Atexo_Config::getParameter('TYPE_PRIX_FERMES_ACTUALISABLES') => 'PRIX_FERME_ACTUALISABLES',
                default => '',
            };
        }

        $variationPrixPf = (new Atexo_CommonReferentiel())->retrieveValeurReferentielByLibelle2(
            Atexo_Config::getParameter('REFERENTIEL_VARIATION_PRIX'),
            $codeVariationPrixPf
        );

        $libelleVariationPrixPf = $variationPrixPf ? $variationPrixPf?->getLibelleValeurReferentielTraduit() : null;

        return $libelleVariationPrixPf ?? '';
    }

    public function getListTypePrixOnLoad(?array $listTypePrix): string
    {
        $listePrix = [];

        foreach ($listTypePrix as $item) {
            $referentielTypePrix = (new Atexo_CommonReferentiel())->retrieveValeurReferentielById(
                Atexo_Config::getParameter('REFERENTIEL_TYPE_PRIX'),
                $item->getIdTypePrix()
            );

            if ($referentielTypePrix && $referentielTypePrix?->getLibelleValeurReferentielTraduit()) {
                $listePrix[] = $referentielTypePrix->getLibelleValeurReferentielTraduit();
            }
        }

        return implode(' - ', $listePrix);
    }

    public function displayFormePrixRow(CommonTFormePrix|bool|null $formePrix = null): void
    {
        if ($formePrix instanceof CommonTFormePrix) {
            $this->scriptJs->Text .= "<script>
                        document.getElementById('choiceFormePrix').style.display = '';
                        document.getElementById('valuesFormePrix').style.display = '';
                        </script>"
            ;
        } else {
            $this->scriptJs->Text .= "<script>
                        document.getElementById('choiceFormePrix').style.display = 'none';
                        document.getElementById('valuesFormePrix').style.display = 'none';
                        </script>"
            ;
        }
    }
}
