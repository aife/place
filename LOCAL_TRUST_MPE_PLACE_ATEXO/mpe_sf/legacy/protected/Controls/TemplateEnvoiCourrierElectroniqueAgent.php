<?php

namespace Application\Controls;

use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonEchange;
use Application\Propel\Mpe\CommonEchangeDestinataire;
use Application\Propel\Mpe\CommonEchangePeer;
use Application\Propel\Mpe\CommonEchangePieceJointe;
use Application\Propel\Mpe\CommonEchangePieceJointePeer;
use Application\Propel\Mpe\CommonEchangeTypeMessage;
use Application\Propel\Mpe\CommonHabilitationAgent;
use Application\Propel\Mpe\CommonHabilitationAgentPeer;
use Application\Propel\Mpe\CommonOrganisme;
use Application\Propel\Mpe\CommonPlateformeVirtuelle;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_MultiDomaine;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Guests;
use Application\Service\Atexo\DossierVolumineux\Atexo_DossierVolumineux_Responses;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Inscrit;
use Application\Service\Atexo\Message\Atexo_Message_RelationEchange;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * Classe EnvoiCourrierElectronique.
 *
 * @author SBAI Yassine <yassine.sbai@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class TemplateEnvoiCourrierElectroniqueAgent extends MpeTPage
{
    public $_calledFrom = false;
    public $_refConsultation;
    public $idMsgModifCons = false;
    public $_org;
    public $_postBack = false;
    public $_forInvitationConcourir;
    public $_tailleMaxPj;
    public $_redirect = false;
    //Pour l'envoie d'un message de confirmation a l'entreprise apres la reponse a la demande de complement
    public $_confirmationReponseEntreprise = false;
    public $_mailOffre;
    public $_mailQuestion;
    public $_figerOptionEnvoie = false;

    public function setFigerOptionEnvoie($value)
    {
        $this->_figerOptionEnvoie = $value;
    }

    public function getFigerOptionEnvoie()
    {
        return $this->_figerOptionEnvoie;
    }

    public function setCalledFrom($value)
    {
        $this->_calledFrom = $value;
    }

    public function getCalledFrom()
    {
        return $this->_calledFrom;
    }

    public function setPostBack($value)
    {
        $this->_postBack = $value;
    }

    public function getPostBack()
    {
        return $this->_postBack;
    }

    public function setTailleMaxPj($value)
    {
        $this->_tailleMaxPj = $value;
    }

    public function getTailleMaxPj()
    {
        return $this->_tailleMaxPj;
    }

    public function setConfirmationReponseEntreprise($value)
    {
        $this->_confirmationReponseEntreprise = $value;
    }

    public function getConfirmationReponseEntreprise()
    {
        return $this->_confirmationReponseEntreprise;
    }

    public function setMailOffre($value)
    {
        $this->_mailOffre = $value;
    }

    public function getMailOffre()
    {
        return $this->_mailOffre;
    }

    public function setRedirect($value)
    {
        $this->_redirect = $value;
    }

    public function setMailQuestion($value)
    {
        $this->_mailQuestion = $value;
    }

    public function getMailQuestion()
    {
        return $this->_mailQuestion;
    }

    public function getRedirect()
    {
        return $this->_redirect;
    }

    public function onLoad($param)
    {
        $agentCon = null;
        $serviceCon = null;
        $emailExpediteur = null;
        $this->_refConsultation = Atexo_Util::atexoHtmlEntities($_GET['id']);
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));
        if (0 === strcmp($this->getCalledFrom(), 'agent')) {//agent
            $this->_org = Atexo_CurrentUser::getOrganismAcronym();
            $serviceCon = Atexo_CurrentUser::getCurrentServiceId();
            $agentCon = Atexo_CurrentUser::getId();
        } else { //entreprise
            $this->_org = Atexo_Util::atexoHtmlEntities($_GET['acronymeOrg']);
            $echangeDest = Atexo_Message::getechangeDestinataireByUid(Atexo_Util::atexoHtmlEntities($_GET['num_ar']), $this->_org);
            if ($echangeDest->getIdEchange()) {
                $objetMessage = Atexo_Message::retrieveMessageById($echangeDest->getIdEchange(), $this->_org);
                $agentCon = $objetMessage->getIdCreateur();
                $serviceCon = $objetMessage->getServiceId();
                $this->corpsAR->Text = $this->getCorpsAR($echangeDest, $objetMessage);
            }
        }
        //ajouter un warning pour les consultations archivees
        $ConsultationO = (new Atexo_Consultation())->retrieveConsultation($this->_refConsultation, $this->_org);
        if ($ConsultationO->getIdEtatConsultation() == Atexo_Config::getParameter('ETAT_EN_ATTENTE_ARCHIVAGE')) {
            $msgAvertissement = '<span>' . Prado::localize('MSG_AVERTISSEMENT_MODIFS_NON_INTEGRES_CAR_CONS_ARCHIVEE') . '</span><br/>';
            $this->panelMessageWarning->setVisible(true);
            $this->panelMessageWarning->setMessage($msgAvertissement);
        }
        if (!$this->getPostBack()) {
            $this->remplirListeMessage();
            $this->remplirListeDossierVolumineux();
            $organismeObj = Atexo_Organismes::retrieveOrganismeByAcronyme($this->_org);
            if ($organismeObj instanceof CommonOrganisme) {
                $expediteur = $organismeObj->getSigle() . ' - ' . $organismeObj->getDenominationOrg();
            } else {
                $expediteur = '-';
            }
            $message = new CommonEchange();
            $message->setIdCreateur('idCreate');
            if (0 == strcmp($this->getCalledFrom(), 'agent')) {//agent
                if (isset($_GET['IdEchange'])) {
                    $message = Atexo_Message::retrieveMessageById(Atexo_Util::atexoHtmlEntities($_GET['IdEchange']), $this->_org);
                    $this->destinataires->Text = $message->getDestinataires();
                    $this->objet->Text = $message->getObjet();
                    $this->textMail->text = $message->getCorps();
                    $this->messageType->setSelectedValue($message->getIdTypeMessage());
                    if ($message->getOptionEnvoi() == Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_SIMPLE')) {
                        $this->courrierElectroniqueSimple->Checked = 1;
                    }
                    if ($message->getOptionEnvoi() == Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_AVEC_AR')) {
                        $this->courrierElectroniqueContenuIntegralAR->Checked = 1;
                    }
                    if ($message->getOptionEnvoi() == Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_AVEC_LIEN')) {
                        $this->courrierElectroniqueUniquementLienTelechargementObligatoire->Checked = 1;
                    }
                    if ($message->getOptionEnvoi() == Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_REPONSE_AUTORISEE')) {
                        $this->courrierElectroniqueDeDemandeDeComplement->Checked = 1;
                    }
                    $this->setViewState('idMessage', Atexo_Util::atexoHtmlEntities($_GET['IdEchange']));
                } elseif (isset($_GET['IdInscrit'])) {
                    $objectInscrit = (new Atexo_Entreprise_Inscrit())->retrieveInscritById(Atexo_Util::atexoHtmlEntities($_GET['IdInscrit']));
                    if ($objectInscrit) {
                        $this->destinataires->Text = trim($objectInscrit->getEmail());
                    }
                } elseif (isset($_GET['IdMsg'])) {
                    $this->setViewState('idMessage', Atexo_Util::atexoHtmlEntities($_GET['IdMsg']));
                    $message = Atexo_Message::retrieveMessageById(Atexo_Util::atexoHtmlEntities($_GET['IdMsg']), $this->_org);
                }
                if ($message->getStatus() == Atexo_Config::getParameter('STATUS_ECHANGE_VALIDE')) {
                    $this->response->redirect('?page=Agent.DetailConsultation&id=' . Atexo_Util::atexoHtmlEntities($_GET['id']));
                }
                if ($this->idMsgModifCons && !$_GET['IdEchange']) {
                    $this->remplirObjetEtCorpsMessage($this->_org, isset($_GET['invitation']));
                }
                $message = $this->setInfoMessage($message, $agentCon, $expediteur, Atexo_Config::getParameter('PF_MAIL_FROM'), $serviceCon, $this->_org);

                if ($message->save($connexionCom)) {
                    $this->setViewState('idMessage', $message->getId());
                }
            } else {
                $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));
                $message = new CommonEchange();
                $inscrit = (new Atexo_Entreprise_Inscrit())->retrieveInscritById(Atexo_CurrentUser::getIdInscrit());
                if ($inscrit) {
                    $emailExpediteur = trim($inscrit->getEmail());
                    $expediteur = $inscrit->getEntreprise($connexionCom)->getNom();
                }
                $message = $this->setInfoMessage($message, null, $expediteur, $emailExpediteur, null, $this->_org);
                if ($message->save($connexionCom)) {
                    $this->setViewState('idMessage', $message->getId());
                }
            }
            $message->setPageSource(Atexo_Util::atexoHtmlEntities($_GET['page']));
            $message->save($connexionCom);
        }

        $PJ = [];
        $c = new Criteria();
        $objetMessage = Atexo_Message::retrieveMessageById($this->getViewState('idMessage'), $this->_org);
        if ($objetMessage) {
            $c->add(CommonEchangePieceJointePeer::ORGANISME, $this->_org);
            $PJ = $objetMessage->getCommonEchangePieceJointes($c, $connexionCom);
        }
        $this->remplirTableauPJ($PJ);

        // Mettre automatiquement et figé "avec réponse attendue"

        if ($this->getFigerOptionEnvoie()) {
            $this->panelOptionEnvoi->Enabled = false;
        }
    }

    public function getCorpsAR($echangeDest, $objetMessage)
    {
        $string = '';
        if ($objetMessage) {
            $string = '-----' . Prado::localize('DEFINE_TEXT_MSG_ORIGINE') . '-----';
            $string .= '
';
            $string .= Prado::localize('TEXT_DE') . trim($objetMessage->getEmailExpediteur());
            $string .= '
';
            $string .= Prado::localize('DEFINE_TEXT_ENVOYE') . Atexo_Util::iso2frnDateTime($objetMessage->getDateMessage());
            $string .= '
';
            $string .= Prado::localize('TEXT_A') . trim($echangeDest->getMailDestinataire());
            $string .= '
';
            $string .= Prado::localize('DEFINE_TEXT_OBJET') . $objetMessage->getObjet();
            $string .= '
';
            $string .= Prado::localize('TEXT_CORPS') . $objetMessage->getCorps();
        }

        return $string;
    }

    public function refreshText($sender, $param)
    {
        $pfUrl = Atexo_MultiDomaine::replaceDomain(
            Atexo_Config::getParameter('PF_URL_MESSAGERIE'),
            Atexo_Config::getParameter('PF_ACTIVE_MULTI_DOMAINE')
        );
        // Rafraichissement du ActiveTextBox
        $string = '';
        $IdTypeMessage = $sender->getSelectedValue();
        $objetMessage = Atexo_Message::retrieveTypeMessageById($IdTypeMessage);

        if (0 != $IdTypeMessage) {
            $this->objet->Text = $objetMessage->getLibelle();
            if (isset($_GET['id'])) {
                $string = (new Atexo_Message())->getInfoConsultation(Atexo_Util::atexoHtmlEntities($_GET['id']), $this->_org);
            }
            $consultation = (new Atexo_Consultation())->retrieveConsultation(Atexo_Util::atexoHtmlEntities($_GET['id']), $this->_org);
            $string .= '
';
            if ($consultation->getTypeAcces() == Atexo_Config::getParameter('TYPE_PROCEDURE_PUBLICITE')) {
                $string .= Prado::localize('DEFINE_ACCES_DIRECT') . ' : ' . $pfUrl . '?page=Entreprise.EntrepriseDetailConsultation&id=' . Atexo_Util::atexoHtmlEntities($_GET['id']) . '&orgAcronyme=' . $this->_org;
                $string .= '
';
            } elseif ($consultation->getTypeAcces() == Atexo_Config::getParameter('TYPE_PROCEDURE_RESTREINTE')) {
                $string .= Prado::localize('DEFINE_ACCES_DIRECT') . ' : ' . $pfUrl . '?page=Entreprise.EntrepriseDetailConsultation&id=' . Atexo_Util::atexoHtmlEntities($_GET['id']) . '&orgAcronyme=' . $this->_org . '&code=' . $consultation->getCodeProcedure();
                $string .= '
';
            }
            if ($objetMessage instanceof CommonEchangeTypeMessage) {
                $string .= '
';
                $string .= $objetMessage->getCorps();
            }

            $placeMarchePublicInterministere = Prado::localize('PLACE_MARCHE_PUBLIC_INTERMINISTERE');

            if ($consultation && $consultation->getCommonPlateformeVirtuelle() instanceof CommonPlateformeVirtuelle) {
                $plateformeVirtuelle = $consultation->getCommonPlateformeVirtuelle();
                $placeMarchePublicInterministere = $plateformeVirtuelle->getFooterMail();
            }

            $string .= (new Atexo_Message())->getBasPage($placeMarchePublicInterministere);
            $this->textMail->Text = $string;
        } else {
            $this->objet->Text = '';
            $this->textMail->Text = '';
        }
        if (
            $IdTypeMessage == Atexo_Config::getParameter('ID_MESSAGE_COURRIER_LIBRE')
            || $IdTypeMessage == Atexo_Config::getParameter('ID_MESSAGE_COURRIER_DEMANDE_COMPLEMENT')
            || $IdTypeMessage == Atexo_Config::getParameter('ID_MESSAGE_MODIFICATION_CONSULTATION')
        ) {
            $this->courrierElectroniqueDeDemandeDeComplement->checked = true;
        } else {
            $this->courrierElectroniqueUniquementLienTelechargementObligatoire->checked = true;
        }

        $this->displayDossierVolumineux();
    }

    public function onDeleteClick($sender, $param)
    {
        $idpj = $param->CommandParameter;
        Atexo_Message::DeletePj($idpj, $this->_org);
    }

    public function refreshRepeaterPJ($sender, $param)
    {
        //Raffraichissement du tableau PJ:
        $objetMessage = Atexo_Message::retrieveMessageById($this->getViewState('idMessage'), $this->_org);
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $PJ = [];
        if ($objetMessage) {
            $c->add(CommonEchangePieceJointePeer::ORGANISME, $this->_org);
            $PJ = $objetMessage->getCommonEchangePieceJointes($c, $connexionCom);
        }
        $this->remplirTableauPJ($PJ);
        $this->PanelPJ->render($param->NewWriter);
    }

    public function remplirTableauPJ($data)
    {
        $this->RepeaterPiecesJointes->dataSource = $data;
        $this->RepeaterPiecesJointes->dataBind();
    }

    public function remplirListeMessage()
    {
        $dataMessageType = [];
        $resultCriteria = Atexo_Message::getAllTypeMessages();
        $dataMessageType[0] = Prado::localize('TEXT_SELECTIONNER') . '...';
        if (is_array($resultCriteria)) {
            foreach ($resultCriteria as $result) {
                $dataMessageType[$result->getId()] = $result->getLibelle();
            }
        }
        $this->messageType->dataSource = $dataMessageType;
        if ($this->idMsgModifCons && !$_GET['IdEchange']) {
            $this->messageType->SelectedValue = $this->idMsgModifCons;
        }
        $this->messageType->dataBind();
    }

    public function remplirListeDossierVolumineux()
    {
        $this->selectDossierVolumineux->dataSource = Atexo_DossierVolumineux_Responses::getListeDossiersVolumineuxActifs(Atexo_CurrentUser::getId(), Atexo_CurrentUser::isAgent());
        $this->selectDossierVolumineux->dataBind();
    }

    public function selectRadioButton($sender, $param)
    {
        if ($this->courrierElectroniqueUniquementLienTelechargementObligatoire->Checked || $this->courrierElectroniqueDeDemandeDeComplement->Checked) {
            $this->displayDossierVolumineux();
        } else {
            $this->hideDossierVolumineux();
        }
    }

    public function displayDossierVolumineux()
    {
        if (Atexo_Module::isEnabled('dossierVolumineux')) {
            $this->panelSelectDossierVolumineux->setStyle('display:block');
        }
    }

    public function hideDossierVolumineux()
    {
        if (Atexo_Module::isEnabled('dossierVolumineux')) {
            $this->panelSelectDossierVolumineux->setStyle('display:none');
        }
    }

    public function setInfoMessage($message, $agentCon, $expediteur, $emailExpediteur, $idService, $org)
    {
        $message->setIdCreateur($agentCon);
        $message->setStatus(Atexo_Config::getParameter('STATUS_ECHANGE_BROUILLON'));
        if (isset($_GET['id'])) {
            $message->setConsultationId(Atexo_Util::atexoHtmlEntities($_GET['id']));
        }
        $message->setExpediteur($expediteur);
        $message->setEmailExpediteur(trim($emailExpediteur));
        $message->setFormat(Atexo_Config::getParameter('ECHANGE_PLATE_FORME'));
        $message->setServiceId($idService);
        $message->setOrganisme($org);

        return $message;
    }

    public function remplirObjetEtCorpsMessage($org, $invitation = null)
    {
        $res = (new Atexo_Message())->remplirObjetEtCorpsMessage($org, Atexo_Util::atexoHtmlEntities($_GET['id']), $this->idMsgModifCons, $invitation);
        $this->objet->Text = $res['objet'];
        $this->textMail->Text = $res['corps'];
    }

    public function envoyer()
    {
        $agents = null;
        $refUtilisateur = null;
        $logger = null;
        if (!$this->validerTaillePj()) {
            $this->messageErreur->setVisible(true);
            $this->messageErreur->setMessage(Prado::localize('ERREUR_TAILLE_FICHIER_PJ'));
        } else {
            $corps = '';
            $maildestinataires = [];
            $arraymaildestinataire = [];
            $objetMessage = Atexo_Message::retrieveMessageById($this->getViewState('idMessage'), $this->_org);
            if ($objetMessage instanceof CommonEchange) {
                if ($objetMessage->getStatus() == Atexo_Config::getParameter('STATUS_ECHANGE_VALIDE')) {
                    $this->messageErreur->setVisible(true);
                    $this->messageErreur->setMessage(Prado::localize('ECHEC_ENVOI_COURRIER_DEJA_ENVOYE'));
                    Prado::log('Erreur mail déjà envoyé id echange :' . $objetMessage->getId(), TLogger::ERROR, 'Error');

                    return false;
                }
            }

            $objetMessage = new CommonEchange();
            $objetMessage->setId($this->getViewState('idMessage'));
            if ($this->courrierElectroniqueSimple->Checked) {
                $objetMessage->setOptionEnvoi(Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_SIMPLE'));
            }
            if ($this->courrierElectroniqueContenuIntegralAR->Checked) {
                $objetMessage->setOptionEnvoi(Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_AVEC_AR'));
            }
            if ($this->courrierElectroniqueUniquementLienTelechargementObligatoire->Checked) {
                $objetMessage->setOptionEnvoi(Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_AVEC_LIEN'));
            }
            if ($this->courrierElectroniqueDeDemandeDeComplement->Checked) {
                $objetMessage->setOptionEnvoi(Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_REPONSE_AUTORISEE'));
            }

            if (
                Atexo_Module::isEnabled('ModuleEnvol')
                &&
                (
                    $this->courrierElectroniqueUniquementLienTelechargementObligatoire->Checked
                    ||
                    $this->courrierElectroniqueDeDemandeDeComplement->Checked
                    || ('entreprise' == $this->getCalledFrom())
                )
            ) {
                if (0 != $this->selectDossierVolumineux->SelectedValue) {
                    $objetMessage->setIdDossierVolumineux($this->selectDossierVolumineux->getSelectedValue());
                }
            }

            $maildestinataires = explode(',', $this->destinataires->Text);
            if (is_array($maildestinataires) && 0 != count($maildestinataires)) {
                foreach ($maildestinataires as $destinataire) {
                    if (!in_array($destinataire, $arraymaildestinataire)) {
                        $echangedestinataire = new CommonEchangeDestinataire();
                        $arraymaildestinataire[] = $destinataire;
                        if (0 == strcmp($this->getCalledFrom(), 'agent')) {
                            if ($objetMessage->getOptionEnvoi() == Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_SIMPLE')) {
                                $echangedestinataire->setTypeAr(Atexo_Config::getParameter('ID_TYPE_AR_SANS_OBJET'));
                            } else {
                                $echangedestinataire->setTypeAr(Atexo_Config::getParameter('ID_TYPE_AR_NON_RETIRE'));
                            }
                        } else {
                            $echangedestinataire->setTypeAr(0);
                        }
                        $echangedestinataire->setUid((new Atexo_Message())->getUniqueId());
                        $echangedestinataire->setMailDestinataire(trim($destinataire));
                        $objetMessage->addCommonEchangeDestinataire($echangedestinataire);
                    }
                }
            }
            $corps = $this->textMail->text;
            if ($this->corpsAR->text) {
                $corps .= '
	';
                $corps .= $this->corpsAR->text;
            }
            $objetConsultation = (new Atexo_Consultation())->retrieveConsultation($this->_refConsultation, $this->_org);
            if ($objetConsultation) {
                $refUtilisateur = $objetConsultation->getReferenceUtilisateur();
            }
            $objetMessage->setCorps($corps);
            $objetMessage->setObjet($this->objet->Text);
            $objetMessage->setIdTypeMessage($this->messageType->getSelectedValue());
            $idEchange = Atexo_Message::EnvoyerEmail($objetMessage, $this->_org, $this->getCalledFrom());
            if ($idEchange) {
                if ($this->messageType->getSelectedValue() == Atexo_Config::getParameter('ID_MESSAGE_COURRIER_REPONSE_A_QUESTION') && isset($_GET['idQuestion'])) {
                    (new Atexo_Message_RelationEchange())->save($idEchange, Atexo_Util::atexoHtmlEntities($_GET['idQuestion']), Atexo_Config::getParameter('TYPE_RELATION_REPONSE_QUESTION'));
                }

                if ($this->messageType->getSelectedValue() == Atexo_Config::getParameter('TYPE_MESSAGE_NOTIFICATION') && isset($_GET['Decision'])) {
                    (new Atexo_Message_RelationEchange())->save($idEchange, Atexo_Util::atexoHtmlEntities($_GET['Decision']), Atexo_Config::getParameter('TYPE_RELATION_NOTIFICATION'));
                }
                if ($this->messageType->getSelectedValue() == Atexo_Config::getParameter('TYPE_MESSAGE_NOTIFICATION') && isset($_GET['idContrat'])) {
                    (new Atexo_Message_RelationEchange())->save($idEchange, Atexo_Util::atexoHtmlEntities($_GET['Decision']), Atexo_Config::getParameter('TYPE_RELATION_NOTIFICATION_CONTRAT'));
                }
                if (0 == strcmp($this->getCalledFrom(), 'agent')) {//agent
                    $this->response->redirect('?page=Agent.DetailConsultation&id=' . Atexo_Util::atexoHtmlEntities($_GET['id']));
                } else {//entreprise
                    $consultation = (new Atexo_Consultation())->retrieveConsultation($this->_refConsultation, $this->_org);
                    if ($consultation) {
                        $guests = (new Atexo_Consultation_Guests())->getAllInvitedAgents(Atexo_Util::atexoHtmlEntities($_GET['id']), $this->_org);
                        $listeIdsAgents = (new Atexo_Consultation_Guests())->getIdsAgentFromArrayGuestsVo($guests);
                        $agents = (new Atexo_Agent())->retrieveAgentsAvecAlerteReceptionMessage($listeIdsAgents);
                    }
                    if (is_array($agents)) {
                        $msg = Atexo_Message::getMessageSoliciteDesEntreprise($this->_refConsultation, $this->_org);
                        foreach ($agents as $Guest) {
                            if (1 == $Guest->getAlerteReceptionMessage()) {
                                if ($Guest->getEmail()) {
                                    $objet = Prado::localize('DEFINE_TEXT_OBJET_REPONSE_ENTREPRISE') . ' - ' . Prado::localize('TEXT_REF') . ' : ' . $refUtilisateur;
                                    $msgFormatHtml = (new Atexo_Message())->generateMailFormatHtml($objet, $msg);

                                    $from = Atexo_Config::getParameter('PF_MAIL_FROM');
                                    $pfName = null;
                                    if ($consultation && $consultation->getCommonPlateformeVirtuelle() instanceof CommonPlateformeVirtuelle) {
                                        $plateformeVirtuelle = $consultation->getCommonPlateformeVirtuelle();
                                        $from = $plateformeVirtuelle->getNoReply();
                                        $pfName = $plateformeVirtuelle->getFromPfName();
                                    }

                                    Atexo_Message::simpleMail(
                                        $from,
                                        trim($Guest->getEmail()),
                                        $objet,
                                        $msgFormatHtml,
                                        '',
                                        '',
                                        false,
                                        true,
                                        true,
                                        $pfName
                                    );
                                }
                            }
                        }
                    }

                    //Envoie d'un message de confirmation a l'entreprise apres la reponse a l'invitation a concourir
                    if ($this->_confirmationReponseEntreprise) {
                        $arrayMail = [];
                        $arrayMail['objetMessage'] = $objetMessage->getObjet();
                        $arrayMail['CorpsMessage'] = $this->textMail->text;
                        $arrayMail['CorpsMessageAR'] = $this->corpsAR->text;
                        $echangeDestinataire = Atexo_Message::getechangeDestinataireByUid(Atexo_Util::atexoHtmlEntities($_GET['num_ar']), $this->_org);
                        $objet = Prado::localize('DEFINE_ACCUSE_RECEPTION_REPONSE_DEMANDE_COMPLEMENT') . ' - ' . Prado::localize('TEXT_REF') . ' : ' . $refUtilisateur;
                        $corpsMailConfirmationEtp = (new Atexo_Message())->getCorpsMessageConfirmationReponseEntreprise($arrayMail, $this->getViewState('idMessage'), $this->_refConsultation, $this->_org);
                        $corpsMailConfirmationEtp = (new Atexo_Message())->generateMailFormatHtml($objet, $corpsMailConfirmationEtp, true);

                        /*
                         * @todo a faire
                         */
                        Atexo_Message::simpleMail(
                            Atexo_Config::getParameter('PF_MAIL_FROM'),
                            trim($echangeDestinataire->getMailDestinataire()),
                            $objet,
                            $corpsMailConfirmationEtp,
                            '',
                            '',
                            false,
                            true
                        );
                    }
                    $this->response->redirect('index.php?page=Entreprise.ReponseEntrepriseEnvoye');
                }
            } else {
                $this->messageErreur->setVisible(true);
                $this->messageErreur->setMessage(Prado::localize('ECHEC_ENVOI_COURRIER'));
                $logger->error("Echec d'envoie");
            }
        }
    }

    public function RedirectReturn()
    {
        if (self::getRedirect()) {
            $this->response->redirect(self::getRedirect());
        } else {
            $this->response->redirect('?page=Agent.DetailConsultation&id=' . Atexo_Util::atexoHtmlEntities($_GET['id']));
        }
    }

    public function setForInvitationConcourir($valeur)
    {
        $this->_forInvitationConcourir = $valeur;
    }

    public function getForInvitationConcourir()
    {
        return $this->_forInvitationConcourir;
    }

    public function validerTaillePj()
    {
        if ($this->_tailleMaxPj) {
            $tailleMaxPj = $this->_tailleMaxPj;
        } else {
            $tailleMaxPj = Atexo_Config::getParameter('MAX_FILE_SIZE_IN_MAIL');
        }
        $limitationTailleFichier = false;
        if ($this->courrierElectroniqueSimple->Checked) {
            $limitationTailleFichier = true;
        }
        if ($this->courrierElectroniqueContenuIntegralAR->Checked) {
            $limitationTailleFichier = true;
        }
        if ($limitationTailleFichier) {
            $pjs = Atexo_Message::getPjByIdEchange($this->getViewState('idMessage'), $this->_org);
            if ($pjs && (is_countable($pjs) ? count($pjs) : 0)) {
                $taille = 0;
                foreach ($pjs as $pj) {
                    if ($pj->getTaille() > $tailleMaxPj) {
                        return false;
                    }
                    $taille += $pj->getTaille();
                    if ($taille > $tailleMaxPj) {
                        return false;
                    }
                }

                return true;
            }
        }

        return true;
    }

    public function editerDestinatairesMail($sender, $param)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));
        $echange = CommonEchangePeer::retrieveByPK($this->getViewState('idMessage'), $connexionCom);
        $echange->setObjet($this->objet->Text);
        $echange->setCorps($this->textMail->text);
        $echange->setIdTypeMessage($this->messageType->getSelectedValue());
        $echange->setDestinataires($this->destinataires->Text);

        if ($this->courrierElectroniqueSimple->Checked) {
            $echange->setOptionEnvoi(Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_SIMPLE'));
        }
        if ($this->courrierElectroniqueContenuIntegralAR->Checked) {
            $echange->setOptionEnvoi(Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_AVEC_AR'));
        }
        if ($this->courrierElectroniqueUniquementLienTelechargementObligatoire->Checked) {
            $echange->setOptionEnvoi(Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_AVEC_LIEN'));
        }
        if ($this->courrierElectroniqueDeDemandeDeComplement->Checked) {
            $echange->setOptionEnvoi(Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_REPONSE_AUTORISEE'));
        }
        if (('1' == $this->getForInvitationConcourir() || '1' == $this->getMailOffre() || '1' == $this->getMailQuestion()) && !$_GET['IdEchange']) {
            $echange->setDestinatairesLibres($this->destinataires->Text);
        }
        //Sauvegarde de l'url de la page
        $urlPage = strstr($_SERVER['REQUEST_URI'], '?');
        $echange->setPageSource($urlPage);
        $echange->save($connexionCom);
        $this->response->redirect('index.php?page=Agent.EnvoiCourrierElectroniqueChoixDestinataire&id=' . $this->_refConsultation . '&forInvitationConcourir=' . $this->getForInvitationConcourir() . '&IdEchange=' . $this->getViewState('idMessage'));
    }

    /**
     * Permet de formatter le nom du fichier.
     *
     * @param CommonEchangePieceJointe $file : objet fichier
     *
     * @return string : nom du fichier
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function formaterNomFichierPj($file)
    {
        if ($file instanceof CommonEchangePieceJointe) {
            return $file->getNomFichierFormate();
        }
    }

    /**
     * @throws PropelException
     */
    public function isDossierVolumineuxEnabled(): bool
    {
        $idAgent = Atexo_CurrentUser::getId();

        $connexion = Propel::getConnection(
            Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE')
        );
        /** @var CommonHabilitationAgent $habilitationAgent */
        $habilitationAgent = CommonHabilitationAgentPeer::retrieveByPK($idAgent, $connexion);

        $consultation = (new Atexo_Consultation())->retrieveConsultation($this->_refConsultation, $this->_org);

        return (
            Atexo_Module::isEnabled('ModuleEnvol')
            && $habilitationAgent
            && $habilitationAgent->getGestionEnvol()
            && $consultation
            && $consultation->isEnvolActivation()
        );
    }
}
