<?php

namespace Application\Controls;

use Application\Propel\Mpe\CommonTDonneeComplementaire;
use Application\Service\Atexo\Atexo_CommonReferentiel;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Config\Atexo_Config_Exception;
use Prado\Prado;

class DonneesComplementaireVariantes extends MpeTTemplateControl
{
    private $mode;
    private $_donneeComplementaire;
    private bool $afficher = true;

    public function getDonneeComplementaire()
    {
        return $this->_donneeComplementaire;
    }

    public function setDonneeComplementaire($donneeComplementaire)
    {
        $this->_donneeComplementaire = $donneeComplementaire;
    }

    public function setAfficher($afficher)
    {
        $this->afficher = $afficher;
    }

    public function getAfficher()
    {
        return $this->afficher;
    }

    /**
     * Affecte la valeur.
     *
     * @param string $value
     */
    public function setMode($mode)
    {
        $this->mode = $mode;
    }

    /**
     * recupère la valeur.
     */
    public function getMode()
    {
        return $this->mode;
    }

    public function onLoad($param)
    {
        if (Atexo_Module::isEnabled('DonneesComplementaires') && Atexo_Module::isEnabled('DonneesRedac')) {
            if (!$this->Page->IsPostBack) {
                self::chargerComposant();
            }
        }
    }

    /**
     * Remplir liste des reférentiels CCAG.
     *
     * @throws Atexo_Config_Exception
     */
    public function fillCcagReference()
    {
        $dataSource = [];
        $dataCcagRef = (new Atexo_CommonReferentiel())->retrieveValeurReferentielCCAG(Atexo_Config::getParameter('REFERENTIEL_CCAG_REFERENCE'), false, true);
        $dataSource[0] = Prado::localize('TEXT_SELECTIONNER').'...';

        if ($dataCcagRef) {
            foreach ($dataCcagRef as $oneVal) {
                $dataSource[$oneVal->getId()] = $oneVal->getLibelleValeurReferentiel(); //$oneVal->getLibelle2()
            }
        }

        $this->CcagRef->DataSource = $dataSource;
        $this->CcagRef->DataBind();
    }

    /*
     * sauvegarde des donnees Variante
     */
    public function saveVariante(&$donneComplementaire)
    {$this->_donneeComplementaire = $donneComplementaire;
		 if($this->_donneeComplementaire instanceof CommonTDonneeComplementaire) {
			 if ('0' != $this->CcagRef->getSelectedValue()) {
				 $this->_donneeComplementaire->setIdCcagReference($this->CcagRef->getSelectedValue());
			 } else {
				 $this->_donneeComplementaire->setIdCcagReference(null);
			 }
			 if ($this->varianteAutorisee->Checked) {
				 $this->_donneeComplementaire->setVariantesAutorisees(1);
			 } else {
				 $this->_donneeComplementaire->setVariantesAutorisees(0);
			 }
			 if ($this->varianteExigee->Checked) {
				 $this->_donneeComplementaire->setVarianteExigee(1);
			 } else {
				 $this->_donneeComplementaire->setVarianteExigee(0);
			 }

            if ($this->varianteTechniquesObligatoires->Checked) {
                $this->_donneeComplementaire->setVariantesTechniquesObligatoires(1);
                $this->_donneeComplementaire->setVariantesTechniquesDescription((new Atexo_Config())->toPfEncoding($this->descVariantesTech->Text));
            } else {
                $this->_donneeComplementaire->setVariantesTechniquesObligatoires(0);
                $this->_donneeComplementaire->setVariantesTechniquesDescription(null);
            }
        }
    }

    public function populateDatas()
    {
        if (($this->_donneeComplementaire instanceof CommonTDonneeComplementaire)) {
            if ($this->_donneeComplementaire->getIdCcagReference()) {
                $this->CcagRef->SelectedValue = $this->_donneeComplementaire->getIdCcagReference();
            }
            if ($this->_donneeComplementaire->getIdCcagDpi()) {
                $this->CcagDpi->setSelectedValue($this->_donneeComplementaire->getIdCcagDpi());
            }
            if ($this->_donneeComplementaire->getVariantesAutorisees()) {
                $this->varianteAutorisee->Checked = true;
            } else {
                $this->varianteAutorisee->Checked = false;
            }
            if ($this->_donneeComplementaire->getVarianteExigee()) {
                $this->varianteExigee->Checked = true;
            } else {
                $this->varianteExigee->Checked = false;
            }

            if ($this->_donneeComplementaire->getVariantesTechniquesObligatoires()) {
                $this->varianteTechniquesObligatoires->Checked = true;
                $this->descVariantesTech->Text = $this->_donneeComplementaire->getVariantesTechniquesDescription();
            } else {
                $this->varianteTechniquesObligatoires->Checked = false;
            }
        } else {
            $this->varianteAutorisee->Checked = false;
            $this->varianteExigee->Checked = false;
            $this->varianteTechniquesObligatoires->Checked = false;
        }
    }

    public function chargerComposant()
    {
        self::fillCcagReference();
        self::fillCcagDpi();
        self::populateDatas();
        if (true == $this->getAfficher()) {
            $this->panelVariantes->style = 'display:""';
        } else {
            $this->panelVariantes->style = 'display:none';
        }
        self::rafraichissementJs();
    }

    /*
     * Permet d'afficher les elements du composant selon le mode choisi
     */
    public function enabledElement()
    {
        return match ($this->mode) {
            0 => false,
            1 => true,
            default => true,
        };
    }

    public function rafraichissementJs()
    {
        $this->scriptLabel->Text = '<script>'.
            "isCheckedShowDiv(document.getElementById('".$this->varianteTechniquesObligatoires->ClientId."'),'".$this->desccription_varvianteTech->getClientId()."');".
                                    "displayDpi(document.getElementById('".$this->CcagRef->ClientId."'));";

        if ('Agent.PopupDetailLot' === $_GET['page']) {
            //Bloc CCAG - Variante
            $this->scriptLabel->Text .='disactivateSelect("'.$this->CcagRef->ClientId.'");';
            $this->scriptLabel->Text .='disactivateSelect("'.$this->varianteAutorisee->ClientId.'");';
            $this->scriptLabel->Text .='disactivateSelect("'.$this->varianteExigee->ClientId.'");';
            $this->scriptLabel->Text .='disactivateSelect("'.$this->varianteTechniquesObligatoires->ClientId.'");';
        }

        $this->scriptLabel->Text .= '</script>';
    }

    /**
     * charge les option de la liste deroulante des droits de proprietaire intellectuelle.
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function fillCcagDpi()
    {
        $dataSource = [];
        $dataCcagDpi = (new Atexo_CommonReferentiel())->retrieveValeurReferentiel(Atexo_Config::getParameter('REFERENTIEL_CCAG_DPI'));
        $dataSource[0] = Prado::localize('TEXT_SELECTION_LISTE');
        if ($dataCcagDpi) {
            foreach ($dataCcagDpi as $oneVal) {
                $dataSource[$oneVal->getId()] = $oneVal->getLibelleValeurReferentiel();
            }
        }
        $this->CcagDpi->DataSource = $dataSource;
        $this->CcagDpi->DataBind();
    }

    public function validerDonneesComplementairesVariantes(&$isValid, &$arrayMsgErreur)
    {
        if ($this->varianteTechniquesObligatoires->Checked && !$this->descVariantesTech->Text) {
            $isValid = false;
            $arrayMsgErreur[] = Prado::localize('DEFINE_VARIANTES_TECHNIQUES_OBLIGATOIRES');
        }
        if (!$this->CcagRef->getSelectedValue()) {
            $isValid = false;
            $arrayMsgErreur[] = Prado::localize('DEFINE_CCAG_REFERENCE');
        }
    }
}
