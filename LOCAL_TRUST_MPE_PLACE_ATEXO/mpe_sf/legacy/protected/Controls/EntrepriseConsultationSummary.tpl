<!--BEGIN RECAP CONSULTATION-->
<div class="form-bloc form-horizontal recap-infos-consultation" id="recap-consultation">
	<div class="panel panel-default" id="collapseHeading" >
		<div class="panel-heading clearfix" role="button" data-toggle="collapse" href="#collapseInfo" aria-expanded="true">
			<h1 class="h4 panel-title">
				<com:TTranslate>DEFINE_LIEN_FICHE_DETAIL</com:TTranslate>
			</h1>
		</div>
		<div>
			<div id="collapseBodyInfo" class="panel-collapse recap-body panel-body clearfix collapse in" role="tablist" aria-labelledby="collapseInfo">
				<div class="recap-bloc">
					<div class="clearfix item_consultation">
						<!--BEGIN DATE LIMITE REMISE DES PLIS-->
						<div class="col-md-10">
							<span class="col-md-8 col-xs-12">
								<com:TPanel id="panelDateLimiteRemisePlis" visible="true">
									<label class="control-label">
										<com:TTranslate>DEFINE_DATE_HEURE_LIMITE_REMISE_PLIS</com:TTranslate> :
									</label>
									<com:TLabel id="dateHeureLimiteRemisePlis"/>
									<span class="small">
										<com:TLabel id="libelleHeureLocalisation"/>
									</span>
									<span class="m-l-1">
										<com:TImage id="pictCertificat" />
									</span>
									<com:TImage id="pictMpsLogo" style="display:none;"/>
									<com:TConditional id="panelPictMpsLogoAffichage" visible="false">
										<prop:TrueTemplate>
											<i class="s s-mps s-18"></i>
										</prop:TrueTemplate>
									</com:TConditional>
									<com:TLabel id="logoDume" />
									<com:TImage id="pictConsultationAnnulee"/>
								</com:TPanel>
								<com:TPanel id="panelDateFinAffichage" visible="false" CssClass="float-left" >
									<label class="control-label">
										<com:TTranslate>DEFINE_TEXT_DATE_FIN_AFFICHAGE</com:TTranslate> :
									</label>
								</com:TPanel>
								<com:TPanel id="panelDatefinLocale" cssclass="line" visible="false">
									<com:TPanel id="panelDateLimiteRemisePlisLocale" visible="true" CssClass="float-left" >
										<div style="visibility:hidden;">
											<label class="control-label">
												<com:TTranslate>DEFINE_DATE_HEURE_LIMITE_REMISE_PLIS</com:TTranslate> :
											</label>
										</div>
									</com:TPanel>
									<com:TLabel id="dateFinLocale" />
									<com:TLabel id="libelleDateHeureLocale" />
								</com:TPanel>
							</span>
						</div>
						<!--END DATE LIMITE REMISE DES PLIS-->

						<!--BEGIN REFERENCE-->
						<div class="col-md-10 text-justify">
							<label class="col-md-4 col-xs-12 spacing-helper-m">
								<com:TTranslate>REFERENCE</com:TTranslate> :
							</label>
							<span class="col-md-8 col-xs-12">
								 <com:TLabel id="reference" />
							</span>
						</div>
						<!--END REFERENCE-->

						<!--BEGIN INTITULE-->
						<div class="col-md-10 text-justify">
							<label class="col-md-4 col-xs-12 spacing-helper-m">
								<com:TTranslate>DEFINE_INTITULE</com:TTranslate> :
							</label>
							<span class="col-md-8 col-xs-12">
								 <com:TLabel id="intitule" />
							</span>
						</div>
						<!--END INTITULE-->

						<!--BEGIN OBJET-->
						<div class="col-md-10 text-justify">
							<label class="col-md-4 col-xs-12 spacing-helper-m">
								<com:TTranslate>OBJET</com:TTranslate> :
							</label>
							<span class="col-md-8 col-xs-12">
								<com:TLabel id="objet" />
							</span>
						</div>
						<!--END OBJET-->
					</div>
					<ul  id="collapseInfo" class="panel-collapse collapseBody list-unstyled clearfix collapse in" role="tablist" aria-labelledby="collapseInfo">
						<li>
							<hr />
						</li>
						<li class="col-md-6">
							<ul class="list-unstyled">
								<li class="clearfix">
									<com:TPanel id="panelLieuResidence" cssclass="line" visible="false">
										<com:TPanel visible="true" CssClass="float-left" >
											<label class="control-label">
												<com:TTranslate>DEFINE_LIEU_RESIDENCE</com:TTranslate> :
											</label>
											<span class="p-l-1">
												<com:TLabel id="lieuResidence" />
											</span>
										</com:TPanel>
									</com:TPanel>
								</li>
								<li class="clearfix">
									<com:TPanel ID="panelDateAnnulation" >
										<label class="col-md-4 col-xs-12 spacing-helper-m">
											<com:TTranslate>DEFINE_DATE_D_ANNULATION</com:TTranslate> :
										</label>
										<span class="col-md-8 col-xs-12">
														<com:TLabel id="dateAnnulation" />
													</span>
									</com:TPanel>
								</li>
								<!-- <div id="infosPrincipales" style="display:none;" class="panel-toggle"> -->
								<li class="clearfix">
									<com:TPanel id="panelDetailsAnnonce" visible="false">
										<label class="col-md-4 col-xs-12 spacing-helper-m">
											<com:TTranslate>DEFINE_TEXT_DETAIL_ANNONCE</com:TTranslate> :
										</label>
										<span class="col-md-8 col-xs-12">
														<com:TLabel ID="detailAnnonce"/>
													</span>
									</com:TPanel>
								</li>
								<li class="clearfix">
									<label class="col-md-4 col-xs-12 spacing-helper-m">
										<com:TTranslate>DEFINE_ENTITE_PUBLIQUE</com:TTranslate>
									</label>
									<span class="col-md-8 col-xs-12">
											<com:TLabel id="entitePublique" />
										</span>
								</li>
								<li class="clearfix">
									<label class="col-md-4 col-xs-12 spacing-helper-m">
										<com:TTranslate>DEFINE_ENTITE_ACHAT</com:TTranslate> :
									</label>
									<span class="col-md-8 col-xs-12">
										<com:TLabel id="entiteAchat" />
									</span>
								</li>
								<li class="clearfix">
									<label class="col-md-4 col-xs-12 spacing-helper-m">
										<com:TTranslate>TYPE_ANNONCE</com:TTranslate> :
									</label>
									<span class="col-md-8 col-xs-12">
										<com:TLabel id="annonce" />
									</span>
								</li>
								<li class="clearfix">
									<label class="col-md-4 col-xs-12 spacing-helper-m">
										<com:TTranslate>DEFINE_PROCEDURE</com:TTranslate> :
									</label>
									<span class="col-md-8 col-xs-12">
											<com:TLabel id="typeProcedure" />
											<com:TLabel ID="modePassation" visible="<%=Application\Service\Atexo\Atexo_Module::isEnabled('ConsultationModePassation')? true:false %>"/>
										</span>
								</li>
							</ul>
						</li>
						<li class="col-md-6">
							<ul class="list-unstyled">
								<li class="clearfix">
									<label class="col-md-4 col-xs-12 spacing-helper-m">
										<com:TTranslate>DEFINE_CATEGORIE_PRINCIPALE</com:TTranslate> :
									</label>
									<span class="col-md-8 col-xs-12">
										<com:TLabel id="categoriePrincipale" />
									</span>
								</li>
								<li class="clearfix">
									<label class="col-md-4 col-xs-12 spacing-helper-m">
										<com:TTranslate>TEXT_ALLOTISSEMENT</com:TTranslate> :
									</label>
									<span class="col-md-8 col-xs-12">
										<com:THyperLink id="linkDetailLots">
											<i class="fa fa-cubes text-warning m-r-1" aria-hidden="true"></i>
											<com:TLabel id="nbrLots" />
										</com:THyperLink>
									</span>
								</li>
								<li class="clearfix">
									<com:TPanel visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('AffichageCodeCpv'))? true:false %>">
										<label class="col-md-4 col-xs-12 spacing-helper-m">
											<com:TTranslate>TEXT_CODE_CPV</com:TTranslate> :
										</label>
										<span class="col-md-8 col-xs-12">
											<com:AtexoReferentiel id="idAtexoRef" cas="cas4" display="onlyCpvPrincipal"/> (Code principal)
										</span>
									</com:TPanel>
								</li>
								<li class="clearfix">
									<com:TPanel cssClass="line" Visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('LieuxExecution'))? true:false %>">
										<label class="col-md-4 col-xs-12 spacing-helper-m">
											<com:TTranslate>DEFINE_LIEUX_EXECUTION</com:TTranslate> :
										</label>
										<span class="col-md-8 col-xs-12">
											<com:TLabel id="lieuxExecutions" />
											<com:TLabel id="spanLieux"  Attributes.onmouseover="afficheBulle('infosLeiExecution', this)"
														Attributes.onmouseout="cacheBulle('infosLeiExecution')"
														CssClass="info-suite">...
											</com:TLabel>
										</span>
										<!--<div id="infosLeiExecution" class="info-bulle" onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();"><div><com:TLabel id="lieuxExecutionSuite"  ></com:TLabel></div></div>-->
									</com:TPanel>
								</li>
								<li class="clearfix">
									<com:TPanel visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('CodeNutLtReferentiel'))? true:false %>" CssClass="line">
										<label class="col-md-4 col-xs-12 spacing-helper-m">
											<com:TTranslate>DEFINE_LIEUX_EXECUTION</com:TTranslate> :
										</label>
										<span class="col-md-8 col-xs-12">
											<div class="truncate-200">
												<com:TLabel id="codesNuts" />
											</div>
											<com:TLabel id="spanCodesNuts"  Attributes.onmouseover="afficheBulle('infosCodesNuts', this)" Attributes.onmouseout="cacheBulle('infosCodesNuts')" CssClass="info-suite">
												...
											</com:TLabel>
										</span>
										<!--<div id="infosCodesNuts" class="info-bulle" onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();">
											<div>
												<com:TLabel id="lieuxinfosCodesNutsSuite"></com:TLabel>
											</div>
											</div>-->
									</com:TPanel>
								</li>
							</ul>
						</li>
					</ul>
				</div>
				<com:TPanel id="infosPrincipales">
					<com:AtexoLtReferentiel id="ltrefCons" consultation="1" idPage="EntrepriseConsultationSummary" mode="0"/>
					<com:AtexoReferentielZoneText id="idReferentielZoneText" consultation="1" idPage="EntrepriseConsultationSummary" mode="0" type="1" label="1"/>
					<com:AtexoLtRefRadio id="idRefRadio" consultation="1"  cssClass="intitule-240 bold" idPage="EntrepriseConsultationSummary" mode="0" type="2" label="true"/>

					<com:TPanel ID="panelDomaineActivites" cssClass="line" >
						<div class="intitule-240 bold"><com:TTranslate>TEXT_DOMAINES_ACTIVITE</com:TTranslate> : </div>
						<div class="content-bloc bloc-500">
							<!-- ul class="default-list" -->
							<com:TLabel ID="domainesActivite" />
							<!-- /ul -->
						</div>
					</com:TPanel>
					<!-- D?but domaines d'activit?s Lt-Ref -->
					<com:TPanel ID="panelDomaineActivitesLtRef" cssClass="line" >
						<div class="intitule-240 bold"><com:TTranslate>TEXT_DOMAINES_ACTIVITE</com:TTranslate> : </div>
						<div class="content-bloc bloc-500">
							<com:AtexoReferentiel ID="idAtexoRefDomaineActivites" cas="cas9"/>
						</div>
					</com:TPanel>
					<!-- Fin domaines d'activit?s Lt-Ref -->
					<span class="spacer-small"></span>
					<com:TPanel ID="adresseRetraitDossier" cssClass="line" >
						<div class="intitule-240 bold"><com:TTranslate>ADRESSE_RETRAIT_DOSSIERS</com:TTranslate> : </div>
						<div class="content-bloc bloc-500"><com:TLabel ID="adresseRetraitDossiers" /></div>
					</com:TPanel>
					<com:TPanel ID="panelAdresseDepotOffre" cssClass="line" Display="None">
						<div class="intitule-240 bold"><com:TTranslate>TEXT_ADRESSE_DEPOT_OFFRES</com:TTranslate> : </div>
						<div class="content-bloc bloc-500"><com:TLabel ID="adresseDepotOffres" /></div>
					</com:TPanel>
					<com:TPanel ID="panelLieuOuverturePlis" cssClass="line" >
						<div class="intitule-240 bold"><com:TTranslate>LIEU_OUVERTURE_PLIS</com:TTranslate> : </div>
						<div class="content-bloc bloc-500"><com:TLabel ID="lieuOuverturePlis" /></div>
					</com:TPanel>
					<span class="spacer-small" ></span>
					<com:TPanel ID="prixAcquisitionPlans" cssClass="line" >
						<div class="intitule-240 bold"><com:TTranslate>PRIX_ACQUISITION_DES_PLANS</com:TTranslate> : </div>
						<div class="content-bloc bloc-500"><com:TLabel ID="prixAcquisitionPlan" /></div>
					</com:TPanel>
					<com:TPanel ID="panelConsultationNonAlotti">
						<com:TPanel ID="panelCautionProvisoire" cssClass="line" >
							<div class="intitule-240 bold"><com:TTranslate>CAUTION_PROVISOIRE</com:TTranslate> : </div>
							<div class="content-bloc bloc-500"><com:TLabel ID="cautionProvisoire" /></div>
						</com:TPanel>
						<span class="spacer-small"></span>
						<com:TPanel ID="panelQualification" cssClass="line" >
							<div class="intitule-240 bold"><com:TTranslate>TEXT_QUALIFICATIONS</com:TTranslate> : </div>
							<div class="content-bloc bloc-500">
								<!-- ul class="default-list" -->
								<com:TLabel ID="qualification" />
								<!-- /ul -->
							</div>
						</com:TPanel>
						<com:TPanel ID="panelAgrements" cssClass="line" >
							<div class="intitule-240 bold"><com:TTranslate>TEXT_AGREMENT</com:TTranslate> : </div>
							<div class="content-bloc bloc-500">
								<!-- ul class="default-list" -->
								<com:TLabel ID="agrements" />
								<!-- /ul -->
							</div>
						</com:TPanel>
						<span class="spacer-small"></span>
						<com:TPanel ID="panelEchantillons" cssClass="line" >
							<div class="intitule-240 bold"><com:TTranslate>ECHANTILLONS_DEMANDES</com:TTranslate> : </div>
							<div class="content-bloc bloc-500"><com:TLabel ID="labelDateHeureLimiteEchantillon" Text="<%=Prado::localize('DEFINE_DATE_HEURE_LIMITE')%> :"/><com:TLabel ID="dateEchantillons" /><br /><com:TLabel ID="adresseEchantillons" /></div>
						</com:TPanel>
						<com:TPanel ID="panelReunion" cssClass="line" >
							<div class="intitule-240 bold"><com:TTranslate>REUNION</com:TTranslate> : </div>
							<div class="content-bloc bloc-500"><com:TLabel ID="labelDateHeureReunion" Text="<%=Prado::localize('DEFINE_DATE_HEURE')%> :"/><com:TLabel ID="dateReunion" /><br /><com:TLabel ID="adresseReunion" /></div>
						</com:TPanel>
						<com:TPanel ID="panelVisitesLieux" cssClass="line" >
							<div class="intitule-240 bold"><com:TTranslate>VISITE_DES_LIEUX</com:TTranslate> : </div>
							<com:TPanel ID="panelRepeaterVisitesLieux" cssClass="content-bloc bloc-500">
								<com:TRepeater id="repeaterVisitesLieux">
									<prop:HeaderTemplate>
										<ul class="default-list">
									</prop:HeaderTemplate>
									<prop:ItemTemplate>
										<li>
											<com:TLabel ID="dateVisites" Text="<%# Application\Service\Atexo\Atexo_Util::iso2frnDateTime($this->Data->getDate())%>"/>
											,&nbsp;
											<com:TLabel ID="adresseVisites" Text="<%#$this->Data->getAdresseTraduit()%>"/>
										</li>
									</prop:ItemTemplate>
									<prop:FooterTemplate>
										</ul>
									</prop:FooterTemplate>
								</com:TRepeater>
							</com:TPanel>
							<com:TPanel ID="panelPasVisitelieu" cssClass="content-bloc bloc-500" visible="false">- </com:TPanel>
						</com:TPanel>
						<com:TPanel ID="panelVariante" cssClass="line">
							<div class="intitule-240 bold"><com:TTranslate>TEXT_VARIANTE</com:TTranslate> : </div>
							<div class="content-bloc bloc-500"><com:TLabel ID="varianteValeur" /></div>
						</com:TPanel>
						<span class="spacer-small"></span>
					</com:TPanel>
					<com:TPanel ID="panelContcatAdministratif" cssClass="line" >
						<div class="intitule-240 bold"><com:TTranslate>TEXT_CONTACT_ADMINISTRATIF</com:TTranslate> : </div>
						<div class="content-bloc bloc-500">
							<com:TLabel ID="contactAdministratif" /><br />
							<com:TPanel ID="blocDetailContactAdministratif">
								<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-email.gif" alt="<%=Prado::localize('ADRESSE_ELECTRONIQUE')%>" title="<%=Prado::localize('ADRESSE_ELECTRONIQUE')%>" /><com:TLabel ID="email" /><br />
								<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-tel.gif" alt="<%=Prado::localize('TEXT_TEL')%>" title="<%=Prado::localize('TEXT_TEL')%>" /><com:TLabel ID="telephone" /><br />
								<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-fax.gif" alt="<%=Prado::localize('TEXT_TELECOPIEUR')%>" title="<%=Prado::localize('TEXT_TELECOPIEUR')%>" /><com:TLabel ID="telecopieur"/>
							</com:TPanel>
						</div>
					</com:TPanel>
					<!--  -->
				</com:TPanel>
			</div>
		</div>
	</div>
</div>
<!--END RECAP CONSULTATION-->