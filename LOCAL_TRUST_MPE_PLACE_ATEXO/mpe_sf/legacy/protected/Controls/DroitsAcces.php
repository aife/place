<?php

namespace Application\Controls;

use App\Service\Agent\AgentInviterService;
use App\Service\AgentService;
use App\Service\Consultation\ConsultationService;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonAgentPeer;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationFavoris;
use Application\Propel\Mpe\CommonConsultationFavorisQuery;
use Application\Propel\Mpe\CommonConsultationPeer;
use Application\Propel\Mpe\CommonHistoriquesConsultation;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_Crypto;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Guests;
use Application\Service\Atexo\Consultation\Atexo_Consultation_GuestVo;
use Application\Service\Atexo\Consultation\Guests\Atexo_Consultation_Guests_CriteriaVo;
use Application\Service\Atexo\EntityPurchase\Atexo_EntityPurchase_Mandataire;
use PDO;
use Prado\Prado;

/**
 * Composant de la gestion des droits d'accès.
 *
 * @author Anas ZAKI <anas.zaki@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class DroitsAcces extends MpeTTemplateControl
{
    public $_reference;
    public $critereTri;
    public $triAscDesc;
    public $_parentPage;
    public ?bool $gestionCentralisee = null;
    public $_IdService;
    public $_IdServiceAssocie;
    public $_IdRpa;
    public bool $_postBack = false;
    public $_consultation;

    public function onLoad($param)
    {
        if (!$this->Page->isPostBack) {
            $this->chargerComposant();
        }
        $organisme = Atexo_CurrentUser::getCurrentOrganism();

        if (Atexo_Module::isEnabled('OrganisationCentralisee', $organisme)) {
            $this->gestionCentralisee = true;
        } else {
            $this->gestionCentralisee = false;
        }
    }

    public function setReference($reference)
    {
        $this->_reference = $reference;
    }

    public function getReference()
    {
        return $this->_reference;
    }

    /**
     * Affiche les invités.
     */
    public function displayGuests()
    {
        $invites = null;
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $organisme = Atexo_CurrentUser::getCurrentOrganism();
        $service = $this->getIdService();
        $criteriaVo = new Atexo_Consultation_Guests_CriteriaVo();
        $criteriaVo->setCurAgentServiceId($service);
        $criteriaVo->setOrgAcronym($organisme);
        if (Atexo_Module::isEnabled('OrganisationCentralisee', $organisme)) {
            $criteriaVo->setCentralizedOrganization(true);
        } else {
            $criteriaVo->setCentralizedOrganization(false);
        }
        if (Atexo_Module::isEnabled('GestionMandataire')) {
            $myService = Atexo_EntityPurchase::retrieveEntityById(Atexo_CurrentUser::getIdServiceAgentConnected(), Atexo_CurrentUser::getCurrentOrganism());
            if ($myService && '0' == $myService->getAffichageService() && $service == Atexo_CurrentUser::getIdServiceAgentConnected()) {
                $criteriaVo->setCentralizedOrganization(false);
            }
        }
        $permanentsGuestsMonEntitee = Atexo_Consultation_Guests::retrievePermanentGuestsMonEntitee($criteriaVo);
        $permanentsGuestsEntiteeDependante = Atexo_Consultation_Guests::retrievePermanentGuestsEntiteeDependante($criteriaVo);
        $permanentsGuestsEntiteeTransverse = Atexo_Consultation_Guests::retrievePermanentGuestsEntiteeTransverse($criteriaVo);
        $permanentsGuests = array_unique(array_merge($permanentsGuestsMonEntitee, $permanentsGuestsEntiteeDependante, $permanentsGuestsEntiteeTransverse), SORT_REGULAR);

        $allGuests = [];
        $reference = $this->getReference();
        if ($reference) {
            //Modification de la consultation

            $criteriaVo = new Atexo_Consultation_Guests_CriteriaVo();
            $criteriaVo->setOrgAcronym(Atexo_CurrentUser::getCurrentOrganism());
            $criteriaVo->setIdReference($reference);

            $criteriaVo->setReadOnlyGuests(false);
            $invitedGuests = (new Atexo_Consultation_Guests())->retrieveInvitedGuests($criteriaVo);

            $criteriaVo->setReadOnlyGuests(true);
            $readOnlyGuests = (new Atexo_Consultation_Guests())->retrieveInvitedGuests($criteriaVo);
            $allGuests = array_merge($permanentsGuests, $invitedGuests, $readOnlyGuests);
        } else {
            if (!Atexo_CurrentUser::hasHabilitation('InvitePermanentMonEntite')) { // le créateur n'est pas invité permanent
                $creatorGuestVo = new Atexo_Consultation_GuestVo();
                $createorAgent = CommonAgentPeer::retrieveByPK(Atexo_CurrentUser::getId(), $connexionCom);
                $creatorGuestVo->populate($createorAgent);
                $creatorGuestVo->setPermanentGuest(0);
                $creatorGuestVo->setTypeInvitation(1);
                $allGuests[] = $creatorGuestVo;
            }

            if (Atexo_Module::isEnabled('GestionMandataire')) {
                $myService = Atexo_EntityPurchase::retrieveEntityById(Atexo_CurrentUser::getIdServiceAgentConnected(), Atexo_CurrentUser::getCurrentOrganism());
                if ($myService && '0' == $myService->getAffichageService()) {
                    $permanentsGuests = $this->changeOnHabilitation($permanentsGuests, 1);
                }
            }
            $allGuests = array_merge($allGuests, $permanentsGuests);
        }
        $arrayGuests = Atexo_Consultation_Guests::guestVoToArrayGuests($allGuests);
        if ($this->getPostBack()) {
            $invites = '';
            if (is_array($arrayGuests)) {
                foreach ($arrayGuests as $idAgent => $guest) {
                    if ('1' == $guest['InvitePermanent']) {
                        $type = '1';
                    } elseif ('1' == $guest['SuiviHabilitation']) {
                        $type = '2';
                    } else {
                        $type = '3';
                    }

                    $invites .= $idAgent.'-'.$type.',';
                }
            }
        }
        $this->setViewState('invitesChoisis', $arrayGuests);
        $this->Page->invites->Value = trim($invites, ',');
        $this->nbrInvite->Text = count($allGuests);
        $this->RepeaterInvites->dataSource = $allGuests;
        $this->RepeaterInvites->dataBind();
    }

    /**
     * Récupère une liste d'objet invités  et les transforme en invités avec habilitation.
     *
     * @param array $list
     *
     * @return array $list
     */
    public function changeOnHabilitation($list, $typeInvitation)
    {
        $permanentsGuests = [];
        if (is_array($list) && count($list)) {
            foreach ($list as $oneList) {
                if ($oneList instanceof Atexo_Consultation_GuestVo) {
                    $oneList->setPermanentGuest(0);
                    $oneList->setTypeInvitation($typeInvitation);
                    $permanentsGuests[] = $oneList;
                }
            }
        }

        return $permanentsGuests;
    }

    /**
     * remplissage de la liste du représentant du pouvoir adjudicateur.
     */
    public function bindDataRpa()
    {
        // Débbut remplissage de la liste des représentants du pouvoir adjudicateur
        if ('DetailConsultation' == $this->getParentPage()) {
            $this->representantAdjudicateur->Enabled = false;
        }
        $this->representantAdjudicateur->Datasource = self::remplirListRpa();
        $this->representantAdjudicateur->dataBind();
        if ($this->getIdRpa()) {
            $this->representantAdjudicateur->setSelectedValue($this->getIdRpa());
        }
        // Fin remplissage de la liste des représentants du pouvoir adjudicateur
    }

    /**
     * remplir la liste RPA.
     */
    public function remplirListRpa()
    {
        $ArrayRpa = [];

        if ($this->entiteeAssociee->getSelectedValue() != $this->getIdService()) {
            $sql = 'Select * From RPA Where id_service in ('.(new Atexo_Db())->quote($this->entiteeAssociee->getSelectedValue()).','.$this->getIdService().") AND acronymeOrg = '".
                    Atexo_CurrentUser::getCurrentOrganism()."' ";
            $results = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_OBJ);

            foreach ($results as $row) {
                $ArrayRpa[$row->id] = $row->nom.' '.$row->prenom;
            }
        }
        $ArrayRpa[' '] = Prado::localize('TEXT_SELECTIONNER');

        return $ArrayRpa;
    }

    /**
     * remplir mon entité d'achat.
     */
    public function remplirMonEntite()
    {
        // Debut remplissage de mon ientité
        $langue = Atexo_CurrentUser::readFromSession('lang');
        $idMyEntity = $this->getIdService();
        $this->monEntite->Text = Atexo_EntityPurchase::getPathEntityById($idMyEntity, Atexo_CurrentUser::getCurrentOrganism(), $langue);
    }

    /*
     * Permet rafraichir la liste des RPA
     *
     * @return void
     * @author : ASO : <ayoub.souidahmed@atexo.com>
     * @version : depuis 4.8.0
     */
    public function RefreshListRPA($sender, $param)
    {
        $this->setIdServiceAssocie($this->entiteeAssociee->getSelectedValue());
        $this->fillListRpa();
        $this->updateGuests($sender, $param);
    }

    /**
     *  vérifie si l'agent é la possibilité de créer une consultration transverse.
     */
    public function isTransverseTender()
    {
        if (Atexo_CurrentUser::hasHabilitation('CreerConsultationTransverse')) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Trier un tableau des invités sélectionnés.
     *
     * @param sender
     * @param param
     */
    public function sortCheckedGuests($sender, $param)
    {
        $this->critereTri = $this->getViewState('critereTri');
        $this->triAscDesc = $this->getViewState('triAscDesc');
        $checkedGuests = Atexo_Consultation_Guests::toArrayGuestsVo($this->getViewState('invitesChoisis'));
        if ('FirstName' == $sender->getActiveControl()->CallbackParameter) {
            $dataSourceTriee = $this->getDataSourceTriee($checkedGuests, 'FirstName', '');
        } elseif ('LastName' == $sender->getActiveControl()->CallbackParameter) {
            $dataSourceTriee = $this->getDataSourceTriee($checkedGuests, 'LastName', '');
        } elseif ('EntityPurchase' == $sender->getActiveControl()->CallbackParameter) {
            $dataSourceTriee = $this->getDataSourceTriee($checkedGuests, 'EntityPurchase', '');
        } else {
            return;
        }
        // Mise dans la session de la liste des invités triée
        $arrayGuests = Atexo_Consultation_Guests::guestVoToArrayGuests($dataSourceTriee[0]);
        $this->setViewState('invitesChoisis', $arrayGuests);
        //Mise en viewState des critéres de tri
        $this->setViewState('critereTri', $this->critereTri);
        $this->setViewState('triAscDesc', $this->triAscDesc);
        //Remplissage du tabeau des invités
        $this->RepeaterInvites->setCurrentPageIndex(0);
        $this->RepeaterInvites->DataSource = $dataSourceTriee[0];
        $this->RepeaterInvites->DataBind();
        // Re-affichage du TActivePanel
        $this->nbrInvite->Text = is_countable($dataSourceTriee[0]) ? count($dataSourceTriee[0]) : 0;
        $this->ApRepeaterInvites->render($param->getNewWriter());
    }

    /**
     * Retourne un tableau de colonnes et un tableau d'objets tries.
     *
     * @param array  $dataSource    le tableau des données
     * @param string $critere
     * @param string $typeCritere
     * @param int    $indiceElement
     */
    public function getDataSourceTriee($dataSource, $critere, $typeCritere = null)
    {
        $resultat = [];
        $this->setCritereAndTypeTri($critere);
        $datatriee = null;
        if ('num' == $typeCritere) {
            $datatriee = Atexo_Util::sortArrayOfObjects($dataSource, $critere, $this->triAscDesc, 'num');
        } else {
            $datatriee = Atexo_Util::sortArrayOfObjects($dataSource, $critere, $this->triAscDesc);
        }
        $resultat[0] = $datatriee;

        return $resultat;
    }

    /**
     * Définit les critères et les types de tri.
     *
     * @param string $critere
     * @param int    $indiceElement
     */
    public function setCritereAndTypeTri($critere)
    {
        if ($this->critereTri != $critere || !$this->critereTri) {
            $this->triAscDesc = 'ASC';
        } else {
            if ('ASC' == $this->triAscDesc) {
                $this->triAscDesc = 'DESC';
            } else {
                $this->triAscDesc = 'ASC';
            }
        }
        $this->critereTri = $critere;
    }

    /**
     * Met à jour la liste des invités.
     */
    public function updateGuests($sender, $param)
    {
        $consultationId = $this->getReference();

        //Enregistrement des invités de la consultation
        $invitesChoisis = Atexo_Consultation_Guests::toArrayGuestsVo((new Atexo_Consultation_Guests())->createArrayGuestFromValueString($this->Page->invites->Value));
        if (is_array($invitesChoisis)) {
            if (isset($consultationId) && Atexo_CurrentUser::hasHabilitation('ModifierConsultation')) {
                (new Atexo_Consultation_Guests())->deleteGuests($consultationId, Atexo_CurrentUser::getOrganismAcronym());
            }
            if ('DetailConsultation' == $this->getParentPage()) {
                $connexionOrg = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
                $consultationO = CommonConsultationPeer::retrieveByPK($consultationId, $connexionOrg);

                $consultationO = Atexo_Consultation::saveConsultationGuests($consultationO, $invitesChoisis, false);
                $consultationO->saveConsultation($connexionOrg);
                // debut : Historique de la liste des invités
                $nbreInitialInvites = $this->Page->nbrInviteAvant->value;
                $nbreFinalInvites = count($invitesChoisis);
                if ($nbreInitialInvites != $nbreFinalInvites) {
                    $nomElement = Atexo_Config::getParameter('ID_HISTORIQUE_LISTES_INVITES');
                    $valeur = '1';
                    $detail1 = '';
                    foreach ($invitesChoisis as $invite) {
                        $detail1 .= $invite->getId().'_';
                        if ($invite->getPermanentGuest()) {
                            $detail1 .= '1,';
                        } else {
                            $detail1 .= '0,';
                        }
                    }
                    $detail2 = '';
                    $newHistorique = new CommonHistoriquesConsultation();
                    $newHistorique->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
                    $newHistorique->setConsultationId($consultationId);
                    $newHistorique->setIdAgent(Atexo_CurrentUser::getIdAgentConnected());
                    $newHistorique->setNomAgent(Atexo_CurrentUser::getFirstNameAgentConnected());
                    $newHistorique->setPrenomAgent(Atexo_CurrentUser::getLastNameAgentConnected());
                    $newHistorique->setNomElement($nomElement);
                    $newHistorique->setValeur($valeur);
                    $newHistorique->setValeurDetail1($detail1);
                    $newHistorique->setValeurDetail2($detail2);
                    $newHistorique->setNumeroLot('0');
                    $newHistorique->setStatut(Atexo_Config::getParameter('MODIFICATION_FILE'));
                    $atexoCrypto = new Atexo_Crypto();
                    $arrayTimeStamp = $atexoCrypto->timeStampData($valeur.'-'.$detail1.'-'.$detail2);
                    $newHistorique->setHorodatage($arrayTimeStamp['horodatage']);
                    $newHistorique->setUntrusteddate($arrayTimeStamp['untrustedDate']);
                    $newHistorique->save($connexionOrg);
                }
                // fin : Historique de la liste des invités
            }
        }

        $this->saveConsultationFavorisInvitePonctuel($consultationId, Atexo_CurrentUser::getOrganismAcronym(), $connexionOrg);

        $arrayGuests = Atexo_Consultation_Guests::guestVoToArrayGuests($invitesChoisis);
        $this->setViewState('invitesChoisis', $arrayGuests);
        $this->nbrInvite->Text = is_countable($invitesChoisis) ? count($invitesChoisis) : 0;
        $this->RepeaterInvites->dataSource = $invitesChoisis;
        $this->RepeaterInvites->dataBind();
        // Rafraichissement du ActivePanel
        $this->ApRepeaterInvites->render($param->NewWriter);
    }

    public function remplirEntiteAssociee()
    {
        $firstKey = null;
        $langue = Atexo_CurrentUser::readFromSession('lang');
        // Debut remplissage de la liste des entité associées
        $entities = Atexo_EntityPurchase::getEntityPurchase(Atexo_CurrentUser::getCurrentOrganism(), true, $langue);
        Atexo_CurrentUser::writeToSession('services', $entities);
        if ('DetailConsultation' == $this->getParentPage()) {
            $this->entiteeAssociee->Enabled = false;
            $this->boutonOkEntiteeAssociee->setDisplay('None');
        }

        $this->entiteeAssociee->Datasource = array_map('htmlspecialchars', $entities);
        $this->entiteeAssociee->dataBind();
        if ($this->getIdServiceAssocie() || '0' === $this->getIdServiceAssocie()) {
            $this->entiteeAssociee->setSelectedValue($this->getIdServiceAssocie());
        } else {
            $value = '';
            foreach ($entities as $key => $value) {
                $firstKey = $key;
                break;
            }
            $this->setIdServiceAssocie($firstKey);
        }

        // Fin remplissage de la liste des entité associées
    }

    public function remplirEntite()
    {
        $firstKey = null;
        $idMonService = Atexo_CurrentUser::getIdServiceAgentConnected();
        $serviceSelectionne = $this->getIdService();
        // Debut remplissage de la liste des services de rattachement

        if (Atexo_CurrentUser::hasHabilitation('RattachementService')) {
            /** @var AgentService $agentService */
            $agentService = Atexo_Util::getSfService(AgentService::class);
            $agent = $agentService->getAgent(Atexo_CurrentUser::getId());

            /** @var AgentInviterService $agentInviterService */
            $agentInviterService = Atexo_Util::getSfService(AgentInviterService::class);
            $services = $agentInviterService->getEntitesWhereAgentIsInvited($agent);
            $entities = [];
            foreach ($services as $service) {
                $entities[$service->getId()] = $service->getLibelle();
            }
        } else {
            $entitiesMan = (new Atexo_EntityPurchase_Mandataire())->getInfosMandataire($idMonService);
            $arrayMyEntie = [$idMonService => Atexo_EntityPurchase::getPathEntityById($idMonService, Atexo_CurrentUser::getCurrentOrganism())];
            $entities = $arrayMyEntie + $entitiesMan;
        }

        if (($serviceSelectionne || '0' == $serviceSelectionne) && isset($entities[$serviceSelectionne])) {
            $this->entitee->setSelectedValue($serviceSelectionne);
        } else {
            foreach ($entities as $key => $value) {
                $firstKey = $key;
                break;
            }
            $this->entitee->setSelectedValue($firstKey);
        }
        Atexo_CurrentUser::writeToSession('services', $entities);
        $this->entitee->Datasource = $entities;
        $this->entitee->dataBind();
        if ('DetailConsultation' == $this->getParentPage()) {
            $this->entitee->Enabled = false;
            $this->boutonOkEntitee->setDisplay('None');
        }
    }

    // Fin remplissage de la liste des services de rattachement

    public function getParentPage()
    {
        return $this->_parentPage;
    }

    public function setParentPage($value)
    {
        $this->_parentPage = $value;
    }

    public function getIdService()
    {
        return $this->_IdService;
    }

    public function setIdService($value)
    {
        $this->_IdService = $value;
    }

    public function getIdServiceAssocie()
    {
        return $this->_IdServiceAssocie;
    }

    public function setIdServiceAssocie($value)
    {
        $this->_IdServiceAssocie = $value;
    }

    public function getIdRpa()
    {
        return $this->_IdRpa;
    }

    public function setIdRpa($value)
    {
        $this->_IdRpa = $value;
    }

    public function getDataRepeater()
    {
        return $this->RepeaterInvites->getDataSource();
    }

    public function getListeRpa()
    {
        $arrayRpa = [];
        $arrayRpa['0'] = '--- '.Prado::localize('TEXT_SELECTIONNER').' ---';
        $services = [];

        if ($this->getIdService() || '0' === $this->getIdService() || 0 === $this->getIdService()) {
            array_unshift($services, $this->getIdService());
        }
        if ($this->getIdServiceAssocie() || '0' === $this->getIdServiceAssocie() || 0 === $this->getIdServiceAssocie()) {
            array_unshift($services, $this->getIdServiceAssocie());
        }

        if (is_array($services) && (is_countable($services) ? count($services) : 0) >= 1) {
            $listeServices = implode(',', $services);
            $sql = 'Select distinct id, nom, prenom From RPA Where service_id in ('.(new Atexo_Db())->quote($listeServices).") AND acronymeOrg = '".Atexo_CurrentUser::getCurrentOrganism()."' ";

            $results = Atexo_Db::getLinkCommon(true)->query($sql, PDO::FETCH_OBJ);

            foreach ($results as $row) {
                $arrayRpa[$row->id] = $row->nom.' '.$row->prenom;
            }
        }

        return $arrayRpa;
    }

    /*
     * Permet de remplir la liste des RPA
     *
     * @return void
     * @author : ASO : <ayoub.souidahmed@atexo.com>
     * @version : depuis 4.8.0
     */
    public function fillListRpa()
    {
        if ('DetailConsultation' == $this->getParentPage()) {
            $this->representantAdjudicateur->Enabled = false;
        }
        $listeRpa = self::getListeRpa();
        if (is_array($listeRpa) && 0 == count($listeRpa)) {
            $listeRpa = [' ' => ''];
        }
        $this->representantAdjudicateur->Datasource = $listeRpa;
        $this->representantAdjudicateur->dataBind();
        if ($this->getIdRpa() && array_key_exists($this->getIdRpa(), $listeRpa)) {
            $this->representantAdjudicateur->setSelectedValue($this->getIdRpa());
        }
    }

    public function getPostBack()
    {
        return $this->_postBack;
    }

    public function setPostBack($postBack)
    {
        $this->_postBack = $postBack;
    }

    public function ajouterPermanentServiceAssocie($sender, $param)
    {
        $serviceAssocie = $this->entiteeAssociee->getSelectedValue();
        $result = self::ajouterServiceAssocie($serviceAssocie, '2');
        $this->Page->invites->value = $result;
        $this->Page->invitePanel->render($param->NewWriter);
        self::updateGuests($sender, $param);

    }

    public function ajouterServiceAssocie($serviceAssocie, $typeInvite)
    {
        $organisme = Atexo_CurrentUser::getCurrentOrganism();
        $criteriaVo = new Atexo_Consultation_Guests_CriteriaVo();
        $criteriaVo->setCurAgentServiceId($serviceAssocie);
        $criteriaVo->setOrgAcronym($organisme);
        if (Atexo_Module::isEnabled('OrganisationCentralisee', $organisme)) {
            $criteriaVo->setCentralizedOrganization(true);
        } else {
            $criteriaVo->setCentralizedOrganization(false);
        }
        $permanentsGuestsMonEntitee = Atexo_Consultation_Guests::retrievePermanentGuestsMonEntitee($criteriaVo);
        $permanentsGuestsEntiteeDependante = Atexo_Consultation_Guests::retrievePermanentGuestsEntiteeDependante($criteriaVo);
        $permanentsGuestsEntiteeTransverse = Atexo_Consultation_Guests::retrievePermanentGuestsEntiteeTransverse($criteriaVo);
        $permanentsGuestsAssocie = array_merge($permanentsGuestsMonEntitee, $permanentsGuestsEntiteeDependante, $permanentsGuestsEntiteeTransverse);

        $arrayInvites = [];
        foreach ($permanentsGuestsAssocie as $oneInvites) {
            $arrayInvites[] = $oneInvites->getId().'-'.$typeInvite;
        }

        $invites = trim($this->Page->invites->value);
        if ($invites) {
            $arrayInvitesWithServiceAssocie = explode(',', $invites);
            foreach ($arrayInvitesWithServiceAssocie as $oneInvite) {
                $arrayOneInvite = explode('-', $oneInvite);
                if (2 == count($arrayOneInvite)
                    && (!(in_array($arrayOneInvite[0].'-2', $arrayInvites) || in_array($arrayOneInvite[0].'-3', $arrayInvites)
                            || in_array($arrayOneInvite[0].'-1', $arrayInvites) || in_array($oneInvite, $arrayInvites)))) {
                    $arrayInvites[] = $arrayOneInvite[0].'-2';
                }
            }
        }

        return implode(',', $arrayInvites);
    }

    public function ajouterPermanentService($sender, $param)
    {
        $service = $this->entitee->getSelectedValue();
        $result = self::ajouterService($service, '1'); //ajoute les invités permanents du service que j'ai choisi
        $this->Page->invites->value = $result;
        $this->Page->invitePanel->render($param->NewWriter);
        self::updateGuests($sender, $param);
        if (Atexo_Config::getParameter('ADD_FAVORIS_CHANGE_SERVICE')) {
            $ListAgent = [];
            foreach ($this->RepeaterInvites->dataSource as $invite) {
                $ListAgent[] = $invite->getId();
            }
            $consultationId = $sender->getPage()->getReference();
            if (null === $consultationId) {
                $consultation = $sender->getPage()->getViewState('consultation');
                if ($consultation instanceof CommonConsultation) {
                    $consultationId = $consultation->getId();
                }
            }

            self::updateFavoris($ListAgent, $consultationId);


        }
    }

    public function ajouterService($serviceAssocie, $typeInvite)
    {
        $organisme = Atexo_CurrentUser::getCurrentOrganism();
        $criteriaVo = new Atexo_Consultation_Guests_CriteriaVo();
        $criteriaVo->setCurAgentServiceId($serviceAssocie);
        $criteriaVo->setOrgAcronym($organisme);
        if (Atexo_Module::isEnabled('OrganisationCentralisee', $organisme)) {
            $criteriaVo->setCentralizedOrganization(true);
        } else {
            $criteriaVo->setCentralizedOrganization(false);
        }
        $permanentsGuestsMonEntitee = Atexo_Consultation_Guests::retrievePermanentGuestsMonEntitee($criteriaVo);
        $permanentsGuestsEntiteeDependante = Atexo_Consultation_Guests::retrievePermanentGuestsEntiteeDependante($criteriaVo);
        $permanentsGuestsEntiteeTransverse = Atexo_Consultation_Guests::retrievePermanentGuestsEntiteeTransverse($criteriaVo);
        $allGuests = array_merge($permanentsGuestsMonEntitee, $permanentsGuestsEntiteeDependante, $permanentsGuestsEntiteeTransverse);

        if (!Atexo_CurrentUser::hasHabilitation('InvitePermanentMonEntite') || !Atexo_CurrentUser::hasHabilitation('InvitePermanentEntiteDependante')) { // le createur n'est pas invite permanent
            $creatorGuestVo = new Atexo_Consultation_GuestVo();
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            $createorAgent = CommonAgentPeer::retrieveByPK(Atexo_CurrentUser::getId(), $connexionCom);
            $creatorGuestVo->populate($createorAgent);
            $creatorGuestVo->setPermanentGuest(0);
            $creatorGuestVo->setTypeInvitation(1);
            $allGuests[] = $creatorGuestVo;
        }

        $arrayInvites = [];
        foreach ($allGuests as $oneInvites) {
            $arrayInvites[$oneInvites->getId()] = $oneInvites->getId().'-'.$typeInvite;
        }
        if ($serviceAssocie != Atexo_CurrentUser::getIdServiceAgentConnected()) {
            $arrayInvites[Atexo_CurrentUser::getId()] = Atexo_CurrentUser::getId().'-2';
        }

        return implode(',', $arrayInvites);
    }

    public function getConsultation()
    {
        return $this->_consultation;
    }

    public function setConsultation($consultation)
    {
        $this->_consultation = $consultation;
    }

    /*
    * Permet de gérer la visibilité du lien ajouter/modifier dans la liste des invités
    */
    public function showHideAjoutModifDroitAcces()
    {
        if (!$this->reference) {
            return true;
        }
        if (!$this->_consultation) {
            $criteria = new Atexo_Consultation_CriteriaVo();
            $criteria->setcalledFromPortail(false);
            $criteria->setIdService(
                Atexo_CurrentUser::getIdServiceAgentConnected() == 0
                    ? null
                    : Atexo_CurrentUser::getIdServiceAgentConnected()
            );
            $criteria->setAcronymeOrganisme(Atexo_CurrentUser::getCurrentOrganism());
            $criteria->setIdReference($this->_reference);
            $criteria->setConnectedAgentId(Atexo_CurrentUser::getId());
            $consultation = (new Atexo_Consultation())->search($criteria);

            if (is_array($consultation) && 1 == count($consultation) && $consultation[0] instanceof CommonConsultation) {
                $consultation = array_shift($consultation);
                $this->_consultation = $consultation;
            }
        }
        if ($this->_consultation->getCurrentUserReadOnly()) {
            $return = false;
        }
        if ($this->_consultation->getStatusConsultation() == Atexo_Config::getParameter('ETAT_EN_ATTENTE')
         || $this->_consultation->getStatusConsultation() == Atexo_Config::getParameter('STATUS_ELABORATION')
        ) {
            $return = Atexo_CurrentUser::hasHabilitation('ModifierConsultationAvantValidation');
        } else {
            $return = $this->_consultation->hasHabilitationModifierApresValidation();
        }

        return $return;
    }

    public function saveHistoriqueListeInvites($consultation, $nbreInitialInvites, $nbreFinalInvites, $listeInvites, $modification = true)
    {
        $arrayParameters = [];
        // debut : Historique de la liste des invités
        if ($nbreInitialInvites != $nbreFinalInvites || (!$modification && $nbreInitialInvites == $nbreFinalInvites)) {
            $nomElement = Atexo_Config::getParameter('ID_HISTORIQUE_LISTES_INVITES');
            $valeur = '1';
            $detail1 = '';
            foreach ($listeInvites as $invite) {
                $detail1 .= $invite->getId().'_';
                if ($invite->getPermanentGuest()) {
                    $detail1 .= '1,';
                } else {
                    $detail1 .= '0,';
                }
            }
            if ($modification) {
                $statut = Atexo_Config::getParameter('MODIFICATION_FILE');
            } else {
                $statut = Atexo_Config::getParameter('CREATION_FILE');
            }
            $detail2 = '';
            $lot = '0';
            $org = Atexo_CurrentUser::getCurrentOrganism();
            //Sauvegarde
            $arrayParameters[0] = $consultation->getId();
            $arrayParameters[1] = $nomElement;
            $arrayParameters[2] = $valeur;
            $arrayParameters[3] = $detail1;
            $arrayParameters[4] = $detail2;
            $arrayParameters[5] = $lot;
            $arrayParameters[6] = $org;
            $arrayParameters[7] = $statut;
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            (new Atexo_Consultation())->saveHistoriqueConsultation($arrayParameters, $connexion);
        }
        // fin : Historique de la liste des invités
    }

    public function chargerComposant()
    {
        $this->displayGuests();
        if ($this->_reference) {
            $criteria = new Atexo_Consultation_CriteriaVo();
            $criteria->setcalledFromPortail(false);
            $criteria->setIdService(Atexo_CurrentUser::getCurrentServiceId());
            $criteria->setAcronymeOrganisme(Atexo_CurrentUser::getCurrentOrganism());
            $criteria->setIdReference($this->_reference);
            $criteria->setConnectedAgentId(Atexo_CurrentUser::getId());
            $consultation = (new Atexo_Consultation())->search($criteria);

            if (is_array($consultation) && 1 == count($consultation) && $consultation[0] instanceof CommonConsultation) {
                $consultation = array_shift($consultation);
                $this->_consultation = $consultation;
            }
        }
    }

    public function updateFavoris($listInvite, $consultation)
    {
        $connexionCom = Propel::getConnection(
            Atexo_Config::getParameter('COMMON_DB')
            . Atexo_Config::getParameter('CONST_READ_WRITE')
        );
        $favoris = new CommonConsultationFavorisQuery();
        $resultConsultationFavoris = $favoris->findByIdConsultation($consultation);
        $agentFavoris = [];
        foreach ($resultConsultationFavoris as $consultationFavoris) {
            $agentFavoris[] = $consultationFavoris->getIdAgent();
        }
        foreach ($listInvite as $invite) {
            if (in_array($invite, $agentFavoris)) {
                continue;
            }
            $addConsultationFavorisInvitePonctuel = new CommonConsultationFavoris();
            $addConsultationFavorisInvitePonctuel->setIdAgent($invite);
            $addConsultationFavorisInvitePonctuel->setIdConsultation($consultation);
            $addConsultationFavorisInvitePonctuel->save($connexionCom);
        }
    }

    private function saveConsultationFavorisInvitePonctuel(
        int $idConsultation,
        string $organismeAcronyme,
        $connexion
    ): void {
        try {
            /** @var ConsultationService $consultationService */
            $consultationService = Atexo_Util::getSfService(ConsultationService::class);
            $agentsToAdd = [];
            $agentsToAdd = $consultationService->addFavorisInvitePonctuel(
                $idConsultation,
                $organismeAcronyme,
                $agentsToAdd
            );

            foreach ($agentsToAdd as $agent) {
                $consultationFavorisQuery = new CommonConsultationFavorisQuery();
                $consultationFavorisExist = $consultationFavorisQuery
                    ->findOneByArray(['idAgent' => $agent->getId(), 'idConsultation' => $idConsultation], $connexion);
                if (!$consultationFavorisExist instanceof CommonConsultationFavoris) {
                    if ('1' === $agent->getAlerteConsultationsInvitePonctuel()) {
                        $this->saveConsultationFavorisParCreateur($agent->getId(), $idConsultation, $connexion);
                    }
                }
            }
        } catch (\Exception $exception) {
            $logger = Atexo_LoggerManager::getLogger('consultation');
            $logger->error(
                sprintf(
                    "Problème lors de l'ajout en favoris des invités ponctuels de la consultation %s : %s.",
                    $idConsultation,
                    $exception->__toString()
                )
            );
        }
    }

    protected function saveConsultationFavorisParCreateur($idAgent, $reference, $connexion)
    {
        $consultationFavoris = new CommonConsultationFavoris();
        $consultationFavoris->setIdAgent($idAgent);
        $consultationFavoris->setIdConsultation($reference);
        $consultationFavoris->save($connexion);
    }
}
