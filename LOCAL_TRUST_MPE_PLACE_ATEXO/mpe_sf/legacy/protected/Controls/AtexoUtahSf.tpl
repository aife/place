<com:TPanel id="panelAccesUtah" cssClass="help-bar" visible="<%= (Application\Service\Atexo\Atexo_Config::getParameter('UTILISER_FAQ')||Application\Service\Atexo\Atexo_Config::getParameter('UTILISER_UTAH')) && ( Application\Service\Atexo\Atexo_Config::getParameter('UTILISER_UTAH_MODE_DECONNECTER') || $this->isConnected()) && !$this->isAdmin()%>" >
    <!-- Debut recuperation java version -->
    <com:THiddenField id="javaVersion"/>
    <script type="text/javascript" src="<%= Application\Service\Atexo\Atexo_Config::getParameter('PF_URL') %>ressources/java/deployJava.js"></script>
    <script type="text/javascript">
        document.getElementById("<%=$this->javaVersion->ClientId%>").value = deployJava.getJREs();
        function goToUtah(){
            var jv = deployJava.getJREs();
            var url = "<%= Application\Service\Atexo\Atexo_Config::getParameter('PF_URL_MPE')%>?<%=$this->request->getUrl()->getQuery()%>&Utah=OK&calledFrom=<%=$this->getCalledFrom()%>&javaVersion="+jv;
            window.open(url, '_blank');
            return false;
        }
    </script>
    <!-- Fin recuperation java version -->
    <!-- Debut message erreur acces UTAH -->
    <com:PanelMessageErreur ID="messageErreur"/>
    <!-- Fin message erreur acces UTAH -->

    <!-- Debut acces UTAH -->
    <ul>
        <li>
            <a href="#" onclick="goToUtah()" Class="assistance">
                <span class="text">
					<%=Prado::localize('DEFINE_ASSISTANCE')%>
			    </span>

            </a>
        </li>
    </ul>
    <!-- Fin acces UTAH -->
</com:TPanel>
