<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_MpeSf;
use Application\Service\Atexo\Config\Atexo_Config_Exception;
use Prado\Prado;

/**
 * Created by PhpStorm.
 * User: AsoAtexo
 * Date: 11/08/2015
 * Time: 12:04.
 */
class AtexoSearchCotraitant extends MpeTTemplateControl
{
    public function initComposant()
    {
        $arrayOption = ['2' => Prado::localize('TEXT_INDIFFERENT'),
                            '1' => Prado::localize('DEFINE_OUI'),
                            '0' => Prado::localize('DEFINE_NON'), ];
        $this->clauseSociales->DataSource = $arrayOption;
        $this->clauseSociales->DataBind();
        $this->EseAdapte->DataSource = $arrayOption;
        $this->EseAdapte->DataBind();
    }

    public function clear($sender, $param)
    {
        $this->keywordSearch->text = '';
        $this->collaborateurMandataire->checked = true;
        $this->collaborateurCotraitantSolidaire->checked = true;
        $this->collaborateurCotraitantConjoint->checked = true;
        $this->collaborateurSoustraitant->checked = true;
        $this->clauseSociales->selectedValue = 2;
        $this->EseAdapte->selectedValue = 2;
    }

    /**
     * Permet de raffraichir le bloc de recherche.
     *
     * @param $sender
     * @param $param
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function refreshBlocSearch($sender, $param)
    {
        $this->Page->blocSearchCotraitant->render($param->getNewWriter());
    }

    /**
     * @param $consultationId
     * @param $orgAcronyme
     *
     * @return string
     *
     * @throws Atexo_Config_Exception
     */
    public function getUrlConsultation($consultationId, $orgAcronyme)
    {
        return Atexo_MpeSf::getUrlDetailsConsultation($consultationId, $orgAcronyme);
    }

    public function getCriteresCotraitant()
    {
        $criteres = [
            'refCons' => $_GET['id'],
            'mandataire' => '0',
            'solidaire' => '0',
            'conjoint' => '0',
            'sousTraitant' => '0',
            'clauseSocial' => '0',
            'eseAdaptee' => '0',
            'limit' => '10',
            'offset' => '0',
            'keyWord' => '',
        ];
        $criteres['keyWord'] = $this->keywordSearch->Text;
        if (true == $this->collaborateurMandataire->checked) {
            $criteres['mandataire'] = '1';
        }
        if (true == $this->collaborateurCotraitantSolidaire->checked) {
            $criteres['solidaire'] = '1';
        }
        if (true == $this->collaborateurCotraitantConjoint->checked) {
            $criteres['conjoint'] = '1';
        }
        if (true == $this->collaborateurSoustraitant->checked) {
            $criteres['sousTraitant'] = '1';
        }
        $criteres['clauseSocial'] = $this->clauseSociales->selectedValue;
        $criteres['eseAdaptee'] = $this->EseAdapte->selectedValue;
        $this->setViewState('critereForSearch', $criteres);

        return $criteres;
    }

    /**
     * @return mixed
     */
    public function getCriteresSearch()
    {
        return $this->criteresSearch;
    }

    /**
     * @param mixed $criteresSearch
     */
    public function setCriteresSearch($criteresSearch)
    {
        $this->criteresSearch = $criteresSearch;
    }
}
