<?php

namespace Application\Controls;

use Application\Service\Atexo\Entreprise\Atexo_Entreprise_EntrepriseVo;

/**
 * permet de gérer les infos du candidat.
 *
 * @author ASO <ayoub.souidahmed@atexo.com>
 *
 * @version 1.0
 *
 * @since 4.10.0
 *
 * @copyright Atexo 2015
 */
class AtexoInfosCandidat extends MpeTTemplateControl
{
    private ?int $candidat = null;

    /**
     * recupere la valeur du [candidat].
     *
     * @return int la valeur courante [candidat]
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.10.0
     *
     * @copyright Atexo 2015
     */
    public function getCandidat()
    {
        return $this->candidat;
    }

    /**
     * modifie la valeur de [candidat].
     *
     * @param int $candidat la valeur a mettre
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.10.0
     *
     * @copyright Atexo 2015
     */
    public function setCandidat($candidat)
    {
        return $this->candidat = $candidat;
    }

    public function initComposant()
    {
        $entreprise = $this->getCandidat();
        if ($entreprise instanceof Atexo_Entreprise_EntrepriseVo) {
            $mandataires = (array) $entreprise->getMandataires();
            $this->repeaterMandataire->DataSource = $mandataires;
            $this->repeaterMandataire->DataBind();
        }
    }
}
