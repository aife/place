<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_InscritOperationsTracker;
use Application\Service\Atexo\Atexo_Util;

/*
 * commentaires
 * @author khadija Chouika <khadija.chouika@atexo.com>
 * @copyright Atexo 2013
 * @version 0
 * @since MPE-4.4
 * @package Controls
 */
class TableauTraceInscrit extends MpeTTemplateControl
{
    public $nbrElementRepeater;

    public function getNbrElementRepeater()
    {
        return $this->nbrElementRepeater;
    }

    public function setNbrElementRepeater($nbrElement)
    {
        return $this->nbrElementRepeater = $nbrElement;
    }

    public function fillFitresEntreprises()
    {
        $entreprises = (new Atexo_InscritOperationsTracker())->retrieveEntrepriseTraceOperationsInscritByRefConsultation(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_CurrentUser::getCurrentOrganism());
        $this->filtresEntreprise->DataSource = $entreprises;
        $this->filtresEntreprise->DataBind();
        $dates = (new Atexo_InscritOperationsTracker())->retrieveDateTraceOperationsInscritByRefConsultation(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_CurrentUser::getCurrentOrganism());
        $this->filtresDate->DataSource = $dates;
        $this->filtresDate->DataBind();
        $this->fillReapterHistorique();
    }

    /*
     * Permet remplir le repeater de historique
     */
    public function fillReapterHistorique()
    {
        $entreprise = null;
        $date = null;
        if (isset($_GET['id'])) {
            if (0 != $this->filtresEntreprise->selectedValue) {
                $entreprise = Atexo_Util::frnDate2iso($this->filtresEntreprise->selectedValue);
            }
            if (0 != $this->filtresDate->selectedValue) {
                $date = Atexo_Util::frnDate2iso($this->filtresDate->selectedItem->Text);
            }
            $allListe = (new Atexo_InscritOperationsTracker())->retrieveTraceByRefConsultation(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_CurrentUser::getCurrentOrganism(), $entreprise, $date);
            $this->setViewState('AllTraceInscrits', $allListe);
            $nombreElement = is_countable($allListe) ? count($allListe) : 0;
            $this->setViewState('nbrElements', $nombreElement);
            $this->historiquesInscrtis->setVirtualItemCount($nombreElement);
            $this->historiquesInscrtis->setCurrentPageIndex(0);
            self::updatePagination($this->nbResultsTop->getSelectedValue(), 1);
            $this->populateData();
            $this->panelhistoriquesInscrits->setStyle('display:block');
        }
    }

    /*
     * Permet lancer la recherche l'historique des inscrits
     */
    public function searchHistoireInscrits($sender, $param)
    {
        $this->fillReapterHistorique();
        $this->panelhistoriquesInscrits->render($param->NewWriter);
    }

    /*
     * L'evenement qui se lance au changement de la nombre de ligne souhaite par page
     */
    public function pageChanged($sender, $param)
    {
        $this->historiquesInscrtis->CurrentPageIndex = $param->NewPageIndex;
        $this->setNbrElementRepeater($this->getViewState('nbrElements'));
        self::updatePagination($this->nbResultsTop->getSelectedValue(), $param->NewPageIndex + 1);
        $this->setSensTRi(true);
        if ($this->getViewState('orderBy')) {
            $this->populateData($this->getViewState('orderBy'), $this->getViewState('triInscrtis'));
        } else {
            $this->populateData();
        }
    }

    /*
     * Permet de recuperer le sens tri
     */
    public function setSensTRi($noChange = false)
    {
        if ('DESC' == $this->getViewState('triInscrtis') && $noChange) {
            $this->setViewState('triInscrtis', 'DESC');
        } elseif ('DESC' == $this->getViewState('triInscrtis')) {
            $this->setViewState('triInscrtis', 'ASC');
        } elseif ('ASC' == $this->getViewState('triInscrtis') && $noChange) {
            $this->setViewState('triInscrtis', 'ASC');
        } elseif ('ASC' == $this->getViewState('triInscrtis')) {
            $this->setViewState('triInscrtis', 'DESC');
        } else {
            $this->setViewState('triInscrtis', 'ASC');
        }
    }

    /*
     * Permet
     */
    public function changePagerLenght($sender, $param)
    {
        $pageSize = null;
        switch ($sender->ID) {
            case 'nbResultsBottom':
                $pageSize = $this->nbResultsBottom->getSelectedValue();
                $this->nbResultsTop->setSelectedValue($pageSize);
                break;
            case 'nbResultsTop':
                $pageSize = $this->nbResultsTop->getSelectedValue();
                $this->nbResultsBottom->setSelectedValue($pageSize);
                break;
        }
        $this->setViewState('listeHitoriqueTriee', '');
        self::updatePagination($pageSize, $this->pageNumberTop->Text);
        $this->setSensTRi(true);
        if ($this->getViewState('orderBy')) {
            $this->populateData($this->getViewState('orderBy'), $this->getViewState('triInscrtis'));
        } else {
            $this->populateData();
        }
    }

    /*
     * Permet de mettre à jour la pagination
     */
    public function updatePagination($pageSize, $numPage)
    {
        $nombreElement = $this->getViewState('nbrElements');
        $this->historiquesInscrtis->PageSize = $pageSize;
        $this->nombrePageTop->Text = ceil($nombreElement / $this->historiquesInscrtis->PageSize);
        $this->nombrePageBottom->Text = ceil($nombreElement / $this->historiquesInscrtis->PageSize);
        if ($numPage > $this->nombrePageTop->Text) {
            $numPage = $this->nombrePageTop->Text;
        }
        $this->historiquesInscrtis->setCurrentPageIndex($numPage - 1);
        $this->pageNumberBottom->Text = $numPage;
        $this->pageNumberTop->Text = $numPage;
        $this->setNbrElementRepeater($this->getViewState('nbrElements'));
    }

    public function sortTraces($sender, $param)
    {
        $colum = '';
        self::updatePagination($this->nbResultsTop->getSelectedValue(), $this->historiquesInscrtis->getCurrentPageIndex() + 1);
        $this->setSensTRi();
        if (!$this->getViewState('triInscrtis')) {
            $this->setViewState('triInscrtis', 'ASC');
        }
        if ('entreprise' == $sender->getActiveControl()->CallbackParameter) {
            $colum = 'e.nom';
            $this->setViewState('orderBy', $colum);
        }
        if ('nom' == $sender->getActiveControl()->CallbackParameter) {
            $colum = 'nom_inscrit';
            $this->setViewState('orderBy', $colum);
        }
        if ('adress_ip' == $sender->getActiveControl()->CallbackParameter) {
            $colum = 'ip';
            $this->setViewState('orderBy', $colum);
        }
        if ('date' == $sender->getActiveControl()->CallbackParameter) {
            $colum = 'date';
            $this->setViewState('orderBy', $colum);
        }
        $this->populateData($colum, $this->getViewState('triInscrtis'));

        $this->panelhistoriquesInscrits->render($param->NewWriter);
    }

    /*
     * Permet d'afficher des compostants
     */
    public function showComponent()
    {
        $this->PagerTop->visible = true;
        $this->PagerBottom->visible = true;
        $this->pageNumberBottom->visible = true;
        $this->pageNumberTop->visible = true;
        $this->labelSlashBottom->visible = true;
        $this->labelSlashTop->visible = true;
        $this->nombrePageTop->visible = true;
        $this->nombrePageBottom->visible = true;
        $this->nbResultsTop->visible = true;
        $this->labelAfficherTop->visible = true;
        $this->labelAfficherDown->visible = true;
        $this->nbResultsBottom->visible = true;
        $this->resultParPageDown->visible = true;
        $this->resultParPageTop->visible = true;
        $this->panelBouttonGotoPageBottom->visible = true;
        $this->panelBouttonGotoPageTop->visible = true;
    }

    /*
     * Permet de maquer des compostants
     */
    public function hideComponent()
    {
        $this->PagerTop->visible = false;
        $this->PagerBottom->visible = false;
        $this->pageNumberBottom->visible = false;
        $this->pageNumberTop->visible = false;
        $this->labelSlashBottom->visible = false;
        $this->labelSlashTop->visible = false;
        $this->nombrePageTop->visible = false;
        $this->nombrePageBottom->visible = false;
        $this->nbResultsTop->visible = false;
        $this->labelAfficherTop->visible = false;
        $this->labelAfficherDown->visible = false;
        $this->nbResultsBottom->visible = false;
        $this->resultParPageTop->visible = false;
        $this->resultParPageDown->visible = false;
        $this->panelBouttonGotoPageBottom->visible = false;
        $this->panelBouttonGotoPageTop->visible = false;
        $this->nbrElementRepeater = 0;
    }

    public function populateData($orderBy = '', $tri = '')
    {
        $consultationId = Atexo_Util::atexoHtmlEntities($_GET['id']);
        $offset = $this->historiquesInscrtis->CurrentPageIndex * $this->historiquesInscrtis->PageSize;
        $limit = $this->historiquesInscrtis->PageSize;

        if ($offset + $limit > $this->historiquesInscrtis->getVirtualItemCount()) {
            $limit = $this->historiquesInscrtis->getVirtualItemCount() - $offset;
        }
        $entreprise = '';
        $date = '';
        if (0 != $this->filtresEntreprise->selectedValue) {
            $entreprise = Atexo_Util::frnDate2iso($this->filtresEntreprise->selectedValue);
        }
        if (0 != $this->filtresDate->selectedValue) {
            $date = Atexo_Util::frnDate2iso($this->filtresDate->selectedItem->Text);
        }
        $historques = (new Atexo_InscritOperationsTracker())->retrieveTraceByRefConsultation($consultationId, Atexo_CurrentUser::getCurrentOrganism(), $entreprise, $date, $orderBy, $tri, $limit, $offset);
        $this->setViewState('listeHistoriques', $historques);
        $nombreElement = $this->getViewState('nbrElements');
        $this->setNbrElementRepeater($nombreElement);
        if (is_countable($historques) ? count($historques) : 0) {
            $this->nombrePageTop->Text = ceil($nombreElement / $this->historiquesInscrtis->PageSize);
            $this->nombrePageBottom->Text = ceil($nombreElement / $this->historiquesInscrtis->PageSize);
            $this->historiquesInscrtis->setVirtualItemCount($nombreElement);
            $this->showComponent();
            $this->historiquesInscrtis->DataSource = $historques;
            $this->historiquesInscrtis->DataBind();
        } else {
            $this->setViewState('listeHistoriques', []);
            $this->historiquesInscrtis->DataSource = [];
            $this->historiquesInscrtis->DataBind();
            $this->hideComponent();
        }
    }
}
