 <div class="form-bloc" id="seanceSummary" class="recap-seance">
	<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
	<h2><com:TTranslate>DEFINE_IDENTIFICATION_SEANCE</com:TTranslate></h2>
	<div class="content-bloc bloc-380">
		<div class="line">
			<div class="intitule-70 bold"><com:TTranslate>DATE</com:TTranslate> : </div>
			<div class="content-bloc bloc-250"><com:TLabel id="date" /></div>
		</div>
		<div class="line">
			<div class="intitule-70 bold"><com:TTranslate>DEFINE_LIEU</com:TTranslate> : </div>
			<div class="content-bloc bloc-250"><com:TLabel id="lieu" /></div>
		</div>
		<div class="line">
			<div class="intitule-70 bold"><com:TTranslate>DEFINE_SALLE</com:TTranslate> : </div>
			<div class="content-bloc bloc-250"><com:TLabel id="salle" /></div>
		</div>
	</div>
	<div class="content-bloc bloc-400">
		<div class="line">
			<div class="intitule-80 bold"><com:TTranslate>DEFINE_TEXT_COMMISSION</com:TTranslate> :</div>
			<div class="content-bloc bloc-250"><com:TLabel id="typeCommission" /></div>
		</div>
		<div class="line">
			<div class="intitule-80 bold"><com:TTranslate>DEFINE_STATUS</com:TTranslate> : </div>
			<div class="content-bloc bloc-250"><com:TImageButton id="statut"></com:TImageButton></div>
		</div>
		<div class="line">
			<div class="intitule-80 bold"><com:TTranslate>DEFINE_INVITES_SEANCE</com:TTranslate> :</div>
			<div class="content-bloc bloc-300">
				<com:TTranslate>DEFINE_AGENTS_ELUS</com:TTranslate> : <com:TLabel id="nombreAgents" />
				<br />
				<com:TTranslate>DEFINE_INTERVENANTS_EXTERNES</com:TTranslate> : <com:TLabel id="nombreIntervenantsExternes" />
				<div class="indent-10 inline">
					<com:THyperLink id="popupInvites">
						<img alt="" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-details.gif" class="inline-img" />
					</com:THyperLink>
				</div>
			</div>
		</div>
	</div>
	<div class="breaker"></div>
	<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
</div>
