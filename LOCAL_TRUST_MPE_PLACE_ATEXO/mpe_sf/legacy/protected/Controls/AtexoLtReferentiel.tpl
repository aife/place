<com:TRepeater ID="RepeaterReferentiel">
	<prop:ItemTemplate>
		<com:TActivePanel ID="panelReferentiel" CssClass="form-group form-group-sm" >
			<label class="control-label col-md-2">
				<com:TPanel CssClass="<%#($this->TemplateControl->getCssClass())%>">
					<com:TLabel Text="<%#Prado::localize($this->Data->getCodeLibelle())%>"/><com:TLabel Id="obligatoire"
								CssClass="<%#($this->Data->getObligatoire()&& $this->TemplateControl->getObligatoire()) ? 'champ-oblig':''%>"
								Style="<%#($this->Data->getObligatoire()&& $this->TemplateControl->getObligatoire()) ? '':'display:none;'%>">*</com:TLabel>
					<com:TLabel Id="infoBulle"
								CssClass="<%#($this->Data->getLibelleInfoBulle()&& $this->TemplateControl->getAfficherInfoBulle()) ? 'champ-oblig':''%>"
								Style="<%#($this->Data->getLibelleInfoBulle()&& $this->TemplateControl->getAfficherInfoBulle()) ? '':'display:none;'%>">
						<com:TImage ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif"
									Attributes.onmouseover="afficheBulle('infosLtRefrentiel_<%#$this->Data->getId()%>', this)"
									Attributes.onmouseout="cacheBulle('infosLtRefrentiel_<%#$this->Data->getId()%>')"
									CssClass="picto-info-intitule"
									Attributes.alt="Info-bulle"
									Attributes.title="Info-bulle"
						/>
					</com:TLabel>:
				</com:TPanel>
			</label>
			<div class="col-md-10">
				<com:TPanel Attributes.id="infosLtRefrentiel_<%#$this->Data->getId()%>"
							CssClass="info-bulle"
							Attributes.onmouseover="mouseOverBulle();"
							Attributes.onmouseout="mouseOutBulle();">
				</com:TPanel>
				<com:TImage Cssclass="logo-referentiel" Visible = "<%#$this->Data->getLogo() ? true : false %>" ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/<%#$this->Data->getLogo()%>" />
				<com:THiddenField id="logoRef" value="<%#$this->Data->getLogo()%>"/>
				<com:AtexoReferentiel ID="idAtexoRef" cas="<%#$this->TemplateControl->getCas($this->Data)%>" />
				<com:THiddenField id="idRef" value="<%#$this->Data->getId()%>"/>
				<com:THiddenField id="typeSearch" value="<%#$this->Data->getTypeSearch()%>"/>
				<com:THiddenField id="chemin" value="<%#$this->Data->getPathConfig()%>"/>
				<com:THiddenField id="codeLibelle" value="<%#$this->Data->getCodeLibelle()%>"/>
				<com:THiddenField id="typeRef" value="<%#$this->Data->getType()%>"/>
				<com:TRequiredFieldValidator
						Id="validatorAtexoRefPrin"
						ControlToValidate="idAtexoRef.codeRefPrinc"
						ValidationGroup="<%#($this->TemplateControl->getValidationGroup())%>"
						Display="Dynamic"
						EnableClientScript="true"
						visible="<%#($this->Data->getObligatoire()&& $this->TemplateControl->getObligatoire() && $this->TemplateControl->isVisible($this->Data)) ? 'true' :'false'%>"
						ErrorMessage="<%#Prado::localize($this->Data->getCodeLibelle())%>"
						Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
					<prop:ClientSide.OnValidationError>
						blockErreur = '<%#($this->TemplateControl->getValidationSummary())%>';
						if(document.getElementById(blockErreur))
						document.getElementById(blockErreur).style.display='';
						if(document.getElementById('ctl0_CONTENU_PAGE_buttonSave'))
						document.getElementById('ctl0_CONTENU_PAGE_buttonSave').style.display='';
						if(document.getElementById('ctl0_CONTENU_PAGE_blocBoutons'))
						document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
					</prop:ClientSide.OnValidationError>
				</com:TRequiredFieldValidator>
				<com:TRequiredFieldValidator
						Id="validatorAtexoRefSec"
						ControlToValidate="idAtexoRef.codesRefSec"
						ValidationGroup="<%#($this->TemplateControl->getValidationGroup())%>"
						Display="Dynamic"
						EnableClientScript="true"
						visible="<%#($this->Data->getObligatoire()&& $this->TemplateControl->getObligatoire() && !$this->TemplateControl->isVisible($this->Data)) ? 'true' :'false'%>"
						ErrorMessage="<%#Prado::localize($this->Data->getCodeLibelle())%>"
						Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
					<prop:ClientSide.OnValidationError>
						blockErreur = '<%#($this->TemplateControl->getValidationSummary())%>';
						if(document.getElementById(blockErreur))
						document.getElementById(blockErreur).style.display='';
						if(document.getElementById('ctl0_CONTENU_PAGE_buttonSave'))
						document.getElementById('ctl0_CONTENU_PAGE_buttonSave').style.display='';
						if(document.getElementById('ctl0_CONTENU_PAGE_blocBoutons'))
						document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
					</prop:ClientSide.OnValidationError>
				</com:TRequiredFieldValidator>
			</div>
		</com:TActivePanel>
	</prop:ItemTemplate>
</com:TRepeater>
