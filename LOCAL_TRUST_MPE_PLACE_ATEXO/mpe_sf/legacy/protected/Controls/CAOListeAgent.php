<?php

namespace Application\Controls;

use Application\Propel\Mpe\CommonTCAOCommissionAgent;
use Application\Propel\Mpe\CommonTCAOSeanceAgent;
use Application\Propel\Mpe\CommonTCAOSeanceInvite;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_ValeursReferentielles;
use Application\Service\Atexo\Commission\Atexo_Commission_Agent;
use Application\Service\Atexo\Commission\Atexo_Commission_IntervenantExterne;
use Prado\Prado;

/**
 * composant d'affichage des agents du module CAO.
 *
 * @author Amal EL BEKKAOUI <amal.elbekkaoui@atexo.com>
 * @copyright Atexo 2012
 *
 * @since MPE4
 */
class CAOListeAgent extends MpeTTemplateControl
{
    private bool $organisation = false;
    private bool $serviceIntervenant = false;
    private bool $fonction = false;
    private bool $typeCompte = false;
    private bool $pictoTypeCompte = false;
    private bool $actionModifier = false;
    private bool $actionSupprimer = false;
    private bool $actions = true;
    private bool $checkbox = false;
    private bool $listeTypeVoix = false;
    private bool $typeVoix = false;
    //     private $typeVoixCommission = false;
    //     private $typeVoixOrdreJour = false;
    /* typeRecherche :
     * 1:liste intervenants externes;
     * 2:liste Agents
     * 3:liste agents commission
     */
    private string $typeRecherche = '1';
    private string $typeAgent = '1';

    public function getOrganisation()
    {
        return $this->organisation;
    }

    public function getServiceIntervenant()
    {
        return $this->serviceIntervenant;
    }

    public function getFonction()
    {
        return $this->fonction;
    }

    public function getTypeCompte()
    {
        return $this->typeCompte;
    }

    public function getPictoTypeCompte()
    {
        return $this->pictoTypeCompte;
    }

    public function getActionModifier()
    {
        return $this->actionModifier;
    }

    public function getActionSupprimer()
    {
        return $this->actionSupprimer;
    }

    public function getActions()
    {
        return $this->actions;
    }

    public function getCheckbox()
    {
        return $this->checkbox;
    }

    public function getTypeRecherche()
    {
        return $this->typeRecherche;
    }

    public function getListeTypeVoix()
    {
        return $this->listeTypeVoix;
    }

    public function getTypeVoix()
    {
        return $this->typeVoix;
    }

    /*public function getTypeVoixCommission() {
        return $this->typeVoixCommission;
    }
    public function getTypeVoixOrdreJour() {
        return $this->typeVoixOrdreJour;
    }*/
    public function getTypeAgent()
    {
        return $this->typeAgent;
    }

    public function setTypeRecherche($x)
    {
        $this->typeRecherche = $x;
    }

    public function setOrganisation($x)
    {
        $this->organisation = $x;
    }

    public function setServiceIntervenant($x)
    {
        $this->serviceIntervenant = $x;
    }

    public function setFonction($x)
    {
        $this->fonction = $x;
    }

    public function setTypeCompte($x)
    {
        $this->typeCompte = $x;
    }

    public function setPictoTypeCompte($x)
    {
        $this->pictoTypeCompte = $x;
    }

    public function setActionModifier($x)
    {
        $this->actionModifier = $x;
    }

    public function setActionSupprimer($x)
    {
        $this->actionSupprimer = $x;
    }

    public function setActions($x)
    {
        $this->actions = $x;
    }

    public function setCheckbox($x)
    {
        $this->checkbox = $x;
    }

    public function setListeTypeVoix($x)
    {
        $this->listeTypeVoix = $x;
    }

    public function setTypeVoix($x)
    {
        $this->typeVoix = $x;
    }

    /*public function setTypeVoixCommission($x) {
        $this->typeVoixCommission = $x;
    }
    public function setTypeVoixOrdreJour($x) {
        $this->typeVoixOrdreJour = $x;
    }*/
    public function setTypeAgent($x)
    {
        $this->typeAgent = $x;
    }

    public function fillRepeaterWithDataForSearchResult($criteriaVo)
    {
        $nombreElement = self::search($criteriaVo, true);

        if ($nombreElement >= 1) {
            $this->panelMoreThanOneElementFound->setVisible(true);
            $this->panelNoElementFound->setVisible(false);
            $this->nombreElement->Text = $nombreElement;
            $this->PagerBottom->setVisible(true);
            $this->PagerTop->setVisible(true);
            $this->setViewState('nombreElement', $nombreElement);
            $this->nombrePageTop->Text = ceil($nombreElement / $this->tableauAgent->PageSize);
            $this->nombrePageBottom->Text = ceil($nombreElement / $this->tableauAgent->PageSize);
            $this->tableauAgent->setVirtualItemCount($nombreElement);
            $this->tableauAgent->setCurrentPageIndex(0);
            $this->populateData($criteriaVo);
        } else {
            $this->panelMoreThanOneElementFound->setVisible(false);
            $this->panelNoElementFound->setVisible(true);
            $this->PagerBottom->setVisible(false);
            $this->PagerTop->setVisible(false);
        }
    }

    public function populateData($criteriaVo)
    {
        $offset = $this->tableauAgent->CurrentPageIndex * $this->tableauAgent->PageSize;
        $limit = $this->tableauAgent->PageSize;
        if ($offset + $limit > $this->tableauAgent->getVirtualItemCount()) {
            $limit = $this->tableauAgent->getVirtualItemCount() - $offset;
        }
        $criteriaVo->setOffset($offset);
        $criteriaVo->setLimit($limit);
        $arrayIntervenant = self::search($criteriaVo);
        $this->setViewState('elements', $arrayIntervenant);
        $this->tableauAgent->DataSource = $arrayIntervenant;
        $this->tableauAgent->DataBind();
    }

    public function pageChanged($sender, $param)
    {
        $this->tableauAgent->CurrentPageIndex = $param->NewPageIndex;
        $this->numPageBottom->Text = $param->NewPageIndex + 1;
        $this->numPageTop->Text = $param->NewPageIndex + 1;
        $criteriaVo = $this->getViewState('CriteriaVo');
        $this->populateData($criteriaVo);
    }

    public function elementSortedWith($panelName, $openCloseTag)
    {
        if ($this->getViewState('sortByElement') == $panelName) {
            if ('open' == $openCloseTag) {
                return '<strong>';
            } else {
                return '</strong>';
            }
        } else {
            return '';
        }
    }

    public function goToPage($sender, $param)
    {
        $numPage = null;
        switch ($sender->ID) {
            case 'DefaultButtonTop':
                 $numPage = $this->numPageTop->Text;
                break;
            case 'DefaultButtonBottom':
                 $numPage = $this->numPageBottom->Text;
                break;
        }
        if (Atexo_Util::isEntier($numPage)) {
            if ($numPage >= $this->nombrePageTop->Text) {
                $numPage = $this->nombrePageTop->Text;
            } elseif ($numPage <= 0) {
                $numPage = 1;
            }
            $this->tableauAgent->CurrentPageIndex = $numPage - 1;
            $this->numPageBottom->Text = $numPage;
            $this->numPageTop->Text = $numPage;
            $criteriaVo = $this->getViewState('CriteriaVo');
            $this->populateData($criteriaVo);
        } else {
            $this->numPageTop->Text = $this->tableauAgent->CurrentPageIndex + 1;
            $this->numPageBottom->Text = $this->tableauAgent->CurrentPageIndex + 1;
        }
    }

    public function changePagerLenght($sender, $param)
    {
        switch ($sender->ID) {
            case 'nombreResultatAfficherBottom':
                 $pageSize = $this->nombreResultatAfficherBottom->getSelectedValue();
                                                  $this->nombreResultatAfficherTop->setSelectedValue($pageSize);
                break;
            case 'nombreResultatAfficherTop':
                 $pageSize = $this->nombreResultatAfficherTop->getSelectedValue();
                                               $this->nombreResultatAfficherBottom->setSelectedValue($pageSize);
                break;
        }

        $this->tableauAgent->PageSize = $pageSize;
        $nombreElement = $this->getViewState('nombreElement');
        $this->nombrePageTop->Text = ceil($nombreElement / $this->tableauAgent->PageSize);
        $this->nombrePageBottom->Text = ceil($nombreElement / $this->tableauAgent->PageSize);
        $criteriaVo = $this->getViewState('CriteriaVo');
        $this->tableauAgent->setCurrentPageIndex(0);
        $this->populateData($criteriaVo);
    }

    public function search($c, $nbreElement = false)
    {
        $this->setViewState('CriteriaVo', $c);
        if (3 == $this->typeRecherche) {
            return (new Atexo_Agent())->getAgentsForCommission($c, $nbreElement);
        }

        return [];
    }

    public function getLibelleTypeVoix($id)
    {
        $typeVoix = $this->getViewState('typeVoix', []);
        if (!(is_array($typeVoix) && count($typeVoix) > 0)) {
            $typeVoix = (new Atexo_ValeursReferentielles())->getValeurRefByIdRef(Atexo_Config::getParameter('ID_REFERENTIEL_TYPE_VOIX_INTERVENANTS'));
            $this->setViewState('typeVoix', $typeVoix);
        }

        return $typeVoix[$id];
    }

    public function getTypeVoixAgent($id, $returnIdTypeVoix = false)
    {
        $idTypeVoix = null;
        if (1 == $this->getTypeAgent()) {
            $idTypeVoix = $this->getTypeVoixCommissionAgent($id);
        } elseif (2 == $this->getTypeAgent()) {
            $idTypeVoix = $this->getTypeVoixSeanceAgent($id);
        } elseif (3 == $this->getTypeAgent()) {
            $idTypeVoix = $this->getTypeVoixOrdreDePassageAgent($id);
        }

        $_t_typeVoix = $this->getViewState('typeVoix', []);
        if (!(is_array($_t_typeVoix) && count($_t_typeVoix) > 0)) {
            $_t_typeVoix = (new Atexo_ValeursReferentielles())->getValeurRefByIdRef(Atexo_Config::getParameter('ID_REFERENTIEL_TYPE_VOIX_INTERVENANTS'));
            $this->setViewState('typeVoix', $_t_typeVoix);
        }

        if ($idTypeVoix) {
            if ($returnIdTypeVoix) {
                return $idTypeVoix;
            } else {
                return $_t_typeVoix[$idTypeVoix];
            }
        }

        //Type de voix par defaut
        $last_item = array_slice($_t_typeVoix, -1, 1, true);
        $idTypeVoix = key($last_item);

        return $idTypeVoix;
    }

    public function getTypeVoixCommissionAgent($id)
    {
        $idCommission = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id']));
        $org = Atexo_CurrentUser::getCurrentOrganism();
        $agent = (new Atexo_Commission_Agent())->getCommissionAgent($id, $idCommission, $org);

        if ($agent instanceof CommonTCAOCommissionAgent) {
            return $agent->getIdRefValTypeVoixDefaut();
        }
    }

    public function getTypeVoixSeanceAgent($id)
    {
        $idSeance = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id']));
        $org = Atexo_CurrentUser::getCurrentOrganism();
        $agent = (new Atexo_Commission_Agent())->getSeanceAgent($id, $idSeance, $org);
        if ($agent instanceof CommonTCAOSeanceAgent) {
            return $agent->getIdRefValTypeVoix();
        }
    }

    public function getTypeVoixOrdreDePassageAgent($id)
    {
        //Recuperation des parametres d'interet
        $idOrdreDePassage = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id']));
        $org = Atexo_CurrentUser::getCurrentOrganism();

        //Recuperation de l'invite
        $invite = (new Atexo_Commission_IntervenantExterne())->getInviteOrdreDePassage($id, $idOrdreDePassage, $org, true);

        if ($invite instanceof CommonTCAOSeanceInvite) {
            return $invite->getIdRefValTypeVoix();
        }
    }

    public function actionsRepeater($sender, $param)
    {
        if ('delete' == $param->CommandName) {
            $id = $param->CommandParameter;
            $this->Page->removeIntervenant($id);
            $criteriaVo = $this->getViewState('CriteriaVo');
            $this->fillRepeaterWithDataForSearchResult($criteriaVo);
        }
    }

    public function getTypeCompteAgent($idAgent, $picto = false)
    {
        if ($picto) {
            if ($idAgent) {
                return Atexo_Config::getParameter('PICTO_COMPTE_ELU');
            } else {
                return Atexo_Config::getParameter('PICTO_COMPTE_AGENT');
            }
        } else {
            if ($idAgent) {
                return Prado::localize('TEXT_ELU');
            } else {
                return Prado::localize('TEXT_AGENT');
            }
        }
    }

    public function getListeAllTypeVoix()
    {
        $typeVoix = $this->getViewState('typeVoix', []);
        if (!(is_array($typeVoix) && count($typeVoix) > 0)) {
            $typeVoix = (new Atexo_ValeursReferentielles())->getValeurRefByIdRef(Atexo_Config::getParameter('ID_REFERENTIEL_TYPE_VOIX_INTERVENANTS'));
            $this->setViewState('typeVoix', $typeVoix);
        }

        return $typeVoix;
    }

    public function getAgents()
    {
        $data = [];

        foreach ($this->tableauAgent->Items as $item) {
            $id = $item->selectedIdAgent->value;
            $idTypeVoix = $item->selectTypeVoixAgent->getSelectedValue();
            $data[$id]['ID'] = $id;
            $data[$id]['ID_REF_VAL_TYPE_VOIX_DEFAUT'] = $idTypeVoix;
        }

        return $data;
    }
}
