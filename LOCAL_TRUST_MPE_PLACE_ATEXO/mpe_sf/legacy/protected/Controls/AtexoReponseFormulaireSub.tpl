<table summary="Liste des pièces" class="table-results tableau-reponse margin-0">
<tbody>
<tr class="on row-title-2 <%#$this->getIdHtlmToggleFormulaire()%>">
	<td class="check-col"></td>
	<td class="col-piece" colspan="2"><strong><%=$this->getNomFormulaire()%></strong></td>
	<td class="col-50 center">
		<img 
			src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/<%=$this->getStatutPictoDossier($this->getDossier())%>"
			alt="<%=$this->getStatutDossier($this->getDossier())%>" 
			title="<%=$this->getStatutDossier($this->getDossier())%>" 
			class="statut" 
			style="display:<%=(Application\Service\Atexo\Atexo_Module::isEnabled('InterfaceModuleSub'))? '':'none'%>"
		/>
	</td>
	<td class="actions">&nbsp;</td>
</tr>
</tbody>
</table>
<com:TActivePanel id="panelRefreshListeEditionsSub">
<com:TRepeater ID="listeEditionsSub" EnableViewState="true" DataKeyField="Identifiant">
	
	<prop:HeaderTemplate>
	<table summary="Liste des pièces" class="table-results tableau-reponse margin-0">
    	<tbody>
	</prop:HeaderTemplate>
    <prop:ItemTemplate>
		<tr class="<%#(($this->ItemIndex%2==0))? ' ':'on '%> <%#$this->SourceTemplateControl->getIdHtlmToggleFormulaire()%> row-piece-heritee">
			<td class="check-col">
				<com:TActiveCheckBox 
					id="idCheckBoxSelectionPieceEdition" 
					Attributes.title="<%#Prado::localize('DEFINE_SELECTIONNER_PIECE')%>"
					CssClass="check"
					visible="<%#($this->SourceTemplateControl->getVisibiliteCocherSignature($this->Data) && ($this->Page->getConsultation() && $this->Page->getConsultation()->getSignatureOffre() == 1))? true:false%>"
				/>
			</td>
			<td class="col-piece ellipsis<%#((count($this->SourceTemplateControl->listeEditionsSub->DataSource)  - 1) == $this->ItemIndex)? ' piece-last':''%>">
				<div class="blue piece-type">
						<com:TImage
						 Attributes.title="<%#Prado::localize('DEFINE_EDITER_FORMULAIRE')%>"
						 ImageUrl="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-type-form.png"
						 Visible="<%#($this->Data->getOrgineFichier() == Application\Service\Atexo\Atexo_Config::getParameter('TYPE_DOCUMENT_EDITION')) ? 'true' : 'false' %>"
						 />
						<com:TImage
						 Attributes.title="<%#Prado::localize('DEFINE_EDITER_FORMULAIRE')%>"
						 ImageUrl="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-type-libre.png"
						 Visible="<%#($this->Data->getOrgineFichier() == Application\Service\Atexo\Atexo_Config::getParameter('TYPE_DOCUMENT_PJ')) ? true : false%>"
						 />
						<abbr title="<%#$this->Data->getNomFichier()%>"><%#$this->Data->getNomFichier()%></abbr>
				</div>
			</td>
			<td colspan="2" class="col-180 ellipsis">
				<com:TPanel cssClass="statut-signature" visible='<%#($this->Page->getConsultation() && $this->Page->getConsultation()->getSignatureOffre() == 1)? true:false%>'>
					<div class="statut">
						<img 
							src="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/<%#$this->SourceTemplateControl->getPictoSignatureExiste($this->Data)%>"
							alt="<%#$this->SourceTemplateControl->getAltSignatureExiste($this->Data)%>"
							title="<%#$this->SourceTemplateControl->getAltSignatureExiste($this->Data)%>"
						/> :
						<img 
							src="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/<%#$this->SourceTemplateControl->getPictoVerifSignature($this->Data)%>"
							alt="<%#$this->SourceTemplateControl->getAltVerifSignature($this->Data)%>"
							title="<%#$this->SourceTemplateControl->getAltVerifSignature($this->Data)%>"
						/>
					</div>
					<div class="detail-statut" style="display:<%#($this->SourceTemplateControl->afficherLienSignature($this->Data))?'none':''%>">
						<%#$this->SourceTemplateControl->getInfosSignataire($this->Data)%>
					</div>
					<com:TActiveLinkButton 
						style="display:<%#($this->SourceTemplateControl->afficherLienSignature($this->Data))?'':'none'%>"
						Attributes.title="<%#Prado::localize('DEFINE_VOIR_DETAILS_SIGNATURE')%>"
						onCallBack="SourceTemplateControl.afficherDetailsSignature"
						ActiveControl.CallbackParameter="<%#$this->ItemIndex%>"
					>
						<%#$this->SourceTemplateControl->getInfosSignataire($this->Data)%>
					</com:TActiveLinkButton>
				</com:TPanel>
			</td>
			<td class="actions"></td>
		</tr>
    	</prop:ItemTemplate>
    	<prop:FooterTemplate>
    		</tbody>
    	</table>
    	</prop:FooterTemplate>
    	
	
</com:TRepeater>
</com:TActivePanel>