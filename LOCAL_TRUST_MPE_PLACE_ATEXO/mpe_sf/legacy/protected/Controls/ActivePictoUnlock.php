<?php

namespace Application\Controls;

use Prado\Prado;
use Prado\Web\UI\ActiveControls\TActiveImageButton;

/**
 * commentaires.
 *
 * @author mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class ActivePictoUnlock extends TActiveImageButton
{
    public string $_activate = 'false';

    public function onLoad($param)
    {
        /* **************************************
         * A supprimer : supprimer lors du merge *
         * **************************************
        if($this->_activate=="false") {
            $this->setImageUrl(t()."/images/picto-verouiller-inactive.gif");
            //le "return false" empeche le contrôle de faire un callback, cela peut être considéré comme un extends TImage (pas de rafraichissement)
            $this->Attributes->onclick="return false";
        } else {
            $this->setImageUrl(t()."/images/picto-verouiller.gif");
        }
        $this->setAlternateText(Prado::localize("DEFINE_VEROUILLER"));
        $this->setToolTip(Prado::localize("DEFINE_VEROUILLER"));
        */
    }

    public function getClickable()
    {
        return $this->_clickable;
    }

    public function setClickable($value)
    {
        $this->_clickable = $value;
    }

    public function getActivate()
    {
        return $this->_activate;
    }

    public function setActivate($bool)
    {
        $this->_activate = $bool;
    }
}
