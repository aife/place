<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Ref;

/**
 * permet de gérer les codes referentiels cpv.
 *
 * @author ASO <ayoub.souidahmed@atexo.com>
 *
 * @version 1.0
 *
 * @since 4.11.0
 *
 * @copyright Atexo 2015
 */
class AtexoCodesCpv extends MpeTTemplateControl
{
    public $titre = '';
    public $_small = false;
    public string $_cssBlocLibelleCPV = 'intitule col-150';
    public string $_cssBlocLibelleCPVEntreprise = 'control-label col-md-2 intitule-150';
    public bool|string $modeAffichage = false;
    public bool $codeCpvObligatoire = true;
    protected bool $infoBulleDisplayed = false;
    public $afficherCodeCpv;

    /**
     * @return string
     */
    public function getModeAffichage()
    {
        return $this->getViewState('modeAffichage');
    }

    /**
     * @param string $modeAffichage
     */
    public function setModeAffichage($modeAffichage)
    {
        $this->modeAffichage = $modeAffichage;
        $this->setViewState('modeAffichage', $modeAffichage);
    }

    /**
     * recupere la valeur du [titre].
     *
     * @return int la valeur courante [titre]
     *
     * @author ASO <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * modifie la valeur de [titre].
     *
     * @param int titre la valeur a mettre
     *
     * @return void
     *
     * @author ASO <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;
    }

    /**
     * recupere la valeur du [titre].
     *
     * @return int la valeur courante [titre]
     *
     * @author ASO <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function getSmall()
    {
        return $this->_small;
    }

    /**
     * modifie la valeur de [titre].
     *
     * @param int titre la valeur a mettre
     *
     * @return void
     *
     * @author ASO <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function setSmall($value)
    {
        $this->_small = $value;
    }

    /**
     * recupere la valeur du [titre].
     *
     * @return int la valeur courante [titre]
     *
     * @author ASO <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function getCssBlocLibelleCPV()
    {
        $callFrom = $this->Page->Master->isCalledFromCompany();

        $cssClass = $this->_cssBlocLibelleCPV;
        if ($callFrom) {
            $cssClass = $this->_cssBlocLibelleCPVEntreprise;
        }

        return $cssClass;
    }

    /**
     * modifie la valeur de [titre].
     *
     * @param int titre la valeur a mettre
     *
     * @return void
     *
     * @author ASO <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function setCssBlocLibelleCPV($value)
    {
        $this->_cssBlocLibelleCPV = $value;
    }

    /**
     * @return mixed
     */
    public function getAfficherCodeCpv()
    {
        if (!isset($this->afficherCodeCpv)) {
            $this->afficherCodeCpv = Atexo_Module::isEnabled('AffichageCodeCpv');
        }

        return $this->afficherCodeCpv;
    }

    /**
     * @param mixed $afficherCodeCpv
     */
    public function setAfficherCodeCpv($afficherCodeCpv)
    {
        $this->afficherCodeCpv = $afficherCodeCpv;
    }

    public function onLoad($param)
    {
        //Gestion de la langue pour les code CPV côté vueJs
        $lang = Atexo_Config::getParameter('AGENT_LOCALE');
        $sessionLang = Atexo_CurrentUser::readFromSession('lang');

        if (isset($sessionLang)) {
            $lang = $sessionLang;
        }
        $this->labelScriptCPVLang->Text = "<script>var currentLocal = '".$lang."';</script>";

        $callFrom = $this->Page->Master->isCalledFromCompany();
        if ($callFrom) {
            $this->cpvWarning->setVisible(false);
        }
    }

    public function setCpvParent($cpvPrincipal, $cpvSecondaire, $categorie)
    {
        $this->labelScriptCategorieParent->Text = "<script>var categorieParent = '".$categorie."';</script>";
        $newCpv1 = $newCpv2 = '';
        if ('' != $cpvPrincipal) {
            $newCpv1 = "'".$cpvPrincipal."'";
        }
        if ('' != $cpvSecondaire) {
            $newCpv2 = ",'".implode("','", explode('#', ltrim(rtrim($cpvSecondaire, '#'), '#')))."'";
        }
        $codeCpvSelected = '[ '.$newCpv1.$newCpv2.' ]';
        $this->labelScriptCpvsParent->Text = '<script>var cpvsParent = '.$codeCpvSelected.';</script>';
    }

    public function onInitComposant()
    {
        self::enabledMajCpv();
    }

    /**
     * charge les valeur cpv enregistrées.
     *
     * @param string $cpvPrincipal
     * @param string $cpvSecondaire
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function chargerReferentielCpv($cpvPrincipal, $cpvSecondaire, $load = true)
    {
        if ('lecture' == $this->modeAffichage) {
            $atexoRef = new Atexo_Ref();
            $atexoRef->setCheminFichierConfigXML(Atexo_Config::getParameter('PATH_FILE_CPV_CONFIG'));
            $atexoRef->setPrincipal($cpvPrincipal);
            $atexoRef->setSecondaires($cpvSecondaire);
            $this->afficherReferentiel($atexoRef);
        } else {
            $cpv1 = $cpv2 = '';
            if ('' != $cpvPrincipal) {
                $cpv1 = "'".$cpvPrincipal."'";
            } else {
                $this->cpvPrincipale->Value = '';
            }
            if ('' != $cpvSecondaire && $cpvSecondaire != '##') {
                $cpv2 = ",'".implode("','", explode('#', ltrim(rtrim($cpvSecondaire, '#'), '#')))."'";
            } else {
                $this->cpvSecondaires->Value = '';
            }
            $codeCpvSelected = '[ '.$cpv1.$cpv2.' ]';
            if ($load) {
                $this->labelScriptCPV->Text = '<script id="cpv-js">
                    var selected = '.$codeCpvSelected.';
                    if (typeof vueCPV != "undefined" && selected.length > 0) {
                        vueCPV.initCPVForSelected();
                    }
                    </script>';
            }
        }
    }

    public function afficherReferentiel($atexoRef)
    {
        $dataRefFile = null;
        $urlRef = null;

        $sessionLang = Atexo_CurrentUser::readFromSession('lang');

        if (isset($sessionLang)) {
            $lang = $sessionLang;
        } else {
            $lang = Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE');
        }

        if ($atexoRef instanceof Atexo_Ref) {
            //l'identifiant
            $atexoRef->setClef($this->ClientId);
            // la langue
            if ('su' == $lang) {
                $lang = 'sv';
            }
            $atexoRef->setLocale($lang);
            //fichier de données
            $arrayConfigXml = $atexoRef->getArrayConfigXml();
            if ($arrayConfigXml) {
                $dataRefFile = './'.$arrayConfigXml['cheminData'].$arrayConfigXml['fichierDonnee'].$lang.'.csv';
            }
            //code principal
            $consulationPrincipal = [];
            if ($atexoRef->getPrincipal()) {
                $consulationPrincipal[0] = $atexoRef->getPrincipal();
                $this->codeRefPrinc->value = $atexoRef->getPrincipal();
                if (is_file($dataRefFile)) {
                    $consulationPrincipal[1] = $atexoRef->getLibelleRef($atexoRef->getPrincipal(), $dataRefFile);
                }
            }
            //codes secondaires
            $consulationSecondaires = [];
            $this->codesRefSec->value = $atexoRef->getSecondaires();
            if ($atexoRef->getSecondaires()) {
                $arrayCodesCPV = explode('#', $atexoRef->getSecondaires());
                if (is_array($arrayCodesCPV) && $arrayCodesCPV) {
                    $i = 0;
                    foreach ($arrayCodesCPV as $cpv) {
                        if (null !== $cpv && false !== $cpv && '' != $cpv) {
                            $consulationSecondaires[$i][0] = $cpv;
                            if (is_file($dataRefFile)) {
                                $consulationSecondaires[$i][1] = $atexoRef->getLibelleRef($cpv, $dataRefFile);
                            }
                            ++$i;
                        }
                    }
                }
            }
            $CodesCpv = $atexoRef->retournerReferentiels($atexoRef->getClef(), $consulationPrincipal, $consulationSecondaires, 'cas4', '');
            $this->scriptRef->Text = $CodesCpv;
            //appel de la fonction retournerReferentiels
            //url Referentiel
            $urlRef = $atexoRef->getUrlReferentiel();
        }
        $this->UrlRef->setValue($urlRef);
        $this->casRef->setValue('cas4');
    }

    /**
     * retourne le code Cpv Principale.
     *
     * @return string cpvPrincipale
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function getCpvPrincipale()
    {
        if ('lecture' == $this->getModeAffichage()) {
            return $this->codeRefPrinc->getValue();
        }

        return $this->cpvPrincipale->getValue();
    }

    /**
     * retourne les codes Cpv secondaires.
     *
     * @return string cpvSecondaires
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function getCpvSecondaires()
    {
        if ('lecture' == $this->getModeAffichage()) {
            return $this->codesRefSec->value;
        }

        return $this->cpvSecondaires->value;
    }

    /**
     * pertmet de mettre dans le composant les valeurs deja selectionnees.
     *
     * @param $cpvSaved liste des code enregistre (CPV1#CPV2#CPV3#...)
     *
     * @return string cpvSecondaires
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function getSelectedValues($cpvSaved)
    {
        $codeCpvSelected = "['".implode("','", explode('#', ltrim(rtrim($cpvSaved, '#'), '#')))."']";
        $this->labelScriptCPV->Text = "<script type='text/javascript'>var selected = ".$codeCpvSelected.';</script>';
    }

    /**
     * permet de supprimer les valeurs selectionnees.
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function clearSelection()
    {
        $this->labelScriptCPV->Text = "<script type='text/javascript'>clearSelection('multiTreeContainer');</script>";
    }

    /**
     * permet de mettre à jour les valeurs selectionnees.
     *
     * @param string $cpvPrincipal
     * @param string $cpvSecondaire
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function updateSelection($cpvPrincipal = '', $cpvSecondaire = '')
    {
        $newCpv1 = $newCpv2 = '';
        if ('' != $cpvPrincipal) {
            $newCpv1 = "'".$cpvPrincipal."'";
        }
        if ('' != $cpvSecondaire) {
            $newCpv2 = ",'".implode("','", explode('#', ltrim(rtrim($cpvSecondaire, '#'), '#')))."'";
        }
        $codeCpvSelected = '[ '.$newCpv1.$newCpv2.' ]';

        $this->labelSourceScriptCPV->Text = "<script type='text/javascript'>
        var newValues = ".$codeCpvSelected.';
       </script>';
    }

    /**
     * permet de mettre à jour les valeurs selectionnees.
     *
     * @return void
     *
     * @author Khalid BENAMAR <khalid.benamar@atexo.com>
     *
     * @version 1.0
     *
     * @since roadmap-2015
     *
     * @copyright Atexo 2015
     */
    public function getCssBlocRechercheCPV()
    {
        $classeCss = '';
        if ($this->getSmall()) {
            $classeCss = ' small';
        }

        return $classeCss;
    }

    /**
     * permet de gerer le bloc CPV à afficher selon le mode (lecture seule ou ecriture).
     *
     * @param void
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function enabledMajCpv()
    {
        if (Atexo_Module::isEnabled('AffichageCodeCpv')) {
            if ('lecture' == $this->modeAffichage) {
                $this->panelModeAffichage->visible = true;
            } else {
                $this->panelModeMaj->visible = true;
            }
        }
    }

    /**
     * @return bool
     */
    public function isCodeCpvObligatoire()
    {
        return $this->codeCpvObligatoire;
    }

    /**
     * @param bool $codeCpvObligatoire
     */
    public function setCodeCpvObligatoire($codeCpvObligatoire)
    {
        $this->codeCpvObligatoire = $codeCpvObligatoire;
    }

    /**
     * @param bool $infoBulleDisplayed
     */
    public function setInfoBulleDisplayed($infoBulleDisplayed)
    {
        $this->infoBulleDisplayed = $infoBulleDisplayed;
    }

    /**
     * @return bool
     */
    public function isInfoBulleDisplayed()
    {
        return $this->infoBulleDisplayed;
    }

    /**
     * @return string
     */
    public function getIdClientImgErrorCpv()
    {
        return $this->ImgErrorCpv->getClientId();
    }
}
