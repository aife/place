<?php

namespace Application\Controls;

use App\Service\Footer\Footer;
use Application\Service\Atexo\Atexo_Util;

class FooterAgent extends MpeTPage
{
    public function getFooter()
    {
        /** @var Footer $footerService */
        $footerService = Atexo_Util::getSfService(Footer::class);

        return $footerService->getTemplate('mpe', 'agent');
    }
}
