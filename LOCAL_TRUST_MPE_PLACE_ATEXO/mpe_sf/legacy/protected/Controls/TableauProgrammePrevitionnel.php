<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_ProgrammesPrevisionnels;

/**
 * commentaires.
 *
 * @author mouslim MITALI
 * @copyright Atexo 2009
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class TableauProgrammePrevitionnel extends MpeTTemplateControl
{
    private string $_postBack = '';
    private bool $_calledFromAgent = true;
    private bool $_classification = false;
    private bool $_organisme = false;
    private bool $_serviceId = false;
    private bool $_annee = false;

    public function setPostBack($value)
    {
        $this->_postBack = $value;
    }

    public function getPostBack()
    {
        return $this->_postBack;
    }

    public function setCalledFromAgent($value)
    {
        $this->_calledFromAgent = $value;
    }

    public function getCalledFromAgent()
    {
        return $this->_calledFromAgent;
    }

    public function setClassification($value)
    {
        $this->_classification = $value;
    }

    public function getClassification()
    {
        return $this->_classification;
    }

    public function setOrganisme($value)
    {
        $this->_organisme = $value;
    }

    public function getOrganisme()
    {
        return $this->_organisme;
    }

    public function setServiceId($value)
    {
        $this->_serviceId = $value;
    }

    public function getServiceId()
    {
        return $this->_serviceId;
    }

    public function setAnnee($value)
    {
        $this->_annee = $value;
    }

    public function getAnnee()
    {
        return $this->_annee;
    }

    public function onLoad($param)
    {
        if (!$this->getPostBack()) {
            $this->displayProgrammePrevisionnel();
        }
        if ('false' == $this->getCalledFromAgent()) {
            $this->linkAdd->setVisible(false);
        }
    }

    /**
     * affiche la liste des marchés d'un organisme.
     */
    public function displayProgrammePrevisionnel()
    {
        $dataSource = [];

        $PPs = (new Atexo_ProgrammesPrevisionnels())->retreivePPs($this->getOrganisme(), $this->getServiceId(), $this->getAnnee(), false, false, $this->getCalledFromAgent(), $this->getClassification());
        $dataSource = (new Atexo_ProgrammesPrevisionnels())->addPathServicePPs($PPs);

        if (0 == (is_countable($dataSource) ? count($dataSource) : 0)) {
            $this->panelListePPs->setDisplay('None');
            $this->panelNoElementFound->setDisplay('Dynamic');
        } else {
            $this->repeaterListePPs->DataSource = $dataSource;
            $this->repeaterListePPs->DataBind();
            $this->panelListePPs->setDisplay('Dynamic');
            $this->panelNoElementFound->setDisplay('None');
        }
    }

    /**
     * Refresh du tableau des listes marchés.
     */
    public function onCallBackRefreshTableau($sender, $param)
    {
        $this->displayProgrammePrevisionnel();
        $this->panelListePPs->render($param->getNewWriter());
    }

    /**
     * Supprimer une liste marché.
     */
    public function deletePP($sender, $param)
    {
        $vars = explode('##', $param->CommandName);
        (new Atexo_ProgrammesPrevisionnels())->doDeletePP($vars[0], Atexo_CurrentUser::getOrganismAcronym());
        (new Atexo_ProgrammesPrevisionnels())->activateLastPP($this->getOrganisme(), $this->getServiceId(), $vars[1]);
    }
}
