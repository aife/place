<!--Debut partitionneur-->
<com:TPanel ID="panelNoElementFound">
	<h2><com:TTranslate>DEFINE_AUCUN_RESULTAT</com:TTranslate>&nbsp;</h2>
</com:TPanel>
<com:TPanel ID="panelMoreThanOneElementFound">
<div class="line-partitioner">
	<h2><com:TTranslate>DEFINE_NOMBRE_RESULTAT</com:TTranslate>&nbsp;<com:TLabel ID="nombreElement"/></h2>

	<div class="partitioner">
		<div class="intitule"><strong><label for="allCons_nbResultsTop"><com:TTranslate>DEFINE_TEXT_AFFICHER</com:TTranslate></label></strong></div>
		<com:TDropDownList ID="nombreResultatAfficherTop" AutoPostBack="true" onSelectedIndexChanged="changePagerLenght" Attributes.title='<%=Prado::localize("NOMBRE_DE_RESULTAT_PAR_PAGE")%>'>
			<com:TListItem Text="10" Selected="10" />
			<com:TListItem Text="20" Value="20" />
			<com:TListItem Text="50" Value="50" />
			<com:TListItem Text="100" Value="100" />
			<com:TListItem Text="500" Value="500" />
		</com:TDropDownList>
		<div class="intitule"><com:TTranslate>DEFINE_RESULTATS_PAGE</com:TTranslate></div>
			<label style="display:none;" for="allCons_pageNumberTop">Aller à la page</label>
			<com:TPanel ID="panelBouttonGotoPageTop" DefaultButton="DefaultButtonTop" CssClass="float-left">
				<com:TTextBox ID="numPageTop" Attributes.title='<%=Prado::localize("N_DE_LA_PAGE")%>' Text="1"/>
				<div class="nb-total ">/ <com:TLabel ID="nombrePageTop"/></div>
				<com:TButton ID="DefaultButtonTop"  OnClick="goToPage" Attributes.style="display:none"/>
			</com:TPanel>
		<div class="liens">
			<com:TPager ID="PagerTop"
				ControlToPaginate="ArchiveTableauBordRepeater"
				FirstPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-premier.gif' alt='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>' title='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>' />"
		   	 	LastPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-dernier.gif' alt='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>' title='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>' />"
		    	Mode="NextPrev"
				NextPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-suivant.gif' alt='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>' title='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>' />"
				PrevPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-precedent.gif' alt='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>' title='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>' />"
				OnPageIndexChanged="pageChanged"/>
		</div>
	</div>
</div>
<!--Fin partitionneur-->
<!--Debut Tableau consultations-->
<!--<com:TActivePanel id="panelRefresh">-->
<com:TRepeater ID="ArchiveTableauBordRepeater"
			   AllowPaging="true"
			   PageSize="10"
			   EnableViewState="true"
			   AllowCustomPaging="true"
			   >
<prop:HeaderTemplate>
<table summary="<%=Prado::localize('TEXT_TOUTES_CONSULTATION')%>" class="table-results">
	<caption><com:TTranslate>TEXT_TOUTES_CONSULTATION</com:TTranslate></caption>
	<thead>
		<tr>
			<th class="top" colspan="7"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></th>
		</tr>
		<tr>
			<th class="check-col" abbr="Tous/Aucun" id="selection">
				<com:TActiveCheckBox
					ID="checkAll"
					OnCallBack="SourceTemplateControl.checkAll" />

				<label style="display:none" for="selection"><com:TTranslate>DEFINE_SELECTION</com:TTranslate></label>
			</th>
			<th class="col-100" id="allCons_ref" abbr="Ref">
					<%#$this->Page->ArchiveResultSearch->elementSortedWith('reference_utilisateur','open')%>
						<com:TTranslate>TEXT_REFERENCE</com:TTranslate>
					<%#$this->Page->ArchiveResultSearch->elementSortedWith('reference_utilisateur','close')%>
					<com:PictoSort CommandName="reference_utilisateur" OnCommand="Page.Trier" />
				<br />
				<com:TTranslate>DEFINE_PROCEDURE</com:TTranslate>
				<br />
				<com:TTranslate>DEFINE_STATUS</com:TTranslate>
				<br />
				<%#$this->Page->ArchiveResultSearch->elementSortedWith('nom_createur','open')%>
					<com:TTranslate>DEFINE_AUTEUR</com:TTranslate>
				<%#$this->Page->ArchiveResultSearch->elementSortedWith('nom_createur','close')%>
				<com:PictoSort CommandName="nom_createur" OnCommand="Page.Trier" />
			</th>
			<th class="col-30" id="allCons_details" abbr="Détails">&nbsp;</th>
			<th class="col-350" id="allCons_intitule" abbr="Intitule">
				<%#$this->Page->ArchiveResultSearch->elementSortedWith('intitule','open')%>
					<com:TTranslate>DEFINE_INTITULE</com:TTranslate>
				<%#$this->Page->ArchiveResultSearch->elementSortedWith('intitule','close')%>
				<com:PictoSort CommandName="intitule" OnCommand="Page.Trier" />
				/
				<%#$this->Page->ArchiveResultSearch->elementSortedWith('objet','open')%>
					<com:TTranslate>OBJET</com:TTranslate>
				<%#$this->Page->ArchiveResultSearch->elementSortedWith('objet','close')%>
				<com:PictoSort CommandName="objet" OnCommand="Page.Trier"  />
			</th>
			<th class="<%#($this->TemplateControl->isTypeConsultation()? 'col-110':'col-100')%>" id="allCons_registres">
				<com:TLabel Visible="<%#$this->TemplateControl->isTypeConsultation()%>">
					<com:TTranslate>DEFINE_REGISTRES</com:TTranslate> :	<com:PictoRegistre />
				</com:TLabel>
			</th>
			<th class="col-100" id="allCons_dateEnd" abbr="Date limite">
				<%#$this->Page->ArchiveResultSearch->elementSortedWith('dateFin','open')%>
					<com:TTranslate>DEFINE_DATE_LIMITE_REMIS_PLIS</com:TTranslate>
				<%#$this->Page->ArchiveResultSearch->elementSortedWith('dateFin','close')%>
				<com:PictoSort CommandName="datefin" OnCommand="Page.Trier"/>
			</th>
			<th class="actions" id="allCons_actions"><com:TTranslate>DEFINE_ACTIONS</com:TTranslate></th>
		</tr>
	</thead>
</prop:HeaderTemplate>
<prop:ItemTemplate>
	<tr class="<%#(($this->ItemIndex%2==0)? '':'on')%>" >
		<td class="check-col" headers="selection" >
			<com:THiddenField ID="idArchive" Value="<%#$this->Data->getId()%>" />
			<com:TPanel Visible="<%#$this->Page->ArchiveResultSearch->getIsVisibleCheckBox($this->Data->getIdEtatConsultation())%>">
			<com:TActiveCheckBox ID="toUpdate" />
			</com:TPanel>
		</td>
		<td class="col-90" headers="allCons_ref">
			<com:TLabel ID="reference" Attributes.class="ref" Text="<%#$this->Page->ArchiveResultSearch->insertBrWhenTextTooLong($this->Data->getReferenceUtilisateur(),15)%>"/>
			<br />
			<com:TLabel ID="procedure" Text="<%#$this->Data->getAbreviationTypeProcedure()%>"/><br />
			<com:TPanel
					Visible="<%#$this->TemplateControl->isTypeConsultation()%>"
					CssClass="consult-etape<%#$this->Data->getStatusConsultation()%>sur5"
					Attributes.title="<%#$this->Page->ArchiveResultSearch->infoBulleEtapeConnsultation(($this->Data instanceof Application\Propel\Mpe\CommonConsultation)? $this->Data->getStatusConsultation():null)%>">
			</com:TPanel>
			<span class="auteur">
				<%#$this->Data->getNomCreateur()." ".$this->Data->getPrenomCreateur()%>
			</span>
		</td>
		<td class="col-30" headers="allCons_details">
			<a href="index.php?page=Agent.DetailConsultation&id=<%#$this->Data->getId()%>&archiveRappel=true"><img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-details.gif" alt="<%=Prado::localize('TEXT_VOIR_DETAIL')%>" title="<%=Prado::localize('TEXT_VOIR_DETAIL')%>" /></a>
			<com:THyperLink Visible="<%#Application\Service\Atexo\Atexo_Module::isEnabled('SuiviPassation', Application\Service\Atexo\Atexo_CurrentUser::getCurrentOrganism())%>" NavigateUrl="index.php?page=Agent.DonneesComplementairesConsultation&id=<%#$this->Data->getId()%>" CssClass="detail">
					<com:TImage ImageUrl="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-donnees-comps.gif" Attributes.alt="<%=Prado::localize('VOIR_DONNEES_COMPLEMENTAIRE')%>" ToolTip="<%=Prado::localize('VOIR_DONNEES_COMPLEMENTAIRE')%>" />
			</com:THyperLink>
		</td>
		<td class="col-350" headers="allCons_intitule">
			<div class="objet-line">
				<strong><com:TTranslate>DEFINE_INTITULE</com:TTranslate> :</strong>
				<com:TLabel ID="intitule" Text="<%#$this->Page->ArchiveResultSearch->insertBrWhenTextTooLong($this->Data->getIntituleTraduit(),65)%>"/>
			</div>
			<div class="objet-line">
				<strong><com:TTranslate>OBJET</com:TTranslate> :</strong>
				<%# Application\Service\Atexo\Atexo_Util::truncateTexte($this->Data->getObjetTraduit(),'65')%>
				<span style="<%#$this->Page->ArchiveResultSearch->isTextTruncated($this->Data->getObjetTraduit())%>" onmouseover="afficheBulle('ctl0_CONTENU_PAGE_ArchiveResultSearch_ArchiveTableauBordRepeater_ctl<%#($this->ItemIndex+1)%>_infosConsultation', this)" onmouseout="cacheBulle('ctl0_CONTENU_PAGE_ArchiveResultSearch_ArchiveTableauBordRepeater_ctl<%#($this->ItemIndex+1)%>_infosConsultation')" class="info-suite">...</span>
				<com:TPanel ID="infosConsultation" CssClass="info-bulle" ><div><%#$this->Data->getObjetTraduit()%></div></com:TPanel>
			    </div>
			</div>
			<div class="objet-line">
				<strong><com:TTranslate>DEFINE_COMMENTAIRE</com:TTranslate> :</strong>
				<com:TLabel ID="commentaire" Text="<%#$this->Page->ArchiveResultSearch->insertBrWhenTextTooLong($this->Data->getChampSuppInvisible(),65)%>"/>
			</div>
		</td>
		<td class="col-110 <%#($this->TemplateControl->isTypeAnnonce() ? 'center':'')%>" headers="allCons_registres">
			<com:TLabel Visible="<%#$this->TemplateControl->isTypeConsultation()%>">
				<div class="registre-line">
					<span class="intitule">
					 	<com:PictoRegistreRetraits Activate="true" CommandName="<%#$this->Data->getId()%>" OnCommand="Page.ResultSearch.redirectToRegistreRetrait"/>&nbsp;&nbsp;:
					 </span>
					<com:THyperLink ID="linkRegistreRetraitsConsultation" visible="true" NavigateUrl="index.php?page=Agent.GestionRegistres&id=<%#$this->Data->getId()%>&type=1">
						<com:TLabel ID="registreRetraits" Text="<%#$this->Data->getNombreTelecharegementElectronique()%> + <%#$this->Data->getNombreTelecharegementPapier()%>"/>
					</com:THyperLink>
				</div>
				<div class="registre-line">
					<span class="intitule">
						<com:PictoRegistreQuestions Activate="true" CommandName="<%#$this->Data->getId()%>" OnCommand="Page.ResultSearch.redirectToRegistreQuestion"/>&nbsp;&nbsp;:
					</span>
					<com:THyperLink ID="linkQuestionsConsultation" visible="true"  NavigateUrl="index.php?page=Agent.GestionRegistres&id=<%#$this->Data->getId()%>&type=3">
						<com:TLabel ID="questions" Text="<%#$this->Data->getNombreQuestionElectroniqueDces()%> + <%#$this->Data->getNombreQuestionPapierDces()%>"/>
					</com:THyperLink>
				</div>
				<div class="registre-line">
					<span class="intitule">
					    <com:PictoRegistreDepot Activate="true" CommandName="<%#$this->Data->getId()%>" OnCommand="Page.ResultSearch.redirectToRegistreDepot"/>&nbsp;&nbsp;:
					</span>
				    <com:THyperLink ID="linkRegistreDepot" visible="true" NavigateUrl="index.php?page=Agent.GestionRegistres&id=<%#$this->Data->getId()%>&type=5">
						<com:TLabel ID="registreDepot" Text="<%#$this->Data->getNombreOffreElectronique()%> + <%#$this->Data->getNombreOffrePapier()%>"/>
					</com:THyperLink>
				</div>
			</com:TLabel>
			<com:TLabel Visible="<%#$this->TemplateControl->isTypeAnnonce()%>">
			 	<com:TLabel Visible="<%#($this->Data->getIdTypeAvis()== Application\Service\Atexo\Atexo_Config::getParameter('TYPE_AVIS_ATTRIBUTION'))%>">
			 		<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-avis-attribution.gif" alt="<%=Prado::localize('ANNONCE_ATTRIBUTION')%>" title="<%=Prado::localize('ANNONCE_ATTRIBUTION')%>" />
			 	</com:TLabel>

			 	<com:TLabel Visible="<%#($this->Data->getIdTypeAvis()== Application\Service\Atexo\Atexo_Config::getParameter('TYPE_AVIS_INFORMATION'))%>">
			 		<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-avis-info.gif" alt="<%=Prado::localize('DEFINE_ANNONCE_INFORMATION')%>" title="<%=Prado::localize('DEFINE_ANNONCE_INFORMATION')%>" />
			 	</com:TLabel>
			</com:TLabel>
		</td>
		<td class="col-100" headers="allCons_dateEnd">
			<div class="cloture-line">
				 <com:TPanel Visible="<%#$this->Data instanceof Application\Propel\Mpe\CommonConsultation && $this->Data->getStatusConsultation()>= Application\Service\Atexo\Atexo_Config::getParameter('ETAT_CLOTURE')%>" >
			        <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-time-off.gif" alt="" />
			        <span class="time-red">
						<com:TLabel Text="<%#str_replace(' ','<br />', Application\Service\Atexo\Atexo_Util::iso2frnDateTime($this->Data->getDatefin()))%>"/>
					</span>
				</com:TPanel>
				<com:TPanel Visible="<%#$this->Data instanceof Application\Propel\Mpe\CommonConsultation && $this->Data->getStatusConsultation()< Application\Service\Atexo\Atexo_Config::getParameter('ETAT_CLOTURE')%>" >
			        <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-time-on.gif" alt="" />
			        <span class="time-green">
			        	<com:TLabel Text="<%#str_replace(' ','<br />', Application\Service\Atexo\Atexo_Util::iso2frnDateTime($this->Data->getDatefin()))%>"/>
					</span>
				</com:TPanel>
			</div>
		</td>
		<td class="actions" headers="allCons_actions">
			<com:TPanel Visible="<%#$this->Page->ArchiveResultSearch->getIsAjoutDocumentsExterneVisible($this->Data->getIdEtatConsultation())%>">
			<a href="index.php?page=Agent.ArchiveSuiviDocsExterne&id=<%#$this->Data->getId()%>&archiveRappel=true"><img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-ajout-doc.gif" alt="<%=Prado::localize('TEXT_GESTION_DOCUMENTS_EXTERNES')%>" title="<%=Prado::localize('TEXT_GESTION_DOCUMENTS_EXTERNES')%>" /></a>
			</com:TPanel>
			<com:TPanel Visible="<%#$this->Page->ArchiveResultSearch->getIsAjoutMetadonneesVisible($this->Data->getIdEtatConsultation())%>">
			<a href="index.php?page=Agent.ArchiveMetadonnee&id=<%#$this->Data->getId()%>&archiveRappel=true"><img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-metadonnees.gif" alt="<%=Prado::localize('TEXT_ENRICHIR_META_DONNEES')%>" title="<%=Prado::localize('TEXT_ENRICHIR_META_DONNEES')%>" /></a>
			</com:TPanel>
			<com:TPanel Visible="<%#$this->Page->ArchiveResultSearch->getIsVoirArborescenceVisible($this->Data->getIdEtatConsultation())%>">
			<a href="index.php?page=Agent.ArchiveArborescence&id=<%#$this->Data->getId()%>&archiveRappel=true"><img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-acces-arbo.gif" alt="<%=Prado::localize('TEXT_VISUALISER_ARBORESCENCE')%>" title="<%=Prado::localize('TEXT_VISUALISER_ARBORESCENCE')%>" /></a>
			</com:TPanel>
			<com:TPanel Visible="<%#$this->Data->isDicTelechargeable()%>">
			<a href="index.php?page=Agent.DownloadArchive&id=<%#$this->Data->getId()%>"><img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-dl-dossier.gif" alt="<%=Prado::localize('TEXT_TELECHARGER_DIC')%>" title="<%=Prado::localize('TEXT_TELECHARGER_DIC')%>" /></a>
			</com:TPanel>
			<com:TPanel Visible="<%#$this->Data->isDacTelechargeable()%>">
				<a href="index.php?page=Agent.DownloadArchive&id=<%#$this->Data->getId()%>&DAC"><img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-dl-dossier.gif" alt="<%=Prado::localize('TELECHARGER_DAC')%>" title="<%=Prado::localize('TELECHARGER_DAC')%>" /></a>
			</com:TPanel>
			<com:TPanel Visible="<%#$this->Page->ArchiveResultSearch->getIsArchiverButtonVisible($this->Data->getIdEtatConsultation())%>">
    			<com:TImageButton
    				ID="Valider"
    				ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-archiver.gif"
    				AlternateText="<%=Prado::localize('TEXT_PASSER_STATUT_A_ARCHIVER')%>"
    				Attributes.title="<%=Prado::localize('TEXT_PASSER_STATUT_A_ARCHIVER')%>"
    				Attributes.onClick="if(!confirm('<%=addslashes(Prado::localize('TEXT_AVERTISSEMENT_STATUT_A_ARCHIVER_1'))%>')) return false;"
    				OnCommand="SourceTemplateControl.changeUniqueStatut"
    				commandName="Valider"
    			/>
			</com:TPanel>

			<com:TPanel Visible="<%#((Application\Service\Atexo\Atexo_Module::isEnabled('DesarchivageConsultation')) && ($this->Data->getIdEtatConsultation() ==  Application\Service\Atexo\Atexo_Config::getParameter('STATUS_ARCHIVE_REALISEE')))? true:false%>">
    			<com:TImageButton
    				ID="desarchivageConsultation"
    				ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-desarchiver.gif"
    				AlternateText="<%=Prado::localize('TEXT_DESARCHIVER_CONSULTATION')%>"
    				Attributes.title="<%=Prado::localize('TEXT_DESARCHIVER_CONSULTATION')%>"
    				OnCommand="SourceTemplateControl.changeStatutDesarchivage"
    			/>
			</com:TPanel>
		</td>
	</tr>
</prop:ItemTemplate>
<prop:FooterTemplate>
</table>
</prop:FooterTemplate>
</com:TRepeater>
<!--</com:TActivePanel>-->
<!--Fin Tableau consultations-->
<!--Debut partitionneur-->
<div class="line-partitioner">
	<div class="partitioner">
		<div class="intitule"><strong><label for="decision_nbResultsBottom"><com:TTranslate>DEFINE_TEXT_AFFICHER</com:TTranslate></label></strong></div>
		<com:TDropDownList ID="nombreResultatAfficherBottom" AutoPostBack="true" onSelectedIndexChanged="changePagerLenght" Attributes.title='<%=Prado::localize("NOMBRE_DE_RESULTAT_PAR_PAGE")%>'>
			<com:TListItem Text="10" Selected="10"/>
			<com:TListItem Text="20" Value="20"/>
			<com:TListItem Text="50" Value="50" />
			<com:TListItem Text="100" Value="100" />
			<com:TListItem Text="500" Value="500" />
		</com:TDropDownList>
		<div class="intitule"><com:TTranslate>DEFINE_RESULTATS_PAGE</com:TTranslate></div>
		<com:TPanel ID="panelBouttonGotoPageBottom" DefaultButton="DefaultButtonBottom" CssClass="float-left">
			<com:TTextBox ID="numPageBottom" Attributes.title='<%=Prado::localize("N_DE_LA_PAGE")%>' Text="1"/>
			<div class="nb-total ">/ <com:TLabel ID="nombrePageBottom"/></div>
			<com:TButton ID="DefaultButtonBottom"  OnClick="goToPage" Attributes.style="display:none"/>
		</com:TPanel>
		<div class="liens">
			<com:TPager ID="PagerBottom"
				ControlToPaginate="ArchiveTableauBordRepeater"
				FirstPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-premier.gif' alt='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>' title='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>' />"
				LastPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-dernier.gif' alt='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>' title='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>' />"
				Mode="NextPrev"
				NextPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-suivant.gif' alt='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>' title='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>' />"
				PrevPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-precedent.gif' alt='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>' title='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>' />"
				OnPageIndexChanged="pageChanged"/>
		</div>
	</div>
</div>
<!--Fin partitionneur-->
<com:TPanel id="panelArchivage">
<com:TLinkButton
	ID="Valider"
	CssClass="bouton-archivage"
	Attributes.onClick="if(!confirm('<%=Prado::localize('TEXT_AVERTISSEMENT_STATUT_A_ARCHIVER')%>')) return false;"
	OnCommand="TemplateControl.changeStatut"
	commandName="Valider">
	<com:TTranslate>TEXT_PASSER_A_ARCHIVER</com:TTranslate>
</com:TLinkButton>
</com:TPanel>
</com:TPanel>
