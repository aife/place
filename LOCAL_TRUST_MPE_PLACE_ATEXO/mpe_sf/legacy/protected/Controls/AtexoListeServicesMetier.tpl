<!--Debut barre modules-->
<div id="barre-modules">
	<ul id="list-modules">
		<com:TRepeater ID="repeaterListeServices"  >
         	<prop:ItemTemplate>
         		<li>
         			<com:TLinkButton 
         							Attributes.target="_blank"
        							Attributes.title="<%#Prado::localize($this->Data['denomination'])%>"
        							OnCommand="SourceTemplateControl.generateUrlAccesWithSso" 
                                    CommandParameter="<%#$this->Data['url_acces'] .'#__#'.$this->Data['idServiceMetier']%>"
                                    CssClass="<%#$this->SourceTemplateControl->getCssClassServiceMetier($this->Data['idServiceMetier'])%>"
    							 >
    							 <img src="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/modules/<%# str_replace(".png", "-small.png", $this->Data['logo'])%>" alt="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/modules/<%#$this->Data['logo']%>" />
    				</com:TLinkButton>
         		</li>
         	</prop:ItemTemplate>
            </com:TRepeater>
	</ul>
</div>
<!--Fin barre modules-->