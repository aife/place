	<!--Debut Bloc Liste des marches-->
		<com:TActivePanel ID="panelListeMarches" cssClass="form-field">
			<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span>
				<span class="title">
				<com:TTranslate visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('Article133GenerationPf'))? true:false%>" >TEXT_LISTE_MARCHES_2</com:TTranslate>
				<com:TTranslate visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('Article133GenerationPf'))? false:true%>">TEXT_LISTE_MARCHES</com:TTranslate>
				</span>
			</div>
			<div class="content">
				<div class="table-bloc">
				<com:TRepeater ID="repeaterListeMarches" >
						<prop:HeaderTemplate>
						<table class="table-results" summary="<%=Prado::localize('TEXT_LISTE_MARCHES')%>">
							<caption><com:TTranslate>TEXT_LISTE_MARCHES</com:TTranslate></caption>
							<thead>
								<tr>
									<th class="top" colspan="2"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></th>
								</tr>
								<tr>
									<th class="col-450" id="nomFichier"><com:TTranslate>DEFINE_NOM_FICHIER</com:TTranslate></th>
									<th  class="actions" id="supprFichier"><com:TTranslate visible="<%#($this->SourceTemplateControl->getCalledFromAgent()) ? true : false%>p" >DEFINE_SUPPRIMER</com:TTranslate></th>
								</tr>
							</thead>
							</prop:HeaderTemplate>
							<prop:ItemTemplate>
							<tr class="<%#(($this->ItemIndex%2==0)? '':'on')%>">
								<td class="col-690" headers="nomFichier">
									<a href="index.php?page=<%#($this->SourceTemplateControl->getCalledFromAgent()==false) ? 'Entreprise':'Agent'%>.PopupListeMarchesDownloadFile&id=<%#$this->Data->getId()%>&org=<%#$this->Page->tableauListeMarches->getOrganisme()%>" >
										<%#$this->Data->getNomFichier()%>
									</a> (<%# Application\Service\Atexo\Atexo_Util::arrondirSizeFile($this->Data->getTaille()/1024)%>)</td>
								<td class="actions" headers="supprFichier">
								<com:PictoDeleteActive
								    visible="<%#($this->SourceTemplateControl->getCalledFromAgent()) ? true : false%>"
									ID="deleteButton"
        							OnCommand="Page.tableauListeMarches.deleteMarche" 
        							OnCallBack="Page.tableauListeMarches.onCallBackRefreshTableau"
        							CommandName = "<%#$this->Data->getId()%>"
        							AlternateText="<%=Prado::localize('TEXT_SUPPRIMER')%>"
        							Attributes.onclick='javascript:if(!confirm("<%=Prado::localize('TEXT_ALERTE_SUPPRESSION_MARCHE')%>"))return false;'/>
								</td>
							</tr>
						</prop:ItemTemplate>
					<prop:FooterTemplate>
					</table>
				</prop:FooterTemplate>
				</com:TRepeater>
					<com:THyperLink 
						 ID="linkAdd"
						 NavigateUrl="javascript:popUp('index.php?page=Agent.PopupAddListeMarches','yes');"
						 cssClass="ajout-el float-left" Text="<%=Prado::localize('DEFINE_AJOUTER_LISTE_MARCHES')%>" />
				</div>
				<div class="breaker"></div>
			</div>
			<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
			<span style="display:none;">
							<com:TActiveButton 
                            ID="refreshRepeater" 
                            Text="buttonRefreshPieceExternes" 
                            OnCallBack="Page.tableauListeMarches.onCallBackRefreshTableau"/>
                        </span>
		</com:TActivePanel>
		<!--Fin Bloc Liste des marches-->