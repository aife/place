<com:TConditional condition="$this->TemplateControl->getVisible()">
    <prop:trueTemplate>
        <div class="modal kt-ajaxmodal fade" id="modalEtablissement" tabindex="-1" role="dialog" aria-labelledby="modalEtablissement">
            <div class="modal-dialog" role="document">
                <form class="form" action="?page=Entreprise.EntrepriseModal&controls=CAtexoModalUserEtablissement&action=submit&idEtablissement=<%=$this->getEtablissement()->getIdEtablissement()%>" method="GET">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel"><com:TTranslate>TEXT_MODIFIER_ETABLISSEMENT</com:TTranslate></h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-horizontal form-group-sm">
                                <input type="hidden" id="idEtablissement" value="<%=$this->getEtablissement()->getIdEtablissement()%>">
                                <input type="hidden" id="idEntreprise" value="<%=$this->getEtablissement()->getIdEntreprise()%>">
                                <div class="form-group">
                                    <label for="codeEtablissement" class="col-sm-4 control-label">
                                        <com:TTranslate>TEXT_NC</com:TTranslate>
                                    </label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="codeEtablissement" name="codeEtablissement" placeholder=""
                                               value="<%=$this->getEtablissement()->getCodeEtablissement()%>"
                                               required
                                               title="Please enter your username (at least 3 characters)">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="EtablissementAddress1" class="col-sm-4 control-label">
                                        <com:TTranslate>TEXT_ADRESSE</com:TTranslate>
                                    </label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="EtablissementAddress1" name="EtablissementAddress1" placeholder=""
                                               value="<%=$this->getEtablissement()->getAdresse()%>"
                                               required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="Etablissementaddress2" class="col-sm-4 control-label">
                                        <com:TTranslate>TEXT_ADRESSE_SUITE</com:TTranslate>
                                    </label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="Etablissementaddress2" name="Etablissementaddress2" placeholder=""
                                               value="<%=$this->getEtablissement()->getAdresse2()%>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="codePostalEtablissement" class="col-sm-4 control-label">
                                        <com:TTranslate>TEXT_CP</com:TTranslate>
                                    </label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="codePostalEtablissement" placeholder="" name="codePostalEtablissement"
                                               value="<%=$this->getEtablissement()->getCodePostal()%>"
                                               required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="villeEtablissement" class="col-sm-4 control-label">
                                        <com:TTranslate>TEXT_VILLE</com:TTranslate>
                                    </label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="villeEtablissement" placeholder="" name="villeEtablissement"
                                               value="<%=$this->getEtablissement()->getVille()%>"
                                               required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="paysEtablissement" class="col-sm-4 control-label">
                                        <com:TTranslate>DEFINE_PAYS_TERRITOIRES</com:TTranslate>
                                    </label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="paysEtablissement" placeholder="" name="paysEtablissement"
                                               value="<%=$this->getEtablissement()->getPays()%>"
                                               required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-4 col-sm-8">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" id="inscritAnnuaireDefenseEtablissement" name="inscritAnnuaireDefenseEtablissement"  <%= $this->getCheckedInscritAnnuaireDefense() %>>
                                                <com:TTranslate>INSCRIT_DEFENSE</com:TTranslate>
                                            </label>
                                        </div>
                                        <p>
                                            <a href="#"><i class="fa fa-angle-right"></i>
                                                <com:TTranslate>DEFINE_SAVOIR_PLUS</com:TTranslate>
                                            </a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="reset"
                                    class="btn btn-sm btn-default btn-reset" data-dismiss="modal"><%=Prado::localize('DEFINE_ANNULER')%>
                            </button>

                            <button type="submit"
                                    class="btn btn-sm btn-primary btn-submit"><%=Prado::localize('TEXT_VALIDER')%>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </prop:trueTemplate>
</com:TConditional>