<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_Menu;

class MenuGaucheHelios extends MpeTPage
{
    public function onLoad($param)
    {
    }

    /**
     * permet d'avoir l'url de redirection vers mpe-1.8.
     *
     * @param le nom de la cmd à faire passer dans l'url $param
     *
     * @return l'url de redirection
     */
    public function activateMenu($param)
    {
        $activateMenu = new Atexo_Menu();

        return $activateMenu->activateMenu($param, 'Agent');
    }

    public function goToTableauDeBord()
    {
        $keyWord = $this->quickSearch->Text;
        $this->response->redirect('?page=helios.RechercheTeletransmission&keyWord='.$keyWord);
    }
}
