<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_Entreprise;
use Application\Service\Atexo\Atexo_Util;

/**
 * @author Fatima Ezzahra IZGUA <fatima.izgua@atexo.com>
 *
 * @version 1.0
 *
 * @since 4.11.0
 *
 * @copyright Atexo 2015
 * @
 */
class AtexoContactDevis extends MpeTTemplateControl
{
    public int|bool $responsive = 0;

    public function onLoad($param)
    {
        $idEntreprise = Atexo_Util::atexoHtmlEntities($_GET['idEntreprise']);
        $entreprise = (new Atexo_Entreprise())->retrieveCompanyBdeById($idEntreprise, null, false);
        if ($entreprise) {
            $this->emailDevis->Text = ($entreprise->getAdressesElectroniques());
        }
    }

    /**
     * @return bool
     */
    public function getResponsive()
    {
        return $this->responsive;
    }

    /**
     * @param bool $responsive
     */
    public function setResponsive($responsive)
    {
        $this->responsive = $responsive;
    }
}
