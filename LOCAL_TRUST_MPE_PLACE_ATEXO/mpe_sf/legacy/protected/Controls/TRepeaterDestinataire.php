<?php

namespace Application\Controls;

use Exception;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * tableau  des destinataires d'une consultation.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class TRepeaterDestinataire extends MpeTTemplateControl
{
    public $_dataSource;

    public function setDataSource($value)
    {
        $this->repeaterDestinataire->dataSource = $value;
        $this->repeaterDestinataire->dataBind();
    }

    public function detailRepeater()
    {
        try {
            $this->repeaterDestinataire->dataSource = $this->_dataSource;
            $this->repeaterDestinataire->dataBind();
        } catch (Exception $e) {
            Prado::log("Impossible de récuperer le tableau des destinataires'-".$e->__toString().'-', TLogger::FATAL, 'Fatal');
        }
    }
}
