<?php

namespace Application\Controls;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonInscrit;
use Application\Propel\Mpe\Entreprise;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Entreprise;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Crypto\Atexo_Crypto_Certificat;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Etablissement;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Inscrit;
use Prado\Prado;

/**
 * composant de manipulation du compte utilisateur.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PanelComptePersonnel extends MpeTTemplateControl
{
    private string $_inscrit = '';
    private $_company;
    private $_postBack = null;

    public int $currentTab = 1;

    public function setCompany($value)
    {
        $this->_company = $value;
    }

    public function setInscrit($value)
    {
        $this->_inscrit = $value;
    }

    public function setPostBack($value)
    {
        $this->_postBack = $value;
    }

    public function getPostBack()
    {
        return $this->_postBack;
    }

    public function onLoad($param)
    {
        if (isset($_GET['retraits'])) {
            $this->currentTab = 1;
        }
        if (isset($_GET['questions'])) {
            $this->currentTab = 2;
        }
        if (isset($_GET['depots'])) {
            $this->currentTab = 3;
        }
        if (isset($_GET['echanges'])) {
            $this->currentTab = 4;
        }

        $this->customizeForm();
        if (isset($_GET['id'])) {
            $this->InfoProfil->setDisplay('Dynamic');
        } else {
            $this->InfoProfil->setDisplay('None');
        }
        if (Atexo_Module::isEnabled('AuthenticateInscritByCert', null)) {
            $this->panelChoixCert->setVisible(true);
        } else {
            $this->panelChoixCert->setVisible(false);
        }
        if (Atexo_Module::isEnabled('AuthenticateInscritByLogin')) {
            $this->panelAuthLoginPass->setVisible(true);
        } else {
            $this->panelAuthLoginPass->setVisible(false);
        }
        if ('ChangingUser' == $_GET['action'] || 'update' == $_GET['action']) {
            //Désactiver les validateurs de création compte
            $this->emailCreationValidator->SetEnabled(false);
            $this->loginCreationValidator->SetEnabled(false);
            $this->certifCreationValidator->SetEnabled(false);
            //Activer les validateurs de modification de compte de l'UES autentifié
            $this->emailModificationValidator->SetEnabled(true);
            $this->loginModificationValidator->SetEnabled(true);
            $this->certifModificationValidator->SetEnabled(true);
            if (!$this->getPostBack()) {
                //Désactiver les validateurs de password et conf password
                $this->password->Text = Atexo_CurrentUser::PASSWORD_INIT_VALUE;
                $this->confPassword->Text = Atexo_CurrentUser::PASSWORD_INIT_VALUE;
                $this->passwordValidator->SetEnabled(false);
            }
            if (!is_object($this->_inscrit)) {
                $this->_inscrit = (new Atexo_Entreprise_Inscrit())->retrieveInscritById(Atexo_CurrentUser::getIdInscrit());
            }
            if ((new Atexo_Entreprise_Inscrit())->inscritHasCertificate(Atexo_CurrentUser::getIdInscrit())) {
                $this->panelChoixLoginMdp->style = 'display:""';
                $this->panelChoixCert->setVisible(false);
                $this->CaseLogin->SetChecked(false);
                $Text = '<script>';
                $Text .= "document.getElementById('panelAuthLoginPass').style.display = 'none';";
                $Text .= "document.getElementById('div_cn_certif').style.display = '';";
                $Text .= "document.getElementById('div_ac_certif').style.display = '';";
                $Text .= '</script>';
                $this->JSCert->Text = $Text;
                $this->cnCertif->Text = (new Atexo_Crypto_Certificat())->getCnCertificat($this->_inscrit->getCert(), session_id());
                $this->acCertif->Text = (new Atexo_Crypto_Certificat())->getACCertificat($this->_inscrit->getCert());
            } else {
                $this->panelChoixLoginMdp->style = 'display:none';
            }
            if (!$this->_postBack) {
                $this->displayInscrit();
                $this->Page->setViewState('profil', $this->_inscrit->getProfil());
            }
        } elseif ('creation' == $_GET['action']) {
            // Desactiver les validateurs de modification Compte
            $this->emailModificationValidator->SetEnabled(false);
            $this->loginModificationValidator->SetEnabled(false);
            $this->certifModificationValidator->SetEnabled(false);
            //Activer les validateurs de création compte
            //$this->validateurSiret->SetEnabled(true);
            $this->emailCreationValidator->SetEnabled(true);
            $this->loginCreationValidator->SetEnabled(true);
            $this->certifCreationValidator->SetEnabled(true);
            $this->panelChoixLoginMdp->style = 'display:none';
            if (!$this->getPostBack()) {
                if (Atexo_Module::isEnabled('SocleExterneEntreprise')) {
                    $headersArray = array_change_key_case(getallheaders(), CASE_UPPER);
                    if (isset($headersArray['IV-USER']) && $headersArray['IV-USER']) {
                        if ($headersArray['Z-GIVENNAME']) {
                            $this->prenomPerso->Text = urldecode(Atexo_Util::atexoHtmlEntities($headersArray['Z-GIVENNAME']));
                        }
                        if ($headersArray['Z-SN']) {
                            $this->nomPerso->Text = urldecode(Atexo_Util::atexoHtmlEntities($headersArray['Z-SN']));
                        }
                        if ($headersArray['Z-MAIL']) {
                            $this->emailPersonnel->Text = urldecode(Atexo_Util::atexoHtmlEntities($headersArray['Z-MAIL']));
                        }
                    } else {
                        $this->response->redirect(Atexo_Config::getParameter('PF_URL').'?page=Entreprise.EntrepriseHome');
                    }
                }
            }
        } elseif ('creationByATES' == $_GET['action']) {
            // Desactiver les validateurs de modification Compte
            $this->emailModificationValidator->SetEnabled(false);
            $this->loginModificationValidator->SetEnabled(false);
            $this->certifModificationValidator->SetEnabled(false);
            //Activer les validateurs de creation compte
            //$this->validateurSiret->SetEnabled(true);
            $this->emailCreationValidator->SetEnabled(true);
            $this->loginCreationValidator->SetEnabled(true);
            $this->certifCreationValidator->SetEnabled(true);
            $this->panelChoixLoginMdp->style = 'display:none';
        }

        if (!$this->_postBack) {
            $this->displayEtablissements();
        }
    }

    /**
     * permet d'initialiser le composant des établissement.
     *
     * @return void
     *
     * @author Oussama SALMANI <oussama.salmani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function displayEtablissements()
    {
        if (!is_object($this->_inscrit)) {
            $this->_inscrit = (new Atexo_Entreprise_Inscrit())->retrieveInscritById(Atexo_CurrentUser::getIdInscrit());
        }
        $etablissements = (new Atexo_Entreprise_Etablissement())->retreiveEtablissementsByIdEntreprise(Atexo_CurrentUser::getIdEntreprise());
        $entreprise = (new Atexo_Entreprise())->retrieveCompanyBdeById(Atexo_CurrentUser::getIdEntreprise());
        if ($entreprise instanceof Entreprise) {
            if (1 == $entreprise->getSaisieManuelle()) {
                $this->etablissements->setAjoutManuel(true);
            }
            $this->etablissements->setIsEntrepriseLocale($entreprise->isEntrepriseLocale());
            $this->etablissements->setEtablissementsList((new Atexo_Entreprise_Etablissement())->fillEtablissementsVoArray($etablissements));
            $this->etablissements->setSiretMonEtablissement($this->_inscrit->getCodeEtablissement());
            $this->etablissements->setMode('3');
            $this->etablissements->setSirenEntreprise($this->_inscrit->getSitrenEntreprise());
            $this->etablissements->initComposant();
        }
    }

    /**
     * Chargement des champs de l'inscrit(Compte presonnel).
     */
    public function displayInscrit()
    {
        $inscrit = $this->_inscrit;
        if ($inscrit) {
            $this->nomPerso->Text = $inscrit->getNom();
            $this->prenomPerso->Text = $inscrit->getPrenom();
            $this->emailPersonnel->Text = $inscrit->getEmail();
            $this->identifiant->Text = $inscrit->getLogin();
            $this->telPersonnel->Text = $inscrit->getTelephone();
            $this->faxPersonnel->Text = $inscrit->getFax();
            if ('1' == $this->_inscrit->getProfil()) {
                $this->inscritSimple->Checked = true;
            } else {
                $this->adminEntreprise->Checked = true;
            }
        }
    }

    /**
     * remplir l'objet inscrit.
     */
    public function setNewInscrit()
    {
        $inscrit = new CommonInscrit();
        $inscrit->setNom($this->nomPerso->Text);
        $inscrit->setPrenom($this->prenomPerso->Text);
        $inscrit->setEmail(trim($this->emailPersonnel->Text));
        $inscrit->setTelephone($this->telPersonnel->Text);
        $inscrit->setFax($this->faxPersonnel->Text);
        if (Atexo_Module::isEnabled('AuthenticateInscritByLogin') || Atexo_Module::isEnabled('AuthenticateInscritByCert')) {
            if (true == $this->CaseLogin->Checked) {
                $inscrit->setLogin($this->identifiant->Text);
                if ('' != $this->password->Text
                && Atexo_CurrentUser::PASSWORD_INIT_VALUE != $this->password->Text
                && '' != $this->confPassword->Text
                && Atexo_CurrentUser::PASSWORD_INIT_VALUE != $this->confPassword->Text) {
                    Atexo_Entreprise_Inscrit::updateNewInscritPasswordByArgon2($inscrit, $this->password->getText());
                }
            } elseif (((Atexo_CurrentUser::readFromSession('userCert')) !== null)
            && (null !== Atexo_CurrentUser::readFromSession('userCert'))) {
                $inscrit->setCert(Atexo_CurrentUser::readFromSession('userCert'));
                $inscrit->setNumCert(Atexo_CurrentUser::readFromSession('userSerial'));
            }
        }

        if (Atexo_Module::isEnabled('SocleExterneEntreprise')) {
            $headersArray = array_change_key_case(getallheaders(), CASE_UPPER);
            if (isset($headersArray['IV-USER']) && $headersArray['IV-USER']) {
                $inscrit->setLogin($headersArray['IV-USER']);
                $inscrit->setMdp('');
            }
        }

        if (isset($_GET['id'])) {
            if (true == $this->inscritSimple->Checked) {
                $inscrit->setProfil('1'); //UES
            } elseif (true == $this->adminEntreprise->Checked) {
                $inscrit->setProfil('2'); //ATES
            }
        } else {
            if ($this->_company) {
                if ('INSEE' == $this->_company->getTypeObject()) { //|| get_class($this->_company)=="BaseQualifiee") { // entreprise existante
                    $inscrit->setProfil('2'); //Admin
                } else {
                    $inscrit->setProfil('1');
                }
            } else {
                $inscrit->setProfil('2'); // User
            }
        }
        $inscrit->setDateCreation(date('Y-m-d H:i:s'));
        $inscrit->setDateModification(date('Y-m-d H:i:s'));
        (new Atexo_Entreprise_Inscrit())->setIdEtablissementForInscrit($inscrit, $this->etablissements->getSiretMonEtablissement(), Atexo_CurrentUser::getIdEntreprise());

        return $inscrit;
    }

    /**
     * Verifier que le login est unique.
     */
    public function verifyLoginCreation($sender, $param)
    {
        if (true == $this->CaseLogin->Checked) {
            $login = $this->identifiant->getText();
            if ($login) {
                $inscrit = (new Atexo_Entreprise_Inscrit())->retrieveInscritByLogin($login);
                if ($inscrit) {
                    $this->Page->labelServerSide->Text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";
                    $param->IsValid = false;
                    $this->loginCreationValidator->ErrorMessage = Prado::localize('TEXT_IDENTIFIANT_EXISTE_DEJA', ['identifiant' => $login]);

                    return;
                }
            }
        }
    }

    /**
     * Verifier que le e-mail est unique.
     */
    public function verifyMailCreation($sender, $param)
    {
        $mail = $this->emailPersonnel->getText();
        if ($mail) {
            $inscrit = (new Atexo_Entreprise_Inscrit())->retrieveInscritByMail($mail);
            if ($inscrit) {
                $this->Page->labelServerSide->Text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";
                $param->IsValid = false;
                $this->emailCreationValidator->ErrorMessage = Prado::localize('TEXT_MAIL_EXISTE_DEJA', ['mail' => $mail]);

                return;
            }
        }
    }

    /*
     * Vérifier que le certificat est unique
     *
     */
    public function verifyCertifCreation($sender, $param)
    {
        if (false == $this->CaseLogin->Checked) {
            $certif = Atexo_CurrentUser::readFromSession('userCert');
            $serial = Atexo_CurrentUser::readFromSession('userSerial');
            if (!$certif && !$serial && ('' == $this->cnCertif->Text) && ('' == $this->acCertif->Text)) {
                $this->Page->labelServerSide->Text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";
                $param->IsValid = false;
                $this->certifCreationValidator->ErrorMessage = Prado::localize('TEXT_CERTIF_NON_SELECTIONNE');
                $this->certifCreationValidator->setVisible(false);

                return;
            }

            $inscrit = (new Atexo_Entreprise_Inscrit())->retrieveCertAuthUE($certif, $serial);
            if ($inscrit) {
                $this->Page->labelServerSide->Text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";
                $param->IsValid = false;
                $this->certifCreationValidator->ErrorMessage = Prado::localize('TEXT_CERTIFICAT_EXISTE_DEJA');
                $this->certifCreationValidator->setVisible(false);

                return;
            }
        }
    }

    /**
     * Verifier que l'identifiant n'existe pas parmis les utilisateurs
     *  autre que l'utilisateur authentifié.
     */
    public function verifyLoginModification($sender, $param)
    {
        if (true === $this->CaseLogin->Checked) {
            $login = $this->identifiant->Text;
            $inscrit = (new Atexo_Entreprise_Inscrit())->retrieveInscritByLoginForModValidation($login, $this->_inscrit->getId());
            if ($inscrit) {
                $this->Page->labelServerSide->Text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";
                $param->IsValid = false;
                $this->loginModificationValidator->ErrorMessage = Prado::localize('TEXT_IDENTIFIANT_EXISTE_DEJA', ['identifiant' => $login]);

                return;
            }
        }
    }

    /**
     * Verifier que l'adresse electronique n'existe pas parmis les utilisateurs
     *  autre que l'utilisateur authentifié.
     */
    public function verifyMailModification($sender, $param)
    {
        $mail = $this->emailPersonnel->Text;
        $inscrit = (new Atexo_Entreprise_Inscrit())->retrieveInscritByMailForModValidation($mail, $this->_inscrit->getId());
        if ($inscrit) {
            $this->Page->labelServerSide->Text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";
            $param->IsValid = false;
            $this->emailModificationValidator->ErrorMessage = Prado::localize('TEXT_MAIL_EXISTE_DEJA', ['mail' => $mail]);
        }
    }

    /*
     *  Verifier que le certificat n'existe pas parmis les utilisateurs
     *  autre que l'utilisateur authentifié
     */
    public function verifyCertifModification($sender, $param)
    {
        if ((false === $this->CaseLogin->Checked) && ('' == $this->identifiant->Text) && ('' == $this->password->Text)) {
            $certif = Atexo_CurrentUser::readFromSession('userCert');
            $serial = Atexo_CurrentUser::readFromSession('userSerial');
            if (!$certif && !$serial) {
                $this->Page->labelServerSide->Text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";
                $param->IsValid = false;
                $this->certifModificationValidator->ErrorMessage = Prado::localize('TEXT_CERTIF_NON_SELECTIONNE');

                return;
            }
            $inscrit = (new Atexo_Entreprise_Inscrit())->retrieveInscritByCertForModValidation($serial, $this->_inscrit->getId());
            if ($inscrit) {
                $searchCert = base64_decode($certif);
                foreach ($inscrit as $oneInscrit) {
                    $userCert = base64_decode($oneInscrit->getCert());
                    $cmp = strcmp($searchCert, $userCert);
                    if (0 == $cmp) {
                        $this->Page->labelServerSide->Text = "<script>document.getElementById('divValidationSummary').style.display='';</script>";
                        $param->IsValid = false;
                        $this->certifModificationValidator->ErrorMessage = Prado::localize('TEXT_CERTIFICAT_EXISTE_DEJA');

                        return;
                    }
                }
            }
        }
    }

    /**
     * Modifier compte Inscrit.
     *
     * @return -1 si erreur, >=0 si OK
     */
    public function updateCompte($inscrit)
    {
        //if ($this->IsValid) {
        if ($inscrit) {
            $inscrit->setNom($this->nomPerso->Text);
            $inscrit->setPrenom($this->prenomPerso->Text);
            $inscrit->setEmail(trim($this->emailPersonnel->Text));
            $inscrit->setTelephone($this->telPersonnel->Text);
            $inscrit->setFax($this->faxPersonnel->Text);
            if (true == $this->CaseLogin->Checked) {
                $inscrit->setLogin($this->identifiant->Text);
                if (Atexo_CurrentUser::PASSWORD_INIT_VALUE != $this->password->Text
                && '' != $this->password->Text
                && '' != $this->confPassword->Text
                && Atexo_CurrentUser::PASSWORD_INIT_VALUE != $this->confPassword->Text) {
                    Atexo_Entreprise_Inscrit::updateNewInscritPasswordByArgon2($inscrit, $this->password->getText());
                }
                $inscrit->setCert(null);
                $inscrit->setNumCert(null);
            } elseif ((false == $this->CaseLogin->Checked)
            && ((Atexo_CurrentUser::readFromSession('userCert')) !== null)
            && (null !== Atexo_CurrentUser::readFromSession('userCert'))) {
                $inscrit->setCert(Atexo_CurrentUser::readFromSession('userCert'));
                $inscrit->setNumCert(Atexo_CurrentUser::readFromSession('userSerial'));
                $inscrit->setLogin(null);
                $inscrit->setMdp(null);
            }
            //if (isset($_GET['id'])) {
            if (true == $this->inscritSimple->Checked) {
                $inscrit->setProfil('1'); //UES
            } elseif (true == $this->adminEntreprise->Checked) {
                $inscrit->setProfil('2'); //ATES
            }
            //}

            $inscrit->setDateModification(date('Y-m-d H:i:s'));
            (new Atexo_Entreprise_Inscrit())->setIdEtablissementForInscrit($inscrit, $this->etablissements->getSiretMonEtablissement(), Atexo_CurrentUser::getIdEntreprise());

            return $inscrit;
        } else {
            return false;
        }
        // }
    }

    public function customizeForm()
    {
        if (!Atexo_Module::isEnabled('PradoValidateurFormatEmail')) {
            $this->emailValidator->Enabled = false;
        }
        if (!Atexo_Module::isEnabled('InscriptionLibreEntreprise')) {
            if ('Entreprise.EntrepriseInscriptionUsers' == $_GET['page'] // Modification de mon compte via l'accueil
            || 'Entreprise.EntreprisePopupUpdateUser' == $_GET['page'] && $_GET['id'] == Atexo_CurrentUser::getIdInscrit()) {// Modification de mon compte via la liste des utilisateurs
                $this->panelNomPerso->SetEnabled(false);
                $this->panelPrenomPerso->SetEnabled(false);
                $this->panelLogin->SetEnabled(false);
            }
        }
    }

    public function goToModeLoginPassword()
    {
        $this->panelChoixLoginMdp->style = 'display:none';
        $this->panelChoixCert->setVisible(true);
        $this->CaseLogin->SetChecked(true);
        $this->password->Text = '';
        $this->confPassword->Text = '';
        $this->passwordValidator->SetEnabled(true);
        $Text = '<script>';
        $Text .= "document.getElementById('panelAuthLoginPass').style.display = '';";
        $Text .= "document.getElementById('div_cn_certif').style.display = 'none';";
        $Text .= "document.getElementById('div_ac_certif').style.display = 'none';";
        $Text .= '</script>';
        $this->JSCert->Text = $Text;
    }

    /**
     * permet sauvegarder la liste des établissements.
     *
     * @return void
     *
     * @author Oussama SALMANI <oussama.salmani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function saveEtablissements()
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
        $etablissementsVo = $this->etablissements->getEtablissementsList();
        (new Atexo_Entreprise_Etablissement())->saveEtablissements($etablissementsVo, Atexo_CurrentUser::getIdEntreprise());
        $etablissementsToDelate = $this->etablissements->getListeIdsEtabToDelete();
        (new Atexo_Entreprise_Etablissement())->deleteEtablissements($etablissementsToDelate, $connexion);
    }

    /**
     * Permet d'activer ou desactiver le profil (inscrit simple ou admin) de l'utilsateur
     *    Profil actif si l'utilisateur n'est pas le seul administrateur de l'entreprise
     *    Profil inactif si l'utilisateur est le seul administrateur de l'entreprise.
     *
     * @return bool
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function isProfilActif()
    {
        if ('ChangingUser' == $_GET['action'] && isset($_GET['id']) && $_GET['id'] != Atexo_CurrentUser::getIdInscrit()) {
            return true;
        }
        if ((new Atexo_Entreprise_Inscrit())->isInscritDernierAdmin()) {
            return false;
        }

        return true;
    }

    public function isCurrentTab($index)
    {
        if (intval($index) === $this->currentTab) {
            return ' active ';
        }

        return '';
    }
}
