<!--Debut Layer 1-->
<div class="ongletLayer" id="ongletLayer1" style="display:block;">
	<div class="top"></div>
	<div class="content"> 
	<com:TActivePanel ID="PanelResult">
		<com:TActivePanel ID="panelNoElementFound">
					<h2><com:TTranslate>DEFINE_AUCUN_RESULTAT</com:TTranslate>&nbsp;</h2>
		</com:TActivePanel>
		<!--Debut partitionneur-->
		<com:TActivePanel ID="panelMoreThanOneElementFound">
		<div class="line-partitioner">
			<h2><com:TTranslate>DEFINE_NOMBRE_RESULTATS</com:TTranslate> : <com:TLabel id="nombreElement"/></h2>
			<div class="partitioner">
				<div class="intitule"><strong><label for="allAnnonces_nbResultsTop"><com:TLabel id="labelAfficherTop"><com:TTranslate>DEFINE_AFFICHER</com:TTranslate></com:TLabel></label></strong></div> 
				<com:TDropDownList Attributes.title="<%=Prado::localize('NOMBRE_DE_RESULTAT_PAR_PAGE')%>" id="nbResultsTop" AutoPostBack="true" onSelectedIndexChanged="changePagerLenght" >
    					<com:TListItem Text="10" Selected="10"/>
    					<com:TListItem Text="20" Value="20"/>
              			<com:TListItem Text="50" Value="50" />
              			<com:TListItem Text="100" Value="100" />
              			<com:TListItem Text="500" Value="500" />
      				</com:TDropDownList> 
      				<div class="intitule"><com:TLabel id="resultParPageTop"><com:TTranslate>DEFINE_RESULTATS_PAGE</com:TTranslate></com:TLabel></div> 
				<label style="display:none;" for="allAnnonces_pageNumberTop"><com:TTranslate>DEFINE_ALLER_PAGE</com:TTranslate></label>
				<com:TPanel ID="panelBouttonGotoPageTop" DefaultButton="DefaultButtonTop" CssClass="float-left">
				<com:TTextBox Attributes.title="<%=Prado::localize('N_DE_LA_PAGE')%>" Text="1" id="pageNumberTop" />
				<div class="nb-total ">
					<com:TLabel ID="labelSlashTop" Text="/" visible="true"/> 
					<com:TLabel ID="nombrePageTop"/>
					<com:TButton ID="DefaultButtonTop"  OnClick="goToPage" Attributes.style="display:none"/>
				</div>
				</com:TPanel>
				<div class="liens">
					<com:TPager
      						ID="PagerTop"
          					ControlToPaginate="RepeaterOperations"
          					FirstPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-premier.gif' alt='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>' title='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>' />"
          					PrevPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-precedent.gif' alt='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>' title='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>' />"
          					Mode="NextPrev"
          					NextPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-suivant.gif' alt='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>' title='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>' />"
          					LastPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-dernier.gif' alt='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>' title='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>' />"
          					OnPageIndexChanged="pageChanged"
          				/>
				</div>
			</div>
		</div>
		<!--Fin partitionneur--> 
		
		<com:TRepeater 
			ID="RepeaterOperations" 
			EnableViewState="true"
		    AllowPaging="true"
   	        PageSize="<%= Application\Service\Atexo\Atexo_Config::getParameter('NOMBRE_AFFICHAGE_PAR_DEFAUT')%>"
   	        AllowCustomPaging="true"
		 >
			<prop:HeaderTemplate>
				<table class="table-results">
					<caption><com:TTranslate>DEFINE_LISTE_FONCTIONNELLES</com:TTranslate></caption>
					<thead>
						<tr>
							<th class="top" colspan="5"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></th>
						</tr>
						<tr>
							<th class="col-90" id="col_01" abbr="Ref">
								<a href="#">
									<com:TTranslate>DEFINE_CODE_OPERATION</com:TTranslate>
									<com:TImageButton imageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/arrow-tri-off.gif"  ToolTip="<%=Prado::localize('TEXT_TRIER')%>" CommandName="code" OnCommand="SourceTemplateControl.Trier" ></com:TImageButton>
								</a>
									<br>
								<a href="#">
									<com:TTranslate>DEFINE_BUDGET_GLOBAL</com:TTranslate>
									<com:TImageButton imageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/arrow-tri-off.gif"  ToolTip="<%=Prado::localize('TEXT_TRIER')%>" CommandName="budget" OnCommand="SourceTemplateControl.Trier" ></com:TImageButton>
								</a>
							</th>
							<th class="col-100" id="col_02">
								<a href="#">
									<com:TTranslate>TEXT_TYPE</com:TTranslate>
									<com:TImageButton imageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/arrow-tri-off.gif"  ToolTip="<%=Prado::localize('TEXT_TRIER')%>" CommandName="type" OnCommand="SourceTemplateControl.Trier" ></com:TImageButton>
									
								</a>
									<br>
								<a href="#">
									<com:TTranslate>TEXT_CATEGORIE</com:TTranslate>
									<com:TImageButton imageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/arrow-tri-off.gif"  ToolTip="<%=Prado::localize('TEXT_TRIER')%>" CommandName="categorie" OnCommand="SourceTemplateControl.Trier" ></com:TImageButton>
								</a>
							</th>
							<th class="col-400" id="col_03"><com:TTranslate>DEFINE_DESCRIPTION_DE_OPERATION</com:TTranslate></th>
							<th class="col-50" id="col_04">
								<a href="#">
									<com:TTranslate>ICONE_CALENDRIER</com:TTranslate>
									<com:TImageButton imageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/arrow-tri-off.gif"  ToolTip="<%=Prado::localize('TEXT_TRIER')%>" CommandName="annee_debut" OnCommand="SourceTemplateControl.Trier" ></com:TImageButton>
								</a>
									<br>
								<a href="#">
									<com:TTranslate>DEFINE_FIN</com:TTranslate>
									<com:TImageButton imageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/arrow-tri-off.gif"  ToolTip="<%=Prado::localize('TEXT_TRIER')%>" CommandName="annee_fin" OnCommand="SourceTemplateControl.Trier" ></com:TImageButton>
								</a>
							</th>
							<th class="actions" id="col_05"><com:TTranslate>TEXT_ACTIONS</com:TTranslate></th>
						</tr>
					</thead>
			</prop:HeaderTemplate>
			<prop:ItemTemplate>
				<tr class="<%#(($this->ItemIndex%2==0)? '':'on')%>">
					<td class="col-90" headers="col_01">
						<span class="ref"><%#$this->Data->getCode()%></span>
						<div class="spacer-mini"></div>
						<%# Application\Service\Atexo\Atexo_Util::getMontantArronditEspace($this->Data->getBudget())%>
						<com:TTranslate>FCSP_EUR_HT</com:TTranslate>
					</td>
					<td class="col-100" headers="col_02">
						<%#$this->Data->getTypeLibelle()%>
						<div class="spacer-mini"></div>
						<com:TImage imageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-travaux.gif" ToolTip="<%=Prado::localize('DEFINE_TRAVAUX')%>"  visible="<%#($this->Data->getCategorie()== '1') ? 'true' : 'false' %>"/>
						<com:TImage imageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-fournitures.gif" ToolTip="<%=Prado::localize('DEFINE_FOURNITURES')%>"  visible="<%#($this->Data->getCategorie()=='2')?'true' :'false'%>"/>
						<com:TImage imageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-services.gif" ToolTip="<%=Prado::localize('DEFINE_SERVICES')%>"  visible="<%#($this->Data->getCategorie()=='3')?'true' :'false'%>" />

					</td>
					<td class="col-400" headers="col_03">
						<%# Application\Service\Atexo\Atexo_Util::truncateTexte($this->Data->getDescription(),'230')%>
						<span style="<%#$this->SourceTemplateControl->isTextTruncated($this->Data->getDescription())%>"
						
						onmouseover="afficheBulle('ctl0_CONTENU_PAGE_ResultSearchOperation_RepeaterOperations_ctl<%#($this->ItemIndex+1)%>_infosConsultation', this)" onmouseout="cacheBulle('ctl0_CONTENU_PAGE_ResultSearchOperation_RepeaterOperations_ctl<%#($this->ItemIndex+1)%>_infosConsultation')" class="info-suite">...</span>
						<com:TPanel ID="infosConsultation" CssClass="info-bulle" ><div><%#$this->Data->getDescription()%></div></com:TPanel>
					</td>
					<td class="col-50" headers="col_04">
						<%#$this->Data->getAnneeDebut()%>
						<br>
						<%#$this->Data->getAnneeFin()%>
					</td>
					<td class="actions" headers="col_05">
						<com:TImageButton
							Visible="<%=Application\Service\Atexo\Atexo_CurrentUser::hasHabilitation('GererOperations')%>"
							Cssclass="action-mpe"
							Text ="<%=Prado::localize('DEFINE_CREER_UNE_OPERATION')%>"
							Oncommand="SourceTemplateControl.createOperation"
							CommandName="<%#base64_encode($this->Data->getIdOperation())%>"
							ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-edit.gif"
						>
						</com:TImageButton>
						
						<com:THyperLink
							Attributes.href="index.php?page=Agent.TableauDeBord&idOperation=<%#base64_encode($this->Data->getIdOperation())%>"
						> 
							<img alt="<%=Prado::localize('DEFINE_ACCEDER_CONSULTATIONS_LIEES')%>" src="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-acces-consultation.gif">
						</com:THyperLink>
						
						<com:TActiveImageButton 
							ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-supprimer-big.gif"
			   				cssClass = "btn-action"
			   				Visible="<%=Application\Service\Atexo\Atexo_CurrentUser::hasHabilitation('GererOperations')%>"
							OnCallBack="SourceTemplateControl.getIdAgent"
				           	ActiveControl.CallbackParameter="<%#$this->Data->getIdOperation()%>"
							Attributes.onclick="openModal('<%=$this->SourceTemplateControl->getClientId()%>_suppression-operation','popup-small2',this);" 
							Attributes.title="<%=Prado::localize('DEFINE_SUPPRIMER_OPERATION')%>" />
						
						<!-- TODO suppression -->
					</td>
				</tr>
			</prop:ItemTemplate>
		 	<prop:FooterTemplate>
				</table>
		   </prop:FooterTemplate>
		</com:TRepeater>
		</com:TActivePanel>
		<!--Debut partitionneur-->
		<div class="line-partitioner">
			<com:TActivePanel Cssclass="partitioner" id="panelPartiner">
				<div class="intitule"><strong><label for="horsLigne_nbResultsBottom"><com:TLabel id="labelAfficherDown"><com:TTranslate>DEFINE_AFFICHER</com:TTranslate></com:TLabel></label></strong></div> 
					<com:TDropDownList Attributes.title="<%=Prado::localize('NOMBRE_DE_RESULTAT_PAR_PAGE')%>" id="nbResultsBottom" AutoPostBack="true" onSelectedIndexChanged="changePagerLenght" >
      						<com:TListItem Text="10" Selected="10"/>
                			<com:TListItem Text="20" Value="20"/>
                			<com:TListItem Text="50" Value="50" />
                			<com:TListItem Text="100" Value="100" />
                			<com:TListItem Text="500" Value="500" />
      				</com:TDropDownList> 
				<div class="intitule"><com:TLabel id="resultParPageDown"><com:TTranslate>DEFINE_RESULTATS_PAGE</com:TTranslate></com:TLabel></div> 
				<label style="display:none;" for="horsLigne_pageNumberBottom"><com:TTranslate>DEFINE_ALLER_PAGE</com:TTranslate></label>
				<com:TPanel ID="panelBouttonGotoPageBottom" DefaultButton="DefaultButtonBottom" CssClass="float-left">
					<com:TTextBox Attributes.title="<%=Prado::localize('N_DE_LA_PAGE')%>" Text="1" id="pageNumberBottom" />
					<div class="nb-total ">
						<com:TLabel ID="labelSlashBottom" Text="/" visible="true"/> 
						<com:TLabel ID="nombrePageBottom"/>
						<com:TButton ID="DefaultButtonBottom"  OnClick="goToPage" Attributes.style="display:none"/>
					</div>
				</com:TPanel>
				<div class="liens">
					<com:TPager
      						ID="PagerBottom"
          					ControlToPaginate="RepeaterOperations"
          					FirstPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-premier.gif' alt='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>' title='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>' />"
          					PrevPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-precedent.gif' alt='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>' title='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>' />"
          					Mode="NextPrev"
          					NextPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-suivant.gif' alt='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>' title='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>' />"
          					LastPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-dernier.gif' alt='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>' title='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>' />"
          					OnPageIndexChanged="pageChanged"
          				/>
				</div>
			</com:TActivePanel>
			<com:TLinkButton
				Visible="<%=Application\Service\Atexo\Atexo_CurrentUser::hasHabilitation('GererOperations')%>"
				Cssclass="ajout-el float-left"
				Text ="<%=Prado::localize('DEFINE_CREER_UNE_OPERATION')%>"
				Onclick="createOperation"
			>
			</com:TLinkButton>
		</div>
		<!--Fin partitionneur--> 
		<!--Fin Tableau Operations--> 
		<div class="breaker"></div>
		</com:TActivePanel>
	</div>
	<div class="bottom"></div>
</div>
<!--Fin Layer 1-->

<!--Debut Modal-->
<div class="<%=$this->getClientId()%>_suppression-operation" style="display:none;">
	<!--Debut Formulaire-->
	<div class="form-bloc">
		<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
		<div class="content">
			<p><com:TTranslate>DEFINE_ETES_VOUS_SUR_SUPPRIMER_OPERATION</com:TTranslate><br /><com:TTranslate>DEFINE_CETTE_ACTION_SUPP_CONSULTATIONS_ASSOCIEES</com:TTranslate></p>
		</div>
		<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
	</div>
	<!--Fin Formulaire-->
	<!--Debut line boutons-->
	<div class="boutons-line">
		<com:TActiveButton 
			Cssclass="bouton-moyen float-left" 
			Text="<%=Prado::localize('TEXT_ANNULER')%>"
			Attributes.title="<%=Prado::localize('TEXT_ANNULER')%>"
			Attributes.onclick="J('.<%=$this->getClientId()%>_suppression-operation').dialog('close');" />
		<com:TActiveButton  
			Cssclass="bouton-moyen float-right" 
			Text="<%=Prado::localize('TEXT_VALIDER')%>"
			OnCallBack="supprimerOperation"  
			Attributes.title="<%=Prado::localize('TEXT_VALIDER')%>"
			Attributes.onclick="J('.suppression-operation').dialog('close');J('.msg-info').hide().fadeIn(1200);" />	
	</div>
	<!--Fin line boutons-->
</div>
<!--Fin Modal-->
<com:TActiveHiddenField id="idSelecteOperation"/>
<com:TActiveLabel id="scriptT" style="display:none"/>
 