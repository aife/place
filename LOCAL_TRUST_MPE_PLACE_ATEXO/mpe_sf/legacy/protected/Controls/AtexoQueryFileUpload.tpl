<div class="bloc-fileupload">
	<div class="clearfix">
		<span class="btn btn-sm btn-success fileinput-button pull-left"><%=$this->getTitre()%>
			<input <%=$this->getDisabled()%> id="<%=$this->getClientId()%>_fileupload" type="file" title="<%=Prado::localize('DEFINE_PARCOURIR_3_POINTS')%>" name="files[]" data-url="index.php?page=<%= Application\Service\Atexo\Atexo_Util::getTypeUserCalledForPradoPages() %>.SaveJqueryUploadFile" multiple >
		</span>
		<div class="p-l-1" id="<%=$this->getClientId()%>fileName" style="overflow: hidden;"></div>
	</div>
	<div style="display:<%=$this->getAfficherProgress() ? '' : 'none'%>">
		<div id="<%=$this->getClientId()%>erreur" class="red" ></div>
		<div class="progress progress-success progress-striped" id="<%=$this->getClientId()%>_progress">
			  <div class="bar" style="width: 0%;"></div>
		</div>
		<div id="<%=$this->getClientId()%>percent" class="pourcentage-chargement">0%</div>
	</div>
	<com:TActiveHiddenField ID="NomFichier" /> 
	<com:TActiveHiddenField	id="reponseServeur" /> 
	<com:TActiveHiddenField ID="hasFile" />
</div>
<script>
	var tmpFilePrefix = '<%=$this->getTmpFileName()%>';
J(function () {
	 J('#<%=$this->getClientId()%>_fileupload').click(function() {

		 let btnRadioId = J(this).attr('id');
		 J('#ctl0_CONTENU_PAGE_blocDce_ajoutPieceDce').removeAttr('checked');
		 J('#ctl0_CONTENU_PAGE_blocDce_remplacerPieceDce').removeAttr('checked');
		 J('#ctl0_CONTENU_PAGE_blocDce_remplacerToutDce').removeAttr('checked');

		 if (btnRadioId == 'ctl0_CONTENU_PAGE_blocDce_uploadBarNewPieceDce_fileupload') {
			 J('#ctl0_CONTENU_PAGE_blocDce_ajoutPieceDce').attr('checked', 'checked');
		 } else if (btnRadioId == 'ctl0_CONTENU_PAGE_blocDce_uploadBar_fileupload') {
			 J('#ctl0_CONTENU_PAGE_blocDce_remplacerToutDce').attr('checked', 'checked');
		 }

		   J('#<%=$this->getClientId()%>_progress .bar').css('width','0');
		   J('#<%=$this->getClientId()%>percent').empty();
		   J('#<%=$this->getClientId()%>percent').append('0%');
		   J('#<%=$this->getClientId()%>fileName').empty();
		   J('#<%=$this->getClientId()%>erreur').empty();
		   J('#<%=$this->getClientId()%>erreur').append(" ");
	 });

    J('#<%=$this->getClientId()%>_fileupload').fileupload({
         dataType: 'json',
		 maxChunkSize: 10000000,
         recalculateProgress : true,
         progressInterval : 200,
		 dropZone: null,
		 add: function (e, data) {
			var tailleFile = data.originalFiles[0]['size'];
			J('#<%=$this->getClientId()%>erreur').empty();
			J('#<%=$this->getClientId()%>erreur').append(" ");
			if (tailleFile > <%=$this->getFileSizeMax()%>) {
				J('#<%=$this->getClientId()%>erreur').append("<%= Prado::localize('TEXT_TAILLE_AUTORISEE_DOC', array('size' =>  Application\Service\Atexo\Atexo_Util::arrondirSizeFile(($this->getFileSizeMax() / 1024)))) %>");
				J('#<%=$this->getClientId()%>_progress .bar').css('width', '0');
				J('#<%=$this->getClientId()%>percent').empty();
				J('#<%=$this->getClientId()%>percent').append('');
			} else {
				data.submit();
			}
		},
		submit : function (e, data) {

			data.formData = {tmpFileName: tmpFilePrefix+J.now()+getUid()};
			},
         done: function (e, data) {
            J.each(data.result.files, function (index, file) {
	            J('#<%=$this->getClientId()%>fileName').empty();
            	J('#<%=$this->getClientId()%>_reponseServeur').val(file.tmpFile);
            	J('#<%=$this->getClientId()%>_hasFile').val('1');
            	J('#<%=$this->getClientId()%>_fileSize').val(file.size);
            	J('#<%=$this->getClientId()%>erreur').empty();
            	var progress = 0;
                var name = navigator.appName ;
                if (name == 'Microsoft Internet Explorer') {
	            	J('#<%=$this->getClientId()%>_NomFichier').val(decodeURIComponent(file.nomFichier));
	            	J('#<%=$this->getClientId()%>fileName').append('<div class="text-left">'+decodeURIComponent((file.nomFichier))+'</div>');
	            } else {
	            	J('#<%=$this->getClientId()%>_NomFichier').val(utf8_decode(decodeURIComponent(file.nomFichier)));
	            	J('#<%=$this->getClientId()%>fileName').append('<div class="text-left">'+utf8_decode(decodeURIComponent(file.nomFichier))+'</div>');
                }
            });
			 <%= $this->getFonctionJs()? $this->getFonctionJs() : "" %>;
        },
        progressall: function (e, data) {
             var progress = parseInt(data.loaded / data.total * 100, 10);
             J('#<%=$this->getClientId()%>_progress .bar').css('width',progress + '%');
         	 J('#<%=$this->getClientId()%>percent').empty();
        	 J('#<%=$this->getClientId()%>percent').append(progress+"%");

	    },
        fail: function (e, data) {
        	J('#<%=$this->getClientId()%>erreur').empty();
        	J('#<%=$this->getClientId()%>erreur').append("<%=Prado::localize('DEFINE_ERREUR_CHARGMENT_FICHIER')%>");
        	J('#<%=$this->getClientId()%>_progress .bar').css('width','0');
  		    J('#<%=$this->getClientId()%>percent').empty();
  		    J('#<%=$this->getClientId()%>percent').append('0%');
        }
    });
});
</script>