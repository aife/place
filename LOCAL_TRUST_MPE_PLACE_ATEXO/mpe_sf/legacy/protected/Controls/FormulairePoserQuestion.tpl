<!--Debut colonne droite-->
<!-- div class="main-part" id="main-part" -->
<div class="clearfix">
	<ul class="breadcrumb">
		<li>
			<a href="/entreprise">
						<com:TTranslate>TEXT_ACCUEIL</com:TTranslate>
                    </a>
		</li>
		<li>
			<a href="#">
				<com:TTranslate>DEFINE_TEXT_CONSULTATIONS</com:TTranslate>
			</a>
		</li>
		<li>
			<com:TTranslate>DEFINE_POSER_QUESTION</com:TTranslate>
		</li>
	</ul>
</div>

<com:PanelMessageErreur id="panelMessageErreur"/>
<com:AtexoValidationSummary ValidationGroup="validateQuestion" />
<com:EntrepriseConsultationSummary id="idEntrepriseConsultationSummary" />
<!--Debut Bloc Poser une question-->

<div class="panel panel-primary">
	<div class="panel-heading">
		<span class="h4 panel-title">
			<com:TTranslate>DEFINE_POSER_QUESTION</com:TTranslate>
		</span>
		<span class="pull-right">
			<com:TTranslate>DEFINE_SYMBOLE</com:TTranslate> <span class="text-danger">*</span> <com:TTranslate>TEXT_INDIQUE_CHAMPS_OBLIGATOIRES</com:TTranslate>
		</span>
	</div>
	<div class="panel-body">
		<!--Debut Bloc Poser une question-->
		<div class="clearfix">
			<!--Debut Line Question-->
			<div class="form-group form-group-sm col-md-12">
				<label for="<%=$this->question->getClientID()%>" class="col-md-2">
					<com:TTranslate>DEFINE_QUESTION_MAX_250</com:TTranslate></span><span class="text-danger"> *</span> :
				</label>
				<div class="col-md-10">
					<com:TTextBox  id="question" Attributes.onKeyUp="limiteTailleTextArea(this,250);" TextMode="MultiLine" CssClass="long-630 high-60 form-control"></com:TTextBox>
					<com:TCustomValidator
							ValidationGroup="validateQuestion"
							ControlToValidate="question"
							ClientValidationFunction="validateQuestion"
							Display="Dynamic"
							ErrorMessage="<%=Prado::localize('DEFINE_QUESTION_MAX_250')%>"
							Text="<span class='text-danger' title='Champ obligatoire' class='check-invalide'><i class='fa fa-times-circle-o p-r-1' aria-hidden='true'></i>Champ obligatoire</span>"
							EnableClientScript="true" >
						<prop:ClientSide.OnValidationError>
							document.getElementById('divValidationSummary').style.display='';
							document.getElementById('ctl0_CONTENU_PAGE_formulairePoserQuestion_btValider').style.display='';
						</prop:ClientSide.OnValidationError>
					</com:TCustomValidator>
				</div>
			</div>
			<!--Fin Line Question-->
			<!--Debut Bloc Ajout piece jointe-->
			<com:TPanel ID="panelPJ" Display="None">
				<div class="form-group form-group-sm col-md-12">
					<label for="<%=$this->ajoutFichier->getClientID()%>" class="col-md-2">
						<com:TTranslate>DEFINE_JOINDRE_FICHIER</com:TTranslate> :</label>
					</label>
					<div class="col-md-10">
						<com:AtexoFileUpload id="ajoutFichier" cssClass="upload-size" Attributes.title="<%=Prado::localize('TEXT_FICHIER_JOINT')%>"  Attributes.size="108"  />
					</div>
				</div>
			</com:TPanel>
			<!--Fin Bloc Ajout piece jointe-->
			<!--Fin Bloc Poser une question-->
		</div>
	</div>
	<div class="panel-footer clearfix">
		<div class="pull-left">
			<com:TButton CssClass="btn btn-default btn-sm" Text="<%=Prado::localize('TEXT_ANNULER')%>" Attributes.title="<%=Prado::localize('DEFINE_ANNULER_RETOURNER_DETAIL_CONSULTATION')%>"  onClick="retour"/>
		</div>
		<div class="pull-right">
			<com:TButton id="btValider" CssClass="btn btn-primary btn-sm" Text="<%=Prado::localize('DEFINE_TEXT_ENVOYER')%>" ValidationGroup="validateQuestion" Attributes.title="<%=Prado::localize('DEFINE_ENVOYER_QUESTION')%>"  Attributes.OnClick = "javascript:document.getElementById('ctl0_CONTENU_PAGE_formulairePoserQuestion_btValider').style.display='none';" onClick="save" />
		</div>
	</div>
</div>