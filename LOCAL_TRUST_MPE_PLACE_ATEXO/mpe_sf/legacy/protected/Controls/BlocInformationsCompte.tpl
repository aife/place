<div class="form-bloc">
	<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
	<div class="content">
		<h2><com:TActiveLabel ID="titre"/><span class="normal italique"> <com:TActiveLabel id="sousTitre" /></span> :</h2>
		<div class="line">
			<div class="intitule intitule-150"><com:TTranslate>DEFINE_NOM_PRENOM</com:TTranslate> :</div>
			<div class="content-bloc bloc-600"><com:TActiveLabel id="nom_prenom" /></div>
		</div>
		<div class="line">
			<div class="intitule intitule-150"><com:TTranslate>TEXT_IDENTIFIANT</com:TTranslate> :</div>
			<div class="content-bloc bloc-600"><com:TActiveLabel id="identifiant" /></div>
		</div>
		<div class="line">
			<div class="intitule intitule-150"><com:TTranslate>DEFINE_EMAIL_EA_PMI</com:TTranslate>  :</div>
			<div class="content-bloc"><com:TActiveLabel id="email" /></div>
		</div>
		<div class="line">
			<div class="intitule intitule-150"><com:TTranslate>DEFINE_ENTITE_PUBLIQUE</com:TTranslate></div>
			<div class="content-bloc bloc-600"><com:TActiveLabel id="entite" /></div>
		</div>
		<div class="line">
			<div class="intitule intitule-150"><com:TTranslate>DEFINE_SERVICE</com:TTranslate>:</div>
			<div class="content-bloc bloc-600"><com:TActiveLabel id="service" /></div>
		</div>
	</div>
	<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
</div>