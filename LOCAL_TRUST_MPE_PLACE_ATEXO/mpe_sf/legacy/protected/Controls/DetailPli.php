<?php

namespace Application\Controls;

use Application\Pages\Commun\DownloadFile;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonEnveloppe;
use Application\Propel\Mpe\CommonOffres;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Responses;
use Application\Service\Atexo\Interfaces\Atexo_Interfaces_Offre;
use Application\Service\Atexo\Response\Atexo_Response_CriteriaVo;
use Prado\Prado;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class DetailPli extends MpeTTemplateControl
{
    public $_idPli;
    public $_typeEnv;
    public $_reference;
    public $_consultation;
    public $_idOffre;

    public function getIdPli()
    {
        return $this->_idPli;
    }

    public function setIdPli($idPli)
    {
        $this->_idPli = $idPli;
    }

    public function getTypeEnv()
    {
        return $this->_typeEnv;
    }

    public function setTypeEnv($typeEnv)
    {
        $this->_typeEnv = $typeEnv;
    }

    public function getReference()
    {
        return $this->_reference;
    }

    public function setReference($reference)
    {
        $this->_reference = $reference;
    }

    public function getConsultation()
    {
        return $this->_consultation;
    }

    public function getIdOffre()
    {
        return $this->_idOffre;
    }

    public function setIdOffre($value)
    {
        $this->_idOffre = $value;
    }

    public function setConsultation($consultation)
    {
        $this->_consultation = $consultation;
    }

    public function onLoad($param)
    {
        $enveloppe = null;
        $infoRapportPDF = [];
        $atexoCurrentUserCurrentOrganisme = Atexo_CurrentUser::getCurrentOrganism();

        if ($this->_idPli && ($this->_reference || $this->_consultation instanceof CommonConsultation)) {
            $this->setInfoConsultation();
            if ($this->_reference && $this->_consultation instanceof CommonConsultation) {
                $this->panelSignature->setVisible(boolval($this->_consultation->getSignatureOffre()));

                $typeEnveloppe = (new Atexo_Consultation_Responses())->getTypesEnveloppes($this->_consultation->getEnvCandidature(), $this->_consultation->getEnvOffre(), $this->_consultation->getEnvAnonymat(), $this->_consultation->getEnvOffreTechnique());

                $criteriaResponse = new Atexo_Response_CriteriaVo();
                $criteriaResponse->setReference($this->_reference);
                $criteriaResponse->setOrganisme($atexoCurrentUserCurrentOrganisme);
                $criteriaResponse->setTypesEnveloppes($typeEnveloppe);
                switch ($this->_typeEnv) {
                    case Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE'):
                         $enveloppes = (new Atexo_Consultation_Responses())->retrieveEnveloppesCandidatureElectroniques($criteriaResponse);
                                                                                $nomTypeEnveloppe = Prado::localize('CANDIDATURE');
                        break;
                    case Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE'):
                         $enveloppes = (new Atexo_Consultation_Responses())->retrieveEnveloppesOffresTechniqueElectroniques($this->_reference, null, $typeEnveloppe, '', '', $atexoCurrentUserCurrentOrganisme);
                                                                                $nomTypeEnveloppe = Prado::localize('OFFRE');
                        break;
                    case Atexo_Config::getParameter('TYPE_ENV_OFFRE'):
                         $enveloppes = (new Atexo_Consultation_Responses())->retrieveEnveloppesOffresElectroniques($criteriaResponse);
                                                                                $nomTypeEnveloppe = Prado::localize('OFFRE');
                        break;
                    case Atexo_Config::getParameter('TYPE_ENV_ANONYMAT'):
                         $enveloppes = (new Atexo_Consultation_Responses())->retrieveEnveloppesAnonymatElectroniques($this->_reference, null, $typeEnveloppe, '', '', $atexoCurrentUserCurrentOrganisme);
                                                                                $nomTypeEnveloppe = Prado::localize('ANONYMAT');
                        break;
                    default:
                         $enveloppes = null;
                        break;
                }

                if ($enveloppes && is_array($enveloppes)) {
                    foreach ($enveloppes as $oneEnveloppe) {
                        if ($oneEnveloppe instanceof CommonEnveloppe && $oneEnveloppe->getIdEnveloppeElectro() == $this->_idPli) {
                            $enveloppe = $oneEnveloppe;
                        }
                    }

                    if ($enveloppe instanceof CommonEnveloppe && $enveloppe->getOffre() instanceof CommonOffres) {
                        $this->setViewState('offre', $enveloppe->getOffre());
                        $this->panelAccuse->setVisible(!empty($enveloppe->getOffre()->getIdPdfEchangeAccuse()));
                        $this->numPli->Text = $enveloppe->getNumPli();
                        $this->nomSociete->Text = $enveloppe->getOffre()->getNomEntrepriseInscrit();
                        $this->TypeEnv->Text = $nomTypeEnveloppe;
                        $infoRapportPDF['numeroPli'] = $enveloppe->getNumPli();
                        $infoRapportPDF['nomSte'] = $enveloppe->getOffre()->getNomEntrepriseInscrit();
                        $infoRapportPDF['typeEnveloppe'] = $nomTypeEnveloppe;
                        if (boolval($this->_consultation->getSignatureOffre())) {
                            $statut = $enveloppe->getResultatValiditeSignature($atexoCurrentUserCurrentOrganisme);
                            $this->statutSignature->setStatut($statut);
                            $infoRapportPDF['statutSignature'] = $statut;
                        }
                        if ('0' != $enveloppe->getSousPli()) {
                            $this->lot->Text = ' - '.Prado::localize('TEXT_LOT').' '.$enveloppe->getSousPli();
                        }
                        $this->horodatage->Text = $enveloppe->getOffre()->getDateRemisePlie();
                        $infoRapportPDF['horodatageEnveloppe'] = $enveloppe->getOffre()->getDateRemisePlie();
                        $agentOuverture = '';
                        $statutEnveloppe = $enveloppe->getLibelleStatutEnveloppe();
                        if ($enveloppe->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_EN_LIGNE')
                        || $enveloppe->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_HORS_LIGNE')
                        || $enveloppe->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE')
                        || $enveloppe->getStatutEnveloppe() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_A_DISTANCE')) {
                            $statutEnveloppe .= ' '.Prado::localize('DEFINE_TEXT_LE').' '.Atexo_Util::iso2frnDateTime($enveloppe->getDateheureOuverture());
                            if (0 != $enveloppe->getAgentIdOuverture()) {
                                $agent = (new Atexo_Agent())->retrieveAgent($enveloppe->getAgentIdOuverture());
                                if ($agent) {
                                    $statutEnveloppe .= ' '.Prado::localize('TEXT_PAR').' '.Atexo_Util::escapeXmlChars($agent->getPrenom()).' '.Atexo_Util::escapeXmlChars($agent->getNom());
                                }
                                $statutEnveloppe2 = '';
                                if (Atexo_Module::isEnabled('fourEyes')) {
                                    if ($enveloppe->getDateheureOuvertureAgent2() && '0000-00-00 00:00:00' != $enveloppe->getDateheureOuvertureAgent2() && $enveloppe->getAgentIdOuverture2()) {
                                        $statutEnveloppe2 = Prado::localize('DEFINE_ET').' '.Prado::localize('DEFINE_TEXT_LE').' '.Atexo_Util::iso2frnDateTime($enveloppe->getDateheureOuvertureAgent2());
                                        $agent = (new Atexo_Agent())->retrieveAgent($enveloppe->getAgentIdOuverture2());
                                        if ($agent) {
                                            $statutEnveloppe2 .= ' '.Prado::localize('TEXT_PAR').' '.Atexo_Util::escapeXmlChars($agent->getPrenom()).' '.Atexo_Util::escapeXmlChars($agent->getNom());
                                        }
                                    }
                                }
                            }
                        }
                        $this->statutPli->Text = $statutEnveloppe;
                        $infoRapportPDF['statutPli'] = $statutEnveloppe;
                        $this->statut2Pli->Text = $statutEnveloppe2;

                        $this->Page->setViewState('infoRapportPdfPli', $infoRapportPDF);
                    } else {
                        echo '4';
                    }
                } else {
                    echo '3';
                }
            } else {
                echo '2';
            }
        } elseif ($this->_idOffre && ($this->_reference || $this->_consultation instanceof CommonConsultation)) {
            $this->statutSignature->setNouveauAffichage(true);
            $this->setInfoConsultation();
            $offre = (new Atexo_Interfaces_Offre())->getOffreByIdOffre($this->_idOffre);
            if ($offre instanceof CommonOffres) {
                if ($this->_consultation instanceof CommonConsultation) {
                    $this->panelSignature->setVisible(boolval($this->_consultation->getSignatureOffre()));
                    if (boolval($this->_consultation->getSignatureOffre())) {
                        $statut = $offre->getResultatValiditeSignatureReponse(Atexo_CurrentUser::getCurrentOrganism());
                        $this->statutSignature->setStatut($statut);
                    }
                }
                $this->setViewState('offre', $offre);
                $this->panelAccuse->setVisible(!empty($offre->getIdPdfEchangeAccuse()));
                $this->numPli->Text = $offre->getNumeroReponse();
                $this->nomSociete->Text = $offre->getNomEntrepriseInscrit();
                $infoRapportPDF['numeroPli'] = $offre->getNumeroReponse();
                $infoRapportPDF['nomSte'] = $offre->getNomEntrepriseInscrit();
                $infoRapportPDF['typeEnveloppe'] = $nomTypeEnveloppe;
                $infoRapportPDF['statutSignature'] = $offre->getLibelleStatutOffres();

                $agentOuverture = '';
                if ($offre->getStatutoffres() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_EN_LIGNE')
                || $offre->getStatutoffres() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_HORS_LIGNE')
                || $offre->getStatutoffres() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE')
                || $offre->getStatutoffres() == Atexo_Config::getParameter('STATUT_ENV_OUVERTE_A_DISTANCE')) {
                    $statutOffre = $offre->getLibelleStatutOffres(); // changer le methode voir avec LEZ
                    $statutOffre .= ' '.Prado::localize('DEFINE_TEXT_LE').' '.Atexo_Util::iso2frnDateTime($offre->getDateheureOuverture());
                    if (0 != $offre->getAgentIdOuverture()) {
                        $agent = (new Atexo_Agent())->retrieveAgent($offre->getAgentIdOuverture());
                        if ($agent) {
                            $statutOffre .= ' '.Prado::localize('TEXT_PAR').' '.Atexo_Util::escapeXmlChars($agent->getPrenom()).' '.Atexo_Util::escapeXmlChars($agent->getNom());
                        }
                        $statutOffre2 = '';
                        if (Atexo_Module::isEnabled('fourEyes')) {
                            if ($offre->getDateheureOuvertureAgent2() && '0000-00-00 00:00:00' != $offre->getDateheureOuvertureAgent2() && $offre->getAgentIdOuverture2()) {
                                $statutOffre2 = Prado::localize('DEFINE_ET').' '.Prado::localize('DEFINE_TEXT_LE').' '.Atexo_Util::iso2frnDateTime($offre->getDateheureOuvertureAgent2());
                                $agent = (new Atexo_Agent())->retrieveAgent($offre->getAgentIdOuverture2());
                                if ($agent) {
                                    $statutOffre2 .= ' '.Prado::localize('TEXT_PAR').' '.Atexo_Util::escapeXmlChars($agent->getPrenom()).' '.Atexo_Util::escapeXmlChars($agent->getNom());
                                }
                            }
                        }
                    }
                }
                $this->statutPli->Text = $statutOffre;
                $infoRapportPDF['statutPli'] = $statutOffre;
                $this->statut2Pli->Text = $statutOffre2;
                $this->horodatage->Text = $offre->getDateRemisePlie();
                $infoRapportPDF['horodatageEnveloppe'] = $offre->getDateRemisePlie();
                $this->panelTypePanel->Visible = false;
                $this->Page->setViewState('infoRapportPdfPli', $infoRapportPDF);
            }
        } else {
            echo '1';
        }
    }

    /**
     * Permet de recuperer l'objet consultation ainsi la reference consultation
     * et de initialisser les propriete $this->_consultation et $this->_reference.
     */
    public function setInfoConsultation()
    {
        if ($this->_reference && !($this->_consultation instanceof CommonConsultation)) {
            $criteria = new Atexo_Consultation_CriteriaVo();
            $criteria->setcalledFromPortail(false);
            $criteria->setIdService(Atexo_CurrentUser::getCurrentServiceId());
            $criteria->setAcronymeOrganisme(Atexo_CurrentUser::getCurrentOrganism());
            $criteria->setConnectedAgentId(Atexo_CurrentUser::getId());
            $criteria->setIdReference($this->_reference);
            $consultation = (new Atexo_Consultation())->search($criteria);
            if ($consultation && is_array($consultation) && 1 == count($consultation)) {
                $this->_consultation = array_shift($consultation);
            }
        } elseif (!$this->_reference && $this->_consultation instanceof CommonConsultation) {
            $this->_reference = $this->_consultation->getId();
        }
    }

    public function downloadAccuseReception()
    {
        $offre = $this->getViewState('offre');
        $ref = '';
        if ($this->_consultation instanceof CommonConsultation) {
            $ref = $this->_consultation->getReferenceUtilisateur();
        }
        if ($offre instanceof CommonOffres) {
            $nomFichier = str_replace('[_info_entreprise_]', '_'.$ref.'_'.$offre->getSiretInscrit(), Atexo_Config::getParameter('NOM_PDF_ACCUSE_RECEPTION'));
            (new DownloadFile())->downloadFiles($offre->getIdPdfEchangeAccuse(), $nomFichier, $offre->getOrganisme());
        }
    }
}
