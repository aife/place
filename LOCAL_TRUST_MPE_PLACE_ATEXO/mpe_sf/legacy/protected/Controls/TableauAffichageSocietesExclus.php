<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_SocietesExclues;
use Application\Service\Atexo\Atexo_Util;

/**
 * Classe d'affichage des sociétés exclues.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2010
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class TableauAffichageSocietesExclus extends MpeTPage
{
    public $critere;
    public $nbrResultSearch;
    public $_callFromEntreprise;

    public function onLoad($param)
    {
    }

    public function setCallFromEntreprise($callFromEntreprise)
    {
        $this->_callFromEntreprise = $callFromEntreprise;
    }

    public function getCallFromEntreprise()
    {
        return $this->_callFromEntreprise;
    }

    public function fillRepeaterWithSearchData($critere, $forNombreResultats = false, $idSocieteExclue = null)
    {
        $this->Page->setViewState('idSocieteExclue', $idSocieteExclue);

        $this->Page->setViewState('critere', $critere);
        $nbrResult = (new Atexo_SocietesExclues())->search($critere, true, $idSocieteExclue);

        $this->Page->setViewState('nbrElements', $nbrResult);

        self::afficherTableauResultat();

        if ($nbrResult) {
            $this->Page->tableauAffichageSocietesExclues->nombrePageTop->Text = ceil($nbrResult / $this->Page->tableauAffichageSocietesExclues->tableauSocietesExclues->PageSize);
            $this->Page->tableauAffichageSocietesExclues->nombrePageBottom->Text = ceil($nbrResult / $this->Page->tableauAffichageSocietesExclues->tableauSocietesExclues->PageSize);
            $this->Page->tableauAffichageSocietesExclues->tableauSocietesExclues->setVirtualItemCount($nbrResult);
            $this->Page->tableauAffichageSocietesExclues->PagerTop->visible = true;
            $this->Page->tableauAffichageSocietesExclues->PagerBottom->visible = true;
            $this->Page->tableauAffichageSocietesExclues->pageNumberBottom->visible = true;
            $this->Page->tableauAffichageSocietesExclues->pageNumberTop->visible = true;
            self::populateData($critere, false, $idSocieteExclue);
        } else {
            $this->Page->tableauAffichageSocietesExclues->PagerTop->visible = false;
            $this->Page->tableauAffichageSocietesExclues->PagerBottom->visible = false;
            $this->Page->tableauAffichageSocietesExclues->panelPagerBottom->visible = false;
            $this->Page->tableauAffichageSocietesExclues->panelPagerTop->visible = false;
            $this->Page->tableauAffichageSocietesExclues->pageNumberBottom->visible = false;
            $this->Page->tableauAffichageSocietesExclues->pageNumberTop->visible = false;
            $this->Page->tableauAffichageSocietesExclues->labelSlashBottom->visible = false;
            $this->Page->tableauAffichageSocietesExclues->labelSlashTop->visible = false;
        }
    }

    public function populateData($critere, $forNombreResultats = false, $idSocieteExclue = null)
    {
        $offset = $this->Page->tableauAffichageSocietesExclues->tableauSocietesExclues->CurrentPageIndex * $this->Page->tableauAffichageSocietesExclues->tableauSocietesExclues->PageSize;
        $limit = $this->Page->tableauAffichageSocietesExclues->tableauSocietesExclues->PageSize;
        if ($offset + $limit > $this->Page->tableauAffichageSocietesExclues->tableauSocietesExclues->getVirtualItemCount()) {
            $limit = $this->Page->tableauAffichageSocietesExclues->tableauSocietesExclues->getVirtualItemCount() - $offset;
        }

        $societesExclues = (new Atexo_SocietesExclues())->search($critere, $forNombreResultats, $idSocieteExclue, $limit, $offset);
        if ($societesExclues) {
            $this->Page->tableauAffichageSocietesExclues->tableauSocietesExclues->DataSource = $societesExclues;
            $this->Page->tableauAffichageSocietesExclues->tableauSocietesExclues->DataBind();
        }
    }

    public function pageChanged($sender, $param)
    {
        $this->Page->tableauAffichageSocietesExclues->tableauSocietesExclues->CurrentPageIndex = $param->NewPageIndex;
        $this->Page->tableauAffichageSocietesExclues->pageNumberBottom->Text = $param->NewPageIndex + 1;
        $this->Page->tableauAffichageSocietesExclues->pageNumberTop->Text = $param->NewPageIndex + 1;
        self::afficherTableauResultat();
        $this->populateData($this->Page->getViewState('critere'));
    }

    public function changePagerLenght($sender, $param)
    {
        switch ($sender->ID) {
            case 'nbResultsBottom':
                $pageSize = $this->Page->tableauAffichageSocietesExclues->nbResultsBottom->getSelectedValue();
                                                  $this->Page->tableauAffichageSocietesExclues->nbResultsTop->setSelectedValue($pageSize);
                break;
            case 'nbResultsTop':
                $pageSize = $this->Page->tableauAffichageSocietesExclues->nbResultsTop->getSelectedValue();
                                               $this->Page->tableauAffichageSocietesExclues->nbResultsBottom->setSelectedValue($pageSize);
                break;
        }

        $this->Page->tableauAffichageSocietesExclues->tableauSocietesExclues->PageSize = $pageSize;
        $nombreElement = $this->Page->getViewState('nbrElements');
        $this->Page->tableauAffichageSocietesExclues->nombrePageTop->Text = ceil($nombreElement / $this->Page->tableauAffichageSocietesExclues->tableauSocietesExclues->PageSize);
        $this->Page->tableauAffichageSocietesExclues->nombrePageBottom->Text = ceil($nombreElement / $this->Page->tableauAffichageSocietesExclues->tableauSocietesExclues->PageSize);

        $critere = $this->Page->getViewState('critere');
        $this->Page->tableauAffichageSocietesExclues->tableauSocietesExclues->setCurrentPageIndex(0);
        self::afficherTableauResultat();
        $this->populateData($critere);
    }

    public function afficherTableauResultat()
    {
        $this->Page->tableauAffichageSocietesExclues->visible = true;
        $this->Page->tableauRechercheSocietesExclues->visible = false;
        $this->Page->labelTextResultat->visible = true;
        $this->Page->labelTextRecherche->visible = true;
        $this->Page->tableauAffichageSocietesExclues->nombreResultats->Text = $this->Page->getViewState('nbrElements');
    }

    public function hasHabilitationModifOrSuppSocietesExclues()
    {
        if (Atexo_CurrentUser::hasHabilitation('ModifierSocietesExclues') || Atexo_CurrentUser::hasHabilitation('SupprimerSocietesExclues')) {
            return 1;
        } else {
            return 0;
        }
    }

    public function getCritere()
    {
        return $this->critere;
    }

    public function setCritere($critere)
    {
        $this->critere = $critere;
    }

    public function setNbrResultSearch($nbrResultSearch)
    {
        $this->Page->tableauAffichageSocietesExclues->nbrResultSearch = $nbrResultSearch;
    }

    public function getNbrResultSearch()
    {
        return $this->Page->tableauAffichageSocietesExclues->nbrResultSearch;
    }

    /*
     * Fonction permettant d'aller à une page donnée
     */
    public function goToPage($sender, $param)
    {
        $numPage = null;
        switch ($sender->ID) {
            case 'DefaultButtonTop':
                $numPage = $this->pageNumberTop->Text;
                break;
            case 'DefaultButtonBottom':
                $numPage = $this->pageNumberBottom->Text;
                break;
        }
        if (Atexo_Util::isEntier($numPage)) {
            if ($numPage >= $this->nombrePageTop->Text) {
                $numPage = $this->nombrePageTop->Text;
            } elseif ($numPage <= 0) {
                $numPage = 1;
            }
            $this->Page->tableauAffichageSocietesExclues->tableauSocietesExclues->CurrentPageIndex = $numPage - 1;
            $this->pageNumberBottom->Text = $numPage;
            $this->pageNumberTop->Text = $numPage;
            $this->populateData($this->Page->getViewState('critere'));
            self::afficherTableauResultat();
        } else {
            $this->pageNumberTop->Text = $this->tableauDeBordRepeater->CurrentPageIndex + 1;
            $this->pageNumberBottom->Text = $this->tableauDeBordRepeater->CurrentPageIndex + 1;
        }
    }
}
