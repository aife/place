<com:TRepeater ID="RepeaterReferentielRadio">
	<prop:ItemTemplate>
		<com:TPanel id="idPanel" cssClass="form-field" > 
			<com:TPanel cssClass="top">
		    	<span class="left">&nbsp;</span>
		    	<span class="right">&nbsp;</span>
					<span class="title">
						<com:TActiveCheckBox id="refRadio" Attributes.title="<%#Prado::localize($this->Data->getCodeLibelle())%>" cssClass="check" Checked="false" 	Attributes.onclick="toggleMenu('<%= $this->divRefRadio->getClientId() %>');"/>
						<com:TLabel id="texteCheck"  text="<%#Prado::localize($this->Data->getCodeLibelle())%>" />
					</span>
			</com:TPanel>
			<com:TPanel cssClass="content">
				<com:TPanel ID="divRefRadio" CssClass="line" style="display:none;" >
					<com:TPanel visible="<%#$this->parent->parent->getLot()%>">
						<com:AtexoReferentielCheckRadioLots id="idLot" lot="1" ref="<%#$this->Data%>"/>
					</com:TPanel>
					<com:TActivePanel ID="panelReferentielRadio" CssClass="line" visible="<%# (!$this->SourceTemplateControl->getCheck()) && (!$this->parent->parent->getLot()) %>">
						<div class="<%#($this->SourceTemplateControl->getCssClass())%>"> 
							<com:TLabel id="titre" text="<%#Prado::localize($this->Data->getCodeLibelle())%>" visible="<%# !$this->SourceTemplateControl->getCheck()%>" />
							<span <%#($this->Data->getObligatoire()&& $this->SourceTemplateControl->getObligatoire()) ? "class='champ-oblig'" : "style='display:none'" %>>
								<com:TLabel id="etoile" text="*" visible="<%# !$this->SourceTemplateControl->getCheck()%>"/>
							</span>
							<span <%#($this->Data->getLibelleInfoBulle()) ? "class='champ-oblig'" : "style='display:none'" %>>
								<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" onmouseover="afficheBulle('infosRefrentiel_<%#$this->Data->getId()%>', this)" onmouseout="cacheBulle('infosRefrentiel_<%#$this->Data->getId()%>')" class="picto-info-intitule" alt="Info-bulle" title="Info-bulle" />
							</span>
							<com:TLabel id="deuxPoint" text=":" visible="<%# !$this->SourceTemplateControl->getCheck()%>"/>
						</div>
						<div class="content-bloc bloc-50">
					 		<div class="line">
								<com:TActiveRadioButton  
									ID="OptionOui" 
									cssClass="radio" 
									GroupName="radioRef"
									checked="<%#($this->Data->getDefaultValue() === '1' ) ? 'true' : 'false' %>"
									Text="<%=Prado::localize('TEXT_OUI')%>"
									Attributes.title="<%=Prado::localize('TEXT_OUI')%>"
								/>
				            </div>
				            <div class="line">
								<com:TActiveRadioButton  
									ID="OptionNon" 
									cssClass="radio"
									GroupName="radioRef" 
									Checked="<%#($this->Data->getDefaultValue() === '0' ) ? 'true' : 'false' %>"
									Text="<%=Prado::localize('TEXT_NON')%>"
									Attributes.title="<%=Prado::localize('TEXT_NON')%>"
								/>
							</div>
						</div>
						<com:TTextBox style="display:none" ID="ClientIdsRadio" text="<%=$this->OptionOui->getClientId().'#'.$this->OptionNon->getClientId()%>"/>
						<com:TCustomValidator 
							ID="validatorOptionSelected"
							visible="<%#($this->Data->getObligatoire()) ? 'true' :'false'%>"
							ControlToValidate="ClientIdsRadio"
							ValidationGroup="<%#($this->SourceTemplateControl->getValidationGroup())%>"
							ClientValidationFunction="validateOptionSelected"
							ErrorMessage="<%=$this->codeLibelle->Text%> ,<%=Prado::localize('DEFINE_EST_OBLIGATOIRE') %>"
							Text=" ">   
							<prop:ClientSide.OnValidationError>
								blockErreur = '<%#($this->SourceTemplateControl->getValidationSummary())%>';
								if(document.getElementById(blockErreur))
				 				document.getElementById(blockErreur).style.display='';
				 				if(document.getElementById('ctl0_CONTENU_PAGE_saveBack'))
								document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
								if(document.getElementById('ctl0_CONTENU_PAGE_save'))
								document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
								if(document.getElementById('ctl0_CONTENU_PAGE_buttonValidate'))
								document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
								if(document.getElementById('ctl0_CONTENU_PAGE_blocBoutons'))
								document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
							</prop:ClientSide.OnValidationError>
						</com:TCustomValidator>
						
			         	<com:TLabel id="iDLibelle" visible="false" text="<%#$this->Data->getId()%>" />
			         	<com:TLabel id="codeLibelle" visible="false" text="<%#$this->Data->getCodeLibelle()%>" />
			         	<com:TLabel id="labelReferentielRadio"  text="" visible="<%# $this->SourceTemplateControl->getLabel()%>"/>
			         	<com:TLabel id="typeRef" visible="false" text="<%#$this->Data->getType()%>" />
			         	<com:TLabel id="typeSearch" visible="false" text="<%#$this->Data->getTypeSearch()%>" />
			         	<com:THiddenField id="modeRecherche" value="<%#$this->Data->getModeRecherche()%>" />
			         	<com:THiddenField id="oldValue" />
						<div id="infosRefrentiel_<%#$this->Data->getId()%>" class="info-bulle" onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();">
							<div><%#Prado::localize($this->Data->getLibelleInfoBulle())%></div>
						</div>
					</com:TActivePanel>
				</com:TPanel>
				<com:TPanel  cssClass="breaker"></com:TPanel >
			</com:TPanel>	
		<com:TPanel  cssClass="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span>
		</com:TPanel >
		</com:TPanel>	
	</prop:ItemTemplate>
</com:TRepeater>
