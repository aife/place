<div class="toggle-panel clearfix m-b-2" id="legende">
    <div class="content pull-right">
        <a href="#" class="btn btn-primary btn-sm" data-toggle="modal" data-target=".panelLegend">
            <com:TTranslate>LEGENDE</com:TTranslate>
            <i class="fa fa-question"></i>
        </a>
        <div class="modal fade panelLegend in" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="display:none;" id="panelLegend">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h2 class="h4 modal-title">
                            <com:TTranslate>LEGENDE</com:TTranslate>
                        </h2>
                    </div>
                    <div class="modal-body clearfix">
                        <ul class="list-unstyled">
                            <h3 class="h4 page-header m-t-0">
                                <com:TTranslate>DEFINE_DETAIL_SIGNATURE</com:TTranslate>
                            </h3>
                            <li>
                                <div>
                                    <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-certificat-small.gif" alt="<%=Prado::localize('TEXT_CERTIFICAT')%>" /> :
                                    <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-ok-small.gif" alt="<%=Prado::localize('TEXT_VERIFICATION_OK')%>" />
                                    <strong>
                                        <com:TTranslate>TEXT_VERIFICATION_OK</com:TTranslate>
                                    </strong>
                                </div>
                                <div class="col-md-offset-1">
                                    <com:TTranslate>DEFINE_MESSAGE_VERIFICATION_OK</com:TTranslate>
                                </div>
                            </li>
                            <li class="m-t-2">
                                <div>
                                    <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-certificat-small.gif" alt="<%=Prado::localize('TEXT_CERTIFICAT')%>"/> :
                                    <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif" alt="<%=Prado::localize('TEXT_VERIFICATION_KO')%>" />
                                    <strong>
                                        <com:TTranslate>TEXT_VERIFICATION_KO</com:TTranslate>
                                    </strong>
                                </div>
                                <div class="col-md-offset-1">
                                    <com:TTranslate>DEFINE_MESSAGE_VERIFICATION_KO</com:TTranslate>
                                </div>
                            </li>
                            <li class="m-t-2">
                                <div>
                                    <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-certificat-small.gif" alt="<%=Prado::localize('TEXT_CERTIFICAT')%>" /> :
                                    <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-validation-incertaine.gif" alt="<%=Prado::localize('TEXT_VERIFICATION_INCERTAINE')%>" />
                                    <strong>
                                        <com:TTranslate>TEXT_VERIFICATION_INCERTAINE</com:TTranslate>
                                    </strong>
                                </div>
                                <div class="col-md-offset-1">
                                    <com:TTranslate>DEFINE_MESSAGE_VERIFICATION_INCERTAINE</com:TTranslate>
                                </div>
                            </li>
                            <li class="m-t-2">
                                <div>
                                    <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-aucune-signature.gif" alt="<%=Prado::localize('TEXT_SIGNATURE_ELECTRONIQUE_NON_VERIFIABLE')%>" /> :
                                    <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-vide.gif" alt="<%=Prado::localize('VALIDITE_NON_VERIFIABLE')%>" />
                                    <strong>
                                        <com:TTranslate>TEXT_SIGNATURE_ELECTRONIQUE_NON_VERIFIABLE</com:TTranslate>
                                    </strong>
                                </div>
                                <div class="col-md-offset-1">
                                    <com:TTranslate>DEFINE_MESSAGE_SIGNATURE_NON_VERIFIABLE</com:TTranslate>
                                </div>
                            </li>
                            <li class="m-t-2">
                                <div>
                                    <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-aucune-signature.gif" alt="<%=Prado::localize('TEXT_SIGNATURE_ELECTRONIQUE_MANQUANTE')%>" /> :
                                    <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-vide.gif" alt="<%=Prado::localize('VALIDITE_NON_VERIFIABLE')%>" />
                                    <strong>
                                        <com:TTranslate>TEXT_SIGNATURE_ELECTRONIQUE_MANQUANTE</com:TTranslate>
                                    </strong>
                                </div>
                                <div class="col-md-offset-1">
                                    <com:TTranslate>DEFINE_MESSAGE_SIGNATURE_INTROUVABLE</com:TTranslate>
                                </div>
                            </li>
                        </ul>
                        <ul class="list-unstyled">
                            <h3 class="h4 page-header m-t-0">
                                <com:TTranslate>TEXT_ACTIONS</com:TTranslate>
                            </h3>
                            <li>
                                <div>
                                    <img alt="<%=Prado::localize('DEFINE_TELECHARGER_SIGNATURE')%>" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-dl-dossier.gif" />
                                    <strong><com:TTranslate>DEFINE_TELECHARGER</com:TTranslate> le jeton de signature </strong>
                                </div>
                                <div class="col-md-offset-1">
                                    <com:TTranslate>TEXT_DWNLD_TOKEN_SIGNATURE</com:TTranslate>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="modal-footer">
                        <div class="col-md-2 pull-right">
                            <button type="button" class="btn btn-sm btn-block btn-danger" data-dismiss="modal">Fermer</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>