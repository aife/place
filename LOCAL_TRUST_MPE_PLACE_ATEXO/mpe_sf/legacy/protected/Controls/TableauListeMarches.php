<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_ListeMarches;

/**
 * commentaires.
 *
 * @author mouslim MITALI
 * @copyright Atexo 2009
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class TableauListeMarches extends MpeTTemplateControl
{
    private string $_postBack = '';
    private bool $_calledFromAgent = true;
    private $_organisme;

    public function setPostBack($value)
    {
        $this->_postBack = $value;
    }

    public function getPostBack()
    {
        return $this->_postBack;
    }

    public function setCalledFromAgent($value)
    {
        $this->_calledFromAgent = $value;
    }

    public function getCalledFromAgent()
    {
        return $this->_calledFromAgent;
    }

    public function setOrganisme($value)
    {
        $this->_organisme = $value;
    }

    public function getOrganisme()
    {
        return $this->_organisme;
    }

    public function onLoad($param)
    {
        if (!$this->getPostBack()) {
            $this->displayListeMarche();
        }
    }

    /**
     * affiche la liste des marchés d'un organisme.
     */
    public function displayListeMarche()
    {
        $organisme = '';
        $dataSource = [];
        if ($this->getOrganisme()) {
            $organisme = $this->getOrganisme();
        } else {
            $organisme = Atexo_CurrentUser::getOrganismAcronym();
        }
        //echo '----'.$organisme;exit;
        if ($organisme) {
            $dataSource = (new Atexo_ListeMarches())->retreiveListeMarches($organisme);
        }
        if (0 == (is_countable($dataSource) ? count($dataSource) : 0)) {
            //$this->panelListeMarches->setDisplay("None");
        }
        $this->repeaterListeMarches->DataSource = $dataSource;
        $this->repeaterListeMarches->DataBind();

        if (!$this->getCalledFromAgent()) {
            $this->linkAdd->setVisible(false);
        }
    }

    /**
     * Refresh du tableau des listes marchés.
     */
    public function onCallBackRefreshTableau($sender, $param)
    {
        $this->displayListeMarche();
        $this->panelListeMarches->render($param->getNewWriter());
    }

    /**
     * Supprimer une liste marché.
     */
    public function deleteMarche($sender, $param)
    {
        (new Atexo_ListeMarches())->doDeleteMarche($param->CommandName, Atexo_CurrentUser::getOrganismAcronym());
    }
}
