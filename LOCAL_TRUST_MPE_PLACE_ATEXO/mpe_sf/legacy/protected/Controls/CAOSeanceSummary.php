<?php

namespace Application\Controls;

use Application\Propel\Mpe\CommonTCAOSeance;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Commission\Atexo_Commission_Agent;
use Application\Service\Atexo\Commission\Atexo_Commission_CriteriaVo;
use Application\Service\Atexo\Commission\Atexo_Commission_IntervenantExterne;
use Application\Service\Atexo\Commission\Atexo_Commission_Seance;

/**
 * Affiche le résumé de la séance.
 *
 * @author Améline PLACIDE <ameline.placide@atexo.com>
 * @copyright Atexo 2012
 *
 * @since MPE4
 */
class CAOSeanceSummary extends MpeTPage
{
    private $_dataSource = null;

    public function onLoad($param)
    {
        if ($this->getDataSource()) {
            $criteriaVo = new Atexo_Commission_CriteriaVo();
            $criteriaVo->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());

            $_t_type_commission = (new Atexo_Commission_Seance())->getCommissionType($criteriaVo);

            $seance = $this->getDataSource();
            if ($seance instanceof CommonTCAOSeance) {
                $date = Atexo_Util::iso2frnDateTime(Atexo_Util::atexoHtmlEntities($seance->getDate()), true) ?: '-';
                $statut = (new Atexo_Commission_Seance())->getPictoStatut($seance->getIdRefValStatut());
                $type = $_t_type_commission[$seance->getIdCommission()] ?: '-';
                $lieu = ($seance->getLieu()) ? Atexo_Util::atexoHtmlEntitiesDecode($seance->getLieu()) : '-';
                $salle = ($seance->getSalle()) ? Atexo_Util::atexoHtmlEntitiesDecode($seance->getSalle()) : '-';
                //Recuperation du nombre d'agents invites a la seance
                $nbAgents = (new Atexo_Commission_Agent())->getNombreSeanceAgents($seance->getIdSeance(), $seance->getOrganisme());
                //Recuperation du nombre d'intervenants externes invites a la seance
                //$nbIntervenants = Atexo_Commission_IntervenantExterne::getNombreSeanceIntervenants($seance->getIdSeance(),$seance->getOrganisme());

                $this->date->Text = $date;
                $this->statut->cssclass = $statut;
                $this->typeCommission->Text = $type;
                $this->lieu->Text = $lieu;
                $this->salle->Text = $salle;

                $this->nombreAgents->Text = $nbAgents;
                //Recuperation du nombre d'intervenants externes invites a la seance
                $this->nombreIntervenantsExternes->Text = (new Atexo_Commission_IntervenantExterne())->getNombreSeanceIntervenants($seance->getIdSeance(), $seance->getOrganisme());
                $this->popupInvites->setNavigateUrl("javascript:popUp('?page=Commission.PopUpSeanceInvites&id=".base64_encode($seance->getIdSeance())."','yes')");
            }
        }
    }

    public function getDataSource()
    {
        return $this->_dataSource;
    }

    public function setDataSource($dataSource)
    {
        $this->_dataSource = $dataSource;
    }
}
