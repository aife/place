<?php

namespace Application\Controls;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonCategorieLot;
use Application\Propel\Mpe\CommonConfigurationPlateformePeer;
use Application\Propel\Mpe\CommonConsultation;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Lots;
use Exception;
use Prado\Prado;

/**
 * Le bloc identite de l'entreprise.
 *
 * @author Amal EL BEKKAOUI <amal.elbekkaoui@atexo.com>
 *
 * @version 1.0
 *
 * @since 4.7.0
 *
 * @copyright Atexo 2014
 */
class AtexoBlocObjetCandidatureMPS extends MpeTTemplateControl
{
    public $_consultation;
    public $mode; //'0' : affichage, '1' : modification

    /**
     * @return mixed
     */
    public function getConsultation()
    {
        return $this->_consultation;
    }

    /**
     * @param mixed $consultation
     */
    public function setConsultation($consultation)
    {
        $this->_consultation = $consultation;
    }

    /**
     * @return mixed
     */
    public function getMode()
    {
        return $this->mode;
    }

    /**
     * @param mixed $mode
     */
    public function setMode($mode)
    {
        $this->mode = $mode;
    }

    /**
     * Permet de charger les informations des lots.
     *
     * @param CommonConsultation $consultation
     *
     * @return void
     *
     * @author Amal EL BEKKAOUI <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2017
     */
    public function chargerInfoLots($consultation, $selectedLots = null)
    {
        if ('1' == $this->getMode()) {
            $this->chargerListeLots($consultation, $selectedLots);
        } else {
            $this->chargerRecapLots($consultation, $selectedLots);
        }
    }

    /**
     * Permet de charger la liste de selection des lots.
     *
     * @param CommonConsultation $consultation
     *
     * @return void
     *
     * @author Amal EL BEKKAOUI <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2017
     */
    public function chargerListeLots($consultation, $selectedLots = null)
    {
        try {
            $this->panelMessageErreur->setDisplay('None');

            if ($consultation instanceof CommonConsultation && $consultation->getAlloti()) {
                $this->panelModif->setVisible(true);
                $this->panelAffichage->setVisible(false);
                $listeLots = [];
                $allLots = $consultation->getAllLots();

                if (is_array($allLots) && count($allLots)) {
                    $listeLots[0] = Prado::localize('TOUS_LES_LOTS');

                    foreach ($allLots as $lot) {
                        if ($lot instanceof CommonCategorieLot) {
                            $listeLots[$lot->getLot()] = Prado::localize('TEXT_LOT').' '.$lot->getLot().' - '.$lot->getDescription();
                        }
                    }

                    $this->listeLots->setDataSource($listeLots);
                    $this->listeLots->dataBind();
                    $idsLots = explode(',', $selectedLots);

                    if (is_array($idsLots) && count($idsLots)) {
                        $this->listeLots->setSelectedValues($idsLots);
                        $this->hiddenSelectedValue->setValue(implode(',', $idsLots));
                    }

                    $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
                    $modules = CommonConfigurationPlateformePeer::doSelectOne(new Criteria(), $connexion);

                    if (1 == $modules->getInterfaceDume()) {
                        $this->listeLots->setEnabled(false);
                    }
                } else {
                    $this->setVisible(false);
                }
            } else {
                $this->setVisible(false);
            }
        } catch (Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('app');
            $logger->error("Erreur recuperation chargement des lots : \n\nErreur = ".$e->getMessage()." \n\nTrace:\n ".$e->getTraceAsString()." \n\n Methode = AtexoBlocObjetCandidatureMPS::chargerListeLots");
        }
    }

    /**
     * Permet de charger les informations des lots.
     *
     * @param string $listeLots
     *
     * @return void
     *
     * @author Amal EL BEKKAOUI <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2017
     */
    public function chargerRecapLots($consultation, $listeLots)
    {
        try {
            $this->panelMessageErreur->setDisplay('None');
            $idsLots = explode(',', $listeLots);
            $arrayData = [];
            if ($consultation instanceof CommonConsultation && $consultation->getAlloti() && !empty($idsLots)) {
                $this->panelAffichage->setVisible(true);
                $this->panelModif->setVisible(false);
                $allLots = (new Atexo_Consultation_Lots())->retrieveListeLotsByArray($consultation->getId(), $consultation->getOrganisme(), $idsLots);
                if (is_array($allLots) && count($allLots)) {
                    if (is_array($allLots) && count($allLots)) {
                        foreach ($allLots as $lot) {
                            if ($lot instanceof CommonCategorieLot) {
                                $arrayData[$lot->getLot()] = Prado::localize('TEXT_LOT').' '.$lot->getLot().' - '.$lot->getDescription();
                            }
                        }
                    }
                }
            } else {
                $this->setVisible(false);
            }
            $this->listeLotsRecap->setDataSource($arrayData);
            $this->listeLotsRecap->dataBind();
        } catch (Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('app');
            $logger->error("Erreur recuperation chargement des lots : \n\nErreur = ".$e->getMessage()." \n\nTrace:\n ".$e->getTraceAsString()." \n\n Methode = AtexoBlocObjetCandidatureMPS::chargerRecapLots");
        }
    }

    /**
     * Permet de retourner les lots selectionnes.
     *
     * @return array list lots selectionnes
     *
     * @author Amal EL BEKKAOUI <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2017
     */
    public function getSelectedLots()
    {
        return $this->hiddenSelectedValue->getValue();
    }
}
