<!--Debut Modal Choix formulaire-->
<div class="modal-choix-formulaire" style="display:none;">
    <com:AtexoValidationSummary  id="validSummaryInfosFormulaire" ValidationGroup="validateInfosFormulaire"  />
    <!--Debut message Erreur-->
    <com:TActivePanel ID="panelMessage" Style="display:none;" cssClass="form-bloc-erreur">
        <div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
        <div class="content">
            <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-erreur.gif" class="picto-erreur" alt="Erreur" />
            <div class="message bloc-700">
                <!--Debut Message type d'erreur general-->
                <span><com:TActiveLabel ID="labelMessage" Attributes.style="color:red"/></span>
                <!--Fin Message type d'erreur general-->
            </div>
            <div class="breaker"></div>
        </div>
        <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
        <com:TActiveLabel id="labelJavascript"/>
    </com:TActivePanel>
    <!--Fin message Erreur-->
    <!--Debut Formulaire-->
    <div class="form-field">
        <div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
        <div class="content">

            <div class="line">
                <div class="intitule-120"><com:TTranslate>DEFINE_CHOIX_SUPPORT</com:TTranslate> :</div>
                <div class="content-bloc bloc-600">
                    <ul class="liste-supports choix-supports">
                    <com:TActiveRepeater ID="listeSupportsPub" >
                        <prop:ItemTemplate>
                                <li><label>
                                <com:TActiveCheckBox id="support" Attributes.title="<%#$this->Data->getNom()%>" checked="<%#$this->SourceTemplateControl->cocherSupportPublication($this->Data->getId())%>" OnCallback="SourceTemplateControl.actionSelectionTypeAnnonceSupport">
                                    <prop:ClientSide.OnLoading>showLoader();</prop:ClientSide.OnLoading>
                                    <prop:ClientSide.OnFailure>showLoader();</prop:ClientSide.OnFailure>
                                    <prop:ClientSide.OnSuccess>showLoader();</prop:ClientSide.OnSuccess>
                                    <prop:ClientSide.RequestTimeOut>60000</prop:ClientSide.RequestTimeOut>
                                </com:TActiveCheckBox>
                                    <com:TActiveImage cssclass="icone" ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/<%#$this->Data->getImageLogo()%>" Attributes.alt="<%#$this->Data->getNom()%>" />
                                </label></li>
                                <com:THiddenField ID="idSupport" value="<%#base64_encode($this->Data->getId())%>"/>
                                <com:THiddenField ID="codeSupport" value="<%#base64_encode($this->Data->getCode())%>"/>
                        </prop:ItemTemplate>
                    </com:TActiveRepeater>
                    </ul>
                    <com:TCustomValidator
                            ValidationGroup="validateInfosFormulaire"
                            ControlToValidate="nbrSupports"
                            Display="Dynamic"
                            ClientValidationFunction="validerSelectionChoixSupportsPublicite"
                            ErrorMessage="<%=Prado::localize('MESSAGE_VOUS_DEVEZ_SELECTIONNER_AU_MOINS_UN_SUPPORT')%>"
                            EnableClientScript="true"
                            Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
                            >
                        <prop:ClientSide.OnValidationError>
                            document.getElementById('validSummaryInfosFormulaire').style.display='';
                        </prop:ClientSide.OnValidationError>
                    </com:TCustomValidator>
                    <com:THiddenField id="nbrSupports" value="<%=$this->getNbrSupports()%>"/>
                </div>
            </div>

            <div class="line">
                <div class="intitule-120"><label for="compteBOAMP"><com:TTranslate>MESSAGE_COMPTE_PUBLICITE</com:TTranslate></label> :</div>
                <com:TDropDownList id="compteBoamp" Attributes.title="<%=Prado::localize('MESSAGE_CHOIX_COMPTE_PUBLICITE')%>" cssClass="bloc-550"/>
                <com:TCompareValidator
                        ControlToValidate="compteBoamp"
                        ValidationGroup="validateInfosFormulaire"
                        ValueToCompare="0"
                        DataType="String"
                        Operator="NotEqual"
                        Display="Dynamic"
                        ErrorMessage="<%=Prado::localize('MESSAGE_CHOIX_COMPTE_PUBLICITE')%>"
                        EnableClientScript="true"
                        Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
                        >
                    <prop:ClientSide.OnValidationError>
                        document.getElementById('validSummaryInfosFormulaire').style.display='';
                    </prop:ClientSide.OnValidationError>
                </com:TCompareValidator>
            </div>
            <div class="line">
                <div class="intitule-120"><label for="typeAnnonce"><com:TTranslate>TEXT_TYPE_AVIS</com:TTranslate></label> :</div>
                <com:TActiveDropDownList id="typeAnnonce" Attributes.title="<%=Prado::localize('TEXT_TYPE_AVIS')%>" cssClass="bloc-550" onCallBack="actionSelectionTypeAnnonceSupport">
                    <prop:ClientSide.OnLoading>showLoader();</prop:ClientSide.OnLoading>
                    <prop:ClientSide.OnFailure>showLoader();</prop:ClientSide.OnFailure>
                    <prop:ClientSide.OnSuccess>showLoader();</prop:ClientSide.OnSuccess>
                    <prop:ClientSide.RequestTimeOut>60000</prop:ClientSide.RequestTimeOut>
                </com:TActiveDropDownList>
                <com:TCompareValidator
                        ControlToValidate="typeAnnonce"
                        ValidationGroup="validateInfosFormulaire"
                        ValueToCompare="0"
                        DataType="String"
                        Operator="NotEqual"
                        Display="Dynamic"
                        ErrorMessage="<%=Prado::localize('TEXT_TYPE_AVIS')%>"
                        EnableClientScript="true"
                        Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
                        >
                    <prop:ClientSide.OnValidationError>
                        document.getElementById('validSummaryInfosFormulaire').style.display='';
                    </prop:ClientSide.OnValidationError>
                </com:TCompareValidator>
            </div>
            <div class="line">
                <div class="intitule-120"><label for="formulaire"><com:TTranslate>DEFINE_CHOIX_FORMULAIRE</com:TTranslate></label> :</div>
                <com:TActiveDropDownList id="formulaire"  DataTextField="libelle" DataValueField="id" DataGroupField="typeFormulaire" Attributes.title="<%=Prado::localize('DEFINE_FORMULAIRE_SOUHAITE')%>" cssClass="bloc-550" />
                <com:TCompareValidator
                        ControlToValidate="formulaire"
                        ValidationGroup="validateInfosFormulaire"
                        ValueToCompare="0"
                        DataType="String"
                        Operator="NotEqual"
                        Display="Dynamic"
                        ErrorMessage="<%=Prado::localize('DEFINE_FORMULAIRE_SOUHAITE')%>"
                        EnableClientScript="true"
                        Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
                        >
                    <prop:ClientSide.OnValidationError>
                        document.getElementById('validSummaryInfosFormulaire').style.display='';
                    </prop:ClientSide.OnValidationError>
                </com:TCompareValidator>
            </div>
            <div class="breaker"></div>
        </div>
        <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
    </div>
    <!--Fin Formulaire-->
    <!--Debut line boutons-->
    <div class="boutons-line">
        <com:TActiveButton cssClass="bouton-moyen float-left" Text="<%=Prado::localize('DEFINE_ANNULER')%>" onCallBack="SourceTemplateControl.annuler">
            <prop:ClientSide.OnLoading>showLoader();</prop:ClientSide.OnLoading>
            <prop:ClientSide.OnFailure>showLoader();</prop:ClientSide.OnFailure>
            <prop:ClientSide.OnSuccess>showLoader();</prop:ClientSide.OnSuccess>
            <prop:ClientSide.RequestTimeOut>60000</prop:ClientSide.RequestTimeOut>
        </com:TActiveButton>
        <com:TActiveButton ValidationGroup="validateInfosFormulaire" cssClass="bouton-moyen float-right" Text="<%=Prado::localize('TEXT_VALIDER')%>" onCallBack="SourceTemplateControl.creerAnnoncePublicite" >
            <prop:ClientSide.OnLoading>showLoader();</prop:ClientSide.OnLoading>
            <prop:ClientSide.OnFailure>showLoader();</prop:ClientSide.OnFailure>
            <prop:ClientSide.OnSuccess>showLoader();</prop:ClientSide.OnSuccess>
            <prop:ClientSide.RequestTimeOut>60000</prop:ClientSide.RequestTimeOut>
        </com:TActiveButton>
    </div>
    <!--Fin line boutons-->
</div>
<!--Fin Modal Choix formulaire-->