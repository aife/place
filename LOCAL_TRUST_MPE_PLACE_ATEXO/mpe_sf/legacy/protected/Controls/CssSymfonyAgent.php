<?php

namespace Application\Controls;

use App\Service\Layout\Agent\Css2Prado;
use Application\Service\Atexo\Atexo_Util;

class CssSymfonyAgent extends MpeTTemplateControl
{
    public function render($writer)
    {
        /** @var Css2Prado $menu */
        $menu = Atexo_Util::getSfService(Css2Prado::class);

        echo $menu->render();
    }
}
