<!--Debut Bloc Message-->
<div class="form-field">
	<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
	<div class="content">
		<h3><com:TTranslate>TEXT_MESSAGE</com:TTranslate></h3>
		<div class="line">
			<div class="intitule-100 bold"><com:TTranslate>DEFINE_TEXT_DE</com:TTranslate> :</div><com:TLabel ID="expediteur"/>
		</div>
		<div class="line">
			<div class="intitule-100 bold"><com:TTranslate>DEFINE_TEXT_A</com:TTranslate> :</div><com:TLabel ID="destinataire"/>
		</div>
		<div class="line">
			<div class="intitule-100 bold"><com:TTranslate>DEFINE_TEXT_DATE_ENVOI</com:TTranslate> :</div><com:TLabel ID="datenvoi"/>
		</div>
		<div class="line">
			<div class="intitule-100 bold"><com:TTranslate>OBJET</com:TTranslate> :</div>
			<div class="content-bloc bloc-660"><com:TLabel ID="objet"/></div>
		</div>
		<div class="spacer-small"></div>
		<div class="line">
			<div class="intitule-100 bold"><com:TTranslate>TEXT_MESSAGE</com:TTranslate> :</div>
			<div class="content-bloc bloc-660">
				<com:TLabel ID="corps"/>
			</div>
		</div>
		<div class="spacer-small"></div>
		<div class="line">
			<div class="intitule-100 bold"><com:TTranslate>DEFINE_TEXT_PIECE_JOINTE</com:TTranslate> :</div>
			<div class="content-bloc bloc-660">

				<com:TRepeater ID="RepeaterPiecesJointes" EnableViewState="true" DataKeyField="Id">
					<prop:ItemTemplate>
						<div class="picto-link inline">
							<a href="<%#$this->Data->retrieveUrl($this->Parent->Parent->getCalledFrom(),$this->Parent->Parent->_acronymeOrg,$this->Parent->Parent->_numAccuseReception)%>"><%#$this->Data->getNomFichier()%><span class=info-aide> (<%# Application\Service\Atexo\Atexo_Util::arrondirSizeFile($this->Data->getTaille()/1024)%>)</span></a>
						</div>
						<br/>
					</prop:ItemTemplate>
				</com:TRepeater>
			</div>
		</div>
		<com:TPanel ID="textUpDV" >
			<div class="line">
				<div class="intitule-100 bold"><com:TTranslate>DEFINE_TEXT_PIECE_JOINTE_DV</com:TTranslate> :</div>
				<div class="content-bloc bloc-660"><br /><br />
					<com:TLabel ID="urlDossierVolumineux"/>
				</div>
			</div>
		</com:TPanel>
		<div class="breaker"></div>
		<com:TPanel Visible="false" ID="reponseMessage">
			<com:TButton
					CssClass="bouton-repondre-message float-right"
					Attributes.value="<%=Prado::localize('REPONSE_AU_MESSAGE')%>"
					Attributes.title="<%=Prado::localize('REPONSE_AU_MESSAGE')%>"
					OnClick="repondre"
			/>
		</com:TPanel>
	</div>
	<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
</div>
<!--Fin Bloc Liste Message-->