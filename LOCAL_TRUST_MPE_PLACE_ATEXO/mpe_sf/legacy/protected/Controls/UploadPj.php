<?php

namespace Application\Controls;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonAnnoncePressPeer;
use Application\Propel\Mpe\CommonAnnoncePressPieceJointe;
use Application\Propel\Mpe\CommonAnnoncePressPieceJointePeer;
use Application\Propel\Mpe\CommonEchangePieceJointe;
use Application\Propel\Mpe\CommonEchangePieceJointePeer;
use Application\Propel\Mpe\CommonNewsletterPeer;
use Application\Propel\Mpe\CommonNewsletterPieceJointe;
use Application\Propel\Mpe\CommonNewsletterPieceJointePeer;
use Application\Service\Atexo\AnnoncePress\Press;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_ScanAntivirus;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Newsletter\Atexo_Newsletter_Newsletter;
use Prado\Prado;

/**
 * Classe UploadPj.
 *
 * @author SBAI Yassine <yassine.sbai@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class UploadPj extends MpeTPage
{
    public $_ID;
    public $_org;
    public $_type;
    public bool $newsLetter = false;
    public ?string $maxSize = null;
    public ?int $sommeTaille = null;

    public function onLoad($param)
    {
        $this->panelMessageErreur->setVisible(false);
        $this->maxSize = Atexo_Config::getParameter('ATACHEMENT_MAX_UPLOAD_FILE_SIZE');
        if ($this->newsLetter) {
            $this->maxSize = Atexo_Config::getParameter('ATTACHEMENT_MAX_UPLOAD_NEWSLETTER_SIZE');
        }
        $this->ajoutFichier->setFileSizeMax($this->maxSize);
        if (!$this->IsPostBack) {
            $this->displayRepeaterPJ();
        }
    }

    public function remplirTableauPJ($data)
    {
        $this->RepeaterPiecesJointes->dataSource = $data;
        $this->RepeaterPiecesJointes->dataBind();
    }

    public function onAjoutClick($sender, $param)
    {
        if (($this->ajoutFichier->HasFile) || ('0' == $this->hasFile->Value) ||
            ($this->ajoutFichier->FileSize > $this->maxSize) ||
            ((bool) $this->hasFile->Value || 0 == $this->hasFile->Value)
        ) {
            if (!$this->isTailleFichiersDepasse($this->ajoutFichier->FileSize, $this->sommeTaille)) {
                return;
            }
            //commencer le scan Antivirus
            $msg = Atexo_ScanAntivirus::startScan($this->ajoutFichier->LocalName);
            if ($msg) {
                $this->afficherErreur($msg.'"'.$this->ajoutFichier->FileName.'"');

                return;
            }
            //Fin du code scan Antivirus
            $infilepj = Atexo_Config::getParameter('COMMON_TMP').'pj'.session_id().time();
            if (move_uploaded_file($this->ajoutFichier->LocalName, $infilepj)) {
                $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                $atexoBlob = new Atexo_Blob();
                $pjIdBlob = $atexoBlob->insert_blob($this->ajoutFichier->FileName, $infilepj, $this->_org);
                //ajout piece jointe
                $c = new Criteria();
                $PJs = [];
                if (isset($_GET['type']) && ($_GET['type'] == Atexo_Config::getParameter('TYPE_PRESS'))) {
                    $ObjetMessage = CommonAnnoncePressPeer::retrieveByPK(Atexo_Util::atexoHtmlEntities($_GET['Idmsg']), Atexo_Util::atexoHtmlEntities($_GET['org']), $connexionCom);
                    if ($ObjetMessage) {
                        $c->add(CommonAnnoncePressPieceJointePeer::ORGANISME, $this->_org);
                        $PJs = $ObjetMessage->getCommonAnnoncePressPieceJointes($c, $connexionCom);
                    }
                    $Pj = new CommonAnnoncePressPieceJointe();
                    $Pj->setNomFichier($this->ajoutFichier->FileName);
                    $Pj->setPiece($pjIdBlob);

                    $Pj->setIdAnnoncePress($this->_ID);
                    $Pj->setTaille($atexoBlob->getTailFile($pjIdBlob, $this->_org));
                    $Pj->setOrganisme($this->_org);

                    if ($ObjetMessage) {
                        $ObjetMessage->addCommonAnnoncePressPieceJointe($Pj);
                        if ($ObjetMessage->save($connexionCom)) {
                            $PJs[] = $Pj;
                        }
                    }
                } elseif (isset($_GET['type']) && ($_GET['type'] == Atexo_Config::getParameter('TYPE_NEWSLETTER'))) {
                    $ObjetMessage = CommonNewsletterPeer::retrieveByPK(Atexo_Util::atexoHtmlEntities($_GET['Idmsg']), Atexo_Util::atexoHtmlEntities($_GET['org']), $connexionCom);
                    if ($ObjetMessage) {
                        $c->add(CommonNewsletterPieceJointePeer::ORGANISME, $this->_org);
                        $PJs = $ObjetMessage->getCommonNewsletterPieceJointes($c, $connexionCom);
                    }
                    $Pj = new CommonNewsletterPieceJointe();
                    $Pj->setNomFichier($this->ajoutFichier->FileName);
                    $Pj->setPiece($pjIdBlob);
                    $Pj->setIdNewsletter($this->_ID);
                    $Pj->setTaille($atexoBlob->getTailFile($pjIdBlob, $this->_org));
                    $Pj->setOrganisme($this->_org);
                    if ($ObjetMessage) {
                        $ObjetMessage->addCommonNewsletterPieceJointe($Pj);
                        if ($ObjetMessage->save($connexionCom)) {
                            $PJs[] = $Pj;
                        }
                    }
                } else {
                    $ObjetMessage = Atexo_Message::retrieveMessageById($this->_ID, $this->_org);
                    if ($ObjetMessage) {
                        $c->add(CommonEchangePieceJointePeer::ORGANISME, $this->_org);
                        $PJs = $ObjetMessage->getCommonEchangePieceJointes($c, $connexionCom);
                    }
                    $Pj = new CommonEchangePieceJointe();

                    $Pj->setNomFichier($this->ajoutFichier->FileName);
                    $Pj->setExtension(Atexo_Util::getExtension($this->ajoutFichier->FileName));
                    $Pj->setPiece($pjIdBlob);

                    $Pj->setIdMessage($this->_ID);
                    $Pj->setTaille($atexoBlob->getTailFile($pjIdBlob, $this->_org));
                    $Pj->setOrganisme($this->_org);
                    if ($ObjetMessage) {
                        $ObjetMessage->addCommonEchangePieceJointe($Pj);
                        if ($ObjetMessage->save($connexionCom)) {
                            $PJs[] = $Pj;
                        }
                    }
                }
                $this->remplirTableauPJ($PJs);
            }
        } else {
            $this->panelMessageErreur->setVisible(true);
            $this->panelMessageErreur->setMessage(Prado::localize('TEXT_TAILLE_AUTORISEE_DOC', [
                'size' => Atexo_Util::arrondirSizeFile(($this->maxSize / 1024)), ]));
        }
    }

    public function onDeleteClick($sender, $param)
    {
        $idpj = $param->CommandParameter;
        if (isset($_GET['type']) && ($_GET['type'] == Atexo_Config::getParameter('TYPE_PRESS'))) {
            Press::DeletePjAnnoncePress($idpj, $this->_org);
        } elseif (isset($_GET['type']) && ($_GET['type'] == Atexo_Config::getParameter('TYPE_NEWSLETTER'))) {
            Atexo_Newsletter_Newsletter::DeletePj($idpj, $this->_org);
        } else {
            Atexo_Message::DeletePj($idpj, $this->_org);
        }
    }

    public function refreshRepeaterPJ($sender, $param)
    {
        //Raffraichissement du tableau PJ:
        $this->displayRepeaterPJ();
        $this->PanelPJ->render($param->NewWriter);
    }

    public function displayRepeaterPJ()
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $c = new Criteria();
        $PJ = [];
        if (isset($_GET['type']) && ($_GET['type'] == Atexo_Config::getParameter('TYPE_PRESS'))) {
            $ObjetMessage = CommonAnnoncePressPeer::retrieveByPK(Atexo_Util::atexoHtmlEntities($_GET['Idmsg']), Atexo_Util::atexoHtmlEntities($_GET['org']), $connexionCom);
            if ($ObjetMessage) {
                $c->add(CommonAnnoncePressPieceJointePeer::ORGANISME, $this->_org);
                $PJs = $ObjetMessage->getCommonAnnoncePressPieceJointes($c, $connexionCom);
                if (is_array($PJs) && 0 != count($PJs)) {
                    foreach ($PJs as $PJ) {
                        if ($PJ->getPiece()) {
                            $PJ->setTaille((new Atexo_Blob())->getTailFile($PJ->getPiece(), $this->_org));
                        }
                    }
                }
            }
        } elseif (isset($_GET['type']) && ($_GET['type'] == Atexo_Config::getParameter('TYPE_NEWSLETTER'))) {
            $ObjetMessage = CommonNewsletterPeer::retrieveByPK(Atexo_Util::atexoHtmlEntities($_GET['Idmsg']), Atexo_Util::atexoHtmlEntities($_GET['org']), $connexionCom);
            if ($ObjetMessage) {
                $c->add(CommonNewsletterPieceJointePeer::ORGANISME, $this->_org);
                $PJs = $ObjetMessage->getCommonNewsletterPieceJointes($c, $connexionCom);
                if (is_array($PJs) && 0 != count($PJs)) {
                    $this->sommeTaille = 0;
                    foreach ($PJs as $PJ) {
                        $this->sommeTaille += (new Atexo_Blob())->getTailFile($PJ->getPiece(), $this->_org);
                        $PJ->setTaille((new Atexo_Blob())->getTailFile($PJ->getPiece(), $this->_org));
                    }
                }
            }
        } else {
            $ObjetMessage = Atexo_Message::retrieveMessageById($this->_ID, $this->_org);
            if ($ObjetMessage) {
                $c->add(CommonEchangePieceJointePeer::ORGANISME, $this->_org);
                $PJs = $ObjetMessage->getCommonEchangePieceJointes($c, $connexionCom);
                if (is_array($PJs) && 0 != count($PJs)) {
                    foreach ($PJs as $PJ) {
                        $PJ->setTaille((new Atexo_Blob())->getTailFile($PJ->getPiece(), $this->_org));
                    }
                }
            }
        }
        $this->remplirTableauPJ($PJs);
    }

    public function afficherErreur($msg)
    {
        $this->panelMessageErreur->setMessage($msg);
        $this->panelMessageErreur->setVisible(true);
    }

    public function getVisibiliteBoutonSupprimer($objetPj)
    {
        if (Atexo_Module::isEnabled('PubliciteOpoce') && (($objetPj instanceof CommonAnnoncePressPieceJointe) && $objetPj->getFichierGenere())) {
            return false;
        } else {
            return true;
        }
    }

    public function getLienTelechargementPj($objetPj)
    {
        if (isset($_GET['idCom'])) {
            return '?page=Commission.DownloadPjEchangeCommission&idPj='.$objetPj->getPiece().'&org='.$objetPj->getOrganisme().'&idCom='.Atexo_Util::atexoHtmlEntities($_GET['idCom']);
        }

        return '?page=Agent.DownloadPjEchange&idPj='.$objetPj->getPiece().'&org='.$objetPj->getOrganisme().
        '&id='.Atexo_Util::atexoHtmlEntities($_GET['id']).'&idAvis='.Atexo_Util::atexoHtmlEntities($_GET['idAvis']);
    }

    /*
     * Test si un la somme des tailles des fichiers joints
     * si il depasse la taille definit dans le parametrage
     * @param $tailleFichier en cours d'ajout
     * @param $sommeTaille la somme des anciens fichiers (passé par reference)
     * return boolean
     */
    public function isTailleFichiersDepasse($tailleFichier, &$sommeTaille)
    {
        if (isset($_GET['type']) && ($_GET['type'] == Atexo_Config::getParameter('TYPE_NEWSLETTER'))) {
            $sommeTaille = $sommeTaille + $tailleFichier;
            if ($sommeTaille > Atexo_Config::getParameter('ATTACHEMENT_MAX_UPLOAD_NEWSLETTER_SIZE_ALL_PIECES')) {
                $this->panelMessageErreur->setVisible(true);
                $this->panelMessageErreur->setMessage(Prado::localize('TEXT_POIDS_SOMME_PIECES_JOINTS', [
                'size' => Atexo_Util::arrondirSizeFile((Atexo_Config::getParameter('ATTACHEMENT_MAX_UPLOAD_NEWSLETTER_SIZE_ALL_PIECES') / 1024)), ]));

                return false;
            }
        }

        return true;
    }

    /**
     * Permet de formatter le nom du fichier.
     *
     * @param CommonEchangePieceJointe $file : objet fichier
     *
     * @return string : nom du fichier
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function formaterNomFichierPj($file)
    {
        if ($file instanceof CommonEchangePieceJointe) {
            return $file->getNomFichierFormate();
        }
    }
}
