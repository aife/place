			<com:TActivePanel ID="PanelListeCriteres" >	
				<div class="form-field">
					<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
					<div class="content">
						<h3><com:TTranslate>DEFINE_CRITERE_EVALUATION_DEFINIS_PAR_ACHETEURS</com:TTranslate></h3>

						<table class="table-results tableau-gestion-doc-prepa" summary="Liste des Criteres">
							<thead>
								<tr>
									<th id="formCand_col_01"><com:TTranslate>TEXT_TYPE_FORMULAIRE</com:TTranslate></th>
									<th id="formCand_col_02" class="col-180"><com:TTranslate>DEFINE_TEXT_REFERENCE</com:TTranslate></th>
									<th id="formCand_col_03" class="col-50 center"><com:TTranslate>DEFINE_STATUS</com:TTranslate></th>
									<th id="formCand_col_04" class="actions"><com:TTranslate>DEFINE_ACTIONS</com:TTranslate></th>

								</tr>
							</thead>
							<tr style="display:<%=(count($this->repeaterFormsCand->DataSource)>0)?'':'none'%>">
								<td headers="formCand_col_01" colspan="4" class="title"><com:TTranslate>CANDIDATURE</com:TTranslate></td>
							</tr>
								<com:TRepeater ID="repeaterFormsCand">
    									<prop:ItemTemplate>
    											<tr class=<%#(($this->ItemIndex%2==0)? '':'on')%>>
													<td headers="formCand_col_01" class="indent-20"><%#$this->Data->getLibelleTypeCritere()%></td>
													<td headers="formCand_col_02" class="col-180"></td>									
					
													<td headers="formCand_col_03" class="col-50 center"><img src="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/<%#$this->TemplateControl->getPictoStatut($this->Data)%>" alt="<%=Prado::localize('DEFINE_STATUS')%> : <%#$this->TemplateControl->getTitleStatut($this->Data)%>" title="<%=Prado::localize('DEFINE_STATUS')%> : <%#$this->TemplateControl->getTitleStatut($this->Data)%>" class="statut" /></td>
													<td headers="formCand_col_04" class="actions">
														<a href="#" title="<%=Prado::localize('TEXT_EDITER_CRITERES_EVALUATION')%>" onclick="javascript:popUp('index.php?page=Agent.PopUpCriteresEvaluation&id=<%#$this->Data->getId()%>','yes');">
															<img src="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-edit.gif" alt="<%=Prado::localize('TEXT_EDITER_CRITERES_EVALUATION')%>" />
														</a>
														<a href="<%#$this->TemplateControl->getUrlVisualisation($this->Data)%>">
															<img src="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-details.gif" alt="<%=Prado::localize('DEFINE_SIMULATION_GRILLE_NOTATION')%>" title="<%=Prado::localize('DEFINE_SIMULATION_GRILLE_NOTATION')%>" />
														</a>
													</td>
												</tr>
										</prop:ItemTemplate>
									</com:TRepeater>
							<tr>
							<com:TRepeater ID="repeaterForms">
							<prop:ItemTemplate>
									<tr style="display:<%#($this->TemplateControl->afficherLigneLot($this->Data->getLot()))?'':'none'%>">
										<td headers="formCand_col_01" colspan="4" class="title"><%#$this->TemplateControl->getLibelleLot($this->Data->getLot())%></td>
									</tr>
									<tr class="<%#($this->Data->getLot() != '0' )?'on':'title'%>" style="display:<%#($this->TemplateControl->afficherLigneEnveloppe($this->Data->getTypeEnveloppe(), $this->Data->getLot()))?'':'none'%>">
										<td headers="formCand_col_01" class="indent-10" colspan="4"><%#$this->Data->getLibelleTypeEnveloppe()%></td>
									</tr>
									<tr>
										<td headers="formCand_col_01" class="indent-20"><%#$this->Data->getLibelleTypeCritere()%></td>
										<td headers="formCand_col_02" class="col-180"></td>
		
										<td headers="formCand_col_03" class="col-50 center"><img src="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/<%#$this->TemplateControl->getPictoStatut($this->Data)%>" alt="<%=Prado::localize('DEFINE_STATUS')%> : <%#$this->TemplateControl->getTitleStatut($this->Data)%>" title="<%=Prado::localize('DEFINE_STATUS')%> : <%#$this->TemplateControl->getTitleStatut($this->Data)%>" class="statut" /></td>
										<td headers="formCand_col_04" class="actions">
											<a href="#" title="<%=Prado::localize('TEXT_EDITER_CRITERES_EVALUATION')%>" onclick="javascript:popUp('index.php?page=Agent.PopUpCriteresEvaluation&id=<%#$this->Data->getId()%>','yes');">
												<img src="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-edit.gif" alt="<%=Prado::localize('TEXT_EDITER_CRITERES_EVALUATION')%>" />
											</a>
											<a href="<%#$this->TemplateControl->getUrlVisualisation($this->Data)%>">
												<img src="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-details.gif" alt="<%=Prado::localize('DEFINE_SIMULATION_GRILLE_NOTATION')%>" title="<%=Prado::localize('DEFINE_SIMULATION_GRILLE_NOTATION')%>" />
											</a>
										</td>
									</tr>
							</prop:ItemTemplate>
							</com:TRepeater>
						</table>
						<div class="breaker"></div>
					</div>
					<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
				</div>
			</com:TActivePanel>		
		<span style="display:none;">
			<com:TActiveButton id="refreshPanel" onCallBack="refreshRepeater"/>	
		</span>		
				<!--Fin Bloc formulaires-->
