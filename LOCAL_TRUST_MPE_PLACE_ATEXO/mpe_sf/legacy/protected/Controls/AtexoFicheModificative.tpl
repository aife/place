<!-- Debut layer 5 -->
<div class="ongletLayer" id="ongletLayer5" style="display:none;" >
    <div class="top"></div>
    <div id="ctl0_CONTENU_PAGE_panelDonneesCandidats" class="content">

        <!--Debut message infos-->
        <div class="bloc-message form-bloc-conf msg-info">
            <div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
            <div class="content">
                <div class="message bloc-700">
                    <p><com:TTranslate>DEFINE_TEXT_INFO_FICHE_MODIFICATIVE_1</com:TTranslate><br /><br /><com:TTranslate>DEFINE_TEXT_INFO_FICHE_MODIFICATIVE_2</com:TTranslate>.</p>
                </div>
                <div class="breaker"></div>
            </div>
            <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
        </div>
        <!--Fin  message infos-->

        <div class="form-field" style="display:block;">
            <div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><span class="title">Références</span></div>
            <div class="content">
                <div class="line">
                    <div class="intitule-bloc intitule-180"><label for="numInterneContrat"><com:TTranslate>TEXT_NUMERO_INTERNE_CONTRAT</com:TTranslate> </label> :</div>
                    <div class="content-bloc">
                        <com:TActiveTextBox
                            id="numInterneContrat"
                            cssClass="bloc-450"
                            Attributes.title="<%=Prado::localize('TEXT_NUMERO_INTERNE_CONTRAT')%>"
                            enabled="false"
                        />
                    </div>
                </div>
                <div class="line">
                    <div class="intitule-bloc intitule-180"><label for="numEJ"><com:TTranslate>TEXT_NUMERO_EJ_CHORUS</com:TTranslate> </label> :</div>
                    <div class="content-bloc">
                        <com:TActiveTextBox
                            id="numEJ"
                            cssClass="bloc-450"
                            Attributes.title="<%=Prado::localize('TEXT_NUMERO_EJ_CHORUS')%>"
                            enabled="false"
                        />
                    </div>
                </div>
                <div class="breaker"></div>
            </div>
            <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
        </div>


        <div class="form-field" style="display:block;">
            <div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><span class="title"><com:TTranslate>TEXT_DONNEES_ACTE_CONTRACTUEL_MODIFICATIF</com:TTranslate></span></div>
            <div class="content">
                <div class="spacer-small"></div>
                <div class="line">
                    <div class="intitule-180"><label for="<%$this->typeModification->ClientId%>"><com:TTranslate>TEXT_TYPE_MODIFICATION</com:TTranslate> <span class="champ-oblig">*</span></label> :</div>

                    <com:TDropDownList
                        id="typeModification"
                        cssClass="bloc-450"
                        Attributes.title="<%=Prado::localize('TEXT_TYPE_MODIFICATION')%>"
                    />
                    <com:TCompareValidator
                        ValidationGroup="validationGroup"
                        ControlToValidate="typeModification"
                        Enabled="<%=$this->TemplateControl->isTypeEnvoiFicheModificative()%>"
                        ValueToCompare="0"
                        DataType="String"
                        Operator="NotEqual"
                        Display="Dynamic"
                        ErrorMessage="<%=Prado::localize('TEXT_TYPE_MODIFICATION')%>"
                        EnableClientScript="true"
                        Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
                    >
                        <prop:ClientSide.OnValidationError>
                            document.getElementById('divValidationSummary').style.display='';
                            document.getElementById('ctl0_CONTENU_PAGE_ButtonSendToChorus').style.display='';
                            document.getElementById('ctl0_CONTENU_PAGE_saveInfoTransmission').style.display='';
                            document.getElementById('ctl0_CONTENU_PAGE_buttonAnnuler').style.display='';
                        </prop:ClientSide.OnValidationError>
                    </com:TCompareValidator>


                </div>
                <div class="line">
                    <div class="intitule-180 line-height-normal"><label for="datePrevNotificationActeModif"><com:TTranslate>DATE_PREVISIONNELLE_NOTIFICATION_ACTE_MODIFICATIF</com:TTranslate><span class="champ-oblig">*</span></label> :</div>
                    <div class="calendar">
                        <com:TTextBox
                            id="datePrevNotificationActeModif"
                            Attributes.title="<%=Prado::localize('DATE_PREVISIONNELLE_NOTIFICATION')%>"
                        />
                        <com:TImage
                            id="calendrierDatePrevNotificationActeModif"
                            ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-calendar.gif"
                            Attributes.alt="<%=Prado::localize('DEFINE_CALENDRIER')%>"
                            Attributes.title="<%=Prado::localize('DEFINE_CALENDRIER')%>"
                            Attributes.onclick="popUpCalendar(this,document.getElementById('ctl0_CONTENU_PAGE_FicheModificative_datePrevNotificationActeModif'),'dd/mm/yyyy','');"
                        />
                    </div>
                    <div class="info-aide-right"><%=Prado::localize('DEFINE_FORMAT_DATE_SIMPLE')%></div>
                    <com:TRequiredFieldValidator
                            ValidationGroup="validationGroup"
                            ControlToValidate="datePrevNotificationActeModif"
                            Enabled="<%=$this->TemplateControl->isTypeEnvoiFicheModificative()%>"
                            ErrorMessage="<%=Prado::localize('DATE_PREVISIONNELLE_NOTIFICATION')%>"
                            EnableClientScript="true"
                            Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
                            >
                        <prop:ClientSide.OnValidationError>
                            document.getElementById('divValidationSummary').style.display='';
                            document.getElementById('ctl0_CONTENU_PAGE_ButtonSendToChorus').style.display='';
                            document.getElementById('ctl0_CONTENU_PAGE_saveInfoTransmission').style.display='';
                            document.getElementById('ctl0_CONTENU_PAGE_buttonAnnuler').style.display='';
                        </prop:ClientSide.OnValidationError>
                    </com:TRequiredFieldValidator>
                    <com:TDataTypeValidator
                            ValidationGroup="validationGroup"
                            ControlToValidate="datePrevNotificationActeModif"
                            Enabled="<%=$this->TemplateControl->isTypeEnvoiFicheModificative()%>"
                            DataType="Date"
                            Display="Dynamic"
                            DateFormat="dd/MM/yyyy"
                            ErrorMessage="<%=Prado::localize('DATE_PREVISIONNELLE_NOTIFICATION')%>"
                            Visible="true"
                            EnableClientScript="true"
                            Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
                    >
                        <prop:ClientSide.OnValidationError>
                            document.getElementById('divValidationSummary').style.display='';
                            document.getElementById('ctl0_CONTENU_PAGE_ButtonSendToChorus').style.display='';
                            document.getElementById('ctl0_CONTENU_PAGE_saveInfoTransmission').style.display='';
                            document.getElementById('ctl0_CONTENU_PAGE_buttonAnnuler').style.display='';
                        </prop:ClientSide.OnValidationError>
                    </com:TDataTypeValidator>

                </div>
                <div class="line">
                    <div class="intitule-180"><label for="dateFinMarcheActeModif"><com:TTranslate>DATE_FIN_MARCHE</com:TTranslate></label> :</div>
                    <div class="calendar">
                        <com:TTextBox
                            id="dateFinMarcheActeModif"
                            Attributes.title="<%=Prado::localize('DATE_FIN_MARCHE')%>"
                            enabled="false"
                        />
                        <com:TImage
                            id="calendrierDateFinMarcheActeModif"
                            ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-calendar.gif"
                            Attributes.alt="<%=Prado::localize('DEFINE_CALENDRIER')%>"
                            Attributes.title="<%=Prado::localize('DEFINE_CALENDRIER')%>"
                        />
                    </div>
                    <div class="info-aide-right"><%=Prado::localize('DEFINE_FORMAT_DATE_SIMPLE')%></div>
                    <com:TCompareValidator
                            ValidationGroup="validationGroup"
                            ControlToValidate="dateFinMarcheActeModif"
                            Enabled="<%=$this->TemplateControl->isTypeEnvoiFicheModificative()%>"
                            DataType="Date"
                            Display="Dynamic"
                            DateFormat="dd/MM/yyyy"
                            ErrorMessage="<%=Prado::localize('DATE_FIN_MARCHE')%>"
                            EnableClientScript="true"
                            Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
                            >
                        <prop:ClientSide.OnValidationError>
                            document.getElementById('divValidationSummary').style.display='';
                            document.getElementById('ctl0_CONTENU_PAGE_ButtonSendToChorus').style.display='';
                            document.getElementById('ctl0_CONTENU_PAGE_saveInfoTransmission').style.display='';
                            document.getElementById('ctl0_CONTENU_PAGE_buttonAnnuler').style.display='';
                        </prop:ClientSide.OnValidationError>
                    </com:TCompareValidator>
                </div>
                <div class="line">
                    <div class="intitule-180 line-height-normal"><com:TTranslate>TEXT_DATE_FIN_MARCHE_ETE_MODIFIEE</com:TTranslate> :</div>
                    <div class="content-bloc">
                        <div class="intitule-auto intitule-80">
                            <com:TRadioButton
                                id="dateFinMarcheModifOui"
                                cssClass="radio"
                                Attributes.title="<%=Prado::localize('TEXT_OUI')%>"
                                GroupName="DateFinMarcheModif"
                                Attributes.onclick="showDiv('layer_dateFinMarcheModif_Oui');"
                            />
                            <label for="dateFinMarcheModifOui"><com:TTranslate>TEXT_OUI</com:TTranslate></label>
                        </div>
                        <div class="intitule-auto intitule-60">
                            <com:TRadioButton
                                id="dateFinMarcheModifNon"
                                cssClass="radio"
                                Attributes.title="<%=Prado::localize('TEXT_NON')%>"
                                GroupName="DateFinMarcheModif"
                                Attributes.onclick="hideDiv('layer_dateFinMarcheModif_Oui');"
                            />
                            <label for="dateFinMarcheModifNon"><com:TTranslate>TEXT_NON</com:TTranslate></label>
                        </div>
                        <com:TCustomValidator
                                ValidationGroup="validationGroup"
                                ControlToValidate="dateFinMarcheModifNon"
                                Enabled="<%=$this->TemplateControl->isTypeEnvoiFicheModificative()%>"
                                Display="Dynamic"
                                ErrorMessage="<%=Prado::localize('TEXT_DATE_FIN_MARCHE_ETE_MODIFIEE')%>"
                                ClientValidationFunction="ValiderRadioBoutonsDateFinModifiee"
                                Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>" >
                            <prop:ClientSide.OnValidationError>
                                document.getElementById('divValidationSummary').style.display='';
                                document.getElementById('ctl0_CONTENU_PAGE_ButtonSendToChorus').style.display='';
                                document.getElementById('ctl0_CONTENU_PAGE_saveInfoTransmission').style.display='';
                                document.getElementById('ctl0_CONTENU_PAGE_buttonAnnuler').style.display='';
                            </prop:ClientSide.OnValidationError>
                        </com:TCustomValidator>
                    </div>
                </div>
                <div class="line" id="layer_dateFinMarcheModif_Oui" style="display:none;">
                    <div class="intitule-180"><label for="dateFinMarcheModifiee"><com:TTranslate>DATE_FIN_MARCHE_MODIFIE</com:TTranslate></label><span class="champ-oblig">*</span> :</div>
                    <div class="calendar">
                        <com:TTextBox
                            id="dateFinMarcheModifiee"
                            Attributes.title="<%=Prado::localize('TEXT_DATE_FIN_MARCHE_MODIFIEE')%>"
                        />
                        <com:TImage
                            id="calendrierDateFinMarcheModifiee"
                            ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-calendar.gif"
                            Attributes.alt="<%=Prado::localize('DEFINE_CALENDRIER')%>"
                            Attributes.title="<%=Prado::localize('DEFINE_CALENDRIER')%>"
                            Attributes.onclick="popUpCalendar(this,document.getElementById('ctl0_CONTENU_PAGE_FicheModificative_dateFinMarcheModifiee'),'dd/mm/yyyy','');"
                        />
                        <com:TCustomValidator
                                ValidationGroup="validationGroup"
                                ControlToValidate="dateFinMarcheModifiee"
                                Enabled="<%=$this->TemplateControl->isTypeEnvoiFicheModificative()%>"
                                Display="Dynamic"
                                ErrorMessage="<%=Prado::localize('TEXT_DATE_FIN_MARCHE_MODIFIEE')%>"
                                ClientValidationFunction="validerDateFinMarcheModifie"
                                Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>" >
                            <prop:ClientSide.OnValidationError>
                                document.getElementById('divValidationSummary').style.display='';
                                document.getElementById('ctl0_CONTENU_PAGE_ButtonSendToChorus').style.display='';
                                document.getElementById('ctl0_CONTENU_PAGE_saveInfoTransmission').style.display='';
                                document.getElementById('ctl0_CONTENU_PAGE_buttonAnnuler').style.display='';
                            </prop:ClientSide.OnValidationError>
                        </com:TCustomValidator>
                        <com:TCompareValidator
                                ValidationGroup="validationGroup"
                                ControlToValidate="dateFinMarcheModifiee"
                                valuetocompare="<%=$this->getContratObjet() instanceof Application\Propel\Mpe\CommonTContratTitulaire ? $this->getContratObjet()->getDateNotification() : '' %>"
                                Enabled="<%=$this->TemplateControl->isTypeEnvoiFicheModificative()%>"
                                DataType="Date"
                                Display="Dynamic"
                                Operator="GreaterThan"
                                DateFormat="dd/MM/yyyy"
                                ErrorMessage="<%=Prado::localize('TEXT_DATE_FIN_MARCHE_MODIFIEE')%>"
                                EnableClientScript="true"
                                Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
                                >
                            <prop:ClientSide.OnValidationError>
                                document.getElementById('divValidationSummary').style.display='';
                                document.getElementById('ctl0_CONTENU_PAGE_ButtonSendToChorus').style.display='';
                                document.getElementById('ctl0_CONTENU_PAGE_saveInfoTransmission').style.display='';
                                document.getElementById('ctl0_CONTENU_PAGE_buttonAnnuler').style.display='';
                            </prop:ClientSide.OnValidationError>
                        </com:TCompareValidator>
                    </div>
                    <div class="info-aide-right"><%=Prado::localize('DEFINE_FORMAT_DATE_SIMPLE')%></div>
                </div>
                <div class="line">
                    <div class="intitule-180"><label for="montantMarche"><com:TTranslate>MONTANT_MARCHE</com:TTranslate><span class="champ-oblig">*</span></label> :</div>
                    <div class="content-bloc">
                        <com:TTextBox
                            id="montantMarche"
                            cssClass="float-left montant montant-formatter"
                            Attributes.title="<%=Prado::localize('MONTANT_MARCHE_HT_EURO')%>"
                            enabled="false"
                        />
                        <com:TRequiredFieldValidator
                                ValidationGroup="validationGroup"
                                ControlToValidate="montantMarche"
                                Enabled="<%=$this->TemplateControl->isTypeEnvoiFicheModificative()%>"
                                ErrorMessage="<%=Prado::localize('MONTANT_MARCHE_HT_EURO')%>"
                                EnableClientScript="true"
                                Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
                                >
                            <prop:ClientSide.OnValidationError>
                                document.getElementById('divValidationSummary').style.display='';
                                document.getElementById('ctl0_CONTENU_PAGE_ButtonSendToChorus').style.display='';
                                document.getElementById('ctl0_CONTENU_PAGE_saveInfoTransmission').style.display='';
                                document.getElementById('ctl0_CONTENU_PAGE_buttonAnnuler').style.display='';
                            </prop:ClientSide.OnValidationError>
                        </com:TRequiredFieldValidator>
                        <com:TDataTypeValidator
                                ValidationGroup="validationGroup"
                                ControlToValidate="montantMarche"
                                Enabled="<%=$this->TemplateControl->isTypeEnvoiFicheModificative()%>"
                                ErrorMessage="<%=Prado::localize('MONTANT_MARCHE_HT_EURO')%>"
                                EnableClientScript="true"
                                Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
                                >
                            <prop:ClientSide.OnValidationError>
                                document.getElementById('divValidationSummary').style.display='';
                                document.getElementById('ctl0_CONTENU_PAGE_ButtonSendToChorus').style.display='';
                                document.getElementById('ctl0_CONTENU_PAGE_saveInfoTransmission').style.display='';
                                document.getElementById('ctl0_CONTENU_PAGE_buttonAnnuler').style.display='';
                            </prop:ClientSide.OnValidationError>
                        </com:TDataTypeValidator>
                        <div class="intitule-bloc"><com:TTranslate>DEFINE_EURO_HT</com:TTranslate></div>
                    </div>
                </div>
                <div class="line">
                    <div class="intitule-180"><label for="montantActe"><com:TTranslate>MONTANT_ACTE</com:TTranslate><span class="champ-oblig">*</span></label> :</div>
                    <div class="content-bloc">
                        <com:TTextBox
                            id="montantActe"
                            cssClass="float-left montant montant-formatter"
                            Attributes.title="<%=Prado::localize('MONTANT_ACTE')%>"
                            Attributes.onblur="formatterMontant('<%=$this->montantActe->getClientId()%>');"
                        />
                        <com:TCustomValidator
                                ValidationGroup="validationGroup"
                                ControlToValidate="montantActe"
                                Enabled="<%=$this->TemplateControl->isTypeEnvoiFicheModificative()%>"
                                Display="Dynamic"
                                ErrorMessage="<%=Prado::localize('MONTANT_ACTE')%>"
                                ClientValidationFunction="validerMontantActeFicheModificativeChorus"
                                Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>" >
                            <prop:ClientSide.OnValidationError>
                                document.getElementById('divValidationSummary').style.display='';
                                document.getElementById('ctl0_CONTENU_PAGE_ButtonSendToChorus').style.display='';
                                document.getElementById('ctl0_CONTENU_PAGE_saveInfoTransmission').style.display='';
                                document.getElementById('ctl0_CONTENU_PAGE_buttonAnnuler').style.display='';
                            </prop:ClientSide.OnValidationError>
                        </com:TCustomValidator>
                        <com:TRequiredFieldValidator
                                ValidationGroup="validationGroup"
                                ControlToValidate="montantActe"
                                Enabled="<%=$this->TemplateControl->isTypeEnvoiFicheModificative()%>"
                                ErrorMessage="<%=Prado::localize('MONTANT_ACTE')%>"
                                EnableClientScript="true"
                                Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
                                >
                            <prop:ClientSide.OnValidationError>
                                document.getElementById('divValidationSummary').style.display='';
                                document.getElementById('ctl0_CONTENU_PAGE_ButtonSendToChorus').style.display='';
                                document.getElementById('ctl0_CONTENU_PAGE_saveInfoTransmission').style.display='';
                                document.getElementById('ctl0_CONTENU_PAGE_buttonAnnuler').style.display='';
                            </prop:ClientSide.OnValidationError>
                        </com:TRequiredFieldValidator>
                        <com:TDataTypeValidator
                                ValidationGroup="validationGroup"
                                ControlToValidate="montantActe"
                                Enabled="<%=$this->TemplateControl->isTypeEnvoiFicheModificative()%>"
                                ErrorMessage="<%=Prado::localize('MONTANT_ACTE')%>"
                                EnableClientScript="true"
                                Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
                                >
                            <prop:ClientSide.OnValidationError>
                                document.getElementById('divValidationSummary').style.display='';
                                document.getElementById('ctl0_CONTENU_PAGE_ButtonSendToChorus').style.display='';
                                document.getElementById('ctl0_CONTENU_PAGE_saveInfoTransmission').style.display='';
                                document.getElementById('ctl0_CONTENU_PAGE_buttonAnnuler').style.display='';
                            </prop:ClientSide.OnValidationError>
                        </com:TDataTypeValidator>
                        <div class="float-left col-60"><com:TTranslate>DEFINE_EURO_HT</com:TTranslate>
                            <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" onmouseover="afficheBulle('bulleMontantActe', this)" onmouseout="cacheBulle('bulleMontantActe')" class="picto-info-intitule" alt="Info-bulle" title="Info-bulle" />
                        </div>

                        <div id="bulleMontantActe" class="info-bulle" onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();"><div><com:TTranslate>MONTANT_HT_ACTE_CONTRACTUEL_MODIFICATIF</com:TTranslate></div></div>
                    </div>
                </div>
                <div class="line">
                    <div class="intitule-bloc intitule-180"><label for="tauxTVAModifie"><com:TTranslate>TAUX_TVA_MODIFIE</com:TTranslate></label> :</div>
                    <div class="content-bloc">
                        <com:TTextBox
                            id="tauxTVAModifie"
                            cssClass="input-30 float-left align-right"
                            Attributes.title="<%=Prado::localize('TAUX_TVA_MODIFIE')%>"
                        />
                        <com:TCompareValidator
                                ValidationGroup="validationGroup"
                                ControlToValidate="tauxTVAModifie"
                                Enabled="<%=$this->TemplateControl->isTypeEnvoiFicheModificative()%>"
                                Display="Dynamic"
                                ErrorMessage="<%=Prado::localize('TAUX_TVA_MODIFIE')%>"
                                EnableClientScript="true"
                                ValueToCompare="100"
                                DataType="Integer"
                                Operator="LessThanEqual"
                                Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
                                >
                            <prop:ClientSide.OnValidationError>
                                document.getElementById('divValidationSummary').style.display='';
                                document.getElementById('ctl0_CONTENU_PAGE_ButtonSendToChorus').style.display='';
                                document.getElementById('ctl0_CONTENU_PAGE_saveInfoTransmission').style.display='';
                                document.getElementById('ctl0_CONTENU_PAGE_buttonAnnuler').style.display='';
                            </prop:ClientSide.OnValidationError>
                        </com:TCompareValidator>
                        <com:TCompareValidator
                                ValidationGroup="validationGroup"
                                ControlToValidate="tauxTVAModifie"
                                Enabled="<%=$this->TemplateControl->isTypeEnvoiFicheModificative()%>"
                                Display="Dynamic"
                                ErrorMessage="<%=Prado::localize('TAUX_TVA_MODIFIE')%>"
                                EnableClientScript="true"
                                ValueToCompare="0"
                                DataType="Integer"
                                Operator="NotEqual"
                                Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
                                >
                            <prop:ClientSide.OnValidationError>
                                document.getElementById('divValidationSummary').style.display='';
                                document.getElementById('ctl0_CONTENU_PAGE_ButtonSendToChorus').style.display='';
                                document.getElementById('ctl0_CONTENU_PAGE_saveInfoTransmission').style.display='';
                                document.getElementById('ctl0_CONTENU_PAGE_buttonAnnuler').style.display='';
                            </prop:ClientSide.OnValidationError>
                        </com:TCompareValidator>
                        <div class="intitule-auto"> %</div>
                    </div>
                </div>
                <div class="separator"></div>
                <div class="line">
                    <div class="intitule-bloc intitule-180"><label for="nbrNouveauxFournisseurs"><com:TTranslate>TEXT_NOUVEAU_FOURNISSEUR</com:TTranslate></label> :</div>
                    <div class="content-bloc">
                        <com:TDropDownList
                            id="nbrNouveauxFournisseurs"
                            Attributes.onChange="afficherBlocNouveauxFournisseurs();"
                            cssClass="input-70 float-left-fix"
                            Attributes.title="<%=Prado::localize('TEXT_NOUVEAU_FOURNISSEUR')%>"
                        />
                        <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" onmouseover="afficheBulle('bulle_nouveauxFournisseurs', this)" onmouseout="cacheBulle('bulle_nouveauxFournisseurs')" class="picto-info-intitule" alt="Info-bulle" title="Info-bulle" />
                        <div id="bulle_nouveauxFournisseurs" class="info-bulle" onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();"><div><com:TTranslate>NOMBRE_NOUVELLES_ENTREPRISE_CONTRACTANTES</com:TTranslate></div></div>
                    </div>
                </div>
                <div class="spacer-small"></div>
                <!--Cas 1 Etabli en France-->
                <com:TRepeater Id="repeatNouveauxFournisseurs">
                    <prop:ItemTemplate>
                        <com:TPanel id="fournisseur" style="display:none;">
                            <div class="line">
                                <div class="intitule-180 line-height-normal"><com:TTranslate>TEXT_L_ENTREPRISE</com:TTranslate> <%#$this->ItemIndex+1%> <com:TTranslate>TEXT_EST_ELLE_ETABLIE_FRANCE</com:TTranslate><span class="champ-oblig">*</span></div>
                                <div class="content-bloc">
                                    <div class="intitule-auto intitule-80">
                                        <com:TActiveRadioButton
                                            id="entrepriseEtablieFranceOui"
                                            cssClass="radio"
                                            Attributes.title="<%=Prado::localize('TEXT_OUI')%>"
                                            GroupName="entreprise_etablieFrance_<%#$this->ItemIndex+1%>"
                                            Attributes.onclick="showDiv('<%#$this->divSiren->ClientId%>');"
                                        />
                                        <label for="entrepriseEtablieFranceOui"><com:TTranslate>TEXT_OUI</com:TTranslate></label>
                                    </div>
                                    <div class="intitule-auto intitule-60">
                                        <com:TActiveRadioButton
                                            id="entrepriseEtablieFranceNon"
                                            cssClass="radio"
                                            Attributes.title="<%=Prado::localize('TEXT_NON')%>"
                                            GroupName="entreprise_etablieFrance_<%#$this->ItemIndex+1%>"
                                            Attributes.onclick="hideDiv('<%#$this->divSiren->ClientId%>');"
                                        />
                                        <label for="entrepriseEtablieFranceNon"><com:TTranslate>TEXT_NON</com:TTranslate></label>
                                    </div>
                                    <com:TTextBox style="display:none" ID="entrepriseFrancaiseItem" text="<%#($this->ItemIndex)%>" />
                                    <com:TCustomValidator
                                            ValidationGroup="validationGroup"
                                            ControlToValidate="entrepriseFrancaiseItem"
                                            Enabled="<%=$this->TemplateControl->isTypeEnvoiFicheModificative()%>"
                                            Display="Dynamic"
                                            ErrorMessage="<%=Prado::localize('TEXT_L_ENTREPRISE')%> <%#$this->ItemIndex+1%> <%=Prado::localize('TEXT_EST_ELLE_ETABLIE_FRANCE')%>"
                                            ClientValidationFunction="validerEtpFrancaiseNouveauxFournisseursFicheModificative"
                                            Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>" >
                                        <prop:ClientSide.OnValidationError>
                                            document.getElementById('divValidationSummary').style.display='';
                                            document.getElementById('ctl0_CONTENU_PAGE_ButtonSendToChorus').style.display='';
                                            document.getElementById('ctl0_CONTENU_PAGE_saveInfoTransmission').style.display='';
                                            document.getElementById('ctl0_CONTENU_PAGE_buttonAnnuler').style.display='';
                                        </prop:ClientSide.OnValidationError>
                                    </com:TCustomValidator>
                                </div>
                            </div>
                            <com:TPanel CssClass="line" id="divSiren">
                                <div class="intitule-180">
                                    <label id="numSiret"><com:TTranslate>TEXT_NUM_SIRET_ENTREPRISE</com:TTranslate> <%#$this->ItemIndex+1%><span class="champ-oblig">*</span></label> :
                                </div>
                                <com:TTextBox
                                    id="sirenEntreprise"
                                    CssClass="siren"
                                    MaxLength="9"
                                    Attributes.title="<%#Prado::localize('TEXT_SIREN_ENTREPRISE')%> <%#$this->ItemIndex+1%>"
                                />
                                <com:TTextBox
                                    id="codeEtablissement"
                                    CssClass="siret"
                                    MaxLength="5"
                                    Attributes.title="<%=Prado::localize('TEXT_SIRET_ENTREPRISE')%> <%#$this->ItemIndex+1%>"
                                />
                                <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" onmouseover="afficheBulle('infosSiret', this)" onmouseout="cacheBulle('infosSiret')" class="picto-info-intitule" alt="Info-bulle" title="Info-bulle" />
                                <div id="infosSiret" class="info-bulle" onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();">
                                    <div><com:TTranslate>TEXT_INDIQUER_NIC</com:TTranslate></div>
                                </div>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <com:TTextBox style="display:none" ID="sirenEntrepriseItem" text="<%#($this->ItemIndex)%>" />
                                <com:TCustomValidator
                                        ValidationGroup="validationGroup"
                                        ControlToValidate="sirenEntrepriseItem"
                                        Enabled="<%=$this->TemplateControl->isTypeEnvoiFicheModificative()%>"
                                        Display="Dynamic"
                                        ErrorMessage="<%=Prado::localize('TEXT_SIREN_ENTREPRISE')%> <%#$this->ItemIndex+1%>"
                                        ClientValidationFunction="validerSirenNouveauxFournisseursFicheModificative"
                                        Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>" >
                                    <prop:ClientSide.OnValidationError>
                                        document.getElementById('divValidationSummary').style.display='';
                                        document.getElementById('ctl0_CONTENU_PAGE_ButtonSendToChorus').style.display='';
                                        document.getElementById('ctl0_CONTENU_PAGE_saveInfoTransmission').style.display='';
                                        document.getElementById('ctl0_CONTENU_PAGE_buttonAnnuler').style.display='';
                                    </prop:ClientSide.OnValidationError>
                                </com:TCustomValidator>
                            </com:TPanel>
                            <div class="line">
                                <div class="intitule-180"><label for="nomEntreprise"><com:TTranslate>NOM_ENTREPRISE</com:TTranslate> <%#$this->ItemIndex+1%><span class="champ-oblig">*</span></label></div>
                                <com:TTextBox
                                    id="nomEntreprise"
                                    cssClass="bloc-450"
                                    Attributes.title="<%=Prado::localize('NOM_ENTREPRISE')%> <%#$this->ItemIndex+1%>"
                                />
                                <com:TTextBox style="display:none" ID="nomEntrepriseItem" text="<%#($this->ItemIndex)%>" />
                                <com:TCustomValidator
                                        ValidationGroup="validationGroup"
                                        ControlToValidate="nomEntrepriseItem"
                                        Enabled="<%=$this->TemplateControl->isTypeEnvoiFicheModificative()%>"
                                        Display="Dynamic"
                                        ErrorMessage="<%=Prado::localize('NOM_ENTREPRISE')%> <%#$this->ItemIndex+1%>"
                                        ClientValidationFunction="validerNomsNouveauxFournisseursFicheModificative"
                                        Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>" >
                                    <prop:ClientSide.OnValidationError>
                                        document.getElementById('divValidationSummary').style.display='';
                                        document.getElementById('ctl0_CONTENU_PAGE_ButtonSendToChorus').style.display='';
                                        document.getElementById('ctl0_CONTENU_PAGE_saveInfoTransmission').style.display='';
                                        document.getElementById('ctl0_CONTENU_PAGE_buttonAnnuler').style.display='';
                                    </prop:ClientSide.OnValidationError>
                                </com:TCustomValidator>
                            </div>
                            <div class="line">
                                <div class="intitule-180">
                                    <label for="<%#$this->typeFournisseur->ClientId%>"><com:TTranslate>TEXT_TYPE_FOURNISSEUR</com:TTranslate> <%#$this->ItemIndex+1%><span class="champ-oblig">*</span></label> :
                                </div>
                                <com:TDropDownList
                                    id="typeFournisseur"
                                    cssClass="bloc-450"
                                    Attributes.title="<%=Prado::localize('TEXT_TYPE_FOURNISSEUR')%>"
                                    dataSource="<%#$this->SourceTemplateControl->fillTypeFournisser()%>"
                                />
                                <com:TTextBox style="display:none" ID="typeEntrepriseItem" text="<%#($this->ItemIndex)%>" />
                                <com:TCustomValidator
                                        ValidationGroup="validationGroup"
                                        ControlToValidate="typeEntrepriseItem"
                                        Enabled="<%=$this->TemplateControl->isTypeEnvoiFicheModificative()%>"
                                        Display="Dynamic"
                                        ErrorMessage="<%=Prado::localize('TEXT_TYPE_FOURNISSEUR')%> <%#$this->ItemIndex+1%>"
                                        ClientValidationFunction="validerTypesNouveauxFournisseursFicheModificative"
                                        Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>" >
                                    <prop:ClientSide.OnValidationError>
                                        document.getElementById('divValidationSummary').style.display='';
                                        document.getElementById('ctl0_CONTENU_PAGE_ButtonSendToChorus').style.display='';
                                        document.getElementById('ctl0_CONTENU_PAGE_saveInfoTransmission').style.display='';
                                        document.getElementById('ctl0_CONTENU_PAGE_buttonAnnuler').style.display='';
                                    </prop:ClientSide.OnValidationError>
                                </com:TCustomValidator>
                            </div>
                        </com:TPanel>
                    </prop:ItemTemplate>
                </com:TRepeater>

                <div class="separator"></div>
                <div class="line">
                    <div class="intitule-180">Visa ACCF :</div>
                    <div class="content-bloc">
                        <div class="intitule-auto intitule-80">
                            <com:TActiveRadioButton
                                id="visaACCFOui"
                                cssClass="radio"
                                Attributes.title="<%=Prado::localize('TEXT_OUI')%>"
                                GroupName="visaACCF"
                            />
                            <label for="visaACCFOui"><com:TTranslate>TEXT_OUI</com:TTranslate></label>
                        </div>
                        <div class="intitule-auto intitule-60">
                            <com:TActiveRadioButton
                                id="visaACCFNon"
                                cssClass="radio"
                                Attributes.title="<%=Prado::localize('TEXT_NON')%>"
                                GroupName="visaACCF"
                            />
                            <label for="visaACCFNon"><com:TTranslate>TEXT_NON</com:TTranslate></label>
                        </div>
                        <div class="intitule-auto intitule-100">
                            <com:TActiveRadioButton
                                id="visaACCFNonRenseigne"
                                cssClass="radio"
                                Attributes.title="<%=Prado::localize('NON_RENSEIGNE')%>"
                                GroupName="visaACCF"
                            />
                            <label for="visaACCFNonRenseigne"><com:TTranslate>NON_RENSEIGNE</com:TTranslate></label>
                        </div>
                        <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" onmouseover="afficheBulle('infosVisaACCF', this)" onmouseout="cacheBulle('infosVisaACCF')" class="picto-info-intitule"/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </div>
                        <div id="infosVisaACCF" class="info-bulle" onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();">
                            <div><com:TTranslate>TEXT_INFOS_BULLE_ACCF</com:TTranslate></div>
                        </div>
                </div>
                <div class="line">
                    <div class="intitule-180">Visa préfet :</div>
                    <div class="content-bloc">
                        <div class="intitule-auto intitule-80">
                            <com:TActiveRadioButton
                                id="visaPrefetOui"
                                cssClass="radio"
                                Attributes.title="<%=Prado::localize('TEXT_OUI')%>"
                                GroupName="visaPrefet"
                            />
                            <label for="visaPrefetOui"><com:TTranslate>TEXT_OUI</com:TTranslate></label>
                        </div>
                        <div class="intitule-auto intitule-60">
                            <com:TActiveRadioButton
                                id="visaPrefetNon"
                                cssClass="radio"
                                Attributes.title="<%=Prado::localize('TEXT_NON')%>"
                                GroupName="visaPrefet"
                            />
                            <label for="visaPrefetNon"><com:TTranslate>TEXT_NON</com:TTranslate></label>
                        </div>
                        <div class="intitule-auto intitule-100">
                            <com:TActiveRadioButton
                                id="visaPrefetNonRenseigne"
                                cssClass="radio"
                                Attributes.title="<%=Prado::localize('NON_RENSEIGNE')%>"
                                GroupName="visaPrefet"
                            />
                            <label for="visaPrefetNonRenseigne"><com:TTranslate>NON_RENSEIGNE</com:TTranslate></label>
                        </div>
                        <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" onmouseover="afficheBulle('infosVisaPrefet', this)" onmouseout="cacheBulle('infosVisaPrefet')" class="picto-info-intitule" /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </div>
                        <div id="infosVisaPrefet" class="info-bulle" onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();">
                            <div><com:TTranslate>TEXT_INFOS_BULLE_PREFET</com:TTranslate></div>
                        </div>
                </div>
                <div class="spacer"></div>
                <div class="line">
                    <div class="intitule-180 line-height-normal"><label for="remarques"><com:TTranslate>TEXT_REMARQUE_S</com:TTranslate></label> :</div>
                    <com:TActiveTextBox
                        id="remarques"
                        TextMode="MultiLine"
                        CssClass="bloc-450 high-120"
                        Attributes.rows="4"
                        Attributes.cols="20"
                        Attributes.title="<%=Prado::localize('DEFINE_REMARQUES')%>"
                    />
                </div>

            </div>
            <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
        </div>
        <div class="form-field">
            <div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><span class="title">Pièces justificatives </span></div>
            <div class="content">
                <com:TActivePanel id="panelPjFicheModificative" >
                    <com:TActiveRepeater id="repeaterPjFicheModificative" >
                        <prop:HeaderTemplate>
                            <table class="table-results bloc-740">
                                <h3>- <com:TTranslate>AUTRE_PIECE</com:TTranslate> : </h3>
                                <div class="spacer-mini"></div>
                        </prop:HeaderTemplate>
                        <prop:ItemTemplate>
                            <com:TTableRow CssClass="<%#(($this->ItemIndex%2==0)? 'on':'')%>">
                                <com:TTableCell><div class="nom-pj"><com:TLabel Text="<%#$this->Data->getNomFichier()%>"/><com:TLabel CssClass="info-aide" Text="(<%# Application\Service\Atexo\Atexo_Util::arrondirSizeFile($this->Data->getTaille()/1024)%>)"/></div></com:TTableCell>
                                <com:TTableCell CssClass="actions">
                                    <com:THyperLink NavigateUrl="javascript:popUp('index.php?page=Agent.popupDownloadPjFicheModificativeChorus&id=<%#$this->Data->getIdFicheModificativePj()%>');" CssClass="indent-5">
                                        <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-dl-dossier.gif" alt="<%=Prado::localize('DEFINE_TELECHARGER')%>" title="<%=Prado::localize('DEFINE_TELECHARGER')%>" />
                                    </com:THyperLink>
                                </com:TTableCell>
                                <com:TTableCell CssClass="actions">
                                    <com:PictoDeleteActive
                                        cssClass="indent-5"
                                        AlternateText="<%=Prado::localize('TEXT_SUPPRIMER')%>"
                                        Attributes.onclick="document.getElementById('<%#$this->SourceTemplateControl->idToDetele->getClientId()%>').value = '<%#$this->Data->getIdFicheModificativePj()%>';
                                                            openModal('modal-confirmation-suppression','popup-small2',this);"
                                    />
                                </com:TTableCell>
                            </com:TTableRow>
                        </prop:ItemTemplate>
                        <prop:FooterTemplate>
                            </table>
                        </prop:FooterTemplate>
                    </com:TActiveRepeater>
                    <div class="bloc-740 margin-left-15">
                        <a href="javascript:popUp('index.php?page=Agent.PopupChorusPjFicheModificative&idFicheModificative=<%=$this->getIdFicheModificative()%>','yes');"
                           class="ajout-el clear-both" title="<%=Prado::localize('TEXT_AJOUTER_PIECE')%>"
                        >
                            <com:TTranslate>TEXT_AJOUTER_PIECE</com:TTranslate>
                        </a>
                        <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" onmouseover="afficheBulle('bulleExtensionsAutoriseesFicheModif', this)" onmouseout="cacheBulle('bulleExtensionsAutoriseesFicheModif')" class="picto-info-intitule" alt="Info-bulle" title="Info-bulle" />
                        <div id="bulleExtensionsAutoriseesFicheModif" class="info-bulle" onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();">
                            <div><%=(new Application\Service\Atexo\Chorus\Atexo_Chorus_Util())->getInfosExtensionsAutoriseesEnvoiChorus()%></div>
                        </div>
                    </div>
                </com:TActivePanel>
                <div class="breaker"></div>
            </div>
            <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
        </div>
        <div class="breaker"></div>
    </div>
    <div class="bottom"></div>
    <div class="modal-confirmation-suppression" style="display:none;">
        <com:TPanel ID="panelMessageDelete" style="display:none;">
            <com:PanelMessageAvertissement ID="messageAvertissementdelete" message="<%=Prado::localize('ALERTE_SUPPRESSION_ETABLISSEMENT_IMPOSSIBLE')%>" />
        </com:TPanel>
        <!--Debut Formulaire-->
        <com:TPanel ID="panelConfirmDelete" cssClass="form-bloc" style="display:'';">
            <div class="top"><span class="left"></span><span class="right"></span></div>
            <div class="content">
                <p><strong><com:TTranslate>TEXT_CONFIRMATION_SUPRESSION_DOCUMENT</com:TTranslate></strong></p>
                <p><com:TActiveLabel ID="nameToDelete" text=""/></p>
            </div>
            <div class="bottom"><span class="left"></span><span class="right"></span></div>
        </com:TPanel>
        <!--Fin Formulaire-->
        <!--Debut line boutons-->
        <div class="boutons-line">
            <com:THiddenField id="idToDetele"/>
            <input
                id="buttonCancel"
                type="button"
                class="bouton-moyen float-left"
                value="<%=Prado::localize('DEFINE_ANNULER')%>"
                title="<%=Prado::localize('DEFINE_ANNULER')%>"
                onclick="J('.modal-confirmation-suppression').dialog('close');return false;"
            />
            <com:TActiveButton
                id="cofirmSupression"
                Text = "<%=Prado::localize('TEXT_POURSUIVRE')%>"
                CssClass="bouton-moyen float-right"
                Attributes.title="<%=Prado::localize('TEXT_POURSUIVRE')%>"
                Attributes.onclick="showLoader();"
                onCommand="SourceTemplateControl.deletePjModificative"
                onCallBack="SourceTemplateControl.refreshPjFicheModificative"
                CommandParameter="<%=$this->idToDetele->value%>"
            >
            </com:TActiveButton >
        </div>
        <!--Fin line boutons-->
    </div>
    <com:TActiveLabel id="labelScript" />
    <com:TActiveLabel id="labelScript2" />
    <com:TActiveButton
        id="refreshRepeaterPjFicheModificative"
        Text="buttonRefreshPieceExternes"
        cssClass="cssAso"
        Attributes.onclick='showLoader();'
        OnCallBack="refreshPjFicheModificative"
        Attributes.style="display:none;"
    />
</div>
<!-- Fin layer 5 -->
<script type="application/javascript">
    var timeout;
    J(document).ready(function(){
        J('.montant-formatter').keyup(function(event){
            //event.preventDefault();
            clearTimeout(timeout);
            timeout = setTimeout(function(i){
                var currentValue = J(i).val();
                if(currentValue.length >= 3){
                    var valeur = currentValue.replace(' ','');
                    var newValeur = arrondirEspace(currentValue,2,' ',0);
                    var mt = newValeur.replace('.',',');
                    if (newValeur == 'NaN.NaN'){
                        mt =  "0,00";
                    }
                    J(i).blur();
                }
            }, 600,this);
        });
    });
</script>
