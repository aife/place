<?php

namespace Application\Controls;

use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonTContratTitulaire;
use Application\Propel\Mpe\CommonTContratTitulaireQuery;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Contrat;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Lots;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureType;
use Prado\Prado;

/**
 * Le bloc de recap de consultation.
 *
 * @author LEZ <loubna.ezziani@atexo.com>
 *
 * @version 1.0
 *
 * @since 4.11.0
 *
 * @copyright Atexo 2015
 */
class AtexoBlocRecapConsultation extends MpeTTemplateControl
{
    public function initialiserComposant($reference, $lots, $idContrat = null)
    {
        $fromCons = false;
        $consultation = (new Atexo_Consultation())->retrieveConsultation($reference);
        if ($consultation instanceof CommonConsultation) {
            $fromCons = true;
            $this->labelTitreConsultation->Text = $consultation->getObjet();
            $this->labelReferenceConsultation->Text = $consultation->getReferenceUtilisateur();
            if (Atexo_Module::isEnabled("NumeroProjetAchat")) {
                $this->labelNumeroProjetAchat->Text = $consultation->getCodeExterne();
            }
            if ($lots) {
                $this->fillInfoLots($consultation, $lots);
            }
        } else {
            $this->infoCons->setVisible(false);
        }

        if ($idContrat) {
            $this->fillInfoAttribution($idContrat, $fromCons);
            $this->panelInfoAttribution->setVisible(true);
        } else {
            $this->panelInfoAttribution->setVisible(false);
        }
        $this->panelRecap->setVisible(true);
    }

    public function fillInfoLots($consultation, $lots)
    {
        $listeLots = explode('#', $lots);
        if (is_array($listeLots)) {
            $categorieLots = (new Atexo_Consultation_Lots())->retrieveListeLotsByArray($consultation->getId(), $consultation->getOrganisme(), $listeLots);
            $this->repeaterLots->DataSource = $categorieLots;
            $this->repeaterLots->DataBind();
        }
    }

    /**
     * Permet de replire des information sur l'attribution.
     *
     * @param int $idContrat l'id de contrat
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function fillInfoAttribution($idContrat, $fromCons = false)
    {
        $contratQuery = new CommonTContratTitulaireQuery(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $contrat = $contratQuery->getTContratTitulaireById($idContrat);
        if ($contrat instanceof CommonTContratTitulaire) {
            if (!$fromCons) {
                $this->panelObjetContrat->setVisible(true);
                $this->panelDateAttribution->setVisible(false);
                $this->objetContrat->setText($contrat->getReferenceLibre().($contrat->getReferenceLibre() ? ' - ' : '').$contrat->getObjetContrat());
            }
            if ($contrat->getStatutContrat() == Atexo_Config::getParameter('STATUT_NOTIFICATION_CONTRAT_EFFECTUEE') && $contrat->getNumIdUniqueMarchePublic()) {
                $this->panelIdentificationUnique->setVisible(true);
                $this->identificationUnique->setText($contrat->getNumIdUniqueMarchePublic());
            }
            $this->typeContrat->Text = Atexo_Util::formaterMessage((new Atexo_Consultation_Contrat())->getLibelleTypeContratById($contrat->getIdTypeContrat()));

            $procedure = (new Atexo_Consultation_ProcedureType())->retrieveTypeProcedurePivotObject($contrat->getProcedurePassationPivot());

            if (null !== $procedure) {
                $this->panelProcedurePassation->setVisible(true);
                $this->procedurePassation->setText(Prado::localize($procedure->getLibelle()));
            }
            $this->dateDecisionAttribution->setText($contrat->getDateAttribution());
        }
    }

    /**
     * @return string
     */
    public function getDateAttribution()
    {
        return  $this->dateDecisionAttribution->getText();
    }
}
