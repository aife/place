<?php

namespace Application\Controls;

use Prado\Prado;

/**
 * Classe EntrepriseResultSearchEntity.
 *
 * @author Yassine SBAI <yassine.sbai@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class EntrepriseResultSearchEntity extends MpeTPage
{
    public string $_calledFrom = '';

    public function onLoad($param)
    {
        /*if(!$this->Page->IsPostBack) {

         }*/
    }

    public function setCalledFrom($value)
    {
        $this->_calledFrom = $value;
    }

    public function getCalledFrom()
    {
        return $this->_calledFrom;
    }

    public function remplirTableauResultat($tableau)
    {
        $this->setViewState('resultats', $tableau);

        if (0 != (is_countable($tableau) ? count($tableau) : 0)) {
            $this->panelMessageResultat->setVisible(false);
            $this->panelResultat->setVisible(true);
            $this->repeaterResultatEntite->dataSource = $tableau;
            $this->repeaterResultatEntite->dataBind();
        } else {
            $this->PanelMessage->setMessage(Prado::localize('MESSAGE_RESULTAT_TROUVE'));
            $this->panelMessageResultat->setVisible(true);
            $this->panelResultat->setVisible(false);
        }
    }

    public function onDetailClick($sender, $param)
    {
        $this->Page->DetailEntiteAchat->detailEA($param->CommandParameter, $this->Page->entitePubliqueChoix->SelectedValue);
        $this->Page->recherche->setVisible(false);
        $this->Page->resultat->setVisible(false);
        $this->Page->detailEntite->setVisible(true);
    }
}
