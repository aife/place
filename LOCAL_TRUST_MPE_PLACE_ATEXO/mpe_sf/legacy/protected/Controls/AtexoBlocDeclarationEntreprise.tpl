<div class="panel panel-default">
	<div class="panel-body">
		<div class="checkbox">
			<label for="ctl0_CONTENU_PAGE_blocDeclaration_pouvoirEngage">
				<com:TActiveImage id="imgPouvoirEngage" cssClass="m-r-1" Style="display:<%=$this->getModeAffichage()?'':'none'%>" />
				<com:TActiveCheckBox id="pouvoirEngage" Attributes.title="<%=Prado::localize('JE_POSSEDE_LE_POUVOIR_D_ENGAGER_LA_SOCIETE')%>" cssclass="check" Style="display:<%=$this->getModeAffichage()?'none':''%>"  />
				<com:TTranslate>MESSAGE_JE_POSSEDE_LE_POUVOIR_D_ENGAGER_LA_SOCIETE</com:TTranslate>
			</label>
		</div>
		<div class="form-horizontal p-l-2">
			<div class="form-group clearfix <%=$this->getModeAffichage()?'':'indent-18'%>">
				<label class="col-md-2" for="ctl0_CONTENU_PAGE_blocDeclaration_representantLegal">
					<com:TActiveImage id="imgRepresentantLegal" cssClass="m-r-1" Style="display:<%=$this->getModeAffichage()?'':'none'%>" />
					<com:TTranslate>REPRESENTANT_LEGAL</com:TTranslate> :
				</label>
				<div class="col-md-10 form-group-sm">
					<com:TActiveDropDownList
							ID="representantLegal"
							Attributes.title="<%=Prado::localize('CHOISISSEZ_UN_REPRESENTANT_LEGAL')%>"
							cssClass="form-control"
							Attributes.onchange="displayOneOptionPanel(this,'1','ctl0_CONTENU_PAGE_<%=$this->getId()%>_panel_representantAutre')"
							Style="display:<%=$this->getModeAffichage()?'none':''%>" />
					<com:TActiveLabel id="representantLegalLabel" Style="display:<%=$this->getModeAffichage()?'':'none'%>"/>
				</div>
			</div>
			<com:TActivePanel id="panel_representantAutre" display="None">
				<div class="form-group form-group-sm clearfix">
					<label class="control-label col-md-2" for="ctl0_CONTENU_PAGE_blocDeclaration_representantNom">
						<com:TTranslate>DEFINE_NOM</com:TTranslate>  :
					</label>
					<div class="col-md-10">
						<com:TActiveTextBox id="representantNom" Attributes.title="<%=Prado::localize('DEFINE_NOM')%>"  cssClass="form-control small" Style="display:<%=$this->getModeAffichage()?'none':''%>" />
						<com:TActiveLabel id="representantNomLabel" Style="display:<%=$this->getModeAffichage()?'':'none'%>"/>
					</div>
				</div>
				<div class="form-group form-group-sm clearfix">
					<label class="control-label col-md-2" for="ctl0_CONTENU_PAGE_blocDeclaration_representantPrenom">
						<com:TTranslate>DEFINE_PRENOM</com:TTranslate>  :
					</label>
					<div class="col-md-10">
						<com:TActiveTextBox id="representantPrenom" Attributes.title="<%=Prado::localize('DEFINE_PRENOM')%>"  cssClass="form-control small" Style="display:<%=$this->getModeAffichage()?'none':''%>" />
						<com:TActiveLabel id="representantPrenomLabel" Style="display:<%=$this->getModeAffichage()?'':'none'%>"/>
					</div>
				</div>
				<div class="form-group form-group-sm clearfix">
					<label class="control-label col-md-2" for="ctl0_CONTENU_PAGE_blocDeclaration_representantFonction">
						<com:TTranslate>DEFINE_FONCTION_MAJ</com:TTranslate>  :
					</label>
					<div  class="col-md-10">
						<com:TActiveTextBox id="representantFonction" Attributes.title="<%=Prado::localize('DEFINE_FONCTION_MAJ')%>"  cssClass="form-control small" Style="display:<%=$this->getModeAffichage()?'none':''%>" />
						<com:TActiveLabel id="representantFonctionLabel" Style="display:<%=$this->getModeAffichage()?'':'none'%>"/>
					</div>
				</div>

			</com:TActivePanel>
		</div>
		<div class="checkbox">
			<label for="ctl0_CONTENU_PAGE_blocDeclaration_autorisationVerification">
				<com:TActiveImage id="imgAutorisationVerification" cssClass="m-r-1" Style="display:<%=$this->getModeAffichage()?'':'none'%>" />
				<com:TActiveCheckBox id="autorisationVerification" Attributes.title="<%=Prado::localize('AUTORISER_LA_VERFICATION_DE_MES_INFORMATIONS')%>" cssclass="check" Style="display:<%=$this->getModeAffichage()?'none':''%>"  />
				<com:TTranslate>AUTORISER_LA_VERFICATION_DE_MES_INFORMATIONS</com:TTranslate>
			</label>
		</div>
	</div>
</div>
<com:TActiveLabel ID="scriptJs" />