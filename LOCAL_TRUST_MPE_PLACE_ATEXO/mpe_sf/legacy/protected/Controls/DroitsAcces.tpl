<!--Debut Layer 5-->
			<!--div class="ongletLayer" id="ongletLayer5"-->
				<!--div class="top"></div-->
				<!--div class="content"-->
					
					<!--Debut Bloc Entite d'achat de rattachement-->
					<div class="form-field">
						<div class="content">
							<!--Debut Ligne Mon entite-->
							<com:TPanel CssClass="line" visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('GestionMandataire') || Application\Service\Atexo\Atexo_CurrentUser::hasHabilitation('RattachementService')) ? false:true%>" >
								<div class="intitule-220"><label for="monEntite"><com:TTranslate>DEFINE_POLE_RATACHEMENT</com:TTranslate></label> :</div><com:TLabel id="monEntite" Attributes.name="<%=Prado::localize('DEFINE_POLE_RATACHEMENT')%>" />
							</com:TPanel>
							<com:TPanel CssClass="line" visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('GestionMandataire') || Application\Service\Atexo\Atexo_CurrentUser::hasHabilitation('RattachementService')) ? true:false%>">
								<div class="intitule-220"><label for="entiteeAssociee"><com:TTranslate>DEFINE_POLE_RATACHEMENT</com:TTranslate></label><span class="champ-oblig">*</span> :</div>
								<com:TActiveDropDownList 
										id="entitee" 
										Attributes.title="<%=Prado::localize('DEFINE_POLE_RATACHEMENT')%>" 
										OnCallback="RefreshListRPA"
										CssClass="liste-entites"
									/>
								<com:TRequiredFieldValidator 
									ControlToValidate="entitee"
									ValidationGroup="validateInfosConsultation"
									Display="Dynamic"
									ErrorMessage="<%=Prado::localize('DEFINE_ENTITE_RATTACHEMENT_EA_PMI')%>"
									EnableClientScript="true"  						 					 
						 			Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
									<prop:ClientSide.OnValidationError>
									    var buttonSave = document.getElementById('ctl0_CONTENU_PAGE_buttonSave');

           				 				document.getElementById('divValidationSummary').style.display='';
           				 				if (buttonSave) {
           				 				    document.getElementById('ctl0_CONTENU_PAGE_buttonSave').style.display='';
           				 				}
         							</prop:ClientSide.OnValidationError>
         						</com:TRequiredFieldValidator>
								<com:ActivePictoOk  id="boutonOkEntitee" OnCallback="ajouterPermanentService" />
							</com:TPanel> 
							<!--Fin Ligne Mon entite-->
							<!--Debut Ligne Entité associee-->
							<com:TPanel id="panelEnbtiteRattachement" Visible="<%=$this->Page->DroitsAcces->isTransverseTender()%>" CssClass="line">
                                <com:TActivePanel id="panelentiteeAssociee">
									<div class="intitule-220"><label for="entiteeAssociee"><com:TTranslate>DEFINE_ENTITE_ASSOCIEE</com:TTranslate></label> :</div>
									<com:TActiveDropDownList 
										id="entiteeAssociee" 
										Attributes.title="<%=Prado::localize('DEFINE_ENTITE_ASSOCIEE')%>" 
										OnCallback="RefreshListRPA"
										CssClass="liste-entites"
									/>
									<com:ActivePictoOk  id="boutonOkEntiteeAssociee" visible=<%=(Application\Service\Atexo\Atexo_Module::isEnabled('AlimentationAutomatiqueListeInvites')) ? true:false%> OnCallback="ajouterPermanentServiceAssocie" />
									
								</com:TActivePanel>
                            </com:TPanel>
							<div class="line">
								<div class="intitule-220 line-height-normal"><label for="representantAdjudicateur"><com:TTranslate>DEFINE_RPA</com:TTranslate></label> :</div>
								<com:TActiveDropDownList id="representantAdjudicateur"  Attributes.title="<%=Prado::localize('DEFINE_RPA')%>" CssClass="liste-entites"/>
							</div>
							<!--Fin Ligne Entité associee-->
							<div class="breaker"></div>
						</div>
					</div>
					<!--Fin Bloc Pôle de ratachement-->
					<!--Debut Bloc Liste des invites-->
					<div class="form-field">
						<span class="title"><com:TTranslate>DEFINE_LISTE_INVITES</com:TTranslate> </span>
						<div class="content">
							<div class="liste-invites">
								<!--Debut partitionneur-->
								<com:TActivePanel ID="ApRepeaterInvites">
								<div class="line-partitioner">
									<h2><com:TTranslate>DEFINE_INVITES</com:TTranslate> : <com:TActiveLabel id="nbrInvite" /></h2>
								</div>
								<!--Fin partitionneur-->								
									<com:TActiveRepeater ID="RepeaterInvites" EnableViewState="true">
									<prop:HeaderTemplate>
									<!--Debut Tableau Invites-->
									<table summary="Liste des invités" class="table-results">
										<caption><com:TTranslate>DEFINE_LISTE_INVITES</com:TTranslate></caption>
										<thead>
											<tr><th class="top" colspan="5"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></th></tr>
											<tr>
												<th class="col-170" id="nomInvite">
													<com:TTranslate>DEFINE_NOM_MAJ</com:TTranslate> <com:TTranslate>DEFINE_PRENOM</com:TTranslate>
													<com:TActiveImageButton
														ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/arrow-tri-off.gif"
														OnCallback="Page.DroitsAcces.sortCheckedGuests" 
														ActiveControl.CallbackParameter="LastName" 
														AlternateText="<%=Prado::localize('DEFINE_TRIER')%>" 
														Attributes.title="<%=Prado::localize('DEFINE_TRIER')%>" />
												</th>
												<th class="col-100 center" id="invitePermanent" abbr="Statut">
													<com:TTranslate>DEFINE_INVITE_PERMANENT</com:TTranslate>
												</th>
												<th class="col-350" id="entiteInvite">
													<com:TTranslate>DEFINE_ENTITE_ACHAT</com:TTranslate>
													<com:TActiveImageButton 
														ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/arrow-tri.gif"
														AlternateText="<%=Prado::localize('DEFINE_TRIER')%>" 
														OnCallback="Page.DroitsAcces.sortCheckedGuests" 
														ActiveControl.CallbackParameter="EntityPurchase" 
														Attributes.title="<%=Prado::localize('DEFINE_TRIER')%>" />
												</th>
												<th class="col-100 center" id="droisInvite"><com:TTranslate>DEFINE_DROITS</com:TTranslate></th>
												<th class="actions" id="actions"><com:TTranslate>DEFINE_HABILITATION</com:TTranslate></th>
											</tr>
										</thead>		
									</prop:HeaderTemplate>
									<prop:ItemTemplate>
										<tr class="<%#(($this->ItemIndex%2==0)? '':'on')%>">
											<td class="col-170" headers="nomInvite"><%#$this->Data->getLastName()%> <%#$this->Data->getFirstName()%></td>
											<td class="col-100 center" headers="invitePermanent"><com:TImage ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-ok.gif" id="invitePermanent" AlternateText="<%=Prado::localize('DEFINE_INVITE_PERMANENT')%>" Visible="<%#$this->Data->getPermanentGuest()== '1' ? true:false%>" Attributes.title="<%=Prado::localize('DEFINE_INVITE_PERMANENT')%>" /></a><com:TLabel Text="-" Visible="<%#$this->Data->getPermanentGuest()!= '1'%>"/></td>
											<td class="col-350" headers="entiteInvite"><%#$this->Data->getEntityPurchase()%></td>
											<td class="col-100 center" headers="droisInvite"><com:TImage ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-habilitation.gif" AlternateText="<%=Prado::localize('DEFINE_SUIVI_HABILITATION')%>" Visible="<%#$this->Data->getTypeInvitation() == '1' || $this->Data->getPermanentGuest()== '1' ? true:false%>" Attributes.title="<%=Prado::localize('DEFINE_SUIVI_HABILITATION')%>" /><com:TImage ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-visiteur.gif" Visible="<%#$this->Data->getTypeInvitation() == '0' ? true:false%>" AlternateText="<%=Prado::localize('DEFINE_SUIVI_SEUL')%>" Attributes.title="<%=Prado::localize('DEFINE_SUIVI_SEUL')%>" /></td>
											<td class="actions" headers="actions"><com:THyperLink NavigateUrl="javascript:popUp('/agent/habilitations/<%#$this->Data->getId()%>?mode=2','yes');"  ><com:TImage ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-details.gif" AlternateText="<%=Prado::localize('TEXT_DETAILS_HABILITATIONS')%>" Attributes.title="<%=Prado::localize('TEXT_DETAILS_HABILITATIONS')%>" /></com:THyperLink>
											</td>
										</tr>
									</prop:ItemTemplate>
									<prop:FooterTemplate>
									</table>					
									</prop:FooterTemplate>								 
									</com:TActiveRepeater>
								</com:TActivePanel>
								 <span style ="display:none;">
								  <com:TActiveButton Id="validateCheckdGuests" OnCallBack="updateGuests"/>
								</span>
								<!--Fin Tableau Invites-->
								<com:THyperLink visible="<%=($this->showHideAjoutModifDroitAcces())? true:false%>" NavigateUrl="javascript:popUpSetSize('index.php?page=Agent.ChoixInvites&inv='+document.getElementById('ctl0_CONTENU_PAGE_invites').value,'1150px','800px','yes');" Cssclass="ajout-el" Attributes.title="<%=Prado::localize('DEFINE_AJOUTER_MODIFIER_LISTE_INVITES')%>">
									<com:TTranslate>DEFINE_AJOUTER_MODIFIER_LISTE_INVITES</com:TTranslate>
								</com:THyperLink>
								<div class="breaker"></div>
							</div>
							<div class="breaker"></div>
						</div>
						<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
					</div>
					<!--Fin Bloc Liste des invites-->
					<div class="breaker"></div>
				<!--/div-->
				<!--div class="bottom"></div-->
			<!--/div-->
<!--Fin Layer 5-->
