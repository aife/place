<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_FormConsultation;
use Application\Service\Atexo\Atexo_Util;

/*
 * Created on 12 oct. 2011
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
class AtexoListeFormulaireInscrit extends MpeTTemplateControl
{
    public $enveloppe;
    public $lot;
    public $refConsultation;

    public function onLoad($param)
    {
    }

    public function setRefConsultation($value)
    {
        //Mettre toujours refcons dans une instanciation du composant comme
        //<com:AtexoListeFormulaireInscrit ID="listeFormuliareInscritOffre" lot="1" enveloppe="1" refConsultation="1"/>
        $this->refConsultation = $value;
        $idIscrit = Atexo_CurrentUser::getIdInscrit();
        self::rmeplirRepeaterFormsInscrit($this->getRefConsultation(), $idIscrit, $this->getEnveloppe(), $this->getLot());
    }

    public function getRefConsultation()
    {
        return $this->refConsultation;
    }

    public function setEnveloppe($value)
    {
        $this->enveloppe = $value;
    }

    public function getEnveloppe()
    {
        return $this->enveloppe;
    }

    public function setLot($value)
    {
        $this->lot = $value;
    }

    public function getLot()
    {
        return $this->lot;
    }

    public function rmeplirRepeaterFormsInscrit($consultationId, $idIscrit, $typeEnv, $lot)
    {
        $ArrayFormsInscrit = (new Atexo_FormConsultation())->getListeFormConsForInscrit($consultationId, $idIscrit, $typeEnv, $lot);
        $this->repeaterFormulaireEnveloppe->DataSource = $ArrayFormsInscrit;
        $this->repeaterFormulaireEnveloppe->dataBind();
    }

    public function getPictoStatut($statut)
    {
        return (new Atexo_FormConsultation())->getPictoStatutForm(false, false, false, $statut);
    }

    public function getTitleStatut($statut)
    {
        return (new Atexo_FormConsultation())->getTitleStatutForm(false, false, false, $statut);
    }

    public function getUrlVisualisationFormCons($typeForm, $idForm)
    {
        if ($typeForm == Atexo_Config::getParameter('QUESTIONNAIRE')) {
            return "javascript:popUp('index.php?page=Entreprise.PopUpDetailQuestionnaireEntreprise&orgAcronyme=".Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']).'&idFormCons='.$idForm."','yes');";
        }
        if ($typeForm == Atexo_Config::getParameter('BORDEREAU_DE_PRIX')) {
            return "javascript:popUp('index.php?page=Entreprise.PopUpDetailBordereauDePrixEntreprise&orgAcronyme=".Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']).'&idFormCons='.$idForm."','yes');";
        }
    }
}
