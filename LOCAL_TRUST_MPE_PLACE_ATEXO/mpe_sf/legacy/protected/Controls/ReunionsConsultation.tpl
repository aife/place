<com:TActivePanel cssClass="line" ID="panelReunion" Enabled="<%=($this->getActif())? true:false%>">
	<div class="intitule-150"><label for="reunion"><com:TTranslate>REUNION</com:TTranslate></label><span class="champ-oblig">*</span> :</div>
	<div class="content-bloc bloc-400">
		<div class="line">
			<com:TActiveRadioButton 
				GroupName="reunion" 
				Checked="true" 
				ID="reunionNon" 
				Text="<%=Prado::localize('TEXT_NON')%>"
				Attributes.onclick="hideDiv('<%=$this->reunionLayer->getClientId()%>');" 
				Attributes.title="<%=Prado::localize('TEXT_NON')%>"/>
		</div>
		<div class="line">
			<com:TActiveRadioButton 
				GroupName="reunion" 
				ID="reunionOui" 
				Text="<%=Prado::localize('FCSP_OUI')%>"
				Attributes.onclick="showDiv('<%=$this->reunionLayer->getClientId()%>');" 
				Attributes.title="<%=Prado::localize('FCSP_OUI')%>"/>
				<span id="spanErrorReunion" style="display:none" title='Champ obligatoire' ><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>
			<com:TCustomValidator
				ClientValidationFunction="validateReunionCons"
				ID="validatorReunion" 
				ControlToValidate="reunionOui"
				ValidationGroup="<%=($this->getValidationGroup())%>" 
				Display="Dynamic" 
				ErrorMessage="<%=Prado::localize('REUNION')%>"
				EnableClientScript="true"  						 					 
	 			Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
				<prop:ClientSide.OnValidationError>
	 				document.getElementById('divValidationSummary').style.display='';
	 				if(document.getElementById('ctl0_CONTENU_PAGE_blocBoutons'))
	 					document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
				</prop:ClientSide.OnValidationError>
			</com:TCustomValidator>
			<com:TCustomValidator
					ClientValidationFunction="validateReunionDateComapareCompsant"
					ID="validatorReunionDateCompare" 
					ControlToValidate="dateReunion"
					ValidationGroup="<%=($this->getValidationGroup())%>"
					Display="Dynamic"
					ErrorMessage="<%=Prado::localize('DATE_REUNION_POSTERIEUR_DATE_FIN')%>"
					EnableClientScript="true"  						 					 
		 			Text=" ">   
					<prop:ClientSide.OnValidationError>
		 				document.getElementById('divValidationSummary').style.display='';
		 				if(document.getElementById('ctl0_CONTENU_PAGE_blocBoutons'))
	 						document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
					</prop:ClientSide.OnValidationError>
			</com:TCustomValidator>
			<com:TCustomValidator
				ClientValidationFunction="dateAnterieurAujourdhui"
				ID="validatorReunionDateCompareDate" 
				ControlToValidate="dateReunion"
				ValidationGroup="<%=($this->getValidationGroup())%>"
				Display="Dynamic"
				ErrorMessage="<%=Prado::localize('DATE_REUNION_POSTERIEUR_AUJOURDHUI')%>"
				EnableClientScript="true"  	
				visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('validationFormatChampsStricte'))?'true' : 'false'%>"
	 			Text=" ">   
				<prop:ClientSide.OnValidationError>
	 				document.getElementById('divValidationSummary').style.display='';
	 				if(document.getElementById('ctl0_CONTENU_PAGE_blocBoutons'))
	 					document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
				</prop:ClientSide.OnValidationError>
			</com:TCustomValidator>
		</div>
	</div>
</com:TActivePanel>
<com:TActivePanel ID="reunionLayer" Style="display:none;" Enabled="<%=($this->getActif())? true:false%>">
	<div class="line">
		<div class="intitule-bloc intitule-140 indent-15"><label for="add_Reunion">- <com:TTranslate>TEXT_ADRESSE</com:TTranslate></label><span class="champ-oblig">*</span> :</div>
		<com:TActiveTextBox TextMode="MultiLine" ID="addReunion"  Attributes.title="<%=Prado::localize('TEXT_ADRESSE')%>" cssClass="long" />
	</div>
	<div class="line">
		<div class="intitule-140 indent-15"><label for="date_Reunion">- <com:TTranslate>DEFINE_DATE_HEURE</com:TTranslate></label><span class="champ-oblig">*</span> :</div>
		<div class="calendar indent-15">
			<com:TActiveTextBox cssClass="heure" id="dateReunion" Attributes.title="<%=Prado::localize('DEFINE_DATE_HEURE')%>" />
				<com:TImage ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-calendar.gif" Attributes.alt="<%=Prado::localize('DEFINE_CALENDRIER')%>" Attributes.title="<%=Prado::localize('DEFINE_CALENDRIER')%>" Attributes.onclick="popUpCalendar(this,document.getElementById('<%=$this->dateReunion->getClientId()%>'),'dd/mm/yyyy 10:30','');" />
		</div>
		<div class="info-aide-right">( jj/mm/aaaa hh:mm )</div>
	</div>							
</com:TActivePanel>	
