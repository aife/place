<?php

namespace Application\Controls;

use Prado\Prado;

/**
 * Copyright (C) Atexo - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential.
 */
class MessageStatusChecker extends MpeTTemplateControl
{
    private $translations = null;
    protected ?string $title = null;

    public function onLoad($param)
    {
        $this->title = addslashes(Prado::localize('DEMANDER_DE_VALIDATION'));
        if ('agent' === $this->Page->Master->getCalledFrom()) {
            $this->title = addslashes(Prado::localize('TEXT_INFORMATIONS_MON_ASSISTANT_MARCHES_PUBLICS'));
        }

        $translations = [
            'en_ligne' => Prado::localize('CONNECTE'),
            'hors_ligne' => Prado::localize('NON_CONNECTE'),
            'etat_assistant' => Prado::localize('ETAT_ASSISTANT_MARCHES_PUBLICS'),
            'lance_assistant' => Prado::localize('ASSISTANT_MARCHES_PUBLICS_LANCE'),
            'non_lance_assistant' => Prado::localize('ASSISTANT_MARCHES_PUBLICS_NON_LANCE'),
            'afficher_plus_informations' => Prado::localize('AFFICHER_PLUS_INFORMATIONS'),
        ];
        $this->setTranslations(base64_encode(json_encode($translations, JSON_THROW_ON_ERROR)));
    }

    /**
     * @return null
     */
    public function getTranslations()
    {
        return $this->translations;
    }

    /**
     * @param null $translations
     *
     * @return MessageStatusChecker
     */
    public function setTranslations($translations)
    {
        $this->translations = $translations;

        return $this;
    }
}
