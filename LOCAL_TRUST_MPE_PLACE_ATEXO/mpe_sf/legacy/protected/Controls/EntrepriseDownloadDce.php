<?php

namespace Application\Controls;

use App\Utils\Encryption;
use Application\Library\Propel\Exception\PropelException;
use Application\Library\Propel\Propel;
use Application\Pages\Commun\DownloadFile;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationPeer;
use Application\Propel\Mpe\CommonDCE;
use Application\Propel\Mpe\CommonDossierVolumineuxQuery;
use Application\Propel\Mpe\CommonTelechargement;
use Application\Propel\Mpe\CommonTelechargementAnonyme;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Files;
use Application\Service\Atexo\Atexo_InscritOperationsTracker;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_ValeursReferentielles;
use Application\Service\Atexo\Atexo_Zip;
use Application\Service\Atexo\Config\Atexo_Config_Exception;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Dce;
use Application\Service\Atexo\Consultation\Atexo_Consultation_SuppliersOfDocuments;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_PaniersEntreprises;
use Prado\Prado;
use ZipArchive;

/**
 * Permet de telecharger le DCE.
 *
 * @author Anas ZAKI <anas.zaki@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class EntrepriseDownloadDce extends DownloadFile
{
    const APPID = 'DOSSIER_VOLUMINEUX_DOWNLOAD';
    private $_reference;
    private $_organisme;
    private $lieuExecution;
    private $referenceUtilisateur;
    private $idFournisseur;
    private $entiteAchat;
    private bool $_baseDce = false;

    /**
     * Set the value of [reference] column.
     *
     * @param int $v new value
     *
     * @return void
     */
    public function setReference($v)
    {
        $this->_reference = $v;
    }

    // setReference()

    public function setOrganisme($v)
    {
        $this->_organisme = $v;
    }

    // setReference()

    public function setBaseDce($v)
    {
        $this->_baseDce = $v;
    }

    public function getBaseDce()
    {
        return $this->_baseDce;
    }

    public function onLoad($param)
    {
        $this->_reference = Atexo_Util::atexoHtmlEntities($_GET['id']);
        $this->_organisme = Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']);
        /*
         * FIXME Il n'y a pas de panel pour afficher le message d'erreur, donc je commente le check
         *
         if(!$this->checkDce())
         {
         $this->panelTelechargementDCE->visible = false;
         $this->messageErreur->Message = Prado::localize('DEFINE_MESSAGE_ERREUR_INTROUVABLE_ENDOMMAGE');
         }*/
        $consultation = (new Atexo_Consultation())->retrieveConsultationForCompagny($this->_reference, $this->_organisme);
        Atexo_Db::closeCommon();
        if ($consultation) {
            if (false === $this->checkDce()) {
                $this->panelTelechargementComplet->visible = false;
                $this->panelTelechargementPartiel->visible = false;
            }

            $this->panelCompleteDownloadAnnexes->visible = false;
            $this->msgAlerteAnnexeTechnique->setVisible(false);
            if ($consultation->getIdDossierVolumineux()) {
                $this->panelCompleteDownloadAnnexes->visible = true;
                $this->msgAlerteAnnexeTechnique->setVisible(true);
            }

            $this->entiteAchat = Atexo_EntityPurchase::getSigleLibelleEntityById($consultation->getServiceId(), $this->_organisme);
            if (1 != $consultation->getPartialDceDownload()) {
                $this->panelTelechargementPartiel->visible = false;
            }
            $this->displayEnvoiComplement($consultation);
            $this->Page->panelMessageErreur->setVisible(false);
        }
        //$this->annuler();
    }

    public function onInit($param)
    {

        //$this->Master->setCalledFrom("entreprise");Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function getLieuxExecution()
    {
        return $this->lieuExecution;
    }

    public function displayEnvoiComplement($consultation)
    {
        if ($consultation->getTiragePlan() > 0) {
            if ($this->getBaseDce()) {
                $this->panelEnvoiPostauComplementaires->visible = false;
            } else {
                $this->panelEnvoiPostauComplementaires->visible = true;
            }
            if (1 == $consultation->getTiragePlan()) {
                $this->papierPanel->visible = true;
                $this->cdRomPanel->visible = false;
            } elseif (2 == $consultation->getTiragePlan()) {
                $this->papierPanel->visible = false;
                $this->cdRomPanel->visible = true;
            } elseif (3 == $consultation->getTiragePlan()) {
                $this->papierPanel->visible = true;
                $this->cdRomPanel->visible = true;
            }
            $this->idFournisseur = $consultation->getTireurPlan();
            $this->tirageDescriptif->Text = $consultation->getTirageDescriptif();
        } else {
            $this->panelEnvoiPostauComplementaires->visible = false;
        }
    }

    public function getConsultation()
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $consultation = CommonConsultationPeer::retrieveByPK($this->_reference, $connexionCom);

        return $consultation;
    }

    /**
     * retourne le fichier zip.
     */
    public function getDce($callFromOther = false)
    {
        $consultation = $this->getConsultation();
        if ($consultation) {
            $this->referenceUtilisateur = $consultation->getReferenceUtilisateur();
        }
        $dce = (new Atexo_Consultation_Dce())->getDce($this->_reference, $this->_organisme);
        if ($dce instanceof CommonDCE && 0 != $dce->getDce()) {
            $tailleDce = (new Atexo_Consultation_Dce())->getTailleDCE($dce, $this->_organisme);
            if (!$callFromOther && 'agent' == strtolower(Atexo_CurrentUser::getRole())) {
                $this->completeDownload->Text = Prado::localize('TELECHARGER_DCE');
                $this->nomDce->Text = $dce->getNomDce().' - '.$tailleDce;
            }
            $blobResource = Atexo_Blob::acquire_lock_on_blob($dce->getDce(), $this->_organisme);
        } else {
            return false;
        }
        $dceTmpFile = $blobResource['blob']['pointer'];
        //$dceTmpFile = Atexo_Files::rebuild_zip($dceTmpFile, $this->referenceUtilisateur);

        return $dceTmpFile;
    }

    /**
     * check dce.
     */
    public function checkDce()
    {
        $consultation = $this->getConsultation();
        if ($consultation instanceof CommonConsultation) {
            $this->referenceUtilisateur = $consultation->getReferenceUtilisateur();
        }
        $dce = (new Atexo_Consultation_Dce())->getDce($this->_reference, $this->_organisme);
        if ($dce instanceof CommonDCE && 0 != $dce->getDce()) {
            $tailleDce = (new Atexo_Consultation_Dce())->getTailleDCE($dce, $this->_organisme);
            $this->completeDownload->Text = Prado::localize('TELECHARGER_DCE');
            $this->nomDce->Text = $dce->getNomDce().' - '.$tailleDce;
            $blobResource = Atexo_Blob::acquire_lock_on_blob($dce->getDce(), $this->_organisme);
        } else {
            return false;
        }
        $dceTmpFile = $blobResource['blob']['pointer'];

        if (!is_file($dceTmpFile)) {
            return false;
        }


        $zip = new ZipArchive();
        $res = $zip->open($dceTmpFile, ZipArchive::CHECKCONS);
        if (true === $res) {
            $zip->close();
            return true;
        } elseif (21 === $res) {
            /**
             * Fix Bugs pour les Zip provenants de Linux.
             */
            $res = $zip->open($dceTmpFile);
            $zip->close();
            if (true === $res) {
                return true;
            }
        }

        return false;
    }

    /**
     * Fabrique l'arboresconce des pieces du zip.
     *
     * @param $dceItem: tableau des index de DCE
     * @param $maxIndexPieces:
     */
    public function getArborescence($dceItem = [], $maxIndexPieces = false)
    {
        return (new Atexo_Consultation_Dce())->getArborescence($this->_reference, $this->_organisme, $dceItem, $maxIndexPieces, true);
    }

    public function downloadCompleteDce($sender, $param)
    {
        $this->traceInscrit();
        if (true == $this->getBaseDce()) {
            $this->downloadAllDce();
        } else {
            $this->saveDceIntegral();
        }
    }

    //Trace inscrit
    public function traceInscrit()
    {
        $dateDebAction = date('Y-m-d H:i:s');
        $ref = Atexo_Util::atexoHtmlEntities($_GET['id']);
        $org = Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']);
        $idDescription = (new Atexo_ValeursReferentielles())->retrieveByPk('DESCRIPTION7', Atexo_Config::getParameter('DESCRIPTION_HISTORIQUES_INSCRITS'), true);
        $this->traceOperationsInsrctis($idDescription, $dateDebAction, '', $ref, $org);
    }

    public function downloadSelectedFilesForDce($sender, $param)
    {
        $this->traceInscrit();
        $tailleSelectedFiles = 0;
        $arrayIndexSelectedFiles = Atexo_Files::getArrayIndexSelectedFiles($this->getDce(), 'dce_item', $tailleSelectedFiles);
        if ($this->getBaseDce()) {
            $this->downloadSelectedFilefromDce();
        } else {
            $this->saveSelectedFilesName($arrayIndexSelectedFiles, $tailleSelectedFiles);
        }
    }

    public function getTelechargement()
    {
        return $this->Page->getViewstate('telechergement');
    }

    public function saveDceIntegral()
    {
        //Ajout de la consultation au panier de l'entreprise
        if (Atexo_Module::isEnabled('PanierEntreprise')) {
            if (!Atexo_Entreprise_PaniersEntreprises::getPanierEntreprise(
                Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']),
                Atexo_Util::atexoHtmlEntities($_GET['id']),
                Atexo_CurrentUser::getIdEntreprise(),
                Atexo_CurrentUser::getIdInscrit()
            )) {
                Atexo_Entreprise_PaniersEntreprises::AddConsultationToPanierEntreprise(
                    Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']),
                    Atexo_Util::atexoHtmlEntities($_GET['id']),
                    Atexo_CurrentUser::getIdEntreprise(),
                    Atexo_CurrentUser::getIdInscrit()
                );
            }
        }

        $tailleDCE = 0;
        $dce = (new Atexo_Consultation_Dce())->getDce($this->_reference, $this->_organisme);
        if ($dce instanceof CommonDCE) {
            $tailleDCE = $dce->getTailleDce();
        }
        if ($this->Page->EntrepriseFormulaireDemande->choixAnonyme->Checked) {
            //Sauvegarder le telechargement anonymement
            self::sauvegarderTelechargementAnonyme(Prado::localize(Atexo_Config::getParameter('DEFINE_DCE_INTEGRAL')), $tailleDCE);
        }

        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $telechargementCom = $this->getTelechargement();
        if ($telechargementCom) {
            if (!($telechargementCom instanceof CommonTelechargement) && '-1' == $telechargementCom) {
                self::DownloadIntegralDce();
            }

            $nameDceIntegral = Prado::localize(Atexo_Config::getParameter('DEFINE_DCE_INTEGRAL'));
            $dceIntegralFileName = $nameDceIntegral;
            if ($telechargementCom instanceof CommonTelechargement) {
                $telechargementCom->setNomsFichiersDce($dceIntegralFileName);
                $telechargementCom->setPoidsTelechargement($tailleDCE);
                $telechargementCom->save($connexionCom);
                self::DownloadIntegralDce();
            } else {
                $this->redirectToDetailConsultation();
            }
        } else {
            $this->Page->panelMessageErreur->setMessage(Prado::localize('TEXT_COMPTE_NO_CONNECTE'));
            $this->Page->panelMessageErreur->setVisible(true);
        }
    }

    /**
     * enregistre les noms des fichiers du DCE.
     *
     * @param array $arrayIndexSelectedFiles
     */
    public function saveSelectedFilesName($arrayIndexSelectedFiles, $tailleSelectedFiles = 0)
    {
        //Ajout de la consultation au panier de l'entreprise
        if (Atexo_Module::isEnabled('PanierEntreprise')) {
            if (!Atexo_Entreprise_PaniersEntreprises::getPanierEntreprise(
                Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']),
                Atexo_Util::atexoHtmlEntities($_GET['id']),
                Atexo_CurrentUser::getIdEntreprise(),
                Atexo_CurrentUser::getIdInscrit()
            )) {
                Atexo_Entreprise_PaniersEntreprises::AddConsultationToPanierEntreprise(
                    Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']),
                    Atexo_Util::atexoHtmlEntities($_GET['id']),
                    Atexo_CurrentUser::getIdEntreprise(),
                    Atexo_CurrentUser::getIdInscrit()
                );
            }
        }

        if ($this->Page->EntrepriseFormulaireDemande->choixAnonyme->Checked) {
            //Sauvegarder le telechargement anonymement
            self::sauvegarderTelechargementAnonyme(self::getSelectedFiles($arrayIndexSelectedFiles), $tailleSelectedFiles);
        }

        //$dateDebAction = date('Y-m-d H:i:s');
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $telechargementCom = $this->getTelechargement();
        if ($telechargementCom) {
            if (!($telechargementCom instanceof CommonTelechargement) && '-1' == $telechargementCom) {
                (new Atexo_Files())->downloadPartialConsFile($this->_reference, $this->_organisme);
            }
            if ($telechargementCom instanceof CommonTelechargement) {
                $telechargementCom->setNomsFichiersDce(self::getSelectedFiles($arrayIndexSelectedFiles));
                $telechargementCom->setPoidsTelechargement($tailleSelectedFiles);
                $telechargementCom->save($connexionCom);
                (new Atexo_Consultation())->updateNbrDownloadDce($this->_reference, $this->_organisme);
                (new Atexo_Files())->downloadPartialConsFile($this->_reference, $this->_organisme);
            } else {
                $this->redirectToDetailConsultation();
            }
        } else {
            $this->Page->panelMessageErreur->setMessage(Prado::localize('TEXT_COMPTE_NO_CONNECTE'));
            $this->Page->panelMessageErreur->setVisible(true);
        }
    }

    /**
     * envoyer une demande de complements du fournisseur.
     */
    public function demander($sender, $param)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $telechargementCom = $this->getTelechargement();
        $messageTransmitFournisseur = $this->precisionsElements->Text;
        $idFournisseur = $this->idFournisseur;
        $support = 0;

        if ($this->papier->checked && !$this->cdRom->checked) {
            $support = 1;
        } elseif ($this->cdRom->checked && !$this->papier->checked) {
            $support = 2;
        } elseif ($this->papier->checked && $this->cdRom->checked) {
            $support = 3;
        }

        if ($telechargementCom instanceof CommonTelechargement) {
            $telechargementCom->setTiragePlan(1);
            $telechargementCom->setSupport($support);
            $telechargementCom->save($connexionCom);
        }

        $this->sendEmailToSupplier($idFournisseur, $messageTransmitFournisseur, $this->_organisme, $telechargementCom);
        $urlRedirectTo = 'index.php?page=Entreprise.EntrepriseDetailConsultation&id='.$this->_reference.'&orgAcronyme='.$this->_organisme;
        $this->response->redirect($urlRedirectTo);
    }

    /**
     * retour a la page detail d'une consultation.
     */
    public function redirectToDetailConsultation()
    {
        $urlRedirectTo = 'index.php?page=Entreprise.EntrepriseDetailConsultation&id='.$this->_reference.'&orgAcronyme='.$this->_organisme;
        $this->response->redirect($urlRedirectTo);
    }

    /**
     * affiche le bloc de telechargement Partiel.
     */
    public function displayTelechargementPartiel()
    {
        $consultation = $this->getConsultation();
        if ($consultation) {
            if (1 != $consultation->getPartialDceDownload()) {
                $this->panelTelechargementPartiel->visible = false;
            }
        }
    }

    /**
     * envoi de mail au fournisseur de document.
     */
    public function sendEmailToSupplier($idFournisseur, $message, $organisme, $telechargementCom)
    {
        $entreprise = null;
        $prenomInscrit = null;
        $mailInscrit = null;
        $adresse = null;
        $adresseSuite = null;
        $codePostal = null;
        $ville = null;
        $pays = null;
        $telephone = null;
        $fax = null;
        $mailSupplier = (new Atexo_Consultation_SuppliersOfDocuments())->getMailSupplierById($idFournisseur, $organisme);
        $from = Atexo_Config::getParameter('PF_MAIL_FROM');
        $to = $mailSupplier;
        if ($telechargementCom) {
            $entreprise = $telechargementCom->getEntreprise() ?: '-';
            $nomInscrit = $telechargementCom->getNom();
            $prenomInscrit = $telechargementCom->getPrenom();
            $mailInscrit = $telechargementCom->getEmail();
            $adresse = $telechargementCom->getAdresse() ?: '-';
            $adresseSuite = $telechargementCom->getAdresse2() ?: '-';
            $codePostal = $telechargementCom->getCodepostal() ?: '-';
            $ville = $telechargementCom->getVille() ?: '-';
            $pays = $telechargementCom->getPays() ?: '-';
            $telephone = $telechargementCom->getTelephone() ?: '-';
            $fax = $telechargementCom->getFax() ?: '-';
        }
        $subject = Prado::localize('TEXT_DEMANDE_DOCUMENT_CONSULTATION').' '.$this->referenceUtilisateur.' '.Prado::localize('DEFINE_TEXT_DE').' '.$this->entiteAchat;
        $htmlMsg = new Atexo_Message();
        $corpsMail = $htmlMsg->render('PATH_FILE_MAIL_DEMANDE_COMPLEMENT_CONSULTATION_TEMPLATE');
        $corpsMail = (new Atexo_Message())->getInfoConsultationV2($this->_reference, $organisme, false, $corpsMail);
        $corpsMail = str_replace('IMAGE_BANDEAU', Atexo_Message::getImageBandeau(), $corpsMail);
        $corpsMail = str_replace('DEFINE_TEXT_ENTITE_PUBLIC', Prado::localize('DEFINE_TEXT_ENTITE_PUBLIC'), $corpsMail);
        $corpsMail = str_replace('DEFINE_ENTITE_ACHAT', Prado::localize('DEFINE_ENTITE_ACHAT'), $corpsMail);
        $corpsMail = str_replace('DEFINE_TEXT_ENTITE_PUBLIC', Prado::localize('DEFINE_TEXT_ENTITE_PUBLIC'), $corpsMail);
        $corpsMail = str_replace('DEFINE_INTITULE_CONSULTATION', Prado::localize('DEFINE_INTITULE_CONSULTATION'), $corpsMail);
        $corpsMail = str_replace('DEFINE_OBJET_CONSULTATION', Prado::localize('DEFINE_OBJET_CONSULTATION'), $corpsMail);
        $corpsMail = str_replace('DEFINE_TEXT_REFERENCE', Prado::localize('DEFINE_TEXT_REFERENCE'), $corpsMail);
        $corpsMail = str_replace('TEXT_DATE_HEURE_LIMITE_REMISE_PLIS', Prado::localize('TEXT_DATE_HEURE_LIMITE_REMISE_PLIS'), $corpsMail);
        $corpsMail = str_replace('DEFINE_DATE_MISE_LIGNE', Prado::localize('DEFINE_DATE_MISE_LIGNE'), $corpsMail);
        $corpsMail = str_replace('TEXT_UTILISATEUR_ENTREPRISE', Prado::localize('TEXT_UTILISATEUR_ENTREPRISE'), $corpsMail);
        $corpsMail = str_replace('TEXT_TYPE_PROCEDURE', Prado::localize('TEXT_TYPE_PROCEDURE'), $corpsMail);

        $corpsMail = str_replace('TEXT_TITRE', Prado::localize('TEXT_ENTREPRISE_DEMANDE_COMPLEMENT_CONSULTATION'), $corpsMail);
        $corpsMail = str_replace('TEXT_CETTE_DEMANDE_LASUIVANTE', Prado::localize('TEXT_CETTE_DEMANDE_LASUIVANTE'), $corpsMail);
        $corpsMail = str_replace('TEXT_COORDONNEES_DEMANDEUR', Prado::localize('TEXT_COORDONNEES_DEMANDEUR'), $corpsMail);
        $corpsMail = str_replace('TEXT_ENTREPRISE', Prado::localize('TEXT_ENTREPRISE'), $corpsMail);
        $corpsMail = str_replace('TEXT_UTILISATEUR_ENTREPRISE ', Prado::localize('TEXT_UTILISATEUR_ENTREPRISE'), $corpsMail);
        $corpsMail = str_replace('ADRESSE_ELECTRONIQUE', Prado::localize('ADRESSE_ELECTRONIQUE'), $corpsMail);
        $corpsMail = str_replace('TEXT_ADRESSE', Prado::localize('TEXT_ADRESSE'), $corpsMail);
        $corpsMail = str_replace('TEXT_TELEPHONE', Prado::localize('TEXT_TELEPHONE'), $corpsMail);
        $corpsMail = str_replace('TEXT_FAX', Prado::localize('TEXT_FAX'), $corpsMail);

        $corpsMail = str_replace('VALEUR_CETTE_DEMANDE_LASUIVANTE', $message, $corpsMail);
        $corpsMail = str_replace('VALEUR_ENTREPRISE', $entreprise, $corpsMail);
        $corpsMail = str_replace('VALEUR_UTILISATEUR_ENTREPRISE', $prenomInscrit, $corpsMail);
        $corpsMail = str_replace('VALEUR_ADRESSE_ELECTRONIQUE', $mailInscrit, $corpsMail);
        $corpsMail = str_replace('VALEUR_ADRESSE', $adresse.' '.$adresseSuite.$codePostal.' '.$ville.' '.$pays, $corpsMail);
        $corpsMail = str_replace('VALEUR_TELEPHONE', $telephone, $corpsMail);
        $corpsMail = str_replace('VALEUR_FAX', $fax, $corpsMail);
        $corpsMail = str_replace('PF_ACHAT_D_ETAT', Prado::localize('PF_ACHAT_D_ETAT'), $corpsMail);

        if ('' != $mailSupplier) {
            Atexo_Message::simpleMail($from, $to, $subject, $corpsMail, '', '', false, true);
        }
    }

    /**
     * Permet de sauvegarder un telechargement anonyme.
     *
     * @param unknown_type $typeTelechargement
     */
    public function sauvegarderTelechargementAnonyme($fichiers, $tailleSelectedFiles = 0)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $telechargement = new CommonTelechargementAnonyme();
        $telechargement->setConsultationId($this->_reference);
        $telechargement->setOrganisme($this->_organisme);
        $telechargement->setDatetelechargement(date('Y-m-d H:i:s'));
        $telechargement->setNomsFichiersDce($fichiers);
        $telechargement->setPoidsTelechargement($tailleSelectedFiles);
        //Sauvegarde du telechargement
        $telechargement->save($connexionCom);
    }

    /**
     * Retourne les noms des fichiers selectionnes dans une chaine.
     */
    public function getSelectedFiles($arrayIndexSelectedFiles)
    {
        $blob_file = $this->getDce();
        $arrayZipFiles = Atexo_Zip::getOrderedFilesForZip($blob_file, false);
        $arraySelectedFilesName = [];
        foreach ($arrayZipFiles as $chemin => $zipFiles) {
            foreach ($zipFiles as $oneFile) {
                if (in_array($oneFile['index'], $arrayIndexSelectedFiles)) {
                    $oneFile['name'] = Atexo_Util::convertFileToHttpEncoding($oneFile['name']);
                    $arraySelectedFilesName[] = $chemin.'/'.$oneFile['name'];
                }
            }
        }
        $chaineSelectedFilesName = '#';
        foreach ($arraySelectedFilesName as $filePath) {
            $chaineSelectedFilesName .= $filePath.'#';
        }

        return $chaineSelectedFilesName;
    }

    public function downloadAllDce()
    {
        if ($this->_reference && $this->_organisme) {
            $this->response->redirect('index.php?page=frame.DownloadDceForBaseDce&id='.$this->_reference.'&orgAcronym='.$this->_organisme);
        } else {
            $this->Page->panelMessageErreur->setMessage(Prado::localize('ERREUR_TEL_FICHIER'));
            $this->Page->panelMessageErreur->setVisible(true);
        }
    }

    public function downloadSelectedFilefromDce()
    {
        if ($this->_reference && $this->_organisme) {
            (new Atexo_Files())->downloadPartialConsFile($this->_reference, $this->_organisme);
        } else {
            $this->Page->panelMessageErreur->setMessage(Prado::localize('ERREUR_TEL_FICHIER'));
            $this->Page->panelMessageErreur->setVisible(true);
        }
    }

    public function getUrlNewDownload()
    {
        if ($this->getBaseDce()) {
            $url = 'index.php?page=Entreprise.popUpDetailsDce&id='.Atexo_Util::atexoHtmlEntities($_GET['id']).
                '&orgAcronyme='.Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']);
        } else {
            $url = 'index.php?page=Entreprise.EntrepriseDemandeTelechargementDce&id='.Atexo_Util::atexoHtmlEntities($_GET['id']).
                '&orgAcronyme='.Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']);
        }

        return $url;
    }

    /*
      * Permet de tracer l'acces de l'inscrit a la PF
      */
    public function traceOperationsInsrctis($IdDesciption, $dateDebAction, $details = '', $ref = '', $org = '')
    {
        $description = (new Atexo_ValeursReferentielles())->retrieveByIdValAndIdRef($IdDesciption, Atexo_Config::getParameter('DESCRIPTION_HISTORIQUES_INSCRITS'), true);

        $arrayDonnees = [];
        $arrayDonnees['ref'] = $ref;
        $arrayDonnees['org'] = $org;
        $arrayDonnees['IdDescritpion'] = $IdDesciption;
        $arrayDonnees['afficher'] = true;
        $arrayDonnees['logApplet'] = '';
        $arrayDonnees['description'] = $description;
        if (null === $description) {
            $description = '-';
        }
        Atexo_InscritOperationsTracker::trackingOperations(
            Atexo_CurrentUser::getIdInscrit(),
            Atexo_CurrentUser::getIdEntreprise(),
            $_SERVER['REMOTE_ADDR'],
            date('Y-m-d'),
            $dateDebAction,
            substr($_SERVER['QUERY_STRING'], 0),
            $description,
            date('Y-m-d H:i:s'),
            $arrayDonnees
        );
    }

    /**
     * Permet de telecharger le DCE de la consultation en cours.
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.10.0
     *
     * @copyright Atexo 2015
     */
    public function DownloadIntegralDce()
    {
        $consultation = (new Atexo_Consultation())->retrieveConsultationForCompagny($this->_reference, $this->_organisme);
        Atexo_Db::closeCommon();
        if ($consultation instanceof CommonConsultation) {
            $dce = (new Atexo_Consultation_Dce())->getDce($this->_reference, $this->_organisme);
            if ($dce instanceof CommonDCE) {
                if ($dce instanceof CommonDCE && 0 != $dce->getDce()) {
                    //Téléchargement du DCE
                    $this->downloadFiles($dce->getDce(), $dce->getNomDce(), $this->_organisme);
                }
                // Mise à jour du nombre de téléchargement du DCE
                (new Atexo_Consultation())->updateNbrDownloadDce($this->_reference, $this->_organisme);
            }
        }
    }

    /* ***********************************************************************
     *                            Gestion des Annexes
     * ***********************************************************************
     */

    /**
     * @param $sender
     * @param $param
     */
    public function downloadCompleteAnnexes($sender, $param)
    {
        $consultation = (new Atexo_Consultation())->retrieveConsultationForCompagny($this->_reference, $this->_organisme);

        if (null !== $consultation->getIdDossierVolumineux()) {
            $connexionDo = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            $commonDossierVolumineuxQuery = new CommonDossierVolumineuxQuery(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            $dossierVolumineux = $commonDossierVolumineuxQuery->findOneById($consultation->getIdDossierVolumineux(), $connexionDo);
        }

        $this->traceInscritAnnexes();
        $tailleDCE = 0;
        if ($this->_reference && $this->_organisme) {
            if ($this->Page->EntrepriseFormulaireDemande->choixAnonyme->Checked) {
                //Sauvegarder le telechargement anonymement
                self::sauvegarderTelechargementAnonyme(Prado::localize('DEFINE_DCE_ANNEXES_TECHNIQUE'), $tailleDCE);
            }

            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $telechargementCom = $this->getTelechargement();
            if ($telechargementCom) {
                if (!($telechargementCom instanceof CommonTelechargement) && '-1' == $telechargementCom) {
                    $this->downloadAnnexes($dossierVolumineux->getUuidReference());
                    exit();
                }

                $nameDceIntegral = Prado::localize('DEFINE_DCE_ANNEXES_TECHNIQUE');
                $dceIntegralFileName = $nameDceIntegral;
                if ($telechargementCom instanceof CommonTelechargement) {
                    $telechargementCom->setNomsFichiersDce($dceIntegralFileName);
                    $telechargementCom->setPoidsTelechargement($tailleDCE);
                    $telechargementCom->save($connexionCom);

                    $this->downloadAnnexes($dossierVolumineux->getUuidReference());
                    exit();
                } else {
                    $this->redirectToDetailConsultation();
                }
            } else {
                $this->Page->panelMessageErreur->setMessage(Prado::localize('TEXT_COMPTE_NO_CONNECTE'));
                $this->Page->panelMessageErreur->setVisible(true);
            }
        } else {
            $this->Page->panelMessageErreur->setMessage(Prado::localize('ERREUR_TEL_FICHIER'));
            $this->Page->panelMessageErreur->setVisible(true);
        }
    }

    /**
     * Trace inscrit Annexes.
     *
     * @throws Atexo_Config_Exception
     */
    public function traceInscritAnnexes()
    {
        $dateDebAction = date('Y-m-d H:i:s');
        $ref = Atexo_Util::atexoHtmlEntities($_GET['id']);
        $org = Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']);
        $idDescription = (new Atexo_ValeursReferentielles())->retrieveByPk('ANNEXES1', Atexo_Config::getParameter('DESCRIPTION_HISTORIQUES_INSCRITS'), true);
        $this->traceOperationsInsrctis($idDescription, $dateDebAction, '', $ref, $org);
    }

    /**
     * @param $uuidReference
     *
     * @throws Atexo_Config_Exception
     */
    public function downloadAnnexes($uuidReference)
    {
        $url = Atexo_Config::getParameter('URL_LT_MPE_SF').'/entreprise/dossier-volumineux/download/'.$uuidReference;
        $this->response->redirect($url);
    }

    /**
     * @param string $idDossierVolumineux
     * @return string
     * @throws PropelException
     */
    public function getJsonDossierVolumineux() : string
    {
        $consultation = (new Atexo_Consultation())->retrieveConsultationForCompagny($this->_reference, $this->_organisme);
        $connexionDo = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $commonDossierVolumineuxQuery = new CommonDossierVolumineuxQuery(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $dossierVolumineux = $commonDossierVolumineuxQuery->findOneById($consultation->getIdDossierVolumineux(), $connexionDo);
        $serviceDossierVolumineuxService = Atexo_Util::getSfService('atexo.dossier_volumineux_service');
        $serviceUtil = Atexo_Util::getSfService('atexo.atexo_util');
        $dossierVolumineux = $serviceDossierVolumineuxService->getDossierVolumineuxById($dossierVolumineux->getId());
        $paramsDV = [
            'reference' => $dossierVolumineux->getUuidReference(),
            'nomDossierVolumineux' => $dossierVolumineux->getNom(),
            'serverLogin' => Atexo_Config::getParameter('LOGIN_TIERS_TUS'),
            'serverPassword' => Atexo_Config::getParameter('PWD_TIERS_TUS'),
            'serverURL' => $serviceUtil->getPfUrlDomaine(Atexo_Config::getParameter('PF_URL_REFERENCE')),
            'statut' => $dossierVolumineux->isActif() ? '1' : '0',
        ];
        $callback = $serviceDossierVolumineuxService->callTusSession($paramsDV, $dossierVolumineux->getUuidTechnique());
        return json_encode([
            "sessionId" => $callback['uuid'],
            "appId" =>  self::APPID,
            "urlEnvol" => Atexo_Config::getParameter('ENVOL_URL')
        ]);
    }

    public function saveTraceInscrit($sender, $param)
    {
        $this->traceInscritAnnexes();
        $tailleDCE = 0;
        if ($this->_reference && $this->_organisme) {
            if ($this->Page->EntrepriseFormulaireDemande->choixAnonyme->Checked) {
                //Sauvegarder le telechargement anonymement
                self::sauvegarderTelechargementAnonyme(Prado::localize('DEFINE_DCE_ANNEXES_TECHNIQUE'), $tailleDCE);
            }
        }
    }
}
