<div class="bloc-login float">
	<div class="form-field">
		<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
		<div class="content">
			<div class="top-title">
				<span class="big"><com:TTranslate>TEXT_NOUVEL_UTILISATEUR</com:TTranslate></span>
				<span class="small"><com:TTranslate>TEXT_INSCRIVEZ_VOUS</com:TTranslate></span>
			</div>
			<!--Debut Bloc Inscription Etablissement Francais-->
			<com:TPanel ID="panelSiren" visible="false" cssClass="form-bloc" DefaultButton="buttonSirenOk">
			
				<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
				<div class="content">
					<h5>
	 							<com:TTranslate>TEXT_ENTREPRISE_EN_FRANCE</com:TTranslate>
					</h5>
					<div class="line">
						<com:TPanel ID="panelLabelSiren" cssClass="intitule-100 line-height-normal">
	     								<label for="siren">
	     								<com:TTranslate>TEXT_SIREN</com:TTranslate><com:TTranslate>TEXT_SLASH</com:TTranslate></label>
	     								<label for="siret"><com:TTranslate>TEXT_SIRET</com:TTranslate></label>
	                                     
						</com:TPanel>
						<div class="content-bloc bloc-240">
							<com:TTextBox ID="siren" CssClass="siren" MaxLength="9"
	                      			 Attributes.title="<%=Prado::localize('TEXT_SIREN')%>" />
	                      			
	                      		<com:TTextBox ID="siret"  CssClass="siret"
	                      			MaxLength="5"  Attributes.title="<%=Prado::localize('TEXT_SIRET')%>" />
	                      			
	                      			<com:PictoOk 
	                      			ID="buttonSirenOk"
	                      			onClick="siretOkButton"
	                      			ValidationGroup="sirenValidationGroup" /> 
	                              <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" onmouseover="afficheBulle('infosSiret', this)" onmouseout="cacheBulle('infosSiret')" class="picto-info-intitule" alt="Info-bulle" title="Info-bulle" />
	                              
	                  			<com:TCustomValidator
	                  				ID="SirenValidator"
	                  				ValidationGroup="sirenValidationGroup"
	                  				ControlToValidate="siren"
	                  				Text="<br/><%=Prado::localize('MSG_ERROR_SIRET_INVALIDE')%>"
	                  				Display="Dynamic"
	                  				ClientValidationFunction="controlValidationSiret" />
	                             </div>
	
					</div>
					<div class="breaker"></div>
				</div>
				<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
			</com:TPanel>
			<com:TPanel ID="panelRc" cssClass="form-bloc" DefaultButton="buttonRcOk">
				<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
				<div class="content tall-110">
					<h5>
	 							<com:TTranslate>TEXT_ENTREPRISE_EN_FRANCE</com:TTranslate>
					</h5>
					<div class="line">
						<div class="intitule-100 line-height-normal">
							<label for="<%=$this->getClientId()%>villeRc"><com:TTranslate>DEFINE_VILLE_RC</com:TTranslate> :</label>  
						</div>
						<com:TDropDownList ID="RcVille" Attributes.title="<%=Prado::localize('DEFINE_VILLE_RC')%>" style="width:190px;"/>
	                 		  	<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" onmouseover="afficheBulle('infosSiret', this)" onmouseout="cacheBulle('infosSiret')" class="picto-info-intitule" alt="Info-bulle" title="Info-bulle" />
					</div>
					
					<div class="line">
						<div class="intitule-100 line-height-normal">
							<label for="<%=$this->getClientId()%>numeroRc"><com:TTranslate>DEFINE_NUMERO</com:TTranslate></label> :
						</div>
	                             <div class="content-bloc bloc-240">
							<com:TTextBox  id="RcNumero" Attributes.title="<%=Prado::localize('DEFINE_NUMERO')%>" cssClass="id-national" />
							<com:PictoOk 
								ID="buttonRcOk"
	                      			onClick="RcOk"
								ValidationGroup="RcValidationGroup" 
							  />
							 <com:THiddenField ID="Rc" Value="<%=$this->Rc->getClientId()%>"/>
							<com:TCustomValidator
	            				ValidationGroup="RcValidationGroup"
	            				ControlToValidate="Rc"
	            				Text="<br/><%=Prado::localize('MSG_ERROR_OBLIGATORY_NUM_RC')%>"
	            				Display="Dynamic"
	            				EnableClientScript="true"
	            				ClientValidationFunction="controlValidationNumRc" />
	                         	</div>
					</div>
					<div class="breaker"></div>
				</div>
				<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
			
			</com:TPanel>
			<!--Fin Bloc Inscription Etablissement Francais-->
			<!--Debut Bloc Identifiant Unique -->
			<com:TPanel ID="panelIdentifiantUnique" visible="false" cssClass="form-bloc" DefaultButton="buttonidentifiantUniqueOk">
			
				<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
				<div class="content">
					<h5>
	 							<com:TTranslate>TEXT_ENTREPRISE_EN_FRANCE</com:TTranslate>
					</h5>
					<div class="line">
						<com:TPanel ID="panelLabelIdentifiantUnique" cssClass="intitule-100 line-height-normal">
	 								
	     								<label for="siren">
	     								<com:TTranslate>TEXT_SIREN</com:TTranslate> :</label>
	 								
						</com:TPanel>
	                             <div class="content-bloc bloc-240">
							<com:TTextBox ID="identifiantUnique" CssClass="id-national"
	                      			 Attributes.title="<%=Prado::localize('TEXT_SIREN')%>" />
	                      		<com:PictoOk ID="buttonidentifiantUniqueOk" onClick="identifiantUniqueOkPressed" />
	                              <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" onmouseover="afficheBulle('infosSiret', this)" onmouseout="cacheBulle('infosSiret')" class="picto-info-intitule" alt="Info-bulle" title="Info-bulle" />
	                             </div>
					</div>
					<div class="breaker"></div>
				</div>
				<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
			</com:TPanel>
			<!--Fin Bloc Identifiant Unique -->
			<!--Debut Bloc Inscription Autre Pays-->
			<com:TPanel cssClass="form-bloc" DefaultButton="buttonidNationalOk">
				<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
				<div class="content">
					<h5><com:TTranslate>TEXT_ENTREPRISE_ETRANGER</com:TTranslate><img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" onmouseover="afficheBulle('infosIdentifiantNational', this)" onmouseout="cacheBulle('infosIdentifiantNational')" class="picto-info-intitule" alt="Info-bulle" title="Info-bulle" /></h5>
					<div class="line">
						<div class="intitule-100 line-height-normal"><label for="<%=$this->getClientId()%>listPays"><com:TTranslate>TEXT_COUNTRY</com:TTranslate></label> :</div>
						<div class="content-bloc bloc-240">
	                              <com:AtexoDropDownListCountries TypeAffichage="withCode" ID="listPays" style="width:190px;" PostBack="<%=$this->iscallBack%>" Attributes.onclick="displayCountries2(<%=$this->listPays->getClientId()%>,<%=$this->refreshListPays->getClientId()%>);" />
							<span style="display:none"><com:TActiveButton id="refreshListPays" onCallback="refreshListPays" /></span>
	                                 <div class="breaker"></div>
							<com:TCustomValidator
	                  				ValidationGroup="PaysEtrangerValidationGroup"
	                  				ControlToValidate="listPays"
	                  				Text="<%=Prado::localize('TEXT_SELECTIONNEZ_UN_PAYS')%>"
	                  				Display="Dynamic"
	                  				ClientValidationFunction="controlValidationListPays" />
	                             </div>
					</div>
					<div class="line">
						<div class="intitule-100 line-height-normal">
	                             <label for="<%=$this->getClientId()%>idNational"><com:TTranslate>TEXT_IDENTIFIANT_NATIONNAL</com:TTranslate></label> :</div>
	                             <div class="content-bloc bloc-240"> 
							<com:TTextBox  id="idNational" Attributes.title="<%=Prado::localize('TEXT_IDENTIFIANT_NATIONNAL')%>" cssClass="id-national" />
							<com:PictoOk ID="buttonidNationalOk"  ValidationGroup="PaysEtrangerValidationGroup" onClick="idNationalOkPressed" />
	                                 <div class="breaker"></div>
	                                 <div class="info-aide"><com:TTranslate>TEXT_NUM_ENREGISTREMENT_NATIONAL</com:TTranslate></div>
							<com:TRequiredFieldValidator 
	  									ControlToValidate="idNational"
	  									ValidationGroup="PaysEtrangerValidationGroup"
	  									Display="Dynamic"
	  									EnableClientScript="true"  						 					 
	  						 			Text="<br/><%=Prado::localize('MSG_ERROR_OBLIGATORY_ID_CONTRY')%>" />
							
	                             </div>
					</div>
					<div class="breaker"></div>
				</div>
				<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
			</com:TPanel>
			<!--Fin Bloc Inscription Autre Pays-->
			<div class="breaker"></div>
		</div>
		<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
	</div>
</div>
<div id="infosSiret" class="info-bulle" onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();"><div><com:TTranslate>TEXT_INFO_BULLE_SIREN</com:TTranslate></div></div>
<div id="infosIdentifiantNational" class="info-bulle" onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();"><div><com:TTranslate>TEXT_INFO_BULLE_ENTREPRISE_ETR</com:TTranslate></div></div>
<com:TConditional condition="$this->isSiretAlertPanelCreationInscrit()">
	<prop:trueTemplate>
		<script language="JavaScript"
				type="text/JavaScript"
				src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/js/entreprise.js?k=<%=Application\Service\Atexo\Atexo_Config::getParameter('MEDIA_CACHE_KEY')%>"
				url="/app.php/entreprise/verification"
		></script>
	</prop:TrueTemplate>
</com:TConditional>