	<!--Debut colonne gauche-->
	<div class="menu-gauche">
		<!--Debut menu -->
		<div id="menu">
			<ul id="menuList">
				<li class="menu-open menu-on"><span><a onclick="toggleMenu('menuSuivi');" ><com:TTranslate>TEXT_SUIVI_ET_RECHERCHE</com:TTranslate></span>
					<ul class="ss-menu-open" id="menuSuivi" style="display: block;">
						<li class="off"><a href="?page=gestionPub.TableauDeBordAvisPublicite"><com:TTranslate>DEFINE_TABLEAU_DE_BORD</com:TTranslate></a></li>
						<li class="off"><label for="quickSearch" style="display: none;"><com:TTranslate>TEXT_RECHERCHE_RAPIDE</com:TTranslate></label>
						<com:TTextBox ID="quickSearch"
										CssClass="rechercher"
										Text="Recherche rapide"
										Attributes.title="Recherche rapide"
										Attributes.onfocus="clearOnFocus(event,this);"
										Attributes.onmouseover="clearOnFocus(event,this);"
										style="display:none"
										/>
						<com:TImage ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/bouton-ok.gif" Attributes.alt="OK" Attributes.title="OK" style="display:none"/>
						<li class="off"><a href="?page=gestionPub.FormulaireRechercheAvisPub"><com:TTranslate>TEXT_RECHERCHE_AVANCEE</com:TTranslate></a></li>
					</ul>

				</li>
				<li class="menu-open"><span><a onclick="toggleMenu('menuAvisNationaux');"><com:TTranslate>TEXT_AVIS_NATIONAUX</com:TTranslate></a></span>
					<ul class="ss-menu-open" id="menuAvisNationaux" style="display: block;">
						<li><a href="?page=gestionPub.FormulaireRechercheAvisPub&region=national"><com:TTranslate>TEXT_TOUS</com:TTranslate></a></li>
						<li><a href="?page=gestionPub.FormulaireRechercheAvisPub&aTraiter&region=national"><com:TTranslate>TEXT_A_TRAITER</com:TTranslate></a></li>
						<li style="display:<%=(!(new Application\Service\Atexo\Publicite\Atexo_Publicite_AvisPub)->isAccesGestionPubSip())? 'block':'none'%>"><a href="?page=gestionPub.FormulaireRechercheAvisPub&attenteValidationSip&region=national"><com:TTranslate>TEXT_ATTENTE_VALIDATION_SIP</com:TTranslate></a></li>
						<li ><a href="?page=gestionPub.FormulaireRechercheAvisPub&envoiPlanifie&region=national"><com:TTranslate>TEXT_ENVOI_PLANIFIE</com:TTranslate></a></li>
						<li><a href="?page=gestionPub.FormulaireRechercheAvisPub&envoye&region=national"><com:TTranslate>TEXT_ENVOYE</com:TTranslate></a></li>
						<li><a href="?page=gestionPub.FormulaireRechercheAvisPub&rejete&region=national"><com:TTranslate>TEXT_REJETE</com:TTranslate></a></li>
					</ul>
				</li>
				<li class="menu-open"><span><a onclick="toggleMenu('menuAvisEuropeens');" href="javascript:void(0);"><com:TTranslate>TEXT_AVIS_EUROPEENS</com:TTranslate></a></span>
					<ul class="ss-menu-open" id="menuAvisEuropeens" style="display: block;">
						<li><a href="?page=gestionPub.FormulaireRechercheAvisPub&region=europeen"><com:TTranslate>TEXT_TOUS</com:TTranslate></a></li>
						<li><a href="?page=gestionPub.FormulaireRechercheAvisPub&aTraiter&region=europeen"><com:TTranslate>TEXT_A_TRAITER</com:TTranslate></a></li>
						<li style="display:<%=(!(new Application\Service\Atexo\Publicite\Atexo_Publicite_AvisPub)->isAccesGestionPubSip())? 'block':'none'%>"><a href="?page=gestionPub.FormulaireRechercheAvisPub&attenteValidationSip&region=europeen"><com:TTranslate>TEXT_ATTENTE_VALIDATION_SIP</com:TTranslate></a></li>
						<li ><a href="?page=gestionPub.FormulaireRechercheAvisPub&envoiPlanifie&region=europeen"><com:TTranslate>TEXT_ENVOI_PLANIFIE</com:TTranslate></a></li>
						<li><a href="?page=gestionPub.FormulaireRechercheAvisPub&envoye&region=europeen"><com:TTranslate>TEXT_ENVOYE</com:TTranslate></a></li>
						<li><a href="?page=gestionPub.FormulaireRechercheAvisPub&rejete&region=europeen"><com:TTranslate>TEXT_REJETE</com:TTranslate></a></li>
					</ul>
				</li>
			</ul>
		</div>
		<div class="menu-bottom"></div>
		<!--Fin menu -->
	</div>