<?php

namespace Application\Controls;

use App\Service\Error\ErrorAgent as ErrorAgentSF;
use Application\Service\Atexo\Atexo_Util;

class ErrorAgent extends MpeTTemplateControl
{
    public function render($writer)
    {
        /** @var ErrorAgentSF $error */
        $error = Atexo_Util::getSfService(ErrorAgentSF::class);

        echo $error->render();
    }
}
