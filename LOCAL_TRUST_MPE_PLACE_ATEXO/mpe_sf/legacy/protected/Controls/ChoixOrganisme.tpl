<!--Debut Bloc Choix Entite d'achat-->
<div class="form-bloc margin-0" id="recapConsultation">
	<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
	<com:TActivePanel id="panelChoixMinistere" CssClass="content">
		<h2>
			<com:TLabel ID="labelTitileAdministration" visible="true" Text="<%=Prado::localize('DEFINE_ADMINISTRER_MINISTERE')%>" />
			<com:TLabel  ID="labelTitileRecherche"  visible="false" Text="<%=Prado::localize('TEXT_RECHERCHE_ENTITE_PUBLIC')%>" />
		</h2>
		<div class="line">
			<div class="intitule-100">
				<label for="organisme"><com:TTranslate>TEXT_MINISTERE</com:TTranslate> :</label>
			</div>
		    <com:TActiveDropDownList id="organisme" Attributes.title="<%=Prado::localize('TEXT_MINISTERE')%>" onCallBack="displayEntityPurchase" CssClass="bloc-630"/>
		</div>
		<com:TPanel CssClass="line" Visible="<%=$this->getShowService()=='true'%>">
			<div class="intitule-100">
				<label for="autreEntite"><com:TTRanslate>DEFINE_ENTITE_ACHAT</com:TTRanslate> : </label>
			</div>
			<com:TActiveDropDownList id="listeEntites" Attributes.title="<%=Prado::localize('DEFINE_ENTITE_ACHAT')%>" CssClass="bloc-630"/>
			<com:TActiveImageButton Visible="<%=$this->getShowPictoOk()=='true'%>" id="buttonOk" CssClass="ok" onCallBack="Page.onClickGlobalSearch" Attributes.title="Ok" AlternateText="Ok"  ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/bouton-ok.gif"/>
		</com:TPanel>
		<div class="breaker"></div>
	</com:TActivePanel>
	<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
</div>
<com:TActiveLabel id="hiddenIdService"  Visible="True"/>
<com:TActiveLabel ID="js"/>
<div class="spacer-mini"></div>
<!--Fin Bloc Choix Entite d'achat-->		
		