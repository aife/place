<?php

namespace Application\Controls;

use Application\Library\Propel\Exception\PropelException;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonGeolocalisationN2;
use Application\Propel\Mpe\CommonTContratTitulaire;
use Application\Service\Atexo\Atexo_CommonReferentiel;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Contrat;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Departement;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_TrancheBudgetaire;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Config\Atexo_Config_Exception;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Category;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Contrat;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureType;
use Application\Service\Atexo\Contrat\Atexo_Contrat_Gestion;
use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2;
use Prado\Prado;

/**
 * permet de lister les differents telechargement des archives effectues.
 *
 * @author Fatima Ezzahra IZGUA <fatima.izgua@atexo.com>
 *
 * @version 1.0
 *
 * @since 4.11.0
 *
 * @copyright Atexo 2015
 * @
 */
class AttributaireComposant extends MpeTTemplateControl
{
    private $contrat;
    private $codeDenomAdapte;

    public function setContrat($v)
    {
        $this->setViewState('contrat', $v);
    }

    public function getContrat()
    {
        return $this->getViewState('contrat');
    }

    public function initialiserComposant()
    {
        $contrat = $this->getContrat();

        if ($contrat instanceof CommonTContratTitulaire) {
            if ($this->getContratAvecMontant($contrat) || !$this->getContratAvecChapeau($contrat)) {
                $this->panelInfoContrat->setVisible(true);
                // L'ordre est iportant le populateData doit etre à la fin
                $this->fillCcagReference();

                if (!empty($contrat->getNomLieuPrincipalExecution())) {
                    $this->lieuPrincipalExecution->setData($contrat->getNomLieuPrincipalExecution());
                } else {
                    $lieuxExecutionArray = explode(',', $contrat->getLieuExecution());
                    $lieuxExecution = array_values(array_filter($lieuxExecutionArray, fn($value) => '' !== $value));

                    $lieuExecution = (new Atexo_Geolocalisation_GeolocalisationN2())->retrieveGeolocalisationN2ById($lieuxExecution[0]);

                    if (!empty($lieuExecution)) {
                        $this->lieuPrincipalExecution->setData($lieuExecution[0]->getDenomination1());
                    }
                }

                $this->chargerCategories();
                $this->chargerFormePrix($contrat);
                $this->chargerMarcheDefense($contrat);
                $this->visibliteComponent($contrat);
                $this->populateDataInformationContrat();
            } else {
                $this->panelInfoContrat->setVisible(false);
            }
            if ($this->getContratAvecArticle133($contrat)) {
                self::initialiserPublicationmarche($contrat);
            }

            $this->populateDataDate();

            if ($contrat->isTypeContratConcession()) {
                $this->montant_lot_1_attributaire_1->setDisplay('None');
                $this->montant_lot_1_attributaire_1_concession->setDisplay('Dynamic');

                $this->info_bulle_valeur_globale_estimee->setDisplay('Dynamic');
                $this->panelProcedurePassation->setVisible(true);
                $this->panelNatureConcession->setVisible(true);
                $this->panelMontantSubvention->setVisible(true);
                $this->panelDateExecution->setVisible(true);

                $this->chargerProceduresConcessionPivot();
                $this->chargerNatureContrat();
            } else {
                $this->panelProcedurePassation->setVisible(false);
                $this->panelNatureConcession->setVisible(false);
                $this->panelMontantSubvention->setVisible(false);
                $this->panelDateExecution->setVisible(false);
            }
        } else {
            $this->panelInfoContrat->setVisible(false);
        }
    }

    /**
     * Remplir liste CCAG.
     *
     * @return void
     *
     * @throws Atexo_Config_Exception
     *
     * @author Fatima Ezzahra IZGUA <fatima.izgua@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function fillCcagReference()
    {
        $dataCcagRef = (new Atexo_CommonReferentiel())->retrieveValeurReferentielCCAG(Atexo_Config::getParameter('REFERENTIEL_CCAG_REFERENCE'), false, true);
        $dataSource = [];
        if ($dataCcagRef) {
            foreach ($dataCcagRef as $oneVal) {
                $dataSource[$oneVal->getId()] = $oneVal->getLibelleValeurReferentiel();
            }
        }
        $this->ccag->DataSource = $dataSource;
        $this->ccag->DataBind();
    }

    protected function populateDataInformationContrat()
    {
        $contrat = $this->getContrat();
        if ($contrat instanceof CommonTContratTitulaire) {
            if (!$this->getContratAvecChapeau($contrat)) {
                $this->intitule->Text = $contrat->getIntitule();
                $this->objetMarche->Text = mb_strimwidth($contrat->getObjetContrat(), 0, 255);
                $this->naturePrestations->selectedValue = $contrat->getCategorie();
                $this->ccag->selectedValue = $contrat->getCcagApplicable();
                //Pre-remplir le composant achat responsable
                if (Atexo_Module::isEnabled('ConsultationClause')) {
                    $this->achatResponsableConsultation->afficherClausesConsultation($contrat);
                }
                if (Atexo_Module::isEnabled('AffichageCodeCpv')) {
                    //charger referentiel CPV
                    $this->referentielCPV->onInitComposant();
                    $this->referentielCPV->setCodeCpvObligatoire(true);
                    $this->referentielCPV->chargerReferentielCpv($contrat->getCodeCpv1(), $contrat->getCodeCpv2());
                }
            }
            if ((new Atexo_Consultation_Contrat())->isTypeContratAvecMontantMaxById($contrat->getIdTypeContrat())) {
                $this->montantMaxContrat->Text = Atexo_Util::getMontantArronditEspace($contrat->getMontantMaxEstime());
            }
            if ((new Atexo_Consultation_Contrat())->isTypeContratMarcheSubsequent($contrat->getIdTypeContrat()) || (new Atexo_Consultation_Contrat())->isTypeContratMarcheSpecifique($contrat->getIdTypeContrat())) {
                $this->numero->Text = $contrat->getNumeroAcCorrespond();
            }
            if ($this->getContratAvecMontant($contrat)) {
                $this->MontantMarche->Text = Atexo_Util::getMontantArronditEspace($contrat->getMontantContrat());
                if (Atexo_Module::isEnabled('DecisionTrancheBudgetaire') && $contrat->getIdTrancheBudgetaire()) {
                    $this->trancheBudgetaire->Text = (new Atexo_TrancheBudgetaire())->getLibelleTrancheById($contrat->getIdTrancheBudgetaire(), $contrat->getOrganisme());
                }
            }

            if ($this->getContratAvecArticle133($contrat)) {
                if ($contrat->getPublicationMontant() && $contrat->getPublicationContrat()) {
                    $this->art133Publie->setChecked(false);
                    $this->art133NonPublie->setChecked(true);
                } elseif (0 === $contrat->getPublicationMontant() && 0 === $contrat->getPublicationContrat()) {
                    $this->art133Publie->setChecked(true);
                    $this->art133NonPublie->setChecked(false);
                }
            }
            if ((new Atexo_Consultation_Contrat())->isTypeContratSad($contrat->getIdTypeContrat())) {
                $this->labelObjetMarche->setText(Prado::Localize('OBJET_SAD'));
            } elseif ((new Atexo_Consultation_Contrat())->isTypeContratAccordCadre($contrat->getIdTypeContrat())) {
                $this->labelObjetMarche->setText(Prado::Localize('OBJET_ACCORD_CADRE'));
            } else {
                $this->labelObjetMarche->setText(Prado::Localize('DEFINE_OBJET_MARCHE'));
            }

            if ($contrat->isTypeContratConcession()) {
                if ($contrat->getIdTypeContratConcessionPivot()) {
                    $this->natureContratConcession->setSelectedValue($contrat->getIdTypeContratConcessionPivot());
                }
                if ($contrat->getIdTypeProcedureConcessionPivot()) {
                    $this->procedurePassation->setSelectedValue($contrat->getIdTypeProcedureConcessionPivot());
                }
                if ($contrat->getMontantSubventionPublique()) {
                    $this->montantSubvention->setText(Atexo_Util::getMontantArronditEspace($contrat->getMontantSubventionPublique()));
                }
            }
        }
    }

    /**
     * @param $sender
     * @param $param
     *
     * @throws PropelException
     */
    protected function suggestLieux($sender, $param)
    {
        $this->labelScriptJs->Text = '<script>suggestionSelected();</script>';
        // Get the token
        $token = $param->getToken();

        if ($listeEntite = (new Atexo_Geolocalisation_GeolocalisationN2())->retrieveDepartements($token)) {
            $sender->DataSource = $listeEntite;
            $sender->dataBind();
        } else {
            $sender->DataSource = [];
            $sender->dataBind();
            $this->codeDenomAdapte = '';
            $this->Page->setViewState('denominationAdapte', '');
        }
    }

    /**
     * @param $sender
     * @param $param
     */
    public function suggestionSelected($sender, $param)
    {
        $this->codeDenomAdapte = $sender->Suggestions->DataKeys[$param->selectedIndex];
        $this->Page->setViewState('denominationAdapte', '');

        if ('' != $this->codeDenomAdapte) {
            $this->Page->setViewState('denominationAdapte', $this->codeDenomAdapte);
        }
    }

    /**
     * @param $sender
     * @param $param
     */
    public function chargerLieuxExecution($sender, $param)
    {
        // Get the token
        $token = $param->getToken();

        // Sender is the Suggestions repeater
        $sender->DataSource = $this->chargerDepartementsListe($token);
        $sender->lieuPrincipalExecution->dataBind();
    }

    /**
     * @param $name
     *
     * @return array
     */
    public function chargerDepartementsListe($name)
    {
        $departements = (new Atexo_Departement())->getListDepartements(true);

        if ($name) {
            return array_filter($departements, fn($el) => false !== stripos($el['name'], (string) $name));
        } else {
            return $departements;
        }
    }

    /**
     * Permet de charger la natures de prestation.
     *
     * @param CommonConsultation $consultation la consultation
     * @param array              $listeLots    la liste des lots
     *
     * @return void
     *
     * @author Fatima Ezzahra IZGUA <fatima.izgua@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function chargerCategories()
    {
        $categories = Atexo_Consultation_Category::retrieveCategories();
        $dataCategories = Atexo_Consultation_Category::retrieveDataCategories($categories);
        $this->naturePrestations->Datasource = $dataCategories;
        $this->naturePrestations->dataBind();
    }

    /**
     * Permet de récupèrer la forme du prix.
     *
     * @param $contrat
     */
    public function chargerFormePrix($contrat)
    {
        if (!empty($contrat->getFormePrix())) {
            if (1 == $contrat->getFormePrix()) {
                $this->formePrix_ferme->setChecked(true);
            } elseif (2 == $contrat->getFormePrix()) {
                $this->formePrix_ferme_actualisable->setChecked(true);
            } elseif (3 == $contrat->getFormePrix()) {
                $this->formePrix_revisable->setChecked(true);
            }
        }

        $this->formePrix_ferme->dataBind();
        $this->formePrix_ferme_actualisable->dataBind();
        $this->formePrix_revisable->dataBind();
    }

    /**
     * Permet de sauvegarder un contrat.
     *
     * @param CommonTContratTitulaire $contrat
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function saveContrat(&$contrat)
    {
        if ($contrat instanceof CommonTContratTitulaire) {
            $dateNotif = Atexo_Util::frnDate2iso($this->dateNotification->Text);
            $contrat->setDatePrevueNotification($dateNotif);
            $contrat->setDatePrevueFinContrat(Atexo_Util::frnDate2iso($this->datePrevisionnelleFinMarche->Text));
            $contrat->setDatePrevueMaxFinContrat(Atexo_Util::frnDate2iso($this->datePrevFinMaxMarche->Text));

            $annee = $dateNotif ? Atexo_Util::getAnneeFromDate($dateNotif) : ($contrat->getDateAttribution() ? Atexo_Util::getAnneeFromDate($contrat->getDateAttribution('Y-m-d')) : '');

            if (!$this->getContratAvecChapeau($contrat)) {
                $contrat->setIntitule($this->intitule->getSafeText());
                $contrat->setObjetContrat($this->objetMarche->Text);
                $contrat->setNomLieuPrincipalExecution($this->lieuPrincipalExecution->Text);

                $lieuExecutionId = (new Atexo_Geolocalisation_GeolocalisationN2())->retrieveIdsByDenomination1($this->lieuPrincipalExecution->Text);
                $lieuExecution = (new Atexo_Geolocalisation_GeolocalisationN2())->retrieveGeolocalisationN2ById($lieuExecutionId);
                if ($lieuExecution[0] instanceof CommonGeolocalisationN2) {
                    $contrat->setCodeLieuPrincipalExecution($lieuExecution[0]->getDenomination2());
                    $contrat->setTypeCodeLieuPrincipalExecution(1); // todo récupèrer valeur dynamiquement (DB)
                }
                $contrat->setCategorie($this->naturePrestations->selectedValue);
                $contrat->setCcagApplicable($this->ccag->selectedValue);
                $this->achatResponsableConsultation->save($contrat);
                $codePrincipal = $this->referentielCPV->getCpvPrincipale();
                if ('' != $codePrincipal) {
                    $contrat->setCodeCpv1($codePrincipal);
                } else {
                    $contrat->setCodeCpv1(null);
                }
                $codesSec = $this->referentielCPV->getCpvSecondaires();
                if ('' != $codesSec) {
                    $contrat->setCodeCpv2($codesSec);
                } else {
                    $contrat->setCodeCpv2(null);
                }
            }

            if ((new Atexo_Consultation_Contrat())->isTypeContratAvecMontantMaxById($contrat->getIdTypeContrat())) {
                $montantMax = Atexo_Util::getMontantArronditSansEspace($this->montantMaxContrat->Text);
                $contrat->setMontantMaxEstime($montantMax);
            }
            if ($this->getContratAvecMontant($contrat)) {
                $montant = Atexo_Util::getMontantArronditSansEspace($this->MontantMarche->Text);
                $contrat->setMontantContrat($montant);

                // Ajout des tranches budgetaire
                $gestionContrat = new Atexo_Contrat_Gestion();
                $gestionContrat->changeTrancheBudgetaire($contrat);
                $this->trancheBudgetaire->Text = (new Atexo_TrancheBudgetaire())->getLibelleTrancheById($contrat->getIdTrancheBudgetaire(), $contrat->getOrganisme());
            }

            $formePrix = null;

            if ($this->formePrix_ferme->Checked) {
                $formePrix = 1;
            } elseif ($this->formePrix_ferme_actualisable->Checked) {
                $formePrix = 2;
            } elseif ($this->formePrix_revisable->Checked) {
                $formePrix = 3;
            }

            $contrat->setFormePrix($formePrix);

            if (Atexo_Module::isEnabled('MarcheDefense', Atexo_CurrentUser::getOrganismAcronym())) {
                if ($this->marcheDefense_Oui->Checked) {
                    $contrat->setMarcheDefense('1');
                } elseif ($this->marcheDefense_Non->Checked) {
                    $contrat->setMarcheDefense('2');
                } else {
                    $contrat->setMarcheDefense('0');
                }
            }

            if($this->marcheInnovant_Oui->Checked){
                $contrat->setMarcheInnovant(1);
            }else{
                $contrat->setMarcheInnovant(0);
            }

            if($this->getContratAvecArticle133($contrat)){
                if($this->art133NonPublie->getChecked()){
                    $contrat->setPublicationMontant('1') ;
                    $contrat->setPublicationContrat('1');
                } elseif ($this->art133Publie->getChecked()) {
                    $contrat->setPublicationMontant('0');
                    $contrat->setPublicationContrat('0');
                }
            }

            if ($contrat->isTypeContratConcession()) {
                $contrat->setIdTypeContratConcessionPivot($this->natureContratConcession->getSelectedValue());
                $contrat->setIdTypeProcedureConcessionPivot($this->procedurePassation->getSelectedValue());
                $contrat->setMontantSubventionPublique(Atexo_Util::getMontantArronditSansEspace($this->montantSubvention->getSafeText()));
                $contrat->setDateDebutExecution(Atexo_Util::frnDate2iso($this->dateExecution->getText()));
            }
            $contrat->setDateModification(date('Y-m-d H:i:s'));
        }
    }

    /**
     * Permet de charger les info date du contrat.
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    protected function populateDataDate()
    {
        $contrat = $this->getContrat();
        if ($contrat instanceof CommonTContratTitulaire) {
            $this->dateNotification->Text = $contrat->getDatePrevueNotification();
            $this->datePrevisionnelleFinMarche->Text = $contrat->getDatePrevueFinContrat();
            $this->datePrevFinMaxMarche->Text = $contrat->getDatePrevueMaxFinContrat();
            if ($contrat->isTypeContratConcession()) {
                $this->dateExecution->setText($contrat->getDateDebutExecution());
            }
        }
    }

    /**
     * Permet de valider les donnée du contrat.
     *
     * @param CommonTContratTitulaire $contrat
     * @param bool                    $isValid
     * @param array                   $arrayMsgErreur tableau des message
     * @param string                  $scriptJs
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function validerDonneesDecisionContrat($contrat, &$isValid, &$arrayMsgErreur, &$scriptJs)
    {
        if (!$this->getContratAvecChapeau($contrat) && Atexo_Module::isEnabled('ConsultationClause')) {
            $this->achatResponsableConsultation->validerAchatResponsable($isValid, $arrayMsgErreur, $scriptJs);
        }
        if (!$this->lieuPrincipalExecution->Text
            || empty((new Atexo_Geolocalisation_GeolocalisationN2())->retrieveListDepartementsByDenomNormalise($this->lieuPrincipalExecution->Text))) {
            $arrayMsgErreur[] = Prado::localize('TEXT_LIEU_PRINCIPAL_EXECUTION');
            $isValid = false;
        }
        if (Atexo_Module::isEnabled('AffichageCodeCpv')
            && empty($this->referentielCPV->getCpvPrincipale())) {
            $scriptJs .= "document.getElementById('".$this->referentielCPV->getIdClientImgErrorCpv()."').style.display = '';";
            $isValid = false;
            $scriptJs .= "document.getElementById('divValidationSummary').style.display='';";
            $arrayMsgErreur[] = Prado::localize('TEXT_CODE_CPV');
        } else {
            $scriptJs .= "document.getElementById('".$this->referentielCPV->getIdClientImgErrorCpv()."').style.display = 'none';";
        }
    }

    /**
     * Permet de verfier si un contrat est avec chapeau.
     *
     * @param CommonTContratTitulaire $contrat
     *
     * @return bool true si le contrat est avec chapeau sinon flase
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    protected function getContratAvecChapeau($contrat)
    {
        return (new Atexo_Consultation_Contrat())->isTypeContratAvecChapeauById($contrat->getIdTypeContrat());
    }

    /**
     * Permet de verfier si un contrat est avec montant.
     *
     * @param CommonTContratTitulaire $contrat
     *
     * @return bool true si le contrat est avec montant sinon flase
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    protected function getContratAvecMontant($contrat)
    {
        return (new Atexo_Consultation_Contrat())->isTypeContratAvecMontantById($contrat->getIdTypeContrat());
    }

    /**
     * Permet de verfier si un contrat est avec article 133.
     *
     * @param CommonTContratTitulaire $contrat
     *
     * @return bool true si le contrat est avec article 133
     *
     * @author AMAL EL BEKKAOUI <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    protected function getContratAvecArticle133($contrat)
    {
        return (new Atexo_Consultation_Contrat())->isTypeContratArticle133ById($contrat->getIdTypeContrat());
    }

    /**
     * Permet de gerer la visibilité des composant.
     *
     * @param CommonTContratTitulaire $contrat
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function visibliteComponent($contrat)
    {
        if ($this->getContratAvecChapeau($contrat)) {
            $this->panelContratSansChapeau->setVisible(false);
        } else {
            $this->panelContratSansChapeau->setVisible(true);
        }
        if ($this->getContratAvecMontant($contrat)) {
            $this->panelMontantContrat->setVisible(true);
        } else {
            $this->panelMontantContrat->setVisible(false);
        }
        if ($this->getContratAvecArticle133($contrat)) {
            $this->article133->setVisible(true);
        } else {
            $this->article133->setVisible(false);
        }

        if ((new Atexo_Consultation_Contrat())->isTypeContratAvecMontantMaxById($contrat->getIdTypeContrat())) {
            $this->panelMontantMax->setVisible(true);
        } else {
            $this->panelMontantMax->setVisible(false);
        }
        if ((new Atexo_Consultation_Contrat())->isTypeContratMarcheSubsequent($contrat->getIdTypeContrat())) {
            $this->panelNumero->setVisible(true);
            $libelle = Prado::localize('NUMERO_ACCORD_CADRE_CORRESPONDANT');
            $this->labelNumero->Text = $libelle;
            $this->labelScriptJs->Text = "<script>J('#".$this->numero->ClientId."').prop('title', '".addslashes($libelle)."');</script>";
        } elseif ((new Atexo_Consultation_Contrat())->isTypeContratMarcheSpecifique($contrat->getIdTypeContrat())) {
            $this->panelNumero->setVisible(true);
            $libelle = Prado::localize('NUMERO_SAD_CORRESPONDANT');
            $this->labelNumero->Text = $libelle;
            $this->labelScriptJs->Text = "<script>J('#".$this->numero->ClientId."').prop('title', '".addslashes($libelle)."');</script>";
        } else {
            $this->panelNumero->setVisible(false);
        }
    }

    /**
     * Permet de change les tranches budgetaire selon l'annee et le montant.
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function changeTrancheBudgetaire()
    {
        $montant = Atexo_Util::getMontantArronditSansEspace($this->MontantMarche->Text);
        $dateNotif = Atexo_Util::frnDate2iso($this->dateNotification->Text);

        $contrat = $this->getContrat();
        $contrat->setMontantContrat($montant);
        $contrat->setDatePrevueNotification($dateNotif);

        $gestionContrat = new Atexo_Contrat_Gestion();
        $gestionContrat->changeTrancheBudgetaire($contrat);

        $this->trancheBudgetaire->Text = (new Atexo_TrancheBudgetaire())->getLibelleTrancheById($contrat->getIdTrancheBudgetaire(), $contrat->getOrganisme());
    }

    /**
     * Permet de recuperer marche defense.
     *
     * @param $contrat
     */
    public function chargerMarcheDefense($contrat)
    {
        if (Atexo_Module::isEnabled('MarcheDefense', Atexo_CurrentUser::getOrganismAcronym())) {
            if (1 == $contrat->getMarcheDefense()) {
                $this->marcheDefense_Oui->setChecked(true);
            } elseif (2 == $contrat->getMarcheDefense()) {
                $this->marcheDefense_Non->setChecked(true);
            }
        }
    }

    /**
     * Permet de changer la valeur de la publicationselon l'activation ou nn deu marché de défense.
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2018-esr
     *
     * @copyright Atexo 2018
     */
    public function marcheDefenseChanged()
    {
        if ($this->marcheDefense_Oui->Checked) {
            $this->art133Publie->setChecked(false);
            $this->art133NonPublie->setChecked(true);
            $this->art133Publie->setEnabled(false);
            $this->art133NonPublie->setEnabled(false);
        } elseif ($this->marcheDefense_Non->Checked) {
            $this->art133Publie->setChecked(true);
            $this->art133NonPublie->setChecked(false);
            $this->art133Publie->setEnabled(true);
            $this->art133NonPublie->setEnabled(true);
        }
    }

    /**
     * Permet d'initialiser les valeur de la publication du contrat.
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2018-esr
     *
     * @copyright Atexo 2018
     */
    public function initialiserPublicationmarche($contrat)
    {
        if (Atexo_Module::isEnabled('MarcheDefense', Atexo_CurrentUser::getOrganismAcronym())) {
            if (1 == $contrat->getMarcheDefense()) {
                $this->art133Publie->setEnabled(false);
                $this->art133NonPublie->setEnabled(false);
            } elseif (2 == $contrat->getMarcheDefense()) {
                $this->art133Publie->setEnabled(true);
                $this->art133NonPublie->setEnabled(true);
            }
        } else {
            $this->art133Publie->setEnabled(true);
            $this->art133NonPublie->setEnabled(true);
        }
    }

    /**
     * Permet de charger les procédures de passation pour concession.
     */
    public function chargerProceduresConcessionPivot()
    {
        $procedures = Atexo_Consultation_ProcedureType::retrieveProcedureTypesConcessionPivot();
        $proc = [];
        $proc[0] = '--- '.Prado::localize('TEXT_TOUTES_PROCEDURES').' ---';
        foreach ($procedures as $procedure) {
            $proc[$procedure->getId()] = (new Atexo_Consultation_ProcedureType())->retrieveTypeProcedurePivot($procedure->getId());
        }
        ksort($proc);
        $this->procedurePassation->Datasource = $proc;
        $this->procedurePassation->dataBind();
    }

    /**
     * Permet de charger les types de contrats.
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function chargerNatureContrat()
    {
        $listeNatureContrat = Atexo_Contrat::getListeNatureContratConcession();
        $this->natureContratConcession->Datasource = $listeNatureContrat;
        $this->natureContratConcession->dataBind();
    }

    public function isTypeContratConcession()
    {
        $contrat = $this->getContrat();
        if ($contrat && $contrat->isTypeContratConcession()) {
            return true;
        }

        return false;
    }
}
