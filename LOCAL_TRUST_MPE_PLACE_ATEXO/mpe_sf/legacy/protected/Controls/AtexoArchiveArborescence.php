<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Tree\Atexo_Consultation_Tree_Arborescence;
use Application\Service\Atexo\Consultation\Tree\Atexo_Consultation_Tree_Directory;
use Application\Service\Atexo\Consultation\Tree\Atexo_Consultation_Tree_Group;
use Application\Service\Atexo\Consultation\Tree\Atexo_Consultation_Tree_Item;

/**
 * permet d'afficher l'arborescence d'un archive.
 *
 * @author LEZ <loubna.ezziani@atexo.com>
 *
 * @version 1.0
 *
 * @since 4.8.0
 *
 * @copyright Atexo 2014
 */
class AtexoArchiveArborescence extends MpeTTemplateControl
{
    /**
     * Permet d'afficher l'arborescence de l'archive.
     *
     * @param int $consultationId la reference de la consultation,Integr $lot le numero du lot
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function displayArborescence($consultationId, $lot)
    {
        $tree = new Atexo_Consultation_Tree_Arborescence($consultationId, false, null, true, $lot);
        self::writeArborescence($tree);
    }

    /**
     * Permet de setter l'arborescence de l'archive.
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function writeArborescence(Atexo_Consultation_Tree_Arborescence $tree)
    {
        $htmlToPrint = null;
        $htmlToPrint .= '<ul>';
        foreach ($tree->getFils() as $archiveItem) {
            self::extractItems($archiveItem, $htmlToPrint);
        }
        $htmlToPrint .= '</ul>';
        $this->arbo->Text = $htmlToPrint;
    }

    /**
     * Permet d'avoir l' html de l'arborescence de l'archive.
     *
     * @param Atexo_Consultation_Tree_Arborescence $tree,string $htmlToPrint
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function extractItems(Atexo_Consultation_Tree_Item $archiveItem, &$htmlToPrint)
    {
        // On affiche le nom de l'item (dossier ou fichier) courant :

        if ($archiveItem instanceof Atexo_Consultation_Tree_Directory) {
            $htmlToPrint .= '<li>';
            $htmlToPrint .= '<span class="folder">';
            $htmlToPrint .= Atexo_Util::toUtf8($archiveItem->getNom());
            $htmlToPrint .= '</span>';
            $ulToAddToHml = "<ul style='display:none'>";

            // On affiche les enfants du dossier
            $arrayFils = $archiveItem->getFils();
            if ($arrayFils) {
                foreach ($arrayFils as $archiveItemFils) {
                    self::extractItems($archiveItemFils, $ulToAddToHml);
                }
            }
            $ulToAddToHml .= '</ul>';
            if ($arrayFils) { // pas de ul si si rien dans le dossier...
                $htmlToPrint .= $ulToAddToHml;
            }
            $htmlToPrint .= '</li>';
        } elseif ($archiveItem instanceof Atexo_Consultation_Tree_Group) {
            $arrayFils = $archiveItem->getFils();
            if ($arrayFils) {
                foreach ($arrayFils as $archiveItemFils) {
                    self::extractItems($archiveItemFils, $htmlToPrint);
                }
            }
        } else {
            $htmlToPrint .= '<li>';
            $htmlToPrint .= '<span class="file" title="'.$archiveItem->getPath().'">';
            $htmlToPrint .= Atexo_Util::toUtf8($archiveItem->getNom());
            $htmlToPrint .= '</span>';
            $htmlToPrint .= '</li>';
        }
    }
}
