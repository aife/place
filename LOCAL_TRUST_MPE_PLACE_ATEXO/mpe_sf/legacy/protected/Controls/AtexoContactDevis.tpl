<com:TConditional condition="$this->getResponsive()">
    <prop:trueTemplate>
        <%include Application.templates.responsive.AtexoContactDevis %>
    </prop:trueTemplate>
    <prop:falseTemplate>
        <%include Application.templates.mpe.AtexoContactDevis %>
    </prop:falseTemplate>
</com:TConditional>