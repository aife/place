<?php

namespace Application\Controls;

use Application\Propel\Mpe\CommonDecisionEnveloppe;
use Application\Propel\Mpe\CommonMarche;
use Application\Propel\Mpe\CommonOffrePapier;
use Application\Propel\Mpe\CommonOffres;
use Application\Service\Atexo\Atexo_CommonReferentiel;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2;
use Prado\Prado;

/**
 * commentaires.
 *
 * @author Khadija CHOUIKA <khadija.chouika@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 5.0
 *
 * @since MPE-4.0
 */
class BlocIdentificationEntreprise extends MpeTTemplateControl
{
    private $offre;
    private $validationGroup; // si non utilise enlever
    private bool $enabled = false;

    /**
     * Recupère la valeur.
     */
    public function getOffre()
    {
        return $this->offre;
    }

    /**
     * Affectes la valeur.
     *
     * @param $value
     */
    public function setEnabled($value)
    {
        $this->enabled = $value;
    }

    public function getEnabled($checkParents = false)
    {
        return $this->enabled;
    }

    /**
     * Affectes la valeur.
     *
     * @param $value
     */
    public function setOffre($value)
    {
        $this->offre = $value;
    }

    /**
     * Affectes la valeur.
     *
     * @param $value
     */
    public function setValidationGroup($value)
    {
        $this->validationGroup = $value;
    }

    /**
     * Recupère la valeur.
     */
    public function getValidationGroup()
    {
        return $this->validationGroup;
    }

    /**
     * load du composant.
     */
    public function onLoad($param)
    {
        if (!$this->Page->isPostBack) {
            if (Atexo_Module::isEnabled('CompteEntrepriseRcville')) {
                $this->moduleRc->Value = 1;
                $this->fillVilleRc();
            }
            if ($offre = $this->getOffre()) {
                $objetDecision = $offre->getCommonDecisionEnveloppe();
                $this->chargerOffre($offre, $objetDecision);
            } elseif (isset($_GET['idMarche'])) {
                $marche = $this->Page->getViewState('marche');
                if ($marche instanceof CommonMarche) {
                    $this->chargerMarche($marche);
                }
            } else {
                $this->showHideDiv(true, false);
            }
        } else {
            $this->listPays->setPostBack(1);
        }
    }

    public function showHideDiv($divNationale, $divEtranger)
    {
        if ((true == $divNationale) && (false == $divEtranger)) {
            $this->attributaire_etablieFrance->setStyle('display:');
            $this->attributaire_nonEtablieFrance->setStyle('display:none');
            $this->attributaire_etranger->checked = false;
            $this->attributaire_Nationale->checked = true;
        } elseif ((false == $divNationale) && (true == $divEtranger)) {
            $this->attributaire_etablieFrance->setStyle('display:none');
            $this->attributaire_nonEtablieFrance->setStyle('display:');
            $this->attributaire_etranger->checked = true;
            $this->attributaire_Nationale->checked = false;
        } else {
            $this->attributaire_etablieFrance->setStyle('display:');
            $this->attributaire_nonEtablieFrance->setStyle('display:none');
            $this->attributaire_etranger->checked = false;
            $this->attributaire_Nationale->checked = true;
        }
    }

    /*
     * Permet de gerer la visiblite des blocs cas decisionEnveloppe
     *
     */
    public function chargerOffre($offre, $objetDecision)
    {
        if ($objetDecision->getSirenAttributaire() || ((1 == $this->moduleRc->Value) && $objetDecision->getRcVilleAttributaire())) {
            $this->chargerChampsEntrepriseNationale($offre, $objetDecision);
            $this->showHideDiv(true, false);
        } elseif ($this->getOffreSiret($offre) && !$objetDecision->getIdentifiantnationalAttributaire()) {
            $this->showHideDiv(true, false);
            $this->chargerChampsEntrepriseNationale($offre, $objetDecision);
        } elseif ($objetDecision->getIdentifiantnationalAttributaire()) {
            $this->chargerChampsEntrepriseEtrange($offre, $objetDecision);
            $this->showHideDiv(false, true);
        } elseif (strtoupper($this->getOffrePays($offre)) != strtoupper(Atexo_Config::getParameter('DENOMINATION1_GEON2_FRANCE'))) {
            $this->chargerChampsEntrepriseEtrange($offre, $objetDecision);
            $this->showHideDiv(false, true);
        } else {
            $this->showHideDiv(true, false);
        }
    }

    /*
    * Permet de gerer la visiblite des blocs selon marche
    *
    */
    public function chargerMarche($marche)
    {
        if ($marche->getSirenAttributaire() || ((1 == $this->moduleRc->Value) && $marche->getRcVilleAttributaire())) {
            $this->chargerChampsEntrepriseNationale(false, false, $marche);
            $this->showHideDiv(true, false);
        } elseif ($marche->getIdentifiantNationalAttributaire()) {
            $this->showHideDiv(false, true);
            $this->chargerChampsEntrepriseEtrange(false, false, $marche);
        } else {
            $this->showHideDiv(true, false);
        }
    }

    public function chargerChampsRcMarche($marche)
    {
        $indexVille = 0;
        $rcNum = '';
        if ($marche->getRcNumAttributaire()) {
            $numRc = explode('-', $marche->getRcNumAttributaire());
            if (is_array($numRc)) {
                $indexVille = $numRc[0];
                $rcNum = $numRc[1];
            }
        }
        if (is_numeric($indexVille)) {
            $this->rcVille->SelectedValue = $indexVille; //TODO une methode pour dire que verifier si l'index dans la liste
        }
        $this->numero_Rc->Text = $rcNum;
    }

    /*
    * Permet charger les champs dans le cas du module Rc
    *
    */
    public function chargerChampsRc($offre, $objetDecision)
    {
        $indexVille = 0;
        $rcNum = '';
        if ($objetDecision->getRcNumAttributaire()) {
            $numRc = explode('-', $objetDecision->getRcNumAttributaire());
            if (is_array($numRc)) {
                $indexVille = $numRc[0];
                $rcNum = $numRc[1];
            }
        } elseif ($offre instanceof CommonOffres) {
            $numRc = explode('-', $offre->getSiretEntreprise());
            if (is_array($numRc)) {
                $indexVille = $numRc[0];
                $rcNum = $numRc[1];
            }
        } elseif ($offre instanceof CommonOffrePapier) {
            $numRc = explode('-', $offre->getSiret());
            if (is_array($numRc)) {
                $indexVille = $numRc[0];
                $rcNum = $numRc[1];
            }
        }
        if (is_numeric($indexVille)) {
            $this->rcVille->SelectedValue = $indexVille; //TODO une methode pour verifier si l'index dans la liste
        }
        $this->numero_Rc->Text = $rcNum;
    }

    /*
    * Permet charger les champs dans le cas du siret
    *
    */
    public function chargerChampsSiret($offre, $objetDecision)
    {
        if ($objetDecision->getSirenAttributaire()) {
            $this->siren->Text = $objetDecision->getSirenAttributaire();
            $this->siret->Text = $objetDecision->getNicAttributaire();
        } else {
            $siretEntreprise = '';
            if ($offre instanceof CommonOffrePapier) {
                if ($offre->getSiret()) {
                    $siretEntreprise = $offre->getSiret();
                }
            } elseif ($offre instanceof CommonOffres) {
                if ($offre->getSiretEntreprise()) {
                    $siretEntreprise = $offre->getSiretEntreprise();
                }
            }
            $this->siren->Text = strlen($siretEntreprise) ? substr($siretEntreprise, 0, 9) : '';
            $this->siret->Text = (strlen($siretEntreprise) > 9) ? substr($siretEntreprise, 9, 5) : '';
        }
    }

    public function chargerChampsSiretMarche($marche)
    {
        if ($marche->getSirenAttributaire()) {
            $this->siren->Text = $marche->getSirenAttributaire();
        }
        if ($marche->getNicAttributaire()) {
            $this->siret->Text = $marche->getNicAttributaire();
        }
    }

    /*
    * Permet charger  identifiantUnique
    *
    */
    public function chargerChampsIdentifiantUnique($offre, $objetDecision)
    {
        if ($objetDecision->getSirenAttributaire()) {
            $this->identifiantUnique->Text = $objetDecision->getSirenAttributaire();
        } else {
            $siretEntreprise = '';
            if ($offre instanceof CommonOffrePapier) {
                if ($offre->getSiret()) {
                    $siretEntreprise = $offre->getSiret();
                }
            } elseif ($offre instanceof CommonOffres) {
                if ($offre->getSiretEntreprise()) {
                    $siretEntreprise = $offre->getSiretEntreprise();
                }
            }
            $this->identifiantUnique->Text = $siretEntreprise;
        }
    }

    public function chargerChampsIdentifiantUniqueMarche($marche)
    {
        if ($marche->getSirenAttributaire()) {
            $this->identifiantUnique->Text = $marche->getSirenAttributaire();
        }
    }

    /*
    * Permet charger les champs dans une entreprise nationale
    *
    */
    public function chargerChampsEntrepriseNationale($offre, $objetDecision, $marche = false)
    {
        if (Atexo_Module::isEnabled('CompteEntrepriseRcville')) {
            if ($marche) {
                $this->chargerChampsRcMarche($marche);
            } else {
                $this->chargerChampsRc($offre, $objetDecision);
            }
        } elseif (Atexo_Module::isEnabled('CompteEntrepriseIdentifiantUnique')) {
            if ($marche) {
                $this->chargerChampsIdentifiantUniqueMarche($marche);
            } else {
                $this->chargerChampsIdentifiantUnique($offre, $objetDecision);
            }
        } else {
            if ($marche) {
                $this->chargerChampsSiretMarche($marche);
            } else {
                $this->chargerChampsSiret($offre, $objetDecision);
            }
        }
    }

    /*
    * Permet charger les champs dans une entreprise Etrange
    *
    */
    public function chargerChampsEntrepriseEtrange($offre, $objetDecision, $marche = false)
    {
        $pays = null;
        if ($marche) {
            if ($marche->getPaysAttributaire()) {
                $idPays = (new Atexo_Geolocalisation_GeolocalisationN2())->retrieveIdsByDenomination1($marche->getPaysAttributaire());
                $this->listPays->selectedValue = $idPays;
            }
            $this->identifiant_national->Text = $marche->getIdentifiantnationalAttributaire();
        } else {
            if ($objetDecision->getPaysAttributaire()) {
                $idPays = (new Atexo_Geolocalisation_GeolocalisationN2())->retrieveIdsByDenomination1($objetDecision->getPaysAttributaire());
                $this->listPays->selectedValue = $idPays;
            } else {
                if ($offre instanceof CommonOffres) {
                    $pays = $offre->getPaysInscrit();
                } elseif ($offre instanceof CommonOffrePapier) {
                    $pays = $offre->getPays();
                }
                if ($pays && ($pays != Atexo_Util::encodeToHttp(Prado::localize('DEFINE_FRANCE')))) {
                    $idPays = (new Atexo_Geolocalisation_GeolocalisationN2())->retrieveIdsByDenomination1($pays);
                    $this->listPays->selectedValue = $idPays;
                }
            }
            if ($objetDecision->getIdentifiantnationalAttributaire()) {
                $this->identifiant_national->Text = $objetDecision->getIdentifiantnationalAttributaire();
            } elseif ($offre->getIdentifiantNational()) {
                $this->identifiant_national->Text = $offre->getIdentifiantNational();
            }
        }
    }

    /*
     * Permet de sauvgarder l'objet decision
     */
    public function Enregister(&$objetDecision)
    {
        if ($objetDecision instanceof CommonDecisionEnveloppe) {
            if ($this->attributaire_Nationale->checked) {
                if (Atexo_Module::isEnabled('CompteEntrepriseRcville')) {
                    if (0 != $this->rcVille->SelectedValue) {
                        $objetDecision->setRcVilleAttributaire($this->rcVille->SelectedItem->Text); //TODO recuperation de la BD du nom
                        $objetDecision->setRcNumAttributaire($this->rcVille->SelectedValue.'-'.$this->numero_Rc->Text);
                        $objetDecision->setSirenAttributaire('');
                        $objetDecision->setNicAttributaire('');
                    }
                } elseif (Atexo_Module::isEnabled('CompteEntrepriseIdentifiantUnique')) {
                    $objetDecision->setSirenAttributaire($this->identifiantUnique->text);
                    $objetDecision->setNicAttributaire('');
                    $objetDecision->setRcVilleAttributaire('');
                    $objetDecision->setRcNumAttributaire('');
                } else {
                    if ($this->siret->Text && $this->siren->Text) {
                        $objetDecision->setSirenAttributaire($this->siren->Text);
                        $objetDecision->setNicAttributaire($this->siret->Text);
                        $objetDecision->setRcVilleAttributaire('');
                        $objetDecision->setRcNumAttributaire('');
                    }
                }
                $objetDecision->setIdentifiantnationalAttributaire('');
                $objetDecision->setAcronymepaysAttributaire(Atexo_Config::getParameter('DENOMINATION2_GEON2_FRANCE'));
                $objetDecision->setPaysAttributaire(Atexo_Util::encodeToHttp(Prado::localize('DEFINE_FRANCE')));
            } else {
                if (0 != $this->listPays->selectedValue) {
                    $nomPays = (new Atexo_Geolocalisation_GeolocalisationN2())->retrieveCountryById($this->listPays->selectedValue);
                    $objetDecision->setPaysAttributaire($nomPays);
                    $acronyme = (new Atexo_Geolocalisation_GeolocalisationN2())->retrieveAccronymeCountryById($this->listPays->selectedValue);
                    $objetDecision->setAcronymepaysAttributaire($acronyme);
                }
                if ($this->identifiant_national->Text) {
                    $objetDecision->setIdentifiantnationalAttributaire($this->identifiant_national->Text);
                }
                $objetDecision->setSirenAttributaire('');
                $objetDecision->setNicAttributaire('');
                $objetDecision->setRcVilleAttributaire('');
                $objetDecision->setRcNumAttributaire('');
            }
        }
    }

    /*
     * Retourne le siret d'une offre
     */
    public function getOffreSiret($offre)
    {
        if ($offre instanceof CommonOffres) {
            return $offre->getSiretEntreprise();
        } elseif ($offre instanceof CommonOffrePapier) {
            return $offre->getSiret();
        }
    }

    /*
     * Retourne le pays d'une offre
     */
    public function getOffrePays($offre)
    {
        if ($offre instanceof CommonOffres) {
            return $offre->getPaysInscrit();
        } elseif ($offre instanceof CommonOffrePapier) {
            return $offre->getPays();
        }
    }

    /*
    * Remplit la liste des villes dans le cas Rc
    */
    public function fillVilleRc()
    {
        $valeur = (new Atexo_CommonReferentiel())->retrieveValeurReferentiel(Atexo_Config::getParameter('VALEUR_REFERENTIEL_VILLE_RC'));
        $data = [];
        $data['0'] = Prado::localize('TEXT_SELECTIONNER').' ...';
        $data['99'] = Prado::localize('JE_PAS_DE_RC');
        if ($valeur) {
            foreach ($valeur as $oneVal) {
                $data[$oneVal->getLibelle2()] = $oneVal->getLibelleValeurReferentiel();
            }
        }
        $this->rcVille->DataSource = $data;
        $this->rcVille->DataBind();
    }

    /*
     * Permet de valider les données du bloc Entreprise
     */
    public function validateBlocIdentificationEntreprise(&$isValid, &$arrayMsgErreur, &$scriptJs)
    {
        if ($this->attributaire_Nationale->checked) {
            if (Atexo_Module::isEnabled('CompteEntrepriseRcville')) {
                $this->validerRCEntreprise($isValid, $arrayMsgErreur, $scriptJs);
            } elseif (Atexo_Module::isEnabled('CompteEntrepriseIdentifiantUnique')) {
                $this->validerIdentifiantUnique($isValid, $arrayMsgErreur, $scriptJs);
            } else {
                $this->validerSiretEntreprise($isValid, $arrayMsgErreur, $scriptJs);
            }
        } else {
            if (Atexo_Module::isEnabled('CompteEntrepriseRcville')) {
                $scriptJs .= "document.getElementById('".$this->ImgErrorRc->getClientId()."').style.display = 'none';";
            } elseif (Atexo_Module::isEnabled('CompteEntrepriseIdentifiantUnique')) {
                $scriptJs .= "document.getElementById('".$this->ImgErrorIdentifiantUnique->getClientId()."').style.display = 'none';";
            } else {
                $scriptJs .= "document.getElementById('".$this->ImgError->getClientId()."').style.display = 'none';";
            }
            $this->validerEntrepriseEtranger($isValid, $arrayMsgErreur, $scriptJs);
        }
    }

    /*
     * Permet de savoir si l'entreprise est nationale retourne true si non false
     */
    public function isPaysNationaleChecked()
    {
        if ($this->attributaire_Nationale->checked) {
            return true;
        }

        return false;
    }

    /*
     * Permet de valider le siret d'entreprise
     */
    public function validerSiretEntreprise(&$isValid, &$arrayMsgErreur, &$scriptJs)
    {
        $siren = $this->siren->Text;
        $siret = $this->siret->Text;

        if (!$siren || !$siret) {
            $scriptJs .= "document.getElementById('".$this->ImgError->getClientId()."').style.display = '';";
            $arrayMsgErreur[] = Prado::localize('SIREN_SIRET');
            $isValid = false;
        } elseif (!Atexo_Util::isSiretValide($siren.$siret)) {
            $scriptJs .= "document.getElementById('".$this->ImgError->getClientId()."').style.display = '';";
            $arrayMsgErreur[] = Prado::localize('MSG_ERROR_SIRET_INVALIDE');
            $isValid = false;
        } else {
            $scriptJs .= "document.getElementById('".$this->ImgError->getClientId()."').style.display = 'none';";
        }
    }

    /*
     * Permet de valider RC entreprisse ( num et ville)
     */
    public function validerRCEntreprise(&$isValid, &$arrayMsgErreur, &$scriptJs)
    {
        if (!$this->rcVille->getSelectedValue()) {
            $scriptJs .= "document.getElementById('".$this->ImgErrorRc->getClientId()."').style.display = '';";
            $arrayMsgErreur[] = Prado::localize('TEXT_SELECTIONNEZ_VILLE_RC');
            $isValid = false;
        } else {
            $scriptJs .= "document.getElementById('".$this->ImgErrorRc->getClientId()."').style.display = 'none';";
        }
        if (!$this->numero_Rc->Text) {
            $scriptJs .= "document.getElementById('".$this->ImgErrorNumRc->getClientId()."').style.display = '';";
            $arrayMsgErreur[] = Prado::localize('MSG_ERROR_OBLIGATORY_NUM_RC');
            $isValid = false;
        } else {
            $scriptJs .= "document.getElementById('".$this->ImgErrorNumRc->getClientId()."').style.display = 'none';";
        }
    }

    /*
     * Permet de valider les donnees d'une entreprise etranger ( Pays+ identifiant)
     */
    public function validerEntrepriseEtranger(&$isValid, &$arrayMsgErreur, &$scriptJs)
    {
        if ($this->attributaire_etranger->checked) {
            if (!$this->listPays->getSelectedValue()) {
                $scriptJs .= "document.getElementById('".$this->ImgErrorPays->getClientId()."').style.display = '';";
                $arrayMsgErreur[] = Prado::localize('TEXT_SELECTIONNEZ_UN_PAYS');
                $isValid = false;
            } else {
                $scriptJs .= "document.getElementById('".$this->ImgErrorPays->getClientId()."').style.display = 'none';";
            }

            if (!trim($this->identifiant_national->Text)) {
                $scriptJs .= "document.getElementById('".$this->ImgErrorIdentifiant->getClientId()."').style.display = '';";
                $arrayMsgErreur[] = Prado::localize('MSG_ERROR_OBLIGATORY_ID_CONTRY');
                $isValid = false;
            } else {
                $scriptJs .= "document.getElementById('".$this->ImgErrorIdentifiant->getClientId()."').style.display = 'none';";
            }
        } else {
            $scriptJs .= "document.getElementById('".$this->ImgErrorPays->getClientId()."').style.display = 'none';";
            $scriptJs .= "document.getElementById('".$this->ImgErrorIdentifiant->getClientId()."').style.display = 'none';";
        }
    }

    /*
    * Permet de valider l'Identifiant Unique
    */
    public function validerIdentifiantUnique(&$isValid, &$arrayMsgErreur, &$scriptJs)
    {
        $identifiantUnique = $this->identifiantUnique->Text;

        if (!$identifiantUnique) {
            $scriptJs .= "document.getElementById('".$this->ImgErrorIdentifiantUnique->getClientId()."').style.display = '';";
            $arrayMsgErreur[] = Prado::localize('SIREN_SIRET');
            $isValid = false;
        } else {
            $scriptJs .= "document.getElementById('".$this->ImgErrorIdentifiantUnique->getClientId()."').style.display = 'none';";
        }
    }
}
