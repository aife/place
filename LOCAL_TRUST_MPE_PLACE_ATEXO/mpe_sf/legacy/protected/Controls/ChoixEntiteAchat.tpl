<!--Debut partie centrale-->
	<!--Debut colonne droite-->
		<!--Debut Bloc Choix Entite d'achat-->
		<com:TPanel id="recapConsultation">
		<div class="form-bloc margin-0" id="recapConsultation">
			<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
			<div class="content">
				<h2><com:TTRanslate>DEFINE_ADMINISTRER_ENTITE_ACHAT</com:TTRanslate></h2>
				<div class="line">
					<div class="intitule-150">
						<com:TActiveRadioButton onCallBack="Page.onClickSearch" GroupName="choixEntite" id="monEntite"  CssClass="radio"  Attributes.title="<%=Prado::localize('DEFINE_MON_ENTITE_ACHAT')%>" Checked="true" />
						<label for="monEntite"><com:TTRanslate>DEFINE_MON_ENTITE_ACHAT </com:TTRanslate> :</label>
					</div>
				    <com:TLabel id="labelEntite" />
				</div>
				<div class="line">
					<div class="intitule-150">
						<com:TRadioButton  GroupName="choixEntite"  id="autreEntite" Attributes.title="<%=Prado::localize('DEFINE_AUTRE_ENTITE_ACHAT')%>" />
						<com:TLabel id="labelAutreEntite" />
					</div>
					<com:TDropDownList id="listeAutreEntites" Attributes.title="<%=Prado::localize('DEFINE_AUTRE_ENTITE_ACHAT')%>" CssClass="bloc-590"/>
					<com:TActiveImageButton id="buttonOk" CssClass="ok" onCallBack="Page.onClickSearch" Attributes.title="Ok" AlternateText="Ok"  ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/bouton-ok.gif"/>
				</div>
				<div class="breaker"></div>
			</div>
			<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
		</div>
		<com:TActiveLabel id="hiddenIdService"  Visible="True"/>
		<com:TActiveLabel ID="js"/>
		</com:TPanel>
		<div class="spacer-mini"></div>
		<!--Fin Bloc Choix Entite d'achat-->
