<!--Debut colonne droite-->
<div class="resultSearch main-part" id="main-part">
	<div class="clearfix">
		<nav role="navigation">
		<ul class="breadcrumb">
			<li>
				<a href="/entreprise">
						<com:TTranslate>TEXT_ACCUEIL</com:TTranslate>
                    </a>
			</li>
			<li>
				<a href="?page=Entreprise.EntrepriseAdvancedSearch&searchAnnCons">
					<com:TTranslate>TEXT_RECHERCHE_AVANCEE</com:TTranslate>
				</a>
			</li>
			<li>
				<com:TTranslate>TEXT_RESULTAT</com:TTranslate>
			</li>
		</ul>
		</nav>
	</div>

	<!--BEGIN BTN NOUVEL RECHERCHER / MODIFIER MA RECHERCHE TOP-->
	<com:TPanel cssClass="actions-page-fixed" visible="<%=(isset($_GET['idAlerte']) || $this->getListeCons()) ? false:true%>">

		<div class="relative">

			<ul class="list-unstyled clearfix m-t-2 dropdown-toggle-menu" style="display:block;" aria-labelledby="dropdownSecondMenu">
				<li class="clearfix">

					<com:TLinkButton visible="<%=(!$this->Page->AdvancedSearch->conditionModifierMaRecherche())? true:false%>"
									 cssClass="btn-link"
									 Text="<div tabindex='0' alt='Modifier recherche' class='btn btn-primary' data-toggle='tooltip' data-placement='left' title='<%=Prado::localize('TEXT_MODIFIER_MA_RECHERCHE')%>'><i class='fa fa-search'></i></div>"
									 onClick="Page.AdvancedSearch.displayCriteria"/>

					<com:TLinkButton visible="<%=($this->Page->AdvancedSearch->conditionModifierMaRecherche())? true:false%>"
									 cssClass="btn-link"
									 Text="<div tabindex='0' class='btn btn-primary' data-toggle='tooltip' data-placement='left' title='<%=Prado::localize('TEXT_MODIFIER_MA_RECHERCHE')%>'><i class='fa fa-search'></i></div>"
									 onClick="Page.AdvancedSearch.lienRechercherAvanceePanier"/>

				</li>
				<li class="clearfix">

					<com:TLinkButton cssClass="btn-link tttt"
									 Text="<div tabindex='0' alt='Nouvelle recherche' class='btn btn-primary' data-toggle='tooltip' data-placement='left' title='<%=Prado::localize('DEFINE_NOUVELLE_RECHERCHE')%>'><i class='fa fa-search-plus'></i></div>"
									 onClick="Page.AdvancedSearch.displayAdvanceSearch"/>
				</li>
			</ul>
		</div>

	</com:TPanel>
	<!--END BTN NOUVEL RECHERCHER / MODIFIER MA RECHERCHE TOP-->

	<!-- BEGIN LEGENDE -->
	<div class="modal fade modal-legende" tabindex="-1" role="dialog" style="display:<%=($this->getBaseDce() || $this->getListeCons() ) ? 'none':'' %>;">
		<div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<div class="h4 modal-title">
						<com:TTranslate>LEGENDE</com:TTranslate>
					</div>
				</div>
				<div class="modal-body clearfix">
					<div class="col-md-6">
						<ul class="list-unstyled">
							<li class="h4 page-header m-t-0"><%=Prado::localize('REPONSE_ELEC')%></li>
							<li class="clearfix">
								<div class="col-md-1">
									<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/reponse-elec-oblig.gif" alt="R&eacute;ponse &eacute;l&eacute;ctronique obligatoire"/>
								</div>
								<div class="col-md-11">
									<%=Prado::localize('TEXT_REPONSE_OBLIGATOIRE_POUR_CONSULTATION_SANS_SIGNATURE')%>
								</div>
							</li>
							<li class="clearfix m-t-2">
								<div class="col-md-1">
									<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/reponse-elec.gif" alt="R&eacute;ponse &eacute;l&eacute;ctronique"/>
								</div>
								<div class="col-md-11">
									<%=Prado::localize('TEXT_REPONSE_NON_OBLIGATOIRE_POUR_CONSULTATION_SANS_SIGNATURE')%>
								</div>
							</li>
							<li class="clearfix m-t-2">
								<div class="col-md-1">
									<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/reponse-elec-oblig-avec-signature.gif" alt="R&eacute;ponse &eacute;l&eacute;ctronique obligatoire avec signature"/>
								</div>
								<div class="col-md-11">
									<%=Prado::localize('TEXT_REPONSE_OBLIGATOIRE_POUR_CONSULTATION_AVEC_SIGNATURE')%>
								</div>
							</li>
							<li class="clearfix m-t-2">
								<div class="col-md-1">
									<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/reponse-elec-avec-signature.gif" alt="R&eacute;ponse &eacute;l&eacute;ctronique avec signature"/>
								</div>
								<div class="col-md-11">
									<%=Prado::localize('TEXT_REPONSE_NON_OBLIGATOIRE_POUR_CONSULTATION_AVEC_SIGNATURE')%>
								</div>
							</li>
							<li class="clearfix m-t-2">
								<div class="col-md-1">
									<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/reponse-elec-non.gif" alt="R&eacute;ponse &eacute;l&eacute;ctronique non obligatoire"/>
								</div>
								<div class="col-md-11">
									<%=Prado::localize('TEXT_PAS_REPONSE_POUR_CONSULTATION')%>
								</div>
							</li>

							<com:TPanel ID="panelElementMPS" visible="<%=(!Application\Service\Atexo\Atexo_Module::isEnabled('MasquerElementsMps'))? true:false %>">
								<li style="display:<%=(Application\Service\Atexo\Atexo_Module::isEnabled('MarchePublicSimplifieEntreprise') ? '' : 'none')%>" class="clearfix m-t-2">
									<div class="col-md-1">
										<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/logo-mps-small.png" class="logo-mps" alt="Logo MPS"
											 style="display:<%=(Application\Service\Atexo\Atexo_Module::isEnabled('MarchePublicSimplifieEntreprise') ? '' : 'none')%>"/>
									</div>
									<div class="col-md-11">
										<%=Prado::localize('TEXT_MARCHE_SIMPLIFIE')%>
									</div>
								</li>
							</com:TPanel>
							<li style="display:<%=(Application\Service\Atexo\Atexo_Module::isEnabled('InterfaceDume') ? '' : 'none')%>" class="clearfix m-t-2">
								<div class="col-md-1">
									<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/logo-dume-publie.png" class="logo-dume" alt="<%=Prado::localize('DEFINE_DUME_ACHETEUR_PUBLIE')%>"
										 style="display:<%=(Application\Service\Atexo\Atexo_Module::isEnabled('InterfaceDume') ? '' : 'none')%>" title="<%=Prado::localize('DEFINE_DUME_ACHETEUR_PUBLIE')%>"/>
								</div>
								<div class="col-md-11">
									<%=Prado::localize("DEFINE_DUME_ACHETEUR_PUBLIE")%>
								</div>
							</li>
							<li style="display:<%=(Application\Service\Atexo\Atexo_Module::isEnabled('InterfaceDume') ? '' : 'none')%>" class="clearfix m-t-2">
								<div class="col-md-1">
									<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/logo-dume-non-publie.png" class="logo-dume" alt="<%=Prado::localize('DEFINE_DUME_ACHETEUR_NON_PUBLIE')%>"
										 style="display:<%=(Application\Service\Atexo\Atexo_Module::isEnabled('InterfaceDume') ? '' : 'none')%>" title="<%=Prado::localize('DEFINE_DUME_ACHETEUR_NON_PUBLIE')%>"/>
								</div>
								<div class="col-md-11">
									<%=Prado::localize("DEFINE_DUME_ACHETEUR_NON_PUBLIE")%>
								</div>
							</li>
						</ul>
					</div>
					<div class="col-md-6">
						<ul class="list-unstyled">
							<li class="h4 page-header m-t-0">
								<com:TTranslate>DEFINE_ACTIONS</com:TTranslate>
							</li>
							<li class="clearfix m-t-2">
								<div class="col-md-1">
									<i class="fa fa-search-plus m-r-1"></i>
								</div>
								<div class="col-md-11">
									<%=Prado::localize('ICONE_VOIR_LE_DETAIL')%>
								</div>
							</li>
							<li class="clearfix m-t-2">
								<div class="col-md-1">
									<i class="fa fa-sign-in m-r-1"></i>
								</div>
								<div class="col-md-11">
									<%=Prado::localize('ACCEDER_ANNONCE')%>
								</div>
							</li>
							<li class="clearfix m-t-2" style="display:<%=(Application\Service\Atexo\Atexo_Module::isEnabled('AnnoncesMarches') ? 'none' : '')%>">
								<div class="col-md-1">
									<i class="fa fa-cog m-r-1"></i>
								</div>
								<div class="col-md-11">
									<%=Prado::localize('TESTER_CONFIGURATION_MON_POSTE')%>
								</div>
							</li>
							<li style="display:<%= (Application\Service\Atexo\Atexo_Module::isEnabled('PartagerConsultation') && $this->calledFromEntreprise() && !$this->searchAnnonce())? '' : 'none' %>">
								<div class="col-md-1">
									<img class="bouton-partager" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-partager.gif" alt="Partager"
										 style="display:<%=(Application\Service\Atexo\Atexo_Module::isEnabled('PartagerConsultation') && $this->calledFromEntreprise() && !$this->searchAnnonce())? '' : 'none' %>"/>
								</div>
								<div class="col-md-11">
									<com:TTranslate>'DEFINE_PARTAGER'</com:TTranslate>
								</div>
							</li>
							<li class="clearfix m-t-2" style="display:<%=(Application\Service\Atexo\Atexo_Module::isEnabled('panierEntreprise') ? '' : 'none')%>">
								<div class="col-md-1">
									<i class="fa fa-cart-arrow-down m-r-1"></i>
								</div>
								<div class="col-md-11">
									<%=Prado::localize('DEFINE_AJOUTER_PANIER')%>
								</div>
							</li>
							<li class="clearfix m-t-2" style="display:<%=(Application\Service\Atexo\Atexo_Module::isEnabled('panierEntreprise') ? '' : 'none')%>">
								<div class="col-md-1">
									<i class="fa fa-trash-o m-r-1"></i>
								</div>
								<div class="col-md-11">
									<%=Prado::localize('DEFINE_SUPPRIMER_DU_PANIER')%>
								</div>
							</li>
							<li class="clearfix m-t-2" style="display:<%=(Application\Service\Atexo\Atexo_Module::isEnabled('MarchePublicSimplifieEntreprise') ? '' : 'none')%>">
								<div class="col-md-1">
									<i class="fa fa-share-square-o m-r-1"></i>
								</div>
								<div class="col-md-11">
									<com:TTranslate>DEFINE_REPONDRE_MARCHE_PUBLIC_SIMPLIFIE</com:TTranslate>
								</div>
							</li>
						</ul>
						<com:TPanel ID="panelClauseLegende" visible="<%= Application\Service\Atexo\Atexo_Module::isEnabled('ConsultationClause') %>">
							<ul class="list-unstyled">
								<li class="h4 page-header">
									<com:TTranslate>TEXT_MENU_CLAUSES</com:TTranslate>
								</li>
								<li class="clearfix m-t-2">
									<div class="col-md-1">
										<i class="s s-social m-r-1"></i>
									</div>
									<div class="col-md-11">
										<com:TTranslate>DEFINE_CLAUSES_SOCIALES_INSERTION</com:TTranslate>
									</div>
								</li>
								<li class="clearfix m-t-2">
									<div class="col-md-1">
										<i class="s s-environnement m-r-1 s-18"></i>
									</div>
									<div class="col-md-11">
										<com:TTranslate>CLAUSES_ENVIRONNEMENTALES</com:TTranslate>
									</div>
								</li>
							</ul>
						</com:TPanel>
					</div>
				</div>
				<div class="modal-footer">
					<div class="col-md-2 pull-right">
						<button type="button" class="btn btn-sm btn-block btn-danger" data-dismiss="modal">Fermer</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- END LEGENDE -->

	<!-- BEGIN AUCUN ELEMENT -->
	<com:TPanel ID="panelNoElementFound" cssClass="alert alert-warning">
		<div class="h4 text-center">
			<com:TTranslate>DEFINE_AUCUN_RESULTAT</com:TTranslate>
		</div>
	</com:TPanel>
	<!-- END AUCUN ELEMENT -->

	<!-- BEGIN ELEMENT FOUND-->
	<com:TPanel ID="panelElementsFound">
		<!--BEGIN BLOC FORMULAIRE AVEC ONGLETS-->
		<div id="tabNav">
			<!--BEGIN NAVIGATION ONGLET -->
			<div class="page-header h4 m-t-2 m-b-0">
				<div class="clearfix">
					<h1 class="h4 m-b-0 pull-left" id="form-nav" style="display:<%=($this->getBaseDce() || $this->getListeCons() ) ? 'none':'' %>;">
						<com:TTranslate visible="<%=($this->searchAnnonce()) ? true:false%>;">TEXT_AUTRES_ANNONCES</com:TTranslate>
						<com:TTranslate visible="<%=($this->searchAnnonce()) ? false:true%>;">TEXT_AVIS_DE_CONSULTATION</com:TTranslate>
					</h1>
					<div class="pull-right">
						<a href="#" class="btn btn-primary btn-sm" data-toggle="modal" data-target=".modal-legende">
							<com:TTranslate>LEGENDE</com:TTranslate>
							<i class="fa fa-question"></i>
						</a>
					</div>
				</div>
			</div>
			<!--END NAVIGATION ONGLET -->

			<!--BEGIN LAYER 1-->
			<div class="ongletLayer">
				<!-- BEGIN PAGINATION TOP-->
				<div class="clearfix m-t-1">
					<div class="pull-left">
						<div class="h5">
							<strong>
								<com:TTranslate>DEFINE_NOMBRE_RESULTAT</com:TTranslate>
								<com:TLabel ID="nombreElement"/>
							</strong>
						</div>
					</div>


					<div class="form-inline pagination form-group-sm pull-right <%=( ($this->hideShowPagination())? 'show' : 'hide' )%>">
						<div class="form-group pagination-section-resultperpage">
							<label for="ctl0_CONTENU_PAGE_resultSearch_listePageSizeTop" class="m-r-1">
								<com:TTranslate>DEFINE_TEXT_AFFICHER</com:TTranslate>
							</label>
							<com:TDropDownList ID="listePageSizeTop" AutoPostBack="true"
											   onSelectedIndexChanged="changePagerLenght"
											   cssClass="form-control">
								<com:TListItem Text="10" Selected="10"/>
								<com:TListItem Text="20" Value="20"/>
							</com:TDropDownList>
						</div>
						<div class="form-group pagination-section-currentpage">
							<label class="sr-only">
								<com:TTranslate>DEFINE_ALLER_PAGE</com:TTranslate>
							</label>
							<com:TPanel ID="panelBouttonGotoPageTop" DefaultButton="DefaultButtonTop" CssClass="float-left">
								<label for="ctl0_CONTENU_PAGE_resultSearch_numPageTop" class="m-l-1 m-r-1">
									<com:TTranslate>DEFINE_RESULTATS_PAGE</com:TTranslate>
								</label>
								<com:TTextBox ID="numPageTop" Text="1" CssClass="form-control w-50 text-center"/>
								<label class="nb-total ">/
									<com:TLabel ID="nombrePageTop"/>
								</label>
								<com:TButton ID="DefaultButtonTop" OnClick="goToPage" Attributes.style="display: none"/>
							</com:TPanel>
						</div>
						<div class="form-group liens pagination-section-arrows">
							<label class="m-l-1">
								<com:TPager ID="PagerTop"
											ControlToPaginate="tableauResultSearch"
											FirstPageText="<span class='btn' data-toggle='tooltip' data-placement='top' title='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>'><i class='fa fa-angle-double-left'></i></span>"
											LastPageText="<span class='btn' data-toggle='tooltip' data-placement='top' title='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>'><i class='fa fa-angle-double-right'></i></span>"
											Mode="NextPrev"
											NextPageText="<span class='btn' data-toggle='tooltip' data-placement='top' title='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>'><i class='fa fa-angle-right'></i></span>"
											PrevPageText="<span class='btn' data-toggle='tooltip' data-placement='top' title='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>'><i class='fa fa-angle-left '></i></span>"
											OnPageIndexChanged="pageChanged"/>
							</label>
						</div>
					</div>

				</div>
				<!-- END PAGINATION TOP-->

				<!--BEGIN TABLE CONSULTAION-->
				<div class="table table-striped table-results list-group">
					<com:TRepeater ID="tableauResultSearch"
								   AllowPaging="true"
								   PageSize="10"
								   EnableViewState="true"
								   AllowCustomPaging="true">
						<prop:HeaderTemplate>

							<div class="list-group-item header kt-vertical-align small p-b-0" style="background-color: white">
								<div class="kt-vertical-align">
									<!--BEGIN TRI PAR PROCEDURE | PUBLIE LE -->
									<div class="col-md-2 top cons_ref" id="cons_ref">
										<div class="row">
											<div class="pull-left">
												<div id="checkAll" style="display:<%=( (!Application\Service\Atexo\Atexo_Module::isEnabled('panierEntreprise'))
    																							  ||(isset($_GET['panierEntreprise']))
    																							  ||(isset($_GET['idAlerte']))
    																							  ||($this->Page->resultSearch->searchAnnonce())
    																							  ||(!Application\Service\Atexo\Atexo_Module::isEnabled('EntrepriseActionsGroupees'))
    																							  ||(!$this->TemplateControl->isConnected())
    																							  ||($this->Page->resultSearch->BaseDce)
    																							  ||($this->Page->resultSearch->ListeCons)
    																							   ? 'none' : '')%>">
													<com:TActiveCheckBox
															id="selectAll"
															Attributes.title="<%=Prado::localize('TEXT_TOUS_AUCUN')%>"
															Attributes.onclick="CheckUnCheckAll(this, 'resultSearch_tableauResultSearch', 'annonceSelection');"
													/>
												</div>
											</div>
											<ul class="list-unstyled">
												<li style="color:black!important;"><%=Prado::localize('DEFINE_PROCEDURE')%></li>
												<li>
													<com:TLinkButton CommandName="categorie" OnCommand="SourceTemplateControl.sortParam" cssClass="">
															<span class="btn-link" data-toggle="tooltip" data-placement="top"
																  title="<%=Prado::localize('ICONE_TRIER')%> <%=Prado::localize('PAR')%> '<%=Prado::localize('TEXT_CATEGORIE')%>'">
																<%=Prado::localize('TEXT_CATEGORIE')%>&nbsp;&nbsp;<i class="fa fa-angle-down"></i>
															</span>
													</com:TLinkButton>

												</li>
												<li>
													<com:TLinkButton CommandName="date_mise_en_ligne_calcule" OnCommand="SourceTemplateControl.sortParam" cssClass="">
															<span class="btn-link" data-toggle="tooltip" data-placement="top"
																  title="<%=Prado::localize('ICONE_TRIER')%> <%=Prado::localize('PAR')%> '<%=Prado::localize('DEFINE_PUBLIE_LE')%>'">
																<%=Prado::localize('DEFINE_PUBLIE_LE')%>&nbsp;&nbsp;<i class="fa fa-angle-down"></i>
															</span>
													</com:TLinkButton>
												</li>
											</ul>
										</div>
									</div>
									<!--END TRI PAR PROCEDURE | PUBLIE LE -->

									<!--BEGIN TRI PAR REFERENCE | INTITULE | OBJET | ORGANISME -->
									<div class="col-md-8 top cons_intitule" id="cons_intitule">
										<ul class="list-unstyled">
											<li>
												<com:TLinkButton CommandName="reference_utilisateur" OnCommand="SourceTemplateControl.sortParam" cssClass="">
															<span class="btn-link" data-toggle="tooltip" data-placement="top"
																  title="<%=Prado::localize('ICONE_TRIER')%> <%=Prado::localize('PAR')%> '<%=Prado::localize('TEXT_REFERENCE')%>'">
																<%=Prado::localize('TEXT_REFERENCE')%>&nbsp;&nbsp;<i class="fa fa-angle-down"></i>
															</span>
												</com:TLinkButton>
												<span class="m-r-1 m-l-1 text-primary">|</span>
												<com:TLinkButton CommandName="intitule" OnCommand="SourceTemplateControl.sortParam" cssClass="">
															<span class="btn-link" data-toggle="tooltip" data-placement="top"
																  title="<%=Prado::localize('ICONE_TRIER')%> <%=Prado::localize('PAR')%> '<%=Prado::localize('DEFINE_INTITULE')%>'">
																<%=Prado::localize('DEFINE_INTITULE')%>&nbsp;&nbsp;<i class="fa fa-angle-down"></i>
															</span>
												</com:TLinkButton>
											</li>
											<li>
												<com:TLinkButton CommandName="objet" OnCommand="SourceTemplateControl.sortParam" cssClass="">
															<span class="btn-link" data-toggle="tooltip" data-placement="top"
																  title="<%=Prado::localize('ICONE_TRIER')%> <%=Prado::localize('PAR')%> '<%=Prado::localize('OBJET')%>'">
																<%=Prado::localize('OBJET')%>&nbsp;&nbsp;<i class="fa fa-angle-down"></i>
															</span>
												</com:TLinkButton>
											</li>
											<li>
												<com:TLinkButton CommandName="<%#(Application\Service\Atexo\Atexo_Module::isEnabled('RechercheAutoCompletion'))? 'org_denomination' : 'organisme' %>" OnCommand="SourceTemplateControl.sortParam" cssClass="">
															<span class="btn-link" data-toggle="tooltip" data-placement="top"
																  title="<%=Prado::localize('ICONE_TRIER')%> <%=Prado::localize('PAR')%> '<%=Prado::localize('ORGANISME')%>'">
																<%=Prado::localize('ORGANISME')%>&nbsp;&nbsp;<i class="fa fa-angle-down"></i>
															</span>
												</com:TLinkButton>
											</li>
										</ul>
									</div>
									<!--END TRI PAR REFERENCE | INTITULE | OBJET | ORGANISME -->

									<!--BEGIN LOT LIEUX D'EXECUTION-->
									<com:TConditional condition="$this->Page->resultSearch->searchAnnonce()">
										<prop:trueTemplate>
											<div class="col-md-8 top cons_lieuExe" id="cons_lieuExe">
												<com:TLabel visible="<%#($this->Page->resultSearch->searchAnnonce() || $this->TemplateControl->getBaseDce()) ? false:true%>">
													<com:TTranslate>DEFINE_LOTS</com:TTranslate>
												</com:TLabel>
												<div class="info-clauses" style="display:<%#(Application\Service\Atexo\Atexo_Module::isEnabled('ConsultationClause') && !$this->Page->resultSearch->searchAnnonce())?'':'none'%>">
													<br/>
													<a href="javascript:void(0);"
													   onmouseover="afficheBulle('infoClauses', this)"
													   onmouseout="cacheBulle('infoClauses')"
													   onfocus="afficheBulle('infoClauses', this)"
													   onblur="cacheBulle('infoClauses')"
													   class="info-suite">
														<com:TTranslate>DEFINE_CLAUSES_SOC_ENV</com:TTranslate>
													</a>

													<div class="info-bulle" id="infoClauses">
														<div>
															<ul>
																<li>-
																	<com:TTranslate>CLAUSES_SOCIALES</com:TTranslate>
																</li>
																<li>-
																	<com:TTranslate>DEFINE_MARCHES_RESRERVES</com:TTranslate>
																</li>
																<li>-
																	<com:TTranslate>CLAUSES_ENVIRONNEMENTALES</com:TTranslate>
																</li>
															</ul>
														</div>
													</div>
												</div>
												<com:TLabel id="cons_codes_nuts"
															Visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('CodeNutLtReferentiel') && (!$this->TemplateControl->getBaseDce()) )? true:false %>">
													<com:TTranslate>DEFINE_LIEUX_EXECUTION</com:TTranslate>
												</com:TLabel>
												<com:TLabel style="display:<%#($this->Page->resultSearch->searchAnnonce()) ? '':'none'%>;">
													<com:TTranslate>TEXT_DETAIL</com:TTranslate>
												</com:TLabel>
											</div>
										</prop:trueTemplate>
									</com:TConditional>
									<!--END LOT LIEUX D'EXECUTION-->

									<!--BEGIN DATE DE REMISE EN PLIS-->
									<div class="col-md-1 top cons_dateEnd col-<%=($this->Page->resultSearch->searchAnnonce()) ? 90:60%>" id="cons_dateEnd">
										<div class="row">
											<com:TLinkButton CommandName="datefin" OnCommand="SourceTemplateControl.sortParam" style="display:<%#($this->Page->resultSearch->searchAnnonce()) ? 'none' : 'block' %>;">
															<span class="btn-link" data-toggle="tooltip" data-placement="top"
																  title="<%=Prado::localize('ICONE_TRIER')%> <%=Prado::localize('PAR')%> '<%=Prado::localize('DEFINE_DATE_LIMITE_REMIS_PLIS')%>'">
																<%=Prado::localize('DEFINE_DATE_LIMITE_REMIS_PLIS')%>&nbsp;&nbsp;<i class="fa fa-angle-down"></i>
															</span>
											</com:TLinkButton>
										</div>
									</div>
									<!--END DATE DE REMISE EN PLIS-->

									<div class="col-md-1 top col_actions col-panier text-right" id="cons_actions" style="color:black!important;">
										<com:TTranslate>DEFINE_ACTIONS</com:TTranslate>
									</div>
								</div>
							</div>


						</prop:HeaderTemplate>
						<prop:ItemTemplate>
							<div id="row-consultation-<%#base64_encode($this->Data->getId())%>"
								 class="item_consultation list-group-item kt-callout left kt-vertical-align <%#(($this->ItemIndex%2==0)? '':'on')%>  <%#(($this->TemplateControl->isAddToCart($this->ItemIndex, $this->Data))? 'success':'')%>">

								<div class="col-md-2 top cons_ref">
									<div>
										<div class="check-col pull-left" style="display:<%=( (!Application\Service\Atexo\Atexo_Module::isEnabled('panierEntreprise'))
																									  ||(isset($_GET['panierEntreprise']))
																									  ||(isset($_GET['idAlerte']))
																									  ||($this->Page->resultSearch->searchAnnonce())
																									  ||(!Application\Service\Atexo\Atexo_Module::isEnabled('EntrepriseActionsGroupees'))
																									  ||(!$this->TemplateControl->isConnected())
																									  ||($this->Page->resultSearch->BaseDce)
																									  ||($this->Page->resultSearch->ListeCons)
																									   ? 'none' : '')%>">
											<com:THiddenField id="refCons" Value="<%#$this->Data->getId()%>"/>
											<com:THiddenField id="orgCons" Value="<%#$this->Data->getOrganisme()%>"/>
											<com:TCheckBox id="annonceSelection" Attributes.title="<%=Prado::localize('TEXT_DEFINE_SELECTIONNER')%>"/>
										</div>
										<div class="">
											<div class="cons_procedure">
												<abbr data-toggle="tooltip"
													  data-placement="top"
													  title="<%#$this->Page->resultSearch->getLibelleTypeProcedure($this->ItemIndex, $this->Data)%>">
													<com:TLabel text="<%#$this->Data->getTypeProcedure()->getAbbreviation()%>"></com:TLabel>
												</abbr>
											</div>

											<com:TActivePanel id="panelBlocCategorie" cssClass="cons_categorie">
												<com:TLabel text="<%#($this->Page->resultSearch->getCategorieConsultation($this->ItemIndex, $this->Data))%>"></com:TLabel>
											</com:TActivePanel>

											<div class="date date-min clearfix">
												<div class="day">
													<com:TLabel text="<%#$this->Page->resultSearch->getDateProcedure($this->ItemIndex, $this->Data)['day']%>"></com:TLabel>
												</div>
												<div class="month-year">
													<div class="month">
														<com:TLabel text="<%#$this->Page->resultSearch->getDateProcedure($this->ItemIndex, $this->Data)['month_abbreviation']%>"></com:TLabel>
													</div>
													<div class="year">
														<com:TLabel text="<%#$this->Page->resultSearch->getDateProcedure($this->ItemIndex, $this->Data)['year']%>"></com:TLabel>
													</div>
												</div>

											</div>
											<div class="cons_img">
												<com:THyperLink
														NavigateUrl="<%#($this->Page->resultSearch->getUrlSiteOrganisme($this->Data)&& Application\Service\Atexo\Atexo_Module::isEnabled('AfficherImageOrganisme'))? $this->Page->resultSearch->getUrlSiteOrganisme($this->Data):''%>"
														Attributes.target="_blank" Visible="<%#(Application\Service\Atexo\Atexo_Module::isEnabled('AfficherImageOrganisme'))%>" Attributes.title="Organisme">
													<com:TImage ID="logoOrganisme"
																visible="<%#(Application\Service\Atexo\Atexo_Util::isLogoOrganismeVisible($this->Data->getOrganisme()))%>"
																ImageUrl="<%# $this->Page->resultSearch->getUrlLogo($this->Data)%>"
																CssClass="logo-organisme"
																Attributes.title="<%#$this->Data->getDenominationOrganisme()%>"
																Attributes.alt="<%#$this->Data->getDenominationOrganisme()%>"
													/>
												</com:THyperLink>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-8 top cons_intitule">
									<div id="identification_cons_<%#$this->ItemIndex%>" class="identification_consultation">
										<com:TActivePanel cssClass="objet-line" id="panelBlocIntitule">

											<!-- BEGIN REFERENCE | INTITULE -->
											<div class="m-b-1 clearfix">
												<div class="small pull-left">
													<%#$this->Data->getReferenceUtilisateur()%>
												</div>
												<span class="pull-left m-l-1 m-r-1">|</span>
												<div class="small pull-left truncate">
													<span data-toggle="tooltip"
														  title="<%#$this->Page->resultSearch->truncate($this->Page->resultSearch->getIntituleConsultation($this->ItemIndex, $this->Data),140)%>">
														<%#$this->Page->resultSearch->truncate($this->Page->resultSearch->getIntituleConsultation($this->ItemIndex, $this->Data),140)%>
													</span>
												</div>
											</div>
											<!--END REFERENCE | INTITULE-->
										</com:TActivePanel>

										<!-- BEGIN OBJET -->
										<com:TActivePanel cssClass="m-b-1 clearfix" id="panelBlocObjet">
											<div data-toggle="tooltip" class="truncate-700"
												 title="<%#$this->Page->resultSearch->getResumeConsulation($this->ItemIndex, $this->Data)%>">
												<span class="h5">
												<strong><com:TTranslate>OBJET</com:TTranslate> : </strong>
											</span>
												<span class="small">
												<span>
													<%#$this->Page->resultSearch->getResumeConsulation($this->ItemIndex, $this->Data)%>
												</span>
											</span>
											</div>
										</com:TActivePanel>
										<!-- END OBJET -->

										<!-- BEGIN ORGANISME -->
										<com:TActivePanel cssClass="m-b-1" id="panelBlocDenomination">
											<div data-toggle="tooltip" class="truncate-700"
												 title="<%#$this->Page->resultSearch->getDenominationOrganisme($this->ItemIndex, $this->Data)%>">
												<span class="h5">
													<strong><com:TTranslate>ORGANISME</com:TTranslate> : </strong>
												</span>
												<span class="small">
													<%#$this->Page->resultSearch->getDenominationOrganisme($this->ItemIndex, $this->Data)%>
												</span>
											</div>
										</com:TActivePanel>
										<!-- END ORGANISME -->

										<com:TPanel ID="panelLangues" CssClass="langue langue-inline" Visible="<%#$this->Page->resultSearch->showLangagesFlags()%>">
											<span class="intitule"><com:TTranslate>DEFINE_LANGUE</com:TTranslate> :</span>
											<!-- Debut affichage des langues actives accessibles -->
											<com:TRepeater ID="traductionConsultations" EnableViewState="true" DataSource="<%#Application\Service\Atexo\Atexo_Languages::retrieveActiveLangages()%>">
												<prop:ItemTemplate>
													<com:TActiveLinkButton
															Attributes.href="javascript:void(0)"
															id="idBoutonChangerLangueConsultation"
															onCallBack="SourceTemplateControl.actionChangerLangueConsultation"
															ActiveControl.CallbackParameter="<%#$this->TemplateControl->Data->getReference()%>_<%#$this->TemplateControl->ItemIndex%>_<%#$this->Data->getLangue()%>_<%#$this->TemplateControl->Data->getOrganisme()%>"
															cssClass="<%#$this->Page->resultSearch->showLanguage($this->TemplateControl->Data, $this->Data)%>"
															style="display:<%#(($this->Page->resultSearch->showFlag($this->Data->getLangue(), $this->TemplateControl->Data)))%>"
													>
														<com:TImage
																ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/flag-<%#$this->Data->getLangue()%>.png"
																visible="true" Attributes.alt="<%#strtoupper($this->Data->getLangue())%>"
																Attributes.title="<%#strtoupper($this->Data->getLangue())%>"
														/>
													</com:TActiveLinkButton>
												</prop:ItemTemplate>
											</com:TRepeater>
											<!-- Fin affichage des langues actives accessibles -->
										</com:TPanel>

										<div id="infoscons_<%#$this->Data->getId()%>" class="clearfix">
											<ul class="line-infos list-unstyled list-inline clearfix">
												<!-- BEGIN NOMBRE DE LOTS -->
												<li>
													<com:TPanel cssClass="lots"
																Visible="<%#( $this->Page->resultSearch->searchAnnonce() || $this->TemplateControl->getBaseDce() || ($this->Data instanceof Application\Propel\Mpe\CommonConsultation && !count($this->Data->getAllLots())) ) ? false:true%>">
														<a href="javascript:popUpOpen('index.php?page=Entreprise.PopUpDetailLots&orgAccronyme=<%#$this->Data->getOrganisme()%>&id=<%#$this->Data->getId()%>&lang=', 'Descriptif des lots - Consultation', 'toolbar=no,menubar=no,resizable=yeslocation=no,width=800,height=275,scrollbars=yes')">
															<i class="fa fa-cubes text-warning" aria-hidden="true"></i>
															<span><%=($this->Data instanceof Application\Propel\Mpe\CommonConsultation)? count($this->Data->getAllLots()):''%>&nbsp;lots</span>
														</a>
													</com:TPanel>
												</li>
												<script>

												</script>
												<!-- END NOMBRE DE LOTS -->

												<!-- BEGIN LIEUX EXE -->
												<li>
													<com:TActivePanel id="panelBlocLieuxExec" cssClass="lieux-exe" Visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('LieuxExecution'))? true:false %>">
														<span>
															<i class="fa fa-map-marker text-danger" aria-hidden="true"></i>
															<com:AtexoLieuxExecution
																	LibelleLieuxExecution="<%#$this->Page->resultSearch->getLibelleLieuxExecution($this->ItemIndex, $this->Data)%>"/>
														</span>
													</com:TActivePanel>
												</li>
												<!-- END LIEUX EXE -->

												<!--BEGIN PICTO CO-TRAITANCE -->
												<com:TConditional Condition="($this->SourceTemplateControl->showBourse() && $this->Data instanceof Application\Propel\Mpe\CommonConsultation && !$this->Data->getConsultationExterne() == '1' && $this->Data->getUrlConsultationExterne() == '')">
													<prop:TrueTemplate>
														<li class="m-b-1">
															<com:TPanel cssClass="co-traitance">
																<a href="#detail_cons-<%#$this->ItemIndex%>" role="button" alt="Bourse à la co-traitance" data-toggle="popover" data-placement="left" data-target="#detail_cons-<%#$this->ItemIndex%>">
																	<com:TImage
																			ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/<%#$this->SourceTemplateControl->getPictoBourseCotraitance($this->Data->getId())%>"/>
																</a>
															</com:TPanel>
															<div class="hide" id="detail_cons-<%#$this->ItemIndex%>">
																<div class="text-center">
																	<com:TLabel id="messageBC"
																				Text="<%#$this->SourceTemplateControl->getMessageInscriptionBourseCotraitance($this->Data->getId())%>"/>
																	<div class="m-t-1">
																		<com:THyperLink cssClass="btn btn-info btn-sm"
																						NavigateUrl="?page=Entreprise.EntrepriseInscriptionBourseCotraitance&id=<%#$this->Data->getId()%>&orgAcronyme=<%#$this->Data->getOrganisme()%>&inscription"
																						visible="<%# Application\Service\Atexo\Atexo_BourseCotraitance::gererActionBourseCotraitance($this->Data->getId()) == 'nouveau' ? 'true' : 'false' %>"
																		>
																			<i class="fa fa-user-plus" aria-hidden="true"></i>
																			<com:TTranslate>DEFINE_SINSCRIRE</com:TTranslate>
																		</com:THyperLink>
																		<com:THyperLink
																				cssClass="btn btn-sm btn-info"
																				NavigateUrl="?page=Entreprise.EntrepriseRechercheBourseCotraitance&id=<%#$this->Data->getId()%>&orgAcronyme=<%#$this->Data->getOrganisme()%>&GME"
																				visible="<%# Application\Service\Atexo\Atexo_BourseCotraitance::gererActionBourseCotraitance($this->Data->getId()) == 'inscrit' ? 'true' : 'false' %>"
																		>
																			<i class="fa fa-search-plus" aria-hidden="true"></i>
																			<com:TTranslate>DEFINE_DETAILS</com:TTranslate>
																		</com:THyperLink>
																	</div>
																</div>
															</div>
														</li>
													</prop:TrueTemplate>
												</com:TConditional>
												<!--END PICTO CO-TRAITANCE -->

												<!--BEGIN PICTO MPS -->
												<li class="m-b-1">
													<com:TLabel Visible="<%= ($this->Data->getMarchePublicSimplifie() && !Application\Service\Atexo\Atexo_Module::isEnabled('MasquerElementsMps')) ? true : false %>">
														<span data-toggle="tooltip" title="<%=Prado::localize('DEFINE_CONSULTATION_MARCHE_PUBLIC_SIMPLIFIE')%>">
															<i class="s s-mps s-18"></i>
                                                            <span class="sr-only"><com:TTranslate>DEFINE_CONSULTATION_MARCHE_PUBLIC_SIMPLIFIE</com:TTranslate></span>
														</span>
													</com:TLabel>
												</li>
												<!--END PICTO MPS -->

												<!--BEGIN PICTO DUME -->
												<li class="m-b-1">
													<com:TLabel Visible="<%= (Application\Service\Atexo\Atexo_Module::isEnabled('InterfaceDume') && $this->Data->getDumeDemande())? true : false %>">
														<span style="display:<%=($this->Data->hasDumeAcheteurStandard())? '' : 'none'%>;" data-toggle="tooltip" title="<%=Prado::localize('DEFINE_DUME_ACHETEUR_NON_PUBLIE')%>">
                                                        <com:TImage
																ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/logo-dume-non-publie.png"
																cssClass="inline-img"
																Attributes.alt="<%=Prado::localize('DEFINE_DUME_ACHETEUR_NON_PUBLIE')%>"
																Attributes.title="<%=Prado::localize('DEFINE_DUME_ACHETEUR_NON_PUBLIE')%>" />
                                                            <span class="sr-only"><com:TTranslate>DEFINE_DUME_ACHETEUR_NON_PUBLIE</com:TTranslate></span>
														</span>
														<span style="display:<%= $this->Data->hasDumeAcheteurStandard()? 'none' : ''%>;" data-toggle="tooltip" title="<%=Prado::localize('DEFINE_DUME_ACHETEUR_PUBLIE')%>">
                                                        <com:TImage
																ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/logo-dume-publie.png"
																cssClass="inline-img"
																Attributes.alt="<%=Prado::localize('DEFINE_DUME_ACHETEUR_PUBLIE')%>"
																Attributes.title="<%=Prado::localize('DEFINE_DUME_ACHETEUR_PUBLIE')%>" />
                                                            <span class="sr-only"><com:TTranslate>DEFINE_DUME_ACHETEUR_PUBLIE</com:TTranslate></span>
														</span>
													</com:TLabel>
												</li>
												<!--END PICTO DUME -->

												<!--BEGIN PICTO DISPOSITION ENVIRONNEMENTALE -->
												<li class="m-b-1" ID="panelClauseConsultation-<%#base64_encode($this->Data->getId())%>">
													<com:TPanel>
														<com:IconeAchatResponsableConsultation objet="<%#$this->Data%>"/>
													</com:TPanel>
												</li>
												<!--END PICTO DISPOSITION ENVIRONNEMENTALE -->
												<!--BEGIN PICTO JO -->
												<li class="m-b-1">
													<com:TLabel Visible="<%= $this->TemplateControl->isTypageJoActivated($this->Data->getId()) ? true : false %>">
														<img src="app/img/logo-jo2024.png" title="<%=Prado::localize('TYPAGE_JO_2024')%>" alt="<%=Prado::localize('TYPAGE_JO_2024')%>">
													</com:TLabel>
												</li>
												<!--END PICTO JO -->
											</ul>
										</div>
									</div>
								</div>
								<com:TConditional condition="$this->Page->resultSearch->searchAnnonce()">
									<prop:trueTemplate>
										<div class="col-md-8 top cons_lieuExe">
											<com:TLabel Visible="<%#($this->Page->resultSearch->searchAnnonce()) ? true:false%>;">
												<com:TImage visible="<%#($this->Data->getIdTypeAvis()== Application\Service\Atexo\Atexo_Config::getParameter('TYPE_AVIS_ATTRIBUTION')) ? true:false%>"
															ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-avis-attribution.gif" Attributes.title="<%=Prado::localize('ANNONCE_ATTRIBUTION')%>"/>
												<com:TImage visible="<%#($this->Data->getIdTypeAvis()== Application\Service\Atexo\Atexo_Config::getParameter('TYPE_AVIS_INFORMATION')) ? true:false%>"
															ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-avis-info.gif" Attributes.title="<%=Prado::localize('DEFINE_ANNONCE_INFORMATION')%>"/>
												<com:TImage visible="<%#($this->Data->getIdTypeAvis()== Application\Service\Atexo\Atexo_Config::getParameter('TYPE_AVIS_EXTRAIT_PV')) ? true:false%>"
															ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-PV.gif" Attributes.title="<%=Prado::localize('TEXT_EXTRAIT_PV')%>"/>
												<com:TImage visible="<%#($this->Data->getIdTypeAvis()== Application\Service\Atexo\Atexo_Config::getParameter('TYPE_AVIS_RAPPORT_ACHEVEMENT')) ? true:false%>"
															ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-rapport-achevement.gif" Attributes.title="<%=Prado::localize('TEXT_RAPPORT_ACHEVEMENT')%>"/>
											</com:TLabel>
											<com:TActivePanel id="panelBlocCodesNuts" style="display:<%=(Application\Service\Atexo\Atexo_Module::isEnabled('CodeNutLtReferentiel'))? ' ':'none' %>">
												<com:AtexoCodeNuts IdComposant="<%=($this->getClientId())%>"
																   LibelleCodesNuts="<%=$this->Page->resultSearch->getLibelleCodesNutsFromConsultation($this->Data, $this->ItemIndex)%>"/>
											</com:TActivePanel>

											<com:TPanel style="display:<%#($this->Page->resultSearch->searchAnnonce()) ? '':'none'%>;">
												<div data-toggle="tooltip" class="cloture-line truncate-110"
													 data-html="true" data-original-title="<%=$this->Data->getDetailConsultationTraduit()%>">
													<com:TLabel text="<%#$this->Page->resultSearch->truncate($this->Data->getDetailConsultationTraduit(),80)%>"></com:TLabel>
													<com:TLabel Visible="<%#$this->Page->resultSearch->isTextTruncated($this->Data->getDetailConsultationTraduit(),79)%>">
															...
													</com:TLabel>
												</div>
											</com:TPanel>
										</div>
									</prop:trueTemplate>
								</com:TConditional>

								<div class="col-md-1 top cons_dateEnd col-<%#($this->Page->resultSearch->searchAnnonce()) ? 90:60%>">
									<com:TPanel style="display:<%#($this->Page->resultSearch->searchAnnonce()) ? 'none':''%>;">
										<div class="m-triggerWrapper text-center"><a href="#" class="btn btn-primary m--triggerDropdown alt="Voir plus"><i class="fa fa-ellipsis-h" aria-hidden="true"></i></a></div>
										<div class="cloture-line">
											<div class="date clearfix ">
												<div class="day ">
													<com:TLabel text="<%#$this->Page->resultSearch->getDeadline($this->Data->getDateFin())['day']%>"></com:TLabel>
												</div>
												<div class="month-year ">
													<div class="month">
														<com:TLabel text="<%#$this->Page->resultSearch->getDeadline($this->Data->getDateFin())['month_abbreviation']%>"></com:TLabel>
													</div>
													<div class="year">
														<com:TLabel text="<%#$this->Page->resultSearch->getDeadline($this->Data->getDateFin())['year']%>"></com:TLabel>
													</div>
												</div>

											</div>
											<div class="time clearfix text-center m-t-1">
												<i class="fa fa-clock-o"></i>
												<label title="<com:TTranslate>DEFINE_DUREE</com:TTranslate>" alt="<com:TTranslate>DEFINE_DUREE</com:TTranslate>"><%#$this->Page->resultSearch->getDeadline($this->Data->getDatefin())['heure_minutes']%></label>
											</div>
										</div>
									</com:TPanel>
									<div class="text-center m-t-1" data-toggle="tooltip">
										<com:TImage style="display:<%#($this->Page->resultSearch->searchAnnonce() || $this->TemplateControl->getBaseDce() ) ? 'none':''%>;"
													ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/<%#$this->Page->returnPictoReponseConsultation($this->Data)%>"
													CssClass="certificat"
													Attributes.alt="<%#$this->Page->returnTitlePictoReponseConsultation($this->Data)%>"
										/>
									</div>

								</div>
								<div class="col-md-1 middle col_actions actions m-triggerWrapperPanier">
									<ul class="list-unstyled list-actions pull-right">
										<!-- BEGIN RETRAIT-->
										<li class="item-action" style="display:<%=(Application\Service\Atexo\Atexo_Module::isEnabled('PanierEntreprise') && isset($_GET['panierEntreprise']))? '':'none'%>">
											<com:TActivePanel Visible="<%#(!$this->Data->getConsultationExterne() && Application\Service\Atexo\Atexo_Module::isEnabled('PanierEntreprise') && isset($_GET['panierEntreprise']))%>">
												<div class="registre-line" data-toggle="tooltip" title="<%=Prado::localize('REGISTRE_RETRAITS')%>">
													<com:TActiveHyperLink cssClass="btn btn-primary btn-sm relative"
																		  NavigateUrl="<%#$this->Page->resultSearch->getUrlDetailConsultationPanier($this->Data, 'retraits')%>">
														<span class="sr-only">Retrait</span>
														<i class="fa fa-upload fa-lg"></i>
														<com:TPanel
																visible="<%#(Application\Service\Atexo\Consultation\Atexo_Consultation_Register::retrieveTelechargementByIdInscritAndRefCons($this->Data->getId(), $this->Data->getOrganisme(), $this->TemplateControl->getIdEntreprise(), $this->TemplateControl->getIdInscrit(), true))%>">
																	<span class="notification-bubble text-center ">
																	  <%#isset($_GET['panierEntreprise'])?Application\Service\Atexo\Consultation\Atexo_Consultation_Register::retrieveTelechargementByIdInscritAndRefCons($this->Data->getId(), $this->Data->getOrganisme(), $this->TemplateControl->getIdEntreprise(), $this->TemplateControl->getIdInscrit(), true):0%>
																	</span>
														</com:TPanel>
													</com:TActiveHyperLink>
												</div>
											</com:TActivePanel>
										</li>
										<!-- END RETRAIT-->

										<!-- BEGIN QUESTION-->
										<li class="item-action" style="display:<%=(Application\Service\Atexo\Atexo_Module::isEnabled('PanierEntreprise') && isset($_GET['panierEntreprise']))? '':'none'%>">
											<com:TActivePanel Visible="<%#(!$this->Data->getConsultationExterne() && Application\Service\Atexo\Atexo_Module::isEnabled('PanierEntreprise') && isset($_GET['panierEntreprise']))%>">
												<div class="registre-line" data-toggle="tooltip" title="<%=Prado::localize('QUESTIONS')%>">
													<com:TActiveHyperLink cssClass="btn btn-primary btn-sm relative"
																		  NavigateUrl="<%#$this->Page->resultSearch->getUrlDetailConsultationPanier($this->Data, 'questions')%>">
														<span class="sr-only">Question</span>
														<i class="fa fa-question-circle fa-lg"></i>
														<com:TPanel
																visible="<%#(Application\Service\Atexo\Consultation\Atexo_Consultation_Register::retrieveQuestionByIdInscritAndRefCons($this->Data->getId(), $this->Data->getOrganisme(), $this->TemplateControl->getIdEntreprise(), $this->TemplateControl->getIdInscrit(), true))%>">
													<span class="notification-bubble text-center ">
														<%#isset($_GET['panierEntreprise'])?Application\Service\Atexo\Consultation\Atexo_Consultation_Register::retrieveQuestionByIdInscritAndRefCons($this->Data->getId(), $this->Data->getOrganisme(), $this->TemplateControl->getIdEntreprise(), $this->TemplateControl->getIdInscrit(), true):0%>
													</span>
														</com:TPanel>
													</com:TActiveHyperLink>
												</div>
											</com:TActivePanel>
										</li>
										<!-- END QUESTION-->

										<!-- BEGIN DEPOT-->
										<li class="item-action" style="display:<%=(Application\Service\Atexo\Atexo_Module::isEnabled('PanierEntreprise') && isset($_GET['panierEntreprise']))? '':'none'%>">
											<com:TActivePanel Visible="<%#(!$this->Data->getConsultationExterne() && Application\Service\Atexo\Atexo_Module::isEnabled('PanierEntreprise') && isset($_GET['panierEntreprise']))%>">
												<div class="registre-line" data-toggle="tooltip" title="<%=Prado::localize('DEPOTS')%>">
													<com:TActiveHyperLink cssClass="btn btn-primary btn-sm relative"
																		  NavigateUrl="<%#$this->Page->resultSearch->getUrlDetailConsultationPanier($this->Data, 'depots')%>">
														<span class="sr-only">d&eacute;p&ocirc;t</span>
														<i class="fa fa-download fa-lg"></i>
														<com:TPanel
																visible="<%#(isset($_GET['panierEntreprise']) && Application\Service\Atexo\Entreprise\Atexo_Entreprise_Reponses::retrieveReponsesEntreprise($this->Data->getId(), $this->Data->getOrganisme(), $this->TemplateControl->getIdInscrit(), $this->TemplateControl->getIdEntreprise(), true))%>">
														<span class="notification-bubble text-center ">
															<%#isset($_GET['panierEntreprise'])?Application\Service\Atexo\Entreprise\Atexo_Entreprise_Reponses::retrieveReponsesEntreprise($this->Data->getId(), $this->Data->getOrganisme(), $this->TemplateControl->getIdInscrit(), $this->TemplateControl->getIdEntreprise(), true):0%>
														</span>
														</com:TPanel>
													</com:TActiveHyperLink>
												</div>
											</com:TActivePanel>
										</li>
										<!-- END DEPOT-->

										<!-- BEGIN MESSAGE ECHANGE-->
										<li class="item-action" style="display:<%=(Application\Service\Atexo\Atexo_Module::isEnabled('PanierEntreprise') && isset($_GET['panierEntreprise']))? '':'none'%>">
											<div class="registre-line" data-toggle="tooltip" title="<%=Prado::localize('MESSAGE_ECHANGE')%>">
												<com:TActiveHyperLink cssClass="btn btn-primary btn-sm relative"
																	  NavigateUrl="<%#$this->Page->resultSearch->getUrlDetailConsultationPanier($this->Data, 'echanges')%>">
													<span class="sr-only">Message &eacute;chang&eacute;s</span>
													<i class="fa fa-comments fa-lg"></i>
													<com:TPanel visible="($this->Data->getConsultationExterne()=='1' && $this->Data->getUrlConsultationExterne()!= '' )">
														<%#isset($_GET['panierEntreprise']) && $this->TemplateControl->getIdInscrit() ?Application\Service\Atexo\Atexo_Message::getAllEchangesDestinataireByIdRef($this->Data->getId(), $this->Data->getOrganisme(),array(),false, false, $this->TemplateControl->getIdInscrit() ,true):0%>
													</com:TPanel>
												</com:TActiveHyperLink>
											</div>
										</li>
										<!-- END MESSAGE ECHANGE-->
										<!-- BEGIN ACCEDER A LA CONSULTATION PROFIL ACHETEUR-->
										<li class="item-action" style="display:<%=(Application\Service\Atexo\Atexo_Module::isEnabled('PanierEntreprise') && isset($_GET['panierEntreprise']))? '':'none'%>"
											data-toggle="tooltip" title="<%=Prado::localize('TEXT_ACCEDER_CONSULTATION_PROFIL_ACHETEUR')%>">
											<com:TActiveHyperLink NavigateUrl="<%#$this->Data->getUrlConsultationExterne()%>" Attributes.target="_blank" cssClass="btn btn-sm btn-primary"
																  Visible="<%#($this->Data->getConsultationExterne()=='1' && $this->Data->getUrlConsultationExterne()!= '' )%>">
												<span class="sr-only"><%=Prado::localize('TEXT_ACCEDER_CONSULTATION_PROFIL_ACHETEUR')%></span>
												<i class="fa fa-share"></i>
											</com:TActiveHyperLink>
										</li>
										<!-- END ACCEDER A LA CONSULTATION PROFIL ACHETEUR-->
										<!-- BEGIN BOAMP-->
										<li data-toggle="tooltip" title="<%=Prado::localize('TEXT_ACCEDER_CONSULTATION_BOAMP')%>">
											<com:TActiveHyperLink NavigateUrl="<%#$this->Data->getUrlConsultationAvisPub()%>" Attributes.target="_blank" cssClass="btn btn-sm btn-primary"
																  Visible="<%#($this->Data->getConsultationExterne()=='1' && $this->Data->getUrlConsultationAvisPub()!= '' )%>">
												<i class="fa fa-share-alt"></i>
											</com:TActiveHyperLink>
										</li>
										<!-- END BOAMP-->
										<!-- BEGIN SUPPRIMER-->
										<li class="item-action" style="display:<%=(Application\Service\Atexo\Atexo_Module::isEnabled('PanierEntreprise') && isset($_GET['panierEntreprise']))? '':'none'%>">
											<div class="registre-line" data-toggle="tooltip" title="<%=Prado::localize('DEFINE_SUPPRIMER_DU_PANIER')%>">
												<button type="button" class="btn btn-danger btn-sm relative btn-block"
														data-toggle="ajaxmodal"
														data-target="?page=Entreprise.EntrepriseModal&controls=CAtexoModalGestionPanier&action=delete&id=<%#base64_encode($this->Data->getId())%>&orgAcronyme=<%#$this->Data->getOrganisme()%>&idRow=row-consultation-<%#base64_encode($this->Data->getId())%>"
														data-json='{
																		"ajaxmodal":{
																			"callback":"KTJS.pages_entreprise_advancedsearch.consultation.callback.delete"
																		},
																		"data":{
																			"target":"#row-consultation-<%#base64_encode($this->Data->getId())%>"
																		}
																	}'>
													<i class="fa fa-trash fa-lg"></i>
													<span class="sr-only"><com:TTranslate>DEFINE_SUPPRIMER_DU_PANIER</com:TTranslate></span>
												</button>

											</div>
										</li>
										<!-- END SUPPRIMER-->
										<li>
											<com:TActivePanel id="panelAction" Visible="<%# !($this->TemplateControl->getBaseDce())%>" cssClass="actions">
												<ul class="list-unstyled">

													<li class="item-action" style="display:<%=(!isset($_GET['panierEntreprise']) )? '':'none'%>" data-toggle="tooltip" title="<%=Prado::localize('DEFINE_ACCEDER_CONSULTATION')%>">
														<com:TConditional condition="$this->TemplateControl->getListeCons()">
															<prop:trueTemplate>
																<com:TActiveHyperLink cssClass="btn btn-primary btn-sm"
																				  NavigateUrl="<%#$this->TemplateControl->getUrlDetailsConsultation($this->Data, 'orgAcronyme='.$this->Data->getOrganisme())%>"
																				  Target="_blank"
																				  Visible="<%#($this->Data->getConsultationAchatPublique()=='0' && $this->Data->getConsultationExterne()=='0')%>">
																<span class="sr-only"><%=Prado::localize('DEFINE_ACCEDER_CONSULTATION')%></span>
																<i class="fa fa-sign-in fa-lg"></i>
																</com:TActiveHyperLink>
															</prop:trueTemplate>
															<prop:falseTemplate>
																<com:TActiveHyperLink cssClass="btn btn-primary btn-sm"
																					  NavigateUrl="<%#$this->TemplateControl->getUrlDetailsConsultation($this->Data, 'orgAcronyme='.$this->Data->getOrganisme())%>"
																					  Visible="<%#($this->Data->getConsultationAchatPublique()=='0' && $this->Data->getConsultationExterne()=='0')%>">
																	<span class="sr-only"><%=Prado::localize('DEFINE_ACCEDER_CONSULTATION')%></span>
																	<i class="fa fa-sign-in fa-lg"></i>
																</com:TActiveHyperLink>
															</prop:falseTemplate>
														</com:TConditional>

													</li>

													<li class="item-action" style="display:<%=(!isset($_GET['panierEntreprise']) )? '':'none'%>" data-toggle="tooltip" title="<%=Prado::localize('TELECHARGER_REGLEMENT_CONSULTATION')%>">
														<com:TConditional condition="$this->TemplateControl->getListeCons()">
															<prop:trueTemplate>
																<com:TActiveHyperLink cssClass="btn btn-primary btn-sm"
																					  NavigateUrl="<%#$this->TemplateControl->getUrlRC($this->Data)%>"
																					  Target="_blank"
																					  Visible="<%#$this->TemplateControl->getUrlRC($this->Data)!=''%>">
																	<span class="sr-only"><%=Prado::localize('TELECHARGER_REGLEMENT_CONSULTATION')%></span>
																	<strong><%=Prado::localize('REGLEMENT_CONSULTATION_RC')%></strong>
																</com:TActiveHyperLink>
															</prop:trueTemplate>
															<prop:falseTemplate>
																<com:TActiveHyperLink cssClass="btn btn-primary btn-sm"
																					  NavigateUrl="<%#$this->TemplateControl->getUrlRC($this->Data)%>"
																					  Visible="<%#$this->TemplateControl->getUrlRC($this->Data)!=''%>">
																	<span class="sr-only"><%=Prado::localize('TELECHARGER_REGLEMENT_CONSULTATION')%></span>
																	<strong><%=Prado::localize('REGLEMENT_CONSULTATION_RC')%></strong>
																</com:TActiveHyperLink>
															</prop:falseTemplate>
														</com:TConditional>
													</li>

													<li class="item-action" style="display:<%=(!isset($_GET['panierEntreprise']) )? '':'none'%>" data-toggle="tooltip" title="<%=Prado::localize('DEFINE_ACCEDER_CONSULTATION')%>">
														<com:TActiveHyperLink NavigateUrl="<%#$this->Data->getUrlConsultationAchatPublique()%>"
																			  Attributes.target="_blank"
																			  cssClass="btn btn-sm btn-primary"
																			  Visible="<%#($this->Data->getConsultationAchatPublique()=='1' && $this->Data->getUrlConsultationAchatPublique()!='urlACompleter.com')%>">
															<label class="sr-only"><%=Prado::localize('DEFINE_ACCEDER_CONSULTATION')%></label>
															<i class="fa fa-share"></i>
														</com:TActiveHyperLink>
													</li>

													<li class="item-action" style="display:<%=(!isset($_GET['panierEntreprise']) )? '':'none'%>" data-toggle="tooltip" title="<%=Prado::localize('TEXT_ACCEDER_CONSULTATION_PROFIL_ACHETEUR')%>">
														<com:TActiveHyperLink NavigateUrl="<%#$this->Data->getUrlConsultationExterne()%>"
																			  Attributes.target="_blank"
																			  cssClass="btn btn-sm btn-primary"
																			  Visible="<%#($this->Data->getConsultationExterne()=='1' && $this->Data->getUrlConsultationExterne()!= '' )%>">
															<label class="sr-only"><%=Prado::localize('TEXT_ACCEDER_CONSULTATION_PROFIL_ACHETEUR')%></label>
															<i class="fa fa-share"></i>
														</com:TActiveHyperLink>
													</li>

													<li class="item-action" style="display:<%=(!isset($_GET['panierEntreprise']) )? '':'none'%>">
														<com:TActiveHyperLink NavigateUrl="<%#$this->Data->getUrlConsultationAvisPub()%>"
																			  Attributes.target="_blank"
																			  Visible="<%#($this->Data->getUrlConsultationAvisPub()!= '' )%>">
															<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-acces-boamp.gif" alt="<%=Prado::localize('TEXT_ACCEDER_CONSULTATION_BOAMP')%>"
																 data-toggle="tooltip" title="<%=Prado::localize('TEXT_ACCEDER_CONSULTATION_BOAMP')%>"/></com:TActiveHyperLink>
													</li>

													<li class="item-action" style="display:<%=(!isset($_GET['panierEntreprise']) )? '':'none'%>" data-toggle="tooltip" title="<%=Prado::localize('DEFINE_ACCEDER_CONSULTATION')%>">
														<com:TConditional condition="$this->TemplateControl->getListeCons()">
															<prop:trueTemplate>
																<com:TActiveHyperLink NavigateUrl="#"
																					  Target="_blank"
																					  cssClass="btn btn-sm btn-primary"
																					  Visible="<%#($this->Data->getConsultationAchatPublique()=='1' && $this->Data->getUrlConsultationAchatPublique()=='urlACompleter.com')%>">
																	<span class="sr-only"><%=Prado::localize('DEFINE_ACCEDER_CONSULTATION')%></span>
																	<i class="fa fa-sign-in fa-lg" data-tnr="accederConsultation"></i>
																</com:TActiveHyperLink>
															</prop:trueTemplate>
															<prop:falseTemplate>
																<com:TActiveHyperLink NavigateUrl="#"
																					  cssClass="btn btn-sm btn-primary"
																					  Visible="<%#($this->Data->getConsultationAchatPublique()=='1' && $this->Data->getUrlConsultationAchatPublique()=='urlACompleter.com')%>">
																	<span class="sr-only"><%=Prado::localize('DEFINE_ACCEDER_CONSULTATION')%></span>
																	<i class="fa fa-sign-in fa-lg" data-tnr="accederConsultation"></i>
																</com:TActiveHyperLink>
															</prop:falseTemplate>
														</com:TConditional>
													</li>

													<li class="item-action" style="display:<%=(!isset($_GET['panierEntreprise']) )? '':'none'%>" data-toggle="tooltip" title="<%=Prado::localize('TESTER_CONFIGURATION_MON_POSTE')%>">
														<com:TConditional condition="$this->TemplateControl->getListeCons()">
															<prop:trueTemplate>
																<com:TActiveHyperLink cssClass="btn btn-primary btn-sm"
																					  Target="_blank"
																					  NavigateUrl="<%= Application\Service\Atexo\Atexo_Config::getParameter('URL_LT_MPE_SF')%>/entreprise/footer/diagnostic-poste"
																					  Display="<%#(($this->Data->getConsultationAchatPublique()=='0' && $this->Data->getConsultationExterne()=='0' && !$this->TemplateControl->getListeCons())? 'Dynamic' : 'None')%>">
																	<span class="sr-only"><%=Prado::localize('TESTER_CONFIGURATION_MON_POSTE')%></span>
																	<i class="fa fa-cog fa-lg"></i>
																</com:TActiveHyperLink>
															</prop:trueTemplate>
															<prop:falseTemplate>
																<com:TActiveHyperLink cssClass="btn btn-primary btn-sm"
																					  NavigateUrl="<%= Application\Service\Atexo\Atexo_Config::getParameter('URL_LT_MPE_SF')%>/entreprise/footer/diagnostic-poste"
																					  Display="<%#(($this->Data->getConsultationAchatPublique()=='0' && $this->Data->getConsultationExterne()=='0' && !$this->TemplateControl->getListeCons())? 'Dynamic' : 'None')%>">
																	<span class="sr-only"><%=Prado::localize('TESTER_CONFIGURATION_MON_POSTE')%></span>
																	<i class="fa fa-cog fa-lg"></i>
																</com:TActiveHyperLink>
															</prop:falseTemplate>
														</com:TConditional>
													</li>

													<li class="item-action" data-toggle="tooltip" title="<%=Prado::localize('DEFINE_PARTAGER')%>">
														<com:TActivePanel Visible="<%= (Application\Service\Atexo\Atexo_Module::isEnabled('PartagerConsultation') && $this->TemplateControl->calledFromEntreprise() && !$this->TemplateControl->searchAnnonce())%>" >
															<a href="javascript:void(0);" class="btn btn-sm btn-primary">
																<span class="sr-only"><%=Prado::localize('DEFINE_PARTAGER')%></span>
																<i class="fa fa-share-alt"></i>
															</a>
															<div class="share-panel">
																<div class="top"></div>
																<div class="content">
																	<com:TActiveLinkButton
																			Visible ="<%# Application\Service\Atexo\Atexo_Config::getParameter('ACCESS_TOKEN_VIADEO') != '' %>"
																			Attributes.onClick="clickViadeo('<%#$this->Data->getReferenceUtilisateur()%>','<%#addslashes($this->TemplateControl->truncate(Prado::localize('DEFINE_CHERCHER_PARTENAIRE_POUR_APPEL_D_OFFRE') . ' ' . $this->TemplateControl->getIntituleConsultation('false', $this->Data) . ' / ' . $this->TemplateControl->getDenominationOrganisme('false',$this->Data,true), Application\Service\Atexo\Atexo_Config::getParameter('CARACTERES_MAX_ENVOI_VIADEO_SANS_URL')))%>','<%= Application\Service\Atexo\Atexo_Config::getParameter('PF_URL')%>index.php?page=Entreprise.EntrepriseAdvancedSearch&AllCons&id=<%#$this->Data->getId()%>&orgAcronyme=<%#$this->Data->getOrganisme()%>');return false;"
																			cssClass="partage-viadeo"
																			Attributes.href="javascript:void();"
																			Attributes.title="<%=Prado::localize('TEXT_PARTAGER_CETTE_ANNONCE_SUR_VIADEO')%>"></com:TActiveLinkButton>

																	<com:TActiveLinkButton
																			onCommand="Page.resultSearch.sendCourrielPartage"
																			CommandParameter="<%#$this->Data%>"
																			CssClass="partage-mail"
																			Attributes.title="<%=Prado::localize('TEXT_PARTAGER_PAR_COURRIEL')%>" >
																		<prop:ClientSide.OnLoading>showLoader();</prop:ClientSide.OnLoading>
																		<prop:ClientSide.OnFailure>showLoader();</prop:ClientSide.OnFailure>
																		<prop:ClientSide.OnSuccess>showLoader();</prop:ClientSide.OnSuccess>
																	</com:TActiveLinkButton>
																</div>
															</div>
														</com:TActivePanel>
													</li>
													<!-- BEGIN AJOUTER AU PANIER-->
													<li class="item-action" style="display:<%=(!isset($_GET['panierEntreprise']) )? '':'none'%>" data-toggle="tooltip" title="<%=Prado::localize('DEFINE_AJOUTER_PANIER')%>">
														<com:TActiveHyperLink cssClass="btn btn-primary btn-sm"
																			  NavigateUrl="<%#$this->Page->resultSearch->getUrlAjoutPanier($this->Data->getId(), $this->Data->getOrganisme())%>"
																			  Visible="<%#(Application\Service\Atexo\Atexo_Module::isEnabled('PanierEntreprise') && !isset($_GET['panierEntreprise']) && !$this->Page->resultSearch->searchAnnonce() && !$this->Data->isInPanier($this->TemplateControl->getIdEntreprise(),$this->TemplateControl->getIdInscrit()) && !$this->TemplateControl->getListeCons())%>"
														>
															<span class="sr-only"><%=Prado::localize('DEFINE_AJOUTER_PANIER')%></span>
															<i class="fa fa-shopping-cart"></i>
														</com:TActiveHyperLink>
													</li>
													<li class="item-action" style="display:<%=(!isset($_GET['panierEntreprise']) )? '':'none'%>; cursor: not-allowed;" data-toggle="tooltip" title="<%=Prado::localize('TEXT_CONSULTATION_DEJA_DANS_PANIER')%>">
														<com:TActiveHyperLink cssClass="btn btn-primary btn-sm disabled"
																			  NavigateUrl="#"
																			  Visible="<%#(Application\Service\Atexo\Atexo_Module::isEnabled('PanierEntreprise')  && !$this->Page->resultSearch->searchAnnonce() && $this->Data->isInPanier($this->TemplateControl->getIdEntreprise(),$this->TemplateControl->getIdInscrit()) && !$this->TemplateControl->getListeCons())%>">
															<span class="sr-only"><%=Prado::localize('TEXT_CONSULTATION_DEJA_DANS_PANIER')%></span>
															<i class="fa fa-shopping-cart"></i>
														</com:TActiveHyperLink>
													</li>
													<!-- END AJOUTER AU PANIER-->
												</ul>
											</com:TActivePanel>
										</li>
										<li>
											<com:TPanel Visible="<%# $this->TemplateControl->getBaseDce() %>">
												<com:TPanel Visible="true">
													<com:THyperLink ID="linkViewDCE"
																	NavigateUrl="javascript:popUp('index.php?page=<%= Application\Service\Atexo\Atexo_Util::getTypeUserCalledForPradoPages() %>.popUpDetailsDce&id=<%#$this->Data->getId()%>&orgAcronyme=<%#$this->Data->getOrganisme()%>','yes');">
														<com:TImage ImageUrl="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-details.gif" Attributes.alt="<%=Prado::localize('TEXT_VOIR_DETAIL')%>"
																	Attributes.title="<%=Prado::localize('TEXT_VOIR_DETAIL')%>"/>
													</com:THyperLink>
												</com:TPanel>
												<com:TPanel Visible="true">
													<com:THyperLink ID="LinkdownloadDCE"
																	NavigateUrl="index.php?page=frame.DownloadDceForBaseDce&id=<%#$this->Data->getId()%>&orgAcronym=<%#$this->Data->getOrganisme()%>">
														<com:TImage ImageUrl="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-dl-dossier.gif" Attributes.alt="<%=Prado::localize('TELECHARGER_DCE')%>"
																	Attributes.title="<%=Prado::localize('TELECHARGER_DCE')%>"/>
													</com:THyperLink>
												</com:TPanel>
											</com:TPanel>
										</li>
									</ul>
								</div>
							</div>
						</prop:ItemTemplate>
					</com:TRepeater>
				</div>
				<!--END TABLE CONSULTAION-->

				<!-- BEGIN PAGINATION BOTTOM-->
				<div class="clearfix m-t-2 <%=( ($this->hideShowPagination())? 'show' : 'hide' )%>">
					<div class="form-inline pagination form-group-sm pull-right">
						<div class="form-group pagination-section-resultperpage">
							<label for="ctl0_CONTENU_PAGE_resultSearch_listePageSizeBottom" class="m-r-1">
								<com:TTranslate>DEFINE_TEXT_AFFICHER</com:TTranslate>
							</label>
							<com:TDropDownList ID="listePageSizeBottom" CssClass="form-control" AutoPostBack="true" onSelectedIndexChanged="changePagerLenght">
								<com:TListItem Text="10" Selected="10"/>
								<com:TListItem Text="20" Value="20"/>
							</com:TDropDownList>
						</div>
						<div class="form-group pagination-section-currentpage">
							<label class="sr-only">
								<com:TTranslate>DEFINE_ALLER_PAGE</com:TTranslate>
							</label>
							<com:TPanel ID="panelBouttonGotoPageBottom" DefaultButton="DefaultButtonBottom" CssClass="float-left">
								<label for="ctl0_CONTENU_PAGE_resultSearch_numPageBottom" class="m-l-1 m-r-1">
									<com:TTranslate>DEFINE_RESULTATS_PAGE</com:TTranslate>
								</label>
								<com:TTextBox ID="numPageBottom" CssClass="form-control w-50 text-center" Text="1"/>
								<label class="nb-total ">/
									<com:TLabel ID="nombrePageBottom"/>
								</label>
								<com:TButton ID="DefaultButtonBottom" OnClick="goToPage" Attributes.style="display: none"/>
							</com:TPanel>
						</div>
						<div class="form-group liens pagination-section-arrows">
							<label class="m-l-1">
								<com:TPager ID="PagerBottom"
											ControlToPaginate="tableauResultSearch"
											FirstPageText="<span class='btn' data-toggle='tooltip' data-placement='top' title='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>'><i class='fa fa-angle-double-left'></i></span>"
											LastPageText="<span class='btn' data-toggle='tooltip' data-placement='top' title='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>'><i class='fa fa-angle-double-right'></i></span>"
											Mode="NextPrev"
											NextPageText="<span class='btn' data-toggle='tooltip' data-placement='top' title='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>'><i class='fa fa-angle-right'></i></span>"
											PrevPageText="<span class='btn' data-toggle='tooltip' data-placement='top' title='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>'><i class='fa fa-angle-left '></i></span>"
											OnPageIndexChanged="pageChanged"/>
							</label>
						</div>
					</div>
				</div>
				<!-- END PAGINATION BOTTOM-->
			</div>
			<!--END LAYER 1-->
		</div>
		<!--END BLOC FORMULAIRE AVEC ONGLETS-->
	</com:TPanel>
	<!--END ELEMENT FOUND-->

	<!--Debut bloc actions groupées-->
	<com:TPanel cssClass="form-field actions-groupees" Visible="<%=( (!Application\Service\Atexo\Atexo_Module::isEnabled('panierEntreprise'))
    																							  ||(isset($_GET['panierEntreprise']))
    																							  ||(isset($_GET['idAlerte']))
    																							  ||($this->Page->resultSearch->searchAnnonce())
    																							  ||(!Application\Service\Atexo\Atexo_Module::isEnabled('EntrepriseActionsGroupees'))
    																							  ||(!$this->TemplateControl->isConnected())
    																							  ||($this->Page->resultSearch->BaseDce)
    																							  ||($this->Page->resultSearch->ListeCons)
    																							   ? false : true)%>">
		<div class="content">
			<h3>
				<com:TTranslate>TEXT_ACTIONS_GROUPEES</com:TTranslate>
			</h3>
			<div class="actions">
				<com:TLinkButton
						onClick="validerChoix"
						attributes.alt="<%=Prado::localize('TEXT_AJOUTER_TOUS_ELEMENTS_AU_PANIER')%>"
						attributes.title="<%=Prado::localize('TEXT_AJOUTER_TOUS_ELEMENTS_AU_PANIER')%>">
					<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-ajout-cart.gif" title="<%=Prado::localize('TEXT_AJOUTER_TOUS_ELEMENTS_AU_PANIER')%>"
						 alt="<%=Prado::localize('TEXT_AJOUTER_TOUS_ELEMENTS_AU_PANIER')%>"/>
				</com:TLinkButton>
			</div>
		</div>
	</com:TPanel>
	<!--Debut bloc actions group?es-->

	<!--BEGIN BTN NOUVEL RECHERCHER / MODIFIER MA RECHERCHE BOTTOM-->

	<com:TPanel ID="BtnBottom" cssClass="hide">
		<com:TPanel cssClass="link-line m-t-2" visible="<%=(isset($_GET['idAlerte']) || $this->getListeCons()) ? false:true%>">
			<ul class="list-unstyled clearfix">
				<li class="pull-left">
					<div>
						<com:TLinkButton visible="<%=(!$this->Page->AdvancedSearch->conditionModifierMaRecherche())? true:false%>"
										 cssClass="btn btn-sm btn-default"
										 Text="<%=Prado::localize('TEXT_MODIFIER_MA_RECHERCHE')%>"
										 onClick="Page.AdvancedSearch.displayCriteria"/>
					</div>
					<div>
						<com:TLinkButton visible="<%=($this->Page->AdvancedSearch->conditionModifierMaRecherche())? true:false%>"
										 cssClass="btn btn-sm btn-default"
										 Text="<%=Prado::localize('TEXT_MODIFIER_MA_RECHERCHE')%>"
										 onClick="Page.AdvancedSearch.lienRechercherAvanceePanier"/>
					</div>
				</li>
				<li class="pull-right">
					<div>
						<com:TLinkButton cssClass="btn btn-sm btn-default"
										 Text="<%=Prado::localize('DEFINE_NOUVELLE_RECHERCHE')%>"
										 onClick="Page.AdvancedSearch.displayAdvanceSearch"/>
					</div>
				</li>
			</ul>
		</com:TPanel>
	</com:TPanel>
	<!--END BTN NOUVEL RECHERCHER / MODIFIER MA RECHERCHE BOTTOM-->

	<!--BEGIN BTN RETOUR-->
	<com:TPanel cssClass="link-line" visible="<%=(isset($_GET['idAlerte'])) ? true:false%>">
		<a class="btn btn-sm btn-default" href="index.php?page=Entreprise.EntrepriseGestionAlertes">
			<com:TTranslate>TEXT_RETOUR</com:TTranslate>
		</a>
	</com:TPanel>
	<!--END BTN RETOUR-->
</div>


<com:TActiveLabel id="labelJavascript"/>
<div id="panelViadeo" class="panel" title="<%=Prado::localize('TEXT_PARTAGER_CETTE_ANNONCE_SUR_VIADEO')%>" style="display: none;">
	<div class="content" id="message">
		<p class="ref">
			<span id="titreViadeo"></span>
		</p>
		<com:TTextBox Id="messageViadeo"
					  Attributes.Title="<%=Prado::localize('DEFINE_MESSAGE_AVEC_CARACTERES_MAX')%>"
					  CssClass="message max-length"
					  TextMode="MultiLine"
					  Attributes.maxlength="<%= Application\Service\Atexo\Atexo_Config::getParameter('CARACTERES_MAX_ENVOI_VIADEO_SANS_URL')%>"/>
		<div class="line">
			<div class="intitule"><label>
					<com:TTranslate>TEXT_URL</com:TTranslate>
				</label></div>
			<com:TTextBox Id="urlMPE"
						  CssClass="url"
						  ReadOnly="true"
						  Attributes.Title="URL"
			/>
			<com:TTextBox Id="urlViadeo"
						  CssClass="url"
						  ReadOnly="true"
						  Attributes.Title="<%=Prado::localize('DEFINE_MESSAGE')%>"
						  Style="display: none"
			/>
		</div>
		<div class="boutons-line">
			<com:TButton Id="envoyerViadeo"
						 Text="<%=Prado::localize('TEXT_PARTAGER_SUR_VIADEO')%>"
						 Attributes.OnClick="partagerCons();return false;"
						 CssClass="btn primary">
			</com:TButton>
			<com:TActivePanel id="pageLoader" style="display: none">
				<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/ajax-loader-small.gif" title="<%=Prado::localize('DEFINE_VEUILLEZ_PATIENTER')%>" alt="<%=Prado::localize('DEFINE_VEUILLEZ_PATIENTER')%>"/>
			</com:TActivePanel>
		</div>
		<div class="breaker"></div>
	</div>
	<div class="content" id="confirmationViadeo" style="display: none">
		<com:TTranslate>TEXT_MESSAGE_CONFIRMATION_DE_PARTAGE_SUR_VIADEO</com:TTranslate>
	</div>
	<div class="content" id="erreurViadeo" style="display: none">
		<com:TTranslate>TEXT_MESSAGE_ERREUR_DE_PARTAGE_SUR_VIADEO</com:TTranslate>
	</div>
</div>
<com:TLabel ID="scriptJS"/>
<com:TActiveLabel ID="scriptJSActive"/>
<!--Fin colonne droite-->