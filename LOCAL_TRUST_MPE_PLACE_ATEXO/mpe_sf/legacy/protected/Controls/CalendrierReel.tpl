<com:TPanel Visible ="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('CalendrierDeLaConsultation'))? 'true' : 'false' %>">
	<div class="form-bloc recap-infos-consultation">
		<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
		<div class="content">
			<div class="title-toggle" onclick="togglePanel(this,'panelCalendrier');afficheCalendrier();">&nbsp;</div>
			<div class="recap-bloc">
				<h2><%=Prado::localize('DEFINE_TEXT_CALENDRIER_REEL')%></h2>
				<div class="breaker"></div>
				<div class="panel-toggle" style="display: none;" id="panelCalendrier">
					<com:AtexoCalendrier id="calendrier" calendrierReel="true"/>
				</div>
			</div>
			<div class="breaker"></div>
		</div>
		<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
	</div>
	<com:THiddenField id="refConsultation"/>
	<com:TActiveLabel id="TextJs" />
</com:TPanel>