<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_CommonReferentiel;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Db;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;

/**
 * Classe : tableau de recherches des sociétés exclues.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2010
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class TableauRechercheSocietesExclues extends MpeTPage
{
    public $critere;
    public $_callFromEntreprise;

    public function onLoad($param)
    {
        self::customizeForm();
    }

    public function setCritere($critere)
    {
        $this->critere = $critere;
    }

    public function getCritere()
    {
        return $this->critere;
    }

    public function setCallFromEntreprise($callFromEntreprise)
    {
        $this->_callFromEntreprise = $callFromEntreprise;
    }

    public function getCallFromEntreprise()
    {
        return $this->_callFromEntreprise;
    }

    public function lancerRecherche($sender, $param)
    {
        $this->critere = self::getCritereRecherche();
        $this->Page->lancerRechercheWithCritere($this->critere);
    }

    public function displayOrganismes()
    {
        $organismes = Atexo_Organismes::retrieveOrganismes(true, null, false);
        $tableauOrg = [];
        $tableauOrg['0'] = Prado::localize('TEXT__TOUS_MINISTERES');
        if ($organismes) {
            foreach ($organismes as $uneOrg) {
                $tableauOrg[$uneOrg->getAcronyme()] = $uneOrg->getDenominationOrg();
            }
        }
        $this->entitePublique->DataSource = $tableauOrg;
        $this->entitePublique->DataBind();
    }

    public function customizeForm()
    {
        self::displayOrganismes();
    }

    /*
     * retour un tableau de critère de la recherche
     * @return: array
     */
    public function getCritereRecherche()
    {
        $critere = [];

        //Libellé fournisseur
        if ($this->libelleFournisseurFr->Text) {
            $critere['libelleFournisseurFr'] = " AND raison_sociale like '%".(new Atexo_Db())->quote($this->libelleFournisseurFr->Text)."%' ";
        }

        //Durée exclusion
        if ($this->exclusion_indifferent->checked) {
            if ($this->exclusion_indifferent_dateStart->Text) {
                $critere['date_debut_exclusion'] = " AND date_debut_exclusion >= '".Atexo_Util::frnDate2iso((new Atexo_Db())->quote($this->exclusion_indifferent_dateStart->Text))."' ";
            }
            if ($this->exclusion_indifferent_dateEnd->Text) {
                $critere['date_fin_exclusion'] = " AND date_fin_exclusion <= '".Atexo_Util::frnDate2iso((new Atexo_Db())->quote($this->exclusion_indifferent_dateEnd->Text))."' ";
            }
        } elseif ($this->exclusion_definitive->checked) {
            $critere['type_exclusion'] = " AND type_exclusion = '".Atexo_Config::getParameter('EXCLUSION_DEFINITIF')."' ";

            if ($this->exclusion_definitive_dateStart->Text) {
                $critere['date_debut_exclusion'] = " AND date_debut_exclusion >= '".Atexo_Util::frnDate2iso((new Atexo_Db())->quote($this->exclusion_definitive_dateStart->Text))."' ";
            }
        } elseif ($this->exclusion_temporaire->checked) {
            $critere['type_exclusion'] = " AND type_exclusion = '".Atexo_Config::getParameter('EXCLUSION_TEMPORAIRE')."' ";

            if ($this->exclusion_temporaire_dateStart->Text) {
                $critere['date_debut_exclusion'] = " AND date_debut_exclusion >= '".Atexo_Util::frnDate2iso((new Atexo_Db())->quote($this->exclusion_temporaire_dateStart->Text))."' ";
            }
            if ($this->exclusion_temporaire_dateEnd->Text) {
                $critere['date_fin_exclusion'] = " AND date_fin_exclusion <= '".Atexo_Util::frnDate2iso((new Atexo_Db())->quote($this->exclusion_temporaire_dateEnd->Text))."' ";
            }
        }
        //Portée exclusion
        if ($this->portee_indifferent->checked) {
            $critere['type_portee'] = ' AND type_portee IS NOT NULL ';
        } elseif ($this->portee_partielle->checked) {
            $critere['type_portee'] = " AND type_portee = '".Atexo_Config::getParameter('PORTEE_PARTIELLE')."' ";

            if ($this->entitePublique->getSelectedValue()) {
                $critere['entitePublique'] = " AND organisme_acronyme = '".$this->entitePublique->getSelectedValue()."' ";
            }
        } elseif ($this->portee_totale->checked) {
            $critere['type_portee'] = " AND type_portee = '".Atexo_Config::getParameter('PORTEE_TOTALE')."' ";
        }

        return $critere;
    }

    public function getUrlTableauRecherche()
    {
        if ((new TableauRechercheSocietesExclues())->getCallFromEntreprise()) {
            $this->response->redirect('?page=Entreprise.EntrepriseRechercherSocietesExclues&search=1');
        } else {
            $this->response->redirect('?page=Agent.AgentRechercherSocietesExclues&search=1');
        }
    }
}
