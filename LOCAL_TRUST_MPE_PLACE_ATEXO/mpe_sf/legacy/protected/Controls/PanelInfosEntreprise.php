<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Entreprise;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_EntrepriseVo;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_EtablissementVo;
use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2;
use Prado\Prado;

/**
 * Classe de gestion des informations de l'entreprise.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 *
 * @version 1.0
 *
 * @since 4.9.0
 *
 * @copyright Atexo 2015
 */
class PanelInfosEntreprise extends MpeTTemplateControl
{
    private $entreprise = null;
    private bool $isEntrepriselocale = false;
    private ?string $identifiantUniqueEtp = null;
    private ?\Application\Service\Atexo\Entreprise\Atexo_Entreprise_EtablissementVo $etablissementSiege = null;
    private bool $lectureSeule = false;
    private ?string $paysEtablissement = null;

    /**
     * Modifie la valeur de l'attribut [lectureSeule].
     *
     * @param bool $value : nouvelle valeur de l'attribut [lectureSeule]
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function setLectureSeule($value)
    {
        $this->lectureSeule = $value;
    }

    /**
     * Recupere la valeur de l'attribut [lectureSeule].
     *
     * @return bool : retourne true ou false
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getLectureSeule()
    {
        return $this->lectureSeule;
    }

    /**
     * Modifie la valeur de l'attribut [entreprise].
     *
     * @param Atexo_Entreprise_EntrepriseVo $value : nouvelle valeur de l'attribut [entreprise]
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function setEntreprise($value)
    {
        $this->entreprise = $value;
    }

    /**
     * Recupere la valeur de l'attribut [entreprise].
     *
     * @return Atexo_Entreprise_EntrepriseVo
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getEntreprise()
    {
        return $this->Page->getEntreprise();
    }

    /**
     * Modifie la valeur de l'attribut [isEntrepriselocale].
     *
     * @param bool $value : nouvelle valeur de l'attribut [isEntrepriselocale]
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function setIsEntrepriselocale($value)
    {
        $this->Page->setViewState('isEntrepriselocale', $value);
    }

    /**
     * Recupere la valeur de l'attribut [isEntrepriselocale].
     *
     * @return bool : retourne true ou false
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getIsEntrepriselocale()
    {
        return $this->Page->getViewState('isEntrepriselocale');
    }

    /**
     * Recupere la valeur de l'attribut [identifiantUniqueEtp].
     *
     * @return string
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getIdentifiantUnique()
    {
        return $this->identifiantUniqueEtp;
    }

    /**
     * Modifie la valeur de l'attribut [identifiantUniqueEtp].
     *
     * @param string $value : nouvelle valeur de l'attribut [identifiantUniqueEtp]
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function setIdentifiantUnique($value)
    {
        $this->identifiantUniqueEtp = $value;
    }

    /**
     * Recupere la valeur de l'attribut [etablissementSiege].
     *
     * @return Atexo_Entreprise_EtablissementVo
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getEtablissementSiege()
    {
        return $this->etablissementSiege;
    }

    /**
     * Recupere la valeur de l'attribut [paysEtablissement].
     *
     * @return string
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getPaysEtablissement()
    {
        $pays = $this->paysEtablissement;
        if (empty($pays)) {
            $pays = $this->Page->getViewState('paysEtablissement');
        }

        return $pays;
    }

    /**
     * Modifie la valeur de l'attribut [paysEtablissement].
     *
     * @param string $value : nouvelle valeur de l'attribut [paysEtablissement]
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function setPaysEtablissement($value)
    {
        $this->paysEtablissement = $value;
    }

    /**
     * Modifie la valeur de l'attribut [etablissementSiege].
     *
     * @param Atexo_Entreprise_EtablissementVo $value : nouvelle valeur de l'attribut [etablissementSiege]
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function setEtablissementSiege(Atexo_Entreprise_EtablissementVo $value)
    {
        $this->etablissementSiege = $value;
    }

    /**
     * Chargement de la page.
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function onLoad($param)
    {
        $this->customizeForm();
    }

    /**
     * Gerer l'affichage de la page.
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function customizeForm()
    {
        if ($this->getEntreprise() instanceof Atexo_Entreprise_EntrepriseVo || $this->getLectureSeule()) {
            $this->blocInfosEntrepriseChargementAuto->visible = true;
            $this->blocSaisieManuelInfosEntreprise->visible = false;
        } else {
            $this->blocInfosEntrepriseChargementAuto->visible = false;
            $this->blocSaisieManuelInfosEntreprise->visible = true;
        }

    $this->panelCodeApe->visible = Atexo_Module::isEnabled('CompteEntrepriseCodeApe');

        $this->gererAffichageBlocIdentifiantUnique();
    }

    /**
     * Gerer l'affichage du bloc identifiant unique
     * Identifiant unique => siren/siret pour les entreprises francaises
     * Identifiant unique => identifiant unique pour les entreprises etrangeres
     * Active et desactive les validateurs de ces champs concernes.
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function gererAffichageBlocIdentifiantUnique()
    {
        if ($this->getEntreprise() instanceof Atexo_Entreprise_EntrepriseVo || $this->getLectureSeule()) {
            if (true === $this->getIsEntrepriselocale()) {
                $this->panelSirenModeNormal->visible = true;
                $this->panelIdUniqueModeNormal->visible = false;
            } else {
                $this->panelSirenModeNormal->visible = false;
                $this->panelIdUniqueModeNormal->visible = true;
            }
        } else {
            if (true === $this->getIsEntrepriselocale()) {
                $this->blocSirenModeSaisieManuelle->visible = true;
                $this->blocIdentifiantUniqueModeSaisieManuelle->visible = false;
                $this->validateurSirenModeSaisieManuelle->enabled = true;
                $this->validateurIdentifiantUniqueModeSaisieManuelle->enabled = false;
            } else {
                $this->blocSirenModeSaisieManuelle->visible = false;
                $this->blocIdentifiantUniqueModeSaisieManuelle->visible = true;
                $this->validateurSirenModeSaisieManuelle->enabled = false;
                $this->validateurIdentifiantUniqueModeSaisieManuelle->enabled = true;
            }
        }
    }

    /**
     * Chargement des formes juridiques.
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function chargerComboFormeJuridique()
    {
        $formeJuredique = (new Atexo_Entreprise())->retierveFormeJuredique();
        $data = [];
        $data['0'] = Prado::localize('TEXT_SELECTIONNER');
        if ($formeJuredique) {
            $langue = Atexo_CurrentUser::readFromSession('lang');
            $getLibelleFormejuridique = 'getLibelleFormejuridique'.ucwords($langue);
            foreach ($formeJuredique as $formJurid) {
                if ($formJurid->$getLibelleFormejuridique()) {
                    $data[$formJurid->getFormejuridique()] = $formJurid->$getLibelleFormejuridique();
                } elseif (!$formJurid->getLibelleFormejuridique() && !$formJurid->$getLibelleFormejuridique()) {
                    $data[$formJurid->getFormejuridique()] = $formJurid->getFormejuridique();
                } elseif (0 == strcmp($langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) && $formJurid->getLibelleFormejuridique()) {
                    $data[$formJurid->getFormejuridique()] = $formJurid->getLibelleFormejuridique();
                }
            }
        }
        $this->formeJuridique->DataSource = $data;
        $this->formeJuridique->dataBind();
    }

    /**
     * Charge le texte lieu établissement.
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function setLieuEtablissement($value)
    {
        $this->lieuEtablissement->Text = Atexo_Util::encodeToHttp($value);
    }

    /**
     * Pré-remplissage du bloc "MON COMPTE ENTREPRISE" => "Entreprise".
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function chargerInformations()
    {
        $this->chargerComboFormeJuridique();
        $this->chargerCategorieEntreprisePME();
        $this->setLieuEtablissement($this->getPaysEtablissement());
        $this->chargerInfosEntreprise();
    }

    /**
     * Charge les informations de l'entreprise sur la page.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function chargerInfosEntreprise()
    {
        if ($this->getEntreprise() instanceof Atexo_Entreprise_EntrepriseVo) {
            if ($this->getIsEntrepriselocale()) {
                $this->siren_chargement_auto->Text = $this->getEntreprise()->getSiren();
            } else {
                $this->IdUnique_chargement_auto->Text = $this->getEntreprise()->getSirenetranger();
            }
            $this->texteRaisonSociale->Text = $this->getEntreprise()->getNom();
            $this->texteFormeJuridique->Text = $this->getEntreprise()->getFormejuridique();
            $codeApe = $this->getEntreprise()->getCodeape();
            $libelleApe = $this->getEntreprise()->getLibelleApe();
            $pays = $this->getEntreprise()->getPaysEnregistrement();
            if (!$codeApe && $this->getEtablissementSiege() instanceof Atexo_Entreprise_EtablissementVo) {
                $codeApe = $this->getEtablissementSiege()->getCodeape();
                $libelleApe = $this->getEtablissementSiege()->getLibelleApe();
            }
            if (!$pays && $this->getEtablissementSiege() instanceof Atexo_Entreprise_EtablissementVo) {
                $pays = $this->getEtablissementSiege()->getPays();
            }
            $this->code_APE_NACE->Text = $codeApe;
            $libelleApe = (!empty($libelleApe)) ? ' - '.$libelleApe : $libelleApe;
            $this->libelleApeNafNace->Text = $libelleApe;
            $this->lieuEtablissement_chargement_auto->Text = $pays;
            $this->categorieEntreprise->Text = ($this->getEntreprise()->getCategorieEntreprise() == Atexo_Config::getParameter('CATEGORIE_ENTREPRISE_PME')) ? Prado::Localize('DEFINE_OUI') : Prado::Localize('DEFINE_NON');
        } else {
            if ($this->getIsEntrepriselocale()) {
                $this->siren->Text = $this->getIdentifiantUnique();
            } else {
                $this->identifiantUnique->Text = $this->getIdentifiantUnique();
                $this->lieuEtablissement->Text = $this->getPaysEtablissement();
            }
        }
    }

    /**
     * Instancie un nouvel objet Entreprise et charge les informations du formulaire dans cet objet.
     *
     * @return Atexo_Entreprise_EntrepriseVo
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function chargerObjetEntreprise()
    {
        $newCompany = new Atexo_Entreprise_EntrepriseVo();
        if (true === $this->getIsEntrepriselocale()) {
            $newCompany->setSiren($this->siren->SafeText);
            $newCompany->setPaysenregistrement(Atexo_Util::encodeToHttp(Atexo_Config::getParameter('DENOMINATION2_GEON2_FRANCE')));
            $newCompany->setPaysadresse(Atexo_Config::getParameter('DENOMINATION1_GEON2_FRANCE'));
            $newCompany->setAcronymePays(Atexo_Config::getParameter('DENOMINATION2_GEON2_FRANCE'));
            $newCompany->setSaisieManuelle(true);
        } else {
            $newCompany->setSirenetranger($this->identifiantUnique->SafeText);
            $newCompany->setPaysadresse($this->lieuEtablissement->Text);
            $newCompany->setPaysenregistrement($this->lieuEtablissement->Text);
            $newCompany->setAcronymePays((new Atexo_Geolocalisation_GeolocalisationN2())->retrieveCodeCountryByLibelle((new Atexo_Config())->toPfEncoding($this->lieuEtablissement->Text)));
        }
        if (Atexo_Module::isEnabled('CompteEntrepriseActivationInscriptionParAgent')) {
            $newCompany->setCompteActif('0');
        } else {
            $newCompany->setCompteActif('1');
        }
        $newCompany->setNom($this->raisonSociale->SafeText);
        if (Atexo_Module::isEnabled('CompteEntrepriseCodeApe')) {
            $newCompany->setCodeape($this->apeNafNace1->SafeText.$this->apeNafNace2->SafeText);
            $newCompany->setLibelleApe($this->libelleApeNafNace->Text);
        }
        /*if(Atexo_Module::isEnabled('CompteEntrepriseCodeNaceReferentiel')) {//@TODO
            $newCompany->setCodeape($this->atexoReferentiel->codesRefSec->Value);
        }*/
        if (('0' != $this->formeJuridique->getSelectedValue()) && ($this->formeJuridique->getSelectedValue())) {
            $newCompany->setFormejuridique($this->formeJuridique->SelectedValue);
        } else {
            $newCompany = false;
        }

        if ('1' === $this->categorieEntrepriseListe->getSelectedValue()) {
            $newCompany->setCategorieEntreprise(Atexo_Config::getParameter('CATEGORIE_ENTREPRISE_PME'));
        }

        return $newCompany;
    }

    /**
     * Chargement des categroies entreprise.
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function chargerCategorieEntreprisePME()
    {
        $data = ['0' => Prado::localize('TEXT_SELECTIONNER'), '1' => Prado::Localize('OUI_MAJ'), '2' => Prado::Localize('NON_MAJ')];
        $this->categorieEntrepriseListe->DataSource = $data;
        $this->categorieEntrepriseListe->dataBind();
    }
}
