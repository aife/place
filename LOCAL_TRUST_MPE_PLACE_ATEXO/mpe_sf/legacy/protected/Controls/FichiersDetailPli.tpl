<com:TConditional condition="$this->getSignature()">
	<prop:trueTemplate>
		<%include Application.templates.mpe.AtexoFichiersDetailPlisAvecSignature %>
	</prop:trueTemplate>
	<prop:falseTemplate>
		<%include Application.templates.mpe.AtexoFichiersDetailPlisSansSignature %>
	</prop:falseTemplate>
</com:TConditional>
