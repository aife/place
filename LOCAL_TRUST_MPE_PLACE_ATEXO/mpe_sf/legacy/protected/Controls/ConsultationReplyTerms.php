<?php

namespace Application\Controls;

use Application\Propel\Mpe\CommonConsultation;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Exception;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Prado\Prado;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class ConsultationReplyTerms extends MpeTTemplateControl
{
    private $_consultation = false;
    private $_reference = false;
    private $_withException = true;

    public function getReference()
    {
        return $this->_reference;
    }

    public function setReference($reference)
    {
        return $this->_reference = $reference;
    }

    public function getConsultation()
    {
        return $this->_consultation;
    }

    public function setConsultation($consultation)
    {
        return $this->_consultation = $consultation;
    }

    public function getWithException()
    {
        return $this->_withException;
    }

    public function setWithException($withException)
    {
        return $this->_withException = $withException;
    }

    public function onLoad($param)
    {
        if (!($this->getConsultation() instanceof CommonConsultation) && !$this->getReference()) {
            if ($this->getWithException()) {
                throw new Atexo_Exception('ConsultationReplyTerms Error : Attendu id de la référence ou objet consultation');
            } else {
                $this->summaryVisible->Visible = false;

                return;
            }
        }
        $org = Atexo_CurrentUser::getCurrentOrganism();
        if (!($this->getConsultation() instanceof CommonConsultation)) {
            $reference = $this->_reference;
            $criteria = new Atexo_Consultation_CriteriaVo();
            $criteria->setcalledFromPortail(false);
            $criteria->setIdService(Atexo_CurrentUser::getCurrentServiceId());
            $criteria->setAcronymeOrganisme($org);
            $criteria->setIdReference($reference);
            $criteria->setConnectedAgentId(Atexo_CurrentUser::getId());
            $consultation = (new Atexo_Consultation())->search($criteria);
            if (1 == count($consultation) && $consultation[0] instanceof CommonConsultation) {
                $consultation = array_shift($consultation);
            }
        } else {
            $consultation = $this->getConsultation();
            $this->_reference = $consultation->getId();
        }

        if ('1' == $consultation->getReponseObligatoire()) {
            $this->reponseElectronique->Text = Prado::Localize('DEFINE_OBLIGATOIRE');
        } elseif ('1' == $consultation->getAutoriserReponseElectronique()) {
            $this->reponseElectronique->Text = Prado::Localize('DEFINE_AUTORISEE');
        } else {
            $this->reponseElectronique->Text = Prado::Localize('DEFINE_REFUSEE');
        }

        if ('1' == $consultation->getModeOuvertureReponse()) {
            $this->modeOuvertureReponseElectronique->Text = Prado::Localize('DEFINE_PAR_REPONSE');
        } else {
            $this->modeOuvertureReponseElectronique->Text = Prado::Localize('DEFINE_PAR_DOSSIER');
        }

        if ((1 == $consultation->getSignatureOffre()) && (1 == $consultation->getAutoriserReponseElectronique())) {
            $this->signatureElectroniqueRequise->Text = Prado::Localize('DEFINE_REQUISE');
        } elseif ((2 == $consultation->getSignatureOffre()) && (1 == $consultation->getAutoriserReponseElectronique())) {
            $this->signatureElectroniqueRequise->Text = Prado::Localize('DEFINE_AUTORISEE');
        } else {
            $this->signatureElectroniqueRequise->Text = Prado::Localize('DEFINE_NON_REQUISE');
        }
        $chiffrementOui = (1 == $consultation->getChiffrementOffre()) && (1 == $consultation->getAutoriserReponseElectronique());
        if ($chiffrementOui) {
            $this->chiffremetPlis->Text = Prado::Localize('DEFINE_OUI');
        } else {
            $this->chiffremetPlis->Text = Prado::Localize('DEFINE_NON');
        }

        if (1 == $consultation->getAutoriserReponseElectronique()) {
            $this->constitutionReponse->visible = true;
            $nbrLots = is_countable($consultation->getAllLots()) ? count($consultation->getAllLots()) : 0;
            if ('1' == $consultation->getEnvCandidature()) {
                $this->enveloppe1->Text = Prado::localize('DEFINE_ENVELOPPE_CONDIDATURE');
                $this->detail1->Visible = $chiffrementOui;
            }
            if ('1' == $consultation->getEnvOffre()) {
                $this->enveloppe2->Text = Prado::localize('DEFINE_ENVELOPPE_OFFRE').' - '.Prado::localize('DEFINE_ENVELOPPE_UNIQUE');
                $this->detail2->Visible = $chiffrementOui;
            }
            if ($consultation->getEnvOffre() >= 2) {
                $this->enveloppe2->Text = Prado::localize('DEFINE_ENVELOPPE_OFFRE').' - '.Prado::localize('DEFINE_ENVELOPPE_PAR_LOT').' '.Prado::localize('DEFINE_NOMBRE_LOT').' : '.$nbrLots;
                $this->detail2->Visible = $chiffrementOui;
            }
            if ('1' == $consultation->getEnvOffreTechnique()) {
                $this->enveloppe4->Text = Prado::localize('DEFINE_ENVELOPPE_OFFRE_TECHNIQUE').' - '.Prado::localize('DEFINE_ENVELOPPE_UNIQUE');
                $this->detail4->Visible = $chiffrementOui;
            }
            if ($consultation->getEnvOffreTechnique() >= 2) {
                $this->enveloppe4->Text = Prado::localize('DEFINE_ENVELOPPE_OFFRE_TECHNIQUE').' - '.Prado::localize('DEFINE_ENVELOPPE_PAR_LOT').' '.Prado::localize('DEFINE_NOMBRE_LOT').' : '.$nbrLots;
                $this->detail4->Visible = $chiffrementOui;
            }
            if ('1' == $consultation->getEnvAnonymat()) {
                $this->enveloppe3->Text = Prado::localize('DEFINE_TROISIEME_ENVELOPPE');
                $this->detail3->Visible = $chiffrementOui;
            }
        }
    }
}
