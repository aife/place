<com:TActivePanel Id="calendrierPrevisionel" visible="false" CssClass="form-field">
<iframe src="javascript:''" id="__gwt_historyFrame" style="width:0;height:0;border:0"></iframe>
<!-- Les messages d'erreur sont affichés ici -->
  <div class="error" id="erreurs"></div>
  <!--Debut Bloc Calendrier initial-->
   <div class="top"></div>
   <span class="left">&nbsp;</span><span class="right">&nbsp;</span>
   <div class="content">
	   <h2><%=Prado::localize('DEFINE_CALENDRIER_INITIAL')%></h2>
	   <div class="bloc-calendrier calendrier-previsionnel">
		<div class="boutons-line">
			<div class="recalcul-planning-modifie" id="recalculer"></div>				
			<div class="file-link">
				 <com:THyperLink  id="urlPdf" >
				 	<com:TImage ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-pdf.gif" Attributes.alt="Export PDF" Attributes.title="Export PDF" />
				 </com:THyperLink>
			 </div>
		</div>
		<a name="planningTop" id="planningTop"></a>
				<div class="liste-taches">
				<div id="liste-etapes"></div>
				<div id="lien-ajouter-ligne"></div>
				<div class="spacer"></div>
		</div>
		</div>
 </div>
 <div class="bottom"></div>
 <div class="breaker"></div>

 </com:TActivePanel>
<!--Fin Bloc Calendrier initial-->

<!-- Calendrier Reel  -->  
 
<com:TPanel visible="false" ID="calendrierReel">
	<iframe src="javascript:''" id="__gwt_historyFrame"	style="width: 0; height: 0; border: 0"></iframe>
	<!-- Les messages d'erreur sont affichés ici -->
	<div class="bloc-calendrier calendrier-reel">
		<div class="error" id="erreurs"></div> <!-- revoir avec regis si necessire -->
		<div class="boutons-line">
				<input type="button" name="recalculerCalendrier" id="recalculerCalendrier" value="Recalculer" class="bouton-moyen float-left" />
				<com:TActiveTextBox id="dateRemisePlis" Attributes.title="Date limite de remise des plis" style="display:none"/>
				<div class="file-link"> 
						 <com:THyperLink  id="urlPdf2" >
								<com:TImage ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-pdf.gif" Attributes.alt="Export PDF" Attributes.title="Export PDF" />
						</com:THyperLink>
				</div>
		</div>
		<a name="planningTop" id="planningTop"></a>
	    <div class="liste-taches">
			<div id="liste-etapes"></div>
			<div id="lien-ajouter-ligne"></div>
		</div>
		<!--Debut Bloc boutons-->
		<div class="boutons-line margin-0">
			<com:TLinkButton CssClass="bouton-moyen float-right" id="save" Attributes.onclick="getCalendrierJson();" OnClick="SourceTemplateControl.save">Enregister</com:TLinkButton>
		</div>				
		<!--Fin Bloc boutons-->
	</div>
	<com:THiddenField id="jsonChaine"/>
</com:TPanel>

<!-- Calendrier parametrage type procedure --> 

<com:TActivePanel Id="calendrierParametre" visible="false" CssClass="form-field">
	<a name="planningTop" id="planningTop"></a>
			<div class="liste-taches">
			<div id="liste-etapes"></div>
			<div id="lien-ajouter-ligne"></div>
	</div>
</com:TActivePanel>