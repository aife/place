<com:TActivePanel cssClass="line" ID="panelEchantillonsDemandes" Enabled="<%=($this->getActif())? true:false%>">
	<div class="intitule-150"><label for="echantillon"><com:TTranslate>ECHANTILLONS_DEMANDES</com:TTranslate></label><span class="champ-oblig">*</span> :</div>
	<div class="content-bloc bloc-400">
		<div class="line">
				<com:TActiveRadioButton 
					cssClass="radio" 
					GroupName="echantillon" 
					Checked="true" 
					ID="echantillonNon" 
					Attributes.onclick="hideDiv('<%=$this->echantillonLayer->getClientId()%>');"  
					Text="<%=Prado::localize('TEXT_NON')%>"
					Attributes.title="<%=Prado::localize('TEXT_NON')%>"/>
		</div>
		<div class="line">
			<com:TActiveRadioButton 
				cssClass="radio" 
				GroupName="echantillon" 
				ID="echantillonOui" 
				Attributes.onclick="showDiv('<%=$this->echantillonLayer->getClientId()%>');"  
				Text="<%=Prado::localize('FCSP_OUI')%>"
				Attributes.title="<%=Prado::localize('FCSP_OUI')%>"/>
				<span id="spanErrorEchantillon" style="display:none" title='Champ obligatoire' ><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>
				<com:TCustomValidator
					ClientValidationFunction="validateEchantillonsDemandesCons"
					ID="validatorEchantillion" 
					ControlToValidate="addEchantillion"
					ValidationGroup="<%=($this->getValidationGroup())%>" 
					Display="Dynamic"
					ErrorMessage="<%=Prado::localize('ECHANTILLONS_DEMANDES')%>"
					EnableClientScript="true"  						 					 
		 			Text=" ">   
					<prop:ClientSide.OnValidationError>
		 				document.getElementById('divValidationSummary').style.display='';
		 				if(document.getElementById('ctl0_CONTENU_PAGE_blocBoutons'))
 							document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
					</prop:ClientSide.OnValidationError>
 				</com:TCustomValidator>
 				<com:TCustomValidator
					ClientValidationFunction="validateEchantillonsDateComapareComposant"
					ID="validatorEchantillionCompare" 
					ControlToValidate="dateLimiteEchantillion"
					ValidationGroup="<%=($this->getValidationGroup())%>"
					Display="Dynamic"
					ErrorMessage="<%=Prado::localize('DATE_ECHANTILLON_POSTERIEUR_DATE_FIN')%>"
					EnableClientScript="true"  						 					 
		 			Text=" ">   
					<prop:ClientSide.OnValidationError>
		 				document.getElementById('divValidationSummary').style.display='';
 						if(document.getElementById('ctl0_CONTENU_PAGE_blocBoutons'))
 							document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
					</prop:ClientSide.OnValidationError>
 				</com:TCustomValidator>
 				<com:TCustomValidator
					ClientValidationFunction="dateAnterieurAujourdhui"
					ID="validatorEchantillionCompareDate" 
					ControlToValidate="dateLimiteEchantillion"
					ValidationGroup="<%=($this->getValidationGroup())%>" 
					Display="Dynamic"
					ErrorMessage="<%=Prado::localize('DATE_ECHANTILLON_POSTERIEUR_DATE_AUJOURDHUI')%>"
					EnableClientScript="true" 
					visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('validationFormatChampsStricte'))?'true' : 'false'%>"
		 			Text=" ">   
					<prop:ClientSide.OnValidationError>
		 				document.getElementById('divValidationSummary').style.display='';
		 				if(document.getElementById('ctl0_CONTENU_PAGE_blocBoutons'))
 							document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
					</prop:ClientSide.OnValidationError>
 				</com:TCustomValidator>
		</div>
	</div>
</com:TActivePanel>
<com:TActivePanel ID="echantillonLayer" Style="display:none;" Enabled="<%=($this->getActif())? true:false%>">
	<div class="line">
		<div class="intitule-bloc intitule-140 indent-15"><label for="addEchantillion">- <com:TTranslate>ADRESSE_DE_DEPOT</com:TTranslate></label><span class="champ-oblig">*</span> :</div>
		<com:TActiveTextBox TextMode="MultiLine" ID="addEchantillion"  Attributes.title="<%=Prado::localize('ADRESSE_DE_DEPOT')%>" cssClass="long" />
	</div>
	<div class="line">
		<div class="intitule-140 indent-15"><label for="dateLimite_Echantillion">- <com:TTranslate>DEFINE_DATE_HEURE_LIMITE</com:TTranslate></label><span class="champ-oblig">*</span> :</div>
		<div class="calendar indent-15">
			<com:TActiveTextBox cssClass="heure" ID="dateLimiteEchantillion" Attributes.title="<%=Prado::localize('DEFINE_DATE_HEURE_LIMITE')%>" />
			<com:TImage ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-calendar.gif" Attributes.alt="<%=Prado::localize('DEFINE_CALENDRIER')%>" Attributes.title="<%=Prado::localize('DEFINE_CALENDRIER')%>" Attributes.onclick="popUpCalendar(this,document.getElementById('<%=$this->dateLimiteEchantillion->getClientId()%>'),'dd/mm/yyyy 10:30','');" />
		</div>
		<div class="info-aide-right">( jj/mm/aaaa hh:mm )</div>
	</div>	
</com:TActivePanel>
