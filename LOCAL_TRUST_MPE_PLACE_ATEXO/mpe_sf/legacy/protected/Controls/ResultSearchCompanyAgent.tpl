<com:PanelMessageAvertissement ID="panelMessageAvertTraduction" Visible="false"/>
<com:TPanel id="panelTextChoixDest" visible="<%=($_GET['page'] == 'Agent.ChoixDestinataireBdFournisseur') ? true:false%>" >
	<div class="breadcrumbs">
				<span class="normal"><com:TTranslate>DEFINE_CONSULTATIONS</com:TTranslate> > <com:TTranslate>DEFINE_TEXT_ENVOI_COURRIER_ELECTRONIQUE</com:TTranslate> ><com:TTranslate>DEFINE_TEXT_AJOUT_MODIFICATION_DESTINATAIRE</com:TTranslate> > <com:TTranslate>TEXT_ADRESSES_BD_FOURNISSEURS</com:TTranslate>
				</span>
	</div>
</com:TPanel>
<com:TPanel id="panelTextBDFournisseur" visible="<%=($_GET['page'] == 'Agent.ChoixDestinataireBdFournisseur') ? 'false' : 'true' %>" >
	<com:TPanel Visible="<%=!($_GET['page'] == 'Entreprise.RechercheCollaborateur') ? 'true' : 'false' %>">
		<div class="breadcrumbs"><span class="normal">
					<com:TTranslate>TEXT_BASE_DONNEES_FOURNISSEURS</com:TTranslate></span>
			&gt; <com:TTranslate>RECHERCHER</com:TTranslate>
			&gt; <com:TTranslate>RESULTATS</com:TTranslate>
		</div>
	</com:TPanel>
	<com:TPanel Visible="<%=($this->getCalledFrom() == 'entreprise')?true:false%>">
		<div class="breadcrumbs"><span class="normal">
					<com:TTranslate>TEXT_BOURSE_A_LA_CO_SOUS_TRAITANCE</com:TTranslate></span>
			&gt; <com:TTranslate>TEXT_RECHERCHE_COLLABORATION</com:TTranslate>
			&gt; <com:TTranslate>RESULTATS</com:TTranslate>
		</div>
	</com:TPanel>
</com:TPanel>

<!-- Début Legende -->
<com:TPanel visible="<%=(($this->getCalledFrom() == 'entreprise') && (Application\Service\Atexo\Atexo_Module::isEnabled('BourseALaSousTraitance'))) ? true:false%>" >
	<div class="toggle-panel" id="legende">
		<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
		<div class="content">
			<div class="title-toggle" onclick="togglePanel(this,'legendPanel');"><com:TTranslate>LEGENDE</com:TTranslate></div>
			<div class="panel" style="display:none;" id="legendPanel">
				<div class="column bloc-600 legende-statut-collaborateur">
					<h2><com:TTranslate>TYPE_DE_COLLABORATION_SOUHAITEE</com:TTranslate></h2>
					<div class="statut-collaborateur statut-collaborateur-1sur4" title="<%=Prado::localize('TEXT_MANDATAIRE')%>"></div>
					<div class="intitule-legende">: <com:TTranslate>MANDATAIRE_DE_GROUPEMENT</com:TTranslate></div>
					<div class="statut-collaborateur statut-collaborateur-2sur4" title="<%=Prado::localize('TEXT_CO_TRAITANT_SOLIDAIRE')%>"></div>
					<div class="intitule-legende">: <com:TTranslate>TEXT_CO_TRAITANT_SOLIDAIRE</com:TTranslate></div>
					<div class="statut-collaborateur statut-collaborateur-3sur4" title="<%=Prado::localize('TEXT_CO_TRAITANT_CONJOINT')%>"></div>
					<div class="intitule-legende">: <com:TTranslate>TEXT_CO_TRAITANT_CONJOINT</com:TTranslate></div>
					<div class="statut-collaborateur statut-collaborateur-4sur4" title="<%=Prado::localize('TEXT_SOUS_TRAITANT')%>"></div>
					<div class="intitule-legende">: <com:TTranslate>TEXT_SOUS_TRAITANT</com:TTranslate></div>
				</div>
			</div>
			<div class="breaker"></div>
		</div>
		<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
	</div>
</com:TPanel>
<!-- Fin Legende -->


<div class="link-line">
	<com:TLinkButton cssClass="bouton-retour" Text ="<%=Prado::localize('TEXT_MODIFIER_MA_RECHERCHE')%>" Attributes.title="<%=Prado::localize('TEXT_MODIFIER_MA_RECHERCHE')%>" onClick ="Page.panelSearch.displayCriteria" />
	<com:TLinkButton cssClass="bouton-suivant" Text ="<%=Prado::localize('DEFINE_NOUVELLE_RECHERCHE')%>" Attributes.title="<%=Prado::localize('DEFINE_NOUVELLE_RECHERCHE')%>" onClick ="Page.panelSearch.displayPanelSearch"  />
</div>
<!--Bloc message-->
<com:TPanel id="messageSaisieManuel" Visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('SynchronisationSGMAP'))? true:false %>"  cssClass="form-bloc margin-0">
	<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
	<div class="content">
		<p class="infos-line margin-0"><%=Prado::localize('TEXT_MESSAGE_INFO_SAISIE_MANUEL')%></p>
		<div class="breaker"></div>
	</div>
	<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
</com:TPanel>
<div class="spacer-small"></div>
<!--Fin Bloc message-->
<!--Debut Bloc Liste des acces-->
<!--     -->
<!--    -->
<div class="form-field">
	<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
	<div class="content">

		<com:TPanel ID="panelNoElementFound">
			<h2><center><com:TTranslate>DEFINE_AUCUN_RESULTAT</com:TTranslate>&nbsp;</center></h2>
		</com:TPanel>
		<com:TPanel ID="panelElementsFound">
			<div style="display:<%=(Application\Service\Atexo\Atexo_Module::isEnabled('BourseALaSousTraitance') && ($this->getCalledFrom() == 'entreprise')) ? '':'none'%>" >
				<h3 class="no-indent"><com:TTranslate>CODE_LISTE_ENTREPRISES_GROUPEMENT</com:TTranslate></h3>
				<div class="link-line">
					<span class="info-aide"><com:TTranslate>NB_LEGENDE_COLLABORATION</com:TTranslate></span>
					<div class="breaker"></div>
				</div>
			</div>
			<div style="display:<%=($this->getCalledFrom() == 'agent') ? '':'none'%>" >
				<h3 class="no-indent"><com:TTranslate>TEXT_LISTE_ENTREPRISE</com:TTranslate></h3>
			</div>
			<!--Debut partitionneur-->
			<div class="line-partitioner">
				<h2><com:TTranslate>DEFINE_NOMBRE_RESULTAT</com:TTranslate>&nbsp;<com:TLabel ID="nombreElement"/></h2>
				<div class="partitioner">
					<div class="intitule"><strong><label for="cons_nbResultsTop"><com:TTranslate>DEFINE_TEXT_AFFICHER</com:TTranslate></label></strong></div>

					<com:TDropDownList ID="listePageSizeTop" AutoPostBack="true" onSelectedIndexChanged="changePagerLenght" Attributes.title="<%=Prado::localize('TEXT_NOMBRE_RESULTAT_PAR_PAGE')%>" >
						<com:TListItem Text="10" Selected="10"/>
						<com:TListItem Text="20" Value="20"/>
						<com:TListItem Text="50" Value="50" />
						<com:TListItem Text="100" Value="100" />
						<com:TListItem Text="500" Value="500" />
					</com:TDropDownList>
					<div class="intitule"><com:TTranslate>DEFINE_RESULTATS_PAGE</com:TTranslate></div>
					<com:TPanel ID="panelBouttonGotoPageTop" DefaultButton="DefaultButtonTop" CssClass="float-left">
						<com:TTextBox ID="numPageTop" Attributes.title='<%=Prado::localize("N_DE_LA_PAGE")%>' Text="1"/>
						<div class="nb-total ">/ <com:TLabel ID="nombrePageTop"/></div>
						<com:TButton ID="DefaultButtonTop"  OnClick="goToPage" Attributes.style="display:none"/>
					</com:TPanel>
					<div class="liens">
						<com:TPager ID="PagerTop"
									ControlToPaginate="tableauResultSearch"
									FirstPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-premier.gif' alt='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>' title='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>' />"
									LastPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-dernier.gif' alt='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>' title='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>' />"
									Mode="NextPrev"
									NextPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-suivant.gif' alt='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>' title='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>' />"
									PrevPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-precedent.gif' alt='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>' title='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>' />"
									OnPageIndexChanged="pageChanged"/>
					</div>

				</div>
			</div>
			<!--Fin partitionneur-->
			<com:TRepeater ID="tableauResultSearch"
						   AllowPaging="true"
						   PageSize="10"
						   EnableViewState="true"
						   AllowCustomPaging="true">

				<prop:HeaderTemplate>
					<table class="table-results" summary="Liste des entreprises">
						<caption><com:TTranslate>TEXT_LISTE_ENTREPRISE</com:TTranslate></caption>
						<thead>
						<tr>
							<th class="top" colspan="<%# ($this->templateControl->getCalledFrom() == 'entreprise') ? '5' : ( ($_GET['page']=='Agent.SearchActivationCompany') ? '7':'9')%>">
								<span class="left">&nbsp;</span><span class="right">&nbsp;</span></th>
						</tr>
						<tr>
							<th class="col-220" id="entrepriseName">
								<%#$this->templateControl->elementSortedWith('NOM','open')%>
								<com:TTranslate>TEXT_ENTREPRISE</com:TTranslate>
								<%#$this->templateControl->elementSortedWith('NOM','close')%>
								<com:PictoSort CommandName="NOM" OnCommand="Page.Trier" />
								<com:TPanel visible="<%#(($_GET['page']=='Agent.SearchActivationCompany')) ? true:false%>" >
									<br />
									<com:TTranslate>DEFINE_STATUS</com:TTranslate>
									<br />
									<com:TTranslate>TEXT_DATE_CREACTION_COMPTE</com:TTranslate>
								</com:TPanel>
								<com:TPanel visible="<%#($this->templateControl->getCalledFrom() == 'entreprise') ? true:false%>" >
									<com:TTranslate>DEFINE_CODE_NAF</com:TTranslate>
									<br />
									<com:TTranslate>TEXT_TYPE_COLLABORATION</com:TTranslate>
								</com:TPanel>

							</th>
							<th class="col-30" id="nbEtablissements"><com:TTranslate>NOMBRE_D_ETABLISSEMENTS</com:TTranslate></th>
							<th class="col-30" id="allCons_details" abbr="Détails">&nbsp;</th>
							<th   style="display:<%=(Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseRegion') && ($this->templateControl->getCalledFrom() == 'agent')) ? 'none':''%>" id="coordonnees">
								<com:TPanel visible="<%#(($this->templateControl->getCalledFrom() == 'entreprise')) ? true:false%>" >
									<br />
									<com:TTranslate>CONTACT</com:TTranslate>
									<br />
									<com:TTranslate>DEFINE_FONCTION_MAJ</com:TTranslate>
									<br />
									<com:TTranslate>DEFINE_E_MAIL</com:TTranslate>
									<br />
									<com:TTranslate>TEXT_CP</com:TTranslate>
									/
									<com:TTranslate>TEXT_VILLE</com:TTranslate>
								</com:TPanel>

								<com:TPanel visible="<%#(!Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseRegion') && ($this->templateControl->getCalledFrom() == 'agent')) ? true:false%>" >
									<com:TTranslate>ADRESSE_DU_SIEGE</com:TTranslate>
									<br />
									<%#$this->templateControl->elementSortedWith('CODEPOSTAL','open')%>
									<com:TTranslate>TEXT_CP</com:TTranslate>
									<%#$this->templateControl->elementSortedWith('CODEPOSTAL','close')%>
									<com:PictoSort CommandName="CODEPOSTAL" OnCommand="Page.Trier" />
									<br />
									<%#$this->templateControl->elementSortedWith('VILLEADRESSE','open')%>
									<com:TTranslate>TEXT_VILLE</com:TTranslate>
									<%#$this->templateControl->elementSortedWith('VILLEADRESSE','close')%>
									<com:PictoSort CommandName="VILLEADRESSE" OnCommand="Page.Trier" />
								</com:TPanel>
							</th>
							<th  style="display:<%=((Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseRegion')) && ($this->templateControl->getCalledFrom() == 'agent')) ? '':'none'%>" id="coordonnees">
								<com:TTranslate>ADRESSE_DU_SIEGE</com:TTranslate>
								<br />
								<%#$this->templateControl->elementSortedWith('CODEPOSTAL','open')%>
								<com:TTranslate>TEXT_CP</com:TTranslate>
								<%#$this->templateControl->elementSortedWith('CODEPOSTAL','close')%>
								<com:PictoSort CommandName="CODEPOSTAL" OnCommand="Page.Trier" />
								<br />
								<%#$this->templateControl->elementSortedWith('REGION','open')%>
								<com:TTranslate>TEXT_REGION</com:TTranslate>
								<%#$this->templateControl->elementSortedWith('REGION','close')%>
								<com:PictoSort CommandName="REGION" OnCommand="Page.Trier" />
								<br />
								<%#$this->templateControl->elementSortedWith('PROVINCE','open')%>
								<com:TTranslate>TEXT_PROVINCE</com:TTranslate>
								<%#$this->templateControl->elementSortedWith('PROVINCE','close')%>
								<com:PictoSort CommandName="PROVINCE" OnCommand="Page.Trier" />
								<br />
								<%#$this->templateControl->elementSortedWith('VILLEADRESSE','open')%>
								<com:TTranslate>TEXT_VILLE</com:TTranslate>
								<%#$this->templateControl->elementSortedWith('VILLEADRESSE','close')%>
								<com:PictoSort CommandName="VILLEADRESSE" OnCommand="Page.Trier" />
							</th>
							<th  style="display:<%=((Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseCodeApe') || Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseCodeNaceReferentiel')) && ($this->templateControl->getCalledFrom() == 'agent')) ? '':'none'%>" id="naf">
								<%#$this->templateControl->elementSortedWith('CODEAPE','open')%>
								<label style="display:<%=(Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseCodeApe')) ? '':'none'%>"><com:TTranslate>TEXT_NAF_SEUL</com:TTranslate></label>
								<label style="display:<%=(Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseCodeNaceReferentiel')) ? '':'none'%>"><com:TTranslate>TEXT_NACE</com:TTranslate></label>
								<%#$this->templateControl->elementSortedWith('CODEAPE','close')%>
								<com:PictoSort CommandName="CODEAPE" OnCommand="Page.Trier" />
							</th>
							<th  id="description">
								<com:TTranslate>TEXT_DESCRIPTION_ACTIVITE </com:TTranslate>
								<label style="display:<%=(Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseActiviteDomaineDefense')) ? '':'none'%>">
									/<br />
									<com:TTranslate>DEFINE_ACTIVITES_DOMAINE_DEFENSE </com:TTranslate>
								</label>
								<label style="display:<%=(Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseDomaineActivite') && ($this->templateControl->getCalledFrom() == 'agent')) ? '':'none'%>">
									/<br />
									<com:TTranslate>TEXT_DOMAINES_ACTIVITES </com:TTranslate>
								</label>
								<!-- Début domaine d'activité utilisant LT-Ref -->
								<label style="display:<%=(Application\Service\Atexo\Atexo_Module::isEnabled('compteEntrepriseDomaineActiviteLtReferentiel') && ($this->templateControl->getCalledFrom() == 'agent')) ? '':'none'%>">
									/<br />
									<com:TTranslate>TEXT_DOMAINES_ACTIVITES </com:TTranslate>
								</label>
								<!-- Fin domaine d'activité utilisant LT-Ref -->
							</th>
							<th  id="agrement" style="display:<%=($this->templateControl->getCalledFrom() == 'agent') ? '' : 'none'%>">
								<com:TLabel style="display:<%=(Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseAgrement')) ? '':'none'%>">
									<com:TTranslate>TEXT_AGREMENT </com:TTranslate>
								</com:TLabel>
								<com:TLabel style="display:<%=(Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseAgrementLtReferentiel') && ($this->templateControl->getCalledFrom() == 'agent')) ? '':'none'%>">
									<com:TTranslate>TEXT_AGREMENT_LT_REFERENTIEL </com:TTranslate>
								</com:TLabel>
							</th>
							<th  id="qualification"  style="display:<%=($this->templateControl->getCalledFrom() == 'agent') ? '' : 'none'%>">
								<com:TLabel style="display:<%=(Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseQualification')) ? '':'none'%>">
									<com:TTranslate>TEXT_QUALIFICATIONS </com:TTranslate>
								</com:TLabel>
								<com:TLabel style="display:<%=(Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseQualificationLtReferentiel') && ($this->templateControl->getCalledFrom() == 'agent')) ? '':'none'%>">
									<com:TTranslate>TEXT_QUALIFICATIONS </com:TTranslate>
								</com:TLabel>
							</th>
							<th  style="display:<%#($_GET['page']=='Agent.SearchActivationCompany') ? '':'none'%>" class="actions" id="actions">
								<com:TTranslate>DEFINE_ACTIONS</com:TTranslate>
							</th>
							<com:TPanel id="panelChoixAllEse" Visible="<%=($_GET['page'] == 'Agent.ChoixDestinataireBdFournisseur') ? true:false%>" >
								<th class="check-col" id="checkAll" abbr="<com:TTranslate>TEXT_TOUS</com:TTranslate>">
									<label for="selectAll" style="display: none;"><com:TTranslate>TEXT_TOUT_SELECTIONNER</com:TTranslate></label>
									<com:TActiveCheckBox
											id="selectAll"
											Attributes.title="<%=Prado::localize('TEXT_TOUS_AUCUN')%>"
											Attributes.onclick="CheckUnCheckAll(this, 'panelResultSerch_tableauResultSearch', 'entrepriseSelection');"
									/>
								</th>
							</com:TPanel>

						</tr>
						</thead>
				</prop:HeaderTemplate>
				<prop:ItemTemplate>
					<tr class="<%#(($this->ItemIndex%2==0)? '':'on')%>">
						<td class="col-220" headers="entrepriseName">
							<span class="blue <%# (new Application\Service\Atexo\Atexo_Entreprise())->getCssSaisieMnuelle($this->Data)%> "><%#$this->Data->getNom()%></span>
							<br />
							<span class="<%# (new Application\Service\Atexo\Atexo_Entreprise())->getCssSaisieMnuelle($this->Data)%> "><%#$this->Data->getPaysenregistrement ()%> -
    							<%#$this->Data->getSiren()%> <%#$this->Data->getSirenetranger()%></span>
							<com:TPanel visible="<%#(($_GET['page']=='Agent.SearchActivationCompany') && (Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseActivationInscriptionParAgent')) && $this->Data->getCompteActif()=='0') ? true:false%>" cssClass="statut-activation red">
								<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif" alt="<%=Prado::localize('TEXT_INACTIF')%>" title="<%=Prado::localize('TEXT_INACTIF')%>" />
								<com:TTranslate>TEXT_INACTIF</com:TTranslate>
							</com:TPanel>
							<com:TPanel visible="<%#(($_GET['page']=='Agent.SearchActivationCompany') && (Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseActivationInscriptionParAgent')) && $this->Data->getCompteActif()=='1') ? true:false%>" cssClass="statut-activation green">
								<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-ok-small.gif" alt="<%=Prado::localize('TEXT_ACTIF')%>" title="<%=Prado::localize('TEXT_ACTIF')%>" />
								<com:TTranslate>TEXT_ACTIF</com:TTranslate>
							</com:TPanel>
							<com:TPanel visible="<%#(($_GET['page']=='Agent.SearchActivationCompany')) ? true:false%>" >
								<span class="<%# (new Application\Service\Atexo\Atexo_Entreprise())->getCssSaisieMnuelle($this->Data)%> "> <%# Application\Service\Atexo\Atexo_Util::iso2frnDateTime($this->Data->getDateCreation())%></span>
								<br/>
								<com:TTranslate>CREER_PAR</com:TTranslate> :
								<span class="<%# (new Application\Service\Atexo\Atexo_Entreprise())->getCssSaisieMnuelle($this->Data)%> ">  <%#$this->Data->getPrenomAgent() . ' ' . $this->Data->getNomAgent()%></span>
							</com:TPanel>
							<com:TPanel visible="<%#($this->templateControl->getCalledFrom() == 'entreprise') ? true:false%>" >
								<span class="<%# (new Application\Service\Atexo\Atexo_Entreprise())->getCssSaisieMnuelle($this->Data)%> ">   <%#str_replace('#','',$this->Data->getCodeape())%></span>
								<br/>
								<div class="collaboration">
									<com:TRepeater  ID="repeaterTypeCollaboration" dataSource="<%#$this->templateControl->getArrayTypeColloboration($this->Data->getTypeCollaboration())%>">
										<prop:ItemTemplate>
											<div  class="statut-collaborateur statut-collaborateur-<%#$this->Data['id']%>sur4" title="<%#($this->Data['libelle'])%>" ></div>
										</prop:ItemTemplate>
									</com:TRepeater>
								</div>
							</com:TPanel>
						</td>
						<td class="col-30" headers="nbEtablissements"><%#$this->Data->countCommonTEtablissements()%></td>
						<td class="col-30" headers="allCons_details">
							<a href="javascript:popUp('index.php?page=<%#$this->templateControl->getCalledFrom()%>.DetailEntreprise&callFrom=<%#((Application\Service\Atexo\Atexo_Module::isEnabled('BourseALaSousTraitance')) && ($_GET['page']=='Entreprise.RechercheCollaborateur')) ? 'entreprise' : 'agent'%>&idEntreprise=<%#$this->Data->getId()%>','yes');">
								<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-details.gif" alt="<%=Prado::localize('TEXT_VOIR_DETAIL')%>" title="<%=Prado::localize('TEXT_VOIR_DETAIL')%>" />
							</a>
						</td>
						<td headers="coordonnees" style="display:<%=(Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseRegion') && ($this->templateControl->getCalledFrom() == 'agent')) ? 'none':''%>" id="coordonnees">

							<com:TPanel visible="<%#($this->templateControl->getCalledFrom() == 'entreprise') ? true:false%>" >
    							<span class="<%# (new Application\Service\Atexo\Atexo_Entreprise())->getCssSaisieMnuelle($this->Data)%> ">
   			  				  				<%#($this->Data->getContactByIdEntreprise()instanceof Application\Propel\Mpe\CommonContactEntreprise)? $this->Data->getContactByIdEntreprise()->getNom() : ''%> - <%#($this->Data->getContactByIdEntreprise()instanceof CommonContactEntreprise)? $this->Data->getContactByIdEntreprise()->getPrenom() : ''%>
        								<br />
        									<%#($this->Data->getContactByIdEntreprise() instanceof Application\Propel\Mpe\CommonContactEntreprise)? $this->Data->getContactByIdEntreprise()->getFonction() : ''%>
        								<br />
        									<%#($this->Data->getContactByIdEntreprise() instanceof Application\Propel\Mpe\CommonContactEntreprise)? $this->Data->getContactByIdEntreprise()->getEmail(): ''%>
										 <br />
        									<%#($this->Data->getContactByIdEntreprise() instanceof Application\Propel\Mpe\CommonContactEntreprise)? $this->Data->getContactByIdEntreprise()->getCodePostal(): ''%> -
        									<%#($this->Data->getContactByIdEntreprise() instanceof Application\Propel\Mpe\CommonContactEntreprise)? $this->Data->getContactByIdEntreprise()->getVille(): ''%>
    							</span>
							</com:TPanel>
							<com:TPanel visible="<%#(!Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseRegion') && ($this->templateControl->getCalledFrom() == 'agent')) ? true:false%>" >
			        					<span class="<%# (new Application\Service\Atexo\Atexo_Entreprise())->getCssSaisieMnuelle($this->Data)%> "> <%#$this->Data->getAdresse()%>
		        						<com:TLabel visible="<%#($this->Data->getAdresse2()!='') ? true:false%>" Text="<br/>">
		        							<%#$this->Data->getAdresse2()%>
		        						</com:TLabel>
		        						<br />
		        						<%#$this->Data->getCodepostal()%>
		        						<br />
		        						<%#$this->Data->getVilleadresse()%>
		        						</span>
							</com:TPanel>
						</td>
						<td  headers="coordonnees" style="display:<%=(Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseRegion') && ($this->templateControl->getCalledFrom() == 'agent')) ? '':'none'%>">
	        						<span class="<%# (new Application\Service\Atexo\Atexo_Entreprise())->getCssSaisieMnuelle($this->Data)%> "><%#$this->Data->getAdresse()%>
	        						<com:TLabel visible="<%#($this->Data->getAdresse2()!='') ? true:false%>" Text="<br/>">
	        							<%#$this->Data->getAdresse2()%>
	        						</com:TLabel></span>
							<br />
							<span class="<%# (new Application\Service\Atexo\Atexo_Entreprise())->getCssSaisieMnuelle($this->Data)%> ">
	        								<%#Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN1::retrieveDenomination1ByIdGeoN1($this->Data->getRegion())%>
	        									<br style="display:<%#($this->Data->getRegion()) ? '':'none'%>" />

	        								<%# (new Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2())->retrieveDenomination1ByIdGeoN3($this->Data->getProvince())%>
	    										<br style="display:<%#($this->Data->getProvince()) ? '':'none'%>" />
	        						<%#$this->Data->getCodepostal()%>
	        						<br />
	        						<%#$this->Data->getVilleadresse()%>
	        						</span>
						</td>
						<td  headers="naf" style="display:<%=((Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseCodeApe') || Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseCodeNaceReferentiel')) && ($this->templateControl->getCalledFrom() == 'agent'))? '':'none'%>">
							<span class="<%# (new Application\Service\Atexo\Atexo_Entreprise())->getCssSaisieMnuelle($this->Data)%> "><%#str_replace('#','',$this->Data->getCodeape())%> </span>
						</td>

						<td  headers="description">
    						<span class="<%# (new Application\Service\Atexo\Atexo_Entreprise())->getCssSaisieMnuelle($this->Data)%> ">
    							<div class="objet-line"><%# Application\Service\Atexo\Atexo_Util::truncateTexte($this->Data->getDescriptionActivite(),'100')%> </div>
    							<div class="spacer-mini"></div>
    							<div style="display:<%=(Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseActiviteDomaineDefense')) ? '':'none'%>" class="objet-line">
								<%# Application\Service\Atexo\Atexo_Util::truncateTexte($this->Data->getActiviteDomaineDefense(),'100')%>
    								<span style="<%#$this->templateControl->isTextTruncated($this->Data->getActiviteDomaineDefense())%>" onmouseover="afficheBulle('ctl0_CONTENU_PAGE_panelResultSerch_tableauResultSearch_ctl<%#($this->ItemIndex+1)%>_infosConsultation', this)" onmouseout="cacheBulle('ctl0_CONTENU_PAGE_panelResultSerch_tableauResultSearch_ctl<%#($this->ItemIndex+1)%>_infosConsultation')" class="info-suite">...</span>
									<com:TPanel ID="infosConsultation" CssClass="info-bulle" >
										<div><%#$this->Data->getActiviteDomaineDefense()%></div>
									</com:TPanel>
								</div>
								</span>
							<div class="spacer-mini"></div>
							<com:TPanel visible="<%#(Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseDomaineActivite') && ($this->templateControl->getCalledFrom() == 'agent')) ? true:false%>" Cssclass="objet-line"><%# Application\Service\Atexo\Atexo_Util::truncateTexte((new Application\Service\Atexo\Consultation\Atexo_Consultation_Category())->displayListeLibelleDomaineByArrayId($this->Data->getDomainesActivites()),100)%>
								<span
										onmouseover="afficheBulle('infosSecteursActivite_<%#$this->ItemIndex%>', this)"
										onmouseout="cacheBulle('infosSecteursActivite_<%#$this->ItemIndex%>')"
										class="info-suite"
										title="Info-bulle">
	    							 	<com:TLabel Display="<%#(strlen((new Application\Service\Atexo\Consultation\Atexo_Consultation_Category())->displayListeLibelleDomaineByArrayId($this->Data->getDomainesActivites()))>110) ? 'Dynamic':'None'%>" Text ="..."/>
	    							 </span>
								<div id="infosSecteursActivite_<%#$this->ItemIndex%>"
									 class="info-bulle"
									 onmouseover="mouseOverBulle();"
									 onmouseout="mouseOutBulle();">
									<div>
										<%#(new Application\Service\Atexo\Consultation\Atexo_Consultation_Category())->displayListeLibelleDomaineByArrayId($this->Data->getDomainesActivites())%>
									</div>
								</div>
							</com:TPanel>
							<!-- Début domaine d'activité utilisant LT-Ref -->
							<com:TPanel visible="<%#(Application\Service\Atexo\Atexo_Module::isEnabled('compteEntrepriseDomaineActiviteLtReferentiel') && ($this->templateControl->getCalledFrom() == 'agent')) ? true:false%>" Cssclass="objet-line">
								<%#$this->TemplateControl->doGetLibelleLtRefDomaineActivite($this->Data)
								%>
								<span
										onmouseover="afficheBulle('infosDomainesAtivitesLtRef_<%#$this->ItemIndex%>', this)"
										onmouseout="cacheBulle('infosDomainesAtivitesLtRef_<%#$this->ItemIndex%>')"
										class="info-suite"
										title="Info-bulle">
		    							 	<com:TLabel Display="<%#(strlen($this->Data->getLibelleLtRef($this->Data->getDomainesActivites(),Application\Service\Atexo\Atexo_Languages::readLanguageFromSession(),  Application\Service\Atexo\Atexo_Config::getParameter('PATH_FILE_DOMAINES_ACTIVITES_CONFIG'))) > 110) ? 'Dynamic':'None'%>" Text ="..."/>
		    							 </span>
								<div id="infosDomainesAtivitesLtRef_<%#$this->ItemIndex%>"
									 class="info-bulle"
									 onmouseover="mouseOverBulle();"
									 onmouseout="mouseOutBulle();">
									<div><%#$this->TemplateControl->doGetLibelleLtRefDomaineActivite($this->Data,true)%></div>
								</div>
							</com:TPanel>
							<!-- Fin domaine d'activité utilisant LT-Ref -->
							<div class="spacer-mini"></div>
						</td>
						<td  headers="agrement" style="display:<%=($this->templateControl->getCalledFrom() == 'agent') ? '' : 'none'%>">

							<com:TPanel visible="<%#(Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseAgrement')) ? true:false%>" Cssclass="objet-line">
								<%# Application\Service\Atexo\Atexo_Util::truncateTexte(wordwrap((new Application\Service\Atexo\Atexo_Agrement())->retrieveLibelleAgrementsByIds($this->Data->getAgrement()),220, '<br/>', true),50)%>
								<span
										onmouseover="afficheBulle('infosAgrement_<%#$this->ItemIndex%>', this)"
										onmouseout="cacheBulle('infosAgrement_<%#$this->ItemIndex%>')"
										class="info-suite"
										title="Info-bulle"> <com:TLabel Display="<%#(strlen((new Application\Service\Atexo\Atexo_Agrement())->retrieveLibelleAgrementsByIds($this->Data->getAgrement()))>50) ? 'Dynamic':'None'%>" Text ="..."/></span>
								<div id="infosAgrement_<%#$this->ItemIndex%>"
									 class="info-bulle"
									 onmouseover="mouseOverBulle();"
									 onmouseout="mouseOutBulle();">
									<div><%#wordwrap((new Application\Service\Atexo\Atexo_Agrement())->retrieveLibelleAgrementsByIds($this->Data->getAgrement()),220, '<br/>', true)%></div>
								</div>

							</com:TPanel>
							<com:TPanel visible="<%#(Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseAgrementLtReferentiel')) ? true:false%>" Cssclass="objet-line">
								<%# $this->TemplateControl->doGetLibelleLtRefAgrement($this->Data)
								%>
								<span
										onmouseover="afficheBulle('infosAgrementLtRef_<%#$this->ItemIndex%>', this)"
										onmouseout="cacheBulle('infosAgrementLtRef_<%#$this->ItemIndex%>')"
										class="info-suite"
										title="Info-bulle">
	    							 	<com:TLabel Display="<%#(strlen($this->Data->getLibelleLtRef($this->Data->getAgrement(),Application\Service\Atexo\Atexo_Languages::readLanguageFromSession(),  Application\Service\Atexo\Atexo_Config::getParameter('PATH_FILE_CERTIFICAT_COMPTE_ENTREPRISE_CONFIG'))) > 110) ? 'Dynamic':'None'%>" Text ="..."/>
	    							 </span>
								<div id="infosAgrementLtRef_<%#$this->ItemIndex%>"
									 class="info-bulle"
									 onmouseover="mouseOverBulle();"
									 onmouseout="mouseOutBulle();">
									<div><%#$this->TemplateControl->doGetLibelleLtRefAgrement($this->Data,true)%></div>
								</div>

							</com:TPanel>
							<div class="spacer-mini"></div>
						</td>
						<td  headers="qualification" style="display:<%=($this->templateControl->getCalledFrom() == 'agent') ? '' : 'none'%>">
							<div class="spacer-mini"></div>
							<com:TPanel visible="<%#(Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseQualification')) ? true:false%>" Cssclass="objet-line">
								<%# Application\Service\Atexo\Atexo_Util::truncateTexte(wordwrap((new Application\Service\Atexo\Atexo_Qualifications())->displayListeLibelleDomaineByArrayId($this->Data->getQualification()),220, '<br/>', true),50)%>
								<span
										onmouseover="afficheBulle('infosQualification_<%#$this->ItemIndex%>', this)"
										onmouseout="cacheBulle('infosQualification_<%#$this->ItemIndex%>')"
										class="info-suite"
										title="Info-bulle"> <com:TLabel Display="<%#(strlen((new Application\Service\Atexo\Atexo_Qualifications())->displayListeLibelleDomaineByArrayId($this->Data->getQualification()))>50) ? 'Dynamic':'None'%>" Text ="..."/></span>
								<div id="infosQualification_<%#$this->ItemIndex%>"
									 class="info-bulle"
									 onmouseover="mouseOverBulle();"
									 onmouseout="mouseOutBulle();">
									<div><%#wordwrap((new Application\Service\Atexo\Atexo_Qualifications())->displayListeLibelleDomaineByArrayId($this->Data->getQualification()),220, '<br/>', true)%></div>
								</div>

							</com:TPanel>
							<com:TPanel visible="<%#(Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseQualificationLtReferentiel')) ? true:false%>" Cssclass="objet-line">
								<%# $this->TemplateControl->doGetLibelleLtRefQualification($this->Data)
								%>
								<span
										onmouseover="afficheBulle('infosQualificationLtRef_<%#$this->ItemIndex%>', this)"
										onmouseout="cacheBulle('infosQualificationLtRef_<%#$this->ItemIndex%>')"
										class="info-suite"
										title="Info-bulle">
	    							 	<com:TLabel Display="<%#(strlen($this->Data->getLibelleLtRef($this->Data->getQualification(),Application\Service\Atexo\Atexo_Languages::readLanguageFromSession(),  Application\Service\Atexo\Atexo_Config::getParameter('PATH_FILE_QUALIFICATION_CONFIG'))) > 110) ? 'Dynamic':'None'%>" Text ="..."/>
	    							 </span>
								<div id="infosQualificationLtRef_<%#$this->ItemIndex%>"
									 class="info-bulle"
									 onmouseover="mouseOverBulle();"
									 onmouseout="mouseOutBulle();">
									<div><%#$this->TemplateControl->doGetLibelleLtRefQualification($this->Data,true)%></div>
								</div>
							</com:TPanel>
						</td>
						<td style="display:<%#($_GET['page']=='Agent.SearchActivationCompany') ? '':'none'%>"
							class="actions" headers="actions">
							<com:TActiveImageButton
									Visible="<%#($this->Data->getCompteActif()=='1') ? true:false%>"
									ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-desactiver.gif"
									Attributes.alt="<%=Prado::localize('TEXT_ALERTE_DESACTIVER')%>"
									Attributes.title="<%=Prado::localize('TEXT_ALERTE_DESACTIVER')%>"
									Attributes.onclick='javascript:if(!confirm("<%=Prado::localize('TEXT_CONFIRAMATION_DESACTIVER_COMPTE_ENTREPRISE')%>"))return false;'
									CommandName="Lock"
									onCommand="Page.panelResultSerch.actionCompany"
									onCallBack="Page.panelResultSerch.refreshRepeater"
									CommandParameter="<%#$this->Data->getId()%>" />
							<com:TActiveImageButton
									Visible="<%#($this->Data->getCompteActif()=='0') ? true:false%>"
									ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-activer.gif"
									Attributes.alt="<%=Prado::localize('TEXT_ACTIVER')%>"
									Attributes.title="<%=Prado::localize('TEXT_ACTIVER')%>"
									Attributes.onclick='javascript:if(!confirm("<%=Prado::localize('TEXT_CONFIRAMATION_ACTIVER_COMPTE_ENTREPRISE')%>"))return false;'
									CommandName="UnLock"
									onCommand="Page.panelResultSerch.actionCompany"
									onCallBack="Page.panelResultSerch.refreshRepeater"
									CommandParameter="<%#$this->Data->getId()%>"/>
						</td>
						<com:TPanel id="panelChoixEse" Visible="<%=($_GET['page'] == 'Agent.ChoixDestinataireBdFournisseur') ? true:false%>" >
							<td class="check-col" headers="checkAll">
								<com:THiddenField id="IdEse" Value="<%#$this->Data->getId()%>" />
								<com:TActiveCheckBox
										id="entrepriseSelection"
										Attributes.title="<%=Prado::localize('TEXT_DEFINE_SELECTIONNER')%>" checked="<%#$this->TemplateControl->getEtat($this->Data->getId())%>"/>
							</td>
						</com:TPanel>
					</tr>
				</prop:ItemTemplate>
				<prop:FooterTemplate>
					</table>
				</prop:FooterTemplate>
			</com:TRepeater>
			<!--Debut partitionneur-->
			<div class="line-partitioner">
				<div class="partitioner">
					<com:TDropDownList ID="listePageSizeBottom" AutoPostBack="true" onSelectedIndexChanged="changePagerLenght" Attributes.title="<%=Prado::localize('TEXT_NOMBRE_RESULTAT_PAR_PAGE')%>" >
						<com:TListItem Text="10" Selected="10"/>
						<com:TListItem Text="20" Value="20"/>
						<com:TListItem Text="50" Value="50" />
						<com:TListItem Text="100" Value="100" />
						<com:TListItem Text="500" Value="500" />
					</com:TDropDownList>

					<div class="intitule"><com:TTranslate>DEFINE_RESULTATS_PAGE</com:TTranslate></div>

					<com:TPanel ID="panelBouttonGotoPageBottom" DefaultButton="DefaultButtonBottom" CssClass="float-left">
						<com:TTextBox ID="numPageBottom" Attributes.title='<%=Prado::localize("N_DE_LA_PAGE")%>' Text="1"/>
						<div class="nb-total ">/ <com:TLabel ID="nombrePageBottom"/></div>
						<com:TButton ID="DefaultButtonBottom"  OnClick="goToPage" Attributes.style="display:none"/>
					</com:TPanel>

					<div class="liens">
						<com:TPager ID="PagerBottom"
									ControlToPaginate="tableauResultSearch"
									FirstPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-premier.gif' alt='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>' title='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>' />"
									LastPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-dernier.gif' alt='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>' title='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>' />"
									Mode="NextPrev"
									NextPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-suivant.gif' alt='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>' title='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>' />"
									PrevPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-precedent.gif' alt='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>' title='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>' />"
									OnPageIndexChanged="pageChanged"/>
					</div>
				</div>
			</div>
		</com:TPanel>
		<com:TPanel ID="exportEntreprise" CssClass="file-link margin-left-15" visible ="<%=($this->getCalledFrom() == 'agent') ? true:false%>">
			<com:TLinkButton
					Visible="<%=($_GET['page'] == 'Agent.ChoixDestinataireBdFournisseur') ? 'false' : 'true'%>"
					OnClick="genererExcelEntreprise">
				<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-xls.gif" alt="Export XLS" title="Export XLS" />
			</com:TLinkButton>
		</com:TPanel >
		<com:TPanel ID="panelValider" visible="<%=($_GET['page'] == 'Agent.ChoixDestinataireBdFournisseur') ? 'true' : 'false'%>" >
			<div class="boutons-line">
				<com:TButton
						id="valider"
						CssClass="bouton-moyen float-right"
						Attributes.title="<%=Prado::localize('ICONE_VALIDER')%>"
						Text="<%=Prado::localize('ICONE_VALIDER')%>"
						OnClick="validerChoix" />
			</div>
		</com:TPanel >
	</div>
	<!--Fin partitionneur-->
	<div class="breaker"></div>
	<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
</div>
<div class="link-line">
	<com:TLinkButton cssClass="bouton-retour" Text ="<%=Prado::localize('TEXT_MODIFIER_MA_RECHERCHE')%>" Attributes.title="<%=Prado::localize('TEXT_MODIFIER_MA_RECHERCHE')%>" onClick ="Page.panelSearch.displayCriteria" />
	<com:TLinkButton cssClass="bouton-suivant" Text ="<%=Prado::localize('DEFINE_NOUVELLE_RECHERCHE')%>" Attributes.title="<%=Prado::localize('DEFINE_NOUVELLE_RECHERCHE')%>" onClick ="Page.panelSearch.displayPanelSearch"  />
</div>

<!--Fin Bloc Liste des acces-->
