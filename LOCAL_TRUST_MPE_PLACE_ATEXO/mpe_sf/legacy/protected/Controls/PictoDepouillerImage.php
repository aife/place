<?php

namespace Application\Controls;

use Application\Service\Atexo\Controller\Atexo_Controller_Front;
use Prado\Prado;
use Prado\Web\UI\WebControls\TImage;

/**
 * commentaires.
 *
 * @author anas ZAKI <anas.zaki@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PictoDepouillerImage extends TImage
{
    public $_parentPage;
    public string $_activate = 'false';

    public function onLoad($param)
    {
        if ('false' == $this->_activate) {
            $this->setImageUrl(Atexo_Controller_Front::t().'/images/picto-depouiller-inactive.gif');
        } else {
            $this->setImageUrl(Atexo_Controller_Front::t().'/images/picto-depouiller.gif');
        }

        $this->setAlternateText(Prado::localize('ICONE_ACCES_AUX_REPONSES'));
        $this->setToolTip(Prado::localize('ICONE_ACCES_AUX_REPONSES'));
    }

    public function getParentPage()
    {
        return $this->_parentPage;
    }

    public function setParentPage($value)
    {
        $this->_parentPage = $value;
    }

    public function getActivate()
    {
        return $this->_activate;
    }

    public function setActivate($bool)
    {
        $this->_activate = $bool;
    }
}
