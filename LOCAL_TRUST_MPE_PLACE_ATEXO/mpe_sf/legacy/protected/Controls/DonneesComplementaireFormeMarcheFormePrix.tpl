<com:TActivePanel id="panelFormeMarchePrix" style="display:<%=($this->afficher)? '':'none'%>" cssclass="toggle-panel form-toggle donnees-redac">
    <div class="content">
        <div class="cover"></div>
        <com:THyperLink ID="linkTogglePanel" NavigateUrl="javascript:void(0);" Attributes.onClick="togglePanel(this,'<%=$this->panelSousFormeMarche->getClientId()%>');" CssClass="title-toggle-open" >
            <com:TTranslate>DEFINE_FORME_MARCHE_ET_FORME_PRIX</com:TTranslate>
        </com:THyperLink>
        <com:TPanel cssClass="panel no-indent panel-donnees-complementaires" style="display:block;" id="panelSousFormeMarche">
            <div class="caracteristiques-forme-marche">
                <!--Debut Ligne Marche reconductible-->
                <div class="line">
                    <div class="intitule-auto float-left">
                        <com:TCheckBox id="marcheTranche" Attributes.title="<%=Prado::localize('DEFINE_MARCHE_AVEC_TRANCHE_FERME_ET_TRANCHE_CONDITIONNELLE')%>" Attributes.onclick="isChecked2(this,'<%=$this->infos_tranches_lot->getClientId()%>','<%=$this->layer_ss_tranche_lot->getClientId()%>');displayAppartenanceLot('<%=$this->marcheTranche->getClientId()%>','<%=$this->repeaterDetailLotsTechniques->getClientId()%>','<%=$this->headAppartenance->getClientId()%>');" Enabled="<%=$this->enabledElement()%>" /><label for="marcheTranche"><com:TTranslate>DEFINE_MARCHE_AVEC_TRANCHE_FERME_ET_TRANCHE_CONDITIONNELLE</com:TTranslate></label>
                    </div>
                    <div class="col-info-bulle">
                        <img title="Info-bulle" alt="Info-bulle" class="picto-info-intitule pos-t-5" onmouseout="cacheBulle('infosMarcheTranche')" onmouseover="afficheBulle('infosMarcheTranche', this)" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" />
                        <div id="infosMarcheTranche" class="info-bulle" onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();"><div><com:TTranslate>TEXT_INFO_BULLE_MARCHE_A_TRANCHES_CONDITIONNELLE</com:TTranslate></div></div>
                    </div>
                </div>
                <!--Fin Ligne Marche reconductible-->
                <com:TActiveButton ID="linkDefinirFormePrix" style="display:none" Attributes.title="<%=Prado::localize('TEXT_FORME_DE_PRIX')%>" Attributes.onClick="javascript:void(0);"  CssClass="bouton-small definir-forme-prix" >
                </com:TActiveButton>
                <!--Debut Forme de prix marche sans tranche-->
                <com:TActivePanel id="panelFormePrixSansTranche">
                    <com:TPanel id="layer_ss_tranche_lot" style="display:block" cssClass="panel-tranche-lot-tech">

                        <table class="form-prix-ss-tranche">
                            <thead>
                            <tr>
                                <th class="forme-prix"><div><com:TTranslate>TEXT_FORME_DE_PRIX</com:TTranslate><span class="champ-oblig">*</span></div>
                                    <div class="col-info-bulle">
                                        <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" onmouseover="afficheBulleWithJquery(this)" onmouseout="cacheBulleWithJquery(this)" class="picto-info infos-tranches pos-t-1"  alt="Info-bulle" title="Info-bulle" />
                                        <div onmouseout="mouseOutBulle();" id="infosFormePrix-ss-tranche-lot"
                                             onmouseover="mouseOverBulle();"
                                             class="info-bulle">
                                            <div><com:TTranslate>TEXT_INFO_BULLE_FORME_DE_PRIX</com:TTranslate></div>
                                        </div>
                                    </div>
                                </th>
                                <th class="estimation" style="display: none;"><com:TTranslate>DEFINE_ESTIMATION_INTERNE_HT</com:TTranslate>&nbsp;</th>
                                <th>&nbsp;</th>
                            </tr>
                            </thead>
                            <tr class="on">
                                <td class="forme-prix">
                                    <com:TActiveImage id="errorFormPrixST" ImageUrl='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' Attributes.alt='Champ obligatoire' style='display:none'/>
                                    <com:TActiveHiddenField
                                            id="formePrixSansTrancheHidden"
                                    />
                                    <span id="errorFormePrixSansTranche" title="Champ obligatoire" style="check-invalide;display:none;"><img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif" alt="Champ obligatoire"></span>
                                    <com:TActiveLinkButton
                                            CssClass="bouton-small priceFormBtn"
                                            ID="linkDefinirActionFormePrix"
                                            Attributes.title="<%=Prado::localize('DEFINIR')%>"
                                            OnCallBack="SourceTemplateControl.definirFormePrixTranche"
                                            ActiveControl.CallbackParameter="<%=$this->linkDefinirFormePrix->getClientId().'#0#'%>"
                                    >
                                        <com:TTranslate>DEFINIR</com:TTranslate>
                                    </com:TActiveLinkButton>

                                </td>
                                <td class="estimation" style="display: none;"><com:TActiveLabel ID="estimationHTSansTranche" /></td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr class="off" id="choiceFormePrix">
                                <td>
                                    <table>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td>Forme de prix: </td>
                                            <td class="forme-prix-values">
                                                <com:TActiveLabel id="formePrixSansTranche" />
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr class="on" id="valuesFormePrix">
                                <td>
                                    <com:TPanel id="blocPf" style="display:none">
                                        <table>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td>Variation du prix: </td>
                                                <td class="forme-prix-values"><com:TActiveLabel id="pfVariationLabel" /></td>
                                            </tr>
                                        </table>
                                    </com:TPanel>

                                    <com:TPanel id="blocPU" style="display:none">
                                        <table>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td>Prix unitaire:</td>
                                                <td class="forme-prix-values"><com:TActiveLabel id="puQuantiteLabel" /></td>
                                            </tr>
                                            <com:TPanel id="blocPUMinMax" style="display:none">
                                                <tr>
                                                    <td>&nbsp;</td>
                                                    <td class="forme-prix-values"><com:TActiveLabel id="puMinMaxSelectionLabel" /></td>
                                                    <td class="forme-prix-values">
                                                        <com:TPanel id="blocPUAvecMinMax" style="display:none">
                                                            <com:TTranslate>FCSP_MIN</com:TTranslate>: <com:TActiveLabel id="puMinValue" /> <com:TActiveLabel id="puUniteMinLabel" /> -
                                                            <com:TTranslate>FCSP_MAX</com:TTranslate>: <com:TActiveLabel id="puMaxValue" /> <com:TActiveLabel id="puUniteMaxLabel" />
                                                        </com:TPanel>
                                                    </td>
                                                </tr>
                                            </com:TPanel>

                                            <tr>
                                                <td>&nbsp;</td>
                                                <td>Variation du prix: </td>
                                                <td class="forme-prix-values"><com:TActiveLabel id="puVariationLabel" /></td>
                                            </tr>

                                            <tr>
                                                <td>&nbsp;</td>
                                                <td>Type de Prix: </td>
                                                <td class="forme-prix-values">
                                                    <com:TActiveLabel id="puTypePrixLabel" />
                                                </td>
                                            </tr>
                                        </table>
                                    </com:TPanel>
                                </td>
                                <td class="estimation" style="display: none;">&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                    </com:TPanel>
                </com:TActivePanel>
                <!--Fin Forme de prix marche sans tranche-->

                <!--Debut detail tranches-->
                <com:TPanel id="infos_tranches_lot" style="display:none;" cssClass="detail-tranches-lots">
                    <a class="title-toggle-open" onclick="togglePanel(this,'layer-tranche');" href="javascript:void(0);"><com:TTranslate>DEFINE_DETAILS_TRANCHES</com:TTranslate></a>
                    <div id="layer-tranche" style="display:block" class="panel-tranche-lot-tech">
                        <com:TActivePanel id="panelTranchesMarcheprix">
                            <com:TRepeater ID="listeTranchesMarchePrix"	>
                                <prop:HeaderTemplate>
                                    <table summary="<com:TTranslate>DEFINE_DETAILS_TRANCHES</com:TTranslate>">
                                        <thead>
                                        <tr>
                                            <th class="identifiant"><com:TTranslate>DEFINE_TEXT_IDENTIFIANT</com:TTranslate></th>
                                            <th class="intitule-tranche"><com:TTranslate>DEFINE_INTITULE</com:TTranslate></th>
                                            <th class="forme-prix"><div><com:TTranslate>TEXT_FORME_DE_PRIX</com:TTranslate><span class="champ-oblig">*</span></div>
                                                <div class="infos-form-prix"><img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" onmouseover="afficheBulle('infosFormePrix-tranche', this)" onmouseout="cacheBulle('infosFormePrix-tranche')" class="picto-info infos-tranches pos-t-1"  alt="Info-bulle" title="Info-bulle" /></div>
                                                <div id="infosFormePrix-tranche" class="info-bulle" onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();"><div><com:TTranslate>TEXT_INFO_BULLE_FORME_DE_PRIX</com:TTranslate></div></div>
                                            </th>
                                            <th class="estimation" style="display: none;"><com:TTranslate>DEFINE_ESTIMATION_INTERNE_HT</com:TTranslate></th>
                                            <th class="actions">&nbsp;</th>
                                        </tr>
                                        </thead>
                                </prop:HeaderTemplate>
                                <prop:ItemTemplate>
                                    <tr class="<%#(($this->ItemIndex%2==0)? '':'on')%>">
                                        <com:TActiveHiddenField id="indexItemTranche" value="<%#$this->ItemIndex%>" />
                                        <com:TActiveHiddenField id="idItemTranche" value="<%#$this->Data['idItemTranche']%>" />
                                        <td class="identifiant">
                                            <span><com:TActiveLabel id="typeTranche" Text="<%#$this->Data['typeTranche']%>"/></span>
                                            <div id="afficherIdTranche" style="display:<%#$this->Data['idTranche']== '0' ?'none':''%>" ><com:TTextBox id="idTranche" Attributes.value="<%#$this->Data['idTranche']%>" Enabled="<%=$this->TemplateControl->enabledElement()%>"/></div>
                                        </td>
                                        <td class="intitule-tranche">
                                            <com:TActiveTextBox
                                                    id="intituleTranche"
                                                    Attributes.title="<%=Prado::localize('DEFINE_INTITULE_TRANCHE')%>"
                                                    Enabled="<%=$this->TemplateControl->enabledElement()%>"
                                                    Attributes.value="<%#$this->Data['intituleTranche']%>"
                                            />
                                            <span id="intituleTrancheText_<%#($this->ItemIndex+1)%>" title="Champ obligatoire" style="check-invalide;display:none;"><img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif" alt="Champ obligatoire"></span>


                                        </td>
                                        <td class="forme-prix">
                                            <com:TActiveLabel id="formePrixTranche" Text="<%#$this->Data['formePrix']%>" />
                                            <com:TActiveHiddenField
                                                    id="formePrixTrancheHidden"
                                                    Attributes.value="<%#$this->Data['formePrix']%>"
                                            />
                                            <span id="errorFormePrixTranche_<%#($this->ItemIndex+1)%>" title="Champ obligatoire" style="check-invalide;display:none;"><img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif" alt="Champ obligatoire"></span>
                                            <com:TActiveLinkButton
                                                    style="display:<%=$this->TemplateControl->enabledElement()?'block':'none'%>"
                                                    CssClass="bouton-small"
                                                    ID="linkDefinirActionFormePrixTranche"
                                                    Text ="<%#$this->Data['definir']%>"
                                                    Attributes.title="<%=Prado::localize('DEFINIR')%>"
                                                    OnCallBack="SourceTemplateControl.definirFormePrixTranche"
                                                    ActiveControl.CallbackParameter="<%#$this->TemplateControl->linkDefinirFormePrix->getClientId().'#'.$this->Data['idItemTranche'].'#'%>"
                                            >
                                            </com:TActiveLinkButton>
                                            <com:TActiveImage id="errorFormPrixTC" ImageUrl='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' Attributes.alt='Champ obligatoire' style='display:none'/>
                                        </td>
                                        <td class="estimation" style="display: none;"><com:TActiveLabel id="estimationHtTranche"  Text="<%#$this->Data['estimationHt']%>"/></td>
                                        <td class="actions">
                                            <com:TActiveHiddenField id="deleteTranche" value="<%#$this->Data['supprimerTranche']%>" />
                                            <com:TActiveLinkButton
                                                    id="deleteThisTranche"
                                                    Attributes.title="<%=Prado::localize('DEFINE_SUPPRIMER_TRANCHE')%>"
                                                    Visible="<%#$this->Data['supprimerTranche']%>"
                                                    OnCallBack="SourceTemplateControl.deleteTranche"
                                                    Attributes.onClick="return confirm('<%#Prado::localize('CONFIRM_DELETE_TRANCHE')%>');"
                                                    ActiveControl.CallbackParameter="<%#$this->ItemIndex%>"
                                                    style="display:<%=$this->TemplateControl->enabledElement()?'block':'none'%>"
                                            >
                                                <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-supprimer-big.gif" alt="<%=Prado::localize('DEFINE_SUPPRIMER')%>" title="<%=Prado::localize('DEFINE_SUPPRIMER')%>"/>
                                            </com:TActiveLinkButton>
                                            <div id="deleteTranche" style="display:<%#$this->Data['supprimerTranche']?'none':''%>" >&nbsp;</div>
                                            <com:TActiveHiddenField id="formPrixTC" value="<%#$this->Data['formPrixTC'] ? $this->Data['formPrixTC'] :0 %>"/>
                                        </td>
                                    </tr>
                                </prop:ItemTemplate>
                                <prop:FooterTemplate>
                                    </table>
                                </prop:FooterTemplate>
                            </com:TRepeater>
                        </com:TActivePanel>
                        <com:TActiveLinkButton
                                CssClass="ajout-el"
                                Attributes.title="<%=Prado::localize('DEFINE_AJOUTER_TRANCHE_CONDITIONNELLE')%>"
                                onCallBack="addTrancheMarchePrix"
                                style="display:<%=$this->enabledElement()?'':'none'%>"
                        >
                            <com:TTranslate>DEFINE_AJOUTER_TRANCHE_CONDITIONNELLE</com:TTranslate>
                        </com:TActiveLinkButton>
                    </div>
                    <div class="spacer-small"></div>
                </com:TPanel>
                <!--Fin detail tranches-->
                <!--Debut Ligne Lot technique-->
                <com:TPanel Visible="<%=($this->sourcePage == 'detailLots') ? false : true %>">
                    <div class="line" id="lotsTechniqueChecked">
                        <div class="intitule-auto float-left">
                            <com:TActiveCheckBox
                                    id="lotTech"
                                    Attributes.title="<%=Prado::localize('DEFINE_LOTS_TECHNIQUES')%>"
                                    Attributes.onclick="isCheckedShowDiv(this,'<%=$this->infos_lots_techniques_lot->getClientId()%>');"
                                    CssClass="check"
                                    Enabled="<%=$this->enabledElement()%>"
                            />
                            <label for="marcheReconductible"><com:TTranslate>DEFINE_DETAILS_LOTS_TECHNIQUES</com:TTranslate></label>
                        </div>
                        <div class="col-info-bulle">
                            <img title="Info-bulle" alt="Info-bulle" class="picto-info-intitule pos-t-1" onmouseout="cacheBulle('infoslLotTech')" onmouseover="afficheBulle('infoslLotTech', this)" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" />
                            <div id="infoslLotTech" class="info-bulle" onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();"><div><com:TTranslate>TEXT_INFO_BULLE_LOT_TECHNIQUE</com:TTranslate></div></div>
                        </div>
                    </div>
                </com:TPanel>
                <!--Fin Ligne Lot technique-->






                <!--Fin Ligne Lot technique-->
                <!--Debut detail caracteristiques lots techniques-->
                <div class="caracteristiques-lots-techniques">
                    <!--Debut Lots techniques Lot 1-->
                    <com:TPanel id="infos_lots_techniques_lot" style="display:none" cssClass="detail-tranches-lots">
                        <a class="title-toggle-open" onclick="togglePanel(this,'layer-lot-technique-lot');" href="javascript:void(0);"><com:TTranslate>DEFINE_DETAILS_LOTS_TECHNIQUES</com:TTranslate></a>
                        <com:TActivePanel id="panelListeLotsTechnique">
                            <div id="layer-lot-technique-lot" style="display:block" class="panel-tranche-lot-tech">
                                <table summary="Détails des lots techniques" class="lots-techniques">
                                    <thead>
                                    <tr>
                                        <th class="identifiant"><com:TTranslate>DEFINE_TEXT_IDENTIFIANT</com:TTranslate></th>
                                        <th class="intitule-lot-techniques"><com:TTranslate>DEFINE_INTITULE</com:TTranslate></th>
                                        <th class="small-col"><com:TTranslate>LOT_TECHNIQUE</com:TTranslate><br /><com:TTranslate>DEFINE_PRINCIPAL</com:TTranslate></th>
                                        <th class="appartenance">
                                            <com:TActivePanel id="headAppartenance" style="display:<%=($this->marcheTranche->checked == 'true')? '' :'none'%>">
                                                <com:TTranslate>DEFINE_APPARTENANCE</com:TTranslate><br /><com:TTranslate>DEFINE_AUX_TRANCHES</com:TTranslate>
                                            </com:TActivePanel>
                                        </th>
                                        <th class="actions">&nbsp;</th>
                                    </tr>
                                    </thead>
                                    <com:TRepeater ID="repeaterDetailLotsTechniques">
                                        <prop:ItemTemplate>
                                            <script>
                                                J( ".saisir-appartenance-tranches" ).click(function() {
                                                    J(".modal-appartenance-tranches").dialog({
                                                        modal: true,
                                                        resizable: false,
                                                        closeText:'Fermer',
                                                        title:'<h2>'+this.title+'</h2>',
                                                        dialogClass: 'modal-form popup-small2',
                                                        open: function(){
                                                            jQuery('.ui-widget-overlay').bind('click',function(){
                                                                J(".modal-appartenance-tranches").dialog('close');
                                                            })
                                                        }
                                                    });
                                                });
                                            </script>
                                            <com:TActiveButton
                                                    CssClass="bouton-small saisir-appartenance-tranches"
                                                    ID="apprtenanceTrancheClick"
                                                    style="display:none"
                                            >
                                            </com:TActiveButton>
                                            <tr class="<%#(($this->ItemIndex%2==0)? '':'on')%>">
                                                <td class="identifiant">
                                                    <com:TActiveTextBox
                                                            id="idLotTechnique"
                                                            Attributes.value="<%#$this->Data['idLotTechnique']%>"
                                                            Enabled="<%=$this->TemplateControl->enabledElement()%>"
                                                    />
                                                </td>
                                                <td class="intitule-tranche">
                                                    <com:TActiveTextBox
                                                            id="intituleLotTechnique"
                                                            Attributes.value="<%#$this->Data['intituleLotTechnique']%>"
                                                            Enabled="<%=$this->TemplateControl->enabledElement()%>"
                                                    />
                                                </td>
                                                <td class="small-col">
                                                    <com:TActiveRadioButton
                                                            ID="lotPrincipal"
                                                            cssClass="radio"
                                                            UniqueGroupName="lotTechPrincipal<%=$this->TemplateControl->getIdPanel()%>"
                                                            checked="<%#$this->Data['lotPrincipal']%>"
                                                            Enabled="<%=$this->TemplateControl->enabledElement()%>"
                                                    />
                                                </td>
                                                <td class="appartenance">
                                                    <com:TActivePanel id="trHeadAppartenance" style="display:<%#$this->Data['visibleAppartenance'] ? '' :'none'%>">
                                                        <com:TActiveLinkButton
                                                                CssClass="bouton-small"
                                                                ID="apprtenanceTranche"
                                                                Attributes.title="<%=Prado::localize('DEFINIR')%>"
                                                                OnCallBack="SourceTemplateControl.definirApprtenanceTranche"
                                                                ActiveControl.CallbackParameter="<%#$this->apprtenanceTrancheClick->getClientId().'#'.$this->Data['index'].'#'%>"
                                                                style="display:<%=$this->TemplateControl->enabledElement()?'block':'none'%>"
                                                        >
                                                            <com:TTranslate>DEFINIR</com:TTranslate>
                                                        </com:TActiveLinkButton>
                                                    </com:TActivePanel>
                                                </td>
                                                <td class="actions"><a href="#">
                                                        <com:TActiveHiddenField id="deleteLot" value="<%#$this->Data['index']%>" />
                                                        <com:TActiveLinkButton
                                                                id="deleteLotTechnique"
                                                                Attributes.title="<%=Prado::localize('DEFINE_SUPPRIMER_TRANCHE')%>"
                                                                Visible="<%#$this->Data['index']%>"
                                                                OnCallBack="SourceTemplateControl.deleteLot"
                                                                Attributes.onClick="return confirm('<%#Prado::localize('CONFIRM_DELETE_LOT')%>');"
                                                                ActiveControl.CallbackParameter="<%#$this->Data['index']%>"
                                                                style="display:<%=$this->TemplateControl->enabledElement()?'block':'none'%>"
                                                        >
                                                            <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-supprimer-big.gif" alt="<%=Prado::localize('DEFINE_SUPPRIMER')%>" title="<%=Prado::localize('DEFINE_SUPPRIMER')%>"/>
                                                        </com:TActiveLinkButton>
                                                        <com:TActiveHiddenField ID="idObjtLotTech" value="<%#$this->Data['idObjetLotTechnique']%>" />
                                                </td>
                                            </tr>

                                        </prop:ItemTemplate>
                                    </com:TRepeater>
                                </table>
                                <com:TActiveLinkButton
                                        CssClass="ajout-el"
                                        Attributes.title="<%=Prado::localize('DEFINE_AJOUTER_LOT_TECHNIQUE')%>"
                                        onCallBack="SourceTemplateControl.addLotTechnique"
                                        style="display:<%=$this->enabledElement()?'':'none'%>"
                                >
                                    <com:TTranslate>DEFINE_AJOUTER_LOT_TECHNIQUE</com:TTranslate>
                                </com:TActiveLinkButton>
                            </div>
                        </com:TActivePanel>

                    </com:TPanel>
                    <!--Fin Lots techniques Lot 1-->
                </div>
                <!--Fin Lots techniques-->
                <com:TActiveLabel id="scriptJsLot" />
                <!--Fin detail caracteristiques lots techniques-->
            </div>
            <br>
            <com:TActivePanel Cssclass="line" id="panelContratFUE" visible="<%=Application\Service\Atexo\Atexo_Module::isEnabled('Publicite')%>">
                <div class="intitule col-450"><com:TTranslate>CONTRAT_INSCRIT_PROJET_FINANCE_PAR_FONDS_UNION_EUROPEENNE</com:TTranslate> :</div>
                <div class="content-bloc">
                    <div class="intitule-60">
                        <com:TActiveRadioButton
                                GroupName="contratFUE"
                                id="contratFUEOui"
                                CssClass="radio"
                                Attributes.onclick="isCheckedShowDiv(this, '<%=$this->panelIdentificationProjet->getClientId()%>');"
                        />
                        <label for="<%=$this->contratFUEOui->getClientId()%>"><com:TTranslate>DEFINE_OUI</com:TTranslate></label>
                    </div>
                    <div class="intitule-120">
                        <com:TActiveRadioButton
                                GroupName="contratFUE"
                                id="contratFUENon"
                                CssClass="radio"
                                Attributes.onclick="isCheckedHideDiv(this, '<%=$this->panelIdentificationProjet->getClientId()%>');"
                        />
                        <label for="<%=$this->contratFUENon->getClientId()%>"><com:TTranslate>DEFINE_NON</com:TTranslate></label>
                    </div>
                </div>
            </com:TActivePanel>
            <com:TActivePanel cssClass="line" id="panelIdentificationProjet"  style="display:none;" visible="<%=Application\Service\Atexo\Atexo_Module::isEnabled('Publicite')%>">
                <div class="spacer-small"></div>
                <div class="intitule col-200"><label for="<%=$this->identificationProjet->getClientId()%>"><com:TTranslate>IDENTIFICATION_PROJET_PROGRAMME</com:TTranslate></label> :</div>
                <div class="content-bloc-justificatif">
                    <com:TTextBox id="identificationProjet"
                                  Attributes.title="<%=Prado::Localize('IDENTIFICATION_PROJET_PROGRAMME')%>"
                                  TextMode="MultiLine"
                                  cssClass="long"
                                  Attributes.maxlength="<%= Application\Service\Atexo\Atexo_Config::getParameter('MAXLENGTH_IDENTIFICATION_PROJET_UNION_EUROPEENNE')%>"
                    />
                </div>
            </com:TActivePanel>
            <div class="spacer-small"></div>
        </com:TPanel>
        <div class="breaker"></div>
    </div>
    <com:TActiveLabel id="scriptJs" />
    </div>


    <!--Debut Modal Forme de prix-->
    <div class="modal-form-prix" style="display:none;">
        <com:AtexoValidationSummary  id="validSummaryFormPrix" ValidationGroup="validateFormPrix"  />
        <com:AtexoValidationSummary  id="validSummaryFormPrixM" ValidationGroup="validateFormPrixM"  />
        <com:AtexoValidationSummary  id="validSummaryFormPrixU" ValidationGroup="validateFormPrixU"  />

        <com:TActiveHiddenField ID="idItemToAddFormePrix" />
        <!--Debut Formulaire-->
        <div class="form-bloc">
            <div class="top"><span class="left"></span><span class="right"></span></div>
            <div class="content">
                <div class="line">
                    <div class="intitule intitule-160"><com:TTranslate>TEXT_FORME_DE_PRIX</com:TTranslate><span class="champ-oblig">*</span> :</div>
                    <com:TActiveDropDownList ID="formePrix" AutoPostBack="true" Attributes.onchange="displayOptionChoice(this);" Attributes.title="<%=Prado::localize('TEXT_FORME_DE_PRIX')%>" />
                </div>
            </div>
            <div class="bottom"><span class="left"></span><span class="right"></span></div>
        </div>
        <div id="FVIDE" style="display:block">
        </div>
        <!--Debut bloc Prix forfaitaire-->
        <div id="PF" class="form-bloc" style="display:none">
            <div class="top"><span class="left"></span><span class="right"></span></div>
            <div class="content">
                <div  >
                    <!--Debut Estimation interne-->
                    <div class="line" style="display: none;">
                        <div class="intitule intitule-160"><com:TTranslate>DEFINE_ESTIMATION_INTERNE</com:TTranslate><span class="champ-oblig">*</span> :</div>
                        <div class="content-bloc">
                            <com:TActiveTextBox id="pf_estimationInterneHT" Attributes.title="<%=Prado::localize('DEFINE_ESTIMATION_INTERNE_EUR_HT')%>" CssClass="montant float-left" /><div class="intitule-auto line-height-normal"><label for="pf_estimationInterneHT"><com:TTranslate>DEFINE_EURO_HT</com:TTranslate></label></div>
                            <div class="breaker"></div>
                            <com:TActiveTextBox id="pf_estimationInterneTTC" Attributes.title="<%=Prado::localize('DEFINE_ESTIMATION_INTERNE_EUR_TTC')%>" CssClass="montant float-left" /><div class="intitule-auto line-height-normal"><label for="pf_estimationInterneHT"><com:TTranslate>DEFINE_UNITE_TTC</com:TTranslate></label></div>
                        </div>
                    </div>
                    <com:TRequiredFieldValidator
                            ControlToValidate="pf_estimationInterneHT"
                            ValidationGroup="validateFormPrix"
                            Display="Dynamic"
                            ErrorMessage="<%=Prado::localize('DEFINE_ESTIMATION_INTERNE_EUR_HT')%>"
                            EnableClientScript="true"
                            enabled="false"
                            Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
                        <prop:ClientSide.OnValidationError>
                            document.getElementById('validSummaryFormPrix').style.display='';
                        </prop:ClientSide.OnValidationError>
                    </com:TRequiredFieldValidator>
                    <com:TCompareValidator
                            ControlToValidate="pf_estimationInterneHT"
                            ValueToCompare="0"
                            DataType="Integer"
                            Operator="NotEqual"
                            ValidationGroup="validateFormPrix"
                            Display="Dynamic"
                            ErrorMessage="<%=Prado::localize('DEFINE_ESTIMATION_INTERNE_EUR_HT')%>"
                            EnableClientScript="true"
                            enabled="false"
                            Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
                        <prop:ClientSide.OnValidationError>
                            document.getElementById('validSummaryFormPrix').style.display='';
                        </prop:ClientSide.OnValidationError>
                    </com:TCompareValidator>
                    <!--Fin Estimation interne-->
                    <div class="spacer-small"></div>
                    <!--Debut Variation du Prix-->
                    <div class="line">
                        <com:TActiveHiddenField id="validationControlPf"></com:TActiveHiddenField>
                        <com:TActivePanel ID="panelRepeaterVariationPrixPF">
                            <com:TRepeater ID="repeaterVariationPrixPF">
                                <prop:HeaderTemplate>
                                    <div class="intitule intitule-160"><com:TTranslate>DEFINE_VARIATION_PRIX</com:TTranslate><span class="champ-oblig">*</span> :</div>
                                    <div class="content-bloc check-list">
                                        <div class="intitule-auto clear-both radio-referentiel-values">
                                </prop:HeaderTemplate>
                                <prop:ItemTemplate>
                                    <com:TActiveRadioButton id="pf_variation" UniqueGroupName="variationPrixPF"  /><label for="pf_variation" class="pos-b-2"><%#$this->Data->getLibelleValeurReferentielTraduit()%></label>
                                    <com:TActiveHiddenField id="idPfVariation" value="<%#$this->Data->getId()%>" />
                                    &nbsp;&nbsp;&nbsp;
                                </prop:ItemTemplate>
                                <prop:FooterTemplate>
                    </div>
                </div>
                </prop:FooterTemplate>
                </com:TRepeater>
</com:TActivePanel>
</div>
<com:TCustomValidator
        ValidationGroup="validateFormPrix"
        ControlToValidate="validationControlPf"
        ClientValidationFunction="validateVariationPrix"
        Display="Dynamic"
        ErrorMessage="<%=Prado::localize('DEFINE_VARIATION_PRIX')%>"
        Text="<span title='Champ obligatoire'
				 	class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
        EnableClientScript="true" >
    <prop:ClientSide.OnValidationError>
        document.getElementById('validSummaryFormPrix').style.display='';
    </prop:ClientSide.OnValidationError>
</com:TCustomValidator>



<div class="spacer-small"></div>
<!--Fin Variation du Prix-->
</div>
<div class="breaker"></div>
</div>
<div class="bottom"><span class="left"></span><span class="right"></span></div>
</div>
<!--Fin bloc Prix forfaitaire-->

<!--Debut bloc Prix Unitaire-->
<div id="PU" class="form-bloc" style="display:none">
    <div class="top"><span class="left"></span><span class="right"></span></div>
    <div class="content">
        <div>
            <div class="line">
                <div class="intitule intitule-160"><com:TTranslate>TEXT_PRIX_UNITAIRE</com:TTranslate> :</div>
                <div class="content-bloc-auto check-list">
                    <div class="intitule intitule-160">
                        <com:TActiveRadioButton GroupName="typePrixUnitaire" id="pu_bc" Attributes.title="<%=Prado::localize('DEFINE_BONS_COMMANDE')%>" Attributes.onclick="showDiv('pu_infos-min-max');" checked="true" /><label for="clauseSociale_oui" class="pos-b-2"><com:TTranslate>DEFINE_BONS_COMMANDE</com:TTranslate></label>
                        <img title="Info-bulle"
                             alt="Info-bulle"
                             class="picto-info-intitule pos-t-1"
                             onmouseout="cacheBulle('infosPrixUnitaireBonsCommande')"
                             onmouseover="afficheBulle('infosPrixUnitaireBonsCommande', this)"
                             src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif"
                        />
                        <div id="infosPrixUnitaireBonsCommande"
                             class="info-bulle"
                             onmouseover="mouseOverBulle();"
                             onmouseout="mouseOutBulle();">
                            <div><com:TTranslate>TEXT_INFO_BULLE_PU_BONS_COMMANDE</com:TTranslate></div>
                        </div>
                    </div>
                    <div class="intitule-auto">
                        <com:TActiveRadioButton GroupName="typePrixUnitaire" id="pu_qd" Attributes.title="<%=Prado::localize('FCSP_A_QUANTITE_DEFINIE')%>" Attributes.onclick="hideDiv('pu_infos-min-max');" /><label for="clauseSociale_oui" class="pos-b-2"><com:TTranslate>FCSP_A_QUANTITE_DEFINIE</com:TTranslate></label>
                    </div>
                </div>
            </div>
            <div class="spacer"></div>
            <!--Debut infos MINI MAXI-->
            <div id="pu_infos-min-max">
                <div class="content-bloc-auto col-250 check-list float-left">
                    <div class="line-input">
                        <div class="intitule intitule-160">
                            <com:TActiveRadioButton GroupName="pUAvecSansMinMax" id="pu_bdcAvecMinimaxi" Attributes.title="<%=Prado::localize('DEFINE_AVEC_MIN_MAX')%>" Attributes.onclick="isCheckedShowDiv(this,'pu_bdcAvecMinimaxi_selectUnite');isCheckedShowDiv(this,'pu_avecMiniMaxi');" checked="true" /><label for="pu_bdcAvecMinimaxi" class="pos-b-1"><com:TTranslate>DEFINE_AVEC_MIN_MAX</com:TTranslate></label>
                        </div>
                        <div class="intitule-auto" style="display:block;" id="pu_bdcAvecMinimaxi_selectUnite">
                            <com:TActiveDropDownList ID="puSelectUnite" AutoPostBack="true" onCallBack = "displayPuAvecMinMax"  Attributes.title="<%=Prado::localize('FCSP_UNITE')%>" />
                        </div>
                    </div>
                    <div class="line-input">
                        <div class="intitule intitule-160 clear-both">
                            <com:TActiveRadioButton GroupName="pUAvecSansMinMax" id="pu_bdcSansMinimaxi"  Attributes.title="<%=Prado::localize('DEFINE_SANS_MIN_MAX')%>" Attributes.onclick="isCheckedHideDiv(this,'pu_bdcAvecMinimaxi_selectUnite');isCheckedHideDiv(this,'pu_avecMiniMaxi');" /><label for="pu_bdcSansMinimaxi"><com:TTranslate>DEFINE_SANS_MIN_MAX</com:TTranslate></label>
                        </div>
                    </div>
                    <div class="breaker"></div>
                </div>
                <com:TActivePanel id="panelPuAvecSansMinMax">
                    <div id="pu_avecMiniMaxi" style="display:block;" class="content-bloc-auto float-left">
                        <div class="line" id="pu_unite">
                            <div class="intitule intitule-min-max"><com:TTranslate>FCSP_MIN</com:TTranslate> :</div><com:TActiveTextBox id="puMin" Attributes.title="<%=Prado::localize('FCSP_MIN')%>" CssClass="montant float-left" /><div class="intitule-unite"><com:TActiveLabel ID="puMinLabel"  /></div>
                            <div class="breaker"></div>
                            <div class="intitule intitule-min-max"><com:TTranslate>FCSP_MAX</com:TTranslate> :</div><com:TActiveTextBox id="puMax" Attributes.title="<%=Prado::localize('FCSP_MAX')%>" CssClass="montant float-left" /><div class="intitule-unite"><com:TActiveLabel ID="puMaxLabel"  /></div>
                        </div>
                    </div>
                </com:TActivePanel>
            </div>
            <div class="breaker"></div>
            <!--Fin infos MINI MAXI-->
            <!--Debut Estimation interne-->
            <div class="spacer-small"></div>
            <div class="line" style="display: none;">
                <div class="intitule intitule-160"><com:TTranslate>DEFINE_ESTIMATION_INTERNE</com:TTranslate><br /><com:TTranslate>DEFINE_FACTURE_TYPE</com:TTranslate><span class="champ-oblig">*</span> :</div>
                <div class="content-bloc">
                    <com:TActiveTextBox id="pu_estimationInterneHT" Attributes.title="<%=Prado::localize('DEFINE_ESTIMATION_INTERNE_EUR_HT')%>" CssClass="montant float-left" /><div class="intitule-unite line-height-normal"><label for="pu_estimationInterneHT"><com:TTranslate>DEFINE_EURO_HT</com:TTranslate></label></div>
                    <div class="breaker"></div>
                    <com:TActiveTextBox id="pu_estimationInterneTTC" Attributes.title="<%=Prado::localize('DEFINE_ESTIMATION_INTERNE_EUR_TTC')%>" CssClass="montant float-left" /><div class="intitule-auto line-height-normal"><label for="pu_estimationInterneTTC"><com:TTranslate>DEFINE_UNITE_TTC</com:TTranslate></label></div>
                </div>
                <com:TRequiredFieldValidator
                        ControlToValidate="pu_estimationInterneHT"
                        ValidationGroup="validateFormPrixU"
                        Display="Dynamic"
                        ErrorMessage="<%=Prado::localize('DEFINE_ESTIMATION_INTERNE_EUR_HT')%>"
                        EnableClientScript="true"
                        enabled="false"
                        Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
                    <prop:ClientSide.OnValidationError>
                        document.getElementById('validSummaryFormPrixU').style.display='';
                    </prop:ClientSide.OnValidationError>
                </com:TRequiredFieldValidator>
                <com:TCompareValidator
                        ControlToValidate="pu_estimationInterneHT"
                        ValueToCompare="0"
                        DataType="Integer"
                        Operator="NotEqual"
                        ValidationGroup="validateFormPrixU"
                        Display="Dynamic"
                        ErrorMessage="<%=Prado::localize('DEFINE_ESTIMATION_INTERNE_EUR_HT')%>"
                        EnableClientScript="true"
                        enabled="false"
                        Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
                    <prop:ClientSide.OnValidationError>
                        document.getElementById('validSummaryFormPrixU').style.display='';
                    </prop:ClientSide.OnValidationError>
                </com:TCompareValidator>

            </div>
            <!--Fin Estimation interne-->
            <!--Debut Variation du Prix-->
            <div class="spacer-small"></div>
            <div class="line">
                <com:TActivePanel ID="panelRepeaterVariationPrixPU">
                    <com:TActiveHiddenField id="validationControlPuVariation"/>
                    <com:TRepeater ID="repeaterVariationPrixPU">
                        <prop:HeaderTemplate>
                            <div class="intitule intitule-160"><com:TTranslate>DEFINE_VARIATION_PRIX</com:TTranslate><span class="champ-oblig">*</span> :</div>
                            <div class="content-bloc check-list">
                                <div class="intitule-auto clear-both radio-referentiel-values">
                        </prop:HeaderTemplate>

                        <prop:ItemTemplate>
                            <com:TActiveRadioButton id="pu_variation" UniqueGroupName="variationPrixPU" /><label for="pu_variation" class="pos-b-2"><%#$this->Data->getLibelleValeurReferentielTraduit()%></label>
                            <com:TActiveHiddenField id="idPUVariation" value="<%#$this->Data->getId()%>" />
                            &nbsp;&nbsp;&nbsp;
                        </prop:ItemTemplate>
                        <prop:FooterTemplate>
            </div>
        </div>
        </prop:FooterTemplate>
        </com:TRepeater>
        </com:TActivePanel>
        <com:TCustomValidator
                ValidationGroup="validateFormPrixU"
                ControlToValidate="validationControlPuVariation"
                ClientValidationFunction="validateVariationPrixU"
                Display="Dynamic"
                ErrorMessage="<%=Prado::localize('DEFINE_VARIATION_PRIX')%>"
                Text="<span title='Champ obligatoire'
				 	class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
                EnableClientScript="true" >
            <prop:ClientSide.OnValidationError>
                document.getElementById('validSummaryFormPrixU').style.display='';
            </prop:ClientSide.OnValidationError>
        </com:TCustomValidator>
    </div>
    <!--Fin Variation du Prix-->
    <div class="spacer-small"></div>
    <!--Debut Type de Prix-->
    <div class="line">
        <com:TActivePanel ID="panelRepeaterTypePrixPU">
            <com:TActiveHiddenField id="validationControlPuTypeP"/>
            <com:TRepeater ID="repeaterTypePrixPU">
                <prop:HeaderTemplate>
                    <div class="intitule intitule-160"><com:TTranslate>DEFINE_TYPE_PRIX</com:TTranslate><span class="champ-oblig">*</span> :</div>
                    <div class="content-bloc check-list">
                </prop:HeaderTemplate>

                <prop:ItemTemplate>
                    <div class="intitule-auto clear-both">
                        <com:TActiveCheckBox id="pu_prixCatalogue" /><label for="pu_prixCatalogue" class="pos-b-2"><%#$this->Data->getLibelleValeurReferentielTraduit()%></label>
                        <com:TActiveHiddenField id="idPUTypePrix" value="<%#$this->Data->getId()%>" />
                        <br/>
                    </div>
                </prop:ItemTemplate>
                <prop:FooterTemplate>
    </div>
    </prop:FooterTemplate>
    </com:TRepeater>
    </com:TActivePanel>
    <com:TCustomValidator
            ValidationGroup="validateFormPrixU"
            ControlToValidate="validationControlPuTypeP"
            ClientValidationFunction="validateTypePrixU"
            Display="Dynamic"
            ErrorMessage="<%=Prado::localize('DEFINE_TYPE_PRIX')%>"
            Text="<span title='Champ obligatoire'
				 	class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
            EnableClientScript="true" >
        <prop:ClientSide.OnValidationError>
            document.getElementById('validSummaryFormPrixU').style.display='';
        </prop:ClientSide.OnValidationError>
    </com:TCustomValidator>
</div>
<!--Fin Type de Prix-->
<div class="spacer"></div>
</div>
<!--Fin bloc Prix Unitaire-->
<div class="breaker"></div>
</div>
<div class="bottom"><span class="left"></span><span class="right"></span></div>
</div>


<div id="PM" class="form-bloc" style="display:none">
    <div class="top"><span class="left"></span><span class="right"></span></div>
    <div class="content">
        <!--Debut bloc Prix Mixte-->
        <div class="clear-both">
            <!--Debut bloc Partie Forfaitaire-->
            <h3><com:TTranslate>DEFINE_PARTIE_FORFAITAIRE</com:TTranslate></h3>
            <!--Debut Estimation interne-->
            <div class="line" style="display: none;">
                <div class="intitule intitule-160"><com:TTranslate>DEFINE_ESTIMATION_INTERNE</com:TTranslate> <com:TTranslate>DEFINE_FACTURE_TYPE</com:TTranslate><span class="champ-oblig">*</span> :</div>
                <div class="content-bloc" style="display: none;">
                    <com:TActiveTextBox id="pm_pf_estimationInterneHT" Attributes.title="<%=Prado::localize('DEFINE_ESTIMATION_INTERNE_EUR_HT')%>" CssClass="montant float-left" /><div class="intitule-auto line-height-normal"><label for="pm_pf_estimationInterneHT"><com:TTranslate>DEFINE_EURO_HT</com:TTranslate></label></div>
                    <div class="breaker"></div>
                    <com:TActiveTextBox id="pm_pf_estimationInterneTTC" Attributes.title="<%=Prado::localize('DEFINE_ESTIMATION_INTERNE_EUR_TTC')%>" CssClass="montant float-left" /><div class="intitule-auto line-height-normal"><label for="pm_pf_estimationInterneTTC"><com:TTranslate>DEFINE_UNITE_TTC</com:TTranslate></label></div>
                </div>
                <com:TRequiredFieldValidator
                        ControlToValidate="pm_pf_estimationInterneHT"
                        ValidationGroup="validateFormPrixM"
                        Display="Dynamic"
                        ErrorMessage="<%=Prado::localize('DEFINE_PRIX_FORFAITE').' - '.Prado::localize('DEFINE_ESTIMATION_INTERNE_EUR_HT')%>"
                        EnableClientScript="true"
                        enabled="false"
                        Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
                    <prop:ClientSide.OnValidationError>
                        document.getElementById('validSummaryFormPrixM').style.display='';
                    </prop:ClientSide.OnValidationError>
                </com:TRequiredFieldValidator>
                <com:TCompareValidator
                        ControlToValidate="pm_pf_estimationInterneHT"
                        ValueToCompare="0"
                        DataType="Integer"
                        Operator="NotEqual"
                        ValidationGroup="validateFormPrixM"
                        Display="Dynamic"
                        ErrorMessage="<%=Prado::localize('DEFINE_PRIX_FORFAITE').' - '.Prado::localize('DEFINE_ESTIMATION_INTERNE_EUR_HT')%>"
                        EnableClientScript="true"
                        enabled="false"
                        Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
                    <prop:ClientSide.OnValidationError>
                        document.getElementById('validSummaryFormPrixM').style.display='';
                    </prop:ClientSide.OnValidationError>
                </com:TCompareValidator>
            </div>
            <!--Debut Variation du Prix-->
            <div class="spacer-small"></div>
            <div class="line">
                <com:TActivePanel ID="panelRepeaterVariationPrixPFPM">
                    <com:TActiveHiddenField id="validationControlPMVariation"/>
                    <com:TRepeater ID="repeaterVariationPrixPFPM">
                        <prop:HeaderTemplate>
                            <div class="intitule intitule-160"><com:TTranslate>DEFINE_VARIATION_PRIX</com:TTranslate><span class="champ-oblig">*</span> :</div>
                            <div class="content-bloc check-list">
                                <div class="intitule-auto clear-both radio-referentiel-values">
                        </prop:HeaderTemplate>

                        <prop:ItemTemplate>
                            <com:TActiveRadioButton id="pm_pf_variation" UniqueGroupName="variationPrixPFPM" /><label for="pu_variation" class="pos-b-2"><%#$this->Data->getLibelleValeurReferentielTraduit()%></label>
                            <com:TActiveHiddenField id="idPMPFVariation" value="<%#$this->Data->getId()%>" />
                            &nbsp;&nbsp;&nbsp;
                        </prop:ItemTemplate>
                        <prop:FooterTemplate>
            </div>
        </div>
        </prop:FooterTemplate>
        </com:TRepeater>
        </com:TActivePanel>
        <com:TCustomValidator
                ValidationGroup="validateFormPrixM"
                ControlToValidate="validationControlPMVariation"
                ClientValidationFunction="validationVariationPrixMF"
                Display="Dynamic"
                ErrorMessage="<%=Prado::localize('DEFINE_PRIX_FORFAITE').' - '.Prado::localize('DEFINE_VARIATION_PRIX')%>"
                Text="<span title='Champ obligatoire'
				 	class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
                EnableClientScript="true" >
            <prop:ClientSide.OnValidationError>
                document.getElementById('validSummaryFormPrixM').style.display='';
            </prop:ClientSide.OnValidationError>
        </com:TCustomValidator>
    </div>
    <!--Fin Variation du Prix-->
    <!--Fin bloc Partie Forfaitaire-->
    <!--Debut bloc Partie Unitaire -->
    <div class="separator"></div>
    <h3><com:TTranslate>DEFINE_PARTIE_UNITAIRE</com:TTranslate> :</h3>
    <div class="line">
        <div class="content-bloc-auto check-list">
            <div class="intitule intitule-160">
                <com:TActiveRadioButton GroupName="typePrixUnitaireMixte" id="pm_pu_bc" Attributes.title="<%=Prado::localize('DEFINE_BONS_COMMANDE')%>" Attributes.onclick="showDiv('pm_pu_infos-min-max');" checked="true" /><label for="pm_pu_bc" class="pos-b-2"><com:TTranslate>DEFINE_BONS_COMMANDE</com:TTranslate></label>
                <img title="Info-bulle"
                     alt="Info-bulle"
                     class="picto-info-intitule pos-t-1"
                     onmouseout="cacheBulle('infosPrixMixteBonsCommande')"
                     onmouseover="afficheBulle('infosPrixMixteBonsCommande', this)"
                     src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif"
                />
                <div id="infosPrixMixteBonsCommande"
                     class="info-bulle"
                     onmouseover="mouseOverBulle();"
                     onmouseout="mouseOutBulle();">
                    <div><com:TTranslate>TEXT_INFO_BULLE_PU_BONS_COMMANDE</com:TTranslate></div>
                </div>
            </div>
            <div class="intitule-auto">
                <com:TActiveRadioButton GroupName="typePrixUnitaireMixte" id="pm_pu_qd" Attributes.title="<%=Prado::localize('FCSP_A_QUANTITE_DEFINIE')%>" Attributes.onclick="hideDiv('pm_pu_infos-min-max');" /><label for="pm_pu_qd" class="pos-b-2"><com:TTranslate>FCSP_A_QUANTITE_DEFINIE</com:TTranslate></label>
            </div>
        </div>
    </div>
    <div class="spacer"></div>
    <!--Debut infos MINI MAXI-->
    <!--Debut infos MINI MAXI-->
    <div id="pm_pu_infos-min-max">
        <div class="content-bloc-auto col-250 check-list float-left">
            <div class="line-input">
                <div class="intitule intitule-160">
                    <com:TActiveRadioButton GroupName="pUPMAvecSansMinMax" id="pm_pu_bdcAvecMinimaxi" Attributes.title="<%=Prado::localize('DEFINE_AVEC_MIN_MAX')%>" Attributes.onclick="isCheckedShowDiv(this,'pm_pu_bdcAvecMinimaxi_selectUnite');isCheckedShowDiv(this,'pm_pu_avecMiniMaxi');" checked="true" /><label for="pm_pu_bdcAvecMinimaxi" class="pos-b-2"><com:TTranslate>DEFINE_AVEC_MIN_MAX</com:TTranslate></label>
                </div>
                <div class="intitule-auto" style="display:block;" id="pm_pu_bdcAvecMinimaxi_selectUnite">
                    <com:TActiveDropDownList ID="pmPuSelectUnite" AutoPostBack="true" onCallBack = "displayPmPuAvecMinMax" Attributes.title="<%=Prado::localize('FCSP_UNITE')%>" />
                </div>
            </div>
            <div class="line-input">
                <div class="intitule intitule-160 clear-both">
                    <com:TActiveRadioButton GroupName="pUPMAvecSansMinMax" id="pm_pu_bdcSansMinimaxi" Attributes.title="<%=Prado::localize('DEFINE_SANS_MIN_MAX')%>" Attributes.onclick="isCheckedHideDiv(this,'pm_pu_bdcAvecMinimaxi_selectUnite');isCheckedHideDiv(this,'pm_pu_avecMiniMaxi');" /><label for="pm_pu_bdcSansMinimaxi" class="pos-b-2"><com:TTranslate>DEFINE_SANS_MIN_MAX</com:TTranslate></label>
                </div>
            </div>
            <div class="breaker"></div>
        </div>
        <com:TActivePanel id="panelPmPuAvecSansMinMax">
            <div id="pm_pu_avecMiniMaxi" style="display:block;" class="content-bloc-auto float-left">
                <div class="line" id="pm_pu_unite">
                    <div class="intitule intitule-min-max pos-b-2"><com:TTranslate>FCSP_MIN</com:TTranslate> :</div><com:TActiveTextBox id="pmPuMin" Attributes.title="<%=Prado::localize('FCSP_MIN')%>" CssClass="montant float-left" /><div class="intitule-unite"><com:TActiveLabel ID="pmPuMinLabel"  /></div>
                    <div class="breaker"></div>
                    <div class="intitule intitule-min-max pos-b-2"><com:TTranslate>FCSP_MAX</com:TTranslate> :</div><com:TActiveTextBox id="pmPuMax" Attributes.title="<%=Prado::localize('FCSP_MAX')%>" CssClass="montant float-left" /><div class="intitule-unite"><com:TActiveLabel ID="pmPuMaxLabel"  /></div>
                </div>
            </div>
        </com:TActivePanel>
    </div>
    <!--Fin infos MINI MAXI-->
    <!--Debut Estimation interne-->
    <div class="spacer-small"></div>
    <div class="line" style="display: none;">
        <div class="intitule intitule-160"><com:TTranslate>DEFINE_ESTIMATION_INTERNE</com:TTranslate><br /><com:TTranslate>DEFINE_FACTURE_TYPE</com:TTranslate><span class="champ-oblig">*</span> :</div>
        <div class="content-bloc">
            <com:TActiveTextBox id="pm_pu_estimationInterneHT" Attributes.title="<%=Prado::localize('DEFINE_ESTIMATION_INTERNE_EUR_HT')%>" CssClass="montant float-left" /><div class="intitule-auto line-height-normal"><label for="pm_pu_estimationInterneHT"><com:TTranslate>DEFINE_EURO_HT</com:TTranslate></label></div>
            <div class="breaker"></div>
            <com:TActiveTextBox id="pm_pu_estimationInterneTTC" Attributes.title="<%=Prado::localize('DEFINE_ESTIMATION_INTERNE_EUR_TTC')%>" CssClass="montant float-left" /><div class="intitule-auto line-height-normal"><label for="pm_pu_estimationInterneTTC"><com:TTranslate>DEFINE_UNITE_TTC</com:TTranslate></label></div>
        </div>
        <com:TRequiredFieldValidator
                ControlToValidate="pm_pu_estimationInterneHT"
                ValidationGroup="validateFormPrixM"
                Display="Dynamic"
                ErrorMessage="<%=Prado::localize('TEXT_PRIX_UNITAIRE').' - '.Prado::localize('DEFINE_ESTIMATION_INTERNE_EUR_HT')%>"
                EnableClientScript="true"
                enabled="false"
                Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
            <prop:ClientSide.OnValidationError>
                document.getElementById('validSummaryFormPrixM').style.display='';
            </prop:ClientSide.OnValidationError>
        </com:TRequiredFieldValidator>
        <com:TCompareValidator
                ControlToValidate="pm_pu_estimationInterneHT"
                ValueToCompare="0"
                DataType="Integer"
                Operator="NotEqual"
                ValidationGroup="validateFormPrixM"
                Display="Dynamic"
                ErrorMessage="<%=Prado::localize('TEXT_PRIX_UNITAIRE').' - '.Prado::localize('DEFINE_ESTIMATION_INTERNE_EUR_HT')%>"
                EnableClientScript="true"
                enabled="false"
                Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
            <prop:ClientSide.OnValidationError>
                document.getElementById('validSummaryFormPrixM').style.display='';
            </prop:ClientSide.OnValidationError>
        </com:TCompareValidator>
    </div>
    <!--Fin Estimation interne-->
    <!--Debut Variation du Prix-->
    <div class="spacer-small"></div>
    <div class="line">
        <com:TActivePanel ID="panelRepeaterVariationPrixPUPM">
            <com:TActiveHiddenField id="validationControlPUPM"/>
            <com:TRepeater ID="repeaterVariationPrixPUPM">
                <prop:HeaderTemplate>
                    <div class="intitule intitule-160"><com:TTranslate>DEFINE_VARIATION_PRIX</com:TTranslate><span class="champ-oblig">*</span> :</div>
                    <div class="content-bloc check-list">
                        <div class="intitule-auto clear-both radio-referentiel-values">
                </prop:HeaderTemplate>
                <prop:ItemTemplate>
                    <com:TActiveRadioButton id="pm_pu_variation" UniqueGroupName="variationPrixPUPM" /><label for="pu_variation" class="pos-b-2"><%#$this->Data->getLibelleValeurReferentielTraduit()%></label>
                    <com:TActiveHiddenField id="idPMPUVariation" value="<%#$this->Data->getId()%>" />
                    &nbsp;&nbsp;&nbsp;
                </prop:ItemTemplate>
                <prop:FooterTemplate>
    </div>
</div>
</prop:FooterTemplate>
</com:TRepeater>
</com:TActivePanel>
<com:TCustomValidator
        ValidationGroup="validateFormPrixM"
        ControlToValidate="validationControlPUPM"
        ClientValidationFunction="validateVariationPrixMU"
        Display="Dynamic"
        ErrorMessage="<%=Prado::localize('TEXT_PRIX_UNITAIRE').' - '.Prado::localize('DEFINE_VARIATION_PRIX')%>"
        Text="<span title='Champ obligatoire'
				 	class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
        EnableClientScript="true" >
    <prop:ClientSide.OnValidationError>
        document.getElementById('validSummaryFormPrixM').style.display='';
    </prop:ClientSide.OnValidationError>
</com:TCustomValidator>
</div>
<!--Fin Variation du Prix-->
<div class="spacer-small"></div>
<!--Debut Type de Prix-->
<div class="line">
    <com:TActivePanel ID="panelRepeaterTypePrixPUPM">
        <com:TActiveHiddenField id="validationControlPM"/>
        <com:TRepeater ID="repeaterTypePrixPUPM">
            <prop:HeaderTemplate>
                <div class="intitule intitule-160"><com:TTranslate>DEFINE_TYPE_PRIX</com:TTranslate><span class="champ-oblig">*</span> :</div>
                <div class="content-bloc check-list">
            </prop:HeaderTemplate>

            <prop:ItemTemplate>
                <div class="intitule-auto clear-both">
                    <com:TActiveCheckBox id="pm_pu_prixCatalogue"  /><label for="pm_pu_prixCatalogue" class="pos-b-2"><%#$this->Data->getLibelleValeurReferentielTraduit()%></label>
                    <com:TActiveHiddenField id="idPUPMTypePrix" value="<%#$this->Data->getId()%>" />
                    <br/>
                </div>
            </prop:ItemTemplate>
            <prop:FooterTemplate>
</div>
</prop:FooterTemplate>
</com:TRepeater>
</com:TActivePanel>
<com:TCustomValidator
        ValidationGroup="validateFormPrixM"
        ControlToValidate="validationControlPM"
        ClientValidationFunction="validateTypePrixM"
        Display="Dynamic"
        ErrorMessage="<%=Prado::localize('TEXT_PRIX_UNITAIRE').' - '.Prado::localize('DEFINE_TYPE_PRIX')%>"
        Text="<span title='Champ obligatoire'
				 	class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
        EnableClientScript="true" >
    <prop:ClientSide.OnValidationError>
        document.getElementById('validSummaryFormPrix').style.display='';
    </prop:ClientSide.OnValidationError>
</com:TCustomValidator>
</div>
<!--Fin Type de Prix-->
<div class="spacer"></div>
<!--Fin bloc Partie Unitaire -->
</div>
<!--Fin bloc Prix Mixte-->
<div class="breaker"></div>
</div>
<div class="bottom"><span class="left"></span><span class="right"></span></div>
</div>
<!--Fin Formulaire-->
<!--Debut line boutons-->
<div class="boutons-line">
    <input type="button" class="bouton-moyen float-left" value="<%=Prado::localize('DEFINE_ANNULER')%>" title="<%=Prado::localize('DEFINE_ANNULER')%>" onclick="J('.modal-form-prix').dialog('close');" />
    <div id="PF1" style="display:none">
        <com:TActiveLinkButton
                id="buttonSaveNew"
                ValidationGroup="validateFormPrix"
                CssClass="bouton-moyen float-right"
                Text="<%=Prado::localize('DEFINE_ENREGISTRER')%>"
                OnCallBack="saveFormePrix"
        />
    </div>
    <div id="PU1" style="display:none">
        <com:TActiveLinkButton
                id="buttonSaveNewU"
                ValidationGroup="validateFormPrixU"
                CssClass="bouton-moyen float-right"
                Text="<%=Prado::localize('DEFINE_ENREGISTRER')%>"
                OnCallBack="saveFormePrix"
        />
    </div>
    <div id="PM1" style="display:none">
        <com:TActiveLinkButton
                id="buttonSaveNewM"
                ValidationGroup="validateFormPrixM"
                CssClass="bouton-moyen float-right"
                Text="<%=Prado::localize('DEFINE_ENREGISTRER')%>"
                OnCallBack="saveFormePrix"
        />
    </div>
</div>
<!--Fin line boutons-->
</div>
<!--Debut Modal Forme de prix-->
<com:TActiveHiddenField id="formPrix" />
<!--Debut Modal Appartenance aux tranches-->
<div class="modal-appartenance-tranches" style="display:none;">
    <!--Debut Formulaire-->
    <div class="form-bloc">
        <div class="top"><span class="left"></span><span class="right"></span></div>
        <div class="content">
            <com:TActivePanel ID="panelTrancheRattachement">
                <com:TActiveHiddenField ID="indexLotTechnique" />
                <div class="line">
                    <div class="intitule-line"><strong><com:TTranslate>DEFINE_SELECTIONNE_TRANCHE_RATTACHER_LOT_TECHNIQUE</com:TTranslate> :</strong></div>
                    <div class="content-bloc">
                        <com:TRepeater ID="repeaterTranchesRattachement">
                            <prop:ItemTemplate>
                                <div class="line">
                                    <div class="intitule-auto">
                                        <com:TCheckBox
                                                id="appartenanceTranche"
                                                Attributes.title="<%=Prado::localize('DEFINE_APPARTENANCE')%>." ".<%=Prado::localize('DEFINE_AUX_TRANCHES')%>"
                                        CssClass="check"
                                        AutoPostBack="false"
                                        />
                                        <com:TLabel id="natureTranche" text="<%#$this->Data['typeTranche']%>" />-
                                        <com:TLabel id="idTranche" text="<%#($this->Data['idTranche'] != 0) ? $this->Data['idTranche'] :'' %>" />
                                        <com:TLabel id="intituleTranche" text="<%#$this->Data['intituleTranche']%>" />
                                        <com:TActiveHiddenField ID="indexTranche" value="<%#$this->Data['idItemTranche']%>" />
                                    </div>
                                </div>
                            </prop:ItemTemplate>
                        </com:TRepeater>
                    </div>
                </div>
            </com:TActivePanel>
        </div>
        <div class="bottom"><span class="left"></span><span class="right"></span></div>
    </div>
    <!--Fin Formulaire-->
    <!--Debut line boutons-->
    <div class="boutons-line">
        <input type="button" class="bouton-moyen float-left" value="<%=Prado::localize('DEFINE_ANNULER')%>" title="<%=Prado::localize('DEFINE_ANNULER')%>" onclick="J('.modal-appartenance-tranches').dialog('close');" />
        <com:TActiveLinkButton
                id="saveRattachement"
                CssClass="bouton-moyen float-right"
                Text="<%=Prado::localize('DEFINE_ENREGISTRER')%>"
                Attributes.onclick="J('.modal-appartenance-tranches').dialog('close');"
                OnCallBack="SaveLotTracheRattachement"
        />
    </div>
    <!--Fin line boutons-->
    <com:TLabel ID="scriptLabel" style="display:none" />
    </com:TActivePanel>
    <!--Fin Modal Appartenance aux tranches-->
