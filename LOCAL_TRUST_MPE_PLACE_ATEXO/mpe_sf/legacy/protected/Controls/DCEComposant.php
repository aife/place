<?php

namespace Application\Controls;

use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonProcedureEquivalence;
use Application\Propel\Mpe\CommonRG;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Files;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Redaction;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Dce;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureEquivalence;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Rg;
use Prado\Prado;
use ZipArchive;

/**
 * commentaires.
 *
 * @author  Ezziani Loubna <loubna.ezziani@atexo.com>
 * @copyright Atexo 2012
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class DCEComposant extends MpeTTemplateControl
{
    public $_consultation = '';
    public $_nomFileDce = '';
    public $createZip = false;
    public $_nameNewDCE = '';
    public $_NewZipDCE = '';
    public $_infileRg = '';
    public $_nameRG = '';
    public $_sourceInfoFiles = [];
    public $_fileToScan = '';
    public $useUplodeBar = true;
    public $_dce = '';
    public $statut = '';
    public $_fileAdddedName = '';
    public $_oldfileName = '';
    public $_rg = '';

    public function getConsultation()
    {
        if (!($this->_consultation instanceof CommonConsultation)) {
            $this->_consultation = $this->Page->getConsultation();
        }

        return $this->_consultation;
    }

    public function setConsultation($v)
    {
        $this->_consultation = $v;
    }

    public function getUseUplodeBar()
    {
        return $this->useUplodeBar;
    }

    public function setUseUplodeBar($v)
    {
        $this->useUplodeBar = $v;
    }

    public function refrechCompsant()
    {
        $this->uploadBar->refrechCompsant();
    }

    public function onLoad($param)
    {
        if ($this->getConsultation() instanceof CommonConsultation) {
            $this->gestionAffichageReglementConsultationInfoMsg();
	 		$this->_dce = Atexo_Consultation_Dce::getDce($this->getConsultation()->getId(), Atexo_CurrentUser :: getOrganismAcronym());
	 		$this->_rg = Atexo_Consultation_Rg::getReglement($this->getConsultation()->getId(),  Atexo_CurrentUser::getCurrentOrganism());
			if ($this->_rg instanceof CommonRG) {
				$this->panelDownloadRg->setVisible(true);
				$this->oldDocRg->Text = $this->_rg->getNomFichier();
		 		self::setDownlodRgNavigateUrl($this->getConsultation()->getId());
			}
			$procEquivalence = Atexo_Consultation_ProcedureEquivalence::retrieveProcedureEquivalenceById($this->_consultation->getIdTypeProcedureOrg());
			if (!$this->Page->IsPostBack ){
				if ($procEquivalence instanceof CommonProcedureEquivalence ){
					$atexoProcEqui = new Atexo_Consultation_ProcedureEquivalence();
					$atexoProcEqui->displayElement($this, 'telechargementPartiel', $procEquivalence->getPartialDceDownload());
				}
				if ($this->_consultation->getPartialDceDownload() == '1') {
					$this->telechargementPartiel->Checked = true;
				} else {
					$this->telechargementPartiel->Checked = false;
				}
			}
		}
	}
    
    public function getNewZipDCE($zip_filename){
        $u = new Atexo_Util();
    	$pathTmpDCE= "";
    	$newPathTmpDCE= "";
    	if ($this->remplacerToutDce->Checked == true) {
    		if ((!$this->_dce) || ($this->_dce->getDce() == '0')) {
    			$this->statut = Atexo_Config::getParameter('AJOUT_FILE');
    		} else {
    			$this->statut = Atexo_Config::getParameter('MODIFICATION_FILE');
    		}
    		if($this->uploadBar->hasFile()){
    			if (strtoupper(Atexo_Util::getExtension($this->uploadBar->getFileName())) == 'ZIP') {
					if ($this->verifyDceIntegrity()) {
						$finalDce = Atexo_Config :: getParameter('COMMON_TMP') . "choixDce" . session_id() . time();
						$finalDceLocalName = $this->uploadBar->getLocalName();
						$finalDceName = Atexo_Util::atexoHtmlEntities(
                            (new Atexo_Config())->toPfEncoding($this->uploadBar->getFileName()));
						$this->_nameNewDCE = $finalDceName;
						if (rename($finalDceLocalName, $finalDce)) {
							$newPathTmpDCE = $finalDce;
							$this->_nomFileDce = $this->_fileToScan = $finalDceName;
                            $u->checkDceSize($pathTmpDCE);
							return $newPathTmpDCE;
						}
					}
				}
    		}
    	}elseif ($this->remplacerPieceDce->Checked == true) {
    		//piece libre
    		if($this->fileToScan->Value != ''){
    			 $this->_fileToScan = $this->fileToScan->Value;
    		}
    		$this->_oldfileName = $this->fileToReplace->Value;
    		$pathTmpDCE= $zip_filename;
    		//piece redac
			$sources = explode("#",$this->paramSource->value);
			$indexDestination = $this->paramDestination->value;
			$sourceFiles = array();
			$i = 0;
			$slash = " ";
			$redaction = new Atexo_Redaction($this->getConsultation());
			foreach($sources as $source){
				$inFile = "";
				$fileName = "";
				$filesIn = explode("|",$source);
				if(count($filesIn)){
					$u = new Atexo_Util();
					$fileName = $u->OterAccents($u->replaceCharactersMsWordWithRegularCharacters($u->atexoHtmlEntitiesDecode(Atexo_Config::toPfEncoding($filesIn[1]))));
					if($filesIn[2] == "0"){
						$inFile = $filesIn[0];
					}elseif($filesIn[2] == "1"){//recuperation du doc depuis redac
						$inFile = $redaction->getContentDocument($filesIn[0],$fileName,Atexo_CurrentUser::getCurrentOrganism());
					}
					if($inFile != ""){
						$sourceFiles[$i]["inFile"] = $inFile;
						$sourceFiles[$i]["namefile"] = $fileName;
						if($this->_fileAdddedName){
							$slash = ' / ';
						}
						$this->_fileAdddedName .= $slash.$fileName;
						$i++;
					}
				}
			}
			$zip = new ZipArchive();
			if($this->createZip == false){
	        	$zip->open($zip_filename);
			}else{
				$zip->open($zip_filename,ZipArchive::CREATE);
			}
	        $index = $indexDestination;
			if ($index != Atexo_Config::getParameter('NOUVELLE_PIECE_RACINE_ZIP')) {
				$old_dir = dirname($zip->getNameIndex(-- $index)); // le -- sert à décrémenter l'index qui est décalé d'un cran.
		        ($old_dir != '.')?$old_dir .= '/':$old_dir = ''; // Au cas où on est à la racine du ZIP
		        $zip->deleteIndex($index);
			}else {
	        	$old_dir = "";
		    }
		    $this->_sourceInfoFiles = $sourceFiles;
			foreach($sourceFiles as $sourceFile){
		        $zip->addFile($sourceFile["inFile"], $old_dir . $sourceFile["namefile"]); // on y met le nouveau au même endroit
			}
			if($this->filesToDelete->Value != ""){
				$list = $this->filesToDelete->Value;//$this->Page->getViewState('ListPiecesToDelete');
				$arrayIndexToDelete = explode("#" ,$list);
				foreach($arrayIndexToDelete as $index){
					if($index != ""){//echo $index;exit;
						$index = intval($index);
				        $zip->deleteIndex(--$index); // on enlève l'ancien fichier

					}

				}
			}
			$zip->close();
    		if($this->_oldfileName){
    			$this->statut = Atexo_Config::getParameter('REMPLACEMENT_FILE');
    		} elseif($this->_fileAdddedName) {
    			$this->statut = Atexo_Config::getParameter('AJOUT_FILE');
    		}
    	}
        $u->checkDceSize($pathTmpDCE);
    	return $pathTmpDCE;
    }

    public function getInfoFileRG()
    {
        $inFile = null;
        $infoFileRG = [];
        if ($this->paramDocReglement->value) {
            $dataRG = explode('|', $this->paramDocReglement->value);
            if (count($dataRG)) {
                $fileName = Atexo_Util::atexoHtmlEntities((new Atexo_Config())->toPfEncoding($dataRG[1]));
                if ('0' == $dataRG[2]) {
                    $inFile = $dataRG[0];
                } elseif ('1' == $dataRG[2]) {
                    $redaction = new Atexo_Redaction($this->getConsultation());
                    $inFile = $redaction->getContentDocument($dataRG[0], $fileName, Atexo_CurrentUser::getCurrentOrganism());
                }
                if ('' != $inFile) {
                    $this->_nameRG = $fileName;
                    $infoFileRG['infile'] = $inFile;
                    $infoFileRG['fileName'] = $fileName;
                }
            }
        }

        return $infoFileRG;
    }

    public function verifyDceIntegrity()
    {
        if ($this->uploadBar->hasFile()) {
            $infileDce = $this->uploadBar->getLocalName();
            // tester l'integrite du fichier zip
            // -T revient au zip precedent en cas d'echec
            $cmd = 'nice zip -T '.$infileDce;
            exec($cmd, $output, $sys_answer);
            if (0 != $sys_answer) {
                return false;
            }
        }

        return true;
    }

    public function getInfoFilesAdded()
    {
        $infoAllFiles = [];
        $i = 0;

        if ('' != $this->_nameRG) {
            $pathDirectoryTmpDceFile = Atexo_Config::getParameter('COMMON_TMP').'fileRG/'.session_id().'_'.time().'_'.md5(uniqid(random_int(0, mt_getrandmax()), true)).'/';
            Atexo_Files::createDir($pathDirectoryTmpDceFile);
            $infileRG = $pathDirectoryTmpDceFile.'tmpDceFile';
            Atexo_Blob::copyBlob($this->_infileRg, $infileRG, Atexo_CurrentUser::getCurrentOrganism());
            $infoAllFiles[$i]['pathFile'] = $infileRG;
            $infoAllFiles[$i]['nameFile'] = $this->_nameRG;
            $infoAllFiles[$i]['from'] = 'RG';
            ++$i;
        }
        if ('' != $this->_nameNewDCE) {
            $zip_finalDCE = Atexo_Config::getParameter('COMMON_TMP').'zip_tmp_dce_'.session_id().'_'.md5(uniqid(random_int(0, mt_getrandmax()), true)).'.zip';
            Atexo_Blob::copyBlob($this->_NewZipDCE, $zip_finalDCE, Atexo_CurrentUser::getCurrentOrganism()); // On copie l'ancien DCE dans un fichier temp
            $infoAllFiles[$i]['pathFile'] = $zip_finalDCE;
            $infoAllFiles[$i]['nameFile'] = $this->_nameNewDCE;
            $infoAllFiles[$i]['from'] = 'RemplacerTT';
            ++$i;
        }
        if (is_array($this->_sourceInfoFiles)) {
            foreach ($this->_sourceInfoFiles as $sourceFile) {
                $infoAllFiles[$i]['pathFile'] = $sourceFile['inFile'];
                $infoAllFiles[$i]['nameFile'] = $sourceFile['namefile'];
                $infoAllFiles[$i]['from'] = 'RemplacerPP';
                ++$i;
            }
        }

        return $infoAllFiles;
    }

    public function getTextDce()
    {
        $dce = (new Atexo_Consultation_Dce())->getDce($this->getConsultation()->getId(), Atexo_CurrentUser::getOrganismAcronym());
        if (!$dce) {
            return Prado::localize('AJOUTER_UN_DCE');
        } else {
            return Prado::localize('REMPLACER_TOUT_PIECE_DCE');
        }
    }

    public function getBlocDCE(): bool
    {
        if ($this->getRequest()->getServiceParameter() == "Agent.ChangingConsultation" || !$this->getParameter('ACTIVE_ELABORATION_V2') == '1') {
            return true;
        }
        return false;
    }
    /*
     * Permt de savoir si un fichier est selectioné
     */
    public function hasFile()
    {
        if (true == $this->remplacerToutDce->Checked) {
            if ($this->uploadBar->hasFile()) {
                return true;
            }
        } elseif (true == $this->remplacerPieceDce->Checked) {
            if ($this->_sourceInfoFiles) {
                return true;
            } elseif ($this->fileToScan->Value) {
                return true;
            }
        }

        return false;
    }

    /**
     * Permet de verifier l'extension du DCE.
     *
     * @return bool true si le dce a l'extension zip si non on retourne false
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function verifiyExtensionDce()
    {
        if ($this->uploadBar->hasFile() && 'ZIP' != strtoupper(Atexo_Util::getExtension($this->uploadBar->getFileName()))) {
            return false;
        }

        return true;
    }

    /**
     * permet de recuperer l'url de telechargement du RC.
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function setDownlodRgNavigateUrl($reference)
    {
        $this->linkDownloadRg->NavigateUrl = 'index.php?page=Agent.DownloadReglement&id='.base64_encode($reference);
    }

    /**
     * Permet de gerer l'affichage du message informatif qui rappelle à l'agent qu'il est en procédure répondant au service MPS.
     *
     * @param $consultation: Objet consultation
     *
     * @return void
     */
    private function gestionAffichageReglementConsultationInfoMsg()
    {
        $this->activeInfoMsg->displayStyle = 'None';

        if ($this->_consultation instanceof CommonConsultation
            && Atexo_Module::isEnabled('MarchePublicSimplifie', Atexo_CurrentUser::getCurrentOrganism())
            && 1 == $this->_consultation->getMarchePublicSimplifie()
        ) {
            $this->activeInfoMsg->displayStyle = 'Dynamic';
        }
    }
}
