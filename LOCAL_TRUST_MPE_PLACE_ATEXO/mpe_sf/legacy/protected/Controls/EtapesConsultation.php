<?php

namespace Application\Controls;

use App\Service\Routing\RouteBuilderInterface;
use Application\Propel\Mpe\CommonConsultation;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Exception;

/**
 * Page de gestion des étapes de la consultation.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 4
 */
class EtapesConsultation extends MpeTTemplateControl
{
    private $validationGroup;

    /**
     * Affectes la valeur.
     *
     * @param $value
     */
    public function setValidationGroup($value)
    {
        $this->validationGroup = $value;
    }

    /**
     * Recupére la valeur.
     */
    public function getValidationGroup()
    {
        return $this->validationGroup;
    }

    /**
     * Chargement du composant.
     */
    public function onLoad($param)
    {
        $this->Page->scriptJs->Text = '';
        if (!$this->Page->isPostBack) {
            $this->updatPourcentagePub();
        }
        $activeElaborationV2 = Atexo_Config::getParameter('ACTIVE_ELABORATION_V2');
        if ($this->Page->getConsultation() instanceof CommonConsultation) {
            $this->displayEtapeLots($this->Page->getConsultation()->getAlloti());
            $this->displayEtapeDume($this->Page->getConsultation()->getDumeDemande());
            if (!$activeElaborationV2) {
                $this->displayEtapeDonneesComplementaires($this->Page->getConsultation());
            }
            if (Atexo_Module::isEnabled('Publicite')) {
                $this->displayEtapePublicite($this->Page->getConsultation());
            }
        }
        //Permet de recuperer des informations pour la gestion du message de confirmation
        $this->gestionMessageConfirmation();

        if (!empty($activeElaborationV2) && !$this->isModeCreationSuite()) {
            if (empty($this->Page->getConsultation()->getId())) {
                $queryParams = [];
                if (!empty($_GET['idContrat'])) {
                    $queryParams['contratId'] = $_GET['idContrat'];
                }
                /** @var RouteBuilderInterface $routeBuilder */
                $routeBuilder = Atexo_Util::getSfService(RouteBuilderInterface::class);
                $this->response->redirect($routeBuilder->getRoute('agent_consultation_creation', $queryParams));
            }
            if (!$this->Page->isPostBack) {
                $etape = $this->getRequest()->itemAt('etape');
                $cons = $this->Page->getConsultation();
                if (
                    empty($etape)
                    || 'etapeIdentification' == $etape
                    || !$cons instanceof CommonConsultation
                    || empty($cons->getId())
                ) {
                    $this->goToEtapeIdentification(null, $param);
                } else {
                    $this->afficherEtapeActive($etape, 'step active', null, $param);
                }
            }
        }
    }

    /**
     * Permet de faire le traitement correspondant à l'étape "Identification".
     */
    public function goToEtapeIdentification($sender, $param)
    {
        $activeElaborationV2 = Atexo_Config::getParameter('ACTIVE_ELABORATION_V2');
        if (empty($activeElaborationV2)) {
            $returnValue = $this->goToEtape('etapeIdentification', 'etape first current', $sender, $param);
            $this->afficherMessageErreur($param, $returnValue);
        } else {
            $returnValue = $this->Page->enregistrer(false, 'etapeIdentification', $sender, $param);
            if (is_array($returnValue) && $returnValue['erreur']) {
                return $returnValue;
            }
            if (!$this->Page->isValid) {
                return;
            }
            $this->response->redirect(
                'agent/consultation/modification/' . $this->Page->getConsultation()->getUuid()
            );
        }
    }


    /**
     * Permet de faire le traitement correspondant à l'étape "Données complémentaires".
     */
    public function goToEtapeDonneesComplementaires($sender, $param)
    {
        $cssClass = empty(Atexo_Config::getParameter('ACTIVE_ELABORATION_V2')) ? 'etape etape-high current' : 'step etape-high active';

        $returnValue = $this->goToEtape('etapeDonneesComplementaires', $cssClass, $sender, $param);
        $this->afficherMessageErreur($param, $returnValue);
    }

    /**
     * Permet de faire le traitement correspondant à l'étape "Détails des lots".
     */
    public function goToEtapeDetailsLots($sender, $param)
    {
        $cssClass = empty(Atexo_Config::getParameter('ACTIVE_ELABORATION_V2')) ? 'etape etape-high current' : 'step etape-high active';

        $returnValue = $this->goToEtape('etapeDetailsLots', $cssClass, $sender, $param);
        $this->afficherMessageErreur($param, $returnValue);
    }

    /**
     * Permet de faire le traitement correspondant à l'étape "DUME Acheteur".
     */
    public function goToEtapeDumeAcheteurs($sender, $param)
    {
        $cssClass = empty(Atexo_Config::getParameter('ACTIVE_ELABORATION_V2')) ? 'etape etape-high current' : 'step etape-high active';

        $returnValue = $this->goToEtape('etapeDumeAcheteur', $cssClass, $sender, $param);
        $this->afficherMessageErreur($param, $returnValue);
    }

    /**
     * Permet de faire le traitement correspondant à l'étape "Calendrier".
     */
    public function goToEtapeCalendrier($sender, $param)
    {
        $cssClass = empty(Atexo_Config::getParameter('ACTIVE_ELABORATION_V2')) ? 'etape etape-high current' : 'step etape-high active';

        $returnValue = $this->goToEtape('etapeCalendrier', $cssClass, $sender, $param);
        $this->afficherMessageErreur($param, $returnValue);
    }

    /**
     * Permet de faire le traitement correspondant à l'étape "Documents joints".
     */
    public function goToEtapeDocumentsJoints($sender, $param)
    {
        $cssClass = empty(Atexo_Config::getParameter('ACTIVE_ELABORATION_V2')) ? 'etape etape-high current' : 'step etape-high active';

        $returnValue = $this->goToEtape('etapeDocumentsJoints', $cssClass, $sender, $param);
        $this->afficherMessageErreur($param, $returnValue);
    }

    /**
     * Permet de faire le traitement correspondant à l'étape "Modalités des réponses".
     */
    public function goToEtapeModalitesReponse($sender, $param)
    {
        $cssClass = empty(Atexo_Config::getParameter('ACTIVE_ELABORATION_V2')) ? 'etape etape-high current' : 'step etape-high active';

        $returnValue = $this->goToEtape('etapeModalitesReponse', $cssClass, $sender, $param);
        $this->afficherMessageErreur($param, $returnValue);
        $this->Page->bloc_etapeModalitesReponse->panelenvelopOffre->render($param->getNewWriter());
    }

    /**
     * Permet de faire le traitement correspondant à l'étape "Droits d'accés".
     */
    public function goToEtapeDroitsAcces($sender, $param)
    {
        $last = '';
        if (!Atexo_Module::isEnabled('Publicite')) {
            $last = ' last';
        }
        $cssClass = empty(Atexo_Config::getParameter('ACTIVE_ELABORATION_V2')) ? "etape {$last} current" : "step {$last} active";

        $returnValue = $this->goToEtape('etapeDroitsAcces', $cssClass, $sender, $param);
        $this->afficherMessageErreur($param, $returnValue);
    }

    /**
     * Permet de faire le traitement correspondant à l'étape "Droits d'accés".
     */
    public function goToEtapePublicite($sender, $param)
    {
        $cssClass = empty(Atexo_Config::getParameter('ACTIVE_ELABORATION_V2')) ? 'etape last current' : 'step last active';

        if (Atexo_Config::getParameter('ACTIVE_ELABORATION_V2')) {
            $returnValue = $this->Page->enregistrer(false, 'PubliciteConsultation', $sender, $param);
            if (is_array($returnValue) && $returnValue['erreur']) {
                return $returnValue;
            }
            if (!$this->Page->isValid) {
                return;
            }
            return $this->response->redirect(
                'agent/consultation/modification/' . $this->Page->getConsultation()->getUuid() . '/publicite'
            );
        }
        if (Atexo_Config::getParameter('CONCENTRATEUR_V2')) {
            $returnValue = $this->goToEtape('PubliciteConsultation', $cssClass, $sender, $param);
            $script = "<script>document.getElementById('ctl0_CONTENU_PAGE_bloc_PubliciteConsultationConcentrateurV2_bloc_PubliciteConsultationConcentrateurV2').style.display = 'block';</script>";
            $this->Page->scriptJs->Text = $script;
        } else {
            $returnValue = $this->goToEtape('PubliciteConsultation', $cssClass, $sender, $param);
        }

        $this->afficherMessageErreur($param, $returnValue);
    }

    /**
     * Permet d'afficher l'étape en cours.
     */
    public function afficherEtapeActive($etape, $cssClass, $sender, $param)
    {
        $script = self::masquerTousComposants();
        $script .= "<script>if (document.getElementById('ctl0_CONTENU_PAGE_bloc_".$etape.'_bloc_'.$etape."')) document.getElementById('ctl0_CONTENU_PAGE_bloc_".$etape.'_bloc_'.$etape."').style.display = 'block';</script>";
        $this->Page->scriptJs->Text = $script;

        $this->Page->etapesConsultation->changerClass($cssClass, $etape);
        if ($param) {
            $this->Page->raffraichirFormulaire($sender, $param);
        }
    }

    /**
     * Permet de masquer toutes les étapes.
     */
    public function masquerTousComposants()
    {
        $script = "<script>document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeIdentification_bloc_etapeIdentification').style.display = 'none';</script>";
        if (Atexo_Module::isEnabled('DonneesComplementaires')) {
            $script .= "<script>document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeDonneesComplementaires_bloc_etapeDonneesComplementaires').style.display = 'none';</script>";
        }
        $script .= "<script>document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeDetailsLots_bloc_etapeDetailsLots').style.display = 'none';</script>";
        if (Atexo_Module::isEnabled('InterfaceDume')) {
            $script .= "<script>document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeDumeAcheteur_bloc_etapeDumeAcheteur').style.display = 'none';</script>";
        }
        $script .= "<script>document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeCalendrier_bloc_etapeCalendrier').style.display = 'none';</script>";
        $script .= "<script>document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeDocumentsJoints_bloc_etapeDocumentsJoints').style.display = 'none';</script>";
        $script .= "<script>document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeModalitesReponse_bloc_etapeModalitesReponse').style.display = 'none';</script>";
        $script .= "<script>document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeDroitsAcces_bloc_etapeDroitsAcces').style.display = 'none';</script>";
        if (Atexo_Module::isEnabled('Publicite')) {
            if (Atexo_Config::getParameter('CONCENTRATEUR_V2')) {
                $script .= "<script>document.getElementById('ctl0_CONTENU_PAGE_bloc_PubliciteConsultationConcentrateurV2_bloc_PubliciteConsultationConcentrateurV2').style.display = 'none';</script>";
            } else {
                $script .= "<script>document.getElementById('ctl0_CONTENU_PAGE_bloc_PubliciteConsultation_bloc_PubliciteConsultation').style.display = 'none';</script>";
            }
        }

        return $script;
    }

    /**
     * Permet d'afficher ou masquer une étape.
     *
     * @param string $etape:    l'identifiant de l'étape
     * @param string $cssClass: la css classe de l'étape cliqué
     */
    public function goToEtape($etape, $cssClass, $sender = null, $param = null)
    {
        try {
            //Sauvegarde de l'étape courante dans un composant hidden
            $this->valeurEtapeCourante->value = $etape;
            //Enregistrement de l'étape précedente (lorsqu'on est pas au premier accés à la page)
            $returnValue = $this->Page->enregistrer(false, $etape, $sender, $param);
            if (is_array($returnValue) && $returnValue['erreur']) {
                return $returnValue;
            }
            if (!$this->Page->isValid) {
                return;
            }

            if (Atexo_Config::getParameter('ACTIVE_ELABORATION_V2')) {
                $idCons = "";
                if (
                    $this->Page->getConsultation() instanceof CommonConsultation
                    &&
                    !empty($this->Page->getConsultation()->getId())
                ) {
                    $idCons = '&id='.base64_encode($this->Page->getConsultation()->getId());
                }
                $urlRedirect = "?page=Agent.FormulaireConsultation&fullWidth=true".$idCons
                    .'&etape='.$etape;
                $this->Page->response->redirect($urlRedirect);
            } else {

                //Afficher la prochaine étape
                $this->afficherEtapeActive($etape, $cssClass, $sender, $param);

                //Sauvegarde de l'étape suivante dans un composant hidden
                $this->valeurEtapeSuivante->value = $this->getViewState('etapeCourante');
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }

    /**
     * Permet de changer la class Css lors du clic.
     *
     * @param string $cssClass
     * @param string $idEtape
     */
    public function changerClass($cssClass, $idEtape)
    {
        $this->cssBoutonsNonCliques();
        $this->$idEtape->setCssClass($cssClass);
        //$this->$idEtape->setEnabled('false');
    }

    /**
     * Permet d'affecter la css des boutons non cochés.
     */
    public function cssBoutonsNonCliques()
    {
        $cssClass = empty(Atexo_Config::getParameter('ACTIVE_ELABORATION_V2')) ? 'etape' : 'step';

        $this->etapeIdentification->setCssClass($cssClass . ' first');
        $this->etapeDonneesComplementaires->setCssClass($cssClass . ' etape-high');
        $this->etapeDetailsLots->setCssClass($cssClass);
        $this->etapeDumeAcheteur->setCssClass($cssClass . ' etape-high');
        $this->etapeCalendrier->setCssClass($cssClass);
        $this->etapeDocumentsJoints->setCssClass($cssClass . ' '.Atexo_Config::getParameter('CSS_CLASS_ONGLET_ETAPE_HIGH'));
        $this->etapeModalitesReponse->setCssClass($cssClass . ' '.Atexo_Config::getParameter('CSS_CLASS_ONGLET_ETAPE_HIGH'));
        $this->etapeDroitsAcces->setCssClass($cssClass . ' last');
        if (Atexo_Module::isEnabled('Publicite')) {
            $this->PubliciteConsultation->setCssClass($cssClass . ' last');
        }
    }

    public function displayEtapeLots($allotit)
    {
        if ($allotit) {
            $this->etapeDetailsLots->setDisplay('Dynamic');
            $this->etapeDetailsLotsInactif->setDisplay('None');
        } else {
            $this->etapeDetailsLots->setDisplay('None');
            $this->etapeDetailsLotsInactif->setDisplay('Dynamic');
        }
    }

    public function displayEtapeDume($dumeDemande)
    {
        if (Atexo_Module::isEnabled('InterfaceDume')) {
            if ($dumeDemande) {
                $this->etapeDumeAcheteur->setDisplay('Dynamic');
                $this->etapeDumeAcheteurInactif->setDisplay('None');
            } else {
                $this->etapeDumeAcheteur->setDisplay('None');
                $this->etapeDumeAcheteurInactif->setDisplay('Dynamic');
            }
        } else {
            $this->etapeDumeAcheteur->setVisible(false);
            $this->etapeDumeAcheteurInactif->setVisible(false);
        }
    }

    /**
     * Enter description here ...
     */
    public function gestionMessageConfirmation()
    {
        if (!$this->Page->isPostBack) {
            //Sauvegarde de l'étape
            $this->valeurEtapeCourante->value = 'etapeIdentification';
        }
    }

    /**
     * Permet d'afficher le message d'erreur.
     *
     * @param $param
     * @param $message: le message à afficher
     */
    public function afficherMessageErreur($param, $returnValue)
    {
        if (is_array($returnValue) && true == $returnValue['erreur']) {
            $this->Page->messageErreur->setVisible(true);
            $this->Page->messageErreur->setMessage($returnValue['messageErreur']);
            $this->Page->panelBlocErreur->render($param->getNewWriter());

            return;
        }
    }

    /**
     * Permet d'aller vers l'etape selectionné depuis la popin.
     *
     * @param $sender
     * @param $param
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function goToEtapeFromPopin($sender, $param)
    {
        $etapeTogo = $this->Page->etapesConsultation->etapeToGo->value;
        if (method_exists($this, $etapeTogo)) {
            $this->$etapeTogo($sender, $param);
        }
    }

    public function updatPourcentagePub()
    {
        if (Atexo_Module::isEnabled('Publicite')) {
            //$this->scriptJsPub->Text = "<script type=\"text/javascript\">initPub();   </script>";
        }
    }

    /**
     * Permet de gerer la visibilite des etapes "Donnees Complementaires" et "Donnees complementaires inactives".
     *
     * @param CommonConsultation $consultation : objet consultation
     *                                         author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-les-echos
     *
     * @copyright Atexo 2016
     */
    public function displayEtapeDonneesComplementaires($consultation)
    {
        if (Atexo_Module::isEnabled('DonneesComplementaires')) {
            if (
                $consultation instanceof CommonConsultation
                && !$consultation->getDonneePubliciteObligatoire()
                && !$consultation->getDonneeComplementaireObligatoire()
            ) {
                $this->etapeDonneesComplementaires->setDisplay('None');
                $this->etapeDonneesComplementairesInactif->setDisplay('Dynamic');
            } else {
                $this->etapeDonneesComplementaires->setDisplay('Dynamic');
                $this->etapeDonneesComplementairesInactif->setDisplay('None');
            }
        }
    }

    /**
     * Permet de gerer la visibilite des etapes "Publicite" et "Publicite inactive".
     *
     * @param CommonConsultation $consultation : objet consultation
     *                                         author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-les-echos
     *
     * @copyright Atexo 2016
     */
    public function displayEtapePublicite($consultation)
    {
        if ($consultation instanceof CommonConsultation && '0' == $consultation->getDonneePubliciteObligatoire()) {
            $this->PubliciteConsultation->setDisplay('None');
            $this->PubliciteConsultationInactif->setDisplay('Dynamic');
        } else {
            $this->PubliciteConsultation->setDisplay('Dynamic');
            $this->PubliciteConsultationInactif->setDisplay('None');
        }
    }

    private function isModeCreationSuite(): bool
    {
        return isset($_GET['continuation']) && (1 == $_GET['continuation']);
    }
}