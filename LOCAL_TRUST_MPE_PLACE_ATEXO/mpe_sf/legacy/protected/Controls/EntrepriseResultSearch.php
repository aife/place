<?php

namespace Application\Controls;

use App\Entity\Consultation;
use App\Service\ConsultationTags;
use App\Service\DocumentService;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonCategorieConsultation;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationPeer;
use Application\Propel\Mpe\CommonEntrepriseQuery;
use Application\Propel\Mpe\CommonLangue;
use Application\Propel\Mpe\CommonOrganisme;
use Application\Propel\Mpe\CommonTypeProcedure;
use Application\Propel\Mpe\Entreprise;
use Application\Service\Atexo\Atexo_BourseCotraitance;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_MarchePublicSimplifie;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_MpeSf;
use Application\Service\Atexo\Atexo_MultiDomaine;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Category;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureType;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_PaniersEntreprises;
use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2;
use Prado\Prado;

/**
 * commentaires.
 *
 * @author mouslim MITALI <mouslim.MITALI@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class EntrepriseResultSearch extends MpeTTemplateControl
{
    public string $urlPrefix = '';

    public bool $_baseDce = false;
    public bool $_listeCons = false;
    public int $_nombreElement = 0;

    public function onLoad($param)
    {
        $this->scriptJS->text = '';
        $this->labelJavascript->Text = '';
    }

    public function getBaseDce()
    {
        return $this->_baseDce;
    }

    public function setBaseDce($value)
    {
        $this->_baseDce = $value;
    }

    public function getListeCons()
    {
        return $this->_listeCons;
    }

    public function setListeCons($value)
    {
        $this->_listeCons = $value;
    }

    public function nombreElement()
    {
        return $this->_nombreElement;
    }

    public function getUrlDetailsConsultation($consultation, $orgAcronyme)
    {
        $consultationId = $consultation->getId();
        if ($consultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION')) {
            return Atexo_MpeSf::getUrlDetailsConsultation($consultationId, $orgAcronyme);
        } else {
            $option = ['slash' => true];
            $pfUrl = Atexo_MultiDomaine::getDomaine(Atexo_Config::getParameter('URL_LT_MPE_SF'), $option);

            return $pfUrl.'?page=Entreprise.EntrepriseDetailsConsultation&id='.
                $consultationId.'&'.$orgAcronyme;
        }
    }

    public function fillRepeaterWithDataForSearchResult($criteriaVo)
    {
        $arrayConsultation = null;
        $this->Page->panelResult->setVisible(true);
        $this->Page->rechercheAvancee->setVisible(false);
        $consultation = new Atexo_Consultation();
        if ($criteriaVo->getOrganismeTest()) {
            $arrayConsultation = $consultation->search($criteriaVo);
            $nombreElement = count($arrayConsultation);
        } else {
            $nombreElement = $consultation->search($criteriaVo, true);
        }
        if ($nombreElement >= 1) {
            $this->panelElementsFound->setVisible(true);
            $this->panelNoElementFound->setVisible(false);
            $this->nombreElement->Text = $nombreElement;
            $this->PagerBottom->setVisible(true);
            $this->PagerTop->setVisible(true);
            $this->BtnBottom->setVisible(true);
            $this->setViewState('nombreElement', $nombreElement);
            $this->nombrePageTop->Text = ceil($nombreElement / $this->tableauResultSearch->PageSize);
            $this->nombrePageBottom->Text = ceil($nombreElement / $this->tableauResultSearch->PageSize);
            $this->tableauResultSearch->setVirtualItemCount($nombreElement);
            //$this->tableauResultSearch->setCurrentPageIndex(0);
            if ($criteriaVo->getOrganismeTest()) {
                $this->populateData($criteriaVo, $arrayConsultation);
            } else {
                $this->populateData($criteriaVo);
            }
        } else {
            $this->panelElementsFound->setVisible(false);
            $this->panelNoElementFound->setVisible(true);
            $this->PagerBottom->setVisible(false);
            $this->PagerTop->setVisible(false);
            $this->BtnBottom->setVisible(false);
            $this->resetViewStat();
        }

        return $nombreElement;
    }

    public function populateData(Atexo_Consultation_CriteriaVo $criteriaVo, $arrayConsultation = null)
    {
        $this->resetViewStat();
        if ('' != Atexo_Config::getParameter('PF_URL_DEMAT')) {
            $this->urlPrefix = Atexo_Config::getParameter('PF_URL_DEMAT');
        }
        $offset = $this->tableauResultSearch->CurrentPageIndex * $this->tableauResultSearch->PageSize;
        $limit = $this->tableauResultSearch->PageSize;
        if ($offset + $limit > $this->tableauResultSearch->getVirtualItemCount()) {
            $limit = $this->tableauResultSearch->getVirtualItemCount() - $offset;
        }
        $consultation = new Atexo_Consultation();
        $criteriaVo->setOffset($offset);
        $criteriaVo->setLimit($limit);
        Atexo_CurrentUser::writeToSession('criteriaCons', $criteriaVo);
        if (null != $arrayConsultation && (is_countable($arrayConsultation) ? count($arrayConsultation) : 0) > 0) {
            $arrayConsultationResults = array_slice($arrayConsultation, $offset, $limit);
        } else {
            $consultation = new Atexo_Consultation();
            $arrayConsultationResults = $consultation->search($criteriaVo);
        }
        $this->tableauResultSearch->DataSource = $arrayConsultationResults;
        $this->tableauResultSearch->DataBind();
    }

    public function goToPage($sender, $param)
    {
        $numPage = null;
        switch ($sender->ID) {
            case 'DefaultButtonTop':
                $numPage = $this->numPageTop->Text;
                break;
            case 'DefaultButtonBottom':
                $numPage = $this->numPageBottom->Text;
                break;
        }
        if (Atexo_Util::isEntier($numPage)) {
            if ($numPage >= $this->nombrePageTop->Text) {
                $numPage = $this->nombrePageTop->Text;
            } elseif ($numPage <= 0) {
                $numPage = 1;
            }
            self::updatePagination($this->listePageSizeTop->getSelectedValue(), $numPage);
            $criteriaVo = $this->Page->getViewState('CriteriaVo');
            $this->populateData($criteriaVo);
        } else {
            $this->numPageTop->Text = $this->tableauResultSearch->CurrentPageIndex + 1;
            $this->numPageBottom->Text = $this->tableauResultSearch->CurrentPageIndex + 1;
        }
    }

    public function changePagerLenght($sender, $param)
    {
        $pageSize = null;
        switch ($sender->ID) {
            case 'listePageSizeBottom':
                $pageSize = $this->listePageSizeBottom->getSelectedValue();
                $this->listePageSizeTop->setSelectedValue($pageSize);
                break;

            case 'listePageSizeTop':
                $pageSize = $this->listePageSizeTop->getSelectedValue();
                $this->listePageSizeBottom->setSelectedValue($pageSize);
                break;
        }

        self::updatePagination($pageSize, $this->numPageTop->Text);
        $criteriaVo = $this->Page->getViewState('CriteriaVo');
        $this->populateData($criteriaVo);
    }

    public function pageChanged($sender, $param)
    {
        $this->tableauResultSearch->CurrentPageIndex = $param->NewPageIndex;
        $this->numPageBottom->Text = $param->NewPageIndex + 1;
        $this->numPageTop->Text = $param->NewPageIndex + 1;
        $criteriaVo = $this->Page->getViewState('CriteriaVo');
        $this->populateData($criteriaVo);
    }

    public function insertNewLineIfTextTooLong($texte, $everyHowMuchCaracter)
    {
        return $texte; //wordwrap($texte, $everyHowManyCaracter, "<br>", true);
    }

    public function sortParam($sender, $param): void
    {
        $fieldName = $param->getCommandName();

        $this->setViewState('sortByElement', $fieldName);

        $criteriaVo = $this->Page->getViewState('CriteriaVo');
        $criteriaVo->setSortByElement($fieldName);
        $arraySensTri = $this->Page->getViewState('sensTriArray', []);

        $orderBy = ('ASC' == $arraySensTri[$fieldName]) ? 'DESC' : 'ASC';
        $arraySensTri[$fieldName] = $orderBy;

        $criteriaVo->setSensOrderBy($orderBy);

        $this->Page->setViewState('sensTriArray', $arraySensTri);
        $this->Page->setViewState('CriteriaVo', $criteriaVo);

        $this->tableauResultSearch->setCurrentPageIndex(0);
        $this->populateData($criteriaVo);
    }

    public function elementSortedWith($panelName, $openCloseTag)
    {
        if ($this->getViewState('sortByElement') == $panelName) {
            if ('open' == $openCloseTag) {
                return '<strong>';
            } else {
                return '</strong>';
            }
        } else {
            return '';
        }
    }

    public function truncate($texte, $maximumNumberOfCaracterToReturn = null)
    {
        $truncatedText = null;
        if (!$maximumNumberOfCaracterToReturn) {
            $maximumNumberOfCaracterToReturn = 230;
        }
        $arrayText = explode(' ', $texte);
        $indexArrayText = 0;
        $textlenght = strlen($arrayText[$indexArrayText]) + 1;
        $truncatedText .= $arrayText[$indexArrayText];
        while (isset($arrayText[$indexArrayText]) && $textlenght <= $maximumNumberOfCaracterToReturn) {
            ++$indexArrayText;
            if ($textlenght + (strlen($arrayText[$indexArrayText]) + 1) > $maximumNumberOfCaracterToReturn) {
                break;
            }
            $textlenght += strlen($arrayText[$indexArrayText]) + 1;
            $truncatedText .= ' '.$arrayText[$indexArrayText];
        }

        return $truncatedText;
    }

    public function truncateLieuExec($texte)
    {
        $truncatedText = '';
        $arrayText = explode(',', $texte);
        $index = 0;
        foreach ($arrayText as $oneText) {
            if (2 == $index) {
                break;
            }
            $truncatedText .= $oneText.'<br />';
            ++$index;
        }

        return $truncatedText;
    }

    public function isLieuExecTruncated($texte)
    {
        $arrayText = explode(',', $texte);

        return (count($arrayText) > 2) ? '' : 'display:none';
    }

    public function isTextTruncated($texte, $nbrCaractere = 100)
    {
        return (strlen($texte) > $nbrCaractere) ? 'true' : 'false';
    }

    public function searchAnnonce()
    {
        if ((isset($_GET['AllAnn']))
             || (isset($_GET['AvisInformation']))
              || (isset($_GET['AvisAttribution']))
              || (isset($_GET['AvisExtraitPV']))
              || (isset($_GET['AvisRapportAchevement']))
              || (isset($_GET['AvisdecisionResiliation']))) {
            return true;
        } elseif ((isset($_GET['AllCons']))
             || (!(isset($_GET['searchAnnCons'])))) {
            return false;
        }
    }

    /** Pour inserer un retour a la ligne (<br>), tous les 22 caracteres, dans une chaine.
     * @param : $texte : texte dans lequel sera ajouter les <br>
     *
     * @return : retourn la chaine apres y avoir inserer un retour a la ligne tous les $everyHowManyCaracter caracteres
     */
    public function insertBrWhenTextTooLong($texte, $everyHowManyCaracter)
    {
        return wordwrap($texte, $everyHowManyCaracter, '<br/>', true);
    }

    public function showLanguage($commonConsultation, $objetLangueIndexee)
    {
        $arrayActiveLangages = Atexo_Languages::retrieveArrayActiveLangages();

        $sessionLang = Atexo_CurrentUser::readFromSession('lang');
        if (isset($_GET['lang']) && in_array($_GET['lang'], $arrayActiveLangages)) {
            $langue = Atexo_Util::atexoHtmlEntities($_GET['lang']);
        } elseif (isset($sessionLang) && in_array($sessionLang, $arrayActiveLangages)) {
            $langue = $sessionLang;
        } else {
            $langue = Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE');
        }
        $functionAccessibiliteName = self::getAccessibiliteFunctionName($langue);
        if ($commonConsultation instanceof CommonConsultation && method_exists($commonConsultation, $functionAccessibiliteName) && '1' == $commonConsultation->$functionAccessibiliteName()) {
            $langueCalculee = $langue;
        } else {
            $langueCalculee = Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE');
        }

        return $this->getCssDrapeauLangue($langueCalculee, $objetLangueIndexee);
    }

    /**
     * @author Oumar KONATE <oumar.konate@atexo.com>
     * Permet de determiner la classe css du drapeau de selection des langues dans la consultation
     *
     * @param string       $langueReference:    acronyme de langue que l'on considere comme langue de reference
     * @param CommonLangue $objetLangueIndexee: objet langue indexee dans la tableau d'affichage (ID="traductionConsultations") des langues de la consultation
     * @param int          $itemIndex
     */
    public function getCssDrapeauLangue($langueReference, $objetLangueIndexee)
    {
        if ($objetLangueIndexee instanceof CommonLangue) {
            $langueSelectionnee = $objetLangueIndexee->getLangue();
        }
        if ($langueReference == $langueSelectionnee) {
            return 'on';
        }

        return '';
    }

    /**
     * @author Oumar KONATE <oumar.konate@atexo.com>
     * Permet de determiner l'acronyme de la langue en fonction de l'index du tableau
     * Le but de cette fonction est de pouvoir recuperer la langue lorsqu'on la perd dans le tableau d'affichage (ID="traductionConsultations") apres le raffraichissement
     *
     * @param int $itemIndex: index du tableau
     */
    public function getAcronymeLangueByItemIndex($itemIndex)
    {
        $listeLanguesActives = Atexo_Languages::retrieveActiveLangages();
        $langueActiveIndexee = $listeLanguesActives[$itemIndex];
        if ($langueActiveIndexee instanceof CommonLangue) {
            return $langueActiveIndexee->getLangue();
        }
    }

    public function getAccessibiliteFunctionName($langue)
    {
        return 'getAccessibilite'.Atexo_Languages::getLanguageAbbreviation($langue);
    }

    public function showLangagesFlags()
    {
        return Atexo_Module::isEnabled('ChoixLangueAffichageConsultation');
    }

    public function getLibelleTypeProcedure($itemIndex, $consultation)
    {
        $typeProcedure = $this->getTypeProcedureConsPourTraduction($consultation);
        $langue = $this->getLangueTraductionConsultation($itemIndex);
        if ($typeProcedure instanceof CommonTypeProcedure) {
            $getLibelleTypeProcedure = 'getLibelleTypeProcedure'.Atexo_Languages::getLanguageAbbreviation($langue);
            if ($langue == Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE') || !method_exists($typeProcedure, $getLibelleTypeProcedure)
                || !$typeProcedure->$getLibelleTypeProcedure()) {
                return $typeProcedure->getLibelleTypeProcedure();
            } else {
                return $typeProcedure->$getLibelleTypeProcedure();
            }
        }
    }

    public function getDateProcedure($itemIndex, $consultation)
    {
        $date = [];
        $date['date'] = $consultation->getDateMiseEnLigneCalcule('Y-m-d H:i:s');
        $date['num_month'] = date('n', strtotime($date['date']));
        $date['day'] = date('j', strtotime($date['date']));
        $date['month'] = Atexo_Util::getMonth()[$date['num_month']];
        $date['month_abbreviation'] = Atexo_Util::getMonthAbbreviation()[$date['num_month']];
        $date['year'] = date('Y', strtotime($date['date']));

        return $date;
    }

    public function getDeadline($_date)
    {
        $date = [];
        $date['date'] = $_date;
        $date['num_month'] = date('n', strtotime($_date));
        $date['day'] = date('j', strtotime($_date));
        $date['month'] = Atexo_Util::getMonth()[$date['num_month']];
        $date['month_abbreviation'] = Atexo_Util::getMonthAbbreviation()[$date['num_month']];
        $date['year'] = date('Y', strtotime($_date));
        $date['heure'] = date('H', strtotime($_date));
        $date['minutes'] = date('i', strtotime($_date));
        $date['heure_minutes'] = $date['heure'].':'.$date['minutes'];

        return $date;
    }

    public function isAddToCart($itemIndex, $consultation)
    {
        if (Atexo_Module::isEnabled('PanierEntreprise')
            && !isset($_GET['panierEntreprise'])
            && !$this->searchAnnonce()
            && !$consultation->isInPanier(Atexo_CurrentUser::getIdEntreprise(), Atexo_CurrentUser::getIdInscrit())
            && !$this->getListeCons()
        ) {
            return false;
        }

        return true;
    }

    public function getCategorieConsultation($itemIndex, $consultation)
    {
        $langue = $this->getLangueTraductionConsultation($itemIndex);
        $categorie = $this->getCategorieConsPourTraduction($consultation);
        if ($categorie instanceof CommonCategorieConsultation) {
            $getLibelle = 'getLibelle'.Atexo_Languages::getLanguageAbbreviation($langue);
            if ($langue == Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE') || !method_exists($categorie, $getLibelle)
                || !$categorie->$getLibelle()) {
                return $categorie->getLibelle();
            } else {
                return $categorie->$getLibelle();
            }
        }
    }

    /**
     * Permet de recuperer la categorie de la consultation pour les besoins de la traduction de la consultation.
     *
     * @param CommonConsultation $consultation
     * @return: categorie de la consultation
     */
    public function getCategorieConsPourTraduction($consultation)
    {
        if ($this->getViewState('refresh_consultation') instanceof CommonConsultation) {
            $consultation = $this->getViewState('refresh_consultation');
        }
        if ($consultation instanceof CommonConsultation) {
            $idCategorie = $consultation->getCategorie();
        }

        return Atexo_Consultation_Category::retrieveCategorie($idCategorie, true);
    }

    /**
     * Permet de recuperer le type de procedure de la consultation pour besoin de la traduction.
     *
     * @param CommonConsultation $consultation
     * @return: type de procedure de la consultation
     */
    public function getTypeProcedureConsPourTraduction($consultation)
    {
        if ($this->getViewState('refresh_consultation') instanceof CommonConsultation) {
            $consultation = $this->getViewState('refresh_consultation');
        }
        if ($consultation instanceof CommonConsultation) {
            $arrayTypeProcedures = Atexo_Consultation_ProcedureType::retrieveProcedureType(true, true);

            return $arrayTypeProcedures[$consultation->getIdTypeProcedure()];
        }
    }

    public function getIntituleConsultation($itemIndex, $commonConsultation)
    {
        if ($this->getViewState('refresh_consultation') instanceof CommonConsultation) {
            $commonConsultation = $this->getViewState('refresh_consultation');
        }
        if ($commonConsultation instanceof CommonConsultation) {
            $langue = $this->getViewState('refresh_langue');
            if ($langue && $itemIndex == $this->getViewState('refresh_itemIndex')) {
                return $commonConsultation->getIntituleTraduit($langue);
            } else {
                return $this->getLibelleIntituleParDefaut($commonConsultation, $langue);
            }
        }
    }

    /**
     * Permet de determiner le libelle a afficher par deaut en fonction de la langue de la session.
     *
     * @param CommonConsultation $consultation: objet consultation
     * @param string             $langue:       acronyme de la langue
     */
    public function getLibelleIntituleParDefaut($consultation, $langue)
    {
        if ($consultation instanceof CommonConsultation) {
            $langue = Atexo_Languages::readLanguageFromSession();
            if ($langue == Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE') || !$consultation->getIntituleTraduit($langue)) {
                return $consultation->getIntitule();
            } else {
                return $consultation->getIntituleTraduit($langue);
            }
        }
    }

    public function getResumeConsulation($itemIndex, $commonConsultation)
    {
        if ($this->getViewState('refresh_consultation') instanceof CommonConsultation) {
            $commonConsultation = $this->getViewState('refresh_consultation');
        }
        if ($commonConsultation instanceof CommonConsultation) {
            $langue = $this->getViewState('refresh_langue');
            if ($langue && $itemIndex == $this->getViewState('refresh_itemIndex')) {
                return $commonConsultation->getObjetTraduit($langue);
            } else {
                return $this->getLibelleResumeParDefaut($commonConsultation, $langue);
            }
        }
    }

    /**
     * Permet de determiner le libelle de l'objet de la consultation a afficher par defaut en fonction de la langue de la session.
     *
     * @param CommonConsultation $consultation: objet consultation
     * @param string             $langue:       acronyme de la langue
     */
    public function getLibelleResumeParDefaut($consultation, $langue)
    {
        if ($consultation instanceof CommonConsultation) {
            $langue = Atexo_Languages::readLanguageFromSession();
            if ($langue == Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE') || !$consultation->getObjetTraduit($langue)) {
                return $consultation->getObjet();
            } else {
                return $consultation->getObjetTraduit($langue);
            }
        }
    }

    public function showFlag($langue, $commonConsultation)
    {
        if ($langue == Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) {
            return 'true';
        }
        $functionAccessibiliteName = self::getAccessibiliteFunctionName($langue);
        if ($commonConsultation instanceof CommonConsultation && method_exists($commonConsultation, $functionAccessibiliteName) && '1' == $commonConsultation->$functionAccessibiliteName()) {
            return 'true';
        } else {
            return 'false';
        }
    }

    public function getLibelleLieuxExecution($itemIndex, $commonConsultation)
    {
        if ($this->getViewState('refresh_consultation') instanceof CommonConsultation) {
            $commonConsultation = $this->getViewState('refresh_consultation');
        }
        $langue = $this->getLangueTraductionConsultation($itemIndex);
        if ($commonConsultation instanceof CommonConsultation) {
            $arIds = explode(',', $commonConsultation->getLieuExecution());

            return Atexo_Geolocalisation_GeolocalisationN2::getLibelleLieuExecution($arIds, $langue);
        }
    }

    /**
     * Permet de retourner la denomination de l'organisme.
     *
     * @param int                $itemIndex          l'index de l'element
     * @param CommonConsultation $commonConsultation la consultation
     * @param bool               $annonceMarche      c'est une annonce la valeur est true si c'est une consultation la valeur est false
     *
     * @return string la denomination de l'organisme
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getDenominationOrganisme($itemIndex, $commonConsultation, $annonceMarche = false)
    {
        if ($this->getViewState('refresh_consultation') instanceof CommonConsultation) {
            $commonConsultation = $this->getViewState('refresh_consultation');
        }
        $langue = $this->getLangueTraductionConsultation($itemIndex);
        if ($commonConsultation instanceof CommonConsultation) {
            if (Atexo_Module::isEnabled('AfficherRattachementService') && !empty($commonConsultation->getServiceId())) {
                $servicePath = (new Atexo_EntityPurchase())->getEntityPathById(
                    $commonConsultation->getServiceId(),
                    $commonConsultation->getOrganisme(),
                    true
                );
                return $commonConsultation->getOrgDenomination() . $servicePath;
            } else {
                if ($commonConsultation->getOrgDenomination() && (Atexo_Module::isEnabled('RechercheAutoCompletion') || (Atexo_Module::isEnabled('AnnoncesMarches') && $annonceMarche))) {
                    return $commonConsultation->getOrgDenomination();
                }

                return $commonConsultation->getDenominationOrganisme($langue, $withCPVille = true);
            }
        }
    }

    /*
     * retourne la liste des langues actives sous forme de chaine de caractere qui sera passe a une fonction javascript
     */
    public function listeLanguesActives()
    {
        $langueActives = Atexo_Languages::retrieveArrayActiveLangages();
        if ($langueActives) {
            return implode('/', $langueActives);
        }
    }

    public function truncateCodesNuts($arrayCodesNuts)
    {
        $truncatedText = '';
        $index = 0;
        foreach ($arrayCodesNuts as $oneCodeNut) {
            if (2 == $index) {
                break;
            }
            $truncatedText .= $oneCodeNut.'<br />';
            ++$index;
        }

        return $truncatedText;
    }

    public function isCodesNutsTruncated($arrayCodesNuts)
    {
        return ((is_countable($arrayCodesNuts) ? count($arrayCodesNuts) : 0) > 2) ? true : false;
    }

    public function getLibelleCodesNuts($arrayCodesNuts)
    {
        $libellesNuts = null;
        foreach ($arrayCodesNuts as $oneCodeNut) {
            $libellesNuts .= $oneCodeNut.'<br />';
        }

        return $libellesNuts;
    }

    public function getMessageAddMyCart($referenceUtilisateur)
    {
        $message = str_replace('[__REF_UTILISATEUR_]', '<strong>'.${$referenceUtilisateur}.'</strong>', Prado::localize('CONFIRMATION_ENREGISTREMENT_CONSULTATION_PANIER'));

        return $message;
    }

    public function getUrlAjoutPanier($reference, $organisme)
    {
        if (Atexo_CurrentUser::isConnected()) {
            return "javascript:popUpSetSize('index.php?page=Entreprise.popUpGestionPanier&id=".base64_encode($reference).'&orgAcronyme='.$organisme."&action=ajout','700px','80px','yes');";
        } else {
            return 'index.php?page=Entreprise.EntrepriseHome&goto='.urlencode('?page=Entreprise.EntrepriseAccueilAuthentifie');
        }
    }

    /**
     * Permet de construire l'url des details de la consultation dans le cas du panier entreprise.
     *
     * @param $consultation: l'objet consultation
     * @param $string: permet de preciser s'il s'agit de retrait, question, depot ou echange
     *
     * @return l'url des details de la consultation
     */
    public function getUrlDetailConsultationPanier($consultation, $string)
    {
        if ('' != $consultation->getUrlConsultationExterne() && $consultation->getConsultationExterne()) {
            return $consultation->getUrlConsultationExterne();
        } elseif ('' != $consultation->getUrlConsultationAvisPub() && $consultation->getConsultationExterne()) {
            return $consultation->getUrlConsultationAvisPub();
        } else {
            $extras = '';
            $ancre = null;
            if ('retraits' == $string) {
                $extras = 'orgAcronyme='.$consultation->getOrganisme().'&code='.$consultation->getCodeProcedure().'&retraits';
            } elseif ('questions' == $string) {
                $extras = 'orgAcronyme='.$consultation->getOrganisme().'&code='.$consultation->getCodeProcedure().'&questions';
                $ancre = '#question';
            } elseif ('depots' == $string) {
                $extras = 'orgAcronyme='.$consultation->getOrganisme().'&code='.$consultation->getCodeProcedure().'&depots';
                $ancre = '#depot';
            } elseif ('echanges' == $string) {
                $extras = 'orgAcronyme='.$consultation->getOrganisme().'&code='.$consultation->getCodeProcedure().'&echanges';
                $ancre = '#messagerie';
            }
            if ($consultation->getDateFin() < date('Y-m-d H:i:s')) {
                $extras .= '&clotures';
            }

            if (null !== $ancre) {
                $extras .= $ancre;
            }

            return Atexo_MpeSf::getUrlDetailsConsultation($consultation->getId(), $extras);
        }
    }

    public function getUrlLogo($consultation)
    {
        $urlLogoCons = $consultation->getUrl();
        $pathLogoLocal = Atexo_Config::getParameter('BASE_ROOT_DIR').'/'.$consultation->getOrganisme().Atexo_Config::getParameter('PATH_ORGANISME_IMAGE').'logo-organisme-petit.jpg';
        $urlLogoLocal = Atexo_Util::getUrlLogoOrganisme($consultation->getOrganisme());
        if ('1' == Atexo_Config::getParameter('LOGO_ORGANISME_LOCAL_PRIORITAIRE')) {
            $urlLogo = $urlLogoLocal;
            if (!is_file($pathLogoLocal) && $urlLogoCons) {
                $urlLogo = $urlLogoCons;
            }
        } else {
            $urlLogo = $urlLogoLocal;
            if ($urlLogoCons) {
                $urlLogo = $urlLogoCons;
            }
        }

        return $urlLogo;
    }

    public function calledFromEntreprise()
    {
        if ('Entreprise.EntrepriseAdvancedSearch' == Atexo_Util::atexoHtmlEntities($_GET['page'])) {
            return true;
        }

        return false;
    }

    public function sendCourrielPartage($sender, $param)
    {
        $commonConsultation = $param->CommandParameter;
        if ($commonConsultation instanceof CommonConsultation) {
            if (Atexo_Module::isEnabled('AnnoncesMarches')) {
                $organismeName = $commonConsultation->getOrgDenomination();
            } else {
                $organismeO = Atexo_Organismes::retrieveOrganismeByAcronyme($commonConsultation->getOrganisme());
                if ($organismeO instanceof CommonOrganisme) {
                    $organismeName = $organismeO->getSigle().' - '.$organismeO->getDenominationOrgTraduit();
                }
            }

            $subject = prado::localize('TEXT_INTERET_POUR_LA_CONSULTATION').' '.$commonConsultation->getReferenceUtilisateur().' - '.$commonConsultation->getIntituleTraduit().' ( '.$organismeName.' )';
            $subject = addslashes(Atexo_Util::replaceCharactersMsWordWithRegularCharacters($subject));
            //$subject = Atexo_Util::atexoHtmlEntities($subject)."&";
            $subject .= '&';

            $sautDeLigne = '
';
            $body = (new Atexo_Message())->getInfoPartagerConsultation($commonConsultation);
            $body = str_replace($sautDeLigne, '%0D%0A%0D%0A', $body);
            $body = str_replace("\r", '', $body);
            $body = str_replace("\n", '%0D%0A%0D%0A', $body);
            //$body = Atexo_Util::atexoHtmlEntities($body);

            $javascript = "<script>sendMailPartage('".$subject."','".$body."');</script>";
            $this->labelJavascript->Text = $javascript;
        }
    }

    public function getUrlSiteOrganisme($commonConsultation)
    {
        if ($commonConsultation instanceof CommonConsultation) {
            if ($commonConsultation->getUrlOrganisme()) {
                return $commonConsultation->getUrlOrganisme();
            } else {
                return '';
            }
        }
    }

    public function getColSpan()
    {
        if (!Atexo_Module::isEnabled('EntrepriseActionsGroupees')) {
            return '5';
        } else {
            return '6';
        }
    }

    public function validerChoix($sender, $param)
    {
        $AtLeastOne = false;
        $idEtp = Atexo_CurrentUser::getIdEntreprise();
        $idInscrit = Atexo_CurrentUser::getIdInscrit();
        foreach ($this->tableauResultSearch->getItems() as $item) {
            if ($item->annonceSelection->checked) {
                $retour = Atexo_Entreprise_PaniersEntreprises::AddConsultationToPanierEntreprise($item->orgCons->value, $item->refCons->value, $idEtp, $idInscrit);
                if ($retour) {
                    $AtLeastOne = true;
                }
            }
        }
        if ($AtLeastOne) {
            $this->scriptJS->text = "<script>javascript:popUp('index.php?page=Entreprise.popUpGestionPanier&id&action=AddLot')</script>";
        }
        self::refreshOnPostBack();
    }

    public function refreshOnPostBack()
    {
        $criteriaVo = $this->Page->getViewState('CriteriaVo');
        if ($criteriaVo && $criteriaVo instanceof Atexo_Consultation_CriteriaVo) {
            $this->fillRepeaterWithDataForSearchResult($criteriaVo);
        }
    }

    /*
     * Permet de mettre a jour la pagination
     */
    public function updatePagination($pageSize, $numPage)
    {
        $nombreElement = $this->getViewState('nombreElement');
        $this->tableauResultSearch->PageSize = $pageSize;
        $this->nombrePageTop->Text = ceil($nombreElement / $this->tableauResultSearch->PageSize);
        $this->nombrePageBottom->Text = ceil($nombreElement / $this->tableauResultSearch->PageSize);
        if ($numPage > $this->nombrePageTop->Text) {
            $numPage = $this->nombrePageTop->Text;
        }
        $this->tableauResultSearch->setCurrentPageIndex($numPage - 1);
        $this->numPageBottom->Text = $numPage;
        $this->numPageTop->Text = $numPage;
    }

    /**
     * Permet de changer la langue de traduction de la consultation.
     *
     * @param $sender
     * @param $param
     */
    public function actionChangerLangueConsultation($sender, $param)
    {
        $parametres = $param->CallBackParameter;
        $arrayParametres = explode('_', $parametres);
        $this->setViewState('refresh_refCons', $arrayParametres[0]);
        $this->setViewState('refresh_itemIndex', $arrayParametres[1]);
        $this->setViewState('refresh_langue', $arrayParametres[2]);
        $this->setViewState('refresh_orgCons', $arrayParametres[3]);
        $this->setViewState('refresh_typeProcedure', $arrayParametres[4]);
        $this->setViewState('refresh_categorieCons', $arrayParametres[5]);
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $consultation = CommonConsultationPeer::retrieveByPK($this->getViewState('refresh_refCons'), $connexion);
        $this->setViewState('refresh_consultation', $consultation);
        $this->raffraichirBlocsChangementLangueConsultation($sender, $param);

        $clientIdRepeater = substr(str_replace('_idBoutonChangerLangueConsultation', '', $sender->ClientId), 0, -1);
        $this->scriptJSActive->text = "<script>afficherLang('".$clientIdRepeater."','".$sender->ClientId."');</script>";
    }

    /**
     * Permet de raffraichir les blocs (intitule consultation, objet consultation, denomination organisme, lieux d'execution, categorie principale,
     * types de procedures.
     *
     * @param $sender
     * @param $param
     */
    public function raffraichirBlocsChangementLangueConsultation($sender, $param)
    {
        //$sender correspond au bouton "idBoutonChangerLangueConsultation"
        //$sender->render($param->getNewWriter());
        //$sender->Parent->Parent->Parent->Parent correspond a l'element "Data" du repeater
        $dataItem = $sender->Parent->Parent->Parent->Parent;
        $dataItem->panelBlocIntitule->render($param->getNewWriter());
        $dataItem->panelBlocObjet->render($param->getNewWriter());
        $dataItem->panelBlocDenomination->render($param->getNewWriter());
        $dataItem->panelBlocLieuxExec->render($param->getNewWriter());
        $dataItem->panelBlocTypesProc->render($param->getNewWriter());
        $dataItem->panelBlocCategorie->render($param->getNewWriter());
        $dataItem->panelBlocCodesNuts->render($param->getNewWriter());
    }

    /**
     * Permet de determiner la langue de traduction de la consultation.
     *
     * @param int $itemIndex: itemIndex du repeater
     */
    public function getLangueTraductionConsultation($itemIndex)
    {
        if (($this->getViewState('refresh_itemIndex') || '0' === $this->getViewState('refresh_itemIndex'))
            && $itemIndex == $this->getViewState('refresh_itemIndex')) {
            return $this->getViewState('refresh_langue');
        } else {
            return Atexo_Languages::readLanguageFromSession();
        }
    }

    /**
     * @author Oumar KONATE <oumar.konate@atexo.com>
     * Permet de recuperer le libelle des codes nuts
     *
     * @param CommonConsultation $consultation
     * @param int                $itemIndex
     */
    public function getLibelleCodesNutsFromConsultation($consultation, $itemIndex)
    {
        if ($this->getViewState('refresh_consultation') instanceof CommonConsultation) {
            $consultation = $this->getViewState('refresh_consultation');
        }
        $codesNuts = [];
        if ($consultation instanceof CommonConsultation) {
            $langue = $this->getLangueTraductionConsultation($itemIndex);
            $codesNuts = $consultation->getLibelleCodesNuts($consultation->getCodesNuts(), $langue);
        }

        return $codesNuts;
    }

    public function resetViewStat()
    {
        $this->setViewState('refresh_refCons', null);
        $this->setViewState('refresh_itemIndex', null);
        $this->setViewState('refresh_langue', null);
        $this->setViewState('refresh_orgCons', null);
        $this->setViewState('refresh_typeProcedure', null);
        $this->setViewState('refresh_categorieCons', null);
        $this->setViewState('refresh_consultation', null);
        $this->scriptJSActive->text = '';
    }

    /**
     * Permet de gerer l'affichage du picto "Repondre a la consultation de maniere simplifiee".
     *
     * @param CommonConsultation $consultation l'objet CommonConsultation
     *
     * @return bool true ou false
     *
     * @author Ayoub SOUID AHMED  <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function showPictoAccesMps($consultation)
    {
        $dateFin = $consultation->getDatefin();
        //$dateFinPoursuivreAffichage = Atexo_Util::dateDansFutur ($dateFin, 0, $consultation->getPoursuivreAffichage());
        $dateFinPoursuivreAffichage = strtotime($dateFin.' + '.$consultation->getPoursuivreAffichage().' '.$consultation->getPoursuivreAffichageUnite());
        $currentDate = date('Y-m-d H:i:s');
        if (((Atexo_Module::isEnabled('EntrepriseRepondreConsultationApresCloture') && $dateFinPoursuivreAffichage >= strtotime($currentDate))
        || (Atexo_Util::cmpIsoDateTime($currentDate, $dateFin) <= 0)) && !$consultation->getConsultationAnnulee()) {
            if (1 == $consultation->getAutoriserReponseElectronique() && Atexo_Module::isEnabled('MarchePublicSimplifie', $consultation->getOrganisme())
                && 1 == $consultation->getMarchePublicSimplifie()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Permet de retourner le nom du picto selon le cas.
     *
     * @return string
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function getPictoBourseCotraitance($refCons)
    {
        if ($this->showBourse()) {
            $groupement = Atexo_BourseCotraitance::getCasGroupement($refCons);
            switch ($groupement['cas']) {
                case 'cas1':
                    return 'icone-cotraitance-1.png';
                case 'cas2':
                    return 'icone-cotraitance-2.png';
                case 'cas3':
                    return 'icone-cotraitance-3.png';
                case 'cas4':
                    return 'icone-cotraitance-4.png';
            }
        } else {
            return 'icone-cotraitance-pas-actif.png';
        }
    }

    /**
     * Permet de retourner le message selon le cas.
     *
     * @return string
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function getMessageInscriptionBourseCotraitance($refConsultation)
    {
        $nomEse = null;
        if ($this->showBourse()) {
            if (0 != Atexo_CurrentUser::getIdEntreprise()) {
                $entreprise = CommonEntrepriseQuery::create()->filterById(Atexo_CurrentUser::getIdEntreprise())->findOne();
                if ($entreprise instanceof Entreprise) {
                    $nomEse = $entreprise->getNom();
                }
            }
            $groupement = Atexo_BourseCotraitance::getCasGroupement($refConsultation);
            switch ($groupement['cas']) {
                case 'cas1':
                    return Prado::localize('TEXT_AUCUN_CANDIDAT_INSCRIT');
                case 'cas2':
                    return '<strong class="m-r-1">'.$nomEse.' </strong>'.Prado::localize('TEXT_MOI_SEUL_INSCRIT');
                case 'cas3':
                    return $groupement['count'].' '.(($groupement['count'] > 1) ? Prado::localize('TEXT_CANDIDATS_INSCRITS') : Prado::localize('TEXT_CANDIDAT_INSCRIT'));
                case 'cas4':
                    return $groupement['count'].' <span class="m-l-1">'.Prado::localize('TEXT_CANDIDAT_INSCRIT_DONT').'</span><br/><strong> '.$nomEse.'</strong>';
            }
        } else {
            return 'Module non actif';
        }
    }

    public function showBourse()
    {
        if (Atexo_Module::isEnabled('BourseCotraitance')
            && !isset($_GET['dateFinEnd'])
            && $this->calledFromEntreprise()
            && !$this->searchAnnonce()) {
            return true;
        }

        return false;
    }

    public function hideShowPagination()
    {
        return intval($this->nombreElement->Text) > $this->tableauResultSearch->PageSize;
    }

    /**
     * @return string
     */
    public function getUrlRC(CommonConsultation $consultation)
    {
        $cachedValue = null;
        try {

            $keyName = 'getUrlRC-'.$consultation->getId();

            $applicationCache = Prado::getApplication()->Cache;
            $cachedValue = $applicationCache->get($keyName);

            if (false === $cachedValue) {
                $documentService = Atexo_Util::getSfService(DocumentService::class);

                $cachedValue = $documentService->getLienRcByIdConsultation($consultation->getId());
                $applicationCache->set($keyName, $cachedValue, 15 * 60);
            }
        } catch (\Exception $exception) {
            $logger = Atexo_LoggerManager::getLogger('supervision');
            $logger->error($exception->getMessage() . " " . $exception->getTraceAsString());
        }

        return $cachedValue;
    }

    public function isTypageJoActivated(int $consultationId): bool
    {
        /** @var ConsultationTags $tagService */
        $tagService = Atexo_Util::getSfService(ConsultationTags::class);

        $consultationTagId = $tagService->getTagByConsultationId($consultationId, 'typageJo');

        return !empty($consultationTagId);
    }
}
