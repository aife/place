<?php

namespace Application\Controls;

use Application\Service\Atexo\Controller\Atexo_Controller_Front;
use Prado\Prado;
use Prado\Web\UI\ActiveControls\TActiveImageButton;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class ActivePictoOk extends TActiveImageButton
{
    public function onLoad($param)
    {
        $this->setImageUrl(Atexo_Controller_Front::t().'/images/bouton-ok.gif');
        $this->setCssClass('ok');
        $this->setAlternateText(Prado::localize('ICONE_OK'));
        $this->setToolTip(Prado::localize('ICONE_OK'));
    }
}
