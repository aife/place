<?php

namespace Application\Controls;

use Application\Propel\Mpe\CommonCategorieLot;
use Application\Propel\Mpe\CommonTDossierFormulaire;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Lots;
use Application\Service\Atexo\Signature\Atexo_Signature_FichierVo;
use Application\Service\Atexo\Signature\Atexo_Signature_InfosVo;
use Prado\Prado;

/**
 * Page de gestion des enveloppes de reponse electronique.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 1.0
 *
 * @since MPE-4.0
 */
class AtexoReponseEnveloppe extends MpeTTemplateControl
{
    public $_refConsultation;
    public $_orgConsultation;
    public $_numLot;
    public $_typeEnv;
    public $_idHtlmToggleFormulaire;

    /**
     * Recupère la valeur.
     */
    public function getNumLot()
    {
        $numLot = $this->_numLot;
        if ($this->numLot->Value) {
            $numLot = $this->numLot->Value;
        }

        return $numLot;
    }

    /**
     * Affectes la valeur.
     */
    public function setNumLot($value)
    {
        $this->_numLot = $value;
    }

    /**
     * Recupère la valeur.
     */
    public function getRefConsultation()
    {
        return $this->_refConsultation;
    }

    /**
     * Affectes la valeur.
     */
    public function setRefConsultation($value)
    {
        $this->_refConsultation = $value;
    }

    /**
     * Recupère la valeur.
     */
    public function getOrgConsultation()
    {
        return $this->_orgConsultation;
    }

    /**
     * Affectes la valeur.
     */
    public function setOrgConsultation($value)
    {
        $this->_orgConsultation = $value;
    }

    /**
     * Recupère la valeur.
     */
    public function getTypeEnv()
    {
        if ($this->_typeEnv) {
            return $this->_typeEnv;
        } else {
            return $this->typeEnveloppe->Value;
        }
    }

    /**
     * Affectes la valeur.
     */
    public function setTypeEnv($value)
    {
        $this->_typeEnv = $value;
    }

    /**
     * Recupère la valeur.
     */
    public function getIdHtlmToggleFormulaire()
    {
        $idHtlmToggleFormulaire = $this->_idHtlmToggleFormulaire;
        if (!$idHtlmToggleFormulaire) {
            $idHtlmToggleFormulaire = $this->hiddenIdHtlmToggleFormulaire->Value;
        }

        return $idHtlmToggleFormulaire;
    }

    /**
     * Attribut la valeur.
     */
    public function setIdHtlmToggleFormulaire($value)
    {
        $this->_idHtlmToggleFormulaire = $value;
    }

    /**
     * Chargement de la page.
     */
    public function onLoad($param)
    {
        if (!$this->Page->IsPostBack && !$this->Page->IsCallBack) {
            $this->typeEnveloppe->Value = $this->getTypeEnv();
            $this->numLot->Value = $this->getNumLot();
        }
    }

    /**
     * Permet de recuperer la consultation.
     */
    public function getConsultation()
    {
        return $this->Page->getViewState('consultation');
    }

    /**
     * Permet de recuperer le lot.
     */
    public function getLot()
    {
        return (new Atexo_Consultation_Lots())->retrieveCategorieLot($this->getConsultation()->getId(), $this->getNumLot(), $this->getConsultation()->getOrganisme());
    }

    /**
     * Recupère le nom du dossier.
     */
    public function getNomDossier()
    {
        $nomDossier = '';
        if ($this->getNumLot()) {
            $nomDossier = Prado::localize('TEXT_LOT').' '.$this->getNumLot().' - ';
        }
        if ($this->getTypeEnv() == Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE')) {
            $nomDossier .= Prado::localize('DEFINE_DOSSIER_CANDIDATURE');
        } elseif ($this->getTypeEnv() == Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE')) {
            $nomDossier .= Prado::localize('DOSSIER_OFFRE_TECHNIQUE');
        } elseif ($this->getTypeEnv() == Atexo_Config::getParameter('TYPE_ENV_OFFRE')) {
            $nomDossier .= Prado::localize('DEFINE_DOSSIER_OFFRES');
        } elseif ($this->getTypeEnv() == Atexo_Config::getParameter('TYPE_ENV_ANONYMAT')) {
            $nomDossier .= Prado::localize('DOSSIER_OFFRE_ANONYMAT');
        }
        if ($this->getNumLot() && $this->getLot() instanceof CommonCategorieLot) {
            $nomDossier .= ' : '.$this->getLot()->getDescription();
        }

        return $nomDossier;
    }

    /**
     * Recupère l'emetteur et le signataire (CN, AC).
     */
    public function getInfosSignataire($fichier)
    {
        $infos = '';
        if ($fichier instanceof Atexo_Signature_FichierVo && $fichier->getInfosSignature() instanceof Atexo_Signature_InfosVo) {//Nous permet de savoir si une signature existe
            $signature = $fichier->getInfosSignature();
            $arrayObjetCertif = explode(',', $signature->getSignataire());
            if (is_array($arrayObjetCertif) && count($arrayObjetCertif)) {
                foreach ($arrayObjetCertif as $key => $cnObjet) {
                    if (strstr($cnObjet, 'CN')) {
                        unset($arrayObjetCertif[$key]);
                        array_unshift($arrayObjetCertif, $cnObjet);
                    }
                }
            }
            $CnObjetCert = $arrayObjetCertif[0];
            $arrayEmetteurCertif = explode(',', $signature->getEmetteur());
            $CnEmetteurCertif = $arrayEmetteurCertif[0];
            $infosComplet = str_replace('=', ' : ', $CnObjetCert).' ( '.Prado::localize('DEFINE_AC').' : '.$CnEmetteurCertif.')';
            $infos = $infosComplet;
        } else {
            $infos .= Prado::localize('DEFINE_AUCUNE_SIGNATURE_IDENTIFIEE');
        }

        return (new Atexo_Config())->toPfEncoding($infos);
    }

    /**
     * Recupère l'image du picto si signature existe ou pas.
     */
    public function getPictoSignatureExiste($fichier)
    {
        if ($fichier instanceof Atexo_Signature_FichierVo && $fichier->getInfosSignature() instanceof Atexo_Signature_InfosVo) {
            $picto = 'images/picto-signature.gif';
        } else {
            $picto = 'images/picto-aucune-signature.gif';
        }

        return $picto;
    }

    /**
     * Recupère le message alt du picto si signature existe ou pas.
     */
    public function getAltSignatureExiste($fichier)
    {
        return Prado::localize('TEXT_SIGNATURE_ELECTRONIQUE');
    }

    /**
     * Recupère l'image du picto de la verification de la signature.
     */
    public function getPictoVerifSignature($fichier)
    {
        if ($fichier instanceof Atexo_Signature_FichierVo && $fichier->isSignedFile()) {
            $picto = match ($fichier->verifierSignature()) {
                Atexo_Config::getParameter('OK') => 'images/picto-check-ok-small.gif',
                Atexo_Config::getParameter('NOK') => 'images/picto-check-not-ok.gif',
                Atexo_Config::getParameter('UNKNOWN') => 'images/picto-validation-incertaine.gif',
                default => 'images/picto-validation-incertaine.gif',
            };
        } else {
            $picto = 'images/picto-vide.gif';
        }

        return $picto;
    }

    /**
     * Recupère fichier a une signature.
     */
    public function getHasFileSignature($fichier)
    {
        return $fichier instanceof Atexo_Signature_FichierVo && $fichier->isSignedFile();
    }

    /**
     * Recupère le message alt du picto de la verification de la signature.
     */
    public function getAltVerifSignature($fichier)
    {
        if ($fichier instanceof Atexo_Signature_FichierVo && $fichier->isSignedFile()) {
            $alt = match ($fichier->verifierSignature()) {
                Atexo_Config::getParameter('OK') => Prado::localize('TEXT_VERIFICATION_OK'),
                Atexo_Config::getParameter('NOK') => Prado::localize('TEXT_VERIFICATION_KO'),
                Atexo_Config::getParameter('UNKNOWN') => Prado::localize('TEXT_VERIFICATION_INCERTAINE'),
                default => Prado::localize('TEXT_VERIFICATION_INCERTAINE'),
            };
        } else {
            $alt = Prado::localize('DEFINE_AUCUNE_SIGNATURE_IDENTIFIEE');
        }

        return $alt;
    }

    /**
     * précise l'affichage de l'image du picto si signature existe ou pas.
     */
    public function afficherLienSignature($fichier)
    {
        if ($fichier instanceof Atexo_Signature_FichierVo && $fichier->getInfosSignature() instanceof Atexo_Signature_InfosVo) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Recupère l'image du statut du dossier.
     */
    public function getStatutPictoDossier($dossier)
    {
        $picto = null;
        if ($dossier instanceof CommonTDossierFormulaire) {
            $picto = match ($dossier->getStatutValidation()) {
                Atexo_Config::getParameter('STATUT_DOSSIER_BROUILLON') => 'images/reponse-statut-1sur2.gif',
                Atexo_Config::getParameter('STATUT_DOSSIER_VALIDE') => 'images/reponse-statut-2sur2.gif',
                default => 'images/reponse-statut-1sur2.gif',
            };
        }

        return $picto;
    }

    /**
     * Recupère le statut du dossier.
     */
    public function getStatutDossier($dossier)
    {
        $statut = null;
        if ($dossier instanceof CommonTDossierFormulaire) {
            $statut = match ($dossier->getStatutValidation()) {
                Atexo_Config::getParameter('STATUT_DOSSIER_BROUILLON') => Prado::localize('DEFINE_STATUS').' : '.Prado::localize('TEXT_BROUILLON_NON_CONFORME'),
                Atexo_Config::getParameter('STATUT_DOSSIER_VALIDE') => Prado::localize('DEFINE_STATUS').' : '.Prado::localize('DEFINE_EN_VALIDE_FINI'),
                default => Prado::localize('DEFINE_STATUS').' : '.Prado::localize('TEXT_BROUILLON_NON_CONFORME'),
            };
        }

        return $statut;
    }

    /**
     * Permet de gerer la visibilité de la case à cocher pour la signature des fichiers de réponse.
     */
    public function getVisibiliteCocherSignature($fichier)
    {
        return !$this->afficherLienSignature($fichier);
    }
}
