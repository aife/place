<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Util;
use Exception;
use Prado\Prado;

/**
 * Template du bloc siège.
 */
class PanelSiege extends MpeTPage
{
    public string $_calledFor = '';

    public function onLoad($param)
    {
        /*if ($this->_calledFor !== Prado::localize('DEFINE_FRANCE') && $this->_calledFor !== "Etrangere")
            {
                throw new Exception ("You must precise _calledFor parameter");

            } */
        // Si enptreprise Française afficher Siren
        if ($this->_calledFor == Atexo_Config::getParameter('DENOMINATION1_GEON2_FRANCE')) {
            $this->panelSiren->setVisible(true);
            $this->panelIdNational->setVisible(false);
        }
        // Sinon afficher idNational
        elseif ('Etrangere' == $this->_calledFor) {
            $this->panelSiren->setVisible(false);
            $this->panelIdNational->setVisible(true);
        }
    }

    public function setCalledFor($value)
    {
        $this->_calledFor = $value;
    }

    public function setSiren($value)
    {
        $this->siren->Text = $value;
    }

    public function setIdNational($value)
    {
        $this->idNational->Text = $value;
    }

    public function setRaisonSociale($value)
    {
        $this->texteRaisonSociale->Text = $value;
    }

    public function setLieuEtablissement($value)
    {
        $this->lieuEtablissement->Text = Atexo_Util::encodeToHttp($value);
    }

    public function fill($company, $visible = true)
    {
        if ('' != $company->getSiren()) {
            $this->setCalledFor(Atexo_Config::getParameter('DENOMINATION1_GEON2_FRANCE'));
            $this->setSiren($company->getSiren());
        } else {
            $this->setCalledFor('Etrangere');
            $this->setIdNational($company->getSirenEtranger());
        }
        $this->setLieuEtablissement($company->getPaysadresse());
        $this->setRaisonSociale($company->getNom());
    }
}
