<?php

namespace Application\Controls;

use Application\Service\Atexo\AlerteMetier\ConfigurationAlerte;
use Application\Service\Atexo\Atexo_AlerteMetier;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Module;
use Prado\Prado;

/**
 * commentaires.
 *
 * @author loubna EZZIANI <loubna.ezziani@atexo.com>
 * @copyright Atexo 2012
 *
 * @version 1.0
 *
 * @since MPE-4.0
 */
class AtexoAlertesPageAcceuil extends MpeTTemplateControl
{
    public function onLoad($param)
    {
        if (Atexo_Module::isEnabled('AlerteMetier')) {
            $dataSource = [];
            $i = 0;
            $allAlertes = (new ConfigurationAlerte())->retrieveAllActiveAlertes();
            if (is_array($allAlertes) && count($allAlertes)) {
                foreach ($allAlertes as $alerte) {
                    $libelle = '';
                    switch ($alerte->getTypeObjet()) {
                        case Atexo_Config::getParameter('TYPE_OBJET_ALERTE_CONSULTATION'):
                             $nomber = (new Atexo_AlerteMetier())->getNombreAlerteOfConsultation($alerte->getId());
                                                                                            $libelle = '<a href="index.php?page=Agent.TableauDeBord&idAlerte='.$alerte->getId().'" >'.Prado::localize('TEXT_CONSULTATION_AVEC').' '.strtolower(Prado::localize($alerte->getLibelle())).' : '.$nomber.'</a>';
                            break;
                        case Atexo_Config::getParameter('TYPE_OBJET_ALERTE_SERVICE'):
                             $nomber = (new Atexo_AlerteMetier())->getNombreAlerteOfService($alerte->getId());
                                                                                        $libelle = Prado::localize($alerte->getLibelle());
                            break;
                        case Atexo_Config::getParameter('TYPE_OBJET_PLATE_FORME'):
                             $nomber = (new Atexo_AlerteMetier())->getNombreAlerteOfPlateForme($alerte->getId());
                                                                                        $libelle = Prado::localize($alerte->getLibelle());
                            break;
                        default:
                             $nomber = 0;
                            break;
                    }
                    if ($nomber > 0) {
                        $dataSource[$i]['libelle'] = $libelle;
                        ++$i;
                    }
                }
            }
            if (count($dataSource)) {
                $this->repeaterAlertes->DataSource = $dataSource;
                $this->repeaterAlertes->DataBind();
                $this->panelAlertes->setVisible(true);
            } else {
                $this->panelAlertes->setVisible(false);
            }
        } else {
            $this->panelAlertes->setVisible(false);
        }
    }
}
