<?php

namespace Application\Controls;

use Application\Propel\Mpe\CommonAgentCommission;
use Application\Propel\Mpe\CommonIntervenantOrdreDuJour;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_ValeursReferentielles;
use Application\Service\Atexo\Commission\Atexo_Commission_Agent;
use Application\Service\Atexo\Commission\Atexo_Commission_IntervenantExterne;
use Prado\Prado;

/**
 * composant d'affichage des intervenents externes.
 *
 * @author Amal EL BEKKAOUI <amal.elbekkaoui@atexo.com>
 * @copyright Atexo 2012
 *
 * @since MPE4
 */
class ListeIntervenant extends MpeTTemplateControl
{
    private bool $organisation = false;
    private bool $serviceIntevenant = false;
    private bool $fonction = false;
    private bool $typeCompte = false;
    private bool $isIntervenant = true;
    private bool $actionModifier = false;
    private bool $actionSupprimer = false;
    private bool $actions = true;
    private bool $checkbox = false;
    private bool $listeTypeVoix = false;
    private bool $typeVoix = false;
    private bool $typeVoixCommission = false;
    private bool $typeVoixOrdreJour = false;
    /* typeRecherche :
     * 1:liste intervenants externes;
     * 2:liste Agents
     * 3:liste agents commission
     */
    private string $typeRecherche = '1';

    public function getOrganisation()
    {
        return $this->organisation;
    }

    public function getServiceIntevenant()
    {
        return $this->serviceIntevenant;
    }

    public function getFonction()
    {
        return $this->fonction;
    }

    public function getTypeCompte()
    {
        return $this->typeCompte;
    }

    public function getIsIntervenant()
    {
        return $this->isIntervenant;
    }

    public function getActionModifier()
    {
        return $this->actionModifier;
    }

    public function getActionSupprimer()
    {
        return $this->actionSupprimer;
    }

    public function getActions()
    {
        return $this->actions;
    }

    public function getCheckbox()
    {
        return $this->checkbox;
    }

    public function getTypeRecherche()
    {
        return $this->typeRecherche;
    }

    public function getListeTypeVoix()
    {
        return $this->listeTypeVoix;
    }

    public function getTypeVoix()
    {
        return $this->typeVoix;
    }

    public function getTypeVoixCommission()
    {
        return $this->typeVoixCommission;
    }

    public function getTypeVoixOrdreJour()
    {
        return $this->typeVoixOrdreJour;
    }

    public function setTypeRecherche($x)
    {
        $this->typeRecherche = $x;
    }

    public function setOrganisation($x)
    {
        $this->organisation = $x;
    }

    public function setServiceIntevenant($x)
    {
        $this->serviceIntevenant = $x;
    }

    public function setFonction($x)
    {
        $this->fonction = $x;
    }

    public function setTypeCompte($x)
    {
        $this->typeCompte = $x;
    }

    public function setIsIntervenant($x)
    {
        $this->isIntervenant = $x;
    }

    public function setActionModifier($x)
    {
        $this->actionModifier = $x;
    }

    public function setActionSupprimer($x)
    {
        $this->actionSupprimer = $x;
    }

    public function setActions($x)
    {
        $this->actions = $x;
    }

    public function setCheckbox($x)
    {
        $this->checkbox = $x;
    }

    public function setListeTypeVoix($x)
    {
        $this->listeTypeVoix = $x;
    }

    public function setTypeVoix($x)
    {
        $this->typeVoix = $x;
    }

    public function setTypeVoixCommission($x)
    {
        $this->typeVoixCommission = $x;
    }

    public function setTypeVoixOrdreJour($x)
    {
        $this->typeVoixOrdreJour = $x;
    }

    public function fillRepeaterWithDataForSearchResult($criteriaVo)
    {
        $nombreElement = self::search($criteriaVo, true);
        if ($nombreElement >= 1) {
            $this->panelMoreThanOneElementFound->setVisible(true);
            $this->panelNoElementFound->setVisible(false);
            $this->nombreElement->Text = $nombreElement;
            $this->PagerBottom->setVisible(true);
            $this->PagerTop->setVisible(true);
            $this->setViewState('nombreElement', $nombreElement);
            $this->nombrePageTop->Text = ceil($nombreElement / $this->tableauIntervenant->PageSize);
            $this->nombrePageBottom->Text = ceil($nombreElement / $this->tableauIntervenant->PageSize);
            $this->tableauIntervenant->setVirtualItemCount($nombreElement);
            $this->tableauIntervenant->setCurrentPageIndex(0);
            $this->populateData($criteriaVo);
        } else {
            $this->panelMoreThanOneElementFound->setVisible(false);
            $this->panelNoElementFound->setVisible(true);
            $this->PagerBottom->setVisible(false);
            $this->PagerTop->setVisible(false);
        }
    }

    public function populateData($criteriaVo)
    {
        $offset = $this->tableauIntervenant->CurrentPageIndex * $this->tableauIntervenant->PageSize;
        $limit = $this->tableauIntervenant->PageSize;
        if ($offset + $limit > $this->tableauIntervenant->getVirtualItemCount()) {
            $limit = $this->tableauIntervenant->getVirtualItemCount() - $offset;
        }
        $criteriaVo->setOffset($offset);
        $criteriaVo->setLimit($limit);
        $arrayIntervenant = self::search($criteriaVo);
        $this->setViewState('elements', $arrayIntervenant);
        $this->tableauIntervenant->DataSource = $arrayIntervenant;
        $this->tableauIntervenant->DataBind();
    }

    public function pageChanged($sender, $param)
    {
        $this->tableauIntervenant->CurrentPageIndex = $param->NewPageIndex;
        $this->numPageBottom->Text = $param->NewPageIndex + 1;
        $this->numPageTop->Text = $param->NewPageIndex + 1;
        $criteriaVo = $this->getViewState('CriteriaVo');
        $this->populateData($criteriaVo);
    }

    public function elementSortedWith($panelName, $openCloseTag)
    {
        if ($this->getViewState('sortByElement') == $panelName) {
            if ('open' == $openCloseTag) {
                return '<strong>';
            } else {
                return '</strong>';
            }
        } else {
            return '';
        }
    }

    public function goToPage($sender, $param)
    {
        $numPage = null;
        switch ($sender->ID) {
            case 'DefaultButtonTop':
                 $numPage = $this->numPageTop->Text;
                break;
            case 'DefaultButtonBottom':
                 $numPage = $this->numPageBottom->Text;
                break;
        }
        if (Atexo_Util::isEntier($numPage)) {
            if ($numPage >= $this->nombrePageTop->Text) {
                $numPage = $this->nombrePageTop->Text;
            } elseif ($numPage <= 0) {
                $numPage = 1;
            }
            $this->tableauIntervenant->CurrentPageIndex = $numPage - 1;
            $this->numPageBottom->Text = $numPage;
            $this->numPageTop->Text = $numPage;
            $criteriaVo = $this->getViewState('CriteriaVo');
            $this->populateData($criteriaVo);
        } else {
            $this->numPageTop->Text = $this->tableauIntervenant->CurrentPageIndex + 1;
            $this->numPageBottom->Text = $this->tableauIntervenant->CurrentPageIndex + 1;
        }
    }

    public function changePagerLenght($sender, $param)
    {
        switch ($sender->ID) {
            case 'nombreResultatAfficherBottom':
                 $pageSize = $this->nombreResultatAfficherBottom->getSelectedValue();
                                                  $this->nombreResultatAfficherTop->setSelectedValue($pageSize);
                break;
            case 'nombreResultatAfficherTop':
                 $pageSize = $this->nombreResultatAfficherTop->getSelectedValue();
                                               $this->nombreResultatAfficherBottom->setSelectedValue($pageSize);
                break;
        }

        $this->tableauIntervenant->PageSize = $pageSize;
        $nombreElement = $this->getViewState('nombreElement');
        $this->nombrePageTop->Text = ceil($nombreElement / $this->tableauIntervenant->PageSize);
        $this->nombrePageBottom->Text = ceil($nombreElement / $this->tableauIntervenant->PageSize);
        $criteriaVo = $this->getViewState('CriteriaVo');
        $this->tableauIntervenant->setCurrentPageIndex(0);
        $this->populateData($criteriaVo);
    }

    public function search($c, $nbreElement = false)
    {
        //var_dump($this->listeTypeVoix);exit;
        $this->setViewState('CriteriaVo', $c);
        return match ($this->typeRecherche) {
            1 => (new Atexo_Commission_IntervenantExterne())->getIntervenants($c, $nbreElement),
            2 => 0,
            3 => (new Atexo_Agent())->getAgentsForCommission($c, $nbreElement),
            default => [],
        };
    }

    public function getLibelleTypeVoix($id)
    {
        $typeVoix = $this->getViewState('typeVoix', []);
        if (!(is_array($typeVoix) && count($typeVoix) > 0)) {
            $typeVoix = (new Atexo_ValeursReferentielles())->getValeurRefByIdRef(Atexo_Config::getParameter('ID_REFERENTIEL_TYPE_VOIX_INTERVENANTS'));
            $this->setViewState('typeVoix', $typeVoix);
        }

        return $typeVoix[$id];
    }

    public function getTypeVoixAgent($id, $returnIdTypeVoix = false)
    {
        $idCommission = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['idCom']));
        $org = Atexo_CurrentUser::getCurrentOrganism();
        $agentCom = (new Atexo_Commission_Agent())->getCommissionAgent($id, $idCommission, $org);

        $typeVoix = $this->getViewState('typeVoix', []);
        if (!(is_array($typeVoix) && count($typeVoix) > 0)) {
            $typeVoix = (new Atexo_ValeursReferentielles())->getValeurRefByIdRef(Atexo_Config::getParameter('ID_REFERENTIEL_TYPE_VOIX_INTERVENANTS'));
            $this->setViewState('typeVoix', $typeVoix);
        }
        if ($agentCom instanceof CommonAgentCommission) {
            $idTypeVoix = $agentCom->getTypeVoix();
            if ($idTypeVoix) {
                if ($returnIdTypeVoix) {
                    return $idTypeVoix;
                } else {
                    return $typeVoix[$idTypeVoix];
                }
            } else {
                foreach ($typeVoix as $key => $value) {
                    return $key;
                }
            }
        } else {
            foreach ($typeVoix as $key => $value) {
                return $key;
            }
        }
    }

    public function getTypeVoixIntervenant($id, $returnIdTypeVoix = false)
    {
        $idCommission = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['idCom']));
        $org = Atexo_CurrentUser::getCurrentOrganism();
        $intervenantCom = (new Atexo_Commission_IntervenantExterne())->getCommissionIntervenantExterne($id, $idCommission, $org);
        $typeVoix = $this->getViewState('typeVoix', []);
        if (!(is_array($typeVoix) && count($typeVoix) > 0)) {
            $typeVoix = (new Atexo_ValeursReferentielles())->getValeurRefByIdRef(Atexo_Config::getParameter('ID_REFERENTIEL_TYPE_VOIX_INTERVENANTS'));
            $this->setViewState('typeVoix', $typeVoix);
        }
        if ($intervenantCom instanceof CommonIntervenantExterneCommission) {
            $idTypeVoix = $intervenantCom->getTypeVoix();
            if ($idTypeVoix) {
                if ($returnIdTypeVoix) {
                    return $idTypeVoix;
                } else {
                    return $typeVoix[$idTypeVoix];
                }
            } else {
                foreach ($typeVoix as $key => $value) {
                    return $key;
                }
            }
        } else {
            foreach ($typeVoix as $key => $value) {
                return $key;
            }
        }
    }

    public function getSelectedValueTypeVoix($idTypeVoix)
    {
        if ($idTypeVoix) {
            return $idTypeVoix;
        } else {
            $typeVoix = $this->getViewState('typeVoix', []);
            if (!(is_array($typeVoix) && count($typeVoix) > 0)) {
                $typeVoix = (new Atexo_ValeursReferentielles())->getValeurRefByIdRef(Atexo_Config::getParameter('ID_REFERENTIEL_TYPE_VOIX_INTERVENANTS'));
                $this->setViewState('typeVoix', $typeVoix);
            }
            $value = '';
            foreach ($typeVoix as $key => $value) {
                return $key;
            }
        }
    }

    public function getLibelleTypeVoixCommission($id, $returnIdTypeVoix = false)
    {
        if ('true' == $this->getIsIntervenant()) {
            return self::getTypeVoixIntervenant($id, $returnIdTypeVoix);
        } else {
            return self::getTypeVoixAgent($id, $returnIdTypeVoix);
        }
    }

    public function getLibelleTypeVoixOrdreJour($id, $returnIdTypeVoix = false)
    {
        if ('true' == $this->getIsIntervenant()) {
            return self::getTypeVoixIntervenantOrdreJour($id, $returnIdTypeVoix);
        } else {
            return self::getTypeVoixAgentOrdreJour($id, $returnIdTypeVoix);
        }
    }

    public function getTypeVoixIntervenantOrdreJour($id, $returnIdTypeVoix = false)
    {
        $idOrdreJour = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id']));
        $org = Atexo_CurrentUser::getCurrentOrganism();
        $intervenant = (new Atexo_Commission_IntervenantExterne())->getIntervenantOrdreJour($id, $idOrdreJour, $org);
        $typeVoix = $this->getViewState('typeVoix', []);
        if (!(is_array($typeVoix) && count($typeVoix) > 0)) {
            $typeVoix = (new Atexo_ValeursReferentielles())->getValeurRefByIdRef(Atexo_Config::getParameter('ID_REFERENTIEL_TYPE_VOIX_INTERVENANTS'));
            $this->setViewState('typeVoix', $typeVoix);
        }
        if ($intervenant instanceof CommonIntervenantOrdreDuJour) {
            $idTypeVoix = $intervenant->getTypeVoix();
            if ($idTypeVoix) {
                if ($returnIdTypeVoix) {
                    return $idTypeVoix;
                } else {
                    return $typeVoix[$idTypeVoix];
                }
            } else {
                foreach ($typeVoix as $key => $value) {
                    return $key;
                }
            }
        } else {
            foreach ($typeVoix as $key => $value) {
                return $key;
            }
        }
    }

    public function getTypeVoixAgentOrdreJour($id, $returnIdTypeVoix = false)
    {
        $idOrdreJour = base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id']));
        $org = Atexo_CurrentUser::getCurrentOrganism();
        $intervenant = (new Atexo_Commission_IntervenantExterne())->getIntervenantOrdreJour($id, $idOrdreJour, $org, null, true);
        $typeVoix = $this->getViewState('typeVoix', []);
        if (!(is_array($typeVoix) && count($typeVoix) > 0)) {
            $typeVoix = (new Atexo_ValeursReferentielles())->getValeurRefByIdRef(Atexo_Config::getParameter('ID_REFERENTIEL_TYPE_VOIX_INTERVENANTS'));
            $this->setViewState('typeVoix', $typeVoix);
        }
        if ($intervenant instanceof CommonIntervenantOrdreDuJour) {
            $idTypeVoix = $intervenant->getTypeVoix();
            if ($idTypeVoix) {
                if ($returnIdTypeVoix) {
                    return $idTypeVoix;
                } else {
                    return $typeVoix[$idTypeVoix];
                }
            } else {
                foreach ($typeVoix as $key => $value) {
                    return $key;
                }
            }
        } else {
            foreach ($typeVoix as $key => $value) {
                return $key;
            }
        }
    }

    public function actionsRepeater($sender, $param)
    {
        if ('delete' == $param->CommandName) {
            $id = $param->CommandParameter;
            $this->Page->supprimer($id, $this->getIsIntervenant());
            $criteriaVo = $this->getViewState('CriteriaVo');
            $this->fillRepeaterWithDataForSearchResult($criteriaVo);
        }
    }

    public function getTypeCompteAgent($type)
    {
        if ($type) {
            return Prado::localize('TEXT_ELU');
        } else {
            return Prado::localize('TEXT_AGENT');
        }
    }

    public function getListeAllTypeVoix()
    {
        $typeVoix = $this->getViewState('typeVoix', []);
        if (!(is_array($typeVoix) && count($typeVoix) > 0)) {
            $typeVoix = (new Atexo_ValeursReferentielles())->getValeurRefByIdRef(Atexo_Config::getParameter('ID_REFERENTIEL_TYPE_VOIX_INTERVENANTS'));
            $this->setViewState('typeVoix', $typeVoix);
        }

        return $typeVoix;
    }

    public function getIntervenantSelectionnes()
    {
        $data = [];
        foreach ($this->tableauIntervenant->Items as $item) {
            if ($item->select->checked) {
                $id = $item->selectedIdIntervenant->value;
                $idTypeVoix = $item->selectTypeVoix->SelectedValue;
                $data[$id]['ID'] = $id;
                $data[$id]['TYPE_VOIX'] = $idTypeVoix;
            }
        }

        return $data;
    }
}
