<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_Util;

/**
 * Composant Calendrier réel d'une consultation.
 *
 * @author khadija CHOUIKA <khadija.chouika@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 4
 */
class CalendrierReel extends MpeTTemplateControl
{
    private $_consultation = false;
    private $_reference = false;

    public function getReference()
    {
        return $this->_reference;
    }

    public function setReference($reference)
    {
        return $this->_reference = $reference;
    }

    public function getConsultation()
    {
        return $this->_consultation;
    }

    public function setConsultation($consultation)
    {
        return $this->_consultation = $consultation;
    }

    public function onLoad($param)
    {
        if ($this->getConsultation()) {
            $consultationId = $this->getConsultation()->getId();
            if ($consultationId) {
                $this->TextJs->Text = "<script>
				 element = document.getElementById('ctl0_CONTENU_PAGE_calendrierReel_refConsultation');
				 element.value = '".base64_encode($consultationId)."';
				 afficheCalendrier();</script> ";
            }
            $this->calendrier->dateRemisePlis->Text = Atexo_Util::iso2frnDateTime($this->getConsultation()->getDatefin());
        }
    }
}
