<?php

namespace Application\Controls;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonCategorieLotPeer;
use Application\Propel\Mpe\CommonConsultation;
use Application\Service\Atexo\Atexo_ActeDEngagement;
use Application\Service\Atexo\Atexo_CategorieLot;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_ValeursReferentielles;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Inscrit;
use Application\Service\Atexo\Signature\Atexo_Signature_FichierVo;
use Prado\Prado;

/**
 * Page de gestion des formulaires de reponse pour les pieces typees.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 1.0
 *
 * @since MPE-4.0
 */
class AtexoReponsePiecesTypees extends MpeTTemplateControl
{
    public $_idHtlmToggleFormulaire;
    public $_typeEnv;
    public $_numLot;

    /**
     * Recupere la valeur.
     */
    public function getIdHtlmToggleFormulaire()
    {
        $idHtlmToggleFormulaire = $this->_idHtlmToggleFormulaire;
        if (!$idHtlmToggleFormulaire) {
            $idHtlmToggleFormulaire = $this->sourceTemplateControl->hiddenIdHtlmToggleFormulaire->Value;
        }

        return $idHtlmToggleFormulaire;
    }

    /**
     * Attribut la valeur.
     */
    public function setIdHtlmToggleFormulaire($value)
    {
        $this->_idHtlmToggleFormulaire = $value;
    }

    /**
     * Affectes la valeur.
     */
    public function setTypeEnv($value)
    {
        $this->_typeEnv = $value;
    }

    /**
     * Recupere la valeur.
     */
    public function getTypeEnv()
    {
        $typeEnv = $this->_typeEnv;
        if (!$typeEnv) {
            $typeEnv = $this->sourceTemplateControl->typeEnveloppe->Value;
        }

        return $typeEnv;
    }

    /**
     * Recupere la valeur.
     */
    public function getNumLot()
    {
        $numLot = $this->_numLot;
        if (!$numLot) {
            $numLot = $this->sourceTemplateControl->numLot->Value;
        }

        return $numLot;
    }

    /**
     * Affectes la valeur.
     */
    public function setNumLot($value)
    {
        $this->_numLot = $value;
    }

    /**
     * Chargement de la page.
     */
    public function onLoad($param)
    {
        if (!Atexo_Module::isEnabled('InterfaceModuleSub') && !$this->Page->IsPostBack && !$this->Page->IsCallBack) {
            if ($this->getTypeEnv() == Atexo_Config::getParameter('TYPE_ENV_OFFRE') && $this->isConsultationHasDc3()) {
                //Initialiser le dataSource
                $this->initDataSource();
                //Chargement des pieces typees
                $this->chargerPieceTypees();
            }
        }
    }

    /**
     * Permet de recuperer la consultation dans la session de la page.
     */
    public function getConsultation()
    {
        return $this->Page->getViewState('consultation');
    }

    public function getSignatureRequis()
    {
        if ($this->getConsultation() instanceof CommonConsultation) {
            return $this->getConsultation()->getSignatureOffre();
        }
    }

    /**
     * Recupere le dataSource.
     */
    public function getDataSource()
    {
        $dataSource = $this->Page->getViewState('dataSourcePieces');
        $dataSourceTypees = $dataSource[$this->getTypeEnv().'_'.$this->getNumLot()]['pieceTypees'];
        if (!$dataSourceTypees && $this->getTypeEnv() == Atexo_Config::getParameter('TYPE_ENV_OFFRE') && $this->isConsultationHasDc3()) {
            $this->initialiserDataSourcePieceTypees();

            return $this->getDataSource();
        } else {
            return $dataSourceTypees;
        }
    }

    /**
     * Affecte le dataSource.
     *
     * @param $arrayPieceTypees : liste des pieces typees
     */
    public function setDataSource($arrayPieceTypees)
    {
        $dataSource = $this->Page->getViewState('dataSourcePieces');
        $dataSource[$this->getTypeEnv().'_'.$this->getNumLot()]['pieceTypees'] = $arrayPieceTypees;
        $this->Page->setViewState('dataSourcePieces', $dataSource);
    }

    /**
     * Permet de charger la liste des pieces typees.
     */
    public function chargerPieceTypees()
    {
        $this->listePiecesTypees->DataSource = $this->getDataSource();
        $this->listePiecesTypees->DataBind();
    }

    /**
     * Permet d'initialiser le dataSource.
     */
    public function initDataSource()
    {
        if (!$this->getDataSource()) {
            $this->initialiserDataSourcePieceTypees();
        }
    }

    /**
     * Permet d'initialiser data source du piece typee.
     */
    public function initialiserDataSourcePieceTypees()
    {
        $arrayPiece = [];
        $fichierVo = new Atexo_Signature_FichierVo();
        $fichierVo->setIdentifiant($this->Page->getLastIdFichier());
        $fichierVo->setCheminFichier(Prado::localize('DEFINE_DC3').' - '.Prado::localize('AE'));
        $fichierVo->setTypeEnveloppe($this->getTypeEnv());
        $fichierVo->setNumeroLot($this->getNumLot());
        $fichierVo->setTypeFichier(Atexo_Config::getParameter('TYPE_DOCUMENT_FICHIER'));
        $fichierVo->setOrgineFichier(Atexo_Config::getParameter('TYPE_DOCUMENT_PIECE_TYPEE'));
        $arrayPiece[$fichierVo->getIdentifiant()] = $fichierVo;
        $this->setDataSource($arrayPiece);
    }

    /**
     * Permet de preciser si la consultation contient
     * un DC3 (Acte engagement).
     *
     * @return true: si oui, false sinon
     */
    public function isConsultationHasDc3()
    {
        if ($this->getConsultation() instanceof CommonConsultation && '1' == $this->getConsultation()->getSignatureActeEngagement()) {
            return true;
        }

        return false;
    }

    /**
     * Permet d'extraire le nom du fichier de la chaine contenant le chemin complet du fichier.
     */
    public function getNomFichier($cheminFicher)
    {
        $nomFichier = '';
        $arrayCheminFichier = explode('\\', $cheminFicher);
        if (is_array($arrayCheminFichier) && count($arrayCheminFichier)) {
            $nomFichier = $arrayCheminFichier[count($arrayCheminFichier) - 1];
        }

        return $nomFichier;
    }

    /**
     * Permet d'ajouter la piece typee.
     *
     * @param unknown_type $sender
     * @param unknown_type $param
     */
    public function ajouterPieceTypee($sender, $param)
    {
        $identifiantPiece = $param->CallbackParameter;
        $arraNameFiles = [];
        $erreurAjoutFile = false;
        $listAddedFiles = [];
        if (Atexo_Module::isEnabled('GenererActeDengagement')) {
            foreach ($this->listePiecesTypees->getItems() as $itemPiece) {
                if ($itemPiece->valueIdentifiantPiece->value == $identifiantPiece) {
                    $dataSource = $this->getDataSource();
                    $pieceTypee = $dataSource[$identifiantPiece];
                    if ($pieceTypee instanceof Atexo_Signature_FichierVo) {
                        $nomFichier = $this->getNomFichier($itemPiece->fichierAE->value);
                        $nomFichierExiste = $this->Page->isNomFichierExiste($this->getTypeEnv(), $this->getNumLot(), $nomFichier);
                        if ($nomFichierExiste) {
                            $arraNameFiles[] = $nomFichier;
                            $erreurAjoutFile = true;
                        } else {
                            $pieceTypee->setFichierAeAjoute(true);
                            $arrayPiece[$pieceTypee->getIdentifiant()] = $pieceTypee;
                            $pieceTypee->setCheminFichier($itemPiece->fichierAE->value);
                            $pieceTypee->setNomFichier($nomFichier);
                            $pieceTypee->setCheminFichierXmlAE($itemPiece->xmlAE->value);
                            //$pieceTypee->setNomFichierXmlAE($this->getNomFichier($itemPiece->xmlAE->value));
                            $this->setDataSource($arrayPiece);
                            //Chargement et raffraichissement des pieces de la reponse
                            $this->refresh($sender, $param);
                            $listAddedFiles[] = $nomFichier;
                        }
                    }
                }
            }
        } else {
            foreach ($this->listePiecesTypees->getItems() as $itemPiece) {
                $json = $itemPiece->listeJsonRetoursAppletPiecesSelectionnees->Value;
                //La fonction json_decode doit recevoir un json encode en utf8
                $json = Atexo_Util::httpEncodingToUtf8($json);
                $listeJsonPieces = json_decode($json, true, 512, JSON_THROW_ON_ERROR);
                if (is_array($listeJsonPieces['fichiers'])) {
                    if ($itemPiece->valueIdentifiantPiece->value == $identifiantPiece) {
                        $dataSource = $this->getDataSource();
                        $pieceTypee = $dataSource[$identifiantPiece];
                        if ($pieceTypee instanceof Atexo_Signature_FichierVo) {
                            $cheminFichier = '';
                            $infosSignature = '';
                            $cheminSignatureXml = '';
                            foreach ($listeJsonPieces['fichiers'] as $file) {
                                $cheminFichier = $file['cheminFichier'];
                                $infosSignature = $file['infosSignature'];
                                $cheminSignatureXml = $file['cheminSignatureXML'];
                                $tailleFichier = $file['tailleFichier'];
                            }
                            $nomFichier = $this->getNomFichier($cheminFichier);
                            $nomFichierExiste = $this->Page->isNomFichierExiste($this->getTypeEnv(), $this->getNumLot(), $nomFichier);
                            if ($nomFichierExiste) {
                                $arraNameFiles[] = $nomFichier;
                                $erreurAjoutFile = true;
                            } else {
                                $pieceTypee->setCheminFichier($cheminFichier);
                                $pieceTypee->setNomFichier($nomFichier);
                                $pieceTypee->setInfosSignature($infosSignature);
                                $pieceTypee->setCheminSignatureXML($cheminSignatureXml);
                                $pieceTypee->setFichierAeAjoute(true);
                                $pieceTypee->setTailleFichier($tailleFichier);
                                $arrayPiece[$pieceTypee->getIdentifiant()] = $pieceTypee;
                                $this->setDataSource($arrayPiece);
                                //Chargement et raffraichissement des pieces de la reponse
                                $this->refresh($sender, $param);
                                $listAddedFiles[] = $nomFichier;
                            }
                        }
                    }
                }
            }
        }
        if ($erreurAjoutFile) {
            $this->Page->afficherMessageErreurAjoutFile($arraNameFiles, $param);
        }
        $this->saveTraceInscrit('DESCRIPTION14', $listAddedFiles);
    }

    /**
     * Permet de raffraichir la page.
     *
     * @param unknown_type $sender
     * @param unknown_type $param
     */
    public function refresh($sender, $param)
    {
        $this->chargerPieceTypees();
        $this->panelListePiecesTypees->render($param->getNewWriter());
    }

    /**
     * Permet de retirer l'acte d'engagement.
     *
     * @param $sender
     * @param $param
     */
    public function retirerActeEngagement($sender, $param)
    {
        $listDeletedFiles = [];
        $arrayPiece = [];
        $dataSource = $this->getDataSource();
        $identifiantPiece = $param->CallbackParameter;
        $pieceTypee = $dataSource[$identifiantPiece];
        if ($pieceTypee instanceof Atexo_Signature_FichierVo) {
            $listDeletedFiles[] = $pieceTypee->getNomFichier();
            $pieceTypee->setFichierAeAjoute(false);
            $arrayPiece[$pieceTypee->getIdentifiant()] = $pieceTypee;
            $pieceTypee->setCheminFichier('');
            $pieceTypee->setNomFichier('');
            $pieceTypee->setCheminFichierXmlAE('');
            $pieceTypee->setCheminFichierSignatureXmlAE('');
            $pieceTypee->setInfosSignature(null);
            $this->setDataSource($arrayPiece);
            //Chargement et raffraichissement des pieces de la reponse
            $this->refresh($sender, $param);
        }
        $this->saveTraceInscrit('DESCRIPTION16', $listDeletedFiles);
    }

    /**
     * Permet de recuperer toutes les pieces typees cochees.
     */
    public function recupererPiecesCochees(&$listPiecesCoches)
    {
        $dataKeys = $this->listePiecesTypees->getDataKeys();
        foreach ($this->listePiecesTypees->getItems() as $itemEdition) {
            if ($itemEdition->idCheckBoxSelectionPiece->Checked) {
                $listPiecesCoches[] = $this->getFichierFromDataSource($dataKeys[$itemEdition->getItemIndex()]);
            }
        }
    }

    /**
     * Permet de recuperer un fichier de dataSource.
     *
     * @param int $identifiant de l'objet Atexo_Signature_FichierVo
     *
     * @return objet Atexo_Signature_FichierVo
     */
    public function getFichierFromDataSource($identifiant)
    {
        $arrayPiece = $this->getDataSource();

        return $arrayPiece[$identifiant];
    }

    /**
     * Recupere l'emetteur et le signataire (CN, AC).
     */
    public function getInfosSignataire($fichier)
    {
        return $this->SourceTemplateControl->getInfosSignataire($fichier);
    }

    /**
     * Recupere l'image du picto de la verification de la signature.
     */
    public function getPictoVerifSignature($fichier)
    {
        return $this->SourceTemplateControl->getPictoVerifSignature($fichier);
    }

    /**
     * Recupere l'image du picto si signature existe ou pas.
     */
    public function getPictoSignatureExiste($fichier)
    {
        return $this->SourceTemplateControl->getPictoSignatureExiste($fichier);
    }

    /**
     * Recupere le message alt du picto de la verification de la signature.
     */
    public function getAltVerifSignature($fichier)
    {
        return $this->SourceTemplateControl->getAltVerifSignature($fichier);
    }

    /**
     * Recupere le message alt du picto si signature existe ou pas.
     */
    public function getAltSignatureExiste($fichier)
    {
        return $this->SourceTemplateControl->getAltSignatureExiste($fichier);
    }

    /**
     * Recupere l'image du picto si signature existe ou pas.
     */
    public function afficherLienSignature($fichier)
    {
        return $this->SourceTemplateControl->afficherLienSignature($fichier);
    }

    /**
     * Permet de gerer la visibilite de la case à cocher pour la signature des fichiers de reponse.
     */
    public function getVisibiliteCocherSignature($fichier)
    {
        return $this->SourceTemplateControl->getVisibiliteCocherSignature($fichier);
    }

    /**
     * Permet d'afficher les details de la signature.
     */
    public function afficherDetailsSignature($sender, $param)
    {
        if ($param->CallbackParameter || 0 === $param->CallbackParameter) {
            $dataKeys = $this->listePiecesTypees->getDataKeys();
            $fichier = $this->getFichierFromDataSource($dataKeys[$param->CallbackParameter]);
            if ($fichier instanceof Atexo_Signature_FichierVo) {
                //L'information fichierVo sera exploitee lors de l'appel du composant <com:AtexoDetailsSignatureFormulaireReponse/>
                //dans le fichier AtexoReponseEnveloppe.tpl
                $this->Page->setViewState('fichierVo', $fichier);
                $this->Page->setViewState('identifiantFichier', $fichier->getIdentifiant());
                $script = "<script>
            				  openModal('modal-ajout-offre_".$fichier->getIdentifiant()."','popup-800',document.getElementById('".$this->Page->scriptJsPopInDetailsSignature->getClientId()."'));
            			  </script>";
            }
            //print_r($this->Page->getViewState("signatureFichier"));exit;
            $this->Page->raffraichirListeFormulaires($sender, $param);
            $this->Page->panelDetailsSignatureFormulaireReponse->render($param->getNewWriter());
            $this->Page->scriptJsPopInDetailsSignature->Text = $script;
        }
    }

    /**
     * Permet de recuperer l'xml de l'acte d'engagement.
     *
     * @param $idLot: le numero du lot
     */
    public function getXmlAE($idLot)
    {
        $lotObject = null;
        $inscritObject = (new Atexo_Entreprise_Inscrit())->retrieveInscritAndEntreprise(Atexo_CurrentUser::getIdInscrit());
        $inscritObject = array_shift($inscritObject);
        $entrepriseObject = $inscritObject->getEntreprise();
        $consultationObject = (new Atexo_Consultation())->retrieveConsultationForCompagny(Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_Util::atexoHtmlEntities($_GET['org']));
        $typeProcedure = $consultationObject->getTypeProcedure();
        if ('0' != $idLot) {
            $connectionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
            $lotObject = (new Atexo_CategorieLot())->retrieveByOrganismeConsultationIdNumero(Atexo_Util::atexoHtmlEntities($_GET['org']), $consultationObject->getId(), $idLot);
            //$lotObject = CommonCategorieLotPeer::retrieveByPK(Atexo_Util::atexoHtmlEntities($_GET['org']),$consultationObject->getId(), $idLot, $connectionCom);
        }

        return (new Atexo_ActeDEngagement())->preRemplirXmlAe(Atexo_Config::getParameter('PATH_XML_ACTE_D_ENGAGEMENT'), $consultationObject, $inscritObject, $entrepriseObject, $typeProcedure, $lotObject);
    }

    /* Permet de sauvgarder les traces des inscris
       * $description  String
       */
    public function saveTraceInscrit($descriptionCle, $listFiles = null)
    {
        if ($this->Page->alloti->value) {
            $message = Prado::localize('DEFINE_DE_OFFRE_DU_FORMULAIRE_ALLOTI');
            $message = str_replace('{num_lot}', $this->getNumLot(), $message);
        } else {
            $message = Prado::localize('DEFINE_DE_OFFRE_DU_FORMULAIRE');
        }
        $IdDesciption = (new Atexo_ValeursReferentielles())->retrieveByPk($descriptionCle, Atexo_Config::getParameter('DESCRIPTION_HISTORIQUES_INSCRITS'), true);
        $description = (new Atexo_ValeursReferentielles())->retrieveByIdValAndIdRef($IdDesciption, Atexo_Config::getParameter('DESCRIPTION_HISTORIQUES_INSCRITS'), true);
        $descri = str_replace('[_typeEnveloppeLot_]', $message, $description);

        if (is_countable($listFiles) ? count($listFiles) : 0) {
            $descri .= '<br/>- '.Atexo_Util::utf8ToIso(implode('<br/> - ', $listFiles));
        }
        $this->Page->saveTraceDiagInscrit($descriptionCle, false, $descri, $IdDesciption);
    }

    /*
     *  Permet de sauvgarder les erreurs de l'applet suite a l'ajout AE
     */
    public function traceHistoriqueNavigationInscrit($sender, $param)
    {
        if ($this->logAppletAjoutAE->value) {
            $this->Page->saveTraceDiagInscrit('DESCRIPTION23', $this->logAppletAjoutAE->value);
        }
    }

    /**
     * fichier a une signature.
     */
    public function getHasFileSignature($fichier)
    {
        return $this->SourceTemplateControl->getHasFileSignature($fichier);
    }
}
