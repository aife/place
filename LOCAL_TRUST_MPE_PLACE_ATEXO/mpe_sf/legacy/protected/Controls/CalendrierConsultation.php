<?php

namespace Application\Controls;

use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonDATEFIN;
use Application\Propel\Mpe\CommonOrganisme;
use Application\Propel\Mpe\CommonProcedureEquivalence;
use Application\Propel\Mpe\CommonService;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_Crypto;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_GestionCalendrier;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureEquivalence;
use Application\Service\Atexo\Publicite\Atexo_Publicite_AvisPub;
use Prado\Prado;

/**
 * Page de gestion du calendrier de la consultation.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 4
 */
class CalendrierConsultation extends MpeTTemplateControl
{
    public $consultation;
    public ?string $_affiche = null; // (=1 pour afficher, 0 sinon)
    public ?string $mode = null;
    public bool $_activationFuseauHoraire = false;

    /**
     * recupère la consultation.
     */
    public function getConsultation()
    {
        if (!($this->consultation instanceof CommonConsultation)) {
            $this->consultation = $this->Page->getConsultation();
        }

        return $this->consultation;
    }

    /**
     * Affecte l'objet de la consultation.
     *
     * @param objet $consultation
     */
    public function setConsultation($consultation)
    {
        if ($this->consultation !== $consultation) {
            $this->consultation = $consultation;
        }
    }

    // setConsultation()

    /**
     * recupère la valeur.
     */
    public function getAffiche()
    {
        return $this->_affiche;
    }

    // getAffiche()

    /**
     * Affecte la valeur.
     *
     * @param string $value
     */
    public function setAffiche($value)
    {
        if ($this->_affiche !== $value) {
            $this->_affiche = $value;
        }
    }

    // setAffiche()

    public function setActivationFuseauHoraire($value)
    {
        $this->_activationFuseauHoraire = $value;
    }

    public function getActivationFuseauHoraire()
    {
        return $this->_activationFuseauHoraire;
    }

    /**
     * Chargement de la page.
     */
    public function onLoad($param)
    {
        //activation du fuseau horaire
        $this->_activationFuseauHoraire = (new Atexo_EntityPurchase())->getEtatActivationFuseauHoraire(Atexo_CurrentUser::getIdServiceAgentConnected(), Atexo_CurrentUser::getCurrentOrganism());
        // remplir la date
        if (!$this->Page->isPostBack && $this->getConsultation()) {
            $this->fillComposants($this->getConsultation());
            $this->afficherMasquerBlocsEnFonctionParametrageProcedures($this->getConsultation());
        }
    }

    public function getHeureRemisDesPlisParDefaut(): int|string
    {
        //get config_organisme
        $heureRemisDesPlisParDefaut = Atexo_Config::getParameter('HEURE_CLOTURE_DEFAUT');
        $configOrganisme = (new Atexo_Module())->getModuleOrganismeByAcronyme(Atexo_CurrentUser::getCurrentOrganism());
        if ($configOrganisme instanceof CommonConfigurationOrganisme) {
            $heureRemisDesPlisParDefaut = $configOrganisme->getHeureLimiteDeRemiseDePlisParDefaut() ?? Atexo_Config::getParameter('HEURE_CLOTURE_DEFAUT');
        }

        return $heureRemisDesPlisParDefaut;
    }

    /**
     * recupère le mode.
     */
    public function getMode()
    {
        return $this->mode;
    }

    // getMode()

    /**
     * Affecte le mode.
     *
     * @param string $mode
     */
    public function setMode($mode)
    {
        if ($this->mode !== $mode) {
            $this->mode = $mode;
        }
    }

    // setMode()

    public function save(&$commonConsultationO, $modification = true)
    {
        if ($this->miseEnLigne->Checked) {
            $commonConsultationO->setDatemiseenligne(Atexo_Util::frnDateTime2iso($this->datemiseEnLigne->Text));
            $commonConsultationO->setRegleMiseEnLigne(Atexo_Config::getParameter('MISE_EN_LIGNE_APRES_VALIDATION_ET_DATE'));
        } elseif ($this->validationDate->Checked) {
            $commonConsultationO->setRegleMiseEnLigne(Atexo_Config::getParameter('MISE_EN_LIGNE_APRES_VALIDATION_MANUELLE'));
        } elseif ($this->dateEnvoiBOAMP->Checked) {
            $commonConsultationO->setRegleMiseEnLigne(Atexo_Config::getParameter('MISE_EN_LIGNE_APRES_VALIDATION_ET_ENVOI_BOAMP'));
        } elseif ($this->datePubBOAMP->Checked) {
            $commonConsultationO->setRegleMiseEnLigne(Atexo_Config::getParameter('MISE_EN_LIGNE_APRES_VALIDATION_ET_PUBLICATION_BOAMP'));
        } elseif (Atexo_Module::isEnabled('RegleMiseEnLigneParEntiteCoordinatrice') && $this->miseEnLigneParEntiteCoordinatrice->Checked) {
            $commonConsultationO->setDateMiseEnLigneSouhaitee(Atexo_Util::frnDateTime2iso($this->dateMiseEnLigneParEntiteCoordinatrice->Text));
            $commonConsultationO->setRegleMiseEnLigne(Atexo_Config::getParameter('MISE_EN_LIGNE_APRES_VALIDATION_ENTITE_COORDINATRICE'));
        }

        if (Atexo_Module::isEnabled('FuseauHoraire', Atexo_CurrentUser::getCurrentOrganism()) && $this->_activationFuseauHoraire) {
            $dateFinLocale = Atexo_Util::frnDateTime2iso($this->dateRemisePlisLocale->Text);
            $commonConsultationO->setDatefinLocale($dateFinLocale);
            $commonConsultationO->setLieuResidence($this->lieuResidence->Text);
        }

        if (empty(Atexo_Config::getParameter('ACTIVE_ELABORATION_V2'))) {
            if ('' != $this->dateRemisePlis->Text) {
                $dateFin = Atexo_Util::frnDateTime2iso($this->dateRemisePlis->Text);
            } else {
                $dateFin = '0000-00-00 00:00:00';
            }
            $dateFinOld = $commonConsultationO->getDatefin();
            $commonConsultationO->setDatefin($dateFin);
            //Historique date Fin
            $this->sauvegarderHistoriqueDateFin($commonConsultationO, $dateFin, $dateFinOld, $modification);
        }

        if ($this->jsFile->value) {
            (new Atexo_GestionCalendrier())->saveJson(base64_decode($this->jsFile->value));
        }
    }

    /**
     * Affiche le panel entite coordinatrice selon le module en relation.
     */
    public function showPanelEntiteCoordinatrice()
    {
        if (!Atexo_Module::isEnabled('RegleMiseEnLigneParEntiteCoordinatrice')) {
            $this->panelmiseEnLigneParEntiteCoordinatrice->setVisible(false);
        }
    }

    /**
     * Permet de remplir la date limite de remise des plis locale.
     */
    public function remplirDateLimiteRemisePlisLocale($sender, $param)
    {
        //Calcul de la date et heure limite de remise des plis locale
        if ($this->dateRemisePlis->Text && Atexo_Util::validateDate($this->dateRemisePlis->Text, 'd/m/Y H:i')) {
            $dateLimiteRemisePlisLocale = (new Atexo_Consultation())->calculDateLimiteRemisePlisLocale($this->dateRemisePlis->Text, $this->decalageFuseauHoraireService->value);
            //Remplissage de la zone de saisie de la date limite de remise des plis locale
            $this->dateRemisePlisLocale->setText(Atexo_Util::iso2frnDateTime($dateLimiteRemisePlisLocale, true));
        } else {
            $this->dateRemisePlisLocale->Text = '';
        }
    }

    public function fillComposants($consultation)
    {
        if ($consultation instanceof CommonConsultation) {
            $refConsultation = $consultation->getId();
            // affiche la panel d'entite coordnatrice
            $this->showPanelEntiteCoordinatrice();
            $this->miseEnLigne->setChecked(false);
            $this->validationDate->setChecked(false);
            $this->dateEnvoiBOAMP->setChecked(false);
            $this->datePubBOAMP->setChecked(false);
            if (Atexo_Module::isEnabled('RegleMiseEnLigneParEntiteCoordinatrice')) {
                $this->miseEnLigneParEntiteCoordinatrice->setChecked(false);
            }
            //Chargement des composants
            $this->chargerComposantsCalendrier($consultation);
            if ($refConsultation && Atexo_Module::isEnabled('CalendrierDeLaConsultation')) {
                $this->Page->scriptJs->Text .= "<script>
			 element = document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeCalendrier_refConsultation');
			 element.value = '".base64_encode($refConsultation)."';
			 J(window).on('load',function(){afficheCalendrier();});</script> ";
            }
        }
    }

    /**
     * Permet de calculer la date de remise des plis heure locale.
     *
     * @param $consultation: l'objet consultation
     *
     * @return : la date limite de remise des plis heure Locale
     */
    public function getDateRemisePlisLocale($consultation)
    {
        if (
            $consultation->getDatefinLocale()
            && '0000-00-00 00:00:00' != $consultation->getDatefinLocale()
            && empty(Atexo_Config::getParameter('ACTIVE_ELABORATION_V2'))
        ) {
            return Atexo_Util::iso2frnDateTime(substr($consultation->getDatefinLocale(), 0, 16));
        } else {
            if (empty(Atexo_Config::getParameter('ACTIVE_ELABORATION_V2'))) {
                $dateLimiteRemisePlis = $this->dateRemisePlis->Text;
            } else {
                $dateLimiteRemisePlis = $consultation->getDatefin();
            }
            if ($dateLimiteRemisePlis) {
                $dateLimiteRemisePlisLocale = (new Atexo_Consultation())->calculDateLimiteRemisePlisLocale($dateLimiteRemisePlis, $this->decalageFuseauHoraireService->value);

                return Atexo_Util::iso2frnDateTime($dateLimiteRemisePlisLocale, true);
            }
        }
    }

    /**
     * Permet de charger les composants du calendrier de la consultation.
     *
     * @param CommonConsultation $consultation
     */
    public function chargerComposantsCalendrier($consultation)
    {
        if ($consultation->getRegleMiseEnLigne() == Atexo_Config::getParameter('MISE_EN_LIGNE_APRES_VALIDATION_ET_DATE')) {
            $this->miseEnLigne->Checked = true;
            if ($consultation->getDatemiseenligne()) {
                $this->datemiseEnLigne->Text = Atexo_Util::iso2frnDateTime($consultation->getDatemiseenligne());
            }
        } else {
            if ($consultation->getRegleMiseEnLigne() == Atexo_Config::getParameter('MISE_EN_LIGNE_APRES_VALIDATION_MANUELLE')) {
                $this->validationDate->Checked = true;
            } else {
                if ($consultation->getRegleMiseEnLigne() == Atexo_Config::getParameter('MISE_EN_LIGNE_APRES_VALIDATION_ET_ENVOI_BOAMP')) {
                    $this->dateEnvoiBOAMP->Checked = true;
                } else {
                    if ($consultation->getRegleMiseEnLigne() == Atexo_Config::getParameter('MISE_EN_LIGNE_APRES_VALIDATION_ET_PUBLICATION_BOAMP')) {
                        $this->datePubBOAMP->Checked = true;
                    } elseif (Atexo_Module::isEnabled('RegleMiseEnLigneParEntiteCoordinatrice') && $consultation->getRegleMiseEnLigne() == Atexo_Config::getParameter('MISE_EN_LIGNE_APRES_VALIDATION_ENTITE_COORDINATRICE')) {
                        $this->miseEnLigneParEntiteCoordinatrice->Checked = true;
                        $this->dateMiseEnLigneParEntiteCoordinatrice->Text = Atexo_Util::iso2frnDateTime($consultation->getDateMiseEnLigneSouhaitee());
                    }
                }
            }
        }
            // la date et heure limite de remise des plis n'est pas reseignee lors d'une creation d'une suite
            if ('0000-00-00 00:00:00' !== $consultation->getDatefin()) {
                $this->dateRemisePlis->Text = Atexo_Util::iso2frnDateTime($consultation->getDatefin());
            }

        if (Atexo_Module::isEnabled('FuseauHoraire', Atexo_CurrentUser::getCurrentOrganism()) && $this->_activationFuseauHoraire) {
            $lieuResidence = '';
            $decalageFuseauHoraireService = '';
            //Cas de l'organisme
            $idService = Atexo_CurrentUser::getIdServiceAgentConnected();
            if (0 == $idService) {
                $objetOrganisme = Atexo_Organismes::retrieveOrganismeByAcronyme(Atexo_CurrentUser::getCurrentOrganism());
                if ($objetOrganisme instanceof CommonOrganisme) {
                    $lieuResidence = $objetOrganisme->getLieuResidence();
                    $decalageFuseauHoraireService = $objetOrganisme->getDecalageHoraire();
                }
            } else {
                $serviceAgentConnecte = (new Atexo_EntityPurchase())->getEntityById(Atexo_CurrentUser::getIdServiceAgentConnected(), Atexo_CurrentUser::getCurrentOrganism());
                if ($serviceAgentConnecte instanceof CommonService) {
                    $lieuResidence = $serviceAgentConnecte->getLieuResidence();
                    $decalageFuseauHoraireService = $serviceAgentConnecte->getDecalageHoraire();
                }
            }
            $this->lieuResidence->Text = $lieuResidence;
            $this->decalageFuseauHoraireService->value = $decalageFuseauHoraireService;
            if ($consultation->getLieuResidence()) {
                $this->lieuResidence->Text = $consultation->getLieuResidence();
            }
            if (self::getDateRemisePlisLocale($consultation)) {
                $this->dateRemisePlisLocale->Text = self::getDateRemisePlisLocale($consultation);
            }
        }
    }

    /**
     * Permet de sauvegarder l'historique de la date de fin.
     *
     * @param $consultation: objet de consultation
     * @param $dateFinNew: nouvelle date fin
     * @param $dateFinOld: ancienne date de fin
     * @param $objetDateFin: objet date fin
     */
    public function sauvegarderHistoriqueDateFin(&$consultation, $dateFinNew, $dateFinOld, $modification = true)
    {
        $objetDateFin = new CommonDATEFIN();
        if (('' != $dateFinNew && 0 != strcmp($dateFinOld, $dateFinNew)) || !$modification) {
            $dateTmpFile = Atexo_Config::getParameter('COMMON_TMP').'df'.session_id().time().'date.tmp';
            if ($modification) {
                Atexo_Util::write_file($dateTmpFile, 'Creation de la consultation -'.$consultation->getReferenceUtilisateur().'-, date de cloture : '.$dateFinNew);
            } else {
                Atexo_Util::write_file($dateTmpFile, 'Creation de la consultation -'.$consultation->getReferenceUtilisateur().'-, date de cloture : ');
            }
            if (is_file($dateTmpFile)) {
                $atexoCrypto = new Atexo_Crypto();
                $arrayTimeStampDateFin = $atexoCrypto->timeStampFile($dateTmpFile);
                if ($arrayTimeStampDateFin) {
                    $objetDateFin->setHorodatage($arrayTimeStampDateFin['horodatage']);
                    $objetDateFin->setUntrusteddate($arrayTimeStampDateFin['untrustedDate']);
                    $objetDateFin->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
                    $objetDateFin->setAgentId(Atexo_CurrentUser::getIdAgentConnected());
                    if ('' != $dateFinNew) {
                        $objetDateFin->setDatefin($dateFinNew);
                    }
                    if ($modification) {
                        $objetDateFin->setStatut(Atexo_Config::getParameter('MODIFICATION_FILE'));
                    } else {
                        $objetDateFin->setStatut(Atexo_Config::getParameter('CREATION_FILE'));
                    }
                    $consultation->addCommonDATEFIN($objetDateFin);
                }
            }
        }
    }

    /**
     * Permet d'afficher/masquer des blocs en fonction des valeurs parametrées dans les procédures équivalences.
     *
     * @param CommonConsultation $consultation
     */
    public function afficherMasquerBlocsEnFonctionParametrageProcedures($consultation = false, $procEquivalence = false)
    {
        if (!$procEquivalence) {
            $procEquivalence = Atexo_Consultation_ProcedureEquivalence::retrieveProcedureEquivalenceById($consultation->getIdTypeProcedureOrg());
            $fromProcedure = false;
        } else {
            $fromProcedure = true;
        }
        if ($procEquivalence instanceof CommonProcedureEquivalence) {
            $this->lignePanelmiseEnLigne->setStyle('NULL');
            $this->panel_lignePanelmiseEnLigne->setStyle('NULL');
            $this->displayElement('miseEnLigne', $procEquivalence->getMiseEnLigne1(), true, $fromProcedure, 'lignePanelmiseEnLigne', 'panel_lignePanelmiseEnLigne');
            $this->displayElement('validationDate', $procEquivalence->getMiseEnLigne2(), true, $fromProcedure, 'lignePanelmiseEnLigne', 'panel_lignePanelmiseEnLigne');
            $this->displayElement('dateEnvoiBOAMP', $procEquivalence->getMiseEnLigne3(), true, $fromProcedure, 'lignePanelmiseEnLigne', 'panel_lignePanelmiseEnLigne');
            $this->displayElement('datePubBOAMP', $procEquivalence->getMiseEnLigne4(), true, $fromProcedure, 'lignePanelmiseEnLigne', 'panel_lignePanelmiseEnLigne');
            $this->panelAvertissementMiseEnLigne->setStyle('display:none');
            if (Atexo_Module::isEnabled('RegleMiseEnLigneParEntiteCoordinatrice')) {
                $this->displayElement(
                    'miseEnLigneParEntiteCoordinatrice',
                    $procEquivalence->getMiseEnLigneEntiteCoordinatrice(),
                    true,
                    $fromProcedure,
                    'lignePanelmiseEnLigne',
                    'panel_lignePanelmiseEnLigne'
                );
                if (Atexo_Module::isEnabled('PubliciteOpoce') && Atexo_Config::getParameter('NB_HEURES_DECALAGE_MISE_EN_LIGNE_CONSULTATION')) {
                    $typeavis = (new Atexo_Publicite_AvisPub())->isTypeAvisEuropean($procEquivalence->getIdTypeProcedure(), $procEquivalence->getOrganisme());
                    if ($typeavis) {
                        $this->panelAvertissementMiseEnLigne->setStyle('display:');
                        $this->panelAvertissement->setMessage(str_replace('[__DECALAGE_MISE_EN_LIGNE__]', Atexo_Config::getParameter('NB_HEURES_DECALAGE_MISE_EN_LIGNE_CONSULTATION'), Prado::localize('MSG_AVERTISSEMENT_DATE_MISE_EN_LIGNE_ENTITE_COORDINATRICE')));
                    }
                }
            }
        }
    }

    /**
     * Permet d'afficher/masquer des composants.
     *
     * @param $element: l'element à afficher/masquer
     * @param $values: valeur (+1= afficher+cocher ...)
     * @param $isChecked
     */
    public function displayElement($element, $values, $isChecked = true, $fromProcedure = false, $ligneAllElements = false, $panelGlobale = false)
    {
        $panel = 'panel'.$element;
        if (!strcmp($values, '0')) {
            $this->$panel->setStyle('display:');
            $this->$element->SetEnabled(false);
            if ($isChecked && $fromProcedure) {
                $this->$element->SetChecked(false);
            }
            if ($ligneAllElements && ('display:none' == $this->$ligneAllElements->getStyle()->getCustomStyle() || 'NULL' == $this->$ligneAllElements->getStyle()->getCustomStyle())) {
                $this->$ligneAllElements->setStyle('display:');
            }
            if ($panelGlobale && ('display:none' == $this->$panelGlobale->getStyle()->getCustomStyle() || 'NULL' == $this->$panelGlobale->getStyle()->getCustomStyle())) {
                $this->$panelGlobale->setStyle('display:');
            }
        } elseif (!strcmp($values, '1')) {
            $this->$panel->setStyle('display:');
            $this->$element->SetEnabled(false);
            if ($isChecked && $fromProcedure) {
                $this->$element->SetChecked(true);
            }
            if ($ligneAllElements && ('display:none' == $this->$ligneAllElements->getStyle()->getCustomStyle() || 'NULL' == $this->$ligneAllElements->getStyle()->getCustomStyle())) {
                $this->$ligneAllElements->setStyle('display:');
            }
            if ($panelGlobale && ('display:none' == $this->$panelGlobale->getStyle()->getCustomStyle() || 'NULL' == $this->$panelGlobale->getStyle()->getCustomStyle())) {
                $this->$panelGlobale->setStyle('display:');
            }
        } elseif (!strcmp($values, '+1')) {
            $this->$panel->setStyle('display:');
            if ($isChecked) {
                $this->$element->SetEnabled(true);
                if ($fromProcedure) {
                    $this->$element->SetChecked(true);
                }
            }
            if ($ligneAllElements && ('display:none' == $this->$ligneAllElements->getStyle()->getCustomStyle() || 'NULL' == $this->$ligneAllElements->getStyle()->getCustomStyle())) {
                $this->$ligneAllElements->setStyle('display:');
            }
            if ($panelGlobale && ('display:none' == $this->$panelGlobale->getStyle()->getCustomStyle() || 'NULL' == $this->$panelGlobale->getStyle()->getCustomStyle())) {
                $this->$panelGlobale->setStyle('display:');
            }
        } elseif (!strcmp($values, '+0')) {
            $this->$panel->setStyle('display:');
            if ($isChecked) {
                $this->$element->SetEnabled(true);
                if ($fromProcedure) {
                    $this->$element->SetChecked(false);
                }
            }
            if ($ligneAllElements && ('display:none' == $this->$ligneAllElements->getStyle()->getCustomStyle() || 'NULL' == $this->$ligneAllElements->getStyle()->getCustomStyle())) {
                $this->$ligneAllElements->setStyle('display:');
            }
            if ($panelGlobale && ('display:none' == $this->$panelGlobale->getStyle()->getCustomStyle() || 'NULL' == $this->$panelGlobale->getStyle()->getCustomStyle())) {
                $this->$panelGlobale->setStyle('display:');
            }
        } elseif (!strcmp($values, '-1')) {
            $this->$panel->setStyle('display:none');
            if ($isChecked && $fromProcedure) {
                $this->$element->SetChecked(true);
            }
            if ($ligneAllElements && ('NULL' == $this->$ligneAllElements->getStyle()->getCustomStyle())) {
                $this->$ligneAllElements->setStyle('display:none');
            }
            if ($panelGlobale && ('NULL' == $this->$panelGlobale->getStyle()->getCustomStyle())) {
                $this->$panelGlobale->setStyle('display:none');
            }
        } elseif (!strcmp($values, '-0')) {
            $this->$panel->setStyle('display:none');
            if ($isChecked && $fromProcedure) {
                $this->$element->SetChecked(false);
            }
            if ($ligneAllElements && ('NULL' == $this->$ligneAllElements->getStyle()->getCustomStyle())) {
                $this->$ligneAllElements->setStyle('display:none');
            }
            if ($panelGlobale && ('NULL' == $this->$panelGlobale->getStyle()->getCustomStyle())) {
                $this->$panelGlobale->setStyle('display:none');
            }
        }
    }
}