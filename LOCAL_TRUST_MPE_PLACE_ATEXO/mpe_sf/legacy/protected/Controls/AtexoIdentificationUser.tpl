<!--Debut Bloc Compte administrateur de l'entreprise-->
		<com:TPanel ID="panelInformationsPersonelles" cssClass="form-field">
			<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><span class="title"><com:TTranslate>IDENTITE_DU_COMPTE_ENTREPRISE</com:TTranslate></span></div>
			<div class="content">
				<div class="spacer-mini"></div>
				<div class="bloc-50pourcent">
					<!-- -->
					<div class="line">
						<div class="intitule-150"><label for="nomPerso"><com:TTranslate>DEFINE_NOM</com:TTranslate></label><span class="champ-oblig">* </span> :</div>
						<com:TTextBox id="nomPerso" Attributes.title="<%=Prado::localize('DEFINE_NOM')%>" cssClass="input-185" />
						<com:TRequiredFieldValidator 
							ControlToValidate="nomPerso"
							ValidationGroup="validateCreateCompte" 
							Display="Dynamic"
							ErrorMessage="<%=Prado::localize('DEFINE_NOM')%>"
							EnableClientScript="true"  						 					 
				 			Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
							<prop:ClientSide.OnValidationError>
   				 				document.getElementById('divValidationSummary').style.display='';
 							</prop:ClientSide.OnValidationError>
         				</com:TRequiredFieldValidator>
					</div>
					<!-- -->
					<div class="line">
						<div class="intitule-150"><label for="prenomPerso"><com:TTranslate>DEFINE_PRENOM</com:TTranslate></label><span class="champ-oblig">* </span> :</div>
						<com:TTextBox id="prenomPerso" Attributes.title="<%=Prado::localize('DEFINE_PRENOM')%>" cssClass="input-185" />
						<com:TRequiredFieldValidator 
							ControlToValidate="prenomPerso"
							ValidationGroup="validateCreateCompte" 
							Display="Dynamic"
							ErrorMessage="<%=Prado::localize('DEFINE_PRENOM')%>"
							EnableClientScript="true"  						 					 
				 			Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
							<prop:ClientSide.OnValidationError>
   				 				document.getElementById('divValidationSummary').style.display='';
 							</prop:ClientSide.OnValidationError>
         				</com:TRequiredFieldValidator>
					</div>
					<!-- -->
					<div class="line">
						<div class="intitule-150"><label for="emailPersonnel"><com:TTranslate>TEXT_ADRESSE_ELECTRONIQUE</com:TTranslate></label> <span class="champ-oblig">* </span> :</div>
						<com:TTextBox id="emailPersonnel" Attributes.title="<%=Prado::localize('TEXT_ADRESSE_ELECTRONIQUE')%>" cssClass="input-185" />
						<com:TRequiredFieldValidator 
							ControlToValidate="emailPersonnel"
							ValidationGroup="validateCreateCompte" 
							Display="Dynamic"
							ErrorMessage="<%=Prado::localize('TEXT_ADRESSE_ELECTRONIQUE')%>"
							EnableClientScript="true"  						 					 
				 			Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
							<prop:ClientSide.OnValidationError>
   				 				document.getElementById('divValidationSummary').style.display='';
 							</prop:ClientSide.OnValidationError>
         				</com:TRequiredFieldValidator>
     					<com:TCustomValidator
                    			ControlToValidate="emailPersonnel" 
                    			ValidationGroup="validateCreateCompte" 
                    			Display="Dynamic" 
                    			EnableClientScript="true"
                    			ID="emailCreationValidator"
                    			onServerValidate="Page.panelInscrit.verifyMailCreation"
                    			Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
                    	</com:TCustomValidator>
                    	<com:TCustomValidator
    						ControlToValidate="emailPersonnel" 
    						ValidationGroup="validateCreateCompte" 
    						ID="emailModificationValidator"
    						EnableClientScript="true"
    						onServerValidate="Page.panelInscrit.verifyMailModification" 
    						Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
    					</com:TCustomValidator>
         				<com:TCustomValidator 
						    ClientValidationFunction="validatorEmail"  
						    ID="validateurMail"
							ControlToValidate="emailPersonnel"
							ValidationGroup="validateCreateCompte"
							Display="Dynamic"
							ErrorMessage="<%=Prado::localize('TEXT_ADRESSE_ELECTRONIQUE')%>"
							Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
						> 
							 <prop:ClientSide.OnValidationError>
                                document.getElementById('divValidationSummary').style.display='';
	                         </prop:ClientSide.OnValidationError>
         		        </com:TCustomValidator> 
					</div>
					<!-- -->
				</div>
				<div class="breaker"></div>
			</div>
			<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
		</com:TPanel>
		<!--Fin Bloc Compte administrateur de l'entreprise-->
          <!--Debut Bloc Conformite dossier presente-->
        	<com:TPanel ID="conformiteDossierPresente" cssClass="form-field">
        		<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><span class="title"><com:TTranslate>CONFROMITE_DOSSIER_DE_ENTREPRISE</com:TTranslate></span></div>
        		<div class="content">
        			<div class="spacer-mini"></div>
        			<div class="content-bloc bloc-600 indent-20">
        				<div class="intitule-auto bloc-500">
	        				<com:TRadioButton id="ouiConforme" GroupName="RadioGroup"
        						Text="<%=Prado::localize('OUI_CONFORMITE_DOSSIER_DE_ENTREPRISE')%>"
        						Attributes.title="<%=Prado::localize('OUI_CONFORMITE_DOSSIER_DE_ENTREPRISE')%>" cssClass="radio" />
        						<span class="champ-oblig">* </span>
	        			   		<com:TCustomValidator
									ClientValidationFunction="validateConformiteDossierEntreprise"
									ID="validatorConforme" 
									ControlToValidate="ouiConforme"
									ValidationGroup="validateCreateCompte" 
									Display="Dynamic"
									ErrorMessage="<%=Prado::localize('CONFROMITE_DOSSIER_DE_ENTREPRISE')%>"
									EnableClientScript="true"  						 					 
						 			Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
									<prop:ClientSide.OnValidationError>
		   				 				document.getElementById('divValidationSummary').style.display='';
		 							</prop:ClientSide.OnValidationError>
	         					</com:TCustomValidator>
	         			 </div>
        			   <div class="breaker"></div>
        				<div class="intitule-auto bloc-500">
        					<com:TRadioButton  id="nonConforme" GroupName="RadioGroup" 
	        					 Text="<%=Prado::localize('NON_CONFORMITE_DOSSIER_DE_ENTREPRISE')%>"
	        					 Attributes.title="<%=Prado::localize('NON_CONFORMITE_DOSSIER_DE_ENTREPRISE')%>" cssClass="radio"  />
        			   </div>
        			</div>
        			<div class="spacer-mini"></div>
        			<div class="breaker"></div>
        		</div>
        		<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
        	</com:TPanel>
            <!--Fin Bloc Informations Profil-->
		<!--Debut Bloc Elements identification-->
		<com:TPanel ID="panelElementIdentification" cssClass="form-field">
			<div class="top">
				<span class="left">&nbsp;</span>
				<span class="right">&nbsp;</span><span class="title"><com:TTranslate>ELEMENT_IDENTIFICATION_ATTRIBUE</com:TTranslate></span>
				</div>
				<div class="content">
				<div class="spacer-mini"></div>
				<div class="bloc-50pourcent">
				</div>
				<div class="line">
					<div class="intitule-150"><label for="identifiant"><com:TTranslate>TEXT_IDENTIFIANT_SANS_POINTS</com:TTranslate></label><span class="champ-oblig">* </span>  :</div>
					<com:TTextBox id="identifiant" Attributes.title="<%=Prado::localize('TEXT_IDENTIFIANT_SANS_POINTS')%>" cssClass="input-185" />
						<com:TRequiredFieldValidator 
							ControlToValidate="identifiant"
							ValidationGroup="validateCreateCompte" 
							Display="Dynamic"
							ErrorMessage="<%=Prado::localize('TEXT_IDENTIFIANT_SANS_POINTS')%>"
							EnableClientScript="true"  						 					 
				 			Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
							<prop:ClientSide.OnValidationError>
   				 				document.getElementById('divValidationSummary').style.display='';
 							</prop:ClientSide.OnValidationError>
         				</com:TRequiredFieldValidator>
 						<com:TCustomValidator
 						    ControlToValidate="identifiant"
							ValidationGroup="validateCreateCompte" 
    						Display="Dynamic" 
    						EnableClientScript="false"
    						ID="loginCreationValidator"
    						onServerValidate="Page.panelInscrit.verifyLoginCreation"
    						Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
 						</com:TCustomValidator>
 						<com:TCustomValidator
    						ControlToValidate="identifiant" 
    						ValidationGroup="validateCreateCompte" 
    						ID="loginModificationValidator"
    						EnableClientScript="true"
    						onServerValidate="Page.panelInscrit.verifyLoginModification"
    						Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
    					</com:TCustomValidator>
				</div>
				<com:TPanel cssClass="line" visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('GenerationAutomatiqueMdpInscrit'))? true:false%>">
					<div class="intitule-150"><label for="password"><com:TTranslate>TEXT_PASSWORD</com:TTranslate></label><span class="champ-oblig">* </span>  :</div>
					<div class="content-bloc bloc-600">
						<div class="line">
							<com:TRadioButton GroupName="genererPwd" id="pwdManuel" checked="true" CssClass="radio" Attributes.onclick="isCheckedShowDiv(this,'panel_PwdManuel');" Attributes.title="<%=Prado::localize('SAISIE_MANUELLE_PWD')%>"   />
							<label for="actif"><com:TTranslate>SAISIE_MANUELLE_PWD</com:TTranslate></label>
						</div>
						<div class="line">
							<com:TRadioButton  GroupName="genererPwd" id="pwdAutomatique" CssClass="radio" Attributes.onclick="isCheckedHideDiv(this,'panel_PwdManuel');"Attributes.title="<%=Prado::localize('GENERATION_ALEATOIRE_PWD')%>"  />
							<label for="verouille"><com:TTranslate>GENERATION_ALEATOIRE_PWD</com:TTranslate></label>
						</div>
					</div>
				</com:TPanel>
				<!--  -->
				<div id="panel_PwdManuel">
					<div class="line">
						<div class="line-20pourcent">
								<com:TPanel visible="<%= ! (Application\Service\Atexo\Atexo_Module::isEnabled('GenerationAutomatiqueMdpInscrit'))%>">
									<div class="intitule-150"><label for="password"><com:TTranslate>TEXT_PASSWORD</com:TTranslate></label><span class="champ-oblig">* </span>  :</div>
								</com:TPanel>
								<com:TPanel visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('GenerationAutomatiqueMdpInscrit'))? true:false%>">
									<div class="intitule-150">&nbsp;</div>
								</com:TPanel>
								<com:TTextBox PersistPassword="true" TextMode="Password"  id="password" Attributes.placeholder="<%=Prado::localize('SAISIR_MOT_DE_PASSE')%>" Attributes.title="<%=Prado::localize('TEXT_PASSWORD')%>" cssClass="input-185" Attributes.autocomplete="off"/>
							<com:TRequiredFieldValidator
									ControlToValidate="password"
									ValidationGroup="validateCreateCompte" 
									Display="Dynamic"
									visible="DisableValidatorPwd('<%=$this->pwdAutomatique->getClientId()%>')"
									ErrorMessage="<%=Prado::localize('TEXT_PASSWORD')%>"
									EnableClientScript="true"  						 					 
						 			Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
									<prop:ClientSide.OnValidate>
										if(document.getElementById('<%=$this->pwdAutomatique->getClientId()%>').checked == true){
											sender.enabled = false;
										}
		 							</prop:ClientSide.OnValidate>
									<prop:ClientSide.OnValidationError>
		   				 				document.getElementById('divValidationSummary').style.display='';
		 							</prop:ClientSide.OnValidationError>
		         				</com:TRequiredFieldValidator>
						</div>
					</div>
				<!--  -->
					<div class="line">
						<div class="line-20pourcent">
							<div class="intitule-150">&nbsp;</div>
							<com:TTextBox PersistPassword="true" TextMode="Password" id="confPassword" Attributes.placeholder="<%=Prado::localize('TEXT_CONFIRME_MOT_PASSE_SANS_POINTS')%>" Attributes.title="<%=Prado::localize('TEXT_CONFIRME_MOT_PASSE_SANS_POINTS')%>" cssClass="input-185" />
								<com:TRequiredFieldValidator 
									ControlToValidate="confPassword"
									ValidationGroup="validateCreateCompte" 
									Display="Dynamic"
									visible="DisableValidatorPwd('<%=$this->pwdAutomatique->getClientId()%>')"
									ErrorMessage="<%=Prado::localize('TEXT_CONFIRMATION_MOT_PASSE')%>"
									EnableClientScript="true"  						 					 
						 			Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
						 			<prop:ClientSide.OnValidate>
										if(document.getElementById('<%=$this->pwdAutomatique->getClientId()%>').checked == true){
											sender.enabled = false;
										}
		 							</prop:ClientSide.OnValidate>   
									<prop:ClientSide.OnValidationError>
		   				 				document.getElementById('divValidationSummary').style.display='';
		 							</prop:ClientSide.OnValidationError>
		         				</com:TRequiredFieldValidator>
		 						<com:TCompareValidator
		    						ValidationGroup="validateCreateCompte" 
		    						ID="confPasswordCompareValidator"
		    						ControlToValidate="password"
		    						ControlToCompare="confPassword"
		    						 DataType="String" 
		    						 Display="Dynamic"
		    						Operator="Equal"
		    						visible="DisableValidatorPwd('<%=$this->pwdAutomatique->getClientId()%>')"
		    						ErrorMessage="<%=Prado::localize('TEXT_DEUX_PASSWORD_DIFF')%>"
		    						Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
		    						<prop:ClientSide.OnValidate>
										if(document.getElementById('<%=$this->pwdAutomatique->getClientId()%>').checked == true){
											sender.enabled = false;
										}
		 							</prop:ClientSide.OnValidate>   
									<prop:ClientSide.OnValidationError>
		   				 				document.getElementById('divValidationSummary').style.display='';
		 							</prop:ClientSide.OnValidationError>
		 							</com:TCompareValidator>
						</div>
					</div>
				</div>
				<!--  -->
				<div class="breaker"></div>
			</div>
			<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
		</com:TPanel>
		<!--Fin Bloc Elements identification-->
		<!--Debut Bloc Informations Profil-->
        	<com:TPanel ID="InfoProfil" cssClass="form-field">
        		<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><span class="title"><com:TTranslate>DEFINE_PROFIL_USER_RNTREPRISE</com:TTranslate></span></div>
        		<div class="content">
        			<div class="spacer-mini"></div>
        			<div class="content-bloc bloc-600 indent-20">
        				<div class="intitule-auto bloc-500">
        					<com:TRadioButton  id="inscritSimple" GroupName="RadioGroupUser"  Attributes.title="<%=Prado::localize('DEFINE_UES_SIMPLE')%>" cssClass="radio"  />
        					<label for="inscritSimple">
        						<com:TTranslate>DEFINE_UES_SIMPLE</com:TTranslate>
        					</label>
        			   </div>
        			   <div class="breaker"></div>
        				<div class="intitule-auto bloc-500">
        					<com:TRadioButton id="adminEntreprise" GroupName="RadioGroupUser" Attributes.title="<%=Prado::localize('DEFINE_UES_SIMPLE')%>" cssClass="radio" />
        						<label for="adminEntreprise">
        							<com:TTranslate>DEFINE_ATES_ENTREPRISE</com:TTranslate>
        					</label>
        			   </div>
        			</div>
        			<div class="spacer-mini"></div>
        			<div class="breaker"></div>
        		</div>
        		<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
        	</com:TPanel>
            <!--Fin Bloc Informations Profil-->