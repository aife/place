<!--Debut Bloc Modalites de reponse-->
	<com:TPanel ID="summaryVisible" CssClass="form-bloc recap-infos-consultation">
		<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
		<div class="content">
				<div class="title-toggle" onclick="togglePanel(this,'modalitesReponses');">&nbsp;</div>
				<div class="recap-bloc">
					<h2><com:TTranslate>DEFINE_MODALITE_DE_REPONSE</com:TTranslate></h2>
					<div class="breaker"></div>
					<div class="panel-toggle" style="display:none;" id="modalitesReponses">
							<div class="line">
								<div class="intitule-250 bold"><com:TTranslate>REPONSE_ELEC</com:TTranslate> : </div>
								<com:TLabel ID="reponseElectronique"/>
							</div>
							<div class="line">
								<div class="intitule-250 bold"><com:TTranslate>DEFINE_SIGNATURE_ELECTRONIQUE_REQUISE</com:TTranslate></div>
								<com:TLabel ID="signatureElectroniqueRequise"/>
							</div>
							<div class="line">
								<div class="intitule-250 bold"><com:TTranslate>DEFINE_MODE_OUVERTURE</com:TTranslate> : </div>
								<com:TLabel ID="modeOuvertureReponseElectronique"/>
							</div>
							<div class="line">
								<div class="intitule-250 bold"><com:TTranslate>DEFINE_CHIFFREMENT_PLIS</com:TTranslate></div><com:TLabel ID="chiffremetPlis"/>
							</div>
							
							<com:TPanel id="constitutionReponse" visible="false">
							<div class="line">
								<div class="intitule-250 bold"><com:TTranslate>DEFINE_CONSTITUTION_DOSSIERS_REPONSES</com:TTranslate> : </div>
								<div class="content-bloc bloc-450">
									<com:TLabel ID="enveloppe1"/>
									<com:TPanel id="detail1" Visible = "false" CssClass="picto-link inline indent-5"><img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-details-grey.gif" alt="<%=Prado::localize('TEXT_DETAIL_CLES')%>" title="<%=Prado::localize('TEXT_DETAIL_CLES')%>"><a href="javascript:popUp('index.php?page=Agent.PopUpListesCles&typeEnv=1&id=<%=$this->getReference()%>','yes');"><com:TTranslate>TEXT_CLES</com:TTranslate></a></com:TPanel>
									<div class="breaker"></div>
									<com:TLabel ID="enveloppe4"/>
									<com:TPanel id="detail4" Visible = "false" CssClass="picto-link inline indent-5"><img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-details-grey.gif" alt="<%=Prado::localize('TEXT_DETAIL_CLES')%>" title="<%=Prado::localize('TEXT_DETAIL_CLES')%>"><a href="javascript:popUp('index.php?page=Agent.PopUpListesCles&typeEnv=4&id=<%=$this->getReference()%>','yes');"><com:TTranslate>TEXT_CLES</com:TTranslate></a></com:TPanel>
									<div class="breaker"></div>
									<com:TLabel ID="enveloppe2"/>
									<com:TPanel id="detail2" Visible = "false" CssClass="picto-link inline indent-5"><img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-details-grey.gif" alt="<%=Prado::localize('TEXT_DETAIL_CLES')%>" title="<%=Prado::localize('TEXT_DETAIL_CLES')%>"><a href="javascript:popUp('index.php?page=Agent.PopUpListesCles&typeEnv=2&id=<%=$this->getReference()%>','yes');"><com:TTranslate>TEXT_CLES</com:TTranslate></a></com:TPanel>
									<div class="breaker"></div>
									<com:TLabel ID="enveloppe3"/>
									<com:TPanel id="detail3" Visible = "false" CssClass="picto-link inline indent-5"><img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-details-grey.gif" alt="<%=Prado::localize('TEXT_DETAIL_CLES')%>" title="<%=Prado::localize('TEXT_DETAIL_CLES')%>"><a href="javascript:popUp('index.php?page=Agent.PopUpListesCles&typeEnv=3&id=<%=$this->getReference()%>','yes');"><com:TTranslate>TEXT_CLES</com:TTranslate></a></com:TPanel>
								</div>
							</div>
							</com:TPanel>
							
							<div class="breaker"></div>
					</div>
				</div>
	 			<div class="breaker"></div>
		 </div>
		 <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
	</com:TPanel>
<!--Fin Bloc Modalites de reponse-->