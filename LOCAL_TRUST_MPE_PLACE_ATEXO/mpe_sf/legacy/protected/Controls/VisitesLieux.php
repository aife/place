<?php

namespace Application\Controls;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonTDonneeComplementaire;
use Application\Propel\Mpe\CommonVisiteLieux;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;

/**
 * Page de gestion des visites des lieux.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 4
 */
class VisitesLieux extends MpeTTemplateControl
{
    private $_afficher;
    private $_donneesComplementaires;
    private ?string $_langue = null;
    private $_consultation;
    private $_validationGroup;
    private $_validationSummary;
    private ?string $_sourcePage = null;
    private $_lot;
    private bool|string $_actif = true;

    /**
     * recupère la valeur de afficher.
     */
    public function getAfficher()
    {
        if (null === $this->_afficher) {
            $this->_afficher = true;
        }

        return $this->_afficher;
    }

    // getAfficher()

    /**
     * Affecte la valeur de afficher.
     *
     * @param string $afficher
     */
    public function setAfficher($afficher)
    {
        if ($this->_afficher !== $afficher) {
            $this->_afficher = $afficher;
        }
    }

    // setAfficher()

    /**
     * recupère l'objet données complémentaires.
     */
    public function getDonneesComplementaires()
    {
        return $this->_donneesComplementaires;
    }

    // getDonneesComplementaires()

    /**
     * recupère la valeur de afficher.
     */
    public function getLot()
    {
        if (null === $this->_lot) {
            return $this->getViewState('numLot');
        }

        return $this->_lot;
    }

    // getLot()

    /**
     * Affecte la valeur de afficher.
     *
     * @param string $afficher
     */
    public function setLot($lot)
    {
        if ($this->_lot !== $lot) {
            $this->_lot = $lot;
        }
    }

    // setAfficher()

    /**
     * Affecte l'objet données complémentaires.
     *
     * @param CommonTDonneesComplementaires $donneesComplementaires
     */
    public function setDonneesComplementaires($donneesComplementaires)
    {
        if ($this->_donneesComplementaires !== $donneesComplementaires) {
            $this->_donneesComplementaires = $donneesComplementaires;
        }
    }

    // setDonneesComplementaires()

    /**
     * recupère la langue.
     */
    public function getLangue()
    {
        return $this->_langue;
    }

    // getLangue()

    /**
     * Affecte la langue.
     *
     * @param string $langue
     */
    public function setLangue($langue)
    {
        if ($this->_langue !== $langue) {
            $this->_langue = $langue;
        }
    }

    // setLangue()

    /**
     * recupère la consultation.
     */
    public function getConsultation()
    {
        return $this->_consultation;
    }

    // getConsultation()

    /**
     * Affecte l'objet de la consultation.
     *
     * @param objet $consultation
     */
    public function setConsultation($consultation)
    {
        $this->_consultation = $consultation;
    }

    // setConsultation()

    /**
     * Affecte le groupe de validation.
     *
     * @param $validationGroup
     */
    public function setValidationGroup($validationGroup)
    {
        $this->_validationGroup = $validationGroup;
    }

    /**
     * Récupère le groupe de validation.
     */
    public function getValidationGroup()
    {
        return $this->_validationGroup;
    }

    /**
     * Affecte le composant.
     *
     * @param $validationSummary
     */
    public function setValidationSummary($validationSummary)
    {
        $this->_validationSummary = $validationSummary;
    }

    /**
     * Récupère le composant.
     */
    public function getValidationSummary()
    {
        return $this->_validationSummary;
    }

    /**
     * recupère la valeur.
     */
    public function getSourcePage()
    {
        return $this->_sourcePage;
    }

    // getSourcePage()

    /**
     * Affecte la valeur.
     *
     * @param string $value
     */
    public function setSourcePage($value)
    {
        if ($this->_sourcePage !== $value) {
            $this->_sourcePage = $value;
        }
    }

    // setSourcePage()

    /**
     * recupère la valeur.
     */
    public function getActif()
    {
        return $this->_actif;
    }

    // getActif()

    /**
     * Affecte la valeur.
     *
     * @param string $value
     */
    public function setActif($value)
    {
        if ($this->_actif !== $value) {
            $this->_actif = $value;
        }
    }

    // setActif()

    /*
     * Chargement du composant
     */
    public function onLoad($param)
    {
        $this->chargerComposant();
    }

    /**
     * Permet de charger le composant.
     */
    public function chargerComposant($recharger = false)
    {
        $this->customize();
        if (Atexo_Module::isEnabled('ConsultationVisiteDesLieux') && $this->getAfficher()) {
            $this->afficherComposant();
        } else {
            $this->masquerComposant();
        }

        if (!$this->Page->isPostBack) {
            if (Atexo_Module::isEnabled('ConsultationVisiteDesLieux') && ($this->_consultation instanceof CommonConsultation)
            && $this->getDonneesComplementaires() instanceof CommonTDonneeComplementaire && $this->getDonneesComplementaires()->getVisitesLieux()) {
                $visitesLieux = [];
                if ($this->getLot() || '0' === $this->getLot()) {
                    $visitesLieux = (new Atexo_Consultation())->retreiveVisitesLieuxByRef($this->_consultation->getId(), $this->getLot(), Atexo_CurrentUser::getCurrentOrganism());
                }
                $this->visiteLotOui->Checked = true;
                $this->addVisite->Text = '1';
                $this->visiteLayerLot->setDisplay('Dynamic');
                $dataSource = [];
                $i = 0;
                foreach ($visitesLieux as $oneVisite) {
                    $dataSource[$i]['adresse'] = (new Atexo_Util())->toHttpEncoding($oneVisite->getAdresse());
                    $dataSource[$i]['date'] = $oneVisite->getDate();
                    $dataSource[$i]['objetV'] = $oneVisite;
                    $dataSource[$i]['id'] = $i;
                    ++$i;
                }
                $this->setViewState('dataSourceVisite_'.$this->getLot(), $dataSource);
                $this->setViewState('numLot', $this->getLot());
                $this->visiteRepeater->DataSource = $dataSource;
                $this->visiteRepeater->DataBind();
            }
        }
    }

    /**
     * Permet de gerer la visibilité.
     */
    public function customize()
    {
        if (Atexo_Module::isEnabled('ConsultationVisiteDesLieux')) {
            $this->panelVisiteLieux->visible = true;
            $this->visiteLayerLot->setDisplay('None');
            $this->validatorVisiteDesLieux->setEnabled(true);
            $this->validatorVisiteDesLieuxDateCompare->setEnabled(true);
        } else {
            $this->panelVisiteLieux->visible = false;
            $this->visiteLayerLot->setDisplay('None');
            $this->validatorVisiteDesLieux->setEnabled(false);
            $this->validatorVisiteDesLieuxDateCompare->setEnabled(false);
        }
    }

    /**
     * Permet d'afficher les échantillons demandés.
     */
    public function afficherComposant()
    {
        $this->panelVisiteLieux->setDisplay('Dynamic');
    }

    /**
     * Permet de masquer les échantillons demandés.
     */
    public function masquerComposant()
    {
        $this->panelVisiteLieux->setDisplay('None');
    }

    /**
     * Permet de sauvegarder les visites des lieux.
     *
     * @param CommonConsultation $consultation: objet consultation
     */
    public function save(&$consultation, $lot = '0', &$donneeComplementaire = 'null', $connexion = null, $modification = true)
    {
        // Suppréssion des visites d'une consultation s'elles existent avant de sauvegarder les nouvelles
        if (Atexo_Module::isEnabled('ConsultationVisiteDesLieux')) {
            if (!$connexion) {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            }
            $visitesLieux = (new Atexo_Consultation())->retreiveVisitesLieuxByRef($consultation->getId(), $lot, Atexo_CurrentUser::getCurrentOrganism());
            if ((is_countable($visitesLieux) ? count($visitesLieux) : 0) > 0) {
                foreach ($visitesLieux as $oneVisite) {
                    (new Atexo_Consultation())->deleteVisiteLieuById($oneVisite->getId(), Atexo_CurrentUser::getCurrentOrganism());
                }
            }
            $visiteLieuxExisteOld = $donneeComplementaire->getVisitesLieux();
            // Ajout des visites des lieux
            $visite = '0';
            $visitesNew = [];
            $i = 0;
            $detail1Visite = '';
            if ($this->visiteLotOui->Checked) {
                $visite = '1';
                foreach ($this->visiteRepeater->Items as $oneItem) {
                    $visiteLieu = new CommonVisiteLieux();
                    $visiteLieu->setAdresse($oneItem->adresseValue->Value);
                    $visiteLieu->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
                    $visiteLieu->setAdresseTraduit($this->getLangue(), $oneItem->adresseValue->Value);
                    $visiteLieu->setDate($oneItem->dateValue->Value);
                    $visiteLieu->setLot($lot);
                    $visitesNew[$i++] = $visiteLieu;
                    if ('0' != $lot) {
                        $visiteLieu->setConsultationId($consultation->getId());
                        $visiteLieu->save(Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE')));
                    } else {
                        $consultation->addCommonVisiteLieux($visiteLieu);
                    }
                    $detail1Visite .= $oneItem->adresseValue->Value.Atexo_Config::getParameter('SEPARATEUR_INTRA_VISTES_LIEUX')
                                    .$oneItem->dateValue->Value.Atexo_Config::getParameter('SEPARATEUR_EXTRA_VISTES_LIEUX');
                }
            }

            $donneeComplementaire->setVisitesLieux($visite);
            $this->saveHistoriqueVisitesLieux($detail1Visite, $visiteLieuxExisteOld, $consultation, $donneeComplementaire, Atexo_CurrentUser::getCurrentOrganism(), $lot, $connexion, $modification);
        }
    }

    /*
     * Action callBack
     */
    public function onCallBackLieuxVisiteCons($sender, $param)
    {
        $this->displayLieuxVisiteCons();
        $this->visiteLayerLot->render($param->NewWriter);
    }

    /**
     * affiche la liste des lieux.
     */
    public function displayLieuxVisiteCons()
    {
        $addVisite = $this->addVisite->Text;
        $infoVisite = $this->infoVisite->Text;
        $dataSource = $this->getDataVisite($infoVisite, $addVisite, $this->getLot());
        if ((is_countable($dataSource) ? count($dataSource) : 0) > 0) {
            $this->visiteLotOui->Checked = true;
            $this->visiteLayerLot->setDisplay('Dynamic');
            $this->visiteRepeater->DataSource = $dataSource;
            $this->visiteRepeater->DataBind();
            $this->infoVisite->Text = '';
            $j = 0;
            foreach ($this->visiteRepeater->Items as $item) {
                $tableau = $dataSource;
                if (is_array($tableau) && $tableau[$j]['objetV']) {
                    $item->visiteLieuxLang->setChamps('adresse');
                    $item->visiteLieuxLang->setObject($tableau[$j]['objetV']);
                    $item->visiteLieuxLang->setIsObject(true);
                    $item->visiteLieuxLang->remplirRepeaterLang();
                }
                ++$j;
            }
        } else {
            $this->visiteRepeater->DataSource = [];
            $this->visiteRepeater->DataBind();
            $this->visiteLotOui->Checked = false;
            $this->visiteLotNon->Checked = true;
            $this->visiteLayerLot->setDisplay('None');
        }
    }

    public function onCallBackDeleteVisiteCons($sender, $param)
    {
        $this->displayLieuxVisiteCons();
        $this->visiteLayerLot->render($param->NewWriter);
    }

    public function deleteVisiteLieuxCons($sender, $param)
    {
        $idElement = $param->CommandName;
        $data = $this->getViewState('dataSourceVisite_'.$this->getLot());
        unset($data[$idElement]);
        $this->setViewState('dataSourceVisite_'.$this->getLot(), $data);
    }

    public function getDataVisite($infoVisite, $addVisite, $lot)
    {
        $data = [];
        $dataSource = [];
        $data = $this->getViewState('dataSourceVisite_'.$lot);
        $arrayInfo = explode('#_#', base64_decode($infoVisite));
        $count = 0;
        if (count($arrayInfo) > 1) {
            if ('1' == $addVisite) {
                if (is_array($data) && 0 != $data) {
                    $count = count($data);
                }
                if (0 != $lot) {
                    ++$count;
                }

                $dataSource[$count]['id'] = $count;
                $dataSource[$count]['adresse'] = $arrayInfo[0];
                $dataSource[$count]['date'] = $arrayInfo[1];
                $dataSource[$count]['objetV'] = null;
                $dataSource = Atexo_Util::arrayMerge($data, $dataSource);
            } else {
                $dataSource = $data;
                $ligne = $dataSource[$arrayInfo[2]];
                $dataSource[$ligne['id']]['adresse'] = $arrayInfo[0];
                $dataSource[$ligne['id']]['date'] = $arrayInfo[1];
                $dataSource[$ligne['id']]['objetV'] = $data[$ligne['id']]['objetV'];
            }
            $this->setViewState('dataSourceVisite_'.$lot, $dataSource);
        } else {
            $dataSource = $data;
        }

        return $dataSource;
    }

    /**
     * Permet de sauvegarder l'historique des vistes lieux.
     *
     * @param string $visites visites lieux ,CommonConsultation $consultation la consultation,CommonTDonneComplementaire,
     * @param int    $lot     le numero du lot ,Objet connexion $connexion,String $org l'acronyme de l'organisme, Boolean $modification
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function saveHistoriqueVisitesLieux($visites, $visiteLieuxExisteOld, $consultation, $donneComplementaire, $org, $lot, $connexion, $modification = true)
    {
        $arrayParameters = [];
        if ($visiteLieuxExisteOld != $donneComplementaire->getVisitesLieux() || !$modification) {
            $nomElement = Atexo_Config::getParameter('ID_VISITES_LIEUX');
            $valeur = '0';
            $detail1 = '';
            $detail2 = '';
            if ($donneComplementaire->getVisitesLieux()) {
                $valeur = '1';
                $detail1 = $visites;
            }
            if ($modification) {
                $statut = Atexo_Config::getParameter('MODIFICATION_FILE');
            } else {
                $statut = Atexo_Config::getParameter('CREATION_FILE');
            }
            //Sauvegarde
            $arrayParameters[0] = $consultation->getId();
            $arrayParameters[1] = $nomElement;
            $arrayParameters[2] = $valeur;
            $arrayParameters[3] = $detail1;
            $arrayParameters[4] = $detail2;
            $arrayParameters[5] = $lot;
            $arrayParameters[6] = $org;
            $arrayParameters[7] = $statut;
            (new Atexo_Consultation())->saveHistoriqueConsultation($arrayParameters, $connexion);
        }
    }
}
