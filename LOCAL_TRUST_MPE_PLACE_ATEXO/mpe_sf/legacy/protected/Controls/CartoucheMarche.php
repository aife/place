<?php

namespace Application\Controls;

use Application\Propel\Mpe\CommonTConsLotContrat;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Category;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class CartoucheMarche extends MpeTPage
{
    private $_consLotContrat;
    private $_consultation = null;

    /**
     * recupere la valeur du [consultation].
     *
     * @return int la valeur courante [consultation]
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function getConsultation()
    {
        return $this->_consultation;
    }

    /**
     * modifie la valeur de [$consultation].
     *
     * @param int $consultation la valeur a mettre
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function setConsultation($consultation)
    {
        $this->_consultation = $consultation;
    }

    /**
     * recupere la valeur du [consLotContrat].
     *
     * @return int la valeur courante [consLotContrat]
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function getConsLotContrat()
    {
        return $this->_consLotContrat;
    }

    /**
     * modifie la valeur de [consLotContrat].
     *
     * @param CommonTConsLotContrat $consLotContrat la valeur a mettre
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function setConsLotContrat($consLotContrat)
    {
        $this->_consLotContrat = $consLotContrat;
    }

    /**
     * permet d'afficher les information de la decision.
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function displayDecision()
    {
        if ($this->getConsLotContrat() instanceof CommonTConsLotContrat) {
            $contratTitulaire = $this->getConsLotContrat()->getCommonTContratTitulaire();
            $this->titulaireMarche->Text = $contratTitulaire->getNomTitulaireContrat();
            $this->numeroMarche->Text = $contratTitulaire->getNumeroContrat();
            $this->dateNotification->Text = Atexo_Util::iso2frnDate($contratTitulaire->getDateNotification());
            $this->montantInitialMarche->Text = $contratTitulaire->getMontantContrat();
            $this->categorie->Text = Atexo_Consultation_Category::retrieveLibelleCategorie($contratTitulaire->getCategorie(), false, Atexo_CurrentUser::getOrganismAcronym());
            $this->objetMarche->Text = $contratTitulaire->getObjetContrat();
            $this->referenceUtilisateur->Text = $this->getConsultation()->getReferenceUtilisateur();
        }
    }
}
