<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_Languages;

/**
 * commentaires.
 *
 * @author amal EL BEKKAOUI <amal.elbekkaoui@atexo.com>
 * @copyright Atexo 2011
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class AtexoHiddenLangue extends MpeTTemplateControl
{
    private $_object;
    private $_champs;
    private $_isObject;

    public function getObject()
    {
        return $this->_object;
    }

    public function setObject($value)
    {
        $this->_object = $value;
    }

    public function getIsObject()
    {
        return $this->_isObject;
    }

    public function setIsObject($value)
    {
        $this->_isObject = $value;
    }

    public function getChamps()
    {
        return $this->_champs;
    }

    public function setChamps($value)
    {
        $this->_champs = $value;
    }

    public function remplirRepeaterLang()
    {
        $resLangues = (new Atexo_Languages())->getAllActiveLanguagesButDefaultOne();
        if ($this->getIsObject()) {
            if ($this->getObject()) {
                $this->remplirByObject($resLangues);
            }
        } else {
            $this->remplirByArray($resLangues);
        }
    }

    public function setLanguesTraduction($consultationO)
    {
        foreach ($this->repeaterLangue->Items as $item) {
            $adresse = 'set'.ucfirst(strtolower(self::getChamps())).ucfirst(strtolower($item->langue->value));
            $consultationO->$adresse($item->libelle->value);
        }
    }

    public function setLanguesInArray(&$tab)
    {
        foreach ($this->repeaterLangue->Items as $item) {
            $adresse = $this->getChamps().'_'.strtolower($item->langue->value);
            $tab[$adresse] = $item->libelle->value;
        }
    }

    public function remplirByObject($resLangues)
    {
        $dataSource = [];
        if (is_array($resLangues) && count($resLangues)) {
            for ($i = 0; $i < count($resLangues); ++$i) {
                $lang = strtoupper($resLangues[$i]->getLangue());
                $adresse = 'get'.ucfirst($this->getChamps()).ucfirst(strtolower($lang));
                $dataSource[$i]['libelle'] = $this->getObject()->$adresse();
                $dataSource[$i]['langue'] = $lang;
            }
        }

        $this->repeaterLangue->DataSource = $dataSource;
        $this->repeaterLangue->DataBind();
    }

    public function remplirByArray($resLangues)
    {
        $dataSource = [];
        $objet = self::getObject();
        if (is_array($resLangues) && count($resLangues)) {
            for ($i = 0; $i < count($resLangues); ++$i) {
                $lang = strtoupper($resLangues[$i]->getLangue());
                $adresse = $this->getChamps().'_'.strtolower($lang);
                $dataSource[$i]['libelle'] = $objet[$adresse];
                $dataSource[$i]['langue'] = $lang;
            }
        }
        $this->repeaterLangue->DataSource = $dataSource;
        $this->repeaterLangue->DataBind();
    }
}
