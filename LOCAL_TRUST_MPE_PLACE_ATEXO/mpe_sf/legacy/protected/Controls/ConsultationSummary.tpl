<!--Debut Bloc Recap Consultation-->
<com:TLabel ID="summaryVisible">
		<div class="form-bloc" id="recap-consultation">
			<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
			<div class="content">
				<a class="title-toggle" onclick="togglePanel(this,'infosPrincipales');" href="javascript:void(0);">&nbsp;</a>
				<div class="recap-bloc">
					<div class="line">
						<div class="intitule-250 pos-rb-4"><strong><com:TTranslate>DEFINE_REFERENCE</com:TTranslate> :</strong></div>
						<div class="content-bloc bloc-400">
							<com:TLabel cssClass="blue bold" ID="reference"/>
							<com:TImage Attributes.alt="<%=Prado::Localize('DEFINE_MARCHE_PUBLIC_SIMPLIFIE')%>" id="pictMpsLogo" CssClass="indent-5 inline-img" Visible="<%=(!Application\Service\Atexo\Atexo_Module::isEnabled('MasquerElementsMps'))? true:false %>" />
							<com:TLabel id="logoDume" />
							<com:TImage Attributes.alt="<%=Prado::Localize('CONSULTATION_ANNULEE')%>" id="pictConsultationAnnulee" CssClass="indent-5 inline-img" />
						</div>
					</div>
					<div class="line">
						<div class="intitule-250"><strong><com:TTranslate>DEFINE_OBJET_CONSULTATION</com:TTranslate> :</strong></div>
						<div class="content-bloc bloc-400"><com:TLabel ID="objet"/></div>
					</div>
					<com:TPanel cssClass="line" Visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('RegleMiseEnLigneParEntiteCoordinatrice'))? true:false %>">
						<div class="intitule-250"><strong><com:TTranslate>DEFINE_MISE_LIGNE_SOUHAITEE</com:TTranslate></strong> : </div>
						<div class="content-bloc bloc-400"><com:TLabel ID="dateMiseEnLigneParEntiteCoordinatrice"/></div>
					</com:TPanel>
                    <com:TPanel id="panelDateLimiteRemisePlis" visible="true">
					<div class="line">
						<div class="intitule-250"><strong><com:TTranslate>DEFINE_DATE_LIMITE_REMISE_PLIS</com:TTranslate></strong> : </div>
						<div class="content-bloc bloc-400"><com:TLabel ID="dateFin"/></div>
					</div>
                    </com:TPanel>
                    <com:TPanel id="panelDateLimiteRemisePlisLocale" visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('FuseauHoraire', Application\Service\Atexo\Atexo_CurrentUser::getCurrentOrganism()) && $this->getActivationFuseauHoraire()) ? true:false%>">
                        <com:TPanel cssClass="line" visible="<%=($this->getConsultation()->getDatefinLocale() && $this->getConsultation()->getDatefinLocale() != '0000-00-00 00:00:00' && ($this->getConsultation() instanceOf Application\Propel\Mpe\CommonConsultation)) ? true:false%>">
                            <div class="intitule-250 bold"><com:TTranslate>TEXT_DATE_HEURE_LIMITE_REMISE_PLIS_LOCALE</com:TTranslate> : </div>
                            <com:TLabel ID="dateFinLocale"/>
                        </com:TPanel>
                        <com:TPanel cssClass="line" visible="<%=($this->getConsultation()->getLieuResidence()) ? true:false%>">
                            <div class="intitule-250 bold"><com:TTranslate>DEFINE_LIEU_RESIDENCE</com:TTranslate> : </div>
                            <com:TLabel ID="lieuResidence"/>
                        </com:TPanel>
                    </com:TPanel>
                    <com:TPanel id="panelDateFinAffichage" visible="false">
                        <div class="line">
                            <div class="intitule-250 bold"><com:TTranslate>DEFINE_TEXT_DATE_FIN_AFFICHAGE</com:TTranslate> : </div>
                            <com:TLabel ID="dateFinAffichage"/>
                        </div>
                    </com:TPanel>
					<com:TPanel cssClass="line" ID="panelDateAnnulation" >
						<div class="intitule-250"><com:TTranslate>DEFINE_DATE_D_ANNULATION</com:TTranslate> : </div>
						<div class="content-bloc bloc-400"><com:TLabel id="dateAnnulation" /></div>
					</com:TPanel>
					<div id="infosPrincipales" style="display:none;" class="panel-toggle">
						<div class="line">
							<div class="intitule-250 bold"><com:TTranslate>DEFINE_ENTITE_PUBLIQUE</com:TTranslate> </div>
							<com:TLabel ID="organisme"/>
						</div>
						<div class="line">
							<div class="intitule-250 bold"><com:TTranslate>DEFINE_ENTITE_ACHAT</com:TTranslate>  :</div>
							<com:TLabel ID="service"/>
						</div>
						<com:TPanel cssClass="line" Visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('NumeroProjetAchat')) %>">
							<div class="intitule-250"><strong><com:TTranslate>DEFINE_FULL_NUMERO_PROJET_ACHAT</com:TTranslate></strong> : </div>
							<div class="content-bloc bloc-400"><com:TLabel ID="numProjetAchat"/></div>
						</com:TPanel>
						<div class="line">
							<div class="intitule-250 bold"><com:TTranslate>DEFINE_TYPE_ANNONCE</com:TTranslate> </div>
							<com:TLabel ID="typeAnnonce"/>
						</div>
						<div class="line">
							<div class="intitule-250 bold"><com:TTranslate>TEXT_TYPE_PROCEDURE</com:TTranslate>  :</div>
							<com:TLabel ID="typeProcedure"/> <com:TLabel ID="modePassation" visible="<%=Application\Service\Atexo\Atexo_Module::isEnabled('ConsultationModePassation')? true:false %>"/>
						</div>

                        <div class="line">
                            <div class="intitule-250 bold"><com:TTranslate>DEFINE_TYPE_CONTRAT</com:TTranslate>  :</div>
                            <com:TLabel ID="typeContrat"/>
                        </div>

						<div class="line">
							<div class="intitule-250"><strong><com:TTranslate>DEFINE_INTITULE_CONSULTATION</com:TTranslate> :</strong></div>
							<div class="content-bloc bloc-500"><com:TLabel ID="titre"/></div>
						</div>
						<div class="line">
							<div class="intitule-250"><strong><com:TTranslate>DEFINE_OBJET_CONSULTATION</com:TTranslate> :</strong></div>
							<div class="content-bloc bloc-500"><com:TLabel ID="objetRecap"/></div>

						</div>
						<com:TPanel id="panelDetailsAnnonce" visible="false">
						<div class="line">
							<div class="intitule-250"><strong><com:TTranslate>DEFINE_TEXT_DETAIL_ANNONCE</com:TTranslate> :</strong></div>
							<div class="content-bloc bloc-500"><com:TLabel ID="detailAnnonce"/></div>
						</div>
						</com:TPanel>
						<div class="line">
							<div class="intitule-250 bold"><com:TTranslate>DEFINE_CATEGORIE_PRINCIPAL</com:TTranslate> : </div>
							<com:TLabel ID="categorie"/>
						</div>
						<com:TPanel cssClass="line" Visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('LieuxExecution'))? true:false %>">
							<div class="intitule-250 bold"><com:TTranslate>DEFINE_LIEUX_EXECUTION</com:TTranslate> : </div>
								<div><com:TLabel id="lieuxExecutions" /><com:TLabel id="spanLieux"  Attributes.onmouseover="afficheBulle('infosLeiExecution', this)" Attributes.onmouseout="cacheBulle('infosLeiExecution')" CssClass="info-suite">...</com:TLabel></div>
						    <div id="infosLeiExecution" class="info-bulle" onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();"><div><com:TLabel id="lieuxExecutionSuite"  ></com:TLabel></div></div>
						    <div class="breaker"></div>
						</com:TPanel>

						<!-- début affichage codes nuts -->
						<com:TPanel visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('CodeNutLtReferentiel'))? true:false %>" CssClass="line">
							<div class="intitule-250 bold"><com:TTranslate>DEFINE_LIEUX_EXECUTION</com:TTranslate> : </div>
								<div><com:AtexoReferentiel ID="idAtexoRefNuts" cas="cas9"/></div>
						    <div class="breaker"></div>
						</com:TPanel>
						<!-- fin affichage codes nuts -->
						<!--
						<com:TPanel CssClass="line" visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('AffichageCodeCpv'))? true:false %>">
							<div class="intitule-250 bold"><com:TTranslate>TEXT_CODE_CPV</com:TTranslate> : </div>
							<com:AtexoCPV id="idAtexoCPV" cas="cas4"/>
						</com:TPanel>
						-->
						<!-- Module referentiel CPV -->
						<com:TPanel CssClass="line" visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('AffichageCodeCpv'))? true:false %>">
							<div class="intitule-250 bold"><com:TTranslate>TEXT_CODE_CPV</com:TTranslate> : </div>
							<com:AtexoReferentiel id="idAtexoRef" cas="cas4"/>
							<div class="breaker"></div>
						</com:TPanel>
						 <com:AtexoLtReferentiel id="ltrefCons" consultation="1"  cssClass="intitule-250 bold" idPage="ConsultationSummary" mode="0" type="0"/>
						 <com:TPanel visible="<%=($this->getConsultation()->getIdTypeAvis() ==  Application\Service\Atexo\Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION'))? true:false %>" >
							 <com:AtexoReferentielZoneText id="idRefZoneText" consultation="1"  cssClass="intitule-250 bold" idPage="ConsultationSummary" mode="0" type="1" label="true"/>
							 <com:AtexoLtRefRadio id="idRefRadio" consultation="1"  cssClass="intitule-250 bold" idPage="ConsultationSummary" mode="0" type="2" label="true"/>
							 <div class="breaker"></div>
						 </com:TPanel>

						<com:TPanel id="panelValeurEstimer">
							<div class="line">
								<div class="intitule-250 bold"><com:TTranslate>DEFINE_MONTANT_ESTIMATIF_CONTRAT</com:TTranslate> : </div>
								<com:TLabel ID="montant"/> <com:TTranslate>DEFINE_UNITE_HT</com:TTranslate>
								<div class="breaker"></div>
							</div>
						</com:TPanel>
						<!--  -->
						<div class="breaker"></div>

					</div>
				</div>
				<div class="spacer-small"></div>
				<div class="breaker"></div>
			</div>
			<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
		</div>
</com:TLabel>
<!--Fin Bloc Recap Consultation-->
