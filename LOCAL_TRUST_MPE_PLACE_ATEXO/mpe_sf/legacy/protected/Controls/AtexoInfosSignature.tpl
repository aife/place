<!-- Debut Modal Detail Signature -->
<div class="modal fade popinSignature" id="modalSignature" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <com:TTranslate>DEFINE_DETAIL_SIGNATURE</com:TTranslate>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <!--Debut Formulaire-->
                <div class="tableau-detail-pli">
                    <!--Debut bloc detail-->
                    <div class="bloc-detail">
                        <!--BEGIN ALERT SUCCESS-->
                        <div id="divMsgPopin">
                            <div class="content alert alert-success">
                                <div class="message bloc-700">
                                    <!--Debut Message-->
                                    <span id="messagePopin"></span>
                                    <!--Fin Message-->
                                </div>
                            </div>
                        </div>
                        <!--END ALERT SUCCESS-->

                        <!--BEGIN JETON DE SIGNATURE-->
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <strong><com:TTranslate>DEFINE_JETON_SIGNATURE</com:TTranslate> :</strong>
                                <a id="jetonSignature"></a>
                            </div>
                        </div>
                        <!--END JETON DE SIGNATURE-->

                        <!--BEGIN CERTIFICAT DE SIGNATAIRE-->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <com:TTranslate>DEFINE_CERTIFICAT_SIGNATURE</com:TTranslate>
                                <i class="fa fa-question-circle text-info m-l-1" data-toggle="tooltip" title="<com:TTranslate>DEFINE_INFO_BULLE_CERTIFICAT_SIGNATURE</com:TTranslate>"></i>
                            </div>
                            <div class="panel-body small">
                                <div class="col-md-5">
                                    <div class="page-header m-t-0">
                                        <strong>
                                            <com:TTranslate>DEFINE_CERTIFICAT_EMIS_A</com:TTranslate> :
                                        </strong>
                                    </div>
                                    <div class="clearfix">
                                        <label class="col-md-3">
                                            <com:TTranslate>DEFINE_E</com:TTranslate> :
                                        </label>
                                        <div class="col-md-9">
                                            <span id="EmailSubject"></span>
                                        </div>
                                    </div>
                                    <div class="clearfix">
                                        <label class="col-md-3">
                                            <com:TTranslate>CN</com:TTranslate> :
                                        </label>
                                        <div class="col-md-9">
                                            <span id="CnSubject"></span>
                                        </div>
                                    </div>
                                    <div class="clearfix">
                                        <label class="col-md-3">
                                            <com:TTranslate>DEFINE_OU</com:TTranslate> :
                                        </label>
                                        <div class="col-md-9">
                                            <span id="OuSubject"></span>
                                        </div>
                                    </div>
                                    <div class="clearfix">
                                        <label class="col-md-3">
                                            <com:TTranslate>DEFINE_O</com:TTranslate> :
                                        </label>
                                        <div class="col-md-9">
                                            <span id="OSubject"></span>
                                        </div>
                                    </div>
                                    <div class="clearfix">
                                        <label class="col-md-3">
                                            <com:TTranslate>DEFINE_C</com:TTranslate> :
                                        </label>
                                        <div class="col-md-9">
                                            <span id="CSubject"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="page-header m-t-0">
                                        <strong>
                                            <com:TTranslate>CERTIFICAT_EMIS_PAR</com:TTranslate> :
                                        </strong>
                                    </div>
                                    <div class="clearfix">
                                        <label class="col-md-3">
                                            <com:TTranslate>CN</com:TTranslate> :
                                        </label>
                                        <div class="col-md-9">
                                            <span id="CnEmmeteur"></span>
                                        </div>
                                    </div>
                                    <div class="clearfix">
                                        <label class="col-md-3">
                                            <com:TTranslate>DEFINE_OU</com:TTranslate> :
                                        </label>
                                        <div class="col-md-9">
                                            <span id="OuEmmeteur"></span>
                                        </div>
                                    </div>
                                    <div class="clearfix">
                                        <label class="col-md-3">
                                            <com:TTranslate>DEFINE_O</com:TTranslate> :
                                        </label>
                                        <div class="col-md-9">
                                            <span id="OEmmeteur"></span>
                                        </div>
                                    </div>
                                    <div class="clearfix">
                                        <label class="col-md-9">
                                            <com:TTranslate>DEFINE_C</com:TTranslate> :
                                        </label>
                                        <div class="col-md-9">
                                            <span id="CEmmeteur"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="page-header m-t-0">
                                        <strong>
                                            <com:TTranslate>TEXT_DATE_DE_VALIDITE</com:TTranslate>
                                        </strong>
                                    </div>
                                    <div class="clearfix">
                                        <label class="col-md-4 m-r-0">
                                            <com:TTranslate>DEFINE_DU</com:TTranslate> :
                                        </label>
                                        <div class="col-md-8">
                                            <span id="dateDu"></span>
                                        </div>
                                    </div>
                                    <div class="clearfix">
                                        <label class="col-md-4 m-r-0">
                                            <com:TTranslate>TEXT_AU</com:TTranslate> :
                                        </label>
                                        <div class="col-md-8">
                                            <span id="dateA"></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--END CERTIFICAT DE SIGNATAIRE-->

                        <!--BEGIN CONTROLES DE VALIDITE -->
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Contr&ocirc;les
                            </div>
                            <div class="panel-body small">
                                <div class="col-md-6">
                                    <div class="page-header m-t-0">
                                        <strong>
                                            <com:TTranslate>DEFINE_CONTROLE_VALIDITATION_CERTIF</com:TTranslate>
                                            <i class="fa fa-question-circle text-info m-l-1" data-toggle="tooltip" title="<com:TTranslate>DEFINE_INFO_BULLE_CONTROLE_VALIDITE</com:TTranslate>"></i>
                                        </strong>
                                    </div>
                                    <div class="clearfix">
                                        <label class="col-md-6">
                                            <com:TTranslate>DEFINE_CONTROLE_REALISES_LE</com:TTranslate> :
                                        </label>
                                        <div class="col-md-6">
                                            <span id="dateDepot"></span>
                                        </div>
                                    </div>
                                    <div class="clearfix">
                                        <label class="col-md-6">
                                            <com:TTranslate>PERIODE_VALIDITE</com:TTranslate> :
                                        </label>
                                        <div class="col-md-6">
                                            <img id="pictoPeriodeValidite" src="source" alt="<%=Prado::localize('PERIODE_VALIDITE')%>" />
                                        </div>
                                    </div>
                                    <div class="clearfix">
                                        <label class="col-md-6">
                                            <com:TTranslate>DEFINE_NON_REVOCATION</com:TTranslate> :
                                        </label>
                                        <div class="col-md-6">
                                            <img id="pictoNonRevocation" src="source" alt="<%=Prado::localize('DEFINE_NON_REVOCATION')%>" />
                                        </div>
                                    </div>
                                    <div class="clearfix">
                                        <label class="col-md-6">
                                            <com:TTranslate>CHAINE_CERTIFICATION</com:TTranslate> :
                                        </label>
                                        <div class="col-md-6">
                                            <img id="pictoChaineCertif" src="source" alt="<%=Prado::localize('CHAINE_CERTIFICATION')%>" />
                                        </div>
                                    </div>
                                    <div id="divReferentielCertificat" class="indent-arrow clearfix">
                                        <label class="col-md-6">
                                            <com:TTranslate>DEFINE_REFERENTIEL_DERTIFICAT</com:TTranslate> :
                                        </label>
                                        <div class="col-md-6">
                                            <span id="RefCertif" ></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="page-header m-t-0">
                                        <strong>
                                            <com:TTranslate>DEFINE_INTERGRITE_FICHIER_SIGNE</com:TTranslate>
                                            <i class="fa fa-question-circle text-info m-l-1" data-toggle="tooltip" title="<com:TTranslate>DEFINE_INFO_BULLE_INTEGRITE_FICHIER</com:TTranslate>"></i>
                                        </strong>
                                    </div>
                                    <div class="clearfix">
                                        <label class="col-md-6">
                                            <com:TTranslate>DEFINE_CONTROLE_REALISES_LE</com:TTranslate> :
                                        </label>
                                        <div class="col-md-6">
                                            <span id="dateControle" ></span>
                                        </div>
                                    </div>
                                    <div class="clearfix">
                                        <label class="col-md-6">
                                            <com:TTranslate>DEFINE_NON_REPUDIATION</com:TTranslate> :
                                        </label>
                                        <div class="col-md-6">
                                            <img id="NonRepudiation" src="source" alt="<%=Prado::localize('DEFINE_NON_REPUDIATION')%>" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--END CONTROLES DE VALIDITE -->
                    </div>
                    <!--Fin bloc detail-->
                </div>
                <!--Fin Formulaire-->
            </div>
            <div class="modal-footer">
                <!--Debut line boutons-->
                <button type="button" class="btn btn-sm btn-danger" data-dismiss="modal"><%=Prado::localize("FERMER")%></button>
                <!--Fin line boutons-->
            </div>
        </div>
    </div>
</div>
<!-- Fin Modal Detail Signature -->