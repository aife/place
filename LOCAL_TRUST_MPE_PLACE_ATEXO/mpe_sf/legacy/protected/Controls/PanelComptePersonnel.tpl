<div class="main-part">
    <div class="form-field form-horizontal">
        <div class="clearfix">
            <div class="pull-right small">
                <com:TTranslate>TEXT_LE_SYMBOLE</com:TTranslate>
                <span class="text-danger">*
			</span>
                <com:TTranslate>TEXT_INDIQUE_CHAMPS_OBLIGATOIRES</com:TTranslate>
            </div>
            <ul class="nav nav-tabs" role="tablist">
                <li class="<%=$this->isCurrentTab(1)%>" role="presentation">
                    <a href="#infoPersonnelles" aria-controls="infoPersonnelles" role="tab" data-toggle="tab" aria-expanded="true">
                        <h2 class="h3 panel-title">
                            <com:TTranslate>TEXT_INFORMATIONS_PERSONNELLES</com:TTranslate>
                        </h2>
                    </a>
                </li>
                <li class="<%=$this->isCurrentTab(2)%>" role="presentation">
                    <a href="#infoidentification" aria-controls="infoidentification" role="tab" data-toggle="tab" aria-expanded="true">
                        <h2 class="h3 panel-title">
                            <com:TTranslate>TEXT_INFORMATIONS_IDENTIFICATION</com:TTranslate>
                        </h2>
                    </a>
                </li>
                <li class="<%=$this->isCurrentTab(3)%>" role="presentation">
                    <a href="#etablissements" aria-controls="etablissements" role="tab" data-toggle="tab" aria-expanded="true">
                        <h2 class="h3 panel-title">
                            Etablissements
                        </h2>
                    </a>
                </li>
            </ul>
            <!--Debut Bloc Mes informations personnelles-->
            <div class="tab-content form-content">

                <!--BEGIN BLOC INFORMATIONS PERSONNELLES-->
                <div class="panel panel-default tab-pane fade active in" id="infoPersonnelles">
                    <div class="panel-body">
                        <!-- BEGIN NOM -->
                        <div class="form-group form-group-sm">
                            <com:TPanel ID="panelNomPerso" cssClass="line">
                                <label for="ctl0_CONTENU_PAGE_PanelInscrit_nomPerso" class="control-label col-md-2">
                                    Nom
                                    <span class="text-danger">*</span> :
                                </label>
                                <div class="col-md-9">
                                    <com:TTextBox id="nomPerso" cssClass="form-control"/>
                                </div>
                                <com:TRequiredFieldValidator
                                        ControlToValidate="nomPerso"
                                        ValidationGroup="validateCreateCompte"
                                        Display="Dynamic"
                                        ErrorMessage="<%=Prado::localize('TEXT_NOM_SANS_POINTS')%>"
                                        EnableClientScript="true"
                                        Text="<span title='Champ obligatoire' class='check-invalide'><i class='fa fa-times-circle-o p-r-1'></i></span>">
                                    <prop:ClientSide.OnValidationError>
                                        document.getElementById('divValidationSummary').style.display='';
                                    </prop:ClientSide.OnValidationError>
                                </com:TRequiredFieldValidator>
                            </com:TPanel>
                        </div>
                        <!-- END NOM -->

                        <!-- BEGIN PRENOM -->
                        <div class="form-group form-group-sm">
                            <com:TPanel ID="panelPrenomPerso" cssClass="line">
                                <label for="ctl0_CONTENU_PAGE_PanelInscrit_prenomPerso" class="control-label col-md-2">
                                    Pr&eacute;nom
                                    <span class="text-danger">*</span> :
                                </label>
                                <div class="col-md-9">
                                    <com:TTextBox id="prenomPerso" cssClass="form-control"/>
                                </div>
                                <com:TRequiredFieldValidator
                                        ControlToValidate="prenomPerso"
                                        ValidationGroup="validateCreateCompte"
                                        Display="Dynamic"
                                        ErrorMessage="<%=Prado::localize('TEXT_PRENOM_SANS_POINTS')%>"
                                        EnableClientScript="true"
                                        Text="<span title='Champ obligatoire' class='check-invalide'><i class='fa fa-times-circle-o p-r-1'></i></span>">
                                    <prop:ClientSide.OnValidationError>
                                        document.getElementById('divValidationSummary').style.display='';
                                    </prop:ClientSide.OnValidationError>
                                </com:TRequiredFieldValidator>
                            </com:TPanel>
                        </div>
                        <!-- END PRENOM -->

                        <!-- BEGIN ADRESSE ELECTRONIQUE -->
                        <div class="form-group form-group-sm">
                            <label for="ctl0_CONTENU_PAGE_PanelInscrit_emailPersonnel" class="control-label col-md-2">
                                <com:TTranslate>TEXT_ADRESSE_ELECTRONIQUE</com:TTranslate>
                                <span class="text-danger">*</span> :
                            </label>
                            <div class="col-md-9">
                                <com:TTextBox id="emailPersonnel" cssClass="form-control"/>
                            </div>
                            <com:TRequiredFieldValidator
                                    ControlToValidate="emailPersonnel"
                                    ID="mailValidator"
                                    ValidationGroup="validateCreateCompte"
                                    Display="Dynamic"
                                    ErrorMessage="<%=Prado::localize('TEXT_ADRESSE_ELECTRONIQUE')%>"
                                    EnableClientScript="true"
                                    Text="<span title='Champ obligatoire' class='check-invalide'><i class='fa fa-times-circle-o p-r-1'></i></span>">
                                <prop:ClientSide.OnValidationError>
                                    document.getElementById('divValidationSummary').style.display='';
                                </prop:ClientSide.OnValidationError>
                            </com:TRequiredFieldValidator>
                            <com:TCustomValidator
                                    ControlToValidate="emailPersonnel"
                                    ValidationGroup="validateCreateCompte"
                                    Display="Dynamic"
                                    EnableClientScript="true"
                                    ID="emailValidator"
                                    ErrorMessage="<%=Prado::localize('TEXT_ADRESSE_ELECTRONIQUE')%>"
                                    ClientValidationFunction="validatorEmailNonObligatoire"
                                    Text="<span title='Champ obligatoire' class='check-invalide'><i class='fa fa-times-circle-o p-r-1'></i></span> ">
                                <prop:ClientSide.OnValidationError>
                                    document.getElementById('divValidationSummary').style.display='';
                                </prop:ClientSide.OnValidationError>
                            </com:TCustomValidator>
                            <com:TCustomValidator
                                    ControlToValidate="emailPersonnel"
                                    ValidationGroup="validateCreateCompte"
                                    Display="Dynamic"
                                    EnableClientScript="true"
                                    ID="emailCreationValidator"
                                    onServerValidate="Page.PanelInscrit.verifyMailCreation"
                                    Text="<span title='Champ obligatoire' class='check-invalide'><i class='fa fa-times-circle-o p-r-1'></i></span>">
                            </com:TCustomValidator>
                            <com:TCustomValidator
                                    ControlToValidate="emailPersonnel"
                                    ValidationGroup="validateCreateCompte"
                                    ID="emailModificationValidator"
                                    EnableClientScript="true"
                                    onServerValidate="Page.PanelInscrit.verifyMailModification"
                                    Text="<span title='Champ obligatoire' class='check-invalide'><i class='fa fa-times-circle-o p-r-1'></i></span>">
                            </com:TCustomValidator>
                        </div>
                        <!-- BEGIN ADRESSE ELECTRONIQUE -->

                        <!-- BEGIN TELEPHONE -->
                        <div class="form-group form-group-sm">
                            <label for="ctl0_CONTENU_PAGE_PanelInscrit_telPersonnel" class="control-label col-md-2">
                                T&eacute;l&eacute;phone
                                <span class="text-danger">* </span> :
                            </label>
                            <div class="col-md-9">
                                <com:TTextBox id="telPersonnel" cssClass="form-control"/>
                            </div>
                            <com:TRequiredFieldValidator
                                    ControlToValidate="telPersonnel"
                                    ValidationGroup="validateCreateCompte"
                                    Display="Dynamic"
                                    ErrorMessage="<%=Prado::localize('TEXT_TEL')%>"
                                    EnableClientScript="true"
                                    Text="<span title='Champ obligatoire' class='check-invalide'><i class='fa fa-times-circle-o p-r-1'></i></span>">
                                <prop:ClientSide.OnValidationError>
                                    document.getElementById('divValidationSummary').style.display='';
                                </prop:ClientSide.OnValidationError>
                            </com:TRequiredFieldValidator>
                            <com:TRegularExpressionValidator
                                    ID="formattelPersonnel"
                                    Display="Dynamic"
                                    ValidationGroup="validateCreateCompte"
                                    ControlToValidate="telPersonnel"
                                    Enabled="<%=Application\Service\Atexo\Atexo_Module::isEnabled('ValidationFormatChampsStricte')%>"
                                    RegularExpression="<%= Application\Service\Atexo\Atexo_Config::getParameter('EXPRESSION_REGULIERE_VALIDATION_TELEPHONE_FAX')%>"
                                    ErrorMessage="<%=Prado::localize('FORMAT_TEL')%>"
                                    Text="<span title='Format incorrecte' class='check-invalide'><i class='fa fa-times-circle-o p-r-1'></i></span>">
                                <prop:ClientSide.OnValidationError>
                                    document.getElementById('divValidationSummary').style.display='';
                                </prop:ClientSide.OnValidationError>
                            </com:TRegularExpressionValidator>
                            <com:TCustomValidator
                                    ID="nombreCaracteresNumTel"
                                    ValidationGroup="validateCreateCompte"
                                    ControlToValidate="telPersonnel"
                                    ClientValidationFunction="validerNombreCaracteresNumTelFax"
                                    Display="Dynamic"
                                    ErrorMessage="<%=str_replace('XXX',  Application\Service\Atexo\Atexo_Config::getParameter('NOMBRE_CARACTERES_MAX_AUTORISES_SAISIE_TELEPHONE_FAX'), Prado::localize('MESSAGE_NOMBRE_CARACTERES_AUTORISES_NUMERO_TEL'))%>"
                                    Text="<span title='Champ obligatoire' class='check-invalide'><i class='fa fa-times-circle-o p-r-1'></i></span>"
                                    EnableClientScript="true"
                            >
                                <prop:ClientSide.OnValidationError>
                                    document.getElementById('divValidationSummary').style.display='';
                                </prop:ClientSide.OnValidationError>
                            </com:TCustomValidator>
                        </div>
                        <!-- END TELEPHONE -->

                        <!-- BEGIN FAX -->
                        <div class="form-group form-group-sm">
                            <label for="ctl0_CONTENU_PAGE_PanelInscrit_faxPersonnel" class="control-label col-md-2">
                                Fax :
                            </label>
                            <div class="col-md-9">
                                <com:TTextBox id="faxPersonnel" cssClass="form-control"/>
                            </div>
                            <com:TRegularExpressionValidator
                                    ID="formatfaxPersonnel"
                                    ValidationGroup="validateCreateCompte"
                                    ControlToValidate="faxPersonnel"
                                    Display="Dynamic"
                                    Enabled="<%=Application\Service\Atexo\Atexo_Module::isEnabled('ValidationFormatChampsStricte')%>"
                                    RegularExpression="<%= Application\Service\Atexo\Atexo_Config::getParameter('EXPRESSION_REGULIERE_VALIDATION_TELEPHONE_FAX')%>"
                                    ErrorMessage="<%=Prado::localize('FORMAT_FAX')%>"
                                    Text="<span title='Format incorrecte' class='check-invalide'><i class='fa fa-times-circle-o p-r-1'></i></span>">
                                <prop:ClientSide.OnValidationError>
                                    document.getElementById('divValidationSummary').style.display='';
                                </prop:ClientSide.OnValidationError>
                            </com:TRegularExpressionValidator>
                            <com:TCustomValidator
                                    ID="nombreCaracteresNumFax"
                                    ValidationGroup="validateCreateCompte"
                                    ControlToValidate="faxPersonnel"
                                    ClientValidationFunction="validerNombreCaracteresNumTelFax"
                                    Display="Dynamic"
                                    ErrorMessage="<%=str_replace('XXX',  Application\Service\Atexo\Atexo_Config::getParameter('NOMBRE_CARACTERES_MAX_AUTORISES_SAISIE_TELEPHONE_FAX'), Prado::localize('MESSAGE_NOMBRE_CARACTERES_AUTORISES_NUMERO_FAX'))%>"
                                    Text="<span title='Champ obligatoire' class='check-invalide'><i class='fa fa-times-circle-o p-r-1'></i></span>"
                                    EnableClientScript="true"
                            >
                                <prop:ClientSide.OnValidationError>
                                    document.getElementById('divValidationSummary').style.display='';
                                </prop:ClientSide.OnValidationError>
                            </com:TCustomValidator>
                        </div>
                        <!-- END FAX -->
                    </div>
                </div>
                <!--END BLOC INFORMATIONS PERSONNELLES-->

                <!--BEGIN BLOC INFORMATIONS D'IDENTIFICATION-->
                <div class="panel panel-default tab-pane fade" id="infoidentification">
                    <div class="panel-body">
                        <div class="section">
                            <div class="section-body">
                                <com:TPanel id="panelAuthLoginPass" cssclass="content">
                                    <div class="form-group form-group-sm">
                                        <label for="ctl0_CONTENU_PAGE_PanelInscrit_identifiant" class="control-label col-md-2">
                                            <com:TTranslate>TEXT_IDENTIFIANT_SANS_POINTS</com:TTranslate>
                                            <span class="text-danger">* </span> :
                                        </label>
                                        <div class="col-md-9">
                                            <com:TTextBox id="identifiant" cssClass="form-control"/>
                                        </div>
                                        <com:TCustomValidator
                                                ControlToValidate="identifiant"
                                                ClientValidationFunction="validateLogin"
                                                ID="loginValidator"
                                                ValidationGroup="validateCreateCompte"
                                                Display="Dynamic"
                                                ErrorMessage="<%=Prado::localize('TEXT_IDENTIFIANT_SANS_POINTS')%>"
                                                EnableClientScript="true"
                                                Text="<span title='Champ obligatoire' class='check-invalide'><i class='fa fa-times-circle-o p-r-1'></i></span>">
                                            <prop:ClientSide.OnValidationError>
                                                document.getElementById('divValidationSummary').style.display='';
                                            </prop:ClientSide.OnValidationError>
                                        </com:TCustomValidator>
                                        <com:TCustomValidator
                                                ControlToValidate="identifiant"
                                                ValidationGroup="validateCreateCompte"
                                                Display="Dynamic"
                                                EnableClientScript="true"
                                                ID="loginCreationValidator"
                                                onServerValidate="Page.PanelInscrit.verifyLoginCreation"
                                                Text="<span title='Champ obligatoire' class='check-invalide'><i class='fa fa-times-circle-o p-r-1'></i></span>">
                                        </com:TCustomValidator>
                                        <com:TCustomValidator
                                                ControlToValidate="identifiant"
                                                ValidationGroup="validateCreateCompte"
                                                ID="loginModificationValidator"
                                                EnableClientScript="true"
                                                onServerValidate="Page.PanelInscrit.verifyLoginModification"
                                                Text="<span title='Champ obligatoire' class='check-invalide'><i class='fa fa-times-circle-o p-r-1'></i></span>">
                                        </com:TCustomValidator>
                                    </div>
                                    <div class="form-group form-group-sm">
                                        <label for="ctl0_CONTENU_PAGE_PanelInscrit_password" class="control-label col-md-2">
                                            <com:TTranslate>TEXT_MOT_PASSE_SANS_POINT</com:TTranslate>
                                            <span class="text-danger">*</span> :
                                            <i class="fa fa-question-circle text-info" data-html="true" data-toggle="tooltip" data-placement="right"
                                               title="<%=Prado::localize('REGLE_VALIDATION_MDP')%>"></i>
                                        </label>
                                        <div class="col-md-9">
                                            <com:TTextBox
                                                    id="password"
                                                    PersistPassword="true"
                                                    TextMode="Password"
                                                    Attributes.title="<%=Prado::localize('TEXT_MOT_PASSE_SANS_POINT')%>"
                                                    cssClass="form-control"
                                            />
                                        </div>
                                        <com:TCustomValidator
                                                ControlToValidate="password"
                                                ID="passwordValidator"
                                                ClientValidationFunction="validatePassword"
                                                ValidationGroup="validateCreateCompte"
                                                Display="Dynamic"
                                                ErrorMessage="<%=Prado::localize('TEXT_MOT_PASSE_SANS_POINT')%>"
                                                EnableClientScript="true"
                                                Text="<span title='Champ obligatoire' class='check-invalide'><i class='fa fa-times-circle-o p-r-1'></i></span>">
                                            <prop:ClientSide.OnValidationError>
                                                document.getElementById('divValidationSummary').style.display='';
                                            </prop:ClientSide.OnValidationError>
                                        </com:TCustomValidator>
                                        <com:TCustomValidator
                                                ValidationGroup="validateCreateCompte"
                                                ControlToValidate="password"
                                                ClientValidationFunction="verifierMdp"
                                                ErrorMessage="<%=Prado::localize('REGLE_VALIDATION_MDP')%>"
                                                visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('EntrepriseControleFormatMotDePasse'))? true:false%>"
                                                Text="<span title='Champ obligatoire' class='check-invalide'><i class='fa fa-times-circle-o p-r-1'></i></span>">
                                            <prop:ClientSide.OnValidationError>
                                                document.getElementById('divValidationSummary').style.display='';
                                            </prop:ClientSide.OnValidationError>
                                        </com:TCustomValidator>
                                    </div>
                                    <div class="form-group form-group-sm">
                                        <label for="ctl0_CONTENU_PAGE_PanelInscrit_confPassword" class="control-label col-md-2">
                                            <com:TTranslate>TEXT_CONFIRMATION_MOT_PASSE</com:TTranslate>
                                            <span class="text-danger">*</span> :
                                        </label>
                                        <div class="col-md-9">
                                            <com:TTextBox
                                                    id="confPassword"
                                                    TextMode="Password"
                                                    PersistPassword="true"
                                                    Attributes.title="<%=Prado::localize('TEXT_CONFIRMATION_MOT_PASSE')%>"
                                                    cssClass="form-control"
                                            />
                                        </div>
                                        <com:TCustomValidator
                                                ControlToValidate="confPassword"
                                                ID="confPasswordValidator"
                                                ValidationGroup="validateCreateCompte"
                                                ClientValidationFunction="validateConfPassword"
                                                Display="Dynamic"
                                                ErrorMessage="<%=Prado::localize('TEXT_CONFIRMATION_MOT_PASSE')%>"
                                                EnableClientScript="true"
                                                Text="<span title='Champ obligatoire' class='check-invalide'><i class='fa fa-times-circle-o p-r-1'></i></span>">
                                            <prop:ClientSide.OnValidationError>
                                                document.getElementById('divValidationSummary').style.display='';
                                            </prop:ClientSide.OnValidationError>
                                        </com:TCustomValidator>
                                        <com:TCompareValidator
                                                ValidationGroup="validateCreateCompte"
                                                ID="confPasswordCompareValidator"
                                                ControlToValidate="password"
                                                ControlToCompare="confPassword"
                                                DataType="String"
                                                Display="Dynamic"
                                                Operator="Equal"
                                                ErrorMessage="<%=Prado::localize('TEXT_DEUX_PASSWORD_DIFF')%>"
                                                Text="<span title='Champ obligatoire' class='check-invalide'><i class='fa fa-times-circle-o p-r-1'></i></span>">
                                            <prop:ClientSide.OnValidationError>
                                                document.getElementById('divValidationSummary').style.display='';
                                            </prop:ClientSide.OnValidationError>
                                        </com:TCompareValidator>
                                    </div>
                                </com:TPanel>
                            </div>
                        </div>
                        <div class="section">
                            <com:TPanel cssClass="form-group form-group-sm" ID="InfoProfil"
                                        Display="<%=(Application\Service\Atexo\Atexo_CurrentUser::isATES() && Application\Service\Atexo\Atexo_Module::isEnabled('AutoriserModifProfilInscritAtes')) ? 'Dynamic':'None'%>">
                                <label class="col-md-2 control-label">
                                    <com:TTranslate>DEFINE_PROFIL_USER_RNTREPRISE</com:TTranslate>
                                    :
                                </label>
                                <div class="col-md-9">
                                    <label for="ctl0_CONTENU_PAGE_PanelInscrit_inscritSimple" class="radio-inline">
                                        <com:TRadioButton enabled="<%=($this->isProfilActif())? true:false%>"
                                                          id="inscritSimple"
                                                          GroupName="RadioGroup"/>
                                        <com:TTranslate>DEFINE_UES_SIMPLE</com:TTranslate>
                                    </label>
                                    <label for="ctl0_CONTENU_PAGE_PanelInscrit_adminEntreprise" class="radio-inline">
                                        <com:TRadioButton enabled="<%=($this->isProfilActif())? true:false%>"
                                                          id="adminEntreprise"
                                                          GroupName="RadioGroup"/>
                                        <com:TTranslate>DEFINE_ATES_ENTREPRISE</com:TTranslate>
                                    </label>
                                </div>
                            </com:TPanel>
                        </div>
                    </div>
                </div>
                <!--BEGIN BLOC INFORMATIONS D'IDENTIFICATION-->

                <!--BEGIN BLOC MON ETABLISSEMENT-->
                <div class="panel panel-default tab-pane fade" id="etablissements">
                    <div class="panel-body">
                        <div class="form-field">
                            <com:AtexoEtablissements id="etablissements" mode="3"/>
                        </div>
                    </div>
                </div>
                <!--END BLOC MON ETABLISSEMENT-->

            </div>
            <!--Fin Bloc Mes informations personnelles-->

            <div class="form-field">


                <!--Debut Bloc Mes informations Identification-->
                <div class="picto-link inline">
                    <!-- Bloc Identification par certificat -->
                    <div class="content">
                        <div class="line">
								<span style="display:none">
									<com:TRadioButton ID="CaseLogin" Checked="true"/>
								</span>
                            <div id="div_cn_certif" style="display:none">
                                <div class="intitule-150"><label>
                                    <com:TTranslate>TEXT_PROPRIETAIRE_CERTIF</com:TTranslate>
                                </label></div>
                                <com:TLabel ID="cnCertif"/>
                                <com:TCustomValidator
                                        ControlToValidate="CaseLogin"
                                        ValidationGroup="validateCreateCompte"
                                        Display="Dynamic"
                                        ID="certifCreationValidator"
                                        EnableClientScript="true"
                                        Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
                                        onServerValidate="Page.PanelInscrit.verifyCertifCreation"/>
                                <com:TCustomValidator
                                        ControlToValidate="CaseLogin"
                                        ValidationGroup="validateCreateCompte"
                                        Display="Dynamic"
                                        ID="certifModificationValidator"
                                        EnableClientScript="true"
                                        Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
                                        onServerValidate="Page.PanelInscrit.verifyCertifModification"/>
                            </div>
                        </div>
                        <div class="line">
                            <div id="div_ac_certif" style="display:none">
                                <div class="intitule-150"><label>
                                    <com:TTranslate>TEXT_AC_CERTIF</com:TTranslate>
                                </label></div>
                                <com:TLabel ID="acCertif"/>
                            </div>
                            <div class="spacer-mini"></div>
                            <com:TPanel id="panelChoixCert">
                                <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-acces-arrow-blue.gif" alt=""/>
                                <com:THyperLink ID="linkChoixCert" Text="<%=Prado::localize('TEXT_PASSER_CERTIF_NUMERIQUE')%>"
                                                NavigateUrl="javascript:popUpSetSize('secure/getCert.php?<%=session_name()%>=<%=session_id()%>','10px','20px','yes');"/>
                            </com:TPanel>
                            <com:TPanel id="panelChoixLoginMdp">
                                <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-acces-arrow-blue.gif" alt=""/>
                                <com:TLinkButton ID="linkChoixLoginMdp" Text="<%=Prado::localize('TEXT_PASSER_MODE_LOGIN_PASS')%>" onclick="goToModeLoginPassword"/>
                            </com:TPanel>
                        </div>
                    </div>
                    <!-- Fin block identification par certificat -->
                </div>
            </div>
        </div>
        <com:TLabel ID="JSCert"/>
        <div style="display: none;" class="info-bulle" onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();">
            <div>
                <com:TTranslate>MSG_CINQ_DERNIER_CHIFFRE_SIRET</com:TTranslate>
            </div>
        </div>
        <div style="display: none;" id="infosMdp" class="info-bulle" onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();">
            <div>
                <com:TTranslate>REGLE_VALIDATION_MDP</com:TTranslate>
            </div>
        </div>
    </div>
</div>