<?php

namespace Application\Controls;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonReferentielConsultation;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Referentiel\Atexo_Referentiel_Referentiel;
use Application\Service\Atexo\Referentiel\Atexo_Referentiel_ReferentielVo;

/**
 * commentaires.
 *
 * @author Hajar HANNAD <hajar.hannad@atexo.com>
 * @copyright Atexo 2011
 *
 * @version 1.0
 *
 * @since MPE-4.0
 */
class AtexoReferentielCheck extends AtexoReferentielZoneText
{
    public function afficherTextConsultation($consultationId = null, $lot = null, $organisme = null, $defineValue = null, $postback = false)
    {
        if (!$this->Page->isPostBack || $postback) {
            self::remplirRepeaterReferentielZoneText();
            foreach ($this->RepeaterReferentielZoneText->Items as $Item) {
                $Item->idLot->afficherZoneLot($consultationId, null, $organisme);
                $IdRef = $Item->iDLibelle->Text;
                $codeRefPrinc = null;
                if (!$defineValue) {
                    if ($organisme) {
                        $refCons = (new Atexo_Referentiel_Referentiel())->getCommonLtReferentielByRefConsultationAndLot($consultationId, $lot, $IdRef, $organisme);
                        if ($refCons) {
                            $codeRefPrinc = $refCons->getValeurPrincipaleLtReferentiel();
                        }
                    }
                } else {
                    if ($defineValue[$IdRef]) {
                        $codeRefPrinc = $defineValue[$IdRef]->getValuePrinc();
                    }
                }
                if ($this->Page->IsPostBack) {//Page en raffraichissement
                    $codeRefPrincAfterFormatage = (new Atexo_Util())->toHttpEncoding(($codeRefPrinc));
                } else {//La page est chargée pour la prémière fois
                    $codeRefPrincAfterFormatage = Atexo_Util::replaceSpecialCharacters($codeRefPrinc);
                }

                if ('montant' == $Item->typeData->Value && $codeRefPrincAfterFormatage) {
                    $codeRefPrincAfterFormatage = Atexo_Util::getMontantArronditEspace($codeRefPrincAfterFormatage);
                }
                $Item->labelReferentielZoneText->SetText($codeRefPrincAfterFormatage);
                $Item->idReferentielZoneText->SetText($codeRefPrincAfterFormatage);
                $Item->oldValue->SetValue($codeRefPrincAfterFormatage);
            }
        }
    }

    public function saveConsultationAndLot($consultationId, $lot, &$consultation = false)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $organisme = Atexo_CurrentUser::getCurrentOrganism();
        foreach ($this->RepeaterReferentielZoneText->Items as $Item) {
            if ($Item->refTexte->checked) {
                $IdRef = $Item->iDLibelle->Text;
                $valeurPricLtRef = $Item->idReferentielZoneText->Text;
                $CommonRefCons = (new Atexo_Referentiel_Referentiel())->getCommonLtReferentielByRefConsultationAndLot($consultationId, $lot, $IdRef, $organisme);
                if (!$CommonRefCons) {
                    $CommonRefCons = new CommonReferentielConsultation();
                    $CommonRefCons->setConsultationId($consultationId);
                    $CommonRefCons->setLot($lot);
                    $CommonRefCons->setIdLtReferentiel($IdRef);
                    $CommonRefCons->setOrganisme($organisme);
                }
                $CommonRefCons->setValeurPrincipaleLtReferentiel($valeurPricLtRef);
                $CommonRefCons->save($connexionCom);
            }
        }
    }

    public function saveRefLot($consultationId, $lot = null)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $organisme = Atexo_CurrentUser::getCurrentOrganism();
        foreach ($this->RepeaterReferentielZoneText->Items as $Item) {
            if ($Item->refTexte->checked) {
                foreach ($Item->idLot->repeaterLot->Items as $Items) {
                    $IdRef = $Items->iDLibelle->Text;
                    $valeurPricLtRef = $Items->idReferentielZoneText->getText();
                    $CommonRefCons = (new Atexo_Referentiel_Referentiel())->getCommonLtReferentielByRefConsultationAndLot($consultationId, $Items->numLot->getText(), $IdRef, $organisme);
                    if (!$CommonRefCons) {
                        $CommonRefCons = new CommonReferentielConsultation();
                        $CommonRefCons->setConsultationId($consultationId);
                        $CommonRefCons->setIdLtReferentiel($IdRef);
                        $CommonRefCons->setOrganisme($organisme);
                        $CommonRefCons->setLot($Items->numLot->getText());
                    }
                    $CommonRefCons->setValeurPrincipaleLtReferentiel($valeurPricLtRef);
                    $CommonRefCons->save($connexionCom);
                }
            }
        }
    }

    public function getValueReferentielLotTextVo()
    {
        $valueFinal = [];
        foreach ($this->RepeaterReferentielZoneText->Items as $Item) {
            if ($Item->refTexte->checked) {
                foreach ($Item->idLot->repeaterLot->Items as $Items) {
                    $value1 = [];
                    $refVo = new Atexo_Referentiel_ReferentielVo();
                    $refVo->setId($Items->iDLibelle->Text);
                    $refVo->setValuePrinc($Items->idReferentielZoneText->Text);
                    $refVo->setValue($Items->idReferentielZoneText->Text);
                    $refVo->setCodeLibelle($Items->codeLibelle->Text);
                    $refVo->setOldValue($Items->oldValue->Value);
                    $value1[$Items->iDLibelle->Text] = $refVo;
                    $valueFinal[$Items->numLot->Text] = $value1;
                }
            }
        }

        return $valueFinal;
    }

    public function getValueReferentielTextVo()
    {
        $value1 = [];
        foreach ($this->RepeaterReferentielZoneText->Items as $Item) {
            if ($Item->refTexte->checked) {
                $refVo = new Atexo_Referentiel_ReferentielVo();
                $refVo->setId($Item->iDLibelle->Text);
                $refVo->setValuePrinc($Item->idReferentielZoneText->Text);
                $refVo->setValue($Item->idReferentielZoneText->Text);
                $refVo->setCodeLibelle($Item->codeLibelle->Text);
                $refVo->setOldValue($Item->oldValue->Value);
                $value1[$Item->iDLibelle->Text] = $refVo;
            }
        }

        return $value1;
    }
}
