	<!--Debut colonne gauche-->
	<div class="left-part" id="left-part">
		<!--Debut menu -->
		<div id="menu">
			<com:TPanel visible="<%= ! ($this->isConnected())%>">
			<ul id="menuList">
				<li class="menu-open"><span><a href="?page=Agent.AgentHome"><com:TTranslate>DEFINE_INDENTIFIER</com:TTranslate></a></span></li>
			</ul>
			<div class="breaker"></div>
			</com:TPanel>
			<com:TPanel visible="<%=($this->isConnected()) ? true:false%>">
			<ul id="menuList">
				<li class="<%=(true)?'menu-open menu-on':'menu-open'%>">
					<span><a href="javascript:void(0);" onclick="toggleMenu('menuBaseDce');" accesskey="b"><com:TTranslate>TEXT_BASE_DCE</com:TTranslate></a></span>
						<ul class="ss-menu-open" id="menuBaseDce" style="display:block" >
							<li class="off">
								<com:TPanel DefaultButton="imageOkDCE">
									<label for="quickSearchDCE" style="display:none;"><com:TTranslate>TEXT_RECHERCHE_RAPIDE</com:TTranslate></label>
									<com:TTextBox ID="quickSearchDCE"
												CssClass="rechercher"
												Text="<%=Prado::localize('TEXT_RECHERCHE_RAPIDE')%>"
												Attributes.title="<%=Prado::localize('TEXT_RECHERCHE_RAPIDE')%>"
												Attributes.onfocus="clearOnFocus(event,this);" 
												Attributes.onmouseover="clearOnFocus(event,this);" 
												Enabled = "true"
												/>
									<com:TLinkButton ID="imageOkDCE"  OnClick="quickSearchDCE" CssClass="ok" Enabled = "true">
										 <com:TImage ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/bouton-ok.gif"  Attributes.alt="OK" Attributes.title="OK" />
									</com:TLinkButton>
								</com:TPanel>
	                        </li>
	                        <li class="off">
								<a href="index.php?page=Agent.SearchDCE"><com:TTranslate>TEXT_RECHERCHE_AVANCEE</com:TTranslate></a>
							</li>
						</ul>
				</li>
			</ul>
			<div class="breaker"></div>	
			</com:TPanel>
</div>
<div class="menu-bottom"></div>

<!--Fin menu -->
</div>