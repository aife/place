<?php

namespace Application\Controls;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonPlateformeVirtuelle;
use Application\Propel\Mpe\CommonPlateformeVirtuelleQuery;
use Application\Propel\Mpe\CommonTInformationModificationPassword;
use Application\Service\Atexo\Atexo_Admin;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_MultiDomaine;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Inscrit;
use Exception;
use Prado\Prado;

/**
 * Page d'envoi de mot de passe.
 *
 * @author ZAKI Anas <anas.zaki@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class ForgottenPasswordAgent extends MpeTPage
{
    public $_parentPage;
    public $_login;
    public $_email;
    public $_newPassword;

    public function onLoad($param)
    {
        if ('agent' == $this->getParentPage()) {
            $this->lienRetour->NavigateUrl = 'index.php?page=Agent.AgentHome';
        } elseif ('admin' == $this->getParentPage()) {
            $this->lienRetour->NavigateUrl = 'index.php?page=Administration.Home';
        } else {
            $this->lienRetour->NavigateUrl = 'index.php?page=Entreprise.EntrepriseHome';
        }
        if (isset($_GET['updateFailed'])) {
            self::setBlocMessage('msg-avertissement');
        }
    }

    public function getParentPage()
    {
        return $this->_parentPage;
    }

    public function setParentPage($value)
    {
        $this->_parentPage = $value;
    }

    public function getPassword($sender, $param)
    {
        $agent = null;
        $admin = null;
        $inscrit = null;
        $this->_email = $this->emailUser->getText();
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));

        if (!Atexo_Util::checkEmailFormat($this->emailUser->Text)) {
            self::setBlocMessage('msg-email-not-valid');
            $this->emailUser->Text = '';

            return true;
        }
        try {
            $connexionCom->beginTransaction();
            if ('agent' == $this->getParentPage()) {    // pour l'appel cote agent
                if ($this->_email) {
                    $agent = (new Atexo_Agent())->retrieveAgentByMail($this->_email);
                }
                if ($agent) {
                    if ('1' == $agent->getActif()) {//Si le compte est actif.
                        $jeton = Atexo_Util::getJetonModificationPsw($agent->getId(), 'agent');
                        self::saveInformationModification($agent->getId(), 'agent', $this->_email, $jeton, $connexionCom);
                        $mailEnvoye = self::generateMailModificationPassWord($this->_email, 'agent', $jeton);
                        if ($mailEnvoye) {
                            $connexionCom->commit();
                            self::setBlocMessage('msg-unique');
                            $this->panelEnvoiLogin->visible = false;
                        } else {
                            self::setBlocMessage('msg-unique');
                            $this->panelEnvoiLogin->visible = false;
                        }
                    }
                } else {
                    self::setBlocMessage('msg-unique');
                    $this->panelEnvoiLogin->visible = false;
                }
            } elseif ('admin' == $this->getParentPage()) {    // pour l'appel cote admin
                if ($this->_email) {
                    $admin = (new Atexo_Admin())->retrieveAdminByMail($this->_email);
                }
                if ($admin) {
                    $jeton = Atexo_Util::getJetonModificationPsw($admin->getId(), 'admin');
                    self::saveInformationModification($admin->getId(), 'admin', $this->_email, $jeton, $connexionCom);
                    $mailEnvoye = self::generateMailModificationPassWord($this->_email, 'admin', $jeton);
                    if ($mailEnvoye) {
                        $connexionCom->commit();
                        self::setBlocMessage('msg-unique');
                        $this->panelEnvoiLogin->visible = false;
                    } else {
                        self::setBlocMessage('msg-unique');
                        $this->panelEnvoiLogin->visible = false;
                    }
                } else {
                    self::setBlocMessage('msg-unique');
                    $this->panelEnvoiLogin->visible = false;
                }
            } else {    // pour l'appel cote entreprise
                if ($this->_email) {
                    $inscrit = (new Atexo_Entreprise_Inscrit())->retrieveInscritByMail($this->_email);
                }
                if ($inscrit) {
                    if ('0' == $inscrit->getBloque()) {//Si l'inscrit n'est pas bloque .
                        $jeton = Atexo_Util::getJetonModificationPsw($inscrit->getId(), 'entreprise');
                        self::saveInformationModification($inscrit->getId(), 'entreprise', $this->_email, $jeton, $connexionCom);
                        $mailEnvoye = self::generateMailModificationPassWord($this->_email, 'entreprise', $jeton);
                        if ($mailEnvoye) {
                            $connexionCom->commit();
                            self::setBlocMessage('msg-unique');
                            $this->panelEnvoiLogin->visible = false;
                        } else {
                            self::setBlocMessage('msg-unique');
                            $this->panelEnvoiLogin->visible = false;
                        }
                    } else {
                        self::setBlocMessage('msg-erreur-bloquer');
                        $this->panelMotPasseOublie->visible = false;
                        $this->panelEnvoiLogin->visible = false;
                        $this->emailUser->Text = '';
                    }
                } else {
                    self::setBlocMessage('msg-unique');
                    $this->panelEnvoiLogin->visible = false;
                }
            }
        } catch (Exception $e) {
            $connexionCom->rollback();
            throw $e;
        }
    }

    /**
     * Permet de sauvgarder les informations du demande de modification du mot de passe.
     *
     * @param int    $idUser l'id d'utilisateur, String $typeUser le type de l'utilisateur
     * @param string $email  l'email de l'utilisateur,String $jeton le jeton,Objet $connexion l'objet de connexion
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function saveInformationModification($idUser, $typeUser, $email, $jeton, $connexion)
    {
        $informationPsw = new CommonTInformationModificationPassword();
        $informationPsw->setIdUser($idUser);
        $informationPsw->setTypeUser($typeUser);
        $informationPsw->setEmail($email);
        $informationPsw->setJeton($jeton);
        $dateDebut = date('Y-m-d H:i:s');
        $informationPsw->setDateDemandeModification($dateDebut);
        $dateFin = Atexo_Util::dateDansFutur($dateDebut, Atexo_Config::getParameter('DUREE_VALIDITE_JETON_MODIFICATION_PASSWORD'));
        $informationPsw->setDateFinValidite($dateFin);
        $informationPsw->save($connexion);
    }

    /**
     * Permet de sauvgarder les informations du demande de modification du mot de passe.
     *
     * @param string $email l'email de l'utilisateur,String $typeUser le type de l'utilisateur,String $jeton le jeton
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function generateMailModificationPassWord($mail, $typeUser, $jeton)
    {
        $pfName = (new Atexo_Config())->toPfEncoding(Atexo_Config::getParameter('PF_LONG_NAME'));
        $from = Atexo_Config::getParameter('PF_MAIL_FROM');
        $footer = prado::localize('BAS_PAGE_MAIL');
        $domain = $_SERVER['HTTP_HOST'];
        $requestDomain = (new CommonPlateformeVirtuelleQuery())->findOneByDomain(
            $domain
        );
        if ($requestDomain instanceof CommonPlateformeVirtuelle) {
            $from = $requestDomain->getNoReply();
            $pfName = $requestDomain->getFromPfName();
            $footer = $requestDomain->getFooterMail();
        }

        $subject = Prado::Localize('DEMANDE_RE_INITIALISER_MOT_PASSE');

        $corps = file_get_contents(Atexo_Config::getParameter('PATH_FILE_MAIL_MODIFICATION_PASSWORD'));
        $corps = str_replace('TITRE_OBJET', Prado::localize('DEMANDE_RE_INITIALISER_MOT_PASSE'), $corps);
        $corps = str_replace('IMAGE_BANDEAU', Atexo_Message::getImageBandeau(), $corps);
        $corps = str_replace('COULEUR_TITRE', Atexo_Config::getParameter('MAIL_COULEUR_TITRE'), $corps);
        $corps = str_replace('PF_LONG_NAME', $pfName, $corps);
        $corps = str_replace('DEFINE_TEXT_BONJOUR', Prado::localize('DEFINE_TEXT_BONJOUR'), $corps);
        $corps = str_replace('DEMANDE_MODIFICATION_PSW_FAITE', Prado::localize('DEMANDE_MODIFICATION_PSW_FAITE'), $corps);
        $corps = str_replace('LIEN_CI_DESSOUS_POUR_RENSEIGNER_PSW', Prado::localize('LIEN_CI_DESSOUS_POUR_RENSEIGNER_PSW'), $corps);
        $option = ['slash' => true];
        $pfUrl = Atexo_MultiDomaine::getDomaine(Atexo_Config::getParameter('PF_URL_REFERENCE'), $option);
        $corps = str_replace('LIEN_MODIFICATION_PSW', $pfUrl.'index.php?page=Agent.ModifierPassword&jeton='.$jeton.'&typeUser='.$typeUser, $corps);
        $corps = str_replace('RE_INITIALISER_MON_PSW', Prado::localize('RE_INITIALISER_MON_PSW'), $corps);
        $corps = str_replace('VALIDITE_DU_LIEN', Prado::localize('VALIDITE_DU_LIEN').' '.Atexo_Config::getParameter('DUREE_VALIDITE_JETON_MODIFICATION_PASSWORD').
             Prado::localize('ABBR_HEURE_MAJUSCULE'), $corps);
        $corps = str_replace('CORDIALEMENT', Prado::localize('CORDIALEMENT'), $corps);
        $corps = str_replace('BAS_PAGE_MAIL', $footer, $corps);

        $to = $mail;

        return Atexo_Message::simpleMail(
            $from,
            $to,
            $subject,
            $corps,
            '',
            '',
            false,
            true,
            true,
            $pfName
        );
    }

    /**
     * Permet de setter le message.
     *
     * @param string $typeMessage le type du message
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function setBlocMessage($typeMessage)
    {
        switch ($typeMessage) {
            case 'msg-confirmation':
                $this->panelMessage->setMessage(Prado::Localize('TEXT_MOT_PASSE_ENVOYE').Atexo_Config::getParameter('DUREE_VALIDITE_JETON_MODIFICATION_PASSWORD').
                                            Prado::localize('ABBR_HEURE_MAJUSCULE'));
                                        $this->panelMessage->setVisible(true);
                                        $this->panelMessage->setTypeMessage('msg-confirmation');
                break;
            case 'msg-erreur':
                $this->panelMessage->setMessage(Prado::Localize('TEXT_MOT_PASSE_NON_ENVOYE'));
                                        $this->panelMessage->setVisible(true);
                                        $this->panelMessage->setTypeMessage('msg-erreur');
                break;
            case 'msg-erreur-bloquer':
                $this->panelMessage->setMessage(Prado::Localize('MESSAGE_COMPTE_VERROUILLE'));
                                        $this->panelMessage->setVisible(true);
                                        $this->panelMessage->setTypeMessage('msg-erreur');
                break;
            case 'msg-avertissement':
                $this->panelMessage->setMessage(Prado::Localize('LIEN_MODIFICATION_MDP_EXPIRE'));
                                        $this->panelMessage->setTypeMessage('msg-avertissement');
                                        $this->panelMessage->setVisible(true);
                break;
            case 'msg-unique':
                $this->panelMessage->setMessage(Prado::Localize('TEXT_MOT_PASSE_UNIQUE'));
                                        $this->panelMessage->setTypeMessage('msg-confirmation');
                                        $this->panelMessage->setVisible(true);
                break;
            case 'msg-email-not-valid':
                $this->panelMessage->setMessage(Prado::Localize('TEXT_EMAIL_NOT_VALID'));
                                            $this->panelMessage->setTypeMessage('msg-erreur');
                                            $this->panelMessage->setVisible(true);
                break;
        }
    }
}
