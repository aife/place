<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_Certificat;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Exception;
use Prado\Web\UI\WebControls\TDropDownList;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class DropDownListBiCle extends TDropDownList
{
    private $_typeBiCle;
    private array $_servicesIds = [];
    private $_organisme;
    private string $_entete = '';

    public function getTypeBiCle()
    {
        return $this->_typeBiCle;
    }

    public function setTypeBiCle($typeBiCle)
    {
        $this->_typeBiCle = $typeBiCle;
    }

    public function getServicesIds()
    {
        return $this->_servicesIds;
    }

    public function setServicesIds(array $servicesIds)
    {
        $this->_servicesIds = $servicesIds;
    }

    public function getOrganisme()
    {
        return $this->_organisme;
    }

    public function setOrganisme($organisme)
    {
        $this->_organisme = $organisme;
    }

    public function getEntete()
    {
        return $this->_entete;
    }

    public function setEntete($entete)
    {
        $this->_entete = $entete;
    }

    public function fillDataList()
    {
        if (($this->getTypeBiCle() != Atexo_Config::getParameter('BI_CLE_PERMANENT') &&
          $this->getTypeBiCle() != Atexo_Config::getParameter('BI_CLE_SECOURS')) ||
          !$this->getOrganisme() || !is_array($this->_servicesIds)) {
            return;
        }

        $listeBiCle = [];

        foreach ($this->getServicesIds() as $idService) {
            $listeBiCle = array_merge($listeBiCle, (new Atexo_Certificat())->retrieveArrayBiCle($idService, $this->getTypeBiCle(), $this->getOrganisme(), true));
        }
        if (is_array($listeBiCle)) {
            $biClesCleaned = (new Atexo_Certificat())->removeDoublons($listeBiCle);
            $listeBiCle = (new Atexo_Certificat())->getArrayNomBiCleFromArrayObject($biClesCleaned);

            if ($this->getEntete()) {
                $listeBiCle[0] = $this->getEntete();
                ksort($listeBiCle);
            }
            ksort($listeBiCle);
            $this->DataSource = $listeBiCle;
            $this->DataBind();
        } else {
            throw new Atexo_Exception('Expected datasource to be an array');
        }
    }
}
