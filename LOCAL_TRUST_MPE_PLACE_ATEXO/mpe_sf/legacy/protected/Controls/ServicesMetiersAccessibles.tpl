<!--Debut Bloc Services Métiers accessibles-->
	<com:TPanel ID="panelServicesMetierAccessibles" CssClass="form-field" Visible="true">
		<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><span class="title"><com:TTranslate>TEXT_SERVICES_METIERS_ACCESSIBLES</com:TTranslate></span></div>
		<div class="content">

			<div class="spacer-small"></div>
			
			<com:TRepeater ID="serviceMetier" EnableViewState="true" >
				<prop:ItemTemplate>
						<!--Debut Ligne service-->
						<com:TPanel CssClass="line-50pourcent clear-both" ID="panel1" Enabled="<%#$this->Data['activate']%>" >
							<div class="intitule-auto">
								<label for="compte1">
									<com:TCheckBox ID="service" Checked="<%#$this->Data['checked']%>" Attributes.title="<%#Prado::localize($this->Data['denomination'])%>" CssClass="check" />
									<com:THiddenField ID="idService" Value="<%#$this->Data['idServiceMetier'] %>"/>
									
								</label>
							</div>
							<div class="intitule-auto bloc-300"><com:TLabel Text ="<%#$this->Data['sigle']%> : <%#Prado::localize($this->Data['denomination'])%>"/></div>
						</com:TPanel>
						<com:TPanel CssClass="line-50pourcent" ID="panel2" Enabled="<%#$this->Data['activate']%>" Visible="<%#$this->TemplateControl->getAfficherProfil()%>">
							<div class="intitule-auto"><label for="profil_1"><com:TTranslate>TEXT_PROFIL</com:TTranslate></label> : </div>
							<com:TDropDownList ID="profils" Attributes.title="<%=Prado::localize('TEXT_PROFIL')%>" CssClass="bloc-300" DataSource=<%#$this->Data['profils']%>/>
         					<com:TCustomValidator
         							ID="profilsCustomValidator"
									ValidationGroup="validateInfoAccount"
									ControlToValidate="profils"
									ClientValidationFunction="controleProfil<%#$this->ItemIndex%>"
                                	Display="Dynamic"
									ErrorMessage="<%#Prado::localize('TEXT_PROFIL')%> <%#$this->Data['sigle']%>"
                                    Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
									EnableClientScript="true" > 						 					 
									<prop:ClientSide.OnValidationError>
   				 						document.getElementById('divValidationSummary').style.display='';
 									</prop:ClientSide.OnValidationError>
								 </com:TCustomValidator> 
    							<com:TCustomValidator
            						ControlToValidate="profils" 
            						ValidationGroup="validateInfoAccount" 
            						Display="Dynamic" 
            						EnableClientScript="true"
            						ID="profilsValidator"
            						onServerValidate="Page.servicesMetiersAccessibles.verifyServicesMetiers"
            						Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
         						</com:TCustomValidator>
								<com:TCustomValidator
										ValidationGroup="validateInfoAccount"
										ControlToValidate="profils"
										visible="<%#(!$_GET['id'])%>"
										ClientValidationFunction="enabledSiCreation"
										Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
										ErrorMessage="<%#Prado::localize('TEXT_PROFIL')%> <%#$this->Data['sigle']%>"
										Display="Dynamic"
										>
									<prop:ClientSide.OnValidationError>
										document.getElementById('divValidationSummary').style.display='';
									</prop:ClientSide.OnValidationError>
								</com:TCustomValidator>
								<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" onmouseover="afficheBulle('infosProfil', this)" onmouseout="cacheBulle('infosProfil')" class="picto-info" alt="Info-bulle" title="Info-bulle" />
						</com:TPanel>
						<!--Fin Ligne service-->
				</prop:ItemTemplate>
			</com:TRepeater>
			<com:TCustomValidator
					ValidationGroup="validateInfoAccount" 
					ControlToValidate="nbreServices"
					ClientValidationFunction="servicesAccessibles" 
					Display="Dynamic"
					ErrorMessage="<%=Prado::localize('ERROR_SERVICE_ACCESSIBLE')%>"
					EnableClientScript="true" 
					Text=" ">    					
					<prop:ClientSide.OnValidationError>
			 						document.getElementById('divValidationSummary').style.display='';
					</prop:ClientSide.OnValidationError>
			 </com:TCustomValidator>
		</div>
		<com:TLabel ID="Script"/>
		<div class="breaker"></div>
		<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
	</com:TPanel>
	<com:THiddenField id="nbreServices" value="<%=count($this->remplirDataSourceServicesMetiersAgent())%>"/>
	<!--Debut Infos-Bulles-->
<div id="infosProfil" class="info-bulle" onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();"><div><com:TTranslate>TEXT_INFO_BULLE_PROFIL</com:TTranslate></div></div>
<!--Fin Bloc Services Métiers accessibles-->
