<!--Debut Formulaire-->
<com:AtexoValidationSummary  id="validSummary" ValidationGroup="validateCompte"/>
<com:TActivePanel id="panelError">
	<com:PanelMessageErreur ID="panelMessageErreur" visible="false"/>
</com:TActivePanel>
<com:BlocInformationsCompte  ID="blocInformations"/>
<div class="form-bloc">
	<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
	<div class="content">
		<label>
			<com:TActiveHiddenField id="doNotRemove"/><!-- Champ caché car impossible de récupérer la valeur du premier champ via Prado -->
			<com:TRadioButton
					cssClass="radio"
					GroupName="AssocierCompte"
					id="inputCompteExistant"
					Attributes.onclick="isCheckedShowDiv(this,'panel_compteExistant');isCheckedHideDiv(this,'panel_nouveauCompte');"
			/>
			<strong><com:TTranslate>A_UN_COMPTE_EXISTANT</com:TTranslate></strong>
		</label>
		<div id="panel_compteExistant" style="display:none;">
			<div class="spacer-mini"></div>
			<div class="line">
				<div class="intitule intitule-150"><label for="compteAssocie"><%=Prado::localize("TEXT_IDENTIFIANT")%> :</label></div>
				<com:TActiveTextBox id="compteAssocie" CssClass="input-185" /><com:TActiveImage id="error"  ImageUrl='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif'  Attributes.alt='Champ obligatoire' Style="display:none" />
				<com:TCustomValidator
					Id="idValidation"
					ControlToValidate="compteAssocie"
					ValidationGroup="validateCompte"
					onServerValidate="chercherAgent"
					Display="Dynamic"
					Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
				>
					<prop:ClientSide.OnValidationError>
						document.getElementById('validSummary').style.display='';
					</prop:ClientSide.OnValidationError>
				</com:TCustomValidator>
			</div>
		</div>
	</div>
	<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
</div>

<div class="form-bloc">
	<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
	<div class="content">
		<label>
			<com:TRadioButton
					cssClass="radio"
					GroupName="AssocierCompte"
					id="inputNouveauCompte"
					Attributes.onclick="isCheckedShowDiv(this,'panel_nouveauCompte');isCheckedHideDiv(this,'panel_compteExistant');"
			/>
			<strong><com:TTranslate>A_UN_NOUVEAU_COMPTE</com:TTranslate></strong></label>

		<div id="panel_nouveauCompte" style="display:none;">
			<div class="float-right"><com:TTranslate>DEFINE_SYMBOLE</com:TTranslate> <abbr class="champ-oblig" title="<%=Prado::localize('DEFINE_OBLIGATOIRE')%>">*</abbr> <com:TTranslate>DEFINE_INDIQUE_CHAMPS_OBLIGATOIRES</com:TTranslate></div>
			<div class="spacer-mini"></div>
			<!--Debut Bloc Identification Agent-->
			<fieldset>
				<legend><com:TTranslate>TEXT_IDENTIFICATION_AGENT</com:TTranslate> </legend>
				<div class="spacer-small"></div>
				<div class="line">
					<div class="intitule-150"><label for="nomAgent"><com:TTranslate>DEFINE_NOM</com:TTranslate></label><abbr class="champ-oblig" title="<%=Prado::localize('DEFINE_OBLIGATOIRE')%>">*</abbr> :</div>
					<com:TActiveTextBox id="nomAgent" Attributes.title="<%=Prado::localize('DEFINE_NOM')%>" CssClass="small" />
					<com:TRequiredFieldValidator
						id="ValidatorNomAgent"
						ControlToValidate="nomAgent"
						ValidationGroup="validateCompte"
						Display="Dynamic"
						ErrorMessage="<%=Prado::localize('TEXT_NOM_MAJ')%>"
						EnableClientScript="true"
						Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
					>
						<prop:ClientSide.OnValidate>
							sender.enabled = (J('<%= $this->inputNouveauCompte->ClientID %>').checked == true) ;
						</prop:ClientSide.OnValidate>
						<prop:ClientSide.OnValidationError>
							document.getElementById('validSummary').style.display='';
						</prop:ClientSide.OnValidationError>
					</com:TRequiredFieldValidator>
				</div>
				<div class="line">
					<div class="intitule-150">
						<label for="prenomAgent"><com:TTranslate>DEFINE_PRENOM</com:TTranslate></label><abbr class="champ-oblig" title="<%=Prado::localize('DEFINE_OBLIGATOIRE')%>">*</abbr> :</div>
						<com:TActiveTextBox id="prenomAgent" Attributes.title="<%=Prado::localize('DEFINE_PRENOM')%>" CssClass="small" />
					<com:TRequiredFieldValidator
							id="ValidatorPrenomAgent"
							ControlToValidate="prenomAgent"
							ValidationGroup="validateCompte"
							Display="Dynamic"
							ErrorMessage="<%=Prado::localize('TEXT_PRENOM_SANS_POINTS')%>"
							EnableClientScript="true"
							Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
					>
						<prop:ClientSide.OnValidate>
							sender.enabled = (J('<%= $this->inputNouveauCompte->ClientID %>').checked == true) ;
						</prop:ClientSide.OnValidate>
						<prop:ClientSide.OnValidationError>
							document.getElementById('validSummary').style.display='';
						</prop:ClientSide.OnValidationError>
					</com:TRequiredFieldValidator>
				</div>
				<div class="line">
					<div class="intitule-150">
						<label for="email"><com:TTranslate>TEXT_ADRESSE_ELECTRONIQUE</com:TTranslate></label><abbr class="champ-oblig" title="<%=Prado::localize('DEFINE_OBLIGATOIRE')%>">*</abbr> :</div>
						<com:TActiveTextBox id="emailAgent" Attributes.title="<%=Prado::localize('TEXT_ADRESSE_ELECTRONIQUE')%>" CssClass="small" />
						<com:TActiveImage id="errorEmail"  ImageUrl='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif'  Attributes.alt='Champ obligatoire' Style="display:none" />
					<com:TRequiredFieldValidator
							id="ValidatorEmailAgent"
							ControlToValidate="emailAgent"
							ValidationGroup="validateCompte"
							Display="Dynamic"
							ErrorMessage="<%=Prado::localize('TEXT_ADRESSE_ELECTRONIQUE')%>"
							EnableClientScript="true"
							Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
					>
						<prop:ClientSide.OnValidate>
							sender.enabled = (J('<%= $this->inputNouveauCompte->ClientID %>').checked == true) ;
						</prop:ClientSide.OnValidate>
						<prop:ClientSide.OnValidationError>
							document.getElementById('validSummary').style.display='';
						</prop:ClientSide.OnValidationError>
					</com:TRequiredFieldValidator>
				</div>
				<!--Debut Ligne Entite publique-->
				<div class="line">
					<div class="intitule-150">
						<label for="organismesNames" style=""><com:TTranslate>TEXT_MINISTERE</com:TTranslate></label><abbr class="champ-oblig" title="<%=Prado::localize('DEFINE_OBLIGATOIRE')%>">*</abbr> :</div>
						<com:TActiveDropDownList ID="organismesNames" cssClass="long" onCallBack = "displayEntityPurchase" Attributes.title="<%=Prado::localize('TEXT_MINISTERE')%>"/>
						<com:TCompareValidator
							ControlToValidate="organismesNames"
							ValidationGroup="validateCompte"
							ValueToCompare="0"
							DataType="String"
							Operator="NotEqual"
							Display="Dynamic"
							ErrorMessage="<%=Prado::localize('TEXT_MINISTERE')%>"
							Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
						>
							<prop:ClientSide.OnValidate>
								sender.enabled = (J('<%= $this->inputNouveauCompte->ClientID %>').checked == true) ;
							</prop:ClientSide.OnValidate>
							<prop:ClientSide.OnValidationError>
								document.getElementById('validSummary').style.display='';
							</prop:ClientSide.OnValidationError>
						</com:TCompareValidator>
				</div>
				<!--Fin Ligne Entite publique-->
				<!--Debut Ligne Service-->
				<div class="line">
					<div class="intitule-150"><label for="entityPurchaseNames"><com:TTranslate>DEFINE_SERVICE</com:TTranslate></label><abbr class="champ-oblig" title="<%=Prado::localize('DEFINE_OBLIGATOIRE')%>">*</abbr> :</div>
					<div class="content-bloc bloc-600">
						<com:TActiveDropDownList ID="entityPurchaseNames" onCallBack = "displayEntityPurchase" cssClass="long float-left" AutoPostBack="true" Attributes.title="<%=Prado::localize('DEFINE_SERVICE')%>" />
						<com:TCompareValidator
								ControlToValidate="entityPurchaseNames"
								ValidationGroup="validateCompte"
								ValueToCompare=""
								DataType="String"
								Operator="NotEqual"
								Display="Dynamic"
								ErrorMessage="<%=Prado::localize('DEFINE_SERVICE')%>"
								Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
						>
							<prop:ClientSide.OnValidate>
								sender.enabled = (J('<%= $this->inputNouveauCompte->ClientID %>').checked == true) ;
							</prop:ClientSide.OnValidate>
							<prop:ClientSide.OnValidationError>
								document.getElementById('validSummary').style.display='';
							</prop:ClientSide.OnValidationError>
						</com:TCompareValidator>
					</div>
				</div>
				<!--Fin Ligne Service-->
			</fieldset>
			<!--Fin Bloc Identification Agent-->
			<!--Debut Bloc Coordonnées d'authentification-->
			<fieldset>
				<legend>Coordonnées</legend>

				<div class="spacer-small"></div>
				<div class="line">
					<div class="intitule-150">
						<label for="idAgent"><com:TTranslate>TEXT_IDENTIFIANT_SANS_POINTS</com:TTranslate></label>
						<abbr class="champ-oblig" title="<%=Prado::localize('DEFINE_OBLIGATOIRE')%>">*</abbr> :</div>
						<com:TActiveTextBox id="loginAgent" Attributes.title="Identifiant" CssClass="small" />
						<com:TActiveImage id="errorLogin"  ImageUrl='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif'  Attributes.alt='Champ obligatoire' Style="display:none" />
						<com:TRequiredFieldValidator
							ControlToValidate="loginAgent"
							ValidationGroup="validateCompte"
							Display="Dynamic"
							ErrorMessage="<%=Prado::localize('TEXT_IDENTIFIANT_SANS_POINTS')%>"
							EnableClientScript="true"
							Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
						>
							<prop:ClientSide.OnValidate>
								sender.enabled = (J('<%= $this->inputNouveauCompte->ClientID %>').checked == true) ;
							</prop:ClientSide.OnValidate>
							<prop:ClientSide.OnValidationError>
								document.getElementById('validSummary').style.display='';
							</prop:ClientSide.OnValidationError>
						</com:TRequiredFieldValidator>
				</div>
				<div class="line">
					<div class="intitule-150"><label for="passwordAgent"><com:TTranslate>TEXT_PASSWORD</com:TTranslate></label>
						<abbr class="champ-oblig" title="<%=Prado::localize('DEFINE_OBLIGATOIRE')%>">*</abbr>

						<a class="picto-info-intitule" onblur="cacheBulle('infosPassword')" onfocus="afficheBulle('infosPassword', this)" onmouseout="cacheBulle('infosPassword')" onmouseover="afficheBulle('infosPassword', this)" >
							<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" />
						</a>
						<div id="infosPassword" class="info-bulle" onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();"><div><com:TTranslate>REGLE_VALIDATION_MDP</com:TTranslate>. </div></div>
						:</div>
						<com:TActiveTextBox id="passwordAgent" TextMode="Password" Attributes.title="<%=Prado::localize('TEXT_PASSWORD')%>" CssClass="small" Attributes.autocomplete="off"/>
					<com:TRequiredFieldValidator
							ControlToValidate="passwordAgent"
							ValidationGroup="validateCompte"
							Display="Dynamic"
							ErrorMessage="<%=Prado::localize('TEXT_MOT_PASSE_SANS_POINT')%>"
							EnableClientScript="true"
							Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
					>
						<prop:ClientSide.OnValidate>
							sender.enabled = (J('<%= $this->inputNouveauCompte->ClientID %>').checked == true) ;
						</prop:ClientSide.OnValidate>
						<prop:ClientSide.OnValidationError>
							document.getElementById('validSummary').style.display='';
						</prop:ClientSide.OnValidationError>
					</com:TRequiredFieldValidator>
					<com:TCustomValidator
							id="validatorPasswordAgent"
							ControlToValidate="passwordAgent"
							ValidationGroup="validateCompte"
							ClientValidationFunction="verifierMdp"
							ErrorMessage="<%=Prado::localize('REGLE_VALIDATION_MDP')%>"
							visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('AgentControleFormatMotDePasse'))? true:false%>"
							Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
					>
						<prop:ClientSide.OnValidate>
							sender.enabled = (J('<%= $this->inputNouveauCompte->ClientID %>').checked == true) ;
						</prop:ClientSide.OnValidate>
						<prop:ClientSide.OnValidationError>
							document.getElementById('validSummary').style.display='';
						</prop:ClientSide.OnValidationError>
					</com:TCustomValidator>
				</div>
				<div class="line">
					<div class="intitule-150">
						<label for="confirmePasswordAgent"><com:TTranslate>TEXT_CONFIRME_MOT_PASSE_SANS_POINTS</com:TTranslate> :</label><abbr class="champ-oblig" title="<%=Prado::localize('DEFINE_OBLIGATOIRE')%>">*</abbr>
					</div>
					<com:TActiveTextBox id="confirmePasswordAgent" TextMode="Password" Attributes.title="<%=Prado::localize('TEXT_CONFIRME_MOT_PASSE_SANS_POINTS')%>" CssClass="small" />
					<com:TCompareValidator
						ControlToValidate="passwordAgent"
						ValidationGroup="validateCompte"
						ControlToCompare="confirmePasswordAgent"
						ErrorMessage="<%=Prado::localize('DEFINE_ERREUR_SAISIE_MDP')%>"
						Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>	"
					>
						<prop:ClientSide.OnValidate>
							sender.enabled = (J('<%= $this->inputNouveauCompte->ClientID %>').checked == true) ;
						</prop:ClientSide.OnValidate>
						<prop:ClientSide.OnValidationError>
							document.getElementById('validSummary').style.display='';
						</prop:ClientSide.OnValidationError>
					</com:TCompareValidator>
				</div>
			</fieldset>

			<!--Fin Bloc Coordonnées d'authentification-->
			<!--Debut Bloc Services Métiers accessibles-->

			<fieldset>
				<legend><com:TTranslate>TEXT_SERVICES_METIERS_ACCESSIBLES</com:TTranslate></legend>
				<div class="spacer-small"></div>
				<com:TRepeater ID="serviceMetier" DataSource="<%=$this->getDataSourceServiceMetier()%>">
					<prop:ItemTemplate>
						<div class="line-50pourcent clear-both">
							<com:TCheckBox ID="checkService" Attributes.title="<%#$this->Data['denomination']%>" CssClass="check" />
							<com:THiddenField ID="idService" Value="<%#$this->Data['idServiceMetier'] %>"/>
							<com:TLabel Attributes.for="idService" Text="<%#$this->Data['denomination']%>"/>
						</div>
						<div class="line-50pourcent">
							<div class="intitule-auto"><label for="profil">Profil</label> : </div>
							<com:TDropDownList ID="profils" Attributes.title="<%=Prado::localize('TEXT_PROFIL')%>" CssClass="bloc-300" DataSource="<%#$this->Data['profils']%>" />
							<a class="picto-info-intitule" onblur="cacheBulle('infosProfil')" onfocus="afficheBulle('infosProfil', this)" onmouseout="cacheBulle('infosProfil')" onmouseover="afficheBulle('infosProfil', this)">
								<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" />
							</a>
							<div id="infosProfil" class="info-bulle" onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();"><div><com:TTranslate>TEXT_CHOIX_PROFIL_HABILITATIONS_DEFAUT</com:TTranslate>.</div></div>
						</div>
					</prop:ItemTemplate>
				</com:TRepeater>
				<com:THiddenField ID="toto" />
				<com:TCustomValidator
					ControlToValidate="toto"
					ValidationGroup="validateCompte"
					ClientValidationFunction="verifyCheckServiceMetier"
					Display="Dynamic"
					ErrorMessage="<%=Prado::localize('ERROR_SERVICE_ACCESSIBLE')%>"
					EnableClientScript="true"
					Text=" ">
					<prop:ClientSide.OnValidate>
						sender.enabled = (J('<%= $this->inputNouveauCompte->ClientID %>').checked == true) ;
					</prop:ClientSide.OnValidate>
					<prop:ClientSide.OnValidationError>
						document.getElementById('validSummary').style.display='';
					</prop:ClientSide.OnValidationError>
				</com:TCustomValidator>
			</fieldset>
			<!--Fin Bloc Services Métiers accessibles-->

		</div>
	</div>
	<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
</div>
<!--Fin Formulaire-->
<!--Debut line boutons-->
<div class="boutons-line">
	<com:TActiveButton 
	Cssclass="bouton-moyen float-left" 
	Text = "<%=Prado::localize('ICONE_ANNULER')%>" 
	Attributes.title="<%=Prado::localize('ICONE_ANNULER')%>" 
	Attributes.onclick="resetForm();document.getElementById('<%=$this->error->getClientId()%>').style.display ='none' ;J('.compte-association').dialog('close');" />
	<com:TActiveButton 
	    id="btEnregister"
		Cssclass="bouton-moyen float-right" 
		validationGroup="validateCompte"
		Text = "<%=Prado::localize('TEXT_ENREGISTER')%>"
		OnCallBack="associerAgent"
		Attributes.onclick="document.getElementById('<%=$this->error->getClientId()%>').style.display ='none' ;  getIdClientErreur('<%=$this->idClientErreur->getClientId()%>','<%=$this->error->getClientId()%>');"
		Attributes.title="<%=Prado::localize('TEXT_ENREGISTER')%>"
	 />
</div>
<com:TActiveLabel id="script" style="display:none"/>
<com:TActiveLabel id="script2" style="display:none"/>
<com:TActiveHiddenField ID="idClientErreur"/>
<script>
	function resetForm(){
		J('#panel_nouveauCompte').find('input[type=text], textarea').val('');
		J('#panel_nouveauCompte').find('input[type=checkbox]').attr('checked', false);
		J('#panel_nouveauCompte').find('select').val('0');
	}
</script>

<script>
	function verifyCheckServiceMetier(){
		var checked = false;
		J("input[type=checkbox]:checked").each(
				function() {
					var id = J(this).attr("id");
					var res = id.replace(/checkService/gi, "profils");
					var option = J( "#"+res ).val();
					if(option != '0'){
						checked = true;
					}
				}
		);
		return checked
	}
</script>
<!--Fin line boutons-->