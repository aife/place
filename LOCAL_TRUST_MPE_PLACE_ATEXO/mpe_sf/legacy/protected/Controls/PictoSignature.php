<?php

namespace Application\Controls;

use Application\Service\Atexo\Controller\Atexo_Controller_Front;
use Prado\Prado;
use Prado\Web\UI\ActiveControls\TActiveImage;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PictoSignature extends TActiveImage
{
    private $_size;

    public function getSize()
    {
        return $this->_size;
    }

    public function setSize($size)
    {
        return $this->_size = $size;
    }

    public function onLoad($param)
    {
        if ('BIG' == strtoupper($this->_size)) {
            $this->setImageUrl(Atexo_Controller_Front::t().'/images/picto-signature-big.gif');
        } else {
            $this->setImageUrl(Atexo_Controller_Front::t().'/images/picto-signature.gif');
        }
        $this->setToolTip(Prado::localize('TEXT_SIGNATURE_ELECTRONIQUE'));
        $this->Attributes->alt = Prado::localize('TEXT_SIGNATURE_ELECTRONIQUE');
    }
}
