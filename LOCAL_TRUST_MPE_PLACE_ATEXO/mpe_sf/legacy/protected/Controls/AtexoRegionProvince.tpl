   	<com:TActivePanel ID="activePanelRegionProvince" >
   		<com:TLabel ID="script"/>
    <!--Debut Ligne Région-->
    	<com:TPanel ID="panelRegion" Cssclass="line">
    		<div class="intitule-150"><label for="regionSiegeSocial"><com:TTranslate>TEXT_REGION</com:TTranslate></label>
    		<com:TLabel ID="regionObligatoire" cssClass="champ-oblig" Text ="*"/> :</div>
    			<div class="content-bloc">
    				<com:TActiveDropDownList onCallBack="onCallBackRegion" AutoPostBack="true" ID="regionSiegeSocial" Attributes.Title="<%=Prado::localize('TEXT_REGION')%>" cssClass="select-185" />
    				<span style="display:none" id="spanRegion" title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>
    			</div>
    	</com:TPanel>
    <!--Fin Ligne Région-->
    <!--Debut Ligne Province-->
    	<com:TActivePanel ID="panelProvince" cssClass="line">
    		<com:TLabel ID="script2"/>
    		<div class="intitule-150"><label for="provinceSiegeSocial"><com:TTranslate>TEXT_PROVINCE</com:TTranslate></label>
    			<com:TLabel ID="provinceObligatoire" cssClass="champ-oblig" Text ="*"/> :</div>
    			<div class="content-bloc">
    				<com:TActiveDropDownList ID="provinceSiegeSocial" onCallBack="onCallBackProvince" Attributes.Title="<%=Prado::localize('TEXT_PROVINCE')%>" cssClass="select-185" />
    				<span style="display:none" id="spanProvince" title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>
    			</div>
    	</com:TActivePanel>
    <!--Fin Ligne Province-->
    
</com:TActivePanel>
<com:TCustomValidator
  		Enabled = "false"
    	ID="validatorRegion"
    	ValidationGroup="validateCreateCompte"
    	ControlToValidate="regionSiegeSocial"
    	Display="Dynamic"
    	ErrorMessage="<%=Prado::localize('TEXT_REGION')%>"
    	ClientValidationFunction="validateRegion"
    	Text="<span title='Champ obligatoire' class='check-invalide'></span>" >
    	<prop:ClientSide.OnValidationError>
    		document.getElementById('divValidationSummary').style.display='';
    	</prop:ClientSide.OnValidationError>
    </com:TCustomValidator>
    
    <com:TCustomValidator
    	Enabled = "false"
    	ID="validatorProvince"
    	ValidationGroup="validateCreateCompte"
    	ControlToValidate="provinceSiegeSocial"
    	Display="Dynamic"
    	ErrorMessage="<%=Prado::localize('TEXT_PROVINCE')%>"
    	ClientValidationFunction="validateProvince"
    	Text="<span title='Champ obligatoire' class='check-invalide'></span>" >
    	<prop:ClientSide.OnValidationError>
    		document.getElementById('divValidationSummary').style.display='';
    	</prop:ClientSide.OnValidationError>
    </com:TCustomValidator>