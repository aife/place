<?php

namespace Application\Controls;

use App\Exception\ConnexionWsException;
use App\Service\Publicite\EchangeConcentrateur;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonConsultation;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_MultiDomaine;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Config\Atexo_Config_Exception;
use Exception;
use Prado\Prado;

/**
 * Class AtexoPubliciteRecap.
 */
class AtexoPubliciteRecap extends MpeTTemplateControl
{
    private bool $isValidationCons = false;

    /**
     * @var string
     */
    private $token;

    /**
     * @var string
     */
    private $xml;

    private ?\Application\Propel\Mpe\CommonConsultation $consultation = null;

    public function isValidationCons(): bool
    {
        return $this->isValidationCons;
    }

    public function setIsValidationCons(bool $isValidationCons): AtexoPubliciteRecap
    {
        $this->isValidationCons = $isValidationCons;

        return $this;
    }

    /**
     * @param $cons
     * @param null $param
     *
     * @throws Atexo_Config_Exception
     */
    public function charger($cons, $param = null)
    {
        $token = null;
        $xml = null;
        $logger = Atexo_LoggerManager::getLogger('publicite');
        $this->hasMol->setText('');
        $this->isEnvoiChecked->setValue('false');
        if ($cons instanceof CommonConsultation && $cons->getId()) {
            $this->consultation = $cons;
            $serviceSF = Atexo_Util::getSfService(EchangeConcentrateur::class);
            try {
                $token = $serviceSF->getToken($cons->getId());
            } catch (ConnexionWsException $e) {
                $logger->error($e->getMessage() . ' ' . $e->getTraceAsString());
            } catch (Exception $e) {
                $logger->error(
                    'Probleme lors recuperation du token Concentrateur'
                    . $e->getMessage()
                    . ' ' . $e->getTraceAsString()
                );
            }
            try {
                $xml = $serviceSF->generateXmlAnnonce($cons->getId());
            } catch (Exception $e) {
                $logger->error(
                    'Probleme lors generation xml annonce'
                    . $e->getMessage()
                    . ' ' . $e->getTraceAsString()
                );
            }
            if (!empty($token) && !empty($xml)) {
                $this->xml = $xml;
                $this->token = $token;
                $this->panelConcentrateur->setDisplay('Dynamic');
                $this->panelBlocErreur->setDisplay('None');
            } else {
                $this->panelConcentrateur->setDisplay('None');
                $this->panelBlocErreur->setDisplay('Dynamic');
            }
        }
        if ($param) {
            $this->publiciteRecap->render($param->getNewWriter());
        }
    }

    /**
     * @return string
     */
    public function getHtml()
    {
        $res = '';
        $cons = $this->consultation;
        if ($cons instanceof CommonConsultation) {
            $clientIdEnvoi = '';
            if ($this->isValidationCons()) {
                $clientIdEnvoi = $this->isEnvoiChecked->getClientId();
            }
            $clientIdMolSelected = $this->hasMol->getClientId();
            $xmlEncoded = base64_encode($this->xml);
            $uidPf = Atexo_Config::getParameter('UID_PF_MPE');
            $uidConsultation = $cons->getId();
            $traductions = $this->getTraduction();
            $referenceUtilisateur = $cons->getReferenceUtilisateur();
            $res = <<<EOD
                  <annonce-recap 
                        plateforme='$uidPf' 
                        id-consultation='$uidConsultation' 
                        access-token='$this->token' 
                        xml-annonce='$xmlEncoded'
                        is-mol-selected='$clientIdMolSelected' 
                        traductions='$traductions' 
                        reference-utilisateur='$referenceUtilisateur' 
                        is-envoi-validation='$clientIdEnvoi' />
                EOD;
        } else {
            $this->panelConcentrateur->setDisplay('None');
            $this->panelBlocErreur->setDisplay('None');
        }

        return $res;
    }

    /**
     * @return bool
     */
    public function isMolSelected()
    {
        return 'true' == $this->hasMol->getText();
    }

    /**
     * @return bool
     */
    public function isEnvoi()
    {
        return 'true' == $this->isEnvoiChecked->getValue();
    }

    /**
     * @param $cons
     *
     * @throws \Application\Library\Propel\Exception\PropelException
     */
    public function envoyerAnnonce($cons)
    {
        if ($cons instanceof CommonConsultation && !empty($cons->getOrganisme())) {
            $connexion = Propel::getConnection(
                Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE')
            );
            $cons->setIsEnvoiPubliciteValidation($this->isEnvoi());
            $cons->save($connexion);
        }
    }

    public function getTraduction(): false|string
    {
        $trad = [
            'NOM_COMPTE_PUB' => base64_encode(Prado::localize('NOM_COMPTE_PUB')),
            'RAPPEL_COMPLETER_AVIS_BOAMP_DANS_MOL' => base64_encode(Prado::localize('RAPPEL_COMPLETER_AVIS_BOAMP_DANS_MOL')),
            'ENVOI_PUBLICITE_SIMPLIFIER_APRES_VALIDATION_CONSULTATION' => base64_encode(Prado::localize('ENVOI_PUBLICITE_SIMPLIFIER_APRES_VALIDATION_CONSULTATION')),
            'EXPLICATION_PUBLICITE_SIMPLIFIER_VALIDER_CONSULTATION' => base64_encode(Prado::localize('EXPLICATION_PUBLICITE_SIMPLIFIER_VALIDER_CONSULTATION')),
            'EXPLICATION_PUBLICITE_AVIS_BOAMP_VALIDER_CONSULTATION' => base64_encode(Prado::localize('EXPLICATION_PUBLICITE_AVIS_BOAMP_VALIDER_CONSULTATION')),
            'PREVISUALISEZ_AVIS_PUB' => base64_encode(Prado::localize('PREVISUALISER_PUBLICITE_SIMPLIFIEE')),
            'AUCUN_SUPPORT_SELECTIONNE' => base64_encode(Prado::localize('AUCUN_SUPPORT_SELECTIONNE')),
        ];
        $trad['NOUS_VOUS_RAPPELONS_QUE'] = base64_encode(Prado::localize('NOUS_VOUS_RAPPELONS_QUE'));
        $trad['RAPPEL_LES_ECHOS'] = base64_encode(Prado::localize('RAPPEL_LES_ECHOS'));

        return json_encode($trad, JSON_THROW_ON_ERROR);
    }

    /**
     * @return string|string[]
     */
    public function getUrlConcentrateur(): string|array
    {
        return Atexo_MultiDomaine::replaceDomain(Atexo_Config::getParameter('PF_URL_REFERENCE'));
    }

    public function setDisplay($style)
    {
        $this->publiciteRecap->setDisplay($style);
    }
}
