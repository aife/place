<com:TPanel ID="panelNoElementFound">
	<h2><com:TTranslate>DEFINE_AUCUN_INTERVENANT_EXTERNE_SELECTIONNE</com:TTranslate>&nbsp;</h2>
</com:TPanel>
<com:TPanel ID="panelMoreThanOneElementFound">
<div class="line-partitioner">
	<h2><com:TTranslate>DEFINE_NOMBRE_RESULTAT</com:TTranslate>&nbsp;<com:TLabel ID="nombreElement"/></h2>

	<div class="partitioner">
		<div class="intitule"><strong><label for="allCons_nbResultsTop"><com:TTranslate>DEFINE_TEXT_AFFICHER</com:TTranslate></label></strong></div>
		<com:TDropDownList ID="nombreResultatAfficherTop" AutoPostBack="true" onSelectedIndexChanged="changePagerLenght" Attributes.title='<%=Prado::localize("NOMBRE_DE_RESULTAT_PAR_PAGE")%>'>
  			<com:TListItem Text="10" Selected="10"/>
  			<com:TListItem Text="20" Value="20"/>
  			<com:TListItem Text="50" Value="50" />
  			<com:TListItem Text="100" Value="100" />
  			<com:TListItem Text="500" Value="500" />
  		</com:TDropDownList>
		<div class="intitule"><com:TTranslate>DEFINE_RESULTATS_PAGE</com:TTranslate></div>
			<label style="display:none;" for="allCons_pageNumberTop">Aller &agrave; la page</label>
			<com:TPanel ID="panelBouttonGotoPageTop" DefaultButton="DefaultButtonTop" CssClass="float-left">
				<com:TTextBox ID="numPageTop" Attributes.title='<%=Prado::localize("N_DE_LA_PAGE")%>' Text="1"/>
				<div class="nb-total ">/ <com:TLabel ID="nombrePageTop"/></div>
				<com:TButton ID="DefaultButtonTop"  OnClick="goToPage" Attributes.style="display:none"/>
			</com:TPanel>
		<div class="liens">
			<com:TPager ID="PagerTop"
           			ControlToPaginate="tableauIntervenant"
	           		FirstPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-premier.gif' alt='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>' title='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>' />"
		            LastPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-dernier.gif' alt='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>' title='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>' />"
	     	        Mode="NextPrev"
	          		NextPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-suivant.gif' alt='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>' title='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>' />"
		            PrevPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-precedent.gif' alt='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>' title='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>' />"
   	                OnPageIndexChanged="pageChanged"/>
		</div>
	</div>
</div>
<!--Debut Tableau intervenant-->
  <table class="table-results" summary="Liste des intervenants">
      <caption><com:TTranslate>DEFINE_LISTE_INTERVENANTS</com:TTranslate></caption>
      	<!--Fin partitionneur-->
	<com:TRepeater ID="tableauIntervenant"
	    AllowPaging="true"
	    PageSize="10"
	    EnableViewState="true"
	    AllowCustomPaging="true"
	    OnItemCommand="actionsRepeater">

			<prop:HeaderTemplate>
	      <thead>
	          <tr>
	              <th colspan="4" class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></th>
	          </tr>
	          <tr>
	          	  <com:TPanel visible="<%#$this->TemplateControl->getCheckbox()%>"><th id="checkIntervenant" class="check-col"></th></com:TPanel>
	              <th class="col-250"  >
	              	<com:TActiveLinkButton onCommand="Page.critereTriInt" onCallBack="Page.TrierInt" CommandName="nom">
	              		<com:TTranslate>DEFINE_NOM</com:TTranslate>
	              		<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/arrow-tri.gif" alt="<%=Prado::localize('ICONE_TRIER')%>" title="<%=Prado::localize('ICONE_TRIER')%>" />
	              	</com:TActiveLinkButton>&nbsp;
	              	<com:TActiveLinkButton onCommand="Page.critereTriInt" onCallBack="Page.TrierInt" CommandName="prenom">
	              		<com:TTranslate>DEFINE_PRENOM</com:TTranslate>
	              		<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/arrow-tri.gif" alt="<%=Prado::localize('ICONE_TRIER')%>" title="<%=Prado::localize('ICONE_TRIER')%>" />
	              	</com:TActiveLinkButton>
	              	<br/>
	              	<com:TActiveLinkButton onCommand="Page.critereTriInt" onCallBack="Page.TrierInt" CommandName="email">
	              		<com:TTranslate>TEXT_EMAIL</com:TTranslate>
	              		<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/arrow-tri.gif" alt="<%=Prado::localize('ICONE_TRIER')%>" title="<%=Prado::localize('ICONE_TRIER')%>" />
	              	</com:TActiveLinkButton>


	              </th>
	              <th class="col-250" >
	              	<com:TActiveLinkButton onCommand="Page.critereTriInt" onCallBack="Page.TrierInt" CommandName="organisation">
	              		<com:TTranslate>DEFINE_ORGANISATION</com:TTranslate>
	              		<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/arrow-tri.gif" alt="<%=Prado::localize('ICONE_TRIER')%>" title="<%=Prado::localize('ICONE_TRIER')%>" />
	              	</com:TActiveLinkButton>
	              	<br/>
	              	<com:TActiveLinkButton onCommand="Page.critereTriInt" onCallBack="Page.TrierInt" CommandName="fonction">
						<com:TTranslate>DEFINE_FONCTION_MAJ</com:TTranslate>
						<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/arrow-tri.gif" alt="<%=Prado::localize('ICONE_TRIER')%>" title="<%=Prado::localize('ICONE_TRIER')%>" />
	              	</com:TActiveLinkButton>
				  </th>
	              <com:TPanel visible="<%#(!$this->TemplateControl->getListeTypeVoix())? 'true' : 'false' %>">
		              <com:TPanel visible="<%#$this->TemplateControl->getTypeVoix()%>">
		              	<th id="typeVoixIntervenant" class="col-200"  >
		              		<com:TActiveLinkButton onCommand="Page.critereTriInt" onCallBack="Page.TrierInt" CommandName="typeVoix">
		              			<com:TTranslate>DEFINE_TYPE_DE_VOIX</com:TTranslate>
		              			<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/arrow-tri.gif" alt="<%=Prado::localize('ICONE_TRIER')%>" title="<%=Prado::localize('ICONE_TRIER')%>" />
		              		</com:TActiveLinkButton>
						</th>
				  	</com:TPanel>
		          </com:TPanel>
	              <com:TPanel visible="<%#$this->TemplateControl->getListeTypeVoix()%>">
	              	<th id="listeTypeVoix" class="col-200"  >
	              		<com:TActiveLinkButton onCommand="Page.critereTriInt" onCallBack="Page.TrierInt" CommandName="typeVoix">
	              			<com:TTranslate>DEFINE_TYPE_DE_VOIX</com:TTranslate>
	              			<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/arrow-tri.gif" alt="<%=Prado::localize('ICONE_TRIER')%>" title="<%=Prado::localize('ICONE_TRIER')%>" />
	              		</com:TActiveLinkButton>
					</th>
				  </com:TPanel>
	              <com:TPanel visible="<%#$this->TemplateControl->getActions()%>"><th id="actionIntervenant" class="actions"><com:TTranslate>TEXT_ACTION</com:TTranslate></th></com:TPanel>
	          </tr>

	      </thead>
	      </prop:HeaderTemplate>
      <tbody>
      <prop:ItemTemplate>
          <tr class="<%#(($this->ItemIndex%2==0)? '':'on')%>">
			<com:TPanel visible="<%#$this->TemplateControl->getCheckbox()%>">
			<td headers="checkIntervenant" class="checkbox">
				<com:TCheckBox id="select"/>
				<com:THiddenField id="selectedIdIntervenant" value="<%#$this->Data->getIdIntervenantExterne()%>"/>
			</td>
			</com:TPanel>
              <td class="col-250">
              	<%# $this->Data->getNom()%>&nbsp;
              	<%# $this->Data->getPrenom()%>&nbsp;<br/>
              	<%#$this->Data->getEmail()%><br/>
              </td>
              <td headers="fontionIntervenant" class="col-250">
              	<%# $this->Data->getOrganisation()%><br/>
                  <%# $this->Data->getFonction()%>
              </td>
              <com:TPanel visible="<%#(!$this->TemplateControl->getListeTypeVoix())? 'true' : 'false' %>">
	              <com:TPanel visible="<%#$this->TemplateControl->getTypeVoix()%>">
	              <td headers="typeVoixIntervenant" class="col-200">
	                  <%#$this->TemplateControl->getLibelleTypeVoix($this->Data->getIdRefValTypeVoixDefaut())%>
	              </td>
	          </com:TPanel>
              </com:TPanel>
              <com:TPanel visible="<%#$this->TemplateControl->getListeTypeVoix()%>">
	              <td headers="listeTypeVoix" class="col-200">
	                  <com:TDropDownList id="selectTypeVoixIntervenant" DataSource="<%#$this->TemplateControl->getListeAllTypeVoix()%>" SelectedValue="<%#$this->TemplateControl->getTypeVoixIntervenant($this->Data->getIdIntervenantExterne(),true)%>" />
	              </td>
              </com:TPanel>

              <com:TPanel visible="<%#$this->TemplateControl->getActions()%>">
              	  <td headers="actionIntervenant" class="actions">
              		  <com:TPanel visible="<%#$this->TemplateControl->getActionModifier()%>"><a href="?page=Commission.FormulaireIntervenantExterne&id=<%#$this->Data->getIdIntervenantExterne()%>"><img title="<%#Prado::localize('BOUTON_MODIFIER')%>" src="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-edit.gif" /></a></com:TPanel>
              		  <com:TPanel visible="<%#$this->TemplateControl->getActionSupprimer()%>" ><com:TLinkButton CommandName="delete" CommandParameter="<%#$this->Data->getIdIntervenantExterne()%>"><img title="<%#Prado::localize('DEFINE_SUPPRIMER')%>"  src="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-supprimer-big.gif" /></com:TLinkButton></com:TPanel>
             	  </td>
             </com:TPanel>
          </tr>
          </prop:ItemTemplate>
     </tbody>
     </com:TRepeater>
  </table>
  <!--Fin Tableau intervenant-->

  <div class="line-partitioner" >
	<div class="partitioner" >
		<div class="intitule"><strong><label for="allCons_nbResultsTop"><com:TTranslate>DEFINE_TEXT_AFFICHER</com:TTranslate></label></strong></div>
		<com:TDropDownList ID="nombreResultatAfficherBottom" AutoPostBack="true" onSelectedIndexChanged="changePagerLenght" Attributes.title='<%=Prado::localize("NOMBRE_DE_RESULTAT_PAR_PAGE")%>'>
  			<com:TListItem Text="10" Selected="10"/>
  			<com:TListItem Text="20" Value="20"/>
  			<com:TListItem Text="50" Value="50" />
  			<com:TListItem Text="100" Value="100" />
  			<com:TListItem Text="500" Value="500" />
  		</com:TDropDownList>
		<div class="intitule"><com:TTranslate>DEFINE_RESULTATS_PAGE</com:TTranslate></div>
		<label style="display:none;" for="allCons_pageNumberBottom">Aller &agrave; la page</label>

		<com:TPanel ID="panelBouttonGotoPageBottom" DefaultButton="DefaultButtonBottom" CssClass="float-left">
			<com:TTextBox ID="numPageBottom" Attributes.title='<%=Prado::localize("N_DE_LA_PAGE")%>' Text="1"/>
			<div class="nb-total ">/ <com:TLabel ID="nombrePageBottom"/></div>
			<com:TButton ID="DefaultButtonBottom"  OnClick="goToPage" Attributes.style="display:none"/>
		</com:TPanel>

		<div class="liens" >
			<com:TPager ID="PagerBottom"
           			ControlToPaginate="tableauIntervenant"
	           		FirstPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-premier.gif' alt='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>' title='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>' />"
		            LastPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-dernier.gif' alt='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>' title='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>' />"
	     	        Mode="NextPrev"
	          		NextPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-suivant.gif' alt='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>' title='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>' />"
		            PrevPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-precedent.gif' alt='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>' title='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>' />"
   	                OnPageIndexChanged="pageChanged"/>

		</div>
	</div>
</div>
<!--Fin partitionneur-->
</com:TPanel>