<com:TPanel cssclass="form-bloc margin-0" visible="<%=($this->getTypeReponse())?true:false%>">
	<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
	<div class="content">
		<div class="line">
			<div class="intitule-bloc bloc-190 bold"><label><com:TTranslate>DOCUMENTS_SIGNATURE_PROPRE</com:TTranslate></label> :</div>
			<com:TActivePanel  id="panelPieces" cssclass="content-bloc bloc-550">
				<com:TRepeater ID="listePiecesRepeater"   EnableViewState="true" >
					<prop:ItemTemplate>
						<com:TPanel id="fichierSignPropre" style="display:none">
							<com:TTextBox ID="fichierPropre" Attributes.title="<%#Prado::localize('DOCUMENTS_SIGNATURE_PROPRE')%>" CssClass="bloc-460 float-left" />
							<input type="button" classe="float-left"  name="javaStartDirectoryChooser" 	id="javaStartDirectoryChooser" 
								title="<%#Prado::localize('DEFINE_PARCOURIR')%>" 
								Value="<%#Prado::localize('DEFINE_PARCOURIR')%>"
								onClick="document.AppletFileChooser.fctTrt4('<%#$this->fichierPropre->ClientId%>');" />
								<com:TLabel id="ajouter" text="0" style="display:none"/>
						</com:TPanel>
					</prop:ItemTemplate>
				</com:TRepeater>
				<com:TLabel id="nbrePiecesAjouter" style="display:none"/>
                <com:TActiveLinkButton  
				   	id="ajout_doc"
				   	CssClass="ajout-el" 
				   	OnCallBack="traceOperationInscrit"
				   	Attributes.onclick="addPiecesPropre('<%=$this->ClientId%>')"
				   	Attributes.title="<%=Prado::localize('AJOUTER_DOCUMENT')%>"
				   >
				   <com:TTranslate>AJOUTER_DOCUMENT</com:TTranslate>
				  </com:TActiveLinkButton>
										   	
			</com:TActivePanel>
		</div>
		<div class="breaker"></div>
	</div>
	<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
</com:TPanel>
<div class="spacer-mini"></div>
<div class="form-bloc margin-0">
	<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
	<div class="content">
		<div class="line">
			<div class="intitule-bloc bloc-190 bold"><label for="<%=$this->autrePieces->ClientId%>"><%=Prado::localize(($this->getTypeReponse())? 'AUTRES_PIECES_DOSSIER_ZIP' : $this->getLibelleAutrePiece())%></label> :</div>
			<div class="content-bloc bloc-550">
				<com:TTextBox ID="autrePieces" Attributes.title="<%=Prado::localize((($this->getTypeReponse())? 'AUTRES_PIECES_DOSSIER_ZIP':$this->getLibelleAutrePiece()))%>" CssClass="bloc-460 float-left" />
				<input type="button" classe="float-left"  
					title="<%=Prado::localize('DEFINE_PARCOURIR')%>" 
					Value="<%=Prado::localize('DEFINE_PARCOURIR')%>"
					onClick="document.AppletFileChooser.fctTrt4('<%=$this->autrePieces->ClientId%>');" />
			</div>
		</div>
		<div class="breaker"></div>
	</div>
	<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
</div>

<!--<com:TJavascriptLogger />-->
