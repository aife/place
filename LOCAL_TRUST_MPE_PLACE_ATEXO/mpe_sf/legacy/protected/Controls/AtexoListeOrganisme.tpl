<!--Debut Bloc Gestion des Organismes-->
<com:TActivePanel id="panelRefresh" >
<div class="form-field">
	<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
	<div class="content">
		<!--<div class="file-link float-right margin-right-10"><a href="#"><img title="Export XLS" alt="Export XLS" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-xls.gif" /> Export XLS</a></div>-->
			<div class="file-link float-right margin-right-10">
                <com:TLinkButton OnClick="genererExcel"  cssClass="lienExcel blue" style="display:<%=$this->getNbrElementRepeater()? 'block':'none'%>">
					<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-xls.gif" alt="<%=Prado::localize('EXPORT_XLS')%>" title="<%=Prado::localize('EXPORT_XLS')%>" /> <com:TTranslate>EXPORT_XLS</com:TTranslate>
				</com:TLinkButton>
            </div>			
		<h3><com:TTranslate>DEFINE_LISTE_ORGANISMES</com:TTranslate></h3>
		<div class="table-bloc">
			<!--Debut partitionneur-->
				<div class="line-partitioner">
					<h2><com:TTranslate>DEFINE_NOMBRE_RESULTATS</com:TTranslate> : <com:TLabel Text="<%=$this->getNbrElementRepeater()%>"/></h2>
					<div class="partitioner">
						<div class="intitule"><strong><label for="allAnnonces_nbResultsTop"><com:TLabel id="labelAfficherTop"><com:TTranslate>DEFINE_AFFICHER</com:TTranslate></com:TLabel></label></strong></div> 
						<com:TDropDownList Attributes.title="<%=Prado::localize('NOMBRE_DE_RESULTAT_PAR_PAGE')%>" id="nbResultsTop" AutoPostBack="true" onSelectedIndexChanged="changePagerLenght" >
        					<com:TListItem Text="10" Selected="10"/>
        					<com:TListItem Text="20" Value="20"/>
                  			<com:TListItem Text="50" Value="50" />
                  			<com:TListItem Text="100" Value="100" />
                  			<com:TListItem Text="500" Value="500" />
        				</com:TDropDownList> 
						<div class="intitule"><com:TLabel id="resultParPageTop"><com:TTranslate>DEFINE_RESULTATS_PAGE</com:TTranslate></com:TLabel></div> 
						<label style="display:none;" for="allAnnonces_pageNumberTop"><com:TTranslate>DEFINE_ALLER_PAGE</com:TTranslate></label>
						<com:TPanel ID="panelBouttonGotoPageTop" DefaultButton="DefaultButtonTop" CssClass="float-left">
							<com:TTextBox Attributes.title="<%=Prado::localize('N_DE_LA_PAGE')%>" Text="1" id="pageNumberTop" />
							<div class="nb-total ">
								<com:TLabel ID="labelSlashTop" Text="/" visible="true"/> 
								<com:TLabel ID="nombrePageTop"/>
								<com:TButton ID="DefaultButtonTop"  OnClick="goToPage" Attributes.style="display:none"/>
							</div>
						</com:TPanel>
						<div class="liens">
							<com:TPager
        						ID="PagerTop"
            					ControlToPaginate="RepeaterOrganisme"
            					FirstPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-premier.gif' alt='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>' title='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>' />"
            					PrevPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-precedent.gif' alt='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>' title='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>' />"
            					Mode="NextPrev"
            					NextPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-suivant.gif' alt='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>' title='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>' />"
            					LastPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-dernier.gif' alt='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>' title='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>' />"
            					OnPageIndexChanged="pageChanged"
            				/>
						</div>
					</div>
				</div>
			<!--Fin partitionneur-->
			<com:TRepeater ID="RepeaterOrganisme" 
						 DataKeyField="Id" 
						 EnableViewState="true" 
						 AllowPaging="true"
			    	     PageSize="<%= Application\Service\Atexo\Atexo_Config::getParameter('NOMBRE_AFFICHAGE_PAR_DEFAUT')%>"
			    	     AllowCustomPaging="true"
					 >
				<prop:HeaderTemplate>
					<table class="table-results" summary="Liste des agents">
						<caption><com:TTranslate>DEFINE_LISTE_ORGANISMES</com:TTranslate></caption>
						<thead>
							<tr>
								<th class="top" colspan="6"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></th>
							</tr>
							<tr>
								<th class="col-150" id="denomination">
									<com:TActiveLinkButton 
									cssClass="on"
									OnCallback="SourceTemplateControl.sortOrganisme" 
									ActiveControl.CallbackParameter="DenominationOrg">
										<com:TTranslate>DENOMINATION</com:TTranslate>
										<com:TActiveImageButton 
											ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/arrow-tri-off.gif"
											OnCallback="SourceTemplateControl.sortOrganisme" 
											ActiveControl.CallbackParameter="DenominationOrg" 
											AlternateText="<%=Prado::localize('DEFINE_TRIER')%>" 
											Attributes.title="<%=Prado::localize('DEFINE_TRIER')%>" />
									</com:TActiveLinkButton>
								</th>
								<th class="col-50" id="OrganismeAcronyme">
									<com:TActiveLinkButton 
									OnCallback="SourceTemplateControl.sortOrganisme" 
									ActiveControl.CallbackParameter="Acronyme">
										<com:TTranslate>ACRONYME</com:TTranslate>
										<com:TActiveImageButton 
											ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/arrow-tri-off.gif"
											OnCallback="SourceTemplateControl.sortOrganisme" 
											ActiveControl.CallbackParameter="Acronyme" 
											AlternateText="<%=Prado::localize('DEFINE_TRIER')%>" 
											Attributes.title="<%=Prado::localize('DEFINE_TRIER')%>" />
									</com:TActiveLinkButton>
								</th>
								<th class="col-50" id="OrganismeSigle"><com:TTranslate>DEFINE_TEXT_SIGLE_EA_PMI</com:TTranslate></th>
								<th class="col-150" id="servciesAccessibles"><com:TTranslate>TEXT_SERVICES_ACCESSIBLES</com:TTranslate></th>
								<th class="col-70 center" id="OrganismeLogo" style="display:<%=Application\Service\Atexo\Atexo_Module::isEnabled('AfficherImageOrganisme')?'':'none'%>" ><com:TTranslate>TEXT_LOGO</com:TTranslate></th>
								<th class="actions" id="actions"><com:TTranslate>DEFINE_ACTIONS</com:TTranslate></th>
							</tr>
						</thead>
				</prop:HeaderTemplate>
				<prop:ItemTemplate>
					<tr class="<%#(($this->ItemIndex%2==0)? '':'on')%>">
						<td class="col-150" headers="denomination"><%#$this->Data->getDenominationOrgTraduit()%></td>
						<td class="col-50" headers="OrganismeAcronyme"><%#$this->Data->getAcronyme()%></td>
						<td class="col-50" headers="OrganismeSigle"><%#$this->Data->getSigle()%></td>
						<td class="col-150" headers="servciesAccessibles"><%#$this->Data->getServiceMetierActive()%></td>
						<td class="col-70 center" headers="OrganismeLogo"  style="display:<%=Application\Service\Atexo\Atexo_Module::isEnabled('AfficherImageOrganisme')?'':'none'%>">
							<com:TImage ID="logoOrganisme" 
							 ImageUrl="<%#$this->TemplateControl->getUrlLogo($this->Data->getAcronyme())%>"
							 CssClass="logo-organisme"
							 Attributes.title="<%#$this->Data->getDenominationOrg()%>"
							 Attributes.alt="<%#$this->Data->getDenominationOrg()%>"
						 	/>	
						</td>
						<td class="actions" headers="actions">
							<com:THyperLink ID="linkModifier" NavigateUrl="index.php?page=Agent.GestionOrganisme&org=<%#$this->Data->getAcronyme()%>&listeOrg=1">
    							<com:TImage ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-edit.gif" Attributes.alt="<%=Prado::localize('MODIFIER')%>" Attributes.title="<%=Prado::localize('MODIFIER')%>"/>
    						</com:THyperLink>
						</td>
					</tr>
					</prop:ItemTemplate>
					<prop:FooterTemplate>
						</table>					
					</prop:FooterTemplate>								 
				</com:TRepeater>
			<!--Debut partitionneur-->
			<div class="line-partitioner">
				<div class="partitioner">
					<div class="intitule"><strong><label for="horsLigne_nbResultsBottom"><com:TLabel id="labelAfficherDown"><com:TTranslate>DEFINE_AFFICHER</com:TTranslate></com:TLabel></label></strong></div> 
						<com:TDropDownList Attributes.title="<%=Prado::localize('NOMBRE_DE_RESULTAT_PAR_PAGE')%>" id="nbResultsBottom" AutoPostBack="true" onSelectedIndexChanged="changePagerLenght" >
       					<com:TListItem Text="10" Selected="10"/>
                 			<com:TListItem Text="20" Value="20"/>
                 			<com:TListItem Text="50" Value="50" />
                 			<com:TListItem Text="100" Value="100" />
                 			<com:TListItem Text="500" Value="500" />
       				</com:TDropDownList> 
					<div class="intitule"><com:TLabel id="resultParPageDown"><com:TTranslate>DEFINE_RESULTATS_PAGE</com:TTranslate></com:TLabel></div> 
					<label style="display:none;" for="horsLigne_pageNumberBottom"><com:TTranslate>DEFINE_ALLER_PAGE</com:TTranslate></label>
					<com:TPanel ID="panelBouttonGotoPageBottom" DefaultButton="DefaultButtonBottom" CssClass="float-left">
						<com:TTextBox Attributes.title="<%=Prado::localize('N_DE_LA_PAGE')%>" Text="1" id="pageNumberBottom" />
						<div class="nb-total ">
							<com:TLabel ID="labelSlashBottom" Text="/" visible="true"/> 
							<com:TLabel ID="nombrePageBottom"/>
							<com:TButton ID="DefaultButtonBottom"  OnClick="goToPage" Attributes.style="display:none"/>
						</div>
					</com:TPanel>
					<div class="liens">
						<com:TPager
       						ID="PagerBottom"
           					ControlToPaginate="RepeaterOrganisme"
           					FirstPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-premier.gif' alt='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>' title='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>' />"
           					PrevPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-precedent.gif' alt='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>' title='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>' />"
           					Mode="NextPrev"
           					NextPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-suivant.gif' alt='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>' title='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>' />"
           					LastPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-dernier.gif' alt='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>' title='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>' />"
           					OnPageIndexChanged="pageChanged"
           				/>
					</div>
				</div>
			</div>
            <!--Fin partitionneur-->
		</div>
		<div class="breaker"></div>
	</div>
	<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
</div>
</com:TActivePanel>

<com:TPanel id="panelExcelAllOrganismes" CssClass="form-field" >
	<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
	<div class="content">
		<div class="file-link float-left">
			<com:TLinkButton OnClick="genererExcelAllOrganismes" >
				<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-xls.gif" alt="<%=Prado::localize("TEXT_XLS_LISTE_ENTITES_PUBLIQUES")%>" title="<%=Prado::localize("TEXT_XLS_LISTE_ENTITES_PUBLIQUES")%>" /><com:TTranslate>TEXT_XLS_LISTE_ENTITES_PUBLIQUES</com:TTranslate>
			</com:TLinkButton>
		</div>
	</div>
	<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
</com:TPanel>
<!--Fin Bloc Gestion des Organismes-->