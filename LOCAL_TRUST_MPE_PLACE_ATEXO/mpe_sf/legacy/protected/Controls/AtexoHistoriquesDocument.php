<?php

namespace Application\Controls;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonTDocumentEntreprise;
use Application\Propel\Mpe\CommonTDocumentType;
use Application\Propel\Mpe\CommonTDocumentTypeQuery;
use Application\Propel\Mpe\CommonTEntrepriseDocumentVersion;
use Application\Propel\Mpe\CommonTEntrepriseDocumentVersionQuery;
use Application\Propel\Mpe\CommonTEtablissement;
use Application\Propel\Mpe\Entreprise;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_Entreprise;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Attestation;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Etablissement;
use Application\Service\Atexo\Entreprise\InterfaceSynchroApiGouvEntreprise;
use Exception;
use Prado\Prado;

/**
 * Page de gestion du bloc "Historique" d'un document d'une entreprise.
 *
 * @author loubna EZZIANI <loubna.ezziani@atexo.com>
 *
 * @version 1.0
 *
 * @since 4.10.0
 *
 * @copyright Atexo 2015
 */
class AtexoHistoriquesDocument extends MpeTTemplateControl
{
    protected $consultationId;
    protected $organisme;
    protected bool $responsive = false;
    protected $identifiant;

    /**
     * @return bool
     */
    public function isResponsive()
    {
        return $this->responsive;
    }

    /**
     * @param bool $responsive
     */
    public function setResponsive($responsive)
    {
        $this->responsive = $responsive;
    }

    /**
     * Get the [_idTypeDocument] column value.
     *
     * @return int
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.10.0
     *
     * @copyright Atexo 2015
     */
    public function getIdTypeDocument()
    {
        return $this->getViewState('idTypeDocument');
    }

    /**
     * Set the value of [_idTypeDocument] column.
     *
     * @param int $v new value
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.10.0
     *
     * @copyright Atexo 2015
     */
    public function setIdTypeDocument($v)
    {
        $this->setViewState('idTypeDocument', $v);
    }

    // setIdTypeDocument()

    /**
     * Get the [_idEntreprise] column value.
     *
     * @return int
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.10.0
     *
     * @copyright Atexo 2015
     */
    public function getIdEntreprise()
    {
        return $this->getViewState('idEntreprise');
    }

    /**
     * Set the value of [_idEntreprise] column.
     *
     * @param int $v new value
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.10.0
     *
     * @copyright Atexo 2015
     */
    public function setIdEntreprise($v)
    {
        $this->setViewState('idEntreprise', $v);
    }

    // setIdEntreprise()

    /**
     * Get the [typeDocActif] column value.
     *
     * @return int
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1.0
     *
     * @since 2017-develop
     *
     * @copyright Atexo 2017
     */
    public function getSynchroActif()
    {
        return $this->getViewState('synchroActif');
    }

    /**
     * Set the value of [typeDocActif] column.
     *
     * @return void
     *
     * @author Amal EL BEKKAOUI <amal@atexo.com>
     *
     * @version 1.0
     *
     * @since 2017-develop
     *
     * @copyright Atexo 2017
     */
    public function setSynchroActif($v)
    {
        $this->setViewState('synchroActif', $v);
    }

    // setIdEntreprise()

    /**
     * @return mixed
     */
    public function getRefConsultation()
    {
        $consultationId = $this->getViewState('referenceConsultation');
        if (empty($consultationId)) {
            $consultationId = $this->refConsultation;
        }

        return $consultationId;
    }

    /**
     * @param mixed $consultationId
     */
    public function setRefConsultation($consultationId)
    {
        $this->refConsultation = $consultationId;
        $this->setViewState('referenceConsultation', $this->refConsultation);
    }

    /**
     * @return mixed
     */
    public function getOrganisme()
    {
        $organisme = $this->getViewState('organisme');
        if (empty($organisme)) {
            $organisme = $this->organisme;
        }

        return $organisme;
    }

    /**
     * @param mixed $organisme
     */
    public function setOrganisme($organisme)
    {
        $this->organisme = $organisme;
        $this->setViewState('organisme', $this->organisme);
    }

    /**
     * @return mixed
     */
    public function getIdentifiant()
    {
        $identifiant = $this->getViewState('identifiant');
        if (empty($identifiant)) {
            $identifiant = $this->identifiant;
        }

        return $identifiant;
    }

    /**
     * @param mixed $identifiant
     */
    public function setIdentifiant($identifiant)
    {
        $this->setViewState('identifiant', $identifiant);
        $this->identifiant = $this->getViewState('identifiant');
    }

    /**
     * permet d'initialiser le composant.
     *
     * @param array $dataSource un tableau d'objet CommonTEntrepriseDocumentVersion
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.10.0
     *
     * @copyright Atexo 2015
     */
    public function initComposant($dataSource)
    {
        $this->setViewState('listeHistoriques', $dataSource);
        $this->fillRepeaterHistorique();
        $typeDoc = $this->getIdTypeDocument();
        $this->titrePopin->Text = (new Atexo_Entreprise_Attestation())->getTitreAttestation($typeDoc);
        if (is_array($dataSource) && count($dataSource)) {
            $this->panelInfoVersion->setVisible(false);
        } else {
            $this->msgInfo->Text = Prado::Localize('AUCUNE_VERSION_DISPONIBLE');
            $this->panelInfoVersion->setVisible(true);
        }
        $this->linkDemanderNouvelleVersion->setVisible(boolval($this->getSynchroActif()));
    }

    /**
     * permet de telecharger une version donnee.
     *
     * @param mixed $sender
     * @param mixed $param
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.10.0
     *
     * @copyright Atexo 2015
     */
    public function downloadVersion($sender, $param)
    {
        $attr = explode('#', $param->CommandParameter);
        $docVersion = CommonTEntrepriseDocumentVersionQuery::create()->filterByIdVersionDocument($attr[0])->findOne();
        if ($docVersion instanceof CommonTEntrepriseDocumentVersion) {
            $document = $docVersion->getCommonTDocumentEntreprise();
            if ($document instanceof CommonTDocumentEntreprise) {
                if (!$docVersion->getIdBlob()) {
                    (new Atexo_Entreprise_Attestation())->genererAttestationFromJson($docVersion, $document);
                }
                if ($docVersion->getIdBlob()) {
                    $nomFichier = $fileName = $document->getNomDocument().'_'.date('Y_m_d_H_i_s').'.'.$docVersion->getExtensionDocument();
                    $this->response->redirect('?page=Agent.FileDownLoad&idFichier='.$docVersion->getIdBlob().'&org='.Atexo_Config::getParameter('COMMON_BLOB').'&nomFichier='.$fileName);
                }
            }
        }
    }

    /**
     * permet de de remplir le repeater d'historique des attestations.
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.10.0
     *
     * @copyright Atexo 2015
     */
    public function fillRepeaterHistorique()
    {
        $listeHistoriques = $this->getViewState('listeHistoriques');
        $this->repeaterHistoriques->DataSource = $listeHistoriques;
        $this->repeaterHistoriques->DataBind();
    }

    /**
     * permet de d'ajouter une nouvelle version.
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.10.0
     *
     * @copyright Atexo 2015
     */
    public function addNewVersion()
    {
        $idObjet = null;
        $version = null;
        try {
            $loggerWs = Atexo_LoggerManager::getLogger();
            $this->reInitialiserMessage();
            $typeDoc = $this->getIdTypeDocument();
            $identifiant = $this->getIdentifiant();
            $this->linkDemanderNouvelleVersion->setVisible(true);
            $loggerWs->info("Debut ajout d'une nouvelle version de document - identifiantDoc = $identifiant");

            if (false === $this->verifierDateDernierAppelValideWs($typeDoc, $identifiant)) {
                $loggerWs->info("Fin ajout d'une nouvelle version de document  - identifiantDoc = $identifiant".PHP_EOL.PHP_EOL);

                return;
            }
            $idEntreprise = $this->getIdEntreprise();

            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            $typeDocument = CommonTDocumentTypeQuery::create()->filterByIdTypeDocument($typeDoc)->findOne($connexion);
            if ($typeDocument instanceof CommonTDocumentType) {
                if ($typeDocument->getTypeDocEntrepriseEtablissement() == Atexo_Config::getParameter('TYPE_DOCUMENT_SGMAP_ENTREPRISE')) {
                    $idObjet = $idEntreprise;
                } elseif ($typeDocument->getTypeDocEntrepriseEtablissement() == Atexo_Config::getParameter('TYPE_DOCUMENT_SGMAP_ETABLISSEMENT')) {
                    $etablissement = (new Atexo_Entreprise_Etablissement())->getEtablissementBySiret($identifiant);
                    if ($etablissement instanceof CommonTEtablissement) {
                        $idObjet = $etablissement->getIdEtablissement();
                    }
                }
            }
            $loggerWs = Atexo_LoggerManager::getLogger();

            $host = Atexo_Config::getParameter('URL_API_GOUV_ENTREPRISE');

            $objet = $this->getReferenceUtilisateurConsultation();
            if (empty($objet)) {
                $objet = $this->getIdentifiantEntreprise();
            }

            $resultat = (new Atexo_Entreprise_Attestation())->synchroAttestation($idObjet, $typeDoc, false, $objet, $host, $version);
            if (is_array($resultat)) {
                if ('OK' == $resultat['statut']) {
                    $newVersion = $resultat['resultat'];
                    if ($newVersion instanceof CommonTEntrepriseDocumentVersion) {
                        $listeHistoriques = $this->getViewState('listeHistoriques');
                        $listeHistoriques[] = $newVersion;
                        $this->setViewState('listeHistoriques', $listeHistoriques);
                        self::fillRepeaterHistorique();
                        $this->panelInfoVersion->setVisible(false);
                    }
                } elseif ('NOK' == $resultat['statut']) {
                    $this->panelInfoVersion->setVisible(true);
                    $this->linkDemanderNouvelleVersion->setVisible(false);
                    $this->msgInfo->Text = $resultat['resultat'];
                }
            }
            $loggerWs->info("Fin ajout d'une nouvelle version de document - identifiantDoc = $identifiant".PHP_EOL.PHP_EOL);
        } catch (Exception $e) {
            $loggerWs->error("Erreur lors de l'ajout d'une nouvelle version : id_entreprise = $idEntreprise , type_doc = $typeDoc \nErreur = ".$e->getMessage()."\n\nTrace = ".$e->getTraceAsString());
        }
    }

    /**
     * permet de de recuperer la liste des attestation a aficher (datasource).
     *
     * @param mixed $sender
     * @param mixed $param
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.10.0
     *
     * @copyright Atexo 2015
     */
    public function sortDataSource($sender, $param)
    {
        $this->setOdreTri();
        $listeHistoriques = $this->getViewState('listeHistoriques');
        $listeHistoriquesTriees = Atexo_Util::sortArrayOfObjects($listeHistoriques, $param->CallbackParameter, $this->getViewState('ordreTri'));
        $this->setViewState('listeHistoriques', $listeHistoriquesTriees);
        self::fillRepeaterHistorique();
    }

    /**
     * permet d enregistrer l'ordre du tri.
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.10.0
     *
     * @copyright Atexo 2015
     */
    private function setOdreTri()
    {
        if ('DESC' == $this->getViewState('ordreTri')) {
            $this->setViewState('ordreTri', 'ASC');
        } else {
            $this->setViewState('ordreTri', 'DESC');
        }
    }

    /**
     * Permet de verifier la date de dernier appel valide du WS.
     *
     * @param string $idTypeDoc   : identifiant du type de document
     * @param string $identifiant : siren ou siret
     *
     * @return bool
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function verifierDateDernierAppelValideWs($idTypeDoc, $identifiant)
    {
        $logger = Atexo_LoggerManager::getLogger();
        $logger->info('Debut verification date de dernier appel valide du WS');
        try {
            $dataDernierAppelValideWs = (new Atexo_Entreprise_Attestation())->recupererDateDernierAppelValideWs($idTypeDoc, $identifiant);
            $logger->info("Date de dernier appel valide du WS : $dataDernierAppelValideWs");
            if (!empty($dataDernierAppelValideWs)
                    && Atexo_Util::validateDate($dataDernierAppelValideWs)
                        && Atexo_Util::comparerDeuxDatesFormatIso(date('Y-m-d H:i:s'), date('Y-m-d H:i:s', strtotime(Atexo_Config::getParameter('DELAI_TO_ENABLE_SYNCHRONISE_WITH_API_ATTESTATION'), strtotime($dataDernierAppelValideWs)))) < 0) {
                $logger->info('Le dernier appel valide du WS date de moins de '.Atexo_Config::getParameter('DELAI_TO_ENABLE_SYNCHRONISE_WITH_API_ATTESTATION'));
                $this->afficherMessage(Atexo_Util::remplacerDatesAnglaisParFrancaisDansChaine(str_replace('[__DELAI__]', str_replace('+', '', Atexo_Config::getParameter('DELAI_TO_ENABLE_SYNCHRONISE_WITH_API_ATTESTATION')), Prado::localize('MESSAGE_ERREUR_VERIFICATION_DATE_DERNIER_APPEL_WS_ATTESTATIONS'))));

                return false;
            }
            $logger->info('Le dernier appel valide du WS date de plus de '.Atexo_Config::getParameter('DELAI_TO_ENABLE_SYNCHRONISE_WITH_API_ATTESTATION'));

            return true;
        } catch (Exception $e) {
            $logger->error('Erreur verification date dernier appel valide WS : '.$e->getMessage());
        }
        $logger->info('Fin verification date de dernier appel valide du WS');
    }

    /**
     * Permet d'afficher les messages d'informations et d'erreurs.
     *
     * @param string $message
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function afficherMessage($message)
    {
        $this->msgInfo->Text = $message;
        $this->panelInfoVersion->setVisible(true);
    }

    /**
     * Permet de re-initialiser le bloc de message d'informations.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since esr-2015
     *
     * @copyright Atexo 2015
     */
    public function reInitialiserMessage()
    {
        $this->msgInfo->Text = '';
        $this->panelInfoVersion->setVisible(false);
    }

    /**
     * Permet de recuperer le siren d'une entreprise.
     *
     * @param string $typeDoc
     *
     * @return string|null
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2016-develop
     *
     * @copyright Atexo 2016
     */
    public function getIdentifiantEntreprise()
    {
        $entreprise = (new Atexo_Entreprise())->retrieveCompanyBdeById($this->getIdEntreprise(), null, false);
        if ($entreprise instanceof Entreprise) {
            return $entreprise->getSiren();
        }

        return null;
    }

    /**
     * Permet de recuperer la reference utilisateur de la consultation.
     */
    public function getReferenceUtilisateurConsultation()
    {
        if (!empty($this->getRefConsultation()) && !empty($this->getOrganisme())) {
            $consultation = (new Atexo_Consultation())->retrieveConsultation($this->getRefConsultation(), $this->getOrganisme());
            if ($consultation instanceof CommonConsultation) {
                return $consultation->getReferenceUtilisateur();
            }
        }

        return null;
    }
}
