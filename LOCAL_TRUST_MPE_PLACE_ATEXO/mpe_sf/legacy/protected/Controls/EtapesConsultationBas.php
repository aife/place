<?php

namespace Application\Controls;

use Application\Propel\Mpe\CommonConsultation;
use Application\Service\Atexo\Atexo_Module;

/**
 * Page de gestion des étapes de la consultation.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 4
 */
class EtapesConsultationBas extends EtapesConsultation
{
    /**
     * Chargement du composant.
     */
    public function onLoad($param)
    {
        if (!$this->Page->isPostBack) {
            $this->updatPourcentagePub();
        }
        if ($this->Page->getConsultation() instanceof CommonConsultation) {
            $this->displayEtapeLots($this->Page->getConsultation()->getAlloti());
            $this->displayEtapeDume($this->Page->getConsultation()->getDumeDemande());
            if (Atexo_Module::isEnabled('Publicite')) {
                $this->displayEtapeDonneesComplementaires($this->Page->getConsultation());
                $this->displayEtapePublicite($this->Page->getConsultation());
            }
        }
    }
}
