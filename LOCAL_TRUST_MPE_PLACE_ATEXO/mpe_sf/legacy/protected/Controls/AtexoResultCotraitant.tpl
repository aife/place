<!--BEGIN FILTRES-->
<div class="form-bloc bloc-filtres form-horizontal">
    <div class="content">
        <div class="panel panel-default">
            <div class="panel-heading collapsed" id="filtresHeadingCollapse" role="button" data-toggle="collapse" href="#filtresHeading" aria-expanded="true" aria-controls="filtresHeadingLabel">
                <div class="h4 panel-title">Filtres</div>
            </div>
            <div id="filtresHeading"  class="panel-collapse collapsed in" role="tabpanel" aria-labelledby="filtresHeadingLabel">
                <div class="panel-body">
                    <div class="col-md-6">
                        <div class="form-group form-group-sm">
                            <label class="control-label col-md-3"
                                   for="ctl0_CONTENU_PAGE_AtexoResultCotraitant_filtreMotsCles">
                                <com:TTranslate>DEFINE_MOTS_CLES</com:TTranslate>
                                :
                            </label>
                            <div class="col-md-9">
                                <com:TActiveTextBox id="filtreMotsCles" CssClass="form-control"/>
                            </div>
                        </div>
                        <div class="form-group form-group-sm ">
                            <label class="control-label col-md-3" for="ctl0_CONTENU_PAGE_AtexoResultCotraitant_localite">
                                <com:TTranslate>DEFINE_LOCALITE</com:TTranslate>
                                :
                            </label>
                            <div class="col-md-9">
                                <com:TActiveTextBox id="localite" cssClass="form-control"
                                                    Attributes.onchange="lancerFiltre();"
                                                    Attributes.onkeyup="this.onchange();"
                                                    Attributes.onpaste="this.onchange();"
                                                    Attributes.oninput="this.onchange();"/>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group form-group-sm" id="filtreTypeGroupement">
                            <label class="col-md-4 control-label" for="filtreTypeGroupement">
                                <com:TTranslate>DEFINE_TYPE_GROUPEMENT</com:TTranslate>
                            </label>
                            <div class="col-md-8">
                                <div class="checkbox">
                                    <label id="type_groupement_mandataire" for="<%=$this->mandataire->ClientId%>">
                                        <com:TActiveCheckBox id="mandataire" checked="checked"
                                                             Attributes.onchange="lancerFiltre();"/>
                                        <com:TTranslate>DEFINE_MANDATAIRE_GROUPEMENT</com:TTranslate>
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label id="type_groupement_cotraitant_conjoint"
                                           for="<%=$this->conjoint->ClientId%>">
                                        <com:TActiveCheckBox id="conjoint" checked="checked"
                                                             Attributes.onchange="lancerFiltre();"/>
                                        <com:TTranslate>DEFINE_COTRAITANT_CONJOINT</com:TTranslate>
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label id="type_groupement_cotraitant_solitaire"
                                           for="<%=$this->solidaire->ClientId%>">
                                        <com:TActiveCheckBox id="solidaire" checked="checked"
                                                             Attributes.onchange="lancerFiltre();"/>
                                        <com:TTranslate>DEFINE_COTRAITANT_SOLIDAIRE</com:TTranslate>
                                    </label>
                                </div>
                                <div class="checkbox">
                                    <label id="type_groupement_sous_traiatant"
                                           for="<%=$this->sousTraitant->ClientId%>">
                                        <com:TActiveCheckBox id="sousTraitant" checked="checked"
                                                             Attributes.onchange="lancerFiltre();"/>
                                        <com:TTranslate>TEXT_SOUS_TRAITANT</com:TTranslate>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="boutons-line">
            <com:TActiveButton
                    id="filtrer"
                    OnCallback="searchWithFiltre"
                    style = "display:none;"
            >
                <prop:ClientSide.OnLoading>showLoader();</prop:ClientSide.OnLoading>
                <prop:ClientSide.OnFailure>showLoader();</prop:ClientSide.OnFailure>
                <prop:ClientSide.OnSuccess>showLoader();</prop:ClientSide.OnSuccess>
                <prop:ClientSide.RequestTimeOut>60000</prop:ClientSide.RequestTimeOut>
            </com:TActiveButton>
        </div>
    </div>
</div>
<!--END FILTRES-->

<!--BEGIN RESULTATS-->
<div class="bloc-map">
    <div class="form-field">
        <div class="panel panel-primary">
            <div class="panel-heading" id="panelElementsFound">
                <h2 class="h4 panel-title">
                    <com:TTranslate>CODE_LISTE_ENTREPRISES_GROUPEMENT</com:TTranslate>
                </h2>
            </div>
            <div class="panel-body">
                <span class="small text-warning text-bold"><com:TTranslate>NB_ENTREPRISES_RECHERCHE_GROUPEMENT</com:TTranslate></span>

                <!-- BEGIN PAGINATION TOP-->
                <com:TActivePanel id="pagerTop" cssClass="line-partitioner">
                    <div class="clearfix m-t-4">
                        <div class="pull-left">
                            <h2 class="h5">
                                <strong>
                                    <com:TTranslate>DEFINE_NOMBRE_RESULTAT</com:TTranslate>
                                    <com:TActiveLabel ID="nombreElement"/>
                                </strong>
                            </h2>
                        </div>
                        <div class="form-inline form-group-sm pull-right">
                            <div class="form-group">
                                <label class="m-r-1" for="nombreResultatAfficherTop">
                                    <com:TTranslate>DEFINE_TEXT_AFFICHER</com:TTranslate>
                                </label>
                                <com:TDropDownList ID="nombreResultatAfficherTop" AutoPostBack="true"
                                                   onSelectedIndexChanged="changePageSize"
                                                   Attributes.title='<%=Prado::localize("NOMBRE_DE_RESULTAT_PAR_PAGE")%>'
                                                   cssClass="form-control">
                                    <com:TListItem Text="10" Selected="10"/>
                                    <com:TListItem Text="20" Value="20"/>
                                    <com:TListItem Text="50" Value="50" />
                                    <com:TListItem Text="100" Value="100" />
                                    <com:TListItem Text="500" Value="500" />
                                </com:TDropDownList>
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="numPageTop">
                                    <com:TTranslate>ALLER_A_LA_PAGE</com:TTranslate>
                                </label>
                                <com:TPanel ID="panelBouttonGotoPageTop" DefaultButton="DefaultButtonTop"
                                            CssClass="float-left">
                                    <label class="m-l-1 m-r-1">
                                        <com:TTranslate>DEFINE_RESULTATS_PAGE</com:TTranslate>
                                    </label>
                                    <com:TTextBox ID="numPageTop"
                                                  Attributes.title='<%=Prado::localize("N_DE_LA_PAGE")%>' Text="1"
                                                  CssClass="form-control w-50 text-center"/>
                                    <label class="nb-total ">/
                                        <com:TLabel ID="nombrePageTop"/>
                                    </label>
                                    <com:TButton ID="DefaultButtonTop" OnClick="goToPage"
                                                 Attributes.style="display:none"/>
                                </com:TPanel>
                            </div>
                            <div class="form-group liens">
                                <label class="m-l-1">
                                    <com:TPager ID="PagerTop"
                                                ControlToPaginate="repeaterCotraitants"
                                                FirstPageText="<div class='btn' data-toggle='tooltip' data-placement='top' title='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>'><i class='fa fa-angle-double-left'></i></div>"
                                                LastPageText="<div class='btn' data-toggle='tooltip' data-placement='top' title='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>'><i class='fa fa-angle-double-right'></i></div>"
                                                Mode="NextPrev"
                                                NextPageText="<div class='btn' data-toggle='tooltip' data-placement='top' title='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>'><i class='fa fa-angle-right'></i></div>"
                                                PrevPageText="<div class='btn' data-toggle='tooltip' data-placement='top' title='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>'><i class='fa fa-angle-left '></i></div>"
                                                OnPageIndexChanged="pagerGoToPage"
                                    />
                                </label>
                            </div>
                        </div>
                    </div>
                </com:TActivePanel>
                <!-- END PAGINATION TOP-->

                <div class="row">
                    <!--BEGIN RESULT SEARCH-->
                    <div class="col-md-12">
                        <!--BEGIN MESSAGE AUCUN RESULTAT-->
                        <com:TActivePanel  id="panelMessage" style="display:none">
                            <div class="alert alert-warning m-b-0 m-t-2">
                                <div class="h4 text-center m-0">
                                    <%=Prado::Localize('DEFINE_AUCUN_RESULTAT')%>
                                </div>
                            </div>
                        </com:TActivePanel>
                        <!--END MESSAGE AUCUN RESULTAT-->
                    </div>
                    <div class="col-md-8">
                        <div class="liste-resultats liste-cotraitance m-t-2">
                            <ul class="list-unstyled">
                                <com:TRepeater
                                        ID="repeaterCotraitants"
                                        AllowPaging="true"
                                        PageSize="10"
                                        EnableViewState="true"
                                        AllowCustomPaging="true"
                                        OnItemCommand="commandConsultation"
                                >
                                    <prop:ItemTemplate>
                                        <li id="entreprise_<%#$this->ItemIndex+1%>" data-entreprise-nom="<%#$this->Data->getNomEntreprise()%>" data-lat="<%#$this->Data->getLong()%>" data-long="<%#$this->Data->getLat()%>">
                                            <div class="resultat-cotraitance clearfix">
                                                <div class="coordonnees list-group-item clearfix">
                                                    <div class="col-md-6">
                                                        <a href='javascript:popUp('?page=Entreprise.DetailEntreprise&callFrom=entreprise&idEntreprise=<%#$this->Data->getIdEntreprise()%>&id=<%#$_GET["id"]%>','yes');"
                                                           data-toggle="tooltip"
                                                           title="D&eacute;tail">
                                                    <span class="h4">
                                                        <i class="fa fa-map-marker text-danger"></i>
                                                        <%#$this->Data->getNomEntreprise()%>
                                                    </span>
                                                        </a>
                                                        <div class="m-l-2">
                                                            <div>
                                                    <span class="small">
                                                         <%# Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($this->Data->getNomInscrit())%>&nbsp;<%# Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($this->Data->getPrenomInscrit())%>
                                                    </span>
                                                            </div>
                                                            <div>
                                                    <span class="small">
                                                        <%#$this->Data->getFonctionInscrit()%>
                                                    </span>
                                                            </div>
                                                            <div>
                                                    <span class="small">
                                                        <%#$this->Data->getEmailInscrit()%>
                                                    </span>
                                                            </div>
                                                            <div>
                                                    <span class="small">
                                                        <%#$this->Data->getCpInscrit()%> - <%#$this->Data->getVilleInscrit()%>
                                                    </span>
                                                            </div>
                                                        </div>
                                                        <div class="collaboration m-l-2">
                                                            <div style="display:<%#$this->Data->getMandataireGroupement() == '1' ? 'block' : 'none'%>"
                                                                 class="statut-collaborateur">
                                                                <strong class="small text-danger">
                                                                    <com:TTranslate>TEXT_MANDATAIRE</com:TTranslate>
                                                                </strong>
                                                            </div>
                                                            <div style="display:<%#$this->Data->getCotraitantSolidaire() == '1' ? 'block' : 'none'%>"
                                                                 class="statut-collaborateur">
                                                                <strong class="small text-danger">
                                                                    <com:TTranslate>DEFINE_COTRAITANT_SOLIDAIRE</com:TTranslate>
                                                                </strong>
                                                            </div>
                                                            <div style="display:<%#$this->Data->getCotraitantConjoint() == '1' ? 'block' : 'none'%>"
                                                                 class="statut-collaborateur">
                                                                <strong class="small text-danger">
                                                                    <com:TTranslate>DEFINE_COTRAITANT_CONJOINT</com:TTranslate>
                                                                </strong>
                                                            </div>
                                                            <div style="display:<%#$this->Data->getSousTraitant() == '1' ? 'block' : 'none'%>"
                                                                 title="<%=Prado::Localize('TEXT_SOUS_TRAITANT')%>"
                                                                 class="statut-collaborateur">
                                                                <strong class="small text-danger">
                                                                    <com:TTranslate>TEXT_SOUS_TRAITANT</com:TTranslate>
                                                                </strong>
                                                            </div>
                                                            <div class="clause">
                                                                <com:TImage
                                                                        visible="<%#$this->Data->getClauseSocial() == '1' ? 'true' : 'false'%>"
                                                                        ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-clause-sociale.gif"
                                                                        Attributes.alt="<%=Prado::Localize('TEXT_CLAUSES_SOCIALES')%>"
                                                                        Attributes.title="<%=Prado::Localize('TEXT_CLAUSES_SOCIALES_INSERTION')%>"
                                                                />
                                                                <com:TImage
                                                                        visible="<%#$this->Data->getEntrepriseAdapte() == '1' ? 'true' : 'false'%>"
                                                                        ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-clause-handicap.gif"
                                                                        Attributes.alt="<%=Prado::Localize('TEXT_MARCHES_RESERVE_EA_ESAT')%>"
                                                                        Attributes.title="<%=Prado::Localize('TEXT_MARCHES_RESERVE_EA_ESAT')%>"
                                                                />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-6">
                                                        <com:TConditional
                                                                Condition="$this->SourceTemplateControl->ShowDescTypeCotraitanceRecherche($this->Data)">
                                                            <prop:TrueTemplate>
                                                                <div class="read-more box description">
                                                                    <blockquote class="blockquote-reverse small">
                                                                        <%#$this->Data->getDescTypeCotraitanceRecherche()%>
                                                                    </blockquote>
                                                                </div>
                                                            </prop:TrueTemplate>
                                                        </com:TConditional>
                                                        <com:TConditional
                                                                Condition="$this->SourceTemplateControl->ShowDescMonApportMarche($this->Data)">
                                                            <prop:TrueTemplate>
                                                                <div class="read-more box description">
                                                                    <blockquote class="blockquote-reverse small">
                                                                        <%#$this->Data->getDescMonApportMarche()%>
                                                                    </blockquote>
                                                                </div>
                                                            </prop:TrueTemplate>
                                                        </com:TConditional>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>
                                    </prop:ItemTemplate>
                                </com:TRepeater>
                            </ul>
                        </div>
                    </div>
                    <!--END RESULT SEARCH-->
                    <!--Debut map small-->
                    <div class="col-md-4">
                        <com:TActivePanel id="panelMap" cssClass="m-t-2">
                            <div id="mapSmall">
                                <a href="javascript:void(0);"
                                   class="zoom-link"
                                   onclick="openModal('map-large','popup-800',this);map.invalidateSize();map.fitBounds(bounds, { padding: [20, 20] });"
                                   title='<%=Prado::localize("CODE_LISTE_ENTREPRISES_GROUPEMENT")%>'>
                                    <i class="fa fa-arrows-alt"></i>
                                </a>
                            </div>
                        </com:TActivePanel>
                    </div>
                    <!--Fin map small-->
                </div>

                <!-- BEGIN PAGINATION BOTTOM-->
                <com:TActivePanel id="pagerButtom"  cssClass="line-partitioner" style="display:none;">
                    <div class="clearfix m-t-2">
                        <div class="form-inline form-group-sm pull-right">
                            <div class="form-group">
                                <label class="m-r-1" for="nombreResultatAfficherBottom">
                                    <com:TTranslate>DEFINE_TEXT_AFFICHER</com:TTranslate>
                                </label>
                                <com:TDropDownList ID="nombreResultatAfficherBottom" CssClass="form-control"
                                                   AutoPostBack="true" onSelectedIndexChanged="changePageSize"
                                                   Attributes.title='<%=Prado::localize("NOMBRE_DE_RESULTAT_PAR_PAGE")%>'>
                                    <com:TListItem Text="10" Selected="10"/>
                                    <com:TListItem Text="20" Value="20"/>
                                    <com:TListItem Text="50" Value="50" />
                                    <com:TListItem Text="100" Value="100" />
                                    <com:TListItem Text="500" Value="500" />
                                </com:TDropDownList>
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="numPageBottom">
                                    <com:TTranslate>ALLER_A_LA_PAGE</com:TTranslate>
                                </label>
                                <com:TPanel ID="panelBouttonGotoPageBottom" DefaultButton="DefaultButtonBottom"
                                            CssClass="float-left">
                                    <label class="m-l-1 m-r-1">
                                        <com:TTranslate>DEFINE_RESULTATS_PAGE</com:TTranslate>
                                    </label>
                                    <com:TTextBox ID="numPageBottom"
                                                  Attributes.title='<%=Prado::localize("N_DE_LA_PAGE")%>' Text="1"
                                                  CssClass="form-control w-50 text-center"/>
                                    <label class="nb-total ">/
                                        <com:TLabel ID="nombrePageBottom"/>
                                    </label>
                                    <com:TButton ID="DefaultButtonBottom" OnClick="goToPage"
                                                 Attributes.style="display:none"/>
                                </com:TPanel>
                            </div>
                            <div class="form-group liens">
                                <com:TPager ID="PagerBottom"
                                            ControlToPaginate="repeaterCotraitants"
                                            FirstPageText="<div class='btn' data-toggle='tooltip' data-placement='top' title='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>'><i class='fa fa-angle-double-left'></i></div>"
                                            LastPageText="<div class='btn' data-toggle='tooltip' data-placement='top' title='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>'><i class='fa fa-angle-double-right'></i></div>"
                                            Mode="NextPrev"
                                            NextPageText="<div class='btn' data-toggle='tooltip' data-placement='top' title='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>'><i class='fa fa-angle-right'></i></div>"
                                            PrevPageText="<div class='btn' data-toggle='tooltip' data-placement='top' title='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>'><i class='fa fa-angle-left '></i></div>"
                                            OnPageIndexChanged="pagerGoToPage"/>
                            </div>
                        </div>
                    </div>
                </com:TActivePanel>
                <!--END PAGINATION BOTTOM-->
            </div>
            <div class="panel-footer">
                <div class="link-line clearfix">
                    <com:TActiveLinkButton id="buttonRetour"
                                           cssClass="btn btn-sm btn-default pull-left"
                                           OnCallback="Page.backForSearch"
                    >
                        <com:TTranslate>TEXT_RETOUR</com:TTranslate>
                    </com:TActiveLinkButton>

                    <com:THyperLink ID="linkNewSearch"
                                    NavigateUrl="?page=Entreprise.EntrepriseRechercheBourseCotraitance&id=<%=$_GET['id']%>&orgAcronyme=<%=$_GET['orgAcronyme']%>"
                                    cssClass="btn btn-sm btn-primary pull-right"
                                    Attributes.title="<%=Prado::localize('TEXT_NOUVELLE_RECHERCHE')%>"
                                    Text="<%=Prado::localize('TEXT_NOUVELLE_RECHERCHE')%>"
                    />

                </div>
            </div>
        </div>
    </div>
</div>
<!--END RESULTATS-->

<!--Debut Modal-->
<div class="map-large" style="display:none;">
    <!--Debut Map full-size-->
    <div id="map" class="full-size-map"></div>
    <!--Fin Map full-size-->
    <!--Debut line boutons-->
    <div class="boutons-line">
        <input type="button" class="btn btn-sm btn-default m-t-1 pull-right" value="Fermer"
               onclick="J('.map-large').dialog('close');"/>
    </div>
    <!--Fin line boutons-->
</div>
<!--Fin Modal-->

<com:TActiveLabel id="labelScript" style="display:none"/>
<com:TActiveLabel id="labelScript2" style="display:none"/>