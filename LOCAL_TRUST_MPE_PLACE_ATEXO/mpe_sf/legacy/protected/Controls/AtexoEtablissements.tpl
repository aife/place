<com:TConditional condition="$this->isResponsive()">
	<prop:trueTemplate>
		<%include Application.templates.responsive.AtexoEtablissements %>
	</prop:trueTemplate>
	<prop:falseTemplate>
		<%include Application.templates.mpe.AtexoEtablissements %>
	</prop:falseTemplate>
</com:TConditional>