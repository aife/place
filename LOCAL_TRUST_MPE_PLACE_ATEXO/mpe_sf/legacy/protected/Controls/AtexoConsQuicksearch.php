<?php

namespace Application\Controls;

use Application\Propel\Mpe\CommonGeolocalisationN2;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Category;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2;
use Prado\Prado;

class AtexoConsQuicksearch extends MpeTTemplateControl
{
    public $langue;

    public function onLoad($param)
    {
        $this->langue = Atexo_CurrentUser::readFromSession('lang');
        self::displayCategory(true);

        // Si le formulaire a été soumis côté SF
        if ($_POST['fromHomeSimpleSearch']) {
            $this->onSearchClick();
        }

    }public function displayCategory($calledFromPrtail)
    {
        $categorie = new Atexo_Consultation_Category();
        $CategorieObj = $categorie->retrieveCategories($calledFromPrtail, true, $this->langue);
        $listCategorie = $categorie->retrieveDataCategories($CategorieObj, $this->langue);
        $arrayCategorie = Atexo_Util::arrayUnshift(
            $listCategorie,
            '--- ' . Prado::localize(
                'TEXT_RECHERCHE_AVANCEE_TOUTES_LES_CATEGORIES'
            ) . ' ---'
        );
        $this->categorie->DataSource = $arrayCategorie;
        $this->categorie->DataBind();
    }

    public function onSearchClick()
    {
        $criteriaVo = new Atexo_Consultation_CriteriaVo();
        $criteriaVo->setLieuxexecution(trim(self::getIdsSelectedGeoN2()));
        $criteriaVo->setEtatConsultation(Atexo_Config::getParameter('STATUS_CONSULTATION'));
        $criteriaVo->setcalledFromPortail(true);
        $criteriaVo->setTypeAcces(Atexo_Config::getParameter('TYPE_PROCEDURE_PUBLICITE'));
        $criteriaVo->setIdTypeAvis(Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION'));

        if (trim($this->motsCle->Text)) {
            $criteriaVo->setKeyWordAdvancedSearch(trim($this->motsCle->Text));
        }
        if ('0' != $this->categorie->getSelectedValue()) {
            $criteriaVo->setCategorieConsultation($this->categorie->getSelectedValue());
        }

        // Si le formulaire a été soumis côté SF
        if ($_POST['fromHomeSimpleSearch']) {
            $motCle = $_POST['keyword'];
            $categorieConsultation = $_POST['categorie'];

            $criteriaVo->setKeyWordAdvancedSearch(trim($motCle));
            $criteriaVo->setCategorieConsultation($categorieConsultation);
        }

        $this->setViewState('CriteriaVo', $criteriaVo);
        Atexo_CurrentUser::writeToSession('criteriaCons', $criteriaVo);
        $this->response->redirect('?page=Entreprise.EntrepriseAdvancedSearch&searchAnnCons&WithCritere');
    }

    public function getIdsSelectedGeoN2()
    {
        $liste = $_POST['localisations'];
        $arrayIdGeolocalisation = [];
        if (is_array($liste)) {
            foreach ($liste as $denominationN2) {
                $denominationN2 = (new Atexo_Geolocalisation_GeolocalisationN2())->retrieveGeoN2ByDenomination2($denominationN2);
                if ($denominationN2 instanceof CommonGeolocalisationN2) {
                    $arrayIdGeolocalisation[] = $denominationN2->getId();
                }
            }
        }
        if (count($arrayIdGeolocalisation)) {
            return implode(',', $arrayIdGeolocalisation);
        } else {
            return '';
        }
    }
}
