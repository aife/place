<?php

namespace Application\Controls;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonEntrepriseInfoExercice;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Entreprise;
use Application\Service\Atexo\Atexo_Exception;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;

/**
 * commentaires.
 *
 * @author SBAI Yassine <yassine.sbai@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class ActiviteEntreprise extends MpeTPage
{
    public bool $_idEntreprise = false;
    public bool $_org = false;
    public bool $_calledFrom = false;
    public $_entreprise = false;
    public ?string $_compteEntrepriseDonneesFinancieres = null;
    public ?string $_compteEntrepriseChiffreAffaireProductionBiensServices = null;

    public function setIdEnreprise($value)
    {
        $this->_idEntreprise = $value;
    }

    public function getIdEnreprise()
    {
        return $this->_idEntreprise;
    }

    public function setCalledFrom($value)
    {
        $this->_calledFrom = $value;
    }

    public function getCalledFrom()
    {
        return $this->_calledFrom;
    }

    public function setOrg($value)
    {
        $this->_org = $value;
    }

    public function getOrg()
    {
        return $this->_org;
    }

    public function onLoad($param)
    {
        $this->_compteEntrepriseDonneesFinancieres = 'none';
        $this->_compteEntrepriseChiffreAffaireProductionBiensServices = '';
        $this->customizeForm();

        /* Pour les tests : */
        if (!$this->getIdEnreprise()) {
            throw new Atexo_Exception("IdentificationEntreprise Error : Attendu id de l'entreprise");
        }

        if (!$this->getCalledFrom()) {
            throw new Atexo_Exception('IdentificationEntreprise Error : Attendu le param calledfrom');
        }

        /*if(!$this->getOrg()) {
            throw new Atexo_Exception("IdentificationEntreprise Error : Attendu l'acronime de l'organisme");
        }*/

        if ('agent' == $this->getCalledFrom()) {
            $this->linkEditerChiffresCles->visible = false;
        } else {
            $this->linkEditerChiffresCles->visible = true;
        }
        $this->remplirActiviteEntreprise($this->getIdEnreprise());
        $this->remplirTroisDernieresAnnees();
        if (!$this->isPostBack) {
            /*$this->siteInternet->Text = Atexo_Util::utf8ToIso($this->_entreprise->getSiteInternet());
            $this->descriptionActivite->Text = Atexo_Util::utf8ToIso($this->_entreprise->getDescriptionActivite());
            $this->activiteDefense->Text = Atexo_Util::utf8ToIso($this->_entreprise->getActiviteDomaineDefense());*/
        }
    }

    public function remplirActiviteEntreprise($idEntreprise)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $this->_entreprise = Atexo_Entreprise::getInfoEntreprise($idEntreprise, $connexionCom);

        if ($this->_entreprise) {
            $annee = Atexo_Util::getlistetroisdernieresannees();
            $EntrepriseInfoExercice1 = CommonEntrepriseInfoExercice::getCommonEntrepriseInfoExercice($this->_entreprise->getId(), $annee[1], $connexionCom);
            if ($EntrepriseInfoExercice1) {
                $this->anneeClotureExercice1->Text = $EntrepriseInfoExercice1->getAnneeClotureExercice();
                $this->debutExercice1->Text = Atexo_Util::iso2frnDate($EntrepriseInfoExercice1->getDebutExerciceGlob());
                $this->finExercice1->Text = Atexo_Util::iso2frnDate($EntrepriseInfoExercice1->getFinExerciceGlob());
                $this->vente1->Text = Atexo_Util::getMontantArronditEspace($EntrepriseInfoExercice1->getVentesGlob(), 2);
                $this->bien1->Text = Atexo_Util::getMontantArronditEspace($EntrepriseInfoExercice1->getBiensGlob(), 2);
                $this->service1->Text = Atexo_Util::getMontantArronditEspace($EntrepriseInfoExercice1->getServicesGlob(), 2);
                $this->total1->Text = Atexo_Util::getMontantArronditEspace($EntrepriseInfoExercice1->getTotalGlob(), 2);
                $this->effectifMoyen1->Text = $EntrepriseInfoExercice1->getEffectifMoyen();
                $this->effectifEncadrement1->Text = $EntrepriseInfoExercice1->getEffectifEncadrement();
                $this->pme1->Text = $this->getEtatEntreprise($EntrepriseInfoExercice1->getPme());

                $this->caA1->Text = Atexo_Util::getMontantArronditEspace($EntrepriseInfoExercice1->getChiffreAffaires(), 2);
                $this->bA1->Text = Atexo_Util::getMontantArronditEspace($EntrepriseInfoExercice1->getBesoinExcedentFinancement(), 2);
                $this->cfA1->Text = Atexo_Util::getMontantArronditEspace($EntrepriseInfoExercice1->getCashFlow(), 2);
                $this->ceA1->Text = Atexo_Util::getMontantArronditEspace($EntrepriseInfoExercice1->getCapaciteEndettement(), 2);
            }
            $EntrepriseInfoExercice2 = CommonEntrepriseInfoExercice::getCommonEntrepriseInfoExercice($idEntreprise, $annee[2], $connexionCom);
            if ($EntrepriseInfoExercice2) {
                $this->anneeClotureExercice2->Text = $EntrepriseInfoExercice2->getAnneeClotureExercice();
                $this->debutExercice2->Text = Atexo_Util::iso2frnDate($EntrepriseInfoExercice2->getDebutExerciceGlob());
                $this->finExercice2->Text = Atexo_Util::iso2frnDate($EntrepriseInfoExercice2->getFinExerciceGlob());
                $this->vente2->Text = Atexo_Util::getMontantArronditEspace($EntrepriseInfoExercice2->getVentesGlob(), 2);
                $this->bien2->Text = Atexo_Util::getMontantArronditEspace($EntrepriseInfoExercice2->getBiensGlob(), 2);
                $this->service2->Text = Atexo_Util::getMontantArronditEspace($EntrepriseInfoExercice2->getServicesGlob(), 2);
                $this->total2->Text = Atexo_Util::getMontantArronditEspace($EntrepriseInfoExercice2->getTotalGlob(), 2);
                $this->effectifMoyen2->Text = $EntrepriseInfoExercice2->getEffectifMoyen();
                $this->effectifEncadrement2->Text = $EntrepriseInfoExercice2->getEffectifEncadrement();
                $this->pme2->Text = $this->getEtatEntreprise($EntrepriseInfoExercice2->getPme());

                $this->caA2->Text = Atexo_Util::getMontantArronditEspace($EntrepriseInfoExercice2->getChiffreAffaires(), 2);
                $this->bA2->Text = Atexo_Util::getMontantArronditEspace($EntrepriseInfoExercice2->getBesoinExcedentFinancement(), 2);
                $this->cfA2->Text = Atexo_Util::getMontantArronditEspace($EntrepriseInfoExercice2->getCashFlow(), 2);
                $this->ceA2->Text = Atexo_Util::getMontantArronditEspace($EntrepriseInfoExercice2->getCapaciteEndettement(), 2);
            }
            $EntrepriseInfoExercice3 = CommonEntrepriseInfoExercice::getCommonEntrepriseInfoExercice($idEntreprise, $annee[3], $connexionCom);
            if ($EntrepriseInfoExercice3) {
                $this->anneeClotureExercice3->Text = $EntrepriseInfoExercice3->getAnneeClotureExercice();
                $this->debutExercice3->Text = Atexo_Util::iso2frnDate($EntrepriseInfoExercice3->getDebutExerciceGlob());
                $this->finExercice3->Text = Atexo_Util::iso2frnDate($EntrepriseInfoExercice3->getFinExerciceGlob());
                $this->vente3->Text = Atexo_Util::getMontantArronditEspace($EntrepriseInfoExercice3->getVentesGlob(), 2);
                $this->bien3->Text = Atexo_Util::getMontantArronditEspace($EntrepriseInfoExercice3->getBiensGlob(), 2);
                $this->service3->Text = Atexo_Util::getMontantArronditEspace($EntrepriseInfoExercice3->getServicesGlob(), 2);
                $this->total3->Text = Atexo_Util::getMontantArronditEspace($EntrepriseInfoExercice3->getTotalGlob(), 2);
                $this->effectifMoyen3->Text = $EntrepriseInfoExercice3->getEffectifMoyen();
                $this->effectifEncadrement3->Text = $EntrepriseInfoExercice3->getEffectifEncadrement();
                $this->pme3->Text = $this->getEtatEntreprise($EntrepriseInfoExercice3->getPme());

                $this->caA3->Text = Atexo_Util::getMontantArronditEspace($EntrepriseInfoExercice3->getChiffreAffaires(), 2);
                $this->bA3->Text = Atexo_Util::getMontantArronditEspace($EntrepriseInfoExercice3->getBesoinExcedentFinancement(), 2);
                $this->cfA3->Text = Atexo_Util::getMontantArronditEspace($EntrepriseInfoExercice3->getCashFlow(), 2);
                $this->ceA3->Text = Atexo_Util::getMontantArronditEspace($EntrepriseInfoExercice3->getCapaciteEndettement(), 2);
            }
        }
    }

    /**
     * retourne le libellé corréspond à l'état de l'entreprise.
     */
    public function getEtatEntreprise($pme)
    {
        if (null == $pme) {
            return Prado::localize('NON_RENSEIGNE');
        } elseif (0 == $pme) {
            return Prado::localize('DEFINE_NON');
        } elseif (1 == $pme) {
            return Prado::localize('DEFINE_OUI');
        }
    }

    public function remplirTroisDernieresAnnees()
    {
        $annees = Atexo_Util::getListeTroisDernieresAnnees();
        $this->anneeClotureExercice1->Text = $annees['1'];
        $this->anneeClotureExercice2->Text = $annees['2'];
        $this->anneeClotureExercice3->Text = $annees['3'];
    }

    /***
     * afficher les champs qui sont propres aux modules activés
     */
    public function customizeForm()
    {
        if (Atexo_Module::isEnabled('CompteEntrepriseDonneesFinancieres')) {
            $this->_compteEntrepriseDonneesFinancieres = '';
        } else {
            $this->_compteEntrepriseDonneesFinancieres = 'none';
        }
        if (!Atexo_Module::isEnabled('CompteEntrepriseChiffreAffaireProductionBiensServices')) {
            $this->_compteEntrepriseChiffreAffaireProductionBiensServices = 'none';
        } else {
            $this->_compteEntrepriseChiffreAffaireProductionBiensServices = '';
        }
    }
}
