<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Statistiques\Atexo_Statistiques_RapportsStatistiques;
use Exception;
use Prado\Prado;
use Prado\Util\TLogger;
use Prado\Web\UI\WebControls\TTextBox;

/**
 * permet de gérer les établissements.
 *
 * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
 *
 * @version 1.0
 *
 * @since 4.11.0
 *
 * @copyright Atexo 2015
 */
class AtexoCriteresStatistiques extends MpeTTemplateControl
{
    protected ?string $_uri = null;
    protected $_nomStat;

    /**
     * Get the value of [_uri] column.
     *
     * @return string _uri
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function getUri()
    {
        return $this->_uri;
    }

    /**
     * Set the value of [_uri] column.
     *
     * @param string $v new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function setUri($v)
    {
        $this->_uri = $v;
    }

    public function onLoad($param)
    {
        $this->panelBlocErreur->setDisplay('None');
        if (!$this->page->isPostBack) {
            $this->initialiserComposant();
        }
    }

    public function getNomStat()
    {
        return $this->_nomStat;
    }

    public function setNomStat($v)
    {
        $this->_nomStat = $v;
    }

    public function initialiserComposant()
    {
        $this->repeaterCriters->DataSource = self::initCritere();
        $this->repeaterCriters->DataBind();
    }

    public function initCritere()
    {
        try {
            $criteres = Atexo_Statistiques_RapportsStatistiques::getCritereStatistiquesByUri($this->_uri);

            if (is_array($criteres) && !empty($criteres)) {
                foreach ($criteres as $i => $critere) {
                    if ('singleValueText' == $critere->type && !Atexo_CurrentUser::hasHabilitation('HyperAdmin')) {
                        $criteres[$i]->defaultValue = (preg_match('#ministere#i', $critere->id)) ? Atexo_CurrentUser::getCurrentOrganism() : '';
                    }
                }
            } else {
                $criteres = [];
            }
        } catch (Exception $e) {
            $this->panelBlocErreur->setDisplay('Dynamic');
            $this->panelRechercheCritere->setDisplay('None');
            Prado::log($e->getMessage().$e->getTraceAsString(), TLogger::ERROR, 'Atexo');

            return [];
        }
        //var_dump($criteres);
        return $criteres;
    }

    public function getDataSourceListe($options)
    {
        $data = [];
        if (is_array($options)) {
            foreach ($options as $option) {
                $data[$option->value] = (new Atexo_Config())->toPfEncoding($option->label);
            }
        }
        //TODO ADD SELECTIONNER
        return $data;
    }

    /**
     * redirige vers la page du rapport.
     *
     * @author FIZ <fatima.izgua@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function onGenererClick()
    {
        try {
            $tabCritere = $this->getCriteresRecherche();
            $this->rapportResultat->Text = (new Atexo_Config())->toPfEncoding(Atexo_Statistiques_RapportsStatistiques::getResultatRechercheRapport($tabCritere, $this->_uri));
            $this->panelRechercheResultat->Visible = true;
        } catch (\Exception $e) {
            $this->panelBlocErreur->setDisplay('Dynamic');
            Prado::log($e->getMessage().$e->getTraceAsString(), TLogger::ERROR, 'Atexo');
        }
    }

    public function onGenererRapportClick($sender, $param)
    {
        try {
            $typeRapport = match ($param->CommandName) {
                'genererXLS' => 'xls',
                'genererPDF' => 'pdf',
                default => 'html',
            };
            Atexo_Statistiques_RapportsStatistiques::getResultatRechercheRapport($this->getCriteresRecherche(), $this->_uri, $typeRapport);
        } catch (\Exception $e) {
            $this->panelBlocErreur->setDisplay('Dynamic');
            Prado::log($e->getMessage().$e->getTraceAsString(), TLogger::ERROR, 'Atexo');
        }
    }

    protected function getCriteresRecherche()
    {
        $tabCritere = [];
        foreach ($this->repeaterCriters->Items as $item) {
            $idCritere = $item->idCritere->getValue();

            $typeCritere = $item->typeCritere->getValue();
            if ('multiSelect' == $typeCritere) {
                $tabCritere[$idCritere] .= $item->dropDownListe->getSelectedValue();
            } elseif ('singleValueText' == $typeCritere) {
                $tabCritere[$idCritere] .= ($item->champText instanceof TTextBox) ? $item->champText->Text : $item->champText->Value;
                if (preg_match('#ministere#i', $idCritere)) {
                    $tabCritere[$idCritere] = Atexo_CurrentUser::getCurrentOrganism();
                }
            } elseif ('singleValueDate' == $typeCritere) {
                $tabCritere[$idCritere] .= Atexo_Util::frnDate2iso($item->champDate->Text);
            }
        }

        return $tabCritere;
    }
}
