<com:TPanel DefaultButton="btSearch">
    <com:TPanel id="panelTextChoixDest" visible="<%=($_GET['page'] == 'Agent.ChoixDestinataireBdFournisseur') ? true:false%>" >
        <div class="breadcrumbs">
            <span class="normal">
                <com:TTranslate>DEFINE_CONSULTATIONS</com:TTranslate> >
                <com:TTranslate>DEFINE_TEXT_ENVOI_COURRIER_ELECTRONIQUE</com:TTranslate> >
                <com:TTranslate>DEFINE_TEXT_AJOUT_MODIFICATION_DESTINATAIRE</com:TTranslate> >
                <com:TTranslate>TEXT_ADRESSES_BD_FOURNISSEURS</com:TTranslate>
            </span>
        </div>
    </com:TPanel>
    <com:TPanel id="panelTextBDFournisseur" visible="<%=($_GET['page'] == 'Agent.ChoixDestinataireBdFournisseur') ? 'false' : 'true' %>" >
        <com:TPanel Visible="<%=!($_GET['page'] == 'Entreprise.RechercheCollaborateur') ? 'true' : 'false' %>">
            <div class="breadcrumbs">
                <span class="normal">
                    <com:TTranslate>TEXT_BASE_DONNEES_FOURNISSEURS</com:TTranslate>
                </span>
                &gt; <com:TTranslate>RECHERCHER</com:TTranslate>
            </div>
        </com:TPanel>
        <com:TPanel Visible="<%=($_GET['page'] == 'Entreprise.RechercheCollaborateur') ? 'true' : 'false' %>">
            <div class="clearfix">
                <ul class="breadcrumb">
                    <li>
                        <a href="/entreprise">
						<com:TTranslate>TEXT_ACCUEIL</com:TTranslate>
                    </a>
                    </li>
                    <li>
                        <a href="?page=Entreprise.RechercheCollaborateur">
                            <com:TTranslate>TEXT_BOURSE_A_LA_CO_SOUS_TRAITANCE</com:TTranslate>
                        </a>
                    </li>
                    <li>
                        <com:TTranslate>TEXT_RECHERCHE_COLLABORATION</com:TTranslate>
                    </li>
                </ul>
            </div>
        </com:TPanel>
    </com:TPanel>
    <div class="form-field <%=($this->getCalledFrom() == 'agent')?'':'panel panel-default form-horizontal'%>" id="blocRechercheCollaboration" style="display: block;">
        <!--Fin Ligne Recherche par mots cles--> <!--Debut Bloc Critere de recherche complementaire-->
        <div class="panel-heading">
            <h1 class="h4 panel-title">
                <span  style="display:<%=($this->getCalledFrom() == 'entreprise')?'':'none'%>" class="title">
                    <com:TTranslate>TEXT_RECHERCHE_COLLABORATION</com:TTranslate>
                </span>
            </h1>
        </div>
        <div class="top" style="display:<%=($this->getCalledFrom() == 'agent')?'':'none'%>">
            <span class="left">&nbsp;</span>
            <span class="right">&nbsp;</span>
            <span  class="title">
                <com:TTranslate>TEXT_RECHERCHE_MULTI_CRITERES</com:TTranslate>
            </span>
        </div>
        <div class="content">
            <div class="panel-body">
                <!--Debut Ligne Reference-->
                <com:TPanel visible="<%=($this->getCalledFrom() == 'agent')?true:false%>">
                    <div class="line">
                        <div class="intitule-150"><label for="raisonSociale"><com:TTranslate>TEXT_RAISON_SOCIALE</com:TTranslate></label>
                            :</div>
                        <com:TTextBox id="raisonSociale" cssClass="moyen" Attributes.title="<%=Prado::localize('TEXT_RAISON_SOCIALE')%>" />
                    </div>
                    <!--Fin Ligne Reference--> <!--Debut Ligne Code Postal-->
                    <div class="line">
                        <div class="intitule-150"><label for="cpSiegeSocial"><com:TTranslate>TEXT_CP</com:TTranslate></label>
                            :</div>
                        <com:TTextBox id="cpSiegeSocial" Attributes.title="<%=Prado::localize('TEXT_CP')%>" cssClass="cp" />
                    </div>
                    <!--Fin Ligne Code Postal--> <!-- Commposant qui affiche la liste des regions et provinces-->
                    <com:AtexoRegionProvince ID="panelRegionProvince" /> <!-- --> <!--Debut Ligne Ville-->
                    <div class="line">
                        <div class="intitule-150"><label for="villeSiegeSocial"><com:TTranslate>TEXT_VILLE</com:TTranslate></label>
                            :</div>
                        <com:TTextBox id="villeSiegeSocial" Attributes.title="<%=Prado::localize('TEXT_VILLE')%>" cssClass="input-185" />
                    </div>
                    <!--Fin Ligne Ville-->
                    <div class="spacer-small"></div>
                    <!--Debut Line Entreprise etablie en France-->
                    <div class="line">
                        <div class="intitule-auto bloc-700">
                            <com:TRadioButton Checked="true" GroupName="RadioGroup" id="france" Attributes.title="<%=Prado::localize('TEXT_ENTREPRISE_ETABLIE_EN_FRANCE')%>"
                                              cssClass="radio" Attributes.onclick="isCheckedShowDiv(this,'etablieFrance');isCheckedHideDiv(this,'nonEtablieFrance');" />
                            <label for="france">
                                <com:TTranslate>TEXT_ENTREPRISE_ETABLIE_EN_FRANCE</com:TTranslate>
                            </label>
                        </div>
                    </div>
                    <div class="line">
                        <div id="etablieFrance" style="display: block;">
                            <com:TPanel ID="panelSiren">
                                <div class="intitule-150 indent-30">
                                    <div class="float-left">
                                        <label for="siren">
                                            <com:TTranslate>TEXT_SIREN</com:TTranslate><com:TTranslate>TEXT_SLASH</com:TTranslate>
                                        </label>
                                        <label for="siret"><com:TTranslate>TEXT_SIRET</com:TTranslate></label>
                                    </div>
                                </div>
                                <com:TTextBox id="siren" MaxLength="9" Attributes.title="<%=Prado::localize('TEXT_SIREN')%>"
                                              cssClass="siren" />
                                <com:TTextBox id="siret" MaxLength="5"
                                              Attributes.title="SIRET" cssClass="siret" />
                                <com:TCustomValidator
                                        Enabled="true"
                                        ID="validatorSiren"
                                        ValidationGroup="sirenValidationGroup"
                                        ControlToValidate="siren"
                                        Text="<br/>Le SIRET est invalide"
                                        Display="Dynamic"
                                        ClientValidationFunction="controlSiretinSerach" />
                            </com:TPanel>
                            <com:TPanel ID="panelRc" >
                                <div class="intitule-120 indent-30">
                                    <label for="<%=$this->getClientId()%>villeRc"><com:TTranslate>DEFINE_VILLE_RC</com:TTranslate> :</label>
                                </div>
                                <com:TDropDownList ID="villeRc" Attributes.title="<%=Prado::localize('DEFINE_VILLE_RC')%>"  CssClass="select-185"/>
                                <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" onmouseover="afficheBulle('infosSiret', this)" onmouseout="cacheBulle('infosSiret')" class="picto-info-intitule" alt="Info-bulle" title="Info-bulle" />
                                <div class="breaker"></div>
                                <div class="intitule-120 indent-30">
                                    <label for="<%=$this->getClientId()%>numeroRc"><com:TTranslate>DEFINE_NUMERO</com:TTranslate></label> :
                                </div>
                                <com:TTextBox  id="numeroRc" CssClass="input-185" Attributes.title="<%=Prado::localize('DEFINE_NUMERO')%>" />
                            </com:TPanel>
                            <com:TPanel ID="panelIdentifiantUnique">
                                <div class="intitule-120 indent-30">
                                    <div class="float-left">
                                        <label for="siren">
                                            <com:TTranslate>TEXT_SIREN</com:TTranslate>
                                        </label>
                                    </div>
                                </div>
                                <com:TTextBox id="identifiantUnique" Attributes.title="<%=Prado::localize('TEXT_SIREN')%>" cssClass="input-185" />
                            </com:TPanel>
                        </div>
                    </div>
                    <!--Fin Line Entreprise etablie en France--> <!--Debut Line Entreprise non etablie en France-->
                    <div class="line">
                        <div class="intitule-auto bloc-700">
                            <com:TRadioButton GroupName="RadioGroup" id="etranger"
                                              Attributes.title="<%=Prado::localize('TEXT_ENTREPRISE_NON_ETABLIE_EN_FRANCE')%>" cssClass="radio"
                                              Attributes.onclick="isCheckedShowDiv(this,'nonEtablieFrance');isCheckedHideDiv(this,'etablieFrance');" />
                            <label for="etranger"><com:TTranslate>TEXT_ENTREPRISE_NON_ETABLIE_EN_FRANCE</com:TTranslate></label>
                        </div>
                        <div id="nonEtablieFrance" style="display: none;">
                            <div class="intitule-120 indent-30"><label for="pays"><com:TTranslate>TEXT_PAYS</com:TTranslate></label>
                                :</div>
                            <com:DropDownListCountries TypeAffichage="withCode" ID="listPays"
                                                       cssClass="select-185" />
                            <div class="breaker"></div>
                            <div class="intitule-120 indent-30"><label for="idNational"><com:TTranslate>TEXT_IDENTIFIANT_NATIONNAL</com:TTranslate></label>
                                :</div>
                            <com:TTextBox id="idNational" Attributes.title="<%=Prado::localize('TEXT_IDENTIFIANT_NATIONNAL')%>" cssClass="input-185" />
                            <span class="info-aide-right"><com:TTranslate>TEXT_NUM_ENREGISTREMENT_NATIONAL</com:TTranslate></span>
                        </div>
                        <div class="spacer-mini"></div>
                    </div>
                    <div class="spacer"></div>
                    <!--Fin Line Entreprise non etablie en France-->
                    <!--Debut ligne Pays indifferent-->
                    <div class="line">
                        <div class="intitule-auto bloc-700">
                            <com:TRadioButton GroupName="RadioGroup" id="paysIndifferent" Attributes.title="<%=Prado::localize('TEXT_PAYS_INDIFFERENT')%>" cssClass="radio" Attributes.onclick="isCheckedHideDiv(this,'nonEtablieFrance');isCheckedHideDiv(this,'etablieFrance');" />
                            <label for="paysIndifferent">
                                <com:TTranslate>TEXT_PAYS_INDIFFERENT</com:TTranslate>
                            </label>
                        </div>
                        <!--Fin ligne Pays indifferent-->
                        <!-- Debut Ligne NAF-->
                        <com:TPanel ID="panelCodeApe" cssClass="line">
                            <div class="intitule-150"><com:TTranslate>TEXT_APE_NAF_NACE</com:TTranslate>:</div>
                            <label for="apeNafNace_1" style="display: none;"><com:TTranslate>TEXT_APE_NAF_NACE_PREMIER_CHIFFRES</com:TTranslate></label>
                            <label for="apeNafNace_2" style="display: none;"><com:TTranslate>TEXT_DERNIER_CHIFFRE</com:TTranslate></label>
                            <com:TTextBox id="apeNafNace_1" MaxLength="4" Attributes.title="<%=Prado::localize('TEXT_APE_NAF_NACE')%>" cssClass="input-30 float-left" />
                            <com:TTextBox id="apeNafNace_2" MaxLength="1"
                                          Attributes.title="<%=Prado::localize('TEXT_APE_NAF_NACE')%>" cssClass="input-20 float-left" />




                        </com:TPanel> <!-- Fin Ligne NAF-->
                        <!-- début code NACE referentiel -->
                        <com:TPanel ID="panelCodeNaceRef" Display="Dynamic" cssClass="line">
                            <div class="intitule-150"><com:TTranslate>TEXT_CODE_NACE</com:TTranslate> :</div>
                            <div class="content-bloc">
                                <com:TActivePanel ID="panelReferentiel" >
                                    <div class="intitule-200">
                                        <com:AtexoReferentiel ID="atexoReferentiel" cas="cas5"/>
                                    </div>
                                </com:TActivePanel>
                            </div>
                        </com:TPanel>
                        <!-- fin code NACE referentiel -->
                    </div>
                </com:TPanel>
                <!-- Debut Ligne Statut particulier-->
                <com:TPanel ID="panelStatutCompteEntreprise" >
                    <div class="section">
                            <div class="line section-body clearfix">
                                <div class="intitule-bloc bloc-155 col-md-3 control-label p-l-0">
                            <com:TTranslate>DEFINE_STATUT_PARTICULIER</com:TTranslate> :
                                    <span class="m-0 p-0 m-l-3"></span>
                                </div>
                                <div class="content-bloc bloc-570 col-md-9 p-l-25">
                                    <div class="line checkbox">
                                        <com:TCheckBox
                                                id="entrepriseEA"
                                                Attributes.title="><%=Prado::localize('DEFINE_MON_ENTREPRISE_EA')%>"
                                                cssclass="cotraitance m-l-15-negative"   />
                                        <label for="type" class="cotraitance-no-padding">
                                        <com:TTranslate>DEFINE_ESAT_EA</com:TTranslate>
                                    </label>
                                </div>
                                    <div class="line checkbox">
                                        <com:TCheckBox id="entrepriseSIAE" Attributes.title="><%=Prado::localize('DEFINE_MON_ENTREPRISE_SIAE')%>" cssclass="check m-l-15-negative"   />
                                        <label for="type" class="cotraitance-no-padding">
                                        <com:TTranslate>DEFINE_SIAE</com:TTranslate>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </com:TPanel>
                <!-- fin Ligne Statut particulier-->
                <div class="section">
                    <com:TPanel  id="panelTypeCollaboration" Visible ="<%=( $this->getCalledFrom() == 'entreprise' &&  Application\Service\Atexo\Atexo_Module::isEnabled('BourseALaSousTraitance')) ? true:false%>">
                        <div class="section-heading page-header">
                            <h2 class="h4 m-b-0">
                                <%=Prado::localize('TEXT_RECHERCHE_MULTI_CRITERES')%>
                            </h2>
                        </div>
                        <div class="line section-body clearfix">
                            <div class="intitule-bloc bloc-155 col-md-3 control-label">
                                <%=Prado::localize('TEXT_TYPE_COLLABORATION')%> :
                                <span class="m-0 p-0 m-l-1">
                                <a data-target="#" data-toggle="tooltip" data-placement="right"
                                   title="<%=Prado::localize('TEXT_ENTREPRISE_POSTULE_EN_TANT_QUE')%> :"><i
                                        class="fa fa-question-circle text-info"></i></a>
                            </span>
                            </div>
                            <div class="content-bloc bloc-570 col-md-9">
                                <com:TRepeater  ID="repeaterTypeCollaboration" >
                                    <prop:ItemTemplate>
                                        <div class="line checkbox">
                                            <com:TCheckBox id="type" Attributes.title="<%#$this->Data['libelle']%>" cssClass="check" checked="true" />
                                            <label for="type" class="cotraitance-no-padding">
                                                <com:TLabel ID="denomination" Text="<%#$this->Data['libelle']%>"/>
                                                <com:THiddenField ID="idType" Value="<%#$this->Data['id']%>"/>
                                            </label>
                                        </div>
                                    </prop:ItemTemplate>
                                </com:TRepeater>

                            </div>
                        </div>
                    </com:TPanel>
                </div>
                <div class="spacer"></div>

                <com:AtexoLtReferentiel id="ltRefEntr" entreprise="1" cssClass="intitule-150" mode="2" idPage="PanelSearchCompany"/>

                <com:TPanel ID="panelDomainesActivites" cssClass="line" visible="<%=($this->getCalledFrom() == 'agent')?true:false%>">
                    <div class="intitule-150"><com:TTranslate>TEXT_DOMAINES_ACTIVITE</com:TTranslate>:</div>
                    <com:AtexoDomainesActivites ID="domaineActivite" />
                </com:TPanel>

                <!-- Début domaines d'activités LT-Réf -->
                <com:TPanel ID="panelDomainesActivitesLtRef" cssClass="line" visible="<%=($this->getCalledFrom() == 'agent')?true:false%>">
                    <div class="intitule-150"><com:TTranslate>TEXT_DOMAINES_ACTIVITE</com:TTranslate>:</div>
                    <com:AtexoReferentiel ID="idAtexoRefDomaineActivite" cas="cas5"/>
                </com:TPanel>

                <!-- Fin domaines d'activités LT-Réf -->
                <com:TPanel ID="panelQualifications" cssClass="line" visible="<%=($this->getCalledFrom() == 'agent')?true:false%>">
                    <div class="intitule-150"><com:TTranslate>TEXT_QUALIFICATIONS</com:TTranslate>:</div>
                    <com:AtexoQualification ID="qualification" />
                </com:TPanel>

                <!-- Début qualification LT-REF -->
                <com:TPanel ID="panelQualificationLtRef" cssClass="line" visible="<%=($this->getCalledFrom() == 'agent')?true:false%>">
                    <div class="intitule-bloc intitule-150"><label for="qualification"><com:TTranslate>TEXT_QUALIFICATIONS</com:TTranslate></label> :</div>
                    <com:AtexoReferentiel ID="idAtexoRefQualification" cas="cas10"/>
                </com:TPanel>
                <!-- Fin qualification LT-REF -->

                <com:TPanel ID="panelAgrements" cssClass="line" visible="<%=($this->getCalledFrom() == 'agent')?true:false%>">
                    <div class="intitule-bloc intitule-150"><label for="agrenments"><com:TTranslate>TEXT_AGREMENT</com:TTranslate></label> :</div>
                    <com:PanelAgrements ID="agrements" />
                </com:TPanel>

                <!-- Début certificat des comptes entreprises -->
                <com:TPanel ID="panelCertificats" cssClass="line">
                    <div class="intitule-bloc intitule-150"><label for="certificats"><com:TTranslate>TEXT_AGREMENT_LT_REFERENTIEL</com:TTranslate></label> :</div>
                    <com:AtexoReferentiel ID="idAtexoCertificatCompteEtpRef" cas="cas10"/>
                </com:TPanel>
                <!--Debut Type de collaboration-->

                <div class="spacer"></div>



                <!--Fin Type de collaboration-->

                <!--Debut Ligne Recherche par mots cles-->
                <div class="section">
                    <div class="section-heading page-header">
                        <h2 class="h4 m-b-0"><com:TTranslate>DEFINE_RECHERCHE_MOT_CLE</com:TTranslate></h2>
                    </div>
                    <div class="section-body clearfix">
                        <div class="line form-group form-group-sm clearfix">
                            <div style="display:<%=($this->getCalledFrom() == 'agent')?'':'none'%>"  class="intitule-auto bloc-740 col-md-3 control-label p-l-0">
                                <label for="ctl0_CONTENU_PAGE_panelSearch_keywordSearch">
                                    <com:TTranslate>TEXT_DANS_LA_DESCRIPTION_ACTIVITE</com:TTranslate>:
                                </label>
                            </div>
                            <div style="display:<%=($this->getCalledFrom() == 'agent')?'none':''%>" class="intitule-bloc bloc-155 col-md-3 control-label">
                                <label for="ctl0_CONTENU_PAGE_panelSearch_keywordSearch">
                                    <%=Prado::localize('DEFINE_MOTS_CLES')%> :
                                </label>
                            </div>
                            <div class="<%=($this->getCalledFrom() == 'agent')?'':'content-bloc bloc-600 col-md-9'%> ">
                                <com:TTextBox id="keywordSearch" Attributes.title="<%=Prado::localize('DEFINE_RECHERCHE_MOT_CLE')%>" cssClass="long<%=($this->getCalledFrom() == 'agent')?'-630 clear-both':' form-control'%>" />
                                <div style="display:<%=($this->getCalledFrom() == 'agent')?'none':''%>" class="info-aide info small">
                                    <%=Prado::localize('TEXT_RECHERCHE_DANS_DESCRPTION_ACTIVITE_ENTREPRISE')%>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <com:ComplementaireSearchCompany ID="panelComplementaireSearch"
                                                 Visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('CompteEntrepriseDonneesComplementaires') && $this->getCalledFrom() == 'agent')%>"/>
                <!--Fin Bloc Critere de recherche complementaire-->

                <div class="spacer"></div>

                <!--Fin Line bouton recherche multi-criteres-->
                <div class="breaker"></div>
            </div>
            <!--Debut Line bouton recherche multi-criteres-->
            <div class="panel-footer">
                <div class="boutons-line clearfix">
                    <input type="reset"
                           class="bouton-long-190 float-right <%=($this->getCalledFrom() == 'agent')?'':'btn btn-primary btn-sm pull-left'%>" value="<%=Prado::localize('DEFINE_EFFACER_CRITERE_RECHERCHE')%>"
                           title="<%=Prado::localize('DEFINE_EFFACER_CRITERE_RECHERCHE')%>" OnClick = "javascript:window.location.href='?page=<%= Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($_GET['page'])%>';"/>
                    <com:TButton
                            ID ="btSearch"
                            ValidationGroup="sirenValidationGroup"
                            CssClass="bouton-moyen-120 float-right <%=($this->getCalledFrom() == 'agent')?'':'btn btn-primary btn-sm pull-right'%>"
                            Text="<%=Prado::localize('DEFINE_LANCER_RECHERCHE')%>"
                            Attributes.title="<%=Prado::localize('DEFINE_LANCER_RECHERCHE')%>"
                            OnClick="onSearchClick"/>
                </div>
            </div>
        </div>
        <div class="bottom <%=($this->getCalledFrom() == 'agent')?'':'hide'%>"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
    </div>
    <!--Fin Bloc Recherche avancee-->
    <com:TPanel id="retour" visible="<%=($_GET['page'] == 'Agent.ChoixDestinataireBdFournisseur') ? true:false%>" >
        <div class="link-line"><a class="bouton-retour" title="Modifier ma recherche" href="index.php?page=Agent.EnvoiCourrierElectroniqueChoixDestinataire&id=<%= Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($_GET['id'])%>&forInvitationConcourir=<%= Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($_GET['forInvitationConcourir'])%>&IdEchange=<%= Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($_GET['IdEchange'])%>"><com:TTranslate>TEXT_RETOUR</com:TTranslate></a></div>
    </com:TPanel>
</com:TPanel>
