<!--Debut partitionneur-->
<com:TPanel ID="panelNoElementFound">
	<h2><com:TTranslate>DEFINE_AUCUN_RESULTAT</com:TTranslate>&nbsp;</h2>
</com:TPanel>	
<com:TPanel ID="panelMoreThanOneElementFound" Attributes.style="display:">
<div class="line-partitioner">
	<h2><com:TTranslate>DEFINE_NOMBRE_RESULTAT</com:TTranslate>&nbsp;<com:TActiveLabel ID="nombreElement"/></h2>

	<div class="partitioner">
		<div class="intitule"><strong><label for="allCons_nbResultsTop"><com:TTranslate>DEFINE_TEXT_AFFICHER</com:TTranslate></label></strong></div> 
		<com:TDropDownList ID="nombreResultatAfficherTop" AutoPostBack="true" onSelectedIndexChanged="changePagerLenght" Attributes.title='<%=Prado::localize("NOMBRE_DE_RESULTAT_PAR_PAGE")%>'>
  			<com:TListItem Text="10" Value="10"/>
  			<com:TListItem Text="20" Value="20"/>
  			<com:TListItem Text="50" Value="50" />
  			<com:TListItem Text="100" Selected="100" />
  			<com:TListItem Text="500" Value="500" />
  		</com:TDropDownList>
		<div class="intitule"><com:TTranslate>DEFINE_RESULTATS_PAGE</com:TTranslate></div> 
			<label style="display:none;" for="allCons_pageNumberTop">Aller à la page</label>
			<com:TPanel ID="panelBouttonGotoPageTop" DefaultButton="DefaultButtonTop" CssClass="float-left">
				<com:TTextBox ID="numPageTop" Attributes.title='<%=Prado::localize("N_DE_LA_PAGE")%>' Text="1"/>
				<div class="nb-total ">/ <com:TActiveLabel ID="nombrePageTop"/></div>
				<com:TButton ID="DefaultButtonTop"  OnClick="goToPage" Attributes.style="display:none"/>
			</com:TPanel>
		<div class="liens">
			<com:TPager ID="PagerTop"
           			ControlToPaginate="tableauDeBordRepeater"
	           		FirstPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-premier.gif' alt='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>' title='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>' />"
		            LastPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-dernier.gif' alt='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>' title='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>' />"
	     	        Mode="NextPrev"
	          		NextPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-suivant.gif' alt='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>' title='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>' />"
		            PrevPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-precedent.gif' alt='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>' title='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>' />"
   	                OnPageIndexChanged="pageChanged"/>
		</div>
	</div>
</div>
<!--Fin partitionneur-->
<!--Debut Tableau consultations-->
<com:TActivePanel ID="panelRefresh">
<com:TRepeater ID="tableauDeBordRepeater"
	           AllowPaging="true"
			   PageSize="100"
  			   EnableViewState="true"
  			   AllowCustomPaging="true"
  			   OnItemCommand="commandConsultation"
			   >
<prop:HeaderTemplate>
<table summary="Toutes les consultations" class="table-results">
	<caption><com:TTranslate>TEXT_TOUTES_CONSULTATION</com:TTranslate></caption>
	<thead>
		<tr>
			<th class="top" colspan="6"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></th>
		</tr>
		<tr>
			<th class="col-90" id="allCons_ref" abbr="Ref">
					<%#$this->TemplateControl->elementSortedWith('reference_utilisateur','open')%>
						<com:TTranslate>TEXT_REFERENCE</com:TTranslate>
					<%#$this->TemplateControl->elementSortedWith('reference_utilisateur','close')%>
					<com:PictoSort Attributes.onclick="return false;" />
				<br />
				<com:TTranslate>TEXT_PROCEDURE</com:TTranslate>
				<br />
				<com:TTranslate>DEFINE_STATUS</com:TTranslate>
				<br />
				<%#$this->TemplateControl->elementSortedWith('nom_createur','open')%>
					<com:TTranslate>DEFINE_AUTEUR</com:TTranslate>
				<%#$this->TemplateControl->elementSortedWith('nom_createur','close')%>
				<com:PictoSort  Attributes.onclick="return false;"/>
			</th>
			<th class="col-30" id="allCons_details" abbr="Détails">&nbsp;</th>
			<th class="col-300'" id="allCons_intitule" abbr="Intitule">
				<%#$this->TemplateControl->elementSortedWith('intitule','open')%>
					<com:TTranslate>DEFINE_INTITULE</com:TTranslate>
				<%#$this->TemplateControl->elementSortedWith('intitule','close')%>
				<com:PictoSort  Attributes.onclick="return false;"/>
				/
				<%#$this->TemplateControl->elementSortedWith('objet','open')%>
					<com:TTranslate>OBJET</com:TTranslate>
				<%#$this->TemplateControl->elementSortedWith('objet','close')%>
				<com:PictoSort  Attributes.onclick="return false;" />
			</th>
			<th class="col-130" id="allCons_dateEnd" abbr="Date limite">
				<%#$this->TemplateControl->elementSortedWith('dateFin','open')%>
					<com:TTranslate>TEXT_DATE_LIMITE_BR_REMISE_PLIS</com:TTranslate>
				<%#$this->TemplateControl->elementSortedWith('dateFin','close')%>
				<com:PictoSort  Attributes.onclick="return false;"/>
			</th>
			<th class="col-110" id="allCons_registres">
				<com:TTranslate>DEFINE_TEXT_STATISTIQUES_ENTREPRISE</com:TTranslate> 
			</th>
			
			<th class="actions" id="allCons_actions"><com:TTranslate>DEFINE_ACTIONS</com:TTranslate></th>

		</tr>
	</thead>
</prop:HeaderTemplate>
<prop:ItemTemplate>
	<tr class="<%#(($this->ItemIndex%2==0)? '':'on')%>">
		<td class="col-90" headers="allCons_ref">
			<com:TLabel ID="reference" Attributes.class="ref" Text="<%#$this->TemplateControl->insertBrWhenTextTooLong($this->Data->getReferenceUtilisateur(),10)%>"/>
			<br />
			<com:TLabel ID="procedure" Text="<%#$this->Data->getAbreviationTypeProcedure()%>"/>
			<com:TPanel CssClass="consult-etape<%#$this->Data->getStatusConsultation()%>sur4" Attributes.title="<%#$this->TemplateControl->infoBulleEtapeConnsultation(($this->Data instanceof CommonConsultation)? $this->Data->getStatusConsultation():null)%>"></com:TPanel>
			<span class="auteur">
				<%#$this->Data->getNomCreateur()%>
				<br/>
				<%#$this->Data->getPrenomCreateur()%>
			</span>
		</td>
		<td class="col-30" headers="allCons_details">
			<a href="index.php?page=Agent.DetailConsultation&id=<%#$this->Data->getId()%>" >
				<com:PictoDetail ParentPage="ResultSearch" />
			</a>
		</td>
		<td class="col-300" headers="allCons_intitule">
			<div class="objet-line">
				<strong><com:TTranslate>DEFINE_INTITULE</com:TTranslate> :</strong>
				<com:TLabel ID="intitule" Text="<%#$this->TemplateControl->insertBrWhenTextTooLong($this->Data->getTitre(),65)%>"/>
			</div>
			<div class="objet-line">
				<strong><com:TTranslate>OBJET</com:TTranslate> :</strong> 
				<%# Application\Service\Atexo\Atexo_Util::truncateTexte($this->Data->getResume(),'100')%>
				<span style="<%#$this->TemplateControl->isTextTruncated($this->Data->getResume())%>" onmouseover="afficheBulle('ctl0_CONTENU_PAGE_ResultSearch_tableauDeBordRepeater_ctl<%#($this->ItemIndex+1)%>_infosConsultation', this)" onmouseout="cacheBulle('ctl0_CONTENU_PAGE_ResultSearch_tableauDeBordRepeater_ctl<%#($this->ItemIndex+1)%>_infosConsultation')" class="info-suite">...</span>
				<com:TPanel ID="infosConsultation" CssClass="info-bulle" ><div><%#$this->Data->getResume()%></div></com:TPanel>
			    </div>
			</div>  
			<com:TPanel visible="<%#($this->Data->getChampSuppInvisible()!='') ? true:false%>" cssClass="objet-line">
				<strong><com:TTranslate>DEFINE_COMMENTAIRE</com:TTranslate> :</strong>
				<com:TLabel ID="commentaire" Text="<%#$this->TemplateControl->insertBrWhenTextTooLong($this->Data->getChampSuppInvisible(),200)%>"/>
			</com:TPanel>
		</td>
		
		<td class="col-100'" headers="allCons_dateEnd">
			<div class="cloture-line">
				 <com:TPanel Visible="<%#(strcmp($this->Data->getDateFin(), date('Y-m-d H:i:s')) <= 0 )%>" >
			        <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-time-off.gif" alt="" />
			        <span class="time-red">
						<com:TLabel Text="<%#$this->TemplateControl->insertBrWhenTextTooLong(Application\Service\Atexo\Atexo_Util::iso2frnDateTime($this->Data->getDateFin()),10)%>"/>
					</span>
				</com:TPanel>
				<com:TPanel Visible="<%#(strcmp($this->Data->getDateFin(), date('Y-m-d H:i:s')) > 0)%>" >
			        <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-time-on.gif" alt="" />
			        <span class="time-green">
			        	<com:TLabel Text="<%#$this->TemplateControl->insertBrWhenTextTooLong(Application\Service\Atexo\Atexo_Util::iso2frnDateTime($this->Data->getDateFin()),10)%>"/>
					</span>
				</com:TPanel>	
			</div>
		</td>
		
		<td class="col-100" headers="allCons_registres">
			 <%#$this->Data->getNomEntreprise()%>
			 <br />	<%#$this->TemplateControl->pliSupprime($this->Data->getTypeEnv(), $this->Data->getLibelleTypeEnveloppe())%>
		</td>
		
		<td class="actions" headers="allCons_actions">
				<com:TActiveImageButton visible="<%=(Application\Service\Atexo\Atexo_CurrentUser::hasHabilitation('RestaurerEnveloppe'))? true:false%>" ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-restaurer.gif" Attributes.alt="<%=Prado::localize('RESTAURER')%>" Attributes.title="<%=Prado::localize('RESTAURER')%>" OnCallBack="Page.refresfRepeater" OnCommand="Page.restaurerPli" CommandParameter="<%#$this->Data%>" Attributes.onClick="return confirm('<%=Prado::localize('CONFIRMATION_RESTAURATION')%>')"/>
				<com:TActiveImageButton ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-supprimer-big.gif" Attributes.alt="<%=Prado::localize('DEFINE_SUPPRIMER')%>" Attributes.title="<%=Prado::localize('DEFINE_SUPPRIMER')%>"  OnCallBack="Page.refresfRepeater" OnCommand="Page.supprimerPli" CommandParameter="<%#$this->Data%>" Attributes.onClick="return confirm('<%=Prado::localize('CONFIRMATION')%>')" />
		</td>
	</tr>
</prop:ItemTemplate>
 
<prop:FooterTemplate>
</table>
</prop:FooterTemplate>
</com:TRepeater>
<com:TActiveLabel ID="javascript"/>
</com:TActivePanel>
<!--Debut partitionneur-->
<div class="line-partitioner" >
	<div class="partitioner" >
		<div class="intitule"><strong><label for="allCons_nbResultsTop"><com:TTranslate>DEFINE_TEXT_AFFICHER</com:TTranslate></label></strong></div> 
		<com:TDropDownList ID="nombreResultatAfficherBottom" AutoPostBack="true" onSelectedIndexChanged="changePagerLenght" Attributes.title='<%=Prado::localize("NOMBRE_DE_RESULTAT_PAR_PAGE")%>'>
  			<com:TListItem Text="10" Value="10"/>
  			<com:TListItem Text="20" Value="20"/>
  			<com:TListItem Text="50" Value="50" />
  			<com:TListItem Text="100" Selected="100" />
  			<com:TListItem Text="500" Value="500" />
  		</com:TDropDownList>
		<div class="intitule"><com:TTranslate>DEFINE_RESULTATS_PAGE</com:TTranslate></div> 
		<label style="display:none;" for="allCons_pageNumberBottom">Aller à la page</label>
		
		<com:TPanel ID="panelBouttonGotoPageBottom" DefaultButton="DefaultButtonBottom" CssClass="float-left">
			<com:TTextBox ID="numPageBottom" Attributes.title='<%=Prado::localize("N_DE_LA_PAGE")%>' Text="1"/>
			<div class="nb-total ">/ <com:TActiveLabel ID="nombrePageBottom"/></div>
			<com:TButton ID="DefaultButtonBottom"  OnClick="goToPage" Attributes.style="display:none"/>
		</com:TPanel>
		
		<div class="liens" >
			<com:TPager ID="PagerBottom"
           			ControlToPaginate="tableauDeBordRepeater"
	           		FirstPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-premier.gif' alt='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>' title='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>' />"
		            LastPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-dernier.gif' alt='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>' title='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>' />"
	     	        Mode="NextPrev"
	          		NextPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-suivant.gif' alt='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>' title='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>' />"
		            PrevPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-precedent.gif' alt='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>' title='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>' />"
   	                OnPageIndexChanged="pageChanged"/>
							
		</div>
	</div>
</div>
<!--Fin partitionneur-->
<!--Fin Tableau consultations-->
</com:TPanel>