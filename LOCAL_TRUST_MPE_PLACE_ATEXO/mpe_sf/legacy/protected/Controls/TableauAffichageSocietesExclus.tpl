<com:TActivePanel ID="panelRepeater">
    <div class="form-field">
    	<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
    	<div class="content">
    		<h3 class="no-indent"><com:TTranslate>TEXT_SOCIETES_EXCLUES</com:TTranslate></h3>
    		<!--Debut partitionneur-->
    		<div class="line-partitioner">
    			<h2><com:TTranslate>DEFINE_NOMBRE_RESULTATS</com:TTranslate> : <com:TLabel ID="nombreResultats"/></h2>
    			<div class="partitioner">
        			<com:TPanel id="panelPagerTop" visible="true">
        				<div class="intitule"><strong><label for="nbResultsTop"><com:TTranslate>DEFINE_TEXT_AFFICHER</com:TTranslate></label></strong></div> 
        				<com:TDropDownList Attributes.title="Nombre de résultats par page" id="nbResultsTop" AutoPostBack="true" onSelectedIndexChanged="changePagerLenght" >
        					<com:TListItem Text="10" Selected="10"/>
        					<com:TListItem Text="20" Value="20"/>
                  			<com:TListItem Text="50" Value="50" />
                  			<com:TListItem Text="100" Value="100" />
                  			<com:TListItem Text="500" Value="500" />
        				</com:TDropDownList> 
        				<div class="intitule"><com:TTranslate>DEFINE_RESULTATS_PAGE</com:TTranslate></div> 
        				<label style="display:none;" for="pageNumberTop"><com:TTranslate>DEFINE_ALLER_PAGE</com:TTranslate></label>
        				<com:TPanel ID="panelBouttonGotoPageTop" DefaultButton="DefaultButtonTop" CssClass="float-left">
	        				<com:TTextBox Attributes.title="N° de la page" Text="1" id="pageNumberTop" />
	        				<div class="nb-total "><com:TLabel ID="labelSlashTop" Text="/" visible="true"/> <com:TLabel ID="nombrePageTop"/></div>
	        				<com:TButton ID="DefaultButtonTop"  OnClick="goToPage" Attributes.style="display:none"/>
        				</com:TPanel>
        				<div class="liens">
        					<com:TPager
        						ID="PagerTop"
            					ControlToPaginate="tableauSocietesExclues"
            					FirstPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-premier.gif' alt='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>' title='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>' />"
            					PrevPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-precedent.gif' alt='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>' title='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>' />"
            					Mode="NextPrev"
            					NextPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-suivant.gif' alt='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>' title='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>' />"
            					LastPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-dernier.gif' alt='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>' title='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>' />"
            					OnPageIndexChanged="pageChanged"
            				/>
        				</div>
        			</com:TPanel>
    			</div>
    		</div>
    		<!--Fin partitionneur-->
    		<!--Debut Tableau Reponses-->
    	<com:TRepeater ID="tableauSocietesExclues"
           AllowPaging="true"
    	   PageSize="10"
    	   EnableViewState="true"
    	   AllowCustomPaging="true"
    	   OnItemCommand="Page.action"
    	>
    		<prop:HeaderTemplate>
    		<table summary="Résultat de la rechercher" class="table-results">
    
    			<caption><com:TTranslate>TEXT_LISTE_SOCIETES_EXCLUES</com:TTranslate></caption>
    			<thead>
    				<tr>
    					<th class="top" colspan="<%=($this->Page->tableauAffichageSocietesExclues->getCallFromEntreprise())? 6:7%>"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></th>
    				</tr>
    				<tr>
    					<th class="col-100" id="entitePublique"><com:TTranslate>DEFINE_TEXT_ENTITE_PUBLIC</com:TTranslate></th>
    					<th class="col-80" id="registreCommerce"><com:TTranslate>TEXT_REGISTRE_COMMERCE</com:TTranslate></th>
    
    					<th class="col-100" id="libelleFournisseur"><a href="#"><com:TTranslate>TEXT_RAISON_SOCIAL</com:TTranslate><com:TImage visible="false" ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/arrow-tri-off.gif" Attributes.alt="<%=Prado::localize('DEFINE_TRIER')%>" Attributes.title="<%=Prado::localize('DEFINE_TRIER')%>" /></a></th>
    					<th class="col-250" id="motif"><com:TTranslate>TEXT_MOTIF_EXCLUSION</com:TTranslate></th>
    					<th class="col-100" id="dateDebutExclusion">
        					<a href="#">
        						<com:TTranslate>DATE</com:TTranslate><br /><com:TTranslate>DEFINE_DEBUT</com:TTranslate><com:TImage visible="false" ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/arrow-tri-off.gif" Attributes.alt="<%=Prado::localize('DEFINE_TRIER')%>" Attributes.title="<%=Prado::localize('DEFINE_TRIER')%>" />
        					</a>/
        					<a href="#">
        						<com:TTranslate>DEFINE_FIN</com:TTranslate><com:TImage visible="false" ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/arrow-tri-off.gif" Attributes.alt="<%=Prado::localize('DEFINE_TRIER')%>" Attributes.title="<%=Prado::localize('DEFINE_TRIER')%>" />
    						</a><br />
    						<a href="#">
    							<com:TTranslate>TEXT_PORTEE</com:TTranslate><com:TImage visible="false" ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/arrow-tri-off.gif" Attributes.alt="<%=Prado::localize('TEXT_TRIER')%>" Attributes.title="<%=Prado::localize('TEXT_TRIER')%>" />
        					</a>
    					</th>
    					<th class="col-70" id="document"><a href="#"><com:TTranslate>TEXT_DOCUMENT</com:TTranslate><com:TImage visible="false" ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/arrow-tri-off.gif" Attributes.alt="<%=Prado::localize('DEFINE_TRIER')%>" Attributes.title="<%=Prado::localize('DEFINE_TRIER')%>" /></a></th>
    
    					<th class="actions" id="actions" style="display:<%=($this->Page->tableauAffichageSocietesExclues->getCallFromEntreprise())? 'none':'block'%>"><com:TTranslate>DEFINE_ACTIONS</com:TTranslate></th>
    				</tr>
    			</thead>
    		</prop:HeaderTemplate>
    		<prop:ItemTemplate>
    			<tr class="<%#(($this->ItemIndex%2==1)? 'on':'')%>">
    				<td class="col-100" headers="entitePublique"><%#$this->Data->getLibelleOrganismeTraduit()%></td>
    				<td class="col-80" headers="registreCommerce"><div class="objet-line"><%#$this->Data->getIdentifiantEntreprise()%></div></td>
    				<td class="col-100" headers="libelleFournisseur">
    
    					<div class="objet-line"><%#$this->Data->getRaisonSocialeTraduit()%></div>
    				</td>
    				<td class="col-250" headers="motif">
    					<div class="objet-line"><%#$this->Data->getMotifTraduit()%> </div>
    				</td>
    				<td class="col-100" headers="dateDebutExclusion">
						<span title="<%=Prado::localize('ICONE_CALENDRIER')%>">D</span> :
						<com:TLabel CssClass="ltr"><%= Application\Service\Atexo\Atexo_Util::iso2frnDate($this->Data->getDateDebutExclusion())%></com:TLabel>
						<br /><span title="<%=Prado::localize('DEFINE_FIN')%>">F</span> :
						<com:TLabel CssClass="ltr"><%=( Application\Service\Atexo\Atexo_Util::iso2frnDate($this->Data->getDateFinExclusion())) ? Application\Service\Atexo\Atexo_Util::iso2frnDate($this->Data->getDateFinExclusion()) : '-'%></com:TLabel>
						<div class="spacer-mini"></div>
						<%#($this->Data->getTypePortee() == Application\Service\Atexo\Atexo_Config::getParameter('PORTEE_PARTIELLE'))? Prado::localize('TEXT_PORTEE_PARTIELLE'):Prado::localize('TEXT_PORTEE_TOTALE')%>
					</td>
    
    				<td class="col-70" headers="document">
						<div style="display : <%= ($this->Data->getNomDocument() ? 'block':'none') %>">
							<a href="javascript:popUp('?page=Agent.DownloadDocumentSocietesExclues&id=<%=$this->Data->getIdSocietesExclues()%>','yes');">
								<com:TImage ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-dl-dossier.gif" Attributes.alt="<%=Prado::localize('DEFINE_TELECHARGER')%>" Attributes.title="<%=Prado::localize('DEFINE_TELECHARGER')%>" />
							</a>
							<div class="spacer-mini"></div><com:TLabel CssClass="ltr">(.<%= Application\Service\Atexo\Atexo_Util::getExtension($this->Data->getNomDocument())%> <%# Application\Service\Atexo\Atexo_Util::arrondirSizeFile($this->Data->getTailleDocument()/1024)%>)</com:TLabel>
						</div>
					</td>
    				<td class="actions" headers="actions" style="display:<%=($this->Page->tableauAffichageSocietesExclues->getCallFromEntreprise())? 'none':'block'%>">
    					<com:TPanel visible="<%=($this->Page->tableauAffichageSocietesExclues->getCallFromEntreprise())? false:true%>">
        					<a href="index.php?page=Agent.FormulaireSocietesExclues&idSocieteExclue=<%#$this->Data->getIdSocietesExclues()%>">
        						<com:PictoEditImage 
        						activate="<%#(($this->Data->hasHabilitationModifierSupprimerSocietesExclues(Application\Service\Atexo\Atexo_CurrentUser::getOrganismAcronym(), Application\Service\Atexo\Atexo_CurrentUser::getCurrentServiceId(), Application\Service\Atexo\Atexo_CurrentUser::hasHabilitation('ModifierSocietesExclues')) == 2))? false:true%>"
        						clickable ="<%#(($this->Data->hasHabilitationModifierSupprimerSocietesExclues(Application\Service\Atexo\Atexo_CurrentUser::getOrganismAcronym(), Application\Service\Atexo\Atexo_CurrentUser::getCurrentServiceId(), Application\Service\Atexo\Atexo_CurrentUser::hasHabilitation('ModifierSocietesExclues')) == 1))? true:false%>"
        						visible ="<%#(($this->Data->hasHabilitationModifierSupprimerSocietesExclues(Application\Service\Atexo\Atexo_CurrentUser::getOrganismAcronym(), Application\Service\Atexo\Atexo_CurrentUser::getCurrentServiceId(), Application\Service\Atexo\Atexo_CurrentUser::hasHabilitation('ModifierSocietesExclues'))))? true:false%>"
        					/>
        					</a>
        					<a href="#">
        						<com:TActiveImageButton
        							ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-supprimer-big.gif"
        							visible ="<%#(($this->Data->hasHabilitationModifierSupprimerSocietesExclues(Application\Service\Atexo\Atexo_CurrentUser::getOrganismAcronym(), Application\Service\Atexo\Atexo_CurrentUser::getCurrentServiceId(), Application\Service\Atexo\Atexo_CurrentUser::hasHabilitation('SupprimerSocietesExclues')))== 2)? true:false%>"
            						CommandName="Delete" 
            						onCommand="Page.action"   
            						onCallBack="Page.refreshRepeater"         						
            						CommandParameter="<%#$this->Data->getIdSocietesExclues()%>"
            						Attributes.onclick="if(!confirm('<%=Prado::localize('TEXT_CONFIRMATION_SUPRESSION_SOCIETE_EXCLUE')%>'))return false;"
            					/>
            					<com:TActiveImageButton
            						ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-supprimer-big-inactive.gif"
        							Attributes.onclick ="return false;"
        							visible ="<%#(($this->Data->hasHabilitationModifierSupprimerSocietesExclues(Application\Service\Atexo\Atexo_CurrentUser::getOrganismAcronym(), Application\Service\Atexo\Atexo_CurrentUser::getCurrentServiceId(), Application\Service\Atexo\Atexo_CurrentUser::hasHabilitation('SupprimerSocietesExclues')))== 1)? true:false%>"
            						CommandName="Delete" 
            						onCommand="Page.action"   
            						onCallBack="Page.refreshRepeater"         						
            						CommandParameter="<%#$this->Data->getIdSocietesExclues()%>"
            					/>
        					</a>
    					</com:TPanel>					
    				</td>
    			</tr>
    		</prop:ItemTemplate>
    		<prop:FooterTemplate>
    		</table>
    		</prop:FooterTemplate>
    	</com:TRepeater>
    		<!--Fin Tableau Reponses-->
    		<!--Debut partitionneur-->
    		<div class="line-partitioner">
        		<com:TPanel id="panelPagerBottom" visible="true">
        			<div class="partitioner">
        				<div class="intitule"><strong><label for="nbResultsBottom"><com:TTranslate>DEFINE_TEXT_AFFICHER</com:TTranslate></label></strong></div> 
        				<com:TDropDownList Attributes.title="Nombre de résultats par page" id="nbResultsBottom" AutoPostBack="true" onSelectedIndexChanged="changePagerLenght" >
        					<com:TListItem Text="10" Selected="10"/>
                  			<com:TListItem Text="20" Value="20"/>
                  			<com:TListItem Text="50" Value="50" />
                  			<com:TListItem Text="100" Value="100" />
                  			<com:TListItem Text="500" Value="500" />
        				</com:TDropDownList> 
        				<div class="intitule"><com:TTranslate>DEFINE_RESULTATS_PAGE</com:TTranslate></div> 
        				<label style="display:none;" for="pageNumberBottom"><com:TTranslate>DEFINE_ALLER_PAGE</com:TTranslate></label>
        				<com:TPanel ID="panelBouttonGotoPageBottom" DefaultButton="DefaultButtonBottom" CssClass="float-left">
	        				<com:TTextBox Attributes.title="N° de la page" Text="1" id="pageNumberBottom" />
	        				<div class="nb-total"><com:TLabel ID="labelSlashBottom" Text="/" visible="true"/> <com:TLabel ID="nombrePageBottom"/></div>
	        				<com:TButton ID="DefaultButtonBottom"  OnClick="goToPage" Attributes.style="display:none"/>
						</com:TPanel>
        				
        				<div class="liens">
        					<com:TPager
        						ID="PagerBottom"
            					ControlToPaginate="tableauSocietesExclues"
            					FirstPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-premier.gif' alt='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>' title='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>' />"
            					PrevPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-precedent.gif' alt='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>' title='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>' />"
            					Mode="NextPrev"
            					NextPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-suivant.gif' alt='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>' title='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>' />"
            					LastPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-dernier.gif' alt='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>' title='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>' />"
            					OnPageIndexChanged="pageChanged"
            				/>
        				</div>
        			</div>
        		</com:TPanel>
    		</div>
    		<!--Fin partitionneur-->
    		<!--Fin Tableau Réponses au format électronique-->
    		<div class="breaker"></div>
    	</div>
    	<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
    </div>
</com:TActivePanel>
<div class="link-line">
	<com:THyperLink 
    	NavigateUrl="<%=($this->Page->tableauRechercheSocietesExclues->getCallFromEntreprise())? '?page=Entreprise.EntrepriseRechercherSocietesExclues&search=1':'?page=Agent.AgentRechercherSocietesExclues&search=1'%>"
    	Text="<%=Prado::localize('TEXT_RETOUR')%>" Cssclass="bouton-retour" Attributes.title="Retour à la recherche">
	</com:THyperLink>
</div>

<div class="breaker"></div>