<com:TActivePanel id="panelListePiecesTypees">
<com:TRepeater ID="listePiecesTypees" EnableViewState="true" DataKeyField="Identifiant">
<prop:HeaderTemplate>
	<table summary="Liste des pièces - Dossier d'offre" class="table-results tableau-reponse margin-0">
    	<tbody>
	</prop:HeaderTemplate>
    <prop:ItemTemplate>
		<tr class="<%#$this->TemplateControl->getIdHtlmToggleFormulaire()%>">
    		<td class="check-col">
    			<com:TActiveCheckBox 
    				id="idCheckBoxSelectionPiece" 
    				Attributes.title="<%#Prado::localize('DEFINE_SELECTIONNER_PIECE')%>"
	 				CssClass="check"
	 				visible="<%#($this->SourceTemplateControl->getVisibiliteCocherSignature($this->Data) && $this->Data->getFichierAeAjoute()&& ($this->Page->getConsultation() && $this->Page->getConsultation()->getSignatureOffre() == 1))? true:false%>"
	 			/>
    		</td>
            <td class="col-piece ellipsis">
    			<com:TPanel cssClass="blue piece-type">
    				<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-piece-typee.png" title="<%#Prado::localize('DEFINE_PIECE_TYPEE')%>" alt="<%#Prado::localize('DEFINE_PIECE_TYPEE')%>"/>
    				<span class="intitule-piece"><com:TTranslate>DEFINE_DC3_ACTE_ENGAGEMENT</com:TTranslate> : </span>
    				<img style="display:<%#(!$this->Data->getFichierAeAjoute())? '':'none'%>" alt="<%#Prado::localize('DEFINE_AUCUN_FICHIER_RENSEIGNE')%>" class="picto-piece" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-piece-non-renseignee.png"/>
    				<abbr 
    					style="display:<%#(!Application\Service\Atexo\Atexo_Module::isEnabled('GenererActeDengagement') && $this->Data->getFichierAeAjoute())? '':'none'%>"
    					title="<%#Prado::localize('DEFINE_CHEMIN_COMPLET')%> : <%# $this->toHttpEncoding($this->Data->getCheminFichier())%>"
    				> 
    					<%# $this->toHttpEncoding($this->SourceTemplateControl->getNomFichier($this->Data->getCheminFichier()))%>
    				</abbr>
    			</com:TPanel>
				<com:TPanel Visible="<%#Application\Service\Atexo\Atexo_Module::isEnabled('GenererActeDengagement') && $this->Data->getFichierAeAjoute()%>">
        			<div class="blue piece-type type-AE">
        				<span class="intitule-piece"><com:TTranslate>DEFINE_PDF</com:TTranslate> : </span>
        				<abbr title="<%#Prado::localize('DEFINE_CHEMIN_COMPLET')%> : <%# $this->toHttpEncoding($this->Data->getCheminFichier())%>">
        					<%# $this->toHttpEncoding($this->SourceTemplateControl->getNomFichier($this->Data->getCheminFichier()))%>
        				</abbr>
        			</div>
        			<div class="blue piece-type type-AE">
        				<span class="intitule-piece"><com:TTranslate>DEFINE_XML</com:TTranslate> : </span>
        				<abbr title="<%#Prado::localize('DEFINE_CHEMIN_COMPLET')%> : <%# $this->toHttpEncoding($this->Data->getCheminFichierXmlAE())%>">
        					<%# $this->toHttpEncoding($this->SourceTemplateControl->getNomFichier($this->Data->getCheminFichierXmlAE()))%>
        				</abbr>
        			</div>
    			</com:TPanel>
    		</td>
    		<td colspan="2" class="col-180 ellipsis">
				<com:TPanel cssClass="statut-signature" Visible="<%#!Application\Service\Atexo\Atexo_Module::isEnabled('GenererActeDengagement') && $this->Data->getFichierAeAjoute() && ($this->Page->getConsultation() && $this->Page->getConsultation()->getSignatureOffre() == 1)%>">
					<div class="statut">
						<img 
							src="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/<%#$this->SourceTemplateControl->getPictoSignatureExiste($this->Data)%>"
							alt="<%#$this->SourceTemplateControl->getAltSignatureExiste($this->Data)%>"
							title="<%#$this->SourceTemplateControl->getAltSignatureExiste($this->Data)%>"
						/> :
						<img 
							src="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/<%#$this->SourceTemplateControl->getPictoVerifSignature($this->Data)%>"
							alt="<%#$this->SourceTemplateControl->getAltVerifSignature($this->Data)%>"
							title="<%#$this->SourceTemplateControl->getAltVerifSignature($this->Data)%>"
							style="<%#$this->SourceTemplateControl->getHasFileSignature($this->Data) ? '' : 'display:none'%>"
						/>
						<com:TLabel style="<%# $this->SourceTemplateControl->getHasFileSignature($this->Data) ? 'display:none' : ''%>" > &nbsp;&nbsp;&ndash;&nbsp;&nbsp;&nbsp; </com:TLabel>
					</div>
					<div class="detail-statut" style="display:<%#($this->SourceTemplateControl->afficherLienSignature($this->Data))?'none':''%>">
						<%#$this->SourceTemplateControl->getInfosSignataire($this->Data)%>
					</div>
					<com:TActiveLinkButton 
						style="display:<%#($this->SourceTemplateControl->afficherLienSignature($this->Data))?'':'none'%>"
						Attributes.class="detail-link" 
						Attributes.title="<%#Prado::localize('DEFINE_VOIR_DETAILS_SIGNATURE')%>"
						onCallBack="SourceTemplateControl.afficherDetailsSignature"
						ActiveControl.CallbackParameter="<%#$this->ItemIndex%>"
					>
						<%#$this->SourceTemplateControl->getInfosSignataire($this->Data)%>
					</com:TActiveLinkButton>
				</com:TPanel>
				<com:TPanel Visible="<%#Application\Service\Atexo\Atexo_Module::isEnabled('GenererActeDengagement') && $this->Data->getFichierAeAjoute() && ($this->Page->getConsultation()->getSignatureOffre() == 1)%>">
    				<div class="statut-signature statut-AE">
        				<div class="statut">
        					<img 
    							src="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/<%#$this->SourceTemplateControl->getPictoSignatureExiste($this->Data)%>"
    							alt="<%#$this->SourceTemplateControl->getAltSignatureExiste($this->Data)%>"
    							title="<%#$this->SourceTemplateControl->getAltSignatureExiste($this->Data)%>"
    						/> :
    						<img 
    							src="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/<%#$this->SourceTemplateControl->getPictoVerifSignature($this->Data)%>"
    							alt="<%#$this->SourceTemplateControl->getAltVerifSignature($this->Data)%>"
    							title="<%#$this->SourceTemplateControl->getAltVerifSignature($this->Data)%>"
    						/>
        				</div>
        				<div class="detail-statut" style="display:<%#($this->SourceTemplateControl->afficherLienSignature($this->Data))?'none':''%>">
        					<%#$this->SourceTemplateControl->getInfosSignataire($this->Data)%>
        				</div>
        				<com:TActiveLinkButton 
    						style="display:<%#($this->SourceTemplateControl->afficherLienSignature($this->Data))?'':'none'%>"
    						Attributes.class="detail-link" 
    						Attributes.title="<%#Prado::localize('DEFINE_VOIR_DETAILS_SIGNATURE')%>"
    						onCallBack="SourceTemplateControl.afficherDetailsSignature"
    						ActiveControl.CallbackParameter="<%#$this->ItemIndex%>"
    					>
    						<%#$this->SourceTemplateControl->getInfosSignataire($this->Data)%>
    					</com:TActiveLinkButton>
        			</div>
        			<div class="statut-signature clear-both">
        				<div class="statut">
        					<img 
    							src="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/<%#$this->SourceTemplateControl->getPictoSignatureExiste($this->Data)%>"
    							alt="<%#$this->SourceTemplateControl->getAltSignatureExiste($this->Data)%>"
    							title="<%#$this->SourceTemplateControl->getAltSignatureExiste($this->Data)%>"
    						/> :
    						<img 
    							src="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/<%#$this->SourceTemplateControl->getPictoVerifSignature($this->Data)%>"
    							alt="<%#$this->SourceTemplateControl->getAltVerifSignature($this->Data)%>"
    							title="<%#$this->SourceTemplateControl->getAltVerifSignature($this->Data)%>"
    						/>
        				</div>
        				<div class="detail-statut" style="display:<%#($this->SourceTemplateControl->afficherLienSignature($this->Data))?'none':''%>">
        					<%#$this->SourceTemplateControl->getInfosSignataire($this->Data)%>
        				</div>
        				<com:TActiveLinkButton 
    						style="display:<%#($this->SourceTemplateControl->afficherLienSignature($this->Data))?'':'none'%>"
    						Attributes.class="detail-link" 
    						Attributes.title="<%#Prado::localize('DEFINE_VOIR_DETAILS_SIGNATURE')%>"
    						onCallBack="SourceTemplateControl.afficherDetailsSignature"
    						ActiveControl.CallbackParameter="<%#$this->ItemIndex%>"
    					>
    						<%#$this->SourceTemplateControl->getInfosSignataire($this->Data)%>
    					</com:TActiveLinkButton>
        			</div>
    			</com:TPanel>
			</td>
    		<td class="actions">
    			<com:TActiveLinkButton 
    				Attributes.title="<%#Prado::localize('DEFINE_AJOUTER_PIECE_ATTENDUE')%>"
    				Visible="<%#!Application\Service\Atexo\Atexo_Module::isEnabled('GenererActeDengagement') && !$this->Data->getFichierAeAjoute()%>"
    				attributes.onClick="initialiserAppletSelectionFichierReponseAe('<%#$this->getClientId()%>');return false;"
    			>
    				<img alt="<%#Prado::localize('DEFINE_AJOUTER_PIECE_ATTENDUE')%>" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-ajout-doc.gif">
    			</com:TActiveLinkButton>
    			<com:TActiveLinkButton 
    				Attributes.title="<%#Prado::localize('GENERER_AE')%>"
    				Visible="<%#Application\Service\Atexo\Atexo_Module::isEnabled('GenererActeDengagement') && !$this->Data->getFichierAeAjoute()%>"
    				attributes.onClick="document.getElementById('idAEXmlCalled').value = '<%#$this->xmlStrAE->ClientId%>';
    									javascript:popUp('index.php?page=Entreprise.FormulaireActeDEngagement','yes');"
    			>
    				<img alt="<%#Prado::localize('GENERER_AE')%>" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-ajout-doc.gif"/>
    			</com:TActiveLinkButton>
    			<com:TPanel Visible="<%#Application\Service\Atexo\Atexo_Module::isEnabled('GenererActeDengagement')%>">
        			<com:THiddenField id="xmlStrAE" value="<%#$this->TemplateControl->getXmlAE($this->TemplateControl->getNumLot())%>"/>
        			<com:THiddenField ID="xmlAE" />
        			<com:THiddenField ID="fichierAE" />
        			<com:TButton 
    					id="launchAppletAEButton"
    					Attributes.OnClick="initialiserGenerationActeEngagementApplet('<%#$this->fichierAE->ClientId%>'
    																	, '<%#$this->xmlAE->ClientId%>'
    																	, '<%#$this->xmlStrAE->ClientId%>'
    																	, '<%#Application\Service\Atexo\Atexo_ActeDEngagement::getUrlDownloadLogoAe()%>'
    																	, '<%#$this->TemplateControl->getNumLot()%>');
    																	  return false;"
    					CssClass="link-line"
    					style="display:none;"
    					>
    				</com:TButton>
    			</com:TPanel>
    			<com:TActiveLinkButton 
    				Attributes.title="<%#Prado::localize('DEFINE_RETIRER_ACTE_ENGAGEMENT')%>"
    				Visible="<%#$this->Data->getFichierAeAjoute()%>"
    				onCallBack="SourceTemplateControl.retirerActeEngagement"
				    ActiveControl.CallbackParameter="<%#$this->Data->getIdentifiant()%>"
    			>
    				<img alt="<%#Prado::localize('DEFINE_RETIRER_ACTE_ENGAGEMENT')%>" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-supprimer-big.gif"/>
    			</com:TActiveLinkButton>
    			<div style="display:none">
            		<com:TActiveLinkButton  
            			id="ajouterPiecesSelectionnees"
            			onCallBack="SourceTemplateControl.ajouterPieceTypee"
            			ActiveControl.CallbackParameter="<%#$this->Data->getIdentifiant()%>"
            		>
            			<prop:ClientSide.OnLoading>showLoader();</prop:ClientSide.OnLoading>
            			<prop:ClientSide.OnFailure>hideLoader();</prop:ClientSide.OnFailure>
            			<prop:ClientSide.OnSuccess>hideLoader();</prop:ClientSide.OnSuccess>
            		</com:TActiveLinkButton>
            		<com:TActiveHiddenField id="valueIdentifiantPiece" value="<%#$this->Data->getIdentifiant()%>"/>
            		<com:TActiveHiddenField id="listePiecesSelectionnees" Value="0"/>
            		<com:TActiveHiddenField id="listeJsonRetoursAppletPiecesSelectionnees"/>
            	</div>
    		</td>
		</tr>
	</prop:ItemTemplate>
    <prop:FooterTemplate>
	
		</tbody>
	</table>
    </prop:FooterTemplate>
</com:TRepeater>
</com:TActivePanel> 
<input type="hidden" id="idAEXmlCalled" name="idAEXmlCalled"/>
<input type="hidden" id="urlCondUsr" name="urlCondUsr" />
<input type="hidden" id="variantesAutorisees" name="urlCondUsr" />
<com:THiddenField id="logAppletAjoutAE" />
<com:TActiveButton 
		ID="boutonlogAppletAjoutAE"   
		Attributes.style="display:none" 
		onCallBack="traceHistoriqueNavigationInscrit"
/>