	<com:TActivePanel ID="panelDomaineActivite" cssClass="content-bloc">	
			<com:TTextBox TextMode="MultiLine" Rows="10" Display="None" ID="idsDomaines" />
			 <com:TActiveButton Display="None" ID="displayDomaine" OnCallBack="onCallBackDomaineActivites" />
		<com:TActivePanel ID="panelLibelleDomaine" display="None">
			<com:TActiveLabel ID="trunLibelleDomaines" />
			<span
				onmouseover="afficheBulle('infosDomaines', this)" 
				onmouseout="cacheBulle('infosDomaines')" 
				class="info-suite"> <com:TActiveLabel Display="None" ID="toisPointsInfoBulle" Text ="<%=Prado::localize('DEFINE_SUITE')%>"/>
			</span>
			<div class="spacer-mini"></div>
		</com:TActivePanel>
		<com:THyperLink 
			ID="linkDisplay"
			Attributes.title="<%=Prado::localize('DEFINIR')%> <%=Prado::localize('TEXT_DOMAINES_ACTIVITE')%> <%=Prado::localize('NOUVELLE_FENETRE')%>"
			NavigateUrl="javascript:popUp('index.php?page=<%= Application\Service\Atexo\Atexo_Util::getTypeUserCalledForPradoPages() %>.PopupDomainesActivites&ids=<%=base64_encode($this->idsDomaines->Text)%>&clientId=<%=$this->getClientId()%><%=($this->getAllLevel()=='true') ? '&AllLevel':''%>','yes');"
			cssClass="bouton-small">
			<com:TTranslate>DEFINIR</com:TTranslate>
		</com:THyperLink>
		<span id="spanDomainesObligatoire" style="display:none;" title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>
		<div ID="infosDomaines" class="info-bulle" >
			<div><com:TActiveLabel ID="libellesDomaines"/></div>
		</div>
	</com:TActivePanel>
	
    <com:TCustomValidator
      	Enabled = "true"
    	ID="validatorDomainesActivites"
    	ValidationGroup="validateCreateCompte"
    	ControlToValidate="idsDomaines"
    	Display="Dynamic"
    	ErrorMessage="<%=Prado::localize('TEXT_DOMAINES_ACTIVITE')%>"
    	ClientValidationFunction="validateDomainesActivites"
    	Text="<span title='Champ obligatoire' class='check-invalide'></span>" >
    	<prop:ClientSide.OnValidationError>
    		document.getElementById('divValidationSummary').style.display='';
    	</prop:ClientSide.OnValidationError>
    </com:TCustomValidator>