<?php

namespace Application\Controls;

use Prado\Prado;
use Prado\Web\UI\ActiveControls\TActiveImageButton;

/**
 * commentaires.
 *
 * @author mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class ActivePictoDelete extends TActiveImageButton
{
    public string $_activate = 'false';

    public function onLoad($param)
    {
        /* ********************** *
        * A suprimer lié au merge *
         *  *************************** *
        if($this->_activate=="false") {
            $this->setImageUrl(t()."/images/picto-supprimer-big-inactive.gif");
            //le "return false" empeche le contrôle de faire un callback, cela peut être considéré comme un extends TImage (pas de rafraichissement)
            $this->Attributes->onclick="return false";
        } else {
            $this->setImageUrl(t()."/images/picto-supprimer-big.gif");
            $this->Attributes->onclick="if(!confirm('".Prado::localize('TEXT_ALERTE_SUPPRESSION_USER')."'))return false;";
        }

        $this->setAlternateText(Prado::localize("ICONE_SUPPRIMER_DE_MA_SELECTION"));
        $this->setToolTip(Prado::localize("ICONE_SUPPRIMER_DE_MA_SELECTION"));
        * ************ */
    }

    public function getActivate()
    {
        return $this->_activate;
    }

    public function setActivate($bool)
    {
        $this->_activate = $bool;
    }
}
