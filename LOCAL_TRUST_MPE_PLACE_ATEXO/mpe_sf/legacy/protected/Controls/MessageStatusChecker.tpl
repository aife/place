<div>
    <div id="assistance-status-checker"
         data-translations='<%=$this->getTranslations()%>'
         data-path_modal="javascript:openModal('demander-validation','popup-moyen1',document.getElementById('panelValidationNt'), '<%=$this->title%>')"
         >
        <assistance-status-checker></assistance-status-checker>
    </div>
</div>

<p></p>

<!--Debut Modal validation-->
<com:TActivePanel id="panelValidationNt" cssclass="demander-validation" style="display:none;">
    <div class="panel panel-default">
        <div class=" panel-body">
            <div class="alert alert-warning  bloc-message form-bloc-conf msg-info" style="display: block;">
                <div class="alert-content">
                    <com:TTranslate>TEXT_MESSAGE_INFO_ASSISTANT_MARCHES_PUBLICS</com:TTranslate>
                </div>
            </div>
            <div class="form-bloc" id="recap-consultation">
                <div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
                <div class="content">
                    <a class="title-toggle" onclick="togglePanel(this,'cas1');" href="javascript:void(0);">&nbsp;</a>
                    <span style="font-size: 1.1em; font-weight: bold"><com:TTranslate>TEXT_CAS_1_MON_ASSISTANT_MARCHES_PUBLICS</com:TTranslate></span>
                    <div class="">
                        <div id="cas1" style="display:none;" class="">
                        <br>
                            <div class="line" style="margin-left:15px">
                                <com:TTranslate>TEXT_CAS_1_TELECHARGER_APPLICATION</com:TTranslate>
                                <div class="">
                                    <div class="col-md-11">
                                        <div class="col-md-12">
                                            <div class="col-md-12">
                                                <ul class="list-unstyled">
                                                    <li>
                                                        <i class="la la-windows"></i>&nbsp;&nbsp;&nbsp;
                                                        <com:TConditional condition="Application\Service\Atexo\Atexo_Config::getParameter('SPECIFIQUE_PLACE')">
                                                            <prop:trueTemplate>
                                                                <a href="<%=Application\Service\Atexo\Atexo_Config::getParameter('URL_DOCS')%><%=Application\Service\Atexo\Atexo_Config::getParameter('LIEN_TELECHARGEMENT_CLIENT_LOURD_WINDOWS')%>">
                                                                    <com:TTranslate>TEXT_CAS_1_TELECHARGER_WINDOWS
                                                                    </com:TTranslate>
                                                                </a>
                                                            </prop:trueTemplate>
                                                            <prop:falseTemplate>
                                                                <a href="<%=Application\Service\Atexo\Atexo_Config::getParameter('URL_CENTRALE_REFERENTIELS')%><%=Application\Service\Atexo\Atexo_Config::getParameter('LIEN_TELECHARGEMENT_CLIENT_LOURD_WINDOWS')%>">
                                                                    <com:TTranslate>TEXT_CAS_1_TELECHARGER_WINDOWS
                                                                    </com:TTranslate>
                                                                </a>
                                                            </prop:falseTemplate>
                                                        </com:TConditional>
                                                    </li>
                                                    <li>
                                                        <i class="la la-apple"></i>&nbsp;&nbsp;&nbsp;

                                                        <com:TConditional condition="Application\Service\Atexo\Atexo_Config::getParameter('SPECIFIQUE_PLACE')">
                                                            <prop:trueTemplate>
                                                                <a href="<%=Application\Service\Atexo\Atexo_Config::getParameter('URL_DOCS')%><%=Application\Service\Atexo\Atexo_Config::getParameter('LIEN_TELECHARGEMENT_CLIENT_LOURD_MACOS')%>">
                                                                    <com:TTranslate>TEXT_CAS_1_TELECHARGER_MACOS
                                                                    </com:TTranslate>
                                                                </a>
                                                            </prop:trueTemplate>
                                                            <prop:falseTemplate>
                                                                <a href="<%=Application\Service\Atexo\Atexo_Config::getParameter('URL_CENTRALE_REFERENTIELS')%><%=Application\Service\Atexo\Atexo_Config::getParameter('LIEN_TELECHARGEMENT_CLIENT_LOURD_MACOS')%>">
                                                                    <com:TTranslate>TEXT_CAS_1_TELECHARGER_MACOS
                                                                    </com:TTranslate>
                                                                </a>
                                                            </prop:falseTemplate>
                                                        </com:TConditional>

                                                    </li>
                                                    <li>
                                                        <i class="la la-linux"></i>&nbsp;&nbsp;&nbsp;
                                                        <com:TConditional condition="Application\Service\Atexo\Atexo_Config::getParameter('SPECIFIQUE_PLACE')">
                                                            <prop:trueTemplate>
                                                                <a href="<%=Application\Service\Atexo\Atexo_Config::getParameter('URL_DOCS')%><%=Application\Service\Atexo\Atexo_Config::getParameter('LIEN_TELECHARGEMENT_CLIENT_LOURD_LINUX')%>">
                                                                    <com:TTranslate>TEXT_CAS_1_TELECHARGER_LINUX
                                                                    </com:TTranslate>
                                                                </a>
                                                            </prop:trueTemplate>
                                                            <prop:falseTemplate>
                                                                <a href="<%=Application\Service\Atexo\Atexo_Config::getParameter('URL_CENTRALE_REFERENTIELS')%><%=Application\Service\Atexo\Atexo_Config::getParameter('LIEN_TELECHARGEMENT_CLIENT_LOURD_LINUX')%>">
                                                                    <com:TTranslate>TEXT_CAS_1_TELECHARGER_LINUX
                                                                    </com:TTranslate>
                                                                </a>
                                                            </prop:falseTemplate>
                                                        </com:TConditional>

                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <p style="line-height: 1.5; margin-top: 10px;">
                                            <com:TTranslate>TEXT_CAS_1_RESUME</com:TTranslate>
                                            <span class="badge badge-success"><com:TTranslate>CONNECTE</com:TTranslate></span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="breaker"></div>
                </div>
                <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
            </div>
            <div class="form-bloc" id="recap-consultation">
                <div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
                <div class="content">
                    <a class="title-toggle" onclick="togglePanel(this,'cas2');" href="javascript:void(0);">&nbsp;</a>
                    <span style="font-size: 1.1em; font-weight: bold"><com:TTranslate>TEXT_CAS_2_MON_ASSISTANT_MARCHES_PUBLICS</com:TTranslate></span>
                    <div class="">
                        <div id="cas2" style="display:none;" class="">
                        <br>
                            <div class="line" style="margin-left:15px">
                                <div class="col-md-12">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <com:TTranslate>TEXT_CAS_2_RESUME_1</com:TTranslate>
                                            <span class="badge badge-danger"><com:TTranslate>NON_CONNECTE</com:TTranslate></span>
                                            <com:TTranslate>TEXT_CAS_2_RESUME_2</com:TTranslate>
                                        </div>
                                    </div>
                                    <div class="row">&nbsp;</div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <com:TTranslate>TEXT_CAS_2_RESUME_3</com:TTranslate>
                                            <span class="badge badge-success"><com:TTranslate>CONNECTE</com:TTranslate></span>
                                        </div>
                                    </div>
                                    <div class="row">&nbsp;</div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <com:TTranslate>TEXT_CAS_2_RESUME_4</com:TTranslate>
                                            <span class="badge badge-danger"><com:TTranslate>NON_CONNECTE</com:TTranslate></span>
                                            <com:TTranslate>TEXT_CAS_2_RESUME_5</com:TTranslate>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="breaker"></div>
                </div>
                <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
            </div>
        </div>
    </div>
    <!--Debut line boutons-->
    <div class="boutons-line">
        <input type="button"
               class="bouton-moyen float-left"
               value="<%=Prado::localize('FERMER')%>"
               title="<%=Prado::localize('FERMER')%>"
               onclick="J('.demander-validation').dialog('close');return false;"
        />
    </div>
    <!--Fin line boutons-->
</com:TActivePanel>
<!--Fin Modal validation-->
