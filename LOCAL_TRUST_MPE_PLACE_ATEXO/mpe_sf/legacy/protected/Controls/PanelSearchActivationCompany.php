<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_CommonReferentiel;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_CriteriaVo;
use Prado\Prado;

/**
 * Formulaire de recherche avancee.
 *
 * @author mouslim mitali <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PanelSearchActivationCompany extends MpeTPage
{
    public function onLoad($param)
    {
        $this->customizeForm();
    }

    /**
     * Fonction qui recupere les choix effectues dans la partie 'Recherche multi-criteres'
     * pour les mettre dans un objet CriteriaVo puis affiche le resultat
     * dans le tableau de bord.
     */
    public function onSearchClick($sender, $param)
    {
        $criteriaVo = new Atexo_Entreprise_CriteriaVo();

        if ('' != $this->raisonSociale->Text) {
            $criteriaVo->setRaisonSociale($this->raisonSociale->Text);
        }

        if (true == $this->entrepriseLocale->Checked) {
            if (Atexo_Module::isEnabled('CompteEntrepriseRcville')) {
                if ('0' != $this->villeRc->SelectedValue || '' != $this->villeRc->SelectedValue) {
                    if ('0' == $this->villeRc->SelectedValue) {
                        $villeRc = '';
                    } else {
                        $villeRc = $this->villeRc->SelectedValue;
                    }
                    $criteriaVo->setSiren($villeRc.Atexo_Config::getParameter('SEPARATEUR_VILLE_RC').$this->numeroRc->Text);
                }
            } elseif (Atexo_Module::isEnabled('CompteEntrepriseIdentifiantUnique')) {
                if ('' != $this->identifiantUnique->Text) {
                    $criteriaVo->setSiren($this->identifiantUnique->Text);
                }
            } else {
                if ('' != $this->ifu->Text) {
                    $criteriaVo->setSiren($this->ifu->Text);
                }
            }
        } elseif (true == $this->etranger->Checked) {
            $criteriaVo->setPays($this->listPays->getSelectedItem()->getText());
            $criteriaVo->setIdNational($this->idNational->Text);
        }

        if ('' != $this->dateEnd->Text) {
            $criteriaVo->setDateCreationEnd($this->dateEnd->Text);
        }
        if ('' != $this->dateStart->Text) {
            $criteriaVo->setDateCreationStart($this->dateStart->Text);
        }
        // on met 0 pour recuperer toutes les entreprises activees ou pas
        $criteriaVo->setCompteActif('0');
        // print_r($criteriaVo);exit;
        $this->Page->setViewState('CriteriaVo', $criteriaVo);
        $this->Page->dataForSearchResult($criteriaVo);
    }

    /**
     * retourner vers la page de recherche avance.
     */
    public function displayPanelSearch()
    {
        $this->response->redirect('index.php?page=Agent.SearchActivationCompany');
    }

    /**
     * afficher le formulaire de la recherche
     * avance avec les criteres deja pre-rempli.
     */
    public function displayCriteria($sender, $param)
    {
        $this->Page->panelSearch->setVisible(true);
        $this->Page->panelResultSerch->setVisible(false);
    }

    public function displayVilleRc()
    {
        $valeur = (new Atexo_CommonReferentiel())->retrieveValeurReferentiel(Atexo_Config::getParameter('VALEUR_REFERENTIEL_VILLE_RC'));
        $data = [];
        $data['0'] = Prado::localize('TEXT_SELECTIONNER').' ...';
        $data['99'] = Prado::localize('PAS_DE_RC');

        if ($valeur) {
            foreach ($valeur as $oneVal) {
                $data[$oneVal->getLibelle2()] = $oneVal->getLibelleValeurReferentiel();
            }
        }
        $this->villeRc->DataSource = $data;
        $this->villeRc->DataBind();
    }

    /**
     * Adapter le formulaire pour un affichage qui correspond a MPE TGR.
     */
    public function customizeForm()
    {
        if (Atexo_Module::isEnabled('CompteEntrepriseRcville')) {
            $this->panelSiren->setVisible(false);
            $this->panelRc->setVisible(true);
            $this->panelIdentifiantUnique->setVisible(false);
            if (!$this->IsPostBack) {
                $this->displayVilleRc();
            }
        } elseif (Atexo_Module::isEnabled('CompteEntrepriseIdentifiantUnique')) {
            $this->panelIdentifiantUnique->setVisible(true);
            $this->panelSiren->setVisible(false);
            $this->panelRc->setVisible(false);
        } else {
            $this->panelIdentifiantUnique->setVisible(false);
            $this->panelSiren->setVisible(true);
            $this->panelRc->setVisible(false);
        }
    }
}
