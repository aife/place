<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Controller\Atexo_Controller_Front;

/**
 * Le composant d'affichage de la bar d'aide.
 *
 * @author LEZ <loubna.ezziani@atexo.com>
 *
 * @version 1.0
 *
 * @since 4.9.0
 *
 * @copyright Atexo 2014
 */
class AtexoHelpBar extends MpeTPage
{
    protected $_calledFrom = '';

    /**
     * Set the value of [_calledFrom] column.
     *
     * @param string $v new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2014
     */
    public function setCalledFrom($value)
    {
        $this->_calledFrom = $value;
    }

    /**
     * Get the value of [_calledFrom] column.
     *
     * @return string [_calledFrom]
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2014
     */
    public function getCalledFrom()
    {
        return $this->_calledFrom;
    }

    /**
     * Permet de retourner l'url de l'assistance Telephonique.
     *
     * @return string
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2014
     */
    public function getUrlAssistanceTelephonique()
    {
        $calledFrom = $this->getCalledFrom();
        if ('agent' == $calledFrom && Atexo_CurrentUser::isConnected()) {
            return Atexo_Controller_Front::urlPf().'?page=Agent.AgentAide';
        } elseif ('entreprise' == $calledFrom) {
            return Atexo_Config::getParameter('URL_LT_MPE_SF').'/entreprise/aide/assistance-telephonique';
        }

        return '';
    }

    /**
     * Permet de retourner l'url du Foire aux questions.
     *
     * @return Stringl'url du Foire aux questions
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2014
     */
    public function getUrlFaq()
    {
        $calledFrom = $this->getCalledFrom();
        if ('agent' == $calledFrom) {
            return Atexo_Controller_Front::urlPf().'?page=Agent.AgentFaq';
        } elseif ('entreprise' == $calledFrom) {
            return Atexo_Controller_Front::urlPf().'?page=Entreprise.EntrepriseFaq';
        }

        return '';
    }

    public function getVisibleLienAssistance()
    {
        $var = explode('.', $_GET['page']);

        $calledFrom = $var[0];

        if ('agent' == $calledFrom) {
            return false;
        }

        return true;
    }

    /**
     * Permet de retourner le numero du telephone de l'assistance.
     *
     * @return string
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since ESR-2015
     *
     * @copyright Atexo 2015
     */
    public function getNumeroAssistanceTelephonique()
    {
        $calledFrom = $this->getCalledFrom();
        if ('agent' == $calledFrom) {
            return Atexo_Config::getParameter('NUMERO_ASSISTANCE_TELEPHONIQUE_AGENT');
        } elseif ('entreprise' == $calledFrom) {
            return Atexo_Config::getParameter('NUMERO_ASSISTANCE_TELEPHONIQUE_ENTREPRISE');
        }

        return Atexo_Config::getParameter('NUMERO_ASSISTANCE_TELEPHONIQUE_DEFAUT');
    }
}
