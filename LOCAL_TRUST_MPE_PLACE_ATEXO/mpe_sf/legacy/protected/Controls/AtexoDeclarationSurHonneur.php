<?php

namespace Application\Controls;

/**
 * Composant pour le message de déclaration sur l'honneur.
 *
 * @author Mouslim MITALI
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class AtexoDeclarationSurHonneur extends MpeTTemplateControl
{
    public string $atexoValidationGroup = '';

    public function setAtexoValidationGroup($value)
    {
        $this->atexoValidationGroup = $value;
    }

    public function onLoad($param)
    {
        $this->validateurDeclaration->setValidationGroup($this->atexoValidationGroup);
    }
}
