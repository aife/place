<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_GestionCalendrier;

/**
 * Page de gestion du Calendrier.
 *
 * @author Khadija CHOUIKA <khadija.chouika@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 4
 */
class AtexoCalendrier extends MpeTTemplateControl
{
    private $_calendrierReel;
    private $_calendrierPrevisionel;
    private $_calendrierParametre;

    public function getCalendrierReel()
    {
        return $this->_calendrierReel;
    }

    public function setCalendrierReel($value)
    {
        $this->_calendrierReel = $value;
    }

    public function getCalendrierPrevisionel()
    {
        return $this->_calendrierPrevisionel;
    }

    public function setCalendrierPrevisionel($value)
    {
        $this->_calendrierPrevisionel = $value;
    }

    public function getCalendrierParametre()
    {
        return $this->_calendrierParametre;
    }

    public function setCalendrierParametre($value)
    {
        $this->_calendrierParametre = $value;
    }

    public function onLoad($param)
    {
        if ($this->_calendrierPrevisionel) {
            $this->calendrierPrevisionel->Visible = true;
        } elseif ($this->_calendrierReel) {
            $this->calendrierReel->Visible = true;
        } elseif ($this->_calendrierParametre) {
            $this->calendrierParametre->Visible = true;
        }
    }

    public function save()
    {
        if ($this->jsonChaine->value) {
            (new Atexo_GestionCalendrier())->saveJson(base64_decode($this->jsonChaine->value));
        }
    }
}
