<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_Config;

/**
 * @author Khadija CHOUIKA <khadija.chouia@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 2.1
 *
 * @since MPE-4.0
 */
class AtexoFileUploadWithProgressBar extends MpeTTemplateControl
{
    public $_maxSize = '';
    public $_url_serveur;
    public $_titre;
    public $_fonctionJs;

    protected string $cssClass = 'ajout-file-upload';
    public bool $afficherProgress = true;
    public bool $afficherImageBoutton = true;
    protected bool $style = false;
    protected string $size = '';

    public function setFileSizeMax($value)
    {
        $this->_maxSize = $value;
    }

    public function getFileSizeMax()
    {
        return $this->_maxSize;
    }

    public function setUrlServeur($value)
    {
        $this->_url_serveur = $value;
    }

    public function getUrlServeur()
    {
        return $this->_url_serveur;
    }

    public function setTitre($value)
    {
        $this->_titre = $value;
    }

    public function getTitre()
    {
        return $this->_titre;
    }

    public function setFonctionJs($value)
    {
        $this->_fonctionJs = $value;
    }

    public function getFonctionJs()
    {
        return $this->_fonctionJs;
    }

    public function setCssClass($value)
    {
        $this->cssClass = $value;
    }

    public function getCssClass()
    {
        return $this->cssClass;
    }

    public function setAfficherProgress($value)
    {
        $this->afficherProgress = $value;
    }

    public function getAfficherProgress()
    {
        return $this->afficherProgress;
    }

    public function setAfficherImageBoutton($value)
    {
        $this->afficherImageBoutton = $value;
    }

    public function getAfficherImageBoutton()
    {
        return $this->afficherImageBoutton;
    }

    public function setStyle($value)
    {
        $this->style = $value;
    }

    public function getStyle()
    {
        return $this->style;
    }

    public function setSize($value)
    {
        $this->size = $value;
    }

    public function getSize()
    {
        return $this->size;
    }

    public function onLoad($param)
    {
        if ('' != $this->getFileSizeMax()) {
            $this->_maxSize = $this->getFileSizeMax();
        } else {
            $this->_maxSize = Atexo_Config::getParameter('MAX_UPLOAD_FILE_SIZE');
        }
        $this->_url_serveur = Atexo_Config::getParameter('DEFAULT_SERVEUR_UPLOAD_FILE');
    }

    public function hasFile()
    {
        return $this->hasFile->value;
    }

    public function getLocalName()
    {
        return Atexo_Config::getParameter('COMMON_TMP').$this->reponseServeur->value;
    }

    public function getFileName()
    {
        return $this->NomFichier->value;
    }

    public function refrechCompsant()
    {
        $this->NomFichier->value = '';
        $this->hasFile->value = '';
        $this->reponseServeur->value = '';
    }
}
