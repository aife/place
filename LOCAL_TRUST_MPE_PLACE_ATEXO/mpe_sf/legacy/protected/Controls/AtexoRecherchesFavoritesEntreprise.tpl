<div class="breadcrumbs"><a href="#"><com:TTranslate>TEXT_MES_RECHERCHES</com:TTranslate></a>&gt; <com:TLabel ID="fileAriane" Text="<%=$this->getTextFileAriane()%>"/></div>

<!--Debut Bloc Gestion des alertes-->
<com:TActivePanel ID="panelRecherches" cssClass="form-field">
	<com:PanelMessageErreur ID="panelMessageErreur"/>
	<com:PanelMessageAvertissement visible = "false" id="panelAvertissement" cssClass="message bloc-700"/>
	<com:TActivePanel ID="panelListeRecherche">
		<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
		<div class="content">
			<h3><com:TLabel ID="titreTableauRecherche" Text="<%=$this->getTitreTableauRecherche()%>"/></h3>
			<div class="table-bloc">
				<com:TRepeater ID="repeaterAlertes" >
					<prop:HeaderTemplate>
						<table class="table-results" summary="<%=Prado::localize('TEXT_LISTES_ALERTE')%>">
							<caption><com:TTranslate>TEXT_LISTES_ALERTE</com:TTranslate></caption>
							<thead>
							<tr>
								<th class="top" colspan="<%=($this->TemplateControl->getTypeCreateur() ==  Application\Service\Atexo\Atexo_Config::getParameter('TYPE_CREATEUR_ENTREPRISE'))?'7':'5'%>"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></th>
							</tr>
							<tr>
								<th class="col-500" id="descriptionRecherche">&nbsp;</th>
								<th class="col-100" id="typeAvis"><com:Tlabel Text="<%=Prado::Localize('TEXT_TYPE_AVIS')%>" /></th>
								<com:TPanel  Visible="<%=$this->TemplateControl->getTypeCreateur() ==  Application\Service\Atexo\Atexo_Config::getParameter('TYPE_CREATEUR_ENTREPRISE')%>" >
									<th class="col-80" id="typeAlerte"><com:TTranslate>TEXT_ALERTE</com:TTranslate></th>
									<th class="col-80" id="typeRecherche" ><com:TTranslate>DEFINE_RECHERCHE</com:TTranslate></th>
								</com:TPanel>
								<th class="actions" id="modifier"><com:Tlabel Text="<%=Prado::Localize('BOUTON_MODIFIER')%>" /></th>
								<th class="actions" id="supprimer"><com:TTranslate>DEFINE_TEXT_SUPPRIMER</com:TTranslate></th>
								<th class="actions" id="recherche"><com:TTranslate>TEXT_RECHERCHE</com:TTranslate></th>
							</tr>
							</thead>
					</prop:HeaderTemplate>
					<prop:ItemTemplate>
						<tr class="<%#(($this->ItemIndex%2==0)? '':'on')%>">
							<td class="col-500" headers="descriptionRecherche"><%#$this->Data->getDenomination()%></td>
							<td class="col-100" headers="typeAvis">
								<com:Tlabel Text="<%=Prado::Localize(($this->Data->getTypeAvis() == Application\Service\Atexo\Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION')) ? 'DEFINE_CONSULTATION' : 'TEXT_AUTRES_ANNONCES')%>" />
							</td>
							<com:TPanel id="panelTypeRecherche" Visible="<%=$this->TemplateControl->getTypeCreateur() ==  Application\Service\Atexo\Atexo_Config::getParameter('TYPE_CREATEUR_ENTREPRISE')%>" >
								<td class="col-80"  headers="typeAlerte"><com:TImage ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-alerte.png" Attributes.alt="<%=Prado::localize('TEXT_ALERTE')%>" Style="display:<%#$this->Data->getAlerte()?'':'none'%>;"/></td>
								<td class="col-80"  headers="typeRecherche"><com:TImage ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-recherche.png" Attributes.alt="<%=Prado::localize('DEFINE_RECHERCHE')%>" Style="display:<%#$this->Data->getRecherche()?'':'none'%>;"/></td>
							</com:TPanel>
							<td class="actions" headers="modifier">
								<com:THiddenField id="labelDescriptionRecherche" value="<%#$this->Data->getDenomination()%>" />
								<com:TImageButton
										Visible="<%=($this->Data->getTypeAvis() == Application\Service\Atexo\Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION'))%>"
										ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-edit.gif"
										Attributes.alt="<%=Prado::localize('TEXT_EDITER_LA_LIGNE')%>"
										Attributes.title="<%=Prado::localize('TEXT_EDITER_LA_LIGNE')%>"
										onCommand="page.rechercheFavoritesEntreprise.modifierMaRecherche"
										CommandParameter="<%#$this->Data->getId()%>"
								/>
							</td>
							<td class="actions" headers="supprimer">
								<com:TActiveImageButton
										ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-supprimer-big.gif"
										Attributes.alt="<%=Prado::localize('TEXT_SUPPRIMER_LA_LIGNE')%>"
										Attributes.title="<%=Prado::localize('TEXT_SUPPRIMER_LA_LIGNE')%>"
										Attributes.OnClick = "document.getElementById('<%#$this->SourceTemplateControl->idToDetele->getClientId()%>').value = '<%#$this->Data->getId()%>';
    														  document.getElementById('<%#$this->SourceTemplateControl->nameToDelete->getClientId()%>').innerHTML =  document.getElementById('<%#$this->labelDescriptionRecherche->getClientId()%>').value;
   															  openModal('modal-confirmation-suppression','popup-small2',this);"
								/>
							</td>
							<td class="actions" headers="recherche">
								<com:TActiveImageButton
										ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-details.gif"
										CommandName="Search"
										onCommand="SourceTemplateControl.actionRecherche"
										onCallBack="SourceTemplateControl.refreshRepeater"
										CommandParameter="<%#$this->Data->getId()%>"
										Attributes.alt="<%=Prado::localize('TEXT_LANCER_RECHERCHE_CORRESPONDANT_AUX_CRITERES')%>"
										Attributes.title="<%=Prado::localize('TEXT_LANCER_RECHERCHE_CORRESPONDANT_AUX_CRITERES')%>"
								/>
							</td>
						</tr>
					</prop:ItemTemplate>
					<prop:FooterTemplate>
						</table>
					</prop:FooterTemplate>
				</com:TRepeater>
			</div>
			<div class="breaker"></div>
			<a href="<%=$this->getCheminAdvancedSearch()%>" class="ajout-el"><com:TTranslate>DEFINE_TEXT_CREER</com:TTranslate></a>
		</div>
		<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
	</com:TActivePanel>
</com:TActivePanel>
<!--Fin Bloc Gestion des alertes-->
<a href="<%=(($this->typeCreateur ==  Application\Service\Atexo\Atexo_Config::getParameter('TYPE_CREATEUR_ENTREPRISE') )? 'index.php?page=Entreprise.EntrepriseAccueilAuthentifie':'agent')%>" class="bouton-retour" title="<%=Prado::localize('TEXT_RETOUR_MON_COMPTE')%>"><com:TTranslate>TEXT_RETOUR</com:TTranslate></a>
<div class="breaker"></div>
<!--Debut Modal Recherche-->
<div class="modal-recherche" style="display:none;">
	<com:AtexoValidationSummary id="divValidation" ValidationGroup="validateName" />
	<!--Debut Formulaire-->
	<div class="form-bloc">
		<div class="top"><span class="left"></span><span class="right"></span></div>
		<div class="content">
			<div class="line">
				<label for="rechercheName" class="intitule-120"><com:TTranslate>TEXT_NOM_RECHERCHE</com:TTranslate><strong><span style="color:red">*</span></strong>:</label>
				<com:TActiveTextBox ID="rechercheName" cssClass="bloc-400" />
				<com:TRequiredFieldValidator
						ControlToValidate="rechercheName"
						ValidationGroup="validateName"
						Display="Dynamic"
						ErrorMessage="<%=Prado::localize('TEXT_NOM_RECHERCHE')%>"
						EnableClientScript="true"
						Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
					<prop:ClientSide.OnValidationError>
						document.getElementById('divValidation').style.display='block';
					</prop:ClientSide.OnValidationError>
					<prop:ClientSide.OnValidationSuccess>
						document.getElementById('divValidation').style.display='none';
					</prop:ClientSide.OnValidationSuccess>
				</com:TRequiredFieldValidator>
			</div>
		</div>
		<div class="bottom"><span class="left"></span><span class="right"></span></div>
	</div>
	<!--Fin Formulaire-->
	<!--Debut line boutons-->
	<div class="boutons-line">
		<input type="button"
			   class="bouton-moyen float-left"
			   value="Annuler" title="Annuler"
			   onclick="J('.modal-recherche').dialog('close');"
		/>
		<com:TActiveButton
				id="modifierRecherche"
				Text = "<%=Prado::localize('TEXT_ENREGISTER')%>"
				Attributes.title="<%=Prado::localize('TEXT_ENREGISTER')%>"
				ValidationGroup="validateName"
				CssClass="bouton-moyen float-right"
				CommandName="Modify"
				CommandParameter="<%=$this->idToDetele->value%>"
				onCommand="actionRecherche"
				onCallBack="refreshRepeater"
		>
		</com:TActiveButton >
	</div>
	<!--Fin line boutons-->
</div>
<!--Fin Modal Recherche-->
<!--Debut Modal Confirmation-Suppression-->
<div class="modal-confirmation-suppression" style="display:none;">
	<!--Debut Formulaire-->
	<div class="form-bloc">
		<div class="top"><span class="left"></span><span class="right"></span></div>
		<div class="content">
			<p><strong><com:TTranslate>TEXT_ALERTE_SUPPRESSION_RECHERCHE</com:TTranslate></strong></p>
			<p><com:TActiveLabel ID="nameToDelete" text=""/></p>

		</div>
		<div class="bottom"><span class="left"></span><span class="right"></span></div>
	</div>
	<!--Fin Formulaire-->
	<!--Debut line boutons-->
	<div class="boutons-line">
		<com:THiddenField id="idToDetele"/>
		<input type="button"
			   class="bouton-moyen float-left"
			   value="<%=Prado::localize('DEFINE_ANNULER')%>"
			   title="<%=Prado::localize('DEFINE_ANNULER')%>"
			   onclick="J('.modal-confirmation-suppression').dialog('close');return false;"
		/>
		<com:TActiveButton
				id="cofirmSupression"
				Text = "<%=Prado::localize('TEXT_POURSUIVRE')%>"
				Attributes.title="<%=Prado::localize('TEXT_POURSUIVRE')%>"
				Attributes.OnClick="J('.modal-confirmation-suppression').dialog('close');"
				CssClass="bouton-moyen float-right"
				CommandName="Delete"
				onCommand="actionRecherche"
				onCallBack="refreshRepeater"
				CommandParameter="<%=$this->idToDetele->value%>"
		>
		</com:TActiveButton >
	</div>
	<!--Fin line boutons-->
</div>
<com:TActiveLabel id="scriptJs" style="display:none;" />
<!--Debut Modal  Confirmation-Suppression-->
