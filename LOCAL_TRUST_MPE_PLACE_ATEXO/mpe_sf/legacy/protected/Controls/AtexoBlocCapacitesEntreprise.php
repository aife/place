<?php

namespace Application\Controls;

use Application\Propel\Mpe\CommonTCandidatureMps;
use Application\Service\Atexo\Atexo_CommonReferentiel;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Entreprise;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_CandidatureMPS;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Capacite;
use Prado\Prado;

/**
 * Le bloc capacites de l'entreprise.
 *
 * @author LEZ <loubna.ezziani@atexo.com>
 *
 * @version 1.0
 *
 * @since 4.7.0
 *
 * @copyright Atexo 2014
 */
class AtexoBlocCapacitesEntreprise extends MpeTTemplateControl
{
    public bool $_modeAffichage = false;
    protected $_capacites;
    protected $_idTrancheEffectifsEntreprise;
    protected $_consultationAlloti;

    /**
     * Set the value of [_modeAffichage] .
     *
     * @param bool $modeAffichage new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function setModeAffichage($modeAffichage)
    {
        $this->_modeAffichage = $modeAffichage;
    }

    /**
     * Get the value of [_modeAffichage] column.
     *
     * @return Booleean
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function getModeAffichage()
    {
        return $this->_modeAffichage;
    }

    /**
     * Get the value of [_capacites] column.
     *
     * @return array of Atexo_Entreprise_Capacite
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function getCapacites()
    {
        return $this->_capacites;
    }

    /**
     * Set the value of [_capacites] .
     *
     * @param array of  Atexo_Entreprise_Capacite $capacites new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function setCapacites($capacites)
    {
        $this->_capacites = $capacites;
    }

    /**
     * @return mixed
     */
    public function getIdTrancheEffectifsEntreprise()
    {
        return $this->_idTrancheEffectifsEntreprise;
    }

    /**
     * @param mixed $idTrancheEffectifsEntreprise
     */
    public function setIdTrancheEffectifsEntreprise($idTrancheEffectifsEntreprise)
    {
        $this->_idTrancheEffectifsEntreprise = $idTrancheEffectifsEntreprise;
    }

    /**
     * @return mixed
     */
    public function getConsultationAlloti()
    {
        return $this->getViewState('consultationAlloti');
    }

    /**
     * @param mixed $consultationAlloti
     */
    public function setConsultationAlloti($consultationAlloti)
    {
        $this->setViewState('consultationAlloti', $consultationAlloti);
    }

    /**
     * Get the value of [_candidatureMPS] column.
     *
     * @return CommonTCandidatureMps
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function getCandidatureMPS()
    {
        return $this->Page->getViewState('candidatureMPS');
    }

    /**
     * Permet de charger les donnees.
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function onLoad($param)
    {
        if (!$this->Page->isPostBack) {
            $this->fillRepeaterCapacite();
            $this->fillTrancheEffectifs();
        }
    }

    /**
     * Remplire le repeater des capacites.
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function fillRepeaterCapacite()
    {
        $capacitesData = $this->getCapacites();
        if (!is_array($capacitesData) || !count($capacitesData)) {
            $capacitesData = (new Atexo_Entreprise())->initialiserCapacite();
        }
        $this->repeaterCapacites->DataSource = $capacitesData;
        $this->repeaterCapacites->dataBind();
    }

    /**
     * Remplire la liste des tranches effectifs.
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function fillTrancheEffectifs()
    {
        $referentiels = (new Atexo_CommonReferentiel())->retrieveValeurReferentiel(Atexo_Config::getParameter('ID_REFERENTIEL_TRANCHE_EFFECTIF'));
        $data = [];
        $data['0'] = Prado::localize('TEXT_SELECTION_LISTE');
        if (is_array($referentiels)) {
            foreach ($referentiels as $referentiel) {
                $data[$referentiel->getId()] = $referentiel->getLibelleValeurReferentiel();
            }
        }
        $this->trancheEffectifs->DataSource = $data;
        $this->trancheEffectifs->DataBind();
        if ($this->getIdTrancheEffectifsEntreprise()) {
            $this->trancheEffectifs->setSelectedValue($this->getIdTrancheEffectifsEntreprise());
        }
    }

    /**
     * Remplire la candidature MPS par les donnees des capacites.
     *
     * @param CommonTCandidatureMps $candidatureMPS la candidature MPS
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function fillCandidatureByDataCapacites(&$candidatureMPS)
    {
        $capacitesCandidature = [];
        foreach ($this->repeaterCapacites->Items as $item) {
            $capacite = new Atexo_Entreprise_Capacite();
            if ($item->check_list->checked) {
                $capacite->setActive(true);
                $capacite->setAnneeCloture($item->anneeCloture->Text);
                $capacite->setDateDebut($item->exercice_date_debut->Text);
                $capacite->setDateFin($item->exercice_date_fin->Text);
                $capacite->setChiffreAffaire($item->exercice_chiffre_affaire->Text);
                $capacite->setSignificatif($item->exercice_significatif->Text);
                $capacite->setCommentaire($item->exercice_commentaire->Text);
                if ($item->exercice_date_debut->Text && $item->exercice_date_fin->Text && $item->exercice_chiffre_affaire->Text && $item->exercice_significatif->Text) {
                    $capacite->setStatutOK(true);
                } else {
                    $capacite->setStatutOK(false);
                }
                $capacitesCandidature[] = $capacite;
            }
        }
        $candidatureMPS->setCapacites($capacitesCandidature);
        $candidatureMPS->setTrancheEffectifs($this->trancheEffectifs->getSelectedValue());
    }

    /**
     * Permet de charger les informations des capacites de l'entreprise.
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function chargerInfoCapacites()
    {
        $candidatureMPS = $this->getCandidatureMPS();
        if ($candidatureMPS instanceof Atexo_Entreprise_CandidatureMPS) {
            if ($this->getModeAffichage()) {
                $this->fillRepeaterCapaciteRecapitulation();
            }
            if ($candidatureMPS->getTrancheEffectifs()) {
                $this->trancheEffectifs->setSelectedValue($candidatureMPS->getTrancheEffectifs());
                $this->trancheEffectifsLabel->Text = $this->trancheEffectifs->getSelectedItem()->getText();
            } else {
                $this->trancheEffectifsLabel->Text = '';
            }
            Atexo_Util::setCheckImageByEtat('imgTrancheEffectifs', $this, $candidatureMPS->getTrancheEffectifs());
        }
    }

    /**
     * Remplire le repeater des capacites Recapitulation.
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function fillRepeaterCapaciteRecapitulation()
    {
        $capacitesData = $this->getCapacites();
        $dataSource = [];
        if (is_array($capacitesData) && count($capacitesData)) {
            $dataSource = $capacitesData;
            $dataSourceVide = [];
        } else {
            $dataSourceVide = [1];
        }
        $this->repeaterCapacitesRecapitulation->DataSource = $dataSource;
        $this->repeaterCapacitesRecapitulation->dataBind();

        $this->repeaterCapacitesRecapitulationVide->DataSource = $dataSourceVide;
        $this->repeaterCapacitesRecapitulationVide->dataBind();
    }
}
