 <!--Debut message de avertissement-->
 <div class="spacer-mini"></div>
 <div class="alert alert-warning <%=$this->getCssClass()%> bloc-message form-bloc-conf msg-avertissement">
        <div class="alert-content">
            <!--Debut Message type d'erreur general-->
            <com:TActiveLabel ID="labelMessage" />
            <div class="spacer-mini"></div>
            <!--Fin Message type d'erreur general-->
        </div>
 </div>
<!--Fin  message de avertissement-->
