<!--Debut Bloc Recap Lot-->
<com:TActivePanel cssClass="form-bloc margin-0" ID="panelRecap" >
	<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
	<div class="content">
		<com:TActivePanel id="infoCons" >

        <com:TPanel cssclass="line" id="panelNumeroProjetAchat" visible="<%=Application\Service\Atexo\Atexo_Module::isEnabled('NumeroProjetAchat')%>">
            <div class="intitule-300"><strong><com:TTranslate>DEFINE_FULL_NUMERO_PROJET_ACHAT</com:TTranslate></strong> :</div>
            <div class="content-bloc"><com:TActiveLabel ID="labelNumeroProjetAchat" /></div>
        </com:TPanel>

		<div class="line">
			<div class="intitule-300"><strong><com:TTranslate>OBJET_CONSULTATION</com:TTranslate> :</strong></div>
			<div class="content-bloc"><com:TActiveLabel ID="labelTitreConsultation" /></div>
		</div>
		<div class="line">
			<div class="intitule-300"><strong><com:TTranslate>REFERENCE_CONSULTATION</com:TTranslate> :</strong></div>
			<div class="content-bloc"><com:TActiveLabel ID="labelReferenceConsultation" /></div>
		</div>
		<div class="line" >
			<com:TRepeater ID="repeaterLots">
				<prop:HeaderTemplate>
					<div class="intitule-300"><strong><com:TTranslate>LOTS_CONCERNES</com:TTranslate> :</strong></div>
					<div class="content-bloc">
				</prop:HeaderTemplate>
					<prop:ItemTemplate>
						<div><com:TTranslate>LOT</com:TTranslate> <com:TLabel ID="numLot" Text="<%#$this->Data->getLot()%>" /> - <com:TLabel ID="intituleLot" Text="<%#$this->Data->getDescription()%>" /></div>
					</prop:ItemTemplate>
				<prop:FooterTemplate>
					</div>
				</prop:FooterTemplate>
			</com:TRepeater>
		</div>
		<div class="breaker"></div>
		</com:TActivePanel>
		<com:TActivePanel ID="panelInfoAttribution">
			<com:TActivePanel id="panelObjetContrat" visible="false" cssClass="line">
				<div class="intitule-300"><strong><com:TTranslate>CONTRAT_NUM_OBJET</com:TTranslate> : </strong></div>
				<div class="content-bloc"><com:TActiveLabel ID="objetContrat" /></div>
			</com:TActivePanel>
			<com:TActivePanel id="panelIdentificationUnique" visible="false" cssClass="line">
				<div class="intitule-300 line-small-spacing" ><strong><com:TTranslate>NUMERO_IDENTIFICATION_UNIQUE_DONNEES_ESSENTIELLES</com:TTranslate> : </strong></div>
				<div class="content-bloc"><com:TActiveLabel ID="identificationUnique" /></div>
			</com:TActivePanel>
			<com:TActivePanel id="panelDateAttribution" cssclass="line">
				<div class="intitule-300"><strong><com:TTranslate>DECISION_D_ATTRIBUTION_LE</com:TTranslate> : </strong></div>
				<div class="calendar">
					<com:TTextBox ID="dateDecisionAttribution"  Attributes.title="<%=Prado::localize('DATE_DECISION_ATTRIBUTION')%>" CssClass="heure"  />
					<com:TImage id="calendarDecisionAttribution" ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-calendar.gif" Attributes.alt="<%=Prado::localize('DEFINE_CALENDRIER')%>" Attributes.title="<%=Prado::localize('DEFINE_CALENDRIER')%>" Attributes.onclick="popUpCalendar(this,document.getElementById('ctl0_CONTENU_PAGE_recapConsultation_dateDecisionAttribution'),'dd/mm/yyyy','');"  />
				</div>
			</com:TActivePanel>
			<div class="line">
				<div class="intitule-300"><strong><com:TTranslate>DEFINE_TYPE_CONTRAT</com:TTranslate> : </strong></div>
				<div class="content-bloc"><com:TActiveLabel ID="typeContrat" /></strong></div>
			</div>
			<com:TActivePanel id="panelProcedurePassation" visible="false" cssClass="line">
				<div class="intitule-300"><strong><com:TTranslate>TEXT_PROCEDURE_PASSATION</com:TTranslate> : </strong></div>
				<div class="content-bloc"><com:TActiveLabel ID="procedurePassation" /></strong></div>
			</com:TActivePanel>
			<div class="breaker"></div>
		</com:TActivePanel>
	</div>
	<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
</com:TActivePanel>
<div class="spacer-mini"></div>
<!--Fin Bloc Recap Lot-->