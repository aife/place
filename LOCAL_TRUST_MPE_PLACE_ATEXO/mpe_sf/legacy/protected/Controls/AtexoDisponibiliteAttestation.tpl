<com:TConditional condition="$this->isResponsive()">
	<prop:trueTemplate>
		<%include Application.templates.responsive.AtexoDisponibiliteAttestation %>
	</prop:trueTemplate>
	<prop:falseTemplate>
		<%include Application.templates.mpe.AtexoDisponibiliteAttestation %>
	</prop:falseTemplate>
</com:TConditional>