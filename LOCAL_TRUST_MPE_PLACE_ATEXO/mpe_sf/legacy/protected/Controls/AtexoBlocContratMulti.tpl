<!--Debut Bloc Formulaire Infos AC,Infos SAD -->
<com:TActivePanel Id="panelContratMulti" cssClass="form-field" display="None">
	<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
	<div class="content">
		<h3><com:TActiveLabel id="titreTypeContrat" /></h3>
		<div class="spacer-small"></div>
		<!--Debut Intitulé-->
		<div class="line">
			<div class="intitule-150">
				<label for="intitule"><com:TTranslate>TEXT_INTITULE_CONTRAT</com:TTranslate></label>
				<span class="champ-oblig">*</span>:
			</div>
			<div class="content-bloc">
				<com:TTextBox
						id="intitule"
						cssclass="long float-left"
						attributes.title="<%=Prado::localize('TEXT_INTITULE_CONTRAT')%>"
						attributes.maxlength="<%= Application\Service\Atexo\Atexo_Config::getParameter('MAXLENGTH_INTITULE_CONTRAT')%>"
				/>
				<com:TActiveImage id="ImgErrorIntituleContrat"
								  ImageUrl='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif'
								  Attributes.alt='Champ obligatoire' Style="display:none"
				/>
			</div>
		</div>
		<!--Fin Intitulé-->
		<!--Debut Bloc Objet-->
		<div class="line">
			<div class="intitule-150 line-height-normal">
				<label for="objetMarche"><com:TActiveLabel ID="labelObjetMarche" />
				<span class="champ-oblig">*</span>:
			</div>
			<div class="content-bloc">
				<com:TTextBox ID="objetMarche" CssClass="long" TextMode="MultiLine" attributes.maxlength="255"/>
				<com:TActiveImage id="ImgErrorObjetMarche"  ImageUrl='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif'  Attributes.alt='Champ obligatoire' Style="display:none" />
			</div>
		</div>
		<!-- Fin Bloc Objet-->
        <!-- Debut Lieu Principal d'exécution -->
        <div class="line line-lieuPrincipalExecution">
            <div class="intitule-150">
                <label for="lieuPrincipalExecution"><com:TTranslate>TEXT_LIEU_PRINCIPAL_EXECUTION</com:TTranslate></label><span class="champ-oblig">*</span>:
            </div>
            <div class="content-bloc">
                <com:TJuiAutoComplete
                        ID="lieuPrincipalExecution"
                        OnSuggest="suggestLieux"
                        Suggestions.DataKeyField="id"
                        ResultPanel.CssClass="acomplete"
                        cssClass="long"
                        MinChars="2"
                        OnSuggestionSelected="suggestionSelected"
                        Attributes.title="<%=Prado::localize('TEXT_LIEU_PRINCIPAL_EXECUTION')%>"
                        Attributes.placeholder="<%=Prado::localize('TEXT_RECHERCHE_DEPARTEMENT')%>"
                >
                    <prop:Suggestions.ItemTemplate>
                        <li title="<%#$this->Data['denomination']%>"><%# $this->Data['denomination']%></li>
                    </prop:Suggestions.ItemTemplate>
                </com:TJuiAutoComplete>
                <com:TRequiredFieldValidator
                        ValidationGroup="validateInfosContrat"
                        ControlToValidate="lieuPrincipalExecution"
                        Display="Dynamic"
                        ErrorMessage="<%=Prado::localize('TEXT_LIEU_PRINCIPAL_EXECUTION')%>"
                        Text="<span title='<%=Prado::localize('TEXT_LIEU_PRINCIPAL_EXECUTION')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' /></span>" >
                    <prop:ClientSide.OnValidationError>
                        document.getElementById('divValidationSummary').style.display='';
                        document.getElementById('<%=$this->page->buttonValidate->getClientId()%>').style.display='';
                    </prop:ClientSide.OnValidationError>
                </com:TRequiredFieldValidator>
            </div>
        </div>
        <!-- Fin Lieu Principal d'exécution -->
		<!--Debut Bloc Numéro-->
		<com:TActivePanel cssClass="line" id="panelNumero">
			<div class="intitule-150"><label for="numero"><com:TActiveLabel ID="labelNumero" Text="<%=Prado::Localize('TEXT_NUMERO_CONTRAT_CHAPEAU')%>"/></label> :</div>
			<div class="content-bloc-auto">
				<com:TActiveTextBox cssClass="input-250 float-left-fix disabled" ID="numero" Attributes.title="<%=Prado::Localize('TEXT_NUMERO_CONTRAT_CHAPEAU')%>"  enabled="false" />
			</div>
		</com:TActivePanel>
		<div class="spacer-mini"></div>
		<!--Fin Bloc Numéro-->
		<!--Debut Bloc Montant-->
		<com:TActivePanel cssClass="line" id="panelMontantAC">
			<div class="intitule-150"><label for="montantMax"><com:TTranslate>MONTANT_MAX_ESTIME</com:TTranslate></label><span class="champ-oblig">*</span> :</div>
			<div class="content-bloc-auto">
				<com:TTextBox ID="montantMax"  Attributes.title="<%=Prado::Localize('MONTANT_MAX_ESTIME')%>"
							  CssClass="input-100 float-left-fix"
							  Attributes.onblur="formatterMontant('<%=$this->montantMax->getClientId()%>');"
						/>
				<div class="intitule-bloc"><com:TTranslate>DEFINE_UNITE_HT</com:TTranslate></div>
				<com:TActiveImage id="imgErrorMontantMax"  ImageUrl='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif'  Attributes.alt='Champ obligatoire' Style="display:none" />
			</div>
		</com:TActivePanel>
		<div class="spacer-mini"></div>
		<!--Fin Bloc Montant-->
        <!--Debut Ligne Forme du prix-->
		<com:TActivePanel ID="panelFormePrix">
			<div class="line">
				<div class="intitule col-150"><label for="formePrix"><com:TTranslate>TEXT_FORME_PRIX</com:TTranslate></label><span class="champ-oblig">*</span> :</div>
				<div class="content-bloc-auto">
					<div class="intitule-auto">
						<com:TActiveRadioButton GroupName="formePrix" id="formePrix_ferme" cssclass="radio" /><label for="formePrix_ferme"><com:TTranslate>DEFINE_FERME</com:TTranslate></label>
					</div>
					<div class="intitule-auto">
						<com:TActiveRadioButton GroupName="formePrix" id="formePrix_ferme_actualisable" cssclass="radio" /><label for="formePrix_ferme_actualisable"><com:TTranslate>TEXT_FERME_ACTUALISABLE</com:TTranslate></label>
					</div>
					<div class="intitule-auto">
						<com:TActiveRadioButton GroupName="formePrix" id="formePrix_revisable" cssclass="radio" /><label for="formePrix_revisable"><com:TTranslate>TEXT_REVISABLE</com:TTranslate></label>
					</div>
				</div>
			</div>
			<div class="spacer"></div>
		</com:TActivePanel>
        <!--Fin Ligne Forme du prix-->
		<!--Debut Ligne Nature prestations-->
		<div class="line">
			<div class="intitule-150 line-height-normal">
				<label for="naturePrestations"><com:TTranslate>NATURE_DES_PRESTATIONS</com:TTranslate></label><span class="champ-oblig">*</span>:</div>
			<div class="content-bloc">
				<com:TDropDownList
						id="naturePrestations"
						CssClass="select-320 cpvCategoryCheck"
						Attributes.title="<%=Prado::localize('NATURE_DES_PRESTATIONS')%>"
				/>
				<com:TActiveImage id="imgErrorNaturePrestations"  ImageUrl='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif'  Attributes.alt='Champ obligatoire' Style="display:none" />
			</div>
		</div>
		<!--Fin Ligne Nature prestations-->
		<!--Debut Ligne CCAG-->
		<div class="line">
			<div class="intitule-150"><label for="ccag"><com:TTranslate>FCSP_CCAG_APPLICABLE</com:TTranslate></label><span class="champ-oblig">*</span>:</div>
			<div class="content-bloc">
				<com:TActiveDropDownList
						id="ccag"
						CssClass="select-320"
						Attributes.title="<%=Prado::localize('FCSP_CCAG_APPLICABLE')%>"
				/>
				<com:TActiveImage id="imgErrorCcag"  ImageUrl='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif'  Attributes.alt='Champ obligatoire' Style="display:none" />
			</div>
		</div>
		<!--Fin Ligne CCAG-->
		<div class="spacer-small"></div>
		<!--Debut Ligne CPV-->
		<com:AtexoCodesCpv id="referentielCPV" titre="<%=Prado::localize('TEXT_CODE_CPV')%>"/>
		<!--Fin Ligne CPV-->
		<div class="spacer-small"></div>
        <!--Debut Ligne Achat responsable-->
        <com:AchatResponsableContrat id="achatResponsableConsultation" mode="1" cssPersonalisee="true" visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('ConsultationClause'))? true:false %>"/>
        <!--Fin Ligne Achat responsable-->

        <com:TActivePanel id="article133">
        <h3><com:TTranslate>TEXT_PUBLICATION_CONTRAT</com:TTranslate> </h3>
        <div class="line">
            <label class="intitule-auto">
                <com:TActiveRadioButton id="art133Publie" GroupName="artile133"
                                        cssclass="check"
                />
                <com:TTranslate>PUBLIER_DE</com:TTranslate>
            </label>
        </div>
        <div class="line">
            <label class="intitule-auto">
                <com:TActiveRadioButton id="art133NonPublie" GroupName="artile133"
                                        cssclass="check"
                />
                <com:TTranslate>NE_PAS_PUBLIER_DE</com:TTranslate>
            </label>
        </div>
    </com:TActivePanel>


	</div>
	<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
</com:TActivePanel>
<!--Fin Bloc Formulaire Infos AC ,Infos SAD-->
