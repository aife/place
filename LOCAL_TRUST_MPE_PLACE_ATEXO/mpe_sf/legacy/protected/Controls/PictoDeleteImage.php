<?php

namespace Application\Controls;

use Application\Service\Atexo\Controller\Atexo_Controller_Front;
use Prado\Prado;
use Prado\Web\UI\WebControls\TImage;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PictoDeleteImage extends TImage
{
    public function onPreRender($param)
    {
        $this->setImageUrl(Atexo_Controller_Front::t().'/images/picto-supprimer-big.gif');
        $this->setAlternateText(Prado::localize('ICONE_MODIFIER'));
        $this->setToolTip(Prado::localize('ICONE_MODIFIER'));
    }
}
