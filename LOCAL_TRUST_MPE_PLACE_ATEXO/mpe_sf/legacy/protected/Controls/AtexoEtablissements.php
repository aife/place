<?php

namespace Application\Controls;

use Application\Library\Propel\Propel;
use Application\Pages\Commun\Classes\Util;
use Application\Propel\Mpe\CommonInscritQuery;
use Application\Propel\Mpe\CommonTEtablissementPeer;
use Application\Propel\Mpe\EntrepriseQuery;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Entreprise;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_EtablissementVo;
use Prado\Prado;
use Prado\Web\UI\TCommandEventParameter;

/**
 * permet de gérer les établissements.
 *
 * @author ASO <ayoub.souidahmed@atexo.com>
 *
 * @version 1.0
 *
 * @since 4.9.0
 *
 * @copyright Atexo 2015
 */
class AtexoEtablissements extends MpeTTemplateControl
{
    /*
    *1=>affichage(readOnly) with mon etab,
    *2=>affichage(readOnly) without mon etab,
    *3=>modification just mon etablissement ,
    *4=>modification (ttes les infos)
    */
    private int $mode = 1;
    protected $critereTri = '';
    protected $triAscDesc = '';
    public $nbrElementRepeater;
    public bool $optionAdd = true;
    public bool $responsive = false;
    public bool $pagination = true;
    private int $isEntrepriseSynchro = 0;

    /*
    *true=>(degradé)le ws Sgmap est inaccessible ou l'entreprise est etrangere
    *false=>(normal)le WS Sgmap est accessible et l entreprise et française
    */
    private bool $ajoutManuel = false;

    /**
     * recupere la valeur du [entrepriseExiste].
     *
     * @return int la valeur courante [entrepriseExiste]
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getEntrepriseExiste()
    {
        return $this->page->getViewState('entrepriseExiste');
    }

    /**
     * modifie la valeur de [entrepriseExiste].
     *
     * @param int entrepriseExiste la valeur a mettre
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function setEntrepriseExiste($entrepriseExiste)
    {
        $this->page->setViewState('entrepriseExiste', $entrepriseExiste);
    }

    /**
     * recupere la valeur de [isEntrepriseLocale] dans le viewstate de la page.
     *
     * @return int la valeur courante [isEntrepriseLocale] dans le viewstate de la page
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getIsEntrepriseLocale()
    {
        return $this->page->getViewState('isEntrepriseLocale');
    }

    /**
     * modifie la valeur de [isEntrepriseLocale] dans le viewstate de la page.
     *
     * @param int $isEntrepriseLocale la valeur a mettre
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function setIsEntrepriseLocale($isEntrepriseLocale)
    {
        if (null === $this->page->getViewState('isEntrepriseLocale')) {
            $this->page->setViewState('isEntrepriseLocale', $isEntrepriseLocale);
        }
    }

    /**
     * recupere la valeur du [nbrElementRepeater].
     *
     * @return int la valeur courante [nbrElementRepeater]
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getNbrElementRepeater()
    {
        return $this->nbrElementRepeater;
    }

    /**
     * modifie la valeur de [nbrElementRepeater].
     *
     * @param int $nbrElementRepeater la valeur a mettre
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function setNbrElementRepeater($nbrElement)
    {
        return $this->nbrElementRepeater = $nbrElement;
    }

    /**
     * modifie la valeur de [mode].
     *
     * @param int $mode la valeur a mettre
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function setMode($mode)
    {
        $this->page->setViewState('atexoEtablissementMode', $mode);
    }

    /**
     * recupere la valeur du [mode].
     *
     * @return int la valeur courante [mode]
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getMode()
    {
        return $this->page->getViewState('atexoEtablissementMode');
    }

    /**
     * modifie la valeur de [ajoutManuel].
     *
     * @param bool $mode la valeur a mettre
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function setAjoutManuel($ajoutManuel)
    {
        $this->ajoutManuel = $ajoutManuel;
        $this->page->setViewState('ajoutManuel', $this->ajoutManuel);
    }

    /**
     * recupere la valeur du [ajoutManuel].
     *
     * @return bool la valeur courante [ajoutManuel]
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getAjoutManuel()
    {
        $ajoutManuel = $this->page->getViewState('ajoutManuel');
        if (null === $ajoutManuel) {
            $ajoutManuel = $this->ajoutManuel;
        }

        return $ajoutManuel;
    }

    /**
     * mettre la liste des Etablissement ds le VS.
     *
     * @param array $etablissements liste des etablissement
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function setEtablissementsList($etablissements)
    {
        $this->page->setViewState('ListeEtablissements', $etablissements);
    }

    /**
     * recupere la liste des Etab ds le VS.
     *
     * @return array $etablissements liste des etablissement
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getEtablissementsList()
    {
        return $this->page->getViewState('ListeEtablissements');
    }

    /**
     * Permet d'ajouter un établissement.
     *
     * @param string $index contient le code de l'établissement
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function addEtablissement($index = false)
    {
        $etablissements = $this->getEtablissementsList();

        if ($index) {
            $etablissementVo = $etablissements[$index];
        } else {
            $etablissementVo = new Atexo_Entreprise_EtablissementVo();
            $etablissementVo->setEstSiege(0);
        }

        if (!$this->getIsEntrepriseLocale() || Atexo_Module::isEnabled('CompteEntrepriseIdentifiantUnique')) {
            if (!$index) {
                $i = 1;

                do {
                    $nombreElement = is_iterable($this->getEtablissementsList()) ?
                        count($this->getEtablissementsList()) : 0;
                    $codeEtablissement = str_pad($nombreElement + $i, 5, '0', STR_PAD_LEFT);
                    ++$i;
                } while (isset($etablissements[$codeEtablissement]));

                $etablissementVo->setCodeEtablissement($codeEtablissement);
            }
        } else {
            $etablissementVo->setCodeEtablissement($this->codeEtablissement->Text);
        }

        $etablissementVo->setSaisieManuelle('1');
        $etablissementVo->setAdresse($this->EtablissementAddress1->Text);
        $etablissementVo->setAdresse2($this->Etablissementaddress2->Text);
        $etablissementVo->setCodePostal($this->cpEtablissement->Text);
        $etablissementVo->setVille($this->villeEtablissement->Text);
        $etablissementVo->setPays($this->paysEtablissement->Text);
        $etablissementVo->setTvaIntracommunautaire($this->tvaIntracommunautaire->Text);

        if ($this->inscriptionAnnuaire->checked) {
            $etablissementVo->setInscritAnnuaireDefense('1');
        } else {
            $etablissementVo->setInscritAnnuaireDefense('0');
        }

        if ($index != $etablissementVo->getCodeEtablissement()) {
            unset($etablissements[$index]);
        }

        $etablissements[$etablissementVo->getCodeEtablissement()] = $etablissementVo;
        $this->setEtablissementsList($etablissements);
    }

    /**
     * Permet de supprimer un établissement.
     *
     * @param int $index de l'établissement à supprimer
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function deleteEtablissement($sender, $param)
    {
        $index = $param->CommandParameter;
        $etablissements = $this->getEtablissementsList();
        if ($etablissements[$index]->getIdEtablissement()) {
            $countInscrit = CommonInscritQuery::create()->filterByIdEtablissement($etablissements[$index]->getIdEtablissement())->count();
            if ($countInscrit) {
                $this->scriptJs->text = "<script>
											document.getElementById('".$this->panelConfirmDelete->getClientId()."').style.display = 'none';
											document.getElementById('buttonCancel').style.display = 'none';
											document.getElementById('".$this->cofirmSupression->getClientId()."').style.display = 'none';
											document.getElementById('".$this->panelMessageDelete->getClientId()."').style.display = '';
											document.getElementById('".$this->buttonClose->getClientId()."').style.display = '';
											KTJS.pages_entreprise_common.common.closeModal('#modalConfirmationSuppression');
										</script>";

                return false;
            }
            $idsEtabToDelete = $this->getListeIdsEtabToDelete();
            $idsEtabToDelete[] = $etablissements[$index]->getIdEtablissement();
            $this->setListeIdsEtabToDelete($idsEtabToDelete);
        }
        if ($index == $this->getSiretMonEtablissement()) {
            $this->setSiretMonEtablissement('');
        }
        unset($etablissements[$index]);
        $this->setEtablissementsList($etablissements);
        $this->scriptJs->text = "<script>
									KTJS.pages_entreprise_common.common.closeModal('#modalConfirmationSuppression');
								</script>";
    }

    /**
     * Permet d'editer un établissement.
     *
     * @param int $index de l'etablissement à supprimer
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function editEtablissement($index)
    {
        $this->addEtablissement($index);
    }

    /**
     * Permet d'enregister un établissement ds le VS.
     *
     * @param mixed $sender
     * @param mixed $param
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function saveEtablissement($sender, $param)
    {
        if ($this->Page->IsValid) {
            switch ($this->Action->value) {
                case 'Add':
                    $this->addEtablissement();
                    break;
                case 'Modify':
                    $nouveauCodeEtablissement = $this->codeEtablissement->Text;
                    $old = $param->CommandParameter;
                    $param = new TCommandEventParameter('', $nouveauCodeEtablissement.'#pictoModifier');
                    $this->saveEtablissementApresModification($param, $old);
                    break;
            }
            $this->scriptJs->text = "
                <script>
                    KTJS.pages_entreprise_common.common.closeModal('#modalEtablissement');
                    document.getElementById('".$this->panelMessageAvertissement->getClientId()."').style.display = 'none';
                </script>";
        }
    }

    public function modifyEtablissement($sender, $param)
    {
        $code = $this->mcodeEtablissement->Text;
        if (empty($code) && !empty($param)) {
            $attr = explode('#', $param->CommandParameter);
            $idToModify = $attr[0];
            if (!empty($idToModify)) {
                $code = $idToModify;
            }
        }
        $etablissements = $this->getEtablissementsList();

        $etablissementVo = $etablissements[$code];
        $etablissementVo->setCodeEtablissement($code);

        $etablissementVo->setSaisieManuelle('0');
        $etablissementVo->setAdresse($this->mEtablissementAddress1->Text);
        $etablissementVo->setAdresse2($this->mEtablissementaddress2->Text);
        $etablissementVo->setCodePostal($this->mcpEtablissement->Text);
        $etablissementVo->setVille($this->mvilleEtablissement->Text);
        $etablissementVo->setPays($this->mpaysEtablissement->Text);
        $etablissementVo->setTvaIntracommunautaire($this->mtvaIntracommunautaire->Text);

        if ($this->minscriptionAnnuaire->checked) {
            $etablissementVo->setInscritAnnuaireDefense('1');
        } else {
            $etablissementVo->setInscritAnnuaireDefense('0');
        }

        $etablissements[$etablissementVo->getCodeEtablissement()] = $etablissementVo;
        $this->setEtablissementsList($etablissements);
        $this->scriptJs->text = "<script>KTJS.pages_entreprise_common.common.closeModal('#modalEtablissementModify'); </script>";
    }

    /**
     * permet d'initialiser le composant selon les parametre qu'on lui donne a la source.
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function initComposant()
    {
        $this->displayEtablissements();
    }

    /**
     * permet d'afficher la liste des établissements.
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function displayEtablissements()
    {
        $etabList = $this->getEtablissementsList();
        $nombreElement = is_countable($etabList) ? count($etabList) : 0;

        if (!isset($etabList[$this->getSiretMonEtablissement()])) {
            $this->setSiretMonEtablissement('');
        }
        $this->nombreElement->Text = $nombreElement;
        $this->setViewState('nbrElements', $nombreElement);
        $this->formatCp->setRegularExpression('[0-9]{'.Atexo_Config::getParameter('NBRE_CARACTERE_CP_ETABLISSEMENT_LOCAL').'}');
        $this->formatCpEtrangere->setRegularExpression('[a-zA-Z0-9]{0,'.Atexo_Config::getParameter('NBRE_CARACTERE_CP_ETABLISSEMENT_ETRANGERE').'}');
        if ($nombreElement) {
            if ($this->isPagination()) {
                $this->nombrePageTop->Text = ceil($nombreElement / $this->repeaterEtablissements->PageSize);
                $this->nombrePageBottom->Text = ceil($nombreElement / $this->repeaterEtablissements->PageSize);
                $this->repeaterEtablissements->setVirtualItemCount($nombreElement);
                $this->repeaterEtablissements->setCurrentPageIndex(0);
            }
            self::showComponent();
            self::populateData();
        } else {
            self::hideComponent();
        }
    }

    /**
     * retourne la class de la ligne courante.
     *
     * @return string class
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getClass($index, $codeEtab)
    {
        if (1 == $this->getMode() && $codeEtab == $this->getSiretMonEtablissement()) {
            return 'line-highlight';
        }
        if (0 == $index % 2) {
            return '';
        }

        return 'on';
    }

    /**
     * permet d'actualiser la liste des etablissements.
     *
     * @param mixed $sender
     * @param mixed $param
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function refreshRepeater($sender, $param)
    {
        if ($this->Page->IsValid) {
            $this->displayEtablissements();
        }
    }

    /**
     * mettre la Siret de mon etablissement ds le VS.
     *
     * @param string $siretEtablissement Siret de mon etablissement
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function setSiretMonEtablissement($siretMonEtablissement)
    {
        $this->page->setViewState('SiretMonEtablissement', $siretMonEtablissement);
    }

    /**
     * recupere la siret de mon etablissement depuis le VS.
     *
     * @return int $siretEtablissement Siret de mon etablissement
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getSiretMonEtablissement()
    {
        return $this->page->getViewState('SiretMonEtablissement');
    }

    /**
     * mettre la Siret de mon etablissement ds le VS.
     *
     * @param mixed $sender
     * @param mixed $param
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function setSiretEtablissement($sender, $param)
    {
        $this->setSiretMonEtablissement($sender->TemplateControl->codeEtab->Value);
    }

    /**
     * mettre la siret du siege social de l'entreprise ds le VS.
     *
     * @param string $siretEtablissement Siret du siege social de l'entreprise
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function setSiretSiegeSocial($siretSiegeSocial)
    {
        $this->page->setViewState('SiretSiegeSocial', $siretSiegeSocial);
    }

    /**
     * recupere la siret du siege social de l'entrepris depuis le VS.
     *
     * @return int $siretEtablissement Siret de mon etablissement
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getSiretSiegeSocial()
    {
        return $this->page->getViewState('SiretSiegeSocial');
    }

    /**
     * fct appler sur le .tpl pour mettre a jour l'etablissement siege.
     *
     * @param mixed $sender
     * @param mixed $param
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function setSiretSiegeSocialEse($sender, $param)
    {
        $this->updateSiretSiegeSocialEse($param->CallbackParameter);
    }

    /**
     * mettre a jour l'etablissement siege ds le VS.
     *
     * @param string $siretNewSiege code du siege social
     *
     * @return void
     *
     * @author AMAL EL BEKKAOUI <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function updateSiretSiegeSocialEse($siretNewSiege)
    {
        $siretOldSiege = $this->getSiretSiegeSocial();
        $etablissements = $this->getEtablissementsList();
        if ($etablissements[$siretOldSiege]) {
            $etablissements[$siretOldSiege]->setEstSiege('0');
        }
        if ($etablissements[$siretNewSiege]) {
            $etablissements[$siretNewSiege]->setEstSiege('1');
        }
        $this->setSiretSiegeSocial($siretNewSiege);
        $this->setEtablissementsList($etablissements);
    }

    /**
     * mettre le siren de l'entreprise dans le VS.
     *
     * @param string $sirenEntreprise siren de l'entreprise
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function setSirenEntreprise($sirenEntreprise)
    {
        $this->page->setViewState('SirenEntreprise', $sirenEntreprise);
    }

    /**
     * recupere le siren de l'entreprise depuis le VS.
     *
     * @return string $sirenEntreprise siren de l'entreprise
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getSirenEntreprise()
    {
        return $this->page->getViewState('SirenEntreprise');
    }

    /**
     * charge les informations de l'etablissement dans le formulaire.
     *
     * @param mixed $sender
     * @param mixed $param
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function chargerInfosEtablissement($sender, $param)
    {
        $attr = explode('#', $param->CommandParameter);
        $idToModify = $attr[0];
        $etablissements = $this->getEtablissementsList();
        $etablissement = $etablissements[$idToModify];
        $this->codeEtablissement->Text = $etablissement->getCodeEtablissement();
        $this->EtablissementAddress1->Text = $etablissement->getAdresse();
        $this->Etablissementaddress2->Text = $etablissement->getAdresse2();
        $this->cpEtablissement->Text = $etablissement->getCodePostal();
        $this->villeEtablissement->Text = $etablissement->getVille();
        $this->tvaIntracommunautaire->Text = $etablissement->getTvaIntracommunautaire();
        $this->paysEtablissement->Text = $etablissement->getPays();

        if ($etablissement->getInscritAnnuaireDefense()) {
            $this->inscriptionAnnuaire->checked = true;
        } else {
            $this->inscriptionAnnuaire->checked = false;
        }

        if (1 === $this->isEntrepriseSynchro) {
            $this->scriptJs->text = "<script>
                                    document.getElementById('".$this->panelMessageAvertissement->getClientId()."').style.display = 'none';
									document.getElementById('".$this->idToModify->getClientId()."').value = '".$idToModify."';
									KTJS.pages_entreprise_common.common.openModal('#modalEtablissement');
								</script>";
        } else {
            $this->messageAvertissement->setMessage(Prado::localize('AVERTISSEMENT_ETABLISSEMENT_NON_RECONNU'));
            $this->scriptJs->text = "<script>
                                    document.getElementById('".$this->panelMessageAvertissement->getClientId()."').style.display = '';
									document.getElementById('".$this->idToModify->getClientId()."').value = '".$idToModify."';
									KTJS.pages_entreprise_common.common.openModal('#modalEtablissement');
								</script>";
        }
    }

    /**
     * charge les informations de l'etablissement dans le formulaire en lecture seule.
     *
     * @param mixed $param
     *
     * @return void
     *
     * @copyright Atexo 2020
     */
    public function chargerInfosEtablissementLectureOnly($param)
    {
        $attr = explode('#', $param->CommandParameter);
        $idToModify = $attr[0];
        $etablissements = $this->getEtablissementsList();
        $etablissement = $etablissements[$idToModify];
        $this->mcodeEtablissement->Text = $etablissement->getCodeEtablissement();
        $this->mEtablissementAddress1->Text = $etablissement->getAdresse();
        $this->mEtablissementaddress2->Text = $etablissement->getAdresse2();
        $this->mcpEtablissement->Text = $etablissement->getCodePostal();
        $this->mvilleEtablissement->Text = $etablissement->getVille();
        $this->mpaysEtablissement->Text = $etablissement->getPays();
        $this->mtvaIntracommunautaire->Text = $etablissement->getTvaIntracommunautaire();

        if ($etablissement->getInscritAnnuaireDefense()) {
            $this->minscriptionAnnuaire->checked = true;
        } else {
            $this->minscriptionAnnuaire->checked = false;
        }

        $this->scriptJs->text = "<script>
									document.getElementById('".$this->midToModify->getClientId()."').value = '".$idToModify."';
									KTJS.pages_entreprise_common.common.openModal('#modalEtablissementModify');
								</script>";
    }

    /**
     * permet de trier la liste des etablissements.
     *
     * @param mixed $sender
     * @param mixed $param
     *
     * @return void
     *
     * @author Oussama SALMANI <oussama.salmani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function sortRepeater($sender, $param)
    {
        $this->critereTri = $this->getViewState('critereTri');
        $this->triAscDesc = $this->getViewState('triAscDesc');
        $cachedEtablissement = $this->getEtablissementsList();

        if ('CodeEtablissement' == $sender->getActiveControl()->CallbackParameter) {
            $dataSourceTriee = $this->getDataSourceTriee($cachedEtablissement, 'CodeEtablissement', '');
        } elseif ('Voie' == $sender->getActiveControl()->CallbackParameter) {
            $dataSourceTriee = $this->getDataSourceTriee($cachedEtablissement, 'Adresse', '');
        } elseif ('Cp' == $sender->getActiveControl()->CallbackParameter) {
            $dataSourceTriee = $this->getDataSourceTriee($cachedEtablissement, 'CodePostal', '');
        } elseif ('Ville' == $sender->getActiveControl()->CallbackParameter) {
            $dataSourceTriee = $this->getDataSourceTriee($cachedEtablissement, 'Ville', '');
        } else {
            return false;
        }

        $this->setEtablissementsList($dataSourceTriee);
        //Mise en viewState des critères de tri
        $this->setViewState('critereTri', $this->critereTri);
        $this->setViewState('triAscDesc', $this->triAscDesc);

        //Remplissage du tabeau des Etablissements
        $this->displayEtablissements();

        // Re-affichage du TActivePanel
        $this->panelEtablissements->render($param->getNewWriter());
    }

    /**
     * recuperer la collection trié selon le critère de trie.
     *
     * @param array  $dataSource  le tableau des données
     * @param string $critere
     * @param string $typeCritere
     *
     * @return array de données triées
     *
     * @author Oussama SALMANI <oussama.salmani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    private function getDataSourceTriee($dataSource, $critere, $typeCritere = null)
    {
        $this->setCritereAndTypeTri($critere);

        return Atexo_Util::sortArrayOfObjects($dataSource, $critere, $this->triAscDesc, $typeCritere);
    }

    /**
     * Définit les critères et les types de tri.
     *
     * @param string $critere
     *
     * @return void
     *
     * @author Oussama SALMANI <oussama.salmani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    private function setCritereAndTypeTri($critere)
    {
        if ($this->critereTri != $critere || !$this->critereTri) {
            $this->triAscDesc = 'ASC';
        } else {
            if ('ASC' == $this->triAscDesc) {
                $this->triAscDesc = 'DESC';
            } else {
                $this->triAscDesc = 'ASC';
            }
        }
        $this->critereTri = $critere;
    }

    /**
     * Permet de changer le nombre de resultat a afficher par page.
     *
     * @param $sender
     * @param $param
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function changePagerLenght($sender, $param)
    {
        $pageSize = null;
        switch ($sender->ID) {
            case 'nbResultsBottom':
                $pageSize = $this->nbResultsBottom->getSelectedValue();
                $this->nbResultsTop->setSelectedValue($pageSize);
                break;
            case 'nbResultsTop':
                $pageSize = $this->nbResultsTop->getSelectedValue();
                $this->nbResultsBottom->setSelectedValue($pageSize);
                break;
        }
        self::updatePagination($pageSize, $this->pageNumberTop->Text);
        self::populateData();
    }

    /**
     * Permet de mettre a jour la pagination.
     *
     * @param int    $pageSize la taille de la page
     * @param Integr $numPage  le numero de la page
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function updatePagination($pageSize, $numPage)
    {
        $nombreElement = count($this->getEtablissementsList());

        $this->repeaterEtablissements->PageSize = $pageSize;
        $this->nombrePageTop->Text = ceil($nombreElement / $this->repeaterEtablissements->PageSize);
        $this->nombrePageBottom->Text = ceil($nombreElement / $this->repeaterEtablissements->PageSize);
        if ($numPage > $this->nombrePageTop->Text) {
            $numPage = $this->nombrePageTop->Text;
        }
        $this->repeaterEtablissements->setCurrentPageIndex($numPage - 1);
        $this->pageNumberBottom->Text = $numPage;
        $this->pageNumberTop->Text = $numPage;
        //$this->setNbrElementRepeater($this->getViewState("nbrElements"));
    }

    /**
     * Permet d'afficher les etablissements selon la pagination.
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function populateData()
    {
        $allEtablissements = $this->getEtablissementsList();
        if ($this->isPagination()) {
            $offset = $this->repeaterEtablissements->CurrentPageIndex * $this->repeaterEtablissements->PageSize;
            $limit = $this->repeaterEtablissements->PageSize;

            if ($offset + $limit > $this->repeaterEtablissements->getVirtualItemCount()) {
                $limit = $this->repeaterEtablissements->getVirtualItemCount() - $offset;
            }
            $dataEtablissement = [];
            $value = current($allEtablissements);
            for ($i = 1; $i < $offset + 1; ++$i) {
                $value = next($allEtablissements);
            }
            $nbreElements = count($allEtablissements);
            for ($i = $offset; $i < $nbreElements && $i < ($limit + $offset); ++$i) {
                $dataEtablissement[$value->getCodeEtablissement()] = $value;
                $value = next($allEtablissements);
            }
        } else {
            $dataEtablissement = $allEtablissements;
        }
        if (count($dataEtablissement)) {
            self::showComponent();
            $this->repeaterEtablissements->DataSource = $dataEtablissement;
            $this->repeaterEtablissements->DataBind();
        } else {
            self::hideComponent();
        }
    }

    /**
     * Permet d'afficher les elements de pagination.
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function showComponent()
    {
        $this->PagerTop->visible = true;
        $this->PagerBottom->visible = true;
        $this->pageNumberBottom->visible = true;
        $this->pageNumberTop->visible = true;
        $this->labelSlashBottom->visible = true;
        $this->nombrePageTop->visible = true;
        $this->nombrePageBottom->visible = true;
        $this->nbResultsTop->visible = true;
        $this->labelAfficherBottom->visible = true;
        $this->nbResultsBottom->visible = true;
        $this->resultParPageBottom->visible = true;
        $this->panelBouttonGotoPageBottom->visible = true;
        $this->panelBouttonGotoPageTop->visible = true;
    }

    /**
     * Permet de cacher les elements de pagination.
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function hideComponent()
    {
        $this->PagerTop->visible = false;
        $this->PagerBottom->visible = false;
        $this->pageNumberBottom->visible = false;
        $this->pageNumberTop->visible = false;
        $this->labelSlashBottom->visible = false;
        $this->nombrePageTop->visible = false;
        $this->nombrePageBottom->visible = false;
        $this->nbResultsTop->visible = false;
        $this->labelAfficherBottom->visible = false;
        $this->nbResultsBottom->visible = false;
        $this->resultParPageBottom->visible = false;
        $this->panelBouttonGotoPageBottom->visible = false;
        $this->panelBouttonGotoPageTop->visible = false;
        $this->nbrElementRepeater = 0;
        $this->repeaterEtablissements->DataSource = [];
        $this->repeaterEtablissements->DataBind();
    }

    /**
     * Permet de naviguer entre les page.
     *
     * @param $sender
     * @param $param
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function goToPage($sender, $param)
    {
        $numPage = null;
        switch ($sender->ID) {
            case 'DefaultButtonTop':
                $numPage = $this->pageNumberTop->Text;
                break;
            case 'DefaultButtonBottom':
                $numPage = $this->pageNumberBottom->Text;
                break;
        }
        self::toPage($numPage);
    }

    /**
     * Permet d'aller a une page precise.
     *
     * @param int $numPage le numero de la page
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function toPage($numPage)
    {
        if ((new Util())->isEntier($numPage)) {
            if ($numPage >= $this->nombrePageTop->Text) {
                $numPage = $this->nombrePageTop->Text;
            } elseif ($numPage <= 0) {
                $numPage = 1;
            }
            self::updatePagination($this->nbResultsTop->getSelectedValue(), $numPage);
            self::populateData();
        } else {
            $this->pageNumberTop->Text = $this->repeaterEtablissements->CurrentPageIndex + 1;
            $this->pageNumberBottom->Text = $this->repeaterEtablissements->CurrentPageIndex + 1;
        }
    }

    /**
     * Permet de changer la page directement par son numero.
     *
     * @param $sender
     * @param $param
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function pageChanged($sender, $param)
    {
        $this->repeaterEtablissements->CurrentPageIndex = $param->NewPageIndex;
        $this->setNbrElementRepeater($this->getViewState('nbrElements'));
        self::updatePagination($this->nbResultsTop->getSelectedValue(), $param->NewPageIndex + 1);
        self::populateData();
    }

    /**
     * Permet de recuperer le picto pour definir mon etablissement.
     *
     * @param int $siretMonEtablissement le siret de mon etablissement
     *
     * @return string picto
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getPictoMonEtablissement($siretMonEtablissement)
    {
        if ($siretMonEtablissement == $this->getSiretMonEtablissement()) {
            return 'picto-check-grey.png';
        }

        return false;
    }

    /**
     * Permet de recuperer le text pour definir mon etablissement.
     *
     * @param int $siretMonEtablissement le siret de mon etablissement
     *
     * @return string alt
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getAltMonEtablissement($siretMonEtablissement)
    {
        if ($siretMonEtablissement == $this->getSiretMonEtablissement()) {
            return Prado::localize('TEXT_MON_ETABLISSEMENT');
        }

        return false;
    }

    /**
     * Permet de recuperer le picto pour definir le siege social de l'entreprise.
     *
     * @param int $siretSiegeSocial le siret de mon etablissement
     *
     * @return string picto
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getPictoSiegeEntreprise($siretSiegeSocial)
    {
        if ($siretSiegeSocial == $this->getSiretSiegeSocial()) {
            return 'picto-siege-social.png';
        }

        return false;
    }

    /**
     * Permet de recuperer le text pour definir le siege social de l'entreprise.
     *
     * @param int $siretSiegeSocial le siret de mon etablissement
     *
     * @return string alt
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getAltSiege($siretSiegeSocial)
    {
        if ($siretSiegeSocial == $this->getSiretSiegeSocial()) {
            return Prado::localize('TEXT_SIEGE_SOCIAL');
        }

        return false;
    }

    /**
     * Permet de recuperer le picto pour precise si l'etablissement est inscrit sur les annuaires des portails Défense.
     *
     * @param int $inscriDefense le siret de mon etablissement
     *
     * @return string picto
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getPictoInscritDefense($inscriDefense)
    {
        if ($inscriDefense) {
            return 'picto-portails-defense.png';
        }

        return false;
    }

    /**
     * Permet de recuperer le text pour precise si l'etablissement est inscrit sur les annuaires des portails Défense.
     *
     * @param int $inscriDefense
     *
     * @return string alt
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getAltinscritDefense($inscriDefense)
    {
        if ($inscriDefense) {
            return Prado::localize('TEXT_ETABLISSEMENT_INSCRIT_DEFENSE');
        }

        return false;
    }

    /**
     * vide le formulaire de l'etablissement.
     *
     * @param mixed $sender
     * @param mixed $param
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function viderInfosEtablissement($sender, $param)
    {
        $this->codeEtablissement->Text = '';
        $this->EtablissementAddress1->Text = '';
        $this->Etablissementaddress2->Text = '';
        $this->cpEtablissement->Text = '';
        $this->villeEtablissement->Text = '';
        $this->paysEtablissement->Text = '';
        $this->tvaIntracommunautaire->Text = '';
        $this->inscriptionAnnuaire->checked = false;
        $this->scriptJs->text = "<script>
									document.getElementById('".$this->Action->getClientId()."').value = 'Add';
									//openModal('modal-etablissement','popup-small2');
									KTJS.pages_entreprise_common.common.openModal('#modalEtablissement');
								</script>";
    }

    /**
     * vide le formulaire de l'etablissement.
     *
     * @param mixed $sender
     * @param mixed $param
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function viderInfosEtablissementSgmap($sender, $param)
    {
        $this->codeEtablissementSgmap->Text = '';
        $this->scriptJs->Text = '';
        $this->inscriptionAnnuaireSgmap->checked = false;
        $this->scriptJs->text = "<script>
									//openModal('modal-etablissementSgmap','popup-small2');
									KTJS.pages_entreprise_common.common.openModal('#modalEtablissementSgmap');
								</script>";
    }

    /**
     * Permet d'enregister un établissement ds le VS.
     *
     * @param mixed $sender
     * @param mixed $param
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function saveEtablissementSgmap($sender, $param)
    {
        $etablissementVo = Atexo_Entreprise::synchroniserEntrepriseEtablissementAvecSGMAP($this->getSirenEntreprise().$this->codeEtablissementSgmap->Text, ($this->getIsEntrepriseLocale()) ? '1' : '2', $modeSynchro);
        //recuperation de l'etablissement depui du WS SGMAP => OK
        if ($etablissementVo instanceof Atexo_Entreprise_EtablissementVo) {
            if ($this->inscriptionAnnuaireSgmap->checked) {
                $etablissementVo->setInscritAnnuaireDefense('1');
            } else {
                $etablissementVo->setInscritAnnuaireDefense('0');
            }
            $etablissements = $this->getEtablissementsList();
            $etablissements[$etablissementVo->getCodeEtablissement()] = $etablissementVo;
            $this->setEtablissementsList($etablissements);
            if ('1' == $etablissementVo->getEstSiege()) {
                $this->updateSiretSiegeSocialEse($etablissementVo->getCodeEtablissement());
            }
            $this->scriptJs->text = "<script>KTJS.pages_entreprise_common.common.closeModal('#modalEtablissementSgmap');</script>";
            $param = new TCommandEventParameter('', $this->codeEtablissementSgmap->Text.'#pictoModifier');
            $this->chargerInfosEtablissementLectureOnly($param);
        } elseif ('Objet Synchronise' === $etablissementVo) {
            $param = new TCommandEventParameter('', $this->codeEtablissementSgmap->Text.'#pictoModifier');
            $this->scriptJs->text = "<script>KTJS.pages_entreprise_common.common.closeModal('#modalEtablissementSgmap');</script>";
            $this->chargerInfosEtablissementLectureOnly($param);
        } elseif ('Objet non synchronise' === $etablissementVo) {
            $codeEtab = $this->codeEtablissementSgmap->Text;
            $this->messageAvertissement->setMessage(Prado::localize('AVERTISSEMENT_ETABLISSEMENT_NON_RECONNU'));
            $this->scriptJs->text = "<script>
										KTJS.pages_entreprise_common.common.closeModal('#modalEtablissementSgmap');
										document.getElementById('".$this->Action->getClientId()."').value = 'Add';
										KTJS.pages_entreprise_common.common.openModal('#modalEtablissement');
										document.getElementById('".$this->codeEtablissement->getClientId()."').value = '".$codeEtab."';
										document.getElementById('".$this->panelMessageAvertissement->getClientId()."').style.display = '';

									</script>";

            $this->viderInfosEtablissement($sender, $param);
        } else {//saisie manuelle des donnees de l'etablissement
            $codeEtab = $this->codeEtablissementSgmap->Text;
            $inscriptionAnnu = $this->inscriptionAnnuaireSgmap->checked;
            $this->messageAvertissement->setMessage(Prado::localize('AVERTISSEMENT_DISPONIBLITE_IDENTIFICATION_AUTOMATIQUE_ENTREPRISE'));
            $this->scriptJs->text = "<script>

										//J('.modal-etablissementSgmap').dialog('close');
										KTJS.pages_entreprise_common.common.closeModal('#modalEtablissementSgmap');
										document.getElementById('".$this->Action->getClientId()."').value = 'Add';
										KTJS.pages_entreprise_common.common.openModal('#modalEtablissement');
										//openModal('modal-etablissement','popup-small2',document.getElementById('".$this->ajouterEtablissementSGMAP->getClientId()."'));
										document.getElementById('".$this->codeEtablissement->getClientId()."').value = '".$codeEtab."';
										document.getElementById('".$this->inscriptionAnnuaire->getClientId()."').checked = '".$inscriptionAnnu."';
										document.getElementById('".$this->panelMessageAvertissement->getClientId()."').style.display = '';
									</script>";
            $this->viderInfosEtablissement($sender, $param);
        }
    }

    /**
     * @param $param
     *
     * @return bool
     */
    public function deleteLastEtablissement($param)
    {
        $etablissements = $this->getEtablissementsList();

        if (isset($etablissements[$param])) {
            unset($etablissements[$param]);
            $this->setEtablissementsList($etablissements);
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));

            CommonTEtablissementPeer::doDelete([$param], $connexion);
        }
    }

    /**
     * @param $sender
     * @param $param
     * @param $old
     */
    public function saveEtablissementApresModification($param, $old)
    {
        $etablissementVo = Atexo_Entreprise::synchroniserEntrepriseEtablissementAvecSGMAP($this->getSirenEntreprise().$this->codeEtablissement->Text, ($this->getIsEntrepriseLocale()) ? '1' : '2', $modeSynchro);
        if ($etablissementVo instanceof Atexo_Entreprise_EtablissementVo) {
            $etablissements = $this->getEtablissementsList();
            $etablissements[$etablissementVo->getCodeEtablissement()] = $etablissementVo;
            $this->setEtablissementsList($etablissements);
            $this->scriptJs->text = "<script>KTJS.pages_entreprise_common.common.closeModal('#modalEtablissementModify');</script>";
            $this->chargerInfosEtablissementLectureOnly($param);
            if ($old != $etablissementVo->getCodeEtablissement()) {
                $this->deleteLastEtablissement($old);
            }
        } else {
            $this->editEtablissement($this->codeEtablissement->Text);
        }
    }

    /**
     * mettre la liste des id des etablissements a supprimer ds le VS.
     *
     * @param array $idsEtabToDelete liste des id des etablissement
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function setListeIdsEtabToDelete($idsEtabToDelete)
    {
        $this->page->setViewState('ListeIdsEtabToDelete', $idsEtabToDelete);
    }

    /**
     * recupere la liste des id des etablissements depuis le VS.
     *
     * @return array $idsEtabToDelete liste des id des etablissement
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getListeIdsEtabToDelete()
    {
        return $this->page->getViewState('ListeIdsEtabToDelete');
    }

    /**
     * remete la pipin de confirmation de suppression a l'etat initial.
     *
     * @return array $idsEtabToDelete liste des id des etablissement
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function resetModalDelet($sender, $param)
    {
        $this->scriptJs->text = "<script>
									document.getElementById('".$this->panelConfirmDelete->getClientId()."').style.display = '';
									document.getElementById('buttonCancel').style.display = '';
									document.getElementById('".$this->cofirmSupression->getClientId()."').style.display = '';
									document.getElementById('".$this->panelMessageDelete->getClientId()."').style.display = 'none';
									document.getElementById('".$this->buttonClose->getClientId()."').style.display = 'none';
									KTJS.pages_entreprise_common.common.closeModal('#modalConfirmationSuppression');
								</script>";
    }

    /**
     * Permet de valider si l'etablissement est deja ajoute.
     *
     * @param $sender
     * @param $param
     *
     * @return string alt
     *
     * @author AMAL EL BEKKAOUI <amal.elbekkaoui@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function validateIfCodeExiste($sender, $param)
    {
        $listeEtablissements = $this->getEtablissementsList();
        if ('Add' == $this->Action->value) {
            if (isset($listeEtablissements[$param->value])) {
                $param->IsValid = false;
                $this->showError();
            }
        } elseif ('Modify' == $this->Action->value) {
            $codeEtabSaisi = $this->codeEtablissement->Text;
            if ($this->idToModify->value != $codeEtabSaisi && isset($listeEtablissements[$codeEtabSaisi])) {
                $param->IsValid = false;
                $this->showError();
            }
        }
    }

    /**
     * affiche un message d'erreur.
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function showError()
    {
        $this->scriptJs->Text = "<script>document.getElementById('divValidationSummaryEtablissement').style.display='';</script>";
        $this->divValidationSummaryEtablissement->addServerError(Prado::localize('MESSAGE_CODE_ETABLISSEMENT_EXISTE'), false);
    }

    /**
     * modifie la valeur de [$optionAdd].
     *
     * @param bool $optionAdd la valeur a mettre
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function setOptionAdd($optionAdd)
    {
        $this->optionAdd = $optionAdd;
    }

    /**
     * recupere la valeur du [optionAdd].
     *
     * @return bool la valeur courante [optionAdd]
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getOptionAdd()
    {
        return $this->optionAdd;
    }

    /**
     * retourne la class du composant.
     *
     * @return string class
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getClassComp()
    {
        if ($this->optionAdd) {
            return 'title';
        }

        return 'title-toggle-open';
    }

    /**
     * charge les informations de l'etablissement dans le formulaire.
     *
     * @param mixed $sender
     * @param mixed $param
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function openModalModifyInscriptionDefence($sender, $param)
    {
        $attr = explode('#', $param->CommandParameter);
        $idToModify = $attr[0];
        $etablissements = $this->getEtablissementsList();
        $etablissement = $etablissements[$idToModify];
        $this->codeEtablissementDefence->Text = $etablissement->getCodeEtablissement();
        if ($etablissement->getInscritAnnuaireDefense()) {
            $this->inscriptionDefence->checked = true;
        } else {
            $this->inscriptionDefence->checked = false;
        }
        $this->scriptJs->text = "<script>
									 KTJS.pages_entreprise_common.common.openModal('#modalEtablissementDefence');
								</script>";
    }

    /**
     * Ouvrir le modal de modif selon l'établissement si il est synchronisé ou pas.
     *
     * @param $sender
     * @param $param
     */
    public function openModalModificationEtablissement($sender, $param)
    {
        $idEntreprise = null;
        $this->scriptJs->text = '';
        $attr = explode('#', $param->CommandParameter);
        $idToModify = $attr[0];
        $etablissements = $this->getEtablissementsList();
        $etablissement = $etablissements[$idToModify];

        //get the Id Entreprise From etablissement déjà créés
        if (
            $etablissement instanceof Atexo_Entreprise_EtablissementVo
            && null !== $etablissement->getIdEntreprise()
        ) {

            $idEntreprise = $etablissement->getIdEntreprise();
            $entrepriseQuery = new EntrepriseQuery();
            $entreprise = $entrepriseQuery->getEntrepriseById($idEntreprise);

            if ($entreprise->getSaisieManuelle()) {
                $this->isEntrepriseSynchro = 1;
            } else {
                $this->isEntrepriseSynchro = 0;
            }
        } else {
            $this->isEntrepriseSynchro = 1;
        }

        switch ($etablissement->getSaisieManuelle()) {
            case 0:
                $this->chargerInfosEtablissementLectureOnly($param);
                break;
            case 1:
                $this->chargerInfosEtablissement($sender, $param);
                break;
        }
    }

    /**
     * Permet de mettre  our l'information description defence.
     *
     * @param mixed $sender
     * @param mixed $param
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function saveInscriptionDefence($sender, $param)
    {
        if ($this->Page->IsValid) {
            $index = $param->CommandParameter;
            $etablissements = $this->getEtablissementsList();
            $etablissementVo = $etablissements[$index];
            if ($etablissementVo instanceof Atexo_Entreprise_EtablissementVo) {
                if ($this->inscriptionDefence->checked) {
                    $etablissementVo->setInscritAnnuaireDefense('1');
                } else {
                    $etablissementVo->setInscritAnnuaireDefense('0');
                }
                $etablissements[$etablissementVo->getCodeEtablissement()] = $etablissementVo;
                $this->setEtablissementsList($etablissements);
            }
            $this->scriptJs->text = "<script>J('.modal-etablissementDefence').dialog('close');</script>";
        }
    }

    /**
     * @return bool
     */
    public function isResponsive()
    {
        return $this->responsive;
    }

    /**
     * @param bool $responsive
     */
    public function setResponsive($responsive)
    {
        $this->responsive = $responsive;
    }

    /**
     * @return bool
     */
    public function isPagination()
    {
        return $this->pagination;
    }

    /**
     * @param bool $pagination
     */
    public function setPagination($pagination)
    {
        $this->pagination = $pagination;
    }

    public function getInscritByEtablissement($idEntreprise, $idEtablissement, $count = false)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $inscritQuery = new CommonInscritQuery();
        if ($count) {
            $return = $inscritQuery->filterByEntrepriseId($idEntreprise)
                        ->filterByIdEtablissement($idEtablissement)
                        ->count($connexion);
        } else {
            $return = $inscritQuery->filterByEntrepriseId($idEntreprise)
                        ->filterByIdEtablissement($idEtablissement)
                        ->find($connexion);
        }

        return $return;
    }
}
