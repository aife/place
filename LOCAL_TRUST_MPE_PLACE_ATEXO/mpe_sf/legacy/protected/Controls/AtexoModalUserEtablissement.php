<?php

namespace Application\Controls;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonTEtablissementQuery;
use Application\Service\Atexo\Atexo_Config;
use Exception;

/**
 * Page de creation compte entreprise et compte utilisateur.
 *
 * @author Mouslim MITALI <anis.abid@atexo.com>
 * @copyright Atexo 2017
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class AtexoModalUserEtablissement extends MpeTTemplateControl
{
    public $etablissement;
    public $nombreElement;
    public $idEtablissement;
    public bool $visible = false;

    /**
     * @return mixed
     */
    public function getEtablissement()
    {
        return $this->etablissement;
    }

    /**
     * @param mixed $etablissement
     */
    public function setEtablissement($etablissement)
    {
        $this->etablissement = $etablissement;
    }

    /**
     * @return mixed
     */
    public function getIdEtablissement()
    {
        return $this->idEtablissement;
    }

    /**
     * @param mixed $idEtablissement
     */
    public function setIdEtablissement($idEtablissement)
    {
        $this->idEtablissement = $idEtablissement;
    }

    /**
     * @return mixed
     */
    public function getVisible($checkParents = true)
    {
        return $this->visible;
    }

    /**
     * @param mixed $visible
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;
    }

    public function getCheckedInscritAnnuaireDefense()
    {
        if ($this->getEtablissement()->getInscritAnnuaireDefense()) {
            return 'checked';
        }

        return '';
    }

    public function updateAction()
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $etablissement = CommonTEtablissementQuery::create()->findOneByIdEtablissement($this->getIdEtablissement(), $connexion);
        $this->setEtablissement($etablissement);
    }

    public function submitAction()
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $etablissement = CommonTEtablissementQuery::create()->findOneByIdEtablissement($this->getIdEtablissement(), $connexion);

        $etablissement->setCodeEtablissement($_REQUEST['codeEtablissement']);
        $etablissement->setAdresse($_REQUEST['EtablissementAddress1']);
        $etablissement->setAdresse2($_REQUEST['Etablissementaddress2']);
        $etablissement->setCodePostal($_REQUEST['codePostalEtablissement']);
        $etablissement->setVille($_REQUEST['villeEtablissement']);
        $etablissement->setPays($_REQUEST['paysEtablissement']);
        $etablissement->setInscritAnnuaireDefense(isset($_REQUEST['inscritAnnuaireDefenseEtablissement']));

        try {
            $etablissement->save($connexion);
            $return = ['idEtablissement' => $etablissement->getIdEtablissement()];
            echo json_encode($etablissement->toArray(), JSON_THROW_ON_ERROR);
        } catch (Exception $e) {
            $connexion->rollback();
            throw $e;
        }

        return false;
    }
}
