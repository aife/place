<!--[if !IE]>-->
    <object
        classid="java:fr.atexo.signature.applet.<%=$this->getClassIdApplet()%>.class"
        id="<%=$this->getMainClass()%>"
          name="<%=$this->getMainClass()%>"
          type="application/x-java-applet;version=1.6"
          height="1" width="1" >
          <param name="mayscript" value="true"/>
          <%=$this->getParamJavaArguments()%>
        <param name="code" value="<%=$this->getMainClass()%>" />
        <param name="codebase" value="<%=$this->getCodeBaseApplet()%>" />
        <param name="cache_option" value="Plugin"/>
        <param name="cache_archive" value="<%=$this->getListeJarsApplet()%>" />
        <param name="locale" value="fr"/>
        <param name="ecraserJavaPolicy" value="true"/>
        <%=$this->getParams()%>
    </object>
    <!--<![endif]-->
    <!--[if IE]>
    <object classid="clsid:8AD9C840-044E-11D1-B3E9-00805F499D93"
        id="<%=$this->getMainClass()%>"
        name="<%=$this->getMainClass()%>"
        type="application/x-java-applet;version=1.6"
        height="1" width="1" >
        <param name="java_code" value="fr.atexo.signature.applet.<%=$this->getClassIdApplet()%>.class" />
        <param name="mayscript" value="true"/>
         <%=$this->getParamJavaArguments()%>
        <param name="code" value="<%=$this->getMainClass()%>" />
        <param name="codebase" value="<%=$this->getCodeBaseApplet()%>" />
        <param name="cache_option" value="Plugin" />
    	<param name="cache_archive" value="<%=$this->getListeJarsApplet()%>" />
        <param name="locale" value="fr"/>
        <param name="ecraserJavaPolicy" value="true"/>
        <%=$this->getParams()%>
    </object>
    <![endif]-->
