<?php

namespace Application\Controls;

use Application\Service\Atexo\Controller\Atexo_Controller_Front;
use Prado\Prado;
use Prado\Web\UI\ActiveControls\TActiveImageButton;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PictoDeleteActive extends TActiveImageButton
{
    public $_parentPage;
    public string $_activate = 'true';
    public string $_clickable = 'true';

    public function onLoad($param)
    {
        if ('true' == $this->_activate) {
            $this->setImageUrl(Atexo_Controller_Front::t().'/images/picto-supprimer-big.gif');
        } else {
            $this->setImageUrl(Atexo_Controller_Front::t().'/images/picto-supprimer-big-inactive.gif');
        }

        if ('false' == $this->_clickable) {
            $this->Attributes->onclick = 'return false;';
        }

        if ('ChoixInvites' == $this->getParentPage()) {
            $this->setAlternateText(Prado::localize('ICONE_SUPPRIMER_DE_MA_SELECTION'));
            $this->setToolTip(Prado::localize('ICONE_SUPPRIMER_DE_MA_SELECTION'));
        } elseif ('DetailConsultation' == $this->getParentPage()) {
            $this->setAlternateText(Prado::localize('TEXT_SUPPRIME_ANNONCE'));
            $this->setToolTip(Prado::localize('TEXT_SUPPRIME_ANNONCE'));
        } else {
            $this->setAlternateText(Prado::localize('DEFINE_SUPPRIMER'));
            $this->setToolTip(Prado::localize('DEFINE_SUPPRIMER'));
        }
    }

    public function getParentPage()
    {
        return $this->_parentPage;
    }

    public function setParentPage($value)
    {
        $this->_parentPage = $value;
    }

    public function getActivate()
    {
        return $this->_activate;
    }

    public function setActivate($bool)
    {
        $this->_activate = $bool;
    }

    public function getClickable()
    {
        return $this->_clickable;
    }

    public function setClickable($value)
    {
        $this->_clickable = $value;
    }
}
