<com:TActivePanel ID="panelPiece" style="display:none" >
	<!--Debut bloc pieces DCE-->
	<div class="toggle-panel">
	<div class="form-field">
		<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
		<div class="content">
				<h2 class="toggle-btn">
					<a href="javascript:void(0);" onclick="togglePanel(this,'panel_<%=$this->getClientId()%>');" class="title-toggle-open"><%=$this->getTitre()%></a>
				</h2>
				<div id="panel_<%=$this->getClientId()%>" style="display: block;" class="panel">
							
<!--				<div class="spacer-small"></div>-->
				<table summary="<%=$this->getTitre()%>" class="table-results tablau-pieces-marche">
					<caption><%=$this->getTitre()%></caption>
				<com:TRepeater ID="tablePiecesMarche" EnableViewState="true" >
				<prop:HeaderTemplate>
					<thead>
						<tr>
							<th class="top" colspan="5"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></th>
						</tr>
						<tr>
							<th class="check-col" id="dce_checkAll" abbr="Tous"><label for="dce_selectAll" style="display:none;"><%#Prado::localize('TEXT_TOUT_SELECTIONNER')%></label>
								<com:TCheckBox ID="dce_selectAll" Attributes.title="<%#Prado::localize('TEXT_TOUS_AUCUN')%>" Attributes.onclick="isCheckedCheckAll('<%#$this->dce_selectAll->getClientId()%>','<%#$this->TemplateControl->getClientId()%>');" />
							</th>
							<th class="col-300" id="dce_col_01"><com:TTranslate>TEXT_NOM_DE_LA_PIECE</com:TTranslate></th>
							<th class="col-70" id="dce_col_02"><com:TTranslate>TEXT_MISE_A_DISPOSITION</com:TTranslate></th>
							<th class="col-70" id="dce_col_03"><com:TTranslate>TEXT_STATUT_RECUPERATION</com:TTranslate></th>
							<th class="actions-small col-40" id="dce_col_actions"><com:TTranslate>DEFINE_ACTIONS</com:TTranslate></th>

						</tr>
					</thead>
				</prop:HeaderTemplate>
				<prop:ItemTemplate>
						<tr class="<%#(($this->ItemIndex%2==0)? '':'on')%>">
							<td class="check-col" headers="dce_checkAll" >
								<com:TCheckBox ID="piece_item"  Attributes.title="<%#Prado::localize('TEXT_DEFINE_SELECTIONNER')%>"  />
							</td>
							<td class="col-300" headers="dce_col_01">
								<com:TActivePanel id="panelAction" >	
									<com:THyperLink ID="linkDownloadAutrePiece"  Visible="<%#$this->Data->getType() ==  Application\Service\Atexo\Atexo_Config::getParameter('TYPE_AUTRE_PIECES')%>" Attributes.title="<%#Prado::localize('DEFINE_TELECHARGER_LA_PIECE')%> <%#$this->Data->getName()%>" NavigateUrl="index.php?page=Agent.DownloadAutrePieceMiseDisposition&idPiece=<%#base64_encode($this->Data->getId())%>"><%#$this->Data->getName()%></com:THyperLink>
									<com:THyperLink ID="linkDownloadDepotAttribu"  Visible="<%#$this->Data->getType() ==  Application\Service\Atexo\Atexo_Config::getParameter('TYPE_PIECES_DEPOT_ATTRIBUTAIRE')%>" Attributes.title="<%#Prado::localize('DEFINE_TELECHARGER_LA_PIECE')%> <%#$this->Data->getName()%>" NavigateUrl="<%#$this->Data->getUrlPiece()%>"><%#$this->Data->getName()%></com:THyperLink>
									<com:THyperLink ID="linkDownloadPublicite" Visible="<%#$this->Data->getType() ==  Application\Service\Atexo\Atexo_Config::getParameter('TYPE_PIECES_PUBLICITE')%>" Attributes.title="<%#Prado::localize('DEFINE_TELECHARGER_LA_PIECE')%> <%#$this->Data->getName()%>" NavigateUrl="index.php?page=Agent.DownloadAvisJAL&idAvis=<%#$this->Data->getId()%>"><%#$this->Data->getName()%></com:THyperLink>
									<com:TLinkButton ID="linkDownloadDce" 	Visible="<%#$this->Data->getType() ==  Application\Service\Atexo\Atexo_Config::getParameter('TYPE_PIECES_DCE')%>" 	Attributes.title="<%#Prado::localize('DEFINE_TELECHARGER_LA_PIECE')%> <%#$this->Data->getName()%>"	CommandParameter="<%#$this->Data->getId()%>" OnClick="Page.PieceDce.getUrlNewDownload"><%#$this->Data->getName()%></com:TLinkButton >
									<com:TLinkButton ID="linkDownloadDocumentExterne" 	
									Visible="<%#$this->Data->getType() ==  Application\Service\Atexo\Atexo_Config::getParameter('TYPE_DOCUMENT_EXTERNE')%>"
									 	Attributes.title="<%#Prado::localize('DEFINE_TELECHARGER_LA_PIECE')%> <%#$this->Data->getName()%>"
									 	CommandParameter="<%#$this->Data->getIdBlob()%>"
									 	CommandName="<%#$this->Data->getName()%>"
									 	OnCommand="Page.documentsExternes.downloadDocExterneFile">
									 	<%#$this->Data->getName()%>
									 	</com:TLinkButton >
								    <div class="spacer-mini"></div>	
									<com:TDropDownList 
										id="dce_piece_type_doc" 
										CssClass="col-300" 
										Visible="<%#($this->Data->getStatutMiseDisposition() == '1')? false :true %>"
										Attributes.title="<%#Prado::localize('DEFINE_TEXT_TYPE_DOC')%>"
										DataSource="<%#$this->TemplateControl->chargerTypeDocument()%>"
									  > 
									</com:TDropDownList>
									<com:TActiveImage id="error"  ImageUrl='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif'  Attributes.alt='Champ obligatoire' Style="display:none" />
									<com:TLabel ID="type" Visible="<%#($this->Data->getStatutMiseDisposition() == '1') ? true : false %>" Text="<%#$this->TemplateControl->getTypeDocName($this->Data->getTypeDocument())%>"/>
									<com:TActiveHiddenField ID="idTypeDoc" Value="<%#$this->Data->getTypeDocument()%>"/>
									<com:TActiveHiddenField ID="id"  Value="<%#$this->Data->getId()%>"/>
									
								</com:TActivePanel>
							</td>
							<td class="col-70" headers="dce_col_02">
								<div class="picto-statut">
									<com:TImage ImageUrl="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-depot.gif" Visible="<%#($this->Data->getStatutMiseDisposition()==  Application\Service\Atexo\Atexo_Config::getParameter('STATUT_MISE_DISPOSITION')) ? true : false %>" Attributes.alt="<%#Prado::localize('TEXT_METTRE_DISPOSITION')%>" CssClass="float-left" />
								</div>
								<div class="info-date">
									<%#($this->Data->getStatutMiseDisposition() ==  Application\Service\Atexo\Atexo_Config::getParameter('STATUT_MISE_DISPOSITION')) ? ' : '. $this->Data->getDateMiseDisposition() : '-'%>
								</div>
							</td>
							<td class="col-70" headers="dce_col_03">
								<com:TPanel Visible="<%#($this->Data->getStatutRecuperation()==  Application\Service\Atexo\Atexo_Config::getParameter('STATUT_ERREUR_RECUPERATION')) ? true : false %>">
									<div class="picto-statut">
										<com:TImage ImageUrl="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-admissible.gif" Visible="<%#($this->Data->getStatutRecuperation()==  Application\Service\Atexo\Atexo_Config::getParameter('STATUT_SUCCES_RECUPERATION')) ? true : false %>" Attributes.alt="<%#Prado::localize('TEXT_STATUT_RECUPERATION')%>" CssClass="float-left" />
										<com:TImage ImageUrl="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-non-admissible.gif" Visible="<%#($this->Data->getStatutRecuperation()==  Application\Service\Atexo\Atexo_Config::getParameter('STATUT_ERREUR_RECUPERATION')) ? true : false %>" Attributes.alt="<%#Prado::localize('TEXT_STATUT_RECUPERATION')%>" CssClass="float-left" />
									</div>
									<div class="info-date">
										<%#($this->Data->getStatutRecuperation() == ( Application\Service\Atexo\Atexo_Config::getParameter('STATUT_SUCCES_RECUPERATION') ||  Application\Service\Atexo\Atexo_Config::getParameter('STATUT_ERREUR_RECUPERATION') )) ? ' : '. $this->Data->getDateRecuperation() : '-'%>
									</div>
									<img title="Info-bulle" alt="Info-bulle" class="picto-info" onmouseout="cacheBulle('infosRefus_dce_piece_5')" onmouseover="afficheBulle('infosRefus_dce_piece_5', this)" src="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" />
									<div id="infosRefus_dce_piece_5" class="info-bulle" onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();">
										<div><%#$this->Data->getMessage()%></div>
									</div>
									<com:TActiveHiddenField ID="idsBlob" Value="<%#$this->Data->getIdBlob()%>"/>
									<com:TActiveHiddenField ID="id_type" Value="<%#$this->Data->getType()%>"/>
								</com:TPanel>
							</td>
							<td class="actions-small col-40" headers="dce_col_actions">
									   	<com:TActiveLinkButton  
										   	id="miseDispo" 
										   	Visible="<%#($this->Data->getStatutMiseDisposition()!=  Application\Service\Atexo\Atexo_Config::getParameter('STATUT_MISE_DISPOSITION')) && (!$this->Data->getStatutRecuperation ())? true : false %>"
										   	CommandParameter="<%#$this->Data->getId().'#'.$this->Data->getType().'#'.$this->Data->getIdBlob().'#'%>"
										   	OnCallBack="Parent.SourceTemplateControl.mettreAdisponible"
										   	Attributes.onclick="getSelectedTypeDoc('<%#$this->dce_piece_type_doc->getClientId()%>','<%#$this->TemplateControl->idType->getClientId()%>');getIdClientErreur('<%#$this->TemplateControl->idClientErreur->getClientId()%>','<%#$this->error->getClientId()%>');"
										   	ValidationGroup="validationTypeDoc<%#$this->TemplateControl->getClientId()%>"
										   	Attributes.title="<%=Prado::localize('TEXT_METTRE_DISPOSITION_CETTE_PIECE')%>"
										   	
										   	>
										   	<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-acces-consultation.gif" alt="<%=Prado::localize('TEXT_METTRE_DISPOSITION')%>" />
												<prop:ClientSide.OnLoading>showLoader();</prop:ClientSide.OnLoading>
						                		<prop:ClientSide.OnFailure>showLoader();</prop:ClientSide.OnFailure>
						                		<prop:ClientSide.OnSuccess>showLoader();</prop:ClientSide.OnSuccess>
										</com:TActiveLinkButton>
										<com:TActiveLinkButton 
											Visible="<%#(!$this->Data->getStatutRecuperation()) && ($this->Data->getStatutMiseDisposition() ==  Application\Service\Atexo\Atexo_Config::getParameter('STATUT_MISE_DISPOSITION')) ? true : false%>"
										   	id="AnnulerMiseDispo" 
										   	ValidationGroup="validation<%#$this->Data->getId()%>"
										    CommandParameter="<%#$this->Data->getId().'#'.
											$this->Data->getType().'#'.
												$this->idTypeDoc->Value.'#'
												%>"
										   	OnCallBack="Parent.SourceTemplateControl.annulerMettreAdisponible"
										   	Attributes.title="<%=Prado::localize('TEXT_ANNULER_MISE_DISPOSITION_CETTE_PIECE')%>"
									   	>
									   	<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-desactiver.gif" alt="<%=Prado::localize('TEXT_ANNULER_MISE_DISPOSITION_CETTE_PIECE')%>" />
											<prop:ClientSide.OnLoading>showLoader();</prop:ClientSide.OnLoading>
					                		<prop:ClientSide.OnFailure>showLoader();</prop:ClientSide.OnFailure>
					                		<prop:ClientSide.OnSuccess>showLoader();</prop:ClientSide.OnSuccess>
										</com:TActiveLinkButton>
										<com:TActiveLinkButton 
											Visible="<%#(!$this->Data->getStatutMiseDisposition() && ($this->Data->getType() ==  Application\Service\Atexo\Atexo_Config::getParameter('TYPE_AUTRE_PIECES'))) ? true : false%>"
										   	id="SupprimerAutrePiece" 
										    CommandParameter="<%#$this->Data->getId()%>"
										   	OnCallBack="Page.AutresPiece.supprimerAutrePiece"
										    Attributes.title="<%=Prado::localize('TEXT_SUPPRIMER_PIECE')%>"
										   	>
										   	<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-supprimer-big.gif" alt="<%=Prado::localize('TEXT_SUPPRIMER_PIECE')%>" />
											<prop:ClientSide.OnLoading>showLoader();</prop:ClientSide.OnLoading>
					                		<prop:ClientSide.OnFailure>showLoader();</prop:ClientSide.OnFailure>
					                		<prop:ClientSide.OnSuccess>showLoader();</prop:ClientSide.OnSuccess>
										</com:TActiveLinkButton>
							</td>
						</tr>
					</prop:ItemTemplate>

				</com:TRepeater>
				</table>
				<com:TActivePanel id="panelAucuneAction" style = "display:none">
					<p><%=Prado::localize('DEFINE_AUCUNE_PIECES_DISPONIBLE')%></p>
				</com:TActivePanel>	
			   <div class="spacer-small"></div>	
			   	<!--Debut bloc ajout autre piecs-->
				<com:TActivePanel CssClass="form-bloc margin-0" style="display:<%=($this->getTypePiece() ==  Application\Service\Atexo\Atexo_Config::getParameter('TYPE_AUTRE_PIECES')) ? '': 'none'%>">
					<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
					<div class="content">
						<div class="spacer-small"></div>
						<div class="line">
							<com:AtexoFileUploadWithProgressBar id="uploadBar" titre="<%=Prado::localize('TEXT_AJOUTER_PIECE_REPONSE')%>"  fonctionJs="addAutresPiecesJointe()" />
							<com:TActiveButton 
							   	id="AddAutresPiece"
							   	OnCallBack="Page.AutresPiece.ajoutAutrePiece"
								Attributes.style="display:none"
							   	>
						    </com:TActiveButton>
						</div>
					</div>
					<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
				</com:TActivePanel>
				<!--Fin bloc ajout autre piecs-->			
		</div>
		</div>	
	 <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
</div>
<com:TLabel id="test_pload" style="display:none" />
<com:TActiveHiddenField ID="idType"/>
<com:TActiveHiddenField ID="idClientErreur"/>
<com:TActiveLabel ID="labelServerSide"/>
<com:TCustomValidator 
 	id="idValidationTypeDoc<%=$this->getClientId()%>"
	ValidationGroup="validationTypeDoc<%=$this->getClientId()%>"
	ErrorMessage="<%=Prado::localize('TEXT_ERREUR_SELECTION_TYPE_DOC_MISE_DISPOSITION')%>"
	ControlToValidate="idType" 
	onServerValidate="SourceTemplateControl.verifyTypeDoc"
	Display="Dynamic" Text=" " EnableClientScript="true">
	
</com:TCustomValidator>
</div>
</com:TActivePanel>