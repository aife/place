<?php

namespace Application\Controls;

use Application\Service\Atexo\Controller\Atexo_Controller_Front;
use Prado\Prado;
use Prado\Web\UI\ActiveControls\TActiveImageButton;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class ActivePictoSort extends TActiveImageButton
{
    private bool $_active = true;

    public function getActive()
    {
        return $this->_active;
    }

    public function setActive($active)
    {
        $this->_active = $active;
    }

    public function onLoad($param)
    {
        $this->setImageUrl(Atexo_Controller_Front::t().'/images/arrow-tri-off.gif');
        $this->setAlternateText(Prado::localize('ICONE_TRIER'));
        $this->setToolTip(Prado::localize('ICONE_TRIER'));
        if (!$this->getActive()) {
            $this->Attributes->onclick = 'return false;';
        }
    }
}
