<?php

namespace Application\Controls;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonReferentielConsultation;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Referentiel\Atexo_Referentiel_Referentiel;
use Application\Service\Atexo\Referentiel\Atexo_Referentiel_ReferentielVo;
use Prado\Prado;

class AtexoRefetielRadioCheck extends AtexoLtRefRadio
{
    /*
    * display composant ref consultation radio
    */
    public function afficherReferentielRadio($consultationId = null, $lot = null, $organisme = null, $defineValue = null, $postback = false)
    {
        if (!$this->Page->isPostBack || $postback) {
            self::remplirRepeaterReferentielRadio();
            foreach ($this->RepeaterReferentielRadio->Items as $Item) {
                $Item->idLot->afficherRadioLot($consultationId, $organisme);
                $IdRef = $Item->iDLibelle->Text;
                $codeRefPrinc = null;
                if (!$defineValue) {
                    if ($organisme) {
                        $refCons = (new Atexo_Referentiel_Referentiel())->getCommonLtReferentielByRefConsultationAndLot($consultationId, $lot, $IdRef, $organisme);
                        if ($refCons) {
                            $codeRefPrinc = $refCons->getValeurPrincipaleLtReferentiel();
                        }
                    }
                } else {
                    if ($defineValue[$IdRef]) {
                        $codeRefPrinc = $defineValue[$IdRef]->getValuePrinc();
                    }
                }
                if ('1' == $codeRefPrinc) {
                    $Item->OptionOui->setChecked(true);
                    $Item->OptionNon->setChecked(false);
                    $Item->labelReferentielRadio->SetText(Prado::localize('TEXT_OUI'));
                } elseif ('0' == $codeRefPrinc) {
                    $Item->OptionOui->setChecked(false);
                    $Item->OptionNon->setChecked(true);
                    $Item->labelReferentielRadio->SetText(Prado::localize('TEXT_NON'));
                }
                $Item->oldValue->SetValue($codeRefPrinc);
            }
        }
    }

    /*
    * enregister referentiel radio
    */
    public function saveRefLot($consultationId)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $organisme = Atexo_CurrentUser::getCurrentOrganism();
        foreach ($this->RepeaterReferentielRadio->Items as $Item) {
            if ($Item->refRadio->checked) {
                foreach ($Item->idLot->repeaterLot->Items as $Items) {
                    $IdRef = $Items->iDLibelle->Text;
                    if (true == $Items->OptionOui->Checked) {
                        $valeurPricLtRef = '1';
                    } elseif (true == $Items->OptionNon->Checked) {
                        $valeurPricLtRef = '0';
                    }
                    $CommonRefCons = (new Atexo_Referentiel_Referentiel())->getCommonLtReferentielByRefConsultationAndLot($consultationId, $Items->numLot->getText(), $IdRef, $organisme);
                    if (!$CommonRefCons) {
                        $CommonRefCons = new CommonReferentielConsultation();
                        $CommonRefCons->setConsultationId($consultationId);
                        $CommonRefCons->setIdLtReferentiel($IdRef);
                        $CommonRefCons->setOrganisme($organisme);
                        $CommonRefCons->setLot($Items->numLot->getText());
                    }
                    $CommonRefCons->setValeurPrincipaleLtReferentiel($valeurPricLtRef);
                    $CommonRefCons->save($connexionCom);
                }
            }
        }
    }

    public function getValueReferentielLotTextVo()
    {
        $valueFinal = [];
        foreach ($this->RepeaterReferentielRadio->Items as $Item) {
            if ($Item->refRadio->checked) {
                foreach ($Item->idLot->repeaterLot->Items as $Items) {
                    $value1 = [];
                    $refVo = new Atexo_Referentiel_ReferentielVo();
                    $refVo->setId($Items->iDLibelle->Text);
                    if ($Item->OptionOui->Checked) {
                        $refPrinc = '1';
                    } elseif ($Item->OptionNon->Checked) {
                        $refPrinc = '0';
                    }
                    $refVo->setValue($refPrinc);
                    $refVo->setValuePrinc('');
                    $refVo->setValueSecon('');
                    $refVo->setType($Item->typeRef->Text);
                    $refVo->setCodeLibelle($Items->codeLibelle->Text);
                    $refVo->setOldValue($Items->oldValue->Value);
                    $value1[$Items->iDLibelle->Text] = $refVo;
                    $valueFinal[$Items->numLot->Text] = $value1;
                }
            }
        }

        return $valueFinal;
    }
}
