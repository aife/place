<?php

namespace Application\Controls;

use Application\Propel\Mpe\CommonChorusEchange;
use Application\Propel\Mpe\CommonTChorusFicheModificative;
use Application\Propel\Mpe\CommonTChorusFicheModificativePjPeer;
use Application\Propel\Mpe\CommonTContratTitulaire;
use Application\Propel\Mpe\CommonValeurReferentiel;
use Application\Propel\Mpe\CommonValeurReferentielQuery;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_CommonReferentiel;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Chorus\Atexo_Chorus_Util;
use DateTime;
use Exception;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
 *
 * @version 1.0
 *
 * @since 2015-place
 *
 * @copyright Atexo 2015
 */
class AtexoFicheModificative extends MpeTTemplateControl
{
    protected bool|array $_typeFournisseur = false;
    protected ?\Application\Propel\Mpe\CommonTContratTitulaire $_contratObjet = null;

    protected ?array $_contratExecArray = null;
    protected ?\Application\Propel\Mpe\CommonChorusEchange $_chorusEchangeObjet = null;

    /**
     * recupere la valeur de [bgcolor].
     *
     * @return string la valeur courante [bgcolor]
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function getBgcolor()
    {
        if (!$this->getViewState('bgColor')) {
            $this->setViewState('bgColor', '#ffffff');
        } elseif ('#ffffff' == $this->getViewState('bgColor')) {
            $this->setViewState('bgColor', '');
        }

        return $this->getViewState('bgColor');
    }

    /**
     * recupere la valeur de [contratObjet].
     *
     * @return array la valeur courante [contratObjet]
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function getContratObjet()
    {
        $contratObjet = $this->_contratObjet;
        if (empty($contratObjet)) {
            $contratObjet = $this->getViewState('contratObjet');
        }

        return $contratObjet;
    }

    /**
     * modifie la valeur de [ContratObjet].
     *
     * @param CommonTContratTitulaire $contratObjet
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function setContratObjet($contratObjet)
    {
        $this->_contratObjet = $contratObjet;
        $this->setViewState('contratObjet', $contratObjet);
    }

    public function setContratExecArray($contratExecArray)
    {
        $this->_contratExecArray = $contratExecArray;
        $this->setViewState('contratExecArray', $contratExecArray);
    }

    public function getContratExecArray()
    {
        $contratExecArray = $this->_contratExecArray;
        if (empty($contratExecArray)) {
            $contratExecArray = $this->getViewState('contratObjet');
        }

        return $contratExecArray;
    }

    /**
     * recupere la valeur de [_chorusEchangeObjet].
     *
     * @return array la valeur courante [_chorusEchangeObjet]
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function getChorusEchangeObjet()
    {
        $chorusEchangeObjet = $this->_chorusEchangeObjet;
        if (empty($chorusEchangeObjet)) {
            $chorusEchangeObjet = $this->getViewState('chorusEchangeObjet');
        }

        return $chorusEchangeObjet;
    }

    /**
     * modifie la valeur de [_chorusEchangeObjet].
     *
     * @param CommonChorusEchange $chorusEchangeObjet
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2016
     */
    public function setChorusEchangeObjet($chorusEchangeObjet)
    {
        $this->_chorusEchangeObjet = $chorusEchangeObjet;
        $this->setViewState('chorusEchangeObjet', $chorusEchangeObjet);
    }

    /**
     * recupere la valeur de [ficheModificativeObjet].
     *
     * @return array la valeur courante [ficheModificativeObjet]
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function getFicheModificativeObjet()
    {
        return $this->getViewState('FicheModificativeObject');
    }

    /**
     * modifie la valeur de [ficheModificativeObjet].
     *
     * @param CommonTchorusFicheModificative $ficheModificativeObjet
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function setFicheModificativeObjet($ficheModificativeObjet)
    {
        $this->setViewState('FicheModificativeObject', $ficheModificativeObjet);
    }

    /**
     * recupere la valeur de [typeFournisseur].
     *
     * @return array la valeur courante [typeFournisseur]
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function getTypeFournisseur()
    {
        return $this->_typeFournisseur;
    }

    /**
     * modifie la valeur de [typeFournisseur].
     *
     * @param array $typeFournisseur la valeur a mettre
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function setTypeFournisseur($typeFournisseur)
    {
        $this->_typeFournisseur = $typeFournisseur;
    }

    /**
     * permet d'initialiser le composant.
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function onInitComposant()
    {
        self::chargerRepeaterFournisseur();
        self::displayPjFicheModificative();
        self::fillNombreFournisser();
        self::fillTypeModification();
    }

    /**
     * permet de charger le tableau des fournisseur.
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function chargerRepeaterFournisseur()
    {
        $dataSource = [];
        for ($j = 0; $j < 50; ++$j) {
            $dataSource[$j] = $j;
        }
        $this->repeatNouveauxFournisseurs->dataSource = $dataSource;
        $this->repeatNouveauxFournisseurs->dataBind();
    }

    /**
     * permet de charger la liste des types des fournisseurs.
     *
     * @return array $dataSourceTypeFournisseur
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function fillTypeFournisser()
    {
        if (!$this->getTypeFournisseur()) {
            $arrayTypeFournisseurs = (new Atexo_CommonReferentiel())->retrieveValeurReferentiel(Atexo_Config::getParameter('REFERENTIEL_TYPE_FOURNISSEUR'));
            $dataSourceTypeFournisseur = [];
            $dataSourceTypeFournisseur['0'] = Prado::localize('TEXT_SELECTIONNER').' ...';
            if (is_array($arrayTypeFournisseurs) && count($arrayTypeFournisseurs)) {
                foreach ($arrayTypeFournisseurs as $unTypeFournisseur) {
                    $dataSourceTypeFournisseur[$unTypeFournisseur->getId()] = $unTypeFournisseur->getLibelleValeurReferentiel();
                }
            }
            $this->setTypeFournisseur($dataSourceTypeFournisseur);

            return $dataSourceTypeFournisseur;
        }

        return $this->getTypeFournisseur();
    }

    /**
     * permet de charger la liste des types de modification.
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function fillTypeModification()
    {
        $arrayTypeModification = (new Atexo_CommonReferentiel())->retrieveValeurReferentiel(Atexo_Config::getParameter('REFERENTIEL_TYPE_MODIFICATION'));
        $dataSourceTypeModification = [];
        $dataSourceTypeModification['0'] = Prado::localize('TEXT_SELECTIONNER').' ...';
        if (is_array($arrayTypeModification) && count($arrayTypeModification)) {
            foreach ($arrayTypeModification as $unTypeModification) {
                $dataSourceTypeModification[$unTypeModification->getId()] = $unTypeModification->getLibelleValeurReferentiel();
            }
        }
        $this->typeModification->dataSource = $dataSourceTypeModification;
        $this->typeModification->dataBind();
    }

    /**
     * permet de charger la liste des nombre de fournisseurs possible.
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function fillNombreFournisser()
    {
        $arrayOptions = [];
        for ($i = 0; $i <= 50; ++$i) {
            $arrayOptions[$i] = $i;
        }
        $this->nbrNouveauxFournisseurs->dataSource = $arrayOptions;
        $this->nbrNouveauxFournisseurs->dataBind();
    }

    /**
     * permet d'ajouter une ficheModificative à l'echange.
     *
     * @param CommonChorusEchange $echangeChorus
     * @param PropelPDO           $connexion
     *
     * @return CommonTChorusFicheModificative $ficheModificative
     *
     * @author    Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version   1.0
     *
     * @since     2015-place
     *
     * @copyright Atexo 2015
     */
    public function addFicheModificative($echangeChorus, $connexion)
    {
        try {
            (new Atexo_Chorus_Util())->loggerInfos(" Creation Fiche Modificative \n");
            $ficheModificative = new CommonTChorusFicheModificative();
            $date = new DateTime();
            $dateCreation = $date->format('Y-m-d H:i:s');
            $ficheModificative->setIdEchange($echangeChorus->getId());
            $ficheModificative->setOrganisme($echangeChorus->getOrganisme());
            $ficheModificative->setDateCreation($dateCreation);
            $ficheModificative->save($connexion);

            return $ficheModificative;
        } catch (Exception $e) {
            $msgErreur = "Erreur lors de la creation de l'objet Fiche Modificative \n".$e->getMessage()."\n";
            Prado::log($msgErreur, TLogger::ERROR, 'ERROR');
            (new Atexo_Chorus_Util())->loggerErreur($msgErreur);
        }
    }

    /**
     * permet de mettre à jour l'objet CommonTChorusFicheModificative.
     *
     * @param PropelPDO $connexion
     *
     * @return CommonTChorusFicheModificative $ficheModificative
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function updateFicheModificative($connexion)
    {
        $arrayIdFiles = [];
        try {
            (new Atexo_Chorus_Util())->loggerInfos(" MAJ Fiche Modificative \n");
            $ficheModificative = $this->getFicheModificativeObjet();
            if ($ficheModificative instanceof CommonTChorusFicheModificative) {
                $ficheModificative->setTypeModification($this->typeModification->selectedValue);
                $ficheModificative->setDatePrevueNotification(Atexo_Util::frnDate2iso($this->datePrevNotificationActeModif->getSafeText()));
                $ficheModificative->setDateFinMarche(Atexo_Util::frnDate2iso($this->dateFinMarcheActeModif->getSafeText()));
                if (true == $this->dateFinMarcheModifOui->checked) {
                    $ficheModificative->setDateFinMarcheModifie(Atexo_Util::frnDate2iso($this->dateFinMarcheModifiee->getSafeText()));
                } else {
                    $ficheModificative->setDateFinMarcheModifie(null);
                }
                $ficheModificative->setMontantActe(Atexo_Util::getMontantArronditSansEspace($this->montantActe->getSafeText()));
                $ficheModificative->setTauxTva($this->tauxTVAModifie->getSafeText());
                $nbrFournisseurs = $this->nbrNouveauxFournisseurs->selectedValue;
                $ficheModificative->setNombreFournisseurCotraitant($nbrFournisseurs);

                $sirenFournisseur = '';
                $siretFournisseur = '';
                $nomFournisseur = '';
                $typeFournisseur = '';
                $localiteFournisseur = '';
                if ('0' != $nbrFournisseurs) {
                    $i = 0;
                    foreach ($this->repeatNouveauxFournisseurs->Items as $item) {
                        if ($i >= $nbrFournisseurs) {
                            break;
                        }
                        $etablieFr = '0';
                        if (true == $item->entrepriseEtablieFranceOui->checked) {
                            $sirenFournisseur .= $item->sirenEntreprise->getSafeText();
                            $siretFournisseur .= $item->codeEtablissement->getSafeText();
                            $etablieFr = '1';
                        }
                        $localiteFournisseur .= $etablieFr.'#';
                        $sirenFournisseur .= '#';
                        $siretFournisseur .= '#';
                        $nomFournisseur .= $item->nomEntreprise->getSafeText().'#';
                        $typeFournisseur .= $item->typeFournisseur->selectedValue.'#';
                        ++$i;
                    }
                }
                $ficheModificative->setLocalitesFournisseurs($localiteFournisseur);
                $ficheModificative->setSirenFournisseur($sirenFournisseur);
                $ficheModificative->setSiretFournisseur($siretFournisseur);
                $ficheModificative->setNomFournisseur($nomFournisseur);
                $ficheModificative->setTypeFournisseur($typeFournisseur);
                $visaAccf = '0';
                if (true == $this->visaACCFOui->checked) {
                    $visaAccf = '1';
                } elseif (true == $this->visaACCFNon->checked) {
                    $visaAccf = '2';
                }
                $ficheModificative->setVisaAccf($visaAccf);
                $visaPrefet = '0';
                if (true == $this->visaPrefetOui->checked) {
                    $visaPrefet = '1';
                } elseif (true == $this->visaPrefetNon->checked) {
                    $visaPrefet = '2';
                }
                $ficheModificative->setVisaPrefet($visaPrefet);
                $ficheModificative->setRemarque($this->remarques->getSafeText());
                $Pjs = $ficheModificative->getCommonTChorusFicheModificativePjs();
                if ($Pjs) {
                    foreach ($Pjs as $unePj) {
                        $arrayIdFiles[] = $unePj->getFichier();
                    }
                }
                $idsFile = Atexo_Util::toUtf8(implode('#', $arrayIdFiles));
                $ficheModificative->setIdBlobPieceJustificatives($idsFile);
                $date = new DateTime();
                $ficheModificative->setDateModification($date->format('Y-m-d H:i:s'));
                $ficheModificative->save($connexion);
                $this->setFicheModificativeObjet($ficheModificative);

                return $ficheModificative;
            }
        } catch (Exception $e) {
            $msgErreur = "Erreur lors de la modification de l'objet Fiche Modificative \n".$e->getMessage()."\n";
            Prado::log($msgErreur, TLogger::ERROR, 'ERROR');
            (new Atexo_Chorus_Util())->loggerErreur($msgErreur);
        }
    }

    /**
     * permet d'afficher les infos de la ficheModificative.
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function displayFicheModificative()
    {
        try {
            $ficheModificative = $this->getFicheModificativeObjet();
            if ($ficheModificative instanceof CommonTChorusFicheModificative) {
                (new Atexo_Chorus_Util())->loggerInfos(" Affichage des donnes de la Fiche Modificative \n");
                if (Atexo_Config::getParameter('ACTIVE_EXEC_V2') && $this->getContratExecArray()) {
                    $contratExec = $this->getContratExecArray();
                    $this->numInterneContrat->Text = $contratExec['referenceLibre'];
                    $this->montantMarche->Text = Atexo_Util::getMontantArronditEspace($contratExec['montant']);
                    $this->numEJ->Text = (new Atexo_Chorus_Util())->retournLastNumeroMarcheDecision($contratExec['numEj'] ?? '');
                    $this->dateFinMarcheActeModif->Text = $contratExec['dateFinContrat'];
                } elseif ($this->getContratObjet() instanceof CommonTContratTitulaire) {
                    $this->numInterneContrat->Text = $this->getContratObjet()->getReferenceLibre();
                    $this->montantMarche->Text = Atexo_Util::getMontantArronditEspace($this->getContratObjet()->getMontantContrat());
                    $this->numEJ->Text = (new Atexo_Chorus_Util())->retournLastNumeroMarcheDecision($this->getContratObjet()->getNumEJ());
                    $this->dateFinMarcheActeModif->Text = $this->getContratObjet()->getDateFinContrat();
                }
                $this->typeModification->selectedValue = $ficheModificative->getTypeModification();
                $this->datePrevNotificationActeModif->Text = Atexo_Util::iso2frnDate($ficheModificative->getDatePrevueNotification());
                if ($ficheModificative->getDateFinMarcheModifie()) {
                    $this->dateFinMarcheModifiee->Text = Atexo_Util::iso2frnDate($ficheModificative->getDateFinMarcheModifie());
                    $this->dateFinMarcheModifOui->checked = true;
                    $this->labelScript->Text .= "<script>showDiv('layer_dateFinMarcheModif_Oui');</script>";
                } else {
                    $this->dateFinMarcheModifNon->checked = true;
                    $this->labelScript->Text .= "<script>hideDiv('layer_dateFinMarcheModif_Oui');</script>";
                }
                $this->montantActe->Text = Atexo_Util::getMontantArronditEspace($ficheModificative->getMontantActe());
                $this->tauxTVAModifie->Text = $ficheModificative->getTauxTva();
                $nbrFournisseurs = $ficheModificative->getNombreFournisseurCotraitant();
                $this->nbrNouveauxFournisseurs->selectedValue = $nbrFournisseurs;
                $this->labelScript->Text .= '<script>afficherBlocNouveauxFournisseurs();</script>';

                $localiteFournisseur = explode('#', $ficheModificative->getLocalitesFournisseurs());
                $sirenFournisseur = explode('#', $ficheModificative->getSirenFournisseur());
                $siretFournisseur = explode('#', $ficheModificative->getSiretFournisseur());
                $nomFournisseur = explode('#', $ficheModificative->getNomFournisseur());
                $typeFournisseur = explode('#', $ficheModificative->getTypeFournisseur());
                if ('0' != $nbrFournisseurs) {
                    $i = 0;
                    foreach ($this->repeatNouveauxFournisseurs->Items as $item) {
                        if ($i >= $nbrFournisseurs) {
                            break;
                        }
                        $item->sirenEntreprise->Text = $sirenFournisseur[$i];
                        $item->codeEtablissement->Text = $siretFournisseur[$i];
                        $item->nomEntreprise->Text = $nomFournisseur[$i];
                        $item->typeFournisseur->selectedValue = $typeFournisseur[$i];
                        if ('1' == $localiteFournisseur[$i]) {
                            $item->entrepriseEtablieFranceOui->checked = true;
                        } else {
                            $item->entrepriseEtablieFranceNon->checked = true;
                            $j = $i + 1;
                            $this->labelScript->Text .= "<script>hideDiv('".$item->divSiren->ClientId."');</script>";
                        }
                        ++$i;
                    }
                }

                $visaAccf = $ficheModificative->getVisaAccf();
                switch ($visaAccf) {
                    case '0':
                        $this->visaACCFNonRenseigne->checked = true;
                        break;
                    case '1':
                        $this->visaACCFOui->checked = true;
                        break;
                    case '2':
                        $this->visaACCFNon->checked = true;
                        break;
                }
                $visaPrefet = $ficheModificative->getVisaPrefet();
                switch ($visaPrefet) {
                    case '0':
                        $this->visaPrefetNonRenseigne->checked = true;
                        break;
                    case '1':
                        $this->visaPrefetOui->checked = true;
                        break;
                    case '2':
                        $this->visaPrefetNon->checked = true;
                        break;
                }
                $this->remarques->Text = $ficheModificative->getRemarque();
            }
        } catch (Exception $e) {
            $msgErreur = "Erreur lors de la visualisation de Fiche Modificative \n".$e->getMessage()."\n";
            Prado::log($msgErreur, TLogger::ERROR, 'ERROR');
            (new Atexo_Chorus_Util())->loggerErreur($msgErreur);
        }
    }

    /**
     * permet de retourner l'id de l'objet CommonTChorusFicheModificative.
     *
     * @return int $idFicheModificative
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function getIdFicheModificative()
    {
        $ficheModificative = $this->getFicheModificativeObjet();
        if ($ficheModificative instanceof CommonTChorusFicheModificative) {
            return $ficheModificative->getIdFicheModificative();
        }

        return null;
    }

    /**
     * permet d'afficher la liste des pieces jointes de la ficheModificative.
     *
     * @author    Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version   1.0
     *
     * @since     2015-place
     *
     * @copyright Atexo 2015
     */
    public function displayPjFicheModificative()
    {
        $PjFicheModificative = [];
        $ficheModificative = $this->getFicheModificativeObjet();
        if ($ficheModificative instanceof CommonTChorusFicheModificative) {
            $PjFicheModificative = $ficheModificative->getCommonTChorusFicheModificativePjs();
        }
        $this->repeaterPjFicheModificative->DataSource = $PjFicheModificative;
        $this->repeaterPjFicheModificative->dataBind();
    }

    /**
     * permet d'actualiser la liste des pieces jointes de la ficheModificative.
     *
     * @param mixed $sender
     * @param mixed $param
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function refreshPjFicheModificative($sender, $param)
    {
        $this->displayPjFicheModificative();
        $this->repeaterPjFicheModificative->render($param->getNewWriter());
        $this->labelScript2->Text = '<script>showLoader();</script>';
    }

    /**
     * permet de supprimer une pj de la liste des pieces jointes de la ficheModificative.
     *
     * @param mixed $sender
     * @param mixed $param
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function deletePjModificative($sender, $param)
    {
        if ($param->CommandParameter) {
            CommonTChorusFicheModificativePjPeer::doDelete($param->CommandParameter);
        }
        $this->labelScript2->text = "<script>
                                        J('.modal-confirmation-suppression').dialog('close');
                                    </script>";
    }

    /**
     * Permet de gerer la visibilite de la fiche modificative.
     *
     * @return bool
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     * @copyright Atexo 2015
     */
    public function isTypeEnvoiFicheModificative()
    {
        try {
            return (new Atexo_Chorus_Util())->isTypeEnvoiFicheModificative($this->getContratObjet(), $this->getChorusEchangeObjet());
        } catch (Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('chorus');
            $logger->error('Erreur : '.$e->getMessage()." \n\nTrace : ".$e->getTraceAsString()." \n\nMethode = ChorusDetailEchanges::isTypeEnvoiFicheModificative");
        }
    }

    /**
     * permet de generer le document de la fiche Modificative.
     *
     * @param CommonTChorusFicheModificative $ficheModificative
     * @param CommonTContratTitulaire        $contrat
     * @param PropelPDO                      $connexion
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function generatePdfFicheModificative($ficheModificative, $contrat, $connexion)
    {
        $idBlobFicheModificative = null;

        if (!empty($contrat)) {
            (new Atexo_Chorus_Util())->loggerInfos(" debut generation du pdf de la Fiche Modificative \n");
            $htmlContent = self::generateHtmlFicheModificative($ficheModificative, $contrat);
            $nomFichier = (new Atexo_Chorus_Util())->getNomPdfActeModificatif(false, $contrat, true);

            //Generation pdf acte modificatif. Parametre "CHORUS_NUM_OF_ATTEMPTS_GEN_ACTE_MODIFICATIF" : nbr de tentatives de generation de l'acte modificatif
            $attempts = 0;
            do {
                try {
                    $idBlobFicheModificative = self::genererPdfFromHtml($htmlContent, $nomFichier, $contrat->getIdContratTitulaire());
                    $fileActeModifOnBlob = Atexo_Config::getParameter('BASE_ROOT_DIR').$contrat->getOrganisme().Atexo_Config::getParameter('FILES_DIR').$idBlobFicheModificative.'-0';
                    if (empty($idBlobFicheModificative) || !is_file($fileActeModifOnBlob) || false === file_get_contents($fileActeModifOnBlob) || empty(file_get_contents($fileActeModifOnBlob))) {
                        ++$attempts;
                        //sleep(1);
                        continue;
                    }
                } catch (Exception $e) {
                    ++$attempts;
                    //sleep(1);
                    continue;
                }
                break;
            } while ($attempts < Atexo_Config::getParameter('CHORUS_NUM_OF_ATTEMPTS_GEN_ACTE_MODIFICATIF'));

            if (!empty($idBlobFicheModificative)) {
                (new Atexo_Chorus_Util())->loggerInfos(" MAJ Fiche Modificative, ajout IdBlob ( = $idBlobFicheModificative) du fichier pdf genere \n");
                try {
                    $ficheModificative->setIdBlobFicheModificative($idBlobFicheModificative);
                    $ficheModificative->save($connexion);
                } catch (Exception $e) {
                    $msgErreur = "Erreur lors du MAJ de la Fiche Modificative. Erreur \n".$e->getMessage()."\n".'Fiche Modificative Objet : '.print_r($ficheModificative, true);
                    Prado::log($msgErreur, TLogger::ERROR, 'ERROR');
                    (new Atexo_Chorus_Util())->loggerErreur($msgErreur);
                }
            } else {
                (new Atexo_Chorus_Util())->loggerInfos("La fiche modificative n'a pas ete mise a jour: car la generation a echoue. Pour plus de details voir le fichier 'BASE_ROOT_DIR/logs/interfacePlaceChorus/aaaa/mm/jj/interfacePlaceChorus.log'");
            }
        } else {
            (new Atexo_Chorus_Util())->loggerInfos("Impossible de generer la fiche modificative. Le contrat n'est pas une instance de CommonTContratTitulaire : ".print_r($contrat, true));
        }
    }

    /**
     * permet de generer le contenu HTML de la ficheModificative.
     *
     * @param CommonTChorusFicheModificative $ficheModificative
     * @param CommonTContratTitulaire        $contrat
     *
     * @return string $content
     *
     * @author    Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version   1.0
     *
     * @since     2015-place
     *
     * @copyright Atexo 2015
     */
    public function generateHtmlFicheModificative($ficheModificative, $contrat)
    {
        $visaAccfText = null;
        $visaPrefetText = null;
        $arrayFiles = [];
        try {
            if ($ficheModificative instanceof CommonTChorusFicheModificative) {
                (new Atexo_Chorus_Util())->loggerInfos(" creation du contenu html pour la Fiche Modificative \n");
                $modelhtmlFicheMofificative = Atexo_Config::getParameter('PATH_MODELE_CHORUS_FICH_MODIFICATIF');
                $content = file_get_contents($modelhtmlFicheMofificative);
                if ($contrat instanceof CommonTContratTitulaire) {
                    $content = str_replace('NUMERO_CONTRAT', $contrat->getReferenceLibre(), $content);
                    $content = str_replace('NUMERO_EJ', (new Atexo_Chorus_Util())->retournLastNumeroMarcheDecision($contrat->getNumEJ()), $content);
                    $content = str_replace('MONTANT_MARCHE', $contrat->getMontantContrat(), $content);
                } elseif (!empty($contrat) && is_array($contrat) && Atexo_Config::getParameter('ACTIVE_EXEC_V2')) {
                    $content = str_replace('NUMERO_CONTRAT', $contrat['referenceLibre'], $content);
                    $content = str_replace('NUMERO_EJ', (new Atexo_Chorus_Util())->retournLastNumeroMarcheDecision($contrat['numEj']), $content);
                    $content = str_replace('MONTANT_MARCHE', $contrat['montant'], $content);
                }
                $idTypeModification = $ficheModificative->getTypeModification();
                $refTypeFournisseur = Atexo_Config::getParameter('REFERENTIEL_TYPE_MODIFICATION');
                $referentielTypeModification = CommonValeurReferentielQuery::create()->filterById($idTypeModification)->filterByIdReferentiel($refTypeFournisseur)->findOne();
                if ($referentielTypeModification instanceof CommonValeurReferentiel) {
                    $libelleTypeModification = Atexo_Util::toUtf8($referentielTypeModification->getLibelleValeurReferentielFr());
                    $content = str_replace('TYPE_MODIFICATION', $libelleTypeModification, $content);
                }
                $content = str_replace('DATE_PREVISIONNELLE_NOTIFICATION', $ficheModificative->getDatePrevueNotification(), $content);
                $content = str_replace('DATE_FIN_MARCHE', $ficheModificative->getDateFinMarche(), $content);
                if ($ficheModificative->getDateFinMarcheModifie()) {
                    $content = str_replace('BOOL_DATE_FIN_MODIFIE', Prado::localize('DEFINE_OUI'), $content);
                    $content = str_replace('DATE_FIN_MODIFIE', $ficheModificative->getDateFinMarcheModifie(), $content);
                } else {
                    $content = str_replace('BOOL_DATE_FIN_MODIFIE', Prado::localize('TEXT_NON'), $content);
                    $content = str_replace('DATE_FIN_MODIFIE', '-', $content);
                }
                $content = str_replace('MONTANT_ACTE', $ficheModificative->getMontantActe(), $content);
                if ($ficheModificative->getTauxTva()) {
                    $content = str_replace('TVA_MODIFIE', $ficheModificative->getTauxTva(), $content);
                } else {
                    $content = str_replace('TVA_MODIFIE', '-', $content);
                }
                $nbrFournisseurs = $ficheModificative->getNombreFournisseurCotraitant();
                $content = str_replace('NOMBRE_FOURNISSEURS', $nbrFournisseurs, $content);
                $listeFournisseurs = '';
                if ($nbrFournisseurs) {
                    $listeFournisseurs = self::getCodeListeFournisseurs($ficheModificative);
                }
                $content = str_replace('CODE_LISTE_NOUVEAUX_FOURNISSEURS', $listeFournisseurs, $content);
                $visaAccfValue = $ficheModificative->getVisaAccf();
                switch ($visaAccfValue) {
                    case '0':
                        $visaAccfText = Atexo_Util::toUtf8(Prado::localize('NON_RENSEIGNE'));
                        break;
                    case '1':
                        $visaAccfText = Prado::localize('DEFINE_OUI');
                        break;
                    case '2':
                        $visaAccfText = Prado::localize('TEXT_NON');
                        break;
                }
                $content = str_replace('VISA_AACF', $visaAccfText, $content);

                $visaPrefetValue = $ficheModificative->getVisaPrefet();
                switch ($visaPrefetValue) {
                    case '0':
                        $visaPrefetText = Atexo_Util::toUtf8(Prado::localize('NON_RENSEIGNE'));
                        break;
                    case '1':
                        $visaPrefetText = Prado::localize('DEFINE_OUI');
                        break;
                    case '2':
                        $visaPrefetText = Prado::localize('TEXT_NON');
                        break;
                }
                $content = str_replace('VISA_PREFET', $visaPrefetText, $content);
                $content = str_replace('VALUE_REMARQUE', Atexo_Util::toUtf8($ficheModificative->getRemarque()), $content);
                $Pjs = $ficheModificative->getCommonTChorusFicheModificativePjs();
                if ($Pjs) {
                    foreach ($Pjs as $unePj) {
                        $arrayFiles[] = $unePj->getNomFichier();
                    }
                }
                $listeNameFile = '- '.Atexo_Util::toUtf8(implode(',<br/>- ', $arrayFiles));
                $content = str_replace('LISTE_PJ', $listeNameFile, $content);

                return $content;
            }
        } catch (Exception $e) {
            $msgErreur = "Erreur lors de la generation de l'html Fiche Modificative \n".$e->getMessage()."\n";
            Prado::log($msgErreur, TLogger::ERROR, 'ERROR');
            (new Atexo_Chorus_Util())->loggerErreur($msgErreur);
        }
    }

    /**
     * permet de generer le contenu HTML des nouveaux fournisseurs de l'acte modificatif.
     *
     * @param CommonTChorusFicheModificative $ficheModificative
     *
     * @return string $listeFournisseurs
     *
     * @author    Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version   1.0
     *
     * @since     2015-place
     *
     * @copyright Atexo 2015
     */
    public function getCodeListeFournisseurs($ficheModificative)
    {
        try {
            $nbrFournisseurs = $ficheModificative->getNombreFournisseurCotraitant();
            $listeFournisseurs = '';
            $localiteFournisseur = explode('#', $ficheModificative->getLocalitesFournisseurs());
            $sirenFournisseur = explode('#', $ficheModificative->getSirenFournisseur());
            $siretFournisseur = explode('#', $ficheModificative->getSiretFournisseur());
            $nomFournisseur = explode('#', $ficheModificative->getNomFournisseur());
            $typeFournisseur = explode('#', $ficheModificative->getTypeFournisseur());
            $refTypeFournisseur = Atexo_Config::getParameter('REFERENTIEL_TYPE_FOURNISSEUR');
            for ($c = 0; $c < $nbrFournisseurs; ++$c) {
                $numEse = $c + 1;
                $entrepriseFranceiase = $localiteFournisseur[$c] ? Prado::localize('DEFINE_OUI') : Prado::localize('DEFINE_NON');
                $referentielTypeFournisseur = CommonValeurReferentielQuery::create()->filterById($typeFournisseur[$c])->filterByIdReferentiel($refTypeFournisseur)->findOne();
                if ($referentielTypeFournisseur instanceof CommonValeurReferentiel) {
                    $libelleTypeFournisseur = $referentielTypeFournisseur->getLibelleValeurReferentielFr();
                }
                $listeFournisseurs .= '
										<tr bgcolor="'.$this->getBgcolor().'">
										<td valign="top" width="40%">
											<strong>'
                    .Prado::localize('TEXT_L_ENTREPRISE').' '.$numEse.' '.Prado::localize('TEXT_EST_ELLE_ETABLIE_FRANCE').
                    '</strong>
										</td>
										  <td valign="top">'.$entrepriseFranceiase.'</td>
										</tr>
										<tr bgcolor="'.$this->getBgcolor().'">
											<td valign="top" width="40%">
                                                <strong>'
                    .Prado::localize('NOM_ENTREPRISE').'<br />'.Prado::localize('TEXT_ENTREPRISE').' '.$numEse.
                    '</strong>
											</td>
											<td valign="top">'.$nomFournisseur[$c].'</td>
										</tr>';
                if ($localiteFournisseur[$c]) {
                    $listeFournisseurs .= '<tr bgcolor="'.$this->getBgcolor().'">
											<td valign="top" width="40%">
												<strong>'
                        .Prado::localize('TEXT_SIRET_L_ENTREPRISE').'<br />'.Prado::localize('TEXT_ENTREPRISE').' '.$numEse.
                        '</strong>
											</td>
										  	<td valign="top">'.$sirenFournisseur[$c].' '.$siretFournisseur[$c].'</td>
										</tr>';
                }
                $listeFournisseurs .= '<tr bgcolor="'.$this->getBgcolor().'">
											<td valign="top" width="40%">
												<strong>'
                    .Prado::localize('DEFINE_TYPE_FOURNISSEUR').'<br />'.Prado::localize('TEXT_ENTREPRISE').' '.$numEse.
                    '</strong>
											</td>
											<td valign="top">'.$libelleTypeFournisseur.'</td>
										</tr>';
            }

            return Atexo_Util::toUtf8($listeFournisseurs);
        } catch (Exception $e) {
            $msgErreur = "Erreur lors de la generation de la liste de fournisseur pour l'acte modificatif  Fiche Modificative \n".$e->getMessage()."\n";
            Prado::log($msgErreur, TLogger::ERROR, 'ERROR');
            (new Atexo_Chorus_Util())->loggerErreur($msgErreur);
        }
    }

    /**
     * permet de generer le PDF de la fiche modificative à partir du contenu HTML.
     *
     * @param CommonTChorusFicheModificative $htmlContent
     * @param string                         $nomFichier
     * @param int                            $idContrat   : identifiant technique du contrat
     *
     * @return int $idBlobFicheModificative
     *
     * @author    Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version   1.0
     *
     * @since     2015-place
     *
     * @copyright Atexo 2015
     */
    public function genererPdfFromHtml($htmlContent, $nomFichier, $idContrat)
    {
        try {
            (new Atexo_Chorus_Util())->loggerInfos(" generation du pdf de la Fiche Modificative a partir du contenu HTML \n");
            $idGen = $idContrat.session_id();
            $nomFichierPdf = $nomFichier.'.pdf';
            $tmpHtmlFile = Atexo_Config::getParameter('COMMON_TMP').DIRECTORY_SEPARATOR.$idGen.'.html';
            $tmpPdfFile = Atexo_Config::getParameter('COMMON_TMP').DIRECTORY_SEPARATOR.$idGen.'.pdf';
            Atexo_Util::write_file($tmpHtmlFile, $htmlContent);
            $cmd = Atexo_Config::getParameter('PATH_BIN_WKHTMLPDF').' '.$tmpHtmlFile.' '.$tmpPdfFile.'  > /tmp/logPrintFile 2>&1 ';
            system($cmd);

            if (is_file($tmpPdfFile)) {
                $atexoBlob = new Atexo_Blob();
                $idBlobFicheModificative = $atexoBlob->insert_blob($nomFichierPdf, $tmpPdfFile, Atexo_CurrentUser::getOrganismAcronym());
            } else {
                $idBlobFicheModificative = null;
                $contenuLogGenerationPdf = file_get_contents('/tmp/logPrintFile');
                (new Atexo_Chorus_Util())->loggerErreur('Erreur: la generation de la fiche modificative a echoue.'.PHP_EOL."Details erreur : $contenuLogGenerationPdf ".PHP_EOL."Commande executee = '$cmd'");
            }

            return $idBlobFicheModificative;
        } catch (Exception $e) {
            $msgErreur = "Erreur lors de la generation du pdf de la Fiche Modificative \n".$e->getMessage()."\n";
            Prado::log($msgErreur, TLogger::ERROR, 'ERROR');
            (new Atexo_Chorus_Util())->loggerErreur($msgErreur);
        }
    }
}
