<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_CommonReferentiel;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Module;
use Prado\Prado;

class PanelCreationInscrit extends MpeTPage
{
    public function onLoad($param)
    {
        $this->customizeForm();
        if (Atexo_CurrentUser::isAgent() || Atexo_CurrentUser::isAgentSocle()) {
            $this->getApplication()->getModule('auth')->logout();
            $this->response->redirect(Atexo_Config::getParameter('PF_URL').'?page=Entreprise.EntrepriseHome');
        }
    }

    /*
     * Redirige vers la page InscriptionUtilisateur
     * @param siren
     * @param siret
     */
    public function siretOkButton()
    {
        $siren = $this->siren->Text;
        $siret = $this->siret->Text;
        $url = '?page=Entreprise.EntrepriseInscriptionUsers&action=creation&siren='.$siren.'&goto='.urlencode($_GET['goto']);
        if ('' != $siren && '' == $siret ) {
            $this->response->redirect($url);
        } elseif ('' != $siren && '' != $siret) {
            $this->response->redirect($url.'&siret=' . $siret);
        }
    }

    public function idNationalOkPressed()
    {
        $idNational = $this->idNational->Text;
        if ((0 != $this->listPays->SelectedIndex) && '' != $idNational) {
            $this->response->redirect('?page=Entreprise.EntrepriseInscriptionUsers&action=creation&idNational='
            .$idNational.'&pays='.substr($this->listPays->getSelectedItem()->getText(), 0, -6));
        }
    }

    /**
     * @return mixed
     */
    public function isSiretAlertPanelCreationInscrit()
    {
        return (new Atexo_CurrentUser())->isSiretAlert();
    }

    public function VerifyAuthLogin()
    {
        // Coquille vide car authentification captée par SF
                }

    public function VerifyAuthCertif($sender, $param)
    {
        // Coquille vide car authentification captée par SF
    }

    public function displayMessage($text)
    {
        $this->panelMessageErreur->setVisible(true);
        $this->panelMessageErreur->setMessage($text);
    }

    public function displayVilleRc()
    {
        $valeur = (new Atexo_CommonReferentiel())->retrieveValeurReferentiel(Atexo_Config::getParameter('VALEUR_REFERENTIEL_VILLE_RC'));
        $data = [];
        $data['0'] = Prado::localize('TEXT_SELECTIONNER') . ' ...';
        $data['99'] = Prado::localize('JE_PAS_DE_RC');
        if ($valeur) {
            foreach ($valeur as $oneVal) {
                $data[$oneVal->getLibelle2()] = $oneVal->getLibelleValeurReferentiel();
            }
        }
        $this->RcVille->DataSource = $data;
        $this->RcVille->DataBind();
    }

    public function RcOk()
    {
        $villeRc = $this->RcVille->getSelectedValue();
        $numeroRc = $this->RcNumero->Text;
        if ('' != $villeRc && '' != $numeroRc) {
            $rc = $villeRc.Atexo_Config::getParameter('SEPARATEUR_VILLE_RC').$numeroRc;
            $url = '?page=Entreprise.EntrepriseInscriptionUsers&action=creation&siren='.$rc;
            $this->response->redirect($url);
        }
    }

    public function customizeForm()
    {
        if (Atexo_Module::isEnabled('CompteEntrepriseRcville')) {
            $this->panelSiren->setVisible(false);
            $this->panelRc->setVisible(true);
            $this->SirenValidator->SetEnabled(false);
            $this->panelIdentifiantUnique->setVisible(false);
            $this->displayVilleRc();
        } elseif (Atexo_Module::isEnabled('CompteEntrepriseIdentifiantUnique')) {
            $this->panelIdentifiantUnique->setVisible(true);
            $this->panelSiren->setVisible(false);
            $this->panelRc->setVisible(false);
            $this->SirenValidator->SetEnabled(false);
        } else {
            $this->panelSiren->setVisible(true);
            $this->panelRc->setVisible(false);
            $this->SirenValidator->SetEnabled(true);
            $this->panelIdentifiantUnique->setVisible(false);
        }
    }

    /*
     * Redirige vers la page InscriptionUtilisateur
     * @param siret
     */
    public function identifiantUniqueOkPressed()
    {
        $identifiantUnique = $this->identifiantUnique->Text;
        $url = '?page=Entreprise.EntrepriseInscriptionUsers&action=creation&siren='.$identifiantUnique;
        if ('' != $identifiantUnique) {
            $this->response->redirect($url);
        }
    }

    public function refreshListPays($sender, $param)
    {
        $this->listPays->fillDataSource();
        //$this->listPays->render($param->getNewWriter());
    }
}
