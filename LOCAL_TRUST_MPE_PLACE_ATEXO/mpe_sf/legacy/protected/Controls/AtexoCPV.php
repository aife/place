<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_CodeCpv;
use Application\Service\Atexo\Atexo_CurrentUser;

/**
 * Template du bloc siège.
 */
class AtexoCPV extends MpeTPage
{
    private $idElement;
    private array $principal = [];
    private array $secondaire = [];
    private string $cas = 'cas2';

    public function setIdElement($idElement)
    {
        $this->idElement = $idElement;
    }

    public function getIdElement()
    {
        return $this->idElement;
    }

    public function setPrincipal($principal)
    {
        $this->principal = $principal;
    }

    public function getPrincipal()
    {
        return $this->principal;
    }

    public function setSecondaire($secondaire)
    {
        $this->secondaire = $secondaire;
    }

    public function getSecondaire()
    {
        return $this->secondaire;
    }

    public function setCas($value)
    {
        $this->cas = $value;
    }

    public function getCas()
    {
        return $this->cas;
    }

    public function onLoad($param)
    {
        $sessionLang = Atexo_CurrentUser::readFromSession('lang');

        if (isset($sessionLang)) {
            $lang = $sessionLang;
        } else {
            $lang = 'fr';
        }
        $atexoCpv = new Atexo_CodeCpv();
        $this->scriptCPV->Text = $atexoCpv->afficherBlocCpv($this->getCas(), $this->getPrincipal(), $this->getSecondaire(), 0, 4, $lang, '', '', 'ctl0_CONTENU_PAGE'.$this->getIdElement().'_idAtexoCPV_code_cpv', '');
    }

    public function afficherCPV()
    {
        $sessionLang = Atexo_CurrentUser::readFromSession('lang');

        if (isset($sessionLang)) {
            $lang = $sessionLang;
        } else {
            $lang = 'fr';
        }
        $atexoCpv = new Atexo_CodeCpv();
        $this->scriptCPV->Text = $atexoCpv->afficherBlocCpv($this->getCas(), $this->getPrincipal(), $this->getSecondaire(), 0, 4, $lang, '', '', 'ctl0_CONTENU_PAGE'.$this->getIdElement().'_idAtexoCPV_code_cpv', '');
    }
}
