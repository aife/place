<com:TLabel visible="<%=(Application\Service\Atexo\Atexo_Consultation::hasClauseSociale($this->getObjet()))? true:false%>">
    <i class="s s-social s-18"
       data-toggle="tooltip" data-placement="top"
       title="<%=Prado::localize('DEFINE_CLAUSES_SOCIALES_INSERTION')%>">
    </i>
</com:TLabel>
<com:TLabel visible="<%=(Application\Service\Atexo\Atexo_Consultation::hasClauseEnvironnementales($this->getObjet()))? true:false%>">
    <i class="s s-environnement s-18"
       data-toggle="tooltip" data-placement="top"
       title="<%=Prado::localize('CLAUSES_ENVIRONNEMENTALES')%>">
    </i>
</com:TLabel>
<com:TActivePanel id="detail_dispSociales_cons" cssclass="hidden"
                  visible="<%=(Application\Service\Atexo\Atexo_Consultation::hasClauseSocialeMarcheReserve($this->getObjet()))? true:false%>">
    <div class="popover-heading">
        <com:TTranslate>DEFINE_CLAUSES_SOCIALES_INSERTION</com:TTranslate>
    </div>
    <div class="popover-body">
        <p>
            <com:TTranslate>CE_MARCHE_EST_RESERVE_A_DES</com:TTranslate>
            <br/>
            <span class="popinInjectDiv"></span>
            <%=$this->getClauseMarcheReserve($this->getObjet())%>
        </p>
    </div>
</com:TActivePanel>
