<com:TActivePanel id="bloc_PubliciteConsultationConcentrateurV2" cssclass="form-field" >
    <div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
    <div class="content">
        <h2>
            <com:TTranslate>PUBLICITE_CONSULTATION</com:TTranslate>
        </h2>
        <!--Debut bloc rappel pub-->
        <div class="panel panel-default with-bg top-panel masquer">
            <div class="panel-body">
                <h3>
                    <com:TTranslate>NOUS_VOUS_RAPPELONS_QUE</com:TTranslate>
                </h3>
                <com:TTranslate>RAPPEL_LES_ECHOS</com:TTranslate>
                <div class="spacer-small"></div>
            </div>
        </div>
        <div class="spacer-small"></div>
        <com:TActivePanel id="rappelOngletPublicite" CssClass="panel panel-default with-bg top-panel masquer">
            <div class="panel-body">
                <com:TTranslate>RAPPEL_COMPLETER_AVIS_BOAMP_DANS_MOL</com:TTranslate>
                <div class="spacer-small"></div>
            </div>
        </com:TActivePanel>
        <com:TActivePanel id="panelBlocErreur" Display="None">
            <com:PanelMessageErreur ID="messageErreur" Message="<%=Prado::localize('CONCENTRATEUR_SERVICE_INDISPONIBLE')%>"/>
        </com:TActivePanel>
        <br>
        <com:TActivePanel id="panelConcentrateur" Display="None" cssclass="panel panel-default top-panel" style="border: 0">
            <link rel="stylesheet" href="<%=$this->getUrlConcentrateur()%>/concentrateur-annonces/consultation-formulaire/consultationFormulaire.css">
            <br>
            <div id="consultation-formulaire">
                <%=$this->getHtml()%>
            </div>

        </com:TActivePanel>

    </div>
</com:TActivePanel>
