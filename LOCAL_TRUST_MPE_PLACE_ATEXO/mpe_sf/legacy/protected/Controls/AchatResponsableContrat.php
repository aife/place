<?php

namespace Application\Controls;

use Application\Propel\Mpe\CommonCategorieLot;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonTContratMulti;
use Application\Propel\Mpe\CommonTContratTitulaire;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Exception;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CategoriesConsiderationsSociales;
use Application\Service\Atexo\MessageStatusCheckerUtil;
use Prado\Prado;

/**
 * Page de gestion du bloc "Achat Responsable" de la consultation.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 4
 */
class AchatResponsableContrat extends MpeTTemplateControl
{
    public ?string $_clauseSocialCondExecution = null;
    public ?string $_clauseSocialInsertion = null;
    public ?string $_clauseSocialAteliersProteges = null;
    public ?string $_clauseEnvSpecsTechniques = null;
    public ?string $_clauseEnvCondExecution = null;
    public ?string $_clauseEnvCriteresInsertion = null;

    public $_objet = null;
    private $mode;
    private $validationGroup;
    private $validationSummary;
    // $sourcePage = ('etapeIdentification' si on est sur l'étape "identification")
    // $sourcePage = ('detailLots' si on est sur l'étape "detailLots")
    private ?string $sourcePage = null;
    public ?string $idElement = null;
    private $validationGroupBtnValidate;
    private $validationSummaryBtnValidate;
    private bool $cssPersonalisee = false;

    private $listSpecificationTechniqueSelected;
    private $listCritereInsertionSelected;
    private $listConditionExecutionSelected;

    public $_clauseSpecificationTechnique;
    public $_clauseMarcheInsertion;

    /**
     * @param $clauseSpecificationTechnique
     */
    public function setClauseSpecificationTechnique($clauseSpecificationTechnique)
    {
        $this->_clauseSpecificationTechnique = $clauseSpecificationTechnique;
    }

    /**
     * @return mixed $_clauseSpecificationTechnique
     */
    public function getClauseSpecificationTechnique()
    {
        return $this->_clauseSpecificationTechnique;
    }

    /**
     * @param $clauseMarcheInsertion
     */
    public function setClauseMarcheInsertion($clauseMarcheInsertion)
    {
        $this->_clauseMarcheInsertion = $clauseMarcheInsertion;
    }

    /**
     * @return mixed $_clauseMarcheInsertion
     */
    public function getClauseMarcheInsertion()
    {
        return $this->_clauseMarcheInsertion;
    }

    private function setlistSpecificationTechniqueSelected($liste)
    {
        $this->listSpecificationTechniqueSelected = $liste;
    }

    private function getlistSpecificationTechniqueSelected()
    {
        return $this->listSpecificationTechniqueSelected;
    }

    /**
     * @param $liste
     */
    private function setlistCritereInsertionSelected($liste)
    {
        $this->listCritereInsertionSelected = $liste;
    }

    private function getlistCritereInsertionSelected()
    {
        return $this->listCritereInsertionSelected;
    }

    /**
     * @param $liste
     */
    private function setlistConditionExecutionSelected($liste)
    {
        $this->listConditionExecutionSelected = $liste;
    }

    private function getlistConditionExecutionSelected()
    {
        return $this->listConditionExecutionSelected;
    }

    /**
     * recupère la valeur.
     */
    public function getIdElement()
    {
        return $this->idElement;
    }

    // getClauseSocialCondExecution()

    /**
     * Affecte la valeur.
     *
     * @param string $value
     */
    public function setIdElement($value)
    {
        $this->idElement = $value;
    }

    // setClauseSocialCondExecution()

    /**
     * recupère la valeur.
     */
    public function getClauseSocialCondExecution()
    {
        return $this->_clauseSocialCondExecution;
    }

    // getClauseSocialCondExecution()

    /**
     * Affecte la valeur.
     *
     * @param string $value
     */
    public function setClauseSocialCondExecution($value)
    {
        if ($this->_clauseSocialCondExecution !== $value) {
            $this->_clauseSocialCondExecution = $value;
        }
    }

    // setClauseSocialCondExecution()

    /**
     * recupère la valeur.
     */
    public function getClauseSocialInsertion()
    {
        return $this->_clauseSocialInsertion;
    }

    // getClauseSocialInsertion()

    /**
     * Affecte la valeur.
     *
     * @param string $value
     */
    public function setClauseSocialInsertion($value)
    {
        if ($this->_clauseSocialInsertion !== $value) {
            $this->_clauseSocialInsertion = $value;
        }
    }

    // setClauseSocialInsertion()

    /**
     * recupère la valeur.
     */
    public function getClauseSocialAteliersProteges()
    {
        return $this->_clauseSocialAteliersProteges;
    }

    // getClauseSocialAteliersProteges()

    /**
     * Affecte la valeur.
     *
     * @param string $value
     */
    public function setClauseSocialAteliersProteges($value)
    {
        if ($this->_clauseSocialAteliersProteges !== $value) {
            $this->_clauseSocialAteliersProteges = $value;
        }
    }

    // setClauseSocialAteliersProteges()

    /**
     * recupère la valeur.
     */
    public function getClauseEnvSpecsTechniques()
    {
        return $this->_clauseEnvSpecsTechniques;
    }

    // getClauseEnvSpecsTechniques()

    /**
     * Affecte la valeur.
     *
     * @param string $value
     */
    public function setClauseEnvSpecsTechniques($value)
    {
        if ($this->_clauseEnvSpecsTechniques !== $value) {
            $this->_clauseEnvSpecsTechniques = $value;
        }
    }

    // setClauseEnvSpecsTechniques()

    /**
     * recupère la valeur.
     */
    public function getClauseEnvCondExecution()
    {
        return $this->_clauseEnvCondExecution;
    }

    // getClauseEnvCondExecution()

    /**
     * Affecte la valeur.
     *
     * @param string $value
     */
    public function setClauseEnvCondExecution($value)
    {
        if ($this->_clauseEnvCondExecution !== $value) {
            $this->_clauseEnvCondExecution = $value;
        }
    }

    // setClauseEnvCondExecution()

    /**
     * recupère la valeur.
     */
    public function getClauseEnvCriteresInsertion()
    {
        return $this->_clauseEnvCriteresInsertion;
    }

    // getClauseEnvCriteresInsertion()

    /**
     * Affecte la valeur.
     *
     * @param string $value
     */
    public function setClauseEnvCriteresInsertion($value)
    {
        if ($this->_clauseEnvCriteresInsertion !== $value) {
            $this->_clauseEnvCriteresInsertion = $value;
        }
    }

    // setClauseEnvCriteresInsertion()

    /**
     * recupère la valeur.
     */
    public function getObjet()
    {
        return $this->_objet;
    }

    // getObjet()

    /**
     * Affecte la valeur.
     *
     * @param string $value
     */
    public function setObjet($value)
    {
        if ($this->_objet !== $value) {
            $this->_objet = $value;
        }
    }

    // setConsultation()

    /**
     * Affecte la valeur.
     *
     * @param string $value
     */
    public function setMode($mode)
    {
        $this->mode = $mode;
    }

    /**
     * recupère la valeur.
     */
    public function getMode()
    {
        return $this->mode;
    }

    public function setValidationGroup($validationGroup)
    {
        $this->validationGroup = $validationGroup;
    }

    public function getValidationGroup()
    {
        return $this->validationGroup;
    }

    public function setValidationSummary($validationSummary)
    {
        $this->validationSummary = $validationSummary;
    }

    public function getValidationSummary()
    {
        return $this->validationSummary;
    }

    public function setCssPersonalisee($v)
    {
        $this->cssPersonalisee = $v;
    }

    public function getCssPersonalisee()
    {
        return $this->cssPersonalisee;
    }

    public function onLoad($param)
    {
        if ('achatResponsableConsultation' !== $this->getId()) {
            throw new Atexo_Exception("L'id du composant 'AchatResponsableConsultation' doit être : 'achatResponsableConsultation'  ");
        }

        MessageStatusCheckerUtil::initEntryPoints('clauses');
    }

    /**
     * recupère la valeur.
     */
    public function getSourcePage()
    {
        return $this->sourcePage;
    }

    // getSourcePage()

    /**
     * Affecte la valeur.
     *
     * @param string $value
     */
    public function setSourcePage($value)
    {
        if ($this->sourcePage !== $value) {
            $this->sourcePage = $value;
        }
    }

    // setSourcePage()

    public function setValidationGroupBtnValidate($validationGroup)
    {
        $this->validationGroupBtnValidate = $validationGroup;
    }

    public function getValidationGroupBtnValidate()
    {
        return $this->validationGroupBtnValidate;
    }

    public function setValidationSummaryBtnValidate($value)
    {
        $this->validationSummaryBtnValidate = $value;
    }

    public function getValidationSummaryBtnValidate()
    {
        return $this->validationSummaryBtnValidate;
    }

    /**
     * Permet de charger l'objet passé en paramètres par référence.
     *
     * @param CommonConsultation, CommonCategorieLot $objet
     *
     * @return CommonConsultation, CommonCategorieLot $objet
     */
    public function save(&$objet = null)
    {
    }

    public function enabledElement()
    {
        return match ($this->mode) {
            '0' => false,
            '1' => true,
            default => false,
        };
    }

    /**
     * Permet d'afficher les clauses d'achat responsable consultation d'un objet.
     *
     * @param CommonConsultation, CommonCategorieLot $objet
     */
    public function afficherClausesConsultation($objet = null)
    {
        $this->_objet = $objet;
    }

    /*
     * on l'utilise si on veux afficher plusieurs instance de ce composant dans une même page
     */
    public function getIdPanel()
    {
        if ($this->idElement) {
            return $this->idElement;
        } else {
            return '';
        }
    }

    /**
     * Permet de valider les clause.
     *
     * @param bool   $isValid
     * @param array  $arrayMsgErreur tableau des message d'erreurs
     * @param string $scriptJs       le script js
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function validerAchatResponsable(&$isValid, &$arrayMsgErreur, &$scriptJs)
    {
    }

    public function getUrl(): string
    {
        $url = '';
        if ($this->_objet instanceof CommonTContratTitulaire && $this->_objet->getIdContratTitulaire()) {
            $url = '/api/v2/contrat-titulaires/' . $this->_objet->getIdContratTitulaire();
        }

        return $url;
    }

    public function getLabelTrans(string $label)
    {
        return Prado::localize($label);
    }
}
