<?php

namespace Application\Controls;

use Application\Propel\Mpe\CommonCategorieLot;
use Application\Propel\Mpe\CommonConsultation;
use Prado\Prado;

/**
 * Page de gestion du bloc icone "Achat Responsable" de la consultation.
 *
 * @author AMAl EL BEKKAOUI <amal.elbekkaoui@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 4
 */
class IconeAchatResponsableConsultation extends MpeTTemplateControl
{
    public $objet;

    /**
     * @return mixed
     */
    public function getObjet()
    {
        return $this->objet;
    }

    /**
     * @param mixed $objet
     */
    public function setObjet($objet)
    {
        $this->objet = $objet;
    }

    public function getClauseMarcheReserve($objet)
    {
        $html = '';
        if ($objet instanceof CommonConsultation || $objet instanceof CommonCategorieLot) {
            if ($objet->hasThisClause('ClauseSocialeAteliersProteges')) {
                $html .= '<abbr title="'.Prado::localize('ETABLISSEMENTS_SERVICES_AIDE_TRAVAIL_ENTREPRISES_ADAPTEES').'">'.Prado::localize('ESAT_EA').'</abbr>';
            }
            if ($objet->hasThisClause('ClauseSocialeAteliersProteges') && ($objet->hasThisClause('ClauseSocialeSiae') || $objet->hasThisClause('ClauseSocialeEss'))) {
                $html .= ',';
            }
            if ($objet->hasThisClause('ClauseSocialeSiae')) {
                $html .= '<abbr title="'.Prado::localize('STRUCTURES_INSERTION_ACTIVITE_ECONOMIQUE').'">'.Prado::localize('SIAE').'</abbr>';
            }
            if ($objet->hasThisClause('ClauseSocialeSiae') && $objet->hasThisClause('ClauseSocialeEss')) {
                $html .= ',';
            }
            if ($objet->hasThisClause('ClauseSocialeEss')) {
                $html .= '<abbr title="'.Prado::localize('ENTREPRISE_ECONOMIE_SOCIALE_SOLIDAIRE').'">'.Prado::localize('EESS').'</abbr>';
            }
            if ($html) {
                $html .= '.';
            }
        }

        return $html;
    }
}
