<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_CommonReferentiel;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Module;

/*
 * Created on 15 mai 2013
 *
 * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
 * @package
 */
class ReferentielPiecesDossierReponse extends MpeTTemplateControl
{
    private $validationGroup;
    private $validationSummary;
    private $_donneeComplementaire;

    public function getDonneeComplementaire()
    {
        return $this->_donneeComplementaire;
    }

    public function setDonneeComplementaire($donneeComplementaire)
    {
        $this->_donneeComplementaire = $donneeComplementaire;
    }

    public function setValidationGroup($validationGroup)
    {
        $this->validationGroup = $validationGroup;
    }

    public function getValidationGroup()
    {
        return $this->validationGroup;
    }

    public function setValidationSummary($validationSummary)
    {
        $this->validationSummary = $validationSummary;
    }

    public function getValidationSummary()
    {
        return $this->validationSummary;
    }

    public function onLoad($param)
    {
        if (!$this->Page->IsPostBack) {
            self::chargerComposant();
        }
    }

    public function displayReferentielPiecesDossiers()
    {
        $valeur = (new Atexo_CommonReferentiel())->retrieveValeurReferentiel(Atexo_Config::getParameter('VALEUR_REFERENTIEL_PIECES_ADRESSES'));
        if ($valeur) {
            if ($valeur[0]) {
                $this->piecesDossierAdmin->Text = $valeur[0]->getLibelleValeurReferentielTraduit();
            }
            if ($valeur[1]) {
                $this->piecesDossierTech->Text = $valeur[1]->getLibelleValeurReferentielTraduit();
            }
            if ($valeur[2]) {
                $this->piecesDossierAdditif->Text = $valeur[2]->getLibelleValeurReferentielTraduit();
            }
        }
    }

    public function chargerComposant()
    {
        if (Atexo_Module::isEnabled('ConsultationPiecesDossiers') && Atexo_Module::isEnabled('DonneesComplementaires')) {
            $this->panelPiecesDossierse->setDisplay('Dynamic');
            self::displayReferentielPiecesDossiers();
            $this->piecesDossierAdmin->setText($this->_donneeComplementaire->getPiecesDossierAdminTraduit());
            $this->piecesDossierTech->setText($this->_donneeComplementaire->getPiecesDossierTechTraduit());
            $this->piecesDossierAdditif->setText($this->_donneeComplementaire->getPiecesDossierAdditifTraduit());
        } else {
            $this->panelPiecesDossierse->setDisplay('None');
        }
    }

    public function savePiecesDossierReponse(&$donneeComplementaire)
    {
        $this->_donneeComplementaire = $donneeComplementaire;
        $this->_donneeComplementaire->setPiecesDossierAdminTraduit($this->piecesDossierAdmin->getText());
        $this->_donneeComplementaire->setPiecesDossierTechTraduit($this->piecesDossierTech->getText());
        $this->_donneeComplementaire->setPiecesDossierAdditifTraduit($this->piecesDossierAdditif->getText());
    }
}
