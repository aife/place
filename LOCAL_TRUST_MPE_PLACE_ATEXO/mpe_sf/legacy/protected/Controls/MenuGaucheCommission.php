<?php

namespace Application\Controls;

class MenuGaucheCommission extends MpeTPage
{
    protected array $ArrayPages = [];

    public function onLoad($param)
    {
        $this->ArrayPages['GESTION_SEANCES'] = [
                                                        '0' => 'Commission.FormulaireSeance', '1' => 'Commission.SuiviSeance',
        ];
        $this->ArrayPages['ADMINISTRATION'] = [
                                                        '0' => 'Commission.ListeCommission', '1' => 'Commission.ListeIntervenantsExternes',
        ];
    }
}
