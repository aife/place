<?php

namespace Application\Controls;

use Application\Controls\MpeTTemplateControl;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Redaction;
use Application\Service\Atexo\Redaction\Atexo_Redaction_Document;

/**
 * commentaires.
 *
 * @author  Ezziani Loubna <loubna.ezziani@atexo.com>
 * @copyright Atexo 2012
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class DCEComposantAjouter extends MpeTTemplateControl
{
    public $_consultation = '';
    private $_isRadio;
    private $_isDocRC;

    public function getIsRadio()
    {
        return $this->_isRadio;
    }

    public function setIsRadio($value)
    {
        $this->_isRadio = $value;
    }

    public function getIsDocRC()
    {
        return $this->_isDocRC;
    }

    public function setIsDocRC($value)
    {
        $this->_isDocRC = $value;
    }

    public function onLoad($param)
    {
    }

    public function isRadio()
    {
        return 'true' == $this->getIsRadio() ? true : false;
    }

    public function displayDocsRedac()
    {
        $redaction = new Atexo_Redaction($this->_consultation);
        $file = $redaction->getListeDocument();
        $dataSource = [];
        $xml = strstr($file, '<?xml');
        if ($xml) {
            $dataSource = (new Atexo_Redaction_Document())->createListeRedacFromXml($xml);
        }
        $this->displayDocs($dataSource);
    }

    public function displayDocs($dataSource)
    {
        if (0 == (is_countable($dataSource) ? count($dataSource) : 0)) {
            $this->panelListeDocsRedac->setDisplay('None');
            $this->panelNoElementFound->setDisplay('Dynamic');
        } else {
            $this->repeaterDocsRedac->DataSource = $dataSource;
            $this->repeaterDocsRedac->DataBind();
            $this->panelListeDocsRedac->setDisplay('Dynamic');
            $this->panelNoElementFound->setDisplay('None');
        }
    }

    public function getIdBlob($id, $name)
    {
        $redaction = new Atexo_Redaction($this->_consultation);

        return $redaction->getContentDocument($id, $name, Atexo_CurrentUser::getCurrentOrganism());
    }

    public function getFileSizeMax(){
        return Atexo_Config::getParameter("MAX_UPLOAD_FILE_SIZE");
    }
}
