<!--Debut message Erreur-->
<com:TActivePanel ID="panelMessage" Attributes.style="display:none;" cssClass="form-bloc-erreur">
        	<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
            <div class="content">
            	<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-erreur.gif" class="picto-erreur" alt="Erreur" />
				<div class="message bloc-700">
					<!--Debut Message type d'erreur general-->
					<span><com:TActiveLabel ID="labelMessage" Attributes.style="color:red"/></span>
					<!--Fin Message type d'erreur general-->
				</div>
                <div class="breaker"></div>
            </div>
        <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
	<com:TActiveLabel id="labelJavascript"/> 
</com:TActivePanel>
<!--Fin message Erreur-->
