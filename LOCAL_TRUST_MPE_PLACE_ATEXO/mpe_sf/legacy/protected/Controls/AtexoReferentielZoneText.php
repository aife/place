<?php

namespace Application\Controls;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonLtReferentiel;
use Application\Propel\Mpe\CommonLtReferentielPeer;
use Application\Propel\Mpe\CommonReferentielConsultation;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Referentiel\Atexo_Referentiel_Referentiel;
use Application\Service\Atexo\Referentiel\Atexo_Referentiel_ReferentielVo;
use Prado\Prado;

/**
 * commentaires.
 *
 * @author Hajar HANNAD <hajar.hannad@atexo.com>
 * @copyright Atexo 2011
 *
 * @version 1.0
 *
 * @since MPE-4.0
 */
class AtexoReferentielZoneText extends MpeTTemplateControl
{
    private bool $consultation = false;
    private bool $lot = false;
    private bool $dependanceLot = false;
    private bool $obligatoire = false;
    private bool $typeSearch = false;
    private $validationGroup;
    private $validationSummary;
    private $cssClass;
    private $mode;
    private bool $entreprise = false;
    private $idPage;
    private bool $type = false;
    private bool $label = false;
    private bool $check = false;
    private $organisme;
    private bool $line = true;

    /**
     * setter de l'attribut line.
     *
     * @param $Line
     *
     * @author Oussama SALMANI <oussama.salmani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function setLine($Line)
    {
        $this->line = $Line;
    }

    /**
     * getter de l'attribut line.
     *
     * @return la valeur de l'attribut line
     *
     * @author Oussama SALMANI <oussama.salmani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function getLine()
    {
        return $this->line;
    }

    public function setConsultation($consultation)
    {
        $this->consultation = $consultation;
    }

    public function getConsultation()
    {
        return $this->consultation;
    }

    public function setLot($lot)
    {
        $this->lot = $lot;
    }

    public function getLot()
    {
        return $this->lot;
    }

    public function setDependanceLot($dependanceLot)
    {
        $this->dependanceLot = $dependanceLot;
    }

    public function getDependanceLot()
    {
        return $this->dependanceLot;
    }

    public function setObligatoire($obligatoire)
    {
        $this->obligatoire = $obligatoire;
    }

    public function getObligatoire()
    {
        return $this->obligatoire;
    }

    public function setTypeSearch($typeSearch)
    {
        $this->typeSearch = $typeSearch;
    }

    public function getTypeSearch()
    {
        return $this->typeSearch;
    }

    public function setValidationGroup($validationGroup)
    {
        $this->validationGroup = $validationGroup;
    }

    public function getValidationGroup()
    {
        return $this->validationGroup;
    }

    public function setValidationSummary($validationSummary)
    {
        $this->validationSummary = $validationSummary;
    }

    public function getValidationSummary()
    {
        return $this->validationSummary;
    }

    public function setCssClass($cssClass)
    {
        $this->cssClass = $cssClass;
    }

    public function getCssClass()
    {
        return $this->cssClass;
    }

    public function setMode($mode)
    {
        $this->mode = $mode;
    }

    public function getMode()
    {
        return $this->mode;
    }

    public function setEntreprise($entreprise)
    {
        $this->entreprise = $entreprise;
    }

    public function getEntreprise()
    {
        return $this->entreprise;
    }

    public function setIdPage($idPage)
    {
        $this->idPage = $idPage;
    }

    public function getIdPage()
    {
        //echo $this->idPage;exit;
        return $this->idPage;
    }

    public function setType($type)
    {
        $this->type = $type;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setLabel($label)
    {
        $this->label = $label;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function setCheck($check)
    {
        $this->check = $check;
    }

    public function getCheck()
    {
        return $this->check;
    }

    public function setOrganisme($organisme)
    {
        $this->organisme = $organisme;
    }

    public function getOrganisme()
    {
        if (Atexo_CurrentUser::isAgent()) {
            return Atexo_CurrentUser::getCurrentOrganism();
        } else {
            return null;
        }
    }

    public function getLtReferentiels($page, $organisme = null)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();

        if ($this->entreprise) {
            $c->add(CommonLtReferentielPeer::ENTREPRISE, 1);
        }
        if ($this->consultation) {
            $c->add(CommonLtReferentielPeer::CONSULTATION, 1);
        }
        if ($this->lot) {
            $c->add(CommonLtReferentielPeer::LOT, 1);
        }

        if ($this->getDependanceLot()) {
            $c->add(CommonLtReferentielPeer::DEPENDANCE_ALLOTISSEMENT, 0);
        }

        if ($page) {
            $c->add(CommonLtReferentielPeer::PAGES, '%#'.$page.'#%', Criteria::LIKE);
        }
        if ($organisme) {
            $cton1 = $c->getNewCriterion(CommonLtReferentielPeer::ORGANISMES, '%#'.$organisme.'#%', Criteria::LIKE);
            $cton2 = $c->getNewCriterion(CommonLtReferentielPeer::ORGANISMES, null, Criteria::ISNULL);
            $cton3 = $c->getNewCriterion(CommonLtReferentielPeer::ORGANISMES, '');
            $cton2->addOr($cton3);
            $cton1->addOr($cton2);
            $c->add($cton1);
        }
        $c->add(CommonLtReferentielPeer::TYPE, 1);

        //la clé utilisée pour le cache est un hash unique du critere de recherche
        $keyCache = 'cached_critere_ref_'.sha1(serialize($c));
        $ltRefs = Prado::getApplication()->Cache->get($keyCache);

        //si non existant dans le cache on interroge la BD
        if (!$ltRefs) {
            $ltRefs = CommonLtReferentielPeer::doSelect($c, $connexionCom);
            //si pas de résultats, on stocke en cache un tableau avec une unité vide sinon on aura null et le cache APC ne sera jamais utilisé
            //du coup c'est une feinte
            if (0 == count($ltRefs)) {
                $ltRefs = [''];
            }
            Prado::getApplication()->Cache->set($keyCache, $ltRefs, Atexo_Config::getParameter('TTL_PRADO_CACHE'));
        }
        //on supprime les éléments vides si on les précédemment mis
        if (1 == (is_countable($ltRefs) ? count($ltRefs) : 0) && !($ltRefs[0] instanceof CommonLtReferentiel)) {
            unset($ltRefs[array_search('', $ltRefs)]);
        }

        return $ltRefs;
    }

    public function afficherTextConsultation($consultationId = null, $lot = null, $organisme = null, $defineValue = null, $postback = false)
    {
        if (!$this->Page->isPostBack || $postback) {
            self::remplirRepeaterReferentielZoneText();
            foreach ($this->RepeaterReferentielZoneText->Items as $Item) {
                $IdRef = $Item->iDLibelle->Text;
                $codeRefPrinc = null;
                $codeRefSec = null;
                if (!$defineValue) {
                    if ($organisme) {
                        $refCons = (new Atexo_Referentiel_Referentiel())->getCommonLtReferentielByRefConsultationAndLot($consultationId, $lot, $IdRef, $organisme);
                        if ($refCons) {
                            $codeRefPrinc = $refCons->getValeurPrincipaleLtReferentiel();
                            $codeRefSec = $refCons->getValeurSecondaireLtReferentiel();
                        }
                    }
                } else {
                    if ($defineValue[$IdRef]) {
                        $codeRefPrinc = $defineValue[$IdRef]->getValuePrinc();
                        $codeRefSec = $defineValue[$IdRef]->getValueSecon();
                    }
                }

                $codeRefPrincFormater = (new Atexo_Config())->toPfEncoding($codeRefPrinc);
                if ('montant' == $Item->typeData->Value && $codeRefPrincFormater) {
                    $codeRefPrincFormater = Atexo_Util::getMontantArronditEspace($codeRefPrinc);
                }
                $Item->labelReferentielZoneText->SetText($codeRefPrincFormater);
                $Item->idReferentielZoneText->SetText($codeRefPrincFormater);
                $Item->oldValue->SetValue($codeRefPrincFormater);
                //Pour l'affichage des cas lt-ref avec type_search = 4
                if (2 == $this->mode && '4' == $Item->typeSearch->Text && '1' == $Item->modeRecherche->value) {
                    if (!empty($codeRefPrincFormater)) {
                        $Item->idReferentielZoneTextFrom->SetText($codeRefPrincFormater);
                    }
                    if (!empty($codeRefSec)) {
                        $Item->idReferentielZoneTextTo->SetText((new Atexo_Config())->toPfEncoding($codeRefSec));
                    }
                }
            }
        }
    }

    public function remplirRepeaterReferentielZoneText()
    {
        if ('' == $this->idPage) {
            $this->idPage = Atexo_Util::atexoHtmlEntities($_GET['page']);
        }
        $organisme = self::getOrganisme();
        if ($organisme) {
            $dataSource = self::getLtReferentiels($this->idPage, $organisme);
        } else {
            $dataSource = self::getLtReferentiels($this->idPage);
        }
        $this->RepeaterReferentielZoneText->DataSource = $dataSource;
        $this->RepeaterReferentielZoneText->DataBind();
    }

    public function saveConsultationAndLot($consultationId, $lot, &$consultation = false)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $organisme = Atexo_CurrentUser::getCurrentOrganism();
        foreach ($this->RepeaterReferentielZoneText->Items as $Item) {
            $IdRef = $Item->iDLibelle->Text;
            $valeurPricLtRef = (new Atexo_Config())->toPfEncoding($Item->idReferentielZoneText->Text);
            if ($consultationId) {
                $CommonRefCons = (new Atexo_Referentiel_Referentiel())->getCommonLtReferentielByRefConsultationAndLot($consultationId, $lot, $IdRef, $organisme);
            }
            if (!$CommonRefCons instanceof CommonReferentielConsultation) {
                $CommonRefCons = new CommonReferentielConsultation();
                $CommonRefCons->setLot($lot);
                $CommonRefCons->setIdLtReferentiel($IdRef);
            }
            $CommonRefCons->setValeurPrincipaleLtReferentiel($valeurPricLtRef);
            if ($consultation instanceof CommonConsultation) {
                $consultation->addCommonReferentielConsultation($CommonRefCons);
            } else {
                $CommonRefCons->setConsultationId($consultationId);
                $CommonRefCons->setReference($consultationId);
                $CommonRefCons->setOrganisme($organisme);
                $CommonRefCons->save($connexionCom);
            }
            unset($CommonRefCons);
        }
    }

    public function getValueReferentielTextVo()
    {
        $value1 = [];
        foreach ($this->RepeaterReferentielZoneText->Items as $Item) {
            if (2 == $this->mode && '1' == $Item->modeRecherche->getValue()) {//mode recherche ds un interval
                $refVo = new Atexo_Referentiel_ReferentielVo();
                $refVo->setId($Item->iDLibelle->Text);
                $refVo->setValuePrinc($Item->idReferentielZoneTextFrom->Text);
                $refVo->setValueSecon($Item->idReferentielZoneTextTo->Text);
                $refVo->setCodeLibelle($Item->codeLibelle->Text);
                $refVo->setType($Item->typeRef->Text);
                $refVo->setTypeSearch($Item->typeSearch->Text);
                $refVo->setOldValue($Item->oldValue->Value);
                $value1[$Item->iDLibelle->Text] = $refVo;
            } else {
                $refVo = new Atexo_Referentiel_ReferentielVo();
                $refVo->setId($Item->iDLibelle->Text);
                $refVo->setValuePrinc($Item->idReferentielZoneText->Text);
                $refVo->setValueSecon($Item->idReferentielZoneText->Text);
                $refVo->setValue($Item->idReferentielZoneText->Text);
                $refVo->setCodeLibelle($Item->codeLibelle->Text);
                $refVo->setType($Item->typeRef->Text);
                $refVo->setTypeSearch($Item->typeSearch->Text);
                $refVo->setOldValue($Item->oldValue->Value);
                $value1[$Item->iDLibelle->Text] = $refVo;
            }
        }

        return $value1;
    }

    /*
     * Retourne les valeur sous format xml
     */
    public function getValeurForXml()
    {
        $valueForXml = '';
        foreach ($this->RepeaterReferentielZoneText->getItems() as $Item) {
            $baliseName = (new Atexo_Referentiel_Referentiel())->getNomAttribue($Item->codeLibelle->Text);
            $refPrinc = $Item->idReferentielZoneText->Text;
            $valeurBalise = $refPrinc;
            if ('' != $valeurBalise) {
                $valueForXml[$baliseName] = $valeurBalise;
            } else {
                $valueForXml[$baliseName] = 'NULL';
            }
        }

        return $valueForXml;
    }

    public function getCas($item)
    {
        return match ($this->mode) {
            0 => $item->getModeAffichage(),
            1 => $item->getModeModification(),
            2 => $item->getModeRecherche(),
            default => $item->getModeModification(),
        };
    }
}
