<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Recherches;
use Prado\Prado;

/**
 * permet de gerer des recherches favorites sauvegarsées.
 *
 * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
 *
 * @version 1.0
 *
 * @since 4.8.0
 *
 * @copyright Atexo 2014
 */
class AtexoRecherchesFavorites extends MpeTTemplateControl
{
    private $idCreateur = null;
    private $typeCreateur = null;
    private $typeAvisConsultation;
    private $callFrom = null;

    /**
     * permet d'initialiser le composant selon les parametre qu'on lui donne a la source.
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function initConposant()
    {
        $this->panelMessageErreur->setVisible(false);
        $this->displayRecherches();
    }

    /**
     * permet d'afficher la liste des recherche favorites sauvegardé.
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function displayRecherches()
    {
        $mesRecherche = (new Atexo_Entreprise_Recherches())->retreiveRecherches($this->idCreateur, $this->typeCreateur, false, false, $this->getTypeAvisConsultation());
        if (count($mesRecherche)) {
            $this->repeaterAlertes->DataSource = $mesRecherche;
            $this->repeaterAlertes->DataBind();
        } else {
            $this->panelListeRecherche->setVisible(false);
            $this->panelAvertissement->setVisible(true);
            $this->panelAvertissement->setMessage(Prado::localize('DEFINE_LISTE_RECHERCHES_SAUVEGARDE_VIDE'));
        }
    }

    /**
     * permet 'actualiser la liste des recherche favorites sauvegardé.
     *
     * @param mixed $sender
     * @param mixed $param
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function refreshRepeater($sender, $param)
    {
        $mesRecherche = (new Atexo_Entreprise_Recherches())->retreiveRecherches($this->idCreateur, $this->typeCreateur, false, false, $this->getTypeAvisConsultation());
        if (count($mesRecherche)) {
            $this->repeaterAlertes->DataSource = $mesRecherche;
            $this->repeaterAlertes->DataBind();
            $this->panelRecherches->render($param->NewWriter);
        } else {
            $this->panelListeRecherche->setVisible(false);
            $this->panelAvertissement->setVisible(true);
            $this->panelAvertissement->setMessage(Prado::localize('DEFINE_LISTE_RECHERCHES_SAUVEGARDE_VIDE'));
            $this->panelRecherches->render($param->NewWriter);
        }
    }

    /**
     * permet de faire une action (Modify=>modification,Search=>recherche,Delete=>suppression) selon le parametre donne.
     *
     * @param mixed $sender
     * @param mixed $param
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function actionRecherche($sender, $param)
    {
        switch ($param->CommandName) {
            case 'Delete':
                (new Atexo_Entreprise_Recherches())->deleteMaRecherche($this->idToDetele->getValue());
                break;
            case 'Search':
                self::launchSerch($param->CommandParameter);
                break;
            case 'Modify':
                $nomRecherche = $this->rechercheName->SafeText;
                                (new Atexo_Entreprise_Recherches())->ModifierMaRecherche($param->CommandParameter, $nomRecherche);
                                $this->scriptJs->text = "<script>J('.modal-recherche').dialog('close');</script>";
                break;
        }
    }

    /**
     * permet de lancer une recherche.
     *
     * @param int $idrecherche id de la recherche favorite sauvegardée
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function launchSerch($idrecherche)
    {
        if ($this->typeCreateur == Atexo_Config::getParameter('TYPE_CREATEUR_AGENT')) {
            if ($this->getTypeAvisConsultation()) {
                $this->Response->Redirect('index.php?page=Agent.TableauDeBord&idRecherche='.$idrecherche);
            } else {
                $this->Response->Redirect('index.php?page=Agent.TableauDeBordAnnonce&idRecherche='.$idrecherche);
            }
        } elseif ($this->typeCreateur == Atexo_Config::getParameter('TYPE_CREATEUR_ENTREPRISE')) {
            if ($this->getTypeAvisConsultation()) {
                $this->Response->Redirect('index.php?page=Entreprise.EntrepriseAdvancedSearch&searchAnnCons&idRecherche='.$idrecherche);
            } else {
                $this->Response->Redirect('index.php?page=Entreprise.EntrepriseAdvancedSearch&AllAnn&idRecherche='.$idrecherche);
            }
        }
    }

    /**
     * modifie la valeur de [idCreateur].
     *
     * @param int $idCreateur la valeur a mettr
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setIdCreateur($idCreateur)
    {
        $this->idCreateur = $idCreateur;
    }

    /**
     * recupere la valeur du [idCreateur].
     *
     * @return int la valeur courante [idCreateur]
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getIdCreateur()
    {
        return $this->idCreateur;
    }

    /**
     * modifie la valeur de [typeCreateur].
     *
     * @param int $typeCreateur la valeur a mettr
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setTypeCreateur($typeCreateur)
    {
        $this->typeCreateur = $typeCreateur;
    }

    /**
     * recupere la valeur du [typeCreateur].
     *
     * @return int la valeur courante [typeCreateur]
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getTypeCreateur()
    {
        return $this->typeCreateur;
    }

    /**
     * Permet d'acceder à la page d'edition de la recherche
     * Redirige vers la page d'edition de la recherche.
     *
     * @param $sender
     * @param $param
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2014
     */
    public function modifierMaRecherche($sender, $param)
    {
        $url = null;
        $idrecherche = $param->CommandParameter;
        if ($this->typeCreateur == Atexo_Config::getParameter('TYPE_CREATEUR_AGENT')) {
            $url = 'index.php?page=Agent.TableauDeBord&AS=1&searchAnnCons&idRecherche='.$idrecherche.'&search';
        } elseif ($this->typeCreateur == Atexo_Config::getParameter('TYPE_CREATEUR_ENTREPRISE')) {
            $url = 'index.php?page=Entreprise.EntrepriseAdvancedSearch&searchAnnCons&idRecherche='.$idrecherche.'&search';
        }
        $this->Response->Redirect($url);
    }

    /**
     * recupere le chemin d'advanced search selon le createur.
     *
     * @return string le chemin vers advanced search
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2014
     */
    public function getCheminAdvancedSearch()
    {
        if ($this->getTypeCreateur() == Atexo_Config::getParameter('TYPE_CREATEUR_AGENT')) {
            if ($this->getTypeAvisConsultation()) {
                return '?page=Agent.TableauDeBord&AS=1&searchAnnCons';
            } else {
                return '?page=Agent.TableauDeBordAnnonce&AS=1&annonce';
            }
        } elseif ($this->getTypeCreateur() == Atexo_Config::getParameter('TYPE_CREATEUR_ENTREPRISE')) {
            if ($this->getTypeAvisConsultation()) {
                return '?page=Entreprise.EntrepriseAdvancedSearch&searchAnnCons';
            } else {
                return '?page=Entreprise.EntrepriseAdvancedSearch&AllAnn';
            }
        }

        return '';
    }

    /**
     * Modifie la valeur de [callFrom].
     *
     * @param string $callFrom : la valeur a mettre
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9
     *
     * @copyright Atexo 2015
     */
    public function setCallFrom($callFrom)
    {
        $this->callFrom = $callFrom;
    }

    /**
     * Recupere la valeur de [callFrom].
     *
     * @return string la valeur courante [callFrom]
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9
     *
     * @copyright Atexo 2015
     */
    public function getCallFrom()
    {
        return $this->callFrom;
    }

    /**
     * Permet de recuperer le text du fil d'ariane.
     *
     * @return string : fil d'ariane a afficher
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getTextFileAriane()
    {
        if ('agent' == $this->getCallFrom()) {
            return Prado::localize('TEXT_GESTION_RECHERCHES_SAUVEGARDEES');
        } elseif ('entreprise' == $this->getCallFrom()) {
            return Prado::localize('TEXT_GESTION_ALERTES_RECHERCHES_SAUVEGARDEES');
        }
    }

    /**
     * Permet de recuperer le text du titre du tableau de recherche/alerte.
     *
     * @return string : texte a afficher
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function getTitreTableauRecherche()
    {
        if ('agent' == $this->getCallFrom()) {
            return Prado::localize('TEXT_MES_RECHERCHES_SAUVEGARDEES');
        } elseif ('entreprise' == $this->getCallFrom()) {
            return Prado::localize('TEXT_MES_ALERTES_RECHERCHES_SAUVEGARDEES');
        }
    }

    /**
     * @return mixed
     */
    public function getTypeAvisConsultation()
    {
        return $this->typeAvisConsultation;
    }

    /**
     * @param mixed $typeAvisConsultation
     */
    public function setTypeAvisConsultation($typeAvisConsultation)
    {
        $this->typeAvisConsultation = $typeAvisConsultation;
    }
}
