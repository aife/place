<?php

namespace Application\Controls;

/*
 * Created on 7 août 2013
 *
 * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
 * @package
 */
class PanelMessageInformation extends MpeTPage
{
    public $message;
    public string $cssClass = 'message bloc-700';

    public function getCssClass()
    {
        return $this->cssClass;
    }

    public function setCssClass($value)
    {
        $this->cssClass = $value;
    }

    public function setMessage($message)
    {
        $this->message = $message;
    }

    public function getMessage()
    {
        return $this->message;
    }
}
