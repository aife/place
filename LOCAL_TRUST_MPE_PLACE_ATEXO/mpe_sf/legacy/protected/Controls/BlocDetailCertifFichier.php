<?php

namespace Application\Controls;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonFichierEnveloppe;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Util;
use Prado\Prado;

/**
 * commentaires.
 *
 * @author khadija Chouika <khadija.chouika@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 2.1
 *
 * @since MPE-4.0
 */
class BlocDetailCertifFichier extends MpeTTemplateControl
{
    public CommonFichierEnveloppe $fichier;
    public $avecSignature;
    public $typeSignature;
    public $_enveloppeCrypte;

    public function getHashId()
    {
        return md5($this->fichier->getIdFichier());
    }

    public function getFichier()
    {
        return $this->fichier;
    }

    public function setFichier($value)
    {
        $this->fichier = $value;
    }

    public function getAvecSignature()
    {
        return $this->avecSignature;
    }

    public function setAvecSignature($value)
    {
        $this->avecSignature = $value;
    }

    public function getTypeSignature()
    {
        return $this->typeSignature;
    }

    public function setTypeSignature($value)
    {
        $this->typeSignature = $value;
    }

    public function getEnveloppeCrypte()
    {
        return $this->_enveloppeCrypte;
    }

    public function setEnveloppeCrypte($value)
    {
        $this->_enveloppeCrypte = $value;
    }

    public function onLoad($param)
    {
        if (!$this->Page->isPostBack) {
            if ($this->fichier instanceof CommonFichierEnveloppe) {
                $this->chargerDetailCertif($this->fichier);
            }
        }
    }

    /**
     * Permet de charger le detail de la signature
     * Param : $fichier type commonFichierEnveloppe
     * Param : $avecSignature
     * Param : $typeSignature.
     */
    public function chargerDetailCertif($fichier)
    {
        if ($this->getResultatValiditeSignature()) {
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
            $enveloppe = $fichier->getCommonEnveloppe($connexionCom);
            $this->_enveloppeCrypte = $enveloppe->getCryptage();
            $arrayFichiers = $this->Page->getViewState('infosFichieParEnveloppe');
            $fichierInfos = $arrayFichiers[$enveloppe->getIdEnveloppeElectro()][$fichier->getIdFichier()];
            if ($fichierInfos['EmailSubject']) {
                $this->identifiantE->Text = $fichierInfos['EmailSubject'];
            } else {
                $this->identifiantE->Text = 'N/A';
            }
            if ($fichierInfos['CnSubject']) {
                $this->identifiantCN->Text = $fichierInfos['CnSubject'];
            } else {
                $this->identifiantCN->Text = 'N/A';
            }
            if ($fichierInfos['OuSubject']) {
                $this->identifiantOU->Text = $fichierInfos['OuSubject'];
            } else {
                $this->identifiantOU->Text = 'N/A';
            }
            if ($fichierInfos['OSubject']) {
                $this->identifiantO->Text = $fichierInfos['OSubject'];
            } else {
                $this->identifiantO->Text = 'N/A';
            }
            if ($fichierInfos['CSubject']) {
                $this->identifiantC->Text = $fichierInfos['CSubject'];
            } else {
                $this->identifiantC->Text = 'N/A';
            }
            if ($fichierInfos['CnEmmeteur']) {
                $this->identifiantCN_PAR->Text = $fichierInfos['CnEmmeteur'];
            } else {
                $this->identifiantCN_PAR->Text = 'N/A';
            }
            if ($fichierInfos['OuEmmeteur']) {
                $this->identifiantOU_PAR->Text = $fichierInfos['OuEmmeteur'];
            } else {
                $this->identifiantOU_PAR->Text = 'N/A';
            }
            if ($fichierInfos['OEmmeteur']) {
                $this->identifiantO_PAR->Text = $fichierInfos['OEmmeteur'];
            } else {
                $this->identifiantO_PAR->Text = 'N/A';
            }
            if ($fichierInfos['CEmmeteur']) {
                $this->identifiantC_PAR->Text = $fichierInfos['CEmmeteur'];
            } else {
                $this->identifiantC_PAR->Text = 'N/A';
            }
            if ($fichierInfos['dateDu']) {
                $this->date_Du->Text = Atexo_Util::iso2frnDateTime($fichierInfos['dateDu']);
            } else {
                $this->date_Du->Text = '-';
            }
            if ($fichierInfos['dateA']) {
                $this->date_Au->Text = Atexo_Util::iso2frnDateTime($fichierInfos['dateA']);
            } else {
                $this->date_Au->Text = '-';
            }
            $this->periodeValidite->setStatut($fichierInfos['CertifPeriodValiditeIcone']);
            $this->chaine->setStatut($fichierInfos['CertifChaineCertifIcone']);
            $this->CRL->setStatut($fichierInfos['CertifCRLIcone']);
            $this->NR->setStatut($fichierInfos['SignatireValiditeIcone']);

            $fileNameJeton = $fichierInfos['jetonSignature'];
            $this->dateControleIntergrite->Text = $arrayFichiers[$enveloppe->getIdEnveloppeElectro()][$fichier->getIdFichier()]['dateControleIntegrite'] = $this->getDateControleIntergrite();
            $this->Page->setViewState('infosFichieParEnveloppe', $arrayFichiers);
            $this->fileName->Text = $fileNameJeton;
            $this->UrldownloadJeton->NavigateUrl = 'index.php?page=Agent.downloadJetonSignature&id='.$this->Page->getViewState('consultation_id')
            .'&idfichier='.$fichier->getIdFichier().'&enveloppe='.$enveloppe->getIdEnveloppeElectro();
            $this->UrldownloadJeton->ToolTip = Prado::localize('TEXT_TELECHARGER_FICHIER').' - '.$this->getJetonSize($fileNameJeton, $fichier);

            $this->panelInfoComp->setVisible($fichierInfos['hasInfoComp']);
            $this->certificatSignature->setText($fichierInfos['certificatSignature']);
            $this->infoBulleQualification->setText($fichierInfos['certificatSignatureInfoBulle']);

            $this->formatSignature->setText($fichierInfos['formatSignature']);
            $this->dateIndicative->setText($fichierInfos['dateIndicative']);
            $this->jetonHorodatage->setText($fichierInfos['jetonSignatureHorodate']);
        }
    }

    /*
     * Permet de retourne le taille du jeton
     */
    public function getJetonSize($signtaureFileName, $fichier)
    {
        if ($fichier instanceof CommonFichierEnveloppe) {
            $signature = $fichier->getSignatureFichier();

            return Atexo_Util::getContentFileSize($signtaureFileName, base64_decode($signature));
        }
    }

    public function getResultatValiditeSignature()
    {
        if ($this->fichier) {
            $resultatStatut = $this->fichier->getResultatValiditeSignatureWithMessage();
            if ('OK' == $resultatStatut['statut']) {
                $this->panelConfirm->Visible = true;

                return true;
            } elseif ('NOK' == $resultatStatut['statut']) {
                $this->panelErreur->Visible = true;
                $this->panelErreur->message = $resultatStatut['message'];

                return true;
            } elseif ('UNKNOWN' == $resultatStatut['statut']) {
                $this->panelAvert->message = $resultatStatut['message'];
                $this->panelAvert->Visible = true;

                return true;
            } elseif ('SANS' == $resultatStatut['statut']) {
                $this->panelAvert->message = $resultatStatut['message'];
                $this->panelAvert->Visible = true;
                $this->panelAucunJeton->Visible = true;
                $this->blocDetailCertif->Visible = false;

                return false;
            } elseif ('UNVERIFIED' == $resultatStatut['statut']) {
                $this->panelAvert->Visible = false;
                $this->panelSignatureNonVerifiable->Visible = true;
                $this->panelAucunJeton->Visible = false;
                $this->blocDetailCertif->Visible = false;

                return false;
            } else {
                $this->panelInfo->Visible = true;
                $this->panelAucunJeton->Visible = true;
                $this->blocDetailCertif->Visible = false;

                return false;
            }
        }
    }

    public function getStatutCertificatPeriodeValidite()
    {
        if ($this->fichier) {
            return $this->fichier->getStatutCertificatPeriodeValidite($this->typeSignature, $this->avecSignature);
        }
    }

    public function getStatutValiditeCrlCertificat()
    {
        if ($this->fichier) {
            return $this->fichier->getStatutValiditeCrlCertificat($this->typeSignature, $this->avecSignature);
        }
    }

    public function getStatutValiditeChaineCertification()
    {
        if ($this->fichier) {
            return $this->fichier->getStatutValiditeChaineCertification($this->typeSignature, $this->avecSignature);
        }
    }

    public function getSignatureValidite()
    {
        if ($this->fichier) {
            return $this->fichier->getSignatureValidite($this->avecSignature, $this->_enveloppeCrypte);
        }
    }

    public function getDateControleIntergrite()
    {
        return Atexo_Util::iso2frnDateTime(date('Y-m-d H:i:s'));
    }

    public function getDateControleValidite()
    {
        if ($this->fichier) {
            return $this->fichier->getCommonOffres()->getDateRemisePlie();
        }
    }

    /*
     * Permet de retourne la css selon le statut
     */
    public function getClassCss($statut)
    {
        return match (strtoupper($statut)) {
            'OK' => 'statut-resultat statut-valide',
            'NOK' => 'statut-resultat statut-erreur',
            'UNKNOWN' => 'statut-resultat statut-avertissement',
            'SANS' => 'statut-resultat statut-vide',
            'UNVERIFIED' => 'statut-resultat statut-vide',
            default => 'statut-resultat statut-vide',
        };
    }

    public function getNomReferentielCertificat()
    {
        if ($this->fichier) {
            return $this->fichier->getNameReferentielCertificat();
        }
    }

    public function getStatutReferentielCertificat()
    {
        if ($this->fichier) {
            return $this->fichier->getStatutValiditeReferentielCertificat($this->avecSignature);
        }
    }
}
