<!--Debut Bloc Recap Infos marche-->
<div class="form-bloc" id="recap-consultation">
<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
<div class="content">
	<a class="title-toggle" onclick="togglePanel(this,'infosPrincipales');" href="javascript:void(0);">&nbsp;</a>
	<div class="recap-bloc">
		<div class="line">
			<div class="intitule-150"><strong><com:TTranslate>TEXT_TITULAIRE_MARCHE</com:TTranslate></strong></div>
			<div class="content-bloc bloc-590"><com:TLiteral ID="titulaireMarche" /></div>
		</div>
		<div class="line">
			<div class="intitule-150"><strong><com:TTranslate>TEXT_NUMERO_MARCHE</com:TTranslate> :</strong></div>

			<div class="content-bloc bloc-590"><com:TLiteral ID="numeroMarche" /></div>
		</div>
		<div class="line">
			<div class="intitule-150"><strong><com:TTranslate>DATE_NOTIFICATION</com:TTranslate> :</strong></div>
			<div class="content-bloc bloc-590"><com:TLiteral ID="dateNotification" /></div>
		</div>
		<div class="line">

			<div class="intitule-150"><strong><com:TTranslate>TEXT_MONTANT_INITIAL_MARCHE</com:TTranslate> :</strong></div>
			<div class="content-bloc bloc-590"><com:TLiteral ID="montantInitialMarche" /> <com:TTranslate>DEFINE_EURO_HT</com:TTranslate></div>
		</div>
		<div id="infosPrincipales" style="display:none;" class="panel-toggle">
			<div class="line">
				<div class="intitule-250"><strong><com:TTranslate>TEXT_NATURE_PRINCIPALE_PRESTATION</com:TTranslate> :</strong></div>
				<div class="content-bloc bloc-500"><com:TLiteral ID="categorie" /></div>

			</div>
			<div class="line">
				<div class="intitule-250"><strong><com:TTranslate>DEFINE_OBJET_MARCHE</com:TTranslate> :</strong></div>
				<div class="content-bloc bloc-500"><com:TLiteral ID="objetMarche" /></div>
			</div>
			<div class="line">
				<div class="intitule-250"><strong><com:TTranslate>TEXT_CONSULTATION_REFERENCE</com:TTranslate> :</strong></div>

				<div class="content-bloc bloc-500"><a href="index.php?page=Agent.DonneesComplementairesConsultation&id=<%=$this->getConsultation()->getConsultationId()%>"><com:TLiteral ID="referenceUtilisateur" /></a></div>
			</div>
			<div class="breaker"></div>
		</div>
	</div>
	<div class="spacer-small"></div>
	<div class="breaker"></div>
</div>

<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
</div>
<!--Fin Bloc Recap Infos marche-->