<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_Config;
use Prado\Prado;

/**
 * commentaires.
 *
 * @author Khadija CHOUIKA <khadija.chouika@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 1.0
 *
 * @since MPE-4.0
 */
class AtexoQueryMultiFilesUpload extends MpeTTemplateControl
{
    protected string $cssClass = 'ajout-file-upload';
    protected bool $afficherProgress = true;
    public $_fonctionJs;
    protected $_maxSize = '';
    protected bool $style = false;
    protected string $size = '';
    protected $_titre;
    protected bool $_enabled = true;

    public function setFileSizeMax($value)
    {
        $this->_maxSize = $value;
    }

    public function getFileSizeMax()
    {
        return $this->_maxSize;
    }

    public function setCssClass($value)
    {
        $this->cssClass = $value;
    }

    public function getCssClass()
    {
        return $this->cssClass;
    }

    public function setAfficherProgress($value)
    {
        $this->afficherProgress = $value;
    }

    public function getAfficherProgress()
    {
        return $this->afficherProgress;
    }

    public function setStyle($value)
    {
        $this->style = $value;
    }

    public function getStyle()
    {
        return $this->style;
    }

    public function setSize($value)
    {
        $this->size = $value;
    }

    public function getSize()
    {
        return $this->size;
    }

    public function setTitre($value)
    {
        $this->_titre = $value;
    }

    public function getTitre()
    {
        return $this->_titre;
    }

    public function refrechCompsant()
    {
        $this->NomFichier->value = '';
        $this->hasFile->value = '';
    }

    public function HasFile()
    {
        return $this->hasFile->value;
    }

    public function getLocalNames()
    {
        $reponsesServeur = null;
        if ($this->reponseServeur->value) {
            $reponsesServeur = explode('|', $this->reponseServeur->value);
        }
        $resultat = [];
        if (is_array($reponsesServeur)) {
            foreach ($reponsesServeur as $reponseServeur) {
                $resultat[] = Atexo_Config::getParameter('CHEMIN_JQUERY_UPLOAD_FILE').$reponseServeur;
            }
        }

        return $resultat;
    }

    public function getFilesNames()
    {
        $filesName = [];
        if ($this->NomFichier->value) {
            $filesName = explode('|', $this->NomFichier->value);
        }

        return $filesName;
    }

    public function setEnabled($value)
    {
        $this->_enabled = $value;
    }

    public function getEnabled($checkParents = false)
    {
        return $this->_enabled;
    }

    public function getDisabled()
    {
        $disabled = '';
        if (!$this->getEnabled()) {
            $disabled = 'disabled="disabled"';
        }

        return $disabled;
    }

    /**
     * @return mixed
     */
    public function getFonctionJs()
    {
        return $this->_fonctionJs;
    }

    /**
     * @param mixed $fonctionJs
     */
    public function setFonctionJs($fonctionJs)
    {
        $this->_fonctionJs = $fonctionJs;
    }

    public function onLoad($param)
    {
        if ('' != $this->getFileSizeMax()) {
            $this->_maxSize = $this->getFileSizeMax();
        } else {
            $this->_maxSize = Atexo_Config::getParameter('MAX_UPLOAD_FILE_SIZE');
        }
        if (empty($this->getTitre())) {
            $this->setTitre(Prado::localize('CHOIX_FICHIER'));
        }
        //  $this->_url_serveur = Atexo_Config::getParameter("DEFAULT_SERVEUR_UPLOAD_FILE");
    }

    public function getTmpFileName()
    {
        return 'file_'.session_id();
    }

    public function getFilesSize()
    {
        $filesSize = [];
        if ($this->filesSize->value) {
            $filesSize = explode('|', $this->filesSize->value);
        }

        return $filesSize;
    }
}
