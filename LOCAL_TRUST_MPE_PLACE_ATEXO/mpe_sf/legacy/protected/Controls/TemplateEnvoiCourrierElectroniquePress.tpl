		<com:AtexoValidationSummary  ValidationGroup="validateinfomessage" />
		<!--Debut Bloc Message à envoyer-->
        <com:TPanel ID="panelTypeMessage" CssClass="form-field">
        	<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
            <com:TPanel CssClass="content">
                <div class="line">
                    <div class="intitule-110"><label for="<%=$this->messageType->getClientId()%>"><com:TTranslate>DEFINE_TEXT_TYPE_MESSAGE</com:TTranslate></label> :</div>
                    <com:TDropDownList
                    	ID="messageType"
                    	CssClass="moyen"
                    	Enabled="false"
                    	Attributes.title = "<%=Prado::localize('DEFINE_TEXT_TYPE_MESSAGE')%>"
                    />
                </div>
            </com:TPanel>
            <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
        </com:TPanel>
		<div class="form-field">
			<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><span class="title"><com:TTranslate>TEXT_MESSAGE</com:TTranslate></span></div>
			<div class="content">
                <!--[if lte IE 6]>
                	<div class="spacer-mini"></div>
                <![endif]-->
				<!--Debut Line Destinataire-->
               	<div class="line">
					<div class="intitule-110"><com:TTranslate>DEFINE_TEXT_DESTINATAIRE </com:TTranslate> :</div>
					<com:TTextBox id="validateDests" style="display:none;"/>
								<com:TCustomValidator
										ClientValidationFunction="validateListDestinatairePress"
										ID="validatorDsts"
										ControlToValidate="validateDests"
										ValidationGroup="validateinfomessage"
										Display="Dynamic"
										ErrorMessage="<%=Prado::localize('TEXT_MESSAGE_ERREUR_DESTINATAIRE')%>"
										EnableClientScript="true"
							 			Text="<span class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
										<prop:ClientSide.OnValidationError>
			   				 				document.getElementById('divValidationSummary').style.display='';
			   				 				document.getElementById('ctl0_CONTENU_PAGE_TemplateEnvoiCourrierElectroniquePress_buttonValid').style.display='';
			 							</prop:ClientSide.OnValidationError>
		         				</com:TCustomValidator>
					<div class="content-bloc bloc-660">
						<div class="select-list liste-destinataires-presse">
						<com:TLabel id="nbreDest" style="display:none;" />
							<com:TRepeater ID="RepeaterDestinataire">
								<prop:ItemTemplate>
								<div class="line">
								<com:TCheckBox
									id="idCheck"
									Attributes.title="<%#$this->Data->getEmail()%>"
									cssClass="check" Checked="<%#$this->TemplateControl->getChekcdDestJal($this->Data->getId())%>"
									Enabled="<%#!isset($_GET['consult'])%>"/>
								<com:TLabel id="nom" text=<%#$this->Data->getNom() %> /> - <com:TLabel id="email" text=<%#$this->Data->getEmail()%>/>
								<com:TLabel id="idJal"  text="<%#$this->Data->getId()%>" visible="false" />
								</div>
								</prop:ItemTemplate>
							</com:TRepeater>
						</div>
					</div>
				</div>
				<!--Fin Line Destinataire-->
				<!--Debut Line Adresse facturation-->
				<com:TPanel id="panelAdressesFacturation" cssClass="line" Visible="<%=Application\Service\Atexo\Atexo_Module::isEnabled('GestionAdressesFacturationJAL')%>">
					<div class="intitule-bloc intitule-110"><com:TTranslate>DEFINE_TEXT_ADRESSES_FACTURATION </com:TTranslate> :</div>
					<com:TTextBox id="validateAdresseFacturation" style="display:none;"/>
					<com:TCustomValidator
						ClientValidationFunction="validateAdresseFact"
						ID="validatorAdresseFacturation"
						ControlToValidate="validateAdresseFacturation"
						ValidationGroup="validateinfomessage"
						Display="Dynamic"
						ErrorMessage="<%=Prado::localize('TEXT_MESSAGE_ERREUR_ADRESSE_FACTURATION')%>"
						EnableClientScript="true"
			 			Text="<span class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
						<prop:ClientSide.OnValidationError>
  				 				document.getElementById('divValidationSummary').style.display='';
  				 				document.getElementById('ctl0_CONTENU_PAGE_TemplateEnvoiCourrierElectroniquePress_buttonValid').style.display='';
							</prop:ClientSide.OnValidationError>
     			   </com:TCustomValidator>
					<div class="content-bloc bloc-660">
						<div class="select-list liste-adresses-facturation">
							<com:TLabel id="nbreAdresseFact" style="display:none;" />
							<com:TRepeater ID="RepeaterAdresseFacturation">
								<prop:ItemTemplate>
									<div class="intitule-auto clear-both">
										<com:TRadioButton id="idRadioBouton" cssClass="check" Attributes.title="<%#$this->Data->getEmailAr()%>" UniqueGroupName="optionFacturation" Checked="<%#($this->Page->TemplateEnvoiCourrierElectroniquePress->getNbrAdresseFacturation() == 1)? true:false%>" Enabled="<%#!isset($_GET['consult'])%>" />
										<label>
											<%#$this->Data->getEmailAr()%>
											<br/>
											<%#$this->Data->getInformationFacturation()%>
											<br/>
											<%#Prado::localize('DEFINE_SIP')%> :  <%#$this->Data->getFacturationSip() == 1 ? Prado::localize('DEFINE_OUI') : Prado::localize('TEXT_NON')%>
											</br>
											<com:TPanel visible="<%#($this->Data->getIdBlobLogo())? true:false%>">
												<com:TCheckBox
													id="ajoutLogo"
													Attributes.title="<%=Prado::localize('TEXT_COCHER_SI_SOUHAITER_PUBLIER_LOGO_PRESSE')%>"
													cssClass="check"
													Enabled="<%#!isset($_GET['consult'])%>"
												/>
												<com:TTranslate>TEXT_COCHER_SI_SOUHAITER_PUBLIER_LOGO_PRESSE</com:TTranslate>
											</com:TPanel>
										</label>
										<com:TLabel id="idAdresseFact"  text="<%#$this->Data->getId()%>" visible="false" />
									</div>
								</prop:ItemTemplate>
							</com:TRepeater>
						</div>
					</div>
				</com:TPanel>
				<!--Fin Line Adresse facturation-->
				<!--Debut Line Objet-->
				<div class="line">
					<div class="intitule-110"><label for="<%=$this->objet->getClientId()%>"><com:TTranslate>OBJET</com:TTranslate></label> :</div>
					<com:TTextBox
							ID="objet"
							CssClass="long-630"
							Attributes.title = "<%=Prado::localize('OBJET')%>"
							Enabled="<%=!isset($_GET['consult'])%>"
						/>
						<com:TRequiredFieldValidator
							ControlToValidate="objet"
							ValidationGroup="validateinfomessage"
							Display="Dynamic"
							ID="validateurObjetMail"
							ErrorMessage="<%=Prado::localize('OBJET')%>"
							EnableClientScript="true"
				 			Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
							<prop:ClientSide.OnValidationError>
				 				document.getElementById('divValidationSummary').style.display='';
	      					</prop:ClientSide.OnValidationError>
      					</com:TRequiredFieldValidator>
				</div>
				<!--Fin Line Objet-->
				<div class="spacer-mini"></div>
				<!--Debut Line Texte-->
				<div class="line">
					<div class="intitule-110"><label for="<%=$this->textMail->getClientId()%>"><com:TTranslate>DEFINE_TEXT_TEXT</com:TTranslate></label> :</div>
						<com:TTextBox
							ID="textMail"
							CssClass="long-630 high-360"
							TextMode="MultiLine"
							Attributes.title = "<%=Prado::localize('DEFINE_TEXT_TEXT')%>"
							Enabled="<%=!isset($_GET['consult'])%>"
						/>
						<com:TRequiredFieldValidator
							ControlToValidate="textMail"
							ValidationGroup="validateinfomessage"
							Display="Dynamic"
							ID="validateurCorpsMail"
							ErrorMessage="<%=Prado::localize('DEFINE_TEXT_TEXT')%>"
							EnableClientScript="true"
				 			Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
							<prop:ClientSide.OnValidationError>
				 				document.getElementById('divValidationSummary').style.display='';
	      					</prop:ClientSide.OnValidationError>
      					</com:TRequiredFieldValidator>
				</div>
				<!--Debut Line Texte-->
				<div class="spacer-mini"></div>

				<div class="spacer-mini"></div>
				<!--Debut Line PJ-->
				<div class="line">
					<div class="intitule-110"><label for="tableau"><com:TTranslate>DEFINE_TEXT_PIECE_JOINTE</com:TTranslate></label> :</div>
                        <com:TActivePanel CssClass="content-bloc bloc-660" ID="PanelPJ">
							  <com:TRepeater ID="RepeaterPiecesJointes" EnableViewState="true"   AllowPaging="true"
						  			   AllowCustomPaging="true" DataKeyField="Id">
									<prop:ItemTemplate>
										<div class="picto-link inline">
											<com:TPanel visible="<%#((new Application\Service\Atexo\Publicite\Atexo_Publicite_AvisPub)->isAccesGestionPubMpe())? true:false%>">
												<com:THyperLink
													NavigateUrl="?page=Agent.AgentDownloadPjAnnoncePress&idPj=<%#$this->Data->getPiece()%>"
													visible="<%=(($this->Data instanceof Application\Propel\Mpe\CommonAnnoncePressPieceJointe) && !$this->Data->getFichierGenere())? true:false%>"><%#$this->Data->getNomFichier()%><span class="info-aide"> (<%# Application\Service\Atexo\Atexo_Util::arrondirSizeFile($this->Data->getTaille()/1024)%>)</span>
												</com:THyperLink>
												<com:THyperLink
													NavigateUrl="?page=Agent.AgentDownloadPjAnnoncePress&idAvis=<%#$this->TemplateControl->getAvisPub()%>&id=<%#$this->TemplateControl->getRefConsultation()%>&organisme=<%#$this->TemplateControl->getOrg()%>"
													visible="<%=(($this->Data instanceof Application\Propel\Mpe\CommonAnnoncePressPieceJointe && $this->Data->getFichierGenere()) && ($this->Page->TemplateEnvoiCourrierElectroniquePress->isStatutAvisBrouillonOuRejette() || !$this->Data->getPiece()))? true:false%>"><%#$this->Data->getNomFichier()%><span class="info-aide"> (<%# Application\Service\Atexo\Atexo_Util::arrondirSizeFile($this->Data->getTaille()/1024)%>)</span>
												</com:THyperLink>
												<com:THyperLink
													NavigateUrl="?page=Agent.AgentDownloadPjAnnoncePress&idAvis=<%#$this->TemplateControl->getAvisPub()%>&id=<%#$this->TemplateControl->getRefConsultation()%>&organisme=<%#$this->TemplateControl->getOrg()%>&fileStored"
													visible="<%=(($this->Data instanceof Application\Propel\Mpe\CommonAnnoncePressPieceJointe && $this->Data->getFichierGenere()) && (!$this->Page->TemplateEnvoiCourrierElectroniquePress->isStatutAvisBrouillonOuRejette() && $this->Data->getPiece()))? true:false%>"><%#$this->Data->getNomFichier()%><span class="info-aide"> (<%# Application\Service\Atexo\Atexo_Util::arrondirSizeFile($this->Data->getTaille()/1024)%>)</span>
												</com:THyperLink>
											</com:TPanel>
											<com:TPanel visible="<%#(!(new Application\Service\Atexo\Publicite\Atexo_Publicite_AvisPub)->isAccesGestionPubMpe())? true:false%>">
												<com:THyperLink
													NavigateUrl="?page=gestionPub.GestionPubDownloadPjAnnoncePress&idPj=<%#$this->Data->getPiece()%>"
													visible="<%=(($this->Data instanceof Application\Propel\Mpe\CommonAnnoncePressPieceJointe) && !$this->Data->getFichierGenere())? true:false%>"><%#$this->Data->getNomFichier()%><span class="info-aide"> (<%# Application\Service\Atexo\Atexo_Util::arrondirSizeFile($this->Data->getTaille()/1024)%>)</span>
												</com:THyperLink>
												<com:THyperLink
													NavigateUrl="?page=gestionPub.GestionPubDownloadPjAnnoncePress&idAvis=<%#$this->TemplateControl->getAvisPub()%>&id=<%#$this->TemplateControl->getRefConsultation()%>&organisme=<%#$this->TemplateControl->getOrg()%>"
													visible="<%=(($this->Data instanceof Application\Propel\Mpe\CommonAnnoncePressPieceJointe) && $this->Data->getFichierGenere() && !$this->Data->getPiece())? true:false%>"><%#$this->Data->getNomFichier()%><span class="info-aide"> (<%# Application\Service\Atexo\Atexo_Util::arrondirSizeFile($this->Data->getTaille()/1024)%>)</span>
												</com:THyperLink>
												<com:THyperLink
													NavigateUrl="?page=gestionPub.GestionPubDownloadPjAnnoncePress&idAvis=<%#$this->TemplateControl->getAvisPub()%>&id=<%#$this->TemplateControl->getRefConsultation()%>&organisme=<%#$this->TemplateControl->getOrg()%>&fileStored"
													visible="<%=(($this->Data instanceof Application\Propel\Mpe\CommonAnnoncePressPieceJointe) && $this->Data->getFichierGenere() && $this->Data->getPiece())? true:false%>"><%#$this->Data->getNomFichier()%><span class="info-aide"> (<%# Application\Service\Atexo\Atexo_Util::arrondirSizeFile($this->Data->getTaille()/1024)%>)</span>
												</com:THyperLink>
											</com:TPanel>
												<com:TActiveLinkButton
												    visible="<%#($this->Data->getFichierGenere())? false:true%>"
													CssClass="indent-5"
													OnCallBack="Page.TemplateEnvoiCourrierElectroniquePress.refreshRepeaterPJ"
													OnCommand="Page.TemplateEnvoiCourrierElectroniquePress.onDeleteClick"
													CommandParameter="<%#$this->Data->getId()%>"
													>
													<com:TImage
														ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-supprimer.gif"
														AlternateText="<%=Prado::localize('DEFINE_SUPPRIMER')%>"
														Attributes.title="<%=Prado::localize('DEFINE_SUPPRIMER')%>"
														Visible="<%=!isset($_GET['consult'])%>"
														/>
												</com:TActiveLinkButton>
										</div>
										<br/><br/>
									</prop:ItemTemplate>
								</com:TRepeater>
								<com:TActiveHyperLink
									ID="buttonEditPj"
									NavigateUrl="javascript:popUp('?page=Agent.popUpListePj&Idmsg=<%=$this->_annoncePress->getId()%>&org=<%=$this->_org%>&type=<%=Application\Service\Atexo\Atexo_Config :: getParameter('TYPE_PRESS')%>&id=<%=$this->Page->TemplateEnvoiCourrierElectroniquePress->getRefConsultation()%>&idAvis=<%=$this->Page->TemplateEnvoiCourrierElectroniquePress->getAvisPub()%>','yes');"
									CssClass="bouton-edit clear-both"
									Attributes.title="<%=Prado::localize('DEFINE_TEXT_EDITER_LIST_PIECE_JOINTE')%>"
									Text="<%=Prado::localize('DEFINE_TEXT_EDITER')%>"
									Visible="<%=!isset($_GET['consult'])%>"
									>
								</com:TActiveHyperLink>
				  				<com:TActiveTextBox ID="countPj" Display="None"/>
							    <span style ="display:none;">
									<com:TActiveButton
										Id="validatePJ"
										onCallBack="Page.TemplateEnvoiCourrierElectroniquePress.refreshRepeaterPJ"
										/>
								</span>
				  				<div class="breaker"></div>
					  	</com:TActivePanel>
				</div>
				<!--Fin Line PJ-->
				<div class="breaker"></div>
			</div>
			<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
		</div>
		<!--Fin Bloc Message à envoyer-->
		 <!--Debut Bloc Option d'envoi-->
		<com:TPanel ID="panelOptionEnvoi" CssClass="form-field">
			<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><span class="title"><com:TTranslate>DEFINE_TEXT_OPTION_ENVOI</com:TTranslate></span></div>
				<div class="content">
				<div class="spacer-mini"></div>
				<div class="line">
					<div class="intitule-auto bloc-700">
					<com:TRadioButton
						ID="courrierElectroniqueSimple"
						GroupName="optionEnvoie"
						CssClass = "radio"
						Attributes.title = "<%=Prado::localize('DEFINE_TEXT_COURIER_ELECTRONIQUE_SIMPLE')%>"
						Enabled="<%=!isset($_GET['consult'])%>"
					/>
					<label for="<%=$this->courrierElectroniqueSimple->getClientId()%>"><com:TTranslate>DEFINE_TEXT_COURIER_ELECTRONIQUE_SIMPLE</com:TTranslate></label><span class="info-aide indent-10"><com:TTranslate>DEFINE_TEXT_TAILLE_PIECE_JOINTE</com:TTranslate></span>
				  </div>
					<div class="spacer-mini"></div>
					<div class="intitule-auto bloc-700">
					<com:TRadioButton
						ID="courrierElectroniqueContenuIntegralAR"
						GroupName="optionEnvoie"
						CssClass = "radio"
						Attributes.title = "<%=Prado::localize('DEFINE_TEXT_COURIER_ELECTRONIQUE_CONTENU_INTEGRAL')%>"
						Enabled="false"
					/>
					<label for="<%=$this->courrierElectroniqueContenuIntegralAR->getClientId()%>"><com:TTranslate>DEFINE_TEXT_COURIER_ELECTRONIQUE_CONTENU_INTEGRAL</com:TTranslate></label> (<com:TTranslate>DEFINE_TEXT_Lien_RECEPTION</com:TTranslate>)<span class="info-aide indent-10"><com:TTranslate>DEFINE_TEXT_TAILLE_PIECE_JOINTE</com:TTranslate></span>
					</div>
                    <div class="spacer-mini"></div>
                    <div class="intitule-auto bloc-700">
                    <com:TRadioButton
						ID="courrierElectroniqueUniquementLienTelechargementObligatoire"
						GroupName="optionEnvoie"
						CssClass = "radio"
						Attributes.title = "<%=Prado::localize('DEFINE_TEXT_COURIER_ELECTRONIQUE_TELECHARGEMENT')%>"
						Enabled="false"
					/>
					<label for="<%=$this->courrierElectroniqueUniquementLienTelechargementObligatoire->getClientId()%>"><com:TTranslate>DEFINE_TEXT_COURIER_ELECTRONIQUE_TELECHARGEMENT_UNIQUE</com:TTranslate></label> <com:TTranslate>TEXT_PLATE_FORME_AR</com:TTranslate>
				  </div>
					<div class="breaker"></div>
				</div>
			</div>
			<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
		</com:TPanel>
		<!--Fin Bloc Option d'envoi-->
		<!--Debut line boutons-->
		<div class="boutons-line">
			<com:TButton
				id="annuler"
				CssClass="bouton-annulation float-left"
				Attributes.value="<%=Prado::localize('TEXT_ANNULER')%>"
				Attributes.title="<%=Prado::localize('DEFINE_TEXT_MAIL_RETOUR')%>"
				OnClick="RedirectReturn"
				/>
			<com:TButton
					ID="buttonValid"
					CssClass="bouton-validation float-right"
					Attributes.value="<%=Prado::localize('TEXT_VALIDER')%>"
					Attributes.title="<%=Prado::localize('TEXT_VALIDER')%>"
					ValidationGroup="validateinfomessage"
					onClick="valider"
					Visible="<%=!isset($_GET['consult'])%>"/>

			<com:TButton
					ID="buttonSave"
					CssClass="bouton-enregistrer float-right"
					Attributes.value="<%=Prado::localize('DEFINE_ENREGISTRER')%>"
					Attributes.title="<%=Prado::localize('DEFINE_ENREGISTRER')%>"
					onClick="enregistrer"
					Visible="<%=!isset($_GET['consult'])%>"/>
		</div>
		<!--Fin line boutons-->
		<div class="breaker"></div>

		<com:TLabel id="scriptJs" />
