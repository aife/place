<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_ValeursReferentielles;

/**
 * Page de recherche avancée.
 *
 * @author Khadija CHOUIKA <khadija.chouika@atexo.com>
 * @copyright Atexo 2012
 *
 * @version 1.2
 *
 * @since MPE-4.0
 */
class EntreprisePartenariat extends MpeTPage
{
    //public  $_calledFrom;
    public function onInit($param)
    {
        //$this->Master->setCalledFrom("entreprise");Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
    }

    public function showBlocContact()
    {
        $this->panel_rechercheCollaborateur->setStyle('display:block');
    }

    public function hideBlocContact()
    {
        $this->panel_rechercheCollaborateur->setStyle('display:none');
    }

    public function fillRepeaterTypeCollaboration()
    {
        $arrayType = (new Atexo_ValeursReferentielles())->getValeurRefByIdRef(Atexo_Config::getParameter('REFERENTIEL_TYPE_COLLABORATION'));
        foreach ($arrayType as $key => $value) {
            $dataSource[$key]['id'] = $key;
            $dataSource[$key]['libelle'] = $value;
        }
        $this->repeaterTypeCollaboration->DataSource = $dataSource;
        $this->repeaterTypeCollaboration->DataBind();
    }
}
