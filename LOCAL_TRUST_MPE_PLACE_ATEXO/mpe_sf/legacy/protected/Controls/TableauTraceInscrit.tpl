<!--Debut filtres -->
<com:TActivePanel id="panelRechercheHistique"  Cssclass="form-bloc bloc-filtres margin-5">
    <div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
    <div class="content">
        <h2><%=Prado::localize('DEFINE_FILTRES')%></h2>
        <div class="line">	
            <div class="intitule-100"><label for="filtresEntreprise"><%=Prado::localize('TEXT_ENTREPRISE')%> :</label></div>
            <com:TActiveDropDownList 
             ID="filtresEntreprise" 
             CssClass="long" 
             AutoPostBack="true"
             Attributes.title="<%=Prado::localize('TEXT_ENTREPRISE')%>"
            />
        </div>
        <div class="line">	
            <div class="intitule-100"><label for="filtresEntreprise"><%=Prado::localize('DEFINE_DATE_ACCES')%> :</label></div>
              <com:TActiveDropDownList  
             ID="filtresDate" 
             AutoPostBack="true"
             CssClass="select-160" 
             Attributes.title="<%=Prado::localize('TEXT_ENTREPRISE')%>"
              />
        </div>
        <div class="boutons-line">
            <com:TActiveButton 
             Cssclass = "bouton-moyen-120 float-right" 
             id = "searchButton" 
             Text = "<%=Prado::localize('DEFINE_LANCER_RECHERCHE')%>"
             OnCallback = "searchHistoireInscrits" 
             Attributes.title = "<%=Prado::localize('DEFINE_LANCER_RECHERCHE')%>"
            />
        </div>
        <div class="breaker"></div>
    </div>
    <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
</com:TActivePanel>
 <!--Fin filtres -->
<!--Debut Bloc historiques des traces-->
<com:TActivePanel id="panelhistoriquesInscrits"  Cssclass="form-field" style="display:none">
	<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
	<div class="content">
		<h2><%=Prado::localize("DEFINE_LISTES_ACTIONS_ENTREPRISES_JOUR")%></h2>
		<div class="table-bloc">
			<!--Debut Line partitionneur-->
			<div class="line-partitioner">
				<h2><com:TTranslate>DEFINE_NOMBRE_RESULTATS</com:TTranslate> : <com:TLabel Text="<%=$this->getNbrElementRepeater()%>"/></h2>
				<div class="partitioner">
					<div class="intitule"><strong><label for="allAnnonces_nbResultsTop"><com:TLabel id="labelAfficherTop"><com:TTranslate>DEFINE_AFFICHER</com:TTranslate></com:TLabel></label></strong></div> 
					<com:TDropDownList Attributes.title="<%=Prado::localize('NOMBRE_DE_RESULTAT_PAR_PAGE')%>" id="nbResultsTop" AutoPostBack="true" onSelectedIndexChanged="changePagerLenght" >
     					<com:TListItem Text="10" Selected="10"/>
     					<com:TListItem Text="20" Value="20"/>
               			<com:TListItem Text="50" Value="50" />
               			<com:TListItem Text="100" Value="100" />
               			<com:TListItem Text="500" Value="500" />
       				</com:TDropDownList> 
       				<div class="intitule"><com:TLabel id="resultParPageTop"><com:TTranslate>DEFINE_RESULTATS_PAGE</com:TTranslate></com:TLabel></div> 
					<label style="display:none;" for="allAnnonces_pageNumberTop"><com:TTranslate>DEFINE_ALLER_PAGE</com:TTranslate></label>
					<com:TPanel ID="panelBouttonGotoPageTop" DefaultButton="DefaultButtonTop" CssClass="float-left">
					<com:TTextBox Attributes.title="<%=Prado::localize('N_DE_LA_PAGE')%>" Text="1" id="pageNumberTop" />
					<div class="nb-total ">
						<com:TLabel ID="labelSlashTop" Text="/" visible="true"/> 
						<com:TLabel ID="nombrePageTop"/>
						<com:TButton ID="DefaultButtonTop"  OnClick="goToPage" Attributes.style="display:none"/>
					</div>
					</com:TPanel>
					<div class="liens">
						<com:TPager
       						ID="PagerTop"
           					ControlToPaginate="historiquesInscrtis"
           					FirstPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-premier.gif' alt='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>' title='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>' />"
           					PrevPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-precedent.gif' alt='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>' title='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>' />"
           					Mode="NextPrev"
           					NextPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-suivant.gif' alt='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>' title='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>' />"
           					LastPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-dernier.gif' alt='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>' title='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>' />"
           					OnPageIndexChanged="pageChanged"
           				/>
					</div>
				</div>
			</div>
			<com:TRepeater 
				ID="historiquesInscrtis" 
				EnableViewState="true"
			    AllowPaging="true"
    	        PageSize="<%= Application\Service\Atexo\Atexo_Config::getParameter('NOMBRE_AFFICHAGE_PAR_DEFAUT')%>"
    	        AllowCustomPaging="true"
			 >
				<prop:HeaderTemplate>
					<table class="table-results">
						<caption><com:TTranslate>DEFINE_LISTES_ACTIONS_ENTREPRISES_JOUR</com:TTranslate></caption>
						<thead>
							<tr>
								<th class="top" colspan="5"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></th>
							</tr>
							<tr>
								<th class="col-200" id="entreprise_raisonSociale">
									<com:TTranslate>TEXT_RAISON_SOCIAL</com:TTranslate>
									<com:ActivePictoSort 
										OnCallback="Page.tableauTraceInscrit.sortTraces" 
										ActiveControl.CallbackParameter="entreprise" 
										Attributes.title="<%=Prado::localize('DEFINE_TRIER')%>" />
								</th>
								<th class="col-200" id="entreprise_user">
									<com:TTranslate>DEFINE_NOM_PRENOM</com:TTranslate>
									<com:ActivePictoSort 
										OnCallback="Page.tableauTraceInscrit.sortTraces" 
										ActiveControl.CallbackParameter="nom" 
										Attributes.title="<%=Prado::localize('DEFINE_TRIER')%>" />
									
								</th>
								<th class="col-100" id="entreprise_ip">
									<com:TTranslate>DEFINE_ADRESSE_IP</com:TTranslate>
									<com:ActivePictoSort 
										OnCallback="Page.tableauTraceInscrit.sortTraces" 
										ActiveControl.CallbackParameter="adress_ip" 
										Attributes.title="<%=Prado::localize('DEFINE_TRIER')%>" />
								</th>
								<th class="col-70" id="entreprise_dateAcces">
									<com:TTranslate>DATE</com:TTranslate>
									<com:ActivePictoSort 
										OnCallback="Page.tableauTraceInscrit.sortTraces" 
										ActiveControl.CallbackParameter="date" 
										Attributes.title="<%=Prado::localize('DEFINE_TRIER')%>" />
								</th>
								<th class="actions" id="actions">
									<com:TTranslate>DEFINE_ACTIONS</com:TTranslate>
								</th>
							</tr>
						</thead>
				</prop:HeaderTemplate>
				<prop:ItemTemplate>
					<tr>
						<td class="col-200" headers="entreprise_raisonSociale">
							<%#$this->Data['nomEntreprise']%>
						</td>
						<td class="col-200" headers="entreprise_user">
							<%#$this->Data['nom_inscrit']%>
							<%#$this->Data['prenom_inscrit']%>
						</td>
						<td class="col-100" headers="entreprise_ip">
							<%#$this->Data['addr_ip']%>
						</td>
						<td class="col-70" headers="entreprise_dateAcces">
							<%#  Application\Service\Atexo\Atexo_Util::iso2frnDate($this->Data['date']) %>
						</td>
						<td class="actions" headers="actions">
							<a href="javascript:popUp('index.php?page=Agent.PopupHistoriqueNavigationEntreprise&id=<%=$this->Data['id']%>&consultationId=<%= Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($_GET['id'])%>&org=<%=Application\Service\Atexo\Atexo_CurrentUser::getCurrentOrganism()%>','yes');">
								<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-details.gif" alt="Voir l'historique de navigation" />
							</a>
						</td>
					</tr>
				</prop:ItemTemplate>
			 	<prop:FooterTemplate>
					</table>
			   </prop:FooterTemplate>
			</com:TRepeater>
		<!--Debut partitionneur-->
		<div class="line-partitioner">
			<div class="partitioner">
				<div class="intitule"><strong><label for="horsLigne_nbResultsBottom"><com:TLabel id="labelAfficherDown"><com:TTranslate>DEFINE_AFFICHER</com:TTranslate></com:TLabel></label></strong></div> 
					<com:TDropDownList Attributes.title="<%=Prado::localize('NOMBRE_DE_RESULTAT_PAR_PAGE')%>" id="nbResultsBottom" AutoPostBack="true" onSelectedIndexChanged="changePagerLenght" >
      						<com:TListItem Text="10" Selected="10"/>
                			<com:TListItem Text="20" Value="20"/>
                			<com:TListItem Text="50" Value="50" />
                			<com:TListItem Text="100" Value="100" />
                			<com:TListItem Text="500" Value="500" />
      				</com:TDropDownList> 
				<div class="intitule"><com:TLabel id="resultParPageDown"><com:TTranslate>DEFINE_RESULTATS_PAGE</com:TTranslate></com:TLabel></div> 
				<label style="display:none;" for="horsLigne_pageNumberBottom"><com:TTranslate>DEFINE_ALLER_PAGE</com:TTranslate></label>
				<com:TPanel ID="panelBouttonGotoPageBottom" DefaultButton="DefaultButtonBottom" CssClass="float-left">
					<com:TTextBox Attributes.title="<%=Prado::localize('N_DE_LA_PAGE')%>" Text="1" id="pageNumberBottom" />
					<div class="nb-total ">
						<com:TLabel ID="labelSlashBottom" Text="/" visible="true"/> 
						<com:TLabel ID="nombrePageBottom"/>
						<com:TButton ID="DefaultButtonBottom"  OnClick="goToPage" Attributes.style="display:none"/>
					</div>
				</com:TPanel>
				<div class="liens">
					<com:TPager
      						ID="PagerBottom"
          					ControlToPaginate="historiquesInscrtis"
          					FirstPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-premier.gif' alt='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>' title='<%=Prado::localize('ALLER_A_LA_PREMIERE_PAGE')%>' />"
          					PrevPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-precedent.gif' alt='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>' title='<%=Prado::localize('ALLER_A_LA_PAGE_PRECEDENTE')%>' />"
          					Mode="NextPrev"
          					NextPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-suivant.gif' alt='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>' title='<%=Prado::localize('ALLER_A_LA_PAGE_SUIVANTE')%>' />"
          					LastPageText="<img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/fleche-dernier.gif' alt='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>' title='<%=Prado::localize('ALLER_A_LA_DERNIERE_PAGE')%>' />"
          					OnPageIndexChanged="pageChanged"
          				/>
				</div>
			</div>
		</div>
       <!--Fin partitionneur-->
	  </div>
	</div>
	<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
</com:TActivePanel >
<a href="index.php?page=Agent.ouvertureEtAnalyse&id=<%= Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($_GET['id'])%>"
	class="bouton-retour" title="<%=Prado::localize('RETOUR_TABLEAU_SUIVI')%>">
	<com:TTranslate>TEXT_RETOUR</com:TTranslate>
</a>
<!--Fin Bloc Gestion des agents-->