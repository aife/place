<?php

namespace Application\Controls;

use Application\Propel\Mpe\CommonTCandidatureMps;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_CandidatureMPS;

/**
 * Le bloc conformite de l'entreprise.
 *
 * @author LEZ <loubna.ezziani@atexo.com>
 *
 * @version 1.0
 *
 * @since 4.7.0
 *
 * @copyright Atexo 2014
 */
class AtexoBlocConformite extends MpeTTemplateControl
{
    public bool $_modeAffichage = false;

    /**
     * Set the value of [_modeAffichage] .
     *
     * @param bool $modeAffichage new value
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function setModeAffichage($modeAffichage)
    {
        $this->_modeAffichage = $modeAffichage;
    }

    /**
     * Get the value of [_modeAffichage] column.
     *
     * @return Booleean
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function getModeAffichage()
    {
        return $this->_modeAffichage;
    }

    /**
     * Get the value of [_candidatureMPS] column.
     *
     * @return CommonTCandidatureMps
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function getCandidatureMPS()
    {
        return $this->Page->getViewState('candidatureMPS');
    }

    /**
     * Remplire la candidature MPS par les donnees de conformite.
     *
     * @param CommonTCandidatureMps $candidatureMPS la candidature MPS
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function fillCandidatureByDataConformite(&$candidatureMPS)
    {
        if ($candidatureMPS instanceof Atexo_Entreprise_CandidatureMPS) {
            if ($this->assuranceContracte->checked) {
                $candidatureMPS->setAssuranceContracte(true);
            }
            $candidatureMPS->setObligDeclaration($this->obligDeclaration->getChecked());
        }
    }

    /**
     * Permet de charger les donnees de conformite de l'entreprise.
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function chargerInfoConformite()
    {
        $candidatureMPS = $this->getCandidatureMPS();
        if ($candidatureMPS instanceof Atexo_Entreprise_CandidatureMPS) {
            Atexo_Util::setCheckImageByEtat('imgObligDeclaration', $this, $candidatureMPS->getObligDeclaration());
            Atexo_Util::setCheckImageByEtat('imgAssuranceContracte', $this, $candidatureMPS->getAssuranceContracte());
        }
    }
}
