<?php

namespace Application\Controls;

use App\Entity\InterfaceSuivi;
use App\Service\TncpService;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Pages\Commun\DownloadFile;
use Application\Propel\Mpe\CommonAgent;
use Application\Propel\Mpe\CommonComplement;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonDCE;
use Application\Propel\Mpe\CommonProcedureEquivalence;
use Application\Propel\Mpe\CommonRG;
use Application\Propel\Mpe\CommonTDonneeComplementaire;
use Application\Propel\Mpe\CommonTDumeNumero;
use Application\Propel\Mpe\CommonTDumeNumeroQuery;
use Application\Propel\Mpe\CommonTTelechargementAsynchrone;
use Application\Propel\Mpe\CommonTTelechargementAsynchroneFichier;
use Application\Propel\Mpe\CommonTTelechargementAsynchronePeer;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Agrement;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Exception;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_MultiDomaine;
use Application\Service\Atexo\Atexo_Qualifications;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Category;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Complements;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Dce;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureEquivalence;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Rg;
use Application\Service\Atexo\Dume\Atexo_Dume_AtexoDume;
use Prado\Prado;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class ConsultationAdditionalInformations extends MpeTTemplateControl
{
    private $_consultation = false;
    private $_reference = false;
    private $_withException = true;

    public function getReference()
    {
        return $this->_reference;
    }

    public function setReference($reference)
    {
        return $this->_reference = $reference;
    }

    public function getConsultation()
    {
        return $this->_consultation;
    }

    public function setConsultation($consultation)
    {
        return $this->_consultation = $consultation;
    }

    public function getWithException()
    {
        return $this->_withException;
    }

    public function setWithException($withException)
    {
        return $this->_withException = $withException;
    }

    public function onLoad($param)
    {
        $donneeComplementaire = null;
        $this->customizeForm();
        if (!($this->getConsultation() instanceof CommonConsultation) && !$this->getReference()) {
            if ($this->getWithException()) {
                throw new Atexo_Exception('ConsultationAdditionalInformations Error : Attendu id de la reference ou objet consultation');
            } else {
                $this->summaryVisible->Visible = false;

                return;
            }
        }
        $org = Atexo_CurrentUser::getCurrentOrganism();
        if (!($this->getConsultation() instanceof CommonConsultation)) {
            $reference = $this->_reference;
            $criteria = new Atexo_Consultation_CriteriaVo();
            $criteria->setcalledFromPortail(false);
            $criteria->setIdService(Atexo_CurrentUser::getCurrentServiceId());
            $criteria->setAcronymeOrganisme($org);
            $criteria->setIdReference($reference);
            $criteria->setConnectedAgentId(Atexo_CurrentUser::getId());
            $consultation = (new Atexo_Consultation())->search($criteria);
            if (1 == count($consultation) && $consultation[0] instanceof CommonConsultation) {
                $consultation = array_shift($consultation);
            }
        } else {
            $consultation = $this->getConsultation();
            $this->setReference($consultation->getId());
            $donneeComplementaire = $consultation->getDonneComplementaire();
        }

        $dateMiseEnLigne = Atexo_Util::iso2frnDateTime($consultation->getDateMiseEnLigneCalcule());
        if ($dateMiseEnLigne) {
            $this->dateMiseEnLigne->Text = $dateMiseEnLigne;
            $this->panelDateMiseEnLigne->setVisible(true);
        }
        $this->dateHeureLimiteRemise->Text = Atexo_Util::iso2frnDateTime($consultation->getDateFin());
        if ('0000-00-00 00:00:00' != $this->getConsultation()->getDatefinLocale() && '' != $this->getConsultation()->getDatefinLocale()) {
            $this->dateFinLocale->Text = Atexo_Util::iso2frnDateTime(substr($this->getConsultation()->getDatefinLocale(), 0, 16));
        }
        if ('' != $this->getConsultation()->getLieuResidence()) {
            $this->lieuResidence->Text = $this->getConsultation()->getLieuResidence();
        }
        if ('1' == $consultation->getAlloti()) {
            $lots = $consultation->getAllLots();
            if (0 != (is_countable($lots) ? count($lots) : 0)) {
                $this->allotissement->Text = Prado::localize('DEFINE_OUI');
                $this->idLienLot->Visible = 'true';
            }
            $this->lots->DataSource = $lots;
            $this->lots->dataBind();
        } else {
            $this->allotissement->Text = Prado::localize('DEFINE_NON');
            $this->idLienLot->Visible = 'false';
        }
        $organisme = Atexo_CurrentUser::getCurrentOrganism();
        $dce = (new Atexo_Consultation_Dce())->getDce($consultation->getId(), $organisme);
        if ($dce instanceof CommonDCE && $dce->getDce()) {
            $tailleDce = (new Atexo_Consultation_Dce())->getTailleDCE($dce, $organisme);
            $this->linkDownloadDce->Text = Prado::localize('DEFINE_TELECHARGER') . ' ' . '- ' . $tailleDce;
            $this->linkDownloadDce->NavigateUrl = 'index.php?page=Agent.DownloadDce&id=' . base64_encode($consultation->getId());
            $this->linkDetailPiece->NavigateUrl = 'index.php?page=Agent.DetailDce&id=' . $consultation->getId();
            $this->linkHistorique->NavigateUrl = 'index.php?page=Agent.HistoriqueConsultation&id=' . $this->getReference();
        } else {
            $this->linkDownloadDce->Text = '-';
            $this->linkDetailPiece->Text = '-';
            $this->linkHistorique->NavigateUrl = 'index.php?page=Agent.HistoriqueConsultation&id=' . $this->getReference();
        }

        if (Atexo_Config::getParameter('SPECIFIQUE_PLACE')) {
            $this->linkDownloadDossierIntegrale->NavigateUrl = 'index.php?page=Agent.DownloadArchive&id='.$consultation->getId();
        } else {
            $this->linkDownloadDossierIntegraleDIC->NavigateUrl = '?page=Agent.MesTelechargements';
        }

        $this->linkDownloadDossierArchive->NavigateUrl = 'index.php?page=Agent.DownloadArchive&id=' . $consultation->getId() . '&DAC';
        $procEquivalence = Atexo_Consultation_ProcedureEquivalence::retrieveProcedureEquivalenceById($consultation->getIdTypeProcedureOrg());
        if ($consultation && $procEquivalence instanceof CommonProcedureEquivalence) {
            if (
                Atexo_Module::isEnabled('ReglementConsultation')
                && ('+1' === $procEquivalence->getReglementCons()
                || '+0' === $procEquivalence->getReglementCons()
                || '0' === $procEquivalence->getReglementCons()
                || '1' === $procEquivalence->getReglementCons())
            ) {
                $this->panelRC->Attributes->Style = 'display:block';
            } else {
                $this->panelRC->Attributes->Style = 'display:none';
            }
        }
        $rg = (new Atexo_Consultation_Rg())->getReglement($consultation->getId(), $organisme);
        if ($rg instanceof CommonRG && $rg->getRg()) {
            $tailleRg = Atexo_Util::arrondirSizeFile((new Atexo_Blob())->getTailFile($rg->getRg(), $organisme) / 1024);
            $this->linkDownloadReglement->Text = Prado::localize('DEFINE_TELECHARGER') . ' ' . '- ' . $tailleRg;
            $this->linkDownloadReglement->NavigateUrl = 'index.php?page=Agent.DownloadReglement&id=' . base64_encode($consultation->getId());
        } else {
            $this->linkDownloadReglement->Text = '-';
        }
        //------------ Autres pieces---
           $complement = (new Atexo_Consultation_Complements())->getComplement($consultation->getId(), $organisme); //print_r($complement);exit;
        if ($complement instanceof CommonComplement && $complement->getComplement()) {
            $tailleComplement = Atexo_Util::arrondirSizeFile((new Atexo_Blob())->getTailFile($complement->getComplement(), $organisme) / 1024);
            $this->linkDownloadOtherPJ->Text = Prado::localize('DEFINE_TELECHARGER') . ' ' . '- ' . $tailleComplement;
            $this->linkDownloadOtherPJ->NavigateUrl = 'index.php?page=Agent.AgentDownloadComplement&id=' . base64_encode($consultation->getId());
        } else {
            $this->linkDownloadOtherPJ->Text = '-';
        }
        //-------------Fin Autres pieces---
        $langue = Atexo_CurrentUser::readFromSession('lang');
        $this->entiteAchatRattachement->Text = Atexo_EntityPurchase::getPathEntityById($consultation->getServiceId(), Atexo_CurrentUser::getOrganismAcronym(), $langue);
        $this->entiteAchatAssociee->Text = Atexo_EntityPurchase::getPathEntityById($consultation->getServiceAssocieId(), Atexo_CurrentUser::getOrganismAcronym(), $langue);
        $this->commantaireInterne->Text = $consultation->getChampSuppInvisible();

        if (1 == $consultation->getTypeAcces()) {
            if (('DEFINE_LIEN_FICHE_DETAIL' == Prado::Localize('DEFINE_LIEN_FICHE_DETAIL'))) {
                $url = Atexo_MultiDomaine::replaceDomain(
                    Atexo_Config::getParameter('PF_URL_MESSAGERIE'),
                    Atexo_Config::getParameter('PF_ACTIVE_MULTI_DOMAINE')
                ) .
                       '?page=Entreprise.EntrepriseDetailsConsultation&id=' . $consultation->getId() .
                       '&orgAcronyme=' . Atexo_CurrentUser::getOrganismAcronym();
            } else {
                $multiDomain = (bool) Atexo_Config::getParameter('PF_ACTIVE_MULTI_DOMAINE');
                $url = Atexo_MultiDomaine::replaceDomain(Atexo_Config::getParameter('PF_URL_REFERENCE'), $multiDomain)
                       . '?page=Entreprise.EntrepriseAdvancedSearch&AllCons&id=' . $consultation->getId() .
                       '&orgAcronyme=' . Atexo_CurrentUser::getOrganismAcronym();
            }
            $this->restreintPublique->Text = Prado::Localize('DEFINE_ACCES_PUBLIC');
            $this->urlDirecteConsultation->Text = $url;
        } else {
            //acces restreint
            $this->restreintPublique->Text = Prado::Localize('DEFINE_ACCES_RESTREINT');
            $this->restreintPublique->Text .= ' - ' . Prado::Localize('DEFINE_CODE_ACCES');
            if ($consultation->getCodeProcedure()) {
                $this->restreintPublique->Text .= ' : ' . $consultation->getCodeProcedure();
            }
        }

        if ($consultation->isDicTelechargeable()) {
            $this->panelDic->setVisible(true);
            $this->panelAutresPieces->setVisible(true);
            $this->panelRC->setVisible(true);
            $this->panelDCE->setVisible(true);
        } else {
            $this->panelDic->setVisible(false);
            $this->panelAutresPieces->setVisible(false);
            $this->panelRC->setVisible(false);
            $this->panelDCE->setVisible(false);
        }
        if ($consultation->isDacTelechargeable()) {
            $this->panelDac->setVisible(true);
        } else {
            $this->panelDac->setVisible(false);
        }

        if (Atexo_Module::isEnabled('SuiviPassation', Atexo_CurrentUser::getCurrentOrganism())) {
            $this->panelDonneeComplementaire1->setVisible(true);
            $this->panelDonneeComplementaire2->setVisible(true);
        }

        $this->domainesActivite->text = "<ul class='default-list'><li>" . Atexo_Consultation_Category::displayListeLibelleDomaineByArrayId(trim($consultation->getDomainesActivites(), '#'), '</li><li>') . '</li></ul>';
        if ($donneeComplementaire instanceof CommonTDonneeComplementaire) {
            $this->prixAcquisitionPlan->text = Atexo_Util::getMontantArronditEspace($donneeComplementaire->getPrixAquisitionPlans(), 2) . ' ' . Prado::localize('TEXT_MAD');
        }
        $this->adresseRetraitDossiers->text = $consultation->getAdresseRetraisDossiersTraduit();
        $this->adresseDepotOffres->text = $consultation->getAdresseDepotOffresTraduit();
        $this->lieuOuverturePlis->text = $consultation->getLieuOuverturePlisTraduit();

        if ('1' == $consultation->getAlloti()) {
            $this->panelConsultationNonAlotti->setDisplay('None');
        } else {
            $this->panelConsultationNonAlotti->setDisplay('Dynamic');
            if ($donneeComplementaire instanceof CommonTDonneeComplementaire) {
                $this->cautionProvisoire->text = Atexo_Util::getMontantArronditEspace($donneeComplementaire->getCautionProvisoire(), 2) . ' ' . Prado::localize('TEXT_MAD');
            }
            if ($libellesQualification = (new Atexo_Qualifications())->displayListeLibelleDomaineByArrayId($consultation->getQualification())) {
                $textQualification = '';
                $arrayLibellesQualif = explode(' , ', $libellesQualification);
                foreach ($arrayLibellesQualif as $oneLibelleQualif) {
                    if ($oneLibelleQualif) {
                        $textQualification .= '<li>' . $oneLibelleQualif . '</li>';
                    }
                }
                $this->qualification->Text = "<ul class='default-list'>" . $textQualification . '</ul>';
            } else {
                $this->qualification->Text = '-';
            }
            if ($libellesAgrements = (new Atexo_Agrement())->retrieveLibelleAgrementsByIds($consultation->getAgrements())) {
                $textAgrements = '';
                $agLibelles = explode(' , ', $libellesAgrements);
                foreach ($agLibelles as $oneAgrementLibelle) {
                    if ($oneAgrementLibelle) {
                        $textAgrements .= '<li>' . $oneAgrementLibelle . '</li>';
                    }
                }
                $this->agrements->Text = "<ul class='default-list'>" . $textAgrements . '</ul>';
            } else {
                $this->agrements->Text = '-';
            }
            if ($donneeComplementaire instanceof CommonTDonneeComplementaire) {
                if ('' != $donneeComplementaire->getDateLimiteEchantillon() && '' != $donneeComplementaire->getAddEchantillonTraduit()) {
                    $this->dateEchantillons->text = Atexo_Util::iso2frnDateTime($donneeComplementaire->getDateLimiteEchantillon());
                    $this->adresseEchantillons->text = $donneeComplementaire->getAddEchantillonTraduit();
                } else {
                    $this->labelDateHeureLimiteEchantillon->setVisible(false);
                    $this->dateEchantillons->Text = '-';
                    $this->adresseEchantillons->setVisible(false);
                }
                if ('' != $donneeComplementaire->getAddReunionTraduit() && '' != $donneeComplementaire->getDateReunion()) {
                    $this->dateReunion->text = Atexo_Util::iso2frnDateTime($donneeComplementaire->getDateReunion());
                    $this->adresseReunion->text = $donneeComplementaire->getAddReunionTraduit();
                } else {
                    $this->labelDateHeureReunion->setVisible(false);
                    $this->dateReunion->Text = '-';
                    $this->adresseReunion->setVisible(false);
                }
                if (Atexo_Module::isEnabled('ConsultationVisiteDesLieux')) {
                    $visitesLieux = (new Atexo_Consultation())->retreiveVisitesLieuxByRef($consultation->getId(), null, $org);
                    $this->repeaterVisitesLieux->dataSource = $visitesLieux;
                    $this->repeaterVisitesLieux->dataBind();
                    if (!$visitesLieux) {
                        $this->panelRepeaterVisitesLieux->setVisible(false);
                        $this->panelPasVisitelieu->setVisible(true);
                    }
                }
                $variante = '';
                if ('1' == $consultation->getVariantes()) {
                    $variante = Prado::Localize('TEXT_OUI');
                } else {
                    $variante = Prado::Localize('TEXT_NON');
                }
                $this->varianteValeur->Text = $variante;
            }
        }
        if (Atexo_Module::isEnabled('moduleTncp', Atexo_CurrentUser::getOrganismAcronym())) {
            $list = Atexo_Util::getSfService(TncpService::class)->getConsultationStatus($consultation->getId());
            $this->statusPublication->text = $this->formatStatusOutput($list, InterfaceSuivi::ACTION_PUBLICATION_CONSULTATION);
            $this->statusDlro->text = $this->formatStatusOutput($list, InterfaceSuivi::ACTION_MODIFICATION_DATE_LIMITE_REMISE_PLI);
            $this->statusDce->text = $this->formatStatusOutput($list, InterfaceSuivi::ACTION_MODIFICATION_DCE);
            $this->panelTncp->visible = true;
        }

        if (Atexo_Module::isEnabled('interfaceDume') && 1 == $consultation->getDumeDemande()) {
            $listeDumeDemande = Atexo_Dume_AtexoDume::getTDumeNumeros($consultation);
            $this->listeDumeDemande->setDataSource($listeDumeDemande);
            $this->listeDumeDemande->dataBind();
        } else {
            $this->panelDumeDemmande->setVisible(false);
        }

        $this->accessCodeDCE->Text = $consultation->getCodeDceRestreint() ?
            Prado::Localize('ACCES_RESTREINT_CODE_ACCES') . ' : ' . $consultation->getCodeDceRestreint()
            : Prado::Localize('TEXT_ACCES_PUBLIC');
    }

    /***
        * afficher les champs qui sont propre a mpe Maroc
        */
    public function customizeForm()
    {
        if (Atexo_Module::isEnabled('ConsultationDomainesActivites')) {
            $this->panelDomaineActivites->setDisplay('Dynamic');
        } else {
            $this->panelDomaineActivites->setDisplay('None');
        }
        if (Atexo_Module::isEnabled('ConsultationAdresseRetraisDossiers')) {
            $this->adresseRetraitDossier->setDisplay('Dynamic');
        } else {
            $this->adresseRetraitDossier->setDisplay('None');
        }
        if (Atexo_Module::isEnabled('ConsultationPrixAcquisition')) {
            $this->panelAdresseDepotOffre->setDisplay('Dynamic');
        } else {
            $this->panelAdresseDepotOffre->setDisplay('None');
        }
        if (Atexo_Module::isEnabled('ConsultationLieuOuverturePlis')) {
            $this->panelLieuOuverturePlis->setDisplay('Dynamic');
        } else {
            $this->panelLieuOuverturePlis->setDisplay('None');
        }
        if (Atexo_Module::isEnabled('ConsultationPrixAcquisition') && Atexo_Module::isEnabled('DonneesComplementaires')) {
            $this->prixAcquisitionPlans->setDisplay('Dynamic');
        } else {
            $this->prixAcquisitionPlans->setDisplay('None');
        }
        if (Atexo_Module::isEnabled('ConsultationCautionProvisoire') && Atexo_Module::isEnabled('DonneesComplementaires')) {
            $this->panelCautionProvisoire->setDisplay('Dynamic');
        } else {
            $this->panelCautionProvisoire->setDisplay('None');
        }
        if (Atexo_Module::isEnabled('ConsultationQualification')) {
            $this->panelQualification->setDisplay('Dynamic');
        } else {
            $this->panelQualification->setDisplay('None');
        }
        if (Atexo_Module::isEnabled('ConsultationAgrement')) {
            $this->panelAgrements->setDisplay('Dynamic');
        } else {
            $this->panelAgrements->setDisplay('None');
        }
        if (Atexo_Module::isEnabled('ConsultationEchantillonsDemandes')) {
            $this->panelEchantillons->setDisplay('Dynamic');
        } else {
            $this->panelEchantillons->setDisplay('None');
        }
        if (Atexo_Module::isEnabled('ConsultationReunion')) {
            $this->panelReunion->setDisplay('Dynamic');
        } else {
            $this->panelReunion->setDisplay('None');
        }
        if (Atexo_Module::isEnabled('ConsultationVariantesAutorisees') && Atexo_Module::isEnabled('DonneesComplementaires')) {
            $this->panelVariante->setDisplay('Dynamic');
        } else {
            $this->panelVariante->setDisplay('None');
        }
        if (Atexo_Module::isEnabled('ConsultationVisiteDesLieux')) {
            $this->panelVisitesLieux->setDisplay('Dynamic');
        } else {
            $this->panelVisitesLieux->setDisplay('None');
        }
    }

    public function getLange()
    {
        return Atexo_CurrentUser::readFromSession('lang');
    }

    public function downloadDumeDemande($sender, $param)
    {
        $cnx = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB') . Atexo_Config::getParameter('CONST_READ_WRITE'));
        $currentItem = $sender->NamingContainer;
        $idPdf = $currentItem->idPdf->getValue();
        $idDumeNumero = $currentItem->idDumeNumero->getValue();
        $tDumeNumero = CommonTDumeNumeroQuery::create()->findOneById($idDumeNumero, $cnx);
        if ($tDumeNumero instanceof CommonTDumeNumero && $idPdf && $tDumeNumero->getBlobId() == $idPdf) {
            $organisme = $this->getConsultation()->getOrganisme();
            (new DownloadFile())->downloadFiles($idPdf, $tDumeNumero->getNumeroDumeNational() . '.pdf', $organisme);
            exit;
        }
    }

    private function formatStatusOutput(array $listStatus, string $action): string
    {
        return isset($listStatus['date'][$action])
            ? Prado::Localize($listStatus['status'][$action]) . ' (' . Prado::Localize('DEFINE_LAST_SUCCESSES')
            . ': ' . $listStatus['date'][$action] . ')'
            : Prado::Localize($listStatus['status'][$action]);
    }


    /**
     * Méthode qui permet d'initialiser le téléchargement asynchrone d'une archive de consultation.
     *
     * @param $referenceConsultation référence de la consultation
     * @param $fileLog pointeur vers le fichier de log
     *
     * @return bool true si tout se passe bien, false sinon
     */
    public function preparerTelechargementAsynchrone()
    {
        $fileLog = Atexo_LoggerManager::getLogger('archivage');
        try {
            $referenceConsultation = $_GET['id'];
            $connexion = Propel::getConnection(
                Atexo_Config::getParameter('COMMON_DB')
                . Atexo_Config::getParameter('CONST_READ_WRITE'));
            $listeTelechargementAsynchroneFichiers = new PropelCollection();
            $telechargementAsynchroneFichiers = new CommonTTelechargementAsynchroneFichier();
            $telechargementAsynchroneFichiers->setIdReferenceTelechargement($referenceConsultation);
            $listeTelechargementAsynchroneFichiers[] = $telechargementAsynchroneFichiers;

            if (count($listeTelechargementAsynchroneFichiers) && $listeTelechargementAsynchroneFichiers[0]) {
                $agent = (new Atexo_Agent())->retrieveAgent(Atexo_CurrentUser::getId());
                if ($agent instanceof CommonAgent) {
                    $telechargementAsynchrone = new CommonTTelechargementAsynchrone();
                    $telechargementAsynchrone->setIdAgent($agent->getId());
                    $telechargementAsynchrone->setEmailAgent($agent->getEmail());
                    $telechargementAsynchrone->setNomPrenomAgent($agent->getNom().' '.$agent->getPrenom());
                    $telechargementAsynchrone->setIdServiceAgent($agent->getServiceId());
                    $telechargementAsynchrone->setOrganismeAgent($agent->getOrganisme());
                    $telechargementAsynchrone->setTypeTelechargement(Atexo_Config::getParameter('TYPE_TELECHARGEMENT_ASYNCHRONE_ARCHIVE'));
                    $telechargementAsynchrone->setCommonTTelechargementAsynchroneFichiers($listeTelechargementAsynchroneFichiers, $connexion);
                    $telechargementAsynchrone->save($connexion);
                    $fileLog->info("Preparation du telechargement d'archive asynchrone pour la consultation  ".$referenceConsultation.' OK');
                }

                return true;
            }

            return false;
        } catch (\Exception $e) {
          $fileLog->error("Erreur lors de la preparation du telechargement d'archive asynchrone pour la consultation  ".$referenceConsultation.' : '.$e->getMessage());

            return false;
        }
    }

}
