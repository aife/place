<?php

namespace Application\Controls;

use App\Service\EchangeDocumentaire\EchangeDocumentaireService;
use Application\Library\Propel\Collection\PropelCollection;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonAgent;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonConsultationPeer;
use Application\Propel\Mpe\CommonTTelechargementAsynchrone;
use Application\Propel\Mpe\CommonTTelechargementAsynchroneFichier;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Contrat;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Responses;
use Exception;
use Prado\Prado;

class ArchiveResultSearch extends MpeTPage
{
    private bool $_typeConsultation = true;

    public function getTypeConsultation()
    {
        return $this->_typeConsultation;
    }

    public function setTypeConsultation($typeConsultation)
    {
        $this->_typeConsultation = $typeConsultation;
    }

    public function fillRepeaterWithDataForArchiveSearchResult($criteriaVo)
    {
        $this->scriptJs->setText('');
        $consultation = new Atexo_Consultation();
        $nombreElement = $consultation->search($criteriaVo, true);
        if ($nombreElement >= 1) {
            $this->panelMoreThanOneElementFound->setVisible(true);
            $this->panelNoElementFound->setVisible(false);
            $this->nombreElement->Text = $nombreElement;
            $this->PagerBottom->setVisible(true);
            $this->PagerTop->setVisible(true);
            $this->setViewState('nombreElement', $nombreElement);
            $this->setViewState('CriteriaVo', $criteriaVo);
            $this->nombrePageTop->Text = ceil($nombreElement / $this->ArchiveTableauBordRepeater->PageSize);
            $this->nombrePageBottom->Text = ceil($nombreElement / $this->ArchiveTableauBordRepeater->PageSize);
            $this->ArchiveTableauBordRepeater->setVirtualItemCount($nombreElement);
            $this->ArchiveTableauBordRepeater->setCurrentPageIndex(0);
            $this->populateData($criteriaVo);
        } else {
            $this->panelMoreThanOneElementFound->setVisible(false);
            $this->panelNoElementFound->setVisible(true);
            $this->PagerBottom->setVisible(false);
            $this->PagerTop->setVisible(false);
        }
    }

    public function populateData(Atexo_Consultation_CriteriaVo $criteriaVo)
    {
        $offset = $this->ArchiveTableauBordRepeater->CurrentPageIndex * $this->ArchiveTableauBordRepeater->PageSize;
        $limit = $this->ArchiveTableauBordRepeater->PageSize;
        if ($offset + $limit > $this->ArchiveTableauBordRepeater->getVirtualItemCount()) {
            $limit = $this->ArchiveTableauBordRepeater->getVirtualItemCount() - $offset;
        }
        $consultation = new Atexo_Consultation();
        $criteriaVo->setOffset($offset);
        $criteriaVo->setLimit($limit);
        $arrayConsultation = $consultation->search($criteriaVo);
        $this->ArchiveTableauBordRepeater->DataSource = $arrayConsultation;
        $this->ArchiveTableauBordRepeater->DataBind();
    }

    public function Trier($champsOrderBy)
    {
        $this->scriptJs->setText('');
        $this->setViewState('sortByElement', $champsOrderBy);
        $criteriaVo = $this->getViewState('CriteriaVo');
        $criteriaVo->setSortByElement($champsOrderBy);
        $arraySensTri = $this->Page->getViewState('sensTriArray', []);
        $arraySensTri[$champsOrderBy] = ('ASC' == $criteriaVo->getSensOrderBy()) ? 'DESC' : 'ASC';
        $criteriaVo->setSensOrderBy($arraySensTri[$champsOrderBy]);
        $this->Page->setViewState('sensTriArray', $arraySensTri);
        $this->Page->setViewState('CriteriaVo', $criteriaVo);
        $this->ArchiveTableauBordRepeater->setCurrentPageIndex(0);
        $this->populateData($criteriaVo);
    }

    public function pageChanged($sender, $param)
    {
        $this->scriptJs->setText('');
        $this->ArchiveTableauBordRepeater->CurrentPageIndex = $param->NewPageIndex;
        $this->numPageBottom->Text = $param->NewPageIndex + 1;
        $this->numPageTop->Text = $param->NewPageIndex + 1;
        $criteriaVo = $this->getViewState('CriteriaVo');
        $this->populateData($criteriaVo);
    }

    public function elementSortedWith($panelName, $openCloseTag)
    {
        if ($this->getViewState('sortByElement') == $panelName) {
            if ('open' == $openCloseTag) {
                return '<strong>';
            } else {
                return '</strong>';
            }
        } else {
            return '';
        }
    }

    public function goToPage($sender, $param)
    {
        $numPage = null;
        $this->scriptJs->setText('');
        switch ($sender->ID) {
            case 'DefaultButtonTop':
                $numPage = $this->numPageTop->Text;
                break;
            case 'DefaultButtonBottom':
                $numPage = $this->numPageBottom->Text;
                break;
        }
        if (Atexo_Util::isEntier($numPage)) {
            if ($numPage >= $this->nombrePageTop->Text) {
                $numPage = $this->nombrePageTop->Text;
            } elseif ($numPage <= 0) {
                $numPage = 1;
            }
            $this->ArchiveTableauBordRepeater->CurrentPageIndex = $numPage - 1;
            $this->numPageBottom->Text = $numPage;
            $this->numPageTop->Text = $numPage;
            $criteriaVo = $this->getViewState('CriteriaVo');
            $this->populateData($criteriaVo);
        } else {
            $this->numPageTop->Text = $this->ArchiveTableauBordRepeater->CurrentPageIndex + 1;
            $this->numPageBottom->Text = $this->ArchiveTableauBordRepeater->CurrentPageIndex + 1;
        }
    }

    public function changePagerLenght($sender, $param)
    {
        $this->scriptJs->setText('');
        switch ($sender->ID) {
            case 'nombreResultatAfficherBottom':
                $pageSize = $this->nombreResultatAfficherBottom->getSelectedValue();
                                                  $this->nombreResultatAfficherTop->setSelectedValue($pageSize);
                break;
            case 'nombreResultatAfficherTop':
                $pageSize = $this->nombreResultatAfficherTop->getSelectedValue();
                                               $this->nombreResultatAfficherBottom->setSelectedValue($pageSize);
                break;
        }

        $this->ArchiveTableauBordRepeater->PageSize = $pageSize;
        $nombreElement = $this->getViewState('nombreElement');
        $this->nombrePageTop->Text = ceil($nombreElement / $this->ArchiveTableauBordRepeater->PageSize);
        $this->nombrePageBottom->Text = ceil($nombreElement / $this->ArchiveTableauBordRepeater->PageSize);
        $criteriaVo = $this->getViewState('CriteriaVo');
        $this->ArchiveTableauBordRepeater->setCurrentPageIndex(0);
        $this->populateData($criteriaVo);
    }

    public function getTrueOrFalse($bool)
    {
        return $bool;
    }

    public function infoBulleEtapeConnsultation($statusConsultation)
    {
        if ($statusConsultation == Atexo_Config::getParameter('STATUS_DECISION')) {
            return Prado::localize('ETAPE_DECISION');
        }
        if ($statusConsultation == Atexo_Config::getParameter('STATUS_A_ARCHIVER')) {
            return Prado::localize('ETAPE_A_ARCHIVER');
        }
        if ($statusConsultation == Atexo_Config::getParameter('STATUS_ARCHIVE_REALISEE')) {
            return Prado::localize('ETAPE_ARCHIVE_REALISEE');
        }
    }

    /** Pour inserer un retour a la ligne (<br>), tous les 22 caracteres, dans une chaine.
     * @param : $texte : texte dans lequel sera ajouter les <br>
     *
     * @return : retourn la chaine apres y avoir inserer un retour a la ligne tous les $everyHowManyCaracter caracteres
     */
    public function insertBrWhenTextTooLong($texte, $everyHowManyCaracter)
    {
        return wordwrap($texte, $everyHowManyCaracter, '<br>', true);
    }

    public function truncate($texte)
    {
        $maximumNumberOfCaracterToReturn = 65;
        $arrayText = explode(' ', $texte);
        $indexArrayText = 0;
        $textlenght = strlen($arrayText[$indexArrayText]) + 1;
        $truncatedText = $arrayText[$indexArrayText];
        while (isset($arrayText[$indexArrayText]) && $textlenght <= $maximumNumberOfCaracterToReturn) {
            ++$indexArrayText;
            if ($textlenght + (strlen($arrayText[$indexArrayText]) + 1) > $maximumNumberOfCaracterToReturn) {
                break;
            }
            $textlenght += strlen($arrayText[$indexArrayText]) + 1;
            $truncatedText .= ' '.$arrayText[$indexArrayText];
        }

        return $truncatedText;
    }

    public function isTextTruncated($texte, $nbrCaractere = 100)
    {
        return (strlen($texte) > 60) ? '' : 'display:none';
    }

    public function isTypeConsultation()
    {
        if ('consultation' == $this->getTypeConsultation()) {
            return true;
        } else {
            return false;
        }
    }

    public function checkAll($sender, $param)
    {
        foreach ($this->ArchiveTableauBordRepeater->Items as $itemConsultation) {
            $itemConsultation->toUpdate->checked = $sender->checked;
        }
        //      $this->panelRefresh->render($param->getNewWriter());
    }

    public function refreshRepeater($sender, $param)
    {
        $criteriaVo = $this->getViewState('CriteriaVo');
        $this->ArchiveTableauBordRepeater->setCurrentPageIndex(0);
        $this->fillRepeaterWithDataForArchiveSearchResult($criteriaVo);
        $this->populateData($criteriaVo);
    }

    public function changeUniqueStatut($sender, $param)
    {
        $org = Atexo_CurrentUser::getCurrentOrganism();
        $ref = $sender->getParent()->getParent()->idArchive->value;
        (new Atexo_Consultation())->updateStatutConsultation($ref, $org, Atexo_Config::getParameter('STATUS_A_ARCHIVER'));
        $this->refreshRepeater($sender, $param);

        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $consultation = CommonConsultationPeer::retrieveByPK($ref, $connexionCom);

        if (Atexo_Module::isEnabled('EchangesDocuments', $consultation->getOrganisme())) {
            /** @var EchangeDocumentaireService $echangeDocumentaireService */
            $echangeDocumentaireService = Atexo_Util::getSfService(EchangeDocumentaireService::class);
            $echangeDocumentaireService->createAutomatedEchanges($ref);
        }
    }

    /**
     * Permet de changer de statut (A Archiver) pour les consultations en statut Decision.
     *
     * @param $sender
     * @param $param
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function changeStatut($sender, $param)
    {
        if ('Valider' == $param->getCommandName()) {
            $valide = Atexo_Config::getParameter('STATUS_A_ARCHIVER');
        } else {
            $valide = Atexo_Config::getParameter('STATUS_DECISION');
        }
        $org = Atexo_CurrentUser::getCurrentOrganism();
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));

        foreach ($this->ArchiveTableauBordRepeater->Items as $itemConsultation) {
            if ($itemConsultation->toUpdate->checked) {
                $consultation = CommonConsultationPeer::retrieveByPK($itemConsultation->idArchive->Value, $connexion);
                if ($consultation->getIdEtatConsultation() < Atexo_Config::getParameter('STATUS_A_ARCHIVER')) {
                    $consultation->setIdEtatConsultation($valide);
                    $consultation->setIdArchiveur(Atexo_CurrentUser::getIdAgentConnected());
                    $consultation->save($connexion);
                }
            }
        }
        $this->refreshRepeater($sender, $param);
    }

    public function getIsAjoutDocumentsExterneVisible($idEtatConsultation)
    {
        if ($idEtatConsultation > 5) {
            return false;
        } else {
            return true;
        }
    }

    public function getIsAjoutMetadonneesVisible($idEtatConsultation)
    {
        if ($idEtatConsultation > 5) {
            return false;
        } else {
            return true;
        }
    }

    public function getIsVoirArborescenceVisible($idEtatConsultation)
    {
        if ($idEtatConsultation > 5) {
            return false;
        } else {
            return true;
        }
    }

    public function getIsArchiverButtonVisible($idEtatConsultation)
    {
        if ($idEtatConsultation >= 5) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Permet de gerer la visibilite de la case a cocher qui se trouve a gauche dans les resultats de la recherche.
     *
     * @param string $idEtatConsultation: l'identifiant qui designe l'etat de la consultation
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function getIsVisibleCheckBox($idEtatConsultation)
    {
        if ($idEtatConsultation > Atexo_Config::getParameter('STATUS_A_ARCHIVER')) {
            return false;
        } else {
            return true;
        }
    }

    public function isTypeAnnonce()
    {
        return true; //($this->getTypeConsultation()=="Annonce");
    }

    public function changeStatutDesarchivage($sender, $param)
    {
        $org = Atexo_CurrentUser::getCurrentOrganism();
        $ref = $sender->getParent()->getParent()->idArchive->value;
        (new Atexo_Consultation())->updateStatutConsultation($ref, $org, Atexo_Config::getParameter('STATUS_DECISION'));
        $this->response->redirect('?page=Agent.TableauDeBord&id='.$sender->getParent()->getParent()->idArchive->value);
    }

    public function getMsgAlerteActionAArchiver($consultation)
    {
        $msg = '';
        if ($consultation->serviceConsHasServiceArchivageDefinitive()) {
            $msg .= addslashes(Prado::localize('ATTENTION_SERVICE_ARCHIVES_DEFINITIVES_NON_RENSIEGNE')).'\\n';
        }
        $msg .= addslashes(Prado::localize('TEXT_AVERTISSEMENT_STATUT_A_ARCHIVER_1')).'\\n'.addslashes(Prado::localize('TEXT_AVERTISSEMENT_STATUT_A_ARCHIVER_2'));

        return $msg;
    }

    public function redirectToRegistreRetrait($sender, $param)
    {
        $reference = $param->CommandName;
        $this->response->redirect('index.php?page=Agent.GestionRegistres&id='.$reference.'&type=1');
    }

    public function redirectToRegistreQuestion($sender, $param)
    {
        $reference = $param->CommandName;
        $this->response->redirect('index.php?page=Agent.GestionRegistres&id='.$reference.'&type=3');
    }

    public function redirectToRegistreDepot($sender, $param)
    {
        $reference = $param->CommandName;
        $this->response->redirect('index.php?page=Agent.GestionRegistres&id='.$reference.'&type=5');
    }

    /**
     * permet de recuperer l'url de telechargement du DIC selon le parametrage.
     *
     * @param string $refCons la reference de la Consultation
     *
     * @return string url de telechargement du DIC selon le parametrage
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function getUrlDownloadDic($commonConsultation)
    {
        if (Atexo_Module::isEnabled('ArchiveParLot') && $commonConsultation->getAlloti()) {
            return "javascript:popUp('index.php?page=Agent.PopUpChoixArchive&id=".base64_encode($commonConsultation->getId())."');";
        } else {
            return 'index.php?page=Agent.DownloadArchive&id='.$commonConsultation->getId();
        }
    }

    /**
     * permet de recuperer l'url de telechargement du DAC selon le parametrage.
     *
     * @param string $refCons la reference de la Consultation
     *
     * @return string url de telechargement du DAC selon le parametrage
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function getUrlDownloadDac($commonConsultation)
    {
        if (Atexo_Module::isEnabled('ArchiveParLot') && $commonConsultation->getAlloti()) {
            return "javascript:popUp('index.php?page=Agent.PopUpChoixArchive&id=".base64_encode($commonConsultation->getId())."&DAC=1');";
        } else {
            return 'index.php?page=Agent.DownloadArchive&id='.$commonConsultation->getId().'&DAC';
        }
    }

    /**
     * Permet de sauvegarder en base les consultations selectionnées pour le téléchargement groupé et asynchrone.
     *
     * @param $sender
     * @param $param
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function preparerTelechargementAsynchrone($sender, $param)
    {
        try {
            $listeTelechargementAsynchroneFichiers = new PropelCollection();
            $organisme = Atexo_CurrentUser::getCurrentOrganism();
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            foreach ($this->ArchiveTableauBordRepeater->Items as $item) {
                if ($item->toUpdate->checked && $item->statutConsultation->Value == Atexo_Config::getParameter('STATUS_A_ARCHIVER')) {
                    $telechargementAsynchroneFichiers = new CommonTTelechargementAsynchroneFichier();
                    $telechargementAsynchroneFichiers->setIdReferenceTelechargement($item->idArchive->Value);
                    $listeTelechargementAsynchroneFichiers[] = $telechargementAsynchroneFichiers;
                }
            }

            if (count($listeTelechargementAsynchroneFichiers) && $listeTelechargementAsynchroneFichiers[0]) {
                $agent = (new Atexo_Agent())->retrieveAgent(Atexo_CurrentUser::getIdAgentConnected());
                if ($agent instanceof CommonAgent) {
                    $telechargementAsynchrone = new CommonTTelechargementAsynchrone();
                    $telechargementAsynchrone->setIdAgent(Atexo_CurrentUser::getIdAgentConnected());
                    $telechargementAsynchrone->setEmailAgent($agent->getEmail());
                    $telechargementAsynchrone->setNomPrenomAgent($agent->getNom().' '.$agent->getPrenom());
                    $telechargementAsynchrone->setIdServiceAgent(Atexo_CurrentUser::getCurrentServiceId());
                    $telechargementAsynchrone->setOrganismeAgent($organisme);
                    $telechargementAsynchrone->setTypeTelechargement(Atexo_Config::getParameter('TYPE_TELECHARGEMENT_ASYNCHRONE_ARCHIVE'));
                    $telechargementAsynchrone->setCommonTTelechargementAsynchroneFichiers($listeTelechargementAsynchroneFichiers, $connexion);
                    $telechargementAsynchrone->save($connexion);
                }
                $script = "<script>openModal('modal-confirmation-envoi','modal-form popup-small2',document.getElementById('modal_confirmation_envoi_telechargement'));</script>";
                $this->scriptJsStatutsArchives->Text = $script;
            }

            return;
        } catch (Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('app');
            $logger->error("Erreur lors de la preparation des telechargements d'archives asynchrone : ".$e->getMessage());
        }
    }

    public function genererContrat($sender, $param)
    {
        $this->scriptJs->setText('');
        $visibleBtnValiderEtAnnuler = false;
        $visibleBtnFermer = false;
        $message = '';
        $data = explode('#', $param->CallbackParameter);
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $consultation = CommonConsultationPeer::retrieveByPK($data[0], $connexionCom);
        if ($consultation instanceof CommonConsultation) {
            $contratClass = new Atexo_Consultation_Contrat();
            $hasContrat = $contratClass->consultationHasContrat($consultation);
            if ($hasContrat) {
                $message = Prado::localize('CONTRAT_DEJA_GENERE');
                $visibleBtnFermer = true;
            } else {
                if (!$consultation->isAllotie()) {
                    $decisions = [(new Atexo_Consultation_Responses())->getDecisionConsultationNonAlloti($consultation) => ['0']];
                } else {
                    $decisions = (new Atexo_Consultation_Responses())->getDecisionConsultationAlloti($consultation);
                }
                if (isset($decisions[Atexo_Config::getParameter('DECISION_ATTRIBUTION_MARCHE')])) {
                    $traitement = 'cas1';
                } elseif (isset($decisions[Atexo_Config::getParameter('DECISION_ATTRIBUTION_ACCORD_CADRE')]) || isset($decisions[Atexo_Config::getParameter('DECISION_A_RENSEIGNER')])) {
                    $traitement = 'cas2';
                } elseif (isset($decisions[0])) {
                    $traitement = 'cas3';
                } else {
                    $traitement = 'default';
                }
                switch ($traitement) {
                    case 'cas1':
                        $infoGeneration = $contratClass->generateContrat($consultation);
                        if ($infoGeneration['nbreDecisionEnveloppe']) {
                            if ($consultation->isAllotie()) {
                                $message = str_replace('{_num_lots}', implode(', ', $infoGeneration['lots']), Prado::localize('CONTRAT_GENERE_POUR_LOTS'));
                            } else {
                                $message = str_replace('{_attributaire_}', implode(', ', $infoGeneration['attributaires']), Prado::localize('CONTRAT_GENERE_POUR_ATTRIBUTAIRE'));
                            }
                            $visibleBtnFermer = true;
                        } else {
                            $message = Prado::localize('TYPE_DECISION_PERMET_PAS_LA_GENERATION_CONTRAT_POSSIBILITE_DESARCHIVAGE');
                            $visibleBtnValiderEtAnnuler = true;
                        }
                        break;
                    case 'cas2':
                        $message = Prado::localize('TYPE_DECISION_PERMET_PAS_LA_GENERATION_CONTRAT_POSSIBILITE_DESARCHIVAGE');
                        $visibleBtnValiderEtAnnuler = true;
                        break;
                    case 'cas3':
                        $message = Prado::localize('ABSENCE_DECISION_PERMET_PAS_LA_GENERATION_CONTRAT_POSSIBILITE_DESARCHIVAGE');
                        $visibleBtnValiderEtAnnuler = true;
                        break;
                    default:
                        $message = Prado::localize('TYPE_DECISION_PERMET_PAS_LA_GENERATION_CONTRAT');
                        $visibleBtnFermer = true;
                }
                unset($decisions[Atexo_Config::getParameter('DECISION_ATTRIBUTION_MARCHE')]);
                if ('cas1' == $traitement && $consultation->isAllotie() && is_array($decisions) && count($decisions)) {
                    $lots = [];
                    foreach ($decisions as $decision) {
                        $lots = array_merge($lots, $decision);
                    }
                    $message .= '<br/>'.str_replace('{_num_lots}', implode(', ', $lots), Prado::localize('CONTRAT_NE_PEUT_PAS_ETRE_GENERE_POUR_LOTS'));
                }
            }
        }
        $this->btnFermerGenerationContrat->setVisible($visibleBtnFermer);
        $this->btnAnnulerGenerationContrat->setVisible($visibleBtnValiderEtAnnuler);
        $this->btnValiderGenerationContrat->setVisible($visibleBtnValiderEtAnnuler);
        $this->messageGenertionContrat->setText($message);
        $this->infoCons->setValue($param->CallbackParameter);
        $this->panelGenerationContrat->render($param->getNewWriter());
    }

    public function doDesarchivage($sender, $param)
    {
        $data = explode('#', $this->infoCons->getValue());
        (new Atexo_Consultation())->updateStatutConsultation($data[0], $data[1], Atexo_Config::getParameter('STATUS_DECISION'));
        $this->response->redirect('?page=Agent.TableauDeBord&id='.$data[0]);
    }

    public function isDesarchivagePossible($consultation)
    {
        return $consultation instanceof CommonConsultation
                    &&
                        Atexo_Module::isEnabled('DesarchivageConsultation')
                    &&
                        (
                            $consultation->getIdEtatConsultation() == Atexo_Config::getParameter('STATUS_A_ARCHIVER')
                            ||
                            $consultation->getIdEtatConsultation() == Atexo_Config::getParameter('STATUS_ARCHIVE_REALISEE')
                        )
                    &&
                        Atexo_Util::diffMois(date('d/m/Y'), substr($consultation->getDateArchivage(), 0, 10)) < Atexo_Config::getParameter('DELAI_DESARCHIVAGE_CONSULTATION')
        ;
    }

    /**
     * @param $data
     * @return bool
     */
    public function afficherStatusEchangeChorus($data)
    {
        $statutCons = [
            Atexo_Config::getParameter('STATUS_DECISION'),
            Atexo_Config::getParameter('STATUS_A_ARCHIVER'),
            Atexo_Config::getParameter('STATUS_ARCHIVE_REALISEE'),
        ];
        $affiche = false;
        if ($data instanceof CommonConsultation
            &&
            in_array($data->getStatusConsultation(), $statutCons)
            &&
            (new Atexo_Module())->isChorusAccessActivate(Atexo_CurrentUser::getCurrentServiceId())
        ) {
            if (!$this->isEnabledModule("GestionContratDansExec") || $this->getParameter('ACTIVATION_CHORUS_POUR_ARCHIVES')) {
                $affiche = true;
            }
        }

        return $affiche;
    }
}
