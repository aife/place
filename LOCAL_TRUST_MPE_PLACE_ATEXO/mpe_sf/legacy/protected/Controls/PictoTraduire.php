<?php

namespace Application\Controls;

use Application\Service\Atexo\Controller\Atexo_Controller_Front;
use Prado\Prado;
use Prado\Web\UI\WebControls\TImageButton;

/**
 * commentaires.
 *
 * @author anas zaki <anas.zaki@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PictoTraduire extends TImageButton
{
    public string $_clickable = 'false';
    public string $_activate = 'false';

    public function onLoad($param)
    {
        if ('false' == $this->_activate) {
            $this->setImageUrl(Atexo_Controller_Front::t().'/images/picto-traduire-inactive.gif');
        } else {
            $this->setImageUrl(Atexo_Controller_Front::t().'/images/picto-traduire.gif');
        }
        if ('false' == $this->getClickable()) {
            //le "return false" empeche le contrôle de faire un callback, cela peut être considéré comme un extends TImage (pas de rafraichissement)
            $this->Attributes->onclick = 'return false';
        }
        $this->setAlternateText(Prado::localize('DEFINE_TRADUIRE'));
        $this->setToolTip(Prado::localize('DEFINE_TRADUIRE'));
    }

    public function getClickable()
    {
        return $this->_clickable;
    }

    public function setClickable($value)
    {
        $this->_clickable = $value;
    }

    public function getActivate()
    {
        return $this->_activate;
    }

    public function setActivate($bool)
    {
        $this->_activate = $bool;
    }
}
