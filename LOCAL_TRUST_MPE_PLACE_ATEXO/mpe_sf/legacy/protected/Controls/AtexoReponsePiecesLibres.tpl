<com:TActivePanel id="panelListePiecesLibres">
<com:TRepeater ID="listePiecesLibres" 
	EnableViewState="true"
	DataKeyField="Identifiant" >
	
	<prop:HeaderTemplate>
	<table summary="Liste des pièces - Dossier d'offre" class="table-results tableau-reponse">
    	<tbody>
	</prop:HeaderTemplate>
    <prop:ItemTemplate>
		<tr class="<%#$this->TemplateControl->getClassLigne($this->ItemIndex%2==0)%> <%#$this->SourceTemplateControl->getIdHtlmToggleFormulaire()%>">
			<td class="check-col">
				<com:TActiveCheckBox 
					id="idCheckBoxSelectionPiece" 
					Attributes.title="<%#Prado::localize('DEFINE_SELECTIONNER_PIECE')%>"
	 				CssClass="check"
	 				visible="<%#($this->SourceTemplateControl->getVisibiliteCocherSignature($this->Data) && ($this->Page->getConsultation() && $this->Page->getConsultation()->getSignatureOffre() == 1))? true:false%>"
 				/> 				
			<td class="col-piece ellipsis">
				<div class="blue piece-type">
					<img src="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-type-libre.png" title="<%#Prado::localize('PIECE_LIBRE')%>" alt="" />
					<span class="intitule-piece"><com:TTranslate>PIECE_LIBRE</com:TTranslate> :</span>
					<abbr 
						title="<%#Prado::localize('DEFINE_CHEMIN_COMPLET')%> : <%# $this->toHttpEncoding($this->Data->getCheminFichier())%>"
					>
						<%# $this->toHttpEncoding($this->SourceTemplateControl->getNomFichier($this->Data->getCheminFichier()))%>
					</abbr>
				</div>
			</td>
			<td colspan="2" class="col-180 ellipsis" >
				<com:TPanel cssClass="statut-signature" visible='<%#($this->Page->getConsultation() && $this->Page->getConsultation()->getSignatureOffre() == 1)? true:false%>'>
					<div class="statut">
						<img 
							src="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/<%#$this->SourceTemplateControl->getPictoSignatureExiste($this->Data)%>"
							alt="<%#$this->SourceTemplateControl->getAltSignatureExiste($this->Data)%>"
							title="<%#$this->SourceTemplateControl->getAltSignatureExiste($this->Data)%>"
						/> :
						<img 
							src="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/<%#$this->SourceTemplateControl->getPictoVerifSignature($this->Data)%>"
							alt="<%#$this->SourceTemplateControl->getAltVerifSignature($this->Data)%>"
							title="<%#$this->SourceTemplateControl->getAltVerifSignature($this->Data)%>"
							style="<%#$this->SourceTemplateControl->getHasFileSignature($this->Data) ? '' : 'display:none'%>"
						/>
						<com:TLabel style="<%# $this->SourceTemplateControl->getHasFileSignature($this->Data) ? 'display:none' : ''%>" > &nbsp;&nbsp;&ndash;&nbsp;&nbsp;&nbsp; </com:TLabel>
					</div>
					<div class="detail-statut" style="display:<%#($this->SourceTemplateControl->afficherLienSignature($this->Data))?'none':''%>"><%#$this->SourceTemplateControl->getInfosSignataire($this->Data)%></div>
					<com:TActiveLinkButton 
						style="display:<%#($this->SourceTemplateControl->afficherLienSignature($this->Data))?'':'none'%>"
						Attributes.class="detail-link" 
						Attributes.title="<%#Prado::localize('DEFINE_VOIR_DETAILS_SIGNATURE')%>"
						onCallBack="SourceTemplateControl.afficherDetailsSignature"
						ActiveControl.CallbackParameter="<%#$this->ItemIndex%>"
					>
						<%#$this->SourceTemplateControl->getInfosSignataire($this->Data)%>
					</com:TActiveLinkButton>
				</com:TPanel>
			</td>
			<td class="actions">
				<com:TActiveLinkButton 
				     Attributes.title="<%#Prado::localize('DEFINE_RETIRER_PIECE')%>"
				     onCallBack="SourceTemplateControl.SupprimerPieceLibre"
				     ActiveControl.CallbackParameter="<%#$this->Data->getIdentifiant()%>"
				    >
				     <img alt="<%#Prado::localize('DEFINE_RETIRER_PIECE')%>" src="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-supprimer-big.gif" />
    			</com:TActiveLinkButton>
			</td>
		</tr>
	</prop:ItemTemplate>
	<prop:FooterTemplate>
		
			</tbody>
		</table>
	</prop:FooterTemplate>
</com:TRepeater>
</com:TActivePanel>
<table summary="Liste des pièces - Dossier d'offre" class="table-results tableau-reponse">
<tbody>
<tr class="<%=$this->SourceTemplateControl->getIdHtlmToggleFormulaire()%>">
        	<td class="check-col"></td>
        	<td class="col-piece" colspan="4">
        		<com:TActiveLinkButton  
        		id="ajouterPieceLibreApplet"
        		cssClass="ajout-el"
        		attributes.onClick="initialiserAppletSelectionFichierReponse('<%=$this->getClientId()%>');return false;"
        		>
        		<com:TTranslate>DEFINE_AJOUTER_UNE_PLUSIEURS_PIECES</com:TTranslate>
        	</com:TActiveLinkButton>
        	<div style="display:">
        		<com:TActiveLinkButton  
        			id="ajouterPiecesSelectionnees"
        			onCallBack="SourceTemplateControl.ajouterPieceLibre"
        		>
        			<prop:ClientSide.OnLoading>showLoader();</prop:ClientSide.OnLoading>
        			<prop:ClientSide.OnFailure>hideLoader();</prop:ClientSide.OnFailure>
        			<prop:ClientSide.OnSuccess>hideLoader();</prop:ClientSide.OnSuccess>
        		</com:TActiveLinkButton>
        		<com:TTextBox id="cheminFichier" style="display:none"/>
        		<com:TActiveHiddenField id="listePiecesSelectionnees" Value="0"/>
        		<com:TActiveHiddenField id="listeJsonRetoursAppletPiecesSelectionnees"/>
        	</div>
        	</td>
        	
        </tr>
    </tbody>
</table>
<com:THiddenField id="logAppletAjoutFichier" />
<com:TActiveButton 
		ID="boutonlogAppletAjoutFichier"   
		Attributes.style="display:none" 
		onCallBack="traceHistoriqueNavigationInscrit"
/>