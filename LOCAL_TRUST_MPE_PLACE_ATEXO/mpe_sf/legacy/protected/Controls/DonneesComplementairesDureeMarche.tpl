<com:TActivePanel id="donneeDureeMarche" cssClass="toggle-panel form-toggle donnees-redac">
<div class="content">
	<div class="cover"></div>
	<a class="title-toggle-open" onclick="togglePanel(this,'<%=$this->panelDuree->getClientId()%>');" href="javascript:void(0);"><com:TTranslate>DEFINE_DUREE_MARCHE</com:TTranslate></a>
	<com:TPanel cssClass="panel no-indent panel-donnees-complementaires" style="display:block;" id="panelDuree">
		<!--Debut Ligne Dureee/delai-->
		<div class="line">
			<div class="intitule col-300">
				<label for="dureeDelai">
					<com:TTranslate>DEFINE_DUREE_FERME</com:TTranslate>
				</label>
				<span style="display:<%=$this->enabledElement()?'':'none'%>" class="champ-oblig">*</span> :
			</div>
			<div class="col-260 float-left">
				<com:TDropDownList
					id="dureeDelai"
					CssClass="col-220 float-left"
					Attributes.title="<%=Prado::localize('DEFINE_DUREE_MARCHE_OU_DELAI_EXECUTION')%>"
					Attributes.onchange="displayOptionChoice(this);"
					Enabled="<%=$this->enabledElement()%>"
				/>
				<div class="col-info-bulle">
					<img title="Info-bulle" alt="Info-bulle" class="picto-info-intitule" onmouseout="cacheBulleWithJquery(this)" onmouseover="afficheBulleWithJquery(this)" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" />
					<div id="infosDelai" class="info-bulle" onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();"><div><com:TTranslate>TEXT_INFO_BULLE_DUREE_MARCHE</com:TTranslate></div></div>
				</div>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<com:TCustomValidator
						ControlToValidate="dureeDelai"
						ValidationGroup="validateInfosConsultation"
						ClientValidationFunction="validationDureeMarche"
						Display="Dynamic"
						Enabled="<%=(($this->getSourcePage() == 'etapeIdentification') && Application\Service\Atexo\Atexo_Module::isEnabled('DonneesComplementaires') && Application\Service\Atexo\Atexo_Module::isEnabled('DonneesRedac'))%>"
						ErrorMessage="<%=Prado::localize('DEFINE_DUREE_MARCHE_OU_DELAI_EXECUTION')%>"
						Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
					>
						<prop:ClientSide.OnValidationError>
							document.getElementById('divValidationSummary').style.display='';
			                document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
			                document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
			                document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
			                document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
						</prop:ClientSide.OnValidationError>
				</com:TCustomValidator>
				<com:TCustomValidator
						ControlToValidate="dureeDelai"
						ValidationGroup="validateInfosConsultation"
						ClientValidationFunction="validationDureeMarche"
						Display="Dynamic"
						Enabled="<%=(($this->getSourcePage() == 'detailLots') && Application\Service\Atexo\Atexo_Module::isEnabled('DonneesComplementaires') && Application\Service\Atexo\Atexo_Module::isEnabled('DonneesRedac') && $this->getConsultation()->getDonneePubliciteObligatoire())%>"
						ErrorMessage="<%=Prado::localize('DEFINE_DUREE_MARCHE_OU_DELAI_EXECUTION')%>"
						Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
				>
					<prop:ClientSide.OnValidationError>
						document.getElementById('divValidationSummary').style.display='';
						document.getElementById('ctl0_CONTENU_PAGE_buttonSave').style.display='';
					</prop:ClientSide.OnValidationError>
				</com:TCustomValidator>
			</div>
			<div style="display: none;" id="divSelectionner"></div>
			<div id="DUREE_MARCHEE<%=$this->getIdPanel()%>" style="display:none;" >
				<com:TActiveDropDownList
						id="dureeJourMois"
						CssClass="col-80 float-left"
						Attributes.title="<%=Prado::localize('DEFINE_EN_JOUR_OU_MOIS')%>"
						Attributes.onchange="displayOptionChoice(this);"
						Enabled="<%=$this->enabledElement()%>"
				/>
				<div class="">
					<com:TTextBox
						ID="duree"
						Attributes.title="<%=Prado::localize('DEFINE_EN_MOIS')%>"
						cssClass="col-30 float-left align-right"
						Enabled="<%=$this->enabledElement()%>"
					/>
					<com:TCustomValidator
						ControlToValidate="duree"
						ValidationGroup="validateInfosConsultation"
						ClientValidationFunction="validationJourMois"
						Display="Dynamic"
						Enabled="<%=(($this->getSourcePage() == 'etapeIdentification') && Application\Service\Atexo\Atexo_Module::isEnabled('DonneesComplementaires') && Application\Service\Atexo\Atexo_Module::isEnabled('DonneesRedac'))%>"
						ErrorMessage="<%=Prado::localize('DEFINE_DUREE_MARCHE')%>"
						Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
						EnableClientScript="true"
					>
						<prop:ClientSide.OnValidationError>
							document.getElementById('divValidationSummary').style.display='';
			                document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
			                document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
			                document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
			                document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
						</prop:ClientSide.OnValidationError>
					</com:TCustomValidator>
					<com:TCustomValidator
							ControlToValidate="duree"
							ValidationGroup="validateInfosConsultation"
							ClientValidationFunction="validationJourMois"
							Display="Dynamic"
							Enabled="<%=(($this->getSourcePage() == 'detailLots') && Application\Service\Atexo\Atexo_Module::isEnabled('DonneesComplementaires') && Application\Service\Atexo\Atexo_Module::isEnabled('DonneesRedac'))%>"
							ErrorMessage="<%=Prado::localize('DEFINE_DUREE_MARCHE')%>"
							Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
							EnableClientScript="true">
						<prop:ClientSide.OnValidationError>
							document.getElementById('divValidationSummary').style.display='';
							document.getElementById('ctl0_CONTENU_PAGE_buttonSave').style.display='';
						</prop:ClientSide.OnValidationError>
					</com:TCustomValidator>
					<div id="dureeMois" style="display:block">
						<com:TLabel id="labelMois" text="<%=Prado::localize('FCSP_MOIS')%>" />
					</div>
					<div id="dureeJours" style="display:none">
						<com:TLabel id="labelJours" text="<%=Prado::localize('DEFINE_JOURS')%>" />
					</div>
				</div>
			</div>
			<div id="DELAI_EXECUTION<%=$this->getIdPanel()%>" style="display:none;">
				<span class="intitule-auto"><com:TTranslate>DEFINE_DEBUT</com:TTranslate>:</span>
				<div class="calendar">
					<com:TActiveTextBox
						Text=""
						ID="dateNotif"
						CssClass="date"
						Attributes.title="<%=Prado::localize('DATE_NOTIFICATION')%>"
						Enabled="<%=$this->enabledElement()%>"
					/>


					<com:TCustomValidator
						ControlToValidate="dateNotif"
						ValidationGroup="validateInfosConsultation"
						ClientValidationFunction="validationDateNotif"
						Display="Dynamic"
						Enabled="<%=(($this->getSourcePage() == 'etapeIdentification') && Application\Service\Atexo\Atexo_Module::isEnabled('DonneesComplementaires') && Application\Service\Atexo\Atexo_Module::isEnabled('DonneesRedac'))%>"
						ErrorMessage="<%=Prado::localize('DEFINE_DELAI_EXECUTION_PRESTATION').' - '.Prado::localize('DATE_DEBUT')%>"
						Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
						EnableClientScript="true"
					>
							<prop:ClientSide.OnValidationError>
								document.getElementById('divValidationSummary').style.display='';
				                document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
				                document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
				                document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
				                document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
							</prop:ClientSide.OnValidationError>
					</com:TCustomValidator>

					<com:TCustomValidator
							ControlToValidate="dateNotif"
							ValidationGroup="validateInfosConsultation"
							ClientValidationFunction="validationDateNotif"
							Display="Dynamic"
							Enabled="<%=(($this->getSourcePage() == 'detailLots') && Application\Service\Atexo\Atexo_Module::isEnabled('DonneesComplementaires') && Application\Service\Atexo\Atexo_Module::isEnabled('DonneesRedac'))%>"
							ErrorMessage="<%=Prado::localize('DEFINE_DELAI_EXECUTION_PRESTATION').' - '.Prado::localize('DATE_DEBUT')%>"
							Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
							EnableClientScript="true"
					>
						<prop:ClientSide.OnValidationError>
							document.getElementById('divValidationSummary').style.display='';
							document.getElementById('ctl0_CONTENU_PAGE_buttonSave').style.display='';
						</prop:ClientSide.OnValidationError>
					</com:TCustomValidator>
					<com:TImage
						ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-calendar.gif"
						Attributes.alt="Calendrier"
						Attributes.title="Calendrier"
						Attributes.onclick="popUpCalendar(this,document.getElementById('<%=$this->dateNotif->getClientId()%>'),'dd/mm/yyyy','');"
					/>

				</div>
				<span class="intitule-auto indent-5"><com:TTranslate>DEFINE_FIN</com:TTranslate> :</span>
				<div class="calendar">
					<com:TActiveTextBox
						Text=""
						ID="dateNotifJusquau"
						Attributes.title="<%=Prado::localize('DATE_NOTIFICATION')%>"
						Enabled="<%=$this->enabledElement()%>"
					/>

					<com:TCustomValidator
						ControlToValidate="dateNotifJusquau"
						ValidationGroup="validateInfosConsultation"
						ClientValidationFunction="validationDateNotifJusquau"
						Display="Dynamic"
						Enabled="<%=(($this->getSourcePage() == 'etapeIdentification') && Application\Service\Atexo\Atexo_Module::isEnabled('DonneesComplementaires') && Application\Service\Atexo\Atexo_Module::isEnabled('DonneesRedac'))%>"
						ErrorMessage="<%=Prado::localize('DEFINE_DELAI_EXECUTION_PRESTATION').' - '.Prado::localize('DATE_FIN')%>"
						Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
						EnableClientScript="true"
					>
							<prop:ClientSide.OnValidationError>
								document.getElementById('divValidationSummary').style.display='';
				                document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
				                document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
				                document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
				                document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
							</prop:ClientSide.OnValidationError>
					</com:TCustomValidator>

					<com:TCustomValidator
							ControlToValidate="dateNotifJusquau"
							ValidationGroup="validateInfosConsultation"
							ClientValidationFunction="validationDateNotifJusquau"
							Display="Dynamic"
							Enabled="<%=(($this->getSourcePage() == 'detailLots') && Application\Service\Atexo\Atexo_Module::isEnabled('DonneesComplementaires') && Application\Service\Atexo\Atexo_Module::isEnabled('DonneesRedac'))%>"
							ErrorMessage="<%=Prado::localize('DEFINE_DELAI_EXECUTION_PRESTATION').' - '.Prado::localize('DATE_FIN')%>"
							Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
							EnableClientScript="true">
						<prop:ClientSide.OnValidationError>
							document.getElementById('divValidationSummary').style.display='';
							document.getElementById('ctl0_CONTENU_PAGE_buttonSave').style.display='';
						</prop:ClientSide.OnValidationError>
					</com:TCustomValidator>
					<com:TImage
						ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-calendar.gif"
						Attributes.alt="Calendrier"
						Attributes.title="Calendrier"
						Attributes.onclick="popUpCalendar(this,document.getElementById('<%=$this->dateNotifJusquau->getClientId()%>'),'dd/mm/yyyy','');"
					/>

				</div>
			</div>
			<div id="DESCRIPTION_LIBRE<%=$this->getIdPanel()%>" style="display:none">
				<com:TTextBox
					id="delaiLibre"
					TextMode="MultiLine"
					Attributes.MaxLength="<%= Application\Service\Atexo\Atexo_Config::getParameter('NOMBRE_CARACTERES_MAX_AUTORISES_VARIANTES_PREST_SUP_EVENTUELLES')%>"
					CssClass="delai-libre"
					Attributes.OnChange="replaceCaracSpecial(this);"
					Attributes.title="<%=Prado::localize('DEFINE_LIBRE_DUREE_DELAI')%>"
					Enabled="<%=$this->enabledElement()%>"
				/>
				<com:TCustomValidator
						ControlToValidate="delaiLibre"
						ValidationGroup="validateInfosConsultation"
						ClientValidationFunction="validationDelaiLibre"
						Display="Dynamic"
						Enabled="<%=(($this->getSourcePage() == 'etapeIdentification') && Application\Service\Atexo\Atexo_Module::isEnabled('DonneesComplementaires') && Application\Service\Atexo\Atexo_Module::isEnabled('DonneesRedac'))%>"
						ErrorMessage="<%=Prado::localize('DEFINE_LIBRE_DUREE_DELAI')%>"
						Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
						EnableClientScript="true"
					>
							<prop:ClientSide.OnValidationError>
								document.getElementById('divValidationSummary').style.display='';
				                document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
				                document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
				                document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
				                document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
							</prop:ClientSide.OnValidationError>
				</com:TCustomValidator>
				<com:TCustomValidator
						ControlToValidate="delaiLibre"
						ValidationGroup="validateInfosConsultation"
						ClientValidationFunction="validationDelaiLibre"
						Display="Dynamic"
						Enabled="<%=(($this->getSourcePage() == 'detailLots') && Application\Service\Atexo\Atexo_Module::isEnabled('DonneesComplementaires') && Application\Service\Atexo\Atexo_Module::isEnabled('DonneesRedac'))%>"
						ErrorMessage="<%=Prado::localize('DEFINE_LIBRE_DUREE_DELAI')%>"
						Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
						EnableClientScript="true">
					<prop:ClientSide.OnValidationError>
						document.getElementById('divValidationSummary').style.display='';
						document.getElementById('ctl0_CONTENU_PAGE_buttonSave').style.display='';
					</prop:ClientSide.OnValidationError>
				</com:TCustomValidator>
			</div>
		</div>
		<!--Fin Ligne Dureee/delai-->
		<div class="spacer-small"></div>
		<com:TActivePanel id="panelMarcheReconductible" style="display:<%=($this->getAfficherMarcheReconductible())? '':'none'%>">
			<!--Debut Ligne Marche reconductible-->
			<div class="line">
				<div class="intitule col-300">
					<com:TActiveCheckBox
							id="marcheReconductible"
							Attributes.title="<%=Prado::localize('DEFINE_MARCHE_RECONDUCTIBLE')%>"
							Attributes.onclick="isCheckedShowDiv(this,'<%=$this->infos_reconductible->getClientId()%>');"
							CssClass="check"
							Enabled="<%=$this->enabledElement()%>"
						/>
						<label for="marcheReconductible"><com:TTranslate>DEFINE_MARCHE_RECONDUCTIBLE</com:TTranslate></label>
				</div>
				<com:TPanel cssClass="content-bloc bloc-530" id="infos_reconductible" style="display:none;">
					<span class="intitule-auto bloc-300">Nombre de reconductions :</span><com:TTextBox id="nbReconduction"
																								CssClass="col-30 align-right"
																								Attributes.title="<%=Prado::localize('FCSP_NOMBRE_RECONDUCTIONS')%>"
																								Enabled="<%=$this->enabledElement()%>"
																						/>
					<div class="breaker"></div>
					<span class="intitule-auto bloc-300"><com:TTranslate>DEFINE_MODALITE_RECONDUCTIONS</com:TTranslate><span class="champ-oblig">*</span> :</span>
					<com:TActiveTextBox
						id="modalitesReconduction"
						TextMode="MultiLine"
						Attributes.cols="50"
						Attributes.rows="2"
						CssClass="mod-reconduction"
						Attributes.title="<%=Prado::localize('DEFINE_MODALITE_RECONDUCTIONS')%>"
						Enabled="<%=$this->enabledElement()%>"
					/>
					<div class="col-info-bulle">
						<img title="Info-bulle" alt="Info-bulle" class="picto-info-intitule" onmouseout="cacheBulle('infosReconduction')" onmouseover="afficheBulle('infosReconduction', this)" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" />
						<div id="infosReconduction" class="info-bulle" onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();"><div><com:TTranslate>TEXT_INFO_BULLE_MARCHE_RECONDUTIBLE</com:TTranslate></div></div>
					</div>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					<com:TCustomValidator
							ControlToValidate="modalitesReconduction"
							ValidationGroup="validateInfosConsultation"
							ClientValidationFunction="validationModalitesReconduction"
							Display="Dynamic"
							Enabled="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('DonneesComplementaires') && Application\Service\Atexo\Atexo_Module::isEnabled('DonneesRedac'))%>"
							ErrorMessage="<%=Prado::localize('DEFINE_MODALITE_RECONDUCTIONS')%>"
							Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
							EnableClientScript="true">
						<prop:ClientSide.OnValidationError>
							if(document.getElementById('divValidationSummary'))document.getElementById('divValidationSummary').style.display='';
							if(document.getElementById('ctl0_CONTENU_PAGE_saveBack'))document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
							if(document.getElementById('ctl0_CONTENU_PAGE_buttonValidate'))document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
							if(document.getElementById('ctl0_CONTENU_PAGE_save'))document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
							if(document.getElementById('ctl0_CONTENU_PAGE_blocBoutons'))document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
						</prop:ClientSide.OnValidationError>
					</com:TCustomValidator>
				</com:TPanel>
			</div>
			<!--Fin Ligne Marche reconductible-->
		</com:TActivePanel>

		 <com:TLabel ID="scriptLabel" />

	</com:TPanel>
	<div class="breaker"></div>
</div>
</com:TActivePanel>
