<?php

namespace Application\Controls;

use Prado\Prado;
use Prado\Web\UI\WebControls\TImageButton;

/**
 * commentaires.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PictoSave extends TImageButton
{
    public function onLoad($param)
    {
        $this->setCssClass('bouton-validation float-right');
        $this->setAlternateText(Prado::localize('DEFINE_ENREGISTRER'));
        $this->setToolTip(Prado::localize('DEFINE_ENREGISTRER'));
    }
}
