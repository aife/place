<?php

namespace Application\Controls;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonTContactContrat;
use Application\Propel\Mpe\CommonTContactContratPeer;
use Application\Propel\Mpe\CommonTContratTitulaireQuery;
use Application\Propel\Mpe\CommonTEtablissement;
use Application\Propel\Mpe\CommonTEtablissementQuery;
use Application\Propel\Mpe\Entreprise;
use Application\Service\Atexo\Atexo_Config;
use Exception;
use Prado\Prado;
use Prado\Util\TLogger;

/**
 * Created by PhpStorm.
 * User: atexo
 * Date: 23/09/2015
 * Time: 12:28.
 */
class AtexoSoumissionnaire extends MpeTTemplateControl
{
    protected $soumissionnaire;
    /*
     * 0 => popup declaer contrat
     * 1 => mpe responsive popup detail entreprise
     */
    protected int $affichage = 0;

    /**
     * @return mixed
     */
    public function getSoumissionnaire()
    {
        return $this->soumissionnaire;
    }

    /**
     * @param mixed $soumissionnaire
     */
    public function setSoumissionnaire($soumissionnaire)
    {
        $this->soumissionnaire = $soumissionnaire;
    }

    /**
     * @return int
     */
    public function getAffichage()
    {
        return $this->affichage;
    }

    /**
     * @param int $affichage
     */
    public function setAffichage($affichage)
    {
        $this->affichage = $affichage;
    }

    public function LoadSoumissionaire()

    {    if ($this->soumissionnaire instanceof CommonTContactContrat) {
            $this->setViewState('idContact', $this->soumissionnaire->getIdContactContrat());
            $this->contact->Text = $this->soumissionnaire->getPrenom().' '.$this->soumissionnaire->getNom();
            if ($this->soumissionnaire->getEmail()) {
                $this->email->Text = $this->soumissionnaire->getEmail();
            } else {
                $this->imgEmail->visible = false;
            }
            if ($this->soumissionnaire->getTelephone()) {
                $this->telephone->Text = $this->soumissionnaire->getTelephone();
            } else {
                $this->imgTelephone->visible = false;
            }
            if ($this->soumissionnaire->getFax()) {
                $this->faxe->Text = $this->soumissionnaire->getFax();
            } else {
                $this->imgFax->visible = false;
            }

            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
            $etablissement = CommonTEtablissementQuery::create()->findOneByIdEtablissement($this->soumissionnaire->getIdEtablissement(), $connexion);
            if ($etablissement instanceof CommonTEtablissement) {
                $this->adresse->Text = $etablissement->getAdresse().' '.$etablissement->getAdresse2().' '.$etablissement->getCodePostal().' '.$etablissement->getVille();
                $entreprise = $etablissement->getEntreprise($connexion);
                if ($entreprise instanceof Entreprise) {
                    $this->siret->Text = $entreprise->getSiren().' '.$etablissement->getCodeEtablissement();
                    $this->setViewState('idEntreprise', $entreprise->getId());
                    $this->setViewState('idEtablissement', $etablissement->getIdEtablissement());
                }

                $this->estSiege->visible = $etablissement->getEstSiege() ? true : false;
            }
        } else {
            $this->setVisible(false);
        }
    }

    public function saveContactPrincipal()
    {
        try {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));

            $contact = new CommonTContactContrat();

            $contratTitulaireQuery = new CommonTContratTitulaireQuery();

            $contratTitulaire = $contratTitulaireQuery->findOneByIdContactContrat($this->getViewState('idContact'));

            if (!empty($this->getViewState('idEtablissement')) && !empty($this->getViewState('idEntreprise'))) {
                $contact->setIdEtablissement($this->getViewState('idEtablissement'));
                $contact->setIdEntreprise($this->getViewState('idEntreprise'));
                $contact->setNom($this->nom->getText());
                $contact->setPrenom($this->prenom->getText());
                $contact->setEmail($this->mail->getText());
                $contact->setTelephone($this->tel->getText());
                $contact->setFax($this->fax->getText());
                $contact->setCreatedAt(date('Y-m-d H:i:s'));
                $contact->setUpdatedAt(date('Y-m-d H:i:s'));

                $contact->save($connexion);

                $contratTitulaire->setIdContactContrat($contact->getIdContactContrat());

                $contratTitulaire->save($connexion);

                $this->contact->Text = $contact->getPrenom().' '.$contact->getNom();
                $this->email->Text = $contact->getEmail();
                $this->telephone->Text = $contact->getTelephone();

                if (!empty($contact->getFax())) {
                    $this->faxe->Text = $contact->getFax();
                    $this->imgFax->visible = true;
                } else {
                    $this->faxe->Text = '';
                    $this->imgFax->visible = false;
                }

                $this->setViewState('idContact', $contact->getIdContactContrat());
                $this->setViewState('idEntreprise', $contact->getIdEntreprise());
                $this->setViewState('idEtablissement', $contact->getIdEtablissement());
            }

        } catch (Exception $e) {
            Prado::log('ERREUR enregistrement du contact '.$e->getMessage().' '.$e->getTraceAsString(), TLogger::ERROR, 'PopupDecisionContrat');
        }
    }


    public function LoadDataSoumissionaireForUpdate()
    {
        $this->soumissionnaire = CommonTContactContratPeer::retrieveByPK($this->getViewState('idContact'));
        if ($this->soumissionnaire instanceof CommonTContactContrat) {
            $this->prenom->Text = $this->soumissionnaire->getPrenom();
            $this->nom->Text = $this->soumissionnaire->getNom();
            $this->mail->Text = $this->soumissionnaire->getEmail();
            $this->tel->Text = $this->soumissionnaire->getTelephone();
            $this->fax->Text = $this->soumissionnaire->getFax();
        }
    }
}
