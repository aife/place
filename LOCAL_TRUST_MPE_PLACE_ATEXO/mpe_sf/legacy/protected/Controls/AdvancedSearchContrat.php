<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Category;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Contrat;
use Application\Service\Atexo\Contrat\Atexo_Contrat_CriteriaVo;
use Prado\Prado;

/**
 * Formulaire de recherche avancee contrat.
 *
 * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
 *
 * @version 1.0
 *
 * @since 2015-roadmap
 *
 * @copyright Atexo 2015
 */
class AdvancedSearchContrat extends MpeTPage
{
    /**
     * Permet d'initialiser le composant.
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function initialiserComposant()
    {
        if (Atexo_Module::isEnabled('ConsultationClause')) {
            $this->panelClauseConsultation->setVisible(true);
            self::displayClauses();
        } else {
            $this->panelClauseConsultation->setVisible(false);
        }
        self::displayTypeContrat();
        self::chargerCategories();
    }

    /**
     * Permet de remplir les listes des clauses.
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function displayClauses()
    {
        $arrayClausesSociales = [Atexo_Config::getParameter('CLAUSES_SOCIALES_ACTIVER') => Prado::localize('DEFINE_OUI'), Atexo_Config::getParameter('CLAUSES_SOCIALES_DESACTIVER') => Prado::localize('DEFINE_NON')];
        $arrayClausesSociales = Atexo_Util::arrayUnshift($arrayClausesSociales, Prado::localize('TEXT_INDIFFERENT'));
        $this->clauseSociales->DataSource = $arrayClausesSociales;
        $this->clauseSociales->DataBind();

        $arrayClausesEnv = [Atexo_Config::getParameter('CLAUSES_ENVIRONNEMENTALES_ACTIVER') => Prado::localize('DEFINE_OUI'), Atexo_Config::getParameter('CLAUSES_ENVIRONNEMENTALES_DESACTIVER') => Prado::localize('DEFINE_NON')];
        $arrayClausesEnv = Atexo_Util::arrayUnshift($arrayClausesEnv, Prado::localize('TEXT_INDIFFERENT'));
        $this->clausesEnvironnementales->DataSource = $arrayClausesEnv;
        $this->clausesEnvironnementales->DataBind();
    }

    /**
     * Permet de remplir la liste de type de contrat.
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function displayTypeContrat()
    {
        $listeContrats = (new Atexo_Consultation_Contrat())->getListeTypeContrat();
        $this->contratType->Datasource = $listeContrats;
        $this->contratType->dataBind();
    }

    /**
     * Permet d'initialiser les champs des.
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2015
     */
    public function cleareCritaire()
    {
        self::initialiserComposant();
        $this->reference->Text = '';
        $this->keywordSearch->Text = '';
        $this->floue->checked = true;
    }

    /**
     * Permet de faire la recherche.
     *
     * @param void
     *
     * @return void
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2016
     */
    public function onSearchClick()
    {
        $criteriaVo = self::getCriteresForSearch();
        self::startSearch($criteriaVo);
    }

    /**
     * Permet de retourner le creteria de recherche.
     *
     * @param void
     *
     * @return Atexo_Contrat_CriteriaVo le criteria dor search
     *
     * @author loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2016
     */
    public function getCriteresForSearch()
    {
        $criteriaVo = new Atexo_Contrat_CriteriaVo();
        $criteriaVo->setOrganisme(Atexo_CurrentUser::getOrganismAcronym());
        $criteriaVo->setServiceId(Atexo_CurrentUser::getCurrentServiceId());
        $criteriaVo->setIdAgentConnecte(Atexo_CurrentUser::getIdAgentConnected());
        if ($this->contratType->getSelectedValue()) {
            $criteriaVo->setTypes([$this->contratType->getSelectedValue()]);
        }
        if ($this->reference->getSafeText()) {
            $criteriaVo->setReferenceLibre($this->reference->getSafeText());
        }
        if ($this->categorie->getSelectedValue()) {
            $criteriaVo->setCategorie($this->categorie->getSelectedValue());
        }
        if (Atexo_Module::isEnabled('ConsultationClause')) {
            if ($this->clauseSociales->getSelectedValue()) {
                $criteriaVo->setClauseSociales($this->clauseSociales->getSelectedValue());
            }
            if ($this->clausesEnvironnementales->getSelectedValue()) {
                $criteriaVo->setClausesEnvironnementales($this->clausesEnvironnementales->getSelectedValue());
            }
        }
        if ($this->dateNotificationStart->getSafeText()) {
            $criteriaVo->setDateNotificationStart($this->dateNotificationStart->getSafeText());
        }
        if ($this->dateNotificationEnd->getSafeText()) {
            $criteriaVo->setDateNotificationEnd($this->dateNotificationEnd->getSafeText());
        }
        if ($this->datePrevNotificationStart->getSafeText()) {
            $criteriaVo->setDatePrevNotificationStart($this->datePrevNotificationStart->getSafeText());
        }
        if ($this->datePrevNotificationStartEnd->getSafeText()) {
            $criteriaVo->setDatePrevNotificationEnd($this->datePrevNotificationStartEnd->getSafeText());
        }
        if ($this->dateFinMarcheStart->getSafeText()) {
            $criteriaVo->setDateFinMarcheStart($this->dateFinMarcheStart->getSafeText());
        }
        if ($this->dateFinMarcheEnd->getSafeText()) {
            $criteriaVo->setDateFinMarcheEnd($this->dateFinMarcheEnd->getSafeText());
        }
        if ($this->dateFinMaxMarcheStart->getSafeText()) {
            $criteriaVo->setDateFinMaxMarcheStart($this->dateFinMaxMarcheStart->getSafeText());
        }
        if ($this->dateFinMaxMarcheEnd->getSafeText()) {
            $criteriaVo->setDateFinMaxMarcheEnd($this->dateFinMaxMarcheEnd->getSafeText());
        }
        if ($this->keywordSearch->getSafeText()) {
            $criteriaVo->setMotsCles($this->keywordSearch->getSafeText());
        }
        if ($this->exact->checked) {
            $criteriaVo->setSearchModeExact(true);
        } else {
            $criteriaVo->setSearchModeExact(false);
        }

        return $criteriaVo;
    }

    /**
     * permet de lancer une reherche.
     *
     * @param Atexo_Contrat_CriteriaVo $criteriaVo
     *
     * @return void
     *
     * @author Loubna EZZIANI <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-roadmap
     *
     * @copyright Atexo 2016
     */
    public function startSearch($criteriaVo)
    {
        $this->Page->advancedSearchContrat->setVisible(false);
        $this->Page->dataForSearchResult($criteriaVo);
    }

    /**
     * permet de replir la liste des categorie.
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since place-2015
     *
     * @copyright Atexo 2016
     */
    public function chargerCategories()
    {
        $categories = Atexo_Consultation_Category::retrieveCategories();
        $dataCategories = Atexo_Consultation_Category::retrieveDataCategories($categories);
        $this->categorie->Datasource = $dataCategories;
        $this->categorie->dataBind();
    }
}
