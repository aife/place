<!--Debut Bloc Recherche multicritere-->
<div class="form-field margin-0 bloc-formulaire">
  <div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><span class="title"><com:TTranslate>DEFINE_RECHERCH_MULTICRITAIRE</com:TTranslate></span></div>
  <div class="content">
	<div class="spacer-small"></div>
	<div class="line">
		<div class="intitule-150"><label for="type"><com:TTranslate>TEXT_TYPE</com:TTranslate></label> :</div>
			<com:TDropDownList 
					id="type" 
					CssClass="small" 
					Attributes.title="<%=Prado::localize('TEXT_TYPE')%>"> 
			</com:TDropDownList>
	</div>
	<div class="line">
		<div class="intitule-150"><label for="categorie"><com:TTranslate>DEFINE_CATEGORIE_PRINCIPAL</com:TTranslate></label> :</div>
		<com:TDropDownList 
			id="categorie" 
			CssClass="small" 
			Attributes.title="<%=Prado::localize('DEFINE_CATEGORIE_PRINCIPALE')%>"> 
		</com:TDropDownList>
	</div>
	<div class="spacer"></div>
	<h2><com:TTranslate>DEFINE_RECHERCHE_PAR_DATE</com:TTranslate></h2>
	<div class="line">
		<div class="intitule-150"><com:TTranslate>ANNEE</com:TTranslate> :</div>
		<div class="intitule-auto"><label for="anneeDebut"><com:TTranslate>ICONE_CALENDRIER</com:TTranslate></label></div>
		<div class="calendar">
			<com:TTextBox  maxlength="4" CssClass="input-annee mask-year" Attributes.title="<%=Prado::localize('DEFINE_ANNEE_DE_DEBUT')%>" id="anneeDebut" />
		</div>
		<div class="intitule-auto indent-10"><label for="anneeFin"><com:TTranslate>DEFINE_FIN</com:TTranslate></label></div>
		<div class="calendar">
			<com:TTextBox  maxlength="4" CssClass="input-annee mask-year" Attributes.title="<%=Prado::localize('DEFINE_ANNEE_DE_FIN')%>" id="anneeFin" />
		</div>
	</div>
	<div class="spacer"></div>
	<!--Debut Recherche par mot-cle-->
	<h2><com:TTranslate>DEFINE_RECHERCHE_MOT_CLE</com:TTranslate>
		<img title="Info-bulle" 
			alt="Info-bulle" 
			class="picto-info" 
			onmouseout="cacheBulle('infosContrat')" 
			onmouseover="afficheBulle('infosContrat', this)" 
			src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" />
		<div id="infosContrat" class="info-bulle" onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();">
			<div>
				<com:TTranslate>TEXT_INFOS_BULLE_SEARCH_AGENT</com:TTranslate>
			</div>
		</div>
	</h2>	
	<div class="line">
		<div class="intitule-bloc bloc-155"><label for="keywordSearch"><com:TTranslate>DEFINE_CODE_DESCIPTION_OPERATATION</com:TTranslate> :</label></div>
		<div class="content-bloc bloc-600">
			<com:TTextBox  CssClass="long" Attributes.title="<%=Prado::localize('DEFINE_ANNEE_DE_FIN')%>" id="keywordSearch" />
			<div id="AdvancedSearch_panelMotsCles" class="content-bloc bloc-600">
			<div id="AdvancedSearch_panelRechercheFloue" class="line">
				<span class="radio" title="Recherche approchée">
				   <com:TRadioButton 
						cssClass="radio" 
						GroupName="recherche" 
						ID="AdvancedSearch_floue" 
						Text="<%=Prado::localize('TEXT_RECHERCHE_FLOUE')%>"
						Attributes.title="<%=Prado::localize('TEXT_RECHERCHE_FLOUE')%>"
						Checked = "true"
						/>
				</span>
				&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				<span class="radio" title="Recherche exacte">
				 <com:TRadioButton 
						cssClass="radio" 
						GroupName="recherche" 
						ID="AdvancedSearch_exact" 
						Text="<%=Prado::localize('TEXT_RECHERCHE_EXACTE')%>"
						Attributes.title="<%=Prado::localize('TEXT_RECHERCHE_EXACTE')%>"/>
				</span>
			</div>
		</div>			
		</div>
	</div>
	<!--Fin Recherche par mot-cle-->
	<div class="spacer-small"></div>
	<div class="boutons-line">
		<com:TButton  
			CssClass="bouton-moyen-120 float-right" 
			Attributes.value="<%=Prado::localize('DEFINE_LANCER_RECHERCHE')%>" 
			Attributes.title="<%=Prado::localize('DEFINE_LANCER_RECHERCHE')%>" 
			Onclick="search"
		/> 
	</div>
	<div class="breaker"></div>
  </div>
  <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
</div>
