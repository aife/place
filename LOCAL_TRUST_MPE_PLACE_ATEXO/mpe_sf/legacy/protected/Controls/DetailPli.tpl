<div class="form-field">
	<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
	<div class="content">
		<h3><com:TTranslate>TEXT_DETAIL_PLI</com:TTranslate></h3>
		<!--Debut Bloc gauche-->
		<div class="bloc-600 float-left">
			<div class="line">
				<div class="intitule-150"><strong><com:TTranslate>TEXT_NUMERO_PLI</com:TTranslate></strong> : </div><com:TLabel ID="numPli"/>
			</div>

			<div class="line">
				<div class="intitule-150"><strong><com:TTranslate>TEXT_ENTREPRISE</com:TTranslate></strong> : </div><com:TLabel ID="nomSociete"/>
			</div>
			<com:TPanel id="panelTypePanel" Cssclass="line">
				<div class="intitule-150"><strong><com:TTranslate>TEXT_TYPE_ENVELOPPE</com:TTranslate></strong> : </div><com:TLabel ID="TypeEnv"/> <com:TLabel ID="lot"/>
			</com:TPanel>
			<div class="line">

				<div class="intitule-150"><strong><com:TTranslate>HORODATAGE_DEPOT</com:TTranslate></strong> : </div><com:TLabel ID="horodatage"/>
			</div>
			<div class="line">
				<div class="intitule-150"><strong><com:TTranslate>TEXT_STATUT_PLI</com:TTranslate></strong> : </div><com:TLabel ID="statutPli"/> <com:TLabel ID="statut2Pli"/>
			</div>
			<com:TPanel id="panelAccuse" cssclass="line"  visible="false">
				<div class="intitule-150"><strong><com:TTranslate>ACCUSE_RECEPTION_ENTREPRISE</com:TTranslate></strong> : </div><com:TLinkButton OnClick="downloadAccuseReception"><img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-pdf.gif" /></com:TLinkButton>
			</com:TPanel>
		</div>
		<!--Fin Bloc gauche-->

		<!--Debut Bloc Droite-->
		<com:TPanel id="panelSignature" cssclass="bloc-signature">
			<div class="statut-signature" style="display:block;">
				<com:PictoSignature ID="pictoSignature" Size="Big"/> : 
				<com:PictoStatutSignature  ID="statutSignature" Size="Big"/>
			</div>
		</com:TPanel>

		<!--Fin Bloc Droite-->
		<div class="breaker"></div>
	</div>
	<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
</div>