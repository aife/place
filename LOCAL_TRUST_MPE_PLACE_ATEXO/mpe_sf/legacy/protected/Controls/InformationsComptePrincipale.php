<?php

namespace Application\Controls;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonAgent;
use Application\Propel\Mpe\CommonAgentQuery;
use Application\Propel\Mpe\CommonAgentServiceMetier;
use Application\Propel\Mpe\CommonServiceMertier;
use Application\Propel\Mpe\CommonServiceMertierProfils;
use Application\Propel\Mpe\CommonServiceMertierQuery;
use Application\Service\Atexo\Agent\Atexo_Agent_AssociationComptes;
use Application\Service\Atexo\Agent\Atexo_Agent_EnablingsAgents;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Util;
use Exception;
use Prado\Prado;

/*
 * commentaires
 * @author khadija Chouika <khadija.chouika@atexo.com>
 * @copyright Atexo 2013
 * @version 0
 * @since MPE-4.4
 * @package Controls
 */
class InformationsComptePrincipale extends MpeTTemplateControl
{
    public $agent_principal;
    public ?array $_arrayMsgErreur = null;
    public bool $_isValid = true;

    public function getAgentPrincipal()
    {
        return $this->agent_principal;
    }

    public function setAgentPrincipal($value)
    {
        $this->agent_principal = $value;
    }

    public function getInformationsAgentPrincipale($idAgent)
    {
        $this->setViewState('idAgent', $idAgent);
        $this->blocInformations->chargerAgent(
            $idAgent,
            Prado::localize('TEXT_ASSOCIER_COMPTE_SUIVANT'),
            Prado::localize('TEXT_MESSAGE_COMPTE_PRINCIPALE'),
            Prado::localize('TEXT_MESSAGE_COMPTE_PRINCIPALE'),
            true
        );
    }

    /**
     * Fonction permet de chercher un agent par son login.
     */
    public function chercherAgent()
    {
        $agent = null;
        if (true == $this->inputCompteExistant->checked) {
            $message = '';
            $agent1 = (new CommonAgentQuery())->findOneById($this->getViewState('idAgent'));
            $login = $agent1->getLogin();
            if ($login == $this->compteAssocie->Text) {
                $message = Prado::localize('TEXT_IMPOSSIBLE_ASSOCIATIONS_AVEC_LUI_MEME');
            } else {
                $agent = (new Atexo_Agent())->retrieveAgentByLogin($this->compteAssocie->Text);
                if ($agent instanceof CommonAgent) {
                    //$this->setViewState('Agent',$agent);
                    if ((3 == $agent->getTentativesMdp()) || ('0' == $agent->getActif())) {
                        $message = Prado::localize('TEXT_ASSOCIER_IDENTIFIANT_N_EXISTE_PAS');
                    } elseif ($agent->getComptesAssocieSecondaire($this->getViewState('idAgent'))) {
                        $message = Prado::localize('TEXT_ASSOCIER_EXISTE');
                    }
                } else {
                    $message = Prado::localize('TEXT_ASSOCIER_IDENTIFIANT_N_EXISTE_PAS');
                }
            }
            if ($message) {
                $imgError = $this->idClientErreur->value;
                $this->validSummary->addServerError($message);
                $this->script->Text = "<script>document.getElementById('validSummary').style.display='';
				document.getElementById('".$imgError."').style.display='';</script>";

                return false;
            }

            return $agent;
        }
    }

    /**
     * Fonction retourne les données de compte en cours.
     *
     * @return array
     */
    public function getArraDonnee()
    {
        $agent = $this->blocInformations->getViewState('Agent');
        $arrayDonne = [];
        $arrayDonne['nom'] = $agent->getNom();
        $arrayDonne['prenom'] = $agent->getPrenom();
        $arrayDonne['email'] = $this->blocInformations->email->Text;
        $arrayDonne['entite'] = $this->blocInformations->entite->Text;
        $arrayDonne['service'] = $this->blocInformations->service->Text;

        return $arrayDonne;
    }

    public function getDataSourceServiceMetier()
    {
        $serviceMetier = CommonServiceMertierQuery::create()->find();
        $dataSource = [];
        foreach ($serviceMetier as $unService) {
            if ($unService instanceof CommonServiceMertier) {
                $denomination = $unService->getDenomination();
                $element['denomination'] = Prado::localize($denomination);
                $element['idServiceMetier'] = $unService->getId();
                $serviecProfils = $unService->getCommonServiceMertierProfilss();
                $profils[0] = Prado::localize('TEXT_SELECTIONNER').'...';
                foreach ($serviecProfils as $oneProfil) {
                    if ($oneProfil instanceof CommonServiceMertierProfils) {
                        $profils[$oneProfil->getIdAuto()] = $oneProfil->getLibelle();
                    }
                }
                $element['profils'] = $profils;
                $dataSource[] = $element;
            }
        }
        $this->serviceMetier->DataSource = $dataSource;
        $this->serviceMetier->DataBind();
    }

    public function displayOrganismes()
    {
        $lanque = Atexo_CurrentUser::readFromSession('lang');
        if (isset($_COOKIE['selectedorg'])) {
            $ArrayOrganismes = [];
            $getDenominationOrg = 'getDenominationOrg'.Atexo_Languages::getLanguageAbbreviation($lanque);
            $ArrayOrganismesO = Atexo_Organismes::retrieveOrganismeByAcronyme($_COOKIE['selectedorg']);
            if (0 == strcmp($this->langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE'))
                || !$ArrayOrganismesO->$getDenominationOrg()) {
                $ArrayOrganismes[$ArrayOrganismesO->getAcronyme()] = $ArrayOrganismesO->getDenominationOrg();
            } else {
                $ArrayOrganismes[$ArrayOrganismesO->getAcronyme()] = $ArrayOrganismesO->$getDenominationOrg();
            }
            $listEntityPurchase = Atexo_EntityPurchase::retrieveEntityPurchaseByOrganisme($ArrayOrganismesO->getAcronyme(), null, $lanque);
            $this->entityPurchaseNames->dataSource = $listEntityPurchase;
            $this->entityPurchaseNames->DataBind();
        } else {
            $ArrayOrganismes = Atexo_Organismes::retrieveOrganismes(false, $lanque);
        }
        array_unshift($ArrayOrganismes, '--- '.Prado::localize('TEXT__TOUS_MINISTERES').' ---');
        $this->organismesNames->dataSource = $ArrayOrganismes;
        $this->organismesNames->DataBind();
        $this->organismesNames->SelectedValue = Atexo_CurrentUser::getOrganismAcronym();
        self::displayEntityPurchase();
        if (Atexo_CurrentUser::hasHabilitation('HyperAdmin')) {
            $this->organismesNames->setEnabled(false);
        }
    }

    public function displayEntityPurchase()
    {
        if ('' != $this->organismesNames->SelectedValue && ' ' != $this->organismesNames->SelectedValue && '0' != $this->organismesNames->SelectedValue) {
            $organisme = $this->organismesNames->SelectedValue;
            $listEntityPurchase = Atexo_EntityPurchase::getEntityPurchase($organisme, true);
            if (is_array($listEntityPurchase) && count($listEntityPurchase)) {
                foreach ($listEntityPurchase as $key => $entityPurchase) {
                    $entities[$key] = $entityPurchase;
                }
            }
        }
        $this->entityPurchaseNames->dataSource = $entities;
        $this->entityPurchaseNames->DataBind();
    }

    public function showBlocCompteExistant()
    {
        $this->script2->Text = "<script>document.getElementById('".$this->inputCompteExistant->getClientId()."').checked = 'true';
										showDiv('panel_compteExistant');
										hideDiv('panel_nouveauCompte');
								 </script>";
    }

    public function validateMailAgent($sender, $param)
    {
        $this->script2->Text = "<script>document.getElementById('".$this->errorEmail->getClientId()."').style.display ='none';</script>";
        $email = $this->emailAgent->getSafeText();
        if ($email) {
            $emailsErrone = Atexo_Util::checkMultipleEmail($email);
            if (is_array($emailsErrone)) {
                $this->_arrayMsgErreur[] = Prado::localize('MSG_ERROR_FORMAT_EMAIL').' : '.implode(',', $emailsErrone);
                $this->script2->Text = "<script>document.getElementById('".$this->errorEmail->getClientId()."').style.display ='';</script>";
            }
            $agent = CommonAgentQuery::create()->filterByEmail($email)->filterById($this->getViewState('idAgent'), Criteria::NOT_EQUAL)->findOne();
            if ($agent instanceof CommonAgent) {
                $this->_arrayMsgErreur[] = Prado::localize('TEXT_EMAIL_EXISTE_DEJA', ['adresse mail' => $email]);
                $this->script2->Text = "<script>document.getElementById('".$this->errorEmail->getClientId()."').style.display ='';</script>";
            }
        }
    }

    public function validateLoginAgent($sender, $param)
    {
        $this->script2->Text = "<script>document.getElementById('".$this->errorLogin->getClientId()."').style.display ='none';</script>";
        $login = $this->loginAgent->getSafeText();
        if ($login) {
            $agent = CommonAgentQuery::create()->filterByLogin($login)->findOne();
            if ($agent instanceof CommonAgent) {
                $this->_arrayMsgErreur[] = Prado::localize('TEXT_IDENTIFIANT_EXISTE_DEJA', ['identifiant' => $login]);
                $this->script2->Text = "<script>document.getElementById('".$this->errorLogin->getClientId()."').style.display ='';</script>";
            }
        }
    }

    public function showServerError($sender, $param)
    {
        if (is_array($this->_arrayMsgErreur) && count((array) $this->_arrayMsgErreur)) {
            $errorMsg = implode('</li><li>', $this->_arrayMsgErreur);
            $errorMsg = $errorMsg.'</li>';
            $this->validSummary->addServerError($errorMsg);
            $this->script2->Text = "<script>document.getElementById('validSummary').style.display='';</script>";
            $this->_isValid = false;
        }
    }

    public function saveAgent($sender, $param)
    {
        self::validateLoginAgent($sender, $param);
        self::validateMailAgent($sender, $param);
        self::showServerError($sender, $param);
        if ($this->_isValid) {
            $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
            $agent = new CommonAgent();
            $agent->setNom($this->nomAgent->Text);
            $agent->setPrenom($this->prenomAgent->Text);
            $agent->setEmail(trim($this->emailAgent->Text));
            $agent->setOrganisme($this->organismesNames->selectedValue);
            $agent->setServiceId($this->entityPurchaseNames->selectedValue != 0 ? $this->entityPurchaseNames->selectedValue  : null);
            $agent->setDateCreation(date('Y-m-d H:i:s'));
            $agent->setDateModification(date('Y-m-d H:i:s'));
            $agent->setAlerteReponseElectronique('1');
            $agent->setAlerteClotureConsultation('1');
            $agent->setAlerteReceptionMessage('1');
            $agent->setAlertePublicationBoamp('1');
            $agent->setAlerteEchecPublicationBoamp('1');
            $agent->setAlerteQuestionEntreprise('1');
            $agent->setLogin(trim($this->loginAgent->Text));
            Atexo_Agent::updateNewAgentPasswordByArgon2($agent, $this->passwordAgent->Text);
            $agent->setActif(1);
            $connexion->beginTransaction();
            try {
                if ($agent->save($connexion)) {
                    self::saveServiceMetierAgent($agent->getId(), $connexion);
                }
                $connexion->commit();

                return $agent;
            } catch (Exception $e) {
                $connexion->rollback();
                throw $e;
            }
        }

        return false;
    }

    public function saveServiceMetierAgent($idAgent, $connexion)
    {
        foreach ($this->serviceMetier->getItems() as $item) {
            if (true == $item->checkService->Checked) {
                $idServiceMetier = $item->idService->Value;
                $idProfileInterne = $item->profils->getSelectedValue();
                if ('' != $idServiceMetier && '0' != $idServiceMetier) {
                    $serviceMetier = new CommonAgentServiceMetier();
                    $serviceMetier->setDateCreation(date('Y-m-d H:i:s'));
                    $serviceMetier->setDateModification(date('Y-m-d H:i:s'));
                    $serviceMetier->setIdAgent($idAgent);
                    $serviceMetier->setIdServiceMetier($idServiceMetier);
                    $serviceMetier->setIdProfilService($idProfileInterne);
                    $serviceMetier->save($connexion);
                    if ($idServiceMetier == Atexo_Config::getParameter('SERVICE_METIER_MPE')) {
                        $newHablitation = Atexo_Agent_EnablingsAgents::addOrModifyEnablingsOfTheAgents($idProfileInterne, $idAgent);
                        $newHablitation->setIdAgent($idAgent);
                        $newHablitation->save($connexion);
                    }
                    if ($idServiceMetier == Atexo_Config::getParameter('SERVICE_METIER_SOCLE')) {
                        $newHablitationSocle = (new Atexo_Agent_EnablingsAgents())->addOrModifyEnablingsSocleOfAgent($idAgent);
                        $newHablitationSocle->setIdAgent($idAgent);
                        $newHablitationSocle->save($connexion);
                    }
                }
            }
        }
    }

    public function associerAgent($sender, $param)
    {
        $agent = null;
        if (true == $this->inputCompteExistant->checked) {
            $agent = self::chercherAgent();
        } elseif (true == $this->inputNouveauCompte->checked) {
            $agent = self::saveAgent($sender, $param);
        }
        if ($agent instanceof CommonAgent) {
            $uid = (new Atexo_Message())->getUniqueId();
            (new Atexo_Agent_AssociationComptes())->associeCompte($this->getViewState('idAgent'), $agent->getId(), $uid);
            (new Atexo_Message())->sendEmailConfirmation($this->getArraDonnee(), $agent->getEmail(), $uid);
            $this->script->Text = "<script> resetForm();
											 J('.compte-association').dialog('destroy');
											 J('.bloc-message').hide().fadeIn(1200);
											 document.getElementById('ctl0_CONTENU_PAGE_refreshRepeater').click();
												</script>";
        }
    }
}
