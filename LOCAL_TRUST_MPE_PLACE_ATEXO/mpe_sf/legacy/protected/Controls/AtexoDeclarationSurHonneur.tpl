	<com:TPanel ID="panelDeclarationInscrit" cssClass="form-bloc margin-0">
		<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
		<div class="content">
		<div class="line">
				<span class="check-bloc"><com:TCheckBox id="checkDeclaration" Attributes.title="<%=Prado::localize('TEXT_DECLARATION_INSCRIT')%>" cssClass="check" /></span>
				<div class="content-bloc bloc-700">
					<label for="infosCertifiesExacte"><com:TTranslate>TEXT_DECLARATION_INSCRIT</com:TTranslate></label>
				</div> 
				<com:TRequiredFieldValidator 
					ControlToValidate="checkDeclaration"
					ID="validateurDeclaration"
					Enabled="true"
					Display="Dynamic"
					ErrorMessage="<%=Prado::localize('TEXT_DECLARATION_HONNEUR')%>"
					EnableClientScript="true"  						 					 
		 			Text="<span title='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' /></span>">
					<prop:ClientSide.OnValidationError>
		 				document.getElementById('divValidationSummary').style.display='';
					</prop:ClientSide.OnValidationError>
			  </com:TRequiredFieldValidator>
				
		</div>		
			<div class="breaker"></div>
		</div>
		
		<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
	</com:TPanel>