<div class="form-field margin-0">
	<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><span class="title"><com:TTranslate>DEFINE_RECHERCHE_MULTICRITAIRE</com:TTranslate></span></div>
	<div class="content">
	             <div class="spacer-small"></div>
		<!--Debut Ligne Entite publique-->
	<div class="line">
		<div class="intitule-140">
			<label for="entitePubique" style=""><com:TTranslate>ENTITE_PUBLIQUE</com:TTranslate></label> :</div>
			<com:TActiveDropDownList ID="entitePubique" cssClass="long" Attributes.title="<%=Prado::localize('TEXT_MINISTERE')%>"/>
			
		</div>
		<!--Fin Ligne Entite publique-->
	                <div class="spacer"></div>
		<!--Debut Recherche Organisme par mot-cle-->
		<h4><com:TTranslate>DEFINE_RECHERCHE_MOT_CLE</com:TTranslate><img title="Info-bulle" alt="Info-bulle" class="picto-info-intitule" onmouseout="cacheBulle('infosRechercheAgent')" onmouseover="afficheBulle('infosRechercheAgent', this)" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" /></h4>
		
		<div class="line">
			<div class="intitule-bloc intitule-140">
				<label for="denominationOrganisme"><com:TTranslate>DENOMINATION</com:TTranslate> :</label>
			</div>
			<div class="content-bloc">
				<com:TTextBox ID="denominationOrganisme"  cssClass="long" Attributes.title="<%=Prado::localize('DENOMINATION')%>" />
			</div>
		</div>
		<!--Fin Recherche Organisme par mot-cle-->
	
	     <div class="spacer-small"></div>
	     <div class="boutons-line">
	          <com:TActiveButton CssClass="bouton-moyen-120 float-right" Text="<%=Prado::localize('DEFINE_LANCER_RECHERCHE')%>" Attributes.title="<%=Prado::localize('DEFINE_LANCER_RECHERCHE')%>" OnCallback="Page.onClickSearch"/>
	     </div>
	     <div class="breaker"></div>
	 </div>
    <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
    <div id="infosRechercheAgent" class="info-bulle" onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();"><div><com:TTranslate>TEXT_INFOS_BULLE_SEARCH_AGENT</com:TTranslate>.</div></div>
</div>
