<!--Debut Bloc Identification de l'entreprise-->
<div class="toggle-panel">
	<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
		<div class="content">
			<div class="title-toggle" onclick="togglePanel(this,'infoDonneesComplementaires');popupResize();"><com:TTranslate>CG76_DEFINE_TEXT_DONNEES_COMPLEMENTAIRES</com:TTranslate></div>
			<div class="panel" style="display:none;" id="infoDonneesComplementaires">
					<div class="spacer-mini"></div>
					<!-- -->
					<div class="line">
						<div class="intitule-180">Adresse électronique :</div>
						<div class="content-bloc bloc-550"><com:TLabel id="emailDC" /></div>
					</div>
					<!-- -->
					<div class="line">
						<div class="intitule-180">Type de formation :</div>
						<div class="content-bloc bloc-550"><com:TLabel id="typeDC" /></div>
					</div>
					<!-- -->
					<div class="line">
						<div class="intitule-180">Coût moyen d'une journée :</div>
						<div class="content-bloc bloc-550"><com:TLabel id="coutMoyenDC" /></div>
					</div>
					<!-- -->
					<!-- -->
					<div class="line">
						<div class="intitule-180">Collaboration FPT ?</div>
						<div class="content-bloc bloc-550"><com:TLabel id="fptDC" /></div>
					</div>
					<!-- -->
					
					<!-- -->
					<div class="line">
						<div class="intitule-180">Collaboration FPE ?</div>
						<div class="content-bloc bloc-550"><com:TLabel id="fpeDC" /></div>
					</div>
					<!-- -->
					<!-- -->
					<div class="line">
						<div class="intitule-180">Centre de documentation :</div>
						<div class="content-bloc bloc-550"><com:TLabel id="centreDocDC" /></div>
					</div>
					<!-- -->
					<!-- -->
					<div class="line">
						<div class="intitule-180">Service de reprographie :</div>
						<div class="content-bloc bloc-550"><com:TLabel id="serviceRepDC" /></div>
					</div>
					<!-- -->
					<!-- -->
					<div class="line">
						<div class="intitule-180">Salle informatique ou multi-média :</div>
						<div class="content-bloc bloc-550"><com:TLabel id="salleInfoDC" /></div>
					</div>
					<!-- -->
					<!-- -->
					<div class="line">
						<div class="intitule-180">Salle de cours :</div>
						<div class="content-bloc bloc-550"><com:TLabel id="salleCoursDC" /></div>
					</div>
					<!-- -->
					
					<!-- -->
					<div class="line">
						<div class="intitule-180">Aire géographique d'intervention :</div>
						<div class="content-bloc bloc-550"><com:TLabel id="aireGeoInterDC" /></div>
					</div>
					<!-- -->
					<!-- -->
					<div class="line">
						<div class="intitule-180">Pièce jointe :</div>
						<div class="content-bloc bloc-550">
							<com:TRepeater ID="RepeaterNomsFiles" EnableViewState="true">
								<prop:ItemTemplate>
									<com:TLinkButton ID="download" OnCommand="Page.idDonneesComplementaires.downloadBlob" CommandName="<%#$this->Data['id']%>" CommandParameter="<%#$this->Data['name']%>"><%#$this->Data['name']%></com:TLinkButton>
									<div class="breaker"></div>									
								</prop:ItemTemplate>
							</com:TRepeater>
							<div class="breaker"></div>
						</div>
					</div>
					<div class="spacer"></div>
					<div class="line">
						<div class="intitule-180">Définition des domaines d'activités :</div>
						<div class="content-bloc bloc-550">
							<com:TRepeater ID="RepeaterNomsDomaines" EnableViewState="true">
								<prop:ItemTemplate>
									<div class="intitule-auto"><%#$this->Data['libelle']%></div>
									<div class="breaker"></div>	
									<com:TRepeater ID="RepeaterNomsDomainesFils" EnableViewState="true" DataSource="<%#$this->Data['fils']%>">
										<prop:ItemTemplate>
											<div class="intitule-auto indent-15">- <%#$this->Data['libelle']%></div>
											<div class="breaker"></div>
										</prop:ItemTemplate>
									</com:TRepeater>							
								</prop:ItemTemplate>
							</com:TRepeater>
						</div>
					</div>
	<!-- -->
					<div class="spacer-small"></div>
					<div class="line">
					<div class="intitule-180">Commentaires :</div>
	
					<com:TTextBox TextMode="MultiLine" ID="commentaires" ToolTip="commentaires" CssClass="long-750 high-60"/>
					<div class="boutons-line">
						<com:TLinkButton  
								ID="enregistrerCom"
								CssClass="bouton-moyen float-left" 
								Attributes.title="Enregistrer" 
								Text="Enregistrer"
								onClick="OnValidClick"/>	
					</div>
			</div>
	<!-- -->
			<div class="spacer-small"></div>
			<div class="breaker"></div>
		</div>
		<div class="breaker"></div>
	</div>
	<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
</div>
<!--Fin Bloc Identification de l'entreprise-->