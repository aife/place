<com:TRepeater ID="RepeaterReferentielRadio">
	<prop:ItemTemplate>
		<com:TActivePanel ID="panelReferentielRadio" visible="<%# !$this->SourceTemplateControl->getCheck()%>">
			<div Class="<%# $this->SourceTemplateControl->getLine()?'line':''%>" >
				<com:TPanel cssClass="<%#($this->SourceTemplateControl->getCssClass())%>"> 
					<com:TLabel id="titre" text="<%#Prado::localize($this->Data->getCodeLibelle())%>" visible="<%# !$this->SourceTemplateControl->getCheck()%>" />
					<span <%#($this->Data->getObligatoire()&& $this->SourceTemplateControl->getObligatoire()) ? "class='champ-oblig'" : "style='display:none'" %>>
						<com:TLabel id="etoile" text="*" visible="<%# !$this->SourceTemplateControl->getCheck()%>"/>
					</span>  
					<span <%#($this->Data->getLibelleInfoBulle()) ? "class='champ-oblig'" : "style='display:none'" %>>
						<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" onmouseover="afficheBulle('infosRefrentiel_<%#$this->Data->getId()%>', this)" onmouseout="cacheBulle('infosRefrentiel_<%#$this->Data->getId()%>')" class="picto-info-intitule" alt="Info-bulle" title="Info-bulle" />
					</span>
					<com:TLabel id="deuxPoint" text=":" visible="<%# !$this->SourceTemplateControl->getCheck()%>"/>
				</com:TPanel>
				<com:TLabel id="labelReferentielRadio" cssClass="content-bloc"  text="" visible="<%# $this->SourceTemplateControl->getLabel()%>"/>
			 	<div class="intitule-auto" style="<%#(($this->SourceTemplateControl->getLabel())) ? 'display:none' :'display:'%>">
					<com:TActiveRadioButton 
						ID="OptionOui" 
						visible="<%#(!$this->SourceTemplateControl->getLabel())%>"
						cssClass="radio" 
						checked="<%#($this->Data->getDefaultValue() === '1' ) ? 'true' : 'false' %>"
						GroupName="radioRef" 
						Text="<%=Prado::localize('TEXT_OUI')%>"
						Attributes.title="<%=Prado::localize('TEXT_OUI')%>"
					/>
	            </div>
			</div>	
			<com:TPanel cssClass="line" visible="<%#(($this->SourceTemplateControl->getLabel())) ? 'false' :'true'%>">          
				<div class="<%#($this->SourceTemplateControl->getCssClass())%>" >&nbsp;</div>  
	            <div class="intitule-auto">
					<com:TActiveRadioButton 
						ID="OptionNon" 
						visible="<%#(!$this->SourceTemplateControl->getLabel())%>
						cssClass="radio" 
						Checked="<%#($this->Data->getDefaultValue() === '0' ) ? 'true' : 'false' %>"
						GroupName="radioRef" 
						Text="<%=Prado::localize('TEXT_NON')%>"
						Attributes.title="<%=Prado::localize('TEXT_NON')%>"
					/>
				</div>
			</com:TPanel>
			<com:TPanel cssClass="line" visible="<%#($this->SourceTemplateControl->getMode() == '2') ? 'true' :'false'%>">          
				<div class="intitule-150">&nbsp;</div>  
	            <div class="intitule-auto">
					<com:TActiveRadioButton 
						ID="OptionIndifferent" 
						visible="<%#(!$this->SourceTemplateControl->getLabel() && ($this->SourceTemplateControl->getMode() == '2') )%>
						cssClass="radio" 
						Checked="<%#($this->Data->getDefaultValue() === '0' ) ? 'true' : 'false' %>"
						GroupName="radioRef" 
						Text="<%=Prado::localize('TEXT_INDIFFERENT')%>"
						Attributes.title="<%=Prado::localize('TEXT_INDIFFERENT')%>"
					/>
				</div>
			</com:TPanel>
			<com:TTextBox style="display:none" ID="ClientIdsRadio" text="<%=$this->OptionOui->getClientId().'#'.$this->OptionNon->getClientId()%>"/>
			<com:TCustomValidator 
				ID="validatorOptionSelected"
				visible="<%#($this->Data->getObligatoire() && (!$this->SourceTemplateControl->getLabel()) && ($this->SourceTemplateControl->getMode() != '2')) ? 'true' :'false'%>"
				ControlToValidate="ClientIdsRadio"
				ValidationGroup="<%#($this->SourceTemplateControl->getValidationGroup())%>"
				ClientValidationFunction="validateOptionSelected"
				ErrorMessage="<%=Prado::localize($this->codeLibelle->Text)%>"
				Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
				>   
				<prop:ClientSide.OnValidationError>
					document.getElementById("<%#($this->SourceTemplateControl->getValidationSummary())%>").style.display='';
						if(document.getElementById('ctl0_CONTENU_PAGE_saveBack'))
					document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
						if(document.getElementById('ctl0_CONTENU_PAGE_save'))
					document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
						if(document.getElementById('ctl0_CONTENU_PAGE_buttonValidate'))
					document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
						if(document.getElementById('ctl0_CONTENU_PAGE_blocBoutons'))
					document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
				</prop:ClientSide.OnValidationError>
			</com:TCustomValidator>
			
         	<com:TLabel id="iDLibelle" visible="false" text="<%#$this->Data->getId()%>" />
         	<com:TLabel id="codeLibelle" visible="false" text="<%#$this->Data->getCodeLibelle()%>" />
         	<com:TLabel id="typeRef" visible="false" text="<%#$this->Data->getType()%>" />
         	<com:TLabel id="typeSearch" visible="false" text="<%#$this->Data->getTypeSearch()%>" />
         	<com:THiddenField id="modeRecherche" value="<%#$this->Data->getModeRecherche()%>" />


			<div id="infosRefrentiel_<%#$this->Data->getId()%>" class="info-bulle" onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();">
				<div><%#Prado::localize($this->Data->getLibelleInfoBulle())%></div>
			</div>
			<div class="breaker"></div>
			
			</com:TActivePanel>
	</prop:ItemTemplate>
</com:TRepeater>
