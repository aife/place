<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_EntrepriseVo;

/**
 * Classe de gestion des informations de l'entreprise.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 *
 * @version 1.0
 *
 * @since 4.9.0
 *
 * @copyright Atexo 2015
 */
class PanelStatutEntreprise extends MpeTTemplateControl
{
    private $entreprise = null;
    private bool $visible = false;

    public function setEntreprise($value)
    {
        $this->entreprise = $value;
    }

    public function getEntreprise()
    {
        return $this->Page->entreprise;
    }

    public function setVisible($value)
    {
        $this->visible = $value;
    }

    public function getVisible($checkParents = true)
    {
        return $this->visible;
    }

    /**
     * Chargement de la page.
     *
     * @return void
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function onLoad($param)
    {
        if (!$this->Page->IsPostBack) {
            $this->customizeForm();
        }
    }

    public function customizeForm()
    {
        $this->panelStatutCompteEntreprise->visible = false;
        if (Atexo_Module::isEnabled('StatutCompteEntreprise') && true === $this->getVisible()) {
            $this->panelStatutCompteEntreprise->visible = true;
            if ($this->getEntreprise()->getEntrepriseEA()) {
                $this->entrepriseEA->checked = true;
            } else {
                $this->entrepriseEA->checked = false;
            }
            if ($this->getEntreprise()->getEntrepriseSIAE()) {
                $this->entrepriseSIAE->checked = true;
            } else {
                $this->entrepriseSIAE->checked = false;
            }
        }
    }

    public function chargertInformation(&$entreprise)
    {
        if (Atexo_Module::isEnabled('StatutCompteEntreprise') && $entreprise instanceof Atexo_Entreprise_EntrepriseVo) {
            if ($this->entrepriseEA->checked) {
                $entreprise->setEntrepriseEA('1');
            } else {
                $entreprise->setEntrepriseEA('0');
            }
            if ($this->entrepriseSIAE->checked) {
                $entreprise->setEntrepriseSIAE('1');
            } else {
                $entreprise->setEntrepriseSIAE('0');
            }
        }
    }
}
