<com:AtexoValidationSummary  ValidationGroup="validateinfomessage" />
<com:PanelMessageErreur  ID="messageErreur" visible="false"/>

<!--warning : si la consultation a été archivée, les modifications ne sont pas intégrées dans l'archive-->
<com:PanelMessageAvertissement ID="panelMessageWarning" Visible="false"/>
<!--Debut Bloc Message à envoyer-->
<com:TPanel ID="panelTypeMessage" CssClass="form-field">
	<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
	<com:TPanel CssClass="content">
		<div class="line">
			<div class="intitule-110"><label for="<%=$this->messageType->getClientId()%>"><com:TTranslate>DEFINE_TEXT_TYPE_MESSAGE</com:TTranslate></label> :</div>
			<com:TActiveDropDownList
					ID="messageType"
					CssClass="moyen"
					OnCallBack="refreshText"
					Attributes.title = "<%=Prado::localize('DEFINE_TEXT_TYPE_MESSAGE')%>"
			/>
		</div>
	</com:TPanel>
	<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
</com:TPanel>
		<!--Debut Bloc Option d'envoi-->
		<com:TPanel ID="panelOptionEnvoi" CssClass="form-field">
			<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><span class="title"><com:TTranslate>DEFINE_TEXT_OPTION_ENVOI</com:TTranslate></span></div>

			<div class="content">
				<!-- Debut msg info DV -->
				<com:TPanel ID="messageInfoDV" Visible="<%=Application\Service\Atexo\Atexo_Module::isEnabled('ModuleEnvol')%>">
					<div class="bloc-detail">
						<div class="bloc-message form-bloc-conf msg-info">
							<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
							<div class="content">
								<div class="message bloc-700">
									<!--Debut Message-->
									<div class="max-height-overflow <max-height-400">
										<com:TTranslate>DOSSIER_VOLUMINEUX_INFO_MESSAGERIE_SECURISEE</com:TTranslate>&nbsp;:<br>
										- <com:TTranslate>DEFINE_TEXT_COURIER_ELECTRONIQUE_TELECHARGEMENT_UNIQUE</com:TTranslate>  <com:TTranslate>TEXT_PLATE_FORME_AR</com:TTranslate>&nbsp;:<br>
										- <com:TTranslate>DEFINE_TEXT_COURIER_ELECTRONIQUE_DEMANDE_COMPLEMENT</com:TTranslate>&nbsp;:<br>
									</div>
									<!--Fin Message-->
								</div>
								<div class="breaker"></div>
							</div>
							<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
						</div>

					</div>
				</com:TPanel>
				<!-- fin msg info DV -->
				<div class="spacer-mini"></div>
				<div class="line">
					<div class="intitule-auto bloc-700">
						<com:TActiveRadioButton
								ID = "courrierElectroniqueSimple"
								GroupName="optionEnvoie"
								CssClass = "radio"
								Attributes.title = "<%=Prado::localize('DEFINE_TEXT_COURIER_ELECTRONIQUE_SIMPLE')%>"
								OnCallback="selectRadioButton"
						/>
						<label for="<%=$this->courrierElectroniqueSimple->getClientId()%>"><com:TTranslate>DEFINE_TEXT_COURIER_ELECTRONIQUE_SIMPLE</com:TTranslate></label><span class="info-aide indent-10"><com:TTranslate>DEFINE_TEXT_TAILLE_PIECE_JOINTE</com:TTranslate></span>
					</div>
					<div class="spacer-mini"></div>
					<div class="intitule-auto bloc-700">
						<com:TActiveRadioButton
								ID = "courrierElectroniqueContenuIntegralAR"
								GroupName="optionEnvoie"
								CssClass = "radio"
								Attributes.title = "<%=Prado::localize('DEFINE_TEXT_COURIER_ELECTRONIQUE_CONTENU_INTEGRAL')%>"
								OnCallback="selectRadioButton"
						/>
						<label for="<%=$this->courrierElectroniqueContenuIntegralAR->getClientId()%>"><com:TTranslate>DEFINE_TEXT_COURIER_ELECTRONIQUE_CONTENU_INTEGRAL</com:TTranslate></label> (<com:TTranslate>DEFINE_TEXT_Lien_RECEPTION</com:TTranslate>)<span class="info-aide indent-10"><com:TTranslate>DEFINE_TEXT_TAILLE_PIECE_JOINTE</com:TTranslate></span>
					</div>
					<div class="spacer-mini"></div>
					<div class="intitule-auto bloc-700">
						<com:TActiveRadioButton
								ID = "courrierElectroniqueUniquementLienTelechargementObligatoire"
								GroupName="optionEnvoie"
								CssClass = "radio"
								Attributes.title = "<%=Prado::localize('DEFINE_TEXT_COURIER_ELECTRONIQUE_TELECHARGEMENT')%>"
								OnCallback="selectRadioButton"
						/>
						<label for="<%=$this->courrierElectroniqueUniquementLienTelechargementObligatoire->getClientId()%>"><com:TTranslate>DEFINE_TEXT_COURIER_ELECTRONIQUE_TELECHARGEMENT_UNIQUE</com:TTranslate></label> <com:TTranslate>TEXT_PLATE_FORME_AR</com:TTranslate>
					</div>
					<div class="spacer-mini"></div>
					<div class="intitule-auto bloc-700">
						<com:TActiveRadioButton
								ID = "courrierElectroniqueDeDemandeDeComplement"
								GroupName="optionEnvoie"
								Enabled="true"
								CssClass = "radio"
								Attributes.title = "<%=Prado::localize('DEFINE_TEXT_COURIER_ELECTRONIQUE_DEMANDE_COMPLEMENT')%>"
								OnCallback="selectRadioButton"
						/>
						<label for="<%=$this->courrierElectroniqueDeDemandeDeComplement->getClientId()%>"><com:TTranslate>DEFINE_TEXT_COURIER_ELECTRONIQUE_DEMANDE_COMPLEMENT</com:TTranslate></label> <com:TTranslate>TEXT_PLATE_FORME_AR</com:TTranslate>
					</div>
					<div class="breaker"></div>
	</div>
			</div>
			<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
		</com:TPanel>
		<!--Fin Bloc Option d'envoi-->
		<div class="panel panel-primary">
			<div class="panel-heading"><span class="h4 panel-title"><com:TTranslate>TEXT_MESSAGE</com:TTranslate></span></div>
			<div class="panel-body">
				<com:TPanel ID="panelDestinataire" CssClass="line">
					<div class="intitule-110"><label for="<%=$this->destinataires->getClientId()%>"><com:TTranslate>DEFINE_TEXT_DESTINATAIRE</com:TTranslate></label> :</div>
					<div class="content-bloc bloc-660">
				<com:TLabel ID="adressesLibresHidden" />
				<com:TTextBox
						ID="destinataires"
							CssClass="form-control disabled"
						TextMode="MultiLine"
						Attributes.readonly = "true"
						Attributes.title = "<%=Prado::localize('DEFINE_TEXT_DESTINATAIRE')%>"
				/>
				<com:TRequiredFieldValidator
						ControlToValidate="destinataires"
						ValidationGroup="validateinfomessage"
						Display="Dynamic"
						ErrorMessage="<%=Prado::localize('DEFINE_TEXT_DESTINATAIRE')%>"
						EnableClientScript="true"
						Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
					<prop:ClientSide.OnValidationError>
						document.getElementById('divValidationSummary').style.display='';
					</prop:ClientSide.OnValidationError>
				</com:TRequiredFieldValidator>
				<com:THiddenField id="adresseRegistreRetrait" value="0"/>
				<com:THiddenField id="adresseRegistreQuestion" value="0"/>
				<com:THiddenField id="adresseRegistreDepot" value="0"/>
				<com:THiddenField id="adresseLibre" value="0"/>

				<com:TLinkButton
						ID="buttonEditdestinataire"
						CssClass="bouton-edit clear-both"
						onClick="editerDestinatairesMail"
						Attributes.title="<%=Prado::localize('DEFINE_TEXT_EDITER_LIST_DESTINATAIRE')%>"
						Text="<%=Prado::localize('DEFINE_TEXT_EDITER')%>">
				</com:TLinkButton>
			</div>
		</com:TPanel>
                <!--[if lte IE 6]>
                	<div class="spacer-mini"></div>
                <![endif]-->
				<div class="form-group form-group-sm col-md-12">
					<label for="<%=$this->objet->getClientId()%>" class="col-md-2"><com:TTranslate>OBJET</com:TTranslate>&nbsp;:</label>
					<div class="col-md-10">
						<com:TActiveTextBox
								ID="objet"
									CssClass="form-control"
								Attributes.title = "<%=Prado::localize('OBJET')%>"
						/>
					</div>
			</div>
				<div class="spacer-mini"></div>
				<div class="form-group form-group-sm col-md-12">
					<label for="<%=$this->textMail->getClientId()%>" class="col-md-2"><com:TTranslate>DEFINE_TEXT_TEXT</com:TTranslate>&nbsp;:</label>
					<div class="col-md-10">
						<com:TActiveTextBox
								ID="textMail"
									CssClass="form-control"
								TextMode="MultiLine"
								Attributes.title = "<%=Prado::localize('DEFINE_TEXT_TEXT')%>"
						/>
					</div>
			</div>
				<div class="spacer-mini"></div>
				<com:TPanel ID="panelCorpsAR" CssClass="form-group form-group-sm col-md-12" Visible="false">
					<label for="<%=$this->textMail->getClientId()%>" class="col-md-2"><com:TTranslate>DEFINE_TEXT_MSG_ORIGINE</com:TTranslate>&nbsp;:</label>
					<div class="col-md-10">
						<com:TTextBox
								ID="corpsAR"
								Attributes.disabled="disabled"
								CssClass="form-control"
								TextMode="MultiLine"
								Attributes.title="<%=Prado::localize('DEFINE_TEXT_LIST_ADRESSE_LIBRE')%>"
						/>
					</div>
		</com:TPanel>
				<div class="spacer-mini"></div>
				<div class="form-group form-group-sm col-md-12">
					<label for="<%=$this->objet->getClientId()%>" class="col-md-2"><com:TTranslate>DEFINE_TEXT_PIECE_JOINTE</com:TTranslate>&nbsp;:</label>
                        <com:TActivePanel CssClass="col-md-10" ID="PanelPJ">
							<com:TRepeater ID="RepeaterPiecesJointes" EnableViewState="true" DataKeyField="Id">
								<prop:ItemTemplate>
								<div class="picto-link inline">
										<a href="?page=Agent.DownloadPjEchange&idPj=<%#$this->Data->getPiece()%>"><%#$this->SourceTemplateControl->formaterNomFichierPj($this->Data)%><span class="info-aide"> (<%# Application\Service\Atexo\Atexo_Util::arrondirSizeFile($this->Data->getTaille()/1024)%>)</span></a>
										<com:TActiveLinkButton
											CssClass="indent-5"
												OnCallBack="Page.TemplateEnvoiCourrierElectronique.refreshRepeaterPJ"
												OnCommand="Page.TemplateEnvoiCourrierElectronique.onDeleteClick"
												CommandParameter="<%#$this->Data->getId()%>"
										>
											<com:TImage
												ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-supprimer.gif"
												AlternateText="<%=Prado::localize('DEFINE_SUPPRIMER')%>"
												Attributes.title="<%=Prado::localize('DEFINE_SUPPRIMER')%>"
												/>
										</com:TActiveLinkButton>
								</div>
								<br/><br/>
								</prop:ItemTemplate>
							</com:TRepeater>

						<com:TActiveHyperLink
							ID="buttonEditPj"
							NavigateUrl="javascript:popUpSetSize('?page=<%= Application\Service\Atexo\Atexo_Util::getTypeUserCalledForPradoPages() %>.popUpListePj&Idmsg=<%=$this->getViewState('idMessage')%>&org=<%=$this->_org%>','650px','400px','yes');"
							CssClass="bouton-edit clear-both"
							Attributes.title="<%=Prado::localize('DEFINE_TEXT_EDITER_LIST_PIECE_JOINTE')%>"
							Text="<%=Prado::localize('DEFINE_TEXT_EDITER')%>">
						</com:TActiveHyperLink>

		  				<div class="breaker"></div>
				</com:TActivePanel>
			<span style ="display:none;">
						<com:TActiveButton
								Id="validatePJ"
								onCallBack="refreshRepeaterPJ"
								ActiveControl.CallbackParameter=<%=$this->getViewState('idMessage')%>
							/>
					</span>
		</div>

				<com:TActivePanel ID="moduleDossierVolumineux" CssClass="line">
					<com:TActivePanel ID="panelSelectDossierVolumineux" CssClass="form-group form-group-sm col-md-12">
		<div class="spacer-mini"></div>
						<label for="<%=$this->objet->getClientId()%>" class="col-md-2"><com:TTranslate>DOSSIER_VOLUMINEUX</com:TTranslate>&nbsp;:</label>
						<div class="col-md-10">
							<com:TDropDownList
									ID="selectDossierVolumineux"
									CssClass="form-control"
									Attributes.title = "<%=Prado::localize('DOSSIER_VOLUMINEUX')%>"
							/>
						</div>
					</com:TActivePanel>
				</com:TActivePanel>
			<div class="breaker"></div>
		</div>

		<div class="panel-footer clearfix">
			<div class="pull-left">
				<com:TButton
						CssClass="btn btn-default btn-sm"
						Attributes.value="<%=Prado::localize('TEXT_ANNULER')%>"
						Attributes.title="<%=Prado::localize('DEFINE_TEXT_MAIL_RETOUR')%>"
						OnClick="RedirectReturn"
				/>
			</div>
			<div class="pull-right">
				<com:TButton
						CssClass="btn btn-primary btn-sm"
						Attributes.value="<%=Prado::localize('DEFINE_TEXT_ENVOYER')%>"
						Attributes.title="<%=Prado::localize('DEFINE_TEXT_ENVOIE_IMMEDIAT')%>"
						onClick="envoyer"
						ValidationGroup="validateinfomessage"
			id="envoyerMessage"
			Attributes.OnClick="javascript:document.getElementById('<%=$this->envoyerMessage->getClientId()%>').style.display='none';"
				/>
			</div>
		</div>
	</div>
		<!--Fin Bloc Message à envoyer-->

<!--Debut line boutons-->
<!--Fin line boutons-->
<div class="breaker"></div>