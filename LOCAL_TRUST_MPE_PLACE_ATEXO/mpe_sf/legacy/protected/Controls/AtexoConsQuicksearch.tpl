<!--Debut bloc recherche rapide -->
<com:TPanel DefaultButton="lancerRecherche" >
<div class="search section-search">
    <div class="container main-section">
        <h2 class="search-head text-center" style="color: black">
            <com:TTranslate>RECHERCHE_RAPIDE_ACCUEIL_ENTREPRISE</com:TTranslate>
        </h2>
        <div class="txt-search">
            <p class="introduction-search text-center col-md-8 col-md-offset-2">
                <com:TTranslate>DEFINE_MSG_RECHERCHE_CONS_LIEUX_EXEC_MAP</com:TTranslate>
            </p>
        </div>
        <div class="row center-content">
            <div class="col-md-6 col-xs-12 col-md-offset-1 col-xs-offset-0 hidden-xs">
                <div id="blocMap">
                    <div id="canvas" >
                        <div id="paper" data-tite="<%=Prado::localize('MESSAGE_CARTE_LIEUX_EXECUTION')%>"></div>
                    </div>
                </div>
            </div>

            <div class="col-md-4 col-xs-12 form-research form-group form-group-sm">
                <com:TPanel cssClass="clearfix hidden-xs" style="display: <%=( Application\Service\Atexo\Atexo_Config::getParameter('ACTIVER_OPTION_CARTE_PAGE_ACCUEIL'))?'':'none'%>">
                    <fieldset class="control-label">
                        <!--titre sélection par lieux d'exécution-->
                        <legend class="control-label"><com:TTranslate>DEFINE_SELECTIONNER_LIEUX_EXECUTION</com:TTranslate></legend>
                        <!-- radio par département-->
                        <com:TPanel cssClass="radio" id="subitemRadioGroup" Attributes.onclick="displaySubitemMap();">
                            <div class="radio">
                                <label for="optionsRadios2">
                                    <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2" checked>
                                    <com:TTranslate>DEFINE_PAR_DEPARTEMENTS</com:TTranslate>
                                </label>
                            </div>
                        </com:TPanel>

                        <!-- radio par région-->
                        <com:TPanel cssClass="radio" id="itemRadioGroup" Attributes.onclick="displayItemMap();">
                            <div class="radio">
                                <label for="optionsRadios1">
                                    <input type="radio" name="optionsRadios" id="optionsRadios1" value="option1">
                                    <com:TTranslate>DEFINE_PAR_REGIONS</com:TTranslate>
                                </label>
                            </div>
                        </com:TPanel>
                    </fieldset>
                </com:TPanel>
                <div class="clearfix">
                    <div class="form-group">
                        <label for="ctl0_CONTENU_PAGE_rechercheRapideCons_categorie"><com:TTranslate>DEFINE_CATEGORIE_PRINCIPAL</com:TTranslate>:</label>
                        <com:TDropDownList ID="categorie" CssClass="form-control" Attributes.title="<%=Prado::localize('DEFINE_CATEGORIE_PRINCIPAL')%>"/>
                    </div>
                </div>

                <div class="line">
                    <div class="form-group">
                        <label for="ctl0_CONTENU_PAGE_rechercheRapideCons_motsCle"><com:TTranslate>DEFINE_MOTS_CLES</com:TTranslate></label>:
                        <com:TTextBox ID="motsCle" Attributes.title="<%=Prado::localize('DEFINE_MOTS_CLES')%>" cssClass="form-control"/>
                    </div>
                </div>

                <div class="form-group">
                    <com:TButton id="lancerRecherche"
                                 CssClass="btn btn-primary btn-block"
                                 Text="<%=Prado::localize('DEFINE_LANCER_RECHERCHE')%>"
                                 Attributes.onClick="selectAllLocalisationsItems('listeLocalisationsSelect');"
                                 onClick="onSearchClick"
                                 Attributes.title="<%=Prado::localize('DEFINE_LANCER_RECHERCHE')%>"/>
                </div>

                <ul class="list-padding list-unstyled">
                    <li>
                        <a class="lien-recherche-avancee" href="?page=Entreprise.EntrepriseAdvancedSearch&amp;searchAnnCons" data-tnr="advancedSearch">
                            <span class="fa fa-chevron-right"></span>
                            <com:TTranslate>TEXT_RECHERCHE_AVANCEE</com:TTranslate>
                        </a>
                    </li>
                    <li>
                        <a class="lien-recherche-avancee" href="?page=Entreprise.EntrepriseAdvancedSearch&searchAnnCons&type=restreinte">
                            <span class="fa fa-chevron-right"></span>
                            <com:TTranslate>RECHERCHER_PROCEDURE_RESTREINTE</com:TTranslate>
                        </a>
                    </li>
                    <li>
                        <a class="lien-recherche-avancee" href="?page=Entreprise.EntrepriseAdvancedSearch&AllCons">
                            <span class="fa fa-chevron-right"></span>
                            <com:TTranslate>DEFINE_VOIR_TOUTES_CONSULTATIONS</com:TTranslate>
                        </a>
                    </li>
                    <li>
                        <a class="lien-recherche-avancee" href="?page=Entreprise.EntrepriseAdvancedSearch&AllAnn">
                            <span class="fa fa-chevron-right"></span>
                            <com:TTranslate>TEXT_RECHERCHE_AVANCEE_AUTRES_ANNONCES</com:TTranslate>
                        </a>
                    </li>
                </ul>
            </div>
        </div>


        <!-- Debut bloc liste de Regions et Departement selectionn?s-->
        <div class="bloc-selection-map" style="display:none;">
            <div class="intitule-auto bold"><label for="listeLocalisationsSelect"><com:TTranslate>RECHERCHE_RAPIDE_MA_SELECTION_ACTUELLE</com:TTranslate></label></div>
            <select class="selectpicker liste-region-selectionnes" multiple="multiple" id="listeLocalisationsSelect" name="localisations[]"></select>
            <div class="vider-liste"><a href="#"><com:TTranslate>RECHERCHE_RAPIDE_VIDER_LISTE</com:TTranslate></a></div>
            <div class="effacer-selection"><a href="#"><com:TTranslate>RECHERCHE_RAPIDE_CARTE_EFFACER</com:TTranslate></a></div>
        </div>
        <!-- Fin bloc liste de Regions et Departement selectionn?s-->
    </div>
</div>
</com:TPanel>
<!--Fin bloc recherche rapide -->