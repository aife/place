
		<com:TPanel ID="blocMessageConfirmation" visible="<%=$this->getMessageConfirmation()%>" cssClass="form-bloc-conf">
			<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
			<div class="content"> <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-ok-big.gif" class="picto-confirmation" alt="Confirmation" />
				<div class="message bloc-700">
					<!--Debut Message type d'erreur general-->
					<div class="line">
						<div class="intitule-auto"><%=Prado::localize('REPONSE_BIEN_ENREGISTRE')%>.</div>
					</div>
					<com:TPanel id="blocHorodatage" cssClass="line">
						<div class="intitule-auto"><%=Prado::localize('HORODATAGE_DEPOT')%> : </div>
						<span id="horodatage"><com:TActiveLabel ID="labelHorodatage" /></span> 
					</com:TPanel>
					<div class="spacer-small"></div>
					<div class="breaker"></div>
					<!--Fin Message type d'erreur general-->
				</div>
				<div class="breaker"></div>
			</div>
			<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
		</com:TPanel>
		
		
		<!--Debut Bloc Pieces a signer-->
		<div class="form-field">
			<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
			<div class="content">
				<com:TPanel visible="<%=($this->getMessageConfirmation()=='true')? false:true%>" cssClass="line">
					<div class="intitule-100 bold"><com:TTranslate>DEFINE_DEPOSE_LE</com:TTranslate> : </div>
					<div class="content-bloc bloc-600"><%=$this->getDateDepotOffre()%></div>
				</com:TPanel>
				<div class="line">
					<div class="intitule-100 bold"><com:TTranslate>PAR</com:TTranslate> : </div>
					<div class="content-bloc bloc-600"><%=Application\Service\Atexo\Entreprise\Atexo_Entreprise_Inscrit::getNomPrenomEmailInscrit(Application\Service\Atexo\Atexo_CurrentUser::getIdInscrit())%></div>
				</div>
				<div class="line">
					<div class="intitule-100 bold"><com:TTranslate>TEXT_ENTREPRISE</com:TTranslate> : </div>
					<div class="content-bloc bloc-600"><%=Application\Service\Atexo\Atexo_Entreprise::getNomEntreprise(Application\Service\Atexo\Atexo_CurrentUser::getIdEntreprise())%></div>
				</div>
				<div class="line">
					<com:TLabel ID="labelContenuTransmis" visible="<%=$this->getMessageConfirmation()%>" ><h3><%=Prado::localize('CONTENU_TRANSMIS')%> </h3></com:TLabel>
				</div>
				<div class="table-bloc">
				<!--Debut tableau liste des contenus transmis-->
				<com:TRepeater ID="repeaterConfirmationDepotPli" >
					<prop:HeaderTemplate>
						<table class="table-results" summary="Liste des contenus transmis">
							<caption><%=Prado::localize('CONTENU_TRANSMIS')%> </caption>
							<thead>
								<tr>
									<th class="top" colspan="3"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></th>
								</tr>
								<tr>
									<th class="col-250" id="piece"><%=Prado::localize('DEFINE_TEXT_PIECE')%></th>
									<th class="col-350" id="nomFichier"><%=Prado::localize('NOM_DU_FICHIER')%></th>
									<th class="actions" id="actionsFichier"><%=Prado::localize('DEFINE_TEXT_SIGNATURE')%></th>
								</tr>
							</thead>
						</prop:HeaderTemplate>
    					<prop:ItemTemplate>
							
								<tr class=<%#(($this->ItemIndex%2==0)? '':'on')%>>
									<td class="col-250" headers="nomFichier"><%#$this->Data["piece"]%></td>
									<td class="col-350" headers="nomFichier"><%#$this->Data["nomFichier"]%></td>
									<td class="actions" headers="actionsFichier" style="display:<%#($this->Parent->Parent->getMessageConfirmation()=='true')? '':'none;'%>">
										<a href="<%#$this->Data["lien"]%>" style="display:<%#($this->Data["signature"]==1)?';':'none;"'%>" title="<%=Prado::localize('DEFINE_TEXT_TELECHARGER_FICHIER_SIGNATURE')%> <%#$this->Data["nomFichier"]%>"><img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-dl-dossier.gif" alt="<%=Prado::localize('DEFINE_TEXT_TELECHARGER_FICHIER_SIGNATURE')%> <%#$this->Data["nomFichier"]%>" /></a>
									</td>
									<td class="actions" headers="actionsFichier" style="display:<%#($this->Parent->Parent->getMessageConfirmation()=='true')? 'none;':''%>">
										<com:TImageButton visible="<%#($this->Data['signature'])? true:false%>" ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-dl-dossier.gif" CommandName="<%#$this->Data['nomFichier'].'|'.$this->Data['signature'].'|'.$this->Data['dateSignature']%>" attributes.title="<%=Prado::localize('DEFINE_TELECHARGER')%>" attributes.alt="<%=Prado::localize('DEFINE_TELECHARGER')%>" OnCommand="Page.downloadSignature" />
									</td>
								</tr>
						</prop:ItemTemplate>
	            		<prop:FooterTemplate>
					    </table>
						</prop:FooterTemplate>
                </com:TRepeater>
				</div>
				<!--Fin tableau liste des contenus transmis-->
				<div class="breaker"></div>
			</div>
			<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
		</div>
		<!--Fin Bloc Pieces a signer-->
		<!--Fin Bloc formulaire avec onglets-->
		<div class="breaker"></div>
