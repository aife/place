<com:AtexoBlocContratMulti id="blocContratMulti" />
<com:TActivePanel cssClass="form-field" id="panelInfoContrat">
    <div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
    <div class="content">
        <h2 class="float-left"><com:TTranslate>INFORMATION_CONTRAT</com:TTranslate></h2>
        <div class="float-right margin-fix"><com:TTranslate>DEFINE_SYMBOLE</com:TTranslate> <span class="champ-oblig">*</span> <com:TTranslate>DEFINE_INDIQUE_CHAMPS_OBLIGATOIRES</com:TTranslate></div>
        <!--Debut Bloc Formulaire -->
        <div class="creation-form donnees-contrats">
            <div class="panel">
                <com:TActivePanel ID="panelContratSansChapeau" >

                    <!--Debut nature de contrat-->
                    <com:TActivePanel cssClass="line" id="panelNatureConcession">
                        <div class="intitule col-150"><label for="natureContratConcession"><com:TTranslate>NATURE_CONTRAT_CONCESSION</com:TTranslate></label><span class="champ-oblig">*</span> :</div>
                        <div class="content-bloc margin-bottom-15">
                            <com:TActiveDropDownList
                                    id="natureContratConcession"
                                    CssClass="moyen float-left"
                                    Attributes.title="<%=Prado::localize('NATURE_CONTRAT_CONCESSION')%>"
                            />
                            <com:TCompareValidator
                                    ControlToValidate="natureContratConcession"
                                    ValueToCompare="0"
                                    DataType="String"
                                    Operator="NotEqual"
                                    ValidationGroup="Validation"
                                    Display="Dynamic"
                                    ErrorMessage="<%=Prado::localize('NATURE_CONTRAT_CONCESSION')%>"
                                    EnableClientScript="<%=$this->isTypeContratConcession()%>"
                                    Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
                            >
                                <prop:ClientSide.OnValidationError>
                                    document.getElementById('divValidationSummary').style.display='';
                                    document.getElementById('<%=$this->page->buttonValidate->getClientId()%>').style.display='';
                                </prop:ClientSide.OnValidationError>
                            </com:TCompareValidator>
                            <a class="picto-info-intitule" onblur="cacheBulle('infosnatureContratConcession')" onfocus="afficheBulle('infosnatureContratConcession', this)" onmouseout="cacheBulle('infosnatureContratConcession')"
                               onmouseover="afficheBulle('infosnatureContratConcession', this)" >
                                <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" />
                            </a>
                        </div>
                        <div id="infosnatureContratConcession" class="info-bulle" onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();">
                            <div><com:TTranslate>INFOBULLE_NATURE_CONTRAT_CONCESSION</com:TTranslate></div>
                        </div>
                    </com:TActivePanel>
                    <!--Fin Nature de contrat-->

                    <!--Debut Intitulé-->
                    <div class="line">
                        <div class="intitule col-150"><label for="intitule"><com:TTranslate>TEXT_INTITULE_CONTRAT</com:TTranslate></label> :</div>
                        <div class="content-bloc">
                            <com:TTextBox id="intitule" cssclass="long float-left" attributes.title="<%=Prado::localize('TEXT_INTITULE_CONTRAT')%>" attributes.maxlength="<%= Application\Service\Atexo\Atexo_Config::getParameter('MAXLENGTH_INTITULE_CONTRAT')%>"/>
                        </div>
                    </div>
                    <!--Fin Intitulé-->

                    <!--Debut Bloc Objet-->
                    <div class="line">
                        <div class="intitule col-150">
                            <label for="objetMarche_lot_1_attributaire_1"><com:TActiveLabel ID="labelObjetMarche" /></label><span class="champ-oblig">*</span>:
                        </div>
                        <com:TTextBox id="objetMarche"
                                      TextMode="MultiLine"
                                      attributes.maxlength="255"
                                      cssClass="long-550 input-propre"
                        />
                        <com:TRequiredFieldValidator
                                ControlToValidate="objetMarche"
                                ValidationGroup="Validation"
                                Display="Dynamic"
                                EnableClientScript="true"
                                ErrorMessage="<%=Prado::localize('DEFINE_OBJET_MARCHE')%>"
                                Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
                            <prop:ClientSide.OnValidationError>
                                document.getElementById('divValidationSummary').style.display='';
                                document.getElementById('<%=$this->page->buttonValidate->getClientId()%>').style.display='';
                            </prop:ClientSide.OnValidationError>
                        </com:TRequiredFieldValidator>
                    </div>
                    <!-- Fin Bloc Objet-->
                    <!--Debut Bloc Procedure passation-->
                    <com:TActivePanel cssClass="line" id="panelProcedurePassation">
                        <div class="intitule col-150">
                            <label for="procedurePassation"><com:TTranslate>TEXT_PROCEDURE_PASSATION</com:TTranslate></label><span class="champ-oblig">*</span>:
                        </div>
                        <com:TActiveDropDownList
                                id="procedurePassation"
                                CssClass="select-320"
                                Attributes.title="<%=Prado::localize('TEXT_PROCEDURE_PASSATION')%>"
                        />
                        <com:TCompareValidator
                                ControlToValidate="procedurePassation"
                                ValueToCompare="0"
                                DataType="String"
                                Operator="NotEqual"
                                ValidationGroup="Validation"
                                Display="Dynamic"
                                ErrorMessage="<%=Prado::localize('TEXT_PROCEDURE_PASSATION')%>"
                                EnableClientScript="<%=$this->isTypeContratConcession()%>"
                                Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
                        >
                            <prop:ClientSide.OnValidationError>
                                document.getElementById('divValidationSummary').style.display='';
                                document.getElementById('<%=$this->page->buttonValidate->getClientId()%>').style.display='';
                            </prop:ClientSide.OnValidationError>
                        </com:TCompareValidator>

                    </com:TActivePanel>
                    <!-- Fin Bloc Procedure passation-->


                    <!-- Debut Lieu Principal d'exécution -->
                    <div class="line line-lieuPrincipalExecution">
                        <div class="intitule col-150">
                            <label for="lieuPrincipalExecution"><com:TTranslate>TEXT_LIEU_PRINCIPAL_EXECUTION</com:TTranslate></label><span class="champ-oblig">*</span>:
                        </div>
                        <com:TJuiAutoComplete
                                ID="lieuPrincipalExecution"
                                OnSuggest="suggestLieux"
                                Suggestions.DataKeyField="id"
                                ResultPanel.CssClass="acomplete"
                                cssClass="long"
                                MinChars="2"
                                OnSuggestionSelected="suggestionSelected"
                                Attributes.title="<%=Prado::localize('TEXT_LIEU_PRINCIPAL_EXECUTION')%>"
                                Attributes.placeholder="<%=Prado::localize('TEXT_RECHERCHE_DEPARTEMENT')%>"
                        >
                            <prop:Suggestions.ItemTemplate>
                                <li title="<%#$this->Data['denomination']%>"><%# $this->Data['denomination']%></li>
                            </prop:Suggestions.ItemTemplate>
                        </com:TJuiAutoComplete>
                        <com:TRequiredFieldValidator
                                ValidationGroup="Validation"
                                ControlToValidate="lieuPrincipalExecution"
                                Display="Dynamic"
                                ErrorMessage="<%=Prado::localize('TEXT_LIEU_PRINCIPAL_EXECUTION')%>"
                                Text="<span title='<%=Prado::localize('TEXT_LIEU_PRINCIPAL_EXECUTION')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' /></span>" >
                            <prop:ClientSide.OnValidationError>
                                document.getElementById('divValidationSummary').style.display='';
                            </prop:ClientSide.OnValidationError>
                        </com:TRequiredFieldValidator>
                        <a class="picto-info-intitule" onblur="cacheBulle('infosLieuPrincipalExecution')" onfocus="afficheBulle('infosLieuPrincipalExecution', this)" onmouseout="cacheBulle('infosLieuPrincipalExecution')"
                           onmouseover="afficheBulle('infosLieuPrincipalExecution', this)" >
                            <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" />
                        </a>
                        <div id="infosLieuPrincipalExecution" class="info-bulle" onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();">
                            <div><com:TTranslate>INFO_BULLE_LIEU_PRINCIPAL_EXECUTION</com:TTranslate></div>
                        </div>
                    </div>
                    <!-- Fin Lieu Principal d'exécution -->
                    <!--Debut Bloc Numéro-->
                    <com:TActivePanel cssClass="line" id="panelNumero">
                        <div class="intitule-150"><label for="numero"><com:TActiveLabel ID="labelNumero" /></label> :</div>
                        <div class="content-bloc-auto">
                            <com:TActiveTextBox cssClass="input-250 float-left-fix disabled" ID="numero" enabled="false" />
                        </div>
                    </com:TActivePanel>
                    <!--Debut Bloc Montant-->
                    <com:TActivePanel cssClass="line" id="panelMontantMax">
                        <div class="intitule-150"><label for="montantMaxContrat"><com:TTranslate>MONTANT_MAX_ESTIME</com:TTranslate></label><span class="champ-oblig">*</span> :</div>
                        <div class="content-bloc-auto">
                            <com:TTextBox ID="montantMaxContrat"  Attributes.title="<%=Prado::Localize('MONTANT_MAX_ESTIME')%>"
                                          CssClass="input-100 float-left-fix"
                                          Attributes.onblur="formatterMontant('<%=$this->montantMaxContrat->getClientId()%>');"
                            />
                            <div class="intitule-bloc"><com:TTranslate>DEFINE_UNITE_HT</com:TTranslate></div>
                        </div>
                    </com:TActivePanel>
                    <div class="spacer-mini"></div>
                    <!--Fin Bloc Montant-->
                    <!--Debut Ligne Nature prestations-->
                    <div class="line">
                        <div class="intitule col-150">
                            <label for="naturePrestations_lot_1_attributaire_1"><com:TTranslate>NATURE_DES_PRESTATIONS</com:TTranslate></label><span class="champ-oblig">*</span>:</div>
                        <com:TDropDownList
                                id="naturePrestations"
                                CssClass="select-320 cpvCategoryCheck"
                                Attributes.title="<%=Prado::localize('NATURE_DES_PRESTATIONS')%>"
                        />
                        <com:TCompareValidator  ControlToValidate="naturePrestations"  ValueToCompare="0"  DataType="String" Operator="NotEqual" ValidationGroup="validerNotification" Display="Dynamic"
                                                ErrorMessage="<%=Prado::localize('NATURE_DES_PRESTATIONS')%>"
                                                EnableClientScript="true"
                                                Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
                        >
                            <prop:ClientSide.OnValidationError>
                                document.getElementById('divValidationSummary').style.display='';
                                document.getElementById('<%=$this->page->buttonValidate->getClientId()%>').style.display='';
                            </prop:ClientSide.OnValidationError>
                        </com:TCompareValidator>
                    </div>
                    <!--Fin Ligne Nature prestations-->
                    <!--Debut Ligne CCAG-->
                    <div class="line">
                        <div class="intitule col-150"><label for="ccag_lot_1_attributaire_1"><com:TTranslate>FCSP_CCAG_APPLICABLE</com:TTranslate></label><span class="champ-oblig">*</span>:</div>
                        <com:TActiveDropDownList
                                id="ccag"
                                CssClass="select-320"
                                Attributes.title="<%=Prado::localize('FCSP_CCAG_APPLICABLE')%>"
                        />
                    </div>
                    <!--Fin Ligne CCAG-->
                    <div class="spacer-small"></div>
                    <!--Debut Ligne CPV-->
                    <com:AtexoCodesCpv id="referentielCPV" titre="<%=Prado::localize('TEXT_CODE_CPV')%>"/>
                    <!--Fin Ligne CPV-->
                    <div class="spacer-small"></div>
                    <!--Achat responsable -->
                    <com:AchatResponsableContrat id="achatResponsableConsultation" mode="1" idElement="clauseContrat" visible="<%=Application\Service\Atexo\Atexo_Module::isEnabled('ConsultationClause') %>" />
                    <!--Debut Bloc Montant-->
                    <div class="spacer-small"></div>
                </com:TActivePanel>
                <com:TActivePanel ID="panelMontantContrat" >
                    <!--Debut Bloc Montant-->
                    <div class="line">
                        <div class="intitule col-150">
                            <com:TLabel id="montant_lot_1_attributaire_1">
                                <com:TTranslate>MONTANT_MARCHE</com:TTranslate>
                                <span class="champ-oblig">*</span> :
                            </com:TLabel>
                            <com:TLabel id="montant_lot_1_attributaire_1_concession" style="display:none">
                                <com:TTranslate>DEFINE_TEXT_MONTANT_CONTRAT_VALEUR_GLOBALE_CONCESSION</com:TTranslate>
                                <span class="champ-oblig">*</span> :
                            </com:TLabel>
                        </div>
                        <div class="content-bloc-auto bloc-180 float-left">
                            <com:TActiveTextBox ID="MontantMarche"
                                                AutoPostBack="true"
                                                Attributes.title="<%=Prado::Localize('MONTANT')%>"
                                                CssClass="input-100 float-left-fix montant-formatter"
                                                Attributes.onblur="formatterMontant('<%=$this->MontantMarche->getClientId()%>');"
                                                OnTextChanged="changeTrancheBudgetaire"
                            >
                                <prop:ClientSide.OnLoading>showLoader();</prop:ClientSide.OnLoading>
                                <prop:ClientSide.OnFailure>showLoader();</prop:ClientSide.OnFailure>
                                <prop:ClientSide.OnSuccess>showLoader();</prop:ClientSide.OnSuccess>
                            </com:TActiveTextBox>

                            <div class="intitule-bloc"><com:TTranslate>DEFINE_UNITE_HT</com:TTranslate></div>
                            <com:TCustomValidator
                                    ControlToValidate="MontantMarche"
                                    ValidationGroup="Validation"
                                    Display="Dynamic"
                                    ClientValidationFunction="ValidateMontantContrat"
                                    ErrorMessage="<%=Prado::localize('MONTANT_MARCHE')%>"
                                    Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
                                    EnableClientScript="true" >
                                <prop:ClientSide.OnValidationError>
                                    document.getElementById('divValidationSummary').style.display='';
                                    document.getElementById('<%=$this->page->buttonValidate->getClientId()%>').style.display='';
                                </prop:ClientSide.OnValidationError>
                            </com:TCustomValidator>
                            <com:TPanel cssClass="col-info-bulle" id="info_bulle_valeur_globale_estimee" style="display: none">
                                <img title="Info-bulle"
                                     alt="Info-bulle"
                                     class="picto-info-intitule"
                                     onmouseout="cacheBulle('infosValeurGlobaleEstimee')"
                                     onmouseover="afficheBulle('infosValeurGlobaleEstimee', this)"
                                     src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif"
                                />
                                <div id="infosValeurGlobaleEstimee"
                                     class="info-bulle"
                                     onmouseover="mouseOverBulle();"
                                     onmouseout="mouseOutBulle();">
                                    <div><com:TTranslate>TEXT_INFO_BULLE_VALEUR_GLOBALE_ESTIMEE</com:TTranslate></div>
                                </div>
                            </com:TPanel>
                        </div>
                    </div>
                    <div class="spacer"></div>
                    <!--Fin Bloc Montant-->
                    <!--Debut Ligne Tranche budgetaire-->
                    <com:TActivePanel cssClass="line" visible="<%=Application\Service\Atexo\Atexo_Module::isEnabled('DecisionTrancheBudgetaire')%>">
                        <div class="intitule col-150">
                            <label for="trancheBudgetaire"><com:TTranslate>TRANCHE_BUDGETAIRE</com:TTranslate></label> :
                            <img  class="picto-info-intitule" onmouseout="cacheBulle('montantMarche')" onmouseover="afficheBulle('montantMarche', this)" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" />
                            <div id="montantMarche" class="info-bulle" onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();"><div><com:TTranslate>MONTANT_MARCHE_INFOBULLE</com:TTranslate></div></div>
                        </div>
                        <div class="content-bloc-auto">
                            <com:TActiveTextBox ID="trancheBudgetaire"
                                                Attributes.title="<%=Prado::Localize('TRANCHE_BUDGETAIRE')%>"
                                                CssClass="bloc-320"
                                                Enabled="false"

                            />
                        </div>
                    </com:TActivePanel>
                    <!--Fin Ligne Tranche budgetaire-->
                </com:TActivePanel>

                <!--Debut Ligne Forme du prix-->
                <com:TActivePanel ID="panelFormePrix">
                    <div class="line">
                        <div class="intitule col-170"><label for="formePrix"><com:TTranslate>MODALITE_REVISION_PRIX</com:TTranslate></label><span class="champ-oblig">*</span> :</div>
                        <div class="content-bloc-auto">
                            <div class="intitule-auto">
                                <com:TActiveRadioButton GroupName="formePrix" id="formePrix_ferme" cssclass="radio" /><label for="formePrix_ferme"><com:TTranslate>DEFINE_FERME</com:TTranslate></label>
                            </div>
                            <div class="intitule-auto">
                                <com:TActiveRadioButton GroupName="formePrix" id="formePrix_ferme_actualisable" cssclass="radio" /><label for="formePrix_ferme_actualisable"><com:TTranslate>TEXT_FERME_ACTUALISABLE</com:TTranslate></label>
                            </div>
                            <div class="intitule-auto">
                                <com:TActiveRadioButton GroupName="formePrix" id="formePrix_revisable" cssclass="radio" /><label for="formePrix_revisable"><com:TTranslate>TEXT_REVISABLE</com:TTranslate></label>
                            </div>
                            <com:TCustomValidator
                                    ValidationGroup="Validation"
                                    ControlToValidate="formePrix_ferme"
                                    ClientValidationFunction="validateFormePrix"
                                    Display="Dynamic"
                                    EnableClientScript="true"
                                    ErrorMessage="<%=Prado::localize('MODALITE_REVISION_PRIX')%>"
                                    Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
                                <prop:ClientSide.OnValidationError>
                                    document.getElementById('divValidationSummary').style.display='';
                                    document.getElementById('<%=$this->page->buttonValidate->getClientId()%>').style.display='';
                                </prop:ClientSide.OnValidationError>
                            </com:TCustomValidator>
                        </div>
                    </div>
                    <div class="spacer"></div>
                </com:TActivePanel>
                <!--Fin Ligne Forme du prix-->
                <!--Debut Ligne montant Subvention-->
                <com:TActivePanel ID="panelMontantSubvention">
                    <div class="line">
                        <div class="intitule col-150"><label for="montantSubvention">
                                <com:TTranslate>MONTANT_SUBVENTION_PUBLIQUE</com:TTranslate></label><span class="champ-oblig">*</span> :
                        </div>
                        <div class="content-bloc bloc-180 float-left">
                            <com:TActiveTextBox ID="montantSubvention"
                                                Attributes.title="<%=Prado::Localize('MONTANT_SUBVENTION_PUBLIQUE')%>"
                                                CssClass="input-100 float-left-fix montant-formatter"
                                                Attributes.onblur="formatterMontant('<%=$this->montantSubvention->getClientId()%>');"
                            />

                            <div class="intitule-bloc"><com:TTranslate>DEFINE_UNITE_HT</com:TTranslate></div>
                            <a class="picto-info-intitule" onblur="cacheBulle('infoMontantSubvention')" onfocus="afficheBulle('infoMontantSubvention', this)" onmouseout="cacheBulle('infoMontantSubvention')"
                               onmouseover="afficheBulle('infoMontantSubvention', this)" >
                                <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" />
                            </a>
                        </div>
                        <div id="infoMontantSubvention" class="info-bulle" onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();">
                            <div><com:TTranslate>INFOBULLE_MONTANT_SUBVENTION_PUBLIQUE</com:TTranslate></div>
                        </div>
                    </div>
                    <div class="spacer"></div>
                </com:TActivePanel>
                <!--Fin Ligne  montant Subvention-->
                <com:TActivePanel ID="panelMarcheDefense" visible="<%=Application\Service\Atexo\Atexo_Module::isEnabled('MarcheDefense',Application\Service\Atexo\Atexo_CurrentUser::getOrganismAcronym()) %>" >
                    <div class="line">
                        <div class="intitule col-170"><label for="marcheDefense"><com:TTranslate>MARCHE_DEFENSE_OU_SECURITE</com:TTranslate></label>&nbsp;<span class="champ-oblig">*</span>&nbsp;:</div>
                        <div class="content-bloc-auto">
                            <div class="intitule-auto">
                                <com:TActiveRadioButton GroupName="marcheDefense" id="marcheDefense_Oui" cssclass="radio" onCheckedChanged="marcheDefenseChanged"/><label for="formePrix_ferme"><com:TTranslate>DEFINE_OUI</com:TTranslate></label>
                            </div>
                            <div class="intitule-auto">
                                <com:TActiveRadioButton GroupName="marcheDefense" id="marcheDefense_Non" cssclass="radio" onCheckedChanged="marcheDefenseChanged"/><label for="formePrix_ferme_actualisable"><com:TTranslate>DEFINE_NON</com:TTranslate></label>
                            </div>
                        </div>
                        <com:TCustomValidator
                                ValidationGroup="Validation"
                                ControlToValidate="marcheDefense_Oui"
                                ClientValidationFunction="validateMarcheDefense"
                                Display="Dynamic"
                                EnableClientScript="<%=Application\Service\Atexo\Atexo_Module::isEnabled('MarcheDefense',Application\Service\Atexo\Atexo_CurrentUser::getOrganismAcronym())?true:false%>"
                                ErrorMessage="<%=Prado::localize('MARCHE_DEFENSE_OU_SECURITE')%>"
                                Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
                            <prop:ClientSide.OnValidationError>
                                document.getElementById('divValidationSummary').style.display='';
                                document.getElementById('<%=$this->page->buttonValidate->getClientId()%>').style.display='';
                            </prop:ClientSide.OnValidationError>
                        </com:TCustomValidator>
                    </div>
                    <div class="spacer"></div>
                </com:TActivePanel>
                <com:TActivePanel ID="panelMarcheInnovant">
                    <div class="line">
                        <div class="intitule col-170">
                            <label for="marcheInnovant">
                                <com:TTranslate>MARCHE_INNOVANT</com:TTranslate>
                            </label>&nbsp;
                            <span class="champ-oblig">*</span>&nbsp;
                            <img  class="picto-info-intitule" onmouseout="cacheBulle('bulleMarcheInnovant')" onmouseover="afficheBulle('bulleMarcheInnovant',this)" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" />
                            :
                            <div id="bulleMarcheInnovant" class="info-bulle" onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();"><div><com:TTranslate>MARCHE_INNOVANT_INFOBULLE</com:TTranslate></div></div>

                        </div>
                        <div class="content-bloc-auto">
                            <div class="intitule-auto">
                                <com:TActiveRadioButton GroupName="marcheInnovant" id="marcheInnovant_Oui" cssclass="radio"/><label for="formePrix_ferme"><com:TTranslate>DEFINE_OUI</com:TTranslate></label>
                            </div>
                            <div class="intitule-auto">
                                <com:TActiveRadioButton GroupName="marcheInnovant" id="marcheInnovant_Non" cssclass="radio"/><label for="formePrix_ferme_actualisable"><com:TTranslate>DEFINE_NON</com:TTranslate></label>
                            </div>
                        </div>
                        <com:TCustomValidator
                                ValidationGroup="Validation"
                                ControlToValidate="marcheInnovant_Oui"
                                Display="Dynamic"
                                ErrorMessage="<%=Prado::localize('MARCHE_INNOVANT')%>"
                                Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
                            <prop:ClientSide.OnValidationError>
                                document.getElementById('divValidationSummary').style.display='';
                                document.getElementById('<%=$this->page->buttonValidate->getClientId()%>').style.display='';
                            </prop:ClientSide.OnValidationError>
                        </com:TCustomValidator>
                    </div>
                    <div class="spacer"></div>
                </com:TActivePanel>
                <com:TPanel id="article133">
                    <h3>
                        <com:TTranslate>TEXT_PUBLICATION_CONTRAT</com:TTranslate>
                        <img  class="picto-info-intitule" onmouseout="cacheBulle('donnesEssentielles')" onmouseover="afficheBulle('donnesEssentielles', this)" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" />
                        <div id="donnesEssentielles" class="info-bulle" onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();"><div><com:TTranslate>DONNEES_ESSENTIELLES_INFOBULLE</com:TTranslate></div></div>
                    </h3>


                    <div class="line">
                        <label class="intitule-auto">
                            <com:TActiveRadioButton id="art133Publie" GroupName="artile133"
                                                    cssclass="check" checked="true"
                            />
                            <com:TTranslate>PUBLIER_DE</com:TTranslate>
                        </label>
                    </div>
                    <div class="line">
                        <label class="intitule-auto">
                            <com:TActiveRadioButton id="art133NonPublie" GroupName="artile133"
                                                    cssclass="check"
                            />
                            <com:TTranslate>NE_PAS_PUBLIER_DE</com:TTranslate></label>
                    </div>
                </com:TPanel>
                <!--Fin panel-->


            </div>
        </div>
        <!--Fin Bloc Formulaire -->

        <div class="breaker"></div>
    </div>
    <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
</com:TActivePanel>
<!--Fin Bloc Infos contrat-->

<!--Debut Bloc Dates-->
<div class="form-field">
    <div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
    <div class="content">
        <h2 class="float-left"><com:TTranslate>DEFINE_DATES</com:TTranslate></h2>
        <div class="float-right margin-fix"><com:TTranslate>DEFINE_SYMBOLE</com:TTranslate> <span class="champ-oblig">*</span> <com:TTranslate>DEFINE_INDIQUE_CHAMPS_OBLIGATOIRES</com:TTranslate></div>
        <div class="creation-form donnees-contrats">
            <div class="panel">
                <!--Debut Ligne Date prévisionnelle de notification-->
                <div class="line">
                    <div class="intitule-250"><label for="dateNotification"><com:TTranslate>DATE_PREVISIONNELLE_NOTIFICATION</com:TTranslate></label><span class="champ-oblig">*</span> : </div>

                    <div class="calendar">
                        <com:TActiveTextBox
                                AutoPostBack="true"
                                ID="dateNotification"
                                Attributes.title="<%=Prado::localize('DATE_PREVISIONNELLE_NOTIFICATION')%>"
                                CssClass="heure"
                                OnTextChanged="changeTrancheBudgetaire"
                        >
                            <prop:ClientSide.OnLoading>showLoader();</prop:ClientSide.OnLoading>
                            <prop:ClientSide.OnFailure>showLoader();</prop:ClientSide.OnFailure>
                            <prop:ClientSide.OnSuccess>showLoader();</prop:ClientSide.OnSuccess>
                        </com:TActiveTextBox>
                        <com:TImage id="calendarDateNotification" ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-calendar.gif" Attributes.alt="<%=Prado::localize('DEFINE_CALENDRIER')%>" Attributes.title="<%=Prado::localize('DEFINE_CALENDRIER')%>" Attributes.onclick="popUpCalendar(this,document.getElementById('<%= $this->dateNotification->getClientID()%>'),'dd/mm/yyyy','');" />
                        <com:TRequiredFieldValidator
                                ControlToValidate="dateNotification"
                                ValidationGroup="Validation"
                                Display="Dynamic"
                                EnableClientScript="true"
                                ErrorMessage="<%=Prado::localize('DATE_PREVISIONNELLE_NOTIFICATION')%>"
                                Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
                            <prop:ClientSide.OnValidationError>
                                document.getElementById('divValidationSummary').style.display='';
                                document.getElementById('<%=$this->page->buttonValidate->getClientId()%>').style.display='';
                            </prop:ClientSide.OnValidationError>
                        </com:TRequiredFieldValidator>
                        <com:TDateValidator
                                Display="Dynamic"
                                ValidationGroup="Validation"
                                ControlToValidate="dateNotification"
                                ErrorMessage="<%=Prado::localize('DATE_PREVISIONNELLE_NOTIFICATION')%>"
                                Text="<span title='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' /></span>" >
                            <prop:ClientSide.OnValidationError>
                                document.getElementById('divValidationSummary').style.display='';
                                document.getElementById('<%=$this->page->buttonValidate->getClientId()%>').style.display='';
                            </prop:ClientSide.OnValidationError>
                        </com:TDateValidator>
                    </div>
                    <img  class="picto-info-intitule" onmouseout="cacheBulle('bulleDatePrevisionnelle')" onmouseover="afficheBulle('bulleDatePrevisionnelle',this)" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" />
                    <div class="info-aide-right indent-20"><com:TTranslate>DEFINE_FORMAT_DATE_SIMPLE</com:TTranslate></div>
                </div>
                <!--Fin Ligne Date prévisionnelle de notification-->



                <!--Fin Ligne Date prévisionnelle de fin de marché-->
                <div class="line">
                    <div class="intitule-250">
                        <label for="datePrevisionnelleFinMarche"><com:TTranslate>DATE_PREVISIONNELLE_FIN_MARCHE</com:TTranslate></label>
                        <span style="display:Dynamic" class="champ-oblig">*</span> : </div>
                    <div class="calendar">
                        <com:TTextBox
                                ID="datePrevisionnelleFinMarche"
                                Attributes.title="<%=Prado::localize('DATE_PREVISIONNELLE_FIN_MARCHE')%>"
                                CssClass="heure"
                        />
                        <com:TImage id="calendarDateFin" ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-calendar.gif" Attributes.alt="<%=Prado::localize('DEFINE_CALENDRIER')%>" Attributes.title="<%=Prado::localize('DEFINE_CALENDRIER')%>" Attributes.onclick="popUpCalendar(this,document.getElementById('<%= $this->datePrevisionnelleFinMarche->getClientId()%>'),'dd/mm/yyyy','');" />
                        <com:TRequiredFieldValidator
                                ControlToValidate="datePrevisionnelleFinMarche"
                                ValidationGroup="Validation"
                                Display="Dynamic"
                                EnableClientScript="true"
                                ErrorMessage="<%=Prado::localize('DATE_PREVISIONNELLE_FIN_MARCHE')%>"
                                Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
                            <prop:ClientSide.OnValidationError>
                                document.getElementById('divValidationSummary').style.display='';
                                document.getElementById('<%=$this->page->buttonValidate->getClientId()%>').style.display='';
                            </prop:ClientSide.OnValidationError>
                        </com:TRequiredFieldValidator>
                        <com:TDateValidator
                                Display="Dynamic"
                                ValidationGroup="Validation"
                                ControlToValidate="datePrevisionnelleFinMarche"
                                ErrorMessage="<%=Prado::localize('DATE_PREVISIONNELLE_FIN_MARCHE')%>"
                                Text="<span title='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' /></span>" >
                            <prop:ClientSide.OnValidationError>
                                document.getElementById('divValidationSummary').style.display='';
                                document.getElementById('<%=$this->page->buttonValidate->getClientId()%>').style.display='';
                            </prop:ClientSide.OnValidationError>
                        </com:TDateValidator>

                    </div>
                    <img  class="picto-info-intitule" onmouseout="cacheBulle('bulleDatePrevisionnelle')" onmouseover="afficheBulle('bulleDatePrevisionnelle',this)" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" />
                    <div class="info-aide-right indent-20"><com:TTranslate>DEFINE_FORMAT_DATE_SIMPLE</com:TTranslate></div>
                    <com:TCustomValidator
                            ControlToValidate="datePrevisionnelleFinMarche"
                            ValidationGroup="Validation"
                            Display="Dynamic"
                            ClientValidationFunction="ValidateDatePrevNotificationFinMarche"
                            ErrorMessage="<%=Prado::localize('MESSAGE_ERREUR_CONTROLE_DATE_PREVISIONNELLE_NOTIFICATION_FIN_MARCHE_DECLARATION_CONTRAT')%>"
                            EnableClientScript="true"
                            Text="<span title='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' /></span>"
                    >
                        <prop:ClientSide.OnValidationError>
                            document.getElementById('divValidationSummary').style.display='';
                        </prop:ClientSide.OnValidationError>
                    </com:TCustomValidator>
                </div>
                <!--Fin Ligne Date prévisionnelle de fin de marché-->
                <!--Fin Ligne Date prévisionnelle de fin maximale de marché-->
                <div class="line">
                    <div class="intitule-250"><label for="datePrevFinMaxMarche"><com:TTranslate>DATE_PREVISIONNELLE_FIN_MAX_MARCHE</com:TTranslate></label><span class="champ-oblig">*</span> : </div>
                    <div class="calendar">
                        <com:TTextBox
                                ID="datePrevFinMaxMarche"
                                Attributes.title="<%=Prado::localize('DATE_PREVISIONNELLE_FIN_MAX_MARCHE')%>"
                                CssClass="heure"
                        />
                        <com:TImage id="calendarDateFinMax" ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-calendar.gif" Attributes.alt="<%=Prado::localize('DEFINE_CALENDRIER')%>" Attributes.title="<%=Prado::localize('DEFINE_CALENDRIER')%>" Attributes.onclick="popUpCalendar(this,document.getElementById('<%=$this->datePrevFinMaxMarche->getClientId()%>'),'dd/mm/yyyy','');" />
                        <com:TRequiredFieldValidator
                                ControlToValidate="datePrevFinMaxMarche"
                                ValidationGroup="Validation"
                                Display="Dynamic"
                                EnableClientScript="true"
                                ErrorMessage="<%=Prado::localize('DATE_PREVISIONNELLE_FIN_MAX_MARCHE')%>"
                                Text="<span title='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' /></span>">
                            <prop:ClientSide.OnValidationError>
                                document.getElementById('divValidationSummary').style.display='';
                                document.getElementById('<%=$this->page->buttonValidate->getClientId()%>').style.display='';
                            </prop:ClientSide.OnValidationError>
                        </com:TRequiredFieldValidator>
                        <com:TDateValidator
                                Display="Dynamic"
                                ValidationGroup="Validation"
                                ControlToValidate="datePrevFinMaxMarche"
                                ErrorMessage="<%=Prado::localize('DATE_PREVISIONNELLE_FIN_MAX_MARCHE')%>"
                                Text="<span title='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' /></span>" >
                            <prop:ClientSide.OnValidationError>
                                document.getElementById('divValidationSummary').style.display='';
                                document.getElementById('<%=$this->page->buttonValidate->getClientId()%>').style.display='';
                            </prop:ClientSide.OnValidationError>
                        </com:TDateValidator>
                    </div>
                    <img  class="picto-info-intitule" onmouseout="cacheBulle('datePrevisionnelle')" onmouseover="afficheBulle('datePrevisionnelle',this)" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" />
                    <div id="datePrevisionnelle" class="info-bulle" onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();"><div><com:TTranslate>DATE_PREVISIONNELLE_INFOBULLE</com:TTranslate></div></div>

                    <div class="info-aide-right indent-20"><com:TTranslate>DEFINE_FORMAT_DATE_SIMPLE</com:TTranslate></div>
                    <com:TCustomValidator
                            ControlToValidate="datePrevFinMaxMarche"
                            ValidationGroup="Validation"
                            Display="Dynamic"
                            ClientValidationFunction="ValidateDatePrevFinMarcheMax"
                            ErrorMessage="<%=Prado::localize('MESSAGE_ERREUR_CONTROLE_DATE_PREVISIONNELLE_FIN_MARCHE_MAXIMALE_DECLARATION_CONTRAT')%>"
                            EnableClientScript="true"
                            Text="<span title='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' /></span>"
                    >
                        <prop:ClientSide.OnValidationError>
                            document.getElementById('divValidationSummary').style.display='';
                        </prop:ClientSide.OnValidationError>
                    </com:TCustomValidator>
                </div>
                <!--Fin Ligne Date prévisionnelle de fin maximale de marché-->
                <div class="breaker"></div>
            </div>
        </div>
        <!--Debut Bloc Formulaire-->

        <div class="breaker"></div>
    </div>
    <div id="bulleDatePrevisionnelle" class="info-bulle" onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();"><div><com:TTranslate>TEXT_INFO_BULLE_DATE_PREVISIONNELLE</com:TTranslate></div></div>
    <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
</div>
<!--Fin Bloc Datess-->

<com:TActivePanel cssClass="form-field" id="panelDateExecution">
    <div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
    <div class="content">
        <div class="float-right margin-fix"><com:TTranslate>DEFINE_SYMBOLE</com:TTranslate> <span class="champ-oblig">*</span> <com:TTranslate>DEFINE_INDIQUE_CHAMPS_OBLIGATOIRES</com:TTranslate></div>
        <div class="creation-form donnees-contrats">
            <div class="panel">
                <!--Debut Ligne Date de début d'exécution -->
                <div Class="line" >
                    <div class="intitule-250"><label for="dateExecution"><com:TTranslate>TEXT_DATE_DEBUT_EXECUTION</com:TTranslate></label><span class="champ-oblig">*</span> : </div>
                    <div class="calendar">
                        <com:TActiveTextBox
                                AutoPostBack="true"
                                ID="dateExecution"
                                Attributes.title="<%=Prado::localize('TEXT_DATE_DEBUT_EXECUTION')%>"
                                CssClass="heure"
                        />
                        <com:TImage ID="calendarDateExecution" ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-calendar.gif" Attributes.alt="<%=Prado::localize('DEFINE_CALENDRIER')%>" Attributes.title="<%=Prado::localize('DEFINE_CALENDRIER')%>" Attributes.onclick="popUpCalendar(this,document.getElementById('<%= $this->dateExecution->getClientID()%>'),'dd/mm/yyyy','');" />
                        <com:TRequiredFieldValidator
                                ControlToValidate="dateExecution"
                                ValidationGroup="Validation"
                                Display="Dynamic"
                                EnableClientScript="true"
                                ErrorMessage="<%=Prado::localize('TEXT_DATE_DEBUT_EXECUTION')%>"
                                Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
                            <prop:ClientSide.OnValidationError>
                                document.getElementById('divValidationSummary').style.display='';
                                document.getElementById('<%=$this->page->buttonValidate->getClientId()%>').style.display='';
                            </prop:ClientSide.OnValidationError>
                        </com:TRequiredFieldValidator>
                        <com:TDateValidator
                                Display="Dynamic"
                                ValidationGroup="Validation"
                                ControlToValidate="dateExecution"
                                EnableClientScript="true"
                                ErrorMessage="<%=Prado::localize('TEXT_DATE_DEBUT_EXECUTION')%>"
                                Text="<span title='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMPS_OBLIGATOIRE')%>' /></span>" >
                            <prop:ClientSide.OnValidationError>
                                document.getElementById('divValidationSummary').style.display='';
                                document.getElementById('<%=$this->page->buttonValidate->getClientId()%>').style.display='';
                            </prop:ClientSide.OnValidationError>
                        </com:TDateValidator>
                    </div>
                    <img alt="Info-bulle" class="picto-info-intitule" onmouseout="cacheBulle('infosDateExecution')" onmouseover="afficheBulle('infosDateExecution',this)" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" />
                    <div class="info-aide-right indent-20"><com:TTranslate>DEFINE_FORMAT_DATE_SIMPLE</com:TTranslate></div>

                    <div id="infosDateExecution" class="info-bulle" onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();">
                        <div><com:TTranslate>INFOBULLE_DATE_DEBUT_EXECUTION</com:TTranslate></div>
                    </div>
                </div>
                <!--Fin Ligne Date de début d'exécution-->

            </div>
        </div>
    </div>
    <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
</com:TActivePanel>







<com:TActiveLabel id="labelScriptJs" style="display:none;" />
<script type="application/javascript">
    J(document).ready(function(){
        if (J('.line-lieuPrincipalExecution').find('input').is(':disabled')){
            J('.choice_lieuPrincipalExecution').css("padding-right", "5px")
            J('.choice_lieuPrincipalExecution').unbind( "click" );
            J('.choice_lieuPrincipalExecution').find('a').hide();
        }

    });
</script>
