<com:TPanel id="bloc_etapeDetailsLots">
	<div class="form-field">
		<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
		<div class="content">
			<h3 class="float-left"><com:TTranslate>DEFINE_DETAIL_LOTS</com:TTranslate></h3>
			<div class="float-right margin-fix"><com:TTranslate>DEFINE_SYMBOLE</com:TTranslate> <span class="champ-oblig">*</span> <com:TTranslate>DEFINE_INDIQUE_CHAMPS_OBLIGATOIRES</com:TTranslate></div>
			<!--Ci-dessous message a afficher si aucun lot saisi-->
			<!--<p class="clear-both">Aucun lot n'a été saisi</p>-->
			<com:TActivePanel ID="PanelListeLots" >
				<com:TRepeater ID="tableauDesLots"  >
					<prop:HeaderTemplate>
						<table class="table-results tableau-lots" summary="Liste des lots">
							<caption><com:TTranslate>TEXT_LISTE_LOTS</com:TTranslate></caption>
							<thead>
							<tr>
								<th class="top" colspan="4"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></th>
							</tr>
							<tr>
								<th class="col-30 center" id="numLot"><com:TTranslate>DEFINE_LOT_NUMERO</com:TTranslate></th>
								<th class="col-50 center" id="catLot"><com:TTranslate>TEXT_CATEGORIE</com:TTranslate></th>
								<th class="col-450" id="intituleLot"><com:TTranslate>DEFINE_INTITULE_LOT</com:TTranslate></th>
								<th class="actions" id="actionsLot"><com:TTranslate>TEXT_ACTIONS</com:TTranslate></th>
							</tr>
							</thead>
					</prop:HeaderTemplate>
					<prop:ItemTemplate>
						<tr class="<%#(($this->ItemIndex%2==0)? '':'on')%>">
							<com:THiddenField id="idLot" Value="<%#$this->Data->getLot()%>" />
							<td class="col-30 center" headers="numLot"><%#$this->Data->getLot()%></td>
							<td class="col-50 center" headers="catLot">
								<div style="display:<%#( Application\Service\Atexo\Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX')== $this->Data->getCategorie())?'':'none'%>">
									<img src="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-travaux.gif" title="<%#Prado::localize('DEFINE_TRAVAUX')%>" />
								</div>
								<div style="display:<%#( Application\Service\Atexo\Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES')== $this->Data->getCategorie())?'':'none'%>">
									<img src="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-fournitures.gif" title="<%#Prado::localize('DEFINE_FOURNITURES')%>" />
								</div>
								<div style="display:<%#( Application\Service\Atexo\Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES')== $this->Data->getCategorie())?'':'none'%>">
									<img src="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-services.gif" title="<%#Prado::localize('DEFINE_SERVICES')%>" />
								</div>
							</td>
							<td class="col-450" headers="intituleLot"><%#$this->Data->getDescriptionTraduite()%></td>
							<td class="actions" headers="actionsLot">
								<com:THyperLink NavigateUrl="javascript:popUp('index.php?page=Agent.PopupDetailLot&orgAccronyme=<%#$this->Data->getOrganisme()%>&id=<%#$this->Data->getConsultationId()%>&idLot=<%#$this->Data->getLot()%>','yes');" Id="lienDetailLot" >
									<img src="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-details.gif" alt="<%#Prado::localize('DEFINE_PREVISUALISER')%>" title="<%#Prado::localize('DEFINE_PREVISUALISER')%>" />
								</com:THyperLink>
								<com:THyperLink NavigateUrl="javascript:popUp('index.php?page=Agent.PopupAjoutLot&orgAccronyme=<%#$this->Data->getOrganisme()%>&id=<%#$this->Data->getConsultationId()%>&idButtonRefresh=<%#$this->TemplateControl->refreshPanel->getClientId()%>&idLot=<%#$this->Data->getLot()%>','yes');" Id="lienModifierLot" >
									<img src="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-edit.gif" alt="<%#Prado::localize('MODIFIER')%>" title="<%#Prado::localize('MODIFIER')%>" />
								</com:THyperLink>
								<com:THyperLink Id="lienDupliquerLot" NavigateUrl="javascript:popUp('index.php?page=Agent.PopupDupliquerLot&org=<%#$this->Data->getOrganisme()%>&id=<%#$this->Data->getConsultationId()%>&idButtonRefresh=<%#$this->TemplateControl->refreshPanel->getClientId()%>&lot=<%#$this->Data->getLot()%>','yes');">
									<img src="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-dupliquer.gif" alt="<%#Prado::localize('DUPLIQUER_LOT')%>" title="<%#Prado::localize('DUPLIQUER_LOT')%>" />
								</com:THyperLink>
								<com:TActiveLinkButton
										ID="delteLotButton"
										OnCallBack="SourceTemplateControl.deleteLot"
										Attributes.title="<%#Prado::localize('DEFINE_SUPPRIMER_LOT')%>"
										Attributes.onClick="return confirm('<%#Prado::localize('CONFIRM_DELETE_LOT')%>');"
										ActiveControl.CallbackParameter="<%#$this->Data->getLot()%>"
								>
									<img src="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-supprimer-big.gif" alt="<%#Prado::localize('DEFINE_SUPPRIMER')%>" title="<%#Prado::localize('DEFINE_SUPPRIMER')%>"/>
								</com:TActiveLinkButton>
								<com:TActiveLinkButton
										id="newDeleteLotButton"
										Attributes.title="<%#Prado::localize('DEFINE_SUPPRIMER_LOT')%>"
										ActiveControl.CallbackParameter="<%#$this->Data->getLot()%>"
										OnCallBack="SourceTemplateControl.openModalDume"
								>
									<img src="<%#Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-supprimer-big.gif" alt="<%#Prado::localize('DEFINE_SUPPRIMER')%>" title="<%#Prado::localize('DEFINE_SUPPRIMER')%>"/>
								</com:TActiveLinkButton>
								<com:THiddenField
										id="hiddenLot"
										value="<%#$this->Data->getLot()%>"
								>
								</com:THiddenField>
							</td>
						</tr>
					</prop:ItemTemplate>
					<prop:FooterTemplate>
						</table>
					</prop:FooterTemplate>
				</com:TRepeater>
				<div class="breaker"></div>
				<com:TActiveHyperLink Id="lienAjouterLot" CssClass="ajout-el float-left" >
					<com:TTranslate>DEFINE_AJOUTER_LOT</com:TTranslate>
				</com:TActiveHyperLink>
				<com:TActiveHyperLink Id="lienImportLot" CssClass="telecharger-el float-left margin-left-15" visible="<%=(!$this->getConsultation()->getDonneeComplementaireObligatoire())%>">
					<com:TTranslate>DEFINE_IMPORTER_LOT</com:TTranslate>
				</com:TActiveHyperLink>
				<com:TActivePanel id="visualiserLots" visible="<%=($this->getNombreDesLots())?true:false%>">
					<com:TActiveLinkButton
							ID="delteAllLotsButton"
							Attributes.title="<%=Prado::localize('SUPPRIMER_TOUS_LES_LOTS')%>"
							CssClass="suppr-el big float-right margin-left-10"
							Attributes.onClick="openModal('modal-confirmation-suppression','popup-small2',this);"
					>
						<com:TTranslate>SUPPRIMER_TOUS_LES_LOTS</com:TTranslate>
					</com:TActiveLinkButton>
					<com:TActiveLinkButton
							ID="newDeleteAllLotsButton"
							Attributes.title="<%=Prado::localize('SUPPRIMER_TOUS_LES_LOTS')%>"
							CssClass="suppr-el big float-right margin-left-10"
							OnCallBack="SourceTemplateControl.openModalDumeAll"
					>
						<com:TTranslate>SUPPRIMER_TOUS_LES_LOTS</com:TTranslate>
					</com:TActiveLinkButton>
					<com:TActiveHyperLink Id="lienDetailLots" CssClass="ajout-el detail-el float-right" >
						<com:TTranslate>DEFINE_VISUALISER_ALL_LOTS</com:TTranslate>
					</com:TActiveHyperLink>
				</com:TActivePanel>
			</com:TActivePanel>
			<div class="breaker"></div>
		</div>
		<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
	</div>
	<!--Fin Bloc formulaire-->
	<com:TActiveButton id="refreshPanel" onCallBack="refreshRepeater" style="display:none"/>
	<div class="modal-confirmation-suppression" style="display:none;">
		<!--Debut Formulaire-->
		<div class="form-bloc">
			<div class="top"><span class="left"></span><span class="right"></span></div>
			<div class="content">
				<p><strong><com:TTranslate>CONFIRMATION_SUPPRESION_TOUS_LOTS</com:TTranslate></strong></p>
			</div>
			<div class="bottom"><span class="left"></span><span class="right"></span></div>
		</div>
		<!--Fin Formulaire-->
		<!--Debut line boutons-->
		<div class="boutons-line">
			<input type="button"
				   class="bouton-moyen float-left"
				   value="<%=Prado::localize('DEFINE_ANNULER')%>"
				   title="<%=Prado::localize('DEFINE_ANNULER')%>"
				   onclick="J('.modal-confirmation-suppression').dialog('close');return false;"
			/>
			<com:TActiveButton
					id="cofirmSupression"
					Text = "<%=Prado::localize('TEXT_POURSUIVRE')%>"
					Attributes.title="<%=Prado::localize('TEXT_POURSUIVRE')%>"
					Attributes.OnClick="J('.modal-confirmation-suppression').dialog('close');"
					CssClass="bouton-moyen float-right"
					CommandName="Delete"
					onCommand="deleteAllLots"
					onCallBack="refreshRepeater"
			>
			</com:TActiveButton >
		</div>
		<!--Fin line boutons-->
	</div>
</com:TPanel>

<!--Debut Modal validation-->
<com:TActivePanel id="panelValidationCheckPurgeDelete" cssclass="demander-purge" style="display:none;">
	<div class="panel panel-default with-bg">
		<div class=" panel-body">
			<p><com:TTranslate>MSG_POPIN_MODIFICATION_LOT_DUME</com:TTranslate></p>
			<com:TActiveHiddenField id="deleteLotDume"></com:TActiveHiddenField>
		</div>
	</div>
	<!--Debut line boutons-->
	<div class="boutons-line">
		<input type="button"
			   class="bouton-moyen float-left"
			   value="<%=Prado::localize('DEFINE_ANNULER')%>"
			   title="<%=Prado::localize('DEFINE_ANNULER')%>"
			   onclick="J('.demander-purge').dialog('close');return false;"
		/>
		<com:TActiveButton
				Text = "<%=Prado::localize('ICONE_VALIDER')%>"
				id="validerPurge"
				CssClass="bouton-moyen float-right"
				OnCallBack="SourceTemplateControl.deleteLot"
		>
			<prop:ClientSide.OnLoading>showLoader();</prop:ClientSide.OnLoading>
			<prop:ClientSide.OnFailure>hideLoader();</prop:ClientSide.OnFailure>
			<prop:ClientSide.OnSuccess>hideLoader();</prop:ClientSide.OnSuccess>
		</com:TActiveButton >
	</div>
	<!--Fin line boutons-->
</com:TActivePanel>
<!--Fin Modal validation-->

<!--Debut Modal validation-->
<com:TActivePanel id="panelValidationCheckPurgeDeleteAll" cssclass="demander-purge-all" style="display:none;">
	<div class="panel panel-default with-bg">
		<div class=" panel-body">
			<p><com:TTranslate>MSG_POPIN_MODIFICATION_LOT_DUME</com:TTranslate></p>
		</div>
	</div>
	<!--Debut line boutons-->
	<div class="boutons-line">
		<input type="button"
			   class="bouton-moyen float-left"
			   value="<%=Prado::localize('DEFINE_ANNULER')%>"
			   title="<%=Prado::localize('DEFINE_ANNULER')%>"
			   onclick="J('.demander-purge-all').dialog('close');return false;"
		/>
		<com:TActiveButton
				Text = "<%=Prado::localize('ICONE_VALIDER')%>"
				id="validerPurgeAll"
				CssClass="bouton-moyen float-right"
				OnCommand="SourceTemplateControl.deleteAllLots"
				onCallBack="SourceTemplateControl.refreshRepeater"
		>
			<prop:ClientSide.OnLoading>showLoader();</prop:ClientSide.OnLoading>
			<prop:ClientSide.OnFailure>hideLoader();</prop:ClientSide.OnFailure>
			<prop:ClientSide.OnSuccess>hideLoader();</prop:ClientSide.OnSuccess>
		</com:TActiveButton >
	</div>
	<!--Fin line boutons-->
</com:TActivePanel>
<!--Fin Modal validation-->
