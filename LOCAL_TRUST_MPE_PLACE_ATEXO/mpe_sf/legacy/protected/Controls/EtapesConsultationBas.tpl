<!--Debut Bloc barre etape action-->
	<com:TPanel cssclass="etape first current" id="etapeIdentification">
		<div>
			<a href="#" onClick="document.getElementById('ctl0_CONTENU_PAGE_etapesConsultation_goToEtapeIdentification').click();return false;"><com:TTranslate>DEFINE_IDENTIFICATION</com:TTranslate></a>
		</div>
		<div class="arrow"></div>
	</com:TPanel>
	<com:TActivePanel cssclass="etape etape-high" id="etapeDonneesComplementaires" Visible="<%=Application\Service\Atexo\Atexo_Module::isEnabled('DonneesComplementaires')%>">
		<div>
			<a href="#" onClick="document.getElementById('ctl0_CONTENU_PAGE_etapesConsultation_goToEtapeDonneesComplementaires').click();return false;"><com:TTranslate>DEFINE_DONNEES</com:TTranslate><br /> <%=(Application\Service\Atexo\Atexo_Module::isEnabled('Publicite') && !Application\Service\Atexo\Atexo_Module::isEnabled('InterfaceModuleRsem'))? Prado::localize('DEFINE_COMPLEMENTAIRES_PUB') : Prado::localize('DEFINE_COMPLEMENTAIRES')%></a>
		</div>
		<div class="arrow"></div>
	</com:TActivePanel>
	<com:TActivePanel cssclass="etape etape-high etape-off" id="etapeDonneesComplementairesInactif" Visible="<%=Application\Service\Atexo\Atexo_Module::isEnabled('DonneesComplementaires') && Application\Service\Atexo\Atexo_Module::isEnabled('Publicite')%>">
		<div>
			<com:TTranslate>DEFINE_DONNEES</com:TTranslate><br /> <%=(Application\Service\Atexo\Atexo_Module::isEnabled('Publicite') && !Application\Service\Atexo\Atexo_Module::isEnabled('InterfaceModuleRsem'))? Prado::localize('DEFINE_COMPLEMENTAIRES_PUB') : Prado::localize('DEFINE_COMPLEMENTAIRES')%>
		</div>
		<div class="arrow"></div>
	</com:TActivePanel>
	<com:TActivePanel cssclass="etape" id="etapeDetailsLots" >
		<div>
			<a href="#" onClick="document.getElementById('ctl0_CONTENU_PAGE_etapesConsultation_goToEtapeDetailsLots').click();return false;"><com:TTranslate>DEFINE_LOTS</com:TTranslate></a>
		</div>
		<div class="arrow"></div>
	</com:TActivePanel>
	<com:TActivePanel cssclass="etape etape-off" id="etapeDetailsLotsInactif">
		<div>
			<com:TLabel><com:TTranslate>DEFINE_LOTS</com:TTranslate></com:TLabel>
		</div>
		<div class="arrow"></div>
	</com:TActivePanel>
	<com:TActivePanel cssclass="etape etape-high" id="etapeDumeAcheteur" Visible="<%=Application\Service\Atexo\Atexo_Module::isEnabled('InterfaceDume')%>">
		<div>
			<a href="#" onClick="document.getElementById('ctl0_CONTENU_PAGE_etapesConsultation_goToEtapeDumeAcheteur').click();return false;"><com:TTranslate>DUME_ACHETEUR</com:TTranslate></a>
		</div>
		<div class="arrow"></div>
	</com:TActivePanel>
	<com:TActivePanel cssclass="etape etape-high etape-off" id="etapeDumeAcheteurInactif" Visible="<%=Application\Service\Atexo\Atexo_Module::isEnabled('InterfaceDume')%>">
		<div>
			<com:TLabel><com:TTranslate>DUME_ACHETEUR</com:TTranslate></com:TLabel>
		</div>
		<div class="arrow"></div>
	</com:TActivePanel>
	<com:TPanel cssclass="etape" id="etapeCalendrier">
		<div>
			<a href="#" onClick="document.getElementById('ctl0_CONTENU_PAGE_etapesConsultation_goToEtapeCalendrier').click();return false;"><com:TTranslate>DEFINE_CALENDRIER</com:TTranslate></a>
		</div>
		<div class="arrow"></div>
	</com:TPanel>
    <com:TPanel cssclass="etape" id="etapeCalendrierPieces">
        <div>
            <a href="#" onClick="document.getElementById('ctl0_CONTENU_PAGE_etapesConsultation_goToEtapeCalendrierPieces').click();return false;"><com:TTranslate>DEFINE_CALENDRIER_PIECE</com:TTranslate></a>
        </div>
        <div class="arrow"></div>
    </com:TPanel>
	<com:TPanel cssclass="etape " id="etapeDocumentsJoints">
		<div>
			<a href="#" onClick="document.getElementById('ctl0_CONTENU_PAGE_etapesConsultation_goToEtapeDocumentsJoints').click();return false;"><com:TTranslate>DEFINE_PIECES</com:TTranslate></a>
		</div>
		<div class="arrow"></div>
	</com:TPanel>
	<com:TPanel cssclass="etape " id="etapeModalitesReponse">
		<div>
			<a href="#" onClick="document.getElementById('ctl0_CONTENU_PAGE_etapesConsultation_goToEtapeModalitesReponse').click();return false;"><com:TTranslate>DEFINE_TEXT_MODALITES</com:TTranslate></a>
		</div>
		<div class="arrow"></div>
	</com:TPanel>
	<com:TPanel cssclass="etape last" id="etapeDroitsAcces">
		<div>
			<a href="#" onClick="document.getElementById('ctl0_CONTENU_PAGE_etapesConsultation_goToEtapeDroitsAcces').click();return false;"><com:TTranslate>TEXT_ACCES</com:TTranslate></a>
		</div>
		<div class="arrow"></div>
	</com:TPanel>
	<com:TActivePanel cssclass="etape last" id="PubliciteConsultation"  Visible="<%=Application\Service\Atexo\Atexo_Module::isEnabled('Publicite')%>">
		<div>
			<a href="#" onClick="document.getElementById('ctl0_CONTENU_PAGE_etapesConsultation_publicite').click();return false;"><com:TTranslate>PUBLICITE_CONSULTATION</com:TTranslate></a>
		</div>
		<div class="arrow"></div>
	</com:TActivePanel>
<com:TActivePanel cssclass="etape etape-off" id="PubliciteConsultationInactif" Visible="<%=Application\Service\Atexo\Atexo_Module::isEnabled('Publicite')%>">
	<div>
		<com:TLabel><com:TTranslate>PUBLICITE_CONSULTATION</com:TTranslate></com:TLabel>
	</div>
	<div class="arrow"></div>
</com:TActivePanel>
	<com:TActiveLabel id="scriptJs"  />
	<com:TActiveLabel id="scriptJsPub"  />
	<com:THiddenField id="valeurEtapeCourante" />
    <com:THiddenField id="valeurEtapeSuivante" />
	<com:TConditional condition=" Application\Service\Atexo\Atexo_Config::getParameter('ACTIVER_WEKA')">
		<prop:TrueTemplate>
			<script language="JavaScript" type="text/JavaScript">
				addListnerPage(false);
			</script>
		</prop:TrueTemplate>
	</com:TConditional>

<!--Fin Bloc barre etape action-->
<com:TActiveLabel id="scriptJsEtapes" />

<!--Debut Modal-->
<div class="demander-enregistrement" style="display:none;">
	<!--Debut Formulaire-->
	<div class="form-bloc">
		<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
		<div class="content">
			<com:TTranslate>CONFIRMATION_ENREGISTRMENT_CONSULTATION</com:TTranslate>
		</div>
		<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
	</div>
	<!--Fin Formulaire-->
	<com:TActiveHiddenField ID="etapeToGo" />
	<!--Debut line boutons-->
	<div class="boutons-line">
		<input type="button"
			   class="bouton-moyen float-left"
			   value="<%=Prado::localize('DEFINE_ANNULER')%>"
			   title="<%=Prado::localize('DEFINE_ANNULER')%>"
			   onclick="J('.demander-enregistrement').dialog('destroy');return false;"
		/>
		<com:TActiveButton
				Text = "<%=Prado::localize('ICONE_OK')%>"
				Attributes.OnClick="getCalendrierJson();J('.demander-enregistrement').dialog('destroy');"
				id="enregistrementEtapeIdentification"
				CssClass="bouton-moyen float-right"
				OnCallBack="goToEtapeFromPopin"
				ValidationGroup="<%=$this->getValidationGroup()%>"
				>
		</com:TActiveButton >
	</div>
	<!--Fin line boutons-->
</div>
<!--Fin Modal-->
