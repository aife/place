<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_GenerationExcel;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Util;

/*
 * Created on 5 oct. 2011
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
class AtexoListeOrganisme extends MpeTPage
{
    public $nbrElementRepeater;
    public $critereTri;
    public $triAscDesc;
    private $langue;

    public function getNbrElementRepeater()
    {
        return $this->nbrElementRepeater;
    }

    public function setNbrElementRepeater($nbrElement)
    {
        return $this->nbrElementRepeater = $nbrElement;
    }

    public function onLoad($param)
    {
        $this->langue = Atexo_CurrentUser::readFromSession('lang');
    }

    public function genererExcel()
    {
        $listeOrgs = $this->getViewState('listeOrganisme');
        (new Atexo_GenerationExcel())->generateExcelOrganismes($listeOrgs);
    }

    /*
     * Permet d'intialiser les valeurs de pagination et le repeater
     */
    public function intialiserRepeater()
    {
        $this->RepeaterOrganisme->DataSource = [];
        $this->RepeaterOrganisme->DataBind();
        $this->nbResultsTop->setSelectedValue(10);
        $this->nbResultsBottom->setSelectedValue(10);
        $this->pageNumberTop->Text = 1;
        $this->pageNumberBottom->Text = 1;
        $this->RepeaterOrganisme->setCurrentPageIndex(0);
    }

    /*
     * permet de remplir le repeater
     *
     */
    public function repeaterDataBind($listeOrganisme)
    {
        $nombreElement = is_countable($listeOrganisme) ? count($listeOrganisme) : 0;
        $this->nbrElementRepeater = $nombreElement;
        $this->setViewState('nbrElements', $nombreElement);
        $this->setViewState('listeOrganisme', $listeOrganisme);
        if ($nombreElement) {
            $this->nombrePageTop->Text = ceil($nombreElement / $this->RepeaterOrganisme->PageSize);
            $this->nombrePageBottom->Text = ceil($nombreElement / $this->RepeaterOrganisme->PageSize);
            $this->RepeaterOrganisme->setVirtualItemCount($nombreElement);
            $this->RepeaterOrganisme->setCurrentPageIndex(0);
            self::showComponent();
            self::populateData();
        } else {
            $this->setViewState('listeOrganisme', []);
            $this->RepeaterOrganisme->DataSource = [];
            $this->RepeaterOrganisme->DataBind();
            self::hideComponent();
        }
    }

    public function populateData($organisme = false)
    {
        $offset = $this->RepeaterOrganisme->CurrentPageIndex * $this->RepeaterOrganisme->PageSize;
        $limit = $this->RepeaterOrganisme->PageSize;
        if ($offset + $limit > $this->RepeaterOrganisme->getVirtualItemCount()) {
            $limit = $this->RepeaterOrganisme->getVirtualItemCount() - $offset;
        }
        $dataOrganisme = [];
        if ($this->getViewState('listeOrganismesTriee')) {
            $dataAllOrganismes = $this->getViewState('listeOrganismesTriee');
        } else {
            $dataAllOrganismes = $this->getViewState('listeOrganisme');
        }
        $j = 0;
        for ($i = $offset; $i < (is_countable($dataAllOrganismes) ? count($dataAllOrganismes) : 0) && $i < ($limit + $offset); ++$i) {
            $dataOrganisme[$j++] = $dataAllOrganismes[$i];
        }
        $this->setNbrElementRepeater($this->getViewState('nbrElements'));
        if (count($dataOrganisme)) {
            self::showComponent();
            $this->RepeaterOrganisme->DataSource = $dataOrganisme;
            $this->RepeaterOrganisme->DataBind();
        } else {
            self::hideComponent();
        }
    }

    public function showComponent()
    {
        $this->PagerTop->visible = true;
        $this->PagerBottom->visible = true;
        $this->pageNumberBottom->visible = true;
        $this->pageNumberTop->visible = true;
        $this->labelSlashBottom->visible = true;
        $this->labelSlashTop->visible = true;
        $this->nombrePageTop->visible = true;
        $this->nombrePageBottom->visible = true;
        $this->nbResultsTop->visible = true;
        $this->labelAfficherTop->visible = true;
        $this->labelAfficherDown->visible = true;
        $this->nbResultsBottom->visible = true;
        $this->resultParPageDown->visible = true;
        $this->resultParPageTop->visible = true;
        $this->panelBouttonGotoPageBottom->visible = true;
        $this->panelBouttonGotoPageTop->visible = true;
    }

    public function hideComponent()
    {
        $this->PagerTop->visible = false;
        $this->PagerBottom->visible = false;
        $this->pageNumberBottom->visible = false;
        $this->pageNumberTop->visible = false;
        $this->labelSlashBottom->visible = false;
        $this->labelSlashTop->visible = false;
        $this->nombrePageTop->visible = false;
        $this->nombrePageBottom->visible = false;
        $this->nbResultsTop->visible = false;
        $this->labelAfficherTop->visible = false;
        $this->labelAfficherDown->visible = false;
        $this->nbResultsBottom->visible = false;
        $this->resultParPageTop->visible = false;
        $this->resultParPageDown->visible = false;
        $this->panelBouttonGotoPageBottom->visible = false;
        $this->panelBouttonGotoPageTop->visible = false;
        $this->nbrElementRepeater = 0;
    }

    public function pageChanged($sender, $param)
    {
        $this->RepeaterOrganisme->CurrentPageIndex = $param->NewPageIndex;
        $this->setNbrElementRepeater($this->getViewState('nbrElements'));
        self::updatePagination($this->nbResultsTop->getSelectedValue(), $param->NewPageIndex + 1);
        $this->populateData();
    }

    public function changePagerLenght($sender, $param)
    {
        $pageSize = null;
        switch ($sender->ID) {
            case 'nbResultsBottom':
                 $pageSize = $this->nbResultsBottom->getSelectedValue();
                                                  $this->nbResultsTop->setSelectedValue($pageSize);
                break;
            case 'nbResultsTop':
                 $pageSize = $this->nbResultsTop->getSelectedValue();
                                               $this->nbResultsBottom->setSelectedValue($pageSize);
                break;
        }
        self::updatePagination($pageSize, $this->pageNumberTop->Text);
        $this->populateData();
    }

    public function goToPage($sender, $param)
    {
        $numPage = null;
        switch ($sender->ID) {
            case 'DefaultButtonTop':
                 $numPage = $this->pageNumberTop->Text;
                break;
            case 'DefaultButtonBottom':
                 $numPage = $this->pageNumberBottom->Text;
                break;
        }
        self::toPage($numPage);
    }

    public function toPage($numPage)
    {
        if (Atexo_Util::isEntier($numPage)) {
            if ($numPage >= $this->nombrePageTop->Text) {
                $numPage = $this->nombrePageTop->Text;
            } elseif ($numPage <= 0) {
                $numPage = 1;
            }
            self::updatePagination($this->nbResultsTop->getSelectedValue(), $numPage);
            $this->populateData();
        } else {
            $this->pageNumberTop->Text = $this->RepeaterOrganisme->CurrentPageIndex + 1;
            $this->pageNumberBottom->Text = $this->RepeaterOrganisme->CurrentPageIndex + 1;
        }
    }

    /*
     * Permet de mettre a jour la pagination
     */
    public function updatePagination($pageSize, $numPage)
    {
        $nombreElement = $this->getViewState('nbrElements');
        $this->RepeaterOrganisme->PageSize = $pageSize;
        $this->nombrePageTop->Text = ceil($nombreElement / $this->RepeaterOrganisme->PageSize);
        $this->nombrePageBottom->Text = ceil($nombreElement / $this->RepeaterOrganisme->PageSize);
        if ($numPage > $this->nombrePageTop->Text) {
            $numPage = $this->nombrePageTop->Text;
        }
        $this->RepeaterOrganisme->setCurrentPageIndex($numPage - 1);
        $this->pageNumberBottom->Text = $numPage;
        $this->pageNumberTop->Text = $numPage;
        $this->setNbrElementRepeater($this->getViewState('nbrElements'));
    }

    /**
     * Trier un tableau des invites selcetionnes.
     *
     * @param sender
     * @param param
     */
    public function sortOrganisme($sender, $param)
    {
        $this->critereTri = $this->getViewState('critereTri');
        $this->triAscDesc = $this->getViewState('triAscDesc');
        $organismes = $this->getViewState('listeOrganisme');
        if ('DenominationOrg' == $sender->getActiveControl()->CallbackParameter) {
            $dataSourceTriee = $this->getDataSourceTriee($organismes, 'DenominationOrgTraduit', '');
        } elseif ('Acronyme' == $sender->getActiveControl()->CallbackParameter) {
            $dataSourceTriee = $this->getDataSourceTriee($organismes, 'Acronyme', '');
        } else {
            return;
        }

        $this->setViewState('listeOrganismesTriee', $dataSourceTriee[0]);
        $this->setViewState('critereTri', $this->critereTri);
        $this->setViewState('triAscDesc', $this->triAscDesc);
        //Remplissage du tabeau des invites
        $this->setNbrElementRepeater($this->getViewState('nbrElements'));
        self::updatePagination($this->nbResultsTop->getSelectedValue(), $this->RepeaterOrganisme->getCurrentPageIndex() + 1);

        $this->setViewState('listeOrganisme', $dataSourceTriee[0]);

        self::populateData();

        // Re-affichage du TActivePanel
        $this->panelRefresh->render($param->getNewWriter());
        self::showComponent();
    }

    /**
     * Retourne un tableau de colonnes et un tableau d'objets tries.
     *
     * @param array  $dataSource    le tableau des donnees
     * @param string $critere
     * @param string $typeCritere
     * @param int    $indiceElement
     */
    private function getDataSourceTriee($dataSource, $critere, $typeCritere = null)
    {
        $resultat = [];
        $this->setCritereAndTypeTri($critere);
        $datatriee = null;
        if ('num' == $typeCritere) {
            $datatriee = Atexo_Util::sortArrayOfObjects($dataSource, $critere, $this->triAscDesc, 'num');
        } else {
            $datatriee = Atexo_Util::sortArrayOfObjects($dataSource, $critere, $this->triAscDesc);
        }
        $resultat[0] = $datatriee;

        return $resultat;
    }

    /**
     * Definit les criteres et les types de tri.
     *
     * @param string $critere
     * @param int    $indiceElement
     */
    private function setCritereAndTypeTri($critere)
    {
        if ($this->critereTri != $critere || !$this->critereTri) {
            $this->triAscDesc = 'ASC';
        } else {
            if ('ASC' == $this->triAscDesc) {
                $this->triAscDesc = 'DESC';
            } else {
                $this->triAscDesc = 'ASC';
            }
        }
        $this->critereTri = $critere;
    }

    /***
     * gererer un excel qui contient tout les organismes
     */
    public function genererExcelAllOrganismes()
    {
        $listeOrgs = Atexo_Organismes::retrieveOrganismes(true, null, true);
        (new Atexo_GenerationExcel())->generateExcelOrganismes($listeOrgs);
    }

    /*
     * Permet d'avoir logo de l'organisme
     */
    public function getUrlLogo($org)
    {
        return Atexo_Config::getParameter('PF_URL').'index.php?page='.Atexo_Util::getTypeUserCalledForPradoPages().'.LogoOrganisme&org='.$org;
    }
}
