<table summary="Liste des pièces - Dossier d'offre Lot 1" class="table-results tableau-reponse margin-0">
	
	<thead>
		<tr class="row-title">
			<th colspan="3"><a class="title-toggle-open" onclick="togglePanelClass(this,'<%=$this->getIdHtlmToggleFormulaire()%>');" href="javascript:void(0);"><%=$this->getNomDossier()%></a></th>
			<th class="col-50 center"><com:TLabel style="display:<%=(Application\Service\Atexo\Atexo_Module::isEnabled('InterfaceModuleSub'))? '':'none'%>"><com:TTranslate>DEFINE_STATUS</com:TTranslate></com:TLabel></th>
			<th class="actions"><com:TTranslate>TEXT_ACTIONS</com:TTranslate></th>
		</tr>
	</thead>
	<tbody>
	</tbody>
	</table>
		<com:AtexoReponseFormulaireSub 
			ID="atexoReponseFormulaireSub" 
			NumLot="<%#$this->getNumLot()%>"
			IdHtlmToggleFormulaire="<%#$this->getIdHtlmToggleFormulaire()%>"
			visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('InterfaceModuleSub'))? true:false%>"
		/>
		<com:AtexoReponsePiecesTypees ID="atexoReponsePiecesTypees" visible="<%=(!Application\Service\Atexo\Atexo_Module::isEnabled('InterfaceModuleSub'))? true:false%>"/>
		<com:AtexoReponsePiecesLibres ID="atexoReponsePiecesLibres" visible="<%=(!Application\Service\Atexo\Atexo_Module::isEnabled('InterfaceModuleSub'))? true:false%>"/>
		<com:THiddenField id="typeEnveloppe" />
		<com:THiddenField id="numLot" />
		<com:THiddenField id="hiddenIdHtlmToggleFormulaire" value="<%#$this->getIdHtlmToggleFormulaire()%>"/>
