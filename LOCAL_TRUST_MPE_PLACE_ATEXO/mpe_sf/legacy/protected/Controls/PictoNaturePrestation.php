<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Controller\Atexo_Controller_Front;
use Prado\Prado;
use Prado\Web\UI\WebControls\TImage;

/**
 * commentaires.
 *
 * @author thibaut Decaudain <thibaut.decaudain@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PictoNaturePrestation extends TImage
{
    public $_type;

    public function onPreRender($param)
    {
        if ($this->getType() == Atexo_Config::getParameter('TYPE_PRESTATION_TRAVAUX')) {
            $this->setImageUrl(Atexo_Controller_Front::t().'/images/picto-travaux.gif');
            $this->setAlternateText(Prado::localize('ICONE_TRAVAUX'));
            $this->setToolTip(Prado::localize('DEFINE_TRAVAUX'));
        } elseif ($this->getType() == Atexo_Config::getParameter('TYPE_PRESTATION_FOURNITURES')) {
            $this->setImageUrl(Atexo_Controller_Front::t().'/images/picto-fournitures.gif');
            $this->setAlternateText(Prado::localize('ICONE_FOURNITURES'));
            $this->setToolTip(Prado::localize('DEFINE_FOURNITURES'));
        } elseif ($this->getType() == Atexo_Config::getParameter('TYPE_PRESTATION_SERVICES')) {
            $this->setImageUrl(Atexo_Controller_Front::t().'/images/picto-services.gif');
            $this->setAlternateText(Prado::localize('ICONE_SERVICES'));
            $this->setToolTip(Prado::localize('DEFINE_SERVICES'));
        }
        //$this->setCssClass("picto-info");
    }

    public function setType($type)
    {
        $this->_type = $type;
    }

    public function getType()
    {
        return $this->_type;
    }
}
