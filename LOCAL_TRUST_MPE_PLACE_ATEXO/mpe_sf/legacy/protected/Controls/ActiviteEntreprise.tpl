		<!--Debut Bloc Activite -->
				<h4><com:TTranslate>DEFINE_INFORMATIONS_CLES_DERNIERS_EXERCICES</com:TTranslate> :</h4>
				<div class="spacer-small"></div>
				<!--Debut Bloc Chiffre cles-->
				<div class="form-bloc bloc-550">
					<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
					<div class="content">
					<com:TActivePanel id="panelActiviteEntreprise" >
						<table summary="<%=Prado::localize('DEFINE_CHIFFRES_CLES_DERNIERS_EXERCICES')%>" class="table-chiffres-cles">
							<caption><com:TTranslate>DEFINE_CHIFFRES_CLES_DERNIERS_EXERCICES</com:TTranslate></caption>
							<thead>
								<tr>
									<th id="empty" class="libelle-col"><com:TTranslate>DEFINE_EXERCICE_CLOS_EN</com:TTranslate> :</th>
									<th id="exerciceA1" class="annee"><com:TActiveLabel id="anneeClotureExercice1" /></th>
									<th id="exerciceA2" class="annee"><com:TActiveLabel id="anneeClotureExercice2" /></th>
									<th id="exerciceA3" class="annee"><com:TActiveLabel id="anneeClotureExercice3" /></th>
								</tr>
							</thead>
							<tr>
								<td headers="empty" class="libelle-col"><com:TTranslate>DEFINE_DATES_EXERCICE</com:TTranslate></td>
								<td headers="exerciceA1" class="annee"><com:TTranslate>TEXT_DU</com:TTranslate> : <com:TActiveLabel id="debutExercice1" /><br /><com:TTranslate>TEXT_AU</com:TTranslate>: <com:TActiveLabel id="finExercice1" /></td>
								<td headers="exerciceA2" class="annee"><com:TTranslate>TEXT_DU</com:TTranslate> : <com:TActiveLabel id="debutExercice2" /><br /><com:TTranslate>TEXT_AU</com:TTranslate> : <com:TActiveLabel id="finExercice2" /></td>
								<td headers="exerciceA3" class="annee"><com:TTranslate>TEXT_DU</com:TTranslate> : <com:TActiveLabel id="debutExercice3" /><br /><com:TTranslate>TEXT_AU</com:TTranslate> : <com:TActiveLabel id="finExercice3" /></td>
							</tr>
							<tr>
								<td headers="empty" colspan="4">&nbsp;</td>
							</tr>
							<tr style="display:<%= $this->_compteEntrepriseChiffreAffaireProductionBiensServices%>">
								<td headers="empty" colspan="4" class="libelle-col bold" ><com:TTranslate>DEFINE_CHIFFRE_AFFAIRES_EUR_HT</com:TTranslate></td>
							</tr>
							<tr style="display:<%= $this->_compteEntrepriseChiffreAffaireProductionBiensServices%>">
								<td headers="empty" class="libelle-col"><com:TTranslate>DEFINE_VENTE_MARCHANDISES</com:TTranslate></td>
								<td headers="exerciceA1"><com:TActiveLabel id="vente1" /> <com:TTranslate>TEXT_CURRENCY</com:TTranslate></td>
								<td headers="exerciceA2"><com:TActiveLabel id="vente2" /> <com:TTranslate>TEXT_CURRENCY</com:TTranslate></td>
								<td headers="exerciceA3"><com:TActiveLabel id="vente3" /> <com:TTranslate>TEXT_CURRENCY</com:TTranslate></td>
							</tr>
							<tr style="display:<%= $this->_compteEntrepriseChiffreAffaireProductionBiensServices%>">
								<td headers="empty" colspan="4" class="libelle-col"><com:TTranslate>DEFINE_PRODUCTION_VENDUE</com:TTranslate></td>
							</tr>
							<tr style="display:<%= $this->_compteEntrepriseChiffreAffaireProductionBiensServices%>">
								<td headers="empty" class="libelle-col-indent">- <com:TTranslate>DEFINE_BIENS</com:TTranslate></td>
								<td headers="exerciceA1"><com:TActiveLabel id="bien1" /> <com:TTranslate>TEXT_CURRENCY</com:TTranslate></td>
								<td headers="exerciceA2"><com:TActiveLabel id="bien2" /> <com:TTranslate>TEXT_CURRENCY</com:TTranslate></td>
								<td headers="exerciceA3"><com:TActiveLabel id="bien3" /> <com:TTranslate>TEXT_CURRENCY</com:TTranslate></td>
							</tr>
							<tr style="display:<%= $this->_compteEntrepriseChiffreAffaireProductionBiensServices%>">
								<td headers="empty" class="libelle-col-indent">- <com:TTranslate>DEFINE_SERVICES</com:TTranslate></td>
								<td headers="exerciceA1"><com:TActiveLabel id="service1" /> <com:TTranslate>TEXT_CURRENCY</com:TTranslate></td>
								<td headers="exerciceA2"><com:TActiveLabel id="service2" /> <com:TTranslate>TEXT_CURRENCY</com:TTranslate></td>
								<td headers="exerciceA3"><com:TActiveLabel id="service3" /> <com:TTranslate>TEXT_CURRENCY</com:TTranslate></td>
							</tr>
							<tr class="total" style="display:<%= $this->_compteEntrepriseChiffreAffaireProductionBiensServices%>">
								<td headers="empty" class="libelle-col"><com:TTranslate>DEFINE_TOTAL</com:TTranslate></td>
								<td headers="exerciceA1"><com:TActiveLabel id="total1" /> <com:TTranslate>TEXT_CURRENCY</com:TTranslate></td>
								<td headers="exerciceA2"><com:TActiveLabel id="total2" /> <com:TTranslate>TEXT_CURRENCY</com:TTranslate></td>
								<td headers="exerciceA3"><com:TActiveLabel id="total3" /> <com:TTranslate>TEXT_CURRENCY</com:TTranslate></td>
							</tr>
							<tr style="display:<%= $this->_compteEntrepriseDonneesFinancieres%>">
								<td headers="empty" colspan="4" class="libelle-col bold"><com:TTranslate>TEXT_DONNEES_FINANCIERES</com:TTranslate></td>
							</tr>
							<tr style="display:<%= $this->_compteEntrepriseDonneesFinancieres%>">
								<td headers="empty" class="libelle-col"><com:TTranslate>DEFINE_CHIFFRE_AFFAIRE</com:TTranslate></td>
								<td headers="exerciceA1"><com:TActiveLabel ID="caA1"/> <com:TTranslate>TEXT_CURRENCY</com:TTranslate></td>
								<td headers="exerciceA2"><com:TActiveLabel ID="caA2"/> <com:TTranslate>TEXT_CURRENCY</com:TTranslate></td>
								<td headers="exerciceA3"><com:TActiveLabel ID="caA3"/> <com:TTranslate>TEXT_CURRENCY</com:TTranslate></td>
							</tr>
							<tr style="display:<%= $this->_compteEntrepriseDonneesFinancieres%>">
								<td headers="empty" class="libelle-col"><com:TTranslate>TEXT_BESOIN_EXECEDENT_FINANCEMENT</com:TTranslate></td>
								<td headers="exerciceA1"><com:TActiveLabel ID="bA1"/> <com:TTranslate>TEXT_CURRENCY</com:TTranslate></td>
								<td headers="exerciceA2"><com:TActiveLabel ID="bA2"/> <com:TTranslate>TEXT_CURRENCY</com:TTranslate></td>
								<td headers="exerciceA3"><com:TActiveLabel ID="bA3"/> <com:TTranslate>TEXT_CURRENCY</com:TTranslate></td>
							</tr>
							<tr style="display:<%= $this->_compteEntrepriseDonneesFinancieres%>">
								<td headers="empty" class="libelle-col"><com:TTranslate>TEXT_CASH_FLOW</com:TTranslate></td>
								<td headers="exerciceA1"><com:TActiveLabel ID="cfA1"/> <com:TTranslate>TEXT_CURRENCY</com:TTranslate></td>
								<td headers="exerciceA2"><com:TActiveLabel ID="cfA2"/> <com:TTranslate>TEXT_CURRENCY</com:TTranslate></td>
								<td headers="exerciceA3"><com:TActiveLabel ID="cfA3"/> <com:TTranslate>TEXT_CURRENCY</com:TTranslate></td>
							</tr>
							<tr style="display:<%= $this->_compteEntrepriseDonneesFinancieres%>">
								<td headers="empty" class="libelle-col"><com:TTranslate>TEXT_CAPACITE_ENDATTEMENT</com:TTranslate></td>
								<td headers="exerciceA1"><com:TActiveLabel ID="ceA1"/> <com:TTranslate>POUR_CENT</com:TTranslate></td>
								<td headers="exerciceA2"><com:TActiveLabel ID="ceA2"/> <com:TTranslate>POUR_CENT</com:TTranslate></td>
								<td headers="exerciceA3"><com:TActiveLabel ID="ceA3"/> <com:TTranslate>POUR_CENT</com:TTranslate></td>
							</tr>
							<tr>
								<td colspan="4">&nbsp;</td>
							</tr>
							<tr>
								<td headers="empty" colspan="4" class="libelle-col bold"><com:TTranslate>DEFINE_EFFECTIF</com:TTranslate></td>
							</tr>
							<tr>
								<td headers="empty" class="libelle-col"><com:TTranslate>DEFINE_EFFECTIF_MOYEN</com:TTranslate></td>
								<td headers="exerciceA1"><com:TActiveLabel id="effectifMoyen1" /></td>
								<td headers="exerciceA2"><com:TActiveLabel id="effectifMoyen2" /></td>
								<td headers="exerciceA3"><com:TActiveLabel id="effectifMoyen3" /></td>
							</tr>
							<tr>
								<td headers="empty" class="libelle-col"><com:TTranslate>DEFINE_DONT_ENCADREMENT</com:TTranslate> :</td>
								<td headers="exerciceA1"><com:TActiveLabel id="effectifEncadrement1" /></td>
								<td headers="exerciceA2"><com:TActiveLabel id="effectifEncadrement2" /></td>
								<td headers="exerciceA3"><com:TActiveLabel id="effectifEncadrement3" /></td>
							</tr>
							<tr style="display:<%= $this->_compteEntrepriseChiffreAffaireProductionBiensServices%>">
								<td colspan="4">&nbsp;</td>
							</tr>
							<tr style="display:<%= $this->_compteEntrepriseChiffreAffaireProductionBiensServices%>">
								<td headers="empty" class="libelle-col bold"><com:TTranslate>DEFINE_PME</com:TTranslate></td>
								<td headers="exerciceA1"><com:TActiveLabel id="pme1" /></td>
								<td headers="exerciceA2"><com:TActiveLabel id="pme2" /></td>
								<td headers="exerciceA3"><com:TActiveLabel id="pme3" /></td>
							</tr>
						</table>
						</com:TActivePanel>
						<div class="spacer"></div>
						<com:TPanel ID="panelEditerChiffresCles" Visible ="true">
							<com:THyperLink id="linkEditerChiffresCles" NavigateUrl="javascript:popUp('index.php?page=Entreprise.EntrepriseEditerChiffresCles','yes');" CssClass="bouton-moyen-edit" Attributes.title="<%=Prado::localize('DEFINE_EDITER_TABLEAU')%>"><com:TTranslate>DEFINE_EDITER_TABLEAU</com:TTranslate></com:THyperLink>
						</com:TPanel>
						<div class="breaker"></div>
					</div>
					<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
				</div>
				<!--Fin Bloc Chiffre cles-->
				<div class="breaker"></div>
			</div>
			<div class="breaker"></div>
    	</div>
			<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
		</div>
		<!--Fin Bloc Activite -->