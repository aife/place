<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Util;
use Prado\Web\UI\WebControls\TFileUpload;

/**
 * Extension du composant du sommateur d'erreurs.
 *
 * @author Omar TANTAOUI <omar.tantaoui@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class AtexoFileUpload extends TFileUpload
{
    public string $_maxSize = '';

    public function getFileName($index = 0)
    {
        foreach ($_FILES as $key => $value) {
            if (str_replace('$', '_', $key) == $this->getClientId()) {
                return Atexo_Util::atexoHtmlEntities($value['name']);
            }
        }
    }

    public function setFileSizeMax($value)
    {
        $this->_maxSize = $value;
    }

    public function getFileSizeMax()
    {
        return $this->_maxSize;
    }

    public function onLoad($param)
    {
        if ('' != $this->getFileSizeMax()) {
            $this->MaxFileSize = $this->getFileSizeMax();
        } else {
            $this->MaxFileSize = Atexo_Config::getParameter('MAX_UPLOAD_FILE_SIZE');
        }
    }
}
