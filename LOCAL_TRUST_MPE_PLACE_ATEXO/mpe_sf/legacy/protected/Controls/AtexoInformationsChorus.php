<?php

namespace Application\Controls;

use App\Service\WebServices\WebServicesExec;
use Application\Propel\Mpe\CommonTContratTitulaire;
use Application\Propel\Mpe\CommonTContratTitulaireQuery;
use Application\Propel\Mpe\CommonTTypeContrat;
use Application\Propel\Mpe\CommonTTypeContratQuery;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Chorus\Atexo_Chorus_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Contrat;
use Exception;
use Prado\Prado;

/**
 * Class AtexoInformationsChorus d'affichage des informations Chorus.
 *
 * @author OKO <oumar.konate@atexo.com>
 *
 * @version 1.0
 *
 * @since 2015-place
 *
 * @copyright Atexo 2015
 */
class AtexoInformationsChorus extends MpeTTemplateControl
{
    private ?string $_idContrat = null;
    private ?string $_uuidContrat = null;
    private $_afficherFichierFso = true;
    private string $_colSpan = '5';
    private string $_classeCssTrHead = '';
    private string $_classeCssTrBody = '';
    private bool $_afficherHeader = true;
    private bool $_afficherFooter = true;
    private bool $_afficherColAction = false;
    private string $typeFlux = '';

    /**
     * Modifie la valeur de [_idContrat].
     *
     * @param string $idContrat : identifiant technique du contrat
     *
     * @author OKO <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function setIdContrat($idContrat)
    {
        $this->_idContrat = $idContrat;
    }

    /**
     * Recupere la valeur courante de [_idContrat].
     *
     * @return mixed
     *
     * @author OKO <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function getIdContrat()
    {
        return $this->_idContrat;
    }

    public function getUuidContrat(): ?string
    {
        return $this->_uuidContrat;
    }

    public function setUuidContrat(?string $uuidContrat)
    {
        $this->_uuidContrat = $uuidContrat;
    }

    /**
     * Sauvegarde le contrat dans la session de la page.
     *
     * @param CommonTContratTitulaire $contrat : contrat
     *
     * @author OKO <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function setContrat($contrat)
    {
        $this->Page->setViewState('contrat', $contrat);
    }

    /**
     * Recupere le contrat sauvegarde dans la session de la page.
     *
     * @return mixed
     *
     * @author OKO <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function getContrat()
    {
        return $this->Page->getViewState('contrat');
    }

    /**
     * Modifie la valeur de [_afficherFichierFso].
     *
     * @param $bool
     *
     * @author OKO <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function setAfficherFichierFso($bool)
    {
        $this->_afficherFichierFso = $bool;
    }

    /**
     * Recupere la valeur courante de [_afficherFichierFso].
     *
     * @return bool
     *
     * @author OKO <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function getAfficherFichierFso()
    {
        return $this->_afficherFichierFso;
    }

    /**
     * Modifie la valeur de [_classeCssTrBody].
     *
     * @param string $classe : classe css pour les lignes <tr/>
     *
     * @author OKO <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function setClasseCssTrBody($classe)
    {
        $this->_classeCssTrBody = $classe;
    }

    /**
     * Recupere la valeur courante de [_classeCssTrBody].
     *
     * @return string
     *
     * @author OKO <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function getClasseCssTrBody()
    {
        return $this->_classeCssTrBody;
    }

    /**
     * Modifie la valeur de [_classeCssTrHead].
     *
     * @param string $classe : classe css pour les lignes <tr/>
     *
     * @author OKO <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function setClasseCssTrHead($classe)
    {
        $this->_classeCssTrHead = $classe;
    }

    /**
     * Recupere la valeur courante de [_classeCssTrHead].
     *
     * @return string
     *
     * @author OKO <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function getClasseCssTrHead()
    {
        return $this->_classeCssTrHead;
    }

    /**
     * Modifie la valeur de [_colSpan].
     *
     * @param string $colSpan
     *
     * @author OKO <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function setColSpan($colSpan)
    {
        $this->_colSpan = $colSpan;
    }

    /**
     * Recupere la valeur courante de [_colSpan].
     *
     * @return string
     *
     * @author OKO <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function getColSpan()
    {
        return $this->_colSpan;
    }

    public function setAfficherHeader($bool)
    {
        $this->_afficherHeader = $bool;
    }

    public function getAfficherHeader()
    {
        return $this->_afficherHeader;
    }

    public function setAfficherFooter($bool)
    {
        $this->_afficherFooter = $bool;
    }

    public function getAfficherFooter()
    {
        return $this->_afficherFooter;
    }

    public function getAfficherColAction()
    {
        return $this->_afficherColAction;
    }

    public function setAfficherColAction($value)
    {
        return $this->_afficherColAction = $value;
    }

    /**
     * Permet d'afficher la liste
     *     Pour en savoir plus, consultez le ticket http://jira.local-trust.com/browse/MPE-954.
     *
     * @author OKO <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function displayList()
    {
        try {
            if (Atexo_Config::getParameter('ACTIVE_EXEC_V2')) {
                $contratExec = $this->getContratExec();
                if ($contratExec['type']['codeExterne']) {
                    $this->typeFlux = $this->isContratAcSadExec($contratExec['type']['codeExterne']) ? 'FEN211' : 'FEN111';
                }

                $dataSource = (new Atexo_Chorus_Util())->remplirTableauInformationsChorusExec($contratExec);

                $listeNumMarche = (new Atexo_Chorus_Util())->getListeNumMarcheByNumEj($contratExec['numEj']);;

                if (is_array($listeNumMarche) && count($listeNumMarche)) {
                    foreach ($listeNumMarche as $numMarche) {
                        if (!str_contains($contratExec['statutEJ'], $numMarche)) {
                            $dataSource[count($dataSource) + 1]['numero_marche'] = $numMarche;
                        }
                    }
                }
                if (empty($dataSource)) {
                    $dataSource[0]['numero'] = Prado::localize('TEXT_NON_GENERE');
                    $dataSource[0]['statut'] = Prado::localize('TEXT_SANS_OBJET');
                }
                $this->repeaterInformationsChorus->DataSource = $dataSource;
                $this->repeaterInformationsChorus->DataBind();
            } else {
                $contrat = $this->recupererContrat();
                if ($contrat instanceof CommonTContratTitulaire) {
                    $dataSource = (new Atexo_Chorus_Util())->remplirTableauInformationsChorus($contrat);
                    $listeNumMarche = (new Atexo_Chorus_Util())->getListeNumMarcheByNumEj($contrat->getNumEj());
                    if (is_array($listeNumMarche) && count($listeNumMarche)) {
                        foreach ($listeNumMarche as $numMarche) {
                            if (!str_contains($contrat->getStatutej(), $numMarche)) {
                                $dataSource[count($dataSource) + 1]['numero_marche'] = $numMarche;
                            }
                        }
                    }
                    if (empty($dataSource)) {
                        $dataSource[0]['numero'] = Prado::localize('TEXT_NON_GENERE');
                        $dataSource[0]['statut'] = Prado::localize('TEXT_SANS_OBJET');
                    }
                    $this->repeaterInformationsChorus->DataSource = $dataSource;
                    $this->repeaterInformationsChorus->DataBind();
                }
            }
        } catch (Exception $e) {
            $logger = Atexo_LoggerManager::getLogger('app');
            $logger->error("Erreur lors de l'affichage du tableau des informations CHORUS : ".$e->getMessage());
        }
    }

    /**
     * Permet de recuperer le type de flux.
     *
     * @return string : type de flux
     *
     * @author OKO <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function getFluxChorus()
    {
        $libelle = '';
        if (Atexo_Config::getParameter('ACTIVE_EXEC_V2')) {
            return $this->typeFlux;
        } else if ($this->getContrat() instanceof CommonTContratTitulaire) {
            if ((new Atexo_Consultation_Contrat())->isRetourChorusModeIntegre($this->getContrat()->getIdContratTitulaire())) {
                $libelle = 'FEN211';
            } else {
                $libelle = 'FEN111';
            }
        }

        return $libelle;
    }

    public function isContratAcSadExec(?string $typeContrat): bool
    {
        $typeContratQuery = new CommonTTypeContratQuery();
        $typeContrat = $typeContratQuery->findOneByLibelleTypeContrat('TYPE_CONTRAT_' . $typeContrat);
        if ($typeContrat instanceof CommonTTypeContrat) {
            return Atexo_Config::getParameter('TYPE_CONTRAT_MODE_ECHANGE_CHORUS_INTEGRER') == $typeContrat->getModeEchangeChorus();
        }

        return false;
    }

    /**
     * Permet de trouver le libelle correspondant au statut pour l'EJ.
     *
     * @param string $statut : statut
     *
     * @return mixed|string
     *
     * @author OKO <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function displayStatutEJ($statut)
    {
        if (!$statut || '0' == $statut) {
            return Prado::localize('TEXT_SANS_OBJET');
        } elseif (strstr($statut, (string) Atexo_Util::toUtf8(Atexo_Config::getParameter('CHORUS_STATUT_EJ_COMMANDE')))) {
            return Prado::localize('TEXT_COMMANDE');
        } elseif (strstr($statut, (string) Atexo_Util::toUtf8(Atexo_Config::getParameter('CHORUS_STATUT_EJ_CLOTURE')))) {
            return Prado::localize('TEXT_CLOTURE');
        } elseif (strstr($statut, (string) Atexo_Util::toUtf8(Atexo_Config::getParameter('CHORUS_STATUT_EJ_SUPPRIME')))) {
            return Prado::localize('TEXT_SUPPRIME');
        }

        return '-';
    }

    /**
     * Permet de recuperer le contrat en base de donnees
     * Sauvegarde le contrat dans la session de la page.
     *
     * @return CommonTContratTitulaire|CommonTContratTitulaire[]|mixed
     *
     * @author OKO <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function recupererContrat()
    {
        $contratQuery = new CommonTContratTitulaireQuery(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $contrat = $contratQuery->getTContratTitulaireById($this->getIdContrat());
        $this->setContrat($contrat);

        return $this->getContrat();
    }

    /**
     * Permet de tronquer le nom du fichier au dela de [$nbrCaracteres] caracteres.
     *
     * @param string $nomFichier    : nom du fichier
     * @param string $nbrCaracteres : nombre de caracteres
     *
     * @return string
     *
     * @author OKO <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 2015-place
     *
     * @copyright Atexo 2015
     */
    public function tronquerNomFichier($nomFichier, $nbrCaracteres)
    {
        if (strlen($nomFichier) > 68) {
            return substr($nomFichier, 0, 68);
        }

        return $nomFichier;
    }

    public function getContratExec()
    {
        if (!Atexo_Config::getParameter('ACTIVE_EXEC_V2')) {
            return [];
        }

        $execWS = Atexo_Util::getSfService(WebServicesExec::class);
        return $execWS->getContent('getContratByUuid', $this->_uuidContrat);
    }
}
