<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_CommonReferentiel;
use Application\Service\Atexo\Atexo_Config;
use Prado\Prado;

class DonneesComplementairesFormeJuridiqueGroupementAttributaire extends MpeTTemplateControl
{
    private $_donneeComplementaire;

    public function getDonneeComplementaire()
    {
        return $this->_donneeComplementaire;
    }

    public function setDonneeComplementaire($donneeComplementaire)
    {
        $this->_donneeComplementaire = $donneeComplementaire;
    }

    public function onLoad($param)
    {
        //self::fillFormeGroupement();
    }

    /*
     * remplir liste Forme Groupement
     */
    public function fillFormeGroupement()
    {
        $dataSource = [];
        $dataFormeGroupement = (new Atexo_CommonReferentiel())->retrieveValeurReferentiel(Atexo_Config::getParameter('REFERENTIEL_FORME_GROUPEMENT'));
        $dataSource[0] = (new Atexo_Config())->toPfEncoding(Prado::localize('TEXT_SELECTIONNER').'...');
        if ($dataFormeGroupement) {
            foreach ($dataFormeGroupement as $oneVal) {
                $dataSource[$oneVal->getId()] = (new Atexo_Config())->toPfEncoding($oneVal->getLibelleValeurReferentiel());
            }
        }
        $this->formeGroupement->DataSource = $dataSource;
        $this->formeGroupement->DataBind();
        if ($this->_donneeComplementaire->getIdGroupementAttributaire()) {
            $this->formeGroupement->SelectedValue = $this->_donneeComplementaire->getIdGroupementAttributaire();
        }
    }

    /*
     * fct qui enregistre les donnees concernant le groupement attributaire dans DonneesComplementaire
     */
    public function saveFormeGroupement($donnesComplementaire)
    {
        $this->donneeComplementaire = $donnesComplementaire;
        $idGroupement = $this->formeGroupement->getSelectedValue();
        if (0 != $idGroupement) {
            $this->donneeComplementaire->setIdGroupementAttributaire($idGroupement);
        }
    }
}
