<?php

namespace Application\Controls;

use Prado\Prado;

/**
 * commentaires.
 *
 * @author Mounir LAMANE <mounir.lamane@atexo.com>
 * @copyright Atexo 2010
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class ConsultationHistorique extends MpeTTemplateControl
{
    private $_titleHistorique;
    private $_detailHistorique;
    private $_message;
    private array $_dataSource = [];

    public function getDataSource()
    {
        return $this->_dataSource;
    }

    public function setDataSource($value)
    {
        if (is_array($value)) {
            $this->_dataSource = $value;
            $this->repeaterHistorique->dataSource = $value;
            $this->repeaterHistorique->dataBind();
        }
    }

    public function getTitleHistorique()
    {
        return $this->_titleHistorique;
    }

    public function setTitleHistorique($value)
    {
        $this->_titleHistorique = $value;
    }

    public function getDetailHistorique()
    {
        return $this->_detailHistorique;
    }

    public function setDetailHistorique($value)
    {
        $this->_detailHistorique = $value;
    }

    public function getMessage()
    {
        return match ($this->_message) {
            '1' => Prado::localize('TEXT_PAS_ECHANTILLONS'),
            '2' => Prado::localize('TEXT_PAS_REUNION'),
            '3' => Prado::localize('TEXT_PAS_VISITES_LIEUX'),
            '4' => Prado::localize('TEXT_PAS_AGREMENT'),
            '5' => Prado::localize('TEXT_PAS_QUALIFICATION'),
            '6' => Prado::localize('TEXT_NON'),
            '7' => Prado::localize('TEXT_PAS_CAUTION_PROVISOIRE'),
            '8' => Prado::localize('TEXT_PAS_D_ADRESSE_RETRAIT_DOSSIER'),
            '9' => Prado::localize('TEXT_PAS_D_ADRESSE_DEPOT_OFFRE'),
            '10' => Prado::localize('TEXT_PAS_DE_LIEU_OUVERTURE_PLIS'),
            '11' => Prado::localize('TEXT_NON_RENSIGNEE'),
            default => $this->_message,
        };
    }

    public function setMessage($value)
    {
        $this->_message = $value;
    }
}
