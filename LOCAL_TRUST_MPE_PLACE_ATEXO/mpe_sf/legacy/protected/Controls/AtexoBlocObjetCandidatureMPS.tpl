<com:PanelMessageErreur ID="panelMessageErreur" />
<!--Debut Bloc Objet Candidature-->
<div class="form-field panel panel-default form-horizontal" style="display:block;">
    <div class="panel-heading">
        <h2 class="h4 panel-title">
            <com:TTranslate>DEFINE_OBJET_CANDIDATURE_MPS</com:TTranslate>
        </h2>
    </div>
    <div class="content panel-body">
        <com:TPanel id="panelModif" cssclass="bloc-blue" visible="false">
            <div>
                <strong>
                    <com:TTranslate>DEFINE_LISTE_LOTS_NON_MODIFIABLE_DEPOT_CANDIDATURE_MPS</com:TTranslate>
                </strong>
            </div>
            <!-- Début Bloc Filtre-->
            <!-- single dropdown -->
            <div class="form-group form-group-sm m-t-2">
                <label class="col-md-2 p-t-1">
                    <com:TTranslate>DEFINE_LOTS_SELECTIONNES</com:TTranslate>
                    <span class="champ-oblig text-danger">*</span> :
                </label>
                <div class="col-md-10 filter-field">
                    <com:TActiveListBox
                            id="listeLots"
                            CssClass="form-control"
                            AutoPostBack="true"
                            SelectionMode="Multiple"
                            Attributes.multiple ="multiple"
                            Attributes.title="<%=Prado::localize('DEFINE_LOTS_SELECTIONNES')%>"
                            attributes.onchange ="updateChosenSelect(this,'<%=$this->hiddenSelectedValue->getClientId()%>', 0);"
                    />
                </div>
                <com:THiddenField id="hiddenSelectedValue" />
                <com:TRequiredFieldValidator
                        ValidationGroup="validateInfosCandidatureMPS"
                        ControlToValidate="hiddenSelectedValue"
                        Enabled="true"
                        EnableClientScript="true"
                        Text=" "
                >
                    <prop:ClientSide.OnValidationError>
                        document.getElementById('<%=$this->panelMessageErreur->panelMessage->getClientId()%>').style.display='';
                        document.getElementById('<%=$this->panelMessageErreur->labelMessage->getClientId()%>').innerHTML='<%=Prado::localize("MESSAGE_ERREUR_SELECTION_LOTS_CANDIDATURE_MPS")%>';
                    </prop:ClientSide.OnValidationError>
                </com:TRequiredFieldValidator>
                <script>
                    J("#<%=$this->listeLots->getClientId()%>").chosen({
                        disable_search: false,
                        no_results_text: "<%=Prado::localize('AUCUN_RESULTAT_LOTS')%>",
                        placeholder_text_multiple : "<%=Prado::localize('DEFINE_RECHERCHER_UN_LOT')%>",
                    });

                </script>
            </div>
        </com:TPanel>
        <com:TPanel id="panelAffichage" visible="false">
            <div class="col-md-4">
                <strong>
                    <com:TTranslate>LOTS_CANDIDATURE_DEPOSEE</com:TTranslate>
                </strong>
            </div>
            <div class="col-md-8">
                <ul class="list-unstyled">
                    <com:TRepeater id="listeLotsRecap" >
                        <prop:ItemTemplate>
                            <li>
                                <i class="fa fa-chevron-right m-r-1"></i>
                                <%#$this->Data%>
                            </li>
                        </prop:ItemTemplate>
                    </com:TRepeater>
                </ul>
            </div>
        </com:TPanel>
    </div>
</div>
<!--Fin Bloc Objet Candidature-->