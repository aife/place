<!--Debut bloc detail-->
<div class="bloc-detail">

	<!-- Message d'erreur debut -->
	<com:PanelMessageInformation  Visible = "false" id="panelInfo" message ="<%=Prado::localize('DEFINE_AUCUNE_SIGNATURE_ENVOYE')%>" cssClass="message bloc-650"/>
	<com:PanelMessageAvertissement  Visible = "false" id ="panelAvert" cssClass="message bloc-650"/>
	<com:PanelMessageConfirmation   Visible = "false" id ="panelConfirm" message ="<%=Prado::localize('DEFINE_TOUS_CONTROLS_SIGNATURE_VALIDE')%>" cssClass="message bloc-650" />
	<com:PanelMessageErreur  Visible = "false" id ="panelErreur" cssClass="message bloc-650"/>
	<!-- Message d'erreur Fin -->
	<com:TPanel id="blocDetailCertif">
		<!-- Jeton de signature debut -->
		<com:TActivePanel id="panelJetonSignature" cssClass="bloc" >
			<p><strong><com:TTranslate>DEFINE_JETON_SIGNATURE</com:TTranslate> :</strong> 
			
			<com:THyperLink 
			id="UrldownloadJeton"
			>
			<com:TLabel id="fileName"></com:TLabel>
			</com:THyperLink>
			</p>
		</com:TActivePanel>
		<!--  Jeton de signature fin -->
		
		
		<!-- Bloc certificat debut -->
		<div class="bloc">
			<div class="title"><com:TTranslate>DEFINE_CERTIFICAT_SIGNATURE</com:TTranslate>

				<img alt="Info-bulle"
					 class="picto-info"
					 onmouseout="cacheBulle('infosCertificatSignataire-<%=$this->getHashId() %>')"
					 onmouseover="afficheBulle('infosCertificatSignataire-<%=$this->getHashId()%>', this)"
					 src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif">

			<div onmouseout="mouseOutBulle();"
				 onmouseover="mouseOverBulle();"
				 class="info-bulle"
				 id="infosCertificatSignataire-<%=$this->getHashId()%>">
				<div><com:TTranslate>DEFINE_INFO_BULLE_CERTIFICAT_SIGNATURE</com:TTranslate></div>
			</div>
			</div>
			<div class="column column-xlarge">
				<strong><com:TTranslate>DEFINE_CERTIFICAT_EMIS_A</com:TTranslate> :</strong><br />
				<span class="intitule-certificat">E :</span> <com:TLabel id="identifiantE" /><br />
				<span class="intitule-certificat">CN :</span> <com:TLabel id="identifiantCN" /><br />
				<span class="intitule-certificat">OU :</span> <com:TLabel id="identifiantOU" /><br />
				<span class="intitule-certificat">O :</span> <com:TLabel id="identifiantO" /><br />
				<span class="intitule-certificat">C :</span> <com:TLabel id="identifiantC" /><br />
		   </div>   
			<div class="column column-large">
				<strong><com:TTranslate>CERTIFICAT_EMIS_PAR</com:TTranslate> :</strong><br />
				<span class="intitule-certificat">CN :</span> <com:TLabel id="identifiantCN_PAR" /><br />
				<span class="intitule-certificat">OU :</span> <com:TLabel id="identifiantOU_PAR" /><br />
				<span class="intitule-certificat">O :</span> <com:TLabel id="identifiantO_PAR" /><br />
				<span class="intitule-certificat">C :</span> <com:TLabel id="identifiantC_PAR" /><br />
				<div class="breaker"></div>
			</div>   
			<div class="column">
				<strong><com:TTranslate>TEXT_DATE_DE_VALIDITE</com:TTranslate><com:TLabel id="dateControlevaliditeCr" /> :</strong><br />
				<com:TTranslate>TEXT_A_PARTIR_DU</com:TTranslate> : <com:TLabel id="date_Du" /><br />
				<com:TTranslate>TEXT_JUSQU_AU</com:TTranslate> : <com:TLabel id="date_Au" /><br />
			</div>  
			<br />
			<div class="breaker"></div>
		</div>
		<!-- Bloc certificat fin -->
		
		
		<!-- Bloc controle validite certificat debut-->
		<div class="bloc">
			<div class="column column-xlarge">
				<div class="title">
					<com:TTranslate>DEFINE_CONTROLE_VALIDITATION_CERTIF</com:TTranslate>

					<img class="picto-info"
						 onmouseout="cacheBulle('infosControleValidite-<%=$this->getHashId()%>')"
						 onmouseover="afficheBulle('infosControleValidite-<%=$this->getHashId()%>', this)"
						 alt="Info-bulle"
						 src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif">

					<div onmouseout="mouseOutBulle();"
						 onmouseover="mouseOverBulle();"
						 class="info-bulle" id="infosControleValidite-<%=$this->getHashId()%>">
						<div>
							<com:TTranslate>DEFINE_INFO_BULLE_CONTROLE_VALIDITE</com:TTranslate>
						</div>
					</div>

				</div>
					<com:TTranslate>DEFINE_CONTROLE_REALISES_LE</com:TTranslate> <com:TLabel id="dateControleValidite" text="<%=$this->getDateControleValidite()%>" /><br />
					<com:TTranslate>PERIODE_VALIDITE</com:TTranslate> : <com:PictoStatutSignature  NouveauAffichage="true" ID="periodeValidite" /><br />
					<com:TTranslate>CHAINE_CERTIFICATION</com:TTranslate> :  <com:PictoStatutSignature  NouveauAffichage="true" ID="chaine"  /><br />
					<com:TPanel ID="panelReferentielCertificat" Visible="<%=Application\Service\Atexo\Atexo_Module::isEnabled('SurchargeReferentiels') && $this->fichier->getStatutReferentielCertificat() !== null %>" >
						<div class="indent-arrow"><com:TTranslate>DEFINE_REFERENTIEL_DERTIFICAT</com:TTranslate> :  <span class="<%=$this->getClassCss($this->getStatutReferentielCertificat())%>"><%=$this->getNomReferentielCertificat()%></span></div>
					</com:TPanel>
					<com:TTranslate>DEFINE_NON_REVOCATION</com:TTranslate> : <com:PictoStatutSignature  NouveauAffichage="true" ID="CRL"  /><br />
			</div>  
			<div class="column column-xlarge">
				<div class="title"><com:TTranslate>DEFINE_INTERGRITE_FICHIER_SIGNE</com:TTranslate> 

					<img class="picto-info"
						 onmouseout="cacheBulle('infosIntegriteFichier-<%=$this->getHashId()%>')"
						 onmouseover="afficheBulle('infosIntegriteFichier-<%=$this->getHashId()%>', this)"
						 alt="Info-bulle"
						 src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif">
					<div onmouseout="mouseOutBulle();"
						 onmouseover="mouseOverBulle();"
						 class="info-bulle"
						 id="infosIntegriteFichier-<%=$this->getHashId()%>">
						<div>
							<com:TTranslate>DEFINE_INFO_BULLE_INTEGRITE_FICHIER</com:TTranslate>
						</div>
					</div>

				</div>
					<com:TTranslate>DEFINE_CONTROLE_REALISES_LE</com:TTranslate> <com:TLabel id="dateControleIntergrite" text="<%=$this->getDateControleIntergrite()%>" /><br />
					<com:TTranslate>DEFINE_NON_REPUDIATION</com:TTranslate> <com:PictoStatutSignature  NouveauAffichage="true" ID="NR"  /> <br />
			</div>  
			<div class="breaker"></div>
		</div>
		<!-- Bloc controle validite certificat fin-->
		<!-- BEGIN INFO COMPL -->
		<com:TPanel id="panelInfoComp" cssclass="panel panel-default bloc" >
			<div class="column column-xlarge">
				<div class="title">
					<com:TTranslate>INFOROMATIONS_COMPLEMANTAIRES_SIGNATURE</com:TTranslate>
				</div>
				<div class="clearfix">
					<com:TTranslate>CERTIFICAT_SIGNATURE</com:TTranslate> :
					<com:TLabel id="certificatSignature" />

					<img class="info-bulle"
						 onmouseout="cacheBulle('certificatSignatureInfoBulle-<%=$this->getHashId()%>')"
						 onmouseover="afficheBulle('certificatSignatureInfoBulle-<%=$this->getHashId()%>', this)"
						 alt="Info-bulle" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif">

					<div onmouseout="mouseOutBulle();"
						 onmouseover="mouseOverBulle();"
						 class="info-bulle"
						 id="certificatSignatureInfoBulle-<%=$this->getHashId()%>">
						<div>
							<com:TLabel id="infoBulleQualification" />
						</div>
					</div>
				</div>
				<div class="clearfix">
					<com:TTranslate>FORMAT_SIGNATURE</com:TTranslate> :
					<com:TLabel id="formatSignature" />
				</div>
				<div class="clearfix">
					<com:TTranslate>DATE_INDICATIVE</com:TTranslate> :
					<com:TLabel id="dateIndicative" />
				</div>
				<div class="clearfix">
					<com:TTranslate>JETON_SIGNATURE_HORODATE</com:TTranslate> :
					<com:TLabel id="jetonHorodatage" />
				</div>
			</div>
		</com:TPanel>
		<!-- END INFO COMPL -->
	</com:TPanel>										
	<com:TPanel Cssclass="bloc" id="panelAucunJeton" Visible="false">
		 <p><strong><com:TTranslate>DEFINE_AUCUN_JETON_SIGNATURE</com:TTranslate></strong></p>
	</com:TPanel>
	<com:TPanel Cssclass="bloc" id="panelSignatureNonVerifiable" Visible="false">
		 <p><strong><com:TTranslate>DEFINE_MESSAGE_VALIDITE_JETON_SIGNATURE_NON_VERIFIABLE</com:TTranslate></strong></p>
	</com:TPanel>
</div>