<?php

namespace Application\Controls;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonLtReferentiel;
use Application\Propel\Mpe\CommonLtReferentielPeer;
use Application\Propel\Mpe\CommonReferentielConsultation;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Referentiel\Atexo_Referentiel_Referentiel;
use Application\Service\Atexo\Referentiel\Atexo_Referentiel_ReferentielVo;
use Prado\Prado;

class AtexoLtRefRadio extends MpeTTemplateControl
{
    private bool $consultation = false;
    private bool $lot = false;
    private bool $dependanceLot = false;
    private bool $obligatoire = false;
    private bool $typeSearch = false;
    private $validationGroup;
    private $validationSummary;
    private $cssClass;
    private int $mode = 1;
    private bool $entreprise = false;
    private $idPage;
    private bool $type = false;
    private bool $label = false;
    private bool $check = false;
    private $organisme;
    private bool $line = true;

    /**
     * setter de l'attribut line.
     *
     * @param $Line
     *
     * @author Oussama SALMANI <oussama.salmani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function setLine($Line)
    {
        $this->line = $Line;
    }

    /**
     * getter de l'attribut line.
     *
     * @return la valeur de l'attribut line
     *
     * @author Oussama SALMANI <oussama.salmani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function getLine()
    {
        return $this->line;
    }

    public function setConsultation($consultation)
    {
        $this->consultation = $consultation;
    }

    public function getConsultation()
    {
        return $this->consultation;
    }

    public function setLot($lot)
    {
        $this->lot = $lot;
    }

    public function getLot()
    {
        return $this->lot;
    }

    public function setDependanceLot($dependanceLot)
    {
        $this->dependanceLot = $dependanceLot;
    }

    public function getDependanceLot()
    {
        return $this->dependanceLot;
    }

    public function setObligatoire($obligatoire)
    {
        $this->obligatoire = $obligatoire;
    }

    public function getObligatoire()
    {
        return $this->obligatoire;
    }

    public function setTypeSearch($typeSearch)
    {
        $this->typeSearch = $typeSearch;
    }

    public function getTypeSearch()
    {
        return $this->typeSearch;
    }

    public function setValidationGroup($validationGroup)
    {
        $this->validationGroup = $validationGroup;
    }

    public function getValidationGroup()
    {
        return $this->validationGroup;
    }

    public function setValidationSummary($validationSummary)
    {
        $this->validationSummary = $validationSummary;
    }

    public function getValidationSummary()
    {
        return $this->validationSummary;
    }

    public function setCssClass($cssClass)
    {
        $this->cssClass = $cssClass;
    }

    public function getCssClass()
    {
        return $this->cssClass;
    }

    public function setMode($mode)
    {
        $this->mode = $mode;
    }

    public function getMode()
    {
        return $this->mode;
    }

    public function setEntreprise($entreprise)
    {
        $this->entreprise = $entreprise;
    }

    public function getEntreprise()
    {
        return $this->entreprise;
    }

    public function setIdPage($idPage)
    {
        $this->idPage = $idPage;
    }

    public function getIdPage()
    {
        return $this->idPage;
    }

    public function setType($type)
    {
        $this->type = $type;
    }

    public function getType()
    {
        return $this->type;
    }

    public function setLabel($label)
    {
        $this->label = $label;
    }

    public function getLabel()
    {
        return $this->label;
    }

    public function setCheck($check)
    {
        $this->check = $check;
    }

    public function getCheck()
    {
        return $this->check;
    }

    public function setOrganisme($organisme)
    {
        $this->organisme = $organisme;
    }

    public function getOrganisme()
    {
        if (Atexo_CurrentUser::isAgent()) {
            $organisme = Atexo_CurrentUser::getCurrentOrganism();

            return $organisme;
        } else {
            return null;
        }
    }

    /*
     * afficher la liste des referentiels radio
     */
    public function afficherReferentielRadio($consultationId = null, $lot = null, $organisme = null, $defineValue = null, $postback = false)
    {
        if (!$this->Page->isPostBack || $postback) {
            self::remplirRepeaterReferentielRadio();
            foreach ($this->RepeaterReferentielRadio->Items as $Item) {
                $IdRef = $Item->iDLibelle->Text;
                $codeRefPrinc = null;
                if (!$defineValue) {
                    if ($organisme) {
                        $refCons = (new Atexo_Referentiel_Referentiel())->getCommonLtReferentielByRefConsultationAndLot($consultationId, $lot, $IdRef, $organisme);
                        if ($refCons) {
                            $codeRefPrinc = $refCons->getValeurPrincipaleLtReferentiel();
                        }
                    }
                } else {
                    if ($defineValue[$IdRef]) {
                        $codeRefPrinc = $defineValue[$IdRef]->getValuePrinc();
                    }
                }
                if ('1' == $codeRefPrinc) {
                    $Item->OptionOui->setChecked(true);
                    $Item->OptionNon->setChecked(false);
                    $Item->labelReferentielRadio->SetText(Prado::localize('TEXT_OUI'));
                } elseif ('0' == $codeRefPrinc) {
                    $Item->OptionOui->setChecked(false);
                    $Item->OptionNon->setChecked(true);
                    $Item->labelReferentielRadio->SetText(Prado::localize('TEXT_NON'));
                }
            }
        }
    }

    /*
     * remplir le repeater des referentiels radio
     */
    public function remplirRepeaterReferentielRadio()
    {
        if ('' == $this->idPage) {
            $this->idPage = Atexo_Util::atexoHtmlEntities($_GET['page']);
        }
        $organisme = self::getOrganisme();
        if ($organisme) {
            $dataSource = self::getLtReferentiels($this->idPage, $organisme);
        } else {
            $dataSource = self::getLtReferentiels($this->idPage);
        }
        $this->RepeaterReferentielRadio->DataSource = $dataSource;
        $this->RepeaterReferentielRadio->DataBind();
    }

    /*
     * retourne liste des referentiels radio
     */
    public function getLtReferentiels($page, $organisme = null)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();

        if ($this->entreprise) {
            $c->add(CommonLtReferentielPeer::ENTREPRISE, 1);
        }
        if ($this->consultation) {
            $c->add(CommonLtReferentielPeer::CONSULTATION, 1);
        }
        if ($this->lot) {
            $c->add(CommonLtReferentielPeer::LOT, 1);
        }
        if ($page) {
            $c->add(CommonLtReferentielPeer::PAGES, '%#'.$page.'#%', Criteria::LIKE);
        }
        if ($organisme) {
            $cton1 = $c->getNewCriterion(CommonLtReferentielPeer::ORGANISMES, '%#'.$organisme.'#%', Criteria::LIKE);
            $cton2 = $c->getNewCriterion(CommonLtReferentielPeer::ORGANISMES, null, Criteria::ISNULL);
            $cton3 = $c->getNewCriterion(CommonLtReferentielPeer::ORGANISMES, '');
            $cton2->addOr($cton3);
            $cton1->addOr($cton2);
            $c->add($cton1);
        }

        if ($this->getDependanceLot()) {
            $c->add(CommonLtReferentielPeer::DEPENDANCE_ALLOTISSEMENT, 0);
        }
        $c->add(CommonLtReferentielPeer::TYPE, 2);

        //la clé utilisée pour le cache est un hash unique du critere de recherche
        $keyCache = 'cached_critere_ref_'.sha1(serialize($c));
        $ltRefs = Prado::getApplication()->Cache->get($keyCache);

        //si non existant dans le cache on interroge la BD
        if (!$ltRefs) {
            $ltRefs = CommonLtReferentielPeer::doSelect($c, $connexionCom);
            //si pas de résultats, on stocke en cache un tableau avec une unité vide sinon on aura null et le cache APC ne sera jamais utilisé
            //du coup c'est une feinte
            if (0 == count($ltRefs)) {
                $ltRefs = [''];
            }
            Prado::getApplication()->Cache->set($keyCache, $ltRefs, Atexo_Config::getParameter('TTL_PRADO_CACHE'));
        }
        //on supprime les éléments vides si on les précédemment mis
        if (1 == (is_countable($ltRefs) ? count($ltRefs) : 0) && !($ltRefs[0] instanceof CommonLtReferentiel)) {
            unset($ltRefs[array_search('', $ltRefs)]);
        }

        return $ltRefs;
    }

    /*
     * Retourne les valeur sous format xml
     */
    public function getValeurForXml()
    {
        $valueForXml = '';
        $valeurBalise = '';
        foreach ($this->RepeaterReferentielRadio->getItems() as $Item) {
            $baliseName = (new Atexo_Referentiel_Referentiel())->getNomAttribue($Item->codeLibelle->Text);
            if ($Item->OptionOui->Checked) {
                $refPrinc = '1';
            } elseif ($Item->OptionNon->Checked) {
                $refPrinc = '0';
            }
            $valeurBalise = $refPrinc;
            if ('' != $valeurBalise) {
                $valueForXml[$baliseName] = $valeurBalise;
            } else {
                $valueForXml[$baliseName] = 'NULL';
            }
        }

        return $valueForXml;
    }

    /*
     * retourne les valeurs des referentiels radio
     */
    public function getValueReferentielRadioVo()
    {
        $value1 = [];
        foreach ($this->RepeaterReferentielRadio->Items as $Item) {
            $refVo = new Atexo_Referentiel_ReferentielVo();
            $refVo->setId($Item->iDLibelle->Text);
            if ($Item->OptionOui->Checked) {
                $refPrinc = '1';
            } elseif ($Item->OptionNon->Checked) {
                $refPrinc = '0';
            }
            $refVo->setValue($refPrinc);
            $refVo->setValuePrinc('');
            $refVo->setValueSecon('');
            $refVo->setCodeLibelle($Item->codeLibelle->Text);
            $refVo->setType($Item->typeRef->Text);
            $refVo->setTypeSearch($Item->typeSearch->Text);
            if ('2' == $this->getMode()) {
                if ($Item->OptionIndifferent->Checked) {//ne pas prendr en consideration ce referentiel
                    $value1[$Item->iDLibelle->Text] = new Atexo_Referentiel_ReferentielVo();
                } else {
                    $value1[$Item->iDLibelle->Text] = $refVo;
                }
            } else {
                $value1[$Item->iDLibelle->Text] = $refVo;
            }
        }

        return $value1;
    }

    /*
     * ajout des infos du referentiel a la consultation
     */
    public function saveConsultationAndLot($consultationId, $lot, &$consultation = false)
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $organisme = Atexo_CurrentUser::getCurrentOrganism();
        foreach ($this->RepeaterReferentielRadio->Items as $Item) {
            $IdRef = $Item->iDLibelle->Text;
            if ($Item->OptionOui->Checked) {
                $valeurPricLtRef = '1';
            } elseif ($Item->OptionNon->Checked) {
                $valeurPricLtRef = '0';
            }
            $CommonRefCons = (new Atexo_Referentiel_Referentiel())->getCommonLtReferentielByRefConsultationAndLot($consultationId, $lot, $IdRef, $organisme);
            if (!$CommonRefCons) {
                $CommonRefCons = new CommonReferentielConsultation();
                $CommonRefCons->setConsultationId($consultationId);
                $CommonRefCons->setLot($lot);
                $CommonRefCons->setIdLtReferentiel($IdRef);
                $CommonRefCons->setOrganisme($organisme);
            }
            $CommonRefCons->setValeurPrincipaleLtReferentiel($valeurPricLtRef);
            if ($consultation instanceof CommonConsultation) {
                $consultation->addCommonReferentielConsultation($CommonRefCons);
            } else {
                $CommonRefCons->setReference($consultationId);
                $CommonRefCons->setOrganisme($organisme);
                $CommonRefCons->save($connexionCom);
            }
            unset($CommonRefCons);
        }
    }
}
