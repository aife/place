<?php

namespace Application\Controls;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonInvitationConsultationTransverse;
use Application\Propel\Mpe\CommonInvitePermanentContrat;
use Application\Propel\Mpe\CommonOrganisme;
use Application\Propel\Mpe\CommonOrganismeQuery;
use Application\Propel\Mpe\CommonService;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Service;
use Application\Service\Atexo\Consultation\Atexo_Consultation_InvitationConsTransverse;
use Application\Service\Atexo\Consultation\InvitePermanentContrat;

/**
 *  Block des Entités éligibles aux marchés issus de cette première phase.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class DecisionEntitesEligibles extends MpeTTemplateControl
{
    private $_idRef = null;
    private $_numLot = null;
    private $_org = null;
    private $_postBack = null;
    private $idContrat = null;

    public function setIdRef($value)
    {
        $this->_idRef = $value;
    }

    public function setNumLot($value)
    {
        $this->_numLot = $value;
    }

    public function setOrg($value)
    {
        $this->_org = $value;
    }

    public function getIdRef()
    {
        return $this->_idRef;
    }

    public function getNumLot()
    {
        return $this->_numLot;
    }

    public function getOrg()
    {
        return $this->_org;
    }

    public function setPostBack($value)
    {
        $this->_postBack = $value;
    }

    /**
     * @return null
     */
    public function getIdContrat()
    {
        return $this->idContrat;
    }

    /**
     * @param null $idContrat
     */
    public function setIdContrat($idContrat)
    {
        $this->idContrat = $idContrat;
    }

    public function onLoad($param)
    {
        $this->repeaterSelectedEntitys->DataSource = [];
        $this->myEntityPurchese->Text = null;
        if (Atexo_CurrentUser::getOrganismDesignation()) {
            $this->myEntityPurchese->Text = Atexo_CurrentUser::getOrganismDesignation();
        }

        if (!$this->_postBack) {
            $this->displayListEntityEligible();
            $this->displayListInviteEligible();
        }
    }

    public function displaySelectedEntitesEligibles($sender, $param)
    {
        $idEntite = $this->idEntite->Text;
        if ('' != $idEntite) {
            $this->panelEntityPurchese->setVisible(true);
            $arIdEntite = explode(',', $idEntite);
            Atexo_CurrentUser::writeToSession('idsOrg', $arIdEntite);
            $listeOrgDenomination = Atexo_Organismes::retrieveArrayOrganismesByAcronyme($arIdEntite);
            $this->repeaterSelectedEntitys->DataSource = $listeOrgDenomination;
            $this->repeaterSelectedEntitys->DataBind();
            $this->isMyEntity->Value = '1';
        }
    }

    public function refreshSelectedEntitesEligibles($sender, $param)
    {
        $idEntite = $this->idEntite->Text;
        if ('' != $idEntite) {
            $this->panelEntityPurchese->setVisible(true);
            $arIdEntite = explode(',', $idEntite);
            Atexo_CurrentUser::writeToSession('idsOrg', $arIdEntite);
            $listeOrgDenomination = Atexo_Organismes::retrieveArrayOrganismesByAcronyme($arIdEntite);
            $this->repeaterSelectedEntitys->DataSource = $listeOrgDenomination;
            $this->repeaterSelectedEntitys->DataBind();
        }
    }

    public function addInvitationConsultationTransverse($ref, $dateDecision, $lot, $idContrat, $connexionCommon = null)
    {
        //on est dans une méthode qui fait une modification de la base donc on accède à la base lecture/ecriture
        if (!$connexionCommon) {
            $connexionCommon = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        }
	        foreach ($this->repeaterSelectedEntitys->getItems() as $item)
	        {
	            $invConsTRans = new CommonInvitationConsultationTransverse();
	            $invConsTRans->setDateDecision($dateDecision);
	            $invConsTRans->setLot($lot);
	            $invConsTRans->setOrganismeEmetteur(Atexo_CurrentUser::getCurrentOrganism());
	            $invConsTRans->setOrganismeInvite($item->acronymeEntite->Value);
	            $invConsTRans->setReference($ref);
	            $invConsTRans->setConsultationId($ref);
	            $invConsTRans->setIdContratTitulaire($idContrat);
	            $invConsTRans->save($connexionCommon);
	        }
    }

    public function displayListEntityEligible()
    {
        $listeInv = null;
        if ('' != $this->getNumLot() && '' != $this->getIdRef()) {
            $listeInv = (new Atexo_Consultation_InvitationConsTransverse())->retreiveInvitationConsultationTransverse($this->getIdRef(), $this->getNumLot());
        } elseif ($this->getIdContrat()) {
            $listeInv = (new Atexo_Consultation_InvitationConsTransverse())->retreiveInvitationConsultationTransverseByIdContrat($this->getIdContrat());
        }
        if ($listeInv) {
            $listeOrg = [];
            $arraySaveAcronyme = [];
            $index = 0;
            $idOrgs = '';
            foreach ($listeInv as $oneInv) {
                if (!isset($arraySaveAcronyme[$oneInv->getOrganismeInvite()])) {
                    $organisme = Atexo_Organismes::retrieveOrganismeByAcronyme($oneInv->getOrganismeInvite());
                    if ($organisme instanceof CommonOrganisme) {
                        $listeOrg[$index++] = $organisme;
                        $arraySaveAcronyme[$organisme->getAcronyme()] = $organisme->getAcronyme();
                        $idOrgs .= $organisme->getId().',';
                    }
                }
            }
            $this->repeaterSelectedEntitys->DataSource = $listeOrg;
            $this->repeaterSelectedEntitys->DataBind();
            $this->idEntite->Text = $idOrgs;
            $script = "<script>document.getElementById('spanEntityPurchese').style.display='';";
            $script .= "document.getElementById('spanMyEntity').style.display='none';</script>";
            $this->script->Text = $script;
            $this->isMyEntity->Value = '1';
        }
    }

    public function setDataInvitePermanent($idEntite)
    {
        $this->isMyEntity->Value = '2';
        $this->panelEntityPurchese->setVisible(true);
        $arService = explode(',', $idEntite);
        Atexo_CurrentUser::writeToSession('idsService', $arService);
        $this->idInvite->text = $idEntite;
        $arIdEntite = explode(',', $idEntite);
        $organisme = Atexo_Config::getParameter('exclusionOrganisme');
        $listeServices = Atexo_Service::retrieveServiceByListId($arIdEntite, $organisme);
        $this->repeaterSelectedServices->DataSource = $listeServices;
        $this->repeaterSelectedServices->DataBind();
    }

    public function displaySelectedInviteEligibles($sender, $param)
    {
        $idEntite = $this->idInvite->Text;
        if ('' != $idEntite) {
            $this->panelInvitePermanent->setVisible(true);
            $arIdEntite = explode(',', $idEntite);
            $organisme = Atexo_CurrentUser::getOrganismAcronym();
            $listeServices = [];
            if (in_array('0', $arIdEntite)) {
                $serviceRacine = new CommonService();
                $serviceRacine->setId(0);
                $organismeRacine = (new CommonOrganismeQuery())->findOneByAcronyme($organisme);
                $serviceRacine->setLibelle($organismeRacine->getDenominationOrg());
                $listeServices[] = $serviceRacine;
            }
            $listeServices = array_merge($listeServices, Atexo_Service::retrieveServiceByListId($arIdEntite, $organisme));
            $this->repeaterSelectedServices->DataSource = $listeServices;
            $this->repeaterSelectedServices->DataBind();
            Atexo_CurrentUser::writeToSession('idsOrg', $arIdEntite);
            $this->isMyEntity->Value = '2';
        }
    }

    public function refreshSelectedInviteEligibles($sender, $param)
    {
        $idEntite = $this->idInvite->Text;
        if ('' != $idEntite) {
            $this->panelInvitePermanent->setVisible(true);
            $arIdEntite = explode(',', $idEntite);
            Atexo_CurrentUser::writeToSession('idsOrg', $arIdEntite);
            $organisme = Atexo_CurrentUser::getOrganismAcronym();
            $listeServices = [];
            if (in_array('0', $arIdEntite)) {
                $serviceRacine = new CommonService();
                $serviceRacine->setId(0);
                $organismeRacine = (new CommonOrganismeQuery())->findOneByAcronyme($organisme);
                $serviceRacine->setLibelle($organismeRacine->getDenominationOrg());
                $listeServices[] = $serviceRacine;
            }
            $listeServices = array_merge($listeServices, Atexo_Service::retrieveServiceByListId($arIdEntite, $organisme));
            $this->repeaterSelectedServices->DataSource = $listeServices;
            $this->repeaterSelectedServices->DataBind();
        }
    }

    /**
     * @param $ref
     * @param $dateDecision
     * @param $lot
     * @param $idContrat
     * @param null $connexionCommon
     *
     * @throws \Application\Library\Propel\Exception\PropelException
     */
    public function addInvitePermanentContrat($ref, $dateDecision, $lot, $idContrat, $connexionCommon = null)
    {
        //on est dans une méthode qui fait une modification de la base donc on accède à la base lecture/ecriture
        if (!$connexionCommon) {
            $connexionCommon = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB')
                .Atexo_Config::getParameter('CONST_READ_WRITE'));
        }
        foreach ($this->repeaterSelectedServices->getItems() as $item) {
            $date = (new \DateTime('now'))->format('Y-m-d H:i:s');
            $invConsTRans = new CommonInvitePermanentContrat();
            $invConsTRans->setDateDecision($dateDecision);
            $invConsTRans->setLot($lot);
            $invConsTRans->setOrganisme(Atexo_CurrentUser::getCurrentOrganism());
            $invConsTRans->setService($item->acronymeService->Value);
            $invConsTRans->setIdConsultation($ref);
            $invConsTRans->setIdContratTitulaire($idContrat);
            $invConsTRans->setCreatedAt($date);
            $invConsTRans->setUpdatedAt($date);
            $invConsTRans->save($connexionCommon);
        }
    }

    public function displayListInviteEligible()
    {
        $criteres = [];
        if ('' != $this->getNumLot() && '' != $this->getIdRef()) {
            $criteres = [
                'idConsultation' => $this->getIdRef(),
                'lot' => $this->getNumLot(),
                'organisme' => Atexo_CurrentUser::getOrganismAcronym(),
            ];
        } elseif ($this->getIdContrat()) {
            $criteres = [
                'idContratTitulaire' => $this->getIdContrat(),
            ];
        }

        $listeInv = (new InvitePermanentContrat())->retreiveInvitePermanentConsultationTransverse($criteres);

        if (!empty($listeInv)) {
            $arIdEntite = [];
            foreach ($listeInv as $oneInv) {
                $arIdEntite[] = $oneInv->getService();
            }
            $organisme = Atexo_CurrentUser::getOrganismAcronym();
            $listeServices = [];
            if (in_array('0', $arIdEntite)) {
                $serviceRacine = new CommonService();
                $serviceRacine->setId(0);
                $organismeRacine = (new CommonOrganismeQuery())->findOneByAcronyme($organisme);
                $serviceRacine->setLibelle($organismeRacine->getDenominationOrg());
                $listeServices[] = $serviceRacine;
            }
            $listeServices = array_merge($listeServices, Atexo_Service::retrieveServiceByListId($arIdEntite, $organisme));

            $this->repeaterSelectedServices->DataSource = $listeServices;
            $this->repeaterSelectedServices->DataBind();
            $this->idInvite->Text = implode(',', $arIdEntite);
            $script = "<script>document.getElementById('spanInvitePermanent').style.display='';";
            $script .= "document.getElementById('spanMyEntity').style.display='none';";
            $script .= "document.getElementById('spanEntityPurchese').style.display='none';</script>";
            $this->script->Text = $script;
            $this->isMyEntity->Value = '2';
        }
    }
}
