<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_CommonReferentiel;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Prado\Prado;

/**
 * Classe de Création et modification des sociétés exclues.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2010
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class CreerModifierSocietesExclues extends MpeTPage
{
    public function onLoad($param)
    {
        self::customizeForm();
    }

    public function displayVilleRc()
    {
        $donnees = (new Atexo_CommonReferentiel())->retrieveValeurReferentiel(Atexo_Config::getParameter('VALEUR_REFERENTIEL_VILLE_RC'));
        $data = [];
        $data['0'] = Prado::localize('TEXT_SELECTIONNER').' ...';
        $data['99'] = Prado::localize('JE_PAS_DE_RC');
        if ($donnees) {
            foreach ($donnees as $donnee) {
                $data[$donnee->getLibelle2()] = $donnee->getLibelleValeurReferentiel();
            }
        }
        $this->registreCommerce->DataSource = $data;
        $this->registreCommerce->DataBind();
    }

    public function customizeForm()
    {
        self::displayVilleRc();
        self::isPorteeTotaleClickable();
    }

    public function isPorteeTotaleClickable()
    {
        if (Atexo_CurrentUser::hasHabilitation('PorteeSocietesExcluesTousOrganismes')) {
            $this->portee_totale->setEnabled(true);
        } else {
            $this->portee_totale->setEnabled(false);
        }
    }
}
