<?php

namespace Application\Controls;

use App\Entity\Configuration\PlateformeVirtuelle;
use App\Service\Configuration\PlateformeVirtuelleService;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonQuestionsDce;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Blob;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_ScanAntivirus;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Guests;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_PaniersEntreprises;
use Prado\Prado;

/**
 * Formilaire pour poser la question pour les entreprises.
 *
 * @author Mounir LAMANE <mounir.lamane@atexo.com>
 * @copyright Atexo 2010
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class FormulairePoserQuestion extends MpeTTemplateControl
{
    public $_idQuestion;
    public $_question;
    public $_organisme;
    public $_refrence;

    public function onInit($param)
    {
        //$this->Master->setCalledFrom("entreprise");Atexo_Languages::setLanguageCatalogue($this->Master->getCalledFrom());
    }

    public function onLoad($param)
    {
        $this->panelMessageErreur->setVisible(false);
        if (Atexo_Module::isEnabled('EntreprisePoserQuestionSansPj')) {
            $this->panelPJ->setDisplay('None');
        } else {
            $this->panelPJ->setDisplay('Dynamic');
        }
        $this->_organisme = Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']);
        $this->_refrence = Atexo_Util::atexoHtmlEntities($_GET['id']);
        $this->_question = $this->Page->getViewState('Question');
    }

    /**
     * retourne l'objet question.
     *
     * @return object question
     */
    public function getQuestion()
    {
        if (!($this->_question instanceof CommonQuestionsDce)) {
            $this->_question = $this->Page->getViewState('Question');
        }

        return $this->_question;
    }

    public function setQuestion(CommonQuestionsDce $question)
    {
        $this->_question = $question;
    }

    /**
     * enregistre la question a la question.
     */
    public function save($sender, $param)
    {
        $params = [];
        $refUtilisateur = null;
        $questionCommon = $this->getQuestion();
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        if ($questionCommon instanceof CommonQuestionsDce && $questionCommon->getEmail()) {
            $questionCommon->setDateDepot(date('Y-m-d H:i:s'));
            $questionCommon->setQuestion(htmlentities(str_replace('amp;', '', $this->question->Text)));
            $atexoBlob = new Atexo_Blob();
            if ($this->ajoutFichier->HasFile) {
                $infile = Atexo_Config::getParameter('COMMON_TMP').'questionFile'.session_id().time();
                if (move_uploaded_file($this->ajoutFichier->LocalName, $infile)) {
                    if (filesize($infile)) {
                        //commencer le scan Antivirus
                        $msg = Atexo_ScanAntivirus::startScan($infile);
                        if ($msg) {
                            $this->afficherErreur($msg.'"'.$this->ajoutFichier->FileName.'"');

                            return;
                        }
                        //Fin du code scan Antivirus

                        $fileIdBlob = $atexoBlob->insert_blob($this->ajoutFichier->FileName, $infile, $this->_organisme);
                        $questionCommon->setIdFichier($fileIdBlob);
                        $questionCommon->setNomFichier($this->ajoutFichier->FileName);
                    } else {
                        $this->panelMessageErreur->setVisible(true);
                        $this->panelMessageErreur->setMessage(Prado::localize('DEFINE_PJ_TAILLE'));

                        return false;
                    }
                }
            }
            $questionCommon->save($connexionCom);

            //Ajout de la consultation au panier de l'entreprise
            if (Atexo_Module::isEnabled('PanierEntreprise')) {
                if (!Atexo_Entreprise_PaniersEntreprises::getPanierEntreprise(
                    Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']),
                    Atexo_Util::atexoHtmlEntities($_GET['id']),
                    Atexo_CurrentUser::getIdEntreprise(),
                    Atexo_CurrentUser::getIdInscrit()
                )) {
                    Atexo_Entreprise_PaniersEntreprises::AddConsultationToPanierEntreprise(Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']), Atexo_Util::atexoHtmlEntities($_GET['id']), Atexo_CurrentUser::getIdEntreprise(), Atexo_CurrentUser::getIdInscrit());
                }
            }

            //------ fonction d'envoi de mail vers les agents concernes et l'entreprise qui a poser la question
            $guests = (new Atexo_Consultation_Guests())->getAllInvitedAgents($this->_refrence, $this->_organisme);
            $listeIdsAgents = (new Atexo_Consultation_Guests())->getIdsAgentFromArrayGuestsVo($guests);
            $agents = (new Atexo_Agent())->retrieveAgentsAvecAlerteQuestionEntreprise($listeIdsAgents);

            $params['formulairePoserQuestion'] = true;
            $listIdAgents = Atexo_Consultation_Guests::listAgentConsultationFavoris($this->_refrence, $params);
            //Determination de la reference utilisateur de la consultation
            $objetConsultation = (new Atexo_Consultation())->retrieveConsultation($this->_refrence, $this->_organisme);
            if ($objetConsultation) {
                $refUtilisateur = $objetConsultation->getReferenceUtilisateur();
            }

            $from = Atexo_Config::getParameter('PF_MAIL_FROM');
            $pfName = null;
            $plateformeVirtuelle = null;
            if (!is_null($questionCommon->getPlateformeVirtuelleId())) {
                $plateformeVirtuelle = $questionCommon->getCommonPlateformeVirtuelle($connexionCom);

                $from = $plateformeVirtuelle->getNoReply();
                $pfName = $plateformeVirtuelle->getFromPfName();
            }

	        if($agents && is_array($agents)) {
	            foreach($agents as $agent) {
	                if($agent->getEmail() && ((is_countable($listIdAgents) && in_array($agent->getId(), $listIdAgents) !== false))) {
	        			$corpsMail =  (new Atexo_Message())->getContentMailForQuestionAR($this->_refrence, $this->_organisme, $questionCommon, $agent, $plateformeVirtuelle);
	        			Atexo_Message::simpleMail(
	        			    $from,
                            $agent->getEmail(),
                            Prado::localize('RECEPTION_QUESTION_ELECRONIQUE').' - '.Prado::localize('TEXT_REF').' : '.$refUtilisateur,
                            $corpsMail,
                            $cc = '',
                            $return_path = '',
                            false,
                            true,
                            true,
                            $pfName
                        );
                    }
                }
            }

            // Email depuis la PFV uniquement pour l'inscrit.
            if (!is_null($questionCommon->getPlateformeVirtuelleId())) {
                $plateformeVirtuelle = $questionCommon->getCommonPlateformeVirtuelle($connexionCom);

                $from = $plateformeVirtuelle->getNoReply();
                $pfName = $plateformeVirtuelle->getFromPfName();
            }

            //Envoi Mail Accuse de Reception pour l'inscrit.
            $corpsMailAr = (new Atexo_Message())->getContentMailForQuestionAR($this->_refrence, $this->_organisme, $questionCommon, null, $plateformeVirtuelle);
            Atexo_Message::simpleMail(
                $from,
                $questionCommon->getEmail(),
                Prado::localize('ACCUSE_RECEPTION_QUESTION_ELECRONIQUE')
                .' - '.Prado::localize('TEXT_REF')
                .' : '.$refUtilisateur,
                $corpsMailAr,
                $cc = '',
                $return_path = '',
                false,
                true,
                true,
                $pfName
            );

            //----------------------------------------Fin fonction

            $this->response->redirect('index.php?page=Entreprise.ConfirmationDepotQuestion&id='.$this->_refrence.'&orgAcronyme='.$this->_organisme);
        } else {
            $this->Page->EntrepriseFormulaireDemande->setVisible(true);
            $this->Page->formulairePoserQuestion->setVisible(false);
            $this->Page->validateButton->setVisible(true);
            $this->Page->annulerButtonQuestion->setVisible(true);
            $this->Page->errorPart->setVisible(true);
            $this->Page->panelMessageErreur->setVisible(true);
            $this->Page->panelMessageErreur->setMessage(Prado::localize('DEFINE_ERREUR_SAISIE'));
        }
    }

    /**
     *  retourne a la page detail d'une consultation.
     */
    public function retour($sender, $param)
    {
        $this->response->redirect('index.php?page=Entreprise.EntrepriseDetailConsultation&id='.$this->_refrence.'&orgAcronyme='.$this->_organisme);
    }

    public function afficherErreur($msg)
    {
        $this->panelMessageErreur->setMessage($msg);
        $this->panelMessageErreur->setVisible(true);
    }
}
