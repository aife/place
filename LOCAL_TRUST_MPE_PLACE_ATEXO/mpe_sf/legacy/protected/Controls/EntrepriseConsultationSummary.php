<?php

namespace Application\Controls;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonConsultation;
use Application\Propel\Mpe\CommonTDonneeComplementaire;
use Application\Service\Atexo\Atexo_Agrement;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Qualifications;
use Application\Service\Atexo\Atexo_Ref;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Category;
use Application\Service\Atexo\Consultation\Atexo_Consultation_TypeAvis;
use Application\Service\Atexo\Controller\Atexo_Controller_Front;
use Application\Service\Atexo\EntityPurchase\Atexo_EntityPurchase_RPA;
use Prado\Prado;

/**
 * commentaires.
 *
 * @author anas ZAKI <aanas.zaki@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class EntrepriseConsultationSummary extends MpeTPage
{
    private $_consultation = false;
    private $_reference = false;
    private $_organisme = false;
    private $lieuExecution;
    private $langue;

    public function setReference($reference)
    {
        $this->_reference = $reference;
    }

    public function getReference()
    {
        return $this->_reference;
    }

    public function setOrganisme($organisme)
    {
        $this->_organisme = $organisme;
    }

    public function getOrganisme()
    {
        return $this->_organisme;
    }

    public function setConsultation($consultation)
    {
        $this->_consultation = $consultation;
    }

    public function getConsultation()
    {
        return $this->_consultation;
    }

    public function onLoad($param)
    {
        $this->customizeForm();
        if (isset($_GET['id'])) {
            $this->_reference = Atexo_Util::atexoHtmlEntities($_GET['id']);
        }
        if (isset($_GET['orgAcronyme'])) {
            $this->_organisme = Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']);
        }
        if ($this->_organisme && $this->_reference) {
            $this->getResumeConsultation();
        }
    }

    public function getResumeConsultation()
    {
        $organisme = null;
        if (Atexo_Module::isEnabled('MultiLinguismeEntreprise')) {
            $arrayActiveLangages = Atexo_Languages::retrieveArrayActiveLangages();
            $sessionLang = Atexo_CurrentUser::readFromSession('lang');
            if (isset($_GET['lang']) && in_array($_GET['lang'], $arrayActiveLangages)) {
                $this->langue = Atexo_Util::atexoHtmlEntities($_GET['lang']);
            } elseif (isset($sessionLang) && in_array($sessionLang, $arrayActiveLangages)) {
                $this->langue = $sessionLang;
            } else {
                $this->langue = Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE');
            }
        } else {
            $this->langue = Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE');
        }

        $consultation = (new Atexo_Consultation())->retrieveConsultationForCompagny($this->_reference, $this->_organisme, false);
        $this->_consultation = $consultation;
        $abbreviationLangue = '';
        if ($this->langue != Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE')) {
            $abbreviationLangue = strtoupper(substr($this->langue, 0, 1)).substr($this->langue, 1);
        }

        $typeProcedureTraduit = 'getLibelleTypeProcedure'.$abbreviationLangue;
        $intituleTypeAvisTraduit = 'getIntituleAvis'.$abbreviationLangue;

        if ($consultation instanceof CommonConsultation) {
            $donnesComplementaire = $consultation->getDonneComplementaire();
            $this->dateHeureLimiteRemisePlis->Text = $this->gererStyleDate($consultation->getDateFin(), $consultation);
            if ('' != $consultation->getLieuResidence()) {
                $this->lieuResidence->Text = $consultation->getLieuResidence();
                $this->panelLieuResidence->visible = true;
            }
            if ('0000-00-00 00:00:00' != $consultation->getDatefinLocale() && '' != $consultation->getDatefinLocale()) {
                $this->dateFinLocale->Text = $this->gererStyleDate(substr($consultation->getDatefinLocale(), 0, 16), $consultation);
                $this->panelDatefinLocale->visible = true;
                $this->libelleDateHeureLocale->Text = Prado::localize('MESSAGE_HEURE_LOCALE');
            }

            $this->intitule->Text = $consultation->getIntituleTraduit();
            $this->objet->Text = $consultation->getObjetTraduit();
            $this->reference->Text = $consultation->getReferenceUtilisateur();

            $organisme = Atexo_Organismes::retrieveOrganismeByAcronyme($this->_organisme);
            $getDenominationOrgTraduit = 'getDenominationOrg'.Atexo_Languages::getLanguageAbbreviation($this->langue);

            if (Atexo_Module::isEnabled('AnnoncesMarches')) {
                $this->entitePublique->Text = $consultation->getOrgDenomination();
            } else {
                if ($organisme) {
                    if (0 == strcmp($this->langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE'))
                    || !$organisme->$getDenominationOrgTraduit()) {
                        $this->entitePublique->Text = $organisme->getDenominationOrg();
                    } else {
                        $this->entitePublique->Text = $organisme->$getDenominationOrgTraduit();
                    }
                }
            }

            if (Atexo_Module::isEnabled('LieuxExecution')) {
                $this->lieuExecution = (new Atexo_Consultation())->getLibelleLieuxExecution($this->langue, $consultation);
                $this->lieuxExecutionSuite->Text = trim((new Atexo_Consultation())->getLibelleLieuxExecution($this->langue, $consultation), ',');
                $this->lieuxExecutions->Text = trim(Atexo_Util::truncateTexte((new Atexo_Consultation())->getLibelleLieuxExecution($this->langue, $consultation), 100, true), ',');
                $this->spanLieux->visible = Atexo_Util::isTextTruncated((new Atexo_Consultation())->getLibelleLieuxExecution($this->langue, $consultation));
            }
            $typeAvis = (new Atexo_Consultation_TypeAvis())->retrieveTypeAnnonce($consultation->getIdTypeAvis());
            if ($typeAvis) {
                if ($typeAvis->$intituleTypeAvisTraduit()) {
                    $this->annonce->Text = $typeAvis->$intituleTypeAvisTraduit();
                } else {
                    $this->annonce->Text = $typeAvis->getIntituleAvis();
                }
            }
            //Debut codes nuts
            if (Atexo_Module::isEnabled('CodeNutLtReferentiel')) {
                $this->codesNuts->Text = $this->truncateCodesNuts($consultation->getLibelleCodesNuts($consultation->getCodesNuts(), $this->langue));
                $this->spanCodesNuts->visible = ($this->isCodesNutsTruncated($consultation->getLibelleCodesNuts($consultation->getCodesNuts(), $this->langue))) ? true : false;
                $this->lieuxinfosCodesNutsSuite->Text = $this->getLibelleCodesNuts($consultation->getLibelleCodesNuts($consultation->getCodesNuts(), $this->langue));
            }
            //Fin codes nuts
            $typeProcedure = $consultation->getTypeProcedure();
            if ($typeProcedure) {
                if ($typeProcedure->$typeProcedureTraduit()) {
                    $this->typeProcedure->Text = $typeProcedure->$typeProcedureTraduit();
                } else {
                    $this->typeProcedure->Text = $typeProcedure->getLibelleTypeProcedure();
                }
            }
            if ($consultation->getModePassation() == Atexo_Config::getParameter('MODE_PASSATION_AU_RABAIS')) {
                $this->modePassation->Text = ' | '.Prado::localize('AU_RABAIS');
            } elseif ($consultation->getModePassation() == Atexo_Config::getParameter('MODE_PASSATION_OFFRE_PRIX')) {
                $this->modePassation->Text = ' | '.Prado::localize('SUR_OFFRE_DE_PRIX');
            }

            $this->categoriePrincipale->Text = (new Atexo_Consultation_Category())->retrieveCatgorieConsultationTraduit($consultation->getCategorie(), $this->langue);

            // $consulationPrincipale = array();
            //$consulationSecondaires = array();

            //<!-- module referentiel CPV
            if (Atexo_Module::isEnabled('AffichageCodeCpv') && is_file(ltrim(Atexo_Config::getParameter('PATH_FILE_CPV_CONFIG'), '/'))) {
                $atexoRef = new Atexo_Ref();
                $atexoRef->setCheminFichierConfigXML(Atexo_Config::getParameter('PATH_FILE_CPV_CONFIG'));
                $atexoRef->setPrincipal($consultation->getCodeCpv1());
                $atexoRef->setSecondaires($consultation->getCodeCpv2());
                $this->idAtexoRef->afficherReferentiel($atexoRef);
            }
            $this->idReferentielZoneText->setDependanceLot($consultation->getAlloti());
            $this->idRefRadio->setDependanceLot($consultation->getAlloti());
            $this->ltrefCons->afficherReferentielConsultation($consultation->getId(), 0, $this->_organisme);
            $this->idReferentielZoneText->afficherTextConsultation($consultation->getId(), 0, $this->_organisme, null,true);
            $this->idRefRadio->afficherReferentielRadio($consultation->getId(), 0, $this->_organisme, null,true);
			//-->
            if($consultation->getIdTypeAvis() != Atexo_Config::getParameter("TYPE_AVIS_CONSULTATION")){
				$this->panelDetailsAnnonce->setVisible(true);
				$this->panelDateLimiteRemisePlis->setVisible(false);
				$this->panelDateFinAffichage->setVisible(true);
				$this->detailAnnonce->Text = $consultation->getDetailConsultation();
				$this->pictCertificat->setVisible(false);
				$this->logoDume->setVisible(false);
				$this->pictMpsLogo->setVisible(false);
                $this->panelPictMpsLogoAffichage->setVisible(false);
            }

       		//ne pas afficher le picto certificat electronique pour avis information
			if($consultation->getIdTypeAvis() == Atexo_Config::getParameter("TYPE_AVIS_CONSULTATION")){
				if(Atexo_Module :: isEnabled('MarchePublicSimplifie',$consultation->getOrganisme()) && $consultation->getMarchePublicSimplifie()){
					$this->pictMpsLogo->setVisible(true);
                    $this->panelPictMpsLogoAffichage->setVisible(true);
                    $this->pictMpsLogo->ImageUrl = Atexo_Controller_Front::t()."/images/logo-mps-small.png";
					$this->pictMpsLogo->Attributes->title = Prado::Localize('DEFINE_MARCHE_PUBLIC_SIMPLIFIE');
				} else {
					$this->pictMpsLogo->setVisible(false);
                    $this->panelPictMpsLogoAffichage->setVisible(false);
                }
				$this->pictCertificat->ImageUrl = Atexo_Controller_Front::t()."/images/".$this->returnPictoReponseConsultation($consultation);
				$this->pictCertificat->Attributes->title = $this->returnTitlePictoReponseConsultation($consultation);
                $this->pictCertificat->Attributes->alt = Prado::localize('MARCHE_PUBLIC_SIMPLIFIE');
                $this->logoDume->setVisible(Atexo_Module::isEnabled('InterfaceDume') && $consultation->getDumeDemande());
                $this->logoDume->setText($consultation->getInfoDumeAcheteur());
            }

            if (Atexo_Module::isEnabled('AnnulerConsultation')) {
                if ($consultation->getConsultationAnnulee()) {
                    $this->panelDateAnnulation->setVisible(true);
                    $this->dateAnnulation->Text = Atexo_Util::iso2frnDate($consultation->getDateDecisionAnnulation());
                    $this->pictConsultationAnnulee->setVisible(true);
                    $this->pictConsultationAnnulee->ImageUrl = Atexo_Controller_Front::t().'/images/picto-cons-annulee.gif';
                    $this->pictConsultationAnnulee->Attributes->title = Prado::Localize('CONSULTATION_ANNULEE');
                } else {
                    $this->panelDateAnnulation->setVisible(false);
                    $this->pictConsultationAnnulee->setVisible(false);
                }
            } else {
                $this->panelDateAnnulation->setVisible(false);
                $this->pictConsultationAnnulee->setVisible(false);
            }

            //permet d'afficher a la suite de l'heure de remise des plis une mention comme par exemple "(heure de Paris)"
            //par defaut on affiche rien si la variable n'est pas renseignee dans le fichier de traduction
            $libelleAAfficherPourLocalisation = Prado::localize('DEFINE_HEURE_LOCALISATION');
            if ('DEFINE_HEURE_LOCALISATION' != $libelleAAfficherPourLocalisation) {
                $this->libelleHeureLocalisation->text = $libelleAAfficherPourLocalisation;
            } else {
                $this->libelleHeureLocalisation->text = ' ';
            }
            /// TGR
            if (Atexo_Module::isEnabled('ConsultationDomainesActivites')) {
                $this->domainesActivite->text = "<ul class='default-list'><li>".Atexo_Consultation_Category::displayListeLibelleDomaineByArrayId(trim($consultation->getDomainesActivites(), '#'), '</li><li>').'</li></ul>	';
            }
            // Domaines d'activites Lt-Ref
            if (Atexo_Module::isEnabled('ConsultationDomainesActivitesLtReferentiel')) {
                $atexoCodesDomainesActivites = new Atexo_Ref();
                $atexoCodesDomainesActivites->setCheminFichierConfigXML(Atexo_Config::getParameter('PATH_FILE_DOMAINES_ACTIVITES_CONFIG'));
                $atexoCodesDomainesActivites->setSecondaires($consultation->getDomainesActivites());
                $this->idAtexoRefDomaineActivites->afficherReferentiel($atexoCodesDomainesActivites);
            }
            $this->adresseRetraitDossiers->text = $consultation->getAdresseRetraisDossiersTraduit();
            $this->adresseDepotOffres->text = $consultation->getAdresseDepotOffresTraduit();
            $this->lieuOuverturePlis->text = $consultation->getLieuOuverturePlisTraduit();
            if ($donnesComplementaire instanceof CommonTDonneeComplementaire) {
                $this->prixAcquisitionPlan->text = Atexo_Util::getMontantArronditEspace($donnesComplementaire->getPrixAquisitionPlans(), 2).' '.Prado::localize('TEXT_MAD');
            }

            $rpa = Atexo_EntityPurchase_RPA::retrieveRpaById($consultation->getIdRpa(), $consultation->getOrganisme());
            if ($rpa) {
                if ('' != $rpa->getPrenom() || '' != $rpa->getNom()) {
                    $this->contactAdministratif->text = $rpa->getPrenom().' '.$rpa->getNom();
                } else {
                    $this->contactAdministratif->text = '-';
                }
                if ('' != $rpa->getEmail()) {
                    $this->email->text = $rpa->getEmail();
                } else {
                    $this->email->text = '-';
                }
                if ('' != $rpa->getTelephone()) {
                    $this->telephone->Text = $rpa->getTelephone();
                } else {
                    $this->telephone->Text = '-';
                }
                if ('' != $rpa->getFax()) {
                    $this->telecopieur->text = $rpa->getFax();
                } else {
                    $this->telecopieur->text = '-';
                }
            } else {
                $this->blocDetailContactAdministratif->setVisible(false);
                $this->contactAdministratif->text = '-';
            }

            if ('1' == $consultation->getAlloti()) {
                $this->panelConsultationNonAlotti->setDisplay('None');
            } else {
                $this->panelConsultationNonAlotti->setDisplay('Dynamic');
                if (Atexo_Module::isEnabled('ConsultationCautionProvisoire') && Atexo_Module::isEnabled('DonneesComplementaires', $consultation->getOrganisme())) {
                    if ($donnesComplementaire instanceof CommonTDonneeComplementaire) {
                        $this->cautionProvisoire->text = Atexo_Util::getMontantArronditEspace($donnesComplementaire->getCautionProvisoire(), 2).' '.Prado::localize('TEXT_MAD');
                    }
                }
                if ($libellesQualification = (new Atexo_Qualifications())->displayListeLibelleDomaineByArrayId($consultation->getQualification())) {
                    $textQualification = '';
                    $arrayLibellesQualif = explode(' , ', $libellesQualification);
                    foreach ($arrayLibellesQualif as $oneLibelleQualif) {
                        if ($oneLibelleQualif) {
                            $textQualification .= '<li>'.$oneLibelleQualif.'</li>';
                        }
                    }
                    $this->qualification->Text = "<ul class='default-list'>".$textQualification.'</ul>';
                } else {
                    $this->qualification->Text = '-';
                }
                if ($libellesAgrements = (new Atexo_Agrement())->retrieveLibelleAgrementsByIds($consultation->getAgrements())) {
                    $textAgrements = '';
                    $agLibelles = explode(' , ', $libellesAgrements);
                    foreach ($agLibelles as $oneAgrementLibelle) {
                        if ($oneAgrementLibelle) {
                            $textAgrements .= '<li>'.$oneAgrementLibelle.'</li>';
                        }
                    }
                    $this->agrements->Text = "<ul class='default-list'>".$textAgrements.'</ul>';
                } else {
                    $this->agrements->text = '-';
                }
                if ($donnesComplementaire instanceof CommonTDonneeComplementaire) {
                    if ('' != $donnesComplementaire->getDateLimiteEchantillon() && '' != $donnesComplementaire->getAddEchantillonTraduit()) {
                        $this->dateEchantillons->text = Atexo_Util::iso2frnDateTime($donnesComplementaire->getDateLimiteEchantillon());
                        $this->adresseEchantillons->text = $donnesComplementaire->getAddEchantillonTraduit();
                    } else {
                        $this->labelDateHeureLimiteEchantillon->setVisible(false);
                        $this->dateEchantillons->Text = '-'; //Prado::localize('TEXT_PAS_ECHANTILLONS');
                        $this->adresseEchantillons->setVisible(false);
                    }
                    if ('' != $donnesComplementaire->getDateReunion() && '' != $donnesComplementaire->getDateReunion()) {
                        $this->dateReunion->text = Atexo_Util::iso2frnDateTime($donnesComplementaire->getDateReunion());
                        $this->adresseReunion->text = $donnesComplementaire->getAddReunionTraduit();
                    } else {
                        $this->labelDateHeureReunion->setVisible(false);
                        $this->dateReunion->Text = '-'; //Prado::localize('TEXT_PAS_REUNION');
                        $this->adresseReunion->setVisible(false);
                    }
                    if (Atexo_Module::isEnabled('ConsultationVisiteDesLieux')) {
                        $visitesLieux = (new Atexo_Consultation())->retreiveVisitesLieuxByRef($consultation->getId(), null, $this->_organisme);
                        $this->repeaterVisitesLieux->dataSource = $visitesLieux;
                        $this->repeaterVisitesLieux->dataBind();
                        if (!$visitesLieux) {
                            $this->panelRepeaterVisitesLieux->setVisible(false);
                            $this->panelPasVisitelieu->setVisible(true);
                        }
                    }
                }
                $variante = '';
                if ('1' == $consultation->getVariantes()) {
                    $variante = Prado::Localize('TEXT_OUI');
                } else {
                    $variante = Prado::Localize('TEXT_NON');
                }
                $this->varianteValeur->Text = $variante;
            }
            // Fin TGR
        }

        if ($organisme) {
            $this->entiteAchat->Text = $organisme->getSigle();
        }
        if ($consultation) {
            if (0 == $consultation->getServiceId()) {
                $getDenominationOrgTraduit = 'getDenominationOrg'.Atexo_Languages::getLanguageAbbreviation($this->langue);
                if (0 == strcmp($this->langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE'))
                || !$organisme->$getDenominationOrgTraduit()) {
                    $this->entiteAchat->Text .= ' - '.$organisme->getDenominationOrg();
                } else {
                    $this->entiteAchat->Text .= ' - '.$organisme->$getDenominationOrgTraduit();
                }
            } else {
                $entityPath = (new Atexo_EntityPurchase())->getEntityPath($this->_reference, $this->_organisme);
                if ($entityPath) {
                    $this->entiteAchat->Text .= ' / '.$entityPath;
                }
                $entityLibelle = (new Atexo_EntityPurchase())->getEntityLibelle($consultation->getServiceId(), $this->_organisme, $this->langue);
                if ($entityLibelle) {
                    $this->entiteAchat->Text .= ' - '.$entityLibelle;
                }
            }
        }

        $this->setUrlLots($consultation);
    }

    /**
     * retourne les lieux d'execution.
     */
    public function getLieuxExecution()
    {
        return $this->lieuExecution;
    }

    public function setUrlLots($consultation)
    {
        $arrayLots = null;
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        if ($consultation) {
            $arrayLots = $consultation->getAllLots($connexionCom, $consultation->getOrganisme(), $consultation->getId());
        }
        $nbrLots = is_countable($arrayLots) ? count($arrayLots) : 0;
        if (is_array($arrayLots) && count($arrayLots) > 0) {
            $sessionLang = Atexo_CurrentUser::readFromSession('lang');

            $url = 'index.php?page=Commun.PopUpDetailLots&orgAccronyme='.$consultation->getOrganisme().'&id='.$consultation->getId().'&lang='.$sessionLang;
            $this->linkDetailLots->NavigateUrl = "javascript:popUp('".$url."','yes')";
            $this->nbrLots->Text = $nbrLots.' '.Prado::localize('DEFINE_LOTS').' ';
        } else {
            $this->linkDetailLots->Text = '-';
        }
    }

    public function returnPictoReponseConsultation($consultation)
    {
        //$consultation = new CommonConsultation();
        if ($consultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION')) {
            if (!$consultation->getAutoriserReponseElectronique()) {
                return 'reponse-elec-non.gif';
            } elseif (($consultation->getSignatureOffre()) && ('1' == $consultation->getReponseObligatoire())) {
                return 'reponse-elec-oblig-avec-signature.gif';
            } elseif (($consultation->getSignatureOffre()) && ('0' == $consultation->getReponseObligatoire())) {
                return 'reponse-elec-avec-signature.gif';
            } elseif ((!$consultation->getSignatureOffre()) && ('1' == $consultation->getReponseObligatoire())) {
                return 'reponse-elec-oblig.gif';
            } elseif ((!$consultation->getSignatureOffre()) && ('0' == $consultation->getReponseObligatoire())) {
                return 'reponse-elec.gif';
            }
        } else {
            return '';
        }
    }

    public function returnTitlePictoReponseConsultation($consultation)
    {
        //$consultation = new CommonConsultation();
        if ($consultation->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION')) {
            if (!$consultation->getAutoriserReponseElectronique()) {
                return Prado::localize('TEXT_PAS_REPONSE_POUR_CONSULTATION');
            } elseif (($consultation->getSignatureOffre()) && ('1' == $consultation->getReponseObligatoire())) {
                return Prado::localize('TEXT_REPONSE_OBLIGATOIRE_POUR_CONSULTATION_AVEC_SIGNATURE');
            } elseif (($consultation->getSignatureOffre()) && ('0' == $consultation->getReponseObligatoire())) {
                return Prado::localize('TEXT_REPONSE_NON_OBLIGATOIRE_POUR_CONSULTATION_AVEC_SIGNATURE');
            } elseif ((!$consultation->getSignatureOffre()) && ('1' == $consultation->getReponseObligatoire())) {
                return Prado::localize('TEXT_REPONSE_OBLIGATOIRE_POUR_CONSULTATION_SANS_SIGNATURE');
            } elseif ((!$consultation->getSignatureOffre()) && ('0' == $consultation->getReponseObligatoire())) {
                return Prado::localize('TEXT_REPONSE_NON_OBLIGATOIRE_POUR_CONSULTATION_SANS_SIGNATURE');
            }
        } else {
            return '';
        }
    }

    /***
     * afficher les champs qui sont propre a mpe Maroc
     */
    public function customizeForm()
    {
        if (Atexo_Module::isEnabled('ConsultationDomainesActivites')) {
            $this->panelDomaineActivites->setDisplay('Dynamic');
        } else {
            $this->panelDomaineActivites->setDisplay('None');
        }
        if (Atexo_Module::isEnabled('ConsultationDomainesActivitesLtReferentiel')) {
            $this->panelDomaineActivitesLtRef->setDisplay('Dynamic');
        } else {
            $this->panelDomaineActivitesLtRef->setDisplay('None');
        }
        if (Atexo_Module::isEnabled('ConsultationAdresseRetraisDossiers')) {
            $this->adresseRetraitDossier->setDisplay('Dynamic');
        } else {
            $this->adresseRetraitDossier->setDisplay('None');
        }
        if (Atexo_Module::isEnabled('ConsultationPrixAcquisition')) {
            $this->panelAdresseDepotOffre->setDisplay('Dynamic');
        } else {
            $this->panelAdresseDepotOffre->setDisplay('None');
        }
        if (Atexo_Module::isEnabled('ConsultationLieuOuverturePlis')) {
            $this->panelLieuOuverturePlis->setDisplay('Dynamic');
        } else {
            $this->panelLieuOuverturePlis->setDisplay('None');
        }
        if (Atexo_Module::isEnabled('ConsultationPrixAcquisition') && Atexo_Module::isEnabled('DonneesComplementaires', $_GET['orgAcronyme'])) {
            $this->prixAcquisitionPlans->setDisplay('Dynamic');
        } else {
            $this->prixAcquisitionPlans->setDisplay('None');
        }
        if (Atexo_Module::isEnabled('ConsultationCautionProvisoire') && Atexo_Module::isEnabled('DonneesComplementaires', $_GET['orgAcronyme'])) {
            $this->panelCautionProvisoire->setDisplay('Dynamic');
        } else {
            $this->panelCautionProvisoire->setDisplay('None');
        }
        if (Atexo_Module::isEnabled('ConsultationQualification')) {
            $this->panelQualification->setDisplay('Dynamic');
        } else {
            $this->panelQualification->setDisplay('None');
        }
        if (Atexo_Module::isEnabled('ConsultationAgrement')) {
            $this->panelAgrements->setDisplay('Dynamic');
        } else {
            $this->panelAgrements->setDisplay('None');
        }
        if (Atexo_Module::isEnabled('ConsultationEchantillonsDemandes')) {
            $this->panelEchantillons->setDisplay('Dynamic');
        } else {
            $this->panelEchantillons->setDisplay('None');
        }
        if (Atexo_Module::isEnabled('ConsultationReunion')) {
            $this->panelReunion->setDisplay('Dynamic');
        } else {
            $this->panelReunion->setDisplay('None');
        }
        if (Atexo_Module::isEnabled('ConsultationVisiteDesLieux')) {
            $this->panelVisitesLieux->setDisplay('Dynamic');
        } else {
            $this->panelVisitesLieux->setDisplay('None');
        }
        if (Atexo_Module::isEnabled('ContactAdministratifDansDetailConsultationCoteEntreprise')) {
            $this->panelContcatAdministratif->setDisplay('Dynamic');
        } else {
            $this->panelContcatAdministratif->setDisplay('None');
        }
        if (Atexo_Module::isEnabled('ConsultationVariantesAutorisees') && Atexo_Module::isEnabled('DonneesComplementaires', $_GET['orgAcronyme'])) {
            $this->panelVariante->setDisplay('Dynamic');
        } else {
            $this->panelVariante->setDisplay('None');
        }
    }

    public function truncateCodesNuts($arrayCodesNuts)
    {
        $truncatedText = '';
        $index = 0;
        foreach ($arrayCodesNuts as $oneCodeNut) {
            if (5 == $index) {
                break;
            }
            $truncatedText .= $oneCodeNut.'<br />';
            ++$index;
        }

        return $truncatedText;
    }

    public function isCodesNutsTruncated($arrayCodesNuts)
    {
        return ((is_countable($arrayCodesNuts) ? count($arrayCodesNuts) : 0) > 5) ? true : false;
    }

    public function getLibelleCodesNuts($arrayCodesNuts)
    {
        $libellesNuts = '';
        foreach ($arrayCodesNuts as $oneCodeNut) {
            $libellesNuts .= $oneCodeNut.'<br />';
        }

        return $libellesNuts;
    }

    /**
     * Permet de gerer le style de la date et heure limite de remise des plis.
     *
     * @param string|date $date: la date
     * @return: date en vert si en cours, date en rouge si depasssée
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function gererStyleDate($date, $consultation)
    {
        if ($consultation instanceof CommonConsultation) {
            if (Atexo_Util::comparerDeuxDatesFormatIso($date, date('Y-m-d H:i')) >= 0) {
                return '<span class="text-success"><strong>'.Atexo_Util::iso2frnDateTime($date).'</strong></span>';
            }

            return '<span"><abbr data-toggle="tooltip" title="'.Prado::localize('MESSAGE_DATE_LIMITE_DEPASSEE').'">'.Atexo_Util::iso2frnDateTime($date).'</abbr></span>';
        }
    }
}
