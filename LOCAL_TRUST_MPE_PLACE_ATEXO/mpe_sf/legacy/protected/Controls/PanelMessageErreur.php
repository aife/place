<?php

namespace Application\Controls;

/**
 * Panel des message.
 *
 * @author Mouslim MITALI <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PanelMessageErreur extends MpeTTemplateControl
{
    public string $cssClass = 'message bloc-700';

    public function getCssClass()
    {
        return $this->cssClass;
    }

    public function setCssClass($value)
    {
        $this->cssClass = $value;
    }

    public function setMessage($message)
    {
        $this->labelMessage->Text = $message;
    }

    public function setDisplay($value)
    {
        $this->panelMessage->setDisplay($value);
    }
}
