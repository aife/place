<?php

namespace Application\Controls;

use Application\Propel\Mpe\CommonConsultation;
use Application\Service\Atexo\Atexo_Module;

/**
 * commentaires.
 *
 * @author loubna EZZIANI <loubna.ezziani@atexo.com>
 * @copyright Atexo 2012
 *
 * @version 1.0
 *
 * @since MPE-4.0
 */
class ConsultationAlertes extends MpeTTemplateControl
{
    private $_consultation = false;

    public function getConsultation()
    {
        return $this->_consultation;
    }

    public function setConsultation($consultation)
    {
        return $this->_consultation = $consultation;
    }

    public function onLoad($param)
    {
        if (!($this->getConsultation() instanceof CommonConsultation)) {
            $this->summaryVisible->Visible = false;
        } else {
            if (Atexo_Module::isEnabled('AlerteMetier')) {
                $this->summaryVisible->Visible = true;
                $consultation = $this->getConsultation();
                $dataSource = [];
                $alertes = $consultation->getAlertes();
                if (is_countable($alertes) ? count($alertes) : 0) {
                    $dataSource = $alertes;
                    $this->repeaterAlertes->DataSource = $dataSource;
                    $this->repeaterAlertes->DataBind();
                    $this->alerteNotExiste->Visible = false;
                } else {
                    $this->alerteNotExiste->Visible = true;
                }
            } else {
                $this->summaryVisible->Visible = false;
            }
        }
    }
}
