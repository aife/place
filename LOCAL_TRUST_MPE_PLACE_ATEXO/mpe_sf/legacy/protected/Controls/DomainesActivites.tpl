<com:TPanel ID="panelToggle" CssClass="title-toggle" Attributes.onclick="togglePanel(this,'organismeFormation');" >
	<com:TTranslate>CG76_DOMAINE_ACTIVITE</com:TTranslate>
</com:TPanel>
<div class="panel margin-left-15 clear-both" style="display:<%=($this->getTogglePanelVisible()? 'none':'')%>;" id="organismeFormation">
	<div class="spacer-mini"></div>
	<!--Debut Bloc Liste des domaines-->
	<ul class="check-arbo">
	<com:TRepeater ID="RepeaterNomsDomaines" EnableViewState="true">
		<prop:ItemTemplate>
			<com:THiddenField ID="IdDomaineParent" Value="<%#$this->Data['id']%>"/>
			<li><img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-plus.gif" alt="Ouvrir/Fermer" title="Ouvrir/Fermer" onclick="openSousCat(this,'sous-cat_<%#$this->Data["id"]%>');" /><com:TCheckBox id="el_1" Attributes.title="<%#$this->Data['libelle']%>" cssClass="check" Attributes.onclick="CheckUnCheckAllRepeaterImbrique('<%#$this->el_1->getClientId()%>', '<%#$this->RepeaterNomsDomainesFils->getClientId()%>', 'el_1_ssElement_1');"/><label for="ctl0_CONTENU_PAGE_DomainesActivites_RepeaterNomsDomaines_ctl<%#$this->ItemIndex%>_el_1" CommandName="<%#$this->Data['id']%>"><%#$this->Data['libelle']%></label>
			<ul id="sous-cat_<%#$this->Data['id']%>" style="display:none">
				<com:TRepeater ID="RepeaterNomsDomainesFils" EnableViewState="true" DataSource="<%#$this->Data['fils']%>">
					<prop:ItemTemplate>
						<com:THiddenField ID="IdDomaineFils" Value="<%#$this->Data['id']%>"/>
						<li class="ss-domaine"><com:TCheckBox id="el_1_ssElement_1" Attributes.title="<%#$this->Data['libelle']%>" cssClass="check" /><label for="el_1_ssElement_1" CommandName="<%#$this->Data['id']%>"><%#$this->Data['libelle']%></label></li>
					</prop:ItemTemplate>
				</com:TRepeater>
			</ul>
			</li>
		</prop:ItemTemplate>
	</com:TRepeater>
	</ul>
	<!--Fin Bloc Liste des domaines-->
	<div class="breaker"></div>
</div>
