<com:AtexoValidationSummary  ValidationGroup="Validation" />
<com:TActivePanel id="panelBlocErreur" Display="None">
    <com:PanelMessageErreur ID="messageErreur" Message="<%=Prado::localize('JASPER_REDAC_SERVICE_INDISPONIBLE')%>"/>
</com:TActivePanel>
<com:TActivePanel cssClass="form-field" id="panelRechercheCritere">
    <div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
    <div class="content">
        <com:TRepeater id="repeaterCriters">
            <prop:ItemTemplate>
                <com:THiddenField ID="idCritere" Value="<%#$this->Data->id %>" />
                <com:THiddenField ID="typeCritere" Value="<%#$this->Data->type %>" />

                <com:TPanel cssClass="line" Visible="<%# ($this->Data->type!='singleValueText') || ($this->Data->type=='singleValueText' && !preg_match('#ministere#i', $this->Data->id)) %>">

                    <div class="intitule-150 line-height-normal"> <com:TLabel id ="labelCritere" Text="<%# Application\Service\Atexo\Atexo_Config::toPfEncoding($this->Data->label)%>" />
                        <com:TLabel cssClass="champ-oblig" visible="<%#$this->Data->mandatory%>">*</com:TLabel>
                        :</div>

                    <com:TPanel cssClass="calendar" Visible="<%# ($this->Data->type == 'singleValueDate') ? true : false %> ">
                        <com:TTextBox id="champDate" CssClass="heure" />
                        <com:TImage ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-calendar.gif" Attributes.alt="<%=Prado::localize('DEFINE_CALENDRIER')%>" Attributes.title="<%=Prado::localize('DEFINE_CALENDRIER')%>" Attributes.onclick="popUpCalendar(this,document.getElementById('<%#$this->champDate->getClientId()%>'),'dd/mm/yyyy','');" />
                        <com:TDataTypeValidator	ID="controlDate" ValidationGroup="Validation" ControlToValidate="champDate" DataType="Date"	DateFormat="dd/M/yyyy" Text="<%=Prado::localize('MSG_ERROR_FORMAT_DATE_INVALIDE')%>" Display="Dynamic"/>
                        <com:TRequiredFieldValidator
                                enabled = "<%(isset($this->Data->mandatory) &&  $this->Data->mandatory == 1) ? true : false%>"
                                ControlToValidate="champDate"
                                ValidationGroup="Validation"
                                Display="Dynamic"
                                ErrorMessage="<%# Application\Service\Atexo\Atexo_Config::toPfEncoding($this->Data->label)%>"
                                EnableClientScript="true"
                                Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
                            <prop:ClientSide.OnValidationError>
                                document.getElementById('divValidationSummary').style.display='';
                                document.getElementById('<%=$this->TemplateControl->valider->getClientId()%>').style.display='';
                            </prop:ClientSide.OnValidationError>
                        </com:TRequiredFieldValidator>
                    </com:TPanel>


                    <com:TTextBox id="champText" enabled="<%=$this->Data->readOnly?'false':'true'%>" Text="<%= $this->Data->defaultValue %>" visible="<%#($this->Data->type=='singleValueText') ? 'true' : 'false' %>" />
                    <com:TRequiredFieldValidator
                            enabled = "<%(isset($this->Data->mandatory) &&  $this->Data->mandatory == 1) ? true : false%>"
                            ControlToValidate="champText"
                            ValidationGroup="Validation"
                            Display="Dynamic"
                            ErrorMessage="<%# Application\Service\Atexo\Atexo_Config::toPfEncoding($this->Data->label)%>"
                            EnableClientScript="true"
                            Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
                        <prop:ClientSide.OnValidationError>
                            document.getElementById('divValidationSummary').style.display='';
                            document.getElementById('<%=$this->TemplateControl->valider->getClientId()%>').style.display='';
                        </prop:ClientSide.OnValidationError>
                    </com:TRequiredFieldValidator>
                    <com:TDropDownList id="dropDownListe" CssClass="long" visible="<%#($this->Data->type=='multiSelect')%>"
                                       DataSource="<%#$this->TemplateControl->getDataSourceListe($this->Data->state->options)%>" />


                </com:TPanel>

            </prop:ItemTemplate>
        </com:TRepeater>
        <div class="right">
            <div class="default fr"><span><span><com:TButton CssClass="bouton-moyen float-right" id="valider" ValidationGroup="Validation" Text="<%=Prado::localize('TEXT_VALIDER')%>" onclick="onGenererClick" attributes.title="<%=Prado::localize('TEXT_VALIDER')%>" /></span></span></div>
        </div>
    </div>
    <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
</com:TActivePanel>
<com:TPanel cssClass="form-field" id="panelRechercheResultat" Visible="false">
    <div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
    <div class="content" style =" overflow: auto">
        <h3><com:TTranslate>DEFINE_TEXT_SYNTHESE</com:TTranslate></h3>
        <com:TLiteral id="rapportResultat" />
        <div class="right">
            <div class="file-link float-right">
                <com:TImageButton id="genererRapportPdf"
                                  Text="<%=Prado::localize('TEXT_EXPORT_PDF')%>"
                                  OnCommand ="onGenererRapportClick"
                                  CommandName="genererPDF"
                                  ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-pdf.gif"
                                  attributes.title="<%=Prado::localize('TEXT_EXPORT_PDF')%>" >
                </com:TImageButton>
                <com:TImageButton id="genererRapportXls"
                                  Text="<%=Prado::localize('EXPORT_XLS')%>"
                                  OnCommand ="onGenererRapportClick"
                                  CommandName="genererXLS"
                                  ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-xls.gif"
                                  attributes.title="<%=Prado::localize('EXPORT_XLS')%>" >
                </com:TImageButton>
                <com:TLabel Text="<%= $this->getNomStat()%>"/>
            </div>
        </div>
    </div>
    <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
</com:TPanel>
<div class="breaker"></div>