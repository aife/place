<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Weka\Atexo_Weka_FicheWeka;
use Prado\Prado;

class FicheWeka extends MpeTPage
{
    public function onLoad($param)
    {
        $ficheWeka = (new Atexo_Weka_FicheWeka())->recupererFicheWekaByPage(Atexo_Util::atexoHtmlEntities($_GET['page']));
        $this->listeFichesWeka->dataSource = $ficheWeka;
        $this->listeFichesWeka->dataBind();
    }

    /*
    * retourne le titre de la fiche
    */
    public function getTitre($ficheObject)
    {
        if (!Atexo_Config::getParameter('GET_TITRE_FICHE_WEKA_FROM_MESSAGE')) {
            return $ficheObject->getTitre();
        } else {
            return Prado::localize($ficheObject->getTitre());
        }
    }
}
