<?php

namespace Application\Controls;

use Application\Service\Atexo\Controller\Atexo_Controller_Front;
use Prado\Prado;
use Prado\Web\UI\WebControls\TImage;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PictoDetail extends TImage
{
    public $_parentPage;
    public string $_active = 'true';

    public function onLoad($param)
    {
        if ('true' == $this->getActive()) {
            $this->setImageUrl(Atexo_Controller_Front::t().'/images/picto-details.gif');
        } else {
            $this->setImageUrl(Atexo_Controller_Front::t().'/images/picto-details-inactive.gif');
        }

        if ('ChoixInvites' == $this->getParentPage()) {
            $this->setAlternateText(Prado::localize('ICONE_DETAIL_HABILITATION'));
            $this->setToolTip(Prado::localize('ICONE_DETAIL_HABILITATION'));
        } elseif ('ResultSearch' == $this->getParentPage()) {
            $this->setAlternateText(Prado::localize('ICONE_VOIR_LE_DETAIL'));
            $this->setToolTip(Prado::localize('ICONE_VOIR_LE_DETAIL'));
        } elseif ('FormulaireConsultation' == $this->getParentPage()) {
            $this->setAlternateText(Prado::localize('ICONE_DETAIL_MODIFIER'));
            $this->setToolTip(Prado::localize('ICONE_DETAIL_MODIFIER'));
        } elseif ('CoffreFort' === $this->getParentPage()) {
            $this->setAlternateText(trim(Prado::localize('TEXT_VISUALISER')));
            $this->setToolTip(trim(Prado::localize('TEXT_VISUALISER')));
        }
    }

    public function getParentPage()
    {
        return $this->_parentPage;
    }

    public function setParentPage($value)
    {
        $this->_parentPage = $value;
    }

    public function getActive()
    {
        return $this->_active;
    }

    public function setActive($active)
    {
        $this->_active = $active;
    }
}
