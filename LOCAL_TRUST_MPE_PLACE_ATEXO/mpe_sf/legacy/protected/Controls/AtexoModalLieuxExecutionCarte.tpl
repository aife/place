<com:TConditional condition="$this->TemplateControl->getVisible()">
    <prop:trueTemplate>
        <div class="modal kt-ajaxmodal fade" id="modalLieuxExecutionCarte" tabindex="-1" role="dialog" aria-labelledby="modalLieuxExecutionCarte">
            <div class="modal-dialog modal-lg" role="document">
                <com:TForm Attributes.name="form" Attributes.class="form" Attributes.method="POST">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel"><com:TTranslate>DEFINE_LIEUX_EXECUTION</com:TTranslate></h4>
                        </div>
                        <div class="modal-body">

                            <div class="form-bloc margin-0">
                                <div class="alert alert-info" role="alert">
                                    <span class="fa fa-info-circle m-r-1" aria-hidden="true"></span>
                                    <span><com:TTranslate>DEFINE_MSG_LIEUX_EXECUTION</com:TTranslate></span>
                                </div>
                            </div>


                            <div class="form-horizontal form-group-sm">


                                <div class="popup-moyen" id="container">
                                    <com:THiddenField ID="selectedType1GeoN2" Value=""/>
                                    <h1><com:TTranslate>DEFINE_LIEUX_EXECUTION</com:TTranslate></h1>
                                    <!--Debut Bloc Lieu d'execution-->
                                    <div class="lieux-execution">
                                        <com:PanelMessageInformation ID="panelMessageInformation" />
                                        <com:TRepeater  ID="repeaterGeoN0"  onItemDataBound="itemDataBindGeoN0" >
                                            <prop:ItemTemplate>
                                                <!--Debut toggle France metropolitaine-->
                                                <div class="form-field toggle-panel">
                                                    <div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
                                                    <div class="content">
                                                        <div class="title-toggle" onclick="togglePanel(this,'panel_<%#$this->Data->getId()%>');"><%#$this->TemplateControl->writeValue($this->Data, 'Denomination')%></div>
                                                        <div class="panel" style="display:block;" id="panel_<%#$this->Data->getId()%>">
                                                            <!--Debut line France-->
                                                            <com:THiddenField ID="typeN0" Value="<%#($this->Data->getType()) %>"/>
                                                            <!--Debut line Geolocalisation N0-->
                                                            <div class="line">
                                                                <!--Debut bloc carte france -->
                                                                <com:TPanel visible="<%# ($this->Data->getType()==2) ? true : false %>')">
                                                                    <div class="content-bloc-auto indent-5">
                                                                        <div class="intitule-auto  clear-both" id="localSubitemRadioGroup">
                                                                            <com:TRadioButton  ID="localSubitemSelectionRadio"  GroupName="RadioGroup<%#$this->ItemIndex %>"
                                                                                               Checked="true"
                                                                                               Attributes.id="selectionByLocalSubItem"
                                                                                               Attributes.name="selectionFrance"
                                                                                               Attributes.title="<%=Prado::localize('DEFINE_SELECTIONNER_CARTE_LOCALE')%>"
                                                                                               cssClass="radio"
                                                                                               Attributes.onclick="displayLocalSubitemMap();"
                                                                            />
                                                                            <label for="localSubitemSelectionRadio"><com:TTranslate>DEFINE_SELECTIONNER_CARTE_LOCALE</com:TTranslate></label>
                                                                        </div>
                                                                        <div class="intitule-auto  clear-both" id="subitemRadioGroup">
                                                                            <com:TRadioButton  ID="subitemSelectionRadio"  GroupName="RadioGroup<%#$this->ItemIndex %>"
                                                                                               Attributes.id="selectionBySubItem"
                                                                                               Attributes.name="selectionFrance"
                                                                                               Attributes.title="<%=Prado::localize('DEFINE_SELECTIONNER_CARTE_DEPARTEMENT')%>"
                                                                                               cssClass="radio"
                                                                                               Attributes.onclick="displaySubitemMap();"
                                                                            />
                                                                            <label for="subitemSelectionRadio"><com:TTranslate>DEFINE_SELECTIONNER_CARTE_DEPARTEMENT</com:TTranslate></label>
                                                                        </div>
                                                                        <div class="intitule-auto  clear-both" id="itemRadioGroup">
                                                                            <com:TRadioButton  ID="itemSelectionRadio"  GroupName="RadioGroup<%#$this->ItemIndex %>"
                                                                                               Attributes.id="selectionByItem"
                                                                                               Attributes.name="selectionFrance"
                                                                                               Attributes.title="<%=Prado::localize('DEFINE_SELECTIONNER_CARTE_REGION')%>"
                                                                                               cssClass="radio"
                                                                                               Attributes.onclick="displayItemMap();"
                                                                            />
                                                                            <label for="itemSelectionRadio"><com:TTranslate>DEFINE_SELECTIONNER_CARTE_REGION</com:TTranslate></label>
                                                                        </div>
                                                                        <!--Debut bloc Map-->
                                                                        <div id="blocMap">
                                                                            <div id="canvas" >
                                                                                <div id="paper"></div>
                                                                            </div>
                                                                        </div>
                                                                        <!--Fin bloc Map-->
                                                                        <!-- Debut bloc liste de Regions et Departement selectionnes-->
                                                                        <div class="bloc-selection-map">
                                                                            <div class="intitule-auto bold"><label for="listeLocalisationsSelect"><com:TTranslate>MA_SELECTION_ACTUELLE</com:TTranslate></label></div>
                                                                            <com:TListBox cssClass="liste-region-selectionnes" id="listeLocalisationsSelect" SelectionMode="Multiple" Attributes.name="localisations[]"></com:TListBox>

                                                                            <!--select class="liste-region-selectionnes" multiple="multiple" id="listeLocalisationsSelect" name="localisations"></select-->
                                                                            <div class="vider-liste"><a href="javascript:void(0);"><com:TTranslate>VIDER_MA_SELECTION_ACTUELLE</com:TTranslate></a></div>
                                                                            <div class="effacer-selection"><a href="javascript:void(0);"><com:TTranslate>EFFACER_MA_SELECTION_ACTUELLE</com:TTranslate></a></div>
                                                                        </div>
                                                                        <!-- Fin bloc liste de Regions et Departement selectionnes-->
                                                                    </div>
                                                                </com:TPanel>
                                                                <!--Fin bloc carte France-->
                                                                <div class="breaker"></div>
                                                                <div id="blockGeo<%#$this->ItemIndex%>" style="display:block;">
                                                                    <!--Debut bloc Geolocalisation N0 Type 1-->
                                                                    <com:TPanel Attributes.class="content-bloc-auto indent-20" id="blocFrance">
                                                                        <com:TPanel visible = "<%# ($this->Data->getType()!=2) ? true:false%>" >
                                                                            <div class="intitule-auto  clear-both">
                                                                                <com:TRadioButton  ID="selectiongeoN0Aucun" Checked="true" GroupName="RadioGroup<%#$this->ItemIndex %>"
                                                                                                   Attributes.title="<%#$this->TemplateControl->writeValue($this->Data, 'LibelleAucun')%>" cssClass="radio"
                                                                                                   Attributes.onclick="hideDiv('<%# ($this->Data->getType()==1) ? $this->choixPays->getClientId():$this->choixDept->getClientId()%>');javaScript:opt<%# $this->ItemIndex%>.transferAllLeft();"
                                                                                />
                                                                                <label for="<%# $this->getClientId()%>selectiongeoN0Aucun">
                                                                                    <com:TLabel  Text="<%#$this->TemplateControl->writeValue($this->Data, 'LibelleAucun')%>" />
                                                                                </label>
                                                                            </div>

                                                                            <div class="intitule-auto  clear-both">
                                                                                <com:TRadioButton ID="selectiongeoN0Select" value="<%# $this->Data->getType() %>" GroupName="RadioGroup<%#$this->ItemIndex %>" Attributes.name="selectionFrance"
                                                                                                  Attributes.title="<%#$this->TemplateControl->writeValue($this->Data, 'LibelleSelectionner')%>" cssClass="radio"
                                                                                                  Attributes.onclick="showDiv('<%# ($this->Data->getType()==1) ? $this->choixPays->getClientId():$this->choixDept->getClientId() %>');"
                                                                                />
                                                                                <label for="<%#$this->getClientId() %>selectiongeoN0Select">
                                                                                    <com:TLabel Text="<%#$this->TemplateControl->writeValue($this->Data, 'LibelleSelectionner')%>" />
                                                                                </label>
                                                                            </div>
                                                                        </com:TPanel>
                                                                        <!--Debut bloc Selection Geolocalisation N1 et N2-->
                                                                        <com:TPanel id="choixDept" Attributes.style="display:none;" Attributes.class="liste-depts">
                                                                            <table summary="<%=Prado::localize('DEFINE_LISTE_GEOLOCALISATION_N2')%>">
                                                                                <com:TRepeater ID="repeaterGeoN1"  visible = "<%# ($this->Data->getType()==0) ? true:false%>">
                                                                                    <caption><com:TTranslate>DEFINE_LISTE_GEOLOCALISATION_N2</com:TTranslate></caption>
                                                                                    <prop:HeaderTemplate>
                                                                                        <tr>
                                                                                            <th colspan="2" style="display:none;" abbr="Collectivités">DEFINE_LISTE_GEOLOCALISATION_N2</th>
                                                                                        </tr>
                                                                                    </prop:HeaderTemplate>
                                                                                    <prop:ItemTemplate>
                                                                                        <tr>
                                                                                            <td>
                    									<span style="" >
                    									<div class="title-toggle" onclick="togglePanel(this,'blocCollectivite1_<%# $this->Data[1]->getId().$this->ItemIndex%>');">

                                                        </div>
                    									<div id="blocCollectivite1_<%# $this->Data[1]->getId().$this->ItemIndex%>" style="display:none;" class="clear-both">
                                                            <ul>
                                                                <li>
                                                                    <com:TCheckBox id="region1" Attributes.title="Tous" cssClass="check"
                                                                                   Attributes.onClick="JavaScript:checkAll('repeaterGeoN1','repeaterGeoN1',1,<%#$this->ItemIndex%>,<%=$this->Parent->Parent->Parent->Parent->ItemIndex %>);" />
                                                                    <label for="region1"><%# Prado::localize('DEFINE_TOUS') %></label>
                                                                </li>
                                                                <com:TRepeater  ID="repeaterGeoN2_1" DataSource=<%# Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2::retrieveListGeolocalisation($this->Data[1]) %>>
                                                                    <prop:ItemTemplate>
                                                                        <li>
                                                                            <com:TPanel Enabled="<%#($this->Data->getValeurAvecSousCategorie()==Application\Service\Atexo\Atexo_Config::getParameter('GEO_N2_ACTIF_AVEC_SOUS_CATEGORIE')) ? true:false%>">
                                                                                <com:TCheckBox id="dep1" Value="<%# $this->Data->getId()%>" Attributes.title="" cssClass="check" />
                                                                                <label for="<%# $this->getClientId()%>dep1">
                                                                                    <com:TLabel ID="geoN2Name_1" Text=""/>
                                                                                    <com:THiddenField ID="geoN2Id_1" Value="<%# $this->Data->getId()%>"/>
                                                                                </label>
                                                                                <!-- info bulle -->
                                                                                <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif"
                                                                                     style="display:<%#($this->Data->getValeurAvecSousCategorie()==Application\Service\Atexo\Atexo_Config::getParameter('GEO_N2_ACTIF_AVEC_SOUS_CATEGORIE')) ? none:block%>"
                                                                                     onmouseover="afficheBulle('infosDep1', this)"
                                                                                     onmouseout="cacheBulle('infosDep1')"
                                                                                     class="picto-info-intitule"
                                                                                     alt="Info-bulle"
                                                                                     title="Info-bulle" />
                                                                                <div id="infosDep1" class="info-bulle"
                                                                                     onmouseover="mouseOverBulle();"
                                                                                     onmouseout="mouseOutBulle();">
                                                                                    <div>
                                                                                        <com:TTranslate>TEXT_GEO_N2_ACTIF_AVEC_SOUS_CATEGORIE</com:TTranslate>
                                                                                    </div>
                                                                                </div>
                                                                                <!-- fin  -->
                                                                            </com:TPanel>
                                                                        </li>
                                                                    </prop:ItemTemplate>
                                                                </com:TRepeater>
                                                            </ul>
                                                        </div>
                								</span>
                                                                                            </td>
                                                                                            <td class="">
            								<span style="display:" >
                									<div class="title-toggle" onclick="togglePanel(this,'blocRegion2_<%# $this->Data[1]->getId().$this->ItemIndex%>');">

                                                    </div>
                									<ul id="blocRegion2_<%# $this->Data[1]->getId().$this->ItemIndex%>" style="display:none;">
                                                        <li>
                                                            <com:TCheckBox id="region2" Attributes.title="Tous" cssClass="check"
                                                                           Attributes.onClick="JavaScript:checkAll('repeaterGeoN1','repeaterGeoN1',2,<%#$this->ItemIndex%>,<%=$this->Parent->Parent->Parent->Parent->ItemIndex %>);" />
                                                            <label for="region2"><%# Prado::localize('DEFINE_TOUS') %></label>
                                                        </li>
                                                        <com:TRepeater ID="repeaterGeoN2_2" DataSource=<%# Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2::retrieveListGeolocalisation($this->Data[2]) %>>
                                                            <prop:ItemTemplate>
                                                                <li>
                                                                    <com:TPanel Enabled="<%#($this->Data->getValeurAvecSousCategorie()==Application\Service\Atexo\Atexo_Config::getParameter('GEO_N2_ACTIF_AVEC_SOUS_CATEGORIE')) ? true:false%>">
                                                                        <com:TCheckBox 	id="dep2" Value="<%# $this->Data->getId()%>" Attributes.title="" cssClass="check" />
                                                                        <label for="<%# $this->getClientId()%>dep2">
                                                                            <com:TLabel ID="geoN2Name_2" Text=""/>
                                                                            <com:THiddenField ID="geoN2Id_2" Value="<%# $this->Data->getId()%>"/>
                                                                        </label>
                                                                        <!-- info bulle -->
                                                                        <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif"
                                                                             style="display:<%#($this->Data->getValeurAvecSousCategorie()==Application\Service\Atexo\Atexo_Config::getParameter('GEO_N2_ACTIF_AVEC_SOUS_CATEGORIE')) ? none:''%>"
                                                                             onmouseover="afficheBulle('infosDep2', this)"
                                                                             onmouseout="cacheBulle('infosDep2')"
                                                                             class="picto-info-intitule"
                                                                             alt="Info-bulle"
                                                                             title="Info-bulle" />
                                                                        <div id="infosDep2" class="info-bulle"
                                                                             onmouseover="mouseOverBulle();"
                                                                             onmouseout="mouseOutBulle();">
                                                                            <div>
                                                                                <com:TTranslate>TEXT_GEO_N2_ACTIF_AVEC_SOUS_CATEGORIE</com:TTranslate>
                                                                            </div>
                                                                        </div>
                                                                        <!-- fin  -->
                                                                    </com:TPanel>
                                                                </li>
                                                            </prop:ItemTemplate>
                                                        </com:TRepeater>
                                                    </ul>
                									</span>
                                                                                            </td>
                                                                                            <td class="">
                									<span style="" >
                									<div class="title-toggle" onclick="togglePanel(this,'blocRegion3_<%# $this->Data[1]->getId().$this->ItemIndex%>');">

                                                    </div>
                									<ul id="blocRegion3_<%# $this->Data[1]->getId().$this->ItemIndex%>" style="display:none;">
                                                        <li>
                                                            <com:TCheckBox id="region3" Attributes.title="Tous" cssClass="check"
                                                                           Attributes.onClick="JavaScript:checkAll('repeaterGeoN1','repeaterGeoN1',3,<%#$this->ItemIndex%>,<%=$this->Parent->Parent->Parent->Parent->ItemIndex %>);" />
                                                            <label for="region3"><%# Prado::localize('DEFINE_TOUS') %></label>
                                                        </li>
                                                        <com:TRepeater   ID="repeaterGeoN2_3" DataSource=<%# Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2::retrieveListGeolocalisation($this->Data[3]) %>>
                                                            <prop:ItemTemplate>
                                                                <li>
                                                                    <com:TPanel Enabled="<%#($this->Data->getValeurAvecSousCategorie()==Application\Service\Atexo\Atexo_Config::getParameter('GEO_N2_ACTIF_AVEC_SOUS_CATEGORIE')) ? true:false%>">
                                                                        <com:TCheckBox 	id="dep3" Value="<%# $this->Data->getId()%>" Attributes.title="" cssClass="check" />
                                                                        <label for="<%# $this->getClientId()%>dep3">
                                                                            <com:TLabel ID="geoN2Name_3" Text=""/>
                                                                            <com:THiddenField ID="geoN2Id_3" Value="<%# $this->Data->getId()%>"/>
                                                                        </label>
                                                                        <!-- info bulle -->
                                                                        <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif"
                                                                             style="display:<%#($this->Data->getValeurAvecSousCategorie()==Application\Service\Atexo\Atexo_Config::getParameter('GEO_N2_ACTIF_AVEC_SOUS_CATEGORIE')) ? none:block%>"
                                                                             onmouseover="afficheBulle('infosDep3', this)"
                                                                             onmouseout="cacheBulle('infosDep3')"
                                                                             class="picto-info-intitule"
                                                                             alt="Info-bulle"
                                                                             title="Info-bulle" />
                                                                        <div id="infosDep3" class="info-bulle"
                                                                             onmouseover="mouseOverBulle();"
                                                                             onmouseout="mouseOutBulle();">
                                                                            <div>
                                                                                <com:TTranslate>TEXT_GEO_N2_ACTIF_AVEC_SOUS_CATEGORIE</com:TTranslate>
                                                                            </div>
                                                                        </div>
                                                                        <!-- fin  -->
                                                                    </com:TPanel>
                                                                </li>
                                                            </prop:ItemTemplate>
                                                        </com:TRepeater>
                                                    </ul>
                								</span>
                                                                                            </td>
                                                                                        </tr>
                                                                                    </prop:ItemTemplate>
                                                                                </com:TRepeater>
                                                                            </table>
                                                                        </com:TPanel>
                                                                        <!--Fin bloc Selection Geolocalisation N1 et N2-->
                                                                    </com:TPanel>
                                                                </div>
                                                                <!--Fin bloc Geolocalisation N0 Type 0-->
                                                                <!--Debut bloc Geolocalisation N0 Type 1-->
                                                                <div class="line">
                                                                    <com:TPanel  visible = "<%# ($this->Data->getType()==1) ? true:false%>">
                                                                        <!--Debut Bloc Selection par Geolocalisation N0 Type 1-->
                                                                        <com:TPanel id="choixPays" Attributes.style="display:none;" Attributes.class="autres-pays">
                                                                            <div class="column-auto">
                                                                                <div class="intitule-auto"><label for="list1"><com:TTranslate>DEFINE_LISTE_PAYS</com:TTranslate></label></div>
                                                                                <div class="breaker"></div>
                                                                                <com:TListBox
                                                                                        ID="list1"
                                                                                        DataSource=<%# $this->TemplateControl->retrieveListGeolocalisationType1InArray($this->Data) %>
                                                                                    cssClass="liste-pays list1"
                                                                                    Rows="6"
                                                                                    Attributes.multiple="multiple"
                                                                                    Attributes.ondblclick="javaScript:opt<%# $this->ItemIndex%>.transferLeft();return false;"
                                                                                    Attributes.title="<%# Prado::localize('DEFINE_LISTE_PAYS')%>"/>
                                                                            </div>
                                                                            <div class="transfer-input">
                                                                                <com:TButton Attributes.name="right" Attributes.type="button" Attributes.value="&nbsp;" CssClass="move-right"  Attributes.onClick="javaScript:opt<%# $this->ItemIndex%>.transferRight();return false;" />
                                                                                <com:TButton Attributes.name="right" Attributes.type="button" Attributes.value="<%# Prado::localize('DEFINE_TOUS') %>" CssClass="move-right-all"  Attributes.onClick="javaScript:opt<%# $this->ItemIndex%>.transferAllRight();return false;" />
                                                                                <com:TButton Attributes.name="left" Attributes.type="button" Attributes.value="&nbsp;" CssClass="move-left" Attributes.onClick="javaScript:opt<%# $this->ItemIndex%>.transferLeft();return false;" />
                                                                                <com:TButton Attributes.name="left" Attributes.type="button" Attributes.value="<%# Prado::localize('DEFINE_TOUS') %>"  CssClass="move-left-all" Attributes.onClick="javaScript:opt<%# $this->ItemIndex%>.transferAllLeft();return false;" />
                                                                            </div>
                                                                            <div class="column-auto">
                                                                                <div class="intitule-auto"><label for="list2"><com:TTranslate>DEFINE_MA_SELECTION</com:TTranslate></label></div>
                                                                                <div class="breaker"></div>
                                                                                <com:TListBox ID="list2"  cssClass="liste-pays" Rows="6" Attributes.multiple="multiple" Attributes.ondblclick="javaScript:opt<%# $this->ItemIndex%>.transferLeft();return false;" Attributes.title="<%# Prado::localize('DEFINE_MA_SELECTION')%>"/>
                                                                            </div>
                                                                            <script type="text/javascript">
                                                                                if(document.getElementById('<%# $this->list1->getClientId()%>')){
                                                                                    var list1Name = document.getElementById('<%# $this->list1->getClientId()%>').name;
                                                                                    var list2Name = document.getElementById('<%# $this->list2->getClientId()%>').name;
                                                                                    var opt<%# $this->ItemIndex%> = new OptionTransfer(list1Name, list2Name);
                                                                                    opt<%# $this->ItemIndex%>.init(document.forms[0]);
                                                                                    opt<%# $this->ItemIndex%>.setAutoSort(true);
                                                                                    opt<%# $this->ItemIndex%>.setDelimiter(",");
                                                                                    opt<%# $this->ItemIndex%>.saveRemovedLeftOptions("removedLeft");
                                                                                    opt<%# $this->ItemIndex%>.saveRemovedRightOptions("removedRight");
                                                                                    opt<%# $this->ItemIndex%>.saveAddedLeftOptions("addedLeft");
                                                                                    opt<%# $this->ItemIndex%>.saveAddedRightOptions("addedRight");
                                                                                    opt<%# $this->ItemIndex%>.saveNewLeftOptions("newLeft");
                                                                                    opt<%# $this->ItemIndex%>.saveNewRightOptions("newRight");

                                                                                }
                                                                            </script>

                                                                        </com:TPanel>
                                                                        <!--Fin Bloc Selection par Geolocalisation N0 Type 1-->
                                                                    </com:TPanel>
                                                                </div>
                                                            </div>
                                                            <!--Fin bloc Geolocalisation N0 Type 1-->
                                                            <!--Fin line France-->
                                                        </div>
                                                        <div class="breaker"></div>
                                                    </div>
                                                    <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
                                                </div>
                                                <!--Fin toggle France metropolitaine-->
                                            </prop:ItemTemplate>
                                        </com:TRepeater>
                                        <com:TPanel cssClass="form-bloc bloc-favori" visible="<%=(Application\Service\Atexo\Atexo_CurrentUser::getIdAgentConnected()!= '') ? 'true' : 'false' %>">
                                            <div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
                                            <div class="content">
                                                <div class="ajout-favori">
                                                    <com:TCheckBox id="addFavori" />
                                                    <label for="addFavori"><%=Prado::localize('DEFINE_ENREGISTRER_SELECTION_PREFERENCES') %></label>
                                                    <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info-utile.gif" onmouseover="afficheBulle('infosFavori', this)" onmouseout="cacheBulle('infosFavori')" class="picto-info-intitule" alt="Info-bulle" title="Info-bulle" />
                                                    <div id="infosFavori" class="info-bulle" onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();"><div><com:TTranslate>DEFINE_REMPLACER_PREFERENCES_LIEUX_EXEC</com:TTranslate>.</div></div>
                                                </div>
                                            </div>
                                            <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
                                        </com:TPanel>
                                        <com:THiddenField ID="idListeLocalisation" />
                                    </div>
                                    <!--Fin Bloc Lieu d'execution-->
                                    <!--Debut line boutons-->
                                    <div class="boutons-line">
                                        <com:PictoAnnuler />
                                        <com:PictoValidateButton ID="validateButton"  onClick="retrieveAllSelectedGeo"/>
                                        <com:TLabel ID="script"/>
                                    </div>
                                    <!--Fin line boutons-->
                                </div>
                                <com:TLabel ID="script2"/>
                                <com:TLabel ID="script3"/>

                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="reset"
                                    class="btn btn-default btn-reset" data-dismiss="modal"><%=Prado::localize('DEFINE_ANNULER')%>
                            </button>

                            <button type="submit"
                                    class="btn btn-primary btn-submit"><%=Prado::localize('TEXT_VALIDER')%>
                            </button>
                        </div>
                    </div>
                </com:TForm>
            </div>
        </div>
    </prop:trueTemplate>
</com:TConditional>