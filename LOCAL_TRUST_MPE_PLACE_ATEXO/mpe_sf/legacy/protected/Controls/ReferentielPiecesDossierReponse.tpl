<com:TPanel id="panelPiecesDossierse" cssClass="form-field">
	<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><span class="title"><com:TTranslate>PIECES_DES_DOSSIERS</com:TTranslate></span></div>
	<div class="content">
		<div class="line">
			<div class="intitule-180"><label for="piecesDossierAdmin"><com:TTranslate>PIECES_DOSSIERS_ADMINISTRATIF</com:TTranslate></label> :</div>
			<com:TTextBox TextMode="MultiLine" ID="piecesDossierAdmin"  Attributes.title="<%=Prado::localize('PIECES_DOSSIERS_ADMINISTRATIF')%>" cssClass="long-550 high-150" />
		</div>
		<div class="line">
			<div class="intitule-180"><label for="piecesDossierTech"><com:TTranslate>PIECES_DOSSIERS_TECHNIQUE</com:TTranslate></label> :</div>
			<com:TTextBox TextMode="MultiLine" ID="piecesDossierTech"  Attributes.title="<%=Prado::localize('PIECES_DOSSIERS_TECHNIQUE')%>" cssClass="long-550 high-150" />
		</div>
		<div class="line">
			<div class="intitule-180"><label for="piecesDossierAdditif"><com:TTranslate>PIECES_DOSSIERS_ADDITIF</com:TTranslate></label> :</div>
			<com:TTextBox TextMode="MultiLine" ID="piecesDossierAdditif"  Attributes.title="<%=Prado::localize('PIECES_DOSSIERS_ADDITIF')%>" cssClass="long-550 high-120" />
		</div>
		<div class="breaker"></div>
	</div>
	<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
</com:TPanel>