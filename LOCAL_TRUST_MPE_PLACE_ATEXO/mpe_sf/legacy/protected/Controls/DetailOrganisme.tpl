<div class="form-field form-horizontal" style="display:block;">
	<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
	<div class="content">
		<div class="panel panel-primary">
			<div>
				<h2 class="h3 panel-title">
					<com:TTranslate>DEFINE_DENOMINATION</com:TTranslate>
				</h2>
			</div>
			<div class="panel-body">
				<div class="clearfix line">
					<label class="col-md-2 col-xs-2 col-sm-3 intitule-150">
						<com:TTranslate>DEFINE_LIBELLE_EA_PMI</com:TTranslate> :
					</label>
					<div class="col-md-10 col-xs-10 col-sm-9 content-bloc bloc-590">
						<com:TLabel ID='denomination' />
					</div>
				</div>
				<com:TPanel CssClass="clearfix" Visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('TraduireOrganismeArabe')) ? true:false%>">
					<div class="clearfix line">
						<label class="col-md-2 col-xs-2 col-sm-3 intitule-150">
							<com:TTranslate>DEFINE_LIBELLE_EA_PMI_AR</com:TTranslate> :
						</label>
						<div class="col-md-10 col-xs-10 col-sm-9 content-bloc bloc-590">
							<com:TLabel ID='denomination_ar' />
						</div>
					</div>
				</com:TPanel>
				<div class="clearfix line">
					<label class="col-md-2 col-xs-2 col-sm-3 intitule-150">
						<com:TTranslate>DEFINE_TEXT_SIGLE_EA_PMI</com:TTranslate> :
					</label>
					<div class="col-md-10 col-xs-10 col-sm-9 content-bloc bloc-590">
						<com:TLabel ID='sigle' />
					</div>
				</div>
				<div class="clearfix line">
					<label class="col-md-2 col-xs-2 col-sm-3 intitule-150">
						<com:TTranslate>DEFINE_DESCRIPTION</com:TTranslate> :
					</label>
					<div class="col-md-10 col-xs-10 col-sm-9 content-bloc bloc-590">
						<com:TLabel ID='description' />
					</div>
				</div>
				<com:TPanel Cssclass="clearfix line" Visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('TraduireOrganismeArabe')) ? true:false%>">
					<div class="clearfix line">
						<label class="col-md-2 col-xs-2 col-sm-3 intitule-150">
							<com:TTranslate>FCSP_DESCRIPTION_AR</com:TTranslate> :
						</label>
						<div class="col-md-10 col-xs-10 col-sm-9 content-bloc bloc-590">
							<com:TLabel ID='description_ar' />
						</div>
					</div>
				</com:TPanel>
				<com:TPanel CssClass="clearfix" id="panelCategorie">
					<div class="clearfix line">
						<label class="col-md-2 col-xs-2 col-sm-3 intitule-150">
							<com:TTranslate>TEXT_CATEGORIE_JURIDIQUE</com:TTranslate> :
						</label>
						<div class="col-md-10 col-xs-10 col-sm-9 content-bloc bloc-590">
							<com:TLabel ID='categorie' />
						</div>
					</div>
				</com:TPanel>
				<div class="clearfix line">
					<label class="col-md-2 col-xs-2 col-sm-3 intitule-150">
						<com:TTranslate>DEFINE_TEXT_ADRESSE_PRINCIPAL_EA_PMI</com:TTranslate> :
					</label>
					<div class="col-md-10 col-xs-10 col-sm-9 content-bloc bloc-590">
						<com:TLabel ID='adresseprincipal' />
					</div>
				</div>
				<com:TPanel CssClass="clearfix" Visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('TraduireOrganismeArabe')) ? true:false%>">
					<div class="clearfix line">
						<label class="col-md-2 col-xs-2 col-sm-3 intitule-150">
							<com:TTranslate>DEFINE_TEXT_ADRESSE_PRINCIPAL_EA_PMI_AR</com:TTranslate> :
						</label>
						<div class="col-md-10 col-xs-10 col-sm-9 content-bloc bloc-590">
							<com:TLabel ID='adresseprincipal_ar' />
						</div>
					</div>
				</com:TPanel>
				<div class="clearfix line">
					<label class="col-md-2 col-xs-2 col-sm-3 intitule-150">
						<com:TTranslate>DEFINE_TEXT_ADRESSE_SECONDAIRE_EA_PMI</com:TTranslate> :
					</label>
					<div class="col-md-10 col-xs-10 col-sm-9 content-bloc bloc-590">
						<com:TLabel ID='adressesecondaire' />
					</div>
				</div>
				<com:TPanel CssClass="clearfix" Visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('TraduireOrganismeArabe')) ? true:false%>">
					<div class="clearfix line">
						<label class="col-md-2 col-xs-2 col-sm-3 intitule-150">
							<com:TTranslate>DEFINE_TEXT_ADRESSE_SECONDAIRE_EA_PMI_AR</com:TTranslate> :
						</label>
						<div class="col-md-10 col-xs-10 col-sm-9 content-bloc bloc-590">
							<com:TLabel ID='adressesecondaire_ar' />
						</div>
					</div>
				</com:TPanel>
				<div class="clearfix line">
					<label class="col-md-2 col-xs-2 col-sm-3 intitule-150">
						<com:TTranslate>DEFINE_CODE_POSTAL</com:TTranslate> :
					</label>
					<div class="col-md-10 col-xs-10 col-sm-9 content-bloc bloc-590">
						<com:TLabel ID='codepostal' />
					</div>
				</div>
				<div class="clearfix line">
					<label class="col-md-2 col-xs-2 col-sm-3 intitule-150">
						<com:TTranslate>TEXT_VILLE</com:TTranslate> :
					</label>
					<div class="col-md-10 col-xs-10 col-sm-9 content-bloc bloc-590">
						<com:TLabel ID='ville' />
					</div>
				</div>
				<com:TPanel CssClass="clearfix" Visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('TraduireOrganismeArabe')) ? true:false%>">
					<div class="clearfix line">
						<label class="col-md-2 col-xs-2 col-sm-3 intitule-150">
							<com:TTranslate>TEXT_VILLE_AR</com:TTranslate> :
						</label>
						<div class="col-md-10 col-xs-10 col-sm-9 content-bloc bloc-590">
							<com:TLabel ID='ville_ar' />
						</div>
					</div>
				</com:TPanel>
				<div class="clearfix line">
					<label class="col-md-2 col-xs-2 col-sm-3 intitule-150">
						<com:TTranslate>TEXT_PAYS</com:TTranslate> :
					</label>
					<div class="col-md-10 col-xs-10 col-sm-9 content-bloc bloc-590">
						<com:TLabel ID='pays' />
					</div>
				</div>
				<com:TPanel CssClass="clearfix" Visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('TraduireOrganismeArabe')) ? true:false%>">
					<div class="clearfix line">
						<label class="col-md-2 col-xs-2 col-sm-3 intitule-150">
							<com:TTranslate>TEXT_PAYS_AR</com:TTranslate> :
						</label>
						<div class="col-md-10 col-xs-10 col-sm-9 content-bloc bloc-590">
							<com:TLabel ID='pays_ar' />
						</div>
					</div>
				</com:TPanel>
				<div class="clearfix line">
					<label class="col-md-2 col-xs-2 col-sm-3 intitule-150">
						<com:TTranslate>TEXT_TEL</com:TTranslate> :
					</label>
					<div class="col-md-10 col-xs-10 col-sm-9 content-bloc bloc-590">
						<com:TLabel ID='tel' />
					</div>
				</div>
				<div class="clearfix line">
					<label class="col-md-2 col-xs-2 col-sm-3 intitule-150">
						<com:TTranslate>DEFINE_TEXT_TELCOPIE_EA_PMI</com:TTranslate> :
					</label>
					<div class="col-md-10 col-xs-10 col-sm-9 content-bloc bloc-590">
						<com:TLabel ID='telecopie' />
					</div>
				</div>
				<div class="clearfix line">
					<label class="col-md-2 col-xs-2 col-sm-3 intitule-150">
						<com:TTranslate>DEFINE_EMAIL_EA_PMI</com:TTranslate> :
					</label>
					<div class="col-md-10 col-xs-10 col-sm-9 content-bloc bloc-590">
						<com:TLabel ID='mail' />
					</div>
				</div>
				<div class="clearfix line">
					<label class="col-md-2 col-xs-2 col-sm-3 intitule-150">
						<com:TTranslate>MODE_ORGANISME</com:TTranslate> :
					</label>
					<div class="col-md-10 col-xs-10 col-sm-9 content-bloc bloc-590">
						<com:TLabel ID="typeOrg" />
					</div>
				</div>
				<com:TPanel id="sirenPanel" Cssclass="clearfix">
					<div class="clearfix line">
						<span class="col-md-2 col-xs-2 col-sm-3 intitule-150 col-xs-2">
							<label>
								<com:TTranslate>TEXT_SIREN</com:TTranslate> <com:TTranslate>TEXT_SLASH</com:TTranslate>
							</label>
							<label>
								<com:TTranslate>TEXT_SIRET</com:TTranslate> :
							</label>
						</span>
						<div class="col-md-10 col-xs-10 col-sm-9 content-bloc bloc-590">
							<com:TLabel ID="siret" />
						</div>
					</div>
				</com:TPanel>
				<div class="clearfix line">
					<span class="col-md-2 col-xs-2 col-sm-3 intitule-150 col-xs-2">
						<label>
							<com:TTranslate>TEXT_ID_ENTITE</com:TTranslate> :</label>
					</span>
					<div class="col-md-10 col-xs-10 col-sm-9 content-bloc bloc-590">
						<com:TLabel ID="idEntiteLabel" />
					</div>
				</div>
			</div>
			<div class="clearfix line">
				<!--Debut Line bouton Créer-->
				<com:TPanel ID="buttoncreation" Visible="true">
					<div class="spacer-small"></div>
					<div class="boutons-line">
						<com:TButton
								CssClass="btn btn-sm btn-default bouton-moyen-140 float-left"
								Attributes.value="<%=Prado::localize('DEFINE_RATTACHEMENT_ENTITE_ACHAT_VALUE')%>"
								Attributes.title="<%=Prado::localize('DEFINE_RATTACHEMENT_ENTITE_ACHAT')%>"
								Enabled="<%=$this->entiteValide%>"
								OnClick="RedirectUrlCreation"
						/>
						<com:TButton
								CssClass="bouton-moyen float-right"
								Enabled="<%=$this->entiteValide%>"
								Attributes.value="<%=Prado::localize('BOUTON_MODIFIER')%>"
								Attributes.title="<%=Prado::localize('BOUTON_MODIFIER')%>"
								OnClick="RedirectUrlUpdate"
								Visible="<%=(Application\Service\Atexo\Atexo_CurrentUser::hasHabilitation('HyperAdmin') && Application\Service\Atexo\Atexo_Module::isEnabled('GestionOrganismeParAgent'))%>"
						/>
					</div>

				</com:TPanel>
				<!--Fin Line bouton Créer-->
			</div>
		</div>
	</div>
	<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
</div>
<com:TLabel ID="scriptJS"/>
