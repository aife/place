<?php

namespace Application\Controls;

use Application\Pages\Commun\DownloadFile;
use Application\Propel\Mpe\CommonCategorieLot;
use Application\Propel\Mpe\CommonTComplementFormulaire;
use Application\Propel\Mpe\CommonTDossierFormulaire;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Lots;
use Application\Service\Atexo\FormulaireSub\Atexo_FormulaireSub_Dossier;
use Application\Service\Atexo\FormulaireSub\Atexo_FormulaireSub_Edition;
use Prado\Prado;

/**
 * Page de gestion des demandes de compléments SUB dans le cadre de la reponse electronique.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 1.0
 *
 * @since MPE-4.0
 */
class AtexoDemandeComplementsSub extends MpeTTemplateControl
{
    private $_consultation;
    private $_complementFormulaire;
    protected string $_callDepuisDemandeComplementTransmis = '0'; //precise si le composant est appelé depuis les demandes de compléments ou un formulaire sub normal

    /*
     * Attribut la valeur
     */
    public function setConsultation($value)
    {
        $this->_consultation = $value;
    }

    /*
     * Recupère la valeur
     */
    public function getConsultation()
    {
        return $this->_consultation;
    }

    /*
     * Attribut la valeur
     */
    public function setComplementFormulaire($value)
    {
        $this->_complementFormulaire = $value;
    }

    /*
     * Recupère la valeur
     */
    public function getComplementFormulaire()
    {
        return $this->_complementFormulaire;
    }

    /**
     * Recupère la valeur.
     */
    public function getCallDepuisDemandeComplementTransmis()
    {
        return $this->_callDepuisDemandeComplementTransmis;
    }

    /**
     * Attribut la valeur.
     */
    public function setCallDepuisDemandeComplementTransmis($value)
    {
        $this->_callDepuisDemandeComplementTransmis = $value;
    }

    public function onLoad($param)
    {
        if (Atexo_Module::isEnabled('InterfaceModuleSub') && Atexo_CurrentUser::isConnected()) {
            if (!$this->Page->IsCallback) {
                //Récuperation de la consultation
                if ($this->getComplementFormulaire() instanceof CommonTComplementFormulaire) {
                    $this->setConsultation($this->Page->getConsultation());
                    //Chargement de la liste des lots dans le pop-in
                    $this->idComposantAtexoFormulaireSub->setDossier($this->getDossierFormulaire());
                    $this->idComposantAtexoFormulaireSub->setRefCons($this->getConsultation()->getId());
                    $this->idComposantAtexoFormulaireSub->setOrganisme($this->getConsultation()->getOrganisme());
                    $this->idComposantAtexoFormulaireSub->setListeEditions((new Atexo_FormulaireSub_Dossier())->recupererEditionsFormulaire($this->getDossierFormulaire()->getIdDossierFormulaire()));
                }
            }
        }
    }

    /**
     * Permet de recuperer le dossier.
     */
    public function getDossierFormulaire()
    {
        if ($this->getComplementFormulaire() instanceof CommonTComplementFormulaire) {
            $dossier = (new Atexo_FormulaireSub_Dossier())->getDossierFormulaireById($this->getComplementFormulaire()->getIdDossierFormulaire());
            if ($dossier instanceof CommonTDossierFormulaire) {
                return $dossier;
            }

            return false;
        }
    }

    /**
     * Recupère le nom du dossier.
     */
    public function getNomDossier()
    {
        $nomDossier = '';
        if ($this->getDossierFormulaire() instanceof CommonTDossierFormulaire) {
            if ($this->getDossierFormulaire()->getIdLot()) {
                $nomDossier = Prado::localize('TEXT_LOT').' '.$this->getDossierFormulaire()->getIdLot().' - ';
            }
            if ($this->getDossierFormulaire()->getTypeEnveloppe() == Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE')) {
                $nomDossier .= Prado::localize('DEFINE_DOSSIER_CANDIDATURE');
            } elseif ($this->getDossierFormulaire()->getTypeEnveloppe() == Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE')) {
                $nomDossier .= Prado::localize('DOSSIER_OFFRE_TECHNIQUE');
            } elseif ($this->getDossierFormulaire()->getTypeEnveloppe() == Atexo_Config::getParameter('TYPE_ENV_OFFRE')) {
                $nomDossier .= Prado::localize('DEFINE_DOSSIER_OFFRES');
            } elseif ($this->getDossierFormulaire()->getTypeEnveloppe() == Atexo_Config::getParameter('TYPE_ENV_ANONYMAT')) {
                $nomDossier .= Prado::localize('DOSSIER_OFFRE_ANONYMAT');
            }
            if ($this->getDossierFormulaire()->getIdLot() && $this->getLot() instanceof CommonCategorieLot) {
                $nomDossier .= ' : '.$this->getLot()->getDescription();
            }
        }

        return $nomDossier;
    }

    /**
     * Permet de recuperer l'intitulé du lot.
     */
    public function getIntituleLot()
    {
        $intituleLot = '';
        if ($this->getConsultation()->getAlloti()) {
            $lot = $this->getLot();
            if ($lot) {
                $intituleLot = $lot->getDescriptionTraduite();
            }
        }

        return $intituleLot;
    }

    /**
     * Permet de recuperer l'objet lot instance de CommonCategorieLot.
     */
    public function getLot()
    {
        return (new Atexo_Consultation_Lots())->retrieveCategorieLot($this->getConsultation()->getId(), $this->getDossierFormulaire()->getIdLot(), $this->getConsultation()->getOrganisme());
    }

    /**
     * Recupère le numéro du lot concatené avec l'identifiant de la demande de complément.
     */
    public function getNumLotConcatIdComplement()
    {
        return $this->getDossierFormulaire()->getIdLot().'_'.$this->getComplementFormulaire()->getIdComplementFormulaire();
    }

    /**
     * Permet de recharger le controller "AtexoFormulaireSub" après la callBack.
     */
    public function actionsApresCallBack()
    {
        if ($this->getComplementFormulaire() instanceof CommonTComplementFormulaire) {
            $this->setConsultation($this->Page->getConsultation());
            //Chargement de la liste des lots dans le pop-in
            $this->idComposantAtexoFormulaireSub->setDossier($this->getDossierFormulaire());
            $this->idComposantAtexoFormulaireSub->setRefCons($this->getConsultation()->getId());
            $this->idComposantAtexoFormulaireSub->setOrganisme($this->getConsultation()->getOrganisme());
            //Chargement de la liste des éditions
            $this->idComposantAtexoFormulaireSub->repeaterListeEditions->DataSource = (new Atexo_FormulaireSub_Dossier())->recupererEditionsFormulaire($this->getDossierFormulaire()->getIdDossierFormulaire());
            $this->idComposantAtexoFormulaireSub->repeaterListeEditions->DataBind();
        }
    }

    /**
     * Permet des informations du lots pour construire l'intitulé du dossier.
     */
    public function getInfosLots()
    {
        $infosLot = '';
        if ($this->Page->getConsultation()->getAlloti() && $this->getDossierFormulaire() instanceof CommonTDossierFormulaire) {
            $infosLot = Prado::localize('TEXT_LOT').' '.$this->getDossierFormulaire()->getIdLot().' - '.$this->getIntituleLot();
        }

        return $infosLot;
    }

    /**
     * Permet de préciser la classe de la demande de compléments.
     */
    public function getClasseDateARemettre($complement)
    {
        $classe = '';
        if ($complement instanceof CommonTComplementFormulaire && $complement->getDateARemettre()) {
            if ($complement->getDateARemettre() < date('Y-m-d h:i:s')) {
                $classe = 'time-red';
            }

            return $classe;
        }
    }

    /**
     * Permet de télécharger l'intégralité des éditions.
     */
    public function telechargerIntegraliteEditions($sender, $param)
    {
        $dossier = (new Atexo_FormulaireSub_Dossier())->getDossierFormulaireByIdComplement($param->CommandParameter);
        if ($dossier instanceof CommonTDossierFormulaire) {
            $tmpZipFile = (new Atexo_FormulaireSub_Edition())->genererIntegraliteEditionsParDossier($dossier->getIdDossierFormulaire());
            //Téléchargement du document à la volée
            if (is_file($tmpZipFile)) {
                DownloadFile::downloadFileContent('liste_Edition_'.$dossier->getIdDossierFormulaire().'.zip', file_get_contents($tmpZipFile));
            }

            return;
        }
    }
}
