<com:TActivePanel id="bloc_PubliciteConsultation" cssclass="form-field">
    <div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
    <div class="content">
        <h2><com:TTranslate>PUBLICITE_CONSULTATION</com:TTranslate></h2>
        <!--Debut bloc rappel pub-->
        <div class="panel panel-default with-bg top-panel">
            <div class="panel-body">
                <h3><com:TTranslate>NOUS_VOUS_RAPPELONS_QUE</com:TTranslate></h3>
                <com:TTranslate>RAPPEL_LES_ECHOS</com:TTranslate>
                <div class="spacer-small"></div>
            </div>
        </div>
        <div class="spacer-small"></div>
        <com:TActivePanel id="rappelOngletPublicite" CssClass="panel panel-default with-bg top-panel" >
            <div class="panel-body">
                <com:TTranslate>RAPPEL_COMPLETER_AVIS_BOAMP_DANS_MOL</com:TTranslate>
                <div class="spacer-small"></div>
            </div>
        </com:TActivePanel>
        <!--Fin bloc rappel pub-->
        <!--Debut bloc recap pub-->
        <!-- ce panel n'est pas visible pour la phase 1 -->
        <com:TPanel visible="false" cssclass="form-bloc recap-pub margin-0">
            <div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
            <div class="content">
                <p class="text-bigger"><com:TTranslate>VOUS_AVEZ_INITIE_UNE_PROCEDURE_DE_TYPE</com:TTranslate> <com:TActiveLabel id="typeProcedure" cssclass="blue"/> <com:TTranslate>AVEC_LES_CARACTERISTIQUES_SUIVANTES</com:TTranslate> :</p>
                <div class="line">
                    <div class="intitule-150"><com:TTranslate>TYPE_ENTITE</com:TTranslate> :</div>
                    <strong><com:TActiveLabel id="typeEntite" /></strong>
                </div>
                <com:TActivePanel id="panelSousTypeOrganisme" cssclass="line">
                    <div class="intitule-150"><com:TTranslate>DEFINE_SOUS_TYPE_ORGANISME</com:TTranslate> :</div>
                    <strong><com:TActiveLabel id="libelleSousTypeOrganisme" /></strong>
                </com:TActivePanel>
                <div class="line">
                    <div class="intitule-150"><com:TTranslate>DEFINE_NATURE_MARCHE</com:TTranslate> :</div>
                    <strong><com:TActiveLabel id="libelleCategorie" /></strong>
                </div>
                <com:TActivePanel id="panelArticle" cssclass="line">
                    <div class="intitule-150"><com:TTranslate>ARTICLE_CONSULTATION</com:TTranslate> :</div>
                    <strong><com:TActiveLabel id="article" /></strong>
                </com:TActivePanel>
                <div class="line">
                    <div class="intitule-150"><com:TTranslate>MONTANT_ESTIME</com:TTranslate> :</div>
                    <strong><com:TActiveLabel id="montantEstime" /> <com:TTranslate>MONTANT_ESTIME_EURO_HT</com:TTranslate></strong>
                </div>
                <div class="spacer-small"></div>
                <com:TActivePanel id="panelFormulairePropose" >
                    <p class="text-bigger"><com:TTranslate>BOAMP_PROPOSITION_TEXTE</com:TTranslate> <com:TActiveLabel id="formulairePropose" cssclass="blue"/> </p>
                </com:TActivePanel>
            </div>
            <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
        </com:TPanel>
        <!--Fin blo. recap pub-->
        <div class="spacer-small"></div>
        <!--Debut bloc recap progression-->
        <com:TPanel visible="false" cssclass="form-field">
            <div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
            <div class="content">
                <p class="text-bigger"><com:TTranslate>VOUS_AVEZ_COMPLETE</com:TTranslate> <span class="blue"><com:TActiveLabel id="pourcentageTotal1" cssclass="updateProgress"/>%</span> <com:TTranslate>DU_FORMULAIRE_DE_PUBLICITE</com:TTranslate></p>
                <com:TActivePanel id="panelBlocManquantCons">
                    <p><com:TTranslate>IL_VOUS_RESTE</com:TTranslate> <strong class="blue"><com:TActiveLabel id="nbreChampsCons" /></strong> <com:TTranslate>CHAMPS_COMPLETER_FORMULAIRE_CREATION_CONSULTATION</com:TTranslate></p>
                    <ul class="default-list">
                        <com:TActiveRepeater id="listeChampsManquantsCons">
                            <prop:ItemTemplate>
                                <li><com:TActiveLinkButton visible="<%#$this->SourceTemplateControl->getMode()%>" oncallback="SourceTemplateControl.getToOnglet" ActiveControl.CallbackParameter="<%#$this->Data['ONGLET']%>" text="<%#$this->Data['LIBELLE_CHAMPS']%>" />
                                    <com:TActiveLabel visible="<%#!$this->SourceTemplateControl->getMode()%>" text="<%#$this->Data['LIBELLE_CHAMPS']%>" /></li>
                            </prop:ItemTemplate>
                        </com:TActiveRepeater>
                    </ul>
                    <div class="spacer"></div>
                </com:TActivePanel>
                <com:TActivePanel id="panelBlocManquantBoamp">
                    <p><com:TTranslate>IL_VOUS_RESTE</com:TTranslate> <strong class="blue"><com:TActiveLabel id="nbreChampsBoamp" /></strong> <com:TTranslate>CHAMP_COMPLETER_FORMULAIRE_COMPTE_BOAMP</com:TTranslate></p>
                    <ul class="default-list">
                        <com:TActiveRepeater id="listeChampsManquantsBoamp">
                            <prop:ItemTemplate>
                                <li><%#$this->Data['LIBELLE_CHAMPS']%></li>
                            </prop:ItemTemplate>
                        </com:TActiveRepeater>
                    </ul>
                    <div class="spacer-mini"></div>
                    <com:TActiveLinkButton id="urlCompteBoamp" cssclass="bouton margin-left-10" OnCallback="goToCompteBoamp"><span class="left">&nbsp;</span><com:TTranslate>COMPLETER</com:TTranslate></com:TActiveLinkButton>
                </com:TActivePanel>
                <div class="breaker"></div>
            </div>
            <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
        </com:TPanel>
        <!--Fin bloc recap progression-->

        <!-- Content for Popover publicité -->
        <div class="popover popover-progression hidden">
            <div class="popover-heading"><com:TTranslate>FORMULAIRE_PUBLICITE</com:TTranslate></div>
            <div class="popover-body">
                <p><com:TTranslate>VOUS_AVEZ_COMPLETE</com:TTranslate> <strong class="blue"><com:TActiveLabel id="pourcentageTotal2" cssclass="updateProgress"></com:TActiveLabel>% </strong> <com:TTranslate>DU_FORMULAIRE</com:TTranslate>
                    <com:TActivePanel id="panelInfoPub" ><com:TTranslate>IL_NE_VOUS_RESTE_PLUS_QUE</com:TTranslate> <strong class="blue"><com:TActiveLabel cssclass="updateInputPub" id="nbreChampsManquant" /></strong> <com:TTranslate>CHAMPS_MANQUANTS_PUB</com:TTranslate></com:TActivePanel></p>
            </div>
        </div>
        <com:TActiveLabel id="scriptJs" Display="None"/>
        <com:TActiveHiddenField id="pourcentageBoamp" />
        <!-- End Content for Popover publicité -->

        <!--Debut bloc Choix du support-->

                    <com:TActiveRepeater id="listeSupport">
                        <prop:HeaderTemplate>
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <h3><com:TTranslate>CHOIX_SUPPORT_PUBLICITE_CONSULTATION</com:TTranslate></h3>
                                    <div class="table-bloc">
                                        <div class="table-results liste-supports margin-0 divtable">
                                            <div class="divtable-thead">
                                                <div class="tr">
                                                    <div class="td check-col" scope="col"><com:TTranslate>SELECTIONNER_SUPPORT_PUBLICITE_CONSULTATION</com:TTranslate></div>
                                                    <div class="td image-col"><com:TTranslate>SUPPORT_PUBLICATION</com:TTranslate></div>
                                                    <div class="td select-col"><com:TTranslate>TYPE_AVIS_OFFRE</com:TTranslate></div>
                                                    <div class="td actions"><com:TTranslate>DEFINE_ACTIONS</com:TTranslate></div>
                                                </div>
                                            </div>
                                            <div class="divtable-tbody">
                        </prop:HeaderTemplate>
                        <prop:ItemTemplate>
                            <com:TLiteral visible="<%#($this->Data->getTagDebutFinGroupe() == '1')%>">
                            <div class="tr-groupe">
                            </com:TLiteral>
                                <div class="tr" >
                                    <div class="td check-col"  >
                                        <com:TActiveCheckBox id="choixSupport"
                                                                                 AutoPostBack="false"
                                                                                 CssClass="<%#($this->Data->getCode()) ? '': 'supportMOL'. $this->Data->getGroupe()%>"
                                                                                 Attributes.onChange="<%#($this->Data->getCode() || $this->Data->getNbreTotalGroupe() == 1) ? '': 'choixSupportPubTypeAvis(this, \'choixOffre'.$this->ItemIndex . '_td\',' . $this->Data->getGroupe() .');'%> gererAffichaDetailgeSupport(this);"
                                                                                 checked="<%#$this->TemplateControl->getSupportChecked($this->Data)%>"
                                                                                 visible="<%#$this->Data->getActif()%>"
                                                                                 attributes.title="<%#$this->Data->getNom()%>"

                                        >

                                        </com:TActiveCheckBox>
                                        <com:THiddenField id="affichageMessageSupport" value="<%#$this->Data->getAffichageMessageSupport()%>" />
                                        <com:THiddenField id="selectionDepartementsParution" value="<%#$this->Data->getSelectionDepartementsParution()%>" />
                                        <com:THiddenField id="departementsParution" value="<%#$this->Data->getDepartementsParution()%>" />
                                        <com:THiddenField id="elementActif" value="<%#$this->Data->getActif()%>" />
                                    </div>
                                    <div class="td image-col">
                                        <img src="<%#$this->Data->getLogo()%>" alt="<%#$this->Data->getNom()%>" title="<%#$this->Data->getNom()%>" class="icone" />
                                    </div>
                                    <div class="td select-col">
                                        <div class="select-col-content">
                                            <div id="choixOffre<%#$this->ItemIndex%>_td" class="<%#($this->Data->getCode()) ? '': 'supportTypeAvisMOL'.$this->Data->getGroupe()%>" style="<%#($this->Data->getCode() ||$this->Data->getNbreTotalGroupe() == 1 || (!$this->Data->getCode() && $this->TemplateControl->getSupportChecked($this->Data))) ? 'display:': 'display:none'%>">
                                                <com:TConditional condition="$this->Data instanceOf Application\Propel\Mpe\CommonTSupportPublication && $this->Data->getActif()">
                                                    <prop:trueTemplate>
                                                        <select data-placeholder="<%=Prado::localize('AUCUNE_OFFRE_COMPATIBLE')%>"
                                                                class="select-with-image liste-offres"
                                                                tabindex="0" id="choixOffre<%#$this->ItemIndex%>"
                                                                onchange ="updateSelection(this, '<%=$this->idOffre->getClientId()%>');"
                                                        >
                                                            <com:TRepeater id="offresSupport"  DataSource="<%#$this->TemplateControl->getListeOffres($this->Data)%>">
                                                                <prop:ItemTemplate>
                                                                    <option  value="<%#$this->Data->getId()%>"
                                                                    data-img-src="<%#$this->Data->getUrlLogo()%>"
                                                                    <%#$this->TemplateControl->TemplateControl->isOffreSelectionne($this->TemplateControl->Data, $this->Data->getId())%>
                                                                    >
                                                                        <%#$this->Data->getLibelleOffre()%>
                                                                    </option>
                                                                </prop:ItemTemplate>
                                                            </com:TRepeater>
                                                        </select>
                                                    </prop:trueTemplate>
                                                    <prop:falseTemplate>
                                                        <i><com:TTranslate>SUPPORT_BIENTOT_DISPONIBLE</com:TTranslate></i>
                                                    </prop:falseTemplate>
                                                </com:TConditional>

                                                <com:TActivePanel id="panelSuppport" style="display:none" visible="<%#$this->Data->getActif()%>">
                                                    <com:TActivePanel cssClass="line" id="panelMessageSuppport" style="display:<%#($this->Data->getAffichageMessageSupport()?'':'none')%>">
                                                        <com:TActiveTextBox ID="textMessageSupport"
                                                                            CssClass="moyen"
                                                                            TextMode="MultiLine"
                                                                            Text="<%#$this->TemplateControl->getMessageSupportValue($this->Data)%>"
                                                        />
                                                        <div class="spacer-small"></div>
                                                    </com:TActivePanel>

                                                    <com:TActivePanel cssClass="line" id="panelDepartementParution"  style="display:<%#(($this->Data->getSelectionDepartementsParution() && $this->Data->getDepartementsParution())?'':'none')%>">
                                                        <com:TConditional condition="true">
                                                            <prop:trueTemplate>
                                                                <com:TActiveListBox
                                                                        ID="departementParution"
                                                                        CssClass="chosenDepartementParution select-320 chosen-select"
                                                                        AutoPostBack="true"
                                                                        SelectionMode="Multiple"
                                                                        Attributes.multiple="multiple"
                                                                        Attributes.title="<%=Prado::localize('TEXT_LIEU_PRINCIPAL_EXECUTION')%>"
                                                                        SelectedValues="<%#$this->TemplateControl->getDepartementsParutionSelected($this->Data)%>"
                                                                />
                                                            </prop:trueTemplate>
                                                        </com:TConditional>

                                                    </com:TActivePanel>
                                                </com:TActivePanel>


                                            </div>
                                            <com:TLiteral visible="<%#($this->Data->getTagDebutFinGroupe() == '1')%>">
                                                <div id="choixOffre<%#$this->ItemIndex%>_tdVide" class="<%#($this->Data->getCode()) ? '': 'supportTypeAvisMOLAucunAvis'.$this->Data->getGroupe()%>" style="<%#(($this->Data->getCode() && !$this->TemplateControl->getSupportChecked($this->Data))) ? 'display:': 'display:none'%>">
                                                    <com:TConditional condition="$this->Data instanceOf Application\Propel\Mpe\CommonTSupportPublication && $this->Data->getActif()">
                                                        <prop:trueTemplate>
                                                            <select data-placeholder="<%=Prado::localize('SELECTIONNER_OFFRE_SUPPORT')%>"
                                                                    class="select-with-image liste-offres"
                                                                    tabindex="0" id="choixOffre<%#$this->ItemIndex%>AucunAvis"
                                                                    onchange ="updateSelection(this, '<%=$this->idOffre->getClientId()%>');"
                                                            >
                                                            </select>
                                                        </prop:trueTemplate>
                                                        <prop:falseTemplate>
                                                            <i><com:TTranslate>SUPPORT_BIENTOT_DISPONIBLE</com:TTranslate></i>
                                                        </prop:falseTemplate>
                                                    </com:TConditional>
                                                </div>
                                            </com:TLiteral>
                                        </div>
                                    </div>
                                    <div class="td actions">
                                        <com:TActiveLinkButton Attributes.style="display: <%#($this->Data->getCode()) ? 'block': 'none'%>" Attributes.title="<%=Prado::localize('PREVISUALISER_PUBLICITE_SIMPLIFIEE')%>" id="btnPrevisualiser"  OnCallback="SourceTemplateControl.downloadPdfAnnonce">
                                            <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-details.gif"  />
                                        </com:TActiveLinkButton>
                                        <com:TActiveLinkButton Attributes.style="display: <%#($this->Data->getAffichageInfos() && $this->Data->getTypeInfo() != 3) ? 'block': 'none'%>"
                                                               Attributes.title="<%=Prado::localize('PLUS_D_INFORMATION_SUPPORT_PUB')%>"
                                                               Attributes.onclick="javascript:popUp('index.php?page=Agent.popUpAffichageInfoSupport&idSupport=<%#base64_encode($this->Data->getId())%>','yes');">
                                            <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-information.gif" />
                                        </com:TActiveLinkButton>
                                        <com:THyperLink Attributes.style="display: <%#($this->Data->getAffichageInfos() && $this->Data->getTypeInfo() == 3) ? 'block': 'none'%>"
                                                        NavigateUrl="<%#$this->Data->getDetailInfo()%>"
                                                        Target="_blank"
                                                        Attributes.title="<%=Prado::localize('PLUS_D_INFORMATION_SUPPORT_PUB')%>">
                                            <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-information.gif" />
                                        </com:THyperLink>
                                    </div>
                                </div>
                            <com:TLiteral visible="<%#($this->Data->getTagDebutFinGroupe() == '3')%>">
                            </div>
                            </com:TLiteral>
                            <com:TActiveHiddenField id="idSupport" value="<%#$this->Data->getId()%>" />
                            <com:TActiveHiddenField id="CodeSupport" value="<%#$this->Data->getCode()%>" />
                            <com:TActiveHiddenField id="idOffre" value="<%#$this->Data->getSelectedOffre()%>" />
                            <com:TActiveHiddenField id="hiddenDepartementsParution" value="<%#$this->TemplateControl->getDepartementsParutionSelected($this->Data, false)%>" />
                            </prop:ItemTemplate>
                        <prop:FooterTemplate>
                        </div>
                    </div>
                </div>
                <div class="spacer-small"></div>
            </div>
            <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
            </div>
                            </prop:FooterTemplate>
                        </com:TActiveRepeater>


        <!--Fin bloc Choix du support-->
    <com:TActivePanel id="supportsSelectionnes" cssclass="panel panel-default" >
        <div class="panel-body">
            <h3><com:TTranslate>NOM_COMPTE_PUB</com:TTranslate></h3>
    <com:TActiveRepeater id="listeSupportLibelles">
        <prop:HeaderTemplate>
            <div class="recap-support">
                <table class="table-results liste-supports  margin-0">
                    <tbody>
        </prop:HeaderTemplate>
        <prop:ItemTemplate>
            <tr>
                <td class="col-80"><img src="<%#$this->Data->getImageLogoSupport()%>" alt="" class="icone" /></td>
                <td class="col-390"><%#($this->Data->getCodeSupport()) ? Prado::localize('OFFRE_PUB_CREATION_CONS') : Prado::localize('TYPE_AVIS_PUB_CREATION_CONS')%><strong><%#$this->Data->getLibelleOffre()%></strong></td>
            </tr>
        </prop:ItemTemplate>
        <prop:FooterTemplate>
            </tbody>
            </table>
            </div>
        </prop:FooterTemplate>
    </com:TActiveRepeater>
        <com:TTranslate>EXPLICATION_PUBLICITE_AVIS_BOAMP_VALIDER_CONSULTATION</com:TTranslate>
        <h3><com:TActiveCheckBox id="publier" cssClass="check" checked="true" text="<%=Prado::localize('ENVOI_PUBLICITE_SIMPLIFIER_APRES_VALIDATION_CONSULTATION')%>"/></h3>
        <com:TTranslate>EXPLICATION_PUBLICITE_SIMPLIFIER_VALIDER_CONSULTATION</com:TTranslate>
        </div>
        <div class="boutons">
            <div class="right"> <com:TActiveLinkButton id="btnPrevisualiser" cssclass="bouton float-right" OnCallback="downloadPdfAnnonce"><img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/icone-publication.png" alt="" /><com:TTranslate>PREVISUALISER_PUBLICITE_SIMPLIFIEE</com:TTranslate>
                </com:TActiveLinkButton> </div>
        </div>
    </com:TActivePanel>
    <div class="spacer-small"></div>
    <div class="boutons">
            <com:TLinkButton id="downloadPdfSub"  onclick="downloadPdf"/>
    </div>
    <div class="breaker"></div>
    </div>
    <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>

    <!--Debut Modal validation-->
    <div class="modal-formules" style="display:none;">
        <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/formules.png" />
        <!--Debut line boutons-->
        <div class="boutons-line">
            <input type="button" class="bouton-moyen float-left" value="Fermer" title="Fermer" onclick="J('.modal-formules').dialog('close');" />
        </div>
        <!--Fin line boutons-->
    </div>
    <!--Fin Modal validation-->

    <script type="text/javascript">
        showTypeAvisVide();

    </script>
</com:TActivePanel>
