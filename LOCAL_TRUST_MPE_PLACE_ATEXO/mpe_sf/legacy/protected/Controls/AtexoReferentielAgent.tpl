	<!--Debut Bloc Referentiel Agent-->
	<com:TPanel id="expertiseAgent" visible="<%#(Application\Service\Atexo\Atexo_Module::isEnabled('AnnuaireAcheteursPublics') ) ? true:false%>" cssClass="form-field">
		<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><span class="title"><com:TTranslate>TEXT_REFERENTIEL_EXPERTISE_AGENT</com:TTranslate></span></div>
		<div class="content">
			<div class="spacer-small"></div>
			<com:AtexoLtReferentiel ID="ltRefAgent" agent="1" cssClass="intitule-140 indent-10" mode="1" />
		</div>
		<div class="breaker"></div>
		<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
	</com:TPanel>
	<!--Fin Bloc Referentiel Agent-->