<com:TActivePanel id="panelRefresh" >
	<com:TRepeater ID="RepeaterAgent" 
		 DataKeyField="Id" 
		 EnableViewState="true" 
		 AllowPaging="true"
   	     PageSize="<%= Application\Service\Atexo\Atexo_Config::getParameter('NOMBRE_AFFICHAGE_PAR_DEFAUT')%>"
   	     AllowCustomPaging="true"
	 >
	<prop:HeaderTemplate>
	<table class="table-results" summary="Liste des comptes secondaires associés">
		<caption><%=Prado::localize("DEFINE_LISTE_COMPTE")%></caption>
			<thead>
			<tr>
				<th class="top" colspan="6"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></th>
			</tr>
			<tr>
				<th class="col-<%#($this->SourceTemplateControl->getTypeAssociation() != '2') ? '200' : '150' %>" id="agentName">
					<com:TTranslate>DEFINE_NOM_MAJ</com:TTranslate>
					<com:TTranslate>DEFINE_PRENOM</com:TTranslate>
					 /<br> 
					 <com:TTranslate>TEXT_ADRESSE_ELECTRONIQUE</com:TTranslate>
			   </th>											
				<th class="col-<%#($this->SourceTemplateControl->getTypeAssociation() != '2') ? '250' : '300' %>" id="entiteAchat">
					<com:TTranslate>DEFINE_TEXT_ENTITE_ACHAT</com:TTranslate>
					<br/>
					<com:TTranslate>DEFINE_ENTITE_ACHAT</com:TTranslate>
				</th>
				<th class="col-<%#($this->SourceTemplateControl->getTypeAssociation() != '2') ? '100' : '50' %>" id="idEtat">
					 	<com:TPanel Visible="<%#($this->SourceTemplateControl->getTypeAssociation() != '2') ? true : false %>">
							<com:TTranslate>TEXT_IDENTIFIANT_SANS_POINTS</com:TTranslate> /
						</com:TPanel>
						<com:TTranslate>TEXT_ETAT</com:TTranslate>
				</th>
				<th class="col-50 center" id="servciesAccessibles" style="display:<%#($this->SourceTemplateControl->getTypeAssociation() != '2') ? '' : 'none' %>" ><com:TTranslate>DEFINE_STATUS</com:TTranslate></th>
				<th class="actions" id="actions"><com:TTranslate>DEFINE_ACTIONS</com:TTranslate></th>
			</tr>
		</thead>
		</prop:HeaderTemplate>
		<prop:ItemTemplate>
		<tr class="<%#(($this->ItemIndex%2==0)? '':'on')%>" >
			<td class="col-200" headers="agentName">
                            <%# Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($this->Data->getNom())%>&nbsp;<%# Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($this->Data->getPrenom())%><br/><%#$this->Data->getEmail()%>
                        </td>
			<td class="col-250" headers="entiteAchat"><%#$this->Data->getDenomonationOrg()%> <br/> <%#$this->Data->getEntityPath()%></td>
		    <td class="col-100" headers="idEtat">
		    <com:TLabel  >
		    	<com:TPanel Visible="<%#($this->SourceTemplateControl->getTypeAssociation() != '2') ? true : false %>">
				    <com:THyperLink NavigateUrl="javascript:popUp('index.php?page=Agent.PopUpDetailAuthentification&id=<%#$this->Data->getId()%>','yes');" Enabled="<%#$this->SourceTemplateControl->getUserRoleAdminCompte()%>" >
				    	<%#(Application\Service\Atexo\Atexo_Module::isEnabled('AuthenticateAgentByCert'))? Prado::localize('TEXT_CERTIFICAT'):''%>
				    	<%#(Application\Service\Atexo\Atexo_Module::isEnabled('AuthenticateAgentByLogin'))? $this->Data->getLogin():''%>
				    </com:THyperLink> / 
				 </com:TPanel>   
			    <com:THyperLink 
			    NavigateUrl="javascript:popUp('index.php?page=Agent.PopUpDetailAuthentification&id=<%#$this->Data->getId()%>','yes');"
			    Enabled="<%#($this->SourceTemplateControl->getUserRoleAdminCompte() && ($this->SourceTemplateControl->getTypeAssociation() != '2'))? true : false %>%>" ><br>
			    	<com:TActiveImage ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-id-actif.gif" Visible="<%#($this->Data->getActif())? true:false%>" AlternateText="<%=Prado::localize('TEXT_ACTIF')%>" Attributes.Title="<%=Prado::localize('TEXT_ACTIF')%>" />
			    	<com:TActiveImage ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-id-inactif.gif" Visible="<%#(!$this->Data->getActif())? true:false%>" AlternateText="<%=Prado::localize('TEXT_VERROUILLE')%>" Attributes.Title="<%=Prado::localize('TEXT_VERROUILLE')%>" />
			    </com:THyperLink>
			</com:TLabel>
		    </td>
		    <td class="col-50 center" headers="servciesAccessibles" style="display:<%#($this->SourceTemplateControl->getTypeAssociation() != '2') ? '' : 'none' %>">
		    	<com:TImage
		    		Visible="<%#$this->Data->isAssociationCompteConfirme( Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($_GET['id']),$this->SourceTemplateControl->getTypeAssociation()) ? false : true%>"
		    		ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-attente.gif"
		    		AlternateText="<%=Prado::localize('TEXT_ASSOCIATION_ATTENTE_CONFIRMATION')%>" 
		    	/>
		    	<com:TImage
		    		Visible="<%#$this->Data->isAssociationCompteConfirme( Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($_GET['id']),$this->SourceTemplateControl->getTypeAssociation()) ? true : false%>"
		    		ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-ok.png"
		    		AlternateText="<%=Prado::localize('TEXT_ASSOCIATION_CONFIRME')%>" 
		    	/>
			</td>
		   <td class="actions" headers="actions">
	   			<com:TActivePanel id="romprePanel" Visible="<%#($this->SourceTemplateControl->getTypeAssociation() != '2') ? true : false %>">
		   			<com:TActiveImageButton 
						ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-rompre.png"
		   				cssClass = "btn-action"
						OnCallBack="SourceTemplateControl.getIdAgent"
			           	ActiveControl.CallbackParameter="<%#$this->Data->getId()%>"
						Attributes.onclick="openModal('<%=$this->SourceTemplateControl->getClientId()%>_rompre_association','popup-small2',this);" 
						Attributes.title="<%=Prado::localize('TEXT_ROMPRE_ASSOCIATION')%>" />	
				</com:TActivePanel>	
				<com:TActivePanel Visible="<%#($this->SourceTemplateControl->getTypeAssociation() == '2') ? true : false %>">
					<com:TActiveImageButton 
						Enabled="<%#$this->Data->getActif()=='1' ? true : false %>"
						Visible="<%#$this->Data->getActif()=='1' ? true : false %>"
						ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-changer-etat.gif"
		   				cssClass = "btn-action"
		   				OnCallBack="SourceTemplateControl.choisirCompte"
			           	ActiveControl.CallbackParameter="<%#$this->Data->getId()%>"
						Attributes.title="<%=Prado::localize('TEXT_UTILISER_COMPTE')%>" />	
					<com:TActiveImageButton 
						Enabled="<%#$this->Data->getActif()=='1' ? false : true %>"
						Visible="<%#$this->Data->getActif()=='1' ? false : true %>"
						ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-changer-etat-inactive.gif"
		   				cssClass = "btn-action"
						Attributes.title="<%=Prado::localize('TEXT_UTILISER_COMPTE')%>" />		
				</com:TActivePanel>		
		   </td>
		   
		</tr>
	</prop:ItemTemplate>
	<prop:FooterTemplate>
	</table>					
	</prop:FooterTemplate>								 
	</com:TRepeater>
<com:TActivePanel id="noResult" Visible="false" Cssclass="margin-0"><%=Prado::localize("TEXT_AUCUNE_ASSOCIATION_DE_CE_TYPE")%>.</com:TActivePanel>
</com:TActivePanel>

<!--Debut Modal-->
<div class="<%=$this->getClientId()%>_rompre_association" style="display:none;">
	<div class="form-bloc">
		<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
		<div class="content">
			<p><%=Prado::localize("TEXT_ETES_VOUS_SUR_VOULOIR_ROMPRE")%></p>
		</div>
		<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
	</div>
	<!--Debut line boutons-->
	<div class="boutons-line">
		<com:TActiveButton 
			Cssclass="bouton-moyen float-left" 
			Text = "<%=Prado::localize('ICONE_ANNULER')%>" 
			Attributes.title="<%=Prado::localize('ICONE_ANNULER')%>"
			Attributes.onclick="J('.<%=$this->getClientId()%>_rompre_association').dialog('close');" 
		/>
		<com:TActiveButton 
  			cssClass = "bouton-moyen float-right" 
			OnCallBack="rompreAssociation" 
			Text="<%=Prado::localize('TEXT_VALIDER')%>" 
			Attributes.title="<%=Prado::localize('TEXT_VALIDER')%>" 
		/>	
	</div>
	<!--Fin line boutons-->
</div>
<!--Fin Modal-->
<com:TActiveHiddenField id="idSelectedAgent"/>
<com:TActiveLabel id="scriptT" style="display:none"/>