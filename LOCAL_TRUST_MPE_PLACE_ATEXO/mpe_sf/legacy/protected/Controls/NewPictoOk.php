<?php

namespace Application\Controls;

use Prado\Prado;
use Prado\Web\UI\WebControls\TButton;

/**
 * Class NewPictoOk.
 */
class NewPictoOk extends TButton
{
    public function onLoad($param)
    {
        //$this->setImageUrl(t()."/images/bouton-ok.gif");
        $this->setText(Prado::localize('TEXT_CONNECTION'));
        $this->setCssClass('btn btn-default btn-block submit-btn margin');
        //$this->setAlternateText(Prado::localize("ICONE_OK"));
        //$this->setToolTip(Prado::localize("ICONE_OK"));
    }
}
