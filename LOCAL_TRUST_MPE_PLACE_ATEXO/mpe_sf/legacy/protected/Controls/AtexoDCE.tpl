
<!--Debut Bloc Pieces de la consultation-->
			<com:TPanel ID="divpiecesConsultation"  >
				<div class="spacer-small"></div>
				<!--Debut Modifier Reglement consultation-->
				<com:TPanel id="panelRg" cssclass="form-field" style="display:<%=(Application\Service\Atexo\Atexo_Module::isEnabled('ReglementConsultation'))? ';':'none;' %>">
					<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span>
					<span class="title"><com:TTranslate>MODIFIER_REGLEMENT_CONSULTATION</com:TTranslate></span></div>
					<div class="content">
                        <com:ActivePanelMessageAvertissement ID="activeInfoMsg" Message="<%=Prado::localize('MSG_INFO_PROCEDURE_REPONDANT_SERVICE_MPS')%>"></com:ActivePanelMessageAvertissement>
                        <div class="line">
							<div class="intitule-180"><label for="uploadBarNewReglement"><com:TTranslate>CHOIX_NOUVEAU_DOC</com:TTranslate></label> :</div>
							<div class="content-bloc">
								<com:AtexoQueryFileUpload 
										id="uploadBarNewReglement"
										fileSizeMax = "<%=Application\Service\Atexo\Atexo_Config::getParameter('MAX_UPLOAD_FILE_DCE_SIZE')%>"
										CssClass="file-550"
										style="display:<%=($this->getUseUplodeBar())? '':'none'%>" 
										size="90"
										afficherProgress="1"
								/>
							<com:THiddenField ID="hasRg" />
							<com:TActivePanel ID="panelDownloadRg" Visible="false">
								<div class="picto-link"> 
									<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-compresse.gif" alt="<com:TTranslate>TELECHARGEMENT_COMPLET</com:TTranslate>" title="<com:TTranslate>TELECHARGEMENT_COMPLET</com:TTranslate>" />
									<com:THyperLink ID="linkDownloadRg" Text="<%=Prado::localize('DEFINE_TELECHARGER_VERSION_ACTUELLE')%>">
										<com:TLabel ID="sizeRg"/>
									</com:THyperLink>
								</div>
							</com:TActivePanel>
							</div>
						</div>	
						<div class="breaker"></div>
					</div>
					<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
				</com:TPanel >
				<!--Fin Line Modifier Reglement consultation-->
				<!--Debut line DCE-->
				<div class="form-field">
					<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><span class="title"><com:TTranslate>MODIFIER_DCE</com:TTranslate></span></div>
					<div class="content">
						<!--Debut Line Ajouter une piece du DCE-->
						<com:TPanel ID="panelAjoutPieceDce">
    						<div class="line">
    							<div class="intitule-auto">
    							<com:TRadioButton id="ajoutPieceDce" GroupName="RadioGroupDce" Attributes.title="<%=Prado::localize('AJOUTER_PIECE_DCE')%>" cssClass="radio" />
    								<label for="ajoutPieceDce"><com:TTranslate>AJOUTER_PIECE_DCE</com:TTranslate></label> 
    							</div>
    							<div class="breaker"></div>
    							<div class="intitule-150 bloc-720 indent-30"><label for="uploadBarNewPieceDce"><com:TTranslate>CHOIX_NOUVEAU_DOC</com:TTranslate></label> :</div>
    							<div class="content-bloc">
										<com:AtexoQueryFileUpload 
												id="uploadBarNewPieceDce"
												fileSizeMax = "<%=Application\Service\Atexo\Atexo_Config::getParameter('MAX_UPLOAD_FILE_DCE_SIZE')%>"
												CssClass="file-550"
												style="display:<%=($this->getUseUplodeBar())? '':'none'%>" 
												size="90"
												afficherProgress="1"
										/>
    							</div>
    						</div>
						</com:TPanel>
						<!--Fin Line Ajouter une piece du DCE-->
						<div class="spacer-mini"></div>
						<!--Debut Line Remplacer une piece du DCE-->
						<com:TPanel ID="panelRemplacerPieceDce">
    						<div class="line">
    							<div class="intitule-auto" >
    								<com:TRadioButton id="remplacerPieceDce" GroupName="RadioGroupDce" Attributes.title="<%=Prado::localize('REMPLACER_PIECE_DCE')%>" cssClass="radio" />
    								<label for="remplacerPieceDce"><com:TTranslate>REMPLACER_PIECE_DCE</com:TTranslate></label> 
    							</div>
    							<div class="breaker"></div>
    							<div class="intitule-auto indent-25" id="divLinkChangeFile" style="display:''">
    								<com:THyperLink id="popupUpdateDCE" Attributes.href="javascript:popUp('?page=Agent.PopupUpdateDce&id=<%= Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($_GET['id'])%>&prefix=<%=base64_encode($this->prefix)%>','yes');" CssClass="arrow-link">
    									<com:TTranslate>CHOIX_DOC_A_REMPLACER</com:TTranslate>
    								</com:THyperLink>
    								<com:THyperLink id="desactivePopupUpdateDCE" CssClass="arrow-link">
    									<com:TTranslate>CHOIX_DOC_A_REMPLACER</com:TTranslate>
    								</com:THyperLink>
    							</div>
    							<div class="intitule-auto indent-30" id="divBlocPiecesMoved" style="display:none">
        							<com:TLabel cssClass="blue" ID="oldDoc"/>
        							<span class="indent-10"> <com:TTranslate>REPLACE_WITH</com:TTranslate></span> 
        							<com:TLabel cssClass="blue indent-10"  ID="newDoc"/>
        							<com:THiddenField ID="filePath"/>
        							<com:THiddenField ID="indexOfFile"/>
        							<com:THiddenField ID="newFileName"/>
        							<com:THiddenField ID="newFileSize"/>
        							<com:THiddenField ID="oldFileSize"/>
        							<com:THiddenField ID="hiddenOldFileName"/>
    							</div>
    						</div>
						</com:TPanel>
						<!--Fin Line Remplacer une piece du DCE-->
						<div class="spacer-mini"></div>
						<!--Debut Line Remplacer tout le DCE-->
						<div class="line">
							<div class="intitule-auto">
								<com:TRadioButton id="remplacerToutDce" Checked="true" GroupName="RadioGroupDce" Attributes.title="<%=$this->getTextDce()%>" cssClass="radio" />
								<label for="remplacerToutDce"><%=$this->getTextDce()%></label> 
							</div>
							<div class="breaker"></div>
							<div class="intitule-150 bloc-720 indent-30"><label for="uploadBar"><com:TTranslate>CHOIX_DU_DCE</com:TTranslate></label> :</div>
								<div class="content-bloc">
									<com:THiddenField ID="hasDce" />
										<com:AtexoQueryFileUpload 
												id="uploadBar"
												fileSizeMax = "<%=Application\Service\Atexo\Atexo_Config::getParameter('MAX_UPLOAD_FILE_DCE_SIZE')%>"
												CssClass="file-550"  
												style="display:<%=($this->getUseUplodeBar())? '':'none'%>" 
												size="90"
												afficherProgress="1"
										/>
									<span class="info-aide-small"><com:TTranslate>TEXT_COMPRESSE_OBLIGATOIRE </com:TTranslate></span>
									<com:TActiveLabel ID="labelServerSide"/>
								</div>
							<div class="spacer-small"></div>
							<com:THiddenField ID="partialDownload" />
							<com:TPanel id="downloadDce">
								<com:TActivePanel id="paneltelechargementPartiel" >
	                                <div class="intitule-auto">
	                                    <com:TActiveCheckBox id="telechargementPartiel" CssClass="check"   />
	                                    <label for="telechargementPartiel"><com:TTranslate>DEFINE_TELECHARGEMENT_PARTIEL_DCE</com:TTranslate></label>
	                                </div>
	                            </com:TActivePanel>
								<com:TActivePanel ID="PanelTelechargementDce" Visible="false">
									<div class="picto-link"> 
										<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-compresse.gif" alt="<com:TTranslate>TELECHARGEMENT_COMPLET</com:TTranslate>" title="<%=Prado::localize('DEFINE_TELECHARGER_VERSION_ACTUELLE')%>" />
										<com:THyperLink ID="linkDownloadDce" Text="<%=Prado::localize('DEFINE_TELECHARGER_VERSION_ACTUELLE')%>">
										<com:TLabel ID="sizeDce"/>
										</com:THyperLink>
									</div>
								</com:TActivePanel>
							</com:TPanel>
						</div>
						<!--Fin Line Remplacer tout le DCE-->
						<div class="breaker"></div>
					</div>
					<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
				</div>
				<!--Fin Line DCE-->
				<div class="breaker"></div>
				<com:THiddenField ID="fileToScan"/>
		</com:TPanel>
		<!--Fin Bloc Pieces de la consultation-->