<!--Debut colonne droite-->
<div style="height: auto;" class="main-part" id="main-part">
	<div class="breadcrumbs"><a href="/?page=Agent.ResultatRechercheContrat"><com:TTranslate>CONTRATS</com:TTranslate></a>&gt; <com:TTranslate>TEXT_RECHERCHE_AVANCEE</com:TTranslate></div>

	<!--Debut Bloc Recherche avancee-->
	<div class="form-field">
		<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><span class="title"><com:TTranslate>DEFINE_RECHERCHE_MULTICRITAIRE</com:TTranslate></span></div>
		<div class="content">

			<!--Debut Type de contrat-->
			<div class="line">
				<div class="intitule-150"><label for="contratType"><com:TTranslate>DEFINE_TYPE_CONTRAT</com:TTranslate></label> :</div>
				<div class="content-bloc">
					<com:TActiveDropDownList
							id="contratType"
							CssClass="moyen float-left"
							Attributes.title="<%=Prado::localize('DEFINE_TYPE_CONTRAT')%>"
							/>

					<div class="col-info-bulle">
						<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" onmouseover="afficheBulle('infosContrat', this)" onmouseout="cacheBulle('infosContrat')" class="picto-info" alt="Info-bulle" title="Info-bulle">
						<div onmouseout="mouseOutBulle();" onmouseover="mouseOverBulle();" class="info-bulle" id="infosContrat"><div><com:TTranslate>INFO_BULLE_FILTRE_TYPE_CONTRAT</com:TTranslate></div></div>
					</div>
				</div>
			</div>
			<!--Fin Type de contrat-->
			<!--Debut Reference-->
			<div class="line">
				<div class="intitule-150"><label for="reference"><com:TTranslate>REFERENCE_INTERNE_CONTRAT</com:TTranslate></label> :</div>
				<div class="content-bloc">
					<com:TActiveTextBox ID="reference" Attributes.title="<%=Prado::localize('REFERENCE_INTERNE_CONTRAT')%>" CssClass="moyen float-left"/>
				</div>
			</div>
			<div class="line">
				<div class="intitule-150"><label for="categorie"><com:TTranslate>DEFINE_CATEGORIE_PRINCIPAL</com:TTranslate></label> :</div>
				<com:TActiveDropDownList id="categorie" CssClass="moyen" Attributes.title="<%=Prado::localize('DEFINE_CATEGORIE_PRINCIPALE')%>" />
			</div>
			<!--Fin Reference-->
			<div class="spacer-small"></div>
			<com:TPanel ID="panelClauseConsultation">
				<!--Debut Ligne Clauses sociales -->
				<div class="line">
					<div class="intitule-150"><label for="clauseSociales"><com:TTRanslate>CLAUSES_SOCIALES</com:TTRanslate></label> :</div>
					<com:TActiveDropDownList ID="clauseSociales" CssClass="small-150" Attributes.title="<%=Prado::localize('CLAUSES_SOCIALES')%>" />
					<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" onmouseover="afficheBulle('infosclauseSociales', this)" onmouseout="cacheBulle('infosclauseSociales')" class="picto-info-intitule" alt="Info-bulle" title="Info-bulle">
					<div id="infosclauseSociales" class="info-bulle" onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();">
						<div><com:TTranslate>DEFINE_INFO_BULLE_CLAUSES_SOCIALES_CONTRAT</com:TTranslate></div>
					</div>
				</div>
				<!--Fin Ligne Clauses sociales -->
				<!--Debut Ligne Clauses environnementales -->
				<div class="line">
					<div class="intitule-150 line-height-normal"><label for="clausesEnvironnementales"><com:TTRanslate>CLAUSES_ENVIRONNEMENTALES</com:TTRanslate> </label> :</div>
					<com:TActiveDropDownList ID="clausesEnvironnementales" CssClass="small-150" Attributes.title="<%=Prado::localize('CLAUSES_ENVIRONNEMENTALES')%>" />
					<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" onmouseover="afficheBulle('infosclauseEnvironnementales', this)" onmouseout="cacheBulle('infosclauseEnvironnementales')" class="picto-info-intitule" alt="Info-bulle" title="Info-bulle">
					<div id="infosclauseEnvironnementales" class="info-bulle" onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();">
						<div><com:TTranslate>DEFINE_INFO_BULLE_CLAUSES_ENVIRONNEMENTALES_CONTRAT</com:TTranslate></div>
					</div>
				</div>
				<!--Fin Ligne Clauses environnementales -->
			</com:TPanel>
			<div class="spacer-small"></div>
			<!--Debut Ligne Recherche par date-->
			<h4><com:TTranslate>DEFINE_RECHERCHE_PAR_DATE</com:TTranslate> </h4>
			<!--Debut Date définitive de notification-->
			<div class="line">
				<div class="intitule-150"><com:TTranslate>CONTRAT_DATE_DEFINITIVE_NOTIFICATION</com:TTranslate> :</div>
				<div class="intitule-auto"><label for="dateNotificationStart"><com:TTranslate>TEXT_RECHERCHE_AVANCEE_AVANT_LE</com:TTranslate> </label></div>
				<div class="calendar">
					<com:TActiveTextBox
							ID="dateNotificationStart"
							Attributes.title="<%=Prado::Localize('TEXT_RECHERCHE_AVANCEE_AVANT_LE')%>"
							CssClass="date"
							Attributes.Onchange="supprimerEspace(this);"/>
					<com:TImage ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-calendar.gif" Attributes.alt="<%=Prado::localize('DEFINE_CALENDRIER')%>" Attributes.title="<%=Prado::localize('DEFINE_CALENDRIER')%>" Attributes.onclick="popUpCalendar(this,document.getElementById('ctl0_CONTENU_PAGE_advancedSearchContrat_dateNotificationStart'),'dd/mm/yyyy','');" />
					<com:TDataTypeValidator
							ID="controlDateNotificationStart"
							ValidationGroup="datesValidation"
							ControlToValidate="dateNotificationStart"
							DataType="Date"
							DateFormat="dd/M/yyyy"
							Text="<%=Prado::localize('MSG_ERROR_FORMAT_DATE_INVALIDE')%>"
							Display="Dynamic"/>
				</div>
				<div class="intitule-auto indent-10"><label for="dateNotificationEnd"><com:TTranslate>TEXT_RECHERCHE_AVANCEE_APRES_LE</com:TTranslate> </label></div>
				<div class="calendar">
					<com:TActiveTextBox ID="dateNotificationEnd" Attributes.title="<%=Prado::Localize('TEXT_RECHERCHE_AVANCEE_APRES_LE')%>" CssClass="date" Attributes.Onchange="supprimerEspace(this);"/>
					<com:TImage ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-calendar.gif" Attributes.alt="<%=Prado::localize('DEFINE_CALENDRIER')%>" Attributes.title="<%=Prado::localize('DEFINE_CALENDRIER')%>" Attributes.onclick="popUpCalendar(this,document.getElementById('ctl0_CONTENU_PAGE_advancedSearchContrat_dateNotificationEnd'),'dd/mm/yyyy','');" />
					<com:TDataTypeValidator
							ID="controlDateNotificationEnd"
							ValidationGroup="datesValidation"
							ControlToValidate="dateNotificationEnd"
							DataType="Date"
							DateFormat="dd/M/yyyy"
							Text="<%=Prado::localize('MSG_ERROR_FORMAT_DATE_INVALIDE')%>"
							Display="Dynamic"/>
				</div>
				<div class="info-aide-right"><com:TTranslate>DEFINE_FORMAT_DATE_SIMPLE</com:TTranslate></div>
			</div>
			<!--Fin Date définitive de notification-->
			<!--Debut Date previsionelle de fin de marché-->
			<div class="line">
				<div class="intitule-150 line-height-normal"><com:TTranslate>DATE_PREVISIONNELLE_NOTIFICATION</com:TTranslate> :</div>
				<div class="intitule-auto"><label for="datePrevNotificationStart"><com:TTranslate>TEXT_RECHERCHE_AVANCEE_AVANT_LE</com:TTranslate> </label></div>
				<div class="calendar">
					<com:TActiveTextBox ID="datePrevNotificationStart" Attributes.title="<%=Prado::Localize('TEXT_RECHERCHE_AVANCEE_AVANT_LE')%>" CssClass="date" Attributes.Onchange="supprimerEspace(this);"/>
					<com:TImage ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-calendar.gif" Attributes.alt="<%=Prado::localize('DEFINE_CALENDRIER')%>" Attributes.title="<%=Prado::localize('DEFINE_CALENDRIER')%>" Attributes.onclick="popUpCalendar(this,document.getElementById('ctl0_CONTENU_PAGE_advancedSearchContrat_datePrevNotificationStart'),'dd/mm/yyyy','');" />
					<com:TDataTypeValidator
							ID="controlDatePrevNotificationStart"
							ValidationGroup="datesValidation"
							ControlToValidate="datePrevNotificationStart"
							DataType="Date"
							DateFormat="dd/M/yyyy"
							Text="<%=Prado::localize('MSG_ERROR_FORMAT_DATE_INVALIDE')%>"
							Display="Dynamic"/>
				</div>
				<div class="intitule-auto indent-10"><label for="datePrevNotificationStartEnd"><com:TTranslate>TEXT_RECHERCHE_AVANCEE_APRES_LE</com:TTranslate> </label></div>
				<div class="calendar">
					<com:TActiveTextBox ID="datePrevNotificationStartEnd" Attributes.title="<%=Prado::Localize('TEXT_RECHERCHE_AVANCEE_APRES_LE')%>" CssClass="date" Attributes.Onchange="supprimerEspace(this);"/>
					<com:TImage ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-calendar.gif" Attributes.alt="<%=Prado::localize('DEFINE_CALENDRIER')%>" Attributes.title="<%=Prado::localize('DEFINE_CALENDRIER')%>" Attributes.onclick="popUpCalendar(this,document.getElementById('ctl0_CONTENU_PAGE_advancedSearchContrat_datePrevNotificationStartEnd'),'dd/mm/yyyy','');" />
					<com:TDataTypeValidator
							ID="controlDatePrevNotificationStartEnd"
							ValidationGroup="datesValidation"
							ControlToValidate="datePrevNotificationStartEnd"
							DataType="Date"
							DateFormat="dd/M/yyyy"
							Text="<%=Prado::localize('MSG_ERROR_FORMAT_DATE_INVALIDE')%>"
							Display="Dynamic"/>
				</div>
				<div class="info-aide-right"><com:TTranslate>DEFINE_FORMAT_DATE_SIMPLE</com:TTranslate></div>
			</div>
			<!--Fin Date previsionelle de notification-->
			<!--Debut Date définitive de fin de marché-->
			<div class="line">
				<div class="intitule-150 line-height-normal"><com:TTranslate>CONTRAT_DATE_DEFINITIVE_FIN_MARCHE</com:TTranslate> :</div>
				<div class="intitule-auto"><label for="dateFinMarcheStart"><com:TTranslate>TEXT_RECHERCHE_AVANCEE_AVANT_LE</com:TTranslate> </label></div>
				<div class="calendar">
					<com:TActiveTextBox ID="dateFinMarcheStart" Attributes.title="<%=Prado::Localize('TEXT_RECHERCHE_AVANCEE_AVANT_LE')%>" CssClass="date" Attributes.Onchange="supprimerEspace(this);"/>
					<com:TImage ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-calendar.gif" Attributes.alt="<%=Prado::localize('DEFINE_CALENDRIER')%>" Attributes.title="<%=Prado::localize('DEFINE_CALENDRIER')%>" Attributes.onclick="popUpCalendar(this,document.getElementById('ctl0_CONTENU_PAGE_advancedSearchContrat_dateFinMarcheStart'),'dd/mm/yyyy','');" />
					<com:TDataTypeValidator
							ID="controlDateFinMarcheStart"
							ValidationGroup="datesValidation"
							ControlToValidate="dateFinMarcheStart"
							DataType="Date"
							DateFormat="dd/M/yyyy"
							Text="<%=Prado::localize('MSG_ERROR_FORMAT_DATE_INVALIDE')%>"
							Display="Dynamic"/>
				</div>
				<div class="intitule-auto indent-10"><label for="dateFinMarcheEnd"><com:TTranslate>TEXT_RECHERCHE_AVANCEE_APRES_LE</com:TTranslate> </label></div>
				<div class="calendar">
					<com:TActiveTextBox ID="dateFinMarcheEnd" Attributes.title="<%=Prado::Localize('TEXT_RECHERCHE_AVANCEE_APRES_LE')%>" CssClass="date" Attributes.Onchange="supprimerEspace(this);"/>
					<com:TImage ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-calendar.gif" Attributes.alt="<%=Prado::localize('DEFINE_CALENDRIER')%>" Attributes.title="<%=Prado::localize('DEFINE_CALENDRIER')%>" Attributes.onclick="popUpCalendar(this,document.getElementById('ctl0_CONTENU_PAGE_advancedSearchContrat_dateFinMarcheEnd'),'dd/mm/yyyy','');" />
					<com:TDataTypeValidator
							ID="controlDateFinMarcheEnd"
							ValidationGroup="datesValidation"
							ControlToValidate="dateFinMarcheEnd"
							DataType="Date"
							DateFormat="dd/M/yyyy"
							Text="<%=Prado::localize('MSG_ERROR_FORMAT_DATE_INVALIDE')%>"
							Display="Dynamic"/>
				</div>
				<div class="info-aide-right"><com:TTranslate>DEFINE_FORMAT_DATE_SIMPLE</com:TTranslate></div>
			</div>
			<!--Fin Date définitive de fin de marché-->
			<!--Debut Date définitive de finmaximale de marché-->
			<div class="line">
				<div class="intitule-150 line-height-normal"><com:TTranslate>CONTRAT_DATE_DEFINITIVE_FIN_MAX_MARCHE</com:TTranslate> :</div>
				<div class="intitule-auto"><label for="dateFinMaxMarcheStart"><com:TTranslate>TEXT_RECHERCHE_AVANCEE_AVANT_LE</com:TTranslate> </label></div>
				<div class="calendar">
					<com:TActiveTextBox ID="dateFinMaxMarcheStart" Attributes.title="<%=Prado::Localize('TEXT_RECHERCHE_AVANCEE_AVANT_LE')%>" CssClass="date" Attributes.Onchange="supprimerEspace(this);"/>
					<com:TImage ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-calendar.gif" Attributes.alt="<%=Prado::localize('DEFINE_CALENDRIER')%>" Attributes.title="<%=Prado::localize('DEFINE_CALENDRIER')%>" Attributes.onclick="popUpCalendar(this,document.getElementById('ctl0_CONTENU_PAGE_advancedSearchContrat_dateFinMaxMarcheStart'),'dd/mm/yyyy','');" />
					<com:TDataTypeValidator
							ID="controlDateFinMaxMarcheStart"
							ValidationGroup="datesValidation"
							ControlToValidate="dateFinMaxMarcheStart"
							DataType="Date"
							DateFormat="dd/M/yyyy"
							Text="<%=Prado::localize('MSG_ERROR_FORMAT_DATE_INVALIDE')%>"
							Display="Dynamic"/>
				</div>
				<div class="intitule-auto indent-10"><label for="dateFinMaxMarcheEnd"><com:TTranslate>TEXT_RECHERCHE_AVANCEE_APRES_LE</com:TTranslate> </label></div>
				<div class="calendar">
					<com:TActiveTextBox ID="dateFinMaxMarcheEnd" Attributes.title="<%=Prado::Localize('TEXT_RECHERCHE_AVANCEE_APRES_LE')%>" CssClass="date" Attributes.Onchange="supprimerEspace(this);"/>
					<com:TImage ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-calendar.gif" Attributes.alt="<%=Prado::localize('DEFINE_CALENDRIER')%>" Attributes.title="<%=Prado::localize('DEFINE_CALENDRIER')%>" Attributes.onclick="popUpCalendar(this,document.getElementById('ctl0_CONTENU_PAGE_advancedSearchContrat_dateFinMaxMarcheEnd'),'dd/mm/yyyy','');" />
					<com:TDataTypeValidator
							ID="controlDateFinMaxMarcheEnd"
							ValidationGroup="datesValidation"
							ControlToValidate="dateFinMaxMarcheEnd"
							DataType="Date"
							DateFormat="dd/M/yyyy"
							Text="<%=Prado::localize('MSG_ERROR_FORMAT_DATE_INVALIDE')%>"
							Display="Dynamic"/>
				</div>
				<div class="info-aide-right"><com:TTranslate>DEFINE_FORMAT_DATE_SIMPLE</com:TTranslate></div>
			</div>
			<!--Fin Date définitive de fin maximale de marché-->
			<div class="spacer"></div>
			<!--Debut Ligne Recherche par mots cles-->
			<h4><com:TTranslate>DEFINE_RECHERCHE_MOT_CLE</com:TTranslate>
				<img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" onmouseover="afficheBulle('infosRechercheMotsCles', this)" onmouseout="cacheBulle('infosRechercheMotsCles')" class="picto-info-intitule" alt="Info-bulle" title="Info-bulle">
			</h4>
			<div id="infosRechercheMotsCles" class="info-bulle" onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();">
				<div><com:TTranslate>INFO_BULLE_RECHERCHE_PAR_MOT_CLE_CONTRAT</com:TTranslate></div>
			</div>
			<div class="line">
				<div class="intitule-bloc bloc-155"><label for="keywordSearch"><com:TTranslate>DEFINE_RECHERCHE_REF_TITRE_NUMERO_OBJET_CONTRAT</com:TTranslate> :</label></div>
				<div class="content-bloc bloc-600">
					<com:TActiveTextBox ID="keywordSearch" Attributes.title="<%=Prado::localize('DEFINE_RECHERCHE_REF_TITRE_NUMERO_OBJET_CONTRAT')%>" CssClass="long"/>
					<div id="AdvancedSearch_panelMotsCles" class="content-bloc bloc-600">
						<div id="AdvancedSearch_panelRechercheFloue" class="line">
							<span class="radio" title="Recherche approchée">
								<com:TActiveRadioButton Attributes.title="<%=Prado::localize('TEXT_RECHERCHE_FLOUE')%>" GroupName="rechercheFloue" ID="floue" Checked="true" CssClass="radio"/>
							</span>
							<label for="AdvancedSearch_floue"><com:TTranslate>TEXT_RECHERCHE_FLOUE</com:TTranslate></label>
							&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
							<span class="radio" title="Recherche exacte">
								<com:TActiveRadioButton Attributes.title="<%=Prado::localize('TEXT_RECHERCHE_EXACTE')%>" GroupName="rechercheFloue" ID="exact" Checked="false" CssClass="radio" />
							</span>
							<label for="AdvancedSearch_exact"><com:TTranslate>TEXT_RECHERCHE_EXACTE</com:TTranslate></label>
						</div>
					</div>
				</div>
			</div>
			<!--Fin Ligne Recherche par mots cles-->
			<!--Debut Line bouton recherche multi-criteres-->
			<div class="spacer"></div>
			<div class="boutons-line">
				<com:TActiveButton id="boutonClear"
								   CssClass="bouton-long-190 float-left"
								   Attributes.title="<%=Prado::localize('DEFINE_EFFACER_CRITERE_RECHERCHE')%>"
								   Attributes.value="<%=Prado::localize('DEFINE_EFFACER_CRITERE_RECHERCHE')%>"
								   OnClick="Page.advancedSearchContrat.cleareCritaire"

						/>
				<com:TButton
						id="lancerRecherche"
						CssClass="bouton-moyen-120 float-right"
						Text="<%=Prado::localize('DEFINE_LANCER_RECHERCHE')%>"
						Attributes.title="<%=Prado::localize('DEFINE_LANCER_RECHERCHE')%>"
						ValidationGroup="datesValidation"
						OnClick="onSearchClick"/>
			</div>
			<!--Fin Line bouton recherche multi-criteres-->
			<div class="breaker"></div>
		</div>
		<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
	</div>
	<!--Fin Bloc Recherche avancee-->

	<div class="breaker"></div>
</div>
<!--Fin colonne droite-->