<div class="panel panel-default">
    <div class="panel-heading">
        <h2 class="h4 panel-title">
            <com:TTranslate>DEFINE_CAPACITES</com:TTranslate>
        </h2>
    </div>
    <div class="panel-body">
        <!--MESSAGE INFO-->
        <com:TActivePanel cssClass="alert alert-info" visible="<%=!$this->getModeAffichage()%>">
            <com:TTranslate>MESSAGE_CHAMPS_PREREMPLIS_POUR_ADMINISTRATEUR</com:TTranslate>
        </com:TActivePanel>
        <!--END MESSAGE INFO-->

        <!--BEGIN TABLE CAPACITE-->
        <com:TActivePanel cssClass="table table-bloc" visible="<%=!$this->getModeAffichage()%>">
            <label>
                <com:TTranslate>SELECTIONNER_LES_EXERCICES_A_PRESENTER</com:TTranslate>
            </label>
            <div class="table table-striped table-results list-group m-t-2">
                <com:TActiveRepeater ID="repeaterCapacites">
                    <prop:HeaderTemplate>
                        <div class="list-group-item header kt-vertical-align small ">
                            <div class="kt-vertical-align">
                                <!--BEGIN COLONNE CHECKBOX-->
                                <div class="col-md-1" abbr="<%=Prado::Localize('TEXT_TOUS_AUCUN')%>" id="selection">
                                    <com:TCheckBox id="selectAll2" Attributes.title="<%=Prado::localize('DEFINE_TOUS')%>" cssClass="check"
                                                   Attributes.onclick="isCheckedCheckAllInRepater(this,'<%=$this->templateControl->getId()%>_repeaterCapacites');"/>
                                    <label style="display:none" for="selectAll">
                                        <com:TTranslate>DEFINE_SELECTION</com:TTranslate>
                                    </label>
                                </div>
                                <!--END COLONNE CHECKBOX-->

                                <!--BEGIN EXERCICE CLOS EN-->
                                <div class="col-md-1 p-l-0" id="capacites_col_01">
                                    <com:TTranslate>DEFINE_EXERCICE_CLOS_EN</com:TTranslate>
                                </div>
                                <!--END EXERCICE CLOS EN-->

                                <!--BEGIN DATE EXERCICE-->
                                <div class="col-md-2 text-center" id="capacites_col_02">
                                    <com:TTranslate>DATE_D_EXERCICE</com:TTranslate>
                                </div>
                                <!--END DATE EXERCICE-->

                                <!--BEGIN CHIFFRE D'AFFAIRE-->
                                <div class="col-md-2 text-center" id="capacites_col_03">
                                    <com:TTranslate>CHIFFRE_AFFAIRES_REALISE_OU_ESTIME</com:TTranslate>
                                </div>
                                <!--END CHIFFRE D'AFFAIRE-->

                                <!--BEGIN SIGNIFICATION POURCENTAGE-->
                                <div class="col-md-2 text-center" id="capacites_col_04">
                                    <%=$this->TemplateControl->getConsultationAlloti()?Prado::Localize('SIGNIFICATIF_POUR_CONSULTATION_ALLOTI'):Prado::Localize('SIGNIFICATIF_POUR_CONSULTATION')%>
                                </div>
                                <!--END SIGNIFICATION POURCENTAGE-->

                                <!--BEGIN COMMENTAIRE-->
                                <div class="col-md-3 text-center" id="capacites_col_05">
                                    <%=$this->TemplateControl->getConsultationAlloti()?Prado::Localize('DEFINE_TEXT_COMMENTAIRES_CAS_ALLOTI'):Prado::Localize('DEFINE_TEXT_COMMENTAIRES')%>
                                </div>
                                <!--END COMMENTAIRE-->
                            </div>
                        </div>
                    </prop:HeaderTemplate>
                    <prop:ItemTemplate>
                        <div class="item_consultation list-group-item kt-callout left kt-vertical-align success">
                            <!--BEGIN COLONNE CHECKBOX-->
                            <div class="col-md-1 top" headers="selection">
                                <com:TActiveCheckBox id="check_list" Attributes.title="<%=Prado::localize('TEXT_DEFINE_SELECTIONNER')%>" cssClass="check" checked="<%#$this->Data->getActive()%>"/>
                                <com:TActiveImage id="ImgError" ImageUrl='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' Style="display:none"/>
                            </div>
                            <!--END COLONNE CHECKBOX-->

                            <!--BEGIN EXERCICE CLOS EN-->
                            <div class="col-md-1 top" headers="capacites_col_01">
                                <com:TLabel ID="anneeCloture" Text="<%#$this->Data->getAnneeCloture()%>"/>
                            </div>
                            <!--END EXERCICE CLOS EN-->

                            <!--BEGIN DATE EXERCICE-->
                            <div class="col-md-2 top p-l-0" headers="capacites_col_02">
                                <div class="clearfix">
                                    <div class="form-group form-group-sm formulaire-inline">
                                        <div class=" control-label col-md-2 p-t-1 m-r-1">
                                            <com:TTranslate>DEFINE_DU</com:TTranslate>
                                        </div>
                                        <div class="input-group datetimepicker calendar col-md-9">
                                            <com:TActiveTextBox
                                                    ID="exercice_date_debut"
                                                    Attributes.title="<%=Prado::localize('DATE_D_EXERCICE_DEBUT')%>"
                                                    CssClass="form-control text-center"
                                                    Attributes.placeholder="<%=Prado::localize('DEFINE_FORMAT_DATE_SIMPLE')%>"
                                                    Text="<%#$this->Data->getDateDebut()%>">
                                            </com:TActiveTextBox>
                                            <com:TActiveImage id="ImgErrorDateDebut" ImageUrl='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' Style="display:none"/>
                                            <span class="input-group-addon hide-calendar">
                                            <i class="fa fa-calendar"></i>
                                        </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix">
                                    <div class="form-group form-group-sm formulaire-inline">
                                        <div class=" control-label col-md-2 p-t-1 m-r-1">
                                            <com:TTranslate>TEXT_AU</com:TTranslate>
                                        </div>
                                        <div class="input-group datetimepicker calendar col-md-9">
                                            <com:TActiveTextBox
                                                    ID="exercice_date_fin"
                                                    Attributes.title="<%=Prado::localize('DATE_D_EXERCICE_FIN')%>"
                                                    CssClass="form-control text-center"
                                                    Attributes.placeholder="<%=Prado::localize('DEFINE_FORMAT_DATE_SIMPLE')%>"
                                                    Text="<%#$this->Data->getDateFin()%>">
                                            </com:TActiveTextBox>
                                            <com:TActiveImage id="ImgErrorDateFin" ImageUrl='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' Style="display:none"/>
                                            <span class="input-group-addon hide-calendar">
                                                <i class="fa fa-calendar"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--END DATE EXERCICE-->

                            <!--BEGIN CHIFFRE D'AFFAIRE-->
                            <div class="col-md-2 top p-l-0" headers="capacites_col_03">
                                <div class="input-group input-group-sm">
                                    <com:TActiveTextBox ID="exercice_chiffre_affaire" Text="<%#$this->Data->getChiffreAffaire()%>" CssClass="form-control montant"/>
                                    <span class="input-group-addon"><com:TTranslate>DEFINE_UNITE_HT</com:TTranslate></span>
                                </div>
                            </div>
                            <!--END CHIFFRE D'AFFAIRE-->

                            <!--BEGIN SIGNIFICATION POURCENTAGE-->
                            <div class="col-md-2 top" headers="capacites_col_04">
                                <div class="input-group input-group-sm">
                                    <com:TActiveTextBox
                                            ID="exercice_significatif" Text="<%#$this->Data->getSignificatif()%>"
                                            Attributes.title="<%=$this->TemplateControl->getConsultationAlloti()?Prado::Localize('SIGNIFICATIF_POUR_CONSULTATION_ALLOTI'):Prado::Localize('SIGNIFICATIF_POUR_CONSULTATION')%>"
                                            CssClass="form-control"/>
                                    <span class="input-group-addon"><com:TTranslate>POUR_CENT</com:TTranslate></span>
                                </div>

                                <div class="intitule-bloc">

                                </div>
                            </div>
                            <!--END SIGNIFICATION POURCENTAGE-->

                            <!--BEGIN COMMENTAIRE-->
                            <div class="col-md-3 top" headers="capacites_col_05">
                                <com:TActiveTextBox
                                        id="exercice_commentaire"
                                        Text="<%#$this->Data->getCommentaire()%>"
                                        TextMode="MultiLine"
                                        Attributes.title="<%=$this->TemplateControl->getConsultationAlloti()?Prado::Localize('DEFINE_TEXT_COMMENTAIRES_CAS_ALLOTI'):Prado::Localize('DEFINE_TEXT_COMMENTAIRES')%>"
                                        CssClass="form-control"/>
                            </div>
                            <!--END COMMENTAIRE-->
                        </div>
                    </prop:ItemTemplate>
                </com:TActiveRepeater>
            </div>
        </com:TActivePanel>
        <!--Fin Tableau Capacités-->


        <!--Debut Bloc Capacités Recapitulation-->
        <com:TActivePanel cssClass="table-bloc" visible="<%=$this->getModeAffichage()%>">
            <com:TActiveRepeater ID="repeaterCapacitesRecapitulation" EnableViewState="false">
                <div class="table table-striped table-results list-group m-t-2">
                    <prop:HeaderTemplate>
                        <div class="list-group-item header kt-vertical-align small ">
                            <div class="kt-vertical-align">
                                <!--BEGIN COLONNE CHECKBOX-->
                                <div class="col-md-1" id="selection"></div>
                                <!--END COLONNE CHECKBOX-->

                                <!--BEGIN EXERCICE CLOS EN-->
                                <div class="col-md-1 p-l-0" id="capacites_col_01">
                                    <com:TTranslate>DEFINE_EXERCICE_CLOS_EN</com:TTranslate>
                                </div>
                                <!--END EXERCICE CLOS EN-->

                                <!--BEGIN DATE EXERCICE-->
                                <div class="col-md-2 text-center" id="capacites_col_02">
                                    <com:TTranslate>DATE_D_EXERCICE</com:TTranslate>
                                </div>
                                <!--END DATE EXERCICE-->

                                <!--BEGIN CHIFFRE D'AFFAIRE-->
                                <div class="col-md-2 text-center" id="capacites_col_03">
                                    <com:TTranslate>CHIFFRE_AFFAIRES_REALISE_OU_ESTIME</com:TTranslate>
                                </div>
                                <!--END CHIFFRE D'AFFAIRE-->

                                <!--BEGIN SIGNIFICATION POURCENTAGE-->
                                <div class="col-md-2 text-center" id="capacites_col_04">
                                    <%=$this->TemplateControl->getConsultationAlloti()?Prado::Localize('SIGNIFICATIF_POUR_CONSULTATION_ALLOTI'):Prado::Localize('SIGNIFICATIF_POUR_CONSULTATION')%>
                                </div>
                                <!--END SIGNIFICATION POURCENTAGE-->

                                <!--BEGIN COMMENTAIRE-->
                                <div class="col-md-3 text-center" id="capacites_col_05">
                                    <%=$this->TemplateControl->getConsultationAlloti()?Prado::Localize('DEFINE_TEXT_COMMENTAIRES_CAS_ALLOTI'):Prado::Localize('DEFINE_TEXT_COMMENTAIRES')%>
                                </div>
                                <!--END COMMENTAIRE-->
                            </div>
                        </div>
                    </prop:HeaderTemplate>
                    <prop:ItemTemplate>
                        <div class="item_consultation list-group-item kt-callout left kt-vertical-align success small">
                            <!--BEGIN COLONNE CHECKBOX-->
                            <div class="col-md-1 top p-l-0" headers="selection">
                                <com:TActiveImage id="imgStatutCapaciteOk" Style="display:<%=($this->TemplateControl->getModeAffichage() && $this->Data->getStatutOk())?'':'none'%>"
                                                  cssClass="statut-mps" ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-ok-small.gif" Attributes.alt="<%=Prado::localize('INFORMATIONS_DECLARES_SUR_HONNEUR')%>"/>
                                <com:TActiveImage id="imgStatutCapaciteNotOk" Style="display:<%=($this->TemplateControl->getModeAffichage() && !$this->Data->getStatutOk())?'':'none'%>"
                                                  cssClass="statut-mps" ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif" Attributes.alt="<%=Prado::localize('INFORMATIONS_MANQUANTES')%>"/>
                            </div>
                            <!--END COLONNE CHECKBOX-->

                            <!--BEGIN EXERCICE CLOS EN-->
                            <div class="col-md-1 top text-center" headers="capacites_col_01">
                                <com:TLabel ID="anneeCloture" Text="<%#$this->Data->getAnneeCloture()%>"/>
                            </div>
                            <!--END EXERCICE CLOS EN-->

                            <!--BEGIN DATE EXERCICE-->
                            <div class="col-md-2 top text-center" headers="capacites_col_02">
                                <com:TTranslate>DEFINE_DU</com:TTranslate>
                                <com:TLabel ID="exerciceDateDebut" Text="<%#$this->Data->getDateDebut()%>"/>
                                <com:TTranslate>TEXT_AU</com:TTranslate>
                                <com:TLabel ID="exerciceDateFin" Text="<%#$this->Data->getDateFin()%>"/>
                            </div>
                            <!--END DATE EXERCICE-->

                            <!--BEGIN CHIFFRE D'AFFAIRE-->
                            <div class="col-md-2 top text-center" headers="capacites_col_03">
                                <com:TLabel ID="exerciceChiffreAffaire" Text="<%#$this->Data->getChiffreAffaire()?$this->Data->getChiffreAffaire():' - '%>"/>
                                <%#$this->Data->getChiffreAffaire()?Prado::Localize('DEFINE_UNITE_HT'):''%>
                            </div>
                            <!--END CHIFFRE D'AFFAIRE-->

                            <!--BEGIN SIGNIFICATION POURCENTAGE-->
                            <div class="col-md-2 top text-center" headers="capacites_col_04">
                                <com:TLabel ID="exerciceSignificatif" Text="<%#$this->Data->getSignificatif()?$this->Data->getSignificatif():' - '%>"/>
                                <%#$this->Data->getSignificatif()?Prado::Localize('POUR_CENT'):''%>
                            </div>
                            <!--END SIGNIFICATION POURCENTAGE-->

                            <!--BEGIN COMMENTAIRE-->
                            <div class="col-md-3 top text-center" headers="capacites_col_05">
                                <com:TLabel ID="exerciceCommentaire" Text="<%#$this->Data->getCommentaire()?$this->Data->getCommentaire():' - '%>"/>
                            </div>
                            <!--END COMMENTAIRE-->
                        </div>
                    </prop:ItemTemplate>
                </div>
            </com:TActiveRepeater>

            <com:TActiveRepeater ID="repeaterCapacitesRecapitulationVide" EnableViewState="false">
                <div class="table table-striped table-results list-group m-t-2">
                    <prop:HeaderTemplate>
                        <div class="list-group-item header kt-vertical-align small ">
                            <div class="kt-vertical-align">
                                <!--BEGIN COLONNE CHECKBOX-->
                                <div class="col-md-1" id="selection"></div>
                                <!--END COLONNE CHECKBOX-->

                                <!--BEGIN EXERCICE CLOS EN-->
                                <div class="col-md-1 p-l-0" id="capacites_col_01">
                                    <com:TTranslate>DEFINE_EXERCICE_CLOS_EN</com:TTranslate>
                                </div>
                                <!--END EXERCICE CLOS EN-->

                                <!--BEGIN DATE EXERCICE-->
                                <div class="col-md-2 text-center" id="capacites_col_02">
                                    <com:TTranslate>DATE_D_EXERCICE</com:TTranslate>
                                </div>
                                <!--END DATE EXERCICE-->

                                <!--BEGIN CHIFFRE D'AFFAIRE-->
                                <div class="col-md-2 text-center" id="capacites_col_03">
                                    <com:TTranslate>CHIFFRE_AFFAIRES_REALISE_OU_ESTIME</com:TTranslate>
                                </div>
                                <!--END CHIFFRE D'AFFAIRE-->

                                <!--BEGIN SIGNIFICATION POURCENTAGE-->
                                <div class="col-md-2 text-center" id="capacites_col_04">
                                    <%=$this->TemplateControl->getConsultationAlloti()?Prado::Localize('SIGNIFICATIF_POUR_CONSULTATION_ALLOTI'):Prado::Localize('SIGNIFICATIF_POUR_CONSULTATION')%>
                                </div>
                                <!--END SIGNIFICATION POURCENTAGE-->

                                <!--BEGIN COMMENTAIRE-->
                                <div class="col-md-3 text-center" id="capacites_col_05">
                                    <%=$this->TemplateControl->getConsultationAlloti()?Prado::Localize('DEFINE_TEXT_COMMENTAIRES_CAS_ALLOTI'):Prado::Localize('DEFINE_TEXT_COMMENTAIRES')%>
                                </div>
                                <!--END COMMENTAIRE-->
                            </div>
                        </div>
                    </prop:HeaderTemplate>
                    <prop:ItemTemplate>
                        <div class="item_consultation list-group-item kt-callout left kt-vertical-align success">
                            <!--BEGIN COLONNE CHECKBOX-->
                            <div class="col-md-1 top" headers="selection">
                                <com:TActiveImage id="imgStatutCapaciteNotOk" cssClass="statut-mps" ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif"
                                                  Attributes.alt="<%=Prado::localize('INFORMATIONS_MANQUANTES')%>"/>
                            </div>
                            <!--END COLONNE CHECKBOX-->

                            <!--BEGIN EXERCICE CLOS EN-->
                            <div class="col-md-1 top text-center" headers="capacites_col_01">
                                -
                            </div>
                            <!--END EXERCICE CLOS EN-->

                            <!--BEGIN DATE EXERCICE-->
                            <div class="col-md-2 top p-l-0 text-center" headers="capacites_col_02">
                                -
                            </div>
                            <!--END DATE EXERCICE-->

                            <!--BEGIN CHIFFRE D'AFFAIRE-->
                            <div class="col-md-2 top p-l-0 text-center" headers="capacites_col_03">
                                -
                            </div>
                            <!--END CHIFFRE D'AFFAIRE-->

                            <!--BEGIN SIGNIFICATION POURCENTAGE-->
                            <div class="col-md-2 top text-center" headers="capacites_col_04">
                                -
                            </div>
                            <!--END SIGNIFICATION POURCENTAGE-->

                            <!--BEGIN COMMENTAIRE-->
                            <div class="col-md-3 top text-center" headers="capacites_col_05">
                                -
                            </div>
                            <!--END COMMENTAIRE-->
                        </div>
                    </prop:ItemTemplate>
                </div>
            </com:TActiveRepeater>
        </com:TActivePanel>
        <!--Fin Tableau Capacités Recapitulation-->

        <hr/>
        <div class="form-horizontal">
            <div class="form-group clearfix">
                <label class="col-md-3" for="ctl0_CONTENU_PAGE_blocCapacite_trancheEffectifs">
                    <com:TActiveImage id="imgTrancheEffectifs" cssClass="m-r-1" Style="display:<%=$this->getModeAffichage()?'':'none'%>"/>
                    <com:TTranslate>TRANCHE_D_EFFECTIFS</com:TTranslate> :
                </label>
                <div class="col-md-9 form-group-sm">
                    <com:TActiveDropDownList ID="trancheEffectifs" cssClass="form-control"  Style="display:<%=$this->getModeAffichage()?'none':''%>"/>
                    <com:TActiveLabel id="trancheEffectifsLabel" Style="display:<%=$this->getModeAffichage()?'':'none'%>"/>
                </div>
            </div>
        </div>
    </div>
</div>
<!--Fin Bloc Capacités-->
<com:TTextBox id="idHiddenValidateDonneesCapacite" text="<%=$this->getId()%>_repeaterCapacites" style="display:none"/>
<com:TCustomValidator
        Id="validateSignificatif"
        ValidationGroup="validateInfosCandidatureMPS"
        ControlToValidate="idHiddenValidateDonneesCapacite"
        Display="Dynamic"
        ClientValidationFunction="validateExerciceSignificatif"
        ErrorMessage="Le % significatif pour cette consultation doit être un chiffre numerique"
        EnableClientScript="true"
        Text=" ">
    <prop:ClientSide.OnValidationError>
        document.getElementById('divValidationSummary').style.display='';
    </prop:ClientSide.OnValidationError>
</com:TCustomValidator>
<com:TCustomValidator
        Id="validateDateDebut"
        ValidationGroup="validateInfosCandidatureMPS"
        ControlToValidate="idHiddenValidateDonneesCapacite"
        Display="Dynamic"
        ClientValidationFunction="validateDateDebut"
        ErrorMessage="<%=Prado::localize('DATE_D_EXERCICE_DEBUT')%>"
        EnableClientScript="true"
        Text=" ">
    <prop:ClientSide.OnValidationError>
        document.getElementById('divValidationSummary').style.display='';
    </prop:ClientSide.OnValidationError>
</com:TCustomValidator>
<com:TCustomValidator
        Id="validateDateFin"
        ValidationGroup="validateInfosCandidatureMPS"
        ControlToValidate="idHiddenValidateDonneesCapacite"
        Display="Dynamic"
        ClientValidationFunction="validateDateFin"
        ErrorMessage="<%=Prado::localize('DATE_D_EXERCICE_FIN')%>"
        EnableClientScript="true"
        Text=" ">
    <prop:ClientSide.OnValidationError>
        document.getElementById('divValidationSummary').style.display='';
    </prop:ClientSide.OnValidationError>
</com:TCustomValidator>


