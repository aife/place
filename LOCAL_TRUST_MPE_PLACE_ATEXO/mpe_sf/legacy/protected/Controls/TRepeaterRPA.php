<?php

namespace Application\Controls;

use App\Entity\Organisme;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\EntityPurchase\Atexo_EntityPurchase_RPA;
use Exception;
use Prado\Prado;
use Prado\Util\TLogger;

class TRepeaterRPA extends MpeTTemplateControl
{
    public $_ID;
    protected $critereTri;
    protected $triAscDesc;
    protected bool $entiteValide = true;
    public $_organisme;

    public function getOrganisme()
    {
        return $this->_organisme;
    }

    public function setOrganisme($value)
    {
        $this->_organisme = $value;
    }

    /**
     * displayEA remplie le tableau des RPAs.
     **/
    public function detailRPA($IdEntite, ?string $organisme = null): void
    {
        try {
            if (Atexo_Module::isEnabled('SocleInterne') || Atexo_Module::isEnabled('SocleExternePpp')) {
                $this->entiteValide = false;
            }

            $this->Page->setViewState('organismeRpa', $this->_organisme);
            $this->Page->setViewState('idEntiteRpa', $IdEntite);
            if ($this->_organisme) {
                if (null !== Atexo_CurrentUser::getCurrentServiceId()) {
                    $arrayServicesVaide = Atexo_EntityPurchase::getSubServices(Atexo_CurrentUser::getCurrentServiceId(), $this->_organisme, true);
                    if ($arrayServicesVaide && !in_array($IdEntite, $arrayServicesVaide)) {
                        $this->entiteValide = false;
                    }
                }
            }
            $this->Page->setViewState('entiteValide', $this->entiteValide);
            Atexo_EntityPurchase_RPA::setCachedRpas($IdEntite, $this->_organisme);
            $Rpa = Atexo_EntityPurchase_RPA::getCachedRpas($IdEntite, $this->_organisme);
            //$Rpa = Atexo_EntityPurchase_RPA::getRpaByIdService($IdEntite, $this->_organisme);
            $this->RepeaterListRepresentant->dataSource = $Rpa;
            $this->RepeaterListRepresentant->DataBind();
        } catch (Exception $e) {
            echo $e;
            Prado::log("Impossible de récuperer le tableau des RPA'-".$e->__toString().'-', TLogger::FATAL, 'Fatal');
        }
    }

    public function onDeleteClick($sender, $param)
    {
        $idrpa = $param->CommandParameter;
        $IdService = Atexo_EntityPurchase_RPA::DeleteRpa($idrpa, $this->_organisme);
        Atexo_EntityPurchase_RPA::setCachedRpas($IdService, $this->_organisme);
    }

    public function refreshRepeaterRpa($sender, $param)
    {
        // Rafraichissement du ActivePanel

        $Rpa = Atexo_EntityPurchase_RPA::getCachedRpas($param->CallbackParameter, $this->_organisme);
        $this->RepeaterListRepresentant->dataSource = $Rpa;
        $this->RepeaterListRepresentant->dataBind();
        $this->PanelListRepresentant->render($param->NewWriter);
    }

    /**
     * Trier un tableau des invités sélctionnés.
     *
     * @param sender
     * @param param
     */
    public function sortRpa($sender, $param)
    {
        $this->critereTri = $this->getViewState('critereTri');
        $this->triAscDesc = $this->getViewState('triAscDesc');
        $Rpa = Atexo_CurrentUser::readFromSession('cachedRpas');

        if ('FirstName' == $sender->getActiveControl()->CallbackParameter) {
            $dataSourceTriee = $this->getDataSourceTriee($Rpa, 'Nom', '');
        } else {
            return;
        }

        // Mise dans la session de la liste des invités triée
        Atexo_CurrentUser::deleteFromSession('cachedRpas');
        Atexo_CurrentUser::writeToSession('cachedRpas', $dataSourceTriee[0]);

        //Mise en viewState des critères de tri
        $this->setViewState('critereTri', $this->critereTri);
        $this->setViewState('triAscDesc', $this->triAscDesc);

        //Remplissage du tabeau des Rpas
        $this->RepeaterListRepresentant->DataSource = $dataSourceTriee[0];
        $this->RepeaterListRepresentant->DataBind();

        // Re-affichage du TActivePanel
        $this->PanelListRepresentant->render($param->getNewWriter());
    }

    /**
     * Retourne un tableau de colonnes et un tableau d'objets tries.
     *
     * @param array  $dataSource    le tableau des données
     * @param string $critere
     * @param string $typeCritere
     * @param int    $indiceElement
     */
    private function getDataSourceTriee($dataSource, $critere, $typeCritere = null)
    {
        $resultat = [];
        $this->setCritereAndTypeTri($critere);
        $datatriee = null;
        if ('num' == $typeCritere) {
            $datatriee = Atexo_Util::sortArrayOfObjects($dataSource, $critere, $this->triAscDesc, 'num');
        } else {
            $datatriee = Atexo_Util::sortArrayOfObjects($dataSource, $critere, $this->triAscDesc);
        }
        $resultat[0] = $datatriee;

        return $resultat;
    }

    /**
     * Définit les critères et les types de tri.
     *
     * @param string $critere
     * @param int    $indiceElement
     */
    private function setCritereAndTypeTri($critere)
    {
        if ($this->critereTri != $critere || !$this->critereTri) {
            $this->triAscDesc = 'ASC';
        } else {
            if ('ASC' == $this->triAscDesc) {
                $this->triAscDesc = 'DESC';
            } else {
                $this->triAscDesc = 'ASC';
            }
        }
        $this->critereTri = $critere;
    }
}
