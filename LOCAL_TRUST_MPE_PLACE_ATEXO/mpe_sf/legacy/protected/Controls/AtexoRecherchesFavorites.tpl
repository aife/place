<div class="clearfix">
	<ul class="breadcrumb">
		<li>
			<a href="/entreprise">
						<com:TTranslate>TEXT_ACCUEIL</com:TTranslate>
                    </a>
		</li>
		<li>
			<com:TTranslate>TEXT_MES_RECHERCHES</com:TTranslate>
		</li>
		<li>
			<com:TLabel ID="fileAriane" Text="<%=$this->getTextFileAriane()%>"/>
		</li>
	</ul>
</div>

<!--BEGIN BLOC GESTION ALERT-->
<com:TActivePanel ID="panelRecherches" cssClass="form-field rechercheFavorites">
	<com:PanelMessageErreur ID="panelMessageErreur"/>
	<com:PanelMessageAvertissement visible = "false" id="panelAvertissement" cssClass="message bloc-700"/>

	<com:TActivePanel ID="panelListeRecherche">
		<div class="content">
			<h1 class="h4 page-header m-t-2">
				<com:TLabel ID="titreTableauRecherche" Text="<%=$this->getTitreTableauRecherche()%>"/>
			</h1>
			<!--BEGIN TABLE ALERT-->
			<div class="table table-striped table-results list-group m-t-2 text-center">
				<com:TRepeater ID="repeaterAlertes" >
					<prop:HeaderTemplate>
						<div>
							<div class="list-group-item active kt-vertical-align small">
								<div class="col-md-5" id="descriptionRecherche"></div>
								<div class="col-md-2" id="typeAvis">
									<com:Tlabel Text="<%=Prado::Localize('TEXT_TYPE_AVIS')%>" />
								</div>
								<div class="col-md-2">
									<com:TPanel  Visible="<%=$this->TemplateControl->getTypeCreateur() ==  Application\Service\Atexo\Atexo_Config::getParameter('TYPE_CREATEUR_ENTREPRISE')%>" >
										<div class="col-80"> <com:TTranslate>TEXT_ALERTE</com:TTranslate> / <com:TTranslate>DEFINE_RECHERCHE</com:TTranslate></div>
									</com:TPanel>
								</div>
								<div class="col-md-1" id="modifier">
									<com:Tlabel Text="<%=Prado::Localize('BOUTON_MODIFIER')%>" Visible="<%=isset($_GET['typeAnnonce'])?false:true%>"/>
								</div>
								<div class="col-md-1" id="supprimer">
									<com:TTranslate>DEFINE_TEXT_SUPPRIMER</com:TTranslate>
								</div>
								<div class="col-md-1" id="recherche">
									<com:TTranslate>TEXT_RECHERCHE</com:TTranslate>
								</div>
							</div>
						</div>
					</prop:HeaderTemplate>

					<prop:ItemTemplate>
						<div class="item_consultation list-group-item kt-callout left kt-vertical-align <%#(($this->ItemIndex%2==0)? '':'on')%>">

							<div class="col-md-5 text-left col-xs-10 float-left-sm">
								<%#$this->Data->getDenomination()%>
							</div>

							<div class="col-md-2 col-xs-12">
									<com:Tlabel Text="<%=Prado::Localize(($this->Data->getTypeAvis() == Application\Service\Atexo\Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION')) ? 'DEFINE_CONSULTATION' : 'TEXT_AUTRES_ANNONCES')%>" />
							</div>
							<div class="col-md-2 col-xs-12">
								<com:TPanel cssClass="blocActionFavorites-m" id="panelTypeRecherche" Visible="<%=$this->TemplateControl->getTypeCreateur() ==  Application\Service\Atexo\Atexo_Config::getParameter('TYPE_CREATEUR_ENTREPRISE')%>" >
									<span data-toggle="tooltip" title="<com:TTranslate>TEXT_ALERTE</com:TTranslate>">
										<i class="fa fa-bell-o" Style="display:<%#$this->Data->getAlerte()?'':'none'%>;"></i>
									</span>
									<span data-toggle="tooltip" title="<com:TTranslate>DEFINE_RECHERCHE</com:TTranslate>">
										<i class="fa fa-search" Style="display:<%#$this->Data->getRecherche()?'':'none'%>;"></i>
									</span>
								</com:TPanel>
							</div>

							<div class="col-md-1 col-xs-2 col-xs-offset-6 float-left-sm">
								<com:THiddenField id="labelDescriptionRecherche" value="<%#$this->Data->getDenomination()%>" />
								<div class="btn btn-link p-0" data-toggle="tooltip" title="<%=Prado::localize('TEXT_EDITER_LA_LIGNE')%>">
									<com:TLinkButton
											Visible="<%=($this->Data->getTypeAvis() == Application\Service\Atexo\Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION'))%>"
											Attributes.title="<%=Prado::localize('TEXT_EDITER_LA_LIGNE')%>"
											cssClass="btn btn-sm btn-primary"
											onCommand="page.rechercheFavorites.modifierMaRecherche"
											CommandParameter="<%#$this->Data->getId()%>" >
										<i class="fa fa-pencil"></i>
									</com:TLinkButton>
								</div>
							</div>

							<div class="col-md-1 col-xs-2 float-left-sm">
								<div class="btn btn-link p-0" data-toggle="tooltip" title="<%=Prado::localize('TEXT_SUPPRIMER_LA_LIGNE')%>">
									<button type="button" class="btn btn-sm btn-danger pull-left btn-delete"
											data-toggle="modal" data-target="#modalDelete"
											onClick = "document.getElementById('<%#$this->SourceTemplateControl->idToDetele->getClientId()%>').value = '<%#$this->Data->getId()%>';
														  document.getElementById('<%#$this->SourceTemplateControl->nameToDelete->getClientId()%>').innerHTML =  document.getElementById('<%#$this->labelDescriptionRecherche->getClientId()%>').value;">
										<i class="fa fa-trash"></i>
									</button>
								</div>
							</div>

							<div class="col-md-1 col-xs-2 float-left-sm">
								<div data-toggle="tooltip" title="<%=Prado::localize('TEXT_LANCER_RECHERCHE_CORRESPONDANT_AUX_CRITERES')%>">
									<com:TLinkButton
											CommandName="Search"
											cssClass="btn btn-sm btn-primary"
											onCommand="SourceTemplateControl.actionRecherche"
											CommandParameter="<%#$this->Data->getId()%>">
										<i class="fa fa-search-plus"></i>
									</com:TLinkButton>
								</div>
							</div>
						</div>
					</prop:ItemTemplate>
				</com:TRepeater>
			</div>
			<!--END TABLE ALERT-->
		</div>
	</com:TActivePanel>
	<a href="<%=$this->getCheminAdvancedSearch()%>"
	   data-toggle="tooltip"
	   data-placement="top"
	   title="<com:TTranslate>TEXT_CREATE_ALERT</com:TTranslate>"
	   class="btn btn-primary btn-sm pull-right btn-action-m">
		<i class="fa fa-bell-o m-r-1" aria-hidden="true"></i>
		<com:TTranslate>DEFINE_TEXT_CREER</com:TTranslate>
	</a>
	<!--BEGIN BTN RETOUR-->
	<a href="<%=(($this->typeCreateur ==  Application\Service\Atexo\Atexo_Config::getParameter('TYPE_CREATEUR_ENTREPRISE') )? 'index.php?page=Entreprise.EntrepriseAccueilAuthentifie':'agent/accueil')%>"
	   class="btn btn-sm btn-default pull-left btn-return-m"
	   data-toggle="tooltip" data-placement="top"
	   title="<%=Prado::localize('TEXT_RETOUR_MON_COMPTE')%>">
		<com:TTranslate>TEXT_RETOUR</com:TTranslate>
	</a>
	<!--END BTN RETOUR-->
</com:TActivePanel>
<!--END BLOC GESTION ALERT-->

<!--Debut Modal Recherche-->
<div class="modal-recherche" style="display:none;">
	<com:AtexoValidationSummary id="divValidation" ValidationGroup="validateName" />
	<!--Debut Formulaire-->
	<div class="form-bloc">
		<div class="content">
			<div class="line">
				<label class="intitule-120"><com:TTranslate>TEXT_NOM_RECHERCHE</com:TTranslate><strong><span style="color:red">*</span></strong>:</label>
				<com:TActiveTextBox ID="rechercheName" cssClass="bloc-400" />
				<com:TRequiredFieldValidator
						ControlToValidate="rechercheName"
						ValidationGroup="validateName"
						Display="Dynamic"
						ErrorMessage="<%=Prado::localize('TEXT_NOM_RECHERCHE')%>"
						EnableClientScript="true"
						Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
					<prop:ClientSide.OnValidationError>
						document.getElementById('divValidation').style.display='block';
					</prop:ClientSide.OnValidationError>
					<prop:ClientSide.OnValidationSuccess>
						document.getElementById('divValidation').style.display='none';
					</prop:ClientSide.OnValidationSuccess>
				</com:TRequiredFieldValidator>
			</div>
		</div>
	</div>
	<!--Fin Formulaire-->
	<!--Debut line boutons-->
	<div class="boutons-line">
		<input type="button"
			   class="bouton-moyen float-left"
			   value="Annuler" title="Annuler"
			   onclick="J('.modal-recherche').dialog('close');"
		/>
		<com:TActiveButton
				id="modifierRecherche"
				Text = "<%=Prado::localize('TEXT_ENREGISTER')%>"
				Attributes.title="<%=Prado::localize('TEXT_ENREGISTER')%>"
				ValidationGroup="validateName"
				CssClass="bouton-moyen float-right"
				CommandName="Modify"
				CommandParameter="<%=$this->idToDetele->value%>"
				onCommand="actionRecherche"
				onCallBack="refreshRepeater"
		>
		</com:TActiveButton >
	</div>
	<!--Fin line boutons-->
</div>
<!--Fin Modal Recherche-->

<!--BEGIN MODAL DELETE-->
<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-labelledby="modalDelete" style="display: none;">
	<!--Debut Formulaire-->
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-body">
				<strong>
					<com:TTranslate>TEXT_ALERTE_SUPPRESSION_RECHERCHE</com:TTranslate>
				</strong>
				<com:TActiveLabel ID="nameToDelete" text=""/>
			</div>
			<div class="modal-footer">
				<!--Debut line boutons-->
				<div class="boutons-line">
					<com:THiddenField id="idToDetele"/>
					<button type="button" class="btn btn-sm btn-default pull-left" data-dismiss="modal"><%=Prado::localize('DEFINE_ANNULER')%></button>
					<com:TActiveButton
							id="cofirmSupression"
							Text = "<%=Prado::localize('TEXT_POURSUIVRE')%>"
							Attributes.title="<%=Prado::localize('TEXT_POURSUIVRE')%>"
							CssClass="btn btn-sm btn-danger"
							CommandName="Delete"
							onCommand="actionRecherche"
							onCallBack="refreshRepeater"
							CommandParameter="<%#$this->idToDetele->value%>"
							Attributes.data-dismiss="modal"
							Attributes.OnClick="J('#modalDelete').modal('hide');"
					>
					</com:TActiveButton >
				</div>
				<!--Fin line boutons-->
			</div>
		</div>
	</div>
	<!--Fin Formulaire-->

</div>
<!--END MODAL DELETE-->
