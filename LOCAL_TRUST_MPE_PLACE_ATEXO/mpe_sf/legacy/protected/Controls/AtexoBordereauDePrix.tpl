<div class="form-field margin-0 bloc-bdp-formulaire">
		<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
		<div class="content">
				<div class="float-right"><com:TTranslate>DEFINE_SYMBOLE</com:TTranslate> <span class="champ-oblig">*</span> <com:TTranslate>TEXT_INDIQUE_CHAMPS_OBLIGATOIRES</com:TTranslate></div>
				<div class="spacer-mini"></div>
				
				<com:TRepeater ID="repeaterBordereauPrixItems" >
				<prop:HeaderTemplate>
				<table class="table-results tableau-prix" summary="Tableau Bordereu de Prix">
					<caption>Détail des prix</caption>
					<thead>
						<tr>

							<th class="top" colspan="6"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></th>
						</tr>
						<tr>
							<th class="col-40" id="bdp_col_01"><com:TTranslate>DEFINE_ITEM</com:TTranslate></th>
							<th class="col-530" id="bdp_col_02"><com:TTranslate>DEFINE_DESCRIPTION</com:TTranslate></th>
							<th class="col-tva" id="bdp_col_03"><div class="bloc-tva"><input id="appliquerPartout" class="check" type="checkbox" title="<%#Prado::localize('TEXT_APPLIQUER_PAR_TOUT')%>" name="appliquerPartout" disabled="disabled"><label for="tauxTva"><com:TTranslate>TEXT_TVA</com:TTranslate></label><com:TTextBox id="tauxTva" Text="" cssClass="taux-tva-copy" enabled="false"/>%</div></th>
							<th class="col-prix" id="bdp_col_04"><com:TTranslate>TEXT_PRIX_UNITAIRE</com:TTranslate><br /><com:TTranslate>DEFINE_EURO_HT</com:TTranslate><br /><com:TTranslate>DEFINE_UNITE_TTC</com:TTranslate></th>
							<th class="col-quantite" id="bdp_col_05"><com:TTranslate>DEFINE_QUANTITE</com:TTranslate></th>
							<th class="col-prix" id="bdp_col_06"><com:TTranslate>TEXT_PRIX_TOTAL</com:TTranslate><br /><com:TTranslate>DEFINE_EURO_HT</com:TTranslate><br /><com:TTranslate>DEFINE_UNITE_TTC</com:TTranslate></th>
						</tr>
					</thead>
					</prop:HeaderTemplate>
	    			<prop:ItemTemplate>
					<tr class="<%#(($this->ItemIndex%2==0)? 'on':'')%>">
						<td class="col-40" headers="bdp_col_01"><%#$this->Data->getNumero()%></td>

						<td class="col-300" headers="bdp_col_02">
							<%#$this->Data->getLibelle()%>
							<div class="commentaire">
								<div class="intitule"><com:TTranslate>DEFINE_COMMENTAIRE_ACHETEUR</com:TTranslate> :</div>
								<%#($this->Data->getCommentaireAcheteur()!='')?$this->Data->getCommentaireAcheteur():'-'%>
							</div>
							
							<!-- precision entreprise -->
							<com:TPanel id="panelPrecisionEntrp" cssClass="commentaire-reponse" Visible="<%#($this->Data->getPrecisionEntreprise()== Application\Service\Atexo\Atexo_Config::getParameter('OBLIGATOIRE') || $this->Data->getPrecisionEntreprise()== Application\Service\Atexo\Atexo_Config::getParameter('LIBRE'))%>">
								<div class="intitule"><label for="itemPrecisionEntrp"><com:TTranslate>DEFINE_PRECISION_ENTREPRISE</com:TTranslate></label> :</div>
								<com:TTextBox id="itemPrecisionEntrp" TextMode="MultiLine" Text="<%#$this->Data->getValuePrecisionentreprise()%>" enabled="false"/>
								<com:TLabel Visible="<%#$this->Data->getPrecisionEntreprise()== Application\Service\Atexo\Atexo_Config::getParameter('OBLIGATOIRE')%>" cssClass="champ-oblig">*</com:TLabel>
								<com:TRequiredFieldValidator 
									ControlToValidate="itemPrecisionEntrp"
									ValidationGroup="validateItemsFormCons"
									Display="Dynamic" 
									Enabled="<%#$this->Data->getPrecisionEntreprise()== Application\Service\Atexo\Atexo_Config::getParameter('OBLIGATOIRE')%>"
									ErrorMessage="<%#'Item '.$this->Data->getNumero().': '.Prado::localize('DEFINE_PRECISION_ENTREPRISE')%>"
									EnableClientScript="true"  						 					 
						 			Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
									<prop:ClientSide.OnValidationError>
           				 				document.getElementById('divValidationSummary').style.display='';
                                        document.getElementById('ctl0_CONTENU_PAGE_valider').style.display='';
         							</prop:ClientSide.OnValidationError>
         						</com:TRequiredFieldValidator>
							</com:TPanel>
															
						</td>
						<td class="col-tva" headers="bdp_col_03"><div class="bloc-tva"><com:TTextBox id="itemTauxTva" Text="<%#$this->Data->getTva()%>" cssClass="taux-tva"  enabled="false"/>%</div>
							<com:TRequiredFieldValidator 
								ControlToValidate="itemTauxTva"
								ValidationGroup="validateItemsFormCons"
								Display="Dynamic"
								ErrorMessage="<%#'Item '.$this->Data->getNumero().': '.Prado::localize('TEXT_TVA')%>"
								EnableClientScript="true"  						 					 
					 			Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
								<prop:ClientSide.OnValidationError>
	   				 				document.getElementById('divValidationSummary').style.display='';
	                                document.getElementById('ctl0_CONTENU_PAGE_valider').style.display='';
	 							</prop:ClientSide.OnValidationError>
	 						</com:TRequiredFieldValidator>	
	 						<com:TRangeValidator
                                  ControlToValidate="itemTauxTva"
                                  ValidationGroup="validateItemsFormCons" 
                                  DataType="Float"
                                  Display="Dynamic"
                                  ErrorMessage="<%#'Item '.$this->Data->getNumero().': '.Prado::localize('TEXT_TVA')%>"
                                  EnableClientScript="true"
                                  Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
						         <prop:ClientSide.OnValidationError>
						              document.getElementById('divValidationSummary').style.display='';
						              document.getElementById('ctl0_CONTENU_PAGE_valider').style.display='';
						          </prop:ClientSide.OnValidationError>
					         </com:TRangeValidator>
						</td>
						<td class="col-prix" headers="bdp_col_04">
							<com:TTextBox id="itemPrixUnitaireHt" Text="<%#$this->Data->getPrixUnitaireValue()%>"  enabled="false"/><br />
							<com:TLabel cssClass="champ-prix" id="itemPrixUnitaireHtCalcule" Text=""/>
							<com:TRequiredFieldValidator 
								ControlToValidate="itemPrixUnitaireHt"
								ValidationGroup="validateItemsFormCons"
								Display="Dynamic"
								ErrorMessage="<%#'Item '.$this->Data->getNumero().': '.Prado::localize('TEXT_PRIX_UNITAIRE')%>"
								EnableClientScript="true"  						 					 
					 			Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
								<prop:ClientSide.OnValidationError>
	   				 				document.getElementById('divValidationSummary').style.display='';
	                                document.getElementById('ctl0_CONTENU_PAGE_valider').style.display='';
	 							</prop:ClientSide.OnValidationError>
	 						</com:TRequiredFieldValidator>
	 						<com:TRangeValidator
                                  ControlToValidate="itemPrixUnitaireHt"
                                  ValidationGroup="validateItemsFormCons" 
                                  DataType="Float"
                                  Display="Dynamic"
                                  ErrorMessage="<%#'Item '.$this->Data->getNumero().': '.Prado::localize('TEXT_PRIX_UNITAIRE')%>"
                                  EnableClientScript="true"
                                  Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
						         <prop:ClientSide.OnValidationError>
						              document.getElementById('divValidationSummary').style.display='';
						              document.getElementById('ctl0_CONTENU_PAGE_valider').style.display='';
						          </prop:ClientSide.OnValidationError>
					         </com:TRangeValidator>
						</td>

						<td class="col-quantite" headers="bdp_col_05"><com:TLabel id="itemQuantite" Text="<%#($this->Data->getQuantite())?$this->Data->getQuantite():'-'%>"/></td></td>
						<td class="col-prix" headers="bdp_col_06"><com:TLabel id="itemPrixTotalHt" Text=""/><br /><com:TLabel id="itemPrixTotalTtc" Text=""/></td>
						<com:THiddenField id="idItemFormcons" value="<%#$this->Data->getId()%>"/>
						<com:THiddenField id="idFormcons" value="<%#$this->Data->getIdFormulaireConsultation()%>"/>
						<com:THiddenField id="isObligatoire" value="<%#$this->Data->getObligatoire()%>"/>
						<com:THiddenField id="precisionEntrp" value="<%#$this->Data->getPrecisionEntreprise()%>"/>
						<com:THiddenField id="typeReponse" value="<%#$this->Data->getTypeReponse()%>"/>
						<com:THiddenField id="idInscritFormcons" value="<%#$this->Data->getId()%>"/>
					</tr>
					
					</prop:ItemTemplate>
        			<prop:FooterTemplate>
        			<tr class="total">
						<td colspan="5" class="align-left"><com:TTranslate>DEFINE_TOTAL</com:TTranslate></td>
						<td><com:TLabel id="htTotal" Text=""/>
						<com:THiddenField id="htTotalHidden" />
						<br /><com:TLabel id="ttcTotal" Text=""/></td>
					</tr>
				</table>
				</prop:FooterTemplate>
        		</com:TRepeater>
        		<com:THiddenField id="nbreItems" value="<%=count($this->repeaterBordereauPrixItems->DataSource)%>"/>
			<div class="breaker"></div>
		</div>
		<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
	</div>