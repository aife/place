<!-- com:TActivePanel id="panelPieces"-->
<com:TRepeater ID="listePiecesRepeater"
				  			   EnableViewState="true"
				  			   DataKeyField="Id"
				  			   OnItemCommand="deleteItemPiece"
							>
							<prop:HeaderTemplate>
							<table class="table-results" summary="Liste des pièces de l'enveloppe de candidature">
								<caption><com:TTranslate>TEXT_PIECES_A_TRANSMETTRE</com:TTranslate></strong></caption>
								<thead style="display: none;">
									<tr>
	
											<th class="top" colspan="3"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></th>
									</tr>
									<tr>
										<th id="numPieceCandidature" class="col-150"><com:TTranslate>TEXT_PIECES_A_TRANSMETTRE</com:TTranslate></strong></th>
										<th id="fichierCandidature" class="col-450">&nbsp;</th>
										<th id="Candidature" class="col-100"><com:TTranslate>TEXT_STATUT_SIGNATURE</com:TTranslate></th>
									</tr>
								</thead>
								</prop:HeaderTemplate>
								<prop:ItemTemplate>
								<tr class="<%#(($this->TemplateControl->styleTableauPieces())?(($this->ItemIndex%2==0)? 'on':''):(($this->ItemIndex%2==0)? '':'on'))%>">
									<td headers="numPieceCandidature" class="col-150">
										<label for="piece_01"><com:TLabel id="pieceText"/><com:TLabel id="numPiece" Text="<%#$this->ItemIndex+1%>"/></label> :
									</td>
									<td class="col-480" headers="fichierCandidature">
										<com:TTextBox ID="intitulePiece" 
													  Attributes.title="<%=Prado::localize('CHOIX_REPERTOIRE')%>"
													  CssClass="bloc-360 float-left" />
										<input 
											type="button"
											class="float-left margin-0"
											name="javaStartDirectoryChooser" 
											id="javaStartDirectoryChooser" 
											title="<%=Prado::localize('CHOIX_REPERTOIRE')%>" 
											Value="<%=Prado::localize('DEFINE_PARCOURIR')%>"
											onClick="document.AppletFileChooser.fctTrt1('<%=Application\Service\Atexo\Atexo_Util::toUtf8($this->intitulePiece->ClientId)%>');"
										/>
										
										<com:THiddenField id="pathSignature"/>
										<com:THiddenField id="fichiersSansSignatures"/>
										<com:THiddenField id="fichiersAvecPlsrsSignatures"/>
										<com:THiddenField id="fichiersAvecCertificatExpire"/>
										<com:THiddenField id="signaturePriseEnCompte"/>
										<script>addFilesSignature('<%#$this->intitulePiece->ClientId%>');</script>
									   <com:TActiveImageButton 	
											id="deleteBtn"
											cssClass="fichier-action"
											visible="false"
											ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-supprimer.gif"
											ToolTip="Supprimer" 
											AlternateText="Supprimer"
											OnCallback="Page.activeRefreshPieces"
											Attributes.OnClick="document.AppletSignaturesInfos.fctTrt2('<%#$this->intitulePiece->ClientId%>');sendIdBtnDelete('<%#$this->intitulePiece->ClientId%>');"
											CommandName="delete">
										</com:TActiveImageButton>
										<com:TLabel id="cheminFichier" />
									</td>
										
									<td class="col-100" headers="Candidature">
										<com:TLabel id="imgSignElectrnik" />
									</td>
								</tr>
								</prop:ItemTemplate>
								<prop:FooterTemplate>	
							</table>
							</prop:FooterTemplate>
							</com:TRepeater>
<!-- /com:TActivePanel-->