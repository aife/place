		<!--Debut Bloc Guides-->
		<com:TPanel id="panelGuides">
			<div class="form-field">
				<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
				<div class="content">
					<div class="title-toggle" onclick="togglePanel(this,'ListeGuides_<%=$this->idUnicite%>');">&nbsp;</div>
					<h3><%=$this->getTitle()%></h3>
					<div class="panel-toggle" style="display:none;" id="ListeGuides_<%=$this->idUnicite%>">
						<com:TRepeater ID="RepeaterGuides">
							<prop:ItemTemplate>
								<div class="line">
									<div class="content-bloc bloc-760">
										<div class="picto-link inline">
											<a target="_blank" href="index.php?page=<%= Application\Service\Atexo\Atexo_Util::getTypeUserCalledForPradoPages() %>.GuideUser&calledFrom=<%= Application\Service\Atexo\Atexo_Util::atexoHtmlEntities($_GET['calledFrom'])%>&file=<%=base64_encode($this->Data->getNomFichier())%>">
												<%#$this->Data->getLibelle()%></li>
											</a>
										</div>
									</div>				
								</div>
							</prop:ItemTemplate>
						</com:TRepeater>
					</div>
					<div class="breaker"></div>
				</div>
				<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
			</div>
		</com:TPanel>
		<!--Fin Bloc Guides-->