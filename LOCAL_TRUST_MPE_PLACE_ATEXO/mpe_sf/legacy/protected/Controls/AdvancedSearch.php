<?php

namespace Application\Controls;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Propel\Mpe\CommonCategorieINSEEPeer;
use Application\Propel\Mpe\CommonEtatConsultationQuery;
use Application\Propel\Mpe\CommonOrganisme;
use Application\Propel\Mpe\CommonOrganismePeer;
use Application\Propel\Mpe\CommonReferentielOrgDenomination;
use Application\Propel\Mpe\CommonServicePeer;
use Application\Propel\Mpe\CommonTMesRecherches;
use Application\Propel\Mpe\CommonTMesRecherchesPeer;
use Application\Service\Atexo\AlerteMetier\ConfigurationAlerte;
use Application\Service\Atexo\Atexo_Agent;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Consultation;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_EntityPurchase;
use Application\Service\Atexo\Atexo_Languages;
use Application\Service\Atexo\Atexo_LieuxExecution;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Organismes;
use Application\Service\Atexo\Atexo_Ref;
use Application\Service\Atexo\Atexo_ReferentielOrgDenomination;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Category;
use Application\Service\Atexo\Consultation\Atexo_Consultation_CriteriaVo;
use Application\Service\Atexo\Consultation\Atexo_Consultation_ProcedureType;
use Application\Service\Atexo\Consultation\Atexo_Consultation_TypeAvis;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_Recherches;
use Application\Service\Atexo\Geolocalisation\Atexo_Geolocalisation_GeolocalisationN2;
use Application\Service\Atexo\Recherche\Atexo_Recherche_CriteriaVo;
use Exception;
use Prado\Prado;
use Prado\Util\TLogger;
use Prado\Web\UI\JuiControls\TJuiAutoComplete;
use Prado\Web\UI\JuiControls\TJuiAutoCompleteEventParameter;

/**
 * Formulaire de recherche avancee.
 *
 * @author Adil El Kanabi <adil.kanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class AdvancedSearch extends MpeTPage
{
    public $_calledFrom = '';
    public $langue;
    public $_baseDce = false;
    private $codeDenomAdapte;
    private $idCrateur;
    private $typeCreateur;
    private $rechercheFavorite = null;
    protected $isProfilRmaVisible = false;

    public function getBaseDce()
    {
        return $this->_baseDce;
    }

    public function setBaseDce($value)
    {
        $this->_baseDce = $value;
    }

    /**
     * modifie la valeur de [idCreateur].
     *
     * @param int $idCreateur la valeur a mettr
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setIdCreateur($idCreateur)
    {
        $this->idCreateur = $idCreateur;
    }

    /**
     * recupere la valeur du [idCreateur].
     *
     * @return int la valeur courante [idCreateur]
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getIdCreateur()
    {
        return $this->idCreateur;
    }

    /**
     * modifie la valeur de [typeCreateur].
     *
     * @param int $typeCreateur la valeur a mettr
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function setTypeCreateur($typeCreateur)
    {
        $this->typeCreateur = $typeCreateur;
    }

    /**
     * recupere la valeur du [typeCreateur].
     *
     * @return int la valeur courante [typeCreateur]
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2014
     */
    public function getTypeCreateur()
    {
        return $this->typeCreateur;
    }

    public function onInit($param)
    {
        if ('agent' !== $this->_calledFrom && 'entreprise' !== $this->_calledFrom) {
            throw new Exception('You must precise _calledFrom parameter');
        }
    }

    public function onLoad($param)
    {
        if (!$this->Page->IsPostBack) {
            $this->inclureDescendances->checked = true;
            $this->referentielCPV->setCodeCpvObligatoire(false);
            $this->referentielCPV->onInitComposant();

            if (Atexo_Module::isEnabled('RechercheAutoCompletion')) {
                $this->typeRechercheEntiteAutocomplete->checked = true;
                $this->typeRechercheEntiteListe->Checked = false;
            }
        }
        $this->boutonClear->Text = Prado::localize('DEFINE_EFFACER_CRITERE_RECHERCHE');
        if ('agent' == $this->_calledFrom && Atexo_Module::isEnabled('RecherchesFavoritesAgent', Atexo_CurrentUser::getCurrentOrganism())) {
            $this->idCreateur = Atexo_CurrentUser::getIdAgentConnected();
            $this->typeCreateur = Atexo_Config::getParameter('TYPE_CREATEUR_AGENT');
        } elseif ('entreprise' == $this->_calledFrom && Atexo_Module::isEnabled('RecherchesFavorites')) {
            $this->idCreateur = Atexo_CurrentUser::getIdInscrit();
            $this->typeCreateur = Atexo_Config::getParameter('TYPE_CREATEUR_ENTREPRISE');
        }
        $this->langue = Atexo_CurrentUser::readFromSession('lang');
        $calledFromPrtail = false;

        $sessionLang = Atexo_CurrentUser::readFromSession('lang');
        if (Atexo_Module::isEnabled('MultiLinguismeEntreprise') && isset($sessionLang)) {
            $this->linkLieuExe1->NavigateUrl = $this->getLieuExecutionUrl();
        }

        if (Atexo_Module::isEnabled('RechercheAvanceeParDomainesActivite')) {
            $this->panelDomainesActivites->setVisible(true);
        }

        if (Atexo_Module::isEnabled('ConsultationDomainesActivitesLtReferentiel')) {
            $this->panelDomainesActivitesLtRef->setVisible(true);
            $atexoRefDomainesActivites = new Atexo_Ref();
            $atexoRefDomainesActivites->setCheminFichierConfigXML(Atexo_Config::getParameter('PATH_FILE_DOMAINES_ACTIVITES_CONFIG'));
            $this->idAtexoRefDomaineActivites->afficherReferentiel($atexoRefDomainesActivites);
        }

        if (Atexo_Module::isEnabled('RechercheAvanceeParQualification')) {
            $this->panelQualifications->setVisible(true);
        }

        if (Atexo_Module::isEnabled('RechercheAvanceeParAgrement')) {
            $this->panelAgrements->setVisible(true);
        }

        //$this->linkLieuExe1->NavigateUrl = "javascript:popUpSetSize('?page=Commun.LieuxExecution','1050px','450px','yes');";
        $this->isCoteEntreprise();
        if ($this->showLtRefPersonnalisesHorsMpe()) {
            $this->ltRefCons->afficherReferentielConsultation();
            $this->idReferentielZoneText->afficherTextConsultation();
            $this->idAtexoLtRefRadio->afficherReferentielRadio();
        }

        // Affichage code NUTS
        if (Atexo_Module::isEnabled('CodeNutLtReferentiel')) {
            $atexoCodesNutsRef = new Atexo_Ref();
            $atexoCodesNutsRef->setCheminFichierConfigXML(Atexo_Config::getParameter('PATH_FILE_NUTS_CONFIG'));
            $this->idAtexoRefNuts->afficherReferentiel($atexoCodesNutsRef);
        }
        //Fin affichage code NUTS
        if ('agent' == $this->_calledFrom) {
            if (Atexo_Module::isEnabled('LieuxExecutionCarte')) {
                //Chargement des lieux d'execution preferes au premier chargement du formulaire
                if (!$this->Page->IsPostBack) {
                    $arIds = $this->getRawFavoritedLieux();
                    $this->idsSelectedGeoN2->Value = $arIds;
                    $this->denominationGeoN2->Text = $this->getSelectedGeoN2($arIds);
                }
            }

            $this->panelModeRechercheEntite->setDisplay('None');
            $this->panelSearchAnnoncesMarches->setDisplay('None');

            $calledFromPrtail = false;
            $this->linkConsultation->setVisible(!isset($_GET['annonce']) && !($this->getBaseDce()));
            if (isset($_GET['searchAnnCons']) && isset($_GET['archive'])) {
                $this->linkConsultation->Text = Prado::localize('TEXT_ARCHIVES');
            } else {
                $this->linkConsultation->Text = Prado::localize('DEFINE_TEXT_CONSULTATIONS');
            }
            $this->linkAnnonces->setVisible(isset($_GET['annonce']));
            $this->linkDCE->setVisible(!isset($_GET['annonce']) && ($this->getBaseDce()));
            $this->panelStatutConsultation->setVisible(true);
            if (('agent' == $this->_calledFrom) && !$this->isCallFromCommission() && isset($_GET['searchAnnCons']) && Atexo_Module::isEnabled('AffichageRechercheAvanceeAgentAcSadTransversaux')) {
                $this->panelACCordsCadres->setVisible(true);
            } else {
                $this->panelACCordsCadres->setVisible(false);
            }
            $this->panelSearchPrRest->setVisible(false);
            $this->panelSearch->setVisible(false);

            if (!$this->Page->IsPostBack) {
                $this->displayStatus();
                $this->dateMiseEnLigneStart->Text = date('d/m/Y');
                $this->dateMiseEnLigneEnd->Text = Atexo_Util::dateWithparameter(+1, date('d/m/Y'), 0, 6, 0);
                if (!isset($_GET['annonce'])) {
                    $this->dateMiseEnLigneCalculeStart->Text = Atexo_Util::dateWithparameter(-1, date('d/m/Y'), 0, 6, 0);
                    $this->dateMiseEnLigneCalculeEnd->Text = date('d/m/Y');
                }
                if (isset($_GET['archive'])) {
                    $this->dateMiseEnLigneStart->Text = Atexo_Util::dateWithparameter(-1, date('d/m/Y'), 0, 0, 5);
                    $this->dateMiseEnLigneEnd->Text = date('d/m/Y');
                    $this->dateMiseEnLigneCalculeStart->Text = Atexo_Util::dateWithparameter(-1, date('d/m/Y'), 0, 2, 0);
                    $this->dateMiseEnLigneCalculeEnd->Text = date('d/m/Y');
                }
                if (isset($_GET['id'])) {//Appele depuis CAO Commission
                    $this->dateMiseEnLigneStart->Text = '';
                    $this->dateMiseEnLigneEnd->Text = '';
                    $this->dateMiseEnLigneCalculeStart->Text = '';
                    $this->dateMiseEnLigneCalculeEnd->Text = '';
                    $this->panelCAORetour->setDisplay('Dynamic');
                    $this->panelCAORetour->setVisible(true);
                }
                if (Atexo_Module::isEnabled('OrganisationCentralisee')) {
                    $this->panelOrganismes->setVisible(false);
                    $this->scriptPurchaseNames->Text = "<script>document.getElementById('divEntityPurchase').style.display = '';</script>";
                    $listEntityPurchase = Atexo_EntityPurchase::getSubServicesAndMyService(Atexo_CurrentUser::getIdServiceAgentConnected(), Atexo_CurrentUser::getCurrentOrganism());
                    $this->entityPurchaseNames->dataSource = $listEntityPurchase;
                    $this->entityPurchaseNames->DataBind();
                } else {
                    $this->panelSearchInEntreprise->setDisplay('None');
                    $this->panelOrganismes->setVisible(false);
                    $this->panelPurchaseNames->setVisible(false);
                }
                if ($this->isProfilRmaVisible) {
                    $this->displayOrganismesBlocRMA();
                    $this->displayEntityPurchaseRma();
                }
                if (isset($_GET['idRecherche']) && isset($_GET['search'])) {
                    $this->displayCriteria();
                }
            }

            if (Atexo_Module::isEnabled('AlerteMetier')) {
                $this->panelAlerte->setVisible(true);
            } else {
                $this->panelAlerte->setVisible(false);
            }
        } elseif ('entreprise' == $this->_calledFrom) {
            $this->linkConsultation->Text = Prado::localize('DEFINE_TEXT_CONSULTATIONS');
            $this->panelAlerte->setVisible(false);
            if (Atexo_Module::isEnabled('RechercheAutoCompletion')) {
                $this->panelModeRechercheEntite->setDisplay('Dynamic');
                if (Atexo_Module::isEnabled('RechercheAvanceeParTypeOrg')) {
                    $this->labelEntitePublique->Text = Prado::localize('TEXT_MINISTERE').' :';
                } else {
                    $this->labelEntitePublique->Text = '&nbsp;';
                }
                $this->panelSearchInEntreprise->setDisplay('None');
                $this->panelSearchAnnoncesMarches->setDisplay('Dynamic');
                $this->labelEntitePubliqueAutoComplition->Text = '&nbsp;';
            } else {
                $this->labelEntitePublique->Text = Prado::localize('TEXT_MINISTERE').' :';
                $this->panelModeRechercheEntite->setDisplay('None');
                $this->panelSearchAnnoncesMarches->setDisplay('None');
                $this->panelSearchInEntreprise->setDisplay('Dynamic');
            }
            if (Atexo_Module::isEnabled('AnnoncesMarches')) {
                // gestion des doublons
                $valeurs = [];
                $valeurs[0] = Prado::localize('TEXT_INDIFFERENT');
                $valeurs[1] = Prado::localize('DEFINE_NON');
                $valeurs[2] = Prado::localize('DEFINE_OUI');
                $this->doublon->dataSource = $valeurs;
                $this->doublon->DataBind();
            }
            $this->panelACCordsCadres->setVisible(false);
            $calledFromPrtail = true;
            $this->linkConsultation->setVisible(!isset($_GET['AllAnn']) && !($this->getBaseDce()));
            $this->linkAnnonces->setVisible(isset($_GET['AllAnn']));
            $this->linkDCE->setVisible(!isset($_GET['AllAnn']) && ($this->getBaseDce()));
            $this->panelStatutConsultation->setVisible(false);
            if (!$this->Page->IsPostBack && $this->getBaseDce()) {
                $dateRemiseStart = date('d/m/').(date('Y') - 2);
                $dateRemiseEnd = date('d/m/').(date('Y') + 2);
                $this->dateRemisePlisStart->Text = $dateRemiseStart; //Atexo_Util::dateWithparameter(date('d/m/Y'),0,6,0);
                $this->dateRemisePlisEnd->Text = $dateRemiseEnd;
            }
            if (isset($_GET['searchAnnCons'])) {
                if (Atexo_Module::isEnabled('AnnoncesMarches')) {
                    $this->panelSearchPrRest->setVisible(false);
                    $this->panelSearch->setVisible(false);
                } else {
                    $this->panelSearchPrRest->setVisible(true);
                    $this->panelSearch->setVisible(true);
                }
                $this->refrentielZoneText->setVisible(true);
            } else {
                $this->panelSearchPrRest->setVisible(false);
                $this->panelSearch->setVisible(false);
                $this->titreDate->setVisible(false);
                $this->panelDate->setVisible(false);
                $this->dateMiseEnLigneStart->Text = '';
                $this->dateMiseEnLigneEnd->Text = '';
                $this->dateMiseEnLigneCalculeStart->Text = '';
                $this->dateMiseEnLigneCalculeEnd->Text = '';
                $this->refrentielZoneText->setVisible(false);
            }
            if (!$this->Page->IsPostBack) {
                $this->dateMiseEnLigneEnd->Text = Atexo_Util::dateWithparameter(+1, date('d/m/Y'), 0, 6, 0);
                $this->dateMiseEnLigneStart->Text = date('d/m/Y');
                $this->dateMiseEnLigneCalculeEnd->Text = date('d/m/Y');
                $this->dateMiseEnLigneCalculeStart->Text = Atexo_Util::dateWithparameter(-1, date('d/m/Y'), 0, 6, 0);
                $this->displayOrganismes();
            }

            if (Atexo_Module::isEnabled('RechercheAvanceeParTypeOrg')) {
                $this->panelSearchByClassification->setVisible(true);
                $this->panelSearchByClassificationConsRestreinte->setVisible(true);
                if (!$this->Page->IsPostBack) {
                    $this->fillClassification();
                }
            }
        }

        //Affichage de champs pour la recherche favorite
        //Ce traitement se fait dans la fonction $this->displayCriteria(), on mis ce code ici car ces champs sont ecrases dans le onload()
        if (!$this->Page->IsPostBack) {
            if (isset($_GET['search']) && isset($_GET['idRecherche'])) {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                $this->rechercheFavorite = CommonTMesRecherchesPeer::retrieveByPK($_GET['idRecherche'], $connexion);
                $this->chargerInfosPopInRechercheFavorite();
                if ($this->Page->getViewState('CriteriaVo') instanceof Atexo_Consultation_CriteriaVo) {
                    $criteriaVo = $this->Page->getViewState('CriteriaVo');
                    if ($criteriaVo->getDateFinStart() || '' == $criteriaVo->getDateFinStart()) {
                        $this->dateMiseEnLigneStart->Text = $criteriaVo->getDateFinStart();
                    }
                    if ($criteriaVo->getDateFinEnd() || '' == $criteriaVo->getDateFinEnd()) {
                        $this->dateMiseEnLigneEnd->Text = $criteriaVo->getDateFinEnd();
                    }
                    if ($criteriaVo->getEnLigneDepuis() || '' == $criteriaVo->getEnLigneDepuis()) {
                        $this->dateMiseEnLigneCalculeStart->Text = $criteriaVo->getEnLigneDepuis();
                    }
                    if ($criteriaVo->getEnLigneJusquau() || '' == $criteriaVo->getEnLigneJusquau()) {
                        $this->dateMiseEnLigneCalculeEnd->Text = $criteriaVo->getEnLigneJusquau();
                    }
                    if (Atexo_Module::isEnabled('LieuxExecution')) {
                        if ($criteriaVo->getLieuxExecution()) {
                            self::displayGeoN2InModification($criteriaVo->getLieuxExecution());
                            $this->selectedGeoN2->Attributes->OnClick = 'Page.AdvancedSearch.displaySelectedGeoN2';
                        }
                    }
                    if ($criteriaVo->getCodesNuts()) {
                        $this->displayCodesNuts($criteriaVo->getCodesNuts());
                    }
                    if (true === $criteriaVo->getInclureDescendance()) {
                        $this->inclureDescendances->Checked = true;
                        $this->entiteSeule->Checked = false;
                    } else {
                        $this->entiteSeule->Checked = true;
                        $this->inclureDescendances->Checked = false;
                    }
                    if (Atexo_Module::isEnabled('RechercheAvanceeParQualification') && $criteriaVo->getQualification()) {
                        $this->qualification->idsQualification->Text = base64_encode($criteriaVo->getQualification());
                    }
                    if (Atexo_Module::isEnabled('RechercheAvanceeParAgrement') && $criteriaVo->getAgrements()) {
                        $this->agrements->idsSelectedAgrements->Value = $criteriaVo->getAgrements();
                    }
                    if ((Atexo_Module::isEnabled('RechercheAutoCompletion') && ($criteriaVo->getDenominationAdapte() || $criteriaVo->getOrgDenomination())) || true === $criteriaVo->getRechercheAvanceeAutoCompletion()) {
                        $this->panelSearchAnnoncesMarches->setDisplay('Dynamic');
                        $this->panelSearchInEntreprise->setDisplay('None');
                        $this->scriptJs->text = "<script>
J('#tabSearchInEntreprise').addClass('active');
J('#labelAutoComp').addClass('active');
J('#tabSearchAnnoncesMarches').removeClass('active');
J('#labelSelect').removeClass('active');
</script>";
                    } else {
                        $this->panelSearchAnnoncesMarches->setDisplay('None');
                        $this->panelSearchInEntreprise->setDisplay('Dynamic');
                        $this->scriptJs->text = "<script>
J('#tabSearchAnnoncesMarches').addClass('active');
J('#labelSelect').addClass('active');
J('#tabSearchInEntreprise').removeClass('active');
J('#labelAutoComp').removeClass('active');
</script>";
                    }
                    //Afficher les champs LT Referentiels personnalisables
                    $this->afficherLtReferentielsPersonnalisables($criteriaVo);
                }
                $this->panelLienRetourMesRecherchesFavorites->visible = true;
                if ('entreprise' == $this->_calledFrom) {
                    $this->lienRetourMesRecherchesFavorites->NavigateUrl = 'index.php?page=Entreprise.EntrepriseGestionRecherches';
                } else {
                    $this->lienRetourMesRecherchesFavorites->NavigateUrl = 'index.php?page=Agent.AgentGestionRecherches';
                }
            } else {
                $this->rechercheName->Text = '';
                $this->sauvegardeRecherche->checked = false;
                $this->sauvegardeAlerte->checked = false;
                $this->tousLesJours->checked = true;
                $this->formatHtml->checked = true;
                $this->infoCompAlerte->style = 'Display:none;';
            }
        }
        if (!$this->Page->IsPostBack) {
            if (isset($_GET['searchAnnCons']) || isset($_GET['AllCons'])) {
                $searchAnnCons = false;
                $this->annonceType->SelectedValue = Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION');
                $this->annonceType->SetEnabled(false);

                //Helios : restreindre la liste des consultations qui sont affichees au statut Decision
                if ('agenthelios' == strtolower(Atexo_CurrentUser::getRole())) {
                    $this->statut->SelectedValue = Atexo_Config::getParameter('STATUS_DECISION');
                    $this->statut->SetEnabled(false);
                }
            } else {
                $searchAnnCons = true;
            }
            $this->displayTypeAvis($searchAnnCons);
            $this->displayProcedureType($calledFromPrtail);
            $this->displayRegleNonObservee();
            $this->displayCategory($calledFromPrtail);
            if ((isset($_GET['searchAnnCons']) || isset($_GET['BaseDce'])) && Atexo_Module::isEnabled('ConsultationClause')) {
                $this->panelClauseConsultation->setVisible(true);
                self::displayClauses();
            } else {
                $this->panelClauseConsultation->setVisible(false);
            }
            $this->gererAffichageMarchesPublicsSimplifies();
            $this->gererAffichageBourseCotraitance();
        }

        $this->customizeForm();
    }

    public function setCalledFrom($value)
    {
        $this->_calledFrom = $value;
    }

    /**
     * Fonction qui recupere les choix effectues dans la partie 'Recherche multi-criteres'
     * pour les mettre dans un objet CriteriaVo puis affiche le resultat
     * dans le tableau de bord.
     */
    public function onSearchClick()
    {
        $criteriaVo = self::getCriteriaForSearch();
        self::startSearch($criteriaVo);
    }

    /*
     * TODO
     */
    public function getCriteriaForSearch()
    {
        Atexo_CurrentUser::deleteFromSession('organisme');
        Atexo_CurrentUser::deleteFromSession('reference');
        Atexo_CurrentUser::deleteFromSession('codeAcces');
        Atexo_CurrentUser::deleteFromSession('criteriaCons');
        $criteriaVo = new Atexo_Consultation_CriteriaVo();
        if ('entreprise' == $this->_calledFrom) {
            if (!$this->getBaseDce()) {
                $criteriaVo->setEtatConsultation(Atexo_Config::getParameter('STATUS_CONSULTATION'));
            }
            $criteriaVo->setcalledFromPortail(true);
            //$criteriaVo->setSecondConditionOnEtatConsultation(Atexo_Config::getParameter('STATUS_AVANT_ARCHIVE'));

            $criteriaVo->setConsultationExterneFilter(true);
            $criteriaVo->setConsultationExterne($this->inclureConsultationExterieur->checked);

            $typeProcedure = 1;
            $criteriaVo->setTypeAcces($typeProcedure);

            if (Atexo_Module::isEnabled('RechercheAutoCompletion') && $this->typeRechercheEntiteAutocomplete->checked) {
                $criteriaVo->setRechercheAvanceeAutoCompletion(true);
                if($this->Page->getViewState('denominationAdapte') && $this->orgNameAM->Text != '' && strlen($this->orgNameAM->Text)>=3) {
                    $org = Atexo_Organismes::retrieveOrganismeByAcronyme($this->Page->getViewState('denominationAdapte'));
                    if ($org instanceof CommonOrganisme){
                        $criteriaVo->setAcronymeOrganisme($org->getAcronyme());
                    }
                    $this->Page->setViewState('denominationAdapte', '');
                    $criteriaVo->setOrgDenomination($this->orgNameAM->Text);
                } elseif ($this->orgNameAM->Text) {
                    $this->Page->setViewState('denominationAdapte', '');
                    $criteriaVo->setOrgDenomination($this->orgNameAM->Text);
                }
            } else {
                $criteriaVo->setRechercheAvanceeAutoCompletion(false);
                if ('0' != $this->organismesNames->getSelectedValue()) {
                    $criteriaVo->setAcronymeOrganisme(trim($this->organismesNames->getSelectedValue()));
                    $criteriaVo->setIdService($this->entityPurchaseNames->SelectedValue);
                    //Lorsque l'on a inclus la descendance du service
                    if (true == $this->inclureDescendances->Checked) {
                        $listOfServicesAllowed = Atexo_EntityPurchase::retrieveAllChildrenServices($criteriaVo->getIdService(), $criteriaVo->getAcronymeOrganisme());
                        $criteriaVo->setIdsService($listOfServicesAllowed);
                    } else {
                        $criteriaVo->setInclureDescendance(false);
                    }
                }
                if (Atexo_Module::isEnabled('RechercheAvanceeParTypeOrg')) {
                    $criteriaVo->setTypeOrganisme($this->classification->getSelectedValue());
                }
            }

            if (isset($_GET['doublon']) && Atexo_Module::isEnabled('AnnoncesMarches')) {
                $afficherDoublon = $this->doublon->getSelectedValue();
                if ('1' == $afficherDoublon) {
                    $criteriaVo->setAfficherDoublon('0');
                } elseif ('2' == $afficherDoublon) {
                    $criteriaVo->setAfficherDoublon('1');
                }
            }

            if (isset($_GET['searchAnnCons'])) {
                //pour lister les consultations annulées
                if (isset($_GET['consAnnulee']) && (1 == $_GET['consAnnulee'])) {
                    $criteriaVo->setEtatConsultation(Atexo_Config::getParameter('STATUS_DECISION'));
                    $criteriaVo->setConsultationAnnulee(true);
                }
            }
            if (isset($_GET['type']) && 'restreinte' == $_GET['type']) {
                $this->panelDefaultRestreinte->setVisible(true);
            }
        } else {
            $criteriaVo->setIdService(Atexo_CurrentUser::getCurrentServiceId());
            if (Atexo_Module::isEnabled('OrganisationCentralisee')) {
                $service = $this->entityPurchaseNames->getSelectedValue();
                $criteriaVo->setIdsService('('.$service.')');
                if (true == $this->inclureDescendances->Checked) {
                    $listOfServicesAllowed = Atexo_EntityPurchase::retrieveAllChildrenServices($service, Atexo_CurrentUser::getCurrentOrganism());
                } else {
                    $criteriaVo->setInclureDescendance(false);
                }
                if ($listOfServicesAllowed) {
                    $criteriaVo->setIdsService($listOfServicesAllowed);
                }
            }
            $criteriaVo->setConnectedAgentId(Atexo_CurrentUser::getId());
            $criteriaVo->setcalledFromPortail(false);

            if (isset($_GET['archive'])) {
                $criteriaVo->setFromArchive(true);
            } else {
                $criteriaVo->setSecondConditionOnEtatConsultation(Atexo_Config::getParameter('STATUS_AVANT_ARCHIVE'));
            }
            //$criteriaVo->setTypeProcedure(1);
            $criteriaVo->setAcronymeOrganisme(Atexo_CurrentUser::getCurrentOrganism());
            if ($this->isProfilRmaVisible && $this->visionRma->Checked) {
                if ('0' != $this->statutRma->getSelectedValue()) {
                    $criteriaVo->setEtatConsultation($this->statutRma->getSelectedValue());
                }// le cas else est deja traite en haut : $criteriaVo->setEtatConsultation(Atexo_Config::getParameter('STATUS_AVANT_ARCHIVE'));
            } else {
                if ('0' != $this->statut->getSelectedValue()) {
                    $this->Page->setViewState('changeStatutWhenTabChanged', false);
                    if ($this->statut->getSelectedValue() == Atexo_Config::getParameter('STATUS_ELABORATION')) {
                        $criteriaVo->setEtatConsultation(Atexo_Config::getParameter('STATUS_ELABORATION'));
                    } elseif ($this->statut->getSelectedValue() == Atexo_Config::getParameter('STATUS_PREPARATION')) {
                        $criteriaVo->setEtatConsultation(Atexo_Config::getParameter('STATUS_PREPARATION'));
                    } elseif ($this->statut->getSelectedValue() == Atexo_Config::getParameter('STATUS_CONSULTATION')) {
                        $criteriaVo->setEtatConsultation(Atexo_Config::getParameter('STATUS_CONSULTATION'));
                    } elseif ($this->statut->getSelectedValue() == Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE')) {
                        $criteriaVo->setEtatConsultation(Atexo_Config::getParameter('STATUS_OUVERTURE_ANALYSE'));
                    } elseif ($this->statut->getSelectedValue() == Atexo_Config::getParameter('STATUS_DECISION')) {
                        if (isset($_GET['archive'])) {
                            $criteriaVo->setEtatConsultation(Atexo_Config::getParameter('STATUS_JUSTE_DECISION'));
                        } else {
                            $criteriaVo->setEtatConsultation(Atexo_Config::getParameter('STATUS_DECISION'));
                        }
                    } elseif ($this->statut->getSelectedValue() == Atexo_Config::getParameter('STATUS_A_ARCHIVER')) {
                        $criteriaVo->setEtatConsultation(Atexo_Config::getParameter('STATUS_A_ARCHIVER'));
                    } else {
                        $criteriaVo->setEtatConsultation(Atexo_Config::getParameter('STATUS_ARCHIVE_REALISEE'));
                    }
                } else {
                    if (isset($_GET['archive'])) {
                        $criteriaVo->setEtatConsultation(Atexo_Config::getParameter('STATUS_APRES_DECISION'));
                    }
                    $this->Page->setViewState('changeStatutWhenTabChanged', true);
                }
            }

            if (Atexo_Module::isEnabled('AlerteMetier')) {
                $cloturee = 1;
                $regleNonObservee = $this->regleNonObservee->getSelectedValue();
                if ($this->affichageAlerteNonCloturee->Checked) {
                    $cloturee = 2;
                }
                if (2 == $cloturee || 0 != $this->regleNonObservee->getSelectedValue()) {
                    $criteriaVo->setIdAlerteConsultation($regleNonObservee);
                    $criteriaVo->setAlerteConsultationCloturee($cloturee);
                }
            }
            if ($this->isProfilRmaVisible) {
                if ($this->visionRma->Checked) {
                    if ($this->organismesRma->SelectedValue) {
                        $criteriaVo->setAcronymeOrganisme($this->organismesRma->getSelectedValue());
                    }
                    if (' ' !== $this->entiteAchat->getSelectedValue()) {
                        $criteriaVo->setIdServiceRma($this->entiteAchat->getSelectedValue());
                    } else {
                        $criteriaVo->setIdServiceRma('');
                    }
                    //$criteriaVo->setEtatConsultation(Atexo_Config::getParameter('STATUS_AVANT_ARCHIVE'));
                    $criteriaVo->setVisionRma(true);
                } else {
                    $criteriaVo->setVisionRma(false);
                }
            }
        }

        if (trim($this->reference->Text)) {
            $criteriaVo->setReferenceConsultation(trim($this->reference->Text));
        }

        if ('0' != $this->annonceType->getSelectedValue()) {
            $criteriaVo->setIdTypeAvis($this->annonceType->getSelectedValue());
        } else {
            if ((isset($_GET['AllAnn']) || isset($_GET['AvisAttribution']) || isset($_GET['AvisInformation']) || isset($_GET['annonce']))) {
                $criteriaVo->setNotIdTypeAvis(Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION'));
            } else {
                $criteriaVo->setIdTypeAvis(Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION'));
            }
        }
        if (isset($_GET['consAnnulee']) && (1 == $_GET['consAnnulee'])) {
            $criteriaVo->setEtatConsultation(Atexo_Config::getParameter('STATUS_DECISION'));
            $criteriaVo->setConsultationAnnulee(true);
        }

        if ('0' != $this->procedureType->getSelectedValue()) {
            $criteriaVo->setIdTypeProcedure($this->procedureType->getSelectedValue());
        }
        if ('0' != $this->categorie->getSelectedValue()) {
            $criteriaVo->setCategorieConsultation($this->categorie->getSelectedValue());
        }

        if (Atexo_CurrentUser::isAgent()) {
            if (trim($this->keywordSearchBottom->Text)) {
                $criteriaVo->setKeyWordAdvancedSearch(trim($this->keywordSearchBottom->Text));
                if ($this->exactBottom->Checked) {
                    $criteriaVo->setSearchModeExact(true);
                } else {
                    $criteriaVo->setSearchModeExact(false);
                }
            }
        } else {
            if (trim($this->keywordSearch->Text)) {
                $criteriaVo->setKeyWordAdvancedSearch(trim($this->keywordSearch->Text));
                if ($this->exact->Checked) {
                    $criteriaVo->setSearchModeExact(true);
                } else {
                    $criteriaVo->setSearchModeExact(false);
                }
            }
        }
        if (trim($this->dateMiseEnLigneStart->Text)) {
            $criteriaVo->setDateFinStart(trim($this->dateMiseEnLigneStart->Text));
        } else {
            $criteriaVo->setDateFinStart('');
        }
        if (trim($this->dateMiseEnLigneEnd->Text)) {
            $criteriaVo->setDateFinEnd(trim($this->dateMiseEnLigneEnd->Text));
        } else {
            $criteriaVo->setDateFinEnd('');
        }
        //Debut recherche par date mise en ligne calculee
        if (trim($this->dateMiseEnLigneCalculeStart->Text)) {
            $criteriaVo->setEnLigneDepuis(trim($this->dateMiseEnLigneCalculeStart->Text));
        } else {
            $criteriaVo->setEnLigneDepuis('');
        }
        if (trim($this->dateMiseEnLigneCalculeEnd->Text)) {
            $criteriaVo->setEnLigneJusquau(trim($this->dateMiseEnLigneCalculeEnd->Text));
        } else {
            $criteriaVo->setEnLigneJusquau('');
        }
        //Fin recherche par date mise en ligne calculee
        if (Atexo_Module::isEnabled('LieuxExecution')) {
            if (trim($this->idsSelectedGeoN2->Value)) {
                $criteriaVo->setLieuxexecution(trim($this->idsSelectedGeoN2->Value));
            }
        }

        //<!-- Module referentiel CPV
        if ('' != $this->referentielCPV->getCpvPrincipale()) {
            $criteriaVo->setIdCodeCpv2($this->referentielCPV->getCpvPrincipale().$this->referentielCPV->getCpvSecondaires());
        }
        //-->

        //Codes nuts
        if (Atexo_Module::isEnabled('CodeNutLtReferentiel')) {
            if ('' != $this->idAtexoRefNuts->codesRefSec->Value) {
                $criteriaVo->setCodesNuts($this->idAtexoRefNuts->codesRefSec->Value);
            }
        }
        //Fin codes nuts
        if ('agenthelios' == strtolower(Atexo_CurrentUser::getRole())) {
            $criteriaVo->setCalledFromHelios(true);
        }

        if (Atexo_Module::isEnabled('RechercheAvanceeParDomainesActivite')) {
            $criteriaVo->setDomaineActivite($this->domaineActivite->idsDomaines->Text);
        }

        if (Atexo_Module::isEnabled('ConsultationDomainesActivitesLtReferentiel')) {
            $criteriaVo->setDomaineActivite($this->idAtexoRefDomaineActivites->codesRefSec->Value);
        }

        if (Atexo_Module::isEnabled('RechercheAvanceeParQualification')) {
            $criteriaVo->setQualification(base64_decode($this->qualification->idsQualification->Text));
        }
        if (Atexo_Module::isEnabled('RechercheAvanceeParAgrement')) {
            $criteriaVo->setAgrements($this->agrements->idsSelectedAgrements->Value);
        }

        $refTextValue = $this->idReferentielZoneText->getValueReferentielTextVo();
        $referentielValue = $this->ltRefCons->getValueReferentielVo();
        $refRadioValue = $this->idAtexoLtRefRadio->getValueReferentielRadioVo();

        $referentielVo = array_merge($refTextValue, $referentielValue, $refRadioValue);
        $criteriaVo->setReferentielVo($referentielVo);

        //Recherche sur les consultations de mon panier
        if ('entreprise' == $this->_calledFrom && Atexo_Module::isEnabled('PanierEntreprise') && isset($_GET['searchAnnCons']) && isset($_GET['panierEntreprise'])) {
            $criteriaVo->setcalledFromPortail(true);
            $criteriaVo->setForPanierEntreprise(true);
            $criteriaVo->setIdInscrit(Atexo_CurrentUser::getIdInscrit());
            $criteriaVo->setIdEntreprise(Atexo_CurrentUser::getIdEntreprise());
        }
        if ($this->getBaseDce()) {
            if (trim($this->dateRemisePlisStart->Text)) {
                $criteriaVo->setDateFinStart(trim($this->dateRemisePlisStart->Text));
            }
            if (trim($this->dateRemisePlisEnd->Text)) {
                $criteriaVo->setDateFinEnd(trim($this->dateRemisePlisEnd->Text));
            }
            $criteriaVo->setWithDce(true);
            $criteriaVo->setcalledFromPortail(true);
            $criteriaVo->setConsultaionPublier(true);
        }
        $criteriaVo->setTypeAcces(Atexo_Config::getParameter('TYPE_PROCEDURE_PUBLICITE'));
        if ((isset($_GET['searchAnnCons']) || isset($_GET['BaseDce'])) && Atexo_Module::isEnabled('ConsultationClause')) {
            if ('0' != $this->clauseSociales->getSelectedValue()) {
                $criteriaVo->setClauseSociale($this->clauseSociales->getSelectedValue());
            }

            if ('0' != $this->ateliersProteges->getSelectedValue()) {
                $criteriaVo->setAtelierProtege($this->ateliersProteges->getSelectedValue());
            }
            if ('0' != $this->ess->getSelectedValue()) {
                $criteriaVo->setEss($this->ess->getSelectedValue());
            }
            if ('0' != $this->siae->getSelectedValue()) {
                $criteriaVo->setSiae($this->siae->getSelectedValue());
            }

            if ('0' != $this->clauseSocialesInsertionActiviteEconomique->getSelectedValue()) {
                $criteriaVo->setSocialeInsertionActiviterEconomique($this->clauseSocialesInsertionActiviteEconomique->getSelectedValue());
            }

            if ('0' != $this->clauseSocialesCommerceEquitable->getSelectedValue()) {
                $criteriaVo->setSocialeCommerceEquitable($this->clauseSocialesCommerceEquitable->getSelectedValue());
            }

            if ('0' != $this->clauseEnvironnementale->getSelectedValue()) {
                $criteriaVo->setClauseEnv($this->clauseEnvironnementale->getSelectedValue());
            }
        }

        if (isset($_GET['searchAnnCons'])
                && (
                    ('agent' === $this->_calledFrom && Atexo_Module::isEnabled('MarchePublicSimplifie', Atexo_CurrentUser::getCurrentOrganism()))
                            ||
                        ('entreprise' === $this->_calledFrom && Atexo_Module::isEnabled('MarchePublicSimplifieEntreprise'))
                )
        ) {
            $criteriaVo->setMps($this->marchesPublicsSimplifies->getSelectedValue());
        } else {
            $criteriaVo->setMps('0');
        }
        if ('0' != $this->organismesNames->getSelectedValue()) {
            if (true == $this->inclureDescendances->Checked) {//Lorsque l'on a inclus la descendance du service
                $criteriaVo->setInclureDescendance(true);
            } else {
                $criteriaVo->setInclureDescendance(false);
            }
        }
        if (isset($_GET['searchAnnCons'])
                && 'entreprise' === $this->_calledFrom
                && Atexo_Module::isEnabled('BourseCotraitance')
        ) {
            $criteriaVo->setIdEntreprise(Atexo_CurrentUser::getIdEntreprise());
            $criteriaVo->setBourseCotraitance($this->bourseCotraitance->getSelectedValue());
        } else {
            $criteriaVo->setBourseCotraitance('0');
        }
        $this->Page->setViewState('CriteriaVo', $criteriaVo);
        Atexo_CurrentUser::writeToSession('criteriaCons', $criteriaVo);

        return $criteriaVo;
    }

    /**
     * afficher les denominations des lieux de'xecutions.
     */
    public function displaySelectedGeoN2($sender, $param)
    {
        $ids = $this->idsSelectedGeoN2->Value;
        $this->denominationGeoN2->Text = $this->getSelectedGeoN2($ids);
    }

    /**
     * recuperer les denominations des lieux de'xecutions.
     */
    public function getSelectedGeoN2($ids)
    {
        $text = '';
        $arIds = explode(',', $ids);
        $list = Atexo_Geolocalisation_GeolocalisationN2::retrieveGeolocalisationN2byIds($arIds);
        $getDenomination1 = 'getDenomination1'.Atexo_Languages::getLanguageAbbreviation($this->langue);

        if ($list) {
            $lieuxExecutionsType1 = '';
            foreach ($list as $oneGeoN2) {
                if (0 == strcmp($this->langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE'))
                || !$oneGeoN2->$getDenomination1()) {
                    $lieuxExecutionsType1 .= $oneGeoN2->getDenomination1().', ';
                } else {
                    $lieuxExecutionsType1 .= $oneGeoN2->$getDenomination1().', ';
                }
            }
            if ($lieuxExecutionsType1) {
                $text = substr($lieuxExecutionsType1, 0, -2);
            }
        }

        return $text;
    }

    /**
     * afficher les denominations des lieux de'xecutions.
     */
    public function refreshDisplaySelectedGeoN2($sender, $param)
    {
        $ids = $this->idsSelectedGeoN2->Value;
        $this->denominationGeoN2->Text = $this->getSelectedGeoN2($ids);

        $this->lieuExecution->render($param->getNewWriter());
        // $this->panelCode_cpv->render($param->getNewWriter());
        $this->panelButton->render($param->getNewWriter());

        $this->getLieuExecutionUrl();
    }

    /**
     * affichage de toutes les ministeres.
     */
    public function displayOrganismes()
    {
        if (isset($_COOKIE['selectedorg'])) {
            $ArrayOrganismes = [];
            $getDenominationOrg = 'getDenominationOrg'.Atexo_Languages::getLanguageAbbreviation($this->langue);
            $ArrayOrganismesO = Atexo_Organismes::retrieveOrganismeByAcronyme($_COOKIE['selectedorg']);
            if (0 == strcmp($this->langue, Atexo_Config::getParameter('LANGUE_PAR_DEFAUT_ENTREPRISE'))
                || !$ArrayOrganismesO->$getDenominationOrg()) {
                $ArrayOrganismes[$ArrayOrganismesO->getAcronyme()] = $ArrayOrganismesO->getDenominationOrg();
            } else {
                $ArrayOrganismes[$ArrayOrganismesO->getAcronyme()] = $ArrayOrganismesO->$getDenominationOrg();
            }
            //$this->organismesNames->SetEnabled(false);
            //$this->scriptPurchaseNames->Text = "<script>document.getElementById('divEntityPurchase').style.display = '';</script>";
            $ArrayOrganismesRestreint = $ArrayOrganismes;
            $listEntityPurchase = Atexo_EntityPurchase::retrieveEntityPurchaseByOrganisme($ArrayOrganismesO->getAcronyme(), null, $this->langue);
            $this->entityPurchaseNames->dataSource = $listEntityPurchase;
            $this->entityPurchaseNames->DataBind();
        } else {
            $ArrayOrganismes = Atexo_Organismes::retrieveOrganismes(false, $this->langue, true, null, false, true);
            $ArrayOrganismesRestreint = $ArrayOrganismes;
            $arrayElement0 = [0 => '--- '.Prado::localize('TEXT__TOUS_MINISTERES').' ---'];
            $ArrayOrganismes = $arrayElement0 + $ArrayOrganismes;
        }

        //Recherche multi-criteres
        $this->organismesNames->dataSource = $ArrayOrganismes;
        $this->organismesNames->DataBind();
        //Recherche d'une procedure restreinte
        $arrayElement0 = [0 => '--- '.Prado::localize('SELECTION_1EP').' ---'];
        $ArrayOrganismesRestreint = $arrayElement0 + $ArrayOrganismesRestreint;
        $this->orgNamesRestreinteSearch->dataSource = $ArrayOrganismesRestreint;
        $this->orgNamesRestreinteSearch->DataBind();
    }

    /**
     * affichage de toutes les ministeres.
     */
    public function displayOrganismesBlocRMA()
    {
        $acronyme = Atexo_CurrentUser::getCurrentOrganism();
        $organisme = Atexo_Organismes::retrieveOrganismeByAcronyme($acronyme);

        $arrayAcronymes = CommonOrganismePeer::getListDenominationOrganismeByIdAgent(Atexo_CurrentUser::getId(), $acronyme);

        if ($organisme instanceof CommonOrganisme) {
            array_unshift($arrayAcronymes, $organisme);
        }

        $this->organismesRma->dataSource = $arrayAcronymes;
        $this->organismesRma->DataBind();

        $this->organismesRma->SelectedValue = $acronyme;
        if (count($arrayAcronymes) <= 1) {
            $this->organismesRma->Enabled = false;
        }
    }

    /**
     * @param $sender
     * @param $param
     */
    protected function displayEntityPurchaseRma($sender = null, $param = null)
    {
        // script pour afficher liste des services
        $organisme = $this->organismesRma->SelectedValue;

        if ('0' != $organisme) {
            $selectionnez = [' ' => '--- '.Prado::localize('DEFINE_TOUTES_ANTITE_ACHAT').' ---'];
            $listEntityPurchase = $this->getlistEntityPurchase($organisme, $this->langue);
            $listEntityPurchase = $selectionnez + $listEntityPurchase;
        } else {
            $listEntityPurchase[' '] = '--- '.Prado::localize('DEFINE_TOUTES_ANTITE_ACHAT').' ---';
        }
        $this->entiteAchat->dataSource = $listEntityPurchase;
        $this->entiteAchat->DataBind();
    }

    /**
     * affichage de toutes les entites d'achats.
     */
    public function displayEntityPurchase($sender, $param)
    {
        // script pour afficher liste des services
        $organisme = $this->organismesNames->SelectedValue;
        $script = '';
        if ('0' != $this->organismesNames->SelectedValue) {
            $resizeIframe = '';
            if ($this->getBaseDce()) {
                $resizeIframe = "parent.resizeFrameGeneral('iframeDce');";
            }
            $script .= " document.getElementById('divEntityPurchase').style.display = '';".$resizeIframe;
            $listEntityPurchase = $this->getlistEntityPurchase($organisme, $this->langue);
            //$arrayEntityPurchase = Atexo_Util::arrayUnshift($listEntityPurchase,'--- '.Prado::localize('DEFINE_TOUTES_ANTITE_ACHAT').' ---');
            $this->entityPurchaseNames->dataSource = $listEntityPurchase;
            $this->entityPurchaseNames->DataBind();
        } else {
            $script .= " document.getElementById('divEntityPurchase').style.display = 'none';";
        }
        $script .= " J('#".$this->panelSearchAnnoncesMarches->getClientId()."').hide();J('#".$this->panelSearchInEntreprise->getClientId()."').show();";
        $this->scriptPurchaseNames->Text = '<script> '.$script.'</script>';
        $this->panelSearchInEntreprise->render($param->NewWriter);
    }

    /**
     * charge les entites d'achat dans un tableau.
     *
     * @param string $organisme
     *
     * @return array $listEntityPurchase
     */
    public function getlistEntityPurchase($organisme, $langue)
    {
        $listEntityPurchaseArray = [];
        if (Atexo_Module::isEnabled('GestionMandataire', $organisme) && 'entreprise' == $this->_calledFrom) {
            $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB'));
            $c = new Criteria();
            $c->add(CommonServicePeer::ORGANISME, $organisme, Criteria::EQUAL);
            $c->add(CommonServicePeer::AFFICHAGE_SERVICE, 1);
            $c->addAscendingOrderByColumn(CommonServicePeer::CHEMIN_COMPLET);
            $visiblesService = CommonServicePeer::doSelect($c, $connexionCom);
            $arrayOrganisme = [0 => Atexo_EntityPurchase::getPathEntityById(0, $organisme)];
            if ($visiblesService && is_array($visiblesService)) {
                foreach ($visiblesService as $oneVisiblesService) {
                    $listEntityPurchaseArray[$oneVisiblesService->getId()] = $oneVisiblesService->getPathServiceTraduit();
                }
            }
            $listEntityPurchaseArray = $arrayOrganisme + $listEntityPurchaseArray;
        } else {
            $listEntityPurchaseArray = Atexo_EntityPurchase::retrieveEntityPurchaseByOrganisme($organisme, true, $langue);
        }

        return $listEntityPurchaseArray;
    }

    /**
     * affichage de toutes les entites d'achats.
     */
    public function displayEntityPurchaseModifySearch()
    {
        // script pour afficher liste des services
        $this->scriptPurchaseNames->Text = "<script>document.getElementById('divEntityPurchase').style.display = '';</script>";
        $listEntityPurchase = Atexo_EntityPurchase::retrieveEntityPurchaseByOrganisme($this->organismesNames->SelectedValue);
        //$arrayEntityPurchase = Atexo_Util::arrayUnshift($listEntityPurchase,'--- '.Prado::localize('DEFINE_TOUTES_ANTITE_ACHAT').' ---');
        $this->entityPurchaseNames->dataSource = $listEntityPurchase;
        $this->entityPurchaseNames->DataBind();
    }

    public function displayTypeAvis($searchAnnCons)
    {
        $typeAvis = new Atexo_Consultation_TypeAvis();
        $listTypeAvis = $typeAvis->retrieveAvisTypes(false, $this->langue);
        $arrayTypeAvis = Atexo_Util::arrayUnshift($listTypeAvis, '--- '.Prado::localize('DEFINE_TOUS_ANNOCES').' ---');
        if ($searchAnnCons) {
            unset($arrayTypeAvis[Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION')]);
        }
        $this->annonceType->DataSource = $arrayTypeAvis;
        $this->annonceType->DataBind();
    }

    public function displayProcedureType($calledFromPrtail)
    {
        $typeProc = new Atexo_Consultation_ProcedureType();
        $listTypeProc = $typeProc->retrieveProcedureType($calledFromPrtail, false, null, $this->langue);
        $arrayTypeProc = Atexo_Util::arrayUnshift($listTypeProc, '--- '.Prado::localize('TEXT_RECHERCHE_AVANCEE_TOUS_TYPES_PROCEDURES').' ---');
        $this->procedureType->DataSource = $arrayTypeProc;
        $this->procedureType->DataBind();
    }

    public function displayRegleNonObservee()
    {
        $alertes = new ConfigurationAlerte();
        $listAlerte = $alertes->retrieveActiveAlerteLibelle(Atexo_Config::getParameter('TYPE_OBJET_ALERTE_CONSULTATION'));
        $arrayAlerte = Atexo_Util::arrayUnshift($listAlerte, '--- '.Prado::localize('TEXT_INDIFFERENT').' ---');
        $this->regleNonObservee->DataSource = $arrayAlerte;
        $this->regleNonObservee->DataBind();
    }

    public function displayCategory($calledFromPrtail)
    {
        $categorie = new Atexo_Consultation_Category();
        $CategorieObj = $categorie->retrieveCategories($calledFromPrtail, true, $this->langue);
        $listCategorie = $categorie->retrieveDataCategories($CategorieObj, $this->langue);
        $arrayCategorie = Atexo_Util::arrayUnshift($listCategorie, '--- '.Prado::localize('TEXT_RECHERCHE_AVANCEE_TOUTES_LES_CATEGORIES').' ---');
        $this->categorie->DataSource = $arrayCategorie;
        $this->categorie->DataBind();
    }

    public function displayStatus()
    {
        $etatConsultationQuery = new CommonEtatConsultationQuery();

        if (isset($_GET['archive'])) {
            $arrayStatus = $etatConsultationQuery->findEtatsApresDecision();
        } else {
            $arrayStatus = $etatConsultationQuery->findEtatsAvantDecision();
        }
        $arrayStatus = Atexo_Util::arrayUnshift($arrayStatus, '--- '.Prado::localize('DEFINE_TOUS_STATUS').' ---');
        $this->statut->DataSource = $arrayStatus;
        $this->statut->DataBind();

        $arrayStatus = $etatConsultationQuery->findEtatsForVisionRMA();
        $arrayStatus = Atexo_Util::arrayUnshift($arrayStatus, '--- '.Prado::localize('DEFINE_TOUS_STATUS').' ---');
        $this->statutRma->DataSource = $arrayStatus;
        $this->statutRma->DataBind();
    }

    /**
     * Permet d'afficher et pre remplir les informations du tableau de recherche avancee
     * Les informations a afficher sont recuperees a partir de l'objet $criteriaVo.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2014
     */
    public function displayCriteria()
    {
        $this->Page->rechercheAvancee->setVisible(true);
        if ('entreprise' == $this->_calledFrom) {
            $this->Page->panelResult->setVisible(false);
        } else {
            if (isset($_GET['archive'])) {
                $this->Page->ArchiveTableauBord->setVisible(false);
            } else {
                $this->Page->TableauDeBord->setVisible(false);
            }
        }
        $criteriaVo = $this->Page->getViewState('CriteriaVo');

        // Gestion des blocs RMA
        $visionRmaCoche = $criteriaVo->getVisionRma() && $this->isProfilRmaVisible;
        if ($visionRmaCoche) {
            $this->visionRma->Checked = true;
            $this->layerVisionRMA->Style = 'display:block';
            $this->statutRma->Style = 'display:block';
            $this->statut->Style = 'display:none';

            $this->organismesRma->SelectedValue = $criteriaVo->getAcronymeOrganisme();
            $this->displayEntityPurchaseRma();
            $this->entiteAchat->SelectedValue = $criteriaVo->getIdServiceRma();

            $this->statutRma->SelectedValue = $criteriaVo->getEtatConsultation();
            $this->statut->SelectedValue = '0';
        } else {
            $this->visionAcheteur->Checked = true;
            $this->layerVisionRMA->Style = 'display:none';
            $this->statut->Style = 'display:block';
            $this->statut->SelectedValue = $criteriaVo->getEtatConsultation();

            $this->statutRma->Style = 'display:none';
            $this->statutRma->SelectedValue = '0';
        }

        if ((string) $criteriaVo->getTypeAcces() == Atexo_Config::getParameter('TYPE_PROCEDURE_PUBLICITE')) {
            if ($criteriaVo->getAcronymeOrganisme()) {
                $this->organismesNames->SelectedValue = $criteriaVo->getAcronymeOrganisme();
            } else {
                $this->organismesNames->SelectedValue = 0;
            }
            if ($criteriaVo->getIdService()) {
                $this->displayEntityPurchaseModifySearch();
                $this->entityPurchaseNames->SelectedValue = $criteriaVo->getIdService();
            } else {
                $this->entityPurchaseNames->SelectedValue = 0;
            }
            if ($criteriaVo->getReferenceConsultation()) {
                $this->reference->Text = $criteriaVo->getReferenceConsultation();
            } else {
                $this->reference->Text = '';
            }
            if ($criteriaVo->getEtatConsultation()) {
                $this->statut->SelectedValue = $criteriaVo->getEtatConsultation();
            } else {
                $this->statut->SelectedValue = 0;
            }
        } else {
            if ($criteriaVo->getAcronymeOrganisme()) {
                $this->orgNamesRestreinteSearch->SelectedValue = $criteriaVo->getAcronymeOrganisme();
            } else {
                $this->orgNamesRestreinteSearch->SelectedValue = 0;
            }
            if ($criteriaVo->getReferenceConsultationRestreinte()) {
                $this->refRestreinteSearch->Text = $criteriaVo->getReferenceConsultationRestreinte();
            } else {
                $this->refRestreinteSearch->Text = '';
            }
            if ($criteriaVo->getCodeAccesProcedureRestreinte()) {
                $this->accesRestreinteSearch->Text = $criteriaVo->getCodeAccesProcedureRestreinte();
            } else {
                $this->accesRestreinteSearch->Text = '';
            }
        }
        if ($criteriaVo->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION') && !$this->getBaseDce()) {//recherche Base DCE ne contient pa le critere type Annonce
            $this->annonceType->SelectedValue = Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION');
        } else {
            $this->annonceType->SelectedValue = 0;
        }
        if ($criteriaVo->getIdTypeProcedure()) {
            $this->procedureType->SelectedValue = $criteriaVo->getIdTypeProcedure();
        } else {
            $this->procedureType->SelectedValue = 0;
        }
        if ($criteriaVo->getCategorieConsultation()) {
            $this->categorie->SelectedValue = $criteriaVo->getCategorieConsultation();
        } else {
            $this->categorie->SelectedValue = 0;
        }
        if (Atexo_Module::isEnabled('LieuxExecution')) {
            if ($criteriaVo->getLieuxExecution()) {
                self::displayGeoN2InModification($criteriaVo->getLieuxExecution());
                $this->selectedGeoN2->Attributes->OnClick = 'Page.AdvancedSearch.displaySelectedGeoN2';
            } else {
                $this->denominationGeoN2->Text = '';
                $this->numSelectedGeoN2->Value = 0;
                $this->linkLieuExe1->NavigateUrl = $this->getLieuExecutionUrl();
            }
        }
        if ($this->getBaseDce()) {
            if ($criteriaVo->getDateFinStart()) {
                $this->dateRemisePlisStart->Text = $criteriaVo->getDateFinStart();
            } else {
                $this->dateRemisePlisStart->Text = date('d/m/').(date('Y') - 2);
            }
            if ($criteriaVo->getDateFinEnd()) {
                $this->dateRemisePlisEnd->Text = $criteriaVo->getDateFinEnd();
            } else {
                $this->dateRemisePlisEnd->Text = date('d/m/').(date('Y') + 2);
            }
        }
        if ($criteriaVo->getKeyWordAdvancedSearch()) {
            $this->keywordSearch->Text = $criteriaVo->getKeyWordAdvancedSearch();
        } else {
            $this->keywordSearch->Text = '';
        }
        if ($criteriaVo->getSearchModeExact()) {
            $this->exact->Checked = true;
            $this->floue->Checked = false;
        } else {
            $this->exact->Checked = false;
            $this->floue->Checked = true;
        }

        //Afficher les champs LT Referentiels personnalisables
        $this->afficherLtReferentielsPersonnalisables($criteriaVo);

        if ((Atexo_Module::isEnabled('RechercheAutoCompletion') && ($criteriaVo->getDenominationAdapte() || $criteriaVo->getOrgDenomination())) || true === $criteriaVo->getRechercheAvanceeAutoCompletion()) {
            $this->typeRechercheEntiteAutocomplete->checked = true;
            $this->typeRechercheEntiteListe->Checked = false;
            if ($criteriaVo->getDenominationAdapte()) {
                $refOrgDenomination = Atexo_ReferentielOrgDenomination::getRefOrgDenominationByDenomAdapter($criteriaVo->getDenominationAdapte());
                if ($refOrgDenomination instanceof CommonReferentielOrgDenomination) {
                    $this->Page->setViewState('denominationAdapte', $refOrgDenomination->getId());
                    $this->orgNameAM->Text = $refOrgDenomination->getDenominationNormalise();
                }
            } elseif ($criteriaVo->getOrgDenomination()) {
                $this->orgNameAM->Text = $criteriaVo->getOrgDenomination();
            }

            $this->scriptPurchaseNames->Text = "<script>document.getElementById('divEntityPurchase').style.display = 'none';";
            $this->scriptPurchaseNames->Text .= "J('#".$this->panelSearchAnnoncesMarches->getClientId()."').show();
        												J('#".$this->panelSearchInEntreprise->getClientId()."').hide();
        										</script>";
        } else {
            if ($criteriaVo->getAcronymeOrganisme()) {
                $this->organismesNames->SelectedValue = $criteriaVo->getAcronymeOrganisme();
                if ($criteriaVo->getIdService()) {
                    if ('entreprise' == $this->_calledFrom) {
                        $listEntityPurchase = $this->getlistEntityPurchase($criteriaVo->getAcronymeOrganisme(), $this->langue);
                    } else {
                        $listEntityPurchase = Atexo_EntityPurchase::getSubServicesAndMyService(Atexo_CurrentUser::getIdServiceAgentConnected(), $criteriaVo->getAcronymeOrganisme());
                    }
                    $this->entityPurchaseNames->dataSource = $listEntityPurchase;
                    $this->entityPurchaseNames->DataBind();
                    $this->entityPurchaseNames->SelectedValue = $criteriaVo->getIdService();
                }
                if (true === $criteriaVo->getInclureDescendance()) {
                    true == $this->inclureDescendances->Checked;
                    $this->entiteSeule->Checked = false;
                } else {
                    $this->entiteSeule->Checked = true;
                    false == $this->inclureDescendances->Checked;
                }
            } else {
                $this->entityPurchaseNames->SelectedValue = 0;
                $this->scriptPurchaseNames->Text = "<script>document.getElementById('divEntityPurchase').style.display = 'none';</script>";
            }
            if (Atexo_Module::isEnabled('RechercheAvanceeParTypeOrg')) {
                if ($criteriaVo->getTypeOrganisme()) {
                    $this->classification->SelectedValue = $criteriaVo->getTypeOrganisme();
                } else {
                    $this->classification->SelectedValue = 0;
                }
            }
            $this->scriptPurchaseNames->Text .= "<script>J('#".$this->panelSearchAnnoncesMarches->getClientId()."').hide();
        												J('#".$this->panelSearchInEntreprise->getClientId()."').show();
        										</script>";
            $this->typeRechercheEntiteListe->Checked = true;

            if ($criteriaVo->getOrgDenomination()) {
                $this->orgNameAM->Text = $criteriaVo->getOrgDenomination();
            } else {
                $this->orgNameAM->Text = '';
            }
        }
        if ($criteriaVo->getClauseSociale()) {
            $this->clauseSociales->SelectedValue = $criteriaVo->getClauseSociale();
        } else {
            $this->clauseSociales->SelectedValue = 0;
        }

        if ($criteriaVo->getAtelierProtege()) {
            $this->ateliersProteges->SelectedValue = $criteriaVo->getAtelierProtege();
        } else {
            $this->ateliersProteges->SelectedValue = 0;
        }
        if ($criteriaVo->getSiae()) {
            $this->siae->SelectedValue = $criteriaVo->getSiae();
        } else {
            $this->siae->SelectedValue = 0;
        }

        if ($criteriaVo->getEss()) {
            $this->ess->SelectedValue = $criteriaVo->getEss();
        } else {
            $this->ess->SelectedValue = 0;
        }

        if ($criteriaVo->getSocialeCommerceEquitable()) {
            $this->clauseSocialesCommerceEquitable->SelectedValue = $criteriaVo->getSocialeCommerceEquitable();
        } else {
            $this->clauseSocialesCommerceEquitable->SelectedValue = 0;
        }

        if ($criteriaVo->getSocialeInsertionActiviterEconomique()) {
            $this->clauseSocialesInsertionActiviteEconomique->SelectedValue = $criteriaVo->getSocialeInsertionActiviterEconomique();
        } else {
            $this->clauseSocialesInsertionActiviteEconomique->SelectedValue = 0;
        }

        if ($criteriaVo->getClauseEnv()) {
            $this->clauseEnvironnementale->SelectedValue = $criteriaVo->getClauseEnv();
        } else {
            $this->clauseEnvironnementale->SelectedValue = 0;
        }
        if ($criteriaVo->getMps()) {
            $this->marchesPublicsSimplifies->SelectedValue = $criteriaVo->getMps();
        } else {
            $this->marchesPublicsSimplifies->SelectedValue = 0;
        }
        if ($criteriaVo->getBourseCotraitance()) {
            $this->bourseCotraitance->SelectedValue = $criteriaVo->getBourseCotraitance();
        } else {
            $this->bourseCotraitance->SelectedValue = 0;
        }
        if (Atexo_Module::isEnabled('ConsultationDomainesActivitesLtReferentiel') && $criteriaVo->getDomaineActivite()) {
            $this->idAtexoRefDomaineActivites->codesRefSec->Value = $criteriaVo->getDomaineActivite();
        }
        if (Atexo_Module::isEnabled('RechercheAvanceeParDomainesActivite') && $criteriaVo->getDomaineActivite()) {
            $this->domaineActivite->idsDomaines->Text = $criteriaVo->getDomaineActivite();
        }
        if (Atexo_Module::isEnabled('RechercheAvanceeParQualification') && $criteriaVo->getQualification()) {
            $this->qualification->idsQualification->Text = base64_encode($criteriaVo->getQualification());
        }
        if (Atexo_Module::isEnabled('RechercheAvanceeParAgrement') && $criteriaVo->getAgrements()) {
            $this->agrements->idsSelectedAgrements->Value = $criteriaVo->getAgrements();
        }
        if ($criteriaVo->getDateFinStart()) {
            $this->dateMiseEnLigneStart->Text = $criteriaVo->getDateFinStart();
        }
        if ($criteriaVo->getDateFinEnd()) {
            $this->dateMiseEnLigneEnd->Text = $criteriaVo->getDateFinEnd();
        }
        if ($criteriaVo->getEnLigneDepuis()) {
            $this->dateMiseEnLigneCalculeStart->Text = $criteriaVo->getEnLigneDepuis();
        }
        if ($criteriaVo->getEnLigneJusquau()) {
            $this->dateMiseEnLigneCalculeEnd->Text = $criteriaVo->getEnLigneJusquau();
        }

        if ($criteriaVo->getIdAlerteConsultation()) {
            $this->regleNonObservee->SelectedValue = $criteriaVo->getIdAlerteConsultation();
        } else {
            $this->regleNonObservee->SelectedValue = 0;
        }
        if ('2' == (string) $criteriaVo->getAlerteConsultationCloturee()) {
            $this->affichageAlerteNonCloturee->Checked = true;
        } else {
            $this->affichageAlerteNonCloturee->Checked = false;
        }
        if ($criteriaVo->getCodesNuts()) {
            $this->displayCodesNuts($criteriaVo->getCodesNuts());
        }
        if ($criteriaVo->getIdCodeCpv2()) {
            $this->referentielCPV->getSelectedValues($criteriaVo->getIdCodeCpv2());
        }
    }

    public function displayInfoRechercheProcedureRestreinte($consultationId, $acrOrg)
    {
        $consultation = (new Atexo_Consultation())->retrieveConsultation($consultationId, $acrOrg);
        if ($consultation) {
            $this->refRestreinteSearch->Text = $consultation->getId();
        }
        $this->orgNamesRestreinteSearch->SelectedValue = $acrOrg;
    }

    public function displayAdvanceSearch()
    {
        $this->Page->rechercheAvancee->setVisible(true);
        $this->Page->panelResult->setVisible(false);
        $criteriaVo = $this->Page->getViewState('CriteriaVo');
        $rechPanier = '';
        if (Atexo_Module::isEnabled('PanierEntreprise') && isset($_GET['panierEntreprise'])) {
            $rechPanier = '&panierEntreprise';
        }
        if (isset($_GET['AllCons']) || ((!$this->getBaseDce() && $criteriaVo->getIdTypeAvis() == Atexo_Config::getParameter('TYPE_AVIS_CONSULTATION')))) {
            $this->response->redirect('?page=Entreprise.EntrepriseAdvancedSearch&searchAnnCons'.$rechPanier);
        } elseif (isset($_GET['searchAnnCons'])) {
            $this->response->redirect('?page=Entreprise.EntrepriseAdvancedSearch&searchAnnCons'.$rechPanier);
        } elseif ($this->getBaseDce()) {
            $this->response->redirect('?page=frame.BaseDce&ticket='.Atexo_Util::atexoHtmlEntities($_GET['ticket']).'&fonctionnalite='.Atexo_Util::atexoHtmlEntities($_GET['fonctionnalite']).'&BaseDce=');
        } else {
            $this->response->redirect('?page=Entreprise.EntrepriseAdvancedSearch'.$rechPanier);
        }
    }

    public function onSearchProcedureRest($sender, $param)
    {
        Atexo_CurrentUser::deleteFromSession('organisme');
        Atexo_CurrentUser::deleteFromSession('reference');
        Atexo_CurrentUser::deleteFromSession('codeAcces');
        Atexo_CurrentUser::deleteFromSession('criteriaCons');

        $criteriaVo = new Atexo_Consultation_CriteriaVo();
        if ('entreprise' == $this->_calledFrom) {
            $criteriaVo->setTypeAcces('2');
            $criteriaVo->setEtatConsultation(Atexo_Config::getParameter('STATUS_CONSULTATION'));
            $criteriaVo->setcalledFromPortail(true);

            /*if (($this->orgNamesRestreinteSearch->getSelectedValue()
               && $this->orgNamesRestreinteSearch->getSelectedValue()!='0')
               && (trim($this->refRestreinteSearch->Text))!=''
               && (trim($this->accesRestreinteSearch->Text))!='') {
            }*/
            if ($this->orgNamesRestreinteSearch->getSelectedValue()
               && '0' != $this->orgNamesRestreinteSearch->getSelectedValue()) {
                $criteriaVo->setAcronymeOrganismeProcRest($this->orgNamesRestreinteSearch->getSelectedValue());
                $criteriaVo->setAcronymeOrganisme(trim($this->orgNamesRestreinteSearch->getSelectedValue())); //cas Recherche restreinte
            }

            if ($this->entityPurchaseNamesConsRestreinte->SelectedValue) {
                $criteriaVo->setIdService($this->entityPurchaseNamesConsRestreinte->SelectedValue);
            }

            if (trim($this->refRestreinteSearch->Text)) {
                $criteriaVo->setReferenceConsultationRestreinte(trim($this->refRestreinteSearch->Text));
            }
            if (trim($this->accesRestreinteSearch->Text)) {
                $criteriaVo->setCodeAccesProcedureRestreinte(trim($this->accesRestreinteSearch->Text));
            }

            Atexo_CurrentUser::writeToSession('organisme', $this->orgNamesRestreinteSearch->getSelectedValue());
            Atexo_CurrentUser::writeToSession('reference', trim($this->refRestreinteSearch->Text));
            Atexo_CurrentUser::writeToSession('codeAcces', trim($this->accesRestreinteSearch->Text));
        }

        //Recherche sur les consultations de mon panier
        if ('entreprise' == $this->_calledFrom && Atexo_Module::isEnabled('PanierEntreprise') && isset($_GET['searchAnnCons']) && isset($_GET['panierEntreprise'])) {
            $criteriaVo->setcalledFromPortail(true);
            $criteriaVo->setForPanierEntreprise(true);
            $criteriaVo->setIdInscrit(Atexo_CurrentUser::getIdInscrit());
            $criteriaVo->setIdEntreprise(Atexo_CurrentUser::getIdEntreprise());
        }

        $this->Page->setViewState('CriteriaVo', $criteriaVo);
        Atexo_CurrentUser::writeToSession('criteriaCons', $criteriaVo);
        $this->Page->rechercheAvancee->setVisible(false);
        $this->Page->dataForSearchResult($criteriaVo);
        //$this->Page->panelResult->setVisible(true);
    }

    public function getLieuExecutionUrl()
    {
        $param = '';

        if (Atexo_Module::isEnabled('LieuxExecutionCarte')) {
            $param = $this->numSelectedGeoN2->Value;

            if (!$this->Page->IsPostBack && empty($param)) {
                $param = $this->getFavoritedLieux();
            }
        }

        return Atexo_LieuxExecution::getLieuExecutionUrl($param);
    }

    public function getRawFavoritedLieux()
    {
        $lieux = '';

        if ('agent' == $this->_calledFrom) {
            $agent = Atexo_Agent::retrieveAgent(Atexo_CurrentUser::getIdAgentConnected());

            if ($agent) {
                $lieux = $agent->getLieuExecution();
            }
        }

        return $lieux;
    }

    public function getFavoritedLieux()
    {
        $buffer = $this->getRawFavoritedLieux();
        $arrayId = explode(',', $buffer);
        //Suppression des elements nuls du tableau
        $arrayId = array_filter($arrayId);
        $new_arrayId = [];

        $geoN2 = Atexo_Geolocalisation_GeolocalisationN2::retrieveGeolocalisationN2byIds($arrayId);

        if ($geoN2) {
            foreach ($geoN2 as $oneN2) {
                $new_arrayId[] = $oneN2->getDenomination2();
            }
        }

        return implode('_', $new_arrayId);
    }

    public function customizeForm()
    {
        if (!Atexo_Module::isEnabled('PradoValidateurFormatDate')) {
            $this->controlDateMiseEnLigneStart->Enabled = false;
            $this->controlDateMiseEnLigneEnd->Enabled = false;
            $this->controlDateMiseEnLigneCalculeStart->Enabled = false;
            $this->controlDateMiseEnLigneCalculeEnd->Enabled = false;
            $this->controlDateRemisePlisStart->Enabled = false;
            $this->controlDateRemisePlisEnd->Enabled = false;
        }
    }

    /* Pour effacer les codes cpv et lieux d'execution
    *  si on click sur le boutton effacer les critaires de recherche
    *
    *
    */
    public function cleareCritaire($sender, $param)
    {
        $criteriaVo = new Atexo_Consultation_CriteriaVo();
        $this->Page->setViewState('CriteriaVo', $criteriaVo);
        Atexo_CurrentUser::writeToSession('criteriaCons', $criteriaVo);
        $this->referentielCPV->clearSelection();
        $this->referentielCPV->chargerReferentielCpv('', '');

        $this->denominationGeoN2->Text = '';
        //domaines d'activites
        $this->domaineActivite->trunLibelleDomaines->Text = '';
        $this->domaineActivite->libellesDomaines->Text = '';
        $this->domaineActivite->idsDomaines->Text = '';
        $this->domaineActivite->toisPointsInfoBulle->setDisplay('None');
        //qualifications
        $this->qualification->trunLibelleQualif->Text = '';
        $this->qualification->idsQualification->Text = '';
        $this->qualification->libellesQualif->Text = '';
        $this->qualification->libelleQualif->Text = '';
        $this->qualification->toisPointsInfoBulle->setDisplay('None');
        //agrements
        $this->agrements->idsSelectedAgrements->Value = '';
        $this->agrements->labelleAgrement->Text = '';
        $this->agrements->libellesInfoBulleAgrement->Text = '';
        $this->agrements->toisPoints->setDisplay('None');

        $criteriaVo->setTypeAcces(Atexo_Config::getParameter('TYPE_PROCEDURE_PUBLICITE'));
        if (Atexo_Module::isEnabled('RechercheAutoCompletion')) {
            $criteriaVo->setRechercheAvanceeAutoCompletion(true);
        } else {
            $criteriaVo->setRechercheAvanceeAutoCompletion(false);
        }
        $this->displayGeoN2InModification('');
        $this->resetDate();
        $this->displayCriteria();
    }

    public function refreshCritaire($sender, $param)
    {
        $this->choixClassification($sender, $param);
        $this->lieuExecution->render($param->getNewWriter());
        $this->panelButton->render($param->getNewWriter());
        $this->panelDomainesActivites->render($param->getNewWriter());
        $this->panelQualifications->render($param->getNewWriter());
        $this->panelAgrements->render($param->getNewWriter());
    }

    public function fillClassification()
    {
        $codesInsee = CommonCategorieINSEEPeer::doSelect(new Criteria(), Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY')));
        $codesInsee = is_array($codesInsee) ? $codesInsee : [];
        $dataSource = ['0' => '--- '.Prado::localize('TOUTES_LES_CLASSIFICATIONS').' ---'];
        foreach ($codesInsee as $insee) {
            $dataSource[$insee->getId()] = $insee->getLibelle();
        }
        $this->classification->setDataSource($dataSource);
        $this->classification->DataBind();

        $this->classificationConsRestreinte->setDataSource($dataSource);
        $this->classificationConsRestreinte->DataBind();
    }

    public function choixClassification($sender, $param)
    {
        $idClassification = $this->classification->getSelectedValue();

        $ArrayOrganismes = Atexo_Organismes::retrieveOrganismes(false, $this->langue, true, $idClassification);
        array_unshift($ArrayOrganismes, '--- '.Prado::localize('TEXT__TOUS_MINISTERES').' ---');

        $this->organismesNames->dataSource = $ArrayOrganismes;
        $this->organismesNames->DataBind();

        $this->scriptPurchaseNames->Text = "<script>document.getElementById('divEntityPurchase').style.display = 'none';
       										 J('#".$this->panelSearchAnnoncesMarches->getClientId()."').hide();
        									J('#".$this->panelSearchInEntreprise->getClientId()."').show();</script>";

        $this->panelSearchInEntreprise->render($param->NewWriter);
    }

    public function choixClassificationConsRestreinte($sender, $param)
    {
        $idClassification = $this->classificationConsRestreinte->getSelectedValue();

        $ArrayOrganismes = Atexo_Organismes::retrieveOrganismes(false, $this->langue, true, $idClassification);
        array_unshift($ArrayOrganismes, '--- '.Prado::localize('TEXT__TOUS_MINISTERES').' ---');

        $this->orgNamesRestreinteSearch->dataSource = $ArrayOrganismes;
        $this->orgNamesRestreinteSearch->DataBind();

        $this->scriptPurchaseNamesConsRestreinte->Text = "<script>document.getElementById('divEntityPurchaseConsRestreinte').style.display = 'none';</script>";

        $this->panelSearchPrRest->render($param->NewWriter);
        $this->panelSearch->render($param->NewWriter);
    }

    public function displayEntityPurchaseConsRestreinte($sender, $param)
    {
        $organisme = $this->orgNamesRestreinteSearch->SelectedValue;
        // script pour afficher liste des services
        $this->scriptPurchaseNamesConsRestreinte->Text = "<script>document.getElementById('divEntityPurchaseConsRestreinte').style.display = '';</script>";
        $listEntityPurchase = $this->getlistEntityPurchase($organisme, $this->langue);
        $this->entityPurchaseNamesConsRestreinte->dataSource = $listEntityPurchase;
        $this->entityPurchaseNamesConsRestreinte->DataBind();
        $this->panelSearchPrRest->render($param->NewWriter);
        $this->panelSearch->render($param->NewWriter);
    }

    public function isCoteEntreprise()
    {
        $page = explode('.', Atexo_Util::atexoHtmlEntities($_GET['page']));
        if ('entreprise' == $page[0]) {
            $this->ltRefCons->setEntreprise(true);
        }
    }

    public function lienRechercherAvanceePanier()
    {
        $this->response->redirect('index.php?page=Entreprise.EntrepriseAdvancedSearch&searchAnnCons&panierEntreprise');
    }

    public function conditionModifierMaRecherche()
    {
        if (Atexo_Module::isEnabled('PanierEntreprise') && isset($_GET['panierEntreprise']) && !isset($_GET['searchAnnCons'])) {
            return true;
        } else {
            return false;
        }
    }

    protected function suggestNames($sender, $param)
    {
        // Get the token
        $token = $param->getToken();
        if ($listeEntite = Atexo_ReferentielOrgDenomination::retreiveOrgDenomination($token)) {
            $sender->DataSource = $listeEntite;
            $sender->dataBind();
        } else {
            $sender->DataSource = [];
            $sender->dataBind();
            $this->codeDenomAdapte = '';
            $this->Page->setViewState('denominationAdapte', '');
        }
    }

    protected function suggestOrganismesNames(TJuiAutoComplete $sender, TJuiAutoCompleteEventParameter $param): void
    {
        $token = $param->getToken();
        $organismes = Atexo_Organismes::retrieveOrganismesByDenominationForAutocompletion($token);

        if (!empty($organismes)) {
            $sender->DataSource = $organismes;
            $sender->dataBind();
        } else {
            $sender->DataSource = [];
            $sender->dataBind();
            $this->codeDenomAdapte = '';
            $this->Page->setViewState('denominationAdapte', '');
        }
    }

    public function suggestionSelected($sender, $param)
    {
        $this->codeDenomAdapte = $sender->Suggestions->DataKeys[$param->selectedIndex];
        $this->Page->setViewState('denominationAdapte', '');
        if ('' != $this->codeDenomAdapte) {
            $this->Page->setViewState('denominationAdapte', $this->codeDenomAdapte);
        }
    }

    /**
     * Permet de remplire les listes des clauses.
     */
    public function displayClauses()
    {
        $arrayClausesSociales = [Atexo_Config::getParameter('CLAUSES_SOCIALES_ACTIVER') => Prado::localize('DEFINE_OUI'), Atexo_Config::getParameter('CLAUSES_SOCIALES_DESACTIVER') => Prado::localize('DEFINE_NON')];
        $arrayClausesSociales = Atexo_Util::arrayUnshift($arrayClausesSociales, Prado::localize('TEXT_INDIFFERENT'));
        $this->clauseSociales->DataSource = $arrayClausesSociales;
        $this->clauseSociales->DataBind();

        $this->ateliersProteges->DataSource = $arrayClausesSociales;
        $this->ateliersProteges->DataBind();

        $this->ess->DataSource = $arrayClausesSociales;
        $this->ess->DataBind();

        $this->siae->DataSource = $arrayClausesSociales;
        $this->siae->DataBind();

        $this->clauseSocialesInsertionActiviteEconomique->DataSource = $arrayClausesSociales;
        $this->clauseSocialesInsertionActiviteEconomique->DataBind();

        $this->clauseSocialesCommerceEquitable->DataSource = $arrayClausesSociales;
        $this->clauseSocialesCommerceEquitable->DataBind();

        $arrayClausesEnv = [Atexo_Config::getParameter('CLAUSES_ENVIRONNEMENTALES_ACTIVER') => Prado::localize('DEFINE_OUI'), Atexo_Config::getParameter('CLAUSES_ENVIRONNEMENTALES_DESACTIVER') => Prado::localize('DEFINE_NON')];
        $arrayClausesEnv = Atexo_Util::arrayUnshift($arrayClausesEnv, Prado::localize('TEXT_INDIFFERENT'));
        $this->clauseEnvironnementale->DataSource = $arrayClausesEnv;
        $this->clauseEnvironnementale->DataBind();
    }

    /**
     * Permet d'afficher les lieux d'execution.
     */
    public function displayGeoN2InModification($lieuExecution)
    {
        $this->idsSelectedGeoN2->Value = $lieuExecution;
        $ids = $this->idsSelectedGeoN2->Value;
        $this->denominationGeoN2->Text = $this->getSelectedGeoN2($ids);
        $this->numSelectedGeoN2->Value = $this->getNumSelectedGeoN2($ids);
    }

    /**
     * Permet de charger les lieux d'execution de la consultation au bon format pour la carte.
     */
    public function getNumSelectedGeoN2($buffer)
    {
        $text = '';
        $arrayId = explode(',', $buffer);
        //Suppression des elements nuls du tableau
        $arrayId = array_filter($arrayId);
        $new_arrayId = [];

        $geoN2 = Atexo_Geolocalisation_GeolocalisationN2::retrieveGeolocalisationN2byIds($arrayId);
        if ($geoN2) {
            foreach ($geoN2 as $oneN2) {
                $new_arrayId[] = $oneN2->getDenomination2();
            }
        }

        $text = implode('_', $new_arrayId);

        return $text;
    }

    /**
     * Permet de gerer l'affichage du bloc "Marche public simplifié".
     *
     * @author Oumar KONATE <oumar.konate@atexom.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function gererAffichageMarchesPublicsSimplifies()
    {
        if (isset($_GET['searchAnnCons'])
                && (
                    ('agent' === $this->_calledFrom && Atexo_Module::isEnabled('MarchePublicSimplifie', Atexo_CurrentUser::getCurrentOrganism()))
                        ||
                        ('entreprise' === $this->_calledFrom && Atexo_Module::isEnabled('MarchePublicSimplifieEntreprise'))
                )
        ) {
            $this->panelMarchesPublicsSimplifies->setVisible(true);
            $this->displayMarchesPublicsSimplifies();
        } else {
            $this->panelMarchesPublicsSimplifies->setVisible(false);
        }
    }

    /**
     * Permet de charger la liste des options de recherches concernant les marches publics simplifies.
     *
     * @author Oumar KONATE <oumar.konate@atexom.com>
     *
     * @version 1.0
     *
     * @since 4.7.0
     *
     * @copyright Atexo 2014
     */
    public function displayMarchesPublicsSimplifies()
    {
        $arrayMPS = [];
        $arrayMPS['0'] = Prado::localize('TEXT_INDIFFERENT');
        $arrayMPS['1'] = Prado::localize('DEFINE_OUI');
        $arrayMPS['2'] = Prado::localize('DEFINE_NON');
        $this->marchesPublicsSimplifies->DataSource = $arrayMPS;
        $this->marchesPublicsSimplifies->DataBind();
    }

    /**
     * permet de sauvegarder une recherche favorite.
     *
     * @param mixed $sender
     * @param mixed $param
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function SaveAndSearch($sender, $param)
    {
        if ($this->Page->isValid) {
            try {
                $criteriaVo = self::getCriteriaForSearch();
                $rechercheInfoComp = self::getCriteriaMesRecherches();
                $rechercheObj = (new Atexo_Entreprise_Recherches)->saveRecherche($criteriaVo, $rechercheInfoComp);
                if ($rechercheObj instanceof CommonTMesRecherches) {
                    self::showConfirmationSave($sender, $param);
                }
                $this->scriptJs->text = "";
                $this->scriptJs->text = "<script>J('#modalSaveSearch').modal('hide');</script>";
            } catch (Exception $e) {
                Prado::log(" Erreur lors de la sauvegarde d'une recherche favorite ".$e->getMessage().$e->getTraceAsString(), TLogger::ERROR, 'advancedSearch.php');
            }
        }
    }

    /**
     * permet de lancer une reherche.
     *
     * @param Atexo_Consultation_CriteriaVo $criteriaVo
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function startSearch($criteriaVo)
    {
        $this->Page->rechercheAvancee->setVisible(false);
        $this->Page->dataForSearchResult($criteriaVo);
    }

    /**
     * permet d'afficher le bloc de confirmation d'enregistrement d'une recherche favorite.
     *
     * @param mixed $sender
     * @param mixed $param
     *
     * @return void
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function showConfirmationSave($sender, $param)
    {
        $this->panelConfirmation->setVisible(true);
        $this->panelConfirmation->setMessage(Prado::localize('TEXT_CONFIRM_SAUVEGARDE_RECHERCHE'));
        $this->panelConfSave->render($param->getNewWriter());
    }

    /**
     * permet de verifier si au moins un type de sauvegarde est selectionne.
     *
     * @param $sender
     * @param $param
     *
     * @return void
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function validerSauvegardeRecherche($sender, $param)
    {
        $param->IsValid = true;
        $errorsTab = [];
        if ('' == $this->rechercheName->SafeText && !$this->rechercheName->SafeText) {
            $param->IsValid = false;
            $errorsTab[] = Prado::localize('NOM');
            $this->scriptJs->text = "<script>document.getElementById('divValidation').style.display='';</script>";
        }
        if (!$this->sauvegardeRecherche->checked && ('agent' == $this->_calledFrom || ('entreprise' == $this->_calledFrom && !$this->sauvegardeAlerte->checked))) {
            $param->IsValid = false;
            $errorsTab[] = Prado::localize('DEFINE_TYPE_SAUVEGARDE');
            $this->scriptJs->text = "<script>document.getElementById('divValidation').style.display='';</script>";
        }
        //Verifier si une (des) recherche(s) porte(nt) le meme nom
        $isRecherche = $this->sauvegardeRecherche->checked;
        if (!isset($_GET['idRecherche']) && $isRecherche) {
            $idUtilisateur = null;
            if ('entreprise' == $this->_calledFrom) {
                $idUtilisateur = Atexo_CurrentUser::getIdInscrit();
            } else {
                $idUtilisateur = Atexo_CurrentUser::getIdAgentConnected();
            }
            $rechercheExiste = (new Atexo_Entreprise_Recherches)->retrieveRechercheByNomAndCreateur($this->rechercheName->SafeText, $idUtilisateur, $isRecherche);
            if (false !== $rechercheExiste) {
                $param->IsValid = false;
                $errorsTab[] = Prado::localize('DEFINE_RECHERCHE_EXISTE_DEJA_AVEC_MEME_NOM');
                $this->scriptJs->text = "<script>document.getElementById('divValidation').style.display='';</script>";
            }
        }
        $this->divValidation->addServerErrors($errorsTab, false);

        return;
    }

    /**
     * Permet de retourner le criterie de recherche.
     *
     * @return Atexo_Recherche_CriteriaVo $criteriaMesRecherches
     *
     * @author LEZ <loubna.ezziani@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.8.0
     *
     * @copyright Atexo 2014
     */
    public function getCriteriaMesRecherches()
    {
        $criteriaMesRecherches = new Atexo_Recherche_CriteriaVo();
        $criteriaMesRecherches->setNomRecherche($this->rechercheName->SafeText);
        $criteriaMesRecherches->setIdCreateur($this->idCreateur);
        $criteriaMesRecherches->setTypeCreateur($this->typeCreateur);
        if ($this->sauvegardeRecherche->checked) {
            $criteriaMesRecherches->setRecherche('1');
        }
        if (isset($_GET['idRecherche']) && $_GET['idRecherche']) {
            $criteriaMesRecherches->setIdRecherche($_GET['idRecherche']);
        }
        if ($this->typeCreateur == Atexo_Config::getParameter('TYPE_CREATEUR_ENTREPRISE') && $this->sauvegardeAlerte->checked) {
            if ($this->sauvegardeAlerte->checked) {
                $criteriaMesRecherches->setAlerte('1');
            }
            if (true == $this->tousLesJours->checked) {
                $criteriaMesRecherches->setPeriodicite(Atexo_Config::getParameter('PERIODICITE_TOUJOURS'));
            } elseif (true == $this->toutesLesSemaines->checked) {
                $criteriaMesRecherches->setPeriodicite(Atexo_Config::getParameter('PERIODICITE_TOUTES_SEMAINES'));
            } elseif (true == $this->desactiver->checked) {
                $criteriaMesRecherches->setPeriodicite(Atexo_Config::getParameter('PERIODICITE_DESACTIVER'));
            }
            if (true == $this->formatHtml->checked) {
                $criteriaMesRecherches->setFormat('1');
            } elseif (true == $this->formatTexte->checked) {
                $criteriaMesRecherches->setFormat('0');
            }
        }

        return $criteriaMesRecherches;
    }

    /**
     * Permet d'afficher les codes nuts.
     *
     * @param string $codesNuts: liste des codes nuts
     *
     * @throws Exception
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2014
     */
    public function displayCodesNuts($codesNuts)
    {
        $atexoCodesNutsRef = new Atexo_Ref();
        $atexoCodesNutsRef->setCheminFichierConfigXML(Atexo_Config::getParameter('PATH_FILE_NUTS_CONFIG'));
        $atexoCodesNutsRef->setSecondaires($codesNuts);
        $this->idAtexoRefNuts->afficherReferentiel($atexoCodesNutsRef);
    }

    /**
     * Permet de recuperer l'attribut $rechercheFavorite.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @return CommonTMesRecherches : objet recherche favorite
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2014
     */
    public function getRechercheFavorite()
    {
        return $this->rechercheFavorite;
    }

    /**
     * Permet de charger les informations de la pop in de sauvegarde de la recherche.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2014
     */
    public function chargerInfosPopInRechercheFavorite()
    {
        if ($this->getRechercheFavorite() instanceof CommonTMesRecherches) {
            $this->rechercheName->Text = $this->getRechercheFavorite()->getDenomination();
            if ('1' == $this->getRechercheFavorite()->getRecherche()) {
                $this->sauvegardeRecherche->checked = true;
            }
            if ($this->typeCreateur == Atexo_Config::getParameter('TYPE_CREATEUR_ENTREPRISE') && '1' == $this->getRechercheFavorite()->getAlerte()) {
                $this->sauvegardeAlerte->checked = true;
                if ($this->getRechercheFavorite()->getPeriodicite() == Atexo_Config::getParameter('PERIODICITE_DESACTIVER')) {
                    $this->desactiver->checked = true;
                } elseif ($this->getRechercheFavorite()->getPeriodicite() == Atexo_Config::getParameter('PERIODICITE_TOUJOURS')) {
                    $this->tousLesJours->checked = true;
                } elseif ($this->getRechercheFavorite()->getPeriodicite() == Atexo_Config::getParameter('PERIODICITE_TOUTES_SEMAINES')) {
                    $this->toutesLesSemaines->checked = true;
                }
                if ('1' == $this->getRechercheFavorite()->getFormat()) {
                    $this->formatHtml->checked = true;
                } elseif ('0' == $this->getRechercheFavorite()->getFormat()) {
                    $this->formatTexte->checked = true;
                }
                $this->infoCompAlerte->style = 'Display:block;';
            } else {
                $this->sauvegardeAlerte->checked = false;
                $this->infoCompAlerte->style = 'Display:none;';
                $this->desactiver->checked = true;
                $this->formatHtml->checked = true;
            }
        }
    }

    /**
     * Permet d'afficher les informations des champs LT-Referentiels personnalisables.
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @param Atexo_Consultation_CriteriaVo $criteriaVo: objet virtuel criteres
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2014
     */
    public function afficherLtReferentielsPersonnalisables($criteriaVo)
    {
        $referentielVo = $criteriaVo->getReferentielVo();
        $refTextValue = null;
        $referentielValue = null;
        if (is_array($referentielVo)) {
            foreach ($referentielVo as $refVo) {
                if (1 == $refVo->getType()) {
                    $refTextValue[$refVo->getId()] = $refVo;
                } elseif (2 == $refVo->getType()) {
                    $referentielRadioValue[$refVo->getId()] = $refVo;
                } else {
                    $referentielValue[$refVo->getId()] = $refVo;
                }
            }
        }
        $this->ltRefCons->afficherReferentielConsultation(null, null, null, $referentielValue, true);
        $this->idReferentielZoneText->afficherTextConsultation(null, null, null, $refTextValue, true);
        $this->idAtexoLtRefRadio->afficherReferentielRadio(null, null, null, $referentielRadioValue, true);
    }

    /**
     * Permet de verifier si l'appel de la page se fait depuis le service metier CAO (Commission).
     *
     * @return bool : true si l'appel se fait depuis Commission, false sinon
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2015
     */
    public function isCallFromCommission()
    {
        $arrayParamRequest = explode('.', $this->getRequest()->getServiceParameter());

        return 'page' == $this->getRequest()->getServiceId() && 'commission' == $arrayParamRequest[0];
    }

    /**
     * Permet de preciser si les champs LT Ref personnalises doivent etre affiches hors de mpe.
     *
     * @return bool : true si champs LT Ref personnalises à afficher hors de mpe, false sinon
     *
     * @author Oumar KONATE <oumar.konate@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.6.0
     *
     * @copyright Atexo 2015
     */
    public function showLtRefPersonnalisesHorsMpe()
    {
        if ($this->isCallFromCommission() && '0' == Atexo_Config::getParameter('AFFICHER_REFERENTIELS_PERSONNALISES_RECHERCHE_AVANCEE_HORS_MPE')) {
            return false;
        }

        return true;
    }

    /**
     * Permet de gerer l'affichage du bloc "Marche public simplifié".
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function gererAffichageBourseCotraitance()
    {
        if (isset($_GET['searchAnnCons'])
            && Atexo_Module::isEnabled('BourseCotraitance')
            && 'entreprise' === $this->_calledFrom
            && Atexo_CurrentUser::getIdEntreprise()
        ) {
            $this->panelBourseCotraitance->setVisible(true);
            $this->displayBourseCotraitance();
        } else {
            $this->panelBourseCotraitance->setVisible(false);
        }
    }

    /**
     * Permet de charger la liste des options de recherches concernant les marches publics simplifies.
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function displayBourseCotraitance()
    {
        $arrayBC = [];
        $arrayBC['0'] = Prado::localize('TEXT_INDIFFERENT');
        $arrayBC['1'] = Prado::localize('DEFINE_OUI');
        $arrayBC['2'] = Prado::localize('DEFINE_NON');
        $this->bourseCotraitance->DataSource = $arrayBC;
        $this->bourseCotraitance->DataBind();
    }

    /**
     * Permet de retourner le message d'infobulle MPS.
     *
     * @return string $message
     *
     * @author Ayoub SOUID AHMED <ayoub.souidahmed@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.11.0
     *
     * @copyright Atexo 2015
     */
    public function getMessageMPS()
    {
        $message = '';
        if ('agent' == $this->_calledFrom) {
            $message = Prado::localize('DEFINE_INFORMATION_CONSULTATION_MARCHE_PUBLIC_SIMPLIFIE_AGENT');
        } elseif ('entreprise' == $this->_calledFrom) {
            $message = Prado::localize('DEFINE_INFORMATION_CONSULTATION_MARCHE_PUBLIC_SIMPLIFIE_ENTREPRISE');
        }

        return $message;
    }

    public function setIsProfilRmaVisible($v)
    {
        $this->isProfilRmaVisible = $v;
    }

    public function resetDate()
    {
        $this->dateMiseEnLigneStart->Text = date('d/m/Y');
        $this->dateMiseEnLigneEnd->Text = Atexo_Util::dateWithparameter(+1, date('d/m/Y'), 0, 6, 0);
        if (!isset($_GET['annonce'])) {
            $this->dateMiseEnLigneCalculeStart->Text = Atexo_Util::dateWithparameter(-1, date('d/m/Y'), 0, 6, 0);
            $this->dateMiseEnLigneCalculeEnd->Text = date('d/m/Y');
        }
        if (isset($_GET['archive'])) {
            $this->dateMiseEnLigneStart->Text = Atexo_Util::dateWithparameter(-1, date('d/m/Y'), 0, 0, 5);
            $this->dateMiseEnLigneEnd->Text = date('d/m/Y');
            $this->dateMiseEnLigneCalculeStart->Text = Atexo_Util::dateWithparameter(-1, date('d/m/Y'), 0, 2, 0);
            $this->dateMiseEnLigneCalculeEnd->Text = date('d/m/Y');
        }
        if (isset($_GET['id'])) {//Appele depuis CAO Commission
            $this->dateMiseEnLigneStart->Text = '';
            $this->dateMiseEnLigneEnd->Text = '';
            $this->dateMiseEnLigneCalculeStart->Text = '';
            $this->dateMiseEnLigneCalculeEnd->Text = '';
        }

        if (!$this->Page->IsPostBack && $this->getBaseDce()) {
            $dateRemiseStart = date('d/m/').(date('Y') - 2);
            $dateRemiseEnd = date('d/m/').(date('Y') + 2);
            $this->dateRemisePlisStart->Text = $dateRemiseStart; //Atexo_Util::dateWithparameter(date('d/m/Y'),0,6,0);
            $this->dateRemisePlisEnd->Text = $dateRemiseEnd;
        }

        if (!isset($_GET['searchAnnCons'])) {
            $this->dateMiseEnLigneStart->Text = '';
            $this->dateMiseEnLigneEnd->Text = '';
            $this->dateMiseEnLigneCalculeStart->Text = '';
            $this->dateMiseEnLigneCalculeEnd->Text = '';
        }

        if (!$this->Page->IsPostBack) {
            $this->dateMiseEnLigneEnd->Text = Atexo_Util::dateWithparameter(+1, date('d/m/Y'), 0, 6, 0);
            $this->dateMiseEnLigneStart->Text = date('d/m/Y');
            $this->dateMiseEnLigneCalculeEnd->Text = date('d/m/Y');
            $this->dateMiseEnLigneCalculeStart->Text = Atexo_Util::dateWithparameter(-1, date('d/m/Y'), 0, 6, 0);
        }
    }
}
