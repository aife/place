<!--com:TContent ID="CONTENU_PAGE"-->
<!--Debut colonne droite-->
<div class="pwd-forgotten">
	<ul class="breadcrumb">
		<li><com:TTranslate>TEXT_ACCUEIL</com:TTranslate></li>
	</ul>
	<com:PanelMessage id="panelMessage" visible="false"/>
	<com:TPanel id="panelEnvoiLogin" >
		<div class="panel panel-primary">
			<div class="panel-heading">
				<h2 class="h4 m-0"><com:TTranslate>TEXT_MOT_PASSE_OUBLIE</com:TTranslate></h2>
			</div>
			<div class="panel-body">
				<com:TPanel id="panelMotPasseOublie" >
					<p><com:TTranslate>TEXT_INFOS_MOT_PASSE_OUBLIE</com:TTranslate></p>
				</com:TPanel>
				<!--Debut Bloc Login Identifiant Mot de passe-->
				<div class="col-md-offset-2 col-md-9 m-t-2">
					<div class="form-group form-group-sm clearfix">
						<label for="emailUser" class="col-md-3 control-label"><com:TTranslate>TEXT_ADRESSE_ELECTRONIQUE</com:TTranslate> :</label>
						<div class="col-md-9">
							<div class="col-md-9">
								<com:TTextBox id="emailUser" Attributes.title="<%=Prado::localize('TEXT_ADRESSE_ELECTRONIQUE')%>" CssClass="form-control" />
							</div>
							<com:TLinkButton CssClass="btn btn-sm btn-primary col-md-3" onClick="getPassword" ><com:TTranslate>DEFINE_TEXT_ENVOYER</com:TTranslate></com:TLinkButton>
						</div>
					</div>

				</div>
				<!--Fin Bloc Login Identifiant Mot de passe-->
			</div>
			<div class="panel-footer">
				<com:THyperLink id="lienRetour" cssClass="btn btn-sm btn-default">
					<com:TTranslate>DEFINE_TEXT_RETOUR_ACCUEIL</com:TTranslate>
				</com:THyperLink>
			</div>
		</div>
	</com:TPanel>
</div>
<!--/com:TContent-->