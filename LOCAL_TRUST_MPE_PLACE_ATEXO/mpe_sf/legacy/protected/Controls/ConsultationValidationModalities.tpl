<!--Debut Bloc Modalites de validation et mise en ligne-->
	<com:TPanel ID="summaryVisible" CssClass="form-bloc recap-infos-consultation">
			<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
			<div class="content">
				<div onclick="togglePanel(this,'modalitesValidation');" class="title-toggle">&nbsp;</div>
				<div class="recap-bloc">
					<h2><com:TTranslate>TEXT_MODALITE_VALIDATION_ET_MISE_EN_LIGNE</com:TTranslate></h2>
					<div class="breaker"></div>
					<div id="modalitesValidation" style="display:none;" class="panel-toggle">
						<div class="line">
							<div class="intitule-250 bold"><com:TTranslate>TEXT_REGLE_MISE_EN_LIGNE</com:TTranslate> : </div>
							<!--Debut Ligne Date-->
							<div class="content-bloc bloc-450">
								<com:TLabel id="datemiseEnLigne" />
							</div>
							<!--Fin Ligne Date-->
						</div>
						<div class="spacer-small"></div>
						<div class="line">
							<div class="intitule-250 bold"><com:TTranslate>TEXT_REGLE_DE_VALIDATION</com:TTranslate>: </div>
							<div class="tableau-etape">
								<com:TPanel ID="panelValidationIntermediaire" CssClass="validation-etape">
                					<div class="line">
                						<div class="intitule-80"><com:TTranslate>ETAPE</com:TTranslate> 1/2 :</div>
                						<div class="content-bloc bloc-630"><com:TTranslate>VALIDATION_INTERMEDIAIRE</com:TTranslate> : <com:TLabel ID="eniteAchat1"/></div>
                					</div>
                				</com:TPanel>
                
                				<com:TPanel ID="panelValidationFinale" CssClass="validation-etape">
                					<div class="line">
                						<div class="intitule-80"><com:TTranslate>ETAPE</com:TTranslate> <com:TLabel ID="numeroEtape"/> :</div>
                						<div class="content-bloc bloc-630"><com:TTranslate>VALIDATION_FINALE</com:TTranslate>: <com:TLabel ID="eniteAchat2"/></div>
                					</div>
                				</com:TPanel>
                				
                				<com:TPanel ID="panelValidationSimple" CssClass="validation-etape">
                					<div class="line">
                						<div class="intitule-80"><com:TTranslate>ETAPE</com:TTranslate> 1/1 :</div>
                						<div class="content-bloc bloc-630"><com:TTranslate>VALIDATION_SIMPLE</com:TTranslate> : <com:TLabel ID="eniteAchat3"/></div>
                					</div>
                				</com:TPanel>
								<div class="breaker"></div>
							</div>
						</div>
						<div class="spacer-small"></div>
						<div class="line">
							<div class="intitule-250 bold"><com:TTranslate>TEXT_DATE_MISE_EN_LIGNE_CALCULE</com:TTranslate> : </div>
							<com:TLabel ID="dateMiseEnLigneCalcule"/>
						</div>
						<div class="breaker"></div>
					</div>
				</div>
				<div class="breaker"></div>
			</div>
			<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
	</com:TPanel>
<!--Fin Bloc  de validation et mise en ligne-->