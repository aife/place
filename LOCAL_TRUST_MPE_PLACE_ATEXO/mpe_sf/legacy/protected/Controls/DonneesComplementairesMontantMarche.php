<?php

namespace Application\Controls;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonTDonneeComplementaire;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Util;

class DonneesComplementairesMontantMarche extends MpeTTemplateControl
{
    private $mode;
    private $_donneeComplementaire;
    private $_connexion;
    private bool $afficher = true;

    /**
     * @return bool
     */
    public function getAfficher()
    {
        return $this->afficher;
    }

    /**
     * @param bool $afficher
     */
    public function setAfficher($afficher)
    {
        $this->afficher = $afficher;
    }

    // $sourcePage = ('etapeIdentification' si on est sur l'étape "identification")
    // $sourcePage = ('detailLots' si on est sur l'étape "detailLots")
    private ?string $sourcePage = null;
    private bool $afficherMarcheReconductible = true;
    private ?string $idElement = null;

    /**
     * recupère la valeur.
     */
    public function getIdElement()
    {
        return $this->idElement;
    }

    /**
     * Affecte la valeur.
     *
     * @param string $value
     */
    public function setIdElement($value)
    {
        $this->idElement = $value;
    }

    public function setAfficherMarcheReconductible($afficher)
    {
        $this->afficherMarcheReconductible = $afficher;
    }

    public function getAfficherMarcheReconductible()
    {
        return $this->afficherMarcheReconductible;
    }

    /**
     * Affecte la valeur.
     *
     * @param string $value
     */
    public function setMode($mode)
    {
        $this->mode = $mode;
    }

    /**
     * recupère la valeur.
     */
    public function getMode()
    {
        return $this->mode;
    }

    /**
     * recupère la valeur.
     */
    public function getSourcePage()
    {
        return $this->sourcePage;
    }

    // getSourcePage()

    /**
     * Affecte la valeur.
     *
     * @param string $value
     */
    public function setSourcePage($value)
    {
        if ($this->sourcePage !== $value) {
            $this->sourcePage = $value;
        }
    }

    // setSourcePage()

    public function getDonneeComplementaire()
    {
        return $this->_donneeComplementaire;
    }

    public function setDonneeComplementaire($donneeComplementaire)
    {
        $this->_donneeComplementaire = $donneeComplementaire;
    }

    public function getConnexion()
    {
        return $this->_connexion;
    }

    public function setConnexion($connexion)
    {
        $this->_connexion = $connexion;
    }

    public function onLoad($param)
    {
        if (Atexo_Module::isEnabled('DonneesComplementaires') && Atexo_Module::isEnabled('DonneesRedac')) {
            self::chargerComposant();
        }
    }

    public function saveMontantMarche(&$donneComplementaire)
    {
        if ($this->intitulePubliciteMontantOui->Checked) {
            $donneComplementaire->setPublicationMontantEstimation('1');
            $donneComplementaire->setValeurMontantEstimationPubliee(Atexo_Util::getMontantArronditSansEspace($this->valeurMarchePubliee->SafeText));
        } else {
            $donneComplementaire->setPublicationMontantEstimation('0');
            $donneComplementaire->setValeurMontantEstimationPubliee(null);
            $this->valeurMarchePubliee->Text = Atexo_Util::getMontantArronditEspace($donneComplementaire->getMontantMarche());
        }
    }

    public function chargerComposant()
    {
        $this->_connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        if (!$this->Page->IsPostBack) {
            if (($this->_donneeComplementaire instanceof CommonTDonneeComplementaire)) {
                if ($this->_donneeComplementaire->getPublicationMontantEstimation()) {
                    $this->intitulePubliciteMontantOui->Checked = true;
                    $this->intitulePubliciteMontantNon->Checked = false;
                    $this->blocValeurMarchePubliee->setDisplay('Dynamic');
                } else {
                    $this->intitulePubliciteMontantOui->Checked = false;
                    $this->intitulePubliciteMontantNon->Checked = true;
                    $this->blocValeurMarchePubliee->setDisplay('None');
                }
                if ($this->_donneeComplementaire->getValeurMontantEstimationPubliee()) {
                    $montant = $this->_donneeComplementaire->getValeurMontantEstimationPubliee();
                } else {
                    $montant = $this->_donneeComplementaire->getMontantMarche();
                }
                $this->valeurMarchePubliee->Text = Atexo_Util::getMontantArronditEspace($montant);
            } else {
                $this->intitulePubliciteMontantOui->Checked = false;
                $this->intitulePubliciteMontantNon->Checked = true;
                $this->blocValeurMarchePubliee->setDisplay('None');
            }
            if (true == $this->getAfficher()) {
                $this->donneeMontantMarche->style = 'display:""';
            } else {
                $this->donneeMontantMarche->style = 'display:none';
            }

            //self::populateDatas();
            self::rafraichissementJs();
        }
    }

    /*
     * Permet d'afficher les elements du composant selon le mode choisi
     */
    public function enabledElement()
    {
        return match ($this->mode) {
            0 => false,
            1 => true,
            default => true,
        };
    }

    /*
     * on l'utilise si on veux afficher plusieurs instance de ce composant dans une même page
     */
    public function getIdPanel()
    {
        if ($this->idElement) {
            return '_'.$this->idElement;
        } else {
            return '';
        }
    }

    /*
     * Permet de supprimer l'id panel dans une valeur
     */
    public function deleteIdPanelFromeValue($value)
    {
        if ($this->idElement) {
            $elements = explode($this->getIdPanel(), $value);
            $value = $elements[0];
        }

        return $value;
    }

    public function rafraichissementJs()
    {
        $this->scriptLabel->Text = '<script>';
        if ('Agent.PopupDetailLot' === $_GET['page']) {
            //Bloc Valeur estimée
           $this->scriptLabel->Text .='disactivateSelect("'.$this->intitulePubliciteMontantNon->ClientId.'");';
           $this->scriptLabel->Text .='disactivateSelect("'.$this->intitulePubliciteMontantOui->ClientId.'");';
           $this->scriptLabel->Text .='disactivateSelect("'.$this->valeurMarchePubliee->ClientId.'");';
          }
        $this->scriptLabel->Text .= '</script>';
    }
}
