<com:TConditional Condition="strstr(htmlspecialchars($_GET['page']), 'Entreprise') || strstr(htmlspecialchars($_GET['page']), 'LieuxExecutionCarte')">
    <prop:TrueTemplate>
        <com:TClientScript PradoScripts="prado,jquery,jqueryui,bootstrap,ajax" />
    </prop:TrueTemplate>
    <prop:FalseTemplate>
        <com:TClientScript PradoScripts="prado,jquery,bootstrap,ajax,jqueryui" />
    </prop:FalseTemplate>
</com:TConditional>

<script type="text/javascript">
    var J = jQuery.noConflict();
</script>


<com:TClientScript ScriptUrl=<%~ ../themes/js/sdk.js %> />
<com:TClientScript ScriptUrl=<%~ ../themes/js/bootboxjs.js %> />
<com:TClientScript ScriptUrl=<%~ ../themes/js/upload/jquery.iframe-transport.js %> />
<com:TClientScript ScriptUrl=<%~ ../themes/js/upload/jquery.fileupload.js %> />
<com:TClientScript ScriptUrl=<%~ ../themes/js/jquery.placeholder.js %> />
<com:TClientScript ScriptUrl=<%~ ../themes/js/chosen.jquery.js %> />
<com:TClientScript ScriptUrl=<%~ ../themes/js/modernizr.js %> />
<com:TClientScript ScriptUrl=<%~ ../themes/js/printThis/printThis.js %> />
<com:TClientScript ScriptUrl=<%~ ../themes/js/placeholder.js %> />

<com:TClientScript ScriptUrl=<%~ ../themes/js/scripts.js %> />
<com:TClientScript ScriptUrl=<%~ ../themes/js/popup.inc.js %> />
<com:TClientScript ScriptUrl=<%~ ../themes/js/scripts-eval.js %> />
<com:TClientScript ScriptUrl=<%~ ../themes/js/atexoFileUpload.js %> />
<com:TClientScript ScriptUrl=<%~ ../themes/js/scripts-lieux-execution.js %> />
<com:TClientScript ScriptUrl=<%~ ../themes/js/tab.js %> />
<com:TClientScript ScriptUrl=<%~ ../themes/js/loader.js %> />
<com:TClientScript ScriptUrl=<%~ ../themes/js/data.js %> />
<com:TClientScript ScriptUrl=<%~ ../themes/js/standard.js %> />
<com:TClientScript ScriptUrl=<%~ ../themes/js/printThis/printThis.js %> />
<com:TClientScript ScriptUrl=<%~ ../themes/js/attachmentMessec.js %> />

<com:TClientScript ScriptUrl=<%~ ../themes/js/jquery.numberformatter-1.2.js %> />
<com:TClientScript ScriptUrl=<%~ ../themes/js/OptionTransfer.js %> />
<com:TClientScript ScriptUrl=<%~ ../themes/js/scripts-lieux-execution.js %> />
<com:TClientScript ScriptUrl=<%~ ../themes/js/popUpInvites.js %> />
<com:TClientScript ScriptUrl="<%=$this->themesPath()%>/js/atexoScript.<%=Application\Service\Atexo\Atexo_CurrentUser::readFromSession('lang')%>.js?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>" />
<com:TClientScript ScriptUrl="<%=$this->themesPath()%>/js/codeCpv.<%=Application\Service\Atexo\Atexo_CurrentUser::readFromSession('lang')%>.js?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>" />
<com:TClientScript ScriptUrl="<%=$this->themesPath()%>/js/atexoRef.<%=Application\Service\Atexo\Atexo_CurrentUser::readFromSession('lang')%>.js?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>" />
<com:TClientScript ScriptUrl="<%=$this->themesPath()%>/js/calendrier.<%=Application\Service\Atexo\Atexo_CurrentUser::readFromSession('lang')%>.js?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>" />

<com:TClientScript ScriptUrl="<%=$this->themesPath()%>/js/atexoApplet.<%=Application\Service\Atexo\Atexo_CurrentUser::readFromSession('lang')%>.js?k=<%=$this->getParameter('MEDIA_CACHE_KEY')%>" />

<com:TClientScript ScriptUrl=<%~ ../themes/js/vue.min.js %> />

<com:TConditional Condition="(htmlspecialchars(
    $_GET['page']) == 'Entreprise.AccueilEntreprise'
    || htmlspecialchars($_GET['page']) == 'Commun.LieuxExecutionCarte'
    || htmlspecialchars($_GET['page']) == 'Agent.LieuxExecutionCarte'
    )">
    <prop:TrueTemplate>
<!-- Debut script map -->
<com:TClientScript ScriptUrl=<%~ ../themes/js/raphael.js %> />
<com:TClientScript ScriptUrl=<%~ ../themes/js/jsmap.components.js %> />
<com:TClientScript ScriptUrl=<%~ ../themes/map/vmap.js %> />
<com:TClientScript ScriptUrl=<%~ ../../../public/themes/map/map_local.js %> />
    </prop:TrueTemplate>
</com:TConditional>
<com:TConditional Condition="(htmlspecialchars($_GET['page']) == 'Agent.GestionNomenclature' || htmlspecialchars($_GET['page']) == 'Agent.UpdateAddNomenclature')">
    <prop:TrueTemplate>
        <com:TClientScript ScriptUrl=<%~ ../themes/js/jquery.treeTable.js %> />
    </prop:TrueTemplate>
</com:TConditional>

<com:TConditional Condition="(htmlspecialchars($_GET['page']) == 'Agent.MessageAccueil')">
    <prop:TrueTemplate>
<com:TClientScript ScriptUrl=<%~ ../themes/responsive/js/bootstrap-specifique-wysihtml5.js %> />
<com:TClientScript ScriptUrl=<%~ ../themes/responsive/js/wysihtml5-0.3.0.js %> />
<com:TClientScript ScriptUrl=<%~ ../themes/responsive/js/bootstrap-wysihtml5.js %> />
<com:TClientScript ScriptUrl=<%~ ../themes/responsive/js/bootstrap-wysihtml5.fr-FR.js %> />
    </prop:TrueTemplate>
</com:TConditional>
<com:TConditional Condition="(htmlspecialchars($_GET['page']) == 'Entreprise.FormulaireCompteEntreprise' || htmlspecialchars($_GET['page']) == 'Agent.AgentModifierCompte')">
    <prop:TrueTemplate>
        <com:TClientScript ScriptUrl=<%~ ../themes/js/bootstrap-toggle.min.js %> />
    </prop:TrueTemplate>
</com:TConditional>


<com:TClientScript Visible="<%= preg_match('/(^Agent)/i', $_GET['page']) || preg_match('/(^Agent)/i', $_GET['calledFrom']) || preg_match('/(^Administration)/i', $_GET['page']) || preg_match('/(^administration)/i', $_GET['calledFrom']) || preg_match('/(^commun)/i', $_GET['page']) || preg_match('/(^commun)/i', $_GET['calledFrom']) || preg_match('/(^GestionPub)/i', $_GET['page']) || preg_match('/(^GestionPub)/i', $_GET['calledFrom']) %>" >
window.addEventListener("load", function(event) {
    //Code à garder
    //dateBandeau('<%=time()*1000%>', '<%=date("Z")%>');
});
</com:TClientScript>
<com:TClientScript ScriptUrl=<%~ ../themes/js/prototype-noconflict.header.js %> />

<com:TClientScript ScriptUrl="/ressources/calendrier/CalendrierGWT/CalendrierGWT.nocache.js" Visible="<%=(($this->isEnabledModule('CalendrierDeLaConsultation') AND ($_GET['page'] == 'Agent.FormulaireConsultation'))) ? true : false%>"/>
<com:TClientScript ScriptUrl="/ressources/calendrier/CalendrierReelGWT/CalendrierReelGWT.nocache.js" Visible="<%=($_GET['page'] == 'Agent.DetailConsultation') ? true : false%>"/>
<com:TClientScript ScriptUrl="/ressources/calendrier/CalendrierAdministrationGWT/CalendrierAdministrationGWT.nocache.js" Visible="<%=(($this->isEnabledModule('CalendrierDeLaConsultation') AND ($_GET['page'] == 'Agent.ParametrerProcedures'))) ? true : false%>"/>

<script type="text/javascript">
    var myDefaultWhiteList = J.fn.popover.Constructor.DEFAULTS.whiteList;
    myDefaultWhiteList.a = ['onclick'];
</script>