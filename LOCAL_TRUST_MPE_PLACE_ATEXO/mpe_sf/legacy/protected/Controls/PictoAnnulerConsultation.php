<?php

namespace Application\Controls;

use Application\Service\Atexo\Controller\Atexo_Controller_Front;
use Prado\Prado;
use Prado\Web\UI\WebControls\TImageButton;

/**
 * commentaires.
 *
 * @author adil El Kanabi <adil.alkanabi@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PictoAnnulerConsultation extends TImageButton
{
    public $_parentPage;
    public string $_activate = 'true';
    public string $_clickable = 'true';

    public function onLoad($param)
    {
        if ('true' == $this->_activate) {
            $this->setImageUrl(Atexo_Controller_Front::t().'/images/picto-desactiver.gif');
        } else {
            $this->setImageUrl(Atexo_Controller_Front::t().'/images/picto-desactiver-inactive.gif');
        }

        if ('false' == $this->_clickable) {
            $this->Attributes->onclick = 'return false;';
        }

        if ('DetailConsultation' == $this->getParentPage()) {
            $this->setAlternateText(Prado::localize('ICONE_ANNULER'));
            $this->setToolTip(Prado::localize('ICONE_ANNULER'));
        }
    }

    public function getParentPage()
    {
        return $this->_parentPage;
    }

    public function setParentPage($value)
    {
        $this->_parentPage = $value;
    }

    public function getActivate()
    {
        return $this->_activate;
    }

    public function setActivate($bool)
    {
        $this->_activate = $bool;
    }

    public function getClickable()
    {
        return $this->_clickable;
    }

    public function setClickable($value)
    {
        $this->_clickable = $value;
    }
}
