<!--Debut Bloc Identité-->
<div class="form-field panel panel-default" style="display:block;">
	<div class="panel-heading">
		<h2 class="h4 panel-title">
			<com:TTranslate>DEFINE_IDENTITE</com:TTranslate>
		</h2>
	</div>
	<div class="content panel-body">
		<com:PanelInfosEntreprise ID="infoEntreprise" lectureSeule="true" />
		<div class="breaker"></div>
		<com:AtexoEtablissements ID="listeEtablissments" mode="1"/>
	</div>
</div>
<!--Fin Bloc Identité-->