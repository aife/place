<com:TConditional condition="$this->isResponsive()">
	<prop:trueTemplate>
		<%include Application.templates.responsive.AtexoHistoriquesDocument %>
	</prop:trueTemplate>
	<prop:falseTemplate>
		<%include Application.templates.mpe.AtexoHistoriquesDocument %>
	</prop:falseTemplate>
</com:TConditional>