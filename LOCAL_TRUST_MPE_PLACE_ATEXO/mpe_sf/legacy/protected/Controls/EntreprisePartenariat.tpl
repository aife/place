  <!--Debut Bloc Recherche de collaborateurs -->
  	<div class="panel-body">
		<div class="clearfix">
			<fieldset>
			<div class="form-group form-group-sm">
				<div class="col-md-7 p-t-1">
					<label>
						<%=Prado::localize('TEXT_ENTREPRISE_VISIBLE_BOURSE')%>
					</label>
					<span>
						<a data-target="#" data-content="<%=Prado::localize('MSG_EXPLICATION_RECHERCHE_SIMPLE')%>"
						   data-toggle="tooltip"
						   data-placement="right"
						   title="<%=Prado::localize('TEXT_VISIBLE_ENTREPRISES_CONNECTEES')%>">
							<i class="fa fa-question-cfa fa-question-circle text-infoircle text-info"></i>
							<div id="infosBulleBourseSousTraitance" class="sr-only info-bulle" onmouseover="mouseOverBulle();" onmouseout="mouseOutBulle();">
								<div>
									<ul>
										<li>- <%=Prado::localize('TEXT_SITE_INTERNET')%></li>
										<li>- <%=Prado::localize('TEXT_DESCRIPTION_ACTIVITE')%></li>
										<li>- <%=Prado::localize('DEFINE_ACTIVITES_DOMAINE_DEFENSE')%></li>
										<li>- <%=Prado::localize('TEXT_DOCUMENT_COMMERCIAUX')%></li>
										<li>- <%=Prado::localize('TEXT_REFERENTIELS')%></li>
										<li>- <%=Prado::localize('TEXT_TYPE_COLLABORATIONS_RECHERCHEES')%></li>
										<li>- <%=Prado::localize('TEXT_PERSONNE_A_CONTACTER_GROUPEMEENT')%></li>
									</ul>
								</div>
							</div>
						</a>
					</span>
				</div>
				<com:TPanel id="panelrepRefusee" cssClass="col-md-5">
					<label class="radio-inline" for="ctl0_CONTENU_PAGE_RechercheCollaborateur_rechercheCollaborateurOui">
						<com:TRadioButton
								GroupName="collaboration"
								id="rechercheCollaborateurOui"
								Attributes.onclick="showDiv('ctl0_CONTENU_PAGE_RechercheCollaborateur_panel_rechercheCollaborateur');overlay('infosBourseSousTraitance','overlayBourseSousTraitance');"
								Attributes.title="<%=Prado::localize('TEXT_OUI')%>"

						/>
						<com:TTranslate>TEXT_OUI</com:TTranslate>
					</label>
					<label class="radio-inline" for="ctl0_CONTENU_PAGE_RechercheCollaborateur_rechercheCollaborateurNon">
						<com:TRadioButton
								GroupName="collaboration"
								id="rechercheCollaborateurNon"
								Attributes.onclick="hideDiv('ctl0_CONTENU_PAGE_RechercheCollaborateur_panel_rechercheCollaborateur')"
								Attributes.title="<%=Prado::localize('TEXT_NON')%>"

						/>
						<com:TTranslate>TEXT_NON</com:TTranslate>
					</label>
				</com:TPanel>
			</div>
			</fieldset>

			<com:TActivePanel ID="panel_rechercheCollaborateur">
				<div class="line clearfix">
					<div class="section-heading page-header m-t-2">
						<com:TTranslate>TEXT_TYPE_COLLABORATION</com:TTranslate>
						<span>
							<a data-target="#"
							   data-content="<%=Prado::localize('MSG_EXPLICATION_RECHERCHE_SIMPLE')%>"
							   data-toggle="tooltip"
							   data-placement="right"
							   title="<com:TTranslate>TEXT_ENTREPRISE_VOLONTAIRE_REGOINDRE_GROUPEMENT</com:TTranslate>">
								<i class="fa fa-question-circle text-info"></i>
							</a>
						</span>
					</div>
					<div class="col-md-offset-3 col-md-9 checkbox">
						<div class="content-bloc bloc-570">
							<com:TRepeater  ID="repeaterTypeCollaboration" >
								<prop:ItemTemplate>
									<div class="col-md-6">
										<com:TCheckBox id="type" Attributes.title="<%#$this->Data['libelle']%>" cssClass="check" />
										<label for="type">
											<com:TLabel ID="denomination" Text="<%#$this->Data['libelle']%>"/>
											<com:THiddenField ID="idType" Value="<%#$this->Data['id']%>"/>
										</label>
									</div>
								</prop:ItemTemplate>
							</com:TRepeater>
						</div>
					</div>
				</div>
				<div class="line clearfix">
					<div class="section-heading page-header">
						<com:TTranslate>TEXT_PERSONNE_A_CONTACTER_GROUPEMENT</com:TTranslate>
					</div>
					<!--BEGIN FORMULAIRE-->
					<div class="col-md-6">
						<!--BEGIN NOM-->
						<div class="form-group form-group-sm">
							<label class="col-md-3 control-label" for="ctl0_CONTENU_PAGE_RechercheCollaborateur_nomContactGroupement">
								<%=Prado::localize('NOM')%>
								<span class="text-danger">* </span>
							</label>
							<div class="col-md-9">
								<com:TTextBox ID="nomContactGroupement"  cssClass="form-control" Attributes.title="<%=Prado::localize('NOM')%>" />
							</div>
							<com:TCustomValidator
									ControlToValidate="nomContactGroupement"
									ValidationGroup="validateInfosEntrperise"
									Display="Dynamic"
									ID="nomValidator"
									ErrorMessage="<%=Prado::localize('NOM')%>"
									Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMP_OBLIGATOIRE')%>' /></span>" EnableClientScript="true"
									ClientValidationFunction="valideControl">
								<prop:ClientSide.OnValidationError>
									document.getElementById('divValidationSummary').style.display='';
								</prop:ClientSide.OnValidationError>
							</com:TCustomValidator>
						</div>
						<!--END NOM-->

						<!--BEGIN PRENOM-->
						<div class="form-group form-group-sm">
							<label class="col-md-3 control-label" for="ctl0_CONTENU_PAGE_RechercheCollaborateur_prenomContactGroupement">
								<%=Prado::localize('PRENOM')%>
								<span class="text-danger">* </span>
							</label>
							<div class="col-md-9">
								<com:TTextBox ID="prenomContactGroupement"  cssClass="form-control" Attributes.title="<%=Prado::localize('PRENOM')%>" />
							</div>
							<com:TCustomValidator
									ControlToValidate="prenomContactGroupement"
									ValidationGroup="validateInfosEntrperise"
									Display="Dynamic"
									ID="prenomValidator"
									ErrorMessage="<%=Prado::localize('PRENOM')%>"
									Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMP_OBLIGATOIRE')%>' /></span>" EnableClientScript="true"
									ClientValidationFunction="valideControl">
								<prop:ClientSide.OnValidationError>
									document.getElementById('divValidationSummary').style.display='';
								</prop:ClientSide.OnValidationError>
							</com:TCustomValidator>
						</div>
						<!--END PRENOM-->

						<!--BEGIN ADRESSE-->
						<div class="form-group form-group-sm">
							<label class="control-label col-md-3" for="ctl0_CONTENU_PAGE_RechercheCollaborateur_addresseContactGroupement">
								<%=Prado::localize('TEXT_ADRESSE')%>
								<span class="text-danger">*</span>
							</label>
							<div class="col-md-9">
								<com:TTextBox ID="addresseContactGroupement"  cssClass="form-control" Attributes.title="<%=Prado::localize('TEXT_ADRESSE')%>" />
							</div>
							<com:TCustomValidator
									ControlToValidate="addresseContactGroupement"
									ValidationGroup="validateInfosEntrperise"
									Display="Dynamic"
									ID="adresseValidator"
									ErrorMessage="<%=Prado::localize('TEXT_ADRESSE')%>"
									Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMP_OBLIGATOIRE')%>' /></span>" EnableClientScript="true"
									ClientValidationFunction="valideControl">
								<prop:ClientSide.OnValidationError>
									document.getElementById('divValidationSummary').style.display='';
								</prop:ClientSide.OnValidationError>
							</com:TCustomValidator>
						</div>
						<!--END ADRESSE-->

						<!--BEGIN ADRESSE SUITE-->
						<div class="form-group form-group-sm">
							<label class="control-label col-md-3" for="ctl0_CONTENU_PAGE_RechercheCollaborateur_addressSuiteContactGroupement">
								<%=Prado::localize('TEXT_ADRESSE_SUITE')%>
							</label>
							<div class="col-md-9">
								<com:TTextBox ID="addressSuiteContactGroupement"  cssClass="form-control" Attributes.title="<%=Prado::localize('TEXT_ADRESSE_SUITE')%>" />
							</div>
						</div>
						<!--END ADRESSE SUITE-->

						<!--BEGIN CODE POSTAL-->
						<div class="form-group form-group-sm">
							<label class="control-label col-md-3" for="ctl0_CONTENU_PAGE_RechercheCollaborateur_cpContactGroupement">
								<%=Prado::localize('TEXT_CP')%>
								<span class="text-danger">* </span>
							</label>
							<div class="col-md-9">
								<com:TTextBox ID="cpContactGroupement"  cssClass="form-control" Attributes.title="<%=Prado::localize('TEXT_CP')%>"/>
							</div>
							<com:TCustomValidator
									ControlToValidate="cpContactGroupement"
									ValidationGroup="validateInfosEntrperise"
									Display="Dynamic"
									ID="cpValidator"
									ErrorMessage="<%=Prado::localize('TEXT_CP')%>"
									Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMP_OBLIGATOIRE')%>' /></span>" EnableClientScript="true"
									ClientValidationFunction="valideControl">
								<prop:ClientSide.OnValidationError>
									document.getElementById('divValidationSummary').style.display='';
								</prop:ClientSide.OnValidationError>
							</com:TCustomValidator>
						</div>
						<!--END CODE POSTAL-->

					</div>
					<div class="col-md-6">
						<!--BEGIN VILLE-->
						<div class="form-group form-group-sm">
							<label class="control-label col-md-3" for="ctl0_CONTENU_PAGE_RechercheCollaborateur_villeContactGroupement">
								<%=Prado::localize('TEXT_VILLE')%>
								<span class="text-danger">* </span>
							</label>
							<div class="col-md-9">
								<com:TTextBox ID="villeContactGroupement"  cssClass="form-control" Attributes.title="<%=Prado::localize('TEXT_VILLE')%>"/>
							</div>
							<com:TCustomValidator
									ControlToValidate="villeContactGroupement"
									ValidationGroup="validateInfosEntrperise"
									Display="Dynamic"
									ID="villeValidator"
									ErrorMessage="<%=Prado::localize('TEXT_VILLE')%>"
									Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMP_OBLIGATOIRE')%>' /></span>" EnableClientScript="true"
									ClientValidationFunction="valideControl" >
								<prop:ClientSide.OnValidationError>
									document.getElementById('divValidationSummary').style.display='';
								</prop:ClientSide.OnValidationError>
							</com:TCustomValidator>
						</div>
						<!--END VILLE-->

						<!--BEGIN FONCTION-->
						<div class="form-group form-group-sm">
							<label class="control-label col-md-3" for="ctl0_CONTENU_PAGE_RechercheCollaborateur_fonction">
								<%=Prado::localize('DEFINE_FONCTION_MAJ')%>
								<span class="text-danger">* </span>
							</label>
							<div class="col-md-9">
								<com:TTextBox ID="fonction"  cssClass="form-control" Attributes.title="<%=Prado::localize('DEFINE_FONCTION_MAJ')%>"/>
							</div>
							<com:TCustomValidator
									ControlToValidate="fonction"
									ValidationGroup="validateInfosEntrperise"
									Display="Dynamic"
									ID="fonctionValidator"
									ErrorMessage="<%=Prado::localize('DEFINE_FONCTION_MAJ')%>"
									Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMP_OBLIGATOIRE')%>' /></span>" EnableClientScript="true"
									ClientValidationFunction="valideControl">
								<prop:ClientSide.OnValidationError>
									document.getElementById('divValidationSummary').style.display='';
								</prop:ClientSide.OnValidationError>
							</com:TCustomValidator>
						</div>
						<!--END FONCTION-->

						<!--BEGIN MAIL-->
						<div class="form-group form-group-sm">
							<label class="control-label col-md-3" for="ctl0_CONTENU_PAGE_RechercheCollaborateur_email">
								<%=Prado::localize('DEFINE_E_MAIL')%>
								<span class="text-danger">* </span>
							</label>
							<div class="col-md-9">
								<com:TTextBox ID="email"  cssClass="form-control" Attributes.title="<%=Prado::localize('DEFINE_E_MAIL')%>"/>
							</div>
							<com:TCustomValidator
									ControlToValidate="email"
									ValidationGroup="validateInfosEntrperise"
									Display="Dynamic"
									ID="emailValidator"
									ErrorMessage="<%=Prado::localize('DEFINE_E_MAIL')%>"
									Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMP_OBLIGATOIRE')%>' /></span>" EnableClientScript="true"
									ClientValidationFunction="valideControlEmail">
								<prop:ClientSide.OnValidationError>
									document.getElementById('divValidationSummary').style.display='';
								</prop:ClientSide.OnValidationError>
							</com:TCustomValidator>

						</div>
						<!--END MAIL-->

						<!--BEGIN TELEPHONE-->
						<div class="form-group form-group-sm">
							<label class="control-label col-md-3" for="ctl0_CONTENU_PAGE_RechercheCollaborateur_tel">
								<%=Prado::localize('DEFINE_TEL')%>
							</label>
							<div class="col-md-9">
								<com:TTextBox ID="tel"  cssClass="form-control"  Attributes.title="<%=Prado::localize('DEFINE_TEL')%>"/>
							</div>
							<com:TCustomValidator
									ControlToValidate="tel"
									ValidationGroup="validateInfosEntrperise"
									Display="Dynamic"
									ID="telValidator"
									ErrorMessage="<%=Prado::localize('FORMAT_TEL')%>"
									Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CHAMP_OBLIGATOIRE')%>' /></span>"
									ClientValidationFunction="valideControlTel">
								<prop:ClientSide.OnValidationError>
									document.getElementById('divValidationSummary').style.display='';
								</prop:ClientSide.OnValidationError>
							</com:TCustomValidator>
						</div>
						<!--END TELEPHONE-->
					</div>
					<!--END FORMULAIRE-->
				</div>
			</com:TActivePanel>
		</div>
	</div>
  <!--Fin Bloc Recherche de collaborateurs -->
  <!--Fin colonne droite-->
  <!--Fin partie centrale-->

 