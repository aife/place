<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_CommonReferentiel;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_Ref;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_ValeursReferentielles;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_CriteriaVo;
use Prado\Prado;

/**
 * Formulaire de recherche avancee.
 *
 * @author mouslim mitali <mouslim.mitali@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class PanelSearchCompany extends MpeTPage
{
    private string $_postBack = '';
    private string $_callBack = '';
    public string $_calledFrom = '';

    public function setCallBack($value)
    {
        $this->_callBack = $value;
    }

    public function getCallBack()
    {
        return $this->_callBack;
    }

    public function setPostBack($value)
    {
        $this->_postBack = $value;
    }

    public function getPostBack()
    {
        return $this->_postBack;
    }

    public function setCalledFrom($value)
    {
        $this->_calledFrom = $value;
    }

    public function getCalledFrom()
    {
        return $this->_calledFrom;
    }

    public function onLoad($param)
    {
        $this->panelRegionProvince->setPostBack($this->getPostBack());
        $this->panelRegionProvince->setCallBack($this->getCallBack());

        $this->domaineActivite->setCallBack($this->getCallBack());
        $this->qualification->setCallBack($this->getCallBack());

        // Pour l'affichage des codes NACES Lt-Ref
        if (Atexo_Module::isEnabled('CompteEntrepriseCodeNaceReferentiel')) {
            $atexoReferentiel = new Atexo_Ref();
            $atexoReferentiel->setCheminFichierConfigXML(Atexo_Config::getParameter('PATH_FILE_NACE_TOUS_NIVEAUX_CONFIG'));
            $this->atexoReferentiel->afficherReferentiel($atexoReferentiel);
        }
        // Pour l'affichage des domaines d'activites Lt-Ref
        if (Atexo_Module::isEnabled('compteEntrepriseDomaineActiviteLtReferentiel')) {
            $atexoRefDomainesActivites = new Atexo_Ref();
            $atexoRefDomainesActivites->setCheminFichierConfigXML(Atexo_Config::getParameter('PATH_FILE_DOMAINES_ACTIVITES_CONFIG'));
            $this->idAtexoRefDomaineActivite->afficherReferentiel($atexoRefDomainesActivites);
        }
        // Selection des certificats des comptes entreprises Lt-Ref
        if (Atexo_Module::isEnabled('CompteEntrepriseAgrementLtReferentiel')) {
            $atexoCertificatCompteEtpRef = new Atexo_Ref();
            $atexoCertificatCompteEtpRef->setCheminFichierConfigXML(Atexo_Config::getParameter('PATH_FILE_CERTIFICAT_COMPTE_ENTREPRISE_CONFIG'));
            $this->idAtexoCertificatCompteEtpRef->afficherReferentiel($atexoCertificatCompteEtpRef);
        }

        // Selection et affichage des qualifications des comptes entreprises Lt-Ref
        if (Atexo_Module::isEnabled('CompteEntrepriseQualificationLtReferentiel')) {
            $atexoRefQualificatin = new Atexo_Ref();
            $atexoRefQualificatin->setCheminFichierConfigXML(Atexo_Config::getParameter('PATH_FILE_QUALIFICATION_CONFIG'));
            $this->idAtexoRefQualification->afficherReferentiel($atexoRefQualificatin);
        }

        if (!$this->_postBack) {
            $this->ltRefEntr->afficherReferentielEntreprise();
            if (Atexo_Module::isEnabled('BourseALaSousTraitance')) {
                self::fillRepeaterTypeCollaboration();
            }
        }
    }

    public function displayDomaines()
    {
        if (Atexo_Module::isEnabled('CompteEntrepriseDonneesComplementaires')) {
            $this->panelComplementaireSearch->displayDomaines();
        }
    }

    /**
     * Fonction qui recupere les choix effectues dans la partie 'Recherche multi-criteres'
     * pour les mettre dans un objet CriteriaVo puis affiche le resultat
     * dans le tableau de bord.
     */
    public function onSearchClick($sender, $param)
    {
        $criteriaVo = new Atexo_Entreprise_CriteriaVo();
        if ('' != $this->raisonSociale->Text) {
            $criteriaVo->setRaisonSociale($this->raisonSociale->Text);
        }
        if ('' != $this->cpSiegeSocial->Text) {
            $criteriaVo->setCpSiegeSocial($this->cpSiegeSocial->Text);
        }
        if (0 != $this->panelRegionProvince->regionSiegeSocial->getSelectedIndex()) {
            $criteriaVo->setRegion($this->panelRegionProvince->regionSiegeSocial->SelectedValue);
        }
        if (0 != $this->panelRegionProvince->provinceSiegeSocial->getSelectedIndex()) {
            $criteriaVo->setProvince($this->panelRegionProvince->provinceSiegeSocial->SelectedValue);
        }
        if ('' != $this->villeSiegeSocial->Text) {
            $criteriaVo->setVilleSiegeSocial($this->villeSiegeSocial->Text);
        }
        if (true == $this->france->Checked) {
            $criteriaVo->setPays('local'); //pays de la PF
            if (Atexo_Module::isEnabled('CompteEntrepriseRcville')) {
                if ('0' != $this->villeRc->SelectedValue || '' != $this->villeRc->SelectedValue) {
                    if ('0' == $this->villeRc->SelectedValue) {
                        $villeRc = '';
                    } else {
                        $villeRc = $this->villeRc->SelectedValue;
                    }
                    $criteriaVo->setSiren($this->villeRc->SelectedValue.Atexo_Config::getParameter('SEPARATEUR_VILLE_RC').$this->numeroRc->Text);
                }
            } elseif (Atexo_Module::isEnabled('CompteEntrepriseIdentifiantUnique')) {
                if ('' != $this->identifiantUnique->Text) {
                    $criteriaVo->setSiren($this->identifiantUnique->Text);
                }
            } else {
                if ('' != $this->siren->Text) {
                    $criteriaVo->setSiren($this->siren->Text);
                }
                if ('' != $this->siret->Text) {
                    $criteriaVo->setSiret($this->siret->Text);
                }
            }
        } elseif (true == $this->etranger->Checked) {
            if ('0' == $this->listPays->getSelectedItem()->getValue()) {
                $criteriaVo->setPays('etranger');
            } else {
                $criteriaVo->setPays($this->listPays->getSelectedItem()->getText());
            }
            $criteriaVo->setIdNational($this->idNational->Text);
        }
        if (Atexo_Module::isEnabled('CompteEntrepriseCodeApe')) {
            if ('' != $this->apeNafNace_1->Text) {
                $criteriaVo->setApeNafNace($this->apeNafNace_1->Text.$this->apeNafNace_2->Text);
            }
        }
        if (Atexo_Module::isEnabled('CompteEntrepriseCodeNaceReferentiel')) {
            if ('' != $this->atexoReferentiel->codesRefSec->Value) {
                $criteriaVo->setApeNafNace($this->atexoReferentiel->codesRefSec->Value);
            }
        }
        if (Atexo_Module::isEnabled('compteEntrepriseDomaineActiviteLtReferentiel')) {
            if ('' != $this->idAtexoRefDomaineActivite->codesRefSec->Value) {
                $criteriaVo->setDomaineActivite($this->idAtexoRefDomaineActivite->codesRefSec->Value);
            }
        }
        if (Atexo_Module::isEnabled('CompteEntrepriseAgrementLtReferentiel')) {
            if ('' != $this->idAtexoCertificatCompteEtpRef->codesRefSec->Value) {
                $criteriaVo->setAgrements($this->idAtexoCertificatCompteEtpRef->codesRefSec->Value);
            }
        }
        if (Atexo_Module::isEnabled('CompteEntrepriseQualificationLtReferentiel')) {
            if ('' != $this->idAtexoRefQualification->codesRefSec->Value) {
                $criteriaVo->setQualification($this->idAtexoRefQualification->codesRefSec->Value);
            }
        }
        if ('' != $this->keywordSearch->Text) {
            $criteriaVo->setKeywordSearche($this->keywordSearch->Text);
        }
        if (Atexo_Module::isEnabled('CompteEntrepriseDonneesComplementaires')) {
            $idsEntreprises = $this->panelComplementaireSearch->getIdsEntreprises();
            if (is_array($idsEntreprises) && count($idsEntreprises) > 0) {
                $criteriaVo->setIdsEntrepriseIn($idsEntreprises);
                $criteriaVo->setRechercheDomaines(true);
            }
        }
        if (Atexo_Module::isEnabled('CompteEntrepriseDomaineActivite')) {
            $criteriaVo->setDomaineActivite($this->domaineActivite->idsDomaines->Text);
        }
        if (Atexo_Module::isEnabled('CompteEntrepriseQualification')) {
            $criteriaVo->setQualification(base64_decode($this->qualification->idsQualification->Text));
        }
        if (Atexo_Module::isEnabled('CompteEntrepriseAgrement')) {
            $criteriaVo->setAgrements($this->agrements->idsSelectedAgrements->Value);
        }
        if (Atexo_Module::isEnabled('CompteEntrepriseActivationInscriptionParAgent')) {
            $criteriaVo->setCompteActif('1');
        }
        if (Atexo_Module::isEnabled('BourseALaSousTraitance') && 'entreprise' == $this->getCalledFrom()) {
            $typeCollaboration = '';
            foreach ($this->repeaterTypeCollaboration->getItems() as $item) {
                if ($item->type->checked) {
                    $typeCollaboration .= '#'.$item->idType->getValue();
                }
            }
            if ($typeCollaboration) {
                $typeCollaboration .= '#';
                $criteriaVo->setTypeCollaboration($typeCollaboration);
            }
            $criteriaVo->setVisibleBourse(1);
        }
        if (Atexo_Module::isEnabled('StatutCompteEntreprise')) {
            if ($this->entrepriseEA->checked) {
                $criteriaVo->setEntrepriseEA('1');
            }
            if ($this->entrepriseSIAE->checked) {
                $criteriaVo->setEntrepriseSIAE('1');
            }
        }
//    $refObjetInterne = $this->ltRefInterne->getValueReferentielVo();
//    $tabRefVo[$refObjetInterne->getId()] = $refObjetInterne;
//    $refObjet = $this->ltRefCpv->getValueReferentielVo();
//    $tabRefVo[$refObjet->getId()] = $refObjet;
//    $criteriaVo->setReferentielVo($tabRefVo);
        $criteriaVo->setReferentielVo($this->ltRefEntr->getValueReferentielVo());
        $this->Page->setViewState('CriteriaVo', $criteriaVo);
        $this->Page->dataForSearchResult($criteriaVo);
    }

    /**
     * retourner vers la page de recherche avance.
     */
    public function displayPanelSearch()
    {
        if (('Entreprise.RechercheCollaborateur' == $_GET['page']) && Atexo_Module::isEnabled('BourseALaSousTraitance')) {
            $this->response->redirect('index.php?page=Entreprise.RechercheCollaborateur');
        } elseif ('Agent.SearchCompany' == $_GET['page']) {
            $this->response->redirect('index.php?page=Agent.SearchCompany');
        } else {
            $this->response->redirect('index.php?page=Agent.ChoixDestinataireBdFournisseur&id='.Atexo_Util::atexoHtmlEntities($_GET['id']).'&forInvitationConcourir='.Atexo_Util::atexoHtmlEntities($_GET['forInvitationConcourir']).'&IdEchange='.Atexo_Util::atexoHtmlEntities($_GET['IdEchange']));
        }
    }

    /**
     * afficher le formulaire de la recherche
     * avance avec les criteres deja pre-rempli.
     */
    public function displayCriteria($sender, $param)
    {
        $this->Page->panelSearch->setVisible(true);
        $this->Page->panelResultSerch->setVisible(false);
        $criteriaVo = $this->Page->getViewState('CriteriaVo');
        if (is_array($criteriaVo->getReferentielVo()) && count($criteriaVo->getReferentielVo()) > 0) {
            $this->ltRefEntr->afficherReferentielEntreprise(null, $criteriaVo->getReferentielVo(), true);
        }
    }

    /**
     * Adapter le formulaire pour un affichage qui correspond a MPE TGR.
     */
    public function customizeForm()
    {
        if (Atexo_Module::isEnabled('CompteEntrepriseRcville')) {
            $this->panelSiren->setVisible(false);
            $this->panelRc->setVisible(true);
            $this->panelIdentifiantUnique->setVisible(false);
            $this->validatorSiren->setEnabled(false);
            if (!$this->IsPostBack) {
                $this->displayVilleRc();
            }
        } elseif (Atexo_Module::isEnabled('CompteEntrepriseIdentifiantUnique')) {
            $this->panelIdentifiantUnique->setVisible(true);
            $this->panelSiren->setVisible(false);
            $this->panelRc->setVisible(false);
            $this->validatorSiren->setEnabled(false);
        } else {
            $this->panelSiren->setVisible(true);
            $this->panelRc->setVisible(false);
            $this->panelIdentifiantUnique->setVisible(false);
        }
        if (!Atexo_Module::isEnabled('CompteInscritCodeNic')) {
            $this->siret->setDisplay('None');
        } else {
            $this->siret->setDisplay('Dynamic');
        }
        if (!Atexo_Module::isEnabled('CompteEntrepriseCodeApe')) {
            $this->panelCodeApe->setDisplay('None');
        } else {
            $this->panelCodeApe->setDisplay('Dynamic');
        }

        if (Atexo_Module::isEnabled('CompteEntrepriseQualification')) {
            $this->panelQualifications->setDisplay('Dynamic');
        } else {
            $this->panelQualifications->setDisplay('None');
        }
        if (Atexo_Module::isEnabled('CompteEntrepriseAgrement')) {
            $this->panelAgrements->setDisplay('Dynamic');
        } else {
            $this->panelAgrements->setDisplay('None');
        }
        if (Atexo_Module::isEnabled('CompteEntrepriseDomaineActivite')) {
            $this->panelDomainesActivites->setDisplay('Dynamic');
            $this->domaineActivite->validatorDomainesActivites->SetEnabled(false);
        } else {
            $this->domaineActivite->validatorDomainesActivites->SetEnabled(false);
            $this->panelDomainesActivites->setDisplay('None');
        }
        //Pour l'affichage du code NACE en accedant au referentiel commun de gestion des compte entreprise
        if (!Atexo_Module::isEnabled('CompteEntrepriseCodeNaceReferentiel')) {
            $this->panelCodeNaceRef->setDisplay('None');
        } else {
            $this->panelCodeNaceRef->setDisplay('Dynamic');
        }
        //Pour l'affichage des domaines d'activites LT-Ref
        if (Atexo_Module::isEnabled('compteEntrepriseDomaineActiviteLtReferentiel')) {
            $this->panelDomainesActivitesLtRef->setDisplay('Dynamic');
        } else {
            $this->panelDomainesActivitesLtRef->setDisplay('None');
        }
        if (Atexo_Module::isEnabled('CompteEntrepriseAgrementLtReferentiel')) {
            $this->panelCertificats->setDisplay('Dynamic');
        } else {
            $this->panelCertificats->setDisplay('None');
        }
        if (Atexo_Module::isEnabled('CompteEntrepriseQualificationLtReferentiel')) {
            $this->panelQualificationLtRef->setDisplay('Dynamic');
        } else {
            $this->panelQualificationLtRef->setDisplay('None');
        }
        if (Atexo_Module::isEnabled('StatutCompteEntreprise')) {
            $this->panelStatutCompteEntreprise->setVisible('true');
        } else {
            $this->panelStatutCompteEntreprise->setVisible('false');
        }
    }

    public function displayVilleRc()
    {
        $valeur = (new Atexo_CommonReferentiel())->retrieveValeurReferentiel(Atexo_Config::getParameter('VALEUR_REFERENTIEL_VILLE_RC'));
        $data = [];
        $data['0'] = Prado::localize('TEXT_SELECTIONNER').' ...';
        $data['99'] = Prado::localize('PAS_DE_RC');

        if ($valeur) {
            foreach ($valeur as $oneVal) {
                $data[$oneVal->getLibelle2()] = $oneVal->getLibelleValeurReferentiel();
            }
        }
        $this->villeRc->DataSource = $data;
        $this->villeRc->DataBind();
    }

    public function getListeDomaines()
    {
        return $this->panelComplementaireSearch->getListeDomainesActivites();
    }

    public function fillRepeaterTypeCollaboration()
    {
        $dataSource = [];
        $arrayType = (new Atexo_ValeursReferentielles())->getValeurRefByIdRef(Atexo_Config::getParameter('REFERENTIEL_TYPE_COLLABORATION'));
        foreach ($arrayType as $key => $value) {
            $dataSource[$key]['id'] = $key;
            $dataSource[$key]['libelle'] = $value;
        }
        $this->Page->setViewState('arrayTypeCollaboration', $dataSource);
        $this->repeaterTypeCollaboration->DataSource = $dataSource;
        $this->repeaterTypeCollaboration->DataBind();
    }
}
