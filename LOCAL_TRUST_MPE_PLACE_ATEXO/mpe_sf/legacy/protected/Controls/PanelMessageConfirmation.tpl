<!--Debut message de confirmation-->
    <div class="bloc-message form-bloc msg-confirmation alert alert-success msg-confirmation">
        <div class="content">
            <div class="<%=$this->getCssClass()%>"> 
                <!--Debut Message type d'erreur general--> 
                <span>
                    <com:TActiveLabel ID="labelMessage" />
                </span>
                <!--Fin Message type d'erreur general--> 
            </div>
        </div>
    </div>
<!--Fin  message de confirmation-->