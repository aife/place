<?php

namespace Application\Controls;

use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonTComplementFormulaire;
use Application\Propel\Mpe\CommonTComplementFormulairePeer;
use Application\Propel\Mpe\CommonTDossierFormulaire;
use Application\Propel\Mpe\CommonTEditionFormulaire;
use Application\Propel\Mpe\CommonTEditionFormulairePeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_LoggerManager;
use Application\Service\Atexo\Atexo_MpeSf;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Config\Atexo_Config_Exception;
use Application\Service\Atexo\Controller\Atexo_Controller_Front;
use Application\Service\Atexo\FormulaireSub\Atexo_FormulaireSub_ComplementFormulaire;
use Application\Service\Atexo\FormulaireSub\Atexo_FormulaireSub_Dossier;
use Application\Service\Atexo\FormulaireSub\Atexo_FormulaireSub_Echange;
use Application\Service\Atexo\FormulaireSub\Atexo_FormulaireSub_Edition;
use Exception;
use Prado\Prado;

/**
 * Page de gestion des formulaires SUB.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 1.0
 *
 * @since MPE-4.0
 */
class AtexoFormulaireSub extends MpeTTemplateControl
{
    protected $_idHtmlFormulaire;
    protected bool $_varianteAutorisee = false;
    protected $_classFormulaire;
    protected $_refCons;
    protected $_organisme;
    protected $_dossier;
    protected $_nomDocument;
    protected $_listeEditions;
    protected $_idHtlmToggleFormulaire;
    protected bool $_verifyCallBack = false;
    protected string $_callDepuisDemandeComplementTransmis = '0'; //precise si le composant est appele depuis les demandes de complements ou un formulaire sub normal
    protected $_idComplementFormulaire;
    protected $_callFrom; //La page ou est appelee le composant
    protected $_idDosssierPere;

    /**
     * Attribut la valeur.
     */
    public function setIdHtmlFormulaire($value)
    {
        $this->_idHtmlFormulaire = $value;
    }

    /**
     * Recupere la valeur.
     */
    public function getIdHtmlFormulaire()
    {
        return $this->_idHtmlFormulaire;
    }

    /**
     * Attribut la valeur.
     */
    public function setVerifyCallBack($value)
    {
        $this->_verifyCallBack = $value;
    }

    /*
     * Recupere la valeur
     */
    public function getVerifyCallBack()
    {
        return $this->_verifyCallBack;
    }

    /*
     * Attribut la valeur
     */
    public function setVarianteAutorisee($value)
    {
        $this->_varianteAutorisee = $value;
    }

    /**
     * Recupere la valeur.
     */
    public function getVarianteAutorisee()
    {
        return $this->_varianteAutorisee;
    }

    /**
     * Attribut la valeur.
     */
    public function setClassFormulaire($value)
    {
        $this->_classFormulaire = $value;
    }

    /**
     * Recupere la valeur.
     */
    public function getClassFormulaire()
    {
        return $this->_classFormulaire;
    }

    /**
     * Attribut la valeur.
     */
    public function setRefCons($value)
    {
        $this->_refCons = $value;
    }

    /**
     * Recupere la valeur.
     */
    public function getRefCons()
    {
        return $this->_refCons;
    }

    /**
     * Attribut la valeur.
     */
    public function setOrganisme($value)
    {
        $this->_organisme = $value;
    }

    /**
     * Recupere la valeur.
     */
    public function getOrganisme()
    {
        return $this->_organisme;
    }

    /**
     * Attribut la valeur.
     */
    public function setDossier($value)
    {
        $this->_dossier = $value;
    }

    /**
     * Recupere la valeur.
     */
    public function getDossier()
    {
        return $this->_dossier;
    }

    /**
     * Attribut la valeur.
     */
    public function setNomDocument($value)
    {
        $this->_nomDocument = $value;
    }

    /**
     * Recupere la valeur.
     */
    public function getNomDocument()
    {
        return $this->_nomDocument;
    }

    /**
     * Attribut la valeur.
     */
    public function setIdComplementFormulaire($value)
    {
        $this->_idComplementFormulaire = $value;
    }

    /**
     * Recupere la valeur.
     */
    public function getIdComplementFormulaire()
    {
        return $this->_idComplementFormulaire;
    }

    /**
     * Recupere la valeur.
     */
    public function getListeEditions()
    {
        if ($this->getDossier() instanceof CommonTDossierFormulaire) {
            $this->_listeEditions = $this->getDossier()->getEditionsDossier();
        }

        return $this->_listeEditions;
    }

    /**
     * Attribut la valeur.
     */
    public function setListeEditions($value)
    {
        $this->_listeEditions = $value;
    }

    /**
     * Recupere la valeur.
     */
    public function getIdHtlmToggleFormulaire()
    {
        return $this->_idHtlmToggleFormulaire;
    }

    /**
     * Attribut la valeur.
     */
    public function setIdHtlmToggleFormulaire($value)
    {
        $this->_idHtlmToggleFormulaire = $value;
    }

    /**
     * Recupere la valeur.
     */
    public function getCallDepuisDemandeComplementTransmis()
    {
        return $this->_callDepuisDemandeComplementTransmis;
    }

    /**
     * Attribut la valeur.
     */
    public function setCallDepuisDemandeComplementTransmis($value)
    {
        $this->_callDepuisDemandeComplementTransmis = $value;
    }

    /*
     * Attribut la valeur
     */
    public function setCallFrom($value)
    {
        $this->_callFrom = $value;
    }

    /*
     * Recupere la valeur
     */
    public function getCallFrom()
    {
        return $this->_callFrom;
    }

    /**
     * Attribut la valeur.
     */
    public function setIdDosssierPere($value)
    {
        $this->_idDosssierPere = $value;
    }

    /**
     * Recupere la valeur.
     */
    public function getIdDosssierPere()
    {
        return $this->_idDosssierPere;
    }

    /**
     * Chargement de la page.
     */
    public function onLoad($param)
    {
        if ($this->getVerifyCallBack()) {
            if (!$this->Page->IsCallback) {
                $this->chargerEditions();
            }
        } else {
            $this->chargerEditions();
        }
    }

    public function chargerEditions()
    {
        $this->repeaterListeEditions->DataSource = $this->getListeEditions();
        $this->repeaterListeEditions->DataBind();
    }

    /**
     * Construit l'url d'edition du document.
     */
    public function getUrl()
    {
        $url = '?page=Entreprise.PopUpFormulaireEnveloppe';
        if (($this->getDossier() instanceof CommonTDossierFormulaire)) {
            $url .= '&idDossier='.$this->getDossier()->getIdDossierFormulaire().'&id='.$this->getRefCons().'&orgAcronyme='.$this->getOrganisme().'&typeEnv='.$this->getDossier()->getTypeEnveloppe().'&lot='.$this->getDossier()->getIdLot();
            if ($this->getDossier()->getTypeReponse() == Atexo_Config::getParameter('TYPE_DOSSIER_DEMANDE_COMPLEMENT')) {
                if ($this->getIdComplementFormulaire()) {
                    $complement = (new Atexo_FormulaireSub_ComplementFormulaire())->getComplementFormulaireByIdDossier($this->getDossier()->getIdDossierFormulaire());
                    if ($complement instanceof CommonTComplementFormulaire && $complement->getStatutDemande() == Atexo_Config::getParameter('STATUT_DEMANDE_COMPLEMENT_ENVOYEE')) {
                        $url .= '&complementEnvoye=1';
                    }
                }
            }
            if ($this->getIdDosssierPere()) {
                $url .= '&idDossierPere='.$this->getIdDosssierPere();
            }
        }

        return $url;
    }

    /**
     * Repere le nombre de documents.
     */
    public function getNombreDocuments($dossier)
    {
        $nbrDossier = '0';
        if ($dossier instanceof CommonTDossierFormulaire && is_array($dossier->getEditionsDossier())) {
            $nbrDossier = count($dossier->getEditionsDossier());
        }

        return $nbrDossier;
    }

    /**
     * Recupere le statut du dossier.
     */
    public function getStatutDossier($dossier)
    {
        $statut = null;
        if ($dossier instanceof CommonTDossierFormulaire) {
            $statut = match ($dossier->getStatutValidation()) {
                Atexo_Config::getParameter('STATUT_DOSSIER_BROUILLON') => Prado::localize('DEFINE_STATUS').' : '.Prado::localize('TEXT_BROUILLON_NON_CONFORME'),
                Atexo_Config::getParameter('STATUT_DOSSIER_VALIDE') => Prado::localize('DEFINE_STATUS').' : '.Prado::localize('DEFINE_EN_VALIDE_FINI'),
                default => Prado::localize('DEFINE_STATUS').' : '.Prado::localize('TEXT_BROUILLON_NON_CONFORME'),
            };
        }

        return $statut;
    }

    /**
     * Recupere l'image du statut du dossier.
     */
    public function getStatutPictoDossier($dossier)
    {
        $picto = null;
        if ($dossier instanceof CommonTDossierFormulaire) {
            $picto = match ($dossier->getStatutValidation()) {
                Atexo_Config::getParameter('STATUT_DOSSIER_BROUILLON') => 'images/reponse-statut-1sur2.gif',
                Atexo_Config::getParameter('STATUT_DOSSIER_VALIDE') => 'images/reponse-statut-2sur2.gif',
                default => 'images/reponse-statut-1sur2.gif',
            };
        }

        return $picto;
    }

    public function getLibelleFormulaire()
    {
        if ($this->getDossier() instanceof CommonTDossierFormulaire) {
            return $this->getDossier()->getLibelleFormulaire();
        }
    }

    /**
     * Permet de telecharger les documents (editions et pieces jointes).
     */
    public function telechargerEdition($sender, $param)
    {
        $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $edition = CommonTEditionFormulairePeer::retrieveByPK($param->CommandParameter, $connexion);
        if ($edition instanceof CommonTEditionFormulaire) {
            Atexo_Util::downloadLocalFile($edition->getPath(), $edition->getNomFichier());
        }
    }

    /**
     * Permet de recuperer la date de validation.
     */
    public function getDateValidation($dossier)
    {
        if ($dossier instanceof CommonTDossierFormulaire) {
            return Atexo_Util::iso2frnDateTime($dossier->getDateModif(), true);
        }

        return ' - ';
    }

    /**
     * Permet de recuperer les pictos associes aux details d'une edition.
     */
    public function getPictoDetailsEdition($edition, $dossier)
    {
        $picto = match ((new Atexo_FormulaireSub_Edition())->gestionActionEdition($edition, $dossier)) {
            'cas1' => Atexo_Controller_Front::t().'/images/picto-details-inactive.gif',
            'cas2' => Atexo_Controller_Front::t().'/images/picto-attente.gif',
            'cas3' => Atexo_Controller_Front::t().'/images/picto-details.gif',
            default => Atexo_Controller_Front::t().'/images/picto-details-inactive.gif',
        };

        return $picto;
    }

    /**
     * Permet d'ouvrir la pop in de generation d'un document (edition SUB).
     */
    public function ouvrirPopInGeneration($sender, $param)
    {
        $arrayParam = explode('_', $param->CallbackParameter);
        $this->Page->setViewState('idEditionGeneration', $arrayParam[0]);
        $this->Page->setViewState('indexEditionGeneration', $arrayParam[1]);
        $this->Page->setViewState('popInCallFrom', $this->getCallFrom());
        $index = $arrayParam[1];
        $script = "<script>
					  document.getElementById('".$this->Page->scriptJsPopInGenerationEdition->getClientId()."').title='".Prado::localize('DEFINE_GENERER_EDITION')."';
    				  openModal('modal-generation-edition-".$index."','popup-small2',document.getElementById('".$this->Page->scriptJsPopInGenerationEdition->getClientId()."'));
    			  </script>";
        $this->Page->BlocPopInGeneration->render($param->getNewWriter());
        $this->Page->scriptJsPopInGenerationEdition->Text = $script;
    }

    /**
     * @param $ref
     * @param $org
     * @param $status
     *
     * @throws Atexo_Config_Exception
     */
    public function redirectDetailsConsultationClotures($ref, $org, $status)
    {
        $url = Atexo_MpeSf::getUrlDetailsConsultation($ref, 'orgAcronyme='.$org.'&depots&clotures&'.$status);
        $this->response->redirect($url);

        return;
    }

    /**
     * Permet d'envoyer la demande de complement a SUB.
     */
    public function envoyerDemandeComplement($sender, $param)
    {
        $idComplementFormulaire = $this->Page->getViewState('IdComplementFormulaire');
        $ref = $this->Page->getViewState('refConsultation');
        $org = $this->Page->getViewState('org');

        if ($idComplementFormulaire && $ref && $org) {
            //Verifier conditions d'envoi demande de complement
            $dossier = (new Atexo_FormulaireSub_Dossier())->getDossierFormulaireByIdComplement($idComplementFormulaire);
            if ($dossier) {
                $toutesEditionGenerees = true;
                $tousDossiersValidees = true;
                //L'appel du ws de passage du dossier en instruction SUB
                (new Atexo_FormulaireSub_Edition())->verifierSiToutesEditionsGenereesParDossier($dossier->getIdDossierFormulaire(), $tousDossiersValidees, $toutesEditionGenerees);

                if (!$tousDossiersValidees) {
                    self::redirectDetailsConsultationClotures($ref, $org, 'dossierNonVal');
                }
                if (!$toutesEditionGenerees) {
                    self::redirectDetailsConsultationClotures($ref, $org, 'editionNonGen');
                }
                if (!(new Atexo_FormulaireSub_Echange())->demanderValidationIdDossiersDemandeComplementToSUB($dossier->getCleExterneDossier())) {
                    self::redirectDetailsConsultationClotures($ref, $org, 'instructionKO');
                }
                //Debut envoi infos modification dossier interface SUB
                try {
                    $arrayInfos = [];
                    $arrayInfos[0]['binding']['type'] = 'dateType';
                    $arrayInfos[0]['binding']['nom'] = 'AOF_Candidature_Date_Modification_Offre';
                    $arrayInfos[0]['binding']['valeur'] = date('d/m/Y');
                    if ($dossier instanceof CommonTDossierFormulaire) {
                        (new Atexo_FormulaireSub_Echange())->updateDossierInterfaceSub($dossier->getCleExterneDossier(), $arrayInfos);
                    }
                } catch (Exception $e) {
                    $logger = Atexo_LoggerManager::getLogger('publicite');
                    $logger->error("Erreur lors de l'envoi de la requete a l'interface SUB : ".$e->getMessage());
                }
                //Fin Debut envoi infos modification dossier interface SUB
                $_GET['depots'] = 'depots';
            }

            if ($idComplementFormulaire) {
                $connexion = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
                $complementFormulaire = CommonTComplementFormulairePeer::retrieveByPK($idComplementFormulaire, $connexion);
                if ($complementFormulaire instanceof CommonTComplementFormulaire) {
                    //print_r($complementFormulaire);exit;
                    $complementFormulaire->setStatutDemande(Atexo_Config::getParameter('STATUT_DEMANDE_COMPLEMENT_ENVOYEE'));
                    $complementFormulaire->setDateRemisLe(date('Y-m-d H:i:s'));
                    $complementFormulaire->save($connexion);
                    self::redirectDetailsConsultationClotures($ref, $org, 'idCmp='.base64_encode($complementFormulaire->getIdComplementFormulaire()));
                }
            }
        }
    }

    /**
     * Cet évenement permet d'ouvrir la pop in de l'envoi d'un dossier complement.
     *
     * @author khadija CHOUIKA <khadija.chouika@atexo.com>
     *
     * @version 1.0
     *
     * @since 4.9.0
     *
     * @copyright Atexo 2015
     */
    public function ouvrirPopInSuppressionDossier($sender, $param)
    {
        $arrayParam = explode('_', $param->CallbackParameter);
        $this->Page->setViewState('IdComplementFormulaire', $arrayParam[0]);
        $this->Page->setViewState('refConsultation', $arrayParam[1]);
        $this->Page->setViewState('org', $arrayParam[2]);
        $id = $arrayParam[0];
        $script = "<script>
					  document.getElementById('".$this->scriptJsPopInEnvoiDossier->getClientId()."').title='".addslashes((Prado::localize('TRANSMETTRE_OFFRE_MODIFIEE')))."';
    				  openModal('modal-Envoi-Dossier-".$id."','popup-small2',document.getElementById('".$this->scriptJsPopInEnvoiDossier->getClientId()."'));
    			  </script>";
        $this->panelRaffraichissementPopInConfirmation->render($param->getNewWriter());
        $this->scriptJsPopInEnvoiDossier->Text = $script;
    }
}
