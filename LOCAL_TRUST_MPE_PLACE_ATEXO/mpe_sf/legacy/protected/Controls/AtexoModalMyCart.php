<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Entreprise\Atexo_Entreprise_PaniersEntreprises;
use Prado\Prado;

/**
 * Page de creation compte entreprise et compte utilisateur.
 *
 * @author Mouslim MITALI <anis.abid@atexo.com>
 * @copyright Atexo 2017
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class AtexoModalMyCart extends MpeTTemplateControl
{
    public bool $visible = false;
    public string $message = '';

    /**
     * @return mixed
     */
    public function getVisible($checkParents = true)
    {
        return $this->visible;
    }

    /**
     * @param mixed $visible
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

    public function addAction()
    {
        Atexo_Entreprise_PaniersEntreprises::AddConsultationToPanierEntreprise(Atexo_Util::atexoHtmlEntities($_GET['orgAcronyme']), base64_decode($_GET['id']), Atexo_CurrentUser::getIdEntreprise(), Atexo_CurrentUser::getIdInscrit());
        $this->setMessage(str_replace('[__REF_UTILISATEUR_]', '<strong>'.$_GET['refUser'].'</strong>', Prado::localize('CONFIRMATION_ENREGISTREMENT_CONSULTATION_PANIER')));
    }
}
