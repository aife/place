<div aria-labelledby="ui-id-1" role="dialog" tabindex="-1" style="outline: 0px none; z-index: 1002; position: absolute; height: auto; width: 300px; top: -1228px; left: 540.5px; display: block;"
     class="ui-dialog ui-widget ui-widget-content ui-corner-all modal-form popup-small2 popup-800 ui-draggable">
    <div class="ui-dialog-titlebar ui-widget-header ui-corner-all ui-helper-clearfix"><span class="ui-dialog-title" id="ui-id-1">
        <div class="h4">Ajouter une offre</div>
    </span>
        <a role="button" class="ui-dialog-titlebar-close ui-corner-all" href="#">
        <span class="ui-icon ui-icon-closethick">
            Fermer
        </span>
        </a>
    </div>
    <div scrollleft="0" scrolltop="0" class="<%=$this->getModal()%> ui-dialog-content ui-widget-content" style="width: auto; min-height: 51px; height: auto;">
        <!--Debut Formulaire-->
        <div class="form-bloc">
            <div class="top"><span class="left"></span><span class="right"></span></div>
            <div class="content">
                <div class="line">
                    <com:TLabel Text="<%=$this->getMessage()%>"/>
                </div>
            </div>
            <div class="bottom"><span class="left"></span><span class="right"></span></div>
        </div>
        <!--Fin Formulaire-->
        <!--Debut line boutons-->
        <div class="boutons-line">
            <input class="bouton-moyen float-left" value="<%=Prado::localize('DEFINE_ANNULER')%>" title="<%=Prado::localize('DEFINE_ANNULER')%>" onclick="J('.<%=$this->getModal()%>').dialog('destroy');"
                   type="button">
            <com:TActiveButton
                    cssClass="bouton-moyen float-right"
                    onCallBack="fonctionAppeleeApresConfirmation"
                    ActiveControl.CallbackParameter="<%=$this->getPlaceFunctionCallBack()%>_<%=$this->getCallFrom()%>"
                    Text="<%=Prado::localize('TEXT_VALIDER')%>"
                    Attributes.title="<%=Prado::localize('TEXT_VALIDER')%>"
                    Attributes.onclick="J('.<%=$this->getModal()%>').dialog('destroy');"
            />
        </div>
        <!--Fin line boutons-->
    </div>
</div>