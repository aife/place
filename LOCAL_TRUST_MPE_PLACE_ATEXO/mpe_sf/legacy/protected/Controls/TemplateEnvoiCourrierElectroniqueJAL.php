<?php

namespace Application\Controls;

use Application\Library\Propel\Propel;
use Application\Library\Propel\Query\Criteria;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Message;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Publicite\Atexo_Publicite_AnnonceJAL;
use Application\Service\Atexo\Publicite\Atexo_Publicite_CentralePublication;
use Application\Service\Atexo\Publicite\Atexo_Publicite_DestinataireJAL;
use Prado\Prado;

/**
 * Classe FormeEnvoiCourrierElectroniqueJAL.
 *
 * @author SBAI Yassine <yassine.sbai@atexo.com>
 * @copyright Atexo 2008
 *
 * @version 1.0
 *
 * @since MPE-3.0
 */
class TemplateEnvoiCourrierElectroniqueJAL extends MpeTPage
{
    public int $_idAnnonceJAL;
    public bool $_modeAnnonceJAL = true;
    public $_refConsultation;
    public $_org;
    public bool $_postBack = false;

    public function setPostBack($value)
    {
        $this->_postBack = $value;
    }

    public function getPostBack()
    {
        return $this->_postBack;
    }

    public function setIdAnnonceJAL($value)
    {
        $this->_idAnnonceJAL = $value;
    }

    public function getIdAnnonceJAL()
    {
        return $this->_idAnnonceJAL;
    }

    public function setRefConsultation($value)
    {
        $this->_refConsultation = $value;
    }

    public function getRefConsultation()
    {
        return $this->_refConsultation;
    }

    public function setOrg($value)
    {
        $this->_org = $value;
    }

    public function getOrg()
    {
        return $this->_org;
    }

    public function onLoad($param)
    {
        $destAnnonceObjects = (new Atexo_Publicite_DestinataireJAL())->retreiveListDestinataireByIdAnnonceJal($this->_idAnnonceJAL, $this->_org);
        foreach ($destAnnonceObjects as $destAnnonceObject) {
            if ($destAnnonceObject->getStatut() != Atexo_Config::getParameter('ANNONCE_BROUILLON')) {
                $this->_modeAnnonceJAL = false;
            }
        }

        $destinaiareCentralePub = (new Atexo_Publicite_CentralePublication())->retreiveListeDestinataireCentraleByIdAnnonce($this->_idAnnonceJAL);
        foreach ($destinaiareCentralePub as $oneDestinataire) {
            if ($oneDestinataire->getStatut() != Atexo_Config::getParameter('ANNONCE_BROUILLON')) {
                $this->_modeAnnonceJAL = false;
            }
        }

        $PJ = [];
        $c = new Criteria();
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $annonceObject = (new Atexo_Publicite_AnnonceJAL())->retreiveListFormulaireLibreById($this->_idAnnonceJAL, $this->_org);

        if (!$this->getPostBack()) {
            $this->remplirListeMessage($this->_org);
            if ($annonceObject) {
                $this->objet->Text = $annonceObject->getObjet();
                $this->textMail->Text = $annonceObject->getTexte();
            }
        }
        if ($annonceObject) {
            $PJ = $annonceObject->getCommonAnnonceJALPieceJointes($c, $connexionCom);
            if (!$this->getPostBack()) {
                if ($annonceObject->getOptionEnvoi() == Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_SIMPLE')) {
                    $this->courrierElectroniqueSimple->Checked = true;
                }
                if ($annonceObject->getOptionEnvoi() == Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_AVEC_AR')) {
                    $this->courrierElectroniqueContenuIntegralAR->Checked = true;
                }
                if ($annonceObject->getOptionEnvoi() == Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_AVEC_LIEN')) {
                    $this->courrierElectroniqueUniquementLienTelechargementObligatoire->Checked = true;
                }
            }
        } else {
            $this->courrierElectroniqueContenuIntegralAR->Checked = true;
        }
        $this->remplirTableauPJ($PJ);
    }

    public function onDeleteClick($sender, $param)
    {
        $idpj = $param->CommandParameter;
        Atexo_Publicite_AnnonceJAL::DeletePjAnnonceJAL($idpj, $this->_org);
    }

    public function refreshRepeaterPJ($sender, $param)
    {
        //Raffraichissement du tableau PJ:
        $annonceObject = (new Atexo_Publicite_AnnonceJAL())->retreiveListFormulaireLibreById($this->_idAnnonceJAL, $this->_org);
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_ONLY'));
        $c = new Criteria();
        $PJ = [];
        if ($annonceObject) {
            $PJ = $annonceObject->getCommonAnnonceJALPieceJointes($c, $connexionCom);
        }
        $this->remplirTableauPJ($PJ);
        $this->PanelPJ->render($param->NewWriter);
        $this->panelButtonSave->render($param->NewWriter);
    }

    public function remplirTableauPJ($data)
    {
        $this->countPj->Text = is_countable($data) ? count($data) : 0;
        $this->RepeaterPiecesJointes->dataSource = $data;
        $this->RepeaterPiecesJointes->dataBind();
    }

    public function remplirListeMessage($org)
    {
        $dataMessageType = [];
        $resultCriteria = Atexo_Message::getAllTypeMessages();
        $dataMessageType[0] = Prado::localize('TEXT_SELECTIONNER').'...';
        if (is_array($resultCriteria)) {
            foreach ($resultCriteria as $result) {
                $dataMessageType[$result->getId()] = $result->getLibelle();
            }
        }
        $this->messageType->dataSource = $dataMessageType;
        $this->messageType->SelectedValue = Atexo_Config::getParameter('ID_MESSAGE_COURRIER_LIBRE');
        $this->messageType->dataBind();
    }

    public function enregistrer()
    {
        $connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $annonceObject = (new Atexo_Publicite_AnnonceJAL())->retreiveListFormulaireLibreById($this->_idAnnonceJAL, $this->_org);
        $annonceObject->setTexte($this->textMail->text);
        $annonceObject->setObjet($this->objet->Text);
        $annonceObject->setDateCreation(date('Y-m-d m:s'));

        if (true == $this->courrierElectroniqueContenuIntegralAR->Checked) {
            $annonceObject->setOptionEnvoi(Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_AVEC_AR'));
        }
        if (true == $this->courrierElectroniqueUniquementLienTelechargementObligatoire->Checked) {
            $annonceObject->setOptionEnvoi(Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_AVEC_LIEN'));
        }
        if (true == $this->courrierElectroniqueSimple->Checked) {
            $annonceObject->setOptionEnvoi(Atexo_Config::getParameter('COURRIER_ELECTRONIQUE_SIMPLE'));
        }
        if ($annonceObject->save($connexionCom)) {
            $this->response->redirect('?page=Agent.PubliciteConsultation&id='.base64_encode(Atexo_Util::atexoHtmlEntities($_GET['id'])));
        }
    }

    public function RedirectReturn()
    {
        $this->response->redirect('?page=Agent.PubliciteConsultation&id='.base64_encode(Atexo_Util::atexoHtmlEntities($_GET['id'])));
    }
}
