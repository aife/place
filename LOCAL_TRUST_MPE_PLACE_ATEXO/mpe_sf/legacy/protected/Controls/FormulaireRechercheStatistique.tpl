<!--Debut Bloc Suivi des avis publies-->
<div class="form-field">
	<div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
	<div class="content">
		<h3><com:TLabel ID="titreFormulaire"/></h3>
		<!---->
		<div class="line">

			<div class="intitule-auto"><com:TLabel ID="rechercheDates"/><label for="ctl0_CONTENU_PAGE_formulaireRechercheStatistique_dateFin"><com:TTranslate>ENTRE_LE</com:TTranslate></label></div>
			<div class="calendar">
				<com:TTextBox  ID="dateDebut" CssClass="heure" />
				<com:TImage ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-calendar.gif" Attributes.alt="Calendrier" Attributes.title="Calendrier" Attributes.onclick="popUpCalendar(this,document.getElementById('ctl0_CONTENU_PAGE_formulaireRechercheStatistique_dateDebut'),'dd/mm/yyyy','');" />
			</div>
			<div class="intitule-auto indent-10"><label for="ctl0_CONTENU_PAGE_formulaireRechercheStatistique_dateFin"><com:TTranslate>ET_LE</com:TTranslate> </label></div>
			<div class="calendar">
				<com:TTextBox  ID="dateFin" CssClass="heure" />
				<com:TImage ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-calendar.gif" Attributes.alt="Calendrier" Attributes.title="Calendrier" Attributes.onclick="popUpCalendar(this,document.getElementById('ctl0_CONTENU_PAGE_formulaireRechercheStatistique_dateFin'),'dd/mm/yyyy','');" />
			</div>
			<div class="breaker"></div>
		</div>
		<!-- -->
		<div class="spacer-small"></div>
		<!-- -->
		<com:TPanel CssClass="line" ID="panelStatistiquesTranservales">
			<div class="intitule-100 "><label for="entiteAchat"><com:TTranslate>TEXT_LIBELLE_ENTITE_ACHAT</com:TTranslate> :</label></div>
			<div class="content-bloc-auto float-left">
				<com:TDropDownList ID="entiteAchat" CssClass="liste-entites" ToolTip="<%=Prado::localize('TEXT_LIBELLE_ENTITE_ACHAT')%>"/>
				<div class="line no-indent">
					<div class="intitule-auto">
						<com:TCheckBox ID="cumulValeurs" CssClass="check" ToolTip="<%=Prado::localize('TEXT_VALEUR_CUMULE')%>" />
						<label for="cumulValeurs"><com:TTranslate>TEXT_VALEUR_CUMULE</com:TTranslate></label>
					</div>
				</div>
			</div>
			<div class="breaker"></div>
		</com:TPanel>
		<!-- -->
		<div class="spacer"></div>
		<!--Debut line boutons-->
		<div class="boutons-line">
			<input type="button" class="bouton-moyen float-left" value="<%=Prado::localize('DEFINE_ANNULER')%>" title="<%=Prado::localize('DEFINE_ANNULER')%>"  />
			<com:TLabel Visible="<%=($this->getUseAjax()=='true')%>">
				<com:TActiveButton CssClass="bouton-moyen float-right" OnCallBack="recalculStatistique" Text="<%=Prado::localize('TEXT_VALIDER')%>" ToolTip="<%=Prado::localize('VALIDER_LA_SELECTION')%>"  />
			</com:TLabel>
			<com:TLabel Visible="<%=!($this->getUseAjax()=='true')%>">
				<span id="scriptEnabled" style="display:none">
					<com:TLinkButton CssClass="bouton-moyen float-right" OnClick="recalculStatistique" Text="<%=Prado::localize('TEXT_VALIDER')%>" ToolTip="<%=Prado::localize('VALIDER_LA_SELECTION')%>"  />
				</span>
				<span id="scriptDisabled" style="">
					<com:TButton CssClass="bouton-moyen float-right" OnClick="recalculStatistique" Text="<%=Prado::localize('TEXT_VALIDER')%>" ToolTip="<%=Prado::localize('VALIDER_LA_SELECTION')%>"  />
				</span>
			</com:TLabel>
		</div>

		<!--Fin line boutons-->
		<div class="breaker"></div>
	</div>
	<div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
	<script>
		document.getElementById('scriptEnabled').style.display="";
		document.getElementById('scriptDisabled').style.display="none";
	</script>
</div>
<!--Fin Bloc Suivi des avis publies-->
