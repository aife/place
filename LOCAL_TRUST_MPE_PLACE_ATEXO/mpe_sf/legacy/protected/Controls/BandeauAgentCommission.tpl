<!--Debut header-->
<div id="header" <%=(Application\Service\Atexo\Atexo_Module::isEnabled("SocleExternePpp") && (strtolower(Application\Service\Atexo\Atexo_CurrentUser::getRole())=='agenthelios'))?'style="background:url(/themes/images/bandeau_helios.gif) no-repeat;"':""%> >
	<div class="top">
		<com:TPanel id="seDeconnecter" CssClass="user-infos">
		<com:THyperLink CssClass="mon-compte" id="linkMonCompteAgent"><com:TTranslate>DEFINE_TEXT_MON_COMPTE</com:TTranslate></com:THyperLink>
			<com:TLinkButton OnClick="Disconnect" CssClass="deconnexion" Text="<%= Prado::localize('DEFINE_TEXT_SE_DECONNECTER') %>" />
		</com:TPanel>
		<com:TPanel CssClass="logo-client" ID="panelLogo" visible="false" >
			<h1><img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/logo.gif" class="logo" alt="<%=Prado::localize('PLACE_MARCHE_PUBLIC_INTERMINISTERE')%>" title="<%=Prado::localize('PLACE_MARCHE_PUBLIC_INTERMINISTERE')%>" /></h1>
		</com:TPanel>
		<com:TPanel CssClass="logo-client" ID="panelLinklogo" visible="false" >
			<h1><a href="<%= Application\Service\Atexo\Atexo_Config::getParameter('URL_LOGO')%>" class="left" target="_blank"><img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/logo.gif" class="logo" alt="<%= Application\Service\Atexo\Atexo_Config::getParameter('ALT_URL_LOGO')%>" title="<%= Application\Service\Atexo\Atexo_Config::getParameter('TITLE_URL_LOGO')%>" /></a></h1>
		</com:TPanel>
		<com:TImage ImageUrl="<%=Application\Service\Atexo\Atexo_Util::getUrlLogoOrganisme(Application\Service\Atexo\Atexo_CurrentUser::getOrganismAcronym(),true)%>"
					CssClass="logo-organisme" Attributes.alt="Logo Organisme" Attributes.title="Logo Organisme"
					Visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('AfficherImageOrganisme')
					&& Application\Service\Atexo\Atexo_Util::isLogoOrganismeVisible(Application\Service\Atexo\Atexo_CurrentUser::getOrganismAcronym(),true))%>"/>
		<com:TPanel ID="panelNaviguationMulilingue" CssClass="choix-langue">
			<span><com:TTranslate>DEFINE_LANGUE_NAVIGATION</com:TTranslate> : </span>
        	<com:TRepeater ID="langages" EnableViewState="true" >
        		<prop:ItemTemplate>
        			<a onclick="selectFlag(this);" class="<%#$this->TemplateControl->selectFlag($this->Data->getLangue())%>"><com:TImageButton id="flagImg" ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/flag-<%#$this->Data->getLangue()%>.png" CommandName="<%#$this->Data->getLangue()%>" OnCommand="Page.Master.bandeauAgent.writeLanguage" Attributes.alt="<%#strtoupper($this->Data->getLangue())%>" Attributes.title="<%#strtoupper($this->Data->getLangue())%>" /></a>
        		</prop:ItemTemplate>
        	</com:TRepeater>
		    &nbsp;&nbsp;
		</com:TPanel>
	</div>
	<div class="nav">
		<com:TActivePanel id="panelDate" CssClass="date" ActiveControl.EnableUpdate="true" ><com:TActiveLabel id="dateAujourdhui" /></com:TActivePanel>
		<div class="lien-portail"><com:THyperLink id="linkAccueilPortail"  visible="true" ><com:TTranslate>TEXT_PORTAIL</com:TTranslate></com:THyperLink></div>&nbsp;
		<div class="accueil"><com:THyperLink id="linkAccueilAgent"><com:TTranslate>TEXT_MESSAGERIE_ACCUEIL</com:TTranslate></com:THyperLink></div>

		<com:TLabel id="listeServiceMetier" CssClass="services-metiers" >
			<com:TRepeater ID="repeaterListeServices"  >
         		<prop:ItemTemplate>
           			<div class="accueil">
           			<com:THyperLink
           			NavigateUrl="index.php?page=Agent.AccueilAgentAuthentifieSocleinterne"
           			Attributes.title="<%#$this->Data['denomination']%>"
           			>
            		<%#$this->Data['sigle']%></com:THyperLink></div>
         		</prop:ItemTemplate>
            </com:TRepeater>
		</com:TLabel>
		<div class="message-user">
		   <com:TPanel id="panelConnecte">
			<span><com:TTranslate>TEXT_BIENVENUE</com:TTranslate> <strong><%=$this->getNomPrenomUser()%></strong></span>	
		   </com:TPanel>	
		   <com:TPanel id="panelNonConnecte" visible="false">
		     <span><com:TTranslate>TEXT_VOUS_ETES_PAS_AUTHENTIFIE</com:TTranslate></span>
		   </com:TPanel>
		</div>
	</div>
</div>
<!--Fin header-->