<?php

namespace Application\Controls;

use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_CurrentUser;
use Application\Service\Atexo\Atexo_Module;
use Application\Service\Atexo\Atexo_ValeursReferentielles;
use Application\Service\Atexo\Consultation\Atexo_Consultation_Category;
use Application\Service\Atexo\Operations\Atexo_Operations_CriteriaVo;
use Prado\Prado;

/**
 * commentaires.
 *
 * @author khadija Chouika <khadija.chouika@atexo.com>
 * @copyright Atexo 2014
 *
 * @version 6.0
 *
 * @since MPE-4.0
 */
class OperationAdvancedSearch extends MpeTTemplateControl
{
    /**
     * Permet de charger les catégories.
     */
    public function chargerCategories()
    {
        $categories = Atexo_Consultation_Category::retrieveCategories();
        $dataCategories = Atexo_Consultation_Category::retrieveDataCategories($categories, Atexo_CurrentUser::readFromSession('lang'));
        $this->categorie->Datasource = $dataCategories;
        $this->categorie->dataBind();
    }

    /**
     * Permet de charger les types.
     */
    public function chargerType()
    {
        $defautlValue = '---'.Prado::localize('DEFINE_TOUS_LES_TYPES').'---';
        $organisme = Atexo_CurrentUser::getCurrentOrganism();
        $typeArray = (new Atexo_ValeursReferentielles())->retrieveValeursReferentiellesByReference(Atexo_Config::getParameter('REFERENTIEL_ORG_TYPE_OPERATION'), $organisme, null, $defautlValue);
        $this->type->Datasource = $typeArray;
        $this->type->dataBind();
    }

    /**
     * Lancer la recherche.
     */
    public function search($sender, $param)
    {
        $criteria = $this->Page->getViewState('criteria');
        if (!$criteria) {
            $criteria = new Atexo_Operations_CriteriaVo();
        }
        $criteria->setOffset(0);
        $criteria->setLimit(0);
        $criteria->setType($this->type->getSelectedValue());
        $criteria->setCategorie($this->categorie->getSelectedValue());
        $criteria->setAnneeDebut($this->anneeDebut->Text);
        $criteria->setAnneeFin($this->anneeFin->Text);
        $criteria->setkeywordSearch($this->keywordSearch->Text);
        $criteria->setRefOperation('');
        $criteria->setAcronyme(Atexo_CurrentUser::getCurrentOrganism());
        if (Atexo_Module::isEnabled('organisationCentralisee', Atexo_CurrentUser::getCurrentOrganism())) {
            $criteria->setOrganisationCentralisee(true);
        }
        $criteria->setIdService(Atexo_CurrentUser::getCurrentServiceId());
        if ($this->AdvancedSearch_exact->checked) {
            $criteria->setSearchModeExact(true);
        } else {
            $criteria->setSearchModeExact(false);
        }
        $this->Page->setViewState('criteria', $criteria);
        $this->Page->fillRepeaterResut($criteria);
    }

    /**
     * Charger les criteres dans le formulaire de la recherche.
     */
    public function chargerSearch(Atexo_Operations_CriteriaVo $criteria)
    {
        if ($criteria->getType()) {
            $this->type->setSelectedValue($criteria->getType());
        }
        if ($criteria->getCategorie()) {
            $this->categorie->setSelectedValue($criteria->getCategorie());
        }
        if ($criteria->getAnneeDebut()) {
            $this->anneeDebut->Text = $criteria->getAnneeDebut();
        }
        if ($criteria->getAnneeFin()) {
            $this->anneeFin->Text = $criteria->getAnneeFin();
        }
        if ($criteria->getkeywordSearch()) {
            $this->keywordSearch->Text = $criteria->getkeywordSearch();
        }
        if ($criteria->getSearchModeExact()) {
            $this->AdvancedSearch_exact->checked = true;
        }
        if ($criteria->getStatut()) {
            $this->Page->chargerTab($criteria->getStatut());
        }
    }
}
