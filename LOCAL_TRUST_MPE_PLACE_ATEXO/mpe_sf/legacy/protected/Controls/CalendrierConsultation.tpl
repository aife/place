<com:TActivePanel id="bloc_etapeCalendrier">
    <div class="form-field">
        <div class="top "><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
        <div class="content">
            <div class="float-right margin-fix"><com:TTranslate>TEXT_LE_SYMBOLE</com:TTranslate> <span class="champ-oblig">*</span> <com:TTranslate>TEXT_INDIQUE_CHAMPS_OBLIGATOIRES</com:TTranslate></div>
            <div class="breaker"></div>
            <com:TPanel style="<%=Application\Service\Atexo\Atexo_Config::getParameter('ACTIVE_ELABORATION_V2') == '1' ? 'display:none':''%>">
                    <h3 class="float-left"><com:TTranslate>DEFINE_CALENDRIER</com:TTranslate></h3>
                    <div class="bloc-recap-calendrier">
                        <!--Debut Ligne Date limite de remise des plis-->
                        <div class="line">
                            <div class="intitule-180"><label for="dateRemisePlis"><com:TTranslate>DEFINE_DATE_LIMITE_REMIS_PLIS</com:TTranslate></label><span class="champ-oblig">*</span> :</div>
                            <div class="calendar indent-15">
                                <div class="intitule-auto"><com:TTranslate>DEFINE_LE</com:TTranslate></div>
                                <com:TActiveTextBox
                                        AutoPostBack="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('FuseauHoraire', Application\Service\Atexo\Atexo_CurrentUser::getCurrentOrganism()) && $this->getActivationFuseauHoraire()) ? true:false%>"
                                        OnTextChanged="Parent.SourceTemplateControl.remplirDateLimiteRemisePlisLocale"
                                        Attributes.title="<%=Prado::localize('DEFINE_DATE_LIMITE_REMIS_PLIS')%>"
                                        Text=""
                                        id="dateRemisePlis"
                                        CssClass="heure champ-pub"
                                />
                                <com:TImage ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-calendar.gif" Attributes.alt="Calendrier" Attributes.title="Calendrier" Attributes.onclick="popUpCalendar(this,document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeCalendrier_dateRemisePlis'),'dd/mm/yyyy <%=$this->getHeureRemisDesPlisParDefaut()%>','');" />
                                <com:TActiveButton id="bouttonRemplissageDateLimiteRemisePlisLocale" Attributes.style="display:none;" OnClick="SourceTemplateControl.remplirDateLimiteRemisePlisLocale"/>
                                <com:THiddenField ID="dateAujourdhui" value="<%= date('d/m/Y 23:59')%>"/>
                                <com:TRequiredFieldValidator
                                        ControlToValidate="dateRemisePlis"
                                        ValidationGroup="validateInfosConsultation"
                                        Display="Dynamic"
                                        ErrorMessage="<%=Prado::localize('DEFINE_DATE_LIMITE_REMISE_PLI')%>"
                                        EnableClientScript="true"
                                        Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
                                    <prop:ClientSide.OnValidationError>
                                        document.getElementById('divValidationSummary').style.display='';
                                        document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
                                        document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
                                        document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
                                        document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
                                    </prop:ClientSide.OnValidationError>
                                </com:TRequiredFieldValidator>
                                <com:TCustomValidator
                                        ValidationGroup="validateInfosConsultation"
                                        ControlToValidate="dateRemisePlis"
                                        ClientValidationFunction="validateDateTime"
                                        Display="Dynamic"
                                        ErrorMessage="<%=Prado::localize('DEFINE_DATE_LIMITE_REMISE_PLI_NON_VALIDE')%>"
                                        Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
                                        EnableClientScript="true" >
                                    <prop:ClientSide.OnValidationError>
                                        document.getElementById('divValidationSummary').style.display='';
                                        document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
                                        document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
                                        document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
                                        document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
                                    </prop:ClientSide.OnValidationError>
                                </com:TCustomValidator>
                                <com:TCustomValidator
                                        ValidationGroup="validateSave"
                                        ControlToValidate="dateRemisePlis"
                                        ClientValidationFunction="validateDateTime"
                                        Display="Dynamic"
                                        ErrorMessage="<%=Prado::localize('DEFINE_DATE_LIMITE_REMISE_PLI_NON_VALIDE')%>"
                                        Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
                                        EnableClientScript="true" >
                                    <prop:ClientSide.OnValidationError>
                                        document.getElementById('divValidationSummarySave').style.display='';
                                        document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
                                        document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
                                        document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
                                        document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
                                    </prop:ClientSide.OnValidationError>
                                </com:TCustomValidator>
                                <com:TCustomValidator
                                        ValidationGroup="validateInfosConsultation"
                                        ControlToValidate="dateRemisePlis"
                                        ClientValidationFunction="dateAnterieurAujourdhuiFormCons"
                                        Display="Dynamic"
                                        visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('validationFormatChampsStricte'))?'true' : 'false'%>"
                                        ErrorMessage="<%=Prado::localize('DATE_FIN_POSTERIEURE_AUJOURDHUI_ET_DATE_MISE_EN_LIGNE')%>"
                                        Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
                                        EnableClientScript="true" >
                                    <prop:ClientSide.OnValidationError>
                                        document.getElementById('divValidationSummary').style.display='';
                                        document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
                                        document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
                                        document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
                                        document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
                                    </prop:ClientSide.OnValidationError>
                                </com:TCustomValidator>
                            </div>
                            <div class="info-aide-right"><com:TTranslate>DATE_TIME_FORMAT</com:TTranslate></div>
                        </div>
                    </div>
            </com:TPanel>
            <!-- Début bloc fuseau horaire -->
            <com:TActivePanel id="panelDateLimiteRemisePlisLocale">
                <com:TPanel visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('FuseauHoraire', Application\Service\Atexo\Atexo_CurrentUser::getCurrentOrganism()) && $this->getActivationFuseauHoraire()) ? true:false%>">
                    <div class="bloc-recap-calendrier">
                        <div class="line">
                            <div class="intitule-180"><label for="dateRemisePlisLocale"><com:TTranslate>TEXT_DATE_HEURE_LIMITE_REMISE_PLIS_LOCALE</com:TTranslate></label><span class="champ-oblig">*</span> <img title="Info-bulle" alt="Info-bulle" class="picto-info-intitule" onmouseout="cacheBulle('infosHeureLocale')" onmouseover="afficheBulle('infosHeureLocale', this)" src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-info.gif" /> :</div>
                            <div onmouseout="mouseOutBulle();" onmouseover="mouseOverBulle();" class="info-bulle" id="infosHeureLocale"><div><com:TTranslate>DEFINE_INFO_BULLE_HEURE_LOCALE</com:TTranslate></div></div>
                            <div class="calendar indent-15">
                                <div class="intitule-auto"><com:TTranslate>DEFINE_LE</com:TTranslate></div>
                                <com:TActiveTextBox Attributes.title="<%=Prado::localize('TEXT_DATE_HEURE_LIMITE_REMISE_PLIS_LOCALE')%>" id="dateRemisePlisLocale" CssClass="heure" />
                                <com:TImage ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-calendar.gif" Attributes.alt="Calendrier" Attributes.title="Calendrier" Attributes.onclick="popUpCalendar(this,document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeCalendrier_dateRemisePlisLocale'),'dd/mm/yyyy <%= Application\Service\Atexo\Atexo_Config::getParameter('HEURE_CLOTURE_DEFAUT')%>','');" />
                                <com:THiddenField id="decalageFuseauHoraireService" />
                                <com:THiddenField id="dateLimiteFuseauHoraire" />
                                <com:TRequiredFieldValidator
                                        ControlToValidate="dateRemisePlisLocale"
                                        ValidationGroup="validateInfosConsultation"
                                        Display="Dynamic"
                                        ErrorMessage="<%=Prado::localize('TEXT_DATE_HEURE_LIMITE_REMISE_PLIS_LOCALE')%>"
                                        visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('FuseauHoraire', Application\Service\Atexo\Atexo_CurrentUser::getCurrentOrganism()) && $this->getActivationFuseauHoraire())?'true' : 'false'%>"
                                        EnableClientScript="true"
                                        Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
                                    <prop:ClientSide.OnValidationError>
                                        document.getElementById('divValidationSummary').style.display='';
                                        document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
                                        document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
                                        document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
                                        document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
                                    </prop:ClientSide.OnValidationError>
                                </com:TRequiredFieldValidator>
                                <com:TCustomValidator
                                        ValidationGroup="validateInfosConsultation"
                                        ControlToValidate="dateRemisePlisLocale"
                                        ClientValidationFunction="validateDateTime"
                                        Display="Dynamic"
                                        visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('FuseauHoraire', Application\Service\Atexo\Atexo_CurrentUser::getCurrentOrganism()) && $this->getActivationFuseauHoraire())?'true' : 'false'%>"
                                        ErrorMessage="<%=Prado::localize('DEFINE_DATE_LIMITE_REMISE_PLI_LOCALE_NON_VALIDE')%>"
                                        Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
                                        EnableClientScript="true" >
                                    <prop:ClientSide.OnValidationError>
                                        document.getElementById('divValidationSummary').style.display='';
                                        document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
                                        document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
                                        document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
                                        document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
                                    </prop:ClientSide.OnValidationError>
                                </com:TCustomValidator>
                                <com:TCustomValidator
                                        ValidationGroup="validateSave"
                                        ControlToValidate="dateRemisePlisLocale"
                                        ClientValidationFunction="validateDateTime"
                                        Display="Dynamic"
                                        visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('FuseauHoraire', Application\Service\Atexo\Atexo_CurrentUser::getCurrentOrganism()) && $this->getActivationFuseauHoraire())?'true' : 'false'%>"
                                        ErrorMessage="<%=Prado::localize('DEFINE_DATE_LIMITE_REMISE_PLI_LOCALE_NON_VALIDE')%>"
                                        Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
                                        EnableClientScript="true" >
                                    <prop:ClientSide.OnValidationError>
                                        document.getElementById('divValidationSummarySave').style.display='';
                                        document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
                                        document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
                                        document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
                                        document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
                                    </prop:ClientSide.OnValidationError>
                                </com:TCustomValidator>
                            </div>
                        </div>
                        <div class="info-aide-right"><%=Prado::localize('DATE_TIME_FORMAT')%></div>
                        <div class="line">
                            <div class="intitule-bloc bloc-180"><label for="lieuResidence"><com:TTranslate>DEFINE_LIEU_RESIDENCE</com:TTranslate></label><span class="champ-oblig">*</span> :</div>
                            <com:TTextBox Attributes.title="<%=Prado::localize('DEFINE_LIEU_RESIDENCE')%>" Text="" id="lieuResidence" cssclass="auto"/>
                            <com:TRequiredFieldValidator
                                    ControlToValidate="lieuResidence"
                                    ValidationGroup="validateInfosConsultation"
                                    Display="Dynamic"
                                    visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('FuseauHoraire', Application\Service\Atexo\Atexo_CurrentUser::getCurrentOrganism()) && $this->getActivationFuseauHoraire())?'true' : 'false'%>"
                                    ErrorMessage="<%=Prado::localize('DEFINE_LIEU_RESIDENCE')%>"
                                    EnableClientScript="true"
                                    Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
                                <prop:ClientSide.OnValidationError>
                                    document.getElementById('divValidationSummary').style.display='';
                                    document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
                                    document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
                                    document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
                                    document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
                                </prop:ClientSide.OnValidationError>
                            </com:TRequiredFieldValidator>
                        </div>
                    </div>
                </com:TPanel>
            </com:TActivePanel>
            <!-- Fin bloc fuseau horaire -->
            <!--Fin Ligne Date limite de remise des plis-->
            <com:TPanel visible="<%= Application\Service\Atexo\Atexo_Config::getParameter('AFFICHAGE_MESSAGE_RECOMMANDATION_DLRO')%>">
                <div class="spacer"></div>
                <div class="message bloc-700"><com:TTranslate>MESSAGE_RECOMMANDATION_DLRO</com:TTranslate></div>
            </com:TPanel>
            <div class="spacer-mini"></div>
            <com:TActivePanel Cssclass="form-field" ID="panel_lignePanelmiseEnLigne">
                <div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
                <div class="content">
                    <h2><com:TTranslate>DEFINE_REGLES_DE_MISE_EN_LIGNE</com:TTranslate></h2>
                    <div class="line">
                        <com:TActivePanel id="lignePanelmiseEnLigne" Cssclass="intitule-180"><label for="miseEnLigne"><com:TTranslate>DEFINE_DATE_MISE_LIGNE</com:TTranslate></label><span class="champ-oblig">*</span> :</com:TActivePanel>
                        <div class="content-bloc bloc-500">
                            <com:TActivePanel id="panelmiseEnLigne">
                                <div class="intitule-auto">
                                    <com:TActiveRadioButton
                                            id="miseEnLigne"
                                            GroupName="date"
                                            CssClass="radio"
                                    />
                                    <label for="miseEnLigne"><com:TTranslate>DEFINE_LE</com:TTranslate></label>
                                </div>
                                <div class="calendar">
                                    <label for="datemiseEnLigne" style="display:none;"><com:TTranslate>DEFINE_DATE_MISE_LIGNE</com:TTranslate></label>
                                    <com:TActiveTextBox  Text="" ID="datemiseEnLigne" CssClass="heure" />
                                    <com:TImage ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-calendar.gif" Attributes.alt="Calendrier" Attributes.title="Calendrier" Attributes.onclick="popUpCalendar(this,document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeCalendrier_datemiseEnLigne'),'dd/mm/yyyy 16:30','');" />
                                    <com:TCustomValidator
                                            ValidationGroup="validateInfosConsultation"
                                            ControlToValidate="datemiseEnLigne"
                                            ClientValidationFunction="checkDatemiseEnLigneFormCons"
                                            Display="Dynamic"
                                            ErrorMessage="<%=Prado::localize('DEFINE_DATE_MISE_LIGNE')%>"
                                            Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
                                            EnableClientScript="true" >
                                        <prop:ClientSide.OnValidationError>
                                            document.getElementById('divValidationSummary').style.display='';
                                            document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
                                            document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
                                            document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
                                            document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
                                        </prop:ClientSide.OnValidationError>
                                    </com:TCustomValidator>
                                </div>
                                <div class="info-aide-right"><com:TTranslate>DATE_TIME_FORMAT</com:TTranslate></div>
                                <div class="spacer-mini"></div>
                            </com:TActivePanel>
                            <com:TActivePanel id="panelvalidationDate">
                                <div CssClass="intitule-auto clear-both">
                                    <com:TActiveRadioButton
                                            id="validationDate"
                                            GroupName="date"
                                            CssClass="radio"
                                            Attributes.title="<%=Prado::localize('DEFINE_DATE_VALIDATION')%>"
                                    />
                                    <label for="validationDate">
                                        <com:TTranslate>DEFINE_DATE_VALIDATION</com:TTranslate>
                                    </label>
                                </div>
                                <div class="spacer-mini"></div>
                            </com:TActivePanel>
                            <com:TActivePanel id="paneldateEnvoiBOAMP">
                                <div CssClass="intitule-auto clear-both">
                                    <com:TActiveRadioButton
                                            id="dateEnvoiBOAMP"
                                            GroupName="date"
                                            CssClass="radio"
                                            Attributes.title="<%=Prado::localize('DATE_ENVOI_BOAMP')%>"
                                    />
                                    <label for="dateEnvoiBOAMP">
                                        <com:TTranslate>DATE_ENVOI_BOAMP</com:TTranslate>
                                    </label>
                                </div>
                                <div class="spacer-mini"></div>
                            </com:TActivePanel>
                            <com:TActivePanel id="paneldatePubBOAMP" >
                                <div CssClass="intitule-auto clear-both">
                                    <com:TActiveRadioButton
                                            id="datePubBOAMP"
                                            GroupName="date"
                                            CssClass="radio"
                                            Attributes.title="<%=Prado::localize('DATE_PUBLICATION_BOAMP')%>"
                                    />
                                    <label for="datePubBOAMP">
                                        <com:TTranslate>DATE_PUBLICATION_BOAMP</com:TTranslate>
                                    </label>
                                </div>
                                <div class="spacer-mini"></div>
                            </com:TActivePanel>
                            <com:TActivePanel id="panelmiseEnLigneParEntiteCoordinatrice">
                                <div class="intitule-auto">
                                    <com:TActiveRadioButton
                                            id="miseEnLigneParEntiteCoordinatrice"
                                            GroupName="date"
                                            CssClass="radio"
                                    />
                                    <label for="miseEnLigneParEntiteCoordinatrice">
                                        <com:TTranslate>DEFINE_DATE_MISE_EN_LIGNE_ECO</com:TTranslate>
                                    </label>
                                </div>
                                <div class="calendar">
                                    <com:TActiveTextBox  Text="" ID="dateMiseEnLigneParEntiteCoordinatrice" CssClass="heure" />
                                    <com:TImage ImageUrl="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-calendar.gif" Attributes.alt="Calendrier" Attributes.title="Calendrier" Attributes.onclick="popUpCalendar(this,document.getElementById('ctl0_CONTENU_PAGE_bloc_etapeCalendrier_dateMiseEnLigneParEntiteCoordinatrice'),'dd/mm/yyyy','');" />
                                    <com:TCustomValidator
                                            ValidationGroup="validateInfosConsultation"
                                            ControlToValidate="dateMiseEnLigneParEntiteCoordinatrice"
                                            ClientValidationFunction="VerifierFormatDateValidationEcoFormCons"
                                            Enabled="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('RegleMiseEnLigneParEntiteCoordinatrice'))? true:false%>"
                                            Display="Dynamic"
                                            ErrorMessage="<%=Prado::localize('VALIDATION_ENTITE_COORDINATRICE_DATE_MISE_LIGNE_SOUHAITEE')%>"
                                            Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
                                            EnableClientScript="true" >
                                        <prop:ClientSide.OnValidationError>
                                            document.getElementById('divValidationSummary').style.display='';
                                            document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
                                            document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
                                            document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
                                            document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
                                        </prop:ClientSide.OnValidationError>
                                    </com:TCustomValidator>
                                </div>
                                <div class="info-aide-right"><com:TTranslate>DEFINE_FORMAT_DATE_SIMPLE</com:TTranslate></div>
                                <div class="spacer-mini"></div>
                            </com:TActivePanel>
                        </div>
                        <com:TActivePanel Id="panelAvertissementMiseEnLigne" >
                            <com:ActivePanelMessageAvertissement id="panelAvertissement"  displayStyle="Dynamic" />
                        </com:TActivePanel>
                    </div>
                    <!--Fin Ligne Date-->
                    <div class="breaker"></div>
                </div>
                <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
                <com:THiddenField id="jsFile"></com:THiddenField>
            </com:TActivePanel>
            <com:TPanel Visible ="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('CalendrierDeLaConsultation'))? 'true' : 'false' %>">
                <com:AtexoCalendrier id="calednrier" calendrierPrevisionel="true"></com:AtexoCalendrier>
            </com:TPanel>
            <com:THiddenField id="refConsultation"  />
        </div>
    </div>
</com:TActivePanel>