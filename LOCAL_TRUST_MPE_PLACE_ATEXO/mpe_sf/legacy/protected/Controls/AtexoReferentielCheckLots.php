<?php

namespace Application\Controls;

use Application\Library\Propel\Connection\PropelPDO;
use Application\Library\Propel\Propel;
use Application\Propel\Mpe\CommonConsultationPeer;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Referentiel\Atexo_Referentiel_Referentiel;

/**
 * commentaires.
 *
 * @author Hajar HANNAD <hajar.hannad@atexo.com>
 * @copyright Atexo 2011
 *
 * @version 1.0
 *
 * @since MPE-4.0
 */
class AtexoReferentielCheckLots extends AtexoReferentielZoneText
{
    private PropelPDO $_connexionCom;
    private $check = false;
    private $ref;

    public function setRef($ref)
    {
        $this->ref = $ref;
    }

    public function getRef()
    {
        return $this->ref;
    }

    public function setCheck($check)
    {
        $this->check = $check;
    }

    public function getCheck()
    {
        return $this->check;
    }

    public function afficherZoneLot($consultationId = null, $lot = null, $organisme = null, $defineValue = null, $postback = false)
    {
        if (!$this->Page->isPostBack || $postback) {
            self::remplirRepeaterLot($this->getRef());
            foreach ($this->repeaterLot->Items as $Item) {
                $IdRef = $Item->iDLibelle->Text;
                $codeRefPrinc = null;
                if (!$defineValue) {
                    if ($organisme) {
						$refCons = (new Atexo_Referentiel_Referentiel())->getCommonLtReferentielByRefConsultationAndLot($consultationId, $Item->Data['lot'], $IdRef, $organisme);
                        if ($refCons) {
                            $codeRefPrinc = $refCons->getValeurPrincipaleLtReferentiel();
                        }
                    }
                } else {
                    if ($defineValue[$IdRef]) {
                        $codeRefPrinc = $defineValue[$IdRef]->getValuePrinc();
                    }
                }
                $Item->labelReferentielZoneText->SetText("$codeRefPrinc");
                $Item->idReferentielZoneText->SetText("$codeRefPrinc");
                $Item->oldValue->SetValue((new Atexo_Config())->toPfEncoding($codeRefPrinc));
            }
        }
    }

    public function remplirRepeaterLot($ref)
    {
        $this->_connexionCom = Propel::getConnection(Atexo_Config::getParameter('COMMON_DB').Atexo_Config::getParameter('CONST_READ_WRITE'));
        $CommonConsultation = CommonConsultationPeer::retrieveByPK(base64_decode(Atexo_Util::atexoHtmlEntities($_GET['id'])), $this->_connexionCom);
        $dataLots = $CommonConsultation->getAllLots();
        $arrayDataSource = [];
        $index = 0;
        if (is_array($dataLots) && count($dataLots)) {
            foreach ($dataLots as $oneLot) {
                $arrayDataSource[$index]['lot'] = $oneLot->getLot();
                $arrayDataSource[$index]['codeLibelle'] = $ref->getCodeLibelle();
                $arrayDataSource[$index]['obligatoire'] = $ref->getObligatoire();
                $arrayDataSource[$index]['mode'] = $ref->getModeModification();
                $arrayDataSource[$index]['id'] = $ref->getId();
                ++$index;
            }
        }
        $this->repeaterLot->DataSource = $arrayDataSource;
        $this->repeaterLot->DataBind();
    }
}
