<com:TRepeater ID="repeaterInformationsChorus" DataSource="<%=$this->displayList()%>">
            <prop:HeaderTemplate>
                <com:TConditional condition="$this->SourceTemplateControl->getAfficherHeader()">
                    <prop:trueTemplate>
                <table class="table-results donnes-simples" summary="<%=Prado::localize('DEFINE_INFORMATIONS_CHORUS')%>">
                    <thead>
                    <tr class="<%#$this->SourceTemplateControl->getClasseCssTrHead()%>">
                        <th class="col-90" scope="col"><com:TTranslate>DEFINE_FLUX</com:TTranslate></th>
                        <th class="col-90" scope="col"><com:TTranslate>TEXT_NUMERO_EJ</com:TTranslate> </th>
                        <th class="col-90" scope="col"><com:TTranslate>TEXT_STATUT_EJ</com:TTranslate> </th>
                        <th class="col-130" scope="col"><com:TTranslate>DEFINE_DATE_MISE_JOUR</com:TTranslate> </th>
                        <com:TConditional condition="$this->SourceTemplateControl->getAfficherFichierFso()">
                            <prop:trueTemplate>
                                <th scope="col" class="col-400"><com:TTranslate>DEFINE_FICHIER_TECHNIQUE_FSO</com:TTranslate></th>
                            </prop:trueTemplate>
                        </com:TConditional>
                        <com:TConditional condition="$this->SourceTemplateControl->getAfficherColAction()">
                            <prop:trueTemplate>
                                <th class="actions" scope="col">&nbsp;</th>
                            </prop:trueTemplate>
                        </com:TConditional>
                    </tr>
                    </thead>
                    </prop:trueTemplate>
                </com:TConditional>
            </prop:HeaderTemplate>

    <prop:ItemTemplate>
        <tr class="<%#$this->SourceTemplateControl->getClasseCssTrBody()%><%#(($this->ItemIndex%2==0)? '':' on')%> ">
            <td><%#$this->SourceTemplateControl->getFluxChorus()%></td>
            <td><%#($this->Data['numero_marche'])? $this->Data['numero_marche']:'-'%></td>
            <td><%#$this->SourceTemplateControl->displayStatutEJ($this->Data['statut_ej'])%></td>
            <td><%#($this->Data['date_heure'])?  Application\Service\Atexo\Atexo_Util::iso2frnDateTime($this->Data['date_heure'], true): '-'%></td>
            <com:TConditional condition="$this->SourceTemplateControl->getAfficherFichierFso()">
                <prop:trueTemplate>
                    <com:TConditional condition="$this->Data['nom_fichier']">
                        <prop:trueTemplate>
                            <td class="col-piece ellipsis"><div class="blue piece-type">
                                    <a href="javascript:popUp('index.php?page=Agent.PopupDetailChorusFSO&fso=<%#base64_encode($this->Data['nom_fichier'])%>&numMarche=<%#base64_encode($this->Data['numero_marche'])%>&org=<%#base64_encode($this->Data['organisme'])%>','yes');">
                                        <abbr title="<%#$this->Data['nom_fichier']%>">
                                            <%#$this->SourceTemplateControl->tronquerNomFichier($this->Data['nom_fichier'], '68')%>
                                        </abbr>
                                    </a>
                                </div>
                            </td>
                        </prop:trueTemplate>
                        <prop:falseTemplate>
                            <td class="col-piece ellipsis">-</td>
                        </prop:falseTemplate>
                    </com:TConditional>
                </prop:trueTemplate>
            </com:TConditional>
            <com:TConditional condition="$this->SourceTemplateControl->getAfficherColAction()">
                <prop:trueTemplate>
                    <td class="actions" scope="col">&nbsp;</td>
                </prop:trueTemplate>
            </com:TConditional>
        </tr>
    </prop:ItemTemplate>
    <prop:FooterTemplate>
        <com:TConditional condition="$this->SourceTemplateControl->getAfficherFooter()">
            <prop:trueTemplate>
                </table>
            </prop:trueTemplate>
        </com:TConditional>
    </prop:FooterTemplate>

</com:TRepeater>