<?php

namespace Application\Controls;

use Application\Propel\Mpe\CommonConsultation;
use Application\Service\Atexo\Atexo_Config;
use Application\Service\Atexo\Atexo_Util;
use Application\Service\Atexo\Atexo_ValeursReferentielles;
use Application\Service\Atexo\Signature\Atexo_Signature_FichierVo;
use Prado\Prado;

/**
 * Page de gestion des formulaires de réponse pour les pièces libres.
 *
 * @author Oumar KONATE <oumar.konate@atexo.com>
 * @copyright Atexo 2013
 *
 * @version 1.0
 *
 * @since MPE-4.0
 */
class AtexoReponsePiecesLibres extends MpeTTemplateControl
{
    public $_idHtlmToggleFormulaire;
    public $_typeEnv;
    public $_numLot;

    /**
     * Permet de recuperer la consultation dans la séssion de la page.
     */
    public function getConsultation()
    {
        return $this->Page->getViewState('consultation');
    }

    /**
     * Recupère la valeur.
     */
    public function getIdHtlmToggleFormulaire()
    {
        $idHtlmToggleFormulaire = $this->_idHtlmToggleFormulaire;
        if (!$idHtlmToggleFormulaire) {
            $idHtlmToggleFormulaire = $this->sourceTemplateControl->hiddenIdHtlmToggleFormulaire->Value;
        }

        return $idHtlmToggleFormulaire;
    }

    /**
     * Attribut la valeur.
     */
    public function setIdHtlmToggleFormulaire($value)
    {
        $this->_idHtlmToggleFormulaire = $value;
    }

    /**
     * Recupère la valeur.
     */
    public function getTypeEnv()
    {
        $typeEnv = $this->_typeEnv;
        if (!$typeEnv) {
            $typeEnv = $this->sourceTemplateControl->typeEnveloppe->Value;
        }

        return $typeEnv;
    }

    /**
     * Affectes la valeur.
     */
    public function setTypeEnv($value)
    {
        $this->_typeEnv = $value;
    }

    /**
     * Recupère la valeur.
     */
    public function getNumLot()
    {
        $numLot = $this->_numLot;
        if (!$numLot) {
            $numLot = $this->sourceTemplateControl->numLot->Value;
        }

        return $numLot;
    }

    /**
     * Affectes la valeur.
     */
    public function setNumLot($value)
    {
        $this->_numLot = $value;
    }

    public function getSignatureRequis()
    {
        if ($this->getConsultation() instanceof CommonConsultation) {
            return $this->getConsultation()->getSignatureOffre();
        }
    }

    /**
     * Chargement de la page.
     */
    public function onLoad($param)
    {
    }

    /**
     * Permet d'ajouter une pièce libre.
     *
     * @param unknown_type $sender
     * @param unknown_type $param
     */
    public function ajouterPieceLibre($sender, $param)
    {
        $listNameAddedFile = [];
        $arrayPiece = $this->getDataSource();
        $arraNameFiles = [];
        $erreurAjoutFile = false;
        $json = $this->listeJsonRetoursAppletPiecesSelectionnees->Value;
        //La fonction json_decode doit recevoir un json encodé en utf8
        $json = Atexo_Util::httpEncodingToUtf8($json);
        $listeJsonPieces = json_decode($json, true, 512, JSON_THROW_ON_ERROR);
        if (is_array($listeJsonPieces['fichiers'])) {
            foreach ($listeJsonPieces['fichiers'] as $file) {
                $nomFichier = $this->getNomFichier($file['cheminFichier']);
                $nomFichierExiste = $this->Page->isNomFichierExiste($this->getTypeEnveloppe(), $this->getNumLot(), $nomFichier);
                if ($nomFichierExiste) {
                    $arraNameFiles[] = $nomFichier;
                    $erreurAjoutFile = true;
                } else {
                    $fichierVo = new Atexo_Signature_FichierVo($file);
                    $fichierVo->setIdentifiant($this->Page->getLastIdFichier());
                    $fichierVo->setTypeEnveloppe($this->getTypeEnveloppe());
                    $fichierVo->setNumeroLot($this->getNumLot());
                    $fichierVo->setNomFichier($nomFichier);
                    $fichierVo->setTypeFichier(Atexo_Config::getParameter('TYPE_DOCUMENT_FICHIER'));
                    $fichierVo->setOrgineFichier(Atexo_Config::getParameter('TYPE_DOCUMENT_PIECE_LIBRE'));
                    $fichierVo->setInfosSignature(Atexo_Util::arrayToHttpEncoding($file['infosSignature']));
                    $fichierVo->setCheminSignatureXML($file['cheminSignatureXML']);
                    $arrayPiece[$fichierVo->getIdentifiant()] = $fichierVo;
                    $listNameAddedFile[] = $nomFichier;
                }
            }
        }

        if ($erreurAjoutFile) {
            $this->Page->afficherMessageErreurAjoutFile($arraNameFiles, $param);
        }
        $this->setDataSource($arrayPiece);
        //Chargement des pièces de la réponse et raffraichissement
        $this->chargerPieces();
        $this->saveTraceInscrit('DESCRIPTION13', $listNameAddedFile);
        $this->refresh($sender, $param);
    }

    public function refresh($sender, $param)
    {
        $this->chargerPieceLibre();
        $this->panelListePiecesLibres->render($param->getNewWriter());
    }

    public function chargerPieceLibre()
    {
        //print_r($this->getDataSource());
        $this->listePiecesLibres->DataSource = $this->getDataSource();
        $this->listePiecesLibres->DataBind();
    }

    public function getDataSource()
    {
        $arrayPiece = $this->Page->getViewState('dataSourcePieces');

        return $arrayPiece[$this->getTypeEnveloppe().'_'.$this->getNumLot()]['pieceLibre'];
    }

    public function setDataSource($arrayPiece)
    {
        $dataSource = $this->Page->getViewState('dataSourcePieces');
        $dataSource[$this->getTypeEnveloppe().'_'.$this->getNumLot()]['pieceLibre'] = $arrayPiece;
        $this->Page->setViewState('dataSourcePieces', $dataSource);
    }

    public function SupprimerPieceLibre($sender, $param)
    {
        $nomFichier = [];
        $dataSource = $this->getDataSource();
        $index = $param->CallbackParameter;
        echo 'index element '.$index."\n";
        $nomFichier[] = Atexo_Util::utf8ToIso($dataSource[$index]->getNomFichier());
        unset($dataSource[$index]);
        $this->setDataSource($dataSource);
        //Permet de charger les pièces de la réponse
        $this->chargerPieces();
        $this->refresh($sender, $param);
        $this->saveTraceInscrit('DESCRIPTION15', $nomFichier);
    }

    public function getTypeEnveloppe()
    {
        return $this->sourceTemplateControl->typeEnveloppe->Value;
    }

    /**
     * Permet de recuperer toutes les pieces libres cochées.
     */
    public function recupererPiecesCochees(&$listPiecesCoches)
    {
        $dataKeys = $this->listePiecesLibres->getDataKeys();
        foreach ($this->listePiecesLibres->getItems() as $itemEdition) {
            if ($itemEdition->idCheckBoxSelectionPiece->Checked) {
                $listPiecesCoches[] = $this->getFichierFromDataSource($dataKeys[$itemEdition->getItemIndex()]);
            }
        }
    }

    /**
     * Permet de recuperer un fichier de dataSource.
     *
     * @param int $identifiant de l'objet Atexo_Signature_FichierVo
     *
     * @return objet Atexo_Signature_FichierVo
     */
    public function getFichierFromDataSource($identifiant)
    {
        $arrayPiece = $this->getDataSource();

        return $arrayPiece[$identifiant];
    }

    /**
     * Recupère l'image du statut du dossier.
     */
    public function getStatutPictoDossier($dossier)
    {
        return $this->SourceTemplateControl->getStatutPictoDossier($dossier);
    }

    /**
     * Recupère le statut du dossier.
     */
    public function getStatutDossier($dossier)
    {
        return $this->SourceTemplateControl->getStatutDossier($dossier);
    }

    /**
     * Construit le name de la case pour cocher les pièces.
     */
    public function getNameSelectBox($numLot, $itemIndex)
    {
        if ($this->getTypeEnv() == Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE')) {
            return 'dossiersCandidature_lot_'.$numLot.'_piece_'.$itemIndex;
        } elseif ($this->getTypeEnv() == Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE')) {
            return 'dossiersOffreTech_lot_'.$numLot.'_piece_'.$itemIndex;
        } elseif ($this->getTypeEnv() == Atexo_Config::getParameter('TYPE_ENV_OFFRE')) {
            return 'dossiersOffre_lot_'.$numLot.'_piece_'.$itemIndex;
        } elseif ($this->getTypeEnv() == Atexo_Config::getParameter('TYPE_ENV_ANONYMAT')) {
            return 'dossiersOffreAnonymat_lot_'.$numLot.'_piece_'.$itemIndex;
        }
    }

    /**
     * Recupère l'emetteur et le signataire (CN, AC).
     */
    public function getInfosSignataire($fichier)
    {
        return $this->SourceTemplateControl->getInfosSignataire($fichier);
    }

    /**
     * Recupère l'image du picto de la verification de la signature.
     */
    public function getPictoVerifSignature($fichier)
    {
        return $this->SourceTemplateControl->getPictoVerifSignature($fichier);
    }

    /**
     * fichier a une signature.
     */
    public function getHasFileSignature($fichier)
    {
        return $this->SourceTemplateControl->getHasFileSignature($fichier);
    }

    /**
     * Recupère l'image du picto si signature existe ou pas.
     */
    public function getPictoSignatureExiste($fichier)
    {
        return $this->SourceTemplateControl->getPictoSignatureExiste($fichier);
    }

    /**
     * Recupère le message alt du picto de la verification de la signature.
     */
    public function getAltVerifSignature($fichier)
    {
        return $this->SourceTemplateControl->getAltVerifSignature($fichier);
    }

    /**
     * Recupère le message alt du picto si signature existe ou pas.
     */
    public function getAltSignatureExiste($fichier)
    {
        return $this->SourceTemplateControl->getAltSignatureExiste($fichier);
    }

    /**
     * Recupère l'image du picto si signature existe ou pas.
     */
    public function afficherLienSignature($fichier)
    {
        return $this->SourceTemplateControl->afficherLienSignature($fichier);
    }

    /**
     * Permet de charger les pièces des éditions, les pièces libres et les pièces typées.
     */
    public function chargerPieces()
    {
        //Chargement des pièces libres
        $this->chargerPieceLibre();
        //Chargement des éditions
        $this->SourceTemplateControl->atexoReponseFormulaireSub->remplirEditionsFormulaire();
    }

    /**
     * Permet de gerer la visibilité de la case à cocher pour la signature des fichiers de réponse.
     */
    public function getVisibiliteCocherSignature($fichier)
    {
        return $this->SourceTemplateControl->getVisibiliteCocherSignature($fichier);
    }

    /**
     * Permet d'extraire le nom du fichier de la chaine contenant le chemin complet du fichier.
     */
    public function getNomFichier($cheminFicher)
    {
        $nomFichier = '';
        $arrayCheminFichier = explode('\\', $cheminFicher);
        if (is_array($arrayCheminFichier) && count($arrayCheminFichier)) {
            $nomFichier = $arrayCheminFichier[count($arrayCheminFichier) - 1];
        }

        return $nomFichier;
    }

    /**
     * Permet d'afficher les détails de la signature.
     */
    public function afficherDetailsSignature($sender, $param)
    {
        if ($param->CallbackParameter || 0 === $param->CallbackParameter) {
            $dataKeys = $this->listePiecesLibres->getDataKeys();
            $fichier = $this->getFichierFromDataSource($dataKeys[$param->CallbackParameter]);
            if ($fichier instanceof Atexo_Signature_FichierVo) {
                //L'information fichierVo sera exploitée lors de l'appel du composant <com:AtexoDetailsSignatureFormulaireReponse/>
                //dans le fichier AtexoReponseEnveloppe.tpl
                $this->Page->setViewState('fichierVo', $fichier);
                $this->Page->setViewState('identifiantFichier', $fichier->getIdentifiant());
                $script = "<script>
            				  openModal('modal-ajout-offre_".$fichier->getIdentifiant()."','popup-800',document.getElementById('".$this->Page->scriptJsPopInDetailsSignature->getClientId()."'));
            			  </script>";
            }
            //print_r($this->Page->getViewState("signatureFichier"));exit;
            $this->Page->raffraichirListeFormulaires($sender, $param);
            $this->Page->panelDetailsSignatureFormulaireReponse->render($param->getNewWriter());
            $this->Page->scriptJsPopInDetailsSignature->Text = $script;
        }
    }

    /* Permet de préciser si la consultation contient
    * un DC3 (Acte engagement)
    * @return true: si oui, false sinon
    */
    public function isConsultationHasDc3()
    {
        if ($this->getConsultation() instanceof CommonConsultation && '1' == $this->getConsultation()->getSignatureActeEngagement()) {
            return true;
        }

        return false;
    }

    /* Permet de retourner la css de la ligne du repeater
     * $index  boolean
     * @return  on ou vide
     */
    public function getClassLigne($index)
    {
        if ($this->isConsultationHasDc3() && ($this->getTypeEnveloppe() == Atexo_Config::getParameter('TYPE_ENV_OFFRE')) && $index) {
            return 'on';
        } elseif ($this->isConsultationHasDc3() && ($this->getTypeEnveloppe() == Atexo_Config::getParameter('TYPE_ENV_OFFRE')) && !$index) {
            return '';
        } elseif ($index) {
            return '';
        } else {
            return 'on';
        }
    }

    /* Permet de sauvgarder les traces des inscris
    * $description  String
    */
    public function saveTraceInscrit($descriptionCle, $listNamedFile = null)
    {
        $message = '';
        if ($this->getTypeEnveloppe() == Atexo_Config::getParameter('TYPE_ENV_CANDIDATURE')) {
            $message = Prado::localize('DEFINE_DE_CANDIDATURE_DU_FORMULAIRE');
        } elseif ($this->getTypeEnveloppe() == Atexo_Config::getParameter('TYPE_ENV_OFFRE') && $this->getConsultation()->getAlloti()) {
            $message = Prado::localize('DEFINE_DE_OFFRE_DU_FORMULAIRE_ALLOTI');
            $message = str_replace('{num_lot}', $this->getNumLot(), $message);
        } elseif ($this->getTypeEnveloppe() == Atexo_Config::getParameter('TYPE_ENV_OFFRE')) {
            $message = Prado::localize('DEFINE_DE_OFFRE_DU_FORMULAIRE');
        } elseif ($this->getTypeEnveloppe() == Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE') && $this->getConsultation()->getAlloti()) {
            $message = Prado::localize('DEFINE_DE_OFFRE_DU_FORMULAIRE_TECHNIQUE_ALLOTI');
            $message = str_replace('{num_lot}', $this->getNumLot(), $message);
        } elseif ($this->getTypeEnveloppe() == Atexo_Config::getParameter('TYPE_ENV_OFFRE_TECHNIQUE')) {
            $message = Prado::localize('DEFINE_DE_OFFRE_DU_FORMULAIRE_TECHNIQUE');
        } elseif ($this->getTypeEnveloppe() == Atexo_Config::getParameter('TYPE_ENV_ANONYMAT') && $this->getConsultation()->getAlloti()) {
            $message = Prado::localize('DEFINE_DE_OFFRE_DU_FORMULAIRE_ANONYMAT_ALLOTI');
            $message = str_replace('{num_lot}', $this->getNumLot(), $message);
        } elseif ($this->getTypeEnveloppe() == Atexo_Config::getParameter('TYPE_ENV_ANONYMAT')) {
            $message = Prado::localize('DEFINE_DE_OFFRE_DU_FORMULAIRE_ANONYMAT');
        }
        $IdDesciption = (new Atexo_ValeursReferentielles())->retrieveByPk($descriptionCle, Atexo_Config::getParameter('DESCRIPTION_HISTORIQUES_INSCRITS'), true);
        $description = (new Atexo_ValeursReferentielles())->retrieveByIdValAndIdRef($IdDesciption, Atexo_Config::getParameter('DESCRIPTION_HISTORIQUES_INSCRITS'), true);
        $descri = str_replace('[_typeEnveloppeLot_]', $message, $description);
        if ($listNamedFile) {
            $descri .= '<br/> - '.Atexo_Util::utf8ToIso(implode('<br/> - ', $listNamedFile));
        }
        $this->Page->saveTraceDiagInscrit('DESCRIPTION13', false, $descri, $IdDesciption);
    }

    /*
    *  Permet de sauvgarder les erreurs de l'applet suite a l'ajout d'une piece libre
    */
    public function traceHistoriqueNavigationInscrit($sender, $param)
    {
        if ($this->logAppletAjoutFichier->value) {
            $this->Page->saveTraceDiagInscrit('DESCRIPTION23', $this->logAppletAjoutFichier->value);
        }
    }
}
