<com:TActivePanel id="bloc_etapeDocumentsJoints" cssclass="form-field">
    <com:TCustomValidator
            id="dceVerifyIntegrity"
            ControlToValidate="hasDce"
            ValidationGroup="validateSave"
            Display="Dynamic"
            onServerValidate="verifyDceIntegrity"
            ErrorMessage="<%= $this->errorMessageDce %>"
            Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>"
    >
    </com:TCustomValidator>
    <div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
    <div class="content">
        <com:TActivePanel id="blocDocJointsDCE" CssClass="form-field">
            <div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><span class="title"><com:TTranslate>DEFINE_DOCUMENTS_JOINT_DCE</com:TTranslate></span></div>
            <div class="content">
                <com:ActivePanelMessageAvertissement ID="activeInfoMsg" Message="<%=Prado::localize('MSG_INFO_PROCEDURE_REPONDANT_SERVICE_MPS')%>" DisplayStyle="None" Visible="<%=(!Application\Service\Atexo\Atexo_Module::isEnabled('MasquerElementsMps'))? true:false %>" ></com:ActivePanelMessageAvertissement>
                <com:TActivePanel id="paneluploadBarRg" CssClass="line" Visible="<%=(Application\Service\Atexo\Atexo_Module::isEnabled('ReglementConsultation'))? true:false %>">
                    <div class="intitule-180"><label for="rg"><com:TTranslate>DEFINE_REGLEMENT_CONSULTATION</com:TTranslate></label> :</div>
                    <div class="content-bloc">
                        <com:AtexoQueryFileUpload
                                id="uploadBarRg"
                                CssClass="file-550"
                                size="70"
                                afficherProgress="1"
                        />
                        <com:THiddenField ID="hasRg" />
                        <com:TActivePanel ID="panelDownloadRc" style="display:none">
                            <div class="picto-link">
                                <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-compresse.gif" alt="<com:TTranslate>TELECHARGEMENT_COMPLET</com:TTranslate>" title="<com:TTranslate>TELECHARGEMENT_COMPLET</com:TTranslate>" />
                                <com:TActiveHyperLink ID="DownloadRc">
                                    <com:TTranslate>DEFINE_TELECHARGER_VERSION_ACTUELLE</com:TTranslate>
                                    <com:TActiveLabel ID="sizeReglementConsultation"/>
                                </com:TActiveHyperLink>
                            </div>
                        </com:TActivePanel>
                    </div>
                </com:TActivePanel>
                <div class="spacer"></div>
                <div class="line indent-10"><strong><com:TTranslate>DEFINE_DOSSIER_JOINT_DCE</com:TTranslate></strong></div>
                <com:TActivePanel id="paneluploadBarDce" CssClass="line">
                    <div class="intitule-180"><label for="dce"><com:TTranslate>DEFINE_DCE</com:TTranslate></label> :</div>
                    <div class="content-bloc">
                        <com:AtexoQueryFileUpload
                                id="uploadBarDce"
                                CssClass="file-400"
                                size="70"
                                afficherProgress="1"
                        />
                        <com:THiddenField ID="hasDce" visible="false"/>
                        <span class="info-aide-small"><com:TTranslate>TEXT_COMPRESSE_OBLIGATOIRE </com:TTranslate></span>
                        <div class="breaker"></div>
                        <com:TActivePanel id="paneltelechargementPartiel" style="display:none">
                            <div class="intitule-auto">
                                <com:TActiveCheckBox id="telechargementPartiel" CssClass="check" Attributes.title="<%=Prado::localize('DEFINE_TELECHARGEMENT_PARTIEL_DCE')%>" /><label for="telechargementPartiel"><com:TTranslate>DEFINE_TELECHARGEMENT_PARTIEL_DCE</com:TTranslate></label>
                            </div>
                        </com:TActivePanel>
                        <com:TActivePanel ID="PanelDownloadDce" style="display:none">
                            <div class="picto-link">
                                <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-compresse.gif" alt="<com:TTranslate>TELECHARGEMENT_COMPLET</com:TTranslate>" title="<%=Prado::localize('DEFINE_TELECHARGER_VERSION_ACTUELLE')%>" />
                                <com:TActiveHyperLink ID="DownloadDce">
                                    <com:TTranslate>DEFINE_TELECHARGER_VERSION_ACTUELLE</com:TTranslate>
                                    <com:TActiveLabel ID="sizeDce"/>
                                </com:TActiveHyperLink>
                            </div>
                        </com:TActivePanel>
                    </div>
                </com:TActivePanel>
                <div class="breaker"></div>
                <!--Debut Ligne Acces DCE-->
                <com:TActivePanel id="blocAccesDCE" style="display:none;">
                    <div><label><com:TTranslate>ACCES_AU_DCE </com:TTranslate></label> :</div>
                    <div class="spacer-small"></div>
                    <div class="content-bloc">
                        <com:TActivePanel id="panelaccesF" CssClass="line">
                            <div class="intitule-auto">
                                <com:TActiveRadioButton
                                        id="accesDCE"
                                        GroupName="accesDCE"
                                        Attributes.title="<%=Prado::localize('DEFINE_ACCES_PUBLIC')%>"
                                        CssClass="radio"
                                />
                                <label for="accesPublic">
                                    <com:TTranslate>TEXT_ACCES_PUBLIC</com:TTranslate>
                                </label>
                                <span class="info-aide indent-10"><com:TTranslate>TOUT_UTILISATEUR_AUTHENTIFIE_TELECHARGER_DCE</com:TTranslate></span>
                            </div>
                        </com:TActivePanel>
                        <div class="spacer-small"></div>
                        <com:TActivePanel id="panelaccesRestreintDCE" CssClass="line">
                            <div class="intitule-auto">
                                <com:TActiveRadioButton
                                        GroupName="accesDCE"
                                        id="accesRestreintDCE"
                                        Attributes.title="<%=Prado::localize('DEFINE_ACCES_RESTREINT')%>"
                                        CssClass="radio"
                                />
                                <label for="accesRestreintDCE">
                                    <com:TTranslate>DEFINE_ACCES_RESTREINT</com:TTranslate>
                                </label>
                                <label for="accessCodeDCE">- <com:TTranslate>DEFINE_CODE_ACCES</com:TTranslate> :</label>
                            </div>
                            <div class="intitule-auto">
                                <com:TTextBox MaxLength="9" Id="accessCodeDCE" Attributes.title="<%=Prado::localize('DEFINE_CODE_ACCES')%>" CssClass="float-left" />
                                <span class="intitule-auto info-aide"><com:TTranslate>SEULS_UTILISATEUR_DISPOSANT_CODE_TELECHARGER_DCE</com:TTranslate></span>
                                <com:TCustomValidator
                                        ValidationGroup="validateSave"
                                        ControlToValidate="accessCodeDCE"
                                        ClientValidationFunction="validateDCERestreintFormulaireConsultation"
                                        Display="Dynamic"
                                        ErrorMessage="<%=Prado::localize('DEFINE_CODE_ACCES')%>"
                                        Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
                                    EnableClientScript="true" >
                                    <prop:ClientSide.OnValidationError>
                                        document.getElementById('divValidationSummary').style.display='';
                                        document.getElementById('divValidationSummarySave').style.display='';
                                        document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
                                    </prop:ClientSide.OnValidationError>
                                </com:TCustomValidator>
                                <com:TCustomValidator
                                        ValidationGroup="validateInfosConsultation"
                                        ControlToValidate="accessCodeDCE"
                                        ClientValidationFunction="validateDCERestreintFormulaireConsultation"
                                        Display="Dynamic"
                                        ErrorMessage="<%=Prado::localize('DEFINE_CODE_ACCES')%>"
                                        Text="<span title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>">
                                    EnableClientScript="true" >
                                    <prop:ClientSide.OnValidationError>
                                        document.getElementById('divValidationSummary').style.display='';
                                        document.getElementById('divValidationSummarySave').style.display='';
                                        document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
                                    </prop:ClientSide.OnValidationError>
                                </com:TCustomValidator>
                            </div>
                        </com:TActivePanel>
                        <div class="breaker"></div>
                    </div>
                </com:TActivePanel>
                <!--Fin Ligne Acces DCE-->
            </div>
            <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
        </com:TActivePanel>
        <com:TActivePanel id="blocDceModif" >
            <com:AtexoDCE id="blocDce"/>
        </com:TActivePanel>
        <com:TActivePanel id="blocDceRedac" >
            <com:DCEComposant id="dceComposant"/>
        </com:TActivePanel>
        <com:THiddenField ID="remplacerPieceDansDce" />
        <com:TActivePanel id="panelDumeAcheteur" CssClass="form-field" style="display:none;">
            <div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><span class="title"><com:TTranslate>DEFINE_DUME_ACHETEUR</com:TTranslate></span></span></div>
            <div class="content">
                <!--Debut message infos-->
                <div class="bloc-message form-bloc-conf msg-info">
                    <div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
                    <div class="content">
                        <div class="message bloc-700">
                            <p><com:TTranslate>DUME_ACHETEUR_TELECHARGABLE</com:TTranslate></p>
                        </div>
                        <div class="breaker"></div>
                    </div>
                    <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
                </div>
                <!--Fin  message infos-->
                <div class="breaker"></div>
            </div>
            <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
        </com:TActivePanel>
        <com:AnnexeTechniqueDCE id="annexeTechniqueDCE"/>
        <com:TConditional condition="Application\Service\Atexo\Atexo_Config::getParameter('ACTIVE_ELABORATION_V2') == '1'">
            <prop:FalseTemplate>
                <com:TActivePanel id="panelmoduleAutrePiece" CssClass="form-field">
                    <div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><span class="title"><com:TTranslate>DEFINE_AUTRES_PIECES_TELECHARGEABLES</com:TTranslate> <span class="normal">( "<com:TTranslate>DEFINE_SAVOIR_PLUS_OPERATION</com:TTranslate>")</span></span></div>
                    <div class="content">
                        <com:TActivePanel id="paneluploadBarAutrePiece" CssClass="line">
                            <div class="intitule-180"><label for="autrePiece"><com:TTranslate>DEFINE_AUTRE_PIECE</com:TTranslate></label> :</div>
                            <div class="content-bloc">
                                <com:AtexoQueryFileUpload
                                        id="uploadBarAutrePiece"
                                        CssClass="file-400"
                                        size="70"
                                        afficherProgress="1"
                                />
                                <com:THiddenField ID="hasAutrePiece" />
                                <com:TActivePanel ID="panelDownloadAutrePiece" style="display:none">
                                    <div class="picto-link">
                                        <img src="<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-compresse.gif" alt="<com:TTranslate>TELECHARGEMENT_COMPLET</com:TTranslate>" title="<com:TTranslate>TELECHARGEMENT_COMPLET</com:TTranslate>" />
                                        <com:TActiveHyperLink ID="DownloadAutrePiece" >
                                            <com:TTranslate>DEFINE_TELECHARGER_VERSION_ACTUELLE</com:TTranslate>
                                            <com:TActiveLabel ID="sizeAutrePiece"/>
                                        </com:TActiveHyperLink>
                                    </div>
                                </com:TActivePanel>
                            </div>
                        </com:TActivePanel>
                        <div class="breaker"></div>
                    </div>
                    <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
                </com:TActivePanel>
                <com:TActivePanel CssClass="form-field" ID="panelAccesPublicRestreint">
                    <div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><span class="title"><com:TTranslate>DEFINE_INFORMATIONS_DCE</com:TTranslate></span></div>
                    <div class="content">
                        <com:TActivePanel id="panelaccesPublic" CssClass="line">
                            <div class="intitule-auto">
                                <com:TActiveRadioButton
                                        id="accesPublic"
                                        GroupName="accesType"
                                        Attributes.title="<%=Prado::localize('DEFINE_ACCES_PUBLIC')%>"
                                        CssClass="radio"
                                />
                                <label for="accesPublic">
                                    <com:TTranslate>DEFINE_ACCES_PUBLIC</com:TTranslate>
                                </label>
                                <span class="info-aide indent-10"><com:TTranslate>DEFINE_EXEMPLE_PHASE_CONDIDATURE</com:TTranslate></span>
                            </div>
                        </com:TActivePanel>
                        <div class="spacer-small"></div>
                        <com:TActivePanel id="panelaccesRestreint" CssClass="line">
                            <div class="intitule-auto">
                                <com:TActiveRadioButton
                                        GroupName="accesType"
                                        id="accesRestreint"
                                        Attributes.title="<%=Prado::localize('DEFINE_ACCES_RESTREINT')%>"
                                        CssClass="radio"
                                />
                                <label for="accesRestreint">
                                    <com:TTranslate>DEFINE_ACCES_RESTREINT</com:TTranslate>
                                </label>
                                <label for="accessCode">- <com:TTranslate>DEFINE_CODE_ACCES</com:TTranslate> :</label>
                            </div>
                            <div class="intitule-auto">
                                <com:TTextBox MaxLength="9" Id="accessCode" Attributes.title="<%=Prado::localize('DEFINE_CODE_ACCES')%>" CssClass="float-left" /><span class="intitule-auto blue"><com:TTranslate>DEFINE_PROPOSITION</com:TTranslate> : <com:TActiveLabel id="propositionAccesCode" /></span>
                                <com:TCustomValidator
                                        ValidationGroup="validateInfosConsultation"
                                        ControlToValidate="accessCode"
                                        ClientValidationFunction="validateAccessCodeFormulaireConsultation"
                                        Display="Dynamic"
                                        ErrorMessage="<%=Prado::localize('DEFINE_CODE_ACCES')%>"
                                        Text=" "
                                        EnableClientScript="true" >
                                    <prop:ClientSide.OnValidationError>
                                        document.getElementById('divValidationSummary').style.display='';
                                        document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
                                    </prop:ClientSide.OnValidationError>
                                </com:TCustomValidator>
                                <span id="erreurCodeAccess" style="display:none" title='Champ obligatoire' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='Champ obligatoire' /></span>
                                <com:TRegularExpressionValidator
                                        ID="formatAccessCode"
                                        ValidationGroup="validateInfosConsultation"
                                        RegularExpression="[0-9a-zA-Z_.-]*"
                                        ControlToValidate="accessCode"
                                        Display="Dynamic"
                                        EnableClientScript="true"
                                        ErrorMessage="<%=Prado::localize('DEFINE_CODE_ACCES') .': '.Prado::localize('CODE_RESTREINT_FORMAT_INCORRECTE')%>"
                                        Text="<span title='<%=Prado::localize('CODE_RESTREINT_FORMAT_INCORRECTE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CODE_RESTREINT_FORMAT_INCORRECTE')%>' /></span>">
                                    <prop:ClientSide.OnValidationError>
                                        document.getElementById('divValidationSummary').style.display='';
                                        document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
                                        document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
                                        document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
                                        document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
                                    </prop:ClientSide.OnValidationError>
                                </com:TRegularExpressionValidator>
                                <com:TRegularExpressionValidator
                                        ID="formatAccessCodeSave"
                                        ValidationGroup="validateSave"
                                        RegularExpression="[0-9a-zA-Z_.-]*"
                                        ControlToValidate="accessCode"
                                        Display="Dynamic"
                                        EnableClientScript="true"
                                        ErrorMessage="<%=Prado::localize('DEFINE_CODE_ACCES') .': '.Prado::localize('CODE_RESTREINT_FORMAT_INCORRECTE')%>"
                                        Text="<span title='<%=Prado::localize('CODE_RESTREINT_FORMAT_INCORRECTE')%>' class='check-invalide'><img src='<%=Application\Service\Atexo\Controller\Atexo_Controller_Front::t()%>/images/picto-check-not-ok.gif' alt='<%=Prado::localize('CODE_RESTREINT_FORMAT_INCORRECTE')%>' /></span>">
                                    <prop:ClientSide.OnValidationError>
                                        document.getElementById('divValidationSummarySave').style.display='';
                                        document.getElementById('ctl0_CONTENU_PAGE_saveBack').style.display='';
                                        document.getElementById('ctl0_CONTENU_PAGE_buttonValidate').style.display='';
                                        document.getElementById('ctl0_CONTENU_PAGE_save').style.display='';
                                        document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
                                    </prop:ClientSide.OnValidationError>
                                </com:TRegularExpressionValidator>
                            </div>
                        </com:TActivePanel>
                        <div class="breaker"></div>
                    </div>
                    <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
                </com:TActivePanel>
            </prop:FalseTemplate>
        </com:TConditional>
        <com:TActivePanel ID="blocEnvoiPostaux" CssClass="form-field">
            <div class="top"><span class="left">&nbsp;</span><span class="right">&nbsp;</span><span class="title"><com:TTranslate>TEXT_GESTION_ENVOIES_POSTAUX</com:TTranslate></span></div>
            <div class="content">
                <div class="line">
                    <com:TActivePanel ID="panelnoEnvoisPostaux" CssClass="intitule-auto">
                        <com:TActiveRadioButton  id="noEnvoisPostaux" GroupName="envoisPostaux" Attributes.title="<%=Prado::localize('TEXT_PLAN_NON')%>" CssClass="radio" /><label for="envoisPostaux"><com:TTranslate>TEXT_PLAN_NON</com:TTranslate></label>
                    </com:TActivePanel>
                </div>
                <div class="spacer-small"></div>
                <com:TActivePanel ID="blocTypeEnvoi" CssClass="line">
                    <com:TActivePanel ID="panelenvoiType" CssClass="intitule-auto">
                        <com:TActiveRadioButton  id="envoiType" GroupName="envoisPostaux"  Attributes.title="<%=Prado::localize('TEXT_PLAN_OUI')%>" CssClass="radio" /><label for="envoiType"><com:TTranslate>TEXT_PLAN_OUI</com:TTranslate> :</label>
                    </com:TActivePanel>
                    <div class="breaker"></div>
                    <com:TActivePanel ID="panelpapier" CssClass="intitule-auto indent-20">
                        <com:TActiveCheckBox id="papier" CssClass="check" Attributes.title="<%=Prado::localize('TEXT_PAPIER')%>" /><label for="papier"><com:TTranslate>TEXT_PAPIER </com:TTranslate></label>
                    </com:TActivePanel>
                    <div class="breaker"></div>
                    <com:TActivePanel ID="panelcdRom" CssClass="intitule-auto indent-20">
                        <com:TActiveCheckBox id="cdRom" CssClass="check" Attributes.title="<%=Prado::localize('TEXT_CD_ROM')%>" /><label for="cdRom"><com:TTranslate>TEXT_CD_ROM </com:TTranslate></label>
                    </com:TActivePanel>
                    <div class="breaker"></div>

                    <com:TActivePanel ID="panelpar" CssClass="intitule-bloc bloc-155 indent-20"><label for="fournisseurDoc"><com:TLabel ID="par" Text="<%=Prado::localize('TEXT_PAR')%>"/></label></com:TActivePanel>

                    <com:TActivePanel ID="panelfournisseurDoc">
                        <com:TActiveDropDownList id="fournisseurDoc" CssClass="long-550" Attributes.title="<%=Prado::localize('TEXT_TYPE_PROCEDURE')%>"> </com:TActiveDropDownList>
                    </com:TActivePanel>
                    <div class="breaker"></div>
                    <com:TActivePanel ID="paneldescriptif" CssClass="intitule-bloc bloc-155 indent-20">
                        <label for="descriptifDoscComp"><com:TLabel ID="descriptif" Text="<%=Prado::localize('TEXT_DESCRIPTIF')%>"/></label></com:TActivePanel>
                    <com:TActivePanel ID="paneldescriptifDoscComp">
                        <com:TTextBox id="descriptifDoscComp" Attributes.OnChange="replaceCaracSpecial(this);" TextMode="MultiLine" CssClass="long-550" Attributes.title="<%=Prado::localize('TEXT_DESCRIPTIF')%>"></com:TTextBox>
                    </com:TActivePanel>
                </com:TActivePanel>
                <div class="breaker"></div>
            </div>
            <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>
        </com:TActivePanel>
    </div>
    <div class="bottom"><span class="left">&nbsp;</span><span class="right">&nbsp;</span></div>

</com:TActivePanel>
<com:TActiveLabel id="labelServerSide" style="display:none"/>

<com:TCustomValidator
        Id="validationFournisseurDoc"
        ValidationGroup="validateInfosConsultation"
        ControlToValidate="fournisseurDoc"
        ClientValidationFunction="checkFournisseurDocFormulaireConsultation"
        Display="Dynamic"
        ErrorMessage="<%=Prado::localize('TEXT_FOURNISSEUR_DOC')%>"
        Text=" "
        EnableClientScript="true" >
    <prop:ClientSide.OnValidationError>
        document.getElementById('divValidationSummary').style.display='';
        document.getElementById('ctl0_CONTENU_PAGE_blocBoutons').style.display='';
    </prop:ClientSide.OnValidationError>
</com:TCustomValidator>